var express = require('express');
var app = express();
var bodyParser = require('body-parser')

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))
//Ekvivalent
/*var urlencodedParses = bodyParser.urlencoded({ extended: false });
app.post('/shoform', urlencodedParses, function(req, res){
    res.render('form');
});*/
 
// parse application/json
//app.use(bodyParser.json())

app.set("view engine","ejs");

app.get('/', function(req, res){
    console.log('home');
    res.sendFile(__dirname + '/site.html');
});
app.get('/home', function(req, res){
    res.send('HOME');
});
app.get('/data/:id', function(req, res){
    res.send(req.params.id);
});
app.get('/profile/:name', function(req, res){
    res.render('profile',{name: req.params.name});
});

app.get('/shoform', function(req, res){
    res.render('form');
});
app.post('/shoform', function(req, res){
    console.log(req.body);
    res.render('form');
});

app.use('/assets', express.static('/ssets'));

app.listen(3000);
console.log("Server runing...");
console.log("PORT: 3000");