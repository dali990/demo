<?php 
$msg = "Included and created FROM PHP!";
 ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>bootsrap-vue</title>
    <link rel="stylesheet" href="dist/main.css">
  </head>
  <body>
    <div id="app">
    </div>
    <?php include "views/vue_x_template.php";?>
    <script src="dist/build.js"></script>
  </body>
</html>
