commandKeyCodes(keyCode, ctrl, shift){
    if(
        shift ||
        keyCode === 17 || //ctrl (kad se radu kombinacija ctrl+a on na kraju trigeruje keyup samo za ctrl)
        keyCode === 37 || keyCode === 39 || //levo, desno
        ctrl === true && (keyCode >= 65 && keyCode <= 90) || //ctrl a...
        (ctrl === true && (keyCode === 86 || keyCode === 88 || keyCode === 90)) //paste,cut,undo
    )
    {
        return true;
    }
    return false;
}

Vue.filter("formatNumber", function (value) {
    if (typeof value === 'number')
    {
        value = (value/1).toFixed(2);
    }
    
    var numeric_separators = sima.getNumericSeparators();
    
    var left_side = value;
    var right_side = null;
    if (value.indexOf('.') > -1)
    {
        var value_parts = value.split('.');
        var left_side = value_parts[0];
        var right_side = value_parts[1];
    }
    
    var formated_value = left_side.replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1' + numeric_separators['thousands']);
    
    if (right_side !== null)
    {
        formated_value += numeric_separators['decimals'] + right_side;
    }
    
    return formated_value;
});

function setValueByPath(obj, path, value) {
    path = path.split('.');
    for (i = 0; i < path.length - 1; i++)
    {
        obj = obj[path[i]];
    }
    obj[path[i]] = value;
}