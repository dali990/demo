(function($){
    var methods = {
        init: function(params)
        {
            var localThis = $(this);
            localThis.data('provider_delimiter', params.provider_delimiter);       
            localThis.data('phone_delimiter',    params.phone_delimiter);       
            localThis.data('old_cnt_delimiters', 0);             
            localThis.data('new_cnt_delimiters', 0);             
            localThis.data('cursor_phone',       0);             
            localThis.data('max_length', 10);
                   
            localThis.data('country_number',    localThis.find(".sima-ui-sf:first"));
            localThis.data('provider_number',   localThis.find(".provider-number"));
            localThis.data('phone_number',      localThis.find(".phone-number"));
            localThis.data('phone_number_only', localThis.find(".phone-number-only"));
            var provider_number = localThis.data('provider_number');
            var phone_number    = localThis.data('phone_number');

            localThis.data('provider_number').on('paste',  providerNumberPaste);
            localThis.data('provider_number').on('keydown',{localThis: localThis}, providerNumberKeyDown);
            localThis.data('provider_number').on('keyup',  {localThis: localThis}, providerNumberKeyUp);
            localThis.data('phone_number').on('keydown',   {localThis: localThis}, phoneNumberKeyDown);
            localThis.data('phone_number').on('paste',     {localThis: localThis}, phoneNumberPaste);
            localThis.data('phone_number').on('keyup',     {localThis: localThis}, phoneNumberKeyUp);                   
            localThis.data('phone_number').on('click',     {localThis: localThis}, phoneNumberClick);                   
            localThis.data('country_number').on('onValue',   function() {
                provider_number.focus();
            });
            localThis.data('phone_number').attr('maxlength', localThis.data('max_length') + (localThis.data('phone_delimiter').length * 3));
            localThis.data('provider_number').attr('maxlength', 3);
            //Ako je prosledjen model ili broj telefona u widget-u
            if (provider_number.val() !== "" && phone_number.val() !== "") 
            {   
                if(parseInt(provider_number.val()[0]) === 0)//Ako je prva cifra 0 onda je odvajamo iz broja
                {
                    provider_number.val(provider_number.val().substring(1, provider_number.val().lenght));              
                }
                provider_number.val(provider_number.val().replace(/[^0-9]/g, ""));
                localThis.data('phone_number').val(phone_number.val().replace(/[^0-9]/g, ""));
                formatNumber(localThis);
            }

            //Selektuje prosledjenu vrednost u search fieldu
            if (params.country_display_name !== null) 
            {
                localThis.data('country_number').simaSearchField('setValue', params.country_id, params.country_display_name);
            }       

        },
        getValue: function(){
            var localThis = this;
            return {
                country_id:      localThis.find(".sima-ui-sf").simaSearchField('getValue').id,
                provider_number: localThis.find(".provider-number").val(),
                phone_number:    localThis.find(".phone-number-only").val()
            };
        }
    };
    
    function providerNumberKeyDown(e)
    {   
        var localThis = e.data.localThis;
        var provider_number = $(this);

        if(provider_number.val().length === 0 && (e.keyCode === 96 || e.keyCode === 48))
        {
            return false;                
        } 

        if (!isValidCharachter(e.keyCode, e.ctrlKey)) 
        {
            return false;
        }

        var selected_characters = provider_number.prop('selectionEnd') - provider_number.prop('selectionStart');

        if ((e.keyCode >= 96 && e.keyCode <= 105) || (e.keyCode >= 48 && e.keyCode <= 57)) 
        {
            //Sa duzinom od 3 karaktera i bez selectovanog broj, prelazi na sledece polje
            if (provider_number.val().length >= 3 && selected_characters === 0) 
            {   
                localThis.data('phone_number').focus();
                localThis.data('phone_number').caret(0);
            }
        }
        //Desna strelica
        if (e.keyCode === 39) 
        {   
            //Ako je kursor vec postavljen ispred poslednjeg broja i ako se jos jednom pritinse desna strelica, prebacujemo se u sledece polje
            if (provider_number.caret() === provider_number.val().length) 
            {
                localThis.data('phone_number').focus();
                localThis.data('phone_number').caret(0);
                //Vracamo false kako bi sprecili okidanje desne strelice u polje phone number
                return false;
            }
            e.stopPropagation();
        }
        //Za backspace prelazak u prethodno polje
        if (    
                (e.keyCode === 8 || e.keyCode === 37 ) && 
                (provider_number.val().length === 0 || provider_number.caret() === 0) && 
                selected_characters === 0
            ) 
        {
            localThis.data('country_number').find('.sima-ui-sf-display').click();
        } 
        //Leva strelica
        else if (e.keyCode === 37) 
        {
            if (provider_number.caret() === 0) 
            {
                localThis.data('country_number').find('.sima-ui-sf-display').click();
            }
            e.stopPropagation();
        } 
    }
        
    function providerNumberPaste(e)
    {  
        var provider_number = $(this);
        setTimeout(function () {        
            provider_number.val(provider_number.val().replace(/[^0-9]/g, "").substr(0,3));
            if(parseInt(provider_number.val()[0]) === 0)//Ako je prva cifra 0 onda je odvajamo iz broja
            {
                provider_number.val(provider_number.val().substring(1, provider_number.val().lenght));   
            }   
        },0);  
    }
        
    function providerNumberKeyUp(e)
    {
        var provider_number = $(this);
        provider_number.val(provider_number.val().replace(/[^0-9]/g, ""));
    }
        
    function phoneNumberKeyDown(e)
    {               
        var localThis      = e.data.localThis;
        var phone_number   = $(this); 
        var caret_position = phone_number.caret();
        localThis.data('cursor_phone', 0);
               
        //Validacija unetih karaktera sa ctrl+copy, paste, cut naredbama i upravljanje sa kursorom tokom izmene brojeva
        if (isValidCharachter(e.keyCode, e.ctrlKey)) 
        {
            //Backspace
            var selected_characters = phone_number.prop('selectionEnd') - phone_number.prop('selectionStart');
            if (e.keyCode === 8 && caret_position === 0 && selected_characters === 0)//Za prelazak u provider number polje
            {   
                formatNumber(localThis); //Posto backspace preskace delimitere, ovo ce da izbrise delimitere ako se backpsace drzi dugo
                localThis.data('provider_number').focus();
                localThis.data('provider_number').caret(localThis.data('provider_number').val().length);
                return false;
            }               
            else if (e.keyCode === 8 && phone_number.val().length > 3) 
            {   
                var character = phone_number.val().charAt(caret_position-1);//Karakter levo od kursora 
                if (isNaN(character) || character === " ") 
                {
                    phone_number.caret(phone_number.caret()-localThis.data('phone_delimiter').length); 
                }              
            }
            
            //Delete
            if (e.keyCode === 46 && phone_number.val().length > 3) 
            {
                var character = phone_number.val().charAt(phone_number.caret());//Karakter desno od kursora
                if (isNaN(character) || character === " ") 
                {   
                    //Kursor preskace delimiter izbrse broj i POSLE formatiranja kursor ce se naci za jedan karakter ispred, zato ga treba vratiti za 1 elemenat u nazad (value.length se skratila za 1 izbrisani karakter)
                    phone_number.caret(phone_number.caret() + localThis.data('phone_delimiter').length);
                    localThis.data('cursor_phone', phone_number.caret()); 
                }                 
            }

            //Leva strelica, preskace delimiter
            if (e.keyCode === 37) 
            {
                if (this.value.length > 3) 
                {
                    var character = phone_number.val().charAt(caret_position - 1);
                    if (isNaN(character) || character === " ") 
                    {
                        //Preskace delimiter, pomera kursor za duzinu delimitera u levo
                        phone_number.caret(caret_position + 1 - localThis.data('phone_delimiter').length); 
                    }  
                }                

                if (phone_number.caret() === 0)
                {
                    localThis.data('provider_number').focus();
                    localThis.data('provider_number').caret(localThis.data('provider_number').val().length);
                    return false; //Sprecavamo da se triguje leva strelica u provider polje
                }
                e.stopPropagation();
            } 
            //Desna strelica preskace delimiter
            if (e.keyCode === 39) 
            {
                if (this.value.length > 3 ) 
                {
                    var character = phone_number.val().charAt(phone_number.caret());
                    if (isNaN(character) || character === " ")
                    {
                        phone_number.caret((phone_number.caret() - 1) + localThis.data('phone_delimiter').length);
                    }
                }               
                e.stopPropagation();
            }
        }
        else 
        {
            return false;
        }
    }
        
    function phoneNumberPaste(e)
    {
        setTimeout(function () {
            var localThis = e.data.localThis;
            formatNumber(localThis); 
        }, 0);      
    }
    
    function phoneNumberKeyPress(e)
    {   
        
//        var phone_number = $(this);
//        var localThis = e.data.localThis;
//        var phone_number_only_length = localThis.data('phone_number_only').val().length;
//        //Zbog toga sto se ne vrsi formatiranje broja tokom keyPress-a, potrebno je proveriti da li korisnik drzi taster
//        if (phone_number_only_length === 0) //Ako je nula to znaci da korisnik kuca prvi broj i tek treba da se okine keyUp zajedno sa formatNumber()
//        {        
//            if (phone_number.val().length > 10) 
//            {
//                localThis.data('cursor_phone', 'LAST');
//                return false;
//            }
//        } 
//        else if (phone_number_only_length > 0 ) //korisnik je vec nesto ukucao i eventualno postoji formatiran broj koji treba ocistiti
//        {
//            var selected_characters = phone_number.prop('selectionEnd') - phone_number.prop('selectionStart');
// 
//            if (phone_number.val().replace(/[^0-9]/g, "").length > 11 && selected_characters === 0) 
//            {
//                localThis.data('cursor_phone', 'LAST');
//                return false;
//            }           
//            
//        }
    }
    
    function phoneNumberKeyUp(e)
    {  
        var localThis = e.data.localThis;    
        var phone_number = $(this);
        var caret_position = phone_number.caret();
        localThis.data('old_cnt_delimiters', phone_number.val().substring(0, phone_number.caret()).split(localThis.data('phone_delimiter')).length-1);
        formatNumber(localThis);
        checkCursor(localThis, caret_position, e) 
    }
        
    function phoneNumberClick(e)
    {
        var phone_number = $(this);   
        if (phone_number.val().length > 3) 
        {
            // desni karakter od kursora
            var character_right = phone_number.val().charAt(phone_number.caret());
            // levi karakter od kursora
            var character_left = phone_number.val().charAt(phone_number.caret()-1);
            if (
                    (isNaN(character_right) || character_right === " ") && 
                    (isNaN(character_left) || character_left === " ")  
               ) 
            {
                phone_number.caret(phone_number.val().length);
            }
        }
    }     
    
    function isValidCharachter(code, ctrl = false)
    {   
        //ako je broj, backspace, tab, delete, levo, desno
        if (
                (code >= 48 && code <= 57) || (code >= 96 && code <= 105) || code === 8 || code === 9 || code === 46 || code === 37 || code === 39 ||  
                (ctrl === true && (code === 65 || code === 67 || code === 86 || code === 88 )) || //select all,copy,paste,cut,undo
                code === 35 || code === 36 || code === 13 || code === 27 //end, home, enter, esc
            )
        {   
            return true;
        }
        else
        {
            return false;
        }
    }   
    
    function checkCursor(localThis, caret_position, e)
    {          
        var phone_number = localThis.data('phone_number');
        if (localThis.data('cursor_phone') === 'LAST') 
        {
            caret_position = phone_number.val().length;
        }
        //Pozicioniranje kursora za delete i backspace
        if (e.keyCode === 46 || e.keyCode === 8) 
        {
            if(localThis.data('cursor_phone') !== 0)
            {
                phone_number.caret(localThis.data('cursor_phone'));
            }
            else 
            {
                phone_number.caret(caret_position);//Mora da se izvrsava samo za odredjene keyCode u suprotno CTRL+A nece moci da selektuje           
            }
        }
        //Provera pozicije i pomeranje kursora
        if ((e.keyCode >= 96 && e.keyCode <= 105) || (e.keyCode >= 48 && e.keyCode <= 57)) 
        {              
            //Dodavanje novih brojeva, upravljanje kursorom
            var character_right = phone_number.val().charAt(caret_position);//Karakter desno od kursora
            var character_left  = phone_number.val().charAt(caret_position-1);//Karakter levo od kursora
            if (!(isNaN(character_right) || character_right === " ") && !(isNaN(character_left) || character_left === " ")) 
            {   
                phone_number.caret(caret_position);               
            }
        }
        localThis.data('new_cnt_delimiters', phone_number.val().substring(0, phone_number.caret()).split(localThis.data('phone_delimiter')).length-1);
        var selected_characters = phone_number.prop('selectionEnd') - phone_number.prop('selectionStart');
        
        if (e.keyCode !== 37 && e.keyCode !== 39 && selected_characters === 0) 
        {
            if (localThis.data('old_cnt_delimiters') > localThis.data('new_cnt_delimiters')) 
            {           
                phone_number.caret((caret_position - localThis.data('phone_delimiter').length));
            }
            else if (localThis.data('old_cnt_delimiters') < localThis.data('new_cnt_delimiters')) 
            {
                phone_number.caret((caret_position + localThis.data('phone_delimiter').length));
            }
        }
    }

    //formatira vrednost inputa
    function formatNumber(localThis)
    {   
        var phone_number       = localThis.data('phone_number');
        var phone_number_only  = phone_number.val().replace(/[^0-9]/g, "").substring(0, localThis.data('max_length'));
        localThis.data('phone_number_only').val(phone_number_only);
        
        if (phone_number_only.length > 3)
        {    
            var chunks = sima.strSplit(phone_number_only, 3);
            
            if (chunks[chunks.length-1].length === 1)
            {
                var last_num = chunks[chunks.length-2][2]; //Poslednji broj predzadnjeg elementa u nizu
                chunks[chunks.length-2] = chunks[chunks.length-2].slice(0, -1); //Brisanje poslednjeg broja predzadnjeg elementa u nizu
                chunks[chunks.length-1] = last_num + chunks[chunks.length-1];  
            }
            var number = chunks.join(localThis.data('phone_delimiter'));

            phone_number.val(number);
        } 
        else 
        {
            phone_number.val(phone_number_only);
        }
    }   
    
    $.fn.simaPhoneField = function(methodOrOptions) 
    {
        var ar = arguments; 
        var i=1;
        var ret = this;
        this.each(function() 
        {
            i++;
            if (methods[methodOrOptions]) 
            {
                var povr = methods[ methodOrOptions ].apply($(this), Array.prototype.slice.call(ar, 1));
                if (typeof povr !== 'undefined')
                {
                    ret = povr;
                }
                return;
            } 
            else if (typeof methodOrOptions === 'object' || !methodOrOptions) 
            {
                // Default to "init"
                return methods.init.apply($(this), ar);
            } 
            else 
            {
                $.error('Method ' + methodOrOptions + ' does not exist on jQuery.simaPhoneField');
            }
        });
        return ret;
    };
})( jQuery );