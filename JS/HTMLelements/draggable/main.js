var div = document.getElementById("Draggable");
console.log(div);
var span = div.querySelector("span");
var header = div.querySelector("#header");

var mouseX, mouseY, x, y = 0;

header.addEventListener('mousedown', function(ev) {
    mouseX = ev.clientX;
    mouseY = ev.clientY;
    document.onmousemove = function(e) {
        x = mouseX - e.clientX;
        y = mouseY - e.clientY;
        mouseX = e.clientX;
        mouseY = e.clientY;
        span.textContent = "X: " + e.clientX + " Y: " + e.clientY;
        div.style.left = (div.offsetLeft - x) + "px";
        div.style.top = (div.offsetTop - y) + "px";
    };

})
header.onmouseup = function(e) {
    console.log("Mouse Up");
    document.onmouseup = null;
    document.onmousemove = null;
}