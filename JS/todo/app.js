var express = require('express');
var todoController = require('./controllers/todoController');

var app = express();

//set up template engine
app.set('view engine', 'ejs');

//static files
//app.use('/assets', express.static('./public'));
app.use(express.static('./public')); //za bilo koj ulr u fromatu 'localhost/images/img.jph'

//fire controllers
todoController(app);

app.listen(3001);
console.log('server - port : 3001');

