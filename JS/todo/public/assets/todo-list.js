$(document).ready(function(){
console.log("RADI");
  $('form').on('submit', function(){

      var item = $('form input');
      var todo = {item: item.val()};

      $.ajax({
        type: 'POST',
        url: '/todo',
        data: todo,
        success: function(data){
          //do something with the data via front-end framework

          location.reload();
        }
      });

      return false;

  });

  $('li').on('click', function(){
      var id = $(this).find("input[name='id']").val();
      console.log(id);
      var item = $(this).text().replace(/ /g, "-");
      $.ajax({
        type: 'DELETE',
        url: '/todo/' + id,
        success: function(data){
          //do something with the data via front-end framework
          location.reload();
        }
      });
  });

});
