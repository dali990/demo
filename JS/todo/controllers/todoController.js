var bodyParser = require('body-parser');
var mongoose = require('mongoose');

mongoose.set('useNewUrlParser', true);
mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);
mongoose.set('useUnifiedTopology', true);
mongoose.connect("mongodb+srv://root:dobrasifra1122@sirus-nkhay.mongodb.net/test");

var todoSchema = new mongoose.Schema({
    item: String
});

var Todo = mongoose.model('Todo', todoSchema);
/* var itemOne = Todo({item: 'something to do'});
itemOne.save(function(err) {
    if(err) throw err;
    console.log("item saved");
}); */

var urlencodedParser = bodyParser.urlencoded({extended: false});
var _data = [
    {item: 'get milk'},
    {item: 'walk'},
    {item: 'coding'},
];

module.exports = function(app){

    app.get('/todo', function(req, res){
        Todo.find({}, function(err, data){
            if (err) throw err;
            res.render('todo', {todos: data});
        });
        
    });
    
    app.post('/todo', urlencodedParser, function(req, res){
        /* _data.push(req.body);
        res.json(_data); */
        var newTodo = Todo(req.body);
        newTodo.save(function(err, data){
            if (err) throw err;
            res.json(data);
        });
    });
    
    app.delete('/todo/:id', function(req, res){
        /* _data = _data.filter(function(todo) {
            return todo.item.replace(/ /g, '-') !== req.params.item;
        })
        res.json(_data); */
        console.log("DELTE");
        Todo.findByIdAndDelete(req.params.id, function(err){
            if (err) throw err;
        });
    });

}