var base = require('./base');
var events = require('events');
var fs = require('fs');
var http = require('http');

var server = http.createServer(function(req, res){
    console.log("Request: "+req.url);
    /* res.writeHead(200, {'Content-Type': 'text/plain'});
    res.end('Hello') */;
    if(req.url == '/data')
    {
        var data = {
            name: "data",
            value: true
        };
        res.writeHead(200, {'Content-Type': 'application/json'});
        res.end(JSON.stringify(data));
    }
    else {
        res.writeHead(200, {'Content-Type': 'text/html'});
        var readStream = fs.createReadStream(__dirname + '/site.html', 'utf8');
        readStream.pipe(res);
    }  
});
server.listen(3000, '127.0.0.1');
console.log("Server listening");
