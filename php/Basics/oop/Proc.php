<?php

function bubleSort($niz){    
        $is_sorted = false;
        while(!$is_sorted)
        {
            $is_sorted = true;
            for ($i=0; $i < count($niz)-1; $i++) {
                if($niz[$i] > $niz[$i+1])
                {
                    $temp = $niz[$i];
                    $niz[$i] = $niz[$i+1];
                    $niz[$i+1] = $temp;
                    $is_sorted = false;
                }

            }
        }
    return $niz;
}

function fak($n){
    $result = 1;
    if($n>1){
        return fak($n-1)*$n;
    }
    return $result;
}