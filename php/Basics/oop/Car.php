<?php

class Car extends Make  {
    public $name = 'car';

    public function __construct($name='Car') {
        $this->name = $name;
    }

    public function name(){
        return $this->name;
    }

    /* public function downName(){
        return strtolower($this->name);
    } */
}