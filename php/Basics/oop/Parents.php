<?php

interface IMake {
    public function downName();
}

abstract class Make implements IMake {
    public $name = 'Make';
    abstract protected function name();

    public function upName()
    {
        return strtoupper($this->name);
    }
    public function downName(){
        return strtolower($this->name);
    }
}