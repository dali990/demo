<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

include 'Parents.php';
include 'Car.php';
include 'Proc.php';

function printr($data)
{
    highlight_string("\n<?php\n" . var_export($data, true) . ";\n?>");
}