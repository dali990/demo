<?php include 'home.php' ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>File upload</title>
    <style>
        .galery {
            border: 1px solid grey;
            padding: 10px;
            margin: 10px;
            display: flex;
            flex-wrap: wrap;
        }
        .img img {
            margin: 10px;
            max-height: 150px;
        }
    </style>
</head>
<body>
    <form action="upload.php" method="post" enctype="multipart/form-data">
        <input type="file" name="images[]" multiple>
        <input type="submit" value="Upload">
    </form>

    <div class="galery">
        <?php if(!empty($files)): ?>
            <?php foreach ($files as $file_name): ?>
                <div class="img">
                    <img src="images/<?=$file_name?>" alt="">
                </div>
            <?php endforeach ?>
        <?php else: ?>
            Nema slika!
        <?php endif ?>
    </div>
</body>
</html>
