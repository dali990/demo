<?php

function printr($data){
    echo "<pre>";
    print_r($data);
    echo "</pre>";
}

if(!empty($_FILES['images']['name'][0])){
    $files = $_FILES['images'];
    $uploaded = [];
    $failed = [];
    $upload_error_msgs = [];
    $image_extensions = ['jpg', 'jpeg', 'png', 'bnp'];

    foreach ($files['name'] as $index => $file_name) {
        $file_tmp = $files['tmp_name'][$index];
        $file_size = $files['size'][$index];
        $file_error = $files['error'][$index];

        $file_ext = strtolower(explode('.', $file_name)[1]);

        //Validacija
        if(!in_array($file_ext, $image_extensions)){
            $failed[$index] = "$file_name ekstenzija fajla nije podrzana";            
        }
        if($file_error != 0){
            $failed[$index] = "$file_name greska. Error code: $file_error";
        }
        if($file_size > 20971520)//20mb
        {
            $failed[$index] = "$file_name ima vise od 20MB";
        }
        //Ako nema gresaka
        if(empty($failed))
        {
            $file_name_new = uniqid().".".$file_ext;
            $upload_dir = "images/".$file_name_new;
            //Uspesan upload
            if(move_uploaded_file($file_tmp, $upload_dir))
            {
                $uploaded[$index] = $file_name_new;
            }
            //Greska tokom uploada
            else
            {
                echo "$file_name_new <br>";
                $upload_error_msgs[$index] = "$file_name_new nije uspeo da se uploaduje u $upload_dir";
            }
        }
    }

    //Provera da li su svi fajlove uspesno uploadovani
    $files_keys = array_keys($_FILES['images']['name']);
    $uploaded_files_keys = array_keys($uploaded);

    $diff = array_diff($files_keys, $uploaded_files_keys);
    if(empty($diff))
    {
        //Svi fajlovi uspesno uploadovani
        //header("Location : index.php");
        //printr($_SERVER);
        echo "Upload OK! <br>";
        echo "<a href='./'> <<< Back</a>";
    }
    else 
    {
        //Greska
    }
}