<?php
namespace OOP;

abstract class CarAbstract {
    public $make;
    public $speed;

    abstract public function start();

    protected function addSpeed(){
        $this->speed += 2;
    }

    public function __toString()
    {
        return __CLASS__;
    }
}