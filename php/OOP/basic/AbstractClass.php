<?php
namespace OOP\basic;
//Class
//Ne moze da se instancira
abstract class AbstractClass {
    public $name;
    //abstract public $id;

    //greska - ne moze da ima telo
    //abstract public function getName(){};

    //Ne moze private
    abstract protected function getName();
}
