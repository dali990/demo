<?php
namespace OOP\basic;

//Klasa moze da nasledi samo jednu abstraktnu ili bilo koju drugu klasu
class PersonClass extends \OOP\basic\AbstractClass{
    public static $COUNT = 0;
    public $name = "PERSON";

    //u abstract moze da bude drugi visibility/access level tj protected,
    //child ne moze da bude private
    //poptis mora da bude isti sa parametrima
    public function getName()
    {
        return $this->name;
    }

    final function getFullName()
    {
        return $this->name;
    }

    public static final function clonePerson()
    {
        return "CLONE";
    }
}
