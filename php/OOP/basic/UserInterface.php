<?php
namespace OOP\basic;

interface UserInterface {
    //Ne moze da sadrzi promenljive
    const code = "USER I";
    //Sve metode moraju biti public
    public function getUserName();
}
