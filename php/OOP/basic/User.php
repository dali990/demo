<?php
namespace OOP\basic;

//Klasa moze da implementira vise interfejsa
class User extends 
    \OOP\basic\PersonClass implements \OOP\basic\UserInterface, 
    \OOP\basic\PersonInterface
{
    //mora da ima isti potpis kao i parent metoda
    /* public function getName($person)
    {
        # code...
    } */

    //ucitava uvek final iz parenta
    /* public function getFullName()
    {
        return "FULL NAME";
    } */

    public function getName()
    {
        return self::$COUNT;
    }

    //ako je u parent staticka mora i ovde, ne moze override ako je u parent final
    /* public static function clonePerson()
    {
        # code...
    } */
    
    //Mora da ima isti visibility/access level kao u interface tj public
    public function getUserName()
    {
        return "user name";
    }

    public function __call($function, $arg)
    {
        echo "Calling $function";
    }
}
