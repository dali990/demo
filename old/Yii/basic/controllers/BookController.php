<?php 
namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;

class BookController extends Controller
{

  public function actionIndex($value='')
  {
    $data['books'] = [
      ['id'=>1,"name"=>"Bokk A"],
      ['id'=>2,"name"=>"Book BokkB"],
      ['id'=>3,"name"=>"Book C"]
    ];
    return $this->render("index",$data);
  }

  public function actionView($id="")
  {
    $data['id']=$id;
    return $this->render("view",$data);
  }

}


 ?>
