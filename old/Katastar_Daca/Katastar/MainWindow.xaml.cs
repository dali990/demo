﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data;

namespace Katastar
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        KatastarDataContext context = new KatastarDataContext();
        ObservableCollection<Parcele> kolekcijaParcela;
        public MainWindow()
        {
            InitializeComponent();

            comoboBoxOpstine.ItemsSource = context.KatastarskeOpstines;
        }

        private void fillGrid()
        {
            int id = int.Parse(comoboBoxOpstine.SelectedValue.ToString());

            string ConString = ConfigurationManager.ConnectionStrings["Katastar.Properties.Settings.KatastarConnectionString"].ConnectionString;
            string CmdString = string.Empty;
            using (SqlConnection con = new SqlConnection(ConString))
            {
                CmdString = "SELECT * FROM Parcele "
                    +"LEFT JOIN Objekti ON Parcele.IDParcele = Objekti.IDParcele " 
                    +"WHERE Parcele.IDKatOpstine = "+id;
                SqlCommand cmd = new SqlCommand(CmdString, con);
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable("Studenti");
                sda.Fill(dt);
                dataGrid.ItemsSource = dt.DefaultView;
            }
            //var parcele = from p in context.Parceles
            //              join o in context.Objektis
            //              on p.IDParcele equals o.IDParcele
            //              where p.IDKatOpstine == id
            //              select new { p.IDParcele, p.Povrsina, p.Vlasnistvo, p.Adresa, o.IDObjekta, o.Kvadratura, o.Uknjizeno, o.Vlasnik };

           // dataGrid.ItemsSource = parcele;
        }

        private void comoboBoxOpstine_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            fillGrid();
            countObjekte();
        }

        //Neuknjizeni objekti na izabranoj parceli
        private void countObjekte()
        {
            int id = int.Parse(comoboBoxOpstine.SelectedValue.ToString());
            int ukupno = (from o in context.Objektis
                           where o.IDKatOpstine == id && o.Uknjizeno == false
                           select o).Count();

            textBlockNeuknjizeni.Text = ukupno.ToString();
        }

        private void RowDelete_Click(int id)
        {
            MessageBox.Show(id.ToString());
        }

        private void buttonVlasnistvo_Click(object sender, RoutedEventArgs e)
        {
            string vlasnistvo = "" ;
            
            if (radioButtonDrzavno.IsChecked == true)
            {
                vlasnistvo = radioButtonDrzavno.Content.ToString();
                radioButtonDrzavno.IsChecked = false;
            }
            else if(radioButtonNaKoriscenje.IsChecked == true)
            {
                vlasnistvo = radioButtonNaKoriscenje.Content.ToString();
                radioButtonNaKoriscenje.IsChecked = false;
            }
            else if (radioButtonPrivatno.IsChecked == true)
            {
                vlasnistvo = radioButtonPrivatno.Content.ToString();
                radioButtonPrivatno.IsChecked = false;
            }
            if (string.IsNullOrEmpty(vlasnistvo))
            {
                MessageBox.Show("Izaberi tip vlasnistva");
            }
            else if (string.IsNullOrEmpty(textBoxIdParcele.Text))
            {
                MessageBox.Show("Unesi sifru parcele");
            }
            else
            {
                int idParcele = int.Parse(textBoxIdParcele.Text);
                var parcela = context.Parceles.SingleOrDefault(p => p.IDParcele == idParcele);
                if (parcela != null)
                {
                    parcela.Vlasnistvo = vlasnistvo;
                    context.SubmitChanges();
                    MessageBox.Show("Sacuvano");

                    fillGrid();
                }
            }
            

            
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            NoviObjekat no = new NoviObjekat();
            no.Show();
        }

        private void dataGrid_LoadingRowDetails_1(object sender, DataGridRowDetailsEventArgs e)
        {
           /* e.DetailsElement
            dynamic parcela = dataGrid.SelectedItem;
            MessageBox.Show(parcela.IDParcele.ToString());*/
        }

        //Context Menu za brisanje iz dataGrid
        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {
            DataRowView selectedValue = dataGrid.SelectedItem as DataRowView;
            int idParcele = int.Parse(selectedValue["IDParcele"].ToString());

            Parcele parcela = context.Parceles.Where(p => p.IDParcele == idParcele).SingleOrDefault();
            context.Parceles.DeleteOnSubmit(parcela);
            context.SubmitChanges();

            fillGrid();
            MessageBox.Show("Brisanje");
        }
    }
}
