﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Katastar
{
    /// <summary>
    /// Interaction logic for NoviObjekat.xaml
    /// </summary>
    public partial class NoviObjekat : Window
    {
        KatastarDataContext context = new KatastarDataContext();
        public NoviObjekat()
        {
            InitializeComponent();
            comboBoxKatOpstine.ItemsSource = context.KatastarskeOpstines;
        }

        private void comboBoxKatOpstine_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {            
            fillListBox();
        }

        private void fillListBox()
        {
            int idOpstine = int.Parse(comboBoxKatOpstine.SelectedValue.ToString());

            string ConString = ConfigurationManager.ConnectionStrings["Katastar.Properties.Settings.KatastarConnectionString"].ConnectionString;
            string CmdString = string.Empty;
            using (SqlConnection con = new SqlConnection(ConString))
            {
                CmdString = "SELECT * FROM Parcele LEFT JOIN Objekti ON Parcele.IDParcele = Objekti.IDParcele "
                    + "WHERE Parcele.IDKatOpstine = " + idOpstine + " AND "
                    + "Parcele.IDParcele NOT IN (SELECT Objekti.IDParcele FROM Objekti)";
                SqlCommand cmd = new SqlCommand(CmdString, con);
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable("Studenti");
                sda.Fill(dt);
                listBox.ItemsSource = dt.DefaultView;
            }

            int idObjekta = context.Objektis.OrderByDescending(o => o.IDObjekta).First().IDObjekta + 1;
            textBoxIDObjekta.Text = idObjekta.ToString();
        }

        private void buttonPotvrdi_Click(object sender, RoutedEventArgs e)
        {      
            List<string> emptyFields = new List<string>();
            if(string.IsNullOrEmpty(textBoxIDObjekta.Text))
            {
                emptyFields.Add("Sifra objekta");
            }
            if (string.IsNullOrEmpty(textBoxVlasnik.Text))
            {
                emptyFields.Add("Vlasnik");
            }
            if (string.IsNullOrEmpty(textBoxKvadratura.Text))
            {
                emptyFields.Add("Kvadratura");
            }
            if(listBox.SelectedItem == null)
            {
                emptyFields.Add("Izberite parcelu");
            }
            

            if (emptyFields.Count > 0)
            {
                string msg = string.Join(",\n", emptyFields.ToArray());
                MessageBox.Show("Molimo vas pupunite sledeca polja:\n"+msg);
            }
            else
            {

                DataRowView selectedValue = listBox.SelectedItem as DataRowView;
                int idParcele = int.Parse(selectedValue["IDParcele"].ToString());

                Objekti objekat = new Objekti();
                objekat.IDParcele = idParcele;
                objekat.IDKatOpstine = int.Parse(comboBoxKatOpstine.SelectedValue.ToString());
                objekat.IDObjekta = int.Parse(textBoxIDObjekta.Text);
                objekat.Vlasnik = textBoxVlasnik.Text;
                objekat.Kvadratura = int.Parse(textBoxKvadratura.Text);
                objekat.Uknjizeno = checkBox.IsChecked;
                
                context.Objektis.InsertOnSubmit(objekat);
                try
                {
                    context.SubmitChanges();
                    fillListBox();
                    MessageBox.Show("Uspesno sacuvan objekat (Sifra: " + textBoxIDObjekta.Text + ")");
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Ne moze da se upise u bazu" + ex);
                }
            }
        }
    }
}
