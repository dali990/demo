﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Katastar
{
	using System.Data.Linq;
	using System.Data.Linq.Mapping;
	using System.Data;
	using System.Collections.Generic;
	using System.Reflection;
	using System.Linq;
	using System.Linq.Expressions;
	using System.ComponentModel;
	using System;
	
	
	[global::System.Data.Linq.Mapping.DatabaseAttribute(Name="Katastar")]
	public partial class KatastarDataContext : System.Data.Linq.DataContext
	{
		
		private static System.Data.Linq.Mapping.MappingSource mappingSource = new AttributeMappingSource();
		
    #region Extensibility Method Definitions
    partial void OnCreated();
    partial void InsertKatastarskeOpstine(KatastarskeOpstine instance);
    partial void UpdateKatastarskeOpstine(KatastarskeOpstine instance);
    partial void DeleteKatastarskeOpstine(KatastarskeOpstine instance);
    partial void InsertObjekti(Objekti instance);
    partial void UpdateObjekti(Objekti instance);
    partial void DeleteObjekti(Objekti instance);
    partial void InsertParcele(Parcele instance);
    partial void UpdateParcele(Parcele instance);
    partial void DeleteParcele(Parcele instance);
    #endregion
		
		public KatastarDataContext() : 
				base(global::Katastar.Properties.Settings.Default.KatastarConnectionString, mappingSource)
		{
			OnCreated();
		}
		
		public KatastarDataContext(string connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public KatastarDataContext(System.Data.IDbConnection connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public KatastarDataContext(string connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public KatastarDataContext(System.Data.IDbConnection connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public System.Data.Linq.Table<KatastarskeOpstine> KatastarskeOpstines
		{
			get
			{
				return this.GetTable<KatastarskeOpstine>();
			}
		}
		
		public System.Data.Linq.Table<Objekti> Objektis
		{
			get
			{
				return this.GetTable<Objekti>();
			}
		}
		
		public System.Data.Linq.Table<Parcele> Parceles
		{
			get
			{
				return this.GetTable<Parcele>();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.KatastarskeOpstine")]
	public partial class KatastarskeOpstine : INotifyPropertyChanging, INotifyPropertyChanged
	{
		
		private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
		
		private int _IDKatOpstine;
		
		private string _Naziv;
		
		private EntitySet<Parcele> _Parceles;
		
    #region Extensibility Method Definitions
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void OnIDKatOpstineChanging(int value);
    partial void OnIDKatOpstineChanged();
    partial void OnNazivChanging(string value);
    partial void OnNazivChanged();
    #endregion
		
		public KatastarskeOpstine()
		{
			this._Parceles = new EntitySet<Parcele>(new Action<Parcele>(this.attach_Parceles), new Action<Parcele>(this.detach_Parceles));
			OnCreated();
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_IDKatOpstine", AutoSync=AutoSync.OnInsert, DbType="Int NOT NULL IDENTITY", IsPrimaryKey=true, IsDbGenerated=true)]
		public int IDKatOpstine
		{
			get
			{
				return this._IDKatOpstine;
			}
			set
			{
				if ((this._IDKatOpstine != value))
				{
					this.OnIDKatOpstineChanging(value);
					this.SendPropertyChanging();
					this._IDKatOpstine = value;
					this.SendPropertyChanged("IDKatOpstine");
					this.OnIDKatOpstineChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Naziv", DbType="NChar(64) NOT NULL", CanBeNull=false)]
		public string Naziv
		{
			get
			{
				return this._Naziv;
			}
			set
			{
				if ((this._Naziv != value))
				{
					this.OnNazivChanging(value);
					this.SendPropertyChanging();
					this._Naziv = value;
					this.SendPropertyChanged("Naziv");
					this.OnNazivChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.AssociationAttribute(Name="KatastarskeOpstine_Parcele", Storage="_Parceles", ThisKey="IDKatOpstine", OtherKey="IDKatOpstine")]
		public EntitySet<Parcele> Parceles
		{
			get
			{
				return this._Parceles;
			}
			set
			{
				this._Parceles.Assign(value);
			}
		}
		
		public event PropertyChangingEventHandler PropertyChanging;
		
		public event PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			if ((this.PropertyChanging != null))
			{
				this.PropertyChanging(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(String propertyName)
		{
			if ((this.PropertyChanged != null))
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
		
		private void attach_Parceles(Parcele entity)
		{
			this.SendPropertyChanging();
			entity.KatastarskeOpstine = this;
		}
		
		private void detach_Parceles(Parcele entity)
		{
			this.SendPropertyChanging();
			entity.KatastarskeOpstine = null;
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.Objekti")]
	public partial class Objekti : INotifyPropertyChanging, INotifyPropertyChanged
	{
		
		private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
		
		private int _IDObjekta;
		
		private string _Vlasnik;
		
		private int _Kvadratura;
		
		private System.Nullable<bool> _Uknjizeno;
		
		private int _IDParcele;
		
		private int _IDKatOpstine;
		
		private EntityRef<Parcele> _Parcele;
		
    #region Extensibility Method Definitions
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void OnIDObjektaChanging(int value);
    partial void OnIDObjektaChanged();
    partial void OnVlasnikChanging(string value);
    partial void OnVlasnikChanged();
    partial void OnKvadraturaChanging(int value);
    partial void OnKvadraturaChanged();
    partial void OnUknjizenoChanging(System.Nullable<bool> value);
    partial void OnUknjizenoChanged();
    partial void OnIDParceleChanging(int value);
    partial void OnIDParceleChanged();
    partial void OnIDKatOpstineChanging(int value);
    partial void OnIDKatOpstineChanged();
    #endregion
		
		public Objekti()
		{
			this._Parcele = default(EntityRef<Parcele>);
			OnCreated();
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_IDObjekta", DbType="Int NOT NULL", IsPrimaryKey=true)]
		public int IDObjekta
		{
			get
			{
				return this._IDObjekta;
			}
			set
			{
				if ((this._IDObjekta != value))
				{
					this.OnIDObjektaChanging(value);
					this.SendPropertyChanging();
					this._IDObjekta = value;
					this.SendPropertyChanged("IDObjekta");
					this.OnIDObjektaChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Vlasnik", DbType="NChar(64) NOT NULL", CanBeNull=false)]
		public string Vlasnik
		{
			get
			{
				return this._Vlasnik;
			}
			set
			{
				if ((this._Vlasnik != value))
				{
					this.OnVlasnikChanging(value);
					this.SendPropertyChanging();
					this._Vlasnik = value;
					this.SendPropertyChanged("Vlasnik");
					this.OnVlasnikChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Kvadratura", DbType="Int NOT NULL")]
		public int Kvadratura
		{
			get
			{
				return this._Kvadratura;
			}
			set
			{
				if ((this._Kvadratura != value))
				{
					this.OnKvadraturaChanging(value);
					this.SendPropertyChanging();
					this._Kvadratura = value;
					this.SendPropertyChanged("Kvadratura");
					this.OnKvadraturaChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Uknjizeno", DbType="Bit")]
		public System.Nullable<bool> Uknjizeno
		{
			get
			{
				return this._Uknjizeno;
			}
			set
			{
				if ((this._Uknjizeno != value))
				{
					this.OnUknjizenoChanging(value);
					this.SendPropertyChanging();
					this._Uknjizeno = value;
					this.SendPropertyChanged("Uknjizeno");
					this.OnUknjizenoChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_IDParcele", DbType="Int NOT NULL")]
		public int IDParcele
		{
			get
			{
				return this._IDParcele;
			}
			set
			{
				if ((this._IDParcele != value))
				{
					if (this._Parcele.HasLoadedOrAssignedValue)
					{
						throw new System.Data.Linq.ForeignKeyReferenceAlreadyHasValueException();
					}
					this.OnIDParceleChanging(value);
					this.SendPropertyChanging();
					this._IDParcele = value;
					this.SendPropertyChanged("IDParcele");
					this.OnIDParceleChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_IDKatOpstine", DbType="Int NOT NULL")]
		public int IDKatOpstine
		{
			get
			{
				return this._IDKatOpstine;
			}
			set
			{
				if ((this._IDKatOpstine != value))
				{
					if (this._Parcele.HasLoadedOrAssignedValue)
					{
						throw new System.Data.Linq.ForeignKeyReferenceAlreadyHasValueException();
					}
					this.OnIDKatOpstineChanging(value);
					this.SendPropertyChanging();
					this._IDKatOpstine = value;
					this.SendPropertyChanged("IDKatOpstine");
					this.OnIDKatOpstineChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.AssociationAttribute(Name="Parcele_Objekti", Storage="_Parcele", ThisKey="IDParcele,IDKatOpstine", OtherKey="IDParcele,IDKatOpstine", IsForeignKey=true)]
		public Parcele Parcele
		{
			get
			{
				return this._Parcele.Entity;
			}
			set
			{
				Parcele previousValue = this._Parcele.Entity;
				if (((previousValue != value) 
							|| (this._Parcele.HasLoadedOrAssignedValue == false)))
				{
					this.SendPropertyChanging();
					if ((previousValue != null))
					{
						this._Parcele.Entity = null;
						previousValue.Objektis.Remove(this);
					}
					this._Parcele.Entity = value;
					if ((value != null))
					{
						value.Objektis.Add(this);
						this._IDParcele = value.IDParcele;
						this._IDKatOpstine = value.IDKatOpstine;
					}
					else
					{
						this._IDParcele = default(int);
						this._IDKatOpstine = default(int);
					}
					this.SendPropertyChanged("Parcele");
				}
			}
		}
		
		public event PropertyChangingEventHandler PropertyChanging;
		
		public event PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			if ((this.PropertyChanging != null))
			{
				this.PropertyChanging(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(String propertyName)
		{
			if ((this.PropertyChanged != null))
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.Parcele")]
	public partial class Parcele : INotifyPropertyChanging, INotifyPropertyChanged
	{
		
		private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
		
		private int _IDParcele;
		
		private int _IDKatOpstine;
		
		private int _Povrsina;
		
		private string _Adresa;
		
		private string _Vlasnistvo;
		
		private EntitySet<Objekti> _Objektis;
		
		private EntityRef<KatastarskeOpstine> _KatastarskeOpstine;
		
    #region Extensibility Method Definitions
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void OnIDParceleChanging(int value);
    partial void OnIDParceleChanged();
    partial void OnIDKatOpstineChanging(int value);
    partial void OnIDKatOpstineChanged();
    partial void OnPovrsinaChanging(int value);
    partial void OnPovrsinaChanged();
    partial void OnAdresaChanging(string value);
    partial void OnAdresaChanged();
    partial void OnVlasnistvoChanging(string value);
    partial void OnVlasnistvoChanged();
    #endregion
		
		public Parcele()
		{
			this._Objektis = new EntitySet<Objekti>(new Action<Objekti>(this.attach_Objektis), new Action<Objekti>(this.detach_Objektis));
			this._KatastarskeOpstine = default(EntityRef<KatastarskeOpstine>);
			OnCreated();
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_IDParcele", DbType="Int NOT NULL", IsPrimaryKey=true)]
		public int IDParcele
		{
			get
			{
				return this._IDParcele;
			}
			set
			{
				if ((this._IDParcele != value))
				{
					this.OnIDParceleChanging(value);
					this.SendPropertyChanging();
					this._IDParcele = value;
					this.SendPropertyChanged("IDParcele");
					this.OnIDParceleChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_IDKatOpstine", DbType="Int NOT NULL", IsPrimaryKey=true)]
		public int IDKatOpstine
		{
			get
			{
				return this._IDKatOpstine;
			}
			set
			{
				if ((this._IDKatOpstine != value))
				{
					if (this._KatastarskeOpstine.HasLoadedOrAssignedValue)
					{
						throw new System.Data.Linq.ForeignKeyReferenceAlreadyHasValueException();
					}
					this.OnIDKatOpstineChanging(value);
					this.SendPropertyChanging();
					this._IDKatOpstine = value;
					this.SendPropertyChanged("IDKatOpstine");
					this.OnIDKatOpstineChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Povrsina", DbType="Int NOT NULL")]
		public int Povrsina
		{
			get
			{
				return this._Povrsina;
			}
			set
			{
				if ((this._Povrsina != value))
				{
					this.OnPovrsinaChanging(value);
					this.SendPropertyChanging();
					this._Povrsina = value;
					this.SendPropertyChanged("Povrsina");
					this.OnPovrsinaChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Adresa", DbType="NChar(64) NOT NULL", CanBeNull=false)]
		public string Adresa
		{
			get
			{
				return this._Adresa;
			}
			set
			{
				if ((this._Adresa != value))
				{
					this.OnAdresaChanging(value);
					this.SendPropertyChanging();
					this._Adresa = value;
					this.SendPropertyChanged("Adresa");
					this.OnAdresaChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Vlasnistvo", DbType="NChar(64)")]
		public string Vlasnistvo
		{
			get
			{
				return this._Vlasnistvo;
			}
			set
			{
				if ((this._Vlasnistvo != value))
				{
					this.OnVlasnistvoChanging(value);
					this.SendPropertyChanging();
					this._Vlasnistvo = value;
					this.SendPropertyChanged("Vlasnistvo");
					this.OnVlasnistvoChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.AssociationAttribute(Name="Parcele_Objekti", Storage="_Objektis", ThisKey="IDParcele,IDKatOpstine", OtherKey="IDParcele,IDKatOpstine")]
		public EntitySet<Objekti> Objektis
		{
			get
			{
				return this._Objektis;
			}
			set
			{
				this._Objektis.Assign(value);
			}
		}
		
		[global::System.Data.Linq.Mapping.AssociationAttribute(Name="KatastarskeOpstine_Parcele", Storage="_KatastarskeOpstine", ThisKey="IDKatOpstine", OtherKey="IDKatOpstine", IsForeignKey=true)]
		public KatastarskeOpstine KatastarskeOpstine
		{
			get
			{
				return this._KatastarskeOpstine.Entity;
			}
			set
			{
				KatastarskeOpstine previousValue = this._KatastarskeOpstine.Entity;
				if (((previousValue != value) 
							|| (this._KatastarskeOpstine.HasLoadedOrAssignedValue == false)))
				{
					this.SendPropertyChanging();
					if ((previousValue != null))
					{
						this._KatastarskeOpstine.Entity = null;
						previousValue.Parceles.Remove(this);
					}
					this._KatastarskeOpstine.Entity = value;
					if ((value != null))
					{
						value.Parceles.Add(this);
						this._IDKatOpstine = value.IDKatOpstine;
					}
					else
					{
						this._IDKatOpstine = default(int);
					}
					this.SendPropertyChanged("KatastarskeOpstine");
				}
			}
		}
		
		public event PropertyChangingEventHandler PropertyChanging;
		
		public event PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			if ((this.PropertyChanging != null))
			{
				this.PropertyChanging(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(String propertyName)
		{
			if ((this.PropertyChanged != null))
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
		
		private void attach_Objektis(Objekti entity)
		{
			this.SendPropertyChanging();
			entity.Parcele = this;
		}
		
		private void detach_Objektis(Objekti entity)
		{
			this.SendPropertyChanging();
			entity.Parcele = null;
		}
	}
}
#pragma warning restore 1591
