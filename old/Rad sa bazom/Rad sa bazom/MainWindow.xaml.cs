﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data;  // dodato
using System.Data.SqlClient; // dodato 

namespace Rad_sa_bazom
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        StudentiDataContext DC = new StudentiDataContext();

        public MainWindow()
        {
            InitializeComponent();
        }

        // povezivanje tabela Student i Smer
        // za studenate koji imaju prosek iznad 8.0, prikazati ime, naziv smera i prosek
        private void btnDobriStud_Click(object sender, RoutedEventArgs e)
        {
            StudentiDataContext DC = new StudentiDataContext();

            var stud = from s in DC.Students
                       join sm in DC.Smers
                       on s.SmerID equals sm.SmerID
                       where s.Prosek > (float)Convert.ToDouble(textBox.Text)
                       select new { s.Ime, sm.Naziv, s.Prosek };

            lstPrikazi.ItemsSource = stud;
        }

        // poziv StoredProcedure
        // prikazati sve predmete
        private void btnPredmeti_Click(object sender, RoutedEventArgs e)
        {
            lstPrikazi.ItemsSource = DC.spSviPredmeti();
        }

        // AutoCompleteTextBox
        // automatsko popunjavanje ListBox-a na osnovu ukucanog teksta u TextBox
        private void txtTrazi_TextChanged(object sender, RoutedEventArgs e)
        {
            var prikazi = from s in DC.Students
                          where s.Ime.StartsWith(txtTrazi.Text)
                          select s.Ime;

            lstBoxTrazi.Visibility = System.Windows.Visibility.Visible;

            if (!string.IsNullOrEmpty(txtTrazi.Text))
            {
                lstBoxTrazi.ItemsSource = prikazi;
            }
            else if (txtTrazi.Text.Equals(""))
            {
                lstBoxTrazi.Visibility = System.Windows.Visibility.Collapsed;
                lstViewNadjen.Visibility = System.Windows.Visibility.Collapsed;
                lstBoxTrazi.ItemsSource = null;
            }
        }

        // prebacivanje izabranog studenta iz ListBox u ListView
        private void lstBoxTrazi_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lstBoxTrazi.ItemsSource != null)
            {
                var st = from s in DC.Students
                         where s.Ime == lstBoxTrazi.SelectedItem.ToString()
                         select s;

                lstViewNadjen.Visibility = System.Windows.Visibility.Visible;

                lstViewNadjen.ItemsSource = st;

                btnBrisi.IsEnabled = true;
                btnIzmeni.IsEnabled = true;
                txtProsek.IsEnabled = true;
            }
        }

        // Delete
        // izbrisati izabranog studenta
        private void btnBrisi_Click(object sender, RoutedEventArgs e)
        {
            string txtBrisi = lstBoxTrazi.SelectedItem.ToString();

            Student brisi = (from s in DC.Students
                             where s.Ime == lstBoxTrazi.SelectedItem.ToString()
                             select s).Single();

            //  Student brisi = DC.Students.Single(s=>s.Ime == lstBoxTrazi.SelectedItem.ToString()); // preko lambda izraza

            MessageBoxResult rez = MessageBox.Show("Da li si siguran da zelis da izbrises podatke o " + txtBrisi + "?", "Brisanje", MessageBoxButton.YesNo);

            if (rez == MessageBoxResult.Yes)
            {
                // ubacuje se u red za brisanje
                DC.Students.DeleteOnSubmit(brisi);

                try
                {
                    // izrsavaju se promene
                    DC.SubmitChanges();
                    MessageBox.Show("Podaci o " + txtBrisi + " su uspesno izbrisani iz baze", "Obavestenje", MessageBoxButton.OK, MessageBoxImage.Information);
                    lstViewNadjen.ItemsSource = null;
                    lstBoxTrazi.ItemsSource = null;
                    lstViewNadjen.Visibility = System.Windows.Visibility.Collapsed;
                    lstBoxTrazi.Visibility = System.Windows.Visibility.Collapsed;
                    txtTrazi.Text = "";
                }

                catch (Exception ex)
                {
                    MessageBox.Show("Ne moze da se izbrise iz baze" + ex);
                }
            }
        }

        // Update
        // Izmeniti prosek izabranom studentu na vrednost unetu u TextBox-u
        private void btnIzmeni_Click(object sender, RoutedEventArgs e)
        {
            Student izmeni = (from s in DC.Students
                              where s.Ime == lstBoxTrazi.SelectedItem.ToString()
                              select s).Single();

            // postavljanje vrenosti iz TextBox-a u atribut Prosek
            izmeni.Prosek = (float)Convert.ToDouble(txtProsek.Text);

            try
            {
                // izrsavaju se promene
                DC.SubmitChanges();
                MessageBox.Show("Prosek za " + lstBoxTrazi.SelectedItem.ToString() + " je uspesno izmenjen u bazi", "Obavestenje", MessageBoxButton.OK, MessageBoxImage.Information);
                var novo = from s in DC.Students
                           where s.Ime == lstBoxTrazi.SelectedItem.ToString()
                           select s;
                lstViewNadjen.ItemsSource = novo;
            }

            catch (Exception ex)
            {
                MessageBox.Show("Ne mogu da se izmene podaci u bazi" + ex);
            }
            txtProsek.Clear();
        }

        // Insert
        // Ubaciti novog studenta
        private void btnUbaci_Click(object sender, RoutedEventArgs e)
        {
            Student nov = new Student
            {
                Ime = "Maksim",
                Prezime = "Pajic",
                StudentID = 17,
                GodinaUpisa = 2004,
                DatumRodjenja = "3.5.1989",
                Skolarina = 2500,
                Prosek = (float)7.65,
                SmerID = 502
            };

            // ubacuje se u red za insertovanje
            DC.Students.InsertOnSubmit(nov);

            try
            {
                // izrsavaju se sve promene koje se nalaze u redu
                DC.SubmitChanges();
                MessageBox.Show("U bazu su uspesno ubaceni podaci o " + nov.Ime + " " + nov.Prezime, "Obavestenje", MessageBoxButton.OK, MessageBoxImage.Information);
                lstBoxTrazi.Visibility = System.Windows.Visibility.Collapsed;
                txtTrazi.Text = "";
                var ubacen = from s in DC.Students
                             where s.StudentID == nov.StudentID
                             select s;
                lstViewNadjen.ItemsSource = ubacen;
                lstViewNadjen.Visibility = System.Windows.Visibility.Visible;
            }

            catch (Exception ex)
            {
                MessageBox.Show("Ne moze da se upise u bazu" + ex);
            }
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            Window1 Prvi = new Window1();
            Prvi.Show();

            StudentiDataContext DC = new StudentiDataContext();

            var stud2 = from s in DC.Students                  
                       select new { s.Ime, s.Prezime};

            Prvi.listViewSviStud.ItemsSource = stud2;


        }
    }
}
