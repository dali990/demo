﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ProbaKolokvijum
{
  
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        BolnicaDataContext DC = new BolnicaDataContext();
        public MainWindow()
        {
            InitializeComponent();
            FillDataGrid();
            puniCombo();
            comboPrioritetPuni();
        }

        public void FillDataGrid()
        {
            var pacijenti = from p in DC.Pacijents
                            join o in DC.Odeljenjes
                            on p.OdeljenjeID equals o.OdeljenjeID
                            join s in DC.Sobas on
                            p.SobaID equals s.SobaID
                            select new PacijentViewModel
                            {
                                Sifra = p.PacijentID,
                                Ime = p.Ime,
                                Prezime = p.Prezime,
                                BrojDana = p.BrojDana,
                                Odeljenje = o.OdeljenjeID,
                                Soba = s.SobaID
                            };

            DataGridPacijent.ItemsSource = pacijenti;
        }

        private void DataGridPacijent_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            IzracunataCena.Text = Convert.ToString(Convert.ToInt32(BrojDana.Text) * Convert.ToInt32(Participacija.Text));
        }

        private void puniCombo()
        {
            var odeljenja = from o in DC.Odeljenjes
                            select new {

                                o.Naziv
                            };

            foreach (var item in odeljenja)
            {
                comboOdeljenja.Items.Add(item.Naziv.ToString());
                
            };
        }

        private void btnOtpusti_Click(object sender, RoutedEventArgs e)
        {
            
                var id = Convert.ToInt32(tbOtpusti.Text);

                var pacijents = from p in DC.Pacijents
                                where p.PacijentID == id
                                select p;

                DC.Pacijents.DeleteOnSubmit((Pacijent)pacijents.Single());
                DC.SubmitChanges();
                FillDataGrid();

            
        }

        private void comboOdeljenja_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {


            string Ime = comboOdeljenja.SelectedItem.ToString();

            var pacijenti = from p in DC.Pacijents
                            join o in DC.Odeljenjes
                            on p.OdeljenjeID equals o.OdeljenjeID
                            where o.Naziv == Ime
                            select new PacijentViewModel
                            {
                                Ime = p.Ime,
                                Prezime = p.Prezime
                            };

            ListBoxPacijenti.ItemsSource = pacijenti;
                      
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
      
        }

        private void MaxDana_Click(object sender, RoutedEventArgs e)
        {
            string Ime = comboOdeljenja.SelectedItem.ToString();

            var pacijenti = from p in DC.Pacijents
                            join o in DC.Odeljenjes
                            on p.OdeljenjeID equals o.OdeljenjeID
                            where o.Naziv == Ime && p.BrojDana ==(DC.Pacijents.Where(m=>m.OdeljenjeID == p.OdeljenjeID).Select( p=>p.BrojDana).Max())
                            select new PacijentViewModel
                            {
                                Ime = p.Ime,
                                Prezime = p.Prezime
                            };

            ListBoxPacijenti.ItemsSource = pacijenti;
        }

        void comboPrioritetPuni()
        {
            var prioriteti = (from p in DC.Pacijents
                              select new { p.Prioritet } ).Distinct();


            foreach (var item in prioriteti)
            {
                ComboPrioritet.Items.Add(item.Prioritet.ToString());
            }



        }

        private void ComboPrioritet_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int prioritet = Convert.ToInt32(ComboPrioritet.SelectedItem.ToString() );

            var pacijenti = from p in DC.Pacijents
                            join o in DC.Odeljenjes on
                            p.OdeljenjeID equals o.OdeljenjeID
                            where p.Prioritet == prioritet
                            select new
                            {
                                p.Ime,
                                p.Prezime,
                                o.Naziv


                            };

            ListBoxPacijenti.ItemsSource = pacijenti;

        }

        private void UnosNovogClick(object sender, RoutedEventArgs e)
        {
            Unos unosWindow = new Unos();
            unosWindow.Show();
        }
    }
}
