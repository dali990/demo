﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProbaKolokvijum
{
    public  class PacijentViewModel
    {
        public int Sifra { get; set; }
        public string Ime { get; set; }
        public string Prezime { get; set; }
        public int Odeljenje { get; set; }
        public int? BrojDana{ get; set; }
        public int Soba { get; set; }
        public string NazivOdeljenja { get; set; }

    }
}
