﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ProbaKolokvijum
{
    /// <summary>
    /// Interaction logic for Unos.xaml
    /// </summary>
    public partial class Unos : Window
    {
        BolnicaDataContext DC = new BolnicaDataContext();
        public Unos()
        {
            InitializeComponent();
            puniComboOdeljenje();
        }

        private void puniComboOdeljenje()
        {
            var odeljenja = from o in DC.Odeljenjes
                            select new
                            {
                                o.Naziv
                            };

            foreach (var item in odeljenja)
            {
                Odeljenje.Items.Add(item.Naziv.ToString());


            }

        }

        private void Odeljenje_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Soba.Items.Clear();
            string naziv = Odeljenje.SelectedItem.ToString();

            var sobe = from o in DC.Odeljenjes
                          join s in DC.Sobas
                          on o.OdeljenjeID equals s.OdeljenjeID
                          where o.Naziv == naziv
                          select new
                          {
                              s.SobaID
                          };

            foreach (var item in sobe)
            {
                Soba.Items.Add(item.SobaID.ToString());
            }
        }

        private void Soba_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var sprat = from s in DC.Sobas
                        select new
                        {
                            s.Sprat
                        };

            foreach (var item in sprat)
            {
                TextboxSprat.Text = item.Sprat.ToString();

            }




        }

        private void Unos_Click(object sender, RoutedEventArgs e)
        {
            if(string.IsNullOrEmpty(tbIme.Text) && string.IsNullOrEmpty(tbPrezime.Text) && string.IsNullOrEmpty(tbSifra.Text) && Odeljenje.SelectedItem== null && Soba.SelectedItem == null)
            {
                MessageBox.Show("sCUGFFED jAYS");
            }
            
                else
            {

                try
                {
                    var odeljenjeID = (from o in DC.Odeljenjes
                                      where o.Naziv == Odeljenje.SelectedItem.ToString()
                                      select new
                                      {
                                          o.OdeljenjeID
                                      }).Single();

                   



                    Pacijent p = new Pacijent();



                    p.Ime = tbIme.Text;
                    p.Prezime = tbPrezime.Text;
                    p.PacijentID = Convert.ToInt32(tbSifra.Text);
                    p.OdeljenjeID = odeljenjeID.OdeljenjeID;
                    p.Prioritet = Convert.ToInt32(tbPrioritet.Text);
                    p.SobaID = Convert.ToInt32(Soba.SelectedItem.ToString());



                    


                    DC.Pacijents.InsertOnSubmit(p);

                    DC.SubmitChanges();
                }
                catch (Exception)
                {

                    throw;
                }
            }

            
         


        }

        private void Odustani_click(object sender, RoutedEventArgs e)
        {

            this.Close();
        }
    }
}
