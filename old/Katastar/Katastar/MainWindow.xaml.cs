﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Katastar
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        KatastarDataContext _context = new KatastarDataContext();
        public MainWindow()
        {
            InitializeComponent();
            puniComboOpstine();
        }

        private void puniComboOpstine()
        {
            var opstine = from o in _context.KatastarskeOpstines
                          select new
                          {
                              o.Naziv
                          };

            foreach (var item in opstine)
            {
                cb_KatOpstine.Items.Add(item.Naziv.ToString());
            }
        }

        private void puniDataGrid()
        {
            var parcele = from p in _context.Parcelas
                          join o in _context.KatastarskeOpstines
                          on p.IDKatOpstine equals o.IDKatOpstina
                          join ob in _context.Objekats
                          on new { p.IDParcele, o.IDKatOpstina } equals new { ob.IDParcele, ob.IDKatOpstina }
                          where o.Naziv == cb_KatOpstine.SelectedItem.ToString()
                          select new ParceleViewModel
                          {
                              Adresa = p.Adresa,
                              Vlasnistvo = p.Vlasnistvo,
                              Povrsina = p.Povrsina,
                              SifraParcele = p.IDParcele,
                              Kvadratura = ob.Kvadratura,
                              Vlasnik = ob.Vlasnik,
                              SifraObjekta = ob.IDObjekta,
                              Uknjizeno = ob.Uknjizeno,


                          };

            DataGrid_Parcele.ItemsSource = parcele;
        }

        private void cb_KatOpstine_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                puniDataGrid();
                var neuknjizeni = (from o in _context.Objekats
                                   join k in _context.KatastarskeOpstines
                                   on o.IDKatOpstina equals k.IDKatOpstina
                                   where o.Uknjizeno == false && k.Naziv == cb_KatOpstine.SelectedItem.ToString()
                                   select o).Count();


                tb_Neuknjizeni.Text = neuknjizeni.ToString();

            }

            catch (Exception)
            {

                throw;
            }
         
        }

        private void RadioButton_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void btn_PromeniVlasnistvo_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var parcela = from p in _context.Parcelas
                              join k in _context.KatastarskeOpstines
                              on p.IDKatOpstine equals k.IDKatOpstina
                              where p.IDParcele == Convert.ToInt32(tb_SifraParcele.Text) && k.Naziv ==  cb_KatOpstine.SelectedItem.ToString()
                              select p;


         

              if(rb_Drzavno.IsChecked == true)
                {
                    parcela.Single().Vlasnistvo = rb_Drzavno.Content.ToString();
                } 
              if(rb_Koriscenje.IsChecked == true)
                {
                    parcela.Single().Vlasnistvo = rb_Koriscenje.Content.ToString();

                }
              if (rb_Privatno.IsChecked == true)
                {
                    parcela.Single().Vlasnistvo = rb_Privatno.Content.ToString();

                }


                _context.SubmitChanges();
                puniDataGrid();
              
            }
            catch (Exception)
            {
                MessageBox.Show("morate uneti samo onu sifru parcele iz selektovane katastarske opstine");
            }
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ParceleViewModel p = (ParceleViewModel)DataGrid_Parcele.SelectedItem;

                var objekat = from o in _context.Objekats
                                       where o.IDObjekta == p.SifraObjekta
                                       select o;

                

                _context.Objekats.DeleteOnSubmit(objekat.Single());
                _context.SubmitChanges();
                puniDataGrid();

                MessageBox.Show("Uspesno obrisano");





                


                 
                
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {
            DodajNovog d = new DodajNovog();
            d.ShowDialog();
        }
    }
}
