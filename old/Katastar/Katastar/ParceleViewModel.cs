﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Katastar
{
    public class ParceleViewModel
    {
        public int SifraParcele { get; set; }
        public int Povrsina { get; set; }
        public string Adresa { get; set; }
        public string Vlasnistvo { get; set; }
        public string Vlasnik { get; set; }
        public int SifraObjekta { get; set; }
        public int Kvadratura { get; set; }
        public bool? Uknjizeno { get; set; }
    }
}
