﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Katastar
{
    /// <summary>
    /// Interaction logic for DodajNovog.xaml
    /// </summary>
    public partial class DodajNovog : Window
    {
        KatastarDataContext _context = new KatastarDataContext();
        public DodajNovog()
        {
            InitializeComponent();
            puniComboOpstine();
        }

        private void puniComboOpstine()
        {
            var opstine = from o in _context.KatastarskeOpstines
                          select new
                          {
                              o.Naziv
                          };

            foreach (var item in opstine)
            {
                cb_KatOpstine.Items.Add(item.Naziv.ToString());
            }
        }

        private void cb_KatOpstine_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                var parcele = from p in _context.Parcelas
                              join k in _context.KatastarskeOpstines
                              on p.IDKatOpstine equals k.IDKatOpstina
                              where k.Naziv == cb_KatOpstine.SelectedItem.ToString()
                              select new ParceleViewModel
                              {
                                  SifraParcele = p.IDParcele,
                                  Adresa = p.Adresa,
                                  Vlasnistvo = p.Vlasnistvo
                              };

                ListBoxParcele.ItemsSource = parcele;
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void btn_Potvrdi_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Objekat o = new Objekat();


                

                o.IDKatOpstina = _context.KatastarskeOpstines.Where(m => m.Naziv == cb_KatOpstine.SelectedItem.ToString()).Select(m=>m.IDKatOpstina).Single();
                o.IDParcele = ((ParceleViewModel)ListBoxParcele.SelectedItem).SifraParcele;
                o.Kvadratura = Convert.ToInt32(tb_Kvadratura.Text);
                o.Vlasnik = tb_Vlasnik.Text;
                o.IDObjekta = Convert.ToInt32(tb_Sifra.Text);


                _context.Objekats.InsertOnSubmit(o);
                _context.SubmitChanges();
                MessageBox.Show("Bravo");
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
