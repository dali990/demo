﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ApotekaVezbanje1
{
    /// <summary>
    /// Interaction logic for UnosNovogLeka.xaml
    /// </summary>
    public partial class UnosNovogLeka : Window
    {
        ApotekaDataContext context = new ApotekaDataContext();

        public Recept noviRecept;
        public UnosNovogLeka()
        {
            InitializeComponent();
            cbDijagnoza.ItemsSource = context.Dijagnozas;
        }

        private void cbUnesi_Click(object sender, RoutedEventArgs e)
        {
            noviRecept = new Recept
            {
                ReceptID = Int32.Parse(tbSifraRecepta.Text),
                Ime = tbIme.Text,
                Prezime = tbPrezime.Text,
                SifraBolesti = (int)cbDijagnoza.SelectedValue,
                SifraLeka = (int)cbSifraLeka.SelectedValue

            };

            DialogResult = true;
        }

        private void cbDijagnoza_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            cbSifraLeka.ItemsSource = context.Terapijas;
        }

        private void cbOdustani_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
