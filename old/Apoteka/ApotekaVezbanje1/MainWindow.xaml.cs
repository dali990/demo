﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ApotekaVezbanje1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ApotekaDataContext context = new ApotekaDataContext();
        ObservableCollection<Recept> kolekcijaRecepata;
        public MainWindow()
        {
            InitializeComponent();

            kolekcijaRecepata = new ObservableCollection<Recept>(context.Recepts);

            kolekcijaRecepata.CollectionChanged += KolekcijaRecepata_CollectionChanged;

            dataGreed.ItemsSource = kolekcijaRecepata;

            cbDijagnoza.ItemsSource = context.Dijagnozas;

            cbBrojKutija.ItemsSource = Enumerable.Range(1, 4);
        }

        private void KolekcijaRecepata_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach(var item in e.NewItems)
                    {
                        context.Recepts.InsertOnSubmit((Recept)item);
                    }
                    break;
                case NotifyCollectionChangedAction.Remove:
                    foreach (var item in e.OldItems)
                    {
                        context.Recepts.DeleteOnSubmit((Recept)item);
                    }
                    break;
                default:
                    break;
            }
        }

        private void Slajder_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            context.SubmitChanges();
        }

        private void Storniraj_Click(object sender, RoutedEventArgs e)
        {
            var poatak = context.Recepts.Single(r => r.ReceptID == Int32.Parse(tbSifraRecepta.Text));
            kolekcijaRecepata.Remove(poatak);
            context.SubmitChanges();
        }

        private void cbDijagnoza_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            listVju.ItemsSource = null;
            listVju.Items.Clear();

            listVju.ItemsSource = context.Recepts.Where(r => r.SifraBolesti == (int)cbDijagnoza.SelectedValue);
        }

        private void btnMinimalnaDoza_Click(object sender, RoutedEventArgs e)
        {

            tbMinimalnaDoza.Text = "";

            var poatak = context.Terapijas.Where(t => t.SifraBolesti == (int)cbDijagnoza.SelectedValue).OrderByDescending(p => p.Doza).First();

            tbMinimalnaDoza.Text = "Sifra leka: " + poatak.SifraLeka + ", doza: " + poatak.Doza;
        }

        private void cbBrojKutija_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            listVju.ItemsSource = null;
            listVju.Items.Clear();

            listVju.ItemsSource = context.Recepts.Where(r => r.BrKutija == (int)cbBrojKutija.SelectedValue);
        }

        private void tbNaplati_Click(object sender, RoutedEventArgs e)
        {
            tbZaNaplatu.Text = ((int)Slajder.Value * Int32.Parse(tbJedinicnaCena.Text)).ToString();
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            UnosNovogLeka novi = new UnosNovogLeka();

            if(novi.ShowDialog().Value)
            {
                kolekcijaRecepata.Add(novi.noviRecept);

            }

            context.SubmitChanges();
        }
    }
}