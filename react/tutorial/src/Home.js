const Home = () => {
    const handleClick = () => {
        console.log(12);
    };
    const handleWithParamClick = (p) => {
        console.log(12 + p);
    };

    return (
        <div className="home">
            <h2>Homepage</h2>
            <button onClick={handleClick}>Click me</button>
            <button onClick={() => handleWithParamClick("aaa")}>
                Click me param
            </button>
        </div>
    );
};

export default Home;
