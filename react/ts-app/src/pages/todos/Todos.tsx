import React, { useState, useRef } from "react";
import { TTodo } from "../../types";
import Todo from "./Todo";
import useFetch from "../../hooks/useFetch"
import "./Todos.css";

export default function Todos() {
    const inputRef = useRef<HTMLInputElement>(null);
    const [newTodo, setNewTodo] = useState<string | undefined>(undefined);
    const [tasks, setTasks] = useState<TTodo[]>([
        { id: 0, text: "Visit Kafka Museum", done: true },
        { id: 1, text: "Watch a puppet show", done: false },
        { id: 2, text: "Lennon Wall pic", done: false },
    ]);
    const fetchApi = useFetch('https://jsonplaceholder.typicode.com/todos/1');

    const addTodoClickHandler = () => {
        if (inputRef.current) {
            setTasks((oldTasks) => [
                ...oldTasks,
                {
                    id: Math.floor(Math.random() * 9999),
                    text: inputRef.current!.value,
                    done: false,
                },
            ]);
            setNewTodo("");
        }
    };

    const onChangeStatus = (id: number) => {
        setTasks((prevTodos) =>
            prevTodos.map((todo) =>
                todo.id === id ? {...todo, done: !todo.done} : todo
            )
        );
    };

    const handleDelteTodo = (id: number) => {
        setTasks((prevTodos) => prevTodos.filter(todo => todo.id !== id) );
    }

    const handleFetchApi = () => {

    }

    return (
        <div className="todos">
            <div>
                <button onClick={handleFetchApi}>Fetch</button>
            </div>
            <br/>
            <div>
                <input
                    value={newTodo}
                    ref={inputRef}
                    onChange={(e) => setNewTodo(e.target.value)}
                />
                <button onClick={addTodoClickHandler}>Add todo</button>
            </div>
            {tasks.map((todo) => (
                <div className="todo" key={todo.id}>
                    <Todo todo={todo} onChangeStatus={onChangeStatus} handleDelteTodo={handleDelteTodo}/>
                </div>
            ))}
        </div>
    );
}
