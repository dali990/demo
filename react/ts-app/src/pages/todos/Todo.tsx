import React, { useState } from "react";
import './Todos.css'
import { TTodo } from '../../types'

type TodoProps = {
    todo: TTodo
    onChangeStatus: (id: number) => void;
    handleDelteTodo: (id: number) => void;
    
};

const Todo: React.FC<TodoProps> = ({todo, onChangeStatus, handleDelteTodo}) => {
    return (
        <div className="todo">
            <input type="checkbox" checked={todo.done} onChange={() => {onChangeStatus(todo.id)}}/>
            {todo.text} <button onClick={()=>{handleDelteTodo(todo.id)}}>Delte</button>
        </div>
    )
}

export default Todo;
