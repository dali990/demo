export type TTodo = {
    text: string;
    id: number;
    done: boolean;
}