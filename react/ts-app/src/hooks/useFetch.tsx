import { useState, useEffect } from "react";

export const useFetch = (url: string) => {
    const [data, setData] = useState<Object | null>(null);
    const [isPending, setIsPending] = useState<Boolean>(false);
    useEffect(() => {
        const fetchData = async () => {
            setIsPending(true);
            const response = await fetch(url);
            const json = await response.json();
            setIsPending(false);
            setData(json);
        };
        fetchData();
    }, [url]);
    return { data, isPending };
};
