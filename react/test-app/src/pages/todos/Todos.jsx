import { useState } from "react";
import Todo from "./Todo";
import './Todos.css'

export default function Todos() {
    const [tasks, setTasks] = useState([
        { id: 0, text: "Visit Kafka Museum", done: true },
        { id: 1, text: "Watch a puppet show", done: false },
        { id: 2, text: "Lennon Wall pic", done: false },
    ]);

    return (
        <div className="todos">
            {tasks.map((todo) => (
                <div className="todo" key={todo.id}>
                {todo.text}
            </div>
            ))}
        </div>
    )
}
