--
-- PostgreSQL database dump
--

-- Dumped from database version 11.6 (Ubuntu 11.6-1.pgdg18.04+1)
-- Dumped by pg_dump version 11.6 (Ubuntu 11.6-1.pgdg18.04+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: private; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA private;


ALTER SCHEMA private OWNER TO postgres;

--
-- Name: min_dates(date, date); Type: FUNCTION; Schema: private; Owner: postgres
--

CREATE FUNCTION private.min_dates(date1 date, date2 date) RETURNS date
    LANGUAGE plpgsql
    AS $$
DECLARE
	result date;
BEGIN
	IF date1 IS NULL
	THEN
		return date2;
	END IF;

	IF date2 IS NULL
	THEN
		RETURN date1;
	END IF;

	IF date1 < date2
	THEN
		return date1;
	END IF;

	RETURN date2;
END;
$$;


ALTER FUNCTION private.min_dates(date1 date, date2 date) OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: account_layouts; Type: TABLE; Schema: private; Owner: postgres
--

CREATE TABLE private.account_layouts (
    id integer NOT NULL,
    code character varying(6) NOT NULL,
    name text NOT NULL,
    parent_id integer,
    law_id integer NOT NULL
);


ALTER TABLE private.account_layouts OWNER TO postgres;

--
-- Name: account_layouts_id_seq; Type: SEQUENCE; Schema: private; Owner: postgres
--

CREATE SEQUENCE private.account_layouts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE private.account_layouts_id_seq OWNER TO postgres;

--
-- Name: account_layouts_id_seq; Type: SEQUENCE OWNED BY; Schema: private; Owner: postgres
--

ALTER SEQUENCE private.account_layouts_id_seq OWNED BY private.account_layouts.id;


--
-- Name: account_layouts_laws_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.account_layouts_laws_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.account_layouts_laws_id_seq OWNER TO postgres;

--
-- Name: account_layouts_laws; Type: TABLE; Schema: private; Owner: postgres
--

CREATE TABLE private.account_layouts_laws (
    id integer DEFAULT nextval('public.account_layouts_laws_id_seq'::regclass) NOT NULL,
    name text,
    date date NOT NULL
);


ALTER TABLE private.account_layouts_laws OWNER TO postgres;

--
-- Name: addresses; Type: TABLE; Schema: private; Owner: postgres
--

CREATE TABLE private.addresses (
    id integer NOT NULL,
    street_number text NOT NULL,
    street_id integer NOT NULL,
    door_number text,
    description text
);


ALTER TABLE private.addresses OWNER TO postgres;

--
-- Name: addresses_id_seq; Type: SEQUENCE; Schema: private; Owner: postgres
--

CREATE SEQUENCE private.addresses_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE private.addresses_id_seq OWNER TO postgres;

--
-- Name: addresses_id_seq; Type: SEQUENCE OWNED BY; Schema: private; Owner: postgres
--

ALTER SEQUENCE private.addresses_id_seq OWNED BY private.addresses.id;


--
-- Name: banks; Type: TABLE; Schema: private; Owner: postgres
--

CREATE TABLE private.banks (
    id integer NOT NULL,
    name text NOT NULL,
    "PIB" text NOT NULL,
    "inVat" boolean DEFAULT true NOT NULL,
    "MB" text NOT NULL,
    bank_account_code_old numeric(3,0),
    short_name text,
    bank_account_code character(3)
);


ALTER TABLE private.banks OWNER TO postgres;

--
-- Name: banks_id_seq; Type: SEQUENCE; Schema: private; Owner: postgres
--

CREATE SEQUENCE private.banks_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE private.banks_id_seq OWNER TO postgres;

--
-- Name: banks_id_seq; Type: SEQUENCE OWNED BY; Schema: private; Owner: postgres
--

ALTER SEQUENCE private.banks_id_seq OWNED BY private.banks.id;


--
-- Name: cities; Type: TABLE; Schema: private; Owner: postgres
--

CREATE TABLE private.cities (
    id integer NOT NULL,
    name text NOT NULL,
    country_id integer NOT NULL,
    default_postal_code_id integer,
    area_code text,
    bzr_code text,
    treasury_office_unit_id integer
);


ALTER TABLE private.cities OWNER TO postgres;

--
-- Name: cities_id_seq; Type: SEQUENCE; Schema: private; Owner: postgres
--

CREATE SEQUENCE private.cities_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE private.cities_id_seq OWNER TO postgres;

--
-- Name: cities_id_seq; Type: SEQUENCE OWNED BY; Schema: private; Owner: postgres
--

ALTER SEQUENCE private.cities_id_seq OWNED BY private.cities.id;


--
-- Name: cities_to_postal_codes; Type: TABLE; Schema: private; Owner: postgres
--

CREATE TABLE private.cities_to_postal_codes (
    id integer NOT NULL,
    city_id integer NOT NULL,
    postal_code_id integer NOT NULL
);


ALTER TABLE private.cities_to_postal_codes OWNER TO postgres;

--
-- Name: cities_to_postal_codes_id_seq; Type: SEQUENCE; Schema: private; Owner: postgres
--

CREATE SEQUENCE private.cities_to_postal_codes_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE private.cities_to_postal_codes_id_seq OWNER TO postgres;

--
-- Name: cities_to_postal_codes_id_seq; Type: SEQUENCE OWNED BY; Schema: private; Owner: postgres
--

ALTER SEQUENCE private.cities_to_postal_codes_id_seq OWNED BY private.cities_to_postal_codes.id;


--
-- Name: countries; Type: TABLE; Schema: private; Owner: postgres
--

CREATE TABLE private.countries (
    id integer NOT NULL,
    name text NOT NULL,
    domain_name character varying(100),
    area_code text,
    alfa_code character varying(2),
    numeric_code character varying(3) NOT NULL,
    designation character varying(3)
);


ALTER TABLE private.countries OWNER TO postgres;

--
-- Name: countries_id_seq; Type: SEQUENCE; Schema: private; Owner: postgres
--

CREATE SEQUENCE private.countries_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE private.countries_id_seq OWNER TO postgres;

--
-- Name: countries_id_seq; Type: SEQUENCE OWNED BY; Schema: private; Owner: postgres
--

ALTER SEQUENCE private.countries_id_seq OWNED BY private.countries.id;


--
-- Name: currencies; Type: TABLE; Schema: private; Owner: postgres
--

CREATE TABLE private.currencies (
    id integer NOT NULL,
    name text NOT NULL,
    short_name character varying(5) NOT NULL,
    code character(3) NOT NULL
);


ALTER TABLE private.currencies OWNER TO postgres;

--
-- Name: currencies_id_seq; Type: SEQUENCE; Schema: private; Owner: postgres
--

CREATE SEQUENCE private.currencies_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE private.currencies_id_seq OWNER TO postgres;

--
-- Name: currencies_id_seq; Type: SEQUENCE OWNED BY; Schema: private; Owner: postgres
--

ALTER SEQUENCE private.currencies_id_seq OWNED BY private.currencies.id;


--
-- Name: driver_license_categories; Type: TABLE; Schema: private; Owner: postgres
--

CREATE TABLE private.driver_license_categories (
    id integer NOT NULL,
    category text NOT NULL,
    description text NOT NULL,
    age_range text NOT NULL
);


ALTER TABLE private.driver_license_categories OWNER TO postgres;

--
-- Name: driver_license_categories_id_seq; Type: SEQUENCE; Schema: private; Owner: postgres
--

CREATE SEQUENCE private.driver_license_categories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE private.driver_license_categories_id_seq OWNER TO postgres;

--
-- Name: driver_license_categories_id_seq; Type: SEQUENCE OWNED BY; Schema: private; Owner: postgres
--

ALTER SEQUENCE private.driver_license_categories_id_seq OWNED BY private.driver_license_categories.id;


--
-- Name: fixed_asset_types; Type: TABLE; Schema: private; Owner: postgres
--

CREATE TABLE private.fixed_asset_types (
    id integer NOT NULL,
    name character varying(100) NOT NULL,
    description text,
    depreciation_rate numeric(5,2) DEFAULT 0 NOT NULL,
    fixed_asset_amortization_group_id integer NOT NULL
);


ALTER TABLE private.fixed_asset_types OWNER TO postgres;

--
-- Name: fixed_asset_types_id_seq; Type: SEQUENCE; Schema: private; Owner: postgres
--

CREATE SEQUENCE private.fixed_asset_types_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE private.fixed_asset_types_id_seq OWNER TO postgres;

--
-- Name: fixed_asset_types_id_seq; Type: SEQUENCE OWNED BY; Schema: private; Owner: postgres
--

ALTER SEQUENCE private.fixed_asset_types_id_seq OWNED BY private.fixed_asset_types.id;


--
-- Name: fixed_assets_amortization_groups; Type: TABLE; Schema: private; Owner: postgres
--

CREATE TABLE private.fixed_assets_amortization_groups (
    id integer NOT NULL,
    group_name text NOT NULL,
    depreciation_rate numeric(5,2) NOT NULL
);


ALTER TABLE private.fixed_assets_amortization_groups OWNER TO postgres;

--
-- Name: fixed_assets_amortization_groups_id_seq; Type: SEQUENCE; Schema: private; Owner: postgres
--

CREATE SEQUENCE private.fixed_assets_amortization_groups_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE private.fixed_assets_amortization_groups_id_seq OWNER TO postgres;

--
-- Name: fixed_assets_amortization_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: private; Owner: postgres
--

ALTER SEQUENCE private.fixed_assets_amortization_groups_id_seq OWNED BY private.fixed_assets_amortization_groups.id;


--
-- Name: fuels; Type: TABLE; Schema: private; Owner: postgres
--

CREATE TABLE private.fuels (
    name text NOT NULL,
    category text,
    id integer NOT NULL
);


ALTER TABLE private.fuels OWNER TO postgres;

--
-- Name: fuels_id_seq; Type: SEQUENCE; Schema: private; Owner: postgres
--

CREATE SEQUENCE private.fuels_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE private.fuels_id_seq OWNER TO postgres;

--
-- Name: fuels_id_seq; Type: SEQUENCE OWNED BY; Schema: private; Owner: postgres
--

ALTER SEQUENCE private.fuels_id_seq OWNED BY private.fuels.id;


--
-- Name: international_diagnosis_of_professional_diseases; Type: TABLE; Schema: private; Owner: postgres
--

CREATE TABLE private.international_diagnosis_of_professional_diseases (
    id integer NOT NULL,
    code text NOT NULL,
    name text NOT NULL,
    description text
);


ALTER TABLE private.international_diagnosis_of_professional_diseases OWNER TO postgres;

--
-- Name: international_diagnosis_of_professional_diseases_id_seq; Type: SEQUENCE; Schema: private; Owner: postgres
--

CREATE SEQUENCE private.international_diagnosis_of_professional_diseases_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE private.international_diagnosis_of_professional_diseases_id_seq OWNER TO postgres;

--
-- Name: international_diagnosis_of_professional_diseases_id_seq; Type: SEQUENCE OWNED BY; Schema: private; Owner: postgres
--

ALTER SEQUENCE private.international_diagnosis_of_professional_diseases_id_seq OWNED BY private.international_diagnosis_of_professional_diseases.id;


--
-- Name: international_diagnosis_of_work_diseases; Type: TABLE; Schema: private; Owner: postgres
--

CREATE TABLE private.international_diagnosis_of_work_diseases (
    id integer NOT NULL,
    code text NOT NULL,
    name text NOT NULL,
    description text
);


ALTER TABLE private.international_diagnosis_of_work_diseases OWNER TO postgres;

--
-- Name: international_diagnosis_of_work_diseases_id_seq; Type: SEQUENCE; Schema: private; Owner: postgres
--

CREATE SEQUENCE private.international_diagnosis_of_work_diseases_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE private.international_diagnosis_of_work_diseases_id_seq OWNER TO postgres;

--
-- Name: international_diagnosis_of_work_diseases_id_seq; Type: SEQUENCE OWNED BY; Schema: private; Owner: postgres
--

ALTER SEQUENCE private.international_diagnosis_of_work_diseases_id_seq OWNED BY private.international_diagnosis_of_work_diseases.id;


--
-- Name: measurement_units; Type: TABLE; Schema: private; Owner: postgres
--

CREATE TABLE private.measurement_units (
    id integer NOT NULL,
    name text NOT NULL,
    short_name character varying(100)
);


ALTER TABLE private.measurement_units OWNER TO postgres;

--
-- Name: measurement_units_id_seq; Type: SEQUENCE; Schema: private; Owner: postgres
--

CREATE SEQUENCE private.measurement_units_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE private.measurement_units_id_seq OWNER TO postgres;

--
-- Name: measurement_units_id_seq; Type: SEQUENCE OWNED BY; Schema: private; Owner: postgres
--

ALTER SEQUENCE private.measurement_units_id_seq OWNED BY private.measurement_units.id;


--
-- Name: mode_injuries; Type: TABLE; Schema: private; Owner: postgres
--

CREATE TABLE private.mode_injuries (
    id integer NOT NULL,
    code text NOT NULL,
    name text NOT NULL,
    description text,
    parent_id integer
);


ALTER TABLE private.mode_injuries OWNER TO postgres;

--
-- Name: mode_injuries_id_seq; Type: SEQUENCE; Schema: private; Owner: postgres
--

CREATE SEQUENCE private.mode_injuries_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE private.mode_injuries_id_seq OWNER TO postgres;

--
-- Name: mode_injuries_id_seq; Type: SEQUENCE OWNED BY; Schema: private; Owner: postgres
--

ALTER SEQUENCE private.mode_injuries_id_seq OWNED BY private.mode_injuries.id;


--
-- Name: occupations; Type: TABLE; Schema: private; Owner: postgres
--

CREATE TABLE private.occupations (
    id integer NOT NULL,
    name text NOT NULL,
    code character varying(16) NOT NULL,
    parent_id integer
);


ALTER TABLE private.occupations OWNER TO postgres;

--
-- Name: occupations_id_seq; Type: SEQUENCE; Schema: private; Owner: postgres
--

CREATE SEQUENCE private.occupations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE private.occupations_id_seq OWNER TO postgres;

--
-- Name: occupations_id_seq; Type: SEQUENCE OWNED BY; Schema: private; Owner: postgres
--

ALTER SEQUENCE private.occupations_id_seq OWNED BY private.occupations.id;


--
-- Name: paychecks_codebook; Type: TABLE; Schema: private; Owner: postgres
--

CREATE TABLE private.paychecks_codebook (
    id integer NOT NULL,
    start_use date NOT NULL,
    end_use date,
    pio_empl numeric(5,2),
    zdr_empl numeric(5,2),
    nez_empl numeric(5,2),
    tax_empl numeric(5,2),
    tax_release numeric(8,2),
    pio_comp numeric(5,2),
    zdr_comp numeric(5,2),
    nez_comp numeric(5,2),
    min_contribution_base numeric(10,2),
    max_contribution_base numeric(10,2)
);


ALTER TABLE private.paychecks_codebook OWNER TO postgres;

--
-- Name: paychecks_codebook_id_seq; Type: SEQUENCE; Schema: private; Owner: postgres
--

CREATE SEQUENCE private.paychecks_codebook_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE private.paychecks_codebook_id_seq OWNER TO postgres;

--
-- Name: paychecks_codebook_id_seq; Type: SEQUENCE OWNED BY; Schema: private; Owner: postgres
--

ALTER SEQUENCE private.paychecks_codebook_id_seq OWNED BY private.paychecks_codebook.id;


--
-- Name: payment_codes; Type: TABLE; Schema: private; Owner: postgres
--

CREATE TABLE private.payment_codes (
    id integer NOT NULL,
    name text NOT NULL,
    code text NOT NULL,
    description text
);


ALTER TABLE private.payment_codes OWNER TO postgres;

--
-- Name: payment_codes_id_seq; Type: SEQUENCE; Schema: private; Owner: postgres
--

CREATE SEQUENCE private.payment_codes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE private.payment_codes_id_seq OWNER TO postgres;

--
-- Name: payment_codes_id_seq; Type: SEQUENCE OWNED BY; Schema: private; Owner: postgres
--

ALTER SEQUENCE private.payment_codes_id_seq OWNED BY private.payment_codes.id;


--
-- Name: payment_configurations; Type: TABLE; Schema: private; Owner: postgres
--

CREATE TABLE private.payment_configurations (
    id integer NOT NULL,
    name text NOT NULL,
    type integer NOT NULL,
    separator text,
    account_number_col text NOT NULL,
    date_col text NOT NULL,
    amount_col text NOT NULL,
    description text,
    amount2_col text NOT NULL,
    path text,
    decimal_separator text NOT NULL,
    account_owner_col text NOT NULL,
    encoding text DEFAULT 'UTF-8'::text NOT NULL,
    payment_code_col text,
    call_for_number_debit_col text,
    call_for_number_credit_col text,
    bank_account_date text,
    bank_account_amount text,
    bank_account_id text,
    bank_statement_number text,
    export_format text NOT NULL,
    bank_transaction_id text NOT NULL
);


ALTER TABLE private.payment_configurations OWNER TO postgres;

--
-- Name: payment_configurations_id_seq; Type: SEQUENCE; Schema: private; Owner: postgres
--

CREATE SEQUENCE private.payment_configurations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE private.payment_configurations_id_seq OWNER TO postgres;

--
-- Name: payment_configurations_id_seq; Type: SEQUENCE OWNED BY; Schema: private; Owner: postgres
--

ALTER SEQUENCE private.payment_configurations_id_seq OWNED BY private.payment_configurations.id;


--
-- Name: postal_codes; Type: TABLE; Schema: private; Owner: postgres
--

CREATE TABLE private.postal_codes (
    id integer NOT NULL,
    code text NOT NULL,
    country_id integer NOT NULL
);


ALTER TABLE private.postal_codes OWNER TO postgres;

--
-- Name: postal_codes_id_seq; Type: SEQUENCE; Schema: private; Owner: postgres
--

CREATE SEQUENCE private.postal_codes_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE private.postal_codes_id_seq OWNER TO postgres;

--
-- Name: postal_codes_id_seq; Type: SEQUENCE OWNED BY; Schema: private; Owner: postgres
--

ALTER SEQUENCE private.postal_codes_id_seq OWNED BY private.postal_codes.id;


--
-- Name: professional_degrees; Type: TABLE; Schema: private; Owner: postgres
--

CREATE TABLE private.professional_degrees (
    name text NOT NULL,
    id integer NOT NULL,
    description text
);


ALTER TABLE private.professional_degrees OWNER TO postgres;

--
-- Name: professional_degrees_id_seq; Type: SEQUENCE; Schema: private; Owner: postgres
--

CREATE SEQUENCE private.professional_degrees_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE private.professional_degrees_id_seq OWNER TO postgres;

--
-- Name: professional_degrees_id_seq; Type: SEQUENCE OWNED BY; Schema: private; Owner: postgres
--

ALTER SEQUENCE private.professional_degrees_id_seq OWNED BY private.professional_degrees.id;


--
-- Name: qualification_levels; Type: TABLE; Schema: private; Owner: postgres
--

CREATE TABLE private.qualification_levels (
    id integer NOT NULL,
    name text NOT NULL,
    code integer NOT NULL,
    description text
);


ALTER TABLE private.qualification_levels OWNER TO postgres;

--
-- Name: qualification_levels_id_seq; Type: SEQUENCE; Schema: private; Owner: postgres
--

CREATE SEQUENCE private.qualification_levels_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE private.qualification_levels_id_seq OWNER TO postgres;

--
-- Name: qualification_levels_id_seq; Type: SEQUENCE OWNED BY; Schema: private; Owner: postgres
--

ALTER SEQUENCE private.qualification_levels_id_seq OWNED BY private.qualification_levels.id;


--
-- Name: reasons_for_enforcement_training; Type: TABLE; Schema: private; Owner: postgres
--

CREATE TABLE private.reasons_for_enforcement_training (
    id integer NOT NULL,
    code text NOT NULL,
    name text NOT NULL,
    description text
);


ALTER TABLE private.reasons_for_enforcement_training OWNER TO postgres;

--
-- Name: reasons_for_enforcement_training_id_seq; Type: SEQUENCE; Schema: private; Owner: postgres
--

CREATE SEQUENCE private.reasons_for_enforcement_training_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE private.reasons_for_enforcement_training_id_seq OWNER TO postgres;

--
-- Name: reasons_for_enforcement_training_id_seq; Type: SEQUENCE OWNED BY; Schema: private; Owner: postgres
--

ALTER SEQUENCE private.reasons_for_enforcement_training_id_seq OWNED BY private.reasons_for_enforcement_training.id;


--
-- Name: work_codes; Type: TABLE; Schema: private; Owner: postgres
--

CREATE TABLE private.work_codes (
    id integer NOT NULL,
    code text NOT NULL,
    name text NOT NULL,
    parent_id integer
);


ALTER TABLE private.work_codes OWNER TO postgres;

--
-- Name: recursive_work_codes; Type: VIEW; Schema: private; Owner: postgres
--

CREATE VIEW private.recursive_work_codes AS
 SELECT for_parent.id,
    for_parent.treename AS display_name_path,
    for_parent.pathname AS display_name_full,
    for_parent.pathcode AS display_code_full,
    for_parent.parent_ids,
    for_parent.master_parent_id,
    for_children.children_ids
   FROM (( WITH RECURSIVE reccur(id, name, code, parent_id, master_parent_id, pathname, pathcode, treename, parent_ids) AS (
                 SELECT work_codes.id,
                    work_codes.name,
                    work_codes.code,
                    work_codes.parent_id,
                    work_codes.id,
                    work_codes.name,
                    work_codes.code,
                    ''::text AS text,
                    ARRAY[]::integer[] AS "array"
                   FROM private.work_codes work_codes
                  WHERE (work_codes.parent_id IS NULL)
                UNION ALL
                 SELECT work_codes.id,
                    work_codes.name,
                    work_codes.code,
                    work_codes.parent_id,
                    reccur.master_parent_id,
                    ((reccur.pathname || '_'::text) || work_codes.name),
                    ((reccur.pathcode || '_'::text) || work_codes.code),
                    (reccur.treename || '|---'::text),
                    (reccur.parent_ids || work_codes.parent_id)
                   FROM private.work_codes work_codes,
                    reccur
                  WHERE (reccur.id = work_codes.parent_id)
                )
         SELECT r.id,
            (r.treename || r.name) AS treename,
            r.master_parent_id,
            j.name,
            j.code,
            r.pathname,
            r.pathcode,
            r.parent_ids
           FROM (reccur r
             LEFT JOIN private.work_codes j ON ((r.id = j.id)))
          ORDER BY r.pathcode) for_parent
     LEFT JOIN ( SELECT work_codes.id,
            array_remove(array_agg(vv.id), NULL::integer) AS children_ids
           FROM (private.work_codes work_codes
             LEFT JOIN ( WITH RECURSIVE reccur(id, parent_id, parent_ids) AS (
                         SELECT work_codes_1.id,
                            work_codes_1.parent_id,
                            ARRAY[]::integer[] AS "array"
                           FROM private.work_codes work_codes_1
                          WHERE (work_codes_1.parent_id IS NULL)
                        UNION ALL
                         SELECT work_codes_1.id,
                            work_codes_1.parent_id,
                            (reccur.parent_ids || work_codes_1.parent_id)
                           FROM private.work_codes work_codes_1,
                            reccur
                          WHERE (reccur.id = work_codes_1.parent_id)
                        )
                 SELECT r.id,
                    r.parent_ids
                   FROM reccur r) vv ON ((work_codes.id = ANY (vv.parent_ids))))
          GROUP BY work_codes.id) for_children ON ((for_parent.id = for_children.id)))
  ORDER BY for_parent.pathcode;


ALTER TABLE private.recursive_work_codes OWNER TO postgres;

--
-- Name: required_medical_abilities; Type: TABLE; Schema: private; Owner: postgres
--

CREATE TABLE private.required_medical_abilities (
    id integer NOT NULL,
    name text NOT NULL,
    description text
);


ALTER TABLE private.required_medical_abilities OWNER TO postgres;

--
-- Name: required_medical_abilities_id_seq; Type: SEQUENCE; Schema: private; Owner: postgres
--

CREATE SEQUENCE private.required_medical_abilities_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE private.required_medical_abilities_id_seq OWNER TO postgres;

--
-- Name: required_medical_abilities_id_seq; Type: SEQUENCE OWNED BY; Schema: private; Owner: postgres
--

ALTER SEQUENCE private.required_medical_abilities_id_seq OWNED BY private.required_medical_abilities.id;


--
-- Name: safe_file_extensions; Type: TABLE; Schema: private; Owner: postgres
--

CREATE TABLE private.safe_file_extensions (
    id integer NOT NULL,
    name text NOT NULL,
    description text
);


ALTER TABLE private.safe_file_extensions OWNER TO postgres;

--
-- Name: safe_file_extensions_id_seq; Type: SEQUENCE; Schema: private; Owner: postgres
--

CREATE SEQUENCE private.safe_file_extensions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE private.safe_file_extensions_id_seq OWNER TO postgres;

--
-- Name: safe_file_extensions_id_seq; Type: SEQUENCE OWNED BY; Schema: private; Owner: postgres
--

ALTER SEQUENCE private.safe_file_extensions_id_seq OWNED BY private.safe_file_extensions.id;


--
-- Name: safe_work_dangers; Type: TABLE; Schema: private; Owner: postgres
--

CREATE TABLE private.safe_work_dangers (
    id integer NOT NULL,
    code text NOT NULL,
    description text NOT NULL
);


ALTER TABLE private.safe_work_dangers OWNER TO postgres;

--
-- Name: safe_work_dangers_id_seq; Type: SEQUENCE; Schema: private; Owner: postgres
--

CREATE SEQUENCE private.safe_work_dangers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE private.safe_work_dangers_id_seq OWNER TO postgres;

--
-- Name: safe_work_dangers_id_seq; Type: SEQUENCE OWNED BY; Schema: private; Owner: postgres
--

ALTER SEQUENCE private.safe_work_dangers_id_seq OWNED BY private.safe_work_dangers.id;


--
-- Name: source_injuries; Type: TABLE; Schema: private; Owner: postgres
--

CREATE TABLE private.source_injuries (
    id integer NOT NULL,
    code text NOT NULL,
    name text NOT NULL,
    description text,
    parent_id integer
);


ALTER TABLE private.source_injuries OWNER TO postgres;

--
-- Name: source_injuries_id_seq; Type: SEQUENCE; Schema: private; Owner: postgres
--

CREATE SEQUENCE private.source_injuries_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE private.source_injuries_id_seq OWNER TO postgres;

--
-- Name: source_injuries_id_seq; Type: SEQUENCE OWNED BY; Schema: private; Owner: postgres
--

ALTER SEQUENCE private.source_injuries_id_seq OWNED BY private.source_injuries.id;


--
-- Name: streets; Type: TABLE; Schema: private; Owner: postgres
--

CREATE TABLE private.streets (
    id integer NOT NULL,
    name text NOT NULL,
    city_id integer NOT NULL,
    postal_code_id integer NOT NULL
);


ALTER TABLE private.streets OWNER TO postgres;

--
-- Name: streets_id_seq; Type: SEQUENCE; Schema: private; Owner: postgres
--

CREATE SEQUENCE private.streets_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE private.streets_id_seq OWNER TO postgres;

--
-- Name: streets_id_seq; Type: SEQUENCE OWNED BY; Schema: private; Owner: postgres
--

ALTER SEQUENCE private.streets_id_seq OWNED BY private.streets.id;


--
-- Name: treasury_office_units; Type: TABLE; Schema: private; Owner: postgres
--

CREATE TABLE private.treasury_office_units (
    id integer NOT NULL,
    name text NOT NULL,
    type text NOT NULL,
    code text NOT NULL,
    control_number text NOT NULL,
    main_branch_id integer NOT NULL,
    sub_branch_id integer,
    bzr_code text,
    country_id integer NOT NULL
);


ALTER TABLE private.treasury_office_units OWNER TO postgres;

--
-- Name: treasury_office_units_id_seq; Type: SEQUENCE; Schema: private; Owner: postgres
--

CREATE SEQUENCE private.treasury_office_units_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE private.treasury_office_units_id_seq OWNER TO postgres;

--
-- Name: treasury_office_units_id_seq; Type: SEQUENCE OWNED BY; Schema: private; Owner: postgres
--

ALTER SEQUENCE private.treasury_office_units_id_seq OWNED BY private.treasury_office_units.id;


--
-- Name: treasury_offices; Type: TABLE; Schema: private; Owner: postgres
--

CREATE TABLE private.treasury_offices (
    id integer NOT NULL,
    name text NOT NULL,
    parent_id integer
);


ALTER TABLE private.treasury_offices OWNER TO postgres;

--
-- Name: treasury_offices_id_seq; Type: SEQUENCE; Schema: private; Owner: postgres
--

CREATE SEQUENCE private.treasury_offices_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE private.treasury_offices_id_seq OWNER TO postgres;

--
-- Name: treasury_offices_id_seq; Type: SEQUENCE OWNED BY; Schema: private; Owner: postgres
--

ALTER SEQUENCE private.treasury_offices_id_seq OWNED BY private.treasury_offices.id;


--
-- Name: work_codes_id_seq; Type: SEQUENCE; Schema: private; Owner: postgres
--

CREATE SEQUENCE private.work_codes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE private.work_codes_id_seq OWNER TO postgres;

--
-- Name: work_codes_id_seq; Type: SEQUENCE OWNED BY; Schema: private; Owner: postgres
--

ALTER SEQUENCE private.work_codes_id_seq OWNED BY private.work_codes.id;


--
-- Name: tbl_migration; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tbl_migration (
    version character varying(255) NOT NULL,
    apply_time integer,
    module character varying(32)
);


ALTER TABLE public.tbl_migration OWNER TO postgres;

--
-- Name: account_layouts id; Type: DEFAULT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.account_layouts ALTER COLUMN id SET DEFAULT nextval('private.account_layouts_id_seq'::regclass);


--
-- Name: addresses id; Type: DEFAULT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.addresses ALTER COLUMN id SET DEFAULT nextval('private.addresses_id_seq'::regclass);


--
-- Name: banks id; Type: DEFAULT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.banks ALTER COLUMN id SET DEFAULT nextval('private.banks_id_seq'::regclass);


--
-- Name: cities id; Type: DEFAULT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.cities ALTER COLUMN id SET DEFAULT nextval('private.cities_id_seq'::regclass);


--
-- Name: cities_to_postal_codes id; Type: DEFAULT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.cities_to_postal_codes ALTER COLUMN id SET DEFAULT nextval('private.cities_to_postal_codes_id_seq'::regclass);


--
-- Name: countries id; Type: DEFAULT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.countries ALTER COLUMN id SET DEFAULT nextval('private.countries_id_seq'::regclass);


--
-- Name: currencies id; Type: DEFAULT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.currencies ALTER COLUMN id SET DEFAULT nextval('private.currencies_id_seq'::regclass);


--
-- Name: driver_license_categories id; Type: DEFAULT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.driver_license_categories ALTER COLUMN id SET DEFAULT nextval('private.driver_license_categories_id_seq'::regclass);


--
-- Name: fixed_asset_types id; Type: DEFAULT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.fixed_asset_types ALTER COLUMN id SET DEFAULT nextval('private.fixed_asset_types_id_seq'::regclass);


--
-- Name: fixed_assets_amortization_groups id; Type: DEFAULT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.fixed_assets_amortization_groups ALTER COLUMN id SET DEFAULT nextval('private.fixed_assets_amortization_groups_id_seq'::regclass);


--
-- Name: fuels id; Type: DEFAULT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.fuels ALTER COLUMN id SET DEFAULT nextval('private.fuels_id_seq'::regclass);


--
-- Name: international_diagnosis_of_professional_diseases id; Type: DEFAULT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.international_diagnosis_of_professional_diseases ALTER COLUMN id SET DEFAULT nextval('private.international_diagnosis_of_professional_diseases_id_seq'::regclass);


--
-- Name: international_diagnosis_of_work_diseases id; Type: DEFAULT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.international_diagnosis_of_work_diseases ALTER COLUMN id SET DEFAULT nextval('private.international_diagnosis_of_work_diseases_id_seq'::regclass);


--
-- Name: measurement_units id; Type: DEFAULT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.measurement_units ALTER COLUMN id SET DEFAULT nextval('private.measurement_units_id_seq'::regclass);


--
-- Name: mode_injuries id; Type: DEFAULT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.mode_injuries ALTER COLUMN id SET DEFAULT nextval('private.mode_injuries_id_seq'::regclass);


--
-- Name: occupations id; Type: DEFAULT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.occupations ALTER COLUMN id SET DEFAULT nextval('private.occupations_id_seq'::regclass);


--
-- Name: paychecks_codebook id; Type: DEFAULT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.paychecks_codebook ALTER COLUMN id SET DEFAULT nextval('private.paychecks_codebook_id_seq'::regclass);


--
-- Name: payment_codes id; Type: DEFAULT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.payment_codes ALTER COLUMN id SET DEFAULT nextval('private.payment_codes_id_seq'::regclass);


--
-- Name: payment_configurations id; Type: DEFAULT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.payment_configurations ALTER COLUMN id SET DEFAULT nextval('private.payment_configurations_id_seq'::regclass);


--
-- Name: postal_codes id; Type: DEFAULT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.postal_codes ALTER COLUMN id SET DEFAULT nextval('private.postal_codes_id_seq'::regclass);


--
-- Name: professional_degrees id; Type: DEFAULT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.professional_degrees ALTER COLUMN id SET DEFAULT nextval('private.professional_degrees_id_seq'::regclass);


--
-- Name: qualification_levels id; Type: DEFAULT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.qualification_levels ALTER COLUMN id SET DEFAULT nextval('private.qualification_levels_id_seq'::regclass);


--
-- Name: reasons_for_enforcement_training id; Type: DEFAULT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.reasons_for_enforcement_training ALTER COLUMN id SET DEFAULT nextval('private.reasons_for_enforcement_training_id_seq'::regclass);


--
-- Name: required_medical_abilities id; Type: DEFAULT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.required_medical_abilities ALTER COLUMN id SET DEFAULT nextval('private.required_medical_abilities_id_seq'::regclass);


--
-- Name: safe_file_extensions id; Type: DEFAULT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.safe_file_extensions ALTER COLUMN id SET DEFAULT nextval('private.safe_file_extensions_id_seq'::regclass);


--
-- Name: safe_work_dangers id; Type: DEFAULT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.safe_work_dangers ALTER COLUMN id SET DEFAULT nextval('private.safe_work_dangers_id_seq'::regclass);


--
-- Name: source_injuries id; Type: DEFAULT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.source_injuries ALTER COLUMN id SET DEFAULT nextval('private.source_injuries_id_seq'::regclass);


--
-- Name: streets id; Type: DEFAULT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.streets ALTER COLUMN id SET DEFAULT nextval('private.streets_id_seq'::regclass);


--
-- Name: treasury_office_units id; Type: DEFAULT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.treasury_office_units ALTER COLUMN id SET DEFAULT nextval('private.treasury_office_units_id_seq'::regclass);


--
-- Name: treasury_offices id; Type: DEFAULT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.treasury_offices ALTER COLUMN id SET DEFAULT nextval('private.treasury_offices_id_seq'::regclass);


--
-- Name: work_codes id; Type: DEFAULT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.work_codes ALTER COLUMN id SET DEFAULT nextval('private.work_codes_id_seq'::regclass);


--
-- Data for Name: account_layouts; Type: TABLE DATA; Schema: private; Owner: postgres
--

COPY private.account_layouts (id, code, name, parent_id, law_id) FROM stdin;
1	0	UPISANI A NEUPLAĆENI  KAPITAL I STALNA IMOVINA	\N	134071
34	1	ZALIHE I STALNA SREDSTVA NAMENJENA PRODAJI	\N	134071
69	2	KRATKOROČNA POTRAŽIVANjA I PLASMANI, NOVČANA SREDSTVA I AKTIVNA VREMENSKA RAZGRANIČENJA	\N	134071
129	3	KAPITAL	\N	134071
154	4	DUGOROČNA REZERVISSANJA, OBAVEZE I PASIVNA VREMENSKA RAZGRANIČENJA	\N	134071
232	5	RASHODI	\N	134071
307	6	PRIHODI	\N	134071
363	7	OTVARANjE I ZAKLjUČAK RAČUNA STANjA I USPEHA	\N	134071
379	8	VANBILANSNA EVIDENCIJA	\N	134071
382	9	OBRAČUN TROŠKOVA I UČINAKA	\N	134071
436	013	Gudvil	\N	134071
437	019	Ispravka vrednosti nematerijalne imovine	\N	134071
438	04	DUGOROČNI FINANSIJSKI PLASMANI	\N	134071
439	040	Učešće u kapitalu zavisnih pravnih lica	\N	134071
440	041	Učešća u kapaitalu pridruženih pravnih lica i zajedničkim poduhvatima	\N	134071
441	042	Učešća u kapitalu ostlaih pravnih lica i druge hartije od vrednosti raspoložive za prodaju	\N	134071
442	043	Dugoročni plasmani matičnim, zavisnim i ostalim povezanim pravnim licima u zemlji	\N	134071
443	044	Dugoročni plasmani matičnim, zavisnim i ostalim povezanim pravnim licima u inostranstvu	\N	134071
444	045	Dugoročni plasmani u zemlji i inostranstvu	\N	134071
445	046	Hartine od vrednosti koje se drže do dospeća	\N	134071
446	047	Otkupljene sopstvene akcije i otkupljeni sopstveni udeli	\N	134071
447	048	Ostali dugoročni finansijski plasmani	\N	134071
448	049	Ispravka vrednosti dugoročnih finansijskih plasmana	\N	134071
449	05	DUGOROČNA POTRAŽIVANJA	\N	134071
450	050	Potraživanja od matičnih i zavisnih pravnih lica	\N	134071
451	051	Potraživanja od ostalih povezanih lica	\N	134071
452	052	Potraživanja po osnovu prodaje na robni kredit	\N	134071
453	053	Potraživanja za prodaju po ugovorima o finansijskom lizingu	\N	134071
454	054	Potraživanja po osnovu jemstva	\N	134071
455	055	Sporna i sumnjiva potraživanja	\N	134071
456	056	Ostala dugoročna potraživanja	\N	134071
457	059	Ispravka vrednosti dugoročnih potraživanja	\N	134071
458	104	Materijal, rezervni delovi, alat i inventar u obradi, doradi i manipulaciji	\N	134071
459	151	Plaćeni avansi za materijal, rezervne delove i inventar u inostranstvu	\N	134071
460	152	Plaćeni avansi za robu u zemlji	\N	134071
461	153	Plaćeni avansi za robu u inostranstvu	\N	134071
462	154	Plaćeni avansi za usluge u zemlji	\N	134071
463	155	Plaćeni avansi za usluge u inostranstvu	\N	134071
464	204	Kupci u zemlji	\N	134071
465	205	Kupci u inostranstvu	\N	134071
466	206	Ostala potraživanja po osnovu prodaje	\N	134071
467	225	Potraživanja za naknade zarada koje se refundiraju	\N	134071
468	226	Potraživanja po osnovu naknada šteta	\N	134071
469	278	PDV nadoknada isplaćena poljoprivrednicima	\N	134071
470	306	Emisiona premija	\N	134071
471	332	Dobici ili gubici po osnovu ulaganja u vlasničke instrumente kapitala	\N	134071
472	333	Dobici ili gubici po osnovu udela u ostalom sveobuhvatnom dobitku ilio gubitku pridruženih društava	\N	134071
473	334	Dobici ili gubici po osnovu preračuna finansijskih izveštaja inostranog poslovanja	\N	134071
474	335	Dobici ili gubici od instrumenat zaštite neto ulaganja u inostrano poslovanje	\N	134071
475	336	Dobici ili gubici po osnovu instrumenata zaštite rizika (hedžinga) novčanog toka	\N	134071
476	337	Dobici ili gubici po osnovu hartija od vrednosti raspoloživih za prodaju	\N	134071
477	405	Rezervisanja za troškove sudskih sporova	\N	134071
478	416	Obaveze po osnovu finansijskog lizinga	\N	134071
479	435	Dobavljači u zemlji	\N	134071
480	436	Dobavljači u inostranstvu	\N	134071
481	467	Obaveze za kratkoročna rezervisanja	\N	134071
482	503	Nabavna arednost ostalih stalnih sredstava namenjenih prodaji	\N	134071
483	512	Troškovi ostalog materijala (režijskog)	\N	134071
484	514	Troškovi rezervnih delova	\N	134071
485	515	Troškovi jednokratnog otpisa alata i inventara	\N	134071
486	566	Rashodi po osnovu efeklata ugovorene zaštite od rizika, koji ne ispunjavaju uslove da se iskažu u okviru ostalog sveobuhvatnog dobitka	\N	134071
487	592	Rashodi po osnovu ispravki grešaka iz ranijih godina koje nisu materijalno značajne	\N	134071
488	604	Prihodi od prodaje robe na domaćem tržištu	\N	134071
489	605	Prihodi od prodaje robe na inostranom tržištu	\N	134071
490	614	Prihodi od prodaje proizvoda i usluga na domaćem tržištu	\N	134071
491	615	Prihodi od prodaje proizvoda i usluga na inostranom tržištu	\N	134071
492	692	Prihodi po osnovu ispravki grešaka iz ranijih godina koje nisu materijalno značajne	\N	134071
493	880	Tuđa sredstva uzeta u operativni lizing (zakup)	\N	134071
494	881	Preuzeti proizvodi i roba za zajedničko poslovanje	\N	134071
495	882	Roba uzeta u komision i konsignaciju	\N	134071
496	883	Materijal i roba primljeni na obradu i doradu	\N	134071
497	884	Data jemstva, garancije i druga prava	\N	134071
498	885	Hartije od vrednosti koje su van prometa	\N	134071
499	889	Imovina kod drugih subjekata	\N	134071
500	890	Obaveze za sredstva uzeta u operativni lizing (zakup)	\N	134071
501	891	Obaveze za preuzete proizvode i robu za zajedničko poslovanje	\N	134071
502	892	Obaveze za robu uzetu u komision i konsignaciju	\N	134071
503	893	Obaveze za materijal i robu primljenu na obradu i doradu	\N	134071
504	894	Obaveze za data jemstva, garancije i druga prava	\N	134071
505	895	Obaveze za hartije od vrednosti koje su van prometa	\N	134071
506	899	Obaveze za imovinu kod drugih subjekata	\N	134071
507	0	UPISANI A NEUPLAĆENI  KAPITAL I STALNA IMOVINA	\N	343342
536	1	ZALIHE I STALNA SREDSTVA NAMENJENA PRODAJI	\N	343342
571	2	KRATKOROČNA POTRAŽIVANjA I PLASMANI, NOVČANA SREDSTVA I AKTIVNA VREMENSKA RAZGRANIČENJA	\N	343342
629	3	KAPITAL	\N	343342
653	4	DUGOROČNA REZERVISSANJA, OBAVEZE I PASIVNA VREMENSKA RAZGRANIČENJA	\N	343342
731	5	RASHODI	\N	343342
806	6	PRIHODI	\N	343342
821	9	OBRAČUN TROŠKOVA I UČINAKA	\N	343342
865	7	OTVARANjE I ZAKLjUČAK RAČUNA STANjA I USPEHA	\N	343342
881	8	VANBILANSNA EVIDENCIJA	\N	343342
2490	941	Mesto troška prodaje	895	343342
2489	9416	Troškovi nabavke, tehničke uprave i pomoćnih delatnosti	2490	343342
2491	9401	Troškovi zarada, naknada zarada i ostali lični rashodi MT uprave	2487	343342
2492	9402	Troškovi proizvodnih usluga MT uprave	2487	343342
2493	9403	Troškovi amortizacije i rezervisanja MT uprave	2487	343342
2494	9404	Nematerijalni troškovi MT uprave	2487	343342
2495	9405	Drugi direktni troškovi MT uprave	2487	343342
2496	9406	Troškovi nabavke, tehničke uprave i pomoćnih delatnosti	2487	343342
2497	9407	Troškovi prodaje i sličnih aktivnosti	2487	343342
2498	9408	Prenos troškova MT uprave	2487	343342
2499	9410	Troškovi materijala MT prodaje	2490	343342
2500	9411	Troškovi zarada, naknada zarada i ostali lični rashodi MT prodaje	2490	343342
2501	9412	Troškovi proizvodnih usluga MT prodaje	2490	343342
2502	9413	Troškovi amortizacije i rezervisanja MT prodaje	2490	343342
2503	9414	Nematerijalni troškovi MT prodaje	2490	343342
2504	9415	Drugi direktni troškovi MT prodaje	2490	343342
2505	9417	Troškovi uprave i sličnih aktivnosti	2490	343342
2506	9418	Prenos troškova MT prodaje	2490	343342
2507	9500	Mterijal za izradu	897	343342
2508	9501	Troškovi usluga osnovne delatnosti	897	343342
2509	9502	Troškovi zarada, naknada zarada i ostali lični rashodi	897	343342
2510	9503	Troškovi amortizacije osnovnih sredstava osnovne delatnosti	897	343342
2511	9504	Drugi direktni troškovi izrade	897	343342
2512	9505	Troškovi nabavke, tehničke uprave i pomoćnih delatnosti	897	343342
2513	9506	Troškovi uprave, prodaje i sličnih aktivnosti (za učinke sa dužim proizvodnim ciklusom)	897	343342
2514	9507	Opšti troškovi finansiranja (za učinke sa dužim proizvodnim ciklusom)	897	343342
2515	9508	Prenos troškova završnih učinaka	897	343342
2516	9510	Materijal za izradu	898	343342
2517	9511	Troškovi usluga sporedne delatnosti	898	343342
2518	9513	Troškovi amortizacije osnovnih sredstava sporedne delatnosti	898	343342
2519	9514	Drugi direktni troškovi izrade	898	343342
2520	9515	Troškovi nabavke, tehničke uprave i pomoćnih delatnosti	898	343342
2521	9516	Troškovi uprave, prodaje i sličnih aktivnosti (za učinke sa dužim proizvodnim ciklusom)	898	343342
2522	9517	Opšti troškovi finansiranja (za učinke sa dužim proizvodnim ciklusom)	898	343342
2523	9518	Prenos troškova završnih učinaka	898	343342
2524	9520	Materijal za izradu	899	343342
2525	9521	Troškovi usluga pomoćne delatnosti	899	343342
2526	9523	Troškovi amortizacije osnovnih sredstava pomoćne delatnosti	899	343342
2527	9524	Drugi direktni troškovi izrade	899	343342
2528	9525	Troškovi nabavke, tehničke uprave i pomoćnih delatnosti	899	343342
2529	9526	Troškovi uprave, prodaje i sličnih aktivnosti (za učinke sa dužim proizvodnim ciklusom)	899	343342
2530	9527	Opšti troškovi finansiranja (za učinke sa dužim proizvodnim ciklusom)	899	343342
2531	9528	Prenos troškova završenih učinaka	899	343342
2532	9580	Poluproizvodi osnovne delatnosti	905	343342
2533	9581	Poluproizvodi sporedne delatnosti	905	343342
2534	9582	Poluproizvodi pomoćne delatnosti	905	343342
2535	9590	odstupanja od troškova nedovršene proizvodnje osnovne delatnosti	906	343342
2536	9591	Odstupanja od troškova nedovršene proizvodnje sporedne delatnosti	906	343342
2537	9592	Odstupanja od troškova nedovršene proizvodnje pomoćne delatnosti	906	343342
2538	9593	Odstupanja od troškova poluproizvoda osnovne delatnosti	906	343342
2539	9594	Odstupanja od troškova poluproizvoda sporedne delatnosti	906	343342
2540	9595	Odstupanja od troškova poluproizvoda pomoćne delatnosti	906	343342
2541	960	Gotovi proizvodi	820	343342
2542	9600	Proizvod "X" (analitika po proizvodima)	2541	343342
2543	9690	Odstupanja od troškova gotovih proizvoda osnovne delatnosti	914	343342
2544	9691	Odstupanja od troškova gotovih proizvoda sporedne delatnosti	914	343342
2545	9692	Odstupanja od troškova gotovih proizvoda pomoćne delatnosti	914	343342
2546	9800	Troškovi prodatih gotovih proizvoda	917	343342
2547	9801	Troškovi prodatih usluga	917	343342
2548	9802	Troškovi sopstvenih proizvoda	917	343342
2549	9803	Troškovi sopstvenog transporta	917	343342
2550	9810	Nabavna vrednost prodate robe na veliko	918	343342
2551	9811	Nabavna vrednost prodate robe iz maloprodajnih objekata	918	343342
2552	9820	Troškovi uprave koji se ne uključuju u cenu koštanja zaliha	919	343342
2553	9821	Troškovi prodaje koji se ne uključuju u cenu koštanja zaliha	919	343342
2554	9822	Ostali troškovi koji se ne uključuju u cenu koštanja zaliha	919	343342
2555	9830	Otpisi nedovrešene proizvodnje	920	343342
2556	9831	Otpisi gotovih proizvoda	920	343342
2557	9832	Manjkovi nedovršene proizvodnje	920	343342
2558	9833	Manjkovi gotovih proizvoda	920	343342
2559	9834	Viškovi nedovršene proizvodnje	920	343342
2560	9835	Viškovi gotovih proizvoda	920	343342
2561	9860	Prihodi od prodaje proizvoda i usluga matičnim, zavisnim i povezanim pravnim licima	922	343342
2562	9861	Prihodi od prodaje proizvoda na domaćem tržištu	922	343342
2563	9862	Prihodi od usluga na domaćem tržištu	922	343342
2564	9863	Prihodi od prodaje proizvoda na stranom tržištu	922	343342
2565	9864	Prihodi od usluga na tranom tržištu	922	343342
2566	9865	Prihodi od aktiviranja ili potrošnje proizvoda i usluga za sopstvene potrebe	922	343342
2567	9866	Prihodi od prodaje proiizvoda u komisionu i konsignaciji	922	343342
2568	9867	Dati popusti kod prodaje proizvoda (rabati, bonifikacije i sl)	922	343342
2569	9868	Drugi prihodi po osnovu proizvoda i usluga	922	343342
2570	9870	Prihodi od prodaje robe matičnim, zavisnim i povezanim pravnim licima	923	343342
2571	9871	Prihodi od prodaje robe na veliko na domaćem tržištu	923	343342
2572	9872	Prihodi od prodaje robe na malo na domaćem tržištu	923	343342
2573	9873	Prihodi od prodaje robe na veliko stranom tržištu	923	343342
2574	9874	Prihodi od aktiviranja ili potrošnje robe za sopstvene potrebe	923	343342
2575	9875	Prihodi od prodaje robe u komisionu i konsignaciji	923	343342
2576	9876	Dati popusti kod prodaje robe (rabati,bonifikacije i sl)	923	343342
2577	9877	Drugi prihodi po osnovu robe	923	343342
2578	9880	Prihodi od premija, subvencija, donacija, regresa, kompenzacija i povraćaja poreskih i drugih dažbina	924	343342
2579	9890	Prihodi od zakupnina	925	343342
2580	9891	Prihodi od članarina	925	343342
2581	9892	Prihodi od ukidanja rerzervisanja	925	343342
2582	9893	Drugi poslovni prihodi	925	343342
2583	9900	Poslovni dobitak	927	343342
2584	9901	Poslovni gubitak	927	343342
2585	9910	Dobitak po osnovu prodaje materijala	928	343342
2586	9911	Gubitak po osnovu prodaje materijala	928	343342
2587	9920	Manjkovi materijala	929	343342
2588	9921	Manjkovi robe	929	343342
2589	9930	Otpisi materijala	930	343342
2590	9931	Otpisi robe	930	343342
2591	9940	Viškovi materijala	931	343342
2592	9941	Viškovi robe	931	343342
1759	4543	Ostale obaveze prema zaposlenima koje se refundiraju	696	343342
2	00	UPISANI A NEUPLAĆENI KAPITAL	1	134071
3	000	Upisane a neuplaćene akcije	2	134071
4	001	Upisani a neuuplaćeni udeli i ulozi	2	134071
5	01	NEMATERIJALNA ULAGANjA	1	134071
6	010	Ulaganja u razvoj	5	134071
7	011	Koncesije, patenti, licence, robne i uslužne marke	5	134071
8	012	Softver i ostala prava	5	134071
9	014	Ostala nematerijalna imovina	5	134071
10	015	Nematerijalna imovina u pripremi	5	134071
11	016	Avansi za nematerijalnu imovinu	5	134071
12	02	NEKRETNINE, POSTROJENjA I OPREMA	1	134071
13	020	Poljoprivredno i ostalo zemljište	12	134071
14	021	Građevinsko zemljište	12	134071
15	022	Građevinski objekti	12	134071
16	023	Postrojenja i oprema	12	134071
17	024	Investicione nekretnine	12	134071
18	025	Ostale  nekretnine, postrojenja i oprema	12	134071
19	026	Nekretnine, postrojenja i oprema u pripremi	12	134071
20	027	Ulaganja na tuđim nekretninama, postrojenjima i opremi	12	134071
21	028	Avansi za nekretnine, postrojenja, opremu 	12	134071
22	029	Ispravka vrednosti nekretnina, postrojenja i opreme	12	134071
23	03	BIOLOŠKA SREDSTVA	1	134071
24	030	Šume	23	134071
25	031	Višegodišnji zasadi	23	134071
26	032	Osnovno stado	23	134071
31	037	Biološka sredstva u pripremi	23	134071
32	038	Avansi za biološka sredstva	23	134071
33	039	Ispravka vrednosti bioloških sredstava	23	134071
35	10	ZALIHE MATERIJALA	34	134071
36	100	Obračun nabavke zaliha materijala, rezervnih delova, alata i inventara	35	134071
37	101	Materijal	35	134071
38	102	Rezervni delovi	35	134071
39	103	Alat i inventar	35	134071
40	109	Ispravka vrednosti materijala, rezervnih delova, alata i invenatara	35	134071
41	11	NEDOVRŠENA PROIZVODNjA I USLUGE	34	134071
42	110	Nedovršena proizvodnja	41	134071
43	111	Nedovršene usluge	41	134071
44	12	GOTOVI PROIZVODI	34	134071
45	120	Gotovi proizvodi u skladištu	44	134071
46	13	ROBA	34	134071
47	130	Obračun nabavke robe	46	134071
48	131	Roba u magacinu	46	134071
49	132	Roba u prometu na veliko	46	134071
50	133	Roba u skladištu, stovarištu i prodavnicama kod drugih pravnih lica	46	134071
51	134	Roba u prometu na malo	46	134071
52	135	Roba u obradi, doradi i manipulaciji	46	134071
53	136	Roba u tranzitu	46	134071
54	137	Roba na putu	46	134071
55	139	Ispravka vrednosti robe	46	134071
56	14	STALNA SREDSTVA NAMENjENA PRODAJI	34	134071
57	140	Nematerijalna imovina  namenjena prodaji	56	134071
58	141	Zemljište namenjeno prodaji	56	134071
59	142	Građevinski objekti namenjeni prodaji	56	134071
60	143	Investicione nekretnine namenjene prodaji	56	134071
61	144	Ostale nekretnine namenjene prodaji	56	134071
62	145	Postrojenja i oprema namenjena prodaji	56	134071
63	146	Biološka sredstva namenjena prodaji	56	134071
64	147	Sredstva poslovanja koje se obustavlja	56	134071
65	149	Ispravka vrednosti stalnih sredstava i sredstava obustavljenog poslovanja namenjenih prodaji	56	134071
66	15	PLAĆENI AVANSI ZA ZALIHE I USLUGE	34	134071
67	150	Plaćeni avansi za materijal, rezervne delovi i inventar u zemlji	66	134071
68	159	Ispravka vrednosti plaćenih avansa	66	134071
70	20	POTRAŽIVANjA PO OSNOVU PRODAJE	69	134071
71	200	Kupci u zemlji -  matična i zavisna pravna lica	70	134071
72	201	Kupci u inostranstvu - matična i zavisna pravna lica	70	134071
73	202	Kupci u zemlji - ostala povezana lica	70	134071
74	203	Kupci u inostranstvu - ostala povezana lica	70	134071
75	209	Ispravka vrednosti potraživanja od prodaje	70	134071
76	21	POTRAŽIVANjA IZ SPECIFIČNIH POSLOVA	69	134071
77	210	Potraživanja od izvoznika	76	134071
78	211	Potraživanja po osnovu uvoza za tuđ račun	76	134071
79	212	Potraživanja iz komisione i konsignacione prodaje	76	134071
80	218	Ostala potraživanja iz specifičnih poslova	76	134071
81	219	Ispravka vrednosti potraživanja iz specifičnih poslova	76	134071
82	22	DRUGA POTRAŽIVANjA	69	134071
83	220	Potraživanja za kamatu i dividende	82	134071
84	221	Potraživanja od zaposlenih	82	134071
85	222	Potraživanja od državnih organa i organizacija	82	134071
86	223	Potraživanja za više plaćen porez na dobitak	82	134071
87	224	Potraživanja po osnovu preplaćenih ostalih poreza i doprinosa	82	134071
88	228	Ostala kratkoročna potraživanja	82	134071
89	229	Ispravka vrednosti drugih potraživanja	82	134071
90	23	KRATKOROČNI FINANSIJSKI PLASMANI	69	134071
91	230	Kratkoročni krediti i plasmani - matična i zavisna pravna lica	90	134071
92	231	Kratkoročni krediti i plasmani -  ostala povezana pravna lica	90	134071
93	232	Kratkoročni krediti i zajmovi  u zemlji	90	134071
94	233	Kratkoročni krediti  i zajmovi u inostranstvu	90	134071
95	234	Deo dugoročnih finansijskih plasmana koji dospeva do jedne godine	90	134071
96	235	Hartije od vrednosti koje se drže do dospeća - deo koji dospeva do jedne godine	90	134071
97	236	Finansijska sredstva koja se vrednuju po fer vrednosti kroz Bilans uspeha	90	134071
98	237	Otkupljene sopstvene akcije namenjene prodaji i otkupljeni sopstveni udeli namenjeni prodaji ili poništavanju	90	134071
99	238	Ostali kratkoročni finansijski plasmani	90	134071
100	239	Ispravka vrednosti kratkoročnih finansijskih plasmana	90	134071
101	24	GOTOVINSKI EKVIVALENTI I GOTOVINA	69	134071
102	240	Hartije od vrednosti – gotovinski ekvivalenti	101	134071
103	241	Tekući (poslovni) računi	101	134071
104	242	Izdvojena novčana sredstva i akreditivi	101	134071
105	243	Blagajna	101	134071
106	244	Devizni račun	101	134071
107	245	Devizni akreditivi	101	134071
108	246	Devizna blagajna	101	134071
109	248	Ostala novčana sredstva	101	134071
110	249	Novčana sredstva čije je korišćenje ograničeno ili vrednost umanjena	101	134071
111	27	POREZ NA DODATU VREDNOST	69	134071
112	270	Porez na dodatu vrednost u primljenim fakturama po opštoj stopi (osim plaćenih avansa)	111	134071
113	271	Porez na dodatu vrednost u primljenim fakturama po posebnoj stopi (osim pla­ćenih avansa)	111	134071
114	272	Porez na dodatu vrednost u datim avansima po opštoj stopi	111	134071
115	273	Porez na dodatu vrednost u datim avansima po posebnoj stopi	111	134071
116	274	Porez na dodatu vrednost plaćen pri uvozu dobara po opštoj stopi	111	134071
117	275	Porez na dodatu vrednost plaćen pri uvozu dobara po posebnoj stopi	111	134071
118	276	Porez na dodatu vrednost obračunat na usluge inostranih lica	111	134071
119	277	Naknadno vraćen porez na dodatu vrednost kupcima - stranim državljanima	111	134071
120	279	Potraživanja za više plaćeni porez na dodatu vrednost	111	134071
121	28	AKTIVNA VREMENSKA RAZGRANIČENjA	69	134071
122	280	Unapred plaćeni troškovi	121	134071
123	281	Potraživanja za nefakturisani prihod	121	134071
124	282	Razgraničeni troškovi po osnovu obaveza	121	134071
125	288	Odložena poreska sredstva	121	134071
126	289	Ostala aktivna vremenska razgraničenja	121	134071
130	30	OSNOVNI KAPITAL	129	134071
131	300	Akcijski kapital	130	134071
132	301	Udeli društava sa ograničenom odgovornošću	130	134071
133	302	Ulozi	130	134071
134	303	Državni kapital	130	134071
135	304	Društveni kapital	130	134071
136	305	Zadružni udeli	130	134071
137	309	Ostali osnovni kapital	130	134071
138	31	UPISANI A NEUPLAĆENI KAPITAL	129	134071
139	310	Upisane a neuplaćene akcije	138	134071
140	311	Upisani a neuplaćeni udeli i ulozi	138	134071
141	32	REZERVE	129	134071
143	321	Zakonske rezerve	141	134071
144	322	Statutarne i druge rezerve	141	134071
145	33	REVALORIZACIONE REZERVE I NEREALIZOVANI DOBICI I GUBICI	129	134071
146	330	Revalorizacione rezerve po osnovu revalorizacije nematerijalne imovine, nekretnina, postrojenja i opreme	145	134071
147	331	Aktuarski dobici ili gubici po osnovu planova definisanih primanja	145	134071
148	34	NERASPOREĐENI DOBITAK	129	134071
149	340	Neraspoređeni dobitak ranijih godina	148	134071
150	341	Neraspoređeni dobitak tekuće godine	148	134071
151	35	GUBITAK	129	134071
152	350	Gubitak ranijih godina	151	134071
153	351	Gubitak tekuće godine	151	134071
155	40	DUGOROČNA REZERVISANjA	154	134071
156	400	Rezervisanja za troškove u garantnom roku	155	134071
157	401	Rezervisanja za troškove obnavljanja prirodnih bogatstava	155	134071
158	402	Rezervisanja za zadržane kaucije i depozite	155	134071
159	403	Rezervisanja za troškove restrukturiranja	155	134071
160	404	Rezervisanja za naknade i druge beneficije zaposlenih	155	134071
161	409	Ostala dugoročna rezervisanja	155	134071
162	41	DUGOROČNE OBAVEZE	154	134071
163	410	Obaveze koje se mogu konvertovati u kapital	162	134071
164	411	Obaveze prema matičnim i zavisnim pravnim licima	162	134071
165	412	Obaveze prema ostalim povezanim pravnim licima	162	134071
166	413	Obaveze po emitovanim hartijama od vrednosti u periodu dužem od godinu dana	162	134071
167	414	Dugoročni krediti i zajmovi u zemlji	162	134071
168	415	Dugoročni kreditii zajmovi  u inostranstvu	162	134071
169	419	Ostale dugoročne obaveze	162	134071
170	42	KRATKOROČNE FINANSIJSKE OBAVEZE	154	134071
171	420	Kratkoročni krediti i zajmovi od matičnih i zavisnih pravnih lica	170	134071
172	421	Kratkoročni krediti i zajmovi od ostalih povezanih pravnih lica	170	134071
173	422	Kratkoročni kreditii zajmovi  u zemlji	170	134071
174	423	Kratkoročni krediti i zajmovi  u inostranstvu	170	134071
175	424	Deo dugoročnih kredita i zajmova koji dospeva do jedne godine	170	134071
176	425	Deo ostalih dugoročnih obaveza koje dospevaju do jedne godine	170	134071
177	426	Obaveze po kratkoročnim hartijama od vrednosti	170	134071
178	427	Obaveze po osnovu stalnih sredstava i sredstava obustavljenog poslovanja na­menjenih prodaji	170	134071
179	429	Ostale kratkoročne finansijske obaveze	170	134071
180	43	OBAVEZE IZ POSLOVANjA	154	134071
181	430	Primljeni avansi, depoziti i kaucije	180	134071
182	431	Dobavljači -  matična i zavisna pravna lica u zemlji	180	134071
183	432	Dobavljači - matična i zavisna pravna lica u inostranstvu	180	134071
184	433	Dobavljači - ostala povezana lica u zemlji	180	134071
185	434	Dobavljači - ostala povezana lica u inostranstvu	180	134071
186	439	Ostale obaveze iz poslovanja	180	134071
187	44	OBAVEZE IZ SPECIFIČNIH POSLOVA	154	134071
188	440	Obaveze prema uvozniku	187	134071
189	441	Obaveze po osnovu izvoza za tuđ račun	187	134071
190	442	Obaveze po osnovu komisione i konsignacione prodaje	187	134071
191	449	Ostale obaveze iz specifičnih poslova	187	134071
192	45	­OBAVEZE PO OSNOVU ZARADA I NAKNADA ZARADA	154	134071
193	450	Obaveze za neto zarade i naknade zarada, osim naknada zarada koje se refundiraju	192	134071
194	451	Obaveze za porez na zarade i naknade zarada na teret zaposlenog	192	134071
195	452	Obaveze za doprinose na zarade i naknade zarada na teret zaposlenog	192	134071
196	453	Obaveze za poreze i doprinose na zarade i naknade zarada na teret poslodavca	192	134071
367	710	Rashodi i prihodi	366	134071
197	454	Obaveze za neto naknade zarada koje se refundiraju	192	134071
198	455	Obaveze za poreze i doprinose na naknade zarada na teret zaposlenog koje se refundiraju	192	134071
199	456	Obaveze za poreze i doprinose na naknade zarada na teret poslodavca koje se refundiraju	192	134071
200	46	DRUGE OBAVEZE	154	134071
201	460	Obaveze po osnovu kamata i troškova finansiranja	200	134071
202	461	Obaveze za dividende	200	134071
203	462	Obaveze za učešće u dobitku	200	134071
204	463	Obaveze prema zaposlenima	200	134071
205	464	Obaveze prema direktoru, odnosno članovima organa upravljanja i nadzora	200	134071
206	465	Obaveze prema fizičkim licima za naknade po ugovorima	200	134071
207	466	Obaveze za neto prihod preduzetnika koji akontaciju podiže u toku godine	200	134071
208	469	Ostale obaveze	200	134071
209	47	OBAVEZE ZA POREZ NA DODATU VREDNOST	154	134071
210	470	Obaveze za porez na dodatu vrednost po izdatim fakturama po opštoj stopi (osim primljenih avansa)	209	134071
211	471	Obaveze za porez na dodatu vrednost po izdatim fakturama po posebnoj stopi (osim primljenih avansa)	209	134071
212	472	Obaveze za porez na dodatu vrednost po primljenim avansima po opštoj stopi	209	134071
213	473	Obaveze za porez na dodatu vrednost po primljenim avansima po posebnoj stopi	209	134071
214	474	Obaveze za porez na dodatu vrednost po osnovu sopstvene potrošnje po opštoj stopi	209	134071
215	475	Obaveze za porez na dodatu vrednost po osnovu sopstvene potrošnje po posebnoj stopi	209	134071
216	476	Obaveze za porez na dodatu vrednost po osnovu prodaje za gotovinu	209	134071
217	479	Obaveze za porez na dodatu vrednost po osnovu razlike obračunatog poreza na dodatu vrednost i prethodnog poreza	209	134071
218	48	OBAVEZE ZA OSTALE POREZE, DOPRINOSE I DRUGE DAŽBINE	154	134071
219	480	Obaveze za akcize	218	134071
220	481	Obaveze za porez iz rezultata	218	134071
221	482	Obaveze za poreze, carine i druge dažbine iz nabavke ili na teret troškova	218	134071
222	483	Obaveze za doprinose koji terete troškove	218	134071
223	489	Ostale obaveze za poreze, doprinose i druge dažbine	218	134071
224	49	PASIVNA VREMENSKA RAZGRANIČENjA	154	134071
225	490	Unapred obračunati troškovi	224	134071
226	491	Unapred naplaćeni prihodi	224	134071
227	494	Razgraničeni zavisni troškovi nabavke	224	134071
228	495	Odloženi prihodi i primljene donacije	224	134071
229	496	Razgraničeni prihodi po osnovu potra­živanja	224	134071
230	498	Odložene poreske obaveze	224	134071
231	499	Ostala pasivna vremenska razgraničenja	224	134071
233	50	NABAVNA VREDNOST PRODATE ROBE	232	134071
234	500	Nabavka robe	233	134071
235	501	Nabavna vrednost prodate robe	233	134071
236	502	Nabavna vrednost prodatih nekretnina pribavljenih radi prodaje	233	134071
237	51	TROŠKOVI MATERIJALA	232	134071
238	510	Nabavka materijala	237	134071
239	511	Troškovi materijala za izradu 	237	134071
240	513	Troškovi goriva i energije	237	134071
241	52	TROŠKOVI ZARADA, NAKNADA ZARADA I OSTALI LIČNI RASHODI	232	134071
242	520	Troškovi zarada i naknada zarada (bruto)	241	134071
243	521	Troškovi poreza i doprinosa na zarade i naknade zarada na teret poslodavca	241	134071
244	522	Troškovi naknada po ugovoru o delu	241	134071
245	523	Troškovi naknada po autorskim ugovorima	241	134071
246	524	Troškovi naknada po ugovoru o privremenim i povremenim poslovima	241	134071
247	525	Troškovi naknada fizičkim licima po osnovu ostalih ugovora	241	134071
248	526	Troškovi naknada direktoru, odnosno članovima organa upravljanja i nadzora	241	134071
249	529	Ostali lični rashodi i naknade	241	134071
250	53	TROŠKOVI PROIZVODNIH USLUGA	232	134071
251	530	Troškovi usluga na izradi učinaka	250	134071
252	531	Troškovi transportnih usluga	250	134071
253	532	Troškovi usluga održavanja	250	134071
254	533	Troškovi zakupnina	250	134071
255	534	Troškovi sajmova	250	134071
256	535	Troškovi reklame i propagande	250	134071
257	536	Troškovi istraživanja	250	134071
258	537	Troškovi razvoja koji se ne kapitalizuju	250	134071
259	539	Troškovi ostalih usluga	250	134071
260	54	TROŠKOVI AMORTIZACIJE I REZERVISANjA	232	134071
261	540	Troškovi amortizacije	260	134071
262	541	Troškovi rezervisanja za garantni rok	260	134071
263	542	Rezervisanja za troškove obnavljanja prirodnih bogatstava	260	134071
264	543	Rezervisanja za zadržane kaucije i depozite	260	134071
265	544	Rezervisanja za troškove restrukturiranja	260	134071
266	545	Rezervisanja za naknade i druge beneficije zaposlenih	260	134071
267	549	Ostala dugoročna rezervisanja	260	134071
268	55	NEMATERIJALNI TROŠKOVI	232	134071
269	550	Troškovi neproizvodnih usluga	268	134071
270	551	Troškovi reprezentacije	268	134071
271	552	Troškovi premija osiguranja	268	134071
272	553	Troškovi platnog prometa	268	134071
273	554	Troškovi članarina	268	134071
274	555	Troškovi poreza	268	134071
275	556	Troškovi doprinosa	268	134071
276	559	Ostali nematerijalni troškovi	268	134071
277	56	FINANSIJSKI RASHODI	232	134071
278	560	Finansijski rashodi iz odnosa sa matičnim i zavisnim pravnim licima	277	134071
279	561	Finansijski rashodi iz odnosa sa ostalim povezanim pravnim licima	277	134071
280	562	Rashodi kamata (prema trećim licima)	277	134071
281	563	Negativne kursne razlike (prema trećim licima)	277	134071
282	564	Rashodi po osnovu efekata valutne klauzule (prema trećim licima)	277	134071
283	565	Rashodi od učešća u gubitku pridruženih pravnih lica i zajedničkih poduhvata	277	134071
284	569	Ostali finansijski rashodi	277	134071
285	57	OSTALI RASHODI	232	134071
366	71	ZAKLjUČAK RAČUNA USPEHA	363	134071
286	570	Gubici po osnovu rashodovanja i prodaje nematerijalne imovine, nekretnina, postrojenja i opreme	285	134071
287	571	Gubici po osnovu rashodovanja i prodaje bioloških sredstava	285	134071
288	572	Gubici po osnovu prodaje učešća u kapitalu i hartija od vrednosti	285	134071
289	573	Gubici od prodaje materijala	285	134071
290	574	Manjkovi	285	134071
291	575	Rashodi po osnovu efekata ugovorene zaštite od rizika, koji ne ispunjavaju uslove da se iskažu u okviru ostalog sveobuhvatnog rezultata	285	134071
292	576	Rashodi po osnovu direktnih otpisa potraživanja	285	134071
293	577	Rashodi po osnovu rashodovanja zaliha materijala i robe	285	134071
294	579	Ostali nepomenuti rashodi	285	134071
295	58	RASHODI PO OSNOVU OBEZVREĐENJA IMOVINE KOJE SE VREDNUJE PO FER VREDNOSTI KROZ BILANS USPEHA	232	134071
296	580	Obezvređenje bioloških sredstava	295	134071
297	581	Obezvređenje nematerijalne imovine	295	134071
298	582	Obezvređenje nekretnina, postrojenja i opreme	295	134071
299	583	Obezvređenje dugoročnih finansijskih plasmana i drugih hartija od vrednosti raspoloživih za prodaju	295	134071
300	584	Obezvređenje zaliha materijala i robe	295	134071
301	585	Obezvređenje potraživanja i kratkoročnih finansijskih plasmana	295	134071
302	589	Obezvređenje ostale imovine	295	134071
303	59	GUBITAK POSLOVANJA KOJE SE OBUSTAVLJA, EFEKTI PROMENE RAČUNOVODSTVENE POLITIKE, ISPRAVKE GREŠAKA RANIJIH PERIODA I PRENOS RASHODA	232	134071
304	590	Gubitak poslovanja koje se obustavlja	303	134071
305	591	Rashodi po osnovu efekata promene računovodstvenih politika	303	134071
306	599	Prenos rashoda 	303	134071
308	60	PRIHODI OD PRODAJE ROBE	307	134071
309	600	Prihodi od prodaje robe matičnim i zavisnim pravnim licima na domaćem tržištu	308	134071
310	601	Prihodi od prodaje robe matičnim i zavisnim pravnim licima na inostranom tržištu	308	134071
311	602	Prihodi od prodaje robe ostalim povezanim pravnim licima na domaćem tržištu	308	134071
312	603	Prihodi od prodaje robe ostalim povezanim pravnim licima na inostranom tržištu	308	134071
313	61	PRIHODI OD PRODAJE PROIZVODA I USLUGA	307	134071
314	610	Prihodi od prodaje proizvoda i usluga matičnim i zavisnim pravnim licima na domaćem tržištu	313	134071
315	611	Prihodi od prodaje proizvoda i usluga matičnim i zavisnim pravnim licima na inostranom tržištu	313	134071
316	612	Prihodi od prodaje proizvoda i usluga ostalim povezanim pravnim licima na domaćem tržištu	313	134071
317	613	Prihodi od prodaje proizvoda i usluga ostalim povezanim pravnim licima na inostranom tržištu	313	134071
318	62	PRIHODI OD AKTIVIRANjA UČINAKA I ROBE	307	134071
319	620	Prihodi od aktiviranja ili potrošnje robe za sopstvene potrebe	318	134071
320	621	Prihodi od aktiviranja ili potrošnje proizvoda i usluga za sopstvene potrebe	318	134071
321	63	PROMENA VREDNOSTI ZALIHA UČINAKA	307	134071
322	630	Povećanje vrednosti zaliha nedovršenih i gotovih proizvoda i nedovršenih usluga	321	134071
323	631	Smanjenje vrednosti zaliha nedovršenih i gotovih proizvoda i nedovršenih usluga	321	134071
324	64	­PRIHODI OD PREMIJA, SUBVENCIJA, DOTACIJA, DONACIJA I SL.	307	134071
325	640	Prihodi od premija, subvencija, dotacija, regresa, kompenzacija i povraćaja poreskih dažbina	324	134071
326	641	Prihodi po osnovu uslovljenih donacija	324	134071
327	65	DRUGI POSLOVNI PRIHODI	307	134071
328	650	Prihodi od zakupnina	327	134071
329	651	Prihodi od članarina	327	134071
330	652	Prihodi od tantijema i licencnih naknada	327	134071
331	659	Ostali poslovni prihodi	327	134071
332	66	FINANSIJSKI PRIHODI	307	134071
333	660	Finansijski prihodi od matičnih i zavisnih pravnih lica	332	134071
334	661	Finansijski prihodi od ostalih povezanih pravnih lica	332	134071
335	662	Prihodi od kamata (od trećih lica)	332	134071
336	663	Pozitivne kursne razlike (prema trećim licima)	332	134071
337	664	Prihodi po osnovu efekata valutne klauzule (prema trećim licima)	332	134071
338	665	Prihoda od učešća u dobitku pridruženih pravnih lica i zajedničkih poduhvata	332	134071
339	669	Ostali finansijski prihodi	332	134071
340	67	OSTALI PRIHODI	307	134071
341	670	Dobici od prodaje nematerijalne imovine, nekretnina, postrojenja i opreme	340	134071
342	671	Dobici od prodaje bioloških sredstava	340	134071
343	672	Dobici od prodaje učešća i dugoročnih hartija od vrednosti	340	134071
344	673	Dobici od prodaje materijala	340	134071
345	674	Viškovi	340	134071
346	675	Naplaćena otpisana potraživanja	340	134071
347	676	Prihodi po osnovu efekata ugovorene zaštite od rizika koji ne ispunjavaju uslove da se iskažu u okviru ostalog sveobuhvatnog rezultata	340	134071
348	677	Prihodi od smanjenja obaveza	340	134071
349	678	Prihodi od ukidanja dugoročnih i kratkoročnih rezervisanja	340	134071
350	679	Ostali nepomenuti prihodi	340	134071
351	68	PRIHODI OD USKLAĐIVANjA VREDNOSTI IMOVINE	307	134071
352	680	Prihodi od usklađivanja vrednosti bioloških sredstava	351	134071
353	681	Prihodi od usklađivanja vrednosti nematerijalne imovine	351	134071
354	682	Prihodi od usklađivanja vrednosti nekretnina, postrojenja i opreme	351	134071
355	683	Prihodi od usklađivanja vrednosti dugoročnih finansijskih plasmana i hartija od vrednosti raspoloživih za prodaju	351	134071
356	684	Prihodi od usklađivanja vrednosti zaliha	351	134071
357	685	Prihodi od usklađivanja vrednosti potraživanja i kratkoročnih finansijskih plasmana	351	134071
358	689	Prihodi od usklađivanja vrednosti ostale imovine	351	134071
359	69	­DOBITAK POSLOVANjA KOJE SE OBUSTAVLJA, EFEKTI PROMENE RAČUNOVODSTVENE POLITIKE, ISPRAVKE GREŠAKA RANIJIH PERIODA I PRENOS PRIHODA	307	134071
360	690	Dobitak poslovanja koje se obustavlja	359	134071
361	691	Prihodi od efekata promene računovodstvenih politika	359	134071
362	699	Prenos prihoda	359	134071
364	70	OTVARANjE GLAVNE KNjIGE	363	134071
365	700	Otvaranje glavne knjige	364	134071
368	711	Dobitak i gubitak poslovanja koje se obustavlja	366	134071
369	712	Prenos ukupnog rezultata	366	134071
370	72	RAČUN DOBITKA I GUBITKA	363	134071
371	720	Dobitak ili gubitak	370	134071
372	721	Poreski rashod perioda	370	134071
373	722	Odloženi poreski rashodi i prihodi perioda	370	134071
374	723	Lična primanja poslodavca	370	134071
375	724	Prenos dobitka ili gubitka	370	134071
376	73	ZAKLjUČAK RAČUNA STANjA	363	134071
377	730	Izravnanje računa stanja	376	134071
378	74	SLOBODNA GRUPA	363	134071
380	88	VANBILANSNA AKTIVA	379	134071
381	89	VANBILANSNA PASIVA	379	134071
383	90	RAČUNI ODNOSA S FINANSIJ­SKIM KNjIGOVODSTVOM	382	134071
384	900	Račun za preuzimanje zaliha	383	134071
385	901	Račun za preuzimanje nabavke materijala i robe	383	134071
386	902	Račun za preuzimanje troškova	383	134071
387	903	Račun za preuzimanje prihoda	383	134071
388	91	MATERIJAL I ROBA	382	134071
389	910	Materijal	388	134071
390	911	­Roba	388	134071
391	912	Proizvodi i roba u prodavnicama pro­izvođača	388	134071
392	92	­RAČUNI MESTA TROŠKOVA NA­BAVKE, TEHNIČKE UPRAVE I PO­MOĆNIH DELATNOSTI	382	134071
393	93	RAČUNI GLAVNIH PROIZVODNIH MESTA TROŠKOVA	382	134071
394	94	RAČUNI MESTA TROŠKOVA UPRA­VE, PRODAJE I SLIČNIH AKTIV­NOSTI	382	134071
395	95	NOSIOCI TROŠKOVA 	382	134071
396	950	Nosioci troškova	395	134071
397	951	Nosioci troškova	395	134071
398	952	Nosioci troškova	395	134071
399	953	Nosioci troškova	395	134071
400	954	Nosioci troškova	395	134071
401	955	Nosioci troškova	395	134071
402	956	Nosioci troškova	395	134071
403	957	Nosioci troškova	395	134071
404	958	Poluproizvodi sopstvene proizvodnje	395	134071
405	959	Odstupanja u troškovima nosioca troškova	395	134071
406	96	GOTOVI PROIZVODI	382	134071
407	960	Gotovi proizvodi	406	134071
408	961	Gotovi proizvodi	406	134071
409	962	Gotovi proizvodi	406	134071
410	963	Gotovi proizvodi	406	134071
411	964	Gotovi proizvodi	406	134071
412	965	Gotovi proizvodi	406	134071
413	966	Gotovi proizvodi	406	134071
414	967	Gotovi proizvodi	406	134071
415	968	Gotovi proizvodi	406	134071
416	969	Odstupanja u troškovima gotovih proizvoda	406	134071
417	97	SLOBODNA GRUPA	382	134071
418	98	RASHODI I PRIHODI	382	134071
419	980	Troškovi prodatih proizvoda i usluga	418	134071
420	981	Nabavna vrednost prodate robe	418	134071
421	982	Troškovi perioda	418	134071
422	983	Otpisi, manjkovi i viškovi zaliha učinaka	418	134071
423	985	Slobodan račun	418	134071
424	986	Prihodi po osnovu proizvoda i usluga	418	134071
425	987	Prihodi po osnovu robe 	418	134071
426	988	Slobodan račun	418	134071
427	989	Drugi prihodi	418	134071
428	99	RAČUNI DOBITKA, GUBITKA I ZAKLjUČKA	382	134071
429	990	Poslovni dobitak i gubitak	428	134071
430	991	Gubitak i dobitak po osnovu prodaje materijala	428	134071
431	992	Manjkovi materijala i robe	428	134071
432	993	Otpisi materijala i robe	428	134071
433	994	Viškovi materijala i robe	428	134071
434	999	Zaključak obračuna troškova i učinaka	428	134071
508	00	UPISANI A NEUPLAĆENI KAPITAL	507	343342
509	000	Upisane a neuplaćene akcije	508	343342
510	001	Upisani a neuuplaćeni udeli i ulozi	508	343342
511	01	NEMATERIJALNA ULAGANjA	507	343342
512	010	Ulaganja u razvoj	511	343342
513	011	Koncesije, patenti, licence, robne i uslužne marke	511	343342
514	012	Softver i ostala prava	511	343342
515	014	Ostala nematerijalna imovina	511	343342
516	015	Nematerijalna imovina u pripremi	511	343342
517	016	Avansi za nematerijalnu imovinu	511	343342
518	02	NEKRETNINE, POSTROJENjA I OPREMA	507	343342
519	020	Poljoprivredno i ostalo zemljište	518	343342
520	021	Građevinsko zemljište	518	343342
521	022	Građevinski objekti	518	343342
522	023	Postrojenja i oprema	518	343342
523	024	Investicione nekretnine	518	343342
524	025	Ostale  nekretnine, postrojenja i oprema	518	343342
525	026	Nekretnine, postrojenja i oprema u pripremi	518	343342
526	027	Ulaganja na tuđim nekretninama, postrojenjima i opremi	518	343342
890	910	Materijal	889	343342
527	028	Avansi za nekretnine, postrojenja, opremu 	518	343342
528	029	Ispravka vrednosti nekretnina, postrojenja i opreme	518	343342
529	03	BIOLOŠKA SREDSTVA	507	343342
530	030	Šume	529	343342
531	031	Višegodišnji zasadi	529	343342
532	032	Osnovno stado	529	343342
533	037	Biološka sredstva u pripremi	529	343342
534	038	Avansi za biološka sredstva	529	343342
535	039	Ispravka vrednosti bioloških sredstava	529	343342
537	10	ZALIHE MATERIJALA	536	343342
538	100	Obračun nabavke zaliha materijala, rezervnih delova, alata i inventara	537	343342
539	101	Materijal	537	343342
540	102	Rezervni delovi	537	343342
541	103	Alat i inventar	537	343342
542	109	Ispravka vrednosti materijala, rezervnih delova, alata i invenatara	537	343342
543	11	NEDOVRŠENA PROIZVODNjA I USLUGE	536	343342
544	110	Nedovršena proizvodnja	543	343342
545	111	Nedovršene usluge	543	343342
546	12	GOTOVI PROIZVODI	536	343342
547	120	Gotovi proizvodi u skladištu	546	343342
548	13	ROBA	536	343342
549	130	Obračun nabavke robe	548	343342
550	131	Roba u magacinu	548	343342
551	132	Roba u prometu na veliko	548	343342
552	133	Roba u skladištu, stovarištu i prodavnicama kod drugih pravnih lica	548	343342
553	134	Roba u prometu na malo	548	343342
554	135	Roba u obradi, doradi i manipulaciji	548	343342
555	136	Roba u tranzitu	548	343342
556	137	Roba na putu	548	343342
557	139	Ispravka vrednosti robe	548	343342
558	14	STALNA SREDSTVA NAMENjENA PRODAJI	536	343342
559	140	Nematerijalna imovina  namenjena prodaji	558	343342
560	141	Zemljište namenjeno prodaji	558	343342
561	142	Građevinski objekti namenjeni prodaji	558	343342
562	143	Investicione nekretnine namenjene prodaji	558	343342
563	144	Ostale nekretnine namenjene prodaji	558	343342
564	145	Postrojenja i oprema namenjena prodaji	558	343342
565	146	Biološka sredstva namenjena prodaji	558	343342
566	147	Sredstva poslovanja koje se obustavlja	558	343342
567	149	Ispravka vrednosti stalnih sredstava i sredstava obustavljenog poslovanja namenjenih prodaji	558	343342
568	15	PLAĆENI AVANSI ZA ZALIHE I USLUGE	536	343342
569	150	Plaćeni avansi za materijal, rezervne delovi i inventar u zemlji	568	343342
570	159	Ispravka vrednosti plaćenih avansa	568	343342
572	20	POTRAŽIVANjA PO OSNOVU PRODAJE	571	343342
573	200	Kupci u zemlji -  matična i zavisna pravna lica	572	343342
574	201	Kupci u inostranstvu - matična i zavisna pravna lica	572	343342
575	202	Kupci u zemlji - ostala povezana lica	572	343342
576	203	Kupci u inostranstvu - ostala povezana lica	572	343342
577	209	Ispravka vrednosti potraživanja od prodaje	572	343342
578	21	POTRAŽIVANjA IZ SPECIFIČNIH POSLOVA	571	343342
579	210	Potraživanja od izvoznika	578	343342
580	211	Potraživanja po osnovu uvoza za tuđ račun	578	343342
581	212	Potraživanja iz komisione i konsignacione prodaje	578	343342
582	218	Ostala potraživanja iz specifičnih poslova	578	343342
583	219	Ispravka vrednosti potraživanja iz specifičnih poslova	578	343342
584	22	DRUGA POTRAŽIVANjA	571	343342
585	220	Potraživanja za kamatu i dividende	584	343342
586	221	Potraživanja od zaposlenih	584	343342
587	222	Potraživanja od državnih organa i organizacija	584	343342
588	223	Potraživanja za više plaćen porez na dobitak	584	343342
589	224	Potraživanja po osnovu preplaćenih ostalih poreza i doprinosa	584	343342
590	228	Ostala kratkoročna potraživanja	584	343342
591	229	Ispravka vrednosti drugih potraživanja	584	343342
592	23	KRATKOROČNI FINANSIJSKI PLASMANI	571	343342
593	230	Kratkoročni krediti i plasmani - matična i zavisna pravna lica	592	343342
594	231	Kratkoročni krediti i plasmani -  ostala povezana pravna lica	592	343342
595	232	Kratkoročni krediti i zajmovi  u zemlji	592	343342
596	233	Kratkoročni krediti  i zajmovi u inostranstvu	592	343342
597	234	Deo dugoročnih finansijskih plasmana koji dospeva do jedne godine	592	343342
598	235	Hartije od vrednosti koje se drže do dospeća - deo koji dospeva do jedne godine	592	343342
599	236	Finansijska sredstva koja se vrednuju po fer vrednosti kroz Bilans uspeha	592	343342
600	237	Otkupljene sopstvene akcije namenjene prodaji i otkupljeni sopstveni udeli namenjeni prodaji ili poništavanju	592	343342
601	238	Ostali kratkoročni finansijski plasmani	592	343342
602	239	Ispravka vrednosti kratkoročnih finansijskih plasmana	592	343342
603	24	GOTOVINSKI EKVIVALENTI I GOTOVINA	571	343342
604	240	Hartije od vrednosti – gotovinski ekvivalenti	603	343342
605	241	Tekući (poslovni) računi	603	343342
606	242	Izdvojena novčana sredstva i akreditivi	603	343342
607	243	Blagajna	603	343342
608	244	Devizni račun	603	343342
609	245	Devizni akreditivi	603	343342
610	246	Devizna blagajna	603	343342
611	248	Ostala novčana sredstva	603	343342
612	249	Novčana sredstva čije je korišćenje ograničeno ili vrednost umanjena	603	343342
613	27	POREZ NA DODATU VREDNOST	571	343342
614	270	Porez na dodatu vrednost u primljenim fakturama po opštoj stopi (osim plaćenih avansa)	613	343342
615	271	Porez na dodatu vrednost u primljenim fakturama po posebnoj stopi (osim pla­ćenih avansa)	613	343342
616	272	Porez na dodatu vrednost u datim avansima po opštoj stopi	613	343342
617	273	Porez na dodatu vrednost u datim avansima po posebnoj stopi	613	343342
618	274	Porez na dodatu vrednost plaćen pri uvozu dobara po opštoj stopi	613	343342
619	275	Porez na dodatu vrednost plaćen pri uvozu dobara po posebnoj stopi	613	343342
620	276	Porez na dodatu vrednost obračunat na usluge inostranih lica	613	343342
621	277	Naknadno vraćen porez na dodatu vrednost kupcima - stranim državljanima	613	343342
622	279	Potraživanja za više plaćeni porez na dodatu vrednost	613	343342
623	28	AKTIVNA VREMENSKA RAZGRANIČENjA	571	343342
624	280	Unapred plaćeni troškovi	623	343342
625	281	Potraživanja za nefakturisani prihod	623	343342
626	282	Razgraničeni troškovi po osnovu obaveza	623	343342
627	288	Odložena poreska sredstva	623	343342
628	289	Ostala aktivna vremenska razgraničenja	623	343342
630	30	OSNOVNI KAPITAL	629	343342
631	300	Akcijski kapital	630	343342
632	301	Udeli društava sa ograničenom odgovornošću	630	343342
633	302	Ulozi	630	343342
634	303	Državni kapital	630	343342
635	304	Društveni kapital	630	343342
636	305	Zadružni udeli	630	343342
637	309	Ostali osnovni kapital	630	343342
638	31	UPISANI A NEUPLAĆENI KAPITAL	629	343342
639	310	Upisane a neuplaćene akcije	638	343342
640	311	Upisani a neuplaćeni udeli i ulozi	638	343342
641	32	REZERVE	629	343342
642	321	Zakonske rezerve	641	343342
643	322	Statutarne i druge rezerve	641	343342
644	33	REVALORIZACIONE REZERVE I NEREALIZOVANI DOBICI I GUBICI	629	343342
645	330	Revalorizacione rezerve po osnovu revalorizacije nematerijalne imovine, nekretnina, postrojenja i opreme	644	343342
646	331	Aktuarski dobici ili gubici po osnovu planova definisanih primanja	644	343342
647	34	NERASPOREĐENI DOBITAK	629	343342
648	340	Neraspoređeni dobitak ranijih godina	647	343342
649	341	Neraspoređeni dobitak tekuće godine	647	343342
650	35	GUBITAK	629	343342
651	350	Gubitak ranijih godina	650	343342
652	351	Gubitak tekuće godine	650	343342
654	40	DUGOROČNA REZERVISANjA	653	343342
655	400	Rezervisanja za troškove u garantnom roku	654	343342
656	401	Rezervisanja za troškove obnavljanja prirodnih bogatstava	654	343342
657	402	Rezervisanja za zadržane kaucije i depozite	654	343342
658	403	Rezervisanja za troškove restrukturiranja	654	343342
659	404	Rezervisanja za naknade i druge beneficije zaposlenih	654	343342
660	409	Ostala dugoročna rezervisanja	654	343342
661	41	DUGOROČNE OBAVEZE	653	343342
662	410	Obaveze koje se mogu konvertovati u kapital	661	343342
663	411	Obaveze prema matičnim i zavisnim pravnim licima	661	343342
664	412	Obaveze prema ostalim povezanim pravnim licima	661	343342
665	413	Obaveze po emitovanim hartijama od vrednosti u periodu dužem od godinu dana	661	343342
666	414	Dugoročni krediti i zajmovi u zemlji	661	343342
667	415	Dugoročni kreditii zajmovi  u inostranstvu	661	343342
668	419	Ostale dugoročne obaveze	661	343342
669	42	KRATKOROČNE FINANSIJSKE OBAVEZE	653	343342
670	420	Kratkoročni krediti i zajmovi od matičnih i zavisnih pravnih lica	669	343342
671	421	Kratkoročni krediti i zajmovi od ostalih povezanih pravnih lica	669	343342
672	422	Kratkoročni kreditii zajmovi  u zemlji	669	343342
673	423	Kratkoročni krediti i zajmovi  u inostranstvu	669	343342
674	424	Deo dugoročnih kredita i zajmova koji dospeva do jedne godine	669	343342
675	425	Deo ostalih dugoročnih obaveza koje dospevaju do jedne godine	669	343342
676	426	Obaveze po kratkoročnim hartijama od vrednosti	669	343342
677	427	Obaveze po osnovu stalnih sredstava i sredstava obustavljenog poslovanja na­menjenih prodaji	669	343342
678	429	Ostale kratkoročne finansijske obaveze	669	343342
679	43	OBAVEZE IZ POSLOVANjA	653	343342
680	430	Primljeni avansi, depoziti i kaucije	679	343342
681	431	Dobavljači -  matična i zavisna pravna lica u zemlji	679	343342
682	432	Dobavljači - matična i zavisna pravna lica u inostranstvu	679	343342
683	433	Dobavljači - ostala povezana lica u zemlji	679	343342
684	434	Dobavljači - ostala povezana lica u inostranstvu	679	343342
685	439	Ostale obaveze iz poslovanja	679	343342
686	44	OBAVEZE IZ SPECIFIČNIH POSLOVA	653	343342
687	440	Obaveze prema uvozniku	686	343342
688	441	Obaveze po osnovu izvoza za tuđ račun	686	343342
689	442	Obaveze po osnovu komisione i konsignacione prodaje	686	343342
690	449	Ostale obaveze iz specifičnih poslova	686	343342
691	45	­OBAVEZE PO OSNOVU ZARADA I NAKNADA ZARADA	653	343342
692	450	Obaveze za neto zarade i naknade zarada, osim naknada zarada koje se refundiraju	691	343342
693	451	Obaveze za porez na zarade i naknade zarada na teret zaposlenog	691	343342
694	452	Obaveze za doprinose na zarade i naknade zarada na teret zaposlenog	691	343342
695	453	Obaveze za poreze i doprinose na zarade i naknade zarada na teret poslodavca	691	343342
696	454	Obaveze za neto naknade zarada koje se refundiraju	691	343342
697	455	Obaveze za poreze i doprinose na naknade zarada na teret zaposlenog koje se refundiraju	691	343342
698	456	Obaveze za poreze i doprinose na naknade zarada na teret poslodavca koje se refundiraju	691	343342
699	46	DRUGE OBAVEZE	653	343342
700	460	Obaveze po osnovu kamata i troškova finansiranja	699	343342
701	461	Obaveze za dividende	699	343342
702	462	Obaveze za učešće u dobitku	699	343342
703	463	Obaveze prema zaposlenima	699	343342
704	464	Obaveze prema direktoru, odnosno članovima organa upravljanja i nadzora	699	343342
705	465	Obaveze prema fizičkim licima za naknade po ugovorima	699	343342
706	466	Obaveze za neto prihod preduzetnika koji akontaciju podiže u toku godine	699	343342
707	469	Ostale obaveze	699	343342
708	47	OBAVEZE ZA POREZ NA DODATU VREDNOST	653	343342
709	470	Obaveze za porez na dodatu vrednost po izdatim fakturama po opštoj stopi (osim primljenih avansa)	708	343342
710	471	Obaveze za porez na dodatu vrednost po izdatim fakturama po posebnoj stopi (osim primljenih avansa)	708	343342
711	472	Obaveze za porez na dodatu vrednost po primljenim avansima po opštoj stopi	708	343342
712	473	Obaveze za porez na dodatu vrednost po primljenim avansima po posebnoj stopi	708	343342
713	474	Obaveze za porez na dodatu vrednost po osnovu sopstvene potrošnje po opštoj stopi	708	343342
714	475	Obaveze za porez na dodatu vrednost po osnovu sopstvene potrošnje po posebnoj stopi	708	343342
715	476	Obaveze za porez na dodatu vrednost po osnovu prodaje za gotovinu	708	343342
716	479	Obaveze za porez na dodatu vrednost po osnovu razlike obračunatog poreza na dodatu vrednost i prethodnog poreza	708	343342
717	48	OBAVEZE ZA OSTALE POREZE, DOPRINOSE I DRUGE DAŽBINE	653	343342
718	480	Obaveze za akcize	717	343342
719	481	Obaveze za porez iz rezultata	717	343342
720	482	Obaveze za poreze, carine i druge dažbine iz nabavke ili na teret troškova	717	343342
721	483	Obaveze za doprinose koji terete troškove	717	343342
722	489	Ostale obaveze za poreze, doprinose i druge dažbine	717	343342
723	49	PASIVNA VREMENSKA RAZGRANIČENjA	653	343342
724	490	Unapred obračunati troškovi	723	343342
725	491	Unapred naplaćeni prihodi	723	343342
726	494	Razgraničeni zavisni troškovi nabavke	723	343342
727	495	Odloženi prihodi i primljene donacije	723	343342
728	496	Razgraničeni prihodi po osnovu potra­živanja	723	343342
729	498	Odložene poreske obaveze	723	343342
730	499	Ostala pasivna vremenska razgraničenja	723	343342
732	50	NABAVNA VREDNOST PRODATE ROBE	731	343342
733	500	Nabavka robe	732	343342
734	501	Nabavna vrednost prodate robe	732	343342
735	502	Nabavna vrednost prodatih nekretnina pribavljenih radi prodaje	732	343342
736	51	TROŠKOVI MATERIJALA	731	343342
737	510	Nabavka materijala	736	343342
738	511	Troškovi materijala za izradu 	736	343342
739	513	Troškovi goriva i energije	736	343342
740	52	TROŠKOVI ZARADA, NAKNADA ZARADA I OSTALI LIČNI RASHODI	731	343342
741	520	Troškovi zarada i naknada zarada (bruto)	740	343342
742	521	Troškovi poreza i doprinosa na zarade i naknade zarada na teret poslodavca	740	343342
743	522	Troškovi naknada po ugovoru o delu	740	343342
744	523	Troškovi naknada po autorskim ugovorima	740	343342
745	524	Troškovi naknada po ugovoru o privremenim i povremenim poslovima	740	343342
746	525	Troškovi naknada fizičkim licima po osnovu ostalih ugovora	740	343342
747	526	Troškovi naknada direktoru, odnosno članovima organa upravljanja i nadzora	740	343342
748	529	Ostali lični rashodi i naknade	740	343342
749	53	TROŠKOVI PROIZVODNIH USLUGA	731	343342
750	530	Troškovi usluga na izradi učinaka	749	343342
751	531	Troškovi transportnih usluga	749	343342
752	532	Troškovi usluga održavanja	749	343342
753	533	Troškovi zakupnina	749	343342
754	534	Troškovi sajmova	749	343342
755	535	Troškovi reklame i propagande	749	343342
756	536	Troškovi istraživanja	749	343342
757	537	Troškovi razvoja koji se ne kapitalizuju	749	343342
758	539	Troškovi ostalih usluga	749	343342
759	54	TROŠKOVI AMORTIZACIJE I REZERVISANjA	731	343342
760	540	Troškovi amortizacije	759	343342
761	541	Troškovi rezervisanja za garantni rok	759	343342
762	542	Rezervisanja za troškove obnavljanja prirodnih bogatstava	759	343342
763	543	Rezervisanja za zadržane kaucije i depozite	759	343342
764	544	Rezervisanja za troškove restrukturiranja	759	343342
765	545	Rezervisanja za naknade i druge beneficije zaposlenih	759	343342
766	549	Ostala dugoročna rezervisanja	759	343342
767	55	NEMATERIJALNI TROŠKOVI	731	343342
768	550	Troškovi neproizvodnih usluga	767	343342
769	551	Troškovi reprezentacije	767	343342
770	552	Troškovi premija osiguranja	767	343342
771	553	Troškovi platnog prometa	767	343342
772	554	Troškovi članarina	767	343342
773	555	Troškovi poreza	767	343342
774	556	Troškovi doprinosa	767	343342
775	559	Ostali nematerijalni troškovi	767	343342
776	56	FINANSIJSKI RASHODI	731	343342
777	560	Finansijski rashodi iz odnosa sa matičnim i zavisnim pravnim licima	776	343342
778	561	Finansijski rashodi iz odnosa sa ostalim povezanim pravnim licima	776	343342
779	562	Rashodi kamata (prema trećim licima)	776	343342
780	563	Negativne kursne razlike (prema trećim licima)	776	343342
781	564	Rashodi po osnovu efekata valutne klauzule (prema trećim licima)	776	343342
782	565	Rashodi od učešća u gubitku pridruženih pravnih lica i zajedničkih poduhvata	776	343342
783	569	Ostali finansijski rashodi	776	343342
784	57	OSTALI RASHODI	731	343342
785	570	Gubici po osnovu rashodovanja i prodaje nematerijalne imovine, nekretnina, postrojenja i opreme	784	343342
786	571	Gubici po osnovu rashodovanja i prodaje bioloških sredstava	784	343342
787	572	Gubici po osnovu prodaje učešća u kapitalu i hartija od vrednosti	784	343342
788	573	Gubici od prodaje materijala	784	343342
789	574	Manjkovi	784	343342
790	575	Rashodi po osnovu efekata ugovorene zaštite od rizika, koji ne ispunjavaju uslove da se iskažu u okviru ostalog sveobuhvatnog rezultata	784	343342
791	576	Rashodi po osnovu direktnih otpisa potraživanja	784	343342
792	577	Rashodi po osnovu rashodovanja zaliha materijala i robe	784	343342
793	579	Ostali nepomenuti rashodi	784	343342
794	58	RASHODI PO OSNOVU OBEZVREĐENJA IMOVINE KOJE SE VREDNUJE PO FER VREDNOSTI KROZ BILANS USPEHA	731	343342
795	580	Obezvređenje bioloških sredstava	794	343342
796	581	Obezvređenje nematerijalne imovine	794	343342
797	582	Obezvređenje nekretnina, postrojenja i opreme	794	343342
798	583	Obezvređenje dugoročnih finansijskih plasmana i drugih hartija od vrednosti raspoloživih za prodaju	794	343342
799	584	Obezvređenje zaliha materijala i robe	794	343342
800	585	Obezvređenje potraživanja i kratkoročnih finansijskih plasmana	794	343342
801	589	Obezvređenje ostale imovine	794	343342
802	59	GUBITAK POSLOVANJA KOJE SE OBUSTAVLJA, EFEKTI PROMENE RAČUNOVODSTVENE POLITIKE, ISPRAVKE GREŠAKA RANIJIH PERIODA I PRENOS RASHODA	731	343342
803	590	Gubitak poslovanja koje se obustavlja	802	343342
804	591	Rashodi po osnovu efekata promene računovodstvenih politika	802	343342
805	599	Prenos rashoda 	802	343342
807	60	PRIHODI OD PRODAJE ROBE	806	343342
808	600	Prihodi od prodaje robe matičnim i zavisnim pravnim licima na domaćem tržištu	807	343342
809	601	Prihodi od prodaje robe matičnim i zavisnim pravnim licima na inostranom tržištu	807	343342
810	602	Prihodi od prodaje robe ostalim povezanim pravnim licima na domaćem tržištu	807	343342
811	603	Prihodi od prodaje robe ostalim povezanim pravnim licima na inostranom tržištu	807	343342
812	61	PRIHODI OD PRODAJE PROIZVODA I USLUGA	806	343342
813	610	Prihodi od prodaje proizvoda i usluga matičnim i zavisnim pravnim licima na domaćem tržištu	812	343342
814	611	Prihodi od prodaje proizvoda i usluga matičnim i zavisnim pravnim licima na inostranom tržištu	812	343342
815	612	Prihodi od prodaje proizvoda i usluga ostalim povezanim pravnim licima na domaćem tržištu	812	343342
816	613	Prihodi od prodaje proizvoda i usluga ostalim povezanim pravnim licima na inostranom tržištu	812	343342
817	62	PRIHODI OD AKTIVIRANjA UČINAKA I ROBE	806	343342
818	620	Prihodi od aktiviranja ili potrošnje robe za sopstvene potrebe	817	343342
820	96	GOTOVI PROIZVODI	821	343342
819	967	Gotovi proizvodi	820	343342
822	621	Prihodi od aktiviranja ili potrošnje proizvoda i usluga za sopstvene potrebe	817	343342
823	63	PROMENA VREDNOSTI ZALIHA UČINAKA	806	343342
824	630	Povećanje vrednosti zaliha nedovršenih i gotovih proizvoda i nedovršenih usluga	823	343342
825	631	Smanjenje vrednosti zaliha nedovršenih i gotovih proizvoda i nedovršenih usluga	823	343342
826	64	­PRIHODI OD PREMIJA, SUBVENCIJA, DOTACIJA, DONACIJA I SL.	806	343342
827	640	Prihodi od premija, subvencija, dotacija, regresa, kompenzacija i povraćaja poreskih dažbina	826	343342
828	641	Prihodi po osnovu uslovljenih donacija	826	343342
829	65	DRUGI POSLOVNI PRIHODI	806	343342
830	650	Prihodi od zakupnina	829	343342
831	651	Prihodi od članarina	829	343342
832	652	Prihodi od tantijema i licencnih naknada	829	343342
833	659	Ostali poslovni prihodi	829	343342
834	66	FINANSIJSKI PRIHODI	806	343342
835	660	Finansijski prihodi od matičnih i zavisnih pravnih lica	834	343342
836	661	Finansijski prihodi od ostalih povezanih pravnih lica	834	343342
837	662	Prihodi od kamata (od trećih lica)	834	343342
838	663	Pozitivne kursne razlike (prema trećim licima)	834	343342
839	664	Prihodi po osnovu efekata valutne klauzule (prema trećim licima)	834	343342
840	665	Prihoda od učešća u dobitku pridruženih pravnih lica i zajedničkih poduhvata	834	343342
841	669	Ostali finansijski prihodi	834	343342
842	67	OSTALI PRIHODI	806	343342
843	670	Dobici od prodaje nematerijalne imovine, nekretnina, postrojenja i opreme	842	343342
844	671	Dobici od prodaje bioloških sredstava	842	343342
845	672	Dobici od prodaje učešća i dugoročnih hartija od vrednosti	842	343342
846	673	Dobici od prodaje materijala	842	343342
847	674	Viškovi	842	343342
848	675	Naplaćena otpisana potraživanja	842	343342
849	676	Prihodi po osnovu efekata ugovorene zaštite od rizika koji ne ispunjavaju uslove da se iskažu u okviru ostalog sveobuhvatnog rezultata	842	343342
850	677	Prihodi od smanjenja obaveza	842	343342
851	678	Prihodi od ukidanja dugoročnih i kratkoročnih rezervisanja	842	343342
852	679	Ostali nepomenuti prihodi	842	343342
853	68	PRIHODI OD USKLAĐIVANjA VREDNOSTI IMOVINE	806	343342
854	680	Prihodi od usklađivanja vrednosti bioloških sredstava	853	343342
955	153	Plaćeni avansi za robu u inostranstvu	568	343342
855	681	Prihodi od usklađivanja vrednosti nematerijalne imovine	853	343342
856	682	Prihodi od usklađivanja vrednosti nekretnina, postrojenja i opreme	853	343342
857	683	Prihodi od usklađivanja vrednosti dugoročnih finansijskih plasmana i hartija od vrednosti raspoloživih za prodaju	853	343342
858	684	Prihodi od usklađivanja vrednosti zaliha	853	343342
859	685	Prihodi od usklađivanja vrednosti potraživanja i kratkoročnih finansijskih plasmana	853	343342
860	689	Prihodi od usklađivanja vrednosti ostale imovine	853	343342
861	69	­DOBITAK POSLOVANjA KOJE SE OBUSTAVLJA, EFEKTI PROMENE RAČUNOVODSTVENE POLITIKE, ISPRAVKE GREŠAKA RANIJIH PERIODA I PRENOS PRIHODA	806	343342
862	690	Dobitak poslovanja koje se obustavlja	861	343342
863	691	Prihodi od efekata promene računovodstvenih politika	861	343342
864	699	Prenos prihoda	861	343342
866	70	OTVARANjE GLAVNE KNjIGE	865	343342
867	700	Otvaranje glavne knjige	866	343342
868	71	ZAKLjUČAK RAČUNA USPEHA	865	343342
869	710	Rashodi i prihodi	868	343342
870	711	Dobitak i gubitak poslovanja koje se obustavlja	868	343342
871	712	Prenos ukupnog rezultata	868	343342
872	72	RAČUN DOBITKA I GUBITKA	865	343342
873	720	Dobitak ili gubitak	872	343342
874	721	Poreski rashod perioda	872	343342
875	722	Odloženi poreski rashodi i prihodi perioda	872	343342
876	723	Lična primanja poslodavca	872	343342
877	724	Prenos dobitka ili gubitka	872	343342
878	73	ZAKLjUČAK RAČUNA STANjA	865	343342
879	730	Izravnanje računa stanja	878	343342
880	74	SLOBODNA GRUPA	865	343342
882	88	VANBILANSNA AKTIVA	881	343342
883	89	VANBILANSNA PASIVA	881	343342
884	90	RAČUNI ODNOSA S FINANSIJ­SKIM KNjIGOVODSTVOM	821	343342
885	900	Račun za preuzimanje zaliha	884	343342
886	901	Račun za preuzimanje nabavke materijala i robe	884	343342
887	902	Račun za preuzimanje troškova	884	343342
888	903	Račun za preuzimanje prihoda	884	343342
889	91	MATERIJAL I ROBA	821	343342
891	911	­Roba	889	343342
892	912	Proizvodi i roba u prodavnicama pro­izvođača	889	343342
893	92	­RAČUNI MESTA TROŠKOVA NA­BAVKE, TEHNIČKE UPRAVE I PO­MOĆNIH DELATNOSTI	821	343342
894	93	RAČUNI GLAVNIH PROIZVODNIH MESTA TROŠKOVA	821	343342
895	94	RAČUNI MESTA TROŠKOVA UPRA­VE, PRODAJE I SLIČNIH AKTIV­NOSTI	821	343342
896	95	NOSIOCI TROŠKOVA 	821	343342
897	950	Nosioci troškova	896	343342
898	951	Nosioci troškova	896	343342
899	952	Nosioci troškova	896	343342
900	953	Nosioci troškova	896	343342
901	954	Nosioci troškova	896	343342
902	955	Nosioci troškova	896	343342
903	956	Nosioci troškova	896	343342
904	957	Nosioci troškova	896	343342
905	958	Poluproizvodi sopstvene proizvodnje	896	343342
906	959	Odstupanja u troškovima nosioca troškova	896	343342
907	961	Gotovi proizvodi	820	343342
908	962	Gotovi proizvodi	820	343342
909	963	Gotovi proizvodi	820	343342
910	964	Gotovi proizvodi	820	343342
911	965	Gotovi proizvodi	820	343342
912	966	Gotovi proizvodi	820	343342
913	968	Gotovi proizvodi	820	343342
914	969	Odstupanja u troškovima gotovih proizvoda	820	343342
915	97	SLOBODNA GRUPA	821	343342
916	98	RASHODI I PRIHODI	821	343342
917	980	Troškovi prodatih proizvoda i usluga	916	343342
918	981	Nabavna vrednost prodate robe	916	343342
919	982	Troškovi perioda	916	343342
920	983	Otpisi, manjkovi i viškovi zaliha učinaka	916	343342
921	985	Slobodan račun	916	343342
922	986	Prihodi po osnovu proizvoda i usluga	916	343342
923	987	Prihodi po osnovu robe 	916	343342
924	988	Slobodan račun	916	343342
925	989	Drugi prihodi	916	343342
926	99	RAČUNI DOBITKA, GUBITKA I ZAKLjUČKA	821	343342
927	990	Poslovni dobitak i gubitak	926	343342
928	991	Gubitak i dobitak po osnovu prodaje materijala	926	343342
929	992	Manjkovi materijala i robe	926	343342
930	993	Otpisi materijala i robe	926	343342
931	994	Viškovi materijala i robe	926	343342
932	999	Zaključak obračuna troškova i učinaka	926	343342
933	013	Gudvil	511	343342
934	019	Ispravka vrednosti nematerijalne imovine	511	343342
935	04	DUGOROČNI FINANSIJSKI PLASMANI	507	343342
936	040	Učešće u kapitalu zavisnih pravnih lica	935	343342
937	043	Dugoročni plasmani matičnim, zavisnim i ostalim povezanim pravnim licima u zemlji	935	343342
938	044	Dugoročni plasmani matičnim, zavisnim i ostalim povezanim pravnim licima u inostranstvu	935	343342
939	045	Dugoročni plasmani u zemlji i inostranstvu	935	343342
940	047	Otkupljene sopstvene akcije i otkupljeni sopstveni udeli	935	343342
941	048	Ostali dugoročni finansijski plasmani	935	343342
942	049	Ispravka vrednosti dugoročnih finansijskih plasmana	935	343342
943	05	DUGOROČNA POTRAŽIVANJA	507	343342
944	050	Potraživanja od matičnih i zavisnih pravnih lica	943	343342
945	051	Potraživanja od ostalih povezanih lica	943	343342
946	052	Potraživanja po osnovu prodaje na robni kredit	943	343342
947	053	Potraživanja za prodaju po ugovorima o finansijskom lizingu	943	343342
948	054	Potraživanja po osnovu jemstva	943	343342
949	055	Sporna i sumnjiva potraživanja	943	343342
950	056	Ostala dugoročna potraživanja	943	343342
951	059	Ispravka vrednosti dugoročnih potraživanja	943	343342
952	104	Materijal, rezervni delovi, alat i inventar u obradi, doradi i manipulaciji	537	343342
953	151	Plaćeni avansi za materijal, rezervne delove i inventar u inostranstvu	568	343342
954	152	Plaćeni avansi za robu u zemlji	568	343342
956	154	Plaćeni avansi za usluge u zemlji	568	343342
957	155	Plaćeni avansi za usluge u inostranstvu	568	343342
958	204	Kupci u zemlji	572	343342
959	205	Kupci u inostranstvu	572	343342
960	206	Ostala potraživanja po osnovu prodaje	572	343342
961	225	Potraživanja za naknade zarada koje se refundiraju	584	343342
962	226	Potraživanja po osnovu naknada šteta	584	343342
963	278	PDV nadoknada isplaćena poljoprivrednicima	613	343342
964	306	Emisiona premija	630	343342
965	332	Dobici ili gubici po osnovu ulaganja u vlasničke instrumente kapitala	644	343342
966	333	Dobici ili gubici po osnovu udela u ostalom sveobuhvatnom dobitku ilio gubitku pridruženih društava	644	343342
967	334	Dobici ili gubici po osnovu preračuna finansijskih izveštaja inostranog poslovanja	644	343342
968	335	Dobici ili gubici od instrumenat zaštite neto ulaganja u inostrano poslovanje	644	343342
969	336	Dobici ili gubici po osnovu instrumenata zaštite rizika (hedžinga) novčanog toka	644	343342
970	337	Dobici ili gubici po osnovu hartija od vrednosti raspoloživih za prodaju	644	343342
971	405	Rezervisanja za troškove sudskih sporova	654	343342
972	416	Obaveze po osnovu finansijskog lizinga	661	343342
973	435	Dobavljači u zemlji	679	343342
974	436	Dobavljači u inostranstvu	679	343342
975	467	Obaveze za kratkoročna rezervisanja	699	343342
976	503	Nabavna arednost ostalih stalnih sredstava namenjenih prodaji	732	343342
977	512	Troškovi ostalog materijala (režinskog)	736	343342
978	514	Troškovi rezervnih delova	736	343342
979	515	Troškovi jednokratnog otpisa alata i inventara	736	343342
980	566	Rashodi po osnovu efeklata ugovorene zaštite od rizika, koji ne ispunjavaju uslove da se iskažu u okviru ostalog sveobuhvatnog dobitka	776	343342
981	592	Rashodi po osnovu ispravki grešaka iz ranijih godina koje nisu materijalno značajne	802	343342
982	604	Prihodi od prodaje robe na domaćem tržištu	807	343342
983	605	Prihodi od prodaje robe na inostranom tržištu	807	343342
984	614	Prihodi od prodaje proizvoda i usluga na domaćem tržištu	812	343342
985	615	Prihodi od prodaje proizvoda i usluga na inostranom tržištu	812	343342
986	692	Prihodi po osnovu ispravki grešaka iz ranijih godina koje nisu materijalno značajne	861	343342
987	880	Tuđa sredstva uzeta u operativni lizing (zakup)	882	343342
988	881	Preuzeti proizvodi i roba za zajedničko poslovanje	882	343342
989	882	Roba uzeta u komision i konsignaciju	882	343342
990	883	Materijal i roba primljeni na obradu i doradu	882	343342
991	884	Data jemstva, garancije i druga prava	882	343342
992	885	Hartije od vrednosti koje su van prometa	882	343342
993	889	Imovina kod drugih subjekata	882	343342
994	890	Obaveze za sredstva uzeta u operativni lizing (zakup)	883	343342
995	891	Obaveze za preuzete proizvode i robu za zajedničko poslovanje	883	343342
996	892	Obaveze za robu uzetu u komision i konsignaciju	883	343342
997	893	Obaveze za materijal i robu primljenu na obradu i doradu	883	343342
998	894	Obaveze za data jemstva, garancije i druga prava	883	343342
999	895	Obaveze za hartije od vrednosti koje su van prometa	883	343342
1000	046	Hartije od vrednosti koje se drže do dospeća	935	343342
1001	899	Obaveze za imovinu kod drugih subjekata	883	343342
1002	0000	Upisane a neuplaćene obične akcije	509	343342
1003	0001	Upisane a neuplaćene preferencijalne akcije	509	343342
1004	0010	Upisani a neuplaćeni udeli i ulozi člana A	510	343342
1005	0011	Upisani a neuplaćeni udeli i ulozi člana B	510	343342
1006	0012	Upisani a neuplaćeni udeli i uloz člana C	510	343342
1007	0100	Ulaganja u razvoj tržišta	512	343342
1008	0101	Ulaganja u razvoj tehnologije	512	343342
1009	0102	Ulaganja u razvoj proizvoda	512	343342
1010	0103	Ulaganja u razvoj usluga	512	343342
1011	0104	Ostali izdaci u razvoj	512	343342
1012	0110	Koncesije	513	343342
1013	0111	Patenti	513	343342
1014	0112	Licence	513	343342
1015	0113	Robne marke	513	343342
1016	0114	Uslužne marke	513	343342
1017	0115	Pravo na industrijski uzorak, žig, model, zaštitni znak	513	343342
1018	0116	Druga slična prava	513	343342
1019	0120	Sistemski softver	514	343342
1020	0121	Aplikativni softver	514	343342
1021	0122	Ostala prava	514	343342
1022	0130	Gudvil po osnovu stečene /pripojene neto imovine drugog pravnog lica	933	343342
1023	0131	Gudvil po osnovu kupovine akcija i udela u drugom pravnom licu	933	343342
1024	0140	Pravo korišćenja gradskog građevinskog zemljišta	515	343342
1025	0141	Ostala nematerijalna imovina	515	343342
1026	0150	Ulaganja u razvoj u pripremi	516	343342
1027	0151	Koncesije, patenti, licence, robne i uslužne marke u pripremi	516	343342
1028	0152	Softver u pripremi	516	343342
1029	0153	Interno generisana nematerijalna imovina u pripremi	516	343342
1030	0154	Ostala nematerijalna imovina u pripremi	516	343342
1031	0160	Avansi za nematerijalnu imovinu u dinarima	517	343342
1032	0161	Avansi za nematerijalnu imovinu u evrima	517	343342
1033	0162	Avansi za nematerijalnu imovinu u dolarima	517	343342
1034	0163	Avansi za nematerijalnu imovinu sa valutnom klauzulom	517	343342
1035	0190	Ispravka vrednosti ulaganja u razvoj	934	343342
1036	0191	Obezvređenje ulaganja u razvoja	934	343342
1037	0192	Ispravka vrednosti koncesija, patenata, licenci, robnih i uslužnih marki	934	343342
1038	0193	Obezvređenje koncesija, paatenata, licenci, robnih i uslužnih marki	934	343342
1039	0194	Ispravka vrednosti softvera i ostalih prava	934	343342
1040	0195	Obezvređenje softvera i ostalih prava	934	343342
1041	0196	Obezvređenje gudvila	934	343342
1042	0197	Ispravka vrednosti ostale nematerijalne imovine	934	343342
1043	0198	Obezvređenje ostale nematerijalne imovine	934	343342
1044	0199	Obezvređenje nematerijalne imovine u pripremi	934	343342
1045	01910	Ispravka vrednosti avansa za nematerijalnu imovinu	1036	343342
1046	01911	Obezvređenje avansa za nematerijalnu imovinu	1036	343342
1047	0200	Poljoprivredno zemljište	519	343342
1048	0201	Zemljište pod šumom	519	343342
1049	0202	Zemljišta pod višegodišnjim zasadima	519	343342
1050	0203	Nezasađeno i neobrađeno zemljište	519	343342
1051	0204	Rudarsko eksploataciono zemljište	519	343342
1052	0205	Zemljište van upotrebe	519	343342
1053	0206	Ostalo zemljiište	519	343342
1054	0210	Gradsko gađevinsko zemljiše	520	343342
1055	0211	Građevinsko zemljište van granica gradskog građevinskog zemljišta	520	343342
1056	0220	Poslovni prostor (građevinski objekti za administraciju i upravu)	521	343342
1057	0221	Prodajni i izložbeni prostor	521	343342
1058	0222	Magacini i skladišta	521	343342
1059	0223	Fabrike i proizvodne hale	521	343342
1060	0224	Ostali građevinski objekti u funkciji	521	343342
1061	0225	Gađevinski objekti van upotrebe	521	343342
1062	0230	Tehnička postrojenja	522	343342
1063	0231	Proizvodne linije	522	343342
1064	0232	Oprema za proizvodnju	522	343342
1065	0233	Kancelarijska oprema	522	343342
1066	0234	Transportna sredstva	522	343342
1067	0235	Alat i inventar sa kalkulativnim otpisom	522	343342
1068	0236	Postrojenja i oprema van upotrebe	522	343342
1069	0240	Investicione nekretnine-zemljišta	523	343342
1070	0241	Investicione nekretnine-zgrade	523	343342
1071	0242	Investicione nekretnine-deo zgrade	523	343342
1072	0243	Investicione nekretnine-magacini i skladišta	523	343342
1073	0250	Knjige i stručne publikacije	524	343342
1074	0251	Spomenici kulture	524	343342
1075	0252	Istorijski spomenici	524	343342
1076	0253	Dela likovne, vajarske, filmske i drugih umetnosti	524	343342
1077	0254	Dela muzejske vrednosti	524	343342
1078	0255	Povratna ambalaža s rokom upotrebe više od jedne godine	524	343342
1079	0256	Ostale nekretnine, postrojenja i oprema	524	343342
1080	0260	Građevinski objekti u pripremi	525	343342
1081	0261	Postrojenja i oprema u pripremi	525	343342
1082	0262	Investicione nekretnine u pripremi	525	343342
1083	0263	Ostale nekretnine, postrojenja i oprema u pripremi	525	343342
1084	0270	Ulaganja na tuđim nekretninama	526	343342
1085	0271	Ulaganja na tuđim postrojenjima	526	343342
1086	0272	Ulaganja na tuđoj opremi	526	343342
1087	0280	Avansi za nekretnine	527	343342
1088	0281	Avansi za postrojenja	527	343342
1089	0282	Avansi za opremu	527	343342
1090	0283	Avansi za investicione nekretnine	527	343342
1091	0290	Obezvređenje zemljišta	528	343342
1092	0291	Obezvređenje građevinskog zemljišta	528	343342
1093	0292	Ispravka vrednosti građevinskih objekata	528	343342
1094	0293	Obezvređenje građevinskih objekata	528	343342
1095	0294	Ispravka vrednosti postrojenja i opreme	528	343342
1096	0295	Obezvređenje postrojenja i opreme	528	343342
1097	0296	Ispravka vrednosti investicionih nekretnina	528	343342
1098	0297	Obezvređenje investicionih nekretnina	528	343342
1099	0298	Ispravka vrednosti ostalih nekretnina, postrojenja i opreme	528	343342
1100	0299	Obezvređenje ostalih nekretnina, postrojenja i opreme	528	343342
1101	02910	Obezvređenje nekretnina, postrojenja i opreme u pripremi	1092	343342
1102	02911	Isspravka vrednosti ulaganja na tuđim nekretninama, postrojenjima i opremi	1092	343342
1103	02912	Obezvređenje ulaganja na tuđim nekretninama, postrojenjima i opremi	1092	343342
1104	02913	Ispravka vrednosti za nekretnine, postrojenja i opremu	1092	343342
1105	0301	Listopadne šume	530	343342
1106	0302	Četinarske šume	530	343342
1107	0303	Mešovite šume	530	343342
1108	0304	Ostale šume	530	343342
1109	0310	Voćnjaci	531	343342
1110	0311	Vinogradi	531	343342
1111	0312	Hmeljnjaci	531	343342
1112	0313	Brzorastuće drveće	531	343342
1113	0314	Ukrasno i ostalo drveće	531	343342
1114	0315	Ostali višegodišnji zasadi	531	343342
1115	0320	Goveda	532	343342
1116	0321	Svinje	532	343342
1117	0322	Ovce i koze	532	343342
1118	0323	Živina (sa obrtom dužim od jedne godine)	532	343342
1119	0324	Konji	532	343342
1120	0325	Mazge, magarci i mule	532	343342
1121	0326	Pčelinja društva	532	343342
1122	0327	Ostalo osnovno stado	532	343342
1123	0370	Šume u pripremi	533	343342
1124	0371	Višegodišnji zasadi u pripremi	533	343342
1125	0372	Osnovno stado u pripremi	533	343342
1126	0380	Avansi za šume	534	343342
1127	0381	Avansi za višegodišnje zasade	534	343342
1128	0382	Avansi za osnovno stado	534	343342
1129	0390	Obezvređenje šuma	535	343342
1130	0391	Ispravka vrednosti višegodišnjih zasada	535	343342
1131	0392	Obezvređenje višegodišnjih zasada	535	343342
1132	0393	Ispravka vrednosti osnovnog stada	535	343342
1133	0394	Obezvređenje osnovnog stada	535	343342
1134	0400	Akcije zavisnih pravnih lica u zemlji	936	343342
1135	0401	Akcije zavisnih pravnih lica u inostranstvu	936	343342
1136	0402	Udeli u kapitalu zavisnih pravnih lica u zemlji	936	343342
1137	0403	Udeli u kapitalu zavisnih pravnih lica u inostranstvu	936	343342
1138	041	Učešća u kapitalu pridruženih pravnih lica i zajedničkim poduhvatima	935	343342
1139	0410	Akcije pridruženih pravnih lica u zemlji	1138	343342
1140	0411	Akcije pridruženih pravnih lica u inostranstvu	1138	343342
1141	0412	Udeli u kapitalu pridruženih pravnih lica u zemlji	1138	343342
1142	0413	Udeli u kapitalu pridruženih pravnih lica u inostranstvu	1138	343342
1143	0414	Zajednički poduhvati u zemlji	1138	343342
1144	0415	Zajednički poduhvati u inostranstvu	1138	343342
1145	042	Učešća u kapitalu ostalih pravnih lica i druge hartije od vrednosti raspoložive za prodaju	935	343342
1146	0420	Akcije ostalih pravnih lica u zemlji	1145	343342
1147	0421	Akcije ostalih pravnih lica u inostranstvu	1145	343342
1148	0422	Udeli u kapitalu ostalih pravnih lica u zemlji	1145	343342
1149	0423	Udeli u kapitalu ostalih pravnih lica u inostranstvu	1145	343342
1150	0424	Druge hartije od vrednosti raspoložive za prodaju	1145	343342
1151	0430	Dugoročni plasmani matičnim pravnim licima u dinarima	937	343342
1152	0431	Dugoročni plasmani matičnim pravnim licima sa vulutnom klauzulom	937	343342
1153	0432	Dugoročni plasmani zavisnim pravnim licima u dinarima	937	343342
1154	0433	Dugoročni plasmani zavisnim pravnim licima sa valutnom klauzulom	937	343342
1155	0434	Dugoročni plasmani ostalim povezanim pravnim licima u dinarima	937	343342
1156	0435	Dugoročni plasmani ostlaim povezanim pravnim licima sa valautnom klauzulom	937	343342
1157	0440	Dugoročni plasmani matičnim pravnim licima u inostranstvu	938	343342
1158	0441	Dugoročni plasmani zavisnim pravnim licima u inostranstvu	938	343342
1159	0442	Dugoročni plasmani ostalim pravnim licima u inostranstvu	938	343342
1160	0450	Dugoročni plasmani u zemlji i domaćoj valuti	939	343342
1161	0451	Dugoročni plasmani z u zemlji sa valutnom klauzulom	939	343342
1162	0452	Dugoročni plasmani u inostranstvu	939	343342
1163	0460	Obveznice u domaćoj valuti sa rokom dospeća preko godinu dana	1000	343342
1164	0461	Obveznice u stranoj valuti sa rokom dospeća preko godinu dana	1000	343342
1165	0462	Ostale hartije od vrednosti koje se drže do dospeća sa rokom dospeća preko godinu dana	1000	343342
1166	0470	Otkupljene sopstvene akcije	940	343342
1167	0471	Otkupljeni sopstveni udeli	940	343342
1168	0480	Dugoročni depoziti i kaucije	941	343342
1169	0481	Dugoročni zajmovi zaposlenima	941	343342
1170	0482	Dugoročni zajmovi ostalim fizičkim licima	941	343342
1171	0483	Ostali dugoročni finansijski plasmani	941	343342
1172	0490	Ispravka vrednosti učešća u kapitalu zavisnih pravnih lica	942	343342
1173	0491	Ispravka vrednosti učešća u kapitalu pridruženih subjekata i zajedničkim poduhvatima	942	343342
1174	0492	Ispravka vrednosti učešća u kapitalu ostalih pravnih lica i druge hartije od vrednosti raspoložive za prodaju	942	343342
1175	0493	Ispravka vrednosti dugoročnih plasmana matičnim, zavisnim i ostalim povezanim pravnim licima u zemlji	942	343342
1176	0494	Ispravka vrednosti dugoročnih plasmana matičnim, zavisnim i ostalim povezanim pravnim licima u inostranstvu	942	343342
1177	0495	Ispravka vrednosti dugoročnih plasmana u zemlji i inostranstvu	942	343342
1178	0496	Ispravka vrednosti hartija od vrednosti koje se drže do dospeća	942	343342
1179	0497	Ispravka vrednosti ostalih dugoročnih finansijskih plasmana	942	343342
1180	0500	Dugoročna potraživanja od matičnih pravnih lica u domaćoj valuti	944	343342
1181	0501	Dugoročna potraživanja od matičnih pravnih lica u stranoj valuti	944	343342
1182	0502	Dugoročna potraživanja od matičnih pravnih lica s valutnom klauzulom	944	343342
1183	0503	Dugoročna potraživanja od zavisnih pravnih lica u domaćoj valuti	944	343342
1184	0504	Dugoročna poraživanja od zavisnih pravnih lica u stranoj valuti	944	343342
1185	0505	Dugoročna potraživanja od zavisnih pravnih lica sa valutnom klauzulom	944	343342
1186	0510	Potraživanja od ostalih povezanih lica u domaćoj valuti	945	343342
1187	0511	Potraživanja od ostalih povezanih lica u stranoj valuti	945	343342
1188	0512	Potraživanja od ostalih povezanih lica sa valutnom klauzulom	945	343342
1189	0520	Potraživanja po osnovu prodaje na robni kredit u domaćoj valuti	946	343342
1190	0521	Potraživanja po osnovu prodaje na robni kredit u stranoj valuti	946	343342
1191	0522	Potraživanja po osnovu prodaje na robni kredit sa valutnom klauzulom	946	343342
1192	0530	Potraživanja za prodaju po ugovorima o finansijskom lizingu u domaćoj valuti	947	343342
1193	0531	Potraživanja za prodaju po ugovorima o finansijskom lizingu u stranoj valuti	947	343342
1194	0532	Potraživanja za prodaju po ugovorima o finansijskom lizingu sa valutnom klauzulom	947	343342
1195	0540	Potraživanja po osnovu jemstva u domaćoj valuti	948	343342
1196	0541	Potraživanja po osnovu jemstva u stranoj valuti	948	343342
1197	0542	Potraživanja po osnovu jemstva s valutnom klauzulom	948	343342
1198	0550	Sporna potraživanja u zemlji	949	343342
1199	0551	Sporna potraživanja u inostranstvu	949	343342
1200	0552	Sumnjiva potraživanja u zemlji	949	343342
1201	0553	Sumnjiva potraživanja u inostranstvu	949	343342
1202	0560	Ostala dugoročna potraživanja u domaćoj valuti	950	343342
1203	0561	Ostala dugoročna potraživanja u stranoj valuti	950	343342
1204	0562	Ostala dugoročna potraživanja sa valutnom klauzulom	950	343342
1205	0590	Ispravka vrednosti potraživanja od matičnih i zavisnih pravnih lica	951	343342
1206	0591	Ispravka vrednosti potraživanja od ostalih povezanih lica	951	343342
1207	0592	Ispravka vrednosti potraživanja po osnovu prodaje na robni kredit	951	343342
1208	0593	Ispravka vrednosti potraživanja za prodaju po ugovorima o finansijskom lizingu	951	343342
1209	0594	Ispravka vrednosti potraživanja po osnovu jemstva	951	343342
1210	0595	Ispravka vrednosti spornih i sumnjivih potraživanja	951	343342
1211	0596	Ispravka vrednosti ostalih dugoročnih potraživanja	951	343342
1212	1000	Obračun nabavne vrednosti materijala	538	343342
1213	1001	Obračun nabavne vrednosti rezervnih delova	538	343342
1214	1002	Obračun nabavne vrednosti alata i inventara	538	343342
1215	1010	Sirovine i osnovni materijal	539	343342
1216	1011	Pomoćni materijal	539	343342
1217	1012	Ostali materijal	539	343342
1218	1013	Gorivo i mazivo	539	343342
1219	1014	Materijal u doradi, obradi i manipulaciji	539	343342
1220	1015	Materijal za pakovanje (ambalaža sa jednokratnim otpisom)	539	343342
1221	1019	Odstupanje od planskih cena materijala	539	343342
1222	1030	Alat i inventar na zalihama	541	343342
1223	1031	Povratna ambalaža s rokom upotrebe do jedne godine	541	343342
1224	1032	Sitan inventar	541	343342
1225	1033	Auto gume	541	343342
1226	1039	Odstupanje od planskih cena alata i inventara	541	343342
1227	1040	Materijal u obradi, doradi i manimupalicij	952	343342
1228	1041	Rezervni delovi u obradi, doradi i manipulaciji	952	343342
1229	1042	Alat u obradi, doradi i manipulaciji	952	343342
1230	1043	Inventar u obradi, doradi i manipulaciji	952	343342
1231	1090	Ispravka vrednosti zaliha maaterijala	542	343342
1232	1091	Ispravka vrednosti zaliha rezervnih delova	542	343342
1233	1092	Ispravka vrednosti zaliha alata i inventara	542	343342
1234	1093	Ispravka vrednosti materijala, rezervnih delova, alata i inventara u obradi, doradi i manipulaciji	542	343342
1235	1100	Nedovršena proizvodnja	544	343342
1236	1101	Poluproiozvodi i delovi	544	343342
1237	1110	Nedovršene usluge na domaćem tržištu	545	343342
1238	1111	Nedovršene usluge na inostranom tržištu	545	343342
1239	1200	Gotovi proizvodi (po vrsti)	547	343342
1240	1300	Obračun neto fakturne vrednosti robe	549	343342
1241	1301	roškovi transporta, utovara, istovara i drugih izvršioca	549	343342
1242	1302	Troškovi sopstvenog ransporta, istovara i utovara	549	343342
1243	1303	Troškovi osiguranja robe u transportu	549	343342
1244	1304	Troškovi nastali pri uvozu obe obračunati od strane državnih organa (carine, obavezni pregledi i sl.)	549	343342
1245	1305	Bankarske provizije, troškovi akreditiva i kamat na uvoz robe	549	343342
1246	1306	Ostali zavisni troškovi nabavke robe	549	343342
1247	1309	Obračun nabavne vrednosti robe	549	343342
1248	1310	Roba u magacinu 1	550	343342
1249	1311	Roba u magacinu 2, itd.	550	343342
1250	1320	Roba u prometu na veliko u skladištu	551	343342
1251	1321	Roba u prometu na veliko u stovarištu	551	343342
1252	1328	Ukalkulisani porez na dodatu vrednost	551	343342
1253	1329	Ukalkulisana razlika u ceni	551	343342
1254	1330	Roba u komisionu	552	343342
1255	1331	Roba u konsignaciji	552	343342
1256	1332	Roba u skladištu i stovarištu kod drugih pravnih lica	552	343342
1257	1333	Roba kod ostalih pravnih lica	552	343342
1258	1340	Roba u prometu na malol	553	343342
1259	1348	Ukalkulisan porez na dodatu vrednost	553	343342
1260	1349	Ukalkulisana razlika u ceni	553	343342
1261	1350	Roba u obradi, doradi i manipulaciji u zemlji	554	343342
1262	1351	Roba u obradi, doradi i manipulaciji u inostranstvu	554	343342
1263	1360	Roba u tranzitu u zemlji	555	343342
1264	1361	Roba u tranzitu u inostranstvu	555	343342
1265	1370	Roba na putu u zemlji	556	343342
1266	1371	Roba na putu u inostranstvu	556	343342
1267	1391	Ispravka vrednosti robe u magaciju	557	343342
1268	1392	Ispravka vrednosti robe u prometu na veliko	557	343342
1269	1393	Ispravka vrednosti robe u skladištu, stovarištu i prodavnicama kod drugih pravnih lica	557	343342
1270	1394	Ispravka vrednosti robe u prometu na malo	557	343342
1271	1395	Ispravka vrednosti robe u obradi, doradi i manaipulaciji	557	343342
1272	1396	Ispravka vrednosti robe u tranzitu	557	343342
1273	1397	Ispravka varednosti robe na putu	557	343342
1274	1400	Nematerijalna ulaganja reklasifikovana iz stalne imovine	559	343342
1275	1411	Zemljište pribavljeno isključivo radi prodaje	560	343342
1276	1420	Graevinski objekti reklasifikovani iz stalne imovine	561	343342
1277	1421	Građevinski objekti pribavljeni isključivo radi prodaje	561	343342
1278	1430	Investicione nekretnine reklasififikovane iz stalne imovine  cele	562	343342
1279	1431	Investicione nekretnine reklasifikovane iz stalne imovine - delovi	562	343342
1280	1440	Ostale nekretnine reklasifikovane iz stalne imovine	563	343342
1281	1441	Ostale nekretnine pribavljene isključivo radi prodaje	563	343342
1282	1450	Postrojenja i oprema reklasifikovani iz stalne imovine	564	343342
1283	1451	Postrojenja i oprema pribavljeni isključivo radi prodaje	564	343342
1284	1460	Biološka sredstva reklasifikovana iz salne imovine	565	343342
1285	1461	Biološka sredstva pribavljena isključivo radi prodaje	565	343342
1286	1470	Nematerijalna ulaganja poslovanja koje se obustavlja	566	343342
1287	1471	Zemljište poslovanja koje se obustavlja	566	343342
1288	1472	Gađevinski objekti poslovanja koje se obustavlja	566	343342
1289	1473	Postrojenja i oprema poslovnja koje se obustavlja	566	343342
1290	1474	Biološka sredstva poslovanja koje se obustavlja	566	343342
1291	1490	Ispravka vrednosti stalnih sredstava namenjenih prodaji	567	343342
1292	1491	Ispravka varednosti sredstava obustavljenog poslovanja namenjenih prodaji	567	343342
1293	1500	Plaćeni avansi za materijal, rezervne delove i inventar povezanim pravnim licima u domaćoj valuti	569	343342
1294	1501	Plaćeni avansi za materijal, rezervne delove i inventar povezanim pravnim licima sa valutnom klauzulom	569	343342
1295	1502	Plaćeni avansi za maerijal, rezervne delove i inventar u domaćoj valuti	569	343342
1296	1503	Plaćeni avansi za materijal, rezervne delove i inventar s valutnom klauzulom	569	343342
1297	1510	Plaćeni avansi za materijal, rezervne delove i inventar povezanim pravnim licima u inostranstvu	953	343342
1298	1511	Plaćeni avansi za materijal u inostranstvu	953	343342
1299	1512	Plaćeni avansi za rezervne delovi u inostranstvu	953	343342
1300	1513	Plaćeni avansi za inventar u inostranstvu	953	343342
1301	1520	Plaćeni avansi za robu povezanim pravnim llicima u zemlji u domaćoj valuti	954	343342
1302	1521	Plaćeni avansi za robu povezanim pravnim llicima u zemlji sa valutnom klauzulom	954	343342
1303	1522	Plaćeni avansi za robu u zemlji u domaćoj valuti	954	343342
1304	1523	Plaćeni avansi za robu u zemlji sa valutnom klauzulom	954	343342
1305	1530	Plaćeni avansi za robu povezanim pravnim licima u inostranstvu	955	343342
1306	1531	Plaćeni avansi za robu u inostranstvu	955	343342
1307	1540	Plaćeni avansi za usluge povezanim pravnim licima u zemlji u domaćoj valuti	956	343342
1308	1541	Plaćeni avansi za usluge povezanim pravnim llicima u zemlji s valutnom klauzulom	956	343342
1309	1542	Plaćeni avansi za usluge u zemlji u domaćoj valuti	956	343342
1310	1543	Plaćeni avansi za usluge u zemlji sa valutnom klauzulom	956	343342
1311	1550	Plaćeni avansi za usluge povezanim pravnim licima u inostranstvu	957	343342
1312	1551	Plaćeni avansi za usluge u inostranstvu	957	343342
1313	1590	Ispravka vrednosti plaćenih avansa za materijal, rezervne delove i inventar u zemlji	570	343342
1314	1591	Ispravka vrednosti plaćenih avansa za materijal, rezervne delove i inventar u inostranstvu	570	343342
1315	1592	Ispravka vrednosti plaćenih avansa za robu u zemlji	570	343342
1316	1593	Ispravka vrednosti plaćenih avansa za robu u inostranstvu	570	343342
1317	1594	Ispravka vrednosti plaćenih avansa za usluge u zemlji	570	343342
1318	1595	Ispravka vrednosti plaćenih avansa za usluge u inostranstvu	570	343342
1319	2000	Kupci u zemlji-matična i zavisna pravna lica za proizvode	573	343342
1320	2001	Kupci u zemlji-matična i zavisna pravna lica za robu	573	343342
1321	2002	Kupci zemlji-matična i zavisna pravna lica za usluge	573	343342
1322	2003	Kupci u zemlji-matična i zavisna pravna lica za nematirjalna ulaganja, nekretnine, postrojenja, opremu i biološka sredstva	573	343342
1323	2010	Kupci u inostranstvu - matična i zavisna pravna lica za proizvode	574	343342
1324	2011	Kupci u inostranstvu - matična i zavisna pravna lica za robu	574	343342
1325	2012	Kupci u inostransvu - matična i zavisna pravna lica za usluge	574	343342
1326	2013	Kupci u inostranstvu - matična i zavisna pravna lica za nematerijalna ulaganja, nekretnine, postrojenja, opremu i biološka sredstva	574	343342
1327	2020	Kupci u zemlji - ostala povezana pravna lica za proizvode	575	343342
1328	2021	Kupci u zemlji - ostala povezana pravna lica za robu	575	343342
1329	2022	Kupci u zemlji - ostala povezana pravna lica za usluge	575	343342
1330	2023	Kupci u zemlji - ostala povezana pravna lica za nematerijalna ulaganja, nekretnine, postrojenja, opremu i biološka sredstva	575	343342
1331	2024	Kupci u zemlji - ostala povezana pravna lica po osnovu komisione i konsignacione prodaje	575	343342
1332	2030	Kupci u inostranstvu - ostala povezana pravna lica za proizvode	576	343342
1333	2031	Kupci u inostranstvu - ostala povezana pravna lica za robu	576	343342
1334	2032	Kupci u inostranstvu - ostala povezana pravna lica za usluge	576	343342
1335	2033	Kupci u inostranstvu - ostala povezana pravna lica za nematerijalna ulaganja, nekretnine, postrojenja, opremu i biološka sredstva	576	343342
1336	2034	Kupci u inostranstvu - ostala povezana pravna lica po osnovu komisione i konsignacione prodaje	576	343342
1337	2040	Kupci u zemlji za proizvode	958	343342
1338	2041	Kupci u zemlji za robu	958	343342
1339	2042	Kupci u zemlji za usluge	958	343342
1340	2043	Kupci u zemlji za nematerijalna ulaganja, nekretnine, postrojenja, opremu i biološka sredstva	958	343342
1341	2044	Kupci u zemlji po osnovu komisione i konsignacione prodaje	958	343342
1342	2050	Kupci u inostranstvu za proizvode	959	343342
1343	2051	Kupci u inostranstvu za robu	959	343342
1344	2052	Kupci u inostranstvu za usluge	959	343342
1345	2053	Kupci u inostranstvu za nematerijalna ulaganja, nekretnine, postrojenja, opremu i biološka sredstva	959	343342
1346	2054	Kupci u inostranstvu po osnovu komisione i konsignacione prodaje	959	343342
1347	2060	Ostala potraživanja po osnovu prodaje	960	343342
1348	2090	Ispravka vrednosti potraživanja od kupaca u zemlji - matična i zavisna pravna lica	577	343342
1349	2091	Ispravka vrdnosti potraživanja od kupaca u inostranstvu-matična i zavisna pravna lica	577	343342
1350	2092	Ispravka varednosti potraživanja od kupaca u zemlji-ostala povezana pravna lica	577	343342
1351	2093	Ispravka varednosti potraživanja od kupaca u inostranstvu-ostala povezana pravna lica	577	343342
1352	2094	Ispravka varednosti potraživanja od kupaca u zemlji	577	343342
1353	2095	Ispravka vrednosti potraživanja od kupaca u inostranstvu	577	343342
1354	2100	Potraživanja od izvoznika za sopstvene proizvode i robu	579	343342
1355	2101	Potraživanja od izvoznika za avans za uslugu izvoza	579	343342
1356	2110	Potraživanja za uvezenu robu za tuđ račun	580	343342
1357	2111	Potraživanja za troškove uvoza za tuđ račun	580	343342
1358	2112	Potraživanja za uvozničku proviziju	580	343342
1359	2120	Potraživanja iz komisione prodaje	581	343342
1360	2121	Potraživanja iz konsignacione prodaje	581	343342
1361	2180	Potraživanja iz zajedničkog poslovanja	582	343342
1362	2181	Druga potraživanja iz specifičnih poslova	582	343342
1363	2190	Ispravka vrednosti potraživanja od izvoznika	583	343342
1364	2191	Ispravka vrednosti potraživanja po osnovu uvoza za tuđi račun	583	343342
1365	2192	Ispravka varednosti potraživanja po osnovu komisione i konsignacione prodaje	583	343342
1366	2193	Ispravka varednosti ostalih potraživanja iz specifičnih poslova	583	343342
1367	2200	Potraživanja za kamatu od povezanih pravnih lica	585	343342
1368	2201	Potraživanja za ugovorenu kamatu u dinarima	585	343342
1369	2202	Potraživanja za zateznu kamatu u dinarima	585	343342
1370	2203	Potraživanja za ugovorenu kamatu u stranoj valuti	585	343342
1371	2204	Potraživanja za zateznu kamatu u stranoj valuti	585	343342
1372	2205	Potraživanja za dividende od povezanih pravnih lica	585	343342
1373	2206	Potraživanja za dividende u dinarima	585	343342
1374	2207	Potraživanja za dividende u stranoj valuti	585	343342
1375	2208	Potraživanja za učešće u dobiti u dinarima	585	343342
1376	2209	Potraživanja za učešće u dobiti u stranoj valuti	585	343342
1377	2210	Potraživanja po osnovu akontacija za službena putovanja u dinarima	586	343342
1378	2211	Potraživanja po osnovu akontacija za službena putovanja u stranoj valuti	586	343342
1379	2212	Potraživanja od zaposlenih za manjkove	586	343342
1380	2213	Potraživanja od zaposlenih po osnovu odobrenog kratkoročnog zajma	586	343342
1381	2214	Potraživanja od zaposlenih po osnovu naknada šeta	586	343342
1382	2215	Ostala potraživanja od zaposlenih	586	343342
1383	2220	Potraživanja od subvencije	587	343342
1384	2221	Potraživanja za premije	587	343342
1385	2222	Potraživanja za dotacije	587	343342
1386	2223	Potraživanja za stimulacije	587	343342
1387	2224	Potraživanja za regres	587	343342
1388	2225	Potraživanja za carine i druge dažbine	587	343342
1389	2226	Ostala potraživanja od državnih organa i organizacija	587	343342
1390	2230	Potraživanja za više plaćen porez na dobit pravnih lica	588	343342
1391	2231	Potraživanja za više plaćen porez na prihode od samostalne delatnosti	588	343342
1392	2240	Potraživanja po osnovu preplaćenih poreza na imovinu	589	343342
1393	2241	Potraživanja po osnovu preplaćenih doprinosa	589	343342
1394	2242	Potraživanja po osnovu preplaćene carine i drugih uvoznih dažbina	589	343342
1395	2243	Potraživanja po osnovu preplaćenih ostlaih poreza i doprinosa	589	343342
1396	2250	Potraživanja za naknade zarada koje se refundiraju trudnicama	961	343342
1397	2251	Potraživanja za naknade zarada koje se refundiraju porodiljama	961	343342
1398	2252	Potraživanja za naknade zarada koje se refundiraju za bolovanje	961	343342
1399	2253	Potraživanje za naknadu zarade koje se refundiraju za vreme vojne vežbe	961	343342
1400	2254	Potraživanja za ostale naknade zarada koje se refundiraju	961	343342
1401	2260	Potraživanja po osnovu naknada đeta od osiguranja	962	343342
1402	2261	Potraživanja po osnovu naknade šteta od poslovnih partnera	962	343342
1403	2262	Potraživanja po osnovu ostalih naknada šteta	962	343342
1404	2280	Poraživanja za izvršene isplate na osnovu avala i garancija	590	343342
1405	2281	Potraživanja po osnovu datih depozita	590	343342
1406	2282	Druga potraživanja	590	343342
1407	2290	Ispravka vrednosti potraživanja za kamatu i dividende	591	343342
1408	2291	Ispravka varednosti potraživanja od zaposlenih	591	343342
1409	2292	Ispravka vrednosti potraživanja od državnih organa i organizacija	591	343342
1410	2293	Ispravka vrednosti potraživanja za više plaćeni porez na dobitak	591	343342
1411	2294	Ispravka vrednosti potraživanja po osnovu preplaćenih ostalih poreza i doprinosa	591	343342
1412	2295	Ispravka vrednosti potraživanja za naknade zarada koje se refundiraju	591	343342
1413	2296	Ispravka vrednosti potraživanja po osnovu naknada šteta	591	343342
1414	2297	Ispravka vrednosti ostalih kratkoročnih potraživanja	591	343342
1415	2300	Kratkorični krediti, zajmovi i plasmani u matična pravna lica u zemlji	593	343342
1416	2301	Krakoročni krediti, zajmovi i plasmani u matična pravna lica u inostranstvu	593	343342
1417	2302	Kratkoročni krediti, zajmovi i plasmani u zavisna pravna lica u zemlji	593	343342
1418	2303	Kratkoročni krediti, zajmovi i plasmani u zavisna pravna lica u inostranstvu	593	343342
1419	2310	Kratkoročni krediti, zajmovi i plasmani u ostala povezana pravna lica u zemlji u dinarima	594	343342
1420	2311	Kratkoročni krediti, zajmovi i plasmani u ostala povezana pravna lica u zemlji sa valutnom klauzulom	594	343342
1421	2312	Kratkoročni krediti, zajmovi i plasmani u ostala povezana pravna lica u inostranstvu	594	343342
1422	2320	Kratkoro;ni robni krediti dati pravnim licima	595	343342
1423	2321	Kratkoro;ni robni krediti dati fizičkim licima	595	343342
1424	2322	Kratkoročni potrošački krediti	595	343342
1425	2323	Zajmovi u domaćoj valuti	595	343342
1426	2324	Zajmovi sa valutnom klauzulom	595	343342
1427	2325	Ostali kratkoročni krediti i zajmovi	595	343342
1428	2330	Kratkoročni robni krediti u inostranstvu	596	343342
1429	2331	Kratkoročni potrošački krediti u inostransvu	596	343342
1430	2332	Zajmovi u inostranstvu	596	343342
1431	2333	Ostali kratkoročni krediti i zajmovi u inostranstvu	596	343342
1432	2340	Deo dugoročnih finansijskih plasmana u matičnim, zavisnim i ostalim povezanim pravnim licima koji dospeva do jedne godine	597	343342
1433	2341	Deo dugoročnih finansijskih plasmana u zemlji koji dospeva do jedne godine	597	343342
1434	2342	Deo dugoročnih finansijskih plasmana u inostranstvu koji dospeva do jedne godine	597	343342
1435	2350	Obveznice koje dospevaju do jedne godine u domaćoj valuti	598	343342
1436	2351	Obveznice koje dospevaju do jedne godine u stranoj valuti	598	343342
1437	2352	Ostale hartije od vrednosti koje se drže do dospeća - deo koji dospeva do jedne godine	598	343342
1438	2360	Akcije	599	343342
1439	2361	Obveznice	599	343342
1440	2362	Blagajnički zapisi	599	343342
1441	2363	Komercijalni zapisi	599	343342
1442	2364	Državni zapisi	599	343342
1443	2365	Sertifikati o depozitu	599	343342
1444	2366	Ostale hartije od vrednosti koje se vrednuju po fer vrednosti kroz bilans uspeha	599	343342
1445	2370	Otkupljene sopstvene akcije namenjene prodaji ili poništavanju	600	343342
1446	2371	Otkupljeni sopstveni udeli namenjeni prodaji ili poništavanju	600	343342
1447	2380	Potraživnaja po menicama kao instrumentima plaćanja	601	343342
1448	2381	Potraživanja po kamatama budućeg perioda sadržanim u meničnoj vrednosti	601	343342
1449	2382	Kratkoročno oročena dinarska sredstva	601	343342
1450	2383	Kratkoročno oročena devizna sredstva	601	343342
1451	2384	Ostali kratkoročni finansijski plasmani u zemlji	601	343342
1452	2385	Ostali kratkoročni finansijski plasmani u inostranstvu	601	343342
1453	2390	Ispravka vrednosti kratkoročnih kredita i plasmana u matična i zavisna pravna lica	602	343342
1454	2391	Ispravka vrednosti kratkoročnih kredita i plasmana u ostala povezana pravna lica	602	343342
1455	2392	Ispravka vrednosti kratkoročnih kredita i zajmova u zemlji	602	343342
1456	2393	Ispravka vrednosti kratkoročnih kredita i zajmova u inostranstvu	602	343342
1457	2394	Ispravka vrednosti dela dugoročnih finansijskih plasmana koji dospeva do jedne godine	602	343342
1458	2395	Ispravka vrednosti hartija od vrednosti koje se drže do dospeća - deo koji dospeva do jedne godine	602	343342
1459	2396	Ispravka vrednosti ostalih kratkoročnih finansijskih plasmana	602	343342
1460	2400	Čekovi	604	343342
1461	2401	Druge neposredno unovčive hartije od vrednosti	604	343342
1462	2410	Tekući računi (analitika po bankama i/ili partijama)	605	343342
1463	2419	Prelazni računi	605	343342
1464	2420	Izdvojena novčana sredstva za akreditive u banci	606	343342
1465	2421	Izdvojena novčana sredstva za isplatu čekova	606	343342
1466	2422	Izdvojena novčana sredstva za investicije	606	343342
1467	2423	Izdvojena novčana sredstva za kupovinu hartija od vrednosti	606	343342
1468	2424	Izdvojena novčana sredstva za druge namene	606	343342
1469	2430	Glavna blagajna	607	343342
1470	2431	Blagajna poslovnih jedinica (analitika po jedinicama)	607	343342
1471	2432	Pomoćna blagajna	607	343342
1472	2433	Blagajna prodavnice	607	343342
1473	2434	Blagajna stovarišta	607	343342
1474	2435	Blagajna ugostiteljskog objekta	607	343342
1475	2436	Blagajna novčanih bonova	607	343342
1476	2437	Vrednosti u sefu	607	343342
1477	2440	Devizni računi kod banaka u zemlji	608	343342
1478	2441	Devizni računi u inostranstvu	608	343342
1479	2449	Prelazni devizni računi	608	343342
1480	2450	Devizni akreditivi za plaćanje uvoza robe	609	343342
1481	2451	Devizni akreditivi za plaćanje usluga u inostranstvu	609	343342
1482	2452	Devizni akreditivi za plaćanje investicionih radova u inostranstvu	609	343342
1483	2453	Ostali devizni akreditivi kod banke za plaćanje u inostranstvu	609	343342
1484	2460	Devizna blagajna u evrima	610	343342
1485	2461	Devizna blagajna u dolarima	610	343342
1486	2480	Deponovane novčane vrednosti u banci za otkup deviza	611	343342
1487	2481	Deponovane novčane vednosti u banci od prodatih deviza	611	343342
1488	2482	Plemeniti metali	611	343342
1489	2483	Ostala novčana sredstva	611	343342
1490	2490	Novčana sredstva čije je korišćenje ograničeno	612	343342
1491	2491	Novčana sredstva čija je vrednost umanjena	612	343342
1492	2492	Druga umanjenja vrednost novčanih sredstava	612	343342
1493	2700	PDV u primljenim računima od dobavljača iz zemlje po opštoj stopi	614	343342
1494	2701	PDV u primljenim računima po opštoj stopi koji se ne priznaje kao prethodni porez (alternativno može biti knjižen na računu 299)	614	343342
1495	2702	Ispravka prethodnog poreza po opštoj stopi	614	343342
1496	2710	PDV u primljenim računima od dobavljača iz zemlje po posebnoj stopi	615	343342
1497	2711	PDV u primljenim računima po posebnoj stopi koji se ne priznaje kao prethodni porez (alternativno može biti knjižen na računu 299)	615	343342
1498	2712	Ispravka prethodnog poreza po posebnoj stopi	615	343342
1499	2720	Porez na dodatu vrednost u datim avansima po opštoj stopi	616	343342
1500	2721	Porez na dodatu vrednost u datim avansima po opštoj stopi koji se ne priznaje kao prethodni porez (alternativno može biti knjižen na računu 299)	616	343342
1501	2722	Ispravka prethodnog poreza u datim avansima po opštoj stopi	616	343342
1502	2730	Porez na dodatu vrednost u datim avansima po posebnoj stopi	617	343342
1503	2731	Porez na dodatu vrednost u datim avansima po posebnoj stopi koji se ne priznaje kao prethodni porez (alternativno može biti knjižen na računu 299)	617	343342
1504	2732	Ispravka prethodnog poreza u datim avansima po posebnoj stopi	617	343342
1505	2740	Porez na dodatu vrednost plaćen pri uvozu dobara po opštoj stopi	618	343342
1506	2741	Porez na dodatu vrednost plaćen pri uvozu dobara po opštoj stopi koji se ne riznaje kao prethodni porez (alternativno može biti knjižen na računu 299)	618	343342
1507	2742	Ispravka prethodnog poreza koji je plaćen pri uvozu dobara po opštoj stopi	618	343342
1508	2750	Porez na dodatu vrednost plaćen pri uvozu dobara po posebnoj stopi	619	343342
1509	2751	Porez na dodatu vrednost plaćen pri uvozu dobara po posebnoj stopi koji se ne priznaje kao prethodni porez (alternativno može biti knjižen na računu 299)	619	343342
1510	2752	Ispravka prethodnog poreza koji je plaćen pri uvozu dobara po posebnoj stopi	619	343342
1511	2760	Porez na dodatu vrednost obračunat na usluge inostranih lica	620	343342
1512	2770	Naknadno vraćen porez na dodatu vrednost kupcima - stranim državljanima	621	343342
1513	2780	PDV nadoknada isplaćena poljoprivrednicima za isporučene poljoprivredne proizvode	963	343342
1514	2781	PDV nadoknada isplaćena poljoprivrednicima za isporučene šumske proizvode	963	343342
1515	2782	PDV nadoknada isplaćena poljoprivrednicima za izvršene usluge	963	343342
1516	2790	Ukupna poraživanja za prethodni PDV u poreskom periodu	622	343342
1517	2791	Poreski kredit po osnovu prethodnog PDV koji će koristitit u narednom poreskom periodu	622	343342
1518	2800	Unapred plaćeni troškovi zakupnine	624	343342
1519	2801	Unapred plaćene premije osiguranja	624	343342
1520	2802	Unapred plaćeni roškopvi reklame i propagande	624	343342
1521	2803	Unapred plaćeni rokovi učešća na sajmovima i izložbama	624	343342
1522	2804	Plaćene pretplaate na stručne publikacije	624	343342
1523	2805	Unapred plaćeni troškovi ogreva	624	343342
1524	2806	Ostali razgraničeni troškovi	624	343342
1525	2810	Potraživanja za zakupnine za tekući period po osnovu kojih nije izdata faktura	625	343342
1526	2811	Potraživanja za kamatu u slučaju kada obračun nije dostavljen dužniku	625	343342
1527	2812	Potraživanja od investitora za izvedene građevinske radove u obračunskom periodu kada se ne ispostavljaju privremene situacije	625	343342
1528	2813	Potraživanja za prihode od povraćaja poreskih i drugih dažbina po osnovu izvoza koja dospevaju u budućem periodu	625	343342
1529	2814	Ostala potraživanja za nefakturisani prihod	625	343342
1530	2820	Razgraničeni troškovi po osnovu obaveza (analitika po vrstama obaveza)	626	343342
1531	2880	Odložena poreska sredstva po osnovu sredstava koja podležu amortizaciji	627	343342
1532	2881	Odložena poreska sredstva po osnovu rezervisanja u skladu sa MRS 19	627	343342
1533	2882	Odložena poreska sredstva po osnovu obezvređenja imovine	627	343342
1534	2883	Odložena poreska sredstva po osnovu ostalih odbitnih razlika	627	343342
1535	2884	Odložena poreska sredstva po osnovu neiskorišćenog poreskog kredita	627	343342
1536	2885	Odložena poreska sredstva po osnovu prenetih poreskih gubitaka	627	343342
1537	2886	Odložena poreska sredstva po ostalim osnovama	627	343342
1538	2890	Razrgraničeni PDV u primljenim fakturama koje se odnose na promet iz prethodne godine	628	343342
1539	2891	PDV u privremenim građevinskim situacijama koje nisu overene do kraja tekuće godine	628	343342
1540	2892	PDV po osnovu knjižnog odobrenja kada se obaveza za PDV smanjuje u narednoj godini	628	343342
1541	2893	Obračunati PDV po osnovu isporuka na teritoriju APKM za koje nisu ispunjeni uslovi za poresko oslobađanje	628	343342
1542	2894	PDV u primljenim (spornim) fakturama po osnovu kojih nije korišćeno pravo na odbitak prethodnog poreza	628	343342
1543	2895	Ostala aktivna vremenska razgraničenja	628	343342
1544	3000	Akcijski kapital - obične akcije (analitika po emisijama)	631	343342
1545	3001	Akcijski kapital - prioritetne akcije (analitika po emisijama)	631	343342
1546	3010	Osnivački udeli člana A	632	343342
1547	3011	Osnivački udeli člana B	632	343342
1548	3012	Osnivački udeli člana C	632	343342
1549	3020	Ulozi članova ortačkog društva i komplementara komanditnog društva (analitika po članovima)	633	343342
1550	3021	Ulozi komplementara komanditnog društva	633	343342
1551	3022	Ulozi članova komanditor komanditnog društva (analitika po članovima)	633	343342
1552	3030	Kapital javnih preduzeća koje osniva država, odnosno jedinica lokalne samouprave	634	343342
1553	3040	Kapital društvenog preduzeća iskazan u obračunskim akcijama	635	343342
1554	3041	Kapital društvenog preduzeća iskazan u obračunskim udelima	635	343342
1555	3050	Zadružni kapital podeljen na udele zadrugara (analitika po zadrugarima)	636	343342
1556	3060	Emisiona premija po osnovu običnih akcija	964	343342
1557	3061	Emisiona premija po osnovu prioritetnih akcija	964	343342
1558	3062	Emisiona premija po osnovu udela	964	343342
1559	3090	Neto imovina (kapital) preduzenika koji knjige vodi po sistemu dvojnog knjigovodstva	637	343342
1560	3091	Ostali oblici osnovnog kapitala koji nisu obuhvaćeni posebnim računima	637	343342
1561	3100	Upisane a neuplaćene obične akcije	639	343342
1562	3101	Upisane a neuplaćene prioritetne akcije	639	343342
1563	3110	Upisani a neuplaćeni udeli (analitika po članovima)	640	343342
1564	3111	Upisani a neuplaćeni ulozi (analitika po članovima)	640	343342
1565	3210	Obavezne rezerve koje se formiraju u skladu sa zakonom	642	343342
1566	3220	Statutarne rezerve	643	343342
1567	3221	Druge rezerve	643	343342
1568	3222	Dodatne uplae za koje nije izvesno da će biti vraćene članovima društva	643	343342
1569	3300	Revalorizacione rezerve po osnovu revalorizacije nematerijalne imovine	645	343342
1570	3301	Revalorizacione rezerve po osnovu revalorizacije nekretnina	645	343342
1571	3302	Revalorizacione rezerve po osnovu revalorizacije postrojenja i opreme	645	343342
1572	3310	Aktuarski dobici po osnovu planova definisanih primanja	646	343342
1573	3311	Aktuarski gubici po osnovu planova definisanih primanja	646	343342
1574	3320	Dobici po osnovu ulaganja u vlasničke instrumente kapiitala	965	343342
1575	3321	Gubici po osnovu ulaganja u vlasničke instrumente kapitala	965	343342
1576	3330	Dobici po osnovu udela u ostalom sveobuhvatnom dobitku ili gubitku pridruženih društava	966	343342
1577	3331	Gubici po osnovu udela u ostalom sveobuhvatnom dobitku ili gubitku pridruženih društava	966	343342
1578	3340	Dobici po osnovu preračuna finansijskih izveštaja inostranog poslovanja	967	343342
1579	3341	Gubici po osnovu preračuna finansijskih izveštaja inostranog poslovanja	967	343342
1580	3342	Kursne razlike u vezi obaveza ili potraživanja prema inostranom poslovanju čije se izmirnje ne planira niti je verovatno da će skoro nastupiti	967	343342
1581	3350	Dobici od insrumenata zaštite neto ulaganja u inostrano poslovanje	968	343342
1582	3351	Gubici od instrumenata zaštite neto ulaganja u inostrano poslovanje	968	343342
1583	3360	Dobici po osnovu instrumenata zaštite rizika (hedžinga) novčanog toka	969	343342
1584	3361	Gubici po osnovu instrumenata zaštite rizika (hedžinga) novčanog toka	969	343342
1585	3370	Dobici ili gubici po osnovu hartija od vrednosti raspoloživih za prodaju	970	343342
1586	3371	Gubici po osnovu hartija od vrednosti raspoloživih za prodaju	970	343342
1587	3400	Neraspoređeni dobitak ranijihgodina (po godinama)	648	343342
1588	3410	Neraspoređeni dobitak tekuće godine	649	343342
1589	3500	Gubitak ranijih godina (po godinama)	651	343342
1590	3510	Gubitak tekuće godine	652	343342
1591	4000	Rezervisanja za troškove u garantnom roku za prodate proizvode	655	343342
1592	4001	Rezervisanja u garantnom roku za prodatu robu	655	343342
1593	4002	Rezervisanja za troškove u garantnom roku za izvršene usluge	655	343342
1594	4010	Rezervisanja za troškove obnavljanja zemljišta	656	343342
1595	4011	Rezervisanja za obnavljanje (reprodukciju) šima	656	343342
1596	4012	Rezervisanja za obnavljanje ostalih prirodnih bogatstava	656	343342
1597	4020	Rezervisanja za zadržane kaucije i depozite u zemlji	657	343342
1598	4021	Rezervisanja za zadržane kaucije i depozite u inostranstvu	657	343342
1599	4030	Rezervisanja za troškove prestanka rada jednog dela poslovanja	658	343342
1600	4031	Rezervisanja za troškove zatvaranja poslovnih lokacija	658	343342
1601	4032	Rezervisanja za troškove premeštanja poslovanja s jedne lokacije na drugu	658	343342
1602	4033	Rezervisanja za troškove promena u rukovodstvu	658	343342
1603	4034	Rezervisanja za troškove kompletne reorganizacije koja će uticati na prirodu i predmet poslovanja	658	343342
1604	4040	Rezervisanja za otpremnine prilikom odlaska u penziju	659	343342
1605	4041	Rezervisanja za jubilarne nagrade	659	343342
1606	4042	Rezervisanja za neiskorišćena plaćena odsustva	659	343342
1607	4043	Ostala rezervisanja za naknade i druge beneficije zaposlenih	659	343342
1608	4050	Rezervisanja za troškove sudskih sporova u zemlji	971	343342
1609	4051	Rezervisanja za troškove sudskih sporova u inostranstvu	971	343342
1610	4090	Rezervisanja po osnovu štetnih ugovora	660	343342
1611	4091	Rezervisanja za izdae garancije i druge jemstva	660	343342
1612	4092	Druga dugoročna rezervisanja	660	343342
1613	4100	Obaveze po dugoročnim kreditima koje se prema ugovoru mogu konvertovati u kapital	662	343342
1614	4101	Obveznce koje se mogu konvertovati u kapital	662	343342
1615	4102	Zamenljive obveznice koje se mogu konvertovati u kapital	662	343342
1616	4103	Varanti	662	343342
1617	4104	Ostale dugoročne hartije od vrednosti koje se mogu konvertovati u kapital	662	343342
1618	4105	Dugoročna ulaganja koja se prema ugovoru mogu konvertovati u kapital	662	343342
1619	4106	Dokapitalizacija po ugovoru o prodaji društvenog kapitala po kojoj se privremeno stiču sopstvene akcije	662	343342
1620	4107	Ostale obaveze koje se mogu konvertovati u kapital	662	343342
1621	4110	Obaveze po osnovu primljenih dugoročnih kredita od matičnih pravnih lica u zemlji	663	343342
1622	4111	Obaveze po osnovu primljenih dugoročnih kredita od matičnih pravnih lica u inostranstvu	663	343342
1623	4112	Obaveze po osnovu primljenih dugoročnih kredita od zavisnih pravnih lica u zemlji	663	343342
1624	4113	Obaveze po osnovu primljenih dugoročnih kredita od zavisnih pravnih lica u inostranstvu	663	343342
1625	4120	Obaveze po osnovu primljenih dugoročnih kredita od ostalih povezanih pravnih lica u zemlji	664	343342
1626	4121	Obaveze po osnovu primljenih dugoročnih kredita od ostalih povezanih pravnih lica u inostranstvu	664	343342
1627	4130	Obaveze za obveznice u domaćoj valuti	665	343342
1628	4131	Obaveze za obveznice u stranoj valuti	665	343342
1629	4132	Obaveze po ostalim emitovanim hartijama od vrednosti u periodu dužem od godinu dana	665	343342
1630	4133	Obaveze po ostalim emitovanim hartijama od vrednosti u periodu dužem od godinu dana	665	343342
1631	4140	Dugoročni finansijski krediti u dinarima	666	343342
1632	4141	Dugoročni finansijski krediti sa valutnom klauzulom	666	343342
1633	4142	Dugoročni finansijski zajmovi u dinarima	666	343342
1634	4143	Dugoročni finansijski zajmovi sa valutnom klauzulom	666	343342
1635	4144	Dugoročni robni krediti u zemlji	666	343342
1636	4145	Ostali dugoročni krediti u zemlji	666	343342
1637	4146	Ostali dugoročni krediti i zajmovi u zemlji	666	343342
1638	4150	Dugoročni finansijski krediti u inostranstvu	667	343342
1639	4151	Dugoročni finansijski zajmovi u inostranstvu	667	343342
1640	4152	Dugoročni robni krediti u inostranstvu	667	343342
1641	4153	Ostali dugoročni krediti u inostranstvu	667	343342
1642	4154	Ostali dugoročni zajmovi u inostranstvu	667	343342
1643	4160	Obaveze po osnovu finansijskog lizinga koje dospevaju za više od 12 meseci - lizing kuće	972	343342
1644	4161	Obaveze po osnovu finansijskog lizinga koje dospevaju za više od 12 meseci - lizing kuće	972	343342
1645	4190	Obaveze za dugoročne depozite	668	343342
1646	4200	Kratkoročni krediti i zajmovi od matičnih pravnih lica u zemlji	670	343342
1647	4191	Obaveze za dugoročne kaucije                      Obaveze za dodatne uplate članova društva sa rokom vraćanja dužim od jedne godine	668	343342
1648	4192	Druge dugoročne obaveze	668	343342
1649	4201	Kratkoročni krediti i zajmovi od maičnih pravnih lica u inostranstvu	670	343342
1650	4202	Kratkoročni krediti i zajmovi od zavisnih pravnih lica u zemlji	670	343342
1651	4203	Kratkoročni krediti i zajmovi od zavisnih pravnih lica u inostranstvu	670	343342
1652	4210	Kratkoročni krediti i zajmovi od ostalih povezanih pravnih lica u zemlji	671	343342
1653	4211	Kratkoročni krediti i zajmovi od ostalih povezanih pravnih lica u inostranstvu	671	343342
1654	4220	Kratkoročni finansijski krediti i zajmovi u dinarima	672	343342
1655	4221	Kratkoročni finansijski krediti i zajmovi sa valutnom klauzulom	672	343342
1656	4222	Kratkoročni robni krediti	672	343342
1657	4223	Ostali krediti i zajmovi	672	343342
1658	4230	Kratkoročni finansijski krediti i zajmovi u inostranstvu	673	343342
1659	4231	Kratkoročni robni krediti u inostranstvu	673	343342
1660	4232	Drugi kratkoročni krediti i zajmovi u inostranstvu	673	343342
1661	4240	Deo dugoročnih kredita koji dospevaju za plaćanje u roku do godinu dana od dana bilansa u dinarima	674	343342
1662	4241	Deo dugoročnih kredita koji dospevaju za plaćanje u roku do godinu dana od dana bilansa sa valutnom klauzulom	674	343342
1663	4242	Deo dugoročnih kredita koji dospevaju za plaćanje u roku do godinu dana od dana bilansa u stranoj valuti	674	343342
1664	4243	Deo dugoročnih robnih kredita koji dospevaju za plaćanje u roku do godinu dana od dana bilansa	674	343342
1665	4250	Deo ostalih dugoročnih obaveza koje dospevaju za plaćanje u roku do godinu dana od dana bilansa u dinarima	675	343342
1666	4251	Obaveze po osnovu finansijskog lizinga koje dospevaju u narednih 12 meseci	675	343342
1667	4260	Kratkoročne obaveze po emitovanim obveznicama	676	343342
1668	4261	Kratkoročne obaveze po komercijalnim zapisima	676	343342
1669	4262	Kratkoročne obaveze po finansijskim derivatima	676	343342
1670	4263	Obaveze po izdatim blagajničkim zapisima sa rokom dospeća do godinu dana	676	343342
1671	4264	Obaveze po izdatim ceertifikatima o depozitu sa rokom dospeća do godinu dana	676	343342
1672	4265	Ostale obaveze po kratkoročnim hartijama od vrednosti	676	343342
1673	4270	Obaveze po osnovu stalnih sredstava namenjenih prodaji	677	343342
1674	4271	Obaveze po osnovu obustavljenog poslovanja	677	343342
1675	4290	Obaveze po izdaim čekovima	678	343342
1676	4291	Obaveze za nominalnu vrednost izdatih menica	678	343342
1677	4292	Obaveze za kamatu budućeg perioda koja je sadržana u meničnoj vrednosti	678	343342
1678	4293	Ostale kratkoročne finansijske obaveze	678	343342
1679	4300	Primljeni avansi u dinarima	680	343342
1680	4301	Primljeni avansi sa valutnom klauzulom	680	343342
1681	4302	Primljeni avansi u stranoj valuti	680	343342
1682	4303	Primljeni depozti u dinarima	680	343342
1683	4304	Primljeni depoziti sa valutnom klauzulom	680	343342
1684	4305	Primljeni depoziti u stranoj valuti	680	343342
1685	4306	Primljene kaucije u dinarima	680	343342
1686	4307	Primljene kaucije sa valutnom klauzulom	680	343342
1687	4308	Primljene kaucije u stranoj valuti	680	343342
1688	4310	Dobavljači-matična pravna lica u zemlji za obrtna sredstva	681	343342
1689	4311	Dobavljači - matična pravna lica u zemlji za nematirijalna ulaganja i nekretnine, postrojenja, opremu i biološka sredstva	681	343342
1690	4312	Dobavljači - matična pravna lica u zemlji - ostalo	681	343342
1691	4313	Dobavljači - zavisna pravna lica u zemlji za obrtna sredstva	681	343342
1692	4314	Dobavljači - zavisna pravna lica u zemlji za nematerijalna ulaganja i nekretnine, postrojenja, opremu i biološka sredstva	681	343342
1693	4315	Dobavljači - zavisna pravna lica u zemlji - ostalo	681	343342
1694	4320	Dobavljači  matična pravna lica u inostranstvu za obrtna sredstva	682	343342
1695	4321	Dobavljači - matična pravna lica u inostranstvu za nematerijalna ulaganja i nekretnine, postrojenja, opremu i biološka sredstva	682	343342
1696	4322	Dobavljači - matična pravna lica u inostranstvu - ostalo	682	343342
1697	4323	Dobavljači - zavisna pravna lica u inostranstvu za obrtna sredstva	682	343342
1698	4324	Dobavljači - zavisna pravna lica u inostranstvu za nematerijalna ulaganja i nekretnine, postrojenja, opremu i biološka sredstva	682	343342
1699	4325	Dobavljači - zavisna pravna lica u inostranstvu - ostalo	682	343342
1700	4330	Dobavljači - ostala povezana pravna lica u zemlji za obartna sredstva	683	343342
1701	4331	Dobavljači - ostala povezana pravna lica u zemlji za nematerijalna ulaganja, i nekretnine, postrojenja, opremu i biološka sredstva	683	343342
1702	4332	Dobavljači - ostala povezana pravna lica u zemlji - ostalo	683	343342
1703	4340	Dobavljači - ostala povezana pravna lica u inostranstvu za obrtna sredstva	684	343342
1776	4610	Obaveze za dividende na obične akcije	701	343342
1777	4611	Obaveze za dividende na prioritetne akcije	701	343342
1704	4341	Dobavljači - ostala povezana pravna lica u inostranstvu za nematerijalna ulaganja, i nekretnine, postrojenja, opremu i biološka sredstva	684	343342
1705	4342	Dobavljači - ostala povezana pravna lica u inostranstvu - ostalo	684	343342
1706	4350	Dobavljači u zemlji za obrtna sredstva	973	343342
1707	4351	Dobavljači u zemlji za nematerijalna ulaganja i nekretnine, postrojenja, opremu i biološka sredstva	973	343342
1708	4352	Dobavljači u zemlji - ostalo	973	343342
1709	4360	Dobavljači u inostranstvu za obrtna sredstva	974	343342
1710	4361	Dobavljači u inostranstvu za nematerijalna ulaganja, i nekretnine, postrojenja, opremu i biološka sredstva	974	343342
1711	4362	Dobavljači u inostranstvu - ostalo	974	343342
1712	4390	Obaveze za naknade šteta na sredstvima drugih pravnih i fizičkih lica	685	343342
1713	4391	Obaveze po izdatim menicama	685	343342
1714	4392	Ostale obaveze iz poslovanja	685	343342
1715	4400	Obaveze prema uvozniku	687	343342
1716	4401	Obaveze prema uvozniku po drugim osnovima	687	343342
1717	4410	Obaveze za primljene depozite za usluge izvoza	688	343342
1718	4411	Druge obveze po osnovu izvoza za tuđ račun	688	343342
1719	4420	Obaveze po osnovu komisione prodaje	689	343342
1720	4421	Obaveze po osnovu konsignacione prodaje	689	343342
1721	4490	Obaveze po osnovu zajedničkih poslova	690	343342
1722	4491	Ostale obavaeze po osnovu specifičnih poslova	690	343342
1723	4500	Obaveze za neto zarade za obavljeni rad i vreme provedeno na radu	692	343342
1724	45000	Obaveze za neto osnovnu zaradu	1723	343342
1725	45001	Obaveze za neto zarade po osnovu radnog vremena	1723	343342
1726	45002	Obaveze za neto zarade po osnovu uvećanja zarade za rad na dan državnog i verskog praznika	1723	343342
1727	45003	Obaveze za neto zarade po osnovu uvećanja zarade za prekovremeni radzarade za	1723	343342
1728	45004	Obaveze za neto zarade po osnovu uvećanja zarade za rad noću i u smenama	1723	343342
1729	45005	Obaveze za neto zarade po osnovu uvećanja zarade za "minuli rad"	1723	343342
1730	4501	Obaveze za neto zarade po osnovu doprinosa zaposlenog poslovnom uspehu poslodavca (bonusi, nagrade i dr.)	692	343342
1731	4502	Obaveze za neto zarade po osnovu ishrane u toku rada ("topli obrok")	692	343342
1732	4503	Obaveze za neto zarade po osnovu regres za korišćenje godišnjeg odmora	692	343342
1733	4504	Obaveze za neto zarade po osnovu "terenskog dodatka"	692	343342
1734	4505	Obaveze za neto zarade po osnovu "stimulativne otpremnine" kod sporazumnog prestanka radnog odnosa	692	343342
1735	4506	Obaveze zaa neto zarade po osnovu dodatka za odvojeni život od porodice	692	343342
1736	4507	Obaveze za neto naknade zarada	692	343342
1737	45070	Obaveze za neto naknade zarada za vreme odsustvovanja sa rada do 30 dana	1736	343342
1738	45071	Obaveze za neto naknade zarada za vreme korišćenja godišnjeg odmora	1736	343342
1739	45072	Obaveze za neto naknade zarada za dane državnog i verskog praznika	1736	343342
1740	45073	Obaveze za neto naknade zarada za dane plaćenog odsustva	1736	343342
1741	45074	Obaveze za neto naknade zarada za vreme prekida rada do kojeg je došlo bez krivice radnika	1736	343342
1742	45075	Obaveze za neto naknade zarada za vraeme privremenog udaljenja zaposlenog sa rada	1736	343342
1743	45076	Obaveze za neto naknade zarada za vreme vojne vežbe	1736	343342
1744	45077	Druge obaveze za neto naknade zarada	1736	343342
1745	4508	Obaveze za neto zarade upućenih na rad u inostranstvo	692	343342
1746	4509	Obaveze za neto zarade iz rezultata	692	343342
1747	45010	Druge obaveze za neto zarade i naknade zarada	1730	343342
1748	4510	Obaveze za porez na zarade	693	343342
1749	4511	Obaveze za porez na naknade zarada	693	343342
1750	4520	Obaveze za doprinos za penzijsko i invalidsko osiguranje na zarade i naknade zarada	694	343342
1751	4521	Obaveze za doprinos za zdravstveno osiguranje na zarade i naknade zarada	694	343342
1752	4522	Obaveze za doprinos za osiguranje od nezaposlenosti na zarade i naknade zarada	694	343342
1753	4530	Obaveze za doprinos za penzijsko i invalidsko osiguranje na teret poslodavca	695	343342
1754	4531	Obaveze za doprinos za zdravstveno isiguranje na teret poslodavca	695	343342
1755	4532	Obaveze za doprinos za osigranje od nezaposlenosti na teret poslodavca	695	343342
1756	4540	Obaveze prema zaposlenima za naknadu zarade za odsustvovanje sa rada duže od 30 dana	696	343342
1757	4541	Obaveze prema zaposlenima za naknadu zarade za porodiljsko bolovanje	696	343342
1758	4542	Obaveze prema zaposlenima za naknadu zarade za odsustvovanje sa rada za vreme vojne vežbe	696	343342
1760	5715	Gubici po osnovu prodaje osnovnog stada	786	343342
1761	5720	Gubici od prodaje učešća u kapitalu	787	343342
1762	5730	Gubici od prodaje osnovnog materijala	788	343342
1763	4550	Obaveze za poreze i doprinose na naknade zarada na teret zaposlenog koje se refundiraju - za bolovanje preko 30 dana	697	343342
1764	4551	Obaveze za poreze i doprinose na naknade zarada na teret zaposlenog koje se refundiraju - za porodiljsko bolovanje	697	343342
1765	4552	Obaveze za poreze i doprinose na naknade zarada na teret zaposlenog koje se refundiraju - za vreme vojne vežbe	697	343342
1766	4553	Ostale obaveze za poreze i doprinose na naknade zarada na teret zaposlenog koje se refundiraju	697	343342
1767	4560	Obaveze za poreze i doprinose na naknade zarada na teret poslodavca koje se refundiraju - za bolovanje preko 30 dana	698	343342
1768	4561	Obaveze za poreze i doprinose na naknade zarada na teret poslodavca koje se - za porodiljsko bolovanje refundiraju	698	343342
1769	4562	Obaveze za poreze i doprinose na naknade zarada na teret poslodavca koje se refundiraju - za vreme vojne vežbe	698	343342
1770	4563	Ostale obaveze za poreze i doprinose na naknade zarada na teret poslodavca koje se refundiraju	698	343342
1771	4600	Obaveze po osnovu kamata po finansijskim kreditima i zajmovima	700	343342
1772	4601	Obaveze po osnovu kamata po robnim kreditima	700	343342
1773	4602	Obaveze po osnovu zateznih kamata	700	343342
1774	4603	Obaveze po osnovu kamata za javne prihode	700	343342
1775	4604	Ostale nepomenute obaveze po osnovu kamata i troškova finansiranja	700	343342
1778	4612	Ostale obaveze za dividende	701	343342
1779	4620	Obaveze prema članovima društva sa ograničenom odgovornošću za učešće u dobitku	702	343342
1780	4621	Obaveze prema ortacima za učešće u dobitku	702	343342
1781	4622	Obaveze prema komplementarima za učešće u dobitku	702	343342
1782	4623	Obaveze prema komanditorima za učešće u dobitku	702	343342
1783	4624	Obaveze prema zadrugarima za učešće u dobitku	702	343342
1784	4625	Ostale obaveze za učešće u dobitku	702	343342
1785	4630	Obaveze za otpremninu prilikom odlaska u penziju	703	343342
1786	4631	Obaveze za otpremninu zbog prestanka radnog odnosa ("tehnološki višak")	703	343342
1787	4632	Obaveze za jubilarne nagrade	703	343342
1788	4633	Obaveze za solidarnu pomoć	703	343342
1789	4634	Obaveze za naknadu troškova smeštaja i ishrane na službenom putu	703	343342
1790	4635	Obaveze za naknadu troškova prevoza na službenom putu	703	343342
1791	4636	Obaveze za naknadu troškova prevoza na radno mesto i sa radnog mesta	703	343342
1792	4637	Obaveze za naknadu troškova smeštaja i ishrane za rad i boravak na terenu	703	343342
1793	4638	Ostale obaveze prema zaposlenima	703	343342
1794	4640	Obaveze prema direktoru, odnosno članovima organa upravljanja i nadzora na teret troškova	704	343342
1795	4641	Obaveze prema direktoru, odnosno članovima organa upravljanja i nadzora na teret neraspoređene dobiti ranijih godina	704	343342
1796	4642	Druge nepomenute obaveze prema direktoru, odnosno članovima organ upravljanja i nadzora	704	343342
1797	4650	Obaveze prema fizičkim licima po osnovu ugovora o autorskom delu (neto iznos)	705	343342
1798	4651	Obaveze prema fizičkim licima po osnovu ugovora o delu (neto iznos)	705	343342
1799	4652	Obaveze prema fizičkim licima po osnovu ugovora o privremenim i povremenim poslovima (neto iznos)	705	343342
1800	4653	Obaveze prema fizičkim licima po osnovu ugovora o dopunskom radu (neto iznos)	705	343342
1801	4654	Obaveze prema fizičkim licima po osnovu drugih nepomenutih ugovora (neto iznos)	705	343342
1802	4660	Obaveze za neto prihod preduzetnika koji akontaciju podižu u toku godine	706	343342
1803	4670	Obaveze za ratkoročna rezervisnaja i rzike u skladu s MRS 37 (analitika po vrstama rezervisanja)	975	343342
1804	4690	Obaveze za doprinose privrednim komorama	707	343342
1805	4691	Obaveze za doprinose zadružnim savezima	707	343342
1806	4692	Obaveze za davanja fizičkim licima koja nisu zaposlena kod isplatioca	707	343342
1807	4693	Obaveze po osnovu bankarskih usluga	707	343342
1808	4694	Druge nepomenute obaveze	707	343342
1809	4700	Obaveza za PDV po osnovu izvršenog prometa dobara i usluga koji je oporeziv PDV	709	343342
1810	4701	Obaveza za PDV po osnovu prometa stranog lica	709	343342
1811	4702	Obaveza za PDV po osnovu ispravke odbitka prethodnog poreza za opremu	709	343342
1812	4703	Obaveza za PDV po osnovu ispravke odbitka prethodnog poreza za objekte za vršenje delatnosti	709	343342
1813	4704	Obaveza za PDV po osnovu izmene (povećanja) poreske osnovice	709	343342
1814	4710	Obaveza za PDV po osnovu izvršenog prometa dobara i usluga koji je oporeziv PDV	710	343342
1815	4711	Obavezaza PDV po osnovu prometa stranog lica	710	343342
1816	4712	Obaveza za PDV po osnovu izmene (povećanja) poreske osnovice	710	343342
1817	4720	Obaveze za porez na dodatu vrednost po primljenim avansima po opštoj stopi	711	343342
1818	4721	Obaveze za porez na dodatu vrednost po primljenim avansima po opštoj stopi od kupca sa APKM	711	343342
1819	4730	Obaveze za porez na dodatu vrednost po primljenim avansima po posebnoj stopi	712	343342
1820	4731	Obaveze za porez na dodatu vrednost po primljenim avansima po posebnoj stopi od kupca sa APKM	712	343342
1821	4740	Obaveze za PDV po osnovu uzimanja dobara za lične potrebe osnivača, zaposlenih i drugih lica	713	343342
1822	4741	Obaveze za PDV po osnovu iskazanog rashoda (kalo, rastur, kvar i lom) iznad propisanog normativa	713	343342
1823	4742	Obaveze za PDV po osnovu svakog drugog prometa dobara bez naknade koji je oporeziv PDV	713	343342
1824	4743	Obaveze za PDV po osnovu manjka dobara	713	343342
1825	5721	Gubici od prodaje harija od vrednosti	787	343342
1826	5731	Gubici od prodaje rezervnih delova	788	343342
1827	5732	Gubici od prodaje sitnog inventara	788	343342
1828	4744	Obaveze za PDV po osnovu upotrebe dobara koja su deo poslovne imovine poreskog obveznika za lične potrebe osnivača, zaposlenih ili drugih lica	713	343342
1829	4745	Obaveze za PDV po osnovu pruženih usluga bez naknade za lične potrebe osnivača, zaposlenih ili drugih lica	713	343342
1830	4746	Obaveze za PDV po osnovu svakog drugog pružanja usluga bez naknade	713	343342
1831	4750	Obaveze za PDV po osnovu uzimanja dobara za lične potrebe osnivača, zaposlenih i drugih lica	714	343342
1832	4751	Obaveze za PDV po osnovu iskazanog rashoda (kalo, rastur, kvar i lom) iznad propisanog normativa	714	343342
1833	4752	Obaveze za PDV po osnovu svakog drugog prometa dobara bez naknade koji je oporeziv PDV	714	343342
1834	4753	Obaveze za PDV po osnovu manjka dobara	714	343342
1835	4754	Obaveze za PDV po osnovu upotrebe dobara koja su deo poslovne imovine poreskog obveznika za lične potrebe osnivača, zaposlenih ili drugih lica	714	343342
1836	4755	Obaveze za PDV po osnovu pruženih usluga bez naknade za lične potrebe osnivača, zaposlenih ili drugih lica	714	343342
1837	4756	Obaveze za PDV po osnovu svakog drugog pružanja usluga bez naknade	714	343342
1838	4760	Obaveze za porez na dodatu vrednost po osnovu prodaje za gotovinu po stopi od 10%	715	343342
1839	4761	Obaveze za porez na dodatu vrednost po osnovu prodaje za gotovinu po stopi od 20%	715	343342
1840	4762	Obaveze za porez na dodatu vrednost po osnovu prodaje na čekove po stopi od 10%	715	343342
1841	4763	Obaveze za porez na dodatu vrednost po osnovu prodaje na čekove po stopi od 20%	715	343342
1842	4764	Obaveze za porez na dodatu vrednost o osnovu prodaje - platnim karticama po stopi od 10%	715	343342
1843	4765	Obaveze za porez na dodatu vrednost po osnovu prodaje - platnim karticama po stopi od 20%	715	343342
1844	4790	Obaveze za porez na dodatu vrednost po osnovu razlike obračunatog oreza na dodatu vrednost i prethodnog poreza	716	343342
1845	4800	Obaveze za akcize na derivate nafte	718	343342
1846	4801	Obaveze za akcize na duvanske prerađevine	718	343342
1847	4802	Obaveze za akcize na alkoholna pića	718	343342
1848	4803	Obaveze za akcize na kafu	718	343342
1849	4810	Obaveze za porez na dobit pravnih lica	719	343342
1850	4811	Obaveze za porez na prihod preduzetnika	719	343342
1851	4820	Obaveze za porez na lična primanja građana na teret poslodavca	720	343342
1852	4821	Obaveze na takse na teret poslodavca	720	343342
1853	4822	Obaveze za carine i druge dažbine po osnovu nabavke	720	343342
1854	4823	Obaveze za carine i druge dažbine na teret troškova	720	343342
1855	4824	Obaveze za porez na imovinu	720	343342
1856	4825	Obaveze za naknade za korišćenje dobara od opšteg interesa	720	343342
1857	4826	Druge obaveze za poreze, carine i druge dažbine iz nabavke ili na teret troškova	720	343342
1858	4830	Obaveze za doprinose koje plaća vlasnik privrednog društva po rešenju nadležnog organa	721	343342
1859	4890	Obaveze za poreze i doprinose obračunate po osnovu ugovora o delu	722	343342
1860	4891	Obaveze za poreze i doprinose obračunate po osnovu ugovora o autorstvu	722	343342
1861	4892	Obaveze za poreze i doprinose po osnovu drugih ugovora uz naknadu	722	343342
1862	4893	Obaveze po osnovama troškova za takse, naknade i slične obaveze	722	343342
1863	4894	Obaveze za porez po odbitku na prihode nerrezidentnih pravnih lica	722	343342
1864	4895	Ostale nepomenute obaveze za poreze i doprinose i druge dažbine	722	343342
1865	4900	Obračunati nefakturisani troškovi zakupnine	724	343342
1866	4901	Obračunati nefakturisani troškovi grejanja	724	343342
1867	4902	Obračunati nefakturisani troškovi kamata	724	343342
1868	4903	Obračunati drugi nefakturisani troškovi 	724	343342
1869	4910	Unapred naplaćena zakupnina za budući obračunski period	725	343342
1870	4911	Unapared naplaćena pretplata koja se odnosi na naredni obračunski period	725	343342
1871	4912	Unapred naplaćeni ostali prihodi koji se odnose na naredni obračunski period	725	343342
1872	4940	Razgraničeni zavisni troškovi nabavke materijala	726	343342
1873	4941	Razgraničeni zavisni troškovi nabavke rezervnih delova	726	343342
1874	4942	Razgraničeni zavisni troškovi nabavke robe	726	343342
1875	4943	Razgraničeni zavisni troškovi nabavke nepokretnosti, postrojenja, opreme i bioloških sredstava	726	343342
1876	4944	Ostali razgraničeni zavisni troškovi nabavke	726	343342
1877	4950	Odloženi prihodi po osnovu primljenih donacija	727	343342
1878	4951	Odloženi prihodi po osnovu drugih državnih davanja	727	343342
1879	4960	Razgraničeni prihodi po osnovu potraživanja (analitika po vrstama potraživanja)	728	343342
1880	4980	Odložene poreske obaveze po osnovu sredstava koja podležu amortizaciji	729	343342
1881	4981	Odložene poreske obaveze po osnovu revalorizacije	729	343342
1882	4982	Odložene poreske obaveze po osnovu ostalih oporezivih privremenih razlika	729	343342
1883	4990	Razgraničene obaveze za PDV po osnovu rashoda i manjka po popisu (alternativno ukoliko se ne knjiže na grupi 47)	730	343342
1884	4991	Razgraničene obaveze za PDV po osnovu privremene situacije koja nije overena do kraja tekuće godine (alternativno ukoliko se ne knjiže na grupi 47)	730	343342
1885	4992	Razgraničene obaveze za PDV po osnovu knjižnog odobrenja, po osnovu kojeg postoji obaveza ispravke odbitka prethodnog poreza u narednoj godini (alernativno ukoliko se ne knjiže na grupi 47)	730	343342
1886	4993	Ostala pisivna vremenska razgraničenja	730	343342
1887	5000	Nabavna vrednost kupljene robe	733	343342
1888	5010	Nabavna vraednost prodate robe matičnim i zavisnim pravnim licima	734	343342
1889	5011	Nabavna vrednost prodate robe ostalim povezanim pravnim licima	734	343342
1890	5012	Nabavna vrednost prodate robe u veleprodaji	734	343342
1891	5013	Nabavna varednost prodate robe u maloprodajnim objektima	734	343342
1892	5014	Nabavna vrednost prodate robe u komisionu	734	343342
1893	5015	Nabavna vrednost prodate robe u tranzitu na domaćem tržištu	734	343342
1894	5016	Nabavna varednost samostalne prodaje na inostranom tržištu	734	343342
1895	5017	Nabavna vrednost prodate robe na inostranom tržištu posredstvom izvoznika	734	343342
1896	5018	Nabavna vrednost prodate robe u reeksportu	734	343342
1897	5019	Nabavna vrednost ostale prodate robe	734	343342
1898	5020	Nabavna vrednost zemljišta pribavljenog radi prodaje	735	343342
1899	5021	Nabavna vrednost građevinskih objekata pribavljenih radi prodaje	735	343342
1900	5022	Nabavna varednost ostalih nekretnina pribavljenih radi prodaje	735	343342
1901	5030	Nabavna vrednost nematerijalnih ulaganja pribavljenih radi prodaje	976	343342
1902	5031	Nabavna vrednost opreme pribavljene radi prodaje	976	343342
1903	5032	Nabavna vrednost postrojenja pribavljenih radi prodaje	976	343342
1904	5033	Nabavna vrednost bioloških sredstava pribavljenih radi prodaje	976	343342
1905	5034	Nabavna vrednost ostalih stalnih sredstava pribavljenih radi prodaje	976	343342
1906	5100	Nabavka materijala	737	343342
1907	5110	Troškovi sirovina i osnovnog materijala	738	343342
1908	5111	Troškovi pomoćnog materijala	738	343342
1909	5112	Troškovi ambalaže (neodvojive)	738	343342
1910	5113	Ukalkulisan dozvoljen kalo, rastur, kvar i lom materijala	738	343342
1911	5114	Utrošen ostali materijal za izradu	738	343342
1912	5115	Odstupanje od cena materijala	738	343342
1913	5120	Troškovi materijala za održavanje higijene	977	343342
1914	5121	Troškovi kancelarijskog materijala	977	343342
1915	5122	Troškovi rezervnih delova	977	343342
1916	5123	Troškovi alata i inventara	977	343342
1917	5124	Troškovi auto-guma i ambalaže	977	343342
1918	5125	Troškovi ostalog režijskog materijala	977	343342
1919	5126	Odstupanje od cena ostalog materijala	977	343342
1920	5130	Troškovi električne energije	739	343342
1921	5131	Drugi troškovi pogonskog goriva (ugalj, gas, nafta i njeni derivati i sl.)	739	343342
1922	5132	Troškovi upotrebljene pare i tople vode	739	343342
1923	5133	Troškovi goriva za vozila	739	343342
1924	5134	Ostali troškovi goriva i energije	739	343342
1925	5140	Troškovi rezervnih delova za opremu	978	343342
1926	5141	Troškovi rezervnih delova za postrojenja	978	343342
1927	5150	Troškovi jednokratnog otpisa alata	979	343342
1928	5151	Troškovi jednokratnog otpisa inventara	979	343342
1929	5200	Troškovi neto zarada za obavljeni rad i vreme provedeno na radu	741	343342
1930	52000	Troškovi neto zarada - osnovna zarada	1929	343342
1931	52001	Troškovi neto zarada - radni učinak	1929	343342
1932	52002	Troškovi neto zarada - rad na dan državnog i verskog praznika	1929	343342
1933	52003	Troškovi neto zarada - prekovremeni rad	1929	343342
1934	52004	Troškovi neto zarada - rad noću i u smenama	1929	343342
1935	52005	Troškovi neto zarada - "minuli rad"	1929	343342
1936	5201	Troškovi neto zarada p osnovu doprinosa zaposlenog poslovnom uspehu poslodavca (bonusi, nagrade i dr.)	741	343342
1937	5202	Troškovi neto zarada po osnovu ishrane u toku rada ("topli obrok")	741	343342
1938	5203	Troškovi neto zarada po osnovu regres za korišćenje godišnjeg odmora	741	343342
1939	5204	Troškovi neto zarada po osnovu "terenskog dodatka"	741	343342
1940	5205	Troškovi neto zarada po osnovu "stimulativne otpremnine" kod sporazumnog prestanka radnog odnosa	741	343342
1941	5206	Troškovi neto zarada po osnovu dodatka za odvojeni život od porodice	741	343342
1942	5207	Troškovi poreza na zarade na teret zaposlenog	741	343342
1943	5208	Troškovi doprinosa na zarade na teret zaposlenog	741	343342
1944	5209	Troškovi neto naknada zarada	741	343342
1945	52090	Troškovi neto naknada za vreme odsustvovanja s rada do 30 dana	1944	343342
1946	52091	Troškovi neto naknada za vreme odsustvovanja s rada u dane državnih i verskih praznika	1944	343342
1947	52092	Troškovi neto naknada za vareme korišćenja godišnjeg odmora	1944	343342
1948	52093	Troškovi neto naknada za dane plaćenog odsustva	1944	343342
1949	52094	Troškovi neto naknada za vareme prekida rada do kojeg je došlo bez krivice zaposlenog	1944	343342
1950	52095	Troškovi neto naknada za vreme privremenog udaljenja zaposlenog s rada	1944	343342
1951	52096	Troškovi neto naknada za vareme vojne vežbe	1944	343342
1952	52010	Troškovi poreza na naknade zarada na teret zaposlenog	1936	343342
1953	52011	Troškovi doprinosa n naknade zarada na teret zaposlenog	1936	343342
1954	52012	Troškovi neto zarada upućenih na rad u inostranstvo	1936	343342
1955	5210	Troškovi doprinosa za penzijsko i invalidsko osiguranje na teret poslodavca	742	343342
1956	5211	Troškovi doprinosa za zdravstveno osiguranje na teret poslodavca	742	343342
1957	5212	Troškovi doprinosa za osiguranje od nezaposlenosti na teret poslodavca	742	343342
1958	5220	Troškovi naknada po ugovoru o delu	743	343342
1959	5230	Troškovi naknada po autorskim ugovorima	744	343342
1960	5240	Troškovi naknada po ugovoru o privremenim i povremenim poslovima	745	343342
1961	5250	Troškovi naknada fizikim licima po osnovu ostlaih ugovora (analitika po fizičkim licima)	746	343342
1962	5260	Troškovi naknada direktoru, odnosno članovima organa upravljanja i nadzora	747	343342
1963	5290	Otpremnina prilikom odlaska u penziju	748	343342
1964	5291	Otpremnina zbog prestanka radno odnosa ("tehnološki višak")	748	343342
1965	5292	Jubilarne nagrade	748	343342
1966	5294	Naknade troškova smeštaja i ishrane na službenom putu	748	343342
1967	5295	Naknade troškova prevoza na službenom putu	748	343342
1968	5296	Naknade troškova prevoza na radno mesto i sa radnog mesta	748	343342
1969	5297	Naknade za smeštaj i ishranu za rad i boravak na terenu	748	343342
1970	5298	Ostale naknade troškova zaposlenima, poslodavcima i drugim fizičkim licima	748	343342
1971	5300	Troškovi usluga na izradi proizvoda	750	343342
1972	5301	Troškovi usluga na izradi u vršenju usluga koje se smatraju učincima	750	343342
1973	5310	Troškovi usluga prevoza  u zemlji	751	343342
1974	5311	roškovi usluga prevoza u inostranstvu	751	343342
1975	5312	Troškovi PTT usluga u zemlji	751	343342
1976	5313	Toškovi PTT usluga u inostranstvu	751	343342
1977	5314	Troškovi taksi i rent-a-car usluga	751	343342
1978	5315	Ostali troškovi transportnih usluga u zemlji	751	343342
1979	5316	Ostali troškovi transportnih usluga u inostranstvu	751	343342
1980	5320	Troškovi usluga održavanja građevinskih objekata u zemlji	752	343342
1981	5321	Troškovi usluga održavanja građevinskih objekata u inostranstvu	752	343342
1982	5322	Troškovi usluga održavanja postrojenja i opreme u zemlji	752	343342
1983	5323	Troškovi usluga održavanja postrojenja i opreme u inostranstvu	752	343342
1984	5324	Ostali troškovi usluga održavanja u zemlji	752	343342
1985	5325	Ostali troškovi usluga održavanja u inostranstvu	752	343342
1986	5330	Troškovi zakupa poslovnog prostora	753	343342
1987	5331	Troškovi zakupa skladišnog prostora	753	343342
1988	5332	Troškovi zakupa postrojenja i opreme	753	343342
1989	5333	Troškovi zakupa transportnih sredstava	753	343342
1990	5334	Troškovi zakupa poslovnog inventara i opreme	753	343342
1991	5335	Troškovi zakupa bioloških sredstava	753	343342
1992	5336	Ostali troškovi zakupnina	753	343342
1993	5340	Troškovi učešća na sajmovima u zemlji	754	343342
1994	5341	Troškovi učešća na sajmovima u inostranstvu	754	343342
1995	5350	Troškovi reklame i propagande u zemlji	755	343342
1996	5351	Troškovi reklame i propagande u inostranstvu	755	343342
1997	5352	Troškovi robe upotrebljene za reklamu i propagandu	755	343342
1998	5353	Troškovi sopstvenih proizvoda upotrebljenih za reklamu i propagandu	755	343342
1999	5354	Troškovi sopstvenih usluga upotrebljenih za reklamu i propagandu	755	343342
2000	5355	Ostali troškovi reklame i propagande	755	343342
2001	5360	Troškovi istraživanja usmereni ka sticanju novog znanja	756	343342
2002	5361	Troškovi israživanja alterntivnih proizvoda, sistema, usluga, materijala i sl.	756	343342
2003	5362	Troškovi istraživanja novih proizvoda, sistema, usluga, materijala i sl.	756	343342
2004	5363	Ostali troškovi istraživanja	756	343342
2005	5370	Troškovi razvoja koji se ne kapitalizuju (analitika po projektima)	757	343342
2006	5390	Troškovi usluga zaštite na radu	758	343342
2007	5391	Troškovi usluga prečišćavanja otpadnih voda	758	343342
2008	5392	Troškovi komunalnih usluga	758	343342
2009	5393	Troškovi ostalih usluga	758	343342
2010	5400	Troškovi amortiizacije nematerijalne imovine	760	343342
2011	5401	Troškovi amortiizacije nekretnina	760	343342
2012	5402	Troškovi amortizacije postrojenja i opreme	760	343342
2013	5403	Troškovi amortizacije investicionih nekretnina koje vrednuju po nabavnoj vrednosti	760	343342
2014	5404	Troškovi amortizacije ostalih nekretnina postrojenja i opreme	760	343342
2015	5410	Rezervisanja za troškove u garantnom roku za prodate proizvode	761	343342
2016	5411	Rezervisanja u garantnom roku za prodatu robu	761	343342
2017	5412	Rezervisanja za troškove u garantnom roku za izvršene usluge	761	343342
2018	5420	Rezervisanja za troškove obnavljanja zemljišta	762	343342
2019	5421	Rezervisanja za obnavljanje (reprodukciju) šuma	762	343342
2020	5422	Rezervisanja za obnavljanje ostalih prirodnih bogatstava	762	343342
2021	5430	roškovi rezervisanja za zadržane kaucije i depozite u zemlji	763	343342
2022	5431	Troškovi rezervisanja za zadržane kaucije i depozite u inostranstvu	763	343342
2023	5440	Rezervisanja za troškove prestanka rada jednog dela poslovanja	764	343342
2024	5441	Rezervisanja za troškove zatvaranja poslovnih lokacija	764	343342
2025	5442	Rezervisanja za troškove premeštanja poslovanja s jedne lokacije na drugu	764	343342
2026	5443	Rezervisanja za troškove promena u rukovodstvu	764	343342
2027	5444	Rezervisanja za troškove kompletne reorganizacije koja će uticati na prirodu i predmet poslovanja	764	343342
2028	5450	Rezervisanja za otpremnine prilikom odlaska u penziju	765	343342
2029	5451	Rezervisanja za jubilarne nagrade	765	343342
2030	5452	Rezervisanja za neiskorišćena plaćena odsustva	765	343342
2031	5453	Ostala rezervisanja za naknade i druge beneficije zaposlenih	765	343342
2032	5490	Rezervisanja po osnovu štetnih ugovora	766	343342
2033	5491	Rezervisanja za izdate garancije i druga jemstva	766	343342
2034	5492	Druga dugoročna rezervisanja	766	343342
2035	5500	Troškovi zdravstvenih usluga	768	343342
2036	5501	Troškovi advokatskih usluga	768	343342
2037	5502	Troškovi usluga čišćenja	768	343342
2038	5503	Troškovi računovodstva i revizije	768	343342
2039	5504	Stručnog obrazovanja zaposlenih	768	343342
2040	5505	Troškovi savetovanja i drugih intelekturalnih usluga pravnih lica	768	343342
2041	5506	Troškovi inelektualnih usluga fizičkih lica	768	343342
2042	5507	Troškovi drugih neproizvodnih usluga	768	343342
2043	5510	Troškovi treprezentacije u zemlji	769	343342
2044	5511	Troškovi reprezentacije u inostranstvu	769	343342
2045	5512	Troškovi sopstvenih proizvoda upotrebljenih za reprezentaciju	769	343342
2046	5513	Troškovi robe upotrebljene za reprezentaciju	769	343342
2047	5520	Troškovi premija osiguranja osnovnih sredstava	770	343342
2048	5521	Troškovi premija osiguranja zaliha	770	343342
2049	5522	Troškovi premija osiguranja potraživanja	770	343342
2050	5523	Troškovi premija osiguranja od odgovornosti prema trećim licima	770	343342
2051	5524	Troškovi osiguranja zaposlenih u toku rada	770	343342
2052	5525	Ostali troškovi premija osiguranja	770	343342
2053	5530	Troškovi platnog prometa u zemlji	771	343342
2054	5531	Troškovi platnog prometa u inostranstvu	771	343342
2055	5540	Troškovi članarina poslovnim udruženjima	772	343342
2056	5541	Troškovi članarina komorama	772	343342
2057	5542	Troškovi članarina međunarodnim organizacijama	772	343342
2058	5543	Ostali troškovi članarina	772	343342
2059	5550	Troškovi poreza na imovinu	773	343342
2060	5551	Troškovi korišćenja dobara od opšteg interesa	773	343342
2061	5552	Troškovi carina	773	343342
2062	5553	Ostali troškovi poreza	773	343342
2063	5560	Doprinosi za socijalno osiguranje vlasnika privrednog društva koji plaća doprinose po rešenju nadležnog organa	774	343342
2064	5590	Sudski troškovi	775	343342
2065	5591	Troškovi veštačenja	775	343342
2066	5592	Troškovi pretplate na časopise i stručne publikacije	775	343342
2067	5593	Ostali nematerijalni troškovi	775	343342
2068	5600	Rashodi kamata iz odnosa sa matičnim pravnim licima	777	343342
2069	5601	Rashodi kamata iz odnosa sa zavisnim pravnim licima	777	343342
2070	5602	Negativne kursne razlike iz odnosa sa matičnim privrednim društvima	777	343342
2071	5603	Negativne kursne razlike iz odnosa sa zavisnim pravnim licima	777	343342
2072	5604	Negativni efekti valutne klauzule iz odnosa sa matičnim privrednim društvima	777	343342
2073	5605	Negativni efekti valutne klauzule iz odnosa sa zavisnim pravnim licima	777	343342
2074	5606	Ostali finansijski rashodi iz odnosa sa matičnim pravnim licima	777	343342
2075	5607	osali finansijski rashodi iz odnosa sa zavisnim pravnim licima	777	343342
2076	5610	Rashodi kamata iz odnosa s ostalim povezanim pravnim licima	778	343342
2077	5611	Negativne kursne razlike iz odnosa sa ostalim povezanim pravnim licima	778	343342
2078	5612	Negativni efekti valutne klauzule iz odnosa sa ostalim pravnim licima	778	343342
2079	5613	Ostali finansijski rashodi iz odnosa s ostalim povezanim pravnim licima	778	343342
2080	5620	Rashodi kamata po finansijskim kreditima u zemlji	779	343342
2081	5621	Rashodi kamata po finansijskim kreditima u inostranstvu	779	343342
2082	5622	Porez po odbitku na kamatu po finansijskim kreditima u inostranstvu	779	343342
2083	5623	Rashodi kamata po osnovu finansijskog lizinga	779	343342
2084	5624	Rashodi kamata po robnim kreditima	779	343342
2085	5625	Rashodi ugovorenih i zateznih kamata po obavezama iz ostalih dužničko-poverilačkih odnosa	779	343342
2086	5626	Rashodi kamata po obavezama za poreze, doprinose i druge dažbine	779	343342
2087	5627	Ostali rashodi kamata	779	343342
2088	5630	Negativne kursne razlike po osnovu kredita i zajmova u stranoj valuti	780	343342
2089	5631	Negativne kursne razlike po osnovu ostalih obaveza i potraživanja u stranoj valuti	780	343342
2090	5632	Negativne kursne razlike po osnovu novčanih sredstava u stranoj valuti	780	343342
2091	5640	Rashodi po osnovu efekata valutne klauzule po osnovu kredita i zajmova sa ugovorenom valutnom klauzulom	781	343342
2092	5641	Rashodi po osnovu efekata valutne klauzule po osnovu ugovora o finansijskom lizingu	781	343342
2093	5642	Rashodi po osnovu efekata valutane klauzule po osnovu ostalih obaveza i potraćivanja sa ugovorenom valutnom klauzulom	781	343342
2094	5650	Rashodi od učešća u gubiatku pridruženih pravnih lica	782	343342
2095	5651	Rashodi od učešća u gubitku zajedničkih poduhvata	782	343342
2096	5660	Rashodi po osnovu efekata ugovorene zaštite od rizika (analitika po vrstama ugovorene zaštite od rizika)	980	343342
2097	5690	Finansijski rashodi po osnovu kasa skonta	783	343342
2098	5691	Zavisni troškovi nabavke hartija od varednosti kojima se trguje	783	343342
2099	5692	Rashodi u vezi sa eskontovanjem menica	783	343342
2100	5693	Drugi finansijski rashodi	783	343342
2101	5700	Gubitak od prodaje nematerijalnih ulaganja	785	343342
2102	5701	Gubitak od rashodovanja nematerijalnih ulaganja	785	343342
2103	5702	Gubitak od prodaje nekretnine	785	343342
2104	5703	Gubitak od rashodovanja (rušenja) nekretnine	785	343342
2105	5704	Gubitak od prodaje postrojenja i opreme	785	343342
2106	5705	Gubitak od rashodovanja postrojenja i opreme	785	343342
2107	5706	Ostali gubici po osnovu rashodovanja i otpisa nematerijalnih ulaganja, nekretnina, postrojenja i opreme	785	343342
2108	5710	Gubici po osnovu rashodovanja šuma	786	343342
2109	5711	Gubici po osnovu prodaje šuma	786	343342
2110	5712	Gubici po osnovu rashodovanja višegodišnjih zasada	786	343342
2111	5713	Gubici po osnovu prodaje višegodišnjih zasada	786	343342
2112	5714	Gubici po osnovu rashodovanja osnovnog stada	786	343342
2113	5740	Manjkovi osnovnih sredstava	789	343342
2114	5741	Manjkovi materijala	789	343342
2115	5742	Manjkovi robe	789	343342
2116	5743	Manjkovi alata i inventara	789	343342
2117	5744	Manjkovi rezervnih delova	789	343342
2118	5745	Manjkovi gotovine	789	343342
2119	5746	Drugi manjkovi	789	343342
2120	5750	Rashodi po osnovu efekta ugovorene revalorizacije	790	343342
2121	5751	Rashodi po osnovu ostalih oblika ugovorene zaštite od rizika	790	343342
2122	5760	Rashodi po osnovu direktnih otpisa kratkoročnih potraživanja u zemlji	791	343342
2123	5761	Rashodi po osnovu direktnih otpisa kratkoročnih potraživanja u inostranstvu	791	343342
2124	5762	Rashodi po osnovu direktnih otpisa dugoročnih potraživanja u zemlji	791	343342
2125	5763	Rashodi po osnovu direktnih otpisa dugoročnih potraživanja u inostranstvu	791	343342
2126	5770	Rashodi po osnovu rashodovanja zaliha materijala	792	343342
2127	5771	Rashodi po osnovu rashodovanja zaliha robe	792	343342
2128	5790	Kazne za privredne prestupe i prekršaje	793	343342
2129	5791	Penali	793	343342
2130	5792	Izgubljene kapare	793	343342
2131	5793	Naknade šteta drugim licima	793	343342
2132	5794	Izdaci za humanitarne, kulturne zdravstvene, obrazovne, naučne, verske i sportske namene	793	343342
2133	5795	Ostali nepomenuti rashodi	793	343342
2134	5800	Obezvređenje šuma	795	343342
2135	5801	Obezvređenje višegodišnjih zasada	795	343342
2136	5802	Obezvređenje osnovnog stada	795	343342
2137	5810	Obezvređenje ulaganja u razvoj	796	343342
2138	5811	Oezvređenje koncesija, patenata, licenci, robnih i uslužnih marki	796	343342
2139	5812	Obezvređenje softvera i ostalih prava	796	343342
2140	5813	Obezvređenje ostalih nematerijalnih ulaganja	796	343342
2141	5820	Obezvređenje nekretnina	797	343342
2142	5821	Obezvređenje postrojenja i opreme	797	343342
2143	5830	Obezvređenje dugoročnih finansijskih plasmana	798	343342
2144	5831	Obezvređenje drugih hartija od vrednosti	798	343342
2145	5840	Obezvređenje zaliha robe	799	343342
2146	5841	Obezvređenje zaliha materijala	799	343342
2147	5850	Obezvređenje potraživanja od matičnih pravnih lica	800	343342
2148	5851	Obezvređenje potraživanja od zavisnih pravnih lica	800	343342
2149	5852	Obezvređenje potraživanja od ostalih povezanih pravnih lica	800	343342
2150	5853	Obezvređenje potraživanja od kupaca u zemlji	800	343342
2151	5854	Obezvređenje potraživanja od kupaca u inostranstvu	800	343342
2152	5855	Obezvređenje dugoročnih potraživanja	800	343342
2153	5856	Obezvređenje kratkoročnih finansijskih plasmana	800	343342
2154	5857	Obezvređenje kratkoročnih hartija od vrednosti	800	343342
2155	5890	Obezvređenje potraživanja za avanse	801	343342
2156	5891	Obezvređenje gudvila	801	343342
2157	5892	Negativan efekat procene fer vrednosti investicionih nekretnina	801	343342
2158	5893	Obezvređenje ostale imovine	801	343342
2159	5900	Gubitak poslovanja koje se obustavlja (analitika po poslovanjima)	803	343342
2160	5910	Rashodi po osnovu efekata promene računovodstvene politike koji nisu materijalno značajni	804	343342
2161	5920	Rashodi po osnovu ispravke grešaka iz ranijih godina koje nisu materijalno značajne	981	343342
2162	6000	Prihodi od prodaje robe matičnim pravnim licima na domaćem tržištu	808	343342
2163	6001	Prihodi od prodaje robe zavisnim pravnim licima na domaćem tržištu	808	343342
2164	6010	Prihodi od prodaje robe matičnim prvnim licima na inostranom tržištu	809	343342
2165	6011	Prihodi od prodaje robe zavisnim pravnim licima na inostranom tržištu	809	343342
2166	6020	Prihodi od prodaje robe ostalim povezanim pravnim licima na domaćem tržištu	810	343342
2167	6030	Prihodi od prodaje robe ostalim povezanim pravnim licima na inostranom tržištu	811	343342
2168	6031	Prihodi od prodaje robe ostalim povezanim pravnim licima na inostranom tržištu	811	343342
2169	6040	Prihodi od prodaje robe na veliko	982	343342
2170	6041	Prihodi od prodaje robe u maloprodajnim objektima	982	343342
2171	6042	Prihodi od prodaje robe u komisionu	982	343342
2172	6043	Prihodi od prodaje robe u tranzitu	982	343342
2173	6044	Prihodi od prodaje stalnih sredstava namenjenih prodaji	982	343342
2174	6045	Ostali prihodi od prodaje robe	982	343342
2175	6050	Prihodi od samostalne prodaje robe na inostranom tržištu	983	343342
2176	6051	Prihodi od prodaje robe na inostranom tržištu posredstvom izvoznika	983	343342
2177	6052	Prihodi od prodaje robe u reeksportu	983	343342
2178	6100	Prihodi od prodaje proizvoda matičnim pravnim licima na domaćem tržištu	813	343342
2179	6101	Prihodi od prodaje proizvoda zavisnim pravnim licima na domaćem tržištu	813	343342
2180	6102	Prihodi od izvršenih usluga matičnim pravnim licima na domaćem tržištu	813	343342
2181	6103	Prihodi od izvršenih usluga zavisnim pravnim licima na domaćem tržištu	813	343342
2182	6110	Prihodi od prodaje proizvoda matičnim pravnim licima na inostranom tržištu	814	343342
2183	6111	Prihodi od prodaje proizvoda zavisnim pravnim licima na inostranom tržištu	814	343342
2184	6112	Prihodi od izvršenih usluga matičnim pravnim licima na inostranom tržištu	814	343342
2185	6113	Prihodi od izvršenih usluga zavisnim pravnim licima na inostranom tržištu	814	343342
2186	6120	Prihodi od prodaje proizvoda ostalim povezanim pravnim licima na domaćem tržištu	815	343342
2187	6121	Prihodi od izvršenih usluga ostalim povezanim pravnim licima na domaćem tržištu	815	343342
2188	6130	Prihodi od prodaje proizvoda ostalim oovezanim pravnim licima na inostranom tržištu	816	343342
2189	6131	Prihodi od izvršenih usluga ostalim povezanim pravnim licima na inostranom tržištu	816	343342
2190	6140	Prihodi od prodaje proizvoda u prometu na veliko	984	343342
2191	6141	Prihodi od prodaje proizvoda u prometu na malo	984	343342
2192	6142	Prihodi od prodaje usluga	984	343342
2193	6150	Prihodi od samostalne prodaje proizvoda na inostranom tržištu	985	343342
2194	6151	Prihodi od prodaje proizvoda preko izvoznika	985	343342
2195	6152	Prihodi od iizvršenih usluga na inostranom tržištu	985	343342
2196	6200	Prihodi od upotrebe robe za nekretnine, postrojenja, opremu i biološka sredstva	818	343342
2197	6201	Prihodi od upotrebe robe za materijal	818	343342
2198	6202	Prihodi od upotrebe robe za rezervne delove	818	343342
2199	6203	Prihodi od upotrebe robe za reprezentaciju	818	343342
2200	6204	Ostali prihodi od aktiviranja ili potrošnje robe za sopstvene potrebe	818	343342
2201	6210	Prihodi po osnovu upotrebe usluga za nematerijalna ulaganja	822	343342
2202	6211	Prihodi po osnovu upotrebe proizvoda za nekretnine, postrojenja i opremu	822	343342
2203	6212	Prihodi po osnovu upotrebe usluga za nekretnine, postrojenja i opremu	822	343342
2204	6213	Prihodi po osnovu prirasta osnovnog stada	822	343342
2205	6214	Prihodi po osnovu upotrebe proizvoda za materijal (kada e vode na kontu 101)	822	343342
2206	6740	Viškovi gotovine i gotovinskih ekvivalenata	847	343342
2207	6215	Prihodi po osnovu upotrebe proizvoda za rezervne delove (kada se vode na kontu 100)	822	343342
2208	6216	Prihodi po osnovu upotrebe sopstvenog transporta pri nabavci materijala	822	343342
2209	6217	Ostali prihodi od aktiviranja ili potrošnje proizvoda i usluga za sopstvene potrebe	822	343342
2210	6300	Povećanje vrednosti zaliha nedovršenih proizvoda	824	343342
2211	6301	Povećanje vrednosti nedovršenih usluga	824	343342
2212	6302	Povećanje vrednosti zaliha gotovih proizvoda	824	343342
2213	6310	Smanjenje varednosti zaliha nedovršenih proizvoda	825	343342
2214	6311	Smanjenje vrednosti nedovršenih usluga	825	343342
2215	6312	Smanjenje vrednosti zaliha gotovih proizvoda	825	343342
2216	6400	Prihodi od premija	827	343342
2217	6401	Prihodi od subvencija	827	343342
2218	6402	Prihodi od dotacija	827	343342
2219	6403	Prihodi od donacija	827	343342
2220	6404	Prihodi od regresa	827	343342
2221	6405	Prihodi od kompenzacija	827	343342
2222	6406	Prihodi od povraćaja poreskih dažbina	827	343342
2223	6410	Prihodi po osnovu uslovljenih donacija (analitika po projektima)	828	343342
2224	6500	Prihodi od iznajmljivanja zemljišta	830	343342
2225	6501	Prihodi od iznajmljivanja poslovnog prostora	830	343342
2226	6502	Prihodi od iznajmljivanj skladištnog prostora	830	343342
2227	6503	Prihodi od iznajmljivanja stanova	830	343342
2228	6504	Prihodi od iznajmljivanja opreme	830	343342
2229	6505	Prihodi od iznajmljivanja transportnih sredstava	830	343342
2230	6506	Ostali prihodi od zakupnina	830	343342
2231	6510	Prihodi od članarina poslovnog udruženja	831	343342
2232	6511	Drugi prihodi od članarina	831	343342
2233	6520	Prihodi od naknada po osnovu patenata	832	343342
2234	6521	Prihodi po osnovu naknada po osnovu žigova	832	343342
2235	6522	Prihodi po osnovu autorskih prava	832	343342
2236	6523	Prihodi po osnovu franšiza	832	343342
2237	6524	Prihodi po osnovu softvera	832	343342
2238	6525	Prihodi po osnovu ostalih prava	832	343342
2239	6600	Prihodi od kamata iz odnosa sa matičnim pravnim licima	835	343342
2240	6601	Prihodi od kamata iz odnosa sa zavisnim pravnim licima	835	343342
2241	6602	Pozitivne kursne razlike iz odnosa sa matičnim pravnim licima	835	343342
2242	6603	Pozitivne kursne razlike iz odnosa sa zavisnim pravnim licima	835	343342
2243	6604	Pozitivni efekti valutne klauzule iz odnosa sa matičnim pravnim licima	835	343342
2244	6605	Pozitivni efekti valutne klauzule iz odnos sa zavisnim pravnim licima	835	343342
2245	6606	Ostali finansijski prihodi iz odnosa sa matičnim pravnim licima	835	343342
2246	6607	Ostali finansijski prihodi iz odnosa sa zavisnim pravnim licima	835	343342
2247	6590	Ostali poslovni prihodi	833	343342
2248	6610	Prihodi od kamata iz odnos s ostalim povezanim pravnim licima	836	343342
2249	6611	Pozitivne kursne razlike iz odnos sa ostalim povezanim pravnim licima	836	343342
2250	6612	Pozitivni efekti valutne klauzule iz odnosa s ostalim povezanim pravnim licima	836	343342
2251	6613	Ostali finansijski porihodi iz odnosa s ostalim povezanim pravnim licima	836	343342
2252	6620	Prihodi od kamat po finansijskim kreditima i zajmovima u zemlji	837	343342
2253	6621	Prihodi od kamata po finansijskim kreditima i zajmovima u inostranstvu	837	343342
2254	6622	Prihodi od kamata po robnim kreditima	837	343342
2255	6623	Prihodi od kamata po finansijskom lizingu	837	343342
2256	6624	Prihodi od ugovorenih i zateznih kamata po potraživanjima iz dužničko-poverilačkih odnosa	837	343342
2257	6625	Ostali prihodi od kamata	837	343342
2258	6630	Pozitivne kursne razlike po kreditima i zajmovima u stranoj valuti	838	343342
2259	6631	Pozitivne kursne razlike po ostalim potraživanjima i obavezama u stranoj valuti	838	343342
2260	6632	Pozitivne kursne razlike po novčanim sredstvima u stranoj valuti	838	343342
2261	6640	Prihodi po osnovu efekata valutne klauzule po kreditima i zajmovima sa ugovorenom valutnom klauzulom	839	343342
2262	6641	Prihodi po osnovuefekata valutne klauzule po ugovoru o finansijskom lizingu	839	343342
2263	6642	Prihodi po osnovu efekata valutne klauzule po ostalim potraživanjima i obavezama sa ugovorenom valutnom klauzulom	839	343342
2264	6650	Prihodi po osnovu učešća u dobitku pridruženih pravnih lica u zemlji	840	343342
2265	6651	Prihodi po osnovu učešća u dobitku pridruženih pravnih lica u inostranstvu	840	343342
2266	6652	Prihodi po osnovu učešća u dobitku zajedničkih poduhvata u zemlji	840	343342
2267	6653	Prihodi po osnovu učešća u dobitku zajedničkih poduhvata u inostranstvu	840	343342
2268	6690	Prihodi po osnovu iskorišćenog kasa skonta	841	343342
2269	6691	Prihodi od dividendi	841	343342
2270	6692	Prihodi od raspoređene dobiti	841	343342
2271	6693	Ostali finansijski prihodi	841	343342
2272	6700	Dobici od prodaje nematerijalnih ulaganja	843	343342
2273	6701	Dobici od prodaje nekretnina	843	343342
2274	6702	Dobici od prodaje postrojenja i opreme	843	343342
2275	6710	Dobici od prodaje šuma	844	343342
2276	6711	Dobici od prodaje višegodišnjih zasada	844	343342
2277	6712	Dobici od prodaje osnovnog stada	844	343342
2278	6720	Dobici od prodaje učešća	845	343342
2279	6721	Dobici od prodaje hartija od vrednosti	845	343342
2280	6730	Dobici od prodaje osnovog materijala	846	343342
2281	6731	Dobici od prodaje rezervnih delova	846	343342
2282	6732	Dobici od prodaje ostalog materijala	846	343342
2283	6741	Viškovi materijala	847	343342
2284	6742	Viškovi robe	847	343342
2285	6743	Viškovi stalnih sredstava	847	343342
2286	6744	Ostali viškovi	847	343342
2287	6750	Naplaćena otpisana kratkoročna potraživanja	848	343342
2288	6751	Naplaćena otpisana dugoročna potraživanja	848	343342
2289	6760	Prihodi po osnovu efekata ugovorene revalorizacija	849	343342
2290	6761	Prihodi po osnovu efekata ostalih instrumenata zaštite od rizika	849	343342
2291	6770	Prihodi od smanjenja obaveza po osnovu zakona	850	343342
2292	6771	Prihodi od smanjenja obaveza po osnovu vanparničkog poravnanja	850	343342
2293	6772	Prihodi od smanjenja obaveza po osnovu oprosta duga	850	343342
2294	6773	Ostali nepomenuti prihodi od smanjenja obaveza	850	343342
2295	6780	Prihodi od neiskorišćenih dugoročnih rezervisnja za troškove u garantnom roku	851	343342
2296	6781	Prihodi od neiskorišćenih dugoročnih rezervisanja za troškove obnavljanja prirodnih bogatstava	851	343342
2297	6782	Prihodi od neiskorišćenih dugoročnih rezervisanja za zadržane kaucije i depozite	851	343342
2298	6783	Prihodi od neiskorišćenih dugoročnih rezervisanja za troškove restrukturiranja	851	343342
2299	6784	Prihodi od neiskorišćenih ostalih dugoročnih rezervisanja	851	343342
2300	6785	Prihodi od neiskorišćenih kratkoročnih rezervisanja	851	343342
2301	6790	Prihodi po osnovu povraćaja poreza, kdoprinosa, taksa	852	343342
2302	6791	Prihodi po osnovu naplaćenih: penala, kapare, izgubljene dobiti, prihoda po osnovu naknade štete i dr.	852	343342
2303	6792	Prihodi po osnovu veće fer vrednosti pripojene neto imovine u odnosu na trošak sticanja (tzv. negativni gudvil)	852	343342
2304	6793	Prihodi po osnovu popusta koji su odobreni nakon sastavljanja finansijskih izveštaja	852	343342
2305	6794	Primljeni pokloni	852	343342
2306	6795	Ostali nepomenuti prihodi	852	343342
2307	6800	Prihodi od usklađivanja vrednosti šuma	854	343342
2308	6801	Prihodi od usklađivanja vrednosti višegodišnjih zasada	854	343342
2309	6802	Prihodi od usklađivanja vrednosti osnovnog stada	854	343342
2310	6810	Prihodi od usklađivanja vednosti ulaganja u razvoj	855	343342
2311	6811	Prihodi od usklađivanja vrednosti koncesija ptenata, licenci, robnih i uslužnih marki	855	343342
2312	6812	Prihodi od usklađivanja vrednosti softvera i ostalih prava	855	343342
2313	6813	Prihodi od usklađivanja vrednosti ostalih nemaerijalnih ulaganja	855	343342
2314	6820	Prihodi od usklađivanja vrednosti nekretnina	856	343342
2315	6821	Prihodi od usklađivanja vrednosti postrojenja i opreme	856	343342
2316	6830	Prihodi od usklađivanja vrednosti učešća u kapitalu	857	343342
2317	6831	Prihodi od usklađivanja vrednosti hartija od vrednosti	857	343342
2318	6832	Ostali prihodi od usklađivanja vrednosti	857	343342
2319	6840	Prihodi od usklađivanja vrednosti zaliha robe	858	343342
2320	6841	Prihodi od usklađivanja vrednosti zaliha materijala	858	343342
2321	6850	Prihodi od usklađivanja vrednosti kratkoročnih potraživanja	859	343342
2322	6851	Prihodi od usklađivanja vrednosti dugoročnih potraživanja	859	343342
2323	6852	Prihodi od usklađivanja vrednosti kratkoročnih finansijskih plasmana	859	343342
2324	6853	Prihodi od usklađivanja vrednosti hartija od vrednosti	859	343342
2325	6854	Prihodi od usklađivanja vrednosti ostalih kartkoročnih finansijskih plasmana	859	343342
2326	6890	Vrednosno usklađivanje potraživanja za avanse	860	343342
2327	6891	Poziivan efekat procene fer vrednosti investicionih nekretnina	860	343342
2328	6892	Prihodi od usklađivanja vrednosti ostale imovine	860	343342
2329	6900	Dobitak poslovanja koje se obustavlja (analitika po poslovanjima)	862	343342
2330	6910	Prihodi od efekata promene računovodstvenih politika	863	343342
2331	6920	Prihodi po osnovu ispravki grešaka iz ranijih godina koje nisu materijalno značajne	986	343342
2332	7200	Dobitak	873	343342
2333	7201	Gubitak	873	343342
2334	7210	Obračunati porez na dobit pravnih lica	874	343342
2335	7220	Odloženi poreski rashodi	875	343342
2336	7221	Odloženi poreski prihodi	875	343342
2337	7230	Neto prihod preduzetnika isplaćen u toku godine 	876	343342
2338	7240	Prenos dobitka	877	343342
2339	7241	Prenos gubitka	877	343342
2340	8800	Zemljište uzeto u zakup	987	343342
2341	8801	Građevinski objekti uzeti u zakup	987	343342
2342	8810	Preuzeti proizvodi za zajedničko poslovanje	988	343342
2343	8803	Ostala tuđa sredstva uzeta u operativni lizing (zakup)	987	343342
2344	8802	Oprema uzeta u zakupu	987	343342
2345	8811	Preuzeta roba za zajedničko poslovanje	988	343342
2346	8820	Roba uzeta u komision	989	343342
2347	8821	Roba uzeta u konsignaciju	989	343342
2348	8830	Materijal primljen na obradu i doradu	990	343342
2349	8831	Roba primljena na obradu i doradu	990	343342
2350	8832	Oprema drugih lica na popravci	990	343342
2351	8833	Proizvodi drugih lica u obradi i doradi	990	343342
2352	8840	Data jemstva	991	343342
2353	8841	Dati avali	991	343342
2354	8842	Date garancije	991	343342
2355	8843	Date menice	991	343342
2356	8850	Bonovi za ishranu	992	343342
2357	8851	Ulaznice	992	343342
2358	8852	Primljene menice	992	343342
2359	8853	Druge hartije od vrednosti	992	343342
2360	8890	Roba drugih lica preuzeta radi izvoza	993	343342
2361	8891	Roba primljena u komision ili konsignaciju	993	343342
2362	8892	Ambalaža drugih lica	993	343342
2363	8893	Tuđa roba u skladištu	993	343342
2364	8894	Ostala imovina kod drugih subjekata analitički kontni plan	993	343342
2365	8900	Obaveze za zemljište uzeto u zakup	994	343342
2366	8901	Obaveze za građevinske objekte uzete u zakup	994	343342
2367	8902	Obaveze za opremu uzetu zakupu	994	343342
2368	8903	Obaveze za ostala tuđa sredstva uzeta u operativni lizing (zakup)	994	343342
2369	8910	Obaveze za preuzete proizvode za zajedničko poslovanje	995	343342
2370	8911	Obaveze za preuzetu robu za zajedničko poslovanje	995	343342
2371	8920	Obaveze za arobu uzetu u komision	996	343342
2372	8921	Obaveze za robu uzetu u konsignaciju	996	343342
2373	8930	Obaveze za materijal primljen na obradu i doradu	997	343342
2374	8931	Obaveze za robu primljenu na obradu i doradu	997	343342
2375	8932	Obaveze za opremu drugih lica na popravci	997	343342
2376	8933	Obaveze za proizvode drugih lica u obradi i doradi	997	343342
2377	8940	Obaveze za data jemstva	998	343342
2378	8941	Obaveze za date avale	998	343342
2379	8942	Obaveze za date garancije	998	343342
2380	8943	Obaveze za date menice	998	343342
2381	8950	Obaveze za bonove za ishranu	999	343342
2382	8951	Obaveze za ulaznice	999	343342
2383	8952	Obaveze za primljene menice	999	343342
2384	8953	Obaveze za druge hartije od vrednosti	999	343342
2385	8990	Obaveze za robu drigih lica preuzetu radi izvoza	1001	343342
2386	8991	Obaveze za robu primljenu u komsion ili konsignaciju	1001	343342
2387	8992	Obaveze za ambalažu drugih lica	1001	343342
2388	8993	Obaveze za tuđu robu u skladištu	1001	343342
2389	8994	Obaveze za ostalu imovinu kod drugih subjekata	1001	343342
2390	9000	Račun za preuzimanje zaliha materijala	885	343342
2391	9001	Račun za preuzimanje zaliha neodvršene proizvodnje	885	343342
2392	9002	Račun za preuzimanje nezavršenih usluga	885	343342
2393	9003	Račun za preuzimanje zaliha gotovih proizvoda	885	343342
2394	9004	Račun za preuzimanje zaliha robe	885	343342
2395	9010	Račun za preuzimanje nabavke materijala	886	343342
2396	9011	Račun za preuzimanje nabavke robe	886	343342
2397	9021	Račun za preuzimanje troškova materijala	887	343342
2398	9022	Račun za preuzimanje troškova zarada, naknada zarada i ostalih ličnih rashoda	887	343342
2399	9023	Račun za preuzimanje troškova proizvodnih usluga	887	343342
2400	9024	Račun za preuzimanje troškova amortizacije i rezervisanja	887	343342
2401	9025	Račun za preuzimanje nematerijalnih troškova	887	343342
2402	9026	Račun za preuzimanje ostalih rashoda	887	343342
2403	9030	Preuzeti prihodi od prodaje proizvoda i usluga	888	343342
2404	9031	Preuzeti prihodi od aktiviranja učinaka proizvoda i usluga	888	343342
2405	9032	Preuzeti prihodi od prodaje robe	888	343342
2406	9033	Preuzeti prihodi od aktiviranja i potrošnje robe za sopstvene potrebe	888	343342
2407	9034	Preuzeti prihodi od premija, subvencija, dotacija i sl.	888	343342
2408	9035	Preuzeti ostali prihodi	888	343342
2409	9100	Sirovine i materijal	890	343342
2410	9101	Pomoćni materijal	890	343342
2411	9102	Gorivo i mazivo	890	343342
2412	9103	Rezervni delovi	890	343342
2413	9104	Ostali materijal	890	343342
2414	9105	Alat, inventar i auto-gume	890	343342
2415	9106	Ambalaža	890	343342
2416	9107	Odstupanje od planskih cena	890	343342
2417	9108	Ispravka vrednosti zaliha materijala	890	343342
2418	9110	Roba u magacinu	891	343342
2419	9111	Roba u skladištu, stovarištu i prodavnicama kod drugih pravnih lica	891	343342
2420	9112	Roba u prometu na veliko	891	343342
2421	9113	Roba u prometu na malo	891	343342
2422	9114	Roba u obradi, doradi i manipulaciji	891	343342
2423	9118	Ukalkulisani porez na dodatu vrednost	891	343342
2424	9119	Ukalkulisana razlika u ceni robe	891	343342
2425	9120	Proizvodi u prodavnicama proizvođača	892	343342
2426	9121	Roba u prodavnici proizvođača	892	343342
2427	9122	Odstupanja od planskih cena proizvoda	892	343342
2428	9123	Ukalkulisana razlika u ceni proizvoda	892	343342
2429	9124	Ukalkulisana razlika u ceni robe	892	343342
2430	9128	Ukalkulisani porez na dodatu vrednost proizvoda	892	343342
2431	9129	Ukalkulisani porez na dodatu vrednost robe	892	343342
2432	920	Mesto troška nabavke	893	343342
2433	9200	Troškovi materijala mesta troška (dalje: MT) nabavke	2432	343342
2434	9201	Troškovi zarada, naknada zarada i ostali lični rashodi MT nabavke	2432	343342
2435	9202	Troškovi proizvodnih usluga MT nabavke	2432	343342
2436	9203	Troškovi amortizacije i rezervisanja MT nabavke	2432	343342
2437	9204	Nematerijalni troškovi MT nabavke	2432	343342
2438	9205	Povećani troškovi koji terete troškove perioda	2432	343342
2439	9206	Drugi direktni troškovi MT nabavke	2432	343342
2440	9207	Troškovi nabavke, tehničke uprave i pomoćnih delatnosti	2432	343342
2441	9208	Troškovi uprave, prodaje i sličnih aktivnosti	2432	343342
2442	9209	Prenos troškova MT nabavke	2432	343342
2443	921	Mesto troška tehničke uprave	893	343342
2444	9210	Troškovi materijala MT tehničke uprave	2443	343342
2445	9522	Troškovi zarada, naknada zarada i ostali lični rashodi	899	343342
2446	9211	Troškovi zarada, naknada zarada i ostali lični rashodi MT tehničke uprave	2443	343342
2447	9212	Troškovi proizvodnih usluga MT tehničke uprave	2443	343342
2448	9213	Troškovi amortizacije i rezervisanja MT tehničke uprave	2443	343342
2449	9214	Nematerijalni troškovi MT tehničke uprave	2443	343342
2450	9215	Povećani troškovi koji terete troškove perioda	2443	343342
2451	9216	Drugi direktni troškovi MT tehničke uprave	2443	343342
2452	9217	Troškovi nabavke i pomoćnih delatnosti	2443	343342
2453	9218	Troškovi uprave, prodaje i sličnih aktivnosti	2443	343342
2454	9219	Prenos troškova MT tehničke uprave	2443	343342
2455	922	Mesta troškova pomoćne delatnosti	893	343342
2456	9220	Računsi mesta troškova pomoćne delatnosti "M" (analitika po pomoćnim delatnostima)	2455	343342
2457	92200	Troškovi materijala pomoćne delatnosti (dalje: PD) "M"	2456	343342
2458	92201	Troškovi zarada, naknada zarada i ostali lični rashodi PD "M"	2456	343342
2459	92202	Troškovi proizvodnih usluga PD "M"	2456	343342
2460	92203	Troškovi amortizacije i rezervisanja PD "M"	2456	343342
2461	92204	Nematerijalni troškovi PD "M"	2456	343342
2462	92205	Povećani troškovi koji terete troškove perioda	2456	343342
2463	92206	Ddrugi didrektni troškovi PD "M"	2456	343342
2464	92207	Troškovi nabavke, tehničke uprave i pomoćne delatnosti	2456	343342
2465	92208	Troškovi uprave, prodaje i sličnih aktivnosti	2456	343342
2466	92209	Prenos troškova pomoćne delatnosti "M"	2456	343342
2467	930	Mesto troškova osnovna delatnosti	894	343342
2468	9300	Troškovi materijala izrade	2467	343342
2469	9301	Troškovi zarada, naknada zarada i ostali lični rashodi	2467	343342
2470	9302	Troškovi proizvodnih usluga	2467	343342
2471	9303	Troškovi amortizacije i rezervisanja	2467	343342
2472	9304	Nematerijalni troškovi	2467	343342
2473	9305	Drugi direktni troškovi	2467	343342
2474	9306	Troškovi nabavke, tehničke uprave i pomoćnih delatnosti	2467	343342
2475	9307	Troškovi uprave, prodaje i sličnih aktivnosti	2467	343342
2476	9308	Prenos troškova osnovne delatnosti	2467	343342
2477	931	Mesto troškova sporedna delatnost	894	343342
2478	9310	Troškovi materijala izrade	2477	343342
2479	9311	Troškovi zarada, naknada zarada i ostali lični rashodi	2477	343342
2480	9312	Troškovi proizvodnih usluga	2477	343342
2481	9313	Troškovi amortizacije i rezervisanja	2477	343342
2482	9314	Nematerijalni troškovi	2477	343342
2483	9315	Drugi direktni troškovi	2477	343342
2484	9316	Troškovi nabavke, tehničke uprave i pomoćnih aktivnosti	2477	343342
2485	9317	Troškovi uprave, prodaje i sličnih aktivnosti	2477	343342
2486	9318	Prenos troškova sporedne delatnosti	2477	343342
2487	940	Mesto troška uprave	895	343342
2488	9400	Troškovi materijala MT uprave	2487	343342
\.


--
-- Data for Name: account_layouts_laws; Type: TABLE DATA; Schema: private; Owner: postgres
--

COPY private.account_layouts_laws (id, name, date) FROM stdin;
134071	iz 2014	2014-09-01
343342	zakon iz 2014 - analitika	2014-12-30
\.


--
-- Data for Name: addresses; Type: TABLE DATA; Schema: private; Owner: postgres
--

COPY private.addresses (id, street_number, street_id, door_number, description) FROM stdin;
1	10	1		
\.


--
-- Data for Name: banks; Type: TABLE DATA; Schema: private; Owner: postgres
--

COPY private.banks (id, name, "PIB", "inVat", "MB", bank_account_code_old, short_name, bank_account_code) FROM stdin;
2	AGROINDUSTRIJSKO KOMERCIJALNA BANKA AIK BANKA AD BEOGRAD	100618836	t	06876366	\N	AIK BANKA	105
27	TELENOR BANKA AD BEOGRAD	100000049	t	17138669	\N	TELENOR BANKA AD BEOGRAD	115
21	PIRAEUS BANK AKCIONARSKO DRUŠTVO BEOGRAD	100000627	t	17082990	\N	PIRAEUS BANK AD BEOGRAD	125
15	"MARFIN BANK" AKCIONARSKO DRUŠTVO BEOGRAD	100003148	t	07534183	\N	"MARFIN BANK" AD BEOGRAD	145
7	DIREKTNA BANKA AD KRAGUJEVAC	101458655	t	07654812	\N	DIREKTNA BANKA AD	150
11	HALKBANK akcionarsko društvo Beograd	100895809	t	07601093	\N	HALKBANK akcionarsko društvo Beograd	155
4	BANCA INTESA AKCIONARSKO DRUŠTVO BEOGRAD	100001159	t	07759231	\N	BANCA INTESA	160
1	ADDIKO BANK AD BEOGRAD	100228215	t	07726716	\N	ADDIKO BANK	165
28	UNICREDIT BANK SRBIJA A.D. BEOGRAD	100000170	t	17324918	\N	UNICREDIT BANK SRBIJA A.D. BEOGRAD	170
3	ALPHA BANK SRBIJA A.D.,BEOGRAD	100001773	t	07736681	\N	ALPHA BANK	180
12	JUBMES BANKA A.D. BEOGRAD	100001829	t	07074433	\N	JUBMES BANKA A.D. BEOGRAD	190
5	BANKA POŠTANSKA ŠTEDIONICA AD BEOGRAD	100002549	t	07004893	\N	BANKA POŠTANSKA ŠTEDIONICA AD BEOGRAD	200
14	KOMERCIJALNA BANKA AD BEOGRAD	100001931	t	07737068	\N	KOMERCIJALNA BANKA AD BEOGRAD	205
22	ProCredit Bank A.D. Beograd	100000215	t	17335677	\N	ProCredit Bank A.D. Beograd	220
10	FINDOMESTIC BANKA AD BEOGRAD	100002313	t	17076841	\N	FINDOMESTIC BANKA AD BEOGRAD	240
9	EUROBANK AKCIONARSKO DRUŠTVO BEOGRAD	100002532	t	17171178	\N	EUROBANK AD	250
23	RAIFFEISEN BANKA a.d. Beograd	100000299	t	17335600	\N	RAIFFEISEN BANKA a.d. Beograd	265
25	SOCIÉTÉ GÉNÉRALE BANKA SRBIJA AD BEOGRAD	100000303	t	07552335	\N	SOCIÉTÉ GÉNÉRALE BANKA SRBIJA AD BEOGRAD	275
24	SBERBANK SRBIJA A.D. BEOGRAD	100000354	t	07792247	\N	SBERBANK SRBIJA A.D. BEOGRAD	285
26	SRPSKA BANKA A.D. BEOGRAD	100000387	t	07092288	\N	SRPSKA BANKA A.D. BEOGRAD	295
18	NLB BANKA A.D. BEOGRAD	101700234	t	08250499	\N	NLB BANKA A.D. BEOGRAD	310
20	OTP BANKA SRBIJA A.D. NOVI SAD	100584604	t	08603537	\N	OTP BANKA SRBIJA A.D. NOVI SAD	325
6	CRÉDIT AGRICOLE BANKA SRBIJA, AKCIONARSKO DRUŠTVO, NOVI SAD	101697525	t	08277931	\N	CREDIT AGRICOLE SRBIJA, AD NOVI SAD	330
8	ERSTE BANK A.D. NOVI SAD	101626723	t	08063818	\N	ERSTE BANK A.D. NOVI SAD	340
29	VOJVOĐANSKA BANKA AD NOVI SAD	101694252	t	08074313	\N	VOJVOĐANSKA BANKA AD NOVI SAD	355
17	MTS BANKA AKCIONARSKO DRUŠTVO, BEOGRAD	100017720	t	09081488	\N	MTS BANKA AD BEOGRAD	360
13	"JUGOBANKA JUGBANKA" A.D. KOSOVSKA MITROVICA	100018950	t	09023321	\N	"JUGOBANKA JUGBANKA" A.D. KOSOVSKA MITROVICA	365
19	OPPORTUNITY BANKA A.D. NOVI SAD	101643574	t	08761132	\N	OPPORTUNITY BANKA A.D. NOVI SAD	370
30	VTB Banka a.d. Beograd	105701111	t	20439866	\N	VTB Banka a.d. Beograd	375
16	MIRABANK AKCIONARSKO DRUŠTVO BEOGRAD	108851504	t	21080608	\N	MIRABANK AD	380
31	NARODNA BANKA SRBIJE	100041150	t	07007965	\N	NBS	908
32	MINISTARSTVO FINANSIJA-UPRAVA ZA TREZOR	103964453	t	17862146	\N	MINISTARSTVO FINANSIJA-UPRAVA ZA TREZOR	840
\.


--
-- Data for Name: cities; Type: TABLE DATA; Schema: private; Owner: postgres
--

COPY private.cities (id, name, country_id, default_postal_code_id, area_code, bzr_code, treasury_office_unit_id) FROM stdin;
1526	Барајево	3	78	\N	703494	174
1527	Баћевац	3	\N	\N	703508	174
1528	Бељина	3	108	\N	703516	174
1529	Бождаревац	3	1396	\N	703524	174
1530	Велики Борак	3	\N	\N	703532	174
1532	Гунцати	3	\N	\N	703567	174
1533	Лисовић	3	\N	\N	703575	174
1535	Мељак	3	811	\N	703591	174
1536	Рожанци	3	\N	\N	703605	174
1537	Шиљаковац	3	\N	\N	703613	174
1539	Вождовац	3	168	\N	791016	6
1542	Рипањ	3	\N	\N	703656	6
1545	Болеч	3	255	\N	703672	124
1546	Брестовик	3	\N	\N	703699	124
1547	Винча	3	1418	\N	703702	124
1548	Врчин	3	1463	\N	703729	124
1549	Гроцка	3	488	\N	703737	124
1550	Дражањ	3	\N	\N	703745	124
1551	Живковац	3	\N	\N	703753	124
1552	Заклопача	3	\N	\N	703761	124
1553	Калуђерица	3	559	\N	703770	124
1554	Камендол	3	\N	\N	703788	124
1555	Лештане	3	736	\N	703796	124
1556	Пударци	3	\N	\N	703800	124
1557	Ритопек	3	\N	\N	703818	124
1558	Умчари	3	1355	\N	703826	124
1559	Звездара	3	143	\N	791032	122
1560	Земун	3	207	\N	791059	103
1561	Угриновци	3	1353	\N	703915	103
1562	Араповац	3	\N	\N	703923	175
1563	Барзиловица	3	\N	\N	703931	175
1564	Барошевац	3	87	\N	703940	175
1565	Бистрица	3	\N	\N	703958	175
1566	Брајковац	3	\N	\N	703966	175
1567	Бурово	3	\N	\N	703974	175
1568	Велики Црљени	3	1397	\N	703982	175
1569	Врбовно	3	\N	\N	704008	175
1570	Вреоци	3	1465	\N	704016	175
1571	Дрен	3	\N	\N	704024	175
1572	Дудовица	3	420	\N	704032	175
1573	Зеоке	3	\N	\N	704067	175
1574	Јунковац	3	553	\N	704075	175
1575	Крушевица	3	\N	\N	704083	175
1576	Лазаревац	3	718	\N	704091	175
1577	Лесковац	3	\N	\N	704105	175
1578	Лукавица	3	\N	\N	704113	175
1579	Пркосава	3	\N	\N	704164	175
1580	Соколово	3	\N	\N	704202	175
1581	Степојевац	3	1258	\N	704229	175
1582	Стрмово	3	\N	\N	704237	175
1583	Стубица	3	\N	\N	704245	175
1584	Трбушница	3	\N	\N	704253	175
1585	Цветовац	3	\N	\N	704261	175
1586	Чибутковица	3	\N	\N	704270	175
1587	Шопић	3	\N	\N	704288	175
1588	Шушњар	3	\N	\N	704296	175
1589	Амерић	3	\N	\N	704300	7
1590	Белуће	3	\N	\N	704318	7
1591	Бељевац	3	\N	\N	704326	7
1592	Велика Иванча	3	1388	\N	704334	7
1593	Велика Крсна	3	1390	\N	704342	7
1594	Влашка	3	1437	\N	704369	7
1595	Границе	3	\N	\N	704377	7
1596	Дубона	3	\N	\N	704385	7
1597	Јагњило	3	521	\N	704393	7
1598	Ковачевац	3	628	\N	704407	7
1599	Кораћица	3	614	\N	704415	7
1600	Мала Врбица	3	\N	\N	704423	7
1601	Марковац	3	\N	\N	704431	7
1602	Међулужје	3	\N	\N	704440	7
1603	Младеновац (варош)	3	834	\N	704458	7
1604	Младеновац (село)	3	1546	\N	704466	7
1605	Пружатовац	3	1059	\N	704474	7
1606	Рабровац	3	\N	\N	704482	7
1607	Рајковац	3	\N	\N	704504	7
1608	Сенаја	3	\N	\N	704512	7
1609	Црквине	3	\N	\N	704539	7
1610	Шепшин	3	1166	\N	704547	7
1611	Нови Београд	3	148	\N	791067	104
1612	Баљевац	3	\N	\N	704555	176
1613	Барич	3	84	\N	704563	176
1614	Бело Поље	3	\N	\N	704571	176
1615	Бргулице	3	\N	\N	704580	176
1616	Бровић	3	\N	\N	704598	176
1617	Велико Поље	3	\N	\N	704601	176
1618	Вукићевица	3	\N	\N	704610	176
1619	Грабовац	3	475	\N	704628	176
1620	Дражевац	3	409	\N	704636	176
1621	Дрен	3	\N	\N	704644	176
1622	Забрежје	3	1480	\N	704652	176
1623	Звечка	3	\N	\N	704679	176
1624	Јасенак	3	\N	\N	704687	176
1625	Конатице	3	\N	\N	704695	176
1626	Кртинска	3	\N	\N	704709	176
1627	Љубинић	3	\N	\N	704717	176
1628	Мала Моштаница	3	769	\N	704725	176
1629	Мислођин	3	\N	\N	704733	176
1630	Обреновац	3	928	\N	704741	176
1631	Орашац	3	\N	\N	704750	176
1632	Пироман	3	\N	\N	704768	176
1633	Пољане	3	\N	\N	704776	176
1634	Ратари	3	\N	\N	704784	176
1635	Рвати	3	\N	\N	704792	176
1636	Скела	3	1189	\N	704806	176
1637	Стублине	3	1270	\N	704814	176
1638	Трстеница	3	\N	\N	704822	176
1639	Уровци	3	\N	\N	704849	176
1640	Ушће	3	\N	\N	704857	176
1641	Палилула	3	158	\N	791075	123
1642	Борча	3	263	\N	704865	123
1643	Велико Село	3	\N	\N	704873	123
1644	Дунавац	3	\N	\N	704881	123
1645	Ковилово	3	\N	\N	704890	123
1646	Овча	3	954	\N	704903	123
1647	Падинска Скела	3	957	\N	704911	123
1648	Сланци	3	1193	\N	704920	123
1649	Раковица	3	201	\N	791083	172
1650	Савски венац	3	1	\N	791091	156
1651	Бабе	3	\N	\N	704938	8
1652	Губеревац	3	\N	\N	704946	8
1653	Дрлупа	3	\N	\N	704954	8
1654	Дучина	3	\N	\N	704962	8
1655	Ђуринци	3	\N	\N	704989	8
1656	Мала Иванча	3	\N	\N	704997	8
1657	Мали Пожаревац	3	789	\N	705004	8
1658	Неменикуће	3	\N	\N	705012	8
1659	Парцани	3	\N	\N	705039	8
1660	Поповић	3	\N	\N	705047	8
1661	Раља	3	1076	\N	705055	8
1662	Рогача	3	1116	\N	705063	8
1663	Ропочево	3	\N	\N	705071	8
1664	Сибница	3	\N	\N	705080	8
1665	Слатина	3	\N	\N	705098	8
1666	Сопот	3	1220	\N	705101	8
1667	Стојник	3	\N	\N	705110	8
1668	Стари Град	3	151	\N	791105	157
1669	Бечмен	3	94	\N	703834	107
1670	Бољевци	3	257	\N	703842	107
1671	Добановци	3	370	\N	703869	107
1672	Јаково	3	526	\N	703877	107
1673	Петровчић	3	994	\N	703885	107
1674	Прогар	3	1053	\N	703893	107
1675	Сурчин	3	1297	\N	703907	107
1676	Чукарица	3	206	\N	791113	173
1677	Велика Моштаница	3	1369	\N	705128	173
1678	Остружница	3	953	\N	705136	173
1679	Пећани	3	\N	\N	705144	173
1680	Руцка	3	\N	\N	705152	173
1681	Рушањ	3	1131	\N	705179	173
1682	Сремчица	3	1547	\N	705187	173
1683	Умка	3	1356	\N	705195	173
1684	Багремово	3	\N	\N	800457	159
1685	Бајша	3	53	\N	800465	159
1686	Бачка Топола	3	33	\N	800473	159
1687	Бачки Соколац	3	42	\N	800481	159
1688	Богараш	3	\N	\N	800490	159
1689	Горња Рогатица	3	\N	\N	800503	159
1690	Гунарош	3	494	\N	800511	159
1691	Зобнатица	3	\N	\N	800520	159
1692	Кавило	3	\N	\N	800538	159
1693	Карађорђево	3	568	\N	800546	159
1694	Криваја	3	662	\N	800554	159
1695	Мали Београд	3	785	\N	800562	159
1696	Мићуново	3	\N	\N	800589	159
1697	Ново Орахово	3	925	\N	800597	159
1698	Његошево	3	874	\N	800619	159
1699	Оборњача	3	\N	\N	805122	159
1700	Томиславци	3	941	\N	800627	159
1701	Панонија	3	974	\N	800635	159
1702	Пачир	3	956	\N	800643	159
1703	Победа	3	1009	\N	800651	159
1704	Светићево	3	\N	\N	800660	159
1705	Средњи Салаш	3	\N	\N	800678	159
1706	Стара Моравица	3	1244	\N	800686	159
1707	Ловћенац	3	752	\N	802492	158
1708	Мали Иђош	3	786	\N	802506	158
1709	Фекетић	3	437	\N	802514	158
1710	Бајмок	3	52	\N	804452	195
1711	Бачки Виногради	3	43	\N	804479	195
1712	Бачко Душаново	3	\N	\N	804487	195
1713	Биково	3	236	\N	804495	195
1714	Вишњевац	3	1421	\N	804509	195
1715	Горњи Таванкут	3	1315	\N	804517	195
1716	Доњи Таванкут	3	1315	\N	804525	195
1717	Ђурђин	3	427	\N	804533	195
1718	Келебија	3	574	\N	804541	195
1719	Љутово	3	748	\N	804550	195
1720	Мала Босна	3	779	\N	804568	195
1721	Мишићево	3	832	\N	804576	195
1722	Нови Жедник	3	922	\N	804584	195
1723	Палић	3	427	\N	804592	195
1724	Стари Жедник	3	1247	\N	804606	195
1725	Суботица	3	1284	\N	804614	195
1726	Хајдуково	3	498	\N	804622	195
1727	Чантавир	3	322	\N	804649	195
1728	Шупљак	3	1295	\N	804657	195
1729	Банатски Двор	3	1493	\N	801348	30
1730	Банатско Вишњићево	3	\N	\N	801356	30
1731	Банатско Карађорђево	3	60	\N	801364	30
1732	Торак	3	\N	\N	801372	30
1733	Житиште	3	1493	\N	801399	30
1734	Међа	3	804	\N	801402	30
1735	Нови Итебеј	3	884	\N	801429	30
1736	Равни Тополовац	3	1093	\N	801437	30
1737	Српски Итебеј	3	1237	\N	801445	30
1738	Торда	3	1329	\N	801453	30
1739	Хетин	3	500	\N	801461	30
1740	Честерег	3	329	\N	801470	30
1741	Арадац	3	18	\N	801488	183
1742	Банатски Деспотовац	3	59	\N	801496	183
1743	Бело Блато	3	109	\N	801500	183
1744	Ботош	3	270	\N	801518	183
1745	Елемир	3	433	\N	801526	183
1746	Ечка	3	432	\N	801534	183
1747	Зрењанин	3	1504	\N	801542	183
1748	Јанков Мост	3	1548	\N	801569	183
1749	Клек	3	589	\N	801577	183
1750	Книћанин	3	599	\N	801585	183
1751	Лазарево	3	719	\N	801593	183
1752	Лукино Село	3	763	\N	801607	183
1753	Лукићево	3	762	\N	801615	183
1754	Меленци	3	810	\N	801623	183
1755	Михајлово	3	820	\N	801631	183
1756	Орловат	3	944	\N	801640	183
1757	Перлез	3	987	\N	801658	183
1758	Стајићево	3	1240	\N	801666	183
1759	Тараш	3	1314	\N	801674	183
1760	Томашевац	3	1325	\N	801682	183
1761	Фаркаждин	3	436	\N	801704	183
1762	Чента	3	326	\N	801712	183
1763	Александрово	3	13	\N	802522	31
1764	Војвода Степа	3	1444	\N	802549	31
1765	Нова Црња	3	876	\N	802557	31
1766	Радојево	3	1070	\N	802565	31
1767	Српска Црња	3	1236	\N	802573	31
1768	Тоба	3	1324	\N	802581	31
1769	Бочар	3	245	\N	802590	32
1770	Кумане	3	694	\N	802603	32
1771	Нови Бечеј	3	882	\N	802611	32
1772	Ново Милошево	3	924	\N	802620	32
1773	Банатска Дубица	3	64	\N	803723	33
1774	Бока	3	254	\N	803731	33
1775	Бусење	3	\N	\N	803740	33
1776	Јарковац	3	531	\N	803758	33
1777	Јаша Томић	3	533	\N	803766	33
1778	Конак	3	608	\N	803774	33
1779	Крајишник	3	647	\N	803782	33
1780	Неузина	3	856	\N	803804	33
1781	Сечањ	3	1156	\N	803812	33
1782	Сутјеска	3	1302	\N	803839	33
1783	Шурјан	3	1300	\N	803847	33
1784	Ада	3	2	\N	800015	161
1785	Мол	3	840	\N	800023	161
1786	Оборњача	3	\N	\N	800031	161
1787	Стеријино	3	\N	\N	800040	161
1788	Утрине	3	1359	\N	800058	161
1789	Адорјан	3	7	\N	801976	160
1790	Велебит	3	1549	\N	801984	160
1791	Долине	3	\N	\N	801992	160
1792	Зимонић	3	\N	\N	802000	160
1793	Кањижа	3	564	\N	802018	160
1794	Мале Пијаце	3	784	\N	802026	160
1795	Мали Песак	3	\N	\N	802034	160
1796	Мартонош	3	802	\N	802042	160
1797	Ново Село	3	\N	\N	802069	160
1798	Ором	3	945	\N	802077	160
1799	Тотово Село	3	1331	\N	802085	160
1800	Трешњевац	3	1334	\N	802093	160
1801	Хоргош	3	501	\N	802107	160
1802	Банатска Топола	3	65	\N	802115	39
1803	Банатско Велико Село	3	68	\N	802123	39
1804	Башаид	3	88	\N	802131	39
1805	Иђош	3	504	\N	802140	39
1806	Кикинда	3	576	\N	802158	39
1807	Мокрин	3	839	\N	802166	39
1808	Наково	3	\N	\N	802174	39
1809	Нови Козарци	3	888	\N	802182	39
1810	Руско Село	3	1133	\N	802204	39
1811	Сајан	3	1138	\N	802212	39
1812	Банатско Аранђелово	3	57	\N	802638	40
1813	Ђала	3	350	\N	802646	40
1814	Мајдан	3	774	\N	802662	40
1815	Нови Кнежевац	3	886	\N	802689	40
1816	Подлокањ	3	\N	\N	802697	40
1817	Рабе	3	\N	\N	802719	40
1818	Сигет	3	\N	\N	802727	40
1819	Српски Крстур	3	1238	\N	802654	40
1820	Филић	3	\N	\N	802735	40
1821	Богараш	3	247	\N	803677	162
1822	Горњи Брег	3	468	\N	803685	162
1823	Кеви	3	575	\N	803693	162
1824	Сента	3	1164	\N	803707	162
1825	Торњош	3	1330	\N	803715	162
1826	Банатски Моноштор	3	\N	\N	804835	163
1827	Врбица	3	1462	\N	804843	163
1828	Јазово	3	539	\N	804851	163
1829	Остојићево	3	951	\N	804860	163
1830	Падеј	3	958	\N	804878	163
1831	Санад	3	1145	\N	804886	163
1832	Црна Бара	3	338	\N	804894	163
1833	Чока	3	333	\N	804908	163
1834	Алибунар	3	15	\N	800066	127
1835	Банатски Карловац	3	67	\N	800074	127
1836	Владимировац	3	1430	\N	800082	127
1837	Добрица	3	374	\N	800104	127
1838	Иланџа	3	506	\N	800112	127
1839	Јаношик	3	529	\N	800139	127
1840	Локве	3	751	\N	800147	127
1841	Николинци	3	859	\N	800155	127
1842	Нови Козјак	3	889	\N	800163	127
1843	Селеуш	3	1160	\N	800171	127
1844	Банатска Паланка	3	56	\N	800732	22
1845	Банатска Суботица	3	62	\N	800759	22
1846	Бела Црква	3	98	\N	800767	22
1847	Врачев Гај	3	1447	\N	800775	22
1848	Гребенац	3	482	\N	800783	22
1849	Добричево	3	\N	\N	800791	22
1850	Дупљаја	3	424	\N	800805	22
1851	Јасеново	3	535	\N	800813	22
1852	Кајтасово	3	556	\N	800821	22
1853	Калуђерово	3	\N	\N	800830	22
1854	Крушчица	3	\N	\N	800848	22
1855	Кусић	3	703	\N	800856	22
1856	Црвена Црква	3	342	\N	800864	22
1857	Чешко Село	3	\N	\N	800872	22
1858	Ватин	3	1383	\N	801038	20
1859	Велико Средиште	3	1412	\N	801046	20
1860	Влајковац	3	1431	\N	801054	20
1861	Војводинци	3	1445	\N	801062	20
1862	Вршац	3	1471	\N	801089	20
1863	Вршачки Ритови	3	\N	\N	801097	20
1864	Гудурица	3	493	\N	801119	20
1865	Загајица	3	1481	\N	801127	20
1866	Избиште	3	514	\N	801135	20
1867	Јабланка	3	\N	\N	801143	20
1868	Куштиљ	3	706	\N	801151	20
1869	Мали Жам	3	\N	\N	801160	20
1870	Мало Средиште	3	\N	\N	801178	20
1871	Марковац	3	\N	\N	801186	20
1872	Месић	3	\N	\N	801194	20
1873	Орешац	3	\N	\N	801208	20
1874	Павлиш	3	981	\N	801216	20
1875	Парта	3	\N	\N	801224	20
1876	Потпорањ	3	\N	\N	801232	20
1877	Ритишево	3	\N	\N	801259	20
1878	Сочица	3	\N	\N	801267	20
1879	Стража	3	1264	\N	801275	20
1880	Уљма	3	1354	\N	801283	20
1881	Шушара	3	\N	\N	801291	20
1882	Дебељача	3	353	\N	802239	125
1883	Идвор	3	505	\N	802247	125
1884	Ковачица	3	629	\N	802255	125
1885	Падина	3	960	\N	802263	125
1886	Путниково	3	\N	\N	802271	125
1887	Самош	3	1144	\N	802280	125
1888	Уздин	3	1360	\N	802298	125
1889	Црепаја	3	336	\N	802301	125
1890	Баваниште	3	91	\N	802310	128
1891	Гај	3	442	\N	802328	128
1892	Делиблато	3	356	\N	802336	128
1893	Дубовац	3	417	\N	802344	128
1894	Ковин	3	632	\N	802352	128
1895	Мало Баваниште	3	\N	\N	802379	128
1896	Мраморак	3	846	\N	802387	128
1897	Плочица	3	1008	\N	802395	128
1898	Скореновац	3	1191	\N	802409	128
1899	Шумарак	3	\N	\N	802417	128
1900	Баранда	3	79	\N	802905	126
1901	Опово	3	937	\N	802913	126
1902	Сакуле	3	1140	\N	802921	126
1903	Сефкерин	3	1158	\N	802930	126
1904	Банатски Брестовац	3	58	\N	803057	190
1905	Банатско Ново Село	3	61	\N	803065	190
1906	Глогоњ	3	450	\N	803073	190
1907	Долово	3	378	\N	803081	190
1908	Иваново	3	513	\N	803090	190
1909	Јабука	3	517	\N	803103	190
1910	Качарево	3	555	\N	803111	190
1911	Омољица	3	935	\N	803120	190
1912	Панчево	3	3	\N	803138	190
1913	Старчево	3	1246	\N	803146	190
1914	Банатски Соколац	3	\N	\N	803324	21
1915	Барице	3	85	\N	803332	21
1916	Велика Греда	3	1387	\N	803359	21
1917	Велики Гај	3	1398	\N	803367	21
1918	Дужине	3	\N	\N	803375	21
1919	Јерменовци	3	544	\N	803383	21
1920	Купиник	3	696	\N	803391	21
1921	Лаудоновац	3	\N	\N	803405	21
1922	Маргита	3	797	\N	803413	21
1923	Марковићево	3	\N	\N	803421	21
1924	Милетићево	3	824	\N	803430	21
1925	Пландиште	3	1001	\N	803448	21
1926	Стари Лец	3	1249	\N	803456	21
1927	Хајдучица	3	824	\N	803464	21
1928	Апатин	3	17	\N	800180	150
1929	Купусина	3	698	\N	800198	150
1930	Пригревица	3	1046	\N	800201	150
1931	Свилојево	3	1309	\N	800210	150
1932	Сонта	3	1218	\N	800228	150
1933	Крушчић	3	670	\N	802425	19
1934	Кула	3	689	\N	802433	19
1935	Липар	3	739	\N	802441	19
1936	Нова Црвенка	3	877	\N	802450	19
1937	Руски Крстур	3	1132	\N	802468	19
1938	Сивац	3	1187	\N	802476	19
1939	Црвенка	3	344	\N	802484	19
1940	Бачки Брестовац	3	37	\N	802948	151
1941	Бачки Грачац	3	38	\N	802956	151
1942	Богојево	3	249	\N	802964	151
1943	Дероње	3	358	\N	802972	151
1944	Каравуково	3	570	\N	802999	151
1945	Лалић	3	712	\N	803006	151
1946	Оџаци	3	933	\N	803014	151
1947	Ратково	3	1089	\N	803022	151
1948	Српски Милетић	3	1239	\N	803049	151
1949	Алекса Шантић	3	10	\N	803855	193
1950	Бачки Брег	3	35	\N	803863	193
1951	Бачки Моноштор	3	40	\N	803871	193
1952	Бездан	3	233	\N	803880	193
1953	Гаково	3	444	\N	803898	193
1954	Дорослово	3	399	\N	803901	193
1955	Кљајићево	3	596	\N	803910	193
1956	Колут	3	606	\N	803928	193
1957	Растина	3	1085	\N	803936	193
1958	Риђица	3	1110	\N	803944	193
1959	Светозар Милетић	3	1305	\N	803952	193
1960	Сомбор	3	1550	\N	803979	193
1961	Станишић	3	1242	\N	803987	193
1962	Стапар	3	1243	\N	803995	193
1963	Телечка	3	1318	\N	804002	193
1964	Чонопља	3	334	\N	804029	193
1965	Бач	3	26	\N	800236	115
1966	Бачко Ново Село	3	46	\N	800244	115
1967	Бођани	3	246	\N	800252	115
1968	Вајска	3	1370	\N	800279	115
1969	Плавна	3	1004	\N	800287	115
1970	Селенча	3	1159	\N	800295	115
1971	Бачка Паланка	3	30	\N	800309	116
1972	Визић	3	\N	\N	800317	116
1973	Гајдобра	3	443	\N	800325	116
1974	Деспотово	3	361	\N	800333	116
1975	Карађорђево	3	567	\N	800341	116
1976	Младеново	3	835	\N	800350	116
1977	Нештин	3	855	\N	800368	116
1978	Нова Гајдобра	3	878	\N	800376	116
1979	Обровац	3	932	\N	800384	116
1980	Параге	3	978	\N	800392	116
1981	Пивнице	3	999	\N	800406	116
1982	Силбаш	3	1177	\N	800414	116
1983	Товаришево	3	1332	\N	800422	116
1984	Челарево	3	324	\N	800449	116
1985	Бачки Петровац	3	41	\N	800694	117
1986	Гложан	3	452	\N	800708	117
1987	Кулпин	3	693	\N	800716	117
1988	Маглић	3	773	\N	800724	117
1989	Баноштор	3	75	\N	800899	111
1990	Беочин	3	114	\N	800902	111
1991	Грабово	3	\N	\N	800929	111
1992	Луг	3	758	\N	800937	111
1993	Раковац	3	1075	\N	800945	111
1994	Свилош	3	\N	\N	800953	111
1995	Сусек	3	1301	\N	800961	111
1996	Черевић	3	327	\N	800970	111
1997	Бачко Градиште	3	45	\N	800988	18
1998	Бачко Петрово Село	3	47	\N	800996	18
1999	Бечеј	3	92	\N	801003	18
2000	Милешево	3	823	\N	801011	18
2001	Радичевић	3	1067	\N	801020	18
2002	Бачко Добро Поље	3	44	\N	804754	16
2003	Врбас	3	1459	\N	804827	16
2004	Змајево	3	1502	\N	804762	16
2005	Косанчић	3	\N	\N	805114	16
2006	Куцура	3	686	\N	804789	16
2007	Равно Село	3	1095	\N	804797	16
2008	Савино Село	3	1154	\N	804819	16
2009	Госпођинци	3	473	\N	801305	118
2010	Ђурђево	3	426	\N	801313	118
2011	Жабаљ	3	1476	\N	801321	118
2012	Чуруг	3	349	\N	801330	118
2013	Бегеч	3	96	\N	802743	112
2014	Будисава	3	303	\N	802751	112
2015	Буковац	3	306	\N	802760	112
2016	Ветерник	3	1414	\N	802778	112
2017	Каћ	3	554	\N	802786	112
2018	Кисач	3	587	\N	802794	112
2019	Ковиљ	3	630	\N	802808	112
2020	Лединци	3	723	\N	802816	112
2021	Нови Сад	3	1551	\N	802824	112
2022	Петроварадин	3	991	\N	802832	112
2023	Руменка	3	1129	\N	802859	112
2024	Сремска Каменица	3	1232	\N	802867	112
2025	Стари Лединци	3	\N	\N	805149	112
2026	Степановићево	3	1257	\N	802875	112
2027	Футог	3	438	\N	802883	112
2028	Ченеј	3	325	\N	802891	112
2029	Надаљ	3	850	\N	804037	17
2030	Србобран	3	1227	\N	804045	17
2031	Турија	3	1347	\N	804053	17
2032	Сремски Карловци	3	1235	\N	804355	114
2033	Бачки Јарак	3	39	\N	804665	119
2034	Сириг	3	1185	\N	804673	119
2035	Темерин	3	1319	\N	804681	119
2036	Вилово	3	1416	\N	804690	120
2037	Гардиновци	3	446	\N	804703	120
2038	Лок	3	750	\N	804711	120
2039	Мошорин	3	843	\N	804720	120
2040	Тител	3	1323	\N	804738	120
2041	Шајкаш	3	1139	\N	804746	120
2042	Бешка	3	232	\N	801739	105
2043	Инђија	3	510	\N	801747	105
2044	Јарковци	3	\N	\N	801755	105
2045	Крчедин	3	657	\N	801763	105
2046	Љуково	3	747	\N	801771	105
2047	Марадик	3	796	\N	801780	105
2048	Нови Карловци	3	885	\N	801798	105
2049	Нови Сланкамен	3	921	\N	801801	105
2050	Сланкаменачки Виногради	3	\N	\N	801810	105
2051	Стари Сланкамен	3	1251	\N	801828	105
2052	Чортановци	3	335	\N	801836	105
2053	Велика Ремета	3	\N	\N	801844	153
2054	Врдник	3	1464	\N	801852	153
2055	Гргетек	3	\N	\N	801879	153
2056	Добродол	3	\N	\N	801887	153
2057	Ириг	3	511	\N	801895	153
2058	Јазак	3	538	\N	801909	153
2059	Крушедол Прњавор	3	673	\N	801917	153
2060	Крушедол Село	3	673	\N	801925	153
2061	Мала Ремета	3	\N	\N	801933	153
2062	Нерадин	3	\N	\N	801941	153
2063	Ривица	3	\N	\N	801950	153
2064	Шатринци	3	\N	\N	801968	153
2065	Ашања	3	22	\N	803154	152
2066	Брестач	3	\N	\N	803162	152
2067	Деч	3	355	\N	803189	152
2068	Доњи Товарник	3	\N	\N	803197	152
2069	Карловчић	3	571	\N	803219	152
2070	Купиново	3	697	\N	803227	152
2071	Обреж	3	931	\N	803235	152
2072	Огар	3	934	\N	803243	152
2073	Пећинци	3	983	\N	803251	152
2074	Попинци	3	1018	\N	803260	152
2075	Прхово	3	1041	\N	803278	152
2076	Сибач	3	1169	\N	803286	152
2077	Сремски Михаљевци	3	1229	\N	803294	152
2078	Суботиште	3	1292	\N	803308	152
2079	Шимановци	3	1179	\N	803316	152
2080	Буђановци	3	302	\N	803472	154
2081	Витојевци	3	\N	\N	803499	154
2082	Вогањ	3	1441	\N	803502	154
2083	Грабовци	3	476	\N	803529	154
2084	Добринци	3	375	\N	803537	154
2085	Доњи Петровци	3	\N	\N	803545	154
2086	Жарковац	3	\N	\N	803553	154
2087	Кленак	3	590	\N	803561	154
2088	Краљевци	3	648	\N	803570	154
2089	Мали Радинци	3	\N	\N	803588	154
2090	Никинци	3	857	\N	803596	154
2091	Павловци	3	\N	\N	803600	154
2092	Платичево	3	1003	\N	803618	154
2093	Путинци	3	1061	\N	803626	154
2094	Рума	3	1126	\N	803634	154
2095	Стејановци	3	1255	\N	803642	154
2096	Хртковци	3	503	\N	803669	154
2097	Бешеновачки Прњавор	3	\N	\N	804061	194
2213	Богоштица	3	\N	\N	720348	89
2098	Бешеново	3	231	\N	804070	194
2099	Босут	3	269	\N	804088	194
2100	Велики Радинци	3	1401	\N	804096	194
2101	Гргуревци	3	485	\N	804100	194
2102	Дивош	3	368	\N	804118	194
2103	Засавица И	3	1487	\N	804126	194
2104	Засавица ИИ	3	1487	\N	804134	194
2105	Јарак	3	530	\N	804142	194
2106	Кузмин	3	707	\N	804169	194
2107	Лаћарак	3	708	\N	804177	194
2108	Лежимир	3	737	\N	804185	194
2109	Манђелос	3	794	\N	804193	194
2110	Мартинци	3	801	\N	804207	194
2111	Мачванска Митровица	3	771	\N	804215	194
2112	Ноћај	3	875	\N	804223	194
2113	Равње	3	1094	\N	804231	194
2114	Раденковић	3	\N	\N	804240	194
2115	Салаш Ноћајски	3	1142	\N	804258	194
2116	Сремска Митровица	3	1222	\N	804266	194
2117	Сремска Рача	3	1234	\N	804274	194
2118	Стара Бингула	3	\N	\N	804282	194
2119	Чалма	3	321	\N	804304	194
2120	Шашинци	3	1150	\N	804312	194
2121	Шишатовац	3	\N	\N	804339	194
2122	Шуљан	3	\N	\N	804347	194
2123	Белегиш	3	105	\N	804363	106
2124	Војка	3	1442	\N	804371	106
2125	Голубинци	3	456	\N	804380	106
2126	Крњешевци	3	665	\N	804398	106
2127	Нова Пазова	3	879	\N	804401	106
2128	Нови Бановци	3	881	\N	804410	106
2129	Стара Пазова	3	1245	\N	804428	106
2130	Стари Бановци	3	1248	\N	804436	106
2131	Сурдук	3	1298	\N	804444	106
2132	Адашевци	3	6	\N	804916	155
2133	Батровци	3	90	\N	804924	155
2134	Бачинци	3	29	\N	804932	155
2135	Беркасово	3	229	\N	804959	155
2136	Бикић До	3	235	\N	804967	155
2137	Бингула	3	238	\N	804975	155
2138	Вашица	3	1381	\N	804983	155
2139	Вишњићево	3	1422	\N	804991	155
2140	Гибарац	3	447	\N	805009	155
2141	Ердевик	3	435	\N	805017	155
2142	Илинци	3	508	\N	805025	155
2143	Јамена	3	528	\N	805033	155
2144	Кукујевци	3	688	\N	805041	155
2145	Љуба	3	743	\N	805050	155
2146	Моловин	3	841	\N	805068	155
2147	Моровић	3	842	\N	805076	155
2148	Привина Глава	3	1051	\N	805084	155
2149	Сот	3	1221	\N	805092	155
2150	Шид	3	1173	\N	805106	155
2151	Бадовинци	3	49	\N	705632	177
2152	Баново Поље	3	77	\N	705659	177
2153	Белотић	3	\N	\N	705667	177
2154	Богатић	3	248	\N	705675	177
2155	Глоговац	3	\N	\N	705683	177
2156	Глушци	3	453	\N	705691	177
2157	Дубље	3	415	\N	705705	177
2158	Клење	3	593	\N	705713	177
2159	Метковић	3	\N	\N	705721	177
2160	Очаге	3	\N	\N	705730	177
2161	Салаш Црнобарски	3	\N	\N	705748	177
2162	Совљак	3	\N	\N	705756	177
2163	Узвеће	3	\N	\N	705764	177
2164	Црна Бара	3	339	\N	705772	177
2165	Белотић	3	\N	\N	709778	178
2166	Бељин	3	\N	\N	709786	178
2167	Бобовик	3	\N	\N	709794	178
2168	Владимирци	3	1429	\N	709808	178
2169	Власаница	3	\N	\N	709816	178
2170	Вукошић	3	\N	\N	709824	178
2171	Вучевица	3	\N	\N	709832	178
2172	Дебрц	3	354	\N	709859	178
2173	Драгојевац	3	\N	\N	709867	178
2174	Звезд	3	\N	\N	709875	178
2175	Јазовник	3	\N	\N	709883	178
2176	Јаловик	3	\N	\N	709891	178
2177	Каона	3	\N	\N	709905	178
2178	Козарица	3	\N	\N	709913	178
2179	Крнић	3	\N	\N	709921	178
2180	Крнуле	3	\N	\N	709930	178
2181	Кујавица	3	\N	\N	709948	178
2182	Лојанице	3	\N	\N	709956	178
2183	Матијевац	3	\N	\N	709964	178
2184	Месарци	3	\N	\N	709972	178
2185	Меховине	3	\N	\N	709999	178
2186	Мровска	3	\N	\N	710008	178
2187	Ново Село	3	\N	\N	710016	178
2188	Пејиновић	3	\N	\N	710024	178
2189	Прово	3	1058	\N	710032	178
2190	Риђаке	3	\N	\N	710059	178
2191	Скупљен	3	\N	\N	710067	178
2192	Суво Село	3	\N	\N	710075	178
2193	Трбушац	3	\N	\N	710083	178
2194	Баталаге	3	\N	\N	718394	179
2195	Брдарица	3	\N	\N	718408	179
2196	Бресница	3	\N	\N	718416	179
2197	Галовић	3	\N	\N	718424	179
2198	Голочело	3	\N	\N	718432	179
2199	Градојевић	3	\N	\N	718459	179
2200	Доње Црниљево	3	391	\N	718467	179
2201	Драгиње	3	402	\N	718475	179
2202	Дружетић	3	\N	\N	718483	179
2203	Зукве	3	\N	\N	718491	179
2204	Каменица	3	561	\N	718505	179
2205	Коцељева	3	602	\N	718513	179
2206	Љутице	3	\N	\N	718521	179
2207	Мали Бошњак	3	\N	\N	718530	179
2208	Свилеува	3	1308	\N	718548	179
2209	Суботица	3	\N	\N	718556	179
2210	Ћуковине	3	\N	\N	718564	179
2211	Бањевац	3	\N	\N	720321	89
2212	Бела Црква	3	99	\N	720330	89
2214	Брезовице	3	\N	\N	720356	89
2215	Брштица	3	\N	\N	720364	89
2216	Врбић	3	\N	\N	720372	89
2217	Дворска	3	\N	\N	720399	89
2218	Завлака	3	1488	\N	720402	89
2219	Костајник	3	\N	\N	720429	89
2220	Красава	3	\N	\N	720437	89
2221	Кржава	3	\N	\N	720445	89
2222	Крупањ	3	668	\N	720453	89
2223	Ликодра	3	\N	\N	720461	89
2224	Липеновић	3	\N	\N	720470	89
2225	Мојковић	3	\N	\N	720488	89
2226	Планина	3	\N	\N	720496	89
2227	Равнаја	3	\N	\N	720500	89
2228	Ставе	3	\N	\N	720518	89
2229	Толисавац	3	\N	\N	720526	89
2230	Томањ	3	\N	\N	720534	89
2231	Цветуља	3	\N	\N	720542	89
2232	Церова	3	\N	\N	720569	89
2233	Шљивова	3	\N	\N	720577	89
2234	Бања Ковиљача	3	72	\N	725196	188
2235	Башчелуци	3	\N	\N	725200	188
2236	Брадић	3	\N	\N	725218	188
2237	Брезјак	3	288	\N	725226	188
2238	Брњац	3	\N	\N	725234	188
2239	Велико Село	3	\N	\N	725242	188
2240	Воћњак	3	\N	\N	725269	188
2241	Горња Бадања	3	\N	\N	725277	188
2242	Горња Борина	3	\N	\N	725285	188
2243	Горња Ковиљача	3	\N	\N	725293	188
2244	Горња Сипуља	3	\N	\N	725307	188
2245	Горње Недељице	3	\N	\N	725315	188
2246	Горњи Добрић	3	\N	\N	725323	188
2247	Грнчара	3	\N	\N	725331	188
2248	Доња Бадања	3	\N	\N	725340	188
2249	Доња Сипуља	3	\N	\N	725358	188
2250	Доње Недељице	3	\N	\N	725366	188
2251	Доњи Добрић	3	\N	\N	725374	188
2252	Драгинац	3	401	\N	725382	188
2253	Зајача	3	1483	\N	725404	188
2254	Јадранска Лешница	3	520	\N	725412	188
2255	Јаребице	3	\N	\N	725439	188
2256	Јелав	3	\N	\N	725447	188
2257	Јошева	3	\N	\N	725455	188
2258	Југовићи	3	\N	\N	725463	188
2259	Каменица	3	\N	\N	725471	188
2260	Клупци	3	\N	\N	725480	188
2261	Козјак	3	\N	\N	725498	188
2262	Коренита	3	617	\N	725501	188
2263	Крајишници	3	\N	\N	725510	188
2264	Лешница	3	735	\N	725528	188
2265	Липница	3	\N	\N	725536	188
2266	Липнички Шор	3	1552	\N	725544	188
2267	Лозница	3	753	\N	725552	188
2268	Лозничко Поље	3	\N	\N	725579	188
2269	Милина	3	\N	\N	725587	188
2270	Ново Село	3	\N	\N	725595	188
2271	Пасковац	3	\N	\N	725609	188
2272	Плоча	3	\N	\N	725617	188
2273	Помијача	3	\N	\N	725625	188
2274	Рибарице	3	\N	\N	725633	188
2275	Руњани	3	\N	\N	725641	188
2276	Симино Брдо	3	\N	\N	725650	188
2277	Слатина	3	\N	\N	725668	188
2278	Стража	3	\N	\N	725676	188
2279	Ступница	3	\N	\N	725684	188
2280	Текериш	3	1316	\N	725692	188
2281	Трбосиље	3	\N	\N	725706	188
2282	Трбушница	3	\N	\N	725714	188
2283	Тршић	3	1340	\N	725722	188
2284	Филиповићи	3	\N	\N	725749	188
2285	Цикоте	3	\N	\N	725757	188
2286	Чокешина	3	\N	\N	725765	188
2287	Шурице	3	\N	\N	725773	188
2288	Берловине	3	\N	\N	726478	90
2289	Врхпоље	3	\N	\N	726486	90
2290	Горња Љубовиђа	3	\N	\N	726494	90
2291	Горња Оровица	3	\N	\N	726508	90
2292	Горња Трешњица	3	\N	\N	726516	90
2293	Горње Кошље	3	\N	\N	726524	90
2294	Грачаница	3	\N	\N	726532	90
2295	Грчић	3	\N	\N	726559	90
2296	Доња Љубовиђа	3	\N	\N	726567	90
2297	Доња Оровица	3	385	\N	726575	90
2298	Дрлаче	3	413	\N	726583	90
2299	Дубоко	3	\N	\N	726591	90
2300	Леовић	3	\N	\N	726605	90
2301	Лоњин	3	\N	\N	726613	90
2302	Љубовија	3	746	\N	726621	90
2303	Оровичка Планина	3	\N	\N	726630	90
2304	Поднемић	3	\N	\N	726648	90
2305	Постење	3	\N	\N	726656	90
2306	Рујевац	3	\N	\N	726664	90
2307	Савковић	3	\N	\N	726672	90
2308	Селенац	3	\N	\N	726699	90
2309	Соколац	3	\N	\N	726702	90
2310	Торник	3	\N	\N	726729	90
2311	Узовница	3	1368	\N	726737	90
2312	Цапарић	3	\N	\N	726745	90
2313	Црнча	3	\N	\N	726753	90
2314	Читлук	3	\N	\N	726761	90
2315	Амајић	3	\N	\N	726923	91
2316	Брасина	3	\N	\N	726931	91
2317	Будишић	3	\N	\N	726940	91
2318	Велика Река	3	1394	\N	726958	91
2319	Вољевци	3	\N	\N	726966	91
2320	Доња Борина	3	380	\N	726974	91
2321	Доња Трешњица	3	\N	\N	726982	91
2322	Мали Зворник	3	790	\N	727008	91
2323	Радаљ	3	1065	\N	727016	91
2324	Сакар	3	\N	\N	727024	91
2325	Цулине	3	\N	\N	727032	91
2326	Читлук	3	\N	\N	727059	91
2327	Бела Река	3	\N	\N	746070	198
2328	Богосавац	3	\N	\N	746088	198
2329	Бојић	3	\N	\N	746096	198
2330	Букор	3	\N	\N	746100	198
2331	Варна	3	1379	\N	746118	198
2332	Волујац	3	\N	\N	746126	198
2333	Горња Врањска	3	\N	\N	746134	198
2334	Грушић	3	\N	\N	746142	198
2335	Двориште	3	\N	\N	746169	198
2336	Десић	3	\N	\N	746177	198
2337	Добрић	3	373	\N	746185	198
2338	Дреновац	3	412	\N	746193	198
2339	Дуваниште	3	\N	\N	746207	198
2340	Жабар	3	\N	\N	746215	198
2341	Заблаће	3	\N	\N	746223	198
2342	Змињак	3	1503	\N	746231	198
2343	Јевремовац	3	\N	\N	746240	198
2344	Јеленча	3	\N	\N	746258	198
2345	Корман	3	\N	\N	746266	198
2346	Криваја	3	\N	\N	746274	198
2347	Липолист	3	741	\N	746282	198
2348	Мајур	3	778	\N	746304	198
2349	Мала Врањска	3	\N	\N	746312	198
2350	Маови	3	\N	\N	746339	198
2351	Мачвански Причиновић	3	772	\N	746347	198
2352	Метлић	3	816	\N	746355	198
2353	Милошевац	3	\N	\N	746363	198
2354	Миокус	3	\N	\N	746371	198
2355	Мишар	3	\N	\N	746380	198
2356	Мрђеновац	3	\N	\N	746398	198
2357	Накучани	3	\N	\N	746401	198
2358	Орашац	3	\N	\N	746410	198
2359	Орид	3	943	\N	746428	198
2360	Петковица	3	\N	\N	746436	198
2361	Петловача	3	989	\N	746444	198
2362	Поцерски Метковић	3	\N	\N	746452	198
2363	Поцерски Причиновић	3	\N	\N	746479	198
2364	Предворица	3	\N	\N	746487	198
2365	Прњавор	3	1052	\N	746495	198
2366	Радовашница	3	\N	\N	746509	198
2367	Рибари	3	1107	\N	746517	198
2368	Румска	3	\N	\N	746525	198
2369	Синошевић	3	\N	\N	746533	198
2370	Скрађани	3	\N	\N	746541	198
2371	Слатина	3	\N	\N	746550	198
2372	Слепчевић	3	\N	\N	746568	198
2373	Табановић	3	\N	\N	746576	198
2374	Церовац	3	328	\N	746584	198
2375	Цуљковић	3	\N	\N	746592	198
2376	Шабац	3	1135	\N	746606	198
2377	Шеварице	3	\N	\N	746614	198
2378	Штитар	3	1259	\N	746622	198
2379	Бабина Лука	3	\N	\N	708267	180
2380	Балиновић	3	\N	\N	708275	180
2381	Бачевци	3	\N	\N	708283	180
2382	Белић	3	\N	\N	708291	180
2383	Белошевац	3	\N	\N	708305	180
2384	Беомужевић	3	\N	\N	708313	180
2385	Близоње	3	\N	\N	708321	180
2386	Бобова	3	\N	\N	708330	180
2387	Богатић	3	\N	\N	708348	180
2388	Бранговић	3	\N	\N	708356	180
2389	Бранковина	3	277	\N	708364	180
2390	Брезовице	3	\N	\N	708372	180
2391	Бујачић	3	\N	\N	708399	180
2392	Ваљево	3	1371	\N	708402	180
2393	Веселиновац	3	\N	\N	708429	180
2394	Влашчић	3	\N	\N	708437	180
2395	Врагочаница	3	\N	\N	708445	180
2396	Вујиновача	3	\N	\N	708453	180
2397	Гола Глава	3	\N	\N	708461	180
2398	Горић	3	\N	\N	708470	180
2399	Горња Буковица	3	\N	\N	708488	180
2400	Горња Грабовица	3	\N	\N	708496	180
2401	Горње Лесковице	3	\N	\N	708500	180
2402	Дегурић	3	\N	\N	708518	180
2403	Дивци	3	365	\N	708526	180
2404	Дивчибаре	3	366	\N	708534	180
2405	Доња Буковица	3	\N	\N	708542	180
2406	Доње Лесковице	3	\N	\N	708569	180
2407	Драчић	3	400	\N	708577	180
2408	Дупљај	3	\N	\N	708585	180
2409	Жабари	3	\N	\N	708593	180
2410	Забрдица	3	\N	\N	708607	180
2411	Зарубе	3	\N	\N	708615	180
2412	Златарић	3	\N	\N	708623	180
2413	Јазовик	3	\N	\N	708631	180
2414	Јасеница	3	\N	\N	708640	180
2415	Јовања	3	\N	\N	708658	180
2416	Јошева	3	\N	\N	708666	180
2417	Каменица	3	562	\N	708674	180
2418	Кланица	3	\N	\N	708682	180
2419	Клинци	3	\N	\N	708704	180
2420	Ковачице	3	\N	\N	708712	180
2421	Козличић	3	\N	\N	708739	180
2422	Котешица	3	\N	\N	708747	180
2423	Кунице	3	\N	\N	708755	180
2424	Лелић	3	724	\N	708763	180
2425	Лозница	3	\N	\N	708771	180
2426	Лукавац	3	\N	\N	708780	180
2427	Мајиновић	3	\N	\N	708798	180
2428	Мијачи	3	\N	\N	708801	180
2429	Миличиница	3	\N	\N	708810	180
2430	Мрчић	3	\N	\N	708828	180
2431	Оглађеновац	3	\N	\N	708836	180
2432	Осладић	3	\N	\N	708844	180
2433	Пакље	3	\N	\N	708852	180
2434	Пауне	3	\N	\N	708879	180
2435	Петница	3	\N	\N	708887	180
2436	Попучке	3	1021	\N	708895	180
2437	Пријездић	3	\N	\N	708909	180
2438	Причевић	3	1045	\N	708917	180
2439	Рабас	3	\N	\N	708925	180
2440	Равње	3	\N	\N	708933	180
2441	Рађево Село	3	\N	\N	708941	180
2442	Ребељ	3	\N	\N	708950	180
2443	Ровни	3	\N	\N	708968	180
2444	Сандаљ	3	\N	\N	708976	180
2445	Седлари	3	\N	\N	708984	180
2446	Ситарице	3	\N	\N	708992	180
2447	Совач	3	\N	\N	709000	180
2448	Станина Река	3	\N	\N	709018	180
2449	Стапар	3	\N	\N	709026	180
2450	Стрмна Гора	3	\N	\N	709034	180
2451	Стубо	3	\N	\N	709042	180
2452	Суводање	3	\N	\N	709069	180
2453	Сушица	3	\N	\N	709077	180
2454	Таор	3	\N	\N	709085	180
2455	Тубравић	3	\N	\N	709093	180
2456	Тупанци	3	\N	\N	709107	180
2457	Бајевац	3	\N	\N	722979	1
2458	Боговађа	3	250	\N	723096	1
2459	Врачевић	3	\N	\N	722987	1
2460	Доњи Лајковац	3	\N	\N	722995	1
2461	Јабучје	3	516	\N	723002	1
2462	Лајковац (варош)	3	711	\N	723029	1
2463	Лајковац (село)	3	\N	\N	723037	1
2464	Мали Борак	3	\N	\N	723045	1
2465	Маркова Црква	3	\N	\N	723053	1
2466	Непричава	3	\N	\N	723061	1
2467	Пепељевац	3	\N	\N	723070	1
2468	Придворица	3	\N	\N	723088	1
2469	Ратковац	3	\N	\N	723100	1
2470	Рубрибреза	3	\N	\N	723118	1
2471	Скобаљ	3	\N	\N	723126	1
2472	Словац	3	1199	\N	723134	1
2473	Степање	3	\N	\N	723142	1
2474	Стрмово	3	\N	\N	723169	1
2475	Ћелије	3	\N	\N	723177	1
2476	Ба	3	\N	\N	726176	2
2477	Бабајић	3	\N	\N	726184	2
2478	Белановица	3	103	\N	726192	2
2479	Бошњановић	3	\N	\N	726206	2
2480	Бранчић	3	\N	\N	726214	2
2481	Велишевац	3	\N	\N	726222	2
2482	Гукош	3	\N	\N	726249	2
2483	Дићи	3	\N	\N	726257	2
2484	Доњи Бањани	3	\N	\N	726265	2
2485	Живковци	3	\N	\N	726273	2
2486	Ивановци	3	\N	\N	726281	2
2487	Јајчић	3	\N	\N	726290	2
2488	Кадина Лука	3	\N	\N	726303	2
2489	Калањевци	3	\N	\N	726311	2
2490	Козељ	3	\N	\N	726320	2
2491	Лалинци	3	\N	\N	726338	2
2492	Латковић	3	\N	\N	726346	2
2493	Липље	3	\N	\N	726354	2
2494	Љиг	3	742	\N	726362	2
2495	Милавац	3	\N	\N	726389	2
2496	Моравци	3	\N	\N	726397	2
2497	Палежница	3	\N	\N	726419	2
2498	Пољанице	3	\N	\N	726427	2
2499	Славковица	3	1196	\N	726435	2
2500	Цветановац	3	\N	\N	726443	2
2501	Штавица	3	\N	\N	726451	2
2502	Шутци	3	\N	\N	726460	2
2503	Берковац	3	\N	\N	728071	3
2504	Брежђе	3	287	\N	728080	3
2505	Буковац	3	\N	\N	728098	3
2506	Велика Маришта	3	\N	\N	728101	3
2507	Вировац	3	\N	\N	728110	3
2508	Вртиглав	3	\N	\N	728128	3
2509	Голубац	3	\N	\N	728136	3
2510	Горњи Лајковац	3	\N	\N	728144	3
2511	Горњи Мушић	3	\N	\N	728152	3
2512	Гуњица	3	\N	\N	728179	3
2513	Доњи Мушић	3	\N	\N	728187	3
2514	Дучић	3	\N	\N	728195	3
2515	Ђурђевац	3	\N	\N	728209	3
2516	Клашнић	3	\N	\N	728217	3
2517	Кључ	3	\N	\N	728225	3
2518	Команице	3	\N	\N	728233	3
2519	Крчмар	3	\N	\N	728241	3
2520	Маљевић	3	\N	\N	728250	3
2521	Мионица (варошица)	3	829	\N	728268	3
2522	Мионица (село)	3	\N	\N	728276	3
2523	Мратишић	3	\N	\N	728284	3
2524	Наномир	3	\N	\N	728292	3
2525	Осеченица	3	\N	\N	728306	3
2526	Паштрић	3	\N	\N	728314	3
2527	Планиница	3	\N	\N	728322	3
2528	Попадић	3	\N	\N	728349	3
2529	Радобић	3	\N	\N	728357	3
2530	Рајковић	3	1073	\N	728365	3
2531	Ракари	3	\N	\N	728373	3
2532	Робаје	3	\N	\N	728381	3
2533	Санковић	3	\N	\N	728390	3
2534	Струганик	3	\N	\N	728403	3
2535	Табановић	3	\N	\N	728411	3
2536	Тодорин До	3	\N	\N	728420	3
2537	Толић	3	\N	\N	728438	3
2538	Шушеока	3	\N	\N	728446	3
2539	Бастав	3	\N	\N	731072	4
2540	Белотић	3	\N	\N	731099	4
2541	Братачић	3	\N	\N	731102	4
2542	Горње Црниљево	3	\N	\N	731129	4
2543	Гуњаци	3	\N	\N	731137	4
2544	Драгијевица	3	\N	\N	731145	4
2545	Драгодол	3	\N	\N	731153	4
2546	Комирић	3	607	\N	731161	4
2547	Коњиц	3	\N	\N	731170	4
2548	Коњуша	3	\N	\N	731188	4
2549	Лопатањ	3	\N	\N	731196	4
2550	Осечина (варошица)	3	947	\N	731200	4
2551	Осечина (село)	3	\N	\N	731218	4
2552	Остружањ	3	\N	\N	731226	4
2553	Пецка	3	984	\N	731234	4
2554	Плужац	3	\N	\N	731242	4
2555	Сирдија	3	\N	\N	731269	4
2556	Скадар	3	\N	\N	731277	4
2557	Туђин	3	\N	\N	731285	4
2558	Царина	3	\N	\N	731293	4
2559	Бањани	3	73	\N	744506	5
2560	Богдановица	3	\N	\N	744514	5
2561	Бргуле	3	292	\N	744522	5
2562	Брезовица	3	\N	\N	744549	5
2563	Врело	3	\N	\N	744557	5
2564	Врховине	3	\N	\N	744565	5
2565	Вукона	3	\N	\N	744573	5
2566	Гвозденовић	3	\N	\N	744581	5
2567	Гуњевац	3	\N	\N	744590	5
2568	Докмир	3	\N	\N	744603	5
2569	Звиздар	3	\N	\N	744611	5
2570	Јошева	3	\N	\N	744620	5
2571	Каленић	3	\N	\N	744638	5
2572	Калиновац	3	\N	\N	744646	5
2573	Кожуар	3	\N	\N	744654	5
2574	Кршна Глава	3	\N	\N	744662	5
2575	Лисо Поље	3	\N	\N	744689	5
2576	Лончаник	3	\N	\N	744697	5
2577	Милорци	3	\N	\N	744719	5
2578	Мургаш	3	\N	\N	744727	5
2579	Новаци	3	\N	\N	744735	5
2580	Паљуви	3	\N	\N	744743	5
2581	Памбуковица	3	963	\N	744751	5
2582	Радљево	3	1069	\N	744760	5
2583	Радуша	3	\N	\N	744778	5
2584	Руклада	3	\N	\N	744786	5
2585	Слатина	3	\N	\N	744794	5
2586	Совљак	3	\N	\N	744808	5
2587	Стубленица	3	\N	\N	744816	5
2588	Таково	3	\N	\N	744824	5
2589	Тврдојевац	3	\N	\N	744832	5
2590	Трлић	3	\N	\N	744859	5
2591	Трњаци	3	\N	\N	744867	5
2592	Тулари	3	\N	\N	744875	5
2593	Уб	3	1350	\N	744883	5
2594	Црвена Јабука	3	\N	\N	744891	5
2595	Чучуге	3	\N	\N	744905	5
2596	Шарбане	3	\N	\N	744913	5
2597	Велика Плана	3	1393	\N	709344	148
2598	Велико Орашје	3	1410	\N	709352	148
2599	Доња Ливадица	3	\N	\N	709379	148
2600	Крњево	3	666	\N	709387	148
2601	Купусина	3	\N	\N	709395	148
2602	Лозовик	3	755	\N	709409	148
2603	Марковац	3	798	\N	709417	148
2604	Милошевац	3	825	\N	709425	148
2605	Ново Село	3	\N	\N	709433	148
2606	Радовање	3	\N	\N	709441	148
2607	Ракинац	3	\N	\N	709450	148
2608	Старо Село	3	1252	\N	709468	148
2609	Трновче	3	\N	\N	709476	148
2610	Бадљевица	3	\N	\N	740268	192
2611	Биновац	3	\N	\N	740276	192
2612	Водањ	3	1440	\N	740284	192
2613	Враново	3	1455	\N	740292	192
2614	Врбовац	3	\N	\N	740306	192
2615	Вучак	3	\N	\N	740314	192
2616	Добри До	3	\N	\N	740322	192
2617	Друговац	3	414	\N	740349	192
2618	Колари	3	605	\N	740357	192
2619	Кулич	3	\N	\N	746746	192
2620	Ландол	3	\N	\N	740365	192
2621	Липе	3	740	\N	740373	192
2622	Лугавчина	3	759	\N	740381	192
2623	Луњевац	3	\N	\N	740390	192
2624	Мала Крсна	3	780	\N	740403	192
2625	Мало Орашје	3	\N	\N	740411	192
2626	Михајловац	3	818	\N	740420	192
2627	Осипаоница	3	948	\N	740438	192
2628	Петријево	3	\N	\N	740446	192
2629	Радинац	3	1068	\N	740454	192
2630	Раља	3	\N	\N	740462	192
2631	Сараорци	3	1146	\N	740489	192
2632	Сеоне	3	\N	\N	740497	192
2633	Скобаљ	3	1190	\N	740519	192
2634	Смедерево	3	1202	\N	740527	192
2635	Суводол	3	\N	\N	740535	192
2636	Удовице	3	\N	\N	740543	192
2637	Шалинац	3	\N	\N	740551	192
2638	Азања	3	23	\N	740560	149
2639	Баничина	3	\N	\N	740578	149
2640	Бачинац	3	\N	\N	740586	149
2641	Башин	3	\N	\N	740594	149
2642	Влашки До	3	\N	\N	740608	149
2643	Водице	3	\N	\N	740616	149
2644	Глибовац	3	\N	\N	740624	149
2645	Голобок	3	454	\N	740632	149
2646	Грчац	3	\N	\N	740659	149
2647	Кусадак	3	702	\N	740667	149
2648	Мала Плана	3	\N	\N	740675	149
2649	Мраморац	3	\N	\N	740683	149
2650	Придворице	3	\N	\N	740691	149
2651	Ратари	3	1087	\N	740705	149
2652	Селевац	3	1161	\N	740713	149
2653	Смедеревска Паланка	3	1200	\N	740721	149
2654	Стојачак	3	\N	\N	740730	149
2655	Церовац	3	\N	\N	740748	149
2656	Бискупље	3	\N	\N	709484	135
2657	Велико Градиште	3	1406	\N	709492	135
2658	Гарево	3	\N	\N	709506	135
2659	Десине	3	\N	\N	709514	135
2660	Дољашница	3	\N	\N	709522	135
2661	Ђураково	3	\N	\N	709549	135
2662	Затоње	3	\N	\N	709557	135
2663	Камијево	3	\N	\N	709565	135
2664	Кисиљево	3	\N	\N	709573	135
2665	Кумане	3	\N	\N	709581	135
2666	Курјаче	3	\N	\N	709590	135
2667	Кусиће	3	\N	\N	709603	135
2668	Љубиње	3	\N	\N	709611	135
2669	Мајиловац	3	776	\N	709620	135
2670	Макце	3	\N	\N	709638	135
2671	Острово	3	\N	\N	709646	135
2672	Печаница	3	\N	\N	709654	135
2673	Пожежено	3	\N	\N	709662	135
2674	Поповац	3	\N	\N	709689	135
2675	Рам	3	\N	\N	709697	135
2676	Сираково	3	\N	\N	709719	135
2677	Средњево	3	1228	\N	709727	135
2678	Тополовник	3	1327	\N	709735	135
2679	Триброде	3	\N	\N	709743	135
2680	Царевац	3	\N	\N	709751	135
2681	Чешљева Бара	3	\N	\N	709760	135
2682	Барич	3	\N	\N	712868	136
2683	Бикиње	3	\N	\N	712876	136
2684	Браничево	3	276	\N	712884	136
2685	Брњица	3	\N	\N	712892	136
2686	Винци	3	\N	\N	712906	136
2687	Војилово	3	\N	\N	712914	136
2688	Голубац	3	455	\N	712922	136
2689	Двориште	3	\N	\N	712949	136
2690	Добра	3	371	\N	712957	136
2691	Доња Крушевица	3	\N	\N	712965	136
2692	Душманић	3	\N	\N	712973	136
2693	Житковица	3	\N	\N	712981	136
2694	Клење	3	592	\N	712990	136
2695	Кривача	3	\N	\N	713007	136
2696	Кудреш	3	\N	\N	713015	136
2697	Малешево	3	\N	\N	713023	136
2698	Миљевић	3	\N	\N	713031	136
2699	Мрчковац	3	\N	\N	713040	136
2700	Поникве	3	\N	\N	713058	136
2701	Радошевац	3	\N	\N	713066	136
2702	Сладинац	3	\N	\N	713074	136
2703	Снеготин	3	\N	\N	713082	136
2704	Усије	3	\N	\N	713104	136
2705	Шувајић	3	\N	\N	713112	136
2706	Александровац	3	12	\N	714828	137
2707	Брзоходе	3	\N	\N	714836	137
2708	Витежево	3	\N	\N	714844	137
2709	Влашки До	3	\N	\N	714852	137
2710	Жабари	3	1477	\N	714879	137
2711	Кочетин	3	\N	\N	714887	137
2712	Миријево	3	\N	\N	714895	137
2713	Ореовица	3	\N	\N	714909	137
2714	Полатна	3	\N	\N	714917	137
2715	Породин	3	1022	\N	714925	137
2716	Свињарево	3	\N	\N	714933	137
2717	Сибница	3	\N	\N	714941	137
2718	Симићево	3	\N	\N	714950	137
2719	Тићевац	3	\N	\N	714968	137
2720	Четереже	3	\N	\N	714976	137
2721	Близнак	3	\N	\N	714984	138
2722	Брезница	3	\N	\N	714992	138
2723	Вуковац	3	1475	\N	715000	138
2724	Жагубица	3	1482	\N	715018	138
2725	Изварица	3	\N	\N	715026	138
2726	Јошаница	3	\N	\N	715034	138
2727	Крепољин	3	659	\N	715042	138
2728	Крупаја	3	\N	\N	715069	138
2729	Лазница	3	720	\N	715077	138
2730	Липе	3	\N	\N	715085	138
2731	Медвеђица	3	\N	\N	715093	138
2732	Милановац	3	\N	\N	715107	138
2733	Милатовац	3	\N	\N	715115	138
2734	Осаница	3	946	\N	715123	138
2735	Рибаре	3	\N	\N	715131	138
2736	Селиште	3	\N	\N	715140	138
2737	Сиге	3	\N	\N	715158	138
2738	Суви До	3	\N	\N	715166	138
2739	Благојев Камен	3	\N	\N	722685	139
2740	Бродица	3	\N	\N	722693	139
2741	Буковска	3	\N	\N	722707	139
2742	Велика Бресница	3	\N	\N	722715	139
2743	Волуја	3	1446	\N	722723	139
2744	Вуковић	3	\N	\N	722731	139
2745	Дубока	3	416	\N	722740	139
2746	Зеленик	3	\N	\N	722758	139
2747	Каона	3	\N	\N	722766	139
2748	Кучајна	3	\N	\N	722774	139
2749	Кучево	3	684	\N	722782	139
2750	Љешница	3	\N	\N	722804	139
2751	Мала Бресница	3	\N	\N	722812	139
2752	Мишљеновац	3	\N	\N	722839	139
2753	Мустапић	3	\N	\N	722847	139
2754	Нересница	3	854	\N	722855	139
2755	Раброво	3	1062	\N	722863	139
2756	Равниште	3	\N	\N	722871	139
2757	Раденка	3	\N	\N	722880	139
2758	Ракова Бара	3	\N	\N	722898	139
2759	Сена	3	\N	\N	722901	139
2760	Српце	3	\N	\N	722910	139
2761	Турија	3	1348	\N	722928	139
2762	Церемошња	3	\N	\N	722936	139
2763	Церовица	3	\N	\N	722944	139
2764	Шевица	3	\N	\N	722952	139
2765	Аљудово	3	\N	\N	727067	134
2766	Батуша	3	\N	\N	727075	134
2767	Божевац	3	272	\N	727083	134
2768	Велико Село	3	\N	\N	727091	134
2769	Велико Црниће	3	\N	\N	727105	134
2770	Врбница	3	\N	\N	727113	134
2771	Забрега	3	\N	\N	727121	134
2772	Калиште	3	\N	\N	727130	134
2773	Кобиље	3	\N	\N	727148	134
2774	Крављи До	3	\N	\N	727156	134
2775	Кула	3	\N	\N	727164	134
2776	Мало Градиште	3	\N	\N	727172	134
2777	Мало Црниће	3	791	\N	727199	134
2778	Салаковац	3	\N	\N	727202	134
2779	Смољинац	3	1208	\N	727229	134
2780	Топоница	3	\N	\N	727237	134
2781	Црљенац	3	\N	\N	727245	134
2782	Шапине	3	\N	\N	727253	134
2783	Шљивовац	3	\N	\N	727261	134
2784	Бистрица	3	\N	\N	731684	140
2785	Бошњак	3	\N	\N	731692	140
2786	Буровац	3	313	\N	731706	140
2787	Бусур	3	\N	\N	731714	140
2788	Везичево	3	\N	\N	731722	140
2789	Велики Поповац	3	\N	\N	731749	140
2790	Велико Лаоле	3	1409	\N	731757	140
2791	Витовница	3	\N	\N	731765	140
2792	Вошановац	3	\N	\N	731773	140
2793	Добрње	3	\N	\N	731781	140
2794	Дубочка	3	\N	\N	731790	140
2795	Ждрело	3	\N	\N	731803	140
2796	Забрђе	3	\N	\N	731811	140
2797	Каменово	3	\N	\N	731820	140
2798	Кладурово	3	\N	\N	731838	140
2799	Кнежица	3	\N	\N	731846	140
2800	Крвије	3	\N	\N	731854	140
2801	Лесковац	3	\N	\N	731862	140
2802	Лопушник	3	\N	\N	731889	140
2803	Мало Лаоле	3	\N	\N	731897	140
2804	Манастирица	3	\N	\N	731919	140
2805	Мелница	3	\N	\N	731927	140
2806	Орешковица	3	942	\N	731935	140
2807	Орљево	3	\N	\N	731943	140
2808	Панково	3	\N	\N	731951	140
2809	Петровац на Млави	3	990	\N	731960	140
2810	Рановац	3	1079	\N	731978	140
2811	Рашанац	3	1080	\N	731986	140
2812	Стамница	3	\N	\N	731994	140
2813	Старчево	3	\N	\N	732001	140
2814	Табановац	3	\N	\N	732010	140
2815	Трновче	3	\N	\N	732028	140
2816	Ћовдин	3	\N	\N	732036	140
2817	Шетоње	3	1167	\N	732044	140
2818	Баре	3	83	\N	732869	191
2819	Батовац	3	\N	\N	732877	191
2820	Берање	3	\N	\N	732885	191
2821	Брадарац	3	275	\N	732893	191
2822	Братинац	3	\N	\N	732907	191
2823	Брежане	3	\N	\N	732915	191
2824	Бубушинац	3	\N	\N	732923	191
2825	Драговац	3	\N	\N	732931	191
2826	Дрмно	3	\N	\N	732940	191
2827	Дубравица	3	419	\N	732958	191
2828	Живица	3	\N	\N	732966	191
2829	Касидол	3	\N	\N	732974	191
2830	Кленовник	3	\N	\N	732982	191
2831	Кличевац	3	594	\N	733008	191
2832	Костолац	3	626	\N	733016	191
2833	Лучица	3	\N	\N	733024	191
2834	Маљуревац	3	\N	\N	733032	191
2835	Набрђе	3	\N	\N	733059	191
2836	Острово	3	\N	\N	733067	191
2837	Петка	3	\N	\N	733075	191
2838	Пожаревац	3	1024	\N	733083	191
2839	Пољана	3	1017	\N	733091	191
2840	Пругово	3	\N	\N	733105	191
2841	Речица	3	\N	\N	733113	191
2842	Село Костолац	3	\N	\N	733121	191
2843	Трњане	3	\N	\N	733130	191
2844	Ћириковац	3	\N	\N	733148	191
2845	Аранђеловац	3	20	\N	701408	72
2846	Бања	3	70	\N	701416	72
2847	Босута	3	\N	\N	701424	72
2848	Брезовац	3	\N	\N	701432	72
2849	Буковик	3	308	\N	701459	72
2850	Венчане	3	1413	\N	701467	72
2851	Врбица	3	\N	\N	701475	72
2852	Вукосавци	3	\N	\N	701483	72
2853	Гараши	3	\N	\N	701491	72
2854	Горња Трешњевица	3	\N	\N	701505	72
2855	Јеловик	3	543	\N	701513	72
2856	Копљаре	3	\N	\N	701521	72
2857	Мисача	3	\N	\N	701530	72
2858	Орашац	3	940	\N	701548	72
2859	Даросава	3	351	\N	701556	72
2860	Прогореоци	3	\N	\N	701564	72
2861	Раниловић	3	1077	\N	701572	72
2862	Стојник	3	1261	\N	701599	72
2863	Тулеж	3	\N	\N	701602	72
2864	Бадњевац	3	48	\N	702838	73
2865	Баточина	3	89	\N	702846	73
2866	Брзан	3	299	\N	702854	73
2867	Градац	3	\N	\N	702862	73
2868	Доброводица	3	\N	\N	702889	73
2869	Жировница	3	1492	\N	702897	73
2870	Кијево	3	\N	\N	702919	73
2871	Милатовац	3	1553	\N	702943	73
2872	Никшић	3	\N	\N	702951	73
2873	Прњавор	3	\N	\N	702960	73
2874	Црни Као	3	\N	\N	702978	73
2875	Бајчетина	3	\N	\N	716758	75
2876	Балосаве	3	\N	\N	716766	75
2877	Баре	3	81	\N	716774	75
2878	Бечевица	3	\N	\N	716782	75
2879	Борач	3	\N	\N	716804	75
2880	Брестовац	3	\N	\N	716812	75
2881	Брњица	3	\N	\N	716839	75
2882	Бумбарево Брдо	3	\N	\N	716847	75
2883	Врбета	3	\N	\N	716855	75
2884	Вучковица	3	\N	\N	716863	75
2885	Грабовац	3	\N	\N	716871	75
2886	Гривац	3	\N	\N	716880	75
2887	Гружа	3	490	\N	716898	75
2888	Губеревац	3	491	\N	716901	75
2889	Гунцати	3	\N	\N	716910	75
2890	Драгушица	3	\N	\N	716928	75
2891	Дубрава	3	\N	\N	716936	75
2892	Жуње	3	\N	\N	716944	75
2893	Забојница	3	1479	\N	716952	75
2894	Кикојевац	3	\N	\N	716979	75
2895	Кнежевац	3	\N	\N	716987	75
2896	Кнић	3	598	\N	716995	75
2897	Коњуша	3	\N	\N	717002	75
2898	Кусовац	3	\N	\N	717029	75
2899	Лесковац	3	\N	\N	717037	75
2900	Липница	3	\N	\N	717045	75
2901	Љубић	3	\N	\N	717053	75
2902	Љуљаци	3	\N	\N	717061	75
2903	Опланић	3	\N	\N	717070	75
2904	Пајсијевић	3	\N	\N	717088	75
2905	Претоке	3	\N	\N	717096	75
2906	Радмиловић	3	\N	\N	717100	75
2907	Рашковић	3	\N	\N	717118	75
2908	Суморовац	3	\N	\N	717126	75
2909	Топоница	3	1328	\N	717134	75
2910	Честин	3	\N	\N	717142	75
2911	Аџине Ливаде	3	\N	\N	718572	70
2912	Баљковац	3	\N	\N	718599	70
2913	Ботуње	3	\N	\N	718629	70
2914	Букоровац	3	309	\N	718637	70
2915	Велика Сугубина	3	\N	\N	718645	70
2916	Велике Пчелице	3	\N	\N	718653	70
2917	Велики Шењ	3	\N	\N	718661	70
2918	Вињиште	3	\N	\N	718670	70
2919	Влакча	3	\N	\N	718688	70
2920	Голочело	3	\N	\N	718696	70
2921	Горња Сабанта	3	462	\N	718700	70
2922	Горње Грбице	3	\N	\N	718718	70
2923	Горње Јарушице	3	\N	\N	718726	70
2924	Горње Комарице	3	\N	\N	718734	70
2925	Грошница	3	489	\N	718742	70
2926	Десимировац	3	359	\N	718769	70
2927	Дивостин	3	369	\N	718777	70
2928	Добрача	3	\N	\N	718785	70
2929	Доња Сабанта	3	\N	\N	718793	70
2930	Доње Грбице	3	\N	\N	718807	70
2931	Доње Комарице	3	\N	\N	718815	70
2932	Драгобраћа	3	404	\N	718823	70
2933	Драча	3	\N	\N	718831	70
2934	Дреновац	3	\N	\N	718840	70
2935	Дулене	3	\N	\N	718858	70
2936	Ђурисело	3	\N	\N	718866	70
2937	Ердеч	3	434	\N	718874	70
2938	Јабучје	3	\N	\N	718912	70
2939	Јовановац	3	\N	\N	718939	70
2940	Каменица	3	\N	\N	718947	70
2941	Корман	3	618	\N	718963	70
2942	Котража	3	\N	\N	718971	70
2943	Крагујевац	3	633	\N	718980	70
2944	Кутлово	3	\N	\N	718998	70
2945	Лужнице	3	768	\N	719005	70
2946	Љубичевац	3	\N	\N	719013	70
2947	Мала Врбица	3	\N	\N	719021	70
2948	Мали Шењ	3	\N	\N	719048	70
2949	Маршић	3	800	\N	719056	70
2950	Маслошево	3	\N	\N	719064	70
2951	Миронић	3	\N	\N	719072	70
2952	Нови Милановац	3	\N	\N	719099	70
2953	Опорница	3	\N	\N	719102	70
2954	Пајазитово	3	\N	\N	719129	70
2955	Поскурице	3	\N	\N	719145	70
2956	Прекопеча	3	\N	\N	719153	70
2957	Рамаћа	3	\N	\N	719161	70
2958	Ресник	3	1103	\N	719170	70
2959	Рогојевац	3	\N	\N	719188	70
2960	Страгари	3	1263	\N	719200	70
2961	Трешњевак	3	\N	\N	719226	70
2962	Трмбас	3	\N	\N	719234	70
2963	Угљаревац	3	\N	\N	719242	70
2964	Цветојевац	3	\N	\N	719269	70
2965	Церовац	3	\N	\N	719277	70
2966	Чумић	3	346	\N	719285	70
2967	Шљивовац	3	1554	\N	719293	70
2968	Лапово (варошица)	3	715	\N	702927	74
2969	Лапово (село)	3	\N	\N	702935	74
2970	Адровац	3	\N	\N	736678	76
2971	Борци	3	\N	\N	736686	76
2972	Бошњане	3	\N	\N	736694	76
2973	Велико Крчмаре	3	\N	\N	736708	76
2974	Вишевац	3	\N	\N	736716	76
2975	Војиновац	3	\N	\N	736724	76
2976	Вучић	3	\N	\N	736732	76
2977	Доња Рача	3	\N	\N	736759	76
2978	Доње Јарушице	3	\N	\N	736767	76
2979	Ђурђево	3	425	\N	736775	76
2980	Мало Крчмаре	3	792	\N	736783	76
2981	Мали Мирашевац	3	\N	\N	746819	76
2982	Мирашевац	3	\N	\N	736791	76
2983	Поповић	3	\N	\N	736805	76
2984	Рача	3	1064	\N	736813	76
2985	Сараново	3	\N	\N	736821	76
2986	Сепци	3	\N	\N	736830	76
2987	Сипић	3	1183	\N	736848	76
2988	Трска	3	\N	\N	736856	76
2989	Белосавци	3	111	\N	741914	77
2990	Блазнава	3	\N	\N	741922	77
2991	Божурња	3	\N	\N	741949	77
2992	Винча	3	\N	\N	741957	77
2993	Војковци	3	\N	\N	741965	77
2994	Горња Трнава	3	1555	\N	741973	77
2995	Горња Шаторња	3	\N	\N	741981	77
2996	Горович	3	\N	\N	741990	77
2997	Гуришевци	3	\N	\N	742007	77
2998	Доња Трешњевица	3	\N	\N	742015	77
2999	Доња Трнава	3	\N	\N	742023	77
3000	Доња Шаторња	3	387	\N	742031	77
3001	Жабаре	3	\N	\N	742040	77
3002	Загорица	3	\N	\N	742058	77
3003	Јарменовци	3	532	\N	742066	77
3004	Јеленац	3	\N	\N	742074	77
3005	Јунковац	3	\N	\N	742082	77
3006	Клока	3	\N	\N	742104	77
3007	Крћевац	3	\N	\N	742112	77
3008	Липовац	3	\N	\N	742139	77
3009	Манојловци	3	\N	\N	742147	77
3010	Маскар	3	\N	\N	742155	77
3011	Наталинци	3	852	\N	742163	77
3012	Овсиште	3	\N	\N	742171	77
3013	Павловац	3	\N	\N	742180	77
3014	Пласковац	3	\N	\N	742198	77
3015	Рајковац	3	\N	\N	742201	77
3016	Светлић	3	\N	\N	742210	77
3017	Топола (варошица)	3	1326	\N	742228	77
3018	Топола (село)	3	\N	\N	742236	77
3019	Шуме	3	\N	\N	742244	77
3020	Балајнац	3	\N	\N	713813	34
3021	Баре	3	\N	\N	713821	34
3022	Бељајка	3	\N	\N	713830	34
3023	Богава	3	\N	\N	713848	34
3024	Брестово	3	\N	\N	713856	34
3025	Буковац	3	\N	\N	713864	34
3026	Велики Поповић	3	1400	\N	713872	34
3027	Витанце	3	\N	\N	713899	34
3028	Војник	3	\N	\N	713945	34
3029	Грабовица	3	\N	\N	713902	34
3030	Двориште	3	\N	\N	713929	34
3031	Деспотовац	3	360	\N	713937	34
3032	Жидиље	3	\N	\N	713953	34
3033	Златово	3	\N	\N	713961	34
3034	Јасеново	3	534	\N	713970	34
3035	Језеро	3	\N	\N	713988	34
3036	Јеловац	3	\N	\N	713996	34
3037	Липовица	3	\N	\N	714003	34
3038	Ломница	3	\N	\N	714011	34
3039	Маквиште	3	\N	\N	714020	34
3040	Медвеђа	3	807	\N	714038	34
3041	Милива	3	\N	\N	714046	34
3042	Пањевац	3	\N	\N	714054	34
3043	Плажане	3	1005	\N	714062	34
3044	Поповњак	3	\N	\N	714089	34
3045	Равна Река	3	\N	\N	714097	34
3046	Ресавица	3	1102	\N	714119	34
3047	Ресавица (село)	3	1102	\N	714127	34
3048	Сењски Рудник	3	1163	\N	714135	34
3049	Сладаја	3	\N	\N	714143	34
3050	Стењевац	3	1256	\N	714151	34
3051	Стрмостен	3	\N	\N	714160	34
3052	Трућевац	3	\N	\N	714178	34
3053	Багрдан	3	50	\N	737909	184
3054	Белица	3	\N	\N	737917	184
3055	Бресје	3	\N	\N	737925	184
3056	Буковче	3	\N	\N	737933	184
3057	Бунар	3	311	\N	737941	184
3058	Винорача	3	\N	\N	737950	184
3059	Вољавче	3	\N	\N	737968	184
3060	Врановац	3	\N	\N	737976	184
3061	Врба	3	\N	\N	737984	184
3062	Главинци	3	1556	\N	737992	184
3063	Глоговац	3	451	\N	738000	184
3064	Горње Штипље	3	\N	\N	738018	184
3065	Горњи Рачник	3	\N	\N	738026	184
3066	Деоница	3	\N	\N	738034	184
3067	Добра Вода	3	\N	\N	738042	184
3068	Доње Штипље	3	\N	\N	738069	184
3069	Доњи Рачник	3	\N	\N	738077	184
3070	Драгоцвет	3	405	\N	738085	184
3071	Драгошевац	3	406	\N	738093	184
3072	Дражмировац	3	\N	\N	738107	184
3073	Дубока	3	1557	\N	738115	184
3074	Ивковачки Прњавор	3	\N	\N	738123	184
3075	Јагодина	3	522	\N	738352	184
3076	Јошанички Прњавор	3	\N	\N	738131	184
3077	Каленовац	3	\N	\N	738140	184
3078	Ковачевац	3	\N	\N	738158	184
3079	Коларе	3	604	\N	738166	184
3080	Кончарево	3	610	\N	738174	184
3081	Кочино Село	3	\N	\N	738182	184
3082	Ловци	3	\N	\N	738204	184
3083	Лозовик	3	\N	\N	738212	184
3084	Лукар	3	\N	\N	738239	184
3085	Мајур	3	777	\N	738247	184
3086	Мали Поповић	3	\N	\N	738255	184
3087	Медојевац	3	\N	\N	738263	184
3088	Међуреч	3	\N	\N	738271	184
3089	Милошево	3	826	\N	738280	184
3090	Мишевић	3	\N	\N	738298	184
3091	Ново Ланиште	3	923	\N	738301	184
3092	Рајкинац	3	\N	\N	738310	184
3093	Ракитово	3	\N	\N	738328	184
3094	Рибаре	3	1105	\N	738336	184
3095	Рибник	3	\N	\N	738344	184
3096	Сиоковац	3	1181	\N	738379	184
3097	Слатина	3	\N	\N	738387	184
3098	Старо Ланиште	3	\N	\N	738395	184
3099	Старо Село	3	\N	\N	738409	184
3100	Стрижило	3	1266	\N	738417	184
3101	Топола	3	\N	\N	738425	184
3102	Трнава	3	\N	\N	738433	184
3103	Црнче	3	\N	\N	738441	184
3104	Шантаровац	3	\N	\N	738450	184
3105	Шуљковац	3	\N	\N	738468	184
3106	Бошњане	3	\N	\N	731307	35
3107	Буљане	3	\N	\N	731315	35
3108	Бусиловац	3	314	\N	731323	35
3109	Главица	3	\N	\N	731331	35
3110	Голубовац	3	\N	\N	731340	35
3111	Горња Мутница	3	384	\N	731358	35
3112	Горње Видово	3	\N	\N	731366	35
3113	Давидовац	3	\N	\N	731374	35
3114	Доња Мутница	3	\N	\N	731382	35
3115	Доње Видово	3	389	\N	731404	35
3116	Дреновац	3	411	\N	731412	35
3117	Забрега	3	\N	\N	731439	35
3118	Извор	3	\N	\N	731447	35
3119	Клачевица	3	\N	\N	731455	35
3120	Крежбинац	3	\N	\N	731463	35
3121	Лебина	3	\N	\N	731471	35
3122	Лешје	3	\N	\N	731480	35
3123	Мириловац	3	\N	\N	731498	35
3124	Параћин	3	975	\N	731501	35
3125	Плана	3	1000	\N	731510	35
3126	Поповац	3	1019	\N	731528	35
3127	Поточац	3	1023	\N	731536	35
3128	Ратаре	3	\N	\N	731544	35
3129	Рашевица	3	1081	\N	731552	35
3130	Својново	3	1311	\N	731579	35
3131	Сикирица	3	1175	\N	731587	35
3132	Сињи Вир	3	\N	\N	731595	35
3133	Сисевац	3	\N	\N	731609	35
3134	Стрижа	3	\N	\N	731617	35
3135	Стубица	3	1558	\N	731625	35
3136	Текија	3	\N	\N	731633	35
3137	Трешњевица	3	1335	\N	731641	35
3138	Чепуре	3	\N	\N	731650	35
3139	Шавац	3	\N	\N	731668	35
3140	Шалудовац	3	\N	\N	731676	35
3141	Баре	3	\N	\N	737542	36
3142	Белушић	3	113	\N	737569	36
3143	Беочић	3	\N	\N	737577	36
3144	Богалинац	3	\N	\N	737585	36
3145	Брајиновац	3	\N	\N	737593	36
3146	Велика Крушевица	3	\N	\N	737607	36
3147	Вукмановац	3	\N	\N	737615	36
3148	Доброселица	3	\N	\N	737623	36
3149	Драгово	3	407	\N	737631	36
3150	Жупањевац	3	\N	\N	737640	36
3151	Кавадар	3	\N	\N	737658	36
3152	Каленићки Прњавор	3	\N	\N	737666	36
3153	Калудра	3	\N	\N	737674	36
3154	Комаране	3	\N	\N	737682	36
3155	Лепојевић	3	\N	\N	737704	36
3156	Ломница	3	\N	\N	737712	36
3157	Лоћика	3	749	\N	737739	36
3158	Малешево	3	\N	\N	737747	36
3159	Мотрић	3	\N	\N	737755	36
3160	Надрље	3	\N	\N	737763	36
3161	Опарић	3	936	\N	737771	36
3162	Превешт	3	1040	\N	737780	36
3163	Рабеновац	3	\N	\N	737798	36
3164	Ратковић	3	\N	\N	737801	36
3165	Рековац	3	1100	\N	737810	36
3166	Секурич	3	\N	\N	737828	36
3167	Сибница	3	\N	\N	737836	36
3168	Сиљевица	3	\N	\N	737844	36
3169	Течић	3	\N	\N	737852	36
3170	Урсуле	3	\N	\N	737879	36
3171	Цикот	3	\N	\N	737887	36
3172	Шљивица	3	\N	\N	737895	36
3173	Бобово	3	244	\N	738476	37
3174	Бресје	3	\N	\N	738484	37
3175	Војска	3	1443	\N	738492	37
3176	Врлане	3	\N	\N	738506	37
3177	Гложане	3	\N	\N	738514	37
3178	Грабовац	3	\N	\N	738522	37
3179	Дубље	3	\N	\N	738549	37
3180	Дубница	3	\N	\N	738557	37
3181	Ђуринац	3	\N	\N	738565	37
3182	Купиновац	3	\N	\N	738573	37
3183	Кушиљево	3	705	\N	738581	37
3184	Луковица	3	\N	\N	738590	37
3185	Мачевац	3	\N	\N	738603	37
3186	Проштинац	3	\N	\N	738611	37
3187	Радошин	3	\N	\N	738620	37
3188	Роанда	3	\N	\N	738638	37
3189	Роћевац	3	\N	\N	738646	37
3190	Свилајнац	3	1306	\N	738654	37
3191	Седларе	3	1157	\N	738662	37
3192	Суботица	3	1273	\N	738689	37
3193	Тропоње	3	\N	\N	738697	37
3194	Црквенац	3	\N	\N	738719	37
3195	Батинац	3	\N	\N	744336	38
3196	Бигреница	3	234	\N	744344	38
3197	Вирине	3	\N	\N	744352	38
3198	Влашка	3	\N	\N	744379	38
3199	Дворица	3	\N	\N	744387	38
3200	Иванковац	3	\N	\N	744395	38
3201	Исаково	3	\N	\N	744409	38
3202	Јовац	3	549	\N	744417	38
3203	Кованица	3	\N	\N	744425	38
3204	Крушар	3	669	\N	744433	38
3205	Мијатовац	3	821	\N	744441	38
3206	Остриковац	3	\N	\N	744450	38
3207	Паљане	3	\N	\N	744468	38
3208	Сење	3	1162	\N	744476	38
3209	Супска	3	1296	\N	744484	38
3210	Ћуприја	3	347	\N	744492	38
3211	Бор	3	259	\N	706418	24
3212	Брестовац	3	264	\N	706426	24
3213	Бучје	3	\N	\N	706434	24
3214	Горњане	3	\N	\N	706442	24
3215	Доња Бела Река	3	379	\N	706469	24
3216	Злот	3	1501	\N	706477	24
3217	Кривељ	3	\N	\N	706485	24
3218	Лука	3	760	\N	706493	24
3219	Метовница	3	817	\N	706507	24
3220	Оштрељ	3	\N	\N	706515	24
3221	Слатина	3	1559	\N	706523	24
3222	Танда	3	\N	\N	706531	24
3223	Топла	3	\N	\N	706540	24
3224	Шарбановац	3	1148	\N	706558	24
3225	Брза Паланка	3	298	\N	716502	26
3226	Вајуга	3	\N	\N	716529	26
3227	Велесница	3	1385	\N	716537	26
3228	Велика Врбица	3	\N	\N	716545	26
3229	Велика Каменица	3	\N	\N	716553	26
3230	Грабовица	3	1560	\N	716561	26
3231	Давидовац	3	\N	\N	716570	26
3232	Кладово	3	588	\N	716588	26
3233	Кладушница	3	\N	\N	716596	26
3234	Корбово	3	616	\N	716600	26
3235	Костол	3	\N	\N	716618	26
3236	Купузиште	3	\N	\N	716626	26
3237	Љубичевац	3	\N	\N	716634	26
3238	Мала Врбица	3	\N	\N	716642	26
3239	Манастирица	3	\N	\N	716669	26
3240	Милутиновац	3	\N	\N	716677	26
3241	Нови Сип	3	1182	\N	716685	26
3242	Петрово Село	3	\N	\N	716693	26
3243	Подвршка	3	1014	\N	716707	26
3244	Река	3	\N	\N	716715	26
3245	Речица	3	\N	\N	716723	26
3246	Ртково	3	\N	\N	716731	26
3247	Текија	3	1317	\N	716740	26
3248	Бољетин	3	\N	\N	726770	25
3249	Влаоле	3	\N	\N	726788	25
3250	Голубиње	3	\N	\N	726796	25
3251	Дебели Луг	3	\N	\N	726800	25
3252	Доњи Милановац	3	397	\N	726818	25
3253	Јасиково	3	1561	\N	726826	25
3254	Клокочевац	3	597	\N	726834	25
3255	Лесково	3	\N	\N	726842	25
3256	Мајданпек	3	775	\N	726869	25
3257	Мироч	3	\N	\N	726877	25
3258	Мосна	3	\N	\N	726885	25
3259	Рудна Глава	3	1121	\N	726893	25
3260	Тополница	3	\N	\N	726907	25
3261	Црнајка	3	\N	\N	726915	25
3262	Александровац	3	\N	\N	728551	28
3263	Браћевац	3	274	\N	728454	28
3264	Брестовац	3	\N	\N	728462	28
3265	Буковче	3	\N	\N	728489	28
3266	Вељково	3	\N	\N	728497	28
3267	Видровац	3	\N	\N	728519	28
3268	Вратна	3	\N	\N	728527	28
3269	Дупљане	3	\N	\N	728535	28
3270	Душановац	3	428	\N	728543	28
3271	Јабуковац	3	519	\N	728560	28
3272	Јасеница	3	1562	\N	728578	28
3273	Карбулово	3	\N	\N	728586	28
3274	Кобишница	3	601	\N	728594	28
3275	Ковилово	3	\N	\N	728608	28
3276	Мала Каменица	3	\N	\N	728616	28
3277	Малајница	3	\N	\N	728624	28
3278	Милошево	3	\N	\N	728632	28
3279	Михајловац	3	819	\N	728659	28
3280	Мокрање	3	838	\N	728667	28
3281	Неготин	3	853	\N	728675	28
3282	Плавна	3	1563	\N	728683	28
3283	Поповица	3	\N	\N	728691	28
3284	Прахово	3	1032	\N	728705	28
3285	Радујевац	3	1071	\N	728713	28
3286	Рајац	3	1072	\N	728721	28
3287	Речка	3	\N	\N	728730	28
3288	Рогљево	3	1119	\N	728748	28
3289	Самариновац	3	\N	\N	728756	28
3290	Сиколе	3	1176	\N	728764	28
3291	Слатина	3	\N	\N	728772	28
3292	Смедовац	3	\N	\N	728799	28
3293	Србово	3	\N	\N	728802	28
3294	Тамнич	3	\N	\N	728829	28
3295	Трњане	3	1339	\N	728837	28
3296	Уровица	3	1357	\N	728845	28
3297	Црномасница	3	\N	\N	728853	28
3298	Чубра	3	\N	\N	728861	28
3299	Шаркамен	3	\N	\N	728870	28
3300	Штубик	3	1269	\N	728888	28
3301	Бачевица	3	\N	\N	706191	23
3302	Боговина	3	251	\N	706205	23
3303	Бољевац	3	256	\N	706213	23
3304	Бољевац Село	3	\N	\N	706221	23
3305	Валакоње	3	\N	\N	706230	23
3306	Врбовац	3	\N	\N	706248	23
3307	Добро Поље	3	\N	\N	706256	23
3308	Добрујевац	3	\N	\N	706264	23
3309	Илино	3	\N	\N	706272	23
3310	Јабланица	3	\N	\N	706299	23
3311	Криви Вир	3	664	\N	706302	23
3312	Луково	3	765	\N	706329	23
3313	Мали Извор	3	\N	\N	706337	23
3314	Мирово	3	\N	\N	706345	23
3315	Оснић	3	\N	\N	706353	23
3316	Подгорац	3	1564	\N	706361	23
3317	Ртањ	3	\N	\N	706370	23
3318	Рујиште	3	\N	\N	706388	23
3319	Савинац	3	1153	\N	706396	23
3320	Сумраковац	3	1294	\N	706400	23
3321	Боровац	3	\N	\N	715506	182
3322	Брусник	3	296	\N	715514	182
3323	Велика Јасикова	3	1389	\N	715522	182
3324	Велики Извор	3	1399	\N	715549	182
3325	Велики Јасеновац	3	\N	\N	715557	182
3326	Вражогрнац	3	1457	\N	715565	182
3327	Вратарница	3	1456	\N	715573	182
3328	Врбица	3	\N	\N	715581	182
3329	Гамзиград	3	445	\N	715590	182
3330	Глоговица	3	\N	\N	715603	182
3331	Горња Бела Река	3	\N	\N	715611	182
3332	Градсково	3	479	\N	715620	182
3333	Грлиште	3	486	\N	715638	182
3334	Грљан	3	487	\N	715646	182
3335	Дубочане	3	1565	\N	715654	182
3336	Заграђе	3	\N	\N	715662	182
3337	Зајечар	3	1484	\N	715689	182
3338	Звездан	3	1524	\N	715697	182
3339	Јелашница	3	\N	\N	715719	182
3340	Кленовац	3	\N	\N	715727	182
3341	Копривница	3	613	\N	715735	182
3342	Ласово	3	\N	\N	715743	182
3343	Леновац	3	725	\N	715751	182
3344	Лесковац	3	\N	\N	715760	182
3345	Лубница	3	756	\N	715778	182
3346	Мала Јасикова	3	\N	\N	715786	182
3347	Мали Извор	3	787	\N	715794	182
3348	Мали Јасеновац	3	788	\N	715808	182
3349	Мариновац	3	\N	\N	715816	182
3350	Метриш	3	\N	\N	715824	182
3351	Николичево	3	858	\N	715832	182
3352	Планиница	3	\N	\N	715859	182
3353	Прлита	3	\N	\N	715867	182
3354	Рготина	3	1104	\N	715875	182
3355	Салаш	3	1141	\N	715883	182
3356	Селачка	3	\N	\N	715891	182
3357	Табаковац	3	\N	\N	715905	182
3358	Трнавац	3	\N	\N	715913	182
3359	Халово	3	\N	\N	715921	182
3360	Чокоњар	3	\N	\N	715930	182
3361	Шипиково	3	\N	\N	715948	182
3362	Шљивар	3	\N	\N	715956	182
3363	Алдина Река	3	\N	\N	717169	27
3364	Алдинац	3	\N	\N	717177	27
3365	Балановац	3	\N	\N	717185	27
3366	Балинац	3	\N	\N	717193	27
3367	Балта Бериловац	3	\N	\N	717207	27
3368	Бањски Орешац	3	\N	\N	717215	27
3369	Бели Поток	3	107	\N	717223	27
3370	Берчиновац	3	\N	\N	717231	27
3371	Божиновац	3	\N	\N	717240	27
3372	Булиновац	3	\N	\N	717258	27
3373	Бучје	3	1013	\N	717266	27
3374	Валевац	3	\N	\N	717274	27
3375	Васиљ	3	1294	\N	717282	27
3376	Видовац	3	\N	\N	717304	27
3377	Вина	3	1417	\N	717312	27
3378	Витковац	3	\N	\N	717339	27
3379	Влашко Поље	3	\N	\N	717347	27
3380	Вртовац	3	\N	\N	717355	27
3381	Габровница	3	\N	\N	717363	27
3382	Глоговац	3	\N	\N	717371	27
3383	Горња Каменица	3	\N	\N	717380	27
3384	Горња Соколовица	3	\N	\N	717398	27
3385	Горње Зуниче	3	\N	\N	717401	27
3386	Градиште	3	\N	\N	717410	27
3387	Грезна	3	\N	\N	717428	27
3388	Дебелица	3	\N	\N	717436	27
3389	Дејановац	3	\N	\N	717444	27
3390	Доња Каменица	3	381	\N	717452	27
3391	Доња Соколовица	3	\N	\N	717479	27
3392	Доње Зуниче	3	394	\N	717487	27
3393	Дрвник	3	\N	\N	717495	27
3394	Дреновац	3	\N	\N	717509	27
3395	Дречиновац	3	\N	\N	717517	27
3396	Жлне	3	\N	\N	717525	27
3397	Жуковац	3	\N	\N	717533	27
3398	Зоруновац	3	\N	\N	717541	27
3399	Зубетинац	3	\N	\N	717550	27
3400	Иново	3	\N	\N	717568	27
3401	Јаковац	3	\N	\N	717576	27
3402	Јаловик Извор	3	\N	\N	717584	27
3403	Јања	3	\N	\N	717592	27
3404	Јелашница	3	\N	\N	717606	27
3405	Каличина	3	\N	\N	717614	27
3406	Кална	3	557	\N	717622	27
3407	Кандалица	3	\N	\N	717649	27
3408	Књажевац	3	600	\N	717657	27
3409	Кожељ	3	\N	\N	717665	27
3410	Крента	3	\N	\N	717673	27
3411	Лепена	3	\N	\N	717681	27
3412	Локва	3	\N	\N	717690	27
3413	Мањинац	3	\N	\N	717703	27
3414	Миљковац	3	\N	\N	717711	27
3415	Минићево	3	828	\N	717720	27
3416	Мучибаба	3	\N	\N	717738	27
3417	Ново Корито	3	\N	\N	717746	27
3418	Орешац	3	\N	\N	717754	27
3419	Ошљане	3	\N	\N	717762	27
3420	Папратна	3	\N	\N	717789	27
3421	Петруша	3	\N	\N	717797	27
3422	Подвис	3	1013	\N	717819	27
3423	Понор	3	\N	\N	717827	27
3424	Потркање	3	\N	\N	717835	27
3425	Причевац	3	\N	\N	717843	27
3426	Равна	3	\N	\N	717851	27
3427	Равно Бучје	3	\N	\N	717860	27
3428	Радичевац	3	\N	\N	717878	27
3429	Ргоште	3	\N	\N	717886	27
3430	Репушница	3	\N	\N	717894	27
3431	Сврљишка Топла	3	\N	\N	717908	27
3432	Скробница	3	\N	\N	717916	27
3433	Слатина	3	\N	\N	717924	27
3434	Стањинац	3	\N	\N	717932	27
3435	Старо Корито	3	\N	\N	717959	27
3436	Стогазовац	3	\N	\N	717967	27
3437	Татрасница	3	\N	\N	717975	27
3438	Трговиште	3	\N	\N	717983	27
3439	Трновац	3	\N	\N	717991	27
3440	Ћуштица	3	\N	\N	718025	27
3441	Црвење	3	\N	\N	718009	27
3442	Црни Врх	3	\N	\N	718017	27
3443	Шарбановац	3	1149	\N	718033	27
3444	Шести Габар	3	\N	\N	718041	27
3445	Штипина	3	\N	\N	718050	27
3446	Штитарац	3	\N	\N	718068	27
3447	Штрбац	3	\N	\N	718076	27
3448	Шуман Топла	3	\N	\N	718084	27
\.


--
-- Data for Name: cities_to_postal_codes; Type: TABLE DATA; Schema: private; Owner: postgres
--

COPY private.cities_to_postal_codes (id, city_id, postal_code_id) FROM stdin;
1524	1526	78
1525	1528	108
1526	1529	1396
1528	1535	811
1529	1539	168
1532	1545	255
1533	1547	1418
1534	1548	1463
1535	1549	488
1536	1553	559
1537	1555	736
1538	1558	1355
1539	1559	143
1540	1560	207
1541	1561	1353
1542	1564	87
1543	1568	1397
1544	1570	1465
1545	1572	420
1546	1574	553
1547	1576	718
1548	1581	1258
1549	1592	1388
1550	1593	1390
1551	1594	1437
1552	1597	521
1553	1598	628
1554	1599	614
1555	1603	834
1556	1604	1546
1557	1605	1059
1558	1610	1166
1559	1611	148
1560	1613	84
1561	1619	475
1562	1620	409
1563	1622	1480
1564	1628	769
1565	1630	928
1566	1636	1189
1567	1637	1270
1568	1641	158
1569	1642	263
1570	1646	954
1571	1647	957
1572	1648	1193
1573	1649	201
1574	1650	1
1575	1657	789
1576	1661	1076
1577	1662	1116
1578	1666	1220
1579	1668	151
1580	1669	94
1581	1670	257
1582	1671	370
1583	1672	526
1584	1673	994
1585	1674	1053
1586	1675	1297
1587	1676	206
1588	1677	1369
1589	1678	953
1590	1681	1131
1591	1682	1547
1592	1683	1356
1593	1685	53
1594	1686	33
1595	1687	42
1596	1690	494
1597	1693	568
1598	1694	662
1599	1695	785
1600	1697	925
1601	1698	874
1602	1700	941
1603	1701	974
1604	1702	956
1605	1703	1009
1606	1706	1244
1607	1707	752
1608	1708	786
1609	1709	437
1610	1710	52
1611	1711	43
1612	1713	236
1613	1714	1421
1614	1715	1315
1615	1716	1315
1616	1717	427
1617	1718	574
1618	1719	748
1619	1720	779
1620	1721	832
1621	1722	922
1622	1723	427
1623	1724	1247
1624	1725	1284
1625	1726	498
1626	1727	322
1627	1728	1295
1628	1729	1493
1629	1731	60
1630	1733	1493
1631	1734	804
1632	1735	884
1633	1736	1093
1634	1737	1237
1635	1738	1329
1636	1739	500
1637	1740	329
1638	1741	18
1639	1742	59
1640	1743	109
1641	1744	270
1642	1745	433
1643	1746	432
1644	1747	1504
1645	1748	1548
1646	1749	589
1647	1750	599
1648	1751	719
1649	1752	763
1650	1753	762
1651	1754	810
1652	1755	820
1653	1756	944
1654	1757	987
1655	1758	1240
1656	1759	1314
1657	1760	1325
1658	1761	436
1659	1762	326
1660	1763	13
1661	1764	1444
1662	1765	876
1663	1766	1070
1664	1767	1236
1665	1768	1324
1666	1769	245
1667	1770	694
1668	1771	882
1669	1772	924
1670	1773	64
1671	1774	254
1672	1776	531
1673	1777	533
1674	1778	608
1675	1779	647
1676	1780	856
1677	1781	1156
1678	1782	1302
1679	1783	1300
1680	1784	2
1681	1785	840
1682	1788	1359
1683	1789	7
1684	1790	1549
1685	1793	564
1686	1794	784
1687	1796	802
1688	1798	945
1689	1799	1331
1690	1800	1334
1691	1801	501
1692	1802	65
1693	1803	68
1694	1804	88
1695	1805	504
1696	1806	576
1697	1807	839
1698	1809	888
1699	1810	1133
1700	1811	1138
1701	1812	57
1702	1813	350
1703	1814	774
1704	1815	886
1705	1819	1238
1706	1821	247
1707	1822	468
1708	1823	575
1709	1824	1164
1710	1825	1330
1711	1827	1462
1712	1828	539
1713	1829	951
1714	1830	958
1715	1831	1145
1716	1832	338
1717	1833	333
1718	1834	15
1719	1835	67
1720	1836	1430
1721	1837	374
1722	1838	506
1723	1839	529
1724	1840	751
1725	1841	859
1726	1842	889
1727	1843	1160
1728	1844	56
1729	1845	62
1730	1846	98
1731	1847	1447
1732	1848	482
1733	1850	424
1734	1851	535
1735	1852	556
1736	1855	703
1737	1856	342
1738	1858	1383
1739	1859	1412
1740	1860	1431
1741	1861	1445
1742	1862	1471
1743	1864	493
1744	1865	1481
1745	1866	514
1746	1868	706
1747	1874	981
1748	1879	1264
1749	1880	1354
1750	1882	353
1751	1883	505
1752	1884	629
1753	1885	960
1754	1887	1144
1755	1888	1360
1756	1889	336
1757	1890	91
1758	1891	442
1759	1892	356
1760	1893	417
1761	1894	632
1762	1896	846
1763	1897	1008
1764	1898	1191
1765	1900	79
1766	1901	937
1767	1902	1140
1768	1903	1158
1769	1904	58
1770	1905	61
1771	1906	450
1772	1907	378
1773	1908	513
1774	1909	517
1775	1910	555
1776	1911	935
1777	1912	3
1778	1913	1246
1779	1915	85
1780	1916	1387
1781	1917	1398
1782	1919	544
1783	1920	696
1784	1922	797
1785	1924	824
1786	1925	1001
1787	1926	1249
1788	1927	824
1789	1928	17
1790	1929	698
1791	1930	1046
1792	1931	1309
1793	1932	1218
1794	1933	670
1795	1934	689
1796	1935	739
1797	1936	877
1798	1937	1132
1799	1938	1187
1800	1939	344
1801	1940	37
1802	1941	38
1803	1942	249
1804	1943	358
1805	1944	570
1806	1945	712
1807	1946	933
1808	1947	1089
1809	1948	1239
1810	1949	10
1811	1950	35
1812	1951	40
1813	1952	233
1814	1953	444
1815	1954	399
1816	1955	596
1817	1956	606
1818	1957	1085
1819	1958	1110
1820	1959	1305
1821	1960	1550
1822	1961	1242
1823	1962	1243
1824	1963	1318
1825	1964	334
1826	1965	26
1827	1966	46
1828	1967	246
1829	1968	1370
1830	1969	1004
1831	1970	1159
1832	1971	30
1833	1973	443
1834	1974	361
1835	1975	567
1836	1976	835
1837	1977	855
1838	1978	878
1839	1979	932
1840	1980	978
1841	1981	999
1842	1982	1177
1843	1983	1332
1844	1984	324
1845	1985	41
1846	1986	452
1847	1987	693
1848	1988	773
1849	1989	75
1850	1990	114
1851	1992	758
1852	1993	1075
1853	1995	1301
1854	1996	327
1855	1997	45
1856	1998	47
1857	1999	92
1858	2000	823
1859	2001	1067
1860	2002	44
1861	2003	1459
1862	2004	1502
1863	2006	686
1864	2007	1095
1865	2008	1154
1866	2009	473
1867	2010	426
1868	2011	1476
1869	2012	349
1870	2013	96
1871	2014	303
1872	2015	306
1873	2016	1414
1874	2017	554
1875	2018	587
1876	2019	630
1877	2020	723
1878	2021	1551
1879	2022	991
1880	2023	1129
1881	2024	1232
1882	2026	1257
1883	2027	438
1884	2028	325
1885	2029	850
1886	2030	1227
1887	2031	1347
1888	2032	1235
1889	2033	39
1890	2034	1185
1891	2035	1319
1892	2036	1416
1893	2037	446
1894	2038	750
1895	2039	843
1896	2040	1323
1897	2041	1139
1898	2042	232
1899	2043	510
1900	2045	657
1901	2046	747
1902	2047	796
1903	2048	885
1904	2049	921
1905	2051	1251
1906	2052	335
1907	2054	1464
1908	2057	511
1909	2058	538
1910	2059	673
1911	2060	673
1912	2065	22
1913	2067	355
1914	2069	571
1915	2070	697
1916	2071	931
1917	2072	934
1918	2073	983
1919	2074	1018
1920	2075	1041
1921	2076	1169
1922	2077	1229
1923	2078	1292
1924	2079	1179
1925	2080	302
1926	2082	1441
1927	2083	476
1928	2084	375
1929	2087	590
1930	2088	648
1931	2090	857
1932	2092	1003
1933	2093	1061
1934	2094	1126
1935	2095	1255
1936	2096	503
1937	2098	231
1938	2099	269
1939	2100	1401
1940	2101	485
1941	2102	368
1942	2103	1487
1943	2104	1487
1944	2105	530
1945	2106	707
1946	2107	708
1947	2108	737
1948	2109	794
1949	2110	801
1950	2111	771
1951	2112	875
1952	2113	1094
1953	2115	1142
1954	2116	1222
1955	2117	1234
1956	2119	321
1957	2120	1150
1958	2123	105
1959	2124	1442
1960	2125	456
1961	2126	665
1962	2127	879
1963	2128	881
1964	2129	1245
1965	2130	1248
1966	2131	1298
1967	2132	6
1968	2133	90
1969	2134	29
1970	2135	229
1971	2136	235
1972	2137	238
1973	2138	1381
1974	2139	1422
1975	2140	447
1976	2141	435
1977	2142	508
1978	2143	528
1979	2144	688
1980	2145	743
1981	2146	841
1982	2147	842
1983	2148	1051
1984	2149	1221
1985	2150	1173
1986	2151	49
1987	2152	77
1988	2154	248
1989	2156	453
1990	2157	415
1991	2158	593
1992	2164	339
1993	2168	1429
1994	2172	354
1995	2189	1058
1996	2200	391
1997	2201	402
1998	2204	561
1999	2205	602
2000	2208	1308
2001	2212	99
2002	2218	1488
2003	2222	668
2004	2234	72
2005	2237	288
2006	2252	401
2007	2253	1483
2008	2254	520
2009	2262	617
2010	2264	735
2011	2266	1552
2012	2267	753
2013	2280	1316
2014	2283	1340
2015	2297	385
2016	2298	413
2017	2302	746
2018	2311	1368
2019	2318	1394
2020	2320	380
2021	2322	790
2022	2323	1065
2023	2331	1379
2024	2337	373
2025	2338	412
2026	2342	1503
2027	2347	741
2028	2348	778
2029	2351	772
2030	2352	816
2031	2359	943
2032	2361	989
2033	2365	1052
2034	2367	1107
2035	2374	328
2036	2376	1135
2037	2378	1259
2038	2389	277
2039	2392	1371
2040	2403	365
2041	2404	366
2042	2407	400
2043	2417	562
2044	2424	724
2045	2436	1021
2046	2438	1045
2047	2458	250
2048	2461	516
2049	2462	711
2050	2472	1199
2051	2478	103
2052	2494	742
2053	2499	1196
2054	2504	287
2055	2521	829
2056	2530	1073
2057	2546	607
2058	2550	947
2059	2553	984
2060	2559	73
2061	2561	292
2062	2581	963
2063	2582	1069
2064	2593	1350
2065	2597	1393
2066	2598	1410
2067	2600	666
2068	2602	755
2069	2603	798
2070	2604	825
2071	2608	1252
2072	2612	1440
2073	2613	1455
2074	2617	414
2075	2618	605
2076	2621	740
2077	2622	759
2078	2624	780
2079	2626	818
2080	2627	948
2081	2629	1068
2082	2631	1146
2083	2633	1190
2084	2634	1202
2085	2638	23
2086	2645	454
2087	2647	702
2088	2651	1087
2089	2652	1161
2090	2653	1200
2091	2657	1406
2092	2669	776
2093	2677	1228
2094	2678	1327
2095	2684	276
2096	2688	455
2097	2690	371
2098	2694	592
2099	2706	12
2100	2710	1477
2101	2715	1022
2102	2723	1475
2103	2724	1482
2104	2727	659
2105	2729	720
2106	2734	946
2107	2743	1446
2108	2745	416
2109	2749	684
2110	2754	854
2111	2755	1062
2112	2761	1348
2113	2767	272
2114	2777	791
2115	2779	1208
2116	2786	313
2117	2790	1409
2118	2806	942
2119	2809	990
2120	2810	1079
2121	2811	1080
2122	2817	1167
2123	2818	83
2124	2821	275
2125	2827	419
2126	2831	594
2127	2832	626
2128	2838	1024
2129	2839	1017
2130	2845	20
2131	2846	70
2132	2849	308
2133	2850	1413
2134	2855	543
2135	2858	940
2136	2859	351
2137	2861	1077
2138	2862	1261
2139	2864	48
2140	2865	89
2141	2866	299
2142	2869	1492
2143	2871	1553
2144	2877	81
2145	2887	490
2146	2888	491
2147	2893	1479
2148	2896	598
2149	2909	1328
2150	2914	309
2151	2921	462
2152	2925	489
2153	2926	359
2154	2927	369
2155	2932	404
2156	2937	434
2157	2941	618
2158	2943	633
2159	2945	768
2160	2949	800
2161	2958	1103
2162	2960	1263
2163	2966	346
2164	2967	1554
2165	2968	715
2166	2979	425
2167	2980	792
2168	2984	1064
2169	2987	1183
2170	2989	111
2171	2994	1555
2172	3000	387
2173	3003	532
2174	3011	852
2175	3017	1326
2176	3026	1400
2177	3031	360
2178	3034	534
2179	3040	807
2180	3043	1005
2181	3046	1102
2182	3047	1102
2183	3048	1163
2184	3050	1256
2185	3053	50
2186	3057	311
2187	3062	1556
2188	3063	451
2189	3070	405
2190	3071	406
2191	3073	1557
2192	3075	522
2193	3079	604
2194	3080	610
2195	3085	777
2196	3089	826
2197	3091	923
2198	3094	1105
2199	3096	1181
2200	3100	1266
2201	3108	314
2202	3111	384
2203	3115	389
2204	3116	411
2205	3124	975
2206	3125	1000
2207	3126	1019
2208	3127	1023
2209	3129	1081
2210	3130	1311
2211	3131	1175
2212	3135	1558
2213	3137	1335
2214	3142	113
2215	3149	407
2216	3157	749
2217	3161	936
2218	3162	1040
2219	3165	1100
2220	3173	244
2221	3175	1443
2222	3183	705
2223	3190	1306
2224	3191	1157
2225	3192	1273
2226	3196	234
2227	3202	549
2228	3204	669
2229	3205	821
2230	3208	1162
2231	3209	1296
2232	3210	347
2233	3211	259
2234	3212	264
2235	3215	379
2236	3216	1501
2237	3218	760
2238	3219	817
2239	3221	1559
2240	3224	1148
2241	3225	298
2242	3227	1385
2243	3230	1560
2244	3232	588
2245	3234	616
2246	3241	1182
2247	3243	1014
2248	3247	1317
2249	3252	397
2250	3253	1561
2251	3254	597
2252	3256	775
2253	3259	1121
2254	3263	274
2255	3270	428
2256	3271	519
2257	3272	1562
2258	3274	601
2259	3279	819
2260	3280	838
2261	3281	853
2262	3282	1563
2263	3284	1032
2264	3285	1071
2265	3286	1072
2266	3288	1119
2267	3290	1176
2268	3295	1339
2269	3296	1357
2270	3300	1269
2271	3302	251
2272	3303	256
2273	3311	664
2274	3312	765
2275	3316	1564
2276	3319	1153
2277	3320	1294
2278	3322	296
2279	3323	1389
2280	3324	1399
2281	3326	1457
2282	3327	1456
2283	3329	445
2284	3332	479
2285	3333	486
2286	3334	487
2287	3335	1565
2288	3337	1484
2289	3338	1524
2290	3341	613
2291	3343	725
2292	3345	756
2293	3347	787
2294	3348	788
2295	3351	858
2296	3354	1104
2297	3355	1141
2298	3369	107
2299	3373	1013
2300	3375	1294
2301	3377	1417
2302	3390	381
2303	3392	394
2304	3406	557
2305	3408	600
2306	3415	828
2307	3422	1013
2308	3443	1149
\.


--
-- Data for Name: countries; Type: TABLE DATA; Schema: private; Owner: postgres
--

COPY private.countries (id, name, domain_name, area_code, alfa_code, numeric_code, designation) FROM stdin;
23	Мађарска	.ху	+36	ХУ	348	ХУН
24	Кина	.цн	+86	ЦН	156	ЦХН
25	Уједињени Арапски Емирати	.ае	+971	АЕ	784	АРЕ
26	Луксембург	.лу	+352	ЛУ	442	ЛУX
27	Израел	.ил	+972	ИЛ	376	ИСР
28	Белгија	.бе	+32	БЕ	56	БЕЛ
22	Бугарска	.бр	+359	БГ	100	БГР
32	Албанија 	.ал	+355	АЛ	8	АЛБ
31	Португалија	.пт	+351	ПТ	620	ПРТ
2	Босна и Херцеговина	.ба	+387	БА	70	БИХ
33	Алжир	.дз	+213	ДЗ	12	ДЗА
34	Бахами	.бс	+1(242)	БС	44	БХС
7	Аустрија	.ат‍	+43	АТ	40	АУТ
8	Пољска	.пл	+48	ПЛ	616	ПОЛ
9	Швајцарска	.цх	+41	ЦХ	756	ЦХЕ
10	Италија	.ит	+39	ИТ	380	ИТА
11	Шпанија	.ес	+34	ЕС	724	ЕСП
12	Словенија	.си	+386	СИ	705	СВН
13	Чешка Република	.цз	+420	ЦЗ	203	ЦЗЕ
14	Немачка	.де	+49	ДЕ	276	ДЕУ
15	Руска Федерација	.рф	+7	РУ	643	РУС
16	Хрватска	.хр	+385	ХР	191	ХРВ
17	Турска	.тр	+90	ТР	792	ТУР
18	Холандија	.нл	+31	НЛ	528	НЛД
19	Француска	.фр	+33	ФР	250	ФРА
20	САД (Америка)	.ус	+1	УС	840	УСА
21	Словачка	.ск	+421	СК	703	СВК
4	Република Македонија	.мк	+389	МК	807	МКД
6	Велика Британија	.ук	+44	ГБ	826	ГБР
5	Република Црна Гора	.ме	+382	МЕ	499	МНЕ
29	Либан	.лб	+961	ЛБ	422	ЛБН
30	Индија	.ин	+395	ИН	356	ИНД
35	Бахреин	.бс	+973	БХ	48	БХР
36	Бангладеш	.бд	+880	БД	50	БГД
37	Америчка Самоа	.ас	+684	АС	16	АСМ
38	Андора	.ад	+376	АД	20	АНД
39	Барбадос	.бб	+1(246)	ББ	52	БРБ
40	Ангола	.ао	+244	АО	24	АГО
41	Ангвила	.аи	+1 (264)	АИ	660	АИА
42	Белизе	.бз	+501	БЗ	84	БЛЗ
43	Белорусија	.бy	+375	БY	112	БЛР
44	Антигва и Барбуда	.аг	+1 (268)	АГ	28	АТГ
45	Аргентина	.ар	+54	АР	32	АРГ
46	Аруба	.аw	+297	АW	533	АБW
47	Аустралија	.ау	+61	АУ	36	АУС
48	Авганистан	.аф	+93	АФ	4	АФГ
49	Бенин	.бј	+229	БЈ	204	БЕН
50	Азербејџан	.аз	+994	АЗ	31	АЗЕ
51	Бермуди	.бм	+1(441)	БМ	60	БМУ
52	Боцвана	.бw	+267	БW	72	БWА
53	Боливија	.бо	+591	БО	68	БОЛ
54	Бразил	.бр	+55	БР	76	БРА
55	Данска	.дк	+45	ДК	208	ДНК
56	Буркина Фасо	.бф	+226	БФ	854	БФА
57	Бурунди	.би	+257	БИ	108	БДИ
58	Бутан	.бт	+975	БТ	64	БТН
59	Џибути	.дј	+253	ДЈ	262	ДЈИ
60	Египат	.ег	+20	ЕГ	818	ЕГY
61	Еквадор	.ец	+593	ЕЦ	218	ЕЦУ
62	Еритреја	.ер	+291	ЕР	232	ЕРИ
63	Естонија	.ее	+372	ЕЕ	233	ЕСТ
64	Етиопија	.ет	+251	ЕТ	231	ЕТХ
65	Девичанска Острва (САД)	.вг	+1 (340)	ВИ	850	ВИР
66	Доминика	.дм	+1 (767)	ДМ	212	ДМА
67	Фарска Острва	.фо	+298	ФО	234	ФРО
68	Доминиканска Република	.до	+1 (809) 	ДО	214	ДОМ
69	Фиџи	.фј	+679	ФЈ	242	ФЈИ
70	Габон	.га	+241	ГА	266	ГАБ
71	Филипини	.пх	+63	ПХ	608	ПХЛ
72	Гамбија	.гм	+220	ГМ	270	ГМБ
73	Финска	.фи	+358	ФИ	246	ФИН
74	Гана	.гх	+233	ГХ	288	ГХА
75	Фокландска Острва (Малвини)	\N	+500	ФК	238	ФЛК
76	Гибралтар	.ги	+350	ГИ	292	ГИБ
77	Гренада	.гд	+1 (473)	ГД	308	ГРД
78	Грузија	.ге	+995	ГЕ	268	ГЕО
79	Француска Полинезија	.пф	+689	ПФ	258	ПYФ
80	Грчка	.гр	+30	ГР	300	ГРЦ
81	Хаити	.хт	+509	ХТ	332	ХТИ
82	Хондурас	.хн	+504	ХН	340	ХНД
83	Гватемала	.гт	+502	ГТ	320	ГТМ
84	Хонг Конг	.хк	+852	хк	344	ХКГ
85	Гвинеја Бисао	.гw	+245	ГW	624	ГНБ
86	Индонезија	.ид	+62	ИД	360	ИДН
87	Јамајка	.јм	+1(876)	ЈМ	388	ЈАМ
88	Ирак	.иq	+964	ИQ	368	ИРQ
89	Јапан	.јп	+81	ЈП	392	ЈПН
90	Иран (Исламска Република)	.ир	+98	ИР	364	ИРН
91	Јемен	.yе	+967	YЕ	887	YЕМ
92	Ирска	.ие	+353	ИЕ	372	ИРЛ
93	Јордан	.јо	+962	ЈО	400	ЈОР
94	Исланд	.ис	+354	ИС	352	ИСЛ
95	Лаос	.ла	+856	ЛА	418	ЛАО
96	Лесото	.лс	+266	ЛС	426	ЛСО
97	Летонија	.лв	+371	ЛВ	428	ЛВА
98	Либерија	.лр	+231	ЛР	430	ЛБР
99	Кајманска Острва	.кy	+1 (345)	КY	136	ЦYМ
100	Либија	.лy	+218	ЛY	434	ЛБY
101	Камбоџа	.кх	+855	КХ	116	КХМ
102	Камерун	.цм	+237	ЦМ	120	ЦМР
103	Лихетенштајн	.ли	+423	ЛИ	438	ЛИЕ
104	Канада	.ца	+1	ЦА	124	ЦАН
105	Катар	.qа	+974	QА	634	QАТ
106	Казахстан	.кз	+7	КЗ	398	КАЗ
107	Литванија	.лт	+370	ЛТ	440	ЛТУ
108	Кенија	.ке	+254	КЕ	404	КЕН
109	Кипар	.цy	+357	ЦY	196	ЦYП
110	Киргистан	.кг	+996	КГ	417	КГЗ
111	Мадагаскар	.мг	+261	МГ	450	МДГ
112	Кирибати	.ки	+686	КИ	296	КИР
113	Макао	.мо	+853	МО	446	МАЦ
114	Малави	.мw	+265	МW	454	МWИ
115	Малдиви	.мв	+960	МВ	462	МДВ
116	Колумбија	.цо	+57	ЦО	170	ЦОЛ
117	Малезија	.мy	+60	МY	458	МYС
118	Комори	.км	+269	КМ	174	ЦОМ
119	Мали	\N	+223	МЛ	466	МЛИ
120	Малта	.мт	+356	МТ	470	МЛТ
122	Мароко	.ма	+212	МА	504	МАР
124	Мартиник	.мq	+596	МQ	474	МТQ
121	Конго	.цг	+242	ЦГ	178	ЦОГ
125	Маршалска Острва	.мх	+692	МХ	584	МХЛ
126	Маурицијус	.му	+230	МУ	480	МУС
128	Мауританија	.мр	+222	МР	478	МРТ
127	Кореја, Демократска Народна Република	.кп	+850	КП	408	ПРК
129	Мексико	.мx	+52	МX	484	МЕX
130	Кореја Јужна	.кр	+82	КР	410	КОР
131	Микронезија (Уједињене Државе)	.фм	+691	ФМ	583	ФСМ
132	Молдавија, Република	.мд	+373	МД	498	МДА
133	Монако	.мц	+377	МЦ	492	МЦО
134	Монголија	.мн	+976	МН	496	МНГ
135	Костарика	.цр	+506	ЦР	188	ЦРИ
136	Мозамбик	.мз	+258	МЗ	508	МОЗ
137	Куба	.цу	+53	ЦУ	192	ЦУБ
138	Намибија	.на	+264	НА	516	НАМ
139	Кукова Острва	.цк	+682	ЦК	184	ЦОК
140	Кувајт	.кw	+965	КW	414	КWТ
141	Обала Слоноваче	.ци	+225	ЦИ	384	ЦИВ
142	Оман	.ом	+968	ОМ	512	ОМН
143	Науру	.нр	+674	НР	520	НРУ
144	Пакистан	.пк	+92	ПК	586	ПАК
145	Непал	.нп	+977	НП	524	НПЛ
146	Палау	.пw	+680	ПW	585	ПЛW
147	Нигер	.не	+227	НЕ	562	НЕР
148	Палестина	\N	+970	ПС	275	ПСЕ
149	Нигерија	.нг	+234	НГ	566	НГА
150	Панама	.па	+507	ПА	591	ПАН
151	Никарагва	.ни	+505	НИ	558	НИЦ
152	Папуа Нова Гвинеја	.пг	+675	ПГ	598	ПНГ
153	Норвешка	.но	+47	НО	578	НОР
154	Парагвај	.пy	+595	ПY	600	ПРY
155	Нова Каледонија	.нц	+687	НЦ	540	НЦЛ
156	Нови Зеланд	.нз	+64	НЗ	554	НЗЛ
157	Перу	.пе	+51	ПЕ	604	ПЕР
158	Питкерн	.пн	\N	ПН	612	ПЦН
159	Порторико	.пр	+1 (787)	ПР	630	ПРИ
160	Руанда	.рw	+250	РW	646	РWА
161	Румунија	.ро	+40	РО	642	РОУ
162	Самоа	.ас	+684	WС	882	WСМ
163	Сан Марино	.см	+378	СМ	674	СМР
164	Саудијска Арабија	.са	+966	СА	682	САУ
165	Сејшели	.сц	+248	СЦ	690	СYЦ
166	Сенегал	.сн	+221	СН	686	СЕН
167	Таџикистан	.тј	+992	ТЈ	762	ТЈК
168	Тајланд	.тх	+66	ТХ	764	ТХА
169	Сент Винсент и Гренадини	.вц	+1(784)	ВЦ	670	ВЦТ
170	Тајван, Кинеска Провинција	.тw	+886	ТW	158	ТWН
171	Сент Лусија	.лц	+1(758)	ЛЦ	662	ЛЦА
172	Танзанија	.тз	+255	ТЗ	834	ТЗА
173	Сент Китс - Невис	.кн	+1(869)	КН	659	КНА
174	Тимор - Лесте 	.тп	+670	ТЛ	219	ТМЛ
175	Того	.тг	+228	ТГ	768	ТГО
176	Северномаријанска Острва	.мп	+1(670)	МП	580	МНП
177	Сијера Леоне	.сл	+232	СЛ	694	СЛЕ
178	Токелау	.тк	+690	ТК	772	ТКЛ
179	Тонга	.то	+676	ТО	776	ТОН
180	Сингапур	.сг	+65	СГ	702	СГП
181	Тринидад и Тобаго	.тт	+1 (868)	ТТ	780	ТТО
182	Тунис	.тн	+216	ТН	788	ТУН
183	Туркменистан	\N	+993	ТМ	795	ТКМ
184	Сиријска Арапска Република	.сy	+963	СY	760	СYР
186	Тувалу	.тв	+688	ТВ	798	ТУВ
187	Уганда	.уг	+256	УГ	800	УГА
188	Соломонска Острва	.сб	+677	СБ	90	СЛБ
189	Украјина	.уа	+380	УА	804	УКР
190	Сомалија	.со	+252	СО	706	СОМ
191	Уругвај	.уy	+598	УY	858	УРY
192	Судан	.сд	\N	СД	736	СДН
193	Суринам	.ср	+597	СР	740	СУР
194	Света Јелена	.сх	+290	СХ	654	СХН
195	Узбекистан	.уз	+998	УЗ	860	УЗБ
196	Вануату	.ву	+678	ВУ	548	ВУТ
197	Свети Бартоломеј	\N	\N	БЛ	652	БЛМ
198	Ватикан - Света Столица	.ва	+379	ВА	336	ВАТ
206	Чад	.тд	+235	ТД	148	ТЦД
199	Замбија	.зм	+260	ЗМ	894	ЗМБ
200	Венецуела	.ве	+58	ВЕ	862	ВЕН
201	Западна Сахара	.ех	\N	ЕХ	732	ЕСХ
202	Вијетнам	.вн	+84	ВН	704	ВНМ
203	Зеленортска Острва	.цв	+238	ЦВ	132	ЦПВ
205	Зимбабве	.зw	+263	ЗW	716	ЗWЕ
207	Шри Ланка	.лк	+94	ЛК	144	ЛКА
208	Шведска	.се	+46	СЕ	752	СWЕ
209	Бонаире, Синт Еустазије и Саба	\N	\N	БQ	535	БЕС
210	Антарктик	.аq	\N	АQ	10	АТА
211	Божићна Острва	.цx	\N	ЦХ	162	ЦXР
3	Република Србија	.рс	+381	РС	688	СРБ
204	Острва Волис и Футуна 	\N	\N	WФ	876	WЛФ
185	Острва Туркс и Каикос	.тц	+1 (649) 	ТЦ	796	ТЦА
252	Тимор	\N	\N	\N	626	\N
212	Арменија	\N	\N	АН	51	АРМ
213	Британска Територија Индијског Океана	.ио	\N	ИО	86	ИОТ
214	Централноафричка Република	\N	\N	ЦФ	140	ЦАФ
216	Брунеј Дарусалам	\N	\N	БН	96	БРН
217	Буве Острво	.бв	\N	БВ	74	БВТ
218	Гренланд	.гл	+299	ГЛ	304	ГРЛ
219	Џерси	\N	\N	ЈЕ	832	ЈЕY
220	Гуам	\N	+1 (671)	ГУ	316	ГУМ
221	Екваторијална Гвинеја	.гq	+240	ГQ	226	ГНQ
222	Ел Салвадор	.св	+503	СВ	222	СЛВ
223	Гваделупе	.гп	\N	ГП	312	ГЛП
224	Европска Унија	.еу	\N	ЕУ	978	ЕЕЗ
225	Француска Гијана	\N	\N	ГФ	254	ГУФ
226	Гвајана	\N	\N	ГY	328	ГУY
228	Гвинеја	.гн	+224	ГН	324	ГИН
229	Јужни Судан	\N	\N	СС	728	ССД
230	Јужна Африка	\N	+27	ЗА	710	ЗАФ
232	Јужна Џорџија и Јужна Сендвич Острва	\N	\N	ГС	239	СГС
233	Јужна Француска Територија	\N	\N	ТФ	260	АТФ
234	Курасао	\N	\N	ЦW	531	ЦУW
235	Мајоте	.yт	\N	YТ	175	МYТ
236	Међународне финансијске организације	\N	\N	МФ	950	МФО
237	Мјанмар	.мм	\N	ММ	104	ММР
239	Монсерат	.мс	\N	МС	500	МСР
240	Кокосова (Килинг) Острва	.цц	\N	ЦЦ	166	ЦЦК
241	Ниуе	\N	\N	НУ	570	НИУ
243	Сао Томе и Принципе	.ст	\N	СТ	678	СТП
244	Сентпјер и Микелон	\N	\N	ПМ	666	СПМ
247	Свазиленд	.сз	\N	СЗ	748	СWЗ
248	Аландска острва	\N	\N	АX	248	АЛА
249	Чиле	.цл	+56	ЦЛ	152	ЦХЛ
250	Девичанска Острва (Британска)	.вг	+1 (284)	ВГ	92	ВГБ
251	Реинион	.ре	\N	РЕ	638	РЕУ
238	Држава Мањих Удаљених Острва	\N	\N	УМ	581	УМИ
246	Острва Свалбард и Јан Мајен	\N	\N	СЈ	744	СЈМ
227	Острва Херд и Мекдоналд	.хм	\N	ХМ	334	ХМД
215	Острво Гернси	\N	\N	ГГ	831	ГГY
231	Острво Ман	\N	\N	ИМ	833	ИМН
242	Острво Норфолк	.нф	\N	НФ	574	НФК
245	Свети Мартин (холандски део)	\N	\N	СX	534	СXМ
123	Конго, Демократска Република	.цд	+243	ЦД	180	ЦОД
\.


--
-- Data for Name: currencies; Type: TABLE DATA; Schema: private; Owner: postgres
--

COPY private.currencies (id, name, short_name, code) FROM stdin;
50	Шпанска пезета	ЕСП	724
51	Етиопски бир	ЕТБ	230
52	Евро	ЕУР	978
53	Финска марка	ФИМ	246
87	Вон	КРW	410
88	Кувајтски динар	КWД	414
89	Кајмански долар	КYД	136
90	Тенџе	КЗТ	398
91	Кип	ЛАК	418
92	Либанска фунта	ЛБП	422
93	Рупија Шри Ланке	ЛКР	144
94	Либеријски долар	ЛРД	430
95	Лоти	ЛСЛ	426
96	Литвански литас	ЛТЛ	440
97	Луксембуршки франак	ЛУФ	442
103	Денар	МКД	807
54	Фиџи долар	ФЈД	242
55	Фокландска фунта	ФКП	238
56	Француски франак	ФРФ	250
57	Фунта стерлинга	ГБП	826
58	Лари	ГЕЛ	981
61	Даласи	ГМД	270
62	Гвинејски франак	ГНФ	324
86	Северно корејски вон	КПW	408
25	Бахамски долар	БСД	044
26	Нгултрум	БТН	064
27	Пула	БWП	072
28	Белоруска рубља	БYР	974
29	Белиски долар	БЗД	084
30	Канадски долар	ЦАД	124
42	Немачка марка	ДЕМ	280
43	Џибутски франак	ДЈФ	262
44	Данска круна	ДКК	208
45	Доминикански пезос	ДОП	214
46	Алжирски динар	ДЗД	012
47	Круна	ЕЕК	233
48	Египатска фунта	ЕГП	818
49	Накфа	ЕРН	232
131	Катарски ријал	QАР	634
132	Леј	РОН	946
133	Српски динар	РСД	941
134	Руска рубља	РУБ	643
135	Руандски франак	РWФ	646
136	Саудијски риал	САР	682
137	Долар соломонских острва	СБД	090
138	Сејшелска рупија	СЦР	690
139	Суданска фунта	СДГ	938
140	Шведска круна	СЕК	752
142	Свете Јелене фунта	СХП	654
143	Толар	СИТ	705
144	Словачка круна	СКК	703
145	Леоне	СЛЛ	694
146	Сомалијски шилинг	СОС	706
147	Суринамски долар	СРД	968
148	Добра	СТД	678
149	Ел Салвадорски колон	СВЦ	222
150	Сиријска фунта	СYП	760
151	Лилангени	СЗЛ	748
152	Бат	ТХБ	764
154	Нови Манат	ТМТ	934
99	Либијски динар	ЛYД	434
100	Марокански дирхам	МАД	504
101	Молдавски леј	МДЛ	498
102	Малгашки аријари	МГА	969
104	Киат	ММК	104
105	Тугрик	МНТ	496
106	Петака	МОП	446
107	Угља	МРО	478
108	Малтешка лира	МТЛ	470
109	Маурицијска рупија	МУР	480
110	Малдивска рупија	МВР	462
111	Квача	МWК	454
112	Мексички пезос	МXН	484
113	Малезијски рингит	МYР	458
114	Метикал	МЗН	943
115	Намибија долар	НАД	516
116	Наира	НГН	566
117	Кордоба оро	НИО	558
118	Холандски гулден	НЛГ	528
119	Норвешка круна	НОК	578
120	Непалска рупија	НПР	524
19	Бурундски франак	БИФ	108
20	Бермудски долар	БМД	060
21	Брунејски долар	БНД	096
22	Боливијано	БОБ	068
23	Мвдол	БОВ	984
24	Бразилски реал	БРЛ	986
31	Франак Дем.Реп. Конго	ЦДФ	976
32	Швајцарски франак	ЦХФ	756
33	Унидадес де фоменто	ЦЛФ	990
34	Чилеански пезос	ЦЛП	152
35	Јуан	ЦНY	156
36	Колумбијски пезос	ЦОП	170
37	Костарикански колон	ЦРЦ	188
38	Кубански пезос	ЦУП	192
39	Зеленортски ескудо	ЦВЕ	132
40	Кипарска фунта	ЦYП	196
41	Чешка круна	ЦЗК	203
59	Седи	ГХС	936
60	Гибралтарска фунта	ГИП	292
155	Туниски динар	ТНД	788
156	Паанга	ТОП	776
157	Турска лира	ТРY	949
158	Тринидад и тобаго долар	ТТД	780
159	Тајвански долар	ТWД	901
160	Танзанијски шилинг	ТЗС	834
161	Гривна	УАХ	980
162	Угандски шилинг	УГX	800
163	Амерички долар	УСД	840
164	Уругвајски пезос	УYУ	858
165	Узбекистан сум	УЗС	860
166	Боливар	ВЕФ	937
1	Андорска пезета	АДП	020
2	УАЕ дирхам	АЕД	784
3	Афгани	АФН	971
4	Лек	АЛЛ	008
5	Јерменијски драм	АМД	051
6	Холандскоантилски гулден	АНГ	532
7	Кванза	АОА	973
8	Аргентински пезос	АРС	032
9	Шилинг	АТС	040
10	Аустралијски долар	АУД	036
11	Арубијски гулден	АWГ	533
12	Азербејџански манат	АЗН	944
13	Конвертибилна марка	БАМ	977
14	Барбадоски долар	ББД	052
15	Така	БДТ	050
16	Белгијски франак	БЕФ	056
17	Лев	БГН	975
18	Бахреински динар	БХД	048
63	Драхма	ГРД	300
64	Кецал	ГТQ	320
65	Гвајана долар	ГYД	328
66	Хонгконг долар	ХКД	344
67	Лемпира	ХНЛ	340
68	Хрватска куна	ХРК	191
69	Гурд	ХТГ	332
70	Форинта	ХУФ	348
71	Рупија	ИДР	360
72	Ирска фунта	ИЕП	372
73	Нови Израелски шекел	ИЛС	376
74	Индијска рупија	ИНР	356
75	Ирачки динар	ИQД	368
76	Ирански ријал	ИРР	364
77	Исландска круна	ИСК	352
78	Италијанска лира	ИТЛ	380
79	Јамајчански долар	ЈМД	388
80	Јордански динар	ЈОД	400
81	Јен	ЈПY	392
82	Кенијски шилинг	КЕС	404
83	Сом	КГС	417
84	Ријал	КХР	116
85	Коморски франак	КМФ	174
121	Новозеландски долар	НЗД	554
122	Омански ријал	ОМР	512
123	Балбоа	ПАБ	590
124	Нуево сол	ПЕН	604
125	Кина	ПГК	598
126	Филипински пезос	ПХП	608
127	Пакистанска рупија	ПКР	586
128	Злот	ПЛН	985
129	Португалски ескудо	ПТЕ	620
98	Летонски латс	ЛВЛ	428
130	Гварани	ПYГ	600
141	Сингапурски долар	СГД	702
153	Сомони	ТЈС	972
167	Донг	ВНД	704
168	Вату	ВУВ	548
169	Тала	WСТ	882
170	ЦФА франак БЕАЦ	XАФ	950
171	Источнокарипски долар	XЦД	951
172	ЦФА франак БЦЕАО	XОФ	952
173	ЦФП франак	XПФ	953
174	Јеменски риал	YЕР	886
175	Ранд	ЗАР	710
176	Замбијска квача	ЗМW	967
177	Зимбабвеански долар	ЗWЛ	932
\.


--
-- Data for Name: driver_license_categories; Type: TABLE DATA; Schema: private; Owner: postgres
--

COPY private.driver_license_categories (id, category, description, age_range) FROM stdin;
2	AM	mopedi, laki tricikli i laki četvorocikli	16
3	A1	Motocikli zapremine do 125 cm3 i snage motora do 11kW, čiji odnos snage motora i mase vozila nije veći od 0,1 kW/kg i teški tricikli snage motora do 15kW	16
4	A2	Motocikli čija snaga motora nije veća od 35kW i čiji odnos snage motora i mase vozila nije veći od 0,2 kW/kg	18
5	A	Motocikli i teški tricikli čija snaga motora prelazi 35kW	20-24
6	B1	Teški četvorocikli	18
7	B	Motorna vozila, osim vozila kategorije A, A1, A2, AM, F i M, čija dozvoljena masa nije veća od 3500kg i koja nemaju više od osam sedišta ne računajući sedište za vozača	18
8	BE	Skup vozila čije vučno vozilo pripada kategoriji B, najveća dozvoljena masa priključnog vozila je veća od 750 kg, a nije veća od 3500 kg	18
9	C1	Motorna vozila, osim vozila kategorije A, A1, A2, AM, F, M, B, D i D1, čija je najveća dozvoljena masa veća od 3500 kg, a nije veća od 7500 kg	18
10	C1E	Skup vozila čije vučno vozilo spada u kategoriju C1, a najveća dozvoljena masa priključnog vozila prelazi 750 kg i naveća dozvoljena masa skupa ne prelazi 12.000 kg, kao i skup vozila čije vučno vozilo spada u kategoriju B, a najveća dozvoljena masa priključnog vozila prelazi 3.500 kg i najveća dozvoljena masa skupa vozila ne prelazi 12.000 kg	18
11	C	Motorna vozila, osim kategorije A, A1, A2, AM, F, M, B, D i D1, čija je najveća dozvoljena masa veća od 3.500 kg	21
12	CE	Skup vozila čije vučno vozilo pripada kategoriji C, a najveća dozvoljena masa priključnog vozila je veća od 750 kg	21
13	D1	Motorna vozila za prevoz lica, koja osim sedišta za vozača imaju više od osam, a najviše 16 sedišta i čija maksimalna dužina ne prelazi osam metara	21
14	D1E	Skup vozila čije vučno vozilo pripada kategoriji D1, a najveća dozvoljena masa priključnog vozila je veća od 750 kg	21
15	D	Motorna vozila za prevoz lica, koja osim sedišta za vozača imaju više od osam sedišta	24
16	DE	Skup vozila čije vučno vozilo pripada kategoriji D, a najveća dozvoljena masa priključnog vozila je veća od 750 kg	24
17	F	Traktori sa ili bez priključnih vozila i radne mašine	16
18	M	Motokultivator	15
\.


--
-- Data for Name: fixed_asset_types; Type: TABLE DATA; Schema: private; Owner: postgres
--

COPY private.fixed_asset_types (id, name, description, depreciation_rate, fixed_asset_amortization_group_id) FROM stdin;
1	Asfaltne površine		0.00	1
2	Avionske piste		0.00	1
3	Brane za akumulaciju voda		0.00	1
4	Cevi za gasovod		0.00	1
5	Dokovi za vezivanje brodova		0.00	1
6	Elektrane		0.00	1
7	Električni dalekovodi		0.00	1
8	Eskalatori - pokretne stepenice		0.00	1
9	Hangari		0.00	1
10	Lukobrani		0.00	1
11	Marine		0.00	1
12	Mostovi		0.00	1
13	Nadvožnjaci i vijadukti		0.00	1
14	Naftovodi		0.00	1
15	Odvodni i dovodni kanali		0.00	1
16	Parking površine		0.00	1
17	Putevi i auto-putevi		0.00	1
18	Ribnjaci		0.00	1
19	Skladišta i rezervoari		0.00	1
20	Sportski objekti (stadioni, bazeni, sportske hale i dr.)		0.00	1
21	Silosi na poljoprivrednim dobrima		0.00	1
22	Tuneli		0.00	1
23	Vodovodi i cevovodi		0.00	1
24	Železnička infrastruktura		0.00	1
25	Zgrade		0.00	1
26	Sve ostale nepomenute nepokretnosti		0.00	1
27	Avioni		0.00	2
28	Automobili		0.00	2
29	Brodovi i ostali plovni objekti		0.00	2
30	Klima uređaji		0.00	2
31	Liftovi		0.00	2
32	Bojleri		0.00	2
33	Nameštaj u brodovima		0.00	2
34	Medicinska oprema		0.00	2
35	Ograde		0.00	2
36	Oprema za kancelariju		0.00	2
37	Oprema za proizvodnju i distribuciju solarne energije		0.00	2
38	Oprema za proizvodnju i distribuciju električne energije, gasa, toplote i vode		0.00	2
39	Vagoni		0.00	2
40	Vinogradi		0.00	2
41	Voćnjaci		0.00	2
42	Nematerijalna ulaganja (koncesije, licence, patenti, žigovi, modeli, autorska prava, franšiza i drua		0.00	2
43	Alat i inventar		0.00	3
44	Autobusi		0.00	3
45	Oprema za termo elektrane		0.00	3
46	Oprema za proizvodnju mleka i mlečnih proizvoda		0.00	3
47	Fiskalne kase		0.00	3
48	Fliperi		0.00	3
49	Hladnjače za povrće		0.00	3
50	Kalkulatori		0.00	3
51	Kamioni i prikolice		0.00	3
52	Laboratorijska oprema		0.00	3
53	Mašine za čišćenje žitarica		0.00	3
54	Oprema za fotokopiranje		0.00	3
55	Nameštaj koji nije pomenut na drugim mestima		0.00	3
56	Oprema za istraživanje		0.00	3
57	Postrojenja za pravljenje betona		0.00	3
58	Pokretna oprema za proizvodnju električne energije (agregati i sl.)		0.00	3
59	Radari		0.00	3
60	Televizijske antene		0.00	3
61	Sva ostala stalna sredstva (osim nepokretnosti) koja nisu posebno naznačena u grupama II do V		0.00	3
62	Nameštaj u avionima		0.00	4
63	Oprema za kontrolu zagađenja vazduha i vode - nelicencirana		0.00	4
64	Oprema za emitovanje radio i TV programa		0.00	4
65	Oprema za naftne bušotine		0.00	4
66	Oprema za obradu rude		0.00	4
67	Rezervni delovi za avione		0.00	4
68	Telegrafska i telefonska oprema (žice i razni kablovi)		0.00	4
69	Automobili za iznajmljivanje ili lizing i taksi vozila		0.00	5
70	Bilbordovi		0.00	5
71	Električne reklame		0.00	5
72	Branici za puteve i pruge		0.00	5
73	Elektronska oprema za procesiranje podataka (kompjuteri) i sistemski i aplikacioni softveri		0.00	5
74	Oprema za informatičku infrastrukturu ( Yupak, Internet)		0.00	5
75	Filmovi		0.00	5
76	Televizijske reklame i spotovi		0.00	5
77	Građevinska pokretna oprema		0.00	5
78	Kalupi za livenje		0.00	5
79	Knjige u biblioteci koje se iznajmljuju		0.00	5
80	Industrijski noževi		0.00	5
81	Oprema za seču drveća		0.00	5
82	Platno (tepisi, zastori, zavese, itisoni i sl.)		0.00	5
83	Pokretna oprema koja koristi električnu energiju (bušilica, brusilica i sl.)		0.00	5
84	Pokretni kampovi		0.00	5
85	Skeneri za bar kod		0.00	5
86	Traktori		0.00	5
87	Uniforme		0.00	5
88	Video igre - na novčić		0.00	5
89	Video trake, CD, DVD i sl.		0.00	5
90	Osnovno stado		0.00	5
\.


--
-- Data for Name: fixed_assets_amortization_groups; Type: TABLE DATA; Schema: private; Owner: postgres
--

COPY private.fixed_assets_amortization_groups (id, group_name, depreciation_rate) FROM stdin;
1	I	2.50
2	II	10.00
3	III	15.00
4	IV	20.00
5	V	30.00
\.


--
-- Data for Name: fuels; Type: TABLE DATA; Schema: private; Owner: postgres
--

COPY private.fuels (name, category, id) FROM stdin;
AUTOGAS TNG 		6
PREMIJUM BMB 95		8
EVRO PREMIJUM BMB 95		2
EVRO BMB 98		5
EVRO DIZEL		1
BMB 98		9
PREMIJUM EVRODIZEL ULTRA D		4
METAN CNG		7
PREMIJUM BENZIN G-Drive 100		3
\.


--
-- Data for Name: international_diagnosis_of_professional_diseases; Type: TABLE DATA; Schema: private; Owner: postgres
--

COPY private.international_diagnosis_of_professional_diseases (id, code, name, description) FROM stdin;
\.


--
-- Data for Name: international_diagnosis_of_work_diseases; Type: TABLE DATA; Schema: private; Owner: postgres
--

COPY private.international_diagnosis_of_work_diseases (id, code, name, description) FROM stdin;
\.


--
-- Data for Name: measurement_units; Type: TABLE DATA; Schema: private; Owner: postgres
--

COPY private.measurement_units (id, name, short_name) FROM stdin;
2	DUŽINA	\N
3	TEŽINA	\N
4	ZAPREMINA	\N
5	POVRŠINA	\N
6	SNAGA	\N
\.


--
-- Data for Name: mode_injuries; Type: TABLE DATA; Schema: private; Owner: postgres
--

COPY private.mode_injuries (id, code, name, description, parent_id) FROM stdin;
2	0	нема информација	\N	\N
3	10	догађај који је претходио повреди због електричних кварова, експлозије и пожара - није назначено	\N	\N
4	11	електрични квар на опреми - који доводи до индиректног контакта	\N	3
5	12	електрични квар на опреми - који доводи до директног контакта	\N	3
6	13	експлозија	\N	3
7	14	пожар и/или пламен	\N	3
8	19	други догађај који није наведен у групи 10	\N	3
9	20	догађај који је претходио повреди услед преливања, превртања, цурења, протока, испаравања и емисије - није назначено	\N	\N
10	21	чврсто стање - преливање и превртање	\N	9
11	22	течно стање - цурење, испуштање, изливање и прскање	\N	9
12	23	гасовито стање - испаравање, ослобађање аеросола и ослобађање гаса	\N	9
13	24	прашкасти материјал - стварање дима, прашине, честица у суспензији или емисији	\N	9
14	29	други догађај који није наведен у групи 20	\N	9
15	30	ломљење, пуцање, раздвајање, клизање, пад лица и пад материјала или предмета - није назначено	\N	\N
16	31	ломљење материјала - на зглобу и на шавовима	\N	15
17	32	ломљење или пуцање материјала (дрво, стакло, метал, камен, пластика, итд.)	\N	15
18	33	исклизнуће, пад лица и пад материјала или предмета - одозго (падајући на повређеног)	\N	15
19	34	исклизнуће, пад лица и пад материјала или предмета - истовремено	\N	15
20	35	исклизнуће, пад лица и пад материјала или предмета - на истом нивоу	\N	15
21	39	други догађај који није наведен у групи 30	\N	15
22	40	губитак контроле (потпуне или делимичне) над машином, превозним средством или при руковању опремом, ручним алатом, предметом и животињом - није назначено	\N	\N
23	41	губитак контроле (потпуне или делимичне) - над машином (укључујући нежељено укључивање) или над материјалом који се обрађује	\N	22
24	42	губитак контроле (потпуне или делимичне) - над превозним средством или опремом (механизована или не)	\N	22
25	43	губитак контроле (потпуне или делимичне) - над ручним алатом (механизованим или не) или над материјалом који се обрађује уз помоћ тог алата	\N	22
26	44	губитак контроле (потпуне или делимичне) - над предметом (који се носи, помера, којим се рукује)	\N	22
27	45	губитак контроле (потпуне или делимичне) - над животињом	\N	22
28	49	други догађај који није наведен у групи 40	\N	22
29	50	клизање, спотицање и пад лица - није назначено	\N	\N
30	51	пад лица на нижи ниво	\N	29
31	52	клизање, спотицање и пад лица на исти ниво	\N	29
32	59	други догађај који није наведен у групи 50	\N	29
33	60	покрети тела без икаквог физичког напора (који доводе до спољашњих повреда) - није назначено	\N	\N
34	61	кретање по оштром предмету	\N	33
35	62	клечање, седење и нагињање	\N	33
36	63	контакт са опремом за рад, телом или предметом које је у покрету	\N	33
37	64	некоординисани покрети, преурањене или закаснеле реакције које су у вези са послом који треба да се обави	\N	33
38	69	други догађај који није наведен у групи 60	\N	33
39	70	покрети тела са или без физичког напора (који доводе до унутрашњих повреда) - није назначено	\N	\N
40	71	подизање, ношење и устајање	\N	39
41	72	гурање и вучење	\N	39
42	73	спуштање и савијање	\N	39
43	74	увртање и окретање	\N	39
44	75	несмотрено гажење, увртање ноге или чланка, клизање без пада	\N	39
45	79	други догађај који није наведен у групи 70	\N	39
46	80	шок, страх, насиље, напад, претња и присуство - није назначено	\N	\N
47	81	шок и страх	\N	46
48	82	насиље, напад и претња - према запосленима у радној јединици који су под непосредном контролом послодавца	\N	46
49	83	насиље, напад, претња - од трећих лица према запосленима који обављају своје послове (пљачка банке)	\N	46
50	84	напад од стране животиње	\N	46
51	85	присуство повређеног или трећег лица што доводи до опасности по личну и безбедност других	\N	46
52	89	други догађај који није наведен у групи 80	\N	46
53	99	други догађај који није наведен у класификацији	\N	\N
\.


--
-- Data for Name: occupations; Type: TABLE DATA; Schema: private; Owner: postgres
--

COPY private.occupations (id, name, code, parent_id) FROM stdin;
1	РУКОВОДИОЦИ 	1	\N
2	Законодавци, високи званичници и извршни директори 	11	1
3	Законодавци и високи званичници 	111	2
4	Законодавци 	1111	3
5	Градоначелник (Председник градског већа) 	1111.01	4
6	Градски секретар (Члан градског већа)	1111.02	4
7	Министар у Влади 	1111.03	4
8	Народни посланик 	1111.04	4
9	Одборник скупштине града	1111.05	4
10	Одборник скупштине општине	1111.06	4
11	Покрајински секретар 	1111.07	4
12	Посланик покрајинске скупштине	1111.08	4
13	Председник Владе 	1111.09	4
14	Председник Народне скупштине	1111.10	4
15	Председник општине (Председник општинског већа)	1111.11	4
16	Председник покрајинске владе 	1111.12	4
17	Председник покрајинске скупштине	1111.13	4
18	Председник Републике	1111.14	4
19	Председник скупштине града	1111.15	4
20	Председник скупштине општине	1111.16	4
21	Члан општинског већа	1111.17	4
22	Високи државни званичници 	1112	3
23	Амбасадор	1112.01	22
24	Вицегувернер 	1112.02	22
25	Генерални конзул	1112.03	22
26	Генерални секретар Владе 	1112.04	22
27	Генерални секретар министарства	1112.05	22
28	Генерални секретар Скупштине 	1112.06	22
29	Гувернер Народне банке 	1112.07	22
30	Директор посебне организације / органа у саставу / службе владе	1112.08	22
31	Директор међународне организације 	1112.09	22
32	Директор полиције 	1112.10	22
33	Државни правобранилац	1112.11	22
34	Државни ревизор 	1112.12	22
35	Државни секретар 	1112.13	22
36	Заштитник грађана	1112.14	22
37	Конзул 	1112.15	22
38	Начелник градске управе	1112.16	22
39	Начелник општинске управе	1112.17	22
40	Начелник управног округа 	1112.18	22
41	Повереник за заштиту равноправности 	1112.19	22
42	Повереник за информације од  јавног значаја и заштиту података о личности	1112.20	22
43	Политички директор министарства	1112.21	22
44	Помоћник градоначелника	1112.22	22
45	Помоћник директора посебне организације / органа у саставу / службе владе	1112.23	22
46	Помоћник министра 	1112.24	22
47	Председник Врховног касационог суда	1112.25	22
48	Председник Уставног суда	1112.26	22
49	Републички јавни тужилац	1112.27	22
50	Секретар Народне скупштине	1112.28	22
51	Шеф сталне дипломатске мисије	1112.29	22
52	Руководиоци локалне самоуправе 	1113	3
53	Шеф месне канцеларије	1113.01	52
54	Високи званичници организација од посебног интереса 	1114	3
55	Генерални секретар синдиката	1114.01	54
56	Генерални секретар спортског савеза	1114.02	54
57	Директор политичке партије 	1114.03	54
58	Директор хуманитарне организације	1114.04	54
59	Председник гранског синдиката	1114.05	54
60	Председник политичке партије	1114.06	54
61	Председник привредне коморе	1114.07	54
62	Председник синдиката 	1114.08	54
63	Председник удружења	1114.09	54
64	Председник удружења послодаваца	1114.10	54
65	Секретар политичке партије 	1114.11	54
66	Генерални и извршни директори 	112	2
67	Генерални и извршни директори 	1120	66
68	Директор јавне службе / организације у јавном сектору	1120.01	67
69	Генерални директор у грађевинарству	1120.02	67
70	Генерални директор у индустрији	1120.03	67
71	Генерални директор у пољопривреди	1120.04	67
72	Генерални директор у услужном сектору	1120.05	67
73	Извршни директор у грађевинарству	1120.06	67
74	Извршни директор у индустрији	1120.07	67
75	Извршни директор у пољопривреди	1120.08	67
76	Извршни директор у услужном сектору	1120.09	67
77	Помоћник директора јавне службе / организације у јавном сектору	1120.10	67
78	Председник управног/извршног одбора	1120.11	67
79	Регионални директор 	1120.12	67
80	Административни и комерцијални руководиоци	12	1
81	Руководиоци пословних и административних јединица	121	80
82	Финансијски руководиоци 	1211	81
83	Руководилац за рачуноводство и ревизију	1211.01	82
84	Руководилац за управљање финансијама	1211.02	82
85	Финансијски директор	1211.03	82
86	Руководиоци за људске ресурсе 	1212	81
87	Директор за људске ресурсе	1212.01	86
88	Руководилац за планирање и развој људских ресурса  	1212.02	86
89	Руководилац за радне односе	1212.03	86
90	Руководилац за селекцију људских ресурса 	1212.04	86
91	Руководиоци за стратешко планирање и политику 	1213	81
92	Директор за стратешко планирање и пословну политику	1213.01	91
93	Руководилац за корпоративно планирање	1213.02	91
94	Руководилац за пословну политику 	1213.03	91
95	Руководилац за стратешко планирање	1213.04	91
96	Руководиоци за правне послове, секретари и други руководиоци у пословним услугама, администрацији и општим пословима	1219	81
97	Директор за правне послове	1219.01	96
98	Јавни тужилац	1219.02	96
99	Правобранилац	1219.03	96
100	Председник суда опште надлежности	1219.04	96
101	Председник суда посебне надлежности	1219.05	96
102	Пројект менаџер	1219.06	96
103	Руководилац за административне услуге	1219.07	96
104	Руководилац за безбедност и здрављe на раду 	1219.08	96
105	Руководилац за инспекцијске послове	1219.09	96
106	Руководилац за комуналне послове	1219.10	96
107	Руководилац за личне услуге 	1219.11	96
108	Руководилац за опште послове	1219.12	96
109	Руководилац за управљање квалитетом	1219.13	96
110	Руководилац обезбеђења	1219.14	96
111	Руководилац пословних услуга 	1219.15	96
112	Руководилац правних послова	1219.16	96
113	Секретар јавне службе / организације у јавном сектору	1219.17	96
114	Секретар министарства / државног органа	1219.18	96
115	Секретар пословног система	1219.19	96
116	Секретар предшколске установе	1219.20	96
117	Секретар суда/тужилаштва/правобранилаштва	1219.21	96
118	Секретар факултета/института	1219.22	96
119	Секретар школе	1219.23	96
120	Тужилац за организовани криминал	1219.24	96
121	Тужилац за ратне злочине	1219.25	96
122	Шеф кабинета	1219.26	96
123	Руководиоци продаје, маркетинга и развоја 	122	80
124	Руководиоци продаје и маркетинга 	1221	123
125	Директор маркетинга 	1221.01	124
126	Комерцијални директор	1221.02	124
127	Регионални менаџер продаје 	1221.03	124
128	Руководилац продаје	1221.04	124
129	Руководиоци за односе с јавношћу и пропаганду	1222	123
130	Руководилац за издаваштво	1222.01	129
131	Руководилац за информисање	1222.02	129
132	Руководилац за односе с јавношћу	1222.03	129
133	Руководилац за рекламирање и оглашавање	1222.04	129
134	Руководиоци за истраживање и развој 	1223	123
135	Руководилац научних истраживања и развоја 	1223.01	134
136	Руководилац за истраживање и развој	1223.02	134
137	Руководилац за развој пословних процеса	1223.03	134
138	Руководилац за развој производа 	1223.04	134
139	Руководиоци производње и специјализованих услуга	13	1
140	Руководиоци производње у пољопривреди, шумарству и рибарству	131	139
141	Руководиоци производње у пољопривреди и шумарству	1311	140
142	Руководилац за пројектовање, подизање и одржавање зеленила	1311.01	141
143	Руководилац на великим фармама 	1311.02	141
144	Руководилац на плантажама 	1311.03	141
145	Руководилац у пољопривреди	1311.04	141
146	Руководилац у шумарству	1311.05	141
147	Руководиоци производње у рибарству 	1312	140
148	Директор рибњака 	1312.01	147
149	Руководилац производње аквакултура 	1312.02	147
150	Руководилац у рибарству	1312.03	147
151	Руководиоци прерађивачке индустрије, рударства, грађевинарства и дистрибуције	132	139
152	Руководиоци у прерађивачкој индустрији	1321	151
153	Директор предузећа у прерађивачкој индустрији	1321.01	152
154	Руководилац производње у прерађивачкој индустрији	1321.02	152
155	Технички директор у прерађивачкој индустрији 	1321.03	152
156	Руководиоци у рударству	1322	151
157	Директор рудника 	1322.01	156
158	Руководилац експлоатације нафте и гаса 	1322.02	156
159	Руководилац одржавања рударске опреме и постројења 	1322.03	156
160	Руководилац производње у каменоломима 	1322.04	156
161	Руководилац производње у руднику	1322.05	156
162	Технички директор у рударству	1322.06	156
163	Управник рудника	1322.07	156
164	Руководиоци у грађевинарству 	1323	151
165	Грађевински предузимач 	1323.01	164
166	Директор градње 	1323.02	164
167	Директор грађевинског предузећа 	1323.03	164
168	Пројект менаџер за изградњу	1323.04	164
169	Пројект менаџер у грађевинарству 	1323.05	164
170	Шеф градилишта	1323.06	164
171	Руководиоци набавке и дистрибуције 	1324	151
172	Директор аеродрома	1324.01	171
173	Директор луке	1324.02	171
174	Руководилац за ланац снадбевања 	1324.03	171
175	Руководилац за логистичку подршку 	1324.04	171
176	Руководилац за систем урбаног транзита 	1324.05	171
177	Руководилац за снадбевање и дистрибуцију 	1324.06	171
178	Руководилац набавке	1324.07	171
179	Руководилац поштанских услуга	1324.08	171
180	Руководилац складиштења 	1324.09	171
181	Руководилац транспорта	1324.10	171
182	Руководилац цевоводног транспорта	1324.11	171
183	Руководилац шпедиције 	1324.12	171
184	Технички директор транспорта	1324.13	171
185	Управник аутобуске станице 	1324.14	171
186	Управник железничке станице	1324.15	171
187	Управник поште	1324.16	171
188	Руководиоци информационо-комуникационих технологија	133	139
189	Руководиоци информационо-комуникационих технологија	1330	188
190	Директор за информациони инжењеринг	1330.01	189
191	Директор информационих система 	1330.02	189
192	Директор телекомуникација 	1330.03	189
193	Руководилац за ИКТ мреже 	1330.04	189
194	Руководилац за интернет услуге 	1330.05	189
195	Руководилац за обраду података	1330.06	189
196	Руководилац за развој апликација 	1330.07	189
197	Руководилац за развој информационо-комуникационих технологија 	1330.08	189
198	Руководилац телекомуникационих услуга 	1330.09	189
199	Руководиоци услуга у здравству, социјалној заштити, образовању, финансијама, осигурању, научноистраживачкој делатности и култури 	134	139
200	Руководиоци стручних служби за бригу о деци 	1341	199
201	Руководилац дома за децу без родитељског старања 	1341.01	200
202	Руководилац центра за дечју заштиту 	1341.02	200
203	Руководиоци стручних служби у здравству 	1342	199
204	Директор болнице 	1342.01	203
205	Директор дома здравља	1342.02	203
206	Директор клинике 	1342.03	203
207	Директор установе здравствене заштите 	1342.04	203
208	Помоћник директора установе здравствене заштите 	1342.05	203
209	Руководилац организационе јединице установе здравствене заштите 	1342.06	203
210	Руководиоци стручних служби за бригу о одраслим и старим лицима 	1343	199
211	Директор геронтолошког центра 	1343.01	210
212	Директор дома за стара лица 	1343.02	210
213	Директор установе за бригу о одраслим лицима 	1343.03	210
214	Помоћник директора установе за бригу о одраслим лицима 	1343.04	210
215	Руководилац геронтолошког центра / дома за стара лица 	1343.05	210
216	Руководилац службе за кућну негу 	1343.06	210
217	Руководилац службе за негу старих 	1343.07	210
218	Руководиоци стручних служби за социјалну заштиту и ресоцијализацију 	1344	199
219	Директор дома за малолетне делинквенте 	1344.01	218
220	Директор казнено-поправне установе 	1344.02	218
221	Директор установе социјалне заштите 	1344.03	218
395	Хидробиолог	2131.29	366
222	Помоћник директора установе социјалне заштите 	1344.04	218
223	Руководилац прихватилишта за стара и одрасла лица	1344.05	218
224	Руководилац центра за социјални рад 	1344.06	218
225	Руководиоци у образовању 	1345	199
226	Декан факултета 	1345.01	225
227	Директор високе школе / високе школе струковних студија 	1345.02	225
228	Директор основне школе 	1345.03	225
229	Директор предшколске установе 	1345.04	225
230	Директор средње школе 	1345.05	225
231	Помоћник директора образовне установе 	1345.06	225
232	Председник академије струковних студија 	1345.07	225
233	Продекан факултета	1345.08	225
234	Проректор универзитета 	1345.09	225
235	Ректор универзитета 	1345.10	225
236	Шеф катедре	1345.11	225
237	Руководиоци стручних служби у области финансија и осигурања 	1346	199
238	Директор банке 	1346.01	237
239	Директор за управљање ризицима у банкарству 	1346.02	237
240	Директор завода/фонда за осигурање 	1346.03	237
241	Директор осигуравајућег друштва	1346.04	237
242	Директор филијале за осигурање имовине и лица 	1346.05	237
243	Руководилац агенције за осигурање	1346.06	237
244	Руководилац експозитуре банке	1346.07	237
245	Руководилац за банкарско извештавање, анализу и моделирање	1346.08	237
246	Руководилац за контролу усклађености пословања банке 	1346.09	237
247	Руководилац за кредитни портфолио	1346.10	237
248	Руководилац за наплату потраживања	1346.11	237
249	Руководилац за реструктурирање и специјализовану подршку клијентима	1346.12	237
250	Руководиоци истраживачких организација, установа културе и други 	1349	199
251	Директор музеја 	1349.01	250
252	Директор научног института 	1349.02	250
253	Директор позоришта	1349.03	250
254	Директор студентског културног центра 	1349.04	250
255	Директор установе културе 	1349.05	250
256	Директор филмске продукције 	1349.06	250
257	Помоћник директора установе културе 	1349.07	250
258	Руководилац архиве	1349.08	250
259	Руководилац уметничке галерије 	1349.09	250
260	Руководилац установе културе 	1349.10	250
261	Управник студентског/ученичког дома 	1349.11	250
262	Руководиоци у туризму, угоститељству, трговини и сродним услугама	14	1
263	Руководиоци хотела и ресторана 	141	262
264	Руководиоци хотела 	1411	263
265	Менаџер мотела 	1411.01	264
266	Менаџер хостела 	1411.02	264
267	Менаџер хотела 	1411.03	264
268	Руководиоци ресторана	1412	263
269	Менаџер за послуживање хране и пића – кетеринг  	1412.01	268
270	Менаџер кафића/клуба 	1412.02	268
271	Менаџер ресторана 	1412.03	268
272	Руководилац угоститељског објекта	1412.04	268
273	Руководиоци у трговини на велико и мало	142	262
274	Руководиоци у трговини на велико и мало	1420	273
275	Менаџер продавнице 	1420.01	274
276	Менаџер супермаркета 	1420.02	274
277	Менаџер у велепродаји 	1420.03	274
278	Менаџер у малопродаји 	1420.04	274
279	Руководилац за спољну трговину	1420.05	274
280	Управник апотеке	1420.06	274
281	Руководиоци у осталим услужним делатностима	143	262
282	Руководиоци спортских, рекреативних и културних центара	1431	281
283	Директор спортског центра 	1431.01	282
284	Директор установе за спорт и физичку културу 	1431.02	282
285	Менаџер биоскопа 	1431.03	282
286	Менаџер забавног парка 	1431.04	282
287	Менаџер казина 	1431.05	282
288	Менаџер рекреативног центра 	1431.06	282
289	Менаџер у спорту	1431.07	282
290	Помоћник директора установе за спорт и физичку културу	1431.08	282
291	Руководилац спортске организације	1431.09	282
292	Руководилац у спорту и рекреацији	1431.10	282
293	Руководиоци у осталим услужним делатностима неразврстани на другом месту 	1439	281
294	Директор марине	1439.01	293
295	Менаџер козметичког салона	1439.02	293
296	Менаџер ауто-кампа	1439.03	293
719	Хомеопат 	2230.02	717
297	Менаџер контакт центра 	1439.04	293
298	Менаџер конференцијског центра 	1439.05	293
299	Менаџер туристичке агенције 	1439.06	293
300	Менаџер у тржном центру	1439.07	293
301	Менаџер центра за камповање 	1439.08	293
302	СТРУЧЊАЦИ И УМЕТНИЦИ	2	\N
303	Стручњаци основних и примењених наука	21	302
304	Стручњаци физичких и гео-наука	211	303
305	Физичари и астрономи 	2111	304
306	Аеро-динамичар	2111.01	305
307	Астроном	2111.02	305
308	Астрофизичар	2111.03	305
309	Балистичар	2111.04	305
310	Истраживач у области физике	2111.05	305
311	Медицински физичар 	2111.06	305
312	Метролог	2111.07	305
313	Молекуларни физичар	2111.08	305
314	Нуклеарни физичар	2111.09	305
315	Реолог	2111.10	305
316	Стручњак за енергију из обновљивих извора 	2111.11	305
317	Стручњак механике	2111.12	305
318	Термодинамичар	2111.13	305
319	Физичар	2111.14	305
320	Физичар акустичар	2111.15	305
321	Физичар за електрицитет и магнетизам	2111.16	305
322	Физичар за електронику	2111.17	305
323	Физичар за оптичку/светлосну физику	2111.18	305
324	Физичар хидродинамичар	2111.19	305
325	Метеоролози	2112	304
326	Агрометеоролог	2112.01	325
327	Климатолог 	2112.02	325
328	Метеоролог	2112.03	325
329	Синоптичар	2112.04	325
330	Хидрометеоролог 	2112.05	325
331	Хемичари и физикохемичари	2113	304
332	Истраживач у области физикохемије	2113.01	331
333	Истраживач у области хемије	2113.02	331
334	Кристалографиста	2113.03	331
335	Нуклеарни хемичар 	2113.04	331
336	Физикохемичар	2113.05	331
337	Хемичар	2113.06	331
338	Хемичар аналитичар	2113.07	331
339	Геолози и геофизичари 	2114	304
340	Геолог геотехнике	2114.01	339
341	Геолог минералних сировина	2114.02	339
342	Геолог регионалне геологије 	2114.03	339
343	Геомагнетичар	2114.04	339
344	Геоморфолог	2114.05	339
345	Геофизичар 	2114.06	339
346	Истраживач у области геологије 	2114.07	339
347	Палеонтолог 	2114.08	339
348	Петролог	2114.09	339
349	Седиментолог	2114.10	339
350	Сеизмолог 	2114.11	339
351	Спелеолог	2114.12	339
352	Хидрогеолог	2114.13	339
353	Математичари, актуари (стручњаци осигурања) и статистичари	212	303
354	Математичари, актуари (стручњаци осигурања) и статистичари	2120	353
355	Актуар 	2120.01	354
356	Аналитичар оперативних истраживања 	2120.02	354
357	Аналитичар осигурања	2120.03	354
358	Демограф 	2120.04	354
359	Истраживач у области математике 	2120.05	354
360	Ликвидатор штета 	2120.06	354
361	Математичар	2120.07	354
362	Статистичар 	2120.08	354
363	Статистичар биометричар	2120.09	354
364	Статистичар истраживач 	2120.10	354
365	Стручњаци биолошких наука	213	303
366	Биолози, ботаничари, зоолози и сродни стручњаци	2131	365
367	Анатом	2131.01	366
368	Бактериолог	2131.02	366
369	Биокибернетичар	2131.03	366
370	Биолог	2131.04	366
371	Биомедицински истраживач 	2131.05	366
372	Биометричар	2131.06	366
373	Биотехнолог	2131.07	366
374	Биофизичар	2131.08	366
375	Биохемичар	2131.09	366
376	Ботаничар 	2131.10	366
377	Генетичар	2131.11	366
378	Ембриолог	2131.12	366
379	Ентомолог	2131.13	366
380	Зоо-бактериолог	2131.14	366
381	Зоолог	2131.15	366
382	Истраживач у области биологије	2131.16	366
383	Ихтиолог	2131.17	366
384	Кинолог	2131.18	366
385	Медицински биохемичар	2131.19	366
386	Микробиолог	2131.20	366
387	Молекуларни биолог 	2131.21	366
388	Молекуларни генетичар 	2131.22	366
389	Орнитолог 	2131.23	366
390	Патолог за биљке	2131.24	366
391	Таксоном за животиње/биљке	2131.25	366
392	Токсиколог	2131.26	366
393	Фармаколог	2131.27	366
394	Физиолог за животиње/биљке	2131.28	366
396	Хистолог за животиње/биљке	2131.30	366
397	Цитолог за животиње/биљке	2131.31	366
398	Стручњаци и саветници у области пољопривреде, шумарства и рибарства	2132	365
399	Агроном 	2132.01	398
400	Инжењер за шумско подручје	2132.02	398
401	Истраживач у области пољопривреде	2132.03	398
402	Пројектант за израду планских докумената за газдовање шумама/ловиштем 	2132.04	398
403	Пројектант за шумске саобраћајнице	2132.05	398
404	Пројектант озелењавања	2132.06	398
405	Саветник за воћарство и виноградарство 	2132.07	398
406	Саветник за ловну привреду 	2132.08	398
407	Саветник за ратарство и повртарство 	2132.09	398
408	Саветник за рибарство 	2132.10	398
409	Саветник за сточарство и пчеларство 	2132.11	398
410	Саветник за цвећарство  	2132.12	398
411	Саветник за шумарство 	2132.13	398
412	Саветник за заштиту биља 	2132.14	398
413	Стручњак за гајење дивљачи 	2132.15	398
414	Стручњак за гајење и заштиту шума 	2132.16	398
415	Стручњак за генофонд и расадничку производњу	2132.17	398
416	Стручњак за коришћење шума и шумских ресурса 	2132.18	398
417	Стручњак за рибарско подручје	2132.19	398
418	Технолог за виноградарство	2132.20	398
419	Технолог за воћарство	2132.21	398
420	Технолог за исхрану животиња  	2132.22	398
421	Технолог за повртарство	2132.23	398
422	Технолог за ратарство	2132.24	398
423	Технолог за селекцију у ратарству и повртарству 	2132.25	398
424	Технолог за селекцију у сточарству 	2132.26	398
425	Технолог за сточарство	2132.27	398
426	Технолог за цвећарство	2132.28	398
427	Технолог заштите биља	2132.29	398
428	Технолог заштите шума од ерозије 	2132.30	398
429	Технолог пољопривредне механизације	2132.31	398
430	Технолог пољопривредних мелиорација	2132.32	398
431	Технолог припреме производње у шумарству	2132.33	398
432	Стручњаци за заштиту животне средине 	2133	365
433	Аналитичар економије животне средине 	2133.01	432
434	Аналитичар заштите животне средине	2133.02	432
435	Аналитичар квалитета ваздуха 	2133.03	432
436	Аналитичар квалитета воде 	2133.04	432
437	Ботанички еколог	2133.05	432
438	Еколог 	2133.06	432
439	Проверивач заштите животне средине	2133.07	432
440	Саветник за заштиту животне средине 	2133.08	432
441	Санитарни еколог 	2133.09	432
442	Стручњак за заштиту животне средине	2133.10	432
443	Инжењери (осим електротехничких)	214	303
444	Индустријски и процесни инжењери 	2141	443
445	Индустријски инжењер 	2141.01	444
446	Инжењер за индустријска постројења 	2141.02	444
447	Инжењер за индустријску ефикасност 	2141.03	444
448	Инжењер организације рада	2141.04	444
449	Инжењер производње	2141.05	444
450	Инжењер студије рада – планер	2141.06	444
451	Грађевински инжењери 	2142	443
452	Грађевински аналитичар	2142.01	451
453	Грађевински инжењер	2142.02	451
454	Грађевински инжењер конструктор	2142.03	451
455	Грађевински инжењер конструкција мостоградње	2142.04	451
456	Грађевински инжењер статичар	2142.05	451
457	Грађевински инспектор	2142.06	451
458	Грађевински пројектант високоградње	2142.07	451
459	Грађевински пројектант нискоградње	2142.08	451
460	Грађевински пројектант путева	2142.09	451
461	Грађевински пројектант хидроенергетских објеката	2142.10	451
462	Грађевински пројектант хидротехничке инфраструктуре	2142.11	451
463	Инжењер високоградње	2142.12	451
464	Инжењер нискоградње	2142.13	451
465	Истраживач у области грађевинарства 	2142.14	451
466	Инжењери за заштиту животне средине	2143	443
467	Еколошки инжењер	2143.01	466
468	Инжењер за управљање загађењем ваздуха	2143.02	466
469	Инжењер заштите животне средине	2143.03	466
470	Процесни инжењер за отпадне воде	2143.04	466
471	Специјалиста за очување животне средине	2143.05	466
1319	Пастор	2636.15	1304
472	Стручњак за енергетску ефикасност	2143.06	466
473	Машински инжењери 	2144	443
474	Авио-инжењер за контролу исправности ваздухоплова 	2144.01	473
475	Инжењер аеро-наутике 	2144.02	473
476	Инжењер бродоградње 	2144.03	473
477	Инжењер за одржавање друмских возила 	2144.04	473
478	Инжењер контроле квалитета обраде метала	2144.05	473
479	Инжењер обраде метала	2144.06	473
480	Инжењер производње и одржавања алатних машина	2144.07	473
481	Инжењер производње и одржавања машина и уређаја прецизне технике	2144.08	473
482	Инжењер производње и одржавања механизације	2144.09	473
483	Инжењер производње и одржавања процесних машина и постројења	2144.10	473
484	Инжењер производње и одржавања термотехничких машина и постројења 	2144.11	473
485	Истраживач пројектовања саобраћајних средстава	2144.12	473
486	Истраживач у области машинства 	2144.13	473
487	Конструктор ваздухоплова	2144.14	473
488	Конструктор мотора са унутрашњим сагоревањем	2144.15	473
489	Конструктор процесних машина и постројења	2144.16	473
490	Конструктор термоенергетских постројења	2144.17	473
491	Конструктор уређаја хидраулике и пнеуматике	2144.18	473
492	Конструктор хидроенергетских постројења	2144.19	473
493	Машински инжењер 	2144.20	473
494	Машински инжењер аутомеханике	2144.21	473
495	Машински инжењер за нуклеарну енергију	2144.22	473
496	Пројектант за индустријску механизацију, лифтове и жичаре	2144.23	473
497	Пројектант машинског инжењеринга	2144.24	473
498	Пројектант металних конструкција 	2144.25	473
499	Пројектант термотехничких уређаја	2144.26	473
500	Пројектант хидроенергетских постројења	2144.27	473
501	Хемијски инжењери и прехрамбени технолози	2145	443
502	Гумарско-пластичарски технолог	2145.01	501
503	Истраживач у области прехрамбене технологије	2145.02	501
504	Истраживач у области хемијске технологије	2145.03	501
505	Петрохемијски технолог	2145.04	501
506	Пројектант хемијских процеса	2145.05	501
507	Технолог графичарства	2145.06	501
508	Технолог за прераду воде 	2145.07	501
509	Технолог за производњу боја и лакова 	2145.08	501
510	Технолог кондиторске производње	2145.09	501
511	Технолог прераде воћа и поврћа	2145.10	501
512	Технолог прераде дувана	2145.11	501
513	Технолог прераде жита 	2145.12	501
514	Технолог прераде меса и рибе	2145.13	501
515	Технолог прераде млека	2145.14	501
516	Технолог производње алкохола, квасца и киселина 	2145.15	501
517	Технолог производње готових јела	2145.16	501
518	Технолог производње и прераде анималних производа	2145.17	501
519	Технолог производње и прераде биљних производа	2145.18	501
520	Технолог производње концентроване сточне хране	2145.19	501
521	Технолог производње пића	2145.20	501
522	Технолог производње хлеба, пецива и тестенина	2145.21	501
523	Технолог текстилне конфекције 	2145.22	501
524	Хемијски аналитичар	2145.23	501
525	Хемијски технолог за неметале	2145.24	501
526	Хемијски технолог примене производа	2145.25	501
527	Инжењери рударства, металургије и сродни стручњаци	2146	443
528	Инжењер галванизације	2146.01	527
529	Инжењер обраде челичних производа	2146.02	527
530	Инжењер припреме и оплемењивања минералних сировина	2146.03	527
531	Инжењер производње каблова и проводника	2146.04	527
532	Истраживач у области рударства и металургије 	2146.05	527
533	Металуршки инжењер 	2146.06	527
534	Рударски инжењер	2146.07	527
535	Рударски инжењер површинске експлоатације	2146.08	527
536	Рударски инжењер подземне експлоатације	2146.09	527
537	Стручњаци техничких наука (осим електротехничких) неразврстани на другом месту	2149	443
538	Биомедицински инжењер 	2149.01	537
539	Енергетски ревизор	2149.02	537
540	Инжењер за експлозивна убојна средства 	2149.03	537
541	Инжењер за материјале 	2149.04	537
542	Инжењер за оптику 	2149.05	537
543	Инжењер безбедности и здравља на раду 	2149.06	537
544	Инжењер заштите од пожара	2149.07	537
545	Инжењер контроле квалитета	2149.08	537
546	Стручњаци електротехничких наука	215	303
547	Инжењери електротехнике и електроенергетике	2151	546
548	Електроинжењер	2151.01	547
549	Инжењер експлоатације електричних машина за уређаје и постројења	2151.02	547
550	Инжењер експлоатације опреме железничке вуче и возила 	2151.03	547
551	Инжењер електроенергетике 	2151.04	547
552	Инжењер електромеханике 	2151.05	547
553	Инжењер електроснабдевања 	2151.06	547
554	Инжењер производње електротехничких производа	2151.07	547
555	Истраживач у области електротехнике 	2151.08	547
556	Конструктор електричних производа	2151.09	547
557	Пројектант електричних апарата за домаћинство 	2151.10	547
558	Пројектант за електроенергетику	2151.11	547
559	Инжењери електронике	2152	546
560	Инжењер електроничар	2152.01	559
561	Инжењер електроничар за инструменте	2152.02	559
562	Инжењер електроничар за информациони инжењеринг	2152.03	559
563	Инжењер електроничар за полупроводнике	2152.04	559
564	Инжењер за рачунарски хардвер 	2152.05	559
565	Инжењер медицинске електронике	2152.06	559
566	Инжењер мехатронике	2152.07	559
567	Инжењер одржавања рачунарске опреме	2152.08	559
568	Инжењер производње рачунарске опреме	2152.09	559
569	Инжењери телекомуникација и ПТТ саобраћаја 	2153	546
570	Инжењер за емисиону технику 	2153.01	569
571	Инжењер за телекомуникационе мреже	2153.02	569
572	Инжењер ПТТ саобраћаја 	2153.03	569
573	Инжењер телекомуникација	2153.04	569
574	Инспектор за телекомуникације	2153.05	569
575	Истраживач у области телекомуникација	2153.06	569
576	Архитекте, урбанисти, геодети и дизајнери	216	303
577	Архитекте	2161	576
578	Архитекта 	2161.01	577
579	Архитекта за просторно планирање 	2161.02	577
580	Архитектонски пројектант	2161.03	577
581	Архитектонски пројектант ентеријера и дизајна	2161.04	577
582	Архитектонски технолог конзервације и ревитализације објекта	2161.05	577
583	Архитекте за пејзажну архитектуру	2162	576
584	Архитекта за пејзажну архитектуру 	2162.01	583
585	Инжењер за пејзажну архитектуру	2162.02	583
586	Дизајнери производа и одевних предмета 	2163	576
587	Дизајнер	2163.01	586
588	Дизајнер амбалаже 	2163.02	586
589	Дизајнер индустријских производа	2163.03	586
590	Дизајнер накита 	2163.04	586
591	Дизајнер обуће	2163.05	586
592	Дизајнер текстила	2163.06	586
593	Дизајнер техничких производа	2163.07	586
594	Костимограф	2163.08	586
595	Модни дизајнер	2163.09	586
596	Модни креатор	2163.10	586
597	Уметнички пројектант ентеријера и дизајна	2163.11	586
598	Урбанисти, саобраћајни инжењери и пројектанти 	2164	576
599	Инжењер друмског саобраћаја 	2164.01	598
600	Инжењер друмског саобраћаја за међународни транспорт и шпедицију 	2164.02	598
601	Инжењер железничког саобраћаја 	2164.03	598
602	Инспектор за друмски и јавни саобраћај	2164.04	598
603	Инспектор за железнички саобраћај	2164.05	598
604	Инспектор за јавне путеве	2164.06	598
605	Инспектор за просторно планирање	2164.07	598
606	Инспектор за унутрашњу пловидбу	2164.08	598
607	Истраживач у области саобраћаја 	2164.09	598
608	Истраживач у области урбанизма 	2164.10	598
609	Просторни планер	2164.11	598
610	Саобраћајни инжењер 	2164.12	598
611	Саобраћајни инспектор	2164.13	598
612	Саобраћајни планер	2164.14	598
613	Урбаниста	2164.15	598
614	Урбанистички инспектор	2164.16	598
615	Урбанистички планер	2164.17	598
616	Урбанистички пројектант	2164.18	598
617	Картографи и геодети 	2165	576
618	Геодета 	2165.01	617
619	Геодета примењене геодезије	2165.02	617
718	Акупунктуролог 	2230.01	717
620	Геодета специјалиста за аероснимање 	2165.03	617
621	Геодета специјалиста за хидрографију 	2165.04	617
622	Геодета у рударству 	2165.05	617
623	Геодетски аналитичар	2165.06	617
624	Инжењер геодезије	2165.07	617
625	Картограф	2165.08	617
626	Катастарски геодета	2165.09	617
627	Фото-граметар 	2165.10	617
628	Графички и мултимедијски дизајнери	2166	576
629	Веб-дизајнер	2166.01	628
630	Графички дизајнер	2166.02	628
631	Графички уредник часописа и новина 	2166.03	628
632	Дигитални уметник 	2166.04	628
633	Дизајнер звука	2166.05	628
634	Дизајнер компјутерских игара	2166.06	628
635	Дизајнер мултимедија 	2166.07	628
636	Дизајнер публикација 	2166.08	628
637	Илустратор књига	2166.09	628
638	Илустратор постера/паноа	2166.10	628
639	Ликовни графичар	2166.11	628
640	Здравствени стручњаци	22	302
641	Доктори медицине	221	640
642	Доктори опште медицине	2211	641
643	Лекар 	2211.01	642
644	Лекар у служби хитне медицинске помоћи	2211.02	642
645	Доктори медицине – специјалисти	2212	641
646	Акушер	2212.01	645
647	Алерголог	2212.02	645
648	Анестезиолог	2212.03	645
649	Вариколог	2212.04	645
650	Вирусолог	2212.05	645
651	Гастроентеролог	2212.06	645
652	Геронтолог	2212.07	645
653	Гинеколог	2212.08	645
654	Дерматовенеролог	2212.09	645
655	Дерматолог	2212.10	645
656	Дијабетолог	2212.11	645
657	Ендокринолог	2212.12	645
658	Епидемиолог	2212.13	645
659	Имунолог	2212.14	645
660	Интерниста	2212.15	645
661	Инфектолог	2212.16	645
662	Кардиоваскуларни хирург	2212.17	645
663	Кардиолог	2212.18	645
664	Клинички патолог	2212.19	645
665	Медицински бактериолог	2212.20	645
666	Медицински патолог	2212.21	645
667	Миколог	2212.22	645
668	Неонатолог	2212.23	645
669	Неуролог	2212.24	645
670	Неуропатолог	2212.25	645
671	Неуропедијатар	2212.26	645
672	Неуропсихијатар	2212.27	645
673	Неурохирург	2212.28	645
674	Нефролог	2212.29	645
675	Онколог	2212.30	645
676	Ортопед	2212.31	645
677	Оториноларинголог	2212.32	645
678	Офталмолог	2212.33	645
679	Паразитолог	2212.34	645
680	Патолог	2212.35	645
681	Патолог судске медицине / хистопатологије / неуропатологије / у хирургији	2212.36	645
682	Педијатар	2212.37	645
683	Перинатолог	2212.38	645
684	Пнеумолог	2212.39	645
685	Психијатар	2212.40	645
686	Пулмолог	2212.41	645
687	Радијациони онколог	2212.42	645
688	Радиолог	2212.43	645
689	Реуматолог	2212.44	645
690	Специјалиста медицине рада 	2212.45	645
691	Специјалиста нуклеарне медицине	2212.46	645
692	Специјалиста социјалне медицине	2212.47	645
693	Специјалиста спортске медицине	2212.48	645
694	Специјалиста ургентне медицине	2212.49	645
695	Трансфузиолог	2212.50	645
696	Трауматолог	2212.51	645
697	Уролог	2212.52	645
698	Физијатар	2212.53	645
699	Физиолог	2212.54	645
700	Физиолог ендокринолог	2212.55	645
701	Физиолог епидемиолог	2212.56	645
702	Физиолог неуролог	2212.57	645
703	Фонијатар	2212.58	645
704	Хематолог	2212.59	645
705	Хирург	2212.60	645
706	Хистопатолог	2212.61	645
707	Више медицинске и акушерске сестре 	222	640
708	Више медицинске сестре	2221	707
709	Виша анестезиолошка медицинска сестра	2221.01	708
710	Виша медицинска сестра	2221.02	708
711	Виша педијатријска медицинска сестра	2221.03	708
712	Виша психијатријска медицинска сестра	2221.04	708
713	Главна медицинска сестра	2221.05	708
714	Више акушерске сестре	2222	707
715	Виша гинеколошко-акушерска сестра 	2222.01	714
716	Стручњаци традиционалне и алтернативне медицине	223	640
717	Стручњаци традиционалне и алтернативне медицине	2230	716
720	Парамедицинско особље 	224	640
721	Парамедицинско особље	2240	720
722	Доктори ветеринарске медицине	225	640
723	Доктори ветеринарске медицине	2250	722
724	Ветеринар	2250.01	723
725	Ветеринар епидемиолог 	2250.02	723
726	Ветеринар патолог 	2250.03	723
727	Ветеринар репродукције и вештачког осемењивања	2250.04	723
728	Ветеринар специјалиста епизоотиологије заразних и паразитских болести	2250.05	723
729	Ветеринар хирург 	2250.06	723
730	Ветеринарски инспектор	2250.07	723
731	Истраживач у области ветеринарства 	2250.08	723
732	Остали здравствени стручњаци	226	640
733	Стоматолози	2261	732
734	Максилофацијални хирург	2261.01	733
735	Орални хирург	2261.02	733
736	Ортодонт	2261.03	733
737	Пародонтолог	2261.04	733
738	Педонтолог	2261.05	733
739	Специјалиста денталне и оралне патологије с пародонтологијом	2261.06	733
740	Специјалиста дечије и превентивне стоматологије	2261.07	733
741	Специјалиста ендодонције	2261.08	733
742	Специјалиста оралне хирургије 	2261.09	733
743	Стоматолог 	2261.10	733
744	Стоматолог протетичар	2261.11	733
745	Фармацеути 	2262	732
746	Апотекарски фармацеут 	2262.01	745
747	Индустријски фармацеут 	2262.02	745
748	Истраживач у области фармације	2262.03	745
749	Организатор клиничких испитивања  	2262.04	745
750	Радио-фармацеут	2262.05	745
751	Специјалиста контроле квалитета фармацеутских производа	2262.06	745
752	Фармацеут 	2262.07	745
753	Фармацеут козметолог	2262.08	745
754	Фармацеут снабдевања, промета и набавке лекова	2262.09	745
755	Фармацеут специјалиста за лековито биље	2262.10	745
756	Фармацеут специјалиста за фармацеутску технологију	2262.11	745
757	Фармацеут специјалиста клиничке фармакологије	2262.12	745
758	Фармацеут у болници 	2262.13	745
759	Стручњаци за здравље на раду 	2263	732
760	Стручњак за безбедност и здравље на раду 	2263.01	759
761	Стручњак за заштиту од радијације 	2263.02	759
762	Стручњак за санитарну безбедност и здравље на раду	2263.03	759
763	Физиотерапеути 	2264	732
764	Спортски физиотерапеут	2264.01	763
765	Физиотерапеут	2264.02	763
766	Физиотерапеут за терапију ласером	2264.03	763
767	Физиотерапеут специјалиста за геријатрију 	2264.04	763
768	Физиотерапеут специјалиста за ортопедију 	2264.05	763
769	Физиотерапеут специјалиста за педијатрију 	2264.06	763
770	Дијететичари-нутриционисти	2265	732
771	Медицински дијететичар 	2265.01	770
772	Нутрициониста дијететичар 	2265.02	770
773	Спортски нутрициониста 	2265.03	770
774	Аудиолози и логопеди	2266	732
775	Аудиолог 	2266.01	774
776	Логопед	2266.02	774
777	Ортофоничар	2266.03	774
778	Фонопед	2266.04	774
779	Оптометричари и сродни 	2267	732
780	Оптометричар	2267.01	779
781	Ортоптичар 	2267.02	779
782	Здравствени стручњаци неразврстани на другом месту 	2269	732
783	Хиропрактичар 	2269.01	782
784	Мртвозорник 	2269.02	782
785	Остеопат 	2269.03	782
786	Радни терапеут 	2269.04	782
787	Рекреативни терапеут	2269.05	782
788	Специјалиста за дерматокозметику	2269.06	782
789	Специјалиста за медицинску естетику	2269.07	782
790	Специјалиста за терапију бојом – хромотерапију	2269.08	782
791	Специјалиста за терапију музиком – музикотерапију	2269.09	782
792	Терапеут специјалиста за игру и покрет 	2269.10	782
793	Стручњаци за образовање и васпитање	23	302
794	Наставно особље на високошколским установама 	231	793
795	Наставници и сарадници 	2310	794
796	Наставник у области архитектуре 	2310.01	795
797	Наставник у области библиотекарства, архиварства и музеологије	2310.02	795
798	Наставник у области биолошких наука 	2310.03	795
799	Наставник у области биотехничких наука 	2310.04	795
1320	Патријарх	2636.16	1304
800	Наставник у области ветеринарских наука 	2310.05	795
801	Наставник у области геодетског инжењерства 	2310.06	795
802	Наставник у области геонаука 	2310.07	795
803	Наставник у области грађевинског инжењерства 	2310.08	795
804	Наставник у области драмских и аудио-визуелних уметности 	2310.09	795
805	Наставник у области економских наука 	2310.10	795
806	Наставник у области електротехничког и рачунарског инжењерства 	2310.11	795
807	Наставник у области индустријског инжењерства и инжењерског менаџмента 	2310.12	795
808	Наставник у области инжењерства заштите животне средине 	2310.13	795
809	Наставник у области историјских и археолошких наука 	2310.14	795
810	Наставник у области културолошке науке и комуникологије 	2310.15	795
811	Наставник у области ликовних уметности 	2310.16	795
812	Наставник у области математичких наука	2310.17	795
813	Наставник у области машинског инжењерства 	2310.18	795
814	Наставник у области медицинских наука 	2310.19	795
815	Наставник у области менаџмента и бизниса 	2310.20	795
816	Наставник у области металуршког инжењерства 	2310.21	795
817	Наставник у области музике и извођачких уметности 	2310.22	795
818	Наставник у области наука о заштити животне средине и заштите на раду 	2310.23	795
819	Наставник у области наука о уметностима 	2310.24	795
820	Наставник у области организационих наука 	2310.25	795
821	Наставник у области педагошких наука 	2310.26	795
822	Наставник у области политичких наука 	2310.27	795
823	Наставник у области правних наука 	2310.28	795
824	Наставник у области примењених уметности и дизајна 	2310.29	795
825	Наставник у области психолошких наука 	2310.30	795
826	Наставник у области рачунарских наука 	2310.31	795
827	Наставник у области рехабилитације 	2310.32	795
828	Наставник у области рударског инжењерства 	2310.33	795
829	Наставник у области саобраћајног инжењерства 	2310.34	795
830	Наставник у области социолошких наука 	2310.35	795
831	Наставник у области специјалне едукације и рехабилитације 	2310.36	795
832	Наставник у области стоматолошких наука 	2310.37	795
833	Наставник у области теологије 	2310.38	795
834	Наставник у области технолошког инжењерства 	2310.39	795
835	Наставник у области фармацеутских наука 	2310.40	795
836	Наставник у области физичких наука 	2310.41	795
837	Наставник у области физичког васпитања и спорта 	2310.42	795
838	Наставник у области физичко-хемијских наука 	2310.43	795
839	Наставник у области филозофије	2310.44	795
840	Наставник у области филолошких наука 	2310.45	795
841	Сарадник у области архитектуре 	2310.46	795
842	Сарадник у области библиотекарства, архиварства и музеологије	2310.47	795
843	Сарадник у области биолошких наука 	2310.48	795
844	Сарадник у области биотехничких наука 	2310.49	795
845	Сарадник у области ветеринарских наука 	2310.50	795
846	Сарадник у области геонаука 	2310.51	795
847	Сарадник у области геодетског инжењерства 	2310.52	795
848	Сарадник у области грађевинског инжењерства 	2310.53	795
849	Сарадник у области драмских и аудио-визуелних уметности 	2310.54	795
850	Сарадник у области економских наука 	2310.55	795
851	Сарадник у области електротехничког и рачунарског инжењерства 	2310.56	795
852	Сарадник у области индустријског инжењерства и инжењерског менаџмента 	2310.57	795
853	Сарадник у области инжењерства заштите животне средине 	2310.58	795
854	Сарадник у области историјских и археолошких наука 	2310.59	795
855	Сарадник у области културолошке науке и комуникологије 	2310.60	795
856	Сарадник у области ликовних уметности 	2310.61	795
857	Сарадник у области математичких наука	2310.62	795
858	Сарадник у области машинског инжењерства 	2310.63	795
859	Сарадник у области медицинских наука 	2310.64	795
860	Сарадник у области менаџмента и бизниса 	2310.65	795
861	Сарадник у области металуршког инжењерства 	2310.66	795
862	Сарадник у области музике и извођачких уметности 	2310.67	795
863	Сарадник у области наука о заштити животне средине и заштите на раду 	2310.68	795
864	Сарадник у области наука о уметностима 	2310.69	795
865	Сарадник у области организационих наука 	2310.70	795
866	Сарадник у области педагошких наука 	2310.71	795
867	Сарадник у области политичких наука 	2310.72	795
868	Сарадник у области правних наука 	2310.73	795
869	Сарадник у области примењених уметности и дизајна 	2310.74	795
870	Сарадник у области психолошких наука 	2310.75	795
871	Сарадник у области рачунарских наука 	2310.76	795
872	Сарадник у области рехабилитације 	2310.77	795
873	Сарадник у области рударског инжењерства 	2310.78	795
874	Сарадник у области саобраћајног инжењерства 	2310.79	795
875	Сарадник у области социолошких наука 	2310.80	795
876	Сарадник у области специјалне едукације и рехабилитације 	2310.81	795
877	Сарадник у области стоматолошких наука 	2310.82	795
878	Сарадник у области теологије 	2310.83	795
879	Сарадник у области технолошког инжењерства 	2310.84	795
880	Сарадник у области фармацеутских наука 	2310.85	795
881	Сарадник у области физичких наука 	2310.86	795
882	Сарадник у области физичког васпитања и спорта 	2310.87	795
883	Сарадник у области физичко-хемијских наука 	2310.88	795
884	Сарадник у области филозофије	2310.89	795
885	Сарадник у области филолошких наука 	2310.90	795
886	Наставници средњег стручног образовања	232	793
887	Наставници стручних предмета и практичне наставе у средњем стручном образовању	2320	886
888	Наставник архитектонске групе предмета	2320.01	887
889	Наставник астрономије	2320.02	887
890	Наставник биротехничке групе предмета	2320.03	887
891	Наставник бродограђевинске групе предмета	2320.04	887
892	Наставник ветеринарске групе предмета	2320.05	887
893	Наставник воћарске и виноградарске групе предмета 	2320.06	887
894	Наставник геодетске групе предмета	2320.07	887
895	Наставник геологије	2320.08	887
896	Наставник геофизике	2320.09	887
897	Наставник грађевинске групе предмета	2320.10	887
898	Наставник графичарско-технолошке групе предмета	2320.11	887
899	Наставник групе предмета безбедности и здравља на раду	2320.12	887
900	Наставник групе предмета друмског саобраћаја	2320.13	887
901	Наставник групе предмета за безбедност и унутрашње послове	2320.14	887
902	Наставник групе предмета кондиторства	2320.15	887
903	Наставник групе предмета кулинарства	2320.16	887
904	Наставник групе предмета основних и социјалних медицинских наука	2320.17	887
905	Наставник групе предмета пољопривредне механизације	2320.18	887
906	Наставник групе предмета противпожарне заштите	2320.19	887
907	Наставник групе предмета технологије дрвета	2320.20	887
908	Наставник екологије и заштите животне средине	2320.21	887
909	Наставник економске групе предмета	2320.22	887
910	Наставник електронске групе предмета	2320.23	887
911	Наставник електротехничке групе предмета	2320.24	887
912	Наставник кожарско-крзнарске технолошке групе предмета	2320.25	887
913	Наставник конфекцијско-технолошке групе предмета	2320.26	887
914	Наставник машинске групе предмета	2320.27	887
915	Наставник медицинско-биохемијске групе предмета	2320.28	887
916	Наставник месарско-прерађивачке групе предмета	2320.29	887
917	Наставник металуршке групе предмета	2320.30	887
918	Наставник механичке групе предмета	2320.31	887
919	Наставник млинарско-пекарске групе предмета	2320.32	887
920	Наставник обућарске и кожногалантеријске технолошке групе предмета	2320.33	887
921	Наставник педагогије	2320.34	887
922	Наставник правне групе предмета	2320.35	887
923	Наставник практичне наставе високоградње	2320.36	887
924	Наставник практичне наставе воћарства и виноградарства	2320.37	887
925	Наставник практичне наставе гинеколошко-опстетричке неге	2320.38	887
926	Наставник практичне наставе електроенергетике	2320.39	887
927	Наставник практичне наставе електромеханике	2320.40	887
928	Наставник практичне наставе електронике	2320.41	887
929	Наставник практичне наставе завршних грађевинских радова	2320.42	887
930	Наставник практичне наставе здравствене лабораторијске технике	2320.43	887
931	Наставник практичне наставе зубне технике – протетике	2320.44	887
932	Наставник практичне наставе класичног балета	2320.45	887
933	Наставник практичне наставе козметологије	2320.46	887
934	Наставник практичне наставе кулинарства	2320.47	887
935	Наставник практичне наставе машинске обраде метала	2320.48	887
936	Наставник практичне наставе монтаже машина	2320.49	887
937	Наставник практичне наставе нискоградње	2320.50	887
938	Наставник практичне наставе опште и специјалне неге болесника	2320.51	887
939	Наставник практичне наставе посластичарства	2320.52	887
940	Наставник практичне наставе прехрамбене технологије	2320.53	887
941	Наставник практичне наставе производње и одржавања рачунарске технике	2320.54	887
942	Наставник практичне наставе производње обуће и кожне галантерије	2320.55	887
943	Наставник практичне наставе ратарства и повртарства	2320.56	887
944	Наставник практичне наставе текстилне технологије	2320.57	887
945	Наставник практичне наставе угоститељског послуживања	2320.58	887
946	Наставник практичне наставе хемијске технологије	2320.59	887
947	Наставник ратарско-повртарске групе предмета	2320.60	887
948	Наставник рачунарске групе предмета	2320.61	887
949	Наставник рударске групе предмета	2320.62	887
950	Наставник стоматолошке групе предмета	2320.63	887
951	Наставник сточарске групе предмета 	2320.64	887
952	Наставник текстилно-технолошке групе предмета	2320.65	887
953	Наставник фармацеутске групе предмета	2320.66	887
954	Наставник хемијско-технолошке групе предмета	2320.67	887
955	Наставник шумарске групе предмета	2320.68	887
956	Наставници средњег општег и уметничког образовања 	233	793
957	Наставници средњег општег и уметничког образовања	2330	956
958	Наставник балета и плеса 	2330.01	957
959	Наставник биологије 	2330.02	957
960	Наставник бугарског језика 	2330.03	957
961	Наставник географије 	2330.04	957
962	Наставник енглеског језика 	2330.05	957
963	Наставник историје 	2330.06	957
964	Наставник историје књижевности 	2330.07	957
965	Наставник италијанског језика 	2330.08	957
966	Наставник јапанског језика 	2330.09	957
967	Наставник кинеског језика 	2330.10	957
968	Наставник културолошке групе предмета 	2330.11	957
969	Наставник латинског језика 	2330.12	957
970	Наставник ликовне групе предмета 	2330.13	957
971	Наставник ликовне културе 	2330.14	957
972	Наставник мађарског језика	2330.15	957
973	Наставник математике	2330.16	957
974	Наставник музичке групе предмета 	2330.17	957
975	Наставник музичке културе 	2330.18	957
976	Наставник немачког језика	2330.19	957
977	Наставник опште књижевности и теорије књижевности 	2330.20	957
978	Наставник психологије 	2330.21	957
979	Наставник рачунарства и информатике	2330.22	957
980	Наставник ромског језика 	2330.23	957
981	Наставник румунског језика 	2330.24	957
982	Наставник русинског језика 	2330.25	957
983	Наставник руског језика 	2330.26	957
984	Наставник словачког језика 	2330.27	957
985	Наставник социологије 	2330.28	957
986	Наставник српског језика 	2330.29	957
987	Наставник старословенског језика и књижевности	2330.30	957
988	Наставник физике	2330.31	957
989	Наставник физичке културе 	2330.32	957
990	Наставник филозофије и логике 	2330.33	957
991	Наставник филолошке групе предмета 	2330.34	957
992	Наставник француског језика 	2330.35	957
993	Наставник хемије 	2330.36	957
994	Наставник шпанског језика	2330.37	957
1321	Рабин 	2636.17	1304
995	Наставници у основном и предшколском образовању	234	793
996	Наставници у основном образовању	2341	995
997	Наставник предметне наставе у основном образовању 	2341.01	996
998	Наставник разредне наставе	2341.02	996
999	Васпитачи у предшколском васпитању и образовању	2342	995
1000	Васпитaч предшколске деце	2342.01	999
1001	Логопед-васпитач	2342.02	999
1002	Медицинска сестра-васпитач	2342.03	999
1003	Остали стручњаци за образовање 	235	793
1004	Андрагози, педагози и стручњаци за системска питања образовања и васпитања	2351	1003
1005	Андрагог	2351.01	1004
1006	Истраживач у области андрагогије 	2351.02	1004
1007	Истраживач у области педагогије	2351.03	1004
1008	Педагог	2351.04	1004
1009	Просветни инспектор	2351.05	1004
1010	Стручњак за вредновање у образовању и васпитању	2351.06	1004
1011	Стручњак за методе образовања 	2351.07	1004
1012	Стручњак за образовне политике	2351.08	1004
1013	Стручњак за развој наставног плана и програма (курикулума)	2351.09	1004
1014	Стручњаци и наставници за особе са сметњама у развоју	2352	1003
1015	Дефектолог	2352.01	1014
1016	Логопедагог 	2352.02	1014
1017	Наставник за особе са менталним поремећајем	2352.03	1014
1018	Наставник за особе са оштећењима вида	2352.04	1014
1019	Наставник за особе са оштећењима слуха	2352.05	1014
1020	Наставник за особе са поремећајем гласа и говора	2352.06	1014
1021	Наставник за особе са поремећајима у понашању	2352.07	1014
1022	Сурдопедагог 	2352.08	1014
1023	Тифлопедагог	2352.09	1014
1024	Наставници језика неразврстани на другом месту 	2353	1003
1025	Наставник у школи страних језика	2353.01	1024
1026	Наставници музике неразврстани на другом месту 	2354	1003
1027	Наставник у школи музике и певања	2354.01	1026
1028	Наставници уметности неразврстани на другом месту 	2355	1003
1029	Наставник у школи балета и плеса 	2355.01	1028
1030	Наставник у школи ликовне и примењене уметности	2355.02	1028
1031	Инструктори информационих технологија	2356	1003
1032	Наставник у школи рачунара	2356.01	1031
1033	Стручњаци за образовање и васпитање неразврстани на другом месту	2359	1003
1034	Васпитач у дому ученика	2359.01	1033
1035	Просветни саветник	2359.02	1033
1036	Саветник у спорту	2359.03	1033
1037	Специјални педагог 	2359.04	1033
1038	Стручњаци пословних услуга и администрације	24	302
1039	Финансијски стручњаци	241	1038
1040	Рачуноводствени стручњаци 	2411	1039
1041	Аналитичар платног промета	2411.01	1040
1042	Банкарски инспектор	2411.02	1040
1043	Буџетски инспектор	2411.03	1040
1044	Девизни инспектор	2411.04	1040
1045	Интерни ревизор 	2411.05	1040
1046	Рачуноводствени планер-аналитичар	2411.06	1040
1047	Рачуновођа 	2411.07	1040
1048	Рачуновођа за порезе 	2411.08	1040
1049	Рачунополагач	2411.09	1040
1050	Финансијски контролор 	2411.10	1040
1051	Финансијски ревизор 	2411.11	1040
1052	Финансијски и инвестициони саветници	2412	1039
1053	Порески саветник 	2412.01	1052
1054	Саветник за инвестиције	2412.02	1052
1055	Саветник за клијенте у банкарству	2412.03	1052
1056	Стручњак за инвестиције и развој	2412.04	1052
1057	Стручњак за кастоди послове	2412.05	1052
1058	Финансијски и инвестициони саветник	2412.06	1052
1059	Финансијски саветник	2412.07	1052
1060	Финансијски аналитичари и сродни 	2413	1039
1061	Аналитичар за инвестиције 	2413.01	1060
1062	Аналитичар за обвезнице 	2413.02	1060
1063	Аналитичар финансијског пословања 	2413.03	1060
1064	Аналитичар ценовне и комерцијалне политике 	2413.04	1060
1065	Консултант за хартије од вредности 	2413.05	1060
1066	Контролор у банкарству 	2413.06	1060
1067	Менаџер за ризик пословања са корпоративним клијентима	2413.07	1060
1068	Менаџер за ризик пословања са малим и средњим преузећима и предузетницима	2413.08	1060
1069	Менаџер за ризик пословања са становништвом	2413.09	1060
1141	Дизајнер ИТ система 	2511.03	1138
1070	Менаџер за унапређење ефикасности управљања ризицима	2413.10	1060
1071	Специјалиста за документацију у банкарству	2413.11	1060
1072	Специјалиста за електронско банкарство	2413.12	1060
1073	Специјалиста за наплату спорних потраживања у банкарству	2413.13	1060
1074	Специјалиста за платне картице	2413.14	1060
1075	Специјалиста за реструктурирање и подршку клијентима 	2413.15	1060
1076	Специјалиста за управљање ризицима у банкарству	2413.16	1060
1077	Специјалиста за усклађеност банкарског пословања	2413.17	1060
1078	Специјалиста за факторинг	2413.18	1060
1079	Стручњак за извештавање, анализу и моделирање у банкарству	2413.19	1060
1080	Стручњак за кредитни портфолио	2413.20	1060
1081	Стручњак за наплату потраживања од правних лица 	2413.21	1060
1082	Стручњак за наплату потраживања од физичких лица	2413.22	1060
1083	Стручњак за супервизију средстава обезбеђења	2413.23	1060
1084	Стручњак за управљање кредитним ризиком 	2413.24	1060
1085	Стручњак за управљање тржишним ризиком 	2413.25	1060
1086	Стручњаци административног пословања	242	1038
1087	Аналитичари организације и управљања	2421	1086
1088	Аналитичар за методе и организацију рада	2421.01	1087
1089	Аналитичар пословних процеса	2421.02	1087
1090	Консултант за управљање пословним процесима	2421.03	1087
1091	Организатор протокола	2421.04	1087
1092	Пословни консултант	2421.05	1087
1093	Специјалиста за стандарде и техничке нормативе	2421.06	1087
1094	Стручњак за ефикасност пословања 	2421.07	1087
1095	Стручњак за организацију рада	2421.08	1087
1096	Стручњак за управљање квалитетом	2421.09	1087
1097	Стручњаци за јавну политику 	2422	1086
1098	Аналитичар јавних политика 	2422.01	1097
1099	Дипломата	2422.02	1097
1100	Саветник за јавне политике 	2422.03	1097
1101	Стручњаци за кадрове и каријерни развој 	2423	1086
1102	Аналитичар занимања	2423.01	1101
1103	Аналитичар политике запошљавања	2423.02	1101
1104	Каријерни саветник 	2423.03	1101
1105	Организатор кадровских процеса	2423.04	1101
1106	Саветник за запошљавање 	2423.05	1101
1107	Специјалиста за кадрове 	2423.06	1101
1108	Стручњак за каријерно вођење и саветовање	2423.07	1101
1109	Стручњак за развој људских ресурса 	2423.08	1101
1110	Стручњак за селекцију кадрова 	2423.09	1101
1111	Стручњаци за обуку и професионални развој 	2424	1086
1112	Организатор образовања одраслих	2424.01	1111
1113	Стручњак за обуку и професионални развој кадрова	2424.02	1111
1114	Стручњак за развој радне снаге 	2424.03	1111
1115	Стручњаци за продају, маркетинг и односе с јавношћу	243	1038
1116	Стручњаци за маркетинг и оглашавање	2431	1115
1117	Аналитичар тржишта 	2431.01	1116
1118	Бренд менаџер	2431.02	1116
1119	Менаџер категорије производа	2431.03	1116
1120	Менаџер производа 	2431.04	1116
1121	Стручњак за маркетинг 	2431.05	1116
1122	Стручњак за оглашавање	2431.06	1116
1123	Стручњаци за односе с јавношћу	2432	1115
1124	Менаџер за односе с јавношћу 	2432.01	1123
1125	Менаџер масовних медија	2432.02	1123
1126	Писац рекламних текстова 	2432.03	1123
1127	Рекламни агент 	2432.04	1123
1128	Стручњак за односе с јавношћу	2432.05	1123
1129	Стручњаци за продају техничких и медицинских производа (осим ИКТ)	2433	1115
1130	Заступник продаје индустријских производа 	2433.01	1129
1131	Заступник продаје медицинских и фармацеутских производа 	2433.02	1129
1132	Заступник продаје техничких производа	2433.03	1129
1133	Стручњаци за продају информационо-комуникационе технологије (ИКТ)	2434	1115
1134	Заступник продаје информационо-комуникационих технологија 	2434.01	1133
1135	Заступник продаје рачунара и рачунарске опреме	2434.02	1133
1136	Стручњаци за информационо-комуникационе технологије (ИКТ) 	25	302
1137	Аналитичари и стручњаци за развој софтвера и апликација 	251	1136
1138	Систем-аналитичари 	2511	1137
1139	Аналитичар информационог система 	2511.01	1138
1140	Аналитичар пословног система (ИТ)	2511.02	1138
1142	Консултант за ИКТ системе	2511.04	1138
1143	Систем-инжењер (ИКТ)	2511.05	1138
1144	Систем-интегратор (ИКТ)	2511.06	1138
1145	Стручњаци за развој софтвера 	2512	1137
1146	Пројектант инфраструктуре за ИКТ	2512.01	1145
1147	Програмер	2512.02	1145
1148	Програмер аналитичар 	2512.03	1145
1149	Програмер кодер 	2512.04	1145
1150	Пројектант информационог система	2512.05	1145
1151	Пројектант софтвера	2512.06	1145
1152	Стручњак за архитектуру информационог система	2512.07	1145
1153	Стручњак за развој софтвера 	2512.08	1145
1154	Стручњаци за развој интернетских апликација и мултимедијалних садржаја	2513	1137
1155	Дизајнер корисничког интерфејса	2513.01	1154
1156	Организатор учења на даљину	2513.02	1154
1157	Програмер анимација 	2513.03	1154
1158	Програмер компјутерских игара 	2513.04	1154
1159	Програмер мултимедија 	2513.05	1154
1160	Пројектант веб-сајта	2513.06	1154
1161	Стручњак за архитектуру веб-сајта 	2513.07	1154
1162	Стручњак за развој веб-сајта 	2513.08	1154
1163	Стручњак за развој интернета 	2513.09	1154
1164	Програмери апликација	2514	1137
1165	Инжењер за инфраструктуру рачунарства у облаку (Cloud Computing Engineer )	2514.01	1164
1166	Програмер апликација	2514.02	1164
1167	Програмер апликација у Андроид окружењу 	2514.03	1164
1168	Програмер апликација у IOS окружењу 	2514.04	1164
1169	Стручњаци за развој софтвера и апликација и аналитичари неразврстани на другом месту	2519	1137
1170	Аналитичар за осигурање квалитета у ИКТ	2519.01	1169
1171	Истраживач у области информационих наука	2519.02	1169
1172	Стручњак за електронско банкарство	2519.03	1169
1173	Стручњак за тестирање система (ИКТ)	2519.04	1169
1174	Стручњак за тестирање софтвера	2519.05	1169
1175	Стручњаци за базе података и мреже	252	1136
1176	Дизајнери и администратори база података 	2521	1175
1177	Администратор базе података	2521.01	1176
1178	Администратор података 	2521.02	1176
1179	Аналитичар базе података 	2521.03	1176
1180	Пројектант базе података	2521.04	1176
1181	Стручњак за архитектуру базе података 	2521.05	1176
1182	Систем-администратори	2522	1175
1183	Администратор мрежа (ИКТ)	2522.01	1182
1184	Систем-администратор (ИКТ)	2522.02	1182
1185	Стручњаци за рачунарске мреже	2523	1175
1186	Аналитичар мрежа (ИКТ)	2523.01	1185
1187	Аналитичар рачунарских комуникација 	2523.02	1185
1188	Инжењер мрежа и телекомуникација 	2523.03	1185
1189	Стручњак за развој рачунарске мреже	2523.04	1185
1190	Стручњаци за базе података и мреже неразврстани на другом месту	2529	1175
1191	Библиотечки информатичар	2529.01	1190
1192	Информатичар	2529.02	1190
1193	Кибернетичар	2529.03	1190
1194	Ревизор за информационо-комуникационе технологије	2529.04	1190
1195	Специјалиста за подршку у раду са апликацијама 	2529.05	1190
1196	Специјалиста за сигурносне системе (ИКТ)	2529.06	1190
1197	Стручњак за безбедност информација (ИТ)	2529.07	1190
1198	Стручњак за дигиталну форензику 	2529.08	1190
1199	Стручњаци за право, друштвене науке и културу	26	302
1200	Правници	261	1199
1201	Заступници	2611	1200
1202	Адвокат	2611.01	1201
1203	Правни заступник 	2611.02	1201
1204	Заменик јавног тужиоца	2611.03	1201
1205	Заменик правобраниоца	2611.04	1201
1206	Судије	2612	1200
1207	Судија Врховног касационог суда	2612.01	1206
1208	Судија Уставног суда 	2612.02	1206
1209	Судија суда опште надлежности	2612.03	1206
1210	Судија суда посебне надлежности	2612.04	1206
1211	Правници неразврстани на другом месту	2619	1200
1212	Арбитар	2619.01	1211
1213	Јавни бележник / нотар 	2619.02	1211
1214	Јавни извршитељ	2619.03	1211
1215	Јавнобележнички помоћник 	2619.04	1211
1216	Јавнобележнички сарадник	2619.05	1211
1217	Посредник	2619.06	1211
1218	Правни аналитичар 	2619.07	1211
1219	Правни саветник 	2619.08	1211
1220	Правник 	2619.09	1211
1221	Правобранилачки помоћник	2619.10	1211
1318	Надбискуп 	2636.14	1304
1222	Саветник за мирно решавање спорова 	2619.11	1211
1223	Судијски помоћник	2619.12	1211
1224	Судски вештак	2619.13	1211
1225	Тужилачки помоћник	2619.14	1211
1226	Библиотекари, архивисти и кустоси 	262	1199
1227	Архивисти и кустоси	2621	1226
1228	Археограф 	2621.01	1227
1229	Архивар специјалиста	2621.02	1227
1230	Библиотечко-архивски конзерватор	2621.03	1227
1231	Историчар уметности	2621.04	1227
1232	Конзерватор-рестауратор културних добара	2621.05	1227
1233	Конзерватор културних добара 	2621.06	1227
1234	Конзерватор одеће	2621.07	1227
1235	Кустос галерије 	2621.08	1227
1236	Кустос музеја	2621.09	1227
1237	Музејски аранжер-декоратер 	2621.10	1227
1238	Музејски архивиста	2621.11	1227
1239	Музејски педагог	2621.12	1227
1240	Нумизматичар	2621.13	1227
1241	Рестауратор културних добара 	2621.14	1227
1242	Филателист	2621.15	1227
1243	Библиотекари, документалисти и сродни	2622	1226
1244	Библиограф 	2622.01	1243
1245	Библиотекар	2622.02	1243
1246	Библиотекар каталогизатор	2622.03	1243
1247	Библиотекар класификатор	2622.04	1243
1248	Библиотекар конзерватор	2622.05	1243
1249	Библиотекар рестауратор	2622.06	1243
1250	Библиотечки инструктор 	2622.07	1243
1251	Документалиста	2622.08	1243
1252	Документалиста у спорту	2622.09	1243
1253	Стручњаци друштвених наука, верски стручњаци и великодостојници	263	1199
1254	Стручњаци економије	2631	1253
1255	Агроекономиста	2631.01	1254
1256	Економетричар 	2631.02	1254
1257	Економиста 	2631.03	1254
1258	Економиста рада 	2631.04	1254
1259	Економски аналитичар 	2631.05	1254
1260	Економски саветник 	2631.06	1254
1261	Истраживач у области економије	2631.07	1254
1262	Саветник за економска питања	2631.08	1254
1263	Стручњак за послове унутрашње трговине	2631.09	1254
1264	Стручњак за спољнотрговинско пословање 	2631.10	1254
1265	Социолози, археолози, антрополози, географи и сродни стручњаци	2632	1253
1266	Антрополог	2632.01	1265
1267	Археолог	2632.02	1265
1268	Географ 	2632.03	1265
1269	Етногеограф	2632.04	1265
1270	Етнограф	2632.05	1265
1271	Етнолог	2632.06	1265
1272	Истраживач у области географије 	2632.07	1265
1273	Истраживач у области социологије 	2632.08	1265
1274	Криминолог	2632.09	1265
1275	Пенолог 	2632.10	1265
1276	Социјални патолог	2632.11	1265
1277	Социолог	2632.12	1265
1278	Туризмолог	2632.13	1265
1279	Филозофи, историчари и политиколози	2633	1253
1280	Генеалог 	2633.01	1279
1281	Историчар	2633.02	1279
1282	Политиколог	2633.03	1279
1283	Филозоф	2633.04	1279
1284	Психолози	2634	1253
1285	Индустријски психолог 	2634.01	1284
1286	Истраживач у области психологије 	2634.02	1284
1287	Клинички психолог	2634.03	1284
1288	Психолог 	2634.04	1284
1289	Психометричар	2634.05	1284
1290	Психотерапеут	2634.06	1284
1291	Социјални психолог	2634.07	1284
1292	Спортски психолог 	2634.08	1284
1293	Школски психолог	2634.09	1284
1294	Стручњаци за социјални рад и саветовање	2635	1253
1295	Водитељ случаја 	2635.01	1294
1296	Саветник за болести зависности 	2635.02	1294
1297	Саветник за брак 	2635.03	1294
1298	Саветник за децу и омладину 	2635.04	1294
1299	Саветник за жртве породичног насиља	2635.05	1294
1300	Саветник за жртве сексуалног насиља	2635.06	1294
1301	Саветник за породицу 	2635.07	1294
1302	Социјални радник 	2635.08	1294
1303	Супервизор за социјални рад 	2635.09	1294
1304	Верски стручњаци, великодостојници и теолози	2636	1253
1305	Архиђакон 	2636.01	1304
1306	Архиепископ 	2636.02	1304
1307	Бискуп 	2636.03	1304
1308	Владика	2636.04	1304
1309	Ђакон 	2636.05	1304
1310	Епископ	2636.06	1304
1311	Жупник	2636.07	1304
1312	Игуман	2636.08	1304
1313	Имам 	2636.09	1304
1314	Јерођакон	2636.10	1304
1315	Јеромонах	2636.11	1304
1316	Митрополит	2636.12	1304
1317	Муфтија 	2636.13	1304
1322	Свештеник 	2636.18	1304
1323	Теолог 	2636.19	1304
1324	Хоџа	2636.20	1304
1325	Књижевници, новинари и лингвисти	264	1199
1326	Књижевници и сродни	2641	1325
1327	Биограф	2641.01	1326
1328	Драматург	2641.02	1326
1329	Есејиста 	2641.03	1326
1330	Књижевни критичар 	2641.04	1326
1331	Књижевник 	2641.05	1326
1332	Критичар ликовних уметности	2641.06	1326
1333	Критичар музичких уметности	2641.07	1326
1334	Критичар позоришних уметности	2641.08	1326
1335	Песник	2641.09	1326
1336	Писац говора 	2641.10	1326
1337	Писац за интерактивне медије 	2641.11	1326
1338	Романописац 	2641.12	1326
1339	Сатиричар	2641.13	1326
1340	Сценариста 	2641.14	1326
1341	Филмски критичар	2641.15	1326
1342	Хроничар	2641.16	1326
1343	Новинари и уредници	2642	1325
1344	Аналитичар масовних медија	2642.01	1343
1345	Главни и одговорни уредник новина/часописа/радија/ТВ	2642.02	1343
1346	Енигмата	2642.03	1343
1347	Издавач	2642.04	1343
1348	Коментатор 	2642.05	1343
1349	Координатор деска 	2642.06	1343
1350	Музички уредник 	2642.07	1343
1351	Новинар	2642.08	1343
1352	Новинар дописник 	2642.09	1343
1353	Новинарски репортер	2642.10	1343
1354	Продуцент вести (радио/телевизија) 	2642.11	1343
1355	Редактор	2642.12	1343
1356	Спортски новинар 	2642.13	1343
1357	Уредник књижевних издања	2642.14	1343
1358	Уредник комуникација на друштвеним мрежама	2642.15	1343
1359	Уредник новина 	2642.16	1343
1360	Уредник редакције 	2642.17	1343
1361	Уредник рубрике	2642.18	1343
1362	Уредник ТВ програма 	2642.19	1343
1363	Уредник у издавачкој делатности	2642.20	1343
1364	Преводиоци, тумачи и остали лингвисти	2643	1325
1365	Графолог	2643.01	1364
1366	Етимолог	2643.02	1364
1367	Инокореспондент	2643.03	1364
1368	Криптолог	2643.04	1364
1369	Лексикограф	2643.05	1364
1370	Лексиколог	2643.06	1364
1371	Лектор	2643.07	1364
1372	Морфолог	2643.08	1364
1373	Преводилац	2643.09	1364
1374	Семантичар/семасиолог	2643.10	1364
1375	Симултани/консекутивни преводилац	2643.11	1364
1376	Судски тумач	2643.12	1364
1377	Тумач знаковног језика	2643.13	1364
1378	Филолог 	2643.14	1364
1379	Фонолог	2643.15	1364
1380	Уметници – ствараоци и извођачи	265	1199
1381	Визуелни уметници 	2651	1380
1382	Вајар 	2651.01	1381
1383	Вајар примењених уметности 	2651.02	1381
1384	Иконописац	2651.03	1381
1385	Илустратор 	2651.04	1381
1386	Истраживач у области ликовних уметности 	2651.05	1381
1387	Карикатуриста	2651.06	1381
1388	Колорист 	2651.07	1381
1389	Ликовни уметник	2651.08	1381
1390	Ликовни уредник 	2651.09	1381
1391	Рестауратор слика 	2651.10	1381
1392	Сликар	2651.11	1381
1393	Сликар примењених уметности	2651.12	1381
1394	Уметник на керамици 	2651.13	1381
1395	Фрескописац 	2651.14	1381
1396	Цртач анимираних филмова	2651.15	1381
1397	Музичари инструменталисти, певачи и композитори	2652	1380
1398	Бубњар	2652.01	1397
1399	Вибрафониста	2652.02	1397
1400	Виолиниста	2652.03	1397
1401	Виолиста	2652.04	1397
1402	Виолончелиста	2652.05	1397
1403	Вокални солиста 	2652.06	1397
1404	Гитариста	2652.07	1397
1405	Диригент оркестра	2652.08	1397
1406	Диригент хора	2652.09	1397
1407	Етномузиколог	2652.10	1397
1408	Кларинетиста	2652.11	1397
1409	Композитор 	2652.12	1397
1410	Контрабасиста	2652.13	1397
1411	Концерт мајстор 	2652.14	1397
1412	Музиколог	2652.15	1397
1413	Музичар инструменталиста	2652.16	1397
1414	Музичар у ноћном клубу 	2652.17	1397
1415	Музички аранжер	2652.18	1397
1416	Музички корепетитор	2652.19	1397
1417	Музички репетитор 	2652.20	1397
1418	Обоиста	2652.21	1397
1419	Оперски певач	2652.22	1397
1420	Органиста	2652.23	1397
1421	Оргуљаш	2652.24	1397
1422	Певач у ноћном клубу 	2652.25	1397
1423	Пијаниста	2652.26	1397
1424	Позауниста	2652.27	1397
1425	Првак опере	2652.28	1397
1426	Саксофониста	2652.29	1397
1427	Тимпаниста	2652.30	1397
1428	Тромбониста	2652.31	1397
1429	Тромпетиста	2652.32	1397
1430	Трубач	2652.33	1397
1431	Тубиста	2652.34	1397
1432	Фаготиста	2652.35	1397
1433	Флаутиста	2652.36	1397
1434	Фрулаш	2652.37	1397
1435	Харфиста	2652.38	1397
1436	Хорниста	2652.39	1397
1437	Хорски певач 	2652.40	1397
1438	Челиста	2652.41	1397
1439	Чембалиста	2652.42	1397
1440	Плесачи и кореографи	2653	1380
1441	Балетски играч	2653.01	1440
1442	Балетски кореограф	2653.02	1440
1443	Балетски корепетитор 	2653.03	1440
1444	Балетски репетитор 	2653.04	1440
1445	Играч модерних игара	2653.05	1440
1446	Кореограф 	2653.06	1440
1447	Плесни асистент	2653.07	1440
1448	Помоћник кореографа 	2653.08	1440
1449	Првак балета	2653.09	1440
1450	Фолклорни играч 	2653.10	1440
1451	Фолклорни кореограф	2653.11	1440
1452	Филмски и позоришни режисери, продуценти и сродни	2654	1380
1453	Асистент режије 	2654.01	1452
1454	Водитељ књиге снимања	2654.02	1452
1455	Дизајнер светла 	2654.03	1452
1456	Директор документарног програма 	2654.04	1452
1457	Директор сцене 	2654.05	1452
1458	Директор филма 	2654.06	1452
1459	Директор фотографије 	2654.07	1452
1460	Мајстор луткар 	2654.08	1452
1461	Мајстор сцене 	2654.09	1452
1462	Музички продуцент	2654.10	1452
1463	Организатор програма/емисије	2654.11	1452
1464	Позоришни продуцент 	2654.12	1452
1465	Позоришни редитељ 	2654.13	1452
1466	Помоћник сценографа	2654.14	1452
1467	Продуцент аудио-видео издања	2654.15	1452
1468	Продуцент радија/телевизије	2654.16	1452
1469	Реализатор програма радија и телевизије	2654.17	1452
1470	Редитељ опере 	2654.18	1452
1471	Редитељ радио-емисија	2654.19	1452
1472	Редитељ фотографије	2654.20	1452
1473	Сценограф	2654.21	1452
1474	Сценограф луткар	2654.22	1452
1475	Технички директор за радио/телевизију 	2654.23	1452
1476	Филмски и телевизијски монтажер	2654.24	1452
1477	Филмски и телевизијски редитељ	2654.25	1452
1478	Филмски продуцент 	2654.26	1452
1479	Глумци	2655	1380
1480	Глумац	2655.01	1479
1481	Глумац имитатор 	2655.02	1479
1482	Глумац комичар	2655.03	1479
1483	Глумац приповедач/наратор 	2655.04	1479
1484	Спикери на радију, телевизији и другим медијима	2656	1380
1485	Водитељ естрадног програма 	2656.01	1484
1486	Водитељ јавних манифестација	2656.02	1484
1487	Водитељ радио и ТВ емисија	2656.03	1484
1488	Најављивач програма / конферансије	2656.04	1484
1489	Репортер за временску прогнозу	2656.05	1484
1490	Спикер	2656.06	1484
1491	Спикер на радију 	2656.07	1484
1492	Спикер на телевизији 	2656.08	1484
1493	Спортски репортер 	2656.09	1484
1494	Уметници и извођачи неразврстани на другом месту	2659	1380
1495	Акробата	2659.01	1494
1496	Артиста на трапезу/жици	2659.02	1494
1497	Вентрилоквиста/трбухозборац	2659.03	1494
1498	Вођа/члан музичке групе	2659.04	1494
1499	Диск џокеј 	2659.05	1494
1500	Естрадни имитатор	2659.06	1494
1501	Естрадни музичар	2659.07	1494
1502	Жонглер	2659.08	1494
1503	Играч у ноћном клубу	2659.09	1494
1504	Илузиониста	2659.10	1494
1505	Капелник	2659.11	1494
1506	Кловн 	2659.12	1494
1507	Луткар 	2659.13	1494
1508	Мађионичар 	2659.14	1494
1509	Пантомимичар	2659.15	1494
1510	Пародичар	2659.16	1494
1511	Стенд ап комичар 	2659.17	1494
1512	Степ играч	2659.18	1494
1513	Улични певач	2659.19	1494
1514	Улични свирач	2659.20	1494
1515	Хипнотизер	2659.21	1494
1516	Циркуски дресер дивљих животиња	2659.22	1494
1517	 ТЕХНИЧАРИ И САРАДНИЦИ СТРУЧЊАКА 	3	\N
1518	Сарадници и техничари у области природних и техничких наука	31	1517
1519	Сарадници и техничари у области физике, хемије и техничких наука	311	1518
1520	Сарадници и техничари у области физике и хемије 	3111	1519
1521	Астрономски техничар	3111.01	1520
1522	Геолошки техничар 	3111.02	1520
1523	Геофизички техничар	3111.03	1520
1524	Метеоролошки техничар	3111.04	1520
1525	Оператер геотехничких и хидрогеолошких истраживања	3111.05	1520
1526	Прехрамбени физичко-хемијски лаборант	3111.06	1520
1527	Физички техничар	3111.07	1520
1528	Хемијски техничар 	3111.08	1520
1529	Хемијско-технолошки техничар	3111.09	1520
1530	Сарадници и техничари у грађевинарству, архитектури, урбанизму и геодезији	3112	1519
1531	Архитектонски техничар	3112.01	1530
1532	Геодетски техничар	3112.02	1530
1533	Геометар	3112.03	1530
1534	Грађевински калкулант	3112.04	1530
1535	Грађевински техничар 	3112.05	1530
1536	Грађевински техничар за механику тла	3112.06	1530
1537	Грађевински техничар за прорачуне и планове 	3112.07	1530
1538	Техничар високоградње	3112.08	1530
1539	Техничар грађевинских основних радова	3112.09	1530
1540	Техничар за просторно планирање	3112.10	1530
1541	Техничар нискоградње	3112.11	1530
1542	Техничар хидроградње	3112.12	1530
1543	Урбанистички техничар	3112.13	1530
1544	Фотограметријски техничар	3112.14	1530
1545	Сарадници и техничари у електротехници	3113	1519
1546	Електроенергетичар	3113.01	1545
1547	Електроенергетски аеродромски техничар експлоатације и одржавања 	3113.02	1545
1548	Електроенергетски техничар експлоатације и одржавања	3113.03	1545
1549	Електромонтерски оператер инсталације и опреме	3113.04	1545
1550	Електромонтерски техничар електричних мрежа 	3113.05	1545
1551	Електромонтерски техничар специјалиста електричних постројења	3113.06	1545
1552	Електротехничар 	3113.07	1545
1553	Електротехничар за енергетску расвету 	3113.08	1545
1554	Електротехничар конструисања електричних делова	3113.09	1545
1555	Електротехничар одржавања апарата за домаћинство	3113.10	1545
1556	Електротехничар одржавања мерних инструмената и склопова	3113.11	1545
1557	Електротехничар одржавања опреме грађевинских, индустријских и лучких дизалица	3113.12	1545
1558	Електротехничар одржавања опреме друмских возила и покретних машина	3113.13	1545
1559	Електротехничар одржавања опреме железничке вуче и возила	3113.14	1545
1560	Електротехничар одржавања опреме лифтова, жичара и елеватора	3113.15	1545
1561	Електротехничар одржавања опреме пловила	3113.16	1545
1562	Електротехничар одржавања опреме производних машина и погона	3113.17	1545
1563	Електротехничар производње машина и опреме	3113.18	1545
1564	Електротехничар производње уређаја за домаћинство	3113.19	1545
1565	Оператер експлоатације и одржавања железничке електроенергетске опреме	3113.20	1545
1566	Техничар преноса електричне енергије 	3113.21	1545
1567	Сарадници и техничари у електроници 	3114	1519
1568	Оператер одржавања електронских сигнално-сигурносних уређаја	3114.01	1567
1569	Оператер одржавања фоно-уређаја и видео студија	3114.02	1567
1570	Оперативни конструктор енергетско-електронских уређаја	3114.03	1567
1571	Техничар електронике 	3114.04	1567
1572	Техничар електронике за мерне инструменте и склопове 	3114.05	1567
1573	Техничар електронике за полупроводнике	3114.06	1567
1574	Техничар електронике за прорачуне 	3114.07	1567
1575	Техничар електронике за рачунарски хардвер 	3114.08	1567
1576	Техничар мехатронике	3114.09	1567
1577	Техничар одржавања рачунарске опреме	3114.10	1567
1578	Сарадници и техничари у машинству	3115	1519
1579	Машински техничар 	3115.01	1578
1580	Машински техничар бродоградње	3115.02	1578
1581	Машински техничар за одржавање ваздухоплова 	3115.03	1578
1582	Планер обраде метала 	3115.04	1578
1583	Техничар аеронаутике 	3115.05	1578
1584	Техничар градње пловила	3115.06	1578
1585	Техничар за гасне инсталације 	3115.07	1578
1586	Техничар заваривања	3115.08	1578
1587	Техничар конструисања алата и прибора	3115.09	1578
1588	Техничар конструисања машинских елемената	3115.10	1578
1589	Техничар конструисања металних конструкција	3115.11	1578
1590	Техничар конструктор металних производа широке потрошње	3115.12	1578
1591	Техничар контроле квалитета машинских материјала 	3115.13	1578
1592	Техничар лабораторијског испитивања и мерењa металних материјала	3115.14	1578
1593	Техничар обраде метала деформисањем и одвајањем	3115.15	1578
1594	Техничар обраде метала резањем	3115.16	1578
1595	Техничар пољопривредне механизације	3115.17	1578
1596	Техничар производње и одржавања алатних машина	3115.18	1578
1597	Техничар производње и одржавања енергетских машина и постројења	3115.19	1578
1598	Техничар производње и одржавања машина и уређаја прецизне технике	3115.20	1578
1599	Техничар производње и одржавања машинске механизације	3115.21	1578
1600	Техничар производње и одржавања расхладних уређаја	3115.22	1578
1601	Техничар производње и одржавања термотехничких машина и постројења	3115.23	1578
1602	Техничар производње и одржавања уређаја хидраулике и пнеуматике	3115.24	1578
1603	Сарадници и техничари хемијских технологија (осим процесних техничара)	3116	1519
1604	Графички лаборант	3116.01	1603
1605	Графички техничар	3116.02	1603
1606	Гумарски техничар	3116.03	1603
1607	Дрвноиндустријски техничар	3116.04	1603
1608	Кожарски техничар 	3116.05	1603
1609	Лаборант за испитивање грађевинског материјала	3116.06	1603
1610	Лаборант у прехрамбеној производњи 	3116.07	1603
1611	Прехрамбени техничар 	3116.08	1603
1612	Сладар	3116.09	1603
1613	Текстилски техничар	3116.10	1603
1614	Техничар производње грађевинског материјала	3116.11	1603
1615	Хемијскo-технолошки лаборант	3116.12	1603
1616	Хемијски лаборант 	3116.13	1603
1617	Сарадници и техничари у рударству и металургији	3117	1519
1618	Металуршки лаборант	3117.01	1617
1619	Металуршки техничар 	3117.02	1617
1620	Металуршки техничар аналитичар	3117.03	1617
1621	Металуршки техничар експлоатације	3117.04	1617
1622	Металуршки техничар за одливке	3117.05	1617
1623	Металуршки техничар за радиоактивне минерале	3117.06	1617
1624	Рударски диспечер	3117.07	1617
1625	Рударски инспектор 	3117.08	1617
1626	Рударски техничар 	3117.09	1617
1627	Рударски техничар површинске експлоатације 	3117.10	1617
1628	Рударски техничар подземне експлоатације 	3117.11	1617
1629	Рударски техничар припреме и оплемењивања минералних сировина	3117.12	1617
1630	Техничар истраживања минералних сировина	3117.13	1617
1631	Техничар постројења за истраживање и производњу нафте и земног гаса	3117.14	1617
1632	Техничар производње племенитих метала	3117.15	1617
1633	Технички цртачи 	3118	1519
1634	Електротехнички цртач	3118.01	1633
1635	Картографски цртач 	3118.02	1633
1636	Машински технички цртач	3118.03	1633
1637	Технички илустратор 	3118.04	1633
1638	Технички цртач	3118.05	1633
1639	Технички цртач у грађевинарству 	3118.06	1633
1640	Сарадници и техничари у копненом саобраћају и техничким наукама неразврстани на другом месту	3119	1519
1641	Ватрогасни инспектор 	3119.01	1640
1642	Ватрогасни истражитељ	3119.02	1640
1643	Инспектор заштите од пожара 	3119.03	1640
1644	Лаборант форензичар 	3119.04	1640
1645	Специјалиста за превенцију од пожара 	3119.05	1640
1646	Техничар друмског саобраћаја 	3119.06	1640
1647	Техничар железничког саобраћаја 	3119.07	1640
1648	Техничар за индустријске пројекте	3119.08	1640
1649	Техничар безбедности и здравља на раду	3119.09	1640
1650	Техничар роботике	3119.10	1640
1651	Техничар утрошка материјала 	3119.11	1640
1652	Надзорници у рударству, прерађивачкој индустрији и грађевинарству	312	1518
1653	Надзорници рудника 	3121	1652
1654	Надзорник у каменолому 	3121.01	1653
1655	Надзорник у руднику	3121.02	1653
1656	Рударски надзорник површинске експлоатације 	3121.03	1653
1657	Рударски надзорник подземне експлоатације	3121.04	1653
1658	Рударски надзорник прераде минералне сировине 	3121.05	1653
1659	Надзорници производње у прерађивачкој индустрији 	3122	1652
1660	Надзорник за фину обраду производа 	3122.01	1659
1661	Надзорник производне линије 	3122.02	1659
1662	Грађевински надзорници 	3123	1652
1663	Надзорник за грађевинску механизацију	3123.01	1662
1664	Надзорник инсталатерских и завршних грађевинских радова	3123.02	1662
1665	Надзорник у високоградњи 	3123.03	1662
1666	Надзорник у грађевинарству 	3123.04	1662
1667	Надзорник у нискоградњи 	3123.05	1662
1668	Техничари контроле процеса –  процесни техничари 	313	1518
1669	Оператери постројења за производњу енергије 	3131	1668
1670	Диспечер електроенергетске станице 	3131.01	1669
1671	Електроенергетски оператер	3131.02	1669
1672	Електроенергетски оператер експлоатације и одржавања	3131.03	1669
1673	Машиниста термоцентрале 	3131.04	1669
1674	Оператер агрегата 	3131.05	1669
1675	Оператер генераторске станице 	3131.06	1669
1676	Оператер за трафо-станице	3131.07	1669
1677	Оператер постројења за дистрибуцију енергије 	3131.08	1669
1678	Оператер постројења за соларну енергију 	3131.09	1669
1679	Оператер производње електричне енергије из природног гаса	3131.10	1669
1680	Оператер станице за производњу енергије 	3131.11	1669
1681	Оператер термоенергетских постројења 	3131.12	1669
1682	Оператер турбине за производњу електричне енергије 	3131.13	1669
1683	Оператер хидроенергетских постројења 	3131.14	1669
1684	Погонски енергетичар	3131.15	1669
1685	Турбомашиниста 	3131.16	1669
1686	Оператери пећи за спаљивање отпада и оператери прераде воде	3132	1668
1687	Машиниста рени бунара	3132.01	1686
1688	Машиниста хидрауличних постројења	3132.02	1686
1689	Оператер базена за пречишћавање воде	3132.03	1686
1690	Оператер канализационог постројења	3132.04	1686
1691	Оператер пећи за спаљивање отпада	3132.05	1686
1692	Оператер прераде отпадних вода	3132.06	1686
1693	Оператер резервоара за воду	3132.07	1686
1694	Оператер у хидростаници 	3132.08	1686
1695	Оператер уређаја за пречишћавање отпадних вода	3132.09	1686
1696	Оператер уређаја за пречишћавање воде	3132.10	1686
1697	Оператер уређаја за филтрирање воде	3132.11	1686
1698	Оператер хидрофора 	3132.12	1686
1699	Оператер црпне станице 	3132.13	1686
1700	Контролори хемијских постројења и процеса	3133	1668
1701	Контролор завршних производа у наменској индустрији	3133.01	1700
1702	Контролор операције делова у наменској индустрији  	3133.02	1700
1703	Контролор процеса полимеризације	3133.03	1700
1704	Контролор процеса производње жестоких пића	3133.04	1700
1705	Контролор процеса производње минералних вода	3133.05	1700
1706	Контролор процеса стерилизације у хемијској производњи 	3133.06	1700
1707	Контролор процеса у производњи кокса	3133.07	1700
1708	Контролор у производњи грађевинског материјала	3133.08	1700
1709	Контролор хемијских процеса	3133.09	1700
1710	Оператер за хемијско филтрирање и одвајање 	3133.10	1700
1711	Оператер обраде радиоактивног отпада	3133.11	1700
1712	Оператер постројења за третман хемијском топлотом 	3133.12	1700
1713	Оператер производње безалкохолних пића	3133.13	1700
1714	Оператер производње вина	3133.14	1700
1715	Оператер производње и прераде адитива, зачина, чаја, кафе и кавовина	3133.15	1700
1716	Оператер у цементари 	3133.16	1700
1717	Техничар кондиторске производње	3133.17	1700
1718	Техничар млевења житарица	3133.18	1700
1719	Техничар прераде воћа и поврћа 	3133.19	1700
1720	Техничар прераде дувана	3133.20	1700
1721	Техничар прераде меса и рибе	3133.21	1700
1722	Техничар прераде млека	3133.22	1700
1723	Техничар производње и прераде скроба	3133.23	1700
1724	Техничар производње пива	3133.24	1700
1725	Техничар производње уља и биљних масти	3133.25	1700
1726	Техничар производње хлеба, пецива и тестенина	3133.26	1700
1727	Техничар производње шећера	3133.27	1700
1728	Оператери постројења за рафинисање нафте и природног гаса 	3134	1668
1729	Дестилатер нафте и природног гаса	3134.01	1728
1730	Оператер блендера за прераду нафте и природног гаса 	3134.02	1728
1731	Оператер гасних постројења 	3134.03	1728
1732	Оператер парафинских постројења 	3134.04	1728
1733	Оператер петролеумских постројења 	3134.05	1728
1734	Петрохемијски оператер	3134.06	1728
1735	Процесни техничар у рафинерији нафте	3134.07	1728
1736	Контролори металуршких процеса	3135	1668
1737	Оператер станице за централну контролу металуршких процеса 	3135.01	1736
1738	Оператер високих пећи	3135.02	1736
1739	Техничар галванизације	3135.03	1736
1740	Техничар производње каблова и проводника	3135.04	1736
1741	Техничар производње одливака	3135.05	1736
1742	Техничар производње челика	3135.06	1736
1743	Техничар термичке обраде метала	3135.07	1736
1744	Контролори процеса неразврстани на другом месту	3139	1668
1745	Контролор индустријских робота 	3139.01	1744
1746	Металофарбарски контролор	3139.02	1744
1747	Оператер за производњу целулозе и папира 	3139.03	1744
1748	Оператер на линијама за аутоматску производњу 	3139.04	1744
1749	Техничар дрвопрерађивачке технологије	3139.05	1744
1750	Техничар машинске роботике и флексибилне аутоматике	3139.06	1744
1751	Сарадници и техничари у биолошким наукама и сродна занимања (осим медицинских)	314	1518
1752	Сарадници и техничари у биолошким наукама (осим медицинских)	3141	1751
1753	Бактериолошки техничар 	3141.01	1752
1754	Биолошки техничар	3141.02	1752
1755	Биофизички техничар	3141.03	1752
1756	Биохемијски техничар 	3141.04	1752
1757	Ботанички техничар	3141.05	1752
1758	Генетички техничар	3141.06	1752
1759	Еколошки техничар 	3141.07	1752
1760	Лаборант биологије	3141.08	1752
1761	Прехрамбени микробиолошки лаборант	3141.09	1752
1762	Техничар за културу ткива 	3141.10	1752
1763	Техничар за микробиологију	3141.11	1752
1764	Техничар зоологије 	3141.12	1752
1765	Фармаколошки техничар 	3141.13	1752
1766	Сарадници и техничари у пољопривреди	3142	1751
1767	Агрономски техничар 	3142.01	1766
1768	Воћарско-виноградарски лаборант	3142.02	1766
1769	Техничар за воћарство-виноградарство	3142.03	1766
1770	Техничар за живинарство	3142.04	1766
1771	Техничар за ратарство-повртарство	3142.05	1766
1772	Техничар за цвећарство-вртларство	3142.06	1766
1773	Техничар заштите биља	3142.07	1766
1774	Техничар млекарства 	3142.08	1766
1775	Техничар пољопривредних мелиорација	3142.09	1766
1776	Техничар сточарства	3142.10	1766
1777	Техничар хортикултуре	3142.11	1766
1778	Сарадници и техничари у шумарству 	3143	1751
1779	Техничар арборикултуре 	3143.01	1778
1780	Техничар озелењавања насеља и уређења предела	3143.02	1778
1781	Техничар узгоја шума	3143.03	1778
1782	Чувар шума	3143.04	1778
1783	Шумарски техничар 	3143.05	1778
1784	Контролори и техничари бродског и авио-саобраћаја	315	1518
1785	Бродски техничари	3151	1784
1786	Бродомашински техничар	3151.01	1785
1787	Бродски механичар моториста	3151.02	1785
1788	Машиниста пловидбе	3151.04	1785
1789	Управник унутрашње пловидбе	3151.05	1785
1790	Бродски официри палубе и навигатори	3152	1784
1791	Заповедник брода	3152.01	1790
1792	Заповедник брода унутрашње пловидбе	3152.02	1790
1793	Капетан брода 	3152.03	1790
1794	Капетан бродске преводнице	3152.04	1790
1795	Капетан јахте 	3152.05	1790
1796	Кормилар 	3152.06	1790
1797	Лучки капетан	3152.07	1790
1798	Лучки техничар	3152.08	1790
1799	Наутичар	3152.09	1790
1800	Наутичар унутрашње пловидбе	3152.10	1790
1801	Наутички техничар	3152.11	1790
1802	Официр бродске машине	3152.12	1790
1803	Официр бродске палубе	3152.13	1790
1804	Пилот глисера	3152.14	1790
1805	Подофицир бродске машине	3152.15	1790
1806	Подофицир бродске палубе	3152.16	1790
1807	Пилоти авиона и сродна занимања 	3153	1784
1808	Инжењер лета 	3153.01	1807
1889	Зубни техничар лаборант	3251.03	1886
1809	Инспектор летачке оперативе и летачког особља	3153.02	1807
1810	Инструктор летења 	3153.03	1807
1811	Капетан авиона	3153.04	1807
1812	Навигатор летења 	3153.05	1807
1813	Организатор летачке оперативе 	3153.06	1807
1814	Пилот авиона	3153.07	1807
1815	Пилот привредне авијације 	3153.08	1807
1816	Пилот хеликоптера	3153.09	1807
1817	Пробни пилот	3153.10	1807
1818	Контролори летења	3154	1784
1819	Контролор ваздушног саобраћаја 	3154.01	1818
1820	Техничари електронике за безбедност авио-саобраћаја	3155	1784
1821	Оператер опреме за скенирање пртљага	3155.01	1820
1822	Техничар безбедности авио-саобраћаја 	3155.02	1820
1823	Медицинске сестре и здравствени техничари	32	1517
1824	Медицински и фармацеутски техничари 	321	1823
1825	Техничари за терапеутску опрему и медицинска снимања 	3211	1824
1826	Рендгенски техничар 	3211.01	1825
1827	Терапеут за медицинска зрачења	3211.02	1825
1828	Техничар за магнетну резонанцу 	3211.03	1825
1829	Техничар нуклеарне медицине 	3211.04	1825
1830	Техничар радиографије	3211.05	1825
1831	Техничар сонографије 	3211.06	1825
1832	Техничари медицинске лабораторије	3212	1824
1833	Биохемијско-здравствени аналитичар	3212.01	1832
1834	Биохемијско-здравствени лаборант	3212.02	1832
1835	Лабораторијски техничар за патологију	3212.03	1832
1836	Медицинско-лабораторијски техничар	3212.04	1832
1837	Микробиолошки лаборант	3212.05	1832
1838	Техничар за банку крви 	3212.06	1832
1839	Техничар серологије	3212.07	1832
1840	Физиолошки техничар	3212.08	1832
1841	Хематолошки техничар 	3212.09	1832
1842	Цитолошки техничар 	3212.10	1832
1843	Фармацеутски техничари 	3213	1824
1844	Техничар фармацеутске производње	3213.01	1843
1845	Фармацеутски лаборант	3213.02	1843
1846	Фармацеутски техничар 	3213.03	1843
1847	Фармацеутски техничар обраде података о лековима	3213.04	1843
1848	Фармацеутски техничар промета робе	3213.05	1843
1849	Медицински и зубни протетичари	3214	1824
1850	Зубни техничар 	3214.01	1849
1851	Зубни техничар специјалиста фиксне протетике	3214.02	1849
1852	Медицински протетичар 	3214.03	1849
1853	Оператер израде ортопедских помагала	3214.04	1849
1854	Ортодонтист 	3214.05	1849
1855	Протетичар за горње екстремитете 	3214.06	1849
1856	Медицинске сестре и бабице	322	1823
1857	Медицинске сестре	3221	1856
1858	Дерматолошка сестра	3221.01	1857
1859	Медицинска сестра	3221.02	1857
1860	Медицинска сестра за кућну помоћ и негу	3221.03	1857
1861	Медицинска сестра инструментарка 	3221.04	1857
1862	Медицинска сестра примарне здравствене заштите	3221.05	1857
1863	Медицинска сестра стационарне здравствене заштите	3221.06	1857
1864	Медицинска сестра у кардиоваскуларно-пулмолошкој дијагностици	3221.07	1857
1865	Медицинска сестра у трансфузиологији	3221.08	1857
1866	Медицинско-педагошка сестра	3221.09	1857
1867	Педијатријска медицинска сестра	3221.10	1857
1868	Психијатријска медицинска сестра	3221.11	1857
1869	Бабице	3222	1856
1870	Акушерска медицинска сестра 	3222.01	1869
1871	Техничари традиционалне и алтернативне медицине	323	1823
1872	Техничари традиционалне и алтернативне медицине	3230	1871
1873	Биоенергетичар	3230.01	1872
1874	Видар/исцелитељ	3230.02	1872
1875	Јога терапеут	3230.03	1872
1876	Натуропат 	3230.04	1872
1877	Техничар за акунпуктуру 	3230.05	1872
1878	Техничар специјалиста хомеопатије 	3230.06	1872
1879	Травар 	3230.07	1872
1880	Ветеринарски техничари и помоћници	324	1823
1881	Ветеринарски техничари и помоћници	3240	1880
1882	Ветеринарски техничар	3240.01	1881
1883	Ветеринарски техничар за вакцинацију 	3240.02	1881
1884	Ветеринарски техничар за вештачко осемењивање 	3240.03	1881
1885	Остали здравствени техничари	325	1823
1886	Стоматолошке сестре – техничари	3251	1885
1887	Зубарски техничар профилактичар	3251.01	1886
1888	Зубни терапеут 	3251.02	1886
1890	Зубни хигијеничар 	3251.04	1886
1891	Стоматолошка сестра	3251.05	1886
1892	Медицински техничари за здравствене информације и картотеку	3252	1885
1893	Медицински картотекар 	3252.01	1892
1894	Патронажне сестре – техничари 	3253	1885
1895	Здравствени медијатор	3253.01	1894
1896	Патронажна бабица	3253.02	1894
1897	Патронажна сестра – техничар 	3253.03	1894
1898	Оптичари 	3254	1885
1899	Оператер за оптичке елементе	3254.01	1898
1900	Оптичар	3254.02	1898
1901	Оптичар за контактна сочива 	3254.03	1898
1902	Техничар обраде оптичких елемената	3254.04	1898
1903	Физиотерапеутски техничари и сарадници 	3255	1885
1904	Балнеотерапеут	3255.01	1903
1905	Електротерапеут 	3255.02	1903
1906	Масер	3255.03	1903
1907	Спортски масер 	3255.04	1903
1908	Техничар акупресуре 	3255.05	1903
1909	Техничар физикалне рехабилитације	3255.06	1903
1910	Физиотерапеутска сестра – техничар 	3255.07	1903
1911	Хидротерапеут 	3255.08	1903
1912	Медицински помоћници	3256	1885
1913	Инспектори и сарадници за заштиту животне средине и здравље на раду 	3257	1885
1914	Инспектор за безбедност и здравље на раду	3257.01	1913
1915	Инспектор за безбедност производа 	3257.02	1913
1916	Инспектор за безбедност хране 	3257.03	1913
1917	Инспектор за контролу загађења животне средине 	3257.04	1913
1918	Санитарни инспектор	3257.05	1913
1919	Санитарни техничар	3257.06	1913
1920	Техничар за заштиту животне средине	3257.07	1913
1921	Техничари у хитној помоћи и сродни радници 	3258	1885
1922	Медицинска сестра – техничар у хитној помоћи 	3258.01	1921
1923	Здравствени техничари и сарадници неразврстани на другом месту	3259	1885
1924	Анестезиолошки техничар 	3259.01	1923
1925	Козметички техничар	3259.02	1923
1926	Медицински гипсар	3259.03	1923
1927	Оператер аудиометријске опреме	3259.04	1923
1928	Оператер електроенцефалографске опреме	3259.05	1923
1929	Оператер електрокардиографске опреме 	3259.06	1923
1930	Техничар за респираторну терапију 	3259.07	1923
1931	Сарадници пословних услуга и администрације	33	1517
1932	Сарадници у области финансија и математике 	331	1931
1933	Финансијски дилери и брокери 	3311	1932
1934	Брокер за акције 	3311.01	1933
1935	Брокер за стране валуте 	3311.02	1933
1936	Брокер за хартије од вредности 	3311.03	1933
1937	Дилер за стране валуте 	3311.04	1933
1938	Референт кастоди послова	3311.05	1933
1939	Финансијски референт	3311.06	1933
1940	Сарадници за кредитне послове и зајмове	3312	1932
1941	Кредитни службеник	3312.01	1940
1942	Референт штедње и кредитирања	3312.02	1940
1943	Службеник за хипотекарне кредите 	3312.03	1940
1944	Сарадници рачуноводства и књиговодства	3313	1932
1945	Књиговодствени референт	3313.01	1944
1946	Књиговођа	3313.02	1944
1947	Књиговођа аналитичар	3313.03	1944
1948	Књиговођа билансиста	3313.04	1944
1949	Књиговођа главне књиге	3313.05	1944
1950	Књиговођа контиста	3313.06	1944
1951	Књиговођа ликвидатор	3313.07	1944
1952	Контиста	3313.08	1944
1953	Обрачунски референт	3313.09	1944
1954	Преузимач ризика осигурања	3313.10	1944
1955	Рачуноводствени референт	3313.11	1944
1956	Сарадници у статистици, математици и актуарству	3314	1932
1957	Проценитељи вредности и проценитељи штете	3315	1932
1958	Контролор потраживања	3315.01	1957
1959	Проценитељ вредности 	3315.02	1957
1960	Проценитељ вредности некретнина 	3315.03	1957
1961	Проценитељ осигурања 	3315.04	1957
1962	Проценитељ потраживања 	3315.05	1957
1963	Проценитељ штете	3315.06	1957
1964	Референт потраживања	3315.07	1957
1965	Посредници у трговини и осигурању	332	1931
1966	Заступници осигурања 	3321	1965
1967	Агент осигурања	3321.01	1966
1968	Комерцијални и трговински заступници	3322	1965
1969	Агент комерцијалне продаје	3322.01	1968
1970	Комерцијалиста	3322.02	1968
1971	Референт продаје	3322.03	1968
3166	Књиговезац	7323.03	3163
1972	Саветник за услуге сервиса након продаје 	3322.04	1968
1973	Трговински заступник 	3322.05	1968
1974	Трговински представник	3322.06	1968
1975	Набављачи	3323	1965
1976	Економ	3323.01	1975
1977	Набављач на велико/мало	3323.02	1975
1978	Референт набавке 	3323.03	1975
1979	Референт снадбевања 	3323.04	1975
1980	Снабдевач робом на велико/мало	3323.05	1975
1981	Трговински посредници 	3324	1965
1982	Посредник за трговину робом 	3324.01	1981
1983	Посредник за шпедитерску трговину 	3324.02	1981
1984	Агенти пословних услуга 	333	1931
1985	Шпедитери и агенти за клиринг 	3331	1984
1986	Агент за клиринг 	3331.01	1985
1987	Агент за шпедицију	3331.02	1985
1988	Царински агент	3331.03	1985
1989	Организатори конференција и догађаја	3332	1984
1990	Организатор венчања 	3332.01	1989
1991	Организатор догађаја (event planner)	3332.02	1989
1992	Организатор конференција 	3332.03	1989
1993	Посредници у запошљавању 	3333	1984
1994	Посредник у запошљавању 	3333.01	1993
1995	Агенти за некретнине 	3334	1984
1996	Агент за некретнине 	3334.01	1995
1997	Агенти пословних услуга неразврстани на другом месту	3339	1984
1998	Агент за естрадне уметнике 	3339.01	1997
1999	Агент за патенте и стандарде 	3339.02	1997
2000	Агент продаје пропагандног простора – реклама 	3339.03	1997
2001	Водитељ аукције 	3339.04	1997
2002	Позоришни посредник/агент 	3339.05	1997
2003	Спортски агент 	3339.06	1997
2004	Административни и специјализовани секретари	334	1931
2005	Шефови административних послова 	3341	2004
2006	Контролор уноса података 	3341.01	2005
2007	Шеф административног особља 	3341.02	2005
2008	Шеф службеника за унос и обраду података 	3341.04	2005
2009	Секретари за правне послове	3342	2004
2010	Управни техничар	3342.01	2009
2011	Административни и извршни секретари 	3343	2004
2012	Административни секретар 	3343.01	2011
2013	Асистент за кореспонденцију 	3343.02	2011
2014	Записничар 	3343.03	2011
2015	Пословни секретар	3343.04	2011
2016	Пројектни асистент 	3343.05	2011
2017	Технички секретар 	3343.06	2011
2018	Секретари за медицинске послове	3344	2004
2019	Царински, порески инспектори и сродни референти 	335	1931
2020	Цариници и погранични инспектори 	3351	2019
2021	Погранични инспектор 	3351.01	2020
2022	Референт за контролу пасоша 	3351.02	2020
2023	Службеник за имиграциона питања	3351.03	2020
2024	Цариник	3351.04	2020
2025	Царински инспектор 	3351.05	2020
2026	Царински референт	3351.06	2020
2027	Службеници за порезе и акцизе 	3352	2019
2028	Порески извршитељ	3352.01	2027
2029	Порески инспектор 	3352.02	2027
2030	Порески контролор	3352.03	2027
2031	Порески службеник 	3352.04	2027
2032	Референт за акцизе 	3352.05	2027
2033	Референти социјалног осигурања	3353	2019
2034	Референт за социјално осигурање 	3353.01	2033
2035	Референти за издавање дозвола и исправа	3354	2019
2036	Катастарски референт 	3354.01	2035
2037	Матичар 	3354.02	2035
2038	Референт за издавање грађевинских дозвола 	3354.03	2035
2039	Референт за издавање лиценци 	3354.04	2035
2040	Референт за издавање пасоша 	3354.05	2035
2041	Референт за издавање пословних дозвола 	3354.06	2035
2042	Референт за патенте и стандарде	3354.07	2035
2043	Референт за регистрацију возила	3354.08	2035
2044	Полицијски инспектори и детективи 	3355	2019
2045	Детектив 	3355.01	2044
2046	Иследник у полицији	3355.02	2044
2047	Криминалистички инспектор 	3355.03	2044
2048	Полицијски инспектор 	3355.04	2044
2049	Полицијски истражитељ	3355.05	2044
2050	Царински, порески и сродни службеници неразврстани на другом месту 	3359	2019
2051	Дипломатски курир	3359.01	2050
2052	Инспектор за контролу зарада	3359.02	2050
2053	Инспектор за контролу тежине и мере 	3359.03	2050
2054	Инспектор за контролу цена 	3359.04	2050
2055	Инспектор за рибарство 	3359.05	2050
2056	Инспектор за шумарство 	3359.06	2050
2057	Комунални инспектор	3359.07	2050
2058	Ловни инспектор	3359.08	2050
2059	Отправник послова у амбасади 	3359.09	2050
2060	Пољопривредни инспектор 	3359.10	2050
2061	Референт за евиденцију војних обвезника 	3359.11	2050
2062	Референт за таксе	3359.12	2050
2063	Тржишни инспектор	3359.13	2050
2064	Трговински контролор	3359.14	2050
2065	Туристички инспектор	3359.15	2050
2066	Сарадници у области права, социјалног рада, спорта, културе и вера	34	1517
2067	Сарадници у области права, социјалног рада и вера	341	2066
2068	Сарадници у области права и сродни	3411	2067
2069	Помоћник јавног извршитеља	3411.01	2068
2070	Референт за нормативно-правне послове	3411.02	2068
2071	Референт за пренос власништва 	3411.03	2068
2072	Судски референт 	3411.04	2068
2073	Сарадници у области социјалног рада	3412	2067
2074	Инструктор за животне вештине	3412.01	2073
2075	Координатор сигурне женске куће 	3412.02	2073
2076	Омладински радник 	3412.03	2073
2077	Персонални асистент за особе са инвалидитетом	3412.04	2073
2078	Богослови и сродни 	3413	2067
2079	Богослов	3413.01	2078
2080	Калуђер	3413.02	2078
2081	Монах 	3413.03	2078
2082	Проповедник 	3413.04	2078
2083	Редовник	3413.05	2078
2084	Фратар	3413.06	2078
2085	Часна сестра	3413.07	2078
2086	Спортски и фитнес радници	342	2066
2087	Спортисти	3421	2086
2088	Атлетичар 	3421.01	2087
2089	Бициклиста	3421.02	2087
2090	Боксер 	3421.03	2087
2091	Ваздухопловни спортиста	3421.04	2087
2092	Ватерполиста 	3421.05	2087
2093	Веслач 	3421.06	2087
2094	Гимнастичар	3421.07	2087
2095	Кајакаш-кануиста	3421.08	2087
2096	Каратиста	3421.09	2087
2097	Кик-боксер	3421.10	2087
2098	Кошаркаш 	3421.11	2087
2099	Куглаш	3421.12	2087
2100	Одбојкаш 	3421.13	2087
2101	Планинар	3421.14	2087
2102	Пливач 	3421.15	2087
2103	Професионални спортиста	3421.16	2087
2104	Рвач	3421.17	2087
2105	Рукометаш 	3421.18	2087
2106	Стонотенисер	3421.19	2087
2107	Стрелац	3421.20	2087
2108	Теквондиста	3421.21	2087
2109	Тенисер 	3421.22	2087
2110	Фудбалер	3421.23	2087
2111	Џудиста	3421.24	2087
2112	Шахиста 	3421.25	2087
2113	Спортски тренери, инструктори, судије и сродни 	3422	2086
2114	Инструктор у спорту	3422.01	2113
2115	Организатор спортског пословања	3422.02	2113
2116	Сарадник новинара у спорту	3422.03	2113
2117	Спортски посредник	3422.04	2113
2118	Спортски пропагандиста	3422.05	2113
2119	Судија у спорту	3422.06	2113
2120	Тренер за атлетику 	3422.07	2113
2121	Тренер за ватерполо 	3422.08	2113
2122	Тренер за веслање 	3422.09	2113
2123	Тренер за кајак/кану	3422.10	2113
2124	Тренер за кошарку 	3422.11	2113
2125	Тренер за одбојку 	3422.12	2113
2126	Тренер за пливање 	3422.13	2113
2127	Тренер за рвање	3422.14	2113
2128	Тренер за рукомет 	3422.15	2113
2129	Тренер за стрељаштво	3422.16	2113
2130	Тренер за теквондо	3422.17	2113
2131	Тренер за тенис 	3422.18	2113
2132	Тренер за фудбал 	3422.19	2113
2133	Тренер у спорту	3422.21	2113
2134	Инструктори фитнеса и рекреације и водитељи спортских активности	3423	2086
2135	Алпинистички спортски водич	3423.01	2134
2136	Инструктор аеробика 	3423.02	2134
2137	Инструктор алпинизма	3423.03	2134
2138	Инструктор јахања 	3423.04	2134
2139	Инструктор једрења 	3423.05	2134
2140	Инструктор једриличарства	3423.06	2134
2141	Инструктор јоге	3423.07	2134
2142	Инструктор клизања	3423.08	2134
2143	Инструктор лова	3423.09	2134
2144	Инструктор маунтибајкинга	3423.10	2134
2145	Инструктор оријентиринга	3423.11	2134
2146	Инструктор падобранства	3423.12	2134
2147	Инструктор параглајдинга	3423.13	2134
2148	Инструктор плеса	3423.14	2134
2149	Инструктор пливања 	3423.15	2134
2150	Инструктор рафтинга	3423.16	2134
2151	Инструктор рекреације	3423.17	2134
2152	Инструктор риболова	3423.18	2134
2153	Инструктор роњења 	3423.19	2134
2154	Инструктор скијања 	3423.20	2134
2155	Инструктор слободног падања	3423.21	2134
2156	Инструктор спелеологије	3423.22	2134
2157	Организатор рекреације у спорту	3423.23	2134
2158	Планинарски спортски водич	3423.24	2134
2159	Рекреативни спортски водич 	3423.25	2134
2160	Спортски спасилац	3423.26	2134
2161	Тренер рекреативаца	3423.27	2134
2162	Фитнес инструктор 	3423.28	2134
2163	Сарадници у области уметности, културе и кулинарства	343	2066
2164	Фотографи	3431	2163
2165	Аеро-фотограф 	3431.01	2164
2166	Индустријски фотограф 	3431.02	2164
2167	Комерцијални фотограф 	3431.03	2164
2168	Микрофотограф	3431.04	2164
2169	Репрофотограф	3431.05	2164
2170	Фотограф 	3431.06	2164
2171	Фотограф за научне часописе и истраживања 	3431.07	2164
2172	Фотограф илустратор	3431.08	2164
2173	Фото-репортер	3431.09	2164
2174	Дизајнери ентеријера и декоратери 	3432	2163
2175	Декоратер ентеријера 	3432.01	2174
2176	Декоратер излога 	3432.02	2174
2177	Декоратер-аранжер	3432.03	2174
2178	Сценски декоратер	3432.04	2174
2179	Технички дизајнер амбалаже	3432.05	2174
2180	Технички дизајнер графике	3432.06	2174
2181	Технички дизајнер ентеријера	3432.07	2174
2182	Технички дизајнер индустријских производа	3432.08	2174
2183	Технички дизајнер текстила	3432.09	2174
2184	Трговински аранжер	3432.10	2174
2185	Филмски декоратер 	3432.11	2174
2186	Галеријски, библиотечки, архивски и музејски техничари 	3433	2163
2187	Архиварски техничар 	3433.01	2186
2188	Библиотекарски техничар 	3433.02	2186
2189	Галеријски техничар 	3433.03	2186
2190	Књижничар	3433.04	2186
2191	Музејски техничар 	3433.05	2186
2192	Препаратор животиња	3433.06	2186
2193	Препаратор културних добара	3433.07	2186
2194	Репаратор 	3433.08	2186
2195	Главни кувари 	3434	2163
2196	Главни кувар 	3434.01	2195
2197	Главни кувар за припрему колача и слатких пецива 	3434.02	2195
2198	Главни кувар за припрему сосова 	3434.03	2195
2199	Шеф кухиње 	3434.04	2195
2200	Сценски сарадници, извођачи и остали неразврстани на другом месту 	3435	2163
2201	Инспицијент позорнице	3435.01	2200
2202	Каскадер/дублер	3435.02	2200
2203	Координатор радио-телевизијског програма 	3435.03	2200
2204	Мајстор за тетовирање 	3435.04	2200
2205	Менаџер радиодифузне продукције 	3435.05	2200
2206	Моделар сценских костима	3435.06	2200
2207	Моделар сценске обуће	3435.07	2200
2208	Пикер	3435.08	2200
2209	Позоришни костимограф 	3435.09	2200
2210	Позоришни техничар 	3435.10	2200
2211	Позоришни шаптач/суфлер	3435.11	2200
2212	Реквизитер	3435.12	2200
2213	Руковалац уређајима покретне позорнице	3435.13	2200
2214	Статиста 	3435.14	2200
2215	Студијски маскер	3435.15	2200
2216	Сценски маскер-власуљар	3435.16	2200
2217	Сценски расветљивач	3435.17	2200
2218	Техничар за специјалне ефекте 	3435.18	2200
2219	Техничар расвете 	3435.19	2200
2220	Технички организатор оркестра	3435.20	2200
2221	Технички помоћник мајстора светла	3435.21	2200
2222	Технички помоћник режисера	3435.22	2200
2223	Технички помоћник сценографа	3435.23	2200
2224	Технички продуцент естрадно-ревијалног програма	3435.24	2200
2225	Технички продуцент преноса и снимања јавних концерата	3435.25	2200
2226	Технички сценариста опере и балета	3435.26	2200
2227	Шминкер позоришни/сценски 	3435.27	2200
2228	Техничари за информационо-комуникационе технологије (ИКТ) и корисничку подршку 	35	1517
2229	Техничари за информационо-комуникационе технологије (ИКТ) 	351	2228
2230	Оператери ИКТ	3511	2229
2231	Инсталатер софтвера 	3511.01	2230
2232	Оператер периферне рачунарске опреме 	3511.02	2230
2233	Оператер принтера	3511.03	2230
2234	Рачунарски оператер 	3511.04	2230
2235	Техничари ИКТ за корисничку подршку	3512	2229
2236	Асистент за анализу рачунарског система 	3512.01	2235
2237	Асистент за комуникационе системе 	3512.02	2235
2238	Асистент за корисничку подршку 	3512.03	2235
2239	Асистент за рачунарске базе података 	3512.04	2235
2240	Асистент за рачунарско програмирање 	3512.05	2235
2241	Продукциони техничар ИТ система 	3512.06	2235
2242	Техничар за одржавање софтвера	3512.07	2235
2243	Техничар за програмирање 	3512.08	2235
2244	Хелп деск техничар 	3512.09	2235
2245	Техничари за рачунарске мреже и системе	3513	2229
2246	Техничар за рачунарске комуникације	3513.01	2245
2247	Техничар за рачунарске мреже 	3513.02	2245
2248	Веб-техничари 	3514	2229
2249	Администратор сајта 	3514.01	2248
2250	Веб-техничар	3514.02	2248
2251	Веб-мастер 	3514.03	2248
2252	Техничари телекомуникација и емитовања	352	2228
2253	Техничари емитовања и аудио-визуелне технике	3521	2252
2254	Аудио-визуелни оператер 	3521.01	2253
2255	Камерман 	3521.02	2253
2256	Оператер теренске радио-емисионе опреме 	3521.03	2253
2257	Помоћник медијске продукције 	3521.04	2253
2258	Приказивач филмова / кинооператер	3521.05	2253
2259	Радио-монтажер 	3521.06	2253
2260	Радио-оператер	3521.07	2253
2261	ТВ монтажер 	3521.08	2253
2262	Техничар емитовања РТВ програма 	3521.09	2253
2263	Техничар за звучне ефекте	3521.10	2253
2264	Технички помоћник сниматеља радио-емисија	3521.11	2253
2265	Технички помоћник сниматеља тона	3521.12	2253
2266	Технички помоћник филмског и телевизијског монтажера	3521.13	2253
2267	Технички помоћник филмског и телевизијског сниматеља	3521.14	2253
2268	Технички реализатор музичког програма	3521.15	2253
2269	Тонски сниматељ	3521.16	2253
2270	Тонски техничар – микроман 	3521.17	2253
2271	Трик сниматељ	3521.18	2253
2272	Филмски камерман 	3521.19	2253
2273	Техничари за телекомуникације 	3522	2252
2274	Оператер бродских телекомуникација	3522.01	2273
2275	Оператер ваздухопловних телекомуникација	3522.02	2273
2276	Оператер железничких телекомуникација	3522.03	2273
2277	Оператер радарске опреме	3522.04	2273
2278	Оператер радиогониометриста	3522.05	2273
2279	Техничар за информациони инжењеринг	3522.06	2273
2280	Техничар за софтвер мобилних телефона 	3522.07	2273
2281	Техничар монтаже телекомуникационих мрежа 	3522.08	2273
2282	Техничар одржавања телекомуникационе опреме	3522.09	2273
2283	Техничар телекомуникација	3522.10	2273
2284	АДМИНИСТРАТИВНИ СЛУЖБЕНИЦИ	4	\N
2285	Службеници за опште административне послове и оператери на тастатури	41	2284
2286	Службеници за опште административне послове 	411	2285
2287	Службеници за опште административне послове 	4110	2286
2288	Административни оператер	4110.01	2287
2289	Административни службеник	4110.02	2287
2290	Администратор писарнице	4110.03	2287
2291	Биротехничар	4110.04	2287
2292	Деловодничар	4110.05	2287
2293	Службеник регистра	4110.06	2287
2294	Секретари за опште административне послове 	412	2285
2295	Секретари за опште административне послове 	4120	2294
2296	Секретарица 	4120.01	2295
2297	Оператери на тастатури	413	2285
2298	Дактилографи и оператери за обраду текста	4131	2297
2299	Аудиодактилограф	4131.01	2298
2300	Дактилограф	4131.02	2298
2301	Дебатни стенограф	4131.03	2298
2302	Инодактилограф	4131.04	2298
2303	Оператер за обраду текста 	4131.05	2298
2304	Оператер на биротехничким машинама	4131.06	2298
2305	Стенодактилограф	4131.07	2298
2306	Судски записничар	4131.08	2298
2307	Телеграфиста	4131.09	2298
2308	Титлер	4131.10	2298
2309	Службеници за унос података 	4132	2297
2310	Оператер машином за фактурисање/обрачунавање	4132.01	2309
2311	Оператер на оптичком читачу	4132.02	2309
2312	Оператер уноса података	4132.03	2309
2313	Руковалац рачунском машином	4132.04	2309
2314	Службеници за рад са странкама	42	2284
2315	Благајници, инкасанти и сродни	421	2314
2316	Банкарски и сродни шалтерски службеници	4211	2315
2317	Банкарски службеник 	4211.01	2316
2318	Благајник 	4211.02	2316
2319	Мењач новца	4211.03	2316
2320	Кладионичари, крупијеи и сродни	4212	2315
2321	Кладионичар	4212.01	2320
2322	Крупије	4212.02	2320
2323	Залагаоничари	4213	2315
2324	Залагаоничар	4213.01	2323
2325	Наплаћивачи дугова и сродни	4214	2315
2326	Инкасант	4214.01	2325
2327	Инкасант добровољних прилога 	4214.02	2325
2328	Службеници на информисању странака и сродни	422	2314
2329	Службеници туристичких агенција и сродни	4221	2328
2330	Агент за продају авионских карата	4221.01	2329
2331	Организатор путовања	4221.02	2329
2332	Представник туристичке агенције	4221.03	2329
2333	Службеник за продају путних карата 	4221.04	2329
2334	Службеник за продају туристичких аранжмана	4221.05	2329
2335	Службеник на инфо-пулту	4221.06	2329
2336	Службеник у туристичкој агенцији	4221.07	2329
2337	Туристички саветник 	4221.08	2329
2338	Службеници у информативним центрима	4222	2328
2339	Службеник у позивном центру	4222.01	2338
2340	Телефонисти 	4223	2328
2341	Телефониста	4223.01	2340
2342	Хотелски рецепционери 	4224	2328
2343	Хотелски рецепционер	4224.01	2342
2344	Службеници за захтеве и рекламације	4225	2328
2345	Службеник за пријем захтева 	4225.01	2344
2346	Службеник за рекламације 	4225.02	2344
2347	Рецепционери на пријавницама	4226	2328
2348	Медицински рецепционер	4226.01	2347
2349	Рецепционер / службеник на пријавници	4226.02	2347
2350	Анкетари 	4227	2328
2351	Анкетар 	4227.01	2350
2352	Анкетар истраживач	4227.02	2350
2353	Службеници на информисању странака и сродни неразврстани на другом месту	4229	2328
2354	Службеник за проверу документације 	4229.01	2353
2355	Службеници за евидентирање и обраду нумеричких података	43	2284
2356	Књиговодствени, финансијски, статистички и обрачунски службеници 	431	2355
2357	Рачуноводствени и књиговодствени службеници 	4311	2356
2358	Књиговодствени службеник 	4311.01	2357
2359	Рачуноводствени службеник 	4311.02	2357
2360	Фактуриста	4311.03	2357
2361	Статистички, финансијски службеници и службеници осигурања 	4312	2356
2362	Брокерски службеник 	4312.01	2361
2363	Калкулант	4312.02	2361
2364	Службеник за актуарске услуге 	4312.03	2361
2365	Службеник за статистичку обраду података 	4312.04	2361
2366	Службеник за хипотеке 	4312.05	2361
2367	Службеник осигурања	4312.06	2361
2368	Службеник средстава обезбеђења	4312.07	2361
2369	Финансијски службеник	4312.08	2361
2370	Службеници за обрачун зарада	4313	2356
2371	Службеник за обрачун зарада	4313.01	2370
2372	Службеници за евидентирање производње, складиштења и транспорта	432	2355
2373	Складиштари	4321	2372
2374	Издавалац алата и опреме	4321.01	2373
2375	Инвентариста 	4321.02	2373
2376	Магационер	4321.03	2373
2377	Складиштар	4321.04	2373
2378	Службеник за мерење робе 	4321.05	2373
2379	Службеник за отпрему робе 	4321.06	2373
2380	Службеници за планирање и евидентирање у производњи и сродни	4322	2372
2381	Дистрибутер материјала	4322.01	2380
2382	Евидентичар набавке	4322.02	2380
2383	Евидентичар пријема и отпреме робе	4322.03	2380
2384	Евидентичар производње	4322.04	2380
2385	Евидентичар утрошака	4322.05	2380
2386	Мерилац времена (нормирац)	4322.06	2380
2387	Службеник за план требовања материјала 	4322.07	2380
2388	Службеници координирања и евидентирања транспорта	4323	2372
2389	Диспечер саобраћаја 	4323.01	2388
2390	Контролор саобраћаја	4323.02	2388
2391	Надзорник дока (лучки диспонент)	4323.03	2388
2392	Надзорник у саобраћају	4323.04	2388
2393	Отправник возова	4323.05	2388
2394	Службеник контроле транспорта	4323.06	2388
2395	Управник саобраћајног терминала	4323.07	2388
2396	Шеф аутобуске станице	4323.08	2388
2397	Шеф железничке станице	4323.09	2388
2398	Остали административни службеници 	44	2284
2399	Остали административни службеници 	441	2398
2400	Библиотечки службеници	4411	2399
2401	Библиотекарски архивар 	4411.01	2400
2402	Библиотекарски службеник	4411.02	2400
2403	Датотекар	4411.03	2400
2404	Кинотекар	4411.04	2400
2405	Микротекар	4411.05	2400
2406	Нотни архивар	4411.06	2400
2407	Филмотекар/видеотекар	4411.07	2400
2408	Фонотекар	4411.08	2400
2409	Поштари – службеници на обављању поштанских услуга	4412	2399
2410	Картиста ПТТ-а	4412.01	2409
2411	Контролор поштанских услуга	4412.02	2409
2412	Оператер за отпрему поште	4412.03	2409
2413	Оператер за сортирање/разврставање поште	4412.04	2409
2414	Поштански службеник	4412.05	2409
2415	Поштар	4412.06	2409
2416	Шифранти, коректори и сродни	4413	2399
2417	Коректор 	4413.01	2416
2418	Службеник за рецензију	4413.02	2416
2419	Службеник за шифрирање	4413.03	2416
2420	Статистички шифрант	4413.04	2416
2421	Писари и сродни 	4414	2399
2422	Службеници за разврставање, архивирање и копирање докумената	4415	2399
2423	Архивар	4415.01	2422
2424	Службеник за копирање докумената 	4415.02	2422
2425	Службеник за разврставање докумената	4415.03	2422
2426	Службеници за кадровске послове	4416	2399
2427	Службеник за кадровске послове	4416.01	2426
2428	Службеник за радне односе	4416.02	2426
2429	Административни службеници неразврстани на другом месту	4419	2399
2430	Оператер на машини за адресирање	4419.01	2429
2431	Оператер на франкир машини	4419.02	2429
2432	Службеник за издавање новина 	4419.03	2429
2433	Службеник за издавање публикација 	4419.04	2429
2434	Службеник за кореспонденцију 	4419.05	2429
2435	Службеник за прес-клипинг 	4419.06	2429
2436	Службеник за рекламе	4419.07	2429
2437	УСЛУЖНА И ТРГОВАЧКА ЗАНИМАЊА	5	\N
2438	Занимања личних услуга	51	2437
2439	Туристички пратиоци и водичи, стјуарди и кондуктери	511	2438
2440	Земаљски, авио и бродски стјуарди	5111	2439
2441	Авио-стјуард	5111.01	2440
2442	Бродски стјуард	5111.02	2440
2443	Земаљски стјуард	5111.03	2440
2444	Стјуард у возу	5111.04	2440
2445	Кондуктери и контролори возних исправа	5112	2439
2446	Кондуктер у друмском саобраћају	5112.01	2445
2447	Кондуктер у железничком саобраћају	5112.02	2445
2448	Контролор карата у јавном саобраћају	5112.03	2445
2449	Туристички водичи и пратиоци на путовањима	5113	2439
2450	Водич у културној и уметничкој установи	5113.01	2449
2451	Туристички аниматор	5113.02	2449
2452	Туристички водич	5113.03	2449
2453	Кувари	512	2438
2454	Кувари	5120	2453
2455	Бродски кувар	5120.01	2454
2456	Дијететски кувар	5120.02	2454
2457	Кувар	5120.03	2454
2458	Кувар посластица 	5120.04	2454
2459	Кувар сосова и зачина за конзервирање	5120.05	2454
2460	Кувар специјалних јела	5120.06	2454
2461	Помоћник кувара 	5120.07	2454
2462	Роштиљ-мајстор	5120.08	2454
2463	Хотелски посластичар	5120.09	2454
2464	Конобари и бармени 	513	2438
2465	Конобари	5131	2464
2466	Главни конобар	5131.01	2465
2467	Конобар	5131.02	2465
2468	Послужитељ у кантини	5131.03	2465
2469	Сервир	5131.04	2465
2470	Сомелијер 	5131.05	2465
2471	Шеф сале	5131.06	2465
2472	Бармени	5132	2464
2473	Бариста	5132.01	2472
2474	Бармен	5132.02	2472
2475	Занимања за негу лепоте 	514	2438
2476	Фризери и сродни 	5141	2475
2477	Берберин	5141.01	2476
2478	Фризер	5141.02	2476
2479	Фризер специјалиста за негу косе 	5141.03	2476
2480	Фризер стилист	5141.04	2476
2481	Козметичари и сродни	5142	2475
2482	Козметичар	5142.01	2481
2483	Мајстор тетоваже	5142.02	2481
2484	Мајстор трајне шминке	5142.03	2481
2485	Маникир 	5142.04	2481
2486	Маникир-педикир	5142.05	2481
2487	Педикир	5142.06	2481
2488	Пирсинг мајстор	5142.07	2481
2489	Шминкер	5142.08	2481
2490	Кућепазитељи и надзорници 	515	2438
2491	Надзорници одржавања чистоће у пословним просторима	5151	2490
2492	Надзорник одржавања чистоће 	5151.01	2491
2493	Надзорник одржавања чистоће у кампу	5151.02	2491
2494	Надзорник одржавања чистоће у пословном простору	5151.03	2491
2495	Надзорник одржавања чистоће у хотелу	5151.04	2491
2496	Кућепазитељи у домаћинствима 	5152	2490
2497	Батлер	5152.01	2496
2498	Домаћин зграде	5152.02	2496
2499	Кућепазитељ у домаћинствима за смештај гостију	5152.03	2496
2500	Кућепазитељ у домаћинству 	5152.04	2496
2501	Шеф послуге у домаћинству	5152.05	2496
2502	Кућепазитељи у стамбеним и пословним зградама	5153	2490
2503	Домар	5153.01	2502
2504	Кућепазитељ у верским објектима	5153.02	2502
2505	Кућепазитељ у стамбеним и пословним зградама 	5153.03	2502
2506	Управник зграде 	5153.04	2502
2507	Остала занимања личних услуга	516	2438
2508	Астролози и сродни	5161	2507
2509	Астролог	5161.01	2508
2510	Нумеролог	5161.02	2508
2511	Хиромант	5161.03	2508
2512	Лични пратиоци и лични собари	5162	2507
2513	Лични собар	5162.01	2512
2514	Пратилац/телохранитељ	5162.02	2512
2515	Погребници и сродни	5163	2507
2516	Балсамер	5163.01	2515
2517	Организатор погреба 	5163.02	2515
2518	Погребни аранжер	5163.03	2515
2519	Погребник	5163.04	2515
2520	Радник на кремирању	5163.05	2515
2521	Занимања за негу и чување животиња 	5164	2507
2522	Ветеринарски помоћник за негу животиња	5164.01	2521
2523	Дресер коња	5164.02	2521
2524	Дресер паса	5164.03	2521
2525	Кротитељ коња	5164.04	2521
2526	Фризер за псе (groomer)	5164.05	2521
2527	Чувар животиња у зоо-врту 	5164.06	2521
2528	Инструктори вожње друмских моторних возила	5165	2507
2529	Инструктор вожње	5165.01	2528
2530	Занимања личних услуга неразврстана на другом месту	5169	2507
2531	Домаћин клуба	5169.01	2530
2532	Хостеса	5169.02	2530
2533	Трговачка и сродна занимања	52	2437
2534	Улични и пијачни продавци 	521	2533
2535	Продавци на тезгама и пијацама 	5211	2534
2536	Продавац на пијаци/вашару	5211.01	2535
2537	Продавац на тезги	5211.02	2535
2538	Продавци хране на улици и другим јавним местима 	5212	2534
2539	Пиљар	5212.01	2538
2540	Продавац брзе хране	5212.02	2538
2541	Продавац освежавајућих пића на јавном месту	5212.03	2538
2542	Улични продавац хране/пића	5212.04	2538
2543	Продавци и трговци у продавницама	522	2533
2544	Продавци у киоску и трговци на мало	5221	2543
2545	Продавац у киоску 	5221.01	2544
2546	Продавац у трговинској радњи	5221.02	2544
2547	Пословође продавница 	5222	2543
2548	Надзорник у супермаркету 	5222.01	2547
2549	Пословођа продавнице	5222.02	2547
2550	Продавци у продавницама 	5223	2543
2551	Продавац мешовите робе	5223.01	2550
2552	Продавац спортске опреме, оружја и муниције	5223.02	2550
2553	Продавац у велепродаји	5223.03	2550
2554	Продавац у малопродаји	5223.04	2550
2555	Продавац у малопродаји ауто-делова	5223.05	2550
2556	Продавац у малопродаји боја, лакова и хемикалија	5223.06	2550
2557	Продавац у малопродаји гвожђарске, металне и техничке робе	5223.07	2550
2558	Продавац у малопродаји грађевинског и огревног материјала	5223.08	2550
2559	Продавац у малопродаји књижарско-канцеларијске робе	5223.09	2550
2560	Продавац у малопродаји коже, гуме и обуће	5223.10	2550
2561	Продавац у малопродаји металне и електротехничке робе	5223.11	2550
2562	Продавац у малопродаји мешовите робе	5223.12	2550
2563	Продавац у малопродаји музичких инструмената	5223.13	2550
2564	Продавац у малопродаји намештаја	5223.14	2550
2565	Продавац у малопродаји парфимерије, бижутерије и играчака	5223.15	2550
2566	Продавац у малопродаји прехрамбене робе	5223.16	2550
2567	Продавац у малопродаји семенске робе, садног материјала и опреме за пољопривреду (осим хемикалија)	5223.17	2550
2568	Продавац у малопродаји спортске опреме и оружја	5223.18	2550
2569	Продавац у малопродаји текстила	5223.19	2550
2570	Продавац у малопродаји фото и оптичке опреме	5223.20	2550
2571	Продавац у малопродаји хемијских средстава за пољопривреду	5223.21	2550
2572	Продавац фото и оптичке опреме	5223.22	2550
2573	Продавац школског и канцеларијског материјала, прибора и књига	5223.23	2550
2574	Трговачки помоћник 	5223.24	2550
2575	Касири и билетари	523	2533
2576	Касири и билетари	5230	2575
2577	Билетар	5230.01	2576
2578	Благајник на бензинској пумпи 	5230.02	2576
2579	Благајник у продавници 	5230.03	2576
2580	Касир	5230.04	2576
2581	Касир у трговинском/угоститељском објекту	5230.05	2576
2582	Наплаћивач путарине	5230.06	2576
2583	Остала трговачка и сродна занимања	524	2533
2584	Манекени и модели	5241	2583
2585	Манекен	5241.01	2584
2586	Рекламни модел 	5241.02	2584
2587	Уметнички модел	5241.03	2584
2588	Фото-модел	5241.04	2584
2589	Демонстратори производа 	5242	2583
2590	Демонстратор производа 	5242.01	2589
2591	Трговачки путници 	5243	2583
2592	Аквизитер	5243.01	2591
2593	Продавац од врата до врата 	5243.02	2591
2594	Трговачки путник 	5243.03	2591
2595	Продавци каталошке продаје	5244	2583
2596	Интернет продавац	5244.01	2595
2597	Продавац каталошке продаје	5244.02	2595
2598	Продавац у позивном центру	5244.03	2595
2599	Точиоци горива 	5245	2583
2600	Точилац горива на бензинској пумпи 	5245.01	2599
2601	Точилац горива у марини	5245.02	2599
2602	Послужитељи хране у кафетеријама и сродни 	5246	2583
2603	Послужитељ брзе хране	5246.01	2602
2604	Послужитељ хране у кафетеријама 	5246.02	2602
2605	Послужитељ хране у салата бару 	5246.03	2602
2606	Трговачка и сродна занимања неразврстана на другом месту	5249	2583
2607	Изнајмљивач бицикала и ролера	5249.01	2606
2608	Изнајмљивач моторних возила 	5249.02	2606
2609	Изнајмљивач опреме за зимске спортове	5249.03	2606
2610	Изнајмљивач реквизита на плажи	5249.04	2606
2611	Трговац отпадом 	5249.05	2606
2612	Занимања за личну негу и помоћ	53	2437
2613	Занимања за негу и чување деце и педагошки асистенти	531	2612
2614	Занимања за негу и чување деце	5311	2613
2615	Дадиља/бебиситер	5311.01	2614
2616	Педагошки асистенти	5312	2613
2617	Андрагошки асистент 	5312.01	2616
2618	Педагошки асистент	5312.02	2616
2619	Школски демонстратор 	5312.03	2616
2620	Занимања за личну здравствену негу и помоћ 	532	2612
2621	Занимања за личну здравствену негу и помоћ у здравственим и сродним установама	5321	2620
2622	Болничар неговатељ 	5321.01	2621
2623	Масер-купељар	5321.02	2621
2624	Занимања за личну здравствену негу и помоћ у кући	5322	2620
2625	Геронтодомаћица	5322.01	2624
2626	Кућна неговатељица	5322.02	2624
2627	Персонални асистент 	5322.03	2624
2628	Релаксациони масер	5322.04	2624
2629	Занимања за личну здравствену негу и помоћ неразврстана на другом месту	5329	2620
2630	Занимања обезбеђења и заштите	54	2437
2631	Занимања обезбеђења и заштите	541	2630
2632	Ватрогасци 	5411	2631
2633	Ватрогасац	5411.01	2632
2634	Ватрогасац спасилац	5411.02	2632
2635	Радник противпожарне заштите	5411.03	2632
2636	Шумски ватрогасац	5411.04	2632
2637	Полицајци 	5412	2631
2638	Полицајац	5412.01	2637
2639	Саобраћајни полицајац 	5412.02	2637
2640	Стражари	5413	2631
2641	Затворски стражар	5413.01	2640
2642	Правосудни стражар	5413.02	2640
2643	Радници физичко-техничког обезбеђења	5414	2631
2644	Портир	5414.01	2643
2645	Радник обезбеђења	5414.02	2643
2646	Редар	5414.03	2643
2647	Телохранитељ	5414.04	2643
2648	Чувар	5414.05	2643
2649	Занимања обезбеђења и заштите неразврстана на другом месту 	5419	2631
2650	Горски спасилац 	5419.01	2649
2651	Комунални полицајац 	5419.02	2649
2652	Ловочувар	5419.03	2649
2653	Рибочувар 	5419.04	2649
2654	Спасилац на плажи	5419.05	2649
2655	Чувар заштићеног привредног добра	5419.06	2649
2656	Чувар плаже 	5419.07	2649
2657	Чувар рибњака	5419.08	2649
2658	Чувар у марини	5419.09	2649
2659	ПОЉОПРИВРЕДНИЦИ, ШУМАРИ, РИБАРИ И СРОДНИ	6	\N
2660	Тржишно оријентисани пољопривредници	61	2659
2661	Тржишно оријентисани земљорадници	611	2660
2662	Узгајивачи ратарских култура и поврћа 	6111	2661
2663	Повртар	6111.01	2662
2664	Ратар	6111.02	2662
2665	Узгајивач гљива	6111.03	2662
2666	Узгајивач дувана	6111.04	2662
2667	Узгајивач житарица	6111.05	2662
2668	Узгајивач конопље/лана/памука	6111.06	2662
2669	Узгајивач кромпира	6111.07	2662
2670	Узгајивач купуса	6111.08	2662
2671	Узгајивач соје	6111.09	2662
2672	Узгајивач хмеља	6111.10	2662
2673	Узгајивач чаја	6111.11	2662
2674	Узгајивач шећерне репе 	6111.12	2662
2675	Воћари, виноградари и сродни	6112	2661
2676	Виноградар	6112.01	2675
2677	Воћар	6112.02	2675
2678	Калемар воћа	6112.03	2675
2679	Поткресивач воћака	6112.04	2675
2680	Узгајивач бобичастог воћа	6112.05	2675
2681	Узгајивач малина	6112.06	2675
2682	Баштовани, расадничари и занимања хортикултуре	6113	2661
2683	Баштован 	6113.01	2682
2684	Радник у стакленику/пластенику	6113.02	2682
2685	Радник хортикултуре	6113.03	2682
2686	Расадничар	6113.04	2682
2687	Узгајивач дрвећа	6113.05	2682
2688	Узгајивач жбунастих култура	6113.06	2682
2689	Узгајивач зачина у расаднику	6113.07	2682
2690	Цвећар	6113.08	2682
2691	Узгајивачи мешовитих биљних култура 	6114	2661
2692	Пољопривредни радник за мешовите културе 	6114.01	2691
2693	Тржишно оријентисани узгајивачи животиња и сродни	612	2660
2694	Узгајивачи стоке и произвођачи млека 	6121	2693
2695	Млекар	6121.01	2694
2696	Пастир	6121.02	2694
2697	Сточар	6121.03	2694
2698	Стрижач стоке 	6121.04	2694
2699	Товилац стоке	6121.05	2694
2700	Трговац стоком	6121.06	2694
2701	Узајивач паса	6121.07	2694
2702	Узгајивач говеда	6121.08	2694
2703	Узгајивач дивљачи у ловишту  	6121.09	2694
2704	Узгајивач коза	6121.10	2694
2705	Узгајивач коња	6121.11	2694
2706	Узгајивач оваца	6121.12	2694
2707	Узгајивач свиња	6121.13	2694
2708	Узгајивачи живине 	6122	2693
2709	Живинар руковалац инкубаторима	6122.01	2708
2710	Произвођач јаја	6122.02	2708
2711	Узгајивач живине 	6122.03	2708
2712	Узгајивачи пчела и свилених буба 	6123	2693
2713	Пчелар	6123.01	2712
2714	Узгајивач свилене бубе	6123.02	2712
2715	Тржишно оријентисани узгајивачи животиња неразврстани на другом месту	6129	2693
2716	Узгајивач крзнаша	6129.01	2715
2717	Узгајивач малих и егзотичних животиња 	6129.02	2715
2718	Узгајивач птица	6129.03	2715
2719	Узгајивач пужева	6129.04	2715
2720	Тржишно оријентисани ратарско-сточарски произвођачи	613	2660
2721	Тржишно оријентисани ратарско-сточарски произвођачи	6130	2720
2722	Пољопривредни произвођач 	6130.01	2721
2723	Тржишно оријентисана шумарска и рибарска занимања, ловци и риболовци	62	2659
2724	Шумари и сродни	621	2723
2725	Шумари и сродни	6210	2724
2726	Катранџија	6210.01	2725
2727	Кречар	6210.02	2725
2728	Мерач трупаца	6210.03	2725
2729	Обележавач стабала / дрвене грађе	6210.04	2725
2730	Пошумљивач	6210.05	2725
2731	Произвођач дрвеног угља	6210.06	2725
2732	Смолар	6210.07	2725
2733	Трговац шумском грађом 	6210.08	2725
2734	Шумар	6210.09	2725
2735	Шумски поткресивач	6210.10	2725
2736	Шумски секач 	6210.11	2725
2737	Узгајивачи риба, риболовци, ловци и замкари 	622	2723
2738	Узгајивачи риба и водених култура	6221	2737
2739	Узгајивач риба	6221.01	2738
2740	Рибари на рекама и језерима 	6222	2737
2741	Алас/рибар 	6222.01	2740
2742	Рибар на језерима	6222.02	2740
2743	Рибари на мору 	6223	2737
2744	Ловци и замкари 	6224	2737
2745	Ловац 	6224.01	2744
2746	Хајкач	6224.02	2744
2747	Пољопривредна и рибарска занимања за сопствене потребе 	63	2659
2748	Земљорадници за сопствене потребе	631	2747
2749	Земљорадници за сопствене потребе	6310	2748
2750	Узгајивач повртларских култура 	6310.01	2749
2751	Узгајивач ратарских култура 	6310.02	2749
2752	Узгајивачи стоке за сопствене потребе	632	2747
2753	Узгајивачи стоке за сопствене потребе	6320	2752
2754	Узгајивач стоке	6320.01	2753
2755	Ратарско-сточарски произвођачи за сопствене потребе	633	2747
2756	Ратарско-сточарски произвођачи за сопствене потребе	6330	2755
2757	Ратарско-сточарски пољопривредни произвођач за сопствене потребе 	6330.01	2756
2758	Рибари, ловци, замкари и сакупљачи шумских плодова за сопствене потребе	634	2747
2759	Рибари, ловци, замкари и сакупљачи шумских плодова за сопствене потребе 	6340	2758
2760	ЗАНАТЛИЈЕ И СРОДНИ	7	\N
2761	Грађевинска и сродна занатска занимања (осим електричара)	71	2760
2762	Извођачи основних грађевинских радова	711	2761
2763	Градитељи мањих и монтажних објеката	7111	2762
2764	Градитељ кућа	7111.01	2763
2765	Монтажер-утезач	7111.02	2763
2766	Монтажер грађевинских префабрикованих конструкција	7111.03	2763
2767	Монтажер куће	7111.04	2763
2768	Зидари 	7112	2762
2769	Ватростални зидар	7112.01	2768
2770	Зидар	7112.02	2768
2771	Зидар ингот плочама	7112.03	2768
2772	Зидар каменом	7112.04	2768
2773	Зидар специјалиста за димњаке 	7112.05	2768
2774	Зидар циглом/опеком	7112.06	2768
2775	Помоћник зидара	7112.07	2768
2776	Обрађивачи камена и каменоресци 	7113	2762
2777	Брусач камена	7113.01	2776
2778	Бушач камена	7113.02	2776
2779	Гатериста камена	7113.03	2776
2780	Каменописац	7113.04	2776
2781	Каменорезац	7113.05	2776
2782	Обрађивач гранита 	7113.06	2776
2783	Полирер камена/шкриљаца/гранита/мермера	7113.07	2776
2784	Помоћник каменоресца	7113.08	2776
2785	Армирачи, бетонирци и финишери бетона 	7114	2762
2786	Армирач	7114.01	2785
2787	Асфалтер	7114.02	2785
2788	Бетонирац	7114.03	2785
2789	Бетонирац финишер бетона 	7114.04	2785
2790	Помоћник армирача	7114.05	2785
2791	Помоћник бетонирца	7114.06	2785
2792	Терацер	7114.07	2785
2793	Тесари и грађевински столари 	7115	2762
2794	Бродоградитељ дрвене конструкције 	7115.01	2793
2795	Бродски тесар	7115.02	2793
2796	Грађевински столар 	7115.03	2793
2797	Грађевински тесар 	7115.04	2793
2798	Изграђивач дрвених чамаца/дереглија/јарбола/греда	7115.05	2793
2799	Позоришни тесар 	7115.06	2793
2800	Помоћник тесара	7115.07	2793
2801	Столар специјалиста за завршну обраду 	7115.08	2793
2802	Тесар	7115.09	2793
2803	Тесар калупар	7115.10	2793
2804	Извођачи основних грађевинских радова неразврстани на другом месту	7119	2762
2805	Монтер скела 	7119.01	2804
2806	Помоћник хидрограђевинара	7119.02	2804
2807	Радник на рушењу зграда 	7119.03	2804
2808	Хидрограђевинар	7119.04	2804
2809	Извођачи завршних грађевинских радова	712	2761
2810	Кровопокривачи	7121	2809
2811	Кровопокривач 	7121.01	2810
2812	Кровопокривач специјалиста за кровове од дрвета	7121.02	2810
2813	Кровопокривач специјалиста за кровове од лима 	7121.03	2810
2814	Кровопокривач специјалиста за кровове од црепа 	7121.04	2810
2815	Помоћник кровопокривача 	7121.05	2810
2816	Подополагачи и керамичари	7122	2809
2817	Керамичар	7122.01	2816
2818	Керамичар специјалиста за мермер	7122.02	2816
2819	Паркетар 	7122.03	2816
2820	Подополагач	7122.04	2816
2821	Помоћник керамичара	7122.05	2816
2822	Помоћник подополагача	7122.06	2816
2823	Гипсари и фасадери	7123	2809
2824	Гипсар	7123.01	2823
2825	Гипсар израђивач орнамената	7123.02	2823
2826	Помоћник гипсара	7123.03	2823
2827	Помоћник фасадера 	7123.04	2823
2828	Фасадер	7123.05	2823
2829	Фасадер специјалиста за украсне фасаде 	7123.06	2823
2830	Изолатери 	7124	2809
2831	Бродоизолатер	7124.01	2830
2832	Изолатер	7124.02	2830
2833	Изолатер специјалиста за звучну изолацију	7124.03	2830
2834	Изолатер специјалиста за климатизацију 	7124.04	2830
2835	Изолатер специјалиста за котлове и цеви 	7124.05	2830
2836	Помоћник изолатера 	7124.06	2830
2837	Термоизолатер	7124.07	2830
2838	Хидроизолатер 	7124.08	2830
2839	Стаклар 	7125	2809
2840	Стаклар 	7125.01	2839
2841	Стаклар специјалиста за аутостакла 	7125.02	2839
2842	Водоинсталатери и монтери цевовода 	7126	2809
2843	Водоинсталатер	7126.01	2842
2844	Инсталатер грејања	7126.02	2842
2845	Инсталатер грејања специјалиста за гасне инсталације 	7126.03	2842
2846	Монтер вентилационих цеви	7126.04	2842
2847	Монтер гасоенергетских и пнеумоенергетских постројења	7126.05	2842
2848	Монтер термоенергетских постројења	7126.06	2842
2849	Монтер цевовода	7126.07	2842
2850	Помоћник водоинсталатера	7126.08	2842
2851	Помоћник инсталатера грејања	7126.09	2842
2852	Помоћник монтера цевовода	7126.10	2842
2853	Инсталатери и механичари система за климатизацију 	7127	2809
2854	Инсталатер климатизације	7127.01	2853
2855	Механичар за расхладне уређаје	7127.02	2853
2856	Извођачи завршних занатских радова у грађевинарству и сродни	713	2761
2857	Молери и сродни	7131	2856
2858	Молер 	7131.01	2857
2859	Молер/фарбар 	7131.02	2857
2860	Помоћник молера/фарбара	7131.03	2857
2861	Фарбари и лакирери 	7132	2856
2862	Авио-фарбар	7132.01	2861
2863	Ауто-лакирер 	7132.02	2861
2864	Бродофарбар	7132.03	2861
2865	Грађевински фарбар	7132.04	2861
2866	Извођач површинске заштите метала/дрвета	7132.05	2861
2867	Лакирер дрвета	7132.06	2861
2868	Лакирер индустријских производа	7132.07	2861
2869	Металофарбар-лакирер	7132.08	2861
2870	Помоћник металофарбара-лакирера	7132.09	2861
2871	Фарбар	7132.10	2861
2872	Фарбар индустријских производа	7132.11	2861
2873	Чистачи фасада и димовода	7133	2856
2874	Димничар	7133.01	2873
2875	Помоћник димничара	7133.02	2873
2876	Чистач индустријских димњака	7133.03	2873
2877	Чистач фасада пескарењем 	7133.04	2873
2878	Металска, машинска и сродна занатска занимања	72	2760
2879	Калупари, ливци, заваривачи, лимари, монтери металних конструкција и сродни	721	2878
2880	Калупари, језграри и ливци	7211	2879
2881	Ливац 	7211.01	2880
2882	Ливац лаких и обојених метала	7211.02	2880
2883	Ливац прецизног лива	7211.03	2880
2884	Ливачки језграр-калупар	7211.04	2880
2885	Заваривачи и резачи пламеном 	7212	2879
2886	Електрозаваривач	7212.01	2885
2887	Заваривач-бравар 	7212.02	2885
2888	Заваривач-резач гасом	7212.03	2885
2889	Лемилац	7212.04	2885
2890	Помоћник заваривача 	7212.05	2885
2891	Универзални заваривач	7212.06	2885
2892	Лимари 	7213	2879
2893	Авио-лимар	7213.01	2892
2894	Ауто-лимар	7213.02	2892
2895	Бродолимар	7213.03	2892
2896	Индустријски лимар	7213.04	2892
2897	Котлар-казанџија 	7213.05	2892
2898	Лимар 	7213.06	2892
2899	Помоћник лимара	7213.07	2892
2900	Монтери металних конструкција	7214	2879
2901	Бродомонтер 	7214.01	2900
2902	Бродоцевар 	7214.02	2900
2903	Закивач металних конструкција	7214.03	2900
2904	Закивач пнеуматиком	7214.04	2900
2905	Израђивач металних конструкција	7214.05	2900
2906	Израђивач металних конструкција бродова/авиона/мостова	7214.06	2900
2907	Монтер металних/челичних конструкција	7214.07	2900
2908	Монтер рекламних паноа	7214.08	2900
2909	Помоћник бродомонтера	7214.09	2900
2910	Помоћник монтера металних/челичних конструкција	7214.10	2900
2911	Савијач металних плоча	7214.11	2900
2912	Монтери челичне ужади и каблова	7215	2879
2913	Монтер железничких каблова	7215.01	2912
2914	Монтер каблова на бушотинама нафте и плина	7215.02	2912
2915	Монтер каблова на дизалицама	7215.03	2912
2916	Монтер каблова на мостовима	7215.04	2912
2917	Монтер лифтова и жичара	7215.05	2912
2918	Ковачи, алатничари и сродни	722	2878
2919	Ковачи и пресери метала	7221	2918
2920	Ковач	7221.01	2919
2921	Ковач извлачилац жице	7221.02	2919
2922	Ковач поткивач	7221.03	2919
2923	Ковач пресер метала 	7221.04	2919
2924	Металопресер	7221.05	2919
2925	Помоћник ковача	7221.06	2919
2926	Помоћник металопресера	7221.07	2919
2927	Алатничари и сродни	7222	2918
2928	Алатничар	7222.01	2927
2929	Алатничар дијамантског алата	7222.02	2927
2930	Алатничар за алате у обради деформисањем, одвајањем и ливењем	7222.03	2927
2931	Алатничар за израду шаблона	7222.04	2927
2932	Алатничар за резне алате	7222.05	2927
2933	Бравар	7222.06	2927
2934	Бродобравар	7222.07	2927
2935	Израђивач мерних алата	7222.08	2927
2936	Машинбравар	7222.09	2927
2937	Моделар	7222.10	2927
2938	Оружар 	7222.11	2927
2939	Помоћник бравара/машинбравара	7222.12	2927
2940	Стрелац ватреним оружјем	7222.13	2927
2941	Стрелац на испитивању отпорности оружја 	7222.14	2927
2942	Подешавачи и управљачи алатним машинама	7223	2918
2943	Израђивач жичаних мрежа и тканина	7223.01	2942
2944	Израђивач металних занатских производа	7223.02	2942
2945	Израђивач ситне металне галантерије	7223.03	2942
2946	Металобушач	7223.04	2942
2947	Металоглодач	7223.05	2942
2948	Металостругар	7223.06	2942
2949	Оператер обраде метала на машинама са нумеричким управљањем	7223.07	2942
2950	Подешавач машина за обраду метала деформисањем и одвајањем	7223.08	2942
2951	Подешавач машина за обраду метала резањем	7223.09	2942
2952	Послужилац подешених брусилица/бушилица/глодалица/стругова	7223.10	2942
2953	Руковалац алатним машинама	7223.11	2942
2954	Руковалац електричним маказама за метал	7223.12	2942
2955	Руковалац машинама за заваривање 	7223.13	2942
2956	Руковалац машинама за израду резног алата	7223.14	2942
2957	Руковалац машином за бушење метала	7223.15	2942
2958	Руковалац машином за гравирање метала	7223.16	2942
2959	Руковалац машином за ецовање метала	7223.17	2942
2960	Руковалац машином за закивање	7223.18	2942
2961	Руковалац машином за израду арматура	7223.19	2942
2962	Руковалац машином за израду металних производа	7223.20	2942
2963	Руковалац машином за израду спортске опреме од метала	7223.21	2942
2964	Руковалац машином за ковање метала	7223.22	2942
2965	Руковалац машином за млевење метала	7223.23	2942
2966	Руковалац машином за моделирање метала	7223.24	2942
2967	Руковалац машином за предење метала	7223.25	2942
2968	Руковалац машином за савијање метала	7223.26	2942
2969	Руковалац машином за сечење метала	7223.27	2942
2970	Руковалац машином за штанцовање метала	7223.28	2942
2971	Руковалац постројењем за ковање и пресовање челика	7223.29	2942
2972	Руковалац стругом за метал	7223.30	2942
2973	Брусачи, полирери и оштрачи алата	7224	2918
2974	Металобрусач	7224.01	2973
2975	Оштрач алата	7224.02	2973
2976	Полирер метала 	7224.03	2973
2977	Механичари машина и сервисери 	723	2878
2978	Ауто-механичари 	7231	2977
2979	Ауто-механичар	7231.01	2978
2980	Ауто-механичар специјалиста за привредна возила	7231.02	2978
2981	Ауто-механичар специјалиста за путничка возила 	7231.03	2978
2982	Механичар за моторе шинских возила	7231.04	2978
2983	Механичар за мотоцикле	7231.05	2978
2984	Механичар за хидраулику на возилима	7231.06	2978
2985	Механичар за штеловање мотора	7231.07	2978
2986	Помоћник аутомеханичара	7231.08	2978
2987	Авио-механичари 	7232	2977
2988	Авио-механичар	7232.01	2987
2989	Ваздухопловни механичар за електросистеме 	7232.02	2987
2990	Ваздухопловни механичар за моторне системе 	7232.03	2987
2991	Ваздухопловни механичар за систем конструкције 	7232.04	2987
2992	Ваздухопловни механичар за хидраулични систем 	7232.05	2987
2993	Надзорник одржавања ваздухоплова 	7232.06	2987
2994	Механичари пољопривредних и индустријских машина 	7233	2977
2995	Бродомеханичар	7233.01	2994
2996	Машински механичар	7233.02	2994
2997	Механичар за алатне машине	7233.03	2994
2998	Механичар за грађевинске машине	7233.04	2994
2999	Механичар за графичке машине	7233.05	2994
3000	Механичар за гумарске и пластичарске машине 	7233.06	2994
3001	Механичар за дрвопрерађивачке машине 	7233.07	2994
3002	Механичар за кожарске и обућарске машине	7233.08	2994
3003	Механичар за ложишне и димоводне уређаје	7233.09	2994
3004	Механичар за лучку и претоварну механизацију	7233.10	2994
3005	Механичар за машине дуванске производње	7233.11	2994
3006	Механичар за машине за шивење	7233.12	2994
3007	Механичар за машине прехрамбене производње	7233.13	2994
3008	Механичар за машинске уређаје нуклеарних постројења	7233.14	2994
3009	Механичар за металуршка постројења	7233.15	2994
3010	Механичар за пољопривредне машине	7233.16	2994
3011	Механичар за рударске машине	7233.17	2994
3012	Механичар за текстилне машине	7233.18	2994
3013	Механичар за уређаје на шинским возилима 	7233.19	2994
3014	Механичар за уређаје хидраулике и пнеуматике	7233.20	2994
3015	Механичар за штампарске машине	7233.21	2994
3016	Механичар индустријских машина	7233.22	2994
3017	Механичар процесних машина и постројења	7233.23	2994
3018	Механичари за поправку бицикала и сродни 	7234	2977
3019	Механичар за бицикле и мотоцикле 	7234.01	3018
3020	Сервисер дечијих колица 	7234.02	3018
3021	Сервисер инвалидских колица 	7234.03	3018
3022	Уметничке занатлије ручним алатима и штампари	73	2760
3023	Уметничкe занатлије ручним алатима	731	3022
3024	Израђивачи прецизних инструмената и сервисери 	7311	3023
3025	Балансер вага	7311.01	3024
3026	Израђивач метеоролошких инструмената 	7311.02	3024
3027	Израђивач хируршких инструмената 	7311.03	3024
3028	Калибратор прецизних инструмената	7311.04	3024
3029	Механичар за уређаје за мерење и регулацију	7311.05	3024
3030	Прецизни механичар	7311.06	3024
3031	Прецизни механичар за бироопрему 	7311.07	3024
3032	Прецизни механичар за индустријску опрему	7311.08	3024
3033	Прецизни механичар за медицинске инструменте и апарате	7311.09	3024
3034	Прецизни механичар за оптичке инструменте	7311.10	3024
3035	Прецизни механичар за ортопедска помагала	7311.11	3024
3036	Прецизни механичар за стоматолошке инструменте и опрему	7311.12	3024
3037	Сервисер фотографске опреме 	7311.13	3024
3038	Часовничар	7311.14	3024
3039	Израђивачи и штимери музичких инструмената 	7312	3023
3040	Израђивач гудачких музичких инструмената	7312.01	3039
3041	Израђивач дувачких музичких инструмената 	7312.02	3039
3042	Израђивач жичаних музичких инструмената	7312.03	3039
3043	Израђивач музичких инструмената удараљки	7312.04	3039
3044	Израђивач музичких клавирских инструмената	7312.05	3039
3045	Клавирштимер	7312.06	3039
3046	Мајстор за поправку жичаних музичких инструмената 	7312.07	3039
3047	Мајстор за поправку музичких инструмената удараљки	7312.08	3039
3048	Мајстор за поправку музичких клавирских инструмената	7312.09	3039
3049	Сервисер лимених дувачких инструмената 	7312.10	3039
3050	Штимер музичких инструмената 	7312.11	3039
3051	Јувелири и израђивачи накита 	7313	3023
3052	Бижутер 	7313.01	3051
3053	Брусач драгог камена	7313.02	3051
3054	Брусач племенитих метала	7313.03	3051
3055	Глачалац накита	7313.04	3051
3056	Гравер драгуљар	7313.05	3051
3057	Златар	7313.06	3051
3058	Златар-драгуљар	7313.07	3051
3059	Израђивач накита	7313.08	3051
3060	Кујунџија 	7313.09	3051
3061	Ливац калупа за накит	7313.10	3051
3062	Плетач племенитих метала	7313.11	3051
3063	Помоћник златара	7313.12	3051
3064	Поправљач накита	7313.13	3051
3065	Резач драгог камења и накита	7313.14	3051
3066	Сребрнар	7313.15	3051
3067	Филиграниста	7313.16	3051
3068	Грнчари и сродни	7314	3023
3069	Брусач грнчарије	7314.01	3068
3070	Гипсомоделар	7314.02	3068
3071	Глачалац керамике	7314.03	3068
3072	Грнчар	7314.04	3068
3073	Израђивач калупа за грнчарију и порцелан	7314.05	3068
3074	Израђивач санитарне керамике	7314.06	3068
3075	Ливац грнчарије и порцелана	7314.07	3068
3076	Моделар грнчарије и порцелана	7314.08	3068
3077	Моделар керамике 	7314.09	3068
3078	Пресер глине/грнчарије/порцелана	7314.10	3068
3079	Израђивачи и обрађивачи стакла	7315	3023
3080	Бушач стакла	7315.01	3079
3081	Мешач стаклене масе	7315.02	3079
3082	Обрађивач стакла	7315.03	3079
3083	Полирер стакла	7315.04	3079
3084	Припремач-млинар стаклене масе	7315.05	3079
3085	Савијач стакла	7315.06	3079
3086	Стаклар за оптичка стакла	7315.07	3079
3087	Стаклобрусач	7315.08	3079
3088	Стаклодувач	7315.09	3079
3089	Стаклоливац	7315.10	3079
3090	Стаклорезац	7315.11	3079
3091	Фирмописци, декоратери и гравери	7316	3023
3092	Бакројеткач	7316.01	3091
3093	Бакрорезац	7316.02	3091
3094	Бојадисер декоративног стакла 	7316.03	3091
3095	Бојадисер керамике/грнчарије	7316.04	3091
3096	Гравер	7316.05	3091
3097	Гравер клишеа	7316.06	3091
3098	Декоратер 	7316.07	3091
3099	Декоратер керамике/грнчарије	7316.08	3091
3100	Јеткач стакла / ецер	7316.09	3091
3101	Калиграф	7316.10	3091
3102	Коректор фото-гравире	7316.11	3091
3103	Литограф	7316.12	3091
3104	Матирер/пескирер стакла	7316.13	3091
3105	Металограф	7316.14	3091
3106	Монтер у фото-гравири	7316.15	3091
3107	Печаторезац	7316.16	3091
3108	Посребривач стакла/огледала	7316.17	3091
3109	Ретушер керамике	7316.18	3091
3110	Стаклогравер	7316.19	3091
3111	Фирмописац	7316.20	3091
3112	Фото-гравер 	7316.21	3091
3113	Фотолитографски цртач	7316.22	3091
3114	Хемиграф 	7316.23	3091
3115	Цинкограф	7316.24	3091
3116	Цинкојеткач	7316.25	3091
3117	Израђивачи предмета од дрвета и других природних материјала ручним алатом 	7317	3023
3118	Занатлија за израду предмета од дрвета	7317.01	3117
3119	Занатлија за израду предмета од камена	7317.02	3117
3120	Занатлија за израду предмета од папира	7317.03	3117
3121	Занатлија за израду предмета од плуте	7317.04	3117
3122	Израђивач дрвене обуће	7317.05	3117
3123	Израђивач дрвених играчака 	7317.06	3117
3124	Израђивач корпи од прућа 	7317.07	3117
3125	Израђивач намештаја од прућа/трске 	7317.08	3117
3126	Ролетнар	7317.09	3117
3127	Четкар-метлар	7317.10	3117
3128	Израђивачи и обрађивачи текстила, коже и сродних материјала ручним алатом 	7318	3023
3129	Бојаџија 	7318.01	3128
3130	Вуновлачар 	7318.02	3128
3131	Импрегнатор текстила 	7318.03	3128
3132	Ручни израђивач предмета од коже 	7318.04	3128
3133	Ручни израђивач предмета од текстила 	7318.05	3128
3134	Ручни плетач	7318.06	3128
3135	Сарач/седлар 	7318.07	3128
3136	Ткач на разбоју – израда тепиха 	7318.08	3128
3137	Ткач на разбоју – израда тканина	7318.09	3128
3138	Ткач предмета од вуне/свиле (ручно)	7318.10	3128
3139	Ткач тепиха (ручно)	7318.11	3128
3140	Уметнички штопер текстила 	7318.12	3128
3141	Чипкар	7318.13	3128
3142	Уметничке занатлије ручним алатом неразврстане на другом месту 	7319	3023
3143	Израђивач металних играчака 	7319.01	3142
3144	Ручни израђивач предмета од камена 	7319.02	3142
3145	Свећар/воскар 	7319.03	3142
3146	Штампари и сродни	732	3022
3147	Занимања припреме за штампу	7321	3146
3148	Графички монтажер	7321.01	3147
3149	Графичко-технички уредник	7321.02	3147
3150	Радник на електронској припреми за штампу	7321.03	3147
3151	Фото-литограф 	7321.04	3147
3152	Фото-слагач	7321.05	3147
3153	Штампарски монтер-кописта	7321.06	3147
3154	Штампари 	7322	3146
3155	Машиниста равне штампе 	7322.01	3154
3156	Пословођа у штампарији 	7322.02	3154
3157	Послужилац графичких машина	7322.03	3154
3158	Ситоштампар 	7322.04	3154
3159	Штампар високе штампе	7322.05	3154
3160	Штампар дубоке штампе	7322.06	3154
3161	Штампар пропусне штампе	7322.07	3154
3162	Штампар равне штампе – офсет	7322.08	3154
3163	Дорађивачи штампе и књиговесци	7323	3146
3164	Бигер	7323.01	3163
3165	Графички радник за дораду и прераду	7323.02	3163
3167	Књиговезац декоратер корица 	7323.04	3163
3168	Књиговезац прошивач	7323.05	3163
3169	Помоћник књиговесца	7323.06	3163
3170	Помоћник машинисте графичке обраде и прераде	7323.07	3163
3171	Радник на изради адинг и термо ролне	7323.08	3163
3172	Радник на изради регистратора	7323.09	3163
3173	Радник на коричењу књига	7323.10	3163
3174	Радник на спиралном повезу	7323.11	3163
3175	Резач папира	7323.12	3163
3176	Електричари и електроничари 	74	2760
3177	Инсталатери и сервисери електричне опреме	741	3176
3178	Електричари и електроинсталатери у пословним објектима	7411	3177
3179	Грађевински електроинсталатер	7411.01	3178
3180	Електричар 	7411.02	3178
3181	Електроинсталатер	7411.03	3178
3182	Електромеханичари и електромонтери 	7412	3177
3183	Ауто-електричар	7412.01	3182
3184	Бродоелектричар	7412.02	3182
3185	Електричар у производном погону 	7412.03	3182
3186	Електромеханичар	7412.04	3182
3187	Електромеханичар за разводне уређаје	7412.05	3182
3188	Електромеханичар за хлађење и климатизацију	7412.06	3182
3189	Електромеханичар лифтова 	7412.07	3182
3190	Електромеханичар погона	7412.08	3182
3191	Електромеханичар-оружар	7412.09	3182
3192	Електромонтер енергетских машина и уређаја	7412.10	3182
3193	Железнички електроенергетичар	7412.11	3182
3194	Механичар електричне сигналне опреме / електричних трансформатора / електроинструмената	7412.12	3182
3195	Механичар електромотора/генератора	7412.13	3182
3196	Механичар за ремонт енергетских постројења	7412.14	3182
3197	Механичар шинских возила 	7412.15	3182
3198	Монтер електрогенератора 	7412.16	3182
3199	Помоћник електромонтера енергетских машина и уређаја	7412.17	3182
3200	Пословођа сервиса апарата за домаћинство 	7412.18	3182
3201	Сервисер електричних лифтова и сродне опреме	7412.19	3182
3202	Сервисер електроопреме	7412.20	3182
3203	Сервисер опреме производних машина и погона	7412.21	3182
3204	Инсталатери и сервисери електричних инсталација и мрежа	7413	3177
3205	Електромонтер инсталација и мрежа	7413.01	3204
3206	Помоћник електромонтера инсталација и мрежа	7413.02	3204
3207	Радник на далеководима	7413.03	3204
3208	Сервисер електромреже 	7413.04	3204
3209	Инсталатери и сервисери електронских и телекомуникационих инсталација и уређаја	742	3176
3210	Механичари и сервисери електронских машина, инструмената и опреме	7421	3209
3211	Авио-електроничар	7421.01	3210
3212	Механичар електронске индустријске опреме 	7421.02	3210
3213	Механичар електронске метеоролошке опреме	7421.03	3210
3214	Механичар електронских сигналних система (радија и радара)	7421.04	3210
3215	Механичар за банкомате 	7421.05	3210
3216	Механичар за биротехничке машине	7421.06	3210
3217	Механичар за медицинску и лабораторијску опрему	7421.07	3210
3218	Сервисер гасних пећи 	7421.08	3210
3219	Сервисер електронске опреме 	7421.09	3210
3220	Инсталатери и сервисери информационо-комуникационих уређаја	7422	3209
3221	Инсталатер рачунарске опреме 	7422.01	3220
3222	Инсталатер хардвера 	7422.02	3220
3223	Монтер телекомуникационих каблова 	7422.03	3220
3224	Сервисер аудио и видео технике 	7422.04	3220
3225	Сервисер мобилних телефона	7422.05	3220
3226	Сервисер рачунара и рачунарске опреме	7422.06	3220
3227	Сервисер сигурносних уређаја	7422.07	3220
3228	Прерађивачи прехрамбених производа, дрвета, текстила и друга занатска занимања 	75	2760
3229	Прерађивачи прехрамбених производа и сродни	751	3228
3230	Месари и сродни	7511	3229
3231	Дерач коже ручним алатима	7511.01	3230
3232	Кланичар	7511.02	3230
3233	Класер меса	7511.03	3230
3234	Кобасичар	7511.04	3230
3235	Месар	7511.05	3230
3236	Радник на маринирању меса/рибе	7511.06	3230
3237	Радник на преради изнутрица	7511.07	3230
3238	Саламурер меса/рибе	7511.08	3230
3239	Секач меса/рибе	7511.09	3230
3240	Сушилац меса/рибе 	7511.10	3230
3241	Чистач рибе	7511.11	3230
3242	Пекари, посластичари и сродни	7512	3229
3243	Бомбонџија	7512.01	3242
3244	Бурегџија	7512.02	3242
3245	Израђивач бисквита/колача	7512.03	3242
3246	Израђивач кора	7512.04	3242
3247	Израђивач сладоледа	7512.05	3242
3248	Израђивач чоколаде	7512.06	3242
3249	Пекар	7512.07	3242
3250	Пица мајстор 	7512.08	3242
3251	Помоћник посластичара	7512.09	3242
3252	Посластичар	7512.10	3242
3253	Пресер тестенина	7512.11	3242
3254	Пресер чоколаде	7512.12	3242
3255	Тестенинар	7512.13	3242
3256	Произвођачи млечних производа 	7513	3229
3257	Проивођач сира 	7513.01	3256
3258	Произвођач маслаца 	7513.02	3256
3259	Произвођач млечних производа	7513.03	3256
3260	Преређивачи воћа и поврћа и сродни 	7514	3229
3261	Пасирер воћа/поврћа	7514.01	3260
3262	Припремач воћа/поврћа за конзервирање	7514.02	3260
3263	Произвођач ајвара 	7514.03	3260
3264	Произвођач алкохолних пића традиционалним методама	7514.04	3260
3265	Произвођач воћних сокова традиционалним методама	7514.05	3260
3266	Произвођач маслиновог уља 	7514.06	3260
3267	Произвођач туршије 	7514.07	3260
3268	Произвођач џема 	7514.08	3260
3269	Сортирер воћа/поврћа	7514.09	3260
3270	Дегустатори и оцењивачи хране и пића 	7515	3229
3271	Дегустатор вина 	7515.01	3270
3272	Дегустатор пива 	7515.02	3270
3273	Дегустатор пића 	7515.03	3270
3274	Дегустатор хране 	7515.04	3270
3275	Прерађивачи дувана и произвођачи дуванских производа	7516	3229
3276	Оцењивач дувана	7516.01	3275
3277	Резач дувана	7516.02	3275
3278	Сортирер дувана	7516.03	3275
3279	Сушилац дувана	7516.04	3275
3280	Обрађивачи дрвета, столари и сродни 	752	3228
3281	Занимања примарне обраде дрвета 	7521	3280
3282	Гатериста дрвета	7521.01	3281
3283	Импрегнатор дрвета	7521.02	3281
3284	Параличар дрвета 	7521.03	3281
3285	Хидротермичар дрвета	7521.04	3281
3286	Столари и сродни	7522	3280
3287	Бачвар 	7522.01	3286
3288	Бродостолар	7522.02	3286
3289	Градитељ дрвених шасија/рамова возила	7522.03	3286
3290	Дрвомоделар	7522.04	3286
3291	Дрворезбар	7522.05	3286
3292	Израђивач дрвене галантерије	7522.06	3286
3293	Израђивач дрвених музичких инструмената	7522.07	3286
3294	Израђивач елемената монтажних кућа	7522.08	3286
3295	Израђивач спортске опреме од дрвета	7522.09	3286
3296	Интарзиста	7522.10	3286
3297	Колар 	7522.11	3286
3298	Ливачки дрвомоделар	7522.12	3286
3299	Помоћник столара 	7522.13	3286
3300	Савијач дрвета	7522.14	3286
3301	Столар 	7522.15	3286
3302	Столар за декор и уметничке предмете од дрвета 	7522.16	3286
3303	Столар за израду намештаја 	7522.17	3286
3304	Столар за израду шаблона и прототипова 	7522.18	3286
3305	Столар за монтажу, оправке и одржавање	7522.19	3286
3306	Финализер дрвеног намештаја	7522.20	3286
3307	Фурнирач	7522.21	3286
3308	Оператери и подешавачи машина за обраду дрвета 	7523	3280
3309	Дрвобрусач	7523.01	3308
3310	Оператер на машини за површинску обраду дрвета	7523.02	3308
3311	Оператер на машини за прецизну обраду дрвета 	7523.03	3308
3312	Оператер на машини за резбарење дрвета 	7523.04	3308
3313	Руковалац машином за глачање дрвета	7523.05	3308
3314	Руковалац машином за савијање дрвета	7523.06	3308
3315	Руковалац рендетом за дрво	7523.07	3308
3316	Руковалац тестером/циркуларом за дрво	7523.08	3308
3317	Произвођачи одеће и сродни	753	3228
3318	Кројачи, крзнари и шеширџије 	7531	3317
3319	Власуљар	7531.01	3318
3320	Израђивач народне ношње	7531.02	3318
3321	Израђивач одеће	7531.03	3318
3322	Израђивач позорошних костима 	7531.04	3318
3323	Израђивач рубља/корсета	7531.05	3318
3324	Крзнар	7531.06	3318
3325	Крзнарски помоћник	7531.07	3318
3326	Кројач 	7531.08	3318
3327	Кројач крзна	7531.09	3318
3328	Кројач по мери	7531.10	3318
3329	Кројач поправљач	7531.11	3318
3330	Помоћни кројач 	7531.12	3318
3331	Ћурчија	7531.13	3318
3332	Шеширџија 	7531.14	3318
3333	Моделари, шаблонери и резачи одеће и сродни 	7532	3317
3334	Моделар одеће 	7532.01	3333
3335	Моделар производа од крзна 	7532.02	3333
3336	Резач крзна 	7532.03	3333
3337	Резач производа од текстила/коже 	7532.04	3333
3338	Тапетарски шаблонер	7532.05	3333
3339	Текстилски цртач	7532.06	3333
3340	Шаблонер текстила/коже	7532.07	3333
3341	Шивачи, везиље и сродни	7533	3317
3342	Везиља	7533.01	3341
3343	Израђивач кишобрана 	7533.02	3341
3344	Израђивач меких (пуњених) играчака	7533.03	3341
3345	Тапетарски шивач	7533.04	3341
3346	Шивач 	7533.05	3341
3347	Шивач коже/крзна	7533.06	3341
3348	Шивач одеће	7533.07	3341
3349	Шивач шатора/једара/постељине	7533.08	3341
3350	Тапетари и сродни 	7534	3317
3351	Израђивач душека 	7534.01	3350
3352	Помоћник тапетара	7534.02	3350
3353	Тапетар 	7534.03	3350
3354	Тапетар-декоратер	7534.04	3350
3355	Тапетар за возила 	7534.05	3350
3356	Тапетар за намештај 	7534.06	3350
3357	Тапетар ортопедских помагала	7534.07	3350
3358	Штављачи и обрађивачи коже и крзна	7535	3317
3359	Белилац коже 	7535.01	3358
3360	Бојилац коже/крзна	7535.02	3358
3361	Импрегнатор коже	7535.03	3358
3362	Класер коже 	7535.04	3358
3363	Кожар	7535.05	3358
3364	Полирер коже	7535.06	3358
3365	Прерађивач коже/крзна	7535.07	3358
3366	Равнач крзна	7535.08	3358
3367	Растезач коже 	7535.09	3358
3368	Шишач крзна 	7535.10	3358
3369	Штавилац коже 	7535.11	3358
3370	Обућари, кожни галантеристи и сродни	7536	3317
3371	Израђивач амова / бичева / коњских оглава	7536.01	3370
3372	Израђивач кожне галантерије	7536.02	3370
3373	Калупар обуће	7536.03	3370
3374	Кожарски галантериста	7536.04	3370
3375	Кројач делова обуће	7536.05	3370
3376	Лепилац делова обуће	7536.06	3370
3377	Моделар кожне галантерије	7536.07	3370
3378	Моделар обуће	7536.08	3370
3379	Обућар	7536.09	3370
3380	Обућар израђивач обуће по мери	7536.10	3370
3381	Обућар за ортопедску обућу 	7536.11	3370
3382	Обућар за поправке	7536.12	3370
3383	Обућар за спортску обућу 	7536.13	3370
3384	Опанчар	7536.14	3370
3385	Помоћник израђивача кожне галантерије	7536.15	3370
3386	Помоћник обућара 	7536.16	3370
3387	Пресер/заобљивач ђонова	7536.17	3370
3388	Ременар	7536.18	3370
3389	Рукавичар	7536.19	3370
3390	Ташнер 	7536.20	3370
3391	Шаблонер обуће	7536.21	3370
3392	Шивач делова обуће	7536.22	3370
3393	Занимања сродна занатским 	754	3228
3394	Рониоци 	7541	3393
3395	Кесонац	7541.01	3394
3396	Монтер ронилац	7541.02	3394
3397	Ронилац 	7541.03	3394
3398	Ронилац спасилац 	7541.04	3394
3399	Минери 	7542	3393
3400	Грађевински минер	7542.01	3399
3401	Минер	7542.02	3399
3402	Помоћник минера	7542.03	3399
3403	Рударски минер 	7542.04	3399
3404	Испитивачи квалитета и оцењивачи производа, осим хране и пића	7543	3393
3405	Инспектор за контролу квалитета производа	7543.01	3404
3406	Класер вуне 	7543.02	3404
3407	Класер коже/крзна	7543.03	3404
3408	Класер обуће	7543.04	3404
3409	Класер текстилних влакана	7543.05	3404
3410	Контролор квалитета непрехрамбених производа	7543.06	3404
3411	Оцењивач крзна	7543.07	3404
3412	Оцењивач производа 	7543.08	3404
3413	Сортирер обуће	7543.09	3404
3414	Запрашивачи и сузбијачи штеточина и корова 	7544	3393
3415	Дезинсектор	7544.01	3414
3416	Дезинфектор	7544.02	3414
3417	Дератизер 	7544.03	3414
3418	Запрашивач корова 	7544.04	3414
3419	Сузбијач штеточина 	7544.05	3414
3420	Занимања сродна занатским неразврстана на другом месту	7549	3393
3421	Аранжер цвећа 	7549.01	3420
3422	Израђивач мрежа (рибарских/заштитних и сл.)	7549.02	3420
3423	Израђивач трака	7549.03	3420
3424	Калупар оптичких сочива	7549.04	3420
3425	Кукичар 	7549.05	3420
3426	Оптички моделар 	7549.06	3420
3427	РУКОВАОЦИ МАШИНАМА И ПОСТРОЈЕЊИМА, МОНТЕРИ И ВОЗАЧИ	8	\N
3969	Возач трамваја	8331.03	3966
3428	Руковаоци стабилним машинама и постројењима	81	3427
3429	Руковаоци рударским и постројењима за прераду минерала 	811	3428
3430	Рудари и руковаоци постројењима у рудницима и каменоломима	8111	3429
3431	Бушач на површинском копу	8111.01	3430
3432	Јамски бушач	8111.02	3430
3433	Јамски копач	8111.03	3430
3434	Јамски подграђивач	8111.04	3430
3435	Подграђивач у каменолому	8111.05	3430
3436	Помоћник рудара у површинској експлоатацији	8111.06	3430
3437	Помоћник рудара у подземној експлоатацији	8111.07	3430
3438	Послужилац постројења површинске експлоатације	8111.08	3430
3439	Послужилац постројења подземне експлоатације	8111.09	3430
3440	Припремач узорака руда и концентрата	8111.10	3430
3441	Радник у каменолому 	8111.11	3430
3442	Рудар површинске експлоатације 	8111.12	3430
3443	Рудар подземне експлоатације	8111.13	3430
3444	Руковалац багером цикличног деловања	8111.14	3430
3445	Руковалац грејдером	8111.15	3430
3446	Руковалац јамском утоварном механизацијом	8111.16	3430
3447	Руковалац једноставном рударском механизацијом	8111.17	3430
3448	Руковалац одлагачем	8111.18	3430
3449	Руковалац постројењем у каменолому 	8111.19	3430
3450	Руковалац рударским конвејером 	8111.20	3430
3451	Руковалац рударским транспортним постројењима	8111.21	3430
3452	Руковалац рударском пумпном станицом 	8111.22	3430
3453	Трасант	8111.23	3430
3454	Руковаоци постројењима за прераду минерала и камена 	8112	3429
3455	Послужилац тријажера руде	8112.01	3454
3456	Пржионичар руде	8112.02	3454
3457	Руковалац машинама за производњу грађевинског материјала	8112.03	3454
3458	Руковалац машином за испирање злата 	8112.04	3454
3459	Руковалац машином за испирање угља	8112.05	3454
3460	Руковалац машином за прераду неметала	8112.06	3454
3461	Руковалац машином за уситњавање минералних сировина	8112.07	3454
3462	Руковалац уређајем за припрему и оплемењивање минералних сировина	8112.08	3454
3463	Сепаратер руде/сребра/злата	8112.09	3454
3464	Сушионичар руде	8112.10	3454
3465	Флотер руде	8112.11	3454
3466	Руковаоци опремом за дубинско бушење и сродни 	8113	3429
3467	Геобушач	8113.01	3466
3468	Оператер геобушења	8113.02	3466
3469	Помоћник геобушача	8113.03	3466
3470	Руковалац гарнитуром за бушење бунара 	8113.04	3466
3471	Руковалац опремом за дубинско бушење (нафте и гаса) 	8113.05	3466
3472	Руковалац опремом за експлоатационо бушење на површинским коповима и јами 	8113.06	3466
3473	Руковаоци машинама у производњи и обради цемента, камена и других неметала 	8114	3429
3474	Пекач клинкера	8114.01	3473
3475	Послужилац постројења за производњу неметалних минерала	8114.02	3473
3476	Руковалац машином за обликовање камена 	8114.03	3473
3477	Руковалац машином за обраду бетона 	8114.04	3473
3478	Руковалац машином за полирање камена 	8114.05	3473
3479	Руковалац машином за производњу бетона 	8114.06	3473
3480	Руковалац машином за производњу бетонских префабриката	8114.07	3473
3481	Руковалац машином за производњу грађевинских елемената	8114.08	3473
3482	Руковаоци металуршким пећима и машинама и уређајима за дораду метала	812	3428
3483	Руковаоци металуршким пећима и сродни 	8121	3482
3484	Калионичар 	8121.01	3483
3485	Ливац алуминијумског лива	8121.02	3483
3486	Ливац сивог лива 	8121.03	3483
3487	Ливац челичног лива	8121.04	3483
3488	Машиски калупар 	8121.05	3483
3489	Послужилац машине за обраду метала пескирењем	8121.06	3483
3490	Послужилац постројења за калуповање	8121.07	3483
3491	Послужилац постројења за кокилни и калупни лив	8121.08	3483
3492	Послужилац постројења за ливење обојених метала	8121.09	3483
3493	Послужилац постројења за ливење прецизног лива	8121.10	3483
3494	Послужилац постројења за обраду одливака	8121.11	3483
3495	Послужилац постројења за прераду олова	8121.12	3483
3496	Послужилац постројења за термичку обраду метала	8121.13	3483
3497	Послужилац постројења за топљење сивог и челичног лива	8121.14	3483
3498	Послужилац постројења за центрифугални лив	8121.15	3483
3499	Пресер метала на екструдеру	8121.16	3483
3500	Припремач металуршких узорака	8121.17	3483
3501	Рафинер метала	8121.18	3483
3502	Руковалац постројењем за прераду бакра и алуминијума ваљањем и ковањем	8121.19	3483
3503	Руковалац постројењем за прераду бакра и алуминијума пресовањем и извлачењем	8121.20	3483
3504	Руковалац постројењем за прераду бакра и алуминијума топљењем и ливењем	8121.21	3483
3505	Руковалац постројењем за производњу челика	8121.22	3483
3506	Руковалац постројењем за производњу челичних цеви	8121.23	3483
3507	Руковалац постројењем за топло ваљање челика	8121.24	3483
3508	Руковалац постројењем за хладно ваљање и вучење челика	8121.25	3483
3509	Термообрађивач метала	8121.26	3483
3510	Топионичар за сиви лив 	8121.27	3483
3511	Топионичар за челични лив	8121.28	3483
3512	Руковаоци машинама и уређајима за дораду метала 	8122	3482
3513	Галванизер	8122.01	3512
3514	Електролизер-анодаш 	8122.02	3512
3515	Емајлирац	8122.03	3512
3516	Израђивач акумулатора	8122.04	3512
3517	Израђивач батерија	8122.05	3512
3518	Израђивач кабловског прибора и прикључака	8122.06	3512
3519	Метализер	8122.07	3512
3520	Оператер чистилац металне опреме пешчаним млазом	8122.08	3512
3521	Помоћник галванизера	8122.09	3512
3522	Поцинковач-шерардирер	8122.10	3512
3523	Руковалац машином за завршну обраду метала 	8122.11	3512
3524	Руковалац машином за облагање метала/жице	8122.12	3512
3525	Руковалац машином за одмашћивање метала	8122.13	3512
3526	Руковалац машином за полирање метала 	8122.14	3512
3527	Руковалац машином за премазивање метала 	8122.15	3512
3528	Руковалац машином за распрскивање метала	8122.16	3512
3529	Руковалац постројењем за дораду челичних производа	8122.17	3512
3530	Руковалац постројењем за електролизу бакра	8122.18	3512
3531	Руковалац постројењем за производњу каблова и проводника	8122.19	3512
3532	Руковаоци машинама и уређајима за производњу хемијских производа и фотоматеријала	813	3428
3533	Руковаоци машинама и уређајима за производњу хемијских производа	8131	3532
3534	Оператер гранулације фармацеутских тоалетних производа	8131.01	3533
3535	Оператер дестилације парфема	8131.02	3533
3536	Оператер електролитским ћелијама у производњи хемикалија	8131.03	3533
3537	Оператер на просејавању хемикалија	8131.04	3533
3538	Оператер уређаја за дестилацију	8131.05	3533
3539	Пиротехничар	8131.06	3533
3540	Руковалац аутоклавом у хемијској производњи	8131.07	3533
3541	Руковалац вакуум лонцима у хемијској производњи (осим нафте и природног гаса)	8131.08	3533
3542	Руковалац дехидратором у хемијској производњи	8131.09	3533
3543	Руковалац екстрактором у хемијској производњи	8131.10	3533
3544	Руковалац калцинатором у хемијској производњи	8131.11	3533
3545	Руковалац конвертором у хемијској производњи (осим нафте и природног гаса)	8131.12	3533
3546	Руковалац котловима/казанима у хемијској производњи	8131.13	3533
3547	Руковалац машинама и уређајима за производњу шибица 	8131.14	3533
3548	Руковалац машином за брушење стакла	8131.15	3533
3549	Руковалац машином за гравирање стакла 	8131.16	3533
3550	Руковалац машином за израду експлозива	8131.17	3533
3551	Руковалац машином за израду муниције	8131.18	3533
3552	Руковалац машином за мешање хемикалија	8131.19	3533
3553	Руковалац машином за производњу детерџената 	8131.20	3533
3554	Руковалац машином за производњу линолеума и сличних материјала	8131.21	3533
3555	Руковалац машином за производњу оловака	8131.22	3533
3556	Руковалац машином за производњу синтетичких влакана	8131.23	3533
3557	Руковалац машином за производњу средстава за хигијену 	8131.24	3533
3558	Руковалац машином за производњу тоалетних/козметичких производа 	8131.25	3533
3559	Руковалац машином за производњу фармацеутских производа	8131.26	3533
3560	Руковалац машином за производњу халоген/хидроген/хлорног гаса	8131.27	3533
3561	Руковалац машином за производњу хемикалија за хигијену	8131.28	3533
3562	Руковалац машином за синтезу хемикалија	8131.29	3533
3563	Руковалац машином за уситњавање/дробљење/млевење/пулверзацију хемикалија	8131.30	3533
3564	Руковалац машином у производњи ћумура	8131.31	3533
3565	Руковалац машином у хемијској производњи	8131.32	3533
3566	Руковалац отпаривачем у хемијској производњи (осим нафте и природног гаса)	8131.33	3533
3567	Руковалац пећи у производњи кокса	8131.34	3533
3568	Руковалац реактором у хемијској производњи (осим нафте и природног гаса)	8131.35	3533
3569	Руковалац ретортом за добијање угљеног гаса	8131.36	3533
3570	Руковалац ретортом у хемијској производњи	8131.37	3533
3571	Руковалац сепаратором у хемијској производњи	8131.38	3533
3572	Руковалац филтер-пресом у хемијској производњи 	8131.39	3533
3573	Руковалац машином за производњу свећа	8131.40	3533
3574	Руковаоци машинама и уређајима за производњу фотоматеријала и израду фотографија	8132	3532
3575	Развијач фотографских филмова 	8132.01	3574
3576	Руковалац машином за израду фотографија 	8132.02	3574
3577	Руковалац машином за производњу фотографских плоча 	8132.03	3574
3578	Руковалац машином за производњу фото-филмова 	8132.04	3574
3579	Руковалац машином за увећање фотографија 	8132.05	3574
3580	Руковалац машином за штампање фотографија 	8132.06	3574
3581	Руковалац машином за производњу фотографског папира 	8132.07	3574
3582	Фото-лаборант	8132.08	3574
3583	Руковаоци машинама за производњу и прераду гуме, пластике и папира	814	3428
3584	Руковаоци машинама за производњу и прераду гуме	8141	3583
3585	Вулканизер 	8141.01	3584
3586	Гумарски моделар 	8141.02	3584
3587	Конфекционар гумарских производа	8141.03	3584
3588	Оператер гумар каландриста	8141.04	3584
3589	Помоћник вулканизера	8141.05	3584
3590	Припремач гумарских полупроизода и производа	8141.06	3584
3591	Руковалац глодалицом за гуму 	8141.07	3584
3592	Руковалац машином за истискивање гуме	8141.08	3584
3593	Руковалац машином за облагање гумом	8141.09	3584
3594	Руковалац машином за пресовање гуме	8141.10	3584
3595	Руковалац машином за производњу гуме	8141.11	3584
3596	Руковалац машином за производњу гумених каблова 	8141.12	3584
3597	Руковалац машином за производњу печата од гуме	8141.13	3584
3598	Руковалац машином за производњу пнеуматика 	8141.14	3584
3599	Руковалац машином за рељефирање гуме	8141.15	3584
3600	Руковаоци машинама за производњу и прераду пластике	8142	3583
3601	Израђивач пластичних чамаца 	8142.01	3600
3602	Моделар пластике 	8142.02	3600
3603	Оператер на машини за надувавање пластичних боца 	8142.03	3600
3604	Руковалац машином за бушење пластике	8142.04	3600
3605	Руковалац машином за ваљање пластике	8142.05	3600
3606	Руковалац машином за ецовање пластике	8142.06	3600
3607	Руковалац машином за истискивање пластике	8142.07	3600
3608	Руковалац машином за калупљење пластике	8142.08	3600
3609	Руковалац машином за ливење пластичних маса бризгањем	8142.09	3600
3610	Руковалац машином за млевење пластике	8142.10	3600
3611	Руковалац машином за облагање пластиком	8142.11	3600
3612	Руковалац машином за прераду полимера	8142.12	3600
3613	Руковалац машином за производњу пластике	8142.13	3600
3614	Руковалац машином за сабијање/пресовање пластичних маса	8142.14	3600
3615	Руковалац машином за сечење пластике	8142.15	3600
3616	Руковалац машином за финално обликовање пластике	8142.16	3600
3617	Руковалац машином и уређајем за производњу оптичких влакана 	8142.17	3600
3618	Руковалац пресом за пластични ламинат 	8142.18	3600
3619	Руковаоци машинама за производњу предмета од папира	8143	3583
3620	Картонажер	8143.01	3619
3621	Моделар предмета од папирне каше	8143.02	3619
3622	Оператер машином за лепљење у производњи папира	8143.03	3619
3623	Оператер машином за рељефно украшавање папира	8143.04	3619
3624	Оператер машином за савијање папирних производа	8143.05	3619
3625	Оператер машином за сечење папирних производа	8143.06	3619
3626	Помоћник картонажера	8143.07	3619
3627	Руковалац машином за производе од папира 	8143.08	3619
3628	Руковалац машином за производњу папирних коверата и кеса	8143.09	3619
3629	Руковалац машином за производњу папирних кутија 	8143.10	3619
3630	Руковаоци машинама за производњу текстила, обраду коже и крзна и сродни	815	3428
3631	Руковаоци машинама за припрему влакана, предење и намотавање	8151	3630
3632	Машински прелац	8151.01	3631
3633	Оператер машином за мешање текстилних влакана 	8151.02	3631
3634	Оператер машином за обраду синтетичких влакана	8151.03	3631
3635	Оператер машином за сортирање текстилних влакана	8151.04	3631
3636	Оператер машином за удвајање текстилних предива 	8151.05	3631
3637	Оператер машином за чешљање текстилних влакана 	8151.06	3631
3638	Руковалац машином за намотавање влакана 	8151.07	3631
3639	Руковалац машином за предење влакана 	8151.08	3631
3640	Руковалац машином за припрему влакана 	8151.09	3631
3641	Скидач ткачког калема	8151.10	3631
3642	Руковаоци индустријским машинама за ткање и плетење	8152	3630
3643	Бушач жакард картица	8152.01	3642
3644	Оператер плетења	8152.02	3642
3645	Оператер ткања	8152.03	3642
3646	Помоћник руковаоца индустријским машинама за ткање и плетење	8152.04	3642
3647	Послужилац разбоја	8152.05	3642
3648	Руковалац жакард разбојем	8152.06	3642
3649	Руковалац машином за везење	8152.07	3642
3650	Руковалац машином за кукичање	8152.08	3642
3651	Руковалац машином за мотање предива са калема	8152.09	3642
3652	Руковалац машином за плетење чарапа/одеће	8152.10	3642
3653	Руковалац машином за производњу мрежа	8152.11	3642
3654	Руковалац машином за ткање тепиха 	8152.12	3642
3655	Руковалац машином за ткање чипке 	8152.13	3642
3656	Сновач	8152.14	3642
3657	Ужар 	8152.15	3642
3658	Руковаоци машинама за шивење у индустрији 	8153	3630
3659	Руковалац машином за производњу шешира и капа 	8153.01	3658
3660	Руковалац машином за шивење мебла 	8153.02	3658
3661	Шивач кожне одеће (индустријски)	8153.03	3658
3662	Шивач конфекције	8153.04	3658
3663	Шивач текстила	8153.05	3658
3664	Руковаоци машинама за дораду предива и текстила	8154	3630
3665	Бојадисер текстила	8154.01	3664
3666	Оплемењивач текстила	8154.02	3664
3667	Помоћник руковаоца машинама за дораду предива и текстила 	8154.03	3664
3668	Руковалац ваљком за текстил	8154.04	3664
3669	Руковалац машином за декатирање тканина	8154.05	3664
3670	Руковалац машином за бељење текстила 	8154.06	3664
3671	Руковалац машином за ваљање текстила	8154.07	3664
3672	Руковалац машином за дегумирање свиле 	8154.08	3664
3673	Руковалац машином за импрегнирање текстила	8154.09	3664
3674	Руковалац машином за оплемењивање свиле	8154.10	3664
3675	Руковаоци машинама за припрему и обраду коже и крзна	8155	3630
3676	Руковалац машином за бојење коже 	8155.01	3675
3677	Руковалац машином за дораду коже/крзна	8155.02	3675
3678	Руковалац машином за извлачење крзна	8155.03	3675
3679	Руковалац машином за прераду коже/крзна 	8155.04	3675
3680	Руковалац машином за производњу филца	8155.05	3675
3681	Руковалац машином за сечење коже 	8155.06	3675
3682	Руковалац машином за штављење коже 	8155.07	3675
3683	Руковаоци машинама за производњу обуће и кожне галантерије	8156	3630
3684	Израђивач горњих делова обуће	8156.01	3683
3685	Израђивач доњих делова обуће	8156.02	3683
3686	Оператер за компјутерско кројење обуће	8156.03	3683
3687	Руковалац машином за кројење обуће 	8156.04	3683
3688	Руковалац машином за производњу обуће 	8156.05	3683
3689	Руковалац машином за производњу ортопедске обуће 	8156.06	3683
3690	Руковалац машином за производњу спортске обуће 	8156.07	3683
3691	Руковалац машином за шивење обуће 	8156.08	3683
3692	Руковалац машином за производњу кожне галантерије 	8156.09	3683
3693	Руковаоци машинама за услужно прање и пеглање веша	8157	3630
3694	Пеглер 	8157.01	3693
3695	Руковалац машином за прање и хемијско чишћење	8157.02	3693
3696	Руковалац машином за хемијско чишћење	8157.03	3693
3697	Руковаоци машинама за производњу текстила, обраду коже и крзна и сродна занимања неразврстанa на другом месту	8159	3630
3698	Конфекцијски кројач	8159.01	3697
3699	Конфекционар кожне и крзнене одеће	8159.02	3697
3700	Конфекционар текстила	8159.03	3697
3701	Моделар конфекције	8159.04	3697
3702	Помоћник конфекционара	8159.05	3697
3703	Руковалац машином за израду цртежа и шаблона за текстил/кожу/крзно	8159.06	3697
3704	Руковалац машином за позамантерију	8159.07	3697
3705	Руковалац машином за производњу шатора 	8159.08	3697
3706	Руковалац машином за резање текстила/коже 	8159.09	3697
3707	Руковаоци машинама за производњу прехрамбених и сродних производа	816	3428
3708	Руковаоци машинама за производњу прехрамбених и сродних производа	8160	3707
3709	Кондитор	8160.01	3708
3710	Кувар индустријских јела	8160.02	3708
3711	Млинар житарица	8160.03	3708
3712	Млинар зачина	8160.04	3708
3713	Руковалац аутоклавом за уље и масноће	8160.05	3708
3714	Руковалац пресом за уљарице	8160.06	3708
3715	Руковалац сепаратором у прехрамбеној производњи 	8160.07	3708
3716	Руковалац сушаром за житарице 	8160.08	3708
3717	Руковалац уређајем за дестилацију алкохолних пића	8160.09	3708
3718	Руковалац уређајем за дехидрирање прехрамбених производа	8160.10	3708
3719	Руковалац уређајем за дифузију у преради шећерне репе	8160.11	3708
3720	Руковалац уређајем за екстракцију намирница	8160.12	3708
3721	Руковалац уређајем за замрзавање намирница и производа	8160.13	3708
3722	Руковалац уређајем за израду млечних производа	8160.14	3708
3723	Руковалац уређајем за израду пекарских производа 	8160.15	3708
3724	Руковалац уређајем за конзервирање намирница и производа	8160.16	3708
3725	Руковалац уређајем за кристализацију шећера	8160.17	3708
3726	Руковалац уређајем за кување слада за производњу алкохолних пића	8160.18	3708
3727	Руковалац уређајем за натапање/клијање слада	8160.19	3708
3728	Руковалац уређајем за пастеризовање млечних производа	8160.20	3708
3729	Руковалац уређајем за прање намирница (воће, поврће, изнутрице)	8160.21	3708
3730	Руковалац уређајем за прераду воћа и поврћа	8160.22	3708
3731	Руковалац уређајем за прераду дувана	8160.23	3708
3732	Руковалац уређајем за прераду меса 	8160.24	3708
3733	Руковалац уређајем за прераду млека 	8160.25	3708
3734	Руковалац уређајем за прераду рибе 	8160.26	3708
3735	Руковалац уређајем за прераду соли	8160.27	3708
3736	Руковалац уређајем за пржење кафе/какаоа	8160.28	3708
3737	Руковалац уређајем за производњу алкохола, квасца и киселина 	8160.29	3708
3738	Руковалац уређајем за производњу алкохолних пића	8160.30	3708
3739	Руковалац уређајем за производњу безалкохолних пића	8160.31	3708
3740	Руковалац уређајем за производњу вина 	8160.32	3708
3741	Руковалац уређајем за производњу воде	8160.33	3708
3742	Руковалац уређајем за производњу и прераду адитива, зачина, чаја, кафе и кавовина	8160.34	3708
3743	Руковалац уређајем за производњу и прераду скроба 	8160.35	3708
3744	Руковалац уређајем за производњу јестивог уља и масноћа (уљар)	8160.36	3708
3745	Руковалац уређајем за производњу концентроване сточне хране	8160.37	3708
3746	Руковалац уређајем за производњу маргарина	8160.38	3708
3747	Руковалац уређајем за производњу пива (пивар)	8160.39	3708
3748	Руковалац уређајем за производњу сирћета	8160.40	3708
3749	Руковалац уређајем за производњу слаткиша 	8160.41	3708
3750	Руковалац уређајем за производњу соде 	8160.42	3708
3751	Руковалац уређајем за производњу сокова од воћа/поврћа	8160.43	3708
3752	Руковалац уређајем за производњу тестенина	8160.44	3708
3753	Руковалац уређајем за производњу цигарета	8160.45	3708
3754	Руковалац уређајем за производњу чоколаде	8160.46	3708
3755	Руковалац уређајем за производњу шећера	8160.47	3708
3756	Руковалац уређајем за стерилисање намирница	8160.49	3708
3757	Руковалац уређајем за сушење прехрамбених производа	8160.50	3708
3758	Руковалац уређајем за требљење/љушћење жита	8160.51	3708
3759	Руковалац уређајем за усољавање меса/рибе	8160.52	3708
3760	Руковалац уређајем за ферментацију алкохолних пића 	8160.53	3708
3761	Руковалац уређајем за хидрогенизацију уља и масноћа	8160.54	3708
3762	Руковаоци машинама и уређајима у процесу обраде и прераде дрвета и производње папирне масе и папира	817	3428
3763	Руковаоци машинама и уређајима у процесу производње папирне масе и папира	8171	3762
3764	Оператер бељења папира	8171.01	3763
3765	Оператер на суперваљку у производњи папира	8171.02	3763
3766	Оператер пресовања папира	8171.03	3763
3767	Оператер рафинисања папирне каше	8171.04	3763
3768	Оператер спајања папира	8171.05	3763
3769	Оператер уређаја за припрему папирне масе/пулпе	8171.06	3763
3770	Руковалац машином за апретирање хартије	8171.07	3763
3771	Руковалац машином за дрвену кашу	8171.08	3763
3772	Руковалац машином за производњу папира/картона 	8171.09	3763
3773	Руковалац машином за производњу целулозе	8171.10	3763
3774	Руковаоци машинама и уређајима за обраду и прераду дрвета 	8172	3762
3775	Дрвостругар (машински)	8172.01	3774
3776	Машински обрађивач за импрегнацију дрвета	8172.02	3774
3777	Облагач дрвених плоча	8172.03	3774
3778	Оператер на машинама за израду лаке амбалаже	8172.04	3774
3779	Оператер на машини за израду паркета	8172.05	3774
3780	Оператер у преради дрвета	8172.06	3774
3781	Оператер у стругари/пилани	8172.07	3774
3782	Оплемењивач дрвета	8172.08	3774
3783	Пескирер дрвета	8172.09	3774
3784	Помоћник руковаоца машинама и уређајима за обраду и прераду дрвета 	8172.10	3774
3785	Руковалац машином за бушење дрвета	8172.11	3774
3786	Руковалац машином за кројење резане грађе, елемената и плоча	8172.12	3774
3787	Руковалац машином за лепљење дрвета 	8172.13	3774
3788	Руковалац машином за обликовање дрвета	8172.14	3774
3789	Руковалац машином за обликовање фурнира	8172.15	3774
3790	Руковалац машином за постављање језгра иверица	8172.16	3774
3791	Руковалац машином за припрему обловине за обраду	8172.17	3774
3792	Руковалац машином за рендисање дрвета	8172.18	3774
3793	Руковалац машином за сечење дрвета 	8172.19	3774
3794	Руковалац машином за сушење дрвета	8172.20	3774
3795	Руковалац машином за уситњавање дрвета	8172.21	3774
3796	Руковалац машином за хидротермичку обраду дрвета	8172.22	3774
3797	Руковалац машинском тестером за дрво (гатер / циркулар / трачна пила / струг)	8172.23	3774
3798	Руковалац пресом за фурнир	8172.24	3774
3799	Руковалац стругом за фурнир	8172.25	3774
3800	Руковалац сушаром за дрво	8172.26	3774
3801	Сортирер елемената и готових производа	8172.27	3774
3802	Руковаоци осталим стабилним машинама и постројењима 	818	3428
3803	Руковаоци машинама и уређајима у процесу производње стакла и керамике	8181	3802
3804	Жарилац стакла	8181.01	3803
3805	Калилац стакла	8181.02	3803
3806	Ливац стакла (машински) 	8181.03	3803
3807	Оператер глазирања у циглани	8181.04	3803
3808	Оператер машинске линије обраде стакла	8181.05	3803
3809	Оператер сушења/печења опеке и црепа	8181.06	3803
3810	Оператер потапач стакла	8181.07	3803
3811	Руковалац калупом за опеку и порцелан	8181.08	3803
3812	Руковалац машином за бојење стакла/керамике	8181.09	3803
3813	Руковалац машином за бушење стакла/опеке	8181.10	3803
3814	Руковалац машином за глачање равног стакла	8181.11	3803
3815	Руковалац машином за гравирање стакла/опеке	8181.12	3803
3816	Руковалац машином за дораду стакла	8181.13	3803
3817	Руковалац машином за израду глазуре за керамику	8181.14	3803
3818	Руковалац машином за израду стаклених боца	8181.15	3803
3819	Руковалац машином за израду стаклених цеви/шипки	8181.16	3803
3820	Руковалац машином за истањивање стакла 	8181.17	3803
3821	Руковалац машином за калупљење у производњи стакла	8181.18	3803
3822	Руковалац машином за ливење опеке и порцелана	8181.19	3803
3823	Руковалац машином за мешање глинене масе 	8181.20	3803
3824	Руковалац машином за мешање стаклене масе 	8181.21	3803
3825	Руковалац машином за надувавање стакла 	8181.22	3803
3826	Руковалац машином за полирање стакла / стаклених сочива	8181.23	3803
3827	Руковалац машином за пресовање стакла 	8181.24	3803
3828	Руковалац машином за производњу опеке 	8181.25	3803
3829	Руковалац машином за производњу санитарне керамике 	8181.26	3803
3830	Руковалац машином за производњу стакла	8181.27	3803
3831	Руковалац машином за резање стакла	8181.28	3803
3832	Руковалац машином за савијање стакла	8181.29	3803
3833	Руковалац машином у ваљаоници равног стакла	8181.30	3803
3834	Руковалац опремом за пескарење стакла	8181.31	3803
3835	Руковалац пећи за сушење/печење грнчарије и порцелана	8181.32	3803
3836	Руковалац пећима за производњу керамике	8181.33	3803
3837	Руковалац пећима за производњу стакла 	8181.34	3803
3838	Руковалац пресом за истискивање глине 	8181.35	3803
3839	Руковалац пресом за опеку и порцелан	8181.36	3803
3840	Темперирер стакла/керамике	8181.37	3803
3841	Топилац стакла	8181.38	3803
3842	Руковаоци парним машинама – котловима и механичари 	8182	3802
3843	Машиниста гасне станице	8182.01	3842
3844	Машиниста за парне котлове	8182.02	3842
3845	Машиниста компресорске станице	8182.03	3842
3846	Машиниста термоенергетских постројења	8182.04	3842
3847	Механичар за гасоенергетска и пнеумоенергетска постројења	8182.05	3842
3848	Механичар за термоенергетска постројења	8182.06	3842
3849	Механичар за термотехничка постројења	8182.07	3842
3850	Механичар за турбине	8182.08	3842
3851	Механичар за хидроенергетска постројења 	8182.09	3842
3852	Оператер генераторске подстанице	8182.10	3842
3853	Оператер топлане	8182.11	3842
3854	Помоћник машинисте компресорске станице	8182.12	3842
3855	Руковалац мазутном станицом	8182.13	3842
3856	Руковалац парним котловима	8182.14	3842
3857	Руковалац парним турбинама 	8182.15	3842
3858	Руковалац терморегулационим уређајем	8182.16	3842
3859	Руковаоци машинама за паковање, флаширање и етикетирање	8183	3802
3860	Руковалац машином за етикетирање 	8183.01	3859
3861	Руковалац машином за паковање	8183.02	3859
3862	Руковалац машином за палетизацију	8183.03	3859
3863	Руковалац машином за печаћење	8183.04	3859
3864	Руковалац машином за пуњење боца под притиском – плинских 	8183.05	3859
3865	Руковалац машином за пуњење конзерви 	8183.06	3859
3866	Руковалац машином за флаширање	8183.07	3859
3867	Руковаоци осталим стабилним машинама и постројењима неразврстани на другом месту	8189	3802
3868	Оператер аутоматске инспекције електронских склопова X-зрацима	8189.01	3867
3869	Оператер аутоматске оптичке инспекције електронских склопова	8189.02	3867
3870	Оператер површинске монтаже електронских производа/склопова 	8189.03	3867
3871	Оператер производње електронских склопова машинским лемљењем	8189.04	3867
3872	Оператер производње електронских склопова ручним уметањем и лемљењем 	8189.05	3867
3873	Оператер производње електротехничких производа	8189.06	3867
3874	Оператер ручног улагања и лемљења електронских производа	8189.07	3867
3875	Руковалац машином за производњу компоненти за интегрисана електронска кола	8189.08	3867
3876	Руковалац машином за производњу силиконских чипова 	8189.09	3867
3877	Руковалац машином за спајање каблова и ужади 	8189.10	3867
3878	Хидрауличар бродске преводнице	8189.11	3867
3879	Монтери производа	82	3427
3880	Монтери производа	821	3879
3881	Монтери механичких машина	8211	3880
3882	Машински инсталатер	8211.01	3881
3883	Машински монтер	8211.02	3881
3884	Монтер авионских мотора	8211.03	3881
3885	Монтер алатних машина	8211.04	3881
3886	Монтер бродских мотора	8211.05	3881
3887	Монтер грађевинских и рударских машина	8211.06	3881
3888	Монтер машина за обраду дрвета	8211.07	3881
3889	Монтер машинских делова за аутомобиле и камионе 	8211.08	3881
3890	Монтер металних производа широке потрошње	8211.09	3881
3891	Монтер мотора моторних возила	8211.10	3881
3892	Монтер пољопривредних машина	8211.11	3881
3893	Монтер трупа авиона	8211.12	3881
3894	Монтер турбина 	8211.13	3881
3895	Монтер уређаја хидраулике и пнеуматике	8211.14	3881
3896	Монтер хидротехничких постројења	8211.15	3881
3897	Монтер шасија возила 	8211.16	3881
3898	Помоћник машинског инсталатера	8211.17	3881
3899	Помоћник машинског монтера	8211.18	3881
3900	Састављач котрљајућих лежаја	8211.19	3881
3901	Монтери електричне и електронске опреме	8212	3880
3902	Електромотач	8212.01	3901
3903	Монтер електричне опреме за аутомобиле и камионе 	8212.02	3901
3904	Монтер електричних апарата за домаћинство	8212.03	3901
3905	Монтер електричних машина и опреме	8212.04	3901
3906	Монтер електричних мерних инструмената и склопова	8212.05	3901
3907	Монтер електромеханичке опреме 	8212.06	3901
3908	Монтер електронских склопова	8212.07	3901
3909	Монтер електронских штампаних кола 	8212.08	3901
3910	Монтер опреме за мерење и контролу	8212.09	3901
3911	Монтер рачунарске опреме	8212.10	3901
3912	Монтер телевизора и радио-апарата 	8212.11	3901
3913	Монтер телекомуникационе опреме	8212.12	3901
3914	Монтер телекомуникационих водова	8212.13	3901
3915	Монтер телефонских апарата 	8212.14	3901
3916	Монтер часовника	8212.15	3901
3917	Оператер за монтажу телекомуникационих мрежа	8212.16	3901
3918	Помоћник монтера електричне опреме 	8212.17	3901
3919	Помоћник монтера електронске опреме	8212.18	3901
3920	Спајач електротехничких елемената	8212.19	3901
3921	Монтери производа неразврстани на другом месту	8219	3880
3922	Монтер бицикала 	8219.01	3921
3923	Монтер врата и прозора	8219.02	3921
3924	Монтер клима-уређаја	8219.03	3921
3925	Монтер металних производа у серијској производњи	8219.04	3921
3926	Монтер предмета од гуме/пластике у серијској производњи	8219.05	3921
3927	Монтер ролетни 	8219.06	3921
3928	Монтер суве градње	8219.07	3921
3929	Монтер у индустрији намештаја	8219.08	3921
3930	Возачи и руковаоци покретном механизацијом	83	3427
3931	Машиновође и сродни	831	3930
3932	Машиновође	8311	3931
3933	Машиновођа 	8311.01	3932
3934	Машиновођа дизел-вуче	8311.02	3932
3935	Машиновођа електро-вуче	8311.03	3932
3936	Машиновођа маневарке	8311.04	3932
3937	Машиновођа у руднику/каменолому	8311.05	3932
3938	Сувозач машиновође вучних возила	8311.06	3932
3939	Железнички кочничари, сигналисти и скретничари	8312	3931
3940	Возач моторних пружних возила	8312.01	3939
3941	Возач транспортних електро-колица	8312.02	3939
3942	Возовођа спроводник воза	8312.03	3939
3943	Железнички кочничар 	8312.04	3939
3944	Железнички скретничар 	8312.05	3939
3945	Кочничар воза у руднику/каменолому	8312.06	3939
3946	Маневриста	8312.07	3939
3947	Прегледач железничких кола	8312.08	3939
3948	Руковалац дрезином	8312.09	3939
3949	Саобраћајно-транспортни помоћник	8312.10	3939
3950	Спајач у маневарским станицама (ранжер вагона)	8312.11	3939
3951	Возачи аутомобила, доставних возила и мотоцикала	832	3930
3952	Возачи мотоцикала и курири 	8321	3951
3953	Курир мотоциклиста 	8321.01	3952
3954	Мотоциклиста достављач хране	8321.02	3952
3955	Возачи аутомобила, таксија и доставних возила	8322	3951
3956	Ауто-превозник	8322.01	3955
3957	Возач доставних возила	8322.02	3955
3958	Возач кола хитне медицинске помоћи	8322.03	3955
3959	Возач лаких моторних возила	8322.04	3955
3960	Возач погребног возила	8322.05	3955
3961	Возач путничког аутомобила	8322.06	3955
3962	Возач санитетског возила	8322.07	3955
3963	Возач службеног возила под пратњом	8322.08	3955
3964	Таксиста 	8322.09	3955
3965	Возачи тешких теретних возила и аутобуса	833	3930
3966	Возачи аутобуса, тролејбуса и трамваја	8331	3965
3967	Возач аутобуса	8331.01	3966
3968	Возач аутобуса у међународном саобраћају	8331.02	3966
3970	Возач тролејбуса	8331.04	3966
3971	Возачи тешких теретних возила и камиона 	8332	3965
3972	Возач ауто-мешалице за бетон 	8332.01	3971
3973	Возач ватрогасних кола	8332.02	3971
3974	Возач возила за превоз асфалтне масе 	8332.03	3971
3975	Возач камиона 	8332.04	3971
3976	Возач камиона са приколицом	8332.05	3971
3977	Возач кипера 	8332.06	3971
3978	Возач комуналних возила	8332.07	3971
3979	Возач тешких теретних возила 	8332.08	3971
3980	Возач цистерне 	8332.09	3971
3981	Руковалац јамским камионом  	8332.10	3971
3982	Руковалац тешким камионом на површинском копу	8332.11	3971
3983	Руковаоци покретном механизацијом	834	3930
3984	Руковаоци пољопривредном и шумском механизацијом	8341	3983
3985	Возач камиона за шумску грађу	8341.01	3984
3986	Помоћник руковаоца механизацијом у пољопривреди и шумарству 	8341.02	3984
3987	Руковалац вршилицом	8341.03	3984
3988	Руковалац жетелицом	8341.04	3984
3989	Руковалац комбајном 	8341.05	3984
3990	Руковалац механизацијом за сечу шуме	8341.06	3984
3991	Руковалац пољопривредним машинама	8341.07	3984
3992	Руковалац шумском механизацијом	8341.08	3984
3993	Тракториста 	8341.09	3984
3994	Хидромелиоратор	8341.10	3984
3995	Руковаоци механизацијом за земљане радове и сродни	8342	3983
3996	Помоћни руковалац једноставним грађевинским машинама	8342.01	3995
3997	Руковалац багером 	8342.02	3995
3998	Руковалац булдожером 	8342.03	3995
3999	Руковалац ваљком за сабијање тла 	8342.04	3995
4000	Руковалац грађевинском механизацијом	8342.05	3995
4001	Руковалац једноставним грађевинским машинама	8342.06	3995
4002	Руковалац машином за катранисање	8342.07	3995
4003	Руковалац машином за копање тунела 	8342.08	3995
4004	Руковалац машином за побијање шипова 	8342.09	3995
4005	Руковалац машином за полагање каблова 	8342.10	3995
4006	Руковалац машином за расипање ризле/соли	8342.11	3995
4007	Руковалац машином за чишћење снега 	8342.12	3995
4008	Руковалац механизацијом за асфалтирање 	8342.13	3995
4009	Руковалац ровокопачем 	8342.14	3995
4010	Руковалац скрепером и равналицом 	8342.15	3995
4011	Руковалац цевополагачем	8342.16	3995
4012	Руковаоци дизалицама, крановима и сродни	8343	3983
4013	Краниста 	8343.01	4012
4014	Оператер механизације високоградње 	8343.02	4012
4015	Руковалац ауто-дизалицом	8343.03	4012
4016	Руковалац бранама/преводницама/уставама 	8343.04	4012
4017	Руковалац бродском покретном дизалицом	8343.05	4012
4018	Руковалац грађевинском дизалицом	8343.06	4012
4019	Руковалац железничком кранском дизалицом 	8343.07	4012
4020	Руковалац жичаром/успињачом	8343.08	4012
4021	Руковалац мостном дизалицом 	8343.09	4012
4022	Руковалац покретним мостом	8343.10	4012
4023	Руковалац порталном кранском дизалицом 	8343.11	4012
4024	Руковалац рударским лифтом	8343.12	4012
4025	Руковалац стабилном кранском дизалицом 	8343.13	4012
4026	Руковалац транспортером	8343.14	4012
4027	Руковалац чекрком/витлом 	8343.15	4012
4028	Руковалац шумском жичаром	8343.16	4012
4029	Руковаоци претоварном механизацијом	8344	3983
4030	Виљушкариста	8344.01	4029
4031	Руковалац претоварном механизацијом	8344.02	4029
4032	Руковалац утоваривачем	8344.03	4029
4033	Бродска посада и сродни	835	3930
4034	Бродска посада и сродни	8350	4033
4035	Бродовођа унутрашње пловидбе	8350.01	4034
4036	Бродски машиниста	8350.02	4034
4037	Бродски сигналист	8350.03	4034
4038	Бродски ужар	8350.04	4034
4039	Кормилар тегљенице/тегљача/шлепа/барже	8350.05	4034
4040	Кормилар унутрашње пловидбе	8350.06	4034
4041	Морнар	8350.07	4034
4042	Руковалац пловном направом	8350.08	4034
4043	Руковалац шлепом	8350.09	4034
4044	Скелар-сплавар	8350.10	4034
4045	Чамџија	8350.11	4034
4046	Чистач бродске машине	8350.12	4034
4047	ЈЕДНОСТАВНА ЗАНИМАЊА	9	\N
4048	Чистачи и помоћно особље	91	4047
4049	Чистачи и помоћно особље у домаћинствима и пословним просторима	911	4048
4050	Кућне помоћнице и чистачи у домаћинствима	9111	4049
4051	Кућна помоћница	9111.01	4050
4052	Чистач просторија у домаћинству 	9111.02	4050
4053	Чистачи и помоћно особље у хотелима, предузећима и другим затвореним просторима 	9112	4049
4054	Помоћни радник у лабораторији 	9112.01	4053
4055	Собарица/спремачица 	9112.02	4053
4056	Чистач просторија (хотела, канцеларија, тоалета)	9112.03	4053
4057	Занимања за ручно прање и чишћење возила, прозора, веша и сродна занимања	912	4048
4058	Перачи и пеглери рубља, ручно	9121	4057
4059	Чистачи и перачи возила	9122	4057
4060	Перач возила	9122.01	4059
4061	Перач прозора на аутомобилима 	9122.02	4059
4062	Перачи прозора	9123	4057
4063	Перач прозора	9123.01	4062
4064	Остали чистачи и помоћно особље 	9129	4057
4065	Чистач базена за купање 	9129.01	4064
4066	Чистач графита	9129.02	4064
4067	Чистач индустријског торња за кондензацију водене паре 	9129.03	4064
4068	Чистач тепиха 	9129.04	4064
4069	Једноставна занимања у пољопривреди, рибарству и шумарству	92	4047
4070	Једноставна занимања у пољопривреди, рибарству и шумарству	921	4069
4071	Једноставна занимања у ратарству	9211	4070
4072	Берач воћа/поврћа 	9211.01	4071
4073	Мануелни радник на плантажама 	9211.02	4071
4074	Једноставна занимања у сточарству	9212	4070
4075	Коњушар	9212.01	4074
4076	Мануелни радник на фарми за узгајање стоке 	9212.02	4074
4077	Једноставна занимања у ратарско-сточарској производњи	9213	4070
4078	Мануелни радник на фарми за ратарске културе 	9213.01	4077
4079	Једноставна занимања у вртларству и хортикултури	9214	4070
4080	Баштенски радник	9214.01	4079
4081	Мануелни радник на пословима озелењивања 	9214.02	4079
4082	Мануелни радник у расаднику 	9214.03	4079
4083	Једноставна занимања у шумарству 	9215	4070
4084	Мануелни радник у шумарству 	9215.01	4083
4085	Радник на сечи и вучи дрва 	9215.02	4083
4086	Шумски путар	9215.03	4083
4087	Једноставна занимања у рибарству	9216	4070
4088	Мануелни радник у рибарству 	9216.01	4087
4089	Једноставна занимања у рударству, грађевинарству, прерађивачкој индустрији и транспорту	93	4047
4090	Једноставна занимања у рударству и грађевинарству	931	4089
4091	Једноставна занимања у рудницима и каменоломима	9311	4090
4092	Мануелни радник у каменолому 	9311.01	4091
4093	Мануелни радник у руднику 	9311.02	4091
4094	Једноставна занимања у нискоградњи 	9312	4090
4095	Мануелни радник за нискоградњу 	9312.01	4094
4096	Мануелни радник на одржавању бране 	9312.02	4094
4097	Путар 	9312.03	4094
4098	Једноставна занимања на градилиштима високоградње	9313	4090
4099	Мануелни радник за високоградњу 	9313.01	4098
4100	Мануелни радник за рушење објеката 	9313.02	4098
4101	Једноставна занимања у прерађивачкој индустрији	932	4089
4102	Пакери, ручно	9321	4101
4103	Пакер намештаја и паркета	9321.01	4102
4104	Радник на етикетирању робе 	9321.02	4102
4105	Ручни пакер амбалажиране робе	9321.03	4102
4106	Остала једноставна занимања у прерађивачкој индустрији	9329	4101
4107	Мануелни радник у графичарству 	9329.01	4106
4108	Мануелни радник у електроенергетици 	9329.02	4106
4109	Мануелни радник у машинству 	9329.03	4106
4110	Мануелни радник у металургији	9329.04	4106
4111	Мануелни радник у преради дрвета 	9329.05	4106
4112	Мануелни радник у прехрамбеној индустрији 	9329.06	4106
4113	Мануелни радник у текстилној индустрији 	9329.07	4106
4114	Мануелни радник у хемијској индустрији 	9329.08	4106
4115	Мерач-вагар у производном погону	9329.09	4106
4116	Намотач влакана/калема (ручно)	9329.10	4106
4117	Једноставна занимања у транспорту и складиштењу	933	4089
4118	Руковаоци возилима на механички погон	9331	4117
4119	Возач возила на ножни погон 	9331.01	4118
4120	Управљачи запрежним возилима 	9332	4117
4121	Возач запрежног возила	9332.01	4120
4122	Руковалац пољопривредном машином без моторног погона	9332.02	4120
4123	Фијакериста 	9332.03	4120
4124	Манипуланти теретом	9333	4117
4125	Лучки радник	9333.01	4124
4126	Манипулант архивском грађом	9333.02	4124
4127	Мануелни радник у ПТТ саобраћају 	9333.03	4124
4128	Радник на претовару робе 	9333.04	4124
4129	Радник на утовару/истовару	9333.05	4124
4130	Утоварач руде	9333.06	4124
4131	Манипуланти робом у трговини 	9334	4117
4132	Мануелни радник у трговини 	9334.01	4131
4133	Радник на пуњењу рафова 	9334.02	4131
4134	Једноставна занимања у припреми хране	94	4047
4135	Једноставна занимања у припреми хране	941	4134
4136	Припремачи брзе хране	9411	4135
4137	Припремач брзе хране (пице, салата, сендвича)	9411.01	4136
4138	Роштиљџија	9411.02	4136
4139	Кухињски помоћници 	9412	4135
4140	Кафе-кувар	9412.01	4139
4141	Перач судова 	9412.02	4139
4142	Помоћни радник у кухињи	9412.03	4139
4143	Једноставна трговачка и услужна занимања која се обављају на улици	95	4047
4144	Једноставна услужна занимања која се обављају на улици	951	4143
4145	Једноставна услужна занимања која се обављају на улици	9510	4144
4146	Постављач плаката и реклама	9510.01	4145
4147	Улични дистрибутер летака 	9510.02	4145
4148	Чистач ципела 	9510.03	4145
4149	Улични продавци непрехрамбених производа	952	4143
4150	Улични продавци непрехрамбених производа	9520	4149
4151	Продавац робе на уличној тезги 	9520.01	4150
4152	Улични продавац новина 	9520.02	4150
4153	Занимања на уклањању отпада и остала једноставна занимања	96	4047
4154	Занимања на уклањању отпада и чишћењу јавних површина	961	4153
4155	Занимања на сакупљању и одвожењу отпада	9611	4154
4156	Комунални радник на одношењу смећа	9611.01	4155
4157	Фекалист	9611.02	4155
4158	Шинтер	9611.03	4155
4159	Сакупљачи секундарних сировина 	9612	4154
4160	Мануелни радник за сортирање отпада	9612.01	4159
4161	Сакупљач секундарних сировина 	9612.02	4159
4162	Чистачи улица и сродни	9613	4154
4163	Чистач улица/паркова/плаже	9613.01	4162
4164	Остала једноставна занимања 	962	4153
4165	Курири, достављачи и сродна занимања 	9621	4164
4166	Достављач новина 	9621.01	4165
4167	Достављач робе	9621.02	4165
4168	Курир 	9621.03	4165
4169	Лифтбој	9621.04	4165
4170	Носач пртљага 	9621.05	4165
4171	Судски достављач / позивар	9621.06	4165
4172	Хотелски вратар	9621.07	4165
4173	Једноставна занимања за споредне послове 	9622	4164
4174	Звонар	9622.01	4173
4175	Читачи бројила и опслуживачи аутомата	9623	4164
4176	Опслужилац аутомата за продају пића/слаткиша/цигарета 	9623.01	4175
4177	Сакупљач новца/жетона из аутомата	9623.02	4175
4178	Читач бројила 	9623.03	4175
4179	Водоноше и сакупљачи огревног дрвета	9624	4164
4180	Једноставна занимања неразврстана на другом месту	9629	4164
4181	Ветеринарски хигијеничар 	9629.01	4180
4182	Гардеробер	9629.02	4180
4183	Гробар (инхуматор)	9629.03	4180
4184	Домаћица	9629.04	4180
4185	Надничар 	9629.05	4180
4186	Радник на паркингу / у гаражи 	9629.06	4180
4187	Разводник у биоскопу/позоришту	9629.07	4180
4188	ВОЈНА ЗАНИМАЊА	0	\N
4189	Официри војске	01	4188
4190	Официри војске	011	4189
4191	Официри војске	0110	4190
4192	Војни пилот	0110.01	4191
4193	Војни представник	0110.02	4191
4194	Војни референт за логистику	0110.03	4191
4195	Војни референт за мобилизацију	0110.04	4191
4196	Војни референт за обавештајно-извиђачке послове	0110.05	4191
4197	Војни референт за обуку	0110.06	4191
4198	Војни референт за оперативне послове	0110.07	4191
4199	Војни референт за телекомуникације и информатику	0110.08	4191
4200	Војни референт за цивилно-војну сарадњу	0110.09	4191
4201	Изасланик одбране	0110.10	4191
4202	Командант здруженог тактичког/оперативног састава	0110.11	4191
4203	Командант/командир у инжењеријским јединицама	0110.12	4191
4204	Командант/командир у јединицама за борбене операције	0110.13	4191
4205	Командант/командир у јединицама логистике	0110.14	4191
4206	Командант/командир у специјалним јединицама	0110.15	4191
4207	Начелник ОЈ за логистику	0110.16	4191
4208	Начелник ОЈ за мобилизацију	0110.17	4191
4209	Начелник ОЈ за обавештајно-извиђачке послове	0110.18	4191
4210	Начелник ОЈ за обуку	0110.19	4191
4211	Начелник ОЈ за оперативне послове	0110.20	4191
4212	Начелник ОЈ за телекомуникације и информатику	0110.21	4191
4213	Начелник ОЈ за цивилно–војну сарадњу	0110.22	4191
4214	Начелник управе у генералштабу	0110.23	4191
4215	Помоћник команданта за операције	0110.24	4191
4216	Помоћник команданта за подршку	0110.25	4191
4217	Подофицири војске	02	4188
4218	Подофицири војске	021	4217
4219	Подофицири војске	0210	4218
4220	Водич службених паса	0210.01	4219
4221	Главни/први подофицир	0210.02	4219
4222	Командир у инжењеријским јединицама	0210.03	4219
4223	Командир у јединицама за борбене операције	0210.04	4219
4224	Командир у јединицама логистике	0210.05	4219
4225	Командир у специјалним јединицама	0210.06	4219
4226	Оператер у јединицама за ваздушно осматрање и јављање	0210.07	4219
4227	Пиротехничар (војни)	0210.08	4219
4228	Радарски техничар	0210.09	4219
4229	Радио-прислушкивач	0210.10	4219
4230	Радио-гониометрист 	0210.11	4219
4231	Специјалац у јединицама пешадије 	0210.12	4219
4232	Техничар за авион и мотор	0210.13	4219
4233	Техничар за ваздухопловно наоружање	0210.14	4219
4234	Техничар за електроопрему	0210.15	4219
4235	Техничар за криптозаштитне уређаје	0210.16	4219
4236	Техничар за муницију и ракетну технику	0210.17	4219
4237	Техничар за ракетно наоружање	0210.18	4219
4238	Управник стрелишта	0210.19	4219
4239	Шифрер/дешифрер	0210.20	4219
4240	Остала војна занимања 	03	4188
4241	Остала војна занимања 	031	4240
4242	Остала војна занимања 	0310	4241
4243	Војник возач оклопних борбених возила 	0310.01	4242
4244	Војник диверзант	0310.02	4242
4245	Војник извиђач	0310.03	4242
4246	Војник оператер артиљеријских и ракетних средстава ПВО	0310.04	4242
4247	Војник оператер инжењеријских средстава	0310.05	4242
4248	Војник оператер на артиљеријским средствима	0310.06	4242
4249	Војник оператер на електронским средствима за прислушкивање, гониометрисање и ометање	0310.07	4242
4250	Војник оператер средстава везе	0310.08	4242
4251	Војник оператер у ОМЈ	0310.09	4242
4252	Војник осматрач	0310.10	4242
4253	Војник падобранац	0310.11	4242
4254	Војник пионир	0310.12	4242
4255	Војник поморски и речни диверзант	0310.13	4242
4256	Војник противмински ронилац	0310.14	4242
4257	Војник противтерориста	0310.15	4242
4258	Војник рачунач	0310.16	4242
4259	Војник стрелац оператер	0310.17	4242
4260	Војник шифрер	0310.18	4242
\.


--
-- Data for Name: paychecks_codebook; Type: TABLE DATA; Schema: private; Owner: postgres
--

COPY private.paychecks_codebook (id, start_use, end_use, pio_empl, zdr_empl, nez_empl, tax_empl, tax_release, pio_comp, zdr_comp, nez_comp, min_contribution_base, max_contribution_base) FROM stdin;
51	2017-12-01	2017-12-31	14.00	5.15	0.75	10.00	11790.00	12.00	5.15	0.75	23074.00	323010.00
4	2014-01-01	2014-01-31	13.00	6.15	0.75	10.00	11000.00	11.00	6.15	0.75	21210.00	304465.00
5	2014-02-01	2014-02-28	13.00	6.15	0.75	10.00	11242.00	11.00	6.15	0.75	22282.00	350355.00
6	2014-03-01	2014-03-31	13.00	6.15	0.75	10.00	11242.00	11.00	6.15	0.75	22282.00	262190.00
7	2014-04-01	2014-04-30	13.00	6.15	0.75	10.00	11242.00	11.00	6.15	0.75	22282.00	304225.00
8	2014-05-01	2014-05-31	13.00	6.15	0.75	10.00	11242.00	11.00	6.15	0.75	20198.00	304225.00
10	2014-07-01	2014-07-31	13.00	6.15	0.75	10.00	11242.00	11.00	6.15	0.75	20198.00	304830.00
11	2014-08-01	2014-08-31	14.00	5.15	0.75	10.00	11242.00	12.00	5.15	0.75	21716.00	309960.00
12	2014-09-01	2014-09-30	14.00	5.15	0.75	10.00	11242.00	12.00	5.15	0.75	21716.00	311900.00
13	2014-10-01	2014-10-31	14.00	5.15	0.75	10.00	11242.00	12.00	5.15	0.75	21718.00	314960.00
14	2014-11-01	2014-11-30	14.00	5.15	0.75	10.00	11242.00	12.00	5.15	0.75	21718.00	304015.00
15	2014-12-01	2014-12-31	14.00	5.15	0.75	10.00	11242.00	12.00	5.15	0.75	21718.00	309815.00
16	2015-01-01	2015-01-31	14.00	5.15	0.75	10.00	11242.00	12.00	5.15	0.75	21718.00	304910.00
17	2015-02-01	2015-02-28	14.00	5.15	0.75	10.00	11433.00	12.00	5.15	0.75	22357.00	350395.00
18	2015-03-01	2015-03-31	14.00	5.15	0.75	10.00	11433.00	12.00	5.15	0.75	22357.00	271040.00
19	2015-04-01	2015-04-30	14.00	5.15	0.75	10.00	11433.00	12.00	5.15	0.75	22357.00	295705.00
20	2015-05-01	2015-05-31	14.00	5.15	0.75	10.00	11433.00	12.00	5.15	0.75	20109.00	295705.00
21	2015-06-01	2015-06-30	14.00	5.15	0.75	10.00	11433.00	12.00	5.15	0.75	20109.00	312660.00
22	2015-07-01	2015-07-31	14.00	5.15	0.75	10.00	11433.00	12.00	5.15	0.75	20109.00	302435.00
23	2015-08-01	2015-08-31	14.00	5.15	0.75	10.00	11433.00	12.00	5.15	0.75	21506.00	306510.00
24	2015-09-01	2015-09-30	14.00	5.15	0.75	10.00	11433.00	12.00	5.15	0.75	21506.00	313435.00
25	2015-10-01	2015-10-31	14.00	5.15	0.75	10.00	11433.00	12.00	5.15	0.75	21506.00	307690.00
26	2015-11-01	2015-11-30	14.00	5.15	0.75	10.00	11433.00	12.00	5.15	0.75	21552.00	302515.00
27	2015-12-01	2015-12-31	14.00	5.15	0.75	10.00	11433.00	12.00	5.15	0.75	21552.00	303835.00
28	2016-01-01	2016-01-31	14.00	5.15	0.75	10.00	11433.00	12.00	5.15	0.75	21552.00	304565.00
29	2016-02-01	2016-02-29	14.00	5.15	0.75	10.00	11604.00	12.00	5.15	0.75	22449.00	353815.00
31	2016-04-01	2016-04-30	14.00	5.15	0.75	10.00	11604.00	12.00	5.15	0.75	22449.00	306395.00
32	2016-05-01	2016-05-31	14.00	5.15	0.75	10.00	11604.00	12.00	5.15	0.75	21012.00	315145.00
33	2016-06-01	2016-06-30	14.00	5.15	0.75	10.00	11604.00	12.00	5.15	0.75	21012.00	337320.00
34	2016-07-01	2016-07-31	14.00	5.15	0.75	10.00	11604.00	12.00	5.15	0.75	21012.00	302600.00
35	2016-08-01	2016-08-31	14.00	5.15	0.75	10.00	11604.00	12.00	5.15	0.75	22400.00	320095.00
36	2016-09-01	2016-09-30	14.00	5.15	0.75	10.00	11604.00	12.00	5.15	0.75	22400.00	318495.00
37	2016-10-01	2016-10-31	14.00	5.15	0.75	10.00	11604.00	12.00	5.15	0.75	22400.00	312370.00
38	2016-11-01	2016-11-30	14.00	5.15	0.75	10.00	11604.00	12.00	5.15	0.75	22204.00	320750.00
39	2016-12-01	2016-12-31	14.00	5.15	0.75	10.00	11604.00	12.00	5.15	0.75	22204.00	312070.00
40	2017-01-01	2017-01-31	14.00	5.15	0.75	10.00	11604.00	12.00	5.15	0.75	22204.00	315305.00
41	2017-02-01	2017-02-28	14.00	5.15	0.75	10.00	11790.00	12.00	5.15	0.75	23229.00	368205.00
42	2017-03-01	2017-03-31	14.00	5.15	0.75	10.00	11790.00	12.00	5.15	0.75	23229.00	286155.00
43	2017-04-01	2017-04-30	14.00	5.15	0.75	10.00	11790.00	12.00	5.15	0.75	23229.00	324235.00
44	2017-05-01	2017-05-31	14.00	5.15	0.75	10.00	11790.00	12.00	5.15	0.75	21906.00	328475.00
45	2017-06-01	2017-06-30	14.00	5.15	0.75	10.00	11790.00	12.00	5.15	0.75	21906.00	341230.00
46	2017-07-01	2017-07-31	14.00	5.15	0.75	10.00	11790.00	12.00	5.15	0.75	21906.00	324300.00
47	2017-08-01	2017-08-31	14.00	5.15	0.75	10.00	11790.00	12.00	5.15	0.75	23446.00	339285.00
48	2017-09-01	2017-09-30	14.00	5.15	0.75	10.00	11790.00	12.00	5.15	0.75	23446.00	331255.00
49	2017-10-01	2017-10-31	14.00	5.15	0.75	10.00	11790.00	12.00	5.15	0.75	23446.00	325470.00
50	2017-11-01	2017-11-30	14.00	5.15	0.75	10.00	11790.00	12.00	5.15	0.75	23074.00	332190.00
53	2018-01-06	2018-12-31	14.00	5.15	0.75	10.00	15000.00	12.00	5.15	0.75	23053.00	329330.00
52	2018-01-01	2018-01-05	14.00	5.15	0.75	10.00	15000.00	12.00	5.15	0.75	23074.00	328045.00
30	2016-03-01	2016-03-31	14.00	5.15	0.75	10.00	11604.00	12.00	5.15	0.75	22449.00	278815.00
9	2014-06-01	2014-06-30	13.00	6.15	0.75	10.00	11242.00	11.00	6.15	0.75	20198.00	315835.00
54	2019-01-01	2019-01-31	14.00	5.15	0.75	10.00	15000.00	12.00	5.15	0.00	23921.00	341725.00
55	2019-02-01	2019-12-31	14.00	5.15	0.75	10.00	15300.00	12.00	5.15	0.00	23921.00	341725.00
\.


--
-- Data for Name: payment_codes; Type: TABLE DATA; Schema: private; Owner: postgres
--

COPY private.payment_codes (id, name, code, description) FROM stdin;
57	Zakupnine	126	zakupnine za korišćenje nepokretnosti i pokretnih stvari na koje se plaća porez prema zakonu
8	Subvencije, regresi i premije s posebnih računa	227	isplata, naplata, prenos i obračun po osnovu subvencija, regresa i premija s konsolidovanog računa trezora, odnosno fondova i organizacija obaveznog socijalnog osiguranja
64	Isplate preko omladinskih i studentskih zadruga	144	isplata članovima zadruge s računa zadruge
19	Ostali prihodi fizičkih lica	249	prihod od ugovorene naknade za stvaranje autorskog dela, prihod sportista i sportskih stručnjaka, prihod od ugovora o delu i drugi prihodi fizičkih lica nepomenuti u šiframa od 40 do 48
23	Preknjižavanje više uplaćenih ili pogrešno uplaćenih tekućih prihoda	258	prenos sredstava s jednog uplatnog računa tekućeg prihoda u korist drugog, a na ime više uplaćenih ili pogrešno uplaćenih tekućih prihoda
34	Polaganje oročenih depozita	273	\N
36	Otplata kratkoročnih kredita	276	\N
37	Otplata dugoročnih kredita	277	\N
38	Povraćaj oročenih depozita	278	\N
40	Eskont hartija od vrednosti	280	\N
43	Naplata čekova građana	283	\N
44	Platne kartice	284	\N
45	Menjački poslovi	285	\N
46	Kupoprodaja deviza	286	\N
49	Transakcije po nalogu građana	289	\N
50	Druge transakcije	290	\N
9	Subvencije, regresi i premije sa ostalih računa	228	isplata, naplata, prenos i obračun po osnovu subvencija, regresa i premija sa ostalih računa
14	Isplate preko omladinskih i studentskih zadruga	244	isplata članovima zadruge s računa zadruge
65	Penzije	145	iznos penzija koji se isplaćuje penzionerima ili prenosi na njihove tekuće račune u banci i drugoj finansijskoj organizaciji, osim isplata u gotovom novcu
84	Polaganje oročenih depozita	173	\N
86	Otplata kratkoročnih kredita	176	\N
87	Otplata dugoročnih kredita	177	\N
88	Povraćaj oročenih depozita	178	\N
90	Eskont hartija od vrednosti	180	\N
93	Naplata čekova građana	183	\N
94	Platne kartice	184	\N
95	Menjački poslovi	185	\N
96	Kupoprodaja deviza	186	\N
99	Transakcije po nalogu građana	189	\N
100	Druge transakcije	190	\N
15	Penzije	245	iznos penzija koji se isplaćuje penzionerima ili prenosi na njihove tekuće račune u banci i drugoj finansijskoj organizaciji, osim isplata u gotovom novcu
1	Promet robe i usluga – međufazna potrošnja	220	plaćanja za robu, sirovine, materijal, proizvodne usluge, gorivo, mazivo, energiju, otkup poljoprivrednih proizvoda, članarine, uplate obaveza javnim preduzećima koje nisu propisane i za drugu robu i usluge
51	Promet robe i usluga – međufazna potrošnja	120	plaćanja za robu, sirovine, materijal, proizvodne usluge, gorivo, mazivo, energiju, otkup poljoprivrednih proizvoda, članarine, uplate obaveza javnim preduzećima koje nisu propisane i za drugu robu i usluge
103	Usluge javnih preduzeća	322	uplate propisanih obaveza javnim preduzećima
53	Usluge javnih preduzeća	122	uplate propisanih obaveza javnim preduzećima
3	Usluge javnih preduzeća	222	uplate propisanih obaveza javnim preduzećima
54	Investicije u objekte i opremu	123	plaćanja po osnovu izgradnje objekata i nabavke opreme (nabavna cena, doprema, montaža i dr.)
4	Investicije u objekte i opremu	223	plaćanja po osnovu izgradnje objekata i nabavke opreme (nabavna cena, doprema, montaža i dr.)
5	Investicije – ostalo	224	plaćanja po osnovu investicija, osim investicija u objekte i opremu
66	Obustave od penzija i zarada	146	obustave po osnovu administrativne zabrane za kredite, članarine i ostale zakonske, administrativne i druge obustave
16	Obustave od penzija i zarada	246	obustave po osnovu administrativne zabrane za kredite, članarine i ostale zakonske, administrativne i druge obustave
20	Uplata javnih prihoda izuzev poreza i doprinosa po odbitku	253	uplata javnih prihoda, i to poreza, izuzev poreza po odbitku, taksi, naknada i dr.
70	Uplata javnih prihoda izuzev poreza i doprinosa po odbitku	153	uplata javnih prihoda, i to poreza, izuzev poreza po odbitku, taksi, naknada i dr.
72	Povraćaj više naplaćenih ili pogrešno naplaćenih tekućih prihoda	157	prenos sredstava sa uplatnog računa tekućeg prihoda u korist poreskog obveznika na ime više naplaćenih ili pogrešno naplaćenih tekućih prihoda
24	Premije osiguranja i nadoknada štete	260	premija osiguranja, reosiguranje, nadoknada štete
374	Premije osiguranja i nadoknada štete	960	premija osiguranja, reosiguranje, nadoknada štete
76	Transferi u okviru državnih organa	162	prenos u okviru računa i podračuna trezora, prenos sredstava budžetskim korisnicima, isplate po socijalnom programu Vlade
77	Ostali transferi	163	prenos u okviru istog pravnog lica i drugi transferi, raspored zajedničkog prihoda
29	Uplata pazara	265	uplata dnevnog pazara
79	Uplata pazara	165	uplata dnevnog pazara
30	Isplata gotovine	266	sve gotovinske isplate s računa pravnog lica i preduzetnika
80	Isplata gotovine	166	sve gotovinske isplate s računa pravnog lica i preduzetnika
31	Kratkoročni krediti	270	prenos sredstava po osnovu odobrenih kratkoročnih kredita
81	Kratkoročni krediti	170	prenos sredstava po osnovu odobrenih kratkoročnih kredita
82	Dugoročni krediti	171	prenos sredstava po osnovu odobrenih dugoročnih kredita
33	Aktivna kamata	272	plaćanje kamate po kreditima
39	Pasivna kamata	279	plaćanje kamate po depozitima i drugim novčanim polozima
41	Pozajmice osnivača za likvidnost	281	uplata pozajmice osnivača – fizičkog lica pravnom licu
42	Povraćaj pozajmice za likvidnost osnivaču	282	povraćaj pozajmice pravnog lica osnivaču – fizičkom licu
47	Donacije i sponzorstva	287	plaćanja iz sredstava banaka i drugih pravnih lica po osnovu interne regulative
401	Donacije i sponzorstva	987	plaćanja iz sredstava banaka i drugih pravnih lica po osnovu interne regulative
348	Donacije	988	donacije iz međunarodnih ugovora
151	Promet robe i usluga – međufazna potrošnja	320	plaćanja za robu, sirovine, materijal, proizvodne usluge, gorivo, mazivo, energiju, otkup poljoprivrednih proizvoda, članarine, uplate obaveza javnim preduzećima koje nisu propisane i za drugu robu i usluge
52	Promet robe i usluga – finalna potrošnja	121	plaćanja za robu, sirovine, materijal, proizvodne usluge, gorivo, mazivo, energiju, otkup poljoprivrednih proizvoda, članarine, uplate obaveza javnim preduzećima koje nisu propisane i za drugu robu i usluge (uključujući i plaćanje svih provizija i naknada), izuzev za investicije – finalna potrošnja
154	Investicije u objekte i opremu	323	plaćanja po osnovu izgradnje objekata i nabavke opreme (nabavna cena, doprema, montaža i dr.)
155	Investicije – ostalo	324	plaćanja po osnovu investicija, osim investicija u objekte i opremu
156	Zakupnine stvari u javnoj svojini	325	zakupnine za korišćenje nepokretnosti i pokretnih stvari u državnoj svojini, naknade za druge usluge koje imaju karakter javnih prihoda
157	Zakupnine	326	zakupnine za korišćenje nepokretnosti i pokretnih stvari na koje se plaća porez prema zakonu
7	Zakupnine	226	zakupnine za korišćenje nepokretnosti i pokretnih stvari na koje se plaća porez prema zakonu
158	Subvencije, regresi i premije s posebnih računa	327	isplata, naplata, prenos i obračun po osnovu subvencija, regresa i premija s konsolidovanog računa trezora, odnosno fondova i organizacija obaveznog socijalnog osiguranja
159	Subvencije, regresi i premije sa ostalih računa	328	isplata, naplata, prenos i obračun po osnovu subvencija, regresa i premija sa ostalih računa
160	Carine i druge uvozne dažbine	331	uplata, naplata, prenos s računa i obračun po osnovu izmirenja carina i drugih uvoznih dažbina (carine i drugi javni prihodi koje Uprava carina naplati objedinjeno na svoj evidentni račun)
161	Zarade i druga primanja zaposlenih	340	zarada; lična zarada preduzetnika; razlika zarade lica postavljenih na javnu funkciju za vreme obavljanja te funkcije; ugovorena naknada za privremene i povremene poslove, kao i oporeziva primanja zaposlenih po osnovu: naknade troškova prevoza u javnom saobraćaju, dnevnice i naknade troškova prevoza i smeštaja za službeno putovanje u zemlji ili inostranstvu, dnevne naknade pripadnika Vojske Srbije, solidarne pomoći za slučaj bolesti, rehabilitacije ili invalidnosti zaposlenog ili člana njegove porodice, novogodišnjih i božićnih poklona deci zaposlenog i jubilarne nagrade zaposlenog
112	Neoporeziva primanja zaposlenih, socijalna i druga davanja izuzeta od oporezivanja	341	neoporeziva primanja zaposlenih po osnovu: naknade troškova prevoza u javnom saobraćaju, dnevnice i naknade troškova prevoza i smeštaja za službeno putovanje u zemlji ili inostranstvu, dnevne naknade pripadnika Vojske Srbije, solidarne pomoći za slučaj bolesti, rehabilitacije ili invalidnosti zaposlenog ili člana njegove porodice, novogodišnjih i božićnih poklona deci zaposlenog i jubilarne nagrade zaposlenog; socijalna i druga davanja izuzeta iz oporezivanja u skladu sa zakonom kojim se uređuje porez na dohodak građana, izuzev naknade troškova volontiranja
13	Naknade zarada na teret poslodavca	242	naknada zarade zbog privremene sprečenosti za rad zbog povrede na radu ili profesionalne bolesti, a koja od prvog dana pa sve vreme trajanja sprečenosti tereti sredstva poslodavca; naknada zarade za slučaj privremene sprečenosti za rad do 30 dana usled bolesti ili povrede van rada, zbog bolesti ili komplikacija u vezi sa održavanjem trudnoće, zbog propisane mere obavezne izolacije, zbog nege bolesnog člana uže porodice, izuzev deteta mlađeg od tri godine, zbog određivanja za pratioca bolesnog lica u skladu sa zakonom kojim se uređuje zdravstveno osiguranje; naknada zarade za vreme plaćenog odsustva za vreme prekida rada, odnosno smanjenja obima rada do kog je došlo bez krivice zaposlenog prema članu 116. Zakona o radu
114	Isplate preko omladinskih i studentskih zadruga	344	isplata članovima zadruge s računa zadruge
115	Penzije	345	iznos penzija koji se isplaćuje penzionerima ili prenosi na njihove tekuće račune u banci i drugoj finansijskoj organizaciji, osim isplata u gotovom novcu
166	Obustave od penzija i zarada	346	obustave po osnovu administrativne zabrane za kredite, članarine i ostale zakonske, administrativne i druge obustave
137	Otplata dugoročnih kredita	377	\N
118	Prihodi fizičkih lica od kapitala i drugih imovinskih prava	348	kamata, dividenda i učešće u dobiti, prinos od investicione jedinice otvorenog investicionog fonda, prihod od izdavanja nepokretnosti i pokretnih stvari, prihod od imovinskog prava nad autorskim delom, odnosno od prava industrijske svojine, prihodi od osiguranja
220	Uplata javnih prihoda izuzev poreza i doprinosa po odbitku	953	uplata javnih prihoda, i to poreza, izuzev poreza po odbitku, taksi, naknada i dr.
122	Povraćaj više naplaćenih ili pogrešno naplaćenih tekućih prihoda	357	prenos sredstava sa uplatnog računa tekućeg prihoda u korist poreskog obveznika na ime više naplaćenih ili pogrešno naplaćenih tekućih prihoda
174	Premije osiguranja i nadoknada štete	360	premija osiguranja, reosiguranje, nadoknada štete
25	Raspored tekućih prihoda	261	raspored poreza, doprinosa i drugih tekućih prihoda uplaćenih korisnicima
177	Ostali transferi	363	prenos u okviru istog pravnog lica i drugi transferi, raspored zajedničkog prihoda
179	Uplata pazara	365	uplata dnevnog pazara
180	Isplata gotovine	366	sve gotovinske isplate s računa pravnog lica i preduzetnika
181	Kratkoročni krediti	370	prenos sredstava po osnovu odobrenih kratkoročnih kredita
182	Dugoročni krediti	371	prenos sredstava po osnovu odobrenih dugoročnih kredita
183	Aktivna kamata	372	plaćanje kamate po kreditima
150	Druge transakcije	390	\N
191	Pozajmice osnivača za likvidnost	381	uplata pozajmice osnivača – fizičkog lica pravnom licu
197	Donacije i sponzorstva	387	plaćanja iz sredstava banaka i drugih pravnih lica po osnovu interne regulative
198	Donacije	388	donacije iz međunarodnih ugovora
184	Polaganje oročenih depozita	373	\N
186	Otplata kratkoročnih kredita	376	\N
188	Povraćaj oročenih depozita	378	\N
190	Eskont hartija od vrednosti	380	\N
193	Naplata čekova građana	383	\N
194	Platne kartice	384	\N
195	Menjački poslovi	385	\N
196	Kupoprodaja deviza	386	\N
199	Transakcije po nalogu građana	389	\N
307	Zakupnine	926	zakupnine za korišćenje nepokretnosti i pokretnih stvari na koje se plaća porez prema zakonu
258	Subvencije, regresi i premije s posebnih računa	927	isplata, naplata, prenos i obračun po osnovu subvencija, regresa i premija s konsolidovanog računa trezora, odnosno fondova i organizacija obaveznog socijalnog osiguranja
58	Subvencije, regresi i premije s posebnih računa	127	isplata, naplata, prenos i obračun po osnovu subvencija, regresa i premija s konsolidovanog računa trezora, odnosno fondova i organizacija obaveznog socijalnog osiguranja
59	Subvencije, regresi i premije sa ostalih računa	128	isplata, naplata, prenos i obračun po osnovu subvencija, regresa i premija sa ostalih računa
309	Subvencije, regresi i premije sa ostalih računa	928	isplata, naplata, prenos i obračun po osnovu subvencija, regresa i premija sa ostalih računa
60	Carine i druge uvozne dažbine	131	uplata, naplata, prenos s računa i obračun po osnovu izmirenja carina i drugih uvoznih dažbina (carine i drugi javni prihodi koje Uprava carina naplati objedinjeno na svoj evidentni račun)
310	Carine i druge uvozne dažbine	931	uplata, naplata, prenos s računa i obračun po osnovu izmirenja carina i drugih uvoznih dažbina (carine i drugi javni prihodi koje Uprava carina naplati objedinjeno na svoj evidentni račun)
61	Zarade i druga primanja zaposlenih	140	zarada; lična zarada preduzetnika; razlika zarade lica postavljenih na javnu funkciju za vreme obavljanja te funkcije; ugovorena naknada za privremene i povremene poslove, kao i oporeziva primanja zaposlenih po osnovu: naknade troškova prevoza u javnom saobraćaju, dnevnice i naknade troškova prevoza i smeštaja za službeno putovanje u zemlji ili inostranstvu, dnevne naknade pripadnika Vojske Srbije, solidarne pomoći za slučaj bolesti, rehabilitacije ili invalidnosti zaposlenog ili člana njegove porodice, novogodišnjih i božićnih poklona deci zaposlenog i jubilarne nagrade zaposlenog
11	Zarade i druga primanja zaposlenih	240	zarada; lična zarada preduzetnika; razlika zarade lica postavljenih na javnu funkciju za vreme obavljanja te funkcije; ugovorena naknada za privremene i povremene poslove, kao i oporeziva primanja zaposlenih po osnovu: naknade troškova prevoza u javnom saobraćaju, dnevnice i naknade troškova prevoza i smeštaja za službeno putovanje u zemlji ili inostranstvu, dnevne naknade pripadnika Vojske Srbije, solidarne pomoći za slučaj bolesti, rehabilitacije ili invalidnosti zaposlenog ili člana njegove porodice, novogodišnjih i božićnih poklona deci zaposlenog i jubilarne nagrade zaposlenog
261	Zarade i druga primanja zaposlenih	940	zarada; lična zarada preduzetnika; razlika zarade lica postavljenih na javnu funkciju za vreme obavljanja te funkcije; ugovorena naknada za privremene i povremene poslove, kao i oporeziva primanja zaposlenih po osnovu: naknade troškova prevoza u javnom saobraćaju, dnevnice i naknade troškova prevoza i smeštaja za službeno putovanje u zemlji ili inostranstvu, dnevne naknade pripadnika Vojske Srbije, solidarne pomoći za slučaj bolesti, rehabilitacije ili invalidnosti zaposlenog ili člana njegove porodice, novogodišnjih i božićnih poklona deci zaposlenog i jubilarne nagrade zaposlenog
12	Neoporeziva primanja zaposlenih, socijalna i druga davanja izuzeta od oporezivanja	241	neoporeziva primanja zaposlenih po osnovu: naknade troškova prevoza u javnom saobraćaju, dnevnice i naknade troškova prevoza i smeštaja za službeno putovanje u zemlji ili inostranstvu, dnevne naknade pripadnika Vojske Srbije, solidarne pomoći za slučaj bolesti, rehabilitacije ili invalidnosti zaposlenog ili člana njegove porodice, novogodišnjih i božićnih poklona deci zaposlenog i jubilarne nagrade zaposlenog; socijalna i druga davanja izuzeta iz oporezivanja u skladu sa zakonom kojim se uređuje porez na dohodak građana, izuzev naknade troškova volontiranja
264	Isplate preko omladinskih i studentskih zadruga	944	isplata članovima zadruge s računa zadruge
315	Penzije	945	iznos penzija koji se isplaćuje penzionerima ili prenosi na njihove tekuće račune u banci i drugoj finansijskoj organizaciji, osim isplata u gotovom novcu
69	Ostali prihodi fizičkih lica	149	prihod od ugovorene naknade za stvaranje autorskog dela, prihod sportista i sportskih stručnjaka, prihod od ugovora o delu i drugi prihodi fizičkih lica nepomenuti u šiframa od 40 do 48
325	Raspored tekućih prihoda	961	raspored poreza, doprinosa i drugih tekućih prihoda uplaćenih korisnicima
252	Promet robe i usluga – finalna potrošnja	921	plaćanja za robu, sirovine, materijal, proizvodne usluge, gorivo, mazivo, energiju, otkup poljoprivrednih proizvoda, članarine, uplate obaveza javnim preduzećima koje nisu propisane i za drugu robu i usluge (uključujući i plaćanje svih provizija i naknada), izuzev za investicije – finalna potrošnja
2	Promet robe i usluga – finalna potrošnja	221	plaćanja za robu, sirovine, materijal, proizvodne usluge, gorivo, mazivo, energiju, otkup poljoprivrednih proizvoda, članarine, uplate obaveza javnim preduzećima koje nisu propisane i za drugu robu i usluge (uključujući i plaćanje svih provizija i naknada), izuzev za investicije – finalna potrošnja
102	Promet robe i usluga – finalna potrošnja	321	plaćanja za robu, sirovine, materijal, proizvodne usluge, gorivo, mazivo, energiju, otkup poljoprivrednih proizvoda, članarine, uplate obaveza javnim preduzećima koje nisu propisane i za drugu robu i usluge (uključujući i plaćanje svih provizija i naknada), izuzev za investicije – finalna potrošnja
277	Ostali transferi	963	prenos u okviru istog pravnog lica i drugi transferi, raspored zajedničkog prihoda
279	Uplata pazara	965	uplata dnevnog pazara
231	Kratkoročni krediti	970	prenos sredstava po osnovu odobrenih kratkoročnih kredita
32	Dugoročni krediti	271	prenos sredstava po osnovu odobrenih dugoročnih kredita
232	Dugoročni krediti	971	prenos sredstava po osnovu odobrenih dugoročnih kredita
303	Usluge javnih preduzeća	922	uplate propisanih obaveza javnim preduzećima
304	Investicije u objekte i opremu	923	plaćanja po osnovu izgradnje objekata i nabavke opreme (nabavna cena, doprema, montaža i dr.)
55	Investicije – ostalo	124	plaćanja po osnovu investicija, osim investicija u objekte i opremu
306	Zakupnine stvari u javnoj svojini	925	zakupnine za korišćenje nepokretnosti i pokretnih stvari u državnoj svojini, naknade za druge usluge koje imaju karakter javnih prihoda
56	Zakupnine stvari u javnoj svojini	125	zakupnine za korišćenje nepokretnosti i pokretnih stvari u državnoj svojini, naknade za druge usluge koje imaju karakter javnih prihoda
296	Kupoprodaja deviza	986	\N
10	Carine i druge uvozne dažbine	231	uplata, naplata, prenos s računa i obračun po osnovu izmirenja carina i drugih uvoznih dažbina (carine i drugi javni prihodi koje Uprava carina naplati objedinjeno na svoj evidentni račun)
362	Neoporeziva primanja zaposlenih, socijalna i druga davanja izuzeta od oporezivanja	941	neoporeziva primanja zaposlenih po osnovu: naknade troškova prevoza u javnom saobraćaju, dnevnice i naknade troškova prevoza i smeštaja za službeno putovanje u zemlji ili inostranstvu, dnevne naknade pripadnika Vojske Srbije, solidarne pomoći za slučaj bolesti, rehabilitacije ili invalidnosti zaposlenog ili člana njegove porodice, novogodišnjih i božićnih poklona deci zaposlenog i jubilarne nagrade zaposlenog; socijalna i druga davanja izuzeta iz oporezivanja u skladu sa zakonom kojim se uređuje porez na dohodak građana, izuzev naknade troškova volontiranja
91	Pozajmice osnivača za likvidnost	181	uplata pozajmice osnivača – fizičkog lica pravnom licu
62	Neoporeziva primanja zaposlenih, socijalna i druga davanja izuzeta od oporezivanja	141	neoporeziva primanja zaposlenih po osnovu: naknade troškova prevoza u javnom saobraćaju, dnevnice i naknade troškova prevoza i smeštaja za službeno putovanje u zemlji ili inostranstvu, dnevne naknade pripadnika Vojske Srbije, solidarne pomoći za slučaj bolesti, rehabilitacije ili invalidnosti zaposlenog ili člana njegove porodice, novogodišnjih i božićnih poklona deci zaposlenog i jubilarne nagrade zaposlenog; socijalna i druga davanja izuzeta iz oporezivanja u skladu sa zakonom kojim se uređuje porez na dohodak građana, izuzev naknade troškova volontiranja
337	Otplata dugoročnih kredita	977	\N
338	Povraćaj oročenih depozita	978	\N
63	Naknade zarada na teret poslodavca	142	naknada zarade zbog privremene sprečenosti za rad zbog povrede na radu ili profesionalne bolesti, a koja od prvog dana pa sve vreme trajanja sprečenosti tereti sredstva poslodavca; naknada zarade za slučaj privremene sprečenosti za rad do 30 dana usled bolesti ili povrede van rada, zbog bolesti ili komplikacija u vezi sa održavanjem trudnoće, zbog propisane mere obavezne izolacije, zbog nege bolesnog člana uže porodice, izuzev deteta mlađeg od tri godine, zbog određivanja za pratioca bolesnog lica u skladu sa zakonom kojim se uređuje zdravstveno osiguranje; naknada zarade za vreme plaćenog odsustva za vreme prekida rada, odnosno smanjenja obima rada do kog je došlo bez krivice zaposlenog prema članu 116. Zakona o radu
340	Eskont hartija od vrednosti	980	\N
351	Promet robe i usluga – međufazna potrošnja	920	plaćanja za robu, sirovine, materijal, proizvodne usluge, gorivo, mazivo, energiju, otkup poljoprivrednih proizvoda, članarine, uplate obaveza javnim preduzećima koje nisu propisane i za drugu robu i usluge
343	Naplata čekova građana	983	\N
163	Naknade zarada na teret poslodavca	342	naknada zarade zbog privremene sprečenosti za rad zbog povrede na radu ili profesionalne bolesti, a koja od prvog dana pa sve vreme trajanja sprečenosti tereti sredstva poslodavca; naknada zarade za slučaj privremene sprečenosti za rad do 30 dana usled bolesti ili povrede van rada, zbog bolesti ili komplikacija u vezi sa održavanjem trudnoće, zbog propisane mere obavezne izolacije, zbog nege bolesnog člana uže porodice, izuzev deteta mlađeg od tri godine, zbog određivanja za pratioca bolesnog lica u skladu sa zakonom kojim se uređuje zdravstveno osiguranje; naknada zarade za vreme plaćenog odsustva za vreme prekida rada, odnosno smanjenja obima rada do kog je došlo bez krivice zaposlenog prema članu 116. Zakona o radu
263	Naknade zarada na teret poslodavca	942	naknada zarade zbog privremene sprečenosti za rad zbog povrede na radu ili profesionalne bolesti, a koja od prvog dana pa sve vreme trajanja sprečenosti tereti sredstva poslodavca; naknada zarade za slučaj privremene sprečenosti za rad do 30 dana usled bolesti ili povrede van rada, zbog bolesti ili komplikacija u vezi sa održavanjem trudnoće, zbog propisane mere obavezne izolacije, zbog nege bolesnog člana uže porodice, izuzev deteta mlađeg od tri godine, zbog određivanja za pratioca bolesnog lica u skladu sa zakonom kojim se uređuje zdravstveno osiguranje; naknada zarade za vreme plaćenog odsustva za vreme prekida rada, odnosno smanjenja obima rada do kog je došlo bez krivice zaposlenog prema članu 116. Zakona o radu
366	Obustave od penzija i zarada	946	obustave po osnovu administrativne zabrane za kredite, članarine i ostale zakonske, administrativne i druge obustave
17	Naknade zarada na teret drugih isplatilaca	247	naknada zarade za bolovanje preko 30 dana za slučaj privremene sprečenosti za rad preko 30 dana, zbog bolesti ili povreda van rada, bolesti ili komplikacija u vezi sa održavanjem trudnoće, propisane mere obavezne izolacije, nege bolesnog člana uže porodice, izuzev deteta mlađeg od tri godine, kao i određivanja za pratioca bolesnog lica; naknada zarade za bolovanje preko 30 dana za slučaj privremene sprečenosti za rad zbog davanja tkiva i organa i zbog nege bolesnog deteta do 3 godine; naknada zarade za vreme porodiljskog odsustva, odsustva s rada radi nege deteta i odsustva s rada radi posebne nege deteta
317	Naknade zarada na teret drugih isplatilaca	947	naknada zarade za bolovanje preko 30 dana za slučaj privremene sprečenosti za rad preko 30 dana, zbog bolesti ili povreda van rada, bolesti ili komplikacija u vezi sa održavanjem trudnoće, propisane mere obavezne izolacije, nege bolesnog člana uže porodice, izuzev deteta mlađeg od tri godine, kao i određivanja za pratioca bolesnog lica; naknada zarade za bolovanje preko 30 dana za slučaj privremene sprečenosti za rad zbog davanja tkiva i organa i zbog nege bolesnog deteta do 3 godine; naknada zarade za vreme porodiljskog odsustva, odsustva s rada radi nege deteta i odsustva s rada radi posebne nege deteta
350	Druge transakcije	990	\N
355	Investicije – ostalo	924	plaćanja po osnovu investicija, osim investicija u objekte i opremu
6	Zakupnine stvari u javnoj svojini	225	zakupnine za korišćenje nepokretnosti i pokretnih stvari u državnoj svojini, naknade za druge usluge koje imaju karakter javnih prihoda
369	Ostali prihodi fizičkih lica	949	prihod od ugovorene naknade za stvaranje autorskog dela, prihod sportista i sportskih stručnjaka, prihod od ugovora o delu i drugi prihodi fizičkih lica nepomenuti u šiframa od 40 do 48
74	Premije osiguranja i nadoknada štete	160	premija osiguranja, reosiguranje, nadoknada štete
380	Isplata gotovine	966	sve gotovinske isplate s računa pravnog lica i preduzetnika
383	Aktivna kamata	972	plaćanje kamate po kreditima
83	Aktivna kamata	172	plaćanje kamate po kreditima
392	Povraćaj pozajmice za likvidnost osnivaču	982	povraćaj pozajmice pravnog lica osnivaču – fizičkom licu
98	Donacije	188	donacije iz međunarodnih ugovora
384	Polaganje oročenih depozita	973	\N
386	Otplata kratkoročnih kredita	976	\N
394	Platne kartice	984	\N
395	Menjački poslovi	985	\N
399	Transakcije po nalogu građana	989	\N
67	Naknade zarada na teret drugih isplatilaca	147	naknada zarade za bolovanje preko 30 dana za slučaj privremene sprečenosti za rad preko 30 dana, zbog bolesti ili povreda van rada, bolesti ili komplikacija u vezi sa održavanjem trudnoće, propisane mere obavezne izolacije, nege bolesnog člana uže porodice, izuzev deteta mlađeg od tri godine, kao i određivanja za pratioca bolesnog lica; naknada zarade za bolovanje preko 30 dana za slučaj privremene sprečenosti za rad zbog davanja tkiva i organa i zbog nege bolesnog deteta do 3 godine; naknada zarade za vreme porodiljskog odsustva, odsustva s rada radi nege deteta i odsustva s rada radi posebne nege deteta
341	Pozajmice osnivača za likvidnost	981	uplata pozajmice osnivača – fizičkog lica pravnom licu
92	Povraćaj pozajmice za likvidnost osnivaču	182	povraćaj pozajmice pravnog lica osnivaču – fizičkom licu
142	Povraćaj pozajmice za likvidnost osnivaču	382	povraćaj pozajmice pravnog lica osnivaču – fizičkom licu
167	Naknade zarada na teret drugih isplatilaca	347	naknada zarade za bolovanje preko 30 dana za slučaj privremene sprečenosti za rad preko 30 dana, zbog bolesti ili povreda van rada, bolesti ili komplikacija u vezi sa održavanjem trudnoće, propisane mere obavezne izolacije, nege bolesnog člana uže porodice, izuzev deteta mlađeg od tri godine, kao i određivanja za pratioca bolesnog lica; naknada zarade za bolovanje preko 30 dana za slučaj privremene sprečenosti za rad zbog davanja tkiva i organa i zbog nege bolesnog deteta do 3 godine; naknada zarade za vreme porodiljskog odsustva, odsustva s rada radi nege deteta i odsustva s rada radi posebne nege deteta
68	Prihodi fizičkih lica od kapitala i drugih imovinskih prava	148	kamata, dividenda i učešće u dobiti, prinos od investicione jedinice otvorenog investicionog fonda, prihod od izdavanja nepokretnosti i pokretnih stvari, prihod od imovinskog prava nad autorskim delom, odnosno od prava industrijske svojine, prihodi od osiguranja
18	Prihodi fizičkih lica od kapitala i drugih imovinskih prava	248	kamata, dividenda i učešće u dobiti, prinos od investicione jedinice otvorenog investicionog fonda, prihod od izdavanja nepokretnosti i pokretnih stvari, prihod od imovinskog prava nad autorskim delom, odnosno od prava industrijske svojine, prihodi od osiguranja
268	Prihodi fizičkih lica od kapitala i drugih imovinskih prava	948	kamata, dividenda i učešće u dobiti, prinos od investicione jedinice otvorenog investicionog fonda, prihod od izdavanja nepokretnosti i pokretnih stvari, prihod od imovinskog prava nad autorskim delom, odnosno od prava industrijske svojine, prihodi od osiguranja
119	Ostali prihodi fizičkih lica	349	prihod od ugovorene naknade za stvaranje autorskog dela, prihod sportista i sportskih stručnjaka, prihod od ugovora o delu i drugi prihodi fizičkih lica nepomenuti u šiframa od 40 do 48
120	Uplata javnih prihoda izuzev poreza i doprinosa po odbitku	353	uplata javnih prihoda, i to poreza, izuzev poreza po odbitku, taksi, naknada i dr.
21	Uplata poreza i doprinosa po odbitku	254	uplata poreza i doprinosa koje je isplatilac prihoda dužan da obračuna, obustavi i uplati na propisani jedinstveni uplatni račun najkasnije na dan isplate prihoda fizičkom licu po odbitku
71	Uplata poreza i doprinosa po odbitku	154	uplata poreza i doprinosa koje je isplatilac prihoda dužan da obračuna, obustavi i uplati na propisani jedinstveni uplatni račun najkasnije na dan isplate prihoda fizičkom licu po odbitku
121	Uplata poreza i doprinosa po odbitku	354	uplata poreza i doprinosa koje je isplatilac prihoda dužan da obračuna, obustavi i uplati na propisani jedinstveni uplatni račun najkasnije na dan isplate prihoda fizičkom licu po odbitku
272	Povraćaj više naplaćenih ili pogrešno naplaćenih tekućih prihoda	957	prenos sredstava sa uplatnog računa tekućeg prihoda u korist poreskog obveznika na ime više naplaćenih ili pogrešno naplaćenih tekućih prihoda
22	Povraćaj više naplaćenih ili pogrešno naplaćenih tekućih prihoda	257	prenos sredstava sa uplatnog računa tekućeg prihoda u korist poreskog obveznika na ime više naplaćenih ili pogrešno naplaćenih tekućih prihoda
123	Preknjižavanje više uplaćenih ili pogrešno uplaćenih tekućih prihoda	358	prenos sredstava s jednog uplatnog računa tekućeg prihoda u korist drugog, a na ime više uplaćenih ili pogrešno uplaćenih tekućih prihoda
73	Preknjižavanje više uplaćenih ili pogrešno uplaćenih tekućih prihoda	158	prenos sredstava s jednog uplatnog računa tekućeg prihoda u korist drugog, a na ime više uplaćenih ili pogrešno uplaćenih tekućih prihoda
273	Preknjižavanje više uplaćenih ili pogrešno uplaćenih tekućih prihoda	958	prenos sredstava s jednog uplatnog računa tekućeg prihoda u korist drugog, a na ime više uplaćenih ili pogrešno uplaćenih tekućih prihoda
75	Raspored tekućih prihoda	161	raspored poreza, doprinosa i drugih tekućih prihoda uplaćenih korisnicima
175	Raspored tekućih prihoda	361	raspored poreza, doprinosa i drugih tekućih prihoda uplaćenih korisnicima
26	Transferi u okviru državnih organa	262	prenos u okviru računa i podračuna trezora, prenos sredstava budžetskim korisnicima, isplate po socijalnom programu Vlade
176	Transferi u okviru državnih organa	362	prenos u okviru računa i podračuna trezora, prenos sredstava budžetskim korisnicima, isplate po socijalnom programu Vlade
376	Transferi u okviru državnih organa	962	prenos u okviru računa i podračuna trezora, prenos sredstava budžetskim korisnicima, isplate po socijalnom programu Vlade
27	Ostali transferi	263	prenos u okviru istog pravnog lica i drugi transferi, raspored zajedničkog prihoda
28	Prenos sredstava iz budžeta za obezbeđenje povraćaja više naplaćenih tekućih prihoda	264	prenos sredstava iz budžeta na uplatni račun tekućeg prihoda s kog je potrebno izvršiti povraćaj sredstava koja će se vratiti obvezniku
78	Prenos sredstava iz budžeta za obezbeđenje povraćaja više naplaćenih tekućih prihoda	164	prenos sredstava iz budžeta na uplatni račun tekućeg prihoda s kog je potrebno izvršiti povraćaj sredstava koja će se vratiti obvezniku
178	Prenos sredstava iz budžeta za obezbeđenje povraćaja više naplaćenih tekućih prihoda	364	prenos sredstava iz budžeta na uplatni račun tekućeg prihoda s kog je potrebno izvršiti povraćaj sredstava koja će se vratiti obvezniku
278	Prenos sredstava iz budžeta za obezbeđenje povraćaja više naplaćenih tekućih prihoda	964	prenos sredstava iz budžeta na uplatni račun tekućeg prihoda s kog je potrebno izvršiti povraćaj sredstava koja će se vratiti obvezniku
185	Ostali plasmani	375	kupoprodaja vlasničkih hartija od vrednosti, kupovina kapitala u postupku privatizacije u smislu zakona kojim se uređuje privatizacija i kupovina udela iz Akcijskog fonda Republike Srbije, međubankarski plasmani (hartije, krediti)
335	Ostali plasmani	975	kupoprodaja vlasničkih hartija od vrednosti, kupovina kapitala u postupku privatizacije u smislu zakona kojim se uređuje privatizacija i kupovina udela iz Akcijskog fonda Republike Srbije, međubankarski plasmani (hartije, krediti)
35	Ostali plasmani	275	kupoprodaja vlasničkih hartija od vrednosti, kupovina kapitala u postupku privatizacije u smislu zakona kojim se uređuje privatizacija i kupovina udela iz Akcijskog fonda Republike Srbije, međubankarski plasmani (hartije, krediti)
85	Ostali plasmani	175	kupoprodaja vlasničkih hartija od vrednosti, kupovina kapitala u postupku privatizacije u smislu zakona kojim se uređuje privatizacija i kupovina udela iz Akcijskog fonda Republike Srbije, međubankarski plasmani (hartije, krediti)
89	Pasivna kamata	179	plaćanje kamate po depozitima i drugim novčanim polozima
189	Pasivna kamata	379	plaćanje kamate po depozitima i drugim novčanim polozima
239	Pasivna kamata	979	plaćanje kamate po depozitima i drugim novčanim polozima
97	Donacije i sponzorstva	187	plaćanja iz sredstava banaka i drugih pravnih lica po osnovu interne regulative
48	Donacije	288	donacije iz međunarodnih ugovora
\.


--
-- Data for Name: payment_configurations; Type: TABLE DATA; Schema: private; Owner: postgres
--

COPY private.payment_configurations (id, name, type, separator, account_number_col, date_col, amount_col, description, amount2_col, path, decimal_separator, account_owner_col, encoding, payment_code_col, call_for_number_debit_col, call_for_number_credit_col, bank_account_date, bank_account_amount, bank_account_id, bank_statement_number, export_format, bank_transaction_id) FROM stdin;
1	комерцијална банка	1	\N	паyееаццоунтинфо.аццтид	дтпостед	трнамт		бенефит	трнлист	.	паyееинфо.наме	УТФ-8	пурпосецоде	паyеерефнумбер	рефнумбер	аваилбал.дтасоф	аваилбал.баламт	аццтид	стмтнумбер	xмл	фитид
2	Халцом #	0	#	10	9	5		6	\N	,	11	УТФ-8	13	14	15	1	5	0	2	ЦСВ	27
3	Ерсте wеб	2	;	5	2	6		7	\N	,	11	УТФ-8	3	9	10		8	0	0	ЦСВ	13
\.


--
-- Data for Name: postal_codes; Type: TABLE DATA; Schema: private; Owner: postgres
--

COPY private.postal_codes (id, code, country_id) FROM stdin;
1	11000	3
2	24430	3
3	26000	3
6	22244	3
7	24425	3
8	36203	3
9	18226	3
10	25212	3
11	37230	3
12	12370	3
13	23217	3
14	18220	3
15	26310	3
16	31307	3
17	25260	3
18	23207	3
19	34303	3
20	34300	3
21	31230	3
22	22418	3
23	11423	3
24	18315	3
25	18330	3
26	21420	3
27	31258	3
28	37265	3
29	22225	3
30	21400	3
31	21401	3
32	21402	3
33	24300	3
34	24302	3
35	25275	3
36	25276	3
37	25242	3
38	25252	3
39	21234	3
40	25272	3
41	21470	3
42	24343	3
43	24415	3
44	21465	3
45	21217	3
46	21429	3
47	21226	3
48	34226	3
49	15358	3
50	35204	3
51	31250	3
52	24210	3
53	24331	3
54	18257	3
55	36344	3
56	26324	3
57	23332	3
58	26234	3
59	23242	3
60	23216	3
61	26314	3
62	26327	3
63	23237	3
64	23251	3
65	23315	3
66	23213	3
67	26320	3
68	23312	3
69	11424	3
70	34304	3
71	31337	3
72	15316	3
73	14214	3
74	38216	3
75	21312	3
76	22303	3
77	15362	3
78	11460	3
79	26205	3
80	18426	3
81	34205	3
82	36315	3
83	12227	3
84	11504	3
85	26367	3
86	18432	3
87	11565	3
88	23316	3
89	34227	3
90	22251	3
91	26222	3
92	21220	3
93	21221	3
94	11279	3
95	11308	3
96	21411	3
97	23232	3
98	26340	3
99	15313	3
100	18310	3
101	37253	3
102	31311	3
103	14246	3
104	32259	3
105	22306	3
106	11223	3
107	19366	3
108	11461	3
109	23205	3
110	18424	3
111	34312	3
112	18411	3
113	35263	3
114	21300	3
115	11101	3
116	11106	3
117	11190	3
118	11138	3
119	11129	3
120	11139	3
121	11147	3
122	11150	3
123	11164	3
124	11163	3
125	11281	3
126	11107	3
127	11122	3
128	11159	3
129	11166	3
130	11169	3
131	11196	3
132	11158	3
133	11108	3
134	11142	3
135	11109	3
136	11110	3
137	11161	3
138	11111	3
139	11162	3
140	11112	3
141	11113	3
142	11114	3
143	11050	3
144	11115	3
145	11143	3
146	11171	3
147	11210	3
148	11070	3
149	11116	3
150	11117	3
151	11102	3
152	11172	3
153	11118	3
154	11040	3
155	11119	3
156	11120	3
157	11121	3
158	11060	3
159	11103	3
160	11131	3
161	11145	3
162	11173	3
163	11174	3
164	11123	3
165	11124	3
166	11146	3
167	11181	3
168	11010	3
169	11191	3
170	11104	3
171	11192	3
172	11125	3
173	11176	3
174	11177	3
175	11134	3
176	11179	3
177	11126	3
178	11127	3
179	11170	3
180	11180	3
185	11148	3
186	11193	3
187	11187	3
188	11152	3
189	11149	3
190	11151	3
191	11153	3
192	11188	3
193	11135	3
194	11136	3
195	11105	3
196	11154	3
197	11189	3
198	11155	3
199	11137	3
200	11160	3
201	11090	3
202	11165	3
203	11156	3
204	11128	3
205	11231	3
206	11030	3
207	11080	3
208	11182	3
209	11183	3
210	11184	3
211	11185	3
212	11186	3
213	11278	3
214	11273	3
215	11274	3
216	11222	3
217	11141	3
218	11221	3
219	11133	3
220	11250	3
221	11252	3
222	11175	3
223	11178	3
224	11157	3
225	11075	3
226	11195	3
227	11077	3
228	11144	3
229	22242	3
230	32305	3
231	22212	3
232	22324	3
233	25270	3
234	35238	3
235	22254	3
236	24206	3
237	17522	3
238	22253	3
239	31241	3
240	17546	3
241	31325	3
242	18420	3
243	37226	3
244	35217	3
245	23274	3
246	21427	3
247	24408	3
248	15350	3
249	25245	3
250	14225	3
251	19372	3
252	36341	3
253	16205	3
254	23252	3
255	11307	3
256	19370	3
257	11275	3
258	32312	3
259	19210	3
260	19211	3
261	19212	3
262	19218	3
263	11211	3
264	19229	3
265	19231	3
266	17540	3
267	16232	3
268	37262	3
269	22217	3
270	23243	3
271	31322	3
272	12313	3
273	17537	3
274	19315	3
275	12206	3
276	12222	3
277	14201	3
278	12225	3
279	32256	3
280	32303	3
281	31234	3
282	32213	3
283	22415	3
284	19216	3
285	16253	3
286	12205	3
287	14244	3
288	15309	3
289	32307	3
290	32253	3
291	38157	3
292	14212	3
293	12230	3
294	31305	3
295	37220	3
296	19313	3
297	36346	3
298	19323	3
299	34228	3
300	37225	3
301	19369	3
302	22421	3
303	21242	3
304	17520	3
305	17567	3
306	21209	3
307	32251	3
308	34301	3
309	34217	3
310	34242	3
311	35273	3
312	18368	3
313	12307	3
314	35249	3
315	32101	3
316	32102	3
317	32103	3
318	32104	3
319	32105	3
320	31310	3
321	22231	3
322	24220	3
323	18417	3
324	21413	3
325	21233	3
326	23266	3
327	21311	3
328	15224	3
329	23215	3
330	37210	3
331	37208	3
332	18232	3
333	23320	3
334	25210	3
335	22326	3
336	26213	3
337	36321	3
338	23328	3
339	15355	3
340	16215	3
341	18304	3
342	26323	3
343	18313	3
344	25220	3
345	36220	3
346	34322	3
347	35230	3
348	35231	3
349	21238	3
350	23335	3
351	34305	3
352	37271	3
353	26214	3
354	15214	3
355	22441	3
356	26225	3
357	36307	3
358	25254	3
359	34321	3
360	35213	3
361	21468	3
362	26316	3
363	36305	3
364	18320	3
365	14222	3
366	14204	3
367	31236	3
368	22232	3
369	34204	3
370	11272	3
371	12224	3
372	18408	3
373	15235	3
374	26354	3
375	22412	3
376	18314	3
377	18410	3
378	26227	3
379	19213	3
380	15317	3
381	19352	3
382	11326	3
383	17544	3
384	35255	3
385	15323	3
386	18404	3
387	34314	3
388	18421	3
389	35258	3
390	18414	3
391	15227	3
392	18254	3
393	17547	3
394	19345	3
395	18242	3
396	37258	3
397	19220	3
398	17526	3
399	25243	3
400	14203	3
401	15311	3
402	15226	3
403	31317	3
404	34231	3
405	35272	3
406	35262	3
407	35265	3
408	38239	3
409	11506	3
410	18223	3
411	35257	3
412	15212	3
413	15324	3
414	11432	3
415	15359	3
416	12255	3
417	26224	3
418	18406	3
419	12207	3
420	11561	3
421	36312	3
422	18237	3
423	37202	3
424	26328	3
425	34215	3
426	21239	3
427	24213	3
428	19335	3
429	37206	3
430	17514	3
431	18405	3
432	23203	3
433	23208	3
434	34207	3
435	22230	3
436	23264	3
437	24323	3
438	21410	3
439	32300	3
440	32301	3
441	18240	3
442	26223	3
443	21432	3
444	25282	3
445	19228	3
446	21247	3
447	22258	3
448	35261	3
449	37251	3
450	26202	3
451	35222	3
452	21412	3
453	15356	3
454	11316	3
455	12223	3
456	22308	3
457	32232	3
458	32225	3
459	31214	3
460	18425	3
461	17538	3
462	34206	3
463	14243	3
464	18202	3
465	32215	3
466	32306	3
467	18241	3
468	24406	3
469	16244	3
470	18204	3
471	37221	3
472	37234	3
473	21237	3
474	31313	3
475	11508	3
476	22423	3
477	38205	3
478	18321	3
479	19205	3
480	37229	3
481	16220	3
482	26347	3
483	18213	3
484	18219	3
485	22213	3
486	19342	3
487	19341	3
488	11306	3
489	34202	3
490	34230	3
491	34232	3
492	32230	3
493	26335	3
494	24312	3
495	18208	3
496	38209	3
497	26370	3
498	24414	3
499	19236	3
500	23235	3
501	24410	3
502	24411	3
503	22427	3
504	23323	3
505	26207	3
506	26352	3
507	34203	3
508	22250	3
509	22319	3
510	22320	3
511	22406	3
512	32250	3
513	26233	3
514	26343	3
515	31314	3
516	14226	3
517	26201	3
518	31306	3
519	19304	3
520	15308	3
521	11412	3
522	35000	3
523	35102	3
524	35103	3
525	35104	3
526	11276	3
527	11270	3
528	22248	3
529	26362	3
530	22426	3
531	23250	3
532	34318	3
533	23230	3
534	35241	3
535	26346	3
536	31319	3
537	37252	3
538	22409	3
539	23327	3
540	18206	3
541	17531	3
542	31215	3
543	34309	3
544	26363	3
545	32222	3
546	31213	3
547	18234	3
548	36345	3
549	35205	3
550	34211	3
551	18258	3
552	18253	3
553	11562	3
554	21241	3
555	26212	3
556	26329	3
557	19353	3
558	31257	3
559	11130	3
560	32206	3
561	15222	3
562	14252	3
563	18324	3
564	24420	3
565	32234	3
566	37256	3
567	21421	3
568	24308	3
569	31204	3
570	25255	3
571	22443	3
572	18225	3
573	24104	3
574	24205	3
575	24407	3
576	23300	3
577	23301	3
578	23302	3
579	23303	3
580	23304	3
581	23306	3
582	23307	3
583	23308	3
584	23309	3
585	23310	3
586	23317	3
587	21211	3
588	19320	3
589	23211	3
590	22424	3
591	17524	3
592	12258	3
593	15357	3
594	12209	3
595	17535	3
596	25221	3
597	19222	3
598	34240	3
599	23265	3
600	19350	3
601	19316	3
602	15220	3
603	31318	3
604	35242	3
605	11431	3
606	25274	3
607	14254	3
608	23253	3
609	36340	3
610	35219	3
611	37254	3
612	36354	3
613	19223	3
614	11415	3
615	17545	3
616	19329	3
617	15302	3
618	34224	3
619	18216	3
620	38220	3
621	38223	3
622	16206	3
623	31260	3
624	38210	3
625	31254	3
626	12208	3
627	32235	3
628	11409	3
629	26210	3
630	21243	3
631	32257	3
632	26220	3
633	34000	3
634	34110	3
635	34111	3
636	34112	3
637	34113	3
638	34114	3
639	34115	3
640	34103	3
641	34104	3
642	34105	3
643	34106	3
644	34107	3
645	34108	3
646	34109	3
647	23231	3
648	22411	3
649	36101	3
650	36102	3
651	36103	3
652	36104	3
653	36105	3
654	36000	3
655	36107	3
656	36108	3
657	22325	3
658	31242	3
659	12316	3
660	17543	3
661	37282	3
662	24341	3
663	19219	3
664	19375	3
665	22314	3
666	11319	3
667	18307	3
668	15314	3
669	35227	3
670	25225	3
671	26380	3
672	31233	3
673	22328	3
674	37000	3
675	37102	3
676	37103	3
677	37104	3
678	37105	3
679	37106	3
680	37107	3
681	37108	3
682	37109	3
683	18409	3
684	12240	3
685	12241	3
686	21466	3
687	37255	3
688	22224	3
689	25230	3
690	25231	3
691	25235	3
692	18214	3
693	21472	3
694	23271	3
695	37222	3
696	26368	3
697	22419	3
698	25262	3
699	18435	3
700	18430	3
701	18431	3
702	11425	3
703	26349	3
704	32258	3
705	35226	3
706	26336	3
707	22223	3
708	22221	3
709	37232	3
710	36204	3
711	14224	3
712	25234	3
713	18201	3
714	18256	3
715	34220	3
716	34221	3
717	34223	3
718	11550	3
719	23241	3
720	12321	3
721	16230	3
722	16248	3
723	21207	3
724	14205	3
725	19343	3
726	37224	3
727	17513	3
728	38218	3
729	38219	3
730	16000	3
731	16104	3
732	16105	3
733	16106	3
734	16107	3
735	15307	3
736	11309	3
737	22207	3
738	18245	3
739	25232	3
740	11310	3
741	15305	3
742	14240	3
743	22255	3
744	18217	3
745	31209	3
746	15320	3
747	22321	3
748	24215	3
749	35274	3
750	21248	3
751	26361	3
752	24322	3
753	15300	3
754	15301	3
755	11317	3
756	19208	3
757	32240	3
758	21315	3
759	11321	3
760	19234	3
761	36306	3
762	23261	3
763	23224	3
764	18437	3
765	19371	3
766	31203	3
767	18228	3
768	34325	3
769	11261	3
770	31312	3
771	22202	3
772	15211	3
773	21473	3
774	23333	3
775	19250	3
776	12221	3
777	35270	3
778	15353	3
779	24217	3
780	11313	3
781	18423	3
782	18207	3
783	34216	3
784	24416	3
785	24309	3
786	24321	3
787	19347	3
788	19209	3
789	11235	3
790	15318	3
791	12311	3
792	34212	3
793	18415	3
794	22208	3
795	16201	3
796	22327	3
797	26364	3
798	11325	3
799	32243	3
800	34209	3
801	22222	3
802	24417	3
803	36201	3
804	23234	3
805	18209	3
806	32255	3
807	35224	3
808	37244	3
809	16240	3
810	23270	3
811	11426	3
812	12305	3
813	18436	3
814	18445	3
815	18252	3
816	15233	3
817	19204	3
818	11312	3
819	19322	3
820	23202	3
821	35236	3
822	37227	3
823	21227	3
824	26373	3
825	11318	3
826	35268	3
827	37246	3
828	19340	3
829	14242	3
830	11567	3
831	16204	3
832	24211	3
833	31251	3
834	11400	3
835	21422	3
836	32211	3
837	31243	3
838	19317	3
839	23305	3
840	24435	3
841	22256	3
842	22245	3
843	21245	3
844	18229	3
845	18251	3
846	26226	3
847	32210	3
848	17529	3
849	23206	3
850	21216	3
851	23311	3
852	34313	3
853	19300	3
854	12242	3
855	21314	3
856	23245	3
857	22422	3
858	19311	3
859	26322	3
860	18101	3
861	18107	3
862	18110	3
863	18111	3
864	18112	3
865	18114	3
866	18103	3
867	18104	3
868	18105	3
869	18106	3
870	18108	3
871	18109	3
872	18366	3
873	18205	3
874	24311	3
875	22203	3
876	23218	3
877	25224	3
878	21431	3
879	22330	3
880	31320	3
881	22304	3
882	23272	3
883	37216	3
884	23236	3
885	22322	3
886	23330	3
887	23336	3
888	23313	3
889	26353	3
890	36300	3
891	36302	3
892	36303	3
893	36304	3
894	36316	3
895	21101	3
896	21102	3
897	21103	3
898	21104	3
899	21105	3
900	21106	3
901	21107	3
902	21108	3
903	21109	3
904	21110	3
905	21111	3
906	21112	3
907	21113	3
908	21114	3
909	21115	3
910	21116	3
911	21117	3
912	21118	3
913	21119	3
914	21120	3
915	21121	3
916	21122	3
917	21123	3
918	21124	3
919	21125	3
920	21126	3
921	22323	3
922	24223	3
923	35271	3
924	23273	3
925	24351	3
926	36216	3
927	18250	3
928	11500	3
929	11503	3
930	37266	3
931	22417	3
932	21423	3
933	25250	3
934	22416	3
935	26230	3
936	35267	3
937	26204	3
938	16233	3
939	17557	3
940	34308	3
941	24344	3
942	12308	3
943	15213	3
944	23263	3
945	24207	3
946	12317	3
947	14253	3
948	11314	3
949	19378	3
950	37281	3
951	23326	3
952	18312	3
953	11251	3
954	11212	3
955	32242	3
956	24342	3
957	11213	3
958	23325	3
959	37257	3
960	26215	3
961	24413	3
962	18363	3
963	14213	3
964	26101	3
965	26110	3
966	26111	3
967	26103	3
968	26104	3
969	26105	3
970	26106	3
971	26107	3
972	26108	3
973	26109	3
974	24330	3
975	35250	3
976	35252	3
977	35253	3
978	21434	3
979	37201	3
980	38266	3
981	26333	3
982	16251	3
983	22410	3
984	14207	3
985	18413	3
986	37231	3
987	23260	3
988	31256	3
989	15304	3
990	12300	3
991	21131	3
992	21132	3
993	21133	3
994	11282	3
995	11226	3
996	18300	3
997	18303	3
998	18308	3
999	21469	3
1000	35247	3
1001	26360	3
1002	19207	3
1003	22420	3
1004	21428	3
1005	35212	3
1006	37238	3
1007	37237	3
1008	26229	3
1009	24313	3
1010	37243	3
1011	14206	3
1012	36215	3
1013	19362	3
1014	19321	3
1015	18326	3
1016	37214	3
1017	12372	3
1018	22428	3
1019	35254	3
1020	18260	3
1021	14221	3
1022	12375	3
1023	35207	3
1024	12000	3
1025	12102	3
1026	12103	3
1027	12104	3
1028	12105	3
1029	12106	3
1030	12107	3
1031	31210	3
1032	19330	3
1033	32308	3
1034	16222	3
1035	18365	3
1036	32212	3
1037	17523	3
1038	17527	3
1039	17555	3
1040	35264	3
1041	22442	3
1042	31330	3
1043	31333	3
1044	17511	3
1045	14251	3
1046	25263	3
1047	31300	3
1048	31303	3
1049	32252	3
1050	38213	3
1051	22257	3
1052	15306	3
1053	11280	3
1054	18400	3
1055	18401	3
1056	18402	3
1057	18433	3
1058	15215	3
1059	11413	3
1060	18255	3
1061	22404	3
1062	12254	3
1063	18440	3
1064	34210	3
1065	15321	3
1066	22206	3
1067	21225	3
1068	11311	3
1069	14211	3
1070	23221	3
1071	19334	3
1072	19314	3
1073	14202	3
1074	11327	3
1075	21299	3
1076	11233	3
1077	34302	3
1078	38267	3
1079	12304	3
1080	12315	3
1081	35206	3
1082	36350	3
1083	36352	3
1084	36355	3
1085	25283	3
1086	37236	3
1087	11411	3
1088	36212	3
1089	25253	3
1090	18246	3
1091	35235	3
1092	31206	3
1093	23212	3
1094	22205	3
1095	21471	3
1096	31265	3
1097	37215	3
1098	37223	3
1099	16252	3
1100	35260	3
1101	17556	3
1102	35237	3
1103	34225	3
1104	19214	3
1105	35220	3
1106	37259	3
1107	15310	3
1108	36309	3
1109	37205	3
1110	25280	3
1111	11232	3
1112	11234	3
1113	17521	3
1114	26331	3
1115	36205	3
1116	11453	3
1117	31255	3
1118	31237	3
1119	19318	3
1120	31208	3
1121	19257	3
1122	36353	3
1123	32313	3
1124	36222	3
1125	11566	3
1126	22400	3
1127	22401	3
1128	22402	3
1129	21201	3
1130	16223	3
1131	11194	3
1132	25233	3
1133	23314	3
1134	18224	3
1135	15000	3
1136	15103	3
1137	15104	3
1138	23324	3
1139	21244	3
1140	26206	3
1141	19224	3
1142	22204	3
1143	36202	3
1144	26350	3
1145	23331	3
1146	11315	3
1147	18235	3
1148	19373	3
1149	19379	3
1150	22425	3
1151	16213	3
1152	31335	3
1153	19377	3
1154	21467	3
1155	31262	3
1156	23240	3
1157	35211	3
1158	26203	3
1159	21425	3
1160	26351	3
1161	11407	3
1162	35233	3
1163	35234	3
1164	24400	3
1165	24401	3
1166	11433	3
1167	12309	3
1168	31205	3
1169	22440	3
1170	11454	3
1171	18311	3
1172	22239	3
1173	22240	3
1174	16246	3
1175	35256	3
1176	19225	3
1177	21433	3
1178	32311	3
1179	22310	3
1180	12373	3
1181	35203	3
1182	19326	3
1183	34213	3
1184	36208	3
1185	21214	3
1186	31207	3
1187	25223	3
1188	36310	3
1189	11509	3
1190	11322	3
1191	26228	3
1192	37218	3
1193	11215	3
1194	32224	3
1195	19324	3
1196	14245	3
1197	31244	3
1198	37239	3
1199	14223	3
1200	11420	3
1201	11421	3
1202	11300	3
1203	11303	3
1204	11304	3
1205	11305	3
1206	11330	3
1207	18323	3
1208	12312	3
1209	38217	3
1210	18230	3
1211	25101	3
1212	25103	3
1213	25104	3
1214	25105	3
1215	25106	3
1216	25107	3
1217	25108	3
1218	25264	3
1219	36308	3
1220	11450	3
1221	22243	3
1222	22000	3
1223	22101	3
1224	22103	3
1225	22111	3
1226	22112	3
1227	21480	3
1228	12253	3
1229	22413	3
1230	11253	3
1231	21202	3
1232	21204	3
1233	21208	3
1234	22247	3
1235	21205	3
1236	23220	3
1237	23233	3
1238	23334	3
1239	25244	3
1240	23204	3
1241	37212	3
1242	25284	3
1243	25240	3
1244	24340	3
1245	22300	3
1246	26232	3
1247	24224	3
1248	22305	3
1249	26371	3
1250	21206	3
1251	22329	3
1252	11324	3
1253	36311	3
1254	14255	3
1255	22405	3
1256	35215	3
1257	21212	3
1258	11564	3
1259	15354	3
1260	36314	3
1261	34307	3
1262	37242	3
1263	34323	3
1264	26345	3
1265	18332	3
1266	35269	3
1267	38236	3
1268	17512	3
1269	19303	3
1270	11507	3
1271	36343	3
1272	37272	3
1273	35209	3
1274	24101	3
1275	24110	3
1276	24111	3
1277	24112	3
1278	24113	3
1279	24114	3
1280	24115	3
1281	24116	3
1282	24117	3
1283	24118	3
1284	24000	3
1285	24103	3
1286	24105	3
1287	24106	3
1288	24107	3
1289	24108	3
1290	24109	3
1291	18227	3
1292	22414	3
1293	18322	3
1294	19376	3
1295	24418	3
1296	35228	3
1297	11271	3
1298	22307	3
1299	17530	3
1300	23254	3
1301	21313	3
1302	23244	3
1303	12322	3
1304	17508	3
1305	25211	3
1306	35210	3
1307	35214	3
1308	15221	3
1309	25265	3
1310	16212	3
1311	35259	3
1312	18360	3
1313	32304	3
1314	23209	3
1315	24214	3
1316	15234	3
1317	19325	3
1318	25222	3
1319	21235	3
1320	21236	3
1321	18355	3
1322	18212	3
1323	21240	3
1324	23222	3
1325	23262	3
1326	34310	3
1327	12226	3
1328	34243	3
1329	23214	3
1330	24352	3
1331	24427	3
1332	21424	3
1333	32205	3
1334	24426	3
1335	35248	3
1336	17525	3
1337	32221	3
1338	37235	3
1339	19306	3
1340	15303	3
1341	37240	3
1342	37241	3
1343	37249	3
1344	18211	3
1345	16247	3
1346	16231	3
1347	21215	3
1348	12257	3
1349	36320	3
1350	14210	3
1351	36313	3
1352	32314	3
1353	11277	3
1354	26330	3
1355	11430	3
1356	11260	3
1357	19305	3
1358	36342	3
1359	24437	3
1360	26216	3
1361	31000	3
1362	31102	3
1363	31103	3
1364	31104	3
1365	31105	3
1366	31106	3
1367	31107	3
1368	15319	3
1369	11262	3
1370	21426	3
1371	14000	3
1372	14111	3
1373	14103	3
1374	14104	3
1375	14105	3
1376	14106	3
1377	14107	3
1378	31263	3
1379	15232	3
1380	37260	3
1381	22241	3
1382	19367	3
1383	26337	3
1384	16221	3
1385	19328	3
1386	37245	3
1387	26366	3
1388	11414	3
1389	19235	3
1390	11408	3
1391	37209	3
1392	18403	3
1393	11320	3
1394	15322	3
1395	37233	3
1396	11462	3
1397	11563	3
1398	26365	3
1399	19206	3
1400	35223	3
1401	22211	3
1402	37204	3
1403	17528	3
1404	18215	3
1405	37207	3
1406	12220	3
1407	12228	3
1408	34214	3
1409	12306	3
1410	11323	3
1411	12314	3
1412	26334	3
1413	34306	3
1414	21203	3
1415	32233	3
1416	21246	3
1417	19368	3
1418	11351	3
1419	12229	3
1420	32254	3
1421	24222	3
1422	22246	3
1423	18306	3
1424	36206	3
1425	36207	3
1426	22431	3
1427	37213	3
1428	17510	3
1429	15225	3
1430	26315	3
1431	26332	3
1432	19258	3
1433	17507	3
1434	17532	3
1435	17533	3
1436	17534	3
1437	11406	3
1438	12371	3
1439	16210	3
1440	11328	3
1441	22429	3
1442	22313	3
1443	35208	3
1444	23219	3
1445	26338	3
1446	12256	3
1447	26348	3
1448	32315	3
1449	11427	3
1450	17501	3
1451	17503	3
1452	17504	3
1453	17505	3
1454	17542	3
1455	11329	3
1456	19344	3
1457	19312	3
1458	36214	3
1459	21460	3
1460	21463	3
1461	21464	3
1462	23329	3
1463	11224	3
1464	22408	3
1465	11560	3
1466	18236	3
1467	36210	3
1468	36217	3
1469	26302	3
1470	26303	3
1471	26300	3
1472	26304	3
1473	26305	3
1474	16203	3
1475	12318	3
1476	21230	3
1477	12374	3
1478	32223	3
1479	34244	3
1480	11505	3
1481	26344	3
1482	12320	3
1483	15315	3
1484	19000	3
1485	19103	3
1486	18244	3
1487	22201	3
1488	15312	3
1489	37203	3
1491	36221	3
1492	34229	3
1493	23210	3
1494	18210	3
1495	18407	3
1496	18412	3
1497	37228	3
1498	31315	3
1499	23255	3
1500	31253	3
1501	19215	3
1502	21213	3
1503	15352	3
1504	23000	3
1505	23101	3
1506	23102	3
1507	23103	3
1508	23104	3
1509	23105	3
1510	23106	3
1511	23107	3
1512	23108	3
1513	23109	3
1514	23110	3
1515	23111	3
1516	23112	3
1517	23113	3
1518	23114	3
1519	23115	3
1520	38228	3
1521	18438	3
1522	11225	3
1523	38227	3
1524	19227	3
1525	18333	3
1526	42000	3
1527	34120	3
1528	14060	3
1529	10510	3
1530	38260	3
1532	17561	3
1533	38288	3
1534	11553	3
1535	20000	3
1536	12060	3
1537	22432	3
1538	20540	3
1539	70030	3
1540	21222	3
1541	18325	3
1542	11405	3
1543	22350	3
1544	23050	3
1545	31304	3
1546	11410	3
1547	11255	3
1548	23201	3
1549	24428	3
1550	25000	3
1551	21000	3
1552	15341	3
1553	34222	3
1554	34219	3
1555	34324	3
1556	24161	3
1557	35218	3
1558	35246	3
1559	19221	3
1560	19327	3
1561	19256	3
1562	19332	3
1563	19307	3
1564	19233	3
1565	19232	3
\.


--
-- Data for Name: professional_degrees; Type: TABLE DATA; Schema: private; Owner: postgres
--

COPY private.professional_degrees (name, id, description) FROM stdin;
I	1	\N
II	2	\N
III	3	\N
IV	4	\N
V	5	\N
VI	6	\N
VII	7	\N
\.


--
-- Data for Name: qualification_levels; Type: TABLE DATA; Schema: private; Owner: postgres
--

COPY private.qualification_levels (id, name, code, description) FROM stdin;
3	Ниво 3 (средње стручно образовање, 3 године)	30	Трећи ниво (ниво 3), који се стиче завршавањем средњег стручног образовања у трогодишњем трајању, односно неформалним образовањем одраслих у трајању од најмање 960 сати обуке; услов за стицање овог нивоа је претходно стечен ниво 1 НОКС-а;\r\nЕквиваленција трећем нивоу (нивоу 3) – средње образовање у трајању од три године, које је до ступања на снагу Закона о Националном оквиру квалификација РС одговарало трећем степену стручне спреме;
4	Ниво 4 (средње гимназијско образовање, 4 године)	40	Четврти ниво (ниво 4), који се стиче завршавањем средњег гимназијског образовања у четворогодишњем трајању; услов за стицање овог нивоа је претходно стечен ниво 1 НОКС-а;\r\nЕквиваленција четвртом нивоу (нивоу 4) – средње образовање у трајању од четири године, које је до ступања на снагу Закона о Националном оквиру квалификација РС одговарало четвртом степену стручне спреме;
5	Ниво 4 (средње стручно образовање, 4 године)	41	Четврти ниво (ниво 4), који се стиче завршавањем средњег стручног и уметничког образовања у четворогодишњем трајању; услов за стицање овог нивоа је претходно стечен ниво 1 НОКС-а;\r\nЕквиваленција четвртом нивоу (нивоу 4) – образовање стечено у школи за талентоване ученике, које је до ступања на снагу Закона о Националном оквиру квалификација РС одговарало четвртом степену стручне спреме;
6	Ниво 5 (мајсторско или специјалистичко послесредње образовање, 3+2 или 4+1 година)	50	Пети ниво (ниво 5), који се стиче завршавањем мајсторског односно специјалистичког образовања у трајању од две односно једне године и неформалним образовањем одраслих у трајању од најмање шест месеци; услов за стицање овог нивоа је претходно стечен ниво 3 односно ниво 4 НОКС-а, а за стицање кроз неформално образовање одраслих претходно стечен ниво 4 НОКС-а;\r\nЕквиваленција петом нивоу (нивоу 5) – школовање за специјализацију, које је до ступања на снагу Закона о Националном оквиру квалификација РС одговарало петом степену стручне спреме;
8	Ниво 6, подниво 6.1 (ОСС, 180 ЕСПБ)	61	Шести ниво, подниво један (ниво 6.1), који се стиче завршавањем основних струковних студија (у даљем тексту: ОСС) обима од 180 ЕСПБ, означава се са 6.1 С; услов за стицање овог нивоа је претходно стечен ниво 4 НОКС-а и положена општа, стручна односно уметничка матура, у складу са законима који уређују средње образовање и васпитање и високо образовање;\r\nЕквиваленција шестом нивоу, подниво један (нивоу 6.1) – стручни назив стечен завршавањем студија на вишој школи у трајању до три године, који је до ступања на снагу Закона о Националном оквиру квалификација РС у погледу права која из њега произлазе био изједначен са стручним називом првог степена струковних студија;
1	Ниво 1 (основно образовање)	10	Први ниво (ниво 1), који се стиче завршавањем основног образовања и васпитања, основног образовања одраслих, основног музичког односно основног балетског образовања и васпитања;\r\n\r\nЕквиваленција првом нивоу (ниво 1) – основно образовање стечено на основу прописа који су важили до ступања на снагу Закона о Националном оквиру квалификација РС („Службени гласник РСˮ, број 27/18);
9	Ниво 6, подниво 6.2 (ОАС, 240 ЕСПБ)	62	Шести ниво, подниво два (ниво 6.2), који се стиче завршавањем ОАС обима од најмање 240 ЕСПБ, означава се са 6.2 А; услов за стицање овог нивоа је претходно стечен ниво 4 НОКС-а и положена општа, стручна односно уметничка матура, у складу са законима који уређују средње образовање и васпитање и високо образовање, односно ниво 6.1 (ОСС обима 180 ЕСПБ);
10	Ниво 6, подниво 6.2 (ОСС, 240 ЕСПБ)	63	Шести ниво, подниво два (ниво 6.2), који се стиче завршавањем специјалистичких струковних студија обима од најмање 60 ЕСПБ, означава се са 6.2 С; услов за стицање овог нивоа је претходно стечен ниво 4 НОКС-а и положена општа, стручна односно уметничка матура, у складу са законима који уређују средње образовање и васпитање и високо образовање, односно ниво 6.1 (ОСС обима 180 ЕСПБ);
11	Ниво 7, подниво 7.1 (МАС, 300–360 ЕСПБ)	70	Седми ниво, подниво један (ниво 7.1), који се стиче завршавањем интегрисаних акадeмских студија обима од 300 до 360 ЕСПБ, мастер академских студија (у даљем тексту: МАС) обима од најмање 60 ЕСПБ, уз претходно остварене ОАС обима 240 ЕСПБ, МАС обима од најмање 120 ЕСПБ (уз претходно остварене ОАС обима 180 ЕСПБ), означава се са 7.1 А;\r\nЕквиваленција седмом нивоу, подниво један (нивоу 7.1) – стручни назив стечен завршавањем основних студија на факултету у трајању од четири до шест година, који је до ступања на снагу Закона о Националном оквиру квалификација РС у погледу права која из њега произлазе био изједначен са академским називом мастер, односно дипломирани мастер;
12	Ниво 7, подниво 7.1 (МСС, 300 ЕСПБ)	71	Седми ниво, подниво један (ниво 7.1), који се стиче завршавањем мастер струковних студија обима од најмање 120 ЕСПБ (уз претходно остварене ОСС обима 180 ЕСПБ), означава се са 7.1 С;\r\nЕквиваленција седмом нивоу, подниво један (нивоу 7.1) – стручни назив стечен завршавањем специјалистичких струковних студија другог степена у складу са Законом о високом образовању;
13	Ниво 7, подниво 7.2 (САС, +60 ЕСПБ)	72	Седми ниво, подниво два (ниво 7.2), који се стиче завршавањем специјалистичких академских студија обима од најмање 60 ЕСПБ (уз претходно остварене мастер академске студије);\r\nЕквиваленција седмом нивоу, подниво два (нивоу 7.2) – академски назив стечен завршавањем специјалистичких студија на факултету, који је до ступања на снагу Закона о Националном оквиру квалификација РС у погледу права која из њега произлазе био изједначен са академским називом специјалисте другог степена академских студија; \r\nЕквиваленција седмом нивоу, подниво два (нивоу 7.2) – академски назив магистра наука стечен завршавањем магистарских студија, односно одбраном магистарске тезе, који је до ступања на снагу Закона о Националном оквиру квалификација РС у погледу права која из њега произлазе био изједначен са научним називом магистра наука;
14	Ниво 8 (докторске студије, +180 ЕСПБ)	80	Осми ниво (ниво 8), који се стиче завршавањем докторских студија обима 180 ЕСПБ (уз претходно завршене интегрисане академске, односно мастер академске студије);\r\nЕквиваленција осмом нивоу (нивоу 8) – научни степен доктора наука стечен завршавањем докторских студија, односно одбраном докторске дисертације, који је до ступања на снагу Закона о Националном оквиру квалификација РС у погледу права која из њега произлазе био изједначен са научним називом доктора наука.
7	Ниво 6, подниво 6.1 (ОАС, 180 ЕСПБ)	60	Шести ниво, подниво један (ниво 6.1), који се стиче завршавањем основних академских студија (у даљем тексту: ОАС) обима од најмање 180 ЕСПБ, означава се са 6.1 А; услов за стицање овог нивоа је претходно стечен ниво 4 НОКС-а и положена општа, стручна односно уметничка матура, у складу са законима који уређују средње образовање и васпитање и високо образовање;\r\n\r\nЕквиваленција шестом нивоу, подниво један (нивоу 6.1) – стручни назив стечен завршавањем дела студијског програма основних студија на факултету, чијим се завршавањем стиче први степен високог образовања, а који је до ступања на снагу Закона о Националном оквиру квалификација РС у погледу права која из њега произлазе био изједначен са стручним називом основних академских студија обима од најмање 180 ЕСПБ бодова;  \r\n\r\nЕквиваленција шестом нивоу, подниво један (нивоу 6.1) – стручни назив стечен завршавањем основних студија на факултету у трајању од три године, који је до ступања на снагу Закона о Националном оквиру квалификација РС у погледу права која из њега произлазе био изједначен са стручним називом основних академских студија обима од најмање 180 ЕСПБ бодова;
2	Ниво 2 (стручно оспособљавање или неформално образовање)	20	Други ниво (ниво 2), који се стиче стручним оспособљавањем у трајању до једне године, образовањем за рад у трајању до две године, односно неформалним образовањем одраслих у трајању 120–360 сати обуке; услов за стицање овог нивоа је претходно стечен ниво 1 НОКС-а;\r\n\r\nЕквиваленција другом нивоу (нивоу 2) – стручно оспособљавање у трајању од годину дана које је до ступања на снагу Закона о Националном оквиру квалификација РС одговарало првом степену стручне спреме и oбразовање за рад у трајању од две године, које је до ступања на снагу Закона о Националном оквиру квалификација РС одговарало другом степену стручне спреме;
\.


--
-- Data for Name: reasons_for_enforcement_training; Type: TABLE DATA; Schema: private; Owner: postgres
--

COPY private.reasons_for_enforcement_training (id, code, name, description) FROM stdin;
\.


--
-- Data for Name: required_medical_abilities; Type: TABLE DATA; Schema: private; Owner: postgres
--

COPY private.required_medical_abilities (id, name, description) FROM stdin;
\.


--
-- Data for Name: safe_file_extensions; Type: TABLE DATA; Schema: private; Owner: postgres
--

COPY private.safe_file_extensions (id, name, description) FROM stdin;
1	odt	OpenDocument Text
2	pptx	
3	pdf	
4	jpg	
5	png	
8	docx	\N
9	ods	\N
10	zip	\N
11	txt	\N
12	rtf	\N
13	xls	\N
14	rar	\N
15	doc	\N
16	dwg	\N
17	tiff	\N
18	backup	\N
19	custom	\N
20	html~	\N
21	js~	\N
22	css~	\N
23	exe	\N
24	html	\N
25	eml	\N
26	xlsx	\N
27	htm	\N
28	dll	\N
29	php	\N
30	phps	\N
31	gif	\N
32	css	\N
33	js	\N
34	dia	\N
35	sql	\N
36	sh~	\N
37	svn-base	\N
38	layout	\N
39	h	\N
40	msi	\N
41	swf	\N
42	sh	\N
43	fbp	\N
44	cbp	\N
45	depend	\N
46	txt~	\N
47	sql~	\N
48	bak	\N
49	mwb	\N
50	ttf	\N
51	7z	\N
52	psd	\N
53	otf	\N
54	db	\N
55	c	\N
56	conf	\N
57	ldif	\N
58	dlz	\N
59	zone	\N
60	reg	\N
61	inf	\N
62	update	\N
63	pm	\N
64	msg	\N
65	dat	\N
66	3pm	\N
67	so	\N
68	pc	\N
69	py	\N
70	tif	\N
71	jar	\N
72	cpp	\N
73	o	\N
74	crt	\N
75	odg	\N
76	xlsb	\N
77	wmv	\N
78	hso2	\N
79	ppt	\N
80	dwf	\N
81	xlt	\N
82	ebp	\N
83	jpeg	\N
84	json	\N
85	out	\N
86	vcf	\N
87	xml	\N
88	log	\N
89	xcf	\N
90	emz	\N
91	z01	\N
92	z02	\N
93	asc	\N
94	iso	\N
95	wmz	\N
96	dot	\N
97	bmp	\N
98	odp	\N
99	mpp	\N
100	gz	\N
101	mp4	\N
102	dwfx	\N
103	ai	\N
104	mso	\N
105	ldt	\N
106	csv	\N
107	tar.gz	\N
\.


--
-- Data for Name: safe_work_dangers; Type: TABLE DATA; Schema: private; Owner: postgres
--

COPY private.safe_work_dangers (id, code, description) FROM stdin;
3	01	недовољна безбедност због ротирајућих или покретних делова
\.


--
-- Data for Name: source_injuries; Type: TABLE DATA; Schema: private; Owner: postgres
--

COPY private.source_injuries (id, code, name, description, parent_id) FROM stdin;
3	00.00	нема информација	\N	\N
4	00.99	друга позната ситуација која није наведена у групи 00	\N	3
5	01.00	зграде, грађевине, површине - у нивоу земље (затворене или отворене, непокретне или покретне, привремене или не) - није назначено	\N	\N
6	01.01	делови зграде, делови грађевине - врата, зидови, преграде, препреке (прозори, итд.)	\N	5
7	01.02	површине у нивоу земље - приземље и спратови (затворени или отворени, пољопривредно земљиште, спортски терени, клизави подови и закрчени пролази)	\N	5
8	01.03	површине у нивоу земље - плутајуће (сплавови)	\N	5
9	01.99	друге зграде, грађевине и површине - на истом нивоу, које нису наведене у групи 01	\N	5
10	02.00	зграде, грађевине и површине - изнад нивоа земље (затворене или отворене, непокретне или покретне, привремене или не) - није назначено	\N	\N
11	02.01	делови зграде изнад нивоа земље - непокретни (кровови, терасе, врата, прозори и степеништа)	\N	10
12	02.02	грађевине, површине изнад нивоа земље - непокретне (укључујући пролазе, фиксиране мердевине и пилоне)	\N	10
13	02.03	грађевине, површине изнад нивоа земље - покретне (укључујући мобилне мердевине и платформе за дизање)	\N	10
14	02.04	грађевине, површине изнад нивоа земље - привремене (укључујући привремене скеле)	\N	10
15	02.05	грађевине, површине изнад нивоа земље - плутајуће (укључујући платформе за бушење и скеле на баржама)	\N	10
16	02.99	друге зграде, грађевине и површине - изнад нивоа земље, које нису наведене у групи 02	\N	10
17	03.00	зграде, грађевине и површине - испод нивоа земље (затворене или отворене) - није назначено	\N	\N
18	03.01	ископине, ровови, бунари, отвори, одрони и гаражне јаме	\N	17
19	03.02	подземне површине и тунели	\N	17
20	03.03	подводна средина	\N	17
21	03.99	друге зграде, грађевине и површине - испод нивоа земље, које нису наведене у групи 03	\N	17
22	04.00	системи за снабдевање и дистрибуцију материјала и цевне мреже - није назначено	\N	\N
23	04.01	системи за снабдевање и дистрибуцију материјала и цевна мрежа - непокретна - за гас, ваздух, течност, чврст материјал - укључујући бункере	\N	22
24	04.02	системи за снабдевање и дистрибуцију материјала и цевна мрежа - покретна	\N	22
25	04.03	канализација и одводи	\N	22
26	04.99	други познати системи за снабдевање и дистрибуцију материјала и цевна мрежа, који нису наведени у групи 04	\N	22
27	05.00	мотори, системи за пренос и складиштење енергије - није назначено	\N	\N
28	05.01	мотори и енергетски генератори (термални, електрични и радијациони)	\N	27
29	05.02	системи за пренос и складиштење енергије (механички, пнеуматски, хидраулични и електрични, укључујући акумулаторе и батерије)	\N	27
30	05.99	други познати мотори, системи за пренос и складиштење енергије, који нису наведени у групи 05	\N	27
31	06.00	ручни алат без напајања	\N	\N
32	06.01	ручни алат без напајања - за тестерисање	\N	31
33	06.02	ручни алат без напајања - за сечење и раздвајање (укључујући маказе, велике маказе и јаке орезне маказе)	\N	31
34	06.03	ручни алат без напајања - за резбарење, прављење жлебова, издубљивање, скраћивање, резање и одсецање	\N	31
35	06.04	ручни алат без напајања - за стругање, шмирглање и глачање	\N	31
36	06.05	ручни алат без напајања - за бушење, нитовање и шрафљење	\N	31
37	06.06	ручни алат без напајања - за закуцавање ексера и закивање	\N	31
38	06.07	ручни алат без напајања - за шивење и плетење	\N	31
39	06.08	ручни алат без напајања - за спајање и лепљење	\N	31
40	06.09	ручни алат без напајања - за вађење материјала и радове на земљи (укључујући пољопривредни алат)	\N	31
41	06.10	ручни алат без напајања - за наношење воска, подмазивање, прање и чишћење	\N	31
42	06.11	ручни алат без напајања - за кречење	\N	31
43	06.12	ручни алат без напајања - за постављање и притезање	\N	31
44	06.13	ручни алат без напајања - за рад у кухињи (изузев ножева)	\N	31
45	06.14	ручни алат без напајања - за медицински и хируршки рад - оштар, за сечење	\N	31
46	06.15	ручни алат без напајања - за медицински и хируршки рад - осим сечења, друго	\N	31
47	06.99	други ручни алат без напајања који није наведен у групи 06	\N	31
48	07.00	механички ручни алат - није назначено	\N	\N
49	07.01	механички ручни алат - за тестерисање	\N	48
50	07.02	механички ручни алат - за сечење, раздвајање (укључујући маказе, велике маказе и јаке орезне маказе)	\N	48
51	07.03	механички ручни алат - за резбарење, прављење жлебова, издубљивање, скраћивање, резање и одсецање	\N	48
52	07.04	механички ручни алат - за стругање, шмирглање и глачање	\N	48
53	07.05	механички ручни алат - за бушење, нитовање и шрафљење	\N	48
54	07.06	механички ручни алат - за закуцавање ексера и закивање	\N	48
55	07.07	механички ручни алат - за шивење и плетење	\N	48
56	07.08	механички ручни алат - за спајање и лепљење	\N	48
57	07.09	механички ручни алат - за вађење материјала и рад на земљи (укључујући пољопривредни алат и алат за разбијање бетона)	\N	48
58	07.10	механички ручни алат - за наношење воска, подмазивање, прање и чишћење (укључујући чистач за усисавање под високим притиском)	\N	48
59	07.11	механички ручни алат - за кречење	\N	48
60	07.12	механички ручни алат - за постављање и притезање	\N	48
61	07.13	механички ручни алат - за рад у кухињи (изузев ножева)	\N	48
62	07.14	механички ручни алат - за загревање (укључујући сушаче, пиштоље са пламеном и пегле)	\N	48
63	07.15	механички ручни алат - за медицински и хируршки рад - оштар, за сечење	\N	48
64	07.16	механички ручни алат - за медицински и хируршки рад - осим сечења, друго	\N	48
65	07.17	пнеуматски пиштољи (без спецификације алата)	\N	48
66	07.99	други ручни механички алат који није наведен у групи 07	\N	48
67	08.00	ручни алат без спецификације извора напајања - није назначено	\N	\N
68	08.01	ручни алат без спецификације извора напајања - за тестерисање	\N	67
69	08.02	ручни алат без спецификације извора напајања - за сечење и раздвајање (укључујући маказе, велике маказе и јаке орезне маказе)	\N	67
70	08.03	ручни алат без спецификације извора напајања - за резбарење, прављење жлебова, издубљивање, скраћивање, резање и одсецање	\N	67
71	08.04	ручни алат без спецификације извора напајања - за стругање, шмирглање и глачање	\N	67
72	08.05	ручни алат без спецификације извора напајања - за бушење, рачвање и шрафљење	\N	67
73	08.06	ручни алат без спецификације извора напајања - за бушење, нитовање и шрафљење	\N	67
74	08.07	ручни алат без спецификације извора напајања - за шивење и плетење	\N	67
75	08.08	ручни алат без спецификације извора напајања - за спајање и лепљење	\N	67
76	08.09	ручни алат без спецификације извора напајања - за вађење материјала и рад на земљи (укључујући пољопривредни алат)	\N	67
77	08.10	ручни алат без спецификације извора напајања - за наношење воска, подмазивање, прање и чишћење	\N	67
78	08.11	ручни алат без спецификације извора напајања - за кречење	\N	67
79	08.12	ручни алат без спецификације извора напајања - за постављање и притезање	\N	67
80	08.13	ручни алат без спецификације извора напајања - за рад у кухињи (изузев ножева)	\N	67
81	08.14	ручни алат без спецификације извора напајања - за медицински и хируршки рад - оштар, за сечење	\N	67
82	08.15	ручни алат без спецификације извора напајања - за медицински и хируршки рад - осим сечења, друго	\N	67
83	08.99	други ручни алат без спецификације извора напајања, који није наведен у групи 08	\N	67
84	09.00	машине и опрема - покретне или мобилне - није назначено	\N	\N
85	09.01	покретне или мобилне машине за вађење материјала из земље - рудници, каменоломи и постројења за изградњу и станоградњу	\N	84
86	09.02	покретне или мобилне машине за рад на земљи - пољопривреда	\N	84
87	09.03	покретне или мобилне машине за градилишта	\N	84
88	09.04	мобилне машине за чишћење подова	\N	84
89	09.99	друге покретне или мобилне машине и опрема које нису наведене у групи 09	\N	84
90	10.00	машине и опрема - фиксна - није назначено	\N	\N
91	10.01	фиксне машине за ископавање материјала или рад на земљи	\N	90
92	10.02	машине за припремање материјала, ломљење, дробљење, филтрирање, одвајање и мешање	\N	90
93	10.03	машине за прераду материјала - хемијски процеси (реактивни и ферметички процеси)	\N	90
94	10.04	машине за прераду материјала - врући процеси (рерне, сушачи и пећи)	\N	90
95	10.05	машине за прераду материјала - хладни процеси (хладна производња)	\N	90
96	10.06	машине за прераду материјала - други процеси	\N	90
97	10.07	машине за обликовање - обрада и ломљење	\N	90
98	10.08	машине за обликовање - пресе за цеђење, роловање и ваљак (укључујући пресе за папир)	\N	90
99	10.09	машине за обликовање - убризгавањем, истискивањем, дувањем, вртењем, обликовањем, топљењем и ливењем	\N	90
100	10.10	машински алати - за глодање, третирање површина, млевење, полирање, окретање и бушење	\N	90
101	10.11	машински алати - за тестерисање	\N	90
102	10.12	машински алати - за сечење, раздвајање и одсецање (укључујући сечење по калупу, машина за шишање, маказе и опрема за сечење кисеоником)	\N	90
103	10.13	машине за површинску обраду - чишћење, прање, сушење, кречење и штампање	\N	90
104	10.14	машине за површинску обраду - цинковање и електролитичко полирање	\N	90
105	10.15	машине за склапање (варење, лепљење, закуцавање ексера, шрафљење, нитовање, окретање, спајање жицом и шивење)	\N	90
106	10.16	машине за паковање и машине за увијање (пуњење, означавање и затварање)	\N	90
107	10.17	друге машине за специфичну индустрију (разне машине за мониторинг и тестирање)	\N	90
108	10.18	специфичне машине које се користе у пољопривреди, које нису наведене	\N	90
109	10.99	друге фиксне машине и опрема које нису наведене у групи 10	\N	90
110	11.00	системи за пренос, транспорт и складиштење - није назначено	\N	\N
111	11.01	фиксна опрема и системи за преношење, континуирано руковање (појасеви, покретне степенице, жичаре, итд.)	\N	110
112	11.02	лифтови - дизалице, корпе и справе за дизање	\N	110
113	11.03	фиксни кранови, мобилни кранови, кранови на возилима, кранови за подизање и уређаји за подизање са висећим теретом	\N	110
114	11.04	мобилни уређаји за руковање, камиони, ручна колица, виљушкари, итд.	\N	110
115	11.05	опрема за дизање, обезбеђивање, хватање и сл. (укључујући ременик, куке и конопце)	\N	110
116	11.06	системи за складиштење, опрема за паковање, контејнери (силоси и танкери) - фиксни (танкери, каце, контејнери, итд.)	\N	110
117	11.07	системи за складиштење, опрема за паковање и контејнери - мобилни	\N	110
118	11.08	средства за складиштење, рафови, регали за палете и палете	\N	110
119	11.09	разна паковања, мале и средње величине - мобилна (корпе, разни контејнери, боце, гајбе и апарати за гашење пожара)	\N	110
120	11.99	други системи за пренос, транспорт и складиштење који нису наведени у групи 11	\N	110
121	12.00	копнена возила - није назначено	\N	\N
122	12.01	возила - тешка: камиони и аутобуси за превоз путника	\N	121
123	12.02	возила - лака: комби возило за транспорт робе или аутомобил за превоз путника	\N	121
124	12.03	возила - на два или три точка, са или без напона	\N	121
125	12.04	скије и ролери	\N	121
126	12.99	друга копнена возила која нису наведена у групи 12	\N	121
127	13.00	друга транспортна возила - није назначено	\N	\N
128	13.01	возила - на шинама, за транспорт робе	\N	127
129	13.02	возила - на шинама, за превоз путника	\N	127
130	13.03	возила - поморска - за транспорт робе	\N	127
131	13.04	возила - поморска - за превоз путника	\N	127
132	13.05	возила - поморска - за пецање	\N	127
133	13.06	ваздухоплови - за транспорт робе	\N	127
134	13.07	ваздухоплови - за превоз путника	\N	127
135	13.99	друга транспортна средства која нису наведена у групи 13	\N	127
136	14.00	материјали, предмети, производи, делови машина или опреме, шут и прашина - није назначено	\N	\N
137	14.01	грађевински материјал - монтажне шкољке, оплата, носачи, греде, цигле, цреп, итд.	\N	136
138	14.02	делови машине, делови возила: шасија, корито мотора, полуге, точкови, итд.	\N	136
139	14.03	машински делови или компоненте и машински алати (укључујући делове)	\N	136
140	14.04	уређаји за спајање: навртњи, завртњи, шрафови, ексери, итд.	\N	136
141	14.05	честице, прашина, парчићи, фрагменти, крхотине и други остаци	\N	136
142	14.06	пољопривредни производи (укључујући семе, сламу и друге пољопривредне производе)	\N	136
143	14.07	производи који се користе у пољопривреди и сточарству (укључујући ђубрива и сточну храну)	\N	136
144	14.08	производи који се складиште - укључујући предмете и паковања у делу за складиштење	\N	136
145	14.09	производи који се складиште - у ролнама и калемовима	\N	136
146	14.10	терет - који се преноси или превози уређајем којим се механички управља	\N	136
147	14.11	терет који виси са уређаја за дизање (крана)	\N	136
148	14.12	терет - којим се ручно рукује	\N	136
149	14.99	други материјали, предмети, производи и делови машина који нису наведени у групи 14	\N	136
150	15.00	хемијске, експлозивне, радиоактивне и биолошке супстанце - није назначено	\N	\N
151	15.01	супстанце - каустичне и корозивне (у чврстом, течном или гасовитом стању)	\N	150
152	15.02	супстанце - штетне и токсичне (у чврстом, течном или гасовитом стању)	\N	150
153	15.03	супстанце - запаљиве (у чврстом, течном или гасовитом стању)	\N	150
154	15.04	супстанце - експлозивне и реактивне (у чврстом, течном или гасовитом стању)	\N	150
155	15.05	гасови и испарења без специфичних ефеката (инертни и загушујући)	\N	150
156	15.06	супстанце - радиоактивне	\N	150
157	15.07	супстанце - биолошке	\N	150
158	15.08	супстанце и материјали - без специфичног ризика (вода и инертни материјали)	\N	150
159	15.99	друге хемијске, експлозивне, радиоактивне и биолошке супстанце које нису наведене у групи 15	\N	150
160	16.00	заштитни уређаји и опрема - није назначено	\N	\N
161	16.01	заштитни уређаји - на машинама	\N	160
162	16.02	заштитни уређаји - индивидуални	\N	160
163	16.03	уређаји и опрема за хитне случајеве	\N	160
164	16.99	други заштитни уређаји и опрема која није наведена у групи 16	\N	160
165	17.00	канцеларијска опрема, спортска опрема, оружје и апарати за домаћинство - није назначено	\N	\N
166	17.01	намештај	\N	165
167	17.02	опрема - рачунари, канцеларијски уређаји, репрографички и комуникацијски уређаји	\N	165
168	17.03	опрема - за подучавање, писање, цртање, укључујући писаћу машину, печати, апарати за увеличавање и апарати за снимање	\N	165
169	17.04	средства и опрема за спорт и игру	\N	165
170	17.05	оружје	\N	165
171	17.06	личне ствари и одећа	\N	165
172	17.07	музички инструменти	\N	165
173	17.08	опрема за домаћинство, алати, предмети (за професионалну употребу)	\N	165
174	17.99	друга канцеларијска опрема, спортска опрема и оружје које нису наведене у групи 17	\N	165
175	18.00	живи организми и људи - није назначено	\N	\N
176	18.01	дрвеће, биљке и усеви	\N	175
177	18.02	животиње - домаће и за узгој	\N	175
178	18.03	животиње - дивље животиње, инсекти и змије	\N	175
179	18.04	микроорганизми	\N	175
180	18.05	инфективни вирусни узрочници	\N	175
181	18.06	људи	\N	175
182	18.99	други живи организми који нису наведени у групи 18	\N	175
183	19.00	расути отпад - није назначено	\N	\N
184	19.01	расути отпад - од сировина, производа, материјала и предмета	\N	183
185	19.02	расути отпад - од хемикалија	\N	183
186	19.03	расути отпад - од биолошких супстанци, биљака и животиња	\N	183
187	19.99	други расути отпад који није наведен у групи 19	\N	183
188	20.00	физички феномени и природни елементи - није назначено	\N	\N
189	20.01	физички феномен - бука, природна радијација, светлост, пресуризација, депресуризација и притисак	\N	188
190	20.02	природни и атмосферски елементи (укључујући воду, блато, кишу, град, снег, лед, ветар, итд.)	\N	188
191	20.03	природне катастрофе (поплаве, земљотреси, пожари)	\N	188
192	20.99	други физички феномени и елементи који нису наведени у групи 20	\N	188
193	99.00	други извор повреде који није наведен у класификацији	\N	\N
\.


--
-- Data for Name: streets; Type: TABLE DATA; Schema: private; Owner: postgres
--

COPY private.streets (id, name, city_id, postal_code_id) FROM stdin;
1	Gospodar Jovanova	1668	151
\.


--
-- Data for Name: treasury_office_units; Type: TABLE DATA; Schema: private; Owner: postgres
--

COPY private.treasury_office_units (id, name, type, code, control_number, main_branch_id, sub_branch_id, bzr_code, country_id) FROM stdin;
124	Београд-Гроцка 	1	012	62	20	106	70122	3
122	Београд-Звездара 	1	022	32	20	\N	70149	3
103	Београд-Земун	1	021	35	17	\N	70157	3
71	Страгари	1	123	20	11	\N	\N	3
8	Београд-Сопот	1	017	47	2	41	70238	3
42	Гора 	1	331	75	10	\N	90336	3
128	Ковин 	1	217	29	21	\N	80225	3
127	Алибунар 	1	202	74	21	107	80039	3
130	Бабушница 	1	006	80	22	109	70050	3
73	Баточина	1	008	74	11	75	70076	3
115	Бач	1	204	68	19	\N	80055	3
116	Бачка Паланка 	1	205	65	19	102	80063	3
22	Бела Црква	1	209	53	5	50	80098	3
18	Бечеј	1	208	56	4	48	80110	3
84	Бојник	1	025	23	14	85	70297	3
23	Бољевац	1	026	20	6	51	70319	3
24	Бор	1	027	17	6	52	70327	3
10	Босилеград	1	028	14	3	42	70335	3
82	Варварин 	1	108	65	13	83	70378	3
89	Крупањ	1	051	42	15	89	70661	3
1	Лајковац	1	055	30	1	35	70700	3
74	Лапово	1	121	26	11	75	71277	3
87	Лебане 	1	057	24	14	87	70718	3
61	Липљан 	1	312	35	10	72	90166	3
25	Мајданпек	1	063	06	6	53	70785	3
91	Мали Зворник	1	065	97	15	91	70793	3
134	Мало Црниће	1	066	94	23	\N	70807	3
88	Медвеђа 	1	067	91	14	88	70815	3
3	Мионица	1	069	85	1	37	70831	3
28	Неготин	1	072	76	6	57	70840	3
31	Нова Црња 	1	220	20	7	60	80250	3
62	Ново Брдо 	1	329	81	10	72	90182	3
57	Зубин Поток 	1	324	96	10	70	90093	3
12	Владичин Хан 	1	111	56	3	44	70416	3
85	Власотинце	1	113	50	14	86	70424	3
9	Врањска Бања 	1	130	96	3	\N	71358	3
16	Врбас	1	240	57	4	\N	80462	3
92	Гаджин Хан 	1	039	78	16	\N	70467	3
59	Глоговац	1	304	59	10	72	90034	3
55	Гњилане 	1	305	56	10	69	90042	3
118	Жабаљ	1	243	48	19	104	80136	3
30	Житиште 	1	244	45	7	59	80144	3
45	Звечан 	1	330	78	10	\N	90352	3
46	Исток	1	306	53	10	\N	90107	3
39	Кикинда 	1	215	35	9	\N	80209	3
26	Кладово	1	043	66	6	54	70572	3
47	Клина 	1	308	47	10	\N	90123	3
27	Књажевац	1	045	60	6	55	70602	3
60	Косово Поље 	1	328	84	10	72	90131	3
56	Косовска Каменица 	1	309	44	10	69	90140	3
44	Ђаковица	1	303	62	10	\N	90085	3
63	Обилић	1	327	87	10	72	90204	3
4	Осечина 	1	076	64	1	38	70882	3
113	Петроварадин 	1	247	36	19	\N	80519	3
50	Пећ	1	314	29	10	\N	90239	3
129	Пирот	1	079	55	22	\N	70939	3
51	Призрен 	1	317	20	10	\N	90255	3
101	Ражањ	1	088	28	16	93	71005	3
76	Рача	1	086	34	11	77	71013	3
108	Рашка 	1	087	31	18	98	71021	3
102	Сврљиг	1	098	95	16	94	71064	3
109	Сјеница 	1	091	19	18	99	71072	3
29	Сокобања 	1	094	10	6	58	71129	3
52	Србица 	1	318	17	10	\N	90271	3
106	Стара Пазова 	1	235	72	17	96	80420	3
53	Сува Река 	1	319	14	10	\N	90280	3
119	Темерин 	1	238	63	19	104	80446	3
77	Топола	1	101	86	11	78	71153	3
15	Трговиште 	1	102	83	3	47	71161	3
79	Трстеник	1	103	80	12	80	71170	3
110	Тутин 	1	104	77	18	100	71188	3
38	Ћуприја 	1	033	96	8	67	71200	3
5	Уб	1	105	74	1	39	71218	3
68	Штимље	1	325	93	10	73	90310	3
70	Крагујевац	1	049	48	11	\N	70645	3
112	Нови Сад 	1	223	11	19	\N	80284	3
199	Београд	2	501	50	29	\N	\N	3
200	Нови Сад 	2	511	20	19	\N	\N	3
201	Ниш	2	521	87	16	\N	\N	3
202	Крагујевац	2	531	57	11	\N	\N	3
203	Приштина 	2	541	27	10	\N	\N	3
204	Војводина 	3	581	04	19	\N	\N	3
206	Република Србија 	4	601	41	29	\N	\N	3
161	Ада	1	201	77	30	131	80012	3
80	Александровац	1	001	95	13	81	70017	3
207	Севојно	1	131	93	31	\N	71366	3
100	Алексинац	1	002	92	16	92	70025	3
131	Бела Паланка 	1	009	71	22	110	70084	3
54	Витина 	1	321	08	10	69	90018	3
90	Љубовија 	1	062	09	15	90	70777	3
36	Рековац	1	089	25	8	\N	71030	3
121	Београд-Врачар 	1	020	38	20	\N	70114	3
66	Качаник	1	307	50	10	73	90115	3
125	Ковачица 	1	216	32	21	\N	80217	3
21	Пландиште 	1	228	93	5	\N	80349	3
33	Сечањ	1	230	87	7	62	80373	3
14	Сурдулица	1	095	07	3	46	71137	3
67	Урошевац	1	320	11	10	73	90301	3
205	Косовска Митровица 	3	591	71	10	\N	\N	3
150	Апатин 	1	203	71	27	124	80047	3
72	Аранђеловац	1	003	89	11	74	70033	3
164	Ариље	1	004	86	31	132	70041	3
165	Бајина Башта	1	007	77	31	133	70068	3
117	Бачки Петровац	1	207	59	19	103	80080	3
156	Београд-Савски венац	1	016	50	29	\N	70220	3
157	Беогрд-Стари град	1	018	44	29	\N	70246	3
107	Београд-Сурчин 	1	124	17	17	97	71293	3
173	Београд-Чукарица	1	011	65	33	\N	70254	3
111	Беочин 	1	210	50	19	\N	80101	3
146	Блаце	1	023	29	25	120	70262	3
177	Богатић	1	024	26	34	144	70289	3
81	Брус	1	029	11	13	82	70343	3
148	Велика Плана 	1	109	62	26	122	70386	3
179	Коцељева	1	046	57	34	146	70637	3
185	Краљево	1г	050	45	12	\N	70653	3
186	Крушевац	1г	052	39	13	\N	70670	3
19	Кула 	1	218	26	4	49	80233	3
147	Куршумлија 	1	054	33	25	121	70688	3
139	Кучево 	1	053	36	23	116	70696	3
187	Лесковац	1г	058	21	14	\N	70726	3
171	Лучани	1	060	15	32	139	70742	3
2	Љиг 	1	061	12	1	36	70769	3
158	Мали Иђош	1	219	23	30	\N	80241	3
95	Мерошина 	1	068	88	16	\N	70823	3
142	Нова Варош	1	074	70	24	118	70866	3
40	Нови Кнежевац	1	222	14	9	68	80276	3
189	Нови Пазар	1г	075	67	18	\N	70874	3
183	Зрењанин	1г	242	51	7	\N	80152	3
166	Ивањица 	1	042	69	31	134	70564	3
178	Владимирци 	1	112	53	34	145	70408	3
136	Голубац	1	040	75	23	113	70475	3
170	Горњи Милановац	1	041	72	32	138	70483	3
137	Жабари	1	117	38	23	114	70521	3
138	Жагубица	1	118	35	23	115	70530	3
144	Житорађа	1	119	32	25	\N	70548	3
182	Зајечар	1г	116	41	6	\N	70556	3
105	Инђија 	1	212	44	17	\N	80179	3
153	Ириг	1	213	41	28	127	80187	3
184	Јагодина 	1г	096	04	8	\N	71048	3
75	Кнић	1	044	63	11	76	70599	3
133	Костолац	1	129	02	23	\N	71340	3
151	Оджаци 	1	224	08	27	125	80306	3
190	Панчево 	1г	226	02	21	\N	80314	3
35	Параћин	1	077	61	8	64	70904	3
152	Пећинци 	1	227	96	28	126	80322	3
191	Пожаревац	1г	080	52	23	\N	70947	3
168	Пожега	1	081	49	31	136	70955	3
13	Прешево	1	082	46	3	45	70963	3
143	Прибој 	1	083	43	24	119	70971	3
145	Прокупље	1	085	37	25	\N	70998	3
154	Рума	1	229	90	28	127	80357	3
162	Сента	1	231	84	30	131	80365	3
192	Смедерево 	1г	092	16	26	\N	71099	3
193	Сомбор	1г	232	81	27	\N	80381	3
194	Сремска Митровица 	1г	234	75	28	\N	80403	3
195	Суботица 	1г	236	69	30	\N	80438	3
120	Тител	1	239	60	19	105	80454	3
83	Ћићевац 	1	032	02	13	84	71196	3
196	Ужице 	1г	100	89	31	\N	71145	3
197	Чачак 	1г	034	93	32	\N	71242	3
163	Чока	1	211	47	30	131	80489	3
198	Шабац	1г	099	92	34	\N	71269	3
155	Шид 	1	237	66	28	128	80497	3
97	Ниш-Палилула 	1	127	08	16	\N	71323	3
174	Београд - Барајево	1	010	68	33	141	70092	3
175	Београд-Лазаревац	1	056	27	33	142	70165	3
7	Београд-Младеновац	1	070	82	2	40	70173	3
172	Београд-Раковица 	1	120	29	33	\N	70211	3
34	Деспотовац 	1	036	87	8	63	70491	3
160	Кањижа	1	214	38	30	130	80195	3
140	Петровац на Млави	1	078	58	23	117	70912	3
141	Пријепоље 	1	084	40	24	\N	70980	3
17	Србобран 	1	233	78	4	\N	80390	3
169	Чајетина 	1	035	90	31	137	71234	3
99	Ниш-Црвени крст	1	126	11	16	\N	71315	3
123	Београд-Палилула	1	015	53	20	\N	70203	3
58	Лепосавић	1	311	38	10	71	90158	3
132	Димитровград	1	037	84	22	111	70505	3
64	Подујево	1	315	26	10	72	90247	3
20	Вршац	1	241	54	5	\N	80128	3
43	Дечани	1	301	68	10	\N	90069	3
41	Вучитрн	1	322	05	10	\N	90026	3
11	Бујановац	1	030	08	3	43	70351	3
32	Нови Бечеј	1	221	17	7	61	80268	3
93	Дољевац 	1	038	81	16	\N	70513	3
126	Опово 	1	225	05	21	\N	80292	3
114	Сремски Карловци	1	250	27	19	\N	80411	3
48	Косовска Митровица 	1	310	41	10	\N	90298	3
94	Ниш-Медијана 	1	128	05	16	\N	71331	3
49	Ораховац	1	313	32	10	\N	90212	3
159	Бачка Топола 206	1	206	62	30	129	80071	3
180	Ваљево	1г	107	68	1	\N	70360	3
188	Лозница 	1г	059	18	15	\N	70734	3
181	Врање	1г	114	47	3	\N	70432	3
167	Косјерић	1	048	51	31	135	70629	3
149	Смедеревска Паланка 	1	093	13	26	123	71102	3
69	Штрпце 	1	326	90	10	73	90328	3
176	Београд-Обреновац 	1	014	56	33	143	70190	3
98	Ниш-Пантелеј	1	125	14	16	\N	71307	3
86	Црна Трава 	1	031	05	14	86	71226	3
104	Београд-Нови Београд 	1	013	59	17	\N	70181	3
96	Нишка Бања 	1	122	23	16	\N	71285	3
65	Приштина 	1	316	23	10	72	90263	3
135	Велико Градиште 	1	110	59	23	112	70394	3
78	Врњачка Бања 	1	115	44	12	79	70459	3
37	Свилајнац	1	097	98	8	66	71056	3
6	Београд-Вождовац	1	019	41	2	\N	70106	3
\.


--
-- Data for Name: treasury_offices; Type: TABLE DATA; Schema: private; Owner: postgres
--

COPY private.treasury_offices (id, name, parent_id) FROM stdin;
1	Valjevo	\N
2	Voždovac	\N
3	Vranje	\N
4	Vrbas	\N
5	Vršac	\N
6	Zaječar	\N
7	Zrenjanin	\N
8	Jagodina 	\N
9	Kikinda	\N
10	Kosovska Mitrovica 	\N
11	Kragujevac	\N
12	Kraljevo	\N
13	Kruševac	\N
14	Leskovac	\N
15	Loznica	\N
16	Niš	\N
17	Novi Beograd	\N
18	Novi Pazar	\N
19	Novi Sad	\N
20	Palilula	\N
21	Pančevo	\N
22	Pirot	\N
23	Požarevac	\N
24	Prijepolje	\N
25	Prokuplje	\N
26	Smederevo	\N
27	Sombor	\N
28	Sremska Mitrovica	\N
29	Stari grad	\N
30	Subotica 	\N
31	Užice	\N
32	Čačak 	\N
33	Čukarica 	\N
34	Šabac	\N
35	Lajkovac	1
36	Ljig	1
37	Mionica	1
38	Osečina	1
39	Ub	1
40	Mladenovac	2
41	Sopot	2
42	Bosilegrad	3
43	Bujanovac	3
44	Vladičin Han	3
45	Preševo	3
46	Surdulica 	3
47	Trgovište 	3
48	Bečej	4
49	Kula	4
50	Bela Crkva	5
51	Boljevac	6
52	Bor	6
53	Donji Milanovac	6
54	Kladovo	6
55	Knjaževac	6
56	Majdanpek 	6
57	Negotin	6
58	Sokobanja 	6
59	Žitište 	7
60	Nova Crnja 	7
61	Novi Bečej 	7
62	Sečanj	7
63	Despotovac	8
64	Paraćin	8
65	Rskovac	8
66	Svilajnac	8
67	Ćuprija 	8
68	Novi Kneževac	9
69	Gnjilane 	10
70	Zubin Potok	10
71	Leposavić	10
72	Priština 	10
73	Štrpce	10
74	Aranđelovac	11
75	Batočina 	11
76	Knić	11
77	Rača	11
78	Topola	11
79	Vrnjačka Banja 	12
80	Trstenik	12
81	Aleksandrovac	13
82	Brus	13
83	Varvarin 	13
84	Ćićevac	13
85	Bojnik	14
86	Vlasotince	14
87	Lebane	14
88	Medveđa	14
89	Krupanj	15
90	Ljubovija 	15
91	Mali Zvornik	15
92	Aleksinac	16
93	Ražanj	16
94	Svrljig	16
95	Inđija 	16
96	Stara Pazova 	17
97	Surčin	17
98	Raška 	18
99	Sjenica	18
100	Tutin 	18
101	Bač	19
102	Bačka Palanka 	19
103	Bački Petrovac	19
104	Temerin	19
105	Titel 	19
106	Grocka 	20
107	Alibunar	21
108	Kovip	21
109	Babušnica	22
110	Bela Palanka	22
111	Dimitrovgrad	22
112	Veliko Gradište 	23
113	Golubac	23
114	Žabari	23
115	Žagubica	23
116	Kučevo 	23
117	Petrovac na Mlavi	23
118	Nova Varoš	24
119	Priboj	24
120	Blace	25
121	Kuršumlija 	25
122	Velika Plana 	26
123	Smederevska Palanka 	26
124	Apatin 	27
125	Odžaci 	27
126	Pećinci 	28
127	Ruma 	28
128	Šid 	28
129	Bačka Topola 	30
130	Kanjiža 	30
131	Senta	30
132	Arilje 	31
133	Bajina Bašta	31
134	Ivanjica 	31
135	Kosjerić	31
136	Požega	31
137	Čajetina 	31
138	Gornji Milanovac 	32
139	Guča	32
140	Lučani	32
141	Barajevo	33
142	Lazarevac	33
143	Obrenovac	33
144	Bogatić	34
145	Vladimirci	34
146	Koceljeva	34
\.


--
-- Data for Name: work_codes; Type: TABLE DATA; Schema: private; Owner: postgres
--

COPY private.work_codes (id, code, name, parent_id) FROM stdin;
779	A	POLjOPRIVREDA, ŠUMARSTVO I RIBARSTVO	\N
819	02	Šumarstvo i seča drveća	779
828	03	Ribarstvo i akvakulture	779
836	05	Eksploatacija uglja	835
841	06	Eksploatacija sirove nafte i prirodnog gasa	835
846	07	Eksploatacija ruda metala	835
852	08	Ostalo rudarstvo	835
861	09	Uslužne delatnosti u rudarstvu i geološkim istraživanjima	835
867	10	Proizvodnja prehrambenih proizvoda	866
780	01	Poljoprivredna proizvodnja, lov i prateće uslužne delatnosti	779
835	B	RUDARSTVO	\N
866	C	PRERAĐIVAČKA INDUSTRIJA	\N
902	11	Proizvodnja pića	866
911	12	Proizvodnja duvanskih proizvoda	866
914	13	Proizvodnja tekstila	866
929	14	Proizvodnja odevnih predmeta	866
941	15	Proizvodnja kože i predmeta od kože	866
947	16	Prerada drveta i proizvodi od drveta, plute, slame i pruća, osim nameštaja	866
956	17	Proizvodnja papira i proizvoda od papira	866
966	18	Štampanje i umnožavanje audio i video zapisa	866
974	19	Proizvodnja koksa i derivata nafte	866
979	20	Proizvodnja hemikalija i hemijskih proizvoda	866
1002	21	Proizvodnja osnovnih farmaceutskih proizvoda i preparata	866
1007	22	Proizvodnja proizvoda od gume i plastike	866
1016	23	Proizvodnja proizvoda od ostalih nemetalnih minerala	866
1049	24	Proizvodnja osnovnih metala	866
1071	25	Proizvodnja metalnih proizvoda, osim mašina i uređaja	866
1097	26	Proizvodnja računara, elektronskih i optičkih proizvoda	866
1116	27	Proizvodnja električne opreme	866
1133	28	Proizvodnja nepomenutih mašina i nepomenute opreme 	866
1160	29	Proizvodnja motornih vozila, prikolica i poluprikolica	866
1168	30	Proizvodnja ostalih saobraćajnih sredstava	866
1090	2573	Proizvodnja alata	1087
1182	31	Proizvodnja nameštaja	866
1188	32	Ostale prerađivačke delatnosti	866
1204	33	Popravka i montaža mašina i opreme	866
1217	35	Snabdevanje električnom energijom, gasom, parom i klimatizacija	1216
1230	36	Skupljanje, prečišćavanje i distribucija vode	1229
1233	37	Uklanjanje otpadnih voda	1229
1236	38	Sakupljanje,  tretman i odlaganje otpada; ponovno iskorišćavanje otpadnih materija	1229
1246	39	Sanacija, rekultivacija i druge usluge u oblasti upravljanja otpadom	1229
1250	41	Izgradnja zgrada	1249
1255	42	Izgradnja ostalih građevina	1249
1266	43	Specijalizovani građevinski radovi	1249
1216	D	SNABDEVANjE ELEKTRIČNOM ENERGIJOM, GASOM, PAROM I KLIMATIZACIJA	\N
1229	E	SNABDEVANjE VODOM; UPRAVLjANjE OTPADNIM VODAMA, KONTROLISANjE PROCESA UKLANjANjA OTPADA I SLIČNE AKTIVNOSTI	\N
1249	F	GRAĐEVINARSTVO	\N
970	1813	Usluge pripreme za štampu	967
1285	45	Trgovina na veliko i trgovina na malo i popravka motornih vozila i motocikala	1284
1296	46	Trgovina na veliko, osim trgovine motornim vozilima i motociklima	1284
1353	47	Trgovina na malo, osim trgovine motornim vozilima i motociklima	1284
1284	G	TRGOVINA NA VELIKO I TRGOVINA NA MALO; POPRAVKA MOTORNIH VOZILA I MOTOCIKALA	\N
1401	49	Kopneni saobraćaj i cevovodni transport	1400
1415	50	Vodeni saobraćaj	1400
1424	51	Vazdušni saobraćaj	1400
1430	52	Skladištenje i prateće aktivnosti u saobraćaju	1400
1439	53	Poštanske aktivnosti	1400
1445	55	Smeštaj	1444
1454	56	Delatnost pripremanja i posluživanja hrane i pića	1444
1400	H	SAOBRAĆAJ I SKLADIŠTENjE	\N
1444	I	USLUGE SMEŠTAJA I ISHRANE	\N
1408	4932	Taksi prevoz	1406
1463	58	Izdavačke delatnosti	1462
1473	59	Kinematografska i televizijska produkcija, snimanje zvučnih zapisa i izdavanje muzičkih zapisa	1462
1481	60	Programske aktivnosti i emitovanje	1462
1486	61	Telekomunikacije	1462
1495	62	Računarsko programiranje, konsultantske i s tim povezane delatnosti	1462
1501	63	Informacione uslužne delatnosti	1462
1462	J	INFORMISANjE I KOMUNIKACIJE	\N
1509	64	Finansijske usluge, osim osiguranja i penzijskih fondova	1508
1529	66	Pomoćne delatnosti u pružanju finansijskih usluga i osiguranju	1508
1521	65	Osiguranje, reosiguranje i penzijski fondovi, osim obaveznog socijalnog osiguranja	1508
1541	68	Poslovanje nekretninama	1540
1550	69	Pravni i računovodstveni poslovi	1549
1508	K	FINANSIJSKE DELATNOSTI I DELATNOST OSIGURANjA	\N
1540	L	POSLOVANjE NEKRETNINAMA	\N
1549	M	STRUČNE, NAUČNE, INOVACIONE I TEHNIČKE DELATNOSTI	\N
1458	5621	Ketering	1457
1555	70	Upravljačke delatnosti; savetovanje u vezi sa upravljanjem	1549
1561	71	Arhitektonske i inženjerske delatnosti; inženjersko ispitivanje i analize	1549
1567	72	Naučno istraživanje i razvoj	1549
1573	73	Reklamiranje i istraživanje tržišta	1549
1579	74	Ostale stručne, naučne i tehničke delatnosti	1549
1588	75	Veterinarske delatnosti	1549
1592	77	Iznajmljivanje i lizing	1591
1609	78	Delatnosti zapošljavanja	1591
1616	79	Delatnost putničkih agencija, tur-operatora, usluge rezervacije i prateće aktivnosti	1591
1622	80	Zaštitne i istražne delatnosti	1591
1629	81	Usluge održavanja objekata i okoline	1591
1638	82	Kancelarijsko-administrativne i druge pomoćne poslovne delatnosti	1591
1591	N	ADMINISTRATIVNE I POMOĆNE USLUŽNE DELATNOSTI	\N
1650	O	DRŽAVNA UPRAVA I ODBRANA; OBAVEZNO SOCIJALNO OSIGURANjE	\N
1651	84	Javna uprava i odbrana; obavezno socijalno osiguranje	1650
1665	85	Obrazovanje	1664
1684	86	Zdravstvene delatnosti	1683
1693	87	Socijalna zaštita sa smeštajem	1683
1702	88	Socijalna zaštita bez smeštaja	1683
1709	90	Stvaralačke, umetničke i zabavne delatnosti	1708
1715	91	Delatnost biblioteka, arhiva, muzeja galerija i zbirki i ostale kulturne delatnosti	1708
1721	92	Kockanje i klađenje	1708
1724	93	Sportske, zabavne i rekreativne delatnosti	1708
1734	94	Delatnosti udruženja 	1733
1744	95	Popravka računara i predmeta za ličnu upotrebu i upotrebu u domaćinstvu	1733
1664	P	OBRAZOVANjE	\N
1683	Q	ZDRAVSTVENA I SOCIJALNA ZAŠTITA	\N
1708	R	UMETNOST; ZABAVA I REKREACIJA	\N
1733	S	OSTALE USLUŽNE DELATNOSTI	\N
1755	96	Ostale lične uslužne delatnosti	1733
1763	97	Delatnost domaćinstava koja zapošljavaju poslugu	1762
1766	98	Delatnost domaćinstava koja proizvode robu i usluge za sopstvene potrebe	1762
1772	99	Delatnost eksteritorijalnih organizacija i tela	1771
1762	T	DELATNOST DOMAĆINSTVA KAO POSLODAVCA; DELATNOST DOMAĆINSTAVA KOJA PROIZVODE ROBU I USLUGE ZA SOPSTVENE POTREBE	\N
1771	U	DELATNOST EKSTERITORIJALNIH ORGANIZACIJA I TELA	\N
781	011	Gajenje jednogodišnjih i dvogodišnjih biljaka	780
782	0111	Gajenje žita (osim pirinča), leguminoza i uljarica	781
783	0112	Gajenje pirinča	781
784	0113	Gajenje povrća, bostana, korenastih i krtolastih biljaka	781
785	0114	Gajenje šećerne trske	781
786	0115	Gajenje duvana	781
787	0116	Gajenje biljaka za proizvodnju vlakana	781
788	0119	Gajenje ostalih jednogodišnjih i dvogodišnjih biljaka	781
789	012	Gajenje višegodišnjih biljaka	780
790	0121	Gajenje grožđa	789
791	0122	Gajenje tropskog i suptropskog voća	789
792	0123	Gajenje agruma	789
793	0124	Gajenje jabučastog i koštičavog voća	789
794	0125	Gajenje ostalog drvenastog, žbunastog i jezgrastog voća	789
795	0126	Gajenje uljnih plodova	789
796	0127	Gajenje biljaka za pripremanje napitaka	789
797	0128	Gajenje začinskog, aromatičnog i lekovitog bilja	789
798	0129	Gajenje ostalih višegodišnjih biljaka	789
799	013	Gajenje sadnog materijala	780
800	0130	Gajenje sadnog materijala	799
801	014	Uzgoj životinja	780
802	0141	Uzgoj muznih krava	801
803	0142	Uzgoj drugih goveda i bivola	801
804	0143	Uzgoj konja i drugih kopitara	801
805	0144	Uzgoj kamila i lama	801
806	0145	Uzgoj ovaca i koza	801
807	0146	Uzgoj svinja	801
808	0147	Uzgoj živine	801
809	0149	Uzgoj ostalih životinja	801
810	015	Mešovita poljoprivredna proizvodnja	780
811	0150	Mešovita poljoprivredna proizvodnja	810
812	016	Uslužne delatnosti u poljoprivredi i delatnosti posle žetve	780
813	0161	Uslužne delatnosti u gajenju useva i zasada	812
814	0162	Pomoćne delatnosti u uzgoju životinja	812
815	0163	Aktivnosti posle žetve	812
816	0164	Dorada semena	812
817	017	Lov, traperstvo i odgovarajuće uslužne delatnosti	780
818	0170	Lov, traperstvo i odgovarajuće uslužne delatnosti	817
820	021	Gajenje šuma i ostale šumarske delatnosti	819
821	0210	Gajenje šuma i ostale šumarske delatnosti	820
822	022	Seča drveća	819
823	0220	Seča drveća	822
824	023	Sakupljanje šumskih plodova	819
825	0230	Sakupljanje šumskih plodova	824
826	024	Uslužne delatnosti u vezi sa šumarstvom	819
827	0240	Uslužne delatnosti u vezi sa šumarstvom	826
829	031	Ribolov	828
830	0311	Morski ribolov	829
831	0312	Slatkovodni ribolov	829
832	032	Akvakulture	828
833	0321	Morske akvakulture	832
834	0322	Slatkovodne akvakulture	832
837	051	Eksploatacija kamenog uglja i antracita	836
838	0510	Eksploatacija kamenog uglja i antracita	837
839	052	Eksploatacija lignita i mrkog uglja	836
840	0520	Eksploatacija lignita i mrkog uglja	839
842	061	Eksploatacija sirove nafte	841
843	0610	Eksploatacija sirove nafte	842
844	062	Vađenje prirodnog gasa	841
845	0620	Eksploatacija prirodnog gasa	844
847	071	Eksploatacija ruda gvožđa	846
848	0710	Eksploatacija ruda gvožđa	847
849	072	Eksploatacija ostalih ruda metala	846
850	0721	Eksploatacija ruda urana i torijuma	849
851	0729	Eksploatacija ruda ostalih crnih, obojenih, plemenitih i drugih metala	849
853	081	Eksploatacija kamena, peska, gline i drugih sirovina za građevinske materijale	852
854	0811	Eksploatacija građevinskog i ukrasnog kamena, krečnjaka, gipsa, krede	853
855	0812	Eksploatacija šljunka, peska, gline i kaolina	853
856	089	Eksploatacija ostalih ruda i kamena	852
857	0891	Eksploatacija minerala, proizvodnja mineralnih đubriva i hemikalija	856
858	0892	Eksploatacija treseta	856
859	0893	Eksploatacija natrijum-hlorida	856
860	0899	Eksploatacija ostalih nemetaličnih ruda i minerala	856
862	091	Uslužne delatnosti u vezi sa istraživanjem i eksploatacijom nafte i gasa	861
863	0910	Uslužne delatnosti u vezi sa istraživanjem i eksploatacijom nafte i gasa	862
864	099	Uslužne delatnosti u vezi sa istraživanjem i eksploatacijom ostalih ruda	861
865	0990	Uslužne delatnosti u vezi sa istraživanjem i eksploatacijom ostalih ruda	864
868	101	Prerada i konzervisanje mesa i proizvoda od mesa	867
869	1011	Prerada i konzervisanje mesa	868
870	1012	Prerada i konzervisanje živinskog mesa	868
871	1013	Proizvodnja mesnih prerađevina	868
872	102	Prerada i konzervisanje ribe, ljuskara i mekušaca	867
873	1020	Prerada i konzervisanje ribe, ljuskara i mekušaca	872
874	103	Prerada i konzervisanje voća i povrća	867
1472	5829	Izdavanje ostalih softvera	1470
875	1031	Prerada i konzervisanje krompira	874
876	1032	Proizvodnja sokova od voća i povrća	874
877	1039	Ostala prerada i konzervisanje voća i povrća	874
878	104	Proizvodnja biljnih i životinjskih ulja i masti	867
879	1041	Proizvodnja ulja i masti	878
880	1042	Proizvodnja margarina i sličnih jestivih masti	878
881	105	Proizvodnja mlečnih proizvoda	867
882	1051	Prerada mleka i proizvodnja sireva	881
883	1052	Proizvodnja sladoleda	881
884	106	Proizvodnja mlinskih proizvoda, skroba i skrobnih proizvoda 	867
885	1061	Proizvodnja mlinskih proizvoda	884
886	1062	Proizvodnja skroba i skrobnih proizvoda	884
887	107	Proizvodnja pekarskih proizvoda i testenine	867
888	1071	Proizvodnja hleba, svežeg peciva i kolača	887
889	1072	Proizvodnja dvopeka, keksa, trajnog peciva i kolača	887
890	1073	Proizvodnja makarona, rezanaca i sličnih proizvoda od brašna	887
891	108	Proizvodnja ostalih prehrambenih proizvoda	867
892	1081	Proizvodnja šećera	891
893	1082	Proizvodnja kakaoa, čokolade i konditorskih proizvoda	891
894	1083	Prerada čaja i kafe	891
895	1084	Proizvodnja začina i drugih dodataka hrani	891
896	1085	Proizvodnja gotovih jela	891
897	1086	Proizvodnja homogenizovanih hranljivih preparata i dijetetske hrane	891
898	1089	Proizvodnja ostalih prehrambenih proizvoda	891
899	109	Proizvodnja gotove hrane za životinje	867
900	1091	Proizvodnja gotove hrane za domaće životinje	899
901	1092	Proizvodnja gotove hrane za kućne ljubimce	899
903	110	Proizvodnja pića	902
904	1101	Destilacija, prečišćavanje i mešanje pića	903
905	1102	Proizvodnja vina od grožđa	903
906	1103	Proizvodnja pića i ostalih voćnih vina	903
907	1104	Proizvodnja ostalih nedestilovanih fermentisanih pića	903
908	1105	Proizvodnja piva	903
909	1106	Proizvodnja slada	903
910	1107	Proizvodnja osvežavajućih pića, mineralne vode i ostale flaširane vode	903
912	120	Proizvodnja duvanskih proizvoda	911
913	1200	Proizvodnja duvanskih proizvoda	912
915	131	Priprema i predenje tekstilnih vlakana	914
916	1310	Priprema i predenje tekstilnih vlakana	915
917	132	Proizvodnja tkanina	914
918	1320	Proizvodnja tkanina	917
919	133	Dovršavanje tekstila	914
920	1330	Dovršavanje tekstila	919
921	139	Proizvodnja ostalog tekstila	914
922	1391	Proizvodnja pletenih i kukičanih materijala	921
923	1392	Proizvodnja gotovih tekstilnih proizvoda, osim odeće	921
924	1393	Proizvodnja tepiha i prekrivača za pod	921
925	1394	Proizvodnja užadi, kanapa, pletenica i mreža	921
926	1395	Proizvodnja netkanog tekstila i predmeta od njega, osim odeće	921
927	1396	Proizvodnja ostalog tehničkog i industrijskog tekstila	921
928	1399	Proizvodnja ostalih tekstilnih predmeta	921
930	141	Proizvodnja odeće, osim krznene	929
931	1411	Proizvodnja kožne odeće	930
932	1412	Proizvodnja radne odeće	930
933	1413	Proizvodnja ostale odeće	930
934	1414	Proizvodnja rublja	930
935	1419	Proizvodnja ostalih odevnih predmeta i pribora	930
936	142	Proizvodnja proizvoda od krzna	929
937	1420	Proizvodnja proizvoda od krzna	936
938	143	Proizvodnja pletene i kukičane odeće	929
939	1431	Proizvodnja pletenih i kukičanih čarapa	938
940	1439	Proizvodnja ostale pletene i kukičane odeće	938
942	151	Štavljenje i dorada kože; proizvodnja putničkih i ručnih torbi i kaiševa; dorada i bojenje krzna	941
943	1511	Štavljenje i dorada kože; dorada i bojenje krzna	942
944	1512	Proizvodnja putnih i ručnih torbi i sl., saračkih proizvoda i kaiševa	942
945	152	Proizvodnja obuće	941
946	1520	Proizvodnja obuće	945
948	161	Rezanje i obrada drveta	947
949	1610	Rezanje i obrada drveta	948
950	162	Proizvodnja proizvoda od drveta, plute, pruća i slame	947
951	1621	Proizvodnja furnira i ploča od drveta	950
952	1622	Proizvodnja parketa	950
953	1623	Proizvodnja ostale građevinske stolarije i elemenata	950
954	1624	Proizvodnja drvne ambalaže	950
955	1629	Proizvodnja ostalih proizvoda od drveta, plute, slame i pruća	950
957	171	Proizvodnja celuloze, papira i kartona	956
958	1711	Proizvodnja vlakana celuloze	957
959	1712	Proizvodnja papira i kartona	957
960	172	Proizvodnja predmeta od papira i kartona	956
961	1721	Proizvodnja talasastog papira i kartona i ambalaže od papira i kartona	960
962	1722	Proizvodnja predmeta od papira za ličnu upotrebu i upotrebu u domaćinstvu	960
963	1723	Proizvodnja kancelarijskih predmeta od papira	960
964	1724	Proizvodnja tapeta	960
965	1729	Proizvodnja ostalih proizvoda od papira i kartona	960
967	181	Štampanje i štamparske usluge	966
968	1811	Štampanje novina	967
969	1812	Ostalo štampanje	967
971	1814	Knjigovezačke i srodne usluge	967
972	182	Umnožavanje snimljenih zapisa	966
973	1820	Umnožavanje snimljenih zapisa	972
975	191	Proizvodnja produkata koksovanja	974
976	1910	Proizvodnja produkata koksovanja	975
977	192	Proizvodnja derivata nafte	974
978	1920	Proizvodnja derivata nafte	977
980	201	Proizvodnja osnovnih hemikalija, veštačkih đubriva i azotnih jedinjenja, plastičnih i sintetičkih masa	979
981	2011	Proizvodnja industrijskih gasova	980
982	2012	Proizvodnja sredstava za pripremanje boja i pigmenata	980
983	2013	Proizvodnja ostalih osnovnih neorganskih hemikalija	980
984	2014	Proizvodnja ostalih osnovnih organskih hemikalija	980
985	2015	Proizvodnja veštačkih đubriva i azotnih jedinjenja	980
986	2016	Proizvodnja plastičnih masa u primarnim oblicima	980
987	2017	Proizvodnja sintetičkog kaučuka u primarnim oblicima	980
988	202	Proizvodnja pesticida i hemikalija za poljoprivredu	979
989	2020	Proizvodnja pesticida i hemikalija za poljoprivredu	988
990	203	Proizvodnja boja, lakova i sličnih premaza, grafičkih boja i kitova	979
991	2030	Proizvodnja boja, lakova i sličnih premaza, grafičkih boja i kitova	990
992	204	Proizvodnja deterdženata, sapuna drugih sredstava za čišćenje, poliranje, parfema i toaletnih preparata	979
993	2041	Proizvodnja deterdženata, sapuna sredstava za čišćenje i poliranje	992
994	2042	Proizvodnja parfema i toaletnih preparata	992
995	205	Proizvodnja ostalih hemijskih proizvoda	979
996	2051	Proizvodnja eksploziva	995
997	2052	Proizvodnja sredstava za lepljenje	995
998	2053	Proizvodnja eteričnih ulja	995
999	2059	Proizvodnja ostalih hemijskih proizvoda	995
1000	206	Proizvodnja veštačkih vlakana	979
1001	2060	Proizvodnja veštačkih vlakana	1000
1003	211	Proizvodnja osnovnih farmaceutskih proizvoda	1002
1004	2110	Proizvodnja osnovnih farmaceutskih proizvoda	1003
1005	212	Proizvodnja farmaceutskih preparata	1002
1006	2120	Proizvodnja farmaceutskih preparata	1005
1008	221	Proizvodnja proizvoda od gume	1007
1009	2211	Proizvodnja guma za vozila, protektiranje guma za vozila	1008
1010	2219	Proizvodnja ostalih proizvoda od gume	1008
1011	222	Proizvodnja proizvoda od plastike	1007
1012	2221	Proizvodnja ploča, listova, cevi i profila od plastike	1011
1013	2222	Proizvodnja ambalaže od plastike	1011
1014	2223	Proizvodnja predmeta od plastike za građevinarstvo	1011
1015	2229	Proizvodnja ostalih proizvoda od plastike	1011
1017	231	Proizvodnja stakla i proizvoda od stakla	1016
1018	2311	Proizvodnja ravnog stakla	1017
1019	2312	Oblikovanje i obrada ravnog stakla	1017
1020	2313	Proizvodnja šupljeg stakla	1017
1021	2314	Proizvodnja staklenih vlakana	1017
1022	2319	Proizvodnja i obrada ostalog stakla, uključujući tehničke staklene proizvode	1017
1023	232	Proizvodnja vatrostalnih proizvoda	1016
1024	2320	Proizvodnja vatrostalnih proizvoda	1023
1025	233	Proizvodnja građevinskih materijala od gline	1016
1026	2331	Proizvodnja keramičkih pločica i ploča	1025
1027	2332	Proizvodnja opeke, crepa i građevinskih proizvoda od pečene gline	1025
1028	234	Proizvodnja ostalih keramičkih i porcelanskih proizvoda	1016
1029	2341	Proizvodnja keramičkih predmeta za domaćinstvo i ukrasnih predmeta	1028
1030	2342	Proizvodnja sanitarnih keramičkih proizvoda	1028
1031	2343	Proizvodnja izolatora i izolacionog pribora od keramike	1028
1032	2344	Proizvodnja ostalih tehničkih proizvoda od keramike	1028
1033	2349	Proizvodnja ostalih keramičkih proizvoda	1028
1034	235	Proizvodnja cementa, kreča i gipsa	1016
1035	2351	Proizvodnja cementa	1034
1036	2352	Proizvodnja kreča i gipsa	1034
1037	236	Proizvodnja proizvoda od betona, cementa i gipsa	1016
1038	2361	Proizvodnja proizvoda od betona namenjenih za građevinarstvo	1037
1039	2362	Proizvodnja proizvoda od gipsa namenjenih za građevinarstvo	1037
1040	2363	Proizvodnja svežeg betona	1037
1041	2364	Proizvodnja maltera	1037
1042	2365	Proizvodnja proizvoda od cementa s vlaknima	1037
1043	2369	Proizvodnja ostalih proizvoda od betona, gipsa i cementa	1037
1044	237	Sečenje, oblikovanje i obrada kamena	1016
1045	2370	Sečenje, oblikovanje i obrada kamena	1044
1046	239	Proizvodnja brusnih i ostalih nemetalnih mineralnih proizvoda	1016
1047	2391	Proizvodnja brusnih proizvoda	1046
1048	2399	Proizvodnja ostalih proizvoda od nemetalnih minerala	1046
1050	241	Proizvodnja sirovog gvožđa, čelika i ferolegura	1049
1051	2410	Proizvodnja sirovog gvožđa, čelika i ferolegura	1050
1052	242	Proizvodnja čeličnih cevi, šupljih profila i fitinga	1049
1053	2420	Proizvodnja čeličnih cevi, šupljih profila i fitinga	1052
1054	243	Proizvodnja ostalih proizvoda primarne prerade čelika	1049
1055	2431	Hladno valjanje šipki	1054
1056	2432	Hladno valjanje pljosnatih proizvoda	1054
1057	2433	Hladno oblikovanje profila	1054
1058	2434	Hladno vučenje žice	1054
1059	244	Proizvodnja plemenitih i ostalih obojenih metala	1049
1060	2441	Proizvodnja plemenitih metala	1059
1061	2442	Proizvodnja aluminijuma	1059
1062	2443	Proizvodnja olova, cinka i kalaja	1059
1063	2444	Proizvodnja bakra	1059
1064	2445	Proizvodnja ostalih obojenih metala	1059
1065	2446	Proizvodnja nuklearnog goriva	1059
1066	245	Livenje metala	1049
1067	2451	Livenje gvožđa	1066
1068	2452	Livenje čelika	1066
1069	2453	Livenje lakih metala	1066
1070	2454	Livenje ostalih obojenih metala	1066
1072	251	Proizvodnja metalnih konstrukcija	1071
1073	2511	Proizvodnja metalnih konstrukcija i delova konstrukcija	1072
1074	2512	Proizvodnja metalnih vrata i prozora	1072
1075	252	Proizvodnja metalnih cisterni, rezervoara i kontejnera	1071
1076	2521	Proizvodnja kotlova i radijatora za centralno grejanje	1075
1077	2529	Proizvodnja ostalih metalnih cisterni, rezervoara i kontejnera	1075
1078	253	Proizvodnja parnih kotlova, osim kotlova za centralno grejanje	1071
1079	2530	Proizvodnja parnih kotlova, osim kotlova za centralno grejanje	1078
1080	254	Proizvodnja oružja i municije	1071
1081	2540	Proizvodnja oružja i municije	1080
1082	255	Kovanje, presovanje, štancovanje i valjanje metala; metalurgija praha	1071
1083	2550	Kovanje, presovanje, štancovanje i valjanje metala; metalurgija praha	1082
1084	256	Obrada i prevlačenje metala; mašinska obrada metala	1071
1085	2561	Obrada i prevlačenje metala	1084
1086	2562	Mašinska obrada metala	1084
1087	257	Proizvodnja sečiva, alata i metalne robe opšte namene	1071
1088	2571	Proizvodnja sečiva	1087
1089	2572	Proizvodnja brava i okova	1087
1091	259	Proizvodnja ostalih metalnih proizvoda	1071
1092	2591	Proizvodnja čeličnih buradi i slične ambalaže	1091
1093	2592	Proizvodnja ambalaže od lakih metala	1091
1094	2593	Proizvodnja žičanih proizvoda, lanaca i opruga	1091
1095	2594	Proizvodnja veznih elemenata i vijčanih mašinskih proizvoda	1091
1096	2599	Proizvodnja ostalih metalnih proizvoda	1091
1098	261	Proizvodnja elektronskih elemenata i ploča	1097
1099	2611	Proizvodnja elektronskih elemenata	1098
1100	2612	Proizvodnja štampanih elektronskih ploča	1098
1101	262	Proizvodnja računara i periferne opreme	1097
1102	2620	Proizvodnja računara i periferne opreme	1101
1103	263	Proizvodnja komunikacione opreme	1097
1104	2630	Proizvodnja komunikacione opreme	1103
1105	264	Proizvodnja elektronskih uređaja za široku potrošnju	1097
1106	2640	Proizvodnja elektronskih uređaja za široku potrošnju	1105
1107	265	Proizvodnja mernih, istraživačkih i navigacionih instrumenata i aparata; proizvodnja satova	1097
1108	2651	Proizvodnja mernih, istraživačkih i navigacionih instrumenata i aparata	1107
1109	2652	Proizvodnja satova	1107
1110	266	Proizvodnja opreme za zračenje, elektromedicinske i elektroterapeutske opreme	1097
1111	2660	Proizvodnja opreme za zračenje, elektromedicinske i elektroterapeutske opreme	1110
1112	267	Proizvodnja optičkih instrumenata i fotografske opreme	1097
1113	2670	Proizvodnja optičkih instrumenata i fotografske opreme	1112
1114	268	Proizvodnja magnetnih i optičkih nosilaca zapisa	1097
1115	2680	Proizvodnja magnetnih i optičkih nosilaca zapisa	1114
1117	271	Proizvodnja elektromotora, generatora, transformatora i opreme za distribuciju električne energije	1116
1118	2711	Proizvodnja elektromotora, generatora i transformatora	1117
1119	2712	Proizvodnja opreme za distribuciju električne energije i opreme za upravljanje električnom energijom	1117
1120	272	Proizvodnja baterija i akumulatora	1116
1121	2720	Proizvodnja baterija i akumulatora	1120
1122	273	Proizvodnja žičane i kablovske opreme	1116
1123	2731	Proizvodnja kablova od optičkih vlakana	1122
1124	2732	Proizvodnja ostalih elektronskih i električnih provodnika i kablova	1122
1125	2733	Proizvodnja opreme za povezivanje žica i kablova	1122
1126	274	Proizvodnja opreme za osvetljenje	1116
1127	2740	Proizvodnja opreme za osvetljenje	1126
1128	275	Proizvodnja aparata za domaćinstvo	1116
1129	2751	Proizvodnja električnih aparata za domaćinstvo	1128
1130	2752	Proizvodnja neelektričnih aparata za domaćinstvo	1128
1131	279	Proizvodnja ostale električne opreme	1116
1132	2790	Proizvodnja ostale električne opreme	1131
1134	281	Proizvodnja mašina opšte namene	1133
1135	2811	Proizvodnja motora i turbina, osim za letelice i motorna vozila	1134
1136	2812	Proizvodnja hidrauličnih pogonskih uređaja	1134
1137	2813	Proizvodnja ostalih pumpi i kompresora	1134
1138	2814	Proizvodnja ostalih slavina i ventila	1134
1139	2815	Proizvodnja ležajeva, zupčanika i zupčastih pogonskih elemenata	1134
1140	282	Proizvodnja ostalih mašina opšte namene	1133
1141	2821	Proizvodnja industrijskih peći i gorionika	1140
1142	2822	Proizvodnja opreme za podizanje i prenošenje	1140
1143	2823	Proizvodnja kancelarijskih mašina i opreme, osim računara i računarske opreme	1140
1144	2824	Proizvodnja ručnih pogonskih aparata sa mehanizmima	1140
1145	2825	Proizvodnja rashladne i ventilacione opreme, osim za domaćinstvo	1140
1146	2829	Proizvodnja ostalih mašina i aparata opšte namene	1140
1147	283	Proizvodnja mašina za poljoprivredu i šumarstvo	1133
1148	2830	Proizvodnja mašina za poljoprivredu i šumarstvo	1147
1149	284	Proizvodnja mašina za obradu metala i alatnih mašina	1133
1150	2841	Proizvodnja mašina za obradu metala	1149
1151	2849	Proizvodnja ostalih alatnih mašina	1149
1152	289	Proizvodnja ostalih mašina za specijalne namene	1133
1153	2891	Proizvodnja mašina za metalurgiju	1152
1154	2892	Proizvodnja mašina za rudnike, kamenolome i građevinarstvo	1152
1155	2893	Proizvodnja mašina za industriju hrane, pića i duvana	1152
1156	2894	Proizvodnja mašina za industriju tekstila, odeće i kože	1152
1157	2895	Proizvodnja mašina za industriju papira i kartona	1152
1158	2896	Proizvodnja mašina za izradu plastike i gume	1152
1159	2899	Proizvodnja mašina za ostale specijalne namene	1152
1161	291	Proizvodnja motornih vozila	1160
1162	2910	Proizvodnja motornih vozila	1161
1163	292	Proizvodnja karoserija za motorna vozila, prikolice i poluprikolice	1160
1164	2920	Proizvodnja karoserija za motorna vozila, prikolice i poluprikolice	1163
1165	293	Proizvodnja delova i pribora za motorna vozila i motore za njih	1160
1166	2931	Proizvodnja električne i elektronske opreme za motorna vozila	1165
1167	2932	Proizvodnja ostalih delova i dodatne opreme za motorna vozila	1165
1169	301	Izgradnja brodova i čamaca	1168
1170	3011	Izgradnja brodova i plovnih objekata	1169
1171	3012	Izrada čamaca za sport i razonodu	1169
1172	302	Proizvodnja lokomotiva i šinskih vozila	1168
1173	3020	Proizvodnja lokomotiva i šinskih vozila	1172
1174	303	Proizvodnja vazdušnih i svemirskih letelica i odgovarajuće opreme	1168
1175	3030	Proizvodnja vazdušnih i svemirskih letelica i odgovarajuće opreme	1174
1176	304	Proizvodnja borbenih vojnih vozila	1168
1177	3040	Proizvodnja borbenih vojnih vozila	1176
1178	309	Proizvodnja ostalih transportnih sredstava	1168
1179	3091	Proizvodnja motocikala	1178
1180	3092	Proizvodnja bicikala i invalidskih kolica	1178
1181	3099	Proizvodnja ostale transportne opreme	1178
1183	310	Proizvodnja nameštaja	1182
1184	3101	Proizvodnja nameštaja za poslovne i prodajne prostore	1183
1185	3102	Proizvodnja kuhinjskog nameštaja	1183
1186	3103	Proizvodnja madraca	1183
1187	3109	Proizvodnja ostalog nameštaja	1183
1189	321	Proizvodnja nakita, bižuterije i sličnih predmeta	1188
1190	3211	Kovanje novca	1189
1191	3212	Proizvodnja nakita i srodnih predmeta	1189
1192	3213	Proizvodnja imitacije nakita i srodnih proizvoda	1189
1193	322	Proizvodnja muzičkih instrumenata	1188
1194	3220	Proizvodnja muzičkih instrumenata	1193
1195	323	Proizvodnja sportske opreme	1188
1196	3230	Proizvodnja sportske opreme	1195
1197	324	Proizvodnja igara i igračaka	1188
1198	3240	Proizvodnja igara i igračaka	1197
1199	325	Proizvodnja medicinskih i stomatoloških instrumenata i materijala	1188
1200	3250	Proizvodnja medicinskih i stomatoloških instrumenata i materijala	1199
1201	329	Ostale prerađivačke delatnosti	1188
1202	3291	Proizvodnja metli i četki	1201
1203	3299	Proizvodnja ostalih predmeta	1201
1205	331	Popravka metalnih proizvoda, mašina i opreme	1204
1206	3311	Popravka metalnih proizvoda	1205
1207	3312	Popravka mašina	1205
1208	3313	Popravka elektronske i optičke opreme	1205
1209	3314	Popravka električne opreme	1205
1210	3315	Popravka i održavanje brodova i čamaca	1205
1211	3316	Popravka i održavanje letelica i svemirskih letelica	1205
1212	3317	Popravka i održavanje druge transportne opreme	1205
1213	3319	Popravka ostale opreme	1205
1214	332	Montaža industrijskih mašina i opreme	1204
1215	3320	Montaža industrijskih mašina i opreme	1214
1218	351	Proizvodnja, prenos i distribucija električne energije	1217
1219	3511	Proizvodnja električne energije	1218
1220	3512	Prenos električne energije	1218
1221	3513	Distribucija električne energije	1218
1222	3514	Trgovina električnom energijom	1218
1223	352	Proizvodnja gasa i distribucija gasovitih goriva gasovodima	1217
1224	3521	Proizvodnja gasa	1223
1225	3522	Distribucija gasovitih goriva gasovodom	1223
1226	3523	Trgovina gasovitim gorivima preko gasovodne mreže	1223
1227	353	Snabdevanje parom i klimatizacija	1217
1228	3530	Snabdevanje parom i klimatizacija	1227
1231	360	Skupljanje, prečišćavanje i distribucija vode	1230
1232	3600	Skupljanje, prečišćavanje i distribucija vode	1231
1234	370	Uklanjanje otpadnih voda	1233
1235	3700	Uklanjanje otpadnih voda	1234
1237	381	Skupljanje otpada	1236
1238	3811	Skupljanje otpada koji nije opasan	1237
1239	3812	Skupljanje opasnog otpada	1237
1240	382	Tretman i odlaganje otpada	1236
1241	3821	Tretman i odlaganje otpada koji nije opasan	1240
1242	3822	Tretman i odlaganje opasnog otpada	1240
1243	383	Ponovna upotreba materijala	1236
1244	3831	Demontaža olupina	1243
1245	3832	Ponovna upotreba razvrstanih materijala	1243
1247	390	Sanacija, rekultivacija i duge usluge u oblasti upravljanja otpadom	1246
1248	3900	Sanacija, rekultivacija i druge usluge u oblasti upravljanja otpadom	1247
1251	411	Razrada građevinskih projekata	1250
1252	4110	Razrada građevinskih projekata	1251
1253	412	Izgradnja stambenih i nestambenih zgrada	1250
1254	4120	Izgradnja stambenih i nestambenih zgrada	1253
1256	421	Izgradnja puteva i železničkih pruga	1255
1257	4211	Izgradnja puteva i autoputeva	1256
1258	4212	Izgradnja železničkih pruga i podzemnih železnica	1256
1259	4213	Izgradnja mostova i tunela	1256
1260	422	Izgradnja cevovoda, električnih i komunikacionih vodova	1255
1261	4221	Izgradnja cevovoda	1260
1262	4222	Izgradnja električnih i telekomunikacionih vodova	1260
1263	429	Izgradnja ostalih građevina	1255
1264	4291	Izgradnja hidrotehničkih objekata	1263
1265	4299	Izgradnja ostalih nepomenutih građevina	1263
1267	431	Rušenje i pripremanje gradilišta	1266
1268	4311	Rušenje objekata	1267
1269	4312	Pripremna gradilišta	1267
1270	4313	Ispitivanje terena bušenjem i sondiranjem	1267
1271	432	Instalacioni radovi u građevinarstvu	1266
1272	4321	Postavljanje električnih instalacija	1271
1273	4322	Postavljanje vodovodnih, kanalizacionih, grejnih i klimatizacionih sistema	1271
1274	4329	Ostali instalacioni radovi u građevinarstvu	1271
1275	433	Završni građevinsko-zanatski radovi	1266
1276	4331	Malterisanje	1275
1277	4332	Ugradnja stolarije	1275
1278	4333	Postavljanje podnih i zidnih obloga	1275
1279	4334	Bojenje i zastakljivanje	1275
1280	4339	Ostali završni radovi	1275
1281	439	Ostali specifični građevinski radovi	1266
1282	4391	Krovni radovi	1281
1283	4399	Ostali nepomenuti specifični građevinski radovi	1281
1286	451	Trgovina motornim vozilima	1285
1287	4511	Trgovina automobilima i lakim motornim vozilima	1286
1288	4519	Trgovina ostalim motornim vozilima	1286
1289	452	Održavanje i popravka motornih vozila	1285
1290	4520	Održavanje i popravka motornih vozila	1289
1291	453	Trgovina delovima i priborom za motorna vozila	1285
1292	4531	Trgovina na veliko delovima i opremom za motorna vozila	1291
1293	4532	Trgovina na malo delovima i opremom za motorna vozila	1291
1294	454	Trgovina motociklima, delovima i priborom, održavanje i popravka motocikala	1285
1295	4540	Trgovina motociklima, delovima i priborom, održavanje i popravka motocikala	1294
1297	461	Trgovina na veliko za naknadu	1296
1298	4611	Posredovanje u prodaji poljoprivrednih sirovina, životinja, tekstilnih sirovina i poluproizvoda	1297
1299	4612	Posredovanje u prodaji goriva, ruda, metala i industrijskih hemikalija	1297
1300	4613	Posredovanje u prodaji drvne građe i građevinskog materijala	1297
1301	4614	Posredovanje u prodaji mašina, industrijske opreme, brodova i aviona	1297
1302	4615	Posredovanje u prodaji nameštaja, predmeta za domaćinstvo i metalne robe	1297
1303	4616	Posredovanje u prodaji tekstila, odeće, krzna, obuće i predmeta od kože	1297
1304	4617	Posredovanje u prodaji hrane, pića i duvana	1297
1305	4618	Specijalizovano posredovanje u prodaji posebnih proizvoda	1297
1306	4619	Posredovanje u prodaji raznovrsnih proizvoda	1297
1307	462	Trgovina na veliko poljoprivrednim sirovinama i životinjama	1296
1308	4621	Trgovina na veliko žitom, sirovim duvanom, semenjem i hranom za životinje	1307
1309	4622	Trgovina na veliko cvećem i sadnicama	1307
1310	4623	Trgovina na veliko životinjama	1307
1311	4624	Trgovina na veliko sirovom, nedovršenom i dovršenom kožom	1307
1312	463	Trgovina na veliko hranom, pićima i duvanom	1296
1313	4631	Trgovina na veliko voćem i povrćem	1312
1314	4632	Trgovina na veliko mesom i proizvodima od mesa	1312
1315	4633	Trgovina na veliko mlečnim proizvodima, jajima i jestivim uljima i mastima	1312
1316	4634	Trgovina na veliko pićima	1312
1317	4635	Trgovina na veliko duvanskim proizvodima	1312
1318	4636	Trgovina na veliko šećerom, čokoladom i slatkišima	1312
1319	4637	Trgovina na veliko kafom, čajevima, kakaom i začinima	1312
1320	4638	Trgovina na veliko ostalom hranom, uključujući ribu, ljuskare i mekušce	1312
1321	4639	Nespecijalizovana trgovina na veliko hranom, pićima i duvanom	1312
1322	464	Trgovina na veliko predmetima za domaćinstvo	1296
1323	4641	Trgovina na veliko tekstilom	1322
1324	4642	Trgovina na veliko odećom i obućom	1322
1325	4643	Trgovina na veliko električnim aparatima za domaćinstvo	1322
1326	4644	Trgovina na veliko porculanom, staklenom robom i sredstvima za čišćenje	1322
1327	4645	Trgovina na veliko parfimerijskim i kozmetičkim proizvodima	1322
1328	4646	Trgovina na veliko farmaceutskim proizvodima	1322
1329	4647	Trgovina na veliko nameštajem, tepisima i opremom za osvetljenje	1322
1330	4648	Trgovina na veliko satovima i nakitom	1322
1331	4649	Trgovina na veliko ostalim proizvodima za domaćinstvo	1322
1332	465	Trgovina na veliko informaciono-komunikacionom opremom	1296
1333	4651	Trgovina na veliko računarima, računarskom opremom i softverima	1332
1334	4652	Trgovina na veliko elektronskim i telekomunikacionim delovima i opremom	1332
1335	466	Trgovina na veliko ostalim mašinama, opremom i priborom	1296
1336	4661	Trgovina na veliko poljoprivrednim mašinama, opremom i priborom	1335
1337	4662	Trgovina na veliko alatnim mašinama	1335
1338	4663	Trgovina na veliko rudarskim i građevinskim mašinama	1335
1339	4664	Trgovina na veliko mašinama za tekstilnu industriju i mašinama za šivenje i pletenje	1335
1340	4665	Trgovina na veliko kancelarijskim nameštajem	1335
1341	4666	Trgovina na veliko ostalim kancelarijskim mašinama i opremom	1335
1342	4669	Trgovina na veliko ostalim mašinama i opremom	1335
1343	467	Ostala specijalizovana trgovina na veliko	1296
1344	4671	Trgovina na veliko čvrstim, tečnim i gasovitim gorivima i sličnim proizvodima	1343
1345	4672	Trgovina na veliko metalima i metalnim rudama	1343
1346	4673	Trgovina na veliko drvetom, građevinskim materijalom i sanitarnom opremom	1343
1347	4674	Trgovina na veliko metalnom robom, instalacionim materijalima, opremom i priborom za grejanje	1343
1348	4675	Trgovina na veliko hemijskim proizvodima	1343
1349	4676	Trgovina na veliko ostalim poluproizvodima	1343
1350	4677	Trgovina na veliko otpacima i ostacima	1343
1351	469	Nespecijalizovana trgovina na veliko	1296
1352	4690	Nespecijalizovana trgovina na veliko	1351
1354	471	Trgovina na malo u nespecijalizovanim prodavnicama	1353
1355	4711	Trgovina na malo u nespecijalizovanim prodavnicama, pretežno hranom, pićima i duvanom	1354
1356	4719	Ostala trgovina na malo u nespecijalizovanim prodavnicama	1354
1357	472	Trgovina na malo hranom, pićima i duvanom u specijalizovanim prodavnicama	1353
1358	4721	Trgovina na malo voćem i povrćem u specijalizovanim prodavnicama	1357
1359	4722	Trgovina na malo mesom i proizvodima od mesa u specijalizovanim prodavnicama	1357
1360	4723	Trgovina na malo ribom, ljuskarima i mekušcima u specijalizovanim prodavnicama	1357
1361	4724	Trgovina na malo hlebom, testeninom, kolačima i slatkišima u specijalizovanim prodavnicama	1357
1362	4725	Trgovina na malo pićima u specijalizovanim prodavnicama	1357
1363	4726	Trgovina na malo proizvodima od duvana u specijalizovanim prodavnicama	1357
1364	4729	Ostala trgovina na malo hranom u specijalizovanim prodavnicama	1357
1365	473	Trgovina na malo motornim gorivima u specijalizovanim prodavnicama	1353
1366	4730	Trgovina na malo motornim gorivima u specijalizovanim prodavnicama	1365
1367	474	Trgovina na malo informaciono-komunikacionom opremom u specijalizovanim prodavnicama	1353
1368	4741	Trgovina na malo računarima, perifernim jedinicama i softverom u specijalizovanim prodavnicama	1367
1369	4742	Trgovina na malo telekomunikacionom opremom u specijalizovanim prodavnicama	1367
1370	4743	Trgovina na malo audio i video opremom u specijalizovanim prodavnicama	1367
1371	475	Trgovina na malo ostalom opremom za domaćinstvo u specijalizovanim prodavnicama	1353
1372	4751	Trgovina na malo tekstilom u specijalizovanim prodavnicama	1371
1373	4752	Trgovina na malo metalnom robom, bojama i staklom u specijalizovanim prodavnicama	1371
1374	4753	Trgovina na malo tepisima, zidnim i podnim oblogama u specijalizovanim prodavnicama	1371
1375	4754	Trgovina na malo električnim aparatima za domaćinstvo u specijalizovanim prodavnicama	1371
1376	4759	Trgovina na malo nameštajem, opremom za osvetljenje i ostalim predmetima za domaćinstvo u specijalizovanim prodavnicama	1371
1377	476	Trgovina na malo predmetima za kulturu i rekreaciju u specijalizovanim prodavnicama	1353
1378	4761	Trgovina na malo knjigama u specijalizovanim prodavnicama	1377
1379	4762	Trgovina na malo novinama i kancelarijskim materijalom u specijalizovanim prodavnicama	1377
1380	4763	Trgovina na malo muzičkim i video zapisima u specijalizovanim prodavnicama	1377
1381	4764	Trgovina na malo sportskom opremom u specijalizovanim prodavnicama	1377
1382	4765	Trgovina na malo igrama i igračkama u specijalizovanim prodavnicama	1377
1383	477	Trgovina na malo ostalom robom u specijalizovanim prodavnicama	1353
1384	4771	Trgovina na malo odećom u specijalizovanim prodavnicama	1383
1385	4772	Trgovina na malo obućom i predmetima od kože u specijalizovanim prodavnicama	1383
1386	4773	Trgovina na malo farmaceutskim proizvodima u specijalizovanim prodavnicama – apotekama	1383
1387	4774	Trgovina na malo medicinskim i ortopedskim pomagalima u specijalizovanim prodavnicama	1383
1388	4775	Trgovina na malo kozmetičkim i toaletnim proizvodima u specijalizovanim prodavnicama	1383
1389	4776	Trgovina na malo cvećem, sadnicama, semenjem, đubrivima, kućnim ljubimcima i hranom za kućne ljubimce u specijalizovanim prodavnicama	1383
1390	4777	Trgovina na malo satovima i nakitom u specijalizovanim prodavnicama	1383
1391	4778	Ostala trgovina na malo novim proizvodima u specijalizovanim prodavnicama	1383
1392	4779	Trgovina na malo polovnom robom u prodavnicama	1383
1393	478	Trgovina na malo na tezgama i pijacama	1353
1394	4781	Trgovina na malo hranom, pićima i duvanskim proizvodima na tezgama i pijacama	1393
1395	4782	Trgovina na malo tekstilom, odećom i obućom na tezgama i pijacama	1393
1396	4789	Trgovina na malo ostalom robom na tezgama i pijacama	1393
1397	479	Trgovina na malo van prodavnica, tezgi i pijaca	1353
1398	4791	Trgovina na malo posredstvom pošte ili preko interneta	1397
1399	4799	Ostala trgovina na malo izvan prodavnica, tezgi i pijaca	1397
1402	491	Železnički prevoz putnika, daljinski i regionalni	1401
1403	4910	Železnički prevoz putnika, daljinski i regionalni	1402
1404	492	Železnički prevoz tereta	1401
1405	4920	Železnički prevoz tereta	1404
1406	493	Ostali kopneni prevoz putnika	1401
1407	4931	Gradski i prigradski kopneni prevoz putnika	1406
1409	4939	Ostali prevoz putnika u kopnenom saobraćaju	1406
1410	494	Drumski prevoz tereta i usluge preseljenja	1401
1411	4941	Drumski prevoz tereta	1410
1412	4942	Usluge preseljenja	1410
1413	495	Cevovodni transport	1401
1414	4950	Cevovodni transport	1413
1416	501	Pomorski i priobalni prevoz putnika	1415
1417	5010	Pomorski i priobalni prevoz putnika 	1416
1418	502	Pomorski i priobalni prevoz tereta	1415
1419	5020	Pomorski i priobalni prevoz tereta	1418
1420	503	Prevoz putnika unutrašnjim plovnim putevima	1415
1421	5030	Prevoz putnika unutrašnjim plovnim putevima	1420
1422	504	Prevoz tereta unutrašnjim plovnim putevima	1415
1423	5040	Prevoz tereta unutrašnjim plovnim putevima	1422
1425	511	Vazdušni prevoz putnika	1424
1426	5110	Vazdušni prevoz putnika	1425
1427	512	Vazdušni prevoz tereta i vasionski saobraćaj	1424
1428	5121	Vazdušni prevoz tereta	1427
1429	5122	Vasionski saobraćaj	1427
1431	521	Skladištenje	1430
1432	5210	Skladištenje	1431
1433	522	Prateće aktivnosti u saobraćaju	1430
1434	5221	Uslužne delatnosti u kopnenom saobraćaju	1433
1435	5222	Uslužne delatnosti u vodenom saobraćaju	1433
1436	5223	Uslužne delatnosti u vazdušnom saobraćaju	1433
1437	5224	Manipulacija teretom	1433
1438	5229	Ostale prateće delatnosti u saobraćaju	1433
1440	531	Poštanske aktivnosti javnog servisa	1439
1441	5310	Poštanske aktivnosti javnog servisa	1440
1442	532	Poštanske aktivnosti komercijalnog servisa	1439
1443	5320	Poštanske aktivnosti komercijalnog servisa	1442
1446	551	Hoteli i sličan smeštaj	1445
1447	5510	Hoteli i sličan smeštaj	1446
1448	552	Odmarališta i slični objekti za kraći boravak	1445
1449	5520	Odmarališta i slični objekti za kraći boravak	1448
1450	553	Delatnost kampova, auto-kampova i kampova za turističke prikolice 	1445
1451	5530	Delatnost kampova, auto-kampova i kampova za turističke prikolice	1450
1452	559	Ostali smeštaj	1445
1453	5590	Ostali smeštaj	1452
1455	561	Delatnosti restorana i pokretnih ugostiteljskih objekta	1454
1456	5610	Delatnosti restorana i pokretnih ugostiteljskih objekta	1455
1457	562	Ketering i ostale usluge pripremanja i posluživanja hrane	1454
1459	5629	Ostale usluge pripremanja i posluživanja hrane	1457
1460	563	Usluge pripremanja i posluživanja pića	1454
1461	5630	Usluge pripremanja i posluživanja pića	1460
1464	581	Izdavanje knjiga, časopisa i druge izdavačke delatnosti	1463
1465	5811	Izdavanje knjiga	1464
1466	5812	Izdavanje imenika i adresara	1464
1467	5813	Izdavanje novina	1464
1468	5814	Izdavanje časopisa i periodičnih izdanja	1464
1469	5819	Ostala izdavačka delatnost	1464
1470	582	Izdavanje softvera	1463
1471	5821	Izdavanje računarskih igara	1470
1474	591	Kinematografska i televizijska produkcija	1473
1475	5911	Proizvodnja kinematografskih dela, audio-vizuelnih proizvoda i televizijskog programa	1474
1476	5912	Delatnosti koje slede nakon faze snimanja u proizvodnji kinematografskih dela i televizijskog programa	1474
1477	5913	Distribucija kinematografskih dela, audio-vizuelnih dela i televizijskog programa	1474
1478	5914	Delatnost prikazivanja kinematografskih dela	1474
1479	592	Snimanje i izdavanje zvučnih zapisa i muzike	1473
1480	5920	Snimanje i izdavanje zvučnih zapisa i muzike	1479
1482	601	Emitovanje radio-programa	1481
1483	6010	Emitovanje radio-programa	1482
1484	602	Proizvodnja i emitovanje televizijskog programa	1481
1485	6020	Proizvodnja i emitovanje televizijskog programa	1484
1487	611	Kablovske telekomunikacije	1486
1488	6110	Kablovske telekomunikacije 	1487
1489	612	Bežične telekomunikacije	1486
1490	6120	Bežične telekomunikacije	1489
1491	613	Satelitske telekomunikacije	1486
1492	6130	Satelitske telekomunikacije	1491
1493	619	Ostale telekomunikacione delatnosti	1486
1494	6190	Ostale telekomunikacione delatnosti	1493
1496	620	Računarsko programiranje, konsultantske i s tim povezane delatnosti	1495
1497	6201	Računarsko programiranje	1496
1498	6202	Konsultantske delatnosti u oblasti informacione tehnologije	1496
1499	6203	Upravljanje računarskom opremom	1496
1500	6209	Ostale usluge informacione tehnologije	1496
1502	631	Obrada podataka, hosting i s tim povezane delatnosti; veb portali	1501
1503	6311	Obrada podataka, hosting i sl.	1502
1504	6312	Veb portali	1502
1505	639	Ostale informacione uslužne delatnosti	1501
1506	6391	Delatnosti novinskih agencija	1505
1507	6399	Informacione uslužne delatnosti na drugom mestu nepomenute 	1505
1510	641	Monetarno posredovanje	1509
1511	6411	Centralna banka	1510
1512	6419	Ostalo monetarno posredovanje	1510
1513	642	Delatnost holding kompanija	1509
1514	6420	Delatnost holding kompanija	1513
1515	643	Poverenički fondovi (trastovi), investicioni fondovi i slični finansijski entiteti	1509
1516	6430	Poverenički fondovi (trastovi), investicioni fondovi i slični finansijski entiteti	1515
1517	649	Ostale finansijske usluge, osim osiguranja i penzijskih fondova	1509
1518	6491	Finansijski lizing	1517
1519	6492	Ostale usluge kreditiranja	1517
1520	6499	Ostale nepomenute finansijske usluge, osim osiguranja i penzijskih fondova	1517
1522	651	Osiguranje	1521
1523	6511	Životno osiguranje	1522
1524	6512	Neživotno osiguranje	1522
1525	652	Reosiguranje	1521
1526	6520	Reosiguranje	1525
1527	653	Penzijski fondovi	1521
1528	6530	Penzijski fondovi	1527
1530	661	Pomoćne delatnosti u pružanju finansijskih usluga, osim osiguranja i penzijskih fondova	1529
1531	6611	Finansijske i robne berze	1530
1532	6612	Brokerski poslovi s hartijama od vrednosti i berzanskom robom	1530
1533	6619	Ostale pomoćne delatnosti u pružanju finansijskih usluga, osim osiguranja i penzijskih fondova	1530
1534	662	Pomoćne delatnosti u osiguranju i penzijskim fondovima	1529
1535	6621	Obrada odštetnih zahteva i procenjivanje rizika i šteta	1534
1536	6622	Delatnost zastupnika i posrednika u osiguranju	1534
1537	6629	Ostale pomoćne delatnosti u osiguranju i penzijskim fondovima	1534
1538	663	Upravljanje fondovima	1529
1539	6630	Upravljanje fondovima	1538
1542	681	Kupovina i prodaja vlastitih nekretnina	1541
1543	6810	Kupovina i prodaja vlastitih nekretnina	1542
1544	682	Iznajmljivanje vlastitih ili iznajmljenih nekretnina i upravljanje njima	1541
1545	6820	Iznajmljivanje vlastitih ili iznajmljenih nekretnina i upravljanje njima	1544
1546	683	Poslovanje nekretninama za naknadu	1541
1547	6831	Delatnost agencija za nekretnine	1546
1548	6832	Upravljanje nekretninama za naknadu	1546
1551	691	Pravni poslovi	1550
1552	6910	Pravni poslovi	1551
1553	692	Računovodstveni, knjigovodstveni i revizorski poslovi; poresko savetovanje	1550
1554	6920	Računovodstveni, knjigovodstveni i revizorski poslovi; poresko savetovanje	1553
1556	701	Upravljanje ekonomskim subjektom	1555
1557	7010	Upravljanje ekonomskim subjektom	1556
1558	702	Menadžerski konsultantski poslovi	1555
1559	7021	Delatnost komunikacija i odnosa s javnošću	1558
1560	7022	Konsultantske aktivnosti u vezi s poslovanjem i ostalim upravljanjem	1558
1562	711	Arhitektonske i inženjerske delatnosti i tehničko savetovanje	1561
1563	7111	Arhitektonska delatnost	1562
1564	7112	Inženjerske delatnosti i tehničko savetovanje	1562
1565	712	Tehničko ispitivanje i analize	1561
1566	7120	Tehničko ispitivanje i analize	1565
1568	721	Istraživanje i eksperimentalni razvoj u prirodnim i tehničko-tehnološkim naukama	1567
1569	7211	Istraživanje i eksperimentalni razvoj u biotehnologiji	1568
1570	7219	Istraživanje i razvoj u ostalim prirodnim i tehničko-tehnološkim naukama	1568
1571	722	Istraživanje i razvoj u društvenim i humanističkim naukama	1567
1572	7220	Istraživanje i razvoj u društvenim i humanističkim naukama	1571
1574	731	Reklamiranje	1573
1575	7311	Delatnost reklamnih agencija	1574
1576	7312	Medijsko predstavljanje	1574
1577	732	Istraživanje tržišta i ispitivanje javnog mnjenja	1573
1578	7320	Istraživanje tržišta i ispitivanje javnog mnjenja	1577
1580	741	Specijalizovane dizajnerske delatnosti	1579
1581	7410	Specijalizovane dizajnerske delatnosti	1580
1582	742	Fotografske usluge	1579
1583	7420	Fotografske usluge	1582
1584	743	Prevođenje i usluge tumača	1579
1585	7430	Prevođenje i usluge tumača	1584
1586	749	Ostale stručne, naučne i tehničke delatnosti	1579
1587	7490	Ostale stručne, naučne i tehničke delatnosti	1586
1589	750	Veterinarska delatnost	1588
1590	7500	Veterinarska delatnost	1589
1593	771	Iznajmljivanje i lizing motornih vozila	1592
1594	7711	Iznajmljivanje i lizing automobila i lakih motornih vozila	1593
1595	7712	Iznajmljivanje i lizing kamiona	1593
1596	772	Iznajmljivanje i lizing predmeta za ličnu upotrebu i upotrebu u domaćinstvu	1592
1597	7721	Iznajmljivanje i lizing opreme za rekreaciju i sport	1596
1598	7722	Iznajmljivanje video-kaseta i kompakt-diskova	1596
1599	7729	Iznajmljivanje i lizing ostalih predmeta za ličnu upotrebu i upotrebu u domaćinstvu	1596
1600	773	Iznajmljivanje i lizing mašina, opreme i materijalnih dobara	1592
1601	7731	Iznajmljivanje i lizing poljoprivrednih mašina i opreme	1600
1602	7732	Iznajmljivanje i lizing mašina i opreme za građevinarstvo	1600
1603	7733	Iznajmljivanje i lizing kancelarijskih mašina i kancelarijske opreme (uključujući računare)	1600
1604	7734	Iznajmljivanje i lizing opreme za vodeni transport	1600
1605	7735	Iznajmljivanje i lizing opreme za vazdušni transport	1600
1606	7739	Iznajmljivanje i lizing ostalih mašina, opreme i materijalnih dobara	1600
1607	774	Lizing intelektualne svojine i sličnih proizvoda, autorskih dela i predmeta srodnih prava	1592
1608	7740	Lizing intelektualne svojine i sličnih proizvoda, autorskih dela i predmeta srodnih prava	1607
1610	781	Delatnost agencija za zapošljavanje	1609
1611	7810	Delatnost agencija za zapošljavanje	1610
1612	782	Delatnost agencija za privremeno zapošljavanje	1609
1613	7820	Delatnost agencija za privremeno zapošljavanje	1612
1614	783	Ostalo ustupanje ljudskih resursa	1609
1615	7830	Ostalo ustupanje ljudskih resursa	1614
1617	791	Delatnost putničkih agencija i tur-operatora	1616
1618	7911	Delatnost putničkih agencija	1617
1619	7912	Delatnost tur-operatora	1617
1620	799	Ostale usluge rezervacije i delatnosti povezane s njima	1616
1621	7990	Ostale usluge rezervacije i delatnosti povezane s njima	1620
1623	801	Delatnost privatnog obezbeđenja	1622
1624	8010	Delatnost privatnog obezbeđenja	1623
1625	802	Usluge sistema obezbeđenja	1622
1626	8020	Usluge sistema obezbeđenja	1625
1627	803	Istražne delatnosti	1622
1628	8030	Istražne delatnosti	1627
1630	811	Usluge održavanja objekata	1629
1631	8110	Usluge održavanja objekata	1630
1632	812	Usluge čišćenja	1629
1633	8121	Usluge redovnog čišćenja zgrada	1632
1634	8122	Usluge ostalog čišćenja zgrada i opreme 	1632
1635	8129	Usluge ostalog čišćenja	1632
1636	813	Usluge uređenja i održavanja okoline	1629
1637	8130	Usluge uređenja i održavanja okoline	1636
1639	821	Kancelarijsko-administrativne i pomoćne delatnosti	1638
1640	8211	Kombinovane kancelarijsko-administrativne usluge	1639
1641	8219	Fotokopiranje, pripremanje dokumenata i druga specijalizovana kancelarijska podrška	1639
1642	822	Delatnost pozivnih centara	1638
1643	8220	Delatnost pozivnih centara	1642
1644	823	Organizovanje sastanaka i sajmova	1638
1645	8230	Organizovanje sastanaka i sajmova	1644
1646	829	Poslovne, pomoćne, uslužne i ostale delatnosti	1638
1647	8291	Delatnost agencija za naplatu potraživanja i kreditnih biroa	1646
1648	8292	Usluge pakovanja	1646
1649	8299	Ostale uslužne aktivnosti podrške poslovanju	1646
1652	841	Državna uprava, ekonomska i socijalna politika 	1651
1653	8411	Delatnost državnih organa	1652
1654	8412	Uređivanje delatnosti subjekata koji pružaju zdravstvenu zaštitu, usluge u obrazovanju i kulturi i druge društvene usluge, osim obaveznog socijalnog osiguranja	1652
1655	8413	Uređenje poslovanja i doprinos uspešnijem poslovanju u oblasti ekonomije	1652
1656	842	Pomoćne aktivnosti za funkcionisanje države	1651
1657	8421	Spoljni poslovi	1656
1658	8422	Poslovi odbrane	1656
1659	8423	Sudske i pravosudne delatnosti	1656
1660	8424	Obezbeđivanje javnog reda i bezbednosti	1656
1661	8425	Delatnost vatrogasnih jedinica	1656
1662	843	Obavezno socijalno osiguranje	1651
1663	8430	Obavezno socijalno osiguranje	1662
1666	851	Predškolsko obrazovanje	1665
1667	8510	Predškolsko obrazovanje	1666
1668	852	Osnovno obrazovanje	1665
1669	8520	Osnovno obrazovanje	1668
1670	853	Srednje obrazovanje	1665
1671	8531	Srednje opšte obrazovanje	1670
1672	8532	Srednje stručno obrazovanje	1670
1673	854	Visoko obrazovanje	1665
1674	8541	Obrazovanje posle srednjeg koje nije visoko	1673
1675	8542	Visoko obrazovanje	1673
1676	855	Ostalo obrazovanje	1665
1677	8551	Sportsko i rekreativno obrazovanje	1676
1678	8552	Umetničko obrazovanje	1676
1679	8553	Delatnost škola za vozače	1676
1680	8559	Ostalo obrazovanje	1676
1681	856	Pomoćne obrazovne delatnosti	1665
1682	8560	Pomoćne obrazovne delatnosti	1681
1685	861	Delatnost bolnica	1684
1686	8610	Delatnost bolnica	1685
1687	862	Medicinska i stomatološka praksa	1684
1688	8621	Opšta medicinska praksa	1687
1689	8622	Specijalistička medicinska praksa	1687
1690	8623	Stomatološka praksa	1687
1691	869	Ostala zdravstvena zaštita 	1684
1692	8690	Ostala zdravstvena zaštita 	1691
1694	871	Delatnosti smeštajnih ustanova s medicinskom negom	1693
1695	8710	Delatnosti smeštajnih ustanova s medicinskom negom	1694
1696	872	Socijalno staranje u smeštajnim ustanovama za lica s teškoćama u razvoju, duševno obolele osobe i osobe s bolestima zavisnosti	1693
1697	8720	Socijalno staranje u smeštajnim ustanovama za lica s teškoćama u razvoju, duševno obolele osobe i osobe s bolestima zavisnosti	1696
1698	873	Rad ustanova za stara lica i lica s posebnim potrebama	1693
1699	8730	Rad ustanova za stara lica i lica s posebnim potrebama	1698
1700	879	Ostali oblici socijalne zaštite sa smeštajem	1693
1701	8790	Ostali oblici socijalne zaštite sa smeštajem	1700
1703	881	Socijalna zaštita bez smeštaja za stara lica i lica s posebnim potrebama	1702
1704	8810	Socijalna zaštita bez smeštaja za stara lica i lica s posebnim potrebama	1703
1705	889	Ostala socijalna zaštita bez smeštaja	1702
1706	8891	Delatnost dnevne brige o deci	1705
1707	8899	Ostala nepomenuta socijalna zaštita bez smeštaja	1705
1710	900	Stvaralačke, umetničke i zabavne delatnosti	1709
1711	9001	Izvođačka umetnost	1710
1712	9002	Druge umetničke delatnosti u okviru izvođačke umetnosti	1710
1713	9003	Umetničko stvaralaštvo	1710
1714	9004	Rad umetničkih ustanova	1710
1716	910	Rad biblioteka, arhiva, muzeja, galerija i zbirki, zavoda za zaštitu spomenika kulture i ostale kulturne delatnosti 	1715
1717	9101	Delatnosti biblioteka i arhiva	1716
1718	9102	Delatnost muzeja galerija i zbirki	1716
1719	9103	Zaštita i održavanje nepokretnih kulturnih dobara, kulturno-istorijskih lokacija, zgrada i sličnih turističkih spomenika	1716
1720	9104	Delatnost botaničkih i zooloških vrtova i zaštita prirodnih vrednosti	1716
1722	920	Kockanje i klađenje	1721
1723	9200	Kockanje i klađenje	1722
1725	931	Sportske delatnosti	1724
1726	9311	Delatnost sportskih objekata	1725
1727	9312	Delatnost sportskih klubova	1725
1728	9313	Delatnost fitnes klubova	1725
1729	9319	Ostale sportske delatnosti	1725
1730	932	Ostale zabavne i rekreativne delatnosti	1724
1731	9321	Delatnost zabavnih i tematskih parkova	1730
1732	9329	Ostale zabavne i rekreativne delatnosti	1730
1735	941	Delatnost poslovnih i profesionalnih organizacija i poslodavaca	1734
1736	9411	Delatnosti poslovnih udruženja i udruženja poslodavaca	1735
1737	9412	Delatnosti strukovnih udruženja 	1735
1738	942	Delatnost sindikata	1734
1739	9420	Delatnost sindikata	1738
1740	949	Delatnost ostalih organizacija na bazi učlanjenja	1734
1741	9491	Delatnost verskih organizacija	1740
1742	9492	Delatnost političkih organizacija	1740
1743	9499	Delatnost ostalih organizacija na bazi učlanjenja	1740
1745	951	Popravka računara i komunikacione opreme	1744
1746	9511	Popravka računara i periferne opreme	1745
1747	9512	Popravka komunikacione opreme	1745
1748	952	Popravka predmeta za ličnu upotrebu i upotrebu u domaćinstvu	1744
1749	9521	Popravka elektronskih aparata za široku upotrebu	1748
1750	9522	Popravka aparata za domaćinstvo i kućne i baštenske opreme	1748
1751	9523	Popravka obuće i predmeta od kože	1748
1752	9524	Održavanje i popravka nameštaja	1748
1753	9525	Popravka satova i nakita	1748
1754	9529	Popravka ostalih ličnih predmeta i predmeta za domaćinstvo	1748
1756	960	Ostale lične uslužne delatnosti	1755
1757	9601	Pranje i hemijsko čišćenje tekstilnih i krznenih proizvoda	1756
1758	9602	Delatnost frizerskih i kozmetičkih salona	1756
1759	9603	Pogrebne i srodne delatnosti	1756
1760	9604	Delatnost nege i održavanja tela	1756
1761	9609	Ostale nepomenute lične uslužne delatnosti	1756
1764	970	Delatnost domaćinstava koja zapošljavaju poslugu	1763
1765	9700	Delatnost domaćinstava koja zapošljavaju poslugu	1764
1767	981	Delatnost domaćinstava koja proizvode robu za sopstvene potrebe	1766
1768	9810	Delatnost domaćinstava koja proizvode robu za sopstvene potrebe	1767
1769	982	Delatnost domaćinstava koja obezbeđuju usluge za sopstvene potrebe	1766
1770	9820	Delatnost domaćinstava koja obezbeđuju usluge za sopstvene potrebe	1769
1773	990	Delatnost eksteritorijalnih organizacija i tela	1772
1774	9900	Delatnost eksteritorijalnih organizacija i tela	1773
\.


--
-- Data for Name: tbl_migration; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tbl_migration (version, apply_time, module) FROM stdin;
m000000_000000_base_public	1507186800	public
m0000000000_00000_create_safe_file_extensions_table	1507186800	public
m0000000000_00001_fixes_for_work_codes	1507315348	public
m0000000000_00002_fix_for_recursive_work_codes_view	1511796029	public
m0000000000_00003_insert_safe_file_extensions	1511857372	public
m0000000000_00004_paychecks	1520860079	public
m0000000000_00005_create_treasury_offices_tables	1520936797	public
m0000000000_00006_create_occupations	1555494091	public
m0000000000_00007_injuries_add_parent_column	1555494091	public
m0000000000_00008_create_qualifications_level	1559130115	public
m0000000000_00009_treasury_office_units_and_city_add_column	1561026125	public
\.


--
-- Name: account_layouts_id_seq; Type: SEQUENCE SET; Schema: private; Owner: postgres
--

SELECT pg_catalog.setval('private.account_layouts_id_seq', 1, false);


--
-- Name: addresses_id_seq; Type: SEQUENCE SET; Schema: private; Owner: postgres
--

SELECT pg_catalog.setval('private.addresses_id_seq', 1, true);


--
-- Name: banks_id_seq; Type: SEQUENCE SET; Schema: private; Owner: postgres
--

SELECT pg_catalog.setval('private.banks_id_seq', 32, true);


--
-- Name: cities_id_seq; Type: SEQUENCE SET; Schema: private; Owner: postgres
--

SELECT pg_catalog.setval('private.cities_id_seq', 3448, true);


--
-- Name: cities_to_postal_codes_id_seq; Type: SEQUENCE SET; Schema: private; Owner: postgres
--

SELECT pg_catalog.setval('private.cities_to_postal_codes_id_seq', 2308, true);


--
-- Name: countries_id_seq; Type: SEQUENCE SET; Schema: private; Owner: postgres
--

SELECT pg_catalog.setval('private.countries_id_seq', 252, true);


--
-- Name: currencies_id_seq; Type: SEQUENCE SET; Schema: private; Owner: postgres
--

SELECT pg_catalog.setval('private.currencies_id_seq', 177, true);


--
-- Name: driver_license_categories_id_seq; Type: SEQUENCE SET; Schema: private; Owner: postgres
--

SELECT pg_catalog.setval('private.driver_license_categories_id_seq', 18, true);


--
-- Name: fixed_asset_types_id_seq; Type: SEQUENCE SET; Schema: private; Owner: postgres
--

SELECT pg_catalog.setval('private.fixed_asset_types_id_seq', 90, true);


--
-- Name: fixed_assets_amortization_groups_id_seq; Type: SEQUENCE SET; Schema: private; Owner: postgres
--

SELECT pg_catalog.setval('private.fixed_assets_amortization_groups_id_seq', 5, true);


--
-- Name: fuels_id_seq; Type: SEQUENCE SET; Schema: private; Owner: postgres
--

SELECT pg_catalog.setval('private.fuels_id_seq', 9, true);


--
-- Name: international_diagnosis_of_professional_diseases_id_seq; Type: SEQUENCE SET; Schema: private; Owner: postgres
--

SELECT pg_catalog.setval('private.international_diagnosis_of_professional_diseases_id_seq', 1, true);


--
-- Name: international_diagnosis_of_work_diseases_id_seq; Type: SEQUENCE SET; Schema: private; Owner: postgres
--

SELECT pg_catalog.setval('private.international_diagnosis_of_work_diseases_id_seq', 1, true);


--
-- Name: measurement_units_id_seq; Type: SEQUENCE SET; Schema: private; Owner: postgres
--

SELECT pg_catalog.setval('private.measurement_units_id_seq', 7, true);


--
-- Name: mode_injuries_id_seq; Type: SEQUENCE SET; Schema: private; Owner: postgres
--

SELECT pg_catalog.setval('private.mode_injuries_id_seq', 53, true);


--
-- Name: occupations_id_seq; Type: SEQUENCE SET; Schema: private; Owner: postgres
--

SELECT pg_catalog.setval('private.occupations_id_seq', 4260, true);


--
-- Name: paychecks_codebook_id_seq; Type: SEQUENCE SET; Schema: private; Owner: postgres
--

SELECT pg_catalog.setval('private.paychecks_codebook_id_seq', 56, true);


--
-- Name: payment_codes_id_seq; Type: SEQUENCE SET; Schema: private; Owner: postgres
--

SELECT pg_catalog.setval('private.payment_codes_id_seq', 401, true);


--
-- Name: payment_configurations_id_seq; Type: SEQUENCE SET; Schema: private; Owner: postgres
--

SELECT pg_catalog.setval('private.payment_configurations_id_seq', 3, true);


--
-- Name: postal_codes_id_seq; Type: SEQUENCE SET; Schema: private; Owner: postgres
--

SELECT pg_catalog.setval('private.postal_codes_id_seq', 1565, true);


--
-- Name: professional_degrees_id_seq; Type: SEQUENCE SET; Schema: private; Owner: postgres
--

SELECT pg_catalog.setval('private.professional_degrees_id_seq', 7, true);


--
-- Name: qualification_levels_id_seq; Type: SEQUENCE SET; Schema: private; Owner: postgres
--

SELECT pg_catalog.setval('private.qualification_levels_id_seq', 14, true);


--
-- Name: reasons_for_enforcement_training_id_seq; Type: SEQUENCE SET; Schema: private; Owner: postgres
--

SELECT pg_catalog.setval('private.reasons_for_enforcement_training_id_seq', 1, true);


--
-- Name: required_medical_abilities_id_seq; Type: SEQUENCE SET; Schema: private; Owner: postgres
--

SELECT pg_catalog.setval('private.required_medical_abilities_id_seq', 1, true);


--
-- Name: safe_file_extensions_id_seq; Type: SEQUENCE SET; Schema: private; Owner: postgres
--

SELECT pg_catalog.setval('private.safe_file_extensions_id_seq', 107, true);


--
-- Name: safe_work_dangers_id_seq; Type: SEQUENCE SET; Schema: private; Owner: postgres
--

SELECT pg_catalog.setval('private.safe_work_dangers_id_seq', 3, true);


--
-- Name: source_injuries_id_seq; Type: SEQUENCE SET; Schema: private; Owner: postgres
--

SELECT pg_catalog.setval('private.source_injuries_id_seq', 193, true);


--
-- Name: streets_id_seq; Type: SEQUENCE SET; Schema: private; Owner: postgres
--

SELECT pg_catalog.setval('private.streets_id_seq', 1, true);


--
-- Name: treasury_office_units_id_seq; Type: SEQUENCE SET; Schema: private; Owner: postgres
--

SELECT pg_catalog.setval('private.treasury_office_units_id_seq', 207, true);


--
-- Name: treasury_offices_id_seq; Type: SEQUENCE SET; Schema: private; Owner: postgres
--

SELECT pg_catalog.setval('private.treasury_offices_id_seq', 146, true);


--
-- Name: work_codes_id_seq; Type: SEQUENCE SET; Schema: private; Owner: postgres
--

SELECT pg_catalog.setval('private.work_codes_id_seq', 1774, true);


--
-- Name: account_layouts_laws_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.account_layouts_laws_id_seq', 1, true);


--
-- Name: account_layouts account_layouts_code_key; Type: CONSTRAINT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.account_layouts
    ADD CONSTRAINT account_layouts_code_key UNIQUE (code, law_id);


--
-- Name: account_layouts_laws account_layouts_laws_pkey; Type: CONSTRAINT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.account_layouts_laws
    ADD CONSTRAINT account_layouts_laws_pkey PRIMARY KEY (id);


--
-- Name: account_layouts account_plans_pkey; Type: CONSTRAINT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.account_layouts
    ADD CONSTRAINT account_plans_pkey PRIMARY KEY (id);


--
-- Name: addresses addresses_pkey; Type: CONSTRAINT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.addresses
    ADD CONSTRAINT addresses_pkey PRIMARY KEY (id);


--
-- Name: addresses addresses_street_id_street_number_door_number_key; Type: CONSTRAINT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.addresses
    ADD CONSTRAINT addresses_street_id_street_number_door_number_key UNIQUE (street_id, street_number, door_number);


--
-- Name: fixed_assets_amortization_groups amortization_groups_fixed_assets_amortization_group_key; Type: CONSTRAINT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.fixed_assets_amortization_groups
    ADD CONSTRAINT amortization_groups_fixed_assets_amortization_group_key UNIQUE (group_name);


--
-- Name: fixed_assets_amortization_groups amortization_groups_fixed_assets_pkey; Type: CONSTRAINT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.fixed_assets_amortization_groups
    ADD CONSTRAINT amortization_groups_fixed_assets_pkey PRIMARY KEY (id);


--
-- Name: banks banks_PIB_key; Type: CONSTRAINT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.banks
    ADD CONSTRAINT "banks_PIB_key" UNIQUE ("PIB");


--
-- Name: banks banks_pkey; Type: CONSTRAINT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.banks
    ADD CONSTRAINT banks_pkey PRIMARY KEY (id);


--
-- Name: cities cities_bzr_code_key; Type: CONSTRAINT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.cities
    ADD CONSTRAINT cities_bzr_code_key UNIQUE (bzr_code);


--
-- Name: cities cities_pkey; Type: CONSTRAINT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.cities
    ADD CONSTRAINT cities_pkey PRIMARY KEY (id);


--
-- Name: cities_to_postal_codes cities_to_postal_codes_city_id_postal_code_id_key; Type: CONSTRAINT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.cities_to_postal_codes
    ADD CONSTRAINT cities_to_postal_codes_city_id_postal_code_id_key UNIQUE (city_id, postal_code_id);


--
-- Name: cities_to_postal_codes cities_to_postal_codes_pkey; Type: CONSTRAINT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.cities_to_postal_codes
    ADD CONSTRAINT cities_to_postal_codes_pkey PRIMARY KEY (id);


--
-- Name: countries countries_name_key; Type: CONSTRAINT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.countries
    ADD CONSTRAINT countries_name_key UNIQUE (name);


--
-- Name: countries countries_pkey; Type: CONSTRAINT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.countries
    ADD CONSTRAINT countries_pkey PRIMARY KEY (id);


--
-- Name: currencies currencies_pkey; Type: CONSTRAINT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.currencies
    ADD CONSTRAINT currencies_pkey PRIMARY KEY (id);


--
-- Name: driver_license_categories driver_license_categories_pkey; Type: CONSTRAINT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.driver_license_categories
    ADD CONSTRAINT driver_license_categories_pkey PRIMARY KEY (id);


--
-- Name: fixed_asset_types fixed_assets_types_pkey; Type: CONSTRAINT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.fixed_asset_types
    ADD CONSTRAINT fixed_assets_types_pkey PRIMARY KEY (id);


--
-- Name: fuels fuels_pkey; Type: CONSTRAINT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.fuels
    ADD CONSTRAINT fuels_pkey PRIMARY KEY (id);


--
-- Name: international_diagnosis_of_professional_diseases international_diagnosis_of_professional_diseases_code_key; Type: CONSTRAINT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.international_diagnosis_of_professional_diseases
    ADD CONSTRAINT international_diagnosis_of_professional_diseases_code_key UNIQUE (code);


--
-- Name: international_diagnosis_of_professional_diseases international_diagnosis_of_professional_diseases_pkey; Type: CONSTRAINT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.international_diagnosis_of_professional_diseases
    ADD CONSTRAINT international_diagnosis_of_professional_diseases_pkey PRIMARY KEY (id);


--
-- Name: international_diagnosis_of_work_diseases international_diagnosis_of_work_diseases_code_key; Type: CONSTRAINT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.international_diagnosis_of_work_diseases
    ADD CONSTRAINT international_diagnosis_of_work_diseases_code_key UNIQUE (code);


--
-- Name: international_diagnosis_of_work_diseases international_diagnosis_of_work_diseases_pkey; Type: CONSTRAINT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.international_diagnosis_of_work_diseases
    ADD CONSTRAINT international_diagnosis_of_work_diseases_pkey PRIMARY KEY (id);


--
-- Name: measurement_units measurement_units_pkey; Type: CONSTRAINT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.measurement_units
    ADD CONSTRAINT measurement_units_pkey PRIMARY KEY (id);


--
-- Name: mode_injuries mode_injuries_code_key; Type: CONSTRAINT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.mode_injuries
    ADD CONSTRAINT mode_injuries_code_key UNIQUE (code);


--
-- Name: mode_injuries mode_injuries_pkey; Type: CONSTRAINT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.mode_injuries
    ADD CONSTRAINT mode_injuries_pkey PRIMARY KEY (id);


--
-- Name: occupations occupations_code_key; Type: CONSTRAINT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.occupations
    ADD CONSTRAINT occupations_code_key UNIQUE (code);


--
-- Name: occupations occupations_pkey; Type: CONSTRAINT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.occupations
    ADD CONSTRAINT occupations_pkey PRIMARY KEY (id);


--
-- Name: paychecks_codebook paychecks_codebook_pkey; Type: CONSTRAINT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.paychecks_codebook
    ADD CONSTRAINT paychecks_codebook_pkey PRIMARY KEY (id);


--
-- Name: payment_codes payment_codes_pkey; Type: CONSTRAINT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.payment_codes
    ADD CONSTRAINT payment_codes_pkey PRIMARY KEY (id);


--
-- Name: payment_configurations payment_configurations_pkey; Type: CONSTRAINT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.payment_configurations
    ADD CONSTRAINT payment_configurations_pkey PRIMARY KEY (id);


--
-- Name: postal_codes postal_codes_code_country_id_key; Type: CONSTRAINT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.postal_codes
    ADD CONSTRAINT postal_codes_code_country_id_key UNIQUE (code, country_id);


--
-- Name: postal_codes postal_codes_pkey; Type: CONSTRAINT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.postal_codes
    ADD CONSTRAINT postal_codes_pkey PRIMARY KEY (id);


--
-- Name: professional_degrees professional_degrees_pkey; Type: CONSTRAINT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.professional_degrees
    ADD CONSTRAINT professional_degrees_pkey PRIMARY KEY (id);


--
-- Name: qualification_levels qualification_levels_code_key; Type: CONSTRAINT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.qualification_levels
    ADD CONSTRAINT qualification_levels_code_key UNIQUE (code);


--
-- Name: qualification_levels qualification_levels_pkey; Type: CONSTRAINT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.qualification_levels
    ADD CONSTRAINT qualification_levels_pkey PRIMARY KEY (id);


--
-- Name: reasons_for_enforcement_training reasons_for_enforcement_training_code_key; Type: CONSTRAINT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.reasons_for_enforcement_training
    ADD CONSTRAINT reasons_for_enforcement_training_code_key UNIQUE (code);


--
-- Name: reasons_for_enforcement_training reasons_for_enforcement_training_pkey; Type: CONSTRAINT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.reasons_for_enforcement_training
    ADD CONSTRAINT reasons_for_enforcement_training_pkey PRIMARY KEY (id);


--
-- Name: required_medical_abilities required_medical_abilities_name_key; Type: CONSTRAINT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.required_medical_abilities
    ADD CONSTRAINT required_medical_abilities_name_key UNIQUE (name);


--
-- Name: required_medical_abilities required_medical_abilitys_pkey; Type: CONSTRAINT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.required_medical_abilities
    ADD CONSTRAINT required_medical_abilitys_pkey PRIMARY KEY (id);


--
-- Name: safe_file_extensions safe_file_extensions_name_key; Type: CONSTRAINT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.safe_file_extensions
    ADD CONSTRAINT safe_file_extensions_name_key UNIQUE (name);


--
-- Name: safe_file_extensions safe_file_extensions_pkey; Type: CONSTRAINT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.safe_file_extensions
    ADD CONSTRAINT safe_file_extensions_pkey PRIMARY KEY (id);


--
-- Name: safe_work_dangers safe_work_dangers_code_key; Type: CONSTRAINT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.safe_work_dangers
    ADD CONSTRAINT safe_work_dangers_code_key UNIQUE (code);


--
-- Name: safe_work_dangers safe_work_dangers_pkey; Type: CONSTRAINT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.safe_work_dangers
    ADD CONSTRAINT safe_work_dangers_pkey PRIMARY KEY (id);


--
-- Name: source_injuries source_injuries_code_key; Type: CONSTRAINT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.source_injuries
    ADD CONSTRAINT source_injuries_code_key UNIQUE (code);


--
-- Name: source_injuries source_injuries_pkey; Type: CONSTRAINT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.source_injuries
    ADD CONSTRAINT source_injuries_pkey PRIMARY KEY (id);


--
-- Name: streets streets_name_city_id_postal_code_id_key; Type: CONSTRAINT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.streets
    ADD CONSTRAINT streets_name_city_id_postal_code_id_key UNIQUE (name, city_id, postal_code_id);


--
-- Name: streets streets_pkey; Type: CONSTRAINT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.streets
    ADD CONSTRAINT streets_pkey PRIMARY KEY (id);


--
-- Name: treasury_office_units treasury_office_units_code_key; Type: CONSTRAINT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.treasury_office_units
    ADD CONSTRAINT treasury_office_units_code_key UNIQUE (code);


--
-- Name: treasury_office_units treasury_office_units_pkey; Type: CONSTRAINT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.treasury_office_units
    ADD CONSTRAINT treasury_office_units_pkey PRIMARY KEY (id);


--
-- Name: treasury_offices treasury_offices_name_key; Type: CONSTRAINT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.treasury_offices
    ADD CONSTRAINT treasury_offices_name_key UNIQUE (name);


--
-- Name: treasury_offices treasury_offices_pkey; Type: CONSTRAINT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.treasury_offices
    ADD CONSTRAINT treasury_offices_pkey PRIMARY KEY (id);


--
-- Name: countries unique_countries_numeric_code; Type: CONSTRAINT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.countries
    ADD CONSTRAINT unique_countries_numeric_code UNIQUE (numeric_code);


--
-- Name: currencies unique_currencies_code; Type: CONSTRAINT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.currencies
    ADD CONSTRAINT unique_currencies_code UNIQUE (code);


--
-- Name: driver_license_categories unique_driver_license_categories_category; Type: CONSTRAINT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.driver_license_categories
    ADD CONSTRAINT unique_driver_license_categories_category UNIQUE (category);


--
-- Name: measurement_units unique_measurement_units_name; Type: CONSTRAINT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.measurement_units
    ADD CONSTRAINT unique_measurement_units_name UNIQUE (name);


--
-- Name: payment_codes unique_payment_codes_code; Type: CONSTRAINT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.payment_codes
    ADD CONSTRAINT unique_payment_codes_code UNIQUE (code);


--
-- Name: professional_degrees unique_professional_degrees_name; Type: CONSTRAINT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.professional_degrees
    ADD CONSTRAINT unique_professional_degrees_name UNIQUE (name);


--
-- Name: work_codes work_codes_code_key; Type: CONSTRAINT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.work_codes
    ADD CONSTRAINT work_codes_code_key UNIQUE (code);


--
-- Name: work_codes work_codes_pkey; Type: CONSTRAINT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.work_codes
    ADD CONSTRAINT work_codes_pkey PRIMARY KEY (id);


--
-- Name: tbl_migration tbl_migration_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tbl_migration
    ADD CONSTRAINT tbl_migration_pkey PRIMARY KEY (version);


--
-- Name: account_layouts account_layouts_law_id_fkey; Type: FK CONSTRAINT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.account_layouts
    ADD CONSTRAINT account_layouts_law_id_fkey FOREIGN KEY (law_id) REFERENCES private.account_layouts_laws(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: account_layouts account_plans_parent_id_fkey; Type: FK CONSTRAINT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.account_layouts
    ADD CONSTRAINT account_plans_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES private.account_layouts(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: addresses addresses_street_id_fkey; Type: FK CONSTRAINT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.addresses
    ADD CONSTRAINT addresses_street_id_fkey FOREIGN KEY (street_id) REFERENCES private.streets(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: cities cities_country_id_fkey; Type: FK CONSTRAINT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.cities
    ADD CONSTRAINT cities_country_id_fkey FOREIGN KEY (country_id) REFERENCES private.countries(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: cities cities_default_postal_code_id_fkey; Type: FK CONSTRAINT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.cities
    ADD CONSTRAINT cities_default_postal_code_id_fkey FOREIGN KEY (default_postal_code_id) REFERENCES private.postal_codes(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: cities_to_postal_codes cities_to_postal_codes_city_id_fkey; Type: FK CONSTRAINT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.cities_to_postal_codes
    ADD CONSTRAINT cities_to_postal_codes_city_id_fkey FOREIGN KEY (city_id) REFERENCES private.cities(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: cities_to_postal_codes cities_to_postal_codes_postal_code_id_fkey; Type: FK CONSTRAINT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.cities_to_postal_codes
    ADD CONSTRAINT cities_to_postal_codes_postal_code_id_fkey FOREIGN KEY (postal_code_id) REFERENCES private.postal_codes(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: treasury_office_units country_id_fkey; Type: FK CONSTRAINT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.treasury_office_units
    ADD CONSTRAINT country_id_fkey FOREIGN KEY (country_id) REFERENCES private.countries(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: fixed_asset_types fixed_assets_types_fa_amortization_group_id_fkey; Type: FK CONSTRAINT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.fixed_asset_types
    ADD CONSTRAINT fixed_assets_types_fa_amortization_group_id_fkey FOREIGN KEY (fixed_asset_amortization_group_id) REFERENCES private.fixed_assets_amortization_groups(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: mode_injuries mode_injuries_parent_id_fkey; Type: FK CONSTRAINT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.mode_injuries
    ADD CONSTRAINT mode_injuries_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES private.mode_injuries(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: occupations occupations_parent_id_fkey; Type: FK CONSTRAINT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.occupations
    ADD CONSTRAINT occupations_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES private.occupations(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: postal_codes postal_codes_country_id_fkey; Type: FK CONSTRAINT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.postal_codes
    ADD CONSTRAINT postal_codes_country_id_fkey FOREIGN KEY (country_id) REFERENCES private.countries(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: source_injuries source_injuries_parent_id_fkey; Type: FK CONSTRAINT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.source_injuries
    ADD CONSTRAINT source_injuries_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES private.source_injuries(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: streets streets_city_id_fkey; Type: FK CONSTRAINT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.streets
    ADD CONSTRAINT streets_city_id_fkey FOREIGN KEY (city_id) REFERENCES private.cities(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: streets streets_postal_code_id_fkey; Type: FK CONSTRAINT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.streets
    ADD CONSTRAINT streets_postal_code_id_fkey FOREIGN KEY (postal_code_id) REFERENCES private.postal_codes(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: cities treasury_office_unit_id_fkey; Type: FK CONSTRAINT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.cities
    ADD CONSTRAINT treasury_office_unit_id_fkey FOREIGN KEY (treasury_office_unit_id) REFERENCES private.treasury_office_units(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: treasury_office_units treasury_office_units_main_branch_id_fkey; Type: FK CONSTRAINT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.treasury_office_units
    ADD CONSTRAINT treasury_office_units_main_branch_id_fkey FOREIGN KEY (main_branch_id) REFERENCES private.treasury_offices(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: treasury_office_units treasury_office_units_sub_branch_id_fkey; Type: FK CONSTRAINT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.treasury_office_units
    ADD CONSTRAINT treasury_office_units_sub_branch_id_fkey FOREIGN KEY (sub_branch_id) REFERENCES private.treasury_offices(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: treasury_offices treasury_offices_parent_id_fkey; Type: FK CONSTRAINT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.treasury_offices
    ADD CONSTRAINT treasury_offices_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES private.treasury_offices(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: work_codes work_codes_parent_id_fkey; Type: FK CONSTRAINT; Schema: private; Owner: postgres
--

ALTER TABLE ONLY private.work_codes
    ADD CONSTRAINT work_codes_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES private.work_codes(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- PostgreSQL database dump complete
--

