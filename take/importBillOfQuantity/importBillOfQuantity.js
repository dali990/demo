/* global Vue, sima, Prism */
/* languages=markup+css+clike+javascript+c+csharp+bash+cpp+ruby+markup-templating+java+php+sql+python */
Vue.component('import-bill-of-quantity', {
    template: '#import-bill-of-quantity',
    props: {
        bill_of_quantity: {
            validator: sima.vue.ProxyValidator
        },
        bill_of_quantity_item_group: {
            validator: sima.vue.ProxyValidator
        }
    },
    data: function () {
        return {
            text: '',
            editing_imported_content: true,
            content_preview_panel_enabled: false, //overlay za desable-ovanje desnog panela
            can_save: true, //validacija
            show_save_btn: false, //treba da se prikaze kada su sve kolone povezane
            imported_content_table: null, // importovana tabela iz excela
            iboq_template: { // Struktura za preview tabelu
                name: sima.translate('ImportBillOfQuantityName'),
                measurement_unit: sima.translate('ImportBillOfQuantityMeasurementUnit'),
                value_per_unit: sima.translate('ImportBillOfQuantityValuePerUnit'),
                objects: []
            },
            boq_map_link_btn: "", //sadrzi naziv kliknutog dugmeta(kolone) za povezivanje sa kolonoma iz excel tabele
            boq_map: { //Mapiranje kolona excel tabele sa kolonama predmera, {naizv_kolone_premdera: 'index_kolone_excel_tabele'}
                name: "",
                measurement_unit: "",
                value_per_unit: "",
                objects: {}
            },
            new_boq_items: [], //Ekstraktovani podaci za cuvanje iz excel tabele
            decimal_chars: [',', '.'],
            selected_decimal_char: '.',
            fields_to_format: ['value_per_unit', 'object_value_per_unit', 'quantity', 'value']
        };
    },
    computed: {
        bill_of_quantity_root_group: function () {
            return this.bill_of_quantity.root_group;
        },
        boq_map_name_check: function () {
            return this.boq_map.name !== "" && !isNaN(this.boq_map.name);
        },
        boq_map_measurement_unit_check: function () {
            return this.boq_map.measurement_unit !== "" && !isNaN(this.boq_map.measurement_unit);
        },
        boq_map_value_per_unit_check: function () {
            return this.boq_map.value_per_unit !== "" && !isNaN(this.boq_map.value_per_unit);
        },
        boq_map_quantity_check: function () {
            return this.boq_map.name !== "" && !isNaN(this.boq_map.name);
        },
        bill_of_quantity_has_object_value_per_unit: function() {
            this.appendObjectsToTemplate();
            this.initContentPreviewTable();
            return this.bill_of_quantity.has_object_value_per_unit;
        }
    },
    watch: {
        selected_decimal_char: function (new_val, old_val) {
            if (new_val !== old_val) {
                this.extractData();
            }
        },
        bill_of_quantity_has_object_value_per_unit: function (new_val, old_val) {
            this.appendObjectsToTemplate();
            this.initContentPreviewTable();
        }
    },
    mounted: function () {
        var _this = this;
        $(this.$el).on('destroyed', function () {
            _this.$destroy();
        });

        this.iboq_table = this.$el.querySelector('.content-preview-table');
        this.appendObjectsToTemplate();
        this.initContentPreviewTable();
    },
    updated() {
        this.parseImportedContent();
    },
    methods: {
        appendObjectsToTemplate: function () {
            this.iboq_template.objects = this.bill_of_quantity.leaf_objects;
            if (this.bill_of_quantity && this.bill_of_quantity.has_object_value_per_unit) {
                Vue.delete(this.iboq_template, 'value_per_unit');
                Vue.delete(this.boq_map, 'value_per_unit');
            }
        },
        fillContentPreviewTable: function () {
            if(this.iboq_table.tBodies.length > 0)
            {
                this.iboq_table.removeChild(this.iboq_table.tBodies[0]);
            }
            this.iboq_table.appendChild(document.createElement('tbody'));
            var tbody = this.iboq_table.tBodies[0];
            for (var index in this.new_boq_items) {
                var row = tbody.insertRow();
                var boq = this.new_boq_items[index];
                for (var template_key in this.iboq_template) {
                    var boq_item = boq[template_key];
                    if (template_key === 'objects') {
                        for (var object_id in boq_item) {
                            var object = boq_item[object_id];
                            var cell_qty = row.insertCell();
                            var object_quantity = this.getEmptyStringOrValue(object, 'quantity');
                            var cell_text_qty = document.createTextNode(this.formatField(object_quantity, 'quantity'));
                            cell_qty.classList.add('content-preview-table-cell');
                            cell_qty.appendChild(cell_text_qty);
                            if (object.hasOwnProperty('object_value_per_unit')) {
                                var cell_ovpu = row.insertCell();
                                var object_ovpu = this.getEmptyStringOrValue(object, 'object_value_per_unit');
                                var cell_text_ovpu = document.createTextNode(this.formatField(object_ovpu, 'object_value_per_unit'));
                                cell_ovpu.classList.add('content-preview-table-cell');
                                cell_ovpu.appendChild(cell_text_ovpu);
                            }
                            var cell_amt = row.insertCell();
                            var object_value = this.getEmptyStringOrValue(object, 'value');
                            var cell_text_amt = document.createTextNode(this.formatField(object_value, 'value'));
                            cell_amt.classList.add('content-preview-table-cell');
                            cell_amt.appendChild(cell_text_amt);
                        }
                    }
                    else {
                        var cell = row.insertCell();
                        var boq_item_value = this.getEmptyStringOrValue(boq_item, template_key);
                        cell.classList.add('content-preview-table-cell');
                        var new_text = document.createTextNode(this.formatField(boq_item_value, template_key));
                        var div = document.createElement("div");
                        div.classList.add('iboq-table-cell-wrapper-long-text');
                        div.setAttribute('title', boq_item);
                        div.appendChild(new_text);
                        cell.appendChild(div);
                    }
                }
            }
        },
        formatField(num, field_name){
            if(this.fields_to_format.includes(field_name) && typeof num !== 'undefined' && num !== '')
            {
                //ako se prebaci u float, moze da izgubi precis\znost kada su veliki brojevi u pitanju
                //da bi se implementiralo kako treba, mora da se uradi zaokruzivanje velikih brojeva zapisanih u stringu
//                return this.$options.filters.formatNumber(parseFloat(num));
                return this.$options.filters.formatNumber(num);
            }
            return num;
        },
        getEmptyStringOrValue(obj, prop_name='')
        {
            if(typeof obj !== 'undefined')
            {
                if(typeof obj === 'object')
                {
                    if(obj.hasOwnProperty(prop_name))
                    {
                        return obj[prop_name];
                    }
                    else
                    {
                        return '';
                    }
                }
                else
                {
                    return obj;
                }              
            }
            
            return '';
        },
        initContentPreviewTable: function () {
            if(this.iboq_table)
            {
                this.iboq_table.innerHTML = "";
                var header = this.iboq_table.createTHead();
                var row_1 = header.insertRow();
                var row_2 = header.insertRow();
                this.iboq_table.insertRow();

                for (var key in this.iboq_template) {
                    var iboq_item = this.iboq_template[key];
                    if (key === 'objects' && iboq_item) {
                        for (var object_index in iboq_item) {
                            var object = iboq_item[object_index];

                            var object_colspan = 2;
                            if (object.bill_of_quantity.has_object_value_per_unit) {
                                object_colspan++;
                            }

                            var cell1_row1 = row_1.insertCell();
                            cell1_row1.colSpan = object_colspan;
                            var text_cell1_row1 = document.createTextNode(object.name);
                            cell1_row1.appendChild(text_cell1_row1);

                            var cell1_row2 = row_2.insertCell();
                            if (object.bill_of_quantity.has_object_value_per_unit) {
                                var cell1_vpu_row2 = row_2.insertCell();
                                var text_cell1_vpu_row2_row2 = document.createTextNode(sima.translate('ImportBillOfQuantityValuePerUnit'));
                                cell1_vpu_row2.appendChild(text_cell1_vpu_row2_row2);
                            }
                            var cell2_row2 = row_2.insertCell();
                            var text_cell1_row2 = document.createTextNode(sima.translate('ImportBillOfQuantityQuantity'));
                            var text_cell2_row2 = document.createTextNode(sima.translate('ImportBillOfQuantityValue'));
                            cell1_row2.appendChild(text_cell1_row2);
                            cell2_row2.appendChild(text_cell2_row2);
                        }
                    }
                    else {
                        var cell = row_1.insertCell();
                        cell.rowSpan = 2;
                        var new_text = document.createTextNode(iboq_item);
                        cell.appendChild(new_text);
                    }
                }
            }
        },
        //Povezuje kolone importovane tabele sa predmerom i izvlaci podatke iz importovane tabele
        parseImportedContent: function () {
            var _this = this;
            this.imported_content_table = this.$el.querySelector('.imported-content-table table');

            if (_this.imported_content_table !== null) {
                this.styleImportedTable();
                //Povezivanje kolona
                $(_this.imported_content_table).unbind().on('click', 'td', function (event) {
                    var cell = this;
                    var row = cell.parentElement;
                    //Eksplicitno povezivanje kolona, kliktanjem kolona predmera(dugme) - kolona excel tabele
                    if (_this.boq_map_link_btn !== "") {
                        if (typeof _this.boq_map_link_btn === "object") {
                            var new_objects = _this.boq_map.objects;
                            if (!_this.boq_map.objects.hasOwnProperty(_this.boq_map_link_btn.object_id)) {
                                _this.boq_map.objects[_this.boq_map_link_btn.object_id] = {};
                            }
//                            new_objects[_this.boq_map_link_btn.object_id] = {
//                                _this.boq_map_link_btn.item_name:
//                            };
//                            Vue.set(_this.boq_map.objects, new_objects);
                            _this.boq_map.objects[_this.boq_map_link_btn.object_id][_this.boq_map_link_btn.item_name] = cell.cellIndex;
                            _this.$forceUpdate();
                            _this.highlighteColumn(cell.cellIndex);
                        }
                        else {
                            _this.boq_map[_this.boq_map_link_btn] = cell.cellIndex;
                            _this.highlighteColumn(cell.cellIndex);
                        }
                        _this.boq_map_link_btn = "";
                    }
                    // Povezivanje kolona kliktanjem kolona excel tabele po redosledu kolona (dugmica) predmera
                    else {
                        //prvi klik 
                        if (_this.isInObj(_this.boq_map, cell.cellIndex) === false) {
                            for (var item_name in _this.boq_map) {
                                if (item_name === "objects") {
                                    //var objects = _this.boq_map.objects;
                                    var objects = _this.bill_of_quantity.leaf_objects;
                                    for (var index in objects) {
                                        var object = objects[index];
                                        var object_maped = false;
                                        if (!_this.boq_map.objects.hasOwnProperty(object.id)) {
                                            _this.boq_map.objects[object.id] = {};
                                        }
                                        if (!_this.boq_map.objects[object.id].hasOwnProperty('quantity')) {
                                            _this.boq_map.objects[object.id]['quantity'] = cell.cellIndex;
                                            object_maped = true;
                                        }
                                        else if (
                                            object.bill_of_quantity.has_object_value_per_unit &&
                                            !_this.boq_map.objects[object.id].hasOwnProperty('object_value_per_unit')
                                        ) {
                                            _this.boq_map.objects[object.id]['object_value_per_unit'] = cell.cellIndex;
                                            object_maped = true;
                                        }
                                        else if (!_this.boq_map.objects[object.id].hasOwnProperty('value')) {
                                            _this.boq_map.objects[object.id]['value'] = cell.cellIndex;
                                            object_maped = true;
                                        }
                                        _this.$forceUpdate();
                                        _this.highlighteColumn(cell.cellIndex);
                                        if (object_maped) {
                                            break;
                                        }
                                    }
                                    break;
                                }
                                else {
                                    if (_this.boq_map[item_name] === "") {
                                        _this.boq_map[item_name] = cell.cellIndex;
                                        _this.highlighteColumn(cell.cellIndex);
                                        break;
                                    }
                                }
                            }
                        }
                        //drugi klik na vec povezane kolone
                        else {
                            _this.unLinkItemByIndex(cell.cellIndex);
                        }
                    }
                    _this.checkCanSave();
                    _this.extractData();
                });
            }
        },
        styleImportedTable(){
            var _this = this;
            for (let row_index = 0; row_index < _this.imported_content_table.rows.length; row_index++) {
                var row = _this.imported_content_table.rows[row_index];
                for (var cell_index = 0; cell_index < row.cells.length; cell_index++) {
                    var cell = row.cells[cell_index];
                    if(cell.textContent.length > 60){
                        cell.removeAttribute('height');
                        cell.setAttribute('title', cell.textContent);
                        cell.innerHTML = "<div class='iboq-table-cell-wrapper-long-text'>"+cell.textContent+"</div>";
                    }
                }                    
            }
        },
        saveGroupItems: function () {
            var _this = this;
            if(this.can_save)
            {
                sima.ajax.get('buildings/billOfQuantity/saveGroupItems', {
                    data: {
                        group_id: _this.bill_of_quantity_item_group.id,
                        items: _this.new_boq_items
                    },
                    success_function: function () {
                        sima.dialog.close();
                    }
                });
            }
            else
            {
                sima.dialog.openWarn(sima.translate('ImportBillOfQuantityErrorWhileSavingUnitPricesAndQuantities'))
            }
        },
        linkBOQItems: function (item_name, object = {}) {
            this.boq_map_link_btn = ""; //Mozda nije potrebno vise ovde
            if (item_name === "object") {
                if (this.boq_map.objects.hasOwnProperty(object.object_id)) {
                    this.unLinkObjectItem(object);
                    this.extractData();
                    this.$forceUpdate();
                }
                else {
                    this.boq_map_link_btn = object;
                }
            }
            else 
            {
                if (this.boq_map[item_name] !== "") {
                    this.unLinkItemByIndex(this.boq_map[item_name]);
                    this.extractData();
                }
                else
                {
                    this.boq_map_link_btn = item_name;
                }
            }
            this.checkCanSave();
            this.fillContentPreviewTable();
        },
        unLinkItemByIndex: function (cell_index) {
            this.new_boq_items = [];
            var _this = this;
            var status = false;

            var item = _this.searchObj(_this.boq_map, cell_index);
            if (item.child_result)//ako je objekat
            {
                _this.unLinkObjectItem({
                    object_id: item.child_result.prop_name,
                    item_name: item.child_result.child_result.prop_name
                });
                _this.$forceUpdate();
                status = true;
            }
            else if (item.prop_name)//ako je stavka
            {
                _this.boq_map[item.prop_name] = "";
                status = true;
            }

            _this.unHighlighteColumn(cell_index);
            return status;
        },
        unLinkObjectItem(object) {
            this.new_boq_items = [];
            //_this.boq_map.objects[object_id] nema prop length za proveru da li je prazan objekat kako bi se uklonio
            if (this.boq_map.objects[object.object_id].hasOwnProperty(object.item_name)) {
                var cell_index = this.boq_map.objects[object.object_id][object.item_name];
                Vue.delete(this.boq_map.objects[object.object_id], object.item_name);
                this.unHighlighteColumn(cell_index);
            }
            if (
                !this.boq_map.objects[object.object_id].hasOwnProperty('quantity') &&
                !this.boq_map.objects[object.object_id].hasOwnProperty('value')
            ) {
                Vue.delete(this.boq_map.objects, object.object_id);
            }
        },
        highlighteColumn: function (cell_index) {
            var _this = this;
            for (let row_index = 0; row_index < _this.imported_content_table.rows.length; row_index++) {
                var row = _this.imported_content_table.rows[row_index];
                row.cells[cell_index].classList.add("imported-content-table-column");
                if (row_index === 0) {
                    row.cells[cell_index].classList.add("imported-content-table-first-row");
                }
                else if ((row_index + 1) == _this.imported_content_table.rows.length) {
                    row.cells[cell_index].classList.add("imported-content-table-last-row");
                }
            }
        },
        unHighlighteColumn: function (cell_index) {
            var _this = this;
            for (let row_index = 0; row_index < _this.imported_content_table.rows.length; row_index++) {
                var row = _this.imported_content_table.rows[row_index];
                row.cells[cell_index].classList.remove("imported-content-table-column");
                if (row_index === 0) {
                    row.cells[cell_index].classList.remove("imported-content-table-first-row");
                }
                else if ((row_index + 1) == _this.imported_content_table.rows.length) {
                    row.cells[cell_index].classList.remove("imported-content-table-last-row");
                }
            }
        },
        extractData: function () {
            var _this = this;
            this.new_boq_items = [];

            for (let row_index = 0; row_index < _this.imported_content_table.rows.length; row_index++) {
                var boq = {
                    objects: {}
                };
                for (var item_name in _this.boq_map) {
                    var boq_item_index = _this.boq_map[item_name];
                    var row = _this.imported_content_table.rows[row_index];

                    if (item_name === 'objects') {
                        var objects = _this.boq_map.objects;

                        for (var object_id in objects) {
                            var object = objects[object_id];
                            if (!boq.objects.hasOwnProperty(object_id)) {
                                boq.objects[object_id] = {};
                            }
                            if (object.hasOwnProperty('quantity')) {
                                var cell_quantity = row.cells[object.quantity];
                                boq.objects[object_id]["quantity"] = _this.getCellValue(cell_quantity, 'quantity');
                            }
                            if (object.hasOwnProperty('object_value_per_unit')) {
                                var cell_object_value_per_unit = row.cells[object.object_value_per_unit];
                                boq.objects[object_id]["object_value_per_unit"] = _this.getCellValue(cell_object_value_per_unit, 'object_value_per_unit');
                            }
                            if (object.hasOwnProperty('value')) {
                                var cell_value = row.cells[object.value];
                                boq.objects[object_id]["value"] = _this.getCellValue(cell_value, 'value');
                            }
                        }
                    }
                    else {
                        var cell = row.cells[boq_item_index];
                        if (boq_item_index !== "") {
                            boq[item_name] = _this.getCellValue(cell, item_name);
                        }
                    }
                }
                _this.new_boq_items.push(boq);
            }
            _this.fillContentPreviewTable();                  
        },
        getCellValue: function (cell, item_name) {
            var value = "";
            value = cell.textContent.trim();
            if(value === "")
            {
                this.can_save = false;
            }
            else if(this.fields_to_format.includes(item_name))
            {
                if (this.selected_decimal_char === '.') {
                    value = value.replace(/,/g, "");
                }
                else if (this.selected_decimal_char === ',') {
                    value = value.replace(/\./g, "").replace(/,/g, ".");
                }
            }
            return value;
        },
        resetContentPreview: function () {
            this.new_boq_items = [];
            this.can_save = true;
            this.boq_map = {
                name: "",
                measurement_unit: "",
                value_per_unit: "",
                objects: {}
            };
            this.appendObjectsToTemplate();
            this.initContentPreviewTable();
            if (this.imported_content_table) {
                var selected_objects = this.imported_content_table.querySelectorAll('.imported-content-table-column');
                for (var index = 0; index < selected_objects.length; index++) {
                    var td = selected_objects[index];
                    td.classList.remove('imported-content-table-column');
                    td.classList.remove('imported-content-table-first-row');
                    td.classList.remove('imported-content-table-last-row');
                }
            }
        },
        deleteImportedContent: function () {
            this.text = "";
            this.resetContentPreview();
        },
        objectClassCheck: function (object_id, item_name) {
            if (this.boq_map.objects.hasOwnProperty(object_id)) {
                var object = this.boq_map.objects[object_id];
                if (object[item_name] !== "" && !isNaN(object[item_name])) {
                    return true;
                }
            }
            return false;
        },
        checkCanSave() {
            var show_save_btn = true;
            for (var item_name in this.boq_map) {
                var item = this.boq_map[item_name];
                if (item_name === "objects") {
                    if (Object.keys(item).length === 0) {
                        show_save_btn = false;
                        break;
                    }
                    for (var leaf_object_index in this.bill_of_quantity.leaf_objects) {
                        var leaf_object = this.bill_of_quantity.leaf_objects[leaf_object_index];
                        if (item.hasOwnProperty(leaf_object.id)) {
                            var object = item[leaf_object.id];
                            if (this.bill_of_quantity.has_object_value_per_unit && !object.hasOwnProperty('object_value_per_unit')) {
                                show_save_btn = false;
                                break;
                            }
                            if (!object.hasOwnProperty('quantity') || !object.hasOwnProperty('value')) {
                                show_save_btn = false;
                                break;
                            }
                        }
                        else {
                            show_save_btn = false;
                            break;
                        }
                    }
                }
                else if (item === "") {
                    show_save_btn = false;
                    break;
                }

            }
            this.show_save_btn = show_save_btn;
        },
        isInObj: function (obj, value, return_prop_name = false) {
            for (var key in obj) 
            {
                var prop = obj[key];
                if (typeof prop === 'object') 
                {
                    var result = this.isInObj(prop, value);
                    if (result) 
                    {
                        return true;
                    }
                }

                if (prop === parseInt(value)) 
                {
                    if (return_prop_name) 
                    {
                        return key;
                    }
                    return true;
                }
            }
            return false;
        },
        searchObj: function (obj, value, by_key = false) {
            for (var key in obj) 
            {
                var prop = obj[key];
                if (by_key) 
                {
                    if (key === value) 
                    {
                        return prop;
                    }
                }

                if (typeof prop === 'object') 
                {
                    if (by_key) 
                    {
                        return this.searchObj(prop, value, true);
                    }
                    else 
                    {
                        var result = {
                            prop_name: key,
                            child_result: this.searchObj(prop, value)
                        };
                        if (result.child_result.prop_name) 
                        {
                            return result;
                        }
                    }
                }

                if (prop === parseInt(value) && !by_key) 
                {
                    return {
                        prop_name: key,
                        child_result: false
                    };
                }
            }
            if (by_key) 
            {
                return false;
            }
            return {
                prop_name: null,
                child_result: false
            };
        }
    }
});
