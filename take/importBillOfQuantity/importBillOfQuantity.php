<script type="text/x-template" id="import-bill-of-quantity">

    <div class="sima-layout-panel _vertical _splitter import-bill-of-quantity">
        <div class="sima-layout-panel _horizontal imported-content-panel"
             data-sima-layout-init='{"proportion":0.5, "min_width":200}' style="height: 100%">
            <div class="layout-segment">
                <button v-if="editing_imported_content" @click="editing_imported_content = false; content_preview_panel_enabled=true">
                    Prikaz
                </button>
                <button v-if="editing_imported_content && text != ''" @click="deleteImportedContent" ><?=Yii::t('BuildingsModule.ImportBillOfQuantity', 'DeleteImportedContent')?></button>
                <button v-if="!editing_imported_content" @click="editing_imported_content = true; resetContentPreview()">
                    <?=Yii::t('BuildingsModule.ImportBillOfQuantity', 'Edit')?>
                </button>
            </div>
            <tinymce v-if="editing_imported_content"
                class="iboq-tinymce"
                v-bind:read_only="true"
                v-bind:tinymce_params="{paste_as_text: false, height: '100%', plugins:'table'}"
                v-model="text">
            </tinymce>
            <div v-else class="layout-segment">
                <div class="layout-segment-title"><?=Yii::t('BuildingsModule.ImportBillOfQuantity', 'Content')?>:</div>
                <div v-html="text" class="imported-content-table"></div>
            </div>
        </div>

        <div class="sima-layout-panel content-preview-panel" data-sima-layout-init='{"proportion":0.5, "min_width":200}'>
            <div class="layout-segment-title"><?=Yii::t('BuildingsModule.ImportBillOfQuantity', 'Settings')?>:</div>
            <div class="layout-segment">
                <button @click="resetContentPreview"><?=Yii::t('BuildingsModule.ImportBillOfQuantity', 'ResetContentPreview')?></button>
                <?=Yii::t('BuildingsModule.ImportBillOfQuantity', 'SelectDecimalsSeparator')?>
                <button class="btn-decimal sima-btn"
                v-bind:class="{'_pressed': ('.' === selected_decimal_char)}"
                @click="selected_decimal_char = '.'">
                    .
                </button>
                <button class="btn-decimal sima-btn"
                v-bind:class="{'_pressed': (',' === selected_decimal_char)}"
                @click="selected_decimal_char = ','"
                >
                    ,
                </button>
            </div>
            <div class="menu iboq-flex iboq-flex-row">
                <div v-bind:class="{'iboq-overlay': editing_imported_content}"></div>
                <button class="column-link-btn iboq-flex-item" @click="linkBOQItems('name')" v-bind:class="{'iboq-linked': boq_map_name_check}">
                    <?=Yii::t('BuildingsModule.BillOfQuantityItem', 'Name')?>
                </button>
                <button class="column-link-btn iboq-flex-item" @click="linkBOQItems('measurement_unit')" v-bind:class="{'iboq-linked': boq_map_measurement_unit_check}">
                    <?=Yii::t('BuildingsModule.BillOfQuantityItem', 'MeasurementUnit')?>
                </button>
                <button class="column-link-btn iboq-flex-item"
                    @click="linkBOQItems('value_per_unit')"
                    v-bind:class="{'iboq-linked': boq_map_value_per_unit_check}"
                    v-if="!bill_of_quantity.has_object_value_per_unit">
                     <?=Yii::t('BuildingsModule.BillOfQuantityItem', 'ValuePerUnit')?>
                </button>
                <div class="column-link-object-btn content-center iboq-flex-item "
                    v-if="bill_of_quantity.leaf_objects.length > 0"
                    v-for="object in bill_of_quantity.leaf_objects">
                    <div class="iboq-flex-row">
                        <div class="column-link-object-title iboq-flex-item iboq-flex-item-auto ">{{object.name}}</div>
                    </div>
                    <div class="iboq-flex-row">
                        <button class="column-link-btn column-link-object-btn-left iboq-flex-item-auto"
                            @click="linkBOQItems('object', {object_id: object.id, item_name: 'quantity'})"
                            v-bind:class="{'iboq-linked': objectClassCheck(object.id,'quantity')}">
                            <?=Yii::t('BuildingsModule.BillOfQuantityItem', 'Quantity')?>
                        </button>
                        <button class="column-link-btn column-link-object-btn iboq-flex-item-auto"
                            v-if="object.bill_of_quantity.has_object_value_per_unit"
                            @click="linkBOQItems('object', {object_id: object.id, item_name: 'object_value_per_unit'})"
                            v-bind:class="{'iboq-linked': objectClassCheck(object.id,'object_value_per_unit')}">
                            <?=Yii::t('BuildingsModule.BillOfQuantityItem', 'ValuePerUnit')?>
                        </button>
                        <button class="column-link-btn column-link-object-btn-right iboq-flex-item-auto"
                            @click="linkBOQItems('object', {object_id: object.id, item_name: 'value'})"
                            v-bind:class="{'iboq-linked': objectClassCheck(object.id,'value')}">
                            <?=Yii::t('BuildingsModule.BillOfQuantityItem', 'Value')?>
                        </button>
                    </div>
                </div>
                <br/>
            </div>

            <div class="layout-segment" >
                <button @click="saveGroupItems" v-if="show_save_btn"><?=Yii::t('BuildingsModule.ImportBillOfQuantity', 'SaveBOQ')?></button>
            </div>

            <div class="layout-segment" >
                <table class="content-preview-table">
                </table>
            </div>
        </div>
    </div>
</script>