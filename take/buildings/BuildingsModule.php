<?php

/**
 * Description of GeoModule
 *
 * @author Milos Sreckovic
 *
 * @package SIMA
 * @subpackage Buildings
 * 
 */
class BuildingsModule  extends SIMAModule 
{
    public function init()
    {        
        parent::init();
        
        // this method is called when the module is being created
        // you may place code here to customize the module or the application
        
        $this->setParams(include(dirname(__FILE__).DIRECTORY_SEPARATOR.'config'.DIRECTORY_SEPARATOR.'main.php'));

        Yii::app()->setParams(include('config/main.php'));
    }
    
    public function permanentBehaviors()
    {
        return [
            'File' => 'FileBuildingsBehavior',
            'CompanyLicenseToCompany' => 'CompanyLicenseToCompanyBehavior',
            'Theme' => 'ThemeBuildingsBehavior',
            'BuildingStructure' => 'BuildingStructureBuildingsBehavior',
            'User' => 'UserBuildingsBehavior'
        ];
    }
      
    public function getBaseUrl()
    {
//            return Yii::app()->assetManager->publish(Yii::getPathOfAlias('buildings.assets'));
    }
        
    public function registerManual()
    {
        $uniq = SIMAHtml::uniqid();
        
        $urlScript = Yii::app()->assetManager->publish(Yii::getPathOfAlias('buildings.assets'));
            
        Yii::app()->clientScript->registerScriptFile($urlScript .'/js/simaBuildings.js');
        
        $sima_buildings_script = "sima.buildings = new SIMABuildings();";
        Yii::app()->clientScript->registerScript(SIMAHtml::uniqid(),$sima_buildings_script, CClientScript::POS_READY);
        
        Yii::app()->clientScript->registerCssFile($urlScript . '/css/buildingProjects.css');
        
//        $theme_tab_bill_of_quantity_dir = dirname(__FILE__).'/vueGUI/models/Theme/TabBillOfQuantity';
//        Yii::app()->clientScript->registerScript("theme_tab_bill_of_quantity_js_$uniq", file_get_contents($theme_tab_bill_of_quantity_dir.'/TabBillOfQuantity.js'), CClientScript::POS_READY);
//        Yii::app()->clientScript->registerScript("theme_tab_bill_of_quantity_php_$uniq", include($theme_tab_bill_of_quantity_dir.'/TabBillOfQuantity.php'), CClientScript::POS_END);
//        Yii::app()->clientScript->registerCss("theme_tab_bill_of_quantity_css_$uniq", file_get_contents($theme_tab_bill_of_quantity_dir.'/TabBillOfQuantity.css'));
//        Yii::app()->clientScript->registerScript("theme_tab_bill_of_quantity_item_group_js_$uniq", file_get_contents($theme_tab_bill_of_quantity_dir.'/TabBillOfQuantityItemGroup.js'), CClientScript::POS_READY);
//        Yii::app()->clientScript->registerScript("theme_tab_bill_of_quantity_item_group_php_$uniq", include($theme_tab_bill_of_quantity_dir.'/TabBillOfQuantityItemGroup.php'), CClientScript::POS_END);
//        Yii::app()->clientScript->registerCss("theme_tab_bill_of_quantity_item_group_css_$uniq", file_get_contents($theme_tab_bill_of_quantity_dir.'/TabBillOfQuantityItemGroup.css'));
//        Yii::app()->clientScript->registerScript("theme_tab_bill_of_quantity_item_js_$uniq", file_get_contents($theme_tab_bill_of_quantity_dir.'/TabBillOfQuantityItem.js'), CClientScript::POS_READY);
//        Yii::app()->clientScript->registerScript("theme_tab_bill_of_quantity_item_php_$uniq", include($theme_tab_bill_of_quantity_dir.'/TabBillOfQuantityItem.php'), CClientScript::POS_END);
//        Yii::app()->clientScript->registerCss("theme_tab_bill_of_quantity_item_css_$uniq", file_get_contents($theme_tab_bill_of_quantity_dir.'/TabBillOfQuantityItem.css'));
//        Yii::app()->clientScript->registerScript("theme_tab_bill_of_quantity_table_header_object_js_$uniq", file_get_contents($theme_tab_bill_of_quantity_dir.'/TabBillOfQuantityTableHeaderObject.js'), CClientScript::POS_READY);
//        Yii::app()->clientScript->registerScript("theme_tab_bill_of_quantity_table_header_object_php_$uniq", include($theme_tab_bill_of_quantity_dir.'/TabBillOfQuantityTableHeaderObject.php'), CClientScript::POS_END);
//        Yii::app()->clientScript->registerCss("theme_tab_bill_of_quantity_table_header_object_css_$uniq", file_get_contents($theme_tab_bill_of_quantity_dir.'/TabBillOfQuantityTableHeaderObject.css'));
//        Yii::app()->clientScript->registerScript("theme_tab_bill_of_quantity_table_body_object_js_$uniq", file_get_contents($theme_tab_bill_of_quantity_dir.'/TabBillOfQuantityTableBodyObject.js'), CClientScript::POS_READY);
//        Yii::app()->clientScript->registerScript("theme_tab_bill_of_quantity_table_body_object_php_$uniq", include($theme_tab_bill_of_quantity_dir.'/TabBillOfQuantityTableBodyObject.php'), CClientScript::POS_END);
//        Yii::app()->clientScript->registerCss("theme_tab_bill_of_quantity_table_body_object_css_$uniq", file_get_contents($theme_tab_bill_of_quantity_dir.'/TabBillOfQuantityTableBodyObject.css'));
//        Yii::app()->clientScript->registerScript("theme_tab_bill_of_quantity_item_edit_value_js_$uniq", file_get_contents($theme_tab_bill_of_quantity_dir.'/TabBillOfQuantityItemEditValue.js'), CClientScript::POS_READY);
//        Yii::app()->clientScript->registerScript("theme_tab_bill_of_quantity_item_edit_value_php_$uniq", include($theme_tab_bill_of_quantity_dir.'/TabBillOfQuantityItemEditValue.php'), CClientScript::POS_END);
//        Yii::app()->clientScript->registerCss("theme_tab_bill_of_quantity_item_edit_value_css_$uniq", file_get_contents($theme_tab_bill_of_quantity_dir.'/TabBillOfQuantityItemEditValue.css'));
        
//        $theme_tab_building_site_diaries_dir = dirname(__FILE__).'/vueGUI/models/Theme/TabBuildingSiteDiaries';
//        Yii::app()->clientScript->registerScript("theme_tab_building_site_diaries_js_$uniq", file_get_contents($theme_tab_building_site_diaries_dir.'/TabBuildingSiteDiaries.js'), CClientScript::POS_READY);
//        Yii::app()->clientScript->registerScript("theme_tab_building_site_diaries_php_$uniq", include($theme_tab_building_site_diaries_dir.'/TabBuildingSiteDiaries.php'), CClientScript::POS_END);
//        Yii::app()->clientScript->registerCss("theme_tab_building_site_diaries_css_$uniq", file_get_contents($theme_tab_building_site_diaries_dir.'/TabBuildingSiteDiaries.css'));
//        Yii::app()->clientScript->registerScript("theme_tab_building_site_diary_js_$uniq", file_get_contents($theme_tab_building_site_diaries_dir.'/TabBuildingSiteDiary.js'), CClientScript::POS_READY);
//        Yii::app()->clientScript->registerScript("theme_tab_building_site_diary_php_$uniq", include($theme_tab_building_site_diaries_dir.'/TabBuildingSiteDiary.php'), CClientScript::POS_END);
//        Yii::app()->clientScript->registerCss("theme_tab_building_site_diary_css_$uniq", file_get_contents($theme_tab_building_site_diaries_dir.'/TabBuildingSiteDiary.css'));
//        Yii::app()->clientScript->registerScript("theme_tab_building_site_diary_work_hour_worker_js_$uniq", file_get_contents($theme_tab_building_site_diaries_dir.'/TabBuildingSiteDiaryWorkHourWorker.js'), CClientScript::POS_READY);
//        Yii::app()->clientScript->registerScript("theme_tab_building_site_diary_work_hour_worker_php_$uniq", include($theme_tab_building_site_diaries_dir.'/TabBuildingSiteDiaryWorkHourWorker.php'), CClientScript::POS_END);
//        Yii::app()->clientScript->registerCss("theme_tab_building_site_diary_work_hour_worker_css_$uniq", file_get_contents($theme_tab_building_site_diaries_dir.'/TabBuildingSiteDiaryWorkHourWorker.css'));
    }
    
    public function getGuiTableDefinition($index)
    {
        switch($index)
        {   
            case '0': return [
                    'label' => 'Vrste gradjevinskih projekata',
                    'settings' => array(
                        'model' => BuildingProjectKind::model(),
                        'add_button' => true
                    )
                ];
            case '1': return [
                'label' => Yii::t('BaseModule.GuiTable', 'BuildingProjectTypes'),
                'settings' => array(
                    'model' => BuildingProjectType::model(),
                    'add_button' => true
                )
            ];
            default:
                throw new SIMAGuiTableIndexNotFound('buildings', $index);
        }
    }
}

?>
