<?php $uniq = SIMAHtml::uniqid(); ?>

<buildings-theme_tab-bill_of_quantity 
    id='<?=$uniq?>' 
    v-bind:bill_of_quantity="bill_of_quantity"
>
</buildings-theme_tab-bill_of_quantity>

<script type='text/javascript'>

    new Vue({
        el: '#<?=$uniq?>',
        data: {
            bill_of_quantity: sima.vue.shot.getters.model('<?=SIMAMisc::getVueModelTag($bill_of_quantity)?>')
        }
    });

</script>