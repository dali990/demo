<page size="A4">
    <div class="building-interim-bill-report">
        <div class="border-wrapper <?=!$exporting_pdf ? 'set-max-width set-max-height html-border-wrapper' : 'pdf-border-wrapper'?>"></div>
        <header class="<?=$exporting_pdf ? 'set-pdf-width' : ''?>">
            <img src='<?=$curr_company->getAttributeDisplay('image_id')?>' />
            <div class="content">
                <ul class="company-info">
                    <li><?= $curr_company->DisplayName ?></li>
                    <li>
                        <?= !empty($curr_company->partner) ? $curr_company->partner->DisplayMainAddress : '' ?> / 
                        <?=Yii::t('BuildingsModule.BuildingInterimBill', 'PhoneNumber')?>:
                        <?= !empty($curr_company->partner->MainPhoneNumber) ? $curr_company->partner->MainPhoneNumber->display_name : '' ?> 
                    </li>
                    <li><?=Yii::t('BuildingsModule.BuildingInterimBill', 'Email')?>: <?= !empty($curr_company->partner) ? $curr_company->partner->MainEmailAddressDisplay : '' ?></li>
                    <li>
                        <?=Yii::t('Company','PIB')?>: <?= $curr_company->PIB ?>;
                        <?=Yii::t('Company','MB')?>: <?= $curr_company->MB ?>
                    </li>
                    <li><?=Yii::t('BuildingsModule.BuildingInterimBill', 'BusinessAccount')?>: <?= !empty($curr_company->partner) ? $curr_company->partner->MainBankAccount->number : '' ?></li>
                </ul>
            </div>
        </header>
    </div>
</page>
