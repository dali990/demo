<page size="A4">
    <div class="building-interim-bill-report">     
        <div class="main <?=!$exporting_pdf ? 'main-html-border' : ''?>">
            <table class="content-table">
                <tr>
                    <td valign="top">
                        <?php if($bill) : ?>
                            <div class="bill-info" >
                                <?=Yii::t('BuildingsModule.BuildingInterimBill', 'PlaceAndDateOfInvoicing')?>: <br>
                                <?=$bill->issue_location ? $bill->issue_location.', ' : ''?>
                                <?=$bill->income_date?><br> <!-- Datum izdavanja/Datum racuna -->
                                <?=Yii::t('BuildingsModule.BuildingInterimBill', 'DateOfservicesSupply')?>: <?=$bill->traffic_date?><br>
                                <?=Yii::t('BuildingsModule.BuildingInterimBill', 'CurrencyPayment')?>: <?=$bill->payment_date?>
                            </div>
                        <?php endif ?>
                    </td>
                    <td>
                        <div class="investor-info">
                            <ul>
                                <li><?= $building_interim_bill->building_construction->investor->DisplayName ?></li>
                                <li>
                                    <?= $building_interim_bill->building_construction->investor->DisplayMainAddress?>
                                </li>
                                <li><?=Yii::t('BuildingsModule.BuildingInterimBill', 'Email')?>: <?= $building_interim_bill->building_construction->investor->MainEmailAddressDisplay?></li>
                                <li>
                                    <?=Yii::t('Company','PIB')?>: <?= isset($building_interim_bill->building_construction->investor->company_model) ? $building_interim_bill->building_construction->investor->company_model->PIB : '' ?>;
                                    <?=Yii::t('Company','MB')?>: <?= isset($building_interim_bill->building_construction->investor->company_model) ? $building_interim_bill->building_construction->investor->company_model->MB : '' ?>
                                </li>
                                <li><?=Yii::t('BuildingsModule.BuildingInterimBill', 'BusinessAccount')?>: <?= isset($building_interim_bill->building_construction->investor->MainBankAccount) ? $building_interim_bill->building_construction->investor->MainBankAccount->number : ''?></li>
                            </ul>
                        </div>
                    </td>
                </tr>
            </table>
            
            
            <div class="title" <?=$exporting_pdf ? 'style="width:189mm"' : ''?>>
                <h1><?=$building_interim_bill->file->name.' '.Yii::t('BuildingsModule.BuildingInterimBill', 'Number').': ' . (!empty($building_interim_bill->file->bill) ? " {$building_interim_bill->file->bill->bill_number}" : '')?></h1>
            </div>
            <div class="small-title">
                <h2><?=Yii::t('BuildingsModule.BuildingInterimBill', 'ToExecuteConstructionWork')?> : <?=$building_interim_bill->building_construction->bill_of_quantity->name?></h2>
            </div>
            <div class="content">
                <?php if(!empty($building_interim_bill->building_construction->theme->job->base_document)) { ?>
                    <div class="small-title">
                        <h2><?=Yii::t('BuildingsModule.BuildingInterimBill', 'For')?> <?=$building_interim_bill->building_construction->theme->job->base_document->DisplayName?> </h2>
                    </div>
                <?php } else if (!empty($building_interim_bill->file->bill->bill_out->invoicing_basis)) { ?>
                    <div class="small-title">
                        <h2><?=Yii::t('BuildingsModule.BuildingInterimBill', 'AccordingToTheConstructionContract')?> <?=$building_interim_bill->file->bill->bill_out->invoicing_basis?> </h2>
                    </div>
                <?php } ?>
                <div class="content-item">
                    <span class="_left">
                        <?=Yii::t('BuildingsModule.BuildingInterimBill', 'TotalContractedWorksWithoutVAT')?>:
                    </span>
                    <span class="_right">
                        <?php 
                            if(!empty($building_interim_bill->building_construction->theme->job)) 
                            {
                                $boq = $building_interim_bill->building_construction->bill_of_quantity;
                                echo BillOfQuantityItemGroupToObject::get($boq->root_group, $boq->root_object)->value->round(2)->toString();
                            }
                        ?>
                    </span>
                </div>
                <div class="content-item">
                    <span class="_left">
                        <?=Yii::t('BuildingsModule.BuildingInterimBill', 'PrepaymentOfTheContractValue')?>:
                    </span>
                    <span class="_right">
                        <?php 
                            if(!empty($building_interim_bill->building_construction->theme->job)) 
                            {
                                echo SIMAHtml::number_format($building_interim_bill->building_construction->theme->job->advance);
                            }
                        ?>
                    </span>
                </div>
                <div class="content-item">
                    <span class="_left">
                        <?=Yii::t('BuildingsModule.BuildingInterimBill', 'WorksCompletedWithoutVAT')?>:
                    </span>
                    <span class="_right">
                        <?=$building_interim_bill->getTotalValue()?>
                    </span>
                </div>
                <div class="content-item">
                    <span class="_left">
                        <?=Yii::t('BuildingsModule.BuildingInterimBill', 'InvoicedAPreviousSituationsWithoutVAT')?>:
                    </span>
                    <span class="_right">
                        <?=$building_interim_bill->getPreValue()?>
                    </span>
                </div>
                <div class="content-item">
                    <span class="_left">
                        <?=Yii::t('BuildingsModule.BuildingInterimBill', 'WorksPerformedOnThisSituation')?>:
                    </span>
                    <span class="_right">
                        <?=$building_interim_bill->getValue()?>
                    </span>
                </div>
                <div class="content-item">
                    <span class="_left">
                        <?=Yii::t('BuildingsModule.BuildingInterimBill', 'AdvanceWithoutVAT')?>:
                    </span>
                    <span class="_right">
                        <?=SIMAHtml::number_format($building_interim_bill->getBillAdvanceValue())?>
                    </span>
                </div>
                <div class="content-item">
                    <span class="_left">
                        <?=Yii::t('BuildingsModule.BuildingInterimBill', 'TotalForThisSituation')?>:
                    </span>
                    <span class="_right">
                        <?=$building_interim_bill->getValueForPay()?>
                    </span>
                </div>
                
                <p class="content-item" style="font-size:3mm"><b><?=Yii::t('BuildingsModule.BuildingInterimBill', 'InLetters')?>: </b><?=SIMAHtml::money2letters($building_interim_bill->getValueForPay()->asFloat(), 'lat')?></p>
                <br>
                <p class="content-item">
                    <b><?=Yii::t('BuildingsModule.BuildingInterimBill', 'TaxExemptionNote')?>: </b> 
                    <?php if(!empty($building_interim_bill->file->bill) && $building_interim_bill->file->bill->note_of_tax_exemption) : ?>
                        <?=$building_interim_bill->file->bill->note_of_tax_exemption->text_of_note?>
                    <?php endif ?>
                </p>
                
                <p class="content-item"><b><?=Yii::t('BuildingsModule.BuildingInterimBill', 'TermsOfPayment')?>: </b> <?=Yii::t('BuildingsModule.BuildingInterimBill', 'Within15Days')?></p>
                
                <div class="small-title">
                    <h2>
                        <?=Yii::t('BuildingsModule.BuildingInterimBill', 'MakePaymentToAccount')?>: <?=$curr_company->partner->MainBankAccount->number?> 
                        <?=Yii::t('BuildingsModule.BuildingInterimBill', 'AtThekod')?> 
                        <?=$curr_company->partner->MainBankAccount->bank->DisplayName?> 
                        <?=Yii::t('BuildingsModule.BuildingInterimBill', 'WithReferenceNumber')?> : <?=!empty($building_interim_bill->file->bill) ? $building_interim_bill->file->bill->call_for_number : ''?>
                    </h2>
                </div>

                <br />
                
                <div class="avoid-break">
                    <div class="content-item">
                        <div class="_left">
                            <?=Yii::t('BuildingsModule.BuildingInterimBill', 'Contractor')?> <br /><br />
                            ___________________________<br>
                            <?=$building_interim_bill->building_construction->constructor->DisplayName?> <!-- responsible_engineer -->
                        </div>
                        <div class="_right">
                            <?=Yii::t('BuildingsModule.BuildingInterimBill', 'ProfessionalSupervision')?> <br /><br />
                            ___________________________
                        </div>
                    </div>

                    <div class="content-item">
                        <div class="_left"></div>
                        <div class="_right">
                            <?=Yii::t('BuildingsModule.BuildingInterimBill', 'Investor')?> <br /><br />
                            ___________________________<br>
                            <?=$building_interim_bill->building_construction->investor->DisplayName?>                           
                        </div>
                    </div>
                </div>
            </div>
            
            <!-- -------------------------------------------------------------- -->
            <!-- Mesto prikaza predmera (za debagovanje i prikaz predmera)-->
            <?php //if(!$exporting_pdf): ?>
            <!--<div class="content" style="margin-left: -11mm; margin-right: -11mm;">
                    <br><br>
                    <?php //echo $bill_of_quantity_report?>
                </div>-->
            <?php //endif ?>
            
            <!-- -------------------------------------------------------------- -->
            <!-- ZBIRNA REKAPITULACIJA -->
            <div style="page-break-after: always"></div>
            <div style="width:189mm; height: 10mm"></div>
            <div class="summary-title title" <?=$exporting_pdf ? 'style="width:189mm"' : ''?> >
                <h1><?=Yii::t('BuildingsModule.BuildingInterimBill', 'SummaryRecapitulationOfWork')?></h1>
                <b><?=$building_interim_bill->building_construction->bill_of_quantity->name?></b>
            </div>
            <div class="content">
                <table class="summary-table">
                    <?php if($bill_of_quantity->leaf_objects_count > 1): ?>
                        <tr>
                            <td colspan="2"></td>
                            <?php foreach($bill_of_quantity->leaf_objects as $leaf_object): ?>
                                <td ><?=$leaf_object->name?></td>
                            <?php endforeach ?>
                        </tr>
                    <?php endif ?>
                    <tr>
                        <td class="no"><?=Yii::t('BuildingsModule.BuildingInterimBill', 'No')?></td>
                        <td><?=Yii::t('BuildingsModule.BuildingInterimBill', 'PositionDescription')?></td>
                        <?php foreach($bill_of_quantity->leaf_objects as $leaf_object): ?>
                            <td class="value"><?=Yii::t('BuildingsModule.BuildingInterimBill', 'Value')?></td>
                        <?php endforeach ?>
                    </tr>
                    <?php $i=0; foreach($item_group_values as $item_group_value): $i++?>
                        <tr>
                            <td><?=$item_group_value['order']?></td>
                            <td class="item-group-tile text-left"><?=$item_group_value['title']?></td>
                            <?php foreach($item_group_value['objects'] as $object): ?>
                                <td><?=$object['total_value_done']?></td>
                            <?php endforeach ?>
                        </tr>
                    <?php endforeach ?>
                    <tr>
                        <td colspan="2"><b><?=Yii::t('BuildingsModule.BuildingInterimBill', 'Total')?></b></td>                       
                        <?php foreach($root_group_values['objects'] as $object): ?>
                            <td><b><?=$object['total_value_done']?></b></td>
                        <?php endforeach ?>
                    </tr>
                </table>
            </div>
            
            <!-- -------------------------------------------------------------- -->
            <!-- PREGLED ISPOSTAVLJENIH SITUACIJA -->
            <div style="page-break-after: always"></div>
            <div style="width:189mm; height: 10mm"></div>
            <div class="title" <?=$exporting_pdf ? 'style="width:189mm"' : ''?> >
                <h1><?=Yii::t('BuildingsModule.BuildingInterimBill', 'OverviewOfSituations')?></h1>
            </div>
            <div class="content">
                <table class="situations-overview">
                    <tr>
                        <td class="row-space">
                            1.
                        </td>
                        <td>
                            <?=Yii::t('BuildingsModule.BuildingInterimBill', 'AllTheWorkDone')?> 
                            <small style="padding-left:2mm"><?=$bill? $bill->income_date : '' ?></small>
                        </td>
                        <td></td>
                        <td></td>
                        <td>
                            <b><?=$building_interim_bill->getTotalValue()?></b>
                        </td>
                    </tr>
                    <tr>
                        <td class="row-space">
                            2.
                        </td>
                        <td colspan="3">
                            <?=Yii::t('BuildingsModule.BuildingInterimBill', 'WfWhichCalculatedOfPreviousSituations')?>:
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td align="center"><?=Yii::t('BuildingsModule.BuildingInterimBill', 'SituationNumber')?></td>
                        <td><?=Yii::t('BuildingsModule.BuildingInterimBill', 'Issuing')?></td>
                        <td><?=Yii::t('BuildingsModule.BuildingInterimBill', 'ValueOfTheSituation')?></td>
                    </tr>
                    <?php foreach($building_interim_bill->prevBuildingInterimBillsByDate($building_interim_bill->issue_date) as $bib): ?>
                        <tr>
                            <td></td>
                            <td></td>
                            <td align="center"><?= isset($bib->file->bill) ? $bib->file->bill->bill_number : $bib->order_number ?></td>
                            <td><?=$bib->issue_date?></td>
                            <td><?=$bib->getValue()?></td>
                        </tr>
                    <?php endforeach ?>
                    <tr>
                        <td></td>
                        <td colspan="3"><?=Yii::t('BuildingsModule.BuildingInterimBill', 'AllDoneAccordingToPreviousSituations')?></td>
                        <td><b><?=$building_interim_bill->getPreValue()?></b></td>
                    </tr>
                    <tr>
                        <td class="row-space">
                            3.
                        </td>
                        <td>
                            <?=Yii::t('BuildingsModule.BuildingInterimBill', 'CxecutedUponThisSituation')?> 
                        </td>
                        <td></td>
                        <td></td>
                        <td>
                            <b><?=$building_interim_bill->getValue()?></b>
                        </td>
                    </tr>
                    <tr>
                        <td class="row-space">
                            4.
                        </td>
                        <td>
                            <?=Yii::t('BuildingsModule.BuildingInterimBill', 'TheAmountOfTheDeductionOnAdvances')?> 
                        </td>
                        <td></td>
                        <td></td>
                        <td>
                            <b><?=$building_interim_bill->getBillAdvanceValue()?></b>
                        </td>
                    </tr>
                    <tr>
                        <td class="row-space">
                            5.
                        </td>
                        <td>
                            <?=Yii::t('BuildingsModule.BuildingInterimBill', 'TheAmountToPayOutInThisSituation')?> 
                        </td>
                        <td></td>
                        <td></td>
                        <td>
                            <b><?=$building_interim_bill->getValueForPay()?></b>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="content">
                <br />
                
                <p class="content-item"><?=Yii::t('BuildingsModule.BuildingInterimBill', 'WorkPriceLikeInContractClaimedBy')?>:</p>

                <br />
                
                <div class="avoid-break">
                    <div class="content-item">
                        <div class="_left">
                            <?=Yii::t('BuildingsModule.BuildingInterimBill', 'ResponsibleContractor')?> <br /><br />
                            ___________________________<br>
                            <?=$building_interim_bill->building_construction->constructor->DisplayName?> <!-- responsible_engineer -->
                        </div>
                        <div class="_right">
                            <?=Yii::t('BuildingsModule.BuildingInterimBill', 'ProfessionalSupervision')?> <br /><br />
                            ___________________________
                        </div>
                    </div>

                    <div class="content-item">
                        <div class="_left"></div>
                        <div class="_right">
                            <?=Yii::t('BuildingsModule.BuildingInterimBill', 'Investor')?> <br /><br />
                            ___________________________<br>
                            <?=$building_interim_bill->building_construction->investor->DisplayName?>                           
                        </div>
                    </div>
                </div>
            </div>
        </div> 

    </div>
</page>