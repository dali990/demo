<page size="A4">
    <div class="bill-of-quantity-report">
        <header>
            <table class="boq-table boq-table-header">   
                <!-- Ispis kolona: Pozicija, jed. mere, nizivi objekata (PRVI RED)  -->
                <tr>
                    <?php foreach($boq_table_header as $column_key => $column): ?>
                        
                        <?php if($column_key !== 'object_columns'): ?>
                            <td rowspan="2" class="<?=$column['css_class']?>" ><?=$column['label']?></td>
                        <?php else: ?>                           
                            <!-- ako ima vise objekata onda prikazuje nazive -->
                            <?php if($leaf_objects_count > 1): ?>
                                <?php foreach($column['objects'] as $object): ?>
                                    <td colspan="<?=$object_colspan?>" style="width: <?=$column['width']?>mm"><?=$object->name?></td>
                                <?php endforeach;?>                                   
                            <!-- ako ima samo 1 objekat -->
                            <?php else: ?>
                                <td style="width: <?=$column['width']/$object_colspan?>mm" rowspan="2">
                                    <?=Yii::t('BuildingsModule.BillOfQuantityItem', 'Quantity')?>
                                </td>
                                <?php //if($has_object_value_per_unit): ?>
                                    <td style="width: <?=$column['width']/$object_colspan?>mm" rowspan="2"
                                        class="<?=$exporting_pdf ? 'value-per-unit-pdf' : 'value-per-unit'?>">
                                        <?=Yii::t('BuildingsModule.BillOfQuantityItem', 'ValuePerUnit')?>
                                    </td>
                                <?php //endif ?>
                                <td style="width: <?=$column['width']/$object_colspan?>mm" rowspan="2">
                                    <?=Yii::t('BuildingsModule.BillOfQuantityItem', 'Value')?>
                                </td>
                                <!-- measurement_columns -->
                                <td colspan="2" class="measurement-column"><?=Yii::t('BuildingsModule.BuildingInterimBill', 'TotalDone')?></td>
                                <td colspan="2" class="measurement-column"><?=Yii::t('BuildingsModule.BuildingInterimBill', 'PreDone')?></td>
                                <td colspan="2" class="measurement-column"><?=Yii::t('BuildingsModule.BuildingInterimBill', 'CurrentDone')?></td>
                            <?php endif; ?><!-- if($leaf_objects_count > 1) -->
                                
                        <?php endif; ?><!-- if($column_key !== 'object_columns') -->
                                
                    <?php endforeach; ?><!-- $boq_table_header -->
                </tr>
                <!-- Ispis kolona objekata: kolicina, jed. cena (ako postoji na nivou objekta), iznos  (DRUGI RED)-->
                <tr>
                    <?php if($leaf_objects_count > 1): ?>
                        <?php foreach($boq_table_header['object_columns']['objects'] as $object): ?>
                            <td style="width: <?=$boq_table_header['object_columns']['width']/$object_colspan?>mm">
                                <?=Yii::t('BuildingsModule.BillOfQuantityItem', 'Quantity')?>
                            </td>
                            <?php if($has_object_value_per_unit): ?>
                                <td class="object-value-per-unit" style="width: <?=$boq_table_header['object_columns']['width']/$object_colspan?>mm; text-align:center">
                                    <?=Yii::t('BuildingsModule.BillOfQuantityItem', 'ValuePerUnit')?>
                                </td>
                            <?php endif ?>
                            <td style="width: <?=$boq_table_header['object_columns']['width']/$object_colspan?>mm">
                                <?=Yii::t('BuildingsModule.BillOfQuantityItem', 'Value')?>
                            </td>
                        <?php endforeach; ?>
                    <?php else: ?>
                        <td class="measurement-quantity"><?=Yii::t('BuildingsModule.BuildingInterimBill', 'Quantity')?></td>
                        <td class="measurement-value"><?=Yii::t('BuildingsModule.BuildingInterimBill', 'Total')?></td>
                        <td class="measurement-quantity"><?=Yii::t('BuildingsModule.BuildingInterimBill', 'Quantity')?></td>
                        <td class="measurement-value"><?=Yii::t('BuildingsModule.BuildingInterimBill', 'Total')?></td>
                        <td class="measurement-quantity"><?=Yii::t('BuildingsModule.BuildingInterimBill', 'Quantity')?></td>
                        <td class="measurement-value"><?=Yii::t('BuildingsModule.BuildingInterimBill', 'Total')?></td>
                    <?php endif; ?>
                </tr>
            </table>           
        </header> 
    </div>
</page>