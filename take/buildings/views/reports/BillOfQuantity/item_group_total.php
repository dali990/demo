<tr>
    <td class="item-name <?=$exporting_pdf ? 'item-name-pdf' : ''?> <?=$last_row ? 'text-center': '' ?>" style="padding-left: <?=$indent_item?>mm">
        <b><?=Yii::t('BuildingsModule.BillOfQuantityItemGroup', 'Total')?></b>
    </td>
    <td class="measurement-unit-name">
        <b><?=$group_item['group_value']['measurement_unit_name']?></b>
    </td>
    <!-- razmestanje kolone jed. cene ako ima samo jedan objekat -->
    <?php if($leaf_objects_count === 1): ?>
        <?php foreach ($group_item['group_value']['objects'] as $object): ?>
            <td class="quantity">
                <b><?=$object['quantity']?></b>
            </td>
            <td class="value-per-unit">
                <b><?=$object['value_per_unit']?></b>
            </td>
            <td class="value">
                <b><?=$object['value']?></b>
            </td>
            <!-- Kumulativno izvedeno -->
            <td class="measurement-quantity"><?=$object['measurement_sheet']['total_quantity_done']?></td>
            <td class="measurement-value"><b><?=$object['measurement_sheet']['total_value_done']?></b></td>
            <!-- Izvedeno po predhodnim -->
            <td class="measurement-quantity"><?=$object['measurement_sheet']['pre_quantity_done']?></td>
            <td class="measurement-value"><b><?=$object['measurement_sheet']['pre_value_done']?></b></td>
            <!-- Izvedeno po ovoj situaciji -->
            <td class="measurement-quantity"><?=$object['measurement_sheet']['quantity_done']?></td>
            <td class="measurement-value"><b><?=$object['measurement_sheet']['value_done']?></b></td>
        <?php endforeach; ?>
    <?php else: ?>
        <?php if(!$has_object_value_per_unit): ?>
            <td class="value-per-unit">
                <b><?=$group_item['group_value']['value_per_unit']?></b>
            </td>
        <?php endif ?>
        <?php foreach ($group_item['group_value']['objects'] as $object): ?>
            <td class="quantity">
                <b><?=$object['quantity']?></b>
            </td>
            <?php if($has_object_value_per_unit): ?>
                <td class="object-value-per-unit">
                    <b><?=$object['value_per_unit']?></b>
                </td>
            <?php endif ?>
            <td class="value">
                <b><?=$object['value']?></b>
            </td>
        <?php endforeach; ?>
    <?php endif ?>
</tr>