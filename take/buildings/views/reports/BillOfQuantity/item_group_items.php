<?php foreach ($group_item['items'] as $item): ?>
    <tr>
        <td class="item-name <?=$exporting_pdf ? 'item-name-pdf' : ''?>" style="padding-left: <?=$indent_item?>mm"><?=$item['name']?></td>  
        <td class="measurement-unit-name "><?=$item['measurement_unit_name']?></td>
        <!-- razmestanje kolone jed. cene ako ima samo jedan objekat -->
        <?php if($leaf_objects_count === 1): ?>
            <?php foreach($item['objects'] as $object): ?>
                <td class="quantity " style="width: <?=$object_columns_width/$object_colspan?>mm">
                    <?=$object['quantity']?>
                </td>
                <td class="value-per-unit " style="width: <?=$object_columns_width/$object_colspan?>mm">
                    <?=$object['value_per_unit']?>
                </td>
                <td class="value " style="width: <?=$object_columns_width/$object_colspan?>mm">
                    <?=$object['value']?>
                </td>
                <!-- Kumulativno izvedeno -->
                <td class="measurement-quantity"><?=$object['measurement_sheet']['total_quantity_done']?></td>
                <td class="measurement-value"><?=$object['measurement_sheet']['total_value_done']?></td>
                <!-- Izvedeno po predhodnim -->
                <td class="measurement-quantity"><?=$object['measurement_sheet']['pre_quantity_done']?></td>
                <td class="measurement-value"><?=$object['measurement_sheet']['pre_value_done']?></td>
                <!-- Izvedeno po ovoj situaciji -->
                <td class="measurement-quantity"><?=$object['measurement_sheet']['quantity_done']?></td>
                <td class="measurement-value"><?=$object['measurement_sheet']['value_done']?></td>
            <?php endforeach; ?>
        <?php else: ?>
            <?php if($has_object_value_per_unit === false): ?>
                <td class="value-per-unit "><?=$item['value_per_unit']?></td>
            <?php endif ?>
            <?php foreach($item['objects'] as $object): ?>
                <td class="quantity " style="width: <?=$object_columns_width/$object_colspan?>mm">
                    <?=$object['quantity']?>
                </td>
                <?php if($has_object_value_per_unit): ?>
                    <td class="object-value-per-unit " style="width: <?=$object_columns_width/$object_colspan?>mm">
                        <?=$object['value_per_unit']?>
                    </td>
                <?php endif ?>
                <td class="value " style="width: <?=$object_columns_width/$object_colspan?>mm">
                    <?=$object['value']?>
                </td>
            <?php endforeach; ?>
        <?php endif ?>     
    </tr>
<?php endforeach; ?>