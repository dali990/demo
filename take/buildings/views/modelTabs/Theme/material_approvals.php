<?php
$uniq = SIMAHtml::uniqid();

$this->widget('SIMAGuiTable', [
    'id'=>"construction_material_approvals$uniq",
    'model'=> ConstructionMaterialApproval::model(),
    'columns_type' => 'inTheme',
    'add_button' => [
        'init_data'=>[
            'ConstructionMaterialApproval'=>[
                'theme_id'=>[
                    'disabled',
                    'init'=>$model->id
                ]
            ]
        ]
    ],
    'fixed_filter'=>[
        'theme' => ['ids'=>$model->id]
    ]
]);