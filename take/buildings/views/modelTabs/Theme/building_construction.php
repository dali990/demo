<?php $uniq = SIMAHtml::uniqid(); ?>

<buildings-theme_tab-bill_of_quantity 
    id='<?=$uniq?>' 
    v-bind:bill_of_quantity="bill_of_quantity"
    v-bind:building_construction="building_construction"
>
</buildings-theme_tab-bill_of_quantity>

<script type='text/javascript'>

    new Vue({
        el: '#<?=$uniq?>',
        data: {
            bill_of_quantity: sima.vue.shot.getters.model('<?=SIMAMisc::getVueModelTag($model->building_construction->bill_of_quantity)?>'),
            building_construction: sima.vue.shot.getters.model('<?=SIMAMisc::getVueModelTag($model->building_construction)?>')
        }
    });

</script>