<?php $this->widget('SIMAGuiTable',[
    'model' => 'BuildingProject',
    'columns_type' => 'inTheme',
    'fixed_filter' => [
        'theme' => ['ids' => $model->id],
        'filter_scopes' => ['baseProjects']
    ],
    'add_button' => $add_button
])?>