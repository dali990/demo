
<?php $uniq = SIMAHtml::uniqid();?>
<div id='panel_<?=$uniq?>' class="sima-layout-panel _splitter _vertical sima-ui-building-projects" >

    <div class="sima-layout-panel building_project_tree" data-sima-layout-init='{"proportion":"0.3","min_width":"300","max_width":"800"}'>
        <?php $this->widget('SIMAGuiTable', [
//            'id' => 
            'model' => BuildingProject::model()
        ]);?>
    </div>

    <div class="sima-layout-panel panel_building_project_info">
         <?php 
            $tab_id= SIMAHtml::uniqid();
            $this->widget('ext.SIMATabs.SIMATabs', array(
                'id'=>$tab_id,
                'list_tabs_model_name' => 'BuildingProject',
                'default_selected'=>'info'
            ));
          ?>
    </div>

</div>

<?php if (Yii::app()->request->isAjaxRequest) Yii::app()->clientScript->clearScriptFiles();
?>
<script type='text/javascript'>
    
    $(function(){
        var wrap=$("#panel_<?php echo $uniq?>"); 
        wrap.on('selectLeaf', function(event, data){
            wrap.find('.sima-ui-tabs').simaTabs('set_list_tabs_params',{
                         list_tabs_get_params:{id:data.id}
                    });
            }); 
    });
    
</script>    

