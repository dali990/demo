<?php $basic_info_panel_id = SIMAHtml::uniqid();?>

<div class="sima-layout-panel _splitter _vertical">
    <div class="sima-layout-panel _horizontal" data-sima-layout-init='{"proportion":0.2,"min_width":400}'>
        
        <div style="height: 40px;" class="sima-layout-fixed-panel">
            opcije
        </div>
    <?php 
        $tree_id = SIMAHtml::uniqid().'_fb_tree';
        $tree_init_params = [
            'id' => $tree_id,
//            'class' => 'sima-vm',
    //        'load_on_init' => false,
            'list_tree_action'=>'buildings/buildingProject/listContent',
            'list_tree_params' => [
                'id' => $model->id
            ]
        ];
        $this->widget('SIMATree2', $tree_init_params);
    ?>

    </div>
    <div class="sima-layout-panel _horizontal">
        <div id="<?=$basic_info_panel_id?>" class="sima-layout-fixed-panel">

        </div>
        <div class="sima-layout-panel">
            <?php 
                $gui_table_id = SIMAHtml::uniqid();
                $this->widget('SIMAGuiTable',[
                    'id' => $gui_table_id,
                    'model' => File::model(),
                    'columns_type' => 'building_project_content',
                    'fixed_filter' => [
                        'ids' => -1
                    ]
                ]);
            ?>
        </div>
    </div>
</div>

<script type="text/javascript">
    $("#<?=$tree_id?>").on('selectItem', function(event, item){            
        var project_id = item.data('project_id');
        sima.ajax.get('base/model/view',{
            get_params: {
                model: 'BuildingProject',
                model_id: project_id,
                model_view: 'info_in_content'
            },
            success_function: function(response){
                var basic_panel = $('#<?=$basic_info_panel_id?>');
                basic_panel.html(response.html);
                basic_panel.parent().parent().trigger('sima-layout-allign');
            }
        });
    });
    
    
    
    $("#<?=$tree_id?>").on('selectItem', function(event, item){            
        var tag_query = item.data('tag_query');
        if (typeof tag_query !== 'undefined')
        {
            var belong_tags = [{
                id: item.data('tag_id'),
                display_name: item.data('tag_display'),
                class: '_non_editable'
            }];
            var document_type_id = item.data('document_type_id');
            var file_template_id = item.data('file_template_id');
            $("#<?=$gui_table_id?>").simaGuiTable('setFixedFilter',{
                belongs: tag_query
            }).simaGuiTable('setAddButton',{
                init_data:{
                    File: {
                        file_belongs: {
                            ids: {
                                ids: belong_tags
                            }
                        }, 
                        document_type_id:{
                            disabled: (document_type_id !== null),
                            init: document_type_id
                        },
                        file_template_id:{
                            disabled: (file_template_id !== null),
                            init: file_template_id
                        },
                        responsible_id: {
                            init: item.data('responsible_id')
                        },
                        confirmed: {
                            init: true
                        }
                    }
                }
            }).simaGuiTable('populate');
        }
        else
        {
            $("#<?=$gui_table_id?>")
                    .simaGuiTable('setFixedFilter',{ids: -1})
                    .simaGuiTable('setAddButton')
                    .simaGuiTable('populate');
        }
    });
    
    
    
</script>