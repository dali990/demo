<?php $uniq = SIMAHtml::uniqid(); ?>

<buildings-theme_tab-building_construction 
    id='<?=$uniq?>' 
    v-bind:theme="theme"
    v-bind:without_bill_of_quantity_id="without_bill_of_quantity_id"
>
</buildings-theme_tab-building_construction>

<script type='text/javascript'>

    new Vue({
        el: '#<?=$uniq?>',
        data: {
            theme: sima.vue.shot.getters.model('<?=SIMAMisc::getVueModelTag($theme)?>'),
            without_bill_of_quantity_id: <?=!empty($without_bill_of_quantity_id) ? $without_bill_of_quantity_id : -1?>
        }
    });

</script>