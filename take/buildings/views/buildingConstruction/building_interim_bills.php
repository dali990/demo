<?php $uniq = SIMAHtml::uniqid(); ?>

<buildings-theme_tab-building_interim_bills 
    id='<?=$uniq?>' 
    v-bind:building_construction="building_construction"
>
</buildings-theme_tab-building_interim_bills>

<script type='text/javascript'>

    new Vue({
        el: '#<?=$uniq?>',
        data: {
            building_construction: sima.vue.shot.getters.model('<?=SIMAMisc::getVueModelTag($building_construction)?>')
        }
    });

</script>