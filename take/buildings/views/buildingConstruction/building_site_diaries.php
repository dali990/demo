<?php $uniq = SIMAHtml::uniqid(); ?>

<buildings-theme_tab-building_site_diaries 
    id='<?=$uniq?>' 
    v-bind:building_construction="building_construction"
>
</buildings-theme_tab-building_site_diaries>

<script type='text/javascript'>

    new Vue({
        el: '#<?=$uniq?>',
        data: {
            building_construction: sima.vue.shot.getters.model('<?=SIMAMisc::getVueModelTag($building_construction)?>')
        }
    });

</script>