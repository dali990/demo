<p class='row'>
    <span class='title'><?php echo $model->getAttributeLabel('name') ?>:</span>
    <span class='data'><?php echo $model->getAttributeDisplay('name') ?></span>
</p>
<p class='row'>
    <span class='title'><?php echo $model->getAttributeLabel('building_project_type_id') ?>:</span>
    <span class='data'><?php echo $model->getAttributeDisplay('building_project_type_id') ?></span>
</p>
<p class='row'>
    <span class='title'><?php echo $model->getAttributeLabel('theme_id') ?>:</span>
    <span class='data'><?php echo $model->getAttributeDisplay('theme_id') ?></span>
</p>
<p class='row'>
    <span class='title'><?php echo $model->getAttributeLabel('order') ?>:</span>
    <span class='data'><?php echo $model->getAttributeDisplay('order') ?></span>
</p>
<p class='row'>
    <span class='title'><?php echo $model->getAttributeLabel('geo_object_id') ?>:</span>
    <span class='data'><?php echo $model->getAttributeDisplay('geo_object_id') ?></span>
</p>
<p class='row'>
    <span class='title'><?php echo $model->getAttributeLabel('responsible_designer_licence_id') ?>:</span>
    <span class='data'><?php echo $model->getAttributeDisplay('responsible_designer_licence_id') ?></span>
</p>

<p class='row'>
    <span class='title'><?php echo $model->getAttributeLabel('date') ?>:</span>
    <span class='data'><?php echo $model->getAttributeDisplay('date') ?></span>
</p>
<p class='row'>
    <span class='title'><?php echo $model->getAttributeLabel('number') ?>:</span>
    <span class='data'><?php echo $model->getAttributeDisplay('number') ?></span>
</p>