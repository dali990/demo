<?php $uniq= SIMAHtml::uniqid();?>

<h2 style='width: auto;'>

    <?php 
//    if($model->CanHaveAdditionalContent)
//    {
        echo SIMAHtml::simpleButton(
            Yii::t('BuildingsModule.BuildingProject', 'AddAdditionalContent'), 
            'sima.buildings.addAdditionalContent('.$model->id.',$(this));',
            '',
            Yii::t('BuildingsModule.BuildingProject', 'AddAdditionalContentTitle')
        );
//    } 
    ?>
</h2>	

<p class="row number">
    <span class="title"><?php echo $model->getAttributeLabel('number')?>:</span>
    <?php if(isset($model->number) && $model->number!=null)
    {?>
        <span class='data'><?php echo $model->getAttributeDisplay('NumberFull');?></span>
    <?php 
          if($model->isBaseProject)
          { ?>
             <span onclick="sima.model.form('BuildingProject',<?php echo $model->id?>,{formName:'changeNumber'});" class="link data">Zameni broj</span>
    <?php }
    }
   else {
        if($model->isBaseProject)
        {
       ?>     
            <span onclick="sima.buildings.addNumberFromBuildingProject(<?php echo $model->id?>);" class="link data">Dodeli zavodni broj</span>
   <?php
        }
        else
        { ?>
             <span>Broj se zavodi/dobija na osnovu osnovnog projekta</span>
      <?php  }
   } ?>
</p>

<p class="row date">
    <?php   
        if(isset($model->date)){?>
            <span class="title"><?php echo $model->getAttributeLabel('date')?>:</span>
            <span class='data'><?php echo $model->getAttributeDisplay('date');?></span>
    <?php }?>
</p>

<?php 
    if($model->isBaseProject || $model->prefix === BuildingProject::$PREFIX_BOOK)
    {
?>
        <p class="row responsible_designers_decision">
            <span class="title"><?php echo $model->getAttributeLabel('responsible_designers_decision')?>:</span>
            <span class='data'><?php echo $model->getAttributeDisplay('responsible_designers_decision');?></span>
        </p>
<?php 
        if($model->prefix === BuildingProject::$PREFIX_BOOK)
        {
?>
        <p class="row responsible_designer_licence">
            <span class="title"><?php echo $model->getAttributeLabel('responsible_designer_licence')?>:</span>
            <span class='data'><?php echo $model->getAttributeDisplay('responsible_designer_licence');?></span>
        </p>
<?php
        }
    }
?>

<p class="row theme_id">
    <?php   
        if(isset($model->theme_id)){?>
            <span class="title"><?php echo $model->getAttributeLabel('theme_id'); ?>:</span>
            <span class='data'><?php echo $model->getAttributeDisplay('theme_id'); ?></span>
    <?php }?>
</p>

<p class="row building_projects_sheet_id">
    <?php   
        if(isset($model->building_projects_sheet_id)){?>
            <span class="title"><?php echo $model->getAttributeLabel('building_projects_sheet_id')?>:</span>
            <span class='data'><?php echo $model->getAttributeDisplay('building_projects_sheet_id');?></span>
    <?php }?>
</p>

<p class="row building_project_type_id">
    <?php   
        if(isset($model->building_project_type_id)){?>
            <span class="title"><?php echo $model->getAttributeLabel('building_project_type_id')?>:</span>
            <span class='data'><?php echo $model->getAttributeDisplay('building_project_type_id');?></span>
    <?php }?>
</p>

<p class="row building_project_kind_id">
    <?php   
        if(isset($model->building_project_kind_id)){?>
            <span class="title"><?php echo $model->getAttributeLabel('building_project_kind_id')?>:</span>
            <span class='data'><?php echo $model->getAttributeDisplay('building_project_kind_id');?></span>
    <?php }?>
</p>


<p class="row geo_object_id">
    <span class="title"><?php echo $model->getAttributeLabel('geo_object_id')?>:</span>
    <span class='data'><?php echo $model->getAttributeDisplay('geo_object_id');?></span>
</p>
<p class="row parent_id">
    <?php   
        if(isset($model->parent_id)){?>
            <span class="title"><?php echo $model->getAttributeLabel('parent_id')?>:</span>
            <span class='data'> <?php echo $model->getAttributeDisplay('parent_id').SIMAHtml::modelDialog($model->parent);?></span>
    <?php }?>
</p>

<p class="row order">
    <?php   
        if(isset($model->order)){?>
            <span class="title"><?php echo $model->getAttributeLabel('order')?>:</span>
            <span class='data'> <?php echo $model->getAttributeDisplay('order');?></span>
    <?php }?>
</p>


<p class="row description">
    <span class="title"><?php echo $model->getAttributeLabel('description')?>:</span>
        <?php if(isset($model->description)){?>
            <span class='data'><?php echo $model->getAttributeDisplay('description');?> </span>
        <?php }?>
</p>

