<?php $uniq= SIMAHtml::uniqid();

function listLeaf($project)
{
    $tree='';
    
    $prefix=($project->prefix=='') ? 'base' : $project->prefix;
    
    $tree.='<li class="sima-building-projects-tree-leaf" data-parent-id="'.$project->parent_id.'" '
                . 'data-project-id="'.$project->id.'">'
                . '<span class="sima-ui-building-project-tree-icon '.$prefix.'">&nbsp;</span>'
                . '<span class="sima-ui-building-project-tree-title">'.$project->DisplayName.'</span>'
                . '<ul>';
    
        foreach ($project->children as $child)
        {
            $tree.=listLeaf($child);
        }
        
        $tree.='</ul></li>';
         
        return $tree;
}

?>

<div class="sima-ui-building-projects-tree-listed">
<?php echo listLeaf($model); ?>
</div>







