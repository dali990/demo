<h3><?=Yii::t('BuildingsModule.BuildingSiteDiary', 'BuildingSiteDiary')?><?=SIMAHtml::modelFormOpen($model)?></h3>
<div class="basic_info">
    <p class='row'>
        <span class='title'><?php echo $model->getAttributeLabel('building_construction') ?>:</span>
        <span class='data'><?php echo $model->getAttributeDisplay('building_construction') ?></span>
    </p>
    <p class='row'>
        <span class='title'><?php echo $model->getAttributeLabel('date') ?>:</span>
        <span class='data'><?php echo $model->getAttributeDisplay('date') ?></span>
    </p>
    <p class='row'>
        <span class='title'><?php echo $model->getAttributeLabel('constructor') ?>:</span>
        <span class='data'><?php echo $model->getAttributeDisplay('constructor') ?></span>
    </p>
    <p class='row'>
        <span class='title'><?php echo $model->getAttributeLabel('work_description') ?>:</span>
        <span class='data'><?php echo $model->getAttributeDisplay('work_description') ?></span>
    </p>
    <p class='row'>
        <span class='title'><?php echo $model->getAttributeLabel('remarks') ?>:</span>
        <span class='data'><?php echo $model->getAttributeDisplay('remarks') ?></span>
    </p>
    <p class='row'>
        <span class='title'><?php echo $model->getAttributeLabel('diary_creator_confirmed') ?>:</span>
        <span class='data'><?php echo $model->getAttributeDisplay('diary_creator_confirmed') ?></span>
    </p>
    <p class='row'>
        <span class='title'><?php echo $model->getAttributeLabel('responsible_engineer_confirmed') ?>:</span>
        <span class='data'><?php echo $model->getAttributeDisplay('responsible_engineer_confirmed') ?></span>
    </p>
    <p class='row'>
        <span class='title'><?php echo $model->getAttributeLabel('supervisor_confirmed') ?>:</span>
        <span class='data'><?php echo $model->getAttributeDisplay('supervisor_confirmed') ?></span>
    </p>
</div>