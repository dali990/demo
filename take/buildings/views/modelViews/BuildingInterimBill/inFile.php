<h3><?=Yii::t('BuildingsModule.BuildingInterimBill', 'BuildingInterimBill')?><?=SIMAHtml::modelFormOpen($model)?></h3>
<div class="basic_info">
    <p class='row'>
        <span class='title'><?php echo $model->getAttributeLabel('building_construction') ?>:</span>
        <span class='data'><?php echo $model->getAttributeDisplay('building_construction') ?></span>
    </p>
</div>