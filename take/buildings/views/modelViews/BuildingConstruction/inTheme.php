<h3><?=Yii::t('BuildingsModule.BuildingConstruction', 'BuildingConstruction')?><?=SIMAHtml::modelFormOpen($model)?></h3>
<div class="basic_info">
    <p class='row'>
        <span class='title'><?php echo $model->getAttributeLabel('constructor') ?>:</span>
        <span class='data'><?php echo $model->getAttributeDisplay('constructor') ?></span>
    </p>
    <p class='row'>
        <span class='title'><?php echo $model->getAttributeLabel('investor') ?>:</span>
        <span class='data'><?php echo $model->getAttributeDisplay('investor') ?></span>
    </p>
    <p class='row'>
        <span class='title'><?php echo $model->getAttributeLabel('bill_of_quantity') ?>:</span>
        <span class='data'><?php echo $model->getAttributeDisplay('bill_of_quantity') ?></span>
    </p>
    <p class='row'>
        <span class='title'><?php echo $model->getAttributeLabel('start_date') ?>:</span>
        <span class='data'><?php echo $model->getAttributeDisplay('start_date') ?></span>
    </p>
    <p class='row'>
        <span class='title'><?php echo $model->getAttributeLabel('responsible_engineer') ?>:</span>
        <span class='data'><?php echo $model->getAttributeDisplay('responsible_engineer') ?></span>
    </p>
    <p class='row'>
        <span class='title'><?php echo $model->getAttributeLabel('construction_boss') ?>:</span>
        <span class='data'><?php echo $model->getAttributeDisplay('construction_boss') ?></span>
    </p>
    <p class='row'>
        <span class='title'><?php echo $model->getAttributeLabel('construction_boss_assistants_list') ?>:</span>
        <span class='data'>
            <?php
                foreach ($model->construction_boss_assistants as $construction_boss_assistant)
                {
                    echo "<span style='display: block; margin-bottom: 3px;'>{$construction_boss_assistant->DisplayHtml}</span>";
                }
            ?>
        </span>
    </p>
</div>