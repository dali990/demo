<div style="border-bottom: dotted" class="sima-layout-fixed-panel">
    <?php
    echo Yii::app()->controller->widget(SIMAButtonVue::class, [
        'id' => 'btn-generate-pdf'.SIMAHtml::uniqid(),
        'title'=>Yii::t('BuildingsModule.BuildingMeasurementSheet','GeneratePDF'),
        'tooltip'=> Yii::t('BuildingsModule.BuildingMeasurementSheet','GeneratePDF'),                               
        'onclick'=> ['sima.buildings.buildingMeasurementSheetExportPdf', $model->id]
    ],true);
    ?>
</div>
<div class="sima-layout-panel">
    <?php   
        $bms = new BuildingMeasurementSheetReport(["building_measurement_sheet" => $model]);
        echo $bms->getHTML();
    ?>
</div>
