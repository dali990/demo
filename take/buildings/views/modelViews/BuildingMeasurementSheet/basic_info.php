<p class='row'>
    <span class='title'><?php echo $model->getAttributeLabel('building_interim_bill') ?>:</span>
    <span class='data'><?php echo $model->getAttributeDisplay('building_interim_bill') ?></span>
</p>
<p class='row'>
    <span class='title'><?php echo $model->getAttributeLabel('bill_of_quantity_item') ?>:</span>
    <span class='data'><?php echo $model->getAttributeDisplay('bill_of_quantity_item') ?></span>
</p>
<p class="row">
    <span class="title"><?php echo $model->getAttributeLabel('written')?>:</span>
    <span class='data'><?php echo SIMAHtml::status($model, 'written'); ?> </span>
</p>
<p class="row">
    <span class="title"><?php echo $model->getAttributeLabel('confirmed')?>:</span>
    <span class='data'><?php echo SIMAHtml::status($model, 'confirmed'); ?> </span>
</p>
<p class="row">
    <span class="title"><?php echo $model->getAttributeLabel('validated')?>:</span>
    <span class='data'><?php echo SIMAHtml::status($model, 'validated'); ?> </span>
</p>
<?php if ($model->confirmed || $model->validated) { ?>
    <p class="row">
        <span class="title"><?php echo $model->getAttributeLabel('quantity_validated')?>:</span>
        <span class='data'><?php echo $model->getAttributeDisplay('quantity_validated'); ?> </span>
    </p>
<?php } ?>