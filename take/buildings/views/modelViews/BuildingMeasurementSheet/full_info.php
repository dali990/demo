<div class="sima-layout-panel _vertical _splitter">   
    <div class="sima-layout-panel _horizontal"
         data-sima-layout-init='{"proportion":0.2, "min_width":100}'>       
        <?php

            $uniq = SIMAHtml::uniqid();

            $this->widget(SIMAGuiTable::class, [
                'id' => "measurement_sheet_items_$uniq",
                'title' => Yii::t('BuildingsModule.BuildingMeasurementSheetItem', 'BuildingMeasurementSheetItems'),
                'model' => BuildingMeasurementSheetItem::model(),
                'columns_type' => 'fromBuildingMeasurementSheet',
                'add_button' => [
                    'init_data' => [
                        BuildingMeasurementSheetItem::class => [
                            'building_measurement_sheet_id' => [
                                'disabled',
                                'init' => $model->id,
                            ]
                        ]
                    ]
                ],
                'fixed_filter' => [
                    'building_measurement_sheet' => [
                        'ids' => $model->id
                    ]
                ]
            ]);
        ?>
    </div>
    
    <div class="sima-layout-panel _horizontal" data-sima-layout-init='{"proportion":0.6, "min_width":600}'>
        <?=Yii::app()->controller->renderModelView($model, 'full_info_right_view');?>
    </div>
</div>

