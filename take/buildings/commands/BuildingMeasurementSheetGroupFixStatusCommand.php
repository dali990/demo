<?php
     
class BuildingMeasurementSheetGroupFixStatusCommand extends SIMACommand
{
    public function performWork($args)
    {
        $count = 1;
        
        $sheets_cnt = BuildingMeasurementSheet::model()->count([
            'model_filter' => [
                'filter_scopes' => 'onlyOnFirstInterimBill'
            ]
        ]);

        $sheet_groups_cnt = BuildingMeasurementSheetGroup::model()->count([
            'model_filter' => [
                'bill_of_quantity_item_group' => [
                    'filter_scopes' => 'onlyLeaf'
                ]
            ]
        ]);
        
        $total_count = $sheets_cnt + $sheet_groups_cnt;
        
        BuildingMeasurementSheet::model()->findAllByParts(function($sheets) use ($total_count, &$count) {
            foreach($sheets as $sheet)
            {
                $sheet->calcValueDone();

                echo "\r - $count/".$total_count;

                $count++;
            }
        }, new SIMADbCriteria([
            'Model' => BuildingMeasurementSheet::class,
            'model_filter' => [
                'filter_scopes' => 'onlyOnFirstInterimBill'
            ]
        ]));

        BuildingMeasurementSheetGroup::model()->findAllByParts(function($sheet_groups) use ($total_count, &$count) {
            foreach($sheet_groups as $sheet_group)
            {
                $sheet_group->calcValueDoneRecursive();

                echo "\r - $count/".$total_count;

                $count++;
            }
        }, new SIMADbCriteria([
            'Model' => BuildingMeasurementSheetGroup::class,
            'model_filter' => [
                'bill_of_quantity_item_group' => [
                    'filter_scopes' => 'onlyLeaf'
                ]
            ]
        ]));

        echo "\n";
    }
}