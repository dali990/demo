<?php

return [
    'project_books' => [
        BuildingProject::$BOOK_NAME_0 => [
            'order' => '0',
            'display_name' => Yii::t('BuildingsModule.BuildingProject', 'Book0')
        ],
        BuildingProject::$BOOK_NAME_1 => [
            'order' => '1',
            'display_name' => Yii::t('BuildingsModule.BuildingProject', 'Book1')
        ],
        BuildingProject::$BOOK_NAME_2 => [
            'order' => '2',
            'display_name' => Yii::t('BuildingsModule.BuildingProject', 'Book2')
        ],
        BuildingProject::$BOOK_NAME_3 => [
            'order' => '3',
            'display_name' => Yii::t('BuildingsModule.BuildingProject', 'Book3')
        ],
        BuildingProject::$BOOK_NAME_4 => [
            'order' => '4',
            'display_name' => Yii::t('BuildingsModule.BuildingProject', 'Book4')
        ],
        BuildingProject::$BOOK_NAME_5 => [
            'order' => '5',
            'display_name' => Yii::t('BuildingsModule.BuildingProject', 'Book5')
        ],
        BuildingProject::$BOOK_NAME_6 => [
            'order' => '6',
            'display_name' => Yii::t('BuildingsModule.BuildingProject', 'Book6')
        ],
        BuildingProject::$BOOK_NAME_7 => [
            'order' => '7',
            'display_name' => Yii::t('BuildingsModule.BuildingProject', 'Book7')
        ],
        BuildingProject::$BOOK_NAME_8 => [
            'order' => '8',
            'display_name' => Yii::t('BuildingsModule.BuildingProject', 'Book8')
        ],
        BuildingProject::$BOOK_NAME_9 => [
            'order' => '9',
            'display_name' => Yii::t('BuildingsModule.BuildingProject', 'Book9')
        ],
        BuildingProject::$BOOK_NAME_10 => [
            'order' => '10',
            'display_name' => Yii::t('BuildingsModule.BuildingProject', 'Book10')
        ],
    ]
];