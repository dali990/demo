<?php

return array(
    'ConstructionMaterialApproval' => [
        'modelConstructionMaterialApprovalApprovedConfirm',
        'modelConstructionMaterialApprovalApprovedRevert'
    ]
);