<?php

return [
    [
        'name' => 'document_type_for_text',
        'display_name' => 'Tip dokumenta za Tekst',
        'type' => 'searchField',
        'type_params' => array(
            'modelName' => 'DocumentType',
        ),
        'value_not_null' => false,
        'user_change_forbidden' => false,
        'description' => 'Tip dokumenta za Tekst',
    ],
    [
        'name' => 'document_type_for_numeric',
        'display_name' => 'Tip dokumenta za Numeriku',
        'type' => 'searchField',
        'type_params' => array(
            'modelName' => 'DocumentType',
        ),
        'value_not_null' => false,
        'user_change_forbidden' => false,
        'description' => 'Tip dokumenta za Numeriku',
    ],
    [
        'name' => 'document_type_for_graphic',
        'display_name' => 'Tip dokumenta za Grafiku',
        'type' => 'searchField',
        'type_params' => array(
            'modelName' => 'DocumentType',
        ),
        'value_not_null' => false,
        'user_change_forbidden' => false,
        'description' => 'Tip dokumenta za Grafiku',
    ],
    [
        'name' => 'interim_bill_document_type_id',
        'display_name' => Yii::t('BuildingsModule.BuildingInterimBill', 'InterimBillDocumentTypeId'),
        'type' => 'searchField',
        'type_params' => [
            'modelName' => 'DocumentType',
        ],
        'value_not_null' => false,
        'user_change_forbidden' => true,
        'description' => Yii::t('BuildingsModule.BuildingInterimBill', 'InterimBillDocumentTypeId'),
    ],
    [
        'name' => 'building_site_diary_document_type_id',
        'display_name' => Yii::t('BuildingsModule.BuildingSiteDiary', 'BuildingSiteDiaryDocumentTypeId'),
        'type' => 'searchField',
        'type_params' => [
            'modelName' => 'DocumentType',
        ],
        'value_not_null' => false,
        'user_change_forbidden' => true,
        'description' => Yii::t('BuildingsModule.BuildingSiteDiary', 'BuildingSiteDiaryDocumentTypeId'),
    ],
];
