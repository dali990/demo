<?php

return array(
    'template_responsible_designers_decision_id' => 166,
    'template_responsible_designer_decision_id' => 168,
    'document_type_for_text' => 273,
    'document_type_for_numeric' => 274,
    'document_type_for_graphic' => 275,
    'file_template_for_text' => 279,
    'file_template_for_numeric' => 280,
    'file_template_for_graphic' => 281,
    'interim_bill_document_type_id' => 416,
    'building_site_diary_document_type_id' => 421,
    'building_construction_boss_theme_coordinator' => 476
);