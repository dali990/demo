<?php

return [
    'display_name' => Yii::t('BuildingsModule.Configuration', 'Buildings'),
    
    [
        'name' => 'template_responsible_designers_decision_id',
        'display_name' => Yii::t('BuildingsModule.Configuration', 'TemplateResponsibleDesignersDecision'),
        'type'=>'searchField',
        'type_params' => array (
            'modelName'=>'Template',
        ),
        'value_not_null' => false,
        'description' => Yii::t('BuildingsModule.Configuration', 'TemplateResponsibleDesignersDecisionDescription')
    ],
    [
        'name' => 'template_responsible_designer_decision_id',
        'display_name' => Yii::t('BuildingsModule.Configuration', 'TemplateResponsibleDesignerDecision'),
        'type'=>'searchField',
        'type_params' => array (
            'modelName'=>'Template',
        ),
        'value_not_null' => false,
        'description' => Yii::t('BuildingsModule.Configuration', 'TemplateResponsibleDesignerDecisionDescription')
    ],
    [
        'name' => 'file_template_for_text',
        'display_name' => 'Template za Tekst',
        'type' => 'searchField',
        'type_params' => array(
            'modelName' => 'FileTemplate',
        ),
        'value_not_null' => false,
        'user_change_forbidden' => false,
        'description' => 'Template za Tekst',
    ],
    [
        'name' => 'file_template_for_numeric',
        'display_name' => 'Template za Numeriku',
        'type' => 'searchField',
        'type_params' => array(
            'modelName' => 'FileTemplate',
        ),
        'value_not_null' => false,
        'user_change_forbidden' => false,
        'description' => 'Template za Tekst',
    ],
    [
        'name' => 'file_template_for_graphic',
        'display_name' => 'Template za Grafiku',
        'type' => 'searchField',
        'type_params' => array(
            'modelName' => 'FileTemplate',
        ),
        'value_not_null' => false,
        'user_change_forbidden' => false,
        'description' => 'Template za Tekst',
    ],
    [
        'name' => 'building_construction_boss_theme_coordinator',
        'display_name' => Yii::t('BuildingsModule.Configuration', 'BuildingConstructionBossThemeCoordinator'),
        'description' => Yii::t('BuildingsModule.Configuration', 'BuildingConstructionBossThemeCoordinator'),
        'value_not_null' => true,
        'type' => 'boolean',
        'default' => false
    ]
];