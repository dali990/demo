<?php

class BuildingProjectComponent
{
    public static function GenerateResponsibleDesignersDecision(BuildingProject $buildingProject)
    {
        $template_id = null;
        $file_name = null;
        
        if($buildingProject->isBaseProject)
        {
//            if(empty($buildingProject->theme))
//            {
//                throw new SIMAWarnException(Yii::t(
//                    'BuildingsModule.BuildingProject', 
//                    'ThemeNotSet',
//                    ['{building_project}' => $buildingProject->DisplayName]
//                ));
//            }
//            
//            if(empty($buildingProject->theme->job->base_document))
//            {
//                throw new SIMAWarnException(Yii::t(
//                    'BuildingsModule.BuildingProject', 
//                    'BaseDocumentNotSet',
//                    ['{job}' => $buildingProject->theme->job->DisplayName]
//                ));
//            }
//            
//            if(empty($buildingProject->theme->job->base_document->contract))
//            {
//                throw new SIMAWarnException(Yii::t(
//                    'BuildingsModule.BuildingProject', 
//                    'ContractNotSet',
//                    ['{base_document}' => $buildingProject->theme->job->base_document->DisplayName]
//                ));
//            }
            
            $children = $buildingProject->children;
        
            foreach($children as $child)
            {
                if($child->isBook)
                {
                    if(empty($child->responsible_designer_licence))
                    {
                        throw new SIMAWarnException(Yii::t(
                            'BuildingsModule.BuildingProject', 
                            'ResponsibleDesignerNotSet',
                            ['{building_project}' => $child->DisplayName]
                        ));
                    }
                }
            }
            
            $template_id = Yii::app()->configManager->get('buildings.template_responsible_designers_decision_id', false);
            $file_name = Yii::t('BuildingsModule.BuildingProject', 'ResponsibleDesignersDecision');
        }
        else if ($buildingProject->isBook)
        {
            if(empty($buildingProject->responsible_designer_licence))
            {
                throw new SIMAWarnException(Yii::t(
                    'BuildingsModule.BuildingProject', 
                    'ResponsibleDesignerNotSet',
                    ['{building_project}' => $buildingProject->DisplayName]
                ));
            }
            
            if(empty($buildingProject->parent->responsible_designers_decision_id))
            {
                throw new SIMAWarnException(Yii::t('BuildingsModule.BuildingProject', 'MustHaveDesignersDecisionToGenerateSubdecisions'));
            }
            
            $template_id = Yii::app()->configManager->get('buildings.template_responsible_designer_decision_id', false);
            $file_name = Yii::t('BuildingsModule.BuildingProject', 'ResponsibleDesignerDecision');
        }
        else
        {
            throw new Exception(Yii::t('BuildingsModule.BuildingProject', 'ThisCannotHaveDesignersDecision'));
        }
        
        if (empty($template_id))
        {
            throw SIMAWarnException('Niste postavili tempalte za generisanje resenja');
        }
        
        if(empty($buildingProject->responsible_designers_decision_id))
        {
            $file = new File();
            $file->name = $file_name;
            $file->responsible_id = Yii::app()->user->id;
            $file->save();
            $file->refresh();
            
            $template_file_id = TemplateController::setTemplate($file->id, $template_id);
            TemplateController::addVersion($template_file_id);

            $buildingProject->responsible_designers_decision_id = $file->id;
            $buildingProject->save();
        }
        else
        {
            $file = $buildingProject->responsible_designers_decision;
            $file->beforeChangeLock();
            $file->name = $file_name;
            $file->responsible_id = Yii::app()->user->id;
            $file->save();
            $file->afterChangeUnLock();
            
            $template_file_id = TemplateController::setTemplate($file->id, $template_id);
            TemplateController::addVersion($template_file_id);
        }
        
//        $filing = $file->filing;
//        if(empty($filing))
//        {
//            $filing = new Filing;
//            $filing->id = $file->id;
//            $filing->partner_id = Yii::app()->company->id;
//            $filing->located_at_id = Yii::app()->user->id;
//            
//            if(!$buildingProject->isBaseProject)
//            {
//                $filing->number = $buildingProject->parent->responsible_designers_decision->filing->number;
//                $filing->subnumber = $buildingProject->order;
//                $filing->save();
//            }
//            
//            $filing->save();
//        }
    }
    
    public static function GenerateResponsibleDesignersSubDecision(BuildingProject $buildingProject)
    {
        $template_id = null;
        
        if(!$buildingProject->isBaseProject)
        {
            throw new Exception(Yii::t('BuildingsModule.BuildingProject', 'MustBeBaseProject'));
        }
        
        if(empty($buildingProject->responsible_designers_decision_id))
        {
            throw new SIMAWarnException(Yii::t('BuildingsModule.BuildingProject', 'MustHaveDesignersDecisionToGenerateSubdecisions'));
        }
        
        $children = $buildingProject->children;
        
        foreach($children as $child)
        {
            if($child->isBook)
            {
                if(empty($child->responsible_designer_licence))
                {
                    throw new SIMAWarnException(Yii::t(
                        'BuildingsModule.BuildingProject', 
                        'ResponsibleDesignerNotSet',
                        ['{building_project}' => $child->DisplayName]
                    ));
                }
            }
        }
        
        foreach($children as $child)
        {
            if($child->isBook)
            {
                BuildingProjectComponent::GenerateResponsibleDesignersDecision($child);
            }
        }
    }
    
    public static function GenerateAddAdditionalContent(BuildingProject $buildingProject)
    {
        if(!$buildingProject->CanHaveAdditionalContent)
        {
            throw new Exception(Yii::t('BuildingsModule.BuildingProject', 'CanNotHaveAdditionalContent'));
        }
        
        $i = 1;
        foreach(BuildingProject::$ADDITIONAL_CONTENT_PREFIXES as $acp)
        {
            $childAdditional = new BuildingProject;
            $childAdditional->prefix = $acp;
            $childAdditional->order = $i++;
            $childAdditional->parent_id = $buildingProject->id;
            $childAdditional->building_project_type_id = $buildingProject->building_project_type_id;
            $childAdditional->name = Yii::t('BuildingsModule.BuildingProject', $acp);
            $childAdditional->theme_id = $buildingProject->theme_id;
            $childAdditional->save();
        }
    }
}
