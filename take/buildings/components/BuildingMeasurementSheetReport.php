<?php

class BuildingMeasurementSheetReport extends SIMAReport
{
    protected $css_file_name = 'css.css';

    protected function getModuleName()
    {
        return 'buildings';
    }

    protected function getHTMLContent()
    {
        $building_measurement_sheet = $this->params['building_measurement_sheet'];
        
        $bill_of_quantity_item = $building_measurement_sheet->bill_of_quantity_item;
        $construction_site = $bill_of_quantity_item->group->bill_of_quantity->name;
        $sector = $building_measurement_sheet->building_interim_bill->building_construction->constructor->DisplayName;
        $object = $building_measurement_sheet->object;
        $sheet_items = $building_measurement_sheet->sheet_items;
        $kind_of_work = $bill_of_quantity_item->name;
        return [
            'html' => $this->render('html', [
                'building_measurement_sheet' => $building_measurement_sheet,
                'bill_of_quantity_item' => $bill_of_quantity_item,
                'object' => $object,
                'sheet_items' => $sheet_items,
                'construction_site' => $construction_site,
                'sector' => $sector,
                'kind_of_work' => $kind_of_work
            ], true, false),
            'css_file_name' => 'css'
        ];        
    }
}