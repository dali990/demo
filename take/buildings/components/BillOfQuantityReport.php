<?php

class BillOfQuantityReport extends SIMAReport
{
    protected $orientation = 'Landscape';
    protected $css_file_name = 'css.css';
    protected $margin_top = 20;
    protected $margin_bottom = 10;
    protected $include_page_numbering = false;
    
    private $exporting_pdf = false;
    private $leaf_objects_count = 1; //ukupan broj objekata
    private $has_object_value_per_unit = false;
    private $object_colspan = 2; //za naslove objekata u headru i za izracunavanje sirine kolone za stavke (item_to_object)
    private $group_item_title_colspan = 2; //colspan za naziv grupe/segmenta
    private $object_columns_width = 0;
    private $indent_item = 0; //padding-left u mm
    private $indent_item_increment = 4;//za koliko da poveca za svaku podgrupu
    private $object_order = []; //redosled id objekata za prikaz
    private $tilde = '';//znak za pribliznu vrednost jedinicne cene

    protected function getModuleName()
    {
        return 'buildings';
    }
    
    protected function getHTMLHeader() 
    {
        $bill_of_quantity = $this->params['bill_of_quantity'];
        $exporting_pdf = $this->params['ACTION_EXPORT_PDF'] ?? false;
        $this->exporting_pdf = $exporting_pdf;
        $this->leaf_objects_count = $bill_of_quantity->leaf_objects_count;
        
        $boq_table_header = [
            'name' => [
                'label' => Yii::t('BuildingsModule.BillOfQuantityItem', 'Name'),
                'css_class' => $exporting_pdf ? 'item-name-pdf' : 'item-name'
            ],
            'measurement_unit' => [
                'label' => Yii::t('BuildingsModule.BillOfQuantityItem', 'MeasurementUnit'),
                'css_class' => $exporting_pdf ? 'measurement-unit-name-pdf' : 'measurement-unit-name'
            ],
            'value_per_unit' => [
                'label' => Yii::t('BuildingsModule.BillOfQuantityItem', 'ValuePerUnit'),
                'css_class' => $exporting_pdf ? 'value-per-unit-pdf' : 'value-per-unit'
            ],
            'object_columns' => [
                'objects' => [],
                'width' => ''
            ]
        ];
        
        $boq_table_header['object_columns']['objects'] = $bill_of_quantity->leaf_objects;
        $objects_cnt = count($boq_table_header['object_columns']['objects']);
        //zbog prikaza kolona za situacija sirisa kolona se deli sa 2
        $boq_table_header['object_columns']['width'] = $this->object_columns_width = round(94/$objects_cnt); //item-name(62)+measurement-unit-name(12)+value-per-unit(20)
        
        if($this->leaf_objects_count === 1)
        {
            $this->group_item_title_colspan = 6;//za kolone measurement sheet
            unset($boq_table_header['value_per_unit']);
            $boq_table_header['object_columns']['width'] = $this->object_columns_width = round((94/$objects_cnt)/2);
        }
            
        if($bill_of_quantity->has_object_value_per_unit)
        {
            unset($boq_table_header['value_per_unit']);
            $this->has_object_value_per_unit = true;
            $this->object_colspan++;
        }
        $this->group_item_title_colspan *= ($this->leaf_objects_count * $this->object_colspan);
        if(!$this->has_object_value_per_unit )
        {
            //Povecava colspan zbog jedinicne cene
            $this->group_item_title_colspan++;
        }
        return $this->render('header', [
            'bill_of_quantity' => $bill_of_quantity,
            'boq_table_header' => $boq_table_header,
            'exporting_pdf' => $exporting_pdf,
            'has_object_value_per_unit' => $this->has_object_value_per_unit,
            'object_colspan' => $this->object_colspan,
            'leaf_objects_count' => $this->leaf_objects_count,
            'object_columns_width' => $this->object_columns_width
        ], true, false);
    }
    
    protected function getHTMLContent()
    {
        $bill_of_quantity = $this->params['bill_of_quantity'];
        
        $this->download_name = $bill_of_quantity->name . '.pdf';
        
        $boq_table_content = $this->boqToArray($bill_of_quantity);
        $content = $this->renderTable($boq_table_content, $bill_of_quantity);
        
        return $this->render('html', [
            'exporting_pdf' => $this->exporting_pdf,
            'content' => $content
        ], true, false);
    }
    
    private function renderTable($boq_table_content, $bill_of_quantity) 
    {
        $content = "";
        foreach ($boq_table_content as $group_item)
        {
            $this->indent_item = 0;
            $content .= $this->renderItemGroups($group_item);
        }
        
        //Poslednji red - ukupna suma
        $content .= $this->render('item_group_total', [
            'exporting_pdf' => $this->exporting_pdf,
            'group_item' => $this->getRootGroupItemTotal($bill_of_quantity),
            'leaf_objects_count' => $this->leaf_objects_count,
            'has_object_value_per_unit' => $this->has_object_value_per_unit,
            'indent_item' => 0,
            'last_row' => true
        ], true, false);
        
        return $content;
    }
    
    private function renderItemGroups($group_item) 
    {
        $content = "";
        
        $content .= $this->render('item_group_title', [
            'exporting_pdf' => $this->exporting_pdf,
            'group_item' => $group_item,
            'group_item_title_colspan' => $this->group_item_title_colspan,
            'indent_item' => $this->indent_item
        ], true, false);
        foreach ($group_item['groups'] as $group)
        {
            $this->indent_item += $this->indent_item_increment;
            $content .= $this->renderItemGroups($group);
            $this->indent_item -= $this->indent_item_increment;
        }
        $content .= $this->render('item_group_items', [
            'exporting_pdf' => $this->exporting_pdf,
            'group_item' => $group_item,
            'object_columns_width' => $this->object_columns_width,
            'has_object_value_per_unit' => $this->has_object_value_per_unit,
            'object_colspan' => $this->object_colspan,
            'leaf_objects_count' => $this->leaf_objects_count,
            'group_item_title_colspan' => $this->group_item_title_colspan,
            'indent_item' => ($this->indent_item += $this->indent_item_increment)
        ], true, false);
        $content .= $this->render('item_group_total', [
            'exporting_pdf' => $this->exporting_pdf,
            'group_item' => $group_item,
            'leaf_objects_count' => $this->leaf_objects_count,
            'has_object_value_per_unit' => $this->has_object_value_per_unit,
            'indent_item' => ($this->indent_item -= $this->indent_item_increment),
            'last_row' => false
        ], true, false);
            
        
        return $content;
    }
    
    private function boqToArray(BillOfQuantity $bill_of_quantity) : array
    {
        $boq_table = [];
        
        foreach ($bill_of_quantity->first_level_groups as $item_group) 
        {
            $boq_table[] = $this->getGroupItemsRecurisve($item_group);
        }
      
        return $boq_table;
    }
    
    private function getGroupItemsRecurisve(BillOfQuantityItemGroup $item_group) : array
    {
        $items = [
            'name' => $item_group->title,
            'measurement_unit_name' => '',
            'groups' => [], //podgrupe, sadrzi children, nizove iste strukture kao $items
            'items' => [], //stavke
            'group_value' => ['objects' => [] ], //suma objekata iz grupe
            'objects_cnt' => 0
        ];
        $has_same_measurement_unit = $this->getMesuramentUnitName($item_group);
        $ordered_item_group_to_objects_values = [];
        $this->object_order = [];//Cuvamo redosled objekata
        foreach($item_group->bill_of_quantity->leaf_objects as $object)
        {
            $this->object_order[] = $object->id;
        }
        foreach($item_group->items as $boq_item)
        {
            $ordered_item_to_objects = [];
            $items['items'][$boq_item->id] = [
                'name' => $boq_item->name,
                'measurement_unit_name' => $boq_item->measurement_unit->name,
                'value_per_unit' => $boq_item->value_per_unit->round(2)->toString(),
                'objects' => []
            ];
            //Cuvanje iznosa za svkau stavku (item_to_object)
            foreach($boq_item->item_to_objects_leafs as $item_to_object)
            {
                $items['items'][$boq_item->id]['objects'][$item_to_object->object->id] = [
                    'quantity' => $item_to_object->quantity->toString(),
                    'value_per_unit' => $item_to_object->value_per_unit->round(2)->toString(),
                    'value' => $item_to_object->value->toString(),
                    'measurement_sheet' => $this->getBuildingMeasurementSheet($boq_item->id, $item_to_object->object->id),
                    'object_name' => $item_to_object->object->name
                ];
            }
            //Sortiranje objekata
            foreach ($this->object_order as $objec_id)
            {
                $ordered_item_to_objects[$objec_id] = $items['items'][$boq_item->id]['objects'][$objec_id];
            }
            $items['items'][$boq_item->id]['objects'] = $ordered_item_to_objects;
        }
        //Cuvanje sume objekata po grupama  
        foreach($item_group->item_group_to_objects_leafs as $item_group_to_object)
        {
            $value_per_unit = '';
            if($has_same_measurement_unit)
            {
                if($this->has_object_value_per_unit)
                {
                    $value_per_unit = $this->tilde.$item_group_to_object->getApproximatelyValuePerUnit()->round(2)->getValue();
                }
                else
                {
                    $value_per_unit = $this->tilde.$item_group_to_object->item_group->getApproximatelyValuePerUnit()->round(2)->getValue();
                }
            }
            $items['group_value']['objects'][$item_group_to_object->object->id] = [
                'value' => $item_group_to_object->value->round(2)->toString(),
                'value_per_unit' => $value_per_unit,
                'quantity' => $has_same_measurement_unit ? $item_group_to_object->quantity->round(2)->toString() : '',
                'measurement_sheet' => $this->getBuildingMeasurementSheetGroup($item_group->id, $item_group_to_object->object->id),
            ];
        }
      
        //Sortiranje kolona za prikaz sume objekata po grupama
        foreach ($this->object_order as $objec_id)
        {
            $ordered_item_group_to_objects_values[$objec_id] = $items['group_value']['objects'][$objec_id];
        }
        $items['group_value']['measurement_unit_name'] = $has_same_measurement_unit; //measurement_unit_name
        $items['group_value']['value_per_unit'] = $has_same_measurement_unit ? $this->tilde.$item_group->getApproximatelyValuePerUnit()->round(2)->getValue() : '';
        $items['group_value']['objects'] = $ordered_item_group_to_objects_values;
        $items['objects_cnt'] = count($items['group_value']['objects']);
        
        //Ukoliko ima podgrupa rekurzivno kreiramo nove elemente istog formata 
        if($item_group->children_count > 0 && isset($item_group->parent))
        {
            foreach($item_group->children as $child_item_group)
            {
                $items['groups'][] = $this->getGroupItemsRecurisve($child_item_group);
            }
        }
        
        return $items;
    }
    
    private function getBuildingMeasurementSheet($item_id, $object_id) 
    {
        $bms = BuildingMeasurementSheet::model()->findByAttributes([
            'building_interim_bill_id' => $this->params['building_interim_bill_id'],
            'bill_of_quantity_item_id' => $item_id,
            'object_id' => $object_id
        ]);
        
        return $this->getBuildingMeasurementSheetItems($bms);
    }
    
    private function getBuildingMeasurementSheetGroup($item_group_id, $object_id) 
    {       
        $bmsg = BuildingMeasurementSheetGroup::model()->findByAttributes([
            'building_interim_bill_id' => $this->params['building_interim_bill_id'],
            'bill_of_quantity_item_group_id' => $item_group_id,
            'object_id' => $object_id
        ]);
        
        return $this->getBuildingMeasurementSheetItems($bmsg);
    }
    
    private function getBuildingMeasurementSheetItems($bms) 
    {
        $default_value = SIMABigNumber::fromFloat(0.00, 2)->getValue();
        $bms_items = [
            'value_done' => $default_value,
            'quantity_done' => $default_value,
            'pre_value_done' => $default_value,
            'pre_quantity_done' => $default_value,
            'total_value_done' => $default_value,
            'total_quantity_done' => $default_value
        ];
        if(!empty($bms))
        {
            $bms_items['value_done'] = $bms->value_done->round(2)->toString();
            $bms_items['quantity_done'] = $bms->quantity_done->round(2)->toString();
            $bms_items['pre_value_done'] = $bms->pre_value_done->round(2)->toString();
            $bms_items['pre_quantity_done'] = $bms->pre_quantity_done->round(2)->toString();
            $bms_items['total_value_done'] = $bms->total_value_done->round(2)->toString();
            $bms_items['total_quantity_done'] = $bms->total_quantity_done->round(2)->toString();
        }
        
        return $bms_items;
    }
    
    //Pakujemo poslednji red za prikaz ukupne sume jed. cene i ostale stavke svih objekata
    private function getRootGroupItemTotal(BillOfQuantity $bill_of_quantity) 
    {
        $root_group_total['group_value']['value_per_unit'] = '';
        $has_same_measurement_unit = $this->getMesuramentUnitName($bill_of_quantity->root_group);
        
        if($has_same_measurement_unit && !$this->has_object_value_per_unit)
        {
            $root_group_total['group_value']['value_per_unit'] = $bill_of_quantity->root_group->getApproximatelyValuePerUnit()->round(2)->getValue();
        }
        
        //Ispisivanje sume za objekte root grupe
        foreach($bill_of_quantity->root_group->item_group_to_objects_leafs as $item_group_to_object)
        {
            $value_per_unit = '';
            if($has_same_measurement_unit)
            {
                if($this->has_object_value_per_unit)
                {
                    $value_per_unit = $this->tilde.$item_group_to_object->getApproximatelyValuePerUnit()->round(2)->getValue();
                }
                else
                {
                    $value_per_unit = $this->tilde.$item_group_to_object->item_group->getApproximatelyValuePerUnit()->round(2)->getValue();
                }
            }
            $root_group_total['group_value']['objects'][$item_group_to_object->object->id] = [
                'value' => $item_group_to_object->value->round(2)->toString(),
                'value_per_unit' => $value_per_unit,
                'quantity' => $has_same_measurement_unit ? $item_group_to_object->quantity->round(2)->toString() : '',
                'measurement_sheet' => $this->getBuildingMeasurementSheetGroup($bill_of_quantity->root_group->id, $item_group_to_object->object->id)
            ];
        }
        
        foreach ($this->object_order as $objec_id)
        {
            $ordered_item_group_to_objects_values[$objec_id] = $root_group_total['group_value']['objects'][$objec_id];
        }
        $root_group_total['group_value']['measurement_unit_name'] = $has_same_measurement_unit; //measurement_unit_name
        $root_group_total['group_value']['objects'] = $ordered_item_group_to_objects_values;
        
        return $root_group_total;
    }
    
    private function getMesuramentUnitName(BillOfQuantityItemGroup $item_group) {
        $measurement_unit = '';
        if($item_group->hasAllItemsSameMeasurementUnit())
        {
            $group_first_item = $item_group->getFirstItemRecursively();
            if (!empty($group_first_item))
            {
                $measurement_unit = $group_first_item->measurement_unit->DisplayName;
            }
        }
        return $measurement_unit;
    }  
}

/*
 * Structura niza koju vraca getGroupItemsRecurisve
 * Primer predmera sa 1objekat

segmenat 1                                              mer. jed.       jed. cena       kolicina        iznos
    grupa1 u segementu1
        stavka u segmentu 1 -> grupa 1                  m3              10.00           2.00            20.00
    UKUPNO                                                              10.00           2.00            20.00
    stavka u segmentu 1                                 м3              10.00           5.00            50.00
    stavka u segmentu 1                                 м3              0.00                            0.00
UKUPNO                                                                  12.00                           70.00
segmenat 2
    grupa1 u segmentu2
        grupa1.1 u grupi1 -> segemenat 2
            savka u grupi 1.1 -> grupe 1 u segemntu 2   kom             0.00            0.00            0.00
        UKUPNO                                                          0.00            0.00            0.00
    UKUPNO                                                              0.00            0.00            0.00
UKUPNO                                                                  0.00            0.00            70.00

* Reprezentacija niza
Array
(
    [0] => Array
        (
            [name] => segmenat 1
            [measurement_unit_name] => 
            [groups] => Array
                (
                    [0] => Array
                        (
                            [name] => grupa1 u segementu1
                            [measurement_unit_name] => 
                            [groups] => Array
                                (
                                )

                            [items] => Array
                                (
                                    [110] => Array
                                        (
                                            [name] => stavka u segmentu 1 -> grupa 1
                                            [measurement_unit_name] => m3
                                            [value_per_unit] => 10.00
                                            [objects] => Array
                                                (
                                                    [23] => Array
                                                        (
                                                            [quantity] => 2.00
                                                            [value_per_unit] => 10.00
                                                            [value] => 20.00
                                                            [object_name] => Predmer uvoz
                                                        )

                                                )

                                        )

                                )

                            [group_value] => Array
                                (
                                    [objects] => Array
                                        (
                                            [23] => Array
                                                (
                                                    [value] => 20.00
                                                    [value_per_unit] => 10.00
                                                    [quantity] => 2.00
                                                )

                                        )

                                    [measurement_unit_name] => m3
                                    [value_per_unit] => 10.00
                                )

                            [objects_cnt] => 1
                        )

                )

            [items] => Array
                (
                    [108] => Array
                        (
                            [name] => stavka u segmentu 1
                            [measurement_unit_name] => m3
                            [value_per_unit] => 590.00
                            [objects] => Array
                                (
                                    [23] => Array
                                        (
                                            [quantity] => 10.00
                                            [value_per_unit] => 5.00
                                            [value] => 50.00
                                            [object_name] => Predmer uvoz
                                        )

                                )

                        )

                    [109] => Array
                        (
                            [name] => Stavka u segmentu 1
                            [measurement_unit_name] => m3
                            [value_per_unit] => 0.00
                            [objects] => Array
                                (
                                    [23] => Array
                                        (
                                            [quantity] => 0.00
                                            [value_per_unit] => 0.00
                                            [value] => 0.00
                                            [object_name] => Predmer uvoz
                                        )

                                )

                        )

                )

            [group_value] => Array
                (
                    [objects] => Array
                        (
                            [23] => Array
                                (
                                    [value] => 70.00
                                    [value_per_unit] => 5.83
                                    [quantity] => 12.00
                                )

                        )

                    [measurement_unit_name] => m3
                    [value_per_unit] => 5.83
                )

            [objects_cnt] => 1
        )

    [1] => Array
        (
            [name] => segmenat 2
            [measurement_unit_name] => 
            [groups] => Array
                (
                    [0] => Array
                        (
                            [name] => grupa1 u segmentu2
                            [measurement_unit_name] => 
                            [groups] => Array
                                (
                                    [0] => Array
                                        (
                                            [name] => grupa1.1 u grupi1 -> segemenat 2
                                            [measurement_unit_name] => 
                                            [groups] => Array
                                                (
                                                )

                                            [items] => Array
                                                (
                                                    [133] => Array
                                                        (
                                                            [name] => savka u grupi 1.1 -> grupe 1 u segemntu 2
                                                            [measurement_unit_name] => kom
                                                            [value_per_unit] => 0.00
                                                            [objects] => Array
                                                                (
                                                                    [23] => Array
                                                                        (
                                                                            [quantity] => 0.00
                                                                            [value_per_unit] => 0.00
                                                                            [value] => 0.00
                                                                            [object_name] => Predmer uvoz
                                                                        )

                                                                )

                                                        )

                                                )

                                            [group_value] => Array
                                                (
                                                    [objects] => Array
                                                        (
                                                            [23] => Array
                                                                (
                                                                    [value] => 0.00
                                                                    [value_per_unit] => 0.00
                                                                    [quantity] => 0.00
                                                                )

                                                        )

                                                    [measurement_unit_name] => kom
                                                    [value_per_unit] => 0.00
                                                )

                                            [objects_cnt] => 1
                                        )

                                )

                            [items] => Array
                                (
                                    [111] => Array
                                        (
                                            [name] => stavka u segment 2 -> grupa1
                                            [measurement_unit_name] => m3
                                            [value_per_unit] => 0.00
                                            [objects] => Array
                                                (
                                                    [23] => Array
                                                        (
                                                            [quantity] => 0.00
                                                            [value_per_unit] => 0.00
                                                            [value] => 0.00
                                                            [object_name] => Predmer uvoz
                                                        )

                                                )

                                        )

                                )

                            [group_value] => Array
                                (
                                    [objects] => Array
                                        (
                                            [23] => Array
                                                (
                                                    [value] => 0.00
                                                    [value_per_unit] => 
                                                    [quantity] => 
                                                )

                                        )

                                    [measurement_unit_name] => 
                                    [value_per_unit] => 
                                )

                            [objects_cnt] => 1
                        )

                )

            [items] => Array
                (
                )

            [group_value] => Array
                (
                    [objects] => Array
                        (
                            [23] => Array
                                (
                                    [value] => 0.00
                                    [value_per_unit] => 
                                    [quantity] => 
                                )

                        )

                    [measurement_unit_name] => 
                    [value_per_unit] => 
                )

            [objects_cnt] => 1
        )

)


 * 
 */


