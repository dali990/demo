<?php

class BuildingInterimBillReport extends SIMAReport
{
    protected $css_file_name = 'css.css';
    protected $margin_top = 45;
    protected $margin_bottom = 10;
    protected $include_page_numbering = false;
    
    private $object_order = [];

    protected function getModuleName()
    {
        return 'buildings';
    }
    
    protected function getHTMLContent()
    {
        $building_interim_bill = $this->params['building_interim_bill'];
        
        $this->download_name = $building_interim_bill . '.pdf';
        
        $exporting_pdf = $this->params['ACTION_EXPORT_PDF'] ?? null;
        
        $bill_of_quantity = $building_interim_bill->building_construction->bill_of_quantity;
        
        $bill = null;
        if(!empty($building_interim_bill->file->bill))
        {
            $bill = $building_interim_bill->file->bill;
        }
        
        $this->object_order = [];//Cuvamo redosled objekata
        foreach($bill_of_quantity->leaf_objects as $object)
        {
            $this->object_order[] = $object->id;
        }
        
        //Za debagovanje - koristi se za prikaz predmera u html-u
//        $bill_of_quantity_report = new BillOfQuantityReport([
//            'bill_of_quantity' => $building_interim_bill->building_construction->bill_of_quantity,
//            'building_interim_bill_id' => $building_interim_bill->id
//        ]);

        return $this->render('html', [
            'building_interim_bill' => $building_interim_bill,
            'curr_company' => Yii::app()->company,
            'exporting_pdf' => $exporting_pdf,
            'bill' => $bill,
            'bill_of_quantity' => $bill_of_quantity,
            'item_group_values' => $this->getItemGroupValues($bill_of_quantity),
            'root_group_values' => $this->getRootGroupItemTotal($bill_of_quantity),
//            'bill_of_quantity_report' => $bill_of_quantity_report->getHTML()
        ], true, false);
    }
    
    protected function getHTMLHeader() 
    {
        $building_interim_bill = $this->params['building_interim_bill'];
        $exporting_pdf = $this->params['ACTION_EXPORT_PDF'] ?? null;
        
        return $this->render('header', [
            'building_interim_bill' => $building_interim_bill,
            'curr_company' => Yii::app()->company,
            'exporting_pdf' => $exporting_pdf
        ], true, false);
    }
    
    protected function getHTMLFooter() 
    {
        $exporting_pdf = $this->params['ACTION_EXPORT_PDF'] ?? null;
        return $this->render('footer', [
            'exporting_pdf' => $exporting_pdf
        ], true, false);
    }
    //dohvata sve vrednost za first_level_groups
    private function getItemGroupValues(BillOfQuantity $bill_of_quantity) 
    {
        $item_groups = [];
        foreach($bill_of_quantity->first_level_groups as $item_group)
        {
            $ordered_item_to_objects = [];
            $item_groups[$item_group->id] = [
                'title' => $item_group->title, 
                'order' => $item_group->order
            ];
            foreach($item_group->item_group_to_objects_leafs as $item_group_to_object)
            {
                $item_groups[$item_group->id]['objects'][$item_group_to_object->object->id] = 
                    $this->getBuildingMeasurementSheetGroup($item_group->id, $item_group_to_object->object->id);
            }
            //Sortiranje objekata
            foreach ($this->object_order as $objec_id)
            {
                $ordered_item_to_objects[$objec_id] = $item_groups[$item_group->id]['objects'][$objec_id];
            }
            $item_groups[$item_group->id]['objects'] = $ordered_item_to_objects;
        }
        
        return $item_groups;
    }
    
    private function getRootGroupItemTotal(BillOfQuantity $bill_of_quantity) 
    {       
        $root_group_total = [];
        $ordered_item_group_to_objects_values = [];
        foreach($bill_of_quantity->root_group->item_group_to_objects_leafs as $item_group_to_object)
        {
            $root_group_total['objects'][$item_group_to_object->object->id] = 
                $this->getBuildingMeasurementSheetGroup($bill_of_quantity->root_group->id, $item_group_to_object->object->id);
        }
        //Sortiranje objekata
        foreach ($this->object_order as $objec_id)
        {
            $ordered_item_group_to_objects_values[$objec_id] = $root_group_total['objects'][$objec_id];
        }
        $root_group_total['objects'] = $ordered_item_group_to_objects_values;
        
        return $root_group_total;
    }
    
    private function getBuildingMeasurementSheet($item_id, $object_id) 
    {
        $building_interim_bill = $this->params['building_interim_bill']->id;
        $bms = BuildingMeasurementSheet::model()->findByAttributes([
            'building_interim_bill_id' => $building_interim_bill->id,
            'bill_of_quantity_item_id' => $item_id,
            'object_id' => $object_id
        ]);
        
        return $this->getBuildingMeasurementSheetItems($bms);
    }
    
    private function getBuildingMeasurementSheetGroup($item_group_id, $object_id) 
    {       
        $bmsg = BuildingMeasurementSheetGroup::model()->findByAttributes([
            'building_interim_bill_id' => $this->params['building_interim_bill']->id,
            'bill_of_quantity_item_group_id' => $item_group_id,
            'object_id' => $object_id
        ]);
        
        return $this->getBuildingMeasurementSheetItems($bmsg);
    }
    
    private function getBuildingMeasurementSheetItems($bms) 
    {
        $default_value = SIMABigNumber::fromFloat(0.00, 2)->getValue();
        $bms_items = [
            'total_value_done' => $default_value
        ];
        if(!empty($bms))
        {
            $bms_items['total_value_done'] = $bms->total_value_done->round(2)->toString();
        }
        
        return $bms_items;
    }
    
}

