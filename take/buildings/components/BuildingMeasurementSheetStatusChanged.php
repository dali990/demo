<?php

class BuildingMeasurementSheetStatusChanged extends SIMAWork
{
    public function run(array $measurement_sheet_groups)
    {
        $applied_sheet_group_ids = [];
        $different_interim_bill_ids = [];

        foreach ($measurement_sheet_groups as $measurement_sheet_group)
        {
            if (!in_array($measurement_sheet_group->id, $applied_sheet_group_ids))
            {
                $measurement_sheet_group->calcValueDoneRecursive();
                array_push($applied_sheet_group_ids, $measurement_sheet_group->id);
            }
            if (empty($different_interim_bill_ids[$measurement_sheet_group->building_interim_bill->id]))
            {
                $different_interim_bill_ids[$measurement_sheet_group->building_interim_bill->id] = $measurement_sheet_group->building_interim_bill;
            }
        }
        
        foreach ($different_interim_bill_ids as $different_interim_bill_id => $different_interim_bill)
        {
            $different_interim_bill->checkStatus();
        }
    }
}
