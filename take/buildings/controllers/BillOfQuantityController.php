<?php

class BillOfQuantityController extends SIMAController
{
    public function actionViewBillOfQuantity($bill_of_quantity_id)
    {
        $bill_of_quantity = BillOfQuantity::model()->findByPkWithCheck($bill_of_quantity_id);

        $html = $this->renderPartial('bill_of_quantity', [
            'bill_of_quantity' => $bill_of_quantity
        ], true, false);
        
        $this->respondOK([
            'html' => $html
        ]);
    }

    public function actionSaveBillOfQuantityItemColumnValue($bill_of_quantity_item_id, $column, $new_value, $object_id = null)
    {
        $new_value2 = SIMABigNumber::fromString($new_value);
        if (empty($object_id))
        {
            $bill_of_quantity_item = BillOfQuantityItem::model()->findByPkWithCheck($bill_of_quantity_item_id);
            $bill_of_quantity_item->$column = $new_value2;
            $bill_of_quantity_item->save();
        }
        else
        {
            $bill_of_quantity_item_to_object = BillOfQuantityItemToObject::model()->findByAttributes([
                'item_id' => $bill_of_quantity_item_id,
                'object_id' => $object_id
            ]);
            if (!empty($bill_of_quantity_item_to_object))
            {
                $bill_of_quantity_item_to_object->$column = $new_value2;
                $bill_of_quantity_item_to_object->save();
            }
        }
        
        $this->respondOK();
    }
    
    public function actionSaveGroupItems() 
    {
        $data = $this->setEventHeader();
        $max_while_cycle_exec_time_seconds = ini_get('max_execution_time');
        $group_id = $this->filter_input($data, 'group_id');
        $items = $this->filter_input($data, 'client_data');
        
        $items_cnt = count($items);
        $validated_items = [];
        $saved_items = [];
        $saved_mus = [];
        $error_msgs = [];
        $i = 1;
        try
        {
            gc_enable();
            foreach ($items as $item)
            {
                $percent = round(($i/$items_cnt)*50, 0, PHP_ROUND_HALF_DOWN);
                $this->sendUpdateEvent($percent);
                
                //Dohvatanje, odnosno kreiranje modela za mernu jedinicu
                $measurement_unit = MeasurementUnit::model()->findByAttributes([
                    'name' => $item['measurement_unit']
                ]);
                $mu_validation_passed = true;
                set_time_limit($max_while_cycle_exec_time_seconds);
                
                if(empty($measurement_unit))
                {
                    $mu = new MeasurementUnit();
                    $mu->name = $item['measurement_unit'];
                    if($mu->validate() === false)
                    {
                        $mu_validation_passed = false;
                        $item_name = $item['name'];
                        if(strlen($item_name)>100)
                        {
                            $item_name = substr($item['name'], 0, 100)."... ";
                        }
                        $item_name = SIMAMisc::convertCyrillicToLatin($item_name);
                        
                        $error_msgs[] = "<b>- ".$item_name."</b><br>{$mu->modelLabel()}<br>".SIMAHtml::showErrorsInHTML($mu, '');
                    }
                    else
                    {
                        $mu->save();
                        $saved_mus[] = $mu;
                        $measurement_unit_id = $mu->id;
                    }
                }
                else
                {
                    $measurement_unit_id = $measurement_unit->id;
                }
                gc_collect_cycles();
                //Kreiranje modela za unos stavke
                if($mu_validation_passed)
                {
                    $item_model = new BillOfQuantityItem();
                    $item_model->group_id = $group_id;
                    $item_model->name = $item['name'];
                    if(!empty($item['value_per_unit']))
                    {
                        $item_model->value_per_unit = SIMABigNumber::fromString(preg_replace("/[^0-9.]/", "", $item['value_per_unit']));
                    }
                    $item_model->measurement_unit_id = $measurement_unit_id;
                    if($item_model->validate())
                    {
                        $validated_items[] = ['model'=>$item_model, 'item' => $item];
                    }
                    else
                    {
                        $item_name = $item['name'];
                        if(strlen($item_name)>100)
                        {
                            $item_name = substr($item['name'], 0, 100)."... ";
                        }
                        $item_name = SIMAMisc::convertCyrillicToLatin($item_name);
                        $error_msgs[] = "<b>- ".$item_name."</b><br>{$item_model->modelLabel()}<br>".SIMAHtml::showErrorsInHTML($item_model, '');
                    }
                    
                }
                $i++;
            }
            //Prikaz gresaka
            if(count($error_msgs) > 0)
            {
                array_unshift($error_msgs, "<br>".Yii::t('BuildingsModule.ImportBillOfQuantity', 'ErrorsInTheFollowingItems').":");
                throw new SIMAWarnException(implode("<br>", $error_msgs));
            }
            //Cuvanje stavki
            else
            {
                $i = 1;
                $validated_items_cnt = count($validated_items);
                foreach ($validated_items as $key => $validated_boq_item) 
                {
                    $percent = round(($i/$validated_items_cnt)*50, 0, PHP_ROUND_HALF_DOWN) + 50;
                    $this->sendUpdateEvent($percent);
                    set_time_limit($max_while_cycle_exec_time_seconds);
                    
                    $item = $validated_boq_item['item']; //Prosledjena stavka iz importa
                    $validated_boq_item['model']->save(false);
                    $saved_items[] = $validated_boq_item['model'];
                    
                    unset($validated_items[$key]);
                    gc_collect_cycles();
                    
                    foreach ($item['objects'] as $object_id => $object) 
                    {
                        $bill_of_quantity_item_to_object = BillOfQuantityItemToObject::model()->findByAttributes([
                            'item_id' => $validated_boq_item['model']->id,
                            'object_id' => $object_id
                        ]);
                        //Ako je pronasao stavku postavlja joj se kolicina
                        if (!empty($bill_of_quantity_item_to_object))
                        {
                            //Kolicina
                            if(!empty($object['quantity']))
                            {
                                $bill_of_quantity_item_to_object->quantity = SIMABigNumber::fromString(preg_replace("/[^0-9.]/", "", $object['quantity']));
                            }
                            //Jedinicna cena na nivou objekta
                            if(!empty($object['object_value_per_unit']))
                            {
                                $bill_of_quantity_item_to_object->value_per_unit = SIMABigNumber::fromString(preg_replace("/[^0-9.]/", "", $object['object_value_per_unit']));
                            }
                            else
                            {
                                $bill_of_quantity_item_to_object->value_per_unit = SIMABigNumber::fromString(preg_replace("/[^0-9.]/", "", $item['value_per_unit']));
                            }
                            if($bill_of_quantity_item_to_object->validate())
                            {
                                $bill_of_quantity_item_to_object->calc_value = false;
                                $bill_of_quantity_item_to_object->save(false);
                                unset($bill_of_quantity_item_to_object);
                                gc_collect_cycles();                               
                            }
                            //Brisemo sve prethodno unete stavke i izbacujem poruku greske
                            else
                            {
                                $item_name = $item['name'];
                                if(strlen($item_name)>100)
                                {
                                    $item_name = substr($item['name'], 0, 100)."... ";
                                }
                                $item_name = SIMAMisc::convertCyrillicToLatin($item_name);
                                $error_msgs[] = "<b>- ".$item_name."</b><br>{$bill_of_quantity_item_to_object->modelLabel()} - {$bill_of_quantity_item_to_object->object->name}<br>".SIMAHtml::showErrorsInHTML($bill_of_quantity_item_to_object, '');
                                array_unshift($error_msgs, "<br>".Yii::t('BuildingsModule.ImportBillOfQuantity', 'ErrorsInTheFollowingItems').":");
                                throw new SIMAWarnException(implode("<br>", $error_msgs));
                            }
                        }
                    }  
                    $i++;
                }
                $group = BillOfQuantityItemGroup::model()->findByPkWithCheck($group_id);
                foreach ($group->bill_of_quantity->leaf_objects as $leaf_object)
                {
                    $leaf_object->calcValueRecursive();
                    gc_collect_cycles();
                }
            }
        }
        catch (Litipk\BigNumbers\Errors\NaNInputError $msg) 
        {
            $this->deleteSavedModels($saved_items);
            $this->deleteSavedModels($saved_mus);
            throw new SIMAWarnException(Yii::t('BuildingsModule.ImportBillOfQuantity', 'ErrorWhileSavingUnitPricesAndQuantities'));
        }
        catch (SIMAWarnException $msg) 
        {
            $this->deleteSavedModels($saved_items);
            $this->deleteSavedModels($saved_mus);
            throw new SIMAWarnException($msg->getMessage());
        }
        catch (Exception $msg) 
        {
            $this->deleteSavedModels($saved_items);
            $this->deleteSavedModels($saved_mus);
            throw new SIMAException($msg->getMessage());
        }   
        $this->sendEndEvent();
    }
    
    private function deleteSavedModels($saved_models) 
    {
        foreach ($saved_models as $saved_model) 
        {
            $saved_model->delete();
        }
    }
    
    public function actiongetBillOfQuantityInitDataForClone($bill_of_quantity_id)
    {
        $bill_of_quantity = BillOfQuantity::model()->findByPkWithCheck($bill_of_quantity_id);
        
        $user_ids = [];
        foreach ($bill_of_quantity->users as $user)
        {
            array_push($user_ids, [
                'class' => '_visible',
                'id' => $user->id,
                'display_name' => $user->DisplayName
            ]);
        }

        $init_data = [
            'name' => $bill_of_quantity->name,
            'theme_id' => $bill_of_quantity->theme_id,
            'has_object_value_per_unit' => $bill_of_quantity->has_object_value_per_unit,
            'is_private' => $bill_of_quantity->is_private,
            'comment' => $bill_of_quantity->comment,
            'owner_id' => $bill_of_quantity->owner_id,
            'users' => [
                'ids' => [
                    'ids' => $user_ids
                ]
            ]
        ];

        $this->respondOK([
            'init_data' => $init_data
        ]);
    }
    
    public function actionCloneBillOfQuantity()
    {
        $data = $this->setEventHeader();
        
        $from_bill_of_quantity_id = $this->filter_input($data, 'from_bill_of_quantity_id');
        $to_bill_of_quantity_id = $this->filter_input($data, 'to_bill_of_quantity_id');
        
        $from_bill_of_quantity = BillOfQuantity::model()->findByPkWithCheck($from_bill_of_quantity_id);
        $to_bill_of_quantity = BillOfQuantity::model()->findByPkWithCheck($to_bill_of_quantity_id);
        
        $this->sendUpdateEvent(10);
        
        $from_bill_of_quantity->cloneToBillOfQuantity($to_bill_of_quantity);
        
        $this->sendUpdateEvent(100);

        $this->sendEndEvent();
    }
}
