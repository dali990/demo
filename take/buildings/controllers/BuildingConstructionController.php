<?php

class BuildingConstructionController extends SIMAController
{
    public function actionBuildingInterimBills($building_construction_id)
    {   
        $building_construction = BuildingConstruction::model()->findByPkWithCheck($building_construction_id);
        
        $html = $this->renderPartial('building_interim_bills', [
            'building_construction' => $building_construction
        ], true, false);

        $this->respondOK([
            'html' => $html
        ]);
    }
    
    public function actionBuildingSiteDiaries($building_construction_id)
    {
        $building_construction = BuildingConstruction::model()->findByPkWithCheck($building_construction_id);
        
        $html = $this->renderPartial('building_site_diaries', [
            'building_construction' => $building_construction
        ], true, false);

        $this->respondOK([
            'html' => $html
        ]);
    }

    public function actionSaveBuildingSiteDiaryColumnValue($building_site_diary_id, $column_name, $new_value = null)
    {
        $building_site_diary = BuildingSiteDiary::model()->findByPkWithCheck($building_site_diary_id);
        $building_site_diary->$column_name = $new_value;
        $building_site_diary->save();

        $this->respondOK();
    }
    
    public function actionBuildingConstructionTab($theme_id, $without_bill_of_quantity_id = null)
    {
        $theme = Theme::model()->findByPkWithCheck($theme_id);

        $html = $this->renderPartial('building_construction_tab', [
            'theme' => $theme,
            'without_bill_of_quantity_id' => $without_bill_of_quantity_id
        ], true, false);

        $this->respondOK([
            'html' => $html
        ]);
    }
    
    public function actionSetBuildingInterimBillAsFinal($building_interim_bill_id)
    {
        $building_interim_bill = BuildingInterimBill::model()->findByPkWithCheck($building_interim_bill_id);
        $building_interim_bill->is_final = true;
        $building_interim_bill->save();
        
        $this->respondOK();
    }

    public function actionGetBuildingMeasurementSheet($building_interim_bill_id, $bill_of_quantity_item_id, $object_id)
    {
        $building_measurement_sheet = BuildingMeasurementSheet::model()->findByAttributes([
            'building_interim_bill_id' => $building_interim_bill_id,
            'bill_of_quantity_item_id' => $bill_of_quantity_item_id,
            'object_id' => $object_id
        ]);
        
        if (empty($building_measurement_sheet))
        {
            $building_measurement_sheet = new BuildingMeasurementSheet();
            $building_measurement_sheet->building_interim_bill_id = $building_interim_bill_id;
            $building_measurement_sheet->bill_of_quantity_item_id = $bill_of_quantity_item_id;
            $building_measurement_sheet->object_id = $object_id;
            $building_measurement_sheet->written_by_user_id = Yii::app()->user->id;
            $building_measurement_sheet->save();
        }
        else if (empty($building_measurement_sheet->written_by_user_id))
        {
            $building_measurement_sheet->written_by_user_id = Yii::app()->user->id;
            $building_measurement_sheet->save();
        }
        
        $this->respondOK([
            'building_measurement_sheet_id' => $building_measurement_sheet->id
        ]);
    }
    
    public function actionBuildingInterimBillExportPdf()
    {
        $data = $this->setEventHeader();
        
        $building_interim_bill_id = $this->filter_input($data, 'building_interim_bill_id');
        
        $building_interim_bill = BuildingInterimBill::model()->findByPkWithCheck($building_interim_bill_id);
        
        $this->sendUpdateEvent(10);
        
        $building_interim_bill_report = new BuildingInterimBillReport(['building_interim_bill' => $building_interim_bill, 'ACTION_EXPORT_PDF'=>true]);
        $temporary_file_bib = $building_interim_bill_report->getPDF();
        
        $bill_of_quantity_report = new BillOfQuantityReport([
            'bill_of_quantity' => $building_interim_bill->building_construction->bill_of_quantity, 
            'building_interim_bill_id' => $building_interim_bill->id,
            'ACTION_EXPORT_PDF'=>true
        ]);
        $temporary_file_boq = $bill_of_quantity_report->getPDF();
        
        $document_pages = [
            [
                "temp_name" => $temporary_file_bib->getFileName(),
                "page_number" => 1 //PRIVREMENA SITUACIJA
            ],
            [
                "temp_name" => $temporary_file_boq->getFileName(),
                "page_number" => "" //PREDMER
            ],
            [
                "temp_name" => $temporary_file_bib->getFileName(),
                "page_number" => 2 //ZBIRNA REKAPITULACIJA
            ],
            [
                "temp_name" => $temporary_file_bib->getFileName(),
                "page_number" => 3 //PREGLED ISPOSTAVLJENIH SITUACIJA
            ],
        ];
        
        $resultTempFile = Yii::app()->stapler->createTempPDF($document_pages);
        
        $this->sendUpdateEvent(100);
        
        $this->sendEndEvent([
            'filename' => $resultTempFile->getFileName(),
            'downloadname' => $building_interim_bill_report->getDownloadName()
        ]);
    }
    
    public function actionBuildingInterimBillGetReport() 
    {
        $data = $this->setEventHeader();
        
        $building_interim_bill_id = $this->filter_input($data, 'building_interim_bill_id');
        
        $building_interim_bill = BuildingInterimBill::model()->findByPk($building_interim_bill_id);
        
        $building_interim_bill_report_html = '';
        if (!empty($building_interim_bill))
        {
            $building_interim_bill_report = new BuildingInterimBillReport(['building_interim_bill' => $building_interim_bill]);
            $building_interim_bill_report_html = $building_interim_bill_report->getHTML();
        }
        
        $this->sendEndEvent([
            'report_html' => $building_interim_bill_report_html
        ]);
    }
    
    public function actionBuildingMeasurementSheetExportPdf()
    {
        $data = $this->setEventHeader();
        
        $building_measurement_sheet_id = $this->filter_input($data, 'building_measurement_sheet_id');
        
        $building_measurement_sheet = BuildingMeasurementSheet::model()->findByPkWithCheck($building_measurement_sheet_id);
        
        $this->sendUpdateEvent(10);
        
        $bms_report = new BuildingMeasurementSheetReport(["building_measurement_sheet" => $building_measurement_sheet]);
        $temporary_file = $bms_report->getPDF();        

        $this->sendUpdateEvent(100);
        
        $this->sendEndEvent([
            'filename' => $temporary_file->getFileName(),
            'downloadname' => $bms_report->getDownloadName()
        ]);
    }
    
    public function actionSetBuildingInterimBillStatus($building_interim_bill_id, $status)
    {
        $building_interim_bill = BuildingInterimBill::model()->findByPkWithCheck($building_interim_bill_id);
        
        if (
                $building_interim_bill->status === BuildingInterimBill::$DELIVERED_MEASUREMENT_SHEETS &&
                $status === BuildingInterimBill::$VALIDATED_MEASUREMENT_SHEETS
            )
        {
            $building_interim_bill->setAllMeasurementSheetsValidated(Yii::app()->user->model, true);
        }
        
        if (
                $building_interim_bill->status === BuildingInterimBill::$VALIDATED_MEASUREMENT_SHEETS &&
                $status === BuildingInterimBill::$DELIVERED_MEASUREMENT_SHEETS
           )
        {
            $building_interim_bill->setAllMeasurementSheetsValidated(Yii::app()->user->model, false);
        }
        
        $building_interim_bill->status = $status;
        $building_interim_bill->save();
        
        $this->respondOK();
    }
    
    public function actionConfirmAllZeroMeasurementSheets($building_interim_bill_id)
    {
        $zero_sheets = BuildingMeasurementSheet::model()->findAll([
            'model_filter' => [
                'AND',
                [
                    'building_interim_bill' => [
                        'ids' => $building_interim_bill_id
                    ],
                    'scopes' => 'onlyZero'
                ],
                [
                    'OR',
                    ['written' => false],
                    ['confirmed' => false]
                ]
            ]
        ]);

        foreach ($zero_sheets as $zero_sheet)
        {
            $zero_sheet->written = true;
            $zero_sheet->written_by_user_id = Yii::app()->user->id;
            $zero_sheet->written_timestamp = 'now()';
            $zero_sheet->confirmed = true;
            $zero_sheet->confirmed_by_user_id = Yii::app()->user->id;
            $zero_sheet->confirmed_timestamp = 'now()';
            $zero_sheet->save();
        }
        
        $this->respondOK();
    }
}
