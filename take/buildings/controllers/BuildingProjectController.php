<?php

/**
 * Description of BuildingProjectController
 *
 * @author goran-set
 */
class BuildingProjectController extends SIMAController
{
    /**
     * 
     * @param integer $id ID projekta koji se prikazuje
     * @respondOK JSON
        [
            'html' => $html,  -- HTML za prikaz
            'title' => $title -- naziv tab-a
        ]
     */
    public function actionIndex($id)
    {
        //MilosS(27.6.2016) - prikazuju se samo osnovni projekti, ostali sluze za razvrstavanje fajlova
        $building_project = BuildingProject::model()->baseProjects()->findByPkWithCheck($id);
        
        $selector = $this->filter_post_input('selector', false);
        if (empty($selector))
        {
            $selector = [];
        }

        $default_selected_tab = [];
        $default_selected = array_shift($selector);
        if (!empty($default_selected) && isset($default_selected['simaTabs']))
        {
            $default_selected_tab = [$default_selected['simaTabs'], 'selector'=>$selector];
        }
        
        $html = $this->widget('SIMADefaultLayout', [
            'model'=>$building_project,
            'default_selected_tab'=>$default_selected_tab
        ], true);
        
        $this->respondOK([
            'html' => $html,
            'title' => $building_project->DisplayName
        ]);
    }

    public function actionAddResponsibleDesignersDecision($building_project_id, $file_id)
    {
        $building_project = ModelController::GetModel('BuildingProject', $building_project_id);
        
        $building_project->responsible_designers_decision_id = $file_id;
        $building_project->save();
        
        $this->respondOK();
    }
    
    public function actionAddResponsibleDesigner($building_project_id, $ptwl_id)
    {
        $building_project = ModelController::GetModel('BuildingProject', $building_project_id);
        
        $building_project->responsible_designer_id = $ptwl_id;
        $building_project->save();
        
        $this->respondOK();
    }
    
    public function actionGenerateResponsibleDesignersDecision($building_project_id)
    {
        $buildingProject = ModelController::GetModel('BuildingProject', $building_project_id);
        
        BuildingProjectComponent::GenerateResponsibleDesignersDecision($buildingProject);
        
        $this->respondOK();
    }
    
    public function actionGenerateResponsibleDesignersSubDecision($building_project_id)
    {
        $buildingProject = ModelController::GetModel('BuildingProject', $building_project_id);
        
        BuildingProjectComponent::GenerateResponsibleDesignersSubDecision($buildingProject);
        
        $this->respondOK();
    }
    
    public function actionAddAdditionalContent($building_project_id)
    {
        $buildingProject = ModelController::GetModel('BuildingProject', $building_project_id);
        
        BuildingProjectComponent::GenerateAddAdditionalContent($buildingProject);
        
        $this->respondOK([], [$buildingProject]);
    }
    
    /**
     * Izlistava delove projekta
     * @param string $parent_code
     * @throws SIMAWarnException
     */
    public function actionListContent($parent_code = null)
    {
        $params = $this->filter_post_input('params');
        $item_data = $this->filter_post_input('item_data', false);
        $id = $params['id'];//$id osnovnog projekta
        
        $building_project = BuildingProject::model()->findByPkWithCheck($id);
        $bpc = new BuildingProjectContent();
        $building_project->attachBehavior('BPC', $bpc);
        
        $subtree = $building_project->getContent($parent_code);
        
        
        
        $this->respondOK([
            'subtree' => $subtree
        ]);
        
    }
    /**
     * Izlistava delove projekta
     * @param string $parent_code
     * @throws SIMAWarnException
     */
    public function actionListContentWithModification($parent_code = null)
    {
        $params = $this->filter_post_input('params');
        $item_data = $this->filter_post_input('item_data', false);
        $id = $params['id'];//$id osnovnog projekta
        
        $building_project = BuildingProject::model()->findByPkWithCheck($id);
        $bpc = new BuildingProjectContent(true);
        $building_project->attachBehavior('BPC', $bpc);
        
        if (isset($item_data['action']))
        {
            $subtree = [];
            
//            $building_project->doAction($item_data['action']);
            switch ($item_data['action']) 
            {
                case 'ADD_TNG':
                    //napravi pod projekte
                    $building_project = BuildingProject::model()->findByPkWithCheck($parent_code);
                    BuildingProjectComponent::GenerateAddAdditionalContent($building_project);

                    //osvezi projekat
//                    $base_project = BuildingProject::model()->findByPkWithCheck($id);
                    error_log($building_project->id);
                    $this->raiseUpdateModels($building_project);
//                    $this->raiseAction('sima.buildings.reloadTree', [$params['tree_id']]);
////                        $this->respondOK([
////                            'subtree' => [],
////                        ],[$base_project]);
//                    
                    break;
                case 'ADD_BOOK':
                    //napravi pod projekte
                    $building_project = BuildingProject::model()->findByPkWithCheck($parent_code);

                    $this->raiseAction('sima.model.form', [
                        'BuildingProject',
                        '',
                        [
                            'init_data' => [
                                'BuildingProject' => [
                                    'parent_id' => $building_project->id,
                                    'theme_id' => $building_project->theme_id,
                                    'building_project_type_id' => $building_project->building_project_type_id,
                                    'prefix' => $item_data['book_type'],
                                    'order' => $item_data['book_order'],
                                    'name' => $item_data['display_name']
                                ]
                            ]
                        ]
                    ]);
                    break;

                default: throw new SIMAWarnException('nema te akcije');
            }
        }
        else
        {
            $subtree = $building_project->getContent($parent_code);
        }
        
        
        $this->respondOK([
            'subtree' => $subtree
        ]);
        
    }
    
}
