<?php

class BookOfProjectsController extends SIMAController
{
    /**
     * Maticna knjiga projekata
     * @respondOK JSON
        [
            'html' => $html,  -- HTML za prikaz
            'title' => $title -- naziv tab-a
        ]
     */
    public function actionIndex()
    {
        $html = $this->renderPartial('index', [], true, true);
        
        $title = BuildingProjectGUI::modelLabel(true);
        
        $this->respondOK(array(
            'html' => $html,
            'title' => $title
        ));
    }
    
    public function actionAddNumber($building_project_id)
    {
        $project=  BuildingProject::model()->findByPk($building_project_id);
        
        if($project==null)
        {
            throw new Exception("Ne postoji gradjevinski projekat sa ID: ".$building_project_id);
        }
        
        $project->setNumber();
        
        $this->respondOK([], SIMAHtml::getTag($project));
    }
    
}