<?php

return [
    'BillOfQuantitiesItemGroups' => 'Bill of quantities item groups',
    'BillOfQuantityItemGroup' => 'Bill of quantity item group',
    'Title' => 'Title',
    'Parent' => 'Belongs to bill of quantity item group',
    'Note' => 'Note',
    'EditGroup' => 'Edit bill of quantity item group',
    'DeleteGroup' => 'Delete bill of quantity item group',
    'AddSubGroup' => 'Add bill of quantity item group',
    'AddItem' => 'Add bill of quantity item',
    'Total' => 'Total',
    'Quantity' => 'Quantity',
    'ValuePerUnit' => 'Value per unit',
    'Value' => 'Value',
    'Order' => 'Order',
    'LeaveOrderEmptyForAutomaticFill' => 'Leave empty for automatic fill',
    'Import' => 'Import bill of quantity group'
];