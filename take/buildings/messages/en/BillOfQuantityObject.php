<?php

return [
    'BillOfQuantityObject' => 'Object',
    'BillOfQuantitiesObjects' => 'Objects',
    'Name' => 'Name',
    'Parent' => 'Belongs to object',
    'Description' => 'Description'
];