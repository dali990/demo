<?php

return [
    'BuildingSiteDiaryWorkHourMechanization' => 'Mechanization',
    'BuildingSiteDiaryWorkHourMechanizations' => 'Mechanizations',
    'MechanizationType' => 'Mechanization type',
    'Mechanization' => 'Mechanization',
    'Mechanizations' => 'Mechanizations',
    'MechanizationCount' => 'Mechanization count',
    'MechanizationCountMustBeOne' => 'Mechanization count must be 1',
    'MechanizationCountMustBeEqualOrGreaterThanOne' => 'Mechanization count must be equal or greater than 1',
    'AddBuildingSiteDiaryWorkHourMechanization' => 'Add mechanization',
    'MechanizationMustBelongsToType' => 'Mechanization must belongs to type {mechanization_type} or its subtypes',
    'DeleteBuildingSiteDiaryWorkHourMechanization' => 'Delete mechanization',
    'EditBuildingSiteDiaryWorkHourMechanization' => 'Edit mechanization'
];