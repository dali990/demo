<?php

return [
    'BuildingSiteDiaryWorkHourWorker' => 'Worker',
    'BuildingSiteDiaryWorkHourWorkers' => 'Workers',
    'WorkerType' => 'Worker type',
    'Worker' => 'Worker',
    'Workers' => 'Workers',
    'WorkerCount' => 'Worker count',
    'Craftsman' => 'Craftsman',
    'Craftsmans' => 'Craftsmans',
    'TehnicalStaff' => 'Tehnical staff',
    'TehnicalStaffs' => 'Tehnical staffs',
    'Others' => 'Others',
    'WorkerCountMustBeOne' => 'Worker count must be 1',
    'WorkerCountMustBeEqualOrGreaterThanOne' => 'Worker count must be equal or greater than 1',
    'AddBuildingSiteDiaryWorkHourWorker' => 'Add worker',
    'DeleteBuildingSiteDiaryWorkHourWorker' => 'Delete worker',
    'EditBuildingSiteDiaryWorkHourWorker' => 'Edit worker',
    'TotalWorkers' => 'Total workers'
];