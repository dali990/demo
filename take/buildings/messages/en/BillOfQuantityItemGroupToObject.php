<?php

return [
    'BillOfQuantityItemGroupToObject' => 'Bill of quantity item group object',
    'BillOfQuantityItemGroupsToObjects' => 'Bill of quantities item groups objects',
    'ValueDone' => 'Value done',
    'QuantityDone' => 'Quantity done'
];