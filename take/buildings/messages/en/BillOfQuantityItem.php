<?php

return [
    'BillOfQuantitiesItems' => 'Bill of quantities items',
    'BillOfQuantityItem' => 'Bill of quantity item',
    'Name' => 'Position description',
    'Order' => 'Order',
    'MeasurementUnit' => 'Measurement unit',
    'Quantity' => 'Quantity',
    'ValuePerUnit' => 'Value per unit',
    'Value' => 'Value',
    'EditItem' => 'Edit bill of quantity item',
    'DeleteItem' => 'Delete bill of quantity item',
    'LeaveOrderEmptyForAutomaticFill' => 'Leave empty for automatic fill',
    'QuantityDone' => 'Quantity done',
    'PercentageDone' => 'Percentage done',
    'Note' => 'Note'
];