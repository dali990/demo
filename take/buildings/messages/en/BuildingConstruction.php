<?php

return [
    'BuildingConstruction' => 'Building construction',
    'BuildingConstructions' => 'Building constructions',
    'Constructor' => 'Constructor',
    'Investor' => 'Investor',
    'ResponsibleEngineer' => 'Responsible engineer',
    'StartDate' => 'Start date',
    'AddBuildingConstruction' => 'Convert the theme to building construction',
    'BillOfQuantityAlreadyHasBuildingConstruction' => 'The bill of quantity has already been assigned to another building construction',
    'BillOfQuantityObjectCanNotChange' => 'You can not change the bill of quantity because the building construction has building measurement sheets',
    'ConstructorMustBelongsToTheme' => 'Constructor must belongs to team of theme {theme_name}',
    'ConstructionBoss' => 'Construction boss',
    'BossAssistantsRemovedFromBuildingConstructionButStillAreInTheme' => 'The following users have been removed from the building construction "{building_construction}", but still remain as members of the theme team: {removed_boss_assistants}',
];