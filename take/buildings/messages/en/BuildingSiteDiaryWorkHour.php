<?php

return [
    'BuildingSiteDiaryWorkHour' => 'Work hour',
    'BuildingSiteDiaryWorkHours' => 'Work hours',
    'StartDateTime' => 'Start',
    'EndDateTime' => 'End',
    'Note' => 'Note',
    'AddBuildingSiteDiaryWorkHour' => 'Add work hour',
    'StartDateTimeMustBeAtDate' => 'Start must be at date {date}',
    'EndDateTimeMustBeGreaterThanStartDateTime' => 'End must be greater than start',
    'From' => 'From',
    'To' => 'to',
    'DeleteBuildingSiteDiaryWorkHour' => 'Delete work hour',
    'EditBuildingSiteDiaryWorkHour' => 'Edit work hour'
];