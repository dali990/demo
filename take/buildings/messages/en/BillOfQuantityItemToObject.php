<?php

return [
    'BillOfQuantityItemToObject' => 'Bill of quantity item object',
    'BillOfQuantitiesItemsToObjects' => 'Bill of quantities items objects',
    'ValueDone' => 'Value done',
    'QuantityDone' => 'Quantity done'
];