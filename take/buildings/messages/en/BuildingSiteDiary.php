<?php

return [
    'BuildingSiteDiary' => 'Site diary',
    'BuildingSiteDiaries' => 'Site diaries',
    'Constructor' => 'Constructor',
    'Date' => 'Date',
    'WorkDescription' => 'Work description',
    'Remarks' => 'Remarks',
    'DiaryCreatorConfirmed' => 'Diary creator confirmed',
    'ResponsibleEngineerConfirmed' => 'Responsible engineer confirmed',
    'SupervisorConfirmed' => 'Supervisor confirmed',
    'AddBuildingSiteDiaryForConstructor' => 'Add site diary for constructor',
    'DeleteBuildingSiteDiary' => 'Delete site diary',
    'EditWorkDescription' => 'Switch edit',
    'EditRemarks' => 'Switch edit',
    'BuildingSiteDiaryDocumentTypeId' => 'Document type for site diaries',
    'ConstructorMustBelongsToTheme' => 'Constructor must belongs to team of theme {theme_name}',
    'OpenBuildingSiteDiary' => 'Open site diary in dialog',
    'EmptyWorkDescription' => 'No work description',
    'EmptyRemarks' => 'No remarks',
    'Diary' => 'Diary'
];