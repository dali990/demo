<?php

return [
    'BillOfQuantityItemValueChanged' => 'Value changed, do you want to save it?',
    'ImportBillOfQuantityGroup' => 'Bill of quantity group import',
    'ErrorWhileSavingUnitPricesAndQuantities' => 'Error while saving unit prices and quantities. Check the numbers.',
    'ImportBillOfQuantityMeasurementUnit' => 'Measurement unit',
    'ImportBillOfQuantityQuantity' => 'Quantity',
    'ImportBillOfQuantityName' => 'Position description',
    'ImportBillOfQuantityValuePerUnit' => 'Value per unit',
    'ImportBillOfQuantityValue' => 'Value',
    'BillOfQuantityEditModeOn' => 'Edit mode on',
    'BillOfQuantityEditModeOff' => 'Edit mode off',
    'MeasurementSheetUnWritten' => 'Measurement sheet is not written',
    'MeasurementSheetUnConfirmed' => 'Measurement sheet is written, but is not confirmed',
    'MeasurementSheetConfirmed' => 'Measurement sheet is confirmed',
    'MeasurementSheetGroupUnWritten' => 'Measurement sheets are not written',
    'MeasurementSheetGroupUnConfirmed' => 'Measurement sheets are written, but not confirmed',
    'MeasurementSheetGroupConfirmed' => 'Measurement sheets are confirmed'
];