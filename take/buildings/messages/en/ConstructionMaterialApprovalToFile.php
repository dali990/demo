<?php

return [
    'ConstructionMaterialApprovalToFileUniq'=>'Already exists pair {construction_material_approval_name} and {file_name}',
    'ConstructionMaterialApproval'=>'Construction material approval',
    'File'=>'File',
    'ConstructionMaterialApprovalToFile'=>'Construction material approval atest'
];

