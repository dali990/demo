<?php

return [
    'BuildingMeasurementSheetItem' => 'Building measurement sheet item',
    'BuildingMeasurementSheetItems' => 'Building measurement sheet items',
    'Calculation' => 'Calculation',
    'Amount' => 'Amount',
    'CalculationIsNotEqualToAmount' => 'Calculation does not match amount {amount}',
    'ParseCalculationError' => 'Calculation processing failed'
];