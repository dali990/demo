<?php

return [
    'File'=>'File',
    'Code'=>'Code',
    'Theme'=>'Theme',
    'MaterialName'=>'Material name',
    'Manufacturer'=>'Manufacturer',
    'Type'=>'Type',
    'UsedIn'=>'Used in',
    'PartOf'=>'Part of',
    'Location'=>'Location',
    'TehnicalCharactetistics'=>'Tehnical charactetistics',
    'Approved'=>'Approved',
    'ConstructionMaterialApproval'=>'Construction material approval',
    'ConstructionMaterialApprovals'=>'Construction material approvals',
    'WarehouseMaterial'=>'Warehouse material',
    'DrawingsList'=>'Drawings',
    'CodeAutomaticFill'=>'Will be filled automatically',
    'AtestsList'=>'Atests',
];

