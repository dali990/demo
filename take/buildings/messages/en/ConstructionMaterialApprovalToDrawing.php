<?php

return [
    'ConstructionMaterialApprovalToDrawingUniq'=>'Already exists pair {construction_material_approval_name} and {file_name}',
    'ConstructionMaterialApproval'=>'Construction material approval',
    'File'=>'File',
    'ConstructionMaterialApprovalToDrawing'=>'Construction material approval drawing'
];

