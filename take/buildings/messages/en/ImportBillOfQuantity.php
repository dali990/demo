<?php

return [
    'SelectDecimalsSeparator' => 'Select the decimals separator',
    'DeleteImportedContent' => 'Delete content',
    'Edit' => 'Edit',
    'Content' => 'Content',
    'Settings' => 'Settings',
    'ResetContentPreview' => 'Reset content preview',
    'SaveBOQ' => 'Save bill of quantity',
    'ErrorWhileSavingUnitPricesAndQuantities' => 'Error while saving unit prices and quantities. Check the numbers.',
    'ErrorsInTheFollowingItems' => 'Errors in the following items',
];

