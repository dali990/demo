<?php

return [
    'BillOfQuantitiesItemGroups' => 'Segmenti predmera',
    'BillOfQuantityItemGroup' => 'Segment predmera',
    'Title' => 'Naziv',
    'Parent' => 'Pripada segmentu predmera',
    'Note' => 'Napomena',
    'EditGroup' => 'Izmenite segment predmera',
    'DeleteGroup' => 'Obrišite segment predmera',
    'AddSubGroup' => 'Dodajte segment predmera',
    'AddItem' => 'Dodajte stavku predmera',
    'Total' => 'Ukupno',
    'Quantity' => 'Količina',
    'ValuePerUnit' => 'Jedinična cena',
    'Value' => 'Iznos',
    'Order' => 'Redni broj',
    'LeaveOrderEmptyForAutomaticFill' => 'Ostavite prazno za automatsko popunjavanje',
    'Import' => 'Uvezite segment'
];