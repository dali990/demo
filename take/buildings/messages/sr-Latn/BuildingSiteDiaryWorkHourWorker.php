<?php

return [
    'BuildingSiteDiaryWorkHourWorker' => 'Radnik',
    'BuildingSiteDiaryWorkHourWorkers' => 'Radnici',
    'WorkerType' => 'Tip radnika',
    'Worker' => 'Radnik',
    'Workers' => 'Radnici',
    'WorkerCount' => 'Broj radnika',
    'Craftsman' => 'Zanatlija',
    'Craftsmans' => 'Zanatlije',
    'TehnicalStaff' => 'Tehničko osoblje',
    'TehnicalStaffs' => 'Tehnička osoblja',
    'Others' => 'Ostali',
    'WorkerCountMustBeOne' => 'Broj radnika mora biti 1',
    'WorkerCountMustBeEqualOrGreaterThanOne' => 'Broj radnika mora biti jednak ili veći od 1',
    'AddBuildingSiteDiaryWorkHourWorker' => 'Dodajte radnika',
    'DeleteBuildingSiteDiaryWorkHourWorker' => 'Obrišite radnika',
    'EditBuildingSiteDiaryWorkHourWorker' => 'Izmenite radnika',
    'TotalWorkers' => 'Ukupno radnika'
];