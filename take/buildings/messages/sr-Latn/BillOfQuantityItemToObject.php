<?php

return [
    'BillOfQuantityItemToObject' => 'Objekat stavke predmera',
    'BillOfQuantitiesItemsToObjects' => 'Objekti stavke predmera',
    'ValueDone' => 'Urađeni iznos',
    'QuantityDone' => 'Urađena količina'
];