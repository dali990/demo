<?php

return [
    'BuildingSiteDiaryWorkHour' => 'Radno vreme',
    'BuildingSiteDiaryWorkHours' => 'Radna vremena',
    'StartDateTime' => 'Početak',
    'EndDateTime' => 'Kraj',
    'Note' => 'Beleška',
    'AddBuildingSiteDiaryWorkHour' => 'Dodajte radno vreme',
    'StartDateTimeMustBeAtDate' => 'Početak mora biti na dan {date}',
    'EndDateTimeMustBeGreaterThanStartDateTime' => 'Kraj mora biti veći od početka',
    'From' => 'Od',
    'To' => 'do',
    'DeleteBuildingSiteDiaryWorkHour' => 'Obrišite radno vreme',
    'EditBuildingSiteDiaryWorkHour' => 'Izmenite radno vreme'
];