<?php

return [
    'ConstructionMaterialApprovalToFileUniq'=>'Već postoji par {construction_material_approval_name} i {file_name}',
    'ConstructionMaterialApproval'=>'Odobrenje materijala za izgradnju',
    'File'=>'Fajl',
    'ConstructionMaterialApprovalToFile'=>'Atest za odobrenje materijala za izgradnju'
];

