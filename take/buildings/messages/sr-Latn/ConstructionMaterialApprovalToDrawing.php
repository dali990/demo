<?php

return [
    'ConstructionMaterialApprovalToDrawingUniq'=>'Već postoji par {construction_material_approval_name} i {file_name}',
    'ConstructionMaterialApproval'=>'Odobrenje materijala za izgradnju',
    'File'=>'Fajl',
    'ConstructionMaterialApprovalToDrawing'=>'Crtež za odobrenje materijala za izgradnju'
];

