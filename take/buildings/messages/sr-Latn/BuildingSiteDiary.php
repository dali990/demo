<?php

return [
    'BuildingSiteDiary' => 'Građevinski dnevnik',
    'BuildingSiteDiaries' => 'Građevinski dnevnici',
    'Constructor' => 'Izvođač',
    'Date' => 'Datum',
    'WorkDescription' => 'Opis radova',
    'Remarks' => 'Primedbe',
    'DiaryCreatorConfirmed' => 'Potvrda kreatora',
    'ResponsibleEngineerConfirmed' => 'Potvrda inženjera',
    'SupervisorConfirmed' => 'Potvrda nadzornog organa',
    'AddBuildingSiteDiaryForConstructor' => 'Dodajte građevinski dnevnik za izvođača',
    'DeleteBuildingSiteDiary' => 'Obrišite građevinski dnevnik',
    'EditWorkDescription' => 'Uključite izmene',
    'EditRemarks' => 'Uključite izmene',
    'BuildingSiteDiaryDocumentTypeId' => 'Tip dokumenta za građevinske dnevnike',
    'ConstructorMustBelongsToTheme' => 'Izvođač mora da pripada timu teme {theme_name}',
    'OpenBuildingSiteDiary' => 'Otvorite građevinski dnevnik u dialogu',
    'EmptyWorkDescription' => 'Nije unet opis radova',
    'EmptyRemarks' => 'Nema primedbi',
    'Diary' => 'Dnevnik'
];