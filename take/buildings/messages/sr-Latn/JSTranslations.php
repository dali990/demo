<?php

return [
    'BillOfQuantityItemValueChanged' => 'Vrednost je promenjena, da li želite da je sačuvate?',
    'ImportBillOfQuantityGroup' => 'Uvoz segmenta predmera',
    'ImportBillOfQuantityErrorWhileSavingUnitPricesAndQuantities' => 'Greška prilikom čuvanja jediničnih cena i količine. Proverite brojeve.',
    'ImportBillOfQuantityMeasurementUnit' => 'Jedinica mere',
    'ImportBillOfQuantityQuantity' => 'Količina',
    'ImportBillOfQuantityName' => 'Opis pozicije',
    'ImportBillOfQuantityValuePerUnit' => 'Jedinična cena',
    'ImportBillOfQuantityValue' => 'Iznos',
    'BillOfQuantityEditModeOn' => 'Uključite izmene',
    'BillOfQuantityEditModeOff' => 'Isključite izmene',
    'MeasurementSheetUnWritten' => 'Građevinski list nije napisan',
    'MeasurementSheetUnConfirmed' => 'Građevinski list je napisan, ali nije potvrđen',
    'MeasurementSheetConfirmed' => 'Građevinski list je potvrđen',
    'MeasurementSheetGroupUnWritten' => 'Građevinski listovi nisu napisani',
    'MeasurementSheetGroupUnConfirmed' => 'Građevinski listovi su napisani, ali nisu potvrđeni',
    'MeasurementSheetGroupConfirmed' => 'Građevinski listovi su potvrđeni'
];