<?php

return [
    'BuildingConstructionsToBossAssistants' => 'Pomoćnici šefa gradilišta',
    'BuildingConstructionToBossAssistant' => 'Pomoćnik šefa gradilišta',
    'BossAssistant' => 'Pomoćnik šefa'
];