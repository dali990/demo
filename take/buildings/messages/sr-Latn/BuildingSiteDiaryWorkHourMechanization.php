<?php

return [
    'BuildingSiteDiaryWorkHourMechanization' => 'Mehanizacija',
    'BuildingSiteDiaryWorkHourMechanizations' => 'Mehanizacije',
    'MechanizationType' => 'Tip mehanizacije',
    'Mechanization' => 'Mehanizacija',
    'Mechanizations' => 'Mehanizacije',
    'MechanizationCount' => 'Broj mehanizacija',
    'MechanizationCountMustBeOne' => 'Broj mehanizacija mora biti 1',
    'MechanizationCountMustBeEqualOrGreaterThanOne' => 'Broj mehanizacija mora biti jednak ili veći od 1',
    'AddBuildingSiteDiaryWorkHourMechanization' => 'Dodajte mehanizaciju',
    'MechanizationMustBelongsToType' => 'Mehanizacija mora da pripada tipu {mechanization_type} ili njegovim podtipovima',
    'DeleteBuildingSiteDiaryWorkHourMechanization' => 'Obrišite mehanizaciju',
    'EditBuildingSiteDiaryWorkHourMechanization' => 'Izmenite mehanizaciju'
];