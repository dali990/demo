<?php

return [
    'BillOfQuantityItemGroupToObject' => 'Objekat segmenta predmera',
    'BillOfQuantityItemGroupsToObjects' => 'Objekti segmenata predmera',
    'ValueDone' => 'Urađeni iznos',
    'QuantityDone' => 'Urađena količina'
];