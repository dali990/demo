<?php

return [
    'TemplateResponsibleDesignersDecision' => 'Sablon za resenje o odgovornim projektantima',
    'TemplateResponsibleDesignersDecisionDescription' => 'Koristi se za automatsko generisanje resenja za gradjevinske projekte',
    'TemplateResponsibleDesignerDecision' => 'Sablon za resenje o odgovornom projektantu',
    'TemplateResponsibleDesignerDecisionDescription' => 'Koristi se za automatsko generisanje resenja za gradjevinske projekte za knjige',
    'Buildings' => 'Građevina',
    'BuildingConstructionBossThemeCoordinator' => 'U izvođenju šefa gradilišta automatski popuniti sa koordinatorom teme'
];