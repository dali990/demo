<?php

return [
    'File'=>'Fajl',
    'Code'=>'Kod',
    'Theme'=>'Tema',
    'MaterialName'=>'Naziv materijala',
    'Manufacturer'=>'Proizvodjač',
    'Type'=>'Tip',
    'UsedIn'=>'Koristi se u',
    'PartOf'=>'Deo od',
    'Location'=>'Lokacija',
    'TehnicalCharactetistics'=>'Tehničke karakteristike',
    'Approved'=>'Odobren',
    'ConstructionMaterialApproval'=>'Odobrenje materijala za izgradnju',
    'ConstructionMaterialApprovals'=>'Odobrenja materijala za izgradnju',
    'WarehouseMaterial'=>'Magacinski materijal',
    'DrawingsList'=>'Crteži',
    'CodeAutomaticFill'=>'Automatski se popunjava',
    'AtestsList'=>'Atesti',
];

