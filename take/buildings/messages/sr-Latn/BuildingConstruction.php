<?php

return [
    'BuildingConstruction' => 'Izvođenje radova',
    'BuildingConstructions' => 'Izvođenja radova',
    'Constructor' => 'Izvođač',
    'Investor' => 'Investitor',
    'ResponsibleEngineer' => 'Odgovorni izvođač',
    'StartDate' => 'Početak radova',
    'AddBuildingConstruction' => 'Pretvorite temu u izvođenje radova',
    'BillOfQuantityAlreadyHasBuildingConstruction' => 'Predmer je već dodeljen drugom izvođenju radova',
    'BillOfQuantityObjectCanNotChange' => 'Ne možete menjati predmer jer izvođenje radova sadrži građevinske listove',
    'ConstructorMustBelongsToTheme' => 'Izvođač mora da pripada timu teme {theme_name}',
    'ConstructionBoss' => 'Šef gradilišta',
    'BossAssistantsRemovedFromBuildingConstructionButStillAreInTheme' => 'Sledeći korisnici su uklonjeni sa izvođenja "{building_construction}" ali se i dalje nalaze kao članovi tima teme: {removed_boss_assistants}',
];