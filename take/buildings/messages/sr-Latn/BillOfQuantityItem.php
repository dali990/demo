<?php

return [
    'BillOfQuantitiesItems' => 'Stavke predmera',
    'BillOfQuantityItem' => 'Stavka predmera',
    'Name' => 'Opis pozicije',
    'Order' => 'Redni broj',
    'MeasurementUnit' => 'Jedinica mere',
    'Quantity' => 'Količina',
    'ValuePerUnit' => 'Jedinična cena',
    'Value' => 'Iznos',
    'EditItem' => 'Izmenite stavku predmera',
    'DeleteItem' => 'Obrišite stavku predmera',
    'LeaveOrderEmptyForAutomaticFill' => 'Ostavite prazno za automatsko popunjavanje',
    'QuantityDone' => 'Urađena količina',
    'PercentageDone' => 'Urađen procenat',
    'Note' => 'Napomena'
];