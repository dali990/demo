<?php

return [
    'BillOfQuantityObject' => 'Objekat',
    'BillOfQuantitiesObjects' => 'Objekti',
    'Name' => 'Naziv',
    'Parent' => 'Pripada objektu',
    'Description' => 'Opis'
];