<?php

return [
    'BuildingMeasurementSheetItem' => 'Stavka lista građevinske knjige',
    'BuildingMeasurementSheetItems' => 'Stavke lista građevinske knjige',
    'Calculation' => 'Računica',
    'Amount' => 'Iznos',
    'CalculationIsNotEqualToAmount' => 'Računica ne odgovara iznosu {amount}',
    'ParseCalculationError' => 'Nije uspela obrada računice'
];