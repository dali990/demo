<?php

return [
    'BuildingProject' => 'Građevinski projekat',
    'BuildingProjects' => 'Građevinski projekti',
    'booksCheckBoxes' => 'Knjige',
    'Book0' => 'Glavna sveska',
    'Book1' => 'Arhitektura',
    'Book2' => 'Konstrukcija',
    'Book3' => 'Hidrotehničke instalacije',
    'Book4' => 'Elektroenergetske instalacije',
    'Book5' => 'Telekomunikacione i signalne instalacije',
    'Book6' => 'Mašinske instalacije',
    'Book7' => 'Tehnologija',
    'Book8' => 'Saobraćaj i saobraćajna signalizacija',
    'Book9' => 'Spoljno uređenje',
    'Book10' => 'Pripremni radovi',
    'CannotDeleteBook' => 'Ne mozete obrisati knjigu {book}',
    'ResponsibleDesignersDecision' => 'Rešenje o odgovornim projektantima',
    'GenerateResponsibleDesignersDecision' => 'Generisi',
    'Order' => 'Redni broj',
    'ResponsibleDesigner' => 'Odgovorni projektant (licenca)',
    'ResponsibleDesignerNotSet' => 'Nije postavljen odgovorni projektant za projekat</br>{building_project}',
    'ResponsibleDesignerDecision' => 'Resenje o odgovornom projektantu',
    'GenerateResponsibleDesignersSubDecision' => 'Generisi podresenja',
    'MustHaveDesignersDecisionToGenerateSubdecisions' => 'Mora postojati resenje za glavni projekat da bi se generisalo podresenje',
    'DecisionIsGeneratedFromBaseProject' => 'Resenje o odgovornom projektantu se inicijalno generise iz glavnog projekta',
    'ThemeNotSet' => 'Tema mora biti postavljena za glavni projekat',
    'NumberTaken' => 'Broj je zauzet',
//    'AddAdditionalContent' => 'Kreiraj dodatni sadrzaj',
//    'AddAdditionalContentTitle' => 'Text/Numerika/Grafika',
    'Book' => 'Knjiga',
    'Notebook' => 'Sveska',
    'Part' => 'Deo sveske',
    'BookPart' => 'Deo knjige',
    'Text' => 'Tekst',
    'text' => 'Tekst',
    'Numeric' => 'Numerika',
    'numeric' => 'Numerika',
    'Graphic' => 'Grafika',
    'graphic' => 'Grafika',
    'BuildingProjectType' => 'Vrsta projekta',
    'BuildingProjectTypes' => 'Vrste projekta',
    'BookChildPrefixInvalid' => 'Projekat sa prefiksom "Knjiga" može imati samo potprojekat projekat sa prefiksom "Sveska", "Deo knjige" ili "Tekst", "Numerika", "Grafika"!',
    'NotebookChildPrefixInvalid' => 'Projekat sa prefiksom "Sveska" može imati samo potprojekat sa prefiksom "Deo sveske" ili "Tekst", "Numerika", "Grafika"!',
    'PartsChildPrefixInvalid' => 'Projekti sa prefiksom "Deo sveske" ili "Deo knjige" može imati samo potprojekate sa prefiksima "Tekst", "Numerika", "Grafika"!'
];