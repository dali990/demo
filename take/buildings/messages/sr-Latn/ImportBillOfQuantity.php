<?php

return [
    'SelectDecimalsSeparator' => 'Izaberite separator decimala',
    'DeleteImportedContent' => 'Obrišite sadržaj',
    'Edit' => 'Izmena',
    'Content' => 'Sadržaj',
    'Settings' => 'Podešavanja',
    'ResetContentPreview' => 'Resetujte prikaz sadržaj',
    'SaveBOQ' => 'Sačuvajte predmer',
    'ErrorWhileSavingUnitPricesAndQuantities' => 'Greška prilikom čuvanja jediničnih cena i količine. Proverite brojeve.',
    'ErrorsInTheFollowingItems' => 'Greške u sledećim stavkama',
];

