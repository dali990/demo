/* global sima */

function SIMABuildings()
{
    this.addNumberFromBuildingProject = function(model_id)
    {
        sima.dialog.openYesNo("Da li zaista želite da dodelite broj ovom projektu?", sima.buildings.addNumberFromBuildingProject_YesFunc, null,{
            model_id:model_id
        });
    };
    
    this.addNumberFromBuildingProject_YesFunc = function(event)
    {
        var params = event.data;
        sima.dialog.close();
        sima.ajax.get('buildings/bookOfProjects/addNumber', {
             get_params:{
                 building_project_id:params.model_id
             },
             success_function:function(response){
             }
          });
    };

    this.addResponsibleDesignersDecision = function(building_project_id)
    {
        var params = {};
        params.view = "guiTable";
        sima.model.choose('Filing', function(data){
            var file_id = data[0].id;
            sima.ajax.get('buildings/buildingProject/addResponsibleDesignersDecision', {
                get_params:{
                    building_project_id : building_project_id,
                    file_id : file_id
                }
            });
        }, params);
    };

    this.addResponsibleDesigner = function(building_project_id)
    {
        var params = {};
        params.view = "guiTable";
        sima.model.choose('PersonToWorkLicense', function(data){
            var ptwl_id = data[0].id;
            sima.ajax.get('buildings/buildingProject/addResponsibleDesigner', {
                get_params:{
                    building_project_id : building_project_id,
                    ptwl_id : ptwl_id
                }
            });
        }, params);
    };

    this.generateResponsibleDesignersDecision = function(building_project_id)
    {
        sima.ajax.get('buildings/buildingProject/generateResponsibleDesignersDecision', {
            get_params:{
                building_project_id : building_project_id
            }
        });
    };

    this.generateResponsibleDesignersSubDecision = function(building_project_id)
    {
        sima.ajax.get('buildings/buildingProject/generateResponsibleDesignersSubDecision', {
            get_params:{
                building_project_id : building_project_id
            }
        });
    };

    this.addAdditionalContent = function(building_project_id)
    {
        sima.ajax.get('buildings/buildingProject/addAdditionalContent', {
            get_params:{
                building_project_id : building_project_id
            }
        });
    };
    
    this.reloadTree = function(tree_id)
    {
        $('#'+tree_id).simaTree2('reloadTree');
    };
    
    this.getBillOfQuantityTableCellsWidthComponent = function() {
        return {
            computed: {
                max_quantity_width: function() {
                    return this.getLargerValue(80, this.bill_of_quantity.scoped('max_quantity_for_object', {forObject: this.object.id}));
                },
                max_value_width: function() {
                    return this.getLargerValue(100, this.bill_of_quantity.scoped('max_value_for_object', {forObject: this.object.id}));
                },
                max_value_per_unit_for_object_width: function() {
                    return this.getLargerValue(80, this.bill_of_quantity.scoped('max_value_per_unit_for_object', {forObject: this.object.id}));
                },
                max_value_per_unit_width: function() {
                    return this.getLargerValue(80, this.bill_of_quantity.max_value_per_unit);
                },
                max_quantity_done_width: function() {
                    return this.getLargerValue(80, this.bill_of_quantity.scoped('max_quantity_done_for_object', {forObject: this.object.id}));
                },
                max_building_interim_bill_pre_quantity_done_width: function() {
                    if (typeof this.building_interim_bill === 'undefined' || this.building_interim_bill === null)
                    {
                        return 80;
                    }
                    return this.getLargerValue(80, this.building_interim_bill.scoped('max_pre_quantity_done_for_object', {forObject: this.object.id}));
                },
                max_building_interim_bill_quantity_done_width: function() {
                    if (typeof this.building_interim_bill === 'undefined' || this.building_interim_bill === null)
                    {
                        return 80;
                    }
                    return this.getLargerValue(80, this.building_interim_bill.scoped('max_quantity_done_for_object', {forObject: this.object.id}));
                },
                max_building_interim_bill_total_quantity_done_width: function() {
                    if (typeof this.building_interim_bill === 'undefined' || this.building_interim_bill === null)
                    {
                        return 80;
                    }
                    return this.getLargerValue(80, this.building_interim_bill.scoped('max_total_quantity_done_for_object', {forObject: this.object.id}));
                },
                max_building_interim_bill_pre_value_done_width: function() {
                    if (typeof this.building_interim_bill === 'undefined' || this.building_interim_bill === null)
                    {
                        return 100;
                    }
                    return this.getLargerValue(100, this.building_interim_bill.scoped('max_pre_value_done_for_object', {forObject: this.object.id}));
                },
                max_building_interim_bill_value_done_width: function() {
                    if (typeof this.building_interim_bill === 'undefined' || this.building_interim_bill === null)
                    {
                        return 100;
                    }
                    return this.getLargerValue(100, this.building_interim_bill.scoped('max_value_done_for_object', {forObject: this.object.id}));
                },
                max_building_interim_bill_total_value_done_width: function() {
                    if (typeof this.building_interim_bill === 'undefined' || this.building_interim_bill === null)
                    {
                        return 100;
                    }
                    return this.getLargerValue(100, this.building_interim_bill.scoped('max_total_value_done_for_object', {forObject: this.object.id}));
                }
            },
            methods: {
                getLargerValue: function(min_value, max_value) {
                    var _calc = Math.floor(Math.log10(max_value) * 14);

                    if (_calc > min_value)
                    {
                        min_value = _calc;
                    }

                    return min_value;
                }
            }
        };
    };
    
    this.buildingMeasurementSheetExportPdf = function(building_measurement_sheet_id) {
        sima.ajaxLong.start('buildings/buildingConstruction/buildingMeasurementSheetExportPdf', {
            data: {
                building_measurement_sheet_id: building_measurement_sheet_id
            },
            showProgressBar: $('body'),
            onEnd: function(response) {
                sima.temp.download(response.filename, response.downloadname);
            }
        });
    };
}
