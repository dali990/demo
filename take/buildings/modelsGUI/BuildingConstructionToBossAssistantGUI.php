<?php

class BuildingConstructionToBossAssistantGUI extends SIMAActiveRecordGUI
{   
    public function columnLabels()
    {
        return [
            'building_construction_id' => Yii::t('BuildingsModule.BuildingConstruction', 'BuildingConstruction'),
            'building_construction' => Yii::t('BuildingsModule.BuildingConstruction', 'BuildingConstruction'),
            'boss_assistant_id' => Yii::t('BuildingsModule.BuildingConstructionToBossAssistant', 'BossAssistant'),
            'boss_assistant' => Yii::t('BuildingsModule.BuildingConstructionToBossAssistant', 'BossAssistant')
        ] + parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return Yii::t('BuildingsModule.BuildingConstructionToBossAssistant', $plural ? 'BuildingConstructionsToBossAssistants': 'BuildingConstructionToBossAssistant');
    }
    
    public function modelForms()
    {
        return [
            'default' => [
                'type' => 'generic',
                'columns' => [
                    'building_construction_id' => ['relation', 'relName' => 'building_construction', 'add_button' => true],
                    'boss_assistant_id' => ['relation', 'relName' => 'boss_assistant', 'add_button' => true]
                ]
            ]
        ];
    }

    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default:
                return [
                    'columns' => ['building_construction', 'boss_assistant']
                ];
        }
    }
}
