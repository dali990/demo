<?php

class BillOfQuantityItemGroupGUI extends SIMAActiveRecordGUI
{
    public function columnLabels()
    {
        return [
            'title' => Yii::t('BuildingsModule.BillOfQuantityItemGroup', 'Title'),
            'bill_of_quantity_id' => Yii::t('BuildingsModule.BillOfQuantity', 'BillOfQuantity'),
            'bill_of_quantity' => Yii::t('BuildingsModule.BillOfQuantity', 'BillOfQuantity'),
            'parent_id' => Yii::t('BuildingsModule.BillOfQuantityItemGroup', 'Parent'),
            'parent' => Yii::t('BuildingsModule.BillOfQuantityItemGroup', 'Parent'),
            'note' => Yii::t('BuildingsModule.BillOfQuantityItemGroup', 'Note'),
            'quantity' => Yii::t('BuildingsModule.BillOfQuantityItemGroup', 'Quantity'),
            'value_per_unit' => Yii::t('BuildingsModule.BillOfQuantityItemGroup', 'ValuePerUnit'),
            'value' => Yii::t('BuildingsModule.BillOfQuantityItemGroup', 'Value'),
            'order' => Yii::t('BuildingsModule.BillOfQuantityItemGroup', 'Order')
        ] + parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return Yii::t('BuildingsModule.BillOfQuantityItemGroup', $plural ? 'BillOfQuantitiesItemGroups' : 'BillOfQuantityItemGroup');
    }
    
    public function modelForms()
    {
        $owner = $this->owner;
        
        $order_field = [
            'textField',
            'placeholder' => Yii::t('BuildingsModule.BillOfQuantityItemGroup','LeaveOrderEmptyForAutomaticFill')
        ];
        
        $parent_id_field = ['relation', 'relName' => 'parent', 'add_button' => true];
        if (!empty($owner->bill_of_quantity_id))
        {
            $parent_id_field['filters'] = [
                'bill_of_quantity' => [
                    'ids' => $owner->bill_of_quantity_id
                ]
            ];
            $parent_id_field['add_button'] = [
                'params' => [
                    'add_button' => [
                        'init_data' => [
                            BillOfQuantityItemGroup::class => [
                                'bill_of_quantity_id' => $owner->bill_of_quantity_id
                            ]
                        ]
                    ]
                ]
            ];
        }
        
        return [
            'default' => [
                'type' => 'generic',
                'columns' => [
                    'title' => 'textField',
                    'order' => $order_field,
                    'bill_of_quantity_id' => 'hidden',
                    'parent_id' => $parent_id_field,
                    'note' => 'textArea'
                ]
            ],
            'withoutParent' => [
                'type' => 'generic',
                'columns' => [
                    'title' => 'textField',
                    'order' => $order_field,
                    'bill_of_quantity_id' => 'hidden',
                    'parent_id' => 'hidden',
                    'note' => 'textArea'
                ]
            ]
        ];
    }
    
    public function guiTableSettings($type)
    {   
        switch ($type)
        {
            default:
                return [
                'columns' => [
                    'title', 'order', 'bill_of_quantity', 'parent', 'quantity', 'value_per_unit', 'value', 'note'
                ]
            ];
        }
    }
    
    public function vueProp(string $prop, array $scopes = []):array
    {
        $owner = $this->owner;

        switch ($prop)
        {
            case 'children':
                $prop_value = [
                    'TYPE' => 'MODEL_ARRAY',
                    'ARRAY' => SIMAMisc::getVueModelTags($owner->children)
                ];
                
                return [$prop_value, SIMAMisc::getModelTags($owner->children)];
            case 'items':
                $prop_value = [
                    'TYPE' => 'MODEL_ARRAY',
                    'ARRAY' => SIMAMisc::getVueModelTags($owner->items)
                ];
                
                return [$prop_value, SIMAMisc::getModelTags($owner->items)];
            case 'has_all_items_same_measurement_unit': 
                return [$owner->hasAllItemsSameMeasurementUnit(), $owner->getAllItemsModelTagsRecursive()];
            default: return parent::vueProp($prop, $scopes);
        }
    }
    
    public function vuePropValue($prop)
    {
        $owner = $this->owner;

        switch ($prop)
        {
            case 'curr_user_model_options': 
                return $owner->getModelOptions(Yii::app()->user->model);
            case 'value_per_unit': 
                return $owner->getApproximatelyValuePerUnit()->getValue();
            case 'value_per_unit_display': 
                return $owner->getApproximatelyValuePerUnit()->round(2)->getValue();
            case 'measurement_unit': 
                $measurement_unit = '';
                $group_first_item = $owner->getFirstItemRecursively();
                if (!empty($group_first_item))
                {
                    $measurement_unit = $group_first_item->measurement_unit->DisplayName;
                }
                return $measurement_unit;

            default: return parent::vuePropValue($prop);
        }
    }
}