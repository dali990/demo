<?php

class BuildingSiteDiaryGUI extends SIMAActiveRecordDisplayGUI
{
    public function columnLabels()
    {
        return [
            'building_construction_id' => Yii::t('BuildingsModule.BuildingConstruction', 'BuildingConstruction'),
            'building_construction' => Yii::t('BuildingsModule.BuildingConstruction', 'BuildingConstruction'),
            'constructor_id' => Yii::t('BuildingsModule.BuildingSiteDiary', 'Constructor'),
            'constructor' => Yii::t('BuildingsModule.BuildingSiteDiary', 'Constructor'),
            'date' => Yii::t('BuildingsModule.BuildingSiteDiary', 'Date'),
            'work_description' => Yii::t('BuildingsModule.BuildingSiteDiary', 'WorkDescription'),
            'remarks' => Yii::t('BuildingsModule.BuildingSiteDiary', 'Remarks'),
            'diary_creator_confirmed' => Yii::t('BuildingsModule.BuildingSiteDiary', 'DiaryCreatorConfirmed'),
            'responsible_engineer_confirmed' => Yii::t('BuildingsModule.BuildingSiteDiary', 'ResponsibleEngineerConfirmed'),
            'supervisor_confirmed' => Yii::t('BuildingsModule.BuildingSiteDiary', 'SupervisorConfirmed'),
        ] + parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return Yii::t('BuildingsModule.BuildingSiteDiary', $plural ? 'BuildingSiteDiaries' : 'BuildingSiteDiary');
    }
    
    public function modelViews()
    {
        return [
            'inFile'
        ] + parent::modelViews();
    }
    
    public function modelForms()
    {
        $owner = $this->owner;
        
        $constructor_id_field = ['relation', 'relName' => 'constructor', 'add_button' => false];
        if (!empty($owner->building_construction_id))
        {
            $constructor_id_field['filters'] = [
                'OR',
                [
                    'scopes' => [
                        'withCompanyToTheme' => $owner->building_construction_id
                    ]
                ],
                [
                    'ids' => Yii::app()->company->id
                ]
            ];
        }
        
        return [
            'default' => [
                'type' => 'generic',
                'columns' => [
                    'building_construction_id' => ['relation', 'relName' => 'building_construction', 'add_button' => true],
                    'constructor_id' => $constructor_id_field,
                    'date' => 'datetimeField',
                    'work_description' => 'tinymce',
                    'remarks' => 'tinymce',
                    'diary_creator_confirmed' => 'status',
                    'responsible_engineer_confirmed' => 'status',
                    'supervisor_confirmed' => 'status'
                ]
            ],
            'onlyConstructor' => [
                'type' => 'generic',
                'columns' => [
                    'building_construction_id' => 'hidden',
                    'constructor_id' => $constructor_id_field,
                    'date' => 'hidden'
                ]
            ]
        ];
    }
    
    public function guiTableSettings($type)
    {   
        switch ($type)
        {
            default:
                return [
                'columns' => [
                    'building_construction', 'constructor', 'date', 'work_description', 'remarks', 'diary_creator_confirmed', 'responsible_engineer_confirmed', 'supervisor_confirmed'
                ]
            ];
        }
    }
    
    public function getDisplayModel()
    {
        return $this->owner->file;
    }
    
    public function vuePropValue($prop)
    {
        $owner = $this->owner;

        switch ($prop)
        {
            //mora da se redefinise jer ne mozemo da koristimo rec constructor u vue shot jer je to javascript predefinisana rec
            case 'constructor_company': 
                return [
                    'TYPE' => 'MODEL',
                    'TAG' => SIMAMisc::getVueModelTag($owner->constructor)
                ];
                
            default: return parent::vuePropValue($prop);
        }
    }
}