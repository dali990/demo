<?php

class BuildingSiteDiaryWorkHourGUI extends SIMAActiveRecordGUI
{
    public function columnLabels()
    {
        return [
            'site_diary_id' => Yii::t('BuildingsModule.BuildingSiteDiary', 'BuildingSiteDiary'),
            'site_diary' => Yii::t('BuildingsModule.BuildingSiteDiary', 'BuildingSiteDiary'),
            'start_datetime' => Yii::t('BuildingsModule.BuildingSiteDiaryWorkHour', 'StartDateTime'),
            'end_datetime' => Yii::t('BuildingsModule.BuildingSiteDiaryWorkHour', 'EndDateTime'),
            'note' => Yii::t('BuildingsModule.BuildingSiteDiaryWorkHour', 'Note')
        ] + parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return Yii::t('BuildingsModule.BuildingSiteDiaryWorkHour', $plural ? 'BuildingSiteDiaryWorkHours' : 'BuildingSiteDiaryWorkHour');
    }
    
    public function modelForms()
    {
        $owner = $this->owner;

        return [
            'default' => [
                'type' => 'generic',
                'columns' => [
                    'site_diary_id' => ['relation', 'relName' => 'site_diary', 'add_button' => true],
                    'start_datetime' => ['datetimeField', 'mode'=>'datetime'],
                    'end_datetime' => ['datetimeField', 'mode'=>'datetime'],
                    'note' => 'textArea'
                ]
            ]
        ];
    }
    
    public function guiTableSettings($type)
    {   
        switch ($type)
        {
            default:
                return [
                'columns' => [
                    'site_diary', 'start_datetime', 'end_datetime', 'note'
                ]
            ];
        }
    }
    
    public function vuePropValue($prop)
    {
        $owner = $this->owner;

        switch ($prop)
        {
            case 'start_time': 
                $start_time = new DateTime($owner->start_datetime);
                return $start_time->format('H:i');
            case 'end_time': 
                $end_time = new DateTime($owner->end_datetime);
                return $end_time->format('H:i');
            default: return parent::vuePropValue($prop);
        }
    }
    
    public function vueProp(string $prop, array $scopes = []):array
    {
        $owner = $this->owner;

        switch ($prop)
        {
            case 'building_site_diary_work_hour_workers_count':
                return [$owner->building_site_diary_work_hour_workers_count, SIMAMisc::getModelTags($owner->building_site_diary_work_hour_workers)];
            case 'building_site_diary_work_hour_worker_workers':
                $prop_value = [
                    'TYPE' => 'MODEL_ARRAY',
                    'ARRAY' => SIMAMisc::getVueModelTags($owner->building_site_diary_work_hour_worker_workers)
                ];
                return [$prop_value, SIMAMisc::getModelTags($owner->building_site_diary_work_hour_workers)];
            case 'building_site_diary_work_hour_worker_workers_count':
                return [$owner->building_site_diary_work_hour_worker_workers_count, SIMAMisc::getModelTags($owner->building_site_diary_work_hour_workers)];
            case 'building_site_diary_work_hour_craftsman_workers':
                $prop_value = [
                    'TYPE' => 'MODEL_ARRAY',
                    'ARRAY' => SIMAMisc::getVueModelTags($owner->building_site_diary_work_hour_craftsman_workers)
                ];
                return [$prop_value, SIMAMisc::getModelTags($owner->building_site_diary_work_hour_workers)];    
            case 'building_site_diary_work_hour_craftsman_workers_count':
                return [$owner->building_site_diary_work_hour_craftsman_workers_count, SIMAMisc::getModelTags($owner->building_site_diary_work_hour_workers)];
            case 'building_site_diary_work_hour_tehnical_staff_workers':
                $prop_value = [
                    'TYPE' => 'MODEL_ARRAY',
                    'ARRAY' => SIMAMisc::getVueModelTags($owner->building_site_diary_work_hour_tehnical_staff_workers)
                ];
                return [$prop_value, SIMAMisc::getModelTags($owner->building_site_diary_work_hour_workers)];    
            case 'building_site_diary_work_hour_tehnical_staff_workers_count':
                return [$owner->building_site_diary_work_hour_tehnical_staff_workers_count, SIMAMisc::getModelTags($owner->building_site_diary_work_hour_workers)];
            case 'building_site_diary_work_hour_other_workers':
                $prop_value = [
                    'TYPE' => 'MODEL_ARRAY',
                    'ARRAY' => SIMAMisc::getVueModelTags($owner->building_site_diary_work_hour_other_workers)
                ];
                return [$prop_value, SIMAMisc::getModelTags($owner->building_site_diary_work_hour_workers)];    
            case 'building_site_diary_work_hour_other_workers_count':
                return [$owner->building_site_diary_work_hour_other_workers_count, SIMAMisc::getModelTags($owner->building_site_diary_work_hour_workers)];
            case 'building_site_diary_work_hour_mechanizations_count':
                return [$owner->building_site_diary_work_hour_mechanizations_count, SIMAMisc::getModelTags($owner->building_site_diary_work_hour_mechanizations)];
            default: return parent::vueProp($prop, $scopes);
        }
    }
}