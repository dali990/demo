<?php

class BillOfQuantityToUserGUI extends SIMAActiveRecordGUI
{   
    public function columnLabels()
    {
        return [
            'bill_of_quantity_id' => Yii::t('BuildingsModule.BillOfQuantity', 'BillOfQuantity'),
            'bill_of_quantity' => Yii::t('BuildingsModule.BillOfQuantity', 'BillOfQuantity'),
            'user' => Yii::t('BuildingsModule.BillOfQuantityToUser', 'User'),
            'user_id' => Yii::t('BuildingsModule.BillOfQuantityToUser', 'User')
        ] + parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return Yii::t('BuildingsModule.BillOfQuantityToUser', $plural ? 'BillOfQuantitiesToUsers': 'BillOfQuantityToUser');
    }
    
    public function modelForms()
    {
        return [
            'default' => [
                'type' => 'generic',
                'columns' => [
                    'bill_of_quantity_id' => ['relation', 'relName' => 'bill_of_quantity', 'add_button' => true],
                    'user_id' => ['relation', 'relName' => 'user']
                ]
            ]
        ];
    }

    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default:
                return [
                    'columns' => ['bill_of_quantity', 'user']
                ];
        }
    }
}
