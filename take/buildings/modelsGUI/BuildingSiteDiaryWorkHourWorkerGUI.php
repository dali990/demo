<?php

class BuildingSiteDiaryWorkHourWorkerGUI extends SIMAActiveRecordGUI
{
    public function columnLabels()
    {
        return [
            'site_diary_work_hour_id' => Yii::t('BuildingsModule.BuildingSiteDiaryWorkHour', 'BuildingSiteDiaryWorkHour'),
            'site_diary_work_hour' => Yii::t('BuildingsModule.BuildingSiteDiaryWorkHour', 'BuildingSiteDiaryWorkHour'),
            'worker_type' => Yii::t('BuildingsModule.BuildingSiteDiaryWorkHourWorker', 'WorkerType'),
            'worker_id' => Yii::t('BuildingsModule.BuildingSiteDiaryWorkHourWorker', 'Worker'),
            'worker' => Yii::t('BuildingsModule.BuildingSiteDiaryWorkHourWorker', 'Worker'),
            'worker_count' => Yii::t('BuildingsModule.BuildingSiteDiaryWorkHourWorker', 'WorkerCount')
        ] + parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return Yii::t('BuildingsModule.BuildingSiteDiaryWorkHourWorker', $plural ? 'BuildingSiteDiaryWorkHourWorkers' : 'BuildingSiteDiaryWorkHourWorker');
    }
    
    public function columnDisplays($column)
    {
        $owner = $this->owner;
        switch ($column)
        {
            case 'worker_type': return BuildingSiteDiaryWorkHourWorker::$worker_types[$owner->$column];
            default: return parent::columnDisplays($column);
        }
    }
    
    public function modelForms()
    {
        $owner = $this->owner;

        return [
            'default' => [
                'type' => 'generic',
                'columns' => [
                    'site_diary_work_hour_id' => ['relation', 'relName' => 'site_diary_work_hour', 'add_button' => true],
                    'worker_type' => 'dropdown',
                    'worker_id' => ['relation', 'relName' => 'worker', 'add_button' => true],
                    'worker_count' => ['textField', 'dependent_on' => [
                        'worker_id' => [
                            'onValue' => 'hide',
                            'onEmpty' => 'show'
                        ]
                    ]]
                ]
            ]
        ];
    }
    
    public function guiTableSettings($type)
    {   
        switch ($type)
        {
            default:
                return [
                'columns' => [
                    'site_diary_work_hour', 'worker_type', 'worker', 'worker_count'
                ]
            ];
        }
    }
    
    public function droplist($key, $relName='', $filter=null)
    {
        switch ($key)
        {
            case 'worker_type': return BuildingSiteDiaryWorkHourWorker::$worker_types;
            default: return parent::droplist($key, $relName, $filter);
        }
    }
}