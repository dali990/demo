<?php

class BuildingInterimBillGUI extends SIMAActiveRecordDisplayGUI
{
    public function columnLabels()
    {
        return [
            'file' => Yii::t('BuildingsModule.BuildingInterimBill', 'File'),
            'building_construction_id' => Yii::t('BuildingsModule.BuildingConstruction', 'BuildingConstruction'),
            'building_construction' => Yii::t('BuildingsModule.BuildingConstruction', 'BuildingConstruction'),
            'order_number' => Yii::t('BuildingsModule.BuildingInterimBill', 'OrderNumber'),
            'issue_date' => Yii::t('BuildingsModule.BuildingInterimBill', 'IssueDate'),
            'status' => Yii::t('BuildingsModule.BuildingInterimBill', 'Status'),
            'is_final' => Yii::t('BuildingsModule.BuildingInterimBill', 'IsFinal')
        ] + parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return Yii::t('BuildingsModule.BuildingInterimBill', $plural ? 'BuildingInterimBills' : 'BuildingInterimBill');
    }
    
    public function modelViews()
    {
        return [
            'inFile'
        ] + parent::modelViews();
    }
    
    public function columnDisplays($column)
    {
        $owner = $this->owner;

        switch ($column)
        {
            case 'status' :
                return BuildingInterimBill::$statuses[$owner->status];
            default : return parent::columnDisplays($column);
        }
    }
    
    public function modelForms()
    {
        $owner = $this->owner;
        
        $building_construction_id_field = ['relation', 'relName' => 'building_construction', 'add_button' => true];
        if (!$owner->isNewRecord)
        {
            $building_construction_id_field['disabled'] = true;
        }

        return [
            'default' => [
                'type' => 'generic',
                'columns' => [
                    'building_construction_id' => $building_construction_id_field,
                    'issue_date' => 'datetimeField',
                    'status' => 'hidden',
                    'is_final' => ($owner->isNewRecord || $owner->isLastInBuildingConstruction()) ? 'checkBox' : 'hidden'
                ]
            ]
        ];
    }
    
    public function guiTableSettings($type)
    {   
        switch ($type)
        {
            case 'fromBuildingConstruction':
                return [
                    'columns' => [
                        'order_number', 'file.name', 'issue_date', 'status', 'is_final'
                    ]
                ];
            default:
                return [
                'columns' => [
                    'order_number', 'file.name', 'building_construction', 'issue_date', 'status', 'is_final'
                ]
            ];
        }
    }
    
    public function getDisplayModel()
    {
        return $this->owner->file;
    }
    
    public function droplist($key, $relName = '', $filter = null)
    {
        switch ($key) 
        {
            case 'status': return BuildingInterimBill::$statuses;
            default: return parent::droplist($key, $relName, $filter);
        }
    }
    
    public function vuePropValue($prop)
    {
        $owner = $this->owner;

        switch ($prop)
        {
            case 'status_display': 
                return BuildingInterimBill::$statuses[$owner->status];
            case 'order_number_roman': 
                return SIMAMisc::numberToRoman($owner->order_number);
            case 'has_admin_privilege':
                return Yii::app()->user->checkAccess('modelThemeAdmin');
            default: return parent::vuePropValue($prop);
        }
    }
    
    public function vueProp(string $prop, array $scopes = []):array
    {
        $owner = $this->owner;

        switch ($prop)
        {
            case 'value': 
                $root_sheet_group = $owner->getRootSheetGroup();
                return [$owner->getValue()->getValue(), !empty($root_sheet_group) ? SIMAMisc::getModelTags($root_sheet_group) : []];
            case 'total_value': 
                $root_sheet_group = $owner->getRootSheetGroup();
                return [$owner->getTotalValue()->getValue(), !empty($root_sheet_group) ? SIMAMisc::getModelTags($root_sheet_group) : []];
            case 'bill_advance':
                return [$owner->getBillAdvanceValue(), !empty($owner->file->bill) ? SIMAMisc::getModelTags($owner->file->bill) : []];
            case 'remaining_bill_advance':
                return [$owner->getRemainingBillAdvanceValue(), !empty($owner->file->bill) ? SIMAMisc::getModelTags($owner->file->bill) : []];
            case 'is_last_in_building_construction': 
                return [$owner->isLastInBuildingConstruction(), SIMAMisc::getModelTags(BuildingInterimBill::model())];
            case 'can_change_building_measurement_sheet':
                return [BuildingMeasurementSheet::hasChangeAccess($owner->building_construction, Yii::app()->user->model), SIMAMisc::getModelTags($owner->building_construction)];
            case 'can_change_status':
                return [
                    !empty(Theme::model()->hasChangeAccess(Yii::app()->user->id)->findByPk($owner->building_construction_id)), 
                    SIMAMisc::getModelTags($owner->building_construction->theme)
                ];
            case 'measurement_sheet': 
                $sheet = $this->getMeasurementSheet($scopes);
                
                $prop_value = null;
                $update_models = [];
                if (!empty($sheet))
                {
                    $prop_value = [
                        'TYPE' => 'MODEL',
                        'TAG' => SIMAMisc::getVueModelTag($sheet)
                    ];
                    $update_models = SIMAMisc::getModelTags($sheet);
                }

                return [$prop_value, $update_models];
            case 'measurement_sheet_group': 
                $sheet_group = $this->getMeasurementSheetGroup($scopes);
                
                $prop_value = null;
                $update_models = [];
                if (!empty($sheet_group))
                {
                    $prop_value = [
                        'TYPE' => 'MODEL',
                        'TAG' => SIMAMisc::getVueModelTag($sheet_group)
                    ];
                    $update_models = SIMAMisc::getModelTags($sheet_group);
                }

                return [$prop_value, $update_models];
            case 'has_unconfirmed_zero_measurement_sheets': 
                $zero_sheets = BuildingMeasurementSheet::model()->findAll([
                    'model_filter' => [
                        'building_interim_bill' => [
                            'ids' => $owner->id
                        ],
                        'confirmed' => false,
                        'filter_scopes' => 'onlyZero'
                    ]
                ]);
                
                $update_model_tags = SIMAMisc::getModelTags($owner->building_measurement_sheets);
                foreach ($owner->building_measurement_sheets as $sheet)
                {
                    $update_model_tags = array_merge($update_model_tags, SIMAMisc::getModelTags($sheet->sheet_items));
                }

                return [count($zero_sheets) > 0, $update_model_tags];

            case 'item_group_pre_quantity_done': 
                return $this->getMeasurementSheetGroupColumnValue('pre_quantity_done', $scopes);
            case 'item_pre_quantity_done': 
                return $this->getMeasurementSheetColumnValue('pre_quantity_done', $scopes);            
            case 'item_group_pre_value_done': 
                return $this->getMeasurementSheetGroupColumnValue('pre_value_done', $scopes);
            case 'item_pre_value_done': 
                return $this->getMeasurementSheetColumnValue('pre_value_done', $scopes);
            case 'item_group_pre_percentage_done': 
                return $this->getMeasurementSheetGroupColumnValue('pre_percentage_done', $scopes);
            case 'item_pre_percentage_done': 
                return $this->getMeasurementSheetColumnValue('pre_percentage_done', $scopes);
                
            case 'item_group_quantity_done': 
                return $this->getMeasurementSheetGroupColumnValue('quantity_done', $scopes);
            case 'item_quantity_done': 
                return $this->getMeasurementSheetColumnValue('quantity_done', $scopes);            
            case 'item_group_value_done': 
                return $this->getMeasurementSheetGroupColumnValue('value_done', $scopes);
            case 'item_value_done': 
                return $this->getMeasurementSheetColumnValue('value_done', $scopes);
            case 'item_group_percentage_done': 
                return $this->getMeasurementSheetGroupColumnValue('percentage_done', $scopes);
            case 'item_percentage_done': 
                return $this->getMeasurementSheetColumnValue('percentage_done', $scopes);
                
            case 'item_group_total_quantity_done': 
                return $this->getMeasurementSheetGroupColumnValue('total_quantity_done', $scopes);
            case 'item_total_quantity_done': 
                return $this->getMeasurementSheetColumnValue('total_quantity_done', $scopes);            
            case 'item_group_total_value_done': 
                return $this->getMeasurementSheetGroupColumnValue('total_value_done', $scopes);
            case 'item_total_value_done': 
                return $this->getMeasurementSheetColumnValue('total_value_done', $scopes);
            case 'item_group_total_percentage_done': 
                return $this->getMeasurementSheetGroupColumnValue('total_percentage_done', $scopes);
            case 'item_total_percentage_done': 
                return $this->getMeasurementSheetColumnValue('total_percentage_done', $scopes);

            case 'max_pre_quantity_done_for_object':
                $result = $this->getMaxColumnValueAndUpdateModelTags('pre_quantity_done', $scopes);
                return [$result['max_value'], $result['update_models']];
            case 'max_quantity_done_for_object':
                $result = $this->getMaxColumnValueAndUpdateModelTags('quantity_done', $scopes);
                return [$result['max_value'], $result['update_models']];
            case 'max_total_quantity_done_for_object':
                $result = $this->getMaxColumnValueAndUpdateModelTags('total_quantity_done', $scopes);
                return [$result['max_value'], $result['update_models']];
                
            case 'max_pre_value_done_for_object':
                $result = $this->getMaxColumnValueAndUpdateModelTags('pre_value_done', $scopes);
                return [$result['max_value'], $result['update_models']];
            case 'max_value_done_for_object':
                $result = $this->getMaxColumnValueAndUpdateModelTags('value_done', $scopes);
                return [$result['max_value'], $result['update_models']];
            case 'max_total_value_done_for_object':
                $result = $this->getMaxColumnValueAndUpdateModelTags('total_value_done', $scopes);
                return [$result['max_value'], $result['update_models']];
            
            default: return parent::vueProp($prop, $scopes);
        }
    }
    
    private function getMeasurementSheetGroupColumnValue($column_name, $scopes)
    {
        $sheet_group = $this->getMeasurementSheetGroup($scopes);
        return [!empty($sheet_group) ? $sheet_group->$column_name->getValue() : SIMABigNumber::fromFloat(0.00, 2)->getValue(), SIMAMisc::getModelTags(!empty($sheet_group) ? $sheet_group : [])];
    }

    private function getMeasurementSheetColumnValue($column_name, $scopes)
    {
        $sheet = $this->getMeasurementSheet($scopes);
        return [!empty($sheet) ? $sheet->$column_name->getValue() : SIMABigNumber::fromFloat(0.00, 2)->getValue(), SIMAMisc::getModelTags(!empty($sheet) ? $sheet : [])];
    }
    
    private function getMeasurementSheetGroup($scopes)
    {
        $owner = $this->owner;

        return BuildingMeasurementSheetGroup::model()->find([
            'model_filter' => [
                'scopes' => $scopes,
                'building_interim_bill' => [
                    'ids' => $owner->id
                ]
            ]
        ]);
    }
    
    private function getMeasurementSheet($scopes)
    {
        $owner = $this->owner;

        return BuildingMeasurementSheet::model()->find([
            'model_filter' => [
                'scopes' => $scopes,
                'building_interim_bill' => [
                    'ids' => $owner->id
                ]
            ]
        ]);
    }
    
    private function getMaxColumnValueAndUpdateModelTags($column_name, $scopes)
    {
        $owner = $this->owner;
        
        $max_value = 0;
        $update_models = [];
        if (!empty($scopes['forObject']))
        {
            $root_group = $owner->building_construction->bill_of_quantity->root_group;
            if (!empty($root_group))
            {
                $root_sheet_group = BuildingMeasurementSheetGroup::model()->findByAttributes([
                    'building_interim_bill_id' => $owner->id,
                    'bill_of_quantity_item_group_id' => $root_group->id,
                    'object_id' => $scopes['forObject']
                ]);
                if (!empty($root_sheet_group))
                {
                    $max_value = $root_sheet_group->$column_name->getValue();
                    $update_models = [$root_sheet_group];
                }
            }
        }
        
        return [
            'max_value' => $max_value,
            'update_models' => SIMAMisc::getModelTags($update_models)
        ];
    }
}