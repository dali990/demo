<?php

class BuildingSiteDiaryWorkHourMechanizationGUI extends SIMAActiveRecordGUI
{
    public function columnLabels()
    {
        return [
            'site_diary_work_hour_id' => Yii::t('BuildingsModule.BuildingSiteDiaryWorkHour', 'BuildingSiteDiaryWorkHour'),
            'site_diary_work_hour' => Yii::t('BuildingsModule.BuildingSiteDiaryWorkHour', 'BuildingSiteDiaryWorkHour'),
            'mechanization_type_id' => Yii::t('BuildingsModule.BuildingSiteDiaryWorkHourMechanization', 'MechanizationType'),
            'mechanization_type' => Yii::t('BuildingsModule.BuildingSiteDiaryWorkHourMechanization', 'MechanizationType'),
            'mechanization_id' => Yii::t('BuildingsModule.BuildingSiteDiaryWorkHourMechanization', 'Mechanization'),
            'mechanization' => Yii::t('BuildingsModule.BuildingSiteDiaryWorkHourMechanization', 'Mechanization'),
            'mechanization_count' => Yii::t('BuildingsModule.BuildingSiteDiaryWorkHourMechanization', 'MechanizationCount')
        ] + parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return Yii::t('BuildingsModule.BuildingSiteDiaryWorkHourMechanization', $plural ? 'BuildingSiteDiaryWorkHourMechanizations' : 'BuildingSiteDiaryWorkHourMechanization');
    }
    
    public function modelForms()
    {
        $owner = $this->owner;

        return [
            'default' => [
                'type' => 'generic',
                'columns' => [
                    'site_diary_work_hour_id' => ['relation', 'relName' => 'site_diary_work_hour', 'add_button' => true],
                    'mechanization_type_id' => ['relation', 'relName' => 'mechanization_type', 'add_button' => true],
                    'mechanization_id' => ['relation', 'relName' => 'mechanization', 'add_button' => true, 'dependent_on' => [
                        'mechanization_type_id' => [
                            'onValue' => [
                                'enable',
                                ['condition', 'model_filter' => 'fixed_asset_type_recur.ids']
                            ],
                            'onEmpty' => 'disable'
                        ]
                    ]],
                    'mechanization_count' => ['numberField', 'dependent_on' => [
                        'mechanization_id' => [
                            'onValue' => 'hide',
                            'onEmpty' => 'show'
                        ]
                    ]]
                ]
            ]
        ];
    }
    
    public function guiTableSettings($type)
    {   
        switch ($type)
        {
            default:
                return [
                'columns' => [
                    'site_diary_work_hour', 'mechanization_type', 'mechanization', 'mechanization_count'
                ]
            ];
        }
    }
}