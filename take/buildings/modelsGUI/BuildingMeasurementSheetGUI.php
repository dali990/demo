<?php

class BuildingMeasurementSheetGUI extends SIMAActiveRecordDisplayGUI
{
    public function columnLabels()
    {
        return [
            'building_interim_bill_id' => Yii::t('BuildingsModule.BuildingInterimBill', 'BuildingInterimBill'),
            'building_interim_bill' => Yii::t('BuildingsModule.BuildingInterimBill', 'BuildingInterimBill'),
            'bill_of_quantity_item_id' => Yii::t('BuildingsModule.BillOfQuantityItem', 'BillOfQuantityItem'),
            'bill_of_quantity_item' => Yii::t('BuildingsModule.BillOfQuantityItem', 'BillOfQuantityItem'),
            'object_id' => Yii::t('BuildingsModule.BillOfQuantityObject', 'BillOfQuantityObject'),
            'object' => Yii::t('BuildingsModule.BillOfQuantityObject', 'BillOfQuantityObject'),
            'value_done' => Yii::t('BuildingsModule.BuildingMeasurementSheet', 'ValueDone'),
            'quantity_done' => Yii::t('BuildingsModule.BuildingMeasurementSheet', 'QuantityDone'),
            'pre_value_done' => Yii::t('BuildingsModule.BuildingInterimBill', 'PreInterimBillValueDone'),
            'pre_quantity_done' => Yii::t('BuildingsModule.BuildingInterimBill', 'PreInterimBillQuantityDone'),
            'quantity_validated' => Yii::t('BuildingsModule.BuildingMeasurementSheet', 'QuantityValidated'),
            'written' => Yii::t('BuildingsModule.BuildingMeasurementSheet', 'Written'),
            'confirmed' => Yii::t('BuildingsModule.BuildingMeasurementSheet', 'Confirmed'),
            'validated' => Yii::t('BuildingsModule.BuildingMeasurementSheet', 'Validated')
        ] + parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return Yii::t('BuildingsModule.BuildingMeasurementSheet', $plural ? 'BuildingMeasurementSheets' : 'BuildingMeasurementSheet');
    }
    
    public function modelViews()
    {
        return [
            'options',
            'basic_info',
            'full_info',
            'full_info_right_view'
        ] + parent::modelViews();
    }
    
    public function modelForms()
    {
        $owner = $this->owner;

        $bill_of_quantity_item_id_field = ['relation', 'relName' => 'bill_of_quantity_item'];
        if (!empty($owner->building_interim_bill_id))
        {
            $building_interim_bill = BuildingInterimBill::model()->findByPkWithCheck($owner->building_interim_bill_id);
            $bill_of_quantity_item_id_field['filters'] = [
                'group' => [
                    'bill_of_quantity' => [
                        'ids' => $building_interim_bill->building_construction->bill_of_quantity_id
                    ]
                ]
            ];
        }
        
        $object_id_field = ['relation', 'relName' => 'object', 'filters' => [
            'scopes' => 'onlyLeaf'
        ]];
        if (!empty($owner->building_interim_bill_id))
        {
            $object_id_field['filters']['bill_of_quantity'] = [
                'ids' => $owner->building_interim_bill->building_construction->bill_of_quantity_id
            ];
        }
        
        return [
            'default' => [
                'type' => 'generic',
                'columns' => [
                    'building_interim_bill_id' => ['relation', 'relName' => 'building_interim_bill', 'add_button' => true],
                    'bill_of_quantity_item_id' => $bill_of_quantity_item_id_field,
                    'object_id' => $object_id_field
                ]
            ],
            'onlyQuantityValidated' => [
                'type' => 'generic',
                'columns' => [
                    'building_interim_bill_id' => 'hidden',
                    'bill_of_quantity_item_id' => 'hidden',
                    'object_id' => 'hidden',
                    'quantity_validated' => 'numberField'
                ]
            ]
        ];
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            case 'fromBuildingInterimBill':
                return [
                    'columns' => [
                        'bill_of_quantity_item', 'object', 'value_done', 'quantity_done', 'pre_value_done', 'pre_quantity_done', 'quantity_validated'
                    ]
                ];
            default:
                return [
                'columns' => [
                    'building_interim_bill', 'bill_of_quantity_item', 'object', 'value_done', 'quantity_done', 'pre_value_done', 'pre_quantity_done', 'quantity_validated'
                ]
            ];
        }
    }
}