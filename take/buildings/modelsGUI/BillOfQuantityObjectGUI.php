<?php

class BillOfQuantityObjectGUI extends SIMAActiveRecordGUI
{
    public function columnLabels()
    {
        return [
            'name' => Yii::t('BuildingsModule.BillOfQuantityObject', 'Name'),
            'bill_of_quantity_id' => Yii::t('BuildingsModule.BillOfQuantity', 'BillOfQuantity'),
            'bill_of_quantity' => Yii::t('BuildingsModule.BillOfQuantity', 'BillOfQuantity'),
            'parent_id' => Yii::t('BuildingsModule.BillOfQuantityObject', 'Parent'),
            'parent' => Yii::t('BuildingsModule.BillOfQuantityObject', 'Parent'),
            'description' => Yii::t('BuildingsModule.BillOfQuantityObject', 'Description')
        ] + parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return Yii::t('BuildingsModule.BillOfQuantityObject', $plural ? 'BillOfQuantitiesObjects' : 'BillOfQuantityObject');
    }
    
    public function modelForms()
    {
        $owner = $this->owner;

        $parent_id_field = ['relation', 'relName' => 'parent', 'add_button' => true];
        if (!empty($owner->bill_of_quantity_id))
        {
            $parent_id_field['filters'] = [
                'bill_of_quantity' => [
                    'ids' => $owner->bill_of_quantity_id
                ]
            ];
            $parent_id_field['add_button'] = [
                'params' => [
                    'add_button' => [
                        'init_data' => [
                            BillOfQuantityObject::class => [
                                'bill_of_quantity_id' => $owner->bill_of_quantity_id
                            ]
                        ]
                    ]
                ]
            ];
        }
        
        return [
            'default' => [
                'type' => 'generic',
                'columns' => [
                    'name' => 'textField',
                    'bill_of_quantity_id' => 'hidden',
                    'parent_id' => $parent_id_field,
                    'description' => 'textArea'
                ]
            ]
        ];
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default:
                return [
                'columns' => [
                    'name', 'bill_of_quantity', 'parent', 'description'
                ]
            ];
        }
    }
    
    public function vueProp(string $prop, array $scopes = []):array
    {
        $owner = $this->owner;

        switch ($prop)
        {
            case 'children':
                $prop_value = [
                    'TYPE' => 'MODEL_ARRAY',
                    'ARRAY' => SIMAMisc::getVueModelTags($owner->children)
                ];
                
                return [$prop_value, SIMAMisc::getModelTags($owner->children)];
            default: return parent::vueProp($prop, $scopes);
        }
    }
}