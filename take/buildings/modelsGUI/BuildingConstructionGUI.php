<?php

class BuildingConstructionGUI extends SIMAActiveRecordGUI
{
    public function columnLabels()
    {
        return [
            'constructor_id' => Yii::t('BuildingsModule.BuildingConstruction', 'Constructor'),
            'constructor' => Yii::t('BuildingsModule.BuildingConstruction', 'Constructor'),
            'investor_id' => Yii::t('BuildingsModule.BuildingConstruction', 'Investor'),
            'investor' => Yii::t('BuildingsModule.BuildingConstruction', 'Investor'),
            'bill_of_quantity_id' => Yii::t('BuildingsModule.BillOfQuantity', 'BillOfQuantity'),
            'bill_of_quantity' => Yii::t('BuildingsModule.BillOfQuantity', 'BillOfQuantity'),
            'responsible_engineer_id' => Yii::t('BuildingsModule.BuildingConstruction', 'ResponsibleEngineer'),
            'responsible_engineer' => Yii::t('BuildingsModule.BuildingConstruction', 'ResponsibleEngineer'),
            'start_date' => Yii::t('BuildingsModule.BuildingConstruction', 'StartDate'),
            'construction_boss_id' => Yii::t('BuildingsModule.BuildingConstruction', 'ConstructionBoss'),
            'construction_boss' => Yii::t('BuildingsModule.BuildingConstruction', 'ConstructionBoss'),
            'construction_boss_assistants_list' => Yii::t('BuildingsModule.BuildingConstructionToBossAssistant', 'BuildingConstructionsToBossAssistants')
        ] + parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return Yii::t('BuildingsModule.BuildingConstruction', $plural ? 'BuildingConstructions' : 'BuildingConstruction');
    }
    
    public function modelViews()
    {
        return [
            'inTheme'
        ] + parent::modelViews();
    }
    
    public function modelForms()
    {
        $owner = $this->owner;
        
        $constructor_id_field = ['relation', 'relName' => 'constructor', 'add_button' => false];

        if (!empty($owner->id))
        {
            $constructor_id_field['filters'] = [
                'OR',
                [
                    'scopes' => [
                        'withCompanyToTheme' => $owner->id
                    ]
                ],
                [
                    'ids' => Yii::app()->company->id
                ]
            ];
        }
        
        if (!empty($owner->theme->job->investor_id))
        {
            $owner->investor_id = $owner->theme->job->investor_id;
        }
        else
        {
            $owner->investor_id = Yii::app()->company->id;
        }

        if (
                Yii::app()->configManager->get('buildings.building_construction_boss_theme_coordinator', false) && 
                empty($owner->construction_boss_id) && 
                !empty($owner->theme->coordinator_id)
            )
        {
            $owner->construction_boss_id = $owner->theme->coordinator_id;
        }
        
        return [
            'default' => [
                'type' => 'generic',
                'columns' => [
                    'bill_of_quantity_id' => ['relation', 'relName' => 'bill_of_quantity', 'add_button' => false, 'filters' => ['scopes' => 'withoutBuildingConstruction']],
                    'investor_id' => ['relation', 'relName' => 'investor', 'disabled' => true],
                    'constructor_id' => $constructor_id_field,
                    'responsible_engineer_id' => ['relation', 'relName' => 'responsible_engineer', 'add_button' => true],
                    'start_date' => 'datetimeField',
                    'construction_boss_id' => ['relation', 'relName' => 'construction_boss'],
                    'construction_boss_assistants_list' => ['list',
                        'model' => User::class,
                        'relName' => 'construction_boss_assistants',
                        'multiselect' => true,
                        'params' => [
                            'add_button' => false
                        ]
                    ]
                ]
            ]
        ];
    }
    
    public function guiTableSettings($type)
    {   
        switch ($type)
        {
            default:
                return [
                'columns' => [
                    'theme', 'constructor', 'investor', 'bill_of_quantity', 'start_date', 'responsible_engineer', 'construction_boss'
                ]
            ];
        }
    }
    
    public function vueProp(string $prop, array $scopes = []):array
    {
        $owner = $this->owner;

        switch ($prop)
        {
            case 'building_interim_bill_init_data':
                $last_interim_bill = BuildingInterimBill::model()->find([
                    'model_filter' => [
                        'building_construction' => [
                            'ids' => $owner->id
                        ],
                        'display_scopes' => 'byDate'
                    ]
                ]);
                $next_order_number = !empty($last_interim_bill) ? (intval($last_interim_bill->order_number) + 1) : 1;
                $roman_next_order_number = SIMAMisc::numberToRoman($next_order_number);
                
                $interim_bill_document_type_id = Yii::app()->configManager->get('buildings.interim_bill_document_type_id', false);

                $init_data = [
                    BuildingInterimBill::class => [
                        'building_construction_id' => [
                            'disabled',
                            'init' => $owner->id
                        ],
                        'issue_date' => SIMAHtml::currentDateTime(false)
                    ],
                    File::class => [
                        'name' => $roman_next_order_number . ' ' . Yii::t('BuildingsModule.BuildingInterimBill', 'TemporaryBuildingInterimBill'),
                        'responsible_id' => $owner->theme->coordinator_id,
                        'document_type_id' => $interim_bill_document_type_id,
                        'file_belongs' => [
                            'ids' => [
                                'ids' => [
                                    [
                                        'class' => '_non_editable',
                                        'id' => $owner->theme->tag->id,
                                        'display_name' => $owner->theme->tag->DisplayName
                                    ]
                                ]
                            ]
                        ]
                    ]
                ];
                
                return [$init_data, SIMAMisc::getModelTags([$owner->theme, BuildingInterimBill::model()])];
            case 'has_final_interim_bill':
                return [$owner->hasFinalInterimBill(), SIMAMisc::getModelTags($owner->building_interim_bills)];
            case 'can_change_building_interim_bill':
                return [BuildingInterimBill::hasChangeAccess($owner, Yii::app()->user->model), SIMAMisc::getModelTags($owner->theme)];
            default: return parent::vueProp($prop, $scopes);
        }
    }
}