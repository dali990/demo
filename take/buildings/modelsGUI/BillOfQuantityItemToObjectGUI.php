<?php

class BillOfQuantityItemToObjectGUI extends SIMAActiveRecordGUI
{
    public function columnLabels()
    {
        return [
            'item_id' => Yii::t('BuildingsModule.BillOfQuantityItem', 'BillOfQuantityItem'),
            'item' => Yii::t('BuildingsModule.BillOfQuantityItem', 'BillOfQuantityItem'),
            'object_id' => Yii::t('BuildingsModule.BillOfQuantityObject', 'BillOfQuantityObject'),
            'object' => Yii::t('BuildingsModule.BillOfQuantityObject', 'BillOfQuantityObject'),
            'quantity' => Yii::t('BuildingsModule.BillOfQuantityItem', 'Quantity'),
            'value_per_unit' => Yii::t('BuildingsModule.BillOfQuantityItem', 'ValuePerUnit'),
            'value' => Yii::t('BuildingsModule.BillOfQuantityItem', 'Value'),
            'value_done' => Yii::t('BuildingsModule.BillOfQuantityItemToObject', 'ValueDone'),
            'quantity_done' => Yii::t('BuildingsModule.BillOfQuantityItemToObject', 'QuantityDone')
        ] + parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return Yii::t('BuildingsModule.BillOfQuantityItemToObject', $plural ? 'BillOfQuantitiesItemsToObjects' : 'BillOfQuantityItemToObject');
    }
    
    public function modelForms()
    {
        return [
            'default' => [
                'type' => 'generic',
                'columns' => [
                    'item_id' => ['relation', 'relName' => 'item', 'add_button' => true],
                    'object_id' => ['relation', 'relName' => 'object', 'add_button' => true],
                    'quantity' => 'numberField',
                    'value_per_unit' => 'numberField'
                ]
            ]
        ];
    }
    
    public function guiTableSettings($type)
    {   
        switch ($type)
        {
            default:
                return [
                'columns' => [
                    'item', 'object', 'quantity', 'value_per_unit', 'value', 'value_done', 'quantity_done'
                ]
            ];
        }
    }
    
    public function vueProp(string $prop, array $scopes = []):array
    {
        $owner = $this->owner;

        switch ($prop)
        {
            case 'quantity': 
                return [$owner->quantity->getValue(), SIMAMisc::getModelTags($owner->item)];
            case 'value_per_unit': 
                return [$owner->value_per_unit->getValue(), SIMAMisc::getModelTags($owner->item)];
            case 'value_per_unit_display': 
                return [$owner->value_per_unit->round(2)->getValue(), SIMAMisc::getModelTags($owner->item)];
            case 'value': 
                return [$owner->value->getValue(), SIMAMisc::getModelTags($owner->item)];
            default: return parent::vueProp($prop, $scopes);
        }
    }
    
    public function vuePropValue($prop)
    {
        $owner = $this->owner;

        switch ($prop)
        {
            case 'percentage_done':
                return $owner->getPercentageDone()->getValue();
            default: return parent::vuePropValue($prop);
        }
    }
}