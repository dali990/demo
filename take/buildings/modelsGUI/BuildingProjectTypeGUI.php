<?php
/**
 * Description of BuildingProjectTypeGUI
 *
 * @author goran-set
 */
class BuildingProjectTypeGUI extends SIMAActiveRecordGUI
{
    public function columnLabels()
    {
        return array(
            'description'=>'Opis',
            'name'=>'Naziv',
            'short_name'=>'Skraćenica'
        )+parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return Yii::t('BuildingsModule.BuildingProject',$plural?'BuildingProjectTypes':'BuildingProjectType');
    }
    
    public function modelForms()
    {
        return array(
            'default'=>array(
                'type'=>'generic',
                'columns'=>array(
                    'name'=>'textField',
                    'short_name'=>'textField',
                    'description'=>'textArea'
                )
            )
        );
    }

    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default:
                return [
                    'columns' => ['name', 'short_name', 'description']
                ];
        }
    }
    
}
