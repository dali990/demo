<?php
/**
 * Description of BuildingProjectSheetGUI
 *
 * @author goran-set
 */
class BuildingProjectSheetGUI extends SIMAActiveRecordGUI
{
    public function columnLabels()
    {
        return array(
            'description'=>'Opis',
            'name'=>'Naziv',
            'id'=>'Redni broj',
            'number'=>'Broj',
            'archive_box_id'=>'Arhivska kutija'
        )+parent::columnLabels();
    }

    public function modelViews()
    {
        return array(
            'basicInfo'=>array('class' => 'basic_info')
        );
    }
    
    public function modelForms()
    {
        return array(
            'default'=>array(
                'type'=>'generic',
                'columns'=>array(
                    'id'=>'hidden',
                    'name'=>'textField',
                     'archive_box_id'=>array('relation', 'relName'=>'archive_box'),
                    'description'=>'textArea'
                )
            )
        );
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default:
                return [
                    'columns' => ['name','number','description']
                ];
        }
    }
}
