<?php

class BillOfQuantityItemGroupToObjectGUI extends SIMAActiveRecordGUI
{
    public function columnLabels()
    {
        return [
            'item_group_id' => Yii::t('BuildingsModule.BillOfQuantityItem', 'BillOfQuantityItem'),
            'item_group' => Yii::t('BuildingsModule.BillOfQuantityItem', 'BillOfQuantityItem'),
            'object_id' => Yii::t('BuildingsModule.BillOfQuantityObject', 'BillOfQuantityObject'),
            'object' => Yii::t('BuildingsModule.BillOfQuantityObject', 'BillOfQuantityObject'),
            'quantity' => Yii::t('BuildingsModule.BillOfQuantityItem', 'Quantity'),
            'value_per_unit' => Yii::t('BuildingsModule.BillOfQuantityItem', 'ValuePerUnit'),
            'value' => Yii::t('BuildingsModule.BillOfQuantityItem', 'Value'),
            'value_done' => Yii::t('BuildingsModule.BillOfQuantityItemGroupToObject', 'ValueDone'),
            'quantity_done' => Yii::t('BuildingsModule.BillOfQuantityItemGroupToObject', 'QuantityDone')
        ] + parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return Yii::t('BuildingsModule.BillOfQuantityItemGroupToObject', $plural ? 'BillOfQuantityItemGroupsToObjects' : 'BillOfQuantityItemGroupToObject');
    }
    
    public function modelForms()
    {
        return [
            'default' => [
                'type' => 'generic',
                'columns' => [
                    'item_group_id' => ['relation', 'relName' => 'item_group', 'add_button' => true],
                    'object_id' => ['relation', 'relName' => 'object', 'add_button' => true]
                ]
            ]
        ];
    }
    
    public function guiTableSettings($type)
    {   
        switch ($type)
        {
            default:
                return [
                'columns' => [
                    'item_group', 'object', 'quantity', 'value_per_unit', 'value', 'value_done', 'quantity_done'
                ]
            ];
        }
    }
    
    public function vuePropValue($prop)
    {
        $owner = $this->owner;

        switch ($prop)
        {
            case 'value_per_unit': 
                return $owner->getApproximatelyValuePerUnit()->getValue();
            case 'value_per_unit_display': 
                return $owner->getApproximatelyValuePerUnit()->round(2)->getValue();
            case 'percentage_done':
                return $owner->getPercentageDone()->getValue();
            default: return parent::vuePropValue($prop);
        }
    }
}