<?php

class ConstructionMaterialApprovalToFileGUI extends SIMAActiveRecordGUI
{
    public function columnLabels()
    {
        return array(
            'construction_material_approval_id'=>Yii::t('BuildingsModule.ConstructionMaterialApprovalToFile', 'ConstructionMaterialApproval'),
            'construction_material_approval' => Yii::t('BuildingsModule.ConstructionMaterialApprovalToFile', 'ConstructionMaterialApproval'),
            'file_id' => Yii::t('BuildingsModule.ConstructionMaterialApprovalToFile', 'File'),
            'file' => Yii::t('BuildingsModule.ConstructionMaterialApprovalToFile', 'File')
        )+parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return Yii::t('BuildingsModule.ConstructionMaterialApprovalToFile', 'ConstructionMaterialApprovalToFile');
    }
    
    public function modelForms()
    {
        $owner = $this->owner;
        return array(
            'default'=>array(
                'type'=>'generic',
                'columns'=>array(
                    'construction_material_approval_id' => array('searchField','relName'=>'construction_material_approval'),
                    'file_id' => array('searchField','relName'=>'file')
                )
            )
        );
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default: return [
                'columns'=>['construction_material_approval', 'file']
            ];
        }
    }
}