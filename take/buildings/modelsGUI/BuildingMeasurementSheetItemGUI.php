<?php

class BuildingMeasurementSheetItemGUI extends SIMAActiveRecordGUI
{
    public function columnLabels()
    {
        return [
            'building_measurement_sheet_id' => Yii::t('BuildingsModule.BuildingMeasurementSheet', 'BuildingMeasurementSheet'),
            'building_measurement_sheet' => Yii::t('BuildingsModule.BuildingMeasurementSheet', 'BuildingMeasurementSheet'),
            'calculation' => Yii::t('BuildingsModule.BuildingMeasurementSheetItem', 'Calculation'),
            'amount' => Yii::t('BuildingsModule.BuildingMeasurementSheetItem', 'Amount')
        ] + parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return Yii::t('BuildingsModule.BuildingMeasurementSheetItem', $plural ? 'BuildingMeasurementSheetItems' : 'BuildingMeasurementSheetItem');
    }
    
    public function modelForms()
    {
        $owner = $this->owner;
        
        return [
            'default' => [
                'type' => 'generic',
                'columns' => [
                    'building_measurement_sheet_id' => ['relation', 'relName' => 'building_measurement_sheet'],
                    'calculation' => 'textField',
                    'amount' => 'numberField',
                    'description' => 'tinymce'
                ]
            ]
        ];
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            case 'fromBuildingMeasurementSheet':
                return [
                    'columns' => [
                        'calculation', 'amount'
                    ]
                ];
            default:
                return [
                'columns' => [
                    'building_measurement_sheet', 'calculation', 'amount'
                ]
            ];
        }
    }
}