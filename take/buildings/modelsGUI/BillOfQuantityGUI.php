<?php

class BillOfQuantityGUI extends SIMAActiveRecordGUI
{
    public function columnLabels()
    {
        return [
            'name' => Yii::t('BuildingsModule.BillOfQuantity', 'Name'),
            'theme_id' => Yii::t('BuildingsModule.BillOfQuantity', 'Theme'),
            'theme' => Yii::t('BuildingsModule.BillOfQuantity', 'Theme'),
            'object_ids' => Yii::t('BuildingsModule.BillOfQuantity', 'Objects'),
            'has_object_value_per_unit' => Yii::t('BuildingsModule.BillOfQuantity', 'HasObjectValuePerUnit'),
            'user_ids' => Yii::t('BuildingsModule.BillOfQuantityToUser', 'Users'),
            'is_private' => Yii::t('BuildingsModule.BillOfQuantity', 'IsPrivate'),
            'comment' => Yii::t('BuildingsModule.BillOfQuantity', 'Comment'),
            'owner_id' => Yii::t('BuildingsModule.BillOfQuantity', 'Owner'),
            'owner' => Yii::t('BuildingsModule.BillOfQuantity', 'Owner'),
            'locked' => Yii::t('BuildingsModule.BillOfQuantity', 'Locked'),
            'locked_by_user_id' => Yii::t('BuildingsModule.BillOfQuantity', 'LockedByUser'),
            'locked_by_user' => Yii::t('BuildingsModule.BillOfQuantity', 'LockedByUser'),
            'locked_timestamp' => Yii::t('BuildingsModule.BillOfQuantity', 'LockedTimestamp'),
            'form_attribute_has_objects' => Yii::t('BuildingsModule.BillOfQuantity', 'HasObjects')
        ] + parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return Yii::t('BuildingsModule.BillOfQuantity', $plural ? 'BillsOfQuantities' : 'BillOfQuantity');
    }
    
    public function modelForms()
    {
        $owner = $this->owner;

        $form_attribute_has_objects = false;
        if (!$owner->isNewRecord && $owner->objects_count > 1)
        {
            $form_attribute_has_objects = true;
        }
        
        return [
            'default' => [
                'type' => 'generic',
                'columns' => [
                    'name' => 'textField',
                    'theme_id' => 'hidden',
                    'owner_id' => ['relation', 'relName' => 'owner'],
                    'form_attribute_has_objects' => ['checkBox', 'value' => $form_attribute_has_objects ? 'checked' : ''],
                    'object_ids' => ['list',
                        'model' => BillOfQuantityObject::class,
                        'relName' => 'objects_without_root',
                        'add_button_params' => [
                            'scenario' => 'from_bill_of_quantity',
                            'init_data' => [
                                BillOfQuantityObject::class => [
                                    'bill_of_quantity_id' => $owner->id
                                ]
                            ]
                        ],
                        'dependent_on' => [
                            'form_attribute_has_objects' => [
                                'onValue' => ['show', 'for_values' => true]
                            ]
                        ]
                    ],
                    'user_ids' => ['list',
                        'model' => User::class,
                        'relName' => 'users',
                        'multiselect' => true
                    ],
                    'has_object_value_per_unit' => 'dropdown',
                    'comment' => 'textArea',
                    'is_private' => 'checkBox'
                ]
            ],
            'forClone' => [
                'type' => 'generic',
                'columns' => [
                    'name' => 'textField',
                    'theme_id' => ['relation', 'relName' => 'theme', 'add_button' => true],
                    'owner_id' => ['relation', 'relName' => 'owner'],
                    'user_ids' => ['list',
                        'model' => User::class,
                        'relName' => 'users',
                        'multiselect' => true
                    ],
                    'has_object_value_per_unit' => 'dropdown',
                    'comment' => 'textArea',
                    'is_private' => 'checkBox'
                ]
            ]
        ];
    }
    
    public function guiTableSettings($type)
    {   
        switch ($type)
        {
            case 'inTheme':
                return [
                    'columns' => [
                        'name', 'has_object_value_per_unit', 'comment', 'is_private', 'owner', 'locked'
                    ]
                ];
            default:
                return [
                'columns' => [
                    'name', 'theme', 'has_object_value_per_unit', 'comment', 'is_private', 'owner', 'locked'
                ]
            ];
        }
    }
    
    public function vueProp(string $prop, array $scopes = []):array
    {
        $owner = $this->owner;

        switch ($prop)
        {
            case 'first_level_groups':
                $prop_value = [
                    'TYPE' => 'MODEL_ARRAY',
                    'ARRAY' => SIMAMisc::getVueModelTags($owner->first_level_groups)
                ];
                
                return [$prop_value, SIMAMisc::getModelTags($owner->groups)];
            case 'leaf_objects':
                $prop_value = [
                    'TYPE' => 'MODEL_ARRAY',
                    'ARRAY' => SIMAMisc::getVueModelTags($owner->leaf_objects)
                ];
                
                return [$prop_value, SIMAMisc::getModelTags($owner->objects)];
            case 'max_quantity_for_object':
                $result = $this->getMaxColumnValueAndUpdateModelTags('quantity', $scopes);

                return [$result['max_value'], $result['update_models']];
            case 'max_value_for_object':
                $result = $this->getMaxColumnValueAndUpdateModelTags('value', $scopes);

                return [$result['max_value'], $result['update_models']];
            case 'max_value_per_unit_for_object':
                $result = $this->getMaxColumnValueAndUpdateModelTags('value_per_unit', $scopes);

                return [$result['max_value'], $result['update_models']];
            case 'max_value_per_unit':
                $root_group = $owner->root_group;

                return [$root_group->value_per_unit->getValue(), SIMAMisc::getModelTags([$root_group])];
            case 'max_quantity_done_for_object':
                $result = $this->getMaxColumnValueAndUpdateModelTags('quantity_done', $scopes);

                return [$result['max_value'], $result['update_models']];
            default: return parent::vueProp($prop, $scopes);
        }
    }
    
    public function vuePropValue($prop)
    {
        $owner = $this->owner;

        switch ($prop)
        {
            case 'has_object_value_per_unit_display': 
                return Yii::t('BaseModule.Common', $owner->has_object_value_per_unit ? 'Yes' : 'No');
            case 'is_private_display': 
                return Yii::t('BaseModule.Common', $owner->is_private ? 'Yes' : 'No');
            case 'has_change_access':
                return !empty(BillOfQuantity::model()->hasChangeAccess(Yii::app()->user->id)->findByPk($owner->id));
                
            default: return parent::vuePropValue($prop);
        }
    }
    
    public function droplist($key, $relName='', $filter=null) 
    {
        switch ($key)
        {
            case 'is_private' : return [
                true => Yii::t('BaseModule.Common', 'Yes'),
                false => Yii::t('BaseModule.Common', 'No')
            ];
            case 'locked' : return [
                true => Yii::t('BaseModule.Common', 'Yes'),
                false => Yii::t('BaseModule.Common', 'No')
            ];
            case 'has_object_value_per_unit' : return [
                true => Yii::t('BaseModule.Common', 'Yes'),
                false => Yii::t('BaseModule.Common', 'No')
            ];
            default: return parent::droplist($key, $relName, $filter);
        }
    }
    
    private function getMaxColumnValueAndUpdateModelTags($column_name, $scopes)
    {
        $owner = $this->owner;
        
        $max_value = 0;
        $update_models = [];
        if (!empty($scopes['forObject']))
        {
            $object_id = $scopes['forObject'];

            $root_group_to_object = $owner->getRootGroupToObject($object_id);
            if (!empty($root_group_to_object))
            {
                $max_value = $root_group_to_object->$column_name->getValue();
                $update_models = [$root_group_to_object];
            }
        }
        
        return [
            'max_value' => $max_value,
            'update_models' => SIMAMisc::getModelTags($update_models)
        ];
    }
}