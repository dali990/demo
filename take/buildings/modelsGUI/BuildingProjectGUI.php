<?php

class BuildingProjectGUI extends SIMAActiveRecordGUI
{

    public function columnLabels()
    {
        return array(
            'building_project_type_id'=>'Vrsta projekta',
            'building_project_type'=>'Vrsta projekta',
//            'building_project_kind_id'=>'Deo projekta',
            'parent_id'=>'Nad projekat',
            'theme_id'=>ThemeGUI::modelLabel(),
            'theme'=>ThemeGUI::modelLabel(),
            'geo_object_id' => GeoObjectGUI::modelLabel(),
            'geo_object' => GeoObjectGUI::modelLabel(),
            'date'=>'Datum',
            'number'=>'Broj',
            '_number'=>'Broj',
            'order'=>Yii::t('BuildingsModule.BuildingProject', 'Order'),
            'prefix'=>'Prefix',
            'building_project_sheet_id'=>'Korica',
            'company_licenses_list'=>'Velike licence',            
            'responsible_designers_decision' => Yii::t('BuildingsModule.BuildingProject', 'ResponsibleDesignersDecision'),
            'responsible_designer_licence' => Yii::t('BuildingsModule.BuildingProject', 'ResponsibleDesigner'),
            'responsible_designer_licence_id' => Yii::t('BuildingsModule.BuildingProject', 'ResponsibleDesigner'),
            'booksCheckBoxes' => Yii::t('BuildingsModule.BuildingProject', 'booksCheckBoxes'),
            'NumberFull' => 'Broj'
        )+parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return Yii::t('BuildingsModule.BuildingProject', $plural?'BuildingProjects':'BuildingProject');
    }
    
    public function columnDisplays($column)
    {
        $owner = $this->owner;
        switch ($column)
        {
            case 'prefix' :
                $prefix_displays = $owner->getPrefixArray();
                $result = $prefix_displays[$owner->prefix];
                return $result;
            case 'responsible_designers_decision':
                $result = '';

                if(empty($owner->responsible_designers_decision))
                {
                    if($owner->isBaseProject)
                    {
                        $result .= Yii::app()->controller->widget('SIMAButton', [
                            'action' => [
                                'title' => 'nadji postojeci fajl',
                                'onclick' => ['sima.buildings.addResponsibleDesignersDecision',$owner->id]
                            ]
                        ], true);
                        $result .= Yii::app()->controller->widget('SIMAButton', [
                            'action' => [
                                'title' => 'Generisi novi',
                                'onclick' => ['sima.buildings.generateResponsibleDesignersDecision',$owner->id]
                            ]
                        ], true);
                    }
                    else
                    {
                        $result = Yii::t('BuildingsModule.BuildingProject', 'DecisionIsGeneratedFromBaseProject');
                    }
                }
//                else
//                {
//                    $result = SIMAHtml::fileOpen($owner->responsible_designers_decision);
//                    
//                    $result .= $owner->responsible_designers_decision->DisplayHTML;
//
//                    $result .= SIMAHtml::simpleButton(
//                            Yii::t('BuildingsModule.BuildingProject', 'GenerateResponsibleDesignersDecision'), 
//                            'sima.buildings.generateResponsibleDesignersDecision('.$owner->id.');'
//                    );
//                }
//                
//                if($owner->isBaseProject)
//                {
//                    $result .= SIMAHtml::simpleButton(
//                            Yii::t('BuildingsModule.BuildingProject', 'GenerateResponsibleDesignersSubDecision'), 
//                            'sima.buildings.generateResponsibleDesignersSubDecision('.$owner->id.');'
//                    );
//                }

                return $result;
            default : return parent::columnDisplays($column);
        }
    }
    
    public function modelViews()
    {
        return array(
            'basic_info',
            'full_info'=>[],
            'buildingProjectTree',
            'info_in_content' => ['params_function' => 'paramsInfoInContent']
        )+parent::modelViews();
    }
    
    public function paramsInfoInContent()
    {
        $owner = $this->owner;
        $options = '';
        if (!in_array($owner->prefix, BuildingProject::$ADDITIONAL_CONTENT_PREFIXES))
        {
            if (isset($owner->responsible_designer_licence))
            {
                $responsible_designer_licence = 'Odgovorno lice: '.$owner->responsible_designer_licence->DisplayHTML;
            }
            else
            {
                $responsible_designer_licence = 'Odgovorno lice nije postavljeno ';
            }
            $options .= SIMAHtml::modelFormOpen($owner);
        }
        else 
        {
            $responsible_designer_licence = '';
        }
        
        
        
        return [
            'responsible_designer_licence' => $responsible_designer_licence,
            'options' => $options
        ];
    }
    
    public function modelForms()
    {
        $owner = $this->owner;
        if($owner->isBaseProject === true)
        {
            $project_books = Yii::app()->getModule('buildings')->params['project_books'];
            
            $children_order_numbers = $owner->childrenBooksOrderNumbers();
                        
            $elements_array = [];
            
            foreach($project_books as $key => $value)
            {
                $elements_array[$key] = [
                    'display' => $value['order'].' : '.$value['display_name']
                ];

                if(in_array($value['order'], $children_order_numbers))
                {
                    $elements_array[$key]['isChecked'] = true;
                }
                $elements_array[$key]['aditional_params'] = [
                    'display_name' => $value['display_name'],
                    'order' => $value['order']
                ];
            }
            
            //ne mozes da menjas ako nisi koordinator
            if (
                    $owner->isNewRecord ||
                    (isset($owner->theme) && $owner->theme->coordinator_id != Yii::app()->user->id)
                )
            {
                $_theme_id = ['relation', 'relName'=>'theme', 'disabled' => 'disabled'];
            }
            else
            {
                $_theme_id = ['relation', 'relName'=>'theme'];
            }
                
            $columns = [
                'name'=>'textField',
//                'date'=>'datetimeField',
//                'theme_id'=>'display',//array('relation', 'relName'=>'job'),
                'theme_id' => $_theme_id,
                'building_project_type_id'=>['dropdown','relName'=>'building_project_type','empty'=>'Izaberi'],
//                'booksCheckBoxes'=>['checkBoxList', 'elements_array' => $elements_array],
                'geo_object_id'=>['searchField', 'relName' => 'geo_object', 'add_button' => true],
                'company_licenses_list'=>array('list',
                    'model'=>'CompanyLicenseToCompany',
                    'relName'=>'company_licenses',                         
                    'multiselect'=>true
                )
            ];
            
        }
        else
        {
            $columns = [
                'name'=>'textField',
                'parent_id'=>'display',
                'theme_id'=>'display',
                'building_project_type_id'=>'display',
//                'building_project_kind_id'=>array('dropdown','relName'=>'building_project_kind','empty'=>'Izaberi'),
            ];
            
            if($this->owner->prefix === BuildingProject::$PREFIX_BOOK)
            {
//                $columns['name'] = 'display';
                $columns['responsible_designer_licence_id'] = ['searchField','relName'=>'responsible_designer_licence'];
                $columns['prefix'] = 'display';
                $columns['order'] = 'display';
            }
            else
            {
                $columns['prefix'] = ['dropdown', 'default'=>''];
            }
            
            if($this->owner->prefix === BuildingProject::$PREFIX_NOTEBOOK)
            {
                $columns['order'] = 'textField';
            }
        }
        
        return array(
            'default'=>array(
                'type'=>'generic',
                'columns'=>$columns
            ),
            'withOutParent'=>array(
                'type'=>'generic',
                'columns'=>$columns
            ),
            'changeNumber'=>array(
                'type'=>'generic',
                'columns'=>array(
                    '_number'=>'textField',
                )
            )
        );
    }
    
     public function droplist($key, $relName='', $filter=null) {
        switch ($key) {
            case 'prefix':
                $prefix_displays = ['' => Yii::t('BaseModule.Common', 'Choose')] + $this->owner->getPrefixArray();
                return $prefix_displays;
            default: return parent::droplist($key, $relName, $filter);
        }
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            case 'inTheme': return [
                'columns' => [
                    'NumberFull', 'building_project_type', 'name', 'geo_object'
                ]
            ];
            default: return [
                'columns' => [
                    'NumberFull', 'building_project_type', 'name', 'geo_object','theme'
                ]
            ];
        };
    }
}