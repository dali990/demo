<?php

class CompanyLicenseToProjectGUI extends SIMAActiveRecordGUI
{

    public function columnLabels()
    {
        return array(
            'company_license' => 'Velika licenca',
            'company_license_id' => 'Velika licenca',
            'project_id' => 'Projekat',
            'project' => 'Projekat',
        ) + parent::columnLabels();
    }

    public function modelLabel($plural = false)
    {
        return 'Velika licenca projekta';
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default: return array(
                'columns'=>array(
                    'company_license','project'
                ),
                'order_by' => array('company_license')
            );
        }
    }
    
    public function modelForms() {
        return array(
            'default' => array(
                'type' => 'generic',
                'columns' => array(
                    'company_license_id'=>array('relation','relName'=>'company_license'),
                    'project_id'=>array('relation','relName'=>'project'),
                )
            )
        );
    }

}

?>
