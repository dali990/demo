<?php

class ConstructionMaterialApprovalGUI extends SIMAActiveRecordGUI
{
    public function columnLabels()
    {
        return array(
            'file' => Yii::t('BuildingsModule.ConstructionMaterialApproval', 'File'),
            'code' => Yii::t('BuildingsModule.ConstructionMaterialApproval', 'Code'),
            'theme_id' => Yii::t('BuildingsModule.ConstructionMaterialApproval', 'Theme'),
            'theme' => Yii::t('BuildingsModule.ConstructionMaterialApproval', 'Theme'),
            'material_name' => Yii::t('BuildingsModule.ConstructionMaterialApproval', 'MaterialName'),
            'manufacturer_id' => Yii::t('BuildingsModule.ConstructionMaterialApproval', 'Manufacturer'),
            'manufacturer' => Yii::t('BuildingsModule.ConstructionMaterialApproval', 'Manufacturer'),
            'type' => Yii::t('BuildingsModule.ConstructionMaterialApproval', 'Type'),
            'part_of_id' => Yii::t('BuildingsModule.ConstructionMaterialApproval', 'PartOf'),
            'part_of' => Yii::t('BuildingsModule.ConstructionMaterialApproval', 'PartOf'),
            'used_in' => Yii::t('BuildingsModule.ConstructionMaterialApproval', 'UsedIn'),
            'location' => Yii::t('BuildingsModule.ConstructionMaterialApproval', 'Location'),
            'tehnical_charactetistics' => Yii::t('BuildingsModule.ConstructionMaterialApproval', 'TehnicalCharactetistics'),
            'approved' => Yii::t('BuildingsModule.ConstructionMaterialApproval', 'Approved'),
            'warehouse_material_id' => Yii::t('BuildingsModule.ConstructionMaterialApproval', 'WarehouseMaterial'),
            'warehouse_material' => Yii::t('BuildingsModule.ConstructionMaterialApproval', 'WarehouseMaterial'),
            'drawings_list' => Yii::t('BuildingsModule.ConstructionMaterialApproval', 'DrawingsList'),
            'atests_list' => Yii::t('BuildingsModule.ConstructionMaterialApproval', 'AtestsList'),
        )+parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return Yii::t('BuildingsModule.ConstructionMaterialApproval', 'ConstructionMaterialApproval');
    }

    public function modelViews()
    {
        return [];
    }
    
    public function modelForms()
    {
        $owner = $this->owner;
        return array(
            'default'=>array(
                'type'=>'generic',
                'columns'=>array(
                    'code' => ['textField','placeholder'=>Yii::t('BuildingsModule.ConstructionMaterialApproval', 'CodeAutomaticFill')],
                    'theme_id' => array('searchField','relName'=>'theme','add_button'=>true),
                    'material_name' => 'textField',
                    'warehouse_material_id' => array('searchField','relName'=>'warehouse_material','add_button'=>true),
                    'manufacturer_id' => array('searchField','relName'=>'manufacturer','add_button'=>['action'=>'simaAddressbook/index']),
                    'type' => 'textField',
                    'used_in' => 'textField',
                    'part_of_id' => array('searchField','relName'=>'part_of','add_button'=>true),
                    'location' => 'textField',
                    'tehnical_charactetistics' => 'textArea',
                    'drawings_list'=>array('list',
                        'model'=>'File',
                        'relName'=>'drawings',
                        'multiselect'=>true
                    ),
                    'atests_list'=>array('list',
                        'model'=>'File',
                        'relName'=>'atests',
                        'multiselect'=>true
                    ),
                )
            )
        );
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            case 'inTheme': return[
                'columns'=>['file', 'code', 'material_name', 'warehouse_material', 'manufacturer', 'type', 'part_of', 'used_in', 'location', 'tehnical_charactetistics']
            ];
            default: return [
                'columns'=>['file', 'code', 'theme', 'material_name', 'warehouse_material', 'manufacturer', 'type', 'part_of', 'used_in', 'location', 'tehnical_charactetistics']
            ];
        }
    }
}