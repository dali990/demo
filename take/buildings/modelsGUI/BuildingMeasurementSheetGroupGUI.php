<?php

class BuildingMeasurementSheetGroupGUI extends SIMAActiveRecordDisplayGUI
{
    public function columnLabels()
    {
        return [
            'building_interim_bill_id' => Yii::t('BuildingsModule.BuildingInterimBill', 'BuildingInterimBill'),
            'building_interim_bill' => Yii::t('BuildingsModule.BuildingInterimBill', 'BuildingInterimBill'),
            'bill_of_quantity_item_id' => Yii::t('BuildingsModule.BillOfQuantityItem', 'BillOfQuantityItem'),
            'bill_of_quantity_item' => Yii::t('BuildingsModule.BillOfQuantityItem', 'BillOfQuantityItem'),
            'object_id' => Yii::t('BuildingsModule.BillOfQuantityObject', 'BillOfQuantityObject'),
            'object' => Yii::t('BuildingsModule.BillOfQuantityObject', 'BillOfQuantityObject'),
            'value_done' => Yii::t('BuildingsModule.BuildingMeasurementSheet', 'ValueDone'),
            'quantity_done' => Yii::t('BuildingsModule.BuildingMeasurementSheet', 'QuantityDone'),
            'pre_value_done' => Yii::t('BuildingsModule.BuildingInterimBill', 'PreInterimBillValueDone'),
            'pre_quantity_done' => Yii::t('BuildingsModule.BuildingInterimBill', 'PreInterimBillQuantityDone')
        ] + parent::columnLabels();
    }
}