<?php

class BillOfQuantityItemGUI extends SIMAActiveRecordGUI
{
    public function columnLabels()
    {
        return [
            'name' => Yii::t('BuildingsModule.BillOfQuantityItem', 'Name'),
            'group_id' => Yii::t('BuildingsModule.BillOfQuantityItemGroup', 'BillOfQuantityItemGroup'),
            'group' => Yii::t('BuildingsModule.BillOfQuantityItemGroup', 'BillOfQuantityItemGroup'),
            'order' => Yii::t('BuildingsModule.BillOfQuantityItem', 'Order'),
            'measurement_unit_id' => Yii::t('BuildingsModule.BillOfQuantityItem', 'MeasurementUnit'),
            'measurement_unit' => Yii::t('BuildingsModule.BillOfQuantityItem', 'MeasurementUnit'),
            'quantity' => Yii::t('BuildingsModule.BillOfQuantityItem', 'Quantity'),
            'value_per_unit' => Yii::t('BuildingsModule.BillOfQuantityItem', 'ValuePerUnit'),
            'value' => Yii::t('BuildingsModule.BillOfQuantityItem', 'Value'),
            'note' => Yii::t('BuildingsModule.BillOfQuantityItem', 'Note'),
        ] + parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return Yii::t('BuildingsModule.BillOfQuantityItem', $plural ? 'BillOfQuantitiesItems' : 'BillOfQuantityItem');
    }
    
    public function modelForms()
    {
        $owner = $this->owner;

        $order_field = [
            'textField',
            'placeholder' => Yii::t('BuildingsModule.BillOfQuantityItem','LeaveOrderEmptyForAutomaticFill')
        ];

        $group_id_field = ['relation', 'relName' => 'group', 'add_button' => true];
        if (!empty($owner->group_id))
        {
            $group = BillOfQuantityItemGroup::model()->findByPkWithCheck($owner->group_id);
            $group_id_field['filters'] = [
                'bill_of_quantity' => [
                    'ids' => $group->bill_of_quantity_id
                ]
            ];
            $group_id_field['add_button'] = [
                'params' => [
                    'add_button' => [
                        'init_data' => [
                            BillOfQuantityItemGroup::class => [
                                'bill_of_quantity_id' => $group->bill_of_quantity_id
                            ]
                        ]
                    ]
                ]
            ];
        }
        
        return [
            'default' => [
                'type' => 'generic',
                'columns' => [
                    'name' => 'textArea',
                    'order' => $order_field,
                    'group_id' => $group_id_field,
                    'measurement_unit_id' => ['relation', 'relName' => 'measurement_unit', 'add_button' => true],
                    'note' => 'textArea'
                ]
            ]
        ];
    }
    
    public function guiTableSettings($type)
    {   
        switch ($type)
        {
            default:
                return [
                'columns' => [
                    'name', 'order', 'group', 'measurement_unit', 'quantity', 'value_per_unit', 'value', 'note'
                ]
            ];
        }
    }

    public function vuePropValue($prop)
    {
        $owner = $this->owner;

        switch ($prop)
        {
            case 'curr_user_model_options': 
                return $owner->getModelOptions(Yii::app()->user->model);
            case 'value_per_unit_display': 
                $rounded_value_per_unit = $owner->value_per_unit->round(2);
                return $rounded_value_per_unit->getValue();
                
            default: return parent::vuePropValue($prop);
        }
    }
}