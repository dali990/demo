<?php

class m0000000080_00001_construction_add_boss extends CDbMigration
{
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        Yii::app()->db->createCommand(
<<<'SIMAMIGRATESQL'
            ALTER TABLE buildings.constructions add column construction_boss_id integer;
            ALTER TABLE buildings.constructions
                ADD CONSTRAINT constructions_construction_boss_id_fkey FOREIGN KEY (construction_boss_id)
                    REFERENCES admin.users (id) MATCH SIMPLE
                    ON UPDATE CASCADE ON DELETE RESTRICT;
            UPDATE buildings.constructions bc set construction_boss_id = (select coordinator_id from base.themes where id=bc.id and coordinator_id is not null);

            CREATE TABLE buildings.constructions_to_boss_assistants
            (
                id serial NOT NULL,
                building_construction_id integer NOT NULL,
                boss_assistant_id integer NOT NULL,
                CONSTRAINT constructions_to_boss_assistants_pkey PRIMARY KEY (id),
                CONSTRAINT constructions_to_boss_assistants_building_construction_id_fkey FOREIGN KEY (building_construction_id)
                    REFERENCES buildings.constructions (id) MATCH SIMPLE
                    ON UPDATE CASCADE ON DELETE RESTRICT,
                CONSTRAINT constructions_to_boss_assistants_boss_assistant_id_fkey FOREIGN KEY (boss_assistant_id)
                    REFERENCES admin.users (id) MATCH SIMPLE
                    ON UPDATE CASCADE ON DELETE RESTRICT,
                CONSTRAINT constructions_to_boss_assistants_building_construction_id_boss_assistant_id_key UNIQUE (building_construction_id, boss_assistant_id)
            );
            CREATE TRIGGER i_am_called_by_everyone
                AFTER INSERT OR UPDATE OR DELETE
                ON buildings.constructions_to_boss_assistants
                FOR EACH ROW
                EXECUTE PROCEDURE public.i_am_called_by_everyone();
                
            ALTER TABLE buildings.bills_of_quantities 
                ADD COLUMN owner_id integer,
                ADD COLUMN locked boolean not null default false,
                ADD COLUMN locked_by_user_id integer,
                ADD COLUMN locked_timestamp timestamp without time zone,
                ADD CONSTRAINT bills_of_quantities_owner_id_fkey FOREIGN KEY (owner_id)
                    REFERENCES admin.users (id) MATCH SIMPLE
                    ON UPDATE CASCADE ON DELETE RESTRICT,
                ADD CONSTRAINT bills_of_quantities_locked_by_user_id_fkey FOREIGN KEY (locked_by_user_id)
                    REFERENCES admin.users (id) MATCH SIMPLE
                    ON UPDATE CASCADE ON DELETE RESTRICT;
                    
            ALTER TABLE buildings.interim_bills DROP CONSTRAINT interim_bills_status_check;
            ALTER TABLE buildings.interim_bills
                ADD CONSTRAINT interim_bills_status_check CHECK (
                    status = ANY (ARRAY[
                        'PREPARATION'::text, 
                        'WRITTEN_MEASUREMENT_SHEETS'::text, 
                        'CONFIRMED_MEASUREMENT_SHEETS'::text, 
                        'DELIVERED_MEASUREMENT_SHEETS'::text, 
                        'VALIDATED_MEASUREMENT_SHEETS'::text, 
                        'DELIVERED'::text, 
                        'VALIDATED'::text, 
                        'PAID'::text
                    ]));
                    
            ALTER TABLE buildings.measurement_sheets 
                ADD COLUMN quantity_validated numeric(20,2) NOT NULL DEFAULT 0,
                ADD COLUMN written boolean not null default false,
                ADD COLUMN written_by_user_id integer,
                ADD COLUMN written_timestamp timestamp without time zone,
                ADD CONSTRAINT measurement_sheets_written_by_user_id_fkey FOREIGN KEY (written_by_user_id)
                    REFERENCES admin.users (id) MATCH SIMPLE
                    ON UPDATE CASCADE ON DELETE RESTRICT,
                ADD COLUMN confirmed boolean not null default false,
                ADD COLUMN confirmed_by_user_id integer,
                ADD COLUMN confirmed_timestamp timestamp without time zone,
                ADD CONSTRAINT measurement_sheets_confirmed_by_user_id_fkey FOREIGN KEY (confirmed_by_user_id)
                    REFERENCES admin.users (id) MATCH SIMPLE
                    ON UPDATE CASCADE ON DELETE RESTRICT,
                ADD COLUMN validated boolean not null default false,
                ADD COLUMN validated_by_user_id integer,
                ADD COLUMN validated_timestamp timestamp without time zone,
                ADD CONSTRAINT measurement_sheets_validated_by_user_id_fkey FOREIGN KEY (validated_by_user_id)
                    REFERENCES admin.users (id) MATCH SIMPLE
                    ON UPDATE CASCADE ON DELETE RESTRICT;
            
SIMAMIGRATESQL
        )->execute();
    }

    public function safeDown()
    {
//        Yii::app()->db->createCommand(
//<<<'SIMAMIGRATESQL'
//
//SIMAMIGRATESQL
//        )->execute();
        echo "m0000000080_00001_construction_add_boss does not support migration down.\n";
        return false;
    }
}