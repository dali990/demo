<?php

class m0000000010_00024_move_material_approvals_to_buildings extends CDbMigration
{
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        Yii::app()->db->createCommand(
"
ALTER TABLE geo.construction_material_approvals
  SET SCHEMA buildings;

ALTER TABLE geo.construction_material_approvals_to_drawings
  SET SCHEMA buildings;

"
        )->execute();
    }

    public function safeDown()
    {
//        Yii::app()->db->createCommand(
//"
//
//"
//        )->execute();
        echo "m0000000010_00024_move_material_approvals_to_buildings does not support migration down.\n";
        return false;
    }
}