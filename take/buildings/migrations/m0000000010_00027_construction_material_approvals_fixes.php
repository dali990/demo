<?php

class m0000000010_00027_construction_material_approvals_fixes extends CDbMigration
{
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        Yii::app()->db->createCommand('SET search_path = buildings, pg_catalog;'.'
                CREATE TABLE construction_material_approvals_to_files
                (
                  construction_material_approval_id integer NOT NULL,
                  file_id integer NOT NULL,
                  CONSTRAINT construction_material_approvals_to_files_pkey PRIMARY KEY (construction_material_approval_id, file_id),
                  CONSTRAINT construction_material_approvals_to_files_cma_id_fkey FOREIGN KEY (construction_material_approval_id)
                      REFERENCES buildings.construction_material_approvals (id) MATCH SIMPLE
                      ON UPDATE CASCADE ON DELETE RESTRICT,
                  CONSTRAINT construction_material_approvals_to_files_file_id_fkey FOREIGN KEY (file_id)
                      REFERENCES files.files (id) MATCH SIMPLE
                      ON UPDATE CASCADE ON DELETE RESTRICT
                );
                CREATE TRIGGER i_am_called_by_everyone
                AFTER INSERT OR UPDATE OR DELETE
                ON buildings.construction_material_approvals_to_files
                FOR EACH ROW
                EXECUTE PROCEDURE public.i_am_called_by_everyone();

            '. 'SET search_path = public, pg_catalog;')->execute();
    }

    public function safeDown()
    {
        Yii::app()->db->createCommand('SET search_path = buildings, pg_catalog;'
                . '
                
                DROP TABLE construction_material_approvals_to_files;
'           
            . 'SET search_path = public, pg_catalog;')->execute();
        
//        echo "m0000000010_00027_construction_material_approvals_fixes does not support migration down.\n";
//        return false;
    }
}