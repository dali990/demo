<?php

class m0000000084_00000_measurement_sheet_group_add_columns extends CDbMigration
{
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        Yii::app()->db->createCommand(
<<<'SIMAMIGRATESQL'
            ALTER TABLE buildings.measurement_sheet_groups 
                ADD COLUMN measurement_sheets_written boolean not null default false,
                ADD COLUMN measurement_sheets_confirmed boolean not null default false;
SIMAMIGRATESQL
        )->execute();
    }

    public function safeDown()
    {
//        Yii::app()->db->createCommand(
//<<<'SIMAMIGRATESQL'
//
//SIMAMIGRATESQL
//        )->execute();
        echo "m0000000084_00000_measurement_sheet_group_add_columns does not support migration down.\n";
        return false;
    }
}