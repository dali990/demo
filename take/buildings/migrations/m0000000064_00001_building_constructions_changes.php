<?php

class m0000000064_00001_building_constructions_changes extends CDbMigration
{
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        Yii::app()->db->createCommand(
<<<'SIMAMIGRATESQL'
            DROP TABLE buildings.constructions;
                
            CREATE TABLE buildings.constructions
            (
                id integer NOT NULL,
                constructor_id integer NOT NULL,
                investor_id integer NOT NULL,
                bill_of_quantity_id integer NOT NULL,
                responsible_engineer_id integer NOT NULL,
                start_date date NOT NULL DEFAULT now(),
                CONSTRAINT constructions_pkey PRIMARY KEY (id),
                CONSTRAINT constructions_id_fkey FOREIGN KEY (id)
                    REFERENCES base.themes (id) MATCH SIMPLE
                    ON UPDATE CASCADE ON DELETE RESTRICT,
                CONSTRAINT constructions_constructor_id_fkey FOREIGN KEY (constructor_id)
                    REFERENCES public.companies (id) MATCH SIMPLE
                    ON UPDATE CASCADE ON DELETE RESTRICT,
                CONSTRAINT constructions_investor_id_fkey FOREIGN KEY (investor_id)
                    REFERENCES public.companies (id) MATCH SIMPLE
                    ON UPDATE CASCADE ON DELETE RESTRICT,
                CONSTRAINT constructions_bill_of_quantity_id_fkey FOREIGN KEY (bill_of_quantity_id)
                    REFERENCES buildings.bills_of_quantities (id) MATCH SIMPLE
                    ON UPDATE CASCADE ON DELETE RESTRICT,
                CONSTRAINT constructions_responsible_engineer_id_fkey FOREIGN KEY (responsible_engineer_id)
                    REFERENCES public.persons (id) MATCH SIMPLE
                    ON UPDATE CASCADE ON DELETE RESTRICT,
                CONSTRAINT constructions_bill_of_quantity_id_uniq UNIQUE(bill_of_quantity_id)
            );
            CREATE TRIGGER i_am_called_by_everyone
                AFTER INSERT OR UPDATE OR DELETE
                ON buildings.constructions
                FOR EACH ROW
                EXECUTE PROCEDURE public.i_am_called_by_everyone();
                
            CREATE TABLE buildings.interim_bills
            (
                id integer NOT NULL,
                building_construction_id integer NOT NULL,
                CONSTRAINT interim_bills_pkey PRIMARY KEY (id),
                CONSTRAINT interim_bills_id_fkey FOREIGN KEY (id)
                    REFERENCES files.files (id) MATCH SIMPLE
                    ON UPDATE CASCADE ON DELETE RESTRICT,
                CONSTRAINT interim_bills_building_construction_id_fkey FOREIGN KEY (building_construction_id)
                    REFERENCES buildings.constructions (id) MATCH SIMPLE
                    ON UPDATE CASCADE ON DELETE RESTRICT
            );
            CREATE TRIGGER i_am_called_by_everyone
                AFTER INSERT OR UPDATE OR DELETE
                ON buildings.interim_bills
                FOR EACH ROW
                EXECUTE PROCEDURE public.i_am_called_by_everyone();

            CREATE TABLE buildings.measurement_sheets
            (
                id serial NOT NULL,
                building_interim_bill_id integer NOT NULL,
                bill_of_quantity_item_id integer NOT NULL,
                CONSTRAINT measurement_sheets_pkey PRIMARY KEY (id),
                CONSTRAINT measurement_sheets_building_interim_bill_id_fkey FOREIGN KEY (building_interim_bill_id)
                    REFERENCES buildings.interim_bills (id) MATCH SIMPLE
                    ON UPDATE CASCADE ON DELETE RESTRICT,
                CONSTRAINT measurement_sheets_bill_of_quantity_item_id_fkey FOREIGN KEY (bill_of_quantity_item_id)
                    REFERENCES buildings.bill_of_quantities_items (id) MATCH SIMPLE
                    ON UPDATE CASCADE ON DELETE RESTRICT,
                CONSTRAINT measurement_sheets_building_interim_bill_id_bill_of_quantity_item_id_uniq UNIQUE(building_interim_bill_id, bill_of_quantity_item_id)
            );
            CREATE TRIGGER i_am_called_by_everyone
                AFTER INSERT OR UPDATE OR DELETE
                ON buildings.measurement_sheets
                FOR EACH ROW
                EXECUTE PROCEDURE public.i_am_called_by_everyone();
                
            CREATE TABLE buildings.measurement_sheet_items
            (
                id serial NOT NULL,
                building_measurement_sheet_id integer NOT NULL,
                object_id integer NOT NULL,
                calculation text,
                amount numeric(20,2) NOT NULL DEFAULT 0,
                CONSTRAINT measurement_sheet_items_pkey PRIMARY KEY (id),
                CONSTRAINT measurement_sheet_items_building_measurement_sheet_id_fkey FOREIGN KEY (building_measurement_sheet_id)
                    REFERENCES buildings.measurement_sheets (id) MATCH SIMPLE
                    ON UPDATE CASCADE ON DELETE RESTRICT,
                CONSTRAINT measurement_sheet_items_object_id_fkey FOREIGN KEY (object_id)
                    REFERENCES buildings.bill_of_quantities_objects (id) MATCH SIMPLE
                    ON UPDATE CASCADE ON DELETE RESTRICT
            );
            CREATE TRIGGER i_am_called_by_everyone
                AFTER INSERT OR UPDATE OR DELETE
                ON buildings.measurement_sheet_items
                FOR EACH ROW
                EXECUTE PROCEDURE public.i_am_called_by_everyone();
                
            ALTER TABLE buildings.bill_of_quantities_items_to_objects ADD COLUMN value_done numeric(22,2) NOT NULL DEFAULT 0;
            ALTER TABLE buildings.bill_of_quantities_item_groups_to_objects ADD COLUMN value_done numeric(22,2) NOT NULL DEFAULT 0;
SIMAMIGRATESQL
        )->execute();
    }

    public function safeDown()
    {
//        Yii::app()->db->createCommand(
//<<<'SIMAMIGRATESQL'
//
//SIMAMIGRATESQL
//        )->execute();
        echo "m0000000064_00001_building_constructions_changes does not support migration down.\n";
        return false;
    }
}