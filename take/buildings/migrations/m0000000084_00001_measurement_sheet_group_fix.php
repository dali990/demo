<?php

class m0000000084_00001_measurement_sheet_group_fix extends CDbMigration
{
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        Yii::app()->db->createCommand(
<<<'SIMAMIGRATESQL'
            ALTER TABLE buildings.measurement_sheet_groups ALTER COLUMN measurement_sheets_written SET DEFAULT TRUE;
            ALTER TABLE buildings.measurement_sheet_groups ALTER COLUMN measurement_sheets_confirmed SET DEFAULT TRUE;
            
            UPDATE buildings.measurement_sheet_groups SET measurement_sheets_written = true WHERE measurement_sheets_written = false;
            UPDATE buildings.measurement_sheet_groups SET measurement_sheets_confirmed = true WHERE measurement_sheets_confirmed = false;
SIMAMIGRATESQL
        )->execute();
    }

    public function safeDown()
    {
//        Yii::app()->db->createCommand(
//<<<'SIMAMIGRATESQL'
//
//SIMAMIGRATESQL
//        )->execute();
        echo "m0000000084_00001_measurement_sheet_group_fix does not support migration down.\n";
        return false;
    }
}