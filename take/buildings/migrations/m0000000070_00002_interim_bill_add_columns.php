<?php

class m0000000070_00002_interim_bill_add_columns extends CDbMigration
{
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        Yii::app()->db->createCommand(
<<<'SIMAMIGRATESQL'
            ALTER TABLE buildings.interim_bills ADD COLUMN issue_date date;
            ALTER TABLE buildings.interim_bills ADD COLUMN order_number integer;
            ALTER TABLE buildings.interim_bills ADD COLUMN status text NOT NULL DEFAULT 'PREPARATION';
            ALTER TABLE buildings.interim_bills
                ADD CONSTRAINT interim_bills_status_check CHECK (status = ANY (ARRAY['PREPARATION'::text, 'DELIVERED'::text, 'VALIDATED'::text, 'PAID'::text]));
            ALTER TABLE buildings.interim_bills ADD COLUMN is_final boolean NOT NULL DEFAULT false;
                
            CREATE OR REPLACE FUNCTION buildings.interim_bills_issue_date_and_order_number()
                RETURNS void AS
            $BODY$
            DECLARE
                building_construction RECORD;
                interim_bill RECORD;
                i integer;
                interim_bills_cnt integer;
            BEGIN
                FOR building_construction IN (SELECT id FROM buildings.constructions)
                LOOP
                    i := 0;
                    interim_bills_cnt := (SELECT count(*) FROM buildings.interim_bills where building_construction_id = building_construction.id);
                    FOR interim_bill IN (SELECT id FROM buildings.interim_bills where building_construction_id = building_construction.id order by id desc)
                    LOOP
                        UPDATE buildings.interim_bills
                        SET issue_date = NOW() - i * interval '1 day',
                            order_number = interim_bills_cnt - i
                        WHERE id = interim_bill.id;

                        i := i + 1;
                    END LOOP;
                END LOOP;
            END;
            $BODY$
                LANGUAGE plpgsql;
                    
            SELECT buildings.interim_bills_issue_date_and_order_number();

            DROP FUNCTION buildings.interim_bills_issue_date_and_order_number();
            

            CREATE OR REPLACE FUNCTION buildings.interim_bills_order_number()
                RETURNS trigger AS
              $BODY$
                    BEGIN
                        IF NEW.order_number IS NULL
                        THEN
                            select COALESCE(MAX(bib.order_number), 0)+1 from buildings.interim_bills bib 
                            where bib.building_construction_id = NEW.building_construction_id
                            into NEW.order_number;
                        END IF;

                        RETURN NEW;
                    END;
                    $BODY$
                LANGUAGE plpgsql VOLATILE
                COST 100;
                
            CREATE TRIGGER interim_bills_order_number
                BEFORE INSERT OR UPDATE
                ON buildings.interim_bills
                FOR EACH ROW
                EXECUTE PROCEDURE buildings.interim_bills_order_number();
                
            ALTER TABLE buildings.interim_bills ALTER COLUMN issue_date SET NOT NULL;
            ALTER TABLE buildings.interim_bills ALTER COLUMN order_number SET NOT NULL;
            
            ALTER TABLE buildings.interim_bills
                ADD CONSTRAINT interim_bills_building_construction_id_issue_date_uniq UNIQUE(building_construction_id, issue_date);
                
            ALTER TABLE buildings.measurement_sheets ADD COLUMN value_done numeric(22,2) NOT NULL DEFAULT 0;
            ALTER TABLE buildings.measurement_sheets ADD COLUMN quantity_done numeric(20,2) NOT NULL DEFAULT 0;
            ALTER TABLE buildings.measurement_sheets ADD COLUMN pre_value_done numeric(22,2) NOT NULL DEFAULT 0;
            ALTER TABLE buildings.measurement_sheets ADD COLUMN pre_quantity_done numeric(20,2) NOT NULL DEFAULT 0;
            ALTER TABLE buildings.measurement_sheets 
                ADD CONSTRAINT measurement_sheets_object_id_fkey FOREIGN KEY (object_id)
                    REFERENCES buildings.bill_of_quantities_objects (id) MATCH SIMPLE
                    ON UPDATE CASCADE ON DELETE RESTRICT;
            
            CREATE TABLE buildings.measurement_sheet_groups
            (
                id serial NOT NULL,
                building_interim_bill_id integer NOT NULL,
                bill_of_quantity_item_group_id integer NOT NULL,
                object_id integer NOT NULL,
                value_done numeric(22,2) NOT NULL DEFAULT 0,
                quantity_done numeric(20,2) NOT NULL DEFAULT 0,
                pre_value_done numeric(22,2) NOT NULL DEFAULT 0,
                pre_quantity_done numeric(20,2) NOT NULL DEFAULT 0,
                CONSTRAINT measurement_sheet_groups_pkey PRIMARY KEY (id),
                CONSTRAINT measurement_sheet_groups_building_interim_bill_id_fkey FOREIGN KEY (building_interim_bill_id)
                    REFERENCES buildings.interim_bills (id) MATCH SIMPLE
                    ON UPDATE CASCADE ON DELETE RESTRICT,
                CONSTRAINT measurement_sheet_groups_bill_of_quantity_item_group_id_fkey FOREIGN KEY (bill_of_quantity_item_group_id)
                    REFERENCES buildings.bill_of_quantities_item_groups (id) MATCH SIMPLE
                    ON UPDATE CASCADE ON DELETE RESTRICT,
                CONSTRAINT measurement_sheet_groups_object_id_fkey FOREIGN KEY (object_id)
                    REFERENCES buildings.bill_of_quantities_objects (id) MATCH SIMPLE
                    ON UPDATE CASCADE ON DELETE RESTRICT,
                CONSTRAINT measurement_sheet_groups_building_interim_bill_id_bill_of_quantity_item_group_id_uniq UNIQUE(building_interim_bill_id, bill_of_quantity_item_group_id, object_id)
            );
            CREATE TRIGGER i_am_called_by_everyone
                AFTER INSERT OR UPDATE OR DELETE
                ON buildings.measurement_sheet_groups
                FOR EACH ROW
                EXECUTE PROCEDURE public.i_am_called_by_everyone();
            
SIMAMIGRATESQL
        )->execute();
    }

    public function safeDown()
    {
//        Yii::app()->db->createCommand(
//<<<'SIMAMIGRATESQL'
//
//SIMAMIGRATESQL
//        )->execute();
        echo "m0000000070_00002_interim_bill_add_columns does not support migration down.\n";
        return false;
    }
}