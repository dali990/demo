<?php

class m0000000077_00002_measurement_sheet_items_add_column extends CDbMigration
{
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        Yii::app()->db->createCommand(
<<<'SIMAMIGRATESQL'
ALTER TABLE buildings.measurement_sheet_items 
    ADD COLUMN description text;
SIMAMIGRATESQL
        )->execute();
    }

    public function safeDown()
    {
//        Yii::app()->db->createCommand(
//<<<'SIMAMIGRATESQL'
//
//SIMAMIGRATESQL
//        )->execute();
        echo "m0000000077_00002_measurement_sheet_items_add_column does not support migration down.\n";
        return false;
    }
}