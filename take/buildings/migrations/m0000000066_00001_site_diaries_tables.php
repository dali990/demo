<?php

class m0000000066_00001_site_diaries_tables extends CDbMigration
{
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        //prvo se brisu svi podaci iz tabela bills_of_quantities i constructions jer je jos uvek u razvoju i prepakovane su neke stvari
        Yii::app()->db->createCommand(
<<<'SIMAMIGRATESQL'
        TRUNCATE TABLE buildings.bills_of_quantities CASCADE;
        TRUNCATE TABLE buildings.constructions CASCADE;
   
        CREATE TABLE buildings.site_diaries
        (
            id integer NOT NULL,
            building_construction_id integer NOT NULL,
            date date NOT NULL DEFAULT now(),
            constructor_id integer NOT NULL,
            work_description text,
            remarks text,
            diary_creator_confirmed boolean NOT NULL DEFAULT FALSE,
            diary_creator_id integer,
            diary_creator_confirmed_timestamp timestamp without time zone,
            responsible_engineer_confirmed boolean NOT NULL DEFAULT FALSE,
            responsible_engineer_id integer,
            responsible_engineer_confirmed_timestamp timestamp without time zone,
            supervisor_confirmed boolean NOT NULL DEFAULT FALSE,
            supervisor_id integer,
            supervisor_confirmed_timestamp timestamp without time zone,
            CONSTRAINT site_diaries_pkey PRIMARY KEY (id),
            CONSTRAINT site_diaries_id_fkey FOREIGN KEY (id)
                REFERENCES files.files (id) MATCH SIMPLE
                ON UPDATE CASCADE ON DELETE RESTRICT,
            CONSTRAINT site_diaries_building_construction_id_fkey FOREIGN KEY (building_construction_id)
                REFERENCES buildings.constructions (id) MATCH SIMPLE
                ON UPDATE CASCADE ON DELETE RESTRICT,
            CONSTRAINT site_diaries_constructor_id_fkey FOREIGN KEY (constructor_id)
                REFERENCES public.companies (id) MATCH SIMPLE
                ON UPDATE CASCADE ON DELETE RESTRICT,
            CONSTRAINT site_diaries_diary_creator_id_fkey FOREIGN KEY (diary_creator_id)
                REFERENCES admin.users (id) MATCH SIMPLE
                ON UPDATE CASCADE ON DELETE RESTRICT,
            CONSTRAINT site_diaries_responsible_engineer_id_fkey FOREIGN KEY (responsible_engineer_id)
                REFERENCES admin.users (id) MATCH SIMPLE
                ON UPDATE CASCADE ON DELETE RESTRICT,
            CONSTRAINT site_diaries_supervisor_id_fkey FOREIGN KEY (supervisor_id)
                REFERENCES admin.users (id) MATCH SIMPLE
                ON UPDATE CASCADE ON DELETE RESTRICT,
            CONSTRAINT site_diaries_building_construction_id_constructor_id_date_uniq UNIQUE(building_construction_id, constructor_id, date)
        );
        CREATE TRIGGER i_am_called_by_everyone
            AFTER INSERT OR UPDATE OR DELETE
            ON buildings.site_diaries
            FOR EACH ROW
            EXECUTE PROCEDURE public.i_am_called_by_everyone();
            
        CREATE TABLE buildings.site_diary_work_hours
        (
            id serial NOT NULL,
            site_diary_id integer NOT NULL,
            start_datetime timestamp without time zone NOT NULL,
            end_datetime timestamp without time zone NOT NULL,
            note text,
            CONSTRAINT site_diary_work_hours_pkey PRIMARY KEY (id),
            CONSTRAINT site_diary_work_hours_site_diary_id_fkey FOREIGN KEY (site_diary_id)
                REFERENCES buildings.site_diaries (id) MATCH SIMPLE
                ON UPDATE CASCADE ON DELETE RESTRICT
        );
        CREATE TRIGGER i_am_called_by_everyone
            AFTER INSERT OR UPDATE OR DELETE
            ON buildings.site_diary_work_hours
            FOR EACH ROW
            EXECUTE PROCEDURE public.i_am_called_by_everyone();
            
        CREATE TABLE buildings.site_diary_work_hour_workers
        (
            id serial NOT NULL,
            site_diary_work_hour_id integer NOT NULL,
            worker_type text NOT NULL,
            worker_id integer,
            worker_count integer,
            CONSTRAINT site_diary_work_hour_workers_pkey PRIMARY KEY (id),
            CONSTRAINT site_diary_work_hour_workers_site_diary_work_hour_id_fkey FOREIGN KEY (site_diary_work_hour_id)
                REFERENCES buildings.site_diary_work_hours (id) MATCH SIMPLE
                ON UPDATE CASCADE ON DELETE RESTRICT,
            CONSTRAINT site_diary_work_hour_workers_worker_id_fkey FOREIGN KEY (worker_id)
                REFERENCES public.persons (id) MATCH SIMPLE
                ON UPDATE CASCADE ON DELETE RESTRICT,
            CONSTRAINT site_diary_work_hour_workers_worker_type_check CHECK (worker_type = ANY (ARRAY['WORKER'::text, 'CRAFTSMAN'::text, 'TEHNICAL_STAFF'::text, 'OTHERS'::text]))
        );
        CREATE TRIGGER i_am_called_by_everyone
            AFTER INSERT OR UPDATE OR DELETE
            ON buildings.site_diary_work_hour_workers
            FOR EACH ROW
            EXECUTE PROCEDURE public.i_am_called_by_everyone();
            
        CREATE TABLE buildings.site_diary_work_hour_mechanizations
        (
            id serial NOT NULL,
            site_diary_work_hour_id integer NOT NULL,
            mechanization_type_id integer NOT NULL,
            mechanization_id integer,
            mechanization_count integer,
            CONSTRAINT site_diary_work_hour_mechanizations_pkey PRIMARY KEY (id),
            CONSTRAINT site_diary_work_hour_mechanizations_site_diary_work_hour_id_fkey FOREIGN KEY (site_diary_work_hour_id)
                REFERENCES buildings.site_diary_work_hours (id) MATCH SIMPLE
                ON UPDATE CASCADE ON DELETE RESTRICT,
            CONSTRAINT site_diary_work_hour_mechanizations_mechanization_type_id_fkey FOREIGN KEY (mechanization_type_id)
                REFERENCES fixed_assets.fixed_asset_types (id) MATCH SIMPLE
                ON UPDATE CASCADE ON DELETE RESTRICT,
            CONSTRAINT site_diary_work_hour_mechanizations_mechanization_id_fkey FOREIGN KEY (mechanization_id)
                REFERENCES private.fixed_assets (id) MATCH SIMPLE
                ON UPDATE CASCADE ON DELETE RESTRICT
        );
        CREATE TRIGGER i_am_called_by_everyone
            AFTER INSERT OR UPDATE OR DELETE
            ON buildings.site_diary_work_hour_mechanizations
            FOR EACH ROW
            EXECUTE PROCEDURE public.i_am_called_by_everyone();
            
        ALTER TABLE buildings.measurement_sheet_items DROP COLUMN object_id;
        ALTER TABLE buildings.measurement_sheets ADD COLUMN object_id integer NOT NULL;
        ALTER TABLE buildings.measurement_sheets DROP CONSTRAINT measurement_sheets_building_interim_bill_id_bill_of_quantity_it;
        ALTER TABLE buildings.measurement_sheets
            ADD CONSTRAINT interim_bill_id_bill_of_quantity_item_id_object_id UNIQUE(building_interim_bill_id, bill_of_quantity_item_id, object_id);
            
        ALTER TABLE buildings.bill_of_quantities_items_to_objects ADD COLUMN quantity_done numeric(20,2) NOT NULL DEFAULT 0;
        ALTER TABLE buildings.bill_of_quantities_item_groups_to_objects ADD COLUMN quantity_done numeric(20,2) NOT NULL DEFAULT 0;
SIMAMIGRATESQL
        )->execute();
    }

    public function safeDown()
    {
//        Yii::app()->db->createCommand(
//<<<'SIMAMIGRATESQL'
//
//SIMAMIGRATESQL
//        )->execute();
        echo "m0000000066_00001_site_diaries_tables does not support migration down.\n";
        return false;
    }
}