<?php

class m0000000011_00059_reAttach_triggers_fix extends CDbMigration
{
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        Yii::app()->db->createCommand(
"
ALTER TRIGGER model_group_tags ON buildings.projects
  RENAME TO \"01_model_group_tags\";

ALTER TRIGGER building_projects_group_tags_update ON buildings.projects
  RENAME TO \"02_group_tags_update\";

ALTER TRIGGER building_projects_group_tags_delete ON buildings.projects
  RENAME TO \"03_group_tags_delete\";




DROP TRIGGER dependent_file_tags_before_insert_update_base_themes ON buildings.projects;

CREATE TRIGGER dependent_file_tags_after_insert_update_base_themes
  AFTER INSERT OR UPDATE OF geo_object_id
  ON buildings.projects
  FOR EACH ROW
  EXECUTE PROCEDURE files.dependent_file_tags_after_insert_update('base.themes', 'buildings.projects');
  
DROP TRIGGER dependent_file_tags_before_insert_update_geo_objects ON buildings.projects;

CREATE TRIGGER dependent_file_tags_after_insert_update_geo_objects
  AFTER INSERT OR UPDATE OF geo_object_id
  ON buildings.projects
  FOR EACH ROW
  EXECUTE PROCEDURE files.dependent_file_tags_after_insert_update('geo.objects', 'buildings.projects');
"
        )->execute();
    }

    public function safeDown()
    {
//        Yii::app()->db->createCommand(
//"
//
//"
//        )->execute();
        echo "m0000000011_00059_reAttach_triggers_fix does not support migration down.\n";
        return false;
    }
}