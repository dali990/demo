<?php

class m0000000074_00001_bill_of_quantities_items_remove_columns extends CDbMigration
{
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        Yii::app()->db->createCommand(
<<<'SIMAMIGRATESQL'
            ALTER TABLE buildings.bill_of_quantities_items DROP COLUMN is_material;
            ALTER TABLE buildings.bill_of_quantities_items DROP COLUMN requires_approval;
SIMAMIGRATESQL
        )->execute();
    }

    public function safeDown()
    {
//        Yii::app()->db->createCommand(
//<<<'SIMAMIGRATESQL'
//
//SIMAMIGRATESQL
//        )->execute();
        echo "m0000000074_00001_bill_of_quantities_items_remove_columns does not support migration down.\n";
        return false;
    }
}