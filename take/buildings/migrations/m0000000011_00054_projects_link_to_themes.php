<?php

class m0000000011_00054_projects_link_to_themes extends CDbMigration
{
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        Yii::app()->db->createCommand(
"
ALTER TABLE buildings.projects RENAME job_id  TO theme_id;
ALTER TABLE buildings.projects
  DROP CONSTRAINT building_projects_job_id_fkey;
ALTER TABLE buildings.projects
  ADD FOREIGN KEY (theme_id) REFERENCES base.themes (id) ON UPDATE CASCADE ON DELETE RESTRICT;

CREATE TRIGGER dependent_file_tags_before_insert_update_base_themes 
    BEFORE INSERT OR UPDATE
    OF geo_object_id
    ON buildings.projects FOR EACH ROW
    EXECUTE PROCEDURE files.dependent_file_tags_before_insert_update('base.themes','buildings.projects');
   
CREATE TRIGGER dependent_file_tags_before_insert_update_geo_objects 
    BEFORE INSERT OR UPDATE
    OF geo_object_id
    ON buildings.projects FOR EACH ROW
    EXECUTE PROCEDURE files.dependent_file_tags_before_insert_update('geo.objects','buildings.projects');
   
CREATE TRIGGER dependent_file_tags_before_delete_update_geo_objects 
    BEFORE DELETE OR UPDATE
    OF theme_id
    ON buildings.projects FOR EACH ROW
    EXECUTE PROCEDURE files.dependent_file_tags_before_delete_update('geo.objects','buildings.projects');

CREATE TRIGGER dependent_file_tags_before_delete_update_base_themes 
    BEFORE DELETE OR UPDATE
    OF theme_id
    ON buildings.projects FOR EACH ROW
    EXECUTE PROCEDURE files.dependent_file_tags_before_delete_update('base.themes','buildings.projects');

update buildings.projects set geo_object_id = building_structure_id where geo_object_id is null and building_structure_id is not null;
update buildings.projects set geo_object_id = building_site_id where geo_object_id is null and building_site_id is not null;

"
        )->execute();
    }

    public function safeDown()
    {
//        Yii::app()->db->createCommand(
//"
//
//"
//        )->execute();
        echo "m0000000011_00054_projects_link_to_themes does not support migration down.\n";
        return false;
    }
}