<?php

class m0000000010_00025_izmena_nekih_kolona extends CDbMigration
{
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        Yii::app()->db->createCommand(
"
ALTER TABLE buildings.construction_material_approvals
  DROP COLUMN used_in_id;
ALTER TABLE buildings.construction_material_approvals
  ADD COLUMN part_of_id integer;
ALTER TABLE buildings.construction_material_approvals
  ADD COLUMN used_in text;

ALTER TABLE buildings.construction_material_approvals
  ADD FOREIGN KEY (part_of_id) REFERENCES geo.building_structure_parts (id) ON UPDATE CASCADE ON DELETE RESTRICT;

"
        )->execute();
    }

    public function safeDown()
    {
//        Yii::app()->db->createCommand(
//"
//
//"
//        )->execute();
        echo "m0000000010_00025_izmena_nekih_kolona does not support migration down.\n";
        return false;
    }
}