<?php

class m0000000068_00005_add_bill_of_quantity_columns extends CDbMigration
{
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        Yii::app()->db->createCommand(
<<<'SIMAMIGRATESQL'
            ALTER TABLE buildings.bills_of_quantities ADD COLUMN is_private boolean default false;
            ALTER TABLE buildings.bills_of_quantities ADD COLUMN comment text;
            
            ALTER TABLE buildings.bill_of_quantities_item_groups ADD COLUMN "order" integer;
            ALTER TABLE buildings.bill_of_quantities_item_groups
                ADD CONSTRAINT bill_of_quantities_item_groups_parent_id_order_uniq UNIQUE(parent_id, "order");

            CREATE OR REPLACE FUNCTION buildings.bill_of_quantities_item_groups_order_number()
                RETURNS trigger AS
              $BODY$
                      BEGIN
                          IF NEW.order IS NULL
                          THEN
                              select MAX(big.order)+1 from buildings.bill_of_quantities_item_groups big
                                  where big.parent_id = NEW.parent_id
                                  into NEW.order;
                          END IF;

                          IF (NEW.order is null) 
                          THEN
                              NEW.order := 1;
                          END IF;

                          RETURN NEW;
                      END;
                      $BODY$
                LANGUAGE plpgsql VOLATILE
                COST 100;

            CREATE TRIGGER bill_of_quantities_item_groups_order_number
                BEFORE INSERT OR UPDATE
                ON buildings.bill_of_quantities_item_groups
                FOR EACH ROW
                EXECUTE PROCEDURE buildings.bill_of_quantities_item_groups_order_number();
            
            UPDATE buildings.bill_of_quantities_item_groups SET id=id;
            
            ALTER TABLE buildings.bill_of_quantities_item_groups ALTER COLUMN "order" SET NOT NULL;
SIMAMIGRATESQL
        )->execute();
    }

    public function safeDown()
    {
//        Yii::app()->db->createCommand(
//<<<'SIMAMIGRATESQL'
//
//SIMAMIGRATESQL
//        )->execute();
        echo "m0000000068_00005_add_bill_of_quantity_columns does not support migration down.\n";
        return false;
    }
}