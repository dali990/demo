<?php

class m0000000060_00008_create_buildings_quantities_tables extends CDbMigration
{
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        Yii::app()->db->createCommand(
<<<'SIMAMIGRATESQL'
            CREATE TABLE buildings.bills_of_quantities
            (
                id serial NOT NULL,
                name text NOT NULL,
                theme_id integer NOT NULL,
                has_object_value_per_unit boolean NOT NULL DEFAULT false,
                CONSTRAINT bills_of_quantities_pkey PRIMARY KEY (id),
                CONSTRAINT bills_of_quantities_theme_id_fkey FOREIGN KEY (theme_id)
                    REFERENCES base.themes (id) MATCH SIMPLE
                    ON UPDATE CASCADE ON DELETE RESTRICT,
                CONSTRAINT bills_of_quantities_theme_id_name_uniq UNIQUE(theme_id, name)
            );
            CREATE TRIGGER i_am_called_by_everyone
                AFTER INSERT OR UPDATE OR DELETE
                ON buildings.bills_of_quantities
                FOR EACH ROW
                EXECUTE PROCEDURE public.i_am_called_by_everyone();
            
            CREATE TABLE buildings.bill_of_quantities_item_groups
            (
                id serial NOT NULL,
                title text NOT NULL,
                bill_of_quantity_id integer NOT NULL,
                parent_id integer,
                description text,
                quantity numeric(20,2) NOT NULL DEFAULT 0,
                value_per_unit numeric(20,10) NOT NULL DEFAULT 0,
                value numeric(22,2) NOT NULL DEFAULT 0,
                CONSTRAINT bill_of_quantities_item_groups_pkey PRIMARY KEY (id),
                CONSTRAINT bill_of_quantities_item_groups_bill_of_quantity_id_fkey FOREIGN KEY (bill_of_quantity_id)
                    REFERENCES buildings.bills_of_quantities (id) MATCH SIMPLE
                    ON UPDATE CASCADE ON DELETE RESTRICT,
                CONSTRAINT bill_of_quantities_item_groups_parent_id_fkey FOREIGN KEY (parent_id)
                    REFERENCES buildings.bill_of_quantities_item_groups (id) MATCH SIMPLE
                    ON UPDATE CASCADE ON DELETE RESTRICT,
                CONSTRAINT bill_of_quantities_item_groups_bill_of_quantity_id_parent_id_title_uniq UNIQUE(bill_of_quantity_id, parent_id, title)
            );
            CREATE TRIGGER i_am_called_by_everyone
                AFTER INSERT OR UPDATE OR DELETE
                ON buildings.bill_of_quantities_item_groups
                FOR EACH ROW
                EXECUTE PROCEDURE public.i_am_called_by_everyone();
            
            CREATE TABLE buildings.bill_of_quantities_items
            (
                id serial NOT NULL,
                name text NOT NULL,
                group_id integer NOT NULL,
                "order" integer NOT NULL,
                measurement_unit_id integer NOT NULL,
                quantity numeric(20,2) NOT NULL DEFAULT 0,
                value_per_unit numeric(20,10) NOT NULL DEFAULT 0,
                value numeric(22,2) NOT NULL DEFAULT 0,
                is_material boolean NOT NULL DEFAULT false,
                requires_approval boolean NOT NULL DEFAULT false,
                CONSTRAINT bill_of_quantities_items_pkey PRIMARY KEY (id),
                CONSTRAINT bill_of_quantities_items_group_id_fkey FOREIGN KEY (group_id)
                    REFERENCES buildings.bill_of_quantities_item_groups (id) MATCH SIMPLE
                    ON UPDATE CASCADE ON DELETE RESTRICT,
                CONSTRAINT bill_of_quantities_items_measurement_unit_id_fkey FOREIGN KEY (measurement_unit_id)
                    REFERENCES base.measurement_units (id) MATCH SIMPLE
                    ON UPDATE CASCADE ON DELETE RESTRICT,
                CONSTRAINT bill_of_quantities_items_group_id_order_uniq UNIQUE(group_id, "order")
            );
            CREATE TRIGGER i_am_called_by_everyone
                AFTER INSERT OR UPDATE OR DELETE
                ON buildings.bill_of_quantities_items
                FOR EACH ROW
                EXECUTE PROCEDURE public.i_am_called_by_everyone();
            CREATE OR REPLACE FUNCTION buildings.bill_of_quantities_items_order_number()
                RETURNS trigger AS
                $BODY$
                BEGIN
                    IF NEW.order IS NULL
                    THEN
                        select MAX(bi.order)+1 from buildings.bill_of_quantities_items bi
                            where bi.group_id = NEW.group_id
                            into NEW.order;
                    END IF;

                    IF (NEW.order is null) 
                    THEN
                        NEW.order := 1;
                    END IF;

                    RETURN NEW;
                END;
                $BODY$
                    LANGUAGE plpgsql;
            CREATE TRIGGER bill_of_quantities_items_order_number
                BEFORE INSERT OR UPDATE
                ON buildings.bill_of_quantities_items
                FOR EACH ROW
                EXECUTE PROCEDURE buildings.bill_of_quantities_items_order_number();
                
            CREATE TABLE buildings.bill_of_quantities_objects
            (
                id serial NOT NULL,
                name text NOT NULL,
                bill_of_quantity_id integer NOT NULL,
                parent_id integer,
                description text,
                CONSTRAINT bill_of_quantities_objects_pkey PRIMARY KEY (id),
                CONSTRAINT bill_of_quantities_objects_bill_of_quantity_id_fkey FOREIGN KEY (bill_of_quantity_id)
                    REFERENCES buildings.bills_of_quantities (id) MATCH SIMPLE
                    ON UPDATE CASCADE ON DELETE RESTRICT,
                CONSTRAINT bill_of_quantities_objects_parent_id_fkey FOREIGN KEY (parent_id)
                    REFERENCES buildings.bill_of_quantities_objects (id) MATCH SIMPLE
                    ON UPDATE CASCADE ON DELETE RESTRICT,
                CONSTRAINT bill_of_quantities_objects_bill_of_quantity_id_parent_id_name_uniq UNIQUE(bill_of_quantity_id, parent_id, name)
            );
            CREATE TRIGGER i_am_called_by_everyone
                AFTER INSERT OR UPDATE OR DELETE
                ON buildings.bill_of_quantities_objects
                FOR EACH ROW
                EXECUTE PROCEDURE public.i_am_called_by_everyone();
            
            CREATE TABLE buildings.bill_of_quantities_items_to_objects
            (
                id serial NOT NULL,
                item_id integer NOT NULL,
                object_id integer NOT NULL,
                quantity numeric(20,2) NOT NULL DEFAULT 0,
                value_per_unit numeric(20,10) NOT NULL DEFAULT 0,
                value numeric(22,2) NOT NULL DEFAULT 0,
                CONSTRAINT bill_of_quantities_items_to_objects_pkey PRIMARY KEY (id),
                CONSTRAINT bill_of_quantities_items_to_objects_item_id_fkey FOREIGN KEY (item_id)
                    REFERENCES buildings.bill_of_quantities_items (id) MATCH SIMPLE
                    ON UPDATE CASCADE ON DELETE RESTRICT,
                CONSTRAINT bill_of_quantities_items_to_objects_object_id_fkey FOREIGN KEY (object_id)
                    REFERENCES buildings.bill_of_quantities_objects (id) MATCH SIMPLE
                    ON UPDATE CASCADE ON DELETE RESTRICT,
                CONSTRAINT bill_of_quantities_items_to_objects_item_id_object_id_uniq UNIQUE(item_id, object_id)
            );
            CREATE TRIGGER i_am_called_by_everyone
                AFTER INSERT OR UPDATE OR DELETE
                ON buildings.bill_of_quantities_items_to_objects
                FOR EACH ROW
                EXECUTE PROCEDURE public.i_am_called_by_everyone();
            
            CREATE TABLE buildings.bill_of_quantities_item_groups_to_objects
            (
                id serial NOT NULL,
                item_group_id integer NOT NULL,
                object_id integer NOT NULL,
                quantity numeric(20,2) NOT NULL DEFAULT 0,
                value_per_unit numeric(20,10) NOT NULL DEFAULT 0,
                value numeric(22,2) NOT NULL DEFAULT 0,
                CONSTRAINT bill_of_quantities_item_groups_to_objects_pkey PRIMARY KEY (id),
                CONSTRAINT bill_of_quantities_item_groups_to_objects_item_group_id_fkey FOREIGN KEY (item_group_id)
                    REFERENCES buildings.bill_of_quantities_item_groups (id) MATCH SIMPLE
                    ON UPDATE CASCADE ON DELETE RESTRICT,
                CONSTRAINT bill_of_quantities_item_groups_to_objects_object_id_fkey FOREIGN KEY (object_id)
                    REFERENCES buildings.bill_of_quantities_objects (id) MATCH SIMPLE
                    ON UPDATE CASCADE ON DELETE RESTRICT,
                CONSTRAINT bill_of_quantities_item_groups_to_objects_item_group_id_object_id_uniq UNIQUE(item_group_id, object_id)
            );
            CREATE TRIGGER i_am_called_by_everyone
                AFTER INSERT OR UPDATE OR DELETE
                ON buildings.bill_of_quantities_item_groups_to_objects
                FOR EACH ROW
                EXECUTE PROCEDURE public.i_am_called_by_everyone();

            CREATE TABLE buildings.bill_of_quantities_to_users
            (
                id serial NOT NULL,
                bill_of_quantity_id integer NOT NULL,
                user_id integer NOT NULL,
                CONSTRAINT bill_of_quantities_to_users_pkey PRIMARY KEY (id),
                CONSTRAINT bill_of_quantities_to_users_bill_of_quantity_id_fkey FOREIGN KEY (bill_of_quantity_id)
                    REFERENCES buildings.bills_of_quantities (id) MATCH SIMPLE
                    ON UPDATE CASCADE ON DELETE RESTRICT,
                CONSTRAINT bill_of_quantities_to_users_user_id_fkey FOREIGN KEY (user_id)
                    REFERENCES admin.users (id) MATCH SIMPLE
                    ON UPDATE CASCADE ON DELETE RESTRICT,
                CONSTRAINT bill_of_quantities_to_users_bill_of_quantity_id_user_id_key UNIQUE (bill_of_quantity_id, user_id)
            );
            CREATE TRIGGER i_am_called_by_everyone
                AFTER INSERT OR UPDATE OR DELETE
                ON buildings.bill_of_quantities_to_users
                FOR EACH ROW
                EXECUTE PROCEDURE public.i_am_called_by_everyone();
            
SIMAMIGRATESQL
        )->execute();
    }

    public function safeDown()
    {
//        Yii::app()->db->createCommand(
//<<<'SIMAMIGRATESQL'
//
//SIMAMIGRATESQL
//        )->execute();
        echo "m0000000060_00008_create_buildings_quantities_tables does not support migration down.\n";
        return false;
    }
}