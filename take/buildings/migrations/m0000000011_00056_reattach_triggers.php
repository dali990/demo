<?php

class m0000000011_00056_reattach_triggers extends CDbMigration
{
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        Yii::app()->db->createCommand(
"
 
DROP TRIGGER dependent_file_tags_before_delete_update_geo_objects ON buildings.projects;

CREATE TRIGGER dependent_file_tags_before_delete_update_geo_objects
  BEFORE UPDATE OF geo_object_id OR DELETE
  ON buildings.projects
  FOR EACH ROW
  EXECUTE PROCEDURE files.dependent_file_tags_before_delete_update('geo.objects', 'buildings.projects');
  
DROP TRIGGER dependent_file_tags_before_insert_update_base_themes ON buildings.projects;

CREATE TRIGGER dependent_file_tags_before_insert_update_base_themes
  BEFORE INSERT OR UPDATE OF theme_id
  ON buildings.projects
  FOR EACH ROW
  EXECUTE PROCEDURE files.dependent_file_tags_before_insert_update('base.themes', 'buildings.projects');
  
"
        )->execute();
    }

    public function safeDown()
    {
//        Yii::app()->db->createCommand(
//"
//
//"
//        )->execute();
        echo "m0000000011_00056_reattach_triggers does not support migration down.\n";
        return false;
    }
}