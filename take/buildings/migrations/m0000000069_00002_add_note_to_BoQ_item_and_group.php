<?php

class m0000000069_00002_add_note_to_BoQ_item_and_group extends CDbMigration
{
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        Yii::app()->db->createCommand(
<<<'SIMAMIGRATESQL'
            ALTER TABLE buildings.bill_of_quantities_items ADD COLUMN note text;
            ALTER TABLE buildings.bill_of_quantities_item_groups RENAME description TO note;
            
            ALTER TABLE buildings.constructions DROP CONSTRAINT constructions_investor_id_fkey;
            ALTER TABLE buildings.constructions
                ADD CONSTRAINT constructions_investor_id_fkey FOREIGN KEY (investor_id)
                    REFERENCES public.partners (id) MATCH SIMPLE
                    ON UPDATE CASCADE ON DELETE RESTRICT;
SIMAMIGRATESQL
        )->execute();
    }

    public function safeDown()
    {
//        Yii::app()->db->createCommand(
//<<<'SIMAMIGRATESQL'
//
//SIMAMIGRATESQL
//        )->execute();
        echo "m0000000069_00002_add_note_to_BoQ_item_and_group does not support migration down.\n";
        return false;
    }
}