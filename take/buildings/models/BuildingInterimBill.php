<?php

class BuildingInterimBill extends SIMAActiveRecord
{
    public static $PREPARATION = 'PREPARATION';
    public static $WRITTEN_MEASUREMENT_SHEETS = 'WRITTEN_MEASUREMENT_SHEETS';
    public static $CONFIRMED_MEASUREMENT_SHEETS = 'CONFIRMED_MEASUREMENT_SHEETS';
    public static $DELIVERED_MEASUREMENT_SHEETS = 'DELIVERED_MEASUREMENT_SHEETS';
    public static $VALIDATED_MEASUREMENT_SHEETS = 'VALIDATED_MEASUREMENT_SHEETS';
    public static $DELIVERED = 'DELIVERED';
    public static $VALIDATED = 'VALIDATED';
    public static $PAID = 'PAID';
    
    public static $statuses = [];

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function moduleName()
    {
        return 'buildings';
    }

    public function tableName()
    {
        return 'buildings.interim_bills';
    }

    public function __get($column) 
    {
        switch ($column) {
            case 'DisplayName': return !empty($this->file) ? $this->file->DisplayName : '';
            case 'SearchName': return $this->DisplayName;

            default: return parent::__get($column);
        }
    }
    
    public function rules()
    {
        return [
            ['building_construction_id, issue_date', 'required'],
            ['status, is_final', 'safe'],
            [
                'building_construction_id', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => ['insert','update']
            ],
            ['issue_date', 'unique_with', 'with' => ['building_construction_id']],
            ['issue_date', 'checkIssueDate'],
            ['is_final', 'checkIsFinal']
        ];
    }

    public function relations($child_relations = [])
    {
        return [
            'file' => [self::HAS_ONE, File::class, 'id'],
            'building_construction' => [self::BELONGS_TO, BuildingConstruction::class, 'building_construction_id'],
            'building_measurement_sheets' => [self::HAS_MANY, BuildingMeasurementSheet::class, 'building_interim_bill_id'],
            'building_measurement_sheets_count' => [self::STAT, BuildingMeasurementSheet::class, 'building_interim_bill_id', 'select' => 'count(*)'],
            'building_measurement_sheet_groups' => [self::HAS_MANY, BuildingMeasurementSheetGroup::class, 'building_interim_bill_id'],
        ];
    }
    
    public function checkIssueDate()
    {
        if (!$this->hasErrors())
        {
            if ($this->isNewRecord)
            {
                $last_interim_bill = $this->getLastBuildingInterim();
                if (
                        !empty($last_interim_bill) && 
                        (new SIMADateTime($this->issue_date))->diffDays(new SIMADateTime($last_interim_bill->issue_date)) <= 0
                    )
                {
                    $this->addError('issue_date', Yii::t('BuildingsModule.BuildingInterimBill', 'IssueDateMustBeGreaterThan', [
                        '{date}' => SIMAHtml::DbToUserDate($last_interim_bill->issue_date)
                    ]));
                }
            }
            else if ($this->columnChanged('issue_date'))
            {
                $prev_bill_interim = $this->prevBuildingInterimBillByDate($this->__old['issue_date']);
                $next_bill_interim = $this->nextBuildingInterimBillByDate($this->__old['issue_date']);
                if (
                        empty($prev_bill_interim) && 
                        !empty($next_bill_interim) && 
                        (new SIMADateTime($this->issue_date))->diffDays(new SIMADateTime($next_bill_interim->issue_date)) >= 0
                    )
                {
                    $this->addError('issue_date', Yii::t('BuildingsModule.BuildingInterimBill', 'IssueDateMustBeLessThan', [
                        '{date}' => SIMAHtml::DbToUserDate($next_bill_interim->issue_date)
                    ]));
                }
                else if (
                    empty($next_bill_interim) && 
                    !empty($prev_bill_interim) && 
                    (new SIMADateTime($this->issue_date))->diffDays(new SIMADateTime($prev_bill_interim->issue_date)) <= 0
                )
                {
                    $this->addError('issue_date', Yii::t('BuildingsModule.BuildingInterimBill', 'IssueDateMustBeGreaterThan', [
                        '{date}' => SIMAHtml::DbToUserDate($prev_bill_interim->issue_date)
                    ]));
                }
                else if (
                    !empty($prev_bill_interim) && !empty($next_bill_interim) && 
                    (
                        (new SIMADateTime($this->issue_date))->diffDays(new SIMADateTime($prev_bill_interim->issue_date)) <= 0 || 
                        (new SIMADateTime($this->issue_date))->diffDays(new SIMADateTime($next_bill_interim->issue_date)) >= 0
                    )
                )
                {
                    $this->addError('issue_date', Yii::t('BuildingsModule.BuildingInterimBill', 'IssueDateMustBeInInterval', [
                        '{prev_date}' => SIMAHtml::DbToUserDate($prev_bill_interim->issue_date),
                        '{next_date}' => SIMAHtml::DbToUserDate($next_bill_interim->issue_date)
                    ]));
                }
            }
        }
    }
    
    public function checkIsFinal()
    {
        if (
                !$this->hasErrors() && 
                (
                    $this->isNewRecord || $this->columnChanged('is_final')
                )
            )
        {
            if (
                    (
                        $this->isNewRecord || $this->is_final
                    ) && 
                    $this->building_construction->hasFinalInterimBill()
                )
            {
                $this->addError('is_final', Yii::t('BuildingsModule.BuildingInterimBill', 'AlreadyHasFinalInterimBill'));
            }
            if ($this->is_final && !$this->isLastInBuildingConstruction())
            {
                $this->addError('is_final', Yii::t('BuildingsModule.BuildingInterimBill', 'CanNotSetToFinalBecauseIsNotLastInterimBill'));
            }
        }
    }
    
    public function scopes()
    {
        $alias = $this->getTableAlias();

        return [
            'byDate' => [
                'order' => "$alias.issue_date DESC"
            ],
            'byDateAsc' => [
                'order' => "$alias.issue_date ASC"
            ]
        ];
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters': return [
                'file' => 'relation',
                'building_construction' => 'relation',
                'order_number' => 'integer',
                'issue_date' => 'date_range',
                'status' => 'dropdown',
                'is_final' => 'boolean',
                'building_measurement_sheets' => 'relation',
                'building_measurement_sheet_groups' => 'relation'
            ];
            case 'textSearch': return [
                'file' => 'relation'
            ];
            case 'mutual_forms': return ['base_relation' => 'file'];
            default: return parent::modelSettings($column);
        }
    }
    
    protected function modelOptions(\User $user = null):array
    {
        $options = ['open'];

        if (self::hasChangeAccess($this->building_construction, $user))
        {
            $options[] = 'form';
            $options[] = 'delete';
        }

        return $options;
    }
    
    /**
     * Korisnik ima pravo da menja situaciju(add/edit/delete) ako ima pravo da menja temu(koordinator ili privilegija modelThemeAdmin) ili
     * ako je sef gradilista na izvodjenju
     * @param BuildingConstruction $building_construction
     * @param User $user
     * @return boolean
     */
    public static function hasChangeAccess(BuildingConstruction $building_construction, User $user = null)
    {
        if (empty($user))
        {
            return false;
        }
        
        return  !empty(Theme::model()->hasChangeAccess($user->id)->findByPk($building_construction->id)) ||
                $building_construction->construction_boss_id === $user->id;
    }
    
    public function beforeSave() 
    {
        if ($this->isNewRecord)
        {
            $this->status = BuildingInterimBill::$PREPARATION;
        }

        return parent::beforeSave();
    }
    
    public function afterSave() 
    {
        parent::afterSave();
        
        if ($this->isNewRecord)
        {
            $prev_interim_bill = BuildingInterimBill::model()->find([
                'model_filter' => [
                    'display_scopes' => 'byDate',
                    'issue_date' => ['<', $this->issue_date],
                    'building_construction' => [
                        'ids' => $this->building_construction_id
                    ]
                ]
            ]);

            if (!empty($prev_interim_bill)) 
            {
                foreach ($prev_interim_bill->building_measurement_sheets as $building_measurement_sheet) 
                {
                    $building_measurement_sheet->calcValueDone();
                }
            }
        }
        else if (
                $this->columnChanged('issue_date')
            )
        {
            $building_interim_bills = BuildingInterimBill::model()->findAll([
                'model_filter' => [
                    'display_scopes' => 'byDateAsc',
                    'building_construction' => [
                        'ids' => $this->building_construction_id
                    ]
                ]
            ]);
            foreach ($building_interim_bills as $building_interim_bill) 
            {
                foreach ($building_interim_bill->building_measurement_sheets as $building_measurement_sheet) 
                {
                    $building_measurement_sheet->calcValueDone();
                }
            }
        }
    }
    
    public function beforeDelete()
    {
        foreach ($this->building_measurement_sheet_groups as $building_measurement_sheet_group) 
        {
            $building_measurement_sheet_group->delete();
        }
        
        foreach ($this->building_measurement_sheets as $building_measurement_sheet) 
        {
            $building_measurement_sheet->is_delete_from_interim_bill = true;
            $building_measurement_sheet->delete();
        }
        
        return parent::beforeDelete();
    }
    
    public function afterValidateDependencyError(array $foreign_dependencies)
    {        
        $allowed_tables = [
            BuildingMeasurementSheet::tableName().':building_interim_bill_id',
            BuildingMeasurementSheetGroup::tableName().':building_interim_bill_id'
        ];
        
        $parsed_foreign_dependencies = array_diff($foreign_dependencies, $allowed_tables);
        
        parent::afterValidateDependencyError($parsed_foreign_dependencies);
    }
    
    public function afterDelete() 
    {
        parent::afterDelete();
        
        $building_interim_bills = BuildingInterimBill::model()->findAll([
            'model_filter' => [
                'display_scopes' => 'byDateAsc',
                'building_construction' => [
                    'ids' => $this->building_construction_id
                ]
            ]
        ]);
        foreach ($building_interim_bills as $building_interim_bill) 
        {
            foreach ($building_interim_bill->building_measurement_sheets as $building_measurement_sheet) 
            {
                $building_measurement_sheet->calcValueDone();
            }
        }
    }
    
    public function getLastBuildingInterim()
    {
        return BuildingInterimBill::model()->find([
            'model_filter' => [
                'building_construction' => [
                    'ids' => $this->building_construction_id
                ],
                'display_scopes' => [
                    'byDate'
                ]
            ]
        ]);
    }
    
    public function isLastInBuildingConstruction()
    {
        //ako nije popunjen id znaci da je nova situacija i ona mora biti poslednja
        if (empty($this->id))
        {
            return true;
        }

        $last_interim_bill = $this->getLastBuildingInterim();
        
        return !empty($last_interim_bill) && $last_interim_bill->id === $this->id;
    }
    
    public function prevBuildingInterimBillByDate($issue_date)
    {
        return BuildingInterimBill::model()->find([
            'model_filter' => [
                'building_construction' => [
                    'ids' => $this->building_construction_id
                ],
                'issue_date' => ['<', SIMAHtml::UserToDbDate($issue_date)],
                'display_scopes' => [
                    'byDate'
                ]
            ]
        ]);
    }
    public function prevBuildingInterimBillsByDate($issue_date)
    {
        return BuildingInterimBill::model()->findAll([
            'model_filter' => [
                'building_construction' => [
                    'ids' => $this->building_construction_id
                ],
                'issue_date' => ['<', SIMAHtml::UserToDbDate($issue_date)],
                'display_scopes' => [
                    'byDate'
                ]
            ]
        ]);
    }
    
    public function nextBuildingInterimBillByDate($issue_date)
    {
        return BuildingInterimBill::model()->find([
            'model_filter' => [
                'building_construction' => [
                    'ids' => $this->building_construction_id
                ],
                'issue_date' => ['>', SIMAHtml::UserToDbDate($issue_date)],
                'display_scopes' => [
                    'byDateAsc'
                ]
            ]
        ]);
    }
    
    public function getRootSheetGroup()
    {
        $root_sheet_group = null;
                
        $root_object = $this->building_construction->bill_of_quantity->root_object;
        $root_group = $this->building_construction->bill_of_quantity->root_group;
        if (!empty($root_object) && !empty($root_group))
        {
            $root_sheet_group = BuildingMeasurementSheetGroup::model()->findByAttributes([
                'building_interim_bill_id' => $this->id,
                'bill_of_quantity_item_group_id' => $root_group->id,
                'object_id' => $root_object->id
            ]);
        }
        
        return $root_sheet_group;
    }
    
    /**
     * Vraca uradjeni iznos na prethodnim situacijama
     * @return SIMABigNumber
     */
    public function getPreValue()
    {
        $pre_value = SIMABigNumber::fromFloat(0.00, 2);

        $root_sheet_group = $this->getRootSheetGroup();
        if(!empty($root_sheet_group))
        {
            $pre_value = $root_sheet_group->pre_value_done;
        }
        
        return $pre_value;
    }
    
    /**
     * Vraca uradjeni iznos na ovoj situaciji
     * @return SIMABigNumber
     */
    public function getValue()
    {
        $value = SIMABigNumber::fromFloat(0.00, 2);

        $root_sheet_group = $this->getRootSheetGroup();
        if(!empty($root_sheet_group))
        {
            $value = $root_sheet_group->value_done;
        }
        
        return $value;
    }

    /**
     * Vraca uradjeni iznos zakljucno sa ovom situacijom
     * @return SIMABigNumber
     */
    public function getTotalValue()
    {
        $total_value = SIMABigNumber::fromFloat(0.00, 2);

        $root_sheet_group = $this->getRootSheetGroup();
        if(!empty($root_sheet_group))
        {
            $total_value = $root_sheet_group->pre_value_done->plus($root_sheet_group->value_done);
        }
        
        return $total_value;
    }
    
    /**
     * Vraca iznos uplacenog avansa
     * @return number
     */
    public function getBillAdvanceValue()
    {
        $bill_advance_value = 0;
        if (!empty($this->file->bill))
        {
            $bill_advance_value = $this->file->bill->advance_released_amount_sum;
        }
        
        return $bill_advance_value;
    }
    
    /**
     * Vraca iznos preostalog avansa
     * @return number
     */
    public function getRemainingBillAdvanceValue()
    {
        $remaining_bill_advance_value = 0;

        if (!empty($this->file->bill))
        {
            $prev_advance_value = 0;
            $prev_building_interim_bills = BuildingInterimBill::model()->findAll([
                'model_filter' => [
                    'building_construction' => [
                        'ids' => $this->building_construction_id
                    ],
                    'issue_date' => ['<=', SIMAHtml::UserToDbDate($this->issue_date)]
                ]
            ]);

            foreach ($prev_building_interim_bills as $prev_building_interim_bill)
            {
                $prev_advance_value += $prev_building_interim_bill->getBillAdvanceValue();
            }

            $advance_bills = [];
            $advance_bill_ids = [];
            foreach ($this->file->bill->advance_releases as $advance_release)
            {
                if (!in_array($advance_release->advance_bill_id, $advance_bill_ids))
                {
                    array_push($advance_bill_ids, $advance_release->advance_bill_id);
                    array_push($advance_bills, $advance_release->advance_bill);
                }
            }
            
            $total_advance_value = 0;
            foreach ($advance_bills as $advance_bill)
            {
                $total_advance_value += $advance_bill->amount;
            }
            
            $remaining_bill_advance_value = $total_advance_value - $prev_advance_value;
        }
        
        return $remaining_bill_advance_value;
    }
    
    /**
     * Vraca iznos za naplatu na ovoj situaciji
     * @return SIMABigNumber
     */
    public function getValueForPay()
    {
        $bill_advance_value = $this->getBillAdvanceValue();
        
        return $this->getValue()->minus(SIMABigNumber::fromString($bill_advance_value));
    }
    
    public function checkStatus()
    {
        $new_status = null;
        
        $all_measurement_sheets_confirmed = $this->isAllMeasurementSheetsConfirmed();
        $all_measurement_sheets_written = $this->isAllMeasurementSheetsWritten();
        
        if (
                $all_measurement_sheets_confirmed && 
                ($this->status === BuildingInterimBill::$WRITTEN_MEASUREMENT_SHEETS || $this->status === BuildingInterimBill::$PREPARATION)
            )
        {
            $new_status = BuildingInterimBill::$CONFIRMED_MEASUREMENT_SHEETS;
        }
        else if (
                $all_measurement_sheets_written && 
                $this->status === BuildingInterimBill::$PREPARATION
            )
        {
            $new_status = BuildingInterimBill::$WRITTEN_MEASUREMENT_SHEETS;
        }
        else if ($all_measurement_sheets_confirmed === false && $all_measurement_sheets_written === false)
        {
            $new_status = BuildingInterimBill::$PREPARATION;
        }

        if (!empty($new_status) && $new_status !== $this->status)
        {
            $this->status = $new_status;
            $this->save();
        }
    }
    
    public function isAllMeasurementSheetsWritten()
    {
        $result = true;

        foreach ($this->building_measurement_sheets as $building_measurement_sheet)
        {
            if ($building_measurement_sheet->written === false)
            {
                $result = false;
                break;
            }
        }
        
        return $result;
    }
    
    public function isAllMeasurementSheetsConfirmed()
    {
        $result = true;
        
        foreach ($this->building_measurement_sheets as $building_measurement_sheet)
        {
            if ($building_measurement_sheet->confirmed === false)
            {
                $result = false;
                break;
            }
        }
        
        return $result;
    }
    
    public function setAllMeasurementSheetsValidated(User $user, $validated)
    {
        foreach ($this->building_measurement_sheets as $building_measurement_sheet)
        {
            $building_measurement_sheet->validated = $validated;
            $building_measurement_sheet->validated_by_user_id = $user->id;
            $building_measurement_sheet->validated_timestamp = 'now()';
            $building_measurement_sheet->save();
        }
    }
}

BuildingInterimBill::$statuses = [
    'PREPARATION' => Yii::t('BuildingsModule.BuildingInterimBill', 'Preparation'),
    'WRITTEN_MEASUREMENT_SHEETS' => Yii::t('BuildingsModule.BuildingInterimBill', 'WrittenMeasurementSheets'),
    'CONFIRMED_MEASUREMENT_SHEETS' => Yii::t('BuildingsModule.BuildingInterimBill', 'ConfirmedMeasurementSheets'),
    'DELIVERED_MEASUREMENT_SHEETS' => Yii::t('BuildingsModule.BuildingInterimBill', 'DeliveredMeasurementSheets'),
    'VALIDATED_MEASUREMENT_SHEETS' => Yii::t('BuildingsModule.BuildingInterimBill', 'ValidatedMeasurementSheets'),
    'DELIVERED' => Yii::t('BuildingsModule.BuildingInterimBill', 'Delivered'),
    'VALIDATED' => Yii::t('BuildingsModule.BuildingInterimBill', 'Validated'),
    'PAID' => Yii::t('BuildingsModule.BuildingInterimBill', 'Paid')
];