<?php

class BillOfQuantityItemGroup extends SIMAActiveRecord
{
    public $is_delete_from_parent_group = false;
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    
    public function tableName()
    {
        return 'buildings.bill_of_quantities_item_groups';
    }
    
    public function moduleName()
    {
        return 'buildings';
    }
    
    public function __get($column) 
    {
        switch ($column) {
            case 'DisplayName': return $this->title;
            case 'SearchName': return $this->DisplayName;

            default: return parent::__get($column);
        }
    }
    
    public function rules()
    {
        return [
            ['title, bill_of_quantity_id', 'required'],
            ['parent_id, note, quantity, value_per_unit, value, order', 'safe'],
            ['quantity', 'checkNumericNumber', 'precision' => 20, 'scale' => 2],
            ['value_per_unit', 'checkNumericNumber', 'precision' => 20, 'scale' => 10],
            ['value', 'checkNumericNumber', 'precision' => 22, 'scale' => 2],
            ['order', 'numerical', 'integerOnly' => true],
            ['order', 'length', 'max' => 10],
            [
                'bill_of_quantity_id, parent_id', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => ['insert','update']
            ],
            ['title', 'unique_with', 'with' => ['bill_of_quantity_id','parent_id']],
            ['parent_id', 'checkParentCycle'],
            ['order', 'unique_with', 'with' => ['parent_id']]
        ];
    }
    
    public function relations($child_relations = [])
    {
        return [
            'bill_of_quantity' => [self::BELONGS_TO, BillOfQuantity::class, 'bill_of_quantity_id'],
            'parent' => [self::BELONGS_TO, BillOfQuantityItemGroup::class, 'parent_id'],
            'children' => [self::HAS_MANY, BillOfQuantityItemGroup::class, 'parent_id', 'scopes' => 'byOrder'],
            'children_count' => [self::STAT, BillOfQuantityItemGroup::class, 'parent_id', 'select' => 'count(*)'],
            'items' => [self::HAS_MANY, BillOfQuantityItem::class, 'group_id', 'scopes' => 'byOrder'],
            'items_count' => [self::STAT, BillOfQuantityItem::class, 'group_id', 'select' => 'count(*)'],
            'item_group_to_objects' => [self::HAS_MANY, BillOfQuantityItemGroupToObject::class, 'item_group_id'],
            'item_group_to_objects_leafs' => [self::HAS_MANY, BillOfQuantityItemGroupToObject::class, 'item_group_id', 'model_filter' => [
                'object' => [
                    'scopes' => ['onlyLeaf','byName']
                ]
            ]],
            'building_measurement_sheet_groups' => [self::HAS_MANY, BuildingMeasurementSheetGroup::class, 'bill_of_quantity_item_group_id']
        ];
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters': return [
                'title' => 'text',
                'bill_of_quantity' => 'relation',
                'parent' => 'relation',
                'note' => 'text',
                'quantity' => 'numeric',
                'value_per_unit' => 'numeric',
                'value' => 'numeric',
                'order' => 'integer',
                'building_measurement_sheet_groups' => 'relation'
            ];
            case 'textSearch': return [
                'title' => 'text'
            ];
            case 'big_numbers': return ['quantity', 'value_per_unit', 'value'];
            default: return parent::modelSettings($column);
        }
    }
    
    public function scopes()
    {
        $alias = $this->getTableAlias();
        $groups_tbl_name = BillOfQuantityItemGroup::model()->tableName();
        $uniq = SIMAHtml::uniqid();

        return [
            'byName' => [
                'order' => "$alias.title ASC"
            ],
            'byOrder' => [
                'order' => "$alias.order ASC"
            ],
            'onlyLeaf' => [
                'condition' => "not exists (select 1 from $groups_tbl_name group$uniq where group$uniq.parent_id=$alias.id)"
            ],
            'withoutRoot' => [
                'condition' => "$alias.parent_id is not null"
            ],
            'onlyRoot' => [
                'condition' => "$alias.parent_id is null"
            ]
        ];
    }
    
    protected function modelOptions(\User $user = null):array
    {
        return ['form', 'delete'];
    }
    
    public function beforeSave()
    {
        if ($this->scenario !== 'root_inserting' && empty($this->parent))
        {
            $root_group = self::addRootGroupForBillOfQuantity($this->bill_of_quantity);
            if ($this->isNewRecord || $this->id !== $root_group->id)
            {
                $this->parent_id = $root_group->id;
            }
        }
        
        return parent::beforeSave();
    }
    
    public function afterSave() 
    {
        parent::afterSave();
        
        if ($this->isNewRecord)
        {
            foreach ($this->bill_of_quantity->objects as $object)
            {
                $bill_of_quantity_item_group_to_object = BillOfQuantityItemGroupToObject::model()->findByAttributes([
                    'item_group_id' => $this->id,
                    'object_id' => $object->id
                ]);
                if (empty($bill_of_quantity_item_group_to_object))
                {
                    $bill_of_quantity_item_group_to_object = new BillOfQuantityItemGroupToObject();
                    $bill_of_quantity_item_group_to_object->item_group_id = $this->id;
                    $bill_of_quantity_item_group_to_object->object_id = $object->id;
                    $bill_of_quantity_item_group_to_object->save();
                }
            }
        }
        
        if ($this->columnChanged('parent_id'))
        {
            $building_construction = BuildingConstruction::model()->findByAttributes([
                'bill_of_quantity_id' => $this->bill_of_quantity_id
            ]);
            if (!empty($building_construction))
            {
                $first_interim_bill = BuildingInterimBill::model()->find([
                    'model_filter' => [
                        'display_scopes' => 'byDateAsc',
                        'building_construction' => [
                            'ids' => $building_construction->id
                        ]
                    ]
                ]);
            }
            
            foreach ($this->bill_of_quantity->leaf_objects as $leaf_object)
            {
                $leaf_object->calcValueRecursive();
                
                if (!empty($first_interim_bill))
                {
                    if (!empty($this->__old['parent_id']))
                    {
                        $measurement_sheet_group = BuildingMeasurementSheetGroup::model()->findByAttributes([
                            'building_interim_bill_id' => $first_interim_bill->id,
                            'bill_of_quantity_item_group_id' => $this->__old['parent_id'],
                            'object_id' => $leaf_object->id
                        ]);
                        if (!empty($measurement_sheet_group))
                        {
                            $measurement_sheet_group->calcValueDoneRecursive();
                        }
                    }
                    
                    if (!empty($this->parent_id))
                    {
                        $measurement_sheet_group1 = BuildingMeasurementSheetGroup::model()->findByAttributes([
                            'building_interim_bill_id' => $first_interim_bill->id,
                            'bill_of_quantity_item_group_id' => $this->parent_id,
                            'object_id' => $leaf_object->id
                        ]);
                        if (!empty($measurement_sheet_group1))
                        {
                            $measurement_sheet_group1->calcValueDoneRecursive();
                        }
                    }
                    
                    $leaf_object->calcValueDoneRecursiveForInterimBill($first_interim_bill);
                }
            }
        }
    }
    
    public function beforeDelete() 
    {
        foreach ($this->children as $children)
        {
            $children->is_delete_from_parent_group = true;
            $children->delete();
        }
        
        foreach ($this->item_group_to_objects as $item_group_to_object)
        {
            $item_group_to_object->delete();
        }
        
        foreach ($this->building_measurement_sheet_groups as $building_measurement_sheet_group)
        {
            $building_measurement_sheet_group->delete();
        }

        foreach ($this->items as $items)
        {
            $items->is_delete_from_group = true;
            $items->delete();
        }
        
        return parent::beforeDelete();
    }
    
    public function afterDelete()
    {
        if ($this->is_delete_from_parent_group === false)
        {
            foreach ($this->bill_of_quantity->leaf_objects as $leaf_object)
            {
                $leaf_object->calcValueRecursive();
            }
        }
    }
    
    public function calcValueRecursiveForObject(BillOfQuantityObject $object)
    {
        $group_to_object = BillOfQuantityItemGroupToObject::model()->findByAttributes([
            'object_id' => $object->id,
            'item_group_id' => $this->id
        ]);

        $quantity = SIMABigNumber::fromFloat(0.00, 2);
        $value_per_unit = SIMABigNumber::fromFloat(0.00, 10);
        $value = SIMABigNumber::fromFloat(0.00, 2);
        $value_done = SIMABigNumber::fromFloat(0.00, 2);
        $quantity_done = SIMABigNumber::fromFloat(0.00, 2);
        
        foreach ($this->children as $child)
        {
            $child_group_to_object = BillOfQuantityItemGroupToObject::get($child, $object);
            if (!empty($child_group_to_object))
            {
                $quantity = $quantity->plus($child_group_to_object->quantity);
                $value_per_unit = $value_per_unit->plus($child_group_to_object->value_per_unit);
                $value = $value->plus($child_group_to_object->value);
                $value_done = $value_done->plus($child_group_to_object->value_done);
                $quantity_done = $quantity_done->plus($child_group_to_object->quantity_done);
            }
        }

        foreach ($this->items as $item)
        {
            $item_to_object = BillOfQuantityItemToObject::get($item, $object);
            if (!empty($item_to_object))
            {
                $quantity = $quantity->plus($item_to_object->quantity);
                $value_per_unit = $value_per_unit->plus($item_to_object->value_per_unit);
                $value = $value->plus($item_to_object->value);
                $value_done = $value_done->plus($item_to_object->value_done);
                $quantity_done = $quantity_done->plus($item_to_object->quantity_done);
            }
        }
        
        $group_to_object->quantity = $quantity;
        if ($this->bill_of_quantity->has_object_value_per_unit)
        {
            $group_to_object->value_per_unit = $value_per_unit;
        }
        else
        {
            $group_to_object->item_group->value_per_unit = $value_per_unit;
            $group_to_object->item_group->update();
        }
        $group_to_object->value = $value;
        $group_to_object->value_done = $value_done;
        $group_to_object->quantity_done = $quantity_done;
        $group_to_object->save();
        
        if (isset($this->parent))
        {
            $this->parent->calcValueRecursiveForObject($object);
        }
    }
    
    public function afterValidateDependencyError(array $foreign_dependencies)
    {        
        $allowed_tables = [
            BillOfQuantityItemGroupToObject::tableName().':item_group_id',
            BillOfQuantityItemGroup::tableName().':parent_id',
            BillOfQuantityItem::tableName().':group_id',
            BuildingMeasurementSheetGroup::tableName().':bill_of_quantity_item_group_id'
        ];
        
        $parsed_foreign_dependencies = array_diff($foreign_dependencies, $allowed_tables);
        
        parent::afterValidateDependencyError($parsed_foreign_dependencies);
    }
    
    public function hasAllItemsSameMeasurementUnit($measurement_unit_id = null)
    {
        foreach ($this->items as $item)
        {
            if (
                    !empty($measurement_unit_id) && 
                    $measurement_unit_id !== $item->measurement_unit_id
                )
            {
                return false;
            }

            if (empty($measurement_unit_id))
            {
                $measurement_unit_id = $item->measurement_unit_id;
            }
        }
        
        foreach ($this->children as $child)
        {
            $child_is_same = $child->hasAllItemsSameMeasurementUnit($measurement_unit_id);
            if (!$child_is_same)
            {
                return false;
            }
        }
        
        return true;
    }
    
    public function getAllItemsModelTagsRecursive()
    {
        $model_tags = [];

        foreach ($this->items as $item)
        {
            $_tags = SIMAMisc::getModelTags($item);
            array_push($model_tags, $_tags[0]);
        }

        foreach ($this->children as $child)
        {
            $model_tags = array_merge($model_tags, $child->getAllItemsModelTagsRecursive());
        }
        
        return $model_tags;
    }
    
    public static function addRootGroupForBillOfQuantity($bill_of_quantity)
    {
        $root_group = self::getRootGroupForBillOfQuantity($bill_of_quantity);
        if (empty($root_group))
        {
            $root_group = new BillOfQuantityItemGroup();
            $root_group->scenario = 'root_inserting';
            $root_group->title = $bill_of_quantity->name;
            $root_group->bill_of_quantity_id = $bill_of_quantity->id;
            $root_group->save();
        }
        
        return $root_group;
    }
    
    public static function getRootGroupForBillOfQuantity($bill_of_quantity)
    {
        return BillOfQuantityItemGroup::model()->find([
            'model_filter' => [
                'bill_of_quantity' => [
                    'ids' => $bill_of_quantity->id
                ],
                'scopes' => 'onlyRoot'
            ]
        ]);
    }
    
    public function getFirstItemRecursively()
    {
        if ($this->items_count > 0)
        {
            return $this->items[0];
        }
        
        foreach ($this->children as $child)
        {
            $first_item = $child->getFirstItemRecursively();
            if (!empty($first_item))
            {
                return $first_item;
            }
        }
        
        return null;
    }
    
    public function cloneGroupRecursive($new_bill_of_quantity, $parent_id, $old_object_ids_mapper)
    {
        $new_group = new BillOfQuantityItemGroup();
        $new_group->setAttributes($this->getAttributes());
        $new_group->bill_of_quantity_id = $new_bill_of_quantity->id;
        $new_group->parent_id = $parent_id;
        if (empty($parent_id))
        {
            $new_group->scenario = 'root_inserting';
        }
        $new_group->save();

        foreach ($this->item_group_to_objects as $item_group_to_object)
        {
            $new_object_id = $old_object_ids_mapper[$item_group_to_object->object_id];
            if (!empty($new_object_id))
            {
                $new_item_group_to_object = BillOfQuantityItemGroupToObject::model()->findByAttributes([
                    'item_group_id' => $new_group->id,
                    'object_id' => $new_object_id
                ]);
                if (empty($new_item_group_to_object))
                {
                    $new_item_group_to_object = new BillOfQuantityItemGroupToObject();
                }
                $new_item_group_to_object->setAttributes($item_group_to_object->getAttributes());
                $new_item_group_to_object->item_group_id = $new_group->id;
                $new_item_group_to_object->object_id = $new_object_id;
                $new_item_group_to_object->save();
            }
        }

        foreach ($this->children as $children)
        {
            $children->cloneGroupRecursive($new_bill_of_quantity, $new_group->id, $old_object_ids_mapper);
        }
        
        foreach ($this->items as $item)
        {
            $item->cloneItem($new_group->id, $old_object_ids_mapper);
        }
    }
    
    public function calcOrderPathRecursively() 
    {
        if (empty($this->parent)) //ako je root grupa onda se njen order ne racuna u putanju jer se root grupa ne prikazuje
        {
            return '';
        }
        
        if (!empty($this->parent))
        {
            $parent_path = $this->parent->calcOrderPathRecursively();
        }
        
        return  !empty($parent_path) ? $parent_path . "." . $this->order : $this->order;
    } 
    
    public function getApproximatelyValuePerUnit()
    {
        $total_value = SIMABigNumber::fromFloat(0.00, 2);
        $total_quantity = SIMABigNumber::fromFloat(0.00, 2);
        
        foreach ($this->item_group_to_objects as $item_group_to_object)
        {
            if ($item_group_to_object->object->children_count === 0)
            {
                $total_value = $total_value->plus($item_group_to_object->value);
                $total_quantity = $total_quantity->plus($item_group_to_object->quantity);
            }
        }
        
        if (!$total_value->isZero() && !$total_quantity->isZero())
        {
            return $total_value->divide($total_quantity, 10);
        }
        
        return SIMABigNumber::fromFloat(0.00, 10);
    }
}

