<?php

/**
 * Description of BuildingProjectSheet
 *
 * @author goran-set
 */
class BuildingProjectSheet extends SIMAActiveRecord
{
    private $_scopes_uniq = null;
    
    
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'buildings.projects_sheets';
    }

    public function moduleName() {
        return 'buildings';
    }

    public function __get($column) {
        switch ($column) {
            case 'DisplayName': return "Korica: ".$this->name;
            case 'SearchName': return $this->DisplayName;
            default: return parent::__get($column);
        }
    }
    
    public function rules() {
        return array(
            array('name','required','message' => 'Polje "{attribute}" mora biti uneto'),
            array('number, name, id, archive_box_id, description', 'safe'),
            array('archive_box_id, number,id ', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => array('insert', 'update'))
        );
    }

    public function relations($child_relations = [])
    {
        return array(
            'building_projects' => array(self::HAS_MANY, 'BuildingProject', 'building_projects_sheet_id', 'order'=>'"order" asc'),
            'building_project_books' => array(self::HAS_MANY, 'BuildingProject', 'building_projects_sheet_id', 
                                'condition'=>"prefix='book'"),
            'base_building_project'=>array(self::HAS_ONE, 'BuildingProject', 'building_projects_sheet_id',
                'condition'=>'parent_id is null'),
            'building_project_notebooks' => array(self::HAS_MANY, 'BuildingProject', 'building_projects_sheet_id', 
                                'condition'=>"prefix='notebook'", 'order'=>'"order" asc'),
            'building_project_notebook_parts' => array(self::HAS_MANY, 'BuildingProject', 'building_projects_sheet_id', 
                                'condition'=>"prefix='part'", 'order'=>'"order" asc'),
             'archive_box' => array(self::BELONGS_TO, 'ArchiveBox', 'archive_box_id')
        );
    }
    
    public function generateNumber()
    {
        if(empty($this->number))
        {
            $number= $this->building_projects[0]->number."/".$this->building_projects[0]->building_project_type->short_name;
            foreach ($this->building_project_books as $bp)
            {   
                $number.="/".$bp->order;
                foreach ($bp->notebooks as $notebook)
                {
                    $number.='.'.$notebook->order;
                }
            } 

            if(count($this->building_project_books)==0 && count($this->building_project_notebooks)>0)
            {
                $number.="/";
                foreach ($this->building_project_notebooks as $nt)
                {     
                    $number.=$nt->order.".";
                } 
            }

            $this->number=$number;
            $this->save();
        }
        
        return $this->number;
    }
    
    public function scopes() {
        if ($this->_scopes_uniq==null)
        {
            $this->_scopes_uniq = SIMAHtml::uniqid();
        }
        $uniq = $this->_scopes_uniq;
        $alias = $this->getTableAlias();
        
        
        return array(
            'recently' => array(
                'order' => 'id',
            )
        );
    }
    
    public function modelSettings($column)
    {        
        switch ($column)
        {
            case 'textSearch': return array(
                'name' => 'text'
            );
            case 'filters': return array(
                'archive_box_id'=>array('func'=>'filter_archive_box_id')
            );
            default: return parent::modelSettings($column);
        }
    }
    
    public function filter_archive_box_id($condition)
    {        
        if(isset($this->archive_box_id) && $this->archive_box_id!=null)
        {
            $param_id=  SIMAHtml::uniqid();
            $alias = $this->getTableAlias();
            $temp_condition=new CDbCriteria();
            $temp_condition->condition="${alias}.archive_box_id=:param${param_id}";
            $temp_condition->params=array(':param'.$param_id=>$this->archive_box_id);
            $condition->mergeWith($temp_condition, true);
        }
    }

}
