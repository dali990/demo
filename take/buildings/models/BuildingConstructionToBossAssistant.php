<?php

class BuildingConstructionToBossAssistant extends SIMAActiveRecord
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
 
    public function tableName()
    {
        return 'buildings.constructions_to_boss_assistants';
    }
    
    public function moduleName()
    {
        return 'buildings';
    }
    
    public function rules()
    {
        return [
            ['building_construction_id, boss_assistant_id', 'required'],
            ['boss_assistant_id', 'unique_with', 'with' => ['building_construction_id']]
        ];
    }
    
    public function relations($child_relations = [])
    {
        return [
            'building_construction' => [self::BELONGS_TO, BuildingConstruction::class, 'building_construction_id'],
            'boss_assistant' => [self::BELONGS_TO, User::class, 'boss_assistant_id']
        ];
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters' : return array_merge(parent::modelSettings('filters'), [
                'building_construction' => 'relation',
                'boss_assistant' => 'relation'
            ]);
            default: return parent::modelSettings($column);
        }
    }
}
