<?php

class BuildingSiteDiaryWorkHourMechanization extends SIMAActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function moduleName()
    {
        return 'buildings';
    }

    public function tableName()
    {
        return 'buildings.site_diary_work_hour_mechanizations';
    }
    
    public function rules()
    {
        return [
            ['site_diary_work_hour_id, mechanization_type_id', 'required'],
            ['mechanization_id, mechanization_count', 'safe'],
            [
                'site_diary_work_hour_id, mechanization_type_id, mechanization_id', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => ['insert','update']
            ],
            ['mechanization_id', 'checkMechanization'],
            ['mechanization_count', 'numerical', 'integerOnly' => true],
            ['mechanization_count', 'length', 'max' => 9],
            ['mechanization_count', 'checkMechanizationCount']
        ];
    }
    
    public function beforeValidate() 
    {
        if (!empty($this->mechanization_id))
        {
            $this->mechanization_count = 1;
        }
        
        return parent::beforeValidate();
    }
    
    public function checkMechanization()
    {
        if (
                !$this->hasErrors() && 
                !empty($this->mechanization_id) && 
                ($this->isNewRecord || $this->columnChanged('mechanization_type_id') || $this->columnChanged('mechanization_id')))
        {
            $mechanization_type = FixedAssetType::model()->withAllNames()->findByPkWithCheck($this->mechanization_type_id);
            $allowed_types = explode(',',str_replace(['{','}'], '', $mechanization_type->children_ids));
            array_push($allowed_types, $this->mechanization_type_id);
            if (!in_array($this->mechanization->fixed_asset_type_id, $allowed_types))
            {
                $this->addError('mechanization_id', Yii::t('BuildingsModule.BuildingSiteDiaryWorkHourMechanization', 'MechanizationMustBelongsToType', [
                    '{mechanization_type}' => $this->mechanization_type->DisplayName
                ]));
            }
        }
    }
    
    public function checkMechanizationCount()
    {
        if (!$this->hasErrors() && ($this->isNewRecord || $this->columnChanged('mechanization_count') || $this->columnChanged('mechanization_id')))
        {
            if (!empty($this->mechanization_id) && intval($this->mechanization_count) !== 1)
            {
                $this->addError('mechanization_count', Yii::t('BuildingsModule.BuildingSiteDiaryWorkHourMechanization', 'MechanizationCountMustBeOne'));
            }
            if (empty($this->mechanization_id) && intval($this->mechanization_count) < 1)
            {
                $this->addError('mechanization_count', Yii::t('BuildingsModule.BuildingSiteDiaryWorkHourMechanization', 'MechanizationCountMustBeEqualOrGreaterThanOne'));
            }
        }
    }

    public function relations($child_relations = [])
    {
        return [
            'site_diary_work_hour' => [self::BELONGS_TO, BuildingSiteDiaryWorkHour::class, 'site_diary_work_hour_id'],
            'mechanization_type' => [self::BELONGS_TO, FixedAssetType::class, 'mechanization_type_id'],
            'mechanization' => [self::BELONGS_TO, FixedAsset::class, 'mechanization_id']
        ];
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters': return [
                'site_diary_work_hour' => 'relation',
                'mechanization_type' => 'relation',
                'mechanization' => 'relation',
                'mechanization_count' => 'integer'
            ];
            default: return parent::modelSettings($column);
        }
    }
    
    protected function modelOptions(\User $user = null):array
    {
        return ['form', 'delete'];
    }
}