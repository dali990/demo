<?php

class BuildingProjectKind extends SIMAActiveRecord {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'buildings.project_kinds';
    }

    public function moduleName() {
        return 'buildings';
    }

    public function __get($column) {
        switch ($column) {
            case 'DisplayName':
            case 'SearchName': return $this->name;
            default: return parent::__get($column);
        }
    }

    public function rules() {
        return array(
            array('name', 'required', 'message' => 'Polje "{attribute}" mora biti uneto'),
            array('description', 'safe'),
        );
    }

    public function scopes() {
        return array(
            'byName' => array(
                'order' => 'name, id',
            ),
        );
    }

    public function relations($child_relations = [])
    {
        return array(
            'building_projects' => array(self::HAS_MANY, 'BuildingProject', 'building_project_kind_id'),
        );
    }
    
    public function modelSettings($column)
    {
        switch ($column) {
            case 'filters': return [
                'name' => 'text',
                'description' => 'text',
            ];
            default: return parent::modelSettings($column);
        }
    }

}