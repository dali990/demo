<?php

class BuildingMeasurementSheet extends SIMAActiveRecord
{
    public $items_amount_sum = 0;
    public $is_delete_from_interim_bill = false;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function moduleName()
    {
        return 'buildings';
    }

    public function tableName()
    {
        return 'buildings.measurement_sheets';
    }
    
    public function __get($column) 
    {
        switch ($column) {
            case 'DisplayName': return "{$this->bill_of_quantity_item->DisplayName} ".Yii::t('BuildingsModule.BuildingInterimBill', 'BuildingInterimBill').": {$this->building_interim_bill->DisplayName}";
            case 'SearchName': return $this->DisplayName;
            case 'total_quantity_done':
                return $this->pre_quantity_done->plus($this->quantity_done);
            case 'total_value_done':
                return $this->pre_value_done->plus($this->value_done);
            case 'total_percentage_done':
                return $this->getTotalPercentageDone();
            case 'pre_percentage_done':
                return $this->getPrePercentageDone();
            case 'percentage_done':
                return $this->getPercentageDone();

            default: return parent::__get($column);
        }
    }
    
    public function rules()
    {
        return [
            ['building_interim_bill_id, bill_of_quantity_item_id, object_id', 'required'],
            ['value_done, quantity_done, pre_value_done, pre_quantity_done, quantity_validated', 'safe'],
            ['written, written_by_user_id, written_timestamp', 'safe'],
            ['confirmed, confirmed_by_user_id, confirmed_timestamp', 'safe'],
            ['validated, validated_by_user_id, validated_timestamp', 'safe'],
            [
                'building_interim_bill_id, bill_of_quantity_item_id, object_id', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => ['insert','update']
            ],
            ['bill_of_quantity_item_id', 'unique_with', 'with' => ['building_interim_bill_id', 'object_id'], 'on' => ['insert','update']],
            ['bill_of_quantity_item_id', 'checkBillOfQuantityItem'],
            ['object_id', 'checkObject']
        ];
    }
    
    public function checkObject()
    {
        if (!$this->hasErrors())
        {
            if (intval($this->object->bill_of_quantity_id) !== intval($this->building_interim_bill->building_construction->bill_of_quantity_id))
            {
                $this->addError('object_id', Yii::t('BuildingsModule.BuildingMeasurementSheet', 'ObjectMustBelongsToBillOfQuantity', [
                    '{bill_of_quantity_name}' => $this->building_interim_bill->building_construction->bill_of_quantity->DisplayName
                ]));
            }
        }
    }
    
    public function checkBillOfQuantityItem()
    {
        if (!$this->hasErrors())
        {
            if (intval($this->bill_of_quantity_item->group->bill_of_quantity_id) !== intval($this->building_interim_bill->building_construction->bill_of_quantity_id))
            {
                $this->addError('bill_of_quantity_item_id', Yii::t('BuildingsModule.BuildingMeasurementSheet', 'BillOfQuantityItemMustBelongsToBillOfQuantity', [
                    '{bill_of_quantity_name}' => $this->building_interim_bill->building_construction->bill_of_quantity->DisplayName
                ]));
            }
        }
    }

    public function relations($child_relations = [])
    {
        return [
            'building_interim_bill' => [self::BELONGS_TO, BuildingInterimBill::class, 'building_interim_bill_id'],
            'bill_of_quantity_item' => [self::BELONGS_TO, BillOfQuantityItem::class, 'bill_of_quantity_item_id'],
            'object' => [self::BELONGS_TO, BillOfQuantityObject::class, 'object_id'],
            'sheet_items' => [self::HAS_MANY, BuildingMeasurementSheetItem::class, 'building_measurement_sheet_id'],
            'written_by_user' => [self::BELONGS_TO, User::class, 'written_by_user_id'],
            'confirmed_by_user' => [self::BELONGS_TO, User::class, 'confirmed_by_user_id'],
            'validated_by_user' => [self::BELONGS_TO, User::class, 'validated_by_user_id']
        ];
    }
    
    public function scopes()
    {
        $alias = $this->getTableAlias();
        $measurement_sheet_items_table_name = BuildingMeasurementSheetItem::model()->tableName();
        $building_interim_bills_table_name = BuildingInterimBill::model()->tableName();
        $uniq = SIMAHtml::uniqid();
        
        return [
            'onlyZero' => [
                'condition' => "
                    not exists (
                        select 1 from $measurement_sheet_items_table_name si$uniq 
                        where si$uniq.building_measurement_sheet_id = $alias.id 
                        group by building_measurement_sheet_id 
                        having sum(si$uniq.amount) > 0
                    )
                "
            ],
            'onlyOnFirstInterimBill' => [
                'join' => "left join $building_interim_bills_table_name bib$uniq on bib$uniq.id = $alias.building_interim_bill_id",
                'condition' => "
                    not exists (
                        select 1 from $building_interim_bills_table_name bib2$uniq 
                        where bib2$uniq.issue_date < bib$uniq.issue_date and bib2$uniq.building_construction_id = bib$uniq.building_construction_id
                    )
                "
            ]
        ];
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters': return [
                'building_interim_bill' => 'relation',
                'bill_of_quantity_item' => 'relation',
                'object' => 'relation',
                'value_done' => 'numeric',
                'quantity_done' => 'numeric',
                'pre_value_done' => 'numeric', 
                'pre_quantity_done' => 'numeric',
                'quantity_validated' => 'numeric',
                'sheet_items' => 'relation',
                'written' => 'boolean',
                'written_by_user' => 'relation',
                'confirmed' => 'boolean',
                'confirmed_by_user' => 'relation',
                'validated' => 'boolean',
                'validated_by_user' => 'relation'
            ];
            case 'textSearch': return [
                'building_interim_bill' => 'relation',
                'bill_of_quantity_item' => 'relation'
            ];
            case 'big_numbers': return ['value_done', 'quantity_done', 'pre_value_done', 'pre_quantity_done', 'quantity_validated'];
            case 'statuses': return [
                'written' => [
                    'title' => Yii::t('BuildingsModule.BuildingMeasurementSheet', 'Written'),
                    'timestamp' => 'written_timestamp',
                    'user' => ['written_by_user_id', 'relName' => 'written_by_user'],
                    'checkAccessConfirm' => 'checkCanWrite',
                    'checkAccessRevert' => 'checkCanWrite',
                    'onConfirm' => 'onWrittenConfirmRevert',
                    'onRevert' => 'onWrittenConfirmRevert'
                ],
                'confirmed' => [
                    'title' => Yii::t('BuildingsModule.BuildingMeasurementSheet', 'Confirmed'),
                    'timestamp' => 'confirmed_timestamp',
                    'user' => ['confirmed_by_user_id', 'relName' => 'confirmed_by_user'],
                    'checkAccessConfirm' => 'checkCanConfirm',
                    'checkAccessRevert' => 'checkCanConfirm',
                    'onConfirm' => 'onConfirmedConfirmRevert',
                    'onRevert' => 'onConfirmedConfirmRevert'
                ],
                'validated' => [
                    'title' => Yii::t('BuildingsModule.BuildingMeasurementSheet', 'Validated'),
                    'timestamp' => 'validated_timestamp',
                    'user' => ['validated_by_user_id', 'relName' => 'validated_by_user'],
                    'checkAccessConfirm' => 'checkCanValidate',
                    'checkAccessRevert' => 'checkCanValidate'
                ]
            ];
            default: return parent::modelSettings($column);
        }
    }
    
    //onaj ko je kreirao obracunski list moze da stavi da je napisan ili privilegija
    //mora biti nepotvrdjen i ne overen
    public function checkCanWrite()
    {
        $result = $this->confirmed === false &&
                  $this->validated === false && 
                  (
                      $this->written_by_user_id === Yii::app()->user->id || 
                      Yii::app()->user->checkAccess('modelThemeAdmin')
                  );

        return $result ? true : [Yii::t('BuildingsModule.BillOfQuantity', 'YouDoNotHavePrivilege')];
    }
    
    //koordinator teme ili privilegija moze da potvrdi obracunski list. Da bi se potvrdio mora prethodno da bude napisan i ne overen
    public function checkCanConfirm()
    {
        $result = $this->written === true && 
                  $this->validated === false && 
                  !empty(Theme::model()->hasChangeAccess(Yii::app()->user->id)->findByPk($this->building_interim_bill->building_construction_id));
        
        return $result ? true : [Yii::t('BuildingsModule.BillOfQuantity', 'YouDoNotHavePrivilege')];
    }
    
    //radi se automatski iz sitaucije
    public function checkCanValidate()
    {
        return [Yii::t('BuildingsModule.BillOfQuantity', 'YouDoNotHavePrivilege')];
    }
    
    public function onWrittenConfirmRevert()
    {
        $this->afterMeasurementSheetStatusChanged();
    }
    
    public function onConfirmedConfirmRevert()
    {
        $this->afterMeasurementSheetStatusChanged();
    }
    
    protected function modelOptions(\User $user = null):array
    {
        $options = [];

        if (self::hasChangeAccess($this->building_interim_bill->building_construction, $user))
        {
            $options = ['form', 'delete'];
        }

        return $options;
    }
    
    /**
     * Korisnik ima pravo da menja gradjevinski list(add/edit/delete) ako ima pravo da menja situaciju ili
     * ako je pomocnik sefa gradilista na izvodjenju
     * @param BuildingConstruction $building_construction
     * @param User $user
     * @return boolean
     */
    public static function hasChangeAccess(BuildingConstruction $building_construction, User $user = null)
    {
        if (empty($user))
        {
            return false;
        }
        
        return  BuildingInterimBill::hasChangeAccess($building_construction, $user) ||
                in_array($user->id, $building_construction->getBossAssistantIds());
    }
    
    public function itemsAmountSumForObject($object_id)
    {
        $uniq = SIMAHtml::uniqid();
        $alias = $this->getTableAlias();
        $measurement_sheet_items_table_name = BuildingMeasurementSheetItem::model()->tableName();

        $this->getDbCriteria()->mergeWith([
            'select' => "$alias.*, sum(si$uniq.amount) as items_amount_sum",
            'join' => "
                left join $measurement_sheet_items_table_name si$uniq on si$uniq.building_measurement_sheet_id = $alias.id
            ",
            'condition' => "$alias.object_id = :param1$uniq",
            'params' => [
                ":param1$uniq" => $object_id
            ],
            'group' => "$alias.object_id, $alias.id"
        ]);

        return $this;
    }
    
    public function forObject($object_id)
    {
        $uniq = SIMAHtml::uniqid();
        $alias = $this->getTableAlias();

        $this->getDbCriteria()->mergeWith([
            'condition' => "$alias.object_id = :param1$uniq",
            'params' => [
                ":param1$uniq" => $object_id
            ]
        ]);

        return $this;
    }
    
    public function forBillOfQuantityItem($bill_of_quantity_item_id)
    {
        $uniq = SIMAHtml::uniqid();
        $alias = $this->getTableAlias();

        $this->getDbCriteria()->mergeWith([
            'condition' => "$alias.bill_of_quantity_item_id = :param1$uniq",
            'params' => [
                ":param1$uniq" => $bill_of_quantity_item_id
            ]
        ]);

        return $this;
    }
    
    public function calcValueDone()
    {
        $quantity_done = SIMABigNumber::fromFloat(0.00, 2);
        foreach ($this->sheet_items as $sheet_item)
        {
            $quantity_done = $quantity_done->plus($sheet_item->amount);
        }
        $this->quantity_done = $quantity_done;
        $item_to_object = BillOfQuantityItemToObject::model()->findByAttributes([
            'item_id' => $this->bill_of_quantity_item_id,
            'object_id' => $this->object_id
        ]);
        $value_per_unit = $this->bill_of_quantity_item->value_per_unit;
        if (!empty($item_to_object))
        {
            $value_per_unit = $item_to_object->value_per_unit;
        }
        $this->value_done = $this->quantity_done->multiple($value_per_unit);
        $this->save();

        $newer_interim_bills = BuildingInterimBill::model()->findAll([
            'model_filter' => [
                'display_scopes' => 'byDateAsc',
                'issue_date' => ['>', $this->building_interim_bill->issue_date],
                'building_construction' => [
                    'ids' => $this->building_interim_bill->building_construction_id
                ]
            ]
        ]);

        $pre_quantity_done = $this->pre_quantity_done->plus($this->quantity_done);
        $pre_value_done = $this->pre_value_done->plus($this->value_done);
        foreach ($newer_interim_bills as $newer_interim_bill)
        {
            $sheet = BuildingMeasurementSheet::get($newer_interim_bill, $this->bill_of_quantity_item, $this->object);
            $sheet->pre_quantity_done = $pre_quantity_done;
            $sheet->pre_value_done = $pre_value_done;
            $sheet->save();

            $pre_quantity_done = $pre_quantity_done->plus($sheet->quantity_done);
            $pre_value_done = $pre_value_done->plus($sheet->value_done);
        }
        
        $sheet_group = BuildingMeasurementSheetGroup::get($this->building_interim_bill, $this->bill_of_quantity_item->group, $this->object);
        $sheet_group->calcValueDoneRecursive();
    }
    
    public function getPrePercentageDone()
    {
        $item_to_object = BillOfQuantityItemToObject::model()->findByAttributes([
            'item_id' => $this->bill_of_quantity_item_id,
            'object_id' => $this->object_id
        ]);
        
        if (!empty($item_to_object))
        {
            $value = $item_to_object->value;
            if (!$value->isZero())
            {
                return $this->pre_value_done->divide($value)->multiple(SIMABigNumber::fromInteger(100), 2);
            }
        }
        
        return SIMABigNumber::fromFloat(0.00, 2);
    }
    
    public function getPercentageDone()
    {
        $item_to_object = BillOfQuantityItemToObject::model()->findByAttributes([
            'item_id' => $this->bill_of_quantity_item_id,
            'object_id' => $this->object_id
        ]);
        
        if (!empty($item_to_object))
        {
            $value = $item_to_object->value;
            if (!$value->isZero())
            {
                return $this->value_done->divide($value)->multiple(SIMABigNumber::fromInteger(100), 2);
            }
        }
        
        return SIMABigNumber::fromFloat(0.00, 2);
    }
    
    public function getTotalPercentageDone()
    {
        $item_to_object = BillOfQuantityItemToObject::model()->findByAttributes([
            'item_id' => $this->bill_of_quantity_item_id,
            'object_id' => $this->object_id
        ]);
        
        if (!empty($item_to_object))
        {
            $value = $item_to_object->value;
            if (!$value->isZero())
            {
                $curr_total_value = $this->pre_value_done->plus($this->value_done);
                return $curr_total_value->divide($value)->multiple(SIMABigNumber::fromInteger(100), 2);
            }
        }
        
        return SIMABigNumber::fromFloat(0.00, 2);
    }
    
    public function afterSave()
    {
        parent::afterSave();
        
        if ($this->is_delete_from_interim_bill === false)
        {
            $this->afterMeasurementSheetStatusChanged();
        }
    }
    
    public function beforeDelete()
    {
        foreach ($this->sheet_items as $sheet_item) 
        {
            $sheet_item->deleting_from_building_measurement_sheet = true;
            $sheet_item->delete();
        }
        
        return parent::beforeDelete();
    }
    
    public function afterDelete() 
    {
        parent::afterDelete();
        
        if ($this->is_delete_from_interim_bill === false)
        {
            $this->afterMeasurementSheetStatusChanged();
        }
    }
    
    public function afterValidateDependencyError(array $foreign_dependencies)
    {        
        $allowed_tables = [
            BuildingMeasurementSheetItem::tableName().':building_measurement_sheet_id'
        ];
        
        $parsed_foreign_dependencies = array_diff($foreign_dependencies, $allowed_tables);
        
        parent::afterValidateDependencyError($parsed_foreign_dependencies);
    }
    
    public static function get($building_interim_bill, $bill_of_quantity_item, $object)
    {
        $building_measurement_sheet = BuildingMeasurementSheet::model()->findByAttributes([
            'building_interim_bill_id' => $building_interim_bill->id,
            'bill_of_quantity_item_id' => $bill_of_quantity_item->id,
            'object_id' => $object->id
        ]);
        if (empty($building_measurement_sheet))
        {
            $building_measurement_sheet = new BuildingMeasurementSheet();
            $building_measurement_sheet->building_interim_bill_id = $building_interim_bill->id;
            $building_measurement_sheet->bill_of_quantity_item_id = $bill_of_quantity_item->id;
            $building_measurement_sheet->object_id = $object->id;
            $building_measurement_sheet->save();
        }
        
        return $building_measurement_sheet;
    }
    
    private function afterMeasurementSheetStatusChanged()
    {
        if ($this->object->children_count === 0)
        {
            $sheet_group = BuildingMeasurementSheetGroup::get($this->building_interim_bill, $this->bill_of_quantity_item->group, $this->object);

            Yii::app()->registerAfterWork(BuildingMeasurementSheetStatusChanged::class, $sheet_group, false);
        }
    }
}