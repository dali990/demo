<?php

class BuildingSiteDiaryWorkHour extends SIMAActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function moduleName()
    {
        return 'buildings';
    }

    public function tableName()
    {
        return 'buildings.site_diary_work_hours';
    }
    
    public function __get($column) 
    {
        switch ($column) {
            case 'DisplayName': 
                return  Yii::t('BuildingsModule.BuildingSiteDiaryWorkHour', 'From') .
                        " {$this->start_datetime} " . 
                        Yii::t('BuildingsModule.BuildingSiteDiaryWorkHour', 'To') .
                        " {$this->end_datetime}";
            case 'SearchName': return $this->DisplayName;

            default: return parent::__get($column);
        }
    }
    
    public function rules()
    {
        return [
            ['site_diary_id, start_datetime, end_datetime', 'required'],
            ['note', 'safe'],
            [
                'site_diary_id', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => ['insert','update']
            ],
            ['start_datetime', 'checkStartDatetime'],
            ['end_datetime', 'checkEndDatetime']
        ];
    }
    
    public function checkStartDatetime()
    {
        if (!$this->hasErrors() && ($this->isNewRecord || $this->columnChanged('start_datetime')))
        {
            if (SIMAHtml::DbToUserDate($this->start_datetime) !== SIMAHtml::DbToUserDate($this->site_diary->date))
            {
                $this->addError('start_datetime', Yii::t('BuildingsModule.BuildingSiteDiaryWorkHour', 'StartDateTimeMustBeAtDate', [
                    '{date}' => SIMAHtml::DbToUserDate($this->site_diary->date)
                ]));
            }
        }
    }

    public function checkEndDatetime()
    {
        if (!$this->hasErrors())
        {
            if ($this->isNewRecord || $this->columnChanged('start_datetime') || $this->columnChanged('end_datetime'))
            {
                if ($this->start_datetime >= $this->end_datetime)
                {
                    $this->addError('end_datetime', Yii::t('BuildingsModule.BuildingSiteDiaryWorkHour', 'EndDateTimeMustBeGreaterThanStartDateTime'));
                }
            }
        }
    }

    public function relations($child_relations = [])
    {
        return [
            'site_diary' => [self::BELONGS_TO, BuildingSiteDiary::class, 'site_diary_id'],
            'building_site_diary_work_hour_workers' => [self::HAS_MANY, BuildingSiteDiaryWorkHourWorker::class, 'site_diary_work_hour_id'],
            'building_site_diary_work_hour_workers_count' => [self::STAT, BuildingSiteDiaryWorkHourWorker::class, 'site_diary_work_hour_id', 
                'select' => 'sum(worker_count)'
            ],
            'building_site_diary_work_hour_worker_workers' => [self::HAS_MANY, BuildingSiteDiaryWorkHourWorker::class, 'site_diary_work_hour_id', 
                'condition' => "worker_type = '".BuildingSiteDiaryWorkHourWorker::$WORKER."'"
            ],
            'building_site_diary_work_hour_worker_workers_count' => [self::STAT, BuildingSiteDiaryWorkHourWorker::class, 'site_diary_work_hour_id', 
                'select' => 'sum(worker_count)',
                'condition' => "worker_type = '".BuildingSiteDiaryWorkHourWorker::$WORKER."'"
            ],
            'building_site_diary_work_hour_craftsman_workers' => [self::HAS_MANY, BuildingSiteDiaryWorkHourWorker::class, 'site_diary_work_hour_id', 
                'condition' => "worker_type = '".BuildingSiteDiaryWorkHourWorker::$CRAFTSMAN."'"
            ],
            'building_site_diary_work_hour_craftsman_workers_count' => [self::STAT, BuildingSiteDiaryWorkHourWorker::class, 'site_diary_work_hour_id', 
                'select' => 'sum(worker_count)',
                'condition' => "worker_type = '".BuildingSiteDiaryWorkHourWorker::$CRAFTSMAN."'"
            ],
            'building_site_diary_work_hour_tehnical_staff_workers' => [self::HAS_MANY, BuildingSiteDiaryWorkHourWorker::class, 'site_diary_work_hour_id', 
                'condition' => "worker_type = '".BuildingSiteDiaryWorkHourWorker::$TEHNICAL_STAFF."'"
            ],
            'building_site_diary_work_hour_tehnical_staff_workers_count' => [self::STAT, BuildingSiteDiaryWorkHourWorker::class, 'site_diary_work_hour_id', 
                'select' => 'sum(worker_count)',
                'condition' => "worker_type = '".BuildingSiteDiaryWorkHourWorker::$TEHNICAL_STAFF."'"
            ],
            'building_site_diary_work_hour_other_workers' => [self::HAS_MANY, BuildingSiteDiaryWorkHourWorker::class, 'site_diary_work_hour_id', 
                'condition' => "worker_type = '".BuildingSiteDiaryWorkHourWorker::$OTHERS."'"
            ],
            'building_site_diary_work_hour_other_workers_count' => [self::STAT, BuildingSiteDiaryWorkHourWorker::class, 'site_diary_work_hour_id', 
                'select' => 'sum(worker_count)',
                'condition' => "worker_type = '".BuildingSiteDiaryWorkHourWorker::$OTHERS."'"
            ],
            'building_site_diary_work_hour_mechanizations' => [self::HAS_MANY, BuildingSiteDiaryWorkHourMechanization::class, 'site_diary_work_hour_id'],
            'building_site_diary_work_hour_mechanizations_count' => [self::STAT, BuildingSiteDiaryWorkHourMechanization::class, 'site_diary_work_hour_id', 'select' => 'sum(mechanization_count)']
        ];
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters': return [
                'site_diary' => 'relation',
                'start_datetime' => 'date_time_range',
                'end_datetime' => 'date_time_range',
                'note' => 'text'
            ];
            default: return parent::modelSettings($column);
        }
    }
    
    protected function modelOptions(\User $user = null):array
    {
        return ['form', 'delete'];
    }
}