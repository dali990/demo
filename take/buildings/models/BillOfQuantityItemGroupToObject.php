<?php

class BillOfQuantityItemGroupToObject extends SIMAActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    
    public function tableName()
    {
        return 'buildings.bill_of_quantities_item_groups_to_objects';
    }
    
    public function moduleName()
    {
        return 'buildings';
    }
    
    public function rules()
    {
        return [
            ['item_group_id, object_id', 'required'],
            ['quantity, value_per_unit, value, value_done, quantity_done', 'safe'],
            [
                'item_group_id, object_id', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => ['insert','update']
            ],
            ['quantity', 'checkNumericNumber', 'precision' => 20, 'scale' => 2],
            ['value_per_unit', 'checkNumericNumber', 'precision' => 20, 'scale' => 10],
            ['value', 'checkNumericNumber', 'precision' => 22, 'scale' => 2],
            ['item_group_id', 'unique_with', 'with' => ['object_id']],
        ];
    }
    
    public function relations($child_relations = [])
    {
        return [
            'item_group' => [self::BELONGS_TO, BillOfQuantityItemGroup::class, 'item_group_id'],
            'object' => [self::BELONGS_TO, BillOfQuantityObject::class, 'object_id'],
        ];
    }
    
    public function forObject($object_id)
    {
        $alias = $this->getTableAlias();
        $uniq = SIMAHtml::uniqid();
        
        $this->getDbCriteria()->mergeWith([
            'condition'=> "$alias.object_id = :object_id$uniq",
            'params' => [
                ":object_id$uniq" => $object_id,
            ]
        ]);

        return $this;
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters': return [
                'item_group' => 'relation',
                'object' => 'relation',
                'quantity' => 'numeric',
                'value_per_unit' => 'numeric',
                'value' => 'numeric',
                'value_done' => 'numeric',
                'quantity_done' => 'numeric'
            ];
            case 'big_numbers': return ['quantity', 'value_per_unit', 'value', 'value_done', 'quantity_done'];
            default: return parent::modelSettings($column);
        }
    }
    
    public function getPercentageDone()
    {
        if (!$this->value->isZero())
        {
            return $this->value_done->divide($this->value)->multiple(SIMABigNumber::fromInteger(100), 2);
        }
        
        return SIMABigNumber::fromFloat(0.00, 2);
    }
    
    public static function get($item_group, $object)
    {
        return BillOfQuantityItemGroupToObject::model()->findByAttributes([
            'item_group_id' => $item_group->id,
            'object_id' => $object->id
        ]);
    }
    
    public function getApproximatelyValuePerUnit()
    {
        if (!$this->value->isZero() && !$this->quantity->isZero())
        {
            return $this->value->divide($this->quantity, 10);
        }
        
        return SIMABigNumber::fromFloat(0.00, 10);
    }
}

