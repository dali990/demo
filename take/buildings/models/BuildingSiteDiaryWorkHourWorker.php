<?php

class BuildingSiteDiaryWorkHourWorker extends SIMAActiveRecord
{
    public static $WORKER = 'WORKER';
    public static $CRAFTSMAN = 'CRAFTSMAN';
    public static $TEHNICAL_STAFF = 'TEHNICAL_STAFF';
    public static $OTHERS = 'OTHERS';
    public static $worker_types = [];
    
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function moduleName()
    {
        return 'buildings';
    }

    public function tableName()
    {
        return 'buildings.site_diary_work_hour_workers';
    }
    
    public function rules()
    {
        return [
            ['site_diary_work_hour_id, worker_type', 'required'],
            ['worker_id, worker_count', 'safe'],
            [
                'site_diary_work_hour_id, worker_id', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => ['insert','update']
            ],
            ['worker_count', 'checkWorkerCount']
        ];
    }
    
    public function beforeValidate() 
    {
        if (!empty($this->worker_id))
        {
            $this->worker_count = 1;
        }
        
        return parent::beforeValidate();
    }
    
    public function checkWorkerCount()
    {
        if (!$this->hasErrors() && ($this->isNewRecord || $this->columnChanged('worker_count') || $this->columnChanged('worker_id')))
        {
            if (!empty($this->worker_id) && intval($this->worker_count) !== 1)
            {
                $this->addError('worker_count', Yii::t('BuildingsModule.BuildingSiteDiaryWorkHourWorker', 'WorkerCountMustBeOne'));
            }
            if (empty($this->worker_id) && intval($this->worker_count) < 1)
            {
                $this->addError('worker_count', Yii::t('BuildingsModule.BuildingSiteDiaryWorkHourWorker', 'WorkerCountMustBeEqualOrGreaterThanOne'));
            }
        }
    }
    
    public function relations($child_relations = [])
    {
        return [
            'site_diary_work_hour' => [self::BELONGS_TO, BuildingSiteDiaryWorkHour::class, 'site_diary_work_hour_id'],
            'worker' => [self::BELONGS_TO, Person::class, 'worker_id']
        ];
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters': return [
                'site_diary_work_hour' => 'relation',
                'worker_type' => 'dropdown',
                'worker' => 'relation',
                'worker_count' => 'integer'
            ];
            default: return parent::modelSettings($column);
        }
    }
    
    protected function modelOptions(\User $user = null):array
    {
        return ['form', 'delete'];
    }
}

BuildingSiteDiaryWorkHourWorker::$worker_types = [
    'WORKER' => Yii::t('BuildingsModule.BuildingSiteDiaryWorkHourWorker', 'Worker'),
    'CRAFTSMAN' => Yii::t('BuildingsModule.BuildingSiteDiaryWorkHourWorker', 'Craftsman'),
    'TEHNICAL_STAFF' => Yii::t('BuildingsModule.BuildingSiteDiaryWorkHourWorker', 'TehnicalStaff'),
    'OTHERS' => Yii::t('BuildingsModule.BuildingSiteDiaryWorkHourWorker', 'Others')
];