<?php

class BillOfQuantityObject extends SIMAActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    
    public function tableName()
    {
        return 'buildings.bill_of_quantities_objects';
    }
    
    public function moduleName()
    {
        return 'buildings';
    }
    
    public function __get($column) 
    {
        switch ($column) {
            case 'DisplayName': return $this->name;
            case 'SearchName': return $this->DisplayName;

            default: return parent::__get($column);
        }
    }
    
    public function rules()
    {
        return [
            ['name, bill_of_quantity_id', 'required'],
            ['bill_of_quantity_id', 'safe', 'on' => 'from_bill_of_quantity'],
            ['description, parent_id', 'safe'],
            [
                'bill_of_quantity_id, parent_id', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => ['insert','update']
            ],
            ['name', 'unique_with', 'with' => ['bill_of_quantity_id','parent_id']],
            ['parent_id', 'checkParentCycle']
        ];
    }
    
    public function relations($child_relations = [])
    {
        return [
            'bill_of_quantity' => [self::BELONGS_TO, BillOfQuantity::class, 'bill_of_quantity_id'],
            'parent' => [self::BELONGS_TO, BillOfQuantityObject::class, 'parent_id'],
            'children' => [self::HAS_MANY, BillOfQuantityObject::class, 'parent_id', 'scopes' => 'byName'],
            'children_count' => [self::STAT, BillOfQuantityObject::class, 'parent_id', 'select' => 'count(*)'],
            'object_to_items' => [self::HAS_MANY, BillOfQuantityItemToObject::class, 'object_id'],
            'object_to_item_groups' => [self::HAS_MANY, BillOfQuantityItemGroupToObject::class, 'object_id']
        ];
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters': return [
                'name' => 'text',
                'bill_of_quantity' => 'relation',
                'parent' => 'relation',
                'description' => 'text'
            ];
            case 'textSearch': return [
                'name' => 'text'
            ];
            default: return parent::modelSettings($column);
        }
    }
    
    public function scopes()
    {
        $alias = $this->getTableAlias();
        $bill_of_quantity_object_tbl_name = BillOfQuantityObject::model()->tableName();
        $uniq = SIMAHtml::uniqid();

        return [
            'onlyLeaf' => [
                'condition' => "not exists (select 1 from $bill_of_quantity_object_tbl_name object$uniq where object$uniq.parent_id=$alias.id)"
            ],
            'byName' => [
                'order' => "$alias.name ASC"
            ],
            'withoutRoot' => [
                'condition' => "$alias.parent_id is not null"
            ],
            'onlyRoot' => [
                'condition' => "$alias.parent_id is null"
            ]
        ];
    }
    
    public function beforeSave()
    {
        if ($this->scenario !== 'root_inserting' && empty($this->parent))
        {
            $root_object = self::addRootObjectForBillOfQuantity($this->bill_of_quantity);
            if ($this->isNewRecord || $this->id !== $root_object->id)
            {
                $this->parent_id = $root_object->id;
            }
        }
        
        return parent::beforeSave();
    }
    
    public function afterSave() 
    {
        parent::afterSave();
        
        if ($this->isNewRecord)
        {
            $bill_of_quantity_items = BillOfQuantityItem::model()->findAll([
                'model_filter' => [
                    'group' => [
                        'bill_of_quantity' => [
                            'ids' => $this->bill_of_quantity_id
                        ]
                    ]
                ]
            ]);
            foreach ($bill_of_quantity_items as $bill_of_quantity_item)
            {
                $bill_of_quantity_item_to_object = BillOfQuantityItemToObject::model()->findByAttributes([
                    'item_id' => $bill_of_quantity_item->id,
                    'object_id' => $this->id
                ]);
                if (empty($bill_of_quantity_item_to_object))
                {
                    $bill_of_quantity_item_to_object = new BillOfQuantityItemToObject();
                    $bill_of_quantity_item_to_object->item_id = $bill_of_quantity_item->id;
                    $bill_of_quantity_item_to_object->object_id = $this->id;
                    $bill_of_quantity_item_to_object->save();
                }
            }
            
            $bill_of_quantity_item_groups = BillOfQuantityItemGroup::model()->findAll([
                'model_filter' => [
                    'bill_of_quantity' => [
                        'ids' => $this->bill_of_quantity_id
                    ]
                ]
            ]);
            foreach ($bill_of_quantity_item_groups as $bill_of_quantity_item_group)
            {
                $bill_of_quantity_item_group_to_object = BillOfQuantityItemGroupToObject::model()->findByAttributes([
                    'item_group_id' => $bill_of_quantity_item_group->id,
                    'object_id' => $this->id
                ]);
                if (empty($bill_of_quantity_item_group_to_object))
                {
                    $bill_of_quantity_item_group_to_object = new BillOfQuantityItemGroupToObject();
                    $bill_of_quantity_item_group_to_object->item_group_id = $bill_of_quantity_item_group->id;
                    $bill_of_quantity_item_group_to_object->object_id = $this->id;
                    $bill_of_quantity_item_group_to_object->save();
                }
            }
        }
        
        if ($this->columnChanged('parent_id'))
        {
            $old_parent = BillOfQuantityObject::model()->findByPk($this->__old['parent_id']);
            if (!empty($old_parent))
            {
                $old_parent->calcValueRecursive();
            }
            if (!empty($this->parent_id))
            {
                $this->parent->calcValueRecursive();
            }
        }
    }
    
    public function beforeDelete()
    {
        foreach ($this->children as $children)
        {
            $children->delete();
        }
        
        foreach ($this->object_to_items as $object_to_item)
        {
            $object_to_item->delete();
        }
        
        foreach ($this->object_to_item_groups as $object_to_item_group)
        {
            $object_to_item_group->delete();
        }
        
        return parent::beforeDelete();
    }
    
    public function afterDelete() 
    {
        parent::afterDelete();

        if (!empty($this->parent))
        {
            $this->parent->calcValueRecursive();
        }
    }
    
    public function calcValueRecursive()
    {
        if ($this->children_count > 0)
        {
            foreach ($this->object_to_items as $object_to_item)
            {
                $value = SIMABigNumber::fromFloat(0.00, 2);
                $value_done = SIMABigNumber::fromFloat(0.00, 2);
                $quantity_done = SIMABigNumber::fromFloat(0.00, 2);
                foreach ($this->children as $child)
                {
                    $child_object_to_item = BillOfQuantityItemToObject::get($object_to_item->item, $child);
                    if (!empty($child_object_to_item))
                    {
                        $value = $value->plus($child_object_to_item->value);
                        $value_done = $value_done->plus($child_object_to_item->value_done);
                        $quantity_done = $quantity_done->plus($child_object_to_item->quantity_done);
                    }
                }
                $object_to_item->value = $value;
                $object_to_item->value_done = $value_done;
                $object_to_item->quantity_done = $quantity_done;
                $object_to_item->save();
            }
            foreach ($this->object_to_item_groups as $object_to_group)
            {
                $value = SIMABigNumber::fromFloat(0.00, 2);
                $value_done = SIMABigNumber::fromFloat(0.00, 2);
                $quantity_done = SIMABigNumber::fromFloat(0.00, 2);
                foreach ($this->children as $child)
                {
                    $child_object_to_group = BillOfQuantityItemGroupToObject::get($object_to_group->item_group, $child);
                    if (!empty($child_object_to_group))
                    {
                        $value = $value->plus($child_object_to_group->value);
                        $value_done = $value_done->plus($child_object_to_group->value_done);
                        $quantity_done = $quantity_done->plus($child_object_to_group->quantity_done);
                    }
                }
                $object_to_group->value = $value;
                $object_to_group->value_done = $value_done;
                $object_to_group->quantity_done = $quantity_done;
                $object_to_group->save();
            }
        }
        else
        {
            foreach ($this->object_to_item_groups as $object_to_group)
            {
                $object_to_group->item_group->calcValueRecursiveForObject($this);
            }
        }
        
        if (isset($this->parent))
        {
            $this->parent->calcValueRecursive();
        }
    }
    
    public function calcValueDoneRecursiveForInterimBill($building_interim_bill)
    {
        if ($this->children_count > 0)
        {
            foreach ($this->object_to_items as $object_to_item)
            {
                $building_measurement_sheet = BuildingMeasurementSheet::get($building_interim_bill, $object_to_item->item, $this);
                $value_done = SIMABigNumber::fromFloat(0.00, 2);
                $quantity_done = SIMABigNumber::fromFloat(0.00, 2);
                $pre_value_done = SIMABigNumber::fromFloat(0.00, 2);
                $pre_quantity_done = SIMABigNumber::fromFloat(0.00, 2);
                $sheets_written = true;
                $sheets_confirmed = true;
                foreach ($this->children as $child)
                {
                    $child_building_measurement_sheet = BuildingMeasurementSheet::model()->findByAttributes([
                        'building_interim_bill_id' => $building_interim_bill->id,
                        'bill_of_quantity_item_id' => $object_to_item->item->id,
                        'object_id' => $child->id
                    ]);
                    if (!empty($child_building_measurement_sheet))
                    {
                        $value_done = $value_done->plus($child_building_measurement_sheet->value_done);
                        $quantity_done = $quantity_done->plus($child_building_measurement_sheet->quantity_done);
                        $pre_value_done = $pre_value_done->plus($child_building_measurement_sheet->pre_value_done);
                        $pre_quantity_done = $pre_quantity_done->plus($child_building_measurement_sheet->pre_quantity_done);
                        if ($sheets_written === true && $child_building_measurement_sheet->written === false)
                        {
                            $sheets_written = false;
                        }
                        if ($sheets_confirmed === true && $child_building_measurement_sheet->confirmed === false)
                        {
                            $sheets_confirmed = false;
                        }
                    }
                }
                $building_measurement_sheet->value_done = $value_done;
                $building_measurement_sheet->quantity_done = $quantity_done;
                $building_measurement_sheet->pre_value_done = $pre_value_done;
                $building_measurement_sheet->pre_quantity_done = $pre_quantity_done;
                $building_measurement_sheet->written = $sheets_written;
                $building_measurement_sheet->confirmed = $sheets_confirmed;
                $building_measurement_sheet->save();
            }
            foreach ($this->object_to_item_groups as $object_to_group)
            {
                $building_measurement_sheet_group = BuildingMeasurementSheetGroup::get($building_interim_bill, $object_to_group->item_group, $this);
                $value_done = SIMABigNumber::fromFloat(0.00, 2);
                $quantity_done = SIMABigNumber::fromFloat(0.00, 2);
                $pre_value_done = SIMABigNumber::fromFloat(0.00, 2);
                $pre_quantity_done = SIMABigNumber::fromFloat(0.00, 2);
                $sheets_written = true;
                $sheets_confirmed = true;
                foreach ($this->children as $child)
                {
                    $child_building_measurement_sheet_group = BuildingMeasurementSheetGroup::get($building_interim_bill, $object_to_group->item_group, $child);
                    $value_done = $value_done->plus($child_building_measurement_sheet_group->value_done);
                    $quantity_done = $quantity_done->plus($child_building_measurement_sheet_group->quantity_done);
                    $pre_value_done = $pre_value_done->plus($child_building_measurement_sheet_group->pre_value_done);
                    $pre_quantity_done = $pre_quantity_done->plus($child_building_measurement_sheet_group->pre_quantity_done);
                    if ($sheets_written === true && $child_building_measurement_sheet_group->measurement_sheets_written === false)
                    {
                        $sheets_written = false;
                    }
                    if ($sheets_confirmed === true && $child_building_measurement_sheet_group->measurement_sheets_confirmed === false)
                    {
                        $sheets_confirmed = false;
                    }
                }
                $building_measurement_sheet_group->value_done = $value_done;
                $building_measurement_sheet_group->quantity_done = $quantity_done;
                $building_measurement_sheet_group->pre_value_done = $pre_value_done;
                $building_measurement_sheet_group->pre_quantity_done = $pre_quantity_done;
                $building_measurement_sheet_group->measurement_sheets_written = $sheets_written;
                $building_measurement_sheet_group->measurement_sheets_confirmed = $sheets_confirmed;
                $building_measurement_sheet_group->save();
            }
        }

        if (isset($this->parent))
        {
            $this->parent->calcValueDoneRecursiveForInterimBill($building_interim_bill);
        }
    }
    
    public function afterValidateDependencyError(array $foreign_dependencies)
    {        
        $allowed_tables = [
            BillOfQuantityObject::tableName().':parent_id',
            BillOfQuantityItemToObject::tableName().':object_id',
            BillOfQuantityItemGroupToObject::tableName().':object_id'
        ];
        
        $parsed_foreign_dependencies = array_diff($foreign_dependencies, $allowed_tables);
        
        parent::afterValidateDependencyError($parsed_foreign_dependencies);
    }
    
    public static function addRootObjectForBillOfQuantity($bill_of_quantity)
    {
        $root_object = BillOfQuantityObject::model()->find([
            'model_filter' => [
                'bill_of_quantity' => [
                    'ids' => $bill_of_quantity->id
                ],
                'scopes' => 'onlyRoot'
            ]
        ]);
        if (empty($root_object))
        {
            $root_object = new BillOfQuantityObject();
            $root_object->scenario = 'root_inserting';
            $root_object->name = $bill_of_quantity->name;
            $root_object->bill_of_quantity_id = $bill_of_quantity->id;
            $root_object->save();
        }
        
        return $root_object;
    }
    
    public function cloneObjectRecursive($new_bill_of_quantity, $parent_id, &$old_object_ids_mapper)
    {
        $new_object = new BillOfQuantityObject();
        $new_object->setAttributes($this->getAttributes());
        $new_object->bill_of_quantity_id = $new_bill_of_quantity->id;
        $new_object->parent_id = $parent_id;
        if (empty($parent_id))
        {
            $new_object->name = $new_bill_of_quantity->name;
            $new_object->scenario = 'root_inserting';
        }
        $new_object->save();

        $old_object_ids_mapper[$this->id] = $new_object->id;
        
        foreach ($this->children as $children_object)
        {
            $children_object->cloneObjectRecursive($new_bill_of_quantity, $new_object->id, $old_object_ids_mapper);
        }
    }
}

