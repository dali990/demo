<?php

class BuildingProjectType extends SIMAActiveRecord {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'buildings.project_types';
    }

    public function moduleName() {
        return 'buildings';
    }

    public function __get($column) {
        switch ($column) {
            case 'DisplayName': return $this->short_name.' - '.$this->name;
            case 'SearchName': return $this->DisplayName;
            default: return parent::__get($column);
        }
    }

    public function rules() {
        return array(
            array('name, short_name', 'required', 'message' => 'Polje "{attribute}" mora biti uneto'),
            array('description', 'safe'),
        );
    }

    public function scopes() {
        return array(
            'byName' => array(
                'order' => 'name, id',
            ),
        );
    }

    public function relations($child_relations = [])
    {
        return array(
            'building_projects' => array(self::HAS_MANY, 'BuildingProject', 'building_project_type_id'),
        );
    }
    
    public function modelSettings($column)
    {
        switch ($column) {
            case 'filters': return [
                'name' => 'text',
                'description' => 'text',
            ];
            case 'textSearch': return [
                'name' => 'text',
                'description' => 'text',
            ];
            default: return parent::modelSettings($column);
        }
    }

}