<?php

class BillOfQuantity extends SIMAActiveRecord
{
    public $user_ids = [];
    public $old_users = []; //niz u kome se pamte clanovi tima(ukljucujuci i vlasnika) predmera pre nego sto se sacuva/obrise
    public $form_attribute_has_objects = null;
    
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    
    public function tableName()
    {
        return 'buildings.bills_of_quantities';
    }
    
    public function moduleName()
    {
        return 'buildings';
    }
    
    public function __get($column) 
    {
        switch ($column) {
            case 'DisplayName': return $this->name;
            case 'SearchName': return "{$this->name}({$this->theme->DisplayName})";

            default: return parent::__get($column);
        }
    }
    
    public function rules()
    {
        return [
            ['name, theme_id, owner_id', 'required'],
            ['has_object_value_per_unit, is_private, comment', 'safe'],
            ['locked, locked_by_user_id, locked_timestamp', 'safe'],
            [
                'theme_id', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => ['insert','update']
            ],
            ['name', 'unique_with', 'with' => ['theme_id']],
            ['user_ids', 'checkUsers'],
            ['object_ids', 'checkObjects']
        ];
    }
    
    public function checkObjects()
    {
        if (!$this->hasErrors())
        {
            if (is_string($this->objects_without_root))
            {
                $objects_without_root = CJSON::decode($this->objects_without_root);
                $objects_without_root_count = count($objects_without_root['ids']);
            }
            else
            {
                $objects_without_root_count = count($this->objects_without_root);
            }
            
            if ($objects_without_root_count === 1)
            {
                $this->addError('object_ids', Yii::t('BuildingsModule.BillOfQuantity', 'BillOfQuantityCanNotHaveOnlyOneObject'));
            }
        }
    }

    public function checkUsers()
    {
        //ako je predmer privatan, mora da ima bar jednog korisnika predmera
        if (!$this->hasErrors() && $this->is_private)
        {
            if (is_string($this->users))
            {
                $users = CJSON::decode($this->users);
                if (empty($users['ids']))
                {
                    $this->addError('user_ids', Yii::t('BuildingsModule.BillOfQuantity', 'UsersCanNotBeEmptyForPrivateBillOfQuantity'));
                }
            }
            else if (empty($this->users))
            {
                $this->addError('user_ids', Yii::t('BuildingsModule.BillOfQuantity', 'UsersCanNotBeEmptyForPrivateBillOfQuantity'));
            }
        }
    }
    
    public function relations($child_relations = [])
    {
        $bill_of_quantities_to_users_tbl_name = BillOfQuantityToUser::model()->tableName();
        
        return [
            'theme' => [self::BELONGS_TO, Theme::class, 'theme_id'],
            'groups' => [self::HAS_MANY, BillOfQuantityItemGroup::class, 'bill_of_quantity_id', 'scopes' => 'byOrder'],
            'root_group' => [self::HAS_ONE, BillOfQuantityItemGroup::class, 'bill_of_quantity_id', 'condition' => 'parent_id is null'],
            'leaf_groups' => [self::HAS_MANY, BillOfQuantityItemGroup::class, 'bill_of_quantity_id', 'scopes' => ['onlyLeaf', 'byOrder']],
            'first_level_groups' => [self::HAS_MANY, BillOfQuantityItemGroup::class, 'bill_of_quantity_id', 'model_filter' => [
                'parent' => [
                    'parent' => 'null'
                ]
            ], 'scopes' => 'byOrder'],
            'first_level_groups_count' => [self::STAT, BillOfQuantityItemGroup::class, 'bill_of_quantity_id', 'model_filter' => [
                'parent' => [
                    'parent' => 'null'
                ]
            ], 'select' => 'count(*)'],
            'objects' => [self::HAS_MANY, BillOfQuantityObject::class, 'bill_of_quantity_id', 'scopes' => 'byName'],
            'root_object' => [self::HAS_ONE, BillOfQuantityObject::class, 'bill_of_quantity_id', 'condition' => 'parent_id is null'],
            'leaf_objects' => [self::HAS_MANY, BillOfQuantityObject::class, 'bill_of_quantity_id', 'scopes' => ['onlyLeaf', 'byName']],
            'leaf_objects_count' => [self::STAT, BillOfQuantityObject::class, 'bill_of_quantity_id', 'select' => 'count(*)', 'scopes' => ['onlyLeaf']],
            'objects_without_root' => [self::HAS_MANY, BillOfQuantityObject::class, 'bill_of_quantity_id', 'condition' => 'parent_id is not null', 'scopes' => 'byName'],
            'objects_count' => [self::STAT, BillOfQuantityObject::class, 'bill_of_quantity_id', 'select' => 'count(*)'],
            'first_level_objects' => [self::HAS_MANY, BillOfQuantityObject::class, 'bill_of_quantity_id', 'model_filter' => [
                'parent' => [
                    'parent' => 'null'
                ]
            ], 'scopes' => 'byName'],
            'first_level_objects_count' => [self::STAT, BillOfQuantityObject::class, 'bill_of_quantity_id', 'model_filter' => [
                'parent' => [
                    'parent' => 'null'
                ]
            ], 'select' => 'count(*)'],
            'bill_of_quantity_to_users' => [self::HAS_MANY, BillOfQuantityToUser::class, 'bill_of_quantity_id'],
            'users' => [self::MANY_MANY, User::class, "$bill_of_quantities_to_users_tbl_name(bill_of_quantity_id, user_id)"],
            'owner' => [self::BELONGS_TO, User::class, 'owner_id'],
            'locked_by_user' => [self::BELONGS_TO, User::class, 'locked_by_user_id'],
            'building_construction' => [self::HAS_ONE, BuildingConstruction::class, 'bill_of_quantity_id'],
        ];
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters': return [
                'name' => 'text',
                'theme' => 'relation',
                'has_object_value_per_unit' => 'boolean',
                'bill_of_quantity_to_users' => 'relation',
                'is_private' => 'boolean',
                'comment' => 'text',
                'owner' => 'relation',
                'locked_by_user' => 'relation',
                'building_construction' => 'relation'
            ];
            case 'textSearch': return [
                'name' => 'text',
                'theme' => 'relation'
            ];
            case 'update_relations': return [
                'theme'
            ];
            case 'form_list_relations': return [
                'objects_without_root', 'users'
            ];
            case 'statuses': return [
                'locked' => [
                    'title' => Yii::t('BuildingsModule.BillOfQuantity', 'Locked'),
                    'timestamp' => 'locked_timestamp',
                    'user' => ['locked_by_user_id', 'relName' => 'locked_by_user'],
                    'checkAccessConfirm' => 'checkCanLock',
                    'checkAccessRevert' => 'checkCanLock'
                ]
            ];
            default: return parent::modelSettings($column);
        }
    }
    
    public function checkCanLock()
    {
        $result =  Yii::app()->user->checkAccess('modelThemeAdmin') || 
                   Yii::app()->user->id === $this->owner_id || 
                   Yii::app()->user->id === $this->theme->coordinator_id;
        
        return $result ? true : [Yii::t('BuildingsModule.BillOfQuantity', 'YouDoNotHavePrivilege')];
    }
    
    protected function modelOptions(\User $user = null):array
    {
        $options = [];

        if (!empty(BillOfQuantity::model()->hasSuperChangeAccess($user->id)->findByPk($this->id)))
        {
            $options[] = 'form';
            if (empty($this->building_construction))
            {
                $options[] = 'delete';
            }
        }

        return $options;
    }
    
    public function scopes()
    {
        $alias = $this->getTableAlias();
        $uniq = SIMAHtml::uniqid();
        $building_construction_table_name = BuildingConstruction::model()->tableName();

        return [
            'byName' => [
                'order' => "$alias.name ASC"
            ],
            'withoutBuildingConstruction' => [
                'condition' => "not exists (select 1 from $building_construction_table_name bc$uniq where bc$uniq.bill_of_quantity_id = $alias.id)"
            ]
        ];
    }

    /*
     * Predmer ima pravo uvek da vidi ko ima privilegiju modelThemeSeeAll ili
     * ako je privatan predmer onda vlasnik predmera, koordinator teme ili tim predmera, a ako nije privatan sve isto kao privatan plus clanovi tima tema
     */
    public function hasAccess($user_id)
    {
        if (!Yii::app()->user->checkAccess('modelThemeSeeAll'))
        {
            $this->getDbCriteria()->mergeWith(new SIMADbCriteria([
                'Model' => BillOfQuantity::class,
                'model_filter' => [
                    'OR',
                    [
                        'AND',
                        [
                            'is_private' => false
                        ],
                        [
                            'theme' => [
                                'filter_scopes' => [
                                    'hasAccess' => $user_id
                                ]
                            ]
                        ]

                    ],
                    [
                        'owner' => [
                            'ids' => $user_id
                        ]
                    ],
                    [
                        'theme' => [
                            'filter_scopes' => [
                                'hasSuperAccess' => $user_id
                            ]
                        ]
                    ],
                    [
                        'bill_of_quantity_to_users' => [
                            'user' => [
                                'ids' => $user_id
                            ]
                        ]
                    ]
                ]
            ]));
        }

        return $this;
    }
    
    /*
     * Predmer ima pravo da menja(menjanje kolicine, jedinicne cene...) ko ima privilegiju da menja temu ili ko ima privilegiju modelThemeAdmin
     * ili je u timu predmera ili je vlasnik predmera
     */
    public function hasChangeAccess($user_id)
    {
        if (!Yii::app()->user->checkAccess('modelThemeAdmin'))
        {
            $this->getDbCriteria()->mergeWith(new SIMADbCriteria([
                'Model' => BillOfQuantity::class,
                'model_filter' => [
                    'OR',
                    [
                        'owner' => [
                            'ids' => $user_id
                        ]
                    ],
                    [
                        'theme' => [
                            'filter_scopes' => [
                                'hasChangeAccess' => $user_id
                            ]
                        ]
                    ],
                    [
                        'bill_of_quantity_to_users' => [
                            'user' => [
                                'ids' => $user_id
                            ]
                        ]
                    ]
                ]
            ]));
        }

        return $this;
    }
    
    /*
     * Predmer ima pravo da menja(edit/delete) ko ima privilegiju da menja temu ili je vlasnik predmera ili ko ima privilegiju modelThemeAdmin
     */
    public function hasSuperChangeAccess($user_id)
    {
        if (!Yii::app()->user->checkAccess('modelThemeAdmin'))
        {
            $this->getDbCriteria()->mergeWith(new SIMADbCriteria([
                'Model' => BillOfQuantity::class,
                'model_filter' => [
                    'OR',
                    [
                        'owner' => [
                            'ids' => $user_id
                        ]
                    ],
                    [
                        'theme' => [
                            'filter_scopes' => [
                                'hasChangeAccess' => $user_id
                            ]
                        ]
                    ]
                ]
            ]));
        }

        return $this;
    }
    
    public function beforeSave()
    {
        if (!$this->isNewRecord)
        {
            //ne mozemo da koristimo $this->users relaciju jer je moguce da dodje iz forme kao string
            $this->old_users = User::model()->findAll([
                'model_filter' => [
                    'user_to_bill_of_quantities' => [
                        'bill_of_quantity' => [
                            'ids' => $this->id
                        ]
                    ]
                ]
            ]);
            if (!empty($this->__old['owner_id']))
            {
                $old_owner = User::model()->findByPkWithCheck($this->__old['owner_id']);
                array_push($this->old_users, $old_owner);
            }
        }
        
        return parent::beforeSave();
    }
    
    public function afterSave() 
    {
        parent::afterSave();
        
        if ($this->isNewRecord)
        {
            if ($this->scenario !== 'clone_inserting')
            {
                BillOfQuantityObject::addRootObjectForBillOfQuantity($this);
                BillOfQuantityItemGroup::addRootGroupForBillOfQuantity($this);
            }
        }
        
        if ($this->columnChanged('has_object_value_per_unit'))
        {
            foreach ($this->leaf_objects as $leaf_object)
            {
                foreach ($leaf_object->object_to_items as $object_to_item)
                {
                    if ($this->has_object_value_per_unit)
                    {
                        $object_to_item->value_per_unit = $object_to_item->item->value_per_unit;
                        $object_to_item->save();
                    }
                    else
                    {
                        $object_to_item->item->value_per_unit = $object_to_item->value_per_unit;
                        $object_to_item->item->save();
                        break;
                    }
                }
            }
        }
        
        if ($this->columnChanged('name'))
        {
            $root_object = BillOfQuantityObject::model()->find([
                'model_filter' => [
                    'bill_of_quantity' => [
                        'ids' => $this->id
                    ],
                    'scopes' => 'onlyRoot'
                ]
            ]);
            if (!empty($root_object))
            {
                $root_object->name = $this->name;
                $root_object->save();
            }
        }
        
        //dodaju se korisnici predmera kao clanovi tima teme
        $new_user_ids = [];
        $bill_of_quantity_to_users = BillOfQuantityToUser::model()->findAllByAttributes([
            'bill_of_quantity_id' => $this->id
        ]);
        foreach ($bill_of_quantity_to_users as $bill_of_quantity_to_user)
        {
            PersonToTheme::add($this->theme, $bill_of_quantity_to_user->user->person);
            array_push($new_user_ids, $bill_of_quantity_to_user->user->id);
        }
        //dodaje se vlasnik predmera kao clan tima teme
        if (!empty($this->owner_id))
        {
            PersonToTheme::add($this->theme, $this->owner->person);
            array_push($new_user_ids, $this->owner->id);
        }
        //ako je izbrisan neki clan predmera javlja se obavestenje trenutno ulogovanom korisniku da ce taj clan da ostane kao clan tima teme 
        //bez obzira sto vise nije clan predmera
        $this->sendNotifAboutRemovedUsersWhichStillInThemeTeam($this->old_users, $new_user_ids);
    }
    
    public function beforeDelete()
    {
        $this->old_users = $this->users;
        if (!empty($this->owner))
        {
            array_push($this->old_users, $this->owner);
        }
        
        if (!empty($this->root_object))
        {
            $this->root_object->delete();
        }
        
        if (!empty($this->root_group))
        {
            $this->root_group->delete();
        }
        
        foreach ($this->bill_of_quantity_to_users as $bill_of_quantity_to_user)
        {
            $bill_of_quantity_to_user->delete();
        }
        
        return parent::beforeDelete();
    }
    
    public function afterDelete()
    {
        parent::afterDelete();

        $this->sendNotifAboutRemovedUsersWhichStillInThemeTeam($this->old_users, []);
    }
    
    private function sendNotifAboutRemovedUsersWhichStillInThemeTeam($old_users, $new_user_ids)
    {
        $removed_users = [];
        foreach ($old_users as $old_user)
        {
            if (!in_array($old_user->id, $new_user_ids))
            {
                array_push($removed_users, $old_user->DisplayName);
            }
        }
        if (isset(Yii::app()->controller) && !empty($removed_users))
        {
            $removed_users_string = implode(', ', $removed_users);
            Yii::app()->controller->raiseNote(Yii::t('BuildingsModule.BillOfQuantity', 'UsersRemovedFromBillOfQuantityButStillAreInTheme', [
                '{bill_of_quantity}' => $this->DisplayName,
                '{removed_users_string}' => $removed_users_string
            ]));
        }
    }
    
    public function afterValidateDependencyError(array $foreign_dependencies)
    {        
        $allowed_tables = [
            BillOfQuantityItemGroup::tableName().':bill_of_quantity_id',
            BillOfQuantityObject::tableName().':bill_of_quantity_id',
            BillOfQuantityToUser::tableName().':bill_of_quantity_id'
        ];
        
        $parsed_foreign_dependencies = array_diff($foreign_dependencies, $allowed_tables);
        
        parent::afterValidateDependencyError($parsed_foreign_dependencies);
    }
    
    public function getRootGroupToObject($object_id)
    {
        $root_group = BillOfQuantityItemGroup::getRootGroupForBillOfQuantity($this);
        
        if (!empty($root_group))
        {
            return BillOfQuantityItemGroupToObject::model()->findByAttributes([
                'item_group_id' => $root_group->id,
                'object_id' => $object_id
            ]);
        }
        
        return null;
    }
    
    public function cloneToBillOfQuantity($to_bill_of_quantity)
    {
        $old_object_ids_mapper = [];
        foreach ($this->root_object->children as $child)
        {
            $child->cloneObjectRecursive($to_bill_of_quantity, $to_bill_of_quantity->root_object->id, $old_object_ids_mapper);
        }
        foreach ($this->root_group->children as $child)
        {
            $child->cloneGroupRecursive($to_bill_of_quantity, $to_bill_of_quantity->root_group->id, $old_object_ids_mapper);
        }
    }
    
    public function allItemsCount()
    {
        $items_cnt = 0;

        foreach ($this->groups as $group)
        {
            $items_cnt += $group->items_count;
        }
        
        return $items_cnt;
    }
}

