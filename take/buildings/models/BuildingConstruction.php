<?php

class BuildingConstruction extends SIMAActiveRecord
{
    public $old_boss_assistants = []; //niz u kome se pamte pomocnici sefa(ukljucujuci i sefa) izvodjenja pre nego sto se sacuva/obrise

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function moduleName()
    {
        return 'buildings';
    }

    public function tableName()
    {
        return 'buildings.constructions';
    }

    public function __get($column) 
    {
        switch ($column) {
            case 'DisplayName': return !empty($this->theme) ? $this->theme->DisplayName : '';
            case 'SearchName': return $this->DisplayName;

            default: return parent::__get($column);
        }
    }
    
    public function rules()
    {
        return [
            ['constructor_id, investor_id, bill_of_quantity_id, responsible_engineer_id, start_date, construction_boss_id', 'required'],
            [
                'constructor_id, investor_id, bill_of_quantity_id, responsible_engineer_id', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => ['insert','update']
            ],
            ['bill_of_quantity_id', 'checkBillOfQuantityObject', 'on' => ['insert','update']],
            ['constructor_id', 'checkConstructor']
        ];
    }
    
    public function checkConstructor()
    {
        if (
                !$this->hasErrors() && 
                intval($this->constructor_id) !== intval(Yii::app()->company->id) && 
                (
                    $this->isNewRecord || $this->columnChanged('constructor_id')
                )
            )
        {
            $company_to_theme_cnt = CompanyToTheme::model()->count([
                'model_filter' => [
                    'company' => [
                        'ids' => $this->constructor_id
                    ],
                    'theme' => [
                        'ids' => $this->id
                    ]
                ]
            ]);
            if ($company_to_theme_cnt === 0)
            {
                $this->addError('constructor_id', Yii::t('BuildingsModule.BuildingConstruction', 'ConstructorMustBelongsToTheme', [
                    '{theme_name}' => $this->theme->DisplayName
                ]));
            }
        }
    }
    
    public function checkBillOfQuantityObject()
    {
        if (!$this->hasErrors())
        {
            if ($this->isNewRecord || $this->columnChanged('bill_of_quantity_id'))
            {
                $building_construction = BuildingConstruction::model()->countByAttributes([
                    'bill_of_quantity_id' => $this->bill_of_quantity_id
                ]);
                if ($building_construction > 0)
                {
                    $this->addError('bill_of_quantity_id', Yii::t('BuildingsModule.BuildingConstruction', 'BillOfQuantityAlreadyHasBuildingConstruction'));
                }
                
                if ($this->hasBuildingMeasurementSheets())
                {
                    $this->addError('bill_of_quantity_id', Yii::t('BuildingsModule.BuildingConstruction', 'BillOfQuantityCanNotChange'));
                }
            }
        }
    }

    public function relations($child_relations = [])
    {
        $constructions_to_boss_assistants_table_name = BuildingConstructionToBossAssistant::model()->tableName();

        return [
            'theme' => [self::HAS_ONE, Theme::class, 'id'],
            'constructor' => [self::BELONGS_TO, Company::class, 'constructor_id'],
            'constructor_model' => [self::BELONGS_TO, Company::class, 'constructor_id'],//dodato zbog kljucne reci 'constructor' kada se dohvata u vue
            'investor' => [self::BELONGS_TO, Partner::class, 'investor_id'],
            'bill_of_quantity' => [self::BELONGS_TO, BillOfQuantity::class, 'bill_of_quantity_id'],
            'responsible_engineer' => [self::BELONGS_TO, Person::class, 'responsible_engineer_id'],
            'building_interim_bills' => [self::HAS_MANY, BuildingInterimBill::class, 'building_construction_id', 'order' => 'issue_date DESC'],
            'building_interim_bills_count' => [self::STAT, BuildingInterimBill::class, 'building_construction_id', 'select' => 'count(*)'],
            'building_site_diaries' => [self::HAS_MANY, BuildingSiteDiary::class, 'building_construction_id'],
            'construction_boss' => [self::BELONGS_TO, User::class, 'construction_boss_id'],
            'construction_to_boss_assistants' => [self::HAS_MANY, BuildingConstructionToBossAssistant::class, 'building_construction_id'],
            'construction_boss_assistants' => [self::MANY_MANY, User::class, "$constructions_to_boss_assistants_table_name(building_construction_id, boss_assistant_id)"]
        ];
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters': return [
                'theme' => 'relation',
                'constructor' => 'relation',
                'investor' => 'relation',
                'bill_of_quantity' => 'relation',
                'responsible_engineer' => 'relation',
                'start_date' => 'date_range',
                'construction_boss' => 'relation',
                'construction_boss_assistants' => 'relation',
                'construction_to_boss_assistants' => 'relation'
            ];
            case 'textSearch': return [
                'theme' => 'relation'
            ];
            case 'mutual_forms': return ['base_relation' => 'theme'];
            case 'update_relations': return ['theme'];
            case 'form_list_relations': return [
                'construction_boss_assistants'
            ];
            default: return parent::modelSettings($column);
        }
    }
    
    protected function modelOptions(\User $user = null):array
    {
        $options = [];

        if (!empty(Theme::model()->hasChangeAccess($user->id)->findByPk($this->id)))
        {
            $options = ['form', 'delete'];
        }

        return $options;
    }
    
    public function beforeSave() 
    {
        if (!$this->isNewRecord)
        {
            //ne mozemo da koristimo $this->construction_boss_assistants relaciju jer je moguce da dodje iz forme kao string
            $this->old_boss_assistants = User::model()->findAll([
                'model_filter' => [
                    'boss_assistant_to_building_constructions' => [
                        'building_construction' => [
                            'ids' => $this->id
                        ]
                    ]
                ]
            ]);
            if (!empty($this->__old['construction_boss_id']))
            {
                $old_construction_boss = User::model()->findByPkWithCheck($this->__old['construction_boss_id']);
                array_push($this->old_boss_assistants, $old_construction_boss);
            }
        }
        
        if (!empty($this->theme->job->investor_id) && intval($this->investor_id) !== intval($this->theme->job->investor_id))
        {
            $this->investor_id = $this->theme->job->investor_id;
        }
        
        return parent::beforeSave();
    }
    
    public function afterSave()
    {
        parent::afterSave();
        
        //dodaju se pomocnici sefa kao clanovi tima teme
        $new_boss_assistant_ids = [];
        $building_construction_to_boss_assistants = BuildingConstructionToBossAssistant::model()->findAllByAttributes([
            'building_construction_id' => $this->id
        ]);
        foreach ($building_construction_to_boss_assistants as $building_construction_to_boss_assistant)
        {
            PersonToTheme::add($this->theme, $building_construction_to_boss_assistant->boss_assistant->person);
            array_push($new_boss_assistant_ids, $building_construction_to_boss_assistant->boss_assistant->id);
        }
        //dodaje se sef kao clan tima teme
        if (!empty($this->construction_boss_id))
        {
            PersonToTheme::add($this->theme, $this->construction_boss->person);
            array_push($new_boss_assistant_ids, $this->construction_boss->id);
        }
        //ako je izbrisan neki pomocnik sefa ili sef javlja se obavestenje trenutno ulogovanom korisniku da ce taj clan da ostane kao clan tima teme 
        //bez obzira sto vise nije clan izvodjenja
        $this->sendNotifAboutRemovedBossAssistantsWhichStillInThemeTeam($this->old_boss_assistants, $new_boss_assistant_ids);
    }
    
    public function beforeDelete()
    {
        $this->old_boss_assistants = $this->construction_boss_assistants;
        if (!empty($this->construction_boss))
        {
            array_push($this->old_boss_assistants, $this->construction_boss);
        }
        
        return parent::beforeDelete();
    }
    
    public function afterDelete() 
    {
        parent::afterDelete();
        
        $this->sendNotifAboutRemovedBossAssistantsWhichStillInThemeTeam($this->old_boss_assistants, []);
    }
    
    private function sendNotifAboutRemovedBossAssistantsWhichStillInThemeTeam($old_boss_assistants, $new_boss_assistant_ids)
    {
        $removed_boss_assistants = [];
        foreach ($old_boss_assistants as $old_boss_assistant)
        {
            if (!in_array($old_boss_assistant->id, $new_boss_assistant_ids))
            {
                array_push($removed_boss_assistants, $old_boss_assistant->DisplayName);
            }
        }
        if (isset(Yii::app()->controller) && !empty($removed_boss_assistants))
        {
            $removed_boss_assistants_string = implode(', ', $removed_boss_assistants);
            Yii::app()->controller->raiseNote(Yii::t('BuildingsModule.BuildingConstruction', 'BossAssistantsRemovedFromBuildingConstructionButStillAreInTheme', [
                '{building_construction}' => $this->DisplayName,
                '{removed_boss_assistants}' => $removed_boss_assistants_string
            ]));
        }
    }
    
    public function hasBuildingMeasurementSheets()
    {
        $has_sheets = false;
        foreach ($this->building_interim_bills as $building_interim_bill)
        {
            if ($building_interim_bill->building_measurement_sheets_count > 0)
            {
                $has_sheets = true;
                break;
            }
        }
        
        return $has_sheets;
    }
    
    public function getFinalInterimBill() 
    {
        return BuildingInterimBill::model()->find([
            'model_filter' => [
                'building_construction' => [
                    'ids' => $this->id
                ],
                'is_final' => true
            ]
        ]);
    }
    
    public function hasFinalInterimBill() 
    {
        return !empty($this->getFinalInterimBill());
    }
    
    public function getBossAssistantIds()
    {
        $ids = [];
        foreach ($this->construction_boss_assistants as $construction_boss_assistant)
        {
            array_push($ids, $construction_boss_assistant->id);
        }
        
        return $ids;
    }
}