<?php

class BillOfQuantityItemToObject extends SIMAActiveRecord
{
    public $calc_value = true;
    
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    
    public function tableName()
    {
        return 'buildings.bill_of_quantities_items_to_objects';
    }
    
    public function moduleName()
    {
        return 'buildings';
    }
    
    public function rules()
    {
        return [
            ['item_id, object_id, quantity, value_per_unit', 'required'],
            ['value, value_done, quantity_done', 'safe'],
            [
                'item_id, object_id', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => ['insert','update']
            ],
            ['quantity', 'checkNumericNumber', 'precision' => 20, 'scale' => 2],
            ['value_per_unit', 'checkNumericNumber', 'precision' => 20, 'scale' => 10],
            ['value', 'checkNumericNumber', 'precision' => 22, 'scale' => 2],
            ['item_id', 'unique_with', 'with' => ['object_id']],
        ];
    }
    
    public function beforeValidate() 
    {
        if (
                $this->isNewRecord || 
                $this->columnChanged('quantity') || 
                $this->columnChanged('value_per_unit')
           )
        {
            $this->value = $this->quantity->multiple($this->value_per_unit);
        }
        
        if (
                $this->isNewRecord || 
                $this->columnChanged('quantity_done') || 
                $this->columnChanged('value_per_unit')
            )
        {
            $this->value_done = $this->quantity_done->multiple($this->value_per_unit);
        }
        
        return parent::beforeValidate();
    }
    
    public function relations($child_relations = [])
    {
        return [
            'item' => [self::BELONGS_TO, BillOfQuantityItem::class, 'item_id'],
            'object' => [self::BELONGS_TO, BillOfQuantityObject::class, 'object_id'],
        ];
    }
    
    public function forObject($object_id)
    {
        $alias = $this->getTableAlias();
        $uniq = SIMAHtml::uniqid();
        
        $this->getDbCriteria()->mergeWith([
            'condition'=> "$alias.object_id = :object_id$uniq",
            'params' => [
                ":object_id$uniq" => $object_id,
            ]
        ]);

        return $this;
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters': return [
                'item' => 'relation',
                'object' => 'relation',
                'quantity' => 'numeric',
                'value_per_unit' => 'numeric',
                'value' => 'numeric',
                'value_done' => 'numeric',
                'quantity_done' => 'numeric'
            ];
            case 'big_numbers': return ['quantity', 'value_per_unit', 'value', 'value_done', 'quantity_done'];
            default: return parent::modelSettings($column);
        }
    }
    
    public function afterSave() 
    {
        parent::afterSave();
        
        if (
                $this->columnChanged('quantity') || 
                $this->columnChanged('value_per_unit') || 
                $this->columnChanged('value') || 
                $this->columnChanged('quantity_done') || 
                $this->columnChanged('value_done')
            )
        {
            if($this->calc_value && $this->object->children_count === 0)
            {
                $this->object->calcValueRecursive();
            }
        }
        
        if (
                $this->columnChanged('quantity') ||
                $this->columnChanged('value_per_unit')
            )
        {
            $building_measurement_sheets = BuildingMeasurementSheet::model()->findAllByAttributes([
                'bill_of_quantity_item_id' => $this->item_id,
                'object_id' => $this->object_id
            ]);
            foreach ($building_measurement_sheets as $building_measurement_sheet) 
            {
                $building_measurement_sheet->calcValueDone();
            }
        }
    }
    
    public function afterDelete() 
    {
        parent::afterDelete();
        if($this->calc_value && $this->object->children_count === 0)
        {
            $this->object->calcValueRecursive();
        }
    }
    
    public function getPercentageDone()
    {
        if (!$this->value->isZero())
        {
            return $this->value_done->divide($this->value)->multiple(SIMABigNumber::fromInteger(100), 2);
        }
        
        return SIMABigNumber::fromFloat(0.00, 2);
    }
    
    public static function get($item, $object)
    {
        return BillOfQuantityItemToObject::model()->findByAttributes([
            'item_id' => $item->id,
            'object_id' => $object->id
        ]);
    }
}

