<?php

class BuildingMeasurementSheetItem extends SIMAActiveRecord
{
    //flag koji se postavlja prilikom brisanja situacije i sluzi kada se brise iz BuildingMeasurementSheet da se ne radi preracunavanje uradjene vrednosti 
    //posle svake obrisane stavke jer se to radi na kraju brisanja svih stavki
    public $deleting_from_building_measurement_sheet = null;
    
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function moduleName()
    {
        return 'buildings';
    }

    public function tableName()
    {
        return 'buildings.measurement_sheet_items';
    }
    
    public function rules()
    {
        return [
            ['building_measurement_sheet_id', 'required'],
            ['calculation, amount, description', 'safe'],
            [
                'building_measurement_sheet_id', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => ['insert','update']
            ],
            ['calculation', 'checkCalculation']
        ];
    }
    
    public function beforeValidate() 
    {
        if (
                !empty($this->calculation) &&
                $this->amount->isZero() && 
                (
                    $this->isNewRecord || $this->columnChanged('calculation')
                )
           )
        {
            $parsed_calculation = $this->parseCalculation();
            if (!is_null($parsed_calculation))
            {
                $this->amount = $parsed_calculation;
            }
        }
        
        return parent::beforeValidate();
    }
    
    public function checkCalculation()
    {
        if (
                !$this->hasErrors() && 
                !empty($this->calculation) && 
                !$this->amount->isZero() && 
                (
                    $this->isNewRecord || 
                    $this->columnChanged('amount') || 
                    $this->columnChanged('calculation')
                )
            )
        {
            $parsed_calculation = $this->parseCalculation();
            if (!is_null($parsed_calculation))
            {
                $parsed_calculation_number = SIMABigNumber::fromString($parsed_calculation);
                if (!$parsed_calculation_number->equals($this->amount))
                {
                    $this->addError('calculation', Yii::t('BuildingsModule.BuildingMeasurementSheetItem', 'CalculationIsNotEqualToAmount', [
                        '{amount}' => $this->amount
                    ]));
                }
            }
        }
    }

    public function relations($child_relations = [])
    {
        return [
            'building_measurement_sheet' => [self::BELONGS_TO, BuildingMeasurementSheet::class, 'building_measurement_sheet_id']
        ];
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters': return [
                'building_measurement_sheet' => 'relation'
            ];
            case 'big_numbers': return ['amount'];
            case 'update_relations': return [
                'building_measurement_sheet'
            ];
            default: return parent::modelSettings($column);
        }
    }
    
    protected function modelOptions(\User $user = null):array
    {
        return ['form', 'delete'];
    }
    
    /**
     * Pomocna funkcija za parsiranje racunice koja se zove u validaciji.
     * @return number/null
     */
    private function parseCalculation()
    {
        $parsed_calculation = null;

        if (!preg_match("/[a-z]/i", $this->calculation))
        {
            try
            {
                eval('$parsed_calculation = ' . preg_replace('/[^0-9\+\-\*\/\(\)\.]/', '', $this->calculation) . ';');
            } 
            catch (Throwable $e)
            {
                $this->addError('calculation', Yii::t('BuildingsModule.BuildingMeasurementSheetItem', 'ParseCalculationError'));
            }
        }
        
        return $parsed_calculation;
    }
    
    public function afterSave() 
    {
        parent::afterSave();
        
        if ($this->isNewRecord || $this->columnChanged('amount'))
        {
            $this->calcValueDone();
        }
    }
    
    public function afterDelete() 
    {
        parent::afterDelete();

        if ($this->deleting_from_building_measurement_sheet !== true)
        {
            $this->calcValueDone();
        }
    }
    
    private function calcValueDone()
    {
        $item_to_object = BillOfQuantityItemToObject::model()->findByAttributes([
            'object_id' => $this->building_measurement_sheet->object_id,
            'item_id' => $this->building_measurement_sheet->bill_of_quantity_item_id
        ]);
        if (!empty($item_to_object))
        {
            $sheet_items = BuildingMeasurementSheetItem::model()->findAll([
                'model_filter' => [
                    'building_measurement_sheet' => [
                        'object' => [
                            'ids' => $this->building_measurement_sheet->object_id
                        ],
                        'bill_of_quantity_item' => [
                            'ids' => $this->building_measurement_sheet->bill_of_quantity_item_id
                        ]
                    ]
                ]
            ]);
            $quantity_done = SIMABigNumber::fromFloat(0.00, 2);
            foreach ($sheet_items as $sheet_item)
            {
                $quantity_done = $quantity_done->plus($sheet_item->amount);
            }
            $item_to_object->quantity_done = $quantity_done;
            $item_to_object->save();
        }
        
        $this->building_measurement_sheet->calcValueDone();
    }
}