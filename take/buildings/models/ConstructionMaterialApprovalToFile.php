<?php

class ConstructionMaterialApprovalToFile extends SIMAActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'buildings.construction_material_approvals_to_files';
    }

    public function moduleName()
    {
        return 'buildings';
    }

    public function __get($column)
    {
        switch ($column)
        {
            case 'DisplayName': return $this->construction_material_approval->DisplayName."-".$this->file->DisplayName;
            case 'SearchName': return $this->DisplayName;
            default: return parent::__get($column);
        }
    }

    public function rules()
    {
        return array(
            array('construction_material_approval_id, file_id', 'required'),
            array('construction_material_approval_id', 'checkUnique'),
            array('construction_material_approval_id, file_id', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => array('insert', 'update'))
        );
    }

    public function relations($child_relations = [])
    {
        return array(
            'construction_material_approval' => array(self::BELONGS_TO, 'ConstructionMaterialApproval', 'construction_material_approval_id'),
            'file' => array(self::BELONGS_TO, 'File', 'file_id')
        );
    }

    public function checkUnique()
    {
        if (
                intval($this->__old['construction_material_approval_id']) !== intval($this->construction_material_approval_id) || 
                intval($this->__old['file_id']) !== intval($this->file_id)
            )
        {
            $construction_material_approval_to_file = ConstructionMaterialApprovalToFile::model()->findByAttributes([
                'construction_material_approval_id'=>$this->construction_material_approval_id,
                'file_id'=>$this->file_id
            ]);
            if (!is_null($construction_material_approval_to_file))
            {
                $this->addError('construction_material_approval_id', Yii::t('BuildingsModule.ConstructionMaterialApprovalToFile', 'ConstructionMaterialApprovalToFileUniq',[
                    '{construction_material_approval_name}'=>$this->construction_material_approval->DisplayName,
                    '{file_name}'=>$this->file->DisplayName
                ]));
            }
        }
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters' : return array(
                'construction_material_approval' => 'relation',
                'file' => 'relation',
            );
            default: return parent::modelSettings($column);
        }
    }
}
