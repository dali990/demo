<?php

class ConstructionMaterialApproval extends SIMAActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'buildings.construction_material_approvals';
    }

    public function moduleName()
    {
        return 'buildings';
    }

    public function __get($column)
    {
        switch ($column)
        {
            case 'DisplayName': return $this->code;
            case 'SearchName': return $this->DisplayName;
            default: return parent::__get($column);
        }
    }

    public function scopes()
    {
        $alias = $this->getTableAlias();
        return array(
            'byName' => array(
                'order' => "$alias.code",
            ),
        );
    }

    public function rules()
    {
        return array(
            array('theme_id, used_in, part_of','required'),
            array('code, material_name, manufacturer_id, type, location, tehnical_charactetistics, '
                . 'approved, approved_submitter_id, approved_timestamp, warehouse_material_id', 'safe'),
            array('id', 'unique'),
            array('theme_id, manufacturer_id, part_of_id, approved_submitter_id, warehouse_material_id', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => array('insert', 'update'))
        );
    }

    public function relations($child_relations = [])
    {
        $construction_material_approvals_to_drawings = ConstructionMaterialApprovalToDrawing::model()->tableName();
        $construction_material_approvals_to_files = ConstructionMaterialApprovalToFile::model()->tableName();
        return array(
            'file' => array(self::BELONGS_TO, 'File', 'id'),
            'theme' => array(self::BELONGS_TO, 'Theme', 'theme_id'),
            'manufacturer' => array(self::BELONGS_TO, 'Partner', 'manufacturer_id'),
            'part_of' => array(self::BELONGS_TO, 'BuildingStructurePart', 'part_of_id'),
            'approved_submitter' => array(self::BELONGS_TO, 'User', 'approved_submitter_id'),
            'warehouse_material' => array(self::BELONGS_TO, 'WarehouseMaterial', 'warehouse_material_id'),
            'drawings' => array(self::MANY_MANY, 'File', "$construction_material_approvals_to_drawings(construction_material_approval_id, file_id)"),
            'atests' => array(self::MANY_MANY, 'File', "$construction_material_approvals_to_files(construction_material_approval_id, file_id)"),
        );
    }

    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters' : return array(
                'code' => 'text',
                'material_name' => 'text',
                'type' => 'text',
                'location' => 'text',
                'tehnical_charactetistics' => 'text',
                'file' => 'relation',
                'theme' => 'relation',
                'manufacturer' => 'relation',
                'part_of' => 'relation',
                'used_in' => 'text',
                'approved' => 'boolean',
                'approved_submitter' => 'relation',
                'approved_timestamp' => 'date_time_range',
                'warehouse_material' => 'relation',
                'drawings' => 'relation',
                'atests' => 'relation'
            );
            case 'textSearch': return array(
                'code' => 'text'
            );
            case 'statuses':  return array(
                'approved' => array(
                    'title' => Yii::t('BuildingsModule.ConstructionMaterialApproval', 'Approved'),
                    'timestamp' => 'approved_timestamp',
                    'user' => array('approved_submitter_id','relName'=>'approved_submitter'),
                    'checkAccessConfirm' => 'ApprovedConfirm',
                    'checkAccessRevert' => 'ApprovedRevert'
                )
            );
            case 'mutual_forms': return [
                'base_relation'=>'file', 'relations' => []
            ];
            case 'form_list_relations' : return array('drawings','atests');
            default: return parent::modelSettings($column);
        }
    }
    
    public function beforeSave() 
    {
        if (empty($this->code))
        {
//            $this->code = $this->part_of->code . '_' . $this->theme->id;
        }
        
        return parent::beforeSave();
    }
}
