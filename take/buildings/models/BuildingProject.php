<?php

class BuildingProject extends SIMAActiveRecord 
{   
    public static $BOOK_NAME_0 = 'book_0';
    public static $BOOK_NAME_1 = 'book_1';
    public static $BOOK_NAME_2 = 'book_2';
    public static $BOOK_NAME_3 = 'book_3';
    public static $BOOK_NAME_4 = 'book_4';
    public static $BOOK_NAME_5 = 'book_5';
    public static $BOOK_NAME_6 = 'book_6';
    public static $BOOK_NAME_7 = 'book_7';
    public static $BOOK_NAME_8 = 'book_8';
    public static $BOOK_NAME_9 = 'book_9';
    public static $BOOK_NAME_10 = 'book_10';
    
    public static $PREFIX_BOOK = 'book';
    public static $PREFIX_NOTEBOOK = 'notebook';
    public static $PREFIX_PART = 'part';
    public static $PREFIX_BOOK_PART = 'book_part';
    public static $ADDITIONAL_CONTENT_PREFIXES = [
        'text', 
        'numeric',
        'graphic'
    ];
    
    public $max;
    public $_number='';
    
    public $booksCheckBoxes = null;
    
    private $_scopes_uniq = '';
    
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'buildings.projects';
    }

    public function moduleName() {
        return 'buildings';
    }

    public function __get($column) {
        switch ($column) {
            case 'path2'    :   return 'buildings/buildingProject';
            case 'DisplayName': 
                return $this->display_name;
            case 'SearchName': return $this->DisplayName;
            case 'isBaseProject': return empty($this->parent_id);
            case 'isBook':
                $result = false;
                
                if($this->prefix === BuildingProject::$PREFIX_BOOK)
                {
                    $result = true;
                }
                
                return $result;
            case 'NumberFull':
                $result = $this->number;
                
                if($this->isBaseProject)
                {
                    $result .= '/'.$this->building_project_type->short_name;
                }
                else
                {
                    $result = $this->parent->NumberFull.'/'.$this->order;
                }
                
                return $result;
            case 'CanHaveAdditionalContent':
                $result = false;
                
                if(count($this->children) === 0
                    && !in_array($this->prefix, BuildingProject::$ADDITIONAL_CONTENT_PREFIXES))
                {
                    $result = true;
                }
                
                return $result;
            default: return parent::__get($column);
        }
    }
    
    public function rules() {
        return array(
            array('name, building_project_type_id','required'),
            array('date, order, number, display_name, geo_object_id, _number, prefix, '
                . 'theme_id, building_projects_sheet_id, '
                . 'building_project_type_id, booksCheckBoxes', 'safe'),
            array('prefix','checkPrefix'),
            array('booksCheckBoxes','checkBooksCheckBoxes'),
            array('number','checkNumber'),
            array('geo_object_id, building_projects_sheet_id,theme_id, building_project_type_id, parent_id', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => array('insert', 'update'))
        );
    }
    
    public function checkPrefix()
    {
        if($this->isBaseProject)
        {
            if(!empty($this->prefix))
            {
                $this->addError('prefix', 'Osnovni projekat ne može imati prefix! - '.$this->prefix);
            }
        }
        else
        {
            if(empty($this->prefix))
            {
                $this->addError('prefix', 'Mora biti izabran prefix projekta!');
            }
            else
            {
                if($this->parent->prefix === BuildingProject::$PREFIX_BOOK
                    && $this->prefix!==BuildingProject::$PREFIX_NOTEBOOK 
                    && $this->prefix!==BuildingProject::$PREFIX_BOOK_PART
                    && !in_array($this->prefix, BuildingProject::$ADDITIONAL_CONTENT_PREFIXES))
                {
                    $this->addError('prefix', Yii::t('BuildingsModule.BuildingProject', 'BookChildPrefixInvalid'));

                }
                else if($this->parent->prefix==BuildingProject::$PREFIX_NOTEBOOK
                        && $this->prefix!=BuildingProject::$PREFIX_PART
                        && !in_array($this->prefix, BuildingProject::$ADDITIONAL_CONTENT_PREFIXES))
                {
                    $this->addError('prefix', Yii::t('BuildingsModule.BuildingProject', 'NotebookChildPrefixInvalid'));
                }
                else if(($this->parent->prefix === BuildingProject::$PREFIX_PART 
                        || $this->parent->prefix === BuildingProject::$PREFIX_BOOK_PART)
                        && !in_array($this->prefix, BuildingProject::$ADDITIONAL_CONTENT_PREFIXES))
                {
                    $this->addError('prefix', Yii::t('BuildingsModule.BuildingProject', 'PartsChildPrefixInvalid'));
                }
            }

            foreach ($this->parent->children as $child)
            {
                if($this->prefix !== $child->prefix 
                    && !in_array($this->prefix, BuildingProject::$ADDITIONAL_CONTENT_PREFIXES))
                {
                    $this->addError('prefix', 'Prefix mora biti '.$child->columnDisplays('prefix').'!');
                    break;
                }
            }
        }
    }
    
    public function checkBooksCheckBoxes()
    {
        if(isset($this->booksCheckBoxes))
        {
            $checked_checkboxes_order_numbers = $this->booksCheckBoxesCheckedOrderNumbers();
                        
            $children = $this->children;
            foreach($children as $child)
            {
                if($child->prefix === 'book')
                {
                    if(!in_array($child->order, $checked_checkboxes_order_numbers))
                    {
                        $this->addError('booksCheckBoxes', 
                                Yii::t('BuildingsModule.BuildingProject', 'CannotDeleteBook', ['{book}' => $child->DisplayName]));
                    }
                }
            }
        }
    }
    
    public function checkNumber()
    {
        if(!empty($this->_number))
        {
            if(BuildingProject::model()->findByAttributes(array('number'=>  $this->_number))!=null)
            {
                $this->addError('_number', Yii::t('BuildingsModule.BuildingProject', 'NumberTaken'));
            }
        }
    }
    
    public function relations($child_relations = [])
    {
        $company_licenses_to_projects_table = CompanyLicenseToProject::model()->tableName();
        return array(
            'parent' => array(self::BELONGS_TO, 'BuildingProject', 'parent_id'),
            'geo_object' => array(self::BELONGS_TO, 'GeoObject', 'geo_object_id'),
            
            'building_project_type' => array(self::BELONGS_TO, 'BuildingProjectType', 'building_project_type_id'),
//            'building_project_kind' => array(self::BELONGS_TO, 'BuildingProjectKind', 'building_project_kind_id'),
            'building_project_sheet'=>array(self::BELONGS_TO, 'BuildingProjectSheet', 'building_projects_sheet_id'),
            'building_project_sheets'=>array(self::HAS_MANY, 'BuildingProjectSheet', 'id'),
            
            'theme'=>array(self::BELONGS_TO, 'Theme', 'theme_id'),
            
            'children'=>array(self::HAS_MANY, 'BuildingProject', 'parent_id'),
            'children_other'=>array(self::HAS_MANY, 'BuildingProject', 'parent_id', 'condition'=>'"order" is null'),
            'child_0'=>array(self::HAS_ONE, 'BuildingProject', 'parent_id', 'condition'=>'"order" = 0'),
            'child_1'=>array(self::HAS_ONE, 'BuildingProject', 'parent_id', 'condition'=>'"order" = 1'),
            'child_2'=>array(self::HAS_ONE, 'BuildingProject', 'parent_id', 'condition'=>'"order" = 2'),
            'child_3'=>array(self::HAS_ONE, 'BuildingProject', 'parent_id', 'condition'=>'"order" = 3'),
            'child_4'=>array(self::HAS_ONE, 'BuildingProject', 'parent_id', 'condition'=>'"order" = 4'),
            'child_5'=>array(self::HAS_ONE, 'BuildingProject', 'parent_id', 'condition'=>'"order" = 5'),
            'child_6'=>array(self::HAS_ONE, 'BuildingProject', 'parent_id', 'condition'=>'"order" = 6'),
            'child_7'=>array(self::HAS_ONE, 'BuildingProject', 'parent_id', 'condition'=>'"order" = 7'),
            'child_8'=>array(self::HAS_ONE, 'BuildingProject', 'parent_id', 'condition'=>'"order" = 8'),
            'child_9'=>array(self::HAS_ONE, 'BuildingProject', 'parent_id', 'condition'=>'"order" = 9'),
            'child_10'=>array(self::HAS_ONE, 'BuildingProject', 'parent_id', 'condition'=>'"order" = 10'),
            
            
            'books'=>array(self::HAS_MANY, 'BuildingProject', 'parent_id', 'condition'=>"prefix='book'", 'order'=>'"order" asc'),
            'book_parts'=>array(self::HAS_MANY, 'BuildingProject', 'parent_id', 'condition'=>"prefix='book_part'", 'order'=>'"order" asc'),
            'notebooks'=>array(self::HAS_MANY, 'BuildingProject', 'parent_id', 'condition'=>"prefix='notebook'", 'order'=>'"order" asc'),
            
            'responsible_designers_decision' => [self::BELONGS_TO, 'File', 'responsible_designers_decision_id'],
            'responsible_designer_licence' => [self::BELONGS_TO, 'PersonToWorkLicense', 'responsible_designer_licence_id'],                
            'company_licenses'=>array(self::MANY_MANY, 'CompanyLicenseToCompany', "$company_licenses_to_projects_table(project_id,company_license_id)")
        );
    }
    
    public function scopes() 
    {
        $alias = $this->getTableAlias(false, false);
        if ($this->_scopes_uniq==null)
        {
            $this->_scopes_uniq = SIMAHtml::uniqid();
        }
        return array(
            'recently' => array(
                'order' => 'id',
            ),
             'byName' => array(
                'order' => 'name, id',
            ),
            'baseProjects' => [
               'condition' => $alias.'.parent_id is null'
            ]
        );
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters': return [
                'name' => 'text',
                'number' => 'text',
                'building_project_type' => 'relation',
                'theme' => 'relation',
                'geo_object' => 'relation',
                'parent' => 'relation'
            ];
            case 'textSearch': return array(
                'name' => 'text',
                'number' => 'text',
            );
            case 'options':  return array_merge(array('form'), array(
                'open' 
            ));
            case 'form_list_relations' : return array('company_licenses');
            default: return parent::modelSettings($column);
        }
    }
    
    public function getmaxNumber() {
        $criteria = new CDbCriteria;
        $criteria->select = 'max(number) AS max';
        $row = BuildingProject::model()->find($criteria);
        return $row['max'];
    }
    
    public function setNumber()
    {
        if($this->number==null)
        {
            if(isset($this->parent) && $this->parent_id!='')
            {
               $this->number=  $this->parent->number;
            }
            else 
            {
                 $this->number=  $this->getmaxNumber()+1;
            }
            
            $this->save();
        }
        
        foreach ($this->children as $child)
        {
            if($child->number==null)
            {
                $child->setNumber();
            }
        }
    }
   
    public function getChildNumber($prefix)
    {   
        $number_children=0;
        if($prefix=='book')
        {
           $number_children=  count($this->books);
        }
        else if($prefix=='notebook')
        {
            $number_children=  count($this->notebooks);
        }
        
        return $number_children;
    }
    
    public function afterSave()
    {
        parent::afterSave();
        //MilosS(11.7.2017): trenuta ideja je da se ne kreiraju pod knjige iz forme
//        if(isset($this->booksCheckBoxes))
//        {
//            $children_order_numbers = $this->childrenBooksOrderNumbers();
//                  
//            $decoded_booksCheckBoxes = json_decode($this->booksCheckBoxes);
//            
//            foreach($decoded_booksCheckBoxes as $bcb)
//            {
//                if($bcb->isChecked === true)
//                {
//                    if(!in_array($bcb->aditional_params->order, $children_order_numbers))
//                    {
//                        $childBook = new BuildingProject;
//                        $childBook->prefix = 'book';
//                        $childBook->parent_id = $this->id;
//                        $childBook->order = $bcb->aditional_params->order;
//                        $childBook->building_project_type_id = $this->building_project_type_id;
//                        $childBook->name = $bcb->aditional_params->display_name;
//                        $childBook->theme_id = $this->theme_id;
//                        $childBook->save();
//                    }
//                }
//            }
//        }
        
        if(!empty($this->parent_id))
        {
            $parent = $this->parent;
            $parent_children = $parent->children;
            foreach($parent_children as $pc)
            {
                if($pc->id === $this->id 
                        || $pc->prefix !== $this->prefix 
                        || $pc->order != $this->order)
                {
                    continue;
                }

                if(isset($this->__old['order']) && $this->__old['order'] !== $this->order)
                {
                    $pc->order = $this->__old['order'];
                }
                else
                {
                    $pc->order = $pc->parent->getChildNumber($pc->prefix)+1;
                }
                $pc->save();
            }
        }
        
        $this->updateBaseProject();
        
        return parent::afterSave();
    }
    
    private function updateBaseProject()
    {
        if (Yii::app()->isWebApplication() && !empty($this->id))
        {
            $curr = $this;
            while(isset($curr->parent))
            {
                $curr = $curr->parent;
            }     
            Yii::app()->controller->raiseUpdateModels($curr);
        }
        
    }

    public function setNewNumber($num)
    {
        $this->number=$num;
        foreach ($this->children as $child)
        {
            $child->setNewNumber($num);
            $child->save();
        }
    }
    
    public function beforeSave()
    {
        if($this->parent_id!=null && $this->parent_id!='')
        {
            if($this->number==null && $this->parent->number!=null)
            {
                $this->number=  $this->parent->number;
            }

//            if(empty($this->order) && !in_array($this->prefix, BuildingProject::$ADDITIONAL_CONTENT_PREFIXES))
//            {
//               $this->order = $this->parent->getChildNumber($this->prefix)+1;
//            }
        }
        
        if($this->_number!='')
        {
            $this->setNewNumber($this->_number);
        }
        
        $this->display_name = $this->name;
        if(!empty($this->prefix) && !in_array($this->prefix, BuildingProject::$ADDITIONAL_CONTENT_PREFIXES))
        {
            $this->display_name = $this->columnDisplays('prefix').' '.$this->order.' : '.$this->name;
        }
                
        return parent::beforeSave();
    }    

    private function booksCheckBoxesCheckedOrderNumbers()
    {
        if(empty($this->booksCheckBoxes))
        {
            throw new Exception(Yii::t('BuildingsModule.BuildingProject', 'BooksCheckBoxesEmpty'));
        }
        
        $checked_checkboxes_order_numbers = [];
        
        $decoded_booksCheckBoxes = json_decode($this->booksCheckBoxes);

        foreach($decoded_booksCheckBoxes as $bcb)
        {
            if($bcb->isChecked === true)
            {
                $checked_checkboxes_order_numbers[] = $bcb->aditional_params->order;
            }
        }
        
        return $checked_checkboxes_order_numbers;
    }
    
    public function childrenBooksOrderNumbers()
    {
        $children_order_numbers = [];
        
        $children = $this->children;
        
        foreach($children as $child)
        {
            if($child->prefix === 'book')
            {
                $children_order_numbers[] = $child->order;
            }
        }
        
        return $children_order_numbers;
    }
    
    public function getPrefixArray()
    {
        $prefix_displays = [
            BuildingProject::$PREFIX_BOOK => Yii::t('BuildingsModule.BuildingProject', 'Book'), 
            BuildingProject::$PREFIX_NOTEBOOK => Yii::t('BuildingsModule.BuildingProject', 'Notebook'), 
            BuildingProject::$PREFIX_PART => Yii::t('BuildingsModule.BuildingProject', 'Part'), 
            BuildingProject::$PREFIX_BOOK_PART => Yii::t('BuildingsModule.BuildingProject', 'BookPart'),
        ];
        foreach(BuildingProject::$ADDITIONAL_CONTENT_PREFIXES as $acp)
        {
            $prefix_displays[$acp] = Yii::t('BuildingsModule.BuildingProject', $acp);
        }
        return $prefix_displays;
    }
}