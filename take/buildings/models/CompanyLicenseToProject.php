<?php

class CompanyLicenseToProject extends SIMAActiveRecord
{   

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'buildings.company_licenses_to_projects';
    }

    public function moduleName()
    {
        return 'buildings';
    }

    public function __get($column)
    {
        switch ($column)
        {
            case 'DisplayName':
                $return = '';
                if (isset($this->company_license) && isset($this->project))
                {
                    $return = $this->company_license->DisplayName.'('.$this->project->DisplayName.')';
                }
                return $return;
            default: return parent::__get($column);
        }
    }

    public function rules()
    {
        return array(
            array('company_license_id, project_id', 'required'),
            array('company_license_id, project_id', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => array('insert', 'update'))
        );
    }

    public function relations($child_relations = [])
    {
        return array(
            'company_license' => array(self::BELONGS_TO, 'CompanyLicenseToCompany', 'company_license_id'),
            'project' => array(self::BELONGS_TO, 'BuildingProject', 'project_id')            
        );
    }

    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters' : return array(
                'company_license' => 'relation',
                'project' => 'relation'
            );
            default : return parent::modelSettings($column);
        }
    }
    
}
