<?php

class BillOfQuantityItem extends SIMAActiveRecord
{
    public $is_delete_from_group = false;
    
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    
    public function tableName()
    {
        return 'buildings.bill_of_quantities_items';
    }
    
    public function moduleName()
    {
        return 'buildings';
    }
    
    public function __get($column) 
    {
        switch ($column) {
            case 'DisplayName': return $this->name;
            case 'SearchName': return "{$this->name}({$this->group->DisplayName})";

            default: return parent::__get($column);
        }
    }
    
    public function rules()
    {
        return [
            ['name, group_id, measurement_unit_id, quantity, value_per_unit', 'required'],
            ['value, order, note', 'safe'],
            [
                'group_id, measurement_unit_id', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => ['insert','update']
            ],
            ['order', 'numerical', 'integerOnly' => true],
            ['order', 'length', 'max' => 10],
            ['quantity', 'checkNumericNumber', 'precision' => 20, 'scale' => 2],
            ['value_per_unit', 'checkNumericNumber', 'precision' => 20, 'scale' => 10],
            ['value', 'checkNumericNumber', 'precision' => 22, 'scale' => 2],
            ['order', 'unique_with', 'with' => ['group_id']]
        ];
    }
    
    public function relations($child_relations = [])
    {
        return [
            'measurement_unit' => [self::BELONGS_TO, MeasurementUnit::class, 'measurement_unit_id'],
            'group' => [self::BELONGS_TO, BillOfQuantityItemGroup::class, 'group_id'],
            'item_to_objects' => [self::HAS_MANY, BillOfQuantityItemToObject::class, 'item_id'],
            'item_to_objects_leafs' => [self::HAS_MANY, BillOfQuantityItemToObject::class, 'item_id', 'model_filter' => [
                'object' => [
                    'scopes' => ['byName','onlyLeaf']
                ]
            ]],
            'building_measurement_sheets' => [self::HAS_MANY, BuildingMeasurementSheet::class, 'bill_of_quantity_item_id']
        ];
    }
    
    public function scopes()
    {
        $alias = $this->getTableAlias();

        return [
            'byName' => [
                'order' => "$alias.name ASC"
            ],
            'byOrder' => [
                'order' => "$alias.order ASC"
            ]
        ];
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters': return [
                'name' => 'text',
                'group' => 'relation',
                'order' => 'integer',
                'measurement_unit' => 'relation',
                'quantity' => 'numeric',
                'value_per_unit' => 'numeric',
                'value' => 'numeric',
                'building_measurement_sheets' => 'relation',
                'note' => 'text'
            ];
            case 'textSearch': return [
                'name' => 'text',
                'group' => 'relation'
            ];
            case 'big_numbers': return ['quantity', 'value_per_unit', 'value'];
            default: return parent::modelSettings($column);
        }
    }
    
    protected function modelOptions(\User $user = null):array
    {
        return ['form', 'delete'];
    }
    
    public function afterSave() 
    {
        parent::afterSave();
        
        if ($this->isNewRecord)
        {
            foreach ($this->group->bill_of_quantity->objects as $object)
            {
                $bill_of_quantity_item_to_object = BillOfQuantityItemToObject::model()->findByAttributes([
                    'item_id' => $this->id,
                    'object_id' => $object->id
                ]);
                if (empty($bill_of_quantity_item_to_object))
                {
                    $bill_of_quantity_item_to_object = new BillOfQuantityItemToObject();
                    $bill_of_quantity_item_to_object->item_id = $this->id;
                    $bill_of_quantity_item_to_object->object_id = $object->id;
                    $bill_of_quantity_item_to_object->save();
                }
            }
            
            //ako dodamo novu stavku onda moramo preracunati statuse u svim situacijama u predmeru
            if (!empty($this->group->bill_of_quantity->building_construction))
            {
                foreach ($this->group->bill_of_quantity->building_construction->building_interim_bills as $building_interim_bill)
                {
                    $building_interim_bill->checkStatus();
                }
            }
        }

        if (
                !$this->group->bill_of_quantity->has_object_value_per_unit &&
                (
                    $this->columnChanged('value_per_unit')
                )
           )
        {
            //mora da se dovuce model iz baze jer se dodavanje veze objekat stavka radi u kodu iznad
            $bill_of_quantity_item = BillOfQuantityItem::model()->findByPkWithCheck($this->id);
            foreach ($bill_of_quantity_item->item_to_objects as $item_to_object)
            {
                $item_to_object->value_per_unit = $this->value_per_unit;
                $item_to_object->save();
            }
        }
        
        if ($this->columnChanged('group_id'))
        {
            $building_construction = BuildingConstruction::model()->findByAttributes([
                'bill_of_quantity_id' => $this->group->bill_of_quantity_id
            ]);
            if (!empty($building_construction))
            {
                $first_interim_bill = BuildingInterimBill::model()->find([
                    'model_filter' => [
                        'display_scopes' => 'byDateAsc',
                        'building_construction' => [
                            'ids' => $building_construction->id
                        ]
                    ]
                ]);
            }
            
            foreach ($this->group->bill_of_quantity->leaf_objects as $leaf_object)
            {
                $leaf_object->calcValueRecursive();
                
                if (!empty($first_interim_bill))
                {
                    if (!empty($this->__old['group_id']))
                    {
                        $measurement_sheet_group = BuildingMeasurementSheetGroup::model()->findByAttributes([
                            'building_interim_bill_id' => $first_interim_bill->id,
                            'bill_of_quantity_item_group_id' => $this->__old['group_id'],
                            'object_id' => $leaf_object->id
                        ]);
                        if (!empty($measurement_sheet_group))
                        {
                            $measurement_sheet_group->calcValueDoneRecursive();
                        }
                    }
                    
                    if (!empty($this->group_id))
                    {
                        $measurement_sheet_group1 = BuildingMeasurementSheetGroup::model()->findByAttributes([
                            'building_interim_bill_id' => $first_interim_bill->id,
                            'bill_of_quantity_item_group_id' => $this->group_id,
                            'object_id' => $leaf_object->id
                        ]);
                        if (!empty($measurement_sheet_group1))
                        {
                            $measurement_sheet_group1->calcValueDoneRecursive();
                        }
                    }
                    
                    $leaf_object->calcValueDoneRecursiveForInterimBill($first_interim_bill);
                }
            }
        }
    }
    
    public function beforeDelete() 
    {
        foreach ($this->item_to_objects as $item_to_object)
        {
            $item_to_object->calc_value = !$this->is_delete_from_group;
            $item_to_object->delete();
        }
        
        foreach ($this->building_measurement_sheets as $building_measurement_sheet)
        {
            $building_measurement_sheet->delete();
        }

        return parent::beforeDelete();
    }
    
    public function afterDelete() 
    {
        parent::afterDelete();
        
        //ako obrisemo stavku onda moramo preracunati statuse u svim situacijama u predmeru
        if (!empty($this->group->bill_of_quantity->building_construction))
        {
            foreach ($this->group->bill_of_quantity->building_construction->building_interim_bills as $building_interim_bill)
            {
                $building_interim_bill->checkStatus();
            }
        }
    }
    
    public function afterValidateDependencyError(array $foreign_dependencies)
    {        
        $allowed_tables = [
            BillOfQuantityItemToObject::tableName().':item_id',
            BuildingMeasurementSheet::tableName().':bill_of_quantity_item_id'
        ];
        
        $parsed_foreign_dependencies = array_diff($foreign_dependencies, $allowed_tables);
        
        parent::afterValidateDependencyError($parsed_foreign_dependencies);
    }
    
    public function cloneItem($group_id, $old_object_ids_mapper)
    {
        $new_item = new BillOfQuantityItem();
        $new_item->setAttributes($this->getAttributes());
        $new_item->group_id = $group_id;
        $new_item->save();

        foreach ($this->item_to_objects as $item_to_object)
        {
            $new_object_id = $old_object_ids_mapper[$item_to_object->object_id];
            if (!empty($new_object_id))
            {
                $new_item_to_object = BillOfQuantityItemToObject::model()->findByAttributes([
                    'item_id' => $new_item->id,
                    'object_id' => $new_object_id
                ]);
                if (empty($new_item_to_object))
                {
                    $new_item_to_object = new BillOfQuantityItemToObject();
                }
                $new_item_to_object->setAttributes($item_to_object->getAttributes());
                $new_item_to_object->item_id = $new_item->id;
                $new_item_to_object->object_id = $new_object_id;
                $new_item_to_object->save();
            }
        }
    }
    
    public function getFullOrderPath() 
    {   
        return $this->group->calcOrderPathRecursively().".".$this->order;
    }
}

