<?php

class BillOfQuantityToUser extends SIMAActiveRecord
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
 
    public function tableName()
    {
        return 'buildings.bill_of_quantities_to_users';
    }
    
    public function moduleName()
    {
        return 'buildings';
    }
    
    public function rules()
    {
        return [
            ['bill_of_quantity_id, user_id', 'required'],
            ['user_id', 'unique_with', 'with' => ['bill_of_quantity_id']]
        ];
    }
    
    public function relations($child_relations = [])
    {
        return [
            'bill_of_quantity' => [self::BELONGS_TO, BillOfQuantity::class, 'bill_of_quantity_id'],
            'user' => [self::BELONGS_TO, User::class, 'user_id']
        ];
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters' : return array_merge(parent::modelSettings('filters'), [
                'bill_of_quantity' => 'relation',
                'user' => 'relation'
            ]);
            default: return parent::modelSettings($column);
        }
    }
}
