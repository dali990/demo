<?php

class BuildingSiteDiary extends SIMAActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function moduleName()
    {
        return 'buildings';
    }

    public function tableName()
    {
        return 'buildings.site_diaries';
    }

    public function __get($column) 
    {
        switch ($column) {
            case 'DisplayName': return !empty($this->file) ? $this->file->DisplayName : '';
            case 'SearchName': return $this->DisplayName;

            default: return parent::__get($column);
        }
    }
    
    public function rules()
    {
        return [
            ['building_construction_id, date, constructor_id', 'required'],
            ['work_description, remarks', 'safe'],
            ['diary_creator_confirmed, diary_creator_id, diary_creator_confirmed_timestamp', 'safe'],
            ['responsible_engineer_confirmed, responsible_engineer_id, responsible_engineer_confirmed_timestamp', 'safe'],
            ['supervisor_confirmed, supervisor_id, supervisor_confirmed_timestamp', 'safe'],
            [
                'building_construction_id, constructor_id, diary_creator_id, responsible_engineer_id, supervisor_id', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => ['insert','update']
            ],
            ['date', 'unique_with', 'with' => ['building_construction_id', 'constructor_id']],
            ['constructor_id', 'checkConstructor']
        ];
    }
    
    public function checkConstructor()
    {
        if (
                !$this->hasErrors() && 
                intval($this->constructor_id) !== intval(Yii::app()->company->id) && 
                (
                    $this->isNewRecord || $this->columnChanged('constructor_id')
                )
            )
        {
            $company_to_theme_cnt = CompanyToTheme::model()->count([
                'model_filter' => [
                    'company' => [
                        'ids' => $this->constructor_id
                    ],
                    'theme' => [
                        'ids' => $this->building_construction_id
                    ]
                ]
            ]);
            if ($company_to_theme_cnt === 0)
            {
                $this->addError('constructor_id', Yii::t('BuildingsModule.BuildingSiteDiary', 'ConstructorMustBelongsToTheme', [
                    '{theme_name}' => $this->building_construction->theme->DisplayName
                ]));
            }
        }
    }

    public function relations($child_relations = [])
    {
        return [
            'file' => [self::HAS_ONE, File::class, 'id'],
            'building_construction' => [self::BELONGS_TO, BuildingConstruction::class, 'building_construction_id'],
            'constructor' => [self::BELONGS_TO, Company::class, 'constructor_id'],
            'diary_creator' => [self::BELONGS_TO, User::class, 'diary_creator_id'],
            'responsible_engineer' => [self::BELONGS_TO, User::class, 'responsible_engineer_id'],
            'supervisor' => [self::BELONGS_TO, User::class, 'supervisor_id'],
            'building_site_diary_work_hours' => [self::HAS_MANY, BuildingSiteDiaryWorkHour::class, 'site_diary_id', 'order' => 'start_datetime ASC']
        ];
    }
    
    public function scopes()
    {
        $alias = $this->getTableAlias();
        $uniq = SIMAHtml::uniqid();
        $company_table_name = Company::model()->tableName();
        
        return [
            'byConstructorName' => [
                'join' => "
                    left join $company_table_name co$uniq on co$uniq.id = $alias.constructor_id
                ",
                'order' => "co$uniq.name ASC"
            ]
        ];
    }
    
    public function forDate($date)
    {
        $uniq = SIMAHtml::uniqid();
        $alias = $this->getTableAlias();
        
        $this->getDbCriteria()->mergeWith([
            'condition' => "$alias.date = :param1$uniq",
            'params' => [
                ":param1$uniq" => SIMAHtml::UserToDbDate($date)
            ]
        ]);

        return $this;
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters': return [
                'file' => 'relation',
                'building_construction' => 'relation',
                'constructor' => 'relation',
                'diary_creator' => 'relation',
                'responsible_engineer' => 'relation',
                'supervisor' => 'relation',
                'diary_creator_confirmed' => 'boolean',
                'responsible_engineer_confirmed' => 'boolean',
                'supervisor_confirmed' => 'boolean'
            ];
            case 'textSearch': return [
                'file' => 'relation'
            ];
            case 'statuses': return [
                'diary_creator_confirmed' => [
                    'title' => Yii::t('BuildingsModule.BuildingSiteDiary', 'DiaryCreatorConfirmed'),
                    'timestamp' => 'diary_creator_confirmed_timestamp',
                    'user' => ['diary_creator_id', 'relName' => 'diary_creator'],
                    'checkAccessConfirm' => 'checkCanDiaryCreatorConfirm',
                    'checkAccessRevert' => 'checkCanDiaryCreatorConfirm'
                ],
                'responsible_engineer_confirmed' => [
                    'title' => Yii::t('BuildingsModule.BuildingSiteDiary', 'ResponsibleEngineerConfirmed'),
                    'timestamp' => 'responsible_engineer_confirmed_timestamp',
                    'user' => ['responsible_engineer_id', 'relName' => 'responsible_engineer'],
                    'checkAccessConfirm' => 'checkCanResponsibleEngineerConfirm',
                    'checkAccessRevert' => 'checkCanResponsibleEngineerConfirm'
                ],
                'supervisor_confirmed' => [
                    'title' => Yii::t('BuildingsModule.BuildingSiteDiary', 'SupervisorConfirmed'),
                    'timestamp' => 'supervisor_confirmed_timestamp',
                    'user' => ['supervisor_id', 'relName' => 'supervisor'],
                    'checkAccessConfirm' => 'checkCanSupervisorConfirm',
                    'checkAccessRevert' => 'checkCanSupervisorConfirm'
                ]
            ];
            case 'mutual_forms': return ['base_relation' => 'file'];
            default: return parent::modelSettings($column);
        }
    }
    
    protected function modelOptions(\User $user = null):array
    {
        return ['form', 'delete', 'open'];
    }
    
    public function checkCanDiaryCreatorConfirm()
    {
        return true;
    }

    public function checkCanResponsibleEngineerConfirm()
    {
        return true;
    }

    public function checkCanSupervisorConfirm()
    {
        return true;
    }
    
    public function beforeSave() 
    {
        if ($this->isNewRecord && empty($this->id))
        {
            $file = new File();
            $file->name = Yii::t('BuildingsModule.BuildingSiteDiary', 'BuildingSiteDiary') . ": {$this->building_construction->theme->DisplayName}";
            if (Yii::app()->isWebApplication())
            {
                $file->responsible_id = Yii::app()->user->id;
            }
            $file->document_type_id = Yii::app()->configManager->get('buildings.building_site_diary_document_type_id', false);
            $file->file_belongs = CJSON::encode([
                'ids' => [
                    [
                        'class' => '_non_editable',
                        'id' => $this->building_construction->theme->tag->id,
                        'display_name' => $this->building_construction->theme->tag->DisplayName
                    ]
                ],
                'default_item' => []
            ]);
            $file->save();
            $this->id = $file->id;
            $this->file = $file;
        }
        
        return parent::beforeSave();
    }
}