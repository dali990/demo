<?php

class BuildingMeasurementSheetGroup extends SIMAActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function moduleName()
    {
        return 'buildings';
    }

    public function tableName()
    {
        return 'buildings.measurement_sheet_groups';
    }
    
    public function __get($column) 
    {
        switch ($column) {
            case 'total_quantity_done':
                return $this->pre_quantity_done->plus($this->quantity_done);
            case 'total_value_done':
                return $this->pre_value_done->plus($this->value_done);
            case 'total_percentage_done':
                return $this->getTotalPercentageDone();
            case 'pre_percentage_done':
                return $this->getPrePercentageDone();
            case 'percentage_done':
                return $this->getPercentageDone();

            default: return parent::__get($column);
        }
    }
    
    public function rules()
    {
        return [
            ['building_interim_bill_id, bill_of_quantity_item_group_id, object_id', 'required'],
            ['value_done, quantity_done, pre_value_done, pre_quantity_done', 'safe'],
            [
                'building_interim_bill_id, bill_of_quantity_item_group_id, object_id', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => ['insert','update']
            ],
            ['bill_of_quantity_item_group_id', 'unique_with', 'with' => ['building_interim_bill_id', 'object_id'], 'on' => ['insert','update']]
        ];
    }

    public function relations($child_relations = [])
    {
        return [
            'building_interim_bill' => [self::BELONGS_TO, BuildingInterimBill::class, 'building_interim_bill_id'],
            'bill_of_quantity_item_group' => [self::BELONGS_TO, BillOfQuantityItemGroup::class, 'bill_of_quantity_item_group_id'],
            'object' => [self::BELONGS_TO, BillOfQuantityObject::class, 'object_id']
        ];
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters': return [
                'building_interim_bill' => 'relation',
                'bill_of_quantity_item_group' => 'relation',
                'object' => 'relation',
                'value_done' => 'numeric',
                'quantity_done' => 'numeric',
                'pre_value_done' => 'numeric',
                'pre_quantity_done' => 'numeric'
            ];
            case 'big_numbers': return ['value_done', 'quantity_done', 'pre_value_done', 'pre_quantity_done'];
            default: return parent::modelSettings($column);
        }
    }
    
    public function forObject($object_id)
    {
        $uniq = SIMAHtml::uniqid();
        $alias = $this->getTableAlias();

        $this->getDbCriteria()->mergeWith([
            'condition' => "$alias.object_id = :param1$uniq",
            'params' => [
                ":param1$uniq" => $object_id
            ]
        ]);

        return $this;
    }
    
    public function forBillOfQuantityItemGroup($bill_of_quantity_item_group_id)
    {
        $uniq = SIMAHtml::uniqid();
        $alias = $this->getTableAlias();

        $this->getDbCriteria()->mergeWith([
            'condition' => "$alias.bill_of_quantity_item_group_id = :param1$uniq",
            'params' => [
                ":param1$uniq" => $bill_of_quantity_item_group_id
            ]
        ]);

        return $this;
    }
    
    public function calcValueDoneRecursive()
    {
        $quantity_done = SIMABigNumber::fromFloat(0.00, 2);
        $value_done = SIMABigNumber::fromFloat(0.00, 2);
        $pre_quantity_done = SIMABigNumber::fromFloat(0.00, 2);
        $pre_value_done = SIMABigNumber::fromFloat(0.00, 2);
        $sheets_written = true;
        $sheets_confirmed = true;
        
        foreach ($this->bill_of_quantity_item_group->children as $child)
        {
            $sheet_group = BuildingMeasurementSheetGroup::get($this->building_interim_bill, $child, $this->object);
            
            $quantity_done = $quantity_done->plus($sheet_group->quantity_done);
            $value_done = $value_done->plus($sheet_group->value_done);
            $pre_quantity_done = $pre_quantity_done->plus($sheet_group->pre_quantity_done);
            $pre_value_done = $pre_value_done->plus($sheet_group->pre_value_done);
            if ($sheets_written === true && $sheet_group->measurement_sheets_written === false)
            {
                $sheets_written = false;
            }
            if ($sheets_confirmed === true && $sheet_group->measurement_sheets_confirmed === false)
            {
                $sheets_confirmed = false;
            }
        }
        
        foreach ($this->bill_of_quantity_item_group->items as $item)
        {
            $sheet = BuildingMeasurementSheet::model()->findByAttributes([
                'building_interim_bill_id' => $this->building_interim_bill->id,
                'bill_of_quantity_item_id' => $item->id,
                'object_id' => $this->object->id
            ]);
            
            if (!empty($sheet))
            {
                $quantity_done = $quantity_done->plus($sheet->quantity_done);
                $value_done = $value_done->plus($sheet->value_done);
                $pre_quantity_done = $pre_quantity_done->plus($sheet->pre_quantity_done);
                $pre_value_done = $pre_value_done->plus($sheet->pre_value_done);
                if ($sheets_written === true && $sheet->written === false)
                {
                    $sheets_written = false;
                }
                if ($sheets_confirmed === true && $sheet->confirmed === false)
                {
                    $sheets_confirmed = false;
                }
            }
        }

        $this->quantity_done = $quantity_done;
        $this->value_done = $value_done;
        $this->pre_quantity_done = $pre_quantity_done;
        $this->pre_value_done = $pre_value_done;
        $this->measurement_sheets_written = $sheets_written;
        $this->measurement_sheets_confirmed = $sheets_confirmed;
        $this->save();
        
        $newer_interim_bills = BuildingInterimBill::model()->findAll([
            'model_filter' => [
                'display_scopes' => 'byDateAsc',
                'issue_date' => ['>', $this->building_interim_bill->issue_date],
                'building_construction' => [
                    'ids' => $this->building_interim_bill->building_construction_id
                ]
            ]
        ]);

        $_pre_quantity_done = $this->pre_quantity_done->plus($this->quantity_done);
        $_pre_value_done = $this->pre_value_done->plus($this->value_done);
        foreach ($newer_interim_bills as $newer_interim_bill)
        {
            $_sheet_group = BuildingMeasurementSheetGroup::get($newer_interim_bill, $this->bill_of_quantity_item_group, $this->object);
            $_sheet_group->pre_quantity_done = $_pre_quantity_done;
            $_sheet_group->pre_value_done = $_pre_value_done;
            $_sheet_group->save();
            
            $_pre_quantity_done = $_pre_quantity_done->plus($_sheet_group->quantity_done);
            $_pre_value_done = $_pre_value_done->plus($_sheet_group->value_done);
            
            $this->object->calcValueDoneRecursiveForInterimBill($newer_interim_bill);
        }
        
        $this->object->calcValueDoneRecursiveForInterimBill($this->building_interim_bill);
        
        if (!empty($this->bill_of_quantity_item_group->parent_id))
        {
            $parent_sheet_group = BuildingMeasurementSheetGroup::get($this->building_interim_bill, $this->bill_of_quantity_item_group->parent, $this->object);
            $parent_sheet_group->calcValueDoneRecursive();
        }
    }
    
    public function getPrePercentageDone()
    {
        $item_group_to_object = BillOfQuantityItemGroupToObject::model()->findByAttributes([
            'item_group_id' => $this->bill_of_quantity_item_group_id,
            'object_id' => $this->object_id
        ]);
        
        if (!empty($item_group_to_object))
        {
            $value = $item_group_to_object->value;
            if (!$value->isZero())
            {
                return $this->pre_value_done->divide($value)->multiple(SIMABigNumber::fromInteger(100), 2);
            }
        }
        
        return SIMABigNumber::fromFloat(0.00, 2);
    }
    
    public function getPercentageDone()
    {
        $item_group_to_object = BillOfQuantityItemGroupToObject::model()->findByAttributes([
            'item_group_id' => $this->bill_of_quantity_item_group_id,
            'object_id' => $this->object_id
        ]);
        
        if (!empty($item_group_to_object))
        {
            $value = $item_group_to_object->value;
            if (!$value->isZero())
            {
                return $this->value_done->divide($value)->multiple(SIMABigNumber::fromInteger(100), 2);
            }
        }
        
        return SIMABigNumber::fromFloat(0.00, 2);
    }
    
    public function getTotalPercentageDone()
    {
        $item_group_to_object = BillOfQuantityItemGroupToObject::model()->findByAttributes([
            'item_group_id' => $this->bill_of_quantity_item_group_id,
            'object_id' => $this->object_id
        ]);
        
        if (!empty($item_group_to_object))
        {
            $value = $item_group_to_object->value;
            if (!$value->isZero())
            {
                $curr_total_value = $this->pre_value_done->plus($this->value_done);
                return $curr_total_value->divide($value)->multiple(SIMABigNumber::fromInteger(100), 2);
            }
        }
        
        return SIMABigNumber::fromFloat(0.00, 2);
    }
    
    public static function get($building_interim_bill, $bill_of_quantity_item_group, $object)
    {
        $building_measurement_sheet_group = BuildingMeasurementSheetGroup::model()->findByAttributes([
            'building_interim_bill_id' => $building_interim_bill->id,
            'bill_of_quantity_item_group_id' => $bill_of_quantity_item_group->id,
            'object_id' => $object->id
        ]);
        if (empty($building_measurement_sheet_group))
        {
            $building_measurement_sheet_group = new BuildingMeasurementSheetGroup();
            $building_measurement_sheet_group->building_interim_bill_id = $building_interim_bill->id;
            $building_measurement_sheet_group->bill_of_quantity_item_group_id = $bill_of_quantity_item_group->id;
            $building_measurement_sheet_group->object_id = $object->id;
            $building_measurement_sheet_group->save();
        }
        
        return $building_measurement_sheet_group;
    }
}