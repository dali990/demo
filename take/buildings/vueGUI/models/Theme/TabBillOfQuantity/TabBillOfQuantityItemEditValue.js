/* global Vue, sima */

Vue.component('buildings-theme_tab-bill_of_quantity_item_edit_value', {
    template: '#buildings-theme_tab-bill_of_quantity_item_edit_value',
    props:  {
        bill_of_quantity_item: {
            validator: sima.vue.ProxyValidator
        },
        object: {
            validator: sima.vue.ProxyValidator
        },
        column_name: String,
        value: [String, Number],
        display_value: [String, Number],
        column_number_field_params: {
            type: Object,
            default: function(){
                return {
                    precision: 20,
                    scale: 2
                };
            }
        },
        edit_mode: {
            type: Boolean,
            default: false
        }
    },
    data: function () {
        return {
            local_value: this.value,
            old_value: null,
            is_edit: false
        };
    },
    computed: {
        display_value_calc: function() {
            return !sima.isEmpty(this.display_value) ? this.display_value : this.value;
        }
    },
    watch: {
        value: function () {
            this.local_value = this.value;
        }
    },
    mounted: function(){
        var _this = this;

        $(this.$el).on('destroyed', function(){
            _this.$destroy();
        });
    },
    methods: {
        toggleEdit: function() {
            if(this.edit_mode)
            {
                var _this = this;
                this.is_edit = !this.is_edit;
                if (this.is_edit)
                {
                    this.old_value = this.local_value;
                    Vue.nextTick(function() {
                        $(_this.$refs.edit_input).numberField('init', _this.column_number_field_params);
                        _this.$refs.edit_input.focus();
                    });
                }
                else
                {
                    this.old_value = null;
                }
            }
        },
        saveEdit: function(event) {
            if(this.edit_mode && this.is_edit)
            {
                if (this.old_value !== this.local_value)
                {
                    //mora da se formatira value pomocu resetValue metode, jer je moguce da korisnik drzi pritisnuto dugme na tastaturi, recimo 0 
                    //i uz to jos pritisne enter, onda ce local_value ostati tako ogroman broj jer nece stici da se lepo formatira 
                    //posto formatiranje se radi u keyup eventu u numberField.js, ali u ovom slucaju nije stigao da udje u keyup, pa mora rucno da se formatira
                    var input = $(this.$refs.edit_input);
                    
                    input.numberField('resetValue');
                    
                    var new_real_value = input.numberField('getRealValue');

                    sima.ajax.get('buildings/billOfQuantity/saveBillOfQuantityItemColumnValue',{
                        async: true,
                        get_params: {
                            bill_of_quantity_item_id: this.bill_of_quantity_item.id,
                            column: this.column_name,
                            new_value: new_real_value,
                            object_id: typeof this.object !== 'undefined' ? this.object.id : null
                        }
                    });
                }
                this.toggleEdit();
            }
        }
    }
});
