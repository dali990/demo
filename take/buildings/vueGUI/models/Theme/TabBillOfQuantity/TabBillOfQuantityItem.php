
<script type="text/x-template" id="buildings-theme_tab-bill_of_quantity_item">
    <li class="theme-tab-bill-of-quantity-item">
        <div class="column-name table-cell">
            <i class="column-name-arrow"></i>
            <div class="column-name-order">
                {{ bill_of_quantity_item.order }}. 
            </div>
            <div class="column-name-text" v-bind:title="bill_of_quantity_item.name"> 
                {{ bill_of_quantity_item.name }}
                <div class="column-name-options" v-show="edit_mode">
                    <i 
                        class="edit-item fas fa-pen-square" 
                        v-if="can_edit"
                        v-on:click="editItem"
                        title="<?=Yii::t('BuildingsModule.BillOfQuantityItem', 'EditItem')?>"
                    ></i>
                    <i 
                        class="delete-item fas fa-trash-alt" 
                        v-if="can_delete"
                        v-on:click="deleteItem"
                        title="<?=Yii::t('BuildingsModule.BillOfQuantityItem', 'DeleteItem')?>"
                    ></i>
                </div>
            </div>
            <i 
                class="column-name-note fas fa-circle" 
                v-if="bill_of_quantity_item.note !== ''"
                v-sima-tooltip="{
                    title: bill_of_quantity_item.note,
                    placement: 'top-center',
                    offset: 10
                }"
            ></i>
        </div>
        <div class="column-measurement_unit table-cell">
            {{ bill_of_quantity_item.measurement_unit.DisplayName }} 
        </div>
        <buildings-theme_tab-bill_of_quantity_item_edit_value 
            v-if="!bill_of_quantity.has_object_value_per_unit && bill_of_quantity.objects_count !== 1" 
            class="column-value_per_unit table-cell _align-right"
            v-bind:bill_of_quantity_item="bill_of_quantity_item"
            column_name="value_per_unit"
            v-bind:value="bill_of_quantity_item.value_per_unit"
            v-bind:display_value="bill_of_quantity_item.value_per_unit_display"
            v-bind:column_number_field_params="{precision:20, scale:10}"
            v-bind:edit_mode="edit_mode"
            v-bind:style="{width: max_value_per_unit_width + 'px'}"
        >
        </buildings-theme_tab-bill_of_quantity_item_edit_value>
        <buildings-theme_tab-bill_of_quantity_table_body_object
            v-bind:object="bill_of_quantity.root_object"
            v-bind:bill_of_quantity="bill_of_quantity"
            v-bind:bill_of_quantity_item="bill_of_quantity_item"
            v-bind:edit_mode="edit_mode"
            v-bind:building_construction="building_construction"
            v-bind:building_interim_bill="building_interim_bill"
            v-bind:show_quantity_done_column="show_quantity_done_column"
            v-bind:show_value_done_column="show_value_done_column"
            v-bind:show_percentage_done_column="show_percentage_done_column"
        >
        </buildings-theme_tab-bill_of_quantity_table_body_object>
    </li>
</script>
