/* global Vue, sima */

Vue.component('buildings-theme_tab-bill_of_quantity', {
    template: '#buildings-theme_tab-bill_of_quantity',
    mixins: [sima.buildings.getBillOfQuantityTableCellsWidthComponent()],
    props:  {
        bill_of_quantity: {
            validator: sima.vue.ProxyValidator
        },
        building_construction: {
            validator: sima.vue.ProxyValidator
        },
        building_interim_bill: {
            validator: sima.vue.ProxyValidator
        }
    },
    data: function () {
        return {
            local_edit_mode: false,
            show_quantity_done_column: true,
            show_value_done_column: true,
            show_percentage_done_column: false
        };
    },
    computed: {
        bill_of_quantity_root_group: function() {
            return this.bill_of_quantity.root_group;
        },
        header_scroll_style: function() {
            return {
                width: sima.getScrollbarWidth() + 'px'
            };
        },
        is_building_construction: function() {
            return typeof this.building_construction !== 'undefined' && this.building_construction !== null;
        },
        is_building_interim_bill: function() {
            return typeof this.building_interim_bill !== 'undefined' && this.building_interim_bill !== null;
        },
        edit_mode: function() {
            return this.local_edit_mode && !this.bill_of_quantity.locked.value;
        },
        edit_mode_display: function() {
            return this.local_edit_mode ? sima.translate('BillOfQuantityEditModeOff') : sima.translate('BillOfQuantityEditModeOn');
        }
    },
    watch: {
        bill_of_quantity_root_group: () => {}
    },
    mounted: function(){
        var _this = this;

        setTimeout(function() {
            sima.layout.allignObject($(_this.$el).parents('.sima-layout-panel:first'));
        }, 0);

        $(this.$el).on('destroyed', function(){
            _this.$destroy();
        });
    },
    updated: function() {
        var _this = this;

        setTimeout(function() {
            sima.layout.allignObject($(_this.$el).parents('.sima-layout-panel:first'));
        }, 0);
    },
    methods: {
        addGroup: function() {
            sima.model.form('BillOfQuantityItemGroup', '', {
                formName: 'withoutParent',
                init_data: {
                    BillOfQuantityItemGroup: {
                        parent_id: {
                            init: this.bill_of_quantity_root_group.id,
                            disabled: true
                        },
                        bill_of_quantity_id: {
                            init: this.bill_of_quantity.id,
                            disabled: true
                        }
                    }
                }
            });
        },
        cloneBillOfQuantity: function() {
            var _this = this;

            sima.ajax.get('buildings/billOfQuantity/getBillOfQuantityInitDataForClone', {
                get_params: {
                    bill_of_quantity_id: this.bill_of_quantity.id
                },
                async: true,
                success_function: function(response) {
                    sima.model.form('BillOfQuantity', '', {
                        formName: 'forClone',
                        init_data: {
                            BillOfQuantity: response.init_data
                        },
                        onSave: function(response) {
                            if (!sima.isEmpty(response.model_id))
                            {
                                sima.ajaxLong.start('buildings/billOfQuantity/cloneBillOfQuantity', {
                                    data: {
                                        from_bill_of_quantity_id: _this.bill_of_quantity.id,
                                        to_bill_of_quantity_id: response.model_id
                                    },
                                    showProgressBar: $(_this.$el)
                                });
                            }
                        }
                    });
                }
            });
        },
        setBuildingInterimBillStatus: function(status) {
            if (this.is_building_interim_bill)
            {
                sima.ajax.get('buildings/buildingConstruction/setBuildingInterimBillStatus',{
                    async: true,
                    get_params: {
                        building_interim_bill_id: this.building_interim_bill.id,
                        status: status
                    }
                });
            }
        },
        confirmAllZeroMeasurementSheets: function() {
            if (this.is_building_interim_bill)
            {
                sima.ajax.get('buildings/buildingConstruction/confirmAllZeroMeasurementSheets',{
                    async: true,
                    get_params: {
                        building_interim_bill_id: this.building_interim_bill.id
                    }
                });
            }
        }
    }
});
