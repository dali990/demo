
<script type="text/x-template" id="buildings-theme_tab-bill_of_quantity_table_header_object">
    
    <div class="theme-tab-bill-of-quantity-header-object" v-bind:class="{_has_only_root_object: bill_of_quantity.objects_count === 1}">
        <div class="theme-tab-bill-of-quantity-header-object-name" v-if="bill_of_quantity.objects_count !== 1">
            {{ object.name }}
        </div>
        <div class="theme-tab-bill-of-quantity-header-object-children">
            <template
                v-if="object.children_count > 0"
            >
                <buildings-theme_tab-bill_of_quantity_table_header_object
                    v-for="child in object.children"
                    v-bind:key="child.id"
                    v-bind:object="child"
                    v-bind:bill_of_quantity="bill_of_quantity"
                    v-bind:building_construction="building_construction"
                    v-bind:building_interim_bill="building_interim_bill"
                    v-bind:show_quantity_done_column="show_quantity_done_column"
                    v-bind:show_value_done_column="show_value_done_column"
                    v-bind:show_percentage_done_column="show_percentage_done_column"
                >
                </buildings-theme_tab-bill_of_quantity_table_header_object>
                <div class="column-value table-cell _no-border" v-bind:style="{width: max_value_width + 'px'}">
                    <?=Yii::t('BuildingsModule.BillOfQuantityItemGroup', 'Total')?>
                </div>
            </template>
            <template
                v-else
            >
                <div class="column-quantity table-cell" v-bind:style="{width: max_quantity_width + 'px'}">
                    <?=Yii::t('BuildingsModule.BillOfQuantityItem', 'Quantity')?>
                </div>
                <div 
                    v-if="has_object_value_per_unit || bill_of_quantity.objects_count === 1" 
                    class="column-value_per_unit table-cell"
                    v-bind:style="{width: max_value_per_unit_for_object_width + 'px'}"
                >
                    <?=Yii::t('BuildingsModule.BillOfQuantityItem', 'ValuePerUnit')?>
                </div>
                <div 
                    class="column-value table-cell"
                    v-bind:class="{'_no-border-right': !is_building_construction || 
                                                        (is_building_interim_bill && !show_quantity_done_column && !show_value_done_column && !show_percentage_done_column)}"
                    v-bind:style="{width: max_value_width + 'px'}"
                >
                    <?=Yii::t('BuildingsModule.BillOfQuantityItem', 'Value')?>
                </div>
                <template
                    v-if="is_building_construction && !is_building_interim_bill"
                >
                    <div class="column-quantity_done table-cell" v-bind:style="{width: max_quantity_done_width + 'px'}">
                        <?=Yii::t('BuildingsModule.BillOfQuantityItem', 'QuantityDone')?>
                    </div>
                    <div class="column-percentage_done table-cell _no-border-right">
                        <?=Yii::t('BuildingsModule.BillOfQuantityItem', 'PercentageDone')?>
                    </div>
                </template>
                <template
                    v-if="is_building_interim_bill"
                >
                    <div v-if="has_building_interim_bill_visible_columns" class="header-column-interim-bill">
                        <div class="header-column-interim-bill-head table-cell _interim-bill _total">
                            <?=Yii::t('BuildingsModule.BuildingInterimBill', 'TotalDone')?>
                        </div>
                        <div class="header-column-interim-bill-body">
                            <div 
                                v-if="show_quantity_done_column"
                                class="column-quantity_done table-cell _interim-bill _total" 
                                v-bind:style="{width: max_building_interim_bill_total_quantity_done_width + 'px'}"
                            >
                                <?=Yii::t('BuildingsModule.BuildingInterimBill', 'Quantity')?>
                            </div>
                            <div 
                                v-if="show_value_done_column"
                                class="column-value_done table-cell _interim-bill _total"
                                v-bind:style="{width: max_building_interim_bill_total_value_done_width + 'px'}"
                            >
                                <?=Yii::t('BuildingsModule.BuildingInterimBill', 'Total')?>
                            </div>
                            <div 
                                v-if="show_percentage_done_column"
                                class="column-percentage_done table-cell _interim-bill _total"
                            >
                                <?=Yii::t('BuildingsModule.BuildingInterimBill', 'Percentage')?>
                            </div>
                        </div>
                    </div>
                    <div v-if="has_building_interim_bill_visible_columns" class="header-column-interim-bill">
                        <div class="header-column-interim-bill-head table-cell _interim-bill _pre">
                            <?=Yii::t('BuildingsModule.BuildingInterimBill', 'PreDone')?>
                        </div>
                        <div class="header-column-interim-bill-body">
                            <div
                                v-if="show_quantity_done_column"
                                class="column-quantity_done table-cell _interim-bill _pre" 
                                v-bind:style="{width: max_building_interim_bill_pre_quantity_done_width + 'px'}"
                            >
                                <?=Yii::t('BuildingsModule.BuildingInterimBill', 'Quantity')?>
                            </div>
                            <div 
                                v-if="show_value_done_column"
                                class="column-value_done table-cell _interim-bill _pre"
                                v-bind:style="{width: max_building_interim_bill_pre_value_done_width + 'px'}"
                            >
                                <?=Yii::t('BuildingsModule.BuildingInterimBill', 'Total')?>
                            </div>
                            <div 
                                v-if="show_percentage_done_column"
                                class="column-percentage_done table-cell _interim-bill _pre"
                            >
                                <?=Yii::t('BuildingsModule.BuildingInterimBill', 'Percentage')?>
                            </div>
                        </div>
                    </div>
                    <div v-if="has_building_interim_bill_visible_columns" class="header-column-interim-bill">
                        <div class="header-column-interim-bill-head table-cell _interim-bill _current _no-border-right">
                            <?=Yii::t('BuildingsModule.BuildingInterimBill', 'CurrentDone')?>
                        </div>
                        <div class="header-column-interim-bill-body">
                            <div 
                                v-if="show_quantity_done_column"
                                class="column-quantity_done table-cell _interim-bill _current" 
                                v-bind:class="{'_no-border-right': show_quantity_done_column && !show_value_done_column && !show_percentage_done_column}"
                                v-bind:style="{width: max_building_interim_bill_quantity_done_width + 'px'}"
                            >
                                <?=Yii::t('BuildingsModule.BuildingInterimBill', 'Quantity')?>
                            </div>
                            <div 
                                v-if="show_value_done_column"
                                class="column-value_done table-cell _interim-bill _current"
                                v-bind:class="{'_no-border-right': show_value_done_column && !show_percentage_done_column}"
                                v-bind:style="{width: max_building_interim_bill_value_done_width + 'px'}"
                            >
                                <?=Yii::t('BuildingsModule.BuildingInterimBill', 'Total')?>
                            </div>
                            <div 
                                v-if="show_percentage_done_column"
                                class="column-percentage_done table-cell _interim-bill _current"
                                v-bind:class="{'_no-border-right': show_percentage_done_column}"
                            >
                                <?=Yii::t('BuildingsModule.BuildingInterimBill', 'Percentage')?>
                            </div>
                        </div>
                    </div>
                </template>
            </template>
        </div>
    </div>

</script>
