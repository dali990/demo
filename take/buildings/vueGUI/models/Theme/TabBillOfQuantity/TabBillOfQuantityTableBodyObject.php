
<script type="text/x-template" id="buildings-theme_tab-bill_of_quantity_table_body_object">
    
    <div class="theme-tab-bill-of-quantity-body-object">
        <template
            v-if="object.children_count > 0"
        >
            <buildings-theme_tab-bill_of_quantity_table_body_object
                v-for="child in object.children"
                v-bind:key="child.id"
                v-bind:object="child"
                v-bind:bill_of_quantity="bill_of_quantity"
                v-bind:bill_of_quantity_item_group="bill_of_quantity_item_group"
                v-bind:bill_of_quantity_item="bill_of_quantity_item"
                v-bind:edit_mode="edit_mode"
                v-bind:building_construction="building_construction"
                v-bind:building_interim_bill="building_interim_bill"
                v-bind:show_quantity_done_column="show_quantity_done_column"
                v-bind:show_value_done_column="show_value_done_column"
                v-bind:show_percentage_done_column="show_percentage_done_column"
            >
            </buildings-theme_tab-bill_of_quantity_table_body_object>
            <buildings-theme_tab-bill_of_quantity_item_edit_value 
                class="column-value table-cell _align-right"
                v-bind:value="value"
                v-bind:edit_mode="false"
                v-bind:style="{width: max_value_width + 'px'}"
            >
            </buildings-theme_tab-bill_of_quantity_item_edit_value>
        </template>
        <template
            v-else
        >
            <buildings-theme_tab-bill_of_quantity_item_edit_value 
                class="column-quantity table-cell _align-right"
                v-bind:bill_of_quantity_item="bill_of_quantity_item"
                v-bind:object="object"
                column_name="quantity"
                v-bind:value="quantity"
                v-bind:edit_mode="edit_mode"
                v-bind:style="{width: max_quantity_width + 'px'}"
            >
            </buildings-theme_tab-bill_of_quantity_item_edit_value>
            <buildings-theme_tab-bill_of_quantity_item_edit_value 
                v-if="object.bill_of_quantity.has_object_value_per_unit || bill_of_quantity.objects_count === 1" 
                class="column-value_per_unit table-cell _align-right"
                v-bind:bill_of_quantity_item="bill_of_quantity_item"
                v-bind:object="object" 
                column_name="value_per_unit"
                v-bind:value="getValuePerUnit()"
                v-bind:display_value="getValuePerUnit(true)"
                v-bind:column_number_field_params="{precision:20, scale:10}"
                v-bind:edit_mode="edit_mode"
                v-bind:style="{width: max_value_per_unit_for_object_width + 'px'}"
            >
            </buildings-theme_tab-bill_of_quantity_item_edit_value>
            <buildings-theme_tab-bill_of_quantity_item_edit_value 
                class="column-value table-cell _align-right"
                v-bind:value="value"
                v-bind:edit_mode="false"
                v-bind:style="{width: max_value_width + 'px'}"
            >
            </buildings-theme_tab-bill_of_quantity_item_edit_value>
            <template
                v-if="is_building_construction && !is_building_interim_bill"
            >
                <div class="column-quantity_done table-cell _align-right" v-bind:style="{width: max_quantity_done_width + 'px'}">
                    {{ quantity_done | formatNumber }}
                </div>
                <div class="column-percentage_done table-cell _align-right">
                    {{ percentage_done | formatNumber }}
                </div>
            </template>
            <template
                v-if="is_building_interim_bill"
            >
                <div 
                    v-if="show_quantity_done_column"
                    class="column-quantity_done table-cell _interim-bill _total _align-right" 
                    v-bind:style="{width: max_building_interim_bill_total_quantity_done_width + 'px'}"
                >
                    {{ interim_bill_total_quantity_done | formatNumber }}
                </div>
                <div 
                    v-if="show_value_done_column"
                    class="column-value_done table-cell _interim-bill _total _align-right"
                    v-bind:style="{width: max_building_interim_bill_total_value_done_width + 'px'}"
                >
                    {{ interim_bill_total_value_done | formatNumber }}
                </div>
                <div 
                    v-if="show_percentage_done_column"
                    class="column-percentage_done table-cell _interim-bill _total _align-right"
                >
                    {{ interim_bill_total_percentage_done | formatNumber }}
                </div>
            
                <div
                    v-if="show_quantity_done_column"
                    class="column-quantity_done table-cell _interim-bill _pre _align-right" 
                    v-bind:style="{width: max_building_interim_bill_pre_quantity_done_width + 'px'}"
                >
                    {{ interim_bill_pre_quantity_done | formatNumber }}
                </div>
                <div 
                    v-if="show_value_done_column"
                    class="column-value_done table-cell _interim-bill _pre _align-right"
                    v-bind:style="{width: max_building_interim_bill_pre_value_done_width + 'px'}"
                >
                    {{ interim_bill_pre_value_done | formatNumber }}
                </div>
                <div 
                    v-if="show_percentage_done_column"
                    class="column-percentage_done table-cell _interim-bill _pre _align-right"
                >
                    {{ interim_bill_pre_percentage_done | formatNumber }}
                </div>
                
                <div 
                    v-if="show_quantity_done_column"
                    class="column-quantity_done table-cell _interim-bill _current _align-right" 
                    v-bind:class="{'_edit-interim-bill': building_interim_bill.can_change_building_measurement_sheet && is_item}"
                    v-bind:style="{width: max_building_interim_bill_quantity_done_width + 'px'}"
                    v-on:click="openBuildingMeasurementSheet"
                    title="<?=Yii::t('BuildingsModule.BuildingMeasurementSheet', 'AddMeasurementSheet')?>"
                >
                    <i 
                        v-if="is_item"
                        class="fas fa-circle"
                        v-bind:style="{
                            'flex-grow': 1,
                            'text-align': 'left',
                            color: interim_bill_measurement_sheet_status_color
                        }"
                        v-bind:title="interim_bill_measurement_sheet_status_title"
                    ></i>
                    <i 
                        v-if="is_item_group"
                        class="fas fa-circle"
                        v-bind:style="{
                            'flex-grow': 1,
                            'text-align': 'left',
                            color: interim_bill_measurement_sheet_group_status_color
                        }"
                        v-bind:title="interim_bill_measurement_sheet_group_status_title"
                    ></i>
                    {{ interim_bill_quantity_done | formatNumber }}
                </div>
                <div 
                    v-if="show_value_done_column"
                    class="column-value_done table-cell _interim-bill _current _align-right"
                    v-bind:style="{width: max_building_interim_bill_value_done_width + 'px'}"
                >
                    {{ interim_bill_value_done | formatNumber }}
                </div>
                <div 
                    v-if="show_percentage_done_column"
                    class="column-percentage_done table-cell _interim-bill _current _align-right"
                >
                    {{ interim_bill_percentage_done | formatNumber }}
                </div>
            </template>
        </template>
    </div>

</script>
