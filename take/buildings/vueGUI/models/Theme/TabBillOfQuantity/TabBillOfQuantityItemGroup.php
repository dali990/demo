
<script type="text/x-template" id="buildings-theme_tab-bill_of_quantity_item_group">
    <li class="theme-tab-bill-of-quantity-item-group">
        <div 
            class="theme-tab-bill-of-quantity-item-group-wrap"
            v-bind:class="group_classes"
        >
            <div v-if="!is_root_group" class="column-name table-cell">
                <i v-bind:class="arrow_classes" v-on:click="onArrowClick"></i>
                <div class="column-name-order">
                    {{ bill_of_quantity_item_group.order }}. 
                </div>
                <div class="column-name-text" v-bind:title="bill_of_quantity_item_group.title">
                    {{ bill_of_quantity_item_group.title }}
                    <div class="column-name-options" v-show="edit_mode">
                        <i 
                            class="edit-group fas fa-pen-square" 
                            v-if="can_edit"
                            v-on:click="editGroup"
                            title="<?=Yii::t('BuildingsModule.BillOfQuantityItemGroup', 'EditGroup')?>"
                        ></i>
                        <i 
                            class="import-group fas fa-file-upload" 
                            v-if="can_edit"
                            v-on:click="openImportGroup"
                            title="<?=Yii::t('BuildingsModule.BillOfQuantityItemGroup', 'Import')?>"
                        ></i>
                        <i 
                            class="delete-group fas fa-trash-alt" 
                            v-if="can_delete"
                            v-on:click="deleteGroup"
                            title="<?=Yii::t('BuildingsModule.BillOfQuantityItemGroup', 'DeleteGroup')?>"
                        ></i>
                        <i 
                            class="add-sub-group fas fa-plus-square" 
                            v-on:click="addSubGroup"
                            title="<?=Yii::t('BuildingsModule.BillOfQuantityItemGroup', 'AddSubGroup')?>"
                        ></i>
                        <i 
                            class="add-item fas fa-plus-square" 
                            v-on:click="addItem"
                            title="<?=Yii::t('BuildingsModule.BillOfQuantityItemGroup', 'AddItem')?>"
                        ></i>
                    </div>
                </div>
                <i 
                    class="column-name-note fas fa-circle" 
                    v-if="bill_of_quantity_item_group.note !== ''"
                    v-sima-tooltip="{
                        title: bill_of_quantity_item_group.note,
                        placement: 'top-center',
                        offset: 10
                    }"
                ></i>
            </div>
            <div v-else class="column-name table-cell">
                <?=Yii::t('BuildingsModule.BillOfQuantityItemGroup', 'Total')?>
            </div>
            <div class="column-measurement_unit table-cell">
                <template
                    v-if="(!expanded || is_root_group) && bill_of_quantity_item_group.has_all_items_same_measurement_unit"
                >
                    {{ bill_of_quantity_item_group.measurement_unit }}
                </template>
            </div>
            <div 
                v-if="!bill_of_quantity.has_object_value_per_unit && bill_of_quantity.objects_count !== 1" 
                class="column-value_per_unit table-cell _align-right"
                v-bind:title="(((!expanded || is_root_group) && bill_of_quantity_item_group.has_all_items_same_measurement_unit) ? bill_of_quantity_item_group.value_per_unit : '') | formatNumber"
                v-bind:style="{width: max_value_per_unit_width + 'px'}"
            >
                <template
                    v-if="(!expanded || is_root_group) && bill_of_quantity_item_group.has_all_items_same_measurement_unit"
                >
                    ~ {{ bill_of_quantity_item_group.value_per_unit_display | formatNumber }}
                </template>
            </div>
            <buildings-theme_tab-bill_of_quantity_table_body_object
                v-bind:object="bill_of_quantity.root_object"
                v-bind:bill_of_quantity="bill_of_quantity"
                v-bind:bill_of_quantity_item_group="!expanded ? bill_of_quantity_item_group : null"
                v-bind:edit_mode="false"
                v-bind:building_construction="building_construction"
                v-bind:building_interim_bill="building_interim_bill"
                v-bind:show_quantity_done_column="show_quantity_done_column"
                v-bind:show_value_done_column="show_value_done_column"
                v-bind:show_percentage_done_column="show_percentage_done_column"
            >
            </buildings-theme_tab-bill_of_quantity_table_body_object>
        </div>
        <ul 
            v-if="loaded"
            v-show="loaded && expanded"
        >
            <buildings-theme_tab-bill_of_quantity_item_group
                v-for="group in children_groups"
                v-bind:key="'ig_' + group.id"
                v-bind:bill_of_quantity_item_group="group"
                v-bind:bill_of_quantity="bill_of_quantity"
                v-bind:edit_mode="edit_mode"
                v-bind:building_construction="building_construction"
                v-bind:building_interim_bill="building_interim_bill"
                v-bind:show_quantity_done_column="show_quantity_done_column"
                v-bind:show_value_done_column="show_value_done_column"
                v-bind:show_percentage_done_column="show_percentage_done_column"
            >
            </buildings-theme_tab-bill_of_quantity_item_group>
            
            <buildings-theme_tab-bill_of_quantity_item
                v-for="item in children_items"
                v-bind:key="'i_' + item.id"
                v-bind:bill_of_quantity_item="item"
                v-bind:bill_of_quantity="bill_of_quantity"
                v-bind:edit_mode="edit_mode"
                v-bind:building_construction="building_construction"
                v-bind:building_interim_bill="building_interim_bill"
                v-bind:show_quantity_done_column="show_quantity_done_column"
                v-bind:show_value_done_column="show_value_done_column"
                v-bind:show_percentage_done_column="show_percentage_done_column"
            >
            </buildings-theme_tab-bill_of_quantity_item>
            
            <div class="theme-tab-bill-of-quantity-item-group-wrap _group-total-row">
                <div class="column-name table-cell"><?=Yii::t('BuildingsModule.BillOfQuantityItemGroup', 'Total')?></div>
                <div class="column-measurement_unit table-cell">
                    <template
                        v-if="bill_of_quantity_item_group.has_all_items_same_measurement_unit"
                    >
                        {{ bill_of_quantity_item_group.measurement_unit }}
                    </template>
                </div>
                <div 
                    v-if="!bill_of_quantity.has_object_value_per_unit && bill_of_quantity.objects_count !== 1" 
                    class="column-value_per_unit table-cell _align-right"
                    v-bind:title="(bill_of_quantity_item_group.has_all_items_same_measurement_unit ? bill_of_quantity_item_group.value_per_unit : '') | formatNumber"
                    v-bind:style="{width: max_value_per_unit_width + 'px'}"
                >
                    <template
                        v-if="bill_of_quantity_item_group.has_all_items_same_measurement_unit"
                    >
                        ~ {{ bill_of_quantity_item_group.value_per_unit_display | formatNumber }}
                    </template>
                </div>
                <buildings-theme_tab-bill_of_quantity_table_body_object
                    v-bind:object="bill_of_quantity.root_object"
                    v-bind:bill_of_quantity="bill_of_quantity"
                    v-bind:bill_of_quantity_item_group="bill_of_quantity_item_group"
                    v-bind:edit_mode="false"
                    v-bind:building_construction="building_construction"
                    v-bind:building_interim_bill="building_interim_bill"
                    v-bind:show_quantity_done_column="show_quantity_done_column"
                    v-bind:show_value_done_column="show_value_done_column"
                    v-bind:show_percentage_done_column="show_percentage_done_column"
                >
                </buildings-theme_tab-bill_of_quantity_table_body_object>
            </div>
        </ul>
    </li>
</script>
