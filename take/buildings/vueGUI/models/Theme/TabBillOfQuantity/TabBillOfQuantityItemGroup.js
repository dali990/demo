/* global Vue, sima */

Vue.component('buildings-theme_tab-bill_of_quantity_item_group', {
    template: '#buildings-theme_tab-bill_of_quantity_item_group',
    mixins: [sima.buildings.getBillOfQuantityTableCellsWidthComponent()],
    props:  {
        bill_of_quantity_item_group: {
            validator: sima.vue.ProxyValidator
        },
        bill_of_quantity: {
            validator: sima.vue.ProxyValidator
        },
        edit_mode: {
            type: Boolean,
            default: false
        },
        building_construction: {
            validator: sima.vue.ProxyValidator
        },
        building_interim_bill: {
            validator: sima.vue.ProxyValidator
        },
        show_quantity_done_column: Boolean,
        show_value_done_column: Boolean,
        show_percentage_done_column: Boolean
    },
    data: function () {
        return {
            loaded: false,
            expanded: false
        };
    },
    computed: {
        children_groups: function() {
            return this.bill_of_quantity_item_group.children;
        },
        children_items: function() {
            return this.bill_of_quantity_item_group.items;
        },
        has_children: function() {
            return this.bill_of_quantity_item_group.children_count > 0 || this.bill_of_quantity_item_group.items_count > 0;
        },
        arrow_classes: function() {
            return {
                'column-name-arrow': true,
                '_visible': this.has_children,
                'fa-lg': true,
                'fas fa-caret-right': !this.expanded,
                'fas fa-caret-down': this.expanded
            };
        },
        group_classes: function() {
            return {
                '_expanded': this.expanded,
                '_group-name-row': !this.is_root_group,
                '_group-total-row': this.is_root_group
            };
        },
        curr_user_model_options: function() {
            return this.bill_of_quantity_item_group.curr_user_model_options;
        },
        can_edit: function() {
            return $.inArray('form', this.curr_user_model_options) !== -1;
        },
        can_delete: function() {
            return $.inArray('delete', this.curr_user_model_options) !== -1;
        },
        is_root_group: function() {
            return sima.isEmpty(this.bill_of_quantity_item_group.parent_id);
        }
    },
    mounted: function(){
        var _this = this;
        
        setTimeout(function() {
            sima.layout.allignObject($(_this.$el).parents('.sima-layout-panel:first').parent());
        }, 0);
        
        $(this.$el).on('destroyed', function(){
            _this.$destroy();
        });
    },
    updated: function() {
        var _this = this;

        setTimeout(function() {
            sima.layout.allignObject($(_this.$el).parents('.sima-layout-panel:first').parent());
        }, 0);
    },
    methods: {
        onArrowClick: function() {
            if (!this.loaded)
            {
                this.loaded = true;
            }
            
            this.expanded = !this.expanded;
        },
        editGroup: function() {
            sima.model.form('BillOfQuantityItemGroup', this.bill_of_quantity_item_group.id);
        },
        deleteGroup: function() {
            sima.model.remove('BillOfQuantityItemGroup', this.bill_of_quantity_item_group.id);
        },
        addSubGroup: function() {
            var _this = this;

            sima.model.form('BillOfQuantityItemGroup', '', {
                init_data: {
                    BillOfQuantityItemGroup: {
                        bill_of_quantity_id: {
                            init: this.bill_of_quantity.id,
                            disabled: true
                        },
                        parent_id: {
                            init: this.bill_of_quantity_item_group.id,
                            disabled: true
                        }
                    }
                },
                onSave: function() {
                    _this.expandGroup();
                }
            });
        },
        addItem: function() {
            var _this = this;

            sima.model.form('BillOfQuantityItem', '', {
                init_data: {
                    BillOfQuantityItem: {
                        group_id: {
                            init: _this.bill_of_quantity_item_group.id,
                            disabled: true
                        }
                    }
                },
                onSave: function(response) {
                    _this.expandGroup();
                }
            });
        },
        expandGroup: function() {
            if (!this.loaded)
            {
                this.loaded = true;
            }

            if (!this.expanded)
            {
                this.expanded = true;
            }
        },
        openImportGroup: function() {
            var props = {
                bill_of_quantity: this.bill_of_quantity,
                bill_of_quantity_item_group: this.bill_of_quantity_item_group
            };
            sima.dialog.openVueComponent('import-bill-of-quantity', props, sima.translate('ImportBillOfQuantityGroup'), {fullscreen: true});
        }
    }
});
