/* global Vue, sima */

Vue.component('buildings-theme_tab-bill_of_quantity_item', {
    template: '#buildings-theme_tab-bill_of_quantity_item',
    mixins: [sima.buildings.getBillOfQuantityTableCellsWidthComponent()],
    props:  {
        bill_of_quantity_item: {
            validator: sima.vue.ProxyValidator
        },
        bill_of_quantity: {
            validator: sima.vue.ProxyValidator
        },
        edit_mode: {
            type: Boolean,
            default: false
        },
        building_construction: {
            validator: sima.vue.ProxyValidator
        },
        building_interim_bill: {
            validator: sima.vue.ProxyValidator
        },
        show_quantity_done_column: Boolean,
        show_value_done_column: Boolean,
        show_percentage_done_column: Boolean
    },
    data: function () {
        return {
            
        };
    },
    computed: {
        curr_user_model_options: function() {
            return this.bill_of_quantity_item.curr_user_model_options;
        },
        can_edit: function() {
            return $.inArray('form', this.curr_user_model_options) !== -1;
        },
        can_delete: function() {
            return $.inArray('delete', this.curr_user_model_options) !== -1;
        }
    },
    mounted: function(){
        var _this = this;

        $(this.$el).on('destroyed', function(){
            _this.$destroy();
        });
    },
    methods: {
        editItem: function() {
            sima.model.form('BillOfQuantityItem', this.bill_of_quantity_item.id);
        },
        deleteItem: function() {
            sima.model.remove('BillOfQuantityItem', this.bill_of_quantity_item.id);
        }
    }
});
