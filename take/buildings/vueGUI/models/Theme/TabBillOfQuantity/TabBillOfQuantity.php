
<script type="text/x-template" id="buildings-theme_tab-bill_of_quantity">
    <div class="theme-tab-bill-of-quantity sima-layout-panel _horizontal" data-sima-layout-init='{"width_cut":20, "height_cut":20}'>
        <div class="sima-layout-fixed-panel" style="display: flex; align-items: center;">
            <h2 
                class="sima-text-dots" style="flex-grow: 1;"
                v-bind:title="bill_of_quantity.DisplayName"
            >
                <?=Yii::t('BuildingsModule.BillOfQuantity', 'BillOfQuantityShort') . ': '?> {{ bill_of_quantity.DisplayName }} 
                <sima-model-options style="font-size: 14px;" v-bind:model="bill_of_quantity"></sima-model-options>
            </h2>
            <div v-if="!is_building_construction">
                <sima-button
                    v-on:click="cloneBillOfQuantity"
                    v-if="bill_of_quantity.has_change_access"
                    title="<?=Yii::t('BuildingsModule.BillOfQuantity', 'CloneBillOfQuantity')?>"
                >
                    <span class="sima-old-btn-title">
                        <?=Yii::t('BuildingsModule.BillOfQuantity', 'CloneBillOfQuantity')?>
                    </span>
                </sima-button>
            </div>
        </div>
        <div class="theme-tab-bill-of-quantity-options sima-layout-fixed-panel">
            <span class="locked">
                <?=Yii::t('BuildingsModule.BillOfQuantity', 'Locked')?>:
                <sima-status-span v-bind:status="bill_of_quantity.locked"></sima-status-span>
            </span>
            <sima-button
                class="edit-mode" 
                v-bind:class="edit_mode ? '_pressed active' : ''"
                v-on:click="local_edit_mode = !local_edit_mode"
                v-if="bill_of_quantity.has_change_access && !bill_of_quantity.locked.value"
            >
                <span class="sima-old-btn-title">
                    {{ edit_mode_display }}
                </span>
            </sima-button>
            <i 
                v-if="edit_mode"
                class="add-group fas fa-plus-square" 
                v-on:click="addGroup"
                title="<?=Yii::t('BuildingsModule.BillOfQuantity', 'AddGroup')?>"
            >
                <span class="add-group-text"><?=Yii::t('BuildingsModule.BillOfQuantity', 'AddGroup')?></span>
            </i>
            <template v-if="is_building_interim_bill && building_interim_bill.has_admin_privilege">
                <sima-button 
                    v-if="building_interim_bill.status === 'PAID'"
                    v-on:click="setBuildingInterimBillStatus('VALIDATED')"
                    title="<?=Yii::t('BuildingsModule.BuildingInterimBill', 'Validated')?>"
                >
                    <span class="sima-old-btn-title">
                        <?=Yii::t('BuildingsModule.BuildingInterimBill', 'Validated')?>
                    </span>
                </sima-button>
                <sima-button 
                    v-else-if="building_interim_bill.status === 'VALIDATED'"
                    v-on:click="setBuildingInterimBillStatus('DELIVERED')"
                    title="<?=Yii::t('BuildingsModule.BuildingInterimBill', 'Delivered')?>"
                >
                    <span class="sima-old-btn-title">
                        <?=Yii::t('BuildingsModule.BuildingInterimBill', 'Delivered')?>
                    </span>
                </sima-button>
                <sima-button 
                    v-else-if="building_interim_bill.status === 'DELIVERED'"
                    v-on:click="setBuildingInterimBillStatus('VALIDATED_MEASUREMENT_SHEETS')"
                    title="<?=Yii::t('BuildingsModule.BuildingInterimBill', 'ValidatedMeasurementSheets')?>"
                >
                    <span class="sima-old-btn-title">
                        <?=Yii::t('BuildingsModule.BuildingInterimBill', 'ValidatedMeasurementSheets')?>
                    </span>
                </sima-button>
                <sima-button 
                    v-else-if="building_interim_bill.status === 'VALIDATED_MEASUREMENT_SHEETS'"
                    v-on:click="setBuildingInterimBillStatus('DELIVERED_MEASUREMENT_SHEETS')"
                    title="<?=Yii::t('BuildingsModule.BuildingInterimBill', 'DeliveredMeasurementSheets')?>"
                >
                    <span class="sima-old-btn-title">
                        <?=Yii::t('BuildingsModule.BuildingInterimBill', 'DeliveredMeasurementSheets')?>
                    </span>
                </sima-button>
                <sima-button 
                    v-else-if="building_interim_bill.status === 'DELIVERED_MEASUREMENT_SHEETS'"
                    v-on:click="setBuildingInterimBillStatus('CONFIRMED_MEASUREMENT_SHEETS')"
                    title="<?=Yii::t('BuildingsModule.BuildingInterimBill', 'ConfirmedMeasurementSheets')?>"
                >
                    <span class="sima-old-btn-title">
                        <?=Yii::t('BuildingsModule.BuildingInterimBill', 'ConfirmedMeasurementSheets')?>
                    </span>
                </sima-button>
            </template>
            <template v-if="is_building_interim_bill && building_interim_bill.can_change_status">
                <sima-button 
                    v-if="
                        (building_interim_bill.status === 'PREPARATION' || building_interim_bill.status === 'WRITTEN_MEASUREMENT_SHEETS') && 
                        building_interim_bill.has_unconfirmed_zero_measurement_sheets === true
                    "
                    v-on:click="confirmAllZeroMeasurementSheets"
                    v-bind:security_question="true"
                    title="<?=Yii::t('BuildingsModule.BuildingInterimBill', 'ConfirmAllZeroMeasurementSheets')?>"
                >
                    <span class="sima-old-btn-title">
                        <?=Yii::t('BuildingsModule.BuildingInterimBill', 'ConfirmAllZeroMeasurementSheets')?>
                    </span>
                </sima-button>
                <sima-button 
                    v-if="building_interim_bill.status === 'CONFIRMED_MEASUREMENT_SHEETS'"
                    v-on:click="setBuildingInterimBillStatus('DELIVERED_MEASUREMENT_SHEETS')"
                    title="<?=Yii::t('BuildingsModule.BuildingInterimBill', 'DeliveredMeasurementSheets')?>"
                >
                    <span class="sima-old-btn-title">
                        <?=Yii::t('BuildingsModule.BuildingInterimBill', 'DeliveredMeasurementSheets')?>
                    </span>
                </sima-button>
                <sima-button 
                    v-else-if="building_interim_bill.status === 'DELIVERED_MEASUREMENT_SHEETS'"
                    v-on:click="setBuildingInterimBillStatus('VALIDATED_MEASUREMENT_SHEETS')"
                    security_question="<?=Yii::t('BuildingsModule.BuildingInterimBill', 'MakeSureYouCheckValidatedQuantities')?>"
                    title="<?=Yii::t('BuildingsModule.BuildingInterimBill', 'ValidatedMeasurementSheets')?>"
                >
                    <span class="sima-old-btn-title">
                        <?=Yii::t('BuildingsModule.BuildingInterimBill', 'ValidatedMeasurementSheets')?>
                    </span>
                </sima-button>
                <sima-button 
                    v-else-if="building_interim_bill.status === 'VALIDATED_MEASUREMENT_SHEETS'"
                    v-on:click="setBuildingInterimBillStatus('DELIVERED')"
                    title="<?=Yii::t('BuildingsModule.BuildingInterimBill', 'Delivered')?>"
                >
                    <span class="sima-old-btn-title">
                        <?=Yii::t('BuildingsModule.BuildingInterimBill', 'Delivered')?>
                    </span>
                </sima-button>
                <sima-button 
                    v-else-if="building_interim_bill.status === 'DELIVERED'"
                    v-on:click="setBuildingInterimBillStatus('VALIDATED')"
                    title="<?=Yii::t('BuildingsModule.BuildingInterimBill', 'Validated')?>"
                >
                    <span class="sima-old-btn-title">
                        <?=Yii::t('BuildingsModule.BuildingInterimBill', 'Validated')?>
                    </span>
                </sima-button>
                <sima-button 
                    v-else-if="building_interim_bill.status === 'VALIDATED'"
                    v-on:click="setBuildingInterimBillStatus('PAID')"
                    title="<?=Yii::t('BuildingsModule.BuildingInterimBill', 'Paid')?>"
                >
                    <span class="sima-old-btn-title">
                        <?=Yii::t('BuildingsModule.BuildingInterimBill', 'Paid')?>
                    </span>
                </sima-button>
            </template>
            <span v-if="is_building_interim_bill" style="margin-left: auto;">
                <span style="cursor: default;"><?=Yii::t('BuildingsModule.BillOfQuantity', 'ShowQuantityDoneColumn')?></span>
                <label
                    class="sima-switch" 
                    v-bind:class="{checked: show_quantity_done_column}" 
                    v-on:click="show_quantity_done_column=!show_quantity_done_column"
                >
                    <span class="sima-slider round"></span>
                </label>
             </span>
            <span v-if="is_building_interim_bill">
                <span style="cursor: default;"><?=Yii::t('BuildingsModule.BillOfQuantity', 'ShowValueDoneColumn')?></span>
                <label 
                    class="sima-switch" 
                    v-bind:class="{checked: show_value_done_column}" 
                    v-on:click="show_value_done_column=!show_value_done_column"
                >
                    <span class="sima-slider round"></span>
                </label>
            </span>
            <span v-if="is_building_interim_bill">
                <span style="cursor: default;"><?=Yii::t('BuildingsModule.BillOfQuantity', 'ShowPercentageDoneColumn')?></span>
                <label 
                    class="sima-switch" 
                    v-bind:class="{checked: show_percentage_done_column}" 
                    v-on:click="show_percentage_done_column=!show_percentage_done_column"
                >
                    <span class="sima-slider round"></span>
                </label>
            </span>
        </div>
        <div class="sima-layout-panel">
            <div class="theme-tab-bill-of-quantity-table sima-layout-panel _horizontal" style="overflow-x: auto !important;">
                <div class="theme-tab-bill-of-quantity-table-header sima-layout-fixed-panel">
                    <div class="column-name table-cell">
                        <?=Yii::t('BuildingsModule.BillOfQuantityItem', 'Name')?>
                    </div>
                    <div class="column-measurement_unit table-cell">
                        <?=Yii::t('BuildingsModule.BillOfQuantityItem', 'MeasurementUnit')?>
                    </div>
                    <div 
                        v-if="!bill_of_quantity.has_object_value_per_unit && bill_of_quantity.objects_count !== 1" 
                        class="column-value_per_unit table-cell"
                        v-bind:style="{width: max_value_per_unit_width + 'px'}"
                    >
                        <?=Yii::t('BuildingsModule.BillOfQuantityItem', 'ValuePerUnit')?>
                    </div>
                    <buildings-theme_tab-bill_of_quantity_table_header_object
                        class="_last"
                        v-bind:object="bill_of_quantity.root_object"
                        v-bind:bill_of_quantity="bill_of_quantity"
                        v-bind:building_construction="building_construction"
                        v-bind:building_interim_bill="building_interim_bill"
                        v-bind:show_quantity_done_column="show_quantity_done_column"
                        v-bind:show_value_done_column="show_value_done_column"
                        v-bind:show_percentage_done_column="show_percentage_done_column"
                    >
                    </buildings-theme_tab-bill_of_quantity_table_header_object>
                    <div class="header-scroll" v-bind:style="header_scroll_style"></div>
                </div>
                <div class="theme-tab-bill-of-quantity-table-body sima-layout-panel">
                    <buildings-theme_tab-bill_of_quantity_item_group
                        v-for="group in bill_of_quantity.first_level_groups"
                        v-bind:key="group.id"
                        v-bind:bill_of_quantity_item_group="group"
                        v-bind:bill_of_quantity="bill_of_quantity"
                        v-bind:edit_mode="edit_mode"
                        v-bind:building_construction="building_construction"
                        v-bind:building_interim_bill="building_interim_bill"
                        v-bind:show_quantity_done_column="show_quantity_done_column"
                        v-bind:show_value_done_column="show_value_done_column"
                        v-bind:show_percentage_done_column="show_percentage_done_column"
                    >
                    </buildings-theme_tab-bill_of_quantity_item_group>
                    <buildings-theme_tab-bill_of_quantity_item_group
                        v-if="bill_of_quantity.first_level_groups_count > 0"
                        v-bind:bill_of_quantity_item_group="bill_of_quantity.root_group"
                        v-bind:bill_of_quantity="bill_of_quantity"
                        v-bind:edit_mode="false"
                        v-bind:building_construction="building_construction"
                        v-bind:building_interim_bill="building_interim_bill"
                        v-bind:show_quantity_done_column="show_quantity_done_column"
                        v-bind:show_value_done_column="show_value_done_column"
                        v-bind:show_percentage_done_column="show_percentage_done_column"
                    >
                    </buildings-theme_tab-bill_of_quantity_item_group>
                </div>
                <div class="theme-tab-bill-of-quantity-table-footer sima-layout-fixed-panel">
                </div>
            </div>
        </div>
    </div>
</script>
