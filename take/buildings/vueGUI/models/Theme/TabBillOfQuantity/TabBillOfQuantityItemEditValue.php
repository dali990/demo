
<script type="text/x-template" id="buildings-theme_tab-bill_of_quantity_item_edit_value">
    
    <div 
        v-on:click="!is_edit && toggleEdit()" 
        class="theme-tab-bill-of-quantity-item-edit-value"
        v-bind:class="(edit_mode && !is_edit) ? '_clickable' : ''"
        v-bind:title="value | formatNumber"
    >
        <span 
            v-if="!is_edit"
        >
            {{ display_value_calc | formatNumber }}
        </span>
        <input 
            v-else
            type="text"
            ref="edit_input"
            v-on:keyup.enter="saveEdit"
            v-on:blur="saveEdit"
            v-model.lazy="local_value"
        />
    </div>

</script>
