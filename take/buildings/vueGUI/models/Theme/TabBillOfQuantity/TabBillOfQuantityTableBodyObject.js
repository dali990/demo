/* global Vue, sima */

Vue.component('buildings-theme_tab-bill_of_quantity_table_body_object', {
    template: '#buildings-theme_tab-bill_of_quantity_table_body_object',
    mixins: [sima.buildings.getBillOfQuantityTableCellsWidthComponent()],
    props:  {
        object: {
            validator: sima.vue.ProxyValidator
        },
        bill_of_quantity: {
            validator: sima.vue.ProxyValidator
        },
        bill_of_quantity_item_group: {
            validator: sima.vue.ProxyValidator
        },
        bill_of_quantity_item: {
            validator: sima.vue.ProxyValidator
        },
        edit_mode: {
            type: Boolean,
            default: false
        },
        building_construction: {
            validator: sima.vue.ProxyValidator
        },
        building_interim_bill: {
            validator: sima.vue.ProxyValidator
        },
        show_quantity_done_column: Boolean,
        show_value_done_column: Boolean,
        show_percentage_done_column: Boolean
    },
    data: function () {
        return {
            open_building_measurement_sheet_in_progress: false
        };
    },
    computed: {
        is_building_construction: function() {
            return typeof this.building_construction !== 'undefined' && this.building_construction !== null;
        },
        is_building_interim_bill: function() {
            return typeof this.building_interim_bill !== 'undefined' && this.building_interim_bill !== null;
        },
        is_item: function() {
            return typeof this.bill_of_quantity_item !== 'undefined' && this.bill_of_quantity_item !== null;
        },
        item_to_object: function() {
            var item_to_object = this.bill_of_quantity_item.scoped('item_to_objects', {forObject: this.object.id});
            
            return item_to_object.length === 1 ? item_to_object[0] : null;
        },
        is_item_group: function() {
            return typeof this.bill_of_quantity_item_group !== 'undefined' && this.bill_of_quantity_item_group !== null;
        },
        item_group_to_object: function() {
            var item_group_to_object = this.bill_of_quantity_item_group.scoped('item_group_to_objects', {forObject: this.object.id});
            
            return item_group_to_object.length === 1 ? item_group_to_object[0] : null;
        },
        quantity: function() {
            var value = '';
            if (
                    this.is_item_group && 
                    this.item_group_to_object !== null && 
                    this.item_group_to_object.item_group.has_all_items_same_measurement_unit
               )
            {
                value = this.item_group_to_object.quantity;
            }
            else if (this.is_item && this.item_to_object !== null)
            {
                value = this.item_to_object.quantity;
            }
            
            return value;
        },
        value: function() {
            var value = '';
            if (this.is_item_group && this.item_group_to_object !== null)
            {
                value = this.item_group_to_object.value;
            }
            else if (this.is_item && this.item_to_object !== null)
            {
                value = this.item_to_object.value;
            }
            
            return value;
        },
        quantity_done: function() {
            var value = '';
            if (
                    this.is_item_group && 
                    this.item_group_to_object !== null && 
                    this.item_group_to_object.item_group.has_all_items_same_measurement_unit
               )
            {
                value = this.item_group_to_object.quantity_done;
            }
            else if (this.is_item && this.item_to_object !== null)
            {
                value = this.item_to_object.quantity_done;
            }
            
            return value;
        },
        percentage_done: function() {
            var value = '';
            if (this.is_item_group && this.item_group_to_object !== null)
            {
                value = this.item_group_to_object.percentage_done;
            }
            else if (this.is_item && this.item_to_object !== null)
            {
                value = this.item_to_object.percentage_done;
            }
            
            return value;
        },
        interim_bill_measurement_sheet: function() {
            if (this.is_building_interim_bill && this.is_item)
            {
                var measurement_sheet = this.building_interim_bill.scoped('measurement_sheet', {forObject: this.object.id, forBillOfQuantityItem: this.bill_of_quantity_item.id});
                if (measurement_sheet !== null && typeof measurement_sheet.id !== 'undefined')
                {
                    return measurement_sheet;
                }
            }
            
            return null;
        },
        interim_bill_measurement_sheet_status_color: function() {
            var color = 'green';

            if (this.interim_bill_measurement_sheet !== null)
            {
                if (this.interim_bill_measurement_sheet.written.value === false)
                {
                    color = 'red';
                }
                else if (this.interim_bill_measurement_sheet.confirmed.value === false)
                {
                    color = 'yellow';
                }
            }
            
            return color;
        },
        interim_bill_measurement_sheet_status_title: function() {
            var title = sima.translate('MeasurementSheetConfirmed');

            if (this.interim_bill_measurement_sheet !== null)
            {
                if (this.interim_bill_measurement_sheet.written.value === false)
                {
                    title = sima.translate('MeasurementSheetUnWritten');
                }
                else if (this.interim_bill_measurement_sheet.confirmed.value === false)
                {
                    title = sima.translate('MeasurementSheetUnConfirmed');
                }
            }
            
            return title;
        },
        interim_bill_measurement_sheet_group: function() {
            if (this.is_building_interim_bill && this.is_item_group)
            {
                var measurement_sheet_group = this.building_interim_bill.scoped('measurement_sheet_group', {forObject: this.object.id, forBillOfQuantityItemGroup: this.bill_of_quantity_item_group.id});
                if (measurement_sheet_group !== null && typeof measurement_sheet_group.id !== 'undefined')
                {
                    return measurement_sheet_group;
                }
            }
            
            return null;
        },
        interim_bill_measurement_sheet_group_status_color: function() {
            var color = 'green';

            if (this.interim_bill_measurement_sheet_group !== null)
            {
                if (this.interim_bill_measurement_sheet_group.measurement_sheets_written === false)
                {
                    color = 'red';
                }
                else if (this.interim_bill_measurement_sheet_group.measurement_sheets_confirmed === false)
                {
                    color = 'yellow';
                }
            }
            
            return color;
        },
        interim_bill_measurement_sheet_group_status_title: function() {
            var title = sima.translate('MeasurementSheetGroupConfirmed');

            if (this.interim_bill_measurement_sheet_group !== null)
            {
                if (this.interim_bill_measurement_sheet_group.measurement_sheets_written === false)
                {
                    title = sima.translate('MeasurementSheetGroupUnWritten');
                }
                else if (this.interim_bill_measurement_sheet_group.measurement_sheets_confirmed === false)
                {
                    title = sima.translate('MeasurementSheetGroupUnConfirmed');
                }
            }
            
            return title;
        },
        interim_bill_pre_quantity_done: function() {
            var value = '';
            if (this.is_building_interim_bill)
            {
                if (this.is_item_group && this.item_group_to_object !== null && this.item_group_to_object.item_group.has_all_items_same_measurement_unit)
                {
                    value = this.building_interim_bill.scoped('item_group_pre_quantity_done', {forObject: this.object.id, forBillOfQuantityItemGroup: this.bill_of_quantity_item_group.id});
                }
                else if (this.is_item)
                {
                    value = this.building_interim_bill.scoped('item_pre_quantity_done', {forObject: this.object.id, forBillOfQuantityItem: this.bill_of_quantity_item.id});
                }
            }

            return $.isArray(value) ? '' : value;
        },
        interim_bill_pre_value_done: function() {
            var value = '';
            if (this.is_building_interim_bill)
            {
                if (this.is_item_group)
                {
                    value = this.building_interim_bill.scoped('item_group_pre_value_done', {forObject: this.object.id, forBillOfQuantityItemGroup: this.bill_of_quantity_item_group.id});
                }
                else if (this.is_item)
                {
                    value = this.building_interim_bill.scoped('item_pre_value_done', {forObject: this.object.id, forBillOfQuantityItem: this.bill_of_quantity_item.id});
                }
            }

            return $.isArray(value) ? '' : value;
        },
        interim_bill_pre_percentage_done: function() {
            var value = '';
            if (this.is_building_interim_bill)
            {
                if (this.is_item_group)
                {
                    value = this.building_interim_bill.scoped('item_group_pre_percentage_done', {forObject: this.object.id, forBillOfQuantityItemGroup: this.bill_of_quantity_item_group.id});
                }
                else if (this.is_item)
                {
                    value = this.building_interim_bill.scoped('item_pre_percentage_done', {forObject: this.object.id, forBillOfQuantityItem: this.bill_of_quantity_item.id});
                }
            }

            return $.isArray(value) ? '' : value;
        },
        interim_bill_quantity_done: function() {
            var value = '';
            if (this.is_building_interim_bill)
            {
                if (this.is_item_group && this.item_group_to_object !== null && this.item_group_to_object.item_group.has_all_items_same_measurement_unit)
                {
                    value = this.building_interim_bill.scoped('item_group_quantity_done', {forObject: this.object.id, forBillOfQuantityItemGroup: this.bill_of_quantity_item_group.id});
                }
                else if (this.is_item)
                {
                    value = this.building_interim_bill.scoped('item_quantity_done', {forObject: this.object.id, forBillOfQuantityItem: this.bill_of_quantity_item.id});
                }
            }

            return $.isArray(value) ? '' : value;
        },
        interim_bill_value_done: function() {
            var value = '';
            if (this.is_building_interim_bill)
            {
                if (this.is_item_group)
                {
                    value = this.building_interim_bill.scoped('item_group_value_done', {forObject: this.object.id, forBillOfQuantityItemGroup: this.bill_of_quantity_item_group.id});
                }
                else if (this.is_item)
                {
                    value = this.building_interim_bill.scoped('item_value_done', {forObject: this.object.id, forBillOfQuantityItem: this.bill_of_quantity_item.id});
                }
            }

            return $.isArray(value) ? '' : value;
        },
        interim_bill_percentage_done: function() {
            var value = '';
            if (this.is_building_interim_bill)
            {
                if (this.is_item_group)
                {
                    value = this.building_interim_bill.scoped('item_group_percentage_done', {forObject: this.object.id, forBillOfQuantityItemGroup: this.bill_of_quantity_item_group.id});
                }
                else if (this.is_item)
                {
                    value = this.building_interim_bill.scoped('item_percentage_done', {forObject: this.object.id, forBillOfQuantityItem: this.bill_of_quantity_item.id});
                }
            }

            return $.isArray(value) ? '' : value;
        },
        interim_bill_total_quantity_done: function() {
            var value = '';
            if (this.is_building_interim_bill)
            {
                if (this.is_item_group && this.item_group_to_object !== null && this.item_group_to_object.item_group.has_all_items_same_measurement_unit)
                {
                    value = this.building_interim_bill.scoped('item_group_total_quantity_done', {forObject: this.object.id, forBillOfQuantityItemGroup: this.bill_of_quantity_item_group.id});
                }
                else if (this.is_item)
                {
                    value = this.building_interim_bill.scoped('item_total_quantity_done', {forObject: this.object.id, forBillOfQuantityItem: this.bill_of_quantity_item.id});
                }
            }

            return $.isArray(value) ? '' : value;
        },
        interim_bill_total_value_done: function() {
            var value = '';
            if (this.is_building_interim_bill)
            {
                if (this.is_item_group)
                {
                    value = this.building_interim_bill.scoped('item_group_total_value_done', {forObject: this.object.id, forBillOfQuantityItemGroup: this.bill_of_quantity_item_group.id});
                }
                else if (this.is_item)
                {
                    value = this.building_interim_bill.scoped('item_total_value_done', {forObject: this.object.id, forBillOfQuantityItem: this.bill_of_quantity_item.id});
                }
            }

            return $.isArray(value) ? '' : value;
        },
        interim_bill_total_percentage_done: function() {
            var value = '';
            if (this.is_building_interim_bill)
            {
                if (this.is_item_group)
                {
                    value = this.building_interim_bill.scoped('item_group_total_percentage_done', {forObject: this.object.id, forBillOfQuantityItemGroup: this.bill_of_quantity_item_group.id});
                }
                else if (this.is_item)
                {
                    value = this.building_interim_bill.scoped('item_total_percentage_done', {forObject: this.object.id, forBillOfQuantityItem: this.bill_of_quantity_item.id});
                }
            }

            return $.isArray(value) ? '' : value;
        },
        can_change_building_measurement_sheet_ready: function() {
            if (this.is_building_interim_bill)
            {
                this.building_interim_bill.can_change_building_measurement_sheet;
                
                return this.building_interim_bill.shotStatus('can_change_building_measurement_sheet') === 'OK';
            }
            
            return false;
        }
    },
    mounted: function(){
        var _this = this;

        $(this.$el).on('destroyed', function(){
            _this.$destroy();
        });
    },
    methods: {
        getValuePerUnit: function(is_display = false) {
            var value = '';
            if (
                    this.is_item_group && 
                    this.item_group_to_object !== null && 
                    this.item_group_to_object.item_group.has_all_items_same_measurement_unit
                )
            {
                value = is_display ? ('~ ' + this.item_group_to_object.value_per_unit_display) : this.item_group_to_object.value_per_unit;
            }
            else if (this.is_item && this.item_to_object !== null)
            {
                value = is_display ? this.item_to_object.value_per_unit_display : this.item_to_object.value_per_unit;
            }
            
            return value;
        },
        openBuildingMeasurementSheet: function() {
            var _this = this;

            if (
                    this.open_building_measurement_sheet_in_progress === false && 
                    this.is_item && 
                    this.is_building_interim_bill && 
                    this.can_change_building_measurement_sheet_ready && 
                    this.building_interim_bill.can_change_building_measurement_sheet
                )
            {
                sima.ajax.get('buildings/buildingConstruction/getBuildingMeasurementSheet',{
                    async: true,
                    get_params: {
                        building_interim_bill_id: this.building_interim_bill.id,
                        bill_of_quantity_item_id: this.bill_of_quantity_item.id,
                        object_id: this.object.id
                    },
                    success_function: function(response) {
                        sima.dialog.openModel('defaultLayout', {model_name: 'BuildingMeasurementSheet', model_id: response.building_measurement_sheet_id});
                    }
                });
            }
            
            this.open_building_measurement_sheet_in_progress = true;
            setTimeout(function() {
                _this.open_building_measurement_sheet_in_progress = false;
            }, 1000);
        }
    }
});
