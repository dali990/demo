/* global Vue, sima */

Vue.component('buildings-theme_tab-bill_of_quantity_table_header_object', {
    template: '#buildings-theme_tab-bill_of_quantity_table_header_object',
    mixins: [sima.buildings.getBillOfQuantityTableCellsWidthComponent()],
    props:  {
        object: {
            validator: sima.vue.ProxyValidator
        },
        bill_of_quantity: {
            validator: sima.vue.ProxyValidator
        },
        building_construction: {
            validator: sima.vue.ProxyValidator
        },
        building_interim_bill: {
            validator: sima.vue.ProxyValidator
        },
        show_quantity_done_column: Boolean,
        show_value_done_column: Boolean,
        show_percentage_done_column: Boolean
    },
    data: function () {
        return {
            
        };
    },
    computed: {
        is_building_construction: function() {
            return typeof this.building_construction !== 'undefined' && this.building_construction !== null;
        },
        is_building_interim_bill: function() {
            return typeof this.building_interim_bill !== 'undefined' && this.building_interim_bill !== null;
        },
        has_object_value_per_unit: function() {
            return typeof this.object.bill_of_quantity !== 'undefined' ? this.object.bill_of_quantity.has_object_value_per_unit : false;
        },
        has_building_interim_bill_visible_columns: function() {
            return this.show_quantity_done_column || this.show_value_done_column || this.show_percentage_done_column;
        }
    },
    mounted: function(){
        var _this = this;

        $(this.$el).on('destroyed', function(){
            _this.$destroy();
        });
    },
    methods: {
        
    }
});
