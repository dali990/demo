
<script type="text/x-template" id="buildings-theme_tab-building_interim_bills">
    <div 
        class="theme-tab-building-interim-bills no-interim-bills sima-layout-panel"
        v-if="building_construction.building_interim_bills_count === '' || building_construction.building_interim_bills_count === 0"
    >
        <sima-button
            v-if="building_construction.can_change_building_interim_bill"
            v-on:click="addBuildingInterimBill"
            class="add-first-interim-bill"
            v-bind:class="{_disabled: !building_interim_bill_init_data_ready}"
            title="<?=Yii::t('BuildingsModule.BuildingInterimBill', 'AddFirstBuildingInterimBill')?>"
        >
            <span class="sima-old-btn-title"><?=Yii::t('BuildingsModule.BuildingInterimBill', 'AddFirstBuildingInterimBill')?></span>
        </sima-button>
    </div>
    <div 
        v-else
        class="theme-tab-building-interim-bills sima-layout-panel _vertical"
    >
        <div class="interim-bills sima-layout-fixed-panel">
            <div 
                class="interim-bills-total"
                v-bind:class="{_selected: selected_interim_bill_id === null}"
                v-on:click="onSelectInterimBill(null)"
            >
                <h2 style="flex-grow: 1;"><?=Yii::t('BuildingsModule.BuildingInterimBill', 'Total')?></h2>
                <div style="display: flex; flex-direction: column;">
                    <sima-button
                        v-if="building_construction.can_change_building_interim_bill"
                        v-on:click="addBuildingInterimBill"
                        v-bind:class="{_disabled: (building_construction.has_final_interim_bill || !building_interim_bill_init_data_ready)}"
                        title="<?=Yii::t('BuildingsModule.BuildingInterimBill', 'AddBuildingInterimBill')?>"
                    >
                        <span class="sima-old-btn-title">
                            <?=Yii::t('BuildingsModule.BuildingInterimBill', 'AddBuildingInterimBill')?>
                        </span>
                    </sima-button>
                    <span v-if="building_construction.has_final_interim_bill" style="font-size:10px; color: red;">
                        <?=Yii::t('BuildingsModule.BuildingInterimBill', 'HasFinalBuildingInterimBill')?>
                    </span>
                </div>
            </div>
            <div
                v-for='interim_bill in building_interim_bills'
                class="interim-bill"
                v-bind:class="{_selected:interim_bill.id === selected_interim_bill_id}"
                v-on:click="onSelectInterimBill(interim_bill)"
            >
                <div class="interim-bill-left">
                    {{ interim_bill.is_final ? 'K' : interim_bill.order_number_roman }}
                </div>
                <div class="interim-bill-right">
                    <div class="interim-bill-right-top">
                        <div class="interim-bill-right-top-left sima-text-dots" v-bind:title="interim_bill.status_display">
                            {{ interim_bill.status_display }}
                        </div>
                        <div class="interim-bill-right-top-right">
                            {{ interim_bill.value | formatNumber }}
                        </div>
                    </div>
                    <div class="interim-bill-right-bottom">
                        {{ interim_bill.issue_date }}
                        <div class="interim-bill-right-bottom-right">
                            <sima-model-options v-bind:model="interim_bill"></sima-model-options>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="sima-layout-panel">
            <vue-tabs v-bind:use_sima_default_layout="true">
                <v-tab title="<?=Yii::t('BuildingsModule.BuildingInterimBill', 'Works')?>">
                    <buildings-theme_tab-bill_of_quantity 
                        v-if="building_construction.bill_of_quantity"
                        v-bind:bill_of_quantity="building_construction.bill_of_quantity"
                        v-bind:building_construction="building_construction"
                        v-bind:building_interim_bill="selected_interim_bill"
                    >
                    </buildings-theme_tab-bill_of_quantity>
                </v-tab>
                <v-tab title="<?=Yii::t('BuildingsModule.BuildingInterimBill', 'Finance')?>">
                    <buildings-theme_tab-building_interim_bills_finance 
                        v-bind:building_construction="building_construction"
                        v-bind:building_interim_bill="selected_interim_bill"
                    >
                    </buildings-theme_tab-building_interim_bills_finance>
                </v-tab>
            </vue-tabs>
        </div>
    </div>
</script>
