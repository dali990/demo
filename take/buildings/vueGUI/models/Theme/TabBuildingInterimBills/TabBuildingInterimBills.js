/* global Vue, sima */

Vue.component('buildings-theme_tab-building_interim_bills', {
    template: '#buildings-theme_tab-building_interim_bills',
    mixins: [sima.vue.getBaseComponent()],
    props:  {
        building_construction: {
            validator: sima.vue.ProxyValidator
        }
    },
    data: function () {
        return {
            selected_interim_bill: null
        };
    },
    computed: {
        building_interim_bill_init_data_ready: function() {
            this.building_construction.building_interim_bill_init_data;
            
            return this.building_construction.shotStatus('building_interim_bill_init_data') === 'OK';
        },
        selected_interim_bill_id: function() {
            return this.selected_interim_bill !== null ? this.selected_interim_bill.id : null;
        },
        building_interim_bills: function() {
            return this.building_construction.building_interim_bills;
        }
    },
    watch: {
        
    },
    updated: function () {
        var _this = this;

        setTimeout(function() {
            sima.layout.allignObject($(_this.$el), 'TRIGGER_buildings-theme_tab-building_interim_bills_updated');
        }, 0);
    },
    mounted: function () {
        var _this = this;

        setTimeout(function() {
            sima.layout.allignObject($(_this.$el), 'TRIGGER_buildings-theme_tab-building_interim_bills_updated');
        }, 0);
    },
    methods: {
        addBuildingInterimBill: function() {
            sima.model.form('BuildingInterimBill', '', {
                init_data: this.building_construction.building_interim_bill_init_data
            });
        },
        onSelectInterimBill: function(interim_bill) {
            this.selected_interim_bill = interim_bill;
        },
        setBuildingInterimBillAsFinal: function(interim_bill) {
            sima.ajax.get('buildings/buildingConstruction/setBuildingInterimBillAsFinal',{
                async: true,
                get_params: {
                    building_interim_bill_id: interim_bill.id
                }
            });
        }
    }
});
