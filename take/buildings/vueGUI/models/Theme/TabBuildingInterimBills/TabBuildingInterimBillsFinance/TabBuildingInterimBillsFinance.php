
<script type="text/x-template" id="buildings-theme_tab-building_interim_bills_finance">
    <div 
        class="theme-tab-building-interim-bills-finance _total sima-layout-panel"
        v-if="building_interim_bill === null"
    >
        <table>
            <thead>
                <tr>
                    <th><?=Yii::t('BuildingsModule.BuildingInterimBill', 'TotalAfter')?></th>
                    <th><?=Yii::t('BuildingsModule.BuildingInterimBill', 'BillAdvance')?></th>
                    <th><?=Yii::t('BuildingsModule.BuildingInterimBill', 'RemainingBillAdvance')?></th>
                </tr>
            </thead>
            <tbody>
                <tr
                    v-for='interim_bill in building_construction.building_interim_bills'
                >
                    <td class="_align-right">{{ interim_bill.total_value | formatNumber }}</td>
                    <td class="_align-right">{{ interim_bill.bill_advance | formatNumber }}</td>
                    <td class="_align-right">{{ interim_bill.remaining_bill_advance | formatNumber }}</td>
                </tr>
            </tbody>
        </table>
    </div>
    <div 
        v-else
        class="theme-tab-building-interim-bills-finance _report sima-layout-panel _horizontal"
        data-sima-layout-init='{"width_cut":40, "height_cut":40}'
    >
        <div class="finance-options sima-layout-fixed-panel">
            <sima-button
                v-on:click="buildingInterimBillExportPdf"
                title="<?=Yii::t('BuildingsModule.BuildingInterimBill', 'BuildingInterimBillExportPdf')?>"
            >
                <span class="sima-old-btn-title"><?=Yii::t('BuildingsModule.BuildingInterimBill', 'BuildingInterimBillExportPdf')?></span>
            </sima-button>
            <sima-button
                v-if="!has_bill"
                v-bind:class="{_disabled:!btn_ready_convert_to_bill}"
                v-on:click="convertToBill()"
                title="<?=Yii::t('BuildingsModule.BuildingInterimBill', 'ConvertToBill')?>"
            >
                <span class="sima-old-btn-title"><?=Yii::t('BuildingsModule.BuildingInterimBill', 'ConvertToBill')?></span>
            </sima-button>
            <sima-button
                v-bind:class=""
                v-on:click="loadReport()"
                title="<?=Yii::t('BuildingsModule.BuildingInterimBill', 'Refresh')?>"
            >
                <span class="sima-old-btn-title"><?=Yii::t('BuildingsModule.BuildingInterimBill', 'Refresh')?></span>
            </sima-button>
        </div>
        <div class="sima-layout-panel" v-html="report_html"></div>
    </div>
</script>
