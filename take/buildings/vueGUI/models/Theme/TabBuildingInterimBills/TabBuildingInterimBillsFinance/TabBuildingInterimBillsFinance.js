/* global Vue, sima */

Vue.component('buildings-theme_tab-building_interim_bills_finance', {
    template: '#buildings-theme_tab-building_interim_bills_finance',
    mixins: [sima.vue.getBaseComponent()],
    props:  {
        building_construction: {
            validator: sima.vue.ProxyValidator
        },
        building_interim_bill: {
            validator: sima.vue.ProxyValidator
        }
    },
    data: function () {
        return {
            report_html: ''
        };
    },
    computed: {
        bib_file: function(){
            return this.building_interim_bill.file;
        },
        has_bill: function(){
            return this.bib_file.bill || false;
        },
        bc_constructor_company: function(){
            return this.building_construction.constructor_model;
        },
        btn_ready_convert_to_bill: function(){
            var building_interim_bill_status = false;
            var bc_constructor_company_status = false;
            var bib_file_status = false;
            
            if(this.building_interim_bill)
            {
                this.building_interim_bill.issue_date;
                building_interim_bill_status = (this.building_interim_bill.shotStatus('issue_date') === 'OK');
            }
            if(this.bc_constructor_company)
            {
                this.bc_constructor_company.partner;
                bc_constructor_company_status = (this.bc_constructor_company.shotStatus('partner') === 'OK');
            }
            if(this.bib_file)
            {
                this.bib_file.name;
                bib_file_status = (this.bib_file.shotStatus('name') === 'OK');
            }
            
            return (
                    building_interim_bill_status && 
                    bc_constructor_company_status && 
                    bib_file_status
                );
        }
    },
    watch: {
        building_interim_bill: function(){
            this.loadReport();
        }
    },
    updated: function () {
        var _this = this;

        setTimeout(function() {
            sima.layout.allignObject($(_this.$el), 'TRIGGER_buildings-theme_tab-building_interim_bills_finance_updated');
        }, 0);
    },
    mounted: function () {
        var _this = this;

        setTimeout(function() {
            sima.layout.allignObject($(_this.$el), 'TRIGGER_buildings-theme_tab-building_interim_bills_finance_updated');
        }, 0);
        
        this.loadReport();
    },
    methods: {
        loadReport: function() {
            var _this = this;
            if(this.building_interim_bill)
            {
                sima.ajaxLong.start('buildings/buildingConstruction/buildingInterimBillGetReport', {
                    data: {
                        building_interim_bill_id: this.building_interim_bill.id
                    },
                    onEnd: function(response) {
                        _this.report_html = response.report_html;
                    }
                });
            }
        },
        buildingInterimBillExportPdf: function() {
            sima.ajaxLong.start('buildings/buildingConstruction/buildingInterimBillExportPdf', {
                data: {
                    building_interim_bill_id: this.building_interim_bill.id
                },
                showProgressBar: $(this.$el),
                onEnd: function(response) {
                    sima.temp.download(response.filename, response.downloadname);
                }
            });
        },
        convertToBill: function() {
            sima.model.form('BillOut', this.building_interim_bill.file.id,{
                init_data:{
                    BillOut: {
                        partner_id: this.bc_constructor_company.id,
                        bill_number: this.bib_file.name,
                        income_date: this.building_interim_bill.issue_date,
                        lock_amounts: true
                    }
                }
            });
        }
    }
});
