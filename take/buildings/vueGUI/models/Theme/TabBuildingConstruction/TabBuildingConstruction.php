
<script type="text/x-template" id="buildings-theme_tab-building_construction">
    <div class="theme-tab-building-construction sima-layout-panel _horizontal">
        <div class="top-panel sima-layout-fixed-panel">
            <sima-button
                v-if="typeof theme.building_construction.id === 'undefined' && (theme.type === 'PLAIN' || theme.type === 'JOB') && theme.has_change_access"
                v-on:click="addBuildingConstruction"
                title="<?=Yii::t('BuildingsModule.BuildingConstruction', 'AddBuildingConstruction')?>"
            >
                <span class="sima-old-btn-title">
                    <?=Yii::t('BuildingsModule.BuildingConstruction', 'AddBuildingConstruction')?>
                </span>
            </sima-button>
            <span class="title"><?=Yii::t('BuildingsModule.BillOfQuantity', 'Constructions')?></span>
        </div>
        <div class="bottom-panel sima-layout-panel" data-sima-layout-init='{"width_cut":10, "height_cut":10}'> 
            <h3><?=Yii::t('BuildingsModule.BillOfQuantity', 'BillsOfQuantities')?></h3>
            <sima-button
                v-if="theme.has_change_access"
                v-on:click="addBillOfQuantity"
                title="<?=Yii::t('BuildingsModule.BillOfQuantity', 'AddBillOfQuantity')?>"
            >
                <span class="sima-old-btn-title">
                    <?=Yii::t('BuildingsModule.BillOfQuantity', 'AddBillOfQuantity')?>
                </span>
            </sima-button>
            <table class="bill-of-quantities">
                <tr>
                    <th></th>
                    <th><?=Yii::t('BuildingsModule.BillOfQuantity', 'Name')?></th>
                    <th><?=Yii::t('BuildingsModule.BillOfQuantity', 'HasObjectValuePerUnit')?></th>
                    <th><?=Yii::t('BuildingsModule.BillOfQuantity', 'Comment')?></th>
                    <th><?=Yii::t('BuildingsModule.BillOfQuantity', 'IsPrivate')?></th>
                </tr>
                <tr
                    class="bill-of-quantity"
                    v-for="bill_of_quantity in bill_of_quantities"
                    v-if="bill_of_quantity.id !== without_bill_of_quantity_id"
                >
                    <td> 
                        <sima-model-options v-bind:model="bill_of_quantity"></sima-model-options>
                    </td>
                    <td style="word-break:break-word; max-width: 400px;">{{ bill_of_quantity.name }}</td>
                    <td>{{ bill_of_quantity.has_object_value_per_unit_display }}</td>
                    <td>{{ bill_of_quantity.comment }}</td>
                    <td>{{ bill_of_quantity.is_private_display }}</td>
                </tr>
            </table>
        </div>
    </div>
</script>
