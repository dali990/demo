/* global Vue, sima */

Vue.component('buildings-theme_tab-building_construction', {
    template: '#buildings-theme_tab-building_construction',
    props:  {
        theme: {
            validator: sima.vue.ProxyValidator
        },
        without_bill_of_quantity_id: {
            type: Number,
            default: null
        }
    },
    data: function () {
        return {
            
        };
    },
    computed: {
        bill_of_quantities: function() {
            return this.theme.scoped('bill_of_quantities', {hasAccess: sima.getCurrentUserId(), byName: true});
        }
    },
    watch: {
        
    },
    mounted: function(){
        var _this = this;
        
        $(this.$el).on('destroyed', function(){
            _this.$destroy();
        });
    },
    updated: function () {
        this.$nextTick(function () {
            sima.layout.allignObject($(this.$el), 'TRIGGER_buildings-theme_tab-building_construction_updated');
        });
    },
    methods: {
        addBuildingConstruction: function() {
            sima.model.form('BuildingConstruction', this.theme.id);
        },
        addBillOfQuantity: function() {
            sima.model.form('BillOfQuantity', '', {
                init_data: {
                    BillOfQuantity: {
                        theme_id: this.theme.id
                    }
                }
            });
        }
    }
});
