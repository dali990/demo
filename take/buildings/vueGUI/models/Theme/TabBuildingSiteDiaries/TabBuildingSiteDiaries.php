
<script type="text/x-template" id="buildings-theme_tab-building_site_diaries">
    <div class="theme-tab-building-site-diaries sima-layout-panel _horizontal">
        <div class="theme-tab-building-site-diaries-top sima-layout-fixed-panel">
            <div class="theme-tab-building-site-diaries-calendar">
                <v-date-picker 
                    is-expanded 
                    is-inline 
                    v-model='selected_date'
                    v-bind:first-day-of-week="2" 
                    v-bind:locale="date_picker_locale"
                ></v-date-picker>
            </div>
            <div class="theme-tab-building-site-diaries-list">
                <div class="theme-tab-building-site-diaries-list-header">
                    <sima-button
                        v-on:click="addBuildingSiteDiaryForConstructor"
                    >
                        <span class="sima-old-btn-title"><?=Yii::t('BuildingsModule.BuildingSiteDiary', 'AddBuildingSiteDiaryForConstructor')?></span>
                    </sima-button>
                </div>
                <div class="theme-tab-building-site-diaries-list-body">
                    <div 
                        class="theme-tab-building-site-diaries-diary"
                        v-bind:class="(selected_building_site_diary !== null && selected_building_site_diary.id === building_site_diary.id) ? '_selected' : ''"
                        v-for="building_site_diary in building_site_diaries"
                        v-on:click="selectBuildingSiteDiary(building_site_diary)"
                    >
                        <i
                            class="fa fa-external-link-alt"
                            title="<?=Yii::t('BuildingsModule.BuildingSiteDiary', 'OpenBuildingSiteDiary')?>"
                            v-on:click="openBuildingSiteDiary(building_site_diary, $event)"
                        ></i>
                        <i
                            class="theme-tab-building-site-diaries-delete-diary fas fa-trash"
                            title="<?=Yii::t('BuildingsModule.BuildingSiteDiary', 'DeleteBuildingSiteDiary')?>"
                            v-on:click="deleteBuildingSiteDiary(building_site_diary, $event)"
                        ></i>
                        <sima-status-span v-bind:status="building_site_diary.diary_creator_confirmed"></sima-status-span>
                        <sima-status-span v-bind:status="building_site_diary.responsible_engineer_confirmed"></sima-status-span>
                        <sima-status-span v-bind:status="building_site_diary.supervisor_confirmed"></sima-status-span>
                        <span>
                            {{ building_site_diary.constructor_company.DisplayName }}
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <buildings-theme_tab-building_site_diary
            class="theme-tab-building-site-diaries-bottom"
            v-if="selected_building_site_diary !== null"
            v-bind:building_site_diary="selected_building_site_diary"
        >
        </buildings-theme_tab-building_site_diary>
    </div>
</script>
