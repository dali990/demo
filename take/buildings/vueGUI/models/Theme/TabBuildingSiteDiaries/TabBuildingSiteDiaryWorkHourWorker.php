
<script type="text/x-template" id="buildings-theme_tab-building_site_diary_work_hour_worker">
    <div>
        <i
            class="fas fa-trash"
            title="<?=Yii::t('BuildingsModule.BuildingSiteDiaryWorkHourWorker', 'DeleteBuildingSiteDiaryWorkHourWorker')?>"
            v-on:click="deleteBuildingSiteDiaryWorkHourWorker(building_site_diary_work_hour_worker, $event)"
        ></i>
        <i
            class="fas fa-pencil-alt"
            title="<?=Yii::t('BuildingsModule.BuildingSiteDiaryWorkHourWorker', 'EditBuildingSiteDiaryWorkHourWorker')?>"
            v-on:click="editBuildingSiteDiaryWorkHourWorker(building_site_diary_work_hour_worker, $event)"
        ></i>
        <div 
            v-if="building_site_diary_work_hour_worker.worker_id !== ''"
            v-html="building_site_diary_work_hour_worker.worker.DisplayHtml"
        ></div>
        <div v-else>
            <b>{{ count_title + ': ' }}</b> {{ building_site_diary_work_hour_worker.worker_count }}
        </div>
    </div>
</script>
