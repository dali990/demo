/* global Vue, sima */

Vue.component('buildings-theme_tab-building_site_diary', {
    template: '#buildings-theme_tab-building_site_diary',
    props:  {
        building_site_diary: {
            validator: sima.vue.ProxyValidator
        }
    },
    data: function () {
        return {
            edit_work_description: false,
            local_building_site_diary_work_description: null,
            edit_remarks: false,
            local_building_site_diary_remarks: null,
            expanded_work_hour_id: null,
            tinymce_params: {
                menubar: false,
                statusbar: false,
                elementpath: false,
                object_resizing: false,
                paste_as_text: true,
                image_max_width: 200,
                image_max_height: 100,
                width: 'auto',
                fontsize_formats: '10=10pt 11=11pt 12=12pt 13=13pt 14=14pt 15=15pt 16=16pt 17=17pt 18=18pt 19=19pt 20=20pt',
                toolbar: 'undo redo | bold italic underline | forecolor backcolor | superscript subscript | numlist bullist outdent indent | image | modelchooser | link',
                plugins: 'autoheight modelchooser image emoticons print searchreplace autolink directionality code visualblocks visualchars link advlist lists textcolor colorpicker nonbreaking paste'
            }
        };
    },
    computed: {
        building_site_diary_date: function() {
            return this.building_site_diary.date;
        },
        building_site_diary_work_description: function() {
            return this.building_site_diary.work_description;
        },
        building_site_diary_remarks: function() {
            return this.building_site_diary.remarks;
        },
        has_building_site_diary_work_description: function() {
            return !sima.isEmpty(this.local_building_site_diary_work_description);
        },
        has_building_site_diary_remarks: function() {
            return !sima.isEmpty(this.local_building_site_diary_remarks);
        }
    },
    watch: {
        building_site_diary_date: () => {},
        building_site_diary_work_description: function() {
            this.local_building_site_diary_work_description = this.building_site_diary_work_description;
        },
        building_site_diary_remarks: function() {
            this.local_building_site_diary_remarks = this.building_site_diary_remarks;
        }
    },
    mounted: function(){
        var _this = this;
        
        $(this.$el).on('destroyed', function(){
            _this.$destroy();
        });
    },
    methods: {
        onClickWorkDescriptionEdit: function() {
            this.edit_work_description = !this.edit_work_description;
            if (this.edit_work_description === false)
            {
                this.updateBuildingSiteDiaryColumnValue('work_description', this.local_building_site_diary_work_description);
            }
        },
        onClickRemarksEdit: function() {
            this.edit_remarks = !this.edit_remarks;
            if (this.edit_remarks === false)
            {
                this.updateBuildingSiteDiaryColumnValue('remarks', this.local_building_site_diary_remarks);
            }
        },
        updateBuildingSiteDiaryColumnValue: function(column_name, column_value) {
            sima.ajax.get("buildings/buildingConstruction/saveBuildingSiteDiaryColumnValue", {
                async: true,
                get_params: {
                    building_site_diary_id: this.building_site_diary.id,
                    column_name: column_name,
                    new_value: column_value 
                }
            });
        },
        addBuildingSiteDiaryWorkHour: function() {
            sima.model.form('BuildingSiteDiaryWorkHour', '', {
                init_data: {
                    BuildingSiteDiaryWorkHour: {
                        site_diary_id: {
                            init: this.building_site_diary.id,
                            disabled: true
                        },
                        start_datetime: this.building_site_diary_date,
                        end_datetime: this.building_site_diary_date
                    }
                }
            });
        },
        deleteBuildingSiteDiaryWorkHour: function(building_site_diary_work_hour, event) {
            event.stopPropagation();
            sima.model.remove('BuildingSiteDiaryWorkHour', building_site_diary_work_hour.id);
        },
        editBuildingSiteDiaryWorkHour: function(building_site_diary_work_hour, event) {
            event.stopPropagation();
            sima.model.form('BuildingSiteDiaryWorkHour', building_site_diary_work_hour.id);
        },
        toggleExpandedWorkHour: function(building_site_diary_work_hour) {
            this.expanded_work_hour_id = (this.expanded_work_hour_id === building_site_diary_work_hour.id) ? null : expanded_work_hour_id = building_site_diary_work_hour.id;
        },
        addBuildingSiteDiaryWorkHourWorker: function(building_site_diary_work_hour) {
            sima.model.form('BuildingSiteDiaryWorkHourWorker', '', {
                init_data: {
                    BuildingSiteDiaryWorkHourWorker: {
                        site_diary_work_hour_id: {
                            init: building_site_diary_work_hour.id,
                            disabled: true
                        }
                    }
                }
            });
        },
        addBuildingSiteDiaryWorkHourMechanization: function(building_site_diary_work_hour) {
            sima.model.form('BuildingSiteDiaryWorkHourMechanization', '', {
                init_data: {
                    BuildingSiteDiaryWorkHourMechanization: {
                        site_diary_work_hour_id: {
                            init: building_site_diary_work_hour.id,
                            disabled: true
                        }
                    }
                }
            });
        },
        deleteBuildingSiteDiaryWorkHourMechanization: function(building_site_diary_work_hour_mechanization, event) {
            event.stopPropagation();
            sima.model.remove('BuildingSiteDiaryWorkHourMechanization', building_site_diary_work_hour_mechanization.id);
        },
        editBuildingSiteDiaryWorkHourMechanization: function(building_site_diary_work_hour_mechanization, event) {
            event.stopPropagation();
            sima.model.form('BuildingSiteDiaryWorkHourMechanization', building_site_diary_work_hour_mechanization.id);
        }
    }
});
