/* global Vue, sima */

Vue.component('buildings-theme_tab-building_site_diaries', {
    template: '#buildings-theme_tab-building_site_diaries',
    props:  {
        building_construction: {
            validator: sima.vue.ProxyValidator
        }
    },
    data: function () {
        return {
            selected_date: new Date(),
            selected_building_site_diary: null
        };
    },
    computed: {
        building_site_diaries: function() {
            return this.building_construction.scoped('building_site_diaries', {'forDate': sima.date.format(this.selected_date), byConstructorName: true});
        },
        date_picker_locale: function() {
            return sima.getCurrentLanguage();
        }
    },
    watch: {
        selected_date: function() {
            this.selected_building_site_diary = null;
        }
    },
    mounted: function(){
        var _this = this;
        
        $(this.$el).on('destroyed', function(){
            _this.$destroy();
        });
    },
    updated: function () {
        this.$nextTick(function () {
            sima.layout.allignObject($(this.$el), 'TRIGGER_buildings-theme_tab-building_site_diaries_updated');
        });
    },
    methods: {
        addBuildingSiteDiaryForConstructor: function() {
            var _this = this;

            sima.model.form('BuildingSiteDiary', '', {
                formName: 'onlyConstructor',
                init_data: {
                    BuildingSiteDiary: {
                        building_construction_id: _this.building_construction.id,
                        date: sima.date.format(_this.selected_date)
                    }
                }
            });
        },
        deleteBuildingSiteDiary: function(building_site_diary, event) {
            event.stopPropagation();
            sima.model.remove('BuildingSiteDiary', building_site_diary.id);
        },
        openBuildingSiteDiary: function(building_site_diary, event) {
            event.stopPropagation();
            sima.dialog.openModel('defaultLayout', {model_name: 'BuildingSiteDiary', model_id: building_site_diary.id});
        },
        selectBuildingSiteDiary: function(building_site_diary) {
            this.selected_building_site_diary = building_site_diary;
        }
    }
});
