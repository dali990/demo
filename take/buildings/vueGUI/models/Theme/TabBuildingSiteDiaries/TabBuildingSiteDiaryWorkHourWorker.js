/* global Vue, sima */

Vue.component('buildings-theme_tab-building_site_diary_work_hour_worker', {
    template: '#buildings-theme_tab-building_site_diary_work_hour_worker',
    props:  {
        building_site_diary_work_hour_worker: {
            validator: sima.vue.ProxyValidator
        },
        count_title: String
    },
    data: function () {
        return {
        };
    },
    computed: {
        
    },
    watch: {
        
    },
    mounted: function(){
        var _this = this;
        
        $(this.$el).on('destroyed', function(){
            _this.$destroy();
        });
    },
    methods: {
        deleteBuildingSiteDiaryWorkHourWorker: function(building_site_diary_work_hour_worker, event) {
            event.stopPropagation();
            sima.model.remove('BuildingSiteDiaryWorkHourWorker', building_site_diary_work_hour_worker.id);
        },
        editBuildingSiteDiaryWorkHourWorker: function(building_site_diary_work_hour_worker, event) {
            event.stopPropagation();
            sima.model.form('BuildingSiteDiaryWorkHourWorker', building_site_diary_work_hour_worker.id);
        }
    }
});
