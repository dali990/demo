
<script type="text/x-template" id="buildings-theme_tab-building_site_diary">
    <div class="theme-tab-building-site-diary sima-layout-panel _splitter _vertical">
        <div class="sima-layout-panel">
            <div class="theme-tab-building-site-diary-left-side sima-layout-panel" data-sima-layout-init='{"width_cut":20, "height_cut":20}'>
                <sima-button
                    v-on:click="addBuildingSiteDiaryWorkHour"
                    title="<?=Yii::t('BuildingsModule.BuildingSiteDiaryWorkHour', 'AddBuildingSiteDiaryWorkHour')?>"
                >
                    <span class="sima-old-btn-title">
                        <?=Yii::t('BuildingsModule.BuildingSiteDiaryWorkHour', 'AddBuildingSiteDiaryWorkHour')?>
                    </span>
                </sima-button>
                <h3><?=Yii::t('BuildingsModule.BuildingSiteDiaryWorkHour', 'BuildingSiteDiaryWorkHour')?>:</h3>
                <div>
                    <div 
                        class="theme-tab-building-site-diary-work-hour"
                        v-for="building_site_diary_work_hour in building_site_diary.building_site_diary_work_hours"
                    >
                        <div class="theme-tab-building-site-diary-work-hour-name">
                            <i
                                class="theme-tab-building-site-diary-work-hour-expand"
                                v-bind:class="(expanded_work_hour_id === building_site_diary_work_hour.id) ? 'fas fa-caret-down' : 'fas fa-caret-right'"
                                v-on:click="toggleExpandedWorkHour(building_site_diary_work_hour)"
                            ></i>
                            <i
                                class="fas fa-trash"
                                title="<?=Yii::t('BuildingsModule.BuildingSiteDiaryWorkHour', 'DeleteBuildingSiteDiaryWorkHour')?>"
                                v-on:click="deleteBuildingSiteDiaryWorkHour(building_site_diary_work_hour, $event)"
                            ></i>
                            <i
                                class="fas fa-pencil-alt"
                                title="<?=Yii::t('BuildingsModule.BuildingSiteDiaryWorkHour', 'EditBuildingSiteDiaryWorkHour')?>"
                                v-on:click="editBuildingSiteDiaryWorkHour(building_site_diary_work_hour, $event)"
                            ></i>
                            <span>
                                <?=Yii::t('BuildingsModule.BuildingSiteDiaryWorkHour', 'From')?> 
                                {{ building_site_diary_work_hour.start_time }} 
                                <?=Yii::t('BuildingsModule.BuildingSiteDiaryWorkHour', 'To')?> 
                                {{ building_site_diary_work_hour.end_time }}
                            </span>
                            <sima-button
                                v-on:click="addBuildingSiteDiaryWorkHourWorker(building_site_diary_work_hour)"
                                title="<?=Yii::t('BuildingsModule.BuildingSiteDiaryWorkHourWorker', 'AddBuildingSiteDiaryWorkHourWorker')?>"
                            >
                                <span class="sima-old-btn-title">
                                    <?=Yii::t('BuildingsModule.BuildingSiteDiaryWorkHourWorker', 'AddBuildingSiteDiaryWorkHourWorker')?>
                                </span>
                            </sima-button>
                            <sima-button
                                v-on:click="addBuildingSiteDiaryWorkHourMechanization(building_site_diary_work_hour)"
                                title="<?=Yii::t('BuildingsModule.BuildingSiteDiaryWorkHourMechanization', 'AddBuildingSiteDiaryWorkHourMechanization')?>"
                            >
                                <span class="sima-old-btn-title">
                                    <?=Yii::t('BuildingsModule.BuildingSiteDiaryWorkHourMechanization', 'AddBuildingSiteDiaryWorkHourMechanization')?>
                                </span>
                            </sima-button>
                        </div>
                        <div class="theme-tab-building-site-diary-work-hour-desc">
                            <div 
                                v-if="expanded_work_hour_id === building_site_diary_work_hour.id"
                                class="theme-tab-building-site-diary-work-hour-desc-long"
                            >
                                <div class="theme-tab-building-site-diary-work-hour-desc-long-workers">
                                    <h3 class="theme-tab-building-site-diary-work-hour-desc-long-worker-title">
                                        <?=Yii::t('BuildingsModule.BuildingSiteDiaryWorkHourWorker', 'Workers').': '?>
                                        {{ building_site_diary_work_hour.building_site_diary_work_hour_worker_workers_count }}
                                    </h3>
                                    <buildings-theme_tab-building_site_diary_work_hour_worker
                                        class="theme-tab-building-site-diary-work-hour-desc-long-worker"
                                        v-bind:key="building_site_diary_work_hour_worker_worker.id"
                                        v-for="building_site_diary_work_hour_worker_worker in building_site_diary_work_hour.building_site_diary_work_hour_worker_workers"
                                        v-bind:building_site_diary_work_hour_worker="building_site_diary_work_hour_worker_worker"
                                        count_title="<?=Yii::t('BuildingsModule.BuildingSiteDiaryWorkHourWorker', 'Workers')?>"
                                    >
                                    </buildings-theme_tab-building_site_diary_work_hour_worker>
                                    <h3 class="theme-tab-building-site-diary-work-hour-desc-long-worker-title">
                                        <?=Yii::t('BuildingsModule.BuildingSiteDiaryWorkHourWorker', 'Craftsmans').': '?>
                                        {{ building_site_diary_work_hour.building_site_diary_work_hour_craftsman_workers_count }}
                                    </h3>
                                    <buildings-theme_tab-building_site_diary_work_hour_worker
                                        class="theme-tab-building-site-diary-work-hour-desc-long-worker"
                                        v-bind:key="building_site_diary_work_hour_craftsman_worker.id"
                                        v-for="building_site_diary_work_hour_craftsman_worker in building_site_diary_work_hour.building_site_diary_work_hour_craftsman_workers"
                                        v-bind:building_site_diary_work_hour_worker="building_site_diary_work_hour_craftsman_worker"
                                        count_title="<?=Yii::t('BuildingsModule.BuildingSiteDiaryWorkHourWorker', 'Craftsmans')?>"
                                    >
                                    </buildings-theme_tab-building_site_diary_work_hour_worker>
                                    <h3 class="theme-tab-building-site-diary-work-hour-desc-long-worker-title">
                                        <?=Yii::t('BuildingsModule.BuildingSiteDiaryWorkHourWorker', 'TehnicalStaffs').': '?>
                                        {{ building_site_diary_work_hour.building_site_diary_work_hour_tehnical_staff_workers_count }}
                                    </h3>
                                    <buildings-theme_tab-building_site_diary_work_hour_worker
                                        class="theme-tab-building-site-diary-work-hour-desc-long-worker"
                                        v-bind:key="building_site_diary_work_hour_tehnical_staff_worker.id"
                                        v-for="building_site_diary_work_hour_tehnical_staff_worker in building_site_diary_work_hour.building_site_diary_work_hour_tehnical_staff_workers"
                                        v-bind:building_site_diary_work_hour_worker="building_site_diary_work_hour_tehnical_staff_worker"
                                        count_title="<?=Yii::t('BuildingsModule.BuildingSiteDiaryWorkHourWorker', 'TehnicalStaffs')?>"
                                    >
                                    </buildings-theme_tab-building_site_diary_work_hour_worker>
                                    <h3 class="theme-tab-building-site-diary-work-hour-desc-long-worker-title">
                                        <?=Yii::t('BuildingsModule.BuildingSiteDiaryWorkHourWorker', 'Others').': '?>
                                        {{ building_site_diary_work_hour.building_site_diary_work_hour_other_workers_count }}
                                    </h3>
                                    <buildings-theme_tab-building_site_diary_work_hour_worker
                                        class="theme-tab-building-site-diary-work-hour-desc-long-worker"
                                        v-bind:key="building_site_diary_work_hour_other_worker.id"
                                        v-for="building_site_diary_work_hour_other_worker in building_site_diary_work_hour.building_site_diary_work_hour_other_workers"
                                        v-bind:building_site_diary_work_hour_worker="building_site_diary_work_hour_other_worker"
                                        count_title="<?=Yii::t('BuildingsModule.BuildingSiteDiaryWorkHourWorker', 'Others')?>"
                                    >
                                    </buildings-theme_tab-building_site_diary_work_hour_worker>
                                </div>
                                <div class="theme-tab-building-site-diary-work-hour-desc-long-mechanizations">
                                    <h3 class="theme-tab-building-site-diary-work-hour-desc-long-worker-title">
                                        <?=Yii::t('BuildingsModule.BuildingSiteDiaryWorkHourMechanization', 'Mechanizations').': '?>
                                        {{ building_site_diary_work_hour.building_site_diary_work_hour_mechanizations_count }}
                                    </h3>
                                    <div
                                        class="theme-tab-building-site-diary-work-hour-desc-long-mechanization"
                                        v-for="building_site_diary_work_hour_mechanization in building_site_diary_work_hour.building_site_diary_work_hour_mechanizations"
                                    >
                                        <i
                                            class="fas fa-trash"
                                            title="<?=Yii::t('BuildingsModule.BuildingSiteDiaryWorkHourMechanization', 'DeleteBuildingSiteDiaryWorkHourMechanization')?>"
                                            v-on:click="deleteBuildingSiteDiaryWorkHourMechanization(building_site_diary_work_hour_mechanization, $event)"
                                        ></i>
                                        <i
                                            class="fas fa-pencil-alt"
                                            title="<?=Yii::t('BuildingsModule.BuildingSiteDiaryWorkHourMechanization', 'EditBuildingSiteDiaryWorkHourMechanization')?>"
                                            v-on:click="editBuildingSiteDiaryWorkHourMechanization(building_site_diary_work_hour_mechanization, $event)"
                                        ></i>
                                        <div 
                                            v-if="building_site_diary_work_hour_mechanization.mechanization_id !== ''"
                                            v-html="building_site_diary_work_hour_mechanization.mechanization.DisplayHtml"
                                        ></div>
                                        <div v-else>
                                            <b><?=Yii::t('BuildingsModule.BuildingSiteDiaryWorkHourMechanization', 'Mechanizations').': '?></b> {{ building_site_diary_work_hour_mechanization.mechanization_count }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div 
                                v-else
                                class="theme-tab-building-site-diary-work-hour-desc-short"
                            >
                                <span>
                                    <b><?=Yii::t('BuildingsModule.BuildingSiteDiaryWorkHourWorker', 'Workers').': '?></b> {{ building_site_diary_work_hour.building_site_diary_work_hour_worker_workers_count }}
                                </span>
                                <span>
                                    <b><?=Yii::t('BuildingsModule.BuildingSiteDiaryWorkHourWorker', 'Craftsmans').': '?></b> {{ building_site_diary_work_hour.building_site_diary_work_hour_craftsman_workers_count }}
                                </span>
                                <span>
                                    <b><?=Yii::t('BuildingsModule.BuildingSiteDiaryWorkHourWorker', 'TehnicalStaffs').': '?></b> {{ building_site_diary_work_hour.building_site_diary_work_hour_tehnical_staff_workers_count }}
                                </span>
                                <span>
                                    <b><?=Yii::t('BuildingsModule.BuildingSiteDiaryWorkHourWorker', 'Others').': '?></b> {{ building_site_diary_work_hour.building_site_diary_work_hour_other_workers_count }}
                                </span>
                                <span>
                                    <b><?=Yii::t('BuildingsModule.BuildingSiteDiaryWorkHourWorker', 'TotalWorkers').': '?></b> {{ building_site_diary_work_hour.building_site_diary_work_hour_workers_count }}
                                </span>
                                <span>
                                    <b><?=Yii::t('BuildingsModule.BuildingSiteDiaryWorkHourMechanization', 'Mechanizations').': '?></b> {{ building_site_diary_work_hour.building_site_diary_work_hour_mechanizations_count }}
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="theme-tab-building-site-diary-right-side sima-layout-panel _splitter _horizontal">
            <div class="theme-tab-building-site-diary-work-description sima-layout-panel" data-sima-layout-init='{"proportion":0.6}'>
                <div class="theme-tab-building-site-diary-work-description-wrap sima-layout-panel _horizontal" data-sima-layout-init='{"width_cut":20, "height_cut":20}'>
                    <div class="theme-tab-building-site-diary-work-description-header sima-layout-fixed-panel" data-sima-layout-init='{"height_cut":5}'>
                        <span class="theme-tab-building-site-diary-work-description-header-title"><?=Yii::t('BuildingsModule.BuildingSiteDiary', 'WorkDescription')?></span>
                        <sima-button
                            v-bind:class="edit_work_description ? '_pressed' : ''"
                            v-on:click="onClickWorkDescriptionEdit"
                            title="<?=Yii::t('BuildingsModule.BuildingSiteDiary', 'EditWorkDescription')?>"
                        >
                            <span class="sima-old-btn-title">
                                <?=Yii::t('BuildingsModule.BuildingSiteDiary', 'EditWorkDescription')?>
                            </span>
                        </sima-button>
                    </div>
                    <div class="theme-tab-building-site-diary-work-description-body sima-layout-panel">
                        <h3 v-if="!edit_work_description && !has_building_site_diary_work_description">
                            <?=Yii::t('BuildingsModule.BuildingSiteDiary', 'EmptyWorkDescription')?>
                        </h3>
                        <template v-else>
                            <tinymce 
                                v-if="edit_work_description"
                                v-model="local_building_site_diary_work_description"
                                v-bind:tinymce_params="tinymce_params" 
                            >
                            </tinymce>
                            <tinymce-viewer v-else v-html="building_site_diary.work_description">
                            </tinymce-viewer>
                        </template>
                    </div>
                </div>
            </div>
            <div class="theme-tab-building-site-diary-remarks sima-layout-panel" data-sima-layout-init='{"proportion":0.4}'>
                <div class="theme-tab-building-site-diary-remarks-wrap sima-layout-panel _horizontal" data-sima-layout-init='{"width_cut":20, "height_cut":20}'>
                    <div class="theme-tab-building-site-diary-remarks-header sima-layout-fixed-panel" data-sima-layout-init='{"height_cut":5}'>
                        <span class="theme-tab-building-site-diary-remarks-header-title"><?=Yii::t('BuildingsModule.BuildingSiteDiary', 'Remarks')?></span>
                        <sima-button
                            v-bind:class="edit_remarks ? '_pressed' : ''"
                            v-on:click="onClickRemarksEdit"
                            title="<?=Yii::t('BuildingsModule.BuildingSiteDiary', 'EditRemarks')?>"
                        >
                            <span class="sima-old-btn-title">
                                <?=Yii::t('BuildingsModule.BuildingSiteDiary', 'EditRemarks')?>
                            </span>
                        </sima-button>
                    </div>
                    <div class="theme-tab-building-site-diary-remarks-body sima-layout-panel">
                        <h3 v-if="!edit_remarks && !has_building_site_diary_remarks">
                            <?=Yii::t('BuildingsModule.BuildingSiteDiary', 'EmptyRemarks')?>
                        </h3>
                        <template v-else>
                            <tinymce 
                                v-if="edit_remarks"
                                v-model="local_building_site_diary_remarks"
                                v-bind:tinymce_params="tinymce_params" 
                            >
                            </tinymce>
                            <tinymce-viewer v-else v-html="building_site_diary.remarks">
                            </tinymce-viewer>
                        </template>
                    </div>
                </div>
            </div>
        </div>
    </div>
</script>
