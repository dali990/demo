<?php

class BuildingProjectContent extends SIMAActiveRecordBehavior
{
    private $_with_mod;
    
    public function __construct($with_modification = false)
    {
        $this->_with_mod = $with_modification;
    }
    
    public function getContent($parent_code)
    {
        $base_project = $this->owner;
        if (is_null($parent_code))
        {
            //Osnovni projekat
            $subtree = $this->getBaseProjectTree();
        }
        elseif(is_numeric($parent_code))
        {
            //podstavka
            $subtree = $this->getSubProjectTree($parent_code);   
        }
        else
        {
            //sustinski bi trebalo da pukne ili je kraj
            $subtree = [];
        }
        
        return $subtree;
    }
    
    private function getBaseProjectTree()
    {
        $_bp = $this->owner;
        $subtree = [];
        $project_books = Yii::app()->getModule('buildings')->params['project_books'];
        foreach ($project_books as $key => $value)
        {
            $i = $value['order'];
            $child = 'child_'.$i;
            if (isset($_bp->$child))
            {
                $_child = $_bp->$child;
                $subtree[] = [
                    'code' => $_child->id,
//                    'id' => $_child->id,
                    'title' => [
                        'text' => $_child->DisplayName,
                        'parts' => [],
                    ],
                    'data' => [
                        'project_id' => $_child->id,
                    ]
                ];
            }
            elseif ($this->_with_mod)
            {
                $subtree[] = [
                    'code' => $_bp->id,
                    
                    'title' => [
                        'text' => 'Dodaj knjigu '.$i.' - '.$value['display_name'],
                        'class' => 'sima-build-add-bp',
//                        'parts' => [],
                    ],
                    'data' => [
                        'action' => 'ADD_BOOK',
                        'book_type' => BuildingProject::$PREFIX_BOOK,
                        'book_order' => $i,
                        'display_name' => $value['display_name']
                    ]
                ];
            }
        }
        
        foreach ($_bp->children_other as $_child)
        {
            $subtree[] = [
                'code' => $_child->id,
//                    'id' => $_child->id,
                'title' => [
                    'text' => $_child->name,
//                    'parts' => [],
                ],
                'data' => [
                    'project_id' => $_child->id,
                ]
            ];
        }
        
        if ($this->_with_mod)
        {
            $subtree[] = [
                'code' => $_bp->id,
                'title' => [
                    'text' => 'Dodaj elabort',
                    'class' => 'sima-build-add-bp',
    //                'parts' => [],
                ],
                'data' => [
                    'action' => 'ADD_BOOK',
                    'book_type' => BuildingProject::$PREFIX_BOOK,
                    'display_name' => 'Elaborat o ...'
                ]
            ];

            $subtree[] = [
                'code' => $_bp->id,
    //            'class' => 'sima-build-add-bp',
                'title' => [
                    'text' => 'Dodaj studiju',
                    'class' => 'sima-build-add-bp',
    //                'parts' => [],
                ],
                'data' => [
                    'action' => 'ADD_BOOK',
                    'book_type' => BuildingProject::$PREFIX_BOOK,
                    'display_name' => 'Studija o ...'
                ]
            ];
        }
        
        return $subtree;
    }
    
    private function getSubProjectTree($parent_code)
    {
        $current_project = BuildingProject::model()->findByPkWithCheck($parent_code);
        $subtree = [];
            
        foreach ($current_project->children as $child)
        {
            switch ($child->prefix)
            {
                case 'text': 
                    $document_type_id = Yii::app()->configManager->get('buildings.document_type_for_text', false); 
                    $file_template_id = Yii::app()->configManager->get('buildings.file_template_for_text', false); 
                    break;
                case 'numeric': 
                    $document_type_id = Yii::app()->configManager->get('buildings.document_type_for_numeric', false); 
                    $file_template_id = Yii::app()->configManager->get('buildings.file_template_for_numeric', false); 
                    break;
                case 'graphic': 
                    $document_type_id = Yii::app()->configManager->get('buildings.document_type_for_graphic', false); 
                    $file_template_id = Yii::app()->configManager->get('buildings.file_template_for_graphic', false); 
                    break;
                default: 
                    $document_type_id = null; 
                    $file_template_id = null; 
                    break;
            }
            
            $_tag = $child->tag;
            
            $subtree[] = [
                'code' => $child->id,
                'tag_query' => $child->tag->query,
                'title' => [
                    'text' => $child->DisplayName,
                    'parts' => [],
                ],

                'data' => [
                    'project_id' => $child->id,
                    'tag_query' => $_tag->query,
                    'tag_id' => $_tag->id,
                    'tag_display' => $_tag->DisplayName,
                    'document_type_id' => $document_type_id,
                    'document_type_id' => $document_type_id,
                    'responsible_id' => Yii::app()->user->id,
                    'file_template_id' => $file_template_id
//                            'confirmed' => ,
                ]
            ];
        }
        if (
                $this->_with_mod 
                && empty($subtree) 
                && !in_array($current_project->prefix, BuildingProject::$ADDITIONAL_CONTENT_PREFIXES)
            )
        {
            $subtree[] = [
                'code' => $parent_code,
                'title' => [
                    'text' => 'Dodaj Text/Numerika/Grafika',
                    'class' => 'sima-build-add-bp',
//                            'parts' => [],
                ],
                'data' => ['action' => 'ADD_TNG']
            ];
        }
        
        return $subtree;
    }
    
    public function doAction()
    {
        
    }
}