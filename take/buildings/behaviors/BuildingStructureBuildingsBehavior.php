<?php

class BuildingStructureBuildingsBehavior extends SIMAActiveRecordBehavior
{
    public function relations($event)
    {
        $event->addResult([
//            'building_projects'=>[SIMAActiveRecord::HAS_MANY, 'BuildingProject', 'theme_id'],
//            'building_projects_count'=>[SIMAActiveRecord::STAT, 'BuildingProject', 'theme_id', 'select' => 'count(*)'],
        ]);
    }
    
    public function tabs($event)
    {
        $event->addResult([
            [
                'title' => Yii::t('BuildingsModule.BuildingProject', 'BuildingProjects'),
                'code' => 'buildings_projects',
                'module_origin' => 'buildings',
                'order' => 50,
            ]
        ]);
    }
}
