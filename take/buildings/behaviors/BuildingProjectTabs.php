<?php

/**
 * Description of BuildingProjectTabs
 *
 * @author goran-set
 */
class BuildingProjectTabs extends SIMAActiveRecordTabs
{
    protected function tabsDefinition()
    {
        $owner = $this->owner;
        $tag = $owner->tag;
        $project_tabs = [
            [
                'title' => 'Sadržaj',
                'code'=>'the_content',
                'reload_on_update' => true
            ],
            [
                'title' => 'Fajlovi',
                'code'=>'files_for_tag',
                'action'=>'guitable',
                'get_params'=>array(
                    'settings' => array(
                        'model' => 'File',
                        'fixed_filter' => array('belongs' => $tag->query),
                        'add_button' => array(
                            'init_data'=>array(
                                'File' => array(
                                    'file_belongs'=>array(
                                        'ids'=>array(
                                            'ids'=>array(
                                                array(
                                                    'id'=>$tag->id,
                                                    'display_name'=>$tag->DisplayName,
                                                    'class'=>'_visible'
                                                )
                                            )
                                        )
                                    )
                                )
                            )
                        )
                    )
                ),
            ]
        ];
        
//        if (Yii::app()->user->id === $owner->coordinator_id)
        $project_tabs[] = [
            'title' => 'Izmena sadržaja',
            'code'=>'the_content_modification',
            'reload_on_update' => true,
            'pre_path' => 'the_content'
        ];
        
        return $project_tabs;
    }
    
}
