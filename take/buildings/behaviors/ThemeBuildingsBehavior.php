<?php

class ThemeBuildingsBehavior extends SIMAActiveRecordBehavior
{
    public function relations($event)
    {
        $event->addResult([
            'building_construction' => [SIMAActiveRecord::HAS_ONE, BuildingConstruction::class, 'id'],
            'building_projects'=>[SIMAActiveRecord::HAS_MANY, 'BuildingProject', 'theme_id'],
            'building_projects_count'=>[SIMAActiveRecord::STAT, 'BuildingProject', 'theme_id', 'select' => 'count(*)'],
            'bill_of_quantities'=>[SIMAActiveRecord::HAS_MANY, BillOfQuantity::class, 'theme_id']
        ]);
    }
    
    public function tabs($event)
    {
        $owner = $this->owner;
        
        $building_project_level = $owner->building_projects_count>0 ? '' : 'others' ;
        
        $theme_tabs = [
//            [
//                'title' => Yii::t('BuildingsModule.BuildingProject', 'BuildingProjects'),
//                'code' => 'buildings_projects',
//                'module_origin' => 'buildings',
//                'order' => 50,
//                'pre_path' => $building_project_level,
//                'params_function' => 'paramsBuildingsProjects'
//            ],
//            [
//                'title' => Yii::t('BuildingsModule.ConstructionMaterialApproval', 'ConstructionMaterialApprovals'),
//                'code' => 'material_approvals',
//                'module_origin' => 'buildings',
//                'pre_path' => 'others',
//            ]
        ];
        
        $bills_of_quantities_subtree = [];
        $bills_of_quantities = BillOfQuantity::model()->findAll([
            'model_filter' => [
                'AND',
                [
                    'theme' => [
                        'ids' => $owner->id
                    ]
                ],
                [
                    'scopes' => [
                        'byName',
                        'hasAccess' => Yii::app()->user->id
                    ]
                ]
            ]
        ]);
        foreach ($bills_of_quantities as $bill_of_quantity)
        {
            if (empty($owner->building_construction) || $owner->building_construction->bill_of_quantity_id !== $bill_of_quantity->id)
            {
                array_push($bills_of_quantities_subtree, [
                    'title' => Yii::t('BuildingsModule.BillOfQuantity', 'BillOfQuantityShort') . ': ' . $bill_of_quantity->name,
                    'code' => "{$bill_of_quantity->id}_{$bill_of_quantity->name}",//mora i kolona name jer se SIMATree2 ne update-uje lepo, pa se nece update-ovati naziv, odnosno radi update samo ako je promenjen code
                    'action' => 'buildings/billOfQuantity/viewBillOfQuantity',
                    'get_params' => [
                        'bill_of_quantity_id' => $bill_of_quantity->id
                    ]
                ]);
            }
        }
        
        //ako je tema izvodjenje radova
        if (!empty($owner->building_construction))
        {
            $construction_tab = [
                'title' => Yii::t('BuildingsModule.BillOfQuantity', 'Constructions'),
                'code' => 'building_construction',
                'order' => 100,
                'module_origin' => 'buildings',
                'subtree' => [
                    [
                        'title' => Yii::t('BuildingsModule.BuildingSiteDiary', 'Diary'),
                        'code' => 'building_site_diaries',
                        'action' => 'buildings/buildingConstruction/buildingSiteDiaries',
                        'get_params' => [
                            'building_construction_id' => $owner->building_construction->id
                        ]
                    ],
                    [
                        'title' => Yii::t('BuildingsModule.BuildingInterimBill', 'BuildingInterimBills'),
                        'code' => "building_interim_bills",
                        'action' => 'buildings/buildingConstruction/buildingInterimBills',
                        'get_params' => [
                            'building_construction_id' => $owner->building_construction->id
                        ]
                    ],
                    [
                        'title' => Yii::t('BuildingsModule.BillOfQuantity', 'OtherBillOfQuantities'),
                        'code' => 'constructions',
                        'action' => 'buildings/buildingConstruction/buildingConstructionTab',
                        'get_params' => [
                            'theme_id' => $owner->id,
                            'without_bill_of_quantity_id' => $owner->building_construction->bill_of_quantity_id
                        ],
                        'subtree' => $bills_of_quantities_subtree
                    ]
                ]
            ];
        }
        else
        {
            $construction_tab = [
                'title' => Yii::t('BuildingsModule.BillOfQuantity', 'Constructions'),
                'code' => 'constructions',
                'order' => 100,
                'action' => 'buildings/buildingConstruction/buildingConstructionTab',
                'get_params' => [
                    'theme_id' => $owner->id
                ],
                'subtree' => $bills_of_quantities_subtree
            ];
            
            if (empty($bills_of_quantities_subtree))
            {
                $construction_tab['pre_path'] = 'others';
            }
        }

        array_push($theme_tabs, $construction_tab);
        
        $event->addResult($theme_tabs);
    }
    
    public function paramsBuildingsProjects()
    {
        $owner = $this->owner;
        
        if ($owner->coordinator_id != Yii::app()->user->id)
        {
            $add_button = false;
        }
        else
        {
            $add_button = [
                'init_data' => [
                    'BuildingProject' => [
                        'theme' => ['ids' => $owner->id],

                    ]
                ]
            ];
        }
        
        return [
            'add_button' => $add_button
        ];
    }
    
    public function views($event)
    {
        $owner = $this->owner;
        
        if($event->params['view'] === 'basic_info')
        {
            if (!empty($owner->building_construction))
            {
                $event->addResult([
                    [
                        'view' => 'basic_info',
                        'html' => Yii::app()->controller->renderModelView($owner->building_construction, 'inTheme')
                    ]
                ]);
            }
        }
    }
}
