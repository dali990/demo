<?php

class CompanyLicenseToCompanyBehavior extends SIMAActiveRecordBehavior
{
    public function relations($event)
    {
        $company_licenses_to_projects_table = CompanyLicenseToProject::model()->tableName();
        $event->addResult([
            'projects'=>[SIMAActiveRecord::MANY_MANY, 'BuildingProject', "$company_licenses_to_projects_table(company_license_id,project_id)"]
        ]);
    }        
}