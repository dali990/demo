<?php

class UserBuildingsBehavior extends SIMAActiveRecordBehavior
{
    public function relations($event)
    {
        $event->addResult([
            'user_to_bill_of_quantities' => [SIMAActiveRecord::HAS_MANY, BillOfQuantityToUser::class, 'user_id'],
            'boss_assistant_to_building_constructions' => [SIMAActiveRecord::HAS_MANY, BuildingConstructionToBossAssistant::class, 'boss_assistant_id']
        ]);
    }
}
