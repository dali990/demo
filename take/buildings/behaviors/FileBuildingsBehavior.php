<?php

/**
 * @package SIMA
 * @subpackage Accounting
 */
class FileBuildingsBehavior extends SIMAActiveRecordBehavior
{
    
    public function relations($event)
    {
        $event->addResult(array(
            'building_project'=>array(SIMAActiveRecord::HAS_ONE, 'BuildingProject', 'responsible_designers_decision_id'),
            'construction_material_approval'=>array(SIMAActiveRecord::HAS_ONE, 'ConstructionMaterialApproval', 'id'),
            'building_interim_bill' => [SIMAActiveRecord::HAS_ONE, BuildingInterimBill::class, 'id'],
            'building_site_diary' => [SIMAActiveRecord::HAS_ONE, BuildingSiteDiary::class, 'id']
        ));
    }
    
    public function views($event)
    {
        $owner = $this->owner;

        if($event->params['view'] === 'basic_info')
        {
            if (!empty($owner->building_interim_bill))
            {
                $event->addResult([
                    [
                        'view' => 'basic_info',
                        'html' => Yii::app()->controller->renderModelView($owner->building_interim_bill, 'inFile')
                    ]
                ]);
            }
            if (!empty($owner->building_site_diary))
            {
                $event->addResult([
                    [
                        'view' => 'basic_info',
                        'html' => Yii::app()->controller->renderModelView($owner->building_site_diary, 'inFile')
                    ]
                ]);
            }
        }
    }
}