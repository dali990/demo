<script type="text/x-template" id="sima-search-field">
    <div class="sima-search-field" v-bind:class="{'_disabled': settings.disabled}" v-click-outside="onClickOutside">
        <input
            class="ssf-search-field"
            v-bind:placeholder="$attrs.placeholder"
            v-model="search"
            v-on="scope.events"
            ref="searchInptuField"
            v-bind:disabled="settings.disabled"
        />
        
        <span class="sima-icon _search_form _16" 
            v-if="field_settings.open_dialog"
            @click="openDialog">
        </span>
        <sima-button v-if="field_settings.open_form" v-on:click="openForm" class="ssf-form-btn">
            <span class="sima-btn-title"><i class="fa fa-plus" style="float:right"></i></span>
        </sima-button>
        
        <input type="hidden" v-bind:name="$attrs.name" v-bind:value="input_value"/>
        
        <sima-search-field-result-list ref="ssfResultListComponent"             
            v-show="open_result_list"
            v-bind:results="results"
            v-bind:search="search"
            v-bind:ssf_bus="ssf_bus"
            v-bind:model_name="model_name"
            v-bind:total="total"
            @selectedItem="selectedItem">
        </sima-search-field-result-list>
        
    </div>
</script>