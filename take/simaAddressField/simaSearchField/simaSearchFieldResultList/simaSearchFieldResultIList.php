<script type="text/x-template" id="sima-search-field-result-list">
    <div class="sima-search-field-result-list">
        <ul v-bind:class="model_name+'_list'" v-show="results.length > 0" ref="results">
            <li v-bind:class="'result-item '+model_name+'_'+result.id+' item_index_'+(i+1)" v-for="(result, i) in results"
                v-on:click="selectItem(i, result.id)">
                <div style="display:inline" v-html="hits(result.display_name, i)"></div>
            </li>
        </ul>
        <hr>
        <div class="ssf-total-results" v-html="totalResultsDisplay"></div>
    </div>
</script>