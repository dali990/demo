
/* global Vue, sima, mixin_search_simaSearchField */
Vue.component('sima-address-field', {
    template: '#sima-address-field',
    name: 'sima-address-field',
    inheritAttrs: false,
    props: {
        value: {default: () => ({ label: '', id: '' }) },
        model_filter: { type: Object, default() { return {}; } },
        search_min_char: { type: Number, default: 2 },
        show_add_new_item: { type: Boolean, default: true }
    },
    data: function () {
        return {
            search: "",
            input_value: '', // Internal value managed by saf if no `value` prop is passed
            saf_result_list: null, //sima_address_field_result_list dom object
            open_result_list: false,
            results: [],
            selected_item: null,
            result_list_html: null,
            initial_text_search_value: "",
            initial_id_value: "",
            text_search_prev_value: "",
            id_prev_value: "",
            saf_bus: new Vue(),
            saved_address: null, //ako je sacuvana adresa onda nece vratiti inicijalnu vrednost polja
            model_name: 'Address', // inicijalno kada rosledimo samo id onda vue_model je 'Address', kada selektujemo onda vue_model je 'Street'
            selected_id: "", //id postavljenog modela (inicijalno Address, tokom pretrage Street)
            street_model: null,
            opened_ssf_form: false //cuva podatak da li je otvoren dialog(lupica od sima search field komponente) 
        };
    },
    computed: {
        scope() {
            return {
                events: {
                    'keydown': this.onKeyDown,
                    'keyup': this.onKeyUp,
                    'blur': this.onBlur,
                    'focus': this.onFocus,
                    'click': this.onClick
                }
            };
        },
        update_label_on_init(){
            if((!this.value.hasOwnProperty('label') || this.value.label === "") && this.selected_id !== "")
            {
                return true;
            }
            return false;
        },
        update_label(){
            if(this.selected_id !== "")
            {
                return true;
            }
            return false;
        },
        vue_model(){
            if(this.update_label || this.update_label_on_init)
            {
                var vue_model = sima.vue.shot.getters.model(this.model_name+'_'+this.selected_id);
                return vue_model;
            }
            return null;
        },
        display_label(){
            if(this.vue_model)
            {
                var display_label = this.vue_model.DisplayName;
                return display_label;
            }
            return this.search;
        }
    },
    watch: {
        display_label(new_val, old_val){
            this.search = new_val;
            if(!this.initial_text_search_value && this.vue_model && this.vue_model.shotStatus('DisplayName') === 'OK')
            {
                this.initial_text_search_value = new_val;
            }
        }
    },
    created() {
        window.addEventListener('mousedown', this.onMouseDown);
    },
    destroyed() {
        if(this.saf_result_list)
        {
            this.saf_result_list.remove();
        }
        window.removeEventListener('mousedown', this.onMouseDown);
    },
    mounted: function () {
        var _this = this;
        this.result_list_html = this.$refs.safResultListComponent.$el;
        var _result_list_html = this.result_list_html;
        this.saf_result_list = document.body.appendChild(this.result_list_html);

        $(this.$el).on('destroyed', function () {
            _this.$destroy();
            _result_list_html.remove();
        });
        this.init();
    },
    methods: {
        init(){
            if (this.value.id)
            {
                this.updateField(this.value.label, this.value.id);
                this.input_value = this.value.id;//inicijalno postavljanje id adrese               
                this.initial_id_value = this.value.id;//inicijalno postavljanje id bilo kog modela
                this.selected_id = this.value.id;
            }
            if (this.value.label)
            {
                this.search = this.value.label;
                this.text_search_prev_value = this.value.label;
                this.initial_text_search_value = this.value.label;
            }
            //Inicijalno ako treba dovuci Address model
            if(this.update_label_on_init)
            {
                this.model_name = 'Address';
            }
          
            this.saf_bus.$on('bus_event_close_result_list', (data) => {
                if(this.saf_result_list)
                {
                    this.open_result_list = false;
                }
                this.revertField();
            });
            this.saf_bus.$on('bus_event_blur_field', (field) => {
                this.$refs.searchInptuField.blur();
            });                        
            this.saf_bus.$on('bus_event_focus_field', (field) => {
                this.$refs.searchInptuField.focus();
            });                        
            this.saf_bus.$on('bus_event_opened_ssf_form', (field) => {
                this.opened_ssf_form = true;
            });                        
            this.saf_bus.$on('bus_event_closed_ssf_form', (field) => {
                this.opened_ssf_form = false;
            });                        
        },
        searchText() {
            var _this = this;
            var globalTimeout = this.search_timeout;
            if (globalTimeout !== null) {
                clearTimeout(globalTimeout);
                this.search_timeout = globalTimeout;
            }
            
            this.search_timeout = setTimeout(function () {
                if(_this.hasSearchTextChanged())//ako je zaista promenjena vrednost
                {
                    this.results = [];
                    var _model_filter = Object.assign(_this.model_filter, {text: _this.search.replace(/\,/g, '')});
                    if (_this.search.trim() !== '' && _this.search.length >= _this.search_min_char) {
                        sima.ajax.get('base/model/simaSearchModel', {
                            get_params: {
                                model_name: 'Street'
                            },
                            data: {
                                model_filter: _model_filter
                            },
                            async: true,
                            success_function: function (response) {
                                _this.results = response.result;
                            }
                        });
                    }
                    _this.renderResults();
                }
            }, 500);
        },
        renderResults() {
            if(this.search.trim() !== '')
            {
                var address_field_position = this.$el.getBoundingClientRect();
                this.saf_result_list.style.top = address_field_position.top + 18 + "px";
                this.saf_result_list.style.left = address_field_position.left + "px";
                this.open_result_list = true;
            }
        },
        onKeyDown(e) {
            if(e.target.value !== "")
            {
                this.text_search_prev_value = e.target.value;
            }
        },
        onKeyUp(e) {
            var _this = this;
            if(e.keyCode === 27)//esc
            {
                if(this.saf_result_list && this.open_result_list)
                {
                    this.open_result_list = false;
                    e.stopPropagation();
                    e.preventDefault();
                }
                e.target.blur();
            }
            else if(e.keyCode === 9)//tab
            {
                e.preventDefault();
            }
            else if(e.keyCode === 13)//enter
            {
                if(this.saf_result_list && this.open_result_list)
                {
                    this.saf_bus.$emit('bus_event_enter', {});
                    e.stopPropagation();
                    e.preventDefault();
                }
            }
            else
            {
                this.selected_item = null;
                this.street_model = null;
                this.open_result_list = false;
                _this.searchText();
            }
            this.$emit('keyup', e);
        },
        onBlur(e) {
            if(e.relatedTarget && !this.opened_ssf_form)
            {
                //Ignorisemo zatvaranje liste ako je fokus na polje broj ulice
                if(!e.relatedTarget.classList.contains('saf-search-field') && !e.relatedTarget.classList.contains('ssf-search-field'))
                { 
                    this.open_result_list = false;
                    this.revertField();
                }
            }
        },
        onFocus(e) {
            this.renderResults();
        },
        onClick(e){
            this.searchText();
        },
        onMouseDown(e){
            if(!this.opened_ssf_form)
            {
                this.onClickOutside(e);
            }
        },
        onClickOutside: function (e) {
            if(this.saf_result_list && !this.opened_ssf_form)
            {
                if (
                        !e.target.closest(".sima-address-field") && 
                        !e.target.closest(".sima-address-field-result-list") &&
                        !e.target.closest(".sima-search-field") && 
                        !e.target.closest(".sima-search-field-result-list") &&
                        !e.target.closest(".ssf-form-btn")
                    ) 
                {
                    this.open_result_list = false;
                    //Regulisemo prikaz teksta u polje adrese, dopustanje izmene i upisa id samo kada je dodata adresa
                    this.revertField();
                }
            }
            if(this.opened_ssf_form)
            {
                this.opened_ssf_form = false;
            }
        },
        savedAddressForm: function (address) {
            this.open_result_list = false;
            this.search = address.DisplayName;
            this.input_value = address.address_id;
            this.saved_address = {id: address.address_id, label: address.DisplayName};
            this.$emit('input', this.saved_address);
        },
        //selektujemo ulicu, prikazujemo privremeno samo display za Street dok se ne sacuva adresa
        selectedItem(i, id) {
            this.selected_item = this.results[i];
            this.updateField("", "");
            this.model_name = 'Street';
            this.selected_id = this.results[i].id;
            this.street_model = this.vue_model;
            if (this.results[i].display_name !== this.search) {
                this.searchText();
            }
            //this.$emit('input', {id: this.input_value, label: this.search, additional_data: this.results[i].additional_data});
            this.open_result_list = true;
        },
        updateField(label, id) {
            this.search = label;
            this.input_value = id;
        },
        //koristi se u simaDependentFields.js
        emptyField() {
            this.selected_id = "";
            this.search = "";
            this.input_value = "";
        },
        hasSearchTextChanged(){
            return this.text_search_prev_value !== this.search || false;
        },
        revertField(){
            if(this.saved_address)
            {
                if(this.saved_address.label !== this.search)//izmenjen je tekst ali nije dodata adresa
                {
                    this.search = this.saved_address.label;
                    this.input_value = this.saved_address.id;
                    this.selected_item = null;
                }
            }
            else
            {
                if(this.initial_text_search_value !== this.search)//izmenjen je tekst ali nije dodata adresa
                {
                    this.search = this.initial_text_search_value;
                    this.input_value = this.initial_id_value;
                    this.selected_item = null;
                }
            }
            this.model_name = 'Address';
            this.selected_id = this.input_value;
        } 
    }
});