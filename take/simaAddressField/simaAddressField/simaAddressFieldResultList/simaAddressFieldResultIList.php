<script type="text/x-template" id="sima-address-field-result-list">
    <div class="sima-address-field-result-list">
        <div v-show="show_add_new_item && add_new_display !=='' && (results.length == 0 || search !== '')" 
            class="saf-add-new-item" 
            v-on:click="addNewAddress"
            ref="add_new_item">
            {{add_new_display}}<i class="fa fa-plus" style="float:right"></i>
        </div>
        <ul class="result-list" v-show="results.length > 0 && !show_form" ref="results">
            <li v-bind:class="'result-item model_'+result.id+' item_index_'+(i+1)" v-for="(result, i) in results"
                v-on:click="selectItem(i, result.id)">
                <div style="display:inline" v-html="hits(result.display_name, i)"></div>
            </li>
        </ul>
        <div v-if="show_form" style="width: 100%" >
            <div class="saf-form" >
                <table>
                    <tr>
                        <td style="text-align: right;">
                            <?=Yii::t('GeoModule.Street','Street')?> <span class="required">*</span>
                        </td>
                        <td>
                        <sima-search-field
                            name="street_id"
                            class="sima-address-field"
                            v-bind:model_name="'Street'"
                            v-bind:model_filter="Street.model_filter"
                            v-bind:settings="Street.settings""
                            v-model="Street.model"
                            @input="streetChange"
                        >
                        </sima-search-field>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right;">
                            <?=Yii::t('GeoModule.City', 'City')?> <span class="required">*</span>
                        </td>
                        <td>
                        <sima-search-field
                            name="city_id"
                            class="sima-address-field"
                            v-bind:model_name="'City'"
                            v-bind:model_filter="City.model_filter"
                            v-bind:settings="City.settings"
                            v-model="City.model"
                            @selected="cityChange"
                            @openForm="openForm"
                            @savedForm="savedForm"
                            @closedForm="closedForm"
                        >
                        </sima-search-field>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right;">
                            {{street_number_label}} <span class="required">*</span>
                        </td>
                        <td>
                            <div class="sima-address-field">
                                <input type="text" class="sima-address-field street-number" v-model="street_number" autofocus ref="streetNumber" style="width: 56px"/>
                                {{floor_label}}  
                                <input type="text" class="sima-address-field street-number" v-model="floor" style="width: 58px"/>
                                {{door_number_label}}
                                <input type="text" class="sima-address-field" v-model="door_number" style="width: 56px"/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right;">
                            <?=Yii::t('GeoModule.PostalCode','PostalCode')?> <span class="required">*</span>
                        </td>
                        <td>
                        <sima-search-field
                            name="postal_code_id"
                            class="sima-address-field"
                            v-bind:model_name="'PostalCode'"
                            v-bind:model_filter="PostalCode.model_filter"
                            v-bind:settings="PostalCode.settings"
                            v-model="PostalCode.model"
                            @selected="postalCodeChange"
                            @openForm="openForm"
                            @savedForm="savedForm"
                            @closedForm="closedForm"
                        >
                        </sima-search-field>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right;">
                            <?=Yii::t('GeoModule.Municipality', 'Municipality')?> 
                        </td>
                        <td>
                            <sima-search-field
                                name="municipality_id"
                                class="sima-address-field"
                                v-bind:model_name="'Municipality'"
                                v-bind:model_filter="Municipality.model_filter"
                                v-bind:settings="Municipality.settings"
                                v-model="Municipality.model"
                                @selected="municipalityChange"
                                @openForm="openForm"
                                @savedForm="savedForm"
                                @closedForm="closedForm"
                            >
                            </sima-search-field>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right;">
                            <?=Yii::t('GeoModule.Country', 'Country')?> <span class="required">*</span>
                        </td>
                        <td>
                            <sima-search-field
                                name="country_id"
                                class="sima-address-field"
                                v-bind:model_name="'Country'"
                                v-bind:model_filter="Country.model_filter"
                                v-model="Country.model"
                                v-bind:settings="Country.settings"
                                @selected="countryChange"
                                @openForm="openForm"
                                @savedForm="savedForm"
                                @closedForm="closedForm"
                            >
                            </sima-search-field>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right;">
                            <?=Yii::t('BaseModule.Common', 'Description')?>
                        </td>
                        <td>
                            <div class="sima-address-field">
                                <textarea class="sima-address-field" v-model="description"></textarea>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align: right; padding-right: 23px;">
                        <sima-button
                            style="float:right"
                            v-on:click="saveNewItem"
                            v-bind:class="{_disabled: !can_save}"
                            title="Dodaj"
                        >
                            <span class="sima-btn-title"><?=Yii::t('GeoModule.Address', 'Add')?></span>
                        </sima-button>
                        </td>
                    </tr>
                </table>

            </div>

        </div>
    </div>
</script>