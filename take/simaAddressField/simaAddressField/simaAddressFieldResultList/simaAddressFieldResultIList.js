
/* global Vue, sima */
Vue.component('sima-address-field-result-list', {
    template: '#sima-address-field-result-list',
    props: {
        results: {
            type: Array,
            default() { return []; }
        },
        street_model: {
            validator: sima.vue.ProxyValidator
        },
        search: { type: String, default: '' },
        initial_text_search_value: { type: String, default: '' },
        input_value: { default: '' },
        saved_address: { type: Object, default: null },
        selected_item: { type: Object, default: null }, 
        show_add_new_item: { type: Boolean, default: false },//Prikaz elementa za otvaranje forme
        saf_bus: {type: Object}
    },
    data: function () {
        return {
            show_form: false,
            search_params: {}, //cuva parametre za odgovarajuce modele koje izvlaci iz 'search' propa
            street_number: "",
            floor: "",
            door_number: "",
            street_number_label: sima.translate('GeoNumber'),
            floor_label: sima.translate('GeoFloor'),
            door_number_label: sima.translate('GeoAppartmentNumber'),
            description: "",
            Street: {
                model: {},
                model_filter: {},
                selected_id: "",
                settings: {
                    display_label: 'name',
                    open_dialog: false,
                    //open_form: false,
                    text_field: true
                }
            },
            City: {
                model: {},
                model_filter: {},
                //kada se selektuje drugo naselje, sluzi samo da kaze componenti da ne updateuje podatke preko sacuvane adrese (dovlacenjem address vue s moela)
                //nego da dovuce novi city model pa iz njega da updateuje ostala polja
                selected_id: "",
                settings: {
                    display_label: 'name',
                    //customClickOutside: true,
                    gui_table: {
                        dialog_title: sima.translate('GeoCity')
                    }
                }
            },
            PostalCode: {
                model: {},
                model_filter: {},
                selected_id: "",
                settings: {
                    display_label: 'code',
                    gui_table: {
                        dialog_title: sima.translate('GeoPostalCode'),
                        fixed_filter: {}
                    }
                }
            },
            Municipality: {
                model: {},
                model_filter: {},
                selected_id: "",
                settings: {
                    display_label: 'name',
                    gui_table: {
                        dialog_title: sima.translate('GeoMunicipality')
                    }
                }
            },
            Country: {
                model: {},
                model_filter: {},
                selected_id: "",
                settings: {
                    display_label: 'name',
                    gui_table: {
                        dialog_title: sima.translate('GeoCountry')
                    }
                }
            },
            // navigacija za selektovanje stavke u listi rezultata. -1 deselect, 0 add_new_item/addNewAddress/dodavanje nove stavke, 1> stavke liste rezultata
            key_navigation_position: -1, 
            add_new_display: ""
        };
    },
    computed: {
        can_save(){
            if(
                    this.street_number.trim() !== "" &&
                    (this.Street.model.id || this.Street.model.label) &&
                    (this.PostalCode.model.id) &&
                    (this.City.model.id) &&
                    (this.Country.model.id)
                )
            {
                return true;
            }
            return false;
        },
        field_changed(){
            if(this.Street.selected_id || this.City.selected_id || this.PostalCode.selected_id || this.Municipality.selected_id || this.Country.selected_id)
            {
                return true;
            }
            return false;
        },
        street_name(){
            if(!this.field_changed && this.street_model && this.street_model.name)
            {
                return this.street_model.name;
            }
        },
        street(){
            if(this.Street.selected_id !== "")
            {
                return sima.vue.shot.getters.model('Street_'+this.Street.selected_id);
            }
            return null;
        },
        city(){
            var city = null;
            if(!this.field_changed && this.street_model && this.street_model.city)
            {
                city = this.street_model.city;
            }
            else if(this.street)
            {
                return this.street.city;
            }
            if(this.City.selected_id !== "")
            {
                city = sima.vue.shot.getters.model('City_'+this.City.selected_id);
            }
            return city;
        },
        postalCode(){
            var street_postal_code = null;
            var city_postal_code = null;
            if(!this.field_changed && this.street_model && this.street_model.postal_code)
            {
                street_postal_code = this.street_model.postal_code;
            }
            else if(this.street)
            {
                return this.street.postal_code;
            }
            if (this.city && !street_postal_code)
            {
                if(this.city.default_postal_code)
                {
                    city_postal_code = this.city.default_postal_code;
                }
                else if(this.city.postal_codes)
                {
                    city_postal_code = this.city.postal_codes[0];
                }
            }
            if(this.PostalCode.selected_id !== "")
            {
                return sima.vue.shot.getters.model('PostalCode_'+this.PostalCode.selected_id);
            }
            
            return street_postal_code || city_postal_code;
        },
        municipality(){
            if(this.city && this.city.municipality)
            {
                return this.city.municipality;
            }
            return null;
        },
        country(){
            if(this.city && this.city.country)
            {
                return this.city.country;
            }
            return null;
        }
    },
    watch: {
        search(new_val, old_val) {
            if(!this.selected_item && (new_val !== old_val))
            {
                this.show_form = false;
                this.key_navigation_position = -1; //deselect
            }
            this.add_new_display = this.generateAddNewItemString();
        },
        street_model(){
            if(this.street_model)
            {
                this.key_navigation_position = -1; //deselect
                this.show_form = true;
                this.focusStreetNumber();
            }
        },
        street_name(){
            var model = {};
            if(this.street_name)
            {
                model = {
                    id: this.street_model.id,
                    label: this.street_name
                };
                
                this.Street.model = model;
            }
        },
        city(){
            if(this.city)
            {
                var model = {
                    id: this.city.id,
                    label: this.city.name
                };
                this.City.model = model;
                Object.assign(this.Street.model_filter, { city:{ids: this.city.id} });
                Object.assign(this.PostalCode.model_filter, {postal_code_to_cities: {city: {ids:this.city.id} }} );
                this.PostalCode.settings.gui_table.fixed_filter = {postal_code_to_cities: {city: {ids:this.city.id} }};
            } 
            if(this.City.selected_id !== "")
            {
                this.Street.model.id = "";
            }
        },
        postalCode(){
            if(this.postalCode)
            {
                var model = {
                    id: this.postalCode.id,
                    label: this.postalCode.code
                };
                this.PostalCode.model = model;
                Object.assign(this.City.model_filter, {city_to_postal_codes:{postal_code:{ ids:model.id} }});
            }
            if(this.PostalCode.selected_id !== "")
            {
                this.Street.model.id = "";
            }
        },
        municipality(){
            if(this.municipality)
            {
                var model = {
                    id: this.municipality.id,
                    label: this.municipality.name
                };
                this.Municipality.model = model;
                Object.assign(this.City.model_filter, { municipality:{ids: this.municipality.id} });
            }
        },
        country(){
            if(this.country)
            {
                var model = {
                    id: this.country.id,
                    label: this.country.name
                };
                this.Country.model = model;
                Object.assign(this.PostalCode.model_filter, {country:{ids:this.country.id}});
                Object.assign(this.City.model_filter, {country:{ids:this.country.id}});
                Object.assign(this.Municipality.model_filter, {country:{ids:this.country.id}});
            }
        }
    },
    created() {
        window.addEventListener('keyup', this.onKeyUp);
        window.addEventListener('keydown', this.onKeyDown);
    },
    destroyed() {
        window.removeEventListener('keyup', this.onKeyUp);
        window.removeEventListener('keydown', this.onKeyDown);
    },
    mounted: function () {
        var _this = this;

        $(this.$el).on('destroyed', function () {
            _this.$destroy();
        });
        
        this.init();
    },
    methods: {
        hits(text, i) {
            var search_mask = "("+this.search+")";
            var reg_ex = new RegExp(search_mask, "ig");
            var replace_mask = "<b>$1</b>";
            return text.replace(reg_ex, replace_mask);
        },
        selectItem(i, id) {
            this.Street.selected_id = "";
            this.City.selected_id = "";
            this.PostalCode.selected_id = "";
            this.Municipality.selected_id = "";
            this.Country.selected_id = "";
            if(!id)
            {
                id = this.results[i].id;
            }
            this.$emit('selectedItem', i, id);
        },
        init(){
            this.saf_bus.$on('bus_event_enter', (data) => {
                if(this.key_navigation_position > 0)
                {
                    this.selectItem(this.key_navigation_position-1);
                }
                else if(this.key_navigation_position === 0)
                {
                    this.key_navigation_position = -1;
                    this.$refs.add_new_item.classList.remove('active-item');
                    this.addNewAddress();
                }
            });
        },
        onKeyDown(e){
            //Izvrsavanje eventova kada je otvorena lista rezultata ili forma
            if(this.$el.style.display !== 'none')
            {
                //Izvrsavanje eventova samo za listu rezultata
                if(!this.show_form)
                {
                    if(e.keyCode === 38)//up
                    {
                        var add_new_item = this.$refs.add_new_item;
                        var ul_list = this.$refs.results;
                        if(this.key_navigation_position >= 1)
                        {
                            for (let item of ul_list.children) {
                                item.classList.remove('active-item');
                            }
                            this.key_navigation_position--;
                            if(this.key_navigation_position > 0)
                            {
                                var list_item = ul_list.children[this.key_navigation_position-1];
                                list_item.classList.add('active-item');
                                if(!this.isScrolledIntoView(list_item))
                                {
                                    list_item.scrollIntoView(false);
                                }
                            }
                            else if(this.key_navigation_position === 0)
                            {
                                add_new_item.classList.add('active-item');
                            }
                        }
                        else if(this.key_navigation_position === 0)
                        {
                            this.key_navigation_position--;
                            add_new_item.classList.remove('active-item');
                            this.saf_bus.$emit('bus_event_focus_field', e.target);
                        }
                        e.stopPropagation();
                        e.preventDefault();
                    }
                    else if(e.keyCode === 40)//down
                    {
                        var ul_list = this.$refs.results;
                        var add_new_item = this.$refs.add_new_item;
                        //focus/blur polja pretrage i selectovanje add_new_item
                        if(this.key_navigation_position === -1)
                        {
                            this.key_navigation_position++;
                            add_new_item.classList.add('active-item');
                            this.saf_bus.$emit('bus_event_blur_field', e.target);
                        }
                        else if(this.key_navigation_position < this.results.length)
                        {
                            add_new_item.classList.remove('active-item');
                            for (let item of ul_list.children) {
                                item.classList.remove('active-item');
                            }
                            this.key_navigation_position++;
                            var list_item = ul_list.children[this.key_navigation_position-1];
                            list_item.classList.add('active-item');
                            if(!this.isScrolledIntoView(list_item))
                            {
                                list_item.scrollIntoView(true);
                            }
                        }
                        e.stopPropagation();
                        e.preventDefault();
                    }
                    
                }
            }
        },
        onKeyUp(e){
            //Izvrsavanje eventova kada je otvorena lista rezultata ili forma
            if(this.$el.style.display !== 'none')
            {
                if(e.keyCode === 27)//esc
                {
                    if(e.target.tagName === "BODY")//ako je ESC nad poljem adresa onda zatvaramo listu rezultata
                    {
                        this.saf_bus.$emit('bus_event_close_result_list');
                    }
                    else
                    {
                        this.saf_bus.$emit('bus_event_blur_field', e.target);
                    }
                    
                }
                else if(e.keyCode === 13)//enter
                {
                    if(this.key_navigation_position > 0)
                    {
                        this.selectItem(this.key_navigation_position-1);
                        this.key_navigation_position = -1;
                    }
                    else if(this.key_navigation_position === 0)
                    {
                        this.key_navigation_position = -1;
                        this.$refs.add_new_item.classList.remove('active-item');
                        this.addNewAddress();
                    }
                }
                e.stopPropagation();
                e.preventDefault();
            }
        },
        addNewAddress() {
            var _this = this;
            var address_id = "";
            if(_this.saved_address && _this.search === _this.saved_address.label)
            {
                address_id = _this.saved_address.id;
            }
            else if(_this.input_value && _this.search === _this.initial_text_search_value)
            {
                address_id = _this.input_value;
            }
            this.resetParams();
            this.fillSearchParams();
            sima.ajax.get('geo/addressField/simaAddressFieldAddItem', {
                data: {
                    model_data: {
                        search_params: _this.search_params,
                        address_id: address_id
                    }
                },
                async: true,
                success_function: function (response) {                       
                    /*Setovanje polja*/
                    if(address_id)
                    {
                        _this.search_params.street_number = response.models_status.street_number;
                        _this.search_params.floor = response.models_status.floor;
                        _this.search_params.door_number = response.models_status.door_number;
                        _this.description = response.models_status.description;
                    }
                    
                    //Ulica
                    var model_status_street = response.models_status.Street;
                    if (model_status_street && model_status_street.status === 'new') 
                    {
                        //Dodavanje nove stavke, definisanje ponasanje polja kao text field
                        _this.Street.model = {label: _this.search_params.street_name, id: ""};
                    }
                    else if (model_status_street && model_status_street.status === 'multiple') 
                    {
                        _this.Street.model = {label: _this.search_params.street_name};
                        //Postavljamo filter za pretragu stavke
                        _this.Street.model_filter = { name: _this.search_params.street_name };
                    }
                    else if(model_status_street && model_status_street.model_data  && model_status_street.status === 'select') 
                    {
                        //Selektujemo pronadjenu stavku
                        _this.Street.model = {id: model_status_street.model_data.id, label: model_status_street.model_data.name};
                    }                  

                    //Postanski borj
                    var model_status_postal_code = response.models_status.PostalCode;
                    if (model_status_postal_code && model_status_postal_code.status === 'new') 
                    {
                        //Dodavanje nove stavke, definisanje ponasanje polja kao text field
                        _this.PostalCode.model = {label: _this.search_params.postal_code, id: ""};
                    }
                    else if (model_status_postal_code && model_status_postal_code.status === 'multiple') 
                    {
                        //Postavljamo filter za pretragu stavke
                        _this.PostalCode.model_filter = { name: model_status_postal_code.search_text };
                    }
                    else if(model_status_postal_code && model_status_postal_code.model_data  && model_status_postal_code.status === 'select') 
                    {
                        //Selektujemo pronadjenu stavku
                        _this.PostalCode.model = {id: model_status_postal_code.model_data.id, label: model_status_postal_code.model_data.code};
                        Object.assign(_this.City.model_filter, {city_to_postal_codes:{postal_code:{ids:model_status_postal_code.model_data.id} }});
                    }

                    //Grad
                    var model_status_city = response.models_status.City;
                    if (model_status_city && model_status_city.status === 'multiple') {
                        Object.assign(_this.City.model_filter, { name: model_status_city.search_text });
                    }
                    else if(model_status_city && model_status_city.status === 'new')
                    {  
                        _this.City.model = {label: _this.search_params.city_name, id: ""};
                    }
                    else if(model_status_city && model_status_city.status === 'select')
                    {
                        _this.City.model = {id: model_status_city.model_data.id, label: model_status_city.model_data.name};
                        if(model_status_city.model_data.postal_code)
                        {
                            _this.PostalCode.model = {
                                id: model_status_city.model_data.postal_code.id, 
                                label: model_status_city.model_data.postal_code.code
                            };
                        }
                        Object.assign(_this.PostalCode.model_filter, {postal_code_to_cities: {city: {ids:model_status_city.model_data.id} }} );
                        _this.PostalCode.settings.gui_table.fixed_filter = {postal_code_to_cities: {city: {ids:model_status_city.model_data.id} }};
                        //Ako je selektovan grad i ako imamo vise ulica sa istim nazivom postavljamo model_filter ulice za naziv grada
                        _this.Street.model_filter = { city:{ids: model_status_city.model_data.id} };
                        if(model_status_street && model_status_street.status === 'multiple')
                        {
                            _this.Street.model_filter = { name: model_status_street.search_text, city:{ids: model_status_city.model_data.id} };
                        }
                    }

                    //Broj ulice
                    _this.street_number = _this.search_params.street_number;
                    _this.floor = _this.search_params.floor;
                    _this.door_number = _this.search_params.door_number;

                    //Opstina
                    var model_status_municipality = response.models_status.Municipality;
                    if (model_status_municipality && model_status_municipality.status === 'multiple') {
                        _this.Municipality.model_filter = { name: model_status_municipality.search_text };
                    }
                    else if(model_status_municipality && model_status_municipality.model_data)//new
                    {
                        _this.Municipality.model = {id: model_status_municipality.model_data.id, label: model_status_municipality.model_data.name};
                        Object.assign(_this.City.model_filter, {municipality:{ids:model_status_municipality.model_data.id}});//appendujemo filter
                    }

                    //Drzava
                    var model_status_country = response.models_status.Country;
                    if (model_status_country && model_status_country.status === 'multiple') {
                        _this.Country.model_filter = { name: model_status_country.search_text };
                    }
                    else if(model_status_country && model_status_country.model_data)
                    {
                        _this.Country.model = {id: model_status_country.model_data.id, label: model_status_country.model_data.name};
                        Object.assign(_this.PostalCode.model_filter, {country:{ids:model_status_country.model_data.id}});//appendujemo filter
                        Object.assign(_this.City.model_filter, {country:{ids:model_status_country.model_data.id}});
                        Object.assign(_this.Municipality.model_filter, {country:{ids:model_status_country.model_data.id}});
                    }
                }
            });
            this.show_form = true;
            this.focusStreetNumber();
        },
        focusStreetNumber(){
            var _this = this;
            setTimeout(function(){
                if(_this.$refs.streetNumber)
                {
                    _this.$refs.streetNumber.focus();
                }
            },500);
        },
        saveNewItem(){
            var _this = this;
            var address_data = {};
            
            //Ulica
            var street = this.Street.model;
            address_data.street_number = this.street_number;
            address_data.floor = this.floor;
            address_data.door_number = this.door_number;
            address_data.description = this.description;
            if (street.id)
            {
                address_data.street_id = street.id;
            }
            else if(street.label)
            {
                address_data.street = {
                    name: street.label
                };
            }
            
            //Grad
            var city = this.City.model;
            if (city.id)
            {
                address_data.city_id = city.id;
            }
            else if(city.label)
            {
                address_data.city = {
                    name: city.label,
                    postal_code: {},
                    country_id: "",
                    default_postal_code_id: "",
                    municipality_id: this.Municipality.model.id ? this.Municipality.model.id : ""
                };
            }
                       
            //Opstina
            var municipality = this.Municipality.model;
            if (municipality.id && address_data.city)//ako je izabrana opstina i uneto novo naselje
            {
                address_data.city.municipality_id = municipality.id;
            }
            
            //Drzava
            var country = this.Country.model;
            if (country.id && address_data.city)//ako je izabrana drzava i uneto novo naselje
            {
                address_data.city.country_id = country.id;
                address_data.city.postal_code.country_id = country.id;
            }
            
            //Postanski broj
            var postal_code = this.PostalCode.model;
            if (postal_code.id)
            {
                address_data.postal_code_id = postal_code.id;
            }
            else if(!postal_code.id && postal_code.label && country.id) //Ako je unet novi postanski on se kasnije u kontroleru vezuje za ulicu
            {
                address_data.postal_code = {
                    code: postal_code.label,
                    country_id: country.id
                };
            }
            sima.ajax.get('geo/addressField/simaAddressFieldSaveItem', {
                data: {
                    address_data: address_data
                },
                async: true,
                success_function: function (response) {
                    if(response.address_result)
                    {
                        _this.show_form = false;
                        _this.$emit('savedAddressForm', response.address_result);
                    }
                }
            });
        },
        resetParams(){
            this.search_params = {};
            this.street_number = "";
            this.floor = "";
            this.door_number = "";
            this.description = "";
            
            this.Street.model = {};
            this.Street.model_filter = {};
            this.Street.selected_id = "";
            
            this.City.model = {};
            this.City.model_filter = {};
            this.City.selected_id = "";
                      
            this.PostalCode.model = {};
            this.PostalCode.model_filter = {};
            this.PostalCode.settings.gui_table.fixed_filter = {};
            this.PostalCode.selected_id = "";
            
            this.Municipality.model = {};
            this.Municipality.model_filter = {};
            this.Municipality.selected_id = "";
            
            this.Country.model = {};
            this.Country.model_filter = {};
            this.Country.selected_id = "";
        },
        streetChange(e){
            if(e.id)
            {
                if(this.Street.selected_id !== e.id)
                {
                    this.City.selected_id = "";
                    this.PostalCode.selected_id = "";
                    this.Municipality.selected_id = "";
                    this.City.model = {};
                    this.PostalCode.model = {};
                    this.Municipality.model = {};
                    this.Street.selected_id = e.id;
                }
            }
            else
            {
                this.Street.selected_id = "";
            }
        },
        cityChange(e){
            if(e.id)
            {
                if(this.City.selected_id !== e.id)
                {
                    this.City.selected_id = e.id;
                    this.PostalCode.model = {};
                    this.Municipality.model = {};
                    this.Street.model_filter = {city:{ids:e.id}};
                    Object.assign(this.Street.model_filter, { city:{ids: this.city.id} });
                    this.PostalCode.selected_id = "";//ako je korisnik izmenio 
                    this.Street.selected_id = "";
                    Object.assign(this.PostalCode.settings.gui_table.fixed_filter, {postal_code_to_cities:{ids:e.id}});
                }
            }
            else
            {
                this.City.selected_id = "";
                this.Street.model_filter = {};
                Object.assign(this.PostalCode.model_filter, {postal_code_to_cities:{}});
                this.PostalCode.settings.gui_table.fixed_filter = {};
            }
        },
        postalCodeChange(e){
            if(e.id)
            {
                if(this.PostalCode.selected_id !== e.id)
                {
                    this.PostalCode.selected_id = e.id;
                    Object.assign(this.City.model_filter, {city_to_postal_codes:{postal_code:{ ids:e.id} }});
                }
            }
            else
            {
                this.PostalCode.selected_id = "";
                Object.assign(this.City.model_filter, {city_to_postal_codes:{}});
            }
        },
        municipalityChange(e){
            if(e.id)
            {
                if(this.Municipality.selected_id !== e.id)
                {
                    this.City.model = {};
                    this.PostalCode.model = {};
                    this.Street.selected_id = "";
                    this.City.selected_id = "";
                    this.Municipality.selected_id = e.id;
                    Object.assign(this.City.model_filter, {municipality:{ids:e.id}});
                }
            }
            else
            {
                Object.assign(this.City.model_filter, {municipality:{}});
            }
        },
        countryChange(e){
            if(e.id)
            {
                if(this.Country.selected_id !== e.id)
                {
                    this.Street.selected_id = "";
                    this.City.model = {};
                    this.City.selected_id = "";
                    this.PostalCode.model = {};
                    this.PostalCode.selected_id = "";
                    this.Municipality.model = {};
                    this.Municipality.selected_id = "";
                    this.Country.selected_id = e.id;

                    this.Street.model_filter = {};
                    Object.assign(this.City.model_filter, {country:{ids:e.id}});
                    this.Municipality.model_filter = {country:{ids:e.id}};
                    Object.assign(this.PostalCode.model_filter, {country:{ids:e.id}});
                }
            }
            else
            {
                this.Country.selected_id = "";
                Object.assign(this.City.model_filter, {country:{}});
                Object.assign(this.PostalCode.model_filter, {country:{}});
                this.Municipality.model_filter = {country:{}};
            }
        },
        onClickOutside: function (e) {
            var _this = this;
            if (!_this.$el.contains(e.target) && _this.$el !== e.target) {
                _this.$el.remove();
            }
        },
        //objedinitu u event formAction - open,saved,closed
        openForm(data){
            this.saf_bus.$emit('bus_event_opened_ssf_form', data);
        },
        savedForm(data){
            
        },
        closedForm(data){
            this.saf_bus.$emit('bus_event_closed_ssf_form', data);
        },
        extractDoorNumber(string){
            var result = {
                street_name: string,
                street_number: "",
                floor: "",
                door_number: ""
            };
            
            var n = string.split(" ");
            if (n.length > 1)
            {
                var last_element = n[n.length - 1];
                
                if(last_element.match(/^[\/0-9]*$/)) //ako je samo broj ili kosa crta
                {
                    var last_element_parts = last_element.split('/');
                    if (typeof last_element_parts[0] !== 'undefined')
                    {
                        result.street_number = last_element_parts[0];
                    }
                    if (typeof last_element_parts[1] !== 'undefined')
                    {
                        result.floor = last_element_parts[1];
                    }
                    if (typeof last_element_parts[2] !== 'undefined')
                    {
                        result.door_number = last_element_parts[2];
                    }
                    
                    n.pop();
                    result.street_name = n.join(" ");
                }
            }

            return result;
        },
        extractPostalCode(string){
            var result = {
                city_name: string,
                postal_code: ""
            };
            var n = string.split(" ");
            if(n && n.length > 0)
            {
                var postal_code = n[0];
                if(!isNaN(postal_code))
                {
                    n.shift();//izbacujemo prvi element
                    result.city_name = n.join(" ");
                    result.postal_code = postal_code;
                }
            }
            return result;
        },
        generateAddNewItemString(){           
            this.fillSearchParams();
            
            if(this.search_params.street_name)
            {            
                var display_string = sima.translate('GeoAddStreet')+" ("+this.search_params.street_name;
                
                display_string += ' ' + this.search_params.street_number;
                if (!sima.isEmpty(this.search_params.floor))
                {
                    display_string += '/' + this.search_params.floor;
                }
                if (!sima.isEmpty(this.search_params.door_number))
                {
                    display_string += '/' + this.search_params.door_number;
                }

                display_string += ')';
                
                if(this.search_params.city_name)
                {
                    display_string += ", "+sima.translate('GeoCity')+" ("+this.search_params.city_name+") ";
                }
                if(this.search_params.municiplaity_name)
                {
                    display_string += ", "+sima.translate('GeoMunicipality')+" ("+this.search_params.municiplaity_name+")";
                }
                return display_string;
            }
            else
            {
                return this.search;
            }
        },
        fillSearchParams(){
            if(this.search.trim() !== "")
            {
                var search_params_array = this.search.split(",").map(item => item.trim());
                var extracted_street = this.extractDoorNumber(search_params_array[0]);
                var city_param_string = search_params_array[1] ? search_params_array[1] : "";
                var extracted_city = this.extractPostalCode(city_param_string);
                this.search_params.street_name = extracted_street.street_name;
                this.search_params.street_number = extracted_street.street_number;
                this.search_params.floor = extracted_street.floor;
                this.search_params.door_number = extracted_street.door_number;
                this.search_params.city_name = extracted_city.city_name;
                this.search_params.postal_code = extracted_city.postal_code;
                this.search_params.municiplaity_name = search_params_array[2] ? search_params_array[2] : "";
                this.search_params.municiplaity_id = this.Municipality.model.id;
                this.search_params.country_name = search_params_array[3] ? search_params_array[3] : "";
                this.search_params.country_id = this.Country.model.id;
            }
            else
            {
                this.search_params = {};
            }
        },
        isScrolledIntoView(el) { //Proverava da li je element vidljiv
            var rect = el.getBoundingClientRect();
            return (
                    (rect.top >= 0) && (rect.bottom <= el.parentElement.parentElement.getBoundingClientRect().bottom) && 
                    (rect.bottom >= 0) && (rect.top >= el.parentElement.parentElement.getBoundingClientRect().top)
            );
        }
    }
});