<?php

class CompanyGUI extends PartnerGUI {

    public function columnLabels() {
        return array(
            'name' => Yii::t('Company','Name'),
            'full_name' => Yii::t('Company','Full Name'),
            'PIB' => Yii::t('Company','PIB'),
            'inVat' => Yii::t('Company','inVat'),
            'street' => Yii::t('Company','Street'),
            'number' => Yii::t('Company','Number'),
            'city' => Yii::t('Company','City'),
            'image_id' => Yii::t('Company','Image'),
            'image' => Yii::t('Company','Image'),
            'file_version_id' => Yii::t('Company','Image'),
            'work_code_id' => Yii::t('Company','WorkCode'),
            'MB' => Yii::t('Company','MB'),
            'bank_accounts' => Yii::t('Company', 'BankAccounts'),
            'addresses' => Yii::t('Company', 'Addresses'),
            'contacts' => Yii::t('Company', 'Contacts'),
            'phone_numbers' => Yii::t('BaseModule.PhoneNumber', 'PhoneNumbers'),
            'email_addresses' => Yii::t('Partner', 'EmailAddresses'),
            'other_contacts' => Yii::t('Partner', 'OtherContacts')
        ) + parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return Yii::t('Company', $plural?'Companies':'Company');
    }
    
    public function columnDisplays($column) 
    {
        $owner = $this->owner;

        switch ($column) 
        {
            case 'image_id':
//                    return (!isset($owner->image) || $owner->image->isDeleted() || !isset($owner->image->last_version_id) || $owner->image->last_version->file_size == 0) ?
//                            SIMAMisc::getCompanyImgBase64() :
//                            $owner->image->last_version->getDownloadLink();
                $result = SIMAMisc::getCompanyImgBase64();
                if(isset($owner->image) && !$owner->image->isDeleted() && isset($owner->image->last_version_id) && $owner->image->last_version->canDownload())
                {
                    try
                    {
                        $result = $owner->image->last_version->getAbsoluteDownloadLink();
                    }
                    catch(Exception $ex)
                    {
                        Yii::app()->errorReport->createAutoClientBafRequest($ex->getMessage());
                    }
                }
                return $result;
            case 'fill_from_nbs_in_form': 
                $button_id = SIMAHtml::uniqid().'_fillfromnbs';
                if(SIMAMisc::isVueComponentEnabled())
                {
                    $return = Yii::app()->controller->widget(SIMAButtonVue::class, [
                        'id' => $button_id,
                        'half' => true,
                        'additional_classes' => ['fill-from-nbs-in-form'],
                        'title'=>Yii::t('Company', 'NBS'),
                        'tooltip'=>Yii::t('Company', 'FillFromNBS'),
                        'onclick'=>["sima.misc.fillFromNbsInForm", "$button_id", $owner->id]
                    ], true);
                }
                else
                {
                    $return = Yii::app()->controller->widget('SIMAButton', [
                        'id' => $button_id,
                        'class' => '_half fill-from-nbs-in-form',
                        'action'=>[                
                            'title'=>Yii::t('Company', 'NBS'),
                            'tooltip'=>Yii::t('Company', 'FillFromNBS'),
                            'onclick'=>["sima.misc.fillFromNbsInForm", "$button_id", $owner->id]
                        ]
                    ], true);
                }
                
                return $return;
            default: return parent::columnDisplays($column);
        }
    }

    public function modelViews() {
        $owner = $this->owner;
        return array_merge(parent::modelViews(), array(
            'default' => array(
//                'style'=>'border: 1px solid;'
            ),
            'basic_info' => array('class'=>'basic_info'),
            'basicInfoTitle' => array('class'=>'top_info'),
            'addresses',
            'withOutPosition',
//            'tabTemplates'=>array('with'=>array('templated_file'),'style'=>'border: 1px solid;')  //.creator
            'addressbook_basic_info',
            'expanded_basic_info_contact',
            'vertical_basic_info_company',
            'expanded_basic_info',
            'inPartner',
            'full_info',
            'left_top_info',
            'left_bottom_cooperation',
            'list_of_employees',
            'left_split',
            'themes'
            
        ));
    }

    public function modelForms()
    {
        $owner = $this->owner;
        $system_company_id = Yii::app()->configManager->get('base.system_company_id');
        return [
            'default' => [
                'type' => 'generic',
                'columns' => [
                    'name' => 'textField',
                    'address_id' => ['simaAddressField'],//za brisanje
                    'full_name' => 'textArea',
                    'file_version_id' => [
                        'fileField', 
                        'relName' => 'image', 
                        'allowed_extansions' => [
                            'png', 'jpg', 'gif', 'bmp', 'jpeg'
                        ], 
                        "additional_options" => [
                            "use_crop" => true, 
                            'image_dimensions_equal' => false
                        ]
                    ],
                    'work_code_id' => ['relation', 'relName' => 'work_code', 'add_button' => true],
                    'PIB' => ['textField', 'htmlAfter' => $owner->getAttributeDisplay('fill_from_nbs_in_form')],
                    'MB' => ['textField', 'htmlAfter' => $owner->getAttributeDisplay('fill_from_nbs_in_form')],
                    'inVat' => 'checkBox',
                    'comment' => 'textArea',
                    'phone_numbers' => ['list',
                        'model' => 'PhoneNumber',
                        'relName' => 'phone_numbers', 
                        'add_button_params' => [ 
                            'formName' => 'from_partner'
                        ],
                        'default_item' => [
                            'model' => 'Partner',
                            'column' => 'main_phone_number_id'
                        ] 
                    ],
                    'email_addresses' => ['list',
                        'model' => 'EmailAddress',
                        'relName' => 'email_addresses',  
                        'add_button_params' => [ 
                            'formName' => 'from_partner',
                            'init_data' => [ 
                                'EmailAddress' => [
                                    'name' => $owner->DisplayName,
                                    'owner_id' => $owner->id
                                ]
                            ]
                        ],
                        'default_item' => [
                            'model' => 'Partner',
                            'column' => 'main_email_address_id'
                        ]
                    ],
                    'addresses' => ['list',
                        'model' => 'Address',
                        'relName' => 'addresses',
                        'multiselect' => true,
                        'default_item' => [
                            'model'=>'Partner',
                            'column'=>'main_address_id'
                        ]
                    ],
                    'other_contacts' => ['list',
                        'model' => 'Contact',
                        'relName' => 'contacts',
                        'add_button_params' => [
                            'init_data' => [
                                'Contact' => [
                                    'partner_id' => $owner->id
                                ]
                            ]
                        ],
                        'default_item' => [
                            'model'=>'Partner',
                            'column'=>'main_contact_id'
                        ]
                    ],
                    'bank_accounts' => ['list',
                        'model' => 'BankAccount',
                        'relName' => 'bank_accounts',
                        'disabled' => ($owner->id === intval($system_company_id)),
                        'add_button_params' => [
                            'init_data' => [
                                'BankAccount' => [
                                    'partner_id' => $owner->id
                                ]
                            ]
                        ],
                        'default_item' => [
                            'model' => 'Partner',
                            'column' => 'main_bank_account_id'
                        ]
                    ],
                    'partner_lists' => ['list',
                        'model' => 'PartnerList',
                        'relName' => 'partner_lists',
                        'filter' => [
                            'filter_scopes' => ['withUserAccessToList', 'withoutSystems']
                        ],
                        'can_remove_item_func' => function($item_id)
                        {
                            $partner_list = PartnerList::model()->findByPkWithCheck($item_id);

                            return !$partner_list->is_system;
                        }
                    ]
                ]
            ]
        ];
    }
    
    public function getDisplayModel()
    {
        return $this->owner->partner;
    }
    
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default:
                return [
                    'columns' => ['name', 'PIB', 'comment']
                ];
        }
    }
    
    public function vueProp(string $prop, array $scopes = []):array
    {
        $owner = $this->owner;
        switch ($prop)
        {
            case 'display_icon': 
                $icon_params = [
                    'type' => SIMAActiveRecordGUI::$MODEL_DISPLAY_ICON_TYPE_ICON,
                    'code' => Company::class
                ];
                
                $img_src = $owner->getImageDownloadUrl();
                if (!empty($img_src))
                {
                    $icon_params = [
                        'type' => SIMAActiveRecordGUI::$MODEL_DISPLAY_ICON_TYPE_IMG,
                        'src' => $img_src
                    ];
                }

                return [$icon_params, SIMAMisc::getModelTags($owner)];
            default: return parent::vueProp($prop, $scopes);          
        }

    }
}