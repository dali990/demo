<script type="text/x-template" id="sima-partner-field-result-list">
    <div class="sima-partner-field-result-list">
        <div ref="companyTitle"> Firme </div>
        <div v-show="show_add_new_item && display_search.company !== '' " 
            class="spf-add-new-item" 
            v-on:click="openPartnerForm('Company')"
            ref="add_new_item_company">      
            {{display_search.company}}<i class="fa fa-plus" style="float:right"></i>
        </div>
        <div v-bind:class="{'slideup': !show_company_form, 'slidedown': show_company_form}" style="width: 100%" >
            <div class="spf-form">
                <table>
                    <tr>
                        <td style="text-align: right;">
                            <?=Yii::t('Company','Name')?> <span class="required">*</span>
                        </td>
                        <td>
                            <input class="text-field" v-model="Company.name" ref="companyName"/>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right;">
                            <?=Yii::t('Company','PIB')?>
                        </td>
                        <td>
                            <input class="text-field" v-model="Company.PIB" v-bind:maxlength="field_settings.company_pib_length"/>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right;">
                            <?=Yii::t('Company','MB')?>
                        </td>
                        <td>
                            <input class="text-field" v-model="Company.MB" v-bind:maxlength="field_settings.company_mb_length"/>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right;">
                            <?=Yii::t('Person', 'Comment')?>
                        </td>
                        <td>
                            <div class="sima-partner-field">
                                <textarea class="spf-search-field" v-model="Company.comment"></textarea>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align: right; padding-right: 23px;">
                        <sima-button
                            style="float:right"
                            v-on:click="saveNewCompany"
                            v-bind:class="{_disabled: !can_save_company}"
                            title="Dodaj"
                        >
                            <span class="sima-btn-title"><?=Yii::t('GeoModule.Address', 'Add')?></span>
                        </sima-button>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <ul class="result-list" ref="results_companies"
            v-bind:class="{'slideup': show_company_form, 'slidedown': !show_company_form}"
            v-show="results.companies.length > 0 && !show_company_form" >
            <li v-bind:class="'result-item model_'+result.id+' item_index_'+(i+1)" v-for="(result, i) in results.companies"
                v-on:click="selectCompanyItem(i, result.id)">
                <div style="display:inline" v-html="hits(result.DisplayName, i)"></div>
            </li>
        </ul>
        <hr>
        <div ref="personTitle"> Osobe </div>
        <div v-show="show_add_new_item && display_search.person !== '' " 
            class="spf-add-new-item" 
            v-on:click="openPartnerForm('Person')"
            ref="add_new_item_person">
            {{display_search.person}}<i class="fa fa-plus" style="float:right"></i>
        </div>
        <div v-bind:class="{'slideup': !show_person_form, 'slidedown': show_person_form}" style="width: 100%" >
            <div class="spf-form " >
                <table>
                    <tr>
                        <td style="text-align: right;">
                            <?=Yii::t('Person','FirstName')?> <span class="required">*</span>
                        </td>
                        <td>
                            <input class="text-field" v-model="Person.firstname" ref="personFirstName"/>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right;">
                            <?=Yii::t('Person','LastName')?> <span class="required">*</span>
                        </td>
                        <td>
                            <input class="text-field" v-model="Person.lastname"/>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right;">
                            <?=Yii::t('Person','JMBG')?>
                        </td>
                        <td>
                            <input class="text-field" v-model="Person.JMBG" v-bind:maxlength="field_settings.person_jmbg_length"/>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right;">
                            <?=Yii::t('Person', 'Comment')?>
                        </td>
                        <td>
                            <div class="sima-partner-field">
                                <textarea class="spf-search-field" v-model="Person.comment"></textarea>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align: right; padding-right: 23px;">
                        <sima-button
                            style="float:right"
                            v-on:click="saveNewPerson"
                            v-bind:class="{_disabled: !can_save_person}"
                            title="Dodaj"
                        >
                            <span class="sima-btn-title"><?=Yii::t('GeoModule.Address', 'Add')?></span>
                        </sima-button>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <ul class="result-list" 
            v-bind:class="{'slideup': show_person_form, 'slidedown': !show_person_form}"
            v-show="results.persons.length > 0 && !show_company_form" 
            ref="results_persons">
            <li v-bind:class="'result-item model_'+result.id+' item_index_'+(i+1)" v-for="(result, i) in results.persons"
                v-on:click="selectPersonItem(i, result.id)">
                <div style="display:inline" v-html="hits(result.DisplayName, i)"></div>
            </li>
        </ul>
    </div>
</script>