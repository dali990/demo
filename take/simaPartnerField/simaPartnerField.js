
/* global Vue, sima */
Vue.component('sima-partner-field', {
    template: '#sima-partner-field',
    name: 'sima-partner-field',
    inheritAttrs: false,
    props: {
        value: {default: () => ({ lable: '', id: '' }) },
        company_model_filter: { type: Object, default: () => ({}) },
        person_model_filter: { type: Object, default: () => ({}) },
        search_min_char: { type: Number, default: 0 },
        show_add_new_item: { type: Boolean, default: true },
        settings: {type: Object, default() { return {company:{}, person:{}}; }}
    },
    data: function () {
        return {
            search: "",
            input_value: '',
            saf_result_list: null,
            open_result_list: false,
            results: {
                companies: [],
                persons: []
            },
            selected_item: null,
            result_list_html: null,
            initial_text_search_value: "",
            initial_id_value: "",
            text_search_prev_value: "",
            id_prev_value: "",
            saf_bus: new Vue(),
            selected_model_name: "", //selektovani model Company/Person
            saved_model: false, //ako je sacuvan model onda nece vratiti inicijalnu vrednost polja
            stop_async_render: false
        };
    },
    computed: {
        scope() {
            return {
                events: {
                    'keydown': this.onKeyDown,
                    'keyup': this.onKeyUp,
                    'blur': this.onBlur,
                    'focus': this.onFocus,
                    'click': this.onClick
                }
            };
        },
        update_label(){
            if(
                    (!this.value.hasOwnProperty('label') || this.value.label === "") && 
                    typeof this.value.id !== 'undefined' && this.value.id !== ""
                )
            {
                return true;
            }
            return false;
        },
        update_label_on_select(){
            if(this.selected_item && this.selected_item.label === "")
            {
                return true;
            }
            return false;
        },
        partner(){
            if(this.value.id && (this.update_label || this.update_label_on_select))
            {
                return sima.vue.shot.getters.model('Partner_'+this.value.id);
            }
            return null;
        },
        display_label(){
            if(this.partner)
            {
                if(this.selected_model_name === 'Company' && this.settings.company && this.settings.company.display_label)
                {
                    return this.partner.company[this.settings.company.display_label];
                }
                else if(this.selected_model_name === 'Person' && this.settings.person && this.settings.person.display_label)
                {
                    return this.partner.company[this.settings.person.display_label];
                }
                return this.partner.DisplayName;
            }
            return this.search;
        }
    },
    watch: {
        value(new_val, old_val){
            this.updateField(new_val);
            if(!this.initial_id_value && this.value.id)
            {
                this.initial_id_value = this.value.id;
            }
        },
        display_label(new_val, old_val){
            if(this.update_label || this.update_label_on_select)
            {
                this.updateField(new_val, this.value.id);
                if(this.selected_item && this.selected_item.label === "")
                {
                    this.selected_item.label = new_val;
                }
            }
        }
    },
    created() {
        window.addEventListener('mousedown', this.onMouseDown);
    },
    destroyed() {
        if(this.saf_result_list)
        {
            this.saf_result_list.remove();
        }
        window.removeEventListener('mousedown', this.onMouseDown);
    },
    mounted: function () {
        var _this = this;
        this.result_list_html = this.$refs.safResultListComponent.$el;
        var _result_list_html = this.result_list_html;
        this.saf_result_list = document.body.appendChild(this.result_list_html);

        $(this.$el).on('destroyed', function () {
            _this.$destroy();
            _result_list_html.remove();
        });
        this.init();
    },
    methods: {
        init(){
            if (this.value.id)
            {
                this.updateField(this.value.label, this.value.id);
                this.input_value = this.value.id;//inicijalno postavljanje id               
                this.initial_id_value = this.value.id;//inicijalno postavljanje id bilo kog modela
            }
            else if (this.value.label)
            {
                this.search = this.value.label;
            }
            this.text_search_prev_value = this.search;
            this.initial_text_search_value = this.search;
          
            this.saf_bus.$on('bus_event_close_result_list', (data) => {
                if(this.saf_result_list)
                {
                    this.open_result_list = false;
                }
                this.undoField();
            });
            this.saf_bus.$on('bus_event_blur_field', (field) => {
                this.$refs.searchInptuField.blur();
            });                        
            this.saf_bus.$on('bus_event_focus_field', (field) => {
                this.$refs.searchInptuField.focus();
            });
        },
        searchText() {
            var _this = this;
            
            var globalTimeout = this.search_timeout;
                if (globalTimeout !== null) {
                    clearTimeout(globalTimeout);
                    this.search_timeout = globalTimeout;
                }
                this.search_timeout = setTimeout(function () {
                    _this.results = {
                        companies: [],
                        persons: []
                    };
                    if (_this.search.length >= _this.search_min_char) {
                        var company_display_label = 'DisplayName';
                        var person_display_label = 'DisplayName';
                        if(_this.settings.company && _this.settings.company.display_label)
                        {
                            company_display_label = _this.settings.company.display_label;
                        }
                        if(_this.settings.person && _this.settings.person.display_label)
                        {
                            person_display_label = _this.settings.person.display_label;
                        }
                        sima.ajax.get('base/model/simaSearchField', {
                            get_params: {
                                model_name: 'Company',
                                display_label: company_display_label
                            },
                            data: {
                                search_text: _this.search.replace(/\,/g, ''),
                                model_filter: _this.company_model_filter,
                                limit: _this.settings.limit || 50
                            },
                            async: true,
                            success_function: function (response) {
                                _this.results.companies = response.result;
                            }
                        });
                        sima.ajax.get('base/model/simaSearchField', {
                            get_params: {
                                model_name: 'Person',
                                display_label: person_display_label
                            },
                            data: {
                                search_text: _this.search.replace(/\,/g, ''),
                                model_filter: _this.person_model_filter,
                                limit: _this.settings.limit || 50
                            },
                            async: true,
                            success_function: function (response) {
                                _this.results.persons = response.result;
                            }
                        });
                    }
                    _this.renderResults();
                }, 500);
        },
        renderResults() {
            if(!this.stop_async_render && this.isVisible())
            {
                var address_field_position = this.$el.getBoundingClientRect();
                this.saf_result_list.style.top = address_field_position.top + 18 + "px";
                this.saf_result_list.style.left = address_field_position.left + "px";
                this.open_result_list = true;
            }
        },
        onKeyDown(e) {
            this.text_search_prev_value = e.target.value;
        },
        onKeyUp(e) {            
            if(e.keyCode === 27)//esc
            {
                if(this.saf_result_list && this.open_result_list)
                {
                    this.open_result_list = false;
                    e.stopPropagation();
                    e.preventDefault();
                }
                e.target.blur();
            }
            else if(e.keyCode === 9)//tab
            {
                e.preventDefault();
            }
            else if(e.keyCode === 13)//enter
            {
                if(this.saf_result_list && this.open_result_list)
                {
                    this.saf_bus.$emit('bus_event_enter', {});
                    e.stopPropagation();
                    e.preventDefault();
                }
            }
            else if(this.validKeyCodes(e.keyCode, e.ctrlKey, e.shiftKey))
            {
                if(this.hasSearchTextChanged())
                {
                    this.searchText();
                }
                this.$emit('input', {id: "", label: e.target.value});
            }
            else if(this.validKeyCodes(e.keyCode, e.ctrlKey, e.shiftKey))
            {
                this.searchText();
                if(this.settings.text_field)
                {
                    this.$emit('input', {id: "", label: e.target.value});
                }
                this.$emit('update', {id: "", label: e.target.value});
                if(this.search === "")
                {
                    this.$emit('selected', {id: "", label: ""});
                    this.$emit('input', {id: "", label: ""});
                }
            }
            e.stopPropagation();
            e.preventDefault();
        },
        onMouseDown(e){
            if(!e.target.classList.contains("spf-search-field"))
            {
                this.onClickOutside(e);
            }
        },
        onBlur(e) {
            if(e.relatedTarget)
            {
                if(!e.relatedTarget.classList.contains('spf-search-field') && !e.relatedTarget.classList.contains('sima-partner-field-result-list'))
                {
                    this.stop_async_render = true; 
                    this.open_result_list = false;
                    this.undoField();
                }
            }
        },
        onFocus(e) {
            this.stop_async_render = false; 
            this.renderResults();
        },
        onClick(e){
            if(this.hasSearchTextChanged() || this.search === "")
            {
                this.searchText();
            }
        },
        onInput(e) {
            //this.$emit('input', {id: "", label: ""});
        },
        onClickOutside: function (e) {
            if(this.saf_result_list)
            {
                if (
                        !e.target.closest(".sima-partner-field") && 
                        !e.target.closest(".sima-partner-field-result-list") &&
                        !e.target.closest(".sima-search-field") && 
                        !e.target.closest(".sima-search-field-result-list")
                    ) 
                {
                    this.open_result_list = false;
                    this.undoField();
                }
            }
        },
        savedPartnerForm: function (model) {
            var label = "";
            if(model.DisplayName !== null)
            {
                label = model.DisplayName;
            }
            this.open_result_list = false;
            this.search = label;
            this.input_value = model.id;
            this.saved_model = {id: model.id, label: label};
            this.selected_item = this.saved_model;
            this.$emit('input', this.saved_model);
        },
        selectedCompanyItem(i, id) {
            this.selected_item = {id: id, label: this.results.companies[i].display_label};
            this.updateField(this.selected_item);
            
            this.selected_model_name = 'Company';
            this.open_result_list = false;
            
            this.text_search_prev_value = this.search;
            this.id_prev_value = this.input_value;
            
            this.$emit('input', this.selected_item);
            this.$emit('selected', this.selected_item);
        },
        selectedPersonItem(i, id) {
            this.selected_item = {id: id, label: this.results.persons[i].display_label};
            this.updateField(this.selected_item);
            
            this.selected_model_name = 'Person';
            this.open_result_list = false;
            
            this.text_search_prev_value = this.search;
            this.id_prev_value = this.input_value;
            
            this.$emit('input', this.selected_item);
            this.$emit('selected', this.selected_item);
        },
        updateField(label, id) {
            if(typeof label === "object")
            {
                this.updateFieldFromVModel(label);
            }
            else
            {
                this.search = label;
                this.input_value = id;
            }
        },
        updateFieldFromVModel(v_model){
            var label = "";
            var id = v_model.id;
            if(v_model.hasOwnProperty('label'))
            {
                label = v_model.label;
            }
            this.search = label;
            this.input_value = id;
        },
        hasSearchTextChanged(){
            return this.text_search_prev_value !== this.search || false;
        },
        undoField(){
            if(this.search === "")
            {
                this.input_value = "";
                this.selected_item = {};
            }
            else if(this.selected_item !== null && this.selected_item.id)
            {
                this.search = this.selected_item.label;
                this.input_value = this.selected_item.id;
            }
            else
            {
                this.revertField();
            }
        },
        revertField(){
            if(this.saved_model)
            {
                if(this.saved_model.label !== this.search)//izmenjen je tekst ali nije dodata adresa
                {
                    this.search = this.saved_model.label;
                    this.input_value = this.saved_model.id;
                    this.selected_item = {};
                }
            }
            else
            {
                if(this.initial_text_search_value !== this.search)//izmenjen je tekst ali nije dodata adresa
                {
                    this.search = this.initial_text_search_value;
                    this.input_value = this.initial_id_value;
                }
            }
        },
        validKeyCodes(keyCode, ctrl, shift){
            if(
                (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (ctrl === false && (keyCode >= 65 && keyCode <= 90)) || 
                shift || 
                keyCode === 46 || keyCode === 8 ||
                (ctrl === true && (keyCode === 86 || keyCode === 88 || keyCode === 90)) //paste,cut,undo
            )
            {
                return  true;
            }
            return false;
        },
        isVisible(){
            if(this.$refs.searchInptuField)
            {
                return !!( 
                    this.$refs.searchInptuField.offsetWidth || 
                    this.$refs.searchInptuField.offsetHeight || 
                    this.$refs.searchInptuField.getClientRects().length ||
                    this.$refs.searchInptuField.offsetParent
                );
            }
            return false;
        }
    }
});