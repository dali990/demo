<script type="text/x-template" id="sima-partner-field">
    <div class="sima-partner-field" v-click-outside="onClickOutside">
        <input
            class="spf-search-field"
            v-bind:placeholder="$attrs.placeholder"
            v-model="search"
            v-on="scope.events"
            ref="searchInptuField"
            v-bind:maxlength="256"
        />
        <input type="hidden" v-bind:name="$attrs.name" v-bind:value="input_value"/>
        
        <sima-partner-field-result-list ref="safResultListComponent"             
            v-show="open_result_list"
            v-bind:results="results"
            v-bind:selected_item="selected_item"
            v-bind:search="search"
            v-bind:show_add_new_item="show_add_new_item"
            v-bind:saf_bus="saf_bus"
            @savedPartnerForm="savedPartnerForm"
            @selectedCompanyItem="selectedCompanyItem"
            @selectedPersonItem="selectedPersonItem">
        </sima-partner-field-result-list>
        
    </div>
</script>