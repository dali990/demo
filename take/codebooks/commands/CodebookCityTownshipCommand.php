<?php
class CodebookCityTownshipCommand extends CConsoleCommand
{    
    private $csv_items = [];
    private $total_csv_items = 0;
    private $total_inserts = 0;
    private $msgs = [
        'missing_municipalities_title' => "\nNedostaju sledece opstine: \n",
        'missing_municipalities' => [],
        'missing_cities_title' => "\nNedostaju sledeca mesta: \n",
        'missing_cities' => [],
        'multiple_cities_title' => "\nPostoje vise mesta sa istim kodom: \n",
        'multiple_cities' => [],
    ];
    
    public function run($args)
    {
        echo get_class($this)." - start\n";
        $input_file_path = $args[0] ?? '';
        $input_command = $args[1] ?? '';
        
        $this->loadFile($input_file_path);
        
        // Overview
        if($input_command === '' || $input_command === 'help')
        {
            $this->help();
        }
        
        if($input_command === 'csv')
        {
            echo "CSV Fajl\n";
            $table_csv = new ConsoleTable();
            $table_csv->addHeader('BZR Opštine')->addHeader('Opština')->addHeader('BZR Mesta')->addHeader('Mesto'); 
            foreach ($this->csv_items as $item)
            {
                $table_csv->addRow([
                    $item['municipality_bzr_code'],
                    substr($item['municipality_name'],0, 80),
                    $item['city_bzr_code'],
                    substr($item['city_name'],0, 80)
                ]);
            }
            $table_csv->display();
            echo "Total: {$this->total_csv_items}\n";
        }
        
        // Opis podataka
        else if($input_command === 'verbose')
        {
            $show_details = $args[2] ?? false;
            echo "*File: ". $input_file_path."\nSearching.......   ";
            $updates = [];
            $i=0;
            
            foreach ($this->csv_items as $item)
            {
                $i++;
                $township = CodebookTreasuryOfficeUnit::model()->findByAttributes(
                    [
                        'bzr_code' => $item['municipality_bzr_code'],               
                    ], 
                    "type = '".CodebookTreasuryOfficeUnit::$TYPE_1."' OR type = '".CodebookTreasuryOfficeUnit::$TYPE_1G."'"
                );
                if(!empty($township))
                {
                    $codebook_city = CodebookCity::model()->findByAttributes(['bzr_code' => $item['city_bzr_code']]);

                    if (empty($codebook_city))
                    {
                        SIMAMisc::pushToArrayIfNotExist($this->msgs['missing_cities'], $item['city_name']);
                    }
                    else
                    {
                        $this->msgs['multiple_cities'][] = $item['city_name']."(bzr code: ".$item['city_bzr_code'].")";                 
                    }
                }
                else
                {
                    $this->msgs['missing_municipalities'][] = $item['municipality_name']; 
                }
                echo "\033[5D";
                echo str_pad(round(($i/$this->total_csv_items)*100,0), 3, ' ', STR_PAD_LEFT) . " %"; 
            }            
            
            echo "\n";
            
            $this->displayMsgs($show_details);
            
            echo "\n";
        }
        
        else if($input_command === 'save')
        {
            echo "*Cheking items for insert... \n";
            
            $this->linkCitiesToMunicipality();
            
            $this->displayMsgs();
            echo "Total inserts: ".$this->total_inserts."\n";
        }
        else if($input_command === 'duplicates')
        {
            echo "*Searching duplicates... \n";
            $i = 0;
            foreach ($this->csv_items as $item)
            {
                $i++;
                foreach ($this->csv_items as $item_b)
                {
                    if($item['city_bzr_code'] === $item_b['city_bzr_code'] && $item['city_name'] !== $item_b['city_name'])
                    {
                        $this->msgs['multiple_cities'][] = $item['city_name']." (bzr code: ".$item['city_bzr_code'].")";
                    }
                }
                echo "\033[5D";
                echo str_pad(round(($i/$this->total_csv_items)*100,0), 3, ' ', STR_PAD_LEFT) . " %";
            }
            echo "\n";
            $this->displayMsgs(true);
        }
    }
    
    private function linkCitiesToMunicipality() 
    {
        $country_id = CodebookCountry::model()->findByAttributes(['numeric_code' => 688])->id;
        foreach ($this->csv_items as $item)
        {
            $township = CodebookTreasuryOfficeUnit::model()->findByAttributes(
                [
                    'bzr_code' => $item['municipality_bzr_code'],               
                ], 
                "type = '".CodebookTreasuryOfficeUnit::$TYPE_1."' OR type = '".CodebookTreasuryOfficeUnit::$TYPE_1G."'"
            );
            if(!empty($township))
            {
                $codebook_city = CodebookCity::model()->findByAttributes(['bzr_code' => $item['city_bzr_code']]);

                if (empty($township->country_id))
                {
                    $township->country_id = $country_id;
                    $township->save();
                }

                $postal_code_id = null;
                if(!empty($item['postal_code']))
                {
                    $postal_code = CodebookPostalCode::model()->findByAttributes(['code' => $item['postal_code']]);
                    if(empty($postal_code))
                    {
                        echo "Novi postanski broj: {$item['postal_code']}\n";
                        $new_pc = new CodebookPostalCode();
                        $new_pc->country_id = $country_id;
                        $new_pc->code = $item['postal_code'];
                        $new_pc->save();
                        $postal_code_id = $new_pc->id;
                    }
                    else
                    {
                        $postal_code_id = $postal_code->id;    
                    }
                }
                
                if (empty($codebook_city))
                {
                    SIMAMisc::pushToArrayIfNotExist($this->msgs['missing_cities'], $item['city_name']);
                    $this->msgs['missing_cities_title'] = "\nDodata su sledeca nova mesta:\n";
                                       
                    $new_city = new CodebookCity();
                    $new_city->name = trim($item['city_name']);
                    $new_city->bzr_code = $item['city_bzr_code'];
                    $new_city->country_id = $country_id;
                    $new_city->default_postal_code_id = $postal_code_id;
                    $new_city->treasury_office_unit_id = $township->id;
                    
                    $new_city->save();
                    
                    if($postal_code_id)
                    {
                        $ctpc = CodebookCityToPostalCode::model()->findAllByAttributes([
                            'postal_code_id' => $postal_code_id,
                            'city_id' => $new_city->id
                        ]);
                        if(empty($ctpc))
                        {
                            $new_ctpc = new CodebookCityToPostalCode();
                            $new_ctpc->postal_code_id = $postal_code_id;
                            $new_ctpc->city_id = $new_city->id;
                            $new_ctpc->save();
                        }
                    }                  
                    echo "Dodato novo mesto: {$item['city_name']}(Mesto) - {$township->name}(Opstina)\n";
                    $this->total_inserts++;
                }
                else
                {
                    $this->msgs['multiple_cities'][] = $item['city_name']." (bzr code: ".$item['city_bzr_code'].")";
                    error_log('Duplikat tokom unosa novog mesta - ' . $item['city_name']."(bzr code: ".$item['city_bzr_code'].")");                    
                }
            }
            else
            {
                $this->msgs['missing_municipalities'][] = $item['municipality_name']; 
            }
        }
        if(!empty($this->msgs['multiple_cities']))
        {
            echo $this->msgs['multiple_cities_title'];
            $table_multiple_cities = new ConsoleTable();
            $table_multiple_cities->addHeader('Mesto'); 
            foreach ($this->msgs['multiple_cities'] as $city_name)
            {
                $table_multiple_cities->addRow([$city_name]);
            }
            $table_multiple_cities->display();
        }
    }
    
    // Ucitavanje csv fajla
    private function loadFile($input_file_path)
    {
        if(file_exists($input_file_path))
        {
            $i = 0; //Inkrementiramo iteraciju kada preskacemo prvi red
            $file_cnt = 0;
            $file = new SplFileObject($input_file_path);
            $file->setFlags(SplFileObject::READ_CSV | SplFileObject::SKIP_EMPTY
                | SplFileObject::DROP_NEW_LINE | SplFileObject::READ_AHEAD);
            $file_icnt = iterator_count($file);
            
            echo "Total files: $file_cnt\n";

            foreach ($file as $row) 
            {
                $this->csv_items[$file_cnt] = array_combine([
                    'region_code',
                    'region_name',
                    'municipality_bzr_code', 
                    'municipality_name', 
                    'city_name',
                    'city_bzr_code', 
                    'city_name2',
                    'postal_code'
                ], $row);
                $file_cnt++;
            }

            echo "Checking file...\n";
            foreach ($this->csv_items as $key => &$item) 
            {
                $i++;
                if(empty($item['city_name']))
                {
                    unset($this->csv_items[$key]);
                }
                $this->formatItems($item);
                echo "\033[5D";      // Move 5 characters backward
                echo str_pad(round(($i/$file_cnt)*100,0), 3, ' ', STR_PAD_LEFT) . " %";               
            }
            $this->total_csv_items = count($this->csv_items);
        }
        else
        {
            echo "\nERROR: File '$input_file_path' not found.\n";
        }
        echo "\n"; 
    }
    
    // Formatiranje
    private function formatItems(array &$item)
    {    
        $item['municipality_name'] = SIMAMisc::convertLatinToCyrillic($item['municipality_name']);
        $item['city_name'] = SIMAMisc::convertLatinToCyrillic($item['city_name']);
        $item['city_name_latin'] = SIMAMisc::convertCyrillicToLatin($item['city_name']);
    }
    
    public function displayMsgs($show_items=false) 
    {
        if($show_items)
        {
            if(!empty($this->msgs['missing_cities']))
            {
                echo $this->msgs['missing_cities_title'];
                $table_missing = new ConsoleTable();
                $table_missing->addHeader('Mesto'); 
                foreach ($this->msgs['missing_cities'] as $city_name)
                {
                    $table_missing->addRow([$city_name]);
                }
                $table_missing->display();
            }
            if(!empty($this->msgs['multiple_cities']))
            {
                echo $this->msgs['multiple_cities_title'];
                $table_multiple_cities = new ConsoleTable();
                $table_multiple_cities->addHeader('Mesto'); 
                foreach ($this->msgs['multiple_cities'] as $city_name)
                {
                    $table_multiple_cities->addRow([$city_name]);
                }
                $table_multiple_cities->display();
            }
            if(!empty($this->msgs['missing_municipalities']))
            {
                echo $this->msgs['missing_municipalities_title'];
                $table_missing_municipalities = new ConsoleTable();
                $table_missing_municipalities->addHeader('Opstina'); 
                foreach ($this->msgs['missing_municipalities'] as $municipality)
                {
                    $table_missing_municipalities->addRow([$municipality]);
                }
                $table_missing_municipalities->display();
            }
        }
        $table_overview = new ConsoleTable();
        $table_overview->addHeader('Br novih mesta')->addHeader('Br mesta sa istim kodom')->addHeader('Br stavki u csv fajlu');
        $table_overview->addRow([
            count($this->msgs['missing_cities']), 
            count($this->msgs['multiple_cities']),
            $this->total_csv_items,
        ]);
        $table_overview->display();
    }
    
    private function help()
    {
        $table_help = new ConsoleTable();
        $table_help->addHeader('Command')->addHeader('Description');
        $table_help->addRows([
            ["verbose", "Kratak pregled"],
            ["verbose details", "Opis podataka i akcije"],
            ["save", "Dodaje nova i povezuje mesta sa opstinama"],
            ["duplicates", "Prikaz mesta sa istim kodom u csv fajlu"],
            ["csv", "List csv file"],
            ["help", "List all commands"],
        ]);
        echo " * Available commands * \n";
        $table_help->display();
        echo "\n";
    }
}