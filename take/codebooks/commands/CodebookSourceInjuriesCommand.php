<?php
class CodebookSourceInjuriesCommand extends ConsoleCodebookDiffViewer
{
    public function modelName()
    {
        return CodebookSourceInjury::class;
    }
    
    // Logika za izvlacenje parent koda
    public function parseParentCode($code)
    {       
        if(!empty($code) && is_numeric($code))
        {
            if(fmod($code, 1) !== 0.00)
            {
                return str_pad(floor($code), 2, '0', STR_PAD_LEFT).".00";
            }
            return null;
        }
        return null;        
    }
}