<?php

namespace App\Console\Commands\Codebook;

use SplFileObject;
use Illuminate\Console\Command;
use App\Models\Geo\Municipality;
use App\Models\Geo\City;

class CityCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cityCommand:fill {path}';
//
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */

    private $storage = [];

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $path = $this->argument('path');
        $this->info('path: '.$path);

        $this->loadFile($path);
        $this->saveItems();

    }

    private function saveItems()
    {
        $bar = $this->output->createProgressBar(count($this->storage));
        foreach ($this->storage as $key => $item) {
            $municipality = Municipality::where('code', $item['municipality_code'])->get(); //dodati rule da je jedinstven
            $city = new City;
            $city->code = $item['city_code'];
            $city->name = $item['city_name'];
            $city->municipality_id = $municipality->id;
            $city->country_id = $item['country_id'];
            $city->save();
            $bar->advance();
        }
        $bar->finish();
        echo "\n";
    }

    private function loadFile($input_file_path)
    {
        if(file_exists($input_file_path))
        {
            $i = 0; //Inkrementiramo iteraciju kada preskacemo prvi red
            $file_cnt = 0;
            $file = new SplFileObject($input_file_path);
            $file->setFlags(SplFileObject::READ_CSV | SplFileObject::SKIP_EMPTY
                | SplFileObject::DROP_NEW_LINE | SplFileObject::READ_AHEAD);
            $file_cnt = iterator_count($file);
            $bar = $this->output->createProgressBar($file_cnt);

            foreach ($file as $row) 
            {
                $row[1] = $this->convertCyrillicToLatin($row[1]);
                $row[3] = $this->convertCyrillicToLatin($row[3]);
                $row[4] = 14;
                $this->storage[$row[0]] = array_combine([
                    'municipality_code', 
                    'municipality_name', 
                    'city_code', 
                    'city_name',
                    'country_id'
                ], $row);
                $bar->advance();
            }

            $bar->finish();
            echo "\n";
        }
        else
        {
            echo "\nERROR: File '$input_file_path' not found.\n";
        } 

        if(!empty($this->storage))
        {
            
            $headers = [
                    'municipality_code', 
                    'municipality_name', 
                    'city_code', 
                    'city_name',
                    'country_id'
                ];

            $this->table($headers, $this->storage);
        }
    }

    public function convertCyrillicToLatin($cyrillic_text)
    {
        $cyrillic_to_latin = [
            'а'=>'a','б'=>'b','в'=>'v','г'=>'g','д'=>'d','ђ'=>'đ','е'=>'e','ж'=>'ž','з'=>'z','и'=>'i','ј'=>'j','к'=>'k','л'=>'l','љ'=>'lj',
            'м'=>'m','н'=>'n','њ'=>'nj','о'=>'o','п'=>'p','р'=>'r','с'=>'s','т'=>'t','ћ'=>'ć','у'=>'u','ф'=>'f','х'=>'h','ц'=>'c','ч'=>'č',
            'џ'=>'dž','ш'=>'š',
            'А'=>'A','Б'=>'B','В'=>'V','Г'=>'G','Д'=>'D','Ђ'=>'Đ','Е'=>'E','Ж'=>'Ž','З'=>'Z','И'=>'I','Ј'=>'J','К'=>'K','Л'=>'L','Љ'=>'Lj',
            'М'=>'M','Н'=>'N','Њ'=>'Nj','О'=>'O','П'=>'P','Р'=>'R','С'=>'S','Т'=>'T','Ћ'=>'Ć','У'=>'U','Ф'=>'F','Х'=>'H','Ц'=>'C','Ч'=>'Č',
            'Џ'=>'Dž','Ш'=>'Š'
        ];
        $latin_text = '';
        $cyrillic_text_strlen = strlen($cyrillic_text);
        for ($i=0; $i<$cyrillic_text_strlen; $i++)
        {            
            $char = substr($cyrillic_text, $i, 2);
            //ako je stvarno cirilicno slovo onda ga zamenjujemo odgovarajucim latinicnim i povecavamo jos jednom brojac,
            //jer cirilicna slova zauzimaju dva karaktera
            if (isset($cyrillic_to_latin[$char]))
            {
                $latin_text .= $cyrillic_to_latin[$char];
                $i++;                
            }
            else
            {
                $latin_text .= $cyrillic_text[$i];
            }
        }
        
        return $latin_text;
    }
}
