<?php

abstract class ConsoleCodebookDiffViewer extends CConsoleCommand
{
    private $storage = [];
    private $insert_data = [];
    private $update_data = [];
    private $delete_data = [];
    private $total_inserts = 0;
    private $total_updates = 0;
    private $total_deletes = 0;
    
    abstract function modelName();
    abstract function parseParentCode($code);
    
    public function headerRow() 
    {
        return true;
    }
    
    public function convertCyrillic() 
    {
        return true;
    }
    
    public function relations() 
    {
        return [];
    }
    
    public function run($args)
    {
        echo get_class($this)." - start\n";
        $input_file_path = $args[0];
        $input_command = isset($args[1]) ? $args[1] : '';
        
        // Overview
        if($input_command === '')
        {
            $this->help();        
            $this->loadFile($input_file_path);
            echo "Total new items: ".$this->total_inserts."\n";
            echo "Total items to update: ".$this->total_updates."\n";
            echo "Total items to delete: ".$this->total_deletes."\n\n";
        }
        
        // Opis podataka
        else if($input_command === 'verbose')
        {
            $this->loadFile($input_file_path);
            echo "*Model name: ". $this->modelName()."\n";
            echo "*Table name: ". $this->modelName()::tableName()."\n\n";
            echo "For insert: ".$this->total_inserts."\n";
            
            // Prikaz tabele za dodavanje novih stavki
            $table_insert = new ConsoleTable();
            $table_insert->addHeader('Code')->addHeader('Name')->addHeader('Parent');            
            foreach ($this->insert_data as $item)
            {
                $parent_code = '';
                if(!empty($item['parent_code']))
                {
                    $parent_code = " (".$item['parent_code'].")";
                }
                $table_insert->addRow([
                    $item['code'],
                    substr($item['name'],0, 80),
                    substr($item['parent_name'], 0, 60).$parent_code
                ]);
            }
            $table_insert->display();
            
            // Prikaz tabele za izmenu postojecih stavki
            echo "\n";
            echo "For update: ".$this->total_updates."\n";
            $table_update = new ConsoleTable();
            $table_update->addHeader('Code')->addHeader('Name')->addHeader('Parent');
            foreach ($this->update_data as $item)
            {
                $item_name = $item['name'];
                if($item['old_name'] !== $item_name)
                {
                    $item_name = "Old: ".$item['old_name']." - New: ".$item_name;
                }
                $parent_code = '';
                if(!empty($item['parent_code']))
                {
                    $parent_code = " (".$item['parent_code'].")";
                }
                $parent = substr($item['parent_name'], 0, 60).$parent_code;
                if($item['old_parent_code'] !== $item['parent_code'])
                {
                    $parent = "Old: ".substr($item['old_parent_name'], 0, 60)." (".$item['old_parent_code'].") - New: ".substr($item['parent_name'], 0, 60);
                }
                $table_update->addRow([
                    $item['code'],
                    substr($item_name, 0, 130),
                    $parent
                ]);
            }           
            $table_update->display();
            
            // Prikaz tabele za brisanje stavki
            $this->checkItemsForDelete();
            echo "\n";
            echo "For delete: ".$this->total_deletes."\n";
            $table_delete = new ConsoleTable();
            $table_delete->addHeader('Code')->addHeader('Name')->addHeader('Parent');            
            foreach ($this->delete_data as $item)
            {
                $parent_code = '';
                if(!empty($item['parent_code']))
                {
                    $parent_code = " (".$item['parent_code'].")";
                }
                $table_delete->addRow([
                    $item['code'],
                    substr($item['name'],0, 80),
                    substr($item['parent_name'], 0, 60).$parent_code
                ]);
            }
            $table_delete->display();
            
            echo "\n";
        }
        
        // Cuvanje novih i azuriranje postojecih stavki
        else if($input_command === 'save')
        {
            $this->loadFile($input_file_path);
            $this->saveData();
        }
        
        // Truncate table
        else if($input_command === 'truncate')
        {
            $table_name = $this->modelName()::model()->tableName();
            $truncate = $this->prompt("Truncate table ".$table_name."? (yes/no): ");
            if($truncate === 'yes' || $truncate === 'y')
            {
                Yii::app()->dbcodebooks->createCommand()->truncateTable($table_name);
                echo 'Table: '.$table_name." is empty\n\n";
            }
        }
        
        // Truncate + save
        else if($input_command === 'override')
        {
            $table_name = $this->modelName()::model()->tableName();
            $override = $this->prompt("Override all data in table ".$table_name."? (yes/no): ");
            if($override === 'yes' || $override === 'y')
            {
                Yii::app()->dbcodebooks->createCommand()->truncateTable($table_name);
                echo 'Table: '.$table_name." is empty\n\n";
                $this->loadFile($input_file_path);
                $this->saveData();
            }
        }
        
        // Prikaz fajla i pretraga zanimanja po kodu
        else if($input_command === 'csv')
        {
            $param = isset($args[2]) ? $args[2] : '';
            $this->loadFile($input_file_path);
            if($param === 'all')
            {
                $table = new ConsoleTable();
                $table->addHeader('Code')->addHeader('Name');            
                foreach ($this->storage as $item)
                {
                    $table->addRow([
                        $item['code'],
                        substr($item['name'],0, 120)
                    ]);
                }
                $table->display();
            }
            else if (isset($this->storage[$param]))
            {
                $table = new ConsoleTable();
                $table->addHeader('Code')->addHeader('Name');            
                $table->addRow([
                    $this->storage[$param]['code'],
                    substr($this->storage[$param]['name'],0, 120)
                ]);
                $table->display();
            }
            else
            {               
                echo "Item with code '$param' cannot be found\n";
            }
        }
        else if($input_command === 'help')
        {
            $this->help();
        }
        else
        {
            $input_command = trim($input_command);
            if(!empty($input_command))
            {
                echo "Wrong command '$input_command'. Type 'help' to list all commands.\n";
                $this->help();
            }
        }
        
        echo "\nTime: ".date("H:i:s d.m.Y")."\n";
        echo get_class($this)." - end\n";
    }
    
    // Ucitavanje csv fajla
    private function loadFile($input_file_path)
    {
        if(file_exists($input_file_path))
        {
            $i = $this->headerRow()? 1 : 0; //Inkrementiramo iteraciju kada preskacemo prvi red
            $file_cnt = 0;
            $file = new SplFileObject($input_file_path);
            $file->setFlags(SplFileObject::READ_CSV | SplFileObject::SKIP_EMPTY
                | SplFileObject::DROP_NEW_LINE | SplFileObject::READ_AHEAD);
//            $file_cnt = iterator_count($file);

            foreach ($file as $row) 
            {
                if($this->headerRow())
                {
                    if($file_cnt !== 0)//Preskacemo
                    {
                        $this->storage[$row[0]] = array_combine(['code', 'name'], $row);
                    }                   
                }
                else
                {
                    $this->storage[$row[0]] = array_combine(['code', 'name'], $row);
                }
                $file_cnt++;
            }

            echo "Checking file...\n";
            foreach ($this->storage as $item) 
            {
                $i++;
                $this->allocateItems($item);
                echo "\033[5D";      // Move 5 characters backward
                echo str_pad(round(($i/$file_cnt)*100,0), 3, ' ', STR_PAD_LEFT) . " %";               
            }
        }
        else
        {
            echo "\nERROR: File '$input_file_path' not found.\n";
        }
        echo "\n"; 
    }
    
    // Formatiranje podataka i njihovo smetanje u odgovarajucim nizovima
    private function allocateItems(array $item)
    {    
        $item['parent_code'] = '';
        $item['parent_name'] = '';
        $item['old_parent_code'] = '';
        $item['old_parent_name'] = '';
        
        $parsed_parent_code_from_csv = $this->parseParentCode($item['code']);
        $parent_from_csv = $this->getParentFromCSV($parsed_parent_code_from_csv);
        if(!empty($parent_from_csv))
        {
            $item['parent_code'] = $parent_from_csv['code'];
            $item['parent_name'] = $parent_from_csv['name'];
        }
        
        if($this->convertCyrillic())
        {
            $item['name'] = SIMAMisc::convertLatinToCyrillic($item['name']);
            $item['parent_name'] = SIMAMisc::convertLatinToCyrillic($item['parent_name']);
        }
        
        $local_model = $this->modelName()::model()->findByAttributes(['code' => $item['code']]);
        $local_item_parent_code = '';
        if(!empty($local_model->parent))
        {
            $local_item_parent_code = $local_model->parent->code;
            $item['old_parent_code'] = $local_model->parent->code;
            $item['old_parent_name'] = $local_model->parent->name;
        }
        
        if(empty($local_model))
        {
            $this->total_inserts++;
            $this->insert_data[$item['code']] = $item;
        }
        else if($local_model->name !== $item['name'] || $local_item_parent_code !== $item['parent_code'])
        {
            $this->total_updates++;
            $item['old_name'] = $local_model->name;
            $this->update_data[$item['code']] = $item;
        }       
    }
    
    private function checkItemsForDelete()
    {
        $unique_fields = array_keys($this->storage);
        $local_models = $this->modelName()::model()->findAll(new SIMADbCriteria([
            'Model' => $this->modelName(),
            'model_filter' => [
                'filter_scopes' => [
                    'withoutUniqueFields' => [$unique_fields]
                ]
            ]
        ]));
        if(!empty($local_models))
        {
            foreach ($local_models as $local_model)
            {
                $this->total_deletes++;
                $local_item_parent_code = '';
                $local_item_parent_name = '';
                if(!empty($local_model->parent))
                {
                    $local_item_parent_code = $local_model->parent->code;
                    $local_item_parent_name = $local_model->parent->name;
                }
                $this->delete_data[$local_model->code] = [
                    'code' => $local_model->code,
                    'name' => $local_model->name,
                    'parent_code' => $local_item_parent_code,
                    'parent_name' => $local_item_parent_name
                ];
            }
        }
    }
    
    private function saveData()
    {        
        echo "*Model name: ".$this->modelName()."\n";
        echo "*Table name: ".$this->modelName()::tableName()."\n";
        $model_name = $this->modelName();
        // Insert
        if($this->total_inserts > 0)
        {
            $i = 0;
            echo "\nInserting new items...\n";
            foreach ($this->insert_data as $item)
            {
                $i++;
                $new_model = new $model_name();
                $new_model->code = $item['code'];
                $new_model->name = $item['name'];
                $new_model->parent_id = $this->getOrSetCodebookItemParentId($item);
                $new_model->save();
                echo "\033[5D";      // Move 5 characters backward
                echo str_pad(round(($i/$this->total_inserts)*100,0), 3, ' ', STR_PAD_LEFT) . " %";               
            }
            echo "\n";
        }
        
        // Update
        if($this->total_updates > 0)
        {
            $i = 0;
            echo "\nUpdating items...\n";
            foreach ($this->update_data as $item) 
            {
                $i++;
                $local_parent_code = '';
                $local_model = $this->modelName()::model()->findByAttributes(['code' => $item['code']]);
                if(!empty($local_model->parent))
                {
                    $local_parent_code = $local_model->parent->code;
                }
                if($local_model->name !== $item['name'])
                {
                    $local_model->name = $item['name'];
                }
                if($local_parent_code !== $item['parent_code'])
                {
                    $local_model->parent_id = $this->getOrSetCodebookItemParentId($item);
                }
                $local_model->save();
                echo "\033[5D";      // Move 5 characters backward
                echo str_pad(round(($i/$this->total_updates)*100,0), 3, ' ', STR_PAD_LEFT) . " %";
            }
            echo "\n";
        }
        
        // Delete
        $this->checkItemsForDelete();
        if($this->total_deletes > 0)
        {
            $i = 0;
            echo "\nDeleting items...\n";
            foreach ($this->delete_data as $item) 
            {
                $i++;
                $local_item = $this->modelName()::model()->findByAttributes(['code' => $item['code']]);
                if(!empty($local_item))
                {
                    $local_children_items = $this->modelName()::model()->findAllByAttributes(['parent_id' => $local_item->id]);
                    if(!empty($local_children_items))
                    {
                        foreach ($local_children_items as $local_child_item)
                        {
                            $local_child_item->delete();
                        }
                    }
                    $local_item->delete();
                }
                echo "\033[5D";      // Move 5 characters backward
                echo str_pad(round(($i/$this->total_deletes)*100,0), 3, ' ', STR_PAD_LEFT) . " %";
            }
            echo "\n";
        }
        
        $status_table = new ConsoleTable();
        $status_table->addHeader('Inserts')->addHeader('Updates')->addHeader('Deletes');            
        $status_table->addRow([
            $this->total_inserts,
            $this->total_updates,
            $this->total_deletes
        ]);
        $status_table->display();
        echo "\n";
    }
    
//    // Logika za izvlacenje parent koda
//    private function parseParentCode($code)
//    {       
//        if(!empty($code) && is_numeric($code))
//        {
//            if($code % 10 !== 0)
//            {
//                return floor($code/10)*10;
//            }
//            return null;
//        }
//        return null;
//    }
    
    // Vraca parent id ukoliko postoji trazeni parent, ako ne postoji kreira novi
    private function getOrSetCodebookItemParentId($parent_attributes)
    {
        if((!empty($parent_attributes['parent_code']) || $parent_attributes['parent_code'] === '0') && !empty($parent_attributes['parent_name']))
        {
            $parent_model = $this->modelName()::model()->findByAttributes([
                'code' => $parent_attributes['parent_code']
            ]);
            if (empty($parent_model))
            {   $model_name = $this->modelName();
                $parent_model = new $model_name();
                $parent_model->name = $parent_attributes['parent_name'];
                $parent_model->code = $parent_attributes['parent_code'];
                $parent_model->save();
            }
            return $parent_model->id;                   
        }
        return null;
    }
    
    private function getParentFromCSV($parent_code=null)
    {
        if($parent_code || $parent_code === '0')
        {
            if(isset($this->storage[$parent_code]))
            {
                return $this->storage[$parent_code];
            }
        }
        return null;
    }
    
    private function help()
    {
        $table_help = new ConsoleTable();
        $table_help->addHeader('Command')->addHeader('Description');
        $table_help->addRows([
            ["verbose", "Describe data"],
            ["save", "Perform insert, update, delete actions on data"],
            ["truncate", "Truncate table"],
            ["csv all", "List csv file"],
            ["csv 'code'", "Find csv item by code"],
            ["help", "List all commands"],
        ]);
        echo " * Available commands * \n";
        $table_help->display();
        echo "\n";
    }
}

 

