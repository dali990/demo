<?php
class CodebookOccupationsCommand extends ConsoleCodebookDiffViewer
{    
    public function modelName()
    {
        return CodebookOccupation::class;
    }
    
    public function convertCyrillic() 
    {
        return false;
    }
    
    // Logika za izvlacenje parent koda
    public function parseParentCode($code)
    {       
        if(!empty($code))
        {
            if(strpos($code, "."))
            {
                return explode(".", $code)[0];
            }
            else if(strlen($code) > 1)
            {
                return substr($code, 0, -1);
            }
            return null;
        }
        return null;
    }   
}