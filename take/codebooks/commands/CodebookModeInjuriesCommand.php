<?php
class CodebookModeInjuriesCommand extends ConsoleCodebookDiffViewer
{    
    public function modelName()
    {
        return CodebookModeInjury::class;
    }
    
    // Logika za izvlacenje parent koda
    public function parseParentCode($code)
    {       
        if(!empty($code) && is_numeric($code))
        {
            if($code % 10 !== 0)
            {
                return floor($code/10)*10;
            }
            return null;
        }
        return null;
    }  
}