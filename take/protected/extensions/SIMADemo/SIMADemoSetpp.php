<?php

class SIMADemoSetpp extends SIMADemo
{
    public $demo_api_url = null;
    public $demo_api_token = null;
    public $db_component_data = null;
    public $demo_db_component_data = null;
    public $demo_sima_url = 'https://demo.is-sima.rs';
    
    private $_demo_setpp_controller = 'demoApiSetpp';
    
    public function IsDemoSetpp()
    {
        return true;
    }
    
    public function init()
    {
        if(empty($this->demo_api_url))
        {
            throw new Exception('demo_api_url not set');
        }
        if(empty($this->demo_api_token))
        {
            throw new Exception('demo_api_token not set');
        }
        if(empty($this->db_component_data))
        {
            throw new Exception('db_component_data not set');
        }
        if(!is_array($this->db_component_data))
        {
            throw new Exception('db_component_data not array');
        }
        if(!isset($this->db_component_data['connectionString']))
        {
            throw new Exception('db_component_data connectionString not set');
        }
        if(!isset($this->db_component_data['username']))
        {
            throw new Exception('db_component_data username not set');
        }
        if(!isset($this->db_component_data['password']))
        {
            throw new Exception('db_component_data password not set');
        }
        if(empty($this->demo_db_component_data))
        {
//            throw new Exception('$dem_db_component_data not set');
            error_log(__METHOD__.' - demo_db_component_data not set');
        }
        if(!is_array($this->demo_db_component_data))
        {
//            throw new Exception('demo_db_component_data not array');
            error_log(__METHOD__.' - demo_db_component_data not array');
        }
        if(!isset($this->demo_db_component_data['connectionString']))
        {
//            throw new Exception('demo_db_component_data connectionString not set');
            error_log(__METHOD__.' - demo_db_component_data connectionString not set');
        }
        if(!isset($this->demo_db_component_data['username']))
        {
//            throw new Exception('demo_db_component_data username not set');
            error_log(__METHOD__.' - demo_db_component_data username not set');
        }
        if(!isset($this->demo_db_component_data['password']))
        {
//            throw new Exception('demo_db_component_data password not set');
            error_log(__METHOD__.' - demo_db_component_data password not set');
        }
    }
    
    public function sendAuthenticationEmail(DemoSession $demo_session)
    {
//        $request_result = Yii::app()->curl->get($this->demo_api_url, [
//            'r' => $this->_demo_setpp_controller.'/sendAuthenticationEmail',
//            'did' => $demo_session->id
//        ]);
//        
//        error_log(__METHOD__.' - $request_result: '.CJSON::encode($request_result));
        $this->getHTTPResult('sendAuthenticationEmail', [
            'get_params' => [
                'did' => $demo_session->id
            ]
        ]);
    }
    
    public function sendAccessEmail(DemoSession $demo_session)
    {
//        $request_result = Yii::app()->curl->get($this->demo_api_url, [
//            'r' => $this->_demo_setpp_controller.'/sendAuthenticationEmail',
//            'did' => $demo_session->id
//        ]);
//        
//        error_log(__METHOD__.' - $request_result: '.CJSON::encode($request_result));
        $this->getHTTPResult('sendAccessEmail', [
            'get_params' => [
                'did' => $demo_session->id
            ]
        ]);
    }
    
    private function getHTTPResult($action, array $params=[])
    {
        $get_params = [];
        if (isset($params['get_params'])) {
            $get_params = $params['get_params'];
        }
        $post_params = [];
        if (isset($params['post_params'])) {
            $post_params = $params['post_params'];
        }
        $curl_options = [];
        if (isset($params['curl_options'])) {
            $curl_options = $params['curl_options'];
        }
        
//        $get_params['r'] = $action;
        
//        $url = $this->demo_api_url.'';
        $url = $this->demo_api_url.'?r='.$this->_demo_setpp_controller.'/'.$action.'&token='.$this->demo_api_token;
//        $url = $this->demo_api_url.'?r='.$this->_demo_setpp_controller.'/'.$action;
        foreach($get_params as $key => $value)
        {
            $url .= '&'.$key.'='.$value;
        }
        
//        $request_result = Yii::app()->curl->get($this->demo_api_url, $get_params);
        $request_result = Yii::app()->curl->post($url, $post_params, [], false, $curl_options);
        
        $request_result_decoded = CJSON::decode($request_result);
        
        if(!isset($request_result_decoded['status']))
        {
//            error_log(__METHOD__.' - 1 - $url: '.CJSON::encode($url));
//            error_log(__METHOD__.' - 1 - $params: '.CJSON::encode($params));
//            error_log(__METHOD__.' - 1 - $request_result: '.$request_result);
            throw new Exception('simademo no status');
        }
        if($request_result_decoded['status'] !== 'success')
        {
            $message = null;
            if(isset($request_result_decoded['message']))
            {
                $message = $request_result_decoded['message'];
            }
            
//            error_log(__METHOD__.' - 2 - $url: '.CJSON::encode($url));
            error_log(__METHOD__.' - 2 - $url: '.$url);
            error_log(__METHOD__.' - 2 - $params: '.CJSON::encode($params));
            error_log(__METHOD__.' - 2 - $request_result: '.$request_result);
            throw new Exception('simademo status not success: '.$message);
        }
    }
}