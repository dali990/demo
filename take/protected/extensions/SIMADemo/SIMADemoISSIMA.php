<?php

class SIMADemoISSIMA extends SIMADemo
{
    private $_session_key = null;
    
    public $demo_api_url = null;
    public $issima_site_url = null;
    
    public function IsDemoISSIMA()
    {
        return true;
    }
    
    public function init()
    {
        if(Yii::app()->isWebApplication() === true)
        {
            if(empty($this->demo_api_url))
            {
                throw new Exception('demo_api_url empty');
            }
            if(empty($this->issima_site_url))
            {
                throw new Exception('issima_site_url empty');
            }

            if(!isset($_SESSION["db_session_key"]))
            {
                throw new Exception('session_key not set');
            }
            $this->_session_key = $_SESSION["db_session_key"];

            $this->validateActiveDBSession();
            
            $this->performMigrationsIfNeeded();
        }
    }
    
    private function validateActiveDBSession()
    {
        $active_session_api_action = 'demoApi/getSessionStatus';
        
        $url = $this->demo_api_url.'?r='.$active_session_api_action.'&session_key='.$this->_session_key;
        $request_result = Yii::app()->curl->get($url);
                
        if($request_result === 'ACTIVE')
        {
            
        }
        else if ($request_result === 'NOT_EXISTS')
        {
            header("Location: {$this->issima_site_url}/demo-not-exists-session");
            die();
        }
        else if ($request_result === 'ARCHIVED')
        {
            header("Location: {$this->issima_site_url}/demo-archived-session");
            die();
        }
        else if ($request_result === 'NOT_AUTHENTICATED')
        {
            header("Location: {$this->issima_site_url}/demo-not-authenticated-session");
            die();
        }
        else if ($request_result === 'EXPIRED')
        {
            header("Location: {$this->issima_site_url}/demo-expired");
            die();
        }
        else
        {
            header("Location: {$this->issima_site_url}/demo-unknown-error");
            die();
        }
    }
    
    private function performMigrationsIfNeeded()
    {
        $action = null;
        if(isset($_GET['r']))
        {
            $action = $_GET['r'];
        }
        if($action === 'user/login' || empty($action))
        {
            $session_key = $this->_session_key;
            
            $lock_key = 'DemoISSIMAPerformMigrationsIfNeeded_'.$session_key;
            try
            {
                Yii::app()->interprocessMutex->Lock($lock_key);
            } 
            catch (SIMAInterprocessMutexLockFileExists $e) 
            {
                return;
            }
            
            $migrateResult = null;
            $command = "echo 'yes' | php yiic migrate -sk $session_key";
            Yii::app()->linuxCommandExecutor->executeCommand($command, null, $migrateResult);
            
            if(!in_array('No new migration found. Your system is up-to-date.', $migrateResult))
            {
                error_log(__METHOD__.' - $migrateResult: '.json_encode($migrateResult));
            }
            
            Yii::app()->interprocessMutex->Unlock($lock_key);
        }
    }
}