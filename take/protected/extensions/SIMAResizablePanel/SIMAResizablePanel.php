<?php

/**
  How to use:

  view:
  $this->beginWidget('ext.SIMAResizablePanel.SIMAResizablePanel',
  array(
  'id'=>'widget_resize',
  'type'=>'vertical' {or} 'horizontal'
  ));

  <div class='sima-layout-panel'>
  </div>
  ..

  $this->endWidget();

 */
class SIMAResizablePanel extends CWidget
{

    public $id=null;
    public $type; //horizonatl-vertical - trenutno se ne kroisti
    public $css = 'css/simaresizablepanel.css';

    /**
     * svaki element ima svoju proporciju koja se zadaje u nizu
     * minimalna proporcija se takodje zadaje, kao i minimalne velicine
     */
    public $min_widths = 200;
    public $max_widths = 2000;
    public $proportions = [];
    public $class = '';

    public function init()
    {
        if (is_null($this->id))
        {
            $this->id = SIMAHtml::uniqid();
        }
        
        echo "<div id=$this->id class='sima-layout-panel _splitter _$this->type $this->class'>";
    }

    public function run()
    {
        echo "</div>";

        $id = $this->id;
        $json = json_encode(array(
            'proportions' => $this->proportions,
            'min_widths' => $this->min_widths,
            'max_widths' => $this->max_widths
        ));
        Yii::app()->clientScript->registerScript(SIMAHtml::uniqid(), "$('#$id').simaResizePanel('init',$json);", CClientScript::POS_READY);
    }

    public function registerManual()
    {
        $assets = dirname(__FILE__) . '/assets';
        $baseUrl = Yii::app()->assetManager->publish($assets);
        Yii::app()->clientScript->registerScriptFile($baseUrl . '/jquery.simaResizePanel.js', CClientScript::POS_HEAD);
//        $css_file = $baseUrl . '/simaresizablepanel.css';
//        Yii::app()->clientScript->registerCssFile($css_file);
    }

}
