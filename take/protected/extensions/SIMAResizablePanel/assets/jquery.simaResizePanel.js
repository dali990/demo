/*
 * jquery plugin koji je vezan za widget simaResizePanel, panele u okviru widgeta postavlja da budu resizabilni
 * @param {type} $
 * @returns {undefined}
 */
/* global sima */

(function($) {

    var default_min_width = 200; //px
    var default_max_width = 2000; //px
    
    /*niz sa metodama u ovom plugin-u*/
    var methods = {
        /*funkcija za inicijalizaciju plugin-a */
        init: function(params) 
        {            
            var whole_widget = $(this);
            whole_widget.data('panels', this.children('.sima-layout-panel'));
            var panels = whole_widget.data('panels');
//            var panels_count = panels.size();
            var panel_iter = 0;
            
            params = params || {};
                        
            params.proportions = params.proportions || (1.0/panels.length);
            params.min_widths = params.min_widths || default_min_width;
            params.max_widths = params.max_widths || default_max_width;
            
            panels.each(function() {
                var current_panel = $(this);
                if (typeof(current_panel.data('proportions')) === 'undefined')
                {
                    var proportion = (typeof params.proportions === 'object') ? params.proportions[panel_iter] : params.proportions;
                    current_panel.data('proportion',proportion);
                }
                if (typeof(current_panel.data('min_width')) === 'undefined')
                {
                    var min_width = (typeof params.min_widths === 'object') ? params.min_widths[panel_iter] : params.min_widths;
                    current_panel.data('min_width',min_width);                    
                }
                if (typeof(current_panel.data('max_width')) === 'undefined')
                {
                    var max_width = (typeof params.max_widths === 'object') ? params.max_widths[panel_iter] : params.max_widths;
                    current_panel.data('max_width',max_width);
                }
                panel_iter++;
            });
        }
    };
    
    /*setovanje metoda plugin-a*/
    $.fn.simaResizePanel = function(methodOrOptions) {
        var arg = arguments;
        this.each(function() {
            if (methods[methodOrOptions]) {
                return methods[ methodOrOptions ].apply($(this), Array.prototype.slice.call(arg, 1));
            } else if (typeof methodOrOptions === 'object' || !methodOrOptions) {
                // Default to "init"
                return methods.init.apply($(this), arg);
            } else {
                $.error('Method ' + method + ' does not exist on jQuery.tooltip');
            }
        });

    };


})(jQuery);