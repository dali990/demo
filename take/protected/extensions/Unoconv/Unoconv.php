<?php

class Unoconv
{
    public $allowed_extensions = [
        'pdf',
        
        'jpg',
        
        'csv',
        'doc',
        'docx',
        'ods',
        'odt',
        'xls',
        'xlsx'
    ];
    
    /// velicina u MB
    public $allowed_extensions_max_sizes = [
        'pdf' => 10,
        'ods' => 1
    ];
    
    //// from - to
    private $convertable_extensions = [
        'pdf' => [
            'jpg',
//            'ods'
        ],
        
        'jpg' => [
            'pdf'
        ],
        
        'csv' => [
            'xls'
        ],
        'doc' => [
            'odt',
            'pdf'
        ],
        'docx' => [
            'odt',
            'pdf'
        ],
        'ods' => [
            'pdf',
            'xls'
        ],
        'odt' => [
            'doc',
            'docx',
            'pdf'
        ],
        'xls' => [
            'ods',
            'pdf'
        ],
        'xlsx' => [
            'pdf'
        ]
    ];
    
    public function init()
    {
    }
    
    /**
     * 
     * @param type $from_ext
     * @param type $file_size
     * @return array
     */
    public function getPossibleConversions($from_ext, $file_size=null)
    {
        /// nema definisane konverzije
        if(!isset($this->convertable_extensions[$from_ext]))
        {
            return [];
        }
        
        /// velicina je veca od dozvoljene
        if(
                !is_null($file_size)                                                            /// ako je prosledjena velicina fajla
                && isset($this->allowed_extensions_max_sizes[$from_ext])                        /// ako ima definisanog max size-a za ekstenziju
                && ($this->allowed_extensions_max_sizes[$from_ext] * 1024 * 1024) < $file_size  /// ako je veci fajl od max size-a
        )
        {
            return [];
        }
        
        return $this->convertable_extensions[$from_ext];
    }
    
    /**
     * za prosledjenu putanju kreira fajl na istoj lokaciji sa istim imenom samo pdf ektenzijom
     * @param type $file_path
     */
    public function ConvertFilePathTo($file_path, $extension, $filter_options=null)
    {        
        if(!in_array($extension, $this->allowed_extensions))
        {
            throw new Exception('invalid extension: '.CJSON::encode($extension));
        }
        
        if(!file_exists($file_path))
        {
            throw new Exception('file does not exist: '.CJSON::encode($file_path));
        }
                        
        putenv('HOME=/tmp');
        
        $file_path_escaped = str_replace('"', '\"', $file_path);
        $file_path_escaped = str_replace('$', '\$', $file_path);
        
        $exec_command = 'unoconv -f '.$extension;
        if(!empty($filter_options))
        {
            $exec_command .= ' -i FilterOptions="'.$filter_options.'"';
        }
        $exec_command .= ' "'.$file_path_escaped.'" 2>&1';
        
        $this->executeCommand($exec_command, $file_path, $extension, true);
    }
    
    private function executeCommand($exec_command, $file_path, $extension, $try_again=false)
    {
        $command_output = null;
        Yii::app()->linuxCommandExecutor->executeCommand($exec_command, Yii::app()->params['linuxcommandExecutor_timeout_s_Unoconv_ConvertFilePathTo'], $command_output);
        
        $allowed_result_lines = [
            ':1: parser error : Document is empty',
            'DF-1.4',
            '^',
            'rtf1\\adeflang1025\\ansi\\ansicpg1252\\uc1\\adeff0\\deff0\\stshfdbch31505\\stshfloch3150',
            '{\rtf1\ansi\ansicpg1252\uc1\deff1\stshfdbch0\stshfloch0\stshfhich0\stshfbi0\defl',
            'rtf1\\ansi\\ansicpg1252\\uc1\\deff1\\stshfdbch0\\stshfloch0\\stshfhich0\\stshfbi0\\deflan',
            'rtf1\ansi\ansicpg1250{\fonttbl{\f0\froman\fcharset238 Times New Roman;}{\f1\fswi',
            'rtf1\ansi\deff3\adeflang1025',
            '{\\rtf1\\ansi\\deff3\\adeflang1025',
            'func=xmlSecCheckVersionExt:file=xmlsec.c:line=188:obj=unknown:subj=unknown:error=19:invalid version:mode=abi compatible;expected minor version=2;real minor version=2;expected subminor version=25;real subminor version=26', /// .odp
            "E: lt_string_value: assertion `string != ((void *)0)' failed", /// fid 495475 u set-u
        ];
        
        if(!empty($command_output))
        {
            $command_output_string = implode("\n", $command_output);
            
            $array_diff_allowed_lines = array_diff($command_output, $allowed_result_lines);
            if(empty($array_diff_allowed_lines))
            {
                return;
            }
            if(SIMAMisc::stringEndsWith($file_path, '.ods') && $extension === 'xls' && strpos($command_output_string, '(ErrCode 3088)') !== false)
            {
                throw new UnoconvInvalidOdsToXlsContentException($command_output_string);
            }
            
//            if($try_again === true && strpos($command_output_string, 'Unable to connect or start own listener. Aborting.') !== false)
            if($try_again === true)
            {
                $random_sleep = rand(0, 5);
                sleep($random_sleep);
                
                $this->executeCommand($exec_command, $file_path, $extension);
            }
            else
            {
                $autobafreq_message = 'unoconv fail'
                                        .' </br>- INPUT_GET: '.SIMAMisc::toTypeAndJsonString(filter_input_array(INPUT_GET))
                                        .' </br>- INPUT_POST: '.SIMAMisc::toTypeAndJsonString(filter_input_array(INPUT_POST))
                                        .' </br>- $exec_command: '.SIMAMisc::toTypeAndJsonString($exec_command)
                                        .' </br>- $file_path: '.SIMAMisc::toTypeAndJsonString($file_path)
                                        .' </br>- $extension: '.SIMAMisc::toTypeAndJsonString($extension)
                                        .' </br>- $command_output_string: '.SIMAMisc::toTypeAndJsonString($command_output_string);
                error_log(__METHOD__.' - $autobafreq_message: '.$autobafreq_message);
                Yii::app()->errorReport->createAutoClientBafRequest($autobafreq_message, 3253, 'AutoBafReq unoconv fail');
                throw new Exception($command_output_string);
            }
        }
    }
}