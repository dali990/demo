<?php

class SIMAInterprocessMutex
{
    public $lock_file_prefix = 'sima_lock_';
    
    public $lock_files_location = null;
    
    public function init($lock_files_location = null)
    {
        if(!is_null($lock_files_location))
        {
            $this->lock_files_location = $lock_files_location;
        }
        else
        {
            $this->lock_files_location = Yii::app()->params['temp_file_repository'];
        }
        
        if(empty($this->lock_files_location))
        {
            throw new Exception('lock_files_location not set');
        }
        
        if(!is_writable($this->lock_files_location))
        {
            throw new Exception('lock_files_location is not writable: '.$this->lock_files_location);
        }
    }
    
    public function Lock($key, $lockData=null)
    {
        if(is_null($lockData))
        {
            $lockData = time();
        }
                
        if($this->isLocked($key))
        {
            throw new SIMAInterprocessMutexLockFileExists($key);
        }
        
        $lock_file_path = $this->getLockFilePath($key);
        
        file_put_contents($lock_file_path, $lockData);
        
        /// TODO idejni fix
//        /// za slucaj da se ne nalazi nas data, neko drugi je ipak uspeo da upise
//        /// moze se desiti da dva procesa u isto vreme prodju if file_exists uslov
//        if($this->GetLockFileData($key) !== $lockData)
//        {
//            throw new SIMAInterprocessMutexLockFileExists($key);
//        }
    }
    
    public function Unlock($key)
    {
        $lock_file_path = $this->getLockFilePath($key);
        
        unlink($lock_file_path);
    }
    
    public function GetLockFileData($key)
    {        
        $data = null;
        if($this->isLocked($key))
        {
            $lock_file_path = $this->getLockFilePath($key);
            $data = file_get_contents($lock_file_path);
        }
        
        return $data;
    }
    
    public function getLockFilePath($key)
    {
        return $this->lock_files_location.DIRECTORY_SEPARATOR.$this->lock_file_prefix.$key;
    }
    
    public function isLocked($key)
    {
        $lock_file_path = $this->getLockFilePath($key);
        return file_exists($lock_file_path);
    }
    
    public function clearAllLocks()
    {
        array_map('unlink', glob($this->lock_files_location.DIRECTORY_SEPARATOR.$this->lock_file_prefix."*"));
    }
}

