<?php

class SIMAInterprocessMutexLockFileExists extends \SIMAException
{
    public function __construct($key)
    {
        $err_msg = 'Lock file exists: '.$key;
        parent::__construct($err_msg);
    }
}