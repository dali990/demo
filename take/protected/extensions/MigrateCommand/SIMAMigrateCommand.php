<?php

class SIMAMigrateCommand extends EMigrateCommand 
{
    public function actionCreate($args) 
    {
        if(empty($args))
        {
            if(Yii::app()->isConsoleApplication())
            {
                echo "no arguments passed\n";
                echo "format: [module name] migration_name\n\n";
                exit;
            }
            else
            {
                throw new Exception('args empty');
            }
        }
        
        $migration_major_version = str_pad(Yii::app()->params['migration_major_version'], 10, "0", STR_PAD_LEFT);
        $current_minor_version = $this->getMigrationMinorVersion(Yii::app()->getBasePath(), $migration_major_version);
        $migration_minor_version = str_pad($current_minor_version+1, 5, "0", STR_PAD_LEFT);
        
        // if module is given adjust path
        if (count($args) == 2) 
        {
            $name = $args[1];
        } 
        else 
        {
            $name = $args[0];
        }

        $name = $migration_major_version . '_' . $migration_minor_version . '_' . $name;
                
        if (count($args) == 2) 
        {
            $args[1] = $name;
        } 
        else 
        {
            $args[0] = $name;
        }
        
        return parent::actionCreate($args);
    }

    protected function getTemplate()
    {
            return <<<EOD
<?php

class {ClassName} extends CDbMigration
{
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
//        Yii::app()->db->createCommand(
//<<<'SIMAMIGRATESQL'
//
//SIMAMIGRATESQL
//        )->execute();
    }

    public function safeDown()
    {
//        Yii::app()->db->createCommand(
//<<<'SIMAMIGRATESQL'
//
//SIMAMIGRATESQL
//        )->execute();
        echo "{ClassName} does not support migration down.\\n";
        return false;
    }
}
EOD;
    }
    
    private function getMigrationMinorVersion($moduleBasePath, $major_version)
    {
        $cur_max_minor_version = -1;
        
        $migrations_directory = $moduleBasePath.'/migrations/';
        
        foreach(glob($migrations_directory.'m'.$major_version.'_*.php') as $filePath)
        {            
            $file_name = str_replace($migrations_directory, '', $filePath);
                        
            $migration_minor_version = (int)(substr($file_name, strlen('m'.$major_version.'_'), 5));
                        
            if($migration_minor_version > $cur_max_minor_version)
            {
                $cur_max_minor_version = $migration_minor_version;
            }
        }
        
        foreach(glob($moduleBasePath.'/modules/*') as $dirPath)
        {
            $temp_max_minor_version = $this->getMigrationMinorVersion($dirPath, $major_version);
            
            if($temp_max_minor_version > $cur_max_minor_version)
            {
                $cur_max_minor_version = $temp_max_minor_version;
            }
        }
        
        return $cur_max_minor_version;
    }
    
    public function beforeAction($action, $params)
    {
        if(session_status() == PHP_SESSION_NONE)
        {
            session_start();
        }
            
        return parent::beforeAction($action, $params);
    }
    
    public function afterAction($action,$params,$exitCode=0)
    {
        parent::afterAction($action,$params,$exitCode);
        
        Yii::app()->db->createCommand(
<<<'SIMAMIGRATESQL'
DO $$
DECLARE
	BEGIN
	IF (SELECT exists(select 1 from pg_proc where proname = 'generate_keywords_from_foreignkeys_cached_fkeys_repopulate'))
	THEN
		PERFORM cache.generate_keywords_from_foreignkeys_cached_fkeys_repopulate();
	END IF;
END $$
SIMAMIGRATESQL
        )->execute();
    }
}
