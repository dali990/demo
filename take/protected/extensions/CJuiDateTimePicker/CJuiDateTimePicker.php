<?php
/**
 * CJuiDateTimePicker class file.
 *
 * @author Anatoly Ivanchin <van4in@gmail.com>
 */

Yii::import('zii.widgets.jui.CJuiDatePicker');
class CJuiDateTimePicker extends CJuiDatePicker
{
	const ASSETS_NAME='/jquery-ui-timepicker-addon';
	
	public $mode='datetime';
        public $readonly = false;
	
	public function init()
	{
		if(!in_array($this->mode, array('date','time','datetime')))
			throw new CException('unknow mode "'.$this->mode.'"');
		if(!isset($this->language))
			$this->language=Yii::app()->getLanguage();
		return parent::init();
	}
	
	public function run()
	{
		list($name,$id)=$this->resolveNameID();

		if(isset($this->htmlOptions['id']))
			$id=$this->htmlOptions['id'];
		else
			$this->htmlOptions['id']=$id;
		if(isset($this->htmlOptions['name']))
			$name=$this->htmlOptions['name'];
		else
			$this->htmlOptions['name']=$name;
                
                $date_format = $this->preapareDateFormatForPlugin($this->options['dateFormat']);
                if($this->mode === 'datetime')
                {   
                    $date_format .= ' '.$this->options['timeFormat'];
                }
//                $params_for_simaDateInputField = [
//                    'date_format' => $date_format,
//                    'show_seconds' => isset($this->options['showSecond']) ? $this->options['showSecond'] : true
//                ];
                
                $uniq = SIMAHtml::uniqid();
                if ($this->readonly === true)
                {
                    $this->htmlOptions['readonly'] = true;
                }
                echo "<div id='$uniq' class='date_range_picker_wrapper'>";
		if($this->hasModel())
			echo CHtml::activeTextField($this->model,$this->attribute,$this->htmlOptions);
		else
			echo CHtml::textField($name,$this->value,$this->htmlOptions);

                $disabled = (isset($this->htmlOptions['disabled']) && ($this->htmlOptions['disabled']===true || $this->htmlOptions['disabled']==='disabled')) ? '_disabled' : '';
                echo "<span class='sima-icon _remove _16 clear_date $disabled'></span>";
                echo "</div>";
                $register_script = "var input_field = $('#$uniq').children('input');"  // uvedeni event-i onValue i onEmpty za zavisna polja
                        . "$('#$uniq').on('click','.sima-icon._remove',function(){ "
                        . "$(this).parent().find('input').val(''); "
                        . "$(this).parent().find('input').trigger('onEmpty'); });" 
                        . "input_field.on('input change', function() {
                                val = $(this).val();
                                if (val === '')
                                {
                                    $(this).trigger('onEmpty');
                                }
                                else
                                {
                                    $(this).trigger('onValue', [{val: val}]);
                                }
                           });
                        ";
//                        . "$('#{$this->htmlOptions['id']}').simaDateInputField('init',". CJSON::encode($params_for_simaDateInputField).");";
                Yii::app()->clientScript->registerScript(SIMAHtml::uniqid(), $register_script, CClientScript::POS_READY);
                
		$options=CJavaScript::encode($this->options);

		$js = "jQuery('#{$id}').{$this->mode}picker($options);";

		if (isset($this->language)){
			$this->registerScriptFile($this->i18nScriptFile);
			$js = "jQuery('#{$id}').{$this->mode}picker(jQuery.extend({showMonthAfterYear:false}, jQuery.datepicker.regional['{$this->language}'], {$options}));";
		}

		$cs = Yii::app()->getClientScript();
		
		
		
		$cs->registerScript(__CLASS__, 	$this->defaultOptions?'jQuery.{$this->mode}picker.setDefaults('.CJavaScript::encode($this->defaultOptions).');':'');
		$cs->registerScript(__CLASS__.'#'.$id, $js);

	}
        
        
        public function registerManual()
        {
            $cs = Yii::app()->getClientScript();
            $assets = Yii::app()->getAssetManager()->publish(dirname(__FILE__).DIRECTORY_SEPARATOR.'assets');
            $cs->registerCssFile($assets.self::ASSETS_NAME.'.css');
            $cs->registerScriptFile($assets.self::ASSETS_NAME.'.js',CClientScript::POS_END);
        }
        
        public function preapareDateFormatForPlugin($date_format)
        {
            if (strpos($date_format,'yy') !== false)
            {
                $year_start_position=strpos($date_format,'yy');
                $date_format=substr($date_format,0,$year_start_position).'yy'.substr($date_format,$year_start_position);
            }
            
            return $date_format;
        }
}