<?php

trait ReceiverTypes
{
    public static $RECEIVER_TYPE_EVENT_RELAY = 'EVENT_RELAY';
    public static $RECEIVER_TYPE_USER = 'USER';
    public static $RECEIVER_TYPE_SESSION = 'SESSION';
    public static $RECEIVER_TYPE_ALL = 'ALL';
}
