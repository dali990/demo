<?php

class SendModelUpdatesWorker
{
    private $_db_connection_string = null;
    private $_db_username = null;
    private $_db_password = null;
    private $_event_relay_port = null;
    
    private $_dbh = null;
    private $_ws_client = null;
//    private $_sma_usage = false;
//    private $_sma_send_handle_exception = true;
    
    private $_models_update_event_php_session_id = null;
    private $_model_tags_to_send_to_all = [];
    private $_model_tags_to_send_per_user = [];
    private $_model_tags_to_send_per_session = [];
//    private $_model_tags_to_send_to_sma = [];
    
    /**
     * key: model tag bez id-a
     * value: kolona koju gledati za userid
     */
    private $_per_user_model_events = [
        'public-sessions_' => 'user_id',
        'public-notifications_' => 'user_id',
    //    'web_sima-paged_table_settings_' => 'user_id'
    ];

    /**
     * key: model tag bez id-a
     * value: kolona koju gledati za session_id
     */
    private $_per_session_model_events = [
    //    'web_sima-paged_table_settings_' => 'session_id'
    ];
    
//    private $_model_tag_parts_for_SMA_send_data = [
//        'public-comments_', /// Comment
//        'public-comment_listeners_', /// CommentListener
//        'public-comment_threads_', /// CommentThread
//        'admin-users_', /// User
//        'bosal-measure_doors_', /// BosalMeasureDoor
//        'bosal-measure_guys_', /// BosalMeasureGuy
//        'bosal-measure_orders_', /// BosalMeasureOrder
//        'bosal-measure_periods_', /// BosalMeasurePeriod
////        'files-files_', /// File
//////        'files-files_to_file_tags', /// FileToFileTag /// salje se jer upada pod file
//    ];
    
    private static $TG_OP_INSERT = 'INSERT';
    private static $TG_OP_UPDATE = 'UPDATE';
    private static $TG_OP_DELETE = 'DELETE';
    
    private $_last_time_db_ping_timestamp = 0;
    
    /**
     * 
     * @param array $conf
     *  'db_params' => [
     *      'connectionString',
     *      'username',
     *      'password'
     *  ],
     *  'event_relay_port'
     */
    public function __construct($conf)
    {
        if(!isset($conf['db_params']))
        {
            throw new \Exception('no db_params');
        }
        if(!isset($conf['event_relay_port']))
        {
            throw new \Exception('no event_relay_port');
        }
        $db_params = $conf['db_params'];
        $this->_event_relay_port = $conf['event_relay_port'];
        
        if(!isset($db_params['connectionString']))
        {
            throw new \Exception('no connectionString');
        }
        if(!isset($db_params['username']))
        {
            throw new \Exception('no username');
        }
        if(!isset($db_params['password']))
        {
            throw new \Exception('no password');
        }
        
        $this->_db_connection_string = $db_params['connectionString'];
        $this->_db_username = $db_params['username'];
        $this->_db_password = $db_params['password'];
        
//        if(isset($conf['sma_usage']) && $conf['sma_usage'] === true)
//        {
////            $this->_sma_usage = TRUE; /// NAJBRZE ISKLJUCIVANJE UPOTREBE SMA U EVENTRELAY-U
//        }
//        if(isset($conf['sma_send_handle_exception']) && $conf['sma_send_handle_exception'] === FALSE)
//        {
//            $this->_sma_send_handle_exception = FALSE;
//        }
    }
    
    public function run()
    {        
        $this->connectToDB();
        $this->_last_time_db_ping_timestamp = time();
        $this->connectToWS();
        
        while(1)
        {
            $conf = include('conf.php');
            if(isset($conf['to_exit']) && $conf['to_exit'] === true)
            {
                exit();
            }
            
            $this->dbPing();

            // wait for one Notify N seconds instead of using sleep(N)
//            $max_wait_ms = 10000; /// 10s
            $max_wait_ms = 1000; /// 1s
            $models_update_event_php_session_id = $this->getNotify($max_wait_ms);
            if($models_update_event_php_session_id !== true)
            {
                continue;
            }

            $this->getEventsAndSend();
        }
    }
    
    public function connectToDB()
    {
        $this->_dbh = new \PDO($this->_db_connection_string, $this->_db_username, $this->_db_password);
        $this->_dbh->exec('TRUNCATE base.models_update_events2;');   // those doublequotes are very important
        $this->_dbh->exec('TRUNCATE base.models_update_events2_completed_phpsessionids;');   // those doublequotes are very important
        $this->_dbh->exec('LISTEN "model_update_event"');   // those doublequotes are very important
    }
    
    private function connectToWS()
    {
        require dirname(dirname(__FILE__)) . '/client/vendor/autoload.php';
        $this->_ws_client = new \WebSocket\Client("ws://0.0.0.0:".$this->_event_relay_port);
    }
    
    private function dbPing()
    {
        $max_sec_since_last_ping = 10; /// 10s
        if(time()-$this->_last_time_db_ping_timestamp < $max_sec_since_last_ping)
        {
            return;
        }
        
        $this->_last_time_db_ping_timestamp = time();
        
        $ping_result = $this->_dbh->query("select 'OK';"); 
        if($ping_result === false)
        {
            $this->connectToDB();
        }
    }
    
    public function getNotify($max_wait_ms)
    {
        $model_update_notify = $this->_dbh->pgsqlGetNotify(PDO::FETCH_ASSOC, $max_wait_ms);
        if($model_update_notify === false)
        {
            return false;
        }

        if(!isset($model_update_notify['payload']))
        {
            return false;
        }
        if(empty($model_update_notify['payload']))
        {
            return false;
        }
        
        $this->_models_update_event_php_session_id = $model_update_notify['payload'];
        
        return true;
    }
    
    private function getEventsAndSend()
    {
        /// get tags
        $this->getEventsAndSend_getTags();

        /// slanje po korisniku
        try
        {
            $this->sendEventsPerUser();
        }
        catch(\Exception $e)
        {
            error_log(__METHOD__.' - '.$e->getMessage());
        }

        /// slanje po sesiji
        try
        {
            $this->sendEventsPerSession();
        }
        catch(\Exception $e)
        {
            error_log(__METHOD__.' - '.$e->getMessage());
        }

        /// slanje svima
        try
        {
            $this->sendEventsToEveryone();
        }
        catch(\Exception $e)
        {
            error_log(__METHOD__.' - '.$e->getMessage());
        }
        
        /// slanje ka SMA
//        if($this->_sma_usage === true)
//        {
//            $this->sendEventsToSMAs();
//        }

        $this->markPHPSessionIdProcessed();
    }
    
    public function getModelTagsToSendToAll()
    {
        return $this->_model_tags_to_send_to_all;
    }
    
    public function getModelTagsToSendPerUser()
    {
        return $this->_model_tags_to_send_per_user;
    }
    
    public function getModelTagsToSendPerSession()
    {
        return $this->_model_tags_to_send_per_session;
    }
    
//    public function getModelTagsToSendToSMA()
//    {
//        return $this->_model_tags_to_send_to_sma;
//    }
    
    public function getEventsAndSend_getTags()
    {
        $this->_model_tags_to_send_to_all = [];
        $this->_model_tags_to_send_per_user = [];
        $this->_model_tags_to_send_per_session = [];
//        $this->_model_tags_to_send_to_sma = [];

        $get_model_update_events_for_sending_sql = "SELECT UNNEST(CASE 
		WHEN tg_op = 'INSERT' THEN base.cmue_generate_row_keywords(tg_table_schema, tg_table_name, new_data) 
		WHEN tg_op = 'UPDATE' THEN base.cmue_generate_row_keywords_update(tg_table_schema, tg_table_name, old_data, new_data)
		WHEN tg_op = 'DELETE' THEN base.cmue_generate_row_keywords(tg_table_schema, tg_table_name, old_data)
	END) as model_tag, 
	*
FROM base.models_update_events2
WHERE php_session_id='{$this->_models_update_event_php_session_id}'
ORDER BY id ASC;";

        $sth = $this->_dbh->prepare($get_model_update_events_for_sending_sql);
        $sth->execute();
        $get_model_update_events_for_sending_result = $sth->fetchAll();
        $model_update_events = $this->getEventsAndSend_getTags_parseDuplicateEvents($get_model_update_events_for_sending_result);
        
        $model_update_events_with_relations = $model_update_events;
        
        $cmue_generate_row_keywords_from_foreignkeys_sql = 'SELECT * FROM base.cmue_generate_row_keywords_from_foreignkeys(:tg_table_schema, :tg_table_name, :data);';
        $cmue_generate_row_keywords_from_foreignkeys_sth = $this->_dbh->prepare($cmue_generate_row_keywords_from_foreignkeys_sql);
        $cmue_generate_row_keywords_from_foreignkeys_update_sql = 'SELECT * FROM base.cmue_generate_row_keywords_from_foreignkeys_update(:tg_table_schema, :tg_table_name, :old_data, :new_data);';
        $cmue_generate_row_keywords_from_foreignkeys_update_sth = $this->_dbh->prepare($cmue_generate_row_keywords_from_foreignkeys_update_sql);
        
        foreach($model_update_events as $model_update_event)
        {            
            $operation = $model_update_event['tg_op'];
            
            $foreign_tags_events = null;
            
            if($operation === self::$TG_OP_INSERT)
            {
                $cmue_generate_row_keywords_from_foreignkeys_sth->execute([
                    ':tg_table_schema' => $model_update_event['tg_table_schema'],
                    ':tg_table_name' => $model_update_event['tg_table_name'],
                    ':data' => $model_update_event['new_data']
                ]);
                $foreign_tags_events = $cmue_generate_row_keywords_from_foreignkeys_sth->fetchAll();
            }
            else if($operation === self::$TG_OP_UPDATE)
            {
                $cmue_generate_row_keywords_from_foreignkeys_update_sth->execute([
                    ':tg_table_schema' => $model_update_event['tg_table_schema'],
                    ':tg_table_name' => $model_update_event['tg_table_name'],
                    ':old_data' => $model_update_event['old_data'],
                    ':new_data' => $model_update_event['new_data']
                ]);
                $foreign_tags_events = $cmue_generate_row_keywords_from_foreignkeys_update_sth->fetchAll();
            }
            else if($operation === self::$TG_OP_DELETE)
            {
                $cmue_generate_row_keywords_from_foreignkeys_sth->execute([
                    ':tg_table_schema' => $model_update_event['tg_table_schema'],
                    ':tg_table_name' => $model_update_event['tg_table_name'],
                    ':data' => $model_update_event['old_data']
                ]);
                $foreign_tags_events = $cmue_generate_row_keywords_from_foreignkeys_sth->fetchAll();
            }
            else
            {
                throw new Exception('unknown operation');
            }
            
            if(empty($foreign_tags_events))
            {
                continue;
            }
            
            $foreign_tags_trimmed = trim($foreign_tags_events[0][0], '{}');
            if(empty($foreign_tags_trimmed))
            {
                continue;
            }
            
            $foreign_tags = explode(',', $foreign_tags_trimmed);
            $foreign_model_update_event_mask = $model_update_event;
            $foreign_model_update_event_mask['new_data'] = null;
            $foreign_model_update_event_mask['old_data'] = null;
            foreach($foreign_tags as $foreign_tag)
            {
                foreach($model_update_events_with_relations as $model_update_event_with_relations)
                {
                    if($model_update_event_with_relations['model_tag'] === $foreign_tag)
                    {
                        continue 2;
                    }
                }

                $foreign_model_update_event = $foreign_model_update_event_mask;
                $foreign_model_update_event['model_tag'] = $foreign_tag;
                $model_update_events_with_relations[] = $foreign_model_update_event;
            }
        }
        
        foreach($model_update_events_with_relations as $model_update_event)
        {
            $this->getEventsAndSend_getTags_parseRow($model_update_event);
        }
    }
    
    private function getEventsAndSend_getTags_parseDuplicateEvents($rows)
    {
        $result = [];
        $grouped_by_models_rows = [];
        foreach($rows as $row)
        {
            $model_tag = $row['model_tag'];
            $operation = $row['tg_op'];
            if(!isset($grouped_by_models_rows[$model_tag]))
            {
                $grouped_by_models_rows[$model_tag] = [
                    'counter' => 0,
                    'rows' => [
                        'INSERT' => [],
                        'UPDATE' => [],
                        'DELETE' => []
                    ]
                ];
            }
            $grouped_by_models_rows[$model_tag]['counter'] = $grouped_by_models_rows[$model_tag]['counter']+1;
            $grouped_by_models_rows[$model_tag]['rows'][$operation][] = $row;
        }
        
        foreach($grouped_by_models_rows as $model_tag => $grouped_by_model_rows)
        {
            if($grouped_by_model_rows['counter'] > 1)
            {
                $final_row = null;
                if(!empty($grouped_by_model_rows['rows']['INSERT']))
                {
                    $final_row = $grouped_by_model_rows['rows']['INSERT'][0];
                }
                else if(!empty($grouped_by_model_rows['rows']['UPDATE']))
                {
                    $final_row = $grouped_by_model_rows['rows']['UPDATE'][0];
                }
                else
                {
                    $final_row = $grouped_by_model_rows['rows']['DELETE'][0];
                    $result[] = $final_row;
                    continue;
                }
                                
                if(!empty($grouped_by_model_rows['rows']['DELETE']))
                {
                    $final_row['tg_op'] = 'DELETE';
                    $final_row['old_data'] = end($grouped_by_model_rows['rows']['DELETE'])['old_data'];
                    $result[] = $final_row;
                    continue;
                }
                else if(!empty($grouped_by_model_rows['rows']['UPDATE']))
                {
                    $final_row['new_data'] = end($grouped_by_model_rows['rows']['UPDATE'])['new_data'];
                    $result[] = $final_row;
                    continue;
                }
                
                $result[] = $final_row;
            }
            else
            {
                if(!empty($grouped_by_model_rows['rows']['INSERT']))
                {
                    $result[] = $grouped_by_model_rows['rows']['INSERT'][0];
                }
                else if(!empty($grouped_by_model_rows['rows']['UPDATE']))
                {
                    $result[] = $grouped_by_model_rows['rows']['UPDATE'][0];
                }
                else if(!empty($grouped_by_model_rows['rows']['DELETE']))
                {
                    $result[] = $grouped_by_model_rows['rows']['DELETE'][0];
                }
            }
        }
        
        return $result;
    }
    
    private function getEventsAndSend_getTags_parseRow($get_model_update_events_for_sending_row)
    {
        if(!isset($get_model_update_events_for_sending_row['model_tag']))
        {
            throw new \Exception('$get_model_update_events_for_sending_row not have model_tag');
        }
        $model_tag = $get_model_update_events_for_sending_row['model_tag'];

        $data_decoded = null;
        if($get_model_update_events_for_sending_row['tg_op'] === 'DELETE')
        {
            $data_decoded = json_decode($get_model_update_events_for_sending_row['old_data']);
        }
        else
        {
            $data_decoded = json_decode($get_model_update_events_for_sending_row['new_data']);
        }

//        if($this->_sma_usage === true && $this->isSMAModelTag($model_tag) && !in_array($model_tag, $this->_model_tags_to_send_to_sma))
//        {
//            $this->_model_tags_to_send_to_sma[] = $model_tag;
//        }

        if(!is_null($data_decoded))
        {
            if($this->getEventsAndSend_getTags_parsePerUserModelEvents($model_tag, $data_decoded) === true)
            {
                return;
            }

            if($this->getEventsAndSend_getTags_parsePerSessionModelEvents($model_tag, $data_decoded) === true)
            {
                return;
            }
        }

        if(!in_array($model_tag, $this->_model_tags_to_send_to_all))
        {
            $this->_model_tags_to_send_to_all[] = $model_tag;
        }
    }
    
    private function getEventsAndSend_getTags_parsePerUserModelEvents($model_tag, $data_decoded)
    {
        $parsed_tag_to_send = false;
        foreach($this->_per_user_model_events as $key=>$value)
        {
            if(strlen($model_tag) <= strlen($key) || substr($model_tag, 0, strlen($key)) !== $key)
            {
                continue;
            }
            
            $parsed_tag_to_send = true;
            
            $this->getEventsAndSend_getTags_parsePerUserModelEvents_parsePerUserModelEvent($data_decoded, $value, $model_tag);
        }
        
        return $parsed_tag_to_send;
    }
    
    private function getEventsAndSend_getTags_parsePerUserModelEvents_parsePerUserModelEvent($data_decoded, $value, $model_tag)
    {
        $user_id = $data_decoded->$value;

        if(!isset($this->_model_tags_to_send_per_user[$user_id]))
        {
            $this->_model_tags_to_send_per_user[$user_id] = [];
        }
        if(!in_array($model_tag, $this->_model_tags_to_send_per_user[$user_id]))
        {
            if(!in_array($model_tag, $this->_model_tags_to_send_per_user[$user_id]))
            {
                $this->_model_tags_to_send_per_user[$user_id][] = $model_tag;
            }
        }
    }
    
    private function getEventsAndSend_getTags_parsePerSessionModelEvents($model_tag, $data_decoded)
    {
        $parsed_tag_to_send = false;

        foreach($this->_per_session_model_events as $key => $column_name)
        {
            if(strlen($model_tag) <= strlen($key) || substr($model_tag, 0, strlen($key)) !== $key)
            {
                continue;
            }
            
            $parsed_tag_to_send = true;
            
            $this->getEventsAndSend_getTags_parsePerSessionModelEvents_parsePerSessionModelEvent($data_decoded, $column_name, $model_tag);
        }
        
        return $parsed_tag_to_send;
    }
    
    private function getEventsAndSend_getTags_parsePerSessionModelEvents_parsePerSessionModelEvent($data_decoded, $column_name, $model_tag)
    {
        $session_id = $data_decoded->$column_name;

        if(!isset($this->_model_tags_to_send_per_session[$session_id]))
        {
            $this->_model_tags_to_send_per_session[$session_id] = [];
        }
        if(!in_array($model_tag, $this->_model_tags_to_send_per_session[$session_id]))
        {
            if(!in_array($model_tag, $this->_model_tags_to_send_per_session[$session_id]))
            {
                $this->_model_tags_to_send_per_session[$session_id][] = $model_tag;
            }
        }
    }
    
    private function sendEventsPerUser()
    {
        foreach($this->_model_tags_to_send_per_user as $user_id => $model_tags_to_send)
        {            
            $session_key = 'sima';

            $message = [
                'sender_user_id' => '',
                'sender_session_id' => '',
                'sender_type' => 'DB',
                'receiver_id' => $user_id,
        //                    'receiver_type' => Message::$RECEIVER_TYPE_USER,
                'receiver_type' => 'USER',
                'message_type' => 'UPDATE_VUE_MODELS',
                'data' => $model_tags_to_send,
                'session_key' => $session_key
            ];
            $json_message = json_encode($message);
            $this->_ws_client->send($json_message);
        }
    }
    
    private function sendEventsPerSession()
    {
        foreach($this->_model_tags_to_send_per_session as $session_id => $model_tags_to_send)
        {
            $session_key = 'sima';

            $message = [
                'sender_user_id' => '',
                'sender_session_id' => '',
                'sender_type' => 'DB',
                'receiver_id' => $session_id,
        //                    'receiver_type' => Message::$RECEIVER_TYPE_SESSION,
                'receiver_type' => 'SESSION',
                'message_type' => 'UPDATE_VUE_MODELS',
                'data' => $model_tags_to_send,
                'session_key' => $session_key
            ];
            $json_message = json_encode($message);
            $this->_ws_client->send($json_message);
        }
    }
    
    private function sendEventsToEveryone()
    {
        if(!empty($this->_model_tags_to_send_to_all))
        {            
            $session_key = 'sima';

            $message = [
                'sender_user_id' => '',
                'sender_session_id' => '',
                'sender_type' => 'DB',
                'receiver_id' => '',
        //                    'receiver_type' => Message::$RECEIVER_TYPE_ALL,
                'receiver_type' => 'ALL',
                'message_type' => 'UPDATE_VUE_MODELS',
                'data' => $this->_model_tags_to_send_to_all,
                'session_key' => $session_key
            ];
            $json_message = json_encode($message);
            $this->_ws_client->send($json_message);
        }
    }
    
//    private function isSMAModelTag($model_tag)
//    {
//        $result = false;
//        
//        foreach($this->_model_tag_parts_for_SMA_send_data as $_model_tag_parts_for_SMA_send_data_part)
//        {
//            if(strpos($model_tag, $_model_tag_parts_for_SMA_send_data_part) !== false)
//            {
//                $result = true;
//                break;
//            }
//        }
//        
//        return $result;
//    }
    
//    public function sendEventsToSMAs()
//    {
//        $events_sent_sum = 0;
//        
//        if(empty($this->_model_tags_to_send_to_sma))
//        {
//            return $events_sent_sum;
//        }
//        
//        $data_to_send = [
//            'event' => 'UPDATE_VUE_MODELS',
//            'data' => implode(',', $this->_model_tags_to_send_to_sma)
//        ];
//        
//        $ch = curl_init();
//        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
//        curl_setopt($ch, CURLOPT_POST, true);
//        curl_setopt($ch, CURLOPT_HTTPHEADER, [
//            'Authorization:key = AAAAg_IKfwU:APA91bHo9a2bJGhZNhCsJys6XN4sLrijB_p6lOMN9InZ3SLL5mUHOEtW6Hzxu9bOCQG5OHJ_2zub3Wc4xdNI6sb11dwMdpETl2OJH2oxPooxybNvSMWQCUHiT_U_vJg9gzxXWlkFlkC3',//Server key from firebase
//            'Content-Type: application/json'
//        ]);
//        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
//        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
//        
//        $sql = 'SELECT google_token FROM mobile.authenticated_devices WHERE in_usage=TRUE AND registered_for_events=TRUE;';
//        $sth = $this->_dbh->prepare($sql);
//        $sth->execute();
//        
//        while($row = $sth->fetch())
//        {
//            $sma_google_token = $row['google_token'];
//            $this->sendDataToSMA($ch, $sma_google_token, $data_to_send);
//            $events_sent_sum++;
//        }
//        
//        curl_close($ch);
//        
//        return $events_sent_sum;
//    }
    
//    private function sendDataToSMA($curl_handler, $sma_google_token, $data)
//    {
//        curl_setopt($curl_handler, CURLOPT_POSTFIELDS, json_encode([
//            'to' => $sma_google_token,
//            'data' => $data
//        ]));
//        $result = curl_exec($curl_handler);
//
//        try
//        {
//            if(empty($result))
//            {
//                throw new Exception('result empty');
//            }
//
//            $result_json = json_decode($result);
//            
//            if(empty($result_json))
//            {
//                throw new Exception('decoded result empty');
//            }
//            
//            if($result_json->success !== 1)
//            {
//                $error_message = 'result not success';
//                $errors = '';
//                if(isset($result_json->results))
//                {
//                    $errors = json_encode($result_json->results);
//                }
//                if(!empty($errors))
//                {
//                    $error_message .= ' - $errors: '.$errors;
//                }
//                
//                throw new Exception($error_message);
//            }
//        }
//        catch(Exception $e)
//        {
//            if($this->_sma_send_handle_exception === true)
//            {
//                $this->sendDataToSMA_handleException($e, $result, $sma_google_token, $data);
//            }
//            else
//            {
//                throw $e;
//            }
//        }
//    }
    
//    private function sendDataToSMA_handleException(Exception $e, $result, $sma_google_token, $data)
//    {
//        
//        /**
//         * bitan je redosled
//         * desava se da fields-a ima mnogo pa se ne vidi ostatak
//         */
//        $error_message = substr(
//                    __METHOD__
//                    .' - exception message: '.$e->getMessage().PHP_EOL
//                    .' - result: '.json_encode($result).PHP_EOL
//                    .' - $data: '.substr(json_encode($data), 0, 200).PHP_EOL
//                    .' - for $sma_google_token: '.$sma_google_token,
//                0, 1000);
//        error_log($error_message);
//        
//        
//        $sql = 'UPDATE mobile.authenticated_devices SET last_error=:lasterror WHERE google_token=:sma_google_token';
//        $sth = $this->_dbh->prepare($sql);
//        $sth->execute([
//            ':lasterror' => $error_message,
//            ':sma_google_token' => $sma_google_token
//        ]);
//    }
    
    private function markPHPSessionIdProcessed()
    {
        if(
                !empty($this->_model_tags_to_send_to_all)
                ||
                !empty($this->_model_tags_to_send_per_user)
                ||
                !empty($this->_model_tags_to_send_per_session)
        )
        {
            $sql = "UPDATE base.models_update_events2_completed_phpsessionids"
                                    . " SET processed_time=now()"
                                    . " WHERE php_session_id='{$this->_models_update_event_php_session_id}'";
            $this->_dbh->exec($sql);
        }
    }
}
