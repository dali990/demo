<?php

require __DIR__.'/../autoload.php';
require_once __DIR__.'/SendModelUpdatesWorker.php';

$conf = include(__DIR__.'/conf.php');
$lock_files_location = $conf['lock_files_location'];
$lockKey = 'SIMAEventRelay_SendUpdateModelEvents';

register_shutdown_function('shutdownFunction', $lock_files_location, $lockKey);

$endModelUpdatesWorker = new SendModelUpdatesWorker($conf);
$endModelUpdatesWorker->run();

function shutdownFunction($lock_files_location, $lockKey)
{
    /// otkljucaj lock file
    $interprocess_mutex = new \SIMAInterprocessMutex();
    $interprocess_mutex->init($lock_files_location);
    $interprocess_mutex->Unlock($lockKey);
}