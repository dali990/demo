<?php

function print_help_and_exit($additional_message = '')
{
    if(!empty($additional_message))
    {
        echo "$additional_message \n";
    }
    exit(1);
}

if(count($argv) !== 2)
{
    print_help_and_exit('invalid number of arguments');
}

$logs_file_path = $argv[1];
if(!file_exists($logs_file_path))
{
    print_help_and_exit('invalid file $logs_file_path: '.$logs_file_path);
}

$sql = file_get_contents($logs_file_path);

$conf = include('conf.php');
$db_params = $conf['db_params'];

$dbh = new \PDO($db_params['connectionString'], $db_params['username'], $db_params['password']);
$dbh->exec($sql);
