<?php

//use Ratchet\Server\IoServer;
use EventRelay\MessageHandler;

require __DIR__.'/../vendor/autoload.php';
require __DIR__.'/../autoload.php';
require_once __DIR__.'/../src/EventRelay/SIMAIoServer.php';

use Ratchet\Server\SIMAIoServer;
use EventRelay\SIMAHttpServer;
use EventRelay\SIMAWsServer;

try
{
echo 'pid: '.getmypid()."\n";
$conf = include('conf.php');
$event_relay_port = $conf['event_relay_port'];
$clients_aftersend_sleep_s = $conf['clients_aftersend_sleep_s'];
$clients_keepalive_check_sleep_s = $conf['clients_keepalive_check_sleep_s'];
$clients_keepalive_check_s = $conf['clients_keepalive_check_s'];
$send_update_model_events_s = $conf['send_update_model_events_s'];
$timecheck_s = $conf['timecheck_s'];
$lock_files_location = $conf['lock_files_location'];
$logfile = $conf['logfile'];
$pushLogsToDb_s = $conf['push_logs_to_db_s'];

if(!file_exists($logfile))
{
    throw new \Exception('$logfile not exists: '.json_encode($logfile));
}

$messageHandlerContructParams = new \EventRelay\MessageHandlerContructParams();
//$messageHandlerContructParams->db_params = $conf['db_params'];
$messageHandlerContructParams->message_queue_count_to_log = $conf['message_queue_count_to_log'];
$messageHandlerContructParams->clients_keepalive_check_s = $conf['clients_keepalive_check_s'];
$messageHandlerContructParams->log_to_db = $conf['log_to_db'];
$messageHandlerContructParams->log_messages = $conf['log_messages'];
$messageHandlerContructParams->is_ssl = $conf['is_ssl'];
$messageHandlerContructParams->messages_to_send_number = $conf['messages_to_send_number'];
$messageHandlerContructParams->messagequeue_exact_count_to_log = $conf['messagequeue_exact_count_to_log'];
$messageHandlerContructParams->max_messagesending_exec_time_s = $conf['max_messagesending_exec_time_s'];
$messageHandlerContructParams->sendmessage_max_prev_start_time_diff_s = $conf['sendmessage_max_prev_start_time_diff_s'];
$messageHandlerContructParams->clients_keepalive_check_sleep_s = $clients_keepalive_check_sleep_s;
$messageHandlerContructParams->validate();

//$datetme_starting = new \DateTime();
//echo $datetme_starting->format('Y-m-d H:i:s.u')." - eventrelay.php starting\n";
//echo "push_logs_to_db_s: $pushLogsToDb_s\n";

//$server = IoServer::factory(
$server = SIMAIoServer::factory(
		new SIMAHttpServer(
		    new SIMAWsServer(
			new MessageHandler($messageHandlerContructParams)
		    )
		)
//                ),
//		$event_relay_port
	);

$server->loop->addPeriodicTimer($clients_aftersend_sleep_s, function() use (&$server){
    $server->app->simaSendMessage();
});
$server->loop->addPeriodicTimer($clients_keepalive_check_s, function() use (&$server){
    $server->app->simaCheckClientsForOldKeepAlive();
});
$server->loop->addPeriodicTimer($clients_keepalive_check_s, function() use (&$server){
    $server->app->simaSendKeepaliveToClients();
});
$server->loop->addPeriodicTimer($pushLogsToDb_s, function() use (&$server, $logfile){
    $logs_file_path = __DIR__.'/logs_for_db.sql';
    $dumped_logs = $server->app->simaDumpLogsSQLToFile($logs_file_path);
    if($dumped_logs === true)
    {
        $script_path = __DIR__.'/logsToDb.php';
        $command_to_exec = 'nohup php '.$script_path.' '.$logs_file_path.' >> '.$logfile.' 2>&1 &';
        exec($command_to_exec);
    }
});
/// na 1s proverava da li u conf stoji flag toexit
$server->loop->addPeriodicTimer(1, function(){
    $conf = include('conf.php');
    if(isset($conf['to_exit']) && $conf['to_exit'] === true)
    {
        $datetme = new \DateTime();
        echo $datetme->format('Y-m-d H:i:s.u')." - eventrelay server stopped\n";
        exit();
    }
});

$lock_key_SendUpdateModelEvents = 'SIMAEventRelay_SendUpdateModelEvents';
$lock_key_timecheck = 'TimeCheckCommand';
$interprocess_mutex = new \SIMAInterprocessMutex();
$interprocess_mutex->init($lock_files_location);

/// tek se startuje eventrelay, i ako je ostao lock od ranije, nije potreban
if($interprocess_mutex->isLocked($lock_key_SendUpdateModelEvents))
{
    $interprocess_mutex->Unlock($lock_key_SendUpdateModelEvents);
}
if($interprocess_mutex->isLocked($lock_key_timecheck))
{
    $interprocess_mutex->Unlock($lock_key_timecheck);
}

$eventRelaySendModelUpdates_function = function() use ($interprocess_mutex, $lock_key_SendUpdateModelEvents, $logfile){
    /// postavi lock fajl (ako postoji, zavrsi)
    try
    {
        $interprocess_mutex->Lock($lock_key_SendUpdateModelEvents);
    } 
    catch (\SIMAInterprocessMutexLockFileExists $e) 
    {
        return;
    }

    $script_path = __DIR__.'/eventRelaySendModelUpdates2.php';
    $command_to_exec = 'nohup php '.$script_path.' >> '.$logfile.' 2>&1 &';
//    echo "command_to_exec: $command_to_exec\n";
    exec($command_to_exec);
//    $command_to_exec_result = shell_exec($command_to_exec);
//    echo "command_to_exec_result: $command_to_exec_result\n";
};

/// izvrsava odmah
$eventRelaySendModelUpdates_function();

/// izvrsava na svakih $send_update_model_events_s sekundi, prvi put nakon $send_update_model_events_s sekundi
$server->loop->addPeriodicTimer($send_update_model_events_s, $eventRelaySendModelUpdates_function);

$timecheckFunction = function () use ($interprocess_mutex, $lock_key_timecheck, $logfile){
    try
    {
        $interprocess_mutex->Lock($lock_key_timecheck);
    } 
    catch (\SIMAInterprocessMutexLockFileExists $e) 
    {
        return;
    }

    $basePath = str_replace('/protected/extensions/SIMAEventRelay/bin/eventRelay.php', '', __FILE__);
    $sima_console_path = $basePath.'/console.php';
    $command_to_exec = 'nohup php '.$sima_console_path.' TimeCheckEventRelay >> '.$logfile.' 2>&1 &';
    exec($command_to_exec);
};

/// izvrsava odmah
$timecheckFunction();

/// izvrsava na svakih $timecheck_s sekundi, prvi put nakon $timecheck_s sekundi
$server->loop->addPeriodicTimer($timecheck_s, $timecheckFunction);

//$server_run_result = $server->run();
$server_run_result = $server->run($event_relay_port);

//$datetme_started = new \DateTime();
//echo $datetme_started->format('Y-m-d H:i:s.u')." - eventrelay.php started\n";
}
catch(\Exception $e)
{
    $exception_message = $e->getMessage();
    echo $exception_message."\n".$e->getTraceAsString()."\n";
    
    $preg_match_output_array = null;
    preg_match('/Could not bind to tcp:\/\/0.0.0.0:\d{1,}: Address already in use/', $exception_message, $preg_match_output_array);
    if(!empty($preg_match_output_array))
    {
        exec("ss -tlwnp | grep LISTEN | grep :$event_relay_port", $output_message);
        echo implode("\n", $output_message)."\n";
        
        $output_message_last_elem = end($output_message);
        $users_pos = strpos($output_message_last_elem, 'users:(');
        if($users_pos !== false)
        {
            $users_part = substr($output_message_last_elem, $users_pos);
            preg_match_all('/pid=\d{1,}/', $users_part, $users_preg_match_output_array);
            if(!empty($users_preg_match_output_array))
            {
                $pid_parts = $users_preg_match_output_array[0];
                foreach($pid_parts as $pid_part)
                {
                    $pid = substr($pid_part, 4); /// 'pid='
                    $pid_info = shell_exec("ps -p $pid -o pid,ppid,command:150,time | grep $pid");
                    echo '$pid: '.$pid.' - $pid_info: '.$pid_info."\n";
                }
            }
        }
    }
    
    exit(1);
}
exit(0);
