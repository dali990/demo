<?php
/**
 * da bi moglo iz iz pretrazivaca i iz terminala da se rekreira
 * chmod 777 conf.php
 */
return [
    'event_relay_port' => 56712,
    'clients_aftersend_sleep_s' => 0.1,
    'is_ssl' => false,
    'message_queue_count_to_log' => 20,
    'clients_keepalive_check_sleep_s' => 30,
    'clients_keepalive_check_s' => 10,
    'send_update_model_events_s' => 1,
    'log_messages' => false,
    'lock_files_location' => '/opt/temp',
    'db_params' => [
        'connectionString' => '',
        'username' => 'postgres',
        'password' => 'postgres',
    ],
];