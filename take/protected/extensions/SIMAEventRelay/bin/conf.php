<?php
return [
    'event_relay_port' => 52114,
    'clients_aftersend_sleep_s' => 0.1,
    'is_ssl' => false,
    'message_queue_count_to_log' => 20,
    'clients_keepalive_check_sleep_s' => 30,
    'clients_keepalive_check_s' => 10,
    'send_update_model_events_s' => 5,
    'timecheck_s' => 5,
    'log_messages' => false,
    'lock_files_location' => '/var/www/html/sima4/protected/runtime/temp/',
    'db_params' => [
        'connectionString' => 'pgsql:host=localhost;port=5432;dbname=bosal',
        'username' => 'postgres',
        'password' => 'postgres'
    ],
    'logfile' => 'protected/runtime/eventRelay.log',
    'to_exit' => false,
    'log_to_db' => true,
    'sma_usage' => FALSE,
    'messages_to_send_number' => 1,
    'messagequeue_exact_count_to_log' => 1000,
    'max_messagesending_exec_time_s' => 1,
    'sendmessage_max_prev_start_time_diff_s' => 1,
    'push_logs_to_db_s' => 60
];