<?php

require __DIR__ . '/vendor/autoload.php';
require_once __DIR__.'/../SIMAEventRelayTraits.php';

use WebSocket\Client;

class WebSocketClient
{
    use ReceiverTypes;
    
    public static function send($receiver_id, $receiver_type, $message_type, $data)
    {
        WebSocketClient::sendBulk([
            [
                'receiver_id' => $receiver_id,
                'receiver_type' => $receiver_type,
                'message_type' => $message_type,
                'data' => $data
            ]
        ]);
    }
    
    public static function sendBulk(array $messages_data)
    {        
        $sender_user_id = -1;
        $session_id = -1;
        $session_key = Yii::app()->getSessionKey();
        if(Yii::app()->isWebApplication() && !Yii::app()->user->isGuest)
        {
            $sender_user_id = Yii::app()->user->id;
            $session_id = Yii::app()->user->db_session->id;
        }
        
        if(!Yii::app()->eventRelay->isInstalled())
        {
            Yii::app()->raiseNote(Yii::t('SIMAEventRelay.Translation', 'EventRelayNotInstalled'));
        }
        else
        {
            $event_relay_port = Yii::app()->eventRelay->getLocalPort();
            $event_relay_host = Yii::app()->eventRelay->getHost();
            
            $client = new Client("ws://".$event_relay_host.":".$event_relay_port);
            
            foreach($messages_data as $message_data)
            {
                if(
                        !isset($message_data['receiver_id'])
                        || !isset($message_data['receiver_type'])
                        || !isset($message_data['message_type'])
                        || !isset($message_data['data'])
                )
                {
                    error_log(__METHOD__.' - '.Yii::t('SIMAEventRelay.Translation', 'InvalidMessageData', [
                        '{message_data}' => CJSON::encode($message_data)
                    ]));
                    continue;
                }
                
                $message = [
                    'sender_user_id' => $sender_user_id,
                    'sender_session_id' => $session_id,
                    'sender_type' => 'PHP',
                    'receiver_id' => $message_data['receiver_id'],
                    'receiver_type' => $message_data['receiver_type'],
                    'message_type' => $message_data['message_type'],
                    'data' => $message_data['data'],
                    'session_key' => $session_key
                ];

                $json_message = json_encode($message);
                
                try
                {
                    $client->send($json_message, 'text', true, Yii::app()->params['event_relay_client_max_reconnect_count']);
                }
                catch (Exception $ex)
                {
                    $message = __METHOD__.' - '.Yii::t('SIMAEventRelay.Translation', 'ExceptionSendingMessage', [
                        '{reason}' => $ex->getMessage(),
                        '{json_message}' => $json_message
                    ]);
                    
                    if(isset($_SESSION['WebSocketSendBulkFailure']) && $_SESSION['WebSocketSendBulkFailure'] === true)
                    {
                    }
                    else
                    {
                        error_log($message);
                        $_SESSION['WebSocketSendBulkFailure'] = true;
//                        Yii::app()->errorReport->createAutoClientBafRequest($message);
                    }
                }
            }
            
            unset($client);
        }
    }
}