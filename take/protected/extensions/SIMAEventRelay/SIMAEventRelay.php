<?php

class SIMAEventRelay
{
    public $host = null;
    public $port = null;
    public $local_port = null;
    public $logfile = null;
//    public $log_messages = false;
    public $without_eventrelay = false; /// parametar za nas programere da nam nije obavezan eventrelay pa da browser ni ne pokusava da se konektuje
    public $without_sda = false;
    public $client_connection_string = null;
    public $clients_aftersend_sleep_s = 0.1;
    public $clients_keepalive_check_s = 10;
    public $clients_keepalive_check_sleep_s = 30;
    public $message_queue_count_to_log = 20;
    
    private $installed = true;
    private $to_exit = false;
    
    public function init()
    {
        if(empty($this->host))
        {
            throw new Exception('empty host');
        }
        if(empty($this->port))
        {
            throw new Exception('empty port');
        }
        if(empty($this->logfile))
        {
            throw new Exception('empty logfile');
        }
        
        if(empty($this->local_port))
        {
            $this->local_port = $this->port;
        }
        
        if(empty($this->client_connection_string))
        {
            error_log(__METHOD__.' - empty client_connection_string');
        }
        
//        if(Yii::app()->params['is_ssl'] === true && (string)$this->local_port === (string)$this->port)
//        {
//            throw new Exception('must set local_port');
//        }
    }
    
    public function isInstalled()
    {
        return $this->installed;
    }
    
    /**
     * vraca true ako eventrelay radi, false ako ne radi
     * @return boolean
     */
    public function eventRelayStatus()
    {
        $pid = $this->getThisEventRelayRunningPID();
        if(!is_null($pid))
        {
            $result = null;
            $command = sprintf("ps %d", $pid);
            Yii::app()->linuxCommandExecutor->executeCommand($command, Yii::app()->params['linuxcommandExecutor_timeout_s_eventRelayStatus'], $result);
            
            if(!empty($result) && count($result) === 2 && SIMAMisc::stringStartsWith(trim($result[1]), $pid))
            {
                return true;
            }
        }
        return false;
    }
    
    /**
     * 
     * @return array
     */
    public function additionalStatusProblems()
    {
        $problems = [];
        
        if($this->eventRelayStatus() !== true)
        {
            $problems[] = 'EventRelay is not running';
        }
        
        $additionalStatusProblems_SendUpdateModelEvents = $this->additionalStatusProblems_SendUpdateModelEvents();
        if(!empty($additionalStatusProblems_SendUpdateModelEvents))
        {
            $problems = array_merge($problems, $additionalStatusProblems_SendUpdateModelEvents);
        }
        
        $additionalStatusProblems_timecheck = $this->additionalStatusProblems_timecheck();
        if(!empty($additionalStatusProblems_timecheck))
        {
            $problems = array_merge($problems, $additionalStatusProblems_timecheck);
        }
        
        return $problems;
    }
    
    public function stopEventRelay()
    {
        $this->to_exit = true;
        $this->startEventRelay_recreateConfFile();
        return true;
    }
    
    public function sendEventToUser($receiver_id, $message_type, $data)
    {
        $usertype = 'USER';
        
        $this->sendEvent($receiver_id, $usertype, $message_type, $data);
    }
    
    public function startEventRelay()
    {
        $result = false;
        
        if(!Yii::app()->eventRelay->isInstalled())
        {
            throw new SIMAWarnException(Yii::t('SIMAEventRelay.Translation', 'EventRelayNotInstalled'));
        }
        
        $this->to_exit = false;
        $this->startEventRelay_recreateConfFile();

        $eventrelay_run_command = $this->getEventRelayRunCommand();

        $formated_command = sprintf("%s >> %s 2>&1 &", $eventrelay_run_command, $this->logfile);

        $commandResult = null;
        $command_return_var = null;
        Yii::app()->linuxCommandExecutor->executeCommand($formated_command, Yii::app()->params['linuxcommandExecutor_timeout_s_startEventRelay'], $commandResult, $command_return_var);

        if(!empty($commandResult))
        {
            error_log(__METHOD__.' - $commandResult: '.$commandResult);
        }
        else
        {
            $result = true;
        }
        
        return $result;
    }
    
    /**
     * da li je uspesno docekano da eventrelay nije pokrenut
     * @return boolean/array[string] - true ako se uspoesno doceka zaustavljenost. niz poruka u suprotnom
     */
    public function stopWait()
    {
        $error_messages = [];
        
        $wait_max_counter = 10;
        $wait_counter = $wait_max_counter;
        while(1)
        {
            $status = Yii::app()->eventRelay->eventRelayStatus();
            $stop_additionalStatusProblems = Yii::app()->eventRelay->additionalStatusProblems();
            if(
                $status === false 
                && empty(array_diff([
                        'EventRelay is not running',
                        'EventRelaySendModelUpdates2 is not running'
                    ], $stop_additionalStatusProblems))
            )
            {
                $error_messages = []; /// uspesno zaustavljen, ponistavaju se sve dosadasnje evidentirane poruke
                break;
            }

            $error_messages[] = "still running - {$wait_counter}\n";

            $wait_counter--;
            if($wait_counter < 0)
            {
                $error_messages[] = "max wait exceeded\n";
                break;
            }

            sleep(1);
        }
        
        $result = true;
        if(!empty($error_messages))
        {
            $result = $error_messages;
        }
        return $result;
    }
    
    /**
     * da li je uspesno docekano da se eventrelay pokrene
     * @return boolean/array[string] - true ako se uspoesno pokrene sve. niz poruka u suprotnom
     */
    public function startWait()
    {
        $error_messages = [];
        
        $wait_max_counter = 10;
        $wait_counter = $wait_max_counter;
        while(1)
        {
            $status = Yii::app()->eventRelay->eventRelayStatus();
            $start_additionalStatusProblems = Yii::app()->eventRelay->additionalStatusProblems();
            if($status === true && empty($start_additionalStatusProblems))
            {
                $error_messages = []; /// uspesno startovan, ponistavaju se sve dosadasnje evidentirane poruke
                break;
            }

            $start_additionalStatusProblems_encoded = json_encode($start_additionalStatusProblems);
            $error_messages[] = "still waiting to start - {$wait_counter} - {$start_additionalStatusProblems_encoded}\n";

            $wait_counter--;
            if($wait_counter < 0)
            {
                $error_messages[] = "max wait exceeded\n";
                break;
            }

            sleep(1);
        }
        
        $result = true;
        if(!empty($error_messages))
        {
            $result = $error_messages;
        }
        return $result;
    }
    
    public function getHost()
    {
        return $this->host;
    }
    
    public function getPort()
    {
        return $this->port;
    }
    
    public function getLocalPort()
    {
        return $this->local_port;
    }
    
    public function getConnectonString()
    {
        if(empty($this->client_connection_string))
        {
            error_log(__METHOD__.' - empty client_connection_string');
        }
        return $this->client_connection_string;
    }
    
    private function sendEvent($receiver_id, $receiver_type, $message_type, $data)
    {
        WebSocketClient::send($receiver_id, $receiver_type, $message_type, $data);
    }
    
    private function startEventRelay_recreateConfFile()
    {        
        $basePath = str_replace('protected/extensions/SIMAEventRelay/SIMAEventRelay.php', '', __FILE__);
        $conf_file_path = $basePath.'/protected/extensions/SIMAEventRelay/bin/conf.php';
        
        $is_ssl = 'false';
        if(Yii::app()->params['is_ssl'] === true)
        {
            $is_ssl = 'true';
        }
        $log_messages = 'false';
//        if($this->log_messages === true)
        if(Yii::app()->configManager->get('base.eventrelay.log_messages') === true)
        {
            $log_messages = 'true';
        }
        
        $db_connectionString = Yii::app()->db->connectionString;
        $db_username = Yii::app()->db->username;
        $db_password = Yii::app()->db->password;
        $lock_files_location = Yii::app()->interprocessMutex->lock_files_location;
        $to_exit = 'false';
        if($this->to_exit === true)
        {
            $to_exit = 'true';
        }
        $log_to_db = 'true';
        if(Yii::app()->configManager->get('base.eventrelay.log_to_db') !== true)
        {
            $log_to_db = 'false';
        }
        $sma_usage = 'FALSE';
//        if(Yii::app()->configManager->get('base.develop.sma_events_over_eventrelay') === true)
//        {
//            $sma_usage = 'TRUE';
//        }
        $messages_to_send_number = Yii::app()->configManager->get('base.eventrelay.messages_to_send_number');
        $messagequeue_exact_count_to_log = Yii::app()->configManager->get('base.eventrelay.messagequeue_exact_count_to_log');
        $max_messagesending_exec_time_s = Yii::app()->configManager->get('base.eventrelay.max_messagesending_exec_time_s');
        $sendmessage_max_prev_start_time_diff_s = Yii::app()->configManager->get('base.eventrelay.sendmessage_max_prev_start_time_diff_s');
        $push_logs_to_db_s = Yii::app()->configManager->get('base.eventrelay.push_logs_to_db_s');
        
        $send_update_model_events_s = 5;
        $timecheck_s = 5;
        
        $conf_content = "<?php
return [
    'event_relay_port' => {$this->local_port},
    'clients_aftersend_sleep_s' => {$this->clients_aftersend_sleep_s},
    'is_ssl' => $is_ssl,
    'message_queue_count_to_log' => {$this->message_queue_count_to_log},
    'clients_keepalive_check_sleep_s' => {$this->clients_keepalive_check_sleep_s},
    'clients_keepalive_check_s' => {$this->clients_keepalive_check_s},
    'send_update_model_events_s' => {$send_update_model_events_s},
    'timecheck_s' => {$timecheck_s},
    'log_messages' => $log_messages,
    'lock_files_location' => '$lock_files_location',
    'db_params' => [
        'connectionString' => '{$db_connectionString}',
        'username' => '{$db_username}',
        'password' => '{$db_password}'
    ],
    'logfile' => '{$this->logfile}',
    'to_exit' => {$to_exit},
    'log_to_db' => {$log_to_db},
    'sma_usage' => {$sma_usage},
    'messages_to_send_number' => {$messages_to_send_number},
    'messagequeue_exact_count_to_log' => {$messagequeue_exact_count_to_log},
    'max_messagesending_exec_time_s' => {$max_messagesending_exec_time_s},
    'sendmessage_max_prev_start_time_diff_s' => {$sendmessage_max_prev_start_time_diff_s},
    'push_logs_to_db_s' => {$push_logs_to_db_s}
];";
        if(file_put_contents($conf_file_path, $conf_content) === false)
        {
            throw new Exception('file_put_contents fail');
        }
    }
    
    public function registerManual() 
    {
        $urlScript = Yii::app()->assetManager->publish(dirname(__FILE__).'/assets');

        Yii::app()->clientScript->registerScriptFile($urlScript .'/js/simaEventRelay.js', CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerScriptFile($urlScript .'/js/simaEventRelayAdmin.js', CClientScript::POS_HEAD);

        if(Yii::app()->isWebApplication())
        {
    //        $sima_hostname = $this->host;
            $sima_hostname = Yii::app()->request->serverName;
            $eventrelay_js_log_every_received_message = Yii::app()->configManager->get('base.eventrelay.js_log_every_received_message');
            $send_autobaf_on_disconnect_reconnecting = Yii::app()->configManager->get('base.eventrelay.js_send_autobaf_on_disconnect_reconnecting');
            $consolelog_keepalive_send = Yii::app()->configManager->get('base.eventrelay.js_consolelog_keepalive_send');

            $event_relay_params = [
                'user_id' => Yii::app()->user->id,
                'session_id' => Yii::app()->user->db_session->id,
                'event_relay_host' => $sima_hostname,
                'event_relay_port' => $this->port,
                'is_ssl' => Yii::app()->params['is_ssl'],
                'without_eventrelay' => $this->without_eventrelay,
                'event_relay_connection_string' => $this->client_connection_string,
                'log_every_received_message' => $eventrelay_js_log_every_received_message,
                'send_autobaf_on_disconnect_reconnecting' => $send_autobaf_on_disconnect_reconnecting,
                'consolelog_keepalive_send' => $consolelog_keepalive_send
            ];

            $event_relay_json_params = CJSON::encode($event_relay_params);

            Yii::app()->clientScript->registerScript(
                SIMAHtml::uniqid(),
                "sima.eventrelay = new SIMAEventRelay($event_relay_json_params);", 
                CClientScript::POS_END
            );
            Yii::app()->clientScript->registerScript(
                SIMAHtml::uniqid(),
                "sima.eventrelayAdmin = new SIMAEventRelayAdmin();", 
                CClientScript::POS_END
            );
        }
    }
    
    public function getThisEventRelayRunningPID()
    {
        $result = null;
        
        $command = $this->getEventRelayRunCommand();
        $command_result = null;
        $get_pid_command = sprintf("ps axww | grep '%s' | grep -v 'grep' | awk '{print $1}'", $command);
        Yii::app()->linuxCommandExecutor->executeCommand($get_pid_command, Yii::app()->params['linuxcommandExecutor_timeout_s_eventRelayStatus'], $command_result);
        if(!empty($command_result) && is_array($command_result))
        {
            $result = $command_result[0];
        }
        
        return $result;
    }
    
    public function getEventRelayRunCommand()
    {
        $basePath = str_replace('protected/extensions/SIMAEventRelay/SIMAEventRelay.php', '', __FILE__);
        $event_relay_file = $basePath.'/protected/extensions/SIMAEventRelay/bin/eventRelay.php';
        $command = 'php '.$event_relay_file;
        return $command;
    }
    
    private function getEventRelayCommandRunningPID($command)
    {
        $result = null;
        
        $command_result = null;
        $get_pid_command = sprintf("ps axww | grep '%s' | grep -v 'grep' | awk '{print $1}'", $command);
        Yii::app()->linuxCommandExecutor->executeCommand($get_pid_command, Yii::app()->params['linuxcommandExecutor_timeout_s_eventRelayStatus'], $command_result);
        if(!empty($command_result) && is_array($command_result))
        {
            $result = $command_result[0];
        }
        
        return $result;
    }
    
    private function getEventRelaySendModelUpdates2RunningPID()
    {
        $command = $this->getEventRelaySendModelUpdates2RunCommand();
        $result = $this->getEventRelayCommandRunningPID($command);
        return $result;
    }
    
    private function getEventRelayTimecheckRunningPID()
    {
        $command = $this->getEventRelayTimecheckRunCommand();
        $result = $this->getEventRelayCommandRunningPID($command);
        return $result;
    }
    
    private function getEventRelaySendModelUpdates2RunCommand()
    {
        $basePath = str_replace('protected/extensions/SIMAEventRelay/SIMAEventRelay.php', '', __FILE__);
        if(!SIMAMisc::stringEndsWith($basePath, '/'))
        {
            $basePath .= '/';
        }
        $event_relay_file = $basePath.'protected/extensions/SIMAEventRelay/bin/eventRelaySendModelUpdates2.php';
        $command = 'php '.$event_relay_file;
        return $command;
    }
    
    private function getEventRelayTimecheckRunCommand()
    {
        $basePath = str_replace('/protected/extensions/SIMAEventRelay/SIMAEventRelay.php', '', __FILE__);
        $event_relay_file = $basePath.'/console.php TimeCheckEventRelay';
        $command = 'php '.$event_relay_file;
        return $command;
    }
    
    private function additionalStatusProblems_SendUpdateModelEvents()
    {
        $problems = [];
        
        $lock_key = 'SIMAEventRelay_SendUpdateModelEvents';
        $is_locked = Yii::app()->interprocessMutex->isLocked($lock_key);

        $smu2_pid = $this->getEventRelaySendModelUpdates2RunningPID();
        if(is_null($smu2_pid))
        {
            $problems[] = 'EventRelaySendModelUpdates2 is not running';
            if($is_locked === true)
            {
                $problems[] = 'EventRelaySendModelUpdates2 lock exists';
            }
        }
        else
        {
            if($is_locked === false)
            {
                $problems[] = 'EventRelaySendModelUpdates2 lock not exists';
            }
            
            $command = $this->getEventRelaySendModelUpdates2RunCommand();
            $verifyOnlyOneInstanceOfCommand = $this->verifyOnlyOneInstanceOfCommand($command);
            if($verifyOnlyOneInstanceOfCommand !== true)
            {
                $problems[] = 'EventRelaySendModelUpdates2 - '.$verifyOnlyOneInstanceOfCommand;
            }
        }
        
        return $problems;
    }
    
    private function additionalStatusProblems_timecheck()
    {
        $problems = [];
        
        $lock_key = 'TimeCheckCommand';
        $is_locked = Yii::app()->interprocessMutex->isLocked($lock_key);

        $smu2_pid = $this->getEventRelayTimecheckRunningPID();
        if(is_null($smu2_pid))
        {
            $problems[] = 'TimeCheckEventRelay is not running';
            if($is_locked === true)
            {
                $problems[] = 'TimeCheckEventRelay lock exists';
            }
        }
        else
        {
            if($is_locked === false)
            {
                $problems[] = 'TimeCheckEventRelay lock not exists';
            }
            
            $command = $this->getEventRelayTimecheckRunCommand();
            $verifyOnlyOneInstanceOfCommand = $this->verifyOnlyOneInstanceOfCommand($command, true);
            if($verifyOnlyOneInstanceOfCommand !== true)
            {
                $problems[] = 'TimeCheckEventRelay - '.$verifyOnlyOneInstanceOfCommand;
            }
        }
        
        return $problems;
    }
    
    /**
     * 
     * @param type $command
     * @param type $allowed_to_have_more_than_one_if_subprocess - TimeCheckEventRelay moze imati vise jer kreira podprocese
     * @return bool/string - ako je sve u redu true, inace string poruka o problemu
     */
    private function verifyOnlyOneInstanceOfCommand($command, $allowed_to_have_more_than_one_if_subprocess=false)
    {
        $result = true;
        
        $command_result = null;
        $get_processes_command = sprintf("ps wwxao pid,ppid,command | grep '%s' | grep -v 'grep'", $command);
        Yii::app()->linuxCommandExecutor->executeCommand($get_processes_command, Yii::app()->params['linuxcommandExecutor_timeout_s_eventRelayStatus'], $command_result);
        $command_result_count = count($command_result);
        if($command_result_count !== 1)
        {
            $error_message = 'number of processes not 1: '.json_encode($command_result_count).' - '.json_encode($command_result);
            if($allowed_to_have_more_than_one_if_subprocess === true && $command_result_count > 1)
            {
                /**
                 * mora sa dve for petlje, jer se kao rezultat dobijaju sortirani po pid-u
                 * a ako main pid bude veliki, blizu max, deca ce imati manji pid
                 * pa se ne moze smatrati da je prvi u nizu i main pid
                 */
                $number_of_processes_without_parent_process = 0;
                for($i=0; $i<$command_result_count; $i++)
                {
                    $command_res = str_replace('  ', ' ', trim($command_result[$i]));
                    $command_res_exploded = explode(' ', $command_res);
                    $command_pid = $command_res_exploded[0];
                    $command_ppid = $command_res_exploded[1];
                           
                    $found_ppid = false;
                    for($j=0; $j<$command_result_count; $j++)
                    {
                        $second_command_res = str_replace('  ', ' ', trim($command_result[$j]));
                        $second_command_res_exploded = explode(' ', $second_command_res);
                        $second_command_pid = $second_command_res_exploded[0];
                        $second_command_ppid = $second_command_res_exploded[1];
                        
                        if($second_command_pid === $command_pid)
                        {
                            continue;
                        }
                        
                        if($command_ppid === $second_command_pid)
                        {
                            $found_ppid = true;
                            break;
                        }
                    }
                    if($found_ppid === false)
                    {
                        $number_of_processes_without_parent_process++;
                    }
                }
                if($number_of_processes_without_parent_process > 1)
                {
                    $error_message = 'number of processes not 1: '.json_encode($number_of_processes_without_parent_process).' - '.json_encode($command_result);
                    $result = $error_message;
                }
            }
            else
            {
                $result = $error_message;
            }
        }
        
        return $result;
    }
}