<?php

namespace EventRelay;
use Ratchet\ConnectionInterface;

class ClientConnection
{
    public static $CLIENT_TYPES = [
        'DESKTOP_APP' => 'DESKTOP_APP',
        'MOBILE_APP' => 'MOBILE_APP',
        'JS' => 'JS',
        'PHP' => 'PHP',
        'DB' => 'DB',
    ];
    
    private $event_relay_id = null;
    private $ip_address = null;
    private $user_id = null;
    private $session_id = null;
    private $conn = null;
    private $client_type = null;
    private $session_key = null;
    private $authenticated = false;
    private $last_keepalive_sent_times = [];
    private $last_keepalive_received_times = [];
    private $_max_number_of_keepalive_times = 5;
    private $is_disconnected_by_server_timeout = false;
    
    public function __construct(ConnectionInterface $conn, $is_ssl=false)
    {
        $this->conn = $conn;
        $this->event_relay_id = $this->conn->resourceId;
        if($is_ssl === true)
        {
            $ip_address = $conn->WebSocket->request->getHeader('X-Forwarded-For');
            if(empty($ip_address))
            {
                /// ako je instaliran https://github.com/leopucci/stunnel
                $ip_address= $conn->WebSocket->request->getHeader('IP_Stunnel_Patch_Pucci', true);
                if(empty($ip_address))
                {
                    $ip_address = $this->conn->remoteAddress;
                }
            }
            $this->ip_address = (string)$ip_address;
        }
        else
        {
            $this->ip_address = (string)$this->conn->remoteAddress;
        }
    }
    
    public function getEventRelayId()
    {
        return $this->event_relay_id;
    }
    
    public function getIpAddress()
    {
        return $this->ip_address;
    }
    
    public function getUserId()
    {
        return $this->user_id;
    }
    
    public function getType()
    {
        return $this->client_type;
    }
    
    public function getSessionKey()
    {
        return $this->session_key;
    }
    
    public function getLastKeepAliveReceivedTime()
    {
        $last_keepalive_received_times_length = count($this->last_keepalive_received_times);
        
        if($last_keepalive_received_times_length === 0)
        {
            return null;
        }
        
        $last_keepalive_received_time_index = $last_keepalive_received_times_length-1;
        $last_keepalive_received_time = $this->last_keepalive_received_times[$last_keepalive_received_time_index];
        return $last_keepalive_received_time;
    }
    
    public function getLastKeepAliveReceivedTimes()
    {
        return $this->last_keepalive_received_times;
    }
    
    public function getLastKeepAliveSentTimes()
    {
        return $this->last_keepalive_sent_times;
    }
    
    public function getIsDisconnectedByServerTimeout()
    {
        return $this->is_disconnected_by_server_timeout;
    }
    
    public function equalEventRelayId($event_relay_id)
    {
        return $this->event_relay_id == $event_relay_id;
    }

    public function equalSessionId($session_id)
    {
        return $this->session_id == $session_id;
    }

    public function equalUserId($user_id)
    {
        return $this->user_id == $user_id;
    }

    public function equalType($client_type)
    {
        return $this->client_type == $client_type;
    }
    
    public function equalSessionKey($session_key)
    {
        return $this->session_key == $session_key;
    }
    
    public function sendMessage(Message $msg)
    {
        $this->conn->send($msg->toString());
    }
    
    public function sendEventRelayId()
    {
        $message = json_encode([
            'message_type' => MessageHandler::$MESSAGE_TYPE_EVENT_RELAY_ID,
            'data' => $this->event_relay_id
        ]);
        
        $this->conn->send($message);
    }
    
    public function isAuthenticated()
    {
        $result = false;
        
        if(
            $this->authenticated === true
            && isset($this->user_id)
            && isset($this->session_id)
            && isset($this->client_type)
            && isset($this->session_key)
        )
        {
            $result = true;
        }
        
        return $result;
    }
    
    public function Authenticate($user_id, $session_id, $client_type, $session_key)
    {
        if(empty($user_id))
        {
            throw new \Exception('$user_id not set');
        }
        if(!is_numeric($user_id))
        {
            throw new \Exception('$user_id not numeric: '.$user_id);
        }
        if(empty($session_id))
        {
            throw new \Exception('$session_id not set');
        }
        if(!is_numeric($session_id))
        {
            throw new \Exception('$session_id not numeric: '.$session_id);
        }
        if(empty($client_type))
        {
            throw new \Exception('$client_type not set');
        }
        if(empty($session_key))
        {
            throw new \Exception(__METHOD__.' - $session_key not set');
        }
        
        $this->user_id = intval($user_id);
        $this->session_id = intval($session_id);
        $this->client_type = $client_type;
        $this->session_key = $session_key;
        $this->authenticated = true;
        $this->updateKeepAliveReceivedTime();
    }
    
    public function updateKeepAliveReceivedTime()
    {
        $this->last_keepalive_received_times[] = time();
        if(count($this->last_keepalive_received_times) > $this->_max_number_of_keepalive_times)
        {
            array_shift($this->last_keepalive_received_times);
        }
    }
    
    public function updateKeepAliveSentTime()
    {
        $this->last_keepalive_sent_times[] = time();
        if(count($this->last_keepalive_sent_times) > $this->_max_number_of_keepalive_times)
        {
            array_shift($this->last_keepalive_sent_times);
        }
    }
    
    public function closeConn()
    {
        $this->is_disconnected_by_server_timeout = true;
        $this->conn->close();
    }
    
    public function toString()
    {
        $array_params = [];
        
        foreach($this as $key => $value)
        {
            if(isset($value))
            {
                $array_params[$key] = $value;
            }
        }
        
        $string_result = json_encode($array_params);
        
        return $string_result;
    }
}

