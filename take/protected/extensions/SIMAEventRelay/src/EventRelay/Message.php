<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace EventRelay;

require_once __DIR__.'/../../SIMAEventRelayTraits.php';

class Message
{
    use \ReceiverTypes;
    
    public static $MESSAGE_TYPES = [
        'CLIENT_AUTHENTICATION_REQUEST' => 'CLIENT_AUTHENTICATION_REQUEST',
        'CLIENT_AUTHENTICATION_RESPONSE' => 'CLIENT_AUTHENTICATION_RESPONSE',
        'CONNECTED_DESKTOP_APPLICATIONS_REQUEST' => 'CONNECTED_DESKTOP_APPLICATIONS_REQUEST',
        'CONNECTED_DESKTOP_APPLICATIONS_RESPONSE' => 'CONNECTED_DESKTOP_APPLICATIONS_RESPONSE',
        'DESKTOP_APP_DATA_REQUEST' => 'DESKTOP_APP_DATA_REQUEST',
        'DESKTOP_APP_DATA_RESPONSE' => 'DESKTOP_APP_DATA_RESPONSE',
        'CLIENT_AUTHENTICATED' => 'CLIENT_AUTHENTICATED',
        'JS_CLIENT_AUTHENTICATED' => 'JS_CLIENT_AUTHENTICATED',
        'JS_CLIENT_DISCONNECTED' => 'JS_CLIENT_DISCONNECTED',
        'DESKTOP_APP_AUTHENTICATED' => 'DESKTOP_APP_AUTHENTICATED',
        'CLIENT_DISCONNECTED' => 'CLIENT_DISCONNECTED',
        'DESKTOP_APP_CHANGE_FILE_SAVE_LOCATION_REQUEST' => 'DESKTOP_APP_CHANGE_FILE_SAVE_LOCATION_REQUEST',
        'DESKTOP_APP_CHANGE_FILE_SAVE_LOCATION_RESPONSE' => 'DESKTOP_APP_CHANGE_FILE_SAVE_LOCATION_RESPONSE',
        'DESKTOP_APP_CHANGE_PSEUDONIM_REQUEST' => 'DESKTOP_APP_CHANGE_PSEUDONIM_REQUEST',
        'DESKTOP_APP_CHANGED_PSEUDONIM' => 'DESKTOP_APP_CHANGED_PSEUDONIM',
        'DESKTOP_APP_CHANGE_CUSTOM_MESSAGES_THREAD_SLEEP_REQUEST' => 'DESKTOP_APP_CHANGE_CUSTOM_MESSAGES_THREAD_SLEEP_REQUEST',
        'DESKTOP_APP_CHANGED_CUSTOM_MESSAGES_THREAD_SLEEP' => 'DESKTOP_APP_CHANGED_CUSTOM_MESSAGES_THREAD_SLEEP',
        'DESKTOP_APP_CHANGE_MAX_PACKET_SIZE_KB_REQUEST' => 'DESKTOP_APP_CHANGE_MAX_PACKET_SIZE_KB_REQUEST',
        'DESKTOP_APP_CHANGED_MAX_PACKET_SIZE_KB' => 'DESKTOP_APP_CHANGED_MAX_PACKET_SIZE_KB',
        'DESKTOP_APP_FILE_TRANSFER_UPLOAD_START' => 'DESKTOP_APP_FILE_TRANSFER_UPLOAD_START',
        'DESKTOP_APP_FILE_TRANSFER_UPLOAD_PROGRESS' => 'DESKTOP_APP_FILE_TRANSFER_UPLOAD_PROGRESS',
        'DESKTOP_APP_FILE_TRANSFER_UPLOAD_FINISH' => 'DESKTOP_APP_FILE_TRANSFER_UPLOAD_FINISH',        
        'CHAT_UPDATE_USER_STATUS' => 'CHAT_UPDATE_USER_STATUS',
        'CHAT_UPDATE_USER_SEEN_TIME'=>'CHAT_UPDATE_USER_SEEN_TIME',
        'COMMENT_THREAD_NEW_MESSAGE'=>'COMMENT_THREAD_NEW_MESSAGE',        
        'COMMENT_THREAD_MESSAGE_NEW' => 'COMMENT_THREAD_MESSAGE_NEW',
        'COMMENT_THREAD_MESSAGE_READ' => 'COMMENT_THREAD_MESSAGE_READ',
        'COMMENT_THREAD_MESSAGE_READ_BYSELF' => 'COMMENT_THREAD_MESSAGE_READ_BYSELF',
        'COMMENT_THREAD_MESSAGE_MARKED' => 'COMMENT_THREAD_MESSAGE_MARKED',
        'COMMENT_THREAD_DRAFT_CHANGED' => 'COMMENT_THREAD_DRAFT_CHANGED',
        'DESKTOP_APP_CHANGE_AUTO_LOGIN_REQUEST' => 'DESKTOP_APP_CHANGE_AUTO_LOGIN_REQUEST',
        'DESKTOP_APP_CHANGED_AUTO_LOGIN' => 'DESKTOP_APP_CHANGED_AUTO_LOGIN',
        'DESKTOP_APP_FILE_TRANSFER_DOWNLOAD_START' => 'DESKTOP_APP_FILE_TRANSFER_DOWNLOAD_START',
        'DESKTOP_APP_FILE_TRANSFER_DOWNLOAD_FINISH' => 'DESKTOP_APP_FILE_TRANSFER_DOWNLOAD_FINISH',
        'DESKTOP_APP_FILE_TRANSFER_DOWNLOAD_PROGRESS' => 'DESKTOP_APP_FILE_TRANSFER_DOWNLOAD_PROGRESS',
        'DESKTOP_APP_REQUIRE_FILE_DOWNLOAD' => 'DESKTOP_APP_REQUIRE_FILE_DOWNLOAD',
        'DESKTOP_APP_FILE_OPEN' => 'DESKTOP_APP_FILE_OPEN',
        'DESKTOP_APP_FILE_MODIFIED' => 'DESKTOP_APP_FILE_MODIFIED',
        'DESKTOP_APP_FILE_TRANSFER_DOWNLOAD_ERROR' => 'DESKTOP_APP_FILE_TRANSFER_DOWNLOAD_ERROR',
        'DESKTOP_APP_REQUIRE_FILE_REDOWNLOAD' => 'DESKTOP_APP_REQUIRE_FILE_REDOWNLOAD',
        'DESKTOP_APP_REMOVED_FROM_OPEN' => 'DESKTOP_APP_REMOVED_FROM_OPEN',
        'DESKTOP_APP_REMOVED_FROM_MODIFIED' => 'DESKTOP_APP_REMOVED_FROM_MODIFIED',
        'DESKTOP_APP_CREATE_NEW_FILE_VERSIONS' => 'DESKTOP_APP_CREATE_NEW_FILE_VERSIONS',
        'NEW_UNREAD_NOTIF' => 'NEW_UNREAD_NOTIF',
        'NOTIF_READ' => 'NOTIF_READ',
        'DESKTOP_APP_SHOW_MESSAGE' => 'DESKTOP_APP_SHOW_MESSAGE',
        'DESKTOP_APP_UNMODIFY_FILE' => 'DESKTOP_APP_UNMODIFY_FILE',
        'WEB_CLIENT_ACTIVE_GLOBAL_MESSAGE' => 'WEB_CLIENT_ACTIVE_GLOBAL_MESSAGE'
    ];
    
    private $sender_event_relay_id = null;
    private $sender_user_id = null;
    private $sender_session_id = null;
    private $sender_type = null;
    private $receiver_id = null;
    private $receiver_type = null;
    private $message_type = null;
    private $session_key = null;
    private $data = null;
    
    private $event_relay_resource_id = null;
    
    public function __construct(array $message_array, $event_relay_resource_id, $with_validation)
    {
        if($with_validation === true)
        {
            $this->validate($message_array);
        }
        
        $this->event_relay_resource_id = $event_relay_resource_id;
                        
        foreach($message_array as $key => $value)
        {
            if(isset($value))
            {
                if($key === 'receiver_id')
                {
                    if(!empty($value) && !is_array($value))
                    {
                        $value = [$value];
                    }
                }
                $this->$key = $value;
            }
        }
        if(is_null($this->session_key))
        {
            $this->session_key = 'sima';
        }
    }
    
    public static function FromArray(array $parameters, $with_validation=true)
    {        
        $instance = new self($parameters, null, $with_validation);
        return $instance;
    }
    
    public static function FromString($parameters, $event_relay_resource_id, $with_validation=true)
    {
        if(!is_string($parameters))
        {
            throw new \Exception(__METHOD__.' - ParametersNotString');
        }
        
        $parameters_decoded = json_decode($parameters, true);
        
        $instance = new self($parameters_decoded, $event_relay_resource_id, $with_validation);
        return $instance;
    }
    
    public function HaveReceiver()
    {
        return !empty($this->receiver_id) && !empty($this->receiver_type);
    }
    
    public function getEventRelayResourceId()
    {
        return $this->event_relay_resource_id;
    }
    
    public function getSenderUserId()
    {
        return $this->sender_user_id;
    }
    public function getSenderSessionId()
    {
        return $this->sender_session_id;
    }
    public function getSenderType()
    {
        return $this->sender_type;
    }
    public function getReceiverIds()
    {
        return $this->receiver_id;
    }
    public function getReceiverType()
    {
        return $this->receiver_type;
    }
    public function getMessageType()
    {
        return $this->message_type;
    }
    public function getData()
    {
        return $this->data;
    }
    public function getSessionKey()
    {
        return $this->session_key;
    }
    
    private function validate(array $message_array)
    {
        try
        {
            $msg_decoded = $message_array;

            if(!isset($msg_decoded['sender_type']))
            {
                throw new \Exception("Message invalid format - not set sender_type");
            }
            $sender_type = $msg_decoded['sender_type'];
            if(!isset($msg_decoded['receiver_id']))
            {
                throw new \Exception("Message invalid format - not set receiver_id");
            }
            $receiver_id = $msg_decoded['receiver_id'];
            if(!isset($msg_decoded['receiver_type']))
            {
                throw new \Exception("Message invalid format - not set receiver_type");
            }
            $receiver_type = $msg_decoded['receiver_type'];
            if(!isset($msg_decoded['data']))
            {
                throw new \Exception("Message invalid format - not set data");
            }
            if(empty($msg_decoded['message_type']))
            {
                throw new \Exception("Message invalid format - empty message_type");
            }
            $message_type = $msg_decoded['message_type'];

            if(!empty($msg_decoded->sender_event_relay_id) && !is_numeric($msg_decoded->sender_event_relay_id))
            {
                throw new \Exception("Message sender_event_relay_id invalid format");
            }

            if(!empty($msg_decoded->sender_user_id) && !is_numeric($msg_decoded->sender_user_id))
            {
                throw new \Exception("Message sender_user_id invalid format");
            }

            if(!empty($msg_decoded->sender_session_id) && !is_numeric($msg_decoded->sender_session_id) && !is_string($msg_decoded->sender_session_id))
            {
                throw new \Exception("Message session_id invalid format");
            }

            if(!is_string($sender_type)
                || !in_array($sender_type, ClientConnection::$CLIENT_TYPES))
            {
                throw new \Exception("Message message_type invalid format");
            }

            if(!empty($receiver_id))
            {
                $receiver_ids = $receiver_id;
                if(!is_array($receiver_ids))
                {
                    $receiver_ids = [$receiver_ids];
                }
                foreach($receiver_ids as $receiver_id)
                {
                    if(!is_numeric($receiver_id) && !is_string($receiver_id))
                    {
                        throw new \Exception("Message receiver_id invalid format");
                    }
                }
            }

            if(!empty($receiver_type) 
                && (!is_string($receiver_type)
                        || 
                        (
                            $receiver_type !== Message::$RECEIVER_TYPE_ALL
                            && $receiver_type !== Message::$RECEIVER_TYPE_EVENT_RELAY
                            && $receiver_type !== Message::$RECEIVER_TYPE_SESSION
                            && $receiver_type !== Message::$RECEIVER_TYPE_USER
                        )
                    )
            )
            {
                throw new \Exception("Message receiver_type invalid format");
            }

            if(
                !is_string($message_type)
    //            || 
    //            !in_array($message_type, Message::$MESSAGE_TYPES)
            )
            {
                throw new \Exception("Message message_type invalid format");
            }
        }
        catch(\Exception $e)
        {
            $message = $e->getMessage().': '.json_encode($message_array);
            throw new \Exception($message);
        }
    }
    
    public function toString()
    {
        $array_params = [];
        
        foreach($this as $key => $value)
        {
            if(isset($value))
            {
                $array_params[$key] = $value;
            }
        }
        
        $string_result = json_encode($array_params);
        
        return $string_result;
    }
}