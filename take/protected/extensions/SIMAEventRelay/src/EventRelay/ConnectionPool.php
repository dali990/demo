<?php

namespace EventRelay;

use EventRelay\ClientConnection;
use Ratchet\ConnectionInterface;


class ConnectionPool
{
    private $clientConnections = [];
    private $is_ssl = false;
    
    public function __construct($is_ssl=false) 
    {
        $this->is_ssl = $is_ssl;
    }
    
    public function attachConnection(ConnectionInterface $conn)
    {
        $newConnection = new ClientConnection($conn, $this->is_ssl);
        
        $this->clientConnections[] = $newConnection;
        
        return $newConnection;
    }
    
    public function detachConnection(ConnectionInterface $conn)
    {
        $event_relay_id = $conn->resourceId;
        
        foreach($this->clientConnections as $key => $value)
        {
            if($value->equalEventRelayId($event_relay_id) === true)
            {
                unset($this->clientConnections[$key]);
                break;
            }
        }
    }
    
    public function getAllConnections($session_key)
    {
        $result = [];
        
        $clients = $this->clientConnections;
        foreach($clients as $client)
        {
            if($client->equalSessionKey($session_key))
            {
                $result[] = $client;
            }
        }
        
        return $result;
    }
    
    public function getConnectionsCount()
    {
        return count($this->clientConnections);
    }
    
    public function findClients(array $client_ids, $client_id_type, $session_key, $connection_type=null)
    {
        $result = [];
                
        if(empty($client_ids) || empty($client_id_type))
        {
            throw new \Exception('$client_ids and/or $client_id_type empty');
        }
        
        $clients = $this->clientConnections;
        foreach($clients as $client)
        {
            if($client->isAuthenticated() && $client->equalSessionKey($session_key) && (empty($connection_type) || $client->equalType($connection_type)))
            {
                foreach($client_ids as $client_id)
                {
                    switch($client_id_type)
                    {
                        case Message::$RECEIVER_TYPE_EVENT_RELAY:
                            if($client->equalEventRelayId($client_id))
                            {
                                $result[] = $client;
                            }
                            break;
                        case Message::$RECEIVER_TYPE_SESSION:
                            if($client->equalSessionId($client_id))
                            {
                                $result[] = $client;
                            }
                            break;
                        case Message::$RECEIVER_TYPE_USER:
                            if($client->equalUserId($client_id))
                            {
                                $result[] = $client;
                            }
                            break;
                        default:
                            throw new \Exception('Unknown receiver type: '.json_encode($connection_type));
                    }
                }
            }
        }
        
        return $result;
    }
    
    public function findByEventRelayId($eventRelayId)
    {
        $result = null;
        
        $clients = $this->clientConnections;
        foreach($clients as $client)
        {
            if($client->equalEventRelayId($eventRelayId))
            {
                $result = $client;
                break;
            }
        }
        
        return $result;
    }
    
    public function closeClientsWithOlderKeepAliveTime($limit_time)
    {
        $clients = $this->clientConnections;
        foreach($clients as $client)
        {
            $keepalive_time = $client->getLastKeepAliveReceivedTime();
            if(is_null($keepalive_time))
            {
                continue;
            }
            if($client->isAuthenticated() && $keepalive_time < $limit_time)
            {
                $client->closeConn();
            }
        }
    }
    
    public function sendMessageToAllClients(Message $message, $update_keepalive=false)
    {
        $clients = $this->clientConnections;
        foreach($clients as $client)
        {
            if($client->isAuthenticated())
            {
                $client->sendMessage($message);
                if($update_keepalive === true)
                {
                    $client->updateKeepAliveSentTime();
                }
            }
        }
    }
}