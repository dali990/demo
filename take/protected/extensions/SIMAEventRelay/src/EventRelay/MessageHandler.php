<?php

/**
 * Message format:
 * {
 *  sender_event_relay_id,
 *  sender_user_id,
 *  sender_session_id,
 *  sender_type,
 *  receiver_id,
 *  receiver_type,
 *  message_type,
 *  data
 * }
 */

namespace EventRelay;
use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;

class MessageHandler implements MessageComponentInterface 
{
    protected $connectionPool = null;
    protected $is_ssl = false;
    private $_message_queue = null;
    private $_message_queue_count_to_log = null;
    private $_clients_keepalive_check_s = null;
    private $_messages_to_send_number = null;
    private $_messagequeue_exact_count_to_log = null;
    private $_max_messagesending_exec_time_s = null;
    private $_sendmessage_max_prev_start_time_diff_s = null;
    private $_clients_keepalive_check_sleep_s = null;
    private $_log_to_db = true;
    private $_log_messages = false;
//    private $_dbh = null;
    private $_messagequeue_exact_count_to_log_logged = false;
    private $_simaSendMessage_prev_start_time = null;
    private $_sendMessageCounter = 0;
    private $_keepAliveMessage = null;
//    private $_db_connection_string = null;
//    private $_db_username = null;
//    private $_db_password = null;
    private $_logs = [];
    
    public function __construct(MessageHandlerContructParams $messageHandlerContructParams)
    {
        $messageHandlerContructParams->validate();
//        $db_params = $messageHandlerContructParams->db_params;
        $message_queue_count_to_log = $messageHandlerContructParams->message_queue_count_to_log;
        $clients_keepalive_check_s = $messageHandlerContructParams->clients_keepalive_check_s;
        $messages_to_send_number = $messageHandlerContructParams->messages_to_send_number;
        $messagequeue_exact_count_to_log = $messageHandlerContructParams->messagequeue_exact_count_to_log;
        $max_messagesending_exec_time_s = $messageHandlerContructParams->max_messagesending_exec_time_s;
        $sendmessage_max_prev_start_time_diff_s = $messageHandlerContructParams->sendmessage_max_prev_start_time_diff_s;
        $clients_keepalive_check_sleep_s = $messageHandlerContructParams->clients_keepalive_check_sleep_s;
        
//        $this->_dbh = new \PDO($db_params['connectionString'], $db_params['username'], $db_params['password']);
//        $this->_db_connection_string = $db_params['connectionString'];
//        $this->_db_username = $db_params['username'];
//        $this->_db_password = $db_params['password'];
        
        $this->_message_queue = new \Ds\Queue();
        $this->_message_queue_count_to_log = (int)$message_queue_count_to_log;
        $this->_clients_keepalive_check_s = (int)$clients_keepalive_check_s;
        $this->_messages_to_send_number = (int)$messages_to_send_number;
        $this->_messagequeue_exact_count_to_log = (int)$messagequeue_exact_count_to_log;
        $this->_clients_keepalive_check_sleep_s = (int)$clients_keepalive_check_sleep_s;
        $this->_max_messagesending_exec_time_s = (float)$max_messagesending_exec_time_s;
        $this->_sendmessage_max_prev_start_time_diff_s = (float)$sendmessage_max_prev_start_time_diff_s;
        $this->_log_to_db = $messageHandlerContructParams->log_to_db;
        $this->_log_messages = $messageHandlerContructParams->log_messages;
        $this->is_ssl = $messageHandlerContructParams->is_ssl;
        $this->connectionPool = new ConnectionPool($this->is_ssl);
        
        $this->_keepAliveMessage = Message::FromArray([
            'message_type' => 'SERVER_KEEP_ALIVE'
        ], false);
        
        $datetme = new \DateTime();
        echo $datetme->format('Y-m-d H:i:s.u')." - eventrelay server started\n";
        $this->Log('eventrelay server started');
    }

    public function onOpen(ConnectionInterface $conn) 
    {
        $newConnection = $this->connectionPool->attachConnection($conn);
        
        $this->Log("New connection! ({$newConnection->getEventRelayId()})({$newConnection->getIpAddress()})({$this->connectionPool->getConnectionsCount()})");
    }

    public function onMessage(ConnectionInterface $from, $msg) 
    {
        try
        {
            $message = Message::FromString($msg, $from->resourceId);
            
            if($message->HaveReceiver() === true)
            {
                $this->_message_queue->push($message);
            }
            else if($message->getReceiverType() === Message::$RECEIVER_TYPE_ALL)
            {
                $this->onMessageToAllClientsGlobal($from->resourceId, $message);
            }
            else
            {
                $message_type = $message->getMessageType();
                
                switch($message_type)
                {
                    case 'CLIENT_AUTHENTICATION_REQUEST':
                        $this->onMessageClientAuthenticationRequest($from->resourceId, $message);
                        break;
                    case 'CLIENT_KEEP_ALIVE':
                        $this->onMessageClientKeepAlive($from->resourceId, $message);
                        break;
                    case 'CONNECTED_DESKTOP_APPLICATIONS_REQUEST':
                        $this->onMessageConnectedDesktopApplicationsRequest($from->resourceId, $message);
                        break;
                    case 'WEB_CLIENT_ACTIVITY_GLOBAL_MESSAGE':
                        $this->onMessageWebClientActivityGlobal($from->resourceId, $message);
                        break;
                    case 'EVENT_RELAY_SERVER_STATS':
                        $this->onMessageEventRelayServerStats($from->resourceId, $message);
                        break;
                    case 'EVENT_RELAY_SERVER_CHECK_CONNECTED_CLIENTS':
                        $this->checkConnectedClients($from->resourceId, $message->getSessionKey());
                        break;
                    default:
                        throw new \Exception("Unhandled message type: ".json_encode($message_type)."\nMessage: ".json_encode($msg));
                }
            }
            
            /// diskonektuj php klienta
            if($message->getSenderType() === ClientConnection::$CLIENT_TYPES['PHP'])
            {
                $this->connectionPool->detachConnection($from);
            }
            
            $this->LogMessage($message);
        }
        catch (\Exception $e) 
        {
            error_log(__METHOD__.' - '.date('d.m.Y. H:i:s').' - ERROR: '.$e->getMessage());
            $from->send($e->getMessage());
            
            $from->close();
        }
    }

    public function onClose(ConnectionInterface $conn) 
    {        
        $clientConnection = $this->connectionPool->findByEventRelayId($conn->resourceId);
        
        $client_ip_address = '';
        $client_connection_type = '';
        $clientConnectionInformations = '';
        if(!is_null($clientConnection))
        {
            $client_ip_address = $clientConnection->getIpAddress();
            $client_connection_type = $clientConnection->getType();
            $clientConnectionInformations = $this->clientConnectionInformations($clientConnection);
        }
        
        $this->onClientDisconnect($clientConnection);
        
        $this->connectionPool->detachConnection($conn);

        $log_message = "Connection {$conn->resourceId} has disconnected"
                        ." (IP:{$client_ip_address})(TYPE:$client_connection_type)({$this->connectionPool->getConnectionsCount()})";
                        
        if(!empty($clientConnectionInformations))
        {
            $log_message .= ' - $clientConnectionInformations: '.$clientConnectionInformations;
        }
        $this->Log($log_message);
    }

    public function onError(ConnectionInterface $conn, \Exception $e) 
    {        
        $clientConnection = $this->connectionPool->findByEventRelayId($conn->resourceId);
        
        if(empty($clientConnection))
        {
            $this->Log("clientConnection is empty");
        }
        else if($clientConnection->isAuthenticated())
        {
            $this->onClientDisconnect($clientConnection);
        }
        
        $this->Log("An error has occurred: {$e->getMessage()} ({$this->connectionPool->getConnectionsCount()})");

        $conn->close();
    }
    
    public function simaSendMessage()
    {
        $start_time = microtime(true);
        
        $this->_sendMessageCounter += 1;
        
        if(!is_null($this->_simaSendMessage_prev_start_time))
        {
            $prev_start_time_diff = $start_time - $this->_simaSendMessage_prev_start_time;
            if($prev_start_time_diff > $this->_sendmessage_max_prev_start_time_diff_s)
            {
                $this->Log($this->_sendMessageCounter.' - sendmessage_max_prev_start_time_diff - $prev_start_time_diff: '.$prev_start_time_diff, 'WARN');
            }
        }
        $this->_simaSendMessage_prev_start_time = $start_time;
        $sendmessage_max_prev_start_time_diff_log_time = microtime(true) - $start_time;
        
        $client_connection_count = $this->connectionPool->getConnectionsCount();
        $number_of_loops = 0;
        $number_of_sent_messages = 0;
        $number_of_sent_messages_clients = 0;
        $sending_data = '';
        $last_message_string = '';
        $find_clients_times = '';
        for($i = 0; $i < $this->_messages_to_send_number; $i++)
        {
            $number_of_loops++;
            
            $message_queue_count = $this->_message_queue->count();

            if($message_queue_count > $this->_message_queue_count_to_log)
            {
                $this->Log($this->_sendMessageCounter.' - message queue count: '.$message_queue_count, 'WARN');
            }
            if($this->_messagequeue_exact_count_to_log_logged === false && $message_queue_count === $this->_messagequeue_exact_count_to_log)
            {
                $messagequeue_exact_count_to_log_message = $this->_sendMessageCounter.' - message queue count exact: '.$message_queue_count;
                
                $queue_array = $this->_message_queue->toArray();
                foreach($queue_array as $queue_array_element)
                {
                    $messagequeue_exact_count_to_log_message .= $queue_array_element->toString();
                }
                
                $this->Log($messagequeue_exact_count_to_log_message, 'WARN');
                
                $this->_messagequeue_exact_count_to_log_logged = true;
            }

            if($message_queue_count === 0)
            {
                break;
            }
            
            $number_of_sent_messages++;
            
            $message = $this->_message_queue->pop();
            $last_message_string = $message->toString();
            
            if($message->HaveReceiver() === true)
            {
                $findClients_start_time = microtime(true);
                $clients = $this->connectionPool->findClients(
                        $message->getReceiverIds(), 
                        $message->getReceiverType(),
                        $message->getSessionKey(),
                        null
                );
                $findClients_end_time = microtime(true);
                $findClients_elapsed_time = $findClients_end_time - $findClients_start_time;
                $find_clients_times .= '|'.$findClients_elapsed_time.'|';

                foreach($clients as $client)
                {
                    /// da ne bi slali sami sebi
                    if($client->getEventRelayId() !== $message->getEventRelayResourceId())
                    {
                        $number_of_sent_messages_clients++;
                        
                        $sending_start_time = microtime(true);
                        $client->sendMessage($message);
                        $sending_end_time = microtime(true);
                        $sending_elapsed_time = $sending_end_time - $sending_start_time;
                        
                        $sending_data .= ' sending_elapsed_time: '.$sending_elapsed_time
                                            . ' - message: '.$last_message_string
                                            . ' - client: '.$client->toString()
                                        .';';
                    }
                }
            }
            else
            {
                $this->Log(__METHOD__.' - nije smeo da udje ovde', 'WARN');
            }
        }
        
        $end_time = microtime(true);
        $elapsed_time = $end_time - $start_time;
        if($elapsed_time > $this->_max_messagesending_exec_time_s)
        {
            $message_to_log = $this->_sendMessageCounter.' - max_messagesending_exec_time_s - $elapsed_time: '.$elapsed_time
                            .' - $sendmessage_max_prev_start_time_diff_log_time: '.$sendmessage_max_prev_start_time_diff_log_time
                            .' - $client_connection_count: '.$client_connection_count
                            .' - $number_of_loops: '.$number_of_loops
                            .' - $find_clients_times: '.$find_clients_times
                            .' - $number_of_sent_messages: '.$number_of_sent_messages
                            .' - $number_of_sent_messages_clients: '.$number_of_sent_messages_clients
                            .' - $sending_data: '.$sending_data;
            
            if($number_of_sent_messages_clients === 0 && $number_of_sent_messages > 0)
            {
                $message_to_log .= ' - $last_message_string: '.$last_message_string;
            }
            
            $this->Log($message_to_log, 'WARN');
        }
    }
    
    public function simaCheckClientsForOldKeepAlive()
    {
        $current_time = time();
        $limit_in_seconds = $this->_clients_keepalive_check_sleep_s;
        $limit_time = $current_time - $limit_in_seconds;
        $this->connectionPool->closeClientsWithOlderKeepAliveTime($limit_time);
    }
    
    public function simaSendKeepaliveToClients()
    {
        $this->connectionPool->sendMessageToAllClients($this->_keepAliveMessage, true);
    }
    
    public function simaDumpLogsSQLToFile($logs_file_path)
    {
        if(empty($this->_logs))
        {
            return false;
        }
        
        $logs = $this->_logs;
        $this->_logs = [];
        
        $sql = 'INSERT INTO admin.eventrelay_server_logs (log_data, log_type, create_time) values';
        $values = '';
        foreach($logs as $log)
        {
            if(!empty($values))
            {
                $values .= ',';
            }

            $log_data = $log['log_data'];
            $log_type = $log['log_type'];
            $log_timestamp = $log['log_timestamp'];
            $log_time_db_value = date('Y-m-d H:i:s', $log_timestamp);

            $values .= "\n('$log_data', '$log_type', '$log_time_db_value')";
        }
        $sql .= $values.';';
        
        $file_put_contents_result = file_put_contents($logs_file_path, $sql);
        if($file_put_contents_result === false)
        {
            return false;
        }
                
        return true;
    }
    
    private function onClientDisconnect(ClientConnection $clientConnection=null)
    {
        if(isset($clientConnection))
        {
            $clients = [];
            $clientConnectionUserId = $clientConnection->getUserId();
            if(empty($clientConnectionUserId))
            {
                $this->Log(__METHOD__." - clientConnectionUserId is empty");
            }
            else
            {
                $clients = $this->connectionPool->findClients([$clientConnectionUserId], Message::$RECEIVER_TYPE_USER, $clientConnection->getSessionKey());
            }

            $responseMessage = Message::FromArray([
                'message_type' => Message::$MESSAGE_TYPES['CLIENT_DISCONNECTED'],
                'data' => $clientConnection->getEventRelayId()
            ], false);

            foreach($clients as $client)
            {
                if($client->getEventRelayId() !== $clientConnection->getEventRelayId())
                {
                    $client->sendMessage($responseMessage);
                }
            }
            
            if($clientConnection->getType() === ClientConnection::$CLIENT_TYPES['JS']) /// obavestavamo sve js-ove o novom js client-u
            {
                $responseMessageClientAuthenticated = Message::FromArray([
                    'message_type' => Message::$MESSAGE_TYPES['JS_CLIENT_DISCONNECTED'],
                    'data' => [
                        'event_relay_id' => $clientConnection->getEventRelayId(),
                        'user_id' => $clientConnection->getUserId()
                    ]
                ], false);
                
                $clients = $this->connectionPool->getAllConnections($clientConnection->getSessionKey());
                foreach($clients as $client)
                {
                    if($client->getType() === ClientConnection::$CLIENT_TYPES['JS']
                        && $client->getEventRelayId() !== $clientConnection->getEventRelayId())
                    {
                        $client->sendMessage($responseMessageClientAuthenticated);
                    }
                }
            }
        }
    }
    
    private function onMessageClientAuthenticationRequest($senderEventRelayId, Message $message)
    {
        $sender_user_id = $message->getSenderUserId();
        $sender_session_id = $message->getSenderSessionId();
        $sender_type = $message->getSenderType();
        $session_key = $message->getSessionKey();
        
        $senderClient = $this->connectionPool->findByEventRelayId($senderEventRelayId);
        
        if($senderClient->isAuthenticated())
        {
            $error_message = 'Already authenticated';
            $error_message .= '- $sender_user_id: '.$sender_user_id;
            $error_message .= '- $sender_session_id: '.$sender_session_id;
            $error_message .= '- $sender_type: '.$sender_type;
            $error_message .= '- $session_key: '.$session_key;
//            throw new \Exception($error_message);
            $this->Log(__METHOD__.' - $error_message: '.$error_message);
            error_log(__METHOD__.' - '.date('d.m.Y. H:i:s').' - $error_message: '.$error_message);
//            throw new \Exception($error_message);
        }
        else
        {
            if(empty($sender_user_id))
            {
                throw new \Exception('$sender_user_id not set - $message: '.$message->toString());
            }
            
            $senderClient->Authenticate($sender_user_id, $sender_session_id, $sender_type, $session_key);
        
            $responseMessage = Message::FromArray([
                'message_type' => Message::$MESSAGE_TYPES['CLIENT_AUTHENTICATION_RESPONSE'],
                'data' => [
                    'event_relay_id' => $senderEventRelayId,
                    'ip_address' => $senderClient->getIpAddress(),
                    'keepalive_check_s' => $this->_clients_keepalive_check_s
                ]
            ], false);

            $senderClient->sendMessage($responseMessage);

            if($senderClient->getType() === ClientConnection::$CLIENT_TYPES['DESKTOP_APP'])
            {
                $responseMessageClientAuthenticated = Message::FromArray([
                    'message_type' => Message::$MESSAGE_TYPES['DESKTOP_APP_AUTHENTICATED'],
                    'data' => [
                        'event_relay_id' => $senderClient->getEventRelayId(),
                        'user_id' => $senderClient->getUserId(),
                        'connection_type' => $senderClient->getType(),
                        'ip_address' => $senderClient->getIpAddress()
                    ]
                ], false);

                $clients = $this->connectionPool->getAllConnections($senderClient->getSessionKey());
                foreach($clients as $client)
                {
                    if($client->getType() === ClientConnection::$CLIENT_TYPES['JS']
                        && $client->getUserId() === $senderClient->getUserId()
                        && $client->getEventRelayId() !== $senderClient->getEventRelayId())
                    {
                        $client->sendMessage($responseMessageClientAuthenticated);
                    }
                }
            }
            else if($senderClient->getType() === ClientConnection::$CLIENT_TYPES['JS']) /// obavestavamo sve js-ove o novom js client-u
            {
                $responseMessageClientAuthenticated = Message::FromArray([
                    'message_type' => Message::$MESSAGE_TYPES['JS_CLIENT_AUTHENTICATED'],
                    'data' => [
                        'event_relay_id' => $senderClient->getEventRelayId(),
                        'user_id' => $senderClient->getUserId()
                    ]
                ], false);
                
                $clients = $this->connectionPool->getAllConnections($senderClient->getSessionKey());
                foreach($clients as $client)
                {
                    if($client->getType() === ClientConnection::$CLIENT_TYPES['JS']
                        && $client->getEventRelayId() !== $senderClient->getEventRelayId())
                    {
                        $client->sendMessage($responseMessageClientAuthenticated);
                    }
                }
            }
        }
    }
    
    private function onMessageClientKeepAlive($sender_event_relay_id)
    {
        $sender_client = $this->connectionPool->findByEventRelayId($sender_event_relay_id);
        $sender_client->updateKeepAliveReceivedTime();
    }
    
    private function onMessageConnectedDesktopApplicationsRequest($senderEventRelayId, Message $message)
    {
        $sender_user_id = $message->getSenderUserId();
        
        $senderClient = $this->connectionPool->findByEventRelayId($senderEventRelayId);
        
        $clients = $this->connectionPool->findClients(
                [$sender_user_id], 
                Message::$RECEIVER_TYPE_USER, 
                $message->getSessionKey(),
                ClientConnection::$CLIENT_TYPES['DESKTOP_APP']
        );
        
        $clients_datas = [];
        foreach($clients as $client)
        {
            $clients_datas[] = [
                'event_relay_id' => $client->getEventRelayId(),
                'ip_address' => $client->getIpAddress()
            ];
        }
        
        $responseMessage = Message::FromArray([
            'message_type' => Message::$MESSAGE_TYPES['CONNECTED_DESKTOP_APPLICATIONS_RESPONSE'],
            'data' => $clients_datas
        ], false);
        
        $senderClient->sendMessage($responseMessage);
    }
    
    private function onMessageWebClientActivityGlobal($senderEventRelayId, Message $message)
    {
        $senderClient = $this->connectionPool->findByEventRelayId($senderEventRelayId);
        $sender_user_id = $message->getSenderUserId();
        $messageData = $message->getData();
        $status = $messageData['status'];
        
        $responseMessageClientAuthenticated = Message::FromArray([
            'message_type' => 'WEB_CLIENT_ACTIVITY_GLOBAL_MESSAGE',
            'data' => [
                'user_id' => $sender_user_id,
                'status' => $status
            ]
        ], false);

        $clients = $this->connectionPool->getAllConnections($senderClient->getSessionKey());
        foreach($clients as $client)
        {
            if($client->getType() === ClientConnection::$CLIENT_TYPES['JS']
                && $client->getUserId() !== $sender_user_id)
            {
                $client->sendMessage($responseMessageClientAuthenticated);
            }
        }
    }
    
    private function onMessageToAllClientsGlobal($senderEventRelayId, Message $message)
    {
//        $senderClient = $this->connectionPool->findByEventRelayId($senderEventRelayId);
        $clients = $this->connectionPool->getAllConnections($message->getSessionKey());
        foreach($clients as $client)
        {
            if($senderEventRelayId !== $client->getEventRelayId())
            {
                $client->sendMessage($message);
            }
        }
    }
    
    private function onMessageEventRelayServerStats($senderEventRelayId, $message)
    {
        $senderClient = $this->connectionPool->findByEventRelayId($senderEventRelayId);
        
        $connected_clients = [];
        $clients = $this->connectionPool->getAllConnections($message->getSessionKey());
        foreach($clients as $client)
        {
            $connected_clients[] = $this->clientConnectionInformations($client);
        }
        
        $messages = [
            'memory usage' => memory_get_usage(true),
            'connections count ' => $this->connectionPool->getConnectionsCount(),
            'connected clients ' => $connected_clients
        ];
        
        $responseMessageEventRelayServerStats = Message::FromArray([
            'message_type' => 'EVENT_RELAY_SERVER_STATS_RESPONSE',
            'data' => [
                'messages' => $messages
            ]
        ], false);
        
        $senderClient->sendMessage($responseMessageEventRelayServerStats);
    }
    
    private function clientConnectionInformations(ClientConnection $clientConnection)
    {
        return 'getEventRelayId: '.$clientConnection->getEventRelayId()
                    .' - getIpAddress: '.$clientConnection->getIpAddress()
                    .' - getUserId: '.$clientConnection->getUserId()
                    .' - getType: '.$clientConnection->getType()
                    .' - getSessionKey: '.$clientConnection->getSessionKey()
                    .' - getLastKeepAliveReceivedTimes: '.json_encode($clientConnection->getLastKeepAliveReceivedTimes())
                    .' - getLastKeepAliveSentTimes: '.json_encode($clientConnection->getLastKeepAliveSentTimes())
                    .' - getIsDisconnectedByServerTimeout: '.($clientConnection->getIsDisconnectedByServerTimeout()?'true':'false');
    }
    
    private function checkConnectedClients($senderEventRelayId, $session_key)
    {
        $ping_message = Message::FromArray([
            'message_type' => 'PING'
        ], false);
        $clients = $this->connectionPool->getAllConnections($session_key);
        foreach($clients as $client)
        {
            if($senderEventRelayId !== $client->getEventRelayId())
            {
                $client->sendMessage($ping_message);
            }
        }
    }
    
    private function Log($message, $log_type='INFO')
    {
        if($this->_log_to_db === true)
        {
            $message = $message."(".memory_get_usage(true).")";
            $this->_logs[] = [
                'log_data' => $message,
                'log_type' => $log_type,
                'log_timestamp' => time()
            ];
        }
    }
    
    private function LogMessage(Message $message)
    {
        if($this->_log_messages === true)
        {
            $this->Log('$message: '.$message->toString());
        }
    }
}

