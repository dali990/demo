<?php

namespace EventRelay;

use Ratchet\Http\HttpServer;

class SIMAHttpServer extends HttpServer
{
    public function simaSendMessage()
    {
        $this->_httpServer->simaSendMessage();
    }
    
    public function simaCheckClientsForOldKeepAlive()
    {
        $this->_httpServer->simaCheckClientsForOldKeepAlive();
    }
    
    public function simaSendKeepaliveToClients()
    {
        $this->_httpServer->simaSendKeepaliveToClients();
    }
    
    public function simaDumpLogsSQLToFile($logs_file_path)
    {
        return $this->_httpServer->simaDumpLogsSQLToFile($logs_file_path);
    }
}
