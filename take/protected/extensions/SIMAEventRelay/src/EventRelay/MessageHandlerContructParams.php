<?php

namespace EventRelay;

class MessageHandlerContructParams
{
//    public $db_params = null;
    public $message_queue_count_to_log = null;
    public $clients_keepalive_check_s = null;
    public $is_ssl = null;
    public $log_to_db = null;
    public $log_messages = null;
    public $messages_to_send_number = null;
    public $clients_keepalive_check_sleep_s = null;
    
    public function validate()
    {
//        if(is_null($this->db_params))
//        {
//            throw new \Exception('db_params empty');
//        }
        if(is_null($this->message_queue_count_to_log))
        {
            throw new \Exception('message_queue_count_to_log empty');
        }
        if(is_null($this->clients_keepalive_check_s))
        {
            throw new \Exception('clients_keepalive_check_s empty');
        }
        if(is_null($this->is_ssl))
        {
            throw new \Exception('is_ssl empty');
        }
        if(is_null($this->log_to_db))
        {
            throw new \Exception('log_to_db empty');
        }
        if(is_null($this->log_messages))
        {
            throw new \Exception('log_messages empty');
        }
        if(is_null($this->messages_to_send_number))
        {
            throw new \Exception('messages_to_send_number empty');
        }
        if(is_null($this->clients_keepalive_check_sleep_s))
        {
            throw new \Exception('clients_keepalive_check_sleep_s empty');
        }
    }
}

