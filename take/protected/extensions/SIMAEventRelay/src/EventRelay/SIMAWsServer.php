<?php

namespace EventRelay;

class SIMAWsServer extends \Ratchet\WebSocket\WsServer
{
    public function simaSendMessage()
    {
        $this->component->simaSendMessage();
    }
    
    public function simaCheckClientsForOldKeepAlive()
    {
        $this->component->simaCheckClientsForOldKeepAlive();
    }
    
    public function simaSendKeepaliveToClients()
    {
        $this->component->simaSendKeepaliveToClients();
    }
    
    public function simaDumpLogsSQLToFile($logs_file_path)
    {
        return $this->component->simaDumpLogsSQLToFile($logs_file_path);
    }
}
