/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* global sima */

function SIMAEventRelay({
    user_id: user_id,
    session_id: session_id,
    without_eventrelay: without_eventrelay,
    event_relay_host: event_relay_host,
    event_relay_port: event_relay_port,
    is_ssl: is_ssl,
    event_relay_connection_string: event_relay_connection_string,
    log_every_received_message: log_every_received_message,
    send_autobaf_on_disconnect_reconnecting: send_autobaf_on_disconnect_reconnecting,
    consolelog_keepalive_send: consolelog_keepalive_send
})
{
    var localThis = this;
    localThis.connecting_to_event_relay = false;
    localThis.connected_to_event_relay = false;
    localThis.user_id = user_id;
    localThis.session_id = session_id;
    localThis.without_eventrelay = without_eventrelay;
    localThis.connection_socket = null;
    localThis.event_relay_id = null;
    localThis.ip_address = null;
    localThis.keepalive_check_s = null;
    localThis.last_keepalive_sent_times = [];
    localThis.last_keepalive_received_times = [];
    localThis.max_number_of_keepalive_times = 5;
    localThis.log_every_received_message = log_every_received_message;
    localThis.send_autobaf_on_disconnect_reconnecting = send_autobaf_on_disconnect_reconnecting;
    localThis.consolelog_keepalive_send = consolelog_keepalive_send;
    
    function init()
    {
        if(localThis.without_eventrelay === false)
        {
            connectToEventRelayServer(localThis);
        }
    }
    
    function connectToEventRelayServer(localThis)
    {
        if(typeof localThis.connection_socket !== 'undefined' 
            && localThis.connection_socket !== null)
        {
            if(localThis.connection_socket.constructor.name === 'WebSocket')
            {
                localThis.connection_socket.close();
            }
            delete localThis.connection_socket;
        }
        
////        var protocol = 'ws';
////        if(params.is_ssl === true)
////        {
////            protocol += 's';
////        }
////        var connection_string = protocol+'://'+params.event_relay_host+':'+params.event_relay_port;
//        var connection_string = 'ws://'+event_relay_host+':'+event_relay_port;
//        if(is_ssl === true)
//        {
//            var connection_string = 'wss://'+event_relay_host+'/wss2/'+event_relay_port;
//        }
        var connection_string = null;
        if(!sima.isEmpty(event_relay_connection_string))
        {
            connection_string = event_relay_connection_string;
        }
        else
        {
            var connection_string = 'ws://'+event_relay_host+':'+event_relay_port;
            if(is_ssl === true)
            {
                var connection_string = 'wss://'+event_relay_host+'/wss2/'+event_relay_port;
            }
        }
        
        localThis.connection_socket = new WebSocket(connection_string);

        localThis.connection_socket.onopen = function () {
            var current_timedate = new Date();
            console.warn(current_timedate.toLocaleString()+' - eventrelay client - onopen');
            localThis.connected_to_event_relay = true;
            localThis.last_keepalive_sent_times = [];
            localThis.last_keepalive_received_times = [];
            updateKeepAliveReceivedTime(localThis);
            sendSocketMessage(localThis, 'CLIENT_AUTHENTICATION_REQUEST');
            $(localThis).trigger('onopen');
        };
        localThis.connection_socket.onmessage = function(event){
            if(sima.ajax.getWasNetworkOutage() === true)
            {
                return true;
            }
            
            var event_data = event.data;

            var message = null;
            var successfull_parse = false;

            try
            {
                message = JSON.parse(event_data);
                successfull_parse = true;
            }
            catch(e)
            {
                sima.addError('simaEventRelay onmessage', 'SIMAEventRelay greska:</br>'+JSON.stringify(event_data));
            }
            
            if(localThis.log_every_received_message === true)
            {
                console.log(message);
            }
            
            if(successfull_parse === true)
            {
                var message_type = message.message_type;
                
                if(message_type === 'CLIENT_AUTHENTICATION_RESPONSE')
                {
                    localThis.event_relay_id = message.data.event_relay_id;
                    localThis.ip_address = message.data.ip_address;
                    localThis.keepalive_check_s = message.data.keepalive_check_s;
                    resetKeepAlive(localThis);
                }
                else if(message_type === 'SERVER_KEEP_ALIVE')
                {
                    updateKeepAliveReceivedTime(localThis);
                }

                $(localThis).trigger(message_type, [{message:message}]);
            }
        };
        localThis.connection_socket.onclose = function(event){
            var current_timedate = new Date();
            console.warn(current_timedate.toLocaleString()+' - eventrelay client - onclose');
            const was_connected_to_eventrelay = localThis.connected_to_event_relay === true;
            localThis.connected_to_event_relay = false;
            $(localThis).trigger('onclose');

            var reason;
            var to_reconnect = true;
            if (event.code === 1000)
            {
                reason = "Normal closure, meaning that the purpose for which the connection was established has been fulfilled.";
            }
            else if(event.code === 1001)
            {
                reason = "An endpoint is \"going away\", such as a server going down or a browser having navigated away from a page.";
                to_reconnect = false;
            }
            else if(event.code === 1002)
            {
                reason = "An endpoint is terminating the connection due to a protocol error";
            }
            else if(event.code === 1003)
            {
                reason = "An endpoint is terminating the connection because it has received a type of data it cannot accept (e.g., an endpoint that understands only text data MAY send this if it receives a binary message).";
            }
            else if(event.code === 1004)
            {
                reason = "Reserved. The specific meaning might be defined in the future.";
            }
            else if(event.code === 1005)
            {
                reason = "No status code was actually present.";
            }
            else if(event.code === 1006)
            {
               reason = "The connection was closed abnormally, e.g., without sending or receiving a Close control frame";
            }
            else if(event.code === 1007)
            {
                reason = "An endpoint is terminating the connection because it has received data within a message that was not consistent with the type of the message (e.g., non-UTF-8 [http://tools.ietf.org/html/rfc3629] data within a text message).";
            }
            else if(event.code === 1008)
            {
                reason = "An endpoint is terminating the connection because it has received a message that \"violates its policy\". This reason is given either if there is no other sutible reason, or if there is a need to hide specific details about the policy.";
            }
            else if(event.code === 1009)
            {
               reason = "An endpoint is terminating the connection because it has received a message that is too big for it to process.";
            }
            else if(event.code === 1010) // Note that this status code is not used by the server, because it can fail the WebSocket handshake instead.
            { 
                reason = "An endpoint (client) is terminating the connection because it has expected the server to negotiate one or more extension, but the server didn't return them in the response message of the WebSocket handshake. <br /> Specifically, the extensions that are needed are: " + event.reason;
            }
            else if(event.code === 1011)
            {
                reason = "A server is terminating the connection because it encountered an unexpected condition that prevented it from fulfilling the request.";
            }
            else if(event.code === 1015)
            {
                reason = "The connection was closed due to a failure to perform a TLS handshake (e.g., the server certificate can't be verified).";
            }
            else
            {
                reason = "Unknown reason";
            }
            var disconnect_log_message = current_timedate.toLocaleString()+' - eventrelay client - '+reason;

            if(to_reconnect === true)
            {
                disconnect_log_message += 'last_keepalive_sent_times: ' + localThis.last_keepalive_sent_times;
                disconnect_log_message += 'last_keepalive_received_times: ' + localThis.last_keepalive_received_times;
                
                setTimeout(function () {
                    if(sima.ajax.getCurrentlyNetworkOutage() === false)
                    {
                        connectToEventRelayServer(localThis);
                    }
                }, 1000);
                
                if(was_connected_to_eventrelay === true && localThis.send_autobaf_on_disconnect_reconnecting)
                {
                    const autobafreq_theme_id = 3299;
                    const autobafreq_theme_name = 'AutoBafReq eventrelay js client disconnected reconnecting';
                    sima.errorReport.createAutoClientBafRequest(disconnect_log_message, autobafreq_theme_id, autobafreq_theme_name);
                }
            }
            console.warn(disconnect_log_message);
        };
        localThis.connection_socket.onerror = function(){
            var current_timedate = new Date();
            console.warn(current_timedate.toLocaleString()+' - eventrelay client - onerror');
            localThis.connected_to_event_relay = false;
            $(localThis).trigger('onerror');
            
            /// ne treba jer se zove i onclose
//            setTimeout(function () {
//                if(sima.ajax.getCurrentlyNetworkOutage() === false)
//                {
//                    connectToEventRelayServer(localThis);
//                }
//            }, 5000);
        };
    }
    
    function sendSocketMessage(localThis, message_type, receiver_id, receiver_type, data)
    {
        if(sima.ajax.getWasNetworkOutage() === true)
        {
            return true;
        }
        
        if(localThis.connected_to_event_relay === true)
        {
            receiver_id = typeof receiver_id !== 'undefined' ? receiver_id : '';
            receiver_type = typeof receiver_type !== 'undefined' ? receiver_type : '';
            data = typeof data !== 'undefined' ? data : '';
            
            var msg = {
                sender_event_relay_id: localThis.event_relay_id,
                sender_user_id: localThis.user_id,
                sender_session_id: localThis.session_id,
                receiver_id: receiver_id,
                receiver_type: receiver_type,
                sender_type: 'JS',
                message_type: message_type,
                data: data,
                session_key: sima.getSessionKey()
            };
            var stringify_msg = JSON.stringify(msg);
            localThis.connection_socket.send(stringify_msg);
        }
    }
    
    this.isConnected = function()
    {
        return this.connected_to_event_relay;
    };
    
    this.isAuthenticated = function()
    {
        return !sima.isEmpty(this.event_relay_id);
    };
    
//    this.isConnecting = function()
//    {
//        return this.connecting_to_event_relay;
//    }

    this.getEventRelayId = function()
    {
        return this.event_relay_id; 
    };
    
    this.getIpAddress = function()
    {
        return this.ip_address; 
    };
    
    this.getSessionId = function()
    {
        return this.session_id;
    };

    this.getConnectionSocket = function()
    {
        return this.connection_socket;
    };
    
    this.reconnect = function()
    {
        init(this);
    };
    
    this.sendMessage = function(message_type, receiver_id, receiver_type, data)
    {
        sendSocketMessage(this, message_type, receiver_id, receiver_type, data);
    };
    
    var keep_alive_timeout = null;
    function keepAlive(localThis)
    {
        const keepAlive_start_timestamp = getCurrentTimestamp();
        const was_keepalive_sent_diff_exceeded = updateKeepAliveSentTime(localThis, keepAlive_start_timestamp);
        const lastKeepAliveReceivedTime = getLastKeepAliveReceivedTime(localThis);
        if(was_keepalive_sent_diff_exceeded === false && keepAlive_start_timestamp - lastKeepAliveReceivedTime > 3*sima.eventrelay.keepalive_check_s)
        {
            var current_timedate = new Date();
            console.warn(current_timedate.toLocaleString()+' - eventrelay client - disconnect - server timeout');
            localThis.connection_socket.close();
        }
        else
        {
            sima.eventrelay.sendMessage('CLIENT_KEEP_ALIVE');
            const beforereset_timestamp = getCurrentTimestamp();
            const diff_time = beforereset_timestamp - keepAlive_start_timestamp;
            const new_timeout_s = sima.eventrelay.keepalive_check_s - diff_time;
            resetKeepAlive(localThis, new_timeout_s);
        }
    }
    
    /**
     * 
     * @param {type} localThis
     * @param {integer} timeout_s - opcioni
     * @returns {undefined}
     */
    function resetKeepAlive(localThis, timeout_s)
    {
        clearTimeout(keep_alive_timeout);
        if(!sima.isEmpty(sima.eventrelay.keepalive_check_s) && sima.eventrelay.connected_to_event_relay === true)
        {
            if(typeof timeout_s === 'undefined' || timeout_s < 1)
            {
                timeout_s = sima.eventrelay.keepalive_check_s;
            }
            
            keep_alive_timeout =  setTimeout(function() {
                keepAlive(localThis);
            }, timeout_s * 1000);
        }
    };
    function updateKeepAliveReceivedTime(localThis)
    {
        localThis.last_keepalive_received_times.push(getCurrentTimestamp());
        if(localThis.last_keepalive_received_times.length > localThis.max_number_of_keepalive_times)
        {
            localThis.last_keepalive_received_times.shift();
        }
    }
    function updateKeepAliveSentTime(localThis, current_timestamp)
    {
        var was_keepalive_sent_diff_exceeded = false;
        
        const lastKeepAliveSentTime = getLastKeepAliveSentTime(localThis);
        localThis.last_keepalive_sent_times.push(current_timestamp);
        if(localThis.last_keepalive_sent_times.length > localThis.max_number_of_keepalive_times)
        {
            localThis.last_keepalive_sent_times.shift();
        }
        
        if(localThis.consolelog_keepalive_send === true)
        {
            console.log('eventrelay js client - current_timestamp: ' + current_timestamp + ' - last_keepalive_sent_times: ' + localThis.last_keepalive_sent_times);
        }
        
        if(typeof lastKeepAliveSentTime !== 'undefined')
        {
            const diff_from_last_sent_time = current_timestamp - lastKeepAliveSentTime;
//            const allowed_diff_percentage = 30;
//            const allowed_diff_percentage = 100; /// za 20s
            const allowed_diff_percentage = 200; /// za 30s
            const allowed_diff = localThis.keepalive_check_s + (localThis.keepalive_check_s * (allowed_diff_percentage / 100));
            if(diff_from_last_sent_time > allowed_diff)
            {
                const disconnect_log_message = 'diff_from_last_sent_time: '+diff_from_last_sent_time
                                                +' - allowed_diff: '+allowed_diff
                                                +' - current_timestamp: '+current_timestamp
                                                +' - lastKeepAliveSentTime: '+lastKeepAliveSentTime;
                const autobafreq_theme_id = 3300;
                const autobafreq_theme_name = 'AutoBafReq eventrelay js client diff from last keepalive sent time exceeded';
                sima.errorReport.createAutoClientBafRequest(disconnect_log_message, autobafreq_theme_id, autobafreq_theme_name);
                
                was_keepalive_sent_diff_exceeded = true;
            }
        }
        
        return was_keepalive_sent_diff_exceeded;
    }
    function getLastKeepAliveReceivedTime(localThis)
    {
        return localThis.last_keepalive_received_times[localThis.last_keepalive_received_times.length - 1];
    }
    function getLastKeepAliveSentTime(localThis)
    {
        return localThis.last_keepalive_sent_times[localThis.last_keepalive_sent_times.length - 1];
    }
    
    /**
     * 
     * @returns {int} current timestamp in seconds
     */
    function getCurrentTimestamp()
    {
        return Math.floor(Date.now() / 1000);
    }
    
    init(localThis);
}
