/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* global sima */

function SIMAEventRelayAdmin()
{
    var localThis = this;
    
    function init()
    {
        $(sima.eventrelay).on('EVENT_RELAY_SERVER_STATS_RESPONSE', onEventRelayServerStatsResponse);
    }
    
    this.onClientStatsRefresh = function()
    {
        var stats = '<div class="sima-layout-panel"><ul>';
        
        stats += '<li>last_keepalive_sent_times: ' + sima.eventrelay.last_keepalive_sent_times + '</li>';
        stats += '<li>last_keepalive_received_times: ' + sima.eventrelay.last_keepalive_received_times + '</li>';
        
        stats += '</ul></div>';
        sima.set_html($('.event-relay-server-admin').find('.client-stats'), stats);
    };
    
    this.onServerStatsRefresh = function()
    {
        $('.event-relay-server-admin').find('.server-stats').text('Refreshing');
        sima.eventrelay.sendMessage('EVENT_RELAY_SERVER_STATS', '', '', {});
    };
    
    this.checkConnectedclients = function()
    {
        sima.eventrelay.sendMessage('EVENT_RELAY_SERVER_CHECK_CONNECTED_CLIENTS', '', '', {});
    };
    
    function onEventRelayServerStatsResponse(event, event_message)
    {
        var stats = '<div class="sima-layout-panel"><ul>';
        var messages = event_message.message.data.messages;
        $.each(messages, function( key, value ) {
            stats += '<li>'+key+': ';
            if($.isArray(value))
            {
                stats += '<ul>';
                $.each(value, function( subkey, subvalue ) {
                    stats += '<li>'+subvalue+'</li>';
                });
                stats += '</ul>';
            }
            else
            {
                stats += value;
            }
            stats += '</li>';
        });
        stats += '</ul></div>';
        sima.set_html($('.event-relay-server-admin').find('.server-stats'), stats);
    }
    
    init(localThis);
}
