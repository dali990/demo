<?php

return [
    'InvalidMessageData' => 'Nepravilni podaci u WebSocketClient::sendBulk: {message_data}',
    'ExceptionSendingMessage' => 'Greska u slanju poruke. Razlog:<br>{reason}; Poruka: {json_message}',
    'EventRelayNotInstalled' => 'SIMAEventRelay nije instaliran</br>molimo kontaktirajte administratora',
];

