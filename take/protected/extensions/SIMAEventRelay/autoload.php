<?php

require_once __DIR__.'/../../../../yii/framework/base/CException.php';
require_once __DIR__.'/../../modules/base/exceptions/SIMAException.php';
require_once __DIR__.'/src/EventRelay/MessageHandlerContructParams.php';
$interprocessmutex_dir = __DIR__.'/../SIMAInterprocessMutex';
require_once $interprocessmutex_dir . DIRECTORY_SEPARATOR . 'SIMAInterprocessMutex.php';
require_once $interprocessmutex_dir . DIRECTORY_SEPARATOR . 'exceptions' . DIRECTORY_SEPARATOR . 'SIMAInterprocessMutexLockFileExists.php';


