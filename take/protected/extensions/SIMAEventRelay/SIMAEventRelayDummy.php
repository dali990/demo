<?php

class SIMAEventRelayDummy
{
    private $pidfile = null;
    
    public $installed = false;
    public $without_sda = true;
    
    public function init()
    {
    }
    
    public function isInstalled()
    {
        return $this->installed;
    }
    
    public function eventRelayStatus()
    {
        throw new SIMAWarnException(Yii::t('SIMAEventRelay.Translation', 'EventRelayNotInstalled'));
    }
    
    public function stopEventRelay()
    {
        throw new SIMAWarnException(Yii::t('SIMAEventRelay.Translation', 'EventRelayNotInstalled'));
    }
    
    public function sendEventToUser($receiver_id, $message_type, $data)
    {
        throw new SIMAWarnException(Yii::t('SIMAEventRelay.Translation', 'EventRelayNotInstalled'));
    }
    
    public function startEventRelay()
    {
        throw new SIMAWarnException(Yii::t('SIMAEventRelay.Translation', 'EventRelayNotInstalled'));
    }
    
    private function sendEvent($receiver_id, $receiver_type, $message_type, $data)
    {
        throw new SIMAWarnException(Yii::t('SIMAEventRelay.Translation', 'EventRelayNotInstalled'));
    }
    
    public function registerManual() 
    {
        throw new SIMAWarnException(Yii::t('SIMAEventRelay.Translation', 'EventRelayNotInstalled'));
    }
}