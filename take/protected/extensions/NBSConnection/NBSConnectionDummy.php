<?php

class NBSConnectionDummy
{    
//    public $username = 'MilosJankovic';
//    public $password = 'MilosJankovic';
//    public $licence = 'MilosJankovic';
//    
    public function init()
    {
    }
    public function isConnected()
    {
        return false;
    }
    
    public function GetCurrentCurrencyMidleExchangeRate(Currency $currency)
    {        
        throw new Exception(Yii::t('NBSConnection.Default', 'NotInstalled'));
    }
    
    
    public function GetCurrentCurrencyMiddleExchangeRateOnDate($currency_code, $date)
    {
        throw new Exception(Yii::t('NBSConnection.Default', 'NotInstalled'));
    }
    
    /**
     * 
     * @param string $date - oblika YYYYMMDD
     * @return array - niz oblika [ [code, value], [code, value], ... ]
     * @throws Exception
     */
    public function GetCurrencyMiddleExchangeRatesOnCurrentDate()
    {
        throw new Exception(Yii::t('NBSConnection.Default', 'NotInstalled'));
    }
    
    public function GetCompanyInformationByPIB($companyPIB)
    {
        throw new Exception(Yii::t('NBSConnection.Default', 'NotInstalled'));
    }
    
    public function GetCompanyAccountByPIB($companyPIB)
    {
        throw new Exception(Yii::t('NBSConnection.Default', 'NotInstalled'));
    }
    
    private function createSoapClient($soap_wsdl)
    {
        throw new Exception(Yii::t('NBSConnection.Default', 'NotInstalled'));
    }
    
    private function UserToNBSWrapDate($date)
    {
        throw new Exception(Yii::t('NBSConnection.Default', 'NotInstalled'));
    }
}