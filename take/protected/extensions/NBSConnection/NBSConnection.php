<?php

require_once('lib/nusoap.php');

class NBSConnection
{    
    public $username = 'MilosJankovic';
    public $password = 'MilosJankovic';
    public $licence = 'MilosJankovic';
    
    public function init()
    {
    }
    
    public function isConnected()
    {
        return true;
    }
    
    public function GetCurrentCurrencyMidleExchangeRate(Currency $currency)
    {        
//        if(!isset(Yii::app()->params['nbs_connection_installed']) || Yii::app()->params['nbs_connection_installed'] === false)
//        {
//            throw new Exception('NBS connection not installed');
//        }
        
        $currencyCode = $currency->code; //euro
        $exchangeRateListTypeID = 3; //srednji
        $rateType = 2; //средњи
        
        $soap_wsdl = "https://webservices.nbs.rs/CommunicationOfficeService1_0/CurrentExchangeRateXmlService.asmx?WSDL";
        
        $soap_client = $this->createSoapClient($soap_wsdl);
        
        $p_parameters = [
            'currencyCode' => $currencyCode,
            'exchangeRateListTypeID' => $exchangeRateListTypeID,
            'rateType' => $rateType
        ];
        
        $m_soap_result = $this->perform_nbs_api_call($soap_client, 'GetCurrentExchangeRateByRateType', $p_parameters);
        
        $m_error = $soap_client->getError();
        if ($m_error) 
        {
            throw new Exception(__METHOD__.' - doslo je do greske: '.CJSON::encode($m_soap_result));
        }
        
        $result = $m_soap_result['GetCurrentExchangeRateByRateTypeResult'];
        
        return $result;
    }
    
    
    public function GetCurrentCurrencyMiddleExchangeRateOnDate($currency_code, $date)
    {
        $result = null;
        
        $exchangeRateListTypeID = 3; //srednji
        $formatedDate = $this->UserToNBSWrapDate($date);
        
        $soap_wsdl = "https://webservices.nbs.rs/CommunicationOfficeService1_0/ExchangeRateXmlService.asmx?WSDL";
        
        $soap_client = $this->createSoapClient($soap_wsdl);
        
        $p_parameters = [
            'date' => $formatedDate,
            'exchangeRateListTypeID' => $exchangeRateListTypeID,
        ];
        
        $m_soap_result = $this->perform_nbs_api_call($soap_client, 'GetExchangeRateByDate', $p_parameters);
        
        $m_error = $soap_client->getError();
        if ($m_error) 
        {
            $error_message = 'doslo je do greske: '.CJSON::encode($m_soap_result);
            throw new Exception(__METHOD__.' - '.$error_message);
        }
        
        $exchangeRateByDateResult = $m_soap_result['GetExchangeRateByDateResult'];
        
        $xml = simplexml_load_string($exchangeRateByDateResult);
        
        $xml_children = $xml->children();
        foreach($xml_children as $child)
        {            
            $currencyCode = $child->CurrencyCode->__toString();
            
            if($currency_code === $currencyCode)
            {
                $result = $child->MiddleRate->__toString();
                break;
            }
        }
        
        return $result;
    }
    
    /**
     * 
     * @param string $date - oblika YYYYMMDD
     * @return array - niz oblika [ [code, value], [code, value], ... ]
     * @throws Exception
     */
    public function GetCurrencyMiddleExchangeRatesOnCurrentDate()
    {
//        if(!isset(Yii::app()->params['nbs_connection_installed']) || Yii::app()->params['nbs_connection_installed'] === false)
//        {
//            throw new Exception('NBS connection not installed');
//        }
        
        $exchangeRateListTypeID = 3; //srednji
                
        $soap_wsdl = "https://webservices.nbs.rs/CommunicationOfficeService1_0/CurrentExchangeRateXmlService.asmx?WSDL";
        
        $soap_client = $this->createSoapClient($soap_wsdl);
        
        $p_parameters = [
            'exchangeRateListTypeID' => $exchangeRateListTypeID,
        ];
        
        $m_soap_result = $this->perform_nbs_api_call($soap_client, 'GetCurrentExchangeRate', $p_parameters);
        
        $m_error = $soap_client->getError();
        if ($m_error) 
        {
            $error_message = 'doslo je do greske: '.CJSON::encode($m_soap_result);
            throw new Exception(__METHOD__.' - '.$error_message);
        }
        
        $exchangeRateByDateResult = $m_soap_result['GetCurrentExchangeRateResult'];
        
        $xml = simplexml_load_string($exchangeRateByDateResult);
        
        $result = [];
        $xml_children = $xml->children();
        foreach($xml_children as $child)
        {            
            $currencyCode = $child->CurrencyCode->__toString();
            $middleRate = $child->MiddleRate->__toString();
            
            $result[] = [
                'code' => $currencyCode,
                'value' => $middleRate
            ];
        }
        
        return $result;
    }
    
    public function GetCompanyInformationByPIB($companyPIB, $call_get_company_account_method = true)
    {
        $result = $this->GetCompanyInformation($companyPIB, 0, $call_get_company_account_method);
        
        return $result;
    }
    
    public function GetCompanyInformationByMB($companyMB, $call_get_company_account_method = true)
    {
        $result = $this->GetCompanyInformation(0, $companyMB, $call_get_company_account_method);
        
        return $result;
    }
    
    private function GetCompanyInformation($companyPIB, $companyMB, $call_get_company_account_method = true)
    {
        $soap_wsdl = "https://webservices.nbs.rs/CommunicationOfficeService1_0/CoreXmlService.asmx?WSDL";
        
        $soap_client = $this->createSoapClient($soap_wsdl);
        
        $p_parameters = [
            'companyID' => '00000000-0000-0000-0000-000000000000',
            'companyCode' => 0,
            'name' => '',
            'city' => '',
            'nationalIdentificationNumber' => $companyMB,
            'taxIdentificationNumber' => $companyPIB,
            'startItemNumber' => 0,
            'endItemNumber' => 0
        ];
        
        $m_soap_result = $this->perform_nbs_api_call($soap_client, 'GetCompany', $p_parameters);

        $m_error = $soap_client->getError();
        if ($m_error) 
        {
            $error_message = 'doslo je do greske: '.CJSON::encode($m_soap_result);
            throw new Exception(__METHOD__.' - '.$error_message);
        }
                
        $getCompanyResult = $m_soap_result['GetCompanyResult'];
                
        $xml = simplexml_load_string($getCompanyResult);
        $xml_children = $xml->children();
        
        $pib_from_nbs = '';
        $mb_from_nbs = '';
        if (!empty($xml_children))
        {
            $pib_from_nbs = trim($xml_children[0]->TaxIdentificationNumber->__toString());
            $mb_from_nbs = trim($xml_children[0]->NationalIdentificationNumber->__toString());
        }

        $result = [];
        //ako u nbs nije nista nasao, ili je prazan pib ili mb, onda se pokusava pomocu GetCompanyAccount metode
        if (
                $call_get_company_account_method === true &&
                (
                    empty($xml_children) ||
                    empty($pib_from_nbs) || 
                    empty($mb_from_nbs)
                )
           )
        {
            if (!empty($companyMB))
            {
                $company_account_result = $this->GetCompanyAccountByMB($companyMB, false);
                if (!empty($company_account_result[0]) && !empty($company_account_result[0]['TaxIdentificationNumber']))
                {
                    $result = $this->GetCompanyInformationByPIB($company_account_result[0]['TaxIdentificationNumber'], false);
                    if (!empty($result) && empty($result['NationalIdentificationNumber']))
                    {
                        $result['NationalIdentificationNumber'] = $companyMB;
                    }
                }
            }

            if (empty($result) && !empty($companyPIB))
            {
                $company_account_result = $this->GetCompanyAccountByPIB($companyPIB, false);
                if (!empty($company_account_result[0]) && !empty($company_account_result[0]['NationalIdentificationNumber']))
                {
                    $result = $this->GetCompanyInformationByMB($company_account_result[0]['NationalIdentificationNumber'], false);
                    if (!empty($result) && empty($result['TaxIdentificationNumber']))
                    {
                        $result['TaxIdentificationNumber'] = $companyPIB;
                    }
                }
            }
        }

        if (empty($result) && !empty($xml_children))
        {
            $company_data_xml = $xml_children[0];
            $result = [
                'CompanyID' => trim($company_data_xml->CompanyID->__toString()),
                'NationalIdentificationNumber' => $mb_from_nbs,
                'TaxIdentificationNumber' => $pib_from_nbs,
                'Name' => trim($company_data_xml->Name->__toString()),
                'ShortName' => trim($company_data_xml->ShortName->__toString()),
                'Address' => trim($company_data_xml->Address->__toString()),
                'City' => trim($company_data_xml->City->__toString()),
                'Municipality' => trim($company_data_xml->Municipality->__toString()),
                'Region' => trim($company_data_xml->Region->__toString()),
                'PostalCode' => trim($company_data_xml->PostalCode->__toString()),
                'ActivityName' => trim($company_data_xml->ActivityName->__toString()),
                'RegistrationDate' => trim($company_data_xml->RegistrationDate->__toString()),
                'FoundingDate' => trim($company_data_xml->FoundingDate->__toString()),
                'CompanyTypeID' => trim($company_data_xml->CompanyTypeID->__toString()),
                'CompanyStatusID' => trim($company_data_xml->CompanyStatusID->__toString()),
                'NBSDate' => trim($company_data_xml->NBSDate->__toString()),
                'Resource' => trim($company_data_xml->Resource->__toString()),
            ];
        }
        else if (empty($result) && empty($xml_children))
        {
            throw new SIMAWarnException(Yii::t('NBSConnection.Default', 'NoCompanyWithThatPIB'));
        }

        return $result;
    }
    
    public function GetCompanyAccountByPIB($companyPIB, $call_get_company_information_method = true)
    {
        $result = $this->getCompanyAccount([
            'taxIdentificationNumber' => $companyPIB
        ], $call_get_company_information_method);
        
        return $result;
    }
    
    public function GetCompanyAccountByMB($companyMB, $call_get_company_information_method = true)
    {
        $result = $this->getCompanyAccount([
            'nationalIdentificationNumber' => $companyMB
        ], $call_get_company_information_method);
        
        return $result;
    }
    
    public function GetCompanyAccountByBankAccountNumber($bank_account_number, $call_get_company_information_method = true)
    {
        $params = [
            'taxIdentificationNumber' => null
        ];
        $bank_account_number_parts = explode('-', $bank_account_number);
        if (!empty($bank_account_number_parts) && count($bank_account_number_parts) === 3)
        {
            $params['bankCode'] = $bank_account_number_parts[0];
            $params['accountNumber'] = $bank_account_number_parts[1];
            $params['controlNumber'] = $bank_account_number_parts[2];
        }
        else
        {
            $params['accountNumber'] = $bank_account_number;
        }

        $result = $this->getCompanyAccount($params, $call_get_company_information_method);
        
        return $result;
    }

    private function getCompanyAccount($params, $call_get_company_information_method = true)
    {
        $soap_wsdl = "https://webservices.nbs.rs/CommunicationOfficeService1_0/CompanyAccountXmlService.asmx?WSDL";
        
        $soap_client = $this->createSoapClient($soap_wsdl);

        $p_parameters = array_merge([
            'nationalIdentificationNumber' => 0,
            'taxIdentificationNumber' => 0,
            'bankCode' => 0,
            'accountNumber' => 0,
            'controlNumber' => 0,
            'companyName' => '',
            'city' => '',
            'startItemNumber' => 0,
            'endItemNumber' => 0
        ], $params);

        $m_soap_result = $this->perform_nbs_api_call($soap_client, 'GetCompanyAccount', $p_parameters);
                
        $m_error = $soap_client->getError();
        if ($m_error) 
        {
            $error_message = 'doslo je do greske: '.CJSON::encode($m_soap_result);
            throw new Exception(__METHOD__.' - '.$error_message);
        }
        
        $getCompanyResult = $m_soap_result['GetCompanyAccountResult'];
        
        $result = [];
        
        $xml = simplexml_load_string($getCompanyResult);
        $xml_children = $xml->children();
        foreach($xml_children as $child)
        {
            $result[] = [
                'Account' => trim($child->Account->__toString()),
                'BankCode' => trim($child->BankCode->__toString()),
                'AccountNumber' => trim($child->AccountNumber->__toString()),
                'ControlNumber' => trim($child->ControlNumber->__toString()),
                'BankName' => trim($child->BankName->__toString()),
                'TaxIdentificationNumber' => trim($child->TaxIdentificationNumber->__toString()),
                'NationalIdentificationNumber' => trim($child->NationalIdentificationNumber->__toString()),
            ];
        }

        if (
                $call_get_company_information_method === true && 
                !empty($result[0]) && 
                (
                    empty($result[0]['TaxIdentificationNumber']) || empty($result[0]['NationalIdentificationNumber'])
                ) && 
                (
                    !empty($result[0]['TaxIdentificationNumber']) || !empty($result[0]['NationalIdentificationNumber'])
                )
            )
        {
            $company_result = $this->GetCompanyInformation($result[0]['TaxIdentificationNumber'], $result[0]['NationalIdentificationNumber'], false);

            if (!empty($company_result))
            {
                $company_result_intersect = array_intersect_key($company_result, $result[0]);
                foreach ($company_result_intersect as $key => $value)
                {
                    if (empty($result[0][$key]))
                    {
                        $result[0][$key] = $value;
                    }
                }
            }
        }
        
        return $result;
    }
    
    private function createSoapClient($soap_wsdl)
    {
        $login_username = $this->username; 
        $login_password = $this->password; 
        $login_licenceid = $this->licence;
        
        $soap_namespace = "http://communicationoffice.nbs.rs";

        $soap_client = new nusoap_client($soap_wsdl, true);
                
        $soap_client->decode_utf8 = 0;
        
        $m_header = '<AuthenticationHeader xmlns="' . $soap_namespace . '">'
                . '<UserName>' . $login_username . '</UserName> '
                . '<Password>' . $login_password . '</Password> '
                . '<LicenceID>' . $login_licenceid . '</LicenceID> '
                . '</AuthenticationHeader>'; 
        
        $soap_client->setHeaders($m_header);
        
        return $soap_client;
    }
    
    private function UserToNBSWrapDate($date)
    {
        if ($date==null || $date=='' || $date=='null')
        {
            return null;
        }
        try
        {
            $dt = new DateTime($date);
            $format = 'Ymd';
            return $dt->format($format);
        }
        catch (Exception $e)
        {
            return $date;
        }
    }
    
    private function perform_nbs_api_call($soap_client, $action, $parameters)
    {
        try
        {
            $m_soap_result = $soap_client->call($action, $parameters);
            return $m_soap_result;
        }
        catch(Exception $e)
        {
            Yii::app()->errorReport->createAutoClientBafRequest(SIMAMisc::toTypeAndJsonString($e));
            throw new SIMAWarnException(Yii::t('NBSConnection.Default', 'CommunicationProblem'));
        }
    }
}