<?php

return [
    'NotInstalled' => 'Konekcija ka NBS nije adekvatno podešena',
    'NoCompanyWithThatPIB' => 'Ne postoji kompanija sa zadatim PIB-om',
    'CommunicationProblem' => 'Doslo je do problema u komunikaciji sa NBS'
];