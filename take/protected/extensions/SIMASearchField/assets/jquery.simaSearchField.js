/* global sima */

/**
 * jquery plugin za tabove u sima sistemu, poziva se za odgovarajuci div koji se i prosledjuje ($)
 * nakon poziva u odgovarajuci niz tabova koji se salje kao parametar se smestaju tabovi
 * 
 * 
 * @param {type} $
 * @returns {}
 */

(function($){
      
    /*struktura metoda u ovom plugin*/
    var methods = {
        /** 
         * funkcija koja sluzi za inicijalizaciju plugin-a postavlja globalne promenljive
         * na prosledjene vrednosti pri pozivu
         * @param {array} objects - levi objekat koji moze da menja id modela za koji su generisani tabovi
         * @param {object} tabs - JQuery objekat tabova koji se obradjuju
         * @param {array} params - parametri 
         *          ignore_tabs - tabovi koji se ne renderuju - ignorisu se (npr info tab kad se otvara profil osobe)
         *          untouchable_tabs - tabovi koji se ne menjaju
         * @returns {undefined}
         */
        init : function(params) {
            
            var localThis = this;
            /* podrazumevane vrednosti */
            params = params || {};
            
            if (typeof params.action !=='undefined')
                this.data('action', params.action);
            if (typeof params.model !=='undefined')
                this.data('model', params.model);
            if (typeof params.relation !=='undefined' && params.relation !== '')
                this.data('relation', params.relation);
            if (!sima.isEmpty(params.filters))
            {                
                this.data('filters', {initial: params.filters});
//                for (var key in params.filters)
//                    console.log(key+' => '+params.filters[key]);
            }
            else
            {                
                this.data('filters', {initial:{}});
            }
            if (typeof params.onchange !=='undefined' && params.onchange !== '')
                this.data('onchange', params.onchange);
            if (typeof params.onremove !=='undefined' && params.onremove !== '')
                this.data('onremove', params.onremove);
            
            var removeIcon = localThis.find('.sima-ui-sf-remove');
            var input = localThis.find('.sima-ui-sf-input');
            var input_display = localThis.find('.sima-ui-sf-display');
            var result = $("<div class='sima-ui-sf-result' tabindex='-1'></div>").append("\
                            <input class='sima-ui-sf-result-input' tabindex='-1' type='text' autocomplete='off' value='' placeholder='pretraga' />").append("\n\
                            <div class='sima-ui-sf-result-body sima-popup-container-for-scroll' tabindex='-1'></div>");
            var result_input = result.find('.sima-ui-sf-result-input');
            var result_body = result.find('.sima-ui-sf-result-body');
            result.simaPopup('init', $('body'), this);          
            localThis.bind('destroyed', function() {                
                result.remove();                
            });
            
            //ako je setovana inicijalna vrednost polja upisemo je u polje za pretragu
            if (input.val() !== '')
            {
                result_input.val(input_display.val());
            }
            
            //ako je search polje unutar dialoga povecavamo z-index od resultata
            if (localThis.parents('.ui-dialog').length > 0)
            {                
                result.css({'z-index':parseInt(localThis.parents('.ui-dialog').css('z-index'))+100});
            }
            
            localThis.data('input', input);
            localThis.data('input_display', input_display);
            localThis.data('result', result);
            localThis.data('result_input', result_input);
            localThis.data('result_body', result_body);
            localThis.data('removeIcon', removeIcon);
            
            localThis.data('globalTimeout', null);
            localThis.data('alreadyclicked',false);
            localThis.data('refresh_timeout', null); //timeout koji se koristi kod refresh funkcije
            localThis.data('result_body_selected', false); //dodatna promenljiva koja oznacava da je fokus presao na result
            localThis.data('is_search_called', false);
            localThis.data('group', sima.uniqid()); //id grupe za koju se zovu ajax pozivi
            
            input_display.on('click', {}, function(event){
                showResult(localThis, true);
                event.stopPropagation();
                event.preventDefault();
            });
            
            input_display.on('keydown',{},function(event){
                if (event.which == 27) //esc
                {
                    hideResult(localThis);
                }
            });
            input_display.on('keyup',{},function(event){
                if (event.which == 9 || event.which == 27) //tab
                {
                    hideResult(localThis);
                }
                else
                {
                    if (localThis.data('input_display').val() !== '')
                    {
                        localThis.data('result_input').val(localThis.data('input_display').val());
                        showResult(localThis);
                    }
                    else
                    {
                        showResult(localThis, true);
                    }
                    event.preventDefault();
                }
            });
            input_display.on('focusin', function (e) {
                if ($(this).hasClass('_disabled') || $(this).parents('._disabled').length > 0)
                {
                    $(this).blur();
                }
            });
            
            removeIcon.on('click', {sf:localThis}, remove_icon);
            
            result.on('click','.sima-ui-sf-choice',{sf:localThis},choiceHandler);
            /**
             * ako se klikne van result-a on se sakriva
             */
            result.bind("mousedownoutside", function(event){                
                if (!input_display.is(event.target))
                {                    
                    hideResult(localThis);
                }
            });
            
            //setujemo initial value
            localThis.data('initialValue', localThis.data('input').val());
            localThis.data('initialValueDisplay', localThis.data('input_display').val());
        },
        setDisplayName : function(id)
        {
            var localThis = $(this);
            sima.ajax.get('base/model/getValueFromRelation',{
                get_params:{
                    model:localThis.data('model'),
                    relation:localThis.data('relation'),
                    id:id
                },
                success_function:function(response){
                    localThis.data('input_display').val(response.value);
                    localThis.data('result_input').val(response.value);
                }
            });
        },
        getFilter: function()
        {
            return $(this).data('filters');
        },
        setFilter : function(filter, prefix)
        {
            if (sima.isEmpty(prefix))
            {
                prefix = 'initial';
            }
            var old_filter = $(this).data('filters');
            old_filter[prefix] = filter;
        },
        refresh : function()
        {
            var _this = this;
            if (typeof _this.data('result') !== 'undefined')
            {
                var refresh_timeout = _this.data('refresh_timeout');
                if(refresh_timeout !== null)
                {
                    clearTimeout(refresh_timeout);
                    _this.data('refresh_timeout', refresh_timeout);
                }  
                _this.data('refresh_timeout', setTimeout(function(){
                    if (typeof _this.data('input_display') !== 'undefined' && typeof _this.data('result') !== 'undefined')
                    {
                        var left = _this.data('input_display').offset().left;
                        var top = _this.data('input_display').offset().top + _this.data('input_display').outerHeight();
                        _this.data('result').css({left: left + 'px', top: top + 'px'});
                    }
                },5));
            }
        },
        hideResult : function()
        {
            hideResult(this);
        },
        getInitialValue: function()
        {
            var localThis = $(this);
            return {
                id: localThis.data('initialValue'),
                display_name: localThis.data('initialValueDisplay')
            };
        },
        getValue : function()
        {
            var localThis = $(this);
            return {
                id:localThis.data('input').val(),
                display_name:localThis.data('input_display').val()
            };
        },
        setValue : function(value, display, call_onchange)
        {
            var localThis = $(this);            
            choice(localThis, {value:value,display:display}, call_onchange);
        },
        setValueWithoutEvents : function(value, display)
        {
            var localThis = $(this);            
            localThis.data('input').val(value);
            localThis.data('input_display').val(display);
            localThis.data('result_input').val(display);
        },
        removeDisable: function()
        {
            var localThis = $(this);
            localThis.data('input_display').prop("disabled", false);
            localThis.data('removeIcon').removeClass('_disabled');
        },
        setDisable: function()
        {
            var localThis = $(this);
            localThis.data('input_display').prop("disabled", true);
            localThis.data('removeIcon').addClass('_disabled');
        },
        onChangeFunc: function(func)
        {
            var localThis = $(this);
            localThis.data('onchange', func);
        },
        onRemoveFunc: function(func)
        {
            var localThis = $(this);
            localThis.data('onremove', func);
        },
        empty: function()
        {
            var localThis = $(this);
            empty(localThis);
        }
    };
    
    function showResult(localThis, select)
    {
        if (!localThis.data('result').is(':visible'))
        {
            localThis.data('result').show();
            //ako postoji vrednost onda input display setujemo na tu vrednost, inace je setujemo na prazno(sluzi u slucaju da smo otkucali veoma brzo nesta u input display-u)
            if (localThis.data('input').val() === '')
            {
                localThis.data('input_display').val('');
            }
            else
            {
                localThis.data('input_display').val(localThis.data('input_display').val());
                localThis.data('result_input').val(localThis.data('input_display').val());
            }
            localThis.simaSearchField('refresh');
            bindResultInputEvents(localThis, select);
            bindResultBodyEvents(localThis);
            SearchFunc({sf:localThis});
        }
        localThis.data('result_input').focus();
        localThis.data('result').simaPopup('refresh');
    }
    
    function hideResult(localThis)
    {
        if (typeof localThis.data('result') !== 'undefined' && localThis.data('result').length > 0)
        {
            localThis.data('result_body').empty();
            localThis.data('result').hide();
            if (localThis.data('input').val() == '')
            {
                localThis.data('input_display').val('');
                localThis.data('result_input').val('');
            }
        }
    }
    
    function scrollOnArrowDownUp(result_body, element)
    {
        var scrollTop = result_body.scrollTop();
        if (result_body.height() < element.position().top)
        {
            var scroll = scrollTop + element.outerHeight(true);
            result_body.scrollTop(scroll);
        }
        else if (scrollTop > 0)
        {
            var scroll = scrollTop - element.outerHeight(true);
            result_body.scrollTop(scroll);
        }
    }
    
    function SearchFunc(params)
    {
        var localThis = params.sf;
        var search_text = $.trim(localThis.data('result_input').val());
        var filters = $.extend(true, {}, localThis.data('filters'));

        var get_params = {};
        get_params.Model = localThis.data('model');
        if (typeof localThis.data('relation') !== 'undefined')
        {
            get_params.relation = localThis.data('relation');
        }        
        sima.ajax.get(localThis.data('action'),{
            get_params:get_params,
            data: {
                search_text: search_text,
                filters: filters
            },
            group: localThis.data('group'),
            async: true,
            loadingCircle: (localThis.data('result_body')),
            success_function:function(response){  
                if (sima.isEmpty(response.result))
                {
                    localThis.data('result_body').html(sima.translate('ResultSetIsEmpty'));
                }
                else
                {
                    localThis.data('result_body').html(response.result);
                }
                localThis.data('result_body').css({'height':'auto'});
                localThis.data('result').css({'height':'auto'});                
                localThis.data('result_body').scrollTop(0);
                localThis.data('is_search_called', false);
                localThis.data('result').simaPopup('refresh');
            }
        });
        
    }
    
    function choiceHandler(event)
    {        
        choice(event.data.sf, $(this));
    }
    
    function choice(localThis, element, call_onchange)
    {       
        var _call_onchange = (typeof call_onchange !== 'undefined')?call_onchange:true;
        var value = '';
        var display= '';        
        if (element instanceof jQuery)
        {
            value = element.data('value');
            display = element.data('display');
        }
        else
        {
            value = element.value;
            display = element.display;
        }        
        localThis.data('input').val(value);
        localThis.data('input_display').val(display);
        localThis.data('result_input').val(display);
        localThis.data('input_display').focus();
        hideResult(localThis);        
        if (typeof localThis.data('onchange') !== 'undefined' && _call_onchange === true)
        {
            callFunction(localThis.data('onchange'), value);
        }
        localThis.trigger("sf-choose", [{id:value}]);
        localThis.trigger("onValue", [{id:value}]);
    }
    
    function bindResultInputEvents(localThis, select)
    {
        unbindResultInputEvents(localThis);
        localThis.data('result_input').on('focus', function (e) {
            if (typeof select !== 'undefined')
            {
                $(this).one('mouseup', function () {
                    $(this).select();
                    return false;
                }).select();
            }
            else
            {
                localThis.data('result_input').caret(localThis.data('result_input').caret()+localThis.data('result_input').val().length);
            }
        });
        localThis.data('result_input').on('keydown', {sf:localThis}, function(event){
            if (event.which == 27) //esc
            {
                localThis.data('input_display').focus();
                hideResult(localThis);
                event.stopPropagation();
                event.preventDefault();
            }
            else if (event.which === 38 || event.which === 40) //down-up
            {
                event.stopPropagation();
            }
        });
        localThis.data('result_input').on('keyup', {sf:localThis}, function(event){
            localThis.data('result_input').unbind("focus");
            if (event.which == 40) //down
            {
                if (localThis.data('result_body').children().first().length > 0 && localThis.data('is_search_called') == false)
                {
                    localThis.data('result_body').focus();
                    localThis.data('result_body').children('.selected').each(function(){
                        $(this).removeClass('selected');
                    });
                    localThis.data('result_body').children().first().addClass('selected');
                    localThis.data('result_body_selected', true);
                }
                event.stopPropagation();
                event.preventDefault();
            }
            else if (event.which == 9)
            {
                event.stopPropagation();
                event.preventDefault();
            }
            else if (event.which !== 37 && event.which !== 38 && event.which !== 39)
            {
                localThis.data('is_search_called', true);
                var globalTimeout = localThis.data('globalTimeout');
                if(globalTimeout !== null) 
                {
                    clearTimeout(globalTimeout);
                    localThis.data('globalTimeout', globalTimeout);
                } 
                localThis.data('globalTimeout', setTimeout(function(){
                    if (!localThis.data('result_body_selected'))
                    {
                        SearchFunc({sf:localThis});
                    }
                },500));
            }
            else
            {
                event.stopPropagation();
            }
        });
    }
    
    function unbindResultInputEvents(localThis)
    {
        localThis.data('result_input').unbind("focus");
        localThis.data('result_input').unbind("keydown");
        localThis.data('result_input').unbind("keyup");
    }
    
    function bindResultBodyEvents(localThis)
    {
        unBindResultBodyEvents(localThis);
        localThis.data('result_body').on('keydown',{},function(event){
            if (event.which == 27) //esc
            {
                localThis.data('input_display').focus();
                hideResult(localThis);
                event.stopPropagation();
                event.preventDefault();
            } 
            else if (event.which === 38 || event.which === 40) //down-up
            {
                event.stopPropagation();
            }
        });
        localThis.data('result_body').on('keyup',{},function(event){
            if (event.which == 38) //up
            {
                var selected = localThis.data('result_body').children('.selected');                    
                if (selected.length > 0)
                {
                    if (selected.prev().length > 0)
                    {
                        selected.removeClass('selected');
                        selected.prev().addClass('selected');
                        scrollOnArrowDownUp(localThis.data('result_body'), selected.prev());
                    }
                    else
                    {
                        selected.removeClass('selected');
                        localThis.data('result_input').focus();
                        localThis.data('result_body_selected', false);
                    }
                }
                event.stopPropagation();
                event.preventDefault();
            }
            else if (event.which == 40) //down
            {
                var selected = localThis.data('result_body').children('.selected');
                if (selected.length > 0)
                {
                    if (selected.next().length > 0)
                    {
                        selected.removeClass('selected');
                        selected.next().addClass('selected');
                        scrollOnArrowDownUp(localThis.data('result_body'), selected.next());
                    }
                }
                else
                {
                    if (localThis.data('result_body').children().first().length > 0)
                    {
                        localThis.data('result_body').children().first().addClass('selected');
                    }
                }
                event.stopPropagation();
                event.preventDefault();
            }
            else if (event.which == 37 || event.which == 39) //left, right
            {
                event.stopPropagation();
                event.preventDefault();
            }
            else if (event.which == 13) //enter
            {
                var selected = localThis.data('result_body').children('.selected');
                if (selected.length > 0)
                {
                    choice(localThis, selected);
                }
                event.stopPropagation();
                event.preventDefault();
            }
        });
    }
    
    function unBindResultBodyEvents(localThis)
    {
        localThis.data('result_body').unbind("keydown");
        localThis.data('result_body').unbind("keyup");
        localThis.data('result_body_selected', false);
    }
    
    function remove_icon(event)
    {
        var localThis = event.data.sf;
        if (!localThis.find('.sima-ui-sf-remove').hasClass('_disabled'))
        {            
            empty(localThis);
            if (typeof localThis.data('onremove') !== 'undefined')
            {
                callFunction(localThis.data('onremove'));
            }
            else if (typeof localThis.data('onchange') !== 'undefined')
            {
                callFunction(localThis.data('onchange'));
            }
        }
    }
    
    function empty(localThis)
    {
        localThis.data('input').val('');
        localThis.data('input_display').val('');
        localThis.data('result_input').val('');
        
        localThis.trigger("sf-remove");
        localThis.trigger("onEmpty");
    }
    
    function callFunction(func, params)
    {
        if(typeof func === 'string')
        {
            eval(func);
        }
        else
        {
            sima.executeFunction(func, params);
        }
    }
    
    $.fn.simaSearchField = function(methodOrOptions) {
        var ar = arguments;
        var i=1;
        var ret = this;
        this.each(function() {
            i++;
            if (methods[methodOrOptions]) {
                var povr = methods[ methodOrOptions ].apply($(this), Array.prototype.slice.call(ar, 1));
                if (typeof povr !== 'undefined')
                {
                    ret = povr;
                }
                return;
            } else if (typeof methodOrOptions === 'object' || !methodOrOptions) {
                // Default to "init"
                return methods.init.apply($(this), ar);
            } else {
                $.error('Method ' + methodOrOptions + ' does not exist on jQuery.simaSearchField');
            }
        });
        return ret;
    };  
})( jQuery );