
<div id="<?php echo $this->id?>" class="sima-ui-sf">
    <input class="sima-ui-sf-input" type="text" name="<?php echo $name?>" value="<?php echo $real_value?>"/>

    <div class="sima-ui-sf-wrapper">
        <input class="sima-ui-sf-display" autocomplete="off" type="text" value="<?php echo htmlspecialchars($display_value, ENT_QUOTES, 'UTF-8'); ?>" 
            <?php
                foreach ($htmlOptions as $key => $value)
                {
                    if ($value != false)
                        echo $key.'="'.$value.'" ';                   
                }
            ?>/>
        <span class="sima-icon sima-ui-sf-remove <?php echo isset($htmlOptions['disabled'])?'_disabled':''; ?>"></span>
    </div>
</div>