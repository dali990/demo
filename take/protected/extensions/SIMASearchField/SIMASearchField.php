<?php

/**
  How to use:

  view:
  $this->beginWidget('ext.SIMASearchField.SIMASearchField',
  array(
    'model'=>$model,
    'attribute'=>$column,
    'relation'=>$relation,
    'filters' => $filters,
    'placeholder'=>$placeholder,
  ));
  $this->endWidget();

 */
class SIMASearchField extends CInputWidget {

    public $id = ''; //id searhField komponente
    public $relation = ''; //relacija u kojoj se pretrazuje
    public $filters = array(); //dodatni filteri
    public $action = '';
    public $htmlOptions = array();
    public $display_value = '';
    public $onchange = ''; //poziva se kada se odabere neka vrednost iz rezultata
    public $onremove = ''; //poziva se kada se klikne na ikonicu obirsi
    public $name = '';
    public $real_value = '';
    
    public function run() 
    {
        if (empty($this->id)) 
            $this->id = SIMAHtml::uniqid();
        
        $model = $this->model;
        if (!is_object($model))
        {
            $model = $model::model();
        }
        $filters = $this->filters;
        
        if ($this->relation != '')
        {
            $rel_arr = explode('.', $this->relation);
            $curr_model = $model;
            $_breaked = false;
            foreach ($rel_arr as $relation)
            {
                if (isset($curr_model->$relation) && is_object($curr_model->$relation))
                    $curr_model = $curr_model->$relation;
                else
                {
                    $_breaked = true;
                    break;
                }
            }
            if ($_breaked)
            {
                $display_value = '';
                $real_value = '';
            }
            else
            {
                $display_value = $curr_model->DisplayName;
                $real_value = $curr_model->id;
            }
            $relations = $model->relations();
            $settings = $relations[$relation][1]::model()->modelSettings('searchField');
            $name = get_class($model)."[".str_replace('.', '][', $relation)."][ids]";
        }
        else
        {
            $display_value = (isset($model) && isset($model->id))?$model->DisplayName:'';
            $real_value = (isset($model) && isset($model->id))?$model->id:'';
            $settings = $model->modelSettings('searchField');
            $name = get_class($model);
        }        
        if ($this->action != '')
        {
            $action = $this->action;
        }
        else if (isset($settings['action']))
        {
            $action = $settings['action'];
        }        
        else
        {
            $action = Yii::app()->params['searchFieldAction'];
        }
        if ($this->onchange !== '')
        {
            $onchange = $this->onchange;
        }
        if ($this->onremove !== '')
        {
            $onremove = $this->onremove;
        }
        
        echo $this->render('index',array(
            'name' => ($this->name==='')?$name:$this->name,
            'real_value' => ($this->real_value === '')?$real_value:$this->real_value,
            'display_value' => ($this->display_value === '')?$display_value:$this->display_value,
            'htmlOptions' => $this->htmlOptions
        ));
        
        $json = json_encode(
            array(
                'action' => $action,
                'model' => get_class($model),
                'relation' => $this->relation,
                'filters' => $filters,
                'onchange' => isset($onchange)?$onchange:'',
                'onremove' => isset($onremove)?$onremove:''
            )
        );
        
        $register_script = "$('#".$this->id."').simaSearchField('init',$json);";
        Yii::app()->clientScript->registerScript(SIMAHtml::uniqid(), $register_script, CClientScript::POS_READY);
    }

    public function registerManual() {
        $assets = dirname(__FILE__) . '/assets';
        $baseUrl = Yii::app()->assetManager->publish($assets);
        Yii::app()->clientScript->registerScriptFile($baseUrl . '/jquery.simaSearchField.js', CClientScript::POS_HEAD);
        $css_file = $baseUrl . '/simasearchfield.css';
        Yii::app()->clientScript->registerCssFile($css_file);
    }

}
