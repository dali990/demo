<?php

class Stapler
{
    private $stapler_location;
    
    public function init()
    {
        $this->stapler_location = dirname(__FILE__).'/stapler/';
    }
    
    public function createTempPDF(array $document_pages)
    {
        if(count($document_pages) === 0)
        {
            throw new Exception('document pages empty');
        }
        
        $command = $this->stapler_location.'stapler sel';
        
        foreach($document_pages as $document_page)
        {
            $temp_name = $document_page['temp_name'];
            $page_number = $document_page['page_number'];
            
            $tempFile = TemporaryFile::GetByName($temp_name);
            $tempFileFullPath = $tempFile->getFullPath();
                        
            $pdf_temp_path = $tempFileFullPath.'.pdf';
            if(!file_exists($pdf_temp_path))
            {
                copy($tempFileFullPath, $pdf_temp_path);
            }
            
            $command .= ' "'.$pdf_temp_path.'" '.$page_number;
        }
        
        $resultTempFile = new TemporaryFile('', null, 'pdf');
        $result_full_path = $resultTempFile->getFullPath();
        $resultTempFile->delete();
        
        $command .= ' '.$result_full_path;
        $command .= ' 2>&1';
        
        $command_output = null;
        Yii::app()->linuxCommandExecutor->executeCommand($command, Yii::app()->params['linuxcommandExecutor_timeout_s_Stapler_createTempPDF'], $command_output);
        
        $allowed_command_result_lines = [
            'PdfReadWarning: Multiple definitions in dictionary at byte 0x3b008 for key /Info [generic.py:588]',
            'PdfReadWarning: Multiple definitions in dictionary at byte 0x3b014 for key /Info [generic.py:588]',
            'PdfReadWarning: Multiple definitions in dictionary at byte 0x3b020 for key /Info [generic.py:588]',
            'PdfReadWarning: Multiple definitions in dictionary at byte 0xb8a7 for key /PageMode [generic.py:588]',
            'PdfReadWarning: Multiple definitions in dictionary at byte 0x2a770 for key /PageMode [generic.py:588]',
        ];
        
        if(!empty($command_output) && !empty(array_diff($command_output, $allowed_command_result_lines)))
        {
            throw new Exception(Yii::t('Stapler.Translation', 'ErrorCreatingPDF', [
                '{command_output}' => implode("\n", $command_output)
            ]));
        }
        
        return $resultTempFile;
    }
}
