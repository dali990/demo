<?php

class SIMALinuxCommandExecutorTimeoutException extends \SIMAException
{
    public function __construct($command)
    {
        $message = 'command "'.$command.'" timed out';
        parent::__construct($message);
    }
}

