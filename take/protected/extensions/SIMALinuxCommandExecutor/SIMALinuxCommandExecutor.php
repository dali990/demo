<?php

class SIMALinuxCommandExecutor
{
    public $default_timeout_seconds = 30;
    
    public function init()
    {
    }
    
    /**
     * 
     * @param string $command - komanda za izvrsavanje
     * @param int $timeout_seconds - max vreme izvrsavanja komande; ako se prosledi 0, nema timeout-a
     * @return array - rezultat izvrsavanja komande
     * @throws SIMALinuxCommandExecutorTimeoutException
     */
    public function executeCommand($command, $timeout_seconds=null, &$output=null, &$return_var=null)
    {        
        if(is_null($timeout_seconds))
        {
            $timeout_seconds = $this->default_timeout_seconds;
        }
        
        $final_command = $command;
        if($timeout_seconds > 0)
        {
            $final_command  = 'timeout '.$timeout_seconds.'s '.$command;
        }
        
        exec($final_command, $output, $return_var);
        
        if($return_var === 124)
        {
            throw new SIMALinuxCommandExecutorTimeoutException($command);
        }
    }
}
