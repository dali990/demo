<?php

/**
 * komponenta koja vodi racuna o licenci korisnika
 */

class SIMALicenceManager
{
    public $master_licences = [];
    public $personal_licences = [];
    public $shared_licences = 0;
    
    public function init()
    {
        $this->reload();
    }
    
    public function reload()
    {
        $this->master_licences = [];
        $this->personal_licences = [];
        $this->shared_licences = 0;
        
        $master_admin_licence_conf_table_exists_command_result = Yii::app()->db->createCommand("
            SELECT EXISTS (
                SELECT 1
                FROM   information_schema.tables
                WHERE  table_schema = 'base'
                AND    table_name = 'master_admin_licence_conf'
            );
        ")->queryAll();
        $table_exists_result = $master_admin_licence_conf_table_exists_command_result[0]['exists'];
        
        if($table_exists_result === true)
        {
            $master_admin_licence_conf_rows = Yii::app()->db->createCommand("select * from base.master_admin_licence_conf")->query();
            foreach($master_admin_licence_conf_rows as $master_admin_licence_conf_row)
            {
                $master_admin_licence_conf_row_type = $master_admin_licence_conf_row['type'];
                $master_admin_licence_conf_row_count = $master_admin_licence_conf_row['count'];
                $master_admin_licence_conf_row_username = $master_admin_licence_conf_row['username'];
                
                if($master_admin_licence_conf_row_type === Session::$LICENCE_TYPE_MASTER)
                {
                    $this->master_licences[] = $master_admin_licence_conf_row_username;
                }
                else if($master_admin_licence_conf_row_type === Session::$LICENCE_TYPE_PERSONAL)
                {
                    $this->personal_licences[] = $master_admin_licence_conf_row_username;
                }
                else if($master_admin_licence_conf_row_type === Session::$LICENCE_TYPE_SHARED)
                {
                    $this->shared_licences = $master_admin_licence_conf_row_count;
                }
                else
                {
                    throw new Exception('invalid row: '.SIMAMisc::toTypeAndJsonString($master_admin_licence_conf_row));
                }
            }
        }
        else
        {
            if(isset(Yii::app()->params['master_licences']))
            {
                $this->master_licences = Yii::app()->params['master_licences'];
            }
            if(isset(Yii::app()->params['personal_licences']))
            {
                $this->personal_licences = Yii::app()->params['personal_licences'];
            }
            if(isset(Yii::app()->params['shared_licences']) && is_numeric(Yii::app()->params['shared_licences']))
            {
                $this->shared_licences = Yii::app()->params['shared_licences'];
            }
        }
    }
    
    public function licenceUrl()
    {
        return array('user/checkLicence');
    }
    
    /**
     * Proverava da li ima slobodna licenca za trenutno ulogovanog korisnika
     * @return boolean
     */
    public function checkLicence(Session $session)
    {
        if ($this->hasAlreadyLicence($session))
        {
            return true;
        }
        // ako korisnik ima master licencu, samo pustiti
        if(in_array($session->user->username, $this->master_licences))
        {
            $session->licence_type = 'master';
            $session->update();
            return true;
        }
        //ako korisnik ima personalnu licencu i ako je aktivna samo jedna sesija tog korisnika omoguci logovanje inace proveri shared licencu
        if($this->checkLicencePersonal($session, true))
        {
            return true;
        }
        //korisnik koji ima licnu licencu, moze da iskoristi dodatnu deljenu licencu
        if ($this->hasFreeSlot())
        {
            $session->licence_type = 'shared';
            $session->update();
            return true;
        }
        
        $session->licence_type = 'unlicenced';
        $session->update();
        return false;
    }
    
    public function userHaveDefinedPersonalLicence(User $user)
    {
        return in_array($user->username, $this->personal_licences);
    }
    
    /**
     * proverava da li sesija moze postati licna
     * po potrebi se i modifikuje u licnu
     * 
     * @param Session $session
     * @param type $update
     * @return boolean
     */
    public function checkLicencePersonal(Session $session, $update = false)
    {
        if ($this->userHaveDefinedPersonalLicence($session->user))
        {
            $personal_sessions_count = Session::model()
                ->online()
                ->personalLicence()
                ->count([
                    'model_filter' => [
                        'user' => [
                            'ids' => $session->user->id
                        ]
                    ]
                ]);
            
            if($personal_sessions_count == 0)
            {
                if($update === true)
                {
                    $session->licence_type = 'personal';
                    $session->update();
                }
                return true;
            }
        }
        return false;
    }
    
    /**
     * 
     * @return boolean
     */
    private function hasFreeSlot()
    {
        $count = ActiveSession::model()->count("licence_type='shared'");
        return ($count < $this->shared_licences);
    }
    
    /**
     * da li je vec ulogovan
     * @return boolean
     */
    private function hasAlreadyLicence(Session $session)
    {
        if ($session==null)
        {
            return false;
        }
        return ($session->licence_type == 'personal' || $session->licence_type == 'shared');
    }
}
