<?php

/**
 * @package SIMA
 * @subpackage Base
 */
class SIMAActiveRecordGUI extends CActiveRecordBehavior
{
    //tip ikonice modela
    public static $MODEL_DISPLAY_ICON_TYPE_IMG = 'IMG'; //ako je tip img onda se dodatno salje src
    public static $MODEL_DISPLAY_ICON_TYPE_ICON = 'ICON'; //ako je tip ikonica, dodatno se salje code ikonice i to je naziv modela

    /**
     * niz koji predstavlja imena kolona array(key=>value)
     * -- ova odgovornost je prebacena iz SIMAActiveRecord
     * @return array 
     */
    public function columnLabels()
    {        
        return array(
            'partner_id' => 'Partner',
            'account_id' => 'Konto',
            'payment_date' => 'Datum plaćanja',
            'amount' => 'Iznos',
            'value' => 'Vrednost',
            'value_per_unit' => 'Jedinična vrednost',
            'date' => 'Datum',
//            'filing_number' => 'Zavodni Broj',
            'name' => Yii::t('BaseModule.Common', 'Name'),
            'description' => Yii::t('BaseModule.Common','Description'),
            'comment' => Yii::t('BaseModule.Common','Comment'),
            'registry_id' => 'Registrator',
            'confirm1' => 'Potvrda1',
            'confirm2' => 'Potvrda2',
            'confirmed' => 'Potvrđeno',
            'subject' => 'Predmet',
//            'belongs' => "Pripadnost<br /><span style='font-size:11px;'>(posao/ponuda/...)</span>",
            'belongs' => Yii::t('BaseModule.Common','Belongs'),
            'DisplayName' => Yii::t('BaseModule.Common', 'Name'),
            'DisplayHtml' => Yii::t('BaseModule.Common', 'Name'),
            'display_name' => Yii::t('BaseModule.Common', 'Name'),
            'type'=>Yii::t('BaseModule.Common', 'Type'),
            'ids'=>$this->modelLabel()
        );
    }
    
    /**
     * Returns name of Class 
     * @param boolean $plural default false - return plural form
     * @return text
     */
    public function modelLabel($plural = false)
    {
        $owner = $this->owner;
        return get_class($owner);
    }
    
    protected function getDisplayHtmlText()
    {
        return $this->owner->DisplayName;
    }
    
    public function getDisplayHtml($params=array())
    {
        $uniq = SIMAHtml::uniqid();
        $owner = $this->owner;
        $model_name = get_class($owner);
        
        $ret = '';
        
        if (isset($params['display_text']))
        {
            $display_text = $params['display_text'];
        }
        else
        {
            $display_text = $this->DisplayHtmlText;
        }
        
        $has_display_html_wrap = false;
        if (!empty($owner->hasDisplay()))
        {
            $display_model = $owner->getDisplayModel();
            if(!is_null($display_model))
            {
                $path = 'defaultLayout';
                $path_id = CJSON::encode([
                    'model_name' => get_class($owner->getDisplayModel()),
                    'model_id' => $owner->id
                ]);
                $_params = CJSON::encode([
                    'displayhtml' => true
                ]);
                $display_text = "
                    <span 
                        class='sima-model-link' 
                        onclick='
                            var _this = $(this); _this.addClass(\"_disabled\"); setTimeout(function(){_this.removeClass(\"_disabled\");},500);
                            var params=$_params; params.event = event; 
                            sima.dialog.openModel(\"$path\",$path_id, null, params);
                        '
                    >$display_text</span>";
                $has_display_html_wrap = true;
            }
        }
        else if (!empty($owner->path2))
        {
            $path = $owner->path2;
            $path_id = $owner->id;
            $_params = CJSON::encode([
                'displayhtml' => true
            ]);
            $display_text = "
                <span 
                    class='sima-model-link' 
                    onclick='
                        var _this = $(this); _this.addClass(\"_disabled\"); setTimeout(function(){_this.removeClass(\"_disabled\");},500);
                        var params=$_params; params.event = event; 
                        sima.dialog.openModel(\"$path\",$path_id, null, params);
                    '
                >$display_text</span>";
            $has_display_html_wrap = true;
        }
        
        if (isset($params['view']))
        {
            $info_box_view = $params['view'];
        }
        else if (!empty($this->infoBoxView))
        {
            $info_box_view = $this->infoBoxView;
        }

        if (isset($info_box_view))
        {
            $ret = SIMAHtml::spanWithInfoBox($display_text, 'base/model/renderInfoBox', [
                'model'=>$model_name,
                'model_id'=>$owner->id,
                'view'=>$info_box_view
            ]);
        }
        else if ($has_display_html_wrap === true)
        {
            $display_html_class = !empty($params['display_html_class']) ? ' '.$params['display_html_class'] : '';
            $ret = "<span class='sima-display-html$display_html_class'>$display_text</span>";
        }
        else
        {
            $ret = $display_text;
        }
        
        return $ret;
    }
    
    public function DisplayHtml($params=array())
    {
        return $this->getDisplayHtml($params);
    }

    private function isColumnStatus($column)
    {
        $statuses = $this->owner->modelSettings('statuses');
        return (isset($statuses[$column]));
    }
    
    /**
     * Funkcija koja vraca propertie pripremljene za vue
     * @param string $prop
     * @param array $scopes
     * @return array
     */
    public function vueProp(string $prop, array $scopes):array
    {
        $owner = $this->owner;
        
        //PROVERA DA LI JE RELACIJA
        $_relations = $owner->relations();
        if (isset($_relations[$prop]) 
                && 
                (
                    ($_relations[$prop][0]=== SIMAActiveRecord::HAS_MANY)
                    ||
                    ($_relations[$prop][0]=== SIMAActiveRecord::MANY_MANY)
                )
            )
        {
            if (
                    isset($_relations[$prop]['scopes'])
                    ||
                    isset($_relations[$prop]['condition'])
                )
            {
                /**
                 * MilosS(15.3.2019)
                 * ako se u relaciji nalazi scopes ili condition, neophodno je napraviti custom za vueProp za tu relaciju
                 * fora je sto stavka koja ne zadovoljava scope, promenom nekog atributa moze da upadne u scope,
                 * a nece se lista updateovati. 
                 * lepse resenje je da se navede koja je base relation za konkretnu relaciju, ali ne znam gde bi to naveli
                 * base relation je relacija koja sadrzi sve stavke koje mogu da upadnu u navedenu relaciju promenom svojih atributa
                 */
                error_log(__METHOD__.' - MilosS: PROGRAMERU! napravi custom funkciju u vueProp za relaciju '.$prop.' - primer u kodu');
                /**
                 * PRIMER kako treba napisati gde je base relation -> bill_items :
                    case 'services_bill_items':
                        $prop_value = [
                            'TYPE' => 'MODEL_ARRAY',
                            'ARRAY' => SIMAMisc::getVueModelTags($owner->$prop(['scopes' => $scopes]))
                        ];
                        $update_tags = SIMAMisc::getModelTags($owner->bill_items);// bill_items je base_relation
                        return [$prop_value,$update_tags];
                 */
            }
            if (empty($scopes))
            {
                $prop_value = [
                    'TYPE' => 'MODEL_ARRAY',
                    'ARRAY' => SIMAMisc::getVueModelTags($owner->$prop)
                ];
                $update_tags = [];
            }
            else
            {
                $prop_value = [
                    'TYPE' => 'MODEL_ARRAY',
                    'ARRAY' => SIMAMisc::getVueModelTags($owner->$prop(['scopes' => $scopes]))
                ];
                $update_tags = SIMAMisc::getModelTags($owner->$prop);
            }
            return [$prop_value,$update_tags];
        }
        
        switch ($prop)
        {
            case 'model_comment_thread_tags':
                return [SIMAMisc::getVueModelTags(Yii::app()->commentsManager->getModelCommentThreads($owner, $scopes)), []];
            case 'display_icon':
                return [[
                    'type' => SIMAActiveRecordGUI::$MODEL_DISPLAY_ICON_TYPE_ICON,
                    'code' => get_class($owner)
                ], SIMAMisc::getModelTags($owner)];
            case 'status_access':
                $error_messages = $owner->checkModelStatusAccessConfirmOrRevert($scopes['status_column'], $scopes['is_revert']);
                return [$error_messages, SIMAMisc::getModelTags($owner)];
            default: return [$this->vuePropValue($prop),[]];
        }
    }
    
    public function vuePropValue($prop)
    {
        $owner = $this->owner;

        if ($prop === 'update_model_tag')
        {
            //odmah radimo sa nizom
            return [SIMAHtml::getTag($owner)];
        }
        elseif ($prop === 'modelLabel')
        {
            return $owner->modelLabel();
        }
        else if ($prop === 'module_name')
        {
            return $owner->moduleName();
        }
        else if ($prop === 'has_display')
        {
            return $owner->hasDisplay();
        }
        else if ($prop === 'display_action')
        {
            return $owner->getDisplayAction();
        }
        else if ($prop === 'path2')
        {
            return $owner->path2;
        }
        else if ($prop === 'model_options')
        {
            return $owner->getModelOptions(Yii::app()->user->model);
        }
        else if ($prop === 'model_options_data')
        {
            return [
                'qrcode' => SIMAHtmlButtons::DownloadQRCodeWidgetAction($owner, true),
                'cache_reset' => SIMAHtmlButtons::CacheResetWidgetAction($owner),
                'cache_devalidate' => SIMAHtmlButtons::CacheDevalidateWidgetAction($owner)
            ];
        }
        else if ($prop === 'default_layout_model_tabs')
        {
            return $this->getDefaultLayoutModelTabs();
        }
        else if ($prop === 'comment_threads_with_not_system_comments_count')
        {
            return $owner->getAllCommentThreadsWithNotSystemCommentsCount();
        }
        else if ($prop === 'not_system_comments_count')
        {
            return $owner->getAllNotSystemCommentsCount();
        }
        else if ($prop === 'default_layout_model_warns')
        {
            return Yii::app()->warnManager->renderWarnsForModel($owner);
        }
//        else if($prop === 'basic_attributes')
//        {
//                $result = [];
//                $columns = $owner->getMetaData()->tableSchema->columns;
//                $column_names = array_keys($columns);
//                foreach ($column_names as $column_name)
//                {
//                    $result[] = [
//                        'label' => $owner->getAttributeLabel($column_name),
//                        'value' => $owner->$column_name
//                    ];
//                }
//                return $result;
//        }
        
        $prop_value = $owner->$prop;
        if ($this->isColumnStatus($prop))
        {
            $statuses = $owner->modelSettings('statuses');
            $columns = $owner->getMetaData()->tableSchema->columns;

            $timestamp = isset($statuses[$prop]['timestamp']) ? $statuses[$prop]['timestamp'] : null;
            $user = isset($statuses[$prop]['user']['relName']) ? $statuses[$prop]['user']['relName'] : null;
            $tooltip = ($owner->$prop) && isset($owner->$timestamp) 
                ? 
                "{$statuses[$prop]['title']} " .date("d.m.Y. H:i", strtotime($owner->$timestamp)) . (isset($owner->$user) ? ' - ' . $owner->$user->DisplayName : '')
                : 
                $statuses[$prop]['title'];
            
            $prop_value = [
                'column' => $prop,
                'value' => $prop_value,
                'title' => $tooltip,
                'model' => get_class($owner),
                'model_id' => $owner->id,
                'allow_null' => $columns[$prop]->allowNull ? true : false
            ];
        }
        elseif (is_null($prop_value))
        {
            $prop_value = '';
        }
        elseif (is_object($prop_value) && !is_null ($prop_value) && $prop_value instanceof SIMAActiveRecord)
        {
            $prop_value = [
                'TYPE' => 'MODEL',
                'TAG' => SIMAMisc::getVueModelTag($prop_value)
            ];
        }
        else if ($prop_value instanceof SIMABigNumber)
        {
            $prop_value = $prop_value->getValue();
        }
        
        return $prop_value;
    }

    /**
     * za zadatu kolonu vraca njen ulepsan izgled ili deo html-a
     * @param string $column
     * @return string
     */
    public function columnDisplays($column)
    {
        $_trace_cat = 'SIMA.SIMAActiveRecordGUI.columnDisplays';
        $owner = $this->owner;
        
        $statuses = $owner->modelSettings('statuses');
        if (isset($statuses[$column]))
        {
            return SIMAHtml::status($owner, $column);
        }
        
        $number_fields = $owner->modelSettings('number_fields');
        if (in_array($column, $number_fields))
        {
            return SIMAHtml::modelNumberFormat($owner, $column);
        }
        
        $columns = $owner->getMetaData()->tableSchema->columns;
        if (isset($columns[$column]) && $columns[$column]->dbType === 'boolean')
        {
            return ($owner->$column) ? 'Da' : 'Ne';
        }
        
        switch ($column)
        {
            case 'statuses':
            {                
                $result = '';
                
                $statuses_keys = array_keys($statuses);
                foreach ($statuses_keys as $status_key)
                {
                    Yii::beginProfile('statuses',$_trace_cat);
                    $result .= SIMAHtml::status($owner, $status_key);
                    Yii::endProfile('statuses',$_trace_cat);
                }
                return $result;
            }
            case 'belongs':
                {
                    $model = null;
                    if (isset($owner->file))
                    {
                        $model = $owner->file;
                    }
                    if (get_class($owner) == 'File')
                    {
                        $model = $owner;
                    }
                    if ($model == null)
                    {
                        return "";
                    }
                    return Yii::app()->controller->renderModelView($model,'belongsList');
                }
            case 'display_name': return $owner->DisplayHTML;
            default: //pokusava da nadje relaciju i ukoliko postoji, uzima DisplayName
                {
                    $relations = $owner->relations();
                    foreach ($relations as $key => $relation)
                    {
                        if  (
                                ( isset($relation[0]) && isset($relation[2]) )
                                &&
                                ( $relation[0] == $owner::BELONGS_TO || $relation[0] == $owner::HAS_ONE) 
                                &&
                                ( $relation[2] == $column || $column == $key )
                            )
                        {
                            if (isset($owner->$key))
                            {
                                if(is_object($owner->$key) && !is_null($owner->$key))
                                {
                                    return $owner->$key->DisplayHtml;
                                }
                                else
                                {
                                    Yii::app()->errorReport->createAutoClientBafRequest("Za model ".get_class($owner)." i relaciju $key php isset kaze da je setovan, ali u stvari nije.");
                                }
                            }
                            else
                            {
                                return  '??????';
                            }
//                                ("$owner -> $key") : '??????';
                        }
                    }
                    return $owner->$column;
                }
        }
    }

    /**
     * Spisak pogleda po modelu 
     * @TODO: opisati sintaksu zadavanja
     * SASA A. - OBAVEZNO JE ZADAVANJE VIEW TAKO DA NAZIV VIEW-A BUDE KEY U NIZU (npr. 'neki_view'=>[])
     * @return array
     */
    public function modelViews()
    {
        return array(
            'basicInfo'=>[],
            'title' => ['view_path' => 'base.views.model.default_title'],
            'options' => ['view_path' => 'base.views.model.default_options'],
            'basic_info'=>['class'=>'basic_info', 'view_path' => 'base.views.model.default_basic_info']
        );
    }
    
    /**
     * Dohvata dodatne view-ove za prosledjeni $view koji su rasporedjeni po modulima
     * @param type $view
     * @return type
     */
    public function getAdditionalModelViews($view)
    {
        $model = $this->owner;
        $event = new SIMAEvent();
        $event->params['view'] = $view;
        $model->onGetViews($event);
        $additional_model_views_html = '';
        $results = [];
        foreach ($event->getResults() as $result)
        {
            foreach ($result as $value)
            {
                array_push($results, $value);
            }
        }

        usort($results, function($a, $b)
        {
            if (isset($a['order']) && isset($b['order']))
            {
                return intval($a['order']) > intval($b['order']);
            }
            else if (isset($a['order']))
            {
                return -1;
            }
            else if (isset($b['order']))
            {
                return 1;
            }
            else 
            {
                return 0;
            }
        });

        foreach ($results as $result)
        {
            if ($result['view'] === $view)
            {
                $additional_model_views_html .= $result['html'];
            }
        }
        
        return $additional_model_views_html;
    }
    
    public function getInfoBoxView()
    {
        return null;
    }
    
    public function modelForms()
    {
        $attributes = $this->owner->attributeNames();
        $columns = array('name'=>'textField');
        if (in_array('comment',$attributes))
        {
            $columns += array('comment'=>'textArea');
        }
        if (in_array('description',$attributes))
        {
            $columns += array('description'=>'textArea');
        }
        if (in_array('parent_id',$attributes))
        {
            $columns += array('parent_id'=>array('dropdown','relName'=>'parent'));
        }
        return array(
            'default'=>array(
                'type'=>'generic',
                'columns'=>$columns
            )
        );
    }

//    public function columnOrder($type)
//    {
//        switch ($type)
//        {
//            default: return array(
//                'name','description'
//            );
//        }
//    }

    /**
     * koristi se kod polja koja su strani kljuc neke druge tabele
     * vraca niz uredjenoih parova $key=>$value 
     * $key predstavlja id reda iz strane tabele
     * $value predstavlja DisplayName te strane tabele
     * @param string $key
     * @param string $relName
     * @param CDBCriteria/array $filter - moze biti ili cdbCriteria ili model_filter
     * @return array
     */
    public function droplist($key, $relName='', $filter=null)
    {        
        switch ($key)
        {
            case 'confirmed': case 'confirm1': case 'confirm2': return array('false' => 'Nije', 'true' => 'Jeste');
            default:
                {
                    $relations = $this->owner->relations();
                    $owner = $this->owner;
                    if ($relName!=='' && isset($relations[$relName]))
                    {
                        return  $relations[$relName][1]::model()->getModelDropList($filter);
                    }

                    foreach ($relations as $relation)
                    {
                        if (isset($relation[2]) && $relation[2] == $key && $relation[0] == $owner::BELONGS_TO)
                        {
                            return $relation[1]::model()->getModelDropList($filter);
                        }
                    }
                    return array('1' => 'prazno');
                }
        }
    }

    /**
     * funkcija koja vraca spisak redova iz tabele koji mogu posluziti kao strani kljuc
     * moze se redefinisati kod nasledjenih modela
     * @return array
     */
    protected function getModelDropList($filter = null)
    {
        $owner = $this->owner;
//        $_cashed=FALSE;
        $criteria = null;
        if ($filter instanceof CDBCriteria)
        {
            $criteria = $filter;
        }
        else if (is_array($filter))
        {
            $criteria = new SIMADbCriteria([
                'Model'=>get_class($owner),
                'model_filter'=>$filter
            ]);
        }
        
//        $_cache_key = 'SIMAAtiveRecordGUI_getModelDropList_'.  get_class($owner) . $owner;
//        if (is_null($criteria))
//        {
//        
//            $_cashed = Yii::app()->cache->get($_cache_key);
//        }
        
//        if ($_cashed===FALSE)
        {
            $_cashed = array();
            $Model = $owner::model();
            $scopes = $this->owner->scopes();
            $order_scope = $Model->modelSettings('default_order_scope');
            if (!empty($order_scope))
            {
                $Model = $Model->$order_scope();
            }
            elseif (isset($scopes['recently']))
            {
                $Model = $Model->recently();
            }
            elseif (isset($scopes['byName']))
            {
                $Model = $Model->byName();
            }
            
            //privremeni bugfix(treba u yii frameworku da se ispravi merge)
            $criteria_for_findall = $criteria;
            if (is_null($criteria_for_findall))
            {                
                $criteria_for_findall = '';
            }            
            $models = $Model->findAll($criteria_for_findall);
            
            foreach ($models as $model)
            {
                $_cashed+=array($model->id => $model->SearchName);
            }
            
//            if (is_null($criteria))
//            {
//                Yii::app()->cache->set($_cache_key,$_cashed);
//            }
        }
        return $_cashed;
    }
    
    /**
     * vraca niz podesavanja kolona tabele za prosledjen tip kolona
     * @param string $type
     */
    public function guiTableSettings($type) {
        $owner = $this->owner;
        $columns = array();
        $columnOrder = $owner->columnOrder($type);
        if (isset($columnOrder))
        {
            $columns = $columnOrder;
        }
        $sums = $owner->modelSettings('sum');
        if (gettype($sums) === 'string')
        {
            $sums = explode(',', $sums);
        }
        $order_by = $owner->modelSettings('order_by');
        $guiTable = $owner->modelSettings('GuiTable');
        
        switch ($type)
        {
            default:
                return array(
                    'columns' => $columns,
                    'filters'=>array(),
                    'sums' => $sums,
                    'order_by'=>$order_by,
                    'GuiTable'=>$guiTable,
                );
        }
    }
    
    public function modelSearchViews()
    {
        return array('guiTable'=>'guiTable', 'simple'=>'simple');
    }
    
    private function cacheClears()
    {
        Yii::app()->cache->delete('SIMAAtiveRecordGUI_getModelDropList_'.  $this->owner->tableName());
    }
    
    public function afterSave($event)
    {
        $this->cacheClears();
    }
    
    public function afterDelete($event)
    {
        $this->cacheClears();
    }
    
    public function afterValidateDependencyError(array $foreign_dependencies)
    {
        if(empty($foreign_dependencies))
        {
            return;
        }
        
        if(Yii::app()->isWebApplication())
        {
            $dependencies_text = '<ul>';
            foreach($foreign_dependencies as $foreign_dependency)
            {
                $foreign_dependency_splited = explode(':', $foreign_dependency);
                $foreign_dependency_table = $foreign_dependency_splited[0];
                $foreign_dependency_column = $foreign_dependency_splited[1];
                                
                $dependencies_text .= '<li>';
                
                $configTablesToModelsMapper = SIMAModule::getTablesToModelsMapped();
                if(isset($configTablesToModelsMapper[$foreign_dependency_table]))
                {
                    $model = $configTablesToModelsMapper[$foreign_dependency_table];
                    $dependencies_text .= $model::model()->modelLabel(true);
                    $dependencies_text .= ' ('.$model::model()->getAttributeLabel($foreign_dependency_column).')';
                }
                else
                {
                    error_log(__METHOD__.' - not mapped table to model: '.$foreign_dependency_table);
                    $dependencies_text .= $foreign_dependency_table;
                }
                $dependencies_text .= '</li>';
            }
            $dependencies_text .= '</ul>';

            $dependencies_text = '<div class="sima-ui-warning-exception-used-tables-list">'
                    .'<hX>'.Yii::t('SIMAActiveRecord', 'UsedInTables').':</hX>'
                        .$dependencies_text
                    .'</div>';

            $message = Yii::t('SIMAActiveRecord', 'ModelIsUsedInOtherTables', [
                '{model_label}' => $this->owner->modelLabel(),
                '{error_message}' => $dependencies_text
            ]);
        }
        else
        {
            $message  = "Model ".$this->owner->modelLabel()." is used in other tables:\n";
            foreach($foreign_dependencies as $foreign_dependency)
            {
                $message .= "\t$foreign_dependency\n";
            }
        }
        
        throw new SIMAWarnException($message);
    }
    
    public function getSumAttributeDisplay($column)
    {
        return $this->owner->$column;
    }
    
    public function hasDisplay()
    {
        return false;
    }
    
    public function getDisplayAction()
    {
        return $this->owner->path2;
    }
    
    public function getColorClasses()
    {
        return '';
    }
    
    public function getSettings($setting_code = null)
    {
        $owner = $this->owner;

        $event = new SIMAEvent();
        $owner->onGetSettings($event);

        $settings = [];
        foreach ($event->getResults() as $result)
        {
            $settings = array_merge_recursive($settings, $result);
        }
        
        $return = null;
        
        if (empty($setting_code))
        {
            $return = $settings;
        }
        else if (isset($settings[$setting_code]))
        {
            $return = $settings[$setting_code];
        }
        
        return $return;
    }
    
    private function getDefaultLayoutModelTabs($ignore_tabs = [])
    {
        $owner = $this->owner;

        if($owner->hasTabs)
        {
            $ModelTabs = $owner->TabsBehavior;
            $owner->attachBehavior('Tabs', new $ModelTabs());
            $model_tabs = $owner->listTabs();
        }

        $tabs = [];

        foreach ($model_tabs as $tab)
        {
            if (!in_array($tab['code'], $ignore_tabs))
            {
                $this->repackDefaultLayoutModelTabs($tab);
                $tabs[] = $tab;
            }
        }
        
        //dodajemo na pocetku tabova tab osnovne informacije koji prikazuje full info
        array_unshift($tabs, [
            'title' => Yii::t('BaseModule.ModelDefaultLayout', 'BasicInformationTab'),
            'code' => 'default_layout_full_info',
            'content_action' => [
                'action' => 'base/modelDefaultLayout/getFullInfoHtml',
                'get_params' => [
                    'model_name' => get_class($owner),
                    'model_id' => $owner->id
                ]
            ],
            'order' => -1
        ]);
        
        return $tabs;
    }
    
    private function repackDefaultLayoutModelTabs(&$tab)
    {
        if (isset($tab['action']))
        {
            $tab['content_action'] = [];
            $tab['content_action']['action'] = $tab['action'];
            $tab['content_action']['get_params'] = [];
            $tab['content_action']['post_params'] = [];
            if (isset($tab['get_params']) && is_array($tab['get_params']))
            {
                foreach ($tab['get_params'] as $get_param_key=>$get_param_value)
                {
                    $tab['content_action']['get_params'][$get_param_key] = $get_param_value;
                }
                unset($tab['get_params']);
            }
            if (isset($tab['params']) && is_array($tab['params']))
            {
                foreach ($tab['params'] as $param_key=>$param_value)
                {
                    $tab['content_action']['post_params'][$param_key] = $param_value;
                }
                unset($tab['params']);
            }
            unset($tab['action']);
        }

        if (isset($tab['subtree']))
        {
            $_subtree = [];
            foreach ($tab['subtree'] as $value) 
            {
                $this->repackDefaultLayoutModelTabs($value);
                array_push($_subtree, $value);
            }
            $tab['subtree'] = $_subtree;
        }
    }
}