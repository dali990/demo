<?php

class SIMAActiveRecord extends CActiveRecord
{

    /**
     * 
     */
    const HAS_MANY_PGARRAY = 'CHasManyPGARRAY';

    /**
     * koristi se prilikom pretrazivanja baze po vise polja
     * u ovo polje se unosi tekst sa kojim se uporedjuje ostatak 
     * */
    public $text;

    /**
     * koristi se za selektovanje pojedinacnih redova iz tabele po ID-u
     * spisak ID-eva se razdvaja zarezom.
     * Rezultat je presek selektovanih ID-eva sa ostatkom pretrage
     * */
    public $ids;

    /**
     * upisuju se imena relacija koja treba da se dovuku iz baze zajedno sa modelom
     * da bi funkcionisalo
     * */
    public $with;

    /**
     * upisuju se imena scope-ova koja treba da se dovuku iz baze zajedno sa modelom
     * da bi funkcionisalo. scopovi se primenjuju u oba nivoa u SIMADbCriteria (fitleri display)
     * */
    public $scopes;

    /**
     * upisuju se imena scope-ova koja treba da se dovuku iz baze zajedno sa modelom
     * da bi funkcionisalo. scopovi se primenjuju samo prilikom prikazivanja (select, order)
     * */
    public $display_scopes;

    /**
     * upisuju se imena scope-ova koja treba da se dovuku iz baze zajedno sa modelom
     * da bi funkcionisalo. scopovi se primenjuju samo prilikom filtriranja (condition, join)
     * */
    public $filter_scopes;

    /**
     * upisuju se imena kolona po kojima treba da se grupise pretraga
     * da bi funkcionisalo, u rules() mora da se stavi pravilo array('group_by','safe')
     * */
    public $group_by;
    
    /**
     * koristi se samo prilikom filtriranja(zadaje se u model_filteru)
     * */
    public $limit;
    
    /**
     * koristi se samo prilikom filtriranja(zadaje se u model_filteru)
     * */
    public $offset;

    /**
     * upisuju se imena kolona po kojima treba da se grupise pretraga
     * da bi funkcionisalo, u rules() mora da se stavi pravilo array('order_by','safe')
     * */
    //ZABRANJENO!!! zbog aliasa, jer ne moze da se zna koji ce alias biti
    public $order_by;

    /**
     * Koristi se prilikom prikazivanja forme, da bi se preno javascript kod 
     * koji se izvrsava ukoliko je usepsno sacuvana forma
     * */
    public $onSave;
    public $sum = false;
    
    /**
     * da moze na novou objekta da se doda
     * koristi se u validiranju brisanja
     * @var array
     */
    public $allowed_foreign_dependencies = [];

    /**
     * file tag modela
     * @var FileTag 
     */
    private $__tag = null;
    
    /**
     * qr code modela
     * @var QR 
     */
    private $__qrcode = null;


    /**
     * Uredjeni par vrednosti iz baze, popunjava se u funkciji afterFind;
     * @var array 
     */
    protected $__old = null;
 
   public function getOld()
    {
        return $this->__old;
    }

    private static $_added_behaviors = array();

    public static function registerPermanentBehavior($behavior, array $params = [])
    {
        $model_name = get_called_class();

        if (!isset(self::$_added_behaviors[$model_name]))
            self::$_added_behaviors[$model_name] = array();

        self::$_added_behaviors[$model_name][] = array_merge(['class' => $behavior],$params);
    }

    /**
     * Sve klase imaju GUI behaviour. 
     * Ukoliko nije specijalno napravljeno za njih, koriste
     * SIMAActiveRecordGUI
     * @return array
     */
    public function behaviors()
    {
        $_result = parent::behaviors();
        $model_name = get_class($this);

        //dodavanje GUI behavior
        $reflector = new ReflectionClass($model_name);
        $class_name = $model_name . 'GUI';
        $file_name = dirname($reflector->getFileName()) . 'GUI' . DIRECTORY_SEPARATOR . $model_name . 'GUI.php';
        if (file_exists($file_name))
            $class = $class_name;
        else
            $class = 'SIMAActiveRecordGUI';
        $_result['GUI'] = array(
            'class' => $class
        );

        while ($model_name != 'SIMAActiveRecord')
        {
            if (isset(self::$_added_behaviors[$model_name]))
            {
                $_result = array_merge($_result, self::$_added_behaviors[$model_name]);
            }
            $model_name = get_parent_class($model_name);
        }

        return $_result;
    }

    public function moduleName()
    {
        throw new Exception('U Modelu ' . get_class($this) . ' nije definisana funkcija moduleName()');
    }

    public function __isset($column)
    {
        if (in_array($column, ['DisplayName', 'SearchName']))
        {
            return true;
        }
        if (in_array($column, ['path2']) && $this->$column !== '')
        {
            return true;
        }

        $relations = $this->relations();
        if (isset($relations[$column]) && $relations[$column][0] === self::HAS_MANY_PGARRAY)
        {
            return true;
        }
        return parent::__isset($column);
    }
    
    private function isTimestampField($dbType)
    {
        return in_array ($dbType, [
            'timestamp without time zone',
            'timestamp(0) without time zone'
        ]);
    }

    public function __set($column, $value)
    {
        $columns = $this->getMetaData()->tableSchema->columns;
        
        if (SIMAMisc::stringEndsWith($column, '_sbn'))
        {
            $column = str_replace('_sbn', '', $column);
        }
        
        if (isset($columns[$column]) && !($value === 'null' && $this->getScenario() == 'filter'))
        {
            if (strpos($columns[$column]->dbType, '[]'))
            {
                $this->setAttribute($column, str_replace(array('["', '"]', '[', ']', '","'), array('{', '}', '{', '}', ','), CJSON::encode($value)));
                return;
            }
            if ($this->isJSONColumn($column))
            {
                $this->setAttribute($column, CJSON::encode($value));
                return;
            }

            if ($columns[$column]->dbType == 'integer')
            {
                if ($value === '' || $value === 'null')
                {
                    parent::__set($column, null);
                    return;
                }
            }
            if ($columns[$column]->dbType == 'boolean')
            {
//                if ($value === NULL) //MilosS: promenjeno u ovo ispod da bi u dropdown prosao prazan, iako je polje bool
                if ($value === NULL || $value === '')
                {
                    parent::__set($column, $value);
                }
                else
                {
                    //ovde se string 'null' pretvara u NULL zato sto misli da je bug
                    $_val = filter_var($value, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
                    parent::__set($column, $_val);
                }
                return;
            }
            if (strpos($columns[$column]->dbType, 'numeric') !== false)
            {                
                if ($value === '' || $value === 'null')
                {
                    parent::__set($column, null);
                    return;
                }
                else if ($value instanceof SIMABigNumber)
                {
                    parent::__set($column, $value->getValue());
                    return;
                }
                else if (gettype($value) === 'string')
                {
                    parent::__set($column, (float) $value);
                    return;
                }
            }

            if ($columns[$column]->dbType === 'date')
            {
                $this->setAttribute($column, SIMAHtml::UserToDbDate($value));
                return;
            }
            if ($this->isTimestampField($columns[$column]->dbType))
            {
                $this->setAttribute($column, SIMAHtml::UserToDbDate($value, true, true));
                return;
            }
            
            //MilosS(11.5.2017): Ne moze jer se u zajednickim formama prepakuju relacijeda pokazuju jedna na drugu, a ne u cikcak
//            $relations = $this->relations();
//            foreach ($relations as $relation_key => $relation_value)
//            {
//                if (!empty($relation_value[2]) && $relation_value[2] === $column)
//                {
//                    unset($this->$relation_key);
//                }
//            }
        }
        parent::__set($column, $value);
    }
            
    public function __get($column)
    {
        $_trace_cat = 'SIMA.SIMAActiveRecord.__get';
        
        if (substr($column, 0) == 'sum_')
        {
            return $this->$column;
        }
        //koristi se kod filtera koji su relacija gde se kolona zadaje u obliku relName.attributeName
        if (strpos(trim($column), '.') !== FALSE)
        {
            $parts = explode('.', $column);
            $i = 0;
            $max_i = count($parts) - 1;
            $localThis = $this;
            if (isset($this->{$parts[0]}))
            {
                $localThis = $this->{$parts[0]};
                $i = 1;
                while ($i < $max_i && isset($localThis->{$parts[$i]}))
                {
                    $localThis = $localThis->{$parts[$i]};
                    $i++;
                }
            }
            return $localThis->getAttributeDisplay($parts[$i]);
        }

        //pretvaranje za polja koja su pgarray i json
        $columns = $this->getMetaData()->tableSchema->columns;
        if (isset($columns[$column]) && strpos($columns[$column]->dbType, '[]'))
        {
            return SIMAMisc::pgArrayToPhpArray($this->getAttribute($column));
        }
        if (isset($columns[$column]) && ($this->isJSONColumn($column)))
        {
            return CJSON::decode($this->getAttribute($column));
        }

        if (isset($columns[$column]) && $columns[$column]->dbType === 'date')
        {
            if ($this->getAttribute($column) !== null)
                return SIMAHtml::DbToUserDate($this->getAttribute($column));
            else
                return null;
        }
        else if (isset($columns[$column]) && $this->isTimestampField($columns[$column]->dbType))
        {
            if ($this->getAttribute($column) !== null)
                return SIMAHtml::DbToUserDate($this->getAttribute($column), true, $this->isMicrosecondColumn($column));
            else
                return null;
        }
        else if ($this->isNumericColumn($column))
        {
            return $this->parseNumericColumn($column);
        }

        Yii::beginProfile(get_class($this).'_relations',$_trace_cat);
        $relations = $this->relations();
        Yii::endProfile(get_class($this).'_relations',$_trace_cat);
        
        if (isset($relations[$column]) && $relations[$column][0] === self::HAS_MANY_PGARRAY)
        {
            $uniq = SIMAHtml::uniqid();
            $Model = $relations[$column][1];
            $fk = $relations[$column][2];
            return $Model::model()->findAll(":param$uniq @> ARRAY[id]", array(':param' . $uniq => $this->getAttribute($fk)));
        }

        //pretvaranja datuma
        if (isset($columns[$column]) && isset($this->$column))
        {
        }

        switch ($column)
        {
            /**
             * koristi se kod fajlova za njihov upit. Trebalo bi da stoji u modelu File
             * */
            case 'query': return "TAG$-1";

            /**
             * ne znam trenutnu primenu
             */
            case 'selected': return false;

            /**
             * prikaz praznog stringa
             * nekad se koristio u paged_table, trebalo bi da se vise nigde ne koristi
             * */
            case 'empty': return '';

            /**
             * ne znam trenutnu primenu
             */
            case 'options': return array();

            /**
             * ne znam trenutnu primenu
             */
            case 'hasChildren': return false;

            /**
             * putanja za otvaranje tab-a koji predstavlja ovaj model. Koristi se u SIMAMainMenu
             */
            case 'path2': return '';

            case 'DBbackend': return 'pgsql';

            case 'hasTabs':
                $result = false;
                try 
                {
                    $className = get_class($this) . 'Tabs';
                    if (class_exists($className))
                    {
                        $result = true;
                    }
                } 
                catch (Exception $ex)
                {
                    $result = false;
                }
                return $result;
            case 'TabsBehavior' :
                if (class_exists(get_class($this) . 'Tabs'))
                    return get_class($this) . 'Tabs';
                else
                    throw new CHttpException(404, 'Model ' . get_class($this) . ' nema zadat TabsBehavior');

            /**
             * Skraceno reprezentativno ime modela
             */
            case 'DisplayName':
                {
                    if (isset($this->display_name))
                        return $this->display_name;
                    else if (isset($this->name))
                        return $this->name;
                    else
                        return 'set DisplayName in ' . get_class($this);
                }

            /**
             * prosireno reprezentativno ime modela - sluzi za pregled u pretrazi
             */
            case 'SearchName':
                {
                    if (isset($this->name))
                    {
                        if (isset($this->description) && ($this->description != null))
                            return $this->name . '(' . $this->description . ')';
                        if (isset($this->comment) && ($this->comment != null))
                            return $this->name . '(' . $this->comment . ')';
                        return $this->name;
                    }
                    else if (isset($this->display_name))
                        return $this->display_name;
                    else
                        return 'set SearchName in ' . get_class($this);
                }

            default: return parent::__get($column);
        }
    }
    
    public function noChange($columns)
    {
        if (!$this->hasErrors() && !$this->isNewRecord && $this->columnChanged($columns))
        {
            $this->addError($columns, Yii::t('BaseModule.SIMAActiveRecord','NoChange'));
        }
    }
    
    public function noChangeWhenConfirmed($attribute, $params)
    {
        $confirmed_column = 'confirmed';
        if (isset($params['confirmed_column']))
        {
            $confirmed_column = $params['confirmed_column'];
        }
        $negative = (isset($params['negative']) && boolval($params['negative']));
        
        if (($this->$confirmed_column) ^ $negative)
        {
            if ($this->columnChanged($attribute))
            {
                if (isset($params['message']))
                {
                    $_msg = $params['message'];
                }
                else
                {
                    $_msg = Yii::t('BaseModule.SIMAActiveRecord','NoChangeWhenConfirmed');
                }
                $this->addError($attribute, $_msg);
            }
        }
    }
    
    //funkcija za rule
    public function unique_with($attribute, $params)
    {
        $attributes = [$attribute => trim($this->$attribute)];
        $withs = [];
        if (isset($params['with']))
        {
            $withs = $params['with'];
            if (!is_array($withs))
            {
                $withs = [$withs];
            }   
            foreach ($withs as $with)
            {
                $attributes[$with] = trim($this->$with);
            }
        }
        if (isset($params['message']))
        {
            $message = $params['message'];
            if (!is_string($message))
            {
                throw new SIMAException('poruka u unique_check nije string!!!');
            }
        }
        else
        {
            if (empty($withs))
            {
                $message = Yii::t('SIMAActiveRecord', 'NotUniq', [
                    '{attribute_label}' => $this->getAttributeLabel($attribute)
                ]);
            }
            else
            {
                $withs_labels = [];
                foreach ($withs as $_with)
                {
                    $withs_labels[] = $this->getAttributeLabel($_with);
                }
                $message = Yii::t('SIMAActiveRecord', 'NotUniqWith', [
                    '{attribute_label}' => $this->getAttributeLabel($attribute),
                    '{withs}' => implode(',', $withs_labels)
                ]);
            }
        }
        
        $this_id = ($this->isNewRecord)?0:$this->id;
        $class = get_class($this);
        $check_model = $class::model()->countByAttributes($attributes,'id!=:this_id',[':this_id'=>$this_id]);
        if ($check_model>0)
        {
            $this->addError($attribute,$message);
        }
    }
    
    public function ruleOnlyLetters($attribute, $params)
    {
        if(!ctype_alpha($this->$attribute))
        {
            $this->addError($attribute, Yii::t('SIMAActiveRecord', 'MustContainOnlyLetters'));
        }
    }
    
    public function ruleWithoutNumbers($attribute, $params)
    {
        if(preg_match('/\\d/', $this->$attribute) > 0)
        {
            $this->addError($attribute, Yii::t('SIMAActiveRecord', 'MustContainOnlyLetters'));
        }
    }
    
    public function ruleDateCheck($attribute, $params)
    {
        $date = $this->$attribute;
        
        if(empty($date))
        {
            return;
        }
        
        $format = Yii::app()->params['date_format'];
        
        $format_for_datetime = str_replace([
            'dd.', 'mm.', 'yyyy.'
        ], [
            'd.', 'm.', 'Y.'
        ], $format);
        
        $d = DateTime::createFromFormat($format_for_datetime, $date);
        
        $date_valid = $d && $d->format($format_for_datetime) == $date;
        if(!$date_valid)
        {
            $this->addError($attribute, Yii::t('SIMAActiveRecord', 'RuleDateCheckFail', [
                '{format}' => $format
            ]));
        }
    }
    
    public function relations($child_relations = [])
    {
        $_m = $this::model();
        $_result = $child_relations;

        $event = new SIMAEvent();
        $_m->onGetRelations($event);
        foreach ($event->getResults() as $result)
        {
            $_result = array_merge($_result, $result);
        }
        return $_result;
    }

    public function onGetRelations($event)
    {
        $this->raiseEvent("onGetRelations", $event);
    }
    
    public function onGetViews($event)
    {
        $this->raiseEvent("onGetViews", $event);
    }
    
    public function onGetFullInfo($event)
    {
        $this->raiseEvent("onGetFullInfo", $event);
    }
    
    public function onGetSettings($event)
    {
        $this->raiseEvent("onGetSettings", $event);
    }
    
    public function onGetBasicInfo($event)
    {
        $this->raiseEvent("onGetBasicInfo", $event);
    }

    protected function afterFind()
    {
        parent::afterFind();
        $this->calcLocalValues();
        $this->populateOld();
    }
    
    public function refresh()
    {
        $ret = parent::refresh();
        if ($ret)
        {
            $this->calcLocalValues();
            $this->populateOld();
        }
        return $ret;
    }   
    
    protected function calcLocalValues()
    {
        
    }
    
    protected function populateOld()
    {
        $this->__old = $this->getAttributes();
        foreach ($this->__old as $column => $value)
        {
            $columns = $this->getMetaData()->tableSchema->columns;
            if (isset($columns[$column]) && $columns[$column]->dbType === 'date')
            {
                $this->__old[$column] = SIMAHtml::DbToUserDate($value);
            }
            else if (isset($columns[$column]) && $this->isTimestampField($columns[$column]->dbType))
            {
                $this->__old[$column] = SIMAHtml::DbToUserDate($value, true, $this->isMicrosecondColumn($column));
            }
            else if ($this->isNumericColumn($column))
            {
                $this->__old[$column] = $this->parseNumericColumn($column);
            }
        }
    }

    public function gettag()
    {
        if ($this->__tag == null)
        {
            $this->__tag = FileTag::model()->findByAttributes(array('model_table' => $this->tableName(), 'model_id' => $this->id));
        }
        return $this->__tag;
    }
    
    public function getQRCode()
    {
        if ($this->__qrcode == null)
        {
            $this->__qrcode = QR::model()->findByAttributes(array('model' => get_class($this), 'model_id' => $this->id));
        }
        return $this->__qrcode;
    }

    /**
     * Prebacena odgovornost na GUI behaviour
     * @see CModel::attributeLabels()
     */
    public function attributeLabels()
    {
        return $this->columnLabels();
    }

    /**
     * vraza niz koji predstavlja redosled kolona koji se prikazuju u paged_table
     * svaki element je ime kolone ili kvazi kolone. To ime se kasnije prosledjuje funkciji 
     * getAttributeDisplay i getAttributeFilter 
     * @param string $type
     */
    public function columnOrder($type)
    {
        switch ($type)
        {
            case 'asLeaf': return array(
                    'name', 'description'
                );
            default:
                if (in_array('description', $this->safeAttributeNames))
                    return array('name', 'description');
                if (in_array('comment', $this->safeAttributeNames))
                    return array('name', 'comment');
                return array('name');
        }
    }

    /**
     * Funkcija koja vraca spisak svih opcija koje korisnik moze da odradi sa modelom
     * http://wiki.setpp.rs/SIMAActiveRecord#modelOptions
     * @param User $user - korisnik za kojeg se pitaju koje opcije ima
     * @return Array - spisak opcija
     */
    final public function getModelOptions(User $user = null)
    {
        $trace_cat = 'SIMA.SIMAActiveRecord.getModelOptions';
        Yii::beginProfile('',$trace_cat);
        
        $old_options = $this->modelSettings('options');
        if (is_string($old_options))
        {
            if ($old_options === self::$DEPRACTED_SETTING)
            {
                /**
                 * kada se iskoreni $this->modelSettings('options'), samo ovo ce da ostane 
                 */
                $_result = $this->modelOptions($user);
                
                Yii::beginProfile('event',$trace_cat);
                $event = new SIMAEvent();
                $this->onGetOptions($event);
                foreach ($event->getResults() as $event_result)
                {
                    foreach ($event_result as $action)
                    {
                        if (is_string($action))
                        {
                            $_result[] = $action;
                        }
                        elseif(is_array($action))
                        {
                            switch ($action[0])
                            {
                                case 'remove':
                                    $_index = array_search($action[1], $_result);
                                    if ($_index !== FALSE)
                                    {
                                        array_splice($_result, $_index, 1);
                                    }
                                    break;
                                case 'add':
                                    $_result[] = $action[1];
                                    break;
                            }
                        }
                    }
                }
                Yii::endProfile('event',$trace_cat);
                Yii::endProfile('',$trace_cat);
                return $_result;
            }
            else
            {
                error_log(__METHOD__.' modelSettings("options") vratio string?? STRING: '.$old_options);
                Yii::endProfile('',$trace_cat);
                return [];
            }
        }
        else
        {
            Yii::endProfile('',$trace_cat);
            return $old_options;
        }
    }

    public function onGetOptions($event)
    {
        $this->raiseEvent("onGetOptions", $event);
    }
    
    /**
     * Funkcija koja izracunava koje opcije korisnik ima nad modelom
     * @param User $user - korisnik za kojeg se izracunavaju opcije
     * @return array - niz kodova sa opcijama
     */
    protected function modelOptions(User $user = null):array
    {
        return ['form','delete'];
    }
    
    private static $DEPRACTED_SETTING = 'DEPRACTED_SETTING';
    /**
     * podesavanje kolona koje postoje u modelu
     * filters - na koji nacin treba filtrirati kolone
     * options - koje sve opcije postoje u modelu
     * */
    public function modelSettings($column)
    {
        $called_class = get_called_class();
        switch ($column)
        {
            case 'clicked_form_first' : return true; //kada se otvori forma, da se prvo prikaze forma kliknutog modela
            case 'filters' : return array(
                    'name' => 'text',
                    'description' => 'text',
                    'subject' => 'text',
                );
            case 'options' : return self::$DEPRACTED_SETTING;
            case 'form_list_relations':
            case 'number_fields':
            case 'textSearch' :
            case 'multiselect_options' :
            case 'statuses' :
            case 'update_relations':
            case 'mutual_forms':
            case 'column_types' :
            case 'select_filters' :  //podesavanje za GuiTable
            case 'sum' :
            case 'order_by':
            case 'GuiTable' : return array(); //podesavanje za GuiTable
            case 'searchField' : return array(
                    'type' => 'default',
                );
            case 'default_order_scope' :
            case 'form_display_scopes': return [];
            case 'default_order' : return '';
            case 'model_change_columns': return [];
            case 'relationsCommentThreads': return [];
            case 'warn_default_model': return ''; //default model iz koga se cita akcija za prikaz tog modela, jer se warn-ovi grupisu na osnovu akcije
            case 'search_model': return [];
            case 'columns_custom_order': return [];
            case 'big_numbers_report': return [];
            case 'big_numbers': return [];
            case 'merge_options': return []; /// koristi se za merge modela
            default : throw new Exception('U Modelu ' . $called_class . ' nije definisan konfiguracioni parametar ' . $column);
        }
    }

    /**
     * Ovo je privremena funkcija dok se ne napravi da je modelSettings staticka funkcija
     * ona vraca koja sva polja treba da se tretiraju kao JSON
     * @return array
     */
    protected static function getJSONColumns()
    {
        return array();
    }

    public function isJSONColumn($column)
    {
        $columns = $this->getMetaData()->tableSchema->columns;
        return
                (
                (strpos($columns[$column]->dbType, 'text') !== FALSE) &&
                in_array($column, $this->getJSONColumns())
                ) ||
                (strpos($columns[$column]->dbType, 'json') !== FALSE);
    }

    /**
     * Ovo je privremena funkcija dok se ne napravi da je modelSettings staticka funkcija
     * ona vraca koja sva polja treba da se tretiraju kao vreme sa milisekundama
     * @return array
     */
    protected static function getMicrosecondColumns()
    {
        return array();
    }

    public function isMicrosecondColumn($column)
    {
        $columns = $this->getMetaData()->tableSchema->columns;
        return
                (
                (strpos($columns[$column]->dbType, 'timestamp') !== FALSE) &&
                in_array($column, $this->getMicrosecondColumns())
                );
    }
    
    /**
     * 
     * @param array $attributes
     * @return Model
     * @throws Exception
     */
    public static function filter($attributes = array())
    {
        $class_name = get_called_class();

        $obj = new $class_name(null);
        $obj->setScenario('filter');
        $obj->attachBehaviors($obj->behaviors());
        if (!isset($attributes))
        {
            throw new Exception("Nisu zadati atributi filtera!");
        }
        $obj->setAttributesFilter($attributes);

        return $obj;
    }

    /**
     * 
     * @param type $values
     * @return postavlja sve atribute modela koji su poslati preko POST-a i vrsi proveru filtera u tom modelu. Za filtere koji su relacija POST-om
     * se salju podaci u sledecem obliku: Model[RelName][attributeName]. Za svaki filter koji je relacija funkcija poziva sebe rekurzivno za
     * tu relaciju gde se salje RelName[attributeName]
     */
    public function setAttributesFilter($values)
    {
        $model_settings_filters = $this->modelSettings('filters');
        $model_settings_statuses = $this->modelSettings('statuses');
        $model_settings_statuses_keys = array_keys($model_settings_statuses);

        if (isset($values) && is_array($values))
        {
            foreach ($values as $name => $value)
            {
                switch ($name)
                {
                    case 'text':
                    case 'with':
                    case 'scopes':
                    case 'display_scopes':
                    case 'filter_scopes':
                    case 'group_by':
                    case 'order_by':
                    case 'onSave':
                    case 'sum':
                    case 'limit':
                    case 'offset':
                        $this->$name = $value;
                        break;
                    case 'ids':
                        if (isset($value['init']))
                        {
                            $this->$name = $value['init'];
                        }
                        else
                        {
                            $this->$name = $value;
                        }
                        break;
                    default:
                        if (!isset($model_settings_filters[$name]) && !in_array($name, $model_settings_statuses_keys))
                        {
                            $message = "Za model ".get_class($this)." nije setovan model filter za kolonu $name.";
                            error_log(__METHOD__.' - '.$message);
//                            Yii::app()->errorReport->createAutoClientBafRequest($message);
                        }
                        break;
                }
            }
        }

        //vrsi proveru za sve filtere koji su definisani u model settings
        foreach ($model_settings_filters as $column => $filter_type)
        {
            if ($filter_type == 'relation' ||
                    (gettype($filter_type) == 'array' && isset($filter_type[0]) && $filter_type[0] == 'relation'))
            {
                $relations = $this->relations();
                if(isset($relations[$column]))
                {
                    $relation = $relations[$column];
                    if (isset($relation[0]) && $relation[0] == 'CManyManyRelation' && !empty($values[$column]))
                    {
                        if (isset($values[$column]['ids']))
                        {
                            $relation_models = $relation[1]::model()->findAll(new SIMADbCriteria([
                                'Model' => $relation[1],
                                'model_filter' => [
                                    'ids' => $values[$column]['ids']
                                ]
                            ]));
                            $this->$column = $relation_models;
                        }
                    }
                    else if (isset($values[$column]))
                    {
                        $model_name = $relation[1];
                        $this->$column = new $model_name(null);
                        $this->$column->setScenario('filter');

                        //vrsi proveru za sve statuse u relaciji
                        foreach ($this->$column->modelSettings('statuses') as $status_key => $status_value)
                        {
                            if (isset($values[$column][$status_key]) && $values[$column][$status_key] != "")
                            {
                                $this->$column->$status_key = $values[$column][$status_key];
                            }
                        }
                        $this->$column->setAttributesFilter($values[$column]);
                    }
                    else if (isset($relation[0]) && $relation[0] == 'CHasManyRelation')
                    {
                        $this->$column = null; //mora da se ne bi pravili nizovi
                    }
                }
            }
            else if (isset($values[$column]) && $values[$column] !== "")
            {                
                if (isset($values[$column]['init']))
                    $this->$column = $values[$column]['init'];
                else
                    $this->$column = $values[$column];
            }
        }

        //vrsi proveru za sve statuse koji su definisani u model settings-u
        foreach ($model_settings_statuses as $status_key => $status_value)
        {
            if (isset($values[$status_key]) && $values[$status_key] != "")
            {
                $this->$status_key = $values[$status_key];
            }
        }
    }

    /**
     * filter koji ne filtrira nista 
     */
    public function filterEmpty()
    {
        
    }
    
    /**
     * filter koji filtrira u potpunosti. Moze da se koristi kada ne treba filter u GuiTabeli, ali treba u nekoj drugoj pretrazi
     */
    public function filterFullMatch($criteria, $alias, $column, $filter_type)
    {
        if (!empty($this->$column))
        {
            $uniq = SIMAHtml::uniqid();
            $condition = "$alias.$column = :value$uniq";
            $has_params = true;
            if (
                    (
                        is_array($filter_type) && $filter_type[0] === 'integer'
                    ) ||
                    $filter_type === 'integer'
               )
            {
                if (!is_numeric($this->$column))
                {
                    $condition = "$alias.id = -1";
                    $has_params = false;
                }
                else
                {
                    $this->$column = intval($this->$column);
                }
            }
            $local_crit = new SIMADbCriteria();
            $local_crit->condition = $condition;
            if ($has_params === true)
            {
                $local_crit->params = [
                    ":value$uniq" => $this->$column
                ];
            }
            $criteria->mergeWith($local_crit);
        }
    }
    
    public function beforeMutualSave()
    {
        
    }
    
    public function afterMutualSave()
    {
        
    }
    
    public function beforeSave()
    {
        $statuses = $this->modelSettings('statuses');
        
        //pokretanje funkcija beforeConfirm ukoliko ima potrebe
        foreach ($statuses as $status_key => $status_value)
        {
            $columns = $this->getMetaData()->tableSchema->columns;
            $column_allow_null = $columns[$status_key]->allowNull;

            $_sk = $this->$status_key;

            if (is_null($this->__old))
            {
                $_osk = ($column_allow_null) ? null : false;
            }
            else
            {
                $_osk = $this->__old[$status_key];
            }

            if (
                    isset($status_value['beforeConfirm']) &&
                    (
                    (!$column_allow_null && !$_osk && $_sk ) ||
                    ($column_allow_null && is_null($_osk) && !is_null($_sk))
                    )
            )
            {                
                $_func = $status_value['beforeConfirm'];
                $this->$_func();
            }
        }

        //postavljamo vreme kada se status promenio i upisujemo ko je promenio
        $columns = $this->getMetaData()->tableSchema->columns;
        foreach ($statuses as $status_key => $status_value)
        {
            $column_allow_null = $columns[$status_key]->allowNull;
            
            /// TODO: optimizovati veliki if
//            $old_val = null;
//            if(!$this->isNewRecord)
//            {
//                $old_val = $this->__old[$status_key];
//            }
//            else ()
            if (
                    (
                        $this->isNewRecord
                        &&
                        (
                            (
                                $column_allow_null && !is_null($this->$status_key)
                            ) 
                            || 
                            (
                                !$column_allow_null && $columns[$status_key]->defaultValue !== boolval($this->$status_key)
                            )
                        )
                    )
                    ||
                    (
                        !$this->isNewRecord
                        &&
                        (
                            (
                                $column_allow_null && 
                                (
                                    (is_null($this->__old[$status_key]) && !is_null($this->$status_key)) || //ako je bio null i sada nije null
                                    (!is_null($this->__old[$status_key]) && is_null($this->$status_key)) || //ako nije bio null i sada je null
                                    boolval($this->__old[$status_key]) !== boolval($this->$status_key) //ako je promenjen iz true u false ili iz false u true
                                )
                            ) || 
                            (
                                !$column_allow_null && 
                                (
                                    (is_null($this->__old[$status_key]) && $columns[$status_key]->defaultValue !== boolval($this->$status_key)) ||
                                    (!is_null($this->__old[$status_key]) && $this->__old[$status_key] !== boolval($this->$status_key))
                                )
                            )
                        )
                    )
                )
            {
                if (isset($status_value['timestamp']))
                {
                    $this->{$status_value['timestamp']} = 'now()';
                }
            }
        }

        return parent::BeforeSave();
    }
    
//    public function validate($attributes = NULL, $clearErrors = true)
//    {
//        return parent::validate($attributes, $clearErrors);
//    }
    
    public function afterDelete()
    {
        parent::afterDelete();
        
        $this->updateModels();

        $this->saveModelChanges();
    }
    
    public function afterSave()
    {
        $this->updateModels();

        //pokretanje funkcija onConfirm i onRevert ukoliko ima potrebe
        $statuses = $this->modelSettings('statuses');
        foreach ($statuses as $status_key => $status_value)
        {
            $columns = $this->getMetaData()->tableSchema->columns;
            $column_allow_null = $columns[$status_key]->allowNull;

            $_sk = $this->$status_key;

            if (is_null($this->__old))
            {
                $_osk = ($column_allow_null) ? null : false;
            }
            else
            {
                $_osk = $this->__old[$status_key];
            }

            if (
                    isset($status_value['onConfirm']) &&
                    (
                    (!$column_allow_null && !$_osk && $_sk ) ||
                    ($column_allow_null && is_null($_osk) && !is_null($_sk))
                    )
            )
            {
                try
                {
                    $_func = $status_value['onConfirm'];
                    $this->$_func();
                }
                catch (Exception $e)
                {
                    if (isset(Yii::app()->controller))
                    {
                        Yii::app()->controller->raiseNote($e->getMessage());
                    }
                    Yii::app()->errorReport->createAutoClientBafRequest(
                        "Nije uspelo izvršavanje onConfirm funkcije za status '$status_key' u modelu '".get_class($this)."' iz sledećih razloga: <br />" . $e->getMessage()
                    );
                }
            }

            if (
                    isset($status_value['onRevert']) &&
                    (
                    (!$column_allow_null && $_osk && !$_sk ) ||
                    ($column_allow_null && !is_null($_osk) && is_null($_sk))
                    )
            )
            {
                try
                {
                    $_func = $status_value['onRevert'];
                    $this->$_func();
                }
                catch (Exception $e)
                {
                    if (isset(Yii::app()->controller))
                    {
                        Yii::app()->controller->raiseNote($e->getMessage());
                    }
                    Yii::app()->errorReport->createAutoClientBafRequest(
                        "Nije uspelo izvršavanje onRevert funkcije za status '$status_key' u modelu '".get_class($this)."' iz sledećih razloga: <br />" . $e->getMessage()
                    );
                }
            }
        }

        $form_list_relations = $this->modelSettings('form_list_relations');
        $model_relations = $this->relations();
        foreach ($model_relations as $key => $value)
        {
            if (in_array($key, $form_list_relations))
            {
                if ($value[0] === "CManyManyRelation" && is_string($this->$key))
                {
                    $table_string = $value[2];
                    $table_name = trim(strstr($table_string, '(', true));
                    $attributes = trim(str_replace(array('(', ')'), '', strstr($table_string, '(')));
                    $attributes_exploded = explode(',', $attributes);
                    $base_column = trim($attributes_exploded[0]);
                    $rel_column = trim($attributes_exploded[1]);
                    $input_value = CJSON::decode($this->$key);
                    $default_item = $input_value['default_item'];
                    if (isset($input_value['ids']) && $input_value['ids'] !== null)
                    {
                        $ids = $input_value['ids'];
                        $new_ids = array();
                        foreach ($ids as $_id)
                        {
                            //ako je stavka prosledjena kao niz to znaci da prvo treba da se edituje ta stavka pa onda se dodaje njen id u spisak
                            if (is_array($_id['id']) && isset($_id['id']['id']))
                            {                                
                                $rel_model = $value[1]::model()->findByPk($_id['id']['id']);
                                if ($rel_model !== null)
                                {
                                    $rel_model->setModelAttributes($_id['id']);
                                    $rel_model->save();
                                }                                
                                array_push($new_ids, $_id['id']['id']);
                            }
                            else
                            {
                                array_push($new_ids, $_id['id']);
                            }                            
                        }
                        //BRISANJE VISKA
                        $rel_models = $this::model()->resetScope()->findByPk($this->id)->$key; //dovlacenje modela
                        //pakovanje ID u niz
                        $old_ids = [];
                        foreach ($rel_models as $rel_model)
                        {
                            $old_ids[] = $rel_model->id;
                        }
                        
                        $to_add = array_diff($new_ids, $old_ids);
                        $to_del = array_diff($old_ids, $new_ids);

                        $to_del_str = implode(',', $to_del); //napraviti string
                        if (!empty($to_del_str))
                        {
                            $command = $this->getDbConnection()->createCommand();
                            $command->delete($table_name, "$base_column=$this->id and $rel_column in ($to_del_str)");
                        }

                        //DODAVANJE MANJKA

                        foreach ($to_add as $id)
                        {
                            if (!empty($id))
                            {
                                $command = $this->getDbConnection()->createCommand();
                                $command->insert($table_name, array($base_column => $this->id, $rel_column => $id));
                            }
                        }

                        //dodaje/brise se default selected item ako je zadata vrednost
                        if (is_array($default_item) && count($default_item) > 0 && isset($default_item['value']))
                        {
                            $model = $default_item['model']::model()->findByPk($default_item['model_id']);
                            if ($model !== null)
                            {
                                if (in_array($default_item['value'], $to_del))
                                {
                                    $rel_models = $this::model()->resetScope()->findByPk($this->id)->$key;
                                    if (count($rel_models) > 0)
                                    {
                                        $model->{$default_item['column']} = $rel_models[0]->id;
                                    }
                                    else
                                    {
                                        $model->{$default_item['column']} = null;
                                    }
                                }
                                else
                                {
                                    $model->{$default_item['column']} = $default_item['value'];
                                }
                                $model->save();
                            }
                        }
                    }
                }
                else if ($value[0] === "CHasManyRelation" && is_string($this->$key))
                {
                    $input_value = CJSON::decode($this->$key);
                    $default_item = $input_value['default_item'];
                    if (isset($input_value['ids']) && $input_value['ids'] !== null)
                    {
                        $ids = $input_value['ids'];
                        //pravi se niz novih id-eva
                        $new_ids = array();
                        foreach ($ids as $_id)
                        {
                            //ako id nije zadat kao niz ili je zadat kao niz koji ima atribut id
                            //onda taj model vec postoji i dodaje se na spisak
                            if (is_array($_id['id']))
                            {
                                if (isset($_id['id']['id']))
                                {
                                    array_push($new_ids, $_id['id']['id']);
                                }
                            }
                            else
                            {
                                array_push($new_ids, $_id['id']);
                            }
                        }
                        //dovlacenje starih modela i pravljenje niza starih id-eva
                        $rel_models = $this::model()->resetScope()->findByPk($this->id)->$key;
                        $old_ids = [];
                        foreach ($rel_models as $rel_model)
                        {
                            $old_ids[] = $rel_model->id;
                        }
                        //razlika koja treba da se obrise
                        $to_del = array_diff($old_ids, $new_ids);
                        foreach ($to_del as $_value)
                        {
                            $del_model = $value[1]::model()->findByPk($_value);
                            if ($del_model !== null)
                            {
//                                $del_model->setScenario('delete');
//                                if (!$del_model->validate())
//                                {
//                                    throw new SIMAWarnException(SIMAHtml::showErrors($del_model));
//                                }
                                $del_model->delete();
                            }
                        }

                        //svi id-evi koji su zadati kao niz su novi modeli koji treba da se kreiraju ili edituju postojeci
                        foreach ($ids as $_id)
                        {                            
                            if (is_array($_id['id']))
                            {
                                if (isset($_id['id']['id']))
                                {                                    
                                    $rel_model = $value[1]::model()->findByPk($_id['id']['id']);                                    
                                }
                                else
                                {
                                    $rel_model = new $value[1]();
                                }
                                if ($rel_model !== null)
                                {
                                    $rel_model->setModelAttributes($_id['id']);
                                    $rel_model->{$value[2]} = $this->id;
                                    $rel_model->save();
                                }
                            }
                        }
                        
                        //dodaje/brise se default selected item ako je zadata vrednost
                        if (is_array($default_item) && count($default_item) > 0 && isset($default_item['value']))
                        {
                            $model = $default_item['model']::model()->findByPk($default_item['model_id']);
                            if ($model !== null)
                            {
                                if (in_array($default_item['value'], $to_del))
                                {
                                    $rel_models = $this::model()->resetScope()->findByPk($this->id)->$key;
                                    if (count($rel_models) > 0)
                                    {
                                        $model->{$default_item['column']} = $rel_models[0]->id;
                                    }
                                    else
                                    {
                                        $model->{$default_item['column']} = null;
                                    }
                                }
                                else
                                {
                                    $model->{$default_item['column']} = $default_item['value'];
                                }
                                $model->save();
                            }
                        }
                    }
                }
                else if ($value[0] === "CBelongsToRelation")
                {
                    $input_value = CJSON::decode($this->$key);
                    //ako je setovan niz ids i on mora biti niz
                    if (isset($input_value['ids']) && $input_value['ids'] !== null && gettype($input_value['ids']) === 'array')
                    {
                        //ako niz ids nije prazan znaci imamo da dodamo element
                        if (count($input_value['ids']) > 0)
                        {
                            //ako je tip id-a array onda znaci da je promenjena adresa ili dodata nova
                            if (gettype($input_value['ids'][0]['id']) === 'array')
                            {
                                $old_rel_column = $this->{$value[2]};
                                $rel_model = new $value[1]();
                                $rel_model->setModelAttributes($input_value['ids'][0]['id']); //u ovom slucaju $_id['id'] je niz atributa                                
                                $rel_model->save();
                                $rel_model->refresh();
                                $_model = $this::model()->findByPk($this->id);
                                if ($_model !== null)
                                {
                                    $_model->{$value[2]} = $rel_model->id;
                                    $_model->save();
                                }
                                //ako je promenjena brisemo stari relacioni model
                                if ($old_rel_column !== null)
                                {
                                    $old_rel_model = $value[1]::model()->findByPk($old_rel_column);
                                    if ($old_rel_model !== null)
                                        $old_rel_model->delete();
                                }
                            }
                        }
                        //inace brisemo ako postoji
                        else if ($this->{$value[2]} !== null)
                        {
                            $old_rel_column = $this->{$value[2]};
                            $this->{$value[2]} = null;
                            $this->$key = $this::model()->resetScope()->findByPk($this->id)->$key;
                            $this->save();
                            $this->refresh();
                            $old_rel_model = $value[1]::model()->findByPk($old_rel_column);
                            if ($old_rel_model !== null)
                                $old_rel_model->delete();
                        }
                    }
                }
            }
        }
        
        $this->saveModelChanges();

        parent::afterSave();
    }

    /**
     * za zadatu kolonu vraca kog je tipa filter
     * @param string $column
     */
    public function getAttributeFilter($column)
    {
        $settings = $this->modelSettings('filters');
        if (isset($settings[$column]))
            return $settings[$column];
        return '';
    }

    /**
     * za zadatu kolonu vraca da li se data kolona sumira
     * @param string $column
     */
    public function isSum($column, $columns_type)
    {
        $model = get_class($this);
        $guiTableSettings = $model::model()->guiTableSettings($columns_type);
        $settings = null;
        if (isset($guiTableSettings['sums']))
        {
            $settings = $guiTableSettings['sums'];
        }
        if (gettype($settings) === 'string' && preg_match('/' . $column . '/', $settings) > 0)
            return true;
        if (gettype($settings) === 'array' && in_array($column, $settings))
            return true;

        return false;
    }

    /**
     * za zadatu kolonu vraca njen ulepsan izgled ili deo html-a
     * @param string $column
     * @return string
     */
    public function getAttributeDisplay($column)
    {
////        $second_arg = null;
////        $arg_list = func_get_args();
////        if(
////                count($arg_list) > 1 
////                && isset($arg_list[1]) 
////        )
////        {
////            $second_arg = $arg_list[1];
////        }
////        return $this->columnDisplays($column, $second_arg);
//        /// ovako prosledjuje sve argumente koje ima dalje
//        return call_user_func_array([$this, 'columnDisplays'], func_get_args());
        return $this->columnDisplays($column);
    }

    public function getClasses()
    {
        return " row " . SIMAHtml::getTag($this).' '.$this->getColorClasses();
    }

    public function getAdditionalInfo()
    {
        return ' tag="' . SIMAHtml::getTag($this) . '" ';
    }

    public function merge_model($model)
    {
        throw new Exception('nije napisana funkcija za spajanje - ' . get_class($model));
    }

    public function checkConfirm()
    {
        return true;
    }

    public function __toString()
    {
        return $this->DisplayName;
    }

    /**
     * za zadati class i id odraditi ->save() (sluzi za trigerovanje beforeSave i afterSave funkcija)
     * @param array $params moraju biti zadati class i id
     */
    public static function ReSave($params = array())
    {
        if (gettype($params) === 'array' && isset($params['id']) && isset($params['class']))
        {
            $wc = $params['class']::model()->findByPk($params['id']);
            if ($wc != null)
                $wc->save();
        }
    }
    
    /**
     * isto kao ReSave, samo sto radi za niz id-eva
     * da se ne bi pravio resave za svaki id pojedinacno, nogo redova
     * @param type $params
     *  * class
     *  * ids
     */
    public static function MultiReSave($params = [])
    {
        if (gettype($params) === 'array' && isset($params['ids']) && isset($params['class']))
        {
            $models = $params['class']::model()->findAll([
                'model_filter' => [
                    'ids' => $params['ids']
                ]
            ]);
            foreach($models as $model)
            {
                $model->save();
            }
        }
    }

    public function recycle()
    {
        throw new Exception("model " . get_class($this) . ' ne podrzava recikliranje');
    }

    public function save($runValidation = true, $attributes = null, $throwExceptionOnFail = true)
    {
        $res = parent::save($runValidation, $attributes);
        if (!$res)
        {
            if ($throwExceptionOnFail)
            {
                if ($this->hasErrors())
                {
    //                error_log(__METHOD__.' ima validacionih gresaka');
                    throw new SIMAWarnModelNotValid($this);
                }
                else
                {
    //                error_log(__METHOD__.' nema validacionih gresaka');
                    throw new SIMAExceptionModelNotSaved($this);
                }
            }
            else
            {
                if (!$this->hasErrors())
                {
                    $Model = get_class($this);
                    $text = "Nije sacuvan Model: $Model ID: $this->id a nije pukla validacija";
                    Yii::app()->errorReport->createAutoClientBafRequest($text);
                }
            }
        }
        return $res;
    }

    public function getModelRelationParams($relation)
    {
        $relations = $this->relations();
        if (strpos(trim($relation), '.') !== FALSE)
        {
            $parts = explode('.', $relation);
            $relation = substr(strstr($relation, '.'), 1);
            $rel_model = $relations[$parts[0]][1]::model();
            return $rel_model->getModelRelationParams($relation);
        }
        else
        {
            if (!isset($relations[$relation]))
            {
                return array(
                    'model' => $this,
                    'column' => $relation,
                );
            }
            else
            {
//                return array(
//                    'model' => $relations[$relation][1]::model(),
//                );
                return $relations[$relation];
            }
        }
    }

    /**
     * funkcija koja vraca relacioni model. Relacija moze biti u vise nivoa(zadata sa tackama)
     * @param string $relation
     * @return \SIMAActiveRecord
     */
    public function getRelationModel($relation)
    {        
        $relations = $this->relations();
        if (strpos(trim($relation), '.') !== FALSE)
        {
            $parts = explode('.', $relation);
            $relation = substr(strstr($relation, '.'), 1);
            $model = $relations[$parts[0]][1]::model();
            return $model->getRelationModel($relation);
        }
        else
        {
            if (isset($this->$relation))
            {
                return $this->$relation;
            }
            else
            {
                $model = $relations[$relation][1]::model();
                return $model;
            }
        }
    }

    public function onListTabs($event)
    {
        $this->raiseEvent("onListTabs", $event);
    }

    /**
     * funkcija koja proverava da li moze da se obrise konkretan model. Ona se predefinise u modelu, a ovde uvek vraca true
     * @return boolean
     */
    public function canDelete()
    {
        return true;
    }

    public function setModelAttributes($attributes = array(), $form_name=null)
    {        
        $columns = $this->getMetaData()->tableSchema->columns;
        if (!is_null($form_name))
        {
            $model_forms = $this->modelForms();            
        }
        //privremeni niz u koji se pamte kolone koje su setovane za CBelongsToRelation
        $temp_columns_set_array = [];
        foreach ($attributes as $key => $value)
        {            
            $is_numeric = false;
            if (
                    (
                        !empty($model_forms) && 
                        !empty($model_forms[$form_name]['columns'][$key]) && 
                        (
                            (is_array($model_forms[$form_name]['columns'][$key]) && $model_forms[$form_name]['columns'][$key][0]==='numberField') ||                            
                            ($model_forms[$form_name]['columns'][$key] === 'numberField')
                        )
                    ) ||
                    (
                        isset($columns[$key]) && strpos($columns[$key]->dbType, 'numeric') !== false
                    )
               )
            {
                $is_numeric = true;
            }
            $number_fields = $this->modelSettings('number_fields');
            if (in_array($key, $number_fields))
            {
                $is_numeric = true;
            }
            $relations = $this->relations();
            //ako je relacija - slucaj kod search fielda
            if (isset($relations[$key]))
            {                
                if (isset($value['ids']))
                {
                    if (($relations[$key][0] === 'CManyManyRelation' || $relations[$key][0] === 'CHasManyRelation'))
                    {
                        $this->$key = $value['ids'];
                    }
                    else if ($relations[$key][0] === 'CBelongsToRelation')
                    {
                        if (gettype(CJSON::decode($value['ids'])) === 'array')
                        {
                            $this->$key = $value['ids'];
                        }
                        else
                        {
                            $rel_column = $relations[$key][2];  
                            {
                                if (!isset($temp_columns_set_array[$rel_column]))
                                {
                                    if (!empty($value['ids']))
                                    {
                                        $temp_columns_set_array[$rel_column] = $value['ids'];
                                    }
                                    $this->$rel_column = $value['ids'];                                    
                                    $this->$key = $relations[$key][1]::model()->findByPk($value['ids']);                                    
                                }
                                else if (!empty($value['ids']) && intval($temp_columns_set_array[$rel_column])!==intval($value['ids']))
                                {
                                    throw new SIMAWarnException("Vrednost za kolonu ".$this->getAttributeLabel($rel_column)." "
                                        . "je unešena dva puta različita!!!");
                                }
                            }
                            
                        }
                    }
                }
            }
            else if (is_array($value))
            {                
                if (isset($value['init']))
                {
                    $attributes[$key] = $value['init'];
                }
                else
                {
                    unset($attributes[$key]);
                }
            }
            else if ($is_numeric)
            {                
                $attributes[$key] = SIMAMisc::userToDbNumeric($value);                
            }
        }
        $this->attributes = $attributes;
        $types = array('text', 'boolean');
        foreach ($this->attributes as $key => $value)
        {
            if (
                    isset($columns[$key]) && 
                    (
                        !in_array($columns[$key]->dbType, $types) || ($columns[$key]->dbType === 'text' && ctype_digit($value))
                    ) && 
                    $this->$key === ''
                )
            {                
                $this->$key = null;
            }
        }
    }

    /**
     * Pretvara korisnicke datume u datume koji mogu da se pretrazuju u bazi
     * @param array $attributes
     * @return array
     */
    private function convertDateTimeAttributes($attributes)
    {
        $columns = $this->getMetaData()->tableSchema->columns;
        foreach ($attributes as $attribute => $value)
        {
            if (isset($columns[$attribute]) && $columns[$attribute]->dbType === 'date')
            {
                $attributes[$attribute] = SIMAHtml::UserToDbDate($value);
            }
            else if (isset($columns[$attribute]) && $this->isTimestampField($columns[$attribute]->dbType))
            {
                $attributes[$attribute] = SIMAHtml::UserToDbDate($value, true);
            }
        }
        return $attributes;
    }

    public function findByAttributes($attributes, $condition = '', $params = array())
    {
        return parent::findByAttributes(
                        $this->convertDateTimeAttributes($attributes), $condition, $params
        );
    }

    public function findAllByAttributes($attributes, $condition = '', $params = array())
    {
        return parent::findAllByAttributes(
                        $this->convertDateTimeAttributes($attributes), $condition, $params
        );
    }

    public function countByAttributes($attributes, $condition = '', $params = array())
    {
        return parent::countByAttributes(
                        $this->convertDateTimeAttributes($attributes), $condition, $params
        );
    }

    public function getAttributeHtml($attribute)
    {
        $labels = $this->attributeLabels();
        if (isset($labels[$attribute]))
        {
            if (is_array($labels[$attribute]))
            {
                if (isset($labels[$attribute]['title']))
                {
                    $onhover = isset($labels[$attribute]['onhover'])?$labels[$attribute]['onhover']:'';
                    return "<span title='".$onhover."'>".$labels[$attribute]['title']."</span>";
                }  
                else
                    return $attribute;
            }
            else
                return $labels[$attribute];
        }
        elseif (strpos($attribute, '.') !== false)
        {
            $segs = explode('.', $attribute);
            $name = array_pop($segs);
            $model = $this;
            foreach ($segs as $seg)
            {
                $relations = $model->getMetaData()->relations;
                if (isset($relations[$seg]))
                    $model = CActiveRecord::model($relations[$seg]->className);
                else
                    break;
            }
            return $model->getAttributeHtml($name);
        }
        else
            return $this->generateAttributeLabel($attribute);
    }
    
    private function collectDisplayFilters($fixed_filter, $select_filter)
    {
        $display_scopes = [];
        if (isset($fixed_filter['display_scopes']))
        {
            if (is_array($fixed_filter['display_scopes']))
            {
                $display_scopes = array_merge($display_scopes,$fixed_filter['display_scopes']);
            }
            else
            {
                $display_scopes[] = $fixed_filter['display_scopes'];
            }
        }
        if (isset($select_filter['display_scopes']))
        {
            if (is_array($select_filter['display_scopes']))
            {
                $display_scopes = array_merge($display_scopes,$select_filter['display_scopes']);
            }
            else
            {
                $display_scopes[] = $select_filter['display_scopes'];
            }
        }
        return $display_scopes;
    }
    
    public function getSumRow($fixed_filter, $select_filter, $columns_type)
    {
        $guiTableSettings = $this->guiTableSettings($columns_type);
        
        $display_scopes = $this->collectDisplayFilters($fixed_filter, $select_filter);
        if (!isset($guiTableSettings['clear_display_scopes']) || $guiTableSettings['clear_display_scopes'])
        {
            $fixed_filter['display_scopes'] = [];
            $select_filter['display_scopes'] = [];
        }
        $criteria = new SIMADbCriteria(array(
            'Model' => get_class($this),
            'model_filter' => $fixed_filter
        ));
        $select_criteria = new SIMADbCriteria(array(
            'Model' => get_class($this),
            'model_filter' => $select_filter
        ));
        $criteria->mergeWith($select_criteria);
        
        $sum_row = null;
        if (isset($guiTableSettings['sums_function']))
        {
            $sum_row = $this->{$guiTableSettings['sums_function']}($criteria, $display_scopes);
        }
        else if (isset($guiTableSettings['sums']) 
                && is_array($guiTableSettings['sums']) 
                && count($guiTableSettings['sums'])>0)
        {            
            $sum_columns = $guiTableSettings['sums'];
            foreach ($sum_columns as $column)
            {
                $temp_criteria = new CDbCriteria();
                $temp_criteria->select = "sum($column) as $column";                
                $criteria->mergeWith($temp_criteria);
            }
            $criteria->order = '';
            $sum_result = $this->findAll($criteria);
            $sum_row = $sum_result[0];        
        }
        
        return $sum_row;
    }
    
    /**
     * Proverava da li je kolona obavezno polje. Kolona moze biti i relacija, tj. strani kljuc
     * prema drugoj tabeli
     * @param string $column
     * @return boolean
     */
    public function isColumnRequired($column)
    {
        $relations = $this->relations();
        $for_req_check = $column;
        if (isset($relations[$column]) && isset($relations[$column][2]))
        {
            $for_req_check = $relations[$column][2];
        }
        
        if ($this->isAttributeRequired($for_req_check))
            return true;
        else
            return false;
    }

    public function checkParentCycle($attribute, $params)
    {
        $baseColumn = 'id';
        if(isset($params['baseColumn']))
        {
            $baseColumn = $params['baseColumn'];
        }
        $parentRelation = 'parent';
        if(isset($params['parentRelation']))
        {
            $parentRelation = $params['parentRelation'];
        }
        
        $baseValue = $this->$baseColumn;
        
        $parentModel = $this->$parentRelation;
        while(isset($parentModel))
        {
            if($parentModel->$baseColumn === $baseValue)
            {
                $this->addError($attribute, Yii::t('BaseModule.SIMAActiveRecord', 'CreatesCycle',['{name}'=>$parentModel->modelLabel()]));
            }
            
            $parentModel = $parentModel->$parentRelation;
        }
    }
    
    public function afterValidate()
    {
        if($this->getScenario() == 'delete')
        {
            $pk = $this->getPrimaryKey();
            if(is_array($pk))
            {
                /// TODO: Milos J.
                /// srediti za pk niz - za kompozitni pk
            }
            else
            {
                $table_name = $this::model()->tableName();
                $this->validateForeignDependencies($table_name, $this->id);
            }
        }
        
        return parent::afterValidate();
    }
    
    public function delete($check_result=true, $throwExceptionOnFail = true)
    {
        try
        {
            $this->setScenario('delete');
            $thiserrors = '';
            if ($check_result)
            {
                $val_res = $this->validate();
                if ($val_res !== true)
                {
                    if ($throwExceptionOnFail)
                    {
                        throw new SIMAWarnModelNotValid($this);
                    }
                    else
                    {
                        return false;
                    }
                        
                }
            }
            
            $res = parent::delete();
            
            if ($check_result && $res === false)
            {        
                if ($throwExceptionOnFail)
                {
                    
                    throw new SIMAExceptionModelNotDeleted($this);
                }
                else
                {
                    $Model = get_class($this);
                    $text = "Nije obrisan Model: $Model ID: $this->id a nije pukla validacija";
                    Yii::app()->errorReport->createAutoClientBafRequest($text);
                    return false;
                }
            }
            
            return $res;
        }
        catch (Exception $e)
        {            
            if ($e->getCode() == 23503) //ako se tabela pojavljuje kao strani kljuc u nekoj drugoj
            {
                /**
                 * ukoliko smo u toku transakcije
                 * mora da se odradi rollback da bi mogao da se odradi novi sql u validateForeignDependencies
                 */
                $current_transaction = Yii::app()->db->getCurrentTransaction();
                if(!is_null($current_transaction))
                {
                    $current_transaction->rollback();
                }
                
                /// iz poruke o gresci se izvlace ime tabele modela i id modela
                /// poruka o gresci za kod 23503 je uvek istog oblika
                
                $errorMessage = $e->getMessage();
                                
                $tableNameMatches = null;
                preg_match('/ERROR:  update or delete on table ".+" violates foreign key constraint/', $errorMessage, $tableNameMatches);
                preg_match('/".+"/', $tableNameMatches[0], $tableNameMatches);
                $tableName = trim($tableNameMatches[0], '"');
                
                $modelIdMatches = null;
                preg_match('/DETAIL:  Key \(id\)=\(\d{1,}\) is still referenced from table/', $errorMessage, $modelIdMatches);
                preg_match('/\(\d{1,}\)/', $modelIdMatches[0], $modelIdMatches);
                $modelId = trim($modelIdMatches[0], '()');
                
                $this->validateForeignDependencies($tableName, $modelId);
                
                /// u slucaju da prodje validacija, da funkcija ne baci gresku, bacicemo mi ipak
                
                $message = Yii::t('SIMAActiveRecord', 'ModelIsUsedInOtherTables', [
                    '{model_label}' => $this->modelLabel(),
                    '{error_message}' => $errorMessage
                ]);
                throw new SIMAWarnException($message);
            }
            else
            {                
                throw $e;
            }
        }
    }
    
    public function haveRelation($relationName)
    {
        $relations = $this->relations();
        if (isset($relations[$relationName]))
        {
            return true;
        }
        
        return false;
    }
    
    /**
     * 
     * @param type $same_obj
     * @return type
     */
    public function compareWithSameObject($same_obj)
    {
        $columns_in_diff = [];
        
        $columns = $this->getMetaData()->tableSchema->columns;
        
        $column_names = array_keys($columns);
        
        foreach($column_names as $column_name)
        {
            if((string)$this->$column_name !== (string)$same_obj->$column_name)
            {
                $columns_in_diff[$column_name] = [
                    $this->$column_name,
                    $same_obj->$column_name
                ];
            }
        }
        
        return $columns_in_diff;
    }
    
    /**
     * 
     * @return type
     */
    public function compareWithDB()
    {
        $model_name = get_called_class();
        
        $same_old_obj = $model_name::model()->findByPk($this->id);
        
        $columns_in_diff = $this->compareWithSameObject($same_old_obj);
        
        return $columns_in_diff;
    }
    
    /**
     * Trazi modele deo po deo i prilikom svake iteracije(broj iteracija zavisi od $offset i $limit) salje niz redova callback funkciji
     * @param function $callback_func
     *          @return bool - ako vrati false, to je signal da se prekine sa daljom iteracijom/pretragom
     * @param SIMADbCriteria $criteria
     * @param int $offset
     * @param int $limit
     */
    public function findAllByParts($callback_func, $criteria=null, $offset=0, $limit=100)
    {        
        if (!is_callable($callback_func))
        {
            throw new SIMAException('SIMAActiveRecord u funkciji findAllByParts nisu lepo zadati parametri!');
        }
        if (empty($criteria))
        {
            $criteria = new SIMADbCriteria();
        }
        $total_rows = $this->count($criteria);
        if(isset($criteria->limit) && $criteria->limit > 0 && $criteria->limit<$total_rows)
        {
            $total_rows = $criteria->limit;
        }
        if($total_rows<$limit)
        {
            $limit = $total_rows;
        }
        $max_while_cycle_exec_time_seconds = ini_get('max_execution_time');        
        while($offset<$total_rows)
        {
            set_time_limit($max_while_cycle_exec_time_seconds);
            $criteria->offset = $offset;
            $criteria->limit = $limit;
            $rows = $this->findAll($criteria);
            
            $callback_result = $callback_func($rows, $offset, $limit);
            if($callback_result === false)
            {
                break;
            }
            
            unset($rows);
            $offset += $limit;
        }
    }
    
    public static function findByVueModelTag($model_tag)
    {
        $trace_cat = 'SIMA.SIMAActiveRecord.findByVueModelTag';
        Yii::beginProfile('mapper',$trace_cat);
        
        $_parts = explode('_', $model_tag);
        $_parts_cnt = count($_parts);
        if (!in_array($_parts_cnt, [2,3]))
        {
            throw new SIMAException('prosledjen los model_tag: '.$model_tag);
        }
        $model_name = $_parts[0];
        if ($_parts_cnt === 2)
        {
            $model_id = $_parts[1];
        }
        else
        {
            $model_id = [ $_parts[1], $_parts[1] ];
        }
        //MilosS(9.9.2018): ovo nema smisla zato sto vise modela koriste istu tabelu, 
        //pa se sustinski ne zna koji je model u pitanju
//        $configTablesToModelsMapper = SIMAModule::getTablesToModelsMapped();
//        
//        $pos = -strpos(strrev($model_tag), '_');
//        $model_table = str_replace('-','.',substr($model_tag, 0, $pos - 1));
//        $model_name = $configTablesToModelsMapper[$model_table];
//        $model_id = substr($model_tag, $pos);
        Yii::endProfile('mapper',$trace_cat);
        return $model_name::model()->findByPk($model_id);
    }
    
    public function findByPkWithCheck($pk, $condition='', array $params=[])
    {
        $model = $this->findByPk($pk, $condition, $params);
        
        if(empty($model))
        {
            throw new SIMAExceptionModelNotFound(get_class($this), $pk);
        }
        
        return $model;
    }
    
    public function findByPkWithWarnCheck($pk, $condition='', array $params=[])
    {
        try
        {
            $model = $this->findByPkWithCheck($pk, $condition, $params);
        }
        catch(SIMAExceptionModelNotFound $e)
        {
            throw new SIMAWarnExceptionModelNotFound(get_class($this), $pk);
        }
        
        return $model;
    }
    
    public function findByPkWithWarnAutoBafCheck($pk, $condition='', array $params=[])
    {
        try
        {
            $model = $this->findByPkWithCheck($pk, $condition, $params);
        }
        catch(SIMAExceptionModelNotFound $e)
        {
            throw new SIMAWarnAutoBafException(Yii::t('BaseModule.Exceptions', 'WarnModelNotFound', [
                '{model_label}' => $this->modelLabel()
            ]));
        }
        
        return $model;
    }
    
    public function findAllByPkWithCheck($pk, $condition='', array $params=[])
    {
        if(is_array($pk))
        {
            $pk = array_unique($pk);
        }
        
        $models = $this->findAllByPk($pk, $condition, $params);
        
        $was_error = false;
        if(is_array($pk))
        {
            if(count($pk) !== count($models))
            {
                $was_error = true;
            }
        }
        else 
        {
            if(empty($models))
            {
                $was_error = true;
            }
        }
        
        if($was_error === true)
        {
            throw new SIMAExceptionModelNotFound(get_class($this), $pk);
        }
        
        return $models;
    }
    
    public function findByModelFilter(array $model_filter=[])
    {        
        return $this->find(new SIMADbCriteria([
            'Model' => get_class($this),
            'model_filter' => $model_filter
        ]));
    }
    
    public function findAllByModelFilter(array $model_filter=[])
    {        
        return  $this->findAll(new SIMADbCriteria([
            'Model' => get_class($this),
            'model_filter' => $model_filter
        ]));
    }
    
    public function countByModelFilter(array $model_filter=[])
    {        
        return  $this->count(new SIMADbCriteria([
            'Model' => get_class($this),
            'model_filter' => $model_filter
        ]));
    }
    
    public function addIfNotExist($attributes)
    {
        $is_added = false;
        $model = $this->findByAttributes($attributes);
        if (is_null($model))
        {
            $class_name = get_called_class();            
            $model = new $class_name();
            foreach ($attributes as $key => $value) 
            {
                $model->$key = $value;
            }
            if ($model->save() === true)
            {
                $is_added = true;
            }
        }
        
        return $is_added;
    }
    
    /**
     * wrapper oko columnChanged s tim sto prethodno vraca true ako je novi element
     * 
     * @param type $columns
     * @param type $glue
     * @param type $accuracy
     * @return bool
     */
    public function columnChangedOrNew($columns, $glue = 'OR', $accuracy = 0.001)
    {
        return $this->isNewRecord || $this->columnChanged($columns, $glue, $accuracy);
    }
    
    /**
     * Vraca da li se vrednost kolone promenila. Za poredjenje koristi $this->__old[].
     * Ukoliko je novi model, uvek vraca false - nema promena
     * @param string/array $columns ime kolone ili niz imena koje treba porediti
     * @param string $glue ako je vise kolona da li rezultat vezuje sa OR ili AND - default OR
     * @param numeric $accurency Ako je kolona numeric, kolika tacnost treba da bude u proceni promene
     */
    public function columnChanged($columns, $glue = 'OR', $accurency = 0.001)
    { 
        //ukoliko je novi model, to nisu promene
        if ($this->isNewRecord)
        {
            return false;
        }
        if (!in_array($glue, ['OR','AND']))
        {
            throw new SIMAExceptionWrongParam('SIMAActiveRecord::columnChanged','$glue',$glue);
        }
        $_all_columns = $this->getMetaData()->tableSchema->columns;
        if (gettype($columns) !== 'array') 
        {
            $columns = [$columns];
        }
        $_changed = ($glue==='OR')?false:true;
        foreach ($columns as $column) 
        {
            if (isset($_all_columns[$column]))
            {
                $type = $_all_columns[$column]->dbType;
            }
            else
            {
                $type = 'virtual';
            }
            
            if ($type == 'integer')
            {
                $_column_changed = intval($this->$column) !== intval($this->__old[$column]);
            }
            else if ($this->isNumericColumn($column))
            {
                if ($this->$column instanceof SIMABigNumber && $this->__old[$column] instanceof SIMABigNumber)
                {
                    $_column_changed = !$this->$column->equals($this->__old[$column]);
                }
                else
                {
                    $_column_changed = !SIMAMisc::areEqual($this->$column, $this->__old[$column], $accurency);
                }
            }
//            elseif (strpos($type, '[]'))
//            {
//            }
//            elseif ($this->isJSONColumn($column))
//            {
//            }
//            elseif ($type == 'boolean')
//            {
//            }
//            elseif ($type === 'date')
//            {
//            }
//            elseif ($this->isTimestampField($type))
//            {
//            }
            else
            {
                $_column_changed = $this->$column != $this->__old[$column];
            }
            
            if ($glue==='OR')
            {
                $_changed = $_changed || $_column_changed;
            }
            else
            {
                $_changed = $_changed && $_column_changed;
            }
        }
        return $_changed;
    }
    
    private function validateForeignDependencies($table_name, $model_id)
    {
        if(strpos($table_name, '.') === false)
        {
            $table_name = 'public.'.$table_name;
        }
        
        $foreign_dependencies = array_diff(SIMASQLFunctions::GetForeignDependencies($table_name, (string)($model_id)), $this->allowed_foreign_dependencies);
        if(!empty($foreign_dependencies))
        {
            $this->afterValidateDependencyError($foreign_dependencies);
        }
    }
    
    private function saveModelChanges()
    {
        try
        {
            $model_change_columns = $this->modelSettings('model_change_columns');
            if(!empty($model_change_columns))
            {
                $change_type = ModelChange::$CHANGE_TYPE_UPDATE;
                if($this->getScenario() == 'delete')
                {
                    $change_type = ModelChange::$CHANGE_TYPE_DELETE;
                }
                else
                {
                    if($this->isNewRecord)
                    {
                        $change_type = ModelChange::$CHANGE_TYPE_INSERT;
                    }
                }
                
                $user_id = null;
                if(Yii::app()->isWebApplication())
                {
                    $user_id = Yii::app()->user->id;
                }
                
                $was_actual_change = false;
                $change_columns = [];
                foreach ($model_change_columns as $column)
                {
                    if($change_type === ModelChange::$CHANGE_TYPE_DELETE
                            || $change_type === ModelChange::$CHANGE_TYPE_UPDATE)
                    {
                        $change_columns[$column]['old'] = $this->__old[$column];
                    }
                    
                    if($change_type === ModelChange::$CHANGE_TYPE_INSERT
                            || $change_type === ModelChange::$CHANGE_TYPE_UPDATE)
                    {
                        $change_columns[$column]['new'] = $this->$column;
                    }
                    
                    if($change_type === ModelChange::$CHANGE_TYPE_INSERT
                            || $change_type === ModelChange::$CHANGE_TYPE_DELETE
                            || ($change_type === ModelChange::$CHANGE_TYPE_UPDATE
                                    && $this->__old[$column] !== $this->$column))
                    {
                        $was_actual_change = true;
                    }
                }

                if($was_actual_change === true)
                {
                    $modelChange = new ModelChange();
                    $modelChange->model_name = get_class($this);
                    $modelChange->model_id = $this->id;
                    $modelChange->change_type = $change_type;
                    $modelChange->change_columns = CJSON::encode($change_columns);
                    $modelChange->user_id = $user_id;
                    $modelChange->save();
                }
            }
        }
        catch(Exception $ex)
        {
            Yii::app()->errorReport->createAutoClientBafRequest($ex->getMessage());
        }
    }
    
    public function isColumnAllowNull($column)
    {
        $columns = $this->getMetaData()->tableSchema->columns;
        return $columns[$column]->allowNull;
    }
    
    public function getStatuses()
    {
        $statuses = $this->modelSettings('statuses');
        
        return array_keys($statuses);
    }
    
    public function checkNumericNumber($attribute, $params)
    {
        if (!is_null($this->$attribute) && SIMAMisc::isValidNumericNumber($this->$attribute, $params['precision'], $params['scale']) === false)
        {
            $this->addError($attribute, Yii::t('SIMAActiveRecord', 'NotValidNumericNumber', [
                '{precision}' => $params['precision'],
                '{scale}' => $params['scale']
            ]));
        }
    }
    
    public function getUnreadCommentsCountForUser(User $user)
    {
        $comment_threads = CommentThread::model()->findAllByAttributes([
            'model' => get_class($this),
            'model_id' => $this->id
        ]);
        
        $cnt = 0;
        foreach ($comment_threads as $comment_thread)
        {
            $cnt += $comment_thread->getUserUnseenCommentsCount($user->id);
        }
        
        return $cnt;
    }
    
    public function count($condition='', $params=[])
    {
        return parent::count($this->parseConditionWithSIMACDbCriteria($condition, $params), $params);
    }
    public function find($condition='', $params=[])
    {
        return parent::find($this->parseConditionWithSIMACDbCriteria($condition, $params), $params);
    }
    public function findAll($condition='', $params=[])
    {
        return parent::findAll($this->parseConditionWithSIMACDbCriteria($condition, $params), $params);
    }
    
    private function parseConditionWithSIMACDbCriteria($condition='', $params=[])
    {        
        if(is_array($condition) && isset($condition['model_filter']))
        {
            $model_filter = $condition['model_filter'];
            unset($condition['model_filter']);
            
            $sima_criteria = new SIMADbCriteria([
                'Model' => get_class($this),
                'model_filter' => $model_filter
            ]);
        
            $condition = new CDbCriteria($condition, $params);

            $condition->mergeWith($sima_criteria);
        }
        
        return $condition;
    }
    
    /**
     * za podrsku scope-a u scope-u
     * @param type $criteria
     */
    public function applyScopes(&$criteria)
    {        
        $criteria->scopes = array_merge((array)$criteria->scopes,(array)$this->dbCriteria->scopes);
        parent::applyScopes($criteria);
    }
    
    public function getThemesModelBelongsTo()
    {
        return [];
    }
    
    public function getDefaultCommentThread()
    {
        $comment_thread = null;

        if ($this->hasAttribute('comment_thread_id'))
        {
            $comment_thread = CommentThread::model()->findByPk($this->comment_thread_id);
        }
        
        return $comment_thread;
    }
    
    /**
     * Scope funkcija koja se redefinise u konkretnom modelu i oznacava da li $user_id ima pristup modelu. Koristi je SIMADefaultLayout
     * @param int $user_id
     * @return $this
     */
    public function hasAccess($user_id)
    {
        return $this;
    }
    
    /**
     * iskopirano iz cmodel i promenjeno da koristi rules_sima
     * @return \CList
     * @throws CException
     */
    public function createValidators()
    {
               $validators=new CList;
               foreach($this->rules_sima() as $rule)
               {
                       if(isset($rule[0],$rule[1]))  // attributes, validator name
                               $validators->add(CValidator::createValidator($rule[1],$this,$rule[0],array_slice($rule,2)));
                       else
                               throw new CException(Yii::t('yii','{class} has an invalid validation rule. The rule must specify attributes to be validated and the validator name.',
                                       array('{class}'=>get_class($this))));
               }
               return $validators;
    }
    
    /**
     * rules dopunjava pravilima za date i timestamp polja (ako za njih vec ne postoje rule-vi)
     * @return type
     */
    private function rules_sima()
    {
        $rules = $this->rules();
        
        $attrs_for_rules = [];
        
        $database_columns = $this->getMetaData()->tableSchema->columns;
        foreach($database_columns as $database_column)
        {
            if($database_column->dbType === 'date')
            {
                $attrs_for_rules[$database_column->name] = 'date_format';
            }
            else if($this->isTimestampField($database_column->dbType))
            {
                $attrs_for_rules[$database_column->name] = 'datetime_format';
            }
        }
                
        foreach($attrs_for_rules as $date_attr => $type)
        {
            $found = false;
            foreach($rules as $rule)
            {
                if($rule[0] === $date_attr && $rule[1] === 'date' && !isset($rule['format']))
                {
                    $found = true;
                    break;
                }
            }
            
            if($found === false)
            {
                $rule_to_add = [$date_attr, 'checkDateFormat', 'format' => Yii::app()->params[$type]];
                array_unshift($rules, $rule_to_add);
            }
        }
        
        return $rules;
    }
    
    /**
     * iskopirano vecinom iz CDateValidator::validateAttribute
     * @param type $attribute
     * @param type $params
     * @return type
     */
    public function checkDateFormat($attribute, $params)
    {
        $value = $this->$attribute;
        $format = $params['format'];
        
        if(empty($value))
            return;
        
        if($value === 'now()')
            return;

        $valid = false;

        if(!is_array($value))
        {
            $formats = is_string($format) ? [$format] : $format;
            foreach($formats as $format)
            {
                $timestamp = CDateTimeParser::parse($value, $format, ['month'=>1,'day'=>1,'hour'=>0,'minute'=>0,'second'=>0]);
                if($timestamp !== false)
                {
                    $valid = true;
                    break;
                }
            }
        }

        if(!$valid)
        {
            $message = Yii::t('yii','The format of {attribute} is invalid.', [
                '{attribute}' => $this->getAttributeLabel($attribute)
            ]).' - '.$format;
            $this->addError($attribute, $message);
        }
    }
    
    public function updateModels()
    {
        if(Yii::app()->isWebApplication() && Yii::app()->getUpdateModelViews() && Yii::app()->controller !== null)
        {
            Yii::app()->controller->raiseUpdateModels($this);
        }
    }
    
    public function getAllCommentThreadsWithNotSystemCommentsCount()
    {
        return CommentThread::model()->count($this->getFilterForCommentThreadsWithNotSystemComments());
    }
    
    public function getAllCommentThreadsWithNotSystemComments()
    {
        return CommentThread::model()->findAll($this->getFilterForCommentThreadsWithNotSystemComments());
    }
    
    private function getFilterForCommentThreadsWithNotSystemComments()
    {
        return [
            'model_filter' => [
                'model' => get_class($this),
                'model_id' => $this->id
            ],
            'scopes' => 'onlyWithUserComments'
        ];
    }
    
    public function getAllNotSystemCommentsCount()
    {
        return Comment::model()->count([
            'model_filter' => [
                'comment_thread' => [
                    'model' => get_class($this),
                    'model_id' => $this->id
                ]
            ],
            'scopes' => 'without_systems'
        ]);
    }
    
    /**
     * 
     * @param string $status
     * @param bool $new_status
     * @return bool
     */
    public function isModelNewStatusRevert(string $status, bool $new_status)
    {
        $columns = $this->getMetaData()->tableSchema->columns;
        $allow_null = $columns[$status]->allowNull ? true : false;

        if ($allow_null)
        {
            return is_null($new_status);
        }
        else
        {
            return $new_status === false;
        }
    }
    
    /**
     * 
     * @param string $status - naziv statusa
     * @param bool $is_revert - da li je check za revert
     * @return array - spisak gresaka
     */
    public function checkModelStatusAccessConfirmOrRevert(string $status, bool $is_revert)
    {
        $errors = [];

        $statuses = $this->modelSettings('statuses');

        if ($is_revert === true)
        {
            if (isset($statuses[$status]['checkAccessRevert']))
            {
                $this->checkModelStatusAccessConfirmOrRevertInternal($status, $statuses[$status]['checkAccessRevert'], $errors, true);
            }
        }
        else
        {
            if (isset($statuses[$status]['checkAccessConfirm']))
            {
                $this->checkModelStatusAccessConfirmOrRevertInternal($status, $statuses[$status]['checkAccessConfirm'], $errors);
            }
        }
        
        return $errors;
    }
    
    private function checkModelStatusAccessConfirmOrRevertInternal(string $status, $status_params, array &$errors = [], bool $is_revert = false)
    {
        if (gettype($status_params) == 'boolean')
        {
            if (!$status_params)
            {
                if (!$is_revert)
                {
                    $tag = SIMAHtml::getTag($this);
                    $text = "Stavljen je false za potvrdu $status nad modelom $tag";
                    Yii::app()->errorReport->createAutoClientBafRequest($text);
                }

                $errors[] = Yii::t('BaseModule.Statuses', $is_revert ? 'ThisConfirmationCanNotBeReverted' : 'ThisConfirmationCanNotBeSigned');
            }
        }
        elseif (method_exists($this, $status_params))
        {
            $ret = $this->$status_params();

            if ($ret === false)
            {
                $tag = SIMAHtml::getTag($this);
                $confirm_text = $is_revert ? 'opovrgne' : 'potvrdi';
                $text = "Ne postoji opis zasto klijent ne moze da $confirm_text model $tag | $status, a treba da bude u funkciji";
                Yii::app()->errorReport->createAutoClientBafRequest(
                    $text,
                    3104,
                    'SIMA_razvoj_01_backend_AutoBafReq Ne postoji opis zasto klijent ne moze da potvrdi model a treba da bude u funkciji'
                );

                $errors[] = Yii::t('BaseModule.Statuses', $is_revert ? 'DontHavePermissionForThisRevert' : 'DontHavePermissionForThisConfirmation');
            }
            elseif (gettype($ret)==='array')
            {
                $errors = array_merge($errors, $ret);
            }
        }
        else //proverava pravo pristupa
        {
            if (!Yii::app()->user->checkAccess($status_params,[],$this))
            {
                $errors[] = Yii::t('BaseModule.Statuses', $is_revert ? 'DontHavePrivilegeForThisRevert' : 'DontHavePrivilegeForThisConfirmation');
            }
        }
    }
    
    /**
     * prazna f-ja koju svaki model moze da redefinise ako treba da radi zakljucavanje pre izmena
     * trenutno se koristi samo za fajlove
     * primer zbog kog mora za sve modele ModelController::actionModelConfirm
     * @param type $parse_exception
     */
    public function beforeChangeLock($parse_exception=false)
    {
    }
    
    /**
     * prazna f-ja koju svaki model moze da redefinise ako treba da radi otkljucavanje nakon izmena
     * trenutno se koristi samo za fajlove
     * primer zbog kog mora za sve modele ModelController::actionModelConfirm
     */
    public function afterChangeUnLock()
    {
    }
    
    /**
     * Scope za postavljanje offset-a i limit-a
     * @param int $offset
     * @param int $limit
     * @return $this
     */
    public function withOffsetAndLimit($offset, $limit)
    {
        $this->getDbCriteria()->mergeWith([
            'offset' => $offset,
            'limit' => $limit
        ]);
        
        return $this;
    }
    
    public function isNumericColumn($column)
    {
        $columns = $this->getMetaData()->tableSchema->columns;
        
        if (SIMAMisc::stringEndsWith($column, '_sbn'))
        {
            $column = str_replace('_sbn', '', $column);
        }
        
        return isset($columns[$column]) && strpos($columns[$column]->dbType, 'numeric') !== false;
    }
    
    public function parseNumericColumn($column)
    {
        $columns = $this->getMetaData()->tableSchema->columns;
        
        $is_column_end_with_sbn = false;
        if (SIMAMisc::stringEndsWith($column, '_sbn'))
        {
            $is_column_end_with_sbn = true;
            $column = str_replace('_sbn', '', $column);
        }
        
        $value = $this->getAttribute($column);
        
        if ($value === null || $value === '' || !isset($columns[$column]))
        {
            return null;
        }
        
        $is_column_in_big_numbers_array = in_array($column, $this->modelSettings('big_numbers'));
        $is_column_in_big_numbers_report_array = in_array($column, $this->modelSettings('big_numbers_report'));
        
        if ($is_column_end_with_sbn || $is_column_in_big_numbers_array || $is_column_in_big_numbers_report_array)
        {
            $new_value = SIMABigNumber::fromString($value, SIMAMisc::getScaleFromDbNumericField($columns[$column]->dbType));
        }
        else
        {
            $new_value = (double) $value;
        }
        
        if (!$is_column_end_with_sbn && $is_column_in_big_numbers_report_array)
        {
            $model_class = get_class($this);
            Yii::app()->errorReport->createAutoClientBafRequest("
                Za model $model_class je pozvana kolona $column bez _sbn reci u nazivu, a ta kolona je definisana u big_numbers_report u model settingsu tog modela.
            ");
        }
        
        return $new_value;
    }
    
    /**
     * 
     * @param SIMAActiveRecord $model
     * @param type $run_in_transaction
     * @param type $send_update_percentage_events
     * @param type $internal_update_counter
     * @param type $update_percentage_max
     * @param type $save_merge_errors
     * @return array of strings
     */
    public function modelMergeWith(SIMAActiveRecord $model, $run_in_transaction=true, $send_update_percentage_events=false, $internal_update_counter=0, $update_percentage_max=100, $save_merge_errors=false)
    {
        $merger = new SIMAModelMerger($this, $model, $send_update_percentage_events);
        $merger->setRunInTransaction($run_in_transaction);
        $merger->setInternalUpdateCounter($internal_update_counter);
        $merger->setUpdatePercentageMax($update_percentage_max);
        if($save_merge_errors === true)
        {
            $merger->saveMergeErrors();
        }
        $merger->run();
        $this->refresh();
        
        return $merger->getMergeErrors();
    }
}

class CHasManyPGARRAY extends CActiveRelation
{
    
}
