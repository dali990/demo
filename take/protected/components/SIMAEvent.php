<?php

class SIMAEvent extends CEvent
{
    private $_results = array();
    
    public function getResults()
    {
        return $this->_results;
    }
    
    public function addResult($result)
    {
        $this->_results[] = $result;
    }
}
