<?php

class SIMAActiveRecordDisplayGUI extends SIMAActiveRecordGUI
{
    public function hasDisplay()
    {
        return true;
    }
    
    public function getDisplayModel()
    {
        return $this->owner;
    }
    
    public function getDisplayAction()
    {
        return 'defaultLayout';
    }
}