<?php

/**
 * ukoliko se u ovom modelu pravi novi SIMADbCriteria, obavezno setovati DBbackend
 */
class SIMADbCriteriaUnOptimized extends CDbCriteria
{
    protected $DBbackend = null;
    protected $spech_chars = [
        'mysql' => [
            'col_quotes' => '`'
        ],
        'pgsql' => [
            'col_quotes' => '"'
        ]
    ];
    //specijalni karakteri po kojima se ne vrsi pretraga u text pretrazi. U spisak ne ulaze wildcard specijalni karakteri
    private $text_search_special_characters = ['"',"'",'\\','/','!','#','%','&','(',')','+',',','-',':',';','<','=','>','?','@','[',']','_','`','{','|','}','~'];
    private $wild_card_special_characters = ['^','.','*','$'];
    
    public $ids = '';//id-evi koji se dobiju prilikom primenjivanja model_filter-a
    
    //promenljiva koja oznacava da li da se pamte ids-evi(uvedeno je zbog nekih slucajeva kada imamo ogroman broj ids-eva i pukne memory limit)
    //Mora da se vodi racuna kada se stavlja na true, u zavisnosti koliki niz id-eva ocekujemo
    public $save_ids = false;
    
    public function mergeWith($criteria,$operator='AND')
    {
        if ($this->save_ids === true && !empty($criteria->ids))
        {            
            $this_ids_array = empty($this->ids)?[]:explode(",", $this->ids);
            $criteria_ids_array = explode(",", $criteria->ids);
            if (strtolower($operator) === 'and')
            {                
                if (empty($this_ids_array))
                {
                    $array_res = $criteria_ids_array;
                }
                else
                {
                    $array_res = array_intersect($this_ids_array, $criteria_ids_array);
                }
            }
            else if (strtolower($operator) === 'or')
            {
                $array_res = array_merge($this_ids_array, $criteria_ids_array);
            }
            
            $this->ids = implode(',', $array_res);
        }
        parent::mergeWith($criteria, $operator);
    }
    
    public function __construct($params=array(), $save_ids=false)
    {
        if (isset($params['Model']) && isset($params['model_filter']))
        {
            $Model = $params['Model'];
            $model_filter = $params['model_filter'];
        }
        
        if (isset($params['Model']))
        {
            unset($params['Model']);
        }
        if (isset($params['alias']))
        {
            $this->alias = $params['alias'];
        }
        if (isset($params['model_filter']))
        {
            unset($params['model_filter']);
        }
        
        $this->save_ids = $save_ids;        

        parent::__construct($params);
        
        if (isset($Model) && isset($model_filter))
        {
            $this->filterParser($Model, $model_filter);
        }
    }
    
    protected function filterParser($Model, $filter)
    {
        $alias = isset($this->alias)?$this->alias:'t';
        
        $criteria = new SIMADbCriteria();
        $criteria->alias = $alias;
        $criteria->save_ids = $this->save_ids;
        
        if (!empty($filter[0]) && is_string($filter[0]) && in_array(strtolower($filter[0]), ['not', 'and', 'or']))
        {
            $filter_type = strtolower($filter[0]);
            array_shift($filter);
            if ($filter_type === 'not')
            {
                if (count($filter) !== 1)
                {
                    throw new SIMAException(Yii::t('SIMADbCriteria', 'NotModelFilterMustHaveOnlyOneCondition'));
                }

                $temp_criteria = new SIMADbCriteria();
                $temp_criteria->alias = $alias;
                $temp_criteria->filterParser($Model, $filter[0]);
                $temp_criteria->condition = "not ({$temp_criteria->condition})";
                $criteria->mergeWith($temp_criteria);
            }
            else
            {
                $merge_type = $filter_type !== 'or';
                foreach ($filter as $_value)
                {
                    /**
                     * izbacivanje elemenata koje ne treba simadbcriteria da obradjuje
                     * ovo se desava samo pod and i/ili or
                     * ZA SADA JOS UVEK NE MOZE DA SE IZBACI
                     * JER SE NE PROPUSTAJU PARAMETRI
                     */
//                    $cdbcriteria_condition = [];
//                    if(is_array($_value) && isset($_value['scopes']))
//                    {
//                        $cdbcriteria_condition['scopes'] = $_value['scopes'];
//                        unset($_value['scopes']);
//                    }
//                    if(!empty($cdbcriteria_condition))
//                    {
//                        $cdbcriteria = new CDbCriteria($cdbcriteria_condition);
//                        $criteria->mergeWith($cdbcriteria, $merge_type);
//                    }
                    
                    $temp_criteria = new SIMADbCriteria();
                    $temp_criteria->alias = $alias;
                    $temp_criteria->filterParser($Model, $_value);
                    $criteria->mergeWith($temp_criteria, $merge_type);
                }
            }
        }
        else
        {            
            $model = new $Model(null);
            $model->setTableAlias($alias);
            $model->setScenario('filter');
            $model->attachBehaviors($model->behaviors());
            $model->setAttributesFilter($filter);            
            $criteria->applyModel($model);
        }
        $this->mergeWith($criteria);        
    }
    
    /**
     * proverava sve zadate kriterijume za dati model (filtere, textsearch itd.) i primenjuje ih tako sto transformise svaki parametar
     * u SIMADbCriteria 
     * @param SIMAActiveRecord $model
     * @throws Exception
     */
    public function applyModel($model)
    {        
        $this->DBbackend = $model->DbConnection->DriverName;
        if (gettype($model->scopes) == 'string' && $model->scopes!='') 
        {
            $model->scopes = array($model->scopes);
        }
        if (gettype($model->display_scopes) == 'string' && $model->display_scopes!='') 
        {
            $model->display_scopes = array($model->display_scopes);
        }
        if (gettype($model->filter_scopes) == 'string' && $model->filter_scopes!='') 
        {
            $model->filter_scopes = array($model->filter_scopes);
        }
        /**
         * nece se u skorije vreme izbacivati jer dosta drugog koda zavisi
         */
//        if(!empty($model->scopes))
//        {
//            SIMADbCriteria::logWronglyUsedAttribute(__METHOD__, 'scopes - '.CJSON::encode($model->scopes), $model);
//        }
//        if(!empty($model->display_scopes))
//        {
//            SIMADbCriteria::logWronglyUsedAttribute(__METHOD__, 'display_scopes - '.CJSON::encode($model->display_scopes), $model);
//        }
//        if(!empty($model->filter_scopes))
//        {
//            SIMADbCriteria::logWronglyUsedAttribute(__METHOD__, 'filter_scopes - '.CJSON::encode($model->filter_scopes), $model);
//        }
        $model->display_scopes = array_merge(
            isset($model->scopes)?$model->scopes:array(),
            isset($model->display_scopes)?$model->display_scopes:array()
        );
        $model->filter_scopes = array_merge(
            isset($model->scopes)?$model->scopes:array(),
            isset($model->filter_scopes)?$model->filter_scopes:array()
        );
        
//        $this->together = true;
        $temp_criteria = new SIMADbCriteria;
        $temp_criteria->DBbackend = $this->DBbackend;
        if ($model->with != null)
        {
//            SIMADbCriteria::logWronglyUsedAttribute(__METHOD__, 'with - '.$model->with, $model);
            $temp_criteria->with = $model->with;
        }
        if ($model->display_scopes != null) ///TODO(srecko): dodati da moze vise scope-ova
        {
            $temp_criteria->scopes = $model->display_scopes;
        }
        
        if (isset($model->limit) && $model->limit !== null)
        {
//            SIMADbCriteria::logWronglyUsedAttribute(__METHOD__, 'limit - '.$model->limit, $model);
            $temp_criteria->limit = $model->limit;
        }
        
        if (isset($model->offset) && $model->offset !== null)
        {
//            SIMADbCriteria::logWronglyUsedAttribute(__METHOD__, 'offset - '.$model->offset, $model);
            $temp_criteria->offset = $model->offset;
        }
        
        if ($model->ids != null)
        {
            $this->putInCondition('id', $model->ids, array(), $model->getTableAlias());
        }

        /** klasicna pretraga - bar jedno REKURZIVNO polje mora da zadovoljava kriterijum */
        if ($model->text != null && $model->group_by == null)
        {
//            SIMADbCriteria::logWronglyUsedAttribute(__METHOD__, 'text preimenovati u full_text_search - '.$model->text, $model);
            $this->SearchTextCondition($model, $model->text);
            $temp_criteria->distinct = true;
        }
        else
        {
            $this->SearchTextCondition($model, '');
            $temp_criteria->distinct = true;
        }
        //filteri koji se takodje primenjuju prilikom normalnog upita(ZBOG RACUNA)//TODO(srecko): mozda treba izmeniti?
        $temp_criteria->FilterConditionRecursive($model, $model->getTableAlias(), 'select_filters');
        
        //statusi kolona koji se takodje primenjuju prilikom normalnog upita (ZBOG RASKNJIZAVANJA)//TODO(srecko): mozda treba izmeniti?
        $temp_criteria->ColumnsStatusCondition($model, $model->getTableAlias());
        
        //TODO: odraditi ponovo group by
//        if ($model->group_by != null && $model->text == null) //ne moze zbog select uslova i group_by i text
//        {
//            $temp_criteria->select = trim($model->group_by, ',');
//            $temp_criteria->select.= ', ' . $model_settings['group_by_select'];
//            $temp_criteria->group = rtrim($model->group_by, ',');
//        }        
        
        //zabranjeno jer se u trenutku slanja ne zna alias
        if ($model->order_by != null && trim($model->order_by) != '')
        {
//            SIMADbCriteria::logWronglyUsedAttribute(__METHOD__, 'order_by - '.$model->order_by, $this);
            $parts = explode(" ", $model->order_by);
            $rel_parts = explode('.', $parts[0]);
            $order_alias = $model->getTableAlias();
            $order_column = '';
            $curr_model = $model;
            $i = 0;
            $len = count($rel_parts);
            $relation = '';
            foreach ($rel_parts as $rel_part)
            {
                $curr_relations = $curr_model->relations();
                if (isset($curr_relations[$rel_part]))
                {
                    if (isset($curr_relations[$rel_part]['alias']))
                    {
                        $curr_rel_alias = $curr_relations[$rel_part]['alias'];
                    }
                    else
                    {
                        $curr_rel_alias = $rel_part;
                    }
                    if ($relation !== '')
                    {
                        $relation .= '.' . $rel_part;
                    }
                    else
                    {
                        $relation = $rel_part;
                    }
                    $order_alias = $curr_rel_alias;
                    $curr_model = $curr_relations[$rel_part][1]::model();
                    //ako je relacija poslednji element u nizu onda dovlacimo iz te relacije kolonu po kojoj ce se vrsiti order
                    if ($i == $len - 1)
                    {
                        $database_columns = $curr_relations[$rel_part][1]::model()->getMetaData()->tableSchema->columns;
                        $default_order = $curr_model->modelSettings('default_order');
                        if(!is_string($default_order))
                        {
                            throw new SIMAExceptionDbCriteriaDefaultOrderInvalid($curr_model, 'not string');
                        }
                        if ($default_order !== '')
                        {
                            $order_column = $default_order;
                        }
                        else if (isset($database_columns['display_name']))
                        {
                            $order_column = 'display_name';
                        }
                        else
                        {
                            throw new SIMAWarnException('U modelu ' .  get_class($curr_model) . ' morate postaviti default order.');
                        }
                    }
                    $i++;
                }
                else
                {
                    $order_column = $rel_part;
                }
            }
            $direction = '';
            if (isset($parts[1]))
            {
                $direction = $parts[1];
            }

            $temp_criteria->with = $relation;
//            $temp_criteria->with = [
//                $relation => [
//                    'select' => $order_column,
//                    'joinType' => 'JOIN',
//                    'alias' => $order_alias,
//                    'condition' => $order_alias.".".$order_column." IS NOT NULL"
//                ]
//            ];
            $temp_criteria->order = $order_alias.".".$order_column." ".$direction;
        }
        
        $this->mergeWith($temp_criteria);
        
    }
    
    /**
     * 
     * @param type $model
     * @param type $text
     * @return parsira tekst za pretragu na vise reci i za svaku rec poziva funkciju SearchTextConditionRecursive koja rekurzivno pretrazuje datu rec, koristi se 
     * za textSearch
     */
    private function SearchTextCondition($model, $text)
    {        
        $alias = $model->getTableAlias();
        
        
        $temp_criteria = new SIMADbCriteria;
        $temp_criteria->DBbackend = $this->DBbackend;
        $temp_criteria->FilterConditionRecursive($model, $alias);
        
        if ($text != '')
        {
            $empty_search = true;
            $exploded = explode(' ', $text);
            foreach ($exploded as $part)
            {
                $empty_search = false;
                $temp_condition = new SIMADbCriteria();
                $temp_condition->DBbackend = $this->DBbackend;
                $temp_condition->SearchTextConditionRecursive($model, $alias, $part);
                if ($this->DBbackend === 'pgsql')
                {
                    $temp_condition->select = "array_agg( $alias.id::text) as ids";
                }
                else if ($this->DBbackend === 'mysql')
                {
                    error_log(__METHOD__.' - nije pokriveno - 5 - mysql');
                    $temp_condition->select = "GROUP_CONCAT($alias.id SEPARATOR ',') as ids";
                }
                else
                {
                    throw new Exception('nije napisan DBBackend1');
                }

                $temp_condition->alias = $alias;
                $modelll = $model::filter(array());
                $modelll->setTableAlias($alias);
                $temp_criteria->scopes = $model->filter_scopes;     
                
//                 $modelll = $model::model();
                
                //moze da se resetuje jer defaultscope nije predmet uslova, nego samo ono sto je zadato
                $result = $modelll->resetScope()->find($temp_condition);
//                $result = $modelll->find($temp_condition);

                $temp2_condition = new CDbCriteria();
                if ($this->DBbackend === 'pgsql')
                {
                    $new_result = str_replace(array('{', '}'), array('(', ')'), $result->ids);
                }
                else if ($this->DBbackend === 'mysql')
                {
                    error_log(__METHOD__.' - nije pokriveno - 7 - mysql');
                    $new_result = '(' . $result->ids . ')';
                }
                else
                {
                    throw new Exception('nije napisan DBBackend2');
                }

                if (!empty($result->ids))
                {
                    $temp2_condition->condition = $alias . '.id in ' . $new_result;
                }
                else
                {
                    $temp2_condition->condition = $alias . '.id=-1';
                }



                $temp_criteria->mergeWith($temp2_condition);
            }
            if ($empty_search)
            {
                error_log(__METHOD__.' - ne bi smelo da udje ovde');
                return;
            }

            //        $temp_criteria->limit=400;
    //        $temp_criteria->order = $model->getTableAlias().'.id';
        }
        
//        $alias = 
        
//        $temp_criteria->select = "'(' || string_agg( t.name::text, ',' ) || ')' as ids"; //$model->getTableAlias().'.id';
        if ($this->DBbackend === 'pgsql')
        {
            $temp_criteria->select = "array_agg( $alias.id::text) as ids";
        }
        else if ($this->DBbackend === 'mysql')
        {
            error_log(__METHOD__.' - nije pokriveno - 12 - mysql');
            $temp_criteria->select = "GROUP_CONCAT($alias.id SEPARATOR ',') as ids";
        }
        else
        {
            throw new Exception('nije napisan DBBackend1');
        }

        $temp_criteria->alias = $model->getTableAlias();
        $modelll = $model::filter(array());
//        $modelll = $model::model();
        $modelll->setTableAlias($alias);
        $temp_criteria->scopes = $model->filter_scopes;        
        
        $result = $modelll->resetScope()->find($temp_criteria);        
//        $result = $modelll->find($temp_criteria);
        $temp2_criteria = new CDbCriteria();
        if ($this->DBbackend === 'pgsql')
        {
            $new_result = str_replace(array('{', '}'), '', $result->ids);
        }
        else if ($this->DBbackend === 'mysql')
        {
            error_log(__METHOD__.' - nije pokriveno - 14 - mysql');
            $new_result = $result->ids;
        }
        else
        {
            throw new Exception('nije napisan DBBackend2');
        }
        
        if ($this->save_ids === true)
        {            
            $this->ids = $new_result;
        }

        if (!empty($result->ids))
        {
            $temp2_criteria->condition = "$alias.id in ($new_result)";
        }
        else
        {
            $temp2_criteria->condition = "$alias.id=-1";
        }

        $this->alias = $alias;
        $this->mergeWith($temp2_criteria);        
    }
    
    /**
     * 
     * @param type $model
     * @param type $alias
     * @param type $rel_alias
     * @param type $param
     * @return  pretrazuje rec u modelu koja se salje kao parametar CDbCritera-e, gde za svaku kolonu koja je definisana u textSearch atributu vrsi proveru, a ako je ta kolona relacija
     * onda rekurzivno poziva samu sebe za tu relaciju. Koristi se za textSearch.
     */
    private function SearchTextConditionRecursive($model, $alias, $text, $processed_models=[])
    {
        //MilosS(13.9.2017) - ostavljeno zakomentarisano radi demonstracije
//        error_log('ULAZ: '.SIMAMisc::toTypeAndJsonString($processed_models));
        if (in_array(get_class($model), $processed_models))
        {
            //MilosS(13.9.2017) - ostavljeno zakomentarisano radi demonstracije
            //MilosJ(21.11.2017.) - odkomentarisano da bi se nasle situacije pod kojima ce se dogoditi
            error_log(__METHOD__.' - POKLAPA SE '.get_class($model));
            error_log(__METHOD__.' - ULAZ: '.SIMAMisc::toTypeAndJsonString($processed_models));
            error_log(__METHOD__.' - $text: '.SIMAMisc::toTypeAndJsonString($text));
            if(Yii::app()->isWebApplication())
            {
                error_log(__METHOD__.' - $_GET: '.SIMAMisc::toTypeAndJsonString(filter_input_array(INPUT_GET)));
                error_log(__METHOD__.' - $_POST: '.SIMAMisc::toTypeAndJsonString(filter_input_array(INPUT_POST)));
            }
            return;
        }
        else
        {
            array_push($processed_models, get_class($model));

            $_q = $this->spech_chars[$this->DBbackend]['col_quotes'];
            $search_columns = $model->modelSettings('textSearch');
            if (count($search_columns) === 0)
            {
                Yii::app()->errorReport->createAutoClientBafRequest("Za model ".get_class($model)." je pozvana pretraga po tekstu, a nije zadata nijedna kolona u textSearch-u u modelSettings-u.");
            }

            foreach ($search_columns as $col => $col_type)
            {
                $text2 = $text;
                $uniq = SIMAHtml::uniqid();
                $mini_condition = new SIMADbCriteria();
                $mini_condition->DBbackend = $this->DBbackend;
                $_col_type = $col_type;
                if (is_array($col_type))
                {
                    $_col_type = $col_type[0];
                }
                switch ($_col_type)
                {
                    case 'text':                            
    //                            $options = [];                            
                        $wildcard = false;

                        if (is_array($col_type))
                        {
                            if (isset($col_type['options']))
                            {
                                if (in_array('wildcard', $col_type['options']))
                                {
                                    $text2 = $this->wildCard($text2);
                                    $wildcard = true;
                                }
                            }
                            if (isset($col_type['use_cyrillic']) && $col_type['use_cyrillic'] === true)
                            {
                                $search_text_cyrillic = SIMAMisc::convertLatinToCyrillic($text2);
                                $search_text_latin = SIMAMisc::convertCyrillicToLatin($text2);                                    
                            }
                        }


                        //MilosS(12.7.2016) - zakomentarisano dok ne smislimo nesto bolje
                        //$text2 = str_replace($this->text_search_special_characters, '', $text2);

                        if (!$wildcard)
                        {
                            //$text2 = str_replace($this->wild_card_special_characters, '', $text2);
                            $text2 = preg_quote($text2);
                        }
                        $text2 = $this->SerbianLettersSearch($text2);


                        //-------------------------------------

                        if (!empty($search_text_cyrillic) && !empty($search_text_latin))
                        {
                            $additional_pgsql = ' or '. $alias . '.'.$_q.$col.$_q.' ~* :match2' . $uniq;
                            $additional_mysql = $alias . '.'.$_q.$col.$_q.' like "%'.$search_text_cyrillic.'%" or '.$alias . '.'.$_q.$col.$_q.' like "%'.$search_text_latin.'%"';
                            $condition_params = [':match'.$uniq => $search_text_cyrillic, ':match2'.$uniq => $search_text_latin];
                        } else {
                            $additional_pgsql = '';
                            $additional_mysql = $alias . '.'.$_q.$col.$_q.' like "%'.$text2.'%"';
                            $condition_params = [':match'.$uniq => $text2];
                        }
                        if ($this->DBbackend === 'pgsql')
                        {
                            $mini_condition->condition = $alias . '.'.$_q.$col.$_q.' ~* :match' . $uniq . $additional_pgsql;
                        }
                        else if($this->DBbackend === 'mysql')
                        {
                            error_log(__METHOD__.' - nije pokriveno - 15 - mysql');
                            $mini_condition->condition = $additional_mysql;
                        }
                        $mini_condition->params = $condition_params;


                        //------------------------------------


    //                            if (!empty($search_text_cyrillic) && !empty($search_text_latin))
    //                            {
    //                                if ($this->DBbackend === 'pgsql')
    //                                    $mini_condition->condition = $alias . '.'.$_q.$col.$_q.' ~* :match' . $uniq.' || '. $alias . '.'.$_q.$col.$_q.' ~* :match2' . $uniq;
    //                                else if($this->DBbackend === 'mysql')
    //                                    $mini_condition->condition = $alias . '.'.$_q.$col.$_q.' like "%'.$search_text_cyrillic.'%" || '.$alias . '.'.$_q.$col.$_q.' like "%'.$search_text_latin.'%"';
    //                                $mini_condition->params = array(':match'.$uniq => $search_text_cyrillic, ':match2'.$uniq => $search_text_latin);
    //                            }
    //                            else
    //                            {
    //                                if ($this->DBbackend === 'pgsql')
    //                                    $mini_condition->condition = $alias . '.'.$_q.$col.$_q.' ~* :match' . $uniq;
    //                                else if($this->DBbackend === 'mysql')
    //                                    $mini_condition->condition = $alias . '.'.$_q.$col.$_q.' like "%'.$text2.'%"';
    //                                $mini_condition->params = array(':match'.$uniq => $text2);
    //                            }

                        break;
                    case 'integer':                            
                        $mini_condition->condition = 'CAST(' . $alias . '."' . $col . '" AS TEXT) ~* :match' . $uniq;
                        $mini_condition->params = array(':match'.$uniq => preg_quote($text2));
                        break;
                    case 'phone_number':
                        $mini_condition->FilterPhoneNumber($col, $text2, $alias); 
                        break;                            
                    case 'relation':

                        $relations = $model->relations();
                        $relation = $relations[$col];
    //                            if (isset($relation['alias']))
    //                                $relation_alias = $relation['alias'];
    //                            else
    //                                $relation_alias = $col;
                        $relation_alias = 't'.SIMAHtml::uniqid();
                        switch ($relation[0])
                        {
                            case 'CHasManyRelation':
                                {   
                                        $mini_condition->join = 'left join '.$relation[1]::model()->tableName().' '.$relation_alias.' on '.$alias.'.id='.$relation_alias.'.'.$relation[2];
    //                                            $mini_condition->SearchTextConditionRecursive($relation[1]::model(), $relation_alias, $text);
                                        $mini_condition->SearchTextConditionRecursive($relation[1]::filter(array()), $relation_alias, $text, $processed_models);
                                }
                                break;
                            case 'CHasOneRelation':
                                {
                                        $mini_condition->join = 'left join '.$relation[1]::model()->tableName().' '.$relation_alias.' on '.$alias.'.id='.$relation_alias.'.'.$relation[2];
    //                                            $mini_condition->SearchTextConditionRecursive($relation[1]::model(), $relation_alias, $text);
                                        $mini_condition->SearchTextConditionRecursive($relation[1]::filter(array()), $relation_alias, $text, $processed_models);
                                }
                                break;
                            case 'CBelongsToRelation':
                                {
                                        $mini_condition->join = 'left join '.$relation[1]::model()->tableName().' '.$relation_alias.' on '.$relation_alias.'.id='.$alias.'.'.$relation[2];
    //                                            $mini_condition->SearchTextConditionRecursive($relation[1]::model(), $relation_alias, $text);
                                        $mini_condition->SearchTextConditionRecursive($relation[1]::filter(array()), $relation_alias, $text, $processed_models);
                                }
                                break;
                        }
                        break;
                }
                $this->mergeWith($mini_condition, false);
            }
        }
    }
    
    /**
     * 
     * @param type $columns
     * @param type $text
     * @param type $alias
     * @param type $and
     * @return parsira tekst na vise reci i svaku rec filtrira u svaku od zadatih kolona, koristi se kod filtera za pretrazivanje reci
     */
    protected function FilterTextCondition($columns, $text, $alias, $options=array(), $and = true)
    {
        $_q = $this->spech_chars[$this->DBbackend]['col_quotes'];
        if ($text == '')
        {
            return;
        }
        $exploded = explode(' ', $text);
        foreach ($exploded as $part)
        {
            if ($part == '')
            {
                continue;
            }
            $part_i = SIMAHtml::uniqid();
            $wildCard = false; //ako je wildcard onda ne vrsimo escape specijalnih karaktera
            if (in_array('wildcard', $options))
            {
                $part = $this->wildCard($part);
                $wildCard = true;
            }
            
            if (!$wildCard)
            {
                $part = preg_quote($part);                
            }
            $part2 = $this->SerbianLettersSearch($part);
            $temp_condition = new CDbCriteria();
            foreach ($columns as $col)
            {
                $mini_condition = new CDbCriteria();
                if ($this->DBbackend === 'pgsql')
                {
                    $mini_condition->condition = $alias . '.'.$_q.$col.$_q.' ~* :param' . $part_i;
                }
                else if($this->DBbackend === 'mysql')
                {
                    error_log(__METHOD__.' - nije pokriveno - 9 - mysql');
                    $mini_condition->condition = $alias . '.'.$_q.$col.$_q.' like "%'.$part2.'%"';
                }
                $temp_condition->mergeWith($mini_condition, false);
            }
            $temp_condition->params = array(':param' . $part_i => pg_escape_string($part2));
            $this->mergeWith($temp_condition, $and);
        }
    }    
    
    private function FilterIntegerCondition($columns, $text, $alias, $and = true)
    {
        if ($text == '')
        {
            return;
        }
        $exploded = explode(' ', $text);
        foreach ($exploded as $part)
        {
            if ($part == '')
            {
                continue;
            }
            $part_i = SIMAHtml::uniqid();
            $temp_condition = new CDbCriteria();
            foreach ($columns as $col)
            {
                $mini_condition = new CDbCriteria();
                $mini_condition->condition = 'CAST(' . $alias . '."' . $col . '" AS TEXT) ~* :param' . $part_i;
                $temp_condition->mergeWith($mini_condition, false);
            }
            $temp_condition->params = array(':param' . $part_i => preg_quote($part));
            $this->mergeWith($temp_condition, $and);
        }
    }
    
    protected function FilterNumericCondition($column, $text, $alias, $params, $and = true)
    {        
        if ($text === '')
        {
            return;
        }
        if (isset($params['origin_column']))
        {
            $column = $params['origin_column'];
        }
//        error_log(__METHOD__.' - '.$text);
        $uniq = SIMAHtml::uniqid();
        $_q = $this->spech_chars[$this->DBbackend]['col_quotes'];
        $mini_condition = new CDbCriteria();
        $exploded = explode('||', $text);
        $exploded1 = explode('&&', $text);
        if (count($exploded) > 1)
        {
            $ex1 = explode('SEPARATOR', $exploded[0]);
            $ex2 = explode('SEPARATOR', $exploded[1]);
            if (count($ex1) > 1 && count($ex2) > 1 && $ex1[1] !== '' && $ex2[1] !== '')
            {
                $operator1 = $ex1[0];
                $operator2 = $ex2[0];
                $number1 = SIMAMisc::userToDbNumeric($ex1[1]);
                $number2 = SIMAMisc::userToDbNumeric($ex2[1]);
                $mini_condition->condition = "$alias.$_q$column$_q $operator1 :param1$uniq or $alias.$_q$column$_q $operator2 :param2$uniq";
                $mini_condition->params = array(":param1$uniq" => $number1, ":param2$uniq" => $number2);
            }            
//            else
//            {
//                
//            }
        }
        else if (count($exploded1) > 1)
        {            
            $ex1 = explode('SEPARATOR', $exploded1[0]);
            $ex2 = explode('SEPARATOR', $exploded1[1]);
            if (count($ex1) > 1 && count($ex2) > 1 && $ex1[1] !== '' && $ex2[1] !== '')
            {
                $operator1 = $ex1[0];
                $operator2 = $ex2[0];
                $number1 = SIMAMisc::userToDbNumeric($ex1[1]);
                $number2 = SIMAMisc::userToDbNumeric($ex2[1]);
                $mini_condition->condition = "$alias.$_q$column$_q $operator1 :param1$uniq and $alias.$_q$column$_q $operator2 :param2$uniq";
                $mini_condition->params = array(":param1$uniq" => $number1, ":param2$uniq" => $number2);
            }
//            else
//            {
//                
//            }
        }
        else
        {
            $ex = explode('SEPARATOR', $text);
            if (count($ex) > 1 && $ex[1] !== '')
            {
                $operator = $ex[0];
                $number = SIMAMisc::userToDbNumeric($ex[1]);
                $mini_condition->condition = "$alias.$column $operator :param1$uniq";
                $mini_condition->params = array(":param1$uniq" => $number);
            }
//            $operator = '=';
//            $number = $text;
//            $ex = explode('SEPARATOR', $text);
//            if (count($ex) > 1 && $ex[1] !== '')
//            {
//                $operator = $ex[0];
//                $number = SIMAMisc::userToDbNumeric($ex[1]);
//            }
//            
//            $mini_condition->condition = "$alias.$column $operator :param1$uniq";
//            $mini_condition->params = array(":param1$uniq" => $number);
        }
        
        $this->mergeWith($mini_condition, $and);
    }
    
    /**
     * 
     * @param type $column
     * @param type $sql_table
     * @param type $params
     * @param type $and
     * @return dodaje condition, koristi se u racunima
     */
    private function putInCondition($column, $ids, $params, $alias, $and = true)
    {
        $_q = $this->spech_chars[$this->DBbackend]['col_quotes'];
        if (!is_array($ids))
        {
            $ids = array($ids);
        }
        $_conds = [];

        foreach ($ids as $key => $value)
        {
            if ($value==='null')
            {                
                unset($ids[$key]);
                $_conds[] = "$alias.$_q$column$_q is null";
            }
            else if ($value==='' || $value===null)
            {                
                unset($ids[$key]);
            }
        }
        
        if (count($ids)>0)
        {
            $_ids = implode(',', $ids);
            if (!empty($_ids))
            {
                $_conds[] = "$alias.$_q$column$_q in ($_ids)";
            }
        }

        $condition = implode(' OR ', $_conds);
        
        $temp_condition = new CDbCriteria();
        $temp_condition->condition = $condition;
        $temp_condition->params = $params;        
        $this->mergeWith($temp_condition, $and);
    }   

    /**
     * 
     * @param type $column
     * @param type $value
     * @param type $and
     * @return filtrira rec u koloni, koristi se kod filtera
     */
    protected function FilterSimpleCondition($column, $value, $alias, $and = true)
    {
        $_q = $this->spech_chars[$this->DBbackend]['col_quotes'];
        if ($value == '')
        {
            return;
        }

        $param_id = SIMAHtml::uniqid();
        $temp_condition = new CDbCriteria();
        
        if (gettype($value) == 'string' && ($value == 'null' || $value == 'NULL'))
        {
            $temp_condition->condition = $alias . '.'.$_q.$column.$_q.' is null';
        }
        else if (gettype($value) == 'array')
        {
            foreach ($value as $val)
            {
                $temp_criteria = new CDbCriteria();
                if (gettype($val) == 'string' && ($val == 'null' || $val == 'NULL'))
                {
                    $temp_criteria->condition = $alias . '.'.$_q.$column.$_q.' is null';
                }
                else
                {
                    $temp_criteria->condition = $alias . '.'.$_q.$column.$_q.' = :param' . $param_id;
                    $temp_criteria->params = array(':param' . $param_id => $val);
                }
                $temp_condition->mergeWith($temp_criteria, $and);
            }
        }
        else
        {
            $temp_condition->condition = $alias . '.'.$_q.$column.$_q.' = :param' . $param_id;
            $temp_condition->params = array(':param' . $param_id => $value);
        }
        
        $this->mergeWith($temp_condition, $and);
    }
    
    protected function FilterBooleanCondition($column, $value, $alias, $and = true)
    {
        if ($value === '' || $value === null)
        {
            return;
        }
        $param_id = SIMAHtml::uniqid();
        $temp_condition = new CDbCriteria();        
        
        if (gettype($value) == 'string' && ($value == 'null' || $value == 'NULL'))
        {
            $temp_condition->condition = $alias . '."' . $column . '" is null';
        }
        else
        {
            $value = filter_var($value, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
            if (gettype($value) == 'boolean')
            {
                if ($value)
                {
                    $_value = 'true';
                }
                else
                {
                    $_value = 'false';
                }
                $temp_condition->condition = $alias . '."' . $column . '" = :param' . $param_id;
                $temp_condition->params = array(':param' . $param_id => $_value);
            } 
        }
        
        $this->mergeWith($temp_condition, $and);
    }

    /**
     * 
     * @param type $column
     * @param type $value
     * @param type $and
     * @return filtrira datum u koloni, koristi se kod filtera
     */
    private function FilterDateRangeCondition($column, $value, $alias, $and = true)
    {
        $temp_condition = new CDbCriteria();
        $param_id = SIMAHtml::uniqid();
                
        if( is_array($value) )
        {       
            $value_count = count($value);
            $operator = '=';
            $final_value = $value[0];
            $trunc_precision = null;
            
            if( $value_count == 3 )
            {
                $operator = $value[0];
                $final_value = $value[1];
                $trunc_precision = $value[2];
            }
            elseif( $value_count == 2 )
            {
                $operator = $value[0];
                $final_value = $value[1];
            }
            else if( $value_count != 1 )
            {
                throw new Exception('invalid array count');
            }
            $pure_param = ':param' . $param_id;
            if (is_null($trunc_precision))
            {
                $_param = $pure_param;
                $_column = $alias.'."' . $column . '"';
            }
            else
            {
                $_param = "date_trunc('$trunc_precision', $pure_param::TIMESTAMP)";
                $_column = "date_trunc('$trunc_precision',".$alias . '."' . $column . '"::TIMESTAMP)';
            }
                
            $temp_condition->condition = "$_column $operator $_param";
            $temp_condition->params = array($pure_param => SIMAHtml::UserToDbDate($final_value));
            
        }   
        elseif (gettype($value) == 'string' && ($value == 'null' || $value == 'NULL'))
        {
            $temp_condition->condition = $alias . '."' . $column . '" is null';
        }
        else 
        {
            $exploded1 = explode('<>', $value);
            $exploded2 = explode('<!>', $value);            
            if (count($exploded1) == 1 && count($exploded2) == 1)
            {   
                $temp_condition->condition = $alias . '."' . $column . '" = :param' . $param_id;
                $temp_condition->params = array(':param' . $param_id => SIMAHtml::UserToDbDate($value));
            }
            else 
            {
                if (count($exploded1) == 2)
                {
                    $exploded = $exploded1;
                    $sign1 = '>=';
                    $sign2 = '<=';
                }
                elseif (count($exploded2) == 2)
                {
                    $exploded = $exploded2;
                    $sign1 = '>';
                    $sign2 = '<';
                }
                else
                {
                    throw new Exception('pretraga po datumu ima neparvilan broj znakova' . $value);
                }


                $temp_condition->condition = "$alias.\"$column\" $sign1 :param1$param_id and $alias.\"$column\" $sign2 :param2$param_id";
                $temp_condition->params = array(
                    ":param1$param_id" => SIMAHtml::UserToDbDate(trim($exploded[0])), 
                    ":param2$param_id" => SIMAHtml::UserToDbDate(trim($exploded[1]))
                );

            }
        }
        $this->mergeWith($temp_condition, $and);
    }
    
    /**
     * 
     * @param type $column
     * @param type $value
     * @param type $alias
     * @param type $and
     * @return filtrira datum i vreme u koloni
     */
    private function FilterDateTimeRangeCondition($column, $value, $alias, $and = true)
    {        
        $param_id = SIMAHtml::uniqid();
        $temp_condition = new CDbCriteria();
        
//        $asd = '^(\d{1,2}\.\d{1,2}\.\d{4}\.(<!? >\d{1,2}\.\d{1,2}\.\d{4}\.)?)$';
         
        if(is_array($value) )
        {       
            $value_count = count($value);
            $operator = '=';
            $final_value = $value[0];
            $trunc_precision = null;
            
            if( $value_count == 3 )
            {
                $operator = $value[0];
                $final_value = $value[1];
                $trunc_precision = $value[2];
            }
            elseif( $value_count == 2 )
            {
                $operator = $value[0];
                $final_value = $value[1];
            }
            else if( $value_count != 1 )
            {
                throw new Exception('invalid array count');
            }
            $pure_param = ':param' . $param_id;
            if (is_null($trunc_precision))
            {
                $_param = $pure_param;
                $_column = $alias.'."' . $column . '"';
            }
            else
            {
                $_param = "date_trunc('$trunc_precision', $pure_param::TIMESTAMP)";
                $_column = "date_trunc('$trunc_precision',".$alias . '."' . $column . '"::TIMESTAMP)';
            }
                
            $temp_condition->condition = "$_column $operator $_param";
            $temp_condition->params = array($pure_param => SIMAHtml::UserToDbDate($final_value, true, true));
            
        }
        elseif (gettype($value) == 'string' && ($value == 'null' || $value == 'NULL'))
        {            
            $temp_condition->condition = $alias . '."' . $column . '" is null';
        }
        else
        {            
            $exploded1 = explode('<>', $value);
            $exploded2 = explode('<!>', $value);
            if (count($exploded1) == 1 && count($exploded2) == 1)
            {   
                $nextday = SIMAHtml::UserToDbDate('+1 day'.$value, true, true);

                $temp_condition->condition = "$alias.\"$column\" >= :param1$param_id and $alias.\"$column\" < :param2$param_id";
                $temp_condition->params = array(":param1$param_id" => SIMAHtml::UserToDbDate($value, true, true), ":param2$param_id" => $nextday);

            }
            else 
            {
                if (count($exploded1) == 2)
                {
                    $exploded = $exploded1;
                    $sign1 = '>=';
                    $sign2 = '<=';
                }
                elseif (count($exploded2) == 2)
                {
                    $exploded = $exploded2;
                    $sign1 = '>';
                    $sign2 = '<';
                }
                else
                {
                    throw new Exception('pretraga po datumu ima neparvilan broj znakova' . $value);
                }


                $temp_condition->condition = "$alias.\"$column\" $sign1 :param1$param_id and $alias.\"$column\" $sign2 :param2$param_id";
                $temp_condition->params = array(
                    ":param1$param_id" => SIMAHtml::UserToDbDate(trim($exploded[0]), true, true), 
                    ":param2$param_id" => SIMAHtml::UserToDbDate(trim($exploded[1]), true, true)
                );

            }
        }     
        $this->mergeWith($temp_condition, $and);
    }
    
    /**
     * Privatna funkcija za filtriranje json kolona. $value se zadaje isto kao i model_filter-i.
     * Filtriranje podrzava i koriscenje OR/AND.
     * Ako zadamo null za neko polje, onda smatramo da json putanja do tog polja ne postoji, odnosno da to polje ne postoji u json-u, i rezultat su sve kolone
     * koje nemaju to polje u svom json-u
     * Primer zadavanja filtera za kolonu settings(u bazi je text polje i u njemu se smesta json kao tekst) za model Theme:
     * $themes = Theme::model()->findAllByModelFilter([
            'settings' => [
                'widget' => [
                    'OR',
                    ['debug' => 'on'],
                    ['debug' => null],
                    [
                        'window' => [
                            'OR',
                            'name' => 'main_window',
                            'width' => 500
                        ]
                    ],
                    [
                        'image' => [
                            'name' => 'sun1',
                            'hOffset' => 250
                        ]
                    ]
                ]
            ]
        ]);
     * @param string $column
     * @param array/null/string null $value
     * @param string $alias
     * @param string $sql_path
     * @param boolean $and
     */
    protected function filterJsonCondition($column, $value, $alias, $sql_path='', $and = true)
    {
        $temp_criteria = new SIMADbCriteria();

        if (is_null($value) || (is_string($value) && strtolower($value) === 'null'))
        {
            $temp_criteria->condition = "$alias.$column is null";
        }
        else if (is_array($value))
        {
            if (empty($sql_path))
            {
                $sql_path = "($alias.$column)::JSON";
            }

            $has_conn_type = false;
            $conn_type = 'and';
            if (isset($value[0]))
            {
                $value_lower = null;
                if(is_string($value[0]))
                {
                    $value_lower = strtolower($value[0]);
                }
                if ($value_lower === 'and' || $value_lower === 'or')
                {
                    $has_conn_type = true;
                    $conn_type = $value_lower;
                    unset($value[0]);
                }
                else
                {
                    throw new SIMAExceptionDbCriteriaJsonConditonInvalid('no key or and/or');
                }
            }

            $conn_type_bool = ($conn_type==='and') ? true : false;

            foreach ($value as $value_key => $value_value)
            {
                if (is_array($value_value))
                {
                    $new_sql_path = $sql_path;
                    if (!$has_conn_type)
                    {
                        $new_sql_path = "(($sql_path)->>'{$value_key}')::JSON";
                    }
                    $temp_criteria->filterJsonCondition($column, $value_value, $alias, $new_sql_path, $conn_type_bool);
                }
                else
                {
                    if (!empty($temp_criteria->condition))
                    {
                        $temp_criteria->condition .= " $conn_type ";
                    }

                    if (is_null($value_value))
                    {
                        $temp_criteria->condition .= "((($sql_path)->>'{$value_key}') is null)";
                    }
                    else
                    {
                        if (is_bool($value_value))
                        {
                            $value_value = $value_value ? 'true' : 'false';
                        }

                        $temp_criteria->condition .= "(($sql_path)->>'{$value_key}' = '$value_value')";
                    }
                }
            }
        }


        $this->mergeWith($temp_criteria, $and);
    }
    
    /**
     * filtrira sve postavljene filtere u modelu tako sto rekurzivno poziva samu sebe za svaki filter koji je relacija u modelu.
     * @param type $model
     * @param type $alias
     * @return integer vraca koliko je filtera primenjeno
     */
    private function FilterConditionRecursive($model, $alias, $filter_settings = 'filters')
    {
        /**
         * brojac koliko je filtera primenjeno
         */
        $count_applied = 0;
        
        if (gettype($model) !== 'object' || $model == null)
        {
            return $count_applied;
        }
        if ($model->ids != null)
        {            
            $this->putInCondition('id', $model->ids, array(), $alias);
            $count_applied++;
        }
        
        $filters_array = $model->modelSettings($filter_settings);
        foreach ($filters_array as $column => $filter_type)
        {
//            if (property_exists ( $model, $column ))
            if (isset($model->$column))
            {
                $mini_condition = new SIMADbCriteria;
                $mini_condition->DBbackend = $this->DBbackend;
                $_filter_type = $filter_type;
                if (is_array($filter_type))
                {
                    if (isset($filter_type[0]))
                    {
                        $_filter_type = $filter_type[0];
                    }
                    if (isset($filter_type['func']))
                    {
                        $funcName = $filter_type['func'];
                        $model->$funcName($mini_condition, $alias, $column, $_filter_type);
                        $_filter_type = '';
                    }
                }              
                switch ($_filter_type)
                {
                    case 'text':
                        $options = [];
                        $criteria_latin = new SIMADbCriteria();
                        $criteria_latin->DBbackend = $this->DBbackend;
                        $criteria_cyrillic = new SIMADbCriteria();
                        $criteria_cyrillic->DBbackend = $this->DBbackend;
                        
                        $search_text = $model->$column;
                        
                        if (is_array($filter_type))
                        {
                            if (isset($filter_type['options']))
                            {
                                $options = $filter_type['options'];
                            }
                            if (isset($filter_type['use_cyrillic']) && $filter_type['use_cyrillic'] === true)
                            {
                                $search_text_cyrillic = SIMAMisc::convertLatinToCyrillic($search_text);
                                $search_text_latin = SIMAMisc::convertCyrillicToLatin($search_text);
                            }
                        }
                        if (!empty($search_text_cyrillic) && !empty($search_text_latin))
                        {
                            $criteria_cyrillic->FilterTextCondition(array($column), $search_text_cyrillic, $alias, $options);
                            $criteria_latin->FilterTextCondition(array($column), $search_text_latin, $alias, $options);
                            $criteria_latin->mergeWith($criteria_cyrillic, false);
                            $mini_condition->mergeWith($criteria_latin);
                        }
                        else
                        {
                            $mini_condition->FilterTextCondition(array($column), $search_text, $alias, $options);
                        }
                        break;                    
                    case 'integer':
                        $mini_condition->FilterIntegerCondition(array($column), $model->$column, $alias); break;
                    case 'partner': 
                    case 'company':
                        throw new Exception(__METHOD__.' - ne sme se koristiti');
                    case 'dropdown': 
                        $mini_condition->FilterSimpleCondition($column, $model->$column, $alias); break;
                    case 'boolean':
                        $mini_condition->FilterBooleanCondition($column, $model->$column, $alias); break;
                    case 'date_range': 
                        $mini_condition->FilterDateRangeCondition($column, $model->$column, $alias); break;
                    case 'date_time_range':
                        $mini_condition->FilterDateTimeRangeCondition($column, $model->$column, $alias); break;
                    case 'phone_number':
                        error_log(__METHOD__.' - nije nadjena upotreba');
                        $mini_condition->FilterPhoneNumber($column, $model->$column, $alias); break;
                    case 'numeric':
                        $params = is_array($filter_type) ? $filter_type : [];
                        $mini_condition->FilterNumericCondition($column, $model->$column, $alias, $params); break;
                    case 'json':
                        $mini_condition->filterJsonCondition($column, $model->$column, $alias); break;
                    case 'relation':
                        
                            $relations = $model->relations();
                            $relation = $relations[$column];
                            $local_alias = 't'.SIMAHtml::uniqid();
                        
                            $local_join = '';
//                            $local_condition = '';
                            $table_name = $relation[1]::model()->tableName();
                            
                            /**
                             * KRAJ obrade
                             */
                            
                            switch ($relation[0])
                            {
                                case 'CHasManyRelation':
                                    $cnt_applies = $mini_condition->FilterConditionRecursive($model->$column, $local_alias, $filter_settings);
                                    if ($cnt_applies > 0)
                                    {
                                        $local_join = "left join $table_name $local_alias on $alias.id=$local_alias." . $relation[2];
                                    }
                                    else
                                    {
                                        $mini_condition->condition = "exists (select 1 from $table_name $local_alias where $alias.id = $local_alias." . $relation[2] . ')';
                                    }
                                    break;
                                case 'CHasOneRelation':
                                    $cnt_applies = $mini_condition->FilterConditionRecursive($model->$column, $local_alias, $filter_settings);
                                    if ($cnt_applies > 0)
                                    {
                                        $local_join = "left join $table_name $local_alias on $alias.id=$local_alias." . $relation[2];
                                    }
                                    else
                                    {
                                        $mini_condition->condition = "exists (select 1 from $table_name $local_alias where $alias.id = $local_alias." . $relation[2] . ')';
                                    }
                                    break;
                                case 'CBelongsToRelation':
                                    $rel_id = $relation[2];
                                    /**
                                     * Milos Sreckovic:
                                     * ukoliko postoje dva filtera:
                                     *  'document_type_id' => 'dropdown',
                                     *  'document_type' => 'relation'
                                     * i postavljen je filter 'document_type_id', onda relacisjski filter ne sme da se koristi
                                     */
                                    if ($model->$rel_id=='' || $model->$rel_id == null)
                                    {
                                        $cnt_applies = $mini_condition->FilterConditionRecursive($model->$column, $local_alias, $filter_settings);
                                        if ($cnt_applies > 0)
                                        {
                                            $local_join = 'left join ' . $relation[1]::model()->tableName() . ' ' . $local_alias . ' on ' . $local_alias . '.id=' . $alias . '.' . $relation[2];
                                        }
                                        else
                                        {
                                            $mini_condition->condition = "exists (select 1 from $table_name $local_alias where $local_alias.id = $alias." . $relation[2] . ')';
                                        }
                                    }
                                    break;
//                                default:
//                                    error_log(__METHOD__.' - nje podrzan tip relacije: '.$relation[0]);
//                                    error_log(__METHOD__.' - $table_name: '.$table_name);
//                                    error_log(__METHOD__.' - $column: '.$column);
//                                    break;
                            }
                                                   
                            if ($local_join!='')
                            {
                                $mini_condition->join = $local_join . ' ' .$mini_condition->join;
                            }
                            
                            //uslovi iz rekurzivnih poziva
                            if (gettype($model->$column)=='object' && $model->$column!=null && $local_join!='')
                            {
                                //postavljamo condition za sve statuse u relaciji
                                $statuses = $model->$column->modelSettings('statuses');
                                foreach ($statuses as $status_key => $status_value)
                                {
                                    if (isset($status_value['filter_func']))
                                    {
                                        $funcName = $status_value['filter_func'];
                                        $model->$column->$funcName($mini_condition);
                                    }
                                    else
                                    {
                                        $mini_condition->FilterBooleanCondition($status_key, $model->$column->$status_key, $local_alias);
                                    }
                                }
                            }
                            
                        
                        break;
                }
                if (isset($mini_condition->condition) && $mini_condition->condition!='' )
                {
                    $count_applied++;
                }
                
                $this->mergeWith($mini_condition); 
                
            }
        }
        return $count_applied;
    }    
    
    /**
     * primenjuje condition na sve statuse koji su setovani u modelu
     * @param Model $model
     * @param string $alias
     * @return type
     */
    private function ColumnsStatusCondition($model, $alias)
    {
        if (gettype($model) !== 'object' || $model == null)
        {
            return;
        }

        $statuses = $model->modelSettings('statuses');
        foreach ($statuses as $status_key => $status_value)
        {
            $mini_condition = new SIMADbCriteria;
            $mini_condition->DBbackend = $this->DBbackend;
            if (isset($status_value['filter_func']))
            {
                $funcName = $status_value['filter_func'];
                $model->$funcName($mini_condition);
            }
            else
            {
                $mini_condition->FilterBooleanCondition($status_key, $model->$status_key, $alias);
            }
            $this->mergeWith($mini_condition);
        }
    }


    /**
     * 
     * @param type $word
     * @return pretraga srpskih slova
     */
    protected function SerbianLettersSearch($word)
    {
        if ($this->DBbackend === 'mysql')
        {
            error_log(__METHOD__.' - nije pokriveno - mysql');
            return $word;
        }

        $r_word = '';
        $word_len = strlen($word);
        for ($i = 0; $i < $word_len; $i++)
        {
            switch ($word[$i])
            {
                case 'c': 
                case 'C':
                case 'ć': 
                case 'Ć':
                case 'Č': 
                case 'Č':
                    $r_word.="(c|ć|č)";
                    break;
                case 's':
                case 'S':
                case 'š':
                case 'Š':
                    $r_word.="(s|š)";
                    break;
                case 'd':
                case 'D':
                case 'đ':
                case 'Đ':
                    if($i+1<$word_len && strtolower($word[$i+1]) === 'j')
                    {
                        $r_word.="(d|đ|dj|đj|djj)";
                        $i++;
                    }
                    else
                    {
                        $r_word.="(d|đ|dj)";
                    }
                    break;
                case 'z':
                case 'Z':
                case 'ž':
                case 'Ž':
                    $r_word.="(z|ž)";
                    break;
                default : $r_word.=$word[$i];
                    break;
            }
        }
        return preg_replace('/\?/', '\?', $r_word);
    }
    
    /**
     * funkcija za pretragu telefona
     * @param string $column
     * @param string $text
     * @param string $alias
     */
    private function FilterPhoneNumber($column, $text, $alias)
    {        
        $_q = $this->spech_chars[$this->DBbackend]['col_quotes'];
        $uniq = SIMAHtml::uniqid();
        $_column = $alias.'.'.$_q.$column.$_q;
        $exploded = explode(' ', $text);
        foreach ($exploded as $part)
        {
            $part = preg_quote($part);
            $temp_condition = new CDbCriteria();
            if (preg_match('/[A-Za-z]/', $part))
            {
                $part = $this->SerbianLettersSearch($part);
                $temp_condition->condition = "$_column ~* :match$uniq";
            }
            else
            {
                $temp_condition->condition = 
                    ":match$uniq ~ '[0-9]+' and :match$uniq !~ '[a-zA-Z]' and
                    $_column ~ '[0-9]+' and $_column !~ '[a-zA-Z]' and
                    (
                          regexp_replace(regexp_replace($_column, '[^0-9]', '', 'g') , '^0', '') ~ regexp_replace(regexp_replace(:match$uniq, '[^0-9]', '', 'g') , '^0', '')
                          OR
                          regexp_replace(regexp_replace(:match$uniq, '[^0-9]', '', 'g') , '^0', '') ~ regexp_replace(regexp_replace($_column, '[^0-9]', '', 'g'), '^0', '')
                    )";
            }
            $temp_condition->params = array(":match$uniq" => $part);
            $this->mergeWith($temp_condition);
        }
    }
    
    public static function DoesHaveAnyModelByFilter($model, $model_filter)
    {
        $criteria = new SIMADbCriteria([
            'Model' => $model,
            'model_filter' => $model_filter
        ], true);
        return !empty($criteria->ids);
    }
    
    /**
     * Konvertuje string u wild card
     * @param string $text
     * @return string
     */
    protected function wildCard($text)
    {
        $special_characters = ["\\","(",")","+","[","]"];
        $escaped_special_characters = ["\\\\","\\(","\\)","\\+","\\[","\\]"];
        
        $new_text = str_replace($special_characters, $escaped_special_characters, $text);

        return '^'.str_replace('*', '.*', $new_text).'$';
    }
    
    /**
     * privremena f-ja koja treba da loguje sve upotrebe atributa 
     * koji trebaju biti izbaceni jer ih podrzava cdbcriteria
     * order (order_by), group (group_by), limit, offset, with, scopes
     */
//    public static function logWronglyUsedAttribute($method, $attribute, $object)
//    {
//        if(Yii::app()->params['log_wrongly_used_attributes'] === true)
//        {
//            error_log($method.' - koristi se '.$attribute);
//    //        error_log($method.' - getallheaders(): '.CJSON::encode(getallheaders()));
//            
//            if(is_object($object))
//            {
//                error_log($method.' - get class $object: '.get_class($object));
//            }
//            error_log($method.' - $object: '.CJSON::encode($object));
//        }
//    }
}