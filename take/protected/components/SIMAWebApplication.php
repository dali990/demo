<?php

require_once __DIR__.'/../traits/SIMAApplicationTraits.php';
require_once __DIR__.'/../traits/ModelUpdateEventsTraits.php';
require_once __DIR__.'/../traits/SIMAAfterWorkTraits.php';

class SIMAWebApplication extends CWebApplication
{    
    use SIMAApplicationTraits;
    use ModelUpdateEventsTraits;
    use SIMAAfterWorkTraits;
    
    /**
     * @var Company model kompanije 
     */
    private $_company = NULL;
    
    /**
     *
     * @var string 
     */
    public $sima_size = NULL;
    
    public $validate_activities = true;
    
    /**
     * Boolean promenljiva koja odredjuje da li se zove update model views.
     * Za SIMAWebApplication default je true
     * @var boolean
     */
    private $update_model_views = true;

    public function init()
    {
        $this->testIsInMaintenance();
        
        parent::init();
        
        $this->validatePrerequisiteRequirements();
        
        Yii::app()->simaDemo->init();
        
        $this->initDbConnection();
        
        SIMAModule::registerModules(Yii::app());
        
        Yii::app()->configManager->loadConfigurationDefinitions();
        if(!Yii::app()->user->isGuest)
        {
            Yii::app()->warnManager->reMapWarnsByModelDisplayAction();
        }
        
        if(
            Yii::app()->params['is_production'] === false
            && Yii::app()->configManager->get('admin.use_memcache') === true
            && !(isset(Yii::app()->params['local_memcache']) && Yii::app()->params['local_memcache'] === true)
        )
        {
            Yii::app()->configManager->set('admin.use_memcache', false, false);
        }
    }
    
    public function getAuthManager()
    {
            return $this->getComponent('authManager');
    }
    
    public function __get($name)
    {
        switch ($name)
        {
            default: return parent::__get($name);
        }
    }
    
    public function __set($name, $value)
    {   
        switch ($name)
        {
            default: parent::__set($name, $value); break;
        }
        
    }
    
    /**
     * 
     * @return Company objekat koji predstavlja red u tabeli companies
     */
    public function getCompany()
    {
        if ($this->_company===NULL)    
        {
            $system_company_id = Yii::app()->configManager->get('base.system_company_id');
            $this->_company = Company::model()->findByPk($system_company_id);
        }
        return $this->_company;
    }

    /**
     * Yii::app()->isWebApplication()
     * @return boolean
     */
    public static function isWebApplication()
    {
        return true;
    }

    /**
     * Yii::app()->isConsoleApplication()
     * @return boolean
     */
    public static function isConsoleApplication()
    {
        return false;
    }
    
    public function hasModule($moduleString, $parent_module=null)
    {
        return SIMAMisc::hasModule($moduleString, $parent_module);
    }
    
    public function raiseNote($note, $note_code = null)
    {
        $this->controller->raiseNote($note, $note_code);
    }

    protected function initSystemHandlers()
    {
            if(YII_ENABLE_EXCEPTION_HANDLER)
                    set_exception_handler(array($this,'simaHandleException'));
            if(YII_ENABLE_ERROR_HANDLER)
            {                
//                set_error_handler(array($this,'handleError'),error_reporting());
                set_error_handler(array($this,'handleError'),error_reporting() & ~E_NOTICE);
            }
    }
    
    public function testIsInMaintenance()
    {
//        if(Yii::app()->params['isUpdating'] === true)
        if(Yii::app()->maintenanceManager->isUpdating())
        {
            header('HTTP/1.1 503 Service Temporarily Unavailable');
            header('Status: 503 Service Temporarily Unavailable');
            echo Yii::t('Site', 'Updating', [
//                '{updatingStartTimestamp}' => SIMAHtml::DbToUserDate(date('Y-m-d H:i:s', Yii::app()->params['updatingStartTimestamp']), true)
                '{updatingStartTimestamp}' => SIMAHtml::DbToUserDate(date('Y-m-d H:i:s', Yii::app()->maintenanceManager->getUpdatingStartTimestamp()), true)
            ]);
            exit();
        }
    }
    
    public function simaHandleException($exception)
{
//       if ($exception instanceof CHttpExceptionActionNotFound && !empty($this->getController())) //Sasa A. - za sada se ne koristi
//       {
//           $this->getController()->parseException($exception);
//       }
//       else
//       {
           parent::handleException($exception);
//       }
    }

    public function getClientIpAddress()
    {
        $ipaddress = 'UNKNOWN';
        
        if (getenv('HTTP_CLIENT_IP'))
        {
            $ipaddress = getenv('HTTP_CLIENT_IP');
        }
        else if(getenv('HTTP_X_FORWARDED_FOR'))
        {
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        }
        else if(getenv('HTTP_X_FORWARDED'))
        {
            $ipaddress = getenv('HTTP_X_FORWARDED');
        }
        else if(getenv('HTTP_FORWARDED_FOR'))
        {
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        }
        else if(getenv('HTTP_FORWARDED'))
        {
           $ipaddress = getenv('HTTP_FORWARDED');
        }
        else if(getenv('REMOTE_ADDR'))
        {
            $ipaddress = getenv('REMOTE_ADDR');
        }
        
        return $ipaddress;
    }
    
    /**
     * 
     * @return boolean
     */
    public function isDemo()
    {
        $result = false;
        
        if(
            isset(Yii::app()->params['is_demo'])
            && Yii::app()->params['is_demo'] === true
            && isset($_SESSION["db_session_key"])
        )
        {
            $result = true;
        }
        
        return $result;
    }
    
    public function getSessionKey()
    {
        $session_key = 'sima';
        if(isset($_SESSION["db_session_key"]))
        {
            $session_key = $_SESSION["db_session_key"];
        }
        return $session_key;
    }
    
    public function getUpdateModelViews()
    {
        return $this->update_model_views;
    }
    
    public function setUpdateModelViews($value)
    {
        if(!is_bool($value))
        {
            $error_message = __METHOD__.' - nije boolean, a mora biti - $value: '. SIMAMisc::toTypeAndJsonString($value);
            error_log($error_message);
//            throw new Exception($error_message);
        }
        $this->update_model_views = $value;
    }
    
    private function validatePrerequisiteRequirements()
    {
        if(Yii::app()->params['is_production'] === true || Yii::app()->params['is_test_env'] === true || filter_input(INPUT_GET, 'r') !== 'user/login')
        {
            return;
        }
        $command = "php install/run.php -vp";
        $output = null;
        $return = null;
        Yii::app()->linuxCommandExecutor->executeCommand($command, null, $output, $return);
        if($return !== 0)
        {
            error_log(__METHOD__.' - $return: '.SIMAMisc::toTypeAndJsonString($return));
            error_log(__METHOD__.' - $output: '.SIMAMisc::toTypeAndJsonString($output));
            throw new Exception('prerequisites not satisfied');
        }
    }
    
    public function sendUpdateEvent($percent)
    {
        Yii::app()->controller->sendUpdateEvent($percent);
    }
}

