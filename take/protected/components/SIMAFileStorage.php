<?php

class SIMAFileStorage
{


    /**
     * 	uzima cist query i vraca kriterijum za sve fajlove koji su obuhvaceni njime 
     * 	takodje se primenjuju i prava pristupa fajlovima jer je ovo jedina tacka ulaza
     * 	TODO(srecko): proveriti da li je ovo jedina tacka pristupa
     * */
    public function applyQuery($query, $alias='t')
    {
        $criteria = new CDbCriteria();
        $criteria->alias = $alias;/** files  tabela */

        $d = SIMASQLFunctions::applyQuery($query, $alias);
        $criteria->condition = $d[0]['applyquery'];
        
        return $criteria;
    }

    /** dodaje Tag fajlu */
    public function addTagToFile($tag_id, $file_id)
    {
        $tag_conn = FiletoFileTag::model()->findByAttributes(array('file_tag_id' => $tag_id, 'file_id' => $file_id));
        if (empty($tag_conn))
        {
            /** dodavanje taga */
            $tag_conn = new FiletoFileTag();
            $tag_conn->file_id = $file_id;
            $tag_conn->file_tag_id = $tag_id;
        }
        else
        {
            $tag_conn->weak = false;
        }
        $tag_conn->save();
    }

}