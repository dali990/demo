<?php

/**
 * @package SIMA
 * @subpackage Base
 */
class SIMAActiveRecordBehavior extends CActiveRecordBehavior
{

    public function relations($event)
    {
    }
    

    public function tabs($event)
    {
    }
    
    /**
     * Zadaje se u obliku:
     * $event->addResult([
     *    [
     *       'view'=>'naziv view-a', - view kojem se prosledjuje html
     *       'html'=>'neki html', - html koji se prosledjuje
     *       'order'=>'order' - nije obavezno polje. Oznacava redosled prilikom ispisivanja html
     *    ]
     * ]);
     * @param SIMAEvent $event - kao dodatni parametar se setuje view($event->params['view']=$view) i u behavioru mora da se proveri za koji view se generise html
     * Ovo je obavezno jer je moguce da dodje do rekurzije ako unutar html-a opet imamo renderModelView za isti model
     * Primer funkcije view koja se nalazi u FileLegalBehavior-u:
     *  public function views($event)
        {
            $file = $this->owner;
            $view = $event->params['view'];
            $view_html = '';
            $view_order = '';
            if ($view === 'basic_info') //OBAVEZNA JE PROVERA KOJI JE VIEW PROSLEDJEN DA NE BI DOSLO DO REKURZIJE JER HTML MOZE TAKODJE DA SADRZI renderModelView
            {
                $view_order = 'filing_number';
                $view_html = '<h3>Zavodna knjiga</h3>';
                if (!$file->isDeleted())
                {
                    $view_html .= ($file->filing !== null) ?
                        Yii::app()->controller->renderModelView($file->filing,'inFile') 
                        :
                        Yii::app()->controller->renderModelView($file,'filingNumber');
                }
            }

            $event->addResult([
                [
                    'view'=>$view,
                    'order'=>$view_order,
                    'html'=>$view_html
                ]
            ]);
        }
     * 
     */
    public function views($event)
    {
    }
    
    /**
     * 
     * @param type $event
     * Zadaje se u obliku:
     * $event->addResult([
     *    [
     *       'model'=>'modela', - model za koji se renderuje full_info
     *       'priority'=>'priority' - prioritet(veci prioritet znaci da ce se renderovati taj full info. Ako dva modela imaju isti prioritet onda se baca greska)
     *    ]
     * ]);
     */
    public function fullInfo($event)
    {
    }
    
    /**
     * Sluzi za dodavanje podesavanja za model. Obrada svih zakacenih podesavanje se vrsi custom zavisno od situacije. Funkcija za dovlacenje svih podesavanja:
     * $model->getSettings($setting_code). $setting_code je opcioni i ako se ne prosledi onda se dovlace sva podesavanja.
     * Svako podesavanje se zadaje u obliku key=>value, gde je key kod tog podesavanja. Value je niz i on mora da ima sledeca polja:
     *      type - tip tog podesavanja: boolean
     *      title - tekst koji ce se videti u gui delu tog podesavanja
     *      default_value - default vrednost tog podesavanja
     * Opciona polja:
     *      check_access_func - funkcija u kojoj se definise da li korisnik za taj model moze da vidi podesavanje 
     * @param type $event
     * Primer zadavanja:
     * $event->addResult([
            'tasks_supported' => [
                'type' => 'boolean',
                'title' => Yii::t('JobsModule.Task', 'TasksSupported'),
                'default_value' => true,
                'check_access_func' => function(Theme $theme, User $user) {
                    return $theme->tasks_count === 0 ? true : false;
                }
            ]
        ]);
     */
    public function settings($event)
    {
    }
    
    public function basicInfo($event)
    {
    }
    
    public function options($event){}
    
    public function events()
    {
        return array_merge(parent::events(),[
            'onGetRelations' => 'relations',
            'onListTabs' => 'tabs',
            'onGetViews' => 'views',
            'onGetFullInfo' => 'fullInfo',
            'onGetSettings' => 'settings',
            'onGetBasicInfo' => 'basicInfo',
            'onGetOptions' => 'options'
        ]);
    }
}