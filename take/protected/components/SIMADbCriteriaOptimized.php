<?php

/**
 * ukoliko se u ovom modelu pravi novi SIMADbCriteria, obavezno setovati DBbackend
 */
class SIMADbCriteriaOptimized extends SIMADbCriteriaUnOptimized
{
    public static $NULL = 'NULL_null_Null_nULL';
    
    public static $MODELSETTINGSFILTERS_TEXT = 'text';
    public static $MODELSETTINGSFILTERS_DROPDOWN = 'dropdown';
    public static $MODELSETTINGSFILTERS_RELATION = 'relation';
    public static $MODELSETTINGSFILTERS_DATERANGE = 'date_range';
    public static $MODELSETTINGSFILTERS_DATETIMERANGE = 'date_time_range';
    public static $MODELSETTINGSFILTERS_BOOLEAN = 'boolean';
    public static $MODELSETTINGSFILTERS_INTEGER = 'integer';
    public static $MODELSETTINGSFILTERS_NUMERIC = 'numeric';
    public static $MODELSETTINGSFILTERS_JSON = 'json';
    
    private $_model = null;
    private $_model_filter = null;
    
    public function __construct($params=array(), $save_ids=false)
    {
        parent::__construct($params, $save_ids);
        
        if($save_ids === true)
        {
            $this->logWronglyUsedAttribute(__METHOD__, 'save_ids', $this);
//            throw new Exception('treba izbaciti $save_ids');
            
            $model_name = $params['Model'];
            $alias = $this->alias;
            $temp_criteria = clone $this;
            $temp_criteria->select = "array_agg($alias.id::text) as ids";
            $model = $model_name::model()->find($temp_criteria);
            $this->ids = str_replace(array('{', '}'), '', $model->ids);
        }
    }
    
    protected function filterParser($Model, $filter)
    {
        $this->_model = $Model;
        $this->_model_filter = $filter;
        
        if(empty($this->alias))
        {
            $this->alias = 't';
        }
        
        if (!empty($filter[0]) && is_string($filter[0]) && in_array(strtolower($filter[0]), ['not', 'and', 'or']))
        {
            $filter_type = strtolower($filter[0]);
            array_shift($filter);
            if ($filter_type === 'not')
            {
                if (count($filter) !== 1)
                {
                    throw new SIMAException(Yii::t('SIMADbCriteria', 'NotModelFilterMustHaveOnlyOneCondition'));
                }

                $temp_criteria = new SIMADbCriteria();
                $temp_criteria->alias = $this->alias;
                $temp_criteria->filterParser($Model, $filter[0]);
                $temp_criteria->condition = "not ({$temp_criteria->condition})";
                $this->mergeWith($temp_criteria);
            }
            else
            {
                $merge_type = $filter_type !== 'or';
                foreach ($filter as $_value)
                {
                    $temp_criteria = new SIMADbCriteria();
                    $temp_criteria->alias = $this->alias;
                    $temp_criteria->filterParser($Model, $_value);
                    $this->mergeWith($temp_criteria, $merge_type);
                }
            }
        }
        else
        {
            /**
             * izbacivanje elemenata koje ne treba simadbcriteria da obradjuje
             * ovo se desava samo pod and i/ili or
             * ZA SADA JOS UVEK NE MOZE DA SE IZBACI
             * JER SE NE PROPUSTAJU PARAMETRI
             */
//            $cdbcriteria_condition = [];
//            if(is_array($filter) && isset($filter['scopes']))
//            {
//                $cdbcriteria_condition['scopes'] = $filter['scopes'];
//                unset($filter['scopes']);
//            }
//            if(!empty($cdbcriteria_condition))
//            {
//                $cdbcriteria = new CDbCriteria($cdbcriteria_condition);
//                $criteria->mergeWith($cdbcriteria);
//            }
            
            $model = new $Model(null);
            $model->setTableAlias($this->alias);
            $model->setScenario('filter');
            $model->attachBehaviors($model->behaviors());
            
            $this->parseModelFilter($model, $filter);
        }
    }
    
    public function applyModel($model)
    {
        $this->logWronglyUsedAttribute(__METHOD__, 'applyModel', $this);
        parent::applyModel($model);
    }
    
    private function parseModelFilter(SIMAActiveRecord $model, array $model_filter)
    {
        $this->DBbackend = $model->DbConnection->DriverName;
        foreach($model_filter as $model_filter_key => $model_filter_value)
        {
            switch($model_filter_key)
            {
                case 'ids':
                    $this->parseModelFilter_ids($model_filter_value);
                    break;
                case 'scopes':
                case 'display_scopes':
                case 'filter_scopes':
//                    $this->scopes = array_merge((array)$this->scopes,(array)$model_filter_value);
                    /// mora da se obradi scopes zbog subcondition-a u obradi relacija
                    $this->parseModelFilter_scopes($model, $model_filter_value);
                    break;
                case 'text':
                    $this->parseModelFilter_text($model, $model_filter_value);
                    break;
                case 'order_by':
                    $this->parseModelFilter_orderBy($model, $model_filter_value);
                    break;
                case 'limit':
                    $this->logWronglyUsedAttribute(__METHOD__, 'limit', $model_filter);
                    $this->parseModelFilter_limit($model_filter_value);
                    break;
                case 'offset':
                    $this->logWronglyUsedAttribute(__METHOD__, 'offset', $model_filter);
                    $this->parseModelFilter_offset($model_filter_value);
                    break;
                default:
                {
                    $model_settings_filters = $model->modelSettings('filters');
                    $model_settings_statuses = $model->modelSettings('statuses');
                    
                    if(isset($model_settings_filters[$model_filter_key]))
                    {
                        $this->parseModelFilter_modelSettingsFilters($model, $model_settings_filters[$model_filter_key], $model_filter_key, $model_filter_value);
                    }
                    else if(isset($model_settings_statuses[$model_filter_key]))
                    {
                        $this->parseModelFilter_modelSettingsStatuses($model, $model_filter_key, $model_settings_statuses[$model_filter_key], $model_filter_value);
                    }
                    else
                    {
                        throw new Exception('not found $model_filter_key: '.$model_filter_key
                                .' - model: '.SIMAMisc::toTypeAndJsonString($model)
                                .' - $model_filter: '.SIMAMisc::toTypeAndJsonString($model_filter));
                    }
                }
            }
        }
    }
    
    /**
     * 
     * @param SIMAActiveRecord $model
     * @param string or array $scopes
     */
    private function parseModelFilter_scopes(SIMAActiveRecord $model, $scopes)
    {
        if(!is_array($scopes))
        {
            $scopes = [$scopes];
        }
        
        $scs=$model->scopes();
        foreach((array)$scopes as $k=>$v)
        {
            if(is_integer($k))
            {
                if(is_string($v))
                {
                    if(isset($scs[$v]))
                    {
                        $_scope = $scs[$v];
                        $_scope['Model'] = get_class($model);
                        $_cond = new SIMADbCriteria($_scope);
                        $this->mergeWith($_cond,true);
                        continue;
                    }
                    $scope=$v;
                    $params=array();
                }
                elseif(is_array($v))
                {
                    $this->logWronglyUsedAttribute(__METHOD__, 'nije pokriveno testom - $model', $model);
                    $this->logWronglyUsedAttribute(__METHOD__, 'nije pokriveno testom - $scopes', $scopes);
//                    throw new Exception(__METHOD__.' - bug, vratice call_user_func_array() expects parameter 1 to be a valid callback, second array member is not a valid method');
                    $scope=key($v);
                    $params=current($v);
                }
            }
            elseif(is_string($k))
            {
                $scope=$k;
                $params=$v;
            }

            call_user_func_array(array($model,$scope),(array)$params);
        }
        $this->mergeWith($model->getDbCriteria());
    }
    
    private function parseModelFilter_modelSettingsFilters($model, $filter_type, $model_filter_key, $model_filter_value)
    {
        $filter_type_params = [];
        if (is_array($filter_type))
        {
            if (isset($filter_type['func']))
            {
                $funcName = $filter_type['func'];
//                $model->$model_filter_key = $model_filter_value;
                $model->setAttributesFilter([
                    $model_filter_key => $model_filter_value
                ]);
//                $this->logWronglyUsedAttribute(__METHOD__, 'setAttributesFilter - $model_filter_key: '.$model_filter_key, $model);
                $model->$funcName($this, $this->alias, $model_filter_key, $filter_type);
                return;
            }
            else if (isset($filter_type['filter_function']))
            {
                $funcName = $filter_type['filter_function'];
                $filter_type_params = $filter_type;
                $filter_type = $filter_type[0];
                $filter_function_criteria = $model->$funcName($model_filter_value, $this->alias, $model_filter_key, $filter_type);
                if (!empty($filter_function_criteria))
                {
                    $this->mergeWith($filter_function_criteria);
                }
            }
            else
            {
                $filter_type_params = $filter_type;
                $filter_type = $filter_type[0];
            }
        }
        $this->parseModelFilter_forModelSettingsFilters($model, $filter_type, $filter_type_params, $model_filter_key, $model_filter_value);
    }
    
    private function parseModelFilter_modelSettingsStatuses($model, $column, $status_data, $model_filter_value)
    {
        $mini_condition = new SIMADbCriteria;
        $mini_condition->DBbackend = $this->DBbackend;
        if (isset($status_data['filter_func']))
        {
            $funcName = $status_data['filter_func'];
            $model->$funcName($mini_condition);
        }
        else
        {
            $mini_condition->FilterBooleanCondition($column, $model_filter_value, $this->alias);
        }
        $this->mergeWith($mini_condition);
    }
    
    private function parseModelFilter_ids($ids)
    {
        if(empty($ids))
        {
//            throw new Exception('empty ids');
            $this->logWronglyUsedAttribute(__METHOD__, ' empty ids', $this);
            return;
        }
        
        if($ids === SIMADbCriteria::$NULL || $ids === 'null' || $ids === 'NULL')
        {
            throw new Exception('invalid usage');
//            error_log(__METHOD__.' - invalid usage');
        }
        
        if(is_array($ids))
        {
            if(!empty($ids) && !isset($ids[0]))
            {
                $this->logWronglyUsedAttribute(__METHOD__, ' array ids first index not zero - $ids: '.SIMAMisc::toTypeAndJsonString($ids), $this);
            }
            if(!empty($ids) && is_null(reset($ids)))
            {
                $this->logWronglyUsedAttribute(__METHOD__, ' array ids null', $this);
                return;
            }
            $ids = implode(',', $ids);
        }
        //MilosS(27.9.2018)
        //koristi se samo u JobFinance i PartnerFinance
        //treba obrisati nakon reimplemnetacije pregleda troskova
        if (gettype($ids) === 'string' && strpos($ids, '},{') !== FALSE)
        {
            $_temp_ids_array = explode('},{', $ids);
            if (count($_temp_ids_array)>1)
            {
                $_new_ids = [];
                foreach ($_temp_ids_array as $_ids_subarray)
                {
                    $_new_ids = array_merge($_new_ids,explode(',',str_replace(['{','}'], '', $_ids_subarray)));
                }
                $ids = implode(',', $_new_ids);
            }
        }
       
        $condition = $this->alias.'.id IN ('.$ids.')';
        
        $this->addCondition($condition);
    }
    
    private function parseModelFilter_text($model, $text)
    {        
        $text_trimmed = trim($text);
        
        if(empty($text_trimmed))
        {
            return;
        }
        
        $exploded_text = explode(' ', $text_trimmed);
        
        foreach ($exploded_text as $part)
        {
            $this->parseModelFilter_text_recursive($model, $part, 'AND');
        }
    }
    
    /**
     * 
     * @param type $model
     * @param type $text
     * @param type $and_or
     * @param type $processed_models
     * @param type $current_depth_level - nivo rekurzije
     * @return type
     */
    private function parseModelFilter_text_recursive($model, $text, $and_or, $processed_models=[], $current_depth_level=0)
    {
        //MilosS(13.9.2017) - ostavljeno zakomentarisano radi demonstracije
//        error_log('ULAZ: '.SIMAMisc::toTypeAndJsonString($processed_models));
        if (in_array(get_class($model), $processed_models))
        {
            //MilosS(13.9.2017) - ostavljeno zakomentarisano radi demonstracije
//            error_log('POKLAPA SE '.get_class($model));
            return;
        }
        
        array_push($processed_models, get_class($model));

        $_q = $this->spech_chars[$this->DBbackend]['col_quotes'];
        $search_columns = $model->modelSettings('textSearch');
        if (count($search_columns) === 0)
        {
            Yii::app()->errorReport->createAutoClientBafRequest("Za model ".get_class($model)." je pozvana pretraga po tekstu, a nije zadata nijedna kolona u textSearch-u u modelSettings-u.");
        }
        
        $temp_condition = new SIMADbCriteria();
        $temp_condition->DBbackend = $this->DBbackend;

        foreach ($search_columns as $col => $col_type)
        {
//            $text2 = $text;
//            $text2 = str_replace("'", "''", $text2);
            $text2 = str_replace("'", "''", $text);
            $_col_type = $col_type;
            if (is_array($col_type))
            {
                $_col_type = $col_type[0];
            }
            switch ($_col_type)
            {
                case 'text':                          
                    $wildcard = false;

                    if (is_array($col_type))
                    {
                        if (isset($col_type['options']))
                        {
                            if (in_array('wildcard', $col_type['options']))
                            {
                                $text2 = $this->wildCard($text2);
                                $wildcard = true;
                            }
                        }
                        if (isset($col_type['use_cyrillic']) && $col_type['use_cyrillic'] === true)
                        {
                            $search_text_cyrillic = SIMAMisc::convertLatinToCyrillic($text2);
                            $search_text_latin = SIMAMisc::convertCyrillicToLatin($text2);                                    
                        }
                    }


                    //MilosS(12.7.2016) - zakomentarisano dok ne smislimo nesto bolje
                    //$text2 = str_replace($this->text_search_special_characters, '', $text2);

                    if (!$wildcard)
                    {
                        $text2 = preg_quote($text2);
                    }
                    $text2 = $this->SerbianLettersSearch($text2);
                    
                    $text_to_search = $text2;

                    if (!empty($search_text_cyrillic) && !empty($search_text_latin))
                    {
                        $additional_pgsql = ' OR '. $this->alias . '.'.$_q.$col.$_q.' ~* \''.$search_text_latin.'\'';
                        $additional_mysql = $this->alias . '.'.$_q.$col.$_q.' like "%'.$search_text_cyrillic.'%" or '.$this->alias . '.'.$_q.$col.$_q.' like "%'.$search_text_latin.'%"';
                        $text_to_search = $search_text_cyrillic;
                    } 
                    else 
                    {
                        $additional_pgsql = '';
                        $additional_mysql = $this->alias . '.'.$_q.$col.$_q.' like "%'.$text2.'%"';
                    }
                    
                    $text_condition = null;
                    
                    if ($this->DBbackend === 'pgsql')
                    {
                        $text_condition = $this->alias . '.'.$_q.$col.$_q.' ~* \'' . $text_to_search .'\'' . $additional_pgsql;
                    }
                    else if($this->DBbackend === 'mysql')
                    {
                        error_log(__METHOD__.' - nije pokriveno - 4 - mysql');
                        $text_condition = $additional_mysql;
                    }
                    $temp_condition->addCondition($text_condition, 'OR');
                    break;
                case 'integer':
                    $temp_condition->alias = $this->alias;
                    $temp_condition->FilterIntegerCondition($col, $text2, false);
                    break;
                case 'phone_number':
                    $temp_condition->alias = $this->alias;
                    $temp_condition->FilterPhoneNumber($col, $text2); 
                    break;                            
                case 'relation':
                    if($current_depth_level >= Yii::app()->params['SIMADbCriteria_textSearch_depthLimit'])
                    {
                        break;
                    }
                    $current_depth_level_up_one = $current_depth_level + 1;
                    $relations = $model->relations();
                    $relation = $relations[$col];
                    $relation_alias = 't'.SIMAHtml::uniqid();
                    switch ($relation[0])
                    {
                        case 'CHasManyRelation':
                            $this->parseModelFilter_text_recursive_relation_hasMany($temp_condition, $relation, $processed_models, $text, $relation_alias, $current_depth_level_up_one);
                            break;
                        case 'CHasOneRelation':
                            $mini_condition = new SIMADbCriteria();
                            $mini_condition->DBbackend = $this->DBbackend;
                            $mini_condition->alias = $relation_alias;
                            $mini_condition->parseModelFilter_text_recursive($relation[1]::filter(array()), $text, 'OR', $processed_models, $current_depth_level_up_one);
                            $pure_cond = $mini_condition->getPureCondition();
                            
                            /// MilosJ 20170115 - ako nema condition-a, odradjen je return na pocetku recursive poziva
                            if(!empty($pure_cond))
                            {
                                /**
                                 * MilosJ:
                                 * ubrzava kada je u pitanju Filing
                                 * primer koda koji ovo oseti:
                                 * Bill::model()->findAll([
                                        'model_filter' => [
                                            'text' => '4444'
                                        ],
                                        'limit' => 50
                                    ]);
                                 */
                                if($relation[1] === Filing::class)
                                {
                                    $condition = 'EXISTS (SELECT 1'
                                            . ' FROM '.$relation[1]::model()->tableName().' '.$relation_alias
                                            . " WHERE $this->alias.id=$relation_alias.$relation[2]";
                                    if(!empty($pure_cond))
                                    {
                                        $condition .= ' AND ('.$pure_cond.')';
                                    }
                                    $condition .= ' LIMIT 1)';
                                }
                                else
                                {
                                    $condition = "$this->alias.id IN (SELECT $relation_alias.$relation[2]"
                                            . ' FROM '.$relation[1]::model()->tableName().' '.$relation_alias;
                                    if(!empty($pure_cond))
                                    {
                                        $condition .= ' WHERE ('.$pure_cond.')';
                                    }
                                    $condition .= ')';
                                }
                                $temp_condition->addCondition($condition, 'OR');
                            }
                            break;
                        case 'CBelongsToRelation':
                            $mini_condition = new SIMADbCriteria();
                            $mini_condition->DBbackend = $this->DBbackend;
                            $mini_condition->alias = $relation_alias;
                            $mini_condition->parseModelFilter_text_recursive($relation[1]::filter(array()), $text, 'OR', $processed_models, $current_depth_level_up_one);
                            $pure_cond = $mini_condition->getPureCondition();
                            
                            /// MilosJ 20170115 - ako nema condition-a, odradjen je return na pocetku recursive poziva
                            if(!empty($pure_cond))
                            {
                                /**
                                 * MilosJ:
                                 * ubrzava kada je u pitanju Filing
                                 * primer koda koji ovo oseti:
                                 * Bill::model()->findAll([
                                        'model_filter' => [
                                            'text' => '4444'
                                        ],
                                        'limit' => 50
                                    ]);
                                 */
                                if($relation[1] === File::class)
                                {
                                    $condition = 'EXISTS (SELECT 1'
                                            . ' FROM '.$relation[1]::model()->tableName().' '.$relation_alias
                                            . " WHERE $relation_alias.id=$this->alias.$relation[2]";
                                    if(!empty($pure_cond))
                                    {
                                        $condition .= ' AND ('.$pure_cond.')';
                                    }
                                    $condition .= ' LIMIT 1)';
                                }
                                else
                                {
                                    $condition = "$this->alias.$relation[2] IN (SELECT $relation_alias.id"
                                            . ' FROM '.$relation[1]::model()->tableName().' '.$relation_alias;
                                    if(!empty($pure_cond))
                                    {
                                        $condition .= ' WHERE ('.$pure_cond.')';
                                    }
                                    $condition .= ')';
                                }
                                $temp_condition->addCondition($condition, 'OR');
                            }
                            break;
                    }
                    break;
            }
        }
        $this->mergeWith($temp_condition);
    }
    
    /**
     * 
     * @param SIMADbCriteria $temp_condition
     * @param array $relation
     * @param array $processed_models
     * @param type $text
     * @param type $relation_alias
     * @param type $current_depth_level - nivo recurzije
     */
    private function parseModelFilter_text_recursive_relation_hasMany(SIMADbCriteria $temp_condition, array $relation, array $processed_models, $text, $relation_alias, $current_depth_level)
    {        
        $mini_condition = new SIMADbCriteria();
        $mini_condition->DBbackend = $this->DBbackend;
        $mini_condition->alias = $relation_alias;
        
        if(isset($relation['condition']))
        {
            $mini_condition->condition = $relation['condition'];
        }
        
        $mini_condition->parseModelFilter_text_recursive($relation[1]::filter(array()), $text, 'OR', $processed_models, $current_depth_level);
        $pure_cond = $mini_condition->getPureCondition();

        /// MilosJ 20170115 - ako nema condition-a, odradjen je return na pocetku recursive poziva
        if(!empty($pure_cond))
        {
//                            $condition = 'EXISTS (SELECT 1'
//                                    . ' FROM '.$relation[1]::model()->tableName().' '.$relation_alias
//                                    . " WHERE $this->alias.id=$relation_alias.$relation[2]";
//                            if(!empty($pure_cond))
//                            {
//                                $condition .= ' AND ('.$pure_cond.')';
//                            }
//                            $condition .= ' LIMIT 1)';
            $condition = "$this->alias.id IN (SELECT $relation_alias.$relation[2]"
                    . ' FROM '.$relation[1]::model()->tableName().' '.$relation_alias;
            if(!empty($pure_cond))
            {
                $condition .= ' WHERE ('.$pure_cond.')';
            }
            $condition .= ')';
            $temp_condition->addCondition($condition, 'OR');
        }
    }
    
    private function FilterPhoneNumber($column, $text)
    {        
        $_q = $this->spech_chars[$this->DBbackend]['col_quotes'];
        $_column = $this->alias.'.'.$_q.$column.$_q;
        $exploded = explode(' ', $text);
        foreach ($exploded as $part)
        {
            $part = preg_quote($part);
            $temp_condition = new CDbCriteria();
            if (preg_match('/[A-Za-z]/', $part))
            {
                $part = $this->SerbianLettersSearch($part);
                $temp_condition->condition = "$_column ~* '$part'";
            }
            else
            {
                $temp_condition->condition = 
                    "'$part' ~ '[0-9]+' and '$part' !~ '[a-zA-Z]' and
                    $_column ~ '[0-9]+' and $_column !~ '[a-zA-Z]' and
                    (
                          regexp_replace(regexp_replace($_column, '[^0-9]', '', 'g') , '^0', '') ~ regexp_replace(regexp_replace('$part', '[^0-9]', '', 'g') , '^0', '')
                          OR
                          regexp_replace(regexp_replace('$part', '[^0-9]', '', 'g') , '^0', '') ~ regexp_replace(regexp_replace($_column, '[^0-9]', '', 'g'), '^0', '')
                    )";
            }
            $this->mergeWith($temp_condition);
        }
    }
    
    private function FilterIntegerCondition($column, $text, $and = true)
    {
        if ($text == '')
        {
            return;
        }
        $exploded = explode(' ', $text);
        
        $operator = 'AND';
        if($and === false)
        {
            $operator = 'OR';
        }
        
        foreach ($exploded as $part)
        {
            $condition = 'CAST(' . $this->alias . '."' . $column . '" AS TEXT) ~* \''.preg_quote($part).'\'';
            $this->addCondition($condition, $operator);
        }
    }
    
    private function parseModelFilter_orderBy($model, $order_by)
    {        
        if(!is_string($order_by))
        {
            throw new Exception('currently only string supported');
        }
                
        $parts = explode(" ", $order_by);
        $rel_parts = explode('.', $parts[0]);
        $order_alias = $this->alias;
        $order_column = '';
        $curr_model = $model;
        $i = 0;
        $len = count($rel_parts);
        $relation = '';
        foreach ($rel_parts as $rel_part)
        {
            $curr_relations = $curr_model->relations();
            if (isset($curr_relations[$rel_part]))
            {
                if($curr_relations[$rel_part][0] === SIMAActiveRecord::HAS_MANY)
                {
//                    $new_exception = new SIMADbCriteriaOrderByForbiddenOnHasMany();
//                    error_log(__METHOD__.' - $new_exception->getMessage(): '.$new_exception->getMessage());
                    throw new SIMADbCriteriaOrderByForbiddenOnHasMany();
                }
                
                if (isset($curr_relations[$rel_part]['alias']))
                {
                    $curr_rel_alias = $curr_relations[$rel_part]['alias'];
                }
                else
                {
                    $curr_rel_alias = $rel_part;
                }
                if ($relation !== '')
                {
                    $relation .= '.';
                }
                $relation .= $rel_part;
//                $order_alias = $curr_rel_alias.SIMAHtml::uniqid();
                $order_alias = $curr_rel_alias;
                $curr_model = $curr_relations[$rel_part][1]::model();
                //ako je relacija poslednji element u nizu onda dovlacimo iz te relacije kolonu po kojoj ce se vrsiti order
                if ($i == $len - 1)
                {
                    $database_columns = $curr_relations[$rel_part][1]::model()->getMetaData()->tableSchema->columns;
                    $default_order = $curr_model->modelSettings('default_order');
                    if(!is_string($default_order))
                    {
                        throw new SIMAExceptionDbCriteriaDefaultOrderInvalid($curr_model, 'not string');
                    }
                    if ($default_order !== '')
                    {
                        $order_column = $default_order;
                    }
                    else if (isset($database_columns['display_name']))
                    {
                        $order_column = 'display_name';
                    }
                    else
                    {
                        throw new SIMAWarnException('U modelu ' .  get_class($curr_model) . ' morate postaviti default order.');
                    }
                }
                $i++;
            }
            else
            {
                $order_column = $rel_part;

                $columns_custom_order = $curr_model->modelSettings('columns_custom_order');
                if (!empty($columns_custom_order[$order_column]))
                {
                    $order_column = $columns_custom_order[$order_column];
                }
            }
        }
        $direction = '';
        if (isset($parts[1]))
        {
            $direction = $parts[1];
        }

        if(empty($relation))
        {
            $this->with = $relation;
        }
        else
        {
            $this->with = [
                $relation => [
//                    'select' => $order_column,
//                    'joinType' => 'JOIN',
                    'alias' => $order_alias,
    //                'condition' => $order_alias.".".$order_column." IS NOT NULL"
                ]
            ];
        }
        $this->order = $order_alias.".".$order_column." ".$direction;
    }
    
    private function parseModelFilter_limit($limit)
    {
        $this->limit = $limit;
    }
    
    private function parseModelFilter_offset($offset)
    {
        $this->offset = $offset;
    }
    
    private function parseModelFilter_forModelSettingsFilters($model, $filter_type, $filter_type_params, $model_filter_key, $model_filter_value)
    {
        switch($filter_type)
        {
            case SIMADbCriteria::$MODELSETTINGSFILTERS_TEXT:
                $this->parseModelFilter_forModelSettingsFilters_text($model, $filter_type, $filter_type_params, $model_filter_key, $model_filter_value);
                break;
            case SIMADbCriteria::$MODELSETTINGSFILTERS_DROPDOWN:
                $this->FilterSimpleCondition($model_filter_key, $model_filter_value, $model->getTableAlias());
                break;
            case SIMADbCriteria::$MODELSETTINGSFILTERS_RELATION:
                $this->parseModelFilter_forModelSettingsFilters_relation($model, $model_filter_key, $model_filter_value);
                break;
            case SIMADbCriteria::$MODELSETTINGSFILTERS_DATERANGE:
                $this->parseModelFilter_forModelSettingsFilters_dateRange($model_filter_key, $model_filter_value, $model->getTableAlias());
                break;
            case SIMADbCriteria::$MODELSETTINGSFILTERS_DATETIMERANGE:
                $this->parseModelFilter_forModelSettingsFilters_dateTimeRange($model_filter_key, $model_filter_value, $model->getTableAlias());
                break;
            case SIMADbCriteria::$MODELSETTINGSFILTERS_BOOLEAN:
                $this->FilterBooleanCondition($model_filter_key, $model_filter_value, $model->getTableAlias());
                break;
            case SIMADbCriteria::$MODELSETTINGSFILTERS_INTEGER:
                $this->FilterIntegerCondition($model_filter_key, $model_filter_value);
                break;
            case SIMADbCriteria::$MODELSETTINGSFILTERS_NUMERIC:
                $this->FilterNumericCondition($model_filter_key, $model_filter_value, $model->getTableAlias(), $filter_type_params);
                break;
            case SIMADbCriteria::$MODELSETTINGSFILTERS_JSON:
                $this->filterJsonCondition($model_filter_key, $model_filter_value, $model->getTableAlias());
                break;
            default:
                throw new Exception('not found $filter_type: '.CJSON::encode($filter_type));
        }
    }
    
    private function parseModelFilter_forModelSettingsFilters_dateRange($column, $value, $alias, $and = true)
    {
        if(empty($value))
        {
            return;
        }
        
        $temp_condition = new CDbCriteria();
        $param_id = SIMAHtml::uniqid();
                
        if( is_array($value) )
        {       
            $value_count = count($value);
            $operator = '=';
            $final_value = $value[0];
            $trunc_precision = null;
            
            if( $value_count == 3 )
            {
                $operator = $value[0];
                $final_value = $value[1];
                $trunc_precision = $value[2];
            }
            elseif( $value_count == 2 )
            {
                $operator = $value[0];
                $final_value = $value[1];
            }
            else if( $value_count != 1 )
            {
                throw new Exception('invalid array count');
            }
            $pure_param = ':param' . $param_id;
            if (is_null($trunc_precision))
            {
                $_param = $pure_param;
                $_column = $alias.'."' . $column . '"';
            }
            else
            {
                $_param = "date_trunc('$trunc_precision', $pure_param::TIMESTAMP)";
                $_column = "date_trunc('$trunc_precision',".$alias . '."' . $column . '"::TIMESTAMP)';
            }
            
            $temp_condition->condition = "$_column $operator '".SIMAHtml::UserToDbDate($final_value)."'::DATE";
            
        }   
        elseif (gettype($value) == 'string' && ($value == 'null' || $value == 'NULL'))
        {
            $temp_condition->condition = $alias . '."' . $column . '" is null';
        }
        else 
        {
            $exploded1 = explode('<>', $value);
            $exploded2 = explode('<!>', $value);            
            if (count($exploded1) == 1 && count($exploded2) == 1)
            {
                $temp_condition->condition = $alias . '."' . $column . '" = \''.SIMAHtml::UserToDbDate($value).'\'::DATE';
            }
            else 
            {
                if (count($exploded1) == 2)
                {
                    $exploded = $exploded1;
                    $sign1 = '>=';
                    $sign2 = '<=';
                }
                elseif (count($exploded2) == 2)
                {
                    $exploded = $exploded2;
                    $sign1 = '>';
                    $sign2 = '<';
                }
                else
                {
                    throw new Exception('pretraga po datumu ima neparvilan broj znakova' . $value);
                }

                $temp_condition->condition = "$alias.\"$column\" $sign1 '".SIMAHtml::UserToDbDate(trim($exploded[0]))."'::DATE "
                        ."and $alias.\"$column\" $sign2 '".SIMAHtml::UserToDbDate(trim($exploded[1]))."'::DATE";
            }
        }
        $this->mergeWith($temp_condition, $and);
    }

    private function parseModelFilter_forModelSettingsFilters_dateTimeRange($column, $value, $alias, $and = true)
    {
        $temp_condition = new CDbCriteria();
        $param_id = SIMAHtml::uniqid();
        
        if( is_array($value) )
        {       
            $value_count = count($value);
            $operator = '=';
            $final_value = $value[0];
            $trunc_precision = null;
            
            if( $value_count == 3 )
            {
                $operator = $value[0];
                $final_value = $value[1];
                $trunc_precision = $value[2];
            }
            elseif( $value_count == 2 )
            {
                $operator = $value[0];
                $final_value = $value[1];
            }
            else if( $value_count != 1 )
            {
                throw new Exception('invalid array count');
            }
            $pure_param = ':param' . $param_id;
            if (is_null($trunc_precision))
            {
                $_param = $pure_param;
                $_column = $alias.'."' . $column . '"';
            }
            else
            {
                $_param = "date_trunc('$trunc_precision', $pure_param::TIMESTAMP)";
                $_column = "date_trunc('$trunc_precision',".$alias . '."' . $column . '"::TIMESTAMP)';
            }
                
            $temp_condition->condition = "$_column $operator $_param";
            $temp_condition->params = array($pure_param => SIMAHtml::UserToDbDate($final_value, true, true));
            
        }
        elseif (gettype($value) == 'string' && ($value == 'null' || $value == 'NULL'))
        {
            $temp_condition->condition = $alias . '."' . $column . '" is null';
        }
        else
        {
            $exploded1 = explode('<>', $value);
            $exploded2 = explode('<!>', $value);
            if (count($exploded1) == 1 && count($exploded2) == 1)
            {   
                $nextday = SIMAHtml::UserToDbDate('+1 day'.$value, true, true);

                $temp_condition->condition = "$alias.\"$column\" >= :param1$param_id and $alias.\"$column\" < :param2$param_id";
                $temp_condition->params = array(":param1$param_id" => SIMAHtml::UserToDbDate($value, true, true), ":param2$param_id" => $nextday);
            }
            else 
            {
                if (count($exploded1) == 2)
                {
                    $exploded = $exploded1;
                    $sign1 = '>=';
                    $sign2 = '<=';
                }
                elseif (count($exploded2) == 2)
                {
                    $exploded = $exploded2;
                    $sign1 = '>';
                    $sign2 = '<';
                }
                else
                {
                    throw new Exception('pretraga po datumu ima neparvilan broj znakova' . $value);
                }

                $temp_condition->condition = "$alias.\"$column\" $sign1 '".SIMAHtml::UserToDbDate(trim($exploded[0]), true, true)."'::TIMESTAMP "
                        ."and $alias.\"$column\" $sign2 '".SIMAHtml::UserToDbDate(trim($exploded[1]), true, true)."'::TIMESTAMP";

            }
        }     
        $this->mergeWith($temp_condition, $and);
    }
    
    private function parseModelFilter_forModelSettingsFilters_text($model, $filter_type, $filter_type_params, $column, $search_text)
    {
        $options = [];
        $criteria_latin = new SIMADbCriteria();
        $criteria_latin->DBbackend = $this->DBbackend;
        $criteria_cyrillic = new SIMADbCriteria();
        $criteria_cyrillic->DBbackend = $this->DBbackend;
        
        $alias = $model->getTableAlias();

        if (isset($filter_type_params['options']))
        {
            $options = $filter_type_params['options'];
        }
        if (isset($filter_type_params['use_cyrillic']) && $filter_type_params['use_cyrillic'] === true)
        {
            $search_text_cyrillic = SIMAMisc::convertLatinToCyrillic($search_text);
            $search_text_latin = SIMAMisc::convertCyrillicToLatin($search_text);
        }
        if (!empty($search_text_cyrillic) && !empty($search_text_latin))
        {
            $criteria_cyrillic->FilterTextCondition(array($column), $search_text_cyrillic, $alias, $options);
            $criteria_latin->FilterTextCondition(array($column), $search_text_latin, $alias, $options);
            $criteria_latin->mergeWith($criteria_cyrillic, false);
            $this->mergeWith($criteria_latin);
        }
        else
        {
            $this->FilterTextCondition(array($column), $search_text, $alias, $options);
        }
    }
    
    private function parseModelFilter_forModelSettingsFilters_relation($model, $column, $model_filter_value)
    {
        $relations = $model->relations();
        $relation = $relations[$column];

        switch ($relation[0])
        {
            case 'CHasManyRelation':
                $this->parseModelFilter_forModelSettingsFilters_relation_hasMany($model, $relation, $model_filter_value);
                break;
            case 'CHasOneRelation':
                $this->parseModelFilter_forModelSettingsFilters_relation_hasOne($model, $relation, $model_filter_value);
                break;
            case 'CBelongsToRelation':
                $this->parseModelFilter_forModelSettingsFilters_relation_belongsTo($model, $relation, $model_filter_value);
                break;
        }

//        if ($local_join!='')
//        {
//            $this->join = $local_join . ' ' .$this->join;
//        }
//
//        //uslovi iz rekurzivnih poziva
//        if (gettype($model->$column)=='object' && $model->$column!=null && $local_join!='')
//        {
//            //postavljamo condition za sve statuse u relaciji
//            $statuses = $model->$column->modelSettings('statuses');
//            foreach ($statuses as $status_key => $status_value)
//            {
//                if (isset($status_value['filter_func']))
//                {
//                    $funcName = $status_value['filter_func'];
//                    $model->$column->$funcName($this);
//                }
//                else
//                {
//                    $this->FilterBooleanCondition($status_key, $model->$column->$status_key, $local_alias);
//                }
//            }
//        }
    }
    
    private function parseModelFilter_forModelSettingsFilters_relation_belongsTo($model, $relation, $model_filter_value)
    {
        $local_alias = null;
        if(isset($relation['alias']))
        {
            $local_alias = $relation['alias'];
        }
        else
        {
            $local_alias = 't'.SIMAHtml::uniqid();
        }
        $alias = $model->getTableAlias();
                
        $relation_model_name = $relation[1];
        $table_name = $relation_model_name::model()->tableName();
        $column = $relation[2];
        
        $new_condition = null;
        
        if($model_filter_value === SIMADbCriteria::$NULL)
        {
            $new_condition = "$alias.$column IS NULL";
        }
        else if($model_filter_value === 'null' || $model_filter_value === 'NULL')
        {
             $this->logWronglyUsedAttribute(__METHOD__, ' belongsto null', $this);
             $new_condition = "$alias.$column IS NULL";
        }
        else if(isset($model_filter_value['ids']) && (
                $model_filter_value['ids'] === SIMADbCriteria::$NULL
                ||
                $model_filter_value['ids'] === 'null' || $model_filter_value['ids'] === 'NULL'
            )
        )
        {
            $this->logWronglyUsedAttribute(__METHOD__, ' belongsto ids null', $this);
            $new_condition = "$alias.$column IS NULL";
        }
        else if(isset($model_filter_value['ids']) && count($model_filter_value) === 1) /// ako ima samo ids
        {
            $ids = $model_filter_value['ids'];
            if(is_array($ids))
            {
                $ids = join(',', $ids);
            }
            $new_condition = $alias.'.'.$column.' IN ('.$ids.')';
        }
        else
        {
            $sub_criteria = new SIMADbCriteria([
                'alias' => $local_alias,
                'Model' => $relation_model_name,
                'model_filter' => $model_filter_value,
            ]);

            $pure_condition = $sub_criteria->getPureCondition();

            $new_condition = "EXISTS (SELECT 1 FROM $table_name $local_alias WHERE $local_alias.id = $alias." . $column;

            if(!empty($pure_condition))
            {
                $new_condition .= ' AND ('.$pure_condition.')';
            }
            
            if(isset($relation['condition']))
            {
                $new_condition .= ' AND ('.$relation['condition'].')';
            }

            $new_condition .=  ' LIMIT 1)';
            
            if(isset($relation['params']))
            {
                $new_condition = $this->changeParamsInCondition($new_condition, $relation['params']);
            }
        }
        
        $this->addCondition($new_condition);
        
        /**
         * Milos Sreckovic:
         * ukoliko postoje dva filtera:
         *  'document_type_id' => 'dropdown',
         *  'document_type' => 'relation'
         * i postavljen je filter 'document_type_id', onda relacisjski filter ne sme da se koristi
         */
//        if ($model->$rel_id=='' || $model->$rel_id == null)
//        {
//            $cnt_applies = $this->FilterConditionRecursive($model->$column, $local_alias, $filter_settings);
//            if ($cnt_applies > 0)
//            {
//                $local_join = 'left join ' . $relation[1]::model()->tableName() . ' ' . $local_alias . ' on ' . $local_alias . '.id=' . $alias . '.' . $relation[2];
//            }
//            else
//            {
//                $this->condition = "exists (select 1 from $table_name $local_alias where $local_alias.id = $alias." . $relation[2] . ')';
//            }
//        }
    }
    
    private function parseModelFilter_forModelSettingsFilters_relation_hasMany($model, $relation, $model_filter_value)
    {
        $local_alias = 't'.SIMAHtml::uniqid();
        $alias = $model->getTableAlias();
        $relation_model_name = $relation[1];
        $table_name = $relation_model_name::model()->tableName();
        $column = $relation[2];
        
        $relation_condition = '';
        if(isset($relation['condition']))
        {
            $relation_condition = $relation['condition'];
        }
        
        $scopes = [];
        if(isset($model_filter_value['scopes']))
        {
            $scopes = $model_filter_value['scopes'];
        }
        if(isset($relation['scopes']))
        {
            $scopes_to_add = $relation['scopes'];
            if(!is_array($scopes_to_add))
            {
                $scopes_to_add = [$scopes_to_add];
            }
            
            $scopes = array_merge($scopes_to_add, $scopes);
            
            $model_filter_value['scopes'] = $scopes;
        }
        
        $sub_criteria = new SIMADbCriteria([
            'alias' => $local_alias,
            'Model' => $relation_model_name,
            'model_filter' => $model_filter_value
        ]);
        
        $sub_criteria_pure_condition = $sub_criteria->getPureCondition();
        
        $new_condition = "EXISTS (SELECT 1 FROM $table_name $local_alias ";
        
        /**
         * TODO: videti da se implementira i za ostale relacije
         */
        if(!empty($sub_criteria->join))
        {
            $new_condition .= $sub_criteria->join;
        }
        
        $new_condition .= " WHERE $alias.id = $local_alias.".$column;
        
        if(!empty($relation_condition))
        {
            $new_condition .= ' AND ('.$relation_condition.')';
        }
        
        if(!empty($sub_criteria_pure_condition))
        {
            $new_condition .= ' AND ('.$sub_criteria_pure_condition.')';
        }
        
        $new_condition .=  ' LIMIT 1)';
        
        $this->addCondition($new_condition);
    }
    
    private function parseModelFilter_forModelSettingsFilters_relation_hasOne($model, $relation, $model_filter_value)
    {
        $local_alias = 't'.SIMAHtml::uniqid();
        $alias = $model->getTableAlias();
        $relation_model_name = $relation[1];
        $table_name = $relation_model_name::model()->tableName();
        $column = $relation[2];
                
        $sub_criteria = new SIMADbCriteria([
            'alias' => $local_alias,
            'Model' => $relation_model_name,
            'model_filter' => $model_filter_value
        ]);
        
        $sub_criteria_pure_condition = $sub_criteria->getPureCondition();
        
        $new_condition = "EXISTS (SELECT 1 FROM $table_name $local_alias WHERE $local_alias.$column = $alias.id";
        
        if(!empty($sub_criteria_pure_condition))
        {
            $new_condition .= ' AND ('.$sub_criteria_pure_condition.')';
        }
        
        $new_condition .=  ' LIMIT 1)';
        
        $this->addCondition($new_condition);
    }
    
    public function getPureCondition()
    {
        $condition = $this->condition;
        $condition = $this->changeParamsInCondition($condition, $this->params);
        return $condition;
    }
    
    private function changeParamsInCondition($input_condition, $params)
    {
        $condition = $input_condition;
        if(!empty($params) && is_array($params))
        {
            $indexed=$params==array_values($params);
            foreach($params as $key => $value)
            {
//                $condition = str_replace($key, $value, $condition);
                if(is_string($value)) $value="'$value'";
                if($indexed)
                {
                    $condition=preg_replace('/\?/',$value,$condition,1);
                }
                else
                {
                    $condition=str_replace($key,$value,$condition);
                }
            }
        }
        return $condition;
    }
    
    public function logWronglyUsedAttribute($method, $attribute, $object)
    {
        if(Yii::app()->params['log_wrongly_used_attributes'] === true)
        {
            error_log($method.' - koristi se '.$attribute);
            
            if(Yii::app()->isWebApplication())
            {
//                error_log($method.' - getallheaders(): '.CJSON::encode(getallheaders()));
                error_log($method.' - INPUT_GET: '.CJSON::encode(filter_input_array(INPUT_GET)));
            }
            
            if(is_object($object))
            {
                error_log($method.' - get class $object: '.get_class($object));
            }
            error_log($method.' - $object: '.CJSON::encode($object));
            error_log($method.' - _model: '.SIMAMisc::toTypeAndJsonString($this->_model));
            error_log($method.' - _model_filter: '.SIMAMisc::toTypeAndJsonString($this->_model_filter));
        }
    }
}