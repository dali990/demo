<?php

class SIMAClientScript extends NLSClientScript //CClientScript //NLSClientScript //ExtMinScript
{
    public function init()
    {
        $this->mergeJsExcludePattern = '/tiny_mce|tinymce|Plumb|jquery.js|jquery.min.js|jquery-ui/';

        //getting url of the document root
        if (!$this->serverBaseUrl) {
            if(Yii::app()->isWebApplication())
            {
                $this->serverBaseUrl = strtolower(preg_replace('#/.*$#','',$_SERVER['SERVER_PROTOCOL'])) . '://' . $_SERVER['HTTP_HOST'];
                if ($_SERVER['SERVER_PORT'] != 80)
                        $this->serverBaseUrl .= ':' . $_SERVER['SERVER_PORT'];
            }
            else
            {
                throw new Exception('must set serverBaseUrl');
            }
        }
        
        parent::init();
        if (Yii::app()->request->isAjaxRequest)
        {
            //ovde MORA reset da bi se ubile inicijalno registrovane skripte
            $this->reset(); 
//            $this->corePackages = array();
//            $this->scriptFiles = array();
        }
        
        if (Yii::app()->configManager->get('base.develop.use_jquery_3_3_1'))
        {
            $this->scriptMap = [
                'jquery.min.js' => 'js/jquery.min.js'
            ];
        }
    }

    /**
     * tu je samo da ne bi brisali po celom kodu
     */
    public function clearScriptFiles()
    {
//        $this->corePackages = array();
//        $this->scriptFiles = array();
    }

    public function renderCoreScripts()
    {
        if (Yii::app()->request->isAjaxRequest)
        {
//            $this->reset(); - ne moze jer ubija i lokalno registrovane skripte
            $this->corePackages = array();
            $this->scriptFiles = array();
        }

        parent::renderCoreScripts();
    }
    
    /**
     * Funkcija zadaje jedinstveno ime u zavosnosti od updatingStartTimestamp imena
     * @param type $name
     * @param type $ext
     * @return type
     */
    protected function hashedName($name, $ext = 'js') {
//        $version = Yii::app()->params['updatingStartTimestamp'];
        $version = Yii::app()->maintenanceManager->getUpdatingStartTimestamp();
        $simaSize = Yii::app()->sima_size;
        return 'sima' 
            . '-' . $version . '-'
            . crc32($name.$version.$simaSize)
//              nema potrebe za minimalnom verzijom jer se ne koristi
            . ( ($ext=='js'&&$this->compressMergedJs)||($ext=='css'&&$this->compressMergedCss) ? '-min':'') 
            . '.' . $ext;
    }
    
    public function registerCssFile($url,$media='')
    {
        $newUrl = '';
        
        $cleanUrl = substr($url, 0, strpos($url, ".css")+4);
        
        $sima_size = Yii::app()->sima_size;
//        $updating_start_timestamp = Yii::app()->params['updatingStartTimestamp'];
        $updating_start_timestamp = Yii::app()->maintenanceManager->getUpdatingStartTimestamp();
        
        $file_name = $updating_start_timestamp.'_'.str_replace('.css', '_'.$sima_size.'.css', basename($url));
        
        if (strpos($url,'assets/') !== false)
        {
            $css_dir = dirname($url);
            
            $newUrl = $css_dir . DIRECTORY_SEPARATOR . $file_name;
            
            $cleanNewUrl =$newUrl;
        }
        else
        {
            $newUrl = Yii::app()->assetManager->baseUrl . '/' . crc32($this->serverBaseUrl) . $file_name;
            
            $cleanNewUrl = Yii::app()->assetManager->baseUrl . '/' . crc32($this->serverBaseUrl) . $file_name;
        }
        
        $cleanNewUrl = substr($cleanNewUrl, 0, strpos($cleanNewUrl, ".css")+4);
        
        $base_url = Yii::app()->getBaseUrl();
        if(!empty($base_url))
        {
            $base_url_strlen = strlen($base_url);
            $cleanUrlBasePos = strpos($cleanUrl, $base_url);
            if($cleanUrlBasePos !== false)
            {
                $cleanUrl = substr($cleanUrl, $cleanUrlBasePos+$base_url_strlen+1);
            }
            $cleanNewUrlBasePos = strpos($cleanNewUrl, $base_url);
            if($cleanNewUrlBasePos !== false)
            {
                $cleanNewUrl = substr($cleanNewUrl, $cleanNewUrlBasePos+$base_url_strlen+1);
            }
        }
        else
        {
            if(substr($cleanUrl,0,1) == '/')
            {
                $cleanUrl = substr($cleanUrl, 1);
            }
            if(substr($cleanNewUrl,0,1) == '/')
            {
                $cleanNewUrl = substr($cleanNewUrl, 1);
            }
        }
        
        if(!file_exists($cleanNewUrl))
        {
            $content=$this->parse($cleanUrl);
            $handle=fopen($cleanNewUrl,'w');
            fwrite($handle,$content);
            fclose($handle);

            if(Yii::app()->isConsoleApplication())
            {
                chmod($cleanNewUrl, 0777);
            }
        }
                        
        parent::registerCssFile($newUrl,$media);
        
        return $this;
    }
    
    private function parse($file)
    {
        if(!file_exists($file))
        {
            throw new Exception('file does not exists: '.CJSON::encode($file));
        }
        
        $lines=file($file);
        $content='';
        foreach($lines as $line)
        {
            $line = $this->findAndReplaceVars($line);
            $line = $this->relativePathToAbsolute($file, $line);
            
            $content .= $line;
        }
        return $content;
    }
    
    private function findAndReplaceVars($line)
    {
        $vars = null;
        preg_match_all('/\s*\\$([A-Za-z1-9_\-]+)(\s*:\s*(.*?);)?\s*/', $line, $vars);
        $found=$vars[0];
        $varNames=$vars[1];
        $count=count($found);
        
        if($count > 0)
        {
            $sima_size = Yii::app()->sima_size;
            $cssVars = Yii::app()->params['cssVars'.$sima_size];
            
            for($i=0; $i<$count; $i++)
            {
                $varName=trim($varNames[$i]);
                
                $varValue = $cssVars[$varName];
                
                $line=preg_replace('/\\$'.$varName.'(\W|\z)/',$varValue.'\\1',$line);
            }
        }
        return $line;
    }
    
    private function relativePathToAbsolute($file, $line)
    {
        $matches = null;
        preg_match_all("/url\((?!'?\"?data:image)(.+)\)/U", $line, $matches);
        if(count($matches) !== 2)
        {
            errror_log(__METHOD__.' - count($matches) !== 2');
            return $line;
        }
        
        $url_matches = array_unique($matches[1]);
        
        if(empty($url_matches))
        {
            return $line;
        }
        
        foreach($url_matches as $image_path_match)
        {
            $image_path_match = trim($image_path_match, " \t\n\r\0\x0B\"'");
            
            /**
             * finalna putanja slike predstavlja putanju fajla (bez samog fajla, tj od roditeljskog dir) uz kretanje kroz putanju slike
             *  '.' - znaci da ostajemo u istom dir
             *  '..' - znaci da mora korak unazad, tj izbacuje se iz putanje poslednji
             *  sve ostalo se nadovezuje
             */

            $file_path_exploded = explode('/', $file);
            array_pop($file_path_exploded); /// izbacuje se sam fajl iz putanje

            $image_path_match_exploded = explode('/', $image_path_match);

            foreach($image_path_match_exploded as $image_path_part)
            {
                if($image_path_part === '..')
                {
                    array_pop($file_path_exploded);
                }
                else if($image_path_part === '.')
                {
                    continue;
                }
                else
                {
                    $file_path_exploded[] = $image_path_part;
                }
            }

            $final_image_path = $this->serverBaseUrl.'/'.implode('/', $file_path_exploded);

            $line = str_replace($image_path_match, $final_image_path, $line);

//            error_log(__METHOD__." - \n in file ".$file." \n replaced: ".$image_path_match." \n with: ".$final_image_path." \n");
        }
        
        return $line;
    }
    
    public function renderHead(&$output='')
    {
        if(!$this->hasScripts)
                return;
        
        $this->renderCoreScripts();

        if(!empty($this->scriptMap))
                $this->remapScripts();

        $this->unifyScripts();

        parent::renderHead($output);

        return $output;
    }
    
    public function renderBodyBegin(&$output='')
    {
        if($this->enableJavaScript)
        {            
            parent::renderBodyBegin($output);
        }
        
        return $output;
    }
    
    public function renderBodyEnd(&$output = '')
    {        
        if($this->enableJavaScript)
        {
            parent::renderBodyEnd($output);
        }
        
        return $output;
    }
    
    public function renderBodyLoadAndReady(&$output = '')
    {        
        if($this->enableJavaScript)
        {
            parent::renderBodyLoadAndReady($output);
        }
        
        return $output;
    }
    
    protected function toAbsUrl($relUrl) {
        $result = $relUrl;
        if($relUrl[0] === '.')
        {            
            $result = rtrim($this->serverBaseUrl,'/') . '/' . ltrim(substr($relUrl, 2),'/');
        }
        else if(!preg_match('&^http(s?)://&',$relUrl))
        {
            $assetsStrPos = strpos($relUrl, '/assets/');
            if($assetsStrPos !== false)
            {
                $relUrl = substr($relUrl, $assetsStrPos);
            }
            else
            {
                $assetsStrPos = strpos($relUrl, '/js/');
                if($assetsStrPos !== false)
                {
                    $relUrl = substr($relUrl, $assetsStrPos);
                }
            }
            
            $result = rtrim($this->serverBaseUrl,'/') . '/' . ltrim($relUrl,'/');
        }
        return $result;
    }
    
    protected function _mergeJs($pos) {
            $smap = null;
            if (Yii::app()->request->isAjaxRequest) {
                    //do not merge for ajax requests
                    if (!$this->mergeIfXhr)
                            return;

                    if ($smap = @$_REQUEST['nlsc_map'])
                            $smap = @json_decode($smap);
            }

            if ($this->mergeJs && !empty($this->scriptFiles[$pos]) && count($this->scriptFiles[$pos]) > $this->mergeAbove) {
                    $finalScriptFiles = array();
                    $name = '/** Content:
';
                    $scriptFiles = array();
                    foreach($this->scriptFiles[$pos] as $src=>$scriptFile) {

                            $absUrl = $this->toAbsUrl($src);//from yii 1.1.14 $scriptFile can be an array

                            if ($this->mergeJsExcludePattern && preg_match($this->mergeJsExcludePattern, $absUrl)) {
                                    $finalScriptFiles[$src] = $scriptFile;
                                    continue;
                            }

                            if ($this->mergeJsIncludePattern && !preg_match($this->mergeJsIncludePattern, $absUrl)) {
                                    $finalScriptFiles[$src] = $scriptFile;
                                    continue;					
                            }

                            $h = $this->h($absUrl);
                            if ($smap && in_array($h, $smap))
                                    continue;

                            //storing hash
                            $scriptFiles[$absUrl] = $h;

                            $parsed_name = $src;
                            if(SIMAMisc::stringStartsWith($absUrl, $this->serverBaseUrl))
                            {
                                $parsed_name = substr($absUrl, strlen($this->serverBaseUrl));
                            }
                            $name .= $parsed_name . '
';
                    }

                    if (count($scriptFiles) <= $this->mergeAbove)
                            return;

                    $name .= '*/
';
                    $hashedName = $this->hashedName($name,'js');
                    $path = Yii::app()->assetManager->basePath . DIRECTORY_SEPARATOR . $hashedName;
                    $path = preg_replace('#\\?.*$#','',$path);
                    $url = Yii::app()->assetManager->baseUrl . '/'. $hashedName;

                    if (!file_exists($path)) {
                            $merged = '';

                            $nlsCode = ';if (!$.nlsc) $.nlsc={resMap:{}};
';

                            if(Yii::app()->isWebApplication())
                            {
                                if (!$this->ch)
                                        $this->initCurlHandler();

                                foreach($scriptFiles as $absUrl=>$h) {
                                        curl_setopt($this->ch, CURLOPT_URL, $absUrl);
                                        $ret = curl_exec($this->ch);
                                        $err = curl_error($this->ch);
                                        if (!$err) {
                                                $merged .= ($ret.'
;');
                                                $nlsCode .= '$.nlsc.resMap["' . $absUrl . '"]="' . $h . '";
';
                                        } else {
                                                $merged .= '
/*
error downloading ' . $absUrl . ':' . $err . '
curl_info:' . print_r(curl_getinfo($this->ch), true) . '
*/
';
                                        }
                                }

                                curl_close($this->ch);
                                $this->ch = null;
                            }
                            else
                            {
                                foreach($scriptFiles as $absUrl=>$h) {
                                    $merged .= file_get_contents($absUrl).'
;';
                                    $nlsCode .= '$.nlsc.resMap["' . $absUrl . '"]="' . $h . '";
';
                                }
                            }

                            if ($this->compressMergedJs)
                                    $merged = JSMin::minify($merged);

                            file_put_contents($path, $name . $merged . $nlsCode);

                            if(Yii::app()->isConsoleApplication())
                            {
                                chmod($path, 0777);
                            }
                    }

                    $finalScriptFiles[$url] = $url;
                    $this->scriptFiles[$pos] = $finalScriptFiles;
            }
    }
    
    protected function _mergeCss() 
    {
        if ($this->mergeCss && !empty($this->cssFiles)) 
        {
            $newCssFiles = array();
            $names = array();
            $files = array();
            foreach($this->cssFiles as $url=>$media) 
            {
                $absUrl = $this->toAbsUrl($url);

                if ($this->mergeCssExcludePattern && preg_match($this->mergeCssExcludePattern, $absUrl)) {
                        $newCssFiles[$url] = $media;
                        continue;
                }

                if ($this->mergeCssIncludePattern && !preg_match($this->mergeCssIncludePattern, $absUrl)) {
                        $newCssFiles[$url] = $media;
                        continue;
                }

                if (!isset($names[$media]))
                        $names[$media] = '/** Content:
';
                $names[$media] .= $url . '
';

                if (!isset($files[$media]))
                        $files[$media] = array();
                $files[$media][$absUrl] = $media;
            }

            //merging css files by "media"
            foreach($names as $media=>$name) 
            {

                if (count($files[$media]) <= $this->mergeAbove) {
                        $newCssFiles = array_merge($newCssFiles, $files[$media]);
                        continue;
                }

                $name .= '*/
';	
                $hashedName = $this->hashedName($name,'css');
                $path = Yii::app()->assetManager->basePath . DIRECTORY_SEPARATOR . $hashedName;
                $path = preg_replace('#\\?.*$#','',$path);
                $url = Yii::app()->assetManager->baseUrl . '/'. $hashedName;
//                                error_log(__METHOD__.' - $path: '.$path);

                if (!file_exists($path)) {
                        $merged = '';
                        if(Yii::app()->isWebApplication())
                        {
                            if (!$this->ch)
                                    $this->initCurlHandler();

                            foreach($files[$media] as $absUrl=>$media) {
                                    curl_setopt($this->ch, CURLOPT_URL, $absUrl);
                                    $ret = curl_exec($this->ch);
                                    $err = curl_error($this->ch);

                                    if (!$err) {
                                            $merged .= ($ret.'
    ');
                                    } else {
                                            $merged .= '
    /*
    error downloading ' . $absUrl . ':' . $err . '
    curl_info:' . print_r(curl_getinfo($this->ch), true) . '
    */
    ';
                                    }						
                            }

                            curl_close($this->ch);
                            $this->ch = null;
                        }
                        else
                        {
                            foreach($files[$media] as $absUrl=>$media) {
                                $merged .= file_get_contents($absUrl).'
';
                            }
                        }

                        if ($this->compressMergedCss)
                                $merged = self::minifyCss($merged);

                        file_put_contents($path, $name . $merged);
                }//if

                $newCssFiles[$url] = $media;
            }//media

            $this->cssFiles = $newCssFiles;
        }
    }
    
    public static function registerManual()
    {
        SIMAModule::registerModules(Yii::app(), true);

        Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/css/main.css');
        Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/css/formyii.css');
        Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/css/form.css');
//        Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/css/simaDialog.css'); //prebaceno u base modul
        Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/css/site.css');
        Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/css/addressbook.css');
//        Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/css/accounts.css');
        Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/css/jobs.css');
        Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/css/user.css');
        Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/css/ajaxView.css');
        Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/css/codemirror.css');            
        Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/css/tablesorter.css');
        Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/css/jquery.svg.css');

        if (Yii::app()->configManager->get('base.develop.use_jquery_3_3_1'))
        {
            Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/jquerymigrate.js');
        }

        Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/jquery.tablesorter.js');
        Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/jquery.nearest.js');
//            Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/jquery.corner.js');
        Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/jquery.scrolltab.js');
        Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/jquery.mousewheel.js');
        Yii::app()->clientScript->registerCoreScript('jquery.ui');
        Yii::app()->clientScript->registerScriptFile(Yii::app()->getClientScript()->getCoreScriptUrl() . '/jui/js/jquery-ui-i18n.min.js');
        Yii::app()->clientScript->registerCoreScript('yiiactiveform');
        Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/simaFileSearch.js');

        Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/hiddenForm.js');
        Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/codemirror.js');
//            Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/codemirror/javascript.js');
        Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/codemirror/xml.js');
        Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/codemirror/css.js');
         Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/codemirror/stex.js');
        Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/codemirror/htmlmixed.js');
        Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/codemirror/clike.js');
        Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/codemirror/shell.js');
        Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/codemirror/php.js');
        Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/codemirror/sql.js');
//            Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/codemirror/comment.js');
//            Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/codemirror/continuecomment.js');
        Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/uniqid.js');
        Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/jquery.jsPlumb-1.4.1-all.js');
        Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/jquery.simaCalendar.js');
        Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/jquery.clickoutside.js');
        Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/jquery.keycombinator.min.js');
        Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/jquery.caret.js');
        Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/moment.js');
//            Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/jquery.canvasAreaDraw.js');
        Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/jquery.GoogleMaps.js');
        Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/jquery.svg.js');
        Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/jquery.base64.js');

        //ovo je za FileVersions korisceno
//            Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/fabric.js');
//            Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/underscore-min.js');          

        //kod za ucitavanje jquery-ui.css
        $cs = Yii::app()->getClientScript();
        $cssCoreUrl = $cs->getCoreScriptUrl();
        $cs->registerCssFile($cssCoreUrl . '/jui/css/base/jquery-ui.css');


//            $this->widget('CTreeView'); ///Samo da bi se inkludovao deo za jquery za drvo
//            $this->widget('CActiveForm');



        EDateRangePicker::registerManual();
        CJuiDateTimePicker::registerManual();
//        EAjaxUpload::registerManual();
        ETinyMce::registerManual();
        SIMAResizablePanel::registerManual();
        SIMATabs::registerManual();
        SIMATree::registerManual();
        SIMASearchField::registerManual();
        SIMABelongs::registerManual();
        CFileBrowserWidget::registerManual();
        SIMADDCreator::registerManual();
        SIMAVMenu::registerManual();
        SIMAFileBrowser::registerManual();
        SIMAGuiTable::registerManual();
        SIMACodeEditor::registerManual();
        SIMAUpload::registerManual();
        SIMANumericFilter::registerManual();
        SIMAMultiSearchField::registerManual();
        SIMAOrganizationScheme::registerManual();
        SIMAAddressbook::registerManual();
        SIMAPagination::registerManual();
        SIMAUIFilter::registerManual();
        SIMATree2::registerManual();
        SIMADefaultLayout::registerManual();
        SIMADiffViewer::registerManual();

        //saljemo niz prevoda js-u
        $register_js_translations = "sima.addTranslations(".CJSON::encode(Yii::app()->params['js_translations']).");";
        Yii::app()->clientScript->registerScript('sima_register_js_translations', $register_js_translations, CClientScript::POS_HEAD);
    }
}
