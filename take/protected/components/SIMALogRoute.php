<?php


class SIMALogRoute extends CLogRoute
{
    
    /**
     *Yii::trace('string','string funkcije') - prikazuje u kom trenutku se to pokazuje
     * 
     * Yii::beginProfile('statuses',$_trace_cat);
     * Yii::endProfile('statuses',$_trace_cat);
     * @var type 
     */
    
    public $enabled = false;
    private $_time = null;
    private $_profile_stack = array();
    private $_profiles = array();
//    public $params = array();
    
    public function init()
    {
        parent::init();
        $this->enabled = true;
        $this->_time = microtime(true)*1000;
    }

    
    
    /**
     * 0->description
     * 1->trace
     * 2->system.db.CDbCommand
     * 3->1419331892.3466
     * @param type $logs
     */
    public function processLogs($logs)
    {
        error_log('');
        error_log('');
        error_log('');
        error_log('TRACE - BEGIN *********************************************');
        foreach ($logs as $log)
        {
            $stack = explode("\n", $log[0]);
            $message = $stack[0];
            $message_disp = str_pad($stack[0], 20);
            $level = $log[1];
//            $level_disp = str_pad($log[1], 10);
            $category = $log[2];
            $category_disp = str_pad($log[2], 40);
            $time = $log[3]*1000;
            $time_diff = $time - $this->_time;
            $time_diff_format = money_format('%!(#5n', $time_diff);
            if ($level === 'profile')
            {
                if (strpos($message, 'begin:')===0)
                {
                    array_push($this->_profile_stack, array(
                        'name' => $category.'.'.substr($message,6),
                        'time' => $time
                    ));
                }
                if (strpos($message, 'end:')===0)
                {
                    $_profile = array_pop($this->_profile_stack);
                    if (!isset($this->_profiles[$_profile['name']]))
                    {
                        $this->_profiles[$_profile['name']] = ['time'=>0,'cnt'=>'0'];
                    }
                    
                    $this->_profiles[$_profile['name']]['time'] += $time - $_profile['time']  ;
                    $this->_profiles[$_profile['name']]['cnt'] ++;
                }
            }
            else
            {
                error_log("$category_disp  -  $message_disp   - $time_diff_format");
                $this->_time = $time;
            }
        }
        ksort($this->_profiles);
        foreach ($this->_profiles as $key => $value)
        {
            $key_displ = str_pad($key, 70);
            $time_diff_format = money_format('%!(#5n', $value['time']);
            $_cnt = $value['cnt'];
            error_log("$key_displ => $time_diff_format - cnt:$_cnt");
        }
        error_log('TRACE - END ***********************************************');
    }
}