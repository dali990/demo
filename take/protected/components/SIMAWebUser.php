<?php

/**
 * SIMAWebModel dodatno omogucava pristup klasi User preko atributa ->model (funkcija getModel()).
 * Takođe omogucava (ukoliko je dodeljena privilegija masterAdmin), logovanje kao neki drugi korisnik
 * 
 * @package SIMA
 * @subpackage Base
 * @author Miloš Srećković <milossreckovic@set.rs>
 */
class SIMAWebUser extends CWebUser {
 
    /**
     * @var User model zaposlenog 
     */
    private $_model=NULL;
    
    /**
     *
     * @var Session model db sesije
     */
    private $_db_session_model = NULL;
    
    private $no_init = false;
    
    /**
     * 
     * @var array koji sadrzi username i password za validaciju i prijavljivanje 
     */
    private $authentification_credentials = null;
    
    /**
     *
     * @var SIMAUserIdentity 
     */
    private $_identity;
    
    public function __construct($params = array())
    {
        if (isset($params['no_init']) && $params['no_init']==true)
            $this->no_init = true;
    }
    
    public function init()
    {
        if (!$this->no_init)
            parent::init();
    }

    public function getUsername()
    {
        return $this->loadUser(Yii::app()->user->id)->username;
    }
    
    public function getmodel()
    {
        return $this->loadUser(Yii::app()->user->id);
    }
    
    public function getdb_session()
    {
        if(!isset($this->_db_session_model))
        {
            $this->_db_session_model = Session::model()->findByPk(Yii::app()->session['sima_session']);
        }
        
        return $this->_db_session_model;
    }

    public function getConfig($param)
    {
        return $this->getmodel()->confParamValue($param);
    }
    
    public function setAuthenticationCredentials($username, $password)
    {
        $this->authentification_credentials = [
            'username' => $username,
            'password' => $password
        ];
    }
    
//    public function authenticateDesktopApp()
//    {
//        if(empty($this->authentification_credentials))
//        {
//            throw new Exceptiion(Yii::t('SIMAWebUser', 'AuthenticationCredentialsNotSet'));
//        }
//        
//        $user = User::model()->findByAttributes(array('username'=>$this->authentification_credentials['username']));
//        
//        if(!isset($user))
//            $this->errorCode=self::ERROR_USERNAME_INVALID;
//        elseif(!$user->active)
//            $this->errorCode=self::ERROR_USERNAME_INVALID;
//        elseif($user->password!==$this->password && $user->password!==md5($this->password) )
//            $this->errorCode=self::ERROR_PASSWORD_INVALID;
//        else
//        {
//            $this->logInUser($user);            
//        }
//        return !$this->errorCode;
//    }
//    
//    public function authenticate()
//    {
//        
//    }
//    
    public function loginDesktopApp()
    {
        if(empty($this->authentification_credentials))
        {
            throw new Exception(Yii::t('SIMAWebUser', 'AuthenticationCredentialsNotSet'));
        }
        
        if($this->_identity===null)
        {
                $this->_identity=new SIMAUserIdentity(
                        $this->authentification_credentials['username'],
                        $this->authentification_credentials['password']
                );
                $this->_identity->authenticate();
        }
        
        if($this->_identity->errorCode===SIMAUserIdentity::ERROR_NONE)
        {
// 			echo "ulogovao se";
// 			$duration=$this->rememberMe ? 3600*24*30 : 0; // 30 days
                $duration= 0; // no remebering
                Yii::app()->user->login($this->_identity,$duration);
                return true;
        }
        else
                return false;
    } 
    public function loginSIMAMobileApp()
    {
        if(empty($this->authentification_credentials))
        {
            throw new Exception(Yii::t('SIMAWebUser', 'AuthenticationCredentialsNotSet'));
        }
        
        if($this->_identity===null)
        {
                $this->_identity=new SIMAUserIdentity(
                        $this->authentification_credentials['username'],
                        $this->authentification_credentials['password']
                );
                $this->_identity->authenticate();
        }
        
        if($this->_identity->errorCode===SIMAUserIdentity::ERROR_NONE)
        {
// 			echo "ulogovao se";
// 			$duration=$this->rememberMe ? 3600*24*30 : 0; // 30 days
                $duration= 0; // no remebering
                Yii::app()->user->login($this->_identity,$duration);
                return true;
        }
        else
                return false;
    } 
    public function loginMobileApp()
    {
        if(empty($this->authentification_credentials))
        {
            throw new Exception(Yii::t('SIMAWebUser', 'AuthenticationCredentialsNotSet'));
        }
        
        if($this->_identity===null)
        {
                $this->_identity=new SIMAUserIdentity(
                        $this->authentification_credentials['username'],
                        $this->authentification_credentials['password']
                );
                $this->_identity->authenticate();
        }
        
        if($this->_identity->errorCode===SIMAUserIdentity::ERROR_NONE)
        {
                return true;
        }
        else
                return false;
    }

    /**
     * Ucitava entitet zaposlenog iz baze
     * Ovo se pokrece prilikom svakog zahteva
     * @param int $id id ulogovanog korisnika
     * @return User vraca entitet zaposlenog
     */
    protected function loadUser($id=NULL)
    {
        if($this->_model===null)
        {
            if ($id===NULL) 
            {
                $id = Yii::app()->user->id;
            }
            
            if ($id!==NULL) 
            {
                $this->_model=User::model()->findByPk($id);
            }
        }
        return $this->_model;
    }
    
    /**
     * U bazi se pravi sesija pristupa sistemu SIMA. Automatski se pamti vreme. U Yii sesiji postaju dostupni parametri 
     * sima_session -> id SIMA sesije, 
     * system_company_id -> id kompanije ciji je trenutni pregled,
     * nema potrebe ucitavati lokalne promenljive jer se ova funkcija poziva samo prilikom logovanja, a one se ucitavaju prilikom svakog zahteva
     * rc_passwd -> pamti se sifra zbog logovanja na roundcube 
     * @todo popraviti logovanje na roundcube
     */
    protected function afterLogin($fromCookie)
    {
        parent::afterLogin($fromCookie);
        $this->_model = null;
        $username = $this->username;
        $server_FQDN = Yii::app()->params['server_FQDN'];
        
        $client_ip = Yii::app()->request->userHostAddress;
        $http_headers = getallheaders();
        if(isset($http_headers['X-Forwarded-For']))
        {
            $client_ip = $http_headers['X-Forwarded-For'];
        }
        
        $sessionType = 'TYPE_SIMAweb';
        $headers = apache_request_headers();
        if(isset($headers['SIMA-Connection-Type']))
        {
            $headerConnectionType = $headers['SIMA-Connection-Type'];
            switch($headerConnectionType)
            {
                case 'SIMADesktopApp':
                    $sessionType = 'TYPE_SIMADesktopApp';
                    break;
                case 'SIMAweb':
                    $sessionType = 'TYPE_SIMAweb';
                    break;
                case 'SIMAMobileApp':
                    $sessionType = 'TYPE_SIMAMobileApp';
                    break;
                default:
                    throw new Exception(Yii::t('SIMAWebUser', 'InvalidHeaderConnectionType', [
                        'headerConnectionType' => $headerConnectionType
                    ]));
            }
        }
        
        $connect_result = null;
        if($sessionType === Session::$TYPE_SIMAMobileApp)
        {
            $connect_result = $this->afterLogin_SMASessionId();
        }
        
        if(empty($connect_result))
        {
            $connect_result = SIMASQLFunctions::connect($username, $server_FQDN, $client_ip, $sessionType);
        }
        
        Yii::app()->session['sima_session'] = $connect_result;
        
        if($sessionType === 'TYPE_SIMAweb')
        {
            Yii::app()->sima_session->id = WebTabSession::createNew('SIMAWebUser')->id;
            
            $session = $this->getdb_session();
            $browser = $this->getBrowser();
            $session->operating_system = $browser['operating_system'];
            $session->browser_string = $browser['browser_string'];
            $session->browser = $browser['browser'];
            $session->browser_version = $browser['browser_version'];
            $session->save();
        }
    }
    
    private function getBrowser() 
    { 
        $browser_string = $_SERVER['HTTP_USER_AGENT'];
        $parsedagent = parse_user_agent($browser_string);
        $operating_system = null;

        //Dohvatanje operativnog sistema
        if (!empty($parsedagent['platform']))
        {
            $operating_system = $parsedagent['platform'];

            //Dodatak za pamcenje bitnih verzija windows-a
            if($operating_system === 'Windows')
            {
                $windows_version = '';
                if (preg_match('/Windows NT 5.1|Windows XP/i', $browser_string)) 
                {
                    $windows_version = ' XP';
                }
                elseif (preg_match('/Windows NT 6.1/i', $browser_string)) 
                {
                    $windows_version = ' 7';
                }
                elseif (preg_match('/Windows NT 6.2/i', $browser_string)) 
                {
                    $windows_version = ' 8';
                }
                elseif (preg_match('/Windows NT 10.0/i', $browser_string)) 
                {
                    $windows_version = ' 10';
                }
                $operating_system .= $windows_version;
            }
        }

        return [
            'browser_string' => $browser_string,
            'browser' => !empty($parsedagent['browser']) ? $parsedagent['browser'] : null,
            'browser_version' => !empty($parsedagent['version']) ? $parsedagent['version'] : null,
            'operating_system' => $operating_system
        ];
    } 
    
    private function afterLogin_SMASessionId()
    {
        $connect_result = null;
        
//        if(!Yii::app()->hasModule('mobile'))
//        {
//            throw new SIMAException('nema mobile modul da bi bio ovde');
//        }

        $google_token = Yii::app()->controller->filter_post_input('androidPushToken');
        $ad = AuthenticatedDevice::model()->find([
            'model_filter' => [
                'google_token' => $google_token,
                'in_usage' => true
            ]
        ]);
        if(!empty($ad))
        {
            $session = $ad->session([
                'condition' => 'user_id='.Yii::app()->user->id /// mozda je ovaj uredjaj bio pod drugim korisnikom, pa ima sesiju za drugog, tako da treba nova
            ]);
            if(!empty($session))
            {
                $session->last_seen = 'now()';
                $session->idle_since = 'now()';
                $session->save();
                $connect_result = $session->id;
            }
        }
        
        return $connect_result;
    }
    
    /**
     * Use SIMA as another user
     * @param integer $user_id id of user
     */
    public function useAs($user_id)
    {
        if (!Yii::app()->user->checkAccess('LogInAsUser')) return false;
        Yii::app()->sima_session['useAs_prev_id'] = Yii::app()->user->id;
        $this->logout(false); //ne unistavamo sesiju
        $ui = SIMAUserIdentity::impersonate($user_id);
        if($ui)
            Yii::app()->user->login($ui, 0);
        return true;
    }
    
    /**
     * 
     */
    public function useAsReturn()
    {
        if (!isset(Yii::app()->sima_session['useAs_prev_id'])) return;
        $this->logout(false); //ne unistavamo sesiju
        $ui = SIMAUserIdentity::impersonate(Yii::app()->sima_session['useAs_prev_id']);
        if($ui)
            Yii::app()->user->login($ui, 0);
        unset(Yii::app()->sima_session['useAs_prev_id']);
    }
    
    public function checkAccess($operation, $params=[], $model=null)
    {
        return Yii::app()->getAuthManager()->checkAccess($operation,$this->getId(),$params, $model);
    }
    
    public function getIsGuest()
    {
//        return (!isset(Yii::app()->user->model->curr_session) || parent::getIsGuest());

        /**
         * otkrilo se da je zbog duge neaktivnosti i isticanja cookie-a
         * srediti da sma u header-u (il bolje u post-u) salje sma token
         */
        $isset_curr_session = !isset(Yii::app()->user->model->curr_session);
        $parent_is_guest = parent::getIsGuest();
        $result = $isset_curr_session || $parent_is_guest;
                
//        if($result === true && Yii::app()->hasModule('mobile') && MobileComponent::isMobileAppRequest())
        if($result === true && MobileComponent::isMobileAppRequest())
        {
            $result = $this->getIsGuest_mobile();
        }
        
        return $result;
    }
    
    private function getIsGuest_mobile()
    {
        $action = filter_input(INPUT_GET, 'r');
        if(
                $action === 'user/ping'
                || SIMAMisc::stringStartsWith($action, 'mobile/mobileApi/')
            )
        {
            return true;
        }
        
        $e = new Exception();
        $stacktrace = $e->getTraceAsString();
        if(strpos($stacktrace, 'RequireLogin->handleBeginRequest') === false)
        {
            return true;
        }
        
        $headers = apache_request_headers();

        if(!isset($headers['SIMA-SMA-Token']))
        {
            return true;
        }
        
        $sma_token = $headers['SIMA-SMA-Token'];
        $ad = AuthenticatedDevice::model()->find([
            'model_filter' => [
                'sma_token' => $sma_token,
                'in_usage' => true
            ]
        ]);
        if(empty($ad))
        {
            return true;
        }
        
        $session = $ad->session;
        if(empty($session))
        {
            return true;
        }
        
        Yii::app()->session['sima_session'] = $session->id;          
        Yii::app()->user->id = $session->user_id;
        $this->_model = null;

        return false;
    }
    
    public function logout($destroySession=true)
    {        
        $active_session = $this->db_session;
//        $active_session->end_time = 'now';
//        $active_session->update();
        $active_session->endSession();
        
        // wiki logout
        Yii::app()->personalWiki->logout();
        
        parent::logout($destroySession);
    }
    
    public function inSafeZone()
    {
        $is_safe_zone = false;

        if (
                SIMAMisc::isLocalhost() ||
                empty(Yii::app()->passwordValidator->safe_zone_ip_range) || 
                SIMAMisc::isIpAddressInRange(SIMAMisc::getClientIpAddress(), Yii::app()->passwordValidator->safe_zone_ip_range)
           )
        {
            $is_safe_zone = true;
        }
        
        return $is_safe_zone;
    }
}
