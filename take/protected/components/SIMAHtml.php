<?php

/**
 * Klasa za ispis standardnih html delova
 * @package SIMA
 */
class SIMAHtml
{
    /**
     * funkcija koja vraca spisak SIMAhtmlTag-ova razdvojenih space-om.
     * <shema_name>-<table_name>_<id> ili tako nesto. <br />
     * ukoliko nema polje id, taj model se zanemaruje.<br />
     * moguce je proslediti i niz<br />
     * 
     * @param mixed $model - SIMAActiveRecord ili array
     * @return string
     */
    public static function getTag($model, $model_id = null)
    {
        if (is_array($model) && !empty($model_id))
        {
            $stacktrace = SIMAMisc::stackTrace();
            $autobaf_message = "
                Pozvan je SIMAHTML::getTag za niz modela i prosledjen je model_id, sto je nepravilno stanje. <br/>
                $stacktrace
            ";
            Yii::app()->errorReport->createAutoClientBafRequest($autobaf_message);
        }
        
        if (is_object($model) && !empty($model_id))
        {
            $model = $model->findByPkWithCheck($model_id);
        }
        
        return implode(' ', SIMAMisc::getModelTags($model));
    }
    
    /**
     * funkcija vraca text gresaka zbog kojih model nije sacuvan.
     * Tekst je generisan na osnovu pravila iz samog ActiveRecord (funkcija rules)
     * @param CActiveRecord $model - mode
     * @return string
     */
    public static function showErrors($model, $unique=false)
    {
        $message = '';
        
        $errors = $model->getErrors();
        
        $messages = [];
        
        foreach ($errors as $error) 
        {
            foreach ($error as $error1) 
            {
                $messages[] = $error1;
            }
        }
        
        if($unique === true)
        {
            $messages = array_unique($messages);
        }
        
        if(!empty($messages))
        {
            $message = implode('. ', $messages);
        }
        
        return $message;
    }
    
    /**
     * funkcija vraca text gresaka zbog kojih model nije sacuvan.
     * Tekst je generisan na osnovu pravila iz samog ActiveRecord (funkcija rules)
     * @param CActiveRecord $model - mode
     * @return string
     */
    public static function showErrorsInHTML($model, $title = null)
    {
        if ($title==null)
        {
            $title = Yii::t('SIMAHtml', 'SaveErrors');
        }
        $errors = $model->getErrors();
        if (count($errors)>0)
        {
            $message="<p>$title:</p><ul>";
            foreach ($errors as $col => $error) 
            {
                foreach ($error as $sub_error)
                {
                    $message .= '<li><strong>';
                    $message .= ($col === 'id')?'':$model->getAttributeLabel($col).' - ';
                    $message .= $sub_error.'</strong></li>';
                }
                
            }
            $message .= '</ul>';
        }
        else
        {
            $message = '<span>Nije bilo gresaka prilikom cuvanja modela</span>';
        }
        
        return $message;
    }
    
    /**
     * iscrtava zeleni i crveni kruzic u zavisnosti da li je status OK ili nije OK
     * @param Model $model - model za koji se setuje confirm
     * @param string $column - kolona u tom modelu
     * @param integer/string $pix_size - velicina ikonice - 16/24/32/400 - za druge velicine, resizeovati veliku sliku u /images
     * @param boolean $save - true je u slucaju da se nakon klika na status promena pamti u bazi, false se koristi u slucaju formi da se ne pamti 
     * direktno u bazi vec se kreira input sa vrednoscu tog statusa
     * @param string $form_status - status forme - treba u slucaju da nije prosla validacija(bitna je za statuse u formama, inace moze da se izostavi)
     * @return string
     */
    public static function status($model=null, $column=null, $pix_size=16, $save=true, $form_status='', $htmlOptions=array())
    {
        $_trace_cat = 'SIMA.SIMAHtml.status';
        Yii::beginProfile('whole',$_trace_cat);
        $ret = '';
        if ($model != null && $column != null)
        {
            Yii::beginProfile('part_one',$_trace_cat);
            $statuses = $model->modelSettings('statuses');
            $status_params = SIMAHtml::getStatusParams($model, $column);
            $column_allow_null = $status_params['column_allow_null'];
//            $has_access_confirm = $status_params['has_access_confirm'];
//            $has_access_revert = $status_params['has_access_revert'];
//            $has_access_errors = $status_params['has_access_errors'];
//            $beforeConfirm = $status_params['beforeConfirm'];
//            $onConfirm = $status_params['onConfirm'];
//            $onRevert = $status_params['onRevert'];
            $tooltip = $status_params['tooltip'];
            $ok = $status_params['ok'];
            $not_ok = $status_params['not_ok'];
            $intermediate = $status_params['intermediate'];
            
//            if (count($status_params['has_access_errors'])>0)
//            {
//                $tooltip .= '&#013; - ' . implode('&#013; - ', $has_access_errors);
//            }
//            
            $params = array();
            $params['model'] = get_class($model);
            $params['model_id'] = $model->id;
            $params['column'] = $column;
            $params['save'] = $save;
            $params['ok'] = $ok;
            $params['not_ok'] = $not_ok;
            $params['intermediate'] = $intermediate;
            $model->$column;
            Yii::endProfile('part_one',$_trace_cat);

            if (gettype($model->$column) == 'NULL') //ako je stanje null
            {
                Yii::beginProfile('first_if',$_trace_cat);
                //UVEK TRUE JER SE PRETPOSTAVLJA DA JE PROVERA PRILIKOM KLIKA
//                if ($has_access_confirm == true || (!$save && $form_status==='validation_fail')) //ako mozemo da potvrdimo
                {
                    $params['confirm'] = true;
                    $params['revert'] = false;
                    $params['message'] = 'Odaberite potvrdu?';
//                    $params['beforeConfirm'] = $beforeConfirm;
//                    $params['onConfirm'] = $onConfirm;
                    $ret = SIMAHtml::createStatusSpan($model, $tooltip, $intermediate, $pix_size, $column_allow_null, $params, $htmlOptions);
                }
//                else
//                {
//                    $ret = SIMAHtml::createStatusSpan($model, $tooltip, "$intermediate _not_clickable", $pix_size, null, null, $htmlOptions);
//                }
                Yii::endProfile('first_if',$_trace_cat);
            }
            else if ($model->$column) //ako je stanje potvrdjeno
            {
                Yii::beginProfile('second_if',$_trace_cat);
                $timestamp = isset($statuses[$column]['timestamp']) ? $statuses[$column]['timestamp'] : null;
                $user = isset($statuses[$column]['user']['relName']) ? $statuses[$column]['user']['relName'] : null;
                $tooltip = isset($model->$timestamp) ? "$tooltip " . date("d.m.Y. H:i", strtotime($model->$timestamp)) . (isset($model->$user) ? ' - ' . $model->$user->DisplayName : '') : $tooltip;
                //UVEK TRUE JER SE PRETPOSTAVLJA DA JE PROVERA PRILIKOM KLIKA
//                if (($has_access_revert == true) || (!$save && $form_status==='validation_fail'))
//                {
                    $params['confirm'] = false;
                    $params['revert'] = true;
                    $params['message'] = 'Da li zaista zelite da opovrgnete potvrdu?';
//                    $params['onRevert'] = $onRevert;
                    $ret = SIMAHtml::createStatusSpan($model, $tooltip, $ok, $pix_size, $column_allow_null, $params, $htmlOptions);
//                }
//                else
//                {
//                    $ret = SIMAHtml::createStatusSpan($model, $tooltip, "$ok _not_clickable", $pix_size, null, null, $htmlOptions);
//                }
                Yii::endProfile('second_if',$_trace_cat);
            }
            else //inace ako stanje nije potvrdjeno
            {
                Yii::beginProfile('third_if',$_trace_cat);
                //UVEK TRUE JER SE PRETPOSTAVLJA DA JE PROVERA PRILIKOM KLIKA
//                if (($has_access_revert == true && $column_allow_null=='true') || (!$save && $form_status==='validation_fail'))
                if ($column_allow_null=='true')
                {
                    $params['confirm'] = false;
                    $params['revert'] = true;
                    $params['message'] = 'Da li zaista zelite da opovrgnete potvrdu?';
//                    $params['onRevert'] = $onRevert;
                    $ret = SIMAHtml::createStatusSpan($model, $tooltip, $not_ok, $pix_size, $column_allow_null, $params, $htmlOptions);
                }
                else //if ($column_allow_null=='false' || (!$save && $form_status==='validation_fail'))
                {
                    $params['confirm'] = true;
                    $params['revert'] = false;
                    $params['message'] = 'Da li zaista zelite da potvrdite?';
//                    $params['beforeConfirm'] = $beforeConfirm;
//                    $params['onConfirm'] = $onConfirm;
                    $ret = SIMAHtml::createStatusSpan($model, $tooltip, $not_ok, $pix_size, $column_allow_null, $params, $htmlOptions);
                }
//                else
//                {
//                    $ret = SIMAHtml::createStatusSpan($model, $tooltip, $not_ok.' _not_clickable', $pix_size, null, null, $htmlOptions);
//                }
                Yii::endProfile('third_if',$_trace_cat);
            }
        }
        Yii::endProfile('whole',$_trace_cat);
        return $ret;
    }
    
    public static function checkBoxList($model, $column, $params)
    {        
        $elements_array = $params['elements_array'];
        
        $value_data = [];
        
        $html = '<div class="sima-form-check-box-list">';
        foreach($elements_array as $key => $value)
        {            
            $string_val = (string)$key;
            
            $isChecked = false;
            
            $html .= '<div class="sima-form-check-box-list-item"><input type="checkbox" '
                    . 'name="'.$value['display'].'" '
                    . 'value="'.$string_val.'" ';
            
            if(isset($value['isChecked']) && $value['isChecked'] === true)
            {
                $html .= ' checked ';
                $isChecked = true;
            }
            
            $html .= 'onclick="sima.model.checkBoxListClick($(this));">'.$value['display'].'</div>';
            
            $value_data[] = [
                'value' => $string_val,
                'isChecked' => $isChecked,
                'aditional_params' => $value['aditional_params'] ?: []
            ];
        }
        
        $value_data_json = CJSON::encode($value_data);
        
        $base_model_name = get_class($model);
        $name = $base_model_name."[$column]";
        $html .= "<input class='mfslinput' type='hidden' value='$value_data_json' data-initial_value='$value_data_json' name='$name'>";
        $html .= '</div>';

        return $html;
    }
    
    /**
     * pomocna funkcija koja preracunava potrebne parametre za status polja. Koristi se samo u SIMAHTML::status()
     * @param model $model
     * @param string $column
     * @return array
     */
    private static function getStatusParams($model, $column)
    {
        $_trace_cat = 'SIMA.SIMAHtml.getStatusParams';
        Yii::beginProfile('modelSettings',$_trace_cat);
        $statuses = $model->modelSettings('statuses');
        Yii::endProfile('modelSettings',$_trace_cat);
        $tooltip = $statuses[$column]['title'];
        $columns = $model->getMetaData()->tableSchema->columns;
        $column_allow_null = $columns[$column]->allowNull?'true':'false'; //ako moze null onda polje ima tri stanja, inace ima sva stanja
//        $has_access_confirm = false;
//        $has_access_revert = false;
//        $has_access_errors = [];
//        $beforeConfirm = '';
//        $onConfirm = '';
//        $onRevert = '';
        
//        Yii::beginProfile('checkAccess',$_trace_cat);
//        //proveravamo da li korisnik ima pravo na potvrdjivanje
//        if (isset($statuses[$column]['checkAccessConfirm']))
//        {
//            $checkAccessConfirm = $statuses[$column]['checkAccessConfirm'];
//            if (gettype($checkAccessConfirm) == 'boolean')
//            {
//                $has_access_confirm = $checkAccessConfirm;
//            }
//            elseif (method_exists($model,$checkAccessConfirm))
//            {
//                Yii::beginProfile('checkAccessPure',$_trace_cat);
//                $ret = $model->$checkAccessConfirm();
//                Yii::endProfile('checkAccessPure',$_trace_cat);
//                if ($ret === true)
//                {
//                    $has_access_confirm = true;
//                }
//                elseif (gettype($ret)==='array')
//                {
//                    $has_access_errors = array_merge($has_access_errors,$ret);
//                }
//            }
//            else //proverava pravo pristupa
//            {
//                $has_access_confirm = Yii::app()->user->checkAccess($checkAccessConfirm,[],$model);
//            }
//        }
//        else
//        {
//            $has_access_confirm = true;
//        }
//        Yii::endProfile('checkAccess',$_trace_cat);
//        Yii::beginProfile('checkRevert',$_trace_cat);
//        //proveravamo da li korisnik ima pravo na revert
//        if (isset($statuses[$column]['checkAccessRevert']))
//        {
//            $checkAccessRevert = $statuses[$column]['checkAccessRevert'];
//            if (gettype($checkAccessRevert) == 'boolean')
//            {
//                $has_access_revert = $checkAccessRevert;
//            }
//            elseif (method_exists($model,$checkAccessRevert))
//            {
//                $ret = $model->$checkAccessRevert();
//                if ($ret === true)
//                {
//                    $has_access_revert = true;
//                }
//                elseif (gettype($ret)==='array')
//                {
//                    $has_access_errors = array_merge($has_access_errors,$ret);
//                }
//            }
//            else //proverava pravo pristupa
//            {
//                $has_access_revert = Yii::app()->user->checkAccess($checkAccessRevert,[],$model);
//            }
//        }
//        Yii::endProfile('checkRevert',$_trace_cat);
//        if ($has_access_confirm && isset($statuses[$column]['beforeConfirm']))
//        {
//            $beforeConfirm = $statuses[$column]['beforeConfirm'];
//        }
//            
//        if ($has_access_confirm && isset($statuses[$column]['onConfirm']))
//        {
//            $onConfirm = $statuses[$column]['onConfirm'];
//        }
//        
//        if ($has_access_revert && isset($statuses[$column]['onRevert']))
//        {
//            $onRevert = $statuses[$column]['onRevert'];
//        }
//        
        $ok_icon = isset($statuses[$column]['icons']['ok'])?$statuses[$column]['icons']['ok']:'_ok';
        $not_ok_icon = isset($statuses[$column]['icons']['not_ok'])?$statuses[$column]['icons']['not_ok']:'_not-ok';
        $intermediate_icon = isset($statuses[$column]['icons']['intermediate'])?$statuses[$column]['icons']['intermediate']:'_intermediate';
        
        return array(
            'column_allow_null'=>$column_allow_null,
//            'has_access_confirm'=>$has_access_confirm,
//            'has_access_revert'=>$has_access_revert,
//            'has_access_errors'=>$has_access_errors,
//            'beforeConfirm'=>$beforeConfirm,
//            'onConfirm'=>$onConfirm,
//            'onRevert'=>$onRevert,
            'tooltip'=>$tooltip,
            'ok'=>$ok_icon,
            'not_ok'=>$not_ok_icon,
            'intermediate'=>$intermediate_icon
        );
    }
    
    /**
     * pomocna funkcija koja sluzi za kreiranje statusa na osnovu parametara koji joj se proslede. Korisiti se u SIMAHTML::status funkciji
     * @param string $title
     * @param string $class
     * @param string $pix_size
     * @param string $column_allow_null - slucaj kada imamo tri stanja
     * @param array $params - dodatni parametri
     * @return string
     */
    private static function createStatusSpan($model, $title, $class, $pix_size, $column_allow_null=null, $params=null, $htmlOptions=array())
    {
        $_disabled = '';
        $_hidden = '';
        if (isset($htmlOptions['disabled']) && ($htmlOptions['disabled']===true || $htmlOptions['disabled']==='disabled'))
        {
            $_disabled = '_disabled';
        }
        if (isset($htmlOptions['hidden']) && ($htmlOptions['hidden']===true || $htmlOptions['hidden']==='hidden'))
        {
            $_hidden = '_hidden';
        }
        if (isset($htmlOptions['icon_class']))
        {
            $class = $htmlOptions['icon_class'];
        }
        if (isset($htmlOptions['class']))
        {
            $class .= " {$htmlOptions['class']}";
        }
        $html_options_as_string = SIMAMisc::renderHtmlOptionsFromArray($htmlOptions);
        if (isset($column_allow_null) && isset($params) && $params !== null) //ako su setovana ova dva parametra onda je status klikabilan, inace nije
        {
            $params_json = CJSON::encode($params);
            if (isset($params['save']) && !$params['save']) //ako nije save - slucaj kada je status u formi
            {
                $return = "<span data-allow_null='$column_allow_null' class='sima-icon $class _$pix_size $_disabled $_hidden' title='$title' "
                            . " onclick='sima.icons.formStatus($(this),$params_json);' $html_options_as_string></span>";
            }
            else //ako je u pitanju save
            {                
                $return = "<span data-allow_null='$column_allow_null' class='sima-icon $class _$pix_size $_disabled $_hidden' title='$title' "
                            . " onclick='event.stopPropagation(); sima.icons.status($(this),$params_json);' $html_options_as_string></span>";
            }
        }
        else
        {
            $return = "<span class='sima-icon $class  _$pix_size $_disabled $_hidden' title='$title' $html_options_as_string></span>";
        }
        
        //ako nije save, onda kreiramo dodatni input u kome upisemo vrednost statusa
        if (isset($params['save']) && !$params['save'])
        {
            $column = $params['column'];
            $input_name = $params['model'].'['.$column.']';            
            $input_value = ($model->$column===true)?1:(($model->$column===false)?0:'null');
            $return .= "<input class='status_input' type='hidden' name='$input_name' value='$input_value' data-initial_value='$input_value'>";
        }
        
        return $return;
    }
    
    /**
     * ikonica koja izgleda kao delete ali se poziva Cancel.
     * jedina primena je u storniranju racuna
     * MilosS(25.12.2015.):TREBA PREBACITI U ACCOUNTING
     * @param SIMAActiveRecord $model
     * @param mixed $pix_size string/int
     * @return string
     */
    public static function cancelLink($model,$pix_size=16)
    {
        return "<span class='sima-icon _cancel _$pix_size' onclick='sima.icons.cancelLink($(this),\"Da li zaista zelite da stornirate racun?\",\"".  get_class($model)."\",\"".  $model->id."\");'></span>";
    }
    
    /**
     * ikonica za fajl koji je zakljucan
     * @param type $pix_size
     * @return string
     */
    public static function fileLocked($model, $pix_size=16)
    {
        if(empty($model))
        {
            error_log(__METHOD__.' - empty $model - '.SIMAMisc::toTypeAndJsonString($model));
        }
        else if(empty($model->locked))
        {
            error_log(__METHOD__.' - empty $model->locked - '
                    .SIMAMisc::toTypeAndJsonString($model).' - '
                    .SIMAMisc::toTypeAndJsonString($model->locked));
        }
        else if(empty($model->locked->user))
        {
            error_log(__METHOD__.' - empty $model->locked->user - '
                    .SIMAMisc::toTypeAndJsonString($model).' - '.
                    SIMAMisc::toTypeAndJsonString($model->locked).' - '.
                    SIMAMisc::toTypeAndJsonString($model->locked->user));
        }
        
        $disabled = "_disabled";
        if ($model->locked->user->id == Yii::app()->user->id 
                    || $model->responsible_id == Yii::app()->user->id 
                    || Yii::app()->user->checkAccess('unlockFileMasterAdmin'))
        {
            $disabled = '';
        }
        return "<span data-file_id='".$model->id."'"
                . " title='Fajl je zaključao ".$model->locked->user->DisplayName."' "
                . "class='sima-icon _file-locked $disabled _$pix_size' onClick='sima.icons.lockFile($(this),true, ".$model->id.");'></span>";
    }
    
    /**
     * ikonica za fajl koji nije zakljucan
     * @param type $pix_size
     * @return string
     */
    public static function fileUnLocked($model, $pix_size=16)
    {
        return "<span data-file_id='".$model->id."' title='Zaključaj fajl' class='sima-icon _file-unlocked _$pix_size' onClick='sima.icons.lockFile($(this),false, ".$model->id.");'></span>";
    }
    
    /**
     * DEPRACTED - trebalo bi da su sve upotrebe obrisane, treba proveriti
     * @param type $command
     * @param type $pix_size
     * @return type
     */
    public function deleteIcon($command,$pix_size=16)
    {
        return "<span class='sima-icon _delete _$pix_size' onclick='sima.dialog.openYesNo(\"Da li zaista zelite da izbrisete model?\",
            function(){
                ".$command.";
            });'></span>";
    }
    
    public function addIcon($command,$pix_size=16)
    {
        return "<span class='sima-icon _add _$pix_size' onclick='sima.dialog.openYesNo(\"Da li zaista zelite da dodate model?\",
            function(){
                ".$command.";
            });'></span>";
    }


    /**
     * DEPRAC - treba izmeniti postojece linkove - NISU IZBRISANE UPOTREBE!!!
     * @param type $model
     * @param type $pix_size
     * @return type
     */
    public function newTab2Link($model,$pix_size=16)
    {
//        if (SIMAHtml::checkAccess($model, 'View'))
//        {
            return "<img class='button edit'"
                ." onclick='sima.mainTabs.openNewTab(\"".
                $model->path2 ."\",".$model->id.");' src='images/new_tab_$pix_size.png' />";
//        }
//        else
//            return '';
    }
        
    /**
     * Pravi ikonicu koja otvara model u novom tab-u
     * @todo prepraviti putanju u _GET i ovde dodati proveru da li model ima svoj izgled u novom tab-u
     * @param SIMAActiveRecord $model
     * @param integer $pix_size
     * @return html
     */
    public static function modelNewTab($model,$pix_size=16)
    {
//        if (SIMAHtml::checkAccess($model, 'View'))
//        {
            return "<img class='button edit'"
                ." onclick='sima.mainTabs.openNewTab(\"".
                $model->path2 ."\",\"".$model->id."\");' src='images/new_tab_$pix_size.png' />";
//        }
//        else
//            return '';
    }
    
    /**
     * Pravi ikonicu koja otvara model u dialog-u
     * @todo prepraviti putanju u _GET i ovde dodati proveru da li model ima svoj izgled u novom tab-u
     * @param SIMAActiveRecord $model
     * @param integer/string $pix_size
     * @return html
     */
    public static function modelDialog($model,$pix_size=16)
    {        
//            if (gettype($model) !== 'object' || $model === null)
//            {
//                return '';
//            }
////        if (SIMAHtml::checkAccess($model, 'View'))
////        {
//            if (gettype($pix_size)=='string')
//            {
//                return "<span style='cursor: pointer;'"
//                    ." onclick='sima.icons.openModel(\"".
//                    $model->path2 ."\",\"".$model->id."\");'>$pix_size</span>";
//            }
//            else
//                return "<span title='Otvori u dialogu' class='sima-icon _dialog _$pix_size'"
//                    ." onclick='sima.icons.openModel(\"".
//                    $model->path2 ."\",\"".$model->id."\");'></span>";
////        }
////        else
////            return '';
        
        $response = '';
        
        $action = SIMAHtmlButtons::modelDialogWidgetAction($model, $pix_size);

        if(!empty($action))
        {
            if(SIMAMisc::isVueComponentEnabled())
            {
                $action['half'] = true;
                $response = Yii::app()->controller->widget(SIMAButtonVue::class, $action, true);
            }
            else
            {
                $response = Yii::app()->controller->widget('SIMAButton', [
                    'class' => '_half',
                    'action' => $action
                ], true);
            }
        }
        
        return $response;
    }
    
    public static function openActionInDialog($action, $params=[], $html_options = [])
    {
        if (!isset($action))
        {
            return '';
        }
        else
        {   
            $temp_params=json_encode($params);
            $html_options_string = SIMAMisc::renderHtmlOptionsFromArray($html_options);
            $class = 'sima-icon _search_form _16';
            if (isset($html_options['class']))
            {
                $class .= ' ' . $html_options['class'];
            }

            return "<span title='Otvori u dialogu' class='$class' $html_options_string "
                ." onclick='sima.dialog.openActionInDialog(\"". $action ."\",$temp_params);'></span>";
        }
    }

    /**
     * poziva se samo prilikom generisanja notifikacija - treba srediti da bude nepotrebna
     * @param type $model
     * @return type
     */
    public static function newTabURL ($model){
        return Yii::app()->createUrl($model->path2,array('id'=>$model->id));
    }

    /**
     * iscrtava ikonicu za narucivanje skeniranja fajla.
     * ukoliko skeniranje nije naruceno, moze da se naruci, ukoliko jeste, moze da se povuce
     * @param SIMAActiveRecord $model
     * @param mixed $pix_size integer/string
     * @return string html
     */
    public static function orderScan($model,$pix_size=16)
    {
//        if (SIMAHtml::checkAccess($model, 'ScanOrder'))
//        {

        //ako je model objekat onda se skenira jedan, ako nije onda se skeniraju vise odjendnom i model je u ovom slucaju id tabele
        if (is_object($model))
        {
            if ($model->scan_order)
            {
                return "<span title='Povuci skeniranje' class='sima-icon _order-scan-accepted _$pix_size' onclick='sima.icons.orderScan($(this),\"Da li zaista zelite da povucete naruceno skeniranje?\",\"".$model->id."\",false);'></span>";
            }
            else
            {
                return "<span title='Naruči skeniranje' class='sima-icon _order-scan-add _$pix_size' onclick='sima.icons.orderScan($(this),\"Da li zaista zelite da narucite skeniranje?\",\"".$model->id."\",true);'></span>";
            }
        }
        else
        {
            return "<span title='Naruči skeniranje' class='sima-icon _order-scan-add _$pix_size' onclick='sima.icons.orderScan($(this),\"Da li zaista zelite da narucite/povucete selektovana skeniranja?\",\"".$model."\");'></span>";
        }

//        }
//        else
//            return '';
    }

    /**
     * Red u formi koje sluzi za upload fajla - pipljiv kod
     * @param type $form
     * @param SIMAActiveRecord $model
     * @param string $version_column - Lazno polje koje na kraju samo salje 'deleted' ili 'changed'
     * @param string $relation - relacija koja vezuje ka fajlu STRING!!
     * @param bool $repmanager_upload - da li da se vrsi upload u repmanager, false znaci u local temp. default true
     * @throws Exception
     */
    public static function fileField($form_id, $form, $model, $version_column, $relation = 'file', $repmanager_upload = true,$allowed_extansions=array(), $additional_options=[])
    {	
        $file_model = SIMAHtml::fileField_getFileModel($model, $relation);
        
        if(is_null($file_model))
        {
            throw new Exception('nije lepo zadat model u SIMAHtml::fileField');
        }
        
        if($file_model->IsInMovingFromTemp())
        {
            echo Yii::t('FilesModule.File', 'FileIsProcessing');
        }
        else if (!$file_model->isLocked)
        {
            echo SIMAHtml::fileField_uploadField($file_model, $form_id, $form, $model, $version_column, $repmanager_upload, $allowed_extansions, $additional_options);
        }
        else
        {
            echo SIMAHtml::fileLocked($file_model);
        }
    }
    
    private static function fileField_getFileModel($model, $relation)
    {
        $file_model = null;
        
        if(
            gettype($relation)==='string' 
            && isset($model->$relation) 
            && get_class($model->$relation)==='File'
        )
        {
            $file_model = $model->$relation;
        }
        elseif  (get_class($model)==='File' && $model->id!=='')
        {
            $file_model = $model;
        }
        elseif  ($file_model===null )
        {
            $file_model = new File(); //prosledjen model, ali je nov
        }
        
        return $file_model;
    }
    
    private static function fileField_uploadField($file_model, $form_id, $form, $model, $version_column, $repmanager_upload, $allowed_extensions, $additional_options)
    {
        $widget_id = SIMAHtml::uniqid();
        $form_input_data_id = get_class($model).'_'.$version_column;
        $widget_params = [
            'id' => $widget_id,
            'form_input_data_id' => $form_input_data_id,
            'form_id' => $form_id,
            'repmanager_upload' => $repmanager_upload,
            'allowed_extansions' => $allowed_extensions,
            'additional_data' => [
                'prev_fv_id' => $file_model->last_version_id
            ],
            'additional_options' => $additional_options
        ];
        if(isset($file_model->last_version) && $file_model->last_version->canDownload())
        {
            $widget_params['initial_file_display'] = $file_model->DisplayName;
        }
        Yii::app()->controller->widget('SIMAUpload', $widget_params);

        return $form->textField($model,$version_column,array('style'=>'display: none; '));
    }

    /**
     * Widget za biranje datuma i opsega datuma
     * @param string $name
     * @param string $value
     * @param array $options - opcije za ekstenziju
     * @param array $jquery_options - opcije za datepicker jquery plugin(opcije koje se odnose bas na datepicker se zadaju unutar
     * key-a 'datepickerOptions')
     * @return string
     */
    public static function dateRange($name, $value='', $options=[], $jquery_options=[])
    {
        $id = SIMAHtml::uniqid();
        if (isset($options['id']))
        {
            $id = $options['id'];
            unset($options['id']);
        }
        Yii::import('application.extensions.EDateRangePicker.EDateRangePicker');
        return Yii::app()->controller->widget('EDateRangePicker', array_merge(array(
            'id' => $id,
            'name' => $name,
            'value' => $value,
            'language' => Yii::app()->language,
            'options' => array_merge(array(
                'dateFormat' => 'dd.mm.yy.',
                'rangeSplitter' => '<>',
                'rangeStartTitle' => Yii::t('BaseModule.dateRangeField', 'rangeStartTitle'),
                'rangeEndTitle' => Yii::t('BaseModule.dateRangeField', 'rangeEndTitle'),
                'nextLinkText' => Yii::t('BaseModule.dateRangeField', 'nextLinkText'),
                'prevLinkText' => Yii::t('BaseModule.dateRangeField', 'prevLinkText'),
                'doneButtonText' => Yii::t('BaseModule.dateRangeField', 'doneButtonText'),
                'presetRanges' => array(
                    'text1' => Yii::t('BaseModule.dateRangeField', 'presetRanges_text1'),
                    'text2' => Yii::t('BaseModule.dateRangeField', 'presetRanges_text2'),
                    'text3' => Yii::t('BaseModule.dateRangeField', 'presetRanges_text3'),
                    'text4' => Yii::t('BaseModule.dateRangeField', 'presetRanges_text4'),
                    'text5' => Yii::t('BaseModule.dateRangeField', 'presetRanges_text5'),
                ),
                'presets' => array(
                    'specificDate' => Yii::t('BaseModule.dateRangeField', 'specificDate'),
                    'allDatesBefore' => Yii::t('BaseModule.dateRangeField', 'allDatesBefore'),
                    'allDatesAfter' => Yii::t('BaseModule.dateRangeField', 'allDatesAfter'),
                    'dateRange' => Yii::t('BaseModule.dateRangeField', 'dateRange')
                ),
                'datepickerOptions'=>[
                    'changeMonth'=>true,
                    'changeYear'=>true,                
                    'showButtonPanel'=>true,
                ],                
            ),$jquery_options),        
        ),$options), true);
    }
    
    /**
     * Widget za datum i vreme na osnovu modela
     * @param obj $model
     * @param string $column
     * @param array $options - opcije za ekstenziju
     * @param array $jquery_options - opcije za datepicker jquery plugin
     * @return string
     */
    
    public static function callDateWidget($options=[], $jquery_options=[])
    {
        $config_date_parametar = Yii::app()->params['date_format'];
        $config_time_parametar = Yii::app()->params['datetime_format'];
        $date_format = SIMAHtml::formatDateParametar($config_date_parametar);
        $time_format=explode(' ',$config_time_parametar)[1];
        $mode = isset($options['mode'])?$options['mode']:'date';
        Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
        return Yii::app()->controller->widget('CJuiDateTimePicker', array_merge(array(
                    'mode'=>$mode,
                    'language' => Yii::app()->language,                        
                    'options'=>array_merge(array(                            
                        'dateFormat'=>$date_format,
                        'timeFormat'=>$time_format,
                        'changeMonth'=>true,
                        'changeYear'=>true,
                        'showSecond'=>false,
                        'showButtonPanel'=>true,
                        'hourMax' => 24, //mora 24, jer je default u js pluginu 23, ali na slajderu u GUI-u onda ne mozemo da stavimo 23 jer ide do 22
                        'minuteMax' => 60,
                        'secondMax' => 60,
                        'timeOnlyTitle'=> Yii::t('BaseModule.dateTimeField','timeOnlyTitle'),
                        'timeText'=> Yii::t('BaseModule.dateTimeField','timeText'),
                        'hourText'=> Yii::t('BaseModule.dateTimeField','hourText'),
                        'minuteText'=> Yii::t('BaseModule.dateTimeField','minuteText'),
                        'secondText'=> Yii::t('BaseModule.dateTimeField','secondText'),
                        'millisecText'=> Yii::t('BaseModule.dateTimeField','milisecText'),
                        'timezoneText'=> Yii::t('BaseModule.dateTimeField','timezoneText'),
                        'currentText'=> Yii::t('BaseModule.dateTimeField','currentText'),
                        'closeText'=> Yii::t('BaseModule.dateTimeField','closeText'),
                        'firstDay' => Yii::t('BaseModule.dateTimeField','firstDay')
                    ), $jquery_options)
            ), $options), true
        );

    }
    
    public static function formatDateParametar($date_format)
    {

        if (strpos($date_format, 'dd') === false) 
        {
            throw new Exception(__METHOD__.'Invalid date format: missing day segment.');
            //throw exception
        }
        if (strpos($date_format, 'mm') === false)
        {
            throw new Exception(__METHOD__.'Invalid date format: missing month segment.');
            // throw exception
        }
        if (strpos($date_format,'yyyy') !== false)
        {
            $year_start_position=strpos($date_format,'yyyy');
            $date_format=substr($date_format,0,$year_start_position).substr($date_format,$year_start_position+2);
        }
        else if (strpos($date_format,'yy') !==false )
        {
            $date_format=$date_format;
        }
        else
        {
            throw new Exception(__METHOD__.'Invalid date format: missing year segment.');
            //throw exception
        }
        
        return $date_format;
    }
    
    public static function datetimeField($model, $column, $options=[], $jquery_options=[])
    {
        $options['model']=$model;
        $options['attribute']=$column;
        return SIMAHtml::callDateWidget($options, $jquery_options);
    }
    
   /**
    * Widget za datum i vreme na osnovu atributa
    * @param string $name
    * @param string $value
    * @param array $options - opcije za ekstenziju
    * @param array $jquery_options - opcije za datepicker jquery plugin
    * @return string
    */
    public static function datetimeFieldByName($name, $value='', $options=[], $jquery_options=[])
    {
        $options['name']=$name;
        $options['value']=$value;
        return SIMAHtml::callDateWidget($options, $jquery_options);
    }
   
    /**
     * vraca ikonicu za download fajla ili fajl verzije
     * @param Model/integer $model
     * @param integer $pix_size
     * @return string
     */
    public static function fileDownload($model,$pix_size = 16)
    {
        if($model!=null)
        {
            $file_id = 'NULL';
            $file_version_id = 'NULL';
            if (gettype($model)==='integer')
            {
                $file = File::model()->with('last_version')->findByPk($model);
                if ($file !== null && isset($file->last_version) && $file->last_version->canDownload())
                {
                    $file_id = $file->id;
                }
                else
                {
                    return '';
                }
            }
            else if (gettype($model)==='object')
            {
                if (get_class($model)==='FileVersion')
                {
                    if ($model->canDownload())
                    {
                        $file_version_id = $model->id;
                    }
                    else
                    {
                        return '';
                    }
                }
                elseif (get_class($model)==='File' && isset($model->last_version) && $model->last_version->canDownload())
                {
                    $file_id = $model->id;
                }
                elseif (isset($model->file) && get_class($model->file)==='File' && isset($model->file->last_version) && $model->file->last_version->canDownload())
                {
                    $file_id = $model->file->id;               
                }
                else
                {
                    return '';
                }
            }
            else
            {
                return '';
            }
            
            return "<span class='sima-icon _download _$pix_size' "
                    . "onclick='sima.icons.fileDownload(\"$file_id\", \"$file_version_id\");'"
                    . "></span>";
            
        }
        else
        {
            return '';
        }
    }
    
    /**
     * prikazuje download ikonicu za FileVersion ukoliko je ta verzija download-abilna
     * @todo proveru treba prebaciti u files/transfer kontroler
     * @param type $id
     * @param type $pix_size
     * @return string
     */
    //// MILOSJ: zakomentarisano jer nije nadjeno da se igde koristi
//    public static function showFileVersion($id,$pix_size = 16)
//    {
//        $file = FileVersion::model()->findByPk($id);
//        if ($file!=null && !$file->isDeleted() && $file->file_size!=0)
//                return CHtml::link("<img class='button edit' src='images/down_$pix_size.png' />",
//                        array('/files/transfer/fileVersionDownload','file_version_id'=>$id),
//                        array('target'=>'_blank','class'=>'new_tab'));
//        else return '';
//    }

    /**
     * dugme za Submitt
     * @param type $model
     * @param type $model_id
     * @param type $formName
     * @param type $refresh
     * @return type
     */
    public function modelFormSubmitButton($form_id)
    {
        $uniq = SIMAHtml::uniqid();

        $response = <<<AAA
        
        <span id="~~UNIQ~~" class="sima-model-form-button submit_form" type="submit" name="yt0" value="~~TITLE~~">~~TITLE~~</span>
          
        <script type="text/javascript">
                $("#~~UNIQ~~").on("click",function(){
                    sima.icons.submitForm('~~FORM_ID~~');
                });
        </script>
AAA;
        $response = preg_replace("/\~\~TITLE\~\~/", Yii::t('SIMAHtml', 'Save'), $response);
        $response = preg_replace("/\~\~UNIQ\~\~/", $uniq, $response);
        $response = preg_replace("/\~\~FORM_ID\~\~/", $form_id, $response);
        return $response;
    }
    
    /**
     * Prikazuje ikonicu za otvaranje forme sa opcijama prema parametrima.
     * primer za novi model<br />
     * <pre>
     *      SIMAHtml::modelFormOpen(Bill::model(), array(
     *              'init_data'=>array(         //inicijalni podaci koji treba da se popune u formi
     *                  'Model'=>array(
     *                      'partner_id' => $this->partner_id,
     *                      'amount' => $this->amount,
     *                      'advance_id' => $this->id,
     *                      'company_sector_id'=>array(  //polje za koje se stavlja filter u pretrazi
     *                          'disabled', //ako hocemo da polje bude disable
     *                          'model_filter'=>array(
     *                              'company_id'=>$company->id //filter koji se primenjuje
     *                          )
     *                      )
     *                  )
     *              )
     *              'updateModelViews'=>SIMAHtml::getTag($employee). //posle zatvaranja forme treba osveziti tag
     *              'formName'=>$formName, //zadavanje imena forme
     *              'append_object'=>array(".${table_id}_list.sima-guitable",'type'=>'GuiTable'),  dodavanje na neki objekat
     *      ))
     * </pre>
     * primer za postojeci model<br />
     * <pre>
     *      SIMAHtml::modelFormOpen($model)
     * </pre>
     * @param SIMAActiveRecord $model
     * @param array $params
     * @return string html
     */
    public static function modelFormOpen($model,$params=array(),$px_size=16)
    {
//        $response = '';
//        
//        if (SIMAHtml::checkAccess($model, 'FormOpen'))
//        {
//            $Model = get_class($model);
//            if (isset($model->id))
//            {
//                $model_id = $model->id;
//                $button_img = 'edit';
//            }
//            else
//            {
//                $add_obj = array();
//                if (isset($params['append_object'])) 
//                    
//                    //u ovom slucaju dodajemo u odredjeni jQuery objekat i to odredjeni pogled
//                    if (gettype($params['append_object'])==='array' && count($params['append_object'])>0)      
//                    {    
//                        $add_obj['append_object'] = array_shift($params['append_object']);
//                        $add_obj += $params['append_object'];
//                    }
//                    //u ovom slucaju samo dodajemo u odredjeni jQuery objekat
//                    elseif (gettype($params['append_object'])==='string') 
//                        $add_obj['append_object'] = $params['append_object'];
//
//                $model_id= CJSON::encode($add_obj);
//                $button_img = 'add';
//            }
//            if (isset($params['model_id']))
//            {
//                $model_id = $params['model_id'];
//            }
//            if (isset($params['button_img']))
//            {
//                $button_img = $params['button_img'];
//            }
//            
//            $hidden = '';
//            if (isset($params['hidden']) && $params['hidden'] === true)
//            {
//                $hidden = 'hidden';
//            }
//            
//            $buttonTitle = (isset($params['button_title']))?$params['button_title']:'Otvori formu';
//            $formName = (isset($params['formName']))?$params['formName']:'default';
//            $scenario = (isset($params['scenario']))?$params['scenario']:'';
//
//            $init_data = isset($params['init_data'])?$params['init_data']:array();
//            $init_data = CJSON::encode($init_data);
//            $status = isset($params['status'])?$params['status']:'initial';
//            
//            if (isset($params['updateModelViews']) && gettype($params['updateModelViews'])=='string')
//                $updateModelViews = $params['updateModelViews'];
//            else
//                $updateModelViews = '';
//
//            if (isset($params['onSave']) && gettype($params['onSave'])=='string')
//                $onSave = $params['onSave'];
//            else
//                $onSave = 'function(){}';
//            
//            $response = <<<AAA
//            <span title="~~BUTTON~TITLE~~" class="sima-icon _~~BUTTON~IMAGE~~ _~~IMAGE~SIZE~~ _~~HIDDEN~~" 
//                onclick='sima.icons.form($(this),"~~MODEL~~", ~~MODEL~ID~~, {
//                    init_data: ~~DATA~~,
//                    updateModelViews: "~~UPDATE~MODEL~VIEWS~~",
//                    formName: "~~FORM~NAME~~",
//                    scenario: "~~SCENARIO~~",
//                    onSave: ~~ON~SAVE~~,
//                    status: "~~STATUS~~"
//                });'/>
//AAA;
//            $response = preg_replace("/\~\~MODEL\~ID\~\~/", $model_id, $response);
//            $response = preg_replace("/\~\~MODEL\~\~/", $Model, $response);
//            $response = preg_replace("/\~\~DATA\~\~/", $init_data, $response);
//            $response = preg_replace("/\~\~FORM\~NAME\~\~/", $formName, $response);
//            $response = preg_replace("/\~\~SCENARIO\~\~/", $scenario, $response);
//            $response = preg_replace("/\~\~IMAGE\~SIZE\~\~/", $px_size, $response);
//            $response = preg_replace("/\~\~BUTTON\~IMAGE\~\~/", $button_img, $response);
//            $response = preg_replace("/\~\~UPDATE\~MODEL\~VIEWS\~\~/", $updateModelViews, $response);
//            $response = preg_replace("/\~\~ON\~SAVE\~\~/", $onSave, $response);
//            $response = preg_replace("/\~\~STATUS\~\~/", $status, $response);
//            $response = preg_replace("/\~\~HIDDEN\~\~/", $hidden, $response);
//            $response = preg_replace("/\~\~BUTTON\~TITLE\~\~/", $buttonTitle, $response);
//        }
//        
//        return $response;
        
        $response = '';
        
        if (SIMAHtml::checkAccess($model, 'FormOpen'))
        {
            $action = SIMAHtmlButtons::modelFormOpenWidgetAction($model, $params, $px_size);

            if(!empty($action))
            {
                $hidden = '';
                if (isset($params['hidden']) && $params['hidden'] === true)
                {
                    $hidden = '_hidden';
                }
                
                $class = isset($params['class']) ? $params['class'] : '';

                if(SIMAMisc::isVueComponentEnabled())
                {
                    $action['half'] = true;
                    $response = Yii::app()->controller->widget(SIMAButtonVue::class, $action, true);
                }
                else
                {
                    $response = Yii::app()->controller->widget('SIMAButton', [
                        'class' => "_half $hidden $class",
                        'action' => $action
                    ], true);
                }
            }
        }
        
        return $response;
    }
        
    /**
     * prikazuje ikonicu za brisanje modela.
     * moguce je obrisati i n2n veze izmedju dve tabele (PK je dva polja)
     * @param SIMAActiveRecord $model model koji se brise
     * @param string $yes_function javascript kod koji se ozvrsi posle klika na yes
     * @param mixed $pix_size velicina ikonice
     * @return string html
     */
    public static function modelDelete($model,$yes_function='',$pix_size=16)
    {
//        if (SIMAHtml::checkAccess($model, 'Delete'))
//        {
//            $pk = $model->getPrimaryKey();
//            if (gettype($pk)==='array')
//            {
//                $new_pk = '{';
//                foreach ($pk as $key => $value) {
//                   $new_pk .= "$key:$value,";
//                }
//                $new_pk[strlen($new_pk)-1]='}';
//                $pk = $new_pk;
//            }            
//            return "<span title='Obriši' class='sima-icon _delete _$pix_size' onclick='sima.icons.remove($(this),\"".get_class($model)."\",$pk, \"$yes_function\");'></span>";
//        }
//        else
//        {
//            return '';
//        }
        
        $response = '';
        
        if (SIMAHtml::checkAccess($model, 'Delete'))
        {
            $action = SIMAHtmlButtons::modelDeleteWidgetAction($model, $yes_function, $pix_size);

            if(!empty($action))
            {
                if(SIMAMisc::isVueComponentEnabled())
                {
                    $action['half'] = true;
                    $response = Yii::app()->controller->widget(SIMAButtonVue::class, $action, true);
                }
                else
                {
                    $response = Yii::app()->controller->widget('SIMAButton', [
                        'class' => '_half',
                        'action' => $action
                    ], true);
                }
            }
        }
        
        return $response;
    }
    
    public static function modelDeleteForever($model,$yes_function='',$pix_size=16)
    {
//        if (SIMAHtml::checkAccess($model, 'Delete'))
//        {
            $pk = $model->getPrimaryKey();
            if (gettype($pk)==='array')
            {
                $new_pk = '{';
                foreach ($pk as $key => $value) {
                   $new_pk .= "$key:$value,";
                }
                $new_pk[strlen($new_pk)-1]='}';
                $pk = $new_pk;
            }            
            return "<span title='Obriši zauvek' class='sima-icon _delete-forever _$pix_size' onclick='sima.icons.deleteforever($(this),\"".get_class($model)."\",$pk, \"$yes_function\");'></span>";
//        }
//        else
//            return '';
    }
    
    public static function modelRestoreFromTrash($model,$yes_function='',$pix_size=16)
    {
        $pk = $model->getPrimaryKey();
        if (gettype($pk)==='array')
        {
            $new_pk = '{';
            foreach ($pk as $key => $value) {
               $new_pk .= "$key:$value,";
            }
            $new_pk[strlen($new_pk)-1]='}';
            $pk = $new_pk;
        }            
        return "<span title='Vrati iz kante' class='sima-icon _restore-from-trash _$pix_size' onclick='sima.icons.restore_from_trash($(this),\"".get_class($model)."\",$pk, \"$yes_function\");'></span>";
    }
    
    /**
     * prikazuje ikonicu za recikliranje modela.
     * primeri su brisanje fajla i otkazivanje 
     * @param SIMAActiveRecord $model model koji se brise
     * @param string $yes_function javascript kod koji se ozvrsi posle klika na yes
     * @param mixed $pix_size velicina ikonice
     * @return string html
     */
    public static function modelRecycle($model,$pix_size=16)
    {
//        if (SIMAHtml::checkAccess($model, 'Delete'))
//        {          
            return "<span title='Recikliraj' class='sima-icon _recycle _$pix_size' onclick='sima.icons.recycle($(this),\"".get_class($model)."\",$model->id);'></span>";
//        }
//        else
//            return '';
    }
    
    public static function foreignKeyDelete($model, $fk_model, $column, $pix_size=16)
    {
        $pk = $model->getPrimaryKey();
        $fk = $fk_model->getPrimaryKey();
        return "<img class='button delete' onclick='sima.model.removeForeignKey(\"".get_class($model)."\",$pk,\"".get_class($fk_model)."\",$fk,\"$column\")' src='images/delete_$pix_size.png' />";
       
    }
     
    /**
     * iscrtava ikonicu koja osvezava odredjeni model
     * @param SIMAActiveRecord $model
     * @param mixed $pix_size velicina ikonice - trenutno ima samo 16px
     * @return string html
     */
    public static function modelRefresh($model, $pix_size=16)
    {
//        if (SIMAHtml::checkAccess($model, 'Refresh'))
//        {
            return "<span title='Refrešuj' class='sima-icon _refresh _$pix_size' onclick='sima.icons.refresh($(this),\"".SIMAHtml::getTag($model)."\");'></span>";
//        }
//        else
//            return '';
    }
      
    /**
     * DEPRAC????
     * @param type $title
     * @param type $link
     * @return type
     */
    public function ajaxSubmitButton($title='Sačuvaj',$link='')
    {
        return CHtml::ajaxSubmitButton($title,$link,
            array('success'=>'function(data){simaModel.ajaxSubmitRet(data);}'),
            array('id'=>SIMAHtml::uniqid()));
    }

    /**
     * DEPRAC?????
     * @param type $options
     * @return type
     */
    public function createDialog($options=array())
    {
            $local_dialog_id = SIMAHtml::uniqid();
            $this->beginWidget('zii.widgets.jui.CJuiDialog', array(
                'id'=>$local_dialog_id,
                'options'=>array(
                    'title'=>'SIMA',
                    'autoOpen'=>false,
                    'modal'=>true,
                    'width'=>'auto',
                    'height'=>'auto',

                )+$options,
            ));
            $parent_dialog_id = (isset($options['parent_dialog_id']))?$options['parent_dialog_id']:'';
            echo "<div id='divFor$local_dialog_id' class='child_of_$parent_dialog_id' dialog_id='$local_dialog_id'></div>";
            $this->endWidget();
            return $local_dialog_id;
    }

    /**
     * funkcija vraca random token
     * @param type $size
     * @return string
     */
    public static function createRandomToken($size)
    {
            $chars = "abcdefghijkmnopqrstuvwxyz0123456789";
            srand((double)microtime()*1000000);
            $i = 0;
            $token = '' ;
            while ($i < $size) {
                    $num = rand() % 35;
                    $tmp = $chars[$num];
                    $token .= $tmp;
                    $i++;
            }
            // 		echo strlen($token);
            return $token;
    }
    
    /**
     * funkcija vraca uniqid()
     * @return string
     */
    public static function uniqid()
    {
       return 'sima'.mt_rand();
    }
    
    /**
     * Funkcija koja skida kukice sa srpskih slova i rec pretvara sve u mala slova
     * @param string $input
     */
    public static function removeSerbianLettersAndLower($input)
    {
        $input_lower = strtolower($input);
        $output = '';

        for ($i = 0; $i < strlen($input_lower); $i++)
        {
            if (isset($input_lower[$i + 1]) && in_array($input_lower[$i] . $input_lower[$i + 1], array('č', 'ć', 'š', 'ž', 'đ', 'Č', 'Ć', 'Š', 'Ž', 'Đ')))
            {
                if ($input_lower[$i] . $input_lower[$i + 1] == 'č')
                    $output.='c';
                elseif ($input_lower[$i] . $input_lower[$i + 1] == 'ć')
                    $output.='c';
                elseif ($input_lower[$i] . $input_lower[$i + 1] == 'š')
                    $output.='s';
                elseif ($input_lower[$i] . $input_lower[$i + 1] == 'ž')
                    $output.='z';
                elseif ($input_lower[$i] . $input_lower[$i + 1] == 'đ')
                    $output.='dj';
                elseif ($input_lower[$i] . $input_lower[$i + 1] == 'Č')
                    $output.='c';
                elseif ($input_lower[$i] . $input_lower[$i + 1] == 'Ć')
                    $output.='c';
                elseif ($input_lower[$i] . $input_lower[$i + 1] == 'Š')
                    $output.='s';
                elseif ($input_lower[$i] . $input_lower[$i + 1] == 'Ž')
                    $output.='z';
                elseif ($input_lower[$i] . $input_lower[$i + 1] == 'Đ')
                    $output.='dj';
                $i++;
            }
            elseif (ctype_alpha($input_lower[$i]))
                $output.=$input_lower[$i];
        }
        
        return $output;
    }
    /**
     * Funkcija koja skida kukice sa srpskih slova i rec pretvara sve u mala slova
     * @param string $input
     */
    public static function removeSerbianLettersAndUpper($input)
    {
        $input_lower = strtoupper($input);
        $output = '';

        for ($i = 0; $i < strlen($input_lower); $i++)
        {
            if (isset($input_lower[$i + 1]) && in_array($input_lower[$i] . $input_lower[$i + 1], array('č', 'ć', 'š', 'ž', 'đ', 'Č', 'Ć', 'Š', 'Ž', 'Đ')))
            {
                if ($input_lower[$i] . $input_lower[$i + 1] == 'č')
                    $output.='C';
                elseif ($input_lower[$i] . $input_lower[$i + 1] == 'ć')
                    $output.='C';
                elseif ($input_lower[$i] . $input_lower[$i + 1] == 'š')
                    $output.='S';
                elseif ($input_lower[$i] . $input_lower[$i + 1] == 'ž')
                    $output.='Z';
                elseif ($input_lower[$i] . $input_lower[$i + 1] == 'đ')
                    $output.='DJ';
                elseif ($input_lower[$i] . $input_lower[$i + 1] == 'Č')
                    $output.='C';
                elseif ($input_lower[$i] . $input_lower[$i + 1] == 'Ć')
                    $output.='C';
                elseif ($input_lower[$i] . $input_lower[$i + 1] == 'Š')
                    $output.='S';
                elseif ($input_lower[$i] . $input_lower[$i + 1] == 'Ž')
                    $output.='Z';
                elseif ($input_lower[$i] . $input_lower[$i + 1] == 'Đ')
                    $output.='DJ';
                $i++;
            }
            elseif (ctype_alpha($input_lower[$i]))
                $output.=$input_lower[$i];
            else
                $output.=$input[$i];
        }
        
        return $output;
    }

    /**
     * pretvara broj u cirilican tekst 
     * @param type $broj
     * @return string
     */
    static function money2letters($broj,$alfabet = 'cyr')
    {
        if (!is_numeric($broj) || is_nan($broj))
        {
            return 'NaN';
        }
        if ($alfabet=='cyr')
        {
            $sZeroValue = 'нула динара';
            $StotiFormat = ' и %2.0f/100';
            $Jedinice[0] = array('', 'једна', 'две', 'три', 'четири', 'пет', 'шест', 'седам', 'осам', 'девет', 'десет',
                'једанаест', 'дванаест', 'тринаест', 'четрнаест', 'петнаест', 'шеснаест', 'седамнаест', 'осамнаест', 'деветнаест');
            $Jedinice[1] = array('', 'један', 'два', 'три', 'четири', 'пет', 'шест', 'седам', 'осам', 'девет', 'десет',
                'једанаест', 'дванаест', 'тринаест', 'четрнаест', 'петнаест', 'шеснаест', 'седамнаест', 'осамнаест', 'деветнаест');
            $Desetice = array('', '', 'двадесет', 'тридесет', 'четрдесет', 'педесет', 'шесдесет', 'седамдесет', 'осамдесет', 'деведесет');
            $Stotine = array('', 'сто', 'двеста', 'триста', 'четристо', 'петсто', 'шесто', 'седамсто', 'осамсто', 'деветсто');
//            $Prilozi[1] = array("-1" => ' динар', 'а', '', 'а', 'а', 'а', 'а');
            $Prilozi[1] = array("-1" => '', '', '', '', '', '', '');
            $Prilozi[2] = array("-1" => 'хиљад', 'a', 'a', 'е', 'е', 'е', 'a');
            $Prilozi[3] = array("-1" => 'милион', 'a', '', 'a', 'a', 'a', 'a');
            $Prilozi[4] = array("-1" => 'милијард', 'и', 'a', 'e', 'e', 'e', 'и');
            $SignPrefix = array('', 'минус ');
            
            $tooBig = 'Превелик број!';
            $ending = '/100 пара';
        }
        else
        {
            $sZeroValue = 'nula dinara';
            $StotiFormat = ' i %2.0f/100';
            $Jedinice[0] = array('', 'jedna', 'dve', 'tri', 'četiri', 'pet', 'šest', 'sedam', 'osam', 'devet',
                'deset', 'jedanaest', 'dvanaest', 'trinaest', 'četrnaest', 'petnaest', 'šesnaest', 'sedamnaest', 'osamnaest', 'devetnaest');
            $Jedinice[1] = array('', 'jedan', 'dva', 'tri', 'četiri', 'pet', 'šest', 'sedam', 'osam', 'devet',
                'deset', 'jedanaest', 'dvanaest', 'trinaest', 'četrnaest', 'petnaest', 'šesnaest', 'sedamnaest', 'osamnaest', 'devetnaest');
            $Desetice = array('', '', 'dvadeset', 'trideset', 'četrdeset', 'pedeset', 'šezdeset', 'sedamdeset', 'osamdeset', 'devedeset');
            $Stotine = array('', 'sto', 'dvesta', 'trista', 'četirsto', 'petsto', 'šesto', 'sedamsto', 'osamsto', 'devetsto');
//            $Prilozi[1] = array("-1" => ' dinar', 'a', '', 'a', 'a', 'a', 'a');
            $Prilozi[1] = array("-1" => '', '', '', '', '', '', '');
            $Prilozi[2] = array("-1" => 'hiljad', 'a', 'a', 'e', 'e', 'e', 'a');
            $Prilozi[3] = array("-1" => 'milion', 'a', '', 'a', 'a', 'a', 'a');
            $Prilozi[4] = array("-1" => 'milijard', 'i', 'a', 'e', 'e', 'e', 'i');
            $SignPrefix = array('', 'minus ');
            
            $tooBig = 'Prevelik broj!';
            $ending = '/100 dinara';
        }
        

        if ($broj >= 1000000000000)
        {//1 000 000 000 000
            return $tooBig;
        }
        else
        {

            $eCeo = abs(floor($broj));
            $eStoti = substr(number_format($broj, 2), -2);
            if (!$broj || !$eCeo)
                return $sZeroValue;
            $res = '';
            $s = number_format($eCeo, 0, '.', '');
            while ((strlen($s) % 3) != 0)
            {
                $s = '0' . $s;
            };
            for ($x = 1; $x <= 4; $x++)
            {
                $trojka = substr($s, strlen($s) - 3, 3);
                $s = substr($s, 0, strlen($s) - 3);
                if ($trojka == '')
                    $trojka = '000';
                if ($trojka != '000')
                {
                    if (($trojka[1] == '0') || ($trojka[1] == '1'))
                        $temp = $Jedinice[$x % 2][($trojka[1] . $trojka[2]) * 1];
                    else
                        $temp = $Desetice[$trojka[1]] . $Jedinice[$x % 2][$trojka[2] * 1];
                    $temp = $Stotine[$trojka[0]] . $temp;
                    if ((1 * $trojka[1] . $trojka[2]) <= 5)
                        $temp.=$Prilozi[$x][-1] . $Prilozi[$x][1 * ($trojka[1] . $trojka[2])];
                    else
                        $temp.=$Prilozi[$x][-1] . $Prilozi[$x][5];
                    $res = $temp . $res;
                }
                if ($x == 1 && $eStoti == 0)
                {
                    if((1 * $trojka[1] . $trojka[2]) != 11 && (1 * $trojka[2]) == 1)
                    {
                        $res .= $alfabet=='cyr' ? ' динар' : ' dinar';
                    }
                    else 
                    {
                        $res .= $alfabet=='cyr' ? ' динара' : ' dinarа';
                    }
                }
            };
            if ($eStoti == 0)
            {
                return $res;
            }
            else
            {
                return $res . ' i ' . $eStoti . $ending;
            }
        }
    }
    
    /**
     * vraca span koji ima proslejene tekstove kao display i hover
     * @param type $spanText - tekst koji ce biti prikazan u span-u
     * @param type $hoverText - tekst koji ce biti prikazan kao hover
     * @return string - span sa tekstom i hoverom
     */
    public static function spanWithHover($spanText, $hoverText)
    {
        $result = '<span title="'.$hoverText.'">'.$spanText.'</span>';
        
        return $result;
    }
    
    /**
     * 
     * @param type $spanText
     * @param type $model
     * @param type $view
     * @return type
     */
    public static function spanWithModelInfoBox($spanText, $model, $view)
    {
        return self::spanWithInfoBox($spanText, 'base/model/view', [
            'model' => get_class($model),
            'model_id' => $model->id,
            'model_view' => $view
        ]);
    }
    
    public static function spanWithInfoBox($spanText, $action, $get_params=[], $post_params=[])
    {
        $uniq = SIMAHtml::uniqid();
        $encoded_get_params = CJSON::encode($get_params);
        $encoded_post_params = CJSON::encode($post_params);
        $result = "<span id='$uniq' class='sima-display-html sima-span-with-info' data-global_timeout='null'"
                . "data-action='$action' data-get_params='$encoded_get_params' data-post_params='$encoded_post_params'>$spanText</span>";
        
        $script = "$(\"<div id='info_box$uniq' class='sima-span-info-box'></div>\").simaPopup('init',$('body'),$('#$uniq'));";
        Yii::app()->clientScript->registerScript("script_registration$uniq",$script, CClientScript::POS_END);

        return $result;
    }
    
    public static function spanWithStaticInfoBox($spanText, $infoBoxHtml, $spanClasses='', $infoBoxClasses='')
    {
        $uniq = SIMAHtml::uniqid();
        $spanText_id = $uniq.'_span_text_id';
        $spanText_popup_id = $uniq.'_span_text_popup_id';
        $result = "<span id='$spanText_id' class='$spanClasses'>$spanText</span>"
                ."<div id='$spanText_popup_id' class='info_box $infoBoxClasses'>$infoBoxHtml</div>";
        $script = "sima.misc.initStaticInfoBox(\"$spanText_id\", \"$spanText_popup_id\");";
        Yii::app()->clientScript->registerScript($uniq,$script, CClientScript::POS_END);   

        return $result;
    }

    /**
     * nicemu ne sluzi
     */
    function ModelTreeView()
    {
            $this->TreeView(array('ATreeModel','model'=>get_class($Model)));
    }

    /** 
     * ovo je verovatno vezano za set.rs
     * SIMAHtml::link() metoda koja se koristi u uredjivanju vesti. Pravi link od teksta.
     */
    public function link($text, $url, $htmlOptions=array ( ))
    {
        if (isset($_GET['lang']))
        {
            if (gettype($url)=='array') $url['lang']=$_GET['lang'];
            if (gettype($url)=='string') $url.='/'.$_GET['lang'];
        }
        return CHtml::link($text,$url,$htmlOptions);
    }

    /**
     * Ispisuje cifru za pare
     * @param numeric $number iznos
     * @param string $currency valute, podrazumeva se din
     * @return string html
     */
    public static function money_format($number,$currency='din')
    {
        return SIMAHtml::number_format($number).' '.$currency;
    }
    
    public static function payment_export_number_format($number)
    {
        return number_format ($number, 2, '.', '');
    }
    
    /**
     * Ispisuje brojeve prema stavkama
     * ako je tipa string i NE prodje is_numeric, samo se vraca
     * @param numeric $number iznos
     * @param integer $precision valute, podrazumeva se din
     * @param boolean $html_hover default FALSE, da li da se okruzi u span i stavi puna vrednost
     * @return string html
     */
    public static function number_format($number, $precision=2, $html_hover = false)
    {
        if  (is_string($number) && !empty($number))
        {
            if (is_numeric($number)) //stringove samo vracamo
            {
                $number = floatval($number);
            }
            else
            {
                return $number;
            }
        }
        if  (!is_numeric($number) && !empty($number))
        {
            return 'NaN';
        }
        
        $amount_field_thousands = null;
        $amount_field_decimals = null;
        if(Yii::app()->isWebApplication())
        {
            $amount_field_thousands = Yii::app()->user->model->confParamValue('base.amount_field_thousands');
            $amount_field_decimals = Yii::app()->user->model->confParamValue('base.amount_field_decimals');
        }
        else
        {
            $amount_field_thousands = Yii::app()->configManager->get('base.amount_field_thousands');
            $amount_field_decimals = Yii::app()->configManager->get('base.amount_field_decimals');
        }
        
        $thousend_separator = ($amount_field_thousands=='space')?' ':
                (($amount_field_decimals=='.')?',':'.');
        
        $ret_string = number_format ($number, $precision,$amount_field_decimals, $thousend_separator);
        //ako se ovo odkomentarise, ne radi dupli klik
//        if ($html_hover && !SIMAMisc::areEqual($number*100.0,floatval(intval($number*100)),0.01) )
//        {
//            $title_string = trim( number_format ($number, 10,$amount_field_decimals, $thousend_separator),'0');
//            return "<span onclick='$(this).parent()' title='$title_string'>$ret_string*</span>";
//        }
        return $ret_string;
    }
    
    public static function checkAccess($model, $type)
    {
//        if (is_object($model) && get_class($model) == 'File')
//        {
//                if (Yii::app()->user->checkAccess('model'.get_class($model).$type, array('file' => $model)))
//                    return true;
//                else 
//                    return false;
//        }
//        else
////        {
            if (Yii::app()->user->checkAccess($type,[],$model))
            {
                return true;
            }
            elseif (!isset(Yii::app()->params['modelsAccess'][get_class($model)]))
            {
                return true;
            }
            elseif 
                (
                    !in_array('model'.get_class($model).$type, Yii::app()->params['modelsAccess'][get_class($model)])
                    &&
                    !isset(Yii::app()->params['modelsAccess'][get_class($model)][('model'.get_class($model).$type)])
                )
            {
                return true;
            }
            else
            {
                return false;
            }
//        }
        
    }
    
    public static function modelIconsContracted($model, $px_size=16, $exclude_icons=array(), $additional_classes='')
    {        
        $_trace_cat = 'SIMA.Html.modelIconsContracted';
        Yii::beginProfile('part1',$_trace_cat);
        $subactions = [];
        foreach ($model->getModelOptions() as $option_pack)
        {
            $current_model = $model;
            if (is_array($option_pack))
            {
                $option = $option_pack[0];
                $option_params = array_slice($option_pack, 1, 1);
                if (isset($option_params['model']))
                {
                    $MODEL = $option_params['model'];
                    if (isset($option_params['model_id']))
                    {
                        $MODEL_ID = $option_params['model_id'];
                    }
                    else
                    {
                        $MODEL_ID = $model->id;
                    }
                    $current_model = $MODEL::model()->findByPk($MODEL_ID);
                }
            }
            else
            {
                $option = $option_pack;
            }
            
            if (in_array($option, $exclude_icons))
            {
                continue;
            }
            
            $subaction = [];
                
            Yii::beginProfile('part1.'.$option,$_trace_cat);
            switch ($option)
            {
                case 'form':
                    $subaction = SIMAHtmlButtons::modelFormOpenWidgetAction($current_model, array(), $px_size);
                    break;
                case 'delete':	
                    $subaction = SIMAHtmlButtons::modelDeleteWidgetAction($current_model,'', $px_size);
                    break;
                case 'delete_forever':	
                    $subaction = SIMAHtmlButtons::modelDeleteForeverWidgetAction($current_model,'', $px_size);
                    break;
                case 'recycle':	
                    $subaction = SIMAHtmlButtons::modelRecycleWidgetAction($current_model, $px_size);
                    break;
                case 'cancel':	
                    $subaction = SIMAHtmlButtons::cancelLinkWidgetAction($current_model, $px_size);
                    break;
                case 'new_tab':	
                case 'new_tab2':
                case 'open':
                    $subaction = SIMAHtmlButtons::modelDialogWidgetAction($current_model, $px_size);
                    break;
                case 'file_id'; 	
                case 'download':
                    $subaction = SIMAHtmlButtons::fileVersionOpenInDesktopAppWidgetAction($current_model);
                    break;
                case 'orderScan': 	
                    $subaction = SIMAHtmlButtons::orderScanWidgetAction($current_model, $px_size);
                    break;
                case 'refresh': 	
                    $subaction = SIMAHtmlButtons::modelRefreshWidgetAction($current_model, $px_size);
                    break;
                case 'lock':
                    $subaction = SIMAHtmlButtons::fileLockWidgetAction($current_model);
                    break;
                case 'restore_from_trash':
                    $subaction = SIMAHtmlButtons::modelRestoreFromTrashWidgetAction($current_model,'', $px_size);
                    break;
                case 'copy':
                    $subaction = SIMAHtmlButtons::copyModelWidgetAction($current_model, $px_size);
                    break;
                case 'qrcode':
                    $subaction = SIMAHtmlButtons::DownloadQRCodeWidgetAction($current_model);
                    break;
                case 'addDocument':
                    $subaction = $current_model->addDocumentWidgetAction($px_size);
                    break;
                case 'configuration_parameter_edit':
                    $subaction = SIMAHtmlButtons::ConfigurationParameterEditWidgetAction($current_model, false);
                    break;
                case 'configuration_parameter_edit_user':
                    $subaction = SIMAHtmlButtons::ConfigurationParameterEditWidgetAction($current_model, true);
                    break;
                case 'configuration_parameter_revert_to_default':
                    $subaction = SIMAHtmlButtons::ConfigurationParameterRevertToDefaultWidgetAction($current_model, false);
                    break;
                case 'configuration_parameter_revert_to_default_user':
                    $subaction = SIMAHtmlButtons::ConfigurationParameterRevertToDefaultWidgetAction($current_model, true);
                    break;
                case 'configuration_parameter_user_changable':
                    $subaction = SIMAHtmlButtons::ConfigurationParameterUserChangableWidgetAction($current_model);
                    break;
                case 'open_new_tab_notification':
                    $subaction = SIMAHtmlButtons::OpenNewTabNotificationWidgetAction($current_model);
                    break;
                case 'account_open_with_date':
                    $subaction = SIMAHtmlButtons::AccountOpenWithDateWidgetAction($current_model);
                    break;
                case 'cache_reset':
                    $subaction = SIMAHtmlButtons::CacheResetWidgetAction($current_model);
                    break;
                case 'cache_devalidate':
                    $subaction = SIMAHtmlButtons::CacheDevalidateWidgetAction($current_model);
                    break;
                case 'merge':
                    $subaction = self::mergeModel($current_model);
                    break;
                default:
                    $opt = $current_model->getAttributeDisplay($option);
                    if(is_array($opt))
                    {
                        $subaction = $opt;
                    }
                    else if(is_string($opt))
                    {
                        $error_message = __METHOD__.' - not implemented - $option: '.$option;
                        error_log($error_message);
                        Yii::app()->errorReport->createAutoClientBafRequest($error_message);
                        
                        $subaction = [
                            'html' => $opt
                        ];
                    }
                    break;
            }
            Yii::endProfile('part1.'.$option,$_trace_cat);
            if(!empty($subaction))
            {
                $subactions[] = $subaction;
            }
        }
        
        Yii::endProfile('part1',$_trace_cat);
        Yii::beginProfile('part2',$_trace_cat);
        
        $result = '';
        if(!empty($subactions))
        {
            if(SIMAMisc::isVueComponentEnabled())
            {
                $result = Yii::app()->controller->widget(SIMAButtonVue::class, [
                    'half' => true,
                    'icon' => 'dots',
                    'iconsize' => 16,
                    'no_border' => true,
                    'transparent_background' => true,
                    'hover_display_subactions' => true,
                    'hover_background_color' => 'e0e0e0',
                    'subactions_data' => $subactions,
                    'subactions_position'=>'right'
                ], true);
            }
            else
            {
                $result = Yii::app()->controller->widget('SIMAButton', [
                    'class' => '_half '.$additional_classes,
                    'action' => [
        //                'icon' => 'sima-icon _main-settings _18',
        //                'icon' => 'sima-icon _hamburger _18',
        //                'icon' => 'sima-icon _hamburger _horizontal _18',
        //                'icon' => 'sima-icon _dots _18',
                        'icon' => 'sima-icon _dots _16',
                        'subactions' => $subactions,
                        'subactions_order'=>'horizontal',
                        'subactions_position'=>'right'
                    ]
                ], true);
            }
        }
        
        Yii::endProfile('part2',$_trace_cat);
        
        return $result;
    }

    public static function modelIcons($model, $px_size=16, $exclude_icons=[], $include_icons=[])
    {
//        $_trace_cat = 'SIMA.Html.modelIcons.' . get_class($model);
//        $cache_tag = 'SIMAHtml_modelIcons_'.Yii::app()->user->id.'_'.get_class($model).'_'.$model->id;
//        $icons = Yii::app()->cache->get($cache_tag);
////        $icons = FALSE;//Yii::app()->cache->get($cache_tag);
//        if ($icons===FALSE)
//        {
            $model_options = $model->getModelOptions();
            if (count($include_icons) > 0)
            {
                $model_options = $include_icons;
            }
            
            $icons = '';
            foreach ($model_options as $option_pack)
            {
                $current_model = $model;
                if (is_array($option_pack))
                {
                    $option = $option_pack[0];
                    $option_params = array_slice($option_pack, 1, 1);
                    if (isset($option_params['model']))
                    {
                        $MODEL = $option_params['model'];
                        if (isset($option_params['model_id']))
                        {
                            $MODEL_ID = $option_params['model_id'];
                        }
                        else
                        {
                            $MODEL_ID = $model->id;
                        }
                        $current_model = $MODEL::model()->findByPk($MODEL_ID);
                    }
                }
                else
                {
                    $option = $option_pack;
                }
                
//                $_stats0 = Logger::ttBeginStat();
//
                if (in_array($option, $exclude_icons))
                {
                    continue;
                }
                
                $action = [];
                
//                    Yii::beginProfile($option,$_trace_cat);
                switch ($option)
                {
                    case 'form': 
//                        $icons .= SIMAHtml::modelFormOpen($model, array(), $px_size);
                        $action = SIMAHtmlButtons::modelFormOpenWidgetAction($current_model, [], $px_size);
                        break;
                    case 'delete':
//                            $icons .= (isset($model->confirmed) && $model->confirmed)?'':SIMAHtml::modelDelete($model,'', $px_size);
                        $action = SIMAHtmlButtons::modelDeleteWidgetAction($current_model,'', $px_size);
                        break;
                    case 'delete_forever':	
                        $icons .= SIMAHtml::modelDeleteForever($current_model,'', $px_size);
                        break;
                    case 'recycle':	
//                        $icons .= SIMAHtml::modelRecycle($model,$px_size);
                        $action = SIMAHtmlButtons::modelRecycleWidgetAction($current_model, $px_size);
                        break;
                    case 'cancel':	
                        $icons .= SIMAHtml::cancelLink($current_model,$px_size);
                        break;
                    case 'new_tab':	
                    case 'new_tab2':	//$icons .= SIMAHtml::newTab2Link($model).SIMAHtml::modelDialog($model); break;
                    case 'open':
//                        $icons .= SIMAHtml::modelDialog($model,$px_size);
                        $action = SIMAHtmlButtons::modelDialogWidgetAction($current_model, $px_size);
                        break;
                    case 'file_id'; 	
                        $icons .= SIMAHtml::fileDownload($current_model,$px_size);
                        break;
                    case 'download':
                        $action = SIMAHtmlButtons::fileVersionOpenInDesktopAppWidgetAction($current_model);
                        break;
                    case 'orderScan': 	
//                        $icons .= SIMAHtml::orderScan($model,$px_size);
                        $action = SIMAHtmlButtons::orderScanWidgetAction($current_model, $px_size);
                        break;
                    case 'refresh': 	
//                        $icons .= SIMAHtml::modelRefresh($model,$px_size);
                        $action = SIMAHtmlButtons::modelRefreshWidgetAction($current_model, $px_size);
                        break;
                    case 'lock': 
//                        $cache_key = __METHOD__.'_'.$model->tableName().'_'.$model->id
//                                                .'_'.$option.'_'.Yii::app()->user->id;
//                        $cache_result = Yii::app()->simacache->get($cache_key);
//
//                        if($cache_result === false)
//                        {
//                            $keywords = array(
//                                array($model, 'locked')
//                            );
//                            if (isset($model->isLocked) && $model->isLocked)
//                            {
//                                $cache_result = SIMAHtml::fileLocked($model, $px_size);
//                            }
//                            else if (isset($model->isLocked) && !$model->isLocked)
//                            {
//                                $cache_result = SIMAHtml::fileUnLocked($model, $px_size);
//                            }
//
//                            Yii::app()->simacache->set($cache_key, $cache_result, $keywords, true);
//                        }
//
//                        $icons .= $cache_result;
                        $action = SIMAHtmlButtons::fileLockWidgetAction($current_model);
                        break;
//                        case 'lock': 
//                            $_isLocked = $model->isLocked;
//    //                        if (isset($model->isLocked) && $model->isLocked)
//                            if ($_isLocked)
//                            {
//    //                            $_stats = Logger::ttBeginStat();
//                                $icons .= SIMAHtml::fileLocked($model, $px_size);
//    //                            Logger::ttEndStat('FileOptions - lock1',$_stats);
//                            }
//                            else //if (isset($model->isLocked) && !$model->isLocked)
//                            {
////                                $_stats1 = Logger::ttBeginStat();
//                                $icons .= SIMAHtml::fileUnLocked($model, $px_size);
////                                Logger::ttEndStat('FileOptions - lock2',$_stats1);
//                            }
//                            break;
                    case 'restore_from_trash':	
                        $icons .= SIMAHtml::modelRestoreFromTrash($current_model,'', $px_size);
                        break;
                    case 'copy': 	
                        $icons .= SIMAHtml::copyModel($current_model, $px_size);
                        break;
//                    case 'qrcode': /// MilosJ 20170828: ne bi trebalo da se koristi
//                        $icons .= SIMAHtmlButtons::DownloadQRCode($model);
//                        break;
                    case 'merge':
                        $action = SIMAHtml::mergeModel($current_model);
                        break;
                    default:       
                        $opt = $current_model->getAttributeDisplay($option);
                        if(is_array($opt))
                        {
                            $action = $opt;
                        }
                        else if(is_string($opt))
                        {
                            $icons .= $opt;
                        }
                        break;
                }
                
                
                if(!empty($action))
                {
                    if(SIMAMisc::isVueComponentEnabled())
                    {
                        $action['half'] = true;
                        if(isset($action['parent_class']))
                        {
                            $action['additional_classes'] = [$action['parent_class']];
                        }
                        if(isset($action['subactions']))
                        {
                            $action['subactions_data'] = $action['subactions'];
                            unset($action['subactions']);
                            error_log(__METHOD__.' - had subactions instead of subactions_data: '.SIMAMisc::toTypeAndJsonString($current_model)
                                    .' - $model_options: '.SIMAMisc::toTypeAndJsonString($model_options));
                        }
                        $icons .= Yii::app()->controller->widget(SIMAButtonVue::class, $action, true);
                    }
                    else
                    {
                        $class = '_half';
                        if(isset($action['parent_class']))
                        {
                            $class .= ' '.$action['parent_class'];
                        }
                        $icons .= Yii::app()->controller->widget(SIMAButton::class, [
                            'class' => $class,
                            'action' => $action
                        ], true);
                    }
                }
                
//                    Yii::endProfile($option,$_trace_cat);
//                Logger::ttEndStat('FileOptions - CALC',$_stats0);
            }
//            Yii::app()->cache->set($cache_tag,$icons);
//        }
        
        return $icons;
    }
    
    public static function modelMultiselectIconsContracted($model, $table_id, $multiselect_options=true, $px_size=16)
    {
        $subactions = [];
        
        if($multiselect_options === true)
        {
            $multiselect_options = $model->modelSettings('multiselect_options');
        }
        else if (!is_array($multiselect_options))
        {
            $multiselect_options = [];
        }

        foreach ($multiselect_options as $option_key=>$option_value)
        {
            $option = $option_value;
            if (is_array($option_value))
            {
                $option = $option_key;
            }
            
            $subaction = [];
            
            switch ($option)
            {
                case 'orderScan':  
                    $subaction = SIMAHtmlButtons::orderScanWidgetAction($table_id, $px_size);
                  break;
                case 'addTags':  
                    $subaction = SIMAHtmlButtons::multiselectAddFileTagsWidgetAction($table_id, $px_size);
                  break;
                case 'lock':  
                    $subaction = SIMAHtmlButtons::multiselectLockWidgetAction($table_id, $px_size);
                  break;
                case 'delete':  
                    $subaction = SIMAHtmlButtons::multiselectDeleteWidgetAction($table_id, $px_size);
                  break;
//                case 'review':  
//                    $subaction = SIMAHtmlButtons::multiselectReviewWidgetAction($table_id, $px_size);
//                  break;
                case 'recycle':  
                    $subaction = SIMAHtmlButtons::multiselectRecycleWidgetAction($table_id, $px_size);
                  break;
                case 'qrcode':  
                    $subaction = SIMAHtmlButtons::multiselectQRCodeWidgetAction($table_id, $px_size);
                  break;
                case 'download_zip':  
                    $subaction = SIMAHtmlButtons::filesDownloadZipWidgetAction($table_id, $px_size, $model, $option_value);
                  break;
                case 'download_ao_zip':  
                    $subaction = SIMAHtmlButtons::AccountOrderDownloadZipWidgetAction($table_id, $px_size, $model, $option_value);
                  break;
                case 'statuses':  
                    $statuses = is_array($option_value) ? $option_value : $model->getStatuses();
                    foreach ($statuses as $status)
                    {
                        $subactions[] = SIMAHtmlButtons::multiselectStatusWidgetAction($model, $status, $table_id, $px_size);
                    }
                  break;
                case 'add_bills_to_account_order':
                    if (Yii::app()->user->checkAccess('AddBillsToAccountOrder'))
                    {
                        $subaction = SIMAHtmlButtons::multiselectAddBillsToAccountOrderWidgetAction($table_id);
                    }
                    break;
                case 'employee_travel_order_payout':
                    if (Yii::app()->user->checkAccess('EmployeeTravelOrderPayout'))
                    {
                        $subaction = SIMAHtmlButtons::multiselectEmployeeTravelOrderPayoutWidgetAction($table_id);
                    }
                    break;
                case 'file_signature_view':
                    $subaction = SIMAHtmlButtons::multiselectFileignatureViewWidgetAction($model, $table_id);
                    break;
                case 'file_signature':
                    $subaction = SIMAHtmlButtons::multiselectFileignatureWidgetAction($model, $table_id);
                    break;
                case 'mark_notifications_as_read':
                    $subaction = [
                        'onclick' => [
                            'sima.notifications.markCheckedNotificationsAsReadOrUnread', 
                            $table_id,
                            true
                        ],
                        'icon' => "_mark_notifications_as_read",
                        'iconsize' => 16,
                        'tooltip' => Yii::t('BaseModule.SIMAHtmlButtons', 'MarkAsRead')
                    ];
                    break;
                case 'mark_notifications_as_unread':
                    $subaction = [
                        'onclick' => [
                            'sima.notifications.markCheckedNotificationsAsReadOrUnread', 
                            $table_id,
                            false
                        ],
                        'icon' => "_mark_notifications_as_unread",
                        'iconsize' => 16,
                        'tooltip' => Yii::t('BaseModule.SIMAHtmlButtons', 'MarkAsUnread')
                    ];
                    break;
                default:
                    error_log(__METHOD__.' - not implemented: $option: '.$option);
                    break;
            }
            
            if(!empty($subaction))
            {
                $subactions[] = $subaction;
            }
        }
        
        $result = '';
        if(!empty($subactions))
        {
            if(SIMAMisc::isVueComponentEnabled())
            {
                $result = Yii::app()->controller->widget(SIMAButtonVue::class, [
                    'half' => true,
                    'icon' => "_hamburger",
                    'iconsize' => $px_size,
                    'subactions_data' => $subactions,
                ], true);
            }
            else
            {
                $result = Yii::app()->controller->widget('SIMAButton', [
                    'class'=>'_half',
                    'action' => [
        //                'icon' => 'sima-icon _main-settings _18',
                        'icon' => "sima-icon _hamburger _$px_size",
        //                'icon' => 'sima-icon _dots _18',
                        'subactions' => $subactions,
        //                'subactions_order'=>'horizontal',
        //                'subactions_position'=>'right'
                    ]
                ], true);
            }
        }
        return $result;
    }
    
    public function multiselectLock($table_id, $px_size)
    {
        return "<span title='Otključaj/zaključaj fajlove' class='sima-icon _file-locked _$px_size' onclick='sima.icons.multiselectLock($(this),\"Da li zaista želite da otključate/zaključate selektovane fajlove?\",\"".$table_id."\");'></span>";
    }
    
    public function multiselectDelete($table_id, $px_size)
    {
        return "<span title='Obrišite selektovane stvari' class='sima-icon _recycle _$px_size' onclick='sima.icons.multiselectDelete($(this),\"Da li zaista želite da obrišete selektovane stvari?\",\"".$table_id."\");'></span>";
    }
    
    public function multiselectRecycle($table_id, $px_size)
    {
        return "<span title='Prebacite u kantu selektovane stvari' class='sima-icon _recycle _$px_size' onclick='sima.icons.multiselectRecycle($(this),\"Da li zaista želite da prebacite u kantu selektovane stvari?\",\"".$table_id."\");'></span>";
    }
    
//    public function multiselectReview($table_id, $px_size)
//    {
//        return "<span title='Označi kao pregledano' class='sima-icon _review _$px_size' onclick='sima.icons.multiselectReview($(this),\"Da li zaista želite da označite selektovane stvari kao pregledane?\",\"".$table_id."\");'></span>";
//    }
    
    public static function modelNumberFormat($model, $column)
    {            
        $columns = $model->getMetaData()->tableSchema->columns;
        $precision = '2';
        if (isset($columns[$column]) && isset($columns[$column]->dbType))
        {
            if ($columns[$column]->dbType === 'numeric')
            {
                if (Yii::app()->params['is_production'] === true)
                {
                    $precision = 2;
                }
                else
                {
                    throw new SIMAException('Sve numeric kolone u bazi moraju da imaju fiksnu preciznost');
                }
            }
            elseif (strpos($columns[$column]->dbType, 'numeric') !== false)
            {
                $precision = trim(str_replace(')','',substr($columns[$column]->dbType, strripos($columns[$column]->dbType, ',')+1)));
            }
            else if (strpos($columns[$column]->dbType, 'integer')!==FALSE)
            {
                $precision = '0';
            }
        }
        
        return SIMAHtml::number_format($model->$column, $precision);
//        return SIMAHtml::number_format($model->$column, 2, true);
    }
    
    public static function numberField($model, $column, $htmlOptions=[], $number_field_params=[])
    {
        $params = SIMAMisc::numberFieldParams($model, $column);
        $json_params = CJSON::encode(array_merge($params, $number_field_params));
        
        $uniq = isset($htmlOptions['id']) ? $htmlOptions['id'] : SIMAHtml::uniqid();
        $script = "$('#$uniq').numberField('init', $json_params);";
        Yii::app()->clientScript->registerScript(SIMAHtml::uniqid(),$script, CClientScript::POS_READY);
        
        $htmlOptions['name'] = get_class($model).'['.$column.']';
        $htmlOptions['value'] = isset($htmlOptions['value'])?$htmlOptions['value']:$params['value'];
        $html_options = '';
        foreach ($htmlOptions as $_key=>$_value)
        {
            $html_options .= $_key.'="'.$_value.'" ';
        }
        return "<input style='text-align:right;' id='$uniq' type='text' $html_options/>";
    }  
    public static function bankAccauntNumberField ($model, $column, $htmlOptions=[])
    {
        // we just need optio value form htmlOptions to set  value if this field alredy have
        $value = isset($model->$column) ? $model->$column : 0;
        
        $uniq = isset($htmlOptions['id']) ? $htmlOptions['id'] : 'sima_bank_number_'.SIMAHtml::uniqid();
        $script = "$('#$uniq').bankAccountField();";
        Yii::app()->clientScript->registerScript(SIMAHtml::uniqid(),$script, CClientScript::POS_READY);
        $htmlOptions['name'] = get_class($model).'['.$column.']';
        //there we keep value of inpu field
        $htmlOptions['value'] = isset($htmlOptions['value'])?$htmlOptions['value']:$value;
        $htmlOptions['class'] = 'bank-account-number-field '.(isset($htmlOptions['class'])?$htmlOptions['class']:'');
        $html_options = '';
        foreach ($htmlOptions as $_key=>$_value)
        {
            $html_options .= $_key.'="'.$_value.'" ';
        }
        return "<input style='text-align:right;' id='$uniq' type='text' $html_options/>";
    }
    
    
    public static function UserToDbDate($date, $is_date_time = false, $has_miliseond = false)
    {
        if(is_array($date))
        {
            return $date;
        }
        
        if ($date==null || $date=='' || $date=='null')
        {
            return null;
        }
        try
        {
            $dt = new DateTime($date);
            $format = 'Y-m-d';
            if ($is_date_time)
            {
                $format.=' H:i:s';
                    if ($has_miliseond)
                        $format.='.u';
            }
                        
//            $dt = DateTime::createFromFormat($format, $date);
//            if($dt === false)
//            {
//                return $date;
//            }
            
            return $dt->format($format);
        }
        catch (Exception $e)
        {            
            return $date;
        }
    }
    
    /**
     * KODA SA FORMATOM IMA I U Day MODELU!!!!
     * @param type $date
     * @param type $is_date_time
     * @param type $has_miliseond
     * @return type
     */
    public static function DbToUserDate($date, $is_date_time = false, $has_miliseond = false)
    {
        if(is_array($date))
        {
            return $date;
        }
        
        if ($date==null || $date=='')
        {
            return null;
        }
        try
        {
            $dt = new DateTime($date);
            $format = 'd.m.Y.';
            if ($is_date_time)
            {
                $format.=' H:i:s';
                    if ($has_miliseond)
                        $format.='.u';
            }
                                    
//            $dt = DateTime::createFromFormat($format, $date);
//            if($dt === false)
//            {
//                return $date;
//            }
                                    
            return $dt->format($format);
        }
        catch (Exception $e)
        {
            return $date;
        }
    }
    
    /**
     * 
     * @param boolean $is_date_time
     * @param boolean $has_miliseond
     * @return timestamp/string
     */
    public static function currentDateTime($is_date_time = true, $has_miliseond = false)
    {
        $dt = new DateTime();
        $format = 'd.m.Y';
        if ($is_date_time)
        {
            $format.=' H:i:s';
            if ($has_miliseond)
            {
                $format.='.u';
            }
        }
        return $dt->format($format);
    }
    
    /**
     * 
     * @param string $start_date
     * @param int $years
     * @param int $months
     * @param int $days
     * @return string
     * @throws Exception
     */
    public function getDateByPeriod($start_date, $years=null, $months=null, $days=null)
    {
        if ($years === null && $months === null && $days === null)
        {
            throw new Exception('Ne mogu biti prazni i godina i mesec i dan!');
        }
        
        $end_date = null;
        if ($years !== null)
        {
            $end_date = SIMAHtml::UserToDbDate(date('d.m.Y.', strtotime($start_date. " + $years year")));
            $start_date = $end_date;
        }
        if ($months !== null)
        {
            $end_date = SIMAHtml::UserToDbDate(date('d.m.Y.', strtotime($start_date. " + $months month")));
            $start_date = $end_date;
        }
        if ($days !== null)
        {
            $end_date = SIMAHtml::UserToDbDate(date('d.m.Y.', strtotime($start_date. " + $days days")));
        }
        
        return $end_date;
    }
    
    public function addContentToIFrame($content, $frame_id=null, $additional_classes='')
    {
        if ($frame_id === null)
        {
            $frame_id = SIMAHtml::uniqid();
        }
        Yii::app()->controller->raiseAddContentToIframe($frame_id, $content);
        return "<iframe id='$frame_id' class='sima-iframe $additional_classes' frameborder='0' scrolling='no'></iframe>";
    }
    
    public function searchFormList($params)
    {
        if (!isset($params['base_model']) || !isset($params['search_model']) || !isset($params['relName']))
        {
            throw new Exception("SIMAHTML - searchFormList - nisu lepo zadati parametri!");
        }
        
        if (!isset($params['uniq']))
        {
            $params['uniq'] = SIMAHtml::uniqid();
        }
        
        //pakovanje parametra
        $base_model = $params['base_model'];
        $base_model_name = get_class($params['base_model']);
        $search_model = $params['search_model'];
        $relName = $params['relName'];
        $params['base_model'] = $base_model_name;
        $relations = $base_model->relations();
        if (isset($relations[$relName]['model_filter']))
        {
            if (isset($params['filter']))
            {
                $params['filter'] = array_merge($params['filter'], $relations[$relName]['model_filter']);
            }
            else
            {
                $params['filter'] = $relations[$relName]['model_filter'];
            }
        }
        $_disabled = '';
        if (!empty($params['disabled']))
        {
            $_disabled = '_disabled';
        }
        $json_params = CJSON::encode($params);
        
        //proveravamo da li je setovana default stavka, ako jeste onda su stavke u listi klikabilne
        $_clickable = '';
        $default_item_params = array();
        if (isset($params['default_item']) && isset($base_model->id))
        {
            $_model = $params['default_item']['model']::model()->findByPk($base_model->id);
            $default_item = $_model->{$params['default_item']['column']};
            $default_item_params = $params['default_item'];
            $default_item_params['model_id'] = $base_model->id;
            if ($default_item !== null)
                $default_item_params['value'] = $default_item;
            $_clickable = '_clickable';
        }
        
        //proveravamo da li je default stavka zadata u relaciji - slucaj prilikom validacije
        $_relation = $base_model->$relName;
        if (is_array($_relation) && isset($_relation['default_item']))
        {
            $default_item_params = $_relation['default_item'];
            if (isset($_relation['default_item']['value']))
                $default_item = $_relation['default_item']['value'];
        }
        $relType = $relations[$relName][0];
        $html = "<div id='{$params['uniq']}' class='sima-model-form-search-list sima-form-custom-field $_disabled' "
                . "base_model='$base_model_name' search_model='$search_model' relName='$relName' relType='$relType'>";
        
        $ids = array();
        $single_list = '';
        //ako su u pitanju has_many i belongs relacije onda se ispisuje add dugme za dodavanje nove forme
        if ($relations[$relName][0]==='CHasManyRelation' || $relations[$relName][0]==='CBelongsToRelation')
        {
            $add_button_params = isset($params['add_button_params'])?$params['add_button_params']:array();
            $add_button_params['onSave'] = "function(response){sima.model.searchFormListAddItem(\"".$params['uniq']."\",response);}";
            $add_button_params['status'] = 'validate';
            if (!isset($add_button_params['init_data'][$search_model][$relations[$relName][2]]) ||
                    $add_button_params['init_data'][$search_model][$relations[$relName][2]] === '')
            {
                //cisto setujemo na neku vrednost da bi proslo validaciju, prava vrednost se upisuje u afterSave kasnije
                $add_button_params['init_data'][$search_model][$relations[$relName][2]] = '-1';
            }
            if ($relations[$relName][0]==='CBelongsToRelation')
            {
                $single_list = 'single_list';
                if ($base_model->$relName !== null)
                {
                    $_relation = $base_model->$relName;
                    if (is_array($_relation) && isset($_relation['ids']) && count($_relation['ids']) == 0)
                    {
                        $add_button_params['hidden'] = false;
                    }
                    else
                    {
                        $add_button_params['hidden'] = true;
                    }
                }
                
                if ($base_model->$relName === null)
                    $base_model->$relName = array();
                if (is_object($base_model->$relName))
                    $base_model->$relName = array($base_model->$relName);
            }
            
            if (isset($add_button_params['class']))
            {
                $add_button_params['class'] .= " add-item";
            }
            else
            {
                $add_button_params['class'] = "add-item";
            }

            $html .= SIMAHtml::modelFormOpen($search_model::model(), $add_button_params);
        }
        else
        {
            $html .= "<span title='Pretraga' class='sima-icon _search_form _16 $_disabled' onclick='sima.model.searchFormList($(this),$json_params);'></span>";
        }
        $html .= "<div class='sima-model-contribs $single_list' data-default_item_params='".  CJSON::encode($default_item_params)."'>";
        
        //ispisujemo stavke redom
        $relation_items = $base_model->$relName;
        if (count($relation_items) > 0)
        {
            $_relation_items = $relation_items;
            if (isset($relation_items['ids']) && is_array($relation_items['ids']))
            {
                $_relation_items = $relation_items['ids'];
            }            
            foreach ($_relation_items as $_relation_item)
            {
                $_id = (is_object($_relation_item))?$_relation_item->id:(is_array($_relation_item)?$_relation_item['id']:'');
                $_display_name = (is_object($_relation_item))?$_relation_item->DisplayName:(is_array($_relation_item)?$_relation_item['display_name']:'');               
                $_class = (is_object($_relation_item))?'_visible':(is_array($_relation_item)?$_relation_item['class']:'');
                
                array_push($ids, ['id'=>$_id, 'display_name'=>str_replace(['\"','\''], ['',''], $_display_name), 'class'=>$_class]);
                if (is_array($_id))
                {                    
                    $_search_model = $search_model::model();
                    $_id_json = CJSON::encode($_id);
                    $init_data = [
                        $search_model=>$_id
                    ];                    
                }
                else
                {                    
                    $_search_model = $search_model::model()->findByPk($_id);
                    $_id_json = $_id;
                    $init_data = [
                        $search_model=>$_search_model->attributes
                    ];                    
                }
                $span_uniq = SIMAHtml::uniqid();
                $edit_icon = '';
                if ($relations[$relName][0]!=='CManyManyRelation')
                {
                    $edit_icon = !is_null($_search_model)?SIMAHtml::modelFormOpen($_search_model, [
                        'button_img'=>'edit',
                        'init_data'=>$init_data,
                        'onSave'=>"function(response){sima.model.searchFormListEditItem(\"".$params['uniq']."\",response,$(\"#$span_uniq\"));}",
                        'status'=>'validate'
                    ]):'';
                }
                $_default_selected = '';
                if (isset($default_item) && $default_item !== null && $_id === $default_item)
                {
                    $_default_selected = '_selected';
                }
                
                $can_remove_item = true;
                if (!empty($params['can_remove_item_func']) && is_object($_relation_item) && $_relation_item !== null)
                {
                    $can_remove_item = call_user_func($params['can_remove_item_func'], $_relation_item->id);
                }
                
                $html .= "<span id='$span_uniq' class='item dots $_class' data-id='$_id_json' data-model='$search_model'>".$edit_icon;
                if ($can_remove_item === true)
                {
                    $html .= "<span class='sima-icon _delete _16 $_disabled' onclick='sima.model.removeContrib($(this));' title='Obriši'></span>";
                }
                $html .= "<span class='name $_default_selected $_clickable $_disabled' title='$_display_name' onclick='sima.model.setFormListDefaultItem($(this));'>$_display_name</span>";
                $html .= '</span>';
            }
        }        
        
        $html .= "</div>";
        $value_data = array(
            'ids'=>$ids,
            'default_item'=>$default_item_params
        );
        $value_data_json = CJSON::encode($value_data);
        $name = $base_model_name."[$relName][ids]";
        $html .= "<input class='mfslinput' type='hidden' value='$value_data_json' data-initial_value='$value_data_json' name='$name'>";
        
        $html .= "</div>";   
        return $html;
    }
    
    /**
     * Convert the data ($str) from RFC 2060's UTF-7 to UTF-8.
     * If input data is invalid, return the original input string.
     * RFC 2060 obviously intends the encoding to be unique (see
     * point 5 in section 5.1.3), so we reject any non-canonical
     * form, such as &ACY- (instead of &-) or &AMA-&AMA- (instead
     * of &AMAAwA-).
     *
     * Translated from C to PHP by Thomas Bruederli <roundcube@gmail.com>
     *
     * @param string $str Input string (UTF7-IMAP)
     * @return string Output string (UTF-8)
     */
    public static function utf7imap_to_utf8($str)
    {
        $Index_64 = array(
            -1,-1,-1,-1, -1,-1,-1,-1, -1,-1,-1,-1, -1,-1,-1,-1,
            -1,-1,-1,-1, -1,-1,-1,-1, -1,-1,-1,-1, -1,-1,-1,-1,
            -1,-1,-1,-1, -1,-1,-1,-1, -1,-1,-1,62, 63,-1,-1,-1,
            52,53,54,55, 56,57,58,59, 60,61,-1,-1, -1,-1,-1,-1,
            -1, 0, 1, 2,  3, 4, 5, 6,  7, 8, 9,10, 11,12,13,14,
            15,16,17,18, 19,20,21,22, 23,24,25,-1, -1,-1,-1,-1,
            -1,26,27,28, 29,30,31,32, 33,34,35,36, 37,38,39,40,
            41,42,43,44, 45,46,47,48, 49,50,51,-1, -1,-1,-1,-1
        );

        $u7len = strlen($str);
        $str   = strval($str);
        $p     = '';
        $err   = '';

        for ($i=0; $u7len > 0; $i++, $u7len--) {
            $u7 = $str[$i];
            if ($u7 == '&') {
                $i++;
                $u7len--;
                $u7 = $str[$i];

                if ($u7len && $u7 == '-') {
                    $p .= '&';
                    continue;
                }

                $ch = 0;
                $k = 10;
                for (; $u7len > 0; $i++, $u7len--) {
                    $u7 = $str[$i];

                    if ((ord($u7) & 0x80) || ($b = $Index_64[ord($u7)]) == -1) {
                        break;
                    }

                    if ($k > 0) {
                        $ch |= $b << $k;
                        $k -= 6;
                    }
                    else {
                        $ch |= $b >> (-$k);
                        if ($ch < 0x80) {
                            // Printable US-ASCII
                            if (0x20 <= $ch && $ch < 0x7f) {
                                return $err;
                            }
                            $p .= chr($ch);
                        }
                        else if ($ch < 0x800) {
                            $p .= chr(0xc0 | ($ch >> 6));
                            $p .= chr(0x80 | ($ch & 0x3f));
                        }
                        else {
                            $p .= chr(0xe0 | ($ch >> 12));
                            $p .= chr(0x80 | (($ch >> 6) & 0x3f));
                            $p .= chr(0x80 | ($ch & 0x3f));
                        }

                        $ch = ($b << (16 + $k)) & 0xffff;
                        $k += 10;
                    }
                }

                // Non-zero or too many extra bits
                if ($ch || $k < 6) {
                    return $err;
                }

                // BASE64 not properly terminated
                if (!$u7len || $u7 != '-') {
                    return $err;
                }

                // Adjacent BASE64 sections
                if ($u7len > 2 && $str[$i+1] == '&' && $str[$i+2] != '-') {
                    return $err;
                }
            }
            // Not printable US-ASCII
            else if (ord($u7) < 0x20 || ord($u7) >= 0x7f) {
                return $err;
            }
            else {
                $p .= $u7;
            }
        }

        return $p;
    }


    /**
     * Convert the data ($str) from UTF-8 to RFC 2060's UTF-7.
     * Unicode characters above U+FFFF are replaced by U+FFFE.
     * If input data is invalid, return an empty string.
     *
     * Translated from C to PHP by Thomas Bruederli <roundcube@gmail.com>
     *
     * @param string $str Input string (UTF-8)
     * @return string Output string (UTF7-IMAP)
     */
    public static function utf8_to_utf7imap($str)
    {
        $B64Chars = array(
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O',
            'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd',
            'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's',
            't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7',
            '8', '9', '+', ','
        );

        $u8len  = strlen($str);
        $base64 = 0;
        $i      = 0;
        $p      = '';
        $err    = '';

        while ($u8len) {
            $u8 = $str[$i];
            $c  = ord($u8);

            if ($c < 0x80) {
                $ch = $c;
                $n  = 0;
            }
            else if ($c < 0xc2) {
                return $err;
            }
            else if ($c < 0xe0) {
                $ch = $c & 0x1f;
                $n  = 1;
            }
            else if ($c < 0xf0) {
                $ch = $c & 0x0f;
                $n  = 2;
            }
            else if ($c < 0xf8) {
                $ch = $c & 0x07;
                $n  = 3;
            }
            else if ($c < 0xfc) {
                $ch = $c & 0x03;
                $n  = 4;
            }
            else if ($c < 0xfe) {
                $ch = $c & 0x01;
                $n  = 5;
            }
            else {
                return $err;
            }

            $i++;
            $u8len--;

            if ($n > $u8len) {
                return $err;
            }

            for ($j=0; $j < $n; $j++) {
                $o = ord($str[$i+$j]);
                if (($o & 0xc0) != 0x80) {
                    return $err;
                }
                $ch = ($ch << 6) | ($o & 0x3f);
            }

            if ($n > 1 && !($ch >> ($n * 5 + 1))) {
                return $err;
            }

            $i += $n;
            $u8len -= $n;

            if ($ch < 0x20 || $ch >= 0x7f) {
                if (!$base64) {
                    $p .= '&';
                    $base64 = 1;
                    $b = 0;
                    $k = 10;
                }
                if ($ch & ~0xffff) {
                    $ch = 0xfffe;
                }

                $p .= $B64Chars[($b | $ch >> $k)];
                $k -= 6;
                for (; $k >= 0; $k -= 6) {
                    $p .= $B64Chars[(($ch >> $k) & 0x3f)];
                }

                $b = ($ch << (-$k)) & 0x3f;
                $k += 16;
            }
            else {
                if ($base64) {
                    if ($k > 10) {
                        $p .= $B64Chars[$b];
                    }
                    $p .= '-';
                    $base64 = 0;
                }

                $p .= chr($ch);
                if (chr($ch) == '&') {
                    $p .= '-';
                }
            }
        }

        if ($base64) {
            if ($k > 10) {
                $p .= $B64Chars[$b];
            }
            $p .= '-';
        }

        return $p;
    }
    
    public static function copyModel($model, $px_size=16)
    {
        return "<span title='Kopiraj model' class='sima-icon _copy _$px_size' onclick='sima.icons.copyModel(\"".get_class($model)."\",$model->id);'></span>";
    }
    
    public static function mergeModel($model)
    {
        $result = [
            'onclick' => [
                'sima.modelMerge.mergeWith', get_class($model), $model->DisplayName, $model->id
            ],
            'icon' => '_merge',
            'tooltip' => 'izbriši spajanjem',
        ];
        return $result;
    }
    
    public static function simpleButton($text, $onclick='', $additionalClasses='', $title='')
    {
        return "<button class='sima-ui-button $additionalClasses' onclick='$onclick' title='$title'>$text</button>";
    }
    
    /**
     * 
     * @param array $buttons niz dugmica koji imaju $text, $onclick, $class
     * @return HTML
     */
    public static function simpleButtonGroup($buttons, $additionalClasses='')
    {
        $ret = "<div class='sima-ui-button-group-horizontal-round $additionalClasses'>";
        foreach ($buttons as $button)
        {
            $ret .= SIMAHtml::simpleButton($button['text'], isset($button['onclick'])?$button['onclick']:'');
        }

        return $ret.'</div>';
    }  
    
    
    public static function echo_param_array($arr, $id)
    {
        $ret = '<ul>';
        foreach ($arr as $param)
        { 
            $ret .= '<li class="template-param-'.$param->id.'">';
            $ret .= $param->name;
            if(!isset($param->templated_file_data))
            {
                $ret .= SIMAHtml::modelFormOpen(TemplatedFiledData::model(), array(
                    'init_data'=>array(
                        'TemplatedFiledData'=>array(
                            'templated_file_id'=>$id,
                            'template_param_id'=>$param->id
                        )
                    )
                ));
            }
            else
            {
                foreach($param->templated_file_data as $tfd)
                {
                    if($tfd->templated_file_id==$id)
                    {
                        $ret .= SIMAHtml::modelFormOpen($tfd);
                    }
                }
            }
            if (count($param->children)>0) 
            {
                $ret .= self::echo_param_array($param->children, $id);
            }
            $ret .= '</li>';
        }
        $ret .= '</ul>';
        return $ret;
    }
    
    public static function showInCurrency($amount, $currency, Day $day)
    {
        if(!is_null($currency))
        {
            try
            {
                $rate = MiddleCurrencyRate::get($currency, $day);
                if(is_null($rate))
                {
                    $result = SIMAHtml::money_format($amount);

                    $result .= SIMAHtml::modelFormOpen(MiddleCurrencyRate::model(), [
                        'init_data' => [
                            'MiddleCurrencyRate' => array(
                                'currency_id' => $currency->id,
                                'date' => $day->day_date
                            )
                        ],
                        'button_title' => Yii::t('AccountingModule.Bill', 'AddMiddleCurrencyRate', [
                            '{currency}'=>$currency, 
                            '{date}' => $day->day_date
                        ]),
    //                    'updateModelViews'=>SIMAHtml::getTag($owner)
                    ]);
                }
                else
                {
                    $result = SIMAHtml::money_format($amount/$rate->value, $currency->DisplayName);
                }
            }
            catch(SIMAWarnNoCurrencyRate $warn)
            {
                $result = '????';
            }
        }
        else
        {
            $result = SIMAHtml::money_format($amount);
        }
        
        return $result;
    }
    
    /**
     * 
     * @param string $name
     * @param string $select
     * @param array $data - (value=>display)
     * @param array $htmlOptions
     * @return string
     */
    public static function dropDownList($name, $select, $data, $htmlOptions=[])
    {
        if (isset($htmlOptions['class']))
        {
            $htmlOptions['class'] .= ' sima-dropdown-list';
        }
        else
        {
            $htmlOptions['class'] = 'sima-dropdown-list';
        }
        
        return CHtml::dropDownList($name, $select, $data, $htmlOptions);
    }    
    
//    public static function searchInputBox($action='', $htmlOptions=[], $placeholder='Pretraga...')
    public static function searchInputBox($action='', $htmlOptions=[], $placeholder=null)
    {
        if(empty($placeholder))
        {
            $placeholder = Yii::t('SIMAHtml', 'SearchPlaceholder');
        }

        if (isset($htmlOptions['class']))
        {
            $htmlOptions['class'] .= " sima-search-input-box";
        }
        else
        {
            $htmlOptions['class'] = "sima-search-input-box";
        }
        $html_options = '';
        foreach ($htmlOptions as $option_key=>$option_value)
        {
            $html_options .= " $option_key='$option_value'";
        }
        
        $return = "<div $html_options>
                       <span class='sima-old-btn _search _half sima-search-input-box-btn' onclick='$action'></span>              
                       <input type='text' class='sima-search-input-box-text' placeholder=$placeholder>
                   </div>";
        
        return $return;
    }
    
    /**
     * 
     * @param type $inputDateRangeParam - moze biti string, month, day, [month, month], [day, day]
     * @return array - ['startDate', 'endDate']
     * @throws Exception
     */
    public static function FormatDateRangeParam($inputDateRangeParam)
    {
        $dateRangeParamFinal = null;
                
        $dateRangeParam_type = gettype($inputDateRangeParam);
        if($dateRangeParam_type === 'string')
        {
            $dateRangeParamFinal = $inputDateRangeParam;
        }
        else if($dateRangeParam_type === 'object' && get_class($inputDateRangeParam)==='Month')
        {
            $fist_month_day = $inputDateRangeParam->firstDay();
            $last_month_day = $inputDateRangeParam->lastDay();
            $period = $fist_month_day->day_date.'<>'.$last_month_day->day_date;

            $dateRangeParamFinal = $period;
        }
        else if($dateRangeParam_type === 'object' && get_class($inputDateRangeParam)==='Day')
        {
            $fist_month_day = $inputDateRangeParam->day_date;
            $last_month_day = $inputDateRangeParam->day_date;
            $period = $fist_month_day.'<>'.$last_month_day;

            $dateRangeParamFinal = $period;
        }
        else if($dateRangeParam_type==='array' && count($inputDateRangeParam)===2
                && is_string($inputDateRangeParam[0]) && is_string($inputDateRangeParam[1]))
        {
            $period = $inputDateRangeParam[0].'<>'.$inputDateRangeParam[1];

            $dateRangeParamFinal = $period;
        }
        else if($dateRangeParam_type==='array' && count($inputDateRangeParam)===2
                && get_class($inputDateRangeParam[0])==='Month' && get_class($inputDateRangeParam[1])==='Month')
        {
            $fist_month_day = $inputDateRangeParam[0]->firstDay();
            $last_month_day = $inputDateRangeParam[1]->lastDay();
            $period = $fist_month_day->day_date.'<>'.$last_month_day->day_date;

            $dateRangeParamFinal = $period;
        }
        else if($dateRangeParam_type==='array' && count($inputDateRangeParam)===2
                && get_class($inputDateRangeParam[0])==='Day' && get_class($inputDateRangeParam[1])==='Day')
        {
            $fist_month_day = $inputDateRangeParam[0]->day_date;
            $last_month_day = $inputDateRangeParam[1]->day_date;
            $period = $fist_month_day.'<>'.$last_month_day;

            $dateRangeParamFinal = $period;
        }
        else
        {
            throw new Exception('invalid param type: '.CJSON::encode($inputDateRangeParam));
        }
        
        $startDate = null;
        $endDate = null;

        $exploded = explode('<>', $dateRangeParamFinal);
        if (count($exploded) == 1)
        {
            $dateRangeParamToDb = SIMAHtml::UserToDbDate(trim($dateRangeParamFinal));

            $startDate = $dateRangeParamToDb;
            $endDate = $dateRangeParamToDb;
        }
        else if (count($exploded) == 2)
        {
            $startDate = SIMAHtml::UserToDbDate(trim($exploded[0]));
            $endDate = SIMAHtml::UserToDbDate(trim($exploded[1]));
        }
        else
        {
            throw new Exception(Yii::t('HRModule.Absence', 'IllegalDateRangeParamFormat'));
        }
        
        return [
            'startDate' => $startDate,
            'endDate' => $endDate
        ];
    }
    
    public static function modelErrorSummary($model,$header=null,$footer=null,$htmlOptions=array(),$only_columns=[])
    {
        $content='';
        if(isset($htmlOptions['firstError']))
        {
            $firstError=$htmlOptions['firstError'];
            unset($htmlOptions['firstError']);
        }
        else
        {
            $firstError=false;
        }                
        
        foreach($model->getErrors() as $column=>$errors)
        {
            if (count($only_columns) === 0 || in_array($column, $only_columns))
            {
                foreach($errors as $error)
                {    
                    if($error!='')
                    {
                        $content.="<li>".$model->modelLabel().' - '.$model->getAttributeLabel($column).": $error</li>\n";
                    }
                    if($firstError)
                    {
                        break;
                    }
                }
            }
        }
            
        if($content!=='')
        {
            if($header===null)
            {
                $header='<p>'.Yii::t('yii','Please fix the following input errors:').'</p>';
            }
            if(!isset($htmlOptions['class']))
            {
                $htmlOptions['class']=CHtml::$errorSummaryCss;
            }
            return CHtml::tag('div',$htmlOptions,$header."\n<ul>\n$content</ul>".$footer);
        }
        else
        {
            return '';
        }
    }
    
    /**
     * 
     * @param boolean $state
     * @param text $tooltip
     * @param array $additionalParams
     * @return string
     */
    public static function StateIcon($state=false, $tooltip='', array $additionalParams=[])
    {
        $state_str = '_not_ok';
        if($state === true)
        {
            $state_str = '_ok';
        }
        
        $not_clickable_sate = '';
        if(isset($additionalParams['not_clickable']) && $additionalParams['not_clickable'] === true)
        {
            $not_clickable_sate = '_not_clickable';
        }
        
        $result = '<span class="sima-icon '.$state_str.' '.$not_clickable_sate.' _16 " title="'.$tooltip.'"></span>';
        
        return $result;
    }
    
    public static function boldSearchedText($text, $searched_text)
    {
        return str_replace($searched_text, "<span style='font-weight:bold;'>".$searched_text."</span>", $text);
    }
    
    public static function showAsImportant($text_to_show)
    {
        return "<span style='color: red; background-color: yellow; min-width: 100px;'>$text_to_show</span>";
    }
    
    /**
     * Funkcija koja vraca string razlike u vremenu 
     * @param DateInterval $interval
     * @return string
     */
    public static function timeDiffDisplay(DateInterval $interval, $params = [])
    {
        $_ret = '';
        $_rs = 10;//RelevanceScale -> koliko jedinica osnovne stavke treba da bude da bi naredni nivo bio nebitan 
        if ($interval->y > 0)
        {
            $_ret = $interval->y.Yii::t('BaseModule.TimeUnits','YearShort');
            if ($interval->y < $_rs)
            {
                $_ret .= ' '.$interval->m.Yii::t('BaseModule.TimeUnits','MonthShort');
            }  
        }
        elseif($interval->m > 0)
        {
            $_ret = $interval->m.Yii::t('BaseModule.TimeUnits','MonthShort');
            if ($interval->m < $_rs)
            {
                $_ret .= ' '.$interval->d.Yii::t('BaseModule.TimeUnits','DayShort');
            }  
        }
        elseif($interval->d > 0)
        {
            $_ret = $interval->d.Yii::t('BaseModule.TimeUnits','DayShort');
            if ($interval->d < $_rs)
            {
                $_ret .= ' '.$interval->h.Yii::t('BaseModule.TimeUnits','HourShort');
            }  
        }
        elseif($interval->h > 0)
        {
            $_ret = $interval->h.Yii::t('BaseModule.TimeUnits','HourShort');
            if ($interval->h < $_rs)
            {
                $_ret .= ' '.$interval->i.Yii::t('BaseModule.TimeUnits','MinuteShort');
            } 
        }
        elseif($interval->i > 0)
        {
            $_ret = $interval->i.Yii::t('BaseModule.TimeUnits','MinuteShort');
            if ($interval->i < $_rs)
            {
                $_ret .= ' '.$interval->s.Yii::t('BaseModule.TimeUnits','SecondShort');
            } 
        }
        elseif($interval->s > 0)
        {
            $_ret = $interval->s.Yii::t('BaseModule.TimeUnits','SecondShort');
        }
        
        if (!empty($_ret))
        {
            if ($interval->invert === 1)
            {
                $_ret = Yii::t('BaseModule.TimeUnits','WordBefore').' '.$_ret;
            }
            else
            {
                $_ret = Yii::t('BaseModule.TimeUnits','WordFor').' '.$_ret;
            }
        }
        
        return $_ret;
    }
    
    /**
     * Prikazuje razliku izmedju trenutnog vremena i zadatog ukoliko je unutar maksimalnog intervala, u suprotnom prikazuje datum
     * U tooltip se stavlja suprotna informacija
     * @param DateTime $_time Vreme koje se uporedjuje sa trenutnim
     * @param DateInterval $max_interval maksimalni period za uporedjivanje
     * @return string
     */
    public function timeDisplayRelative(DateTime $_time, DateInterval $max_interval = null)
    {
        $_diff_string = SIMAHtml::timeDiffDisplay((new DateTime())->diff($_time));
        $_time_string = $_time->format('d.m.Y.');
        
        if (is_null($max_interval))
        {
            $max_interval = new DateInterval('P1M');//default je jedan mesec
        }
        
        $_min_time = (new DateTime())->sub($max_interval);
        $_max_time = (new DateTime())->add($max_interval);
        
        if ($_time > $_min_time && $_time < $_max_time) //ako ulazi u opseg, da prikaze razliku
        {
            $_ret = "<span title='$_time_string'>$_diff_string</span>";
        }
        else
        {
            $_ret = "<span title='$_diff_string'>$_time_string</span>";
        }
        
        return $_ret;
    }
    
    public static function modelsStatusShowErrors($model_ids, $errors)
    {
        $errors_html = '';

        foreach ($errors as $error_key => $error_value)
        {
            if (!empty($error_value['error_messages']))
            {
                if (count($model_ids) > 1)
                {
                    $errors_html .= "<p style='font-weight:bold;'>{$error_value['model_display_name']}:</p>";
                }
                foreach ($error_value['error_messages'] as $error_message)
                {
                    $errors_html .= $error_message.'<br />';
                }
            }
        }

        if (!empty($errors_html))
        {
            $exception_message = $errors_html;
            if (count($model_ids) > 1)
            {
                $exception_message = '<p>'.Yii::t('BaseModule.Statuses', 'ValidationDidnotPassedForNextModels').':</p>'.$errors_html;
            }
            throw new SIMAWarnException($exception_message);
        }
    }
}