<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class SIMAUserIdentity extends CUserIdentity
{
    private $id;
    
    public $user_model = null;

    public function authenticate()
    {
        $user = User::model()->findByAttributes(array('username'=>$this->username));
        $this->user_model = $user;
        if(!isset($user))
        {
            $this->errorCode=self::ERROR_USERNAME_INVALID;
        }
        elseif(!$user->active)
        {
            $this->errorCode=self::ERROR_USERNAME_INVALID;
        }
        elseif($user->password!==$this->password && $user->password!==md5($this->password))
        {
            $this->errorCode=self::ERROR_PASSWORD_INVALID;
        }
        else
        {
            $this->logInUser($user);            
        }
        return !$this->errorCode;

    }
	
    public function getId()
    {
        return $this->id;
    }
    
    protected function logInUser($user)
    {
        if($user)
        {
            $this->id=$user->id;
            $this->errorCode=self::ERROR_NONE;
        }
    }
    
    public static function impersonate($userId)
    {
        $ui = null;
        $user = User::model()->findByPk($userId);
        if($user)
        {   
            $ui = new SIMAUserIdentity($user->username, "");
            $ui->logInUser($user);
        }
        return $ui;
    }
}