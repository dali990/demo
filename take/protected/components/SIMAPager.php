<?php

class SIMAPager
{
    public $count = 0;
    public $curr_page = 1;
    public $page_size = 20;
    public $max_page = 1;

    public function setPage($model, $condition, $curr_page=1, $page_size = 20, $resetScope=false)
    {
        $this->curr_page = $curr_page;
        if($resetScope === true)
        {
            $this->count = $model->resetScope()->count($condition);
        }
        else
        {
            $this->count = $model->count($condition);
        }
        $this->page_size = $page_size;
        $this->max_page = ($this->count == 0) ? 1 : ceil($this->count / $this->page_size);
        $condition->limit = $this->page_size;
        $condition->offset = ($this->curr_page - 1) * $this->page_size;
    }
}
