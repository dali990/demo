<?php

/**
 * Description of SIMAtexlive
 *
 * @author goran-set
 */
class SIMAtexlive
{
    private $tex = '';
    private $file_path='';
    public function set_tex_code($code, $path)
    {
        $this->tex =  $path;
        file_put_contents($this->tex, $code);
    }
    
    public function renderPDF($file_path)
    {
        $this->file_path=$file_path;
        $command="pdflatex --jobname=".$file_path.' '.$this->tex;
        Yii::app()->linuxCommandExecutor->executeCommand($command, Yii::app()->params['linuxcommandExecutor_timeout_s_SIMAtexlive_renderPDF1'], $output, $return); /// TODO
        if ($return !== 0)
        {            
            throw new SIMALatexException('Greska u kreiranju pdf-a!<br />'.CJSON::encode($output));
        }
        
        if(!file_exists ( $this->file_path.'.pdf' ))
        {
            $e = new SIMALatexException('Greska u kreiranju pdf-a!', $this->file_path.'.log');
            throw $e;
        }
        
        Yii::app()->linuxCommandExecutor->executeCommand($command.' && '.$command, Yii::app()->params['linuxcommandExecutor_timeout_s_SIMAtexlive_renderPDF2']);
        
        if(!file_exists ( $this->file_path.'.pdf' ))
        {
            $e = new SIMALatexException('Greska u kreiranju pdf-a!', $this->file_path.'.log');
            throw $e;
        }
        
        unlink($this->tex.'.aux');
    }
    
    protected function getImgCode($path)
    {
        $type = pathinfo($path, PATHINFO_EXTENSION);
        $data = file_get_contents($path);
        $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
        
        return $base64;
    }
    
    /**
    *
    * From passed table data generate pdf
    *
    * @columnnum    broj kolona tabele
    * 
    * @header       header tabele formata:
    *                   niz koji predstavlja parove {stirng, broj} 
    *                       - broj predstavlja koliko kolona header zahvata, string text header-a
    *               
    * @data         podaci tabele formata:
    *                   niz nizova redova, svaki podniz je niz podataka reda
    * 
    * @sum          suma odredjenih kolona formata:
    *                   niz elemenata array(string, broj)
    *                       - broj predstavlja redni broj kolone (pocev od 0), a string vrednost polja
    * 
    * @path         putanja na kojoj ce finalni pdf biti sacuvan
    * 
     * Primer za header oblika  |a|_b_|
     *                          | |c|d|
     * slalo bi se: $header = array(
     *                                  array(array('a',1), array('b',2))
     *                                  array(array('',1), array('c',1), array('d',1))
     *                                  --array('a'=>1, 'b'=>2),
     *                                  --array(''=>1, 'c'=>1, 'd'=>1)
     *                             )
    *
    */
    public function tableToPdf($pretable, $columnnum, $header, $data, $sum, $path)
    {
        // provera validnosti parametara
        
        if( !isset($columnnum) )
        {
            throw new Exception('broj kolona tabele nije postavljen');
        }
        
        // provera validnosti header-a
        if( !isset($header) )
        {
            throw new Exception('header tabele nije postavljen');
        }
        $header_rows = count($header);
        if( $header_rows < 1 )
        {
            throw new Exception('broj redova u header-u je manji od 1');
        }
        for($i=0; $i<$header_rows; $i++)
        {
            $row_elem_count = 0;
            
            for($j=0; $j<count($header[$i]); $j++)
            {
                $row_elem_count += $header[$i][$j][1];
            }
            
            if($row_elem_count != $columnnum)
            {
                throw new Exception('broj kolona u header-u se ne slaze sa prosledjenim brojem kolona u tabeli: '.$row_elem_count.' != '.$columnnum);
            }
        }
        
        // provera validnosti data-a
        if( !isset($data) )
        {
            throw new Exception('sadrzaj tabele nije postavljen');
        }
        $data_rows = count($data);
        if( $data_rows < 1 )
        {
            throw new Exception('broj redova u sadrzaju je manji od 1');
        }
        for($i=0; $i<$data_rows; $i++)
        {
            $row_elem_count = count($data[$i]);
            
            if($row_elem_count != $columnnum)
            {
                throw new Exception('broj kolona u redu sadrzaja se ne slaze sa prosledjenim brojem kolona u tabeli: '.$row_elem_count.' != '.$columnnum);
            }
        }
        
        // privera validnosti sum-e
        if( isset($sum) )
        {
            foreach($sum as $s)
            {
                if ($s[1] > $columnnum) 
                {
                    throw new Exception('u sumi je kolona koja prelazi predvidjeni broj kolona');
                }
            }
        }
        
        // privera validnosti putanje
        if( !isset($path) )
        {
            throw new Exception('putanja pdf-a nije postavljen');
        }
        
        if( isset($pretable) )
        {
            if( !is_array($pretable) )
            {
                throw new Exception('pretable mora biti niz');
            }
        }
        // sredjivanje header-a        
        $landscape_page_max_w = 25; // in cm
        $portrait_page_max_w = 18; // in cm
        $final_sizes = $this->calculateTableToPdfFinalSizes($columnnum, $landscape_page_max_w, $header, $data, $sum);
        
        $code = "\documentclass[a4paper]{article}
\usepackage[cm]{fullpage}
\usepackage{fancyhdr}
\usepackage{lastpage}
\usepackage{longtable}
\usepackage[utf8x]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{multirow}";
        
        $code .= "
\usepackage[";
        if( array_sum($final_sizes) > $portrait_page_max_w )
        {
            $code .= "landscape,";
        }
        $code .= "left=0cm,top=1cm,right=0cm,bottom=2cm]{geometry}";

        $code .= "
\pagestyle{fancy}
\\renewcommand{\headrulewidth}{0pt}
\\fancyhf{}
\\fancyfoot[R]{\\fontsize{7}{12}\selectfont \\textbf{Strana: {\\thepage} od \pageref{LastPage}}}

\\newcommand\MyHead[3]{\multicolumn{#1}{|c|}{\parbox{#2}{\centering \\vspace{0.05cm}\\textbf{#3}\\vspace{0.05cm}}}}
\\newcommand\MyCell[2]{\parbox{#1}{\\vspace{0.05cm}#2\\vspace{0.05cm}}}
\\newcommand\MySum[2]{\parbox{#1}{\\vspace{0.05cm}\\textbf{#2}\\vspace{0.05cm}}}
\\newcommand\MyCellPreRight[1]{\multicolumn{1}{r}{#1}}
\\newcommand\MyCellPreLeft[1]{\multicolumn{1}{l}{#1}}

\begin{document}
{
";
        /*
         * pretable deo
         */
        $code .= "{ % pretabela
\\renewcommand{\\tabcolsep}{1pt}
\begin{minipage}{0.5\\textwidth}
\begin{longtable}{ccc}
\\endhead
\\endfoot 
\\endlastfoot
";
        
        foreach($pretable as $key=>$value)
        {
            if( is_array($value) )
            {
                $had_first = false;
                foreach($value as $key1=>$value1)
                {
                    if(!$had_first)
                    {
                        $code .= "\MyCellPreLeft{".$key.":} & & \\\\ ";
                        $had_first = true;
                    }
                    $code .= " & \MyCellPreRight{".$key1.":} & \MyCellPreLeft{".$value1."} \\\\";
                }
            }
            else
            {
                $code .= "\MyCellPreLeft{".$key.":} & \MyCellPreLeft{".$value."} & \\\\";
            }
        }
        
        $code .= "
\\end{longtable}
\\end{minipage} 
}";
        
        $code .="
{ % tabela
\\renewcommand{\\tabcolsep}{1pt}
\begin{center}
\begin{longtable}{|";
        
        for($i=0; $i<$columnnum; $i++)
        {
            $code .= "c|";
        }
        
        $code .= "}

\multicolumn{".$columnnum."}{c}{} \\\\ \n
\hline ";
        
        /*
         * kreiranje dela header-a
         */
        $header_rows = count($header);
        for($i=0; $i<$header_rows; $i++)
        {
            $clines = array();
            
            $heder_row_count = $header[$i];

            $colw = $final_sizes[0];
            
            $header[$i][0][0] = SIMAtexlive::formatCellContent($header[$i][0][0], $colw, 1, '- ');
            
            $code .= "\MyHead{".$header[$i][0][1]."}{".$colw."cm}{".$header[$i][0][0]."}";
            
            if($header[$i][0][1] > 1)
            {
                array_push($clines,array(1, $header[$i][0][1]));
            }
            
            if($heder_row_count > 1)
            {
                $column_count = 1;
                for($j=1; $j<count($header[$i]); $j++)
                {
                    $colw = $final_sizes[$j];
                    
                    $header[$i][$j][0] = SIMAtexlive::formatCellContent($header[$i][$j][0], $colw, 1, '- ');
                    
                    $code .= " & \MyHead{".$header[$i][$j][1]."}{".$colw."cm}{".$header[$i][$j][0]."}";
                                        
                    if($header[$i][$j][1] > 1)
                    {
                        array_push($clines, array($column_count+1, ($column_count+1 + $header[$i][$j][1]-1)));
                    }
                    
                    $column_count += $header[$i][$j][1];
                }
            }
            
            $code .= " \\\\";
            
            $clines_count = count($clines);
            if($clines_count > 0)
            {
                for($n=0; $n<$clines_count; $n++)
                {
                    $code .= " \cline{". $clines[$n][0] ."-". $clines[$n][1] ."}";
                }
            }
            
            $code .= " \n";
        }
        
        $code .= "
\\endhead

\multicolumn{".$columnnum."}{|r|}{{Nastavlja se na slede\\'{c}oj strani}} \\\\ \hline
\\endfoot ";
        
        /*
         * kreiranje dela sume
         */
        if( isset($sum) && count($sum) > 0 )
        {
            $code .= "\hline \hline ";
            
            for($i=0; $i<$columnnum; $i++)
            {
                if($i != 0)
                {
                    $code .= ' &';
                }
                
                foreach( $sum as $s )
                {
                    if($i == $s[1])
                    {
                        $colw = $final_sizes[$i];
                        $s[0] = SIMAtexlive::formatCellContent($s[0], $colw, 1, '- ');

                        $code .= " \MySum{".$colw."cm}{".$s[0]."}";
                    }
                }
            }
            
            $code .= " \\\\ \n";
        }
        
        $code .= "\hline \\endlastfoot";
        
        /*
         * kreiranje dela podataka
         */
        for($i=0; $i<count($data); $i++)
        {            
            $code .= " \hline ";
            if( !empty($data[$i][0]) )
            {
                $colw = $final_sizes[0];
                $data[$i][0] = SIMAtexlive::formatCellContent($data[$i][0], $colw, 1, '- ');
                
                $code .= " \MyCell{".$colw."cm}{".$data[$i][0]."}";
            }
            
            if(count($data[$i]) > 1)
            {
                for($j=1; $j<count($data[$i]); $j++)
                {
                    $code .= " & ";
                    if( !empty($data[$i][$j]) )
                    {
                        $colw = $final_sizes[$j];
                        $data[$i][$j] = SIMAtexlive::formatCellContent($data[$i][$j], $colw, 1, '- ');
                        
                        $code .= " \MyCell{".$colw."cm}{".$data[$i][$j]."}";
                    }
                }
            }
            
            $code .= " \\\\ \n";
        }
        
        $code .="\\end{longtable}
\\end{center}
}

}
\\end{document}";
        
        $this->set_tex_code($code, $path);
        $this->renderPDF($path);
    }
    
    public static function getLongestWord($words)
    {
        $longestWord = '';
                
        $pattern = '/[ \-]/';
        $words = preg_split( $pattern, $words);
                
        $longestWordLength = 0;
        foreach ($words as $word) 
        {
            if($word != "\\textbackslash")
            {
                if (strlen($word) > $longestWordLength) 
                {
                   $longestWordLength = strlen($word);
                   $longestWord = $word;
                }
            }
        }
        
        return $longestWord;
    }
    
    public static function calcWordLengthInCm($word, $multiplicator = 1)
    {
        $result_size = 0;
        
        if(strlen($word) > 0)
        {
            /*
             * obrada "specijalnih" karaktera
             */

            $cnt = substr_count ($word, "\\_");
            if($cnt > 0)
            {
                $word = preg_replace("/\\\\_/", "", $word);
                $result_size += ($cnt * 0.2);
            }
            
            $cnt = substr_count ($word, "\n");
            if($cnt > 0)
            {
                $word = preg_replace("/\\n/", "", $word);
            }
            
            $cnt = substr_count ($word, "\r");
            if($cnt > 0)
            {
                $word = preg_replace("/\\r/", "", $word);
            }
            
            $cnt = substr_count ($word, "\\#");
            if($cnt > 0)
            {
                $word = preg_replace("/\\\\#/", "", $word);
                $result_size += ($cnt * 0.333);
            }

            $cnt = substr_count ($word, "\xe2\x80\x9c");
            if($cnt > 0)
            {
                $word = preg_replace("/\\xe2\\x80\\x9c/", "", $word);
                $result_size += ($cnt * 0.22);
            }

            $cnt = substr_count ($word, "\xe2\x80\x9d");
            if($cnt > 0)
            {
                $word = preg_replace("/\\xe2\\x80\\x9d/", "", $word);
                $result_size += ($cnt * 0.22);
            }

            $cnt = substr_count ($word, "\\xc4\\x91");
            if($cnt > 0)
            {
                $word = preg_replace("/\\xc4\\x91/", "", $word);
                $result_size += ($cnt * 0.22);
            }
                        
            $cnt = substr_count ($word, "\xf6");
            if($cnt > 0)
            {
                $word = preg_replace("/\\xf6/", "", $word);
                $result_size += ($cnt * 0.2);
            }
            
            $cnt = substr_count ($word, "\xc3\xb6");
            if($cnt > 0)
            {
                $word = preg_replace("/\\xc3\\xb6/", "", $word);
                $result_size += ($cnt * 0.2);
            }
            
            $cnt = substr_count ($word, "\xc3\xb0");
            if($cnt > 0)
            {
                $word = preg_replace("/\\xc3\\xb0/", "", $word);
                $result_size += ($cnt * 0.2);
            }
            
            $cnt = substr_count ($word, "\xc3\x88");
            if($cnt > 0)
            {
                $word = preg_replace("/\\xc3\\x88/", "", $word);
                $result_size += ($cnt * 0.2);
            }
                        
            $cnt = substr_count ($word, "а");
            if($cnt > 0)
            {
                $word = preg_replace("/а/", "", $word);
                $result_size += ($cnt * 0.2);
            }
            
            $cnt = substr_count ($word, "б");
            if($cnt > 0)
            {
                $word = preg_replace("/б/", "", $word);
                $result_size += ($cnt * 0.2);
            }
            
            $cnt = substr_count ($word, "в");
            if($cnt > 0)
            {
                $word = preg_replace("/в/", "", $word);
                $result_size += ($cnt * 0.2);
            }
            
            $cnt = substr_count ($word, "г");
            if($cnt > 0)
            {
                $word = preg_replace("/г/", "", $word);
                $result_size += ($cnt * 0.166);
            }
            
            $cnt = substr_count ($word, "д");
            if($cnt > 0)
            {
                $word = preg_replace("/д/", "", $word);
                $result_size += ($cnt * 0.22);
            }

            $cnt = substr_count ($word, "đ");
            if($cnt > 0)
            {
                $word = preg_replace("/đ/", "", $word);
                $result_size += ($cnt * 0.22);
            }
            
            $cnt = substr_count ($word, "ђ");
            if($cnt > 0)
            {
                $word = preg_replace("/ђ/", "", $word);
                $result_size += ($cnt * 0.2);
            }
            
            $cnt = substr_count ($word, "Đ");
            if($cnt > 0)
            {
                $word = preg_replace("/Đ/", "", $word);
                $result_size += ($cnt * 0.287);
            }
            
            $cnt = substr_count ($word, "е");
            if($cnt > 0)
            {
                $word = preg_replace("/е/", "", $word);
                $result_size += ($cnt * 0.183);
            }
            
            $cnt = substr_count ($word, "ж");
            if($cnt > 0)
            {
                $word = preg_replace("/ж/", "", $word);
                $result_size += ($cnt * 0.3);
            }
            
            $cnt = substr_count ($word, "ž");
            if($cnt > 0)
            {
                $word = preg_replace("/ž/", "", $word);
                $result_size += ($cnt * 0.166);
            }
            
            $cnt = substr_count ($word, "з");
            if($cnt > 0)
            {
                $word = preg_replace("/з/", "", $word);
                $result_size += ($cnt * 0.166);
            }
            
            $cnt = substr_count ($word, "и");
            if($cnt > 0)
            {
                $word = preg_replace("/и/", "", $word);
                $result_size += ($cnt * 0.22);
            }
            
            $cnt = substr_count ($word, "ј");
            if($cnt > 0)
            {
                $word = preg_replace("/ј/", "", $word);
                $result_size += ($cnt * 0.111);
            }
            
            $cnt = substr_count ($word, "к");
            if($cnt > 0)
            {
                $word = preg_replace("/к/", "", $word);
                $result_size += ($cnt * 0.22);
            }
            
            $cnt = substr_count ($word, "л");
            if($cnt > 0)
            {
                $word = preg_replace("/л/", "", $word);
                $result_size += ($cnt * 0.22);
            }
            
            $cnt = substr_count ($word, "м");
            if($cnt > 0)
            {
                $word = preg_replace("/м/", "", $word);
                $result_size += ($cnt * 0.25);
            }
            
            $cnt = substr_count ($word, "н");
            if($cnt > 0)
            {
                $word = preg_replace("/н/", "", $word);
                $result_size += ($cnt * 0.22);
            }
            
            $cnt = substr_count ($word, "њ");
            if($cnt > 0)
            {
                $word = preg_replace("/њ/", "", $word);
                $result_size += ($cnt * 0.287);
            }
            
            $cnt = substr_count ($word, "о");
            if($cnt > 0)
            {
                $word = preg_replace("/о/", "", $word);
                $result_size += ($cnt * 0.2);
            }
            
            $cnt = substr_count ($word, "п");
            if($cnt > 0)
            {
                $word = preg_replace("/п/", "", $word);
                $result_size += ($cnt * 0.21);
            }
            
            $cnt = substr_count ($word, "П");
            if($cnt > 0)
            {
                $word = preg_replace("/П/", "", $word);
                $result_size += ($cnt * 0.3);
            }
            
            $cnt = substr_count ($word, "р");
            if($cnt > 0)
            {
                $word = preg_replace("/р/", "", $word);
                $result_size += ($cnt * 0.22);
            }
            
            $cnt = substr_count ($word, "с");
            if($cnt > 0)
            {
                $word = preg_replace("/с/", "", $word);
                $result_size += ($cnt * 0.166);
            }
            
            $cnt = substr_count ($word, "т");
            if($cnt > 0)
            {
                $word = preg_replace("/т/", "", $word);
                $result_size += ($cnt * 0.183);
            }
            
            $cnt = substr_count ($word, "ћ");
            if($cnt > 0)
            {
                $word = preg_replace("/ћ/", "", $word);
                $result_size += ($cnt * 0.22);
            }
            
            $cnt = substr_count ($word, "ć");
            if($cnt > 0)
            {
                $word = preg_replace("/ć/", "", $word);
                $result_size += ($cnt * 0.166);
            }
            
            $cnt = substr_count ($word, "у");
            if($cnt > 0)
            {
                $word = preg_replace("/у/", "", $word);
                $result_size += ($cnt * 0.2);
            }
            
            $cnt = substr_count ($word, "х");
            if($cnt > 0)
            {
                $word = preg_replace("/х/", "", $word);
                $result_size += ($cnt * 0.2);
            }
            
            $cnt = substr_count ($word, "ц");
            if($cnt > 0)
            {
                $word = preg_replace("/ц/", "", $word);
                $result_size += ($cnt * 0.2);
            }
            
            $cnt = substr_count ($word, "ч");
            if($cnt > 0)
            {
                $word = preg_replace("/ч/", "", $word);
                $result_size += ($cnt * 0.22);
            }
            
            $cnt = substr_count ($word, "č");
            if($cnt > 0)
            {
                $word = preg_replace("/č/", "", $word);
                $result_size += ($cnt * 0.166);
            }
            
            $cnt = substr_count ($word, "š");
            if($cnt > 0)
            {
                $word = preg_replace("/š/", "", $word);
                $result_size += ($cnt * 0.15);
            }
            
            $cnt = substr_count ($word, "Š");
            if($cnt > 0)
            {
                $word = preg_replace("/Š/", "", $word);
                $result_size += ($cnt * 0.22);
            }

            $cnt = substr_count ($word, "\\v{c}");
            if($cnt > 0)
            {
                $word = preg_replace("/\\\\v{c}/", "", $word);
                $result_size += ($cnt * 0.166);
            }

            $cnt = substr_count ($word, "\\'{c}");
            if($cnt > 0)
            {
                $word = preg_replace("/\\\\'{c}/", "", $word);
                $result_size += ($cnt * 0.166);
            }
            
            $cnt = substr_count ($word, "\\v{C}");
            if($cnt > 0)
            {
                $word = preg_replace("/\\\\v{C}/", "", $word);
                $result_size += ($cnt * 0.275);
            }

            $cnt = substr_count ($word, "\\'{C}");
            if($cnt > 0)
            {
                $word = preg_replace("/\\\\'{C}/", "", $word);
                $result_size += ($cnt * 0.275);
            }

            $cnt = substr_count ($word, "\\v{s}");
            if($cnt > 0)
            {
                $word = preg_replace("/\\\\v{s}/", "", $word);
                $result_size += ($cnt * 0.15);
            }

            $cnt = substr_count ($word, "\\v{S}");
            if($cnt > 0)
            {
                $word = preg_replace("/\\\\v{S}/", "", $word);
                $result_size += ($cnt * 0.22);
            }

            $cnt = substr_count ($word, "\\v{z}");
            if($cnt > 0)
            {
                $word = preg_replace("/\\\\v{z}/", "", $word);
                $result_size += ($cnt * 0.166);
            }
            
            $cnt = substr_count ($word, "\\v{Z}");
            if($cnt > 0)
            {
                $word = preg_replace("/\\\\v{Z}/", "", $word);
                $result_size += ($cnt * 0.24);
            }
            
            $cnt = substr_count ($word, "\\");
            if($cnt > 0)
            {
                $word = preg_replace("/\\\\/", "", $word);
                $result_size += ($cnt * 0.2);
            }
            
            $cnt = substr_count ($word, "–");
            if($cnt > 0)
            {
                $word = preg_replace("/–/", "", $word);
                $result_size += ($cnt * 0.2);
            }

            $word_array = str_split($word);

            foreach($word_array as $char)
            {
                switch($char)
                {
                    case '':
                        $result_size += 0;
                        break;
                    case ' ':
                        $result_size += 0.02;
                        break;
                    case '.':
                        $result_size += 0.105;
                        break;
                    case ':':
                        $result_size += 0.111;
                        break;
                    case '-':
                        $result_size += 0.131;
                        break;
                    case '_':
                        $result_size += 0.2;
                        break;
                    case '/':
                        $result_size += 0.2;
                        break;
                    case '%':
                        $result_size += 0.333;
                        break;
                    case '>':
                        $result_size += 0.333;
                        break;
                    case '?':
                        $result_size += 0.183;
                        break;
                    case '+':
                        $result_size += 0.3;
                        break;
                    case ',':
                        $result_size += 0.09;
                        break;
                    case '"':
                        $result_size += 0.2;
                        break;
                    case '(':
                        $result_size += 0.15;
                        break;
                    case ')':
                        $result_size += 0.15;
                        break;
                    case '`':
                        $result_size += 0.09;
                        break;
                    case '\'':
                        $result_size += 0.09;
                        break;
                    case '!':
                        $result_size += 0.125;
                        break;
                    case '=':
                        $result_size += 0.2;
                        break;
                    case ']':
                        $result_size += 0.111;
                        break;
                    case '[':
                        $result_size += 0.111;
                        break;
                    case '#':
                        $result_size += 0.333   ;
                        break;
                    case '}':
                        $result_size += 0.2;
                        break;
                    case '{':
                        $result_size += 0.2;
                        break;
                    case '&':
                        $result_size += 0.3;
                        break;
                    case '$':
                        $result_size += 0.2;
                        break;
                    case ';':
                        $result_size += 0.111;
                        break;
                    case '|':
                        $result_size += 0.111;
                        break;
                    case '0':
                        $result_size += 0.2;
                        break;
                    case '1':
                        $result_size += 0.2;
                        break;
                    case '2':
                        $result_size += 0.2;
                        break;
                    case '3':
                        $result_size += 0.2;
                        break;
                    case '4':
                        $result_size += 0.2;
                        break;
                    case '5':
                        $result_size += 0.2;
                        break;
                    case '6':
                        $result_size += 0.2;
                        break;
                    case '7':
                        $result_size += 0.2;
                        break;
                    case '8':
                        $result_size += 0.2;
                        break;
                    case '9':
                        $result_size += 0.2;
                        break;
                    case 'a':
                        $result_size += 0.195;
                        break;
                    case 'b':
                        $result_size += 0.22;
                        break;
                    case 'c':
                        $result_size += 0.166;
                        break;
                    case 'd':
                        $result_size += 0.22;
                        break;
                    case 'e':
                        $result_size += 0.175;
                        break;
                    case 'f':
                        $result_size += 0.116;
                        break;
                    case 'g':
                        $result_size += 0.2;
                        break;
                    case 'h':
                        $result_size += 0.21;
                        break;
                    case 'i':
                        $result_size += 0.111;
                        break;
                    case 'j':
                        $result_size += 0.125;
                        break;
                    case 'k':
                        $result_size += 0.2;
                        break;
                    case 'l':
                        $result_size += 0.111;
                        break;
                    case 'm':
                        $result_size += 0.324;
                        break;
                    case 'n':
                        $result_size += 0.22;
                        break;
                    case 'o':
                        $result_size += 0.2;
                        break;
                    case 'p':
                        $result_size += 0.21;
                        break;
                    case 'q':
                        $result_size += 0.2;
                        break;
                    case 'r':
                        $result_size += 0.157;
                        break;
                    case 's':
                        $result_size += 0.15;
                        break;
                    case 't':
                        $result_size += 0.15;
                        break;
                    case 'u':
                        $result_size += 0.21;
                        break;
                    case 'v':
                        $result_size += 0.2;
                        break;
                    case 'w':
                        $result_size += 0.275;
                        break;
                    case 'x':
                        $result_size += 0.2;
                        break;
                    case 'y':
                        $result_size += 0.2;
                        break;
                    case 'z':
                        $result_size += 0.166;
                        break;
                    case 'A':
                        $result_size += 0.287;
                        break;
                    case 'B':
                        $result_size += 0.275;
                        break;
                    case 'C':
                        $result_size += 0.275;
                        break;
                    case 'D':
                        $result_size += 0.287;
                        break;
                    case 'E':
                        $result_size += 0.25;
                        break;
                    case 'F':
                        $result_size += 0.24;
                        break;
                    case 'G':
                        $result_size += 0.3;
                        break;
                    case 'H':
                        $result_size += 0.3;
                        break;
                    case 'I':
                        $result_size += 0.157;
                        break;
                    case 'J':
                        $result_size += 0.2;
                        break;
                    case 'K':
                        $result_size += 0.3;
                        break;
                    case 'L':
                        $result_size += 0.24;
                        break;
                    case 'M':
                        $result_size += 0.36;
                        break;
                    case 'N':
                        $result_size += 0.3;
                        break;
                    case 'O':
                        $result_size += 0.3;
                        break;
                    case 'P':
                        $result_size += 0.25;
                        break;
                    case 'Q':
                        $result_size += 0.3;
                        break;
                    case 'R':
                        $result_size += 0.3;
                        break;
                    case 'S':
                        $result_size += 0.22;
                        break;
                    case 'T':
                        $result_size += 0.275;
                        break;
                    case 'U':
                        $result_size += 0.3;
                        break;
                    case 'V':
                        $result_size += 0.287;
                        break;
                    case 'W':
                        $result_size += 0.4;
                        break;
                    case 'X':
                        $result_size += 0.287;
                        break;
                    case 'Y':
                        $result_size += 0.287;
                        break;
                    case 'Z':
                        $result_size += 0.24;
                        break;
                    case '*':
                        $result_size += 0.02;
                        break;
                    case '@':
                        $result_size += 0.4;
                        break;
                    default:
                        error_log(__METHOD__.' - unknown char: "'.$char.'" in text: "'.$word.'"');
                }
            }
        }
        
        return $result_size * $multiplicator;
    }
    
    public static function formatCellContent($content, $max_col_w, $multiplicator = 1, $wordseparator)
    {
        $content = SIMAtexlive::formatCellContent_escape_undefined_chars($content);

        /// ovo ce se konvertovati u karakter pa ne mora razbijati
        $string_combinations_that_will_become_sigle_symbols = [
            '$\\sum$'
        ];
        
        $max_runs = 10;
        $count = 0;
        $have_long_content = true;
        while($have_long_content)
        {            
            $count++;
            if($count >= $max_runs)
            {
                break;
            }
            
            $longestWord = SIMAtexlive::getLongestWord($content);
            $longestWordLength = SIMAtexlive::calcWordLengthInCm($longestWord, $multiplicator);
                        
            if($longestWordLength > $max_col_w 
                    && !in_array($longestWord, $string_combinations_that_will_become_sigle_symbols))
            {                
                if( preg_match("/,(?! )/",$longestWord) )
                {
                    $content = preg_replace("/,(?! )/", ', ', $content);
                }
                else if( preg_match("/;(?! )/",$longestWord) )
                {
                    $content = preg_replace("/;(?! )/", '; ', $content);
                }
                else if( preg_match("/(?<!\\\\)(?<! )_/",$longestWord) )
                {
                    $content = preg_replace("/(?<!\\\\)(?<! )_/", ' _', $content);
                }
                else if( preg_match("/(?<! )\\\\_/",$longestWord) )
                {
                    $content = preg_replace("/(?<! )\\\\_/", ' \\\\_', $content);
                }
                else if( preg_match("/\/(?! )/",$longestWord) )
                {
                    $content = preg_replace("/\/(?! )/", '/ ', $content);
                }
                else
                {
                    $halfed_len = strlen($longestWord)/2;
                    $part1 = substr ($longestWord, 0, $halfed_len);
                    $part2 = substr ($longestWord, $halfed_len);
                    
                    $latex_break_matches = null;
                    preg_match('/[^\\\\]\\\\+$/', $part1, $latex_break_matches);
                    if(!empty($latex_break_matches))
                    {
                        $latex_break_match = $latex_break_matches[0];
                        
                        $part1 = substr($part1, 0, strlen($part1) - strlen($latex_break_match) + 1);
                        $part2 = substr($latex_break_match, 1).$part2;
                    }
                    
                    $res_word = $part1.$wordseparator.$part2;

                    $content = str_replace($longestWord, $res_word, $content);
                }
            }
            else
            {
                $have_long_content = false;
            }
        }
        
        return $content;
    }
    
    public static function formatCellContent_escape_undefined_chars($content)
    {        
        $content_initial_replace = str_replace([
            "ľ",
            "á",
            "ß",
            "æ"
        ], [
            "\\'{l}",
            "\\'{a}",
            "?",
            "?"
        ], $content);
        
        $content_encoded_to_utf8 = mb_convert_encoding($content_initial_replace, 'UTF-8', 'UTF-8');
        $content_decoded_from_utf8 = utf8_decode($content_encoded_to_utf8);
        
        $content_str_replaced = str_replace([
            '­',
            "\xf6",
            "\xf0",
            "\xc8"
        ], [
            '-',
            "ö",
            "ð",
            "È"
        ], $content_decoded_from_utf8);
        
        $content_strtred = strtr($content_str_replaced, [
            "\xAD" => "",
        ]);
        
        return $content_strtred;
    }
    
    public function calculateTableToPdfFinalSizes($columnnum, $page_max_width, $header, $data, $sum)
    {
        $max_col_w = round(($page_max_width / $columnnum), 1); // in cm
        // prolaz da bi se utvrdile velicine kolona
        $suggested_column_sizes = array_fill(0, $columnnum, null);
        $longest_words = array_fill(0, $columnnum, null);
        $max_field_size = array_fill(0, $columnnum, null);
        $max_field_data = array_fill(0, $columnnum, null);
        $max_field_size_by_how_much = array_fill(0, $columnnum, null);
        
        for($i=0; $i<count($header); $i++)
        {
            for($j=0; $j<count($header[$i]); $j++)
            {
                $fielsData = TemplatedFile::specialCharactersSimplificationInTex($header[$i][$j][0]);
                
                /// \xc2\xad - soft hyphen - invisible
                $fielsData = str_replace('­', '-', $fielsData);
                $fielsData = strtr($fielsData, array("\xAD" => ""));

                $longestWord = SIMAtexlive::getLongestWord($fielsData);
                $longestWordLength = SIMAtexlive::calcWordLengthInCm($longestWord);
                
                $field_len = SIMAtexlive::calcWordLengthInCm($fielsData);
                if( !isset($max_field_size[$j]) || $max_field_size[$j] < $field_len )
                {
                    $max_field_size[$j] = $field_len;
                    $max_field_data[$j] = $fielsData;
                }
                
                if( !isset($suggested_column_sizes[$j]) || $suggested_column_sizes[$j] < $longestWordLength )
                {
                    $suggested_column_sizes[$j] = $longestWordLength;
                    $longest_words[$j] = $longestWord;
                }
            }
        }
        
        for($i=0; $i<count($data); $i++)
        {
            for($j=0; $j<count($data[$i]); $j++)
            {
                $fielsData = TemplatedFile::specialCharactersSimplificationInTex($data[$i][$j]);
                
                /// \xc2\xad - soft hyphen - invisible
                $fielsData = str_replace('­', '-', $fielsData);
                $fielsData = strtr($fielsData, array("\xAD" => ""));

                $longestWord = SIMAtexlive::getLongestWord($fielsData);
                $longestWordLength = SIMAtexlive::calcWordLengthInCm($longestWord);
                
                $field_len = SIMAtexlive::calcWordLengthInCm($fielsData);
                if( !isset($max_field_size[$j]) || $max_field_size[$j] < $field_len )
                {
                    $max_field_size[$j] = $field_len;
                    $max_field_data[$j] = $fielsData;
                }
                
                if( !isset($suggested_column_sizes[$j]) || $suggested_column_sizes[$j] < $longestWordLength )
                {
                    $suggested_column_sizes[$j] = $longestWordLength;
                    $longest_words[$j] = $longestWord;
                }
            }
        }
        
        foreach($sum as $s)
        {
            $word = $s[0];
            /// \xc2\xad - soft hyphen - invisible
            $word = str_replace('­', '-', $word);
            $word = strtr($word, array("\xAD" => ""));
            
            $longestWordLength = SIMAtexlive::calcWordLengthInCm($word);
            if( !isset($max_field_size[$j]) || $max_field_size[$j] < $longestWordLength )
            {
                $max_field_size[$j] = $field_len;
                $max_field_data[$j] = $fielsData;
            }

            if( !isset($suggested_column_sizes[$j]) || $suggested_column_sizes[$j] < $longestWordLength )
            {
                $suggested_column_sizes[$j] = $longestWordLength;
                $longest_words[$j] = $word;
            }
        }
        
        $num_of_fields_larger_than_max = 0;
        $suggestedsize = 0;
        $suggestedsize2 = 0;
        foreach($suggested_column_sizes as $size)
        {
            $suggestedsize += $size;
        }
        
        $use_max_suggested_size = false;
        if($suggestedsize < $page_max_width)
        {
            $use_max_suggested_size = true;
        }
        
        $maxsize = 0;
        $max_field_size_by_how_much_sum = 0;
        for($i=0; $i<$columnnum; $i++)
        {
            $size = $max_field_size[$i];
            
            if( $size > $max_col_w )
            {
                $suggestedsize2 += $max_col_w;
                $num_of_fields_larger_than_max++;
                $max_field_size_by_how_much[$i] = ($size - $max_col_w);
                $max_field_size_by_how_much_sum += ($size - $max_col_w);
            }
            else
            {
                $suggestedsize2 += $size;
            }
            
            $maxsize += $size;
        }
        
        $available_size = $page_max_width - $suggestedsize2;
                
        $use_max_available_size = false;
        if($maxsize < $page_max_width)
        {
            $use_max_available_size = true;
        }
        
        $final_sizes = array_fill(0, $columnnum, null);
        for($i=0; $i<$columnnum; $i++)
        {
            if($use_max_available_size)
            {
                $final_sizes[$i] = $max_field_size[$i];
            }
            else
            {
                if($max_field_size[$i] < $max_col_w)
                {
                    $final_sizes[$i] = $max_field_size[$i];
                }
                else
                {
                    if($available_size > 0)
                    {
                        $size_to_be_given = $max_col_w + 
                                        ($available_size 
                                            * ($max_field_size_by_how_much[$i]/$max_field_size_by_how_much_sum));

                        if($size_to_be_given > $max_field_size[$i])
                        {
                            $size_to_be_given = $max_field_size[$i];
                        }

                        $final_sizes[$i] = $size_to_be_given;
                    }
                    else
                    {
                        $final_sizes[$i] = $max_col_w;
                    }
                }
            }
        }
        
        return $final_sizes;
    }
}
