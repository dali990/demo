<?php

class SIMAAssetManager extends CAssetManager
{
    public function init()
    {
        parent::init();
    }
    
    protected function generatePath($file, $hashByName = false)
    {
        $directory = $file;
        if (is_file($file))
        {
            $pathForHashing = $hashByName ? basename($file) : dirname($file) . filemtime($file);
            $directory = dirname($file);
        }
        else
        {
            $pathForHashing = $hashByName ? basename($file) : $file . filemtime($file);
        }

//        $curr_version = Yii::app()->params['updatingStartTimestamp'];
        $curr_version = Yii::app()->maintenanceManager->getUpdatingStartTimestamp();

        if ($curr_version === false)
        {
            $curr_version = $this->hashDirectory($directory);
        }

        return $curr_version . '_' . $this->hash($pathForHashing).crc32(Yii::app()->clientScript->serverBaseUrl);
    }

    private function hashDirectory($directory)
    {
        if (!is_dir($directory))
        {
            return false;
        }

        $files = array();
        $dir = dir($directory);

        while (false !== ($file = $dir->read()))
        {
            if ($file != '.' and $file != '..')
            {
                if (is_dir($directory . DIRECTORY_SEPARATOR . $file))
                {
                    $files[] = $this->hashDirectory($directory . DIRECTORY_SEPARATOR . $file);
                }
                else
                {
                    $files[] = md5_file($directory . DIRECTORY_SEPARATOR . $file);
                }
            }
        }

        $dir->close();

        return md5(implode('', $files));
    }

}
