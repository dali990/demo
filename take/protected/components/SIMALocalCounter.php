<?php

/**
 * Klasa koja sluzi kao omotac za brojac. Poenta je da brojac moze da se prenosi kao parametar po referenci
 * primer koriscenja je broj za XML u platama (jer se od jedne plate moze napraviti vise XML-ova)
 */
class SIMALocalCounter
{
    private $_cnt = 0;
    
    /**
     * 
     * @param integer $init_value odakle se pocinje brojanje
     */
    public function __construct($init_value = 0)
    {
        $this->_cnt = $init_value;
    }
    
    /**
     * Vraca poslednji zadat broj
     * @return integer
     */
    public function getLastValue()
    {
        return $this->_cnt;
    }
    
    /**
     * uvecava brojac za jedan i vraca novu vrednost
     * @return integer
     */
    public function getNextValue()
    {
        return ++$this->_cnt;
    }
}
