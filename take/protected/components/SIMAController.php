<?php


class SIMAController extends CController
{
    
    public $called_process_output = false;
    public $processOutputHead = '';
    public $processOutputBegin = '';
    public $processOutputEnd = '';
    public $processOutputLoadAndReady = '';
    
    /**
     * @var string the default layout for the controller view. Defaults to '//layouts/column1',
     * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
     */
    public $layout='//layouts/mainmenu';
    public $isAjaxLong = false;

    public function filters()
    {
        return array(
//            'isInMaintenance',
            'accessControl',
            'mainLoop'// - renderModelView'
        );
    }
    
    public function processOutput($output, $render_scripts=true) 
    {
        if ($this->called_process_output === true)
        {
            error_log(__METHOD__. ' - ProcessOutput se pozvao dva puta');
            error_log('---------------TRACE START---------------');
//            error_log(CJSON::encode(debug_backtrace()));
//            error_log(print_r(debug_backtrace(), true));
            error_log(SIMAMisc::stackTrace());
            error_log('---------------TRACE END---------------');            
        }
        $this->called_process_output = true;
        
        if (Yii::app()->request->isAjaxRequest === true)
        {
            $this->processOutputHead = Yii::app()->clientScript->renderHead();
            $this->processOutputBegin = Yii::app()->clientScript->renderBodyBegin();
            $this->processOutputEnd = Yii::app()->clientScript->renderBodyEnd();            
            $this->processOutputLoadAndReady = Yii::app()->clientScript->renderBodyLoadAndReady();
            return parent::processOutput($output, false);
        }
        else
        {
            return parent::processOutput($output, $render_scripts);
        }                        
    }

    /**
     * it all starts here
     * @param unknown_type $filterChain
     */
    public function filterMainLoop($filterChain)
    {
        if (Yii::app()->controller->getRoute() != 'base/events/index')
        {
            ob_start();
        }
        
        register_shutdown_function('SIMAController::handleFatalPhpError');

        $status = null;
        $message_array = [];
        $status_code = null;

        try
        {
            $filterChain->run();
        }
        catch (SIMAWarnException $e)
        {
            $status = 'failure'; 
            $message_temp = CJSON::decode($e->getMessage());
            if (gettype($message_temp) === 'array')
            {
                $this->respondFail($message_temp);
            }
            else
            {
                $message_array['exception_message'] = $e->getMessage();
            }
        }
        catch (SIMAWarnAutoBafException $e)
        {
            $status = 'failure';
            $message_array['exception_message'] = $e->getMessage();
        }
        catch (SIMALatexException $e)
        {
            $status = 'failure';
            $message = $e->getMessage();

            if (isset($e->error_log_file) && file_exists($e->error_log_file))
            {
                $message .= '<br/><div align="left">';

                $in = fopen($e->error_log_file, "r");

                $found = false;

                while (($buffer = fgets($in)) !== false)
                {
                    if (!$found && preg_match('/^\!/', $buffer))
                    {
                        $found = true;
                    }

                    if ($found)
                    {
                        $message .= "<br/>" . $buffer;
                    }
                }

                $message .= "</div>";
            }
            
            $message_array['exception_message'] = $message;
        }
        catch (CDbException $e)
        {
            $status = 'fatal_db_failure';
            $message_array = $this->getExceptionParamsAsArray($e);
        }
        catch (SIMAAjaxLongException $e)
        {
            $status = 'fatal_failure';
            $message_array = $this->getExceptionParamsAsArray($e);
            if (!empty($e->statusCode))
            {
                $status_code = $e->statusCode;
            }
        }
        catch (SIMAException $e)
        {
            $status = 'fatal_failure';
            $message_array = $this->getExceptionParamsAsArray($e);
            if (!empty($e->statusCode))
            {
                $status_code = $e->statusCode;
            }
        }
        catch (Exception $e)
        {
            $status = 'fatal_failure';
            $message_array = $this->getExceptionParamsAsArray($e);
            if (!empty($e->statusCode))
            {
                $status_code = $e->statusCode;
            }
        }
        catch (Throwable $t)
        {
            $status = 'fatal_failure';
            $message_array = $this->getExceptionParamsAsArray($t);
            if (!empty($t->statusCode))
            {
                $status_code = $t->statusCode;
            }
        }
        
        if (!empty($message_array['exception_message']))
        {
            $detected_encoding = mb_detect_encoding($message_array['exception_message']);
            if($detected_encoding === false)
            {
                $message_array['exception_message'] = utf8_encode($message_array['exception_message']);
            }
        }
        
        if ($status != null && Yii::app()->controller->getRoute() != 'base/events/index')
        {
            $this->addAndSendError($status, $status_code, $message_array);
        }
    }
    
    public static function handleFatalPhpError()
    {
        $last_error = error_get_last();
         
        if (
                $last_error['type'] === E_ERROR || 
                $last_error['type'] === E_COMPILE_ERROR
           ) 
        {
            Yii::app()->controller->addAndSendError('fatal_failure', $last_error['type'], [
                'exception_message' => $last_error['message'],
                'exception_file' => $last_error['file'],
                'exception_line' => $last_error['line']
            ]);
        }
    }
    
    public function addAndSendError($status, $status_code, $message_array)
    {
        $response_params = [
            'status' => $status,
            'status_code' => $status_code,
            'message' => !empty($message_array['exception_message']) ? $message_array['exception_message']: '',
        ];
        if ($status === 'failure')
        {
            if ($this->isAjaxLong === true)
            {
                $this->sendFailureEvent($response_params);
            }
            else
            {
                ob_end_clean();
                $this->respond($response_params);
            }
        }
        else
        {
            $error_report = Yii::app()->errorReport->addError([
                'submitter_id' => Yii::app()->user->id,
                'action' => Yii::app()->controller->getRoute(),
                'message' => CJSON::encode($message_array),
                'get_params' => filter_input_array(INPUT_GET),
                'post_params' => filter_input_array(INPUT_POST)
            ]);
            $response_params['error_id'] = !empty($error_report) ? $error_report->id : null;

            if ($this->isAjaxLong === true)
            {                    
                $this->sendFatalFailureEvent($response_params);
            }
            else
            {
                ob_end_clean();
                $this->respond($response_params);
            }
        }
    }
    
    public function getExceptionParamsAsArray($exception)
    {
        return [
            'exception_class' => get_class($exception),
            'exception_message' => $exception->getMessage(),
            'exception_file' => $exception->getFile(),
            'exception_line' => $exception->getLine(),
            'exception_trace' => $exception->getTraceAsString()
        ];
    }

    private $_notes = array();
    private $_updateModels = array();
    private $_iframes = array();
    private $_actions = array();
    /**
     * 
     * @param type $note
     */
    public function raiseNote($note, $note_code = null)
    {
        if (is_null($note_code))
        {
            if (gettype($note) === 'string')
            {
                $note = array('text' => $note);
            }
        
            if(!in_array($note, $this->_notes))
            {
                $this->_notes[] = $note;
            }
        }
        else
        {
            if (isset($this->_notes[$note_code]))
            {
                $this->_notes[$note_code]['text'] = $this->_notes[$note_code]['text'] . '<br />' . $note;
            }
            else
            {
                $this->_notes[$note_code] = ['text' => $note];
            }
        }
    }

    /**
     * 
     * @param type $model
     */
    public function raiseUpdateModels($model)
    {        
        $this->_updateModels[] = $model;
    }

    public function raiseAddContentToIframe($frame_id, $content)
    {
        $this->_iframes[] = array(
            'frame_id'=>$frame_id,
            'content'=>$content
        );
    }
    
    public function raiseAction($function, $params=array(), $merge=false)
    {
        $new_actions = array();
        $founded = false;        
        foreach ($this->_actions as $action)
        {
            if ($action[0] === $function && $action[2] && $merge)
            {
                $new_params = array();
                foreach ($action[1] as $key => $value)
                {
                    if (is_array($value) && is_array($params[$key]))
                    {
                        array_push($new_params, array_merge_recursive($value, $params[$key]));
                    }
                    else
                    {
                        $new_params[] = $value.','.$params[$key];                        
                    }
                }
                $new_action = array($function, $new_params, $merge);
                array_push($new_actions, $new_action);
                $founded = true;
            }
            else
            {
                array_push($new_actions, $action);
            }
        }
        if (!$founded)
        {
            $new_action = array($function, $params, $merge);
            array_push($new_actions, $new_action);
        }
        
        $this->_actions = $new_actions;
    }
    
    /**
     * @param type $ret - niz koji se prosledjuje
     */
    public function respond($ret, $updateModels=[])
    {
        Yii::app()->performAfterWork();
        
        if($this->isApiAction())
        {            
//            throw new Exception('Cannot use respond with api function');
        }
        else 
        {
            $update_model_tags = $this->mergeUpdateModels($updateModels);
            
            /*if(Yii::app()->configManager->get('base.develop.sma_events_over_eventrelay') !== true && Yii::app()->hasModule('mobile') && !empty($update_model_tags))
            {
                MobileComponent::sendDataToAllUsersDevices([
                    'event' => 'UPDATE_VUE_MODELS',
//                    'data' => implode(',', $update_model_tags)
                    'data' => $update_model_tags
                ]);
            }*/
            
            $ret['notes'] = $this->_notes;
            $ret['iframes'] = $this->_iframes;
            $ret['actions'] = $this->_actions;
            $ret['updateModelViews'] = $update_model_tags; 
            if (!$this->called_process_output)
            {
                $this->processOutput('');
            }
            $ret['processOutputHead'] = $this->processOutputHead;
            $ret['processOutputBegin'] = $this->processOutputBegin;
            $ret['processOutputEnd'] = $this->processOutputEnd;
            $ret['processOutputLoadAndReady'] = $this->processOutputLoadAndReady;
        }
                
        echo CJSON::encode($ret);
        Yii::app()->end();
    }
    
    public function respondMobileApp($response)
    {
        Yii::app()->performAfterWork();
        
        $callback = $this->filter_get_input('callback');
        echo $callback."(".json_encode($response).");";
        Yii::app()->end();
    }
    public function respondMobileAppOK($response)
    {
        $ret = array(
            'status' => 'success',
            'response' => $response
        );
        
        $this->respondMobileApp($ret);
    }
    public function respondMobileAppFail($message)
    {
        $response = array(
            'status' => 'failure',
            'message' => $message
        );
        $this->respondMobileApp($response);
    }

    /**
     * odgovor je success i proverafa notifikacije
     * @param array $response niz koji se prosledjuje success funkciji
     * @param string $updateModels modeli koje treba updateovati
     * @param string $message poruka koja se prosledjuje
     */
    public function respondOK($response = array(), $updateModels = array(), $message = '')
    {        
        $ret = array(
            'status' => 'success',
            'message' => $message,
            'response' => $response
        );
        
        if($this->isApiAction())
        {
            $this->respond($ret);
        }
        else
        {           
            $this->respond($ret, $updateModels);
        }                
    }

    /**
     * odgovor je success i proverafa notifikacije
     * @param array $response niz koji se prosledjuje success funkciji
     * @param string $message poruka koja se prosledjuje
     */
    public function respondFail($message = '')
    {
        if (gettype($message) == 'array')
        {
            $_new_messages = [];
            foreach ($message as $key => $value)
            {
                $_new_messages[$key] = (gettype($value) == 'string')?$value:CJSON::encode($value);
            }
            $message = 'Problemi:<br /><ul><li>'.implode('</li><li>', $_new_messages).'</li></ul>';
        }
        
        $ret = array(
            'status' => 'failure',
            'message' => $message
        );
        $this->respond($ret);
    }
    
    public function getModelViewsForUpdate()
    {
        return $this->mergeUpdateModels([]);
    }
    
    public function mergeUpdateModels($updateModels)
    {
        if (gettype($updateModels) == 'string')
        {
            throw new Exception('funkciji respondOK poslat string za drugi argument');
        }
        if (gettype($updateModels) != 'array')
        {
            $updateModels = array($updateModels);
        }
        $updateModelViews = array();            
        $updateModelsFinal = array_merge($updateModels, $this->_updateModels);
        while (count($updateModelsFinal) > 0)
        {
            $model = array_pop($updateModelsFinal);
            if (gettype($model) == 'object' && $model != null)
            {
                $tag = SIMAHtml::getTag($model);
                if (!in_array($tag, $updateModelViews))
                {
                    $updateModelViews[] = $tag;
                    $relations = $model->modelSettings('update_relations');
                    foreach ($relations as $relation)
                    {
                        $rels = array();
                        if(isset($model->$relation))
                        {
                            if (gettype($model->$relation) == 'array')
                            {
                                $rels = $model->$relation;
                            }
                            else
                            {
                                $rels[] = $model->$relation;
                            }
                        }
                        foreach ($rels as $rel)
                        {
                            array_push($updateModelsFinal, $rel);
                        }
                    }
                }
            }
        }
        
        return $updateModelViews;
    }
    
    /**
     * Pomocna funkcija u funkciji renderModelView() koja dohvata decu od $start_model prema relaciji $with_array
     * @param type $start_model
     * @param array $with_array ne menja se, nego se kroz rekurziju provlaci $i brojac (zadaje se u GUI modelima)
     * @param integer $i brojac do koje dubine je stigao $with_array 
     * @return string
     */
    private function getTagsRecursive($start_model, $with_array, $i = 0)
    {
        $tags = '';
        $setted = true;
        for (; $i < count($with_array) && $setted; $i++)
        {
            $rel = $with_array[$i];
            if (isset($start_model->$rel))
            {
                $start_model = $start_model->$rel;
                $tags .= ' ' . SIMAHtml::getTag($start_model);
                if (gettype($start_model) === 'array')
                {
                    foreach ($start_model as $child)
                    {
                        $tags .= ' ' . $this->getTagsRecursive($child, $with_array, $i + 1);
                    }
                    $setted = false; //ostatak niza je odradjen rekurzijom
                }
            }
            else
            {
                $setted = false;
            }
        }
        return $tags;
    }

    /**
     * Renderuje pogled jednog modela (moze da sadrzi i relacije)
     * @param SIMAActiveRecord $model model koji se renderuje
     * @param string $view ime pogleda
     * @param array $settings dodatna podesavanja (processOutput)
     * @return HTML Izrenderovan pogled
     * @throws Exception
     */
    public function renderModelView(SIMAActiveRecord $model, $view = 'default', $settings = array())
    {
        if(!isset($model))
        {
            throw new Exception(Yii::t('SIMAController', 'ModelNotSet'));
        }
        $model_id = $model->getPrimaryKey();
        //za obican id, vratice cist broj, samo u string-u, za kombinovan, vratice JSON
        $model_id_str = CJSON::encode($model_id); 
        
        $cache_key = __METHOD__.'_'.$model_id_str.'_'.$view;
     
        $behaviors = $model->behaviors();
        $Model = get_class($model);

        $processOutput = (isset($settings['processOutput']) && $settings['processOutput']);

        //kada se pretrazuju podaci sa SIMADbCriteria, sve tabele se join-uju i onda nestanu neki podaci iz relacija 
        //primer -> u imenuku nestanu kontakti koji ne zadovoljavaju uslov pretrage
        //** nema vise potrebe za ovim - potreba je bila dok su se joinnovale tabele u SIMADbCriteria**/
//        if (!isset($settings['refresh']) || $settings['refresh']==true )
//            $model->refresh();

        $origin = $Model;
        $module_name = $model->moduleName();
        if (!isset($behaviors['GUI']))
        {
            throw new SIMAException("Modelu '$Model' nije zadato GUI ponasanje - fali GUI fajl");
        }

        $views = $model->modelViews();
        if (!isset($views[$view]) && !in_array($view, $views))
        {
            throw new SIMAModelViewNotFound($Model, $view);
        }
        
        $view_path = '';

        $params = []; //model mora da se zada posle
        //Sasa A. - dodatna provera in_array se vrsi jer je moguce da se u nekom modelu definise view kao value niza (definicija nije npr. 'neki_view'=>[] nego samo 'neki_view')
        //onda dolazi do problema jer ako je u SIMAActiveRecordGUI definisan isti view kao 'neki_view'=>[], racunace se ovaj iz SIMAActiveRecordGUI, nece ga preisati view iz deteta
        //da ne bi menjao mergeovanje view-a jer ima dosta u kodu, uvodi se pravilo da u SIMAActiveRecordGUI view mora da se definise kao 'neki_view'=>[], tj da ime view-a bude key u nizu
        if (isset($views[$view]) && !in_array($view, $views))
        {
            if (in_array('cache',$views[$view]) || isset($views[$view]['cache']))
            {
                if (isset($views[$view]['cache']) && in_array('user', $views[$view]['cache']))
                {
                    $cache_key .= '_'.Yii::app()->user->id;
                }
                $cache_result = Yii::app()->simacache->get($cache_key);
                if ($cache_result!==FALSE)
                {
                    return $cache_result;
                }
            }
            if (isset($views[$view]['scopes']))
            {
                $_model = $Model::model();
                $scopes = $views[$view]['scopes'];
                if (gettype($scopes) === 'string')
                {
                    $scopes = array($scopes); //ukoliko je samo strig, prebaci ga u niz da bi se dalje obradjivao na isti nacin
                }
                if (gettype($scopes) === 'array')
                {
                    foreach ($scopes as $scope)
                    {
                        $_model->$scope();
                    }
                }
                $model = $_model->findByPk($model_id);
            }

            
            if (isset($views[$view]['params_function']))
            {
                $params_function = $views[$view]['params_function'];
    //            if (get_type($params_function)!=='function')
    //                throw new Exception("Modelu '$Model' nije zadata parametarska funkcija '$params_function'");
                $ret = $model->$params_function($model);
                if (gettype($ret) !== 'array')
                {
                    throw new Exception("povratna vrednost u funkciji $params_function u modelu $Model GUI nije niz");
                }
                $params += $ret;
            }

            
            if (isset($views[$view]['origin']))
            {
                $origin = $views[$view]['origin'];
                $module_name = $views[$view]['origin']::model()->moduleName();
            }
            
            if (isset($views[$view]['view_path']))
            {
                $view_path = $views[$view]['view_path'];
            }
            
        }
        $params['model'] = $model;
        $params['params'] = isset($settings['params'])?$settings['params']:[];
        
        if(empty($view_path))
        {
            $view_path = $module_name . '.views.modelViews.' . $origin . '.' . $view;
        }

        //renderovanje izgleda
        if ($this->getViewFile($view_path) === false)
        {
            throw new SIMAModelViewNotFound($Model, $view);
        }
        $content = $this->renderPartial(
                $view_path, $params, true
        );

        $content .= $model->getAdditionalModelViews($view);

//        $tags = SIMAHtml::getTag($model);

        /**
         * uzimanje svih tagova koji se pojavljuju na pogledu i ubacivanje u class
         * da bih se i ti pogledi refreshovali ukoliko dodje do promene izgleda
         */
        $class = 'sima-model-view ' . SIMAHtml::getTag($model);
        if (isset($views[$view]['with']))
        {
            $withs = $views[$view]['with'];
            if (gettype($withs) === 'string')
            {
                $withs = array($withs); //ukoliko je samo strig, prebaci ga u niz da bi se dalje obradjivao na isti nacin
            }
            if (gettype($withs) === 'array')
            {
                foreach ($withs as $with)
                {
                    $with_array = explode('.', $with);
                    $class .= ' ' . $this->getTagsRecursive($model, $with_array);
                }
            }
        }
        else
        {
            $withs = [];
        }

        if (isset($settings['class']))
        {
            $class .= ' '.$settings['class'];
        }

        if (isset($views[$view]['class']))
        {
            $class .= ' ' . $views[$view]['class'];
        }
        $style = (isset($views[$view]['style'])) ? $views[$view]['style'] : '';
        $wrapper = (isset($views[$view]) && is_array($views[$view]) && array_key_exists('html_wrapper',$views[$view])) ? $views[$view]['html_wrapper'] : 'div';
        
        if(is_null($wrapper))
        {
            $html = $content;
        }
        else
        {
            //renderovanje okvira - sluzi iskljucivo 
            $html = $this->renderPartial('base.views.model.view', array(
                'content' => $content,
                'tag' => SIMAHtml::getTag($model),
                'class' => $class,
                'style' => $style,
                'model' => $Model,
                'model_id' => $model_id_str,
                'model_view' => $view,
                'wrapper' => $wrapper,
                'display_name' => $model->DisplayName,
                'params' => $params['params'],
                'additional_classes'=>isset($settings['class'])?$settings['class']:''
            ), true, $processOutput);
        }

        if (isset($views[$view]) && (in_array('cache',$views[$view]) || isset($views[$view]['cache'])))
        {
            Yii::app()->simacache->set($cache_key, $html, [[$model,$withs]], true);
        }

        return $html;
    }

    /**
     * 
     * @param type $model
     * @param type $tab_code
     * @param type $settings
     * @param type $processOutput
     * @return type
     * @throws SIMAWarnExceptionModelTabNotFound
     */
    public function renderModelTab($model, $tab_code, $settings = array(), $processOutput = false)
    {
        $behs = $model->behaviors();
        if (!isset($behs['Tabs']))
        {
            $beh = $model->TabsBehavior;
            $model->attachBehavior('Tabs', new $beh());
        }
        if (isset($settings['local_params']))
        {
            $local_params = $settings['local_params'];
        }
        else
        {
            $local_params = [];
        }
        $params = $model->tabParams($tab_code, $local_params);
        if (isset($settings['selector']))
        {
            $params['selector'] = $settings['selector'];
        }
        if (isset($settings['params']))
        {
            $params['params'] = $settings['params'];
        }

        if (isset($params['html']))
        {
            if ($processOutput)
            {
                return Yii::app()->controller->processOutput($params['html']);
            }
            else
            {
                return $params['html'];
            }
        }
        else
        {
            $module_name = $model->moduleName();
            if (isset($settings['module_origin']))
            {
                $module_name = $settings['module_origin'];
            }

            $origin = $model->tabOrigin($tab_code);

            $modelclass = get_class($model);

            if ($origin != null)
            {
                $modelclass = $origin;

                $module_name = $origin::model()->moduleName();
            }

            return $this->renderPartial(
                $module_name . '.views.modelTabs.' . $modelclass . '.' . str_replace('.', '_', $tab_code), $params, true, $processOutput
            );
        }
    }
        
    /**
     * This is the action to handle external exceptions.
     */
    public function actionError()
    {
        if (Yii::app()->errorHandler->error)
        {
            $error = Yii::app()->errorHandler->error;
            if (Yii::app()->request->isAjaxRequest)
            {

                echo $error['message'] . 'aaaa';
            }
            else
            {
                $this->render('../site/error', $error);
            }
        }
    }
   
    /**
     * 
     * @param type $param_name
     * @param type $throw_exception
     * @return type
     * @throws Exception
     */
    public function filter_post_input($param_name, $throw_exception=true)
    {
        $input_array = filter_input_array(INPUT_POST);
        
        return $this->filter_input($input_array, $param_name, $throw_exception);
    }

    protected function filter_get_input($param_name, $throw_exception=true)
    {
        $input_array = filter_input_array(INPUT_GET);
        
        return $this->filter_input($input_array, $param_name, $throw_exception);
    }
    
    protected function filter_input($input_array, $param_name, $throw_exception=true)
    {
        $result = null;

        if(empty($input_array) || !array_key_exists($param_name, $input_array))
        {
            if($throw_exception)
            {
                throw new SIMAException(Yii::t('BaseModule.Common', 'Parameter "{parameter}" not set', [
                    '{parameter}'=>$param_name
                ]));
            }
        }
        else
        {
            $result = $input_array[$param_name];
        }
        
        return $result;
    }
    
    public function setEventHeader()
    {
        $is_ajax_long = filter_input(INPUT_GET, 'is_ajax_long');
        if($is_ajax_long !== 'TRUE')
        {
            $error_message = 'not ajax long';
            error_log(__METHOD__.' - '.$error_message);
            throw new Exception($error_message);
        }
        
        ExceptionThrower::Start(E_ALL & ~E_STRICT & ~E_DEPRECATED & ~E_NOTICE, 'SIMAAjaxLongException');
        $event_id = $this->filter_get_input('event_id');
        $data = Yii::app()->sima_session->offsetGet($event_id);
        ob_end_clean();
        header('Content-Type: text/event-stream');
        header('Cache-Control: no-cache');
        $this->isAjaxLong = true;
        
        return $data;
    }
    
    public function sendEvent($eventName, $data=[])
    {
        echo "event: $eventName\n";
        echo "data: ".CJSON::encode($data);
        echo "\n\n\n";

        ob_flush();
        flush();
    }
    
    public function sendUpdateEvent($percent, $data=[])
    {
        $new_data = $data;
        $new_data['percent'] = $percent;
        $this->sendEvent('update',$new_data);
    }
    
    public function sendEndEvent($data=[], $update_models=[])
    {
        Yii::app()->performAfterWork();
        
        $update_model_tags = $this->mergeUpdateModels($update_models);
        
        /*if(Yii::app()->configManager->get('base.develop.sma_events_over_eventrelay') !== true && Yii::app()->hasModule('mobile') && !empty($update_model_tags))
        {
            MobileComponent::sendDataToAllUsersDevices([
                'event' => 'UPDATE_VUE_MODELS',
//                'data' => implode(',', $update_model_tags)
                'data' => $update_model_tags
            ]);
        }*/
        
        $data['processOutput'] = $this->processOutput('');
        $data['actions'] = $this->_actions;
        $data['notes'] = $this->_notes;
        $data['update_models'] = $update_model_tags;
        $this->sendEvent('end',$data);
        ExceptionThrower::Stop();
        Yii::app()->end();
    }
    
    public function sendFailureEvent($data=[])
    {
        Yii::app()->performAfterWork();
        
        //Sasa A. - Stavljen je ob_clean jer mozda imamo neki echovan html pre bacanja greske i onda ce i on da se flushuje, pa ce ajax long da se poremeti
        //i automatski ce baciti error event, nece otici u failure. Ovako ocistimo sve sto smo echovali pre i samo se baci failure event. 
        //Ovo je takodje uradjeno i za sendFatalFailureEvent
        ob_clean();
        $this->sendEvent('failure',$data);
        Yii::app()->end();
    }
    
    public function sendFatalFailureEvent($data=[])
    {
        /**
         * za razmisliti: ne sme nista vise da se radi kada je fatal failure
         */
        Yii::app()->performAfterWork();
        
        ob_clean();
        $this->sendEvent('fatal_failure',$data);
        Yii::app()->end();
    }
    
    public function isApiAction()
    {        
        $action = filter_input(INPUT_GET, 'r');
        $apiFunctions = Yii::app()->params['apiFunctions'];
        
        if(in_array($action, $apiFunctions))
        {
            return true;
        }
        
        return false;
    }
    
    public function renderContentLayout($title, $panels, $params=[])
    {
        $processOutput = isset($params['processOutput'])?$params['processOutput']:true;
        $id = isset($params['id'])?$params['id']:SIMAHtml::uniqid();
        $class = 'sima-page-wrap sima-layout-panel';
        if (isset($params['class']))
        {
            $class .= ' '.$params['class'];
        }
        $title_name_info_box = null;
        if(isset($params['title_name_info_box']))
        {
            $title_name_info_box = $params['title_name_info_box'];
        }
            
        if (is_string($title))
        {
            $title = [
                'name' => $title
            ];
        }
        if (!isset($title['name']))
        {
            throw new SIMAException('renderContentLayout - nije zadat title name');
        }
        $title_name = $title['name'];
        $title_class = isset($title['class'])?$title['class']:'';
        $title_width = isset($title['title_width'])?$title['title_width']:'';
        $title_options_position_type = isset($title['options_position_type'])?'_'.$title['options_position_type']:'_right';
        $title_options_class = isset($title['options_class'])?' '.$title['options_class']:'';
        $title_options_width = isset($title['options_width'])?' '.$title['options_width']:'';
        $title_options = isset($title['options'])?$title['options']:[];
        
        foreach ($panels as $key=>$panel) 
        {
            $panels[$key]['proportion'] = isset($panel['proportion'])?$panel['proportion']:(1/(count($panels))); 
            $panels[$key]['min_width']  = isset($panel['min_width']) ?$panel['min_width'] :200; 
            $panels[$key]['max_width']  = isset($panel['max_width']) ?$panel['max_width'] :2000; 
            $panels[$key]['class']  = isset($panel['class']) ? $panel['class']: ''; 
        }
        
        $use_resizable_panel = count($panels) > 1;
        
        $html = $this->renderPartial('base.views.layouts.content', [
            'id'=>$id,
            'class'=>$class,
            'title'=>$title,            
            'panels'=>$panels,
            'use_resizable_panel'=>$use_resizable_panel,
            'title_name'=>$title_name,
            'title_class'=>$title_class,
            'title_width'=>$title_width,
            'title_options_position_type'=>$title_options_position_type,
            'title_options_class'=>$title_options_class,
            'title_options_width'=>$title_options_width,
            'title_options'=>$title_options,
            'title_name_info_box' => $title_name_info_box
        ], true, $processOutput);
        
        return $html;
    }
    
    public function renderDefaultModelView($model, $params=[])
    {
        $tabs_params = [];
        if(isset($params['tabs_params']))
        {
            $tabs_params = $params['tabs_params'];
        }
        else
        {
            $tabs_params = [
                'default_selected' => 'comments'
            ];
        }
        $tabs_params['list_tabs_model'] = $model;
        
        $html = $this->renderPartial('base.views.model.default_model_view',array(
            'model' => $model,
            'tabs_params' => $tabs_params
        ),true,false);
        
        return $html;
    }
    
    public function redirect($url, $terminate=true, $statusCode=302)
    {
        Yii::app()->performAfterWork();
        
        parent::redirect($url, $terminate, $statusCode);
    }
}