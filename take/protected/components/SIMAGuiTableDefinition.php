<?php

class SIMAGuiTableDefinition
{
    public static function getGuiTableDefinition($moduleName, $index)
    {
        $guiTableDefinition = null;
        if($moduleName === 'application')
        {
            switch($index)
            {
                default:
                    throw new SIMAGuiTableIndexNotFound($moduleName, $index);
            }
        }
        else
        {
            try
            {
                $module = SIMAModule::getModuleByName(Yii::app(), $moduleName);

                if(empty($module))
                {
                    throw new SIMAExceptionNoModule($moduleName);
                }

                $guiTableDefinition = $module->getGuiTableDefinition($index);
            }
            catch(SIMAExceptionNoModule $ex)
            {
                Yii::app()->errorReport->createAutoClientBafRequest($ex->getMessage());

                throw new SIMAWarnException(Yii::t('SIMAGuiTable.Translation', 'ProblemInGeneratingTable'));
            }
        }
        return $guiTableDefinition;
    }
    
    public static function getOldGuiTableDefinition($index) 
    {
        switch ($index)
        {
//            case '12': return [ /// MilosJ 20170823: nije nadjeno da se igde koristi
//                    'label' => Yii::t('BaseModule.GuiTable', 'DocumentTypes'),
//                    'settings' => array(
//                        'model' => DocumentType::model(),
//                        'add_button' => true
//                    )
//                ];
//            case '19': return [ /// MilosJ 20170824: nije nadjeno da se igde koristi
//                    'label' => 'Tabela Osnovna sredstva',
//                    'settings' => array(
//                        'model' => FixedAsset::model(),
//                        'add_button' => true
//                    )
//                ];
//            case '28': return [ /// MilosJ 20170824: nije nadjeno da se igde koristi
//                    'label' => 'Ponude',
//                    'settings' => array(
//                        'model' => JobBid::model(),
//                        'add_button' => true
//                    )
//                ];
//            case '54': return [ /// MilosJ 20170824: nije nadjeno da se igde koristi
//                    'label' => 'Spisak dana',
//                    'settings' => array(
//                        'model' => Day::model(),
//                        'add_button' => true,
//                        'fixed_filter' => array(
//                            'order_by' => 'day_date DESC',
//                        ),
//                    ),
//                ];
//            case '56': return [ /// MilosJ 20170824: nije nadjeno da se igde koristi
//                    'model' => 'VehicleTravelOrder',
//                    'label' => 'Putni nalozi',
//                    'settings' => array(
//                        'model' => VehicleTravelOrder::model(),
//                        'add_button' => Yii::app()->user->checkAccess('travelOrderMaster') ? true : false,
//                        'fixed_filter' => array(
//                            'status' => 'odobren'
//                        )
//                    )
//                ];
//            case '57': return [ /// MilosJ 20170824: nije nadjeno da se igde koristi
//                    'label' => Yii::t('JobsModule.Meeting','Meetings'),
//                    'settings' => array(
//                        'model' => Meeting::model(),
//                        'add_button' => array(
//                            'init_data' => array(
//                                'Meeting' => array(
//                                    'scorer_id' => Yii::app()->user->id
//                                )
//                            ),
//                            'formName' => 'default',
//                        ),
//                        'fixed_filter' => [
//                            'order_by' => 'begin DESC'
//                        ]
//                    )
//                ];
//            case '62': return [//Knjiga agregata /// MilosJ 20170824: nije nadjeno da se igde koristi
//                    'label' => 'Kanta - tabela',
//                    'settings' => array(
//                        'model' => File::model(),
//                        'fixed_filter' => array(
//                            'recycle' => true
//                        ),
//                    ),
//                ];
//            case '63': return [ /// MilosJ 20170824: nije nadjeno da se igde koristi
//                    'label' => 'Kanta - tabela',
//                    'settings' => array(
//                        'model' => File::model(),
//                        'fixed_filter' => array(
//                            'recycle' => true
//                        ),
//                    ),
//                ];
//            case '65': return [ /// MilosJ 20170824: nije nadjeno da se igde koristi
//                    'label' => Yii::t('JobsModule.Training','Trainings'),
//                    'settings' => array(
//                        'model' => Training::model(),
//                        'add_button' => array(
//                            'formName' => 'default'
//                        )
//                    )
//                ];
//            case '72': return [ /// MilosJ 20170824: nije nadjeno da se igde koristi
//                    'label' => 'E-mail nalozi',
//                    'settings' => array(
//                        'model' => EmailAccount::model(),
//                        'add_button' => array(
//                            'formName' => 'default'
//                        ),
//                    )
//                ];
//            case '78': return [ /// MilosJ 20170824: nije nadjeno da se igde koristi
//                    'label' => 'Odsustva',
//                    'settings' => array(
//                        'model' => Absence::model(),
//                        'add_button' => true
//                    )
//                ];
//            case '86': return [ /// MilosJ 20170825: nije nadjeno da se igde koristi
//                    'label' => 'Objekti',
//                    'settings' => array(
//                        'model' => BuildingStructure::model(),
//                        'add_button' => true
//                    )
//                ];
//            case '87': /// MilosJ 20170823: nije nadjeno da se igde koristi
//                $curr_year = Year::get(date('Y'));
//                return [
//                    'label' => 'Bilans',
//                    'settings' => array(
//                        'model' => Account::model(),
//                        'select_filter' => array(
//                            'year' => array(
//                                'ids' => $curr_year->id
//                            )
//                        )
//                    )
//                ];
//            case '88': return [ /// MilosJ 20170823: nije nadjeno da se igde koristi
//                    'label' => 'Odsustva - Plaćeno odsustvo',
//                    'settings' => array(
//                        'model' => Absence::model(),
//                        'add_button' => array(
//                            'init_data' => array(
//                                'Absence' => array(
//                                    'type' => 2
//                                )
//                            ),
//                            'formName' => 'default',
//                        ),
//                        'fixed_filter' => array(
//                            'type' => 2
//                        )
//                    ),
//                ];
//            case '116': return [ /// MilosJ 20170824: nije nadjeno da se igde koristi
//                    'label' => 'Reference',
//                    'settings' => array(
//                        'model' => Reference::model(),
//                        'add_button' => true
//                    ),
//                ];
//            case '119': return [ /// MilosJ 20170823: nije nadjeno da se igde koristi
//                    'label' => 'Finansijske godine',
//                    'settings' => array(
//                        'model' => AccountingYear::model(),
//                        'add_button' => true,
//                    ),
//                ];
//            case '127': return [ /// MilosJ 20170824: nije nadjeno da se igde koristi
//                    'label' => 'Zaposleni',
//                    'settings' => array(
//                        'model' => Employee::model(),
//                        'columns_type' => 'in_finances',
//                    ),
//                ];
//            case '130': return [ /// MilosJ 20170823: nije nadjeno da se igde koristi
//                    'label' => 'Vrste troška',
//                    'settings' => [
//                        'add_button' => true,
//                        'model' => CostType::model(),
//                    ],
//                ];
//            case '182': return [ /// MilosJ 20170824: nije nadjeno da se igde koristi
//                    'label' => Yii::t('MainMenu', 'UnsignedWorkContracts'),
//                    'settings' => array(
//                        'model' => WorkContract::model(),
//                        'fixed_filter' => [
//                            'file' => [
//                                'filing' => [
//                                    'returned' => false
//                                ]
//                            ]
//                        ]
//                    )
//                ];
//            case '186': return [ /// MilosJ 20170824: nije nadjeno da se igde koristi
//                    'label' => Yii::t('MainMenu', 'PrivateAccounts'),
//                    'settings' => array(
//                        'model' => EmailAccount::model(),
//                        'add_button' => true,
//                        'columns_type' => 'default',
//                        'fixed_filter' => array(
//                            'filter_scopes' => ['onlyPrivate']
//                        ),
//                    ),
//                ];
            default:
                throw new SIMAGuiTableIndexNotFound('getOldGuiTableDefinition', $index);        
        }
    }

}
