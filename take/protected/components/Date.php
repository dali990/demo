<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Date
{
    public static function generateDaysBetweenDates($start_date, $end_date)
    {
        $days = array();
        
        $end_date_str = strtotime($end_date);
        $curdate_str = strtotime($start_date);
        while ($curdate_str <= $end_date_str)
        {
            $date = date ("d.m.Y.", $curdate_str);
            $userDate = SIMAHtml::DbToUserDate($date);
            
            $days[] = $userDate;
            
            $curdate_str = strtotime("+1 day", $curdate_str);
        }
        
        return $days;
    }
    
    public static function getNextDate($date)
    {
        $date = strtotime($date);
        $date = strtotime("+1 day", $date);
        $date = date ("d.m.Y.", $date);
        $date = SIMAHtml::DbToUserDate($date);
        
        return $date;
    }
    
    public static function geDateEnd($date)
    {
        $date = Date::getNextDate($date);
        $date = strtotime($date);
        $date = strtotime("-1 second", $date);
        $date = date ("d.m.Y. H:i:s", $date);
        $date = SIMAHtml::DbToUserDate($date, true);
        
        return $date;
    }
}
