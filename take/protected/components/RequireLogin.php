<?php
class RequireLogin extends CBehavior
{
    public function attach($owner)
    {
        $owner->attachEventHandler('onBeginRequest', [$this, 'handleBeginRequest']);
        $owner->attachEventHandler('onEndRequest', [$this, 'handleEndRequest']);
    }
    
    public static function handleFatalPhpError()
    {
        $last_error = error_get_last();
        
        if($last_error['type'] === E_ERROR) 
        {
            $get_array = filter_input_array(INPUT_GET);
            $post_array = filter_input_array(INPUT_POST);

            error_log(__METHOD__.' - handling');
            error_log(__METHOD__.' - $get_array: '.CJSON::encode($get_array));
            error_log(__METHOD__.' - $post_array: '.CJSON::encode($post_array));
            error_log(__METHOD__.' - $last_error: '.CJSON::encode($last_error));
        }
    }

    public function handleBeginRequest($event)
    {
        $_trace_cat = 'SIMA.RequireLogin.BeginRequest';
        Yii::trace('start',$_trace_cat);
        /**
         * dohvata sve module da bi im se registrovali behaviori
         */
        
        $apiFunctions = Yii::app()->params['apiFunctions'];
                
        $allowed_pages = array_merge(array(
            'user/login',
            'user/changePassword',
            'download',
            'addressbook/listEmails', /// MilosJ: koristi roundcube
            'BB/status',
            'BB/addDispatch',
            'BB/addOrder',
            'BB/setFinished'
            ), $apiFunctions);
        
        $action = filter_input(INPUT_GET, 'r');
        
        if($action === 'setpp/testApi/test')
        {
            error_log(__METHOD__.' - starttime: '.microtime(true));
        }
        
        if  (
                (
                    Yii::app()->user->isGuest //gost
                ) 
                && 
                !(isset($action) && in_array($action,$allowed_pages)) //nisu specijalne stranice
            ) 
        {
            $network_outage = $this->getNetworkOutage();
            if($network_outage === true && $action === 'site/keepAlive')
            {
                /// MilosJ: do daljnjeg se ne salje dok se ne razmotri dobro sta/kako reagovati
//                $autobaf_message = 'korisnik bio bez mreze';
//                error_log(__METHOD__.' - $autobaf_message: '.$autobaf_message);
//                Yii::app()->errorReport->createAutoClientBafRequest($autobaf_message);
                
                $controller = new SIMAController('SIMAController');
                $controller->respondOK();
            }
            else
            {
//                if(Yii::app()->hasModule('mobile') && MobileComponent::isMobileAppRequest())
                if(MobileComponent::isMobileAppRequest())
                {
                    $autobafreq_message = 'mobile request is guest'
                                        .' -</br> INPUT_GET: '.SIMAMisc::toTypeAndJsonString(filter_input_array(INPUT_GET))
                                        .' -</br> INPUT_POST: '.SIMAMisc::toTypeAndJsonString(filter_input_array(INPUT_POST));
                    error_log(__METHOD__.' - $autobafreq_message: '.$autobafreq_message);
                    Yii::app()->errorReport->createAutoClientBafRequest($autobafreq_message, 3193, 'AutoBafReq isGuest');
                    
                }
                Yii::app()->user->loginRequired();
            }
        }
        else if (!(isset($action) && in_array($action,$allowed_pages)))
        {
            $is_idle_since = $this->getIsIdleSince();
            
            $session = Yii::app()->user->model->curr_session;
                $session->last_seen = 'NOW()';
            if (!isset($action) || $is_idle_since === true)
            {
                if(Yii::app()->params['handleBeginRequest_idle_since_action_log'] === true && !empty($action))
                {
                    error_log(__METHOD__.' - $action: '.$action);
                }
                $session->idle_since = new CDbExpression('NOW()');
            }
            $session->update();

            if($session->session_type === Session::$TYPE_SIMAweb)
            {
                if (isset($_POST['YIISIMASESSION']))
                    Yii::app()->sima_session->id = $_POST['YIISIMASESSION'];
                else if (isset($_GET['YIISIMASESSION']))
                    Yii::app()->sima_session->id = $_GET['YIISIMASESSION'];
                else 
                {
                    Yii::app()->sima_session->id = WebTabSession::createNew('RequireLogin')->id;
                }
                
                Yii::app()->sima_session->checkPHPSESSION($_COOKIE['PHPSESSID']);
            }
        } 
        else if (isset($action) && in_array($action,$allowed_pages))
        {
            if(in_array($action,$apiFunctions))
            {
                $this->validateApiCall();
            }
            Yii::app()->session['allowed_pages'] = 'allowed_pages';
        }
        else
        {
            throw new Exception('nije zadat YIISIMASESSION');
        }
                
        if(isset(Yii::app()->params['handle_php_fatal_error_shutdown']) 
                && Yii::app()->params['handle_php_fatal_error_shutdown'] === true)
        {
            register_shutdown_function('RequireLogin::handleFatalPhpError');
        }
        
//        if(Yii::app()->params['request_log'] === true)
//        {
//            Yii::app()->sima_session['time'] = microtime(true);
//        }
                
        Yii::trace('end',$_trace_cat);
        
    }

    public function handleEndRequest($event)
    {
        try
        { 
            $_trace_cat = 'SIMA.RequireLogin.handleEndRequest';
            Yii::trace('start',$_trace_cat);
            unset(Yii::app()->session['allowed_pages']);
            Yii::trace('end',$_trace_cat);
            
//            if(Yii::app()->params['request_log'] === true)
//            {
//                if (isset(Yii::app()->sima_session->id) && isset(Yii::app()->sima_session['time']) && !Yii::app()->user->isGuest)
//                {
//                    $used_time = microtime(true) - Yii::app()->sima_session['time'];
//    //                if ($used_time>0.5)
//                    {
//                        $log = new RequestLog();
//                        $log->user_id = Yii::app()->user->id;
//                        $log->action = isset($_GET['r'])?$_GET['r']:'index.php';
//                        $log->params = $_GET;
//                        $log->used_time = $used_time;
//                        $log->save();
//                    }
//                    
//                }
//            }
                        
            $get_array = filter_input_array(INPUT_GET);
            $post_array = filter_input_array(INPUT_POST);

            $log_messages = array_merge(
                    $this->checkInputVarsMemoryLimit($get_array, $post_array)
                    , 
                    $this->checkMemoryLimit()
                    ,
                    $this->checkExecutionTime()
                );
            
            if(count($log_messages) > 0)
            {                
                $imploded_messages = implode('; ', $log_messages);
                
                $log = new RequestLog();
                $log->user_id = Yii::app()->user->id;
                $log->action = isset($get_array['r'])?$get_array['r']:'index.php';
                $log->params = CJSON::encode($get_array);
                $log->post_params = CJSON::encode($post_array);
                $log->message = $imploded_messages;                
                $log->save();
            }
            
            if(filter_input(INPUT_GET, 'r') === 'setpp/testApi/test')
            {
                error_log(__METHOD__.' - endtime: '.microtime(true));
            }
        }
        catch (Exception $e)
        {            
            error_log(__METHOD__.' - Exception: '.$e->getMessage());
            throw $e;
        }
    }
    
    private function checkInputVarsMemoryLimit(array $get_array=null, array $post_array=null)
    {
        $log_messages = [];

        if(Yii::app()->params['log_max_input_vars_php_warning'] === true)
        {
            $maxInputVars = ini_get("max_input_vars");

            if(!empty($post_array) && count($post_array, COUNT_RECURSIVE) >= $maxInputVars)
            {
                $log_messages[] = Yii::t('RequireLogin', 'InputVarsPhpWarningPostArray');
            }

            if(!empty($get_array) && count($get_array, COUNT_RECURSIVE) >= $maxInputVars)
            {
                $log_messages[] = Yii::t('RequireLogin', 'InputVarsPhpWarningGetArray');
            }
        }
        
        return $log_messages;
    }
    
    private function checkMemoryLimit()
    {
        $log_messages = [];
        
        if(Yii::app()->params['log_memory_limit_php_error'] === true)
        {
            $memory_usage = memory_get_usage(true);
            $memory_limit = ini_get('memory_limit');

            if (preg_match('/^(\d+)(.)$/', $memory_limit, $matches)) {
                if ($matches[2] == 'M') {
                    $memory_limit = $matches[1] * 1024 * 1024; // nnnM -> nnn MB
                } else if ($matches[2] == 'K') {
                    $memory_limit = $matches[1] * 1024; // nnnK -> nnn KB
                }
            }

            if($memory_usage >= $memory_limit)
            {
                $log_messages[] = Yii::t('RequireLogin', 'MemoryLimitPhpError');
            }
        }
        
        return $log_messages;
    }
    
    private function checkExecutionTime()
    {
        $log_messages = [];
                
        if(Yii::app()->params['log_max_execution_time_php_error'] === true)
        {
            if(isset($_SERVER['REQUEST_TIME_FLOAT']))
            {
                $execution_time = floor(microtime(true) - $_SERVER['REQUEST_TIME_FLOAT']);
                $execution_time_limit = ini_get('max_execution_time');
                
                if($execution_time >= $execution_time_limit)
                {
                    $log_messages[] = Yii::t('RequireLogin', 'MaxExecutionTimePhpError');
                }
            }
        }
        
        return $log_messages;
    }
    
    private function validateApiCall()
    {
//        $token = filter_input(INPUT_GET, 'token');
//        if(empty($token))
//        {
//            throw new Exception('NotSetToken');
//        }
//        
//        $server_array = filter_input_array(INPUT_SERVER);
////        $user_port = $server_array['REMOTE_ADDR'];
//        
//        if(!isset($server_array['REMOTE_ADDR']))
//        {
//            throw new Exception('NotSetREMOTE_ADDR');
//        }
//        $remote_address = $server_array['REMOTE_ADDR'];
//        
//        if(!isset($server_array['SERVER_PORT']))
//        {
//            throw new Exception('NotSetSERVER_PORT');
//        }
//        $server_port = $server_array['SERVER_PORT'];
//        
//        $criteria = new SIMADbCriteria([
//            'Model' => 'ConnectedSIMASystem',
//            'model_filter' => [
//                'ip_address' => $remote_address,
//                'port' => $server_port,
//                'token' => $token
//            ]
//        ]);
//        $count = ConnectedSIMASystem::model()->count($criteria);
//        if($count === 0)
//        {
//            throw new Exception('InvalidRemoteHost');
//        }
////        error_log(__METHOD__.' - $user_address: '.$user_address);
////        foreach($server_array as $key => $value)
////        {
////            if (strpos($key,'PORT') !== false
////                    || strpos($key,'port') !== false)
////            {
////                error_log(__METHOD__.' - $key: '.CJSON::encode($key).' => $value: '.CJSON::encode($value));
////            }
////        }
////        error_log(__METHOD__.' - $_SERVER: '.CJSON::encode($_SERVER));
    }
    
    private function getIsIdleSince()
    {
        $is_idle_since = filter_input(INPUT_POST, 'is_idle_since');
        if(is_null($is_idle_since))
        {
            $is_idle_since = filter_input(INPUT_GET, 'is_idle_since');
        }
        if(is_null($is_idle_since))
        {
            $is_idle_since = true;
        }
        
        $is_idle_since_bool = filter_var($is_idle_since, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
        if(is_null($is_idle_since_bool))
        {
            $is_idle_since_bool = true;
        }
        
        return $is_idle_since_bool;
    }
    
    private function getNetworkOutage()
    {
        $network_outage = filter_input(INPUT_POST, 'network_outage');
        if(is_null($network_outage))
        {
            $network_outage = filter_input(INPUT_GET, 'network_outage');
        }
        if(is_null($network_outage))
        {
            $network_outage = false;
        }
        
        $network_outage_bool = filter_var($network_outage, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
        if(is_null($network_outage_bool))
        {
            $network_outage_bool = false;
        }
        
        return $network_outage_bool;
    }
}
