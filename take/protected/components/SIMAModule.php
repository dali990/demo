<?php

class SIMAModule extends CWebModule
{
//    public function init()
//    {        
////        $this->registerPermanentBehaviors();
////        $this->mapClasses();
////        $this->includeConfFiles();
////        
////        if(Yii::app()->isConsoleApplication() === true)
////        {
////            $this->includeCommands();
////        }
//    }
    
    /**
     * vraca niz gde je svaki element oblika $key=>$value
     * gde je 
     *  $key - model
     *  $value - behavior(s) - array
     * 
     * primer:
     *  [ 'File' => 'FileBehavior', 'Theme' => ['ThemeBehavior'] ]
     * 
     * @return type
     */
    protected function permanentBehaviors(){
        return [];
    }
    protected function mapClasses(){}
    protected function simaActivities(){
        return [];
    }
    protected function maintainOneMinuteCommands(){
        return [];
    }
    protected function maintainFiveMinuteCommands(){
        return [];
    }
    
    private function registerPermanentBehaviors(){
        $permanentBehaviors = $this->permanentBehaviors();
        foreach($permanentBehaviors as $key => $value)
        {
            if(!is_array($value))
            {
                $value = [$value];
            }
            
            foreach($value as $val)
            {
                $key::registerPermanentBehavior($val);
            }
        }
    }
    
    private function registerSimaActivities()
    {
        $sima_activities = $this->simaActivities();
        foreach($sima_activities as $sima_activity)
        {
            Yii::app()->activityManager->registrationActivity($sima_activity);
        }
    }
    
    public static function includeVueComponentsModulePath($dirname)
    {
        if (Yii::app()->isWebApplication())
        {
            $s = DIRECTORY_SEPARATOR;
            foreach(self::includeVueComponentsModulePath_rglob($dirname.$s.'vueGUI'.$s.'*.js') as $filepath)
            {
                Yii::app()->clientScript->registerScript($filepath, file_get_contents($filepath), CClientScript::POS_READY);
            }
            
            foreach(self::includeVueComponentsModulePath_rglob($dirname.$s.'vueGUI'.$s.'*.php') as $filepath)
            {
                Yii::app()->clientScript->registerScript($filepath, include($filepath), CClientScript::POS_END);
            }
            
            foreach(self::includeVueComponentsModulePath_rglob($dirname.$s.'vueGUI'.$s.'*.css') as $filepath)
            {
                Yii::app()->clientScript->registerCss($filepath, file_get_contents($filepath));
            }
            
            foreach(self::includeVueComponentsModulePath_rglob($dirname.$s.'vueGUI'.$s.'*.scss') as $filepath)
            {
                Yii::app()->sass->register($filepath);
            }
        }
        else
        {
            /// moze da se pozove tokom azuriranja
//            error_log(__METHOD__.' - ne bi smelo da se pozove - $dirname: '.SIMAMisc::toTypeAndJsonString($dirname));
        }
    }
    
    /**
     * source: https://stackoverflow.com/a/17161106
     * 
     * @param type $pattern
     * @param type $flags
     */
    private static function includeVueComponentsModulePath_rglob($pattern, $flags = 0)
    {
        $files = glob($pattern, $flags);
        $subdirectories = glob(dirname($pattern).'/*', GLOB_ONLYDIR);
        usort($subdirectories, function($a, $b) { /// sortiramo rucno alfabetski
            return strcmp($a, $b);
        });
        foreach ($subdirectories as $dir) {
            $files = array_merge($files, self::includeVueComponentsModulePath_rglob($dir.'/'.basename($pattern), $flags));
        }
        return $files;
    }
    
    public static function includeTranslationsForModulePath($dirname)
    {
        if (Yii::app()->isWebApplication())
        {
            $s = DIRECTORY_SEPARATOR;
            $lang = Yii::app()->language;
            
            /// messages dir translation
            $filename = $dirname.$s.'messages'.$s.$lang.$s.'JSTranslations.php';
            SIMAModule::includeTranslationsFromFilePath($filename);
            
            /// extensions translations
            foreach(glob($dirname.$s.'extensions'.$s.'*'.$s.'messages'.$s.$lang.$s.'JSTranslations.php') as $filepath)
            {
                SIMAModule::includeTranslationsFromFilePath($filepath);
            }
        }
    }
    
    public static function includeTranslationsFromFilePath($filepath)
    {
        $translations = Yii::app()->params['js_translations'];
        if (file_exists($filepath))
        {
            $translations = array_merge($translations,include($filepath));
        }
        Yii::app()->params['js_translations'] = $translations;
    }
    
    private function includeMaintainOneMinuteCommands()
    {
        $modul_maintain_one_minute_commands = $this->maintainOneMinuteCommands();
        if(!empty($modul_maintain_one_minute_commands))
        {
            $maintain_one_minute_commands = Yii::app()->params['maintain_one_minute_commands'];
            $maintain_one_minute_commands_merged = array_merge($maintain_one_minute_commands, $modul_maintain_one_minute_commands);
            Yii::app()->params['maintain_one_minute_commands'] = $maintain_one_minute_commands_merged;
        }
    }
    
    private function includeMaintainFiveMinuteCommands()
    {
        $modul_maintain_five_minute_commands = $this->maintainFiveMinuteCommands();
        if(!empty($modul_maintain_five_minute_commands))
        {
            $maintain_five_minute_commands = Yii::app()->params['maintain_five_minute_commands'];
            $maintain_five_minute_commands_merged = array_merge($maintain_five_minute_commands, $modul_maintain_five_minute_commands);
            Yii::app()->params['maintain_five_minute_commands'] = $maintain_five_minute_commands_merged;
        }
    }
    
    private function includeConfFiles()
    {
        parent::init();
        
        $dirname = $this->getBasePath();
        
        $modelAccessFilePath = $dirname.'/config/configModelAccess.php';
        if(file_exists($modelAccessFilePath))
        {
            SIMAMisc::includeModelAccesses(include($modelAccessFilePath));
        }
        
        $mainMenuAccessFilePath = $dirname.'/config/configMainMenuItems.php';
        if(file_exists($mainMenuAccessFilePath))
        {
            SIMAMisc::includeMainMenuAccesses(include($mainMenuAccessFilePath));
        }
        
        $configOperationsFilePath = $dirname.'/config/configOperations.php';
        if(file_exists($configOperationsFilePath))
        {
            SIMAMisc::includeOperations(include($configOperationsFilePath), $this->getName());
        }
        
        $configCheckOperationsFilePath = $dirname.'/config/configCheckOperations.php';
        if(file_exists($configCheckOperationsFilePath))
        {
            SIMAMisc::includeCheckOperations(include($configCheckOperationsFilePath), $this->getName());
        }
        
        $configMapperFilePath = $dirname.'/config/configMapper.php';
        if(file_exists($configMapperFilePath))
        {
            SIMAMisc::includeConfigMapper(include($configMapperFilePath), $this->getName('.'));
        }
        
        $configApiFunctionsFilePath = $dirname.'/config/configApiFunctions.php';
        if(file_exists($configApiFunctionsFilePath))
        {
            SIMAMisc::includeApiFunctions(include($configApiFunctionsFilePath), $this->getFullName());
        }
        
        $warnsFilePath = $dirname.'/config/warns.php';
        if (file_exists($warnsFilePath))
        {
            Yii::app()->warnManager->addWarns(include($warnsFilePath));
        }

        /**
        * Zadaju se sistemske liste u obliku:
        *  [
               'system_list_code' => [
                   'name' => 'list name',
                   'calc_list_func' => function() {
                       //updejtuje tabelu partners_to_partner_lists sa novim spiskom partnera koji su na toj listi
                   },
                   'description' => 'list description,
               ]
            ];
        */
        $system_lists_file_path = $dirname.'/config/systemPartnerLists.php';
        if (file_exists($system_lists_file_path))
        {
            Yii::app()->params['system_partner_lists'] = array_merge(Yii::app()->params['system_partner_lists'], include($system_lists_file_path));
        }
        
        $cron_jobs_file_path = $dirname.'/config/configCronJobs.php';
        if (file_exists($cron_jobs_file_path))
        {
            Yii::app()->simaCron->includeJobs(include($cron_jobs_file_path));
        }
    }
    
    private function includeCommands()
    {
        $dirname = $this->getBasePath();
        
        $commands_path = $dirname.DIRECTORY_SEPARATOR.'commands';
                
        Yii::app()->getCommandRunner()->addCommands($commands_path);
    }
    
    private function includeMainMenuItems()
    {
        $main_menu_items_file = $this->getBasePath() . '/config/mainMenuItems.php';
        if(file_exists($main_menu_items_file))
        {
            Yii::app()->mainMenu->add(include($main_menu_items_file));
        }
    }
    
    /**
     * 
     * @param type $nameSeparator
     * @return string
     */
    public function getFullName($nameSeparator=DIRECTORY_SEPARATOR)
    {
        $thisName = $this->getName();
        
        $parentModule = $this->getParentModule();
        if(is_a($parentModule, 'SIMAModule'))
        {
            if(empty($nameSeparator))
            {
                $nameSeparator = DIRECTORY_SEPARATOR;
            }
            
            $thisName = $parentModule->getFullName($nameSeparator).$nameSeparator.$thisName;
        }
                
        return $thisName;
    }
    
    public static function getModuleByName($module, $moduleName)
    {
        $result = null;
        
        $submodules = $module->getModules();
        $submodules_keys = array_keys($submodules);
        
        if(in_array($moduleName, $submodules_keys))
        {
            $result = $module->getModule($moduleName);
            return $result;
        }
                
        foreach ($submodules_keys as $key)
        {
            $submodule = $module->getModule($key);
            
            $result = SIMAModule::getModuleByName($submodule, $moduleName);
            
            if(!empty($result))
            {
                break;
            }
        }
        
        return $result;
    }
    
    public static function registerModules($module, $with_registermanual=false)
    {
//        if($with_registermanual === true)
//        {
//            /// ucitaj prevode za ovaj modul
//            $dirname = $module->getBasePath();
//            SIMAModule::includeTranslationsForModulePath($dirname);
//        }
        
        
//        $submodules = $module->getModules();
//        $submodules_keys = array_keys($submodules);
//        foreach ($submodules_keys as $key)
//        {
//            $submodule = $module->getModule($key);
//            if($with_registermanual === true)
//            {
//                $submodule->registerManual();
//            }
//            SIMAModule::registerModules($submodule, $with_registermanual);
//        } 
        
        SIMAModule::registerModules_PermanentBehaviors($module);
        SIMAModule::registerModules_SimaActivities($module);
        SIMAModule::registerModules_mapClasses($module);
        SIMAModule::registerModules_ConfFiles($module);
        
        if(Yii::app()->isConsoleApplication() === true)
        {
            SIMAModule::registerModules_Commands($module);
            SIMAModule::registerModules_MaintainCommands($module);
        }
        
        if($with_registermanual === true)
        {
            SIMAModule::registerModules_Translations($module);
            SIMAModule::registerModules_registerManual($module);
            SIMAModule::registerModules_vueComponents($module);
        }
    }
    
    public static function registerModules_PermanentBehaviors($module)
    {
        $submodules = $module->getModules();
        $submodules_keys = array_keys($submodules);
        foreach ($submodules_keys as $key)
        {
            $submodule = $module->getModule($key);
            
            $submodule->registerPermanentBehaviors();
            
            SIMAModule::registerModules_PermanentBehaviors($submodule);
        }
    }
    public static function registerModules_SimaActivities($module)
    {
        $submodules = $module->getModules();
        $submodules_keys = array_keys($submodules);
        foreach ($submodules_keys as $key)
        {
            $submodule = $module->getModule($key);
            
            $submodule->registerSimaActivities();
            
            SIMAModule::registerModules_SimaActivities($submodule);
        }
    }
    public static function registerModules_mapClasses($module)
    {
        $submodules = $module->getModules();
        $submodules_keys = array_keys($submodules);
        foreach ($submodules_keys as $key)
        {
            $submodule = $module->getModule($key);
            
            $submodule->mapClasses();
            
            SIMAModule::registerModules_mapClasses($submodule);
        }
    }
    public static function registerModules_ConfFiles($module)
    {
        $submodules = $module->getModules();
        $submodules_keys = array_keys($submodules);
        foreach ($submodules_keys as $key)
        {
            $submodule = $module->getModule($key);
            
            $submodule->includeConfFiles();
            
            SIMAModule::registerModules_ConfFiles($submodule);
        }
    }
    public static function registerModules_MaintainCommands($module)
    {
        $submodules = $module->getModules();
        $submodules_keys = array_keys($submodules);
        foreach ($submodules_keys as $key)
        {
            $submodule = $module->getModule($key);
            
            $submodule->includeMaintainOneMinuteCommands();
            $submodule->includeMaintainFiveMinuteCommands();
            
            SIMAModule::registerModules_MaintainCommands($submodule);
        }
        
    }
    public static function registerModules_Commands($module)
    {
        $submodules = $module->getModules();
        $submodules_keys = array_keys($submodules);
        foreach ($submodules_keys as $key)
        {
            $submodule = $module->getModule($key);
            
            $submodule->includeCommands();
            
            SIMAModule::registerModules_Commands($submodule);
        }
    }
    public static function registerModules_Translations($module)
    {
        /// ucitaj prevode za ovaj modul
        $dirname = $module->getBasePath();
        SIMAModule::includeTranslationsForModulePath($dirname);
            
        $submodules = $module->getModules();
        $submodules_keys = array_keys($submodules);
        foreach ($submodules_keys as $key)
        {
            $submodule = $module->getModule($key);
            
            SIMAModule::registerModules_Translations($submodule);
        }
    }
    
    /**
     * ucitava sve vue komponente koje nadje u modulu u vueGUI folderu
     * js se ucitava kao POS_READY
     * php kao POS_END
     * @param type $module
     */
    public static function registerModules_vueComponents($module)
    {
        $dirname = $module->getBasePath();
        SIMAModule::includeVueComponentsModulePath($dirname);
            
        $submodules = $module->getModules();
        $submodules_keys = array_keys($submodules);
        foreach ($submodules_keys as $key)
        {
            $submodule = $module->getModule($key);
            
            SIMAModule::registerModules_vueComponents($submodule);
        }
    }
    public static function registerModules_registerManual($module)
    { 
        $submodules = $module->getModules();
        $submodules_keys = array_keys($submodules);
        foreach ($submodules_keys as $key)
        {
            $submodule = $module->getModule($key);
            
            $submodule->registerManual();
            
            SIMAModule::registerModules_registerManual($submodule);
        }
    }
    
    public static function registerModules_MainMenuItems($module)
    {
        $submodules = $module->getModules();
        $submodules_keys = array_keys($submodules);
        foreach ($submodules_keys as $key)
        {
            $submodule = $module->getModule($key);
            
            $submodule->includeMainMenuItems();
            
            SIMAModule::registerModules_MainMenuItems($submodule);
        }
    }
    
    public static function getTablesToModelsMapped()
    {
        $result = Yii::app()->params['configTablesToModelsMapper'];
        
//        if(empty($result) && Yii::app()->isWebApplication())
        if(empty($result))
        {
            $result = SIMAModule::getTablesToModels(Yii::app());
            Yii::app()->params['configTablesToModelsMapper'] = $result;
        }
        
        return $result;
    }
    
    public static function getTablesToModels($module)
    {
        $result = [];
        
        /**
        * mapiranje
        */
        
        $table_name_declaration = 'public function tableName()';
        
        /// za svaki fajl u folderu models
        $dirname = $module->getBasePath();
        foreach(glob($dirname.'/models/*.php') as $file_path){
            
            $in = fopen($file_path, "r");
            
            /// skuplja se sadrzaj tablename f-je
            $found_func = false;
            $func_str = '';
            while (($buffer = fgets($in)) !== false) {
                if($found_func === true)
                {
                    if(strpos($buffer, '}') !== false) /// kraj f-je
                    {
                        break;
                    }
                    
                    $func_str .= trim($buffer);
                }
                else if(strpos($buffer, $table_name_declaration) !== false  /// ako je deklaracija tablename f-je
                        && substr( trim($buffer), 0, 2 ) !== '//')          /// a nije zakomentarisana linij
                {
                    $found_func = true;
                }
            }
            
            if(!empty($func_str))
            {
                $modelTable = str_replace([' ', '{', '}', ';', 'return\'', 'return"', '\'', '"', "\n"], '', $func_str);
                
                if(strpos($modelTable, '.') === false)
                {
                    $modelTable = 'public.'.$modelTable;
                }
                
                $modelName = str_replace([
                    $dirname.'/models/',
                    '.php'
                ], [
                    '',
                    ''
                ], $file_path);

                $result[$modelTable] = $modelName;
            }
            
//            $modelName = str_replace([
//                $dirname.'/models/',
//                '.php'
//            ], [
//                '',
//                ''
//            ], $file_path);
            
//            $table_name_declaration = 'public function tableName()';
//            $table_name_declaration_len = strlen($table_name_declaration);
//            $file_content = file_get_contents($file_path);
//            $pos_of_tablename_method = strpos($file_content, 'public function tableName()');
//            
//            if($pos_of_tablename_method !== false)
//            {
//                $pos_of_end_tablename_method = strpos($file_content, '}', $pos_of_tablename_method);
//                $tablename_func_substr = substr($file_content, 
//                        $pos_of_tablename_method+$table_name_declaration_len, 
//                        $pos_of_end_tablename_method-$table_name_declaration_len-$pos_of_tablename_method);
//
//                $modelTable = str_replace([' ', '{', '}', ';', 'return\'', 'return"', '\'', '"', "\n"], '', $tablename_func_substr);
//                
//                if(strpos($modelTable, '.') === false)
//                {
//                    $modelTable = 'public.'.$modelTable;
//                }
//                
//                $configTablesToModelsMapper = Yii::app()->params['configTablesToModelsMapper'];
//                $configTablesToModelsMapper[$modelTable] = $modelName;
//                Yii::app()->params['configTablesToModelsMapper'] = $configTablesToModelsMapper;
//            }
        }

        /**
         * rekurzivno za podmodule
         */
        $submodules = $module->getModules();
        $submodules_keys = array_keys($submodules);
        foreach ($submodules_keys as $key)
        {
            $submodule = $module->getModule($key);
            $result = array_merge($result, SIMAModule::getTablesToModels($submodule));
        }
        
        return $result;
    }
    
    public static function getMigrationMinorVersion($module, $major_version)
    {
        $dirname = $module->getBasePath();
        $result = count(glob($dirname.'/migrations/m'.$major_version.'_*'));
        
        $submodules = $module->getModules();
        $submodules_keys = array_keys($submodules);
        foreach ($submodules_keys as $key)
        {
            $submodule = $module->getModule($key);
            $result += SIMAModule::getMigrationMinorVersion($submodule, $major_version);
        }
        
        return $result;
    }
}