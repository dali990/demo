<?php

class SIMATestApplication extends SIMAWebApplication
{
    public static function isWebApplication()
    {
        return false;
    }
    
    public function raiseNote($text, $note_code = null)
    {
        echo "***** NOTE ***** $note_code: $text\n";
    }
    
    public function sendUpdateEvent($percent)
    {
        error_log(__METHOD__.' - $percent: '.$percent);
    }
}

