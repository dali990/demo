<?php

class SIMAActivityLogger extends CBehavior
{
    private $db_component_name = 'db_web_statistics_logger';
    private $logger_table = 'web_statistics_logger.web_statistics_logger';
    private $start_time = null;
    
    public function attach($owner)
    {
        if(Yii::app()->params['log_activities'] === true)
        {
            $owner->attachEventHandler('onBeginRequest', [$this, 'handleBeginRequest']);
            $owner->attachEventHandler('onEndRequest', [$this, 'handleEndRequest']);
        }
    }
    
    public function handleBeginRequest($event)
    {
        try
        {
            if(!Yii::app()->hasComponent($this->db_component_name))
            {
                throw new Exception('component not set: '.CJSON::encode($this->db_component_name));
            }

            $this->start_time = microtime(true);
        }
        catch(Exception $e)
        {
            error_log(__METHOD__.' - error message: '.$e->getMessage());
        }
    }
    
    public function handleEndRequest($event)
    {
        try
        {
            $get_array = filter_input_array(INPUT_GET);
            $post_array = filter_input_array(INPUT_POST);
            $http_headers = getallheaders();

            $logger_data = '';
            $begin_timestamp = $this->start_time;
            $end_timestamp = microtime(true);
            $issima_fqdn = Yii::app()->params['server_FQDN'];
            $issima_user_id = -1;
            if(Yii::app()->isWebApplication())
            {
                $issima_user_id = Yii::app()->user->id;
            }
            $http_action = isset($get_array['r'])?$get_array['r']:'index.php';
            $http_get_data = CJSON::encode($get_array);
            $http_post_data = CJSON::encode($post_array);
            $http_header_data = CJSON::encode($http_headers);
            $issima_user_ip_address = Yii::app()->getClientIpAddress();
            $issima_session_id = Yii::app()->session['sima_session'];
            $issima_memory_usage = memory_get_usage(true);

            $sql = 'INSERT INTO '.$this->logger_table.' ('
                        .'logger_data, begin_timestamp, end_timestamp, issima_fqdn, '
                        .'issima_user_id, issima_user_ip_address, issima_session_id, '
                        .'issima_memory_usage, '
                        .'http_action, http_get_data, http_post_data, http_header_data'
                    .') '
                    .'VALUES ('
                        .':logger_data, :begin_timestamp, :end_timestamp, :issima_fqdn, '
                        .':issima_user_id, :issima_user_ip_address, :issima_session_id, '
                        .':issima_memory_usage, '
                        .':http_action, :http_get_data, :http_post_data, :http_header_data'
                    .')';

            $command = Yii::app()->{$this->db_component_name}->createCommand($sql);
            $command->bindParam(':logger_data', $logger_data);
            $command->bindParam(':begin_timestamp', $begin_timestamp);
            $command->bindParam(':end_timestamp', $end_timestamp);
            $command->bindParam(':issima_fqdn', $issima_fqdn);
            $command->bindParam(':issima_user_id', $issima_user_id);
            $command->bindParam(':http_action', $http_action);
            $command->bindParam(':http_get_data', $http_get_data);
            $command->bindParam(':http_post_data', $http_post_data);
            $command->bindParam(':http_header_data', $http_header_data);
            $command->bindParam(':issima_user_ip_address', $issima_user_ip_address);
            $command->bindParam(':issima_session_id', $issima_session_id);
            $command->bindParam(':issima_memory_usage', $issima_memory_usage);

            $command->execute();
        }
        catch(Exception $e)
        {
            error_log(__METHOD__.' - error message: '.$e->getMessage());
        }
    }
}
