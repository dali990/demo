<?php

/**
 * @package SIMA
 * @subpackage Base
 */
class SIMAActiveRecordTabs extends CActiveRecordBehavior
{
    public function listTabs($params=[])
    {        
        $_tabs = $this->tabsDefinition();
        
        $event = new SIMAEvent($this->owner);
        $this->owner->onListTabs($event);
        foreach ($event->getResults() as $result)
        {
            foreach ($result as $value) 
            {
                $this->addTabToTabs($_tabs, $value);
            }
        }
        
        foreach ($_tabs as $key => $tab)
        {            
            if (!isset($_tabs[$key]['action'])) 
            {
                $_tabs[$key]['action'] = 'base/model/tab';
                if(isset($_tabs[$key]['is_ajax_log']) && $_tabs[$key]['is_ajax_log']===true)
                {
                    $_tabs[$key]['action'] = 'base/model/tabAjaxLong';
                }
                
                $get_params = array('model'=>get_class($this->owner),'id' => $this->owner->id, 'code' => $tab['code']);
                
                if (isset($_tabs[$key]['module_origin']))
                {
                    $get_params['module_origin'] = $_tabs[$key]['module_origin'];
                    unset($_tabs[$key]['module_origin']);
                }
                
                $_tabs[$key]['get_params'] = $get_params;
                $_tabs[$key]['params'] = isset($params[$tab['code']])?$params[$tab['code']]:[];
                $_tabs[$key]['local_render'] = true;
            }
        }
        return $_tabs;
    }
    
    private function addTabToTabs(&$tabs, $tab)
    {
        $code_path = explode('.', $tab['code']);
        $code_path_cnt = count($code_path);
        if ($code_path_cnt < 2)
        {
            array_push($tabs, $tab);
        }
        else
        {
            $_tabs = &$tabs;
            $path = '';
            for($i = 0; $i < $code_path_cnt-1; $i++)
            {
                $path .= empty($path) ? $code_path[$i] : ".{$code_path[$i]}";
                $tab_index = $this->getTabIndexByCode($_tabs, $path);
                if (!is_null($tab_index))
                {
                    $_tab = &$_tabs[$tab_index];
                    if ($i === $code_path_cnt-2)
                    {
                        if (!isset($_tab['subtree']))
                        {
                            $_tab['subtree'] = [];
                        }
                        array_push($_tab['subtree'], $tab);
                    }
                    else if (isset($_tab['subtree']))
                    {
                        $_tabs = &$_tab['subtree'];
                    }
                }
            }
        }
    }
    
    private function getTabIndexByCode(&$tabs, $code)
    {
        foreach ($tabs as $tab_key=>$tab_value)
        {
            if ($tab_value['code'] === $code)
            {
                return $tab_key;
            }
        }
        
        return null;
    }
    
    /**
     * 
     * @param string $code - kod taba
     * @param type $local_params - dodatni parametri
     * @return array
     * @throws SIMAWarnExceptionModelTabNotFound
     */
    public function tabParams($code, $local_params)
    {
        $_result_params = [];
        $tabs = $this->listTabs();
        $code_found = false;
        foreach ($tabs as $tab)
        {
            if ($tab['code'] === $code)
            {
                $code_found = true;
            }
            if ($tab['code'] == $code && isset($tab['params_function']))
            {
                $funcName = $tab['params_function'];
                $_result_params = $this->owner->$funcName($local_params);
            }
        }
        if (!$code_found)
        {
            throw new SIMAWarnExceptionModelTabNotFound($this->owner, $code);
        }
        $_result_params['model'] = $this->owner;
        $_result_params['local_params'] = $local_params;
        return $_result_params;
        
    }
    
    public function tabOrigin($code)
    {
        $tabs = $this->tabsDefinition();
        foreach ($tabs as $key => $tab)
        {
            if ($tab['code'] == $code && isset($tab['origin']))
            {
                return $tab['origin'];
            }
        }
        
        return null;
    }
}