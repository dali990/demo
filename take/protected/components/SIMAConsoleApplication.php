<?php

require_once __DIR__.'/../traits/SIMAApplicationTraits.php';
require_once __DIR__.'/../traits/ModelUpdateEventsTraits.php';
require_once __DIR__.'/../traits/SIMAAfterWorkTraits.php';

class SIMAConsoleApplication extends CConsoleApplication
{
    use SIMAApplicationTraits;
    use ModelUpdateEventsTraits;
    use SIMAAfterWorkTraits;
    
    /**
     * @var Company model kompanije 
     */
    private $_company = NULL;
    
    /**
     *
     * @var string 
     */
    public $sima_size = NULL;
    
    public $validate_activities = true;
    
    /**
     * Boolean promenljiva koja odredjuje da li se zove update model views.
     * Za SIMAConsoleApplication default je false
     * @var boolean
     */
    private $update_model_views = false;

    public function init()
    {
        parent::init();
        
        if(!Yii::app()->simaDemo->IsDemoISSIMA())
        {
            $this->initDbConnection();
        }
        
        SIMAModule::registerModules(Yii::app());
        Yii::app()->configManager->loadConfigurationDefinitions();
        
        if(
            Yii::app()->params['is_production'] === false
            && Yii::app()->configManager->get('admin.use_memcache') === true
            && !(isset(Yii::app()->params['local_memcache']) && Yii::app()->params['local_memcache'] === true)
        )
        {
            Yii::app()->configManager->set('admin.use_memcache', false, false);
        }
    }
    
    public function getAuthManager()
    {
            return $this->getComponent('authManager');
    }
    
    public function __get($name)
    {
        switch ($name)
        {
            default: return parent::__get($name);
        }
    }
    
    public function __set($name, $value)
    {   
        switch ($name)
        {
            default: parent::__set($name, $value); break;
        }
        
    }
    
    /**
     * 
     * @return Company objekat koji predstavlja red u tabeli companies
     */
    public function getCompany()
    {
        if ($this->_company===NULL)    
        {
            $system_company_id = Yii::app()->configManager->get('base.system_company_id');
            $this->_company = Company::model()->findByPk($system_company_id);
        }
        return $this->_company;
    }

    /**
     * Yii::app()->isWebApplication()
     * @return boolean
     */
    public static function isWebApplication()
    {
        return false;
    }

    /**
     * Yii::app()->isConsoleApplication()
     * @return boolean
     */
    public static function isConsoleApplication()
    {
        return true;
    }
    
    /**
     * 
     * @param type $note
     * @param type $note_code - ne postoji podrska za ovo, posto ne znam sta bih stavili jer se ispisuje u realnom vremenu, a ne na kraju zahteva
     */
    public function raiseNote($note, $note_code = null)
    {
        echo "***** NOTE *****  $note\n";
    }
    
    public function getAssetManager()
    {
            return $this->getComponent('assetManager');
    }
    
    /**
    * Returns the client script manager.
    * @return CClientScript the client script manager
    */
    public function getClientScript()
    {
           return $this->getComponent('clientScript');
    }
    
    public function getBaseUrl($absolute = false)
    {
        if ($absolute === true && !empty(Yii::app()->params['server_FQDN']))
        {
            return Yii::app()->params['server_FQDN'];
        }
        else
        {
            return parent::getBaseUrl($absolute);
        }
    }
    
    /**
     * 
     * @return boolean
     */
    public function isDemo()
    {
        $result = false;
        
        if(
            isset(Yii::app()->params['is_demo'])
            && Yii::app()->params['is_demo'] === true
            && isset($_SESSION["db_session_key"])
        )
        {
            $result = true;
        }
        
        return $result;
    }
    
    public function getSessionKey()
    {
        $session_key = 'sima';
        if(isset($_SESSION["db_session_key"]))
        {
            $session_key = $_SESSION["db_session_key"];
        }
        return $session_key;
    }
    
    public function getClientIpAddress()
    {
        $ipaddress = '127.0.0.1';
        
        return $ipaddress;
    }
    
    public function getUpdateModelViews()
    {
        return $this->update_model_views;
    }
    
    public function setUpdateModelViews($value)
    {
        if(!is_bool($value))
        {
            $error_message = __METHOD__.' - nije boolean, a mora biti - $value: '. SIMAMisc::toTypeAndJsonString($value);
            error_log($error_message);
//            throw new Exception($error_message);
        }
        $this->update_model_views = $value;
    }
    
    public function hasModule($moduleString, $parent_module=null)
    {
        return SIMAMisc::hasModule($moduleString, $parent_module);
    }
    
    public function sendUpdateEvent($percent)
    {
        error_log(__METHOD__.' - $percent: '.$percent);
    }
}

