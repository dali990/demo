<?php

/*
 * SIMACache pruza kesiranje u postgresql bazi
 * 
 * primer kada je kljuc rucno kreiran string:
 * 
 * $cache_key = 'kljuc'.$dodatni_parametar;
 * 
 * 
 * 
 * primer upotrebe kada je kljuc objekat:
 * 
 * ako je dosada bio deo koda:
 *  $command = Yii::app()->db->createCommand($sql);
 *  $data = $command->queryAll();
 * 
 * pretvara se u:
 *  $command = Yii::app()->db->createCommand($sql);
 *  $command_clone = clone $command; (nije obavezno kloniranje, ali za cdbcommand jeste)
 *  $data = SIMACache::get($command)
 *  if($data === false)
 *  {
 *      $data = $command->queryAll();
 * 
 *      $keywords = generisiKljucneReciNekako();
 *      SIMACache::set($command, $data, $keywords);
 *  }
 * 
 */
/* 
 * 
 * Zahteva postojanje tabela:
 * CREATE UNLOGGED TABLE sima_cache_table
    (
        id serial NOT NULL,
        key text NOT NULL,
        value text NOT NULL,
        keywords text NOT NULL,
        hit integer NOT NULL DEFAULT 0,
        miss integer NOT NULL DEFAULT 1,
        is_valid boolean NOT NULL DEFAULT false,
        last_access timestamp without time zone NOT NULL DEFAULT now(),
        CONSTRAINT sima_cache_table_pkey PRIMARY KEY (id)
    );
    CREATE INDEX "sima_cache_table_key_idx" ON sima_cache_table(key);
 */ 
/* 
  * funkcije: 
 * CREATE OR REPLACE FUNCTION get_cache(cache_table_name text, cache_key text)
  RETURNS text AS
$BODY$
DECLARE
	cache_rec RECORD;
	result TEXT;
BEGIN
	EXECUTE 'SELECT * FROM ' || cache_table_name || ' WHERE key=''' || cache_key || ''''
	INTO cache_rec;

	IF cache_rec.key IS NOT NULL
	THEN
		IF cache_rec.is_valid = TRUE
		THEN
			result := cache_rec.value;
			
			EXECUTE 'UPDATE ' || cache_table_name || ' SET hit=hit+1, last_access=now() WHERE key=''' || cache_key || '''';
		ELSE
			EXECUTE 'UPDATE ' || cache_table_name || ' SET miss=miss+1, last_access=now() WHERE key=''' || cache_key || '''';
		END IF;
	ELSE
		PERFORM 'SELECT set_cache(''' || cache_table_name || ''', ''' || cache_key || ''', '''', '''')';
	END IF;
	
	RETURN result;
END;
$BODY$
  LANGUAGE plpgsql
 */
/*  
 * CREATE OR REPLACE FUNCTION set_cache(cache_table_name text, cache_key text, cache_value text, cache_keywords text, is_valid text DEFAULT 'FALSE'::text)
  RETURNS boolean AS
$BODY$
DECLARE
	count_value INT;
BEGIN
	EXECUTE 'SELECT COUNT(*) FROM ' || cache_table_name || ' WHERE key=''' || cache_key || ''''
	INTO count_value;

	IF count_value = 0
	THEN
		EXECUTE 'INSERT INTO ' || cache_table_name || ' (key, value, keywords, is_valid) '
			|| 'VALUES (''' || cache_key || ''', ''' || cache_value || ''', ''' || cache_keywords || ''', ''' || is_valid || ''')';
	ELSIF count_value = 1
	THEN
		EXECUTE 'UPDATE ' || cache_table_name 
			|| ' SET value=''' || cache_value || ''', keywords=''' || cache_keywords || ''',, last_access=now(), is_valid=''' || is_valid || ''''
			|| ' WHERE key=''' || cache_key || '''';
	ELSE
		RAISE EXCEPTION 'invalid number of rows for key';
	END IF;

	RETURN TRUE;
END;
$BODY$
  LANGUAGE plpgsql
 * 
 */ 
/*
 *  CREATE OR REPLACE FUNCTION devalidate_cache_by_id(cache_table_name text, cache_id integer)
  RETURNS text AS
$BODY$
DECLARE
	count_value INT;
BEGIN
	EXECUTE 'UPDATE ' || cache_table_name || ' SET value=''NULL'', keywords=''NULL'', is_valid=FALSE, last_access=now() WHERE id=' || cache_id;

	RETURN 'OK';
END;
$BODY$
  LANGUAGE plpgsql
 * 
 */ 
/* 
 * CREATE OR REPLACE FUNCTION delete_cache_by_id(cache_table_name text, cache_id integer)
  RETURNS text AS
$BODY$
DECLARE
	count_value INT;
BEGIN
	EXECUTE 'DELETE FROM ' || cache_table_name || ' WHERE id=' || cache_id;

	RETURN 'OK';
END;
$BODY$
  LANGUAGE plpgsql
 */ 
/* 
 * CREATE OR REPLACE FUNCTION devalidate_cache_by_keywords(cache_table_name text, keywords text)
  RETURNS text AS
$BODY$
DECLARE
	keyword TEXT;
BEGIN
	FOREACH keyword IN ARRAY string_to_array(keywords, ' ')
	LOOP
		EXECUTE 'UPDATE ' || cache_table_name || ' SET value=''NULL'', keywords=''NULL'', is_valid=FALSE, last_access=now() WHERE keywords LIKE ''%' || keyword || '%''';
	END LOOP;

	RETURN 'OK';
END;
$BODY$
  LANGUAGE plpgsql
 * 
 */
/*
 * Postojanje u sima bazi:
 * 
 * CREATE OR REPLACE FUNCTION i_am_called_by_everyone()
  RETURNS trigger AS
$BODY$
DECLARE
	log_cache_devalidation BOOLEAN := FALSE;

	v_old_data TEXT;
	v_new_data TEXT;

	keywords TEXT;

	return_rec RECORD;
BEGIN 
	IF (TG_OP = 'UPDATE') 
	THEN
		keywords := generate_row_keywords(TG_TABLE_SCHEMA, TG_TABLE_NAME, row_to_json(OLD.*), 'u');
		return_rec := NEW;
	ELSIF (TG_OP = 'DELETE') 
	THEN
		keywords := generate_row_keywords(TG_TABLE_SCHEMA, TG_TABLE_NAME, row_to_json(OLD.*), 'd');
		return_rec := OLD;
	ELSIF (TG_OP = 'INSERT') 
	THEN
		keywords := generate_row_keywords(TG_TABLE_SCHEMA, TG_TABLE_NAME, row_to_json(NEW.*), 'i');
		return_rec := NEW;
	ELSE
		RAISE WARNING '[i_am_called_by_everyone] - Other action occurred: %, at %',TG_OP,now();
		return_rec := NULL;
	END IF;

	IF log_cache_devalidation = TRUE
	THEN
		INSERT INTO cache_devalidation_log (keywords) VALUES (keywords);
	END IF;

	EXECUTE 'SELECT * FROM dblink(''hostaddr=192.168.17.118 port=5432 dbname=sima_cache user=postgres password=postgres'', ''SELECT * FROM devalidate_cache_by_keywords(''''sima_cache_table'''', '''''|| keywords || ''''') '') AS tmp(result text)';

	RETURN return_rec;
END;
$BODY$
  LANGUAGE plpgsql
 * 
 * CREATE OR REPLACE FUNCTION generate_row_keywords(tableschema text, tablename text, rowjson json, action_letter text)
  RETURNS text AS
$BODY$
DECLARE
	tag_param TEXT;
	pk_parts TEXT[];
	pk_part TEXT;
	result TEXT := '';
BEGIN
	tag_param := tableschema || '-' || tablename;

	SELECT array_agg(pg_attribute.attname)
	FROM pg_index, pg_class, pg_attribute 
	WHERE	pg_class.oid = (tableschema||'.'||tablename)::regclass 
		AND indrelid = pg_class.oid AND pg_attribute.attrelid = pg_class.oid AND pg_attribute.attnum = any(pg_index.indkey) AND indisprimary
	INTO
	pk_parts;

	FOREACH pk_part IN ARRAY pk_parts
	LOOP
		IF result NOT LIKE ''
		THEN
			result := ' ';
		END IF;
		
		result := result || tag_param || '_' || trim(rowjson->>pk_part);
	END LOOP;

	result := result || ' ' || tag_param || '_*_' || action_letter;

        RETURN result;
END;
$BODY$
  LANGUAGE plpgsql
 * 
 * 
 */

class SIMACache extends CApplicationComponent
{
    /**
     * Niz koji pamti vreme racunanja kesa;
     * Uzima se u obzir da se posle miss-a vrsi set-ovanje;
     * 
     * kljucevi su cache kljucevi;
     * vrednosti s vremena u mikrosekundama;
     * 
     * Kada se u get-u napravi miss, postavi se vrednos kljuca u nizu na trenutno vreme;
     * Kada se vrsi set, proverava se da li se kljuc nalazi u nizu i njegova vrednost oduzima od trenutnog vremena;
     * To vreme predstavlja vreme izvrsavanja;
     */
    private $cacheCalcTime = array();
    
    /**
     * ime schema-e koja se koristi za kesiranje.
     * default 'public'
     * 
     * @var string 
     */
    public $cacheSchema = 'public';
    
    /**
     * ime tabele koja se koristi za kesiranje.
     * default 'sima_cache_table'
     * 
     * @var string 
     */
    public $cacheTable = 'sima_cache_table';
    
    /**
     * naziv komponente koja predstavlja pgsql bazu, preciznije DBComponent
     * default 'db'
     * 
     * @var string 
     */
    public $dbComponent = 'db';
    
    /**
     * maksimalna velicina baze nakon cega sega se prikazuje greska
     * default '512k'
     * 
     * @var string 
     */
    public $maxSize = '512k';
    
    /**
     * A string prefixed to every cache key so that it is unique.
     * If different applications need to access the same pool of cached data, 
     * the same prefix should be set for each of the applications explicitly.
    */
    public $keyPrefix = 'SIMACache';
    
    /**
     * Komponenta koja predstavlja CApcCache.
     * Koristi se za dodatno ubrzanje kesiranjem na strani php-a
     * default null
     */
    public $apcComponent = null;
    
    /**
     * if conf use_memcache is true, use conf memcache_data to set component
     * @var obj Memcache
     */
    public $memcacheComponent = null;
    
    /**
     * cached here to check if changed from config to reconnect
     * user - requred for appending to begining of memcache key
     * @var string 
     */
    public $memcacheComponent_host = null;
    public $memcacheComponent_port = null;
    public $memcacheComponent_user = null;

    /**
     * Funkcija koja dovlaci kesiranu vrednost za dati kljuc, ako postoji;
     * 
     * ako postoji kesiranje, ono ce se nalaziti u bazi, 
     * ili ako je slucajno trazeno pre $apc_cache_time sekundi ili manje i u apc-u;
     * tako da se prvo proverava da li se vec nalazi u apc cache-u, ako ne, da li se nalazi u bazi 
     * nakon cega se stavlja u apc kes na $apc_cache_time sekundi;
     * 
     * @param (object/string) $key - kljuc(id) po kojem ce da se vrsi kesiranje;
     * @return povratna vrednost je kesiran podatak ili false ako ne postoji kesiranje
     */
    public function get($key, $with_apc = false, $apc_cache_time = 5)
    {
        $result = false;
        $parsedKey = $this->parseKey($key);

        /* prvo se pokusa iz apc-a */
        if($with_apc === true && $this->apc() !== null)
        {
            $result = $this->apc()->get($parsedKey);
        }
        
        // zatim se pokusa memcache
        if($result === false && Yii::app()->configManager->get('admin.use_memcache') === true)
        {
            $this->setupMemCachedComponent();
            $result = $this->memcacheComponent->get($this->memcacheComponent_user.$parsedKey);
            if($result !== false)
            {
                $result = $this->unserializeValue($result);
            }
        }

        /* ukoliko nema u apc-u pokusava se iz db-a i stavlja u apc */
        if ($result===false)
        {            
            $result = $this->getValue($parsedKey);
                       
            if($result !== false)
            {
                /// postavi u apc ako moze i treba
                if($with_apc === true && $this->apc() !== null)
                {
                    $this->apc()->set($parsedKey,$result,$apc_cache_time);
                }
                
                /// postavi u memcache ako moze i treba
                if(Yii::app()->configManager->get('admin.use_memcache') === true)
                {
                    $this->memcacheComponent->set($this->memcacheComponent_user.$parsedKey, $this->serializeValue($result));
                }
            }
        }
        
        /* ukoliko nema postavlja se cachecalc */
//        if ($result===false)
//        {
            $this->setCacheCalcAfterMiss($parsedKey);
//        }
                        
        return $result;
    }
    
    /**
     * Funkcija koja postavlja kesiranu vrednost za dati kljuc
     * 
     * @param (object/string) $key - kljuc(id) po kojem ce da se pretrazuje kesiranje;
     * @param (array) $value - rezultat kesiranja
     * @param (array of strings) $keywords - kljucne reci po kojima ce se vrsiti devalidacija cache-a
     * @param (bool) $is_valid - da li nakon postavljanja kesa je on validan ili ne
     */
    public function set($key, $value, $models_array, $is_valid = false)
    {        
        $parsedKey = $this->parseKey($key);
        $calc_time = $this->getCacheExecTime($parsedKey);
        $parsedValue = $this->parseValue($value);
        $keywords = $this->generateKeywords($models_array);
        $parsed_is_valid = $this->parseIsValid($is_valid);
        
        $this->setValue($parsedKey, $parsedValue, $keywords, $parsed_is_valid, $calc_time);
    }
    
    public function devalidateByKey($key)
    {
        $parsedKey = $this->parseKey($key);
        
        $this->devalidateKey($parsedKey);
    }
    
    public function devalidateById($id)
    {
        $this->devalidateId($id);
    }
    
    public function deleteById($id)
    {
        $this->deleteId($id);
    }
    
    public function resetById($id)
    {
        $this->resetId($id);
    }
    
    public function getCachedDatabaseSize()
    {
        $sql = "SELECT ".$this->cacheSchema.".get_cache_size();";
        $result = $this->db()->createCommand($sql)->queryScalar();
        
        return $result;
    }
    
    public function getCachedRowsCount()
    {
        $sql = "SELECT COUNT(*) FROM $this->cacheSchema.$this->cacheTable;";
        $result = $this->db()->createCommand($sql)->queryScalar();
        return $result;
    }
    
    public function getMaxSize()
    {
        return $this->getMaxSizeBytes();
    }
    
    public function flushCache()
    {
        $sql = "DELETE FROM $this->cacheSchema.$this->cacheTable;";
        $this->db()->createCommand($sql)->execute();
    }
    
    /**  
     * 	brise podatke iz tabele cache.sima_cache_table
     *  NA SVAKIH $older_than
     * 
     * $older_than string formata: {broj}{vreme}
     * gde {vreme} je:
     *  - h - za sat
     *  - d - za dan
     *  - w - za nedelju
     *  - month - za mesec
     *  - y - za godinu
     * */
    public function deleteOlderThan($older_than)
    {
        $simaCacheTable = $this->schemaTable();
        $result = Yii::app()->db->createCommand()->delete($simaCacheTable, "last_access<(now()-INTERVAL '".$older_than."')");
        return $result;
    }
    
    public function vacuumFull()
    {
        $sql = "VACUUM FULL $this->cacheSchema.$this->cacheTable;";
        $this->db()->createCommand($sql)->execute();
    }
    
    public function reindex()
    {
        $sql = "REINDEX table $this->cacheSchema.$this->cacheTable;";
        $this->db()->createCommand($sql)->execute();
    }
    
    public function db()
    {
        return Yii::app()->{$this->dbComponent};
    }
    
    public function getschemaTable()
    {
        return $this->schemaTable();
    }
    
    public function schemaTable()
    {
        return $this->cacheSchema.'.'.$this->cacheTable;
    }
    
    public function apc()
    {
        if($this->apcComponent === null)
        {
            return null;
        }
        
        return Yii::app()->{$this->apcComponent};
    }
    
    /**
     * From array parameter generates array of strings which represent keywords
     * 
     * @param (array) $array - svaki element je oblika:
     *              * model ili - konkretan model po kome se kreira kljuc
     *              * array     - niz ciji je prvi element model, a drugi element su relacije iz modela prvog elementa 
     *                              oblika: 
     *                              * string ili
     *                              * array of strings
     *              { Model || array( Model, {string || array[string+]} ) }
     * @return (array of strings)
     */
    private function generateKeywords($array)
    {
        $result = "";
        
        if(!is_array($array))
        {
            throw New Exception('Expected array');
        }
        
        for ($i = 0; $i < count($array); ++$i) 
        {
            $elem = $array[$i];
            $keyword_type = gettype($elem);
            
            $keywords = null;
            
            if( $keyword_type === "object" )
            {
                $keywords = $this->modelToKeywords($elem);
            }
            else if ( $keyword_type === "array" )
            {
                $keywords = $this->modelArrayToKeywords($elem[0], $elem[1]);
            }
            else
            {
                throw New Exception('Expected model or array');
            }
                        
            if($keywords === null or empty($keywords) or count($keywords) === 0)
            {
                throw New Exception('keyword is not suposed to be empty');
            }
            
            foreach($keywords as $k)
            {
                if(!empty($result))
                {
                    $result .= " ";
                }
                $result .= $k;
            }
        }
                
        return $result;
    }
    
    /* PRIVATE */
    
    /**
     * Funkcija koja iz baze trazi vrednost za prosledjeni kljuc
     * 
     * @param string $key - parsiran kljuc po kojem se pretrazuje kes
     * @return boolean - false ukoliko nije nadjena kesirana vrednost
     */
    private function getValue($key)
    {
        try
        {
            $_trace_cat = 'SIMA.CACHE.getValue';
            
            $result = false;
            
            $sql = "SELECT * FROM $this->cacheSchema.get_cache(:table, :key)";
            $schema_table = $this->schemaTable();
            $command = $this->db()->createCommand($sql);
            $command->bindParam(':table', $schema_table);
            $command->bindParam(':key', $key);
            $cache_value = $command->queryScalar();
            
            if( $cache_value !== false )
            {
                Yii::beginProfile('unserialize',$_trace_cat);
                $result = $this->unserializeValue($cache_value);
                Yii::endProfile('unserialize',$_trace_cat);
            }
                        
            return $result;
        }
        catch(Exception $e)
        {
            error_log(__METHOD__.' - '.$e->getMessage());
            return false;
        }
    }
    
    private function unserializeValue($value)
    {
        return unserialize(base64_decode($value));
    }
    
    private function serializeValue($value)
    {
        return base64_encode(serialize($value));
    }
    
    private function setValue($key, $value, $keywords, $is_valid, $calc_time)
    {
        $sql = "SELECT * FROM $this->cacheSchema.set_cache(:table, :key, :value, :keywords, :is_valid, :calc_time);";
        $command = $this->db()->createCommand($sql);
        $schema_table = $this->schemaTable();
        $command->bindParam(':table', $schema_table);
        $command->bindParam(':key', $key);
        $command->bindParam(':value', $value);
        $command->bindParam(':keywords', $keywords);
        $command->bindParam(':is_valid', $is_valid);
        $command->bindParam(':calc_time', $calc_time);
        $command->execute();
    }
    
    private function devalidateKey($key)
    {
        $sql = "SELECT * FROM $this->cacheSchema.devalidate_cache_by_key(:table, :key);";
        $command = $this->db()->createCommand($sql);
        $schema_table = $this->schemaTable();
        $command->bindParam(':table', $schema_table);
        $command->bindParam(':key', $key);
        $command->execute();
    }

    /**
     * 
     * @param int id
     */
    private function devalidateId($id)
    {
        $sql = "SELECT * FROM $this->cacheSchema.devalidate_cache_by_id(:table, :id);";
        $command = $this->db()->createCommand($sql);
        $schema_table = $this->schemaTable();
        $command->bindParam(':table', $schema_table);
        $command->bindParam(':id', $id);
        $command->execute();
    }
    
    /**
     * 
     * @param int id
     */
    private function deleteId($id)
    {
        $sql = "SELECT * FROM $this->cacheSchema.delete_cache_by_id(:table, :id);";
        $command = $this->db()->createCommand($sql);
        $schema_table = $this->schemaTable();
        $command->bindParam(':table', $schema_table);
        $command->bindParam(':id', $id);
        $command->execute();
    }
    
    /**
     * 
     * @param int id
     */
    private function resetId($id)
    {
        $sql = "SELECT * FROM $this->cacheSchema.reset_cache_by_id(:table, :id);";
        $command = $this->db()->createCommand($sql);
        $schema_table = $this->schemaTable();
        $command->bindParam(':table', $schema_table);
        $command->bindParam(':id', $id);
        $command->execute();
    }
    
    /**
     * Funkcija koja za prosledjeni kljuc kreira vrednost koja ce biti smestena u bazi
     * 
     * @param (object/string) $key - vrednost koju treba parsirati u validnu vrednost kljuca za bazu
     * @return string - parsirana vrednost kljuca
     * @throws Exception
     */
    protected function parseKey($key)
    {
        $parsed_key = false;
                
        $key_type = gettype($key);
                
        if( $key_type === "object" )
        {
            $parsed_key = md5($this->keyPrefix.serialize($key));
        }
        else if($key_type === "string")
        {
            $parsed_key = $this->keyPrefix.'_'.$key;
        }
        else
        {
            throw new Exception('Unsuppored key type: '.$key_type);
        }
        
//        $parsed_key = base_convert($parsed_key, 16, 10);
        
        return $parsed_key;
    }
    
    /**
     * Funkcija vraca parsiranu vrednost za kesiranje u string obliku
     * 
     * @param array $value rezultat kesiranja
     * @return type serializovana vrednost
     * @throws Exception
     */
    private function parseValue($value)
    {
        $_trace_cat = 'SIMA.CACHE.parseValue';
        
        Yii::beginProfile('unserialize',$_trace_cat);
        $parsed_value = $this->serializeValue($value);
        Yii::endProfile('unserialize',$_trace_cat);
        
        return $parsed_value;
    }
    
    /**
     * 
     * @param type $is_valid
     * @return type
     */
    private function parseIsValid($is_valid)
    {
        if($is_valid === true)
        {
            $is_valid = 'TRUE';
        }
        else
        {
            $is_valid = 'FALSE';
        }
                
        return $is_valid;
    }
    
    /**
     * Funkcija koja proverava validnost parametra SIMACache -> maxSize
     * parametar mora biti oblika: 
     * {BROJ}{VELICINA}
     * gde BROJ predstavlja cifru, 
     * a VELICINA predstavlja neko od slova 'k' ili 'm' ili 'g'
     * koja oznacavaju kilobajte, megabajte, gigabajte, respektivno
     * 
     * primer: 512m
     * predstavlja da maksimalna velicina kesa moze biti 512 megabajta
     * 
     * @return boolean
     * @throws Exception
     */
    private function validMaxSize()
    {
        if(strlen($this->maxSize) <= 1)
        {
            throw new Exception('Invalid max size1');
        }
        
        $num = substr($this->maxSize, 0, strlen($this->maxSize)-1);
        $letter = substr($this->maxSize, strlen($this->maxSize)-1);
        
        if(!is_numeric($num))
        {
            throw new Exception('Invalid max size2');
        }
        
        if( $letter !== 'k' 
            && $letter !== 'm'
            && $letter !== 'g')
        {
            throw new Exception('Invalid max size3');
        }
        
        return true;
    }
    
    /**
     * Funkcija koja vraca max size baze u bajtovima
     */
    private function getMaxSizeBytes()
    {
        $this->validMaxSize();
                
        $num = substr($this->maxSize, 0, strlen($this->maxSize)-1);
        $letter = substr($this->maxSize, strlen($this->maxSize)-1);
        $multiplicator = 0;
                
        if($letter === 'k')
        {
            $multiplicator = 1024;
        }
        else if($letter === 'm')
        {
            $multiplicator = 1024 * 1024;
        }
        else if($letter === 'g')
        {
            $multiplicator = 1024 * 1024 * 1024;
        }
        
        return $num * $multiplicator;
    }
    
    /**
     * Funkcija koja uklanja karaktere koji su specijalni za kljucne reci ('|', ' ');
     * 
     * @param string $string - ulazni string
     * @return string - string sa uklonjenim specijalnim karakterima
     */
    private function removeSpecialCharacters($string)
    {
        $string_replaced_vertical_lines = str_replace('|', '', $string);
        $string_replaced_spaces = str_replace(' ', '', $string_replaced_vertical_lines);
        $result = $string_replaced_spaces;
        
        return $result;
    }
    
    private function tableNameFix($string)
    {
        $result = $this->removeSpecialCharacters($string);
        
        if(strpos($result, ".") === false)
        {
            $result = "public.".$result;
        }
        
        return $result;
    }
    
    private function modelToKeywords($model)
    {
        $keywords = array();
        
        $table = $this->tableNameFix($model->tableName());
        if (isset($model->id))
        {
            $value = $this->removeSpecialCharacters($model->id);

            $keyword = $table.'|id|'.$value;
            $keywords[] = $keyword;
        }
        else
        {
            $primaryKey = $model->getPrimaryKey();
            if ($primaryKey !== NULL)
            {
                foreach ($primaryKey as $key => $val)
                {
                    $col = $this->removeSpecialCharacters($key);
                    $id = $this->removeSpecialCharacters($val);

                    $keyword = $table.'|'.$col.'|'.$id;
                    $keywords[] = $keyword;
                }
            }
        }
        
        return $keywords;
    }
    
    /**
     * 
     * @param type $array - oblika:
     *              * prvi element je model
     *              * svi ostali elemeneti su stringovi koji predstavljaju relacija unutar modela
     * @return string array - niz kljucnih reci
     */
    private function modelArrayToKeywords($model, $relations)
    {
        if(!is_array($relations))
        {
            $relations = array($relations);
        }
                        
        $result = $this->modelToKeywords($model);
                
        $model_relations = $model->relations();
        
        $keywords = array();
        foreach($relations as $relation_name)
        {
            $dotpos = strpos($relation_name, ".");
            if($dotpos !== false && $dotpos !== strlen($relation_name)-1)
            {   
                $rest = substr($relation_name, $dotpos+1);
                $relation_name = substr($relation_name, 0, $dotpos);
                
                $relation_models = $model->$relation_name;
                
                if($relation_models !== null)
                {
                    if(!is_array($relation_models))
                    {
                        $relation_models = array($relation_models);
                    }

                    foreach($relation_models as $rm)
                    {
                        $keywords = array_merge($keywords, $this->modelArrayToKeywords($rm, $rest));
                    }
                }
            }
            
            $relation = $model_relations[$relation_name];
            if($relation[0] === $model::HAS_ONE || $relation[0] === $model::HAS_MANY)
            {
                $table = $this->tableNameFix($relation[1]::model()->tableName());
                $col = $this->removeSpecialCharacters($relation[2]);
                $val = $this->removeSpecialCharacters($model->id);

                $keywords[] = $table.'|'.$col.'|'.$val;
            }
            else if($relation[0] === $model::BELONGS_TO)
            {
                $table = $this->tableNameFix($relation[1]::model()->tableName());
                $col = 'id';
                $model_rel = $relation[2];
                $val = $this->removeSpecialCharacters($model->$model_rel);

                $keywords[] = $table.'|'.$col.'|'.$val;
            }
            else if($relation[0] === $model::MANY_MANY)
            {
                $temp_arr = $arr = explode("(", $relation[2], 2);
                $parameters = rtrim($temp_arr[1], ')');
                $params_array = explode(",", $parameters, 2);

                $table = $this->tableNameFix($temp_arr[0]);

                $keyword = array();

                $table1 = $table;
                $col1 = $params_array[0];
                $val1 = $this->removeSpecialCharacters($model->id);
                $keyword[] = $table1.'|'.$col1.'|'.$val1;

                $table2 = $table;
                $col2 = $params_array[1];
                $val2 = '*';
                $keyword[] = $table2.'|'.$col2.'|'.$val2;

                $table3 = $this->tableNameFix($relation[1]::model()->tableName());
                $col3 = 'id';
                $val3 = '*';
                $keyword[] = $table3.'|'.$col3.'|'.$val3;
            }
            else
            {
                throw new Exception("not suposed to be called for this");
            }
        }
                
        foreach($keywords as $k)
        {
            $result[] = $k;
        }
                
        return $result;
    }
    
    private function setCacheCalcAfterMiss($key)
    {
        $this->cacheCalcTime[$key] = microtime(true);
    }
    
    /**
     * Izracunava vreme izvrsavanja u milisekundama.
     * 
     * Uzima vreme miss-a sacuvano u mikrosekundama;
     * Oduzima od trenutnog vremena u mikrosekundama;
     * Vraca vreme u milisekundama.
     * 
     * @param type $key - kljuc kesa
     * @return float - broj milisekundi
     */
    private function getCacheExecTime($key)
    {
        $result = 0;
        if(isset($this->cacheCalcTime[$key]))
        {
            $result = (microtime(true) - $this->cacheCalcTime[$key]);
            $result *= 1000;
        }
        return $result;
    }
    
    private function setupMemCachedComponent()
    {
        $memcache_data = Yii::app()->configManager->get('admin.memcache_data');
        
        $memcache_data_split = explode(':', $memcache_data);
        
        if(count($memcache_data_split) !== 3)
        {
            throw new Exception('invalid memcache data (host:port:user)');
        }
        
        $memcache_host = $memcache_data_split[0];
        $memcache_port = (int)($memcache_data_split[1]);
        $memcache_user = $memcache_data_split[2];
        
        if(!is_null($this->memcacheComponent)) /// ako imamo vec postavljenu komponentu
        {
            if(
                $this->memcacheComponent_host !== $memcache_host
                || $this->memcacheComponent_port !== $memcache_port
                || $this->memcacheComponent_user !== $memcache_user
            ) /// ako se promenili conf podaci
            {
                $this->memcacheComponent->quit();
            }
            else
            {
                return;
            }
        }
        else
        {
            $this->memcacheComponent = new Memcached();
        }
        
        $this->memcacheComponent_host = $memcache_host;
        $this->memcacheComponent_port = $memcache_port;
        $this->memcacheComponent_user = $memcache_user;
        
        $this->memcacheComponent->addServer($memcache_host, $memcache_port);
        
        $sql = "SELECT memcache_server_add('$memcache_host:$memcache_port');";
        $command = $this->db()->createCommand($sql);
        $command->execute();
    }
}
