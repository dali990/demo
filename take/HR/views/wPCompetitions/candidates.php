<div class='sima-layout-panel _splitter _vertical'>
    <div class='partner sima-layout-panel'
         data-sima-layout-init='<?=CJSON::encode([
                'proportion' => 0.6
            ])?>'>
        <?php
            $this->widget('SIMAGuiTable', [
                'id' => 'wpc'.$uniq,
                'model' => EmployeeCandidate::model(),                
                'add_button' => true,
                'setRowSelect' => "on_select$uniq",
            ]);            
        ?>
    </div>
    <div class='partner additional_info sima-layout-panel'
         data-sima-layout-init='<?=CJSON::encode([
                'proportion' => 0.4
            ])?>'>
        <?php 
            $this->widget('ext.SIMATabs.SIMATabs', array(
                'id'=>'tabs'.$uniq,
                'list_tabs_model_name' => 'EmployeeCandidate',
//                'list_tabs_populate' => false,
                'default_selected'=>'files',
                'show_tabs' => ['files']
            ));  
        ?>
    </div>
</div>
<script type='text/javascript'>
    /*prva tabela se popunjava osobama koje su zaposlene u SET-u, a druga se ostavlja prazna*/
    function on_select<?php echo $uniq; ?>(obj)
    {
        $('#tabs<?php echo $uniq?>').simaTabs('set_list_tabs_params',{
            list_tabs_trigger_tag: sima.getTag('<?php echo SIMAHtml::getTag(EmployeeCandidate::model()); ?>', obj.attr('model_id')),
            list_tabs_get_params:{id:obj.attr('model_id')}
        });
    }
</script>
