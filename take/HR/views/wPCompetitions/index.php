<div class='sima-layout-panel _splitter _vertical'>
    <div class='partner sima-layout-panel'
         data-sima-layout-init='<?=CJSON::encode([
                'proportion' => 0.15
            ])?>'>
        <?php
            $this->widget('base.extensions.SIMAGuiTable.SIMAGuiTable', [
                'id' => 'wpc'.$uniq,
                'model' => WorkPositionCompetition::model(),     
                'fixed_filter' => $fixed_filter,
                'add_button' => $add_button,
                'setRowSelect' => ['sima.hr.onWorkPositionCompetitionSelect', $uniq, SIMAHtml::getTag(WorkPositionCompetition::model())],
                'setRowUnSelect' => ['sima.hr.onWorkPositionCompetitionUnSelect', $uniq],
                'setMultiSelect' => ['sima.hr.onWorkPositionCompetitionUnSelect', $uniq]
            ]);            
        ?>
    </div>

    <div class='partner additional_info sima-layout-panel'
         data-sima-layout-init='<?=CJSON::encode([
//                'min_width' => 500,
//                'max_width' => 1000,
                'proportion' => 0.85
            ])?>'>
        <?php 
            $this->widget('ext.SIMATabs.SIMATabs', array(
                'id'=>'tabs'.$uniq,
                'list_tabs_model_name' => 'WorkPositionCompetition',
//                'list_tabs_populate' => false,
                'default_selected'=>'info',
            ));  
        ?>
    </div>
</div>
