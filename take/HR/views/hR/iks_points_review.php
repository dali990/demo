<?php
    $uniq = SIMAHtml::uniqid();
    $this->widget('SIMAGuiTable', array(
        'id' => "persons$uniq",
        'model' => Person::model(),            
        'columns_type' => 'iks_points_review',            
        'fixed_filter'=>array(                
            'filter_scopes'=>'iks_points_review'
        )
     ));
?>
