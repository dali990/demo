
<h4 style="text-align: center;">Obrazac 6</h4>
<h3 style="text-align: center;"><?php echo mb_strtoupper(SafeRecordController::$SAFE_RECORDS[6]['name'], 'UTF-8'); ?></h3>

<?php require_once 'safe_evd_header.php'; ?>

<table class="evd_safe_container_table">
    <tr>
        <th colspan="2">Ime i prezime zaposlenog koji je osposobljen za bezbedan i zdrav rad</th>
        <td colspan="2"><?php echo $employee ?></td>
    </tr>
    <tr>
        <th colspan="2">Naziv radnog mesta</th>
        <td colspan="2"><?php echo $rows->work_position->DisplayName ?></td>
    </tr>
    <tr>
        <th colspan="2">Slučaj, odnosno razlog izvršenog osposobljavanja zaposlenog za bezbedan i zdrav rad</th>
        <td colspan="2" style="text-align:left;"><?php echo $rows->reasons_for_enforcement_training_list; ?></td>
    </tr>
    <tr>
        <th colspan="2">Datum osposobljavanja za bezbedan i zdrav rad</th>
        <th colspan="2">Datum provere osposobljenosti za bezbedan i zdrav rad</th>
    </tr>
    <tr>
        <th>teorijskog</th>
        <th>praktičnog</th>
        <th>teorijske</th>
        <th>praktične</th>
    </tr>
    <tr>
        <td><?php echo $rows->date_theoretical_training ?></td>
        <td><?php echo $rows->date_practical_training ?></td>
        <td><?php echo $rows->date_check_theoretical_training ?></td>
        <td><?php echo $rows->date_check_practical_training ?></td>
    </tr>
    <tr>
        <th>Rizici sa kojima je zaposleni upoznat prilikom osposobljavanja za bezbedan i zdrav rad</th>
        <td colspan="3" style="text-align:left;"><?php
            foreach ($rows->work_position->risks as $value)
            {
                echo $value . "<br />";
            }
            ?>
        </td>
    </tr>
    <tr>
        <th>Konkretne mere za bezbedan i zdrav rad na tom radnom mestu</th>
        <td colspan="3" style="text-align:left;"><?php echo $rows->work_position->safe_actions ?></td>
    </tr>
    <tr>
        <th>Obaveštenja, uputstva ili instrukcije sa kojima je zaposleni upoznat radi obavaljanja procesa rada na bezbedan način</th>
        <td colspan="3" style="text-align:left;"><?php echo $rows->work_position->notice_safe ?></td>
    </tr>
</table>

<?php require_once 'safe_evd_footer.php'; ?>


