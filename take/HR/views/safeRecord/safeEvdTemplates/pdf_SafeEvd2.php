<h4 style="text-align: center;">Obrazac 2</h4>
<h3 style="text-align: center;"><?php echo mb_strtoupper(SafeRecordController::$SAFE_RECORDS[2]['name'], 'UTF-8'); ?></h3>

<?php require_once 'safe_evd_header.php'; ?>

<table class="evd_safe_container_table">
    <tr>
        <th width="6%">Redni broj</th>
        <th width="20%">Ime i prezime zaposlenog koji radi na radnom mestu sa povećanim rizikom</th>
        <th width="18%">Naziv radnog mesta sa povećanim rizikom</th>
        <th width="10%">Interval vršenja periodičnih lekarskih pregleda</th>
        <th width="10%">Datum kada treba da se vrši sledeći lekarski pregled zaposlenog</th>
        <th width="10%">Broj lekarskog izveštaja</th>
        <th width="12%">Ocena zdravstvene sposobnosti</th>
        <th width="14%">Preduzete mere (raspoređen na drugo radno mesto - poslove)</th>
    </tr>
    <?php
        $i = 1;
        foreach ($persons as $person)
        {
            $records_cnt = $person->safe_evd_2_records_cnt;
        ?>
        <?php            
            $work_positions = SafeRecordController::getSafeEvd2WorkPositions($person->safe_evd_2_records);            
            $j = 1;
            foreach ($work_positions as $work_position_key=>$work_position_value)
            {
                $k = 1;
                foreach ($work_position_value['records'] as $safe_evd_2_record) 
                {                    
                    ?>
                        <tr nobr="true" height="15px">
                            <?php if ($j === 1 && $k === 1) { ?>
                                <td width="6%" rowspan="<?php echo $records_cnt; ?>"><?php echo $i; ?></td>
                                <td width="20%" rowspan="<?php echo $records_cnt; ?>"><?php echo $person->DisplayName; ?></td>
                            <?php } ?>
                            <?php
                                if ($k === 1)
                                {
                            ?>
                                    <td width="18%" rowspan="<?php echo $work_position_value['cnt']; ?>"><?php echo $safe_evd_2_record->work_position->DisplayName; ?></td>
                            <?php
                                }
                            ?>
                            <td width="10%"><?php echo isset($safe_evd_2_record->medical_report_period)?$safe_evd_2_record->medical_report_period->DisplayName:''; ?></td>
                            <td width="10%"><?php echo $safe_evd_2_record->date_next_report; ?></td>
                            <td width="10%"><?php echo $safe_evd_2_record->report_code; ?></td>
                            <td width="12%"><?php echo $safe_evd_2_record->getAttributeDisplay('healthy_mark'); ?></td>
                            <td width="14%"><?php echo $safe_evd_2_record->actions; ?></td>
                        </tr>
                    <?php
                    $k++;
                }                
                $j++;
            }
        ?>
        <?php $i++; } ?>   
</table>

<?php require_once 'safe_evd_footer.php'; ?> 
