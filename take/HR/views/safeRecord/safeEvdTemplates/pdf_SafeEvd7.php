<h4 style="text-align: center;">Obrazac 7</h4>
<h3 style="text-align: center;"><?php echo mb_strtoupper(SafeRecordController::$SAFE_RECORDS[7]['name'], 'UTF-8'); ?></h3>


<?php require_once 'safe_evd_header.php'; ?>

<table class="evd_safe_container_table">
    <tr>
        <td width="100%"><?php echo $work_position?></td>
    </tr>
    <tr>
        <th width="100%">Naziv radnog mesta na kojem se koriste opasne materije</th>        
    </tr>
    <tr><th style="border:none;"></th></tr>
</table>
<br />

<table class="evd_safe_container_table">
    <thead>
        <tr>
            <th width="6%" rowspan="2">Redni broj</th>
            <th width="13%" rowspan="2">Naziv opasne materije koja se korsiti u toku rada na tom radnom mestu</th>
            <th width="11%" rowspan="2">Hemijsko ime opasne materije</th>
            <th width="21%" colspan="3">Oznaka opasnosti - brojčana oznaka opasne materije</th>
            <th width="10%" rowspan="2">Klasa opasne materije</th>
            <th width="13%" rowspan="2">Način upotrebe, odnosno korišćenja u toku rada</th>
            <th width="13%" rowspan="2">Dnevna količina opasne materije koja se koristi na tom radnom mestu</th>
            <th width="13%" rowspan="2">Napomena</th>
        </tr>
        <tr>
            <th>UN broj</th>
            <th>ADR broj</th>
            <th>RID broj</th>
        </tr>        
    </thead>
    <tbody>
        <?php $i = 1;
        foreach ($rows as $row)
        { ?>
            <tr nobr="true">
                <td width="6%"><?php echo $i++ ?></td>
                <td width="13%"><?php echo $row->name ?></td>
                <td width="11%"><?php echo $row->hemical_name ?></td>
                <td width="7%"><?php echo $row->un_number ?></td>
                <td width="7%"><?php echo $row->adr_number ?></td>
                <td width="7%"><?php echo $row->rid_number ?></td>
                <td width="10%"><?php echo $row->class_material ?></td>
                <td width="13%"><?php echo $row->mode_use ?></td>
                <td width="13%"><?php echo $row->quantity ?></td>
                <td width="13%"><?php echo $row->comment ?></td>
            </tr>
        <?php } ?>
    </tbody>
</table>

<?php require_once 'safe_evd_footer.php'; ?> 
