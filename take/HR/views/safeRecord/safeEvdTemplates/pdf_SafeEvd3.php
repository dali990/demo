<h4 style="text-align: center;">Obrazac 3</h4>
<h3 style="text-align: center;"><?php echo mb_strtoupper(SafeRecordController::$SAFE_RECORDS[3]['name'], 'UTF-8'); ?></h3>

<?php require_once 'safe_evd_header.php'; ?>

<table class="evd_safe_container_table">
    <tr>
        <th width="6%">Redni broj</th>
        <th width="15%">Ime i prezime povređenog</th>
        <th width="14%">Vreme nastanka povrede na radu (datum, dan u sedmici, čas)</th>
        <th width="14%">Radno mesto na kome se povreda dogodila</th>
        <th width="12%">Vrsta povrede (pojedinačna ili kolektivna)</th>
        <th width="15%">Ocena težine povrede (laka, teška, smrtna povreda na radu, odnosno povreda na radu zbog koje zaposleni nije
            sposoban za rad više od tri uzastopna radna dana)</th>
        <th width="12%">Izvor povrede na radu - materijalni uzročnik (međunarodna šifra)</th>
        <th width="12%">Uzrok povrede na radu - način povređivanja (međunarodna šifra)</th>
    </tr>
    <?php foreach ($rows as $row){?>
        <tr nobr="true">
            <td><?php echo $row->display_evd_number ?? $row->id?></td>
            <td><?php echo $row->employee?></td>
            <td><?php echo $row->date?></td>
            <td><?php echo $row->work_position->DisplayName?></td>
            <td><?php echo SafeEvd3Lists::$injury_type_list[$row->type_injury] ?? ''?></td>
            <td><?php echo SafeEvd3Lists::$mark_injury_list[$row->mark_injury] ?? ''?></td>
            <td><?php echo !empty($row->source_injury) ? $row->source_injury->DisplayName : '' ?></td>
            <td><?php echo !empty($row->mode_injury) ? $row->mode_injury->DisplayName : '' ?></td>
        </tr>
    
    <?php } ?>
</table>

<?php require_once 'safe_evd_footer.php'; ?>
