<h4 style="text-align: center;">Obrazac 11</h4>
<h3 style="text-align: center;"><?php echo mb_strtoupper(SafeRecordController::$SAFE_RECORDS[11]['name'], 'UTF-8'); ?></h3>

<?php require_once 'safe_evd_header.php'; ?>

<table class="evd_safe_container_table">
    <thead>
        <tr>
            <th width="6%" rowspan="3">Redni broj</th>
            <th width="15%" rowspan="3">Povreda na radu koja je prijavljena nadležnoj inspekciji rada i
                nadležnom organu za unutrašnje poslove (redni broj iz Obrasca 3)</th>
            <th width="12%" rowspan="3">Datum podnošenja prijave</th>
            <th width="12%" rowspan="3">Način podnošenja prijave</th>
            <th width="40%" colspan="4">Nadležni organ kome je prijava podneta</th>
            <th width="15%" rowspan="3">Napomena</th>
        </tr>
        <tr>
            <th width="20%" colspan="2">Inspekcija rada</th>
            <th width="20%" colspan="2">OUP</th>

        </tr>
        <tr>
            <th width="10%">Sedište mesno nadležnog organa kome je prijava podneta</th>
            <th width="10%">Ime i prezime lica koje je prijavu primilo</th>
            <th width="10%">Sedište mesno nadležnog organa kome je prijava podneta</th>
            <th width="10%">Ime i prezime lica koje je prijavu primilo</th>
        </tr>
    </thead>
    <tbody>
        <?php $i = 1;
        foreach ($rows as $row)
        { ?>
            <tr nobr="true">
                <td width="6%" rowspan="2"><?php echo $i++ ?></td>
                <td width="15%" rowspan="2"><?php echo $row->safe_evd_3 ?></td>
                <td width="12%"><?php echo $row->date_request ?></td>
                <th width="12%">pismeno</th>
                <td width="10%"><?php echo $row->inspection_company ?></td>
                <td width="10%"><?php echo $row->inspection_person ?></td>
                <td width="10%"><?php echo $row->oup_company ?></td>
                <td width="10%"><?php echo $row->oup_person ?></td>
                <td width="15%"><?php echo $row->comment ?></td>
            </tr>
            <tr nobr="true">
                <td width="12%"><?php echo $row->date_request_writing ?></td>
                <th width="12%">usmeno</th>
                <td width="10%"><?php echo $row->inspection_company_writing ?></td>
                <td width="10%"><?php echo $row->inspection_person_writing ?></td>
                <td width="10%"><?php echo $row->oup_company_writing ?></td>
                <td width="10%"><?php echo $row->oup_person_writing ?></td>
                <td width="15%"><?php echo $row->comment ?></td>
            </tr>
        <?php } ?>
    </tbody>
</table>

<?php require_once 'safe_evd_footer.php'; ?> 
