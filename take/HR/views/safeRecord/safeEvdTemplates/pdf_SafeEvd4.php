<h4 style="text-align: center;">Obrazac 4</h4>
<h3 style="text-align: center;"><?php echo mb_strtoupper(SafeRecordController::$SAFE_RECORDS[4]['name'], 'UTF-8'); ?></h3>

<?php require_once 'safe_evd_header.php'; ?>

<table class="evd_safe_container_table">
    <tr>
        <th width="6%">Redni broj</th>
        <th width="15%">Ime i prezime obolelog od profesionalnog oboljenja</th>
        <th width="16%">Naziv radnog mesta na kome je oboleli od profesionalnog oboljenja radio kada je utvrđeno profesionalno oboljenje</th>
        <th width="16%">Dijagnoza i međunarodna šifra profesionalnog oboljenja</th>
        <th width="16%">Naziv zdravstvene ustanove koja je izvršila pregled obolelog od profesionalnog oboljenja</th>
        <th width="15%">Stepen telesnog oštećenja</th>
        <th width="16%">Preostala radna sposobnost obolelog od profesionalnog oboljenja za dalji rad</th>
    </tr>
    <?php foreach ($rows as $row){?>
        <tr nobr="true">
            <td><?php echo $row->id?></td>
            <td><?php echo $row->person?></td>
            <td><?php echo $row->work_position->DisplayName?></td>
            <td><?php echo $row->international_diagnosis->DisplayName?></td>
            <td><?php echo $row->company?></td>
            <td><?php echo $row->degree?></td>
            <td><?php echo $row->person_ability?></td>
        </tr>
    
    <?php } ?>
</table>

<?php require_once 'safe_evd_footer.php'; ?>

