<h4 style="text-align: center;">Obrazac 8</h4>
<h3 style="text-align: center;"><?php echo mb_strtoupper(SafeRecordController::$SAFE_RECORDS[8]['name'], 'UTF-8'); ?></h3>

<?php require_once 'safe_evd_header.php'; ?>

<table class="evd_safe_container_table">
    <tr>
        <th width="6%">Redni broj</th>
        <th width="35%">Ispitivani parametri radne okoline (hemijske, biološke i fizičke štetnosti - osim jonizujućih zračenja, mikroklima i osvetljenost)</th>
        <th width="15%">Broj stručnog nalaza ili izveštaja</th>
        <th width="13%">Datum ispitivanja</th>
        <th width="13%">Datum sledećeg ispitivanja</th>
        <th width="18%">Napomena</th>
    </tr>
    <?php $i=1; foreach ($rows as $row){?>
        <tr nobr="true">
            <td><?php echo $i++?></td>
            <td><?php echo $row->data?></td>
            <td><?php echo $row->num_expert_report?></td>
            <td><?php echo $row->date_control?></td>
            <td><?php echo $row->date_next_control?></td>
            <td><?php echo $row->comment?></td>
        </tr>
    <?php } ?>
</table>

<?php require_once 'safe_evd_footer.php'; ?> 

