<h4 style="text-align: center;">Obrazac 10</h4>
<h3 style="text-align: center;"><?php echo mb_strtoupper(SafeRecordController::$SAFE_RECORDS[10]['name'], 'UTF-8'); ?></h3>

<?php require_once 'safe_evd_header.php'; ?>

<table class="evd_safe_container_table">
    <tr>
        <th width="6%">Redni broj</th>
        <th width="31%">Podaci o sredstvu i opremi za ličnu zaštitu na radu čiji je pregled odnosno ispitivanje izvršeno (vrsta, fabrički broj, godina proizvodnje, i dr.)</th>
        <th width="15%">Datum pregleda, odnosno ispitivanja</th>
        <th width="15%">Datum sledećeg pregleda, odnosno ispitivanja</th>
        <th width="15%">Potpis lica koje je izvršilo pregled, odnosno ispitivanje</th>
        <th width="18%">Napomena</th>
    </tr>
    <?php $i=1; foreach ($rows as $row){?>
        <tr nobr="true">
            <td><?php echo $i++?></td>
            <td><?php echo $row->data?></td>
            <td><?php echo $row->date_control?></td>
            <td><?php echo $row->date_next_control?></td>
            <td></td>
            <td><?php echo $row->comment?></td>
        </tr>
    <?php } ?>
</table>

<?php require_once 'safe_evd_footer.php'; ?> 
