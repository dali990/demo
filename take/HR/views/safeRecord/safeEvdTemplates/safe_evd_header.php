<table class="evd_safe_header_table" border=0>
    <tr>     
        <td style="font-weight:bold;"><?php echo Yii::app()->company->DisplayName; ?></td>      
        <td style="font-weight:bold;"><?= !empty(Yii::app()->company->main_address) ? Yii::app()->company->main_address->DisplayNameLong : '' ?></td>
        <td style="font-weight:bold;"><?php echo Yii::app()->company->PIB; ?></td>      
    </tr>  
    <tr>        
        <td style="font-size:2.4mm">Poslovno ime ili firma radnje poslodavca</td>        
        <td style="font-size:2.4mm">Adresa sedišta poslodavca</td>        
        <td style="font-size:2.4mm">PIB poslodavca</td>        
    </tr>
    <tr>
        <td></td><td></td><td></td>
    </tr>
</table>
<br />