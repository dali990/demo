<h4 style="text-align: center;">Obrazac 12</h4>
<h3 style="text-align: center;"><?php echo mb_strtoupper(SafeRecordController::$SAFE_RECORDS[12]['name'], 'UTF-8'); ?></h3>

<?php require_once 'safe_evd_header.php'; ?>

<table class="evd_safe_container_table">
    <tr>
        <th width="6%" rowspan="3">Redni broj</th>
        <th width="18%" rowspan="3">Profesionalno oboljenje koje je prijavljeno nadležnoj inspekciji rada (redni broj iz Obrasca 4)</th>
        <th width="12%" rowspan="3">Datum podnošenja prijave</th>
        <th width="15%" rowspan="3">Način podnošenja prijave</th>
        <th width="34%" colspan="2">Nadležni organ kome je prijava podneta</th>
        <th width="15%" rowspan="3">Napomena</th>
    </tr>
    <tr>
        <th width="34%" colspan="2">Inspekcija rada</th>
    </tr>
    <tr>
        <th width="17%">Sedište mesno nadležnog organa kome je prijava podneta</th>
        <th width="17%">Ime i prezime lica koje je prijavu primilo</th>
    </tr>
    <?php $i = 1;
    foreach ($rows as $row)
    { ?>
        <tr nobr="true">
            <td width="6%" rowspan="2"><?php echo $i++ ?></td>
            <td width="18%" rowspan="2"><?php echo $row->safe_evd_4 ?></td>
            <td width="12%"><?php echo $row->date_request ?></td>
            <th width="15%">pismeno</th>
            <td width="17%"><?php echo $row->inspection_company ?></td>
            <td width="17%"><?php echo $row->inspection_person ?></td>
            <td width="15%"><?php echo $row->comment ?></td>
        </tr>
        <tr nobr="true">
            <td width="12%"><?php echo $row->date_request_writing ?></td>
            <th width="15%">usmeno</th>
            <td width="17%"><?php echo $row->inspection_company_writing ?></td>
            <td width="17%"><?php echo $row->inspection_person_writing ?></td>
            <td width="15%"><?php echo $row->comment ?></td>
        </tr>

<?php } ?>
</table>

<?php require_once 'safe_evd_footer.php'; ?> 