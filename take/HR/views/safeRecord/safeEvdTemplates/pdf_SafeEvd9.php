
<style type='text/css'>
    .evd_safe_container_table_safe_evd_9 {
        border: 0.6mm solid black;
    }
    .evd_safe_container_table_safe_evd_9 .last_tr > td, .evd_safe_container_table_safe_evd_9 th {
        border-bottom: 0.6mm solid black;
    }
</style>

<h4 style="text-align: center;">Obrazac 9</h4>
<h3 style="text-align: center;"><?php echo mb_strtoupper(SafeRecordController::$SAFE_RECORDS[9]['name'], 'UTF-8'); ?></h3>

<?php require_once 'safe_evd_header.php'; ?>

<table class="evd_safe_container_table evd_safe_container_table_safe_evd_9">
    <tr>
        <th width="6%">Redni broj</th>
        <th width="38%">Podaci o opremi za rad čiji je pregled, odnosno ispitivanje izvršeno (vrsta, fabrički broj, godina proizvodnje, lokacija i namena)</th>        
        <th width="17%">Broj stručnog nalaza</th>
        <th width="13%">Datum pregleda, odnosno ispitivanja</th>
        <th width="13%">Datum sledećeg pregleda, odnosno ispitivanja</th>
        <th width="13%">Napomena</th>
    </tr>    
    <?php
        $i = 1;
        foreach ($rows as $row)
        {
            $records_cnt = $row->safe_evd_9_records_cnt;
        ?>
        <?php
            $j = 1;
            foreach ($row->safe_evd_9_records as $safe_evd_9_record)
            {
                ?>
                    <tr nobr="true" height="15px">
                        <?php if ($j === 1) { ?>
                            <td width="6%" rowspan="<?php echo $records_cnt; ?>"><?php echo $i; ?></td>
                            <td width="38%" rowspan="<?php echo $records_cnt; ?>">
                                <table>
                                    <tr>
                                        <td width="40%">Naziv mašine</td>
                                        <td width="60%"><?php echo $row->DisplayName; ?></td>
                                    </tr>
                                    <tr>
                                        <td width="40%">Fabrički broj</td>
                                        <td width="60%"><?php echo $row->serial_number; ?></td>
                                    </tr>
                                    <tr>
                                        <td width="40%">Proizvođač</td>
                                        <td width="60%"><?php echo isset($row->fixed_asset_type->company)?$row->fixed_asset_type->company->DisplayName:''; ?></td>
                                    </tr>
                                    <tr>
                                        <td width="40%">Godina proizv.</td>
                                        <td width="60%"><?php echo isset($row->year_built)?$row->year_built->year:''; ?></td>
                                    </tr>                    
                                    <tr>
                                        <td width="40%">Tip</td>
                                        <td width="60%"><?php echo !empty($row->fixed_asset_type->model)?$row->fixed_asset_type->model:''; ?></td>
                                    </tr>
                                    <tr>
                                        <td width="40%">Max. Nosivost</td>
                                        <td width="60%"><?php echo isset($row->vehicle)?$row->vehicle->capacity . ' kg':''; ?></td>
                                    </tr>
                                </table>
                            </td>
                        <?php } ?>
                        <td width="17%"><?php echo $safe_evd_9_record->num_expert_report; ?></td>
                        <td width="13%"><?php echo $safe_evd_9_record->date_control; ?></td>
                        <td width="13%"><?php echo $safe_evd_9_record->date_next_control; ?></td>
                        <td width="13%"><?php echo $safe_evd_9_record->comment; ?></td>
                    </tr>
                <?php
                $j++;
            }            
        ?>
    <?php $i++; } ?>
</table>

<?php require_once 'safe_evd_footer.php'; ?>
