<h4 style="text-align: center;">Obrazac 1</h4>
<h3 style="text-align: center;"><?php echo mb_strtoupper(SafeRecordController::$SAFE_RECORDS[1]['name'], 'UTF-8'); ?></h3>

<?php require_once 'safe_evd_header.php'; ?>

<table class="evd_safe_container_table">
    <tr>
        <th width="6%">Redni broj</th>
        <th width="27%">Naziv radnog mesta sa povećanim rizikom koje je utvrđeno aktom o proceni rizika</th>
        <th width="12%">Broj zaposlenih na tom radnom mestu</th>
        <th width="27%">Šifra opasnosti, odnosno štetnosti na osnovu kojih je utvrđeno radno mesto sa povećanim rizikom</th>
        <th width="28%">Napomena o specifičnim karakteristikama utvrđenih opasnosti, odnosno štetnosti i radnim postupcima u kojima se pojavljuju</th>
    </tr>
    <?php $i=1; foreach ($rows as $row){ ?>
        <?php
            $risks = '';
            foreach ($row->risks as $value)
            {
                $risks .= $value->code. ', ';
            }
            $risks = substr($risks, 0, -2);
        ?>
        <tr nobr="true">
            <td><?php echo $i++; ?></td>
            <td><?php echo $row->DisplayHtml; ?></td>
            <td><?php echo $row->number_person; ?></td>
            <td><?php echo $risks; ?></td>
            <td><?php echo $row->description; ?></td>
        </tr>
    
    <?php } ?>
</table>

<?php require_once 'safe_evd_footer.php'; ?> 

