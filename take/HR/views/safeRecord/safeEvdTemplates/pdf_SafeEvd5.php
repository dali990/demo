<h4 style="text-align: center;">Obrazac 5</h4>
<h3 style="text-align: center;"><?php echo mb_strtoupper(SafeRecordController::$SAFE_RECORDS[5]['name'], 'UTF-8'); ?></h3>

<?php require_once 'safe_evd_header.php'; ?>

<table class="evd_safe_container_table">
    <tr>
        <th width="6%">Redni broj</th>
        <th width="15%">Ime i prezime obolelog</th>
        <th width="16%">Naziv radnog mesta na kome je oboleli radio kada je utvrđeno oboljenje u vezi sa radom</th>
        <th width="16%">Dijagnoza i međunarodna šifra oboljenja u vezi sa radom</th>
        <th width="16%">Naziv zdravstvene ustanove koja je izvršila pregled obolelog</th>
        <th width="15%">Stepen telesnog oštećenja</th>
        <th width="16%">Preostala radna sposobnost obolelog za dalji rad</th>
    </tr>
    <?php foreach ($rows as $row){?>
        <tr nobr="true">
            <td><?php echo $row->id?></td>
            <td><?php echo $row->person?></td>
            <td><?php echo $row->work_position->DisplayName?></td>
            <td><?php echo $row->international_diagnosis->DisplayName?></td>
            <td><?php echo $row->company?></td>
            <td><?php echo $row->degree?></td>
            <td><?php echo $row->person_ability?></td>
        </tr>
    
    <?php } ?>
</table>

<?php require_once 'safe_evd_footer.php'; ?> 
