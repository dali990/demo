<?php

$uniq = SIMAHtml::uniqid();
$this->widget('SIMAGuiTable', [
    'id' => 'required_medical_ability'.$uniq,
    'model' => RequiredMedicalAbility::model(),
    'add_button' => true,
    'custom_buttons' => [
        Yii::t('BaseModule.Misc','TakeDataFromCodeBook') => [
            'func' => 'sima.adminMisc.syncRequiredMedicalAbilityFromCodebook(this)',
        ],
    ],
]);

?>