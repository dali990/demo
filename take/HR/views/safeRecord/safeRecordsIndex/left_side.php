
<div id="sima_safe_records_list<?php echo $uniq; ?>" class="sima-layout-panel">
    <table cellpadding="5" class="sima-safe-records-list-t">
        <thead>
            <tr class="sima-safe-record">
                <th width="80px" class="code">Oznaka</th>
                <th class="name">Naziv</th>
            </tr>
        </thead>
        <tbody>
            <?php
            foreach ($safe_records as $key => $safe_record)
            {
                ?>
                <tr class="sima-safe-record" onclick="on_select<?php echo $uniq; ?>($(this))" data-id="<?php echo $key; ?>">
                    <td class="code"><?php echo $safe_record['code']; ?></td>
                    <td class="name"><?php echo $safe_record['name']; ?></td>
                </tr>
                <?php
            }
            ?>
        </tbody>
    </table>
</div>

<script type="text/javascript">
    function on_select<?php echo $uniq; ?>(obj)
    {
        if (obj.hasClass('selected'))
        {
            obj.removeClass('selected');
            $('#sima_safe_record_tabs<?php echo $uniq; ?>').hide();
        }
        else
        {
            $('#sima_safe_records_list<?php echo $uniq ?>').find('tbody .sima-safe-record.selected').removeClass('selected');
            obj.addClass('selected');
            $('#sima_safe_record_tabs<?php echo $uniq; ?>').simaTabs('set_list_tabs_params', {
                list_tabs_action: 'HR/safeRecord/safeRecordTabs',
                list_tabs_get_params: {id: obj.data('id')}
            });
            $('#sima_safe_record_tabs<?php echo $uniq; ?>').show();
        }
    }
</script>