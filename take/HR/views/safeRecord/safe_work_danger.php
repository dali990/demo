<?php

$uniq = SIMAHtml::uniqid();
$this->widget('base.extensions.SIMAGuiTable.SIMAGuiTable', [
    'id' => 'safe_work_danger'.$uniq,
    'model' => SafeWorkDanger::model(),
    'add_button' => true,
    'custom_buttons' => [
        Yii::t('BaseModule.Misc','TakeDataFromCodeBook') => [
            'func' => 'sima.adminMisc.syncSafeWorkDangersFromCodebook(this)',
        ],
    ],
]);

?>