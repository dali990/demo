<?php $uniq=  SIMAHtml::uniqid();?>

<div id="work_positions<?php $uniq?>">
    <h4>Radna pozicija:</h4>
    <select id="positions<?php $uniq?>">
        <option value="">Izaberi poziciju</option>
        <?php
            $all_evd=  SafeEvd7::model()->findAll();
            foreach ($all_evd as $evd)
            {
                $position=  WorkPosition::model()->findByPk($evd->work_position_id);
             ?>
        <option value="<?php echo $position->id?>"><?php echo $position->DisplayName?></option>
             
        
           <?php }?>
    </select>
</div>
<div id="safe_evd_7<?php $uniq?>" class="scroll-container">
</div>

<script>
    
    $('select option').each(function() {
        $(this).prevAll('option[value="' + this.value + '"]').remove();
    });
    var new_safe_evd=function(){
        var pos = $("#positions<?php $uniq?>").val();
        if(pos!='')
        {
            sima.ajax.get('HR/safeRecord/safeRecordDocument',{
               'get_params':{
                            'Model':'SafeEvd7',
                            'work_position_id':pos,
               },
               'success_function':function(response){
                   $("#safe_evd_7<?php $uniq?>").html(response.html);
               }
            });
        }
        else
        {
             $("#safe_evd_7<?php $uniq?>").html("");
        }
    };
    
    $(function(){
           $("#positions<?php $uniq?>").change(new_safe_evd);
    });
    
 
    
</script>