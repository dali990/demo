<style type='text/css'>

.evd_safe_wrap {
    width: 193mm;
    margin: auto;
    padding: 0px;
}

.evd_safe_wrap table { 
    width: 100%;
}

.evd_safe_header_table {
    font-size:2.8mm;    
}

.evd_safe_header_table td, .evd_safe_header_table th {
    width: 33%;
    border: none;
    text-align: center;
}

.evd_safe_container_table {
    font-size:2.8mm;    
}

.evd_safe_container_table td {
    border: 0.3mm solid black;    
    border-collapse: collapse;
    text-align: center;
    vertical-align: top;
}

.evd_safe_container_table th {
    border: 0.3mm solid black;    
    border-collapse: collapse;
    text-align: center;
    font-weight: bold;
    vertical-align: top;
}

.evd_safe_footer_table{
    font-size:2.8mm;
}

.evd_safe_footer_table td{
    border: none;
}

.evd_safe_footer_table td.left {
    width: 50%;
    border-top: 0.3mm solid black;
    text-align: center;
}

.evd_safe_footer_table td.middle {
    width: 25%;
}

.evd_safe_footer_table td.right {
    width: 25%;
    border-top: 0.3mm solid black;
    text-align: center;
}

</style>