<?php
    $file_init_data = [];
    $medical_examination_referral_document_type_id = Yii::app()->configManager->get('HR.medical_examination_referral_document_type_id', false);
    if (!empty($medical_examination_referral_document_type_id))
    {
        $file_init_data['document_type_id'] = [
            'disabled',
            'init' => $medical_examination_referral_document_type_id
        ];
    }
    $this->widget('SIMAGuiTable', [
        'model'=> MedicalExaminationReferral::model(),
        'add_button'=> [
            'init_data'=>[
                'MedicalExaminationReferral'=>[
                    'referral_type'=>MedicalExaminationReferral::$SAFE_EVD_13
                ],
                'File' => $file_init_data
            ]
        ],    
        'fixed_filter' => [
            'referral_type'=>MedicalExaminationReferral::$SAFE_EVD_13,
            'filter_scopes'=>'noConnectionToSafeEvd13'
        ]
    ]);