<?php $uniq=  SIMAHtml::uniqid();?>

<div id="persons<?php $uniq?>">
    <h4>Zaposleni:</h4>
    <select id="employees<?php $uniq?>">
        <option value="">Izaberi zaposlenog</option>
        <?php
            $safe_evd=  SafeEvd6::model()->findAll();
            foreach ($safe_evd as $evd)
            {
                $person=  Person::model()->findByPk($evd->person_id);
             ?>
        <option value="<?php echo $person->id?>"><?php echo $person->DisplayName?></option>
             
        
           <?php }?>
    </select>
</div>

<div id="safe_evd_6<?php $uniq?>" class="scroll-container">
</div>

<script>
    
    $('select option').each(function() {
        $(this).prevAll('option[value="' + this.value + '"]').remove();
    });
    var new_safe_evd=function(){
        var employee_id = $("#employees<?php $uniq?>").val();
        if(employee_id!='')
        {
            sima.ajax.get('HR/safeRecord/safeRecordDocument',{
               'get_params':{
                            'Model':'SafeEvd6',
                            'employee_id':employee_id,
               },
               'success_function':function(response){
                   $("#safe_evd_6<?php $uniq?>").html(response.html);
               }
            });
        }
        else
        {
             $("#safe_evd_6<?php $uniq?>").html("");
        }
    };
    
    $(function(){
           $("#employees<?php $uniq?>").change(new_safe_evd);
    });
    
 
    
</script>




