<?php 
    if (Yii::app()->request->isAjaxRequest) Yii::app()->clientScript->clearScriptFiles(); 
?>
<?php $uniq=SIMAHtml::uniqid();
     
      $tab_id=SIMAHtml::uniqid();
             $this->widget('ext.SIMATabs.SIMATabs', array(
                    'id'=>$tab_id,
                    'list_tabs_action' => 'HR/absence/getAbsenceTabsForEmployee',
                    'list_tabs_get_params' => array('employee_id'=>$employee_id),                    
                    'list_tabs_populate' => true,
                    'default_selected'=>'absences_review',
                ));
?>

<?php if (Yii::app()->request->isAjaxRequest) Yii::app()->clientScript->clearScriptFiles(); ?>
       
