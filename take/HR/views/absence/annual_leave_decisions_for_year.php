
<?php 
    $uniq = SIMAHtml::uniqid();
    $this->widget('base.extensions.SIMAGuiTable.SIMAGuiTable', [
        'id' => 'annual_leave_decisions'.$uniq,
        'title' => Yii::t('HRModule.AnnualLeaveDecision', 'AnnualLeaveDecisions'),
        'model' => AnnualLeaveDecision::model(),
        'columns_type' => 'byYear',
        'add_button' => array(
            'init_data'=>array(
                'AnnualLeaveDecision'=>array(
                    'year_id'=>array(
                        'disabled',
                        'init'=>$year_id
                    ),
                )
            )
        ),
        'fixed_filter'=>array(
//            'ids'=>[323215, 321253],
            'year'=>array(
                'ids'=>$year_id
            ),
            'order_by'=>'employee.username ASC'
        )        
    ]);    
?>