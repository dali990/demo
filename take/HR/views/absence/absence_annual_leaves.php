
<?php $uniq = SIMAHtml::uniqid();?>

<?php echo $this->renderModelView($employee, 'active_annual_leave_decision');?>

<?php
    $this->widget('base.extensions.SIMAGuiTable.SIMAGuiTable', [
        'id'=>'absence_annual_leave'.$uniq,
        'model'=>AbsenceAnnualLeave::model(),
        'columns_type'=>'from_user',
        'add_button' => array(
            'init_data'=>array(
                'AbsenceAnnualLeave' => array(
                    'employee_id' => [
                        'disabled',
                        'init' => $employee->id
                    ],
                    'type' => Absence::$ANNUAL_LEAVE
                )
            ),            
        ),
        'fixed_filter'=>array(
            'employee' => array(
                'ids' => $employee->id
            ),
            'type' => Absence::$ANNUAL_LEAVE
        )
    ]);