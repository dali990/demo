<?php $uniq = SIMAHtml::uniqid(); ?>

<div id="<?php echo $uniq; ?>" class="sima-layout-panel _splitter _vertical">
    <div class='left_side sima-layout-panel'>
        <?php
            $this->widget('base.extensions.SIMAGuiTable.SIMAGuiTable', [
                'id' => 'years' . $uniq,
                'title' => Yii::t('HRModule.HRYear', 'LegalYears'),
                'model' => HRYear::model(),
                'add_button' => true,
                'setRowSelect' => "on_select$uniq",
                'setRowUnSelect' => "on_unselect$uniq",
                'setMultiSelect' => "on_unselect$uniq",
            ]);
        ?>
    </div>
    <div class='right_side sima-layout-panel'>
    </div>
</div>

<script>
    function on_select<?php echo $uniq; ?>(row)
    {        
        sima.hr.annualLeaveDecisionsByYear($('#<?php echo $uniq; ?>').find('.right_side'), row.attr('model_id'));
    }
    function on_unselect<?php echo $uniq; ?>(row)
    {
        $('#<?php echo $uniq; ?>').find('.right_side').hide();
    }
</script>
