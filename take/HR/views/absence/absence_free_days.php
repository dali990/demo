
<?php $uniq = SIMAHtml::uniqid();?>

<?php
    $this->widget('base.extensions.SIMAGuiTable.SIMAGuiTable', [
        'id'=>'absence_free_days'.$uniq,
        'model'=>AbsenceFreeDays::model(),
        'columns_type'=>'from_user',
        'add_button' => array(
            'init_data'=>array(
                'AbsenceFreeDays' => array(
                    'employee_id' => [
                        'disabled',
                        'init' => $employee_id
                    ],
                    'type' => Absence::$FREE_DAYS
                )
            ),            
        ),
        'fixed_filter'=>array(
            'employee' => array(
                'ids' => $employee_id
            ),
            'type' => Absence::$FREE_DAYS
        )
    ]);