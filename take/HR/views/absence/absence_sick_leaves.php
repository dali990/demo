
<?php $uniq = SIMAHtml::uniqid();?>

<?php
    $this->widget('base.extensions.SIMAGuiTable.SIMAGuiTable', [
        'id'=>'absence_sick_leave'.$uniq,
        'model'=>AbsenceSickLeave::model(),
        'columns_type'=>'from_user',
        'add_button' => array(
            'init_data'=>array(
                'AbsenceSickLeave' => array(
                    'employee_id' => [
                        'disabled',
                        'init' => $employee_id
                    ],
                    'type' => Absence::$SICK_LEAVE
                )
            ),            
        ),
        'fixed_filter'=>array(
            'employee' => array(
                'ids' => $employee_id
            ),
            'type' => Absence::$SICK_LEAVE
        )
    ]);