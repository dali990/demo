<div class="sima-layout-panel _splitter _vertical">
    <div class='sima-layout-panel left_side' data-sima-layout-init='{"proportion":0.2, "max_width":500}'>
        <?php
        $uniq = SIMAHtml::uniqid();
            $this->widget(SIMAGuiTable::class, [
                'id' => 'collective-annual-leave'.$uniq,   
                'model' => CollectiveAnnualLeave::model(),     
                'title' => Yii::t('HRModule.Absence', 'CollectiveAbsencesAndHolidays'),
                'add_button' => true,
                'setRowSelect'   => ['sima.hr.onSelectCollectiveAnnualLeave', "$uniq"],                
                'setRowUnSelect' => ['sima.hr.onUnselectCollectiveAnnualLeave', "$uniq"],
            ]);
        ?>
    </div>    

    <div class='sima-layout-panel right_side ' id="collective-annual-leave-full-info<?=$uniq;?>" data-sima-layout-init='{"proportion":0.8}'>
        
    </div> 
</div>

