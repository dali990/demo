
<h3>
    <?php
        echo Yii::t('HRModule.CompanyLicense', 'CompanyLicensesDecision');
    ?>
</h3>
<div class="row">
    <span class="title"><?php echo Yii::t('HRModule.CompanyLicense', 'Decision'); ?>:</span>
    <span class="data">
        <?php
            echo $model->DisplayHtml.SIMAHtml::modelIconsContracted($model,16,['open'])
        ?>
    </span>
</div>

<p class='row decision_date'>
    <span class='title'><?php echo $model->getAttributeLabel('decision_date') ?>:</span> 
    <span class='data'><?php echo $model->getAttributeDisplay('decision_date'); ?></span>
</p>

<div class="row">
    <span class="title" style="vertical-align: middle;"><?php echo Yii::t('HRModule.CompanyLicense', 'CompanyLicenses'); ?>:</span>
    <span class="data">
        <?php
            foreach ($model->company_licenses as $company_license)
            {
                ?>
                    <span>
                        <?php 
                            echo $company_license->DisplayName . SIMAHtml::modelFormOpen($company_license);
                        ?>
                    </span>
                <?php
            }
        ?>
    </span>
</div>