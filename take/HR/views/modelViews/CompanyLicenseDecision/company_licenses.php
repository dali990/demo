<h2>
    <?php 
        echo Yii::t('HRModule.CompanyLicense', 'CompanyLicensesOnDecision');
        echo $model->DisplayHtml.SIMAHtml::modelIconsContracted($model,16,['open']) 
    ?>
</h2>
<?php 
    foreach ($model->company_licenses as $company_license)
    {
        ?>
        <div class="company_license">
            <?php
                echo SIMAHtml::modelFormOpen($company_license).$company_license->DisplayName;
            ?>
        </div>
        <?php
    }
?>