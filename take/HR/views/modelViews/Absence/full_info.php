 <p class="row confirmed">
        <span class="title"><?php echo $model->getAttributeLabel('confirmed')?>:</span>
        <span class='data'><?php echo $model->getAttributeDisplay('confirmed');?> </span>
 </p>

<?php if(isset($model->employee_id)){?>
    <p class="row employee_id">
        <span class="title"><?php echo $model->getAttributeLabel('employee_id')?>:</span>
        <span class='data'><?php echo $model->employee->DisplayHtml; ?> </span>
    </p>
<?php }
    if(isset($model->begin)){?>
    <p class="row begin_day_id">
        <span class="title"><?php echo $model->getAttributeLabel('begin')?>:</span>
        <span class='data'><?php echo $model->getAttributeDisplay('begin');?> </span>
    </p>
<?php }

if(isset($model->end)){?>
    <p class="row end_day_id">
        <span class="title"><?php echo $model->getAttributeLabel('end')?>:</span>
        <span class='data'><?php echo $model->getAttributeDisplay('end');?> </span>
    </p>
<?php } ?>
    <p class="row work_hours">
        <span class="title"><?php echo $model->getAttributeLabel('number_work_days')?>:</span>
        <span class='data'><?php echo $model->getAttributeDisplay('number_work_days');?> </span>
    </p>

<?php if(isset($model->confirmed_timestamp)){?>
    <p class="row confirmed_timestamp">
        <span class="title"><?php echo $model->getAttributeLabel('confirmed_timestamp')?>:</span>
        <span class='data'><?php echo $model->getAttributeDisplay('confirmed_timestamp');?> </span>
    </p>
<?php }
if(isset($model->confirmed_user_id)){?>
    <p class="row confirmed_user_id">
        <span class="title"><?php echo $model->getAttributeLabel('confirmed_user_id')?>:</span>
        <span class='data'><?php echo $model->getAttributeDisplay('confirmed_user_id'); ?> </span>
    </p>
<?php }
if(isset($model->description)){?>
    <p class="row theme">
        <span class="title"><?php echo $model->getAttributeLabel('description')?>:</span>
        <span class='data'><?php echo $model->getAttributeDisplay('description');?> </span>
    </p>
<?php } ?>
    
    
<p class='row'>
    <span class='title'><?php echo $model->getAttributeLabel('set_document') ?>:</span> 
    <span class='data'><?php echo $model->getAttributeDisplay('set_document'); ?></span>
</p>