 <h3>Lekarski pregled<?php echo SIMAHtml::modelFormOpen($model) ?></h3>
   
    <p class='row add_medical_referral'>
        <span class='title'><?php echo $model->getAttributeLabel('generate_medical_referral') ?>:</span> 
        <span class='data'><?php echo $model->getAttributeDisplay('generate_medical_referral'); ?></span>
    </p>
    <p class='row person_name'>
        <span class='title'><?php echo $model->getAttributeLabel('person') ?>:</span> 
        <span class='data'><?php echo $model->getAttributeDisplay('person'); ?></span>
    </p>
    <p class='row work_position_id'>
        <span class='title'><?php echo $model->getAttributeLabel('work_position_id') ?>:</span> 
        <span class='data'><?php echo $model->getAttributeDisplay('work_position_id'); ?></span>
    </p>
    <p class='row medical_report_interval'>
        <span class='title'><?php echo $model->getAttributeLabel('medical_report_period_id') ?>:</span> 
        <span class='data'><?php echo $model->getAttributeDisplay('medical_report_period_id'); ?></span>
    </p>
    <p class='row date_previous_report'>
        <span class='title'><?php echo $model->getAttributeLabel('date_previous_report') ?>:</span> 
        <span class='data'><?php echo $model->getAttributeDisplay('date_previous_report'); ?></span>
    </p>
    <p class='row date_next_report'>
        <span class='title'><?php echo $model->getAttributeLabel('date_next_report') ?>:</span> 
        <span class='data'><?php echo $model->getAttributeDisplay('date_next_report'); ?></span>
    </p>
    <p class='row healthy_mark'>
        <span class='title'><?php echo $model->getAttributeLabel('healthy_mark') ?>:</span> 
        <span class='data'><?php echo $model->getAttributeDisplay('healthy_mark'); ?></span>
    </p>
    <p class='row actions'>
        <span class='title'><?php echo $model->getAttributeLabel('actions') ?>:</span> 
        <span class='data'><?php echo $model->getAttributeDisplay('actions'); ?></span>
    </p>
    <p class='row description'>
        <span class='title'><?php echo $model->getAttributeLabel('description') ?>:</span> 
        <span class='data'><?php echo $model->getAttributeDisplay('description'); ?></span>
    </p>
    