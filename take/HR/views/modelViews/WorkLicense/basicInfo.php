<?php $uniq=  SIMAHtml::uniqid();
if(isset($model->code)){?>
    <p class="row code">
        <span class="title"><?php echo $model->getAttributeLabel('code')?>:</span>
        <span class='data'><?php echo $model->getAttributeDisplay('code');?> </span>
    </p>
<?php }
if(isset($model->issuer_id)){?>
    <p class="row issuer">
        <span class="title"><?php echo $model->getAttributeLabel('issuer_id')?>:</span>
        <span class='data'> 
             <?php echo $model->getAttributeDisplay('issuer_id'); echo SIMAHtml::modelDialog(Company::model()->findByPk($model->issuer_id));?>
        </span>
    </p>
<?php }
if(isset($model->conditions)){?>
    <p class="row conditions">
        <span class="title"><?php echo $model->getAttributeLabel('conditions')?>:</span>
        <span class='data'><?php echo $model->getAttributeDisplay('conditions');?> </span>
    </p>
<?php }
if(isset($model->description)){?>
    <p class="row description">
        <span class="title"><?php echo $model->getAttributeLabel('description')?>:</span>
        <span class='data'><?php echo $model->getAttributeDisplay('description');?> </span>
    </p>
<?php }?>
<div class="persons_to_work_license">
    <p class="row">
        <span class="title"><?php echo $model->getAttributeLabel('Licencu poseduju')?>:</span>
        <span class="add_button"><?php echo SIMAHtml::modelFormOpen(PersonToWorkLicense::model(),array(
            'init_data'=>array(
                'PersonToWorkLicense'=>array(
                    'working_license_id'=>$model->id
                )
            ),
            'updateModelViews'=>SIMAHtml::getTag($model))) ?></span>
         <?php 
            echo GuiTableController::renderEmbeddedSimple(array(
                'pg_id'=>'person_to_work_licenses'.$uniq,
                'Model'=> PersonToWorkLicense::model(),
                'columns_type'=>'work_licenses',
                'filter_params'=>array('working_license'=>['ids'=>$model->id]) 
            ));?>
    </p>
</div>


