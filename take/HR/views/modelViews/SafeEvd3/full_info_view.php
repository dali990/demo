<div style="border-bottom: dotted" class="sima-layout-fixed-panel">
    <?=Yii::t('HRModule.SafeEvd3','SafeEvdConfrimed').$model->getAttributeDisplay('statuses');?>
</div>
<div  class="sima-layout-panel">
    <?php   
        if($model->confirmed)
        {
            $this->widget('SIMAFilePreview', [
                'file' => $model->file
            ]);
        }
        else
        {
            $se = new SafeEvd3Report(["save_evd_3" => $model]);
            echo $se->getHTML();
        }
    ?>
</div>

