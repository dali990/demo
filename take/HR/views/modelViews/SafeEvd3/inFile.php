<h3><?php echo SafeRecordController::$SAFE_RECORDS[3]['name'] . SIMAHtml::modelFormOpen($model); ?></h3>
   
    <p class='row'>
        <span class='title'><?php echo $model->getAttributeLabel('employee') ?>:</span> 
        <span class='data'><?php echo $model->getAttributeDisplay('employee'); ?></span>
    </p>
    <p class='row'>
        <span class='title'><?php echo $model->getAttributeLabel('date') ?>:</span> 
        <span class='data'><?php echo $model->getAttributeDisplay('date'); ?></span>
    </p>
    <p class='row'>
        <span class='title'><?php echo $model->getAttributeLabel('work_position') ?>:</span> 
        <span class='data'><?php echo $model->getAttributeDisplay('work_position'); ?></span>
    </p>
    <p class='row'>
        <span class='title'><?php echo $model->getAttributeLabel('type_injury') ?>:</span> 
        <span class='data'><?php echo $model->getAttributeDisplay('type_injury'); ?></span>
    </p>
    <p class='row'>
        <span class='title'><?php echo $model->getAttributeLabel('mark_injury') ?>:</span> 
        <span class='data'><?php echo $model->getAttributeDisplay('mark_injury'); ?></span>
    </p>
    <p class='row'>
        <span class='title'><?php echo $model->getAttributeLabel('source_injury') ?>:</span> 
        <span class='data'><?php echo $model->getAttributeDisplay('source_injury'); ?></span>
    </p>
    <p class='row'>
        <span class='title'><?php echo $model->getAttributeLabel('mode_injury') ?>:</span> 
        <span class='data'><?php echo $model->getAttributeDisplay('mode_injury'); ?></span>
    </p>
    <p class='row'>
        <span class='title'><?php echo $model->getAttributeLabel('description') ?>:</span> 
        <span class='data'><?php echo $model->getAttributeDisplay('description'); ?></span>
    </p>
    