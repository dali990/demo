<div class="sima-layout-panel _vertical _splitter">
    <div class="sima-layout-panel _horizontal"
         data-sima-layout-init='{"proportion":0.6, "min_width":600}'>
        
        <?=Yii::app()->controller->renderModelView($model, 'full_info_view', ['class' => 'sima-layout-panel _horizontal']);?>
    </div>

    <div class="sima-layout-panel" data-sima-layout-init='{"proportion":0.4, "min_width":200}'>
        <?php
        echo Yii::app()->controller->renderModelView($model->file, 'full_info_right_view', ['class' => 'sima-layout-panel _horizontal _splitter']);
        ?>  
    </div>
</div>
