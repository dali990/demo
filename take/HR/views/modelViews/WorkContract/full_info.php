<div class="sima-layout-panel _vertical _splitter">
    <div class="sima-layout-panel" 
        data-sima-layout-init='{"proportion":0.8, "min_width":400}'>
        <div style="padding: 10px;">
            <?php echo "<h1>" . Yii::t('HRModule.WorkContract', 'Work Contract') . ":</h1> "; ?>
            <div style="
                    clear: both;
                    display: table;">
                <div class='image'>
                    <img  src='<?= $model->employee->person->getAttributeDisplay('image_id'); ?>' />
                </div>
                <div style="float:left;
                    padding-left: 10px;
                    padding-top: 10px;">
                    <?= $model->employee->person->DisplayHTML; ?><br>
                    <?= $model->work_position->DisplayName; ?>
                    <div style=" <?php
                    if (strtotime($model->getEndDate()) < strtotime(date("d.m.Y"))) {
                        echo "background-color: #96ec96";
                    } else {
                        echo "background-color: yellow;";
                    }
                    ?>">
                        <p class='row sign_date'>
                            <span class='title'> <?php echo $model->getAttributeLabel('start_date') ?>:</span>
                            <span class='data'> <?php echo $model->getAttributeDisplay('start_date'); ?></span>
                        </p>
                        <span class='title'><?php echo $model->getAttributeLabel('end_date') ?>:</span>
                        <span class='data'><?php echo $model->getAttributeDisplay('end_date'); ?></span>
                    </div>

                </div>
            </div>
            <p>
                <?php
                $next_wc = $model->next();
                $prev_wc = $model->prev();
                    echo "<span>" . Yii::t('HRModule.WorkContract', 'PreviousContract') . ":</span> ";
                if (!is_null($prev_wc)) {
                    echo $prev_wc->DisplayHtml;
                }
                    echo '</br>';
                    echo "<span>" . Yii::t('HRModule.WorkContract', 'NextContract') . ":</span> ";
                if (!is_null($next_wc)) {
                    echo $next_wc->DisplayHtml;
                }
                ?>
            </p>
        </div>
    </div>
    <div class="sima-layout-panel" data-sima-layout-init='{"proportion":0.2, "min_width":250, "max_width":400}'>
        <?php
        echo Yii::app()->controller->renderModelView($model->file, 'full_info_right_view', ['class' => 'sima-layout-panel _horizontal _splitter']);
        ?>  
    </div>
</div>

