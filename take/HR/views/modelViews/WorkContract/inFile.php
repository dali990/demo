    <h3>Ugovor o radu<?php echo SIMAHtml::modelFormOpen($model) ?></h3>


    <p class='row partner_id'>
        <span class='title'><?php echo $model->getAttributeLabel('employee_id') ?>:</span> 
        <span class='data'><?php echo $model->getAttributeDisplay('employee_id'); ?></span>
    </p>
    <p class='row sign_date'>
        <span class='title'><?php echo $model->getAttributeLabel('start_date') ?>:</span> 
        <span class='data'><?php echo $model->getAttributeDisplay('start_date'); ?></span>
    </p>
    <p class='row sign_date'>
        <span class='title'><?php echo $model->getAttributeLabel('end_date') ?>:</span> 
        <span class='data'><?php echo $model->getAttributeDisplay('end_date'); ?></span>
    </p>
    <p class='row sign_date'>
        <span class='title'><?php echo $model->getAttributeLabel('work_position_id') ?>:</span> 
        <span class='data'><?php echo $model->getAttributeDisplay('work_position_id'); ?></span>
    </p>
    <p class='row generate_work_contract_template'>
        <span class='title'><?php echo $model->getAttributeLabel('generate_work_contract_template') ?>:</span> 
        <span class='data'><?php echo $model->getAttributeDisplay('generate_work_contract_template'); ?></span>
    </p>
    <p class='row generate_notice_of_mobbing_template'>
        <span class='title'><?php echo $model->getAttributeLabel('generate_notice_of_mobbing_template') ?>:</span> 
        <span class='data'><?php echo $model->getAttributeDisplay('generate_notice_of_mobbing_template'); ?></span>
    </p>
    <p class='row add_filing_number'>
        <span class='title'><?php echo $model->getAttributeLabel('add_filing_number') ?>:</span> 
        <span class='data'><?php echo $model->getAttributeDisplay('add_filing_number'); ?></span>
    </p>
    <p class='row occupation'>
        <span class='title'><?= Yii::t('HRModule.Occupation','Occupation') ?>:</span>   
        <?php if (!empty($model->active_work_contract->occupation)) { ?>       
        <span class='data'><?= $model->active_work_contract->occupation->name;?></span>
        <?php } ?>
    </p>
    <p class='row qualification_level'>
        <span class='title'><?php echo $model->employee->person->getAttributeLabel('qualification_level_id') ?>:</span> 
        <span class='data'><?php echo $model->employee->person->getAttributeDisplay('qualification_level_id'); ?></span>
    </p>

