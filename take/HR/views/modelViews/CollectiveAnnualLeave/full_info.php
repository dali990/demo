<div class="sima-layout-panel _horizontal">
    <div class="sima-layout-fixed-panel">
        <?php 
           echo Yii::app()->controller->renderModelView($model, 'basic_info');
        ?>
    </div>
    
    <div class="sima-layout-panel">
        <?php
            $guitable_id = 'etcal_'.  SIMAHtml::uniqid();

            $this->widget('SIMAGuiTable', [
                'id' => $guitable_id,
                'model' => AbsenceAnnualLeave::model(),
                'columns_type'=>'from_collective',
                'add_button' => "sima.hr.employeesToCollectiveAnnualLeavesChooser('$model->id', '$guitable_id');",
                'custom_buttons' => [
                    Yii::t('HRModule.Absence', 'AddAllEmployeesToCollectiveLeave') => [
                        'func' => "sima.hr.onAddAllEmployeesToCollectiveLeave($model->id, '$guitable_id');"
                    ]
                ],
                'fixed_filter' => [
                    'collective_annual_leave' => [
                        'ids' => $model->id
                    ]
                ]
            ]);
        ?>
    </div>
</div>
