<p class="row begin_day_id">
   <span class="title"><?php echo $model->getAttributeLabel('sector'); ?>:</span>
   <span class='data'><?php echo $model->getAttributeDisplay('sector'); ?> </span>
</p>
<p class="row begin_day_id">
   <span class="title"><?php echo $model->getAttributeLabel('from_day'); ?>:</span>
   <span class='data'><?php echo $model->getAttributeDisplay('from_day'); ?> </span>
</p>
<p class="row begin_day_id">
   <span class="title"><?php echo $model->getAttributeLabel('to_day'); ?>:</span>
   <span class='data'><?php echo $model->getAttributeDisplay('to_day'); ?> </span>
</p>
<p class="row theme">
    <span class="title"><?php echo $model->getAttributeLabel('comment')?>:</span>
    <span class='data'><?php echo $model->getAttributeDisplay('comment');?> </span>
</p>