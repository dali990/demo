 <p class="row begin_day_id">
        <span class="title"><?php echo $model->getAttributeLabel('confirmed')?>:</span>
        <span class='data'><?php echo $model->getAttributeDisplay('confirmed');?> </span>
 </p>


<p class="row employee_id">
    <span class="title"><?php echo $model->getAttributeLabel('employee')?>:</span>
    <span class='data'><?php echo $model->getAttributeDisplay('employee'); ?> </span>
</p>

<p class="row employee_id">
    <span class="title"><?php echo $model->getAttributeLabel('subtype')?>:</span>
    <span class='data'><?php echo $model->getAttributeDisplay('subtype'); ?> </span>
</p>

<p class="row begin_day_id">
    <span class="title"><?php echo $model->getAttributeLabel('begin')?>:</span>
    <span class='data'><?php echo $model->getAttributeDisplay('begin');?> </span>
</p>



<p class="row end_day_id">
    <span class="title"><?php echo $model->getAttributeLabel('end')?>:</span>
    <span class='data'><?php echo $model->getAttributeDisplay('end');?> </span>
</p>

<p class="row work_hours">
    <span class="title"><?php echo $model->getAttributeLabel('number_work_days')?>:</span>
    <span class='data'><?php echo $model->getAttributeDisplay('number_work_days');?> </span>
</p>


<p class="row theme">
    <span class="title"><?php echo $model->getAttributeLabel('description')?>:</span>
    <span class='data'><?php echo $model->getAttributeDisplay('description');?> </span>
</p>

    
    
<p class='row'>
    <span class='title'><?php echo $model->getAttributeLabel('set_document') ?>:</span> 
    <span class='data'><?php echo $model->getAttributeDisplay('set_document'); ?></span>
</p>