<div class="sima-org-scheme-sector-work-position <?php echo ($model->is_director)?'_director-position':''; ?>" model_id="<?php echo $model->id; ?>">
    <div class="sima-org-scheme-sector-work-position-name">
        <span class="sima-org-scheme-sector-work-position-display-name" title="<?php echo $model->DisplayName; ?>">
            <?php echo $model->DisplayName; ?>
        </span>
        <?php 
            $cnt1 = $model->count_not_fictive_position_to_persons;
            $cnt2 = $model->count_fictive_position_to_persons;
            if ($cnt1 > 0 || $cnt2 > 0)
            {
        ?>
                <span class="sima-org-scheme-sector-work-position-person-cnt" 
                      title="Broj osoba čija je prava pozicija + Broj osoba čija je privremena pozicija">
                    <?php echo $cnt1.'+'.$cnt2; ?>
                </span>
                <span class="sima-org-scheme-sector-work-position-exp sima-icon _black _18 _collapsed" title="Proširi/skupi radnu poziciju">        
                </span>
        <?php
            } 
        ?>    
    </div>
    <div class="sima-org-scheme-sector-work-position-persons">
        <?php
            foreach ($model->not_fictive_position_to_persons as $ptwp)
            {
                echo $this->renderModelView($ptwp, 'personBasicView');
            }
            foreach ($model->fictive_position_to_persons as $ptwp)
            {
                echo $this->renderModelView($ptwp, 'personBasicView');
            }
        ?>
    </div>
</div>