<p class='row company_sector_id'>
    <span class='title'><?php echo $model->getAttributeLabel('company_sector_id') ?>:</span> 
    <span class='data'><?php echo $model->getAttributeDisplay('company_sector_id'); ?></span>
</p>
<p class='row company_sector_id'>
    <span class='title'><?php echo $model->getAttributeLabel('code') ?>:</span> 
    <span class='data'><?php echo $model->getAttributeDisplay('code'); ?></span>
</p>
<p class='row enlarged_risk'>
    <span class='title'><?php echo $model->getAttributeLabel('enlarged_risk') ?>:</span> 
    <span class='data' title="Radna pozicija je sa povećenim rizikom jer postoje rizici za nju" style="cursor: help;">
        <?php echo $model->getAttributeDisplay('enlarged_risk'); ?>
    </span>
</p>
<p class='row reason_of_training'>
    <span class='title'><?php echo $model->getAttributeLabel('reason_of_training') ?>:</span> 
    <span class='data'><?php echo $model->getAttributeDisplay('reason_of_training'); ?></span>
</p>
<p class='row medical_report_interval'>
    <span class='title'><?php echo $model->getAttributeLabel('medical_report_interval') ?>:</span> 
    <span class='data'><?php echo $model->getAttributeDisplay('medical_report_interval'); ?></span>
</p>
<p class='row description'>
    <span class='title'><?php echo $model->getAttributeLabel('description_of_activities') ?>:</span> 
    <span class='data'><?php echo $model->getAttributeDisplay('description_of_activities'); ?></span>
</p>
<p class='row safe_actions'>
    <span class='title'><?php echo $model->getAttributeLabel('safe_actions') ?>:</span> 
    <span class='data'><?php echo $model->getAttributeDisplay('safe_actions'); ?></span>
</p>
<p class='row notice_safe'>
    <span class='title'><?php echo $model->getAttributeLabel('notice_safe') ?>:</span> 
    <span class='data'><?php echo $model->getAttributeDisplay('notice_safe'); ?></span>
</p>
<p class='row'>
    <span class='title'><?php echo $model->getAttributeLabel('add_risks'); ?></span>
</p>
<?php
    $count = 0;
    if (count($model->work_position_to_risks) > 0)
    {
        foreach ($model->work_position_to_risks as $risk) 
        {
            ?>
            <p class='row'>
                <span class='title'></span>
                <span class='data sima-text-dots' style="width: 100%;">
                    <?php
                        echo SIMAHtml::modelDelete($risk).$risk->work_danger->DisplayName;
                    ?>
                </span>
            </p>
            <?php
        }
    }
?>
<p class='row'>
    <span class='title'><?php echo $model->getAttributeLabel('add_required_medical_abilities'); ?></span>
</p>
<?php       
    if (count($model->work_position_to_required_medical_abilities) > 0)
    {
        foreach ($model->work_position_to_required_medical_abilities as $work_position_to_required_medical_ability) 
        {
            ?>
            <p class='row'>
                <span class="title"></span>
                <span class='data sima-text-dots' style="width: 100%;">
                    <?php
                        echo SIMAHtml::modelDelete($work_position_to_required_medical_ability).$work_position_to_required_medical_ability->required_medical_ability->DisplayName;
                    ?>
                </span>
            </p>
            <?php
        }
    }
?>
<p class='row'>
    <span class='title'><?php echo $model->getAttributeLabel('occupations'); ?></span>
</p>
<?php
    foreach ($model->work_position_to_occupations as $wpo) 
    {
        ?>
        <p class='row'>
            <span class='title'></span>
            <span class='data sima-text-dots' style="width: 100%;">
                <?= SIMAHtml::modelDelete($wpo).$wpo->occupation->DisplayName;?>
            </span>
        </p>
        <?php
    }
?>
