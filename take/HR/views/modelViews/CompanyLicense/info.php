<div class="basic_info"> 
    <h3><?php echo $model->DisplayName.SIMAHtml::modelFormOpen($model); ?></h3>
   
    <p class='row code'>
        <span class='title'><?php echo $model->getAttributeLabel('code') ?>:</span> 
        <span class='data'><?php echo $model->getAttributeDisplay('code'); ?></span>
    </p>
    <p class='row references_number_value'>
        <span class='title'><?php echo $model->getAttributeLabel('references_number') ?>:</span> 
        <span class='data'><?php echo $model->getAttributeDisplay('references_number'); ?></span>
    </p>
    <p class='row persons_number'>
        <span class='title'><?php echo $model->getAttributeLabel('persons_number') ?>:</span> 
        <span class='data'><?php echo $model->getAttributeDisplay('persons_number'); ?></span>
    </p>
    <p class='row description'>
        <span class='title'><?php echo $model->getAttributeLabel('description') ?>:</span> 
        <span class='data'><?php echo $model->getAttributeDisplay('description'); ?></span>
    </p>
    <?php if (count($model->work_licenses)>0) { ?>
        <div  class='row person_work_licenses'>
            <span class='title'><?php echo $model->getAttributeLabel('person_work_licenses'); ?></span>
            <div class='data'>
                <?php
                    foreach ($model->work_licenses as $work_license) 
                    {
                        ?>
                        <div>
                            <?php
                                echo $work_license->DisplayName.SIMAHtml::modelFormOpen($work_license);
                            ?>
                        </div>
                        <?php
                    }
                ?>
            </div>
        </div>
    <?php } ?>
</div>
    