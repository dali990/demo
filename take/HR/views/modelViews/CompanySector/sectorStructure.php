<div class="sima-org-scheme-sector _horizontal" model_id="<?php echo $model->id; ?>">
    <div class='sima-org-scheme-sector-name'>
        <span class="sima-org-scheme-sector-display-name" title="<?php echo $model->DisplayName; ?>">
            <?php echo $model->DisplayName; ?>
        </span>
        <?php
            $cnt1 = count($model->get_not_fictive_people_position_count());
            $cnt2 = count($model->get_fictive_people_position_count());
            if ($cnt1 > 0 || $cnt2 > 0)
            {
                ?>
                    <span class="sima-org-scheme-sector-person-cnt" 
                          title="Broj osoba u sektoru sa pravom pozicijom + Broj osobe u sektoru sa privremenom pozicijom">
                        <?php echo $cnt1.'+'.$cnt2; ?>
                    </span>
                <?php
            }
        ?>    
    </div>
    <div class='sima-org-scheme-sector-work-positions'>
    <?php
        $cnt = 0;    
        foreach ($model->director_work_positions as $work_pos)
        {
            $cnt++;
            echo $this->renderModelView($work_pos,'positionStructure'); 
        }
        foreach ($model->not_director_work_positions as $work_pos)
        {
            $cnt++;
            echo $this->renderModelView($work_pos,'positionStructure'); 
        }
     ?>
        <div class="clearfix"></div>
        <?php if ($cnt > 0) { ?>
        <div style="position: relative;">
            <div class="sima-org-scheme-sector-work-positions-exp sima-icon _white _18 _collapsed" title="Prikaži/sakrij samo radne pozicije"></div>
            <div class="sima-org-scheme-sector-work-positions-exp-all" title="Prikaži/sakrij sve"></div>
        </div>
        <?php } ?>
    </div>
    <div class='sima-org-scheme-sector-childrens <?php echo ($model->hasGrandChildrens())?'tree_structure':'out_tree_structure'; ?>'>  
        <?php foreach($model->childrens as $child) { ?>
               <?php echo $this->renderModelView($child,'sectorStructure');?>
        <?php }?>  
    </div>
</div>
