<div class="sima-layout-panel _horizontal">

    <div class="sima-layout-fixed-panel">
        <h3 class="sima-display-inline"><?=$model->DisplayHTML?></h3>
        <div class="sima-display-inline">
            <?=$this->renderModelView($model,'options');?>
        </div>
    </div>
    

    <div class="sima-layout-panel">
        <?=$this->renderModelView($model,'full_info');?>
    </div>
    
</div>
