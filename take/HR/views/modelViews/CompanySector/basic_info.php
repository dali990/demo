<p class='row type'>
    <span class='title'><?php echo $model->getAttributeLabel('type') ?>:</span> 
    <span class='data'><?php echo $model->getAttributeDisplay('type'); ?></span>
</p>
<p class='row code'>
    <span class='title'><?php echo $model->getAttributeLabel('code') ?>:</span> 
    <span class='data'><?php echo $model->getAttributeDisplay('code'); ?></span>
</p>
<p class='row parent_id'>
    <span class='title'><?php echo $model->getAttributeLabel('parent_id') ?>:</span> 
    <span class='data'><?php echo $model->getAttributeDisplay('parent_id'); ?></span>
</p>
<p class='row description'>
    <span class='title'><?php echo $model->getAttributeLabel('description') ?>:</span> 
    <span class='data'><?php echo $model->getAttributeDisplay('description'); ?></span>
</p>
