<h3>Raskid ugovora o radu<?php echo SIMAHtml::modelFormOpen($model) ?></h3>
   
    <p class='row end_date'>
        <span class='title'><?php echo $model->getAttributeLabel('end_date') ?>:</span> 
        <span class='data'><?php echo $model->getAttributeDisplay('end_date'); ?></span>
    </p>
    <p class='row work_contract_id'>
        <span class='title'><?php echo $model->getAttributeLabel('work_contract_id') ?>:</span> 
        <span class='data'><?php echo $model->getAttributeDisplay('work_contract_id').SIMAHtml::modelDialog(WorkContract::model()->findByPk($model->work_contract->id)); ?></span>
    </p>
    <p class='row generate_close_work_contract_template'>
        <span class='title'><?php echo $model->getAttributeLabel('generate_close_work_contract_template') ?>:</span> 
        <span class='data'><?php echo $model->getAttributeDisplay('generate_close_work_contract_template'); ?></span>
    </p>