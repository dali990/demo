 
    <p class='row for_employee'>
        <span class='title'><?= Yii::t('HRModule.MedicalExaminationReferral', 'ForEmployee') ?>:</span> 
        <span class='data'><?= $model->getAttributeDisplay('person'); ?></span>
    </p>
    <p class='row work_position'>
        <span class='title'><?= $model->getAttributeLabel('work_position') ?>:</span> 
        <span class='data'><?= $model->getAttributeDisplay('work_position'); ?></span>
    </p>
    <?php if (count($model->safe_evd2_medical_reports) > 0) { ?>
        <p class='row work_position'>
            <span class='title'><?=Yii::t('HRModule.SafeEvd2', 'MedicalReports')?>:</span> 
            <span class='data'>
                <?php 
                    foreach ($model->safe_evd2_medical_reports as $safe_evd2_medical_report)
                    {
                        echo $safe_evd2_medical_report->DisplayHtml;
                    }
                ?>
            </span>
        </p>
    <?php } ?>
    