<div class="sima-org-scheme-sector-work-position-person <?php echo (!$model->fictive)?'_not-fictive':''; ?>" model_id="<?php echo $model->id; ?>" 
     person_id="<?php echo $model->person->id; ?>" title="<?php echo $model->person->DisplayName; ?>">
    <div class="sima-org-scheme-sector-work-position-person-img">
        <img title='<?php echo $model->person->DisplayName; ?>' src='<?php echo $model->person->getAttributeDisplay('image_id'); ?>'/>
    </div>
    <div class="sima-org-scheme-sector-work-position-person-name">
        <span><?php echo $model->person->DisplayName; ?></span>
    </div>
</div>