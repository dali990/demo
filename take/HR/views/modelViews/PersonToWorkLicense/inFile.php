   <h3>Radna Licenca<?php echo SIMAHtml::modelFormOpen($model) ?></h3>
   
    <p class='row number'>
        <span class='title'><?php echo $model->getAttributeLabel('number') ?>:</span> 
        <span class='data'><?php echo $model->getAttributeDisplay('number'); ?></span>
    </p>
    <p class='row code'>
        <span class='title'><?php echo $model->getAttributeLabel('code') ?>:</span> 
        <span class='data'><?php echo $model->working_license->code ?></span>
    </p>
    <p class='row working_license'>
        <span class='title'><?php echo $model->getAttributeLabel('working_license_id') ?>:</span> 
        <span class='data'><?php echo $model->getAttributeDisplay('working_license_id'); ?></span>
    </p>
    <p class='row partner_id'>
        <span class='title'><?php echo $model->getAttributeLabel('person_id') ?>:</span> 
        <span class='data'><?php echo $model->getAttributeDisplay('person_id'); ?></span>
    </p>
    <p class='row issuer_name'>
        <span class='title'><?php echo $model->getAttributeLabel('issuer_name') ?>:</span> 
        <span class='data'><?php echo $model->getAttributeDisplay('issuer_name'); ?></span>
    </p>
    <p class='row start_date'>
        <span class='title'><?php echo $model->getAttributeLabel('start_date') ?>:</span> 
        <span class='data'><?php echo $model->getAttributeDisplay('start_date'); ?></span>
    </p>
     <p class='row end_date'>
        <span class='title'><?php echo $model->getAttributeLabel('end_date') ?>:</span> 
        <span class='data'><?php echo $model->getAttributeDisplay('end_date'); ?></span>
    </p>
    <p class='row conditions'>
        <span class='title'><?php echo $model->getAttributeLabel('conditions') ?>:</span> 
        <span class='data'><?php echo $model->working_license->conditions; ?></span>
    </p>
    <p class='row description'>
        <span class='title'><?php echo $model->getAttributeLabel('description') ?>:</span> 
        <span class='data'><?php echo $model->working_license->description; ?></span>
    </p>

    
  