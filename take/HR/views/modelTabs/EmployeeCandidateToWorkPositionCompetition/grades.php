<?php
    $this->widget('base.extensions.SIMAGuiTable.SIMAGuiTable', [
//        'id' => 'grades'.$uniq,
        'model' => CompetitionGrade::model(),
        'columns_type' => 'inCandidate',
        'add_button' => $add_button,
        'fixed_filter' => array(
            'employee_candidate_to_work_position_competition' => ['ids' => $model->id],
            'work_position_competition_juries' => [
                'jury' => ['ids' => Yii::app()->user->id]
            ]
        )            
    ]);            
?>