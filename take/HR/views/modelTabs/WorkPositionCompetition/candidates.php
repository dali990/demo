<?php
    $candidates_guitable_id = "ec$uniq";
    $curr_user_id = Yii::app()->user->id;
?>
<div class='partner sima-layout-panel _splitter _vertical'>
    <div class='partner sima-layout-panel _horizontal'
         data-sima-layout-init='<?=CJSON::encode([
                'proportion' => 0.7
            ])?>'>
        <div class="sima-layout-fixed-panel" style="padding: 5px;">
            <?php
                echo SIMAHtml::dropDownList('grade_filter_for_curr_user', '', [
                    'hasCVGradeByUser' => Yii::t('HRModule.EmployeeCandidateToWorkPositionCompetition', 'HasCVGradeByCurrUser'),
                    'hasNotCVGradeByUser' => Yii::t('HRModule.EmployeeCandidateToWorkPositionCompetition', 'HasNotCVGradeByCurrUser'),
                    'hasInterviewGradeByUser' => Yii::t('HRModule.EmployeeCandidateToWorkPositionCompetition', 'HasInterviewGradeByCurrUser'),
                    'hasNotInterviewGradeByUser' => Yii::t('HRModule.EmployeeCandidateToWorkPositionCompetition', 'HasNotInterviewGradeByCurrUser'),
                ], 
                [
                    'empty' => Yii::t('HRModule.EmployeeCandidateToWorkPositionCompetition', 'IsGradedByCurrUser'),
                    'onchange' => "sima.hr.filterGradeForCurrUser($(this), $curr_user_id, '$candidates_guitable_id');"
                ]);
            ?>
        </div>
        <div class="sima-layout-panel">
            <?php
                $fixed_filter = [
                    'work_position_competition' => [
                        'ids' => $model->id
                    ]
                ];
                if($model->is_graded)
                {
                    $fixed_filter['display_scopes'] = ['byAvgCVGradeDESC'];
                }
                else
                {
                    $fixed_filter['display_scopes'] = ['byIdASC'];
                }
            
                $this->widget('SIMAGuiTable', [
                    'id' => $candidates_guitable_id,
                    'model' => EmployeeCandidateToWorkPositionCompetition::model(),                
                    'fixed_filter' => $fixed_filter,
                    'setRowSelect' => ['sima.hr.onWorkPositionCompetitionCandidateSelect', $uniq, SIMAHtml::getTag(EmployeeCandidateToWorkPositionCompetition::model())],
                    'setRowUnSelect' => ['sima.hr.onWorkPositionCompetitionCandidateUnSelect', $uniq],
                    'setMultiSelect' => ['sima.hr.onWorkPositionCompetitionCandidateUnSelect', $uniq]
                ]);            
            ?>
        </div>
    </div>
    <div class='partner additional_info sima-layout-panel'
         data-sima-layout-init='<?=CJSON::encode([
                'proportion' => 0.3
            ])?>'>
        <?php 
            $this->widget('ext.SIMATabs.SIMATabs', array(
                'id'=>'tabs'.$uniq,
                'list_tabs_model_name' => 'EmployeeCandidateToWorkPositionCompetition',
                'list_tabs_populate' => false,
                'default_selected'=>'documents',
            ));  
        ?>
    </div>
</div>
