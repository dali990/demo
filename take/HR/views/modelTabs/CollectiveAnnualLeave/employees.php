<?php

$guitable_id = 'etcal_'.  SIMAHtml::uniqid();

$this->widget('SIMAGuiTable', [
    'id' => $guitable_id,
    'model' => AbsenceAnnualLeave::model(),
    'columns_type'=>'from_collective',
    'add_button' => "sima.hr.employeesToCollectiveAnnualLeavesChooser('$model->id', '$guitable_id');",
    'fixed_filter' => [
        'collective_annual_leave' => [
            'ids' => $model->id
        ]
    ]
]);
