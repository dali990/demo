
<?php

    $uniq = SIMAHtml::uniqid();
    $this->widget('base.extensions.SIMAGuiTable.SIMAGuiTable', [
        'id' => "work_positions_$uniq",
        'model' => WorkPosition::model(),
        'fixed_filter' => [
            'company_sector' => [
                'ids' => $model->id
            ]
        ],
        'add_button' => [
            'init_data' => [
                'WorkPosition' => [
                    'company_sector_id' => [
                        'disabled',
                        'init' => $model->id
                    ]
                ]
            ]
        ]
    ]);