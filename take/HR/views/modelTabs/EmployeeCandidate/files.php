<?php

$this->widget('base.extensions.SIMAGuiTable.SIMAGuiTable', [
//    'id' => 'files'.$uniq,
    'model' => File::model(),
    'fixed_filter' => array(
        'belongs' => $model->tag->query
    ),
    'add_button' => [
        'init_data' => [
            'File'=> [
                'file_belongs'=> [
                    'ids'=> [
                        'ids'=> [[
                            'class'=> '_non_editable',
                            'id'=>$model->tag->id,
                            'display_name'=> $model->tag->DisplayName,
                        ]]
                    ]
                ]
            ]
        ]
    ]
]);         