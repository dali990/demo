
<?php 
    $uniq = SIMAHtml::uniqid();
    $company_license = $model->company_license;
?>
<div id="<?php echo $uniq; ?>" class="choose_persons_to_company_licence sima-layout-panel" 
     data-references_number='<?php echo $company_license->references_number; ?>' data-persons_number='<?php echo $company_license->persons_number; ?>'>
    <div class="company_license_info">
        <?php echo Yii::app()->controller->renderModelView($company_license, 'info');  ?>
    </div>
    <div class="choose_persons_to_company_licence_body">
        <?php if (count($model->partners) === 0) { ?>
            <div class="error">Niste popunili broj osoba ili broj referenci.</div>
            <div class="curr_references_number_wrap overflow">Broj različitih referenci: <span class="curr_references_number">0</span>/<?php echo $company_license->references_number; ?></div>
            <div class="curr_persons_number_wrap overflow">Broj osoba: <span class="curr_persons_number">0</span>/<?php echo $company_license->persons_number; ?></div>
            <br />
            <div class="head">
                <div class="add">Dodaj</div>
                <div class="name">Osoba</div>
                <div class="references_number">Broj referenci</div>
            </div>
            <div class="body_wrap scroll-container">
                <div class="body">
                    <?php 
                        foreach ($partners as $partner)
                        {
                            $references_ids = $model->getReferencesForPartner($partner);
                            ?>
                            <div class="choose_persons_to_company_licence_row" 
                                 data-id='<?php echo $partner->id; ?>' 
                                 data-references_ids='<?php echo CJSON::encode($references_ids); ?>' 
                                 data-references_number="<?php echo count($references_ids); ?>">
                                <div class="add">
                                    <input onclick="onInputChange<?php echo $uniq; ?>($(this))" type="checkbox" title="Dodaj">
                                </div>
                                <div class="name">
                                    <?php
                                        echo $partner->DisplayHtml;
                                    ?>
                                </div>
                                <div class="references_number">
                                    <?php
                                        echo count($references_ids);
                                    ?>
                                </div>
                            </div>
                            <?php
                        }
                    ?>
                </div>
                <div>
                    <button class="sima-ui-button" onclick="sima.hr.addPartnersToCompanyLicense($(this),'<?php echo $model->id; ?>');">Sačuvaj</button>
               </div>
            </div>
        <?php } else { ?>
            <div class="head">
                <div class="name">Osoba</div>
                <div class="references_number">Broj referenci</div>
                <div class="add_references">Izaberi reference</div>
                <div class="accepted_references">Broj prihvaćenih referenci</div>
            </div>
            <div class="body_wrap scroll-container">
                <div class="body">
                    <?php
                        foreach ($partners as $partner)
                        {
                            $references_ids = $model->getReferencesForPartner($partner);
                            ?>
                            <div class="choose_persons_to_company_licence_row" 
                                 data-id='<?php echo $partner->id; ?>' 
                                 data-references_ids='<?php echo CJSON::encode($references_ids); ?>' 
                                 data-references_number="<?php echo count($references_ids); ?>">
                                <div class="name">
                                    <?php
                                        echo $partner->DisplayHtml;
                                    ?>
                                </div>
                                <div class="references_number">
                                    <?php
                                        echo count($references_ids);
                                    ?>
                                </div>
                                <div class="add_references">
                                    <?php
                                        Yii::app()->controller->widget('SIMAButton',[
                                            'class'=>'add_reference_btn _half',
                                            'action'=>[
                                                'icon'=>'sima-icon _add _16',
                                                'tooltip'=>'Izaberi',
                                                'onclick'=>['sima.hr.addReferencesToCompanyLicense','$(this)',"$model->id", $partner->id]
                                            ]
                                        ]);
                                    ?>
                                    <div class="references_list">
                                        <?php
                                            foreach ($references_ids as $reference_id) 
                                            {
                                                if (ReferenceToCompanyLicense::model()->countByAttributes([
                                                    'reference_id'=>$reference_id,
                                                    'company_license_id'=>$model->id,
                                                    'partner_id'=>$partner->id
                                                ]) > 0)
                                                {
                                                    $reference_model = Reference::model()->findByPk($reference_id);
                                                    ?>
                                                    <div class="reference_item" data-id="<?php echo $reference_model->id; ?>" 
                                                         data-display_name="<?php echo $reference_model->DisplayName; ?>">
                                                        <?php echo $reference_model->DisplayName; ?>
                                                    </div>
                                                    <?php
                                                }
                                            }
                                        ?>
                                    </div>
                                </div>
                                <div class="accepted_references">
                                    <?php
                                        $company_license_to_partner = CompanyLicenseToPartner::getModelByPartnerAndCompanyLicense($partner,$model);
                                        if (!is_null($company_license_to_partner))
                                        {
                                            echo SIMAHtml::modelFormOpen($company_license_to_partner,[
                                                'formName'=>'onlyReferencesNumber',
                                                'onSave'=>"function(){sima.refreshActiveTab($('#$uniq'));}"
                                            ]);
                                            if (!is_null($company_license_to_partner->accepted_references_number))
                                            {
                                                echo $company_license_to_partner->accepted_references_number;
                                            }
                                        }
                                    ?>
                                </div>
                            </div>
                            <?php
                        }
                    ?>
                </div>
                <?php if (Yii::app()->user->checkAccess('RemovePersons',[],$model)) { ?>
                    <div>
                        <button class="sima-ui-button" onclick="sima.hr.removePartnersFromCompanyLicense($(this),'<?php echo $model->id; ?>');">Obriši osobe</button>
                   </div>
                <?php } ?>
            </div>
        <?php } ?>
    </div>
</div>

<script type='text/javascript'>
    
    function onInputChange<?php echo $uniq; ?>(_this)
    {
        var wrap = $('#<?php echo $uniq; ?>');
        var row = _this.parents('.choose_persons_to_company_licence_row:first');
        var $curr_references_number = wrap.find('.curr_references_number');
        var $curr_persons_number = wrap.find('.curr_persons_number');
        var used_references_ids = [];
        var references_sum = 0;
        var persons_number = 0;
        
        wrap.find('.body .choose_persons_to_company_licence_row').each(function(){
            $(this).find('.add_reference_btn').hide();
            if ($(this).find('.add input').is(':checked'))
            {
                var references_number = parseInt($(this).data('references_number'));
                if (references_number > 0)
                {
                    var old_references_sum = references_sum;
                    var references_ids = $(this).data('references_ids');
                    $.each(references_ids, function(index, value) {
                        if(jQuery.inArray(value, used_references_ids) === -1)
                        {
                            used_references_ids.push(value);
                            references_sum++;
                        }
                    });
                    if (old_references_sum !== references_sum)
                    {
                        persons_number++;
                    }
                }
            }
        });
        $curr_references_number.html(references_sum);
        $curr_persons_number.html(persons_number);
        if (references_sum < parseInt(wrap.data('references_number')))
            $curr_references_number.parent().addClass('overflow');
        else
            $curr_references_number.parent().removeClass('overflow');
        if (persons_number < parseInt(wrap.data('persons_number')))
            $curr_persons_number.parent().addClass('overflow');
        else
            $curr_persons_number.parent().removeClass('overflow');
        if (!wrap.find('.curr_references_number_wrap').hasClass('overflow') && !wrap.find('.curr_persons_number_wrap').hasClass('overflow'))
        {
            wrap.find('.error').hide("slow");
        }
        else
        {
            wrap.find('.error').show("slow");
        }
    }
    
</script>