

<?php $tab_id = SIMAHtml::uniqid(); 
    $this->widget('ext.SIMATabs.SIMATabs', array(
        'id'=>$tab_id,
        'tabs'=>array(
            array(
                'title' => 'Velike Licence',
                'code'=>'company_licenses',
                'action'=>'guitable',
                'get_params'=>array(
                    'settings'=>array(
                        'model' => 'CompanyLicenseToCompany',
                        'setRowDblClick'=>"add_license_to_bid$tab_id"
                    )
                ),
            ),
            array(
                'title' => 'Male Licence',
                'code'=>'work_licenses',
                'action'=>'guitable',
                'get_params'=>array(
                    'settings'=>array(
                        'model' => 'PersonToWorkLicense',
                        'setRowDblClick'=>"add_license_to_bid$tab_id"
                    )
                ),
            )
        ),
    ));
?>
		
<script type='text/javascript'>

function add_license_to_bid<?php echo $tab_id?>(obj)
{
    var license_model = obj.attr('model');
    var license_id = obj.attr('model_id');
    var bid_id = "<?php echo $model->id?>";
    sima.legal.addLicenseToBid(bid_id,license_model,license_id);
}

</script>