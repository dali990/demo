<?php
 	if (Yii::app()->request->isAjaxRequest) Yii::app()->clientScript->clearScriptFiles();
?>
    
        <?php
            $id_tab=SIMAHtml::uniqid();
            $this->widget('ext.SIMATabs.SIMATabs', array(
                'id'=>$id_tab,
                'list_tabs_action' => 'HR/headOf/directorTabs',
                'list_tabs_get_params' => array('id'=>$model->id),
                'list_tabs_trigger_tag' =>  SIMAHtml::getTag($model),
                'list_tabs_populate' => true,
                'default_selected'=>'view_sector'
            ));
        ?>

<?php
 	if (Yii::app()->request->isAjaxRequest) Yii::app()->clientScript->clearScriptFiles();
?>