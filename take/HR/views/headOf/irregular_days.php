<?php
    if (Yii::app()->request->isAjaxRequest) Yii::app()->clientScript->clearScriptFiles();
        
    $uniq = SIMAHtml::uniqid();
    
    $this->beginWidget('ext.SIMAResizablePanel.SIMAResizablePanel',
        array(
            'id'=>'panel_'.$uniq,
            'type'=>'vertical',
            'proportions'=>array(0.3,0.7)
            ));
?>
    
        <div class='irregular_days sima-layout-panel'>
            <?php
                $this->widget('base.extensions.SIMAGuiTable.SIMAGuiTable', [
                    'id'=>'irregular_days'.$uniq,
                    'model'=> User::model(),                    
                    'columns_type' => 'irregular_days',
                    'fixed_filter'=>array(
                        'ids'=>$user_ids,
                        'unconfirmed_irregular_days_only' => '0',
                    ),
                    'setRowSelect' => "on_select$uniq"                    
                ]);
            ?>
        </div>
        <div class='irregular_days_tabs additional_info sima-layout-panel'>
            <?php 
                $tab_id=SIMAHtml::uniqid();
                    $this->widget('ext.SIMATabs.SIMATabs', array(
                        'id'=>$tab_id,
                        'list_tabs_model_name' => 'Partner',
                        'default_selected'=>'activities'
                ));
            ?>
        </div>

<?php $this->endWidget();
 	if (Yii::app()->request->isAjaxRequest) Yii::app()->clientScript->clearScriptFiles();
?>

<script type="text/javascript">
    function on_select<?php echo $uniq; ?>(obj)
    {
        $('#<?php echo $tab_id?>').simaTabs('set_list_tabs_params',{
            list_tabs_trigger_tag: sima.getTag('<?php echo SIMAHtml::getTag(User::model()); ?>', obj.attr('model_id')),
            list_tabs_get_params:{id:obj.attr('model_id')}
        });
    }
 </script>
