<?php
if (Yii::app()->request->isAjaxRequest) Yii::app()->clientScript->clearScriptFiles();
?>

<?php $uniq = SIMAHtml::uniqid();?>

<div id='panel_<?=$uniq?>' class="sima-layout-panel _splitter _vertical">

    <div class="sima-layout-panel sima-ui-sector_tree"  data-sima-layout-init='{"proportion":0.2}'>
        <?php
            echo $tree;
        ?>
    </div>

    <div id='preview_panel_<?=$uniq?>'class="sima-layout-panel"  data-sima-layout-init='{"proportion":0.8}'>
        
    </div>

</div>

<?php if (Yii::app()->request->isAjaxRequest) Yii::app()->clientScript->clearScriptFiles();
?>

<script type='text/javascript'>
    $(function(){
            
       $("#panel_<?=$uniq?>").find('span.person_name').on('click', function(){
            $("#panel_<?=$uniq?>").find('li.selected').each(function(){$(this).removeClass('selected');});
            $(this).parent().addClass('selected');
            
            sima.ajax.get('defaultLayout',{
                get_params: {
                    model_name:'<?=Person::class?>',
                    model_id:$(this).parent().data('person-id')
                },
                success_function: function(response){
                    sima.set_html( 
                        $('#preview_panel_<?=$uniq?> '), 
                        response.html
                    );
                }
            });
       });
      
      
       $("#panel_<?=$uniq?>").find('span.sector_title').on('click', function(){
           $("#panel_<?=$uniq?>").find('li.selected').each(function(){$(this).removeClass('selected');});
            $(this).parent().addClass('selected');
            
            sima.ajax.get('defaultLayout',{
                get_params: {
                    model_name:'<?= CompanySector::class?>',
                    model_id:$(this).parent().data('sector-id')
                },
                success_function: function(response){
                    sima.set_html( 
                        $('#preview_panel_<?=$uniq?>'), 
                        response.html
                    );
                }
            });
            
            $(this).parent().children('ul').removeClass('hidden').show();
       });
       
       $("#panel_<?=$uniq?>").find('span.sima-ui-sector-tree-icon').on('click', function(){
           var parent=$(this).parent();
           var parentUlChildren=parent.children('ul');
           
           if(!parentUlChildren.hasClass('hidden'))
           {
               parentUlChildren.addClass('hidden').hide();
           }
           else
           {
               parentUlChildren.removeClass('hidden').show();
           }
           
       });
    });
    
</script>
