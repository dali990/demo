<?php
//$id=SIMAHtml::uniqid();
?>
<div class="sima-layout-panel _horizontal">
    <div class="work-contracts-header sima-layout-fixed-panel">
        <div class="work-contracts-emp-display">
            <h3>
                <?php echo $model->order_in_company ?> - <?php echo $model->DisplayName . SIMAHtml::modelFormOpen($model) ?>
            </h3>
        </div>
        <div class="work-contracts-work-times-display">
                <h4>
                <?php echo Yii::t('HRModule.WorkContract', 'StartedWorkingAt') . ': ' . $startedWorkingAt ?> - <?php echo $startedWorkingAtTime; ?> </br>
                <?php echo Yii::t('HRModule.WorkContract', 'ContinuousWorkSince') . ': ' . $continuousWorkSince ?> - <?php echo $continuousWorkSinceTime; ?> </br>
                <?php echo Yii::t('HRModule.WorkContract', 'ContinuousIndefiniteWorkSince') . ': ' . $continuousIndefiniteWorkSince ?> - <?php echo $continuousIndefiniteWorkSinceTime; ?> </br>
                </h4>
        </div>
        <div>
            <h4>
                <?php echo Yii::t('HRModule.WorkContract', 'TotalWorkExp') . ': ' . $total_work_exp ?>
            </h4>
        </div>
    </div>
    <div class="work-contracts-table sima-layout-panel">
    <?php
        $work_contract_document_types = Yii::app()->configManager->get('HR.work_contract', false);
        $work_contract_document_type = isset($work_contract_document_types[0]) ? $work_contract_document_types[0] : null;
        $file_tag = FileTag::model()->findByAttributes([
            'model_id' => $model->id,
            'model_table' => Employee::model()->tableName(),
        ]);
        $file_belongs = [
            'ids' => '{"ids":[{"id":' . $file_tag->id . ',"display_name":"' . $file_tag->DisplayName . '","class":"_visible"}],"default_item":[]}'
        ];

        $add_button = array(
            'init_data' => array(
                'WorkContract' => array(
                    'employee_id' => [
                        'disabled',
                        'init' => $model->id
                    ],                    
                    'working_percent' => 100
                ),
                'File' => [
                    'confirmed' => true,
                    'name' => $model->DisplayName,
                    'document_type_id' => $work_contract_document_type,
                    'responsible_id' => Yii::app()->user->id,
                    'file_belongs' => $file_belongs
                ]
            )
        );


        $this->widget('SIMAGuiTable', [
            'model' => WorkContract::model(),
            'fixed_filter' => array(
                'employee' => [
                    'ids' => $model->id
                ],
                'order_by' => 'start_date ASC'
            ),
    //        'select_filter' => [
    //            'signed' => 1
    //        ],
            'add_button' => $add_button,
        ]);
    ?>
    </div>
</div>