
<?php 
    $uniq = SIMAHtml::uniqid();
    $base_work_position = isset($person->employee->active_work_contract->work_position)?$person->employee->active_work_contract->work_position:null;
?>

<div id="<?php echo $uniq; ?>">
    <?php if (!is_null($base_work_position)) { ?>
        <div class="person_work_position">
            <h3>Glavna radna pozicija:
                <?php echo $base_work_position->DisplayHtml; ?>
            </h3>
        </div>
    <?php } ?>
    <div class="person_virtual_work_positions">
        <?php
            $this->widget('base.extensions.SIMAGuiTable.SIMAGuiTable', [
                'id'=>'person_to_work_positions'.$uniq,
                'model' => PersonToWorkPosition::model(),
                'columns_type' => 'fromPerson',
                'fixed_filter' => [                
                    'person'=>[
                        'ids'=>$person->id
                    ],
                    'fictive'=>true                    
                ],
                'add_button'=>[
                    'init_data'=>[
                        'PersonToWorkPosition'=>[
                            'person_id'=>[
                                'disabled',
                                'init'=>$person->id
                            ],
                            'fictive'=>true
                        ]
                    ]
                ],
                'title'=>"Privremene radne pozicije:",
                'show_add_button'=>true,
                'auto_height'=>false,
                'table_settings'=>true,    
                'pagination'=>false,
                'filters_in'=>true,
                'export'=>false,
                'filters_out'=>false,
                'headerOptionRows'=>[
                    ['title'],
                    ['table_settings','show_add_button','filters_in'],                                
                ],
            ]);
        ?>
    </div>
</div>


