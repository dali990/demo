
<div id="<?php echo $uniq; ?>" class="check_company_license_decision_disagreements" data-company_license_decision_id="<?php echo $company_license_decision->id; ?>">
    <div class="title">
        <h3><?php echo Yii::t('HRModule.CompanyLicense', 'CompanyLicenseDecisionDisagreementsTitle',[
            '{company_license_decision_name}'=>$company_license_decision->DisplayHtml,
            '{company_name}'=>$company->DisplayHtml
                ]); ?></h3>
    </div>
    <div class="header">
        <div class="row">
            <div class="company_license">
                <?php echo Yii::t('HRModule.CompanyLicense', 'CompanyLicenseDecisionDisagreementsCompanyLicense'); ?>
            </div>
            <div class="operation">
                <?php echo Yii::t('HRModule.CompanyLicense', 'CompanyLicenseDecisionDisagreementsOperation'); ?>
            </div>
        </div>
    </div>
    <div class="body">
        <?php 
            foreach ($disagreements as $disagreement)
            {
                ?>
                    <div class="row" 
                         data-operation="<?php echo $disagreement['operation']; ?>" 
                         data-company_license_id="<?php echo $disagreement['company_license']->id; ?>" 
                         data-company_id="<?php echo $disagreement['company']->id; ?>" 
                     >
                        <div class="company_license">
                            <?php
                                echo $disagreement['company_license']->DisplayHtml;
                            ?>
                        </div>
                        <div class="operation">
                            <?php echo ($disagreement['operation']==='ADD')?Yii::t('HRModule.CompanyLicense', 'CompanyLicenseDecisionDisagreementsAdd'):Yii::t('HRModule.CompanyLicense', 'CompanyLicenseDecisionDisagreementsRemove'); ?>
                        </div>
                    </div>
                <?php
            } 
        ?>
    </div>
</div>

