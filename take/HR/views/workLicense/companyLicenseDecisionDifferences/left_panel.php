<div id="company_licenses_to_companies_<?php echo $uniq; ?>" class="sima-layout-panel">
    <?php
        $this->widget('base.extensions.SIMAGuiTable.SIMAGuiTable', [
            'id'=>'company_licenses_to_companies_tbl_'.$uniq,
            'model'=> CompanyLicenseToCompany::model(),
            'columns_type'=>'byCompany',
            'add_button'=>[
                'init_data'=>[
                    'CompanyLicenseToCompany'=>[
                        'company_id'=>[
                            'disabled',
                            'init'=>$company->id
                        ]
                    ]
                ]
            ],
            'fixed_filter' => [
                'company'=>[
                    'ids'=>$company->id
                ]
            ],
            'select_filter'=>[
                'is_active'=>true
            ],
            'title'=>Yii::t('HRModule.CompanyLicense', 'CompanyLicensesOwnsByCompany', [
                '{company_name}'=>$company->DisplayName
            ])
        ]);
    ?>
</div>