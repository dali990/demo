
<div id="company_licenses_<?php echo $uniq; ?>" class="sima-layout-panel">
    <?php
        if (empty($last_decision))
        {
            echo Yii::t('HRModule.CompanyLicense', 'NoCompanyLicenseDecision');
        }
        else
        {
            $this->widget('base.extensions.SIMAGuiTable.SIMAGuiTable', [
                'id'=>"company_licenses_tbl_$uniq",
                'model'=> CompanyLicenseDecisionToCompanyLicense::model(),
                'columns_type'=>'byCompanyLicenseDecision',
                'add_button'=>[
                    'init_data'=>[
                        'CompanyLicenseDecisionToCompanyLicense'=>[
                            'company_license_decision_id'=>[
                                'disabled',
                                'init'=>$last_decision->id
                            ]
                        ]
                    ]
                ],
                'fixed_filter'=>[
                    'company_license_decision'=>[
                        'ids'=>$last_decision->id
                    ]
                ],
                'title'=>Yii::t('HRModule.CompanyLicense', 'LastCompanyLicenseDecision').': '.$last_decision->DisplayHtml.SIMAHtml::modelFormOpen($last_decision),
                'custom_buttons' => [
                    Yii::t('HRModule.CompanyLicense', 'CompanyLicenseDecisionCheckDisagreements') => [
                        'func' => "sima.hr.companyLicenseDecisionToCompanyLicensesDisagreements($(this),{$last_decision->id});"
                    ],
                ]
            ]);
        }
    ?>
</div>

