<?php
    if(empty($uniq))
    {
        $uniq = SIMAHtml::uniqid();
    }

    $this->widget('base.extensions.SIMAGuiTable.SIMAGuiTable', [
        'id'=>"company_licenses_$uniq",
        'model'=> CompanyLicenseToCompany::model(),                
        'add_button'=>true,                
        'setRowSelect' => "on_select$uniq",
        'setRowUnSelect' => "on_unselect$uniq",
        'setMultiSelect' => "on_unselect$uniq",
        'fixed_filter' => $fixed_filter
    ]);
?>


<script type="text/javascript">
    function on_select<?php echo $uniq; ?>(obj)
    {
        $('#tabs_<?php echo $uniq; ?>').simaTabs('set_list_tabs_params',{
            list_tabs_trigger_tag: sima.getTag('<?php echo SIMAHtml::getTag(CompanyLicenseToCompany::model()); ?>', obj.attr('model_id')),
            list_tabs_get_params:{id:obj.attr('model_id')}
        });
    }
    function on_unselect<?php echo $uniq; ?>(obj)
    {
        $('#tabs_<?php echo $uniq; ?>').simaTabs('set_list_tabs_params',{
            list_tabs_trigger_tag:'<?php echo SIMAHtml::getTag(CompanyLicenseToCompany::model(), 0) ?>',
            list_tabs_get_params:{id:0}
        });
    }
</script>