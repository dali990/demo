<div id="<?php echo 'left_side'.$uniq; ?>" class="sima-layout-panel">
    <?php
        $this->widget('base.extensions.SIMAGuiTable.SIMAGuiTable', [
            'id'=>'companies_'.$uniq,
            'model'=> Company::model(),
            'title'=>Yii::t('HRModule.CompanyLicense', 'Companies'),
            'add_button'=>true,
            'setRowSelect' => "on_select$uniq",
            'setRowUnSelect' => "on_unselect$uniq",
            'setMultiSelect' => "on_unselect$uniq"
        ]);
    ?>
</div>


<script type='text/javascript'>
    $(function(){
        $('#right_side<?php echo $uniq?>').hide();
    });
    function on_select<?php echo $uniq; ?>(row)
    {
        $('#right_side<?php echo $uniq?>').show();
        var company_id = row.attr('model_id');
        var company_licenses_to_companies_table = $('#company_licenses_to_companies_<?php echo $uniq?>');
        var fixed_filter = company_licenses_to_companies_table.simaGuiTable('getFixedFilter');
        if (sima.isEmpty(fixed_filter))
        {
            fixed_filter = {
                'company':{
                    'ids':company_id
                }
            };
        }
        else
        {
            fixed_filter['company'] = {'ids':company_id};
        }

        company_licenses_to_companies_table.simaGuiTable('setFixedFilter', fixed_filter).simaGuiTable('populate');
        
        company_licenses_to_companies_table.data('company_id', company_id);
    }
    function on_unselect<?php echo $uniq; ?>()
    {
        $('#right_side<?php echo $uniq?>').hide();
        $('#companies_<?php echo $uniq?>').data('company_id', null);
    }
</script>