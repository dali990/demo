
<div id="<?php echo 'right_side'.$uniq; ?>" class="sima-layout-panel">
    <?php
        $this->widget('base.extensions.SIMAGuiTable.SIMAGuiTable', [
            'id'=>'company_licenses_to_companies_'.$uniq,
            'title'=>Yii::t('HRModule.CompanyLicense', 'CompanyLicensesToCompanies'),
            'model'=> CompanyLicenseToCompany::model(),                
            'add_button'=>"sima.hr.chooseAndAddCompanyLicensesToCompany('$uniq')",
            'columns_type' => 'byCompany'
        ]);
    ?>
</div>