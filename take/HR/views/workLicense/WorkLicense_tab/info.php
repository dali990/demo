<?php $uniq=  SIMAHtml::uniqid(); ?>

<div class="work_license_info" id="work_license_<?php echo $uniq?>">
    <div class="license_title">
        <?php echo $this->renderModelView($model,'basicInfoTitle');?>
    </div> 
    <div class="basic_info">
        <?php echo $this->renderModelView($model,'basicInfo'); ?>
    </div>
</div>
