
<div id="company_licenses_<?php echo $uniq; ?>" class="sima-layout-panel" style="display: none;">
    <?php
        $this->widget('base.extensions.SIMAGuiTable.SIMAGuiTable', [
            'id'=>"company_licenses_tbl_$uniq",
            'model'=> CompanyLicenseDecisionToCompanyLicense::model(),
            'add_button'=>true,
            'columns_type'=>'byCompanyLicenseDecision'
        ]);
    ?>
</div>

