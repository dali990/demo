<?php
    $this->widget('base.extensions.SIMAGuiTable.SIMAGuiTable', [
        'id'=>"company_licenses_decisions_tbl_$uniq",
        'model'=> CompanyLicenseDecision::model(),                
        'add_button'=>true,                
        'setRowSelect' => "on_select$uniq",
        'setRowUnSelect' => "on_unselect$uniq",
        'setMultiSelect' => "on_unselect$uniq"
    ]);
?>


<script type="text/javascript">
    function on_select<?php echo $uniq; ?>(obj)
    {
        var tbl = $('#company_licenses_tbl_<?php echo $uniq; ?>');
        tbl.simaGuiTable('setFixedFilter',{
            company_license_decision:{ids:obj.attr('model_id')}
        }).simaGuiTable('setAddButton',{
            init_data:{
                CompanyLicenseDecisionToCompanyLicense: {
                    company_license_decision_id: {
                        disabled: true,
                        init: obj.attr('model_id')
                    }
                }
            }
        }).simaGuiTable('populate');
        $('#company_licenses_<?php echo $uniq; ?>').show();
    }
    function on_unselect<?php echo $uniq; ?>(obj)
    {
        $('#company_licenses_<?php echo $uniq; ?>').hide();
    }
</script>