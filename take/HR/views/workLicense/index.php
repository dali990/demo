<?php
 	if (Yii::app()->request->isAjaxRequest) Yii::app()->clientScript->clearScriptFiles();
?>

<?php $uniq = SIMAHtml::uniqid();?>

<?php 
    $this->beginWidget('ext.SIMAResizablePanel.SIMAResizablePanel',
        array(
            'id'=>'panel_'.$uniq,
            'type'=>'vertical',
            'proportions'=>array(0.3,0.7)
            ));
?>
    <div class='work_licenses sima-layout-panel'>
        <?php
            $this->widget('base.extensions.SIMAGuiTable.SIMAGuiTable', [
                'id'=>'licenses'.$uniq,
                'model'=> WorkLicense::model(),
                'add_button'=>true,
                'setRowSelect' => "on_select$uniq",
            ]);
        ?>
    </div>
    <div class='work_licenses additional_info sima-layout-panel'>
    <?php
            $id_tab=SIMAHtml::uniqid();
            $this->widget('ext.SIMATabs.SIMATabs', array(
                'id'=>$id_tab,
                'list_tabs_action' => 'HR/workLicense/licenseTabs',
//                'list_tabs_populate' => true,
                'default_selected'=>'info'
            ));  
    ?>
    </div>
<?php $this->endWidget();?>


<script type="text/javascript">

    function on_select<?php echo $uniq; ?>(obj)
    {
        $('#<?php echo $id_tab?>').simaTabs('set_list_tabs_params',{
            list_tabs_trigger_tag: sima.getTag('<?php echo SIMAHtml::getTag(WorkLicense::model()); ?>', obj.attr('model_id')),
            list_tabs_get_params:{id:obj.attr('model_id')}
        });
    }

</script>

<?php
 	if (Yii::app()->request->isAjaxRequest) Yii::app()->clientScript->clearScriptFiles();
?>