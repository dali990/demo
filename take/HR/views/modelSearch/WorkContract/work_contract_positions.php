
<?php 
    $uniq=SIMAHtml::uniqid();
    $work_contract_id = $params['work_contract_id'];
    $employee_id = $params['employee_id'];
    $company_id=Yii::app()->company->id;;
    $company = Company::model()->findByPk($company_id);
    $head_sector = CompanySector::model()->findByPk($company->head_sector_id);
    if ($head_sector==null)
    {
        throw new SIMAWarnException('Glavni sektor firme '.$company->DisplayName.' ne postoji');
    }
?>
 <div id="<?php echo $uniq; ?>" class='sectors_structure_<?php echo $uniq; ?>  sima-layout-panel sima-ui-org-scheme' tag='<?php  echo $head_sector->id?>'  > 
      <?php echo $this->renderModelView($head_sector,'sectorStructure');  ?>
 </div>
<script type="text/javascript">

$(function(){
    $('#<?php echo $uniq; ?>').on('dblclick', '.position', function(){
        var obj = $(this);
        var params = [];
        params.push([{'id':obj.attr('model_id'), 'display_name':sima.model.getDisplayName(obj.attr('model'), obj.attr('model_id'))}]);
        $('#<?php echo $uniq; ?>').parents('.sima-search-model-choose:first').trigger('add', params);
    });
});

</script>