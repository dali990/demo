
<?php

$uniq = SIMAHtml::uniqid();

$guitable_id = 'netdy_'.$uniq;
$span1_id = 'span1_'.$uniq;
$span2_id = 'span2_'.$uniq;
?>
<div class="sima-layout-panel _splitter _vertical">
    <div class="sima-layout-panel"
         data-sima-layout-init='<?=CJSON::encode([
                'proportion' => 0.4
            ])?>'>

    <?php 
    $this->widget('SIMAGuiTable', [
        'model' => NationalEvent::model(),
        'add_button' => true,
        'fixed_filter' => [
            'order_by' => 'date asc'
        ],
        'title' => Yii::t('HRModule.NationalEvent', 'NationalEventTitle'),
        'setAppendRowTrigger' => 'setAppendRowTrigger'.$uniq,
        'setRefreshRowTrigger' => 'setRefreshRowTrigger'.$uniq,
    ]); 
    ?>
    </div>
    <div class="sima-layout-panel"
         data-sima-layout-init='<?=CJSON::encode([
                'proportion' => 0.6
            ])?>'>
    <?php

    if(SIMAMisc::isVueComponentEnabled())
    {
        echo Yii::app()->controller->widget(SIMAButtonVue::class, [
                'title'=>Yii::t('HRModule.NationalEvent', 'AutoFillForCurrentYear'),
                'tooltip'=>Yii::t('HRModule.NationalEvent', 'AutoFillForCurrentYearTitle'),
                'onclick'=>['sima.hr.autoFillNationalEventsForCurrentYear',$guitable_id]
        ],true);
    }
    else
    {
        echo Yii::app()->controller->widget('SIMAButton', [
                'action'=>[ 
                    'title'=>Yii::t('HRModule.NationalEvent', 'AutoFillForCurrentYear'),
                    'tooltip'=>Yii::t('HRModule.NationalEvent', 'AutoFillForCurrentYearTitle'),
                    'onclick'=>['sima.hr.autoFillNationalEventsForCurrentYear',$guitable_id]
                ]
            ],true);
    }

    echo " | ";
    echo Yii::t('HRModule.NationalEvent', 'NumberOfUnusedNationalEvents')
            .': <span id='.$span1_id.'>'.$number_of_unused_national_events.'</span>';
    echo " | ";
    echo Yii::t('HRModule.NationalEvent', 'NumberOfUnusedNationalEventsWithNotNullDates')
            .': <span id='.$span2_id.'>'.$number_of_unused_national_events_with_notnulldates.'</span>';

    $this->widget('SIMAGuiTable', [
        'id' => $guitable_id,
        'model' => NationalEventToDayYear::model(),
        'add_button' => true,
        'select_filter'=>[
            'year' => [
                'ids' => $yearModel->id
            ],
        ],
        'title' => Yii::t('HRModule.NationalEvent', 'NationalEventDayTitle')
    ]);  
    ?>
    </div>
</div>
<script type="text/javascript">
   function setAppendRowTrigger<?php echo $uniq; ?>()
   {
       sima.hr.onChangeNationalEvent(
               '<?php echo $guitable_id; ?>', '<?php echo $span1_id; ?>', '<?php echo $span2_id; ?>'
        );
   }
   function setRefreshRowTrigger<?php echo $uniq; ?>()
   {
       sima.hr.onChangeNationalEvent(
               '<?php echo $guitable_id; ?>', '<?php echo $span1_id; ?>', '<?php echo $span2_id; ?>'
        );
   }
</script>
