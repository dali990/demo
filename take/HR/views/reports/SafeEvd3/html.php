<page size="A4">
    <div id="container safe-evd3-report">
        <header>
            <h3 style="text-align: right">
                <?=Yii::t('HRModule.SafeEvd3','Form1')?>
            </h3><br>
            <h1 class="safe-evd3-report-title safe-evd3-report-text-center">
                <?=Yii::t('HRModule.SafeEvd3','ReportOnInjuryAtWork')?>
            </h1>
        </header>
        <div class="safe-evd3-report-main">
            <?php if($save_evd_3->confirmed || $company_segment === 1): ?>
            <!-- 1. segmenat - podaci o poslodavcu-->
            <div class="safe-evd3-content">
                <div class="safe-evd3-report-title">
                    I. <?=Yii::t('HRModule.SafeEvd3','EmployerData')?>
                </div><!-- /title -->
                <table class="safe-evd3-report-table">
                    <tr>
                        <td>1</td>
                        <td><?=$translations['company_name']?></td>
                        <td><?=$segment1['company_name']?> <?=SafeEvd3Report::modelFormOpenWithCheck(Yii::app()->company)?> </td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td><?=$translations['company_address']?></td>
                        <td><?=$segment1['company_address']?></td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td><?=$translations['company_email']?></td>
                        <td><?=$segment1['company_email']?></td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td><?=$translations['company_registration_number_of_contributors']?></td>
                        <td> 
                            <div class="safe-evd3-report-array-field-container">
                            <?=SafeEvd3Report::arrayField($segment1['company_registration_number_of_contributors'], 10)?>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>5</td>
                        <td><?=$translations['company_pib']?></td>
                        <td> 
                            <div class="safe-evd3-report-array-field-container">
                                <?=SafeEvd3Report::arrayField($segment1['company_pib'], 9)?>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>6</td>
                        <td><?=$translations['company_work_code']?></td>
                        <td> 
                            <div class="safe-evd3-report-array-field-container">
                                <?=SafeEvd3Report::arrayField($segment1['company_work_code'] ?? '', 4)?>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>7</td>
                        <td><?=$translations['total_employee']?></td>
                        <td> 
                            <div class="safe-evd3-report-array-field-container">
                                <?=SafeEvd3Report::arrayField(SafeEvd3Report::getCode('cnt_employees', $segment1['company_active_employee']),1)?>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>            
            <?php endif ?>
            <?php if($save_evd_3->confirmed || $company_segment === 2): ?>
            <!-- 2. segmenat - podaci o poslodavcu korisniku-->
            <div class="safe-evd3-content">
                <div class="safe-evd3-report-title">
                    II. <?=Yii::t('HRModule.SafeEvd3','BeneficiaryEmployerData')?>
                </div><!-- /title -->
                <table class="safe-evd3-report-table">
                    <tr>
                        <td>8</td>
                        <td><?=$translations['beneficiary_name']?></td>
                        <td><?=$segment2['beneficiary_name']?></td>
                    </tr>
                    <tr>
                        <td>9</td>
                        <td><?=$translations['beneficiary_headquarters_and_address']?></td>
                        <td><?=$segment2['beneficiary_headquarters_and_address']?></td>
                    </tr>
                    <tr>
                        <td>10</td>
                        <td><?=$translations['beneficiary_email']?></td>
                        <td><?=$segment2['beneficiary_email']?></td>
                    </tr>
                    <tr>
                        <td>11</td>
                        <td><?=$translations['beneficiary_id_number']?></td>
                        <td> 
                            <div class="safe-evd3-report-array-field-container">
                            <?=SafeEvd3Report::arrayField($segment2['beneficiary_id_number'], 8)?>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>12</td>
                        <td><?=$translations['beneficiary_pib']?></td>
                        <td> 
                            <div class="safe-evd3-report-array-field-container">
                                <?=SafeEvd3Report::arrayField($segment2['beneficiary_pib'], 8)?>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>13</td>
                        <td><?=$translations['beneficiary_work_code']?></td>
                        <td> 
                            <div class="safe-evd3-report-array-field-container">
                                <?=SafeEvd3Report::arrayField($segment2['beneficiary_work_code'], 4)?>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>14</td>
                        <td><?=$translations['total_employee']?></td>
                        <td> 
                            <div class="safe-evd3-report-array-field-container">
                                <?=SafeEvd3Report::arrayField(SafeEvd3Report::getCode('cnt_employees', $segment2['beneficiary_total_employee']),1)?>
                            </div>
                        </td>
                    </tr>
                </table>               
            </div>
            <?php endif; ?>
            <?php if($save_evd_3->confirmed): ?>
            <!-- 3. segmenat - podaci o poslodavcu fizickom licu-->
            <div class="safe-evd3-content">
                <div class="safe-evd3-report-title">
                    III. <?=Yii::t('HRModule.SafeEvd3','EmployerPersonData')?>
                </div><!-- /title -->
                <table class="safe-evd3-report-table">
                    <tr>
                        <td>15</td>
                        <td><?=$translations['beneficiary_person_full_name']?></td>
                        <td><?=$segment3['beneficiary_person_full_name']?></td>
                    </tr>
                    <tr>
                        <td>16</td>
                        <td><?=$translations['beneficiary_person_id_number']?></td>
                        <td> 
                            <div class="safe-evd3-report-array-field-container">
                                <?=SafeEvd3Report::arrayField($segment3['beneficiary_person_id_number'], 13)?>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>17</td>
                        <td><?=$translations['beneficiary_person_address']?></td>
                        <td><?=$segment3['beneficiary_person_address']?></td>
                    </tr>
                    <tr>
                        <td>18</td>
                        <td><?=$translations['beneficiary_person_phone']?></td>
                        <td><?=$segment3['beneficiary_person_phone']?></td>
                    </tr>
                    <tr>
                        <td>19</td>
                        <td><?=$translations['beneficiary_person_email']?></td>
                        <td><?=$segment3['beneficiary_person_email']?></td>
                    </tr>
                </table>               
            </div>
            <?php endif; ?>
            
            <?php if(!$save_evd_3->confirmed && $company_segment === 1): ?>
                <div class="safe-evd3-content">
                    <?=Yii::t('HRModule.SafeEvd3','Segment2And3DisplayInFilePreview')?>
                </div><br>
            <?php elseif(!$save_evd_3->confirmed && $company_segment === 2): ?>
                <div class="safe-evd3-content">
                    <?=Yii::t('HRModule.SafeEvd3','Segment1And3DisplayInFilePreview')?>
                </div><br>
            <?php endif; ?>
                
            <!-- 4. segmenat - podaci o povredjenom-->
            <br>
            <div class="safe-evd3-content">
                <div class="safe-evd3-report-title">
                    IV. <?=Yii::t('HRModule.SafeEvd3','InjuredPersonInformation')?>
                </div>
                <table class="safe-evd3-report-table se3r-t-3">
                    <tr>
                        <td style="min-width: 10mm">20</td>
                        <td colspan="2" style="width: 50%"><?=$translations['employee_full_name']?></td>
                        <td style="width: 50%"><?=$segment4['employee_full_name']?></td>
                    </tr>
                    <tr>
                        <td>21</td>
                        <td colspan="2"><?=$translations['employee_id_number']?></td>
                        <td> 
                            <div class="safe-evd3-report-array-field-container">
                            <?=SafeEvd3Report::arrayField($segment4['employee_id_number'], 13)?> <?=SafeEvd3Report::modelFormOpenWithCheck($save_evd_3->employee->person)?>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>22</td>
                        <td colspan="2"><?=$translations['employee_gender']?></td>
                        <td> 
                            <div class="safe-evd3-report-array-field-container">
                                <?=SafeEvd3Report::arrayField(SafeEvd3Report::getCode('gender', $segment4['employee_gender']), 1)?>  <?=SafeEvd3Report::modelFormOpenWithCheck($save_evd_3->employee)?>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>23</td>
                        <td colspan="2"><?=$translations['employee_age']?></td>
                        <td>
                            <div class="safe-evd3-report-array-field-container">
                                <?=SafeEvd3Report::arrayField($segment4['employee_age'], 2)?>  <?=SafeEvd3Report::modelFormOpenWithCheck($save_evd_3->employee->person)?>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td rowspan="3">24</td>
                        <td rowspan="3" style=""><?=Yii::t('HRModule.SafeEvd3','Residence')?></td>
                        <td style="width: 60mm; text-align: left; padding-left: 4mm; "><?=$translations['employee_street_and_number']?></td>
                        <td style="text-align: center; padding-left: 0; "><?=$segment4['employee_street_and_number']?> <?=SafeEvd3Report::modelFormOpenWithCheck($segment4['employee_address_model'])?></td>
                    </tr>
                    <tr>
                        <td style="width: 60mm; text-align: left; padding-left: 4mm; "><?=$translations['employee_city_municipality']?></td>
                        <td style="text-align: center; padding-left: 0; "><?=$segment4['employee_city_municipality']?> <?=SafeEvd3Report::modelFormOpenWithCheck($segment4['employee_city_model'])?></td>
                    </tr>
                    <tr>
                        <td style="width: 60mm; text-align: left; padding-left: 4mm; "><?=$translations['employee_country']?></td>
                        <td style="text-align: center; padding-left: 0; "><?=$segment4['employee_country']?> <?=SafeEvd3Report::modelFormOpenWithCheck($segment4['employee_country_model'])?></td>
                    </tr>
                    <tr>
                        <td>25</td>
                        <td colspan="2"><?=$translations['employee_citizenship']?></td>
                        <td>
                            <div class="safe-evd3-report-array-field-container">
                                <?=SafeEvd3Report::arrayField($segment4['employee_citizenship'], 1)?>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>26</td>
                        <td colspan="2"><?=$translations['employee_country_name']?></td>
                        <td><?=$segment4['employee_country_name']?> <?=SafeEvd3Report::modelFormOpenWithCheck($save_evd_3->employee->person)?></td>
                    </tr>
                    <tr>
                        <td>27</td>
                        <td colspan="2"><?=$translations['employment_status']?></td>
                        <td>
                            <div class="safe-evd3-report-array-field-container">
                                <?=SafeEvd3Report::arrayField($segment4['employment_status'], 3)?>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>28</td>
                        <td colspan="2"><?=$translations['occupation']?></td>
                        <td>
                            <div class="safe-evd3-report-array-field-container">
                                <?=SafeEvd3Report::arrayField($segment4['occupation'], 6)?> 
                                <?=SafeEvd3Report::modelFormOpenWithCheck($save_evd_3->employee->last_work_contract)?> 
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>29</td>
                        <td colspan="2"><?=$translations['qualification']?></td>
                        <td>
                            <div class="safe-evd3-report-array-field-container">
                                <?=SafeEvd3Report::arrayField($segment4['qualification'], 2)?> <?=SafeEvd3Report::modelFormOpenWithCheck($save_evd_3->employee->person)?>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>30</td>
                        <td colspan="2"><?=$translations['work_exp']?></td>
                        <td>
                            <span class="safe-evd3-report-array-field-container">
                                <?=SafeEvd3Report::arrayField($segment4['work_exp_years'], 2, $leading_zero_for_wor_exp)?>
                            </span>
                            <span style="margin-left: 5mm"></span>
                            <span class="safe-evd3-report-array-field-container">
                                <?=SafeEvd3Report::arrayField($segment4['work_exp_months'], 2, $leading_zero_for_wor_exp)?>
                            </span>
                            <span style="margin-left: 5mm"></span>
                            <span class="safe-evd3-report-array-field-container">
                                <?=SafeEvd3Report::arrayField($segment4['work_exp_days'], 2, $leading_zero_for_wor_exp)?>
                            </span>
                        </td>
                    </tr>
                </table>               
            </div>
            <!-- 5. segmenat - podaci o povredi na radu-->    
            <div class="safe-evd3-content">
                <div class="safe-evd3-report-title">
                    V. <?=Yii::t('HRModule.SafeEvd3','InjuryInformationAtWork')?>
                </div>
                <table class="safe-evd3-report-table">
                    <tr>
                        <td style="min-width: 10mm">31</td>
                        <td style="width: 50%"><?=$translations['date_of_injury']?></td>
                        <td style="width: 50%">
                            <span class="safe-evd3-report-array-field-container">
                                <?=SafeEvd3Report::arrayField($segment5['date_of_injury'], 8)?>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td>32</td>
                        <td><?=$translations['time_of_injury']?></td>
                        <td>
                            <span class="safe-evd3-report-array-field-container">
                                <?=SafeEvd3Report::arrayField($segment5['time_of_injury'], 2);?>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td>33</td>
                        <td><?=$translations['beginning_working_hour']?></td>
                        <td>
                            <span class="safe-evd3-report-array-field-container">
                                <?=SafeEvd3Report::arrayField($segment5['beginning_working_hour'], 2);?>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td>34</td>
                        <td><?=$translations['injury_location_address']?></td>
                        <td>
                            <span class="safe-evd3-report-array-field-container">
                                <?=$segment5['injury_location_address']?> <?=SafeEvd3Report::modelFormOpenWithCheck($segment5['injury_location_address_model'])?>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td>35</td>
                        <td><?=$translations['municipality']?></td>
                        <td>
                            <span class="safe-evd3-report-array-field-container">
                                <?=SafeEvd3Report::arrayField($segment5['municipality'], 5);?> <?=SafeEvd3Report::modelFormOpenWithCheck($segment5['injury_location_municipality_model'])?>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td>36</td>
                        <td><?=$translations['settlement']?></td>
                        <td>
                            <span class="safe-evd3-report-array-field-container">
                                <?=SafeEvd3Report::arrayField($segment5['settlement'], 6);?> <?=SafeEvd3Report::modelFormOpenWithCheck($segment5['injury_location_settlement_model'])?>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td>37</td>
                        <td><?=$translations['country']?></td>
                        <td>
                            <span class="safe-evd3-report-array-field-container">
                                <?=SafeEvd3Report::arrayField($segment5['country'], 3);?> <?=SafeEvd3Report::modelFormOpenWithCheck($segment5['injury_location_country_model'])?>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td>38</td>
                        <td><?=$translations['commuting_accidents']?></td>
                        <td>
                            <span class="safe-evd3-report-array-field-container">
                                <?=SafeEvd3Report::arrayField($segment5['commuting_accidents'], 1);?>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td>39</td>
                        <td><?=$translations['work_environment']?></td>
                        <td>
                            <span class="safe-evd3-report-array-field-container">
                                <?=SafeEvd3Report::arrayField($segment5['work_environment'], 3);?>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td>40</td>
                        <td><?=$translations['work_process']?></td>
                        <td>
                            <span class="safe-evd3-report-array-field-container">
                                <?=SafeEvd3Report::arrayField($segment5['work_process'], 2);?>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td>41</td>
                        <td><?=$translations['specific_physical_activity']?></td>
                        <td>
                            <span class="safe-evd3-report-array-field-container">
                                <?=SafeEvd3Report::arrayField($segment5['specific_physical_activity'], 2);?>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td>42</td>
                        <td><?=$translations['source_injury']?></td>
                        <td>
                            <span class="safe-evd3-report-array-field-container">
                                <?=SafeEvd3Report::arrayField($segment5['source_injury'], 4, SafeEvd3Report::$LEADING_ZERO_IF_VALUE_OR_ZERO);?>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td>43</td>
                        <td><?=$translations['contact_injury']?></td>
                        <td>
                            <span class="safe-evd3-report-array-field-container">
                                <?=SafeEvd3Report::arrayField($segment5['contact_injury'], 2);?>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td>44</td>
                        <td><?=$translations['mode_injury']?></td>
                        <td>
                            <span class="safe-evd3-report-array-field-container">
                                <?=SafeEvd3Report::arrayField($segment5['mode_injury'], 2, SafeEvd3Report::$LEADING_ZERO_IF_VALUE_OR_ZERO);?>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td>45</td>
                        <td><?=$translations['increased_risk']?></td>
                        <td>
                            <span class="safe-evd3-report-array-field-container">
                                <?=SafeEvd3Report::arrayField($segment5['increased_risk'], 1);?>
                            </span>
                        </td>
                    </tr>
                </table>
            </div>
            <!-- 6. segmenat - Podaci o neposrednom rukovodiocu povređenog-->    
            <div class="safe-evd3-content">
                <div class="safe-evd3-report-title">
                    VI. <?=Yii::t('HRModule.SafeEvd3','BossOfInjured')?>
                </div>
                <table class="safe-evd3-report-table">
                    <tr>
                        <td style="min-width: 10mm">46</td>
                        <td style="width: 50%"><?=$translations['boss_of_injured']?></td>
                        <td style="width: 50%">
                            <span class="safe-evd3-report-array-field-container">
                                <?=$segment6['boss_of_injured']?>
                            </span>
                        </td>
                    </tr>
                </table>
            </div>
            <!-- 7. segmenat - Podaci o očevidcu-->    
            <div class="safe-evd3-content">
                <div class="safe-evd3-report-title">
                    VII. <?=Yii::t('HRModule.SafeEvd3','EyewitnessInformation')?>
                </div>
                <table class="safe-evd3-report-table">
                    <tr>
                        <td style="min-width: 10mm">47</td>
                        <td style="width: 50%"><?=$translations['eyewitness_full_name']?></td>
                        <td style="width: 50%"><?=$segment7['eyewitness_full_name']?></td>
                    </tr>
                    <tr>
                        <td>48</td>
                        <td><?=$translations['eyewitness_address']?></td>
                        <td><?=$segment7['eyewitness_address']?></td>
                    </tr>
                </table>
            </div>
            <!-- segmenat za poslodavca - datum, mesto, potpis--> 
            <div class="safe-evd3-content">
                <div class="safe-evd3-filed">
                    <span><?=Yii::t('HRModule.SafeEvd3','FormFillingDate')?>: </span>
                    <span class="safe-evd3-report-array-field-container">
                        <?=SafeEvd3Report::arrayField($employer_segment['form_filling_date']['day'], 2, SafeEvd3Report::$LEADING_ZERO_IF_VALUE);?>
                    </span>
                    <span class="safe-evd3-report-array-field-container">
                        <?=SafeEvd3Report::arrayField($employer_segment['form_filling_date']['month'], 2, SafeEvd3Report::$LEADING_ZERO_IF_VALUE);?>
                    </span>
                    <span class="safe-evd3-report-array-field-container">
                        <?=SafeEvd3Report::arrayField($employer_segment['form_filling_date']['year'], 4, SafeEvd3Report::$LEADING_ZERO_IF_VALUE);?>
                    </span>
                </div>
                <div class="safe-evd3-filed">
                    <span><?=Yii::t('HRModule.SafeEvd3','Settlement')?>: </span>
                    <div class="safe-evd3-empty-filed-underlined"><?=$employer_segment['form_filling_settlement']?></div>
                </div>
            </div>
            <div class="safe-evd3-content" style="margin: 20mm 0 20mm 0">
                <div class="safe-evd3-container">
                    <table style="width: 100%">
                        <tr>
                            <td style="width: 50%">
                                <div class="safe-evd3-report-text-center">
                                    <div>M.P.</div>
                                    <div style="margin: 10mm 0 10mm 0"><?=Yii::t('HRModule.SafeEvd3','BeneficiaryEmployerResponsiblePerson')?></div>
                                    <div class="safe-evd3-empty-filed-underlined"></div>
                                    <div>(<?=Yii::t('HRModule.SafeEvd3','FullNameAndSignature')?>)</div>
                                </div>
                            </td>
                            <td style="width: 50%">
                                <div class="safe-evd3-report-text-center">
                                    <div>M.P</div>
                                    <div style="margin: 10mm 0 10mm 0"><?=Yii::t('HRModule.SafeEvd3','EmployerResponsiblePerson')?></div>
                                    <div class="safe-evd3-empty-filed-underlined"></div>
                                    <div>(<?=Yii::t('HRModule.SafeEvd3','FullNameAndSignature')?>)</div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="safe-evd3-page-break"></div>
             <!-- 8. segmenat - Podaci o očevidcu-->    
            <div class="safe-evd3-content">
                <div class="safe-evd3-report-title">
                    VIII. <?=Yii::t('HRModule.SafeEvd3','DoctorWhoFirstExaminedTheInjured')?>
                </div>
                <table class="safe-evd3-report-table">
                    <tr>
                        <td style="min-width: 10mm">49</td>
                        <td style="width: 50%"><?=$translations['hospital_address']?></td>
                        <td style="width: 50%"><?=$segment8['hospital']?></td>
                    </tr>
                    <tr>
                        <td>50</td>
                        <td><?=$translations['injury_diagnosis']?></td>
                        <td>
                            <span class="safe-evd3-report-array-field-container">
                                <?=SafeEvd3Report::arrayField($segment8['injury_diagnosis'][0] ?? '', 4);?>
                            </span>
                            <span style="margin-left: 5mm"></span>
                            <span class="safe-evd3-report-array-field-container">
                                <?=SafeEvd3Report::arrayField($segment8['injury_diagnosis'][1] ?? '', 4);?>
                            </span>
                            <span style="margin-left: 5mm"></span>
                            <span class="safe-evd3-report-array-field-container">
                                <?=SafeEvd3Report::arrayField($segment8['injury_diagnosis'][2] ?? '', 4);?>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td>51</td>
                        <td><?=$translations['external_cause_of_injury']?></td>
                        <td>
                            <span class="safe-evd3-report-array-field-container">
                                <?=SafeEvd3Report::arrayField($segment8['external_cause_of_injury'], 3);?>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td>52</td>
                        <td><?=$translations['type_injury']?></td>
                        <td>
                            <span class="safe-evd3-report-array-field-container">
                                <?=SafeEvd3Report::arrayField($segment8['type_injury'], 3);?>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td>53</td>
                        <td><?=$translations['injury_part_of_body']?></td>
                        <td>
                            <span class="safe-evd3-report-array-field-container">
                                <?=SafeEvd3Report::arrayField($segment8['injury_part_of_body'], 2);?>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td>54</td>
                        <td><?=$translations['mark_injury']?></td>
                        <td>
                            <span class="safe-evd3-report-array-field-container">
                                <?=SafeEvd3Report::arrayField($segment8['mark_injury'], 1);?>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td>55</td>
                        <td><?=$translations['doctor_remarks']?></td>
                        <td>
                            <span class="safe-evd3-report-array-field-container">
                                <?=$segment8['doctor_remarks'];?>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td>56</td>
                        <td><?=$translations['mt3_days_unable_work']?></td>
                        <td>
                            <span class="safe-evd3-report-array-field-container">
                                <?=SafeEvd3Report::arrayField($segment8['mt3_days_unable_work'], 1);?>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td>57</td>
                        <td><?=$translations['estimated_lost_days']?></td>
                        <td>
                            <span class="safe-evd3-report-array-field-container">
                                <?=SafeEvd3Report::arrayField($segment8['estimated_lost_days'], 3);?>
                            </span>
                        </td>
                    </tr>
                </table>
            </div>
             <!-- segmenat za doktora - datum, mesto, potpis--> 
            <div class="safe-evd3-content">
                <table style="width: 100%">
                    <tr>
                        <td style="width: 43%" colspan="3">
                            <div class="safe-evd3-filed">
                                <span><?=Yii::t('HRModule.SafeEvd3','Date')?>: </span>
                                <span class="safe-evd3-report-array-field-container">
                                    <?=SafeEvd3Report::arrayField($segment8['doctor_examined_date']['day'], 2, SafeEvd3Report::$LEADING_ZERO_IF_VALUE);?>
                                </span>
                                <span class="safe-evd3-report-array-field-container">
                                    <?=SafeEvd3Report::arrayField($segment8['doctor_examined_date']['month'], 2, SafeEvd3Report::$LEADING_ZERO_IF_VALUE);?>
                                </span>
                                <span class="safe-evd3-report-array-field-container">
                                    <?=SafeEvd3Report::arrayField($segment8['doctor_examined_date']['year'], 4, SafeEvd3Report::$LEADING_ZERO_IF_VALUE);?>
                                </span>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 43%">
                            <div class="safe-evd3-filed" >
                                <span><?=Yii::t('HRModule.SafeEvd3','Settlement')?>: </span>
                                <div class="safe-evd3-empty-filed-underlined" style="width: 40mm"><?=$segment8['doctor_examined_settlement']?></div>
                            </div>
                        </td>
                        <td class="safe-evd3-report-text-center">M.P.</td>
                        <td class="safe-evd3-report-text-center"><?=Yii::t('HRModule.SafeEvd3','DoctorSignature')?></td>
                    </tr>
                    <tr>
                        <td style="width: 43%"></td>
                        <td style="width: 13%; text-align: center"></td>
                        <td style="width: 43%; text-align: center"><div class="safe-evd3-empty-filed-underlined"></div></td>
                    </tr>
                </table>
            </div>           
            <div class="safe-evd3-content safe-evd3-content-box">
                 <div class="safe-evd3-report-text-center" style="margin-bottom: 10mm">
                    <?=Yii::t('HRModule.SafeEvd3','HealthInsurance')?>
                 </div>
                 <div class="safe-evd3-filed">
                    <span><?=Yii::t('HRModule.SafeEvd3','DateOfReceivedReport')?>: </span>
                    <span class="safe-evd3-report-array-field-container">
                        <?=SafeEvd3Report::arrayField($segment8['date_of_received_report']['day'], 2, SafeEvd3Report::$LEADING_ZERO_IF_VALUE);?>
                    </span>
                    <span class="safe-evd3-report-array-field-container">
                        <?=SafeEvd3Report::arrayField($segment8['date_of_received_report']['month'], 2, SafeEvd3Report::$LEADING_ZERO_IF_VALUE);?>
                    </span>
                    <span class="safe-evd3-report-array-field-container">
                        <?=SafeEvd3Report::arrayField($segment8['date_of_received_report']['year'], 4, SafeEvd3Report::$LEADING_ZERO_IF_VALUE);?>
                    </span>
                </div>
                <div class="safe-evd3-filed">
                    <span><?=Yii::t('HRModule.SafeEvd3','ViolationDate')?>: </span>
                    <span class="safe-evd3-report-array-field-container">
                        <?=SafeEvd3Report::arrayField($segment8['violation_date']['day'], 2, SafeEvd3Report::$LEADING_ZERO_IF_VALUE);?>
                    </span>
                    <span class="safe-evd3-report-array-field-container">
                        <?=SafeEvd3Report::arrayField($segment8['violation_date']['month'], 2, SafeEvd3Report::$LEADING_ZERO_IF_VALUE);?>
                    </span>
                    <span class="safe-evd3-report-array-field-container">
                        <?=SafeEvd3Report::arrayField($segment8['violation_date']['year'], 4, SafeEvd3Report::$LEADING_ZERO_IF_VALUE);?>
                    </span>
                </div>
                <div class="safe-evd3-filed" >
                    <span><?=Yii::t('HRModule.SafeEvd3','Settlement')?>: </span>
                    <div class="safe-evd3-empty-filed-underlined"><?=$segment8['insurance_settlement']?></div>
                </div>
                <table style="width: 100%">
                    <tr>
                        <td style="width: 33%"></td>
                        <td style="width: 33%; text-align: center">M.P.</td>
                        <td style="width: 33%; text-align: center"><?=Yii::t('HRModule.SafeEvd3','ResponsiblePerson')?></td>
                    </tr>
                    <tr style="height: 15mm">
                        <td></td>
                        <td></td>
                        <td style="text-align: center"><div class="safe-evd3-empty-filed-underlined"></div></td>
                    </tr>
                </table>
             </div>
             <div class="safe-evd3-filed" style="margin: 0">
                <span><?=Yii::t('HRModule.SafeEvd3','UniqueNumberOfInjuriesAtWork')?>: </span>
                <span class="safe-evd3-report-array-field-container">
                    <?=SafeEvd3Report::arrayField($segment8['unique_injury_number'], 14);?>
                </span>
            </div>
        </div>
    </div>
</page>
