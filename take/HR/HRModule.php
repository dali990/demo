<?php

/**
 * @package SIMA
 * @subpackage HR
 */
class HRModule extends SIMAModule
{

    public function init()
    {
        parent::init();
        
        /**
         * Register Behaviors
         */
//        Year::registerPermanentBehavior('YearHRBehavior');
//        File::registerPermanentBehavior('FileHRBehavior');
//        User::registerPermanentBehavior('UserHRBehavior');
//        Day::registerPermanentBehavior('DayHRBehavior');
//        Employee::registerPermanentBehavior('EmployeeHRBehavior');
//        Absence::registerPermanentBehavior('AbsenceBehavior');
//        Absence::registerPermanentBehavior('AbsenceActivityBehavior');
//        AbsenceAnnualLeave::registerPermanentBehavior('AbsenceAnnualLeaveActivityBehavior');
//        AbsenceFreeDays::registerPermanentBehavior('AbsenceFreeDaysActivityBehavior');
//        AbsenceSickLeave::registerPermanentBehavior('AbsenceSickLeaveActivityBehavior');
//        NationalEventToDayYear::registerPermanentBehavior('NationalEventToDayActivityBehavior');
//        NationalEventStateHolidayToDay::registerPermanentBehavior('NationalEventStateHolidayToDayActivityBehavior');
//        NationalEventReligiousHolidayToDay::registerPermanentBehavior('NationalEventReligiousHolidayToDayActivityBehavior');
//        NationalEventSlavaToDay::registerPermanentBehavior('NationalEventSlavaToDayActivityBehavior');
//        FixedAsset::registerPermanentBehavior('FixedAssetHRBehavior');
//        Theme::registerPermanentBehavior('ThemeHRBehavior');
        
//        Yii::app()->activityManager->registrationActivity('AbsenceAnnualLeave');
//        Yii::app()->activityManager->registrationActivity('AbsenceFreeDays');
//        Yii::app()->activityManager->registrationActivity('AbsenceSickLeave');
//        Yii::app()->activityManager->registrationActivity('NationalEventStateHolidayToDay');
//        Yii::app()->activityManager->registrationActivity('NationalEventReligiousHolidayToDay');
//        Yii::app()->activityManager->registrationActivity('NationalEventSlavaToDay');
        
        Yii::app()->entryDoorLog->init();
        SafeEvd3Lists::init();
    }
    
    public function permanentBehaviors()
    {
        return [
            'Year' => 'YearHRBehavior',
            'File' => 'FileHRBehavior',
            'User' => 'UserHRBehavior',
            'Day' => 'DayHRBehavior',
            'Employee' => 'EmployeeHRBehavior',
            'Absence' => [
                'AbsenceBehavior',
                'AbsenceActivityBehavior'
            ],
            'AbsenceAnnualLeave' => 'AbsenceAnnualLeaveActivityBehavior',
            'AbsenceFreeDays' => 'AbsenceFreeDaysActivityBehavior',
            'AbsenceSickLeave' => 'AbsenceSickLeaveActivityBehavior',
            'NationalEventToDayYear' => 'NationalEventToDayActivityBehavior',
            'NationalEventStateHolidayToDay' => 'NationalEventStateHolidayToDayActivityBehavior',
            'NationalEventReligiousHolidayToDay' => 'NationalEventReligiousHolidayToDayActivityBehavior',
            'NationalEventSlavaToDay' => 'NationalEventSlavaToDayActivityBehavior',
            'FixedAsset' => 'FixedAssetHRBehavior',
            'Theme' => 'ThemeHRBehavior',
            'Person' => 'PersonHRBehavior',
            'Partner' => 'PartnerHRBehavior'
        ];
    }
    
    public function simaActivities()
    {
        return [
            'AbsenceAnnualLeave',
            'AbsenceFreeDays',
            'AbsenceSickLeave',
            'NationalEventStateHolidayToDay',
            'NationalEventReligiousHolidayToDay',
            'NationalEventSlavaToDay'
        ];
    }

    public function getBaseUrl()
    {
        return Yii::app()->assetManager->publish(Yii::getPathOfAlias('HR.assets'));
    }

    public function registerManual()
    {
        $urlScript = Yii::app()->assetManager->publish(Yii::getPathOfAlias('HR.assets'));

        Yii::app()->clientScript->registerCssFile($urlScript . '/css/default.css');
        Yii::app()->clientScript->registerCssFile($urlScript . '/css/sectorTree.css');
        Yii::app()->clientScript->registerScriptFile($urlScript . '/js/simaHR.js');
        Yii::app()->clientScript->registerScriptFile($urlScript . '/js/simaEmployeeOfficeEntryLog.js');
        Yii::app()->clientScript->registerScriptFile($urlScript . '/js/simaWorkContract.js');
        Yii::app()->clientScript->registerScriptFile($urlScript .'/js/simaSafeRecord.js');

        Yii::app()->clientScript->registerScript(SIMAHtml::uniqid(), "sima.hr = new SIMAHR(); sima.workContract = new SIMAWorkContract();", CClientScript::POS_READY);
        Yii::app()->clientScript->registerScript(SIMAHtml::uniqid(), "sima.employeeOfficeEntryLog = new SIMAEmployeeOfficeEntryLog();", CClientScript::POS_READY);
        Yii::app()->clientScript->registerScript(SIMAHtml::uniqid(), "sima.safeRecord = new SIMASafeRecord();", CClientScript::POS_READY);
    
        SIMASVPField::registerManual();
    }

    public function getGuiTableDefinition($index)
    {
        switch($index)
        {
            case '0':
                return [
                    'label' => Yii::t('HRModule.Absence', 'DirectorNotConfirmedAbsences'),
                    'settings' => [
                        'model' => Absence::class,
                        'fixed_filter' => [
                            'confirmed' => 'null',
                            'filter_scopes' => ['forBossSubordinates' => [Yii::app()->user->id]]
                        ]
                    ]
                ];
            case '1':
                $settings = [
                    'model' => EntryDoorLog::model(),
                    'select_filter' => [
                        'display_scopes' => [
                            'orderByTimeDesc'
                        ]
                    ],
                ];
                
                if(!Yii::app()->entryDoorLog->isByActivity())
                {
                    $settings['custom_buttons'] = [
                        Yii::t('HRModule.EntryDoor', 'GetNewEmployeeOfficeEntryLogs') => [
                            'func' => "sima.employeeOfficeEntryLog.getNewEntryDoorLogs($(this));"
                        ]
                    ];
                }
                
                return [
                    'label' => Yii::t('HRModule.EntryDoor', 'EmployeeOfficeEntryLogs'),
                    'settings' => $settings
                ];
            case '2':
                $settings = [
                    'model' => EntryDoorCard::model(),
                    'select_filter' => [
                        'display_scopes' => [
                            'orderByCardNumberASC'
                        ]
                    ],
                    'add_button' => true,
                ];
                
                if(!Yii::app()->entryDoorLog->isByActivity())
                {
                    $settings['custom_buttons'] = [
                        Yii::t('HRModule.EntryDoor', 'GetNonExistingEmployeeOfficeEntryLogCards') => [
                                'func' => "sima.employeeOfficeEntryLog.getNonExistingEntryDoorLogCards($(this));"
                            ],
                    ];
                }
                
                return [
                    'label' => Yii::t('HRModule.EntryDoor', 'EmployeeOfficeEntryLogCards'),
                    'settings' => $settings
                ];
            case '3': return [
                    'label' => Yii::t('MainMenu', 'IntroductionsIntoBusinessProgram'),
                    'settings' => [
                        'model' => IntroductionIntoBusinessProgram::model()
                    ]
                ];
            case '4': return [
                    'label' => 'Velike licence',
                    'settings' => array(
                        'model' => CompanyLicense::model(),
                        'add_button' => true
                    ),
                ];
            case '5': return [
                    'label' => 'Osobe koje su nosioci velike licence a nemaju godišnju kvotu od 15 bodova',
                    'settings' => array(
                        'model' => CompanyLicenseToPartner::model(),
                        'columns_type' => 'notCollectMinIksPoints',
                        'fixed_filter' => [
                            'partner' => [
                                'person' => [
                                    'not_collect_min_iks_points' => true
                                ]
                            ]
                        ]
                    )
                ];
            case '6': return [
                    'label' => 'Pravne godine',
                    'settings' => array(
                        'model' => HRYear::model(),
                        'add_button' => true,
                    ),
                ];
            case '7': return [
                    'label' => Yii::t('MainMenu', 'CollectiveAnnualLeaves'),
                    'settings' => array(
                        'model' => CollectiveAnnualLeave::model(),
                        'add_button' => true
                    )
                ];
            case '8': return [
                    'label' => Yii::t('BaseModule.GuiTable', 'Absences'),
                    'settings' => array(
                        'model' => Absence::model(),
                        'fixed_filter' => array(
                            'display_scopes' => 'byBeginDateAscOrder'
                        ),
                        'select_filter' => [
                            'absent_on_date' => SIMAHtml::DbToUserDate(date('d.m.Y.'))
                        ]
                    )
                ];
            case '9': return [
                    'label' => 'Odsustva koja su potvrdjena ali nemaju generisan dokument',
                    'settings' => array(
                        'add_button' => true,
                        'model' => Absence::model(),
                        'fixed_filter' => [
                            'confirmed' => true,
                            'document' => [
                                'ids' => 'null'
                            ]
                        ]
                    ),
                ];
            case '10':
                return [
                    'label' => Yii::t('HRModule.WorkContract', 'NotReturnedWorkContracts'),
                    'settings' => [
                        'model' => WorkContract::model(),
                        'fixed_filter' => [
                            'file' => [
                                'filing' => [
                                    'returned' => false
                                ]
                            ]
                        ]
                    ]
                ];
            case '11': return [
                    'label' => 'Licence koje ističu',
                    'settings' => [
                        'model' => PersonToWorkLicense::model(),
                        'columns_type' => 'expiration_licenses',
                        'select_filter' => [
                            'expiration_period' => 0
                        ]
                    ],
                ];
            case '12': return [
                    'label' => Yii::t('MainMenu', 'UnsignedWorkContracts'),
                    'settings' => array(
                        'model' => WorkContract::model(),
                        'fixed_filter' => [
                            'signed' => false
                        ]
                    )
                ];
            case '13': return [
                    'label' => 'Platni razredi',
                    'settings' => array(
                        'model' => WorkPositionPayment::model(),
                        'add_button' => true
                    ),
                ];
            case '14': return [
                    'label' => Yii::t('HRModule.WorkContract', 'WorkContractsWithDuplicatedStartDate'),
                    'settings' => [
                        'model' => WorkContract::model(),
                        'fixed_filter' => [
                            'filter_scopes' => 'hasDuplicatedStartDate'
                        ],
                        'columns_type' => 'with_employee'
                    ]
                ];
            case '15': 
                $uniq = SIMAHtml::uniqid();
                $button_parameters = [
                    'title' => Yii::t('HRModule.Occupation', 'SyncOccupations'),
                    'onclick' => ["sima.hr.syncOccupationsFromCodebook", "occupations_$uniq"]
                ];
                $additional_options_html = Yii::app()->controller->widget(SIMAButtonVue::class, $button_parameters, true);         
                return [
                    'label' => Yii::t('HRModule.Occupation', 'Occupations'),
                    'settings' => [
                        'id' => "occupations_$uniq",
                        'model' => Occupation::class,
                        'add_button' => true,
                        'additional_options_html' => $additional_options_html
                    ]
                ];
            case '16': 
                $uniq = SIMAHtml::uniqid();
                $button_parameters = [
                    'title' => Yii::t('HRModule.QualificationLevel', 'SyncQualificationLevels'),
                    'onclick' => ["sima.hr.syncQualificationLevelsFromCodebook", "qualification_levels_$uniq"]
                ];
                $additional_options_html = Yii::app()->controller->widget(SIMAButtonVue::class, $button_parameters, true);         
                return [
                    'label' => Yii::t('HRModule.QualificationLevel', 'QualificationLevel'),
                    'settings' => [
                        'id' => "qualification_levels_$uniq",
                        'model' => QualificationLevel::class,
                        'add_button' => true,
                        'additional_options_html' => $additional_options_html,
                        'fixed_filter' => [
                            'order_by' => 'code ASC'
                        ]
                    ]
                ];
            default:
                throw new SIMAGuiTableIndexNotFound('HR', $index);

        }
    }
}
