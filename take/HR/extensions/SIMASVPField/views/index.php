<div id="<?php echo $this->id?>" class="sima-svp-field <?=$disabled_class?>" style="display: inline-block;"> 
    <input type="text" class="type-of-payer" title="<?php echo Yii::t('SIMASVPField.Svp', 'TipeOfPayer')?>" name="WorkContract[type_of_payer]" value="<?php echo $type_of_payer; ?>" maxlength="1" disabled="true"/> 
&nbsp; 
    <?php 
         echo  SIMAHtml::dropDownList('WorkContract[employment_code]',$this->employment_code, WorkContract::$employment_codes , [
                    'class'=>'employment-code',
                    'title' => Yii::t('SIMASVPField.Svp', 'EmploymentCode')
                ]);
        ?>   
 &nbsp; <input class="basic-income-type" title="<?php echo Yii::t('SIMASVPField.Svp', 'Calculation')?>" type="text" name="basic_income_type" value="XXX"  maxlength="3" disabled="true"/>    
 &nbsp; 
    <?php 
         echo   SIMAHtml::dropDownList('WorkContract[benefit]',$this->benefit, WorkContract::$benefits, [
                    'class'=>'benefit',
                    'title' => Yii::t('SIMASVPField.Svp', 'Benefit')
                ]);
        ?>
 &nbsp; 
    <?php
         echo  SIMAHtml::dropDownList('WorkContract[benefited_work_experience]',$this->benefited_work_experience, WorkContract::$benefited_work_experiences,[
                    'class' => "benefited-work-experience $benefited_work_experience_disabled_class",
                    'title' => Yii::t('SIMASVPField.Svp', 'BenefitedWorkExperience')
                ]);
        ?>
</div>
