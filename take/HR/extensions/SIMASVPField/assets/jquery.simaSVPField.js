(function($){
     var methods = {
        init: function(params){
        },
        getValue : function()
        {
            var localThis = $(this);
            return {
                type_of_payer:localThis.find('.type-of-payer:first').val(),
                employment_code:localThis.find('.employment-code:first').val(),
                basic_income_type:localThis.find('.basic-income-type:first').val(),                
                benefit:localThis.find('.benefit:first').val(),
                benefited_work_experience:localThis.find('.benefited-work-experience:first').val()
            };
        },
        setValue : function(obj)
        {
            var localThis = $(this); 
            if (typeof obj.type_of_payer !== 'undefined'){
                localThis.find('.type-of-payer:first').val(obj.type_of_payer);
            }
            if (typeof obj.employment_code !== 'undefined'){
                localThis.find('.employment-code:first').val(obj.employment_code);
            }
            if (typeof obj.basic_income_type !== 'undefined'){
                localThis.find('.basic-income-type:first').val(obj.basic_income_type);
            }
            if (typeof obj.benefit !== 'undefined'){
                localThis.find('.benefit:first').val(obj.benefit);
            }
            if (typeof obj.benefited_work_experience !== 'undefined'){
                localThis.find('.benefited-work-experience:first').val(obj.benefited_work_experience);
            }
        }
    };

    $.fn.simaSVPField = function(methodOrOptions) {
        var ar = arguments;
        var i=1;
        var ret = this;
        this.each(function() {
            i++;
            if (methods[methodOrOptions]) {
                var povr = methods[ methodOrOptions ].apply($(this), Array.prototype.slice.call(ar, 1));
                if (typeof povr !== 'undefined')
                {
                    ret = povr;
                }
                return;
            } else if (typeof methodOrOptions === 'object' || !methodOrOptions) {
                // Default to "init"
                return methods.init.apply($(this), ar);
            } else {
                $.error('Method ' + methodOrOptions + ' does not exist on jQuery.simaSVPField');
            }
        });
        return ret;
    };

})( jQuery );