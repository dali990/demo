<?php

class SIMASVPField extends CInputWidget 
{
    public $id = null;
    public $model = null;
    public $class = '';
    public $employment_code = '';
    public $benefit = '';
    public $benefited_work_experience = '';

    public $html_options = [];
    
    public function run() 
    {
        if(isset($this->model) && !($this->model instanceof WorkContract))
        {
            throw new SIMAWarnException(Yii::t('SIMASVPField.Svp', 'ModelError'));
        }
        
        if(empty($this->id))
        {
            $this->id = SIMAHtml::uniqid().'_svp';
        }
        
        if (!empty($this->model))
        {
            $type_of_payer = $this->model->type_of_payer;
            $this->employment_code = $this->model->employment_code;
            $this->benefit = $this->model->benefit;
            $this->benefited_work_experience = $this->model->benefited_work_experience;
        }
        
        if (empty($type_of_payer))
        {
            $type_of_payer = Yii::app()->configManager->get('accounting.paychecks.company_type');
        }
        
        $benefited_work_experience_override = Yii::app()->configManager->get('HR.benefited_work_experience_override', false);
        $benefited_work_experience_disabled_class = '_disabled';
        if (!is_null($benefited_work_experience_override))
        {
            $benefited_work_experience_disabled_class = !SIMAMisc::filter_bool_var($benefited_work_experience_override) ? '_disabled' : '';
        }
        
        $disabled_class = '';
        if (
                isset($this->html_options['disabled']) && 
                (
                    $this->html_options['disabled'] === 'disabled' || $this->html_options['disabled'] === true
                )
           )
        {
            $disabled_class = '_disabled';
        }
        
        echo $this->render('index', array(
            'type_of_payer' => $type_of_payer,
            'benefited_work_experience_disabled_class' => $benefited_work_experience_disabled_class,
            'disabled_class' => $disabled_class
        ));

        $params = [];
        $json = CJSON::encode($params);
        Yii::app()->clientScript->registerScript(SIMAHtml::uniqid(), "$('#$this->id').simaSVPField('init',$json);", CClientScript::POS_READY);     
    }
    
    public static function registerManual() 
    {
        $assets = dirname(__FILE__) . '/assets';
        $baseUrl = Yii::app()->assetManager->publish($assets);
        Yii::app()->clientScript->registerScriptFile($baseUrl . '/jquery.simaSVPField.js' , CClientScript::POS_HEAD);
        $css_file = $baseUrl . '/simaSVPField.css';
        Yii::app()->clientScript->registerCssFile($css_file);
    }
}
