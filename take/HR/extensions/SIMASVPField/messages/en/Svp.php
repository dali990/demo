<?php
return [
    'Calculation' => 'Basic type of income - It is calculated when paying the salary',
    'TipeOfPayer' => 'Type of payer',
    'ModelError' => 'The forwarded model can only be WorkContract',
    'EmploymentCode' => 'Recipient type',
    'Benefit' => 'Equity', 
    'BenefitedWorkExperience' => 'Benefited work experience'
];