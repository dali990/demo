<?php
return [
    'Calculation' => 'Osnovna vrsta prihoda - Izračunava se prilikom obračuna plate',
    'TipeOfPayer' => 'Verzija kataloga',
    'ModelError' => 'Prosleđeni model može biti samo WorkContract',
    'EmploymentCode' => 'Vrsta primaoca',
    'Benefit' => 'Olakšice',
    'BenefitedWorkExperience' => 'Beneficirani radni staž'
];
