
/* global sima */

(function($) {

    var methods = {
        init: function(params)
        {
            var _scheme = this;
            params = params || {};
            
            _scheme.data('schemeContainer', _scheme.find('.sima-org-scheme-container'));
            _scheme.data('basicInfoPanel', _scheme.find('.sima-org-scheme-basic-info-panel'));            
            
            initScheme(_scheme);
            
            setSchemeOnHoverEvents(_scheme);
            setSchemeOnClickEvents(_scheme);
            
            setTimeout(function(){
                _scheme.trigger('sima-layout-allign');
            }, 200);
            
        },
        expandCollapseAll: function(button_id)
        {
            button_obj = $('#'+button_id);
            var _scheme = $(this);            
            if (_scheme.hasClass('expanded-all'))
            {
                _scheme.removeClass('expanded-all');
                checkExpandCollapseTitle(_scheme, sima.translate('Expand'), sima.translate('Collapse'));
                _scheme.simaOrganizationScheme('collapseAll');
            }
            else
            {
                _scheme.addClass('expanded-all');
                checkExpandCollapseTitle(_scheme, sima.translate('Collapse'), sima.translate('Expand'));
                _scheme.simaOrganizationScheme('expandAll');
            }
        },
        expandAll: function()
        {
            var _scheme = $(this);
            expandAll(_scheme);
        },
        collapseAll: function()
        {
            var _scheme = $(this);
            collapseAll(_scheme);
        },
        addSector: function()
        {
            var _scheme = $(this);
            addSector(_scheme);
        },
        addWorkPosition: function()
        {
            var _scheme = $(this);
            addWorkPosition(_scheme);
        },
        refresh: function()
        {
            var _scheme = $(this);
            refresh(_scheme);
        }
    };
    
    function initScheme(_scheme)
    {
        _scheme.data('schemeContainer').wrapInner("<div class='sima-org-scheme-wrap sima-layout-panel'></div>"); //dodajemo ga zbog zuma
        setSectorsWidth(_scheme.data('schemeContainer').find('.sima-org-scheme-wrap:first').children());
        setSectorsDisplayNameWidth(_scheme);
        setWorkPositionsDisplayNameWidth(_scheme);
        setSchemeZoom(_scheme.data('schemeContainer').find('.sima-org-scheme-wrap:first'));
        selectSector(_scheme, _scheme.find('.sima-org-scheme-sector:first'));
        expandOnlyDirectorPositions(_scheme);
    }
    
    function setSchemeOnHoverEvents(_scheme)
    {
        _scheme.find('.sima-org-scheme-sector-work-positions-exp').hover(
            function() {
                var work_positions = $(this).parents('.sima-org-scheme-sector-work-positions:first');
                if (!$(this).hasClass('_expanded'))
                {
                    work_positions.find('.sima-org-scheme-sector-work-positions-exp-all').show();
                }
            }, function() {                    
                var expand = $(this);
                var expand_all = $(this).parents('.sima-org-scheme-sector-work-positions:first').
                    find('.sima-org-scheme-sector-work-positions-exp-all');                    
                setTimeout(function(){
                    if (!expand_all.is(':hover'))
                    {
                        expand.parents('.sima-org-scheme-sector-work-positions:first').
                            find('.sima-org-scheme-sector-work-positions-exp-all').hide();
                    }
                },50);
            }
        );

        _scheme.find('.sima-org-scheme-sector-work-positions-exp-all').hover(
            function() {                    
                $(this).show();
            }, function() {                    
                $(this).hide();
            }
        );
    }
    
    function setSchemeOnClickEvents(_scheme)
    {
        //Sasa A. - morao sam svuda da stavim mousedown event umesto click eventa jer dodje do konflikta sa mousedown eventom koji je vec setovan
        //kako bi organizaciona sema mogla da se pomera.
        _scheme.on('mousedown', '.sima-org-scheme-sector-work-positions-exp', function(){                
            if ($(this).hasClass('_expanded'))
            {
                sectorCollapseAll($(this).parents('.sima-org-scheme-sector:first'));
            }
            else
            {
                sectorExpandOnlyWorkPositions($(this).parents('.sima-org-scheme-sector:first'));
            }
        });

        _scheme.on('mousedown', '.sima-org-scheme-sector-work-positions-exp-all', function(){                
            if ($(this).hasClass('_expanded'))
            {
                sectorCollapseAll($(this).parents('.sima-org-scheme-sector:first'));
            }
            else
            {
                sectorExpandAll($(this).parents('.sima-org-scheme-sector:first'));
            }
        });

        _scheme.on('mousedown', '.sima-org-scheme-sector-work-position-exp', function(event){
            event.preventDefault();
            event.stopPropagation();
            if ($(this).hasClass('_expanded'))
            {
                workPositionCollapse($(this).parents('.sima-org-scheme-sector-work-position:first'));
            }
            else
            {
                workPositionExpand($(this).parents('.sima-org-scheme-sector-work-position:first'));                    
            }
        });

        _scheme.on('mousedown', '.sima-org-scheme-sector-name', function(){
            if (!$(this).hasClass('_selected'))
            {
                selectSector(_scheme, $(this).parents('.sima-org-scheme-sector:first'));
            }
        });

        _scheme.on('mousedown', '.sima-org-scheme-sector-work-position-name', function(){                
            if (!$(this).hasClass('_selected'))
            {
                selectWorkPosition(_scheme, $(this).parents('.sima-org-scheme-sector-work-position:first'));
            }
        });

        _scheme.on('mousedown', '.sima-org-scheme-sector-work-position-person', function(){                
            if (!$(this).hasClass('_selected'))
            {
                selectPerson(_scheme, $(this));
            }
        });
    }
    
    function expandOnlyDirectorPositions(_scheme)
    {
        _scheme.find('.sima-org-scheme-sector-work-position').each(function(){
            if ($(this).hasClass('_director-position'))
            {
                workPositionExpand($(this));
            }
            else
            {
                workPositionCollapse($(this), false);
            }
        });
    }
    
    function expandAll(_scheme)
    {
        _scheme.find('.sima-org-scheme-sector').each(function(){
            sectorExpandAll($(this));
        });
    }    
    
    function collapseAll(_scheme)
    {
        _scheme.find('.sima-org-scheme-sector').each(function(){
            sectorCollapseAll($(this));
        });
    }
    
    function sectorExpandAll(sector)
    {
        var work_positions = sector.children('.sima-org-scheme-sector-work-positions');
        work_positions.find('.sima-org-scheme-sector-work-positions-exp-all').hide();        
        work_positions.find('.sima-org-scheme-sector-work-positions-exp').removeClass('_collapsed').addClass('_expanded');
        work_positions.find('.sima-org-scheme-sector-work-position').each(function(){
            workPositionExpand($(this), false);
        });
        work_positions.find('.sima-org-scheme-sector-work-position').slideDown("slow");

        setWorkPositionsDisplayNameWidth(sector);
    }
    
    function sectorExpandOnlyWorkPositions(sector)
    {
        var work_positions = sector.children('.sima-org-scheme-sector-work-positions');
        work_positions.find('.sima-org-scheme-sector-work-positions-exp').removeClass('_collapsed').addClass('_expanded');
        work_positions.find('.sima-org-scheme-sector-work-position').not('._director-position').each(function(){
            workPositionCollapse($(this), false);
        });
        work_positions.find('.sima-org-scheme-sector-work-position').not('._director-position').slideDown("slow");
        
        setWorkPositionsDisplayNameWidth(sector);
    }    
    
    function sectorCollapseAll(sector)
    {
        var work_positions = sector.children('.sima-org-scheme-sector-work-positions');        
        work_positions.find('.sima-org-scheme-sector-work-positions-exp').removeClass('_expanded').addClass('_collapsed');
        work_positions.find('.sima-org-scheme-sector-work-position').not('._director-position').each(function(){
            workPositionCollapse($(this));
        });
        work_positions.find('.sima-org-scheme-sector-work-position').not('._director-position').slideUp("slow");
    }    
    
    function workPositionExpand(work_position, slide_down)
    {
        var _slide_down = (typeof slide_down === 'undefined')?true:slide_down;
        var work_position_expand_obj = work_position.find('.sima-org-scheme-sector-work-position-exp');
        work_position_expand_obj.removeClass('_collapsed').addClass('_expanded');
        if (_slide_down === true)
        {
            work_position.find('.sima-org-scheme-sector-work-position-person').slideDown("slow");
        }
        else
        {
            work_position.find('.sima-org-scheme-sector-work-position-person').show();
        }
    }
    
    function workPositionCollapse(work_position, slideUp)
    {
        var _slideUp = (typeof slideUp === 'undefined')?true:slideUp;
        
        var work_position_collapse_obj = work_position.find('.sima-org-scheme-sector-work-position-exp');        
        work_position_collapse_obj.removeClass('_expanded').addClass('_collapsed');
        
        var position_persons;
        //ako je direktorska pozicija mogu samo fiktivna radna mesta da se skupe
        if (work_position.hasClass('_director-position'))
        {
            position_persons = work_position.find('.sima-org-scheme-sector-work-position-person').not('._not-fictive');
        }
        else
        {
            position_persons = work_position.find('.sima-org-scheme-sector-work-position-person');
        }
        
        if (_slideUp === true)
        {
            position_persons.slideUp("slow");
        }
        else
        {
            position_persons.hide();
        }
    }    
    
    function setSectorsWidth(sectors)
    {
        sectors.each(function() {
            var sector = $(this);                
            var sector_children = sector.children('.sima-org-scheme-sector-childrens').children();                
            if (sector_children.length > 0)
            {
                setSectorsWidth(sector_children);
                var sector_width = 0;
                sector_children.each(function(){
                    sector_width += $(this).outerWidth();
                });                
                sector.css({width:sector_width});
            }
        });
    }    
    
    function addSector(_scheme)
    {        
        sima.model.form('CompanySector','',{            
            init_data:{}
        });
    }
    
    function addWorkPosition(_scheme)
    {
        sima.model.form('WorkPosition','',{            
            init_data:{}            
        });
    }
    
    function setSchemeZoom(_scheme_wrap)
    {
        _scheme_wrap.addClass('disabled-selection');
        _scheme_wrap.attr('ng-controller', 'OrganizationSchemeController');        
        _scheme_wrap.wrapInner('<panzoom id="PanZoom" config="panzoomConfig" model="panzoomModel" class="sima-layout-panel"></panzoom>');
        _scheme_wrap.prepend('<panzoomwidget panzoom-id="PanZoom"></panzoomwidget>');        
        var app = angular.module('sima-organization-scheme', ['panzoom','panzoomwidget']);
        app.controller('OrganizationSchemeController', ['$scope', 'PanZoomService', function ($scope, PanZoomService) {
            $scope.panzoomConfig = {
                zoomLevels: 12,
                neutralZoomLevel: 5,
                scalePerZoomLevel: 1.4,
                zoomToFitZoomLevelFactor: 1,
                zoomOnDoubleClick: false,
                invertMouseWheel: true
            };
            $scope.panzoomModel = {};
        }]);        
        angular.bootstrap(_scheme_wrap, ['sima-organization-scheme']);
    }
    
    function setSectorsDisplayNameWidth(_scheme_part)
    {
        _scheme_part.find('.sima-org-scheme-sector').each(function(){
            var name_obj = $(this).children('.sima-org-scheme-sector-name');
            var display_name_obj = name_obj.find('.sima-org-scheme-sector-display-name');
            var cnt_obj = name_obj.find('.sima-org-scheme-sector-person-cnt');
            var new_width = name_obj.width()-10;

            if (typeof cnt_obj !== 'undefined')
            {                
                new_width -= cnt_obj.outerWidth(true);
            }
            display_name_obj.css({width:new_width});
        });
    }
    
    function setWorkPositionsDisplayNameWidth(_scheme_part)
    {
        _scheme_part.find('.sima-org-scheme-sector-work-position').each(function(){
            var name_obj = $(this).children('.sima-org-scheme-sector-work-position-name');
            var display_name_obj = name_obj.find('.sima-org-scheme-sector-work-position-display-name');
            var cnt_obj = name_obj.find('.sima-org-scheme-sector-work-position-person-cnt');
            var exp_obj = name_obj.find('.sima-org-scheme-sector-work-position-exp');
            var new_width = name_obj.width()-10;

            if (typeof cnt_obj !== 'undefined')
            {                
                new_width -= cnt_obj.outerWidth(true);
            }
            if (typeof exp_obj !== 'undefined')
            {                
                new_width -= exp_obj.outerWidth(true);
            }
            display_name_obj.css({width:new_width});
        });
    }
    
    function selectSector(_scheme, sector)
    {
        if (typeof _scheme.data('basicInfoPanel') !== 'undefined')
        {
            unselectAll(_scheme);
            sector.children('.sima-org-scheme-sector-name').addClass('_selected');
            sima.ajax.get('base/model/view',{
                get_params:{
                    model:'CompanySector',
                    model_id:sector.attr('model_id'),
                    model_view:'basic_info_from_organization_scheme'
                },
                success_function:function(response){
                    _scheme.data('basicInfoPanel').html(response.html);
                }
            });
        }
    }
    
    function selectWorkPosition(_scheme, work_position)
    {
        if (typeof _scheme.data('basicInfoPanel') !== 'undefined')
        {
            unselectAll(_scheme);
            work_position.children('.sima-org-scheme-sector-work-position-name').addClass('_selected');
            work_position.children('.sima-org-scheme-sector-work-position-name').find('.sima-org-scheme-sector-work-position-exp').removeClass('_black').addClass('_white');
            sima.ajax.get('base/model/view',{
                get_params:{
                    model:'WorkPosition',
                    model_id:work_position.attr('model_id'),
                    model_view:'basic_info_from_organization_scheme'
                },
                success_function:function(response){
                    _scheme.data('basicInfoPanel').html(response.html);
                }
            });
        }
    }
    
    function selectPerson(_scheme, person)
    {
        if (typeof _scheme.data('basicInfoPanel') !== 'undefined')
        {
            unselectAll(_scheme);
            person.addClass('_selected');            
            sima.ajax.get('base/model/view',{
                get_params:{
                    model:'Person',
                    model_id:person.attr('person_id'),
                    model_view:'basicInfoOrganizationScheme'
                },
                success_function:function(response){
                    _scheme.data('basicInfoPanel').html(response.html);
                }
            });
        }
    }        
    
    function unselectSector(_scheme)
    {
        _scheme.find('.sima-org-scheme-sector-name._selected').removeClass('_selected');
    }
    
    function unselectWorkPosition(_scheme)
    {
        _scheme.find('.sima-org-scheme-sector-work-position-name._selected').each(function(){
            $(this).removeClass('_selected');
            $(this).find('.sima-org-scheme-sector-work-position-exp').removeClass('_white').addClass('_black');
        });
    }
    
    function unselectPerson(_scheme)
    {
        _scheme.find('.sima-org-scheme-sector-work-position-person._selected').removeClass('_selected');
    }
    
    function unselectAll(_scheme)
    {
        unselectSector(_scheme);
        unselectWorkPosition(_scheme);
        unselectPerson(_scheme);
    }
    
    function refresh(_scheme)
    {
        sima.ajax.get('organizationScheme/refreshOrganizationScheme',{
            loadingCircle:_scheme.data('schemeContainer'),
            async: true,
            success_function:function(response){
                _scheme.data('schemeContainer').html(response.html);
                initScheme(_scheme);
                setSchemeOnHoverEvents(_scheme);
                _scheme.removeClass('expanded-all');
                checkExpandCollapseTitle(_scheme, sima.translate('Expand'), sima.translate('Collapse'));
                sima.layout.allignObject(_scheme.data('schemeContainer'), 'TRIGGER_on_org_scheme_refresh');
            }
        });
    }
    
    function checkExpandCollapseTitle(_scheme, new_title, old_title)
    {
        var button = _scheme.find('div.sima-btn span.sima-btn-title[title="'+old_title+'"]');
        button.attr('title', new_title);
        button.html(new_title);
        sima.layout.allignObject(_scheme);
    }
    
    $.fn.simaOrganizationScheme = function(methodOrOptions) {
        var ar = arguments;
        var i=1;
        var ret = this;
        this.each(function() {
            i++;
            if (methods[methodOrOptions]) {
                var povr = methods[ methodOrOptions ].apply($(this), Array.prototype.slice.call(ar, 1));
                if (typeof povr !== 'undefined')
                {
                    ret = povr;
                }
                return;
            } else if (typeof methodOrOptions === 'object' || !methodOrOptions) {
                // Default to "init"
                return methods.init.apply($(this), ar);
            } else {
                $.error('Method ' + methodOrOptions + ' does not exist on jQuery.simaOrganizationScheme');
            }
        });
        return ret;
    };
})(jQuery);
