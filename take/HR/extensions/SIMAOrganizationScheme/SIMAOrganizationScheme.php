<?php

class SIMAOrganizationScheme extends CWidget 
{
    public $id = null;
    public $head_sector = null;
    
    public function run() 
    {
        $uniq = SIMAHtml::uniqid();
        
        if(empty($this->id))
        {
            $this->id = SIMAHtml::uniqid().'_sima_organization_scheme';
        }
        
        if (empty($this->head_sector))
        {
            $company = Yii::app()->company;
            $company->getHeadSector();//pravi head_Sector ukoliko je potrebo
            $this->head_sector = CompanySector::model()->findByPk($company->head_sector_id);
        }
                
        $panels = [
            [
                'proportion'=>0.7,
                'html'=>$this->render('scheme_panel', [
                    'head_sector'=>$this->head_sector
                ], true)
            ],
            [
                'proportion'=>0.3,
                'html'=>'<div class="sima-org-scheme-basic-info-panel sima-layout-panel"></div>'
            ]
        ];
        
        $expand_collapse_all_button_id = $uniq.'eca';
        
        if(SIMAMisc::isVueComponentEnabled())
        {
            $refresh_button = Yii::app()->controller->widget(SIMAButtonVue::class, [
                'title'=> Yii::t('SIMAOrganizationScheme.Translation', 'Refresh'),
                'tooltip'=> Yii::t('SIMAOrganizationScheme.Translation', 'Refresh'),
                'onclick'=>['sima.callPluginMethod', '#'.$this->id, 'simaOrganizationScheme', 'refresh']
            ], true);
            
            $expand_all_button = Yii::app()->controller->widget(SIMAButtonVue::class, [
                'id' => $expand_collapse_all_button_id,
                'title'=> Yii::t('SIMAOrganizationScheme.Translation', 'ExpandAll'),
                'tooltip'=> Yii::t('SIMAOrganizationScheme.Translation', 'ExpandAll'),
                'onclick'=>['sima.callPluginMethod', '#'.$this->id, 'simaOrganizationScheme', 'expandCollapseAll', "$expand_collapse_all_button_id"]
            ], true);
            
            $add_sector_button = Yii::app()->controller->widget(SIMAButtonVue::class, [
                'title'=> Yii::t('SIMAOrganizationScheme.Translation', 'AddSector'),
                'tooltip'=> Yii::t('SIMAOrganizationScheme.Translation', 'AddSector'),
                'onclick'=>['sima.callPluginMethod', '#'.$this->id, 'simaOrganizationScheme', 'addSector']
            ], true);
            
            $add_work_position_button = Yii::app()->controller->widget(SIMAButtonVue::class, [
                'title'=> Yii::t('SIMAOrganizationScheme.Translation', 'AddWorkPositon'),
                'tooltip'=> Yii::t('SIMAOrganizationScheme.Translation', 'AddWorkPositon'),
                'onclick'=>['sima.callPluginMethod', '#'.$this->id, 'simaOrganizationScheme', 'addWorkPosition']
            ], true);
        }
        else
        {
            $refresh_button = Yii::app()->controller->widget('SIMAButton', [
                'action'=>[ 
                    'title'=> Yii::t('SIMAOrganizationScheme.Translation', 'Refresh'),
                    'tooltip'=> Yii::t('SIMAOrganizationScheme.Translation', 'Refresh'),
                    'onclick'=>['sima.callPluginMethod', '#'.$this->id, 'simaOrganizationScheme', 'refresh']
                ]
            ], true);
            
            $expand_all_button = Yii::app()->controller->widget('SIMAButton', [
                'id' => $expand_collapse_all_button_id,
                'action'=>[ 
                    'title'=> Yii::t('SIMAOrganizationScheme.Translation', 'ExpandAll'),
                    'tooltip'=> Yii::t('SIMAOrganizationScheme.Translation', 'ExpandAll'),
                    'onclick'=>['sima.callPluginMethod', '#'.$this->id, 'simaOrganizationScheme', 'expandCollapseAll', "$expand_collapse_all_button_id"]
                ]
            ], true);
            
            $add_sector_button = Yii::app()->controller->widget('SIMAButton', [
                'action'=>[ 
                    'title'=> Yii::t('SIMAOrganizationScheme.Translation', 'AddSector'),
                    'tooltip'=> Yii::t('SIMAOrganizationScheme.Translation', 'AddSector'),
                    'onclick'=>['sima.callPluginMethod', '#'.$this->id, 'simaOrganizationScheme', 'addSector']
                ]
            ], true);
            
            $add_work_position_button = Yii::app()->controller->widget('SIMAButton', [
                'action'=>[ 
                    'title'=> Yii::t('SIMAOrganizationScheme.Translation', 'AddWorkPositon'),
                    'tooltip'=> Yii::t('SIMAOrganizationScheme.Translation', 'AddWorkPositon'),
                    'onclick'=>['sima.callPluginMethod', '#'.$this->id, 'simaOrganizationScheme', 'addWorkPosition']
                ]
            ], true);
        }
        
        echo $this->render('index', [
            'content_layout'=>Yii::app()->controller->renderContentLayout([
                'name' => Yii::t('LegalModule.Legal', 'OrganizationScheme'),
                'options_class'=>'sima-org-scheme-options',
                'options'=>[
                    [
                        'html' => $refresh_button
                    ],
                    [
                        'html' => $expand_all_button
                    ],
                    [
                        'html'=> $add_sector_button
                    ],
                    [
                        'html'=> $add_work_position_button
                    ]
                ]
            ], $panels, [
                'id'=>$this->id,
                'class'=>'sima-org-scheme',
                'processOutput'=>false
            ])
        ]);
                
        $params = [];
        $json = CJSON::encode($params);
        $register_script = "$('#" . $this->id . "').simaOrganizationScheme('init',$json);";
        Yii::app()->clientScript->registerScript(SIMAHtml::uniqid(), $register_script, CClientScript::POS_READY);
    }
    
    public function registerManual() {
        $assets = dirname(__FILE__) . '/assets';
        $baseUrl = Yii::app()->assetManager->publish($assets);
        Yii::app()->clientScript->registerScriptFile($baseUrl . '/js/jquery.simaOrganizationScheme.js', CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerCssFile($baseUrl . '/css/simaOrganizationScheme.css');
    }
}