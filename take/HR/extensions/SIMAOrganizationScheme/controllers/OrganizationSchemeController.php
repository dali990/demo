<?php

class OrganizationSchemeController extends SIMAController
{
    public $views_path = 'HR.extensions.SIMAOrganizationScheme.views.organizationScheme.';
    
    public function filters()
    {
        return array(
            'ajaxOnly'
        )+parent::filters();
    }    
    
    public function actionIndex()
    {
        $id = Yii::app()->company->id;
        $company = Company::model()->findByPk($id);
        if ($company==null)     
            throw new Exception('kompanija ne postoji!!!');                       
        
        $html = $this->renderPartial($this->views_path.'index', [], true, true);

        $this->respondOK([
            'html' => $html,
            'title' => Yii::t('LegalModule.Legal', 'OrganizationScheme').' '.$company->DisplayName,
        ]);
    }

    public function actionTabIndex($id)
    {
        $company = Company::model()->findByPk($id);
        if ($company==null)     
            throw new Exception('kompanija ne postoji!!!');
        
        if (!isset($company->head_sector_id))
        {
            $company_head_sector=new CompanySector();
            $company_head_sector->name=$company->DisplayName;
            $company_head_sector->type="Head";
            $company_head_sector->code="1.0";
            $company_head_sector->save();
            $company_head_sector->refresh();

            $company->head_sector_id = $company_head_sector->id;
            $company->save();
        }
       
        $head_sector = CompanySector::model()->findByPk($company->head_sector_id);
        $html=$this->renderPartial($this->views_path.'index', array(
            'model'=>$head_sector, 
            'company'=>$company
        ), true, true);
        
        $this->respondOK([
            'html'=>$html
        ]);
    }
    
    public function actionTabEmployeeOrg($company_id, $id)
    {
        $company = Company::model()->with('head_sector')->findByPk($company_id);

        $head_sector = $company->HeadSector;

        $html = $this->renderPartial($this->views_path.'OrgScheme_tab.personScheme', array(
            'model' => $head_sector, 
            'partner_id'=>$id
        ), true, true);
        
        $this->respondOK(['html'=>$html]);
    }
    
    /// MilosJ: nije nadjeno da se koristi
//    public function actionAddWorkPositionToEmployee()
//    {
//          $work_position=$_GET['work_position_id'];
//          $employee=$_GET['employee_id'];
//          
//          $wp=  WorkPosition::model()->findByPk($work_position);
//          $person=  Person::model()->findByPk($employee);
//          
//          $employee_to_work_position=new PersonToWorkPosition();
//          $employee_to_work_position->person_id=$employee;
//          $employee_to_work_position->work_position_id=$work_position;
//          $employee_to_work_position->save();
//          
//          $updateModel='';
//          if(isset($_GET['updateModelViews']))
//              $updateModel= $_GET['updateModelViews'];
//          
//          echo CJSON::encode(array(
//                'status' => 'success',
//                'message' => 'uspesno',
//                'updateModelViews'=>  $updateModel,
//                'updatePersonViews'=> SIMAHtml::getTag($person),
//                'response'=>array(
//                   'updateTabs'=>''
//                )
//            ));
//    }
    
    /// MilosJ: nije nadjeno da se koristi
//    public function actionRemoveWorkPositionToEmployee()
//    {
//        $work_position=$_GET['work_position_id'];
//        $employee=$_GET['employee_id'];
//        
//        $workToEmployee = PersonToWorkPosition::model()->findByAttributes(array('person_id'=>$employee, 'work_position_id'=>$work_position));
//        $workToEmployee->delete();
//        
//        echo CJSON::encode(array(
//                'status' => 'success',
//                'message' => 'uspesno',
//                'updateModelViews'=>'',
//                'response'=>array(
//                   'updateTabs'=>''
//                )
//            ));
//    }
   
    public function actionSelectEmployeeWorkPositions()
    {
        $employee_id = $_POST['employee_id'];
        
        $work_positions = PersonToWorkPosition::model()->findAllByAttributes(array('person_id'=>$employee_id));
        
         $positions = array();
         foreach ($work_positions as $pos)
          {
                    $positions[] = $pos->work_position_id;
          }
           
        $this->respondOK([
            'work_positions' => $positions,
        ]);
    }    
    
    public function actionSectorActiveTasksTabs($id)
    {
        $tabs=array();
        $sector=  CompanySector::model()->findByPk($id);
        if($sector==null)
        {
            throw new Exception("Sektor sa ID: ".$id." vise ne postoji u bazi!");
        }
        
        foreach ($sector->work_positions as $position)
        {
            foreach ($position->persons as $person)
            {
                $tabs=array_merge($tabs, array(
                    array(
                        'title'=>$person->DisplayName,
                        'code'=>'person_active_tasks'.$person->id,
                        'action'=> 'jobs/task/tabActiveTasks',
                        'get_params' => array('user_id' => $person->id)
                        )
                ));
            }
        }
        
        
        $this->respondOK(array('tabs'=>$tabs));
    }
    
    public function actionRefreshOrganizationScheme()
    {
        $company = Yii::app()->company;
        $company->getHeadSector();//pravi head_Sector ukoliko je potrebo
        $head_sector = CompanySector::model()->findByPk($company->head_sector_id);
        
        $html = Yii::app()->controller->renderModelView($head_sector, 'sectorStructure');
        
        $this->respondOK([
            'html'=>$html
        ]);
    }
}

?>
