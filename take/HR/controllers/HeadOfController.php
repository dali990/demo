<?php
/**
 * Description of HeadOfController
 *
 * @author goran-set
 */
class HeadOfController extends SIMAController
{
    public function actionIndex()
    {
        $html=  $this->renderPartial('index', [
            'model'=>Yii::app()->user->model
        ], true);
        
        $this->respondOK([
            'title' => Yii::t('MainMenu', 'MyTeam'),
            'html'=>$html
        ]);
    }
    public function actionIrregularDay($id)
    {
        $day=  TaskReportIrregularDay::model()->findByPk($id);
        $html=  $this->renderPartial('irregular_day', array('model'=>$day), true, true);
        
        $this->respondOK(array('html'=>  $html));
    }
    public function actionTabViewSector($id)
    {
        $user = Yii::app()->user->model;
        $tree='';
        foreach ($user->person->getAllSectorsWhereIamDirector() as $sector)
        {
           $tree.='<ul class="sector_tree_structure">';
           $tree.=$this->getSectorTree($sector);
           $tree.='</ul>'; 
        }        
        
        $html=  $this->renderPartial('director', array('tree'=>$tree), true, true);
        
        $this->respondOK(array('html'=>$html)); 
    }
    
    public function getSectorTree($sector)
    {
        $tree='';
        $tree.='<li class="sector" data-parent-id="'.$sector->parent_id.'" '
                . 'data-sector-id="'.$sector->id.'">'
                . '<span class="sima-ui-sector-tree-icon sector-icon">&nbsp;</span>'
                . '<span class="sima-ui-sector-tree-title sector_title">'.$sector->DisplayName.'</span>'
                . '<ul class="hidden" style="display:none">';
        
        foreach ($sector->work_positions as $position)
        {
            foreach ($position->persons as $person)
            {
                $tree.='<li class="person_to_sector" data-person-id="'.$person->id.'" '
                        . 'data-sector-id="'.$sector->id.'">'
                        .'<span class="sima-ui-sector-tree-icon person-icon">&nbsp;</span>'
                        . '<span class="sima-ui-sector-tree-title person_name">'.$person->DisplayName.'</span></li>';
            }
        }
        
        foreach ($sector->childrens as $child)
        {
            $tree.=$this->getSectorTree($child);
        }
        
        $tree.='</ul></li>';
         
        return $tree;
    }
    
    public function actionDirectorTabs($id)
    {   
        $tabs = [
            [
                'title' => Yii::t('HRModule.MyTeam', 'SectorOverview'),
                'code'=>'view_sector',
                'action' =>'HR/headOf/tabViewSector',
                'get_params' => [
                    'id' => $id
                ]
            ],
            [
                'title' => Yii::t('JobsModule.Task', 'QualityCheck'),
                'code'=>'tasks_quality_check',
                'action' => 'guitable',
                'get_params' => [
                    'settings'=>[
                        'model' => Task::class,
                        'fixed_filter' => [
                            'qualitychecker' => [
                                'ids' => Yii::app()->user->id
                            ],
                            'scopes' => [
                                'forQualityCheck'
                            ]
                        ]
                    ]
                ],
            ]
        ];
        $this->respondOK(array('tabs'=>$tabs));
    }
}
