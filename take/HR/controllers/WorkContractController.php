<?php

class WorkContractController extends SIMAController
{
    public function actionTabWorkContracts($id)
    {        
        $empModel = Employee::model()->findByPk($id);
        if ($empModel == null)
        {
            throw new Exception('Partner ne postoji');
        }
        
        $continuousWorkSince = null;
        $continuousIndefiniteWorkSince = null;
        $startedWorkingAt = null;
        $continuousWorkSinceTime = null;
        $continuousIndefiniteWorkSinceTime = null;
        $startedWorkingAtTime = null;
        
        $current_work_contract = $empModel->last_work_contract;
        
        if(isset($current_work_contract))
        {
            $startedWorkingAt = $empModel->first_work_contract->start_date;
            
            $continuousWorkSince = $current_work_contract->start_date;

            if($current_work_contract->isIndefinite())
            {
                $continuousIndefiniteWorkSince = $current_work_contract->start_date;
            }

            $searchingContinuousIndefiniteWorkSince = true;

            $prev_work_contract = $current_work_contract->prev();
            while(isset($prev_work_contract))
            {
                $continuousWorkSince = $prev_work_contract->start_date;

                if($searchingContinuousIndefiniteWorkSince === true
                        && $prev_work_contract->isIndefinite())
                {
                    $continuousIndefiniteWorkSince = $prev_work_contract->start_date;
                }
                else
                {
                    $searchingContinuousIndefiniteWorkSince = false;
                }

                $prev_work_contract = $prev_work_contract->prev();
            }
        }
        
        if(isset($startedWorkingAt))
        {
            $startedWorkingAtTime = WorkContractComponent::formatWorkExp(WorkContractComponent::getWorkExpBetweenTwoDates($startedWorkingAt, date('d.m.Y.')));
        }
        
        if(isset($continuousWorkSince))
        {
            $continuousWorkSinceTime = WorkContractComponent::formatWorkExp(WorkContractComponent::getWorkExpBetweenTwoDates($continuousWorkSince, date('d.m.Y.')));
        }
        
        if(isset($continuousIndefiniteWorkSince))
        {
            $continuousIndefiniteWorkSinceTime = WorkContractComponent::formatWorkExp(WorkContractComponent::getWorkExpBetweenTwoDates($continuousIndefiniteWorkSince, date('d.m.Y.')));
        }
        
        $total_work_days = $empModel->old_work_exp * 12*30;
        $emp_work_contracts = $empModel->work_contracts;
        foreach($emp_work_contracts as $emp_work_contract)
        {
            $total_work_days += $emp_work_contract->getWorkExperience();
        }
        $total_work_exp = WorkContractComponent::formatWorkExp($total_work_days);
                
        $html = $this->renderPartial('work_contracts', array(
            'model' => $empModel,
            'startedWorkingAt' => $startedWorkingAt,
            'continuousWorkSince' => $continuousWorkSince,
            'continuousIndefiniteWorkSince' => $continuousIndefiniteWorkSince,
            'startedWorkingAtTime' => $startedWorkingAtTime,
            'continuousWorkSinceTime' => $continuousWorkSinceTime,
            'continuousIndefiniteWorkSinceTime' => $continuousIndefiniteWorkSinceTime,
            'total_work_exp' => $total_work_exp
        ), true, true);

        $this->respondOK(array('html' => $html));
    }
    
    public function actionWorkContractWithOutWorkPosition()
    {
        $html = $this->renderPartial('work_contract_without_work_position', array(), true, true);
        $this->respondOK(array('html' => $html, 'title' => 'Radni ugovori'));
    }
    
    public function actionGenerateWorkContractTemplates($work_contract_id, $template_type)
    {
        $work_contract = WorkContract::model()->findByPk($work_contract_id);
        $file_id = $work_contract_id;
        $refresh_model = $work_contract;
        if ($template_type === '1')
        {
            if ($work_contract->end_date === null) //ako je ugovor na neodredjeno
            {
                $template_id = Yii::app()->configManager->get('HR.indefinite_work_contract_template', false);
                if (empty($template_id))
                {
                    throw new SIMAWarnException('Niste template za ugovor na neodredjeno');
                }
            }
            else //ako je ugovor na odredjeno
            {
                $template_id = Yii::app()->configManager->get('HR.fixed_work_contract_template', false);
                if (empty($template_id))
                {
                    throw new SIMAWarnException('Niste postavili template za ugovor na odredjeno');
                }
            }
        }
        else if ($template_type === '3') //ako je slucaj da je notice_of_mobbing
        {
            $template_id = Yii::app()->configManager->get('legal.notice_of_mobbing_template', false);
            if (empty($template_id))
            {
                throw new SIMAWarnException('Niste template za obavestenje za mobing');
            }
            if (!isset($work_contract->notice_of_mobbing))
            {
                $file = new File();
                $notice_of_mobbing_document_type_id = Yii::app()->configManager->get('HR.notice_of_mobbing', false);
                if ($notice_of_mobbing_document_type_id === '')
                    $this->raiseNote("Za dokumenat 'Obaveštenje na mobing' nije postavljen tip dokumenta. Kontaktirajte administratora kako bi postavio tip dokumenta!");
                $file->document_type_id = $notice_of_mobbing_document_type_id;
                $file->responsible_id = Yii::app()->user->id;
                $file->save();
                $file->refresh();
                $work_contract->notice_of_mobbing_file_id = $file->id;
                $work_contract->save();
            }
            $file_id = $work_contract->notice_of_mobbing_file_id;
        }
        else if ($template_type === '4' && isset($work_contract->close_work_contract)) //ako je slucaj da je raskid ugovora o radu
        {
            $template_id = Yii::app()->configManager->get('HR.close_work_contract_template', false);
            if (empty($template_id))
            {
                throw new SIMAWarnException('Niste template za prekid radnog odnosa');
            }
            $file_id = $work_contract->close_work_contract->id;
            $refresh_model = $work_contract->close_work_contract;
        }
        $templated_file = TemplatedFile::model()->with('templated_file_data')->findByPk($file_id);
        if ($templated_file === null)
        {
            $template_file_id = TemplateController::setTemplate($file_id, $template_id);
        }
        else
        {
            //ako je promenjen template, onda ga updejtujemo
            if (intval($templated_file->template_id) !== intval($template_id))
            {
                $templated_file->template_id = $template_id;
                $templated_file->save();
            }
            $template_file_id = $templated_file->id;
        }
        TemplateController::addVersion($template_file_id);

        $this->respondOK(array(), array($refresh_model));
    }
    
    public function actionPersonWorkPositions($person_id)
    {
        $person = ModelController::GetModel('Person', $person_id);
        $html = $this->renderPartial('person_work_position', [
            'person'=>$person
        ], true, true);

        $this->respondOK(array(
            'html' => $html,
            'title' => 'Radne pozicije',
            'color' => '#FED39E'
        ));
    }
    
    public function actionGetBenefitedWorkExperience($work_position_id)
    {
        $model_work_position = WorkPosition::model()->findByPkWithCheck($work_position_id);
        
        $this->respondOK(array(
            'benefited_work_experience' => $model_work_position->benefited_work_experience,
            'benefited_mess' => Yii::t('HRModule.WorkContract', 'BenefitMess')
        ));
    }
}

