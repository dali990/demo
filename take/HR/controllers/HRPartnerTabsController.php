<?php

class HRPartnerTabsController extends SIMAController 
{
    public function actionEmployeeFilesTab($id)
    {
        $partner = Partner::model()->findByPkWithCheck($id);
        $employee_tag = $partner->person->employee->getTag();
        
        $html = $this->widget(SIMAFileBrowser::class, [
            'location_code' => 'partner_files',
            'default_json' => '{"name":"zaposleni","children":[[{"model":"DocumentType","type":"tag_group","attributes":{"show_unlisted":"true","tag_group_tree_view":"true"},"conditions":"","alias":"Tipovi dokumenta","clas":"group_table","highest_level_num":"1","children":[[{"type_column":"partner_id","type":"file_attribute","model":"Filing","attributes":{"show_unlisted":"true","null_name":""},"conditions":"","alias":"Partner","clas":"group_attribute"}]]}]],"attributes":{"show_unlisted":"false"}}',
            'init_tql' => $employee_tag->query.'!!TAG_PARENT$'.$employee_tag->id,
        ], true);
        
        $this->respondOK([
            'html' => $html
        ]);
    }
    
    public function actionCandidateFilesTab($id)
    {
        $partner = Partner::model()->findByPkWithCheck($id);
        
        $html = $this->widget(SIMAFileBrowser::class, [
            'location_code' => 'candidates',
            'default_json' => '{"name":"kandidati","children":[[{"model":"DocumentType","type":"tag_group","attributes":{"show_unlisted":"true","tag_group_tree_view":"true"},"conditions":"","alias":"Tipovi dokumenta","clas":"group_table","highest_level_num":"1","children":[[{"type_column":"partner_id","type":"file_attribute","model":"Filing","attributes":{"show_unlisted":"true","null_name":""},"conditions":"","alias":"Partner","clas":"group_attribute"}]]}]],"attributes":{"show_unlisted":"false"}}',
            'init_tql' => 'TAG_GROUP$hr.employee_candidates$id$'.$partner->id,
        ], true);
        
        $this->respondOK([
            'html' => $html
        ]);
    }
}
