<?php

class WPCompetitionsController extends SIMAController
{
    
    public function filters()
    {
        return array(
            'ajaxOnly'
        ) + parent::filters();
    }
    
    public function accessRules()
    {
        return array(
            array(
                'allow',
//                'actions' => array('index'),
                'roles' => array('menuShowLegalWorkPositionCompetition')
            ),
//            array(
//                'allow',
//                'actions' => array('index', 'confirm1', 'ATModel', 'ATModelSum', 'billTab', 'tabBill'),
//                'roles' => array('modelBillConfirm1')
//            ),
            array(
                'deny'
            )
        );
    }
    
    public function actionIndex()
    {
        if (Yii::app()->user->checkAccess('HRPositionCompetitionSeeAll') || Yii::app()->user->checkAccess('HRPositionCompetitionAdministration'))
        {
            $fixed_filter = [];
            $add_button = true;
        }
        else
        {
            $fixed_filter = [
                'work_position_competition_juries' => [
                    'jury' => ['ids' => Yii::app()->user->id]
                ]
            ];
            $add_button = false;
        }
        
        $work_position_competitions_filter = $this->filter_post_input('work_position_competitions_filter', false);
        
        if (!empty($work_position_competitions_filter))
        {
            $fixed_filter = array_merge_recursive($fixed_filter, $work_position_competitions_filter);
        }

        $html = $this->renderPartial('index', [
            'uniq' => SIMAHtml::uniqid(),
            'fixed_filter' => $fixed_filter,
            'add_button' => $add_button,
        ], true, false);
        
        $this->respondOK([
            'html' => $html,
            'title' => Yii::t('HRModule.WorkPositionCompetitions','WorkPositionCompetitions')
        ]);
    }
    
    public function actionExpiredWorkPositionCompetitionsWithoutCompetitionGradeForCurrUser()
    {
        $add_button = false;
        if (Yii::app()->user->checkAccess('HRPositionCompetitionSeeAll'))
        {
            $add_button = true;
        }
        
        $curr_user_id = Yii::app()->user->id;
        
        $work_position_competitions_filter = [
            'active' => true,
            'work_position_competition_juries' => [
                'jury' => ['ids' => $curr_user_id]
            ],
            'filter_scopes' => [
                'onlyExpired',
                'notGradedByUser' => $curr_user_id
            ]
        ];
        
        $html = $this->renderPartial('index', [
            'uniq' => SIMAHtml::uniqid(),
            'fixed_filter' => $work_position_competitions_filter,
            'add_button' => $add_button,
        ], true, false);
        
        $this->respondOK([
            'html' => $html,
            'title' => Yii::t('HRModule.WorkPositionCompetitions','WorkPositionCompetitions')
        ]);
    }
    
    public function actionCandidates()
    {
        
        $html = $this->renderPartial('candidates',array(
            'uniq' => SIMAHtml::uniqid()
        ),true,true);
        
        $this->respondOK(array(
            'html' => $html,
            'title' => 'HR kandidati'
        ));
    }
    
    public function actionSchools()
    {
        
        $html = $this->renderPartial('schools',array(
            'uniq' => SIMAHtml::uniqid()
        ),true,true);
        
        $this->respondOK(array(
            'html' => $html,
            'title' => 'Skole i smerovi'
        ));
    }

}
