<?php

class NationalEventsController extends SIMAController 
{
    public function actionIndex($id)
    {
        $model = ModelController::GetModel('NationalEventToDayYear', $id);
        
        $html = $this->renderPartial('index', [
            'model' => $model
        ],true,false);
        
        $this->respondOK([
            'html' => $html
        ]);
    }
    
    public function actionNationalEvents()
    {
        $current_year = date("Y");
        $yearModel = Year::get($current_year);
        
        $unusedAndWithoutNullDatesNationalEvents = $this->getUnusedAndWithoutNullDatesNationalEvents($yearModel);
        
        $html = $this->renderPartial('national_events',array(
            'yearModel' => $yearModel,
            'number_of_unused_national_events' => $unusedAndWithoutNullDatesNationalEvents['number_of_unused_national_events'],
            'number_of_unused_national_events_with_notnulldates' => $unusedAndWithoutNullDatesNationalEvents['number_of_unused_national_events_with_notnulldates']
        ),true,false);
        
        $this->respondOK(array(
            'html' => $html,
            'title' => Yii::t('MainMenu', 'NationalEvents'),
        ));
    }
    
    public function actionGetUnusedAndWithoutNullDatesNationalEvents()
    {
        $unusedAndWithoutNullDatesNationalEvents = $this->getUnusedAndWithoutNullDatesNationalEvents();
        
        $this->respondOK($unusedAndWithoutNullDatesNationalEvents);
    }
    
    private function getUnusedAndWithoutNullDatesNationalEvents(Year $yearModel=null)
    {
        if(is_null($yearModel))
        {
            $current_year = date("Y");
            $yearModel = Year::get($current_year);
        }
        
        $number_of_unused_national_events = NationalEvent::model()->unusedInCurrentYear($yearModel)->count();
        $number_of_unused_national_events_with_notnulldates = NationalEvent::model()->unusedInCurrentYear($yearModel)->withSetDate()->count();
        
        $result = [
            'number_of_unused_national_events' => $number_of_unused_national_events,
            'number_of_unused_national_events_with_notnulldates' => $number_of_unused_national_events_with_notnulldates,
        ];
        
        return $result;
    }
    
    public function actionAutoFillForCurrentYear()
    {
        $current_year = date("Y");
        $yearModel = Year::get($current_year);
        
        $unusedNationalEvents = NationalEvent::model()->unusedInCurrentYear($yearModel)->withSetDate()->findAll();
        
        foreach($unusedNationalEvents as $ne)
        {
            $day_date = $ne->date.$current_year.'.';

            $netdyModel = new NationalEventToDayYear();
            $netdyModel->national_event_id = $ne->id;
            $netdyModel->_day_date = $day_date;
            $netdyModel->save();
        }
        
        $this->respondOK();
    }
    
    public function actionNationalEventToDayAfterSaveRecheck()
    {
        $data = $this->setEventHeader();
        
        $eventSourceSteps = 0;
        $eventSourceStepsSum = 0;
        Yii::app()->controller->sendUpdateEvent($eventSourceStepsSum);

        $national_event_to_day_date = $this->filter_input($data, 'national_event_to_day_date', false);
        $before_save_day_date = $this->filter_input($data, 'before_save_day_date', false);

        /// recheck absences
        $model_filter = [
            'ids' => -1
        ];
        
        if(!empty($before_save_day_date) && !empty($national_event_to_day_date))
        {
            $model_filter = [
                    'OR',
                    ['absent_on_date' => $before_save_day_date],
                    ['absent_on_date' => $national_event_to_day_date]
            ];
        }
        else if(!empty($before_save_day_date))
        {
            $model_filter = ['absent_on_date' => $before_save_day_date];
        }
        else if(!empty($national_event_to_day_date))
        {
            $model_filter = ['absent_on_date' => $national_event_to_day_date];
        }
        $criteria = new SIMADbCriteria([
            'Model' => 'Absence',
            'model_filter' => $model_filter
        ]);
        $absences = Absence::model()->confirmed()->findAll($criteria);
        
        $absences_count = count($absences);
        if($absences_count > 0)
        {
            $eventSourceSteps = (80/$absences_count);
        }
        else
        {
            $eventSourceStepsSum += 80;
            Yii::app()->controller->sendUpdateEvent($eventSourceStepsSum);
        }
        
        $revert_confirmed_absence_on_national_event_add = Yii::app()->configManager->get('HR.revert_confirmed_absence_on_national_event_add');
        
        foreach($absences as $ab)
        {
            set_time_limit(5);

            if($revert_confirmed_absence_on_national_event_add === 'true')
            {
                $ab->confirmed = null;
            }
            $ab->recheckAndResaveWorkDays(false);
            
            $eventSourceStepsSum += $eventSourceSteps;
            Yii::app()->controller->sendUpdateEvent($eventSourceStepsSum);
        }
        
        $message = Yii::t('BaseModule.Common', 'Success');
        $this->sendEndEvent([
            'message' => $message
        ]);
    }
}
