<?php

class EmployeeOfficeEntryLogController extends SIMAController 
{
    public function actionGetNewEntryDoorLogs()
    {
        $this->setEventHeader(); 
        
        if(Yii::app()->entryDoorLog->haveEmployeeCard())
        {
            $cards_count = EntryDoorCard::model()->count();
            if($cards_count === 0)
            {
                throw new SIMAWarnException(Yii::t('HRModule.EntryDoor', 'NoCardsToEnterFor'));
            }
        }
        
        $saved_count = Yii::app()->entryDoorLog->getNewLogs(true);
        
        $this->sendEndEvent([
           'saved_count' => $saved_count
        ]);
    }
    
    public function actionGetNonExistingEntryDoorLogCards()
    {
        $saved_count = Yii::app()->entryDoorLog->getNonExistingCards();
        
        $this->respondOK([
            'saved_count' => $saved_count
        ]);
    }
}

