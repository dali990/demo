<?php

/**
 * Description of RecordSafeController
 *
 * @author goran
 */
class SafeRecordController extends SIMAController
{   
    public static $SAFE_RECORDS = [
        '1' => [
            'name' => 'Evidencija o radnim mestima sa povećanim rizikom',
            'description' => 'Evidencija o radnim mestima sa povećanim rizikom',
            'code' => 'Obrazac 1',
            'Model' => 'SafeEvd1'
        ],
        '2' => [
            'name' => 'Evidencija o zaposlenima raspoređenim na radna mesta sa povećanim rizikom i lekarskim pregledima zaposlenih raspoređenih na ta radna mesta',
            'description' => 'Evidencija o zaposlenima raspoređenim na radna mesta sa povećanim rizikom i lekarskim pregledima zaposlenih raspoređenih na ta radna mesta',
            'code' => 'Obrazac 2',
            'Model' => 'SafeEvd2'
        ],
        '3' => [
            'name' => 'Evidencija o povredama na radu',
            'description' => 'Evidencija o povredama na radu',
            'code' => 'Obrazac 3',
            'Model' => 'SafeEvd3'
        ],
        '4' => [
            'name' => 'Evidencija o profesionalnim oboljenjima',
            'description' => 'Evidencija o profesionalnim oboljenjima',
            'code' => 'Obrazac 4',
            'Model' => 'SafeEvd4'
        ],
        '5' => [
            'name' => 'Evidencija o bolestima u vezi sa radom',
            'description' => 'Evidencija o bolestima u vezi sa radom',
            'code' => 'Obrazac 5',
            'Model' => 'SafeEvd5'
        ],
        '6' => [
            'name' => 'Evidencija o zaposlenima osposobljenim za bezbedan i zdrav rad',
            'description' => 'Evidencija o zaposlenima osposobljenim za bezbedan i zdrav rad',
            'code' => 'Obrazac 6',
            'Model' => 'SafeEvd6'
        ],
        '7' => [
            'name' => 'Evidencija o opasnim materijama koje se koriste u toku rada',
            'description' => 'Evidencija o opasnim materijama koje se koriste u toku rada',
            'code' => 'Obrazac 7',
            'Model' => 'SafeEvd7'
        ],
        '8' => [
            'name' => 'Evidencija o izvršenim ispitivanjima uslova radne okoline',
            'description' => 'Evidencija o izvršenim ispitivanjima uslova radne okoline',
            'code' => 'Obrazac 8',
            'Model' => 'SafeEvd8'
        ],
        '9' => [
            'name' => 'Evidencija o izvršenim pregledima i proverama opreme za rad',
            'description' => 'Evidencija o izvršenim pregledima i proverama opreme za rad',
            'code' => 'Obrazac 9',
            'Model' => 'SafeEvd9'
        ],
        '10' => [
            'name' => 'Evidencija o izdatim sredstvima i opremi za ličnu zaštitu na radu',
            'description' => 'Evidencija o izdatim sredstvima i opremi za ličnu zaštitu na radu',
            'code' => 'Obrazac 10',
            'Model' => 'SafeEvd10'
        ],
        '11' => [
            'name' => 'Evidencija o prijavi smrtnih, kolektivnih i teških povreda na radu, kao i povreda zbog kojih zaposleni nije sposoban za rad više od tri uzastopna radna dana',
            'description' => 'Evidencija o prijavi smrtnih, kolektivnih i teških povreda na radu, kao i povreda zbog kojih zaposleni nije sposoban za rad više od tri uzastopna radna dana',
            'code' => 'Obrazac 11',
            'Model' => 'SafeEvd11'
        ],
        '12' => [
            'name' => 'Evidencija o prijavama profesionalnih oboljenja',
            'description' => 'Evidencija o prijavama profesionalnih oboljenja',
            'code' => 'Obrazac 12',
            'Model' => 'SafeEvd12'
        ],
        '13' => [
            'name' => 'Evidencija o izvršenim lekarskim pregledima zaposlenih u skladu sa propisima o bezbednosti i zdravlju na radu',
            'description' => 'Evidencija o izvršenim lekarskim pregledima zaposlenih u skladu sa propisima o bezbednosti i zdravlju na radu',
            'code' => 'Obrazac 13',
            'Model' => 'SafeEvd13'
        ],
        '14' => [
            'name' => 'Evidencija o prijavama opasnih pojava koje bi mogle da ugroze bezbednost i zdravlje zaposlenih',
            'description' => 'Evidencija o prijavama opasnih pojava koje bi mogle da ugroze bezbednost i zdravlje zaposlenih',
            'code' => 'Obrazac 14',
            'Model' => 'SafeEvd14'
        ],
//        '15' => [
//            'name' => 'Evidencija ciljanih pregleda',
//            'description' => 'Evidencija ciljanih pregleda',
//            'code' => 'Obrazac 15',
//            'Model' => 'SafeEvd15'
//        ]
    ];   

    public function filters()
    {
        return array(
            'ajaxOnly - safeRecordDocument - safeEvd1Document - safeEvd9Document - safeEvd2Document - safeEvd13Document'
            . ' - getModeInjuryDiffBetweenLocalAndCodebook - getSourceInjuryDiffBetweenLocalAndCodebook - saveDiffBetweenLocalAndCodebook'
                ) + parent::filters();
    }

    public function actionIndex()
    {
        $uniq = SIMAHtml::uniqid();
        
        $subactions_data = [
            [
                'title'=>'Šifre opasnosti',
                'onclick'=>["sima.dialog.openActionInDialog", 'HR/safeRecord/safeWorkDanger']
            ],
            [
                'title'=>'Zahtevane zdravstvene sposobnosti',
                'onclick'=>["sima.dialog.openActionInDialog", 'HR/safeRecord/requiredMedicalAbility']
            ],
            [
                'title'=>Yii::t('HRModule.SourceInjury', 'SourceInjuries'),
                'onclick'=>["sima.dialog.openActionInDialog", 'guitable', [
                    'get_params' => [
                        'settings'=>array(
                            'model' => 'SourceInjury',
                            'add_button' => true,
                            'custom_buttons' => [
                                Yii::t('BaseModule.Misc','TakeDataFromCodeBook') => [
                                    'func' => 'sima.safeRecord.syncSourceInjuriesFromCodebook(this)',
                                ],
                            ],
                        )
                    ]
                ]]
            ],
            [
                'title'=>Yii::t('HRModule.ModeInjury', 'ModeInjuries'),
                'onclick'=>["sima.dialog.openActionInDialog", 'guitable', [
                    'get_params' => [
                        'settings'=>array(
                            'model' => 'ModeInjury',
                            'add_button' => true,
                            'custom_buttons' => [
                                Yii::t('BaseModule.Misc','TakeDataFromCodeBook') => [
                                    'func' => 'sima.safeRecord.syncModeInjuriesFromCodebook(this)',
                                ],
                            ],
                        )
                    ]
                ]]
            ],
            [
                'title'=>'Dijagnoza i medjunarnodna sifra profesionalnog oboljenja',
                'onclick'=>["sima.dialog.openActionInDialog", 'guitable', [
                    'get_params' => [
                        'settings'=>array(
                            'model' => 'InternationalDiagnosisOfProfessionalDiseases',
                            'add_button' => true,
                            'custom_buttons' => [
                                Yii::t('BaseModule.Misc','TakeDataFromCodeBook') => [
                                    'func' => 'sima.adminMisc.syncInternationalDiagnosisOfPDFromCodebook(this)',
                                ],
                            ],
                        )
                    ]
                ]]
            ],
            [
                'title'=>'Dijagnoza i medjunarnodna sifra bolesti u vezi sa radom',
                'onclick'=>["sima.dialog.openActionInDialog", 'guitable', [
                    'get_params' => [
                        'settings'=>array(
                            'model' => 'InternationalDiagnosisOfWorkDiseases',
                            'add_button' => true,
                            'custom_buttons' => [
                                Yii::t('BaseModule.Misc','TakeDataFromCodeBook') => [
                                    'func' => 'sima.adminMisc.syncInternationalDiagnosisOfWDFromCodebook(this)',
                                ],
                            ],
                        )
                    ]
                ]]
            ],
            [
                'title'=>'Razlozi za izvršenje osposobljavanja',
                'onclick'=>["sima.dialog.openActionInDialog", 'guitable', [
                    'get_params' => [
                        'settings'=>array(
                            'model' => 'ReasonForEnforcementTraining',
                            'add_button' => true,
                            'custom_buttons' => [
                                Yii::t('BaseModule.Misc','TakeDataFromCodeBook') => [
                                    'func' => 'sima.adminMisc.syncReasonForEnforcementTrainingFromCodebook(this)',
                                ],
                            ],
                        )
                    ]
                ]]
            ]                                
        ];
        
        $settings_button = '';
        if(SIMAMisc::isVueComponentEnabled())
        {
            $settings_button = Yii::app()->controller->widget(SIMAButtonVue::class,[        
                'icon' => '_main-settings',
                'iconsize' => 18,
                'subactions_data' => $subactions_data
            ], true);
        }
        else
        {
            $settings_button = Yii::app()->controller->widget('base.extensions.SIMAButton.SIMAButton',[        
                'action'=>[
                    'icon'=>'sima-icon _main-settings _18',
                    'subactions' => $subactions_data

                ]
            ], true);
        }
        
        $html = Yii::app()->controller->renderContentLayout([
            'name'=>'Evidencije bezbednosti',            
            'options_class'=>'sima-safe-records-options',
            'options'=>[
                [
                    'class'=>'sima-safe-records-option _settings',
                    'html' => $settings_button
                ]
            ]
        ], [
            [
                'proportion'=>0.3,
                'min_width'=>200,  
                'html'=>$this->renderPartial('safeRecordsIndex/left_side', [
                    'uniq'=>$uniq,
                    'safe_records' => SafeRecordController::$SAFE_RECORDS
                ], true, false),
                'class'=>'sima-safe-records-list'
            ],
            [
                'proportion'=>0.7,
                'min_width'=>600,
                'html'=>$this->renderPartial('safeRecordsIndex/right_side', [
                    'uniq'=>$uniq
                ], true, false),
                'class'=>'sima-safe-record-content'
            ]
        ], [
            'id'=>$uniq,
            'class'=>'sima-safe-records'
        ]);
        
        $this->respondOK([
            'title' => 'Evidencije bezbednosti',
            'html' => $html
        ]);
    }

    public function actionTabSafeEvd($id)
    {
        $partner = Partner::model()->findByPk($id);
        if ($partner == null)
            throw new Exception('Partner ne postoji');
        $html = $this->renderPartial('partner_tabs/safeEvd', array('model' => $partner), true, true);
        
        $this->respondOK([
            'html' => $html
        ]);
    }

    public function actionSafeRecordDocument($Model)
    {
        $model = $Model::model();
        
        $model_filter = [];       
        if (isset($_POST['params']['model_filter']))
        {
            $model_filter = $_POST['params']['model_filter'];
        }        
        $scopes = $model->scopes();
        if (isset($scopes['forReportDocument']))
            $model = $model->forReportDocument();
        if (isset($scopes['byName']))
            $model = $model->byName();
        if (isset($_GET['work_position_id']))
        {
            $work_position = WorkPosition::model()->findByPk($_GET['work_position_id']);
//            $safe_evd=$Model::model()->findAllByAttributes(array('work_position_id'=>$_GET['work_position_id']));
            $model_filter['work_position']['ids'] = $_GET['work_position_id'];
            $safe_evd = $model->findAll(new SIMADbCriteria([
                'Model'=>$Model,
                'model_filter'=>$model_filter
            ]));
            $params = array('rows' => $safe_evd, 'work_position' => $work_position->DisplayName);
        }
        else if (isset($_GET['employee_id']))
        {
            $person = Person::model()->findByPk($_GET['employee_id']);
//            $safe_evd=$Model::model()->findByAttributes(array('person_id'=>$_GET['employee_id']));
            $model_filter['person']['ids'] = $_GET['employee_id'];
            $safe_evd = $model->findAll(new SIMADbCriteria([
                'Model'=>$Model,
                'model_filter'=>$model_filter
            ]));            
            $params = array('rows' => $safe_evd, 'employee' => $person->DisplayName);
        }
        else
        {
//            $safe_evd = $Model::model()->findAll();
            $safe_evd = $model->findAll(new SIMADbCriteria([
                'Model'=>$Model,
                'model_filter'=>$model_filter
            ]));
            $params = array('rows' => $safe_evd);
        }

        $html = $this->renderPDF($Model, $params, array());

        $this->respondOK([
            'html' => $html
        ]);
    }

    public function actionSafeEvd7Document()
    {

        $html = $this->renderPartial('safeEvd7_view', array(), true, true);

        $this->respondOK([
            'html' => $html
        ]);
    }

    public function actionSafeEvd6Document()
    {
        $html = $this->renderPDF($Model, $params, array());

        $this->respondOK([
            'html' => $html
        ]);
    }
    
    public function actionSafeEvd1Document()
    {
        $work_positions = WorkPosition::model()->findAllByAttributes([
            'enlarged_risk'=>true
        ]);        
        $html = $this->renderPDF('SafeEvd1', [
            'rows'=>$work_positions
        ], []);

        $this->respondOK([
            'html' => $html
        ]);
    }

    public function actionSafeEvd9Document()
    {
        $fixed_assets = FixedAsset::model()->findAll(new SIMADbCriteria([
            'Model'=>'FixedAsset',
            'model_filter'=>[
                'filter_scopes'=>'hasSafeEvd9'
            ]
        ]));
        
        $html = $this->renderPDF('SafeEvd9', [
            'rows'=>$fixed_assets
        ], []);

        $this->respondOK([
            'html' => $html
        ]);
    }
    
    public function actionSafeEvd2Document()
    {
        $persons = Person::model()->findAll([
            'model_filter' => [
                'safe_evd_2_records' => []
            ]
        ]);
        
        $html = $this->renderPDF('SafeEvd2', [
            'persons' => $persons
        ], []);

        $this->respondOK([
            'html' => $html
        ]);
    }
    
    public function actionSafeEvd13Document()
    {
        $persons = Person::model()->findAll([
            'model_filter' => [
                'safe_evd_13_records' => []
            ]
        ]);
        
        $html = $this->renderPDF('SafeEvd13', [
            'persons' => $persons
        ], []);

        $this->respondOK([
            'html' => $html
        ]);
    }
    
    public static function getSafeEvd2WorkPositions($safe_evd_2_records)
    {
        $work_positions = [];
        foreach ($safe_evd_2_records as $safe_evd_2_record) 
        {
            $work_position_id = $safe_evd_2_record->work_position_id;
            if (isset($work_positions[$work_position_id]))
            {
                $work_positions[$work_position_id]['cnt'] = $work_positions[$work_position_id]['cnt'] + 1;
                array_push($work_positions[$work_position_id]['records'], $safe_evd_2_record);                
            }
            else
            {
                $work_positions[$work_position_id] = [                    
                    'cnt' => 1,
                    'records'=>[$safe_evd_2_record]
                ];                
            }
        }
        
        return $work_positions;
    }
    
    public static function getSafeEvd13WorkPositions($safe_evd_13_records)
    {
        $work_positions = [];
        foreach ($safe_evd_13_records as $safe_evd_13_record) 
        {
            $work_position_id = $safe_evd_13_record->work_position_id;
            if (isset($work_positions[$work_position_id]))
            {
                $work_positions[$work_position_id]['cnt'] = $work_positions[$work_position_id]['cnt'] + 1;
                array_push($work_positions[$work_position_id]['records'], $safe_evd_13_record);                
            }
            else
            {
                $work_positions[$work_position_id] = [                    
                    'cnt' => 1,
                    'records'=>[$safe_evd_13_record]
                ];                
            }
        }
        
        return $work_positions;
    }
    
    function renderPDF($Model, $params, $pdf_params = array())
    {
        $this->beginClip('pdf_safe_evd_css');
        if (isset($pdf_params['orientation']) && $pdf_params['orientation'] == 'Landscape')
            $this->renderPartial('pdf_safe_evd_css_table');
        else
            $this->renderPartial('pdf_safe_evd_css_A4_portrait');
        $this->endClip();

        if (isset($_GET['type']) && $_GET['type'] == 'pdf')
        {
            $this->layout = 'safe_evd';
            $html = $this->render('safeEvdTemplates/pdf_' . $Model, $params, true);
            $pdf = new SIMATCPdf('P', 'mm', 'A4', true, 'UTF-8', false);
            $pdf->SetCreator(PDF_CREATOR);
            $pdf->SetFont('freesans', '', 12);            
            $pdf->setHtmlFooter('', true);                
            $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
            $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
            $pdf->getAliasNbPages();
            $pdf->AddPage();
            $pdf->writeHTML($html, true, false, true, false, 'C');
            $pdf->Output('Izveštaj.pdf', 'D');
        }
        else
        {
            $this->layout = 'safe_evd_preview';
            return $this->render('safeEvdTemplates/pdf_' . $Model, $params, true);
        }
    }

    public function actionSafeRecordTabs($id)
    {
        $safe_record = SafeRecordController::$SAFE_RECORDS[$id];
        $Model = $safe_record['Model'];
        
        $safe_record_tabs = [];
        if ($Model === 'SafeEvd1')
        {
            $safe_record_tabs = [
                [
                    'title' => 'Izveštaj',
                    'code' => 'safe_evd1',
                    'action' => 'HR/safeRecord/safeEvd1Document',
                    'get_params' => []
                ]
            ];
        }
        else if ($Model === 'SafeEvd2')
        {
            $file_init_data = [
                'responsible_id' => Yii::app()->user->id
            ];
            $safe_evd_2_document_type_id = Yii::app()->configManager->get('HR.safe_evd_2', false);
            if (!empty($safe_evd_2_document_type_id))
            {
                $file_init_data['document_type_id'] = [
                    'disabled' => true,
                    'init' => $safe_evd_2_document_type_id
                ];
            }
            $add_button = [
                'init_data' => [
                    'File' => $file_init_data
                ]
            ];
            $safe_record_tabs = [
                $this->getSafeEvdFormTab($Model, $add_button, [], ['display_scopes'=>'byTime']),
                [
                    'title' => 'Uputi za lekarski pregled',
                    'code' => 'medical_examination_referrals',
                    'action' => 'HR/safeRecord/medicalExaminationReferrals',
                    'get_params' => []
                ],
                [
                    'title' => 'Izveštaj',
                    'code' => 'safe_evd2',
                    'action' => 'HR/safeRecord/safeEvd2Document',
                    'get_params' => []
                ]                
            ];
        }
        else if ($Model === 'SafeEvd3')
        {
            $file_init_data = [
                'responsible_id' => Yii::app()->user->id
            ];
            $safe_evd_3_document_type_id = Yii::app()->configManager->get('HR.safe_evd3_document_type_id', false);
            if (!empty($safe_evd_3_document_type_id))
            {
                $file_init_data['document_type_id'] = [
                    'disabled' => true,
                    'init' => $safe_evd_3_document_type_id
                ];
            }
            $add_button_params = [
                'init_data' => [
                    'File' => $file_init_data
                ]
            ];
            $safe_record_tabs = [
                $this->getSafeEvdFormTab($Model, $add_button_params),
                $this->getSafeEvdReportTab($Model)
            ];
        }
        else if ($Model === 'SafeEvd4')
        {
            $safe_record_tabs = [
                $this->getSafeEvdFormTab($Model, true),
                $this->getSafeEvdReportTab($Model)
            ];
        }
        else if ($Model === 'SafeEvd5')
        {
            $safe_record_tabs = [
                $this->getSafeEvdFormTab($Model, true),
                $this->getSafeEvdReportTab($Model)
            ];
        }
        else if ($Model === 'SafeEvd6')
        {
            $safe_record_tabs = [
                $this->getSafeEvdFormTab($Model, true),
            ];
        }
        else if ($Model === 'SafeEvd7')
        {
            $safe_record_tabs = [
                $this->getSafeEvdFormTab($Model, true),
                [
                    'title' => 'Izveštaj',
                    'code' => 'safe_evd7',
                    'action' => 'HR/safeRecord/safeEvd7Document',
                    'get_params' => []
                ]
            ];
        }
        else if ($Model === 'SafeEvd8')
        {
            $safe_record_tabs = [
                $this->getSafeEvdFormTab($Model, true),
                $this->getSafeEvdReportTab($Model)
            ];
        }
        else if ($Model === 'SafeEvd9')
        {
            $safe_record_tabs = [
                $this->getSafeEvdFormTab($Model, true),
                [
                    'title' => 'Izveštaj',
                    'code' => 'safe_evd9',
                    'action' => 'HR/safeRecord/safeEvd9Document',
                    'get_params' => []
                ]
            ];
        }
        else if ($Model === 'SafeEvd10')
        {
            $safe_record_tabs = [
                $this->getSafeEvdFormTab($Model, true),
                $this->getSafeEvdReportTab($Model)
            ];
        }
        else if ($Model === 'SafeEvd11')
        {
            $safe_record_tabs = [
                $this->getSafeEvdFormTab($Model, true),
                $this->getSafeEvdReportTab($Model)
            ];
        }
        else if ($Model === 'SafeEvd12')
        {
            $safe_record_tabs = [
                $this->getSafeEvdFormTab($Model, true),
                $this->getSafeEvdReportTab($Model)
            ];
        }
        else if ($Model === 'SafeEvd13')
        {
            $file_init_data = [
                'responsible_id' => Yii::app()->user->id
            ];
            $safe_evd_2_document_type_id = Yii::app()->configManager->get('HR.safe_evd_2', false);
            if (!empty($safe_evd_2_document_type_id))
            {
                $file_init_data['document_type_id'] = [
                    'disabled' => true,
                    'init' => $safe_evd_2_document_type_id
                ];
            }

            $add_button = [
                'init_data' => [
                    'File' => $file_init_data
                ]
            ];
            $safe_record_tabs = [
                $this->getSafeEvdFormTab($Model, $add_button, [], ['display_scopes'=>'byName']),
                [
                    'title' => 'Uputi za lekarski pregled',
                    'code' => 'safe_evd_13_medical_examination_referrals',
                    'action' => 'HR/safeRecord/safeEvd13MedicalExaminationReferrals',
                    'get_params' => []
                ],
                [
                    'title' => 'Izveštaj',
                    'code' => 'safe_evd13',
                    'action' => 'HR/safeRecord/safeEvd13Document',
                    'get_params' => []
                ]                
            ];
        }
        else if ($Model === 'SafeEvd14')
        {
            $safe_record_tabs = [
                $this->getSafeEvdFormTab($Model, true),
                $this->getSafeEvdReportTab($Model)
            ];
        }
        else if ($Model === 'SafeEvd15')
        {
            $safe_record_tabs = [
                $this->getSafeEvdFormTab($Model, true)                
            ];
        }       
        
        $this->respondOK([
            'tabs' => $safe_record_tabs
        ]);
    }

    private function getSafeEvdFormTab($Model, $add_button, $fixed_filter=[], $select_filter=[], $title='Evidencije-Obrazac')
    {
        return [
            'title' => $title,
            'code' => 'safe_evd_form',
            'action' => 'guitable',
            'get_params' => array(
                'settings' => array(
                    'model' => $Model,
                    'add_button' => $add_button,
                    'fixed_filter' => $fixed_filter,
                    'select_filter' => $select_filter
                )
            )
        ];
    }
    
    private function getSafeEvdReportTab($Model, $model_filter=[])
    {
        return [
            'title' => 'Izveštaj',
            'code' => 'safe_evd',
            'action' => 'HR/safeRecord/safeRecordDocument',
            'get_params' => [
                'Model' => $Model                
            ],
            'params' => [
                'model_filter' => $model_filter                
            ]
        ];
    }
    
//    public function actionGenerateC6Document($id)
//    {
//        if (!isset($id))
//        {
//            throw new Exception('actionGenerateC6Document - nisu lepo zadati parametri');
//        }
//
//        $safe_evd6 = SafeEvd6::model()->findByPk($id);
//        $template_id = Yii::app()->configManager->get('HR.safe_evd_6_template', true);
//        $templated_file = TemplatedFile::model()->with('templated_file_data')->findByPk($id);
//        if ($templated_file === null)
//        {
//            $template_file_id = TemplateController::setTemplate($id, $template_id);
//        }
//        else
//        {
//            $template_file_id = $templated_file->id;
//        }
//        TemplateController::addVersion($template_file_id);
//
//        $this->respondOK([], [$safe_evd6]);
//    }

    public function actionAddWorkPositionRisks($work_position_id)
    {
        if (!isset($work_position_id))
            throw new Exception('actionAddWorkPositionRisks - nisu lepo zadati parametri');

        $work_position_risks = isset($_POST['work_position_risks']) ? $_POST['work_position_risks'] : [];
        $work_position = WorkPosition::model()->findByPk($work_position_id);
        $new_work_position_risks_ids = [];
        foreach ($work_position_risks as $work_position_risk)
        {
            $safe_evd_1 = SafeEvd1::model()->findByAttributes(array('work_position_id' => $work_position_id, 'work_danger_id' => $work_position_risk['id']));
            if ($safe_evd_1 === null)
            {
                $safe_evd_1 = new SafeEvd1();
                $safe_evd_1->work_position_id = $work_position_id;
                $safe_evd_1->work_danger_id = $work_position_risk['id'];
                $safe_evd_1->save();
            }
            array_push($new_work_position_risks_ids, $work_position_risk['id']);
        }

        $old_work_position_risks = SafeEvd1::model()->findAllByAttributes(array('work_position_id' => $work_position_id));
        foreach ($old_work_position_risks as $old_work_position_risk)
        {
            if (!in_array($old_work_position_risk->work_danger_id, $new_work_position_risks_ids))
            {
                $old_work_position_risk->delete();
            }
        }
        $this->respondOK([], $work_position);
    }

    public function actionGenerateMedicalReferral($safe_evd2_id, $file_id=null)
    {
        $template_id = Yii::app()->configManager->get('HR.template_medical_referral_id', false);
        if (empty($template_id))
        {
            throw new SIMAWarnException('Nije postavljen template za lekarski uput');
        }
        if (!isset($safe_evd2_id))
        {
            throw new Exception('actionGenerateMedicalReferral - nisu lepo zadati parametri.');
        }

        $safe_evd_2 = SafeEvd2::model()->findByPk($safe_evd2_id);        
        if (!isset($safe_evd_2->medical_referral))
        {            
            if (is_null($file_id))
            {
                $medical_examination_referral = new MedicalExaminationReferral();
                $medical_examination_referral->person_id = $safe_evd_2->person_id;
                $medical_examination_referral->work_position_id = $safe_evd_2->work_position_id;
                $medical_examination_referral->save();
                $medical_examination_referral->refresh();
                $file_id = $medical_examination_referral->id;
            }
            
            $safe_evd_2->medical_referral_id = $file_id;            
            $safe_evd_2->save();
        }
        else
        {            
            $file_id = $safe_evd_2->medical_referral_id;
        }
        
        $templated_file = TemplatedFile::model()->with('templated_file_data')->findByPk($file_id);
        if ($templated_file === null)
        {
            $template_file_id = TemplateController::setTemplate($file_id, $template_id);
        }
        else
        {
            $template_file_id = $templated_file->id;
        }
        TemplateController::addVersion($template_file_id);

        $this->respondOK([], [$safe_evd_2]);
    }
    
    public function actionGenerateMedicalReferralForSafeEvd13($safe_evd13_id, $file_id=null)
    {
        $template_id = Yii::app()->configManager->get('HR.template_medical_referral_id', false);
        if (empty($template_id))
        {
            throw new SIMAWarnException('Nije postavljen template za lekarski uput');
        }
        if (!isset($safe_evd13_id))
        {
            throw new Exception('actionGenerateMedicalReferralForSafeEvd13 - nisu lepo zadati parametri.');
        }

        $safe_evd_13 = SafeEvd13::model()->findByPk($safe_evd13_id);        
        if (!isset($safe_evd_13->medical_referral))
        {            
            if (is_null($file_id))
            {
                $medical_examination_referral = new MedicalExaminationReferral();
                $medical_examination_referral->person_id = $safe_evd_13->person_id;
                $medical_examination_referral->work_position_id = $safe_evd_13->work_position_id;
                $medical_examination_referral->save();
                $medical_examination_referral->refresh();
                $file_id = $medical_examination_referral->id;
            }
            
            $safe_evd_13->medical_referral_id = $file_id;            
            $safe_evd_13->save();
        }
        else
        {            
            $file_id = $safe_evd_13->medical_referral_id;
        }
        
        $templated_file = TemplatedFile::model()->with('templated_file_data')->findByPk($file_id);
        if ($templated_file === null)
        {
            $template_file_id = TemplateController::setTemplate($file_id, $template_id);
        }
        else
        {
            $template_file_id = $templated_file->id;
        }
        TemplateController::addVersion($template_file_id);

        $this->respondOK([], [$safe_evd_13]);
    }
    
    public function actionAddWorkPositionMedicalAbilities($work_position_id)
    {
        if (!isset($work_position_id))
            throw new Exception('actionAddWorkPositionMedicalAbilities - nisu lepo zadati parametri');

        $work_position_medical_abilities = isset($_POST['work_position_medical_abilities']) ? $_POST['work_position_medical_abilities'] : [];
        $work_position = WorkPosition::model()->findByPk($work_position_id);
        $new_work_position_medical_abilities_ids = [];
        foreach ($work_position_medical_abilities as $work_position_medical_ability)
        {
            $rmatwp = RequiredMedicalAbilityToWorkPosition::model()->findByAttributes(array('work_position_id' => $work_position_id, 'required_medical_ability_id' => $work_position_medical_ability['id']));
            if ($rmatwp === null)
            {                
                $rmatwp = new RequiredMedicalAbilityToWorkPosition();
                $rmatwp->work_position_id = $work_position_id;
                $rmatwp->required_medical_ability_id = $work_position_medical_ability['id'];
                $rmatwp->save();
            }
            array_push($new_work_position_medical_abilities_ids, $work_position_medical_ability['id']);
        }

        $old_work_position_medical_abilities = RequiredMedicalAbilityToWorkPosition::model()->findAllByAttributes(array('work_position_id' => $work_position_id));
        foreach ($old_work_position_medical_abilities as $old_work_position_medical_ability)
        {
            if (!in_array($old_work_position_medical_ability->required_medical_ability_id, $new_work_position_medical_abilities_ids))
            {
                $old_work_position_medical_ability->delete();
            }
        }
        $this->respondOK([], $work_position);
    }
    
    public function actionGenerateSafeEvd6Report($id)
    {
        $template_id = Yii::app()->configManager->get('HR.template_safe_evd6_report_id', false);
        if (empty($template_id))
        {
            throw new SIMAWarnException('Niste postavili tempalte za generisanje izvestaja');
        }
        if (!isset($id))
            throw new Exception('actionGenerateSafeEvd6Report - nisu lepo zadati parametri.');

        $safe_evd_6 = SafeEvd6::model()->findByPk($id);
                
        
        $templated_file = TemplatedFile::model()->with('templated_file_data')->findByPk($safe_evd_6->id);
        if ($templated_file === null)
        {
            $template_file_id = TemplateController::setTemplate($safe_evd_6->id, $template_id);
        }
        else
        {
            $template_file_id = $templated_file->id;
        }
        TemplateController::addVersion($template_file_id);

        $this->respondOK([], [$safe_evd_6]);
    }
    
    public function actionSafeWorkDanger()
    {
        $html = $this->renderPartial('safe_work_danger', [], true, false);
        
        $this->respondOK([
            'html'=>$html
        ]);
    }
    
    public function actionRequiredMedicalAbility()
    {
        $html = $this->renderPartial('required_medical_ability', [], true, false);

        $this->respondOK([
            'html'=>$html
        ]);
    }
    
    public function actionMedicalExaminationReferrals()
    {
        $html = $this->renderPartial('medical_examination_referrals', [], true, false);

        $this->respondOK([
            'html'=>$html
        ]);
    }
    
    public function actionSafeEvd13MedicalExaminationReferrals()
    {
        $html = $this->renderPartial('safe_evd_13_medical_examination_referrals', [], true, false);

        $this->respondOK([
            'html'=>$html
        ]);
    }
    
    public function actionGetPersonWorkPositionInSystemCompany($person_id)
    {
        $person = Person::model()->findByPkWithCheck($person_id);

        $respond_params = [];
        if (!empty($person->employee->active_work_contract))
        {
            $respond_params = [
                'id' => $person->employee->active_work_contract->work_position->id,
                'display_name' => $person->employee->active_work_contract->work_position->DisplayName
            ];
        }
        
        $this->respondOK($respond_params);
    }
    public function actionGetModeInjuryDiffBetweenLocalAndCodebook() 
    {
        $this->getEntityDiffBetweenLocalAndCodebook(ModeInjury::class, 'getModeInjuries');
    }
    
    public function actionGetSourceInjuryDiffBetweenLocalAndCodebook() 
    {
        $this->getEntityDiffBetweenLocalAndCodebook(SourceInjury::class, 'getSourceInjuries');
    }
    
    private function getEntityDiffBetweenLocalAndCodebook($model_name, $codebook_path) 
    {
        $this->setEventHeader();
        
        $codebook_items = AdminController::getDataFromCodebook(Yii::app()->params['codebook_base'].AdminController::$codebook_const_path_part.$codebook_path);
        $converted_codebook_items = AdminController::convertCodebookValuesIntoAppropriateLanguage($codebook_items);
        
        $name_column_display = Yii::t('HRModule.'.$model_name, 'Meaning');
        $code_column_display = Yii::t('HRModule.'.$model_name, 'Code');
        $parent_column_display = Yii::t('HRModule.'.$model_name, 'Type');
                       
        $codebook_unique_fields = [];
        $items = [];
        $i = 1;
        
        $codebook_items_cnt = count($converted_codebook_items);
        foreach ($converted_codebook_items as $codebook_item)
        {
            $percent = round(($i/$codebook_items_cnt)*70, 0, PHP_ROUND_HALF_DOWN);
            $this->sendUpdateEvent($percent);
            
            $local_model = $model_name::model()->findByAttributes(['code' => $codebook_item['code']]);
            
            $local_model_parent_attributes = '';
            $local_model_parent_name = '';
            $local_model_parent_code = '';
                       
            if(!empty($codebook_item["parent"]))
            {
                $local_model_parent = $model_name::model()->findByAttributes(['code' => $codebook_item['parent']['code']]);
                if(!empty($local_model_parent))
                {
                    $local_model_parent_attributes = $local_model_parent->id;
                }
                else
                {
                    $local_model_parent_attributes = $codebook_item['parent'];
                }
            }
                        
            if (empty($local_model))
            {
                $items[] = [
                    'status' => 'ADD',
                    'columns' => [
                        $name_column_display => ['new_value' => $codebook_item["name"]],
                        $code_column_display => ['new_value' => $codebook_item["code"]],
                        $parent_column_display => ['new_value' => !empty($codebook_item["parent"]) ? $codebook_item["parent"]["name"]:'']
                    ],
                    'data' => [
                        'status' => 'ADD',
                        'model' => $model_name,
                        'attributes' => [
                            'name' => $codebook_item['name'],
                            'code' => $codebook_item['code'],
                            'parent' => $local_model_parent_attributes
                        ]
                    ]
                ];  
            }
            else
            {
                if(!empty($local_model->parent))
                {
                    $local_model_parent_code = $local_model->parent->code;
                    $local_model_parent_name = $local_model->parent->name;
                }
                
                if (
                        $codebook_item['name'] !== $local_model->name || 
                        $codebook_item['code'] !== $local_model->code || 
                        (
                            !empty($codebook_item['parent']) &&
                            $codebook_item['parent']['code'] !== $local_model_parent_code   
                        )
                    )
                {                   
                    $items[] = [
                        'status' => 'MODIFY',
                        'columns' => [
                            $name_column_display => [
                                'old_value' => $local_model->name,
                                'new_value' => $codebook_item['name']
                            ],
                            $code_column_display => [
                                'old_value' => $local_model->code,
                                'new_value' => $codebook_item['code']
                            ],
                            $parent_column_display => [
                                'old_value' => $local_model_parent_name,
                                'new_value' => !empty($codebook_item["parent"]) ? $codebook_item["parent"]["name"]:''
                            ]
                        ],
                        'data' => [
                            'status' => 'MODIFY',
                            'model' => $model_name,
                            'model_id' => $local_model->id,
                            'attributes' => [
                                'name' => $codebook_item['name'],
                                'code' => $codebook_item['code'],
                                'parent' => $local_model_parent_attributes,
                            ]
                        ]
                    ]; 
                }                                 
                $codebook_unique_fields[] = $codebook_item["code"];
            }
            $i++;
        }       
                
        $local_models = $model_name::model()->findAll(new SIMADbCriteria([
            'Model' => $model_name,
            'model_filter' => [
                'filter_scopes' => [
                    'withoutUniqueFields' => [$codebook_unique_fields]
                ]
            ]
        ]));        
                
        $j = 1;
        $local_models_cnt = count($local_models);
        foreach ($local_models as $local_model)
        {          
            $percent = 70 + round(($j/$local_models_cnt)*30, 0, PHP_ROUND_HALF_DOWN);
            $this->sendUpdateEvent($percent);
            
            $local_model_parent_name = '';
            if(!empty($local_model->parent))
            {
                $local_model_parent_name = $local_model->parent->name;
            }
            $items[] = [
                'status' => 'REMOVE',
                'columns' => [
                    $name_column_display => ['old_value' => $local_model->name],
                    $code_column_display => ['old_value' => $local_model->code],
                    $parent_column_display => ['old_value' => $local_model_parent_name]
                ],
                'data' => [
                    'status' => 'REMOVE',
                    'model' => $model_name,
                    'model_id' => $local_model->id,
                ]
            ];
            $j++;
        }
        
        $html = '';      
        if (!empty($items))
        {
            $html = $this->widget('SIMADiffViewer', [                   
                'diffs' => [
                    [
                        'type' => 'group',
                        'group_id' => SIMAHtml::uniqid(),
                        'group_name' => $model_name::model()->modelLabel(true),
                        'columns' => [
                            $name_column_display,
                            $code_column_display,
                            $parent_column_display
                        ],
                        'items' => $items
                    ]
                ]
            ], true);
        }
        
        $this->sendEndEvent([
            'html' => $html
        ]);
    }
    
    public function actionSaveDiffBetweenLocalAndCodebook()
    {
        $data = $this->setEventHeader();
        $diffs = $data['client_data'];
        
        if (!empty($diffs) && is_array($diffs))
        {
            foreach ($diffs as $diff)
            {
                if (!empty($diff['status']))
                {
                    $status = $diff['status'];
                    if ($status === 'ADD')
                    {
                        if (!empty($diff['model']) && !empty($diff['attributes']))
                        {
                            $model = $diff['model']::model()->findByAttributes([
                                'code' => $diff['attributes']['code']
                            ]);
                            if (empty($model))
                            {
                                $model = new $diff['model']();
                            }
                            $model->name = $diff['attributes']['name'];
                            $model->code = $diff['attributes']['code'];
                            if (!empty($diff['attributes']['parent']))
                            {
                                $model->parent_id = $this->getItemCodeParentIdFromParentAttributes($diff['model'], $diff['attributes']['parent']);
                            }
                            $model->save();
                        }
                    }
                    else if ($status === 'MODIFY')
                    {
                        if (!empty($diff['model']) && !empty($diff['model_id']) && !empty($diff['attributes']))
                        {
                            $model = $diff['model']::model()->findByPk($diff['model_id']);
                            if (!is_null($model))
                            {
                                $model->name = $diff['attributes']['name'];
                                $model->code = $diff['attributes']['code'];
                                if (!empty($diff['attributes']['parent']))
                                {
                                    $model->parent_id = $this->getItemCodeParentIdFromParentAttributes($diff['model'], $diff['attributes']['parent']);
                                }
                                $model->save();
                            }
                        }
                    }
                    if ($status === 'REMOVE')
                    {
                        if (!empty($diff['model']) && !empty($diff['model_id']))
                        {
                            $model = $diff['model']::model()->findByPk($diff['model_id']);
                            if (!is_null($model))
                            {
                                $model->delete();
                            }
                        }
                    }
                }
            }
        }
        else 
        {
            throw new SIMAWarnException(Yii::t('SIMADiffViewer.Translation', 'PleaseSelectSynchronizationItems'));
        }
        
        $this->sendEndEvent();
    }
    
    private function getItemCodeParentIdFromParentAttributes($model_name, $parent_attributes)
    {
        if(is_array($parent_attributes))
        {
            $parent_model = $model_name::model()->findByAttributes([
                'code' => $parent_attributes['code']
            ]);
            if (empty($parent_model))
            {
                $parent_model = new $model_name();
                $parent_model->name = $parent_attributes['name'];
                $parent_model->code = $parent_attributes['code'];
                $parent_model->save();
            }
            
            return $parent_model->id;           
        }
        else 
        {
            return $parent_attributes;
        }
    }
}
