<?php
/**
 * Description of WorkLicenseController
 *
 * @author goran
 */
class WorkLicenseController extends SIMAController
{
    public function filters()
    {
        return array(
            'ajaxOnly'
        )+parent::filters();
    }
    
    public function actionIndex()
    {   
        $html = $this->renderPartial('index', array(), true, true);

        $this->respondOk([
            'html' => $html,
            'title' => 'Prava-Licence',
        ]);
    }
    
    public function actionTabPersonWorkLicense($id)
    {
        $partner = Partner::model()->findByPk($id);
        if ($partner==null)
            throw new Exception('Partner ne postoji');
        $html = $this->renderPartial('partner_tab/workLicenses', array(
            'model' => $partner,
        ), true, false);
        
        $this->respondOK([
            'html'=>$html
        ]);
    }
    
    public function actionTabInfo($id)
    {
        $license = WorkLicense::model()->findByPk($id);
        if ($license==null)
            throw new Exception('Licenca ne postoji');
        $html = $this->renderPartial('WorkLicense_tab/info', array(
            'model' => $license,
        ), true, true);
        
        $this->respondOK([
            'html'=>$html
        ]);
    }
    
    public function actionLicenseTabs($id)
    {   
        $license_tabs=array();
        $license_tabs=array_merge($license_tabs, array(
           array(
               'title' => 'Informacije',
               'code'=>'info',
               'action'=> 'HR/workLicense/tabInfo',
               'get_params' => array('id' => $id)
           ), 
        ));
        
        $this->respondOK([
            'tabs' => $license_tabs,
        ]);
    }
    
    public function actionAddPartnersToCompanyLicense($company_license_to_company_id)
    {
        if (!isset($company_license_to_company_id))
        {
            throw new Exception('actionAddPartnersToCompanyLicense - nije zadata velika licenca');
        }
        if (!isset($_POST['partner_ids']))
        {
            throw new Exception('actionAddPartnersToCompanyLicense - nisu zadati partneri');
        }
        
        $partner_ids = isset($_POST['partner_ids']) ? $_POST['partner_ids']:[];
        $company_license_to_company = CompanyLicenseToCompany::model()->findByPk($company_license_to_company_id);

        foreach($partner_ids as $partner_id)
        {
            $company_license_to_partner = CompanyLicenseToPartner::model()->findByAttributes([
                'company_license_id'=>$company_license_to_company_id,
                'partner_id'=>$partner_id
            ]);
            if (is_null($company_license_to_partner))
            {
                $company_license_to_partner = new CompanyLicenseToPartner();
                $company_license_to_partner->company_license_id = $company_license_to_company_id;
                $company_license_to_partner->partner_id = $partner_id;
                $company_license_to_partner->save();
            }
        }
        
        Yii::app()->warnManager->sendWarns(Yii::app()->user->id, 'WARN_COMPANY_LICENSES_NOT_CONNECTED');
        
        $this->respondOK(array(),$company_license_to_company);
    }
    
    public function actionRemovePartnersFromCompanyLicense($company_license_to_company_id)
    {
        if (!isset($company_license_to_company_id))
        {
            throw new Exception('actionRemovePartnersFromCompanyLicense - nije zadata velika licenca');
        }
        
        $company_license_to_company = CompanyLicenseToCompany::model()->findByPk($company_license_to_company_id);
        foreach ($company_license_to_company->partners as $partner)
        {
            $company_license_to_partner = CompanyLicenseToPartner::model()->findByAttributes(array(
                'company_license_id'=>$company_license_to_company_id,
                'partner_id'=>$partner->id
            ));
            if ($company_license_to_partner !== null)
            {
                $company_license_to_partner->delete();
            }
        }
        $reference_to_company_license = ReferenceToCompanyLicense::model()->findAllByAttributes([
            'company_license_id'=>$company_license_to_company_id
        ]);
        if (count($reference_to_company_license) > 0)
        {
            foreach ($reference_to_company_license as $value) 
            {
                $value->delete();
            }
        }
        Yii::app()->warnManager->sendWarns(Yii::app()->user->id, 'WARN_COMPANY_LICENSES_NOT_CONNECTED');
        
        $this->respondOK(array(),$company_license_to_company);
    }
    
    public function actionCompanyLicenseByCompanies()
    {
        $title = Yii::t('HRModule.CompanyLicense', 'CompanyLicensesByCompanies');
        $uniq = SIMAHtml::uniqid();
        $html = Yii::app()->controller->renderContentLayout([
            'name'=>$title
        ], [
            [
                'proportion'=>0.5,
                'min_width'=>200,
                'class'=>'company-licenses',
                'html'=>$this->renderPartial('company_license_by_companies', [
                    'uniq' => $uniq,
                    'fixed_filter'=>[
                        'order_by' => 'company_license'
                    ]
                ], true, false)
            ],
            [
                'proportion'=>0.5,
                'min_width'=>200,
                'class'=>'company-licenses-tabs',
                'html'=>$this->widget('ext.SIMATabs.SIMATabs', array(
                    'id'=>"tabs_$uniq",
                    'list_tabs_model_name' => 'CompanyLicenseToCompany',
                    'list_tabs_populate' => false,
                    'default_selected'=>'info'
                ), true)
            ]
        ], [
            'id'=>$uniq,
            'class'=>'company-licenses-by-companies'
        ]);
        
        $this->respondOK([
            'html' => $html,
            'title' => $title
        ]);
    }
    
    public function actionCompanyLicenseByCompaniesNotConnected()
    {
        $html = $this->renderPartial('company_license_by_companies', [
            'fixed_filter'=>[
                'order_by' => 'company_license',
                'filter_scopes'=>['notConnected']
            ]
        ],true, true);
        
        $this->respondOK([
            'html' => $html,
            'title' => 'Velike licence po kompanijama koje nisu konektovane'
        ]);        
    }
    
    public function actionCompanyLicenseToCompanyDifferences()
    {
        $title = Yii::t('HRModule.CompanyLicense', 'CompanyLicenseDecisionDisagreementsView');
        $last_decision = CompanyLicenseDecision::getLastDecision();
        $company = Yii::app()->company;
        $uniq = SIMAHtml::uniqid();
        $html = Yii::app()->controller->renderContentLayout([
            'name'=>$title
        ], [
            [
                'proportion'=>0.5,
                'min_width'=>200,
                'class'=>'company-license-to-company',
                'html'=>$this->renderPartial('companyLicenseDecisionDifferences/left_panel', [
                    'uniq' => $uniq,
                    'company'=>$company
                ], true, false)
            ],
            [
                'proportion'=>0.5,
                'min_width'=>200,
                'class'=>'company-license-to-company-decision',
                'html'=>$this->renderPartial('companyLicenseDecisionDifferences/right_panel', [
                    'uniq' => $uniq,
                    'last_decision'=>$last_decision
                ], true, false)
            ]
        ], [
            'id'=>$uniq,
            'class'=>'company-licenses-by-companies'
        ]);
        
        $this->respondOK([
            'html' => $html,
            'title' => $title
        ]);
    }
    
    public function actionAddReferencesToCompanyLicense($company_license_id, $partner_id)
    {
        $remove_references_ids = isset($_POST['remove_references_ids']) ? $_POST['remove_references_ids'] : [];
        $add_references_ids = isset($_POST['add_references_ids']) ? $_POST['add_references_ids'] : [];
        
        foreach ($remove_references_ids as $remove_reference_id) 
        {
            $reference_to_company_licence = ReferenceToCompanyLicense::model()->findByAttributes([
                'reference_id'=>$remove_reference_id,
                'company_license_id'=>$company_license_id,
                'partner_id'=>$partner_id
            ]);
            if (!is_null($reference_to_company_licence))
            {
                $reference_to_company_licence->delete();
            }
        }
        
        $not_added_references = "";
        foreach ($add_references_ids as $add_reference_id) 
        {
            $reference_to_company_licence = ReferenceToCompanyLicense::model()->findByAttributes([
                'reference_id'=>$add_reference_id,
                'company_license_id'=>$company_license_id
            ]);
            if (is_null($reference_to_company_licence))
            {
                $reference_to_company_licence = new ReferenceToCompanyLicense();
                $reference_to_company_licence->reference_id = $add_reference_id;
                $reference_to_company_licence->company_license_id = $company_license_id;
                $reference_to_company_licence->partner_id = $partner_id;
                $reference_to_company_licence->save();
            }
            else
            {
                $not_added_references .= "<br />".$reference_to_company_licence->reference->DisplayName;
            }
        }
        
        if ($not_added_references !== '')
        {
            $this->raiseNote(Yii::t('HRModule.CompanyLicense', 'AlreadyAddedReferences').$not_added_references);
        }
        
        $this->respondOK();
    }
    
    public function actionCompanyLicensesByDecisions()
    {
        $title = Yii::t('HRModule.CompanyLicense', 'CompanyLicensesByDecisions');
        $uniq = SIMAHtml::uniqid();
        $html = Yii::app()->controller->renderContentLayout([
            'name'=>$title
        ], [
            [
                'proportion'=>0.5,
                'min_width'=>200,
                'class'=>'company-licenses-decisions',
                'html'=>$this->renderPartial('companyLicensesByDecisions/left_panel', [
                    'uniq' => $uniq
                ], true, false)
            ],
            [
                'proportion'=>0.5,
                'min_width'=>200,
                'class'=>'company-licenses',
                'html'=>$this->renderPartial('companyLicensesByDecisions/right_panel', [
                    'uniq' => $uniq
                ], true, false)
            ]
        ], [
            'id'=>$uniq,
            'class'=>'company-licenses-by-decisions'
        ]);
        
        $this->respondOK([
            'html' => $html,
            'title' => $title
        ]);
    }
    
    public function actionCompanyLicenseDecisionToCompanyLicensesDisagreements($company_license_decision_id, $uniq)
    {
        $company_license_decision = CompanyLicenseDecision::model()->findByPk($company_license_decision_id);
        $company = Yii::app()->company;
        $disagreements = CompanyLicenseDecision::getCompanyLicenseDifferencesByDecision($company_license_decision);
        
        $html = $this->renderPartial('company_license_decision_to_company_licenses_disagreements', array(
            'uniq' => $uniq,
            'company_license_decision' => $company_license_decision,
            'company'=>$company,
            'disagreements' => $disagreements,
        ), true, true);
        
        $this->respondOK([
            'html'=>$html,
            'has_disagreements'=>(count($disagreements)>0)?true:false
        ]);
    }
    
    public function actionApplyCompanyLicenseDecisionToCompanyLicensesDisagreements()
    {
        $disagreements = isset($_POST['disagreements']) ? $_POST['disagreements'] : [];
        
        foreach ($disagreements as $disagreement)
        {
            $company_license_to_company = CompanyLicenseToCompany::model()->findByAttributes([
                'company_id'=>$disagreement['company_id'],
                'company_license_id'=>$disagreement['company_license_id']
            ]);
            if ($disagreement['operation'] === 'REMOVE')
            {
                if (!is_null($company_license_to_company))
                {
                    $company_license_to_company->is_active = false;
                    $company_license_to_company->save();
                }
            }
            else if ($disagreement['operation'] === 'ADD')
            {
                if (is_null($company_license_to_company))
                {
                    $company_license_to_company = new CompanyLicenseToCompany();
                    $company_license_to_company->company_id = $disagreement['company_id'];
                    $company_license_to_company->company_license_id = $disagreement['company_license_id'];
                    $company_license_to_company->is_active = true;
                    $company_license_to_company->save();
                }
                else
                {
                    $company_license_to_company->is_active = true;
                    $company_license_to_company->save();
                }
            }
        }
        
        $this->respondOK();
    }
    
    public function actionCompanyLicensesByCompaniesView()
    {
        $uniq = SIMAHtml::uniqid();
        
        $left_panel = $this->renderPartial('companyLicensesToCompanies/left_panel', [
            'uniq' => $uniq
        ], true, false);
        $right_panel = $this->renderPartial('companyLicensesToCompanies/right_panel', [
            'uniq' => $uniq
        ], true, false);
        
        $title = Yii::t('HRModule.CompanyLicense', 'CompanyLicensesByCompanies2');
        $html = Yii::app()->controller->renderContentLayout([
            'name'=>$title,
            'options_class'=>'company-licenses-to-companies-options',
            'title_width'=>'50%',
            'options_width'=>'50%',
            'options'=>[]
        ], [
            [
                'proportion'=>0.5,
                'min_width'=>500,
                'html'=>$left_panel,
                'class'=>'company-licenses-to-companies-ls'
            ],
            [
                'proportion'=>0.5,
                'min_width'=>500,
                'html'=>$right_panel,
                'class'=>'company-licenses-to-companies-rs'
            ]
        ], [
            'id'=>$uniq,
            'class'=>'company-licenses-to-companies'
        ]);
        
        $this->respondOK([
            'html' => $html,
            'title' => $title
        ]);
    }
    
    public function actionAddCompanyLicensesToCompany($company_id)
    {
        $company_licenses_ids = $this->filter_post_input('company_licenses_ids', false);
        
        if (!empty($company_licenses_ids))
        {
            foreach ($company_licenses_ids as $company_license_id)
            {
                $company_license_to_company = CompanyLicenseToCompany::model()->findByAttributes([
                    'company_id' => $company_id,
                    'company_license_id' => $company_license_id
                ]);
                if (is_null($company_license_to_company))
                {
                    $company_license_to_company = new CompanyLicenseToCompany();
                    $company_license_to_company->company_id = $company_id;
                    $company_license_to_company->company_license_id = $company_license_id;
                    $company_license_to_company->save();
                }
            }
        }
        
        $this->respondOK();
    }
}

?>
