<?php

class AbsenceController extends SIMAController 
{
    public function actionAbsence($id)
    {
        $model = Absence::model()->findByPkWithWarnCheck($id);

        $selector = $this->filter_post_input('selector', false);

        if(empty($selector))
        {
            $selector = [];
        }

        if (is_string($selector))
        {
           $selector = CJSON::decode($selector,true);
        }
        
        $default_selected_tab = [];
        $default_selected = array_shift($selector);
        if (!empty($default_selected) && isset($default_selected['simaTabs']))
        {
            $default_selected_tab = [$default_selected['simaTabs'], 'selector'=>$selector];
        }
        
        $html = $this->widget('SIMADefaultLayout', [
            'model' => $model,
            'default_selected_tab' => $default_selected_tab
        ], true);

        $this->respondOK([
            'html' => $html,
            'title' => $model->DisplayName
        ]);
    }

    public function actionAbsenceTabs($employee_id)
    {
        $employee = Employee::model()->findByPk($employee_id);
        if ($employee === null)
        {
            throw new Exception("Ne postoji korisnik sa id-em $employee_id");
        }
        
        $html = $this->renderPartial('absences', array(            
            'employee_id' => $employee_id
        ), true, true);

        $this->respondOK(array('html' => $html));
    }
    
    public function actionGetAbsenceTabsForEmployee($employee_id)
    {
        $annual_leave_decision_document_type_id = Yii::app()->configManager->get('HR.absences.annual_leave_decision', false);
        $employee = Employee::model()->findByPk($employee_id);
        $curr_year = Year::current();
        $file_tag = FileTag::model()->findByAttributes([
            'model_id'=>$employee_id,
            'model_table'=>Employee::model()->tableName(),
        ]);
        $table_id = SIMAHtml::uniqid();
        $tabs = array(
            array(
                'title'=>'Pregled',
                'code'=>'absences_review',
                'action'=>'guitable',
                'get_params'=>array(
                    'settings'=>array(
                        'model'=>'Absence',
                        'columns_type'=>'from_user',
                        'fixed_filter'=>array('employee'=>['ids'=>$employee_id]),
                      )
                )
            ),
            array(
                'title' => 'Godišnji odmori',
                'code' => 'absence_annual_leaves',
                'action' => 'HR/absence/getAnnualLeavesForEmployee',
                'get_params' => array('employee_id' => $employee_id)
            ),
            array(
                'title'=>'Rešenja o godišnjem odmoru',
                'code'=>'absence_annual_leave_decisions',
                'action'=>'guitable',
                'get_params'=>array(
                    'settings'=>array(
                        'id'=>$table_id,
                        'model'=>'AnnualLeaveDecision',
                        'columns_type'=>'from_user',
                        'add_button' => array(
                            'init_data'=>array(
                                'AnnualLeaveDecision' => array(
                                    'employee_id' => [
                                        'disabled',
                                        'init' => $employee_id
                                    ],                                    
                                ),
                                'File'=>[
                                    'name'=>$employee->DisplayName,
                                    'document_type_id'=>[
                                        'disabled',
                                        'init'=>$annual_leave_decision_document_type_id
                                    ],
                                    'responsible_id'=>Yii::app()->user->id,
                                    'file_belongs' => [
                                        'ids' => '{"ids":[{"id":'.$file_tag->id.',"display_name":"'.$file_tag->DisplayName.'","class":"_visible"}],"default_item":[]}'
                                    ]
                                ]
                            ),            
                        ),
                        'fixed_filter'=>array('employee'=>['ids'=>$employee_id]),
                        'select_filter'=>[
                            'year'=>[
                                'ids'=>$curr_year->id
                            ]
                        ],
                        'custom_buttons' => [
                            'Dodaj postojeći fajl' => [
                                'func' =>"sima.hr.addAbsenceAnnualLeaveDecisionFromExistingFile('$table_id', $employee_id, $annual_leave_decision_document_type_id)"
                            ]
                        ]
                      )
                )
            ),
            array(
                'title' => 'Bolovanja',
                'code' => 'absence_sick_leaves',
                'action' => 'HR/absence/getSickLeavesForEmployee',
                'get_params' => array('employee_id' => $employee_id)
            ),
            array(
                'title' => 'Plaćeno odsustvo',
                'code' => 'absence_free_days',
                'action' => 'HR/absence/getFreeDaysForEmployee',
                'get_params' => array('employee_id' => $employee_id)
            ),
        );

        $this->respondOK([
            'tabs' => $tabs,
        ]);
    }
    
    public function actionGetAnnualLeavesForEmployee($employee_id)
    {
        $employee = Employee::model()->findByPk($employee_id);
        
        if(empty($employee))
        {
            throw new SIMAWarnException(Yii::t('HRModule.Absence', 'ThisActionPossibleOnlyForEmployee'));
        }
        
        $html = $this->renderPartial('absence_annual_leaves', array(            
            'employee' => $employee
        ), true, true);
        $title = AbsenceAnnualLeave::model()->modelLabel(true);
        
        $this->respondOK(array(
            'html' => $html,
            'title' => $title
        ));
    }
    
    public function actionGetSickLeavesForEmployee($employee_id)
    {   
        $employee = Employee::model()->findByPk($employee_id);
        
        if(empty($employee))
        {
            throw new SIMAWarnException(Yii::t('HRModule.Absence', 'ThisActionPossibleOnlyForEmployee'));
        }
        
        $html = $this->renderPartial('absence_sick_leaves', array(            
            'employee_id' => $employee->id
        ), true, true);
        $title = AbsenceSickLeave::model()->modelLabel(true);

        $this->respondOK(array(
            'html' => $html,
            'title' => $title
        ));
    }
    
    public function actionGetFreeDaysForEmployee($employee_id)
    {   
        $employee = Employee::model()->findByPk($employee_id);
        
        if(empty($employee))
        {
            throw new SIMAWarnException(Yii::t('HRModule.Absence', 'ThisActionPossibleOnlyForEmployee'));
        }
        
        $html = $this->renderPartial('absence_free_days', array(            
            'employee_id' => $employee->id
        ), true, true);
        $title = AbsenceFreeDays::model()->modelLabel(true);

        $this->respondOK(array(
            'html' => $html,
            'title' => $title
        ));
    }    
    
    public function actionGenerateAbsenceTemplates($absence_id, $template_type)
    {
        $absence = Absence::model()->findByPk($absence_id);
        
        if ($template_type === '1') //ako je slucaj da je godisnji odmor
        {
            $template_id = Yii::app()->configManager->get('HR.annual_leave_template');
            if (empty($template_id))
            {
                throw new SIMAWarnException('Nije postavljen template za godišnji odmor. Molimo Vas da kontaktirate administratora.');
            }
            if (!isset($absence->document))
            {
                $file = new File();
                $annual_leave_document_type_id=Yii::app()->configManager->get('HR.absences.annual_leave', false);
                if ($annual_leave_document_type_id === '')
                {
                    $this->raiseNote("Za dokumenat 'Godisnji odmor' nije postavljen tip dokumenta. Kontaktirajte administratora kako bi postavio tip dokumenta!");
                }
                $file->document_type_id=$annual_leave_document_type_id;
                $file->responsible_id=Yii::app()->user->id;
                $file->save();
                $file->refresh();
                $absence->document_id = $file->id;
                $absence->save();
            }
            $file_id = $absence->document_id;
        }
        else if ($template_type === '2') //ako je slucaj da su slobodni dani
        {
            $template_id = Yii::app()->configManager->get('HR.days_off_template');
            if (empty($template_id))
            {
                throw new SIMAWarnException('Nije postavljen template za plaćeno odsustvo. Molimo Vas da kontaktirate administratora.');
            }
            if (!isset($absence->document))
            {
                $file = new File();
                $days_off_document_type_id=Yii::app()->configManager->get('HR.absences.days_off', false);
                if ($days_off_document_type_id === '')
                {
                    $this->raiseNote("Za dokumenat 'Plaćeno odsustvo' nije postavljen tip dokumenta. Kontaktirajte administratora kako bi postavio tip dokumenta!");
                }
                $file->document_type_id=$days_off_document_type_id;
                $file->responsible_id=Yii::app()->user->id;
                $file->save();
                $file->refresh();
                $absence->document_id = $file->id;
                $absence->save();
            }
            $file_id = $absence->document_id;
        }
        
        $templated_file = TemplatedFile::model()->with('templated_file_data')->findByPk($file_id);
        if ($templated_file === null)
        {
            $template_file_id = TemplateController::setTemplate($file_id, $template_id);
        }
        else
        {
            $template_file_id = $templated_file->id;
        }
        TemplateController::addVersion($template_file_id);
        
        $this->respondOK(array(), array($absence));
    }
    
    public function actionAddSickLeaveDocument($absence_id, $file_id)
    {
        if (!isset($absence_id) || !isset($file_id))
        {
            throw new Exception('actionAddSickLeaveDocument - nisu lepo zadati parametri.');
        }
        //Absence ne treba da se koristi za cuvanje
//        $absence = Absence::model()->findByPk($absence_id);
        $absence = AbsenceSickLeave::model()->findByPk($absence_id);
        $absence->document_id = $file_id;
        $absence->save();
        
        $this->respondOK(array(), array($absence));
    }
    
    public function actionGenerateAnnualLeaveDecisionDocument($annual_leave_decision_id)
    {
        if (!isset($annual_leave_decision_id))
        {
            throw new Exception('actionGenerateAnnualLeaveDecisionDocument - nisu lepo zadati parametri');
        }
        
        $annual_leave_decision = AnnualLeaveDecision::model()->findByPk($annual_leave_decision_id);
        
        if ($annual_leave_decision === null)
        {
            throw new Exception('Ne postoji rešenje za godišnji odmor sa id '.$annual_leave_decision_id);
        }
        
        $annual_leave_decision->generateDocument();
        
        $this->respondOK([], $annual_leave_decision);
    }
    
    public function actionCollectiveAnnualLeave($id)
    {
        $model = CollectiveAnnualLeave::model()->findByPk($id);
        if ($model==null)
        {
            throw new Exception("Odsustvo sa ID $id ne postoji");
        }
        
        $html = $this->renderDefaultModelView($model, [
            'tabs_params' => [
                'default_selected' => 'employees'
            ]
        ]);
        
        $title = $model->DisplayName;
        
        $this->respondOK(array(
            'html' => $html,
            'title' => $title
        ));
    }
    
    public function actionAddAllEmployeesToCollectiveAnnualLeave()
    {        
        Yii::app()->setUpdateModelViews(false);
        
        $data = $this->setEventHeader(); 
        
        set_time_limit(5);
        $eventSourceStepsSum = 0.0;
        Yii::app()->controller->sendUpdateEvent($eventSourceStepsSum); 
                
        $collective_annual_leave_id = $data['collective_annual_leave_id'];
        $collectiveLeave = CollectiveAnnualLeave::model()->findByPk($collective_annual_leave_id);
        
        $employees = $collectiveLeave->getEmployeeIdsActiveForThisLeave();
        
        $error_messages = [];
                
        $employees_count = count($employees);
        if($employees_count > 0)
        {
            $eventSourceSteps = (100/$employees_count);
        }
        else
        {
            $eventSourceStepsSum += 100;
            Yii::app()->controller->sendUpdateEvent($eventSourceStepsSum);
        }
        
        $max_execution_time = ini_get('max_execution_time'); 
                        
        foreach($employees as $emp_id)
        {
            set_time_limit(1);
            
            $add_result = $this->addEmployeeToCollectiveAnnualLeaveIfNotInAlready($collectiveLeave, $emp_id);
            if($add_result !== true)
            {
                $error_messages[] = $add_result;
            }
                        
            $eventSourceStepsSum += $eventSourceSteps;
            Yii::app()->controller->sendUpdateEvent($eventSourceStepsSum);
        }
        
        set_time_limit($max_execution_time);
                
        $message = Yii::t('BaseModule.Common', 'Success');
        if(count($error_messages) > 0)
        {            
            $message = Yii::t('HRModule.Absence', 'CollectiveAnnualLeavesCouldNotBeCreated', [
                '{error_messages}' => implode('</br>', $error_messages)
            ]);
        }
        
        $this->sendEndEvent([
            'message' => $message
        ]);
    }
    
    public function actionAddEmployeesToCollectiveAnnualLeaves()
    {
        $collective_annual_leave_id = $this->filter_post_input('collective_annual_leave_id');
        $employee_ids = $this->filter_post_input('employee_ids');
        
        $collective_annual_leave = CollectiveAnnualLeave::model()->findByPkWithCheck($collective_annual_leave_id);
        
        $error_messages = [];
        
        foreach($employee_ids as $empid)
        {
            $add_result = $this->addEmployeeToCollectiveAnnualLeaveIfNotInAlready($collective_annual_leave, $empid);
            if($add_result !== true)
            {
                $error_messages[] = $add_result;
            }
        }
        
        if(count($error_messages) > 0)
        {
            $message = Yii::t('HRModule.Absence', 'CollectiveAnnualLeavesCouldNotBeCreated', [
                '{error_messages}' => implode('</br>', $error_messages)
            ]);
            throw new SIMAWarnException($message);
        }
        
        $this->respondOK();
    }
    
    public function actionCollectiveLeaveAfterSaveResaveAnnualLeaves()
    {        
        $data = $this->setEventHeader(); 
        
        set_time_limit(5);
        $eventSourceSteps = 0;
        $eventSourceStepsSum = 1;
        Yii::app()->controller->sendUpdateEvent($eventSourceStepsSum);
        
        $collective_annual_leave_id = $data['collective_annual_leave_id'];
        $collectiveLeave = CollectiveAnnualLeave::model()->findByPk($collective_annual_leave_id);
                
        $annual_leaves = $collectiveLeave->annual_leaves;
                
        $deleted_messages = [];
        
        $annual_leaves_count = count($annual_leaves);
        if($annual_leaves_count > 0)
        {
            $eventSourceSteps = (99/$annual_leaves_count);
        }
        else
        {
            $eventSourceStepsSum += 99;
            Yii::app()->controller->sendUpdateEvent($eventSourceStepsSum);
        }
                
        foreach($annual_leaves as $annual_leave)
        {            
            set_time_limit(5);
            
            $annual_leave->begin = $collectiveLeave->from_day->day_date;
            $annual_leave->end = $collectiveLeave->to_day->day_date;
            if(!$annual_leave->save(true, null, false))
            {
                $errors = $annual_leave->getErrors();
                $error_message = '';
                foreach($errors as $value)
                {
                    if(!empty($error_message))
                    {
                        $error_message .= '</br>';
                    }

                    $error_message .= implode('</br>', $value);
                }
                
                $deleted_messages[] = Yii::t('HRModule.Absence', 'AbsenceNotValid', [
                    '{emp}' => $annual_leave->employee->DisplayName,
                    '{errors}' => $error_message
                ]);
                
                $annual_leave->delete();
            }
                        
            $eventSourceStepsSum += $eventSourceSteps;
            Yii::app()->controller->sendUpdateEvent($eventSourceStepsSum);
        }
                
        set_time_limit(5);
        
        $message = Yii::t('BaseModule.Common', 'Success');
        if(count($deleted_messages) > 0)
        {            
            $message = Yii::t('HRModule.Absence', 'DeletedAnnualLeavesForDateDiff', [
                '{messages}' => implode('</br>', $deleted_messages)
            ]);
        }
                
        $this->sendEndEvent([
            'message' => $message
        ]);
    }
    
    //za sada ne treba, ali se moze iskoristiti uz male modifikacije za kasnije - NE BRISATI - SASA A.
//    public function actionCreateAnnualLeaveDecisionsForAllEmployeesForYear()
//    {
//        $data = $this->setEventHeader();                
//        
//        if (!isset($data['year']))
//        {
//            throw new SIMAException('actionCreateAnnualLeaveDecisionsForAllEmployeesForYear - nije zadata godina');
//        }
//        
//        $year = $data['year'];
//        $year_model = Year::get($year);        
//        $from_day = Day::getByDate("01.01.$year");
//        $to_day = Day::getByDate("31.12.$year");
//        
//        $employees = Employee::model()->findAll();
//        
//        $i = 0;
//        $cnt = count($employees);
//        $max_while_cycle_exec_time_seconds = ini_get('max_execution_time');
//        foreach ($employees as $employee) 
//        {
//            set_time_limit($max_while_cycle_exec_time_seconds);
//            $percent = round(($i/$cnt)*100, 0, PHP_ROUND_HALF_DOWN);
//            $this->sendUpdateEvent($percent);
//            if ($employee->id === 5019)
//            {
//                $criteria = new SIMADbCriteria([
//                    'Model' => 'WorkContract',
//                    'model_filter' => [
//                        'employee' => [
//                            'ids' => $employee->id
//                        ],
//                        'active_at' => [$from_day, $to_day]
//                    ]
//                ], true);
//                $work_contracts = WorkContract::model()->findAll($criteria);
//
//                foreach ($work_contracts as $work_contract) 
//                {
//                    if ($work_contract->id === 330212)
//                    {
//                        $annual_leave_decision = new AnnualLeaveDecision();
//                        $annual_leave_decision->employee_id = $employee->id;
//                        $annual_leave_decision->work_contract_id = $work_contract->id;
//                        $annual_leave_decision->year_id = $year_model->id;
//                        $annual_leave_decision->days_number = 20;
//                        $annual_leave_decision->description = 'Automatsko kreirano rešenje';
//                        $annual_leave_decision->save();
//                        $annual_leave_decision->refresh();
//                        $annual_leave_decision->days_number = $annual_leave_decision->calcAnnualLeaveDays();
//                        $annual_leave_decision->save();
//                    }
//                }
//            }
//            $i++;
//        }
//        
//        $this->sendEndEvent([]);
//    }
    
    public function actionGetAnnualLeaveDecisionsForYear($year_id)
    {        
        $html = $this->renderPartial('annual_leave_decisions_for_year',[            
            'year_id'=>$year_id
        ],true,true);
        
        $this->respondOK(array(
            'html'=>$html
        ));
    }
    
    public function actionAnnualLeaveDecisionsByYear()
    {
        $html = $this->renderPartial('annual_leave_decisions_by_year',array(),true,true);
        $this->respondOK(array(
           'html' => $html
        ));
    }
    
    public function actionAbsencesOverview()
    {
        $uniq = SIMAHtml::uniqid();
        $absence_list = [
            [
                'title' => Yii::t('HRModule.Absence', 'Absences'),
                'code'  => 1,
                'data'  => ['url' => 'HR/absence/allAbsences']
            ],
            [
                'title' => Yii::t('HRModule.Absence', 'CollectiveAbsencesAndHolidays'),
                'code'  => 2,
                'data'  => ['url' => 'HR/absence/collectiveAnnualLeaves']
            ],
            [
                'title' => Yii::t('HRModule.Absence', 'AnnualLeaveDecisionsByYear'),
                'code'  => 3,
                'data'  => ['url' => 'HR/absence/annualLeaveDecisionsByYear']
            ],
            [
                'title' => Yii::t('HRModule.Absence', 'EmployeeSlave'),
                'code'  => 4,
                'data'  => ['url' => 'HR/absence/employeeSlave']
            ],
        ];
        
        Yii::app()->clientScript->registerScript(
            "absence_$uniq",
            "sima.hr.initAbsencesMenuEvents('$uniq');",
            CClientScript::POS_READY
        );
               
        $panels = [
            [
                'proportion'=>0.2,
                'min_width'=>200,
                'max_width'=>300,
                'html'=>$this->renderPartial('absencesOverview/absence_menu', [
                    'uniq'=>$uniq,
                    'absence_list' => $absence_list
                ], true, false)
            ],
            [
                'proportion'=>0.8,
                'min_width'=>300,
                'html'=>$this->renderPartial('absencesOverview/absence_wrap', [
                    'uniq'=>$uniq
                ], true, false)
            ]
        ];

        $html = Yii::app()->controller->renderContentLayout(
            ['name' => Yii::t('HRModule.Absence', 'Absences')],
            $panels, 
            ['id'=>$uniq,' class'=>'sima-annual']
        );       
        
        $this->respondOK(array(
            'html' => $html,
            'title'=> Yii::t('HRModule.Absence', 'Absences')
        ));
    }
    
    public function actionAllAbsences() 
    {
        $uniq = SIMAHtml::uniqid();
        
        if(SIMAMisc::isVueComponentEnabled())
        {
            $additional_options_html = Yii::app()->controller->widget(SIMAButtonVue::class, [
                'id' => 'btn-toggle-activity'.$uniq,
                'title'=> Yii::t('HRModule.Absence', 'ActiveAbsences'),
                'tooltip'=> Yii::t('HRModule.Absence', 'ActiveAbsences'),                               
                'onclick'=> ['sima.hr.applyActiveAbsencesFilter', $uniq]
            ],true);
        }
        else
        {
            $additional_options_html = Yii::app()->controller->widget('SIMAButton', [
                'id' => 'btn-toggle-activity'.$uniq,
                'action'=>[ 
                    'title'=> Yii::t('HRModule.Absence', 'ActiveAbsences'),
                    'tooltip'=> Yii::t('HRModule.Absence', 'ActiveAbsences'),                               
                    'onclick'=> ['sima.hr.applyActiveAbsencesFilter', $uniq]
                ]
            ],true);
        }
        
        $html = $this->widget(SIMAGuiTable::class, [
                'id' => 'absence'.$uniq,    
                'title' => Yii::t('HRModule.Absence', 'Absences'),
                'columns_type' => 'from_all_absences',
                'show_add_button' => false,
                'model' => Absence::model(),            
                'fixed_filter' => [
                    'display_scopes' => 'byBeginDateAscOrder'
                ],
                'additional_options_html'=> $additional_options_html
            ], true);
        
        $this->respondOK(array(
            'html' => $html
        ));
    }   
    
    public function actionCollectiveAnnualLeaves() 
    {      
        $html = $this->renderPartial('collective_annual_leaves', [], true, false);
                
        $this->respondOK(array(
            'html' => $html
        ));
    }
    
    public function actionEmployeeSlave() 
    {
        $uniq = SIMAHtml::uniqid();
        $employee_slave = Yii::t('MainMenu', 'EmployeeSlave');
        $html = $this->widget(SIMAGuiTable::class, [
                    'id' => 'employee-table'.$uniq,   
                    'model' => Employee::model(),     
                    'title' => $employee_slave,
                    'columns_type' => 'only_slava',
                    'show_add_button' => false,
                ], true);
        $this->respondOK(array(
            'html' => $html,
            'title' => $employee_slave
        ));
    }
    
    public function actionGetCollectiveAnnualLeaveFullInfo($model_id)
    {
        $model = CollectiveAnnualLeave::model()->findByPkWithCheck($model_id);
        $html = Yii::app()->controller->renderModelView($model, 'full_info');
        
        $this->respondOK(array(
            'html' => $html
        ));
    }  
    
    private function addEmployeeToCollectiveAnnualLeaveIfNotInAlready(CollectiveAnnualLeave $collectiveLeave, $emp_id)
    {
        $result = true;
        
        $absCriteria = new SIMADbCriteria([
            'Model' => 'AbsenceAnnualLeave',
            'model_filter' => [
                'employee' => [
                    'ids' => $emp_id
                ],
                'collective_annual_leave' => [
                    'ids' => $collectiveLeave->id
                ]
            ]
        ], true);

        if(empty($absCriteria->ids))
        {
            $newAbsence = new AbsenceAnnualLeave();
            $newAbsence->employee_id = $emp_id;
            $newAbsence->begin = $collectiveLeave->from_day->day_date;
            $newAbsence->end = $collectiveLeave->to_day->day_date;
            $newAbsence->type = Absence::$ANNUAL_LEAVE;
            $newAbsence->collective_annual_leave_id = $collectiveLeave->id;
            $newAbsence->description = Yii::t('HRModule.Absence', 'CollectiveAnnualLeave');
            $newAbsence->number_work_days = AbsenceComponent::CalculateNumberWorkDaysBetweenDates($newAbsence);

            if(!$newAbsence->save(true, null, false))
            {                    
                $errors = $newAbsence->getErrors();
                $error_message = '';
                foreach($errors as $value)
                {
                    if(!empty($error_message))
                    {
                        $error_message .= '</br>';
                    }

                    $error_message .= implode('</br>', $value);
                }

                $result = Yii::t('HRModule.Absence', 'AbsenceNotValid', [
                    '{emp}' => $newAbsence->employee->DisplayName,
                    '{errors}' => $error_message
                ]);
            }
        }
        
        return $result;
    }       
}
