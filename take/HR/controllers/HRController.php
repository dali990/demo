<?php

class HRController extends SIMAController 
{    
    public function actionIksPointsReview()
    {
        $html = $this->renderPartial('iks_points_review', [], true, true);        
        
        $this->respondOK([                                    
            'html' => $html,
            'title' => 'IKS poeni svih inžinjera',
        ]);
    }
    
    public function actionEmailConfirmHRDocumentDelivery($delivery_hr_document_id, $employee_id)
    {
        $ids = explode(',', $delivery_hr_document_id);
        foreach ($ids as $id) 
        {
            $delivery_hr_document = ModelController::GetModel('DeliveredHRDocument', $id);
            if ($delivery_hr_document->received === false)
            {
                $delivery_hr_document->received = true;
                $delivery_hr_document->recipient_id = $employee_id;
                $delivery_hr_document->save();
            }
        }
        
        $this->modelUpdateEventsMarkWaiting();
        
        echo "Hvala na potvrdi!";
        Yii::app()->end();
    }
    
    public function actionWarnsByEmployeesOverview($company_sector_id)
    {
        $html = Yii::app()->warnManager->renderWarnsByUsers([
            'person' => [
                'person_to_work_positions' => [
                    'work_position' => [
                        'recursive_company_sector_id' => $company_sector_id
                    ]
                ]
            ]
        ]);
        
        $this->respondOK([
            'html' => $html
        ]);
    }
    
    public function actionGetOccupationDiffBetweenLocalAndCodebook() 
    {
        $this->setEventHeader();
        
        $codebook_occupations = AdminController::getDataFromCodebook(Yii::app()->params['codebook_base'].AdminController::$codebook_const_path_part.'getOccupations');
        $converted_codebook_occupations = AdminController::convertCodebookValuesIntoAppropriateLanguage($codebook_occupations);
        
        $name_column_display = Yii::t('BaseModule.Common', 'Name');
        $code_column_display = Yii::t('HRModule.Occupation', 'Code');
        $parent_column_display = Yii::t('HRModule.Occupation', 'Parent');
                       
        $codebook_unique_fields = [];
        $items = [];
        $i = 1;
        
        $codebook_occupations_cnt = count($converted_codebook_occupations);
        foreach ($converted_codebook_occupations as $codebook_occupation)
        {
            $percent = round(($i/$codebook_occupations_cnt)*70, 0, PHP_ROUND_HALF_DOWN);
            $this->sendUpdateEvent($percent);
            $local_occupation = Occupation::model()->findByAttributes(['code' => $codebook_occupation['code']]);
            
            $local_occupation_parent_attributes = '';
            $local_occupation_parent_name = '';
            $local_occupation_parent_code = '';
                       
            if(!empty($codebook_occupation["parent"]))
            {
                $local_occupation_parent = Occupation::model()->findByAttributes(['code' => $codebook_occupation['parent']['code']]);
                if(!empty($local_occupation_parent))
                {
                    $local_occupation_parent_attributes = $local_occupation_parent->id;
                }
                else
                {
                    $local_occupation_parent_attributes = $codebook_occupation['parent'];
                }
            }
                        
            if (empty($local_occupation))
            {
                $items[] = [
                    'status' => 'ADD',
                    'columns' => [
                        $name_column_display => ['new_value' => $codebook_occupation["name"]],
                        $code_column_display => ['new_value' => $codebook_occupation["code"]],
                        $parent_column_display => ['new_value' => !empty($codebook_occupation["parent"]) ? $codebook_occupation["parent"]["name"]:'']
                    ],
                    'data' => [
                        'status' => 'ADD',
                        'model' => Occupation::class,
                        'attributes' => [
                            'name' => $codebook_occupation['name'],
                            'code' => $codebook_occupation['code'],
                            'parent' => $local_occupation_parent_attributes
                        ]
                    ]
                ];  
            }
            else
            {
                if(!empty($local_occupation->parent))
                {
                    $local_occupation_parent_code = $local_occupation->parent->code;
                    $local_occupation_parent_name = $local_occupation->parent->name;
                }
                
                if (
                        $codebook_occupation['name'] !== $local_occupation->name || 
                        $codebook_occupation['code'] !== $local_occupation->code || 
                        (
                            !empty($codebook_occupation['parent']) &&
                            $codebook_occupation['parent']['code'] !== $local_occupation_parent_code   
                        )
                    )
                {                   
                    $items[] = [
                        'status' => 'MODIFY',
                        'columns' => [
                            $name_column_display => [
                                'old_value' => $local_occupation->name,
                                'new_value' => $codebook_occupation['name']
                            ],
                            $code_column_display => [
                                'old_value' => $local_occupation->code,
                                'new_value' => $codebook_occupation['code']
                            ],
                            $parent_column_display => [
                                'old_value' => $local_occupation_parent_name,
                                'new_value' => !empty($codebook_occupation["parent"]) ? $codebook_occupation["parent"]["name"]:''
                            ]
                        ],
                        'data' => [
                            'status' => 'MODIFY',
                            'model' => Occupation::class,
                            'model_id' => $local_occupation->id,
                            'attributes' => [
                                'name' => $codebook_occupation['name'],
                                'code' => $codebook_occupation['code'],
                                'parent' => $local_occupation_parent_attributes,
                            ]
                        ]
                    ]; 
                }                                 
                $codebook_unique_fields[] = $codebook_occupation["code"];
            }
            $i++;
        }       
                
        $local_occupations = Occupation::model()->findAll(new SIMADbCriteria([
            'Model' => Occupation::class,
            'model_filter' => [
                'filter_scopes' => [
                    'withoutUniqueFields' => [$codebook_unique_fields]
                ]
            ]
        ]));
        
                
        $j = 1;
        $local_occupation_cnt = count($local_occupations);
        foreach ($local_occupations as $local_occupation)
        {          
            $percent = 70 + round(($j/$local_occupation_cnt)*30, 0, PHP_ROUND_HALF_DOWN);
            $this->sendUpdateEvent($percent);
            
            $local_occupation_parent_name = '';
            if(!empty($local_occupation->parent))
            {
                $local_occupation_parent_name = $local_occupation->parent->name;
            }
            $items[] = [
                'status' => 'REMOVE',
                'columns' => [
                    $name_column_display => ['old_value' => $local_occupation->name],
                    $code_column_display => ['old_value' => $local_occupation->code],
                    $parent_column_display => ['old_value' => $local_occupation_parent_name]
                ],
                'data' => [
                    'status' => 'REMOVE',
                    'model' => Occupation::class,
                    'model_id' => $local_occupation->id,
                ]
            ];
            $j++;
        }
        
        $html = '';      
        if (!empty($items))
        {
            $html = $this->widget('SIMADiffViewer', [                   
                'diffs' => [
                    [
                        'type' => 'group',
                        'group_id' => SIMAHtml::uniqid(),
                        'group_name' => Yii::t('HRModule.Occupation', 'Occupations'),
                        'columns' => [
                            $name_column_display,
                            $code_column_display,
                            $parent_column_display
                        ],
                        'items' => $items
                    ]
                ]
            ], true);
        }
        
        $this->sendEndEvent([
            'html' => $html
        ]);
    }
    
    public function actionSaveOccupationsDiffBetweenLocalAndCodebook()
    {
        $data = $this->setEventHeader();
        $diffs = $data['client_data'];
        
        if (!empty($diffs) && is_array($diffs))
        {
            foreach ($diffs as $diff)
            {
                if (!empty($diff['status']))
                {
                    $status = $diff['status'];
                    if ($status === 'ADD')
                    {
                        if (!empty($diff['model']) && !empty($diff['attributes']))
                        {
                            $model = Occupation::model()->findByAttributes([
                                'code' => $diff['attributes']['code']
                            ]);
                            if (empty($model))
                            {
                                $model = new Occupation();
                            }
                            $model->name = $diff['attributes']['name'];
                            $model->code = $diff['attributes']['code'];
                            if (!empty($diff['attributes']['parent']))
                            {
                                $model->parent_id = $this->getOccupationCodeParentIdFromParentAttributes($diff['attributes']['parent']);
                            }
                            $model->save();
                        }
                    }
                    else if ($status === 'MODIFY')
                    {
                        if (!empty($diff['model']) && !empty($diff['model_id']) && !empty($diff['attributes']))
                        {
                            $model = Occupation::model()->findByPk($diff['model_id']);
                            if (!is_null($model))
                            {
                                $model->name = $diff['attributes']['name'];
                                $model->code = $diff['attributes']['code'];
                                if (!empty($diff['attributes']['parent']))
                                {
                                    $model->parent_id = $this->getOccupationCodeParentIdFromParentAttributes($diff['attributes']['parent']);
                                }
                                $model->save();
                            }
                        }
                    }
                    if ($status === 'REMOVE')
                    {
                        if (!empty($diff['model']) && !empty($diff['model_id']))
                        {
                            $model = $diff['model']::model()->findByPk($diff['model_id']);
                            if (!is_null($model))
                            {
                                $model->delete();
                            }
                        }
                    }
                }
            }
        }
        else 
        {
            throw new SIMAWarnException(Yii::t('SIMADiffViewer.Translation', 'PleaseSelectSynchronizationItems'));
        }
        
        $this->sendEndEvent();
    }
    private function getOccupationCodeParentIdFromParentAttributes($parent_attributes)
    {
        if(is_array($parent_attributes))
        {
            $parent_model = Occupation::model()->findByAttributes([
                'code' => $parent_attributes['code']
            ]);
            if (empty($parent_model))
            {
                $parent_model = new Occupation();
                $parent_model->name = $parent_attributes['name'];
                $parent_model->code = $parent_attributes['code'];
                $parent_model->save();
            }

            return $parent_model->id;           
        }
        else 
        {
            return $parent_attributes;
        }
    }
    
    public function actionGetQualificationLevelsDiffBetweenLocalAndCodebook() 
    {
        $this->setEventHeader();
        
        $codebook_qls = AdminController::getDataFromCodebook(Yii::app()->params['codebook_base'].AdminController::$codebook_const_path_part.'getQualificationsLevel');
        $converted_codebook_qls = AdminController::convertCodebookValuesIntoAppropriateLanguage($codebook_qls);
        
        $name_column_display = Yii::t('BaseModule.Common', 'Name');
        $code_column_display = Yii::t('HRModule.QualificationLevel', 'Code');
        $description_column_display = Yii::t('HRModule.QualificationLevel', 'Description');
                       
        $codebook_unique_fields = [];
        $items = [];
        $i = 1;
        
        $codebook_qls_cnt = count($converted_codebook_qls);
        foreach ($converted_codebook_qls as $codebook_ql)
        {
            $percent = round(($i/$codebook_qls_cnt)*70, 0, PHP_ROUND_HALF_DOWN);
            $this->sendUpdateEvent($percent);
            $local_ql = QualificationLevel::model()->findByAttributes(['code' => $codebook_ql['code']]);                     
                        
            if (empty($local_ql))
            {
                $items[] = [
                    'status' => 'ADD',
                    'columns' => [
                        $name_column_display => ['new_value' => $codebook_ql["name"]],
                        $code_column_display => ['new_value' => $codebook_ql["code"]],
                        $description_column_display => ['new_value' => $codebook_ql["description"]]
                    ],
                    'data' => [
                        'status' => 'ADD',
                        'model' => QualificationLevel::class,
                        'attributes' => [
                            'name' => $codebook_ql['name'],
                            'code' => $codebook_ql['code'],
                            'description' => $codebook_ql['description']
                        ]
                    ]
                ];  
            }
            else
            {               
                if (
                        $codebook_ql['name'] !== $local_ql->name || 
                        $codebook_ql['code'] !== $local_ql->code || 
                        $codebook_ql['description'] !== $local_ql->description
                    )
                {                   
                    $items[] = [
                        'status' => 'MODIFY',
                        'columns' => [
                            $name_column_display => [
                                'old_value' => $local_ql->name,
                                'new_value' => $codebook_ql['name']
                            ],
                            $code_column_display => [
                                'old_value' => $local_ql->code,
                                'new_value' => $codebook_ql['code']
                            ],
                            $description_column_display => [
                                'old_value' => $local_ql->description,
                                'new_value' => $codebook_ql['description']
                            ]
                        ],
                        'data' => [
                            'status' => 'MODIFY',
                            'model' => QualificationLevel::class,
                            'model_id' => $local_ql->id,
                            'attributes' => [
                                'name' => $codebook_ql['name'],
                                'code' => $codebook_ql['code'],
                                'description' => $codebook_ql['description'],
                            ]
                        ]
                    ]; 
                }                                 
                $codebook_unique_fields[] = $codebook_ql["code"];
            }
            $i++;
        }       
                
        $local_qls = QualificationLevel::model()->findAll(new SIMADbCriteria([
            'Model' => QualificationLevel::class,
            'model_filter' => [
                'filter_scopes' => [
                    'withoutUniqueFields' => [$codebook_unique_fields]
                ]
            ]
        ]));
        
                
        $j = 1;
        $local_ql_cnt = count($local_qls);
        foreach ($local_qls as $local_ql)
        {          
            $percent = 70 + round(($j/$local_ql_cnt)*30, 0, PHP_ROUND_HALF_DOWN);
            $this->sendUpdateEvent($percent);
            
            $items[] = [
                'status' => 'REMOVE',
                'columns' => [
                    $name_column_display => ['old_value' => $local_ql->name],
                    $code_column_display => ['old_value' => $local_ql->code],
                    $description_column_display => ['old_value' => $local_ql->description]
                ],
                'data' => [
                    'status' => 'REMOVE',
                    'model' => QualificationLevel::class,
                    'model_id' => $local_ql->id,
                ]
            ];
            $j++;
        }
        
        $html = '';      
        if (!empty($items))
        {
            $html = $this->widget('SIMADiffViewer', [                   
                'diffs' => [
                    [
                        'type' => 'group',
                        'group_id' => SIMAHtml::uniqid(),
                        'group_name' => Yii::t('HRModule.QualificationLevel', 'QualificationLevels'),
                        'columns' => [
                            $name_column_display,
                            $code_column_display,
                            $description_column_display
                        ],
                        'items' => $items
                    ]
                ]
            ], true);
        }
        
        $this->sendEndEvent([
            'html' => $html
        ]);
    }
}