<?php

return array(
    array(
        'name' => 'introduction_into_business_program_document_type',
        'display_name' => Yii::t('HRModule.Configuration', 'IntroductionIntoBusinessProgramDocumentTypeConfiguration'),
        'type' => 'searchField',
        'type_params' => array(
            'modelName' => 'DocumentType',
        ),
        'user_change_forbidden' => true,
        'description' => Yii::t('HRModule.Configuration', 'IntroductionIntoBusinessProgramDocumentTypeConfigurationDescription'),
    ),
    array(
        'name' => 'absences',
        'display_name' => 'Odsustva',
        'type' => 'group',
        'type_params' => array(
            array(
                'name' => 'annual_leave',
                'display_name' => 'Godišnji odmor - Zahtev',
                'type' => 'searchField',
                'type_params' => array(
                    'modelName' => 'DocumentType',
                ),
                'value_not_null' => false,
                'condition' => array(
                    'absence' => array(
                        'type' => 0
                    )
                ),
                'user_change_forbidden' => true,
                'description' => 'Tip dokumenta za zahtev za godišnji odmor.'
            ),
            array(
                'name' => 'sick_leave',
                'display_name' => 'Bolovanje - Doznaka',
                'type' => 'searchField',
                'type_params' => array(
                    'modelName' => 'DocumentType',
                ),
                'value_not_null' => false,
                'condition' => array(
                    'absence' => array(
                        'type' => 1
                    )
                ),
                'user_change_forbidden' => true,
                'description' => 'Tip dokumenta za doznaku za bolovanje.'
            ),
            array(
                'name' => 'days_off',
                'display_name' => 'Plaćeno odsustvo - Odobrenje',
                'type' => 'searchField',
                'type_params' => array(
                    'modelName' => 'DocumentType',
                ),
                'value_not_null' => false,
                'condition' => array(
                    'absence' => array(
                        'type' => 2
                    )
                ),
                'user_change_forbidden' => true,
                'description' => 'Tip dokumenta za odobrenje za plaćeno odsustvo.'
            ),
            array(
                'name' => 'annual_leave_decision',
                'display_name' => 'Rešenje o godišnjem odmoru',
                'type' => 'searchField',
                'type_params' => array(
                    'modelName' => 'DocumentType',
                ),
                'value_not_null' => false,
                'user_change_forbidden' => true,
                'description' => 'Tip dokumenta za rešenje o godišnjem odmoru.'
            )
        )
    ),
    array(
        'name' => 'company_license_decision_document_type_id',
        'display_name' => Yii::t('HRModule.Configuration', 'CompanyLicenseDecisionDocumentTypeId'),
        'type' => 'searchField',
        'type_params' => array(
            'modelName' => 'DocumentType',
        ),
        'user_change_forbidden' => true,
        'description' => Yii::t('HRModule.Configuration', 'CompanyLicenseDecisionDocumentTypeId'),
    ),
    array(
        'name' => 'safe_evd_2',
        'display_name' => 'Lekarski izvestaj zaposlenih',
        'type' => 'searchField',
        'type_params' => array(
            'modelName' => 'DocumentType',
        ),
        'value_not_null' => false,
        'condition' => array(
            'medical_report' => array()
        ),
        'user_change_forbidden' => true,
        'description' => 'Tip dokumenta za Lekarski izvestaj zaposlenih.'
    ),
    array(
        'name' => 'license',
        'display_name' => 'Radna licenca zaposlenih',
        'type' => 'searchField',
        'type_params' => array(
            'modelName' => 'DocumentType',
        ),
        'value_not_null' => false,
        'condition' => array(
            'work_license' => array()
        ),
        'user_change_forbidden' => true,
        'description' => 'Tip dokumenta za radnu licencu zaposlenih.'
    ),
    array(
        'name' => 'working_licence_certificate',
        'display_name' => 'Potvrda licence',
        'type' => 'searchField',
        'type_params' => array(
            'modelName' => 'DocumentType',
        ),
        'value_not_null' => false,
        'condition' => array(
            'working_licence_certificate' => array()
        ),
        'user_change_forbidden' => true,
        'description' => 'Tip dokumenta za potvrdu licence.'
    ),
    array(
        'name' => 'training_report_file',
        'display_name' => 'Beleska sa obuke',
        'type' => 'searchField',
        'type_params' => array(
            'modelName' => 'DocumentType',
        ),
        'value_not_null' => false,
        'condition' => array(
            'training_report_file_id' => array()
        ),
        'user_change_forbidden' => true,
        'description' => 'Tip dokumenta za belesku sa obuke.'
    ),
    array(
        'name' => 'notice_of_mobbing',
        'display_name' => 'Obaveštenje na mobing',
        'type' => 'searchField',
        'type_params' => array(
            'modelName' => 'DocumentType',
        ),
        'value_not_null' => false,
        'condition' => array(
            'notice_of_mobbing' => array()
        ),
        'user_change_forbidden' => true,
        'description' => 'Obaveštenje na mobing.'
    ),
    array(
        'name' => 'close_work_contract',
        'display_name' => 'Ugovor o prestanku radnog odnosa',
        'type' => 'searchField',
        'type_params' => array(
            'modelName' => 'DocumentType',
        ),
        'value_not_null' => false,
        'condition' => array(
            'close_work_contract' => array()
        ),
        'user_change_forbidden' => true,
        'description' => 'Tip dokumenta za Ugovor o prestanku radnog odnosa.'
    ),
    array(
        'name' => 'work_contract',
        'display_name' => 'Ugovori o radu',
        'type' => 'searchField',
        'type_params' => array(
            'modelName' => 'DocumentType',
        ),
        'value_not_null' => false,
        'value_is_array' => true,
        'condition' => array(
            'work_contract' => array()
        ),
        'user_change_forbidden' => true,
        'description' => 'Tipovi dokumenata za ugovore o radu.'
    ),
    [
        'name' => 'medical_examination_referral_document_type_id',
        'display_name' => Yii::t('HRModule.MedicalExaminationReferral', 'MedicalExaminationReferralDocumentType'),
        'type' => 'searchField',
        'type_params' => [
            'modelName' => 'DocumentType'
        ],
        'user_change_forbidden' => true,
        'description' => Yii::t('HRModule.MedicalExaminationReferral', 'MedicalExaminationReferralDocumentTypeDesc'),
    ],
    [
        'name' => 'safe_evd3_document_type_id',
        'display_name' => Yii::t('HRModule.SafeEvd3', 'SafeEvd3DocumentType'),
        'type' => 'searchField',
        'type_params' => [
            'modelName' => 'DocumentType'
        ],
        'user_change_forbidden' => true,
        'description' => Yii::t('HRModule.SafeEvd3', 'SafeEvd3DocumentType'),
    ],
    [
        'name' => 'safe_evd2_document_type_id',
        'display_name' => Yii::t('HRModule.SafeEvd2', 'SafeEvd2DocumentType'),
        'type' => 'searchField',
        'type_params' => [
            'modelName' => 'DocumentType'
        ],
        'user_change_forbidden' => true,
        'description' => Yii::t('HRModule.SafeEvd2', 'SafeEvd2DocumentType'),
    ],
    [
        'name' => 'safe_evd8_document_type_id',
        'display_name' => Yii::t('HRModule.SafeEvd8', 'SafeEvd8DocumentType'),
        'type' => 'searchField',
        'type_params' => [
            'modelName' => 'DocumentType'
        ],
        'user_change_forbidden' => true,
        'description' => Yii::t('HRModule.SafeEvd8', 'SafeEvd8DocumentType'),
    ],
    [
        'name' => 'safe_evd9_document_type_id',
        'display_name' => Yii::t('HRModule.SafeEvd9', 'SafeEvd9DocumentType'),
        'type' => 'searchField',
        'type_params' => [
            'modelName' => 'DocumentType'
        ],
        'user_change_forbidden' => true,
        'description' => Yii::t('HRModule.SafeEvd9', 'SafeEvd9DocumentType'),
    ],
);

