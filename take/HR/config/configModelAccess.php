<?php

return array(
    'CompanyLicense' => array(
        'modelCompanyLicenseFormOpen',
        'modelCompanyLicenseDelete',
    ),
    'CompanyLicenseToCompany' => array(
        'modelCompanyLicenseToCompanyRemovePersons',
        'modelCompanyLicenseToCompanyConfirm',
        'modelCompanyLicenseToCompanyRevert'
    ),
    'Absence' => [
        'modelAbsenceGenerateDocument',
        'modelAbsenceConfirm',
        'modelAbsenceRevertConfirm',
        'modelAbsenceChangeType',
        'modelAbsenceDelete'
    ],
    'AnnualLeaveDecision' => [
        'modelAnnualLeaveDecisionGenerateDocument'
    ],
    'CollectiveAnnualLeave' => [
        'modelCollectiveAnnualLeaveRevertConfirm'
    ],
    'WorkContract'=>array(
        'modelWorkContractAdd',
        'modelWorkContractEdit',
        'modelWorkContractEditSigned',
        'modelWorkContractDelete'
    ),
);