<?php

return array(
    'display_name' => Yii::t('HRModule.Configuration', 'HR'),
    array(
        'name' => 'template_safe_evd6_report_id',
        'display_name'=>'Template za evidenciju zaposlenih osposobljenim za rad',
        'type'=>'searchField',
        'type_params' => array(
            'modelName'=>'Template',
        ),
        'value_not_null'=>false,
        'description'=>'Template za evidenciju zaposlenih osposobljenim za bezbedan i zdrav rad'
    ),
    array(
        'name' => 'template_medical_referral_id',
        'display_name'=>'Template za uput na lekarski pregled',
        'type'=>'searchField',
        'type_params' => array (
            'modelName'=>'Template',
        ),
        'value_not_null'=>false,
        'description'=>'Template za uput na lekarski pregled'
    ),
    array(
        'name' => 'annual_leave_template',
        'display_name'=>'Template za godisnji odmor',
        'type'=>'searchField',
        'type_params' => array(
            'modelName'=>'Template',
        ),
        'description'=>'Template za godisnji odmor'
    ),
    array(
        'name' => 'days_off_template',
        'display_name'=>'Template za plaćeno odsustvo',
        'type'=>'searchField',
        'type_params' => array(
            'modelName'=>'Template',
        ),        
        'description'=>'Template za plaćeno odsustvo'
    ),
    array(
        'name' => 'annual_leave_decision_template',
        'display_name'=>'Template za rešenje o godišnjem odmoru',
        'type'=>'searchField',
        'type_params' => array(
            'modelName'=>'Template',
        ),        
        'description'=>'Template za rešenje o godišnjem odmoru'
    ),
    array(
        'name' => 'introduction_into_business_program_template',
        'display_name' => Yii::t('HRModule.Configuration', 'IntroductionIntoBusinessProgramTemplateConfiguration'),
        'type' => 'searchField',
        'type_params' => array(
            'modelName' => 'Template',
        ),
        'user_change_forbidden' => true,
        'description' => Yii::t('HRModule.Configuration', 'IntroductionIntoBusinessProgramTemplateConfigurationDescription'),
    ),
    array(
        'name' => 'company_license_to_company_change_receiver_id',
        'display_name' => 'Primalac obaveštenja kada dodje do promene izmedju velikih licenci i kompanija',
        'type' => 'searchField',
        'type_params' => array(
            'modelName' => 'User',
        ),        
        'description' => 'Primalac obaveštenja kada dodje do promene izmedju velikih licenci i kompanija',
    ),
    array(
        'name' => 'annual_leave_days_number_round_type',
        'user_changable' => true,
        'display_name'=>'Način zaokruživanja dana kod srazmernog dela godišnjeg odmora',
        'type'=>'dropdown',
        'type_params'=>array(
          array(
              'title'=>'Gore(npr. 1.66~2)',
              'value'=>'UP'
          ),
          array(
              'title'=>'Dole(npr. 1.66~1)',
              'value'=>'DOWN'
          )  
        ),
        'default'=>'UP',
        'description'=>'Način zaokruživanja dana kod srazmernog dela godišnjeg odmora'
    ),
    array(
        'name' => 'hr_documentation_email',
        'display_name' => 'Email sa kog se šalju hr dokumenti',
        'type' => 'searchField',
        'type_params' => array(
            'modelName' => 'EmailAccount',
        ),        
        'description' => 'Email sa kog se šalju hr dokumenti',
    ),
    [
        'name' => 'revert_confirmed_absence_on_national_event_add',
        'user_changable' => false,
        'display_name' => Yii::t('HRModule.Configuration', 'RevertConfirmedAbsenceOnNationalEventAdd'),
        'type'=>'dropdown',
        'type_params' => [
            [
                'title' => Yii::t('BaseModule.Common', 'Yes'),
                'value' => 'true'
            ],
            [
                'title' => Yii::t('BaseModule.Common', 'No'),
                'value' => 'false'
            ]
        ],
        'default' => 'false',
        'description' => Yii::t('HRModule.Configuration', 'RevertConfirmedAbsenceOnNationalEventAddDescription')
    ],
    array(
        'name' => 'fixed_work_contract_template',
        'display_name'=>'Template za ugovor o radu na odredjeno',
        'type'=>'searchField',
        'type_params' => array(
            'modelName'=>'Template',
        ),
        'value_not_null'=>false,
        'description'=>'Template za ugovor o radu na odredjeno'
    ),
    array(
        'name' => 'indefinite_work_contract_template',
        'display_name'=>'Template za ugovor o radu na neodredjeno',
        'type'=>'searchField',
        'type_params' => array(
            'modelName'=>'Template',
        ),
        'value_not_null'=>false,
        'description'=>'Template za ugovor o radu na neodredjeno'
    ),
    array(
        'name' => 'close_work_contract_template',
        'display_name'=>'Template za raskid ugovora o radu',
        'type'=>'searchField',
        'type_params' => array(
            'modelName'=>'Template',
        ),
        'value_not_null'=>false,
        'description'=>'Template za raskid ugovora o radu'
    ),
    [
        'name' => 'benefited_work_experience_override',
        'display_name' => Yii::t('HRModule.Configuration', 'BenefitedWorkExperience'),
        'type'=>'dropdown',
        'type_params' => [
            [
                'title' => Yii::t('BaseModule.Common', 'Yes'),
                'value' => 'true'
            ],
            [
                'title' => Yii::t('BaseModule.Common', 'No'),
                'value' => 'false'
            ]
        ],
        'default' => 'false',
        'description' => Yii::t('HRModule.Configuration', 'BenefitedWorkExperienceDescription')
    ],
    [
        'name' => 'bzr',
        'display_name' => Yii::t('HRModule.SafeEvd3','BZR'),
        'type' => 'group',
        'type_params' => [
            [
                'name' => 'safe_evd_3_report',
                'display_name' => Yii::t('HRModule.SafeEvd3','InjuriesRecords'),
                'type' => 'group',
                'type_params' => [
                    [
                        'name' => 'registration_number_of_contributors',
                        'display_name' => Yii::t('HRModule.SafeEvd3','RegistrationTaxpayerNumber'),
                        'description' => Yii::t('HRModule.SafeEvd3','RegistrationTaxpayerNumber'),
                        'type' => 'textField'
                    ],
                    [
                        'name' => 'company_in_segment',
                        'display_name' => Yii::t('HRModule.SafeEvd3','CompanyBelongsToSegment'),
                        'description' => Yii::t('HRModule.SafeEvd3','ChooseCompanyBelongsToSegment'),
                        'type'=>'dropdown',
                        'type_params' => [
                            [
                                'title' => 'I. '.Yii::t('HRModule.SafeEvd3','EmployerData'),
                                'value' => 1
                            ],
                            [
                                'title' => 'II. '.Yii::t('HRModule.SafeEvd3','BeneficiaryEmployerData'),
                                'value' => 2
                            ]
                        ],
                        'default' => 1,
                    ]
                ]
            ]
        ]
    ]
);