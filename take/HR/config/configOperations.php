<?php

return array(
    'HRSickLeaves' => array(
        'bizrule' => 'true',
//            'tql_limits'=>'',
        'description' => 'moze da menja,dodaje i potvrdjuje bolovanja za zaposlene'
    ),
    'HRWorkContractRead' => array(
        'bizrule' => 'true',
//            'tql_limits'=>'',
        'description' => 'moze da pregleda ugovore o radu'
    ),
    'HRWorkContractWrite' => array(
        'bizrule' => 'true',
        'description' => 'moze da menja ugovore o radu i da im dodaje pozicije'
    ),
    'HRWorkContractTakeCare' => array(
        'bizrule' => 'true',
//            'tql_limits'=>'',
        'description' => 'Osoba koja vodi racuna o ugovorima -> pojavice se ugovori u kalendaru'
    ),
    'HRLicenceWrite' => array(
        'bizrule' => 'true',
//            'tql_limits'=>''
    ),
    'HRLicenceRead' => array(
//            'bizrule'=>'true',
//            'tql_limits'=>''
    ),
    'HRLicenceShowInCalendar' => array(
        'description' => 'Osoba koja vodi racuna o Radnim licencam -> pojavice se licence u kalendaru i warning za isticanje'
    ),
    'HRLicenceWarning' => array(
        'description' => 'Pokazuje se warning ukoliko je istekla licenca za neku osobu'
    ),
    'CompanySchemaWrite' => array(
        'bizrule' => 'true',
//            'tql_limits'=>''
    ),
    'HRMedicalExaminationsTakeCare' => array(
        'bizrule' => 'true',
        'description' => 'Osoba koja vodi racuna o lekarskim pregledima -> pojavice se u kalendaru'
    ),
    'HRSafeEvd6TakeCare'=> array(
        'bizrule' => 'true',
        'description' => 'Osoba koja vodi racuna o evidencijama o zaposlenima osposobljenim za rad -> pojavice se u kalendaru'
    ),
    'HRInsufficientDataForEmployees' => array(
        'description' => 'Pokazuje se warning ukoliko postoje osobe koje imaju nepotpune podatke'
    ),
    'HRInsufficientDataForMunicipalities' => array(
        'description' => 'Pokazuje se warning ukoliko postoje opstine koje imaju nepotpune podatke'
    ),
    'CompanyLicensesNotConnected' => [
        'description' => 'Prikazuje warn sa spiskom svih velikih licenci koje nisu konektovane(nemaju popunjene zaposlene i reference)'
    ],
    'CompanyLicenseToCompanyDifferences' => [
        'description' => 'Prikazuje warn ako postoje razlike izmedju velikih licenca iz poslednjeg resenja i velikih licenca koje poseduje firma'
    ],
    'HRPositionCompetitionSeeAll' => [
        'description' => 'Moze da vidi ocene na kojima nije komisija'
    ],
    'HRPositionCompetitionAdministration' => [
        'description' => 'Administrator, moze da vidi sve konkurse, ali ne i njihove ocene'
    ],
    'HREmployeeSlava' => [
        'description' => Yii::t('HRModule.Absence', 'HREmployeeSlavaPrivilege')
    ],
    'HRDeliveryPrintConfirm' => [
        'description' => 'Privilegija ko može da potvrdi da je hr dokumentacija dostavljena štampanjem'
    ],
    'LegalWorkPositionManager' => [
        'description' => 'Osoba koja moze da vidi tab radne pozicije u profilu. U njemu se nalazi spisak radnih pozicija te osobe i mogucnost dodavanja'
    ],
    'HROccupation' => [
        'description' => Yii::t('HRModule.Occupation','HROccupationPrivilege')
    ]
);
