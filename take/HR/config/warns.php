<?php

return [
    'WARN_LICENSE_NOCERTIFICATE' => [
        'title' => Yii::t('MainMenu','NumberOfLicenceThatAreExpiring'),
        'onclick' => ['sima.mainTabs.openNewTab','guitable','HR_11'],        
        'countFunc' => function($user) {
            $cnt = 0;
            if (Yii::app()->authManager->checkAccess('HRLicenceWarning',$user->id))
            {
                $criteria = new SIMADbCriteria(array(
                    'Model'=>'PersonToWorkLicense',
                    'model_filter'=>array(
                        'expiration_period'=>0
                    )
                ));
                $cnt = PersonToWorkLicense::model()->count($criteria);
            }
            
            return $cnt;
        }
    ],
    'WARN_CONFIRMED_ABSENCES_WITHOUT_DOCUMENT' => [
        'title' => 'Odsustva koja su potvrdjena ali nemaju setovan dokument',
        'onclick' => ['sima.mainTabs.openNewTab','guitable','HR_9'],        
        'countFunc' => function($user) {
            $cnt = 0;
            if (Yii::app()->authManager->checkAccess('LegalEmployeeAbsences',$user->id))
            {
                $criteria = new SIMADbCriteria([
                    'Model'=>'Absence',
                    'model_filter'=>[
                        'confirmed' => true,
                        'document'=>[
                            'ids'=>'null'
                        ]
                    ]
                ]);
                $cnt = Absence::model()->count($criteria);
            }
            
            return $cnt;
        }
    ],
    'WARN_COMPANY_LICENSES_NOT_CONNECTED' => [
        'title' => 'Velike licence koje nisu konektovane(nemaju popunjene zaposlene i reference)',
        'onclick' => ['sima.mainTabs.openNewTab','HR/WorkLicense/companyLicenseByCompaniesNotConnected'],        
        'countFunc' => function($user) {
            $cnt = 0;
            if (Yii::app()->authManager->checkAccess('CompanyLicensesNotConnected',$user->id))
            {
                $criteria = new SIMADbCriteria([
                    'Model' => 'CompanyLicenseToCompany',
                    'model_filter' => [                        
                        'filter_scopes'=>['notConnected']
                    ],
                ]);
                $cnt = CompanyLicenseToCompany::model()->count($criteria);
            }
            
            return $cnt;
        }
    ],
    'WARN_COMPANY_LICENSES_TO_COMPANIES_DIFFERENCES' => [
        'title' => 'Broj velikih licenci koje poseduje firma a ne slažu se sa velikim licencama iz poslednjeg rešenja',
        'onclick' => ['sima.mainTabs.openNewTab','HR/WorkLicense/companyLicenseToCompanyDifferences'],        
        'countFunc' => function($user) {
            $cnt = 0;
            if (Yii::app()->authManager->checkAccess('CompanyLicenseToCompanyDifferences',$user->id))
            {
                $cnt = count(CompanyLicenseDecision::getCompanyLicenseDifferencesByDecision());
            }
            
            return $cnt;
        }
    ],
    'WARN_DIRECTOR_NOT_CONFIRMED_ABSENCES' => [
        'title' => Yii::t('HRModule.Absence', 'DirectorNotConfirmedAbsences'),
        'onclick' => ['sima.mainTabs.openNewTab','guitable', "HR_0"],        
        'countFunc' => function($user) {
            $cnt = 0;
            if ($user->person->isDirector())
            {
                $cnt = Absence::model()->count(new SIMADbCriteria([
                    'Model'=>'Absence',
                    'model_filter'=>[
                        'confirmed'=>'null',
                        'filter_scopes'=>['forBossSubordinates'=>[$user->id]]
                    ]
                ]));
            }
            
            return $cnt;
        }
    ],
    'WARN_NOT_REVIEWED_EMPLOYEE_CANDIDATES' => [
        'title' => Yii::t('HRModule.Warns','NotReviewedEmployeeCandidates'),
        'onclick' => ['sima.dialog.openActionInDialog','HR/WPCompetitions/index', [
            'post_params' => [
                'work_position_competitions_filter' => [
                    'work_position_competition_juries' => [
                        'jury' => ['ids' => Yii::app()->user->id]
                    ],
                    'filter_scopes' => [
                        'notGradedByUser' => Yii::app()->user->id
                    ],
                    'active' => true
                ]
            ]
        ]],        
        'countFunc' => function($user) {
//            return EmployeeCandidateToWorkPositionCompetition::model()->countByModelFilter([
//                'work_position_competition' => [
//                    'work_position_competition_juries' => [
//                        'jury' => [
//                            'ids' => $user->id
//                        ]
//                    ],
//                    'active' => true
//                ],
//                'filter_scopes' => [
//                    'notGradedByUser' => $user->id
//                ]
//            ]);
            return EmployeeCandidateToWorkPositionCompetition::model()->count([
                'scopes' => [
                    'notGradedByUser' => $user->id
                ],
                'model_filter' => [
                    'work_position_competition' => [
                        'work_position_competition_juries' => [
                            'jury' => [
                                'ids' => $user->id
                            ]
                        ],
                        'active' => true
                    ]
                ]
            ]);
        }
    ],
    'WARN_WORK_CONTRACT_WITH_OUT_POSITION' => [
        'title' => 'Broj ugovora o radu koji nemaju radne pozicije',
        'onclick' => ['sima.mainTabs.openNewTab','HR/workContract/workContractWithOutWorkPosition'],        
        'countFunc' => function($user) {
            $cnt = 0;
            if (Yii::app()->authManager->checkAccess('HRWorkContractTakeCare',$user->id))
            {
                $cnt = WorkContract::model()->count('work_position_id is null');
            }
            
            return $cnt;
        }
    ],
    'WARN_UNSIGNED_WORK_CONTRACTS' => [
        'title' => Yii::t('MainMenu', 'UnsignedWorkContracts'),
        'onclick' => ['sima.mainTabs.openNewTab','guitable','HR_12'],        
        'countFunc' => function($user) {
            $cnt = 0;
            if (Yii::app()->authManager->checkAccess('HRWorkContractTakeCare',$user->id))
            {
                $cnt = WorkContract::model()->count(new SIMADbCriteria([
                    'Model' => WorkContract::model(),
                    'model_filter'=>[
                        'signed' => false
                    ]
                ]));
            }
            
            return $cnt;
        }
    ],
    'WARN_NOT_RETURNED_WORK_CONTRACTS' => [
        'title' => Yii::t('HRModule.WorkContract', 'NotReturnedWorkContracts'),
        'onclick' => ['sima.mainTabs.openNewTab','guitable','HR_10'],        
        'countFunc' => function($user) {
            $cnt = 0;
            if (Yii::app()->authManager->checkAccess('HRWorkContractTakeCare',$user->id))
            {
                $cnt = WorkContract::model()->count(new SIMADbCriteria([
                    'Model' => WorkContract::model(),
                    'model_filter'=>[
                        'file' => [
                            'filing' => [
                                'returned' => false
                            ]
                        ]
                    ]
                ]));
            }
            
            return $cnt;
        }
    ],
    'WARN_DOCUMENT_UNSET_WORK_CONTRACT' => [
        'title' => 'Broj fajlova kojima treba dodati ugovora o radu ili aneks ugovora o radu',
        'onclick' => ['sima.mainTabs.openNewTab','guitable','files_21'],        
        'countFunc' => function($user) {
            $cnt = 0;
            if (Yii::app()->authManager->checkAccess('HRWorkContractTakeCare',$user->id))
            {
                $document_types = Yii::app()->configManager->get('HR.work_contract', false);
                if (count($document_types)>0)
                {
                    $document_types_temp=  implode(",", $document_types);
                    $work_contracts_table = WorkContract::model()->tableName();
                    $cnt = File::model()->count("t.document_type_id in (".$document_types_temp.")"
                            . "  and t.id not in ("
                              . "select id from $work_contracts_table"
                              . ")");
                }
            }
            
            return $cnt;
        }
    ],
    'WARN_DOCUMENT_UNSET_CLOSE_WORK_CONTRACT' => [
        'title' => 'Broj fajlova kojima treba dodati prekid radnog odnosa',
        'onclick' => ['sima.mainTabs.openNewTab','guitable','files_22'],        
        'countFunc' => function($user) {
            $cnt = 0;
            if (Yii::app()->authManager->checkAccess('HRWorkContractTakeCare',$user->id))
            {
                $document_type = Yii::app()->configManager->get('HR.close_work_contract', false);
                if (!empty($document_type))
                {
                    $close_work_contracts_table = CloseWorkContract::model()->tableName();
                    $cnt = File::model()->count("t.document_type_id in (".$document_type.")"
                            . "  and t.id not in ("
                              . "select id from $close_work_contracts_table"
                              . ")");                    
                }
            }
            
            return $cnt;
        }
    ],
    'WARN_WORK_CONTRACTS_WITH_DUPLICATED_START_DATE' => [
        'title' => Yii::t('HRModule.WorkContract', 'WorkContractsWithDuplicatedStartDate'),
        'onclick' => ['sima.mainTabs.openNewTab','guitable','HR_14'],
        'countFunc' => function($user) {
            $cnt = 0;
            if (Yii::app()->authManager->checkAccess('HRWorkContractTakeCare',$user->id))
            {
                $cnt = WorkContract::model()->count([
                    'model_filter' => [
                        'filter_scopes' => 'hasDuplicatedStartDate'
                    ]
                ]);
            }
            
            return $cnt;
        }
    ],
];

