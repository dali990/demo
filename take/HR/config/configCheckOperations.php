<?php

return [
    'checkIsJury' => array(
        'bizrule' => 'Yii::app()->user->model->checkIsJury()',
        'description' => 'Proverava da li je ulogovani korisnik bar na jednom konkursu ziri'
    ),
];
