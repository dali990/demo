<?php

return array(
    'menuShowRecords'=>array(        
        'menuShowRecords_iks_points_review'
    ),
    'menuShowNationalEvents',
    'menuShowLegalEmployees' => [
        'menuShowLegalEmployeesAbsencesOverview',
    ],
    'menuShowHRIntroductionsIntoBusinessProgram'
);