<?php

class AbsenceComponent
{
    public static function CalculateNumberWorkDaysBetweenDates(Absence $absence)
    {        
        if(!isset($absence->begin))
        {
            throw new Exception(Yii::t('HRModule.Absence', 'EmptyBegin'));
        }
        if(!isset($absence->end))
        {
            throw new Exception(Yii::t('HRModule.Absence', 'EmptyEnd'));
        }
        
        $startDay = Day::getByDate($absence->begin);
        $endDay = Day::getByDate($absence->end);

        $days = Day::getBetweenDates($startDay, $endDay);
        
        $result_number = 0;
                
        foreach ($days as $day)
        {
            if(
                (
                    $absence->type === Absence::$SICK_LEAVE 
                    && $absence->subtype === AbsenceSickLeave::$SUBTYPE_SICK_LEAVE_OVER_30
                    && $day->IsWeekend === false
                ) 
                || 
                (
                    $absence->subtype !== AbsenceSickLeave::$SUBTYPE_SICK_LEAVE_OVER_30 
//                    && $absence->subtype !== AbsenceFreeDays::$SUBTYPE_PAID_LEAVE_SLAVA 
                    && $day->IsRegularWorkDay === true
                )
//                || 
//                (
//                    $absence->subtype === AbsenceFreeDays::$SUBTYPE_PAID_LEAVE_SLAVA
//                    && $absence->employee->have_slava 
//                    && isset($absence->employee->slava)
//                    && $absence->employee->slava->getIsOnDay($day)
//                )
            )
            {
                $result_number++;
            }
        }
        
        return $result_number;
    }
}
