<?php
class SafeEvd3Report extends SIMAReport
{
    /**
     * Vraca nule samo ako polje u nizu nije prazno (nula ili prazan string)
     * @var type 
     */
    public static $LEADING_ZERO_IF_VALUE = 'LEADING_ZERO_IF_VALUE';
    /**
     * Vraca nule samo ako polje u nizu nije prazan string
     * @var type 
     */
    public static $LEADING_ZERO_IF_VALUE_OR_ZERO = 'LEADING_ZERO_IF_VALUE_OR_ZERO';
    /**
     * Vraca nule za sva svako prazno polje u nizu
     * @var type 
     */
    public static $LEADING_ZERO = 'LEADING_ZERO';
    public $segments = [];
    protected $css_file_name = 'css.css';
    private $validate_segments = ['segment1', 'segment4', 'segment5', 'segment6', 'employer_segment'];
    
    public function __construct($params = []) 
    {
        parent::__construct($params);
        $this->prepareSegments();
    }
    
    private function prepareSegments()
    {
        $save_evd_3 = $this->params['save_evd_3'];
        $company_segment = intval(Yii::app()->configManager->get('HR.bzr.safe_evd_3_report.company_in_segment', false));
        $occupation_code = $save_evd_3->employee->last_work_contract->occupation->code ?? '';
        $occupation_code = str_replace('.', '', $occupation_code);
        $source_injury = $save_evd_3->source_injury->code ?? '';
        $source_injury = str_replace('.', '', $source_injury);
        $qualification_code = $save_evd_3->employee->person->qualification_level->code ?? '';
        $age = "";
        if(!empty($save_evd_3->employee->person->JMBG))
        {
            $age = SafeEvd3Report::getCode('age', $save_evd_3->employee->person->birthdate);
        }
        $country = "";
        $country_model = null;
        $employee_address_model = null;
        $employee_address = "";
        $employee_city_model = null;
        $employee_city_municipality = "";
        $address = $save_evd_3->employee->person->getmain_or_any_address();
        if(!empty($address))
        {
            $employee_city_model = $address->city;
            $country_model = $address->city->country;
            $country = $address->city->country->name;
            $employee_address_model = $address;
            $employee_address = $address->DisplayNameShort;
            $employee_city_municipality = $address->city->name;
            if(!empty($address->city->municipality))
            {
                $employee_city_municipality .= ", ".$address->city->municipality->name;
            }
        }
        $active_employee = Employee::model()->countByModelFilter([
            'active_contracts' => SIMAHtml::DbToUserDate(date('d.m.Y.'))
        ]);
        $form_filling_date = [
            'day' => '',
            'month' => '',
            'year' => ''
        ];
        if($save_evd_3->form_filling_date)
        {
            $date = date_parse_from_format("d.m.Y.", $save_evd_3->form_filling_date);
            $form_filling_date['day'] = $date['day'];
            $form_filling_date['month'] = $date['month'];
            $form_filling_date['year'] = $date['year'];
        }
        $hospital_address = $save_evd_3->hospital->DisplayName ?? '';
        $hospital = $save_evd_3->hospital;
        if(!empty($hospital) && !empty($hospital->getmain_or_any_address()))
        {
            $hospital_address .= "<br>".$hospital->getmain_or_any_address();
        }
        $injury_diagnosis = [0=>'',1=>'',2=>''];
        if($save_evd_3->injury_diagnosis)
        {
            $injury_diagnosis = str_split($save_evd_3->injury_diagnosis, 4);
        }
        $doctor_examined_date = [
            'day' => '',
            'month' => '',
            'year' => ''
        ];
        if($save_evd_3->doctor_examined_date)
        {
            $date = date_parse_from_format("d.m.Y.", $save_evd_3->doctor_examined_date);
            $doctor_examined_date['day'] = $date['day'];
            $doctor_examined_date['month'] = $date['month'];
            $doctor_examined_date['year'] = $date['year'];
        }
        $date_of_received_report = [
            'day' => '',
            'month' => '',
            'year' => ''
        ];
        if($save_evd_3->date_of_received_report)
        {
            $date = date_parse_from_format("d.m.Y.", $save_evd_3->date_of_received_report);
            $date_of_received_report['day'] = $date['day'];
            $date_of_received_report['month'] = $date['month'];
            $date_of_received_report['year'] = $date['year'];
        }
        $violation_date = [
            'day' => '',
            'month' => '',
            'year' => ''
        ];
        if($save_evd_3->violation_date)
        {
            $date = date_parse_from_format("d.m.Y.", $save_evd_3->violation_date);
            $violation_date['day'] = $date['day'];
            $violation_date['month'] = $date['month'];
            $violation_date['year'] = $date['year'];
        }
        $eyewitness_address = "X";
        $eyewitness_person = $save_evd_3->eyewitness;       
        if(!empty($eyewitness_person))
        {
            $ea = $eyewitness_person->getmain_or_any_address();
            $eyewitness_person = $eyewitness_person->DisplayName;
            if(!empty($ea))
            {
                $eyewitness_address = $ea;
            }
        }
        else
        {
            $eyewitness_person = "X";
        }
        //Filtriranje starih evidencija
        $type_injury = $save_evd_3->type_injury;
        if(in_array($save_evd_3->type_injury, SafeEvd3Lists::$injury_type_old_list))
        {
            $type_injury = '';
        }
        $mark_injury = $save_evd_3->mark_injury;
        if(in_array($save_evd_3->mark_injury, SafeEvd3Lists::$mark_injury_old_list))
        {
            $mark_injury = '';
        }
        
        $injury_location_address_model = null;
        $injury_location_address = '';
        $injury_location_municipality_model = null;
        $injury_location_municipality_bzr_code = '';
        $injury_location_settlement_model = null;
        $injury_location_settlement_bzr_code = '';
        $injury_location_country_model = null;
        $injury_location_country_numeric_code = '';
        if(!empty($save_evd_3->injury_location_address))
        {
            $injury_location_address_model = $save_evd_3->injury_location_address;
            $injury_location_address = $save_evd_3->injury_location_address->DisplayNameShort;
            if(!empty($save_evd_3->injury_location_address->city->municipality))
            {
                $injury_location_municipality_model = $save_evd_3->injury_location_address->city->municipality;
                $injury_location_municipality_bzr_code = $save_evd_3->injury_location_address->city->municipality->bzr_code;
            }
            $injury_location_settlement_model = $save_evd_3->injury_location_address->city;
            $injury_location_settlement_bzr_code = $save_evd_3->injury_location_address->city->bzr_code;
            $injury_location_country_model = $save_evd_3->injury_location_address->city->country;
            $injury_location_country_numeric_code = $save_evd_3->injury_location_address->city->country->numeric_code;
        }
        
        $this->segments['translations'] = [
            'segment1_title' => Yii::t('HRModule.SafeEvd3','EmployerData'),
            'segment4_title' => Yii::t('HRModule.SafeEvd3','InjuredPersonInformation'),
            'segment5_title' => Yii::t('HRModule.SafeEvd3','InjuryInformationAtWork'),
            'segment6_title' => Yii::t('HRModule.SafeEvd3','BossOfInjured'),
            'segment7_title' => Yii::t('HRModule.SafeEvd3','EyewitnessInformation'),
            'employer_segment_title' => Yii::t('HRModule.SafeEvd3','EmployerData'),
            'segment8_title' => Yii::t('HRModule.SafeEvd3','DoctorWhoFirstExaminedTheInjured'),
            'company_name' => Yii::t('HRModule.SafeEvd3','NameOfEmployer'),
            'company_address' => Yii::t('HRModule.SafeEvd3','HeadquartersAndAddress'),
            'company_email' => Yii::t('HRModule.SafeEvd3','Email'),
            'company_registration_number_of_contributors' => Yii::t('HRModule.SafeEvd3','RegistrationTaxpayerNumber'),
            'company_pib' => Yii::t('HRModule.SafeEvd3','PIB'),
            'company_work_code' => Yii::t('HRModule.SafeEvd3','ActivityCode'),
            'total_employee' => Yii::t('HRModule.SafeEvd3','NumberOfEmployees'),
            'beneficiary_name' => Yii::t('HRModule.SafeEvd3','BeneficiaryEmployerName'),
            'beneficiary_headquarters_and_address' => Yii::t('HRModule.SafeEvd3','HeadquartersAndAddress'),
            'beneficiary_email' => Yii::t('HRModule.SafeEvd3','Email'),
            'beneficiary_id_number' => Yii::t('HRModule.SafeEvd3','IDNumber'),
            'beneficiary_pib' => Yii::t('HRModule.SafeEvd3','PIB'),
            'beneficiary_work_code' => Yii::t('HRModule.SafeEvd3','ActivityCode'),
            'beneficiary_person_full_name' => Yii::t('HRModule.SafeEvd3','NameAndSurname'),
            'beneficiary_person_id_number' => Yii::t('HRModule.SafeEvd3','IDNumberPerson'),
            'beneficiary_person_address' => Yii::t('HRModule.SafeEvd3','AddressAndPlace'),
            'beneficiary_person_phone' => Yii::t('HRModule.SafeEvd3','Phone'),
            'beneficiary_person_email' => Yii::t('HRModule.SafeEvd3','Email'),
            'employee_full_name' => Yii::t('HRModule.SafeEvd3','NameAndSurname'),
            'employee_id_number' => Yii::t('HRModule.SafeEvd3','IDNumberPerson'),
            'employee_gender' => Yii::t('HRModule.SafeEvd3','Gender'),
            'employee_age' => Yii::t('HRModule.SafeEvd3','AgeOfInjuredPerson'),
            'employee_street_and_number' => Yii::t('HRModule.SafeEvd3','Residence'),
            'employee_city_municipality' => Yii::t('HRModule.SafeEvd3','CityMunicipality'),
            'employee_country' => Yii::t('HRModule.SafeEvd3','Country'),
            'employee_citizenship' => Yii::t('HRModule.SafeEvd3','Citizenship'),
            'employee_country_name' => Yii::t('HRModule.SafeEvd3','CountryName'),
            'employment_status' => Yii::t('HRModule.SafeEvd3','EmploymentStatus'),
            'occupation' => Yii::t('HRModule.SafeEvd3','Occupation'),
            'qualification' => Yii::t('HRModule.SafeEvd3','QulaificationLevel'),
            'work_exp' => Yii::t('HRModule.SafeEvd3','WorkExperienceWhereInjuryOccured'),
            'date_of_injury' => Yii::t('HRModule.SafeEvd3','DateOfInjury'),
            'time_of_injury' => Yii::t('HRModule.SafeEvd3','TimeOfInjury'),
            'beginning_working_hour' => Yii::t('HRModule.SafeEvd3','BeginningWorkingHour'),
            'injury_location_address' => Yii::t('HRModule.SafeEvd3','AddressStreetAndNumber'),
            'municipality' => Yii::t('HRModule.SafeEvd3','Municipality'),
            'settlement' => Yii::t('HRModule.SafeEvd3','Settlement'),
            'form_filling_settlement' => Yii::t('HRModule.SafeEvd3','Settlement'),
            'country' => Yii::t('HRModule.SafeEvd3','Country'),
            'commuting_accidents' => Yii::t('HRModule.SafeEvd3','CommutingAccidents'),
            'work_environment' => Yii::t('HRModule.SafeEvd3','WorkEnvironment'),
            'work_process' => Yii::t('HRModule.SafeEvd3','WorkProcess'),
            'specific_physical_activity' => Yii::t('HRModule.SafeEvd3','SpecificPhysicalActivity'),
            'source_injury' => Yii::t('HRModule.SourceInjury','SourceInjury'),
            'contact_injury' => Yii::t('HRModule.SafeEvd3','ContactInjury'),
            'mode_injury' => Yii::t('HRModule.ModeInjury','ModeInjury'),
            'increased_risk' => Yii::t('HRModule.SafeEvd3','InjuryAtWorkIncreasedRiskOrNot'),
            'boss_of_injured' => Yii::t('HRModule.SafeEvd3','NameAndSurname'),
            'eyewitness_full_name' => Yii::t('HRModule.SafeEvd3','NameAndSurname'),
            'eyewitness_address' => Yii::t('HRModule.SafeEvd3','ResidenceAddress'),
            'form_filling_date' => Yii::t('HRModule.SafeEvd3','FormFillingDate'),
            'hospital_address' => Yii::t('HRModule.SafeEvd3','HospitalAddressWhereInjuredChecked'),
            'injury_diagnosis' => Yii::t('HRModule.SafeEvd3','DiagnosisOfInjury'),
            'external_cause_of_injury' => Yii::t('HRModule.SafeEvd3','ExternalCauseOfInjury'),
            'type_injury' => Yii::t('HRModule.SafeEvd3','TypeOfInjury'),
            'injury_part_of_body' => Yii::t('HRModule.SafeEvd3','InjuredPartOfBody'),
            'mark_injury' => Yii::t('HRModule.SafeEvd3','MarkInjury'),
            'doctor_remarks' => Yii::t('HRModule.SafeEvd3','DoctorRemarks'),
            'mt3_days_unable_work' => Yii::t('HRModule.SafeEvd3','MT3DaysUnableWork'),
            'estimated_lost_days' => Yii::t('HRModule.SafeEvd3','EstimatedLostDays'),
            'doctor_examined_date' => Yii::t('HRModule.SafeEvd3','Date'),
            'doctor_examined_settlement' => Yii::t('HRModule.SafeEvd3','Settlement'),
            'date_of_received_report' => Yii::t('HRModule.SafeEvd3','DateOfReceivedReport'),
            'violation_date' => Yii::t('HRModule.SafeEvd3','ViolationDate'),
            'insurance_settlement' => Yii::t('HRModule.SafeEvd3','Settlement'),
        ];
        $this->segments['segment1'] = [
            'company_name' => '',
            'company_address' => '',
            'company_email' => '',
            'company_registration_number_of_contributors' => '',
            'company_pib' => '',
            'company_work_code' => '',
            'company_active_employee' => '',
        ];
        if($company_segment === 1)
        {
            $this->segments['segment1']['company_name'] = Yii::app()->company->DisplayName;
            $this->segments['segment1']['company_address'] = Yii::app()->company->getmain_or_any_address()->DisplayName;
            $this->segments['segment1']['company_email'] = Yii::app()->company->getmain_or_any_email()->DisplayName;
            $this->segments['segment1']['company_registration_number_of_contributors'] = Yii::app()->configManager->get('HR.bzr.safe_evd_3_report.registration_number_of_contributors', false);
            $this->segments['segment1']['company_pib'] = Yii::app()->company->PIB;
            $this->segments['segment1']['company_work_code'] = Yii::app()->company->work_code->code;
            $this->segments['segment1']['company_active_employee'] = $active_employee;
        }
        
        $this->segments['segment2'] = [
            'beneficiary_name' => '',
            'beneficiary_headquarters_and_address' => '',
            'beneficiary_email' => '',
            'beneficiary_id_number' => '',
            'beneficiary_pib' => '',
            'beneficiary_work_code' => '',
            'beneficiary_total_employee' => '',
        ];
        if($company_segment === 2)
        {
            $this->segments['segment2']['beneficiary_name'] = Yii::app()->company->DisplayName;
            $this->segments['segment2']['beneficiary_headquarters_and_address'] = Yii::app()->company->getmain_or_any_address()->DisplayName;
            $this->segments['segment2']['beneficiary_email'] = Yii::app()->company->getmain_or_any_email()->DisplayName;
            $this->segments['segment2']['beneficiary_id_number'] = Yii::app()->company->MB;
            $this->segments['segment2']['beneficiary_pib'] = Yii::app()->company->PIB;
            $this->segments['segment2']['beneficiary_work_code'] = Yii::app()->company->work_code->code;
            $this->segments['segment2']['beneficiary_total_employee'] = $active_employee;
        }
        $this->segments['segment3'] = [
            'beneficiary_person_full_name' => '',
            'beneficiary_person_id_number' => '',
            'beneficiary_person_address' => '',
            'beneficiary_person_phone' => '',
            'beneficiary_person_email' => '',
        ];
        $this->segments['segment4'] = [
            'employee_full_name' => $save_evd_3->employee->DisplayName,
            'employee_id_number' => $save_evd_3->employee->person->JMBG,
            'employee_gender' => $save_evd_3->employee->gender,
            'employee_age' => $age,
            'employee_street_and_number' => $employee_address,
            'employee_address_model' => $employee_address_model,
            'employee_city_model' => $employee_city_model,
            'employee_city_municipality' => $employee_city_municipality,
            'employee_country_model' => $country_model,
            'employee_country' => $country,
            'employee_citizenship' => $save_evd_3->citizenship,
            'employee_country_name' => $country,
            'employment_status' => $save_evd_3->employment_status,
            'occupation' => $occupation_code,
            'qualification' => $qualification_code,
            'work_exp_years' => $save_evd_3->work_exp_years,
            'work_exp_months' => $save_evd_3->work_exp_months,
            'work_exp_days' => $save_evd_3->work_exp_days,
        ];
        
        $this->segments['segment5'] = [
            'date_of_injury' => $save_evd_3->date ? date('Ymd', strtotime($save_evd_3->date)) : '',
            'time_of_injury' => $save_evd_3->date ? SafeEvd3Report::getCode('time', date('H:i:s', strtotime($save_evd_3->date))) : '',
            'beginning_working_hour' => $save_evd_3->beginning_working_hour,
            'injury_location_address_model' => $injury_location_address_model,
            'injury_location_address' => $injury_location_address,
            'injury_location_municipality_model' => $injury_location_municipality_model,
            'municipality' => $injury_location_municipality_bzr_code,
            'injury_location_settlement_model' => $injury_location_settlement_model,
            'settlement' => $injury_location_settlement_bzr_code,
            'injury_location_country_model' => $injury_location_country_model,
            'country' => $injury_location_country_numeric_code,
            'commuting_accidents' => $save_evd_3->commuting_accidents,
            'work_environment' => $save_evd_3->work_environment,
            'work_process' => $save_evd_3->work_process,
            'specific_physical_activity' => $save_evd_3->specific_physical_activity,
            'source_injury' => self::getCode('model', $save_evd_3->source_injury),
            'contact_injury' => $save_evd_3->contact_injury,
            'mode_injury' => self::getCode('model', $save_evd_3->mode_injury),
            'increased_risk' => $save_evd_3->increased_risk,
        ];
        $this->segments['segment6'] = [
            'boss_of_injured' => $save_evd_3->boss_of_injured->DisplayName ?? ''
        ];
        $this->segments['segment7'] = [
            'eyewitness_full_name' => $eyewitness_person,
            'eyewitness_address' => $eyewitness_address
        ];
        $this->segments['employer_segment'] = [
            'form_filling_date' => $form_filling_date,
            'form_filling_settlement' => $save_evd_3->form_filling_settlement->name ?? '',
        ];
        $this->segments['segment8'] = [
            'hospital' => $hospital_address,
            'injury_diagnosis' => $injury_diagnosis,
            'external_cause_of_injury' => $save_evd_3->external_cause_of_injury,
            'type_injury' => $type_injury,
            'injury_part_of_body' => $save_evd_3->injury_part_of_body,
            'mark_injury' => $mark_injury,
            'doctor_remarks' => $save_evd_3->doctor_remarks,
            'mt3_days_unable_work' => $save_evd_3->mt3_days_unable_work,
            'estimated_lost_days' => $save_evd_3->estimated_lost_days,
            'doctor_examined_date' => $doctor_examined_date,
            'doctor_examined_settlement' => $save_evd_3->doctor_examined_settlement->name ?? '',
            'date_of_received_report' => $date_of_received_report,
            'violation_date' => $violation_date,
            'insurance_settlement' => $save_evd_3->insurance_settlement->name ?? '',
            'unique_injury_number' => $save_evd_3->unique_injury_number,
        ];
    }

    protected function getModuleName()
    {
        return 'HR';
    }

    protected function getHTMLContent()
    {
        $save_evd_3 = $this->params['save_evd_3'];
        $company_segment = intval(Yii::app()->configManager->get('HR.bzr.safe_evd_3_report.company_in_segment', false));
        $leading_zero_for_wor_exp = SafeEvd3Report::$LEADING_ZERO;
        if($save_evd_3->work_exp_years === 0 && $save_evd_3->work_exp_months === 0 && $save_evd_3->work_exp_days === 0)
        {
            $leading_zero_for_wor_exp = SafeEvd3Report::$LEADING_ZERO_IF_VALUE;
        }
        
        return [
            'html' => $this->render('html', [
                'save_evd_3' => $save_evd_3,
                'company_segment' => $company_segment,
                'translations' => $this->segments['translations'], 
                'segment1' => $this->segments['segment1'], 
                'segment2' => $this->segments['segment2'], 
                'segment3' => $this->segments['segment3'], 
                'segment4' => $this->segments['segment4'], 
                'segment5' => $this->segments['segment5'], 
                'segment6' => $this->segments['segment6'], 
                'segment7' => $this->segments['segment7'], 
                'employer_segment' => $this->segments['employer_segment'], 
                'segment8' => $this->segments['segment8'], 
                'leading_zero_for_wor_exp' => $leading_zero_for_wor_exp
            ], true, false),
            'css_file_name' => 'css'
        ];        
    }
    
    /**
     * Proverava da li su popunjena polja za izabarane segmente
     * Struktura niza sa postojecim greskama
        Array
        (
            [segment4] => Array
                (
                    [0] => <br><br><b>Podaci o povređenom:</b>
                    [1] => Pol
                    [2] => Radno iskustvo na poslovima na kojima se povreda dogodila (GGMMDD)
                )

            [segment5] => Array
                (
                    [0] => <br><br><b>Podaci o povredi na radu:</b>
                    [1] => Datum kada se povreda dogodila<br>(GGGGMMDD)
                    [2] => Vreme kada se povreda dogodila
                )
        )
     */
    public function validateSegments()
    {
        //Polja koje treba preskociti u default validaciji kako bi se validirali custom
        $skip = ['work_exp_years', 'work_exp_months', 'work_exp_days', 'form_filling_date'];
        $error_msgs = [];
        foreach ($this->validate_segments as $segment_name) 
        {
            foreach ($this->segments[$segment_name] as $key => $value) 
            {
                if(in_array($key, $skip))
                {
                    continue;
                }
                
                if($value === '' || $value === null)
                {
                    $error_msgs[$segment_name][] = $this->segments['translations'][$key];
                }
            }
            // Custom validacija
            if($segment_name === 'segment4')
            {
                if(empty($this->segments['segment4']['work_exp_years']) && empty($this->segments['segment4']['work_exp_months']) && empty($this->segments['segment4']['work_exp_days']))
                {
                    $error_msgs['segment4'][] = $this->segments['translations']['work_exp'];
                }                
            }
            // Custom validacija
            if($segment_name === 'employer_segment')
            {
                $ffd = $this->segments['employer_segment']['form_filling_date'];
                if(empty($ffd['day']) && empty($ffd['month']) && empty($ffd['year']))
                {
                    $error_msgs['employer_segment'][] = $this->segments['translations']['form_filling_date'];
                }                
            }
            if(!empty($error_msgs[$segment_name]))
            {
                array_unshift($error_msgs[$segment_name], "<br><br><b>".$this->segments['translations'][$segment_name."_title"].":</b>");
            }
        }
        
        $warning = "";
        foreach ($error_msgs as $segment => $segment_msgs) 
        {
            if(!empty($segment_msgs))
            {
                $warning .= implode("<br>", $segment_msgs);               
            }
        }
        if($warning !== "")
        {
            $warning = Yii::t('HRModule.SafeEvd3','YouHaveNotFilledOutTheFollowingItems').$warning;
            Yii::app()->controller->raiseNote($warning);
        }
    }
    
    /**
     * Otvara formu ukoliko prosledjeni model nije null
     * @param type $model
     * @return string
     */
    public static function modelFormOpenWithCheck($model, $params=[]): string
    {
        if(!empty($model))
        {
            return SIMAHtml::modelFormOpen($model, $params);
        }
        else 
        {
            return '';
        }
    }
    
    /**
     * Formatiranje polja za prikaz niza karaktera
     * @param string|null $data string/integer
     * @param int $size broj polja za prikaz (ukolike je 1, upisuje ceo string/int u polje)
     * @param string $leading_zero popunjava prazna polja sa "0"
     * @return string
     */
    public static function arrayField($data, int $size = 0, string $leading_zero = ""): string
    {
        if($leading_zero === self::$LEADING_ZERO_IF_VALUE && $data == 0)
        {
            $data = "";
        }
        $array_data = str_split($data);//vraca niz karaktera
        $array_data_cnt = count($array_data);
        //Ukoliko imamo visak polja, definisemo njihov prikaz
        if($size > $array_data_cnt)
        {
            $empty_field = "";
            //Popunjavanje praznih polja sa 0 ukoliko nije prosledjen prazan podatak ili nula
            if($leading_zero === self::$LEADING_ZERO_IF_VALUE && ($data !== "" && $data != 0 && $data !== null))
            {
                $empty_field = 0;
            }
            //Popunjavanje praznih polja sa 0 ukoliko nije prosledjen prazan podatak
            else if ($leading_zero === self::$LEADING_ZERO_IF_VALUE_OR_ZERO && ($data !== "" && $data !== null))
            {
                $empty_field = 0;
            }
            //Uvek popunjava prazna polja sa 0
            else if ($leading_zero === self::$LEADING_ZERO)
            {
                $empty_field = 0;
            }
            for ($index = 0; $index < ($size-$array_data_cnt); $index++) 
            {
                array_unshift($array_data, $empty_field);
            }
        }
        //Upisivanje celovitog podatka ukoliko je difinasno samo jedno polje za prikaz
        else if ($size === 1)
        {
            $array_data = [$data];
        }
        $result = '';
        foreach ($array_data as $data) 
        {
            $result .= '<div class="safe-evd3-report-array-field">'.$data.'</div>';
        }
        return $result;
    }
    
    /**
     * Vraca sifru za prosledjenu vrednost
     * @param string|null $type
     * @param string|int $param
     * @return string|int
     */
    public static function getCode($type='', $param='') 
    {          
        switch ($type) {
            case 'gender':
                if($param === 'male')
                {
                    return 1;
                }
                else if ($param === 'female')
                {
                    return 2;
                }
                return '';
            case 'cnt_employees':
                if($param === null || $param === '') 
                {
                    return '';
                }
                if((int)$param === 0)
                {
                    return 0;
                }
                if($param < 10)
                {
                    return 1;
                }
                if($param < 50) 
                {
                    return 2;
                }
                if($param < 250)
                {
                    return 3;
                }
                if($param < 500) 
                {
                    return 4;
                }
                if($param >= 500) 
                {
                    return 5;
                }
                return '';
            case 'age':
                if($param === 0) 
                {
                    return 00;
                }
                if($param)
                {                       
                    $age = DateTime::createFromFormat('d.m.Y.', $param)
                    ->diff(new DateTime('now'))
                    ->y;
                    if($age < 10) 
                    {
                        return "0".$age;
                    }
                    if($age > 90)
                    {
                        return 98;
                    }
                    return $age;
                }
                else 
                {
                    return 99;
                }
                break;                
            case 'time':
                if(empty($param))
                {
                    return '';
                }
                return DateTime::createFromFormat("H:i:s", $param)->format("H");
            case 'model':
                if(!empty($param))
                {
                    $code = $param->code ?? '';
                    return str_replace('.', '', $code);
                }
                return '';
            default:
                break;
        }
    }

}