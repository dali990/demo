<?php

class WorkContractComponent extends SIMAComponent
{
    public static function getWorkExpBetweenTwoDates($startDateString, $endDateString, $haveWorkContractInDayBeforeThisStart=false)
    {        
        $worked_days = 0;
        
        $startdatetime = new DateTime($startDateString);
        $enddatetime = new DateTime($endDateString);
        
        if($startdatetime <= $enddatetime)
        {
            $startdatetime_first_day = (new DateTime($startDateString))->modify('first day of this month');
            $enddatetime_first_day = (new DateTime($endDateString))->modify('first day of next month');

            $start_date_day = (int)$startdatetime->format('d');
            $start_date_number_days = (int)$startdatetime->format('t');
            $end_date_day = (int)$enddatetime->format('d');
            $end_date_number_days = (int)$enddatetime->format('t');

            $period = new DatePeriod(
                $startdatetime_first_day,
                new DateInterval('P1M'),
                $enddatetime_first_day
            );

            $start_modificator = 0;
            $end_modificator = 0;

            foreach($period as $dt)
            {
                $worked_days += 30;
            }

            $worked_days_in_first_month = 0;
            if ($start_date_day !== 1)
            {
                if($haveWorkContractInDayBeforeThisStart === true)
                {
                    if($start_date_number_days < 30)
                    {
                        $start_date_number_days = 30;
                    }
                    
                    $worked_days_in_first_month = $start_date_number_days - $start_date_day;

                    if($start_date_number_days === 30)
                    {
                        $worked_days_in_first_month += 1;
                    }
                }
                else 
                {
                    $worked_days_in_first_month = $start_date_number_days - $start_date_day;
                    
                    $worked_days_in_first_month += 1;
                }
                
                $start_modificator = -(30-$worked_days_in_first_month);
            }

            if ($end_date_day !== $end_date_number_days)
            {
                $worked_days_in_last_month = $end_date_day;
                
                $end_modificator = -(30 - $worked_days_in_last_month);
            }

            $worked_days += $start_modificator;
            $worked_days += $end_modificator;
        }
                
        return $worked_days;
    }
    
    public static function formatWorkExp($workedDays, $raw_data = false)
    {        
        $wd = $workedDays;
        $wm = 0;
        $wy = 0;

        if($wd >= 30)
        {
            $wm += floor($wd/30);
            $wd = $wd%30;
        }

        if($wm >= 12)
        {
            $wy += floor($wm/12);
            $wm = $wm%12;
        }
        
        if($raw_data)
        {
            $result =  [
                'y' => $wy,
                'm' => $wm,
                'd' => $wd
            ];
        }
        else
        {
            $result = Yii::t('HRModule.WorkContract', 'WorkTimeYearsMonthsDays', [
                '{years}' => $wy,
                '{months}' => $wm,
                '{days}' => $wd
            ]);
        }
               
        return $result;
    }
}