<?php

abstract class EntryDoorLogComponent
{   
    protected $_installed = true;
    protected $_have_employee_card = true;
    protected $_is_by_activity = false;
    
    public function init()
    {
    }
    
    public function isInstalled()
    {
        return $this->_installed;
    }
    
    public function haveEmployeeCard()
    {
        return $this->_have_employee_card;
    }
    
    public function isByActivity()
    {
        return $this->_is_by_activity;
    }
    
    public function getLastEntryDorLog()
    {
        $result = null;
        
        $criteria = new CDbCriteria();
        $criteria->order = 'time DESC';
        $criteria->limit = 1;
        $model = EntryDoorLog::model()->resetScope()->find($criteria);
        
        if(!empty($model->id))
        {
            $result = $model;
        }
        
        return $result;
    }
    
    // Force Extending class to define this method
    abstract protected function getNewLogs($sendUpdateEvents);
    abstract protected function validateCardNumber($cardNumber);
    abstract protected function getNonExistingCards();
}

