<?php

class EntryDoorLogComponentDummy extends EntryDoorLogComponent
{
    protected $_installed = false;
    protected $_have_employee_card = false;
    
    public function init()
    {
    }
    
    public function getLastEntryDorLog()
    {
        throw new Exception(__METHOD__.' - dummy called');
    }
    
    public function getNewLogs($sendUpdateEvents)
    {
        throw new Exception(__METHOD__.' - dummy called');
    }
    
    public function validateCardNumber($cardNumber)
    {
        throw new Exception(__METHOD__.' - dummy called');
    }
    
    public function getNonExistingCards()
    {
        throw new Exception(__METHOD__.' - dummy called');
    }
}

