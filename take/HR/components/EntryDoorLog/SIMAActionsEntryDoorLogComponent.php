<?php

class SIMAActionsEntryDoorLogComponent extends EntryDoorLogComponent
{
    protected $_is_by_activity = true;
    
    public function init()
    {
        Session::registerPermanentBehavior('SessionEntryDoorLogBehavior');
    }
    
    public function getNewLogs($sendUpdateEvents)
    {
        throw new Exception('should not be called');
    }
    
    public function validateCardNumber($cardNumber)
    {
        if(preg_match('/(\d{1,3}|\*)\.(\d{1,3}|\*)\.(\d{1,3}|\*)\.(\d{1,3}|\*)/', $cardNumber) !== 1)
        {
            throw new Exception('Nevalidan broj kartice (NUM.NUM.NUM.*)');
        }
    }
    
    public function getNonExistingCards()
    {
        throw new Exception(__METHOD__.' - dummy called');
    }
    
    public function getCardByUserIP()
    {
        $card = null;
                
        $user_ip = Yii::app()->getClientIpAddress();
        
//        $emp_cards = EntryDoorCard::model()->findAll(new SIMADbCriteria([
//            'Model' => EntryDoorCard::class,
//            'model_filter' => [
//                'partner' => [
//                    'ids' => Yii::app()->user->id
//                ],
//                'scopes' => [
//                    'confirmed'
//                ]
//            ]
//        ]));
        $emp_cards = EntryDoorCard::model()->findAll([
            'scopes' => [
                'confirmed'
            ],
            'model_filter' => [
                'partner' => [
                    'ids' => Yii::app()->user->id
                ]
            ]
        ]);
        
        $user_ip_exploded = explode('.', $user_ip);
        
        foreach($emp_cards as $emp_card)
        {            
            $emp_card_exploded = explode('.', $emp_card);
            
            $card_satisfies = true;
            for($index_counter = 0; $index_counter < 4; $index_counter++)
            {
                if($emp_card_exploded[$index_counter] === '*')
                {
                    continue;
                }
                
                if($emp_card_exploded[$index_counter] !== $user_ip_exploded[$index_counter])
                {
                    $card_satisfies = false;
                    break;
                }
            }
            
            if($card_satisfies === true)
            {
                $card = $emp_card;
                break;
            }
        }
                
        return $card;
    }
}

