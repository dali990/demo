<?php

class CentaurEntryDoorLogComponent extends EntryDoorLogComponent
{
    public $db_hostname = null;
    public $db_port = null;
    public $db_name = null;
    public $db_username = null;
    public $db_pass = null;
    
    public $entry_type_id = 23;
    public $exit_type_id = 33;
        
    public function init()
    {
        if(empty($this->db_hostname))
        {
            throw new Exception('empty db_hostname');
        }
        if(empty($this->db_port))
        {
            throw new Exception('empty db_port');
        }
        if(empty($this->db_name))
        {
            throw new Exception('empty db_name');
        }
        if(empty($this->db_username))
        {
            throw new Exception('empty db_username');
        }
        if(empty($this->db_pass))
        {
            throw new Exception('empty db_pass');
        }
        
        parent::init();
    }
    
    /**
     * 
     * @return int - broj sacuvanih novih redova
     * @throws Exception
     */
    public function getNewLogs($sendUpdateEvents)
    {        
        $dbh = $this->connectToDB();
        
        $saved_count = 0;
        $eventSourceStepsSum = 0;
                
        $confirmedCards = EntryDoorCard::model()->confirmed()->findAll();
                
        if($sendUpdateEvents === true)
        {
            $confirmedCards_count = count($confirmedCards);
            if($confirmedCards_count > 0)
            {
                $eventSourceSteps = (50/$confirmedCards_count);
            }
            else
            {
                $eventSourceStepsSum += 50;
                Yii::app()->controller->sendUpdateEvent($eventSourceStepsSum);
            }
        }
        
        foreach($confirmedCards as $confirmedCard)
        {
            $saved_count += $this->getNewLogsForCard($confirmedCard, $dbh);
            
            if($sendUpdateEvents === true)
            {
                $eventSourceStepsSum += $eventSourceSteps;
                Yii::app()->controller->sendUpdateEvent($eventSourceStepsSum);
            }
        }
        
        $notConfirmedCards = EntryDoorCard::model()->notConfirmed()->findAll();
        
        if($sendUpdateEvents === true)
        {
            $notConfirmedCards_count = count($notConfirmedCards);
            if($notConfirmedCards_count > 0)
            {
                $eventSourceSteps = (50/$notConfirmedCards_count);
            }
            else
            {
                $eventSourceStepsSum += 50;
                Yii::app()->controller->sendUpdateEvent($eventSourceStepsSum);
            }
        }
        
        $raiseNoteMessage = "";
        foreach($notConfirmedCards as $notConfirmedCard)
        {
            $notConfirmedCardCount = $this->getNewLogsForCard($notConfirmedCard, $dbh);
            
            if($notConfirmedCardCount > 0)
            {
                $raiseNoteMessage .= Yii::t('HRModule.EntryDoor', 'NotConfirmedCardUsed', [
                    '{card_number}' => $notConfirmedCard->card_number,
                    '{used_count}' => $notConfirmedCardCount
                ]).'<br>';
            }
            
            $saved_count += $notConfirmedCardCount;
            
            if($sendUpdateEvents === true)
            {
                $eventSourceStepsSum += $eventSourceSteps;
                Yii::app()->controller->sendUpdateEvent($eventSourceStepsSum);
            }
        }
        
        if(!empty($raiseNoteMessage))
        {
            Yii::app()->raiseNote($raiseNoteMessage);
        }
        
        unset($dbh);
        
        return $saved_count;
    }
    
    public function getNonExistingCards()
    {
        $saved_count = 0;
        
        $dbh = $this->connectToDB();
        
        $sql = 'SELECT * FROM [Centaur3Events].[dbo].[Card Holder Names] WHERE [Access Level2]=1';
        $stmt = $dbh->prepare($sql);
        $stmt->execute();
        
        while ($row = $stmt->fetch()) 
        {
            $cardNumber = $row['Card Number'];
            $familyNumber = $row['Family Number'];
            $card_family_number_exact_param = $familyNumber.'-'.$cardNumber;
            
            $criteria = new SIMADbCriteria([
                'Model' => EntryDoorCard::model(),
                'model_filter' => [
                    'card_family_number_exact' => $card_family_number_exact_param
                ]
            ], true);
            
            if(!empty($criteria->ids))
            {
                continue;
            }
            
            $comment = 'Last Name: '.utf8_encode($row['Last Name']).' - First Name: '.utf8_encode($row['First Name']);
                        
            $newCard = new EntryDoorCard();
            $newCard->card_number = $card_family_number_exact_param;
            $newCard->comment = $comment;
            $newCard->save();
            
            $saved_count++;
        }
        
        unset($dbh);
        
        return $saved_count;
    }
    
    public function validateCardNumber($cardNumber)
    {
        if(preg_match('/\d{1,}\-\d{1,}/', $cardNumber) !== 1)
        {
            throw new Exception('Nevalidan broj kartice (FamilyNum-CardNum)');
        }
    }
    
    private function getNewLogsForCard(EntryDoorCard $card, $dbh)
    {
        $card_number_split = explode('-', $card->card_number, 2);
        if(count($card_number_split) != 2)
        {
            return;
        }
        $family_number = $card_number_split[0];
        $card_number = $card_number_split[1];
        $lastEntryLog = $card->last_entry_log;
        
        $sql = "SELECT * "
            ."FROM [Centaur3Events].[dbo].[Events] [E] "
                ."INNER JOIN [Centaur3Events].[dbo].[Card Holder Names] [CHN] ON [E].[Card Holder ID]=[CHN].[Card Holder ID] "
            ."WHERE ";
        
        if(!empty($lastEntryLog))
        {            
            $additonal_data = $lastEntryLog->additional_data;
            $additonal_data_decoded = json_decode($additonal_data);
            $event_id = $additonal_data_decoded->{'Event ID'};

            $sql .= "[Event ID]>$event_id "
                    . "AND "
                    . "([Record Name ID] IN ($this->entry_type_id, $this->exit_type_id))";
        }
        else
        {      
            $where_addition = '';
            $last_pay_day = PaycheckPeriodComponent::GetLastPayOutDayInFinalPeriod();
                        
            if(!empty($last_pay_day))
            {
                $day_after_payday = $last_pay_day->next();
                $where_addition = " AND [Field Time] > Convert(datetime, '"
                    .$day_after_payday->getYearNumber()
                    .'-'.str_pad($day_after_payday->getMonthNumber(), 2, '0', STR_PAD_LEFT)
                    .'-'.str_pad($day_after_payday->getDayInMonthNumber(), 2, '0', STR_PAD_LEFT)
                ."') ";
            }
            
            $sql .= "([Record Name ID] IN ($this->entry_type_id, $this->exit_type_id))"
                    . $where_addition;
        }
        
        $sql .= "AND [Access Level2]=1 AND [Family Number]=$family_number AND [Card Number]=$card_number "
            . "ORDER BY [Event ID] ASC";
                
        $stmt = $dbh->prepare($sql);
                
        $stmt->execute();
        
        $saved_count = 0;
                
        $count = 0;
        while ($row = $stmt->fetch()) 
        {
            $count++;
            
            $timestamp = strtotime($row['Field Time']);
            $date_str = date('Y-m-d H:i:s', $timestamp);
            $time = SIMAHtml::UserToDbDate($date_str, true);
                                    
            $type = null;
            if(intval($row['Record Name ID']) === intval($this->entry_type_id))
            {
                $type = EntryDoorLog::$TYPE_ENTRY;
            }
            else if(intval($row['Record Name ID']) === intval($this->exit_type_id))
            {
                $type = EntryDoorLog::$TYPE_EXIT;
            }
                        
            $entryDoorLog = new EntryDoorLog();
            $entryDoorLog->time = $time;
            $entryDoorLog->partner_id = $card->partner_id;
            $entryDoorLog->entry_door_card_id = $card->id;
            $entryDoorLog->type = $type;
            $entryDoorLog->additional_data = json_encode([
                'Event ID' => $row['Event ID'],
                'Last Name' => utf8_encode($row['Last Name']),
                'First Name' => utf8_encode($row['First Name'])
            ]);
            $entryDoorLog->save();
            
            $saved_count++;
        }
        
        unset($stmt);
        
        return $saved_count;
    }
    
    private function connectToDB()
    {
        try
        {
            $dbh = new PDO("dblib:host=$this->db_hostname:$this->db_port;dbname=$this->db_name", "$this->db_username", "$this->db_pass", [
                PDO::ATTR_TIMEOUT => 5,
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
            ]);
            return $dbh;
        } catch (PDOException $e) {
            throw new SIMAWarnException(Yii::t('HRModule.EntryDoor', 'DBConnectionFail', ['{errmsg}' => $e->getMessage()]));
        }
    }
}

