<?php

class SessionEntryDoorLogBehavior extends SIMAActiveRecordBehavior
{
    public function afterSave($event)
    {
        parent::afterSave($event);
                
        $owner = $this->owner;
        if(
                $owner->session_type === Session::$TYPE_SIMAweb                                             /// ako je web sesija
                && Yii::app()->entryDoorLog->isByActivity()                                                 /// da je evidencija ulazaka/izlazaka po aktivnosti
                && (($owner->isNewRecord                                                                    /// da je nova sesija
                    || (!$owner->isNewRecord && $owner->getOld()['idle_since'] !== $owner->idle_since)))    /// ili nije nova, ali se promenio idle_since
        )
        {
            $emp_card = Yii::app()->entryDoorLog->getCardByUserIP();
            
            if(empty($emp_card))
            {
                return;
            }
            
            /// ako nema ulazaka
            if(EntryDoorLog::model()->count(new SIMADbCriteria([
                'Model' => EntryDoorLog::class,
                'model_filter' => [
                    'partner' => [
                        'ids' => Yii::app()->user->id
                    ],
                    'type' => EntryDoorLog::$TYPE_ENTRY,
                    'scopes' => [
                        'today'
                    ]
                ]
            ])) === 0)
            {
                $entryDoorLog = new EntryDoorLog();
                $entryDoorLog->partner_id = Yii::app()->user->id;
                $entryDoorLog->type = EntryDoorLog::$TYPE_ENTRY;
            }
            else
            {
                $entryDoorLog = EntryDoorLog::model()->find(new SIMADbCriteria([
                    'Model' => EntryDoorLog::class,
                    'model_filter' => [
                        'partner' => [
                            'ids' => Yii::app()->user->id
                        ],
                        'type' => EntryDoorLog::$TYPE_EXIT,
                        'scopes' => [
                            'today'
                        ]
                    ]
                ]));

                if(empty($entryDoorLog))
                {
                    $entryDoorLog = new EntryDoorLog();
                    $entryDoorLog->partner_id = Yii::app()->user->id;
                    $entryDoorLog->type = EntryDoorLog::$TYPE_EXIT;
                }
            }

            $entryDoorLog->time = 'now()';
            $entryDoorLog->entry_door_card_id = $emp_card->id;
            $entryDoorLog->additional_data = json_encode([
                '$action' => filter_input(INPUT_GET, 'r')
            ]);
            $entryDoorLog->save();
        }
    }
}

