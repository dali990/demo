<?php

class SafeEvd3Lists 
{    
    /**
    * Drzavljanstvo
    */
    public static $citizenship_list = [];
    /**
    * Radni status
    */
    public static $employment_status_list = [];
    /**
     * Sati
     */
    public static $beginning_working_hour_list = [];
    /**
     *  Povreda na radno mesto ili na putu
     */
    public static $commuting_accidents_list = [];
    /**
     * Radna sredina
     */
    public static $work_environment_list = [];
    /**
     * Proces rada
     */
    public static $work_process_list = [];
    /**
     * Specifične fizičke aktivnosti
     */
    public static $specific_physical_activity_list = [];
    /**
     * Kontakt - način povređivanja
     */
    public static $contact_injury_list = [];
    /**
     * Da li je u momentu povrede povređeni radio na radnom mestu sa povećanim rizikom ili nije
     */
    public static $increased_risk_list = [];
    /**
     * Vrsta povrede
     */
    public static $injury_type_list = [];
    /**
     * Vrsta povrede - stara evidencija
     */
    public static $injury_type_old_list = ['01', '02'];
    /**
     * Povredjeni deo tela
     */
    public static $injury_part_of_body_list = [];
    /**
     * da li je povređeni sprečen za rad više od tri uzastopna kalendarska dan
     */
    public static $mt3_days_unable_work_list = [];
    /**
     * procenjen broj izgubljenih kalendarskih dana
     */
    public static $estimated_lost_days_list = [];
    /**
     * Ocena 
     * @var typetežine povrede
     */
    public static $mark_injury_list = [];
    /**
     * Ocena težine povrede - stara evidencija
     */
    public static $mark_injury_old_list = [4];
    
    public static function init()
    {
        self::$citizenship_list = [
            0 => "(0) ".Yii::t('HRModule.SafeEvd3','citizenship_0'),
            1 => "(1) ".Yii::t('HRModule.SafeEvd3','citizenship_1'),
            2 => "(2) ".Yii::t('HRModule.SafeEvd3','citizenship_2'),
            3 => "(3) ".Yii::t('HRModule.SafeEvd3','citizenship_3'),
        ];
        
        self::$employment_status_list = [
            "000" => "(000) ".Yii::t('HRModule.SafeEvd3','emp_status_000'), 
            100 => "(100) ".Yii::t('HRModule.SafeEvd3','emp_status_100'),
            301 => "(301) ".Yii::t('HRModule.SafeEvd3','emp_status_301'),
            302 => "(302) ".Yii::t('HRModule.SafeEvd3','emp_status_302'), 
            310 => "(310) ".Yii::t('HRModule.SafeEvd3','emp_status_310'), 
            311 => "(311) ".Yii::t('HRModule.SafeEvd3','emp_status_311'), 
            312 => "(312) ".Yii::t('HRModule.SafeEvd3','emp_status_312'), 
            320 => "(320) ".Yii::t('HRModule.SafeEvd3','emp_status_320'), 
            321 => "(321) ".Yii::t('HRModule.SafeEvd3','emp_status_321'), 
            322 => "(322) ".Yii::t('HRModule.SafeEvd3','emp_status_322'), 
            323 => "(323) ".Yii::t('HRModule.SafeEvd3','emp_status_323'),
            400 => "(400) ".Yii::t('HRModule.SafeEvd3','emp_status_400'), 
            500 => "(500) ".Yii::t('HRModule.SafeEvd3','emp_status_500'), 
            900 => "(900) ".Yii::t('HRModule.SafeEvd3','emp_status_900'),
        ];
        
        self::$beginning_working_hour_list = [
            "00" => "(00) ".Yii::t('HRModule.SafeEvd3','beginning_working_hour_00'),
            "01" => "(01) ".Yii::t('HRModule.SafeEvd3','beginning_working_hour_01'),
            "02" => "(02) ".Yii::t('HRModule.SafeEvd3','beginning_working_hour_02'),
            "03" => "(03) ".Yii::t('HRModule.SafeEvd3','beginning_working_hour_03'),
            "04" => "(04) ".Yii::t('HRModule.SafeEvd3','beginning_working_hour_04'),
            "05" => "(05) ".Yii::t('HRModule.SafeEvd3','beginning_working_hour_05'),
            "06" => "(06) ".Yii::t('HRModule.SafeEvd3','beginning_working_hour_06'),
            "07" => "(07) ".Yii::t('HRModule.SafeEvd3','beginning_working_hour_07'),
            "08" => "(08) ".Yii::t('HRModule.SafeEvd3','beginning_working_hour_08'),
            "09" => "(09) ".Yii::t('HRModule.SafeEvd3','beginning_working_hour_09'),
            "10" => "(10) ".Yii::t('HRModule.SafeEvd3','beginning_working_hour_10'),
            "11" => "(11) ".Yii::t('HRModule.SafeEvd3','beginning_working_hour_11'),
            "12" => "(12) ".Yii::t('HRModule.SafeEvd3','beginning_working_hour_12'),
            "13" => "(13) ".Yii::t('HRModule.SafeEvd3','beginning_working_hour_13'),
            "14" => "(14) ".Yii::t('HRModule.SafeEvd3','beginning_working_hour_14'),
            "15" => "(15) ".Yii::t('HRModule.SafeEvd3','beginning_working_hour_15'),
            "16" => "(16) ".Yii::t('HRModule.SafeEvd3','beginning_working_hour_16'),
            "17" => "(17) ".Yii::t('HRModule.SafeEvd3','beginning_working_hour_17'),
            "18" => "(18) ".Yii::t('HRModule.SafeEvd3','beginning_working_hour_18'),
            "19" => "(19) ".Yii::t('HRModule.SafeEvd3','beginning_working_hour_19'),
            "20" => "(20) ".Yii::t('HRModule.SafeEvd3','beginning_working_hour_20'),
            "21" => "(21) ".Yii::t('HRModule.SafeEvd3','beginning_working_hour_21'),
            "22" => "(22) ".Yii::t('HRModule.SafeEvd3','beginning_working_hour_22'),
            "23" => "(23) ".Yii::t('HRModule.SafeEvd3','beginning_working_hour_23'),
            "24" => "(24) ".Yii::t('HRModule.SafeEvd3','beginning_working_hour_24'),
            "99" => "(99) ".Yii::t('HRModule.SafeEvd3','beginning_working_hour_99')
        ];
        
        self::$commuting_accidents_list = [
            0 => "(0) ".Yii::t('HRModule.SafeEvd3','commuting_accidents_0'),
            1 => "(1) ".Yii::t('HRModule.SafeEvd3','commuting_accidents_1'),
            2 => "(2) ".Yii::t('HRModule.SafeEvd3','commuting_accidents_2'),
            3 => "(3) ".Yii::t('HRModule.SafeEvd3','commuting_accidents_3'),
            4 => "(4) ".Yii::t('HRModule.SafeEvd3','commuting_accidents_4'),
            9 => "(9) ".Yii::t('HRModule.SafeEvd3','commuting_accidents_9'),
        ];
        
        self::$work_environment_list = [ 
            "000" => '(000) '.Yii::t('HRModule.SafeEvd3','work_environment_0'), 
            "010" => '(010) '.Yii::t('HRModule.SafeEvd3','work_environment_10'), 
            "011" => '(011) '.Yii::t('HRModule.SafeEvd3','work_environment_11'), 
            "012" => '(012) '.Yii::t('HRModule.SafeEvd3','work_environment_12'), 
            "013" => '(013) '.Yii::t('HRModule.SafeEvd3','work_environment_13'), 
            "019" => '(019) '.Yii::t('HRModule.SafeEvd3','work_environment_19'), 
            "020" => '(020) '.Yii::t('HRModule.SafeEvd3','work_environment_20'), 
            "021" => '(021) '.Yii::t('HRModule.SafeEvd3','work_environment_21'), 
            "022" => '(022) '.Yii::t('HRModule.SafeEvd3','work_environment_22'), 
            "023" => '(023) '.Yii::t('HRModule.SafeEvd3','work_environment_23'), 
            "024" => '(024) '.Yii::t('HRModule.SafeEvd3','work_environment_24'), 
            "025" => '(025) '.Yii::t('HRModule.SafeEvd3','work_environment_25'), 
            "026" => '(026) '.Yii::t('HRModule.SafeEvd3','work_environment_26'), 
            "029" => '(029) '.Yii::t('HRModule.SafeEvd3','work_environment_29'), 
            "030" => '(030) '.Yii::t('HRModule.SafeEvd3','work_environment_30'), 
            "031" => '(031) '.Yii::t('HRModule.SafeEvd3','work_environment_31'), 
            "032" => '(032) '.Yii::t('HRModule.SafeEvd3','work_environment_32'), 
            "033" => '(033) '.Yii::t('HRModule.SafeEvd3','work_environment_33'), 
            "034" => '(034) '.Yii::t('HRModule.SafeEvd3','work_environment_34'), 
            "035" => '(035) '.Yii::t('HRModule.SafeEvd3','work_environment_35'), 
            "036" => '(036) '.Yii::t('HRModule.SafeEvd3','work_environment_36'), 
            "039" => '(039) '.Yii::t('HRModule.SafeEvd3','work_environment_39'), 
            "040" => '(040) '.Yii::t('HRModule.SafeEvd3','work_environment_40'), 
            "041" => '(041) '.Yii::t('HRModule.SafeEvd3','work_environment_41'), 
            "042" => '(042) '.Yii::t('HRModule.SafeEvd3','work_environment_42'), 
            "043" => '(043) '.Yii::t('HRModule.SafeEvd3','work_environment_43'), 
            "044" => '(044) '.Yii::t('HRModule.SafeEvd3','work_environment_44'), 
            "049" => '(049) '.Yii::t('HRModule.SafeEvd3','work_environment_49'), 
            "050" => '(050) '.Yii::t('HRModule.SafeEvd3','work_environment_50'), 
            "051" => '(051) '.Yii::t('HRModule.SafeEvd3','work_environment_51'), 
            "052" => '(052) '.Yii::t('HRModule.SafeEvd3','work_environment_52'), 
            "060" => '(060) '.Yii::t('HRModule.SafeEvd3','work_environment_60'), 
            "061" => '(061) '.Yii::t('HRModule.SafeEvd3','work_environment_61'), 
            "062" => '(062) '.Yii::t('HRModule.SafeEvd3','work_environment_62'), 
            "063" => '(063) '.Yii::t('HRModule.SafeEvd3','work_environment_63'), 
            "069" => '(069) '.Yii::t('HRModule.SafeEvd3','work_environment_69'), 
            "070" => '(070) '.Yii::t('HRModule.SafeEvd3','work_environment_70'), 
            "071" => '(071) '.Yii::t('HRModule.SafeEvd3','work_environment_71'), 
            "072" => '(072) '.Yii::t('HRModule.SafeEvd3','work_environment_72'), 
            "079" => '(079) '.Yii::t('HRModule.SafeEvd3','work_environment_79'), 
            "080" => '(080) '.Yii::t('HRModule.SafeEvd3','work_environment_80'), 
            "081" => '(081) '.Yii::t('HRModule.SafeEvd3','work_environment_81'), 
            "082" => '(082) '.Yii::t('HRModule.SafeEvd3','work_environment_82'), 
            "089" => '(089) '.Yii::t('HRModule.SafeEvd3','work_environment_89'), 
            "090" => '(090) '.Yii::t('HRModule.SafeEvd3','work_environment_90'), 
            "091" => '(091) '.Yii::t('HRModule.SafeEvd3','work_environment_91'), 
            "092" => '(092) '.Yii::t('HRModule.SafeEvd3','work_environment_92'), 
            "093" => '(093) '.Yii::t('HRModule.SafeEvd3','work_environment_93'), 
            "099" => '(099) '.Yii::t('HRModule.SafeEvd3','work_environment_99'), 
            "100" => '(100) '.Yii::t('HRModule.SafeEvd3','work_environment_100'), 
            "101" => '(101) '.Yii::t('HRModule.SafeEvd3','work_environment_101'), 
            "102" => '(102) '.Yii::t('HRModule.SafeEvd3','work_environment_102'), 
            "103" => '(103) '.Yii::t('HRModule.SafeEvd3','work_environment_103'), 
            "109" => '(109) '.Yii::t('HRModule.SafeEvd3','work_environment_109'), 
            "110" => '(110) '.Yii::t('HRModule.SafeEvd3','work_environment_110'), 
            "111" => '(111) '.Yii::t('HRModule.SafeEvd3','work_environment_111'), 
            "112" => '(112) '.Yii::t('HRModule.SafeEvd3','work_environment_112'), 
            "119" => '(119) '.Yii::t('HRModule.SafeEvd3','work_environment_119'), 
            "120" => '(120) '.Yii::t('HRModule.SafeEvd3','work_environment_120'), 
            "121" => '(121) '.Yii::t('HRModule.SafeEvd3','work_environment_121'), 
            "122" => '(122) '.Yii::t('HRModule.SafeEvd3','work_environment_122'), 
            "129" => '(129) '.Yii::t('HRModule.SafeEvd3','work_environment_129'), 
            "999" => '(999) '.Yii::t('HRModule.SafeEvd3','work_environment_999'), 
        ];
        
        self::$work_process_list = [
            '00' => '(00) '.Yii::t('HRModule.SafeEvd3','work_process_0'), 
            '10' => '(10) '.Yii::t('HRModule.SafeEvd3','work_process_10'), 
            '11' => '(11) '.Yii::t('HRModule.SafeEvd3','work_process_11'), 
            '12' => '(12) '.Yii::t('HRModule.SafeEvd3','work_process_12'), 
            '19' => '(19) '.Yii::t('HRModule.SafeEvd3','work_process_19'), 
            '20' => '(20) '.Yii::t('HRModule.SafeEvd3','work_process_20'), 
            '21' => '(21) '.Yii::t('HRModule.SafeEvd3','work_process_21'), 
            '22' => '(22) '.Yii::t('HRModule.SafeEvd3','work_process_22'), 
            '23' => '(23) '.Yii::t('HRModule.SafeEvd3','work_process_23'), 
            '24' => '(24) '.Yii::t('HRModule.SafeEvd3','work_process_24'), 
            '25' => '(25) '.Yii::t('HRModule.SafeEvd3','work_process_25'), 
            '29' => '(29) '.Yii::t('HRModule.SafeEvd3','work_process_29'), 
            '30' => '(30) '.Yii::t('HRModule.SafeEvd3','work_process_30'), 
            '31' => '(31) '.Yii::t('HRModule.SafeEvd3','work_process_31'), 
            '32' => '(32) '.Yii::t('HRModule.SafeEvd3','work_process_32'), 
            '33' => '(33) '.Yii::t('HRModule.SafeEvd3','work_process_33'), 
            '34' => '(34) '.Yii::t('HRModule.SafeEvd3','work_process_34'), 
            '35' => '(35) '.Yii::t('HRModule.SafeEvd3','work_process_35'), 
            '39' => '(39) '.Yii::t('HRModule.SafeEvd3','work_process_39'), 
            '40' => '(40) '.Yii::t('HRModule.SafeEvd3','work_process_40'), 
            '41' => '(41) '.Yii::t('HRModule.SafeEvd3','work_process_41'), 
            '42' => '(42) '.Yii::t('HRModule.SafeEvd3','work_process_42'), 
            '43' => '(43) '.Yii::t('HRModule.SafeEvd3','work_process_43'), 
            '49' => '(49) '.Yii::t('HRModule.SafeEvd3','work_process_49'), 
            '50' => '(50) '.Yii::t('HRModule.SafeEvd3','work_process_50'), 
            '51' => '(51) '.Yii::t('HRModule.SafeEvd3','work_process_51'), 
            '52' => '(52) '.Yii::t('HRModule.SafeEvd3','work_process_52'), 
            '53' => '(53) '.Yii::t('HRModule.SafeEvd3','work_process_53'), 
            '54' => '(54) '.Yii::t('HRModule.SafeEvd3','work_process_54'), 
            '55' => '(55) '.Yii::t('HRModule.SafeEvd3','work_process_55'), 
            '59' => '(59) '.Yii::t('HRModule.SafeEvd3','work_process_59'), 
            '60' => '(60) '.Yii::t('HRModule.SafeEvd3','work_process_60'), 
            '61' => '(61) '.Yii::t('HRModule.SafeEvd3','work_process_61'), 
            '62' => '(62) '.Yii::t('HRModule.SafeEvd3','work_process_62'), 
            '69' => '(69) '.Yii::t('HRModule.SafeEvd3','work_process_69'), 
            '99' => '(99) '.Yii::t('HRModule.SafeEvd3','work_process_99'),
        ];
        
        self::$specific_physical_activity_list = [
            '00' => '(00) '.Yii::t('HRModule.SafeEvd3','specific_physical_activity_0'), 
            '10' => '(10) '.Yii::t('HRModule.SafeEvd3','specific_physical_activity_10'), 
            '11' => '(11) '.Yii::t('HRModule.SafeEvd3','specific_physical_activity_11'), 
            '12' => '(12) '.Yii::t('HRModule.SafeEvd3','specific_physical_activity_12'), 
            '13' => '(13) '.Yii::t('HRModule.SafeEvd3','specific_physical_activity_13'), 
            '19' => '(19) '.Yii::t('HRModule.SafeEvd3','specific_physical_activity_19'), 
            '20' => '(20) '.Yii::t('HRModule.SafeEvd3','specific_physical_activity_20'), 
            '21' => '(21) '.Yii::t('HRModule.SafeEvd3','specific_physical_activity_21'), 
            '22' => '(22) '.Yii::t('HRModule.SafeEvd3','specific_physical_activity_22'), 
            '29' => '(29) '.Yii::t('HRModule.SafeEvd3','specific_physical_activity_29'), 
            '30' => '(30) '.Yii::t('HRModule.SafeEvd3','specific_physical_activity_30'), 
            '31' => '(31) '.Yii::t('HRModule.SafeEvd3','specific_physical_activity_31'), 
            '32' => '(32) '.Yii::t('HRModule.SafeEvd3','specific_physical_activity_32'), 
            '33' => '(33) '.Yii::t('HRModule.SafeEvd3','specific_physical_activity_33'), 
            '39' => '(39) '.Yii::t('HRModule.SafeEvd3','specific_physical_activity_39'), 
            '40' => '(40) '.Yii::t('HRModule.SafeEvd3','specific_physical_activity_40'), 
            '41' => '(41) '.Yii::t('HRModule.SafeEvd3','specific_physical_activity_41'), 
            '42' => '(42) '.Yii::t('HRModule.SafeEvd3','specific_physical_activity_42'), 
            '43' => '(43) '.Yii::t('HRModule.SafeEvd3','specific_physical_activity_43'), 
            '44' => '(44) '.Yii::t('HRModule.SafeEvd3','specific_physical_activity_44'), 
            '45' => '(45) '.Yii::t('HRModule.SafeEvd3','specific_physical_activity_45'), 
            '46' => '(46) '.Yii::t('HRModule.SafeEvd3','specific_physical_activity_46'), 
            '47' => '(47) '.Yii::t('HRModule.SafeEvd3','specific_physical_activity_47'), 
            '49' => '(49) '.Yii::t('HRModule.SafeEvd3','specific_physical_activity_49'), 
            '50' => '(50) '.Yii::t('HRModule.SafeEvd3','specific_physical_activity_50'), 
            '51' => '(51) '.Yii::t('HRModule.SafeEvd3','specific_physical_activity_51'), 
            '52' => '(52) '.Yii::t('HRModule.SafeEvd3','specific_physical_activity_52'), 
            '53' => '(53) '.Yii::t('HRModule.SafeEvd3','specific_physical_activity_53'), 
            '59' => '(59) '.Yii::t('HRModule.SafeEvd3','specific_physical_activity_59'), 
            '60' => '(60) '.Yii::t('HRModule.SafeEvd3','specific_physical_activity_60'), 
            '61' => '(61) '.Yii::t('HRModule.SafeEvd3','specific_physical_activity_61'), 
            '62' => '(62) '.Yii::t('HRModule.SafeEvd3','specific_physical_activity_62'), 
            '63' => '(63) '.Yii::t('HRModule.SafeEvd3','specific_physical_activity_63'), 
            '64' => '(64) '.Yii::t('HRModule.SafeEvd3','specific_physical_activity_64'), 
            '65' => '(65) '.Yii::t('HRModule.SafeEvd3','specific_physical_activity_65'), 
            '66' => '(66) '.Yii::t('HRModule.SafeEvd3','specific_physical_activity_66'), 
            '67' => '(67) '.Yii::t('HRModule.SafeEvd3','specific_physical_activity_67'), 
            '69' => '(69) '.Yii::t('HRModule.SafeEvd3','specific_physical_activity_69'), 
            '70' => '(70) '.Yii::t('HRModule.SafeEvd3','specific_physical_activity_70'), 
            '99' => '(99) '.Yii::t('HRModule.SafeEvd3','specific_physical_activity_99'),
        ];
    
        self::$contact_injury_list = [
            '00' => '(00) '.Yii::t('HRModule.SafeEvd3','contact_injury_0'), 
            '10' => '(10) '.Yii::t('HRModule.SafeEvd3','contact_injury_10'), 
            '11' => '(11) '.Yii::t('HRModule.SafeEvd3','contact_injury_11'), 
            '12' => '(12) '.Yii::t('HRModule.SafeEvd3','contact_injury_12'), 
            '13' => '(13) '.Yii::t('HRModule.SafeEvd3','contact_injury_13'), 
            '14' => '(14) '.Yii::t('HRModule.SafeEvd3','contact_injury_14'), 
            '15' => '(15) '.Yii::t('HRModule.SafeEvd3','contact_injury_15'), 
            '16' => '(16) '.Yii::t('HRModule.SafeEvd3','contact_injury_16'), 
            '17' => '(17) '.Yii::t('HRModule.SafeEvd3','contact_injury_17'), 
            '19' => '(19) '.Yii::t('HRModule.SafeEvd3','contact_injury_19'), 
            '20' => '(20) '.Yii::t('HRModule.SafeEvd3','contact_injury_20'), 
            '21' => '(21) '.Yii::t('HRModule.SafeEvd3','contact_injury_21'), 
            '22' => '(22) '.Yii::t('HRModule.SafeEvd3','contact_injury_22'), 
            '23' => '(23) '.Yii::t('HRModule.SafeEvd3','contact_injury_23'), 
            '29' => '(29) '.Yii::t('HRModule.SafeEvd3','contact_injury_29'), 
            '30' => '(30) '.Yii::t('HRModule.SafeEvd3','contact_injury_30'), 
            '31' => '(31) '.Yii::t('HRModule.SafeEvd3','contact_injury_31'), 
            '32' => '(32) '.Yii::t('HRModule.SafeEvd3','contact_injury_32'), 
            '39' => '(39) '.Yii::t('HRModule.SafeEvd3','contact_injury_39'), 
            '40' => '(40) '.Yii::t('HRModule.SafeEvd3','contact_injury_40'), 
            '41' => '(41) '.Yii::t('HRModule.SafeEvd3','contact_injury_41'), 
            '42' => '(42) '.Yii::t('HRModule.SafeEvd3','contact_injury_42'), 
            '43' => '(43) '.Yii::t('HRModule.SafeEvd3','contact_injury_43'), 
            '44' => '(44) '.Yii::t('HRModule.SafeEvd3','contact_injury_44'), 
            '45' => '(45) '.Yii::t('HRModule.SafeEvd3','contact_injury_45'), 
            '49' => '(49) '.Yii::t('HRModule.SafeEvd3','contact_injury_49'), 
            '50' => '(50) '.Yii::t('HRModule.SafeEvd3','contact_injury_50'), 
            '51' => '(51) '.Yii::t('HRModule.SafeEvd3','contact_injury_51'), 
            '52' => '(52) '.Yii::t('HRModule.SafeEvd3','contact_injury_52'), 
            '53' => '(53) '.Yii::t('HRModule.SafeEvd3','contact_injury_53'), 
            '59' => '(59) '.Yii::t('HRModule.SafeEvd3','contact_injury_59'), 
            '60' => '(60) '.Yii::t('HRModule.SafeEvd3','contact_injury_60'), 
            '61' => '(61) '.Yii::t('HRModule.SafeEvd3','contact_injury_61'), 
            '62' => '(62) '.Yii::t('HRModule.SafeEvd3','contact_injury_62'), 
            '63' => '(63) '.Yii::t('HRModule.SafeEvd3','contact_injury_63'), 
            '64' => '(64) '.Yii::t('HRModule.SafeEvd3','contact_injury_64'), 
            '69' => '(69) '.Yii::t('HRModule.SafeEvd3','contact_injury_69'), 
            '70' => '(70) '.Yii::t('HRModule.SafeEvd3','contact_injury_70'), 
            '71' => '(71) '.Yii::t('HRModule.SafeEvd3','contact_injury_71'), 
            '72' => '(72) '.Yii::t('HRModule.SafeEvd3','contact_injury_72'), 
            '73' => '(73) '.Yii::t('HRModule.SafeEvd3','contact_injury_73'), 
            '79' => '(79) '.Yii::t('HRModule.SafeEvd3','contact_injury_79'), 
            '80' => '(80) '.Yii::t('HRModule.SafeEvd3','contact_injury_80'), 
            '81' => '(81) '.Yii::t('HRModule.SafeEvd3','contact_injury_81'), 
            '82' => '(82) '.Yii::t('HRModule.SafeEvd3','contact_injury_82'), 
            '83' => '(83) '.Yii::t('HRModule.SafeEvd3','contact_injury_83'), 
            '89' => '(89) '.Yii::t('HRModule.SafeEvd3','contact_injury_89'), 
            '99' => '(99) '.Yii::t('HRModule.SafeEvd3','contact_injury_99')
        ];
        
        self::$increased_risk_list = [
            1 => '(1) '.Yii::t('HRModule.SafeEvd3','increased_risk_1'),
            2 => '(2) '.Yii::t('HRModule.SafeEvd3','increased_risk_2'),
        ];
        
        self::$injury_type_list = [ 
            '01' => Yii::t('HRModule.SafeEvd3','injury_type_01'), //stara evidencija
            '02' => Yii::t('HRModule.SafeEvd3','injury_type_02'), //stara evidencija
            '000' => '(000) '.Yii::t('HRModule.SafeEvd3','injury_type_000'), 
            '010' => '(010) '.Yii::t('HRModule.SafeEvd3','injury_type_10'), 
            '011' => '(011) '.Yii::t('HRModule.SafeEvd3','injury_type_11'), 
            '012' => '(012) '.Yii::t('HRModule.SafeEvd3','injury_type_12'), 
            '019' => '(019) '.Yii::t('HRModule.SafeEvd3','injury_type_19'), 
            '020' => '(020) '.Yii::t('HRModule.SafeEvd3','injury_type_20'), 
            '021' => '(021) '.Yii::t('HRModule.SafeEvd3','injury_type_21'), 
            '022' => '(022) '.Yii::t('HRModule.SafeEvd3','injury_type_22'), 
            '029' => '(029) '.Yii::t('HRModule.SafeEvd3','injury_type_29'), 
            '030' => '(030) '.Yii::t('HRModule.SafeEvd3','injury_type_30'), 
            '031' => '(031) '.Yii::t('HRModule.SafeEvd3','injury_type_31'), 
            '032' => '(032) '.Yii::t('HRModule.SafeEvd3','injury_type_32'), 
            '039' => '(039) '.Yii::t('HRModule.SafeEvd3','injury_type_39'), 
            '040' => '(040) '.Yii::t('HRModule.SafeEvd3','injury_type_40'), 
            '050' => '(050) '.Yii::t('HRModule.SafeEvd3','injury_type_50'), 
            '051' => '(051) '.Yii::t('HRModule.SafeEvd3','injury_type_51'), 
            '052' => '(052) '.Yii::t('HRModule.SafeEvd3','injury_type_52'), 
            '059' => '(059) '.Yii::t('HRModule.SafeEvd3','injury_type_59'), 
            '060' => '(060) '.Yii::t('HRModule.SafeEvd3','injury_type_60'), 
            '061' => '(061) '.Yii::t('HRModule.SafeEvd3','injury_type_61'), 
            '062' => '(062) '.Yii::t('HRModule.SafeEvd3','injury_type_62'), 
            '063' => '(063) '.Yii::t('HRModule.SafeEvd3','injury_type_63'), 
            '069' => '(069) '.Yii::t('HRModule.SafeEvd3','injury_type_69'), 
            '070' => '(070) '.Yii::t('HRModule.SafeEvd3','injury_type_70'), 
            '071' => '(071) '.Yii::t('HRModule.SafeEvd3','injury_type_71'), 
            '072' => '(072) '.Yii::t('HRModule.SafeEvd3','injury_type_72'), 
            '079' => '(079) '.Yii::t('HRModule.SafeEvd3','injury_type_79'), 
            '080' => '(080) '.Yii::t('HRModule.SafeEvd3','injury_type_80'), 
            '081' => '(081) '.Yii::t('HRModule.SafeEvd3','injury_type_81'), 
            '082' => '(082) '.Yii::t('HRModule.SafeEvd3','injury_type_82'), 
            '089' => '(089) '.Yii::t('HRModule.SafeEvd3','injury_type_89'), 
            '090' => '(090) '.Yii::t('HRModule.SafeEvd3','injury_type_90'), 
            '091' => '(091) '.Yii::t('HRModule.SafeEvd3','injury_type_91'), 
            '092' => '(092) '.Yii::t('HRModule.SafeEvd3','injury_type_92'), 
            '099' => '(099) '.Yii::t('HRModule.SafeEvd3','injury_type_99'), 
            '100' => '(100) '.Yii::t('HRModule.SafeEvd3','injury_type_100'), 
            '101' => '(101) '.Yii::t('HRModule.SafeEvd3','injury_type_101'), 
            '102' => '(102) '.Yii::t('HRModule.SafeEvd3','injury_type_102'), 
            '103' => '(103) '.Yii::t('HRModule.SafeEvd3','injury_type_103'), 
            '109' => '(109) '.Yii::t('HRModule.SafeEvd3','injury_type_109'), 
            '110' => '(110) '.Yii::t('HRModule.SafeEvd3','injury_type_110'), 
            '111' => '(111) '.Yii::t('HRModule.SafeEvd3','injury_type_111'), 
            '112' => '(112) '.Yii::t('HRModule.SafeEvd3','injury_type_112'), 
            '119' => '(119) '.Yii::t('HRModule.SafeEvd3','injury_type_119'), 
            '120' => '(120) '.Yii::t('HRModule.SafeEvd3','injury_type_120'), 
            '999' => '(999) '.Yii::t('HRModule.SafeEvd3','injury_type_999'), 
        ];
        
        self::$injury_part_of_body_list = [ 
            '00' => '(00) '.Yii::t('HRModule.SafeEvd3','injury_part_of_body_0'), 
            '10' => '(10) '.Yii::t('HRModule.SafeEvd3','injury_part_of_body_10'), 
            '11' => '(11) '.Yii::t('HRModule.SafeEvd3','injury_part_of_body_11'), 
            '12' => '(12) '.Yii::t('HRModule.SafeEvd3','injury_part_of_body_12'), 
            '13' => '(13) '.Yii::t('HRModule.SafeEvd3','injury_part_of_body_13'), 
            '14' => '(14) '.Yii::t('HRModule.SafeEvd3','injury_part_of_body_14'), 
            '15' => '(15) '.Yii::t('HRModule.SafeEvd3','injury_part_of_body_15'), 
            '18' => '(18) '.Yii::t('HRModule.SafeEvd3','injury_part_of_body_18'), 
            '19' => '(19) '.Yii::t('HRModule.SafeEvd3','injury_part_of_body_19'), 
            '20' => '(20) '.Yii::t('HRModule.SafeEvd3','injury_part_of_body_20'), 
            '29' => '(29) '.Yii::t('HRModule.SafeEvd3','injury_part_of_body_29'), 
            '30' => '(30) '.Yii::t('HRModule.SafeEvd3','injury_part_of_body_30'), 
            '39' => '(39) '.Yii::t('HRModule.SafeEvd3','injury_part_of_body_39'), 
            '40' => '(40) '.Yii::t('HRModule.SafeEvd3','injury_part_of_body_40'), 
            '41' => '(41) '.Yii::t('HRModule.SafeEvd3','injury_part_of_body_41'), 
            '42' => '(42) '.Yii::t('HRModule.SafeEvd3','injury_part_of_body_42'), 
            '43' => '(43) '.Yii::t('HRModule.SafeEvd3','injury_part_of_body_43'), 
            '48' => '(48) '.Yii::t('HRModule.SafeEvd3','injury_part_of_body_48'), 
            '49' => '(49) '.Yii::t('HRModule.SafeEvd3','injury_part_of_body_49'), 
            '50' => '(50) '.Yii::t('HRModule.SafeEvd3','injury_part_of_body_50'), 
            '51' => '(51) '.Yii::t('HRModule.SafeEvd3','injury_part_of_body_51'), 
            '52' => '(52) '.Yii::t('HRModule.SafeEvd3','injury_part_of_body_52'), 
            '53' => '(53) '.Yii::t('HRModule.SafeEvd3','injury_part_of_body_53'), 
            '54' => '(54) '.Yii::t('HRModule.SafeEvd3','injury_part_of_body_54'), 
            '55' => '(55) '.Yii::t('HRModule.SafeEvd3','injury_part_of_body_55'), 
            '58' => '(58) '.Yii::t('HRModule.SafeEvd3','injury_part_of_body_58'), 
            '59' => '(59) '.Yii::t('HRModule.SafeEvd3','injury_part_of_body_59'), 
            '60' => '(60) '.Yii::t('HRModule.SafeEvd3','injury_part_of_body_60'), 
            '61' => '(61) '.Yii::t('HRModule.SafeEvd3','injury_part_of_body_61'), 
            '62' => '(62) '.Yii::t('HRModule.SafeEvd3','injury_part_of_body_62'), 
            '63' => '(63) '.Yii::t('HRModule.SafeEvd3','injury_part_of_body_63'), 
            '64' => '(64) '.Yii::t('HRModule.SafeEvd3','injury_part_of_body_64'), 
            '65' => '(65) '.Yii::t('HRModule.SafeEvd3','injury_part_of_body_65'), 
            '68' => '(68) '.Yii::t('HRModule.SafeEvd3','injury_part_of_body_68'), 
            '69' => '(69) '.Yii::t('HRModule.SafeEvd3','injury_part_of_body_69'), 
            '70' => '(70) '.Yii::t('HRModule.SafeEvd3','injury_part_of_body_70'), 
            '71' => '(71) '.Yii::t('HRModule.SafeEvd3','injury_part_of_body_71'), 
            '72' => '(72) '.Yii::t('HRModule.SafeEvd3','injury_part_of_body_72'), 
            '99' => '(99) '.Yii::t('HRModule.SafeEvd3','injury_part_of_body_99'),  
        ];
        
        self::$mt3_days_unable_work_list = [
            1 => '(1) '.Yii::t('HRModule.SafeEvd3','mt3_days_unable_work_1'),
            2 => '(2) '.Yii::t('HRModule.SafeEvd3','mt3_days_unable_work_2')
        ];
        
        self::$estimated_lost_days_list = [ 
            '000' => '(000) '.Yii::t('HRModule.SafeEvd3','estimated_lost_days_0'), 
            '004-182' => '(004-182) '.Yii::t('HRModule.SafeEvd3','estimated_lost_days_004-182'), 
            'A01' => '(A01) '.Yii::t('HRModule.SafeEvd3','estimated_lost_days_A01'), 
            'A02' => '(A02) '.Yii::t('HRModule.SafeEvd3','estimated_lost_days_A02'), 
            'A03' => '(A03) '.Yii::t('HRModule.SafeEvd3','estimated_lost_days_A03'), 
            'A04' => '(A04) '.Yii::t('HRModule.SafeEvd3','estimated_lost_days_A04'), 
            'A05' => '(A05) '.Yii::t('HRModule.SafeEvd3','estimated_lost_days_A05'), 
            'A06' => '(A06) '.Yii::t('HRModule.SafeEvd3','estimated_lost_days_A06'), 
            '997' => '(997) '.Yii::t('HRModule.SafeEvd3','estimated_lost_days_997'), 
            '998' => '(998) '.Yii::t('HRModule.SafeEvd3','estimated_lost_days_998'), 
        ];
        
        self::$mark_injury_list = [
            1 => '(1) '. Yii::t('HRModule.SafeEvd3','mark_injury_1'), 
            2 => '(2) '. Yii::t('HRModule.SafeEvd3','mark_injury_2'), 
            3 => '(3) '. Yii::t('HRModule.SafeEvd3','mark_injury_3'),
            4 => '(4) '. Yii::t('HRModule.SafeEvd3','mark_injury_4'), //stara evidencija
        ];
    }
    
    public static function markInjuriesWithoutOldItems() 
    {   
        $mark_injury_list = SafeEvd3Lists::$mark_injury_list;
        foreach (SafeEvd3Lists::$mark_injury_old_list as $old_mark_injury) 
        {
            unset($mark_injury_list[$old_mark_injury]);
        }
        return $mark_injury_list;
    }
    
    public static function typeInjuriesWithoutOldItems() 
    {   
        $type_injury_list = SafeEvd3Lists::$injury_type_list;
        foreach (SafeEvd3Lists::$injury_type_old_list as $old_injury_type) 
        {
            unset($type_injury_list[$old_injury_type]);
        }
        return $type_injury_list;
    }
}