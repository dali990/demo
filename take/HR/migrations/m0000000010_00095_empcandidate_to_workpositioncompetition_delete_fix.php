<?php

class m0000000010_00095_empcandidate_to_workpositioncompetition_delete_fix extends CDbMigration
{
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        Yii::app()->db->createCommand(
"
CREATE OR REPLACE FUNCTION hr.employee_candidate_to_work_position_competition_before_delete()
  RETURNS trigger AS
\$BODY$
DECLARE
BEGIN
	DELETE FROM hr.competition_grades WHERE employee_candidate_to_work_position_competition_id=OLD.id;
	RETURN OLD;
END;\$BODY$
  LANGUAGE plpgsql;

CREATE TRIGGER employee_candidate_to_work_position_competition_before_delete
  BEFORE DELETE
  ON hr.employee_candidate_to_work_position_competition
  FOR EACH ROW
  EXECUTE PROCEDURE hr.employee_candidate_to_work_position_competition_before_delete();
"
        )->execute();
    }

    public function safeDown()
    {
//        Yii::app()->db->createCommand(
//"
//
//"
//        )->execute();
        echo "m0000000010_00095_empcandidate_to_workpositioncompetition_delete_fix does not support migration down.\n";
        return false;
    }
}