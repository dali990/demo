<?php

class m0000000005_00009_entrydoorlog_cards_additions extends EDbMigration
{
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
            Yii::app()->db->createCommand(
'
ALTER TABLE hr.entry_door_cards ADD COLUMN comment TEXT;
ALTER TABLE hr.entry_door_cards ADD COLUMN confirmed boolean NOT NULL DEFAULT FALSE;
ALTER TABLE hr.entry_door_cards ADD COLUMN confirmed_user_id integer;
ALTER TABLE hr.entry_door_cards ADD COLUMN confirmed_timestamp timestamp without time zone;
ALTER TABLE hr.entry_door_cards RENAME COLUMN card_id TO card_number;
ALTER TABLE hr.entry_door_logs ALTER COLUMN partner_id DROP NOT NULL;
'
            )->execute();
    }

    public function safeDown()
    {
            Yii::app()->db->createCommand(
'
ALTER TABLE hr.entry_door_logs ALTER COLUMN partner_id SET NOT NULL;
ALTER TABLE hr.entry_door_cards RENAME COLUMN card_number TO card_id;
ALTER TABLE hr.entry_door_cards DROP COLUMN confirmed_timestamp;
ALTER TABLE hr.entry_door_cards DROP COLUMN confirmed_user_id;
ALTER TABLE hr.entry_door_cards DROP COLUMN confirmed;
ALTER TABLE hr.entry_door_cards DROP COLUMN comment;
'
            )->execute();
    }
}