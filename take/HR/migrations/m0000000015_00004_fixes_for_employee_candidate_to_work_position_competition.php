<?php

class m0000000015_00004_fixes_for_employee_candidate_to_work_position_competition extends CDbMigration
{
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        Yii::app()->db->createCommand(
<<<'SIMAMIGRATESQL'
        ALTER TABLE hr.employee_candidate_to_work_position_competition DROP CONSTRAINT employee_candidate_to_work_position_competition_status_check;
        ALTER TABLE hr.employee_candidate_to_work_position_competition
          ADD CONSTRAINT employee_candidate_to_work_position_competition_status_check CHECK (status = ANY (ARRAY['APPLIED'::text, 'NOT_INVITED_TO_INTERVIEW'::text, 'TO_INVITE_TO_INTERVIEW'::text, 'INVITED_TO_INTERVIEW'::text, 'CONFIRMED_INTERVIEW'::text, 'NOT_CAME'::text, 'DONE_INTERVIEW'::text, 'NOT_PASSED_INTERVIEW'::text, 'OFFERED_JOB'::text, 'REJECTED_JOB'::text, 'ACCEPTED_JOB'::text]));
SIMAMIGRATESQL
        )->execute();
    }

    public function safeDown()
    {
        Yii::app()->db->createCommand(
<<<'SIMAMIGRATESQL'
        ALTER TABLE hr.employee_candidate_to_work_position_competition DROP CONSTRAINT employee_candidate_to_work_position_competition_status_check;
        ALTER TABLE hr.employee_candidate_to_work_position_competition
          ADD CONSTRAINT employee_candidate_to_work_position_competition_status_check CHECK (status = ANY (ARRAY['APPLIED'::text, 'NOT_INVITED_TO_INTERVIEW'::text, 'TO_INVITE_TO_INTERVIEW'::text, 'INVITED_TO_INTERVIEW'::text, 'NOT_CAME'::text, 'DONE_INTERVIEW'::text, 'NOT_PASSED_INTERVIEW'::text, 'OFFERED_JOB'::text, 'REJECTED_JOB'::text, 'ACCEPTED_JOB'::text]));
SIMAMIGRATESQL
        )->execute();
//        echo "m0000000015_00004_fixes_for_employee_candidate_to_work_position_competition does not support migration down.\n";
//        return false;
    }
}