<?php

class m0000000015_00000_absences_notifs_action_fix extends CDbMigration
{
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        Yii::app()->db->createCommand(
<<<'SIMAMIGRATESQL'
        update public.notifications 
        set params=replace(params, (params::JSON)->>'action', '{"action":"defaultLayout","action_id":{"model_name":"Absence", "model_id":' || ((((params::JSON)->>'action')::JSON)->>'action_id') || '}}') 
        where params like '%"action":""%' and params like '%absences%'
SIMAMIGRATESQL
        )->execute();
    }

    public function safeDown()
    {
//        Yii::app()->db->createCommand(
//<<<'SIMAMIGRATESQL'
//
//SIMAMIGRATESQL
//        )->execute();
        echo "m0000000015_00000_absences_notifs_action_fix does not support migration down.\n";
        return false;
    }
}