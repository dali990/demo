<?php

class m0000000003_00001_entry_door_logs extends EDbMigration
{
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
            Yii::app()->db->createCommand('CREATE TABLE hr.entry_door_logs
(
  id serial NOT NULL,
  "time" timestamp without time zone NOT NULL,
  employee_id integer NOT NULL,
  additional_data text,
  CONSTRAINT set_door_pkey PRIMARY KEY (id),
  CONSTRAINT entry_door_logs_employee_id_fkey FOREIGN KEY (employee_id)
      REFERENCES employees (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE RESTRICT
);')->execute();
	}

	public function safeDown()
	{
           Yii::app()->db->createCommand('DROP TABLE hr.entry_door_logs;')->execute();
	}
}