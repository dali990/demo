<?php

class m0000000003_00006_introductiontobusiness_add_year extends EDbMigration
{
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
            Yii::app()->db->createCommand(
'
ALTER TABLE hr.introduction_into_business_programs ADD COLUMN year integer;

ALTER TABLE hr.introduction_into_business_programs
  ADD CONSTRAINT introduction_into_business_programs_number_year_key UNIQUE(number, year);
'
            )->execute();
            
            
            $introductions = IntroductionIntoBusinessProgram::model()->findAll();
            foreach($introductions as $introduction)
            {
                $wc = $introduction->work_contract;
                
                if(empty($wc))
                {
                    $introduction->delete();
                    continue;
                }
                
                if(!empty($introduction->year))
                {
                    continue;
                }
                
                $year = (new DateTime($wc->start_date))->format('Y');
                
                $introduction->year = $year;
                $introduction->save();
            }
            
            Yii::app()->db->createCommand(
'
ALTER TABLE hr.introduction_into_business_programs ALTER COLUMN year SET NOT NULL;
'
            )->execute();
	}

	public function safeDown()
	{
            Yii::app()->db->createCommand(
'
CREATE OR REPLACE FUNCTION hr.new_introduction_into_business_programs_number()
  RETURNS integer AS
$BODY$
DECLARE
	num integer;
BEGIN
	select COALESCE(max(number),0)+1 from hr.introduction_into_business_programs into num;
	RETURN num;
END;
$BODY$
  LANGUAGE plpgsql;    


ALTER TABLE hr.introduction_into_business_programs DROP CONSTRAINT introduction_into_business_programs_number_year_key;    

ALTER TABLE hr.introduction_into_business_programs DROP COLUMN year;
'
            )->execute();
	}
}