<?php

class m0000000034_00013_svp_fields_remove_defaults extends CDbMigration
{
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        Yii::app()->db->createCommand(
<<<'SIMAMIGRATESQL'
            ALTER TABLE hr.work_contracts ALTER COLUMN employment_code DROP DEFAULT;
            ALTER TABLE hr.work_contracts ALTER COLUMN benefit DROP DEFAULT;
            ALTER TABLE hr.work_contracts ALTER COLUMN benefited_work_experience DROP DEFAULT;
SIMAMIGRATESQL
        )->execute();
    }

    public function safeDown()
    {
//        Yii::app()->db->createCommand(
//<<<'SIMAMIGRATESQL'
//
//SIMAMIGRATESQL
//        )->execute();
        echo "m0000000034_00013_svp_fields_remove_defaults does not support migration down.\n";
        return false;
    }
}