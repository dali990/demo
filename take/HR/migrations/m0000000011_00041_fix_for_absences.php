<?php

class m0000000011_00041_fix_for_absences extends CDbMigration
{
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        Yii::app()->db->createCommand(
"
        update public.comment_threads set model='AbsenceAnnualLeave' where model='AnnualLeave';
        update public.comment_threads set model='AbsenceFreeDays' where model='FreeDays';
        update public.comment_threads set model='AbsenceSickLeave' where model='SickLeave';
"
        )->execute();
    }

    public function safeDown()
    {
//        Yii::app()->db->createCommand(
//"
//
//"
//        )->execute();
        echo "m0000000011_00041_fix_for_absences does not support migration down.\n";
        return false;
    }
}