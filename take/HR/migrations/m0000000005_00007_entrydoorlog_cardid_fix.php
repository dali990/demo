<?php

class m0000000005_00007_entrydoorlog_cardid_fix extends EDbMigration
{
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
            Yii::app()->db->createCommand(
'
ALTER TABLE hr.entry_door_logs RENAME COLUMN card_id TO entry_door_card_id;

ALTER TABLE hr.entry_door_logs
  ADD CONSTRAINT entry_door_logs_entry_door_card_id_fkey FOREIGN KEY (entry_door_card_id)
      REFERENCES hr.entry_door_cards (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE RESTRICT;
'
            )->execute();
	}

	public function safeDown()
	{
            Yii::app()->db->createCommand(
'
ALTER TABLE hr.entry_door_logs DROP CONSTRAINT entry_door_logs_entry_door_card_id_fkey;

ALTER TABLE hr.entry_door_logs RENAME COLUMN entry_door_card_id TO card_id;
'
            )->execute();
	}
}