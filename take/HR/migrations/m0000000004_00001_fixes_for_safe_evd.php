<?php

class m0000000004_00001_fixes_for_safe_evd extends EDbMigration
{
//	public function up()
//	{
//	}
//
//	public function down()
//	{
//		echo "m0000000004_00001_fixes_for_safe_evd does not support migration down.\n";
//		return false;
//	}
	
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
            Yii::app()->db->createCommand('SET search_path = hr, pg_catalog;' 
                    . 'CREATE TABLE international_diagnosis_of_professional_diseases (
	id serial NOT NULL,
	code text NOT NULL,
	name text NOT NULL,
	description text
);
ALTER SEQUENCE international_diagnosis_of_professional_diseases_id_seq
	OWNED BY international_diagnosis_of_professional_diseases.id;
ALTER TABLE international_diagnosis_of_professional_diseases
	ADD CONSTRAINT international_diagnosis_of_professional_diseases_pkey PRIMARY KEY (id);
CREATE TRIGGER i_am_called_by_everyone
	AFTER INSERT OR UPDATE OR DELETE ON international_diagnosis_of_professional_diseases
	FOR EACH ROW
	EXECUTE PROCEDURE public.i_am_called_by_everyone();

CREATE TABLE international_diagnosis_of_work_diseases (
	id serial NOT NULL,
	code text NOT NULL,
	name text NOT NULL,
	description text
);
ALTER SEQUENCE international_diagnosis_of_work_diseases_id_seq
	OWNED BY international_diagnosis_of_work_diseases.id;
ALTER TABLE international_diagnosis_of_work_diseases
	ADD CONSTRAINT international_diagnosis_of_work_diseases_pkey PRIMARY KEY (id);
CREATE TRIGGER i_am_called_by_everyone
	AFTER INSERT OR UPDATE OR DELETE ON international_diagnosis_of_work_diseases
	FOR EACH ROW
	EXECUTE PROCEDURE public.i_am_called_by_everyone();

CREATE TABLE medical_examination_referrals (
	id integer NOT NULL,
	description text,
        referral_type text DEFAULT \'SAFE_EVD_2\'::text NOT NULL
);
ALTER TABLE medical_examination_referrals
	ADD CONSTRAINT medical_examination_referrals_pkey PRIMARY KEY (id);
ALTER TABLE medical_examination_referrals
	ADD CONSTRAINT medical_examination_referrals_id_fkey FOREIGN KEY (id) REFERENCES files.files(id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE medical_examination_referrals
	ADD CONSTRAINT medical_examination_referrals_referral_type_check CHECK ((referral_type = ANY (ARRAY[\'SAFE_EVD_2\'::text, \'SAFE_EVD_13\'::text])));
CREATE TRIGGER i_am_called_by_everyone
	AFTER INSERT OR UPDATE OR DELETE ON medical_examination_referrals
	FOR EACH ROW
	EXECUTE PROCEDURE public.i_am_called_by_everyone();

CREATE TABLE medical_examination_referrals_to_employees (
	id serial NOT NULL,
	employee_id integer NOT NULL,
	medical_examination_referral_id integer NOT NULL,
	description text
);
ALTER SEQUENCE medical_examination_referrals_to_employees_id_seq
	OWNED BY medical_examination_referrals_to_employees.id;
ALTER TABLE medical_examination_referrals_to_employees
	ADD CONSTRAINT medical_examination_referrals_to_employees_pkey PRIMARY KEY (id);
ALTER TABLE medical_examination_referrals_to_employees
	ADD CONSTRAINT medical_examination_referrals_to_employees_employee_id_fkey FOREIGN KEY (employee_id) REFERENCES public.employees(id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE medical_examination_referrals_to_employees
	ADD CONSTRAINT medical_examination_referrals_to_employees_medical_examination_ FOREIGN KEY (medical_examination_referral_id) REFERENCES medical_examination_referrals(id) ON UPDATE CASCADE ON DELETE RESTRICT;
CREATE TRIGGER i_am_called_by_everyone
	AFTER INSERT OR UPDATE OR DELETE ON medical_examination_referrals_to_employees
	FOR EACH ROW
	EXECUTE PROCEDURE public.i_am_called_by_everyone();

CREATE TABLE mode_injuries (
	id serial NOT NULL,
	code text NOT NULL,
	name text NOT NULL,
	description text
);
ALTER SEQUENCE mode_injuries_id_seq
	OWNED BY mode_injuries.id;
ALTER TABLE mode_injuries
	ADD CONSTRAINT mode_injuries_pkey PRIMARY KEY (id);
CREATE TRIGGER i_am_called_by_everyone
	AFTER INSERT OR UPDATE OR DELETE ON mode_injuries
	FOR EACH ROW
	EXECUTE PROCEDURE public.i_am_called_by_everyone();        

CREATE TABLE source_injuries (
	id serial NOT NULL,
	code text NOT NULL,
	name text NOT NULL,
	description text
);
ALTER SEQUENCE source_injuries_id_seq
	OWNED BY source_injuries.id;
ALTER TABLE source_injuries
	ADD CONSTRAINT source_injuries_pkey PRIMARY KEY (id);
CREATE TRIGGER i_am_called_by_everyone
	AFTER INSERT OR UPDATE OR DELETE ON source_injuries
	FOR EACH ROW
	EXECUTE PROCEDURE public.i_am_called_by_everyone();

CREATE TABLE reasons_for_enforcement_training (
	id serial NOT NULL,
	code text NOT NULL,
	name text NOT NULL,
	description text
);
ALTER SEQUENCE reasons_for_enforcement_training_id_seq
	OWNED BY reasons_for_enforcement_training.id;
ALTER TABLE reasons_for_enforcement_training
	ADD CONSTRAINT reasons_for_enforcement_training_pkey PRIMARY KEY (id);
CREATE TRIGGER i_am_called_by_everyone
	AFTER INSERT OR UPDATE OR DELETE ON reasons_for_enforcement_training
	FOR EACH ROW
	EXECUTE PROCEDURE public.i_am_called_by_everyone();
        
CREATE TABLE safe_evd6_to_reasons_for_enforcement_training (
	reason_for_enforcement_training_id integer NOT NULL,
	safe_evd6_id integer NOT NULL
);
ALTER TABLE safe_evd6_to_reasons_for_enforcement_training
	ADD CONSTRAINT safe_evd6_to_reasons_for_enforcement_training_pkey PRIMARY KEY (reason_for_enforcement_training_id, safe_evd6_id);
ALTER TABLE safe_evd6_to_reasons_for_enforcement_training
	ADD CONSTRAINT safe_evd6_to_reasons_for_enforcement_training_reason_for_enforc FOREIGN KEY (reason_for_enforcement_training_id) REFERENCES reasons_for_enforcement_training(id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE safe_evd6_to_reasons_for_enforcement_training
	ADD CONSTRAINT safe_evd6_to_reasons_for_enforcement_training_safe_evd6_id_fk FOREIGN KEY (safe_evd6_id) REFERENCES safe_evd_6(id) ON UPDATE CASCADE ON DELETE RESTRICT;
CREATE TRIGGER i_am_called_by_everyone
	AFTER INSERT OR UPDATE OR DELETE ON safe_evd6_to_reasons_for_enforcement_training
	FOR EACH ROW
	EXECUTE PROCEDURE public.i_am_called_by_everyone();
        SET search_path = public, pg_catalog;
')->execute();
            
            
            
            //SAFE_EVD_1
            $safe_evd1 = SafeEvd1::model()->findAll();
            foreach ($safe_evd1 as $value)
            {
                if (empty($value->work_position->description) && !empty($value->comment))
                {
                    $value->work_position->description = $value->comment;
                    $value->work_position->save();
                }
            }
            Yii::app()->db->createCommand('SET search_path = hr, pg_catalog;'
                . 'ALTER TABLE safe_evd_1
                   DROP COLUMN comment,
                   ALTER COLUMN work_position_id SET NOT NULL,
                   ALTER COLUMN work_danger_id SET NOT NULL;
                   ALTER TABLE safe_evd_1
                   ADD CONSTRAINT safe_evd_1_work_danger_id_fkey FOREIGN KEY (work_danger_id) REFERENCES safe_work_dangers(id) ON UPDATE CASCADE ON DELETE RESTRICT;
                   CREATE OR REPLACE FUNCTION safe_evd_1_after_insert_update_delete() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
	risc BOOLEAN := FALSE;
	return_rec RECORD;	
BEGIN	
	IF (TG_OP = \'UPDATE\' OR TG_OP = \'INSERT\') 
	THEN		
		return_rec := NEW;
	ELSIF (TG_OP = \'DELETE\') 
	THEN		
		return_rec := OLD;	
	END IF;

	IF ((select count(*) from hr.safe_evd_1 where work_position_id=return_rec.work_position_id) > 0)
	THEN
		risc := TRUE;
	END IF;
	
	UPDATE legal.work_positions
	SET enlarged_risk=risc
	WHERE id=return_rec.work_position_id;
	
	RETURN return_rec;
END;
$$;
                   CREATE TRIGGER safe_evd_1_after_insert_update_delete
                   AFTER INSERT OR UPDATE OR DELETE ON safe_evd_1
                   FOR EACH ROW
                   EXECUTE PROCEDURE safe_evd_1_after_insert_update_delete();
                   update legal.work_positions set enlarged_risk=false;
                   update hr.safe_evd_1 set id=id;
                   SET search_path = public, pg_catalog;')->execute();

            
            
            //SAFE_EVD_2
            $safe_evd2 = SafeEvd2::model()->findAll();
            foreach ($safe_evd2 as $value)
            {
                if (!empty($value->medical_referral_id))
                {
                    $medical_examination_referral = MedicalExaminationReferral::model()->findByPk($value->medical_referral_id);
                    if (is_null($medical_examination_referral))
                    {
                        $medical_examination_referral = new MedicalExaminationReferral();
                        $medical_examination_referral->id = $value->medical_referral_id;
                        $medical_examination_referral->save();
                    }
                }
            }
            Yii::app()->db->createCommand('SET search_path = hr, pg_catalog;'
                . 'ALTER TABLE safe_evd_2
                   DROP CONSTRAINT safe_evd_2_medical_report_id_fkey;
                   ALTER TABLE safe_evd_2
                   ADD CONSTRAINT safe_evd_2_medical_referral_id_fkey FOREIGN KEY (medical_referral_id) REFERENCES medical_examination_referrals(id) ON UPDATE CASCADE ON DELETE RESTRICT;
                   SET search_path = public, pg_catalog;')->execute();            

            
            
            //SAFE_EVD_3
            Yii::app()->db->createCommand('SET search_path = hr, pg_catalog;'
                    . 'ALTER TABLE safe_evd_3	
	ADD COLUMN source_injury_id integer,
	ADD COLUMN mode_injury_id integer;
        ALTER TABLE hr.safe_evd_3
            RENAME COLUMN source_injury TO source_injury_text;
        ALTER TABLE hr.safe_evd_3
            RENAME COLUMN mode_injury TO mode_injury_text;SET search_path = public, pg_catalog;')->execute();
            $safe_evd3 = SafeEvd3::model()->findAll();
            foreach ($safe_evd3 as $value)
            {
                if (!empty($value->source_injury_text))
                {
                    $source_injury = SourceInjury::model()->findByAttributes(['name'=>$value->source_injury_text]);
                    if (is_null($source_injury))
                    {
                        $source_injury = new SourceInjury();
                        $source_injury->name = $value->source_injury_text;
                        $source_injury->code = $value->source_injury_text;
                        $source_injury->save();
                    }
                    $value->source_injury_id = $source_injury->id;
                    $value->save();
                }
                if (!empty($value->mode_injury_text))
                {
                    $mode_injury = ModeInjury::model()->findByAttributes(['name'=>$value->mode_injury_text]);
                    if (is_null($mode_injury))
                    {
                        $mode_injury = new ModeInjury();
                        $mode_injury->name = $value->mode_injury_text;
                        $mode_injury->code = $value->mode_injury_text;
                        $mode_injury->save();
                    }
                    $value->mode_injury_id = $mode_injury->id;
                    $value->save();
                }
            }
            Yii::app()->db->createCommand('SET search_path = hr, pg_catalog;'
                    . 'ALTER TABLE safe_evd_3	
	DROP COLUMN source_injury_text,
	DROP COLUMN mode_injury_text;
        ALTER TABLE safe_evd_3
	ADD CONSTRAINT safe_evd_3_mode_injury_id_fkey FOREIGN KEY (mode_injury_id) REFERENCES mode_injuries(id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE safe_evd_3
	ADD CONSTRAINT safe_evd_3_source_injury_id_fkey FOREIGN KEY (source_injury_id) REFERENCES source_injuries(id) ON UPDATE CASCADE ON DELETE RESTRICT;
        SET search_path = public, pg_catalog;')->execute();
            
            
            
            //SAFE_EVD_4
            Yii::app()->db->createCommand('SET search_path = hr, pg_catalog;'
                    . 'ALTER TABLE safe_evd_4
	DROP COLUMN diagnosis,
	ADD COLUMN international_diagnosis_id integer;
        ALTER TABLE safe_evd_4
	ADD CONSTRAINT safe_evd_4_international_diagnosis_id_fkey FOREIGN KEY (international_diagnosis_id) REFERENCES international_diagnosis_of_professional_diseases(id) ON UPDATE CASCADE ON DELETE RESTRICT;
        SET search_path = public, pg_catalog;')->execute();
            
            
            
            //SAFE_EVD_5
            Yii::app()->db->createCommand('SET search_path = hr, pg_catalog;'
                    . 'ALTER TABLE safe_evd_5
	DROP COLUMN diagnosis,
	ADD COLUMN international_diagnosis_id integer;
        ALTER TABLE safe_evd_5
	ADD CONSTRAINT safe_evd_5_international_diagnosis_id_fkey FOREIGN KEY (international_diagnosis_id) REFERENCES international_diagnosis_of_work_diseases(id) ON UPDATE CASCADE ON DELETE RESTRICT;
        SET search_path = public, pg_catalog;')->execute();
            
            
            
            //SAFE_EVD_6
            Yii::app()->db->createCommand('SET search_path = hr, pg_catalog;'
                    . 'ALTER TABLE safe_evd_6
	DROP CONSTRAINT safe_evd_6_person_id_fkey;
        ALTER TABLE hr.safe_evd_6
            RENAME COLUMN person_id TO employee_id;
            ALTER TABLE safe_evd_6
	ADD CONSTRAINT safe_evd_6_employee_id_fkey FOREIGN KEY (employee_id) REFERENCES public.employees(id) ON UPDATE CASCADE ON DELETE RESTRICT;
        SET search_path = public, pg_catalog;')->execute();
            $safe_evd6 = SafeEvd6::model()->findAll();
            foreach ($safe_evd6 as $value)
            {
                if (!empty($value->reason))
                {
                    $reason = ReasonForEnforcementTraining::model()->findByAttributes(['name'=>$value->reason]);
                    if (is_null($reason))
                    {
                        $reason = new ReasonForEnforcementTraining();
                        $reason->name = $value->reason;
                        $reason->code = $value->reason;
                        $reason->save();
                    }
                    $safe_evd_6_to_reason = SafeEvd6ToReasonForEnforcementTraining::model()->findByAttributes([
                        'reason_for_enforcement_training_id'=>$reason->id,
                        'safe_evd6_id'=>$value->id
                    ]);
                    if (is_null($safe_evd_6_to_reason))
                    {
                        $safe_evd_6_to_reason = new SafeEvd6ToReasonForEnforcementTraining();
                        $safe_evd_6_to_reason->reason_for_enforcement_training_id = $reason->id;
                        $safe_evd_6_to_reason->safe_evd6_id = $value->id;
                        $safe_evd_6_to_reason->save();
                    }
                }
            }
            
            
            //SAFE_EVD_8
            Yii::app()->db->createCommand('SET search_path = hr, pg_catalog;'
                    . 'CREATE TABLE safe_evd_8 (
	id integer NOT NULL,
	"data" text NOT NULL,
	num_expert_report text,
	date_control date,
	date_next_control date,
	comment text,
	description text
);
ALTER TABLE safe_evd_8
	ADD CONSTRAINT safe_evd_8_pkey PRIMARY KEY (id);
ALTER TABLE safe_evd_8
	ADD CONSTRAINT safe_evd_8_id_fkey FOREIGN KEY (id) REFERENCES files.files(id) ON UPDATE CASCADE ON DELETE RESTRICT;
CREATE TRIGGER i_am_called_by_everyone
	AFTER INSERT OR UPDATE OR DELETE ON safe_evd_8
	FOR EACH ROW
	EXECUTE PROCEDURE public.i_am_called_by_everyone();
CREATE TABLE safe_evd_10 (
	id serial NOT NULL,
	"data" text NOT NULL,
	date_control date,
	date_next_control date,
	comment text,
	description text
);
ALTER SEQUENCE safe_evd_10_id_seq
	OWNED BY safe_evd_10.id;
ALTER TABLE safe_evd_10
	ADD CONSTRAINT safe_evd_10_pkey PRIMARY KEY (id);
CREATE TRIGGER i_am_called_by_everyone
	AFTER INSERT OR UPDATE OR DELETE ON safe_evd_10
	FOR EACH ROW
	EXECUTE PROCEDURE public.i_am_called_by_everyone(); SET search_path = public, pg_catalog;')->execute();                                                            
            $safe_evd_8_10 = SafeEvd8_10::model()->findAll();
            foreach ($safe_evd_8_10 as $value) 
            {
                if ($value->type_evd === 8)
                {
                    $safe_evd_8 = new SafeEvd8();
                    $safe_evd_8->data = $value->data;
                    $safe_evd_8->num_expert_report = $value->num_expert_report;
                    $safe_evd_8->date_control = $value->date_control;
                    $safe_evd_8->date_next_control = $value->date_next_control;
                    $safe_evd_8->comment = $value->comment;
                    $safe_evd_8->description = $value->description;
                    $safe_evd_8->save();
                }
                else if ($value->type_evd === 10)
                {
                    $safe_evd_10 = new SafeEvd10();
                    $safe_evd_10->data = $value->data;                
                    $safe_evd_10->date_control = $value->date_control;
                    $safe_evd_10->date_next_control = $value->date_next_control;
                    $safe_evd_10->comment = $value->comment;
                    $safe_evd_10->description = $value->description;
                    $safe_evd_10->save();
                }
            }
            Yii::app()->db->createCommand('SET search_path = hr, pg_catalog;'
                    . 'DROP TABLE safe_evd_8_10; SET search_path = public, pg_catalog;')->execute();
            
            
            //SAFE_EVD_9
            Yii::app()->db->createCommand('SET search_path = hr, pg_catalog;'
                    . 'ALTER TABLE safe_evd_9
	ALTER COLUMN id DROP DEFAULT;
        DROP SEQUENCE safe_evd_9_id_seq; SET search_path = public, pg_catalog;')->execute();
            //skine se serial sa id-a
            $safe_evd9 = SafeEvd9::model()->findAll();
            foreach ($safe_evd9 as $value)
            {
                $file = new File();
                $file->name = $value->modelLabel();
                $file->save();            
                $value->id = $file->id;
                $value->save();
            }

            Yii::app()->db->createCommand('SET search_path = hr, pg_catalog;'
                    . 'ALTER TABLE safe_evd_9
	ADD CONSTRAINT safe_evd_9_id_fkey FOREIGN KEY (id) REFERENCES files.files(id) ON UPDATE CASCADE ON DELETE RESTRICT; SET search_path = public, pg_catalog;')->execute();           
	
            
            
            //SAFE_EVD_13
            Yii::app()->db->createCommand('SET search_path = hr, pg_catalog;'
                    . 'CREATE TABLE safe_evd_13 (
	id integer NOT NULL,
	employee_id integer NOT NULL,
	work_position_id integer NOT NULL,
	report_code text,
	report_date date,
	next_report_date date,
	medical_referral_id integer,
	actions text,
	description text
);
ALTER TABLE safe_evd_13
	ADD CONSTRAINT safe_evd_13_pkey PRIMARY KEY (id);
ALTER TABLE safe_evd_13
	ADD CONSTRAINT safe_evd_13_employee_id_fkey FOREIGN KEY (employee_id) REFERENCES public.employees(id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE safe_evd_13
	ADD CONSTRAINT safe_evd_13_id_fkey FOREIGN KEY (id) REFERENCES files.files(id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE safe_evd_13
	ADD CONSTRAINT safe_evd_13_medical_referral_id_fkey FOREIGN KEY (medical_referral_id) REFERENCES medical_examination_referrals(id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE safe_evd_13
	ADD CONSTRAINT safe_evd_13_work_position_id_fkey FOREIGN KEY (work_position_id) REFERENCES legal.work_positions(id) ON UPDATE CASCADE ON DELETE RESTRICT;
CREATE TRIGGER i_am_called_by_everyone
	AFTER INSERT OR UPDATE OR DELETE ON safe_evd_13
	FOR EACH ROW
	EXECUTE PROCEDURE public.i_am_called_by_everyone();
SET search_path = public, pg_catalog;')->execute();
        
            
         
          
            //RENAME SAFE_EVD_11_12_13_14 u SAFE_EVD_11_12_14
            Yii::app()->db->createCommand('SET search_path = hr, pg_catalog;'
                    . 'ALTER SEQUENCE safe_evd_11_12_13_14_id_seq RENAME TO safe_evd_11_12_14_id_seq;'
                    . 'ALTER TABLE safe_evd_11_12_13_14 RENAME CONSTRAINT safe_evd_11_12_13_pkey TO safe_evd_11_12_14_pkey;'
                    . 'ALTER TABLE safe_evd_11_12_13_14 RENAME CONSTRAINT safe_evd_11_12_13_14_inspection_company_id_fkey TO safe_evd_11_12_14_inspection_company_id_fkey;'
                    . 'ALTER TABLE safe_evd_11_12_13_14 RENAME CONSTRAINT safe_evd_11_12_13_14_inspection_company_id_writing_fkey TO safe_evd_11_12_14_inspection_company_id_writing_fkey;'
                    . 'ALTER TABLE safe_evd_11_12_13_14 RENAME CONSTRAINT safe_evd_11_12_13_14_inspection_person_id_fkey TO safe_evd_11_12_14_inspection_person_id_fkey;'
                    . 'ALTER TABLE safe_evd_11_12_13_14 RENAME CONSTRAINT safe_evd_11_12_13_14_inspection_person_id_writing_fkey TO safe_evd_11_12_14_inspection_person_id_writing_fkey;'
                    . 'ALTER TABLE safe_evd_11_12_13_14 RENAME CONSTRAINT safe_evd_11_12_13_14_oup_company_id_fkey TO safe_evd_11_12_14_oup_company_id_fkey;'
                    . 'ALTER TABLE safe_evd_11_12_13_14 RENAME CONSTRAINT safe_evd_11_12_13_14_oup_company_id_writing_fkey TO safe_evd_11_12_14_oup_company_id_writing_fkey;'
                    . 'ALTER TABLE safe_evd_11_12_13_14 RENAME CONSTRAINT safe_evd_11_12_13_14_oup_person_id_fkey TO safe_evd_11_12_14_oup_person_id_fkey;'
                    . 'ALTER TABLE safe_evd_11_12_13_14 RENAME CONSTRAINT safe_evd_11_12_13_14_oup_person_id_writing_fkey TO safe_evd_11_12_14_oup_person_id_writing_fkey;'
                    . 'ALTER TABLE safe_evd_11_12_13_14 RENAME CONSTRAINT safe_evd_11_12_13_14_safe_evd_3_id_fkey TO safe_evd_11_12_14_safe_evd_3_id_fkey;'
                    . 'ALTER TABLE safe_evd_11_12_13_14 RENAME CONSTRAINT safe_evd_11_12_13_14_safe_evd_4_id_fkey TO safe_evd_11_12_14_safe_evd_4_id_fkey;'
                    . 'ALTER TABLE safe_evd_11_12_13_14 RENAME CONSTRAINT safe_evd_11_12_13_14_safe_evd_5_id_fkey TO safe_evd_11_12_14_safe_evd_5_id_fkey;'
                    . 'ALTER TABLE safe_evd_11_12_13_14 RENAME TO safe_evd_11_12_14;'
                    . 'SET search_path = public, pg_catalog;')->execute();
            
        }

	public function safeDown()
	{
            echo "m0000000004_00001_fixes_for_safe_evd does not support migration down.\n";
            return false;
	}	
}