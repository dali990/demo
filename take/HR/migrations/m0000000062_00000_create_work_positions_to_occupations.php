<?php

class m0000000062_00000_create_work_positions_to_occupations extends CDbMigration
{
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        Yii::app()->db->createCommand(
<<<'SIMAMIGRATESQL'
CREATE TABLE hr.work_positions_to_occupations
(
    id serial NOT NULL,
    work_position_id integer NOT NULL,
    occupation_id integer NOT NULL,
    CONSTRAINT work_positions_to_occupations_pkey PRIMARY KEY (id),
    CONSTRAINT work_positions_to_occupations_wp_id_o_id_key UNIQUE (work_position_id, occupation_id),
    CONSTRAINT work_positions_to_occupations_work_position_id_fkey FOREIGN KEY (work_position_id)
      REFERENCES hr.work_positions (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE RESTRICT,
    CONSTRAINT work_positions_to_occupations_occupation_id_fkey FOREIGN KEY (occupation_id)
      REFERENCES legal.occupations (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE RESTRICT  
);
CREATE TRIGGER i_am_called_by_everyone
  AFTER INSERT OR UPDATE OR DELETE
  ON hr.work_positions_to_occupations
  FOR EACH ROW
  EXECUTE PROCEDURE public.i_am_called_by_everyone();
  
ALTER TABLE hr.work_contracts ADD COLUMN occupation_id integer,
    ADD CONSTRAINT work_contracts_occupation_id_fkey 
        FOREIGN KEY (occupation_id)
        REFERENCES legal.occupations (id) MATCH SIMPLE
        ON UPDATE CASCADE ON DELETE RESTRICT;
        
ALTER TABLE legal.occupations
    SET SCHEMA hr;
SIMAMIGRATESQL
        )->execute();
    }

    public function safeDown()
    {
//        Yii::app()->db->createCommand(
//<<<'SIMAMIGRATESQL'
//
//SIMAMIGRATESQL
//        )->execute();
        echo "m0000000062_00000_create_work_positions_to_occupations does not support migration down.\n";
        return false;
    }
}