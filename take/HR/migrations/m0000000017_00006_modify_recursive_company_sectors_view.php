<?php

class m0000000017_00006_modify_recursive_company_sectors_view extends CDbMigration
{
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        Yii::app()->db->createCommand(
<<<'SIMAMIGRATESQL'
            DROP VIEW web_sima.work_position_count;
            DROP VIEW public.recursive_company_sector;
            
            CREATE OR REPLACE VIEW public.recursive_company_sector AS 
                SELECT for_parent.id,
                   for_parent.treename AS display_name_path,
                   for_parent.pathname AS display_name_full,
                   for_parent.parent_ids,
                   for_parent.head_sector_id,
                   for_children.children_ids
                  FROM ( WITH RECURSIVE reccur(id, name, parent_id, head_sector_id, pathname, treename, parent_ids) AS (
                                SELECT company_sectors.id,
                                   company_sectors.name,
                                   company_sectors.parent_id,
                                   company_sectors.id,
                                   company_sectors.name,
                                   ''::text AS text,
                                   ARRAY[]::integer[] AS "array"
                                  FROM public.company_sectors company_sectors
                                 WHERE company_sectors.parent_id IS NULL
                               UNION ALL
                                SELECT company_sectors.id,
                                   company_sectors.name,
                                   company_sectors.parent_id,
                                   reccur.head_sector_id,
                                   (reccur.pathname || '_'::text) || company_sectors.name,
                                   reccur.treename || '|---'::text,
                                   reccur.parent_ids || company_sectors.parent_id
                                  FROM public.company_sectors company_sectors,
                                   reccur
                                 WHERE reccur.id = company_sectors.parent_id
                               )
                        SELECT r.id,
                           r.treename || r.name AS treename,
                           r.head_sector_id,
                           j.name,
                           r.pathname,
                           r.parent_ids
                          FROM reccur r
                            LEFT JOIN company_sectors j ON r.id = j.id
                         ORDER BY r.pathname) for_parent
                    LEFT JOIN ( SELECT company_sectors.id,
                           array_remove(array_agg(vv.id), NULL::integer) AS children_ids
                          FROM public.company_sectors company_sectors
                            LEFT JOIN ( WITH RECURSIVE reccur(id, parent_id, parent_ids) AS (
                                        SELECT company_sectors_1.id,
                                           company_sectors_1.parent_id,
                                           ARRAY[]::integer[] AS "array"
                                          FROM public.company_sectors company_sectors_1
                                         WHERE company_sectors_1.parent_id IS NULL
                                       UNION ALL
                                        SELECT company_sectors_1.id,
                                           company_sectors_1.parent_id,
                                           reccur.parent_ids || company_sectors_1.parent_id
                                          FROM public.company_sectors company_sectors_1,
                                           reccur
                                         WHERE reccur.id = company_sectors_1.parent_id
                                       )
                                SELECT r.id,
                                   r.parent_ids
                                  FROM reccur r) vv ON company_sectors.id = ANY (vv.parent_ids)
                         GROUP BY company_sectors.id) for_children ON for_parent.id = for_children.id
                 ORDER BY for_parent.pathname;
                 
            CREATE OR REPLACE VIEW web_sima.work_position_count AS 
                SELECT count(*) AS people_count,
                   p2wp.work_position_id,
                   rcs.head_sector_id
                  FROM public.persons p
                    JOIN legal.persons_to_work_positions p2wp ON p.id = p2wp.person_id
                    LEFT JOIN legal.work_positions wp ON p2wp.work_position_id = wp.id
                    LEFT JOIN public.company_sectors cs ON wp.company_sector_id = cs.id
                    LEFT JOIN public.recursive_company_sector rcs ON wp.company_sector_id = rcs.id
                 GROUP BY p2wp.work_position_id, rcs.head_sector_id;
SIMAMIGRATESQL
        )->execute();
    }

    public function safeDown()
    {
        Yii::app()->db->createCommand(
<<<'SIMAMIGRATESQL'
            DROP VIEW web_sima.work_position_count;
            DROP VIEW public.recursive_company_sector;
            
            CREATE OR REPLACE VIEW public.recursive_company_sector AS 
                WITH RECURSIVE reccur(id, name, parent_id, head_sector) AS (
                        SELECT cs.id,
                           cs.name,
                           cs.parent_id,
                           cs.id
                          FROM public.company_sectors cs
                         WHERE cs.parent_id IS NULL
                       UNION ALL
                        SELECT cs.id,
                           cs.name,
                           cs.parent_id,
                           reccur.head_sector
                          FROM public.company_sectors cs,
                           reccur
                         WHERE reccur.id = cs.parent_id
                       )
                SELECT rr.id,
                   rr.name,
                   rr.parent_id,
                   rr.head_sector AS head_sector_id
                  FROM reccur rr
                 ORDER BY rr.id;
                 
            CREATE OR REPLACE VIEW web_sima.work_position_count AS 
                SELECT count(*) AS people_count,
                   p2wp.work_position_id,
                   rcs.head_sector_id
                  FROM public.persons p
                    JOIN legal.persons_to_work_positions p2wp ON p.id = p2wp.person_id
                    LEFT JOIN legal.work_positions wp ON p2wp.work_position_id = wp.id
                    LEFT JOIN public.company_sectors cs ON wp.company_sector_id = cs.id
                    LEFT JOIN public.recursive_company_sector rcs ON wp.company_sector_id = rcs.id
                 GROUP BY p2wp.work_position_id, rcs.head_sector_id;
SIMAMIGRATESQL
        )->execute();
//        echo "m0000000017_00006_modify_recursive_company_sectors_view does not support migration down.\n";
//        return false;
    }
}