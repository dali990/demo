<?php

class m0000000060_00004_injuries_add_parent_column extends CDbMigration
{
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        Yii::app()->db->createCommand(
<<<'SIMAMIGRATESQL'
ALTER TABLE hr.mode_injuries ADD COLUMN parent_id integer,
    ADD CONSTRAINT parent_id_fkey 
        FOREIGN KEY (parent_id)
        REFERENCES hr.mode_injuries (id) MATCH SIMPLE
        ON UPDATE CASCADE ON DELETE RESTRICT;
        
ALTER TABLE hr.source_injuries ADD COLUMN parent_id integer,
    ADD CONSTRAINT parent_id_fkey 
        FOREIGN KEY (parent_id)
        REFERENCES hr.source_injuries (id) MATCH SIMPLE
        ON UPDATE CASCADE ON DELETE RESTRICT;
SIMAMIGRATESQL
        )->execute();
    }

    public function safeDown()
    {
//        Yii::app()->db->createCommand(
//<<<'SIMAMIGRATESQL'
//
//SIMAMIGRATESQL
//        )->execute();
        echo "m0000000060_00004_injuries_add_parent_column does not support migration down.\n";
        return false;
    }
}