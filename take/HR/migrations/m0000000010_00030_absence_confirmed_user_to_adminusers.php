<?php

class m0000000010_00030_absence_confirmed_user_to_adminusers extends CDbMigration
{
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        Yii::app()->db->createCommand(
"
ALTER TABLE hr.absences DROP CONSTRAINT absences_confirmed_user_id_fkey;
ALTER TABLE hr.absences
  ADD CONSTRAINT absences_confirmed_user_id_fkey FOREIGN KEY (confirmed_user_id)
      REFERENCES admin.users (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE RESTRICT;
"
        )->execute();
    }

    public function safeDown()
    {
        Yii::app()->db->createCommand(
"
ALTER TABLE hr.absences DROP CONSTRAINT absences_confirmed_user_id_fkey;
ALTER TABLE hr.absences
  ADD CONSTRAINT absences_confirmed_user_id_fkey FOREIGN KEY (confirmed_user_id)
      REFERENCES employees (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE RESTRICT;
"
        )->execute();
    }
}