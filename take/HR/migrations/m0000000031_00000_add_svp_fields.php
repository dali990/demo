<?php

class m0000000031_00000_add_svp_fields extends CDbMigration
{
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        Yii::app()->db->createCommand(
<<<'SIMAMIGRATESQL'
        ALTER TABLE hr.work_contracts DISABLE TRIGGER i_am_called_by_everyone;
        ALTER TABLE hr.work_contracts DISABLE TRIGGER work_contracts_after_insert_delete;
        ALTER TABLE hr.work_contracts DISABLE TRIGGER work_contracts_after_update_of_start_date_end_date;
   
        ALTER TABLE hr.work_contracts
            ADD COLUMN employment_code character varying(2) DEFAULT '01',
            ADD COLUMN benefit character varying(2) NOT NULL DEFAULT '00',
            ADD COLUMN benefited_work_experience character varying(1) NOT NULL DEFAULT '0',
            ADD COLUMN type_of_payer character varying(1);
            
        ALTER TABLE hr.work_positions
            ADD COLUMN benefited_work_experience character varying(1) NOT NULL DEFAULT '0';

        UPDATE hr.work_contracts as wc 
        SET employment_code = (
                                    SELECT 
                                        CASE 
                                            WHEN employment_code IS NOT NULL THEN 
                                                employment_code
                                            ELSE
                                                '01'
                                        END
                                    FROM public.employees
                                    WHERE id=wc.employee_id
			       );
                               
        UPDATE hr.work_contracts
        SET type_of_payer = (
                                SELECT (substr(value, 2, 1))::varchar(1)
                                FROM base.configuration_parameters 
                                WHERE mapping_id=14 and user_id is null
			    );
            
        ALTER TABLE hr.work_contracts
            ALTER COLUMN employment_code SET NOT NULL,
            ALTER COLUMN type_of_payer SET NOT NULL;
            
        ALTER TABLE hr.work_contracts ENABLE TRIGGER i_am_called_by_everyone;
        ALTER TABLE hr.work_contracts ENABLE TRIGGER work_contracts_after_insert_delete;
        ALTER TABLE hr.work_contracts ENABLE TRIGGER work_contracts_after_update_of_start_date_end_date;
SIMAMIGRATESQL
        )->execute();
    }

    public function safeDown()
    {
//        Yii::app()->db->createCommand(
//<<<'SIMAMIGRATESQL'
//
//SIMAMIGRATESQL
//        )->execute();
        echo "m0000000031_00000_add_svp_fields does not support migration down.\n";
        return false;
    }
}