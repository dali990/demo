<?php

class m0000000044_00012_safe_evd2_fixes extends CDbMigration
{
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        Yii::app()->db->createCommand(
<<<'SIMAMIGRATESQL'
            ALTER TABLE hr.medical_examination_referrals 
                ADD COLUMN person_id integer,
                ADD COLUMN work_position_id integer,
                ADD CONSTRAINT medical_examination_referrals_person_id_fkey FOREIGN KEY (person_id)
                    REFERENCES public.persons (id) MATCH SIMPLE
                    ON UPDATE CASCADE ON DELETE RESTRICT,
                ADD CONSTRAINT medical_examination_referrals_work_position_id_fkey FOREIGN KEY (work_position_id)
                    REFERENCES hr.work_positions (id) MATCH SIMPLE
                    ON UPDATE CASCADE ON DELETE RESTRICT;
                    
            UPDATE hr.medical_examination_referrals as mer
            SET person_id = (
                SELECT employee_id FROM hr.medical_examination_referrals_to_employees 
                WHERE medical_examination_referral_id = mer.id
            );
                            
            UPDATE hr.medical_examination_referrals as mer
            SET work_position_id = (
                SELECT wp.id
                FROM hr.work_contracts as wc
                JOIN hr.work_positions as wp on wc.work_position_id = wp.id  
                WHERE (
                    wc.employee_id = mer.person_id AND
                    (
                        wc.start_date <= current_date 
                        AND 
                        (
                            base.min_dates(wc.calculated_end_date, current_date) = current_date
                        )
                    )
                )
            );
            
            ALTER TABLE hr.safe_evd_2 RENAME COLUMN employee_id TO person_id;
            ALTER TABLE hr.safe_evd_2 DROP CONSTRAINT safe_evd_2_employee_id_fkey;
            ALTER TABLE hr.safe_evd_2
                ADD CONSTRAINT safe_evd_2_person_id_fkey FOREIGN KEY (person_id)
                    REFERENCES public.persons (id) MATCH SIMPLE
                    ON UPDATE CASCADE ON DELETE RESTRICT;
                    
            ALTER TABLE hr.safe_evd_13 RENAME COLUMN employee_id TO person_id;
            ALTER TABLE hr.safe_evd_13 DROP CONSTRAINT safe_evd_13_employee_id_fkey;
            ALTER TABLE hr.safe_evd_13
                ADD CONSTRAINT safe_evd_13_person_id_fkey FOREIGN KEY (person_id)
                    REFERENCES public.persons (id) MATCH SIMPLE
                    ON UPDATE CASCADE ON DELETE RESTRICT;
                    
            DROP TABLE hr.medical_examination_referrals_to_employees;
SIMAMIGRATESQL
        )->execute();
    }

    public function safeDown()
    {
//        Yii::app()->db->createCommand(
//<<<'SIMAMIGRATESQL'
//
//SIMAMIGRATESQL
//        )->execute();
        echo "m0000000044_00012_safe_evd2_fixes does not support migration down.\n";
        return false;
    }
}