<?php

class m1000000071_00001_work_position_add_column extends CDbMigration
{
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        Yii::app()->db->createCommand(
<<<'SIMAMIGRATESQL'
    ALTER TABLE hr.work_positions 
    ADD COLUMN increased_risk boolean NOT NULL DEFAULT false;
SIMAMIGRATESQL
        )->execute();
    }

    public function safeDown()
    {
//        Yii::app()->db->createCommand(
//<<<'SIMAMIGRATESQL'
//
//SIMAMIGRATESQL
//        )->execute();
        echo "m0000000071_00001_work_position_add_column does not support migration down.\n";
        return false;
    }
}