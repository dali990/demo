<?php

class m0000000070_00003_safe_evd3_add_columns extends CDbMigration
{
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        Yii::app()->db->createCommand(
<<<'SIMAMIGRATESQL'
CREATE TABLE hr.safe_evd_3_to_work_contracts
(
    id serial NOT NULL,
    safe_evd_3_id integer NOT NULL,
    work_contract_id integer NOT NULL,
    CONSTRAINT safe_evd_3_to_work_contracts_pkey PRIMARY KEY (id),
    CONSTRAINT safe_evd_3_to_work_contracts_safe_evd_3_id_work_contract_id_key UNIQUE (safe_evd_3_id, work_contract_id),
    CONSTRAINT safe_evd_3_to_work_contracts_safe_evd_3_id_fkey FOREIGN KEY (safe_evd_3_id)
      REFERENCES hr.safe_evd_3 (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE RESTRICT,
    CONSTRAINT safe_evd_3_to_work_contracts_work_contract_id_fkey FOREIGN KEY (work_contract_id)
      REFERENCES hr.work_contracts (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE RESTRICT  
);
CREATE TRIGGER i_am_called_by_everyone
  AFTER INSERT OR UPDATE OR DELETE
  ON hr.safe_evd_3_to_work_contracts
  FOR EACH ROW
  EXECUTE PROCEDURE public.i_am_called_by_everyone();
  
ALTER TABLE hr.safe_evd_3 
    ADD COLUMN confirmed boolean NOT NULL DEFAULT false,
    ADD COLUMN confirmed_timestamp timestamp without time zone,
    ADD COLUMN confirmed_user_id integer,
    ADD COLUMN citizenship integer,
    ADD COLUMN employment_status text,
    ADD COLUMN work_exp_years integer NOT NULL DEFAULT 0,
    ADD COLUMN work_exp_months integer NOT NULL DEFAULT 0,
    ADD COLUMN work_exp_days integer NOT NULL DEFAULT 0,
    ADD COLUMN beginning_working_hour text,
    ADD COLUMN injury_location_address_id integer,
    ADD COLUMN commuting_accidents text,
    ADD COLUMN work_environment text,
    ADD COLUMN work_process text,
    ADD COLUMN specific_physical_activity text,
    ADD COLUMN contact_injury text,
    ADD COLUMN increased_risk integer,
    ADD COLUMN boss_of_injured_id integer,
    ADD COLUMN eyewitness_id integer,
    ADD COLUMN form_filling_date timestamp without time zone,
    ADD COLUMN form_filling_settlement_id integer,
    ADD COLUMN doctor_examined_settlement_id integer,
    ADD COLUMN hospital_id integer,
    ADD COLUMN injury_diagnosis text,
    ADD COLUMN external_cause_of_injury text,
    ADD COLUMN injury_part_of_body text,
    ADD COLUMN doctor_remarks text,
    ADD COLUMN mt3_days_unable_work integer,
    ADD COLUMN estimated_lost_days text,
    ADD COLUMN doctor_examined_date timestamp without time zone,
    ADD COLUMN date_of_received_report timestamp without time zone,
    ADD COLUMN violation_date timestamp without time zone,
    ADD COLUMN insurance_settlement_id integer,
    ADD COLUMN unique_injury_number text,
    ADD COLUMN evd_number integer,
    ADD COLUMN display_evd_number text,
    ADD CONSTRAINT safe_evd_3_confirmed_user_id_fkey FOREIGN KEY (confirmed_user_id)
      REFERENCES admin.users (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE RESTRICT,
    ADD CONSTRAINT safe_evd_3_eyewitness_id_fkey FOREIGN KEY (eyewitness_id)
      REFERENCES public.persons (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE RESTRICT,
    ADD CONSTRAINT safe_evd_3_injury_location_address_id_fkey FOREIGN KEY (injury_location_address_id)
      REFERENCES geo.addresses (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE RESTRICT,
    ADD CONSTRAINT safe_evd_3_boss_of_injured_id_fkey FOREIGN KEY (boss_of_injured_id)
      REFERENCES public.employees (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE RESTRICT,
    ADD CONSTRAINT safe_evd_3_form_filling_settlement_id_fkey FOREIGN KEY (form_filling_settlement_id)
      REFERENCES geo.cities (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE RESTRICT,
    ADD CONSTRAINT safe_evd_3_doctor_examined_settlement_id_fkey FOREIGN KEY (doctor_examined_settlement_id)
      REFERENCES geo.cities (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE RESTRICT,
    ADD CONSTRAINT safe_evd_3_form_hospital_id_fkey FOREIGN KEY (hospital_id)
      REFERENCES public.companies (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE RESTRICT,
    ADD CONSTRAINT safe_evd_3_insurance_settlement_id_fkey FOREIGN KEY (insurance_settlement_id)
      REFERENCES geo.cities (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE RESTRICT;
      
UPDATE hr.safe_evd_3
SET mark_injury = CASE 
	WHEN mark_injury = 'laka' THEN '1'
	WHEN mark_injury = 'teška' THEN '2'
	WHEN mark_injury = 'smrtna povreda na radu' THEN '3'
	WHEN mark_injury = 'povreda na radu zbog koje zaposleni nije sposoban za rad više od tri uzastopna radna dana' THEN '4'
	ELSE mark_injury
END;

UPDATE hr.safe_evd_3
SET type_injury = CASE 
	WHEN type_injury = 'pojedinačna' THEN '01'
	WHEN type_injury = 'kolektivna' THEN '02'
	ELSE type_injury
END;
	
---------------------------------------------------------
  
CREATE OR REPLACE FUNCTION hr.evd_number()
    RETURNS trigger AS
$BODY$
    DECLARE
        old_year text;
        new_year text;
    BEGIN
        IF (TG_OP = 'INSERT') THEN
            IF (NEW.date IS NOT NULL) THEN
                NEW.evd_number := (select COALESCE(max(evd_number),0)+1 from hr.safe_evd_3 where date_part('year', date)::TEXT = date_part('year', NEW.date)::TEXT );
                NEW.display_evd_number := NEW.evd_number||'/'||to_char(NEW.date,'YYYY');
            END IF;
        END IF;
        IF (TG_OP = 'UPDATE') THEN
            old_year := COALESCE(to_char(OLD.date,'YYYY'), '');
            new_year := COALESCE(to_char(NEW.date,'YYYY'), '');
            IF (old_year != new_year) THEN
                NEW.evd_number := (select COALESCE(max(evd_number),0)+1 from hr.safe_evd_3 where date_part('year', date)::TEXT = new_year );			
                IF (new_year != '') THEN
                    NEW.display_evd_number := NEW.evd_number||'/'||new_year;
                END IF;
            END IF;
        END IF;	
        RETURN NEW;
    END;
$BODY$
    LANGUAGE plpgsql VOLATILE
    COST 100;

----------------------------------------------------------

CREATE TRIGGER evd_number_trigger
    BEFORE INSERT OR UPDATE
    ON hr.safe_evd_3
    FOR EACH ROW
    EXECUTE PROCEDURE hr.evd_number();

---------------------------------------------------------
  
CREATE OR REPLACE FUNCTION hr.insert_evd_number()
	RETURNS void AS
    $BODY$
        DECLARE
        evd_row RECORD;
        new_evd_number integer;
        new_display_evd_number text;
        BEGIN
            FOR evd_row IN (
                SELECT safe_evd_3.id, safe_evd_3.date, safe_evd_3.evd_number
                FROM hr.safe_evd_3 
                ORDER BY safe_evd_3.date, safe_evd_3.id ASC)
            LOOP
                IF (evd_row.date IS NOT NULL) THEN
                    new_evd_number := (
                        SELECT COALESCE(max(safe_evd_3.evd_number),0)+1 
                        FROM hr.safe_evd_3 
                        WHERE date_part('year', safe_evd_3.date)::TEXT = date_part('year', evd_row.date)::TEXT 
                    );
                    new_display_evd_number := new_evd_number||'/'||to_char(evd_row.date,'YYYY');

                    UPDATE hr.safe_evd_3
                    SET evd_number = new_evd_number, 
                    display_evd_number = new_display_evd_number
                    WHERE id = evd_row.id;
                END IF;
            END LOOP;  
        END;
    $BODY$
	LANGUAGE plpgsql;
    
SELECT hr.insert_evd_number();
DROP FUNCTION hr.insert_evd_number();
    

    
SIMAMIGRATESQL
        )->execute();
    }

    public function safeDown()
    {
//        Yii::app()->db->createCommand(
//<<<'SIMAMIGRATESQL'
//
//SIMAMIGRATESQL
//        )->execute();
        echo "m0000000070_00003_safe_evd3_add_confirm_column does not support migration down.\n";
        return false;
    }
}