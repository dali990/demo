<?php

class m0000000017_00011_add_working_percent_at_other_companies extends CDbMigration
{
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        Yii::app()->db->createCommand(
<<<'SIMAMIGRATESQL'

ALTER TABLE private.work_contracts
  ADD COLUMN working_percent_at_other_companies numeric(3,0) NOT NULL DEFAULT 0;
   

SIMAMIGRATESQL
        )->execute();
    }

    public function safeDown()
    {
//        Yii::app()->db->createCommand(
//<<<'SIMAMIGRATESQL'
//
//SIMAMIGRATESQL
//        )->execute();
        echo "m0000000017_00011_add_working_percent_at_other_companies does not support migration down.\n";
        return false;
    }
}