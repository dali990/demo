<?php

class m0000000004_00002_changes_for_delivered_hr_documents extends EDbMigration
{
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        Yii::app()->db->createCommand('SET search_path = hr, pg_catalog;'
                . 'ALTER TABLE delivered_hr_documents DROP CONSTRAINT delivered_hr_documents_id_fkey;'
                . 'ALTER TABLE delivered_hr_documents ADD COLUMN file_id integer;
                   ALTER TABLE delivered_hr_documents
                   ADD CONSTRAINT delivered_hr_documents_file_id_fkey FOREIGN KEY (file_id)
                   REFERENCES files.files (id) MATCH SIMPLE
                   ON UPDATE CASCADE ON DELETE RESTRICT;
                   CREATE SEQUENCE delivered_hr_documents_id_seq
                   START WITH 1
                   INCREMENT BY 1
                   NO MAXVALUE
                   NO MINVALUE
                   CACHE 1;
                   ALTER TABLE delivered_hr_documents ALTER COLUMN id SET DEFAULT nextval(\'delivered_hr_documents_id_seq\'::regclass);
                   ALTER SEQUENCE delivered_hr_documents_id_seq OWNED BY delivered_hr_documents.id;
                   ALTER TABLE delivered_hr_documents ALTER COLUMN sender_id SET NOT NULL;
                   ALTER TABLE delivered_hr_documents ALTER COLUMN sent_timestamp SET NOT NULL;
                   ALTER TABLE delivered_hr_documents ALTER COLUMN sent_timestamp SET DEFAULT now();'
                . 'SET search_path = public, pg_catalog;')->execute();
        
        $delivered_hr_documents = DeliveredHRDocument::model()->findAll();
        foreach ($delivered_hr_documents as $delivered_hr_document) 
        {
            $delivered_hr_document->file_id = $delivered_hr_document->id;
            $delivered_hr_document->save();
        }
        
        Yii::app()->db->createCommand('SET search_path = hr, pg_catalog;'
                . 'ALTER TABLE delivered_hr_documents ALTER COLUMN file_id SET NOT NULL;'
                . 'SET search_path = public, pg_catalog;')->execute();
    }

    public function safeDown()
    {
        Yii::app()->db->createCommand('SET search_path = hr, pg_catalog;'
                . 'ALTER TABLE delivered_hr_documents DROP COLUMN file_id;'                
                . 'ALTER TABLE delivered_hr_documents ALTER COLUMN id DROP DEFAULT;
                   ALTER TABLE delivered_hr_documents
                   ADD CONSTRAINT delivered_hr_documents_id_fkey FOREIGN KEY (id)
                   REFERENCES files.files (id) MATCH SIMPLE
                   ON UPDATE CASCADE ON DELETE RESTRICT;
                   DROP SEQUENCE delivered_hr_documents_id_seq;'
                . 'SET search_path = public, pg_catalog;')->execute();
    }	
}