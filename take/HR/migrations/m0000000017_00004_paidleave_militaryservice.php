<?php

class m0000000017_00004_paidleave_militaryservice extends CDbMigration
{
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        Yii::app()->db->createCommand(
<<<'SIMAMIGRATESQL'
ALTER TABLE hr.absences DROP CONSTRAINT absences_subtype_check;
ALTER TABLE hr.absences
  ADD CONSTRAINT absences_subtype_check CHECK (subtype = ANY (ARRAY['SICK_LEAVE_INJURY_AT_WORK'::text, 'SICK_LEAVE_PREGNANCY_TILL_30'::text, 'SICK_LEAVE_PREGNANCY_OVER_30'::text, 'SICK_LEAVE_MATERNITY'::text, 'SICK_LEAVE_CHILD_CARE_TILL_3'::text, 'SICK_LEAVE_TILL_30'::text, 'SICK_LEAVE_OVER_30'::text, 'PAID_LEAVE_WEDDING'::text, 'PAID_LEAVE_DEATH_IN_FAMILY'::text, 'PAID_LEAVE_BLOOD_DONATION'::text, 'PAID_LEAVE_CHILD_BIRTH'::text, 'PAID_LEAVE_NATURAL_DISASTER'::text, 'PAID_LEAVE_SLAVA'::text, 'PAID_LEAVE_MILITARY_SERVICE'::text]));

ALTER TABLE accounting.paycheck_additionals DROP CONSTRAINT paycheck_additionals_type_check;
ALTER TABLE accounting.paycheck_additionals
  ADD CONSTRAINT paycheck_additionals_type_check CHECK (type = ANY (ARRAY['ANNUAL_LEAVE'::text, 'SICK_LEAVE_INJURY_AT_WORK'::text, 'SICK_LEAVE_PREGNANCY_TILL_30'::text, 'SICK_LEAVE_PREGNANCY_OVER_30'::text, 'SICK_LEAVE_MATERNITY'::text, 'SICK_LEAVE_CHILD_CARE_TILL_3'::text, 'SICK_LEAVE_TILL_30'::text, 'SICK_LEAVE_OVER_30'::text, 'STATE_AND_RELIGIOUS_HOLIDAY'::text, 'WORK_ON_NON_WORKING_HOLIDAY'::text, 'PAID_LEAVE'::text, 'STANDBY'::text, 'OVERTIME'::text, 'PAID_LEAVE_MILITARY_SERVICE'::text]));
SIMAMIGRATESQL
        )->execute();
    }

    public function safeDown()
    {
//        Yii::app()->db->createCommand(
//<<<'SIMAMIGRATESQL'
//
//SIMAMIGRATESQL
//        )->execute();
        echo "m0000000017_00004_paidleave_militaryservice does not support migration down.\n";
        return false;
    }
}