<?php

class m0000000010_00018_fixes_for_company_licenses extends EDbMigration
{
//	public function up()
//	{
//	}
//
//	public function down()
//	{
//		echo "m0000000010_00018_fixes_for_company_licenses does not support migration down.\n";
//		return false;
//	}
	
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
            Yii::app()->db->createCommand('SET search_path = hr, pg_catalog;'.'
                ALTER TABLE company_licenses_to_companies ADD COLUMN is_active boolean NOT NULL default true;
                ALTER TABLE company_licenses_to_companies ADD COLUMN is_active_submitter_id integer;
                ALTER TABLE company_licenses_to_companies ADD COLUMN is_active_timestamp timestamp without time zone;
                ALTER TABLE company_licenses_to_companies
                    ADD CONSTRAINT company_licenses_to_companies_is_active_submitter_id_fkey FOREIGN KEY (is_active_submitter_id)
                    REFERENCES admin.users (id) MATCH SIMPLE
                    ON UPDATE CASCADE ON DELETE RESTRICT;
                
                CREATE TABLE jobs_references_to_company_licenses
                (
                  company_license_id integer NOT NULL,
                  reference_id integer NOT NULL,
                  partner_id integer NOT NULL,
                  CONSTRAINT jobs_references_to_company_licenses_pkey PRIMARY KEY (company_license_id, partner_id, reference_id),
                  CONSTRAINT jobs_references_to_company_licenses_company_license_id_fkey FOREIGN KEY (company_license_id)
                      REFERENCES hr.company_licenses_to_companies (id) MATCH SIMPLE
                      ON UPDATE CASCADE ON DELETE RESTRICT,
                  CONSTRAINT jobs_references_to_company_licenses_reference_id_fkey FOREIGN KEY (reference_id)
                      REFERENCES legal.jobs_references (id) MATCH SIMPLE
                      ON UPDATE CASCADE ON DELETE RESTRICT,
                  CONSTRAINT jobs_references_to_company_licenses_partner_id_fkey FOREIGN KEY (partner_id)
                      REFERENCES public.partners (id) MATCH SIMPLE
                      ON UPDATE CASCADE ON DELETE RESTRICT
                );
                CREATE TRIGGER i_am_called_by_everyone
                AFTER INSERT OR UPDATE OR DELETE
                ON hr.jobs_references_to_company_licenses
                FOR EACH ROW
                EXECUTE PROCEDURE public.i_am_called_by_everyone();
                
                ALTER TABLE jobs_references_to_persons_working_licences RENAME CONSTRAINT jobs_references_to_persons_working_licences_pkey TO persons_working_licences_to_jobs_references_pkey;
                ALTER TABLE jobs_references_to_persons_working_licences RENAME CONSTRAINT jobs_references_to_persons_working_licences_id_fkey TO persons_working_licences_to_jobs_references_id_fkey;
                ALTER TABLE jobs_references_to_persons_working_licences RENAME CONSTRAINT jobs_references_to_persons_working_licences_person_license_id_f TO persons_working_licences_to_jobs_references_person_license_id_f;
                ALTER TABLE jobs_references_to_persons_working_licences RENAME CONSTRAINT jobs_references_to_persons_working_licences_reference_id_fkey TO persons_working_licences_to_jobs_references_reference_id_fkey;
                ALTER TABLE jobs_references_to_persons_working_licences RENAME TO persons_working_licences_to_jobs_references;
                
                ALTER TABLE company_licenses_to_persons DROP CONSTRAINT companies_company_licenses_to_persons_person_id_fkey;
                ALTER TABLE company_licenses_to_persons RENAME COLUMN person_id TO partner_id;
                ALTER TABLE company_licenses_to_persons RENAME CONSTRAINT companies_company_licenses_to_persons_pkey TO company_licenses_to_partners_pkey;
                ALTER TABLE company_licenses_to_persons RENAME CONSTRAINT companies_company_licenses_to_persons_company_license_id_fkey TO company_licenses_to_partners_company_license_id_fkey;
                ALTER SEQUENCE company_licenses_to_persons_id_seq RENAME TO company_licenses_to_partners_id_seq;
                ALTER TABLE company_licenses_to_persons RENAME TO company_licenses_to_partners;
                ALTER TABLE company_licenses_to_partners
                    ADD CONSTRAINT company_licenses_to_partners_partner_id_fkey FOREIGN KEY (partner_id)
                        REFERENCES public.partners (id) MATCH SIMPLE
                        ON UPDATE CASCADE ON DELETE RESTRICT;
                        
                ALTER TABLE company_licenses_to_partners ADD COLUMN accepted_references_number integer;
                
                ALTER TABLE company_license_decisions_to_company_licenses ADD COLUMN id serial NOT NULL;
                ALTER TABLE company_license_decisions_to_company_licenses DROP CONSTRAINT company_license_decisions_to_company_licenses_pkey;
                ALTER TABLE company_license_decisions_to_company_licenses
                    ADD CONSTRAINT company_license_decisions_to_company_licenses_pkey PRIMARY KEY(id);
                ALTER TABLE company_license_decisions_to_company_licenses
                    ADD CONSTRAINT company_license_decisions_to_company_licenses_company_license_u UNIQUE(company_license_decision_id, company_license_id);

            '. 'SET search_path = public, pg_catalog;')->execute();
	}

	public function safeDown()
	{
            Yii::app()->db->createCommand('SET search_path = hr, pg_catalog;'
                . '
                    ALTER TABLE company_licenses_to_companies DROP COLUMN is_active;
                    ALTER TABLE company_licenses_to_companies DROP COLUMN is_active_submitter_id;
                    ALTER TABLE company_licenses_to_companies DROP COLUMN is_active_timestamp;
                    DROP TABLE jobs_references_to_company_licenses;

                    ALTER TABLE persons_working_licences_to_jobs_references RENAME CONSTRAINT persons_working_licences_to_jobs_references_pkey TO jobs_references_to_persons_working_licences_pkey;
                    ALTER TABLE persons_working_licences_to_jobs_references RENAME CONSTRAINT persons_working_licences_to_jobs_references_id_fkey TO jobs_references_to_persons_working_licences_id_fkey;
                    ALTER TABLE persons_working_licences_to_jobs_references RENAME CONSTRAINT persons_working_licences_to_jobs_references_person_license_id_f TO jobs_references_to_persons_working_licences_person_license_id_f;
                    ALTER TABLE persons_working_licences_to_jobs_references RENAME CONSTRAINT persons_working_licences_to_jobs_references_reference_id_fkey TO jobs_references_to_persons_working_licences_reference_id_fkey;
                    ALTER TABLE persons_working_licences_to_jobs_references RENAME TO jobs_references_to_persons_working_licences;
                    
                    ALTER TABLE company_licenses_to_partners DROP CONSTRAINT company_licenses_to_partners_partner_id_fkey;
                    ALTER TABLE company_licenses_to_partners RENAME COLUMN partner_id TO person_id;
                    ALTER TABLE company_licenses_to_partners RENAME CONSTRAINT company_licenses_to_partners_pkey TO companies_company_licenses_to_persons_pkey;
                    ALTER TABLE company_licenses_to_partners RENAME CONSTRAINT company_licenses_to_partners_company_license_id_fkey TO companies_company_licenses_to_persons_company_license_id_fkey;
                    ALTER SEQUENCE company_licenses_to_partners_id_seq RENAME TO company_licenses_to_persons_id_seq;
                    ALTER TABLE company_licenses_to_partners RENAME TO company_licenses_to_persons;
                    ALTER TABLE company_licenses_to_persons
                        ADD CONSTRAINT companies_company_licenses_to_persons_person_id_fkey FOREIGN KEY (person_id)
                            REFERENCES public.persons (id) MATCH SIMPLE
                            ON UPDATE CASCADE ON DELETE RESTRICT;
                            
                    ALTER TABLE company_licenses_to_persons DROP COLUMN accepted_references_number;
                    
                    ALTER TABLE company_license_decisions_to_company_licenses DROP CONSTRAINT company_license_decisions_to_company_licenses_pkey;
                    ALTER TABLE company_license_decisions_to_company_licenses DROP COLUMN id;
                    ALTER TABLE company_license_decisions_to_company_licenses
                        ADD CONSTRAINT company_license_decisions_to_company_licenses_pkey PRIMARY KEY(company_license_decision_id, company_license_id);
                    ALTER TABLE company_license_decisions_to_company_licenses DROP CONSTRAINT company_license_decisions_to_company_licenses_company_license_u;
'           
                . 'SET search_path = public, pg_catalog;')->execute();
	}
}