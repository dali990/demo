<?php

class m0000000044_00007_nationaleventtoday_iamusedbyeveryone extends CDbMigration
{
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        Yii::app()->db->createCommand(
<<<'SIMAMIGRATESQL'
CREATE TRIGGER i_am_called_by_everyone
  AFTER INSERT OR UPDATE OR DELETE
  ON hr.national_event_to_day_year
  FOR EACH ROW
  EXECUTE PROCEDURE public.i_am_called_by_everyone();

CREATE OR REPLACE FUNCTION public.i_am_called_by_everyone()
  RETURNS trigger AS
$BODY$
                  DECLARE
                          old_data json;
                          new_data json;

                          /*tables_to_cache TEXT[] := ARRAY[
                                  'files.file_versions',
                                  'files.files',
                                  'files.locked_files',
                                  'private.filing_book',
                                  'public.comment_threads',
                                  'public.comments',
                                  'public.days',
                                  'hr.work_contracts'
                          ];*/

                          return_rec RECORD;
                  BEGIN
                          /*IF NOT ((TG_TABLE_SCHEMA||'.'||TG_TABLE_NAME) = ANY (tables_to_cache))
                          THEN
                                  IF (TG_OP = 'UPDATE') OR (TG_OP = 'INSERT') 
                                  THEN
                                          RETURN NEW;
                                  ELSIF (TG_OP = 'DELETE') 
                                  THEN
                                          RETURN OLD;
                                  ELSE
                                          RAISE WARNING '[i_am_called_by_everyone] - Other action occurred: %, at %',TG_OP,now();
                                          RETURN NULL;
                                  END IF;
                          END IF;*/

                          IF (TG_OP = 'UPDATE') 
                          THEN
                                  old_data := row_to_json(OLD.*);
                                  return_rec := NEW;
                          ELSIF (TG_OP = 'DELETE') 
                          THEN
                                  old_data := row_to_json(OLD.*);
                                  return_rec := OLD;
                          ELSIF (TG_OP = 'INSERT') 
                          THEN
                                  new_data := row_to_json(NEW.*);
                                  return_rec := NEW;
                          ELSE
                                  RAISE WARNING '[i_am_called_by_everyone] - Other action occurred: %, at %',TG_OP,now();
                                  return_rec := NULL;
                          END IF;

                          PERFORM cache.trigger_devalidate(TG_OP::TEXT, TG_TABLE_SCHEMA::TEXT, TG_TABLE_NAME::TEXT, old_data, new_data);

                          RETURN return_rec;
                  /*EXCEPTION
                          WHEN data_exception THEN
                                  RAISE WARNING '[i_am_called_by_everyone] - UDF ERROR [DATA EXCEPTION] - SQLSTATE: %, SQLERRM: %',SQLSTATE,SQLERRM;
                                  RETURN NULL;
                          WHEN unique_violation THEN
                                  RAISE WARNING '[i_am_called_by_everyone] - UDF ERROR [UNIQUE] - SQLSTATE: %, SQLERRM: %',SQLSTATE,SQLERRM;
                                  RETURN NULL;
                          WHEN OTHERS THEN
                                  RAISE WARNING '[i_am_called_by_everyone] - UDF ERROR [OTHER] - SQLSTATE: %, SQLERRM: %',SQLSTATE,SQLERRM;
                                  RETURN NULL;*/
                  END;
                  $BODY$
  LANGUAGE plpgsql;
SIMAMIGRATESQL
        )->execute();
    }

    public function safeDown()
    {
//        Yii::app()->db->createCommand(
//<<<'SIMAMIGRATESQL'
//
//SIMAMIGRATESQL
//        )->execute();
        echo "m0000000044_00007_nationaleventtoday_iamusedbyeveryone does not support migration down.\n";
        return false;
    }
}