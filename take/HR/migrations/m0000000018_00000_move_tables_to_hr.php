<?php

class m0000000018_00000_move_tables_to_hr extends CDbMigration
{
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        Yii::app()->db->createCommand(
<<<'SIMAMIGRATESQL'
            ALTER TABLE public.working_licence_certificates SET SCHEMA hr;
                

            ALTER TABLE public.company_sectors SET SCHEMA hr;
            

            ALTER TABLE public.persons_to_working_licences SET SCHEMA hr;
            

            ALTER TABLE private.close_work_contracts SET SCHEMA hr;
            ALTER SEQUENCE private.close_work_contracts_id_seq SET SCHEMA hr;
            ALTER FUNCTION private.close_work_contracts_after_insert_update_delete() SET SCHEMA hr;
            

            ALTER TABLE private.work_contracts SET SCHEMA hr;
            ALTER FUNCTION private.work_contracts_after_insert_delete() SET SCHEMA hr;
            ALTER FUNCTION private.work_contracts_after_update_of_start_date_end_date() SET SCHEMA hr;
            

            ALTER TABLE legal.persons_to_work_positions SET SCHEMA hr;
            

            ALTER TABLE legal.work_positions SET SCHEMA hr;
            

            ALTER TABLE legal.work_position_payments SET SCHEMA hr;


            ALTER VIEW public.recursive_company_sector SET SCHEMA hr;
            ALTER VIEW hr.recursive_company_sector RENAME TO recursive_company_sectors;


            ALTER FUNCTION private.recalculate_work_contract_end_date(integer,date) SET SCHEMA hr;


            CREATE OR REPLACE FUNCTION hr.safe_evd_1_after_insert_update_delete()
                    RETURNS trigger AS
                  $BODY$
                  DECLARE
                          risc BOOLEAN := FALSE;
                          return_rec RECORD;	
                  BEGIN	
                          IF (TG_OP = 'UPDATE' OR TG_OP = 'INSERT') 
                          THEN		
                                  return_rec := NEW;
                          ELSIF (TG_OP = 'DELETE') 
                          THEN		
                                  return_rec := OLD;	
                          END IF;

                          IF ((select count(*) from hr.safe_evd_1 where work_position_id=return_rec.work_position_id) > 0)
                          THEN
                                  risc := TRUE;
                          END IF;

                          UPDATE hr.work_positions
                          SET enlarged_risk=risc
                          WHERE id=return_rec.work_position_id;

                          RETURN return_rec;
                  END;
                  $BODY$
                    LANGUAGE plpgsql;
                
            
            CREATE OR REPLACE FUNCTION web_sima.get_employee_subordinate_ids(empid integer, return_all boolean DEFAULT true)
                    RETURNS text AS
                  $BODY$
                  DECLARE
                          sectors INT[];
                          result TEXT := '';
                          subresult TEXT;

                          time_var TEXT := clock_timestamp()::text;
                  BEGIN
                          select array_agg(cs.id) 
                          from hr.company_sectors cs, hr.work_positions wp, hr.persons_to_work_positions ptwp
                          where cs.id=wp.company_sector_id and wp.id=ptwp.work_position_id and ptwp.person_id=empid AND wp.is_director=TRUE
                          into sectors;

                          LOOP
                                  IF sectors IS NULL OR array_length(sectors, 1) IS NULL OR array_length(sectors, 1) = 0
                                  THEN
                                          EXIT;
                                  END IF;

                                  select array_to_string(array_agg(ptwp.person_id), ',')
                                  from hr.company_sectors cs, hr.work_positions wp, hr.persons_to_work_positions ptwp
                                  where cs.id=wp.company_sector_id and wp.id=ptwp.work_position_id and cs.id IN (select unnest(sectors)) and ptwp.person_id!=empid
                                  INTO subresult;

                                  IF result IS NULL OR result LIKE ''
                                  THEN
                                          result := subresult;
                                  ELSE
                                          result := result || ',' || subresult;
                                  END IF;

                                  IF return_all IS FALSE
                                  THEN
                                          EXIT;
                                  END IF;

                                  SELECT array_agg(id) FROM hr.company_sectors WHERE parent_id IN (select unnest(sectors))
                                  INTO sectors;
                          END LOOP;

                          IF log_function('web_sima.get_employee_subordinate_ids'::text, '"' || empid || '", "' || return_all::TEXT ||'"', null, time_var) NOT LIKE 'OK'
                          THEN
                                  RAISE EXCEPTION 'error during logging';
                          END IF;

                          RETURN result;
                  END;
                  $BODY$
                    LANGUAGE plpgsql;
                

            CREATE OR REPLACE FUNCTION web_sima.get_person_boss_ids(personid integer, return_all boolean DEFAULT true)
                    RETURNS text AS
                  $BODY$
                  DECLARE
                          sectorid INT;
                          bossids INT[];
                          subbossids INT[];
                          return_bossids INT[];

                          time_var TEXT := clock_timestamp()::text;
                  BEGIN
                          FOR sectorid IN (select cs.id 
                                                  from hr.company_sectors cs, hr.work_positions wp, hr.persons_to_work_positions ptwp
                                                  where cs.id=wp.company_sector_id and wp.id=ptwp.work_position_id and ptwp.person_id=personid)
                          LOOP
                                  IF (SELECT COUNT(*)!=0 
                                          FROM hr.work_positions wp, hr.persons_to_work_positions ptwp 
                                          WHERE wp.company_sector_id=sectorid AND wp.id=ptwp.work_position_id AND ptwp.person_id=personid AND wp.is_director=TRUE)
                                  THEN
                                          SELECT parent_id FROM hr.company_sectors WHERE id=sectorid
                                          INTO sectorid;
                                  END IF;

                                  bossids := ARRAY[]::INT[];
                                  LOOP
                                          IF sectorid IS NULL
                                                  OR (return_all = FALSE AND bossids IS NOT NULL AND array_length(bossids, 1) > 0)
                                          THEN
                                                  IF bossids IS NOT NULL AND array_length(bossids, 1) > 0
                                                  THEN
                                                          IF return_bossids IS NULL OR array_length(return_bossids, 1) = 0
                                                          THEN
                                                                  return_bossids := bossids;
                                                          ELSE
                                                                  return_bossids := return_bossids || bossids;
                                                          END IF;
                                                  END IF;

                                                  EXIT;
                                          END IF;

                                          select array_agg(ptwp.person_id)
                                          from hr.company_sectors cs, hr.work_positions wp, hr.persons_to_work_positions ptwp
                                          where cs.id=wp.company_sector_id and wp.id=ptwp.work_position_id and wp.is_director=TRUE
                                                  and cs.id=sectorid
                                          INTO subbossids;

                                          IF subbossids IS NOT NULL AND array_length(subbossids, 1) > 0
                                          THEN
                                                  IF bossids IS NULL OR array_length(bossids, 1) = 0
                                                  THEN
                                                          bossids := subbossids;
                                                  ELSE
                                                          bossids := bossids || subbossids;
                                                  END IF;
                                          END IF;

                                          SELECT parent_id FROM hr.company_sectors WHERE id=sectorid
                                          INTO sectorid;
                                  END LOOP;
                          END LOOP;

                          IF log_function('web_sima.get_person_boss_ids'::text, '"' || personid || '"', null, time_var) NOT LIKE 'OK'
                          THEN
                                  RAISE EXCEPTION 'error during logging';
                          END IF;

                          SELECT ARRAY(SELECT DISTINCT UNNEST(return_bossids) ORDER BY 1)
                          into return_bossids;

                          RETURN array_to_string(return_bossids, ',');
                  END;
                  $BODY$
                    LANGUAGE plpgsql;
                

            CREATE OR REPLACE FUNCTION web_sima.get_sector_tasks(IN sectorid integer)
                    RETURNS TABLE(id integer, begin date, "end" date, reports text, report_hours real, have_completed_child boolean, have_uncompleted_child boolean, have_archived_child boolean, subcoordinators text) AS
                  $BODY$
                  DECLARE
                          task_id INT;
                          task_ids INT[];

                          time_var TEXT := clock_timestamp()::TEXT;
                  BEGIN
                          select array_agg(t.id)
                          from hr.company_sectors cs, hr.work_positions wp, hr.persons_to_work_positions ptwp, tasks t
                          where cs.id=wp.company_sector_id and wp.id=ptwp.work_position_id and t.coordinator_id=ptwp.person_id
                                  and cs.id=sectorid
                          into task_ids;

                          FOR task_id IN SELECT unnest(task_ids)
                          LOOP
                                  IF web_sima.get_task_parents(task_id) && task_ids
                                  THEN ELSE
                                          RETURN QUERY select tv.*, web_sima.task_subcoordinators(task_id)::TEXT
                                                          from web_sima.tasks_view(task_id, null) tv;
                                  END IF;
                          END LOOP;

                          IF log_function('web_sima.get_sector_tasks'::text, '"' || sectorid || '"', null, time_var) NOT LIKE 'OK'
                          THEN
                                  RAISE EXCEPTION 'error during logging';
                          END IF;
                  end;
                  $BODY$
                    LANGUAGE plpgsql;
                    

            CREATE OR REPLACE FUNCTION public.partners_before_delete()
                    RETURNS trigger AS
                  $BODY$
                  DECLARE
                  BEGIN
                          DELETE FROM persons_to_companies WHERE person_id=OLD.id;
                          DELETE FROM hr.persons_to_work_positions WHERE person_id=OLD.id;
                          DELETE FROM persons WHERE id=OLD.id;

                          DELETE FROM companies WHERE id=OLD.id;

                          RETURN OLD;
                  END;
                  $BODY$
                    LANGUAGE plpgsql;
                    
            CREATE OR REPLACE FUNCTION hr.close_work_contracts_after_insert_update_delete()
                    RETURNS trigger AS
                  $BODY$
                  DECLARE
                          return_rec RECORD;
                  BEGIN
                          IF (TG_OP = 'DELETE') 
                          THEN
                                  return_rec := OLD;
                          ELSIF (TG_OP = 'INSERT') OR (TG_OP = 'UPDATE') 
                          THEN
                                  return_rec := NEW;
                          ELSE
                                  RAISE WARNING '[hr.work_contracts_before_update_of_start_date_end_date] - Other action occurred: %, at %',TG_OP,now();
                                  return_rec := NULL;
                          END IF;

                          UPDATE hr.work_contracts 
                          SET calculated_end_date=hr.recalculate_work_contract_end_date(id) 
                          WHERE employee_id=(select employee_id FROM hr.work_contracts WHERE id=return_rec.work_contract_id);

                          RETURN return_rec;
                  END;
                  $BODY$
                    LANGUAGE plpgsql;
                    

            CREATE OR REPLACE FUNCTION hr.recalculate_work_contract_end_date(work_contract_id integer, end_date_limit date DEFAULT NULL::date)
                    RETURNS date AS
                  $BODY$
                  DECLARE
                          result DATE;
                  BEGIN
                          select base.min_dates(base.min_dates(base.min_dates(wc.end_date, (select end_date 
                                                                      from hr.close_work_contracts cwc 
                                                                      where cwc.work_contract_id=wc.id 
                                                                      limit 1)),
                                                      (select (start_date - INTERVAL '1 day')::date from hr.work_contracts wc_next
                                                      where wc_next.employee_id=wc.employee_id 
                                                      and ((wc_next.start_date=wc.start_date AND wc_next.id>wc.id)
                                                              OR (wc_next.start_date>wc.start_date AND wc_next.id!=wc.id))
                                                      order by wc_next.start_date ASC
                                                      limit 1)), end_date_limit) as end_date
                          from hr.work_contracts wc
                          where id=work_contract_id
                          INTO result;

                          RETURN result;
                  END;
                  $BODY$
                    LANGUAGE plpgsql;
                    

            CREATE OR REPLACE FUNCTION hr.work_contracts_after_insert_delete()
                    RETURNS trigger AS
                  $BODY$
                  DECLARE
                          return_rec RECORD;
                  BEGIN
                          IF (TG_OP = 'DELETE') 
                          THEN
                                  return_rec := OLD;
                          ELSIF (TG_OP = 'INSERT') 
                          THEN
                                  return_rec := NEW;
                          ELSE
                                  RAISE WARNING '[hr.work_contracts_before_update_of_start_date_end_date] - Other action occurred: %, at %',TG_OP,now();
                                  return_rec := NULL;
                          END IF;

                          UPDATE hr.work_contracts 
                          SET calculated_end_date=hr.recalculate_work_contract_end_date(id) 
                          WHERE employee_id=return_rec.employee_id;

                          RETURN return_rec;
                  END;
                  $BODY$
                    LANGUAGE plpgsql;
                    

            CREATE OR REPLACE FUNCTION hr.work_contracts_after_update_of_start_date_end_date()
                    RETURNS trigger AS
                  $BODY$
                  DECLARE
                  BEGIN
                          UPDATE hr.work_contracts 
                          SET calculated_end_date=hr.recalculate_work_contract_end_date(id) 
                          WHERE employee_id=NEW.employee_id;

                          RETURN NEW;
                  END;
                  $BODY$
                    LANGUAGE plpgsql;
                    

            CREATE OR REPLACE FUNCTION public.i_am_called_by_everyone()
                    RETURNS trigger AS
                  $BODY$
                  DECLARE
                          old_data json;
                          new_data json;

                          tables_to_cache TEXT[] := ARRAY[
                                  'files.file_versions',
                                  'files.files',
                                  'files.locked_files',
                                  'private.filing_book',
                                  'public.comment_threads',
                                  'public.comments',
                                  'public.days',
                                  'hr.work_contracts'
                          ];

                          return_rec RECORD;
                  BEGIN
                          IF NOT ((TG_TABLE_SCHEMA||'.'||TG_TABLE_NAME) = ANY (tables_to_cache))
                          THEN
                                  IF (TG_OP = 'UPDATE') OR (TG_OP = 'INSERT') 
                                  THEN
                                          RETURN NEW;
                                  ELSIF (TG_OP = 'DELETE') 
                                  THEN
                                          RETURN OLD;
                                  ELSE
                                          RAISE WARNING '[i_am_called_by_everyone] - Other action occurred: %, at %',TG_OP,now();
                                          RETURN NULL;
                                  END IF;
                          END IF;

                          IF (TG_OP = 'UPDATE') 
                          THEN
                                  old_data := row_to_json(OLD.*);
                                  return_rec := NEW;
                          ELSIF (TG_OP = 'DELETE') 
                          THEN
                                  old_data := row_to_json(OLD.*);
                                  return_rec := OLD;
                          ELSIF (TG_OP = 'INSERT') 
                          THEN
                                  new_data := row_to_json(NEW.*);
                                  return_rec := NEW;
                          ELSE
                                  RAISE WARNING '[i_am_called_by_everyone] - Other action occurred: %, at %',TG_OP,now();
                                  return_rec := NULL;
                          END IF;

                          PERFORM cache.trigger_devalidate(TG_OP::TEXT, TG_TABLE_SCHEMA::TEXT, TG_TABLE_NAME::TEXT, old_data, new_data);

                          RETURN return_rec;
                  /*EXCEPTION
                          WHEN data_exception THEN
                                  RAISE WARNING '[i_am_called_by_everyone] - UDF ERROR [DATA EXCEPTION] - SQLSTATE: %, SQLERRM: %',SQLSTATE,SQLERRM;
                                  RETURN NULL;
                          WHEN unique_violation THEN
                                  RAISE WARNING '[i_am_called_by_everyone] - UDF ERROR [UNIQUE] - SQLSTATE: %, SQLERRM: %',SQLSTATE,SQLERRM;
                                  RETURN NULL;
                          WHEN OTHERS THEN
                                  RAISE WARNING '[i_am_called_by_everyone] - UDF ERROR [OTHER] - SQLSTATE: %, SQLERRM: %',SQLSTATE,SQLERRM;
                                  RETURN NULL;*/
                  END;
                  $BODY$
                    LANGUAGE plpgsql;
            
SIMAMIGRATESQL
        )->execute();
    }

    public function safeDown()
    {
        Yii::app()->db->createCommand(
<<<'SIMAMIGRATESQL'
            ALTER TABLE hr.working_licence_certificates SET SCHEMA public;
                

            ALTER TABLE hr.company_sectors SET SCHEMA public;
            

            ALTER TABLE hr.persons_to_working_licences SET SCHEMA public;
            

            ALTER TABLE hr.close_work_contracts SET SCHEMA private;
            ALTER SEQUENCE hr.close_work_contracts_id_seq SET SCHEMA private;
            ALTER FUNCTION hr.close_work_contracts_after_insert_update_delete() SET SCHEMA private;
            

            ALTER TABLE hr.work_contracts SET SCHEMA private;
            ALTER FUNCTION hr.work_contracts_after_insert_delete() SET SCHEMA private;
            ALTER FUNCTION hr.work_contracts_after_update_of_start_date_end_date() SET SCHEMA private;
            

            ALTER TABLE hr.persons_to_work_positions SET SCHEMA legal;
            

            ALTER TABLE hr.work_positions SET SCHEMA legal;
            

            ALTER TABLE hr.work_position_payments SET SCHEMA legal;
            

            ALTER VIEW hr.recursive_company_sectors SET SCHEMA public;
            ALTER VIEW public.recursive_company_sectors RENAME TO recursive_company_sector;
            

            ALTER FUNCTION hr.recalculate_work_contract_end_date(integer,date) SET SCHEMA private;


            CREATE OR REPLACE FUNCTION hr.safe_evd_1_after_insert_update_delete()
                    RETURNS trigger AS
                  $BODY$
                  DECLARE
                          risc BOOLEAN := FALSE;
                          return_rec RECORD;	
                  BEGIN	
                          IF (TG_OP = 'UPDATE' OR TG_OP = 'INSERT') 
                          THEN		
                                  return_rec := NEW;
                          ELSIF (TG_OP = 'DELETE') 
                          THEN		
                                  return_rec := OLD;	
                          END IF;

                          IF ((select count(*) from hr.safe_evd_1 where work_position_id=return_rec.work_position_id) > 0)
                          THEN
                                  risc := TRUE;
                          END IF;

                          UPDATE legal.work_positions
                          SET enlarged_risk=risc
                          WHERE id=return_rec.work_position_id;

                          RETURN return_rec;
                  END;
                  $BODY$
                    LANGUAGE plpgsql;
                
            
            CREATE OR REPLACE FUNCTION web_sima.get_employee_subordinate_ids(empid integer, return_all boolean DEFAULT true)
                    RETURNS text AS
                  $BODY$
                  DECLARE
                          sectors INT[];
                          result TEXT := '';
                          subresult TEXT;

                          time_var TEXT := clock_timestamp()::text;
                  BEGIN
                          select array_agg(cs.id) 
                          from company_sectors cs, legal.work_positions wp, legal.persons_to_work_positions ptwp
                          where cs.id=wp.company_sector_id and wp.id=ptwp.work_position_id and ptwp.person_id=empid AND wp.is_director=TRUE
                          into sectors;

                          LOOP
                                  IF sectors IS NULL OR array_length(sectors, 1) IS NULL OR array_length(sectors, 1) = 0
                                  THEN
                                          EXIT;
                                  END IF;

                                  select array_to_string(array_agg(ptwp.person_id), ',')
                                  from company_sectors cs, legal.work_positions wp, legal.persons_to_work_positions ptwp
                                  where cs.id=wp.company_sector_id and wp.id=ptwp.work_position_id and cs.id IN (select unnest(sectors)) and ptwp.person_id!=empid
                                  INTO subresult;

                                  IF result IS NULL OR result LIKE ''
                                  THEN
                                          result := subresult;
                                  ELSE
                                          result := result || ',' || subresult;
                                  END IF;

                                  IF return_all IS FALSE
                                  THEN
                                          EXIT;
                                  END IF;

                                  SELECT array_agg(id) FROM company_sectors WHERE parent_id IN (select unnest(sectors))
                                  INTO sectors;
                          END LOOP;

                          IF log_function('web_sima.get_employee_subordinate_ids'::text, '"' || empid || '", "' || return_all::TEXT ||'"', null, time_var) NOT LIKE 'OK'
                          THEN
                                  RAISE EXCEPTION 'error during logging';
                          END IF;

                          RETURN result;
                  END;
                  $BODY$
                    LANGUAGE plpgsql;
                

            CREATE OR REPLACE FUNCTION web_sima.get_person_boss_ids(personid integer, return_all boolean DEFAULT true)
                    RETURNS text AS
                  $BODY$
                  DECLARE
                          sectorid INT;
                          bossids INT[];
                          subbossids INT[];
                          return_bossids INT[];

                          time_var TEXT := clock_timestamp()::text;
                  BEGIN
                          FOR sectorid IN (select cs.id 
                                                  from company_sectors cs, legal.work_positions wp, legal.persons_to_work_positions ptwp
                                                  where cs.id=wp.company_sector_id and wp.id=ptwp.work_position_id and ptwp.person_id=personid)
                          LOOP
                                  IF (SELECT COUNT(*)!=0 
                                          FROM legal.work_positions wp, legal.persons_to_work_positions ptwp 
                                          WHERE wp.company_sector_id=sectorid AND wp.id=ptwp.work_position_id AND ptwp.person_id=personid AND wp.is_director=TRUE)
                                  THEN
                                          SELECT parent_id FROM company_sectors WHERE id=sectorid
                                          INTO sectorid;
                                  END IF;

                                  bossids := ARRAY[]::INT[];
                                  LOOP
                                          IF sectorid IS NULL
                                                  OR (return_all = FALSE AND bossids IS NOT NULL AND array_length(bossids, 1) > 0)
                                          THEN
                                                  IF bossids IS NOT NULL AND array_length(bossids, 1) > 0
                                                  THEN
                                                          IF return_bossids IS NULL OR array_length(return_bossids, 1) = 0
                                                          THEN
                                                                  return_bossids := bossids;
                                                          ELSE
                                                                  return_bossids := return_bossids || bossids;
                                                          END IF;
                                                  END IF;

                                                  EXIT;
                                          END IF;

                                          select array_agg(ptwp.person_id)
                                          from company_sectors cs, legal.work_positions wp, legal.persons_to_work_positions ptwp
                                          where cs.id=wp.company_sector_id and wp.id=ptwp.work_position_id and wp.is_director=TRUE
                                                  and cs.id=sectorid
                                          INTO subbossids;

                                          IF subbossids IS NOT NULL AND array_length(subbossids, 1) > 0
                                          THEN
                                                  IF bossids IS NULL OR array_length(bossids, 1) = 0
                                                  THEN
                                                          bossids := subbossids;
                                                  ELSE
                                                          bossids := bossids || subbossids;
                                                  END IF;
                                          END IF;

                                          SELECT parent_id FROM company_sectors WHERE id=sectorid
                                          INTO sectorid;
                                  END LOOP;
                          END LOOP;

                          IF log_function('web_sima.get_person_boss_ids'::text, '"' || personid || '"', null, time_var) NOT LIKE 'OK'
                          THEN
                                  RAISE EXCEPTION 'error during logging';
                          END IF;

                          SELECT ARRAY(SELECT DISTINCT UNNEST(return_bossids) ORDER BY 1)
                          into return_bossids;

                          RETURN array_to_string(return_bossids, ',');
                  END;
                  $BODY$
                    LANGUAGE plpgsql;
                    
                    
            CREATE OR REPLACE FUNCTION web_sima.get_sector_tasks(IN sectorid integer)
                    RETURNS TABLE(id integer, begin date, "end" date, reports text, report_hours real, have_completed_child boolean, have_uncompleted_child boolean, have_archived_child boolean, subcoordinators text) AS
                  $BODY$
                  DECLARE
                          task_id INT;
                          task_ids INT[];

                          time_var TEXT := clock_timestamp()::TEXT;
                  BEGIN
                          select array_agg(t.id)
                          from company_sectors cs, legal.work_positions wp, legal.persons_to_work_positions ptwp, tasks t
                          where cs.id=wp.company_sector_id and wp.id=ptwp.work_position_id and t.coordinator_id=ptwp.person_id
                                  and cs.id=sectorid
                          into task_ids;

                          FOR task_id IN SELECT unnest(task_ids)
                          LOOP
                                  IF web_sima.get_task_parents(task_id) && task_ids
                                  THEN ELSE
                                          RETURN QUERY select tv.*, web_sima.task_subcoordinators(task_id)::TEXT
                                                          from web_sima.tasks_view(task_id, null) tv;
                                  END IF;
                          END LOOP;

                          IF log_function('web_sima.get_sector_tasks'::text, '"' || sectorid || '"', null, time_var) NOT LIKE 'OK'
                          THEN
                                  RAISE EXCEPTION 'error during logging';
                          END IF;
                  end;
                  $BODY$
                    LANGUAGE plpgsql;
                    

            CREATE OR REPLACE FUNCTION public.partners_before_delete()
                    RETURNS trigger AS
                  $BODY$
                  DECLARE
                  BEGIN
                          DELETE FROM persons_to_companies WHERE person_id=OLD.id;
                          DELETE FROM legal.persons_to_work_positions WHERE person_id=OLD.id;
                          DELETE FROM persons WHERE id=OLD.id;

                          DELETE FROM companies WHERE id=OLD.id;

                          RETURN OLD;
                  END;
                  $BODY$
                    LANGUAGE plpgsql;
                    

            CREATE OR REPLACE FUNCTION private.close_work_contracts_after_insert_update_delete()
                    RETURNS trigger AS
                  $BODY$
                  DECLARE
                          return_rec RECORD;
                  BEGIN
                          IF (TG_OP = 'DELETE') 
                          THEN
                                  return_rec := OLD;
                          ELSIF (TG_OP = 'INSERT') OR (TG_OP = 'UPDATE') 
                          THEN
                                  return_rec := NEW;
                          ELSE
                                  RAISE WARNING '[private.work_contracts_before_update_of_start_date_end_date] - Other action occurred: %, at %',TG_OP,now();
                                  return_rec := NULL;
                          END IF;

                          UPDATE private.work_contracts 
                          SET calculated_end_date=private.recalculate_work_contract_end_date(id) 
                          WHERE employee_id=(select employee_id FROM private.work_contracts WHERE id=return_rec.work_contract_id);

                          RETURN return_rec;
                  END;
                  $BODY$
                    LANGUAGE plpgsql;
                    

            CREATE OR REPLACE FUNCTION private.recalculate_work_contract_end_date(work_contract_id integer, end_date_limit date DEFAULT NULL::date)
                    RETURNS date AS
                  $BODY$
                  DECLARE
                          result DATE;
                  BEGIN
                          select base.min_dates(base.min_dates(base.min_dates(wc.end_date, (select end_date 
                                                                      from private.close_work_contracts cwc 
                                                                      where cwc.work_contract_id=wc.id 
                                                                      limit 1)),
                                                      (select (start_date - INTERVAL '1 day')::date from private.work_contracts wc_next
                                                      where wc_next.employee_id=wc.employee_id 
                                                      and ((wc_next.start_date=wc.start_date AND wc_next.id>wc.id)
                                                              OR (wc_next.start_date>wc.start_date AND wc_next.id!=wc.id))
                                                      order by wc_next.start_date ASC
                                                      limit 1)), end_date_limit) as end_date
                          from private.work_contracts wc
                          where id=work_contract_id
                          INTO result;

                          RETURN result;
                  END;
                  $BODY$
                    LANGUAGE plpgsql;
                    

            CREATE OR REPLACE FUNCTION private.work_contracts_after_insert_delete()
                    RETURNS trigger AS
                  $BODY$
                  DECLARE
                          return_rec RECORD;
                  BEGIN
                          IF (TG_OP = 'DELETE') 
                          THEN
                                  return_rec := OLD;
                          ELSIF (TG_OP = 'INSERT') 
                          THEN
                                  return_rec := NEW;
                          ELSE
                                  RAISE WARNING '[private.work_contracts_before_update_of_start_date_end_date] - Other action occurred: %, at %',TG_OP,now();
                                  return_rec := NULL;
                          END IF;

                          UPDATE private.work_contracts 
                          SET calculated_end_date=private.recalculate_work_contract_end_date(id) 
                          WHERE employee_id=return_rec.employee_id;

                          RETURN return_rec;
                  END;
                  $BODY$
                    LANGUAGE plpgsql;
                    

            CREATE OR REPLACE FUNCTION private.work_contracts_after_update_of_start_date_end_date()
                    RETURNS trigger AS
                  $BODY$
                  DECLARE
                  BEGIN
                          UPDATE private.work_contracts 
                          SET calculated_end_date=private.recalculate_work_contract_end_date(id) 
                          WHERE employee_id=NEW.employee_id;

                          RETURN NEW;
                  END;
                  $BODY$
                    LANGUAGE plpgsql;
                    

            CREATE OR REPLACE FUNCTION public.i_am_called_by_everyone()
                    RETURNS trigger AS
                  $BODY$
                  DECLARE
                          old_data json;
                          new_data json;

                          tables_to_cache TEXT[] := ARRAY[
                                  'files.file_versions',
                                  'files.files',
                                  'files.locked_files',
                                  'private.filing_book',
                                  'public.comment_threads',
                                  'public.comments',
                                  'public.days',
                                  'private.work_contracts'
                          ];

                          return_rec RECORD;
                  BEGIN
                          IF NOT ((TG_TABLE_SCHEMA||'.'||TG_TABLE_NAME) = ANY (tables_to_cache))
                          THEN
                                  IF (TG_OP = 'UPDATE') OR (TG_OP = 'INSERT') 
                                  THEN
                                          RETURN NEW;
                                  ELSIF (TG_OP = 'DELETE') 
                                  THEN
                                          RETURN OLD;
                                  ELSE
                                          RAISE WARNING '[i_am_called_by_everyone] - Other action occurred: %, at %',TG_OP,now();
                                          RETURN NULL;
                                  END IF;
                          END IF;

                          IF (TG_OP = 'UPDATE') 
                          THEN
                                  old_data := row_to_json(OLD.*);
                                  return_rec := NEW;
                          ELSIF (TG_OP = 'DELETE') 
                          THEN
                                  old_data := row_to_json(OLD.*);
                                  return_rec := OLD;
                          ELSIF (TG_OP = 'INSERT') 
                          THEN
                                  new_data := row_to_json(NEW.*);
                                  return_rec := NEW;
                          ELSE
                                  RAISE WARNING '[i_am_called_by_everyone] - Other action occurred: %, at %',TG_OP,now();
                                  return_rec := NULL;
                          END IF;

                          PERFORM cache.trigger_devalidate(TG_OP::TEXT, TG_TABLE_SCHEMA::TEXT, TG_TABLE_NAME::TEXT, old_data, new_data);

                          RETURN return_rec;
                  /*EXCEPTION
                          WHEN data_exception THEN
                                  RAISE WARNING '[i_am_called_by_everyone] - UDF ERROR [DATA EXCEPTION] - SQLSTATE: %, SQLERRM: %',SQLSTATE,SQLERRM;
                                  RETURN NULL;
                          WHEN unique_violation THEN
                                  RAISE WARNING '[i_am_called_by_everyone] - UDF ERROR [UNIQUE] - SQLSTATE: %, SQLERRM: %',SQLSTATE,SQLERRM;
                                  RETURN NULL;
                          WHEN OTHERS THEN
                                  RAISE WARNING '[i_am_called_by_everyone] - UDF ERROR [OTHER] - SQLSTATE: %, SQLERRM: %',SQLSTATE,SQLERRM;
                                  RETURN NULL;*/
                  END;
                  $BODY$
                    LANGUAGE plpgsql;
SIMAMIGRATESQL
        )->execute();
    }
}