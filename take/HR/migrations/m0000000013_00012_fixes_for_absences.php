<?php

class m0000000013_00012_fixes_for_absences extends CDbMigration
{
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        Yii::app()->db->createCommand(
<<<'SIMAMIGRATESQL'
            update public.comment_threads ct
            set model='Absence'
            where ct.model='AbsenceAnnualLeave' or ct.model='AbsenceFreeDays' or ct.model='AbsenceSickLeave';
SIMAMIGRATESQL
        )->execute();
    }

    public function safeDown()
    {
        Yii::app()->db->createCommand(
<<<'SIMAMIGRATESQL'
            update public.comment_threads ct
            set model=(
                    select 
                            case
                                    when a.type='ANNUAL_LEAVE' 
                                            then 'AbsenceAnnualLeave'
                                    when a.type='FREE_DAYS' 
                                            then 'AbsenceFreeDays'
                                    when a.type='SICK_LEAVE' 
                                            then 'AbsenceSickLeave'
                            end as eeee
                    from hr.absences a 
                    where a.id=ct.model_id and ct.model='Absence'
            )
            where ct.model='Absence';
SIMAMIGRATESQL
        )->execute();
//        echo "m0000000013_00012_fixes_for_absences does not support migration down.\n";
//        return false;
    }
}