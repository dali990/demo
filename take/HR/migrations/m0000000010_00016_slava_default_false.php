<?php

class m0000000010_00016_slava_default_false extends CDbMigration
{
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        Yii::app()->db->createCommand(
"
ALTER TABLE public.employees
   ALTER COLUMN have_slava SET DEFAULT false;
   
update public.employees set have_slava = false where have_slava=true and national_event_slava_id is null;
"
        )->execute();
    }

    public function safeDown()
    {
//        Yii::app()->db->createCommand(
//"
//
//"
//        )->execute();
        echo "m0000000010_00016_slava_default_false does not support migration down.\n";
        return false;
    }
}