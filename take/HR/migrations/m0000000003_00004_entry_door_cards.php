<?php

class m0000000003_00004_entry_door_cards extends EDbMigration
{
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
            Yii::app()->db->createCommand(
'
CREATE TABLE hr.entry_door_cards
(
  id serial NOT NULL,
  card_id text NOT NULL,
  partner_id integer,
  CONSTRAINT entry_door_cards_pkey PRIMARY KEY (id),
  CONSTRAINT entry_door_logs_partner_id_fkey FOREIGN KEY (partner_id)
      REFERENCES partners (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE RESTRICT
);

ALTER TABLE hr.entry_door_logs DROP CONSTRAINT entry_door_logs_employee_id_fkey;

ALTER TABLE hr.entry_door_logs RENAME COLUMN employee_id TO partner_id;

ALTER TABLE hr.entry_door_logs ADD COLUMN type text NOT NULL;

ALTER TABLE hr.entry_door_logs
  ADD CONSTRAINT entry_door_logs_partner_id_fkey FOREIGN KEY (partner_id)
      REFERENCES partners (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE RESTRICT;
      
ALTER TABLE hr.entry_door_logs
  ADD CONSTRAINT entry_door_logs_type_check CHECK (type = ANY (ARRAY[\'ENTRY\'::text, \'EXIT\'::text]));
'
            )->execute();
	}

	public function safeDown()
	{
            Yii::app()->db->createCommand(
'ALTER TABLE hr.entry_door_logs DROP CONSTRAINT entry_door_logs_partner_id_fkey;

ALTER TABLE hr.entry_door_logs DROP CONSTRAINT entry_door_logs_type_check;

ALTER TABLE hr.entry_door_logs RENAME COLUMN partner_id TO employee_id;

ALTER TABLE hr.entry_door_logs DROP COLUMN type;

ALTER TABLE hr.entry_door_logs
  ADD CONSTRAINT entry_door_logs_employee_id_fkey FOREIGN KEY (employee_id)
      REFERENCES employees (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE RESTRICT;
      
DROP TABLE hr.entry_door_cards;'
            )->execute();
	}
}