<?php

class m0000000017_00005_remap_guitable_indexes extends CDbMigration
{
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        Yii::app()->db->createCommand(
<<<'SIMAMIGRATESQL'
        SELECT convert_maintab_guitable_index('"legal_0"', '"HR_10"');
        SELECT convert_maintab_guitable_index('"legal_13"', '"HR_11"');
        SELECT convert_maintab_guitable_index('"legal_19"', '"HR_12"');
        SELECT convert_maintab_guitable_index('"legal_24"', '"HR_13"');
SIMAMIGRATESQL
        )->execute();
    }

    public function safeDown()
    {
        Yii::app()->db->createCommand(
<<<'SIMAMIGRATESQL'
        SELECT convert_maintab_guitable_index('"HR_10"', '"legal_0"');
        SELECT convert_maintab_guitable_index('"HR_11"', '"legal_13"');
        SELECT convert_maintab_guitable_index('"HR_12"', '"legal_19"');
        SELECT convert_maintab_guitable_index('"HR_13"', '"legal_24"');
SIMAMIGRATESQL
        )->execute();
//        echo "m0000000017_00005_remap_guitable_indexes does not support migration down.\n";
//        return false;
    }
}