<?php

class m0000000013_00013_change_absence_notifs_action extends CDbMigration
{
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        Yii::app()->db->createCommand(
<<<'SIMAMIGRATESQL'
    update public.notifications 
    set params=replace(params, (params::JSON)->>'action', '{"action":"defaultLayout","action_id":{"model_name":"Absence", "model_id":' || ((((params::JSON)->>'action')::JSON)->>'action_id') || '}}') 
    where params like '%"action":"HR\\/absence\\/absence%'
SIMAMIGRATESQL
        )->execute();
    }

    public function safeDown()
    {
        Yii::app()->db->createCommand(
<<<'SIMAMIGRATESQL'
    update public.notifications 
    set params=replace(params, (params::JSON)->>'action', '{"action":"HR\/absence\/absence","action_id":' || ((((((params::JSON)->>'action')::JSON)->>'action_id')::JSON)->>'model_id') || '}') 
    where params like '%"action":"defaultLayout","action_id":{"model_name":"Absence"%'
SIMAMIGRATESQL
        )->execute();
//        echo "m0000000013_00013_change_absence_notifs_action does not support migration down.\n";
//        return false;
    }
}