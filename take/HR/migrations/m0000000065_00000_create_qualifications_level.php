<?php

class m0000000065_00000_create_qualifications_level extends CDbMigration
{
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        Yii::app()->db->createCommand(
<<<'SIMAMIGRATESQL'
CREATE TABLE hr.qualification_levels
(
    id serial NOT NULL,
    name text NOT NULL,
    code integer NOT NULL,
    description text,
    CONSTRAINT qualification_levels_pkey PRIMARY KEY (id),
    CONSTRAINT qualification_levels_code_key UNIQUE (code)
);
CREATE TRIGGER i_am_called_by_everyone
  AFTER INSERT OR UPDATE OR DELETE
  ON hr.qualification_levels
  FOR EACH ROW
  EXECUTE PROCEDURE public.i_am_called_by_everyone();
  
ALTER TABLE public.persons
ADD COLUMN qualification_level_id integer,
ADD CONSTRAINT persons_qualification_level_id_fkey FOREIGN KEY (qualification_level_id)
      REFERENCES hr.qualification_levels (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE RESTRICT;
SIMAMIGRATESQL
        )->execute();
    }

    public function safeDown()
    {
//        Yii::app()->db->createCommand(
//<<<'SIMAMIGRATESQL'
//
//SIMAMIGRATESQL
//        )->execute();
        echo "m0000000065_00000_create_qualifications_level does not support migration down.\n";
        return false;
    }
}