<?php

class m0000000010_00013_work_licence_fix extends CDbMigration
{
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        Yii::app()->db->createCommand(
"
ALTER TABLE public.working_licence_certificates
  ADD FOREIGN KEY (licence_id) REFERENCES public.persons_to_working_licences (id) ON UPDATE CASCADE ON DELETE RESTRICT;

"
        )->execute();
    }

    public function safeDown()
    {
//        Yii::app()->db->createCommand(
//'
//
//'
//        )->execute();
        echo "m0000000010_00013_work_licence_fix does not support migration down.\n";
        return false;
    }
}