<?php

class m0000000003_00005_entry_door_log_card_column_add extends EDbMigration
{
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
            Yii::app()->db->createCommand(
'ALTER TABLE hr.entry_door_logs ADD COLUMN card_id integer NOT NULL;'
            )->execute();
	}

	public function safeDown()
	{
            Yii::app()->db->createCommand(
'ALTER TABLE hr.entry_door_logs DROP COLUMN card_id;'
            )->execute();
	}
}