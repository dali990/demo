<?php

class m0000000023_00003_check_persons_to_companies_with_work_contracts extends CDbMigration
{
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        Yii::app()->db->createCommand(
<<<'SIMAMIGRATESQL'
            CREATE OR REPLACE FUNCTION hr.temp_check_persons_to_companies_with_work_contracts()
                RETURNS void AS
                $BODY$
                DECLARE
                    system_company_id integer;
                    person RECORD;
                    person_to_company RECORD;
                    active_work_contract RECORD;
                BEGIN
                    system_company_id := (SELECT cp.id FROM base.configuration_parameters cp WHERE cp.mapping_id = 57);

                    IF system_company_id is not null
                    THEN
                        FOR person IN (select * from public.persons)
                        LOOP
                            select *
			    from hr.work_contracts wc
		            left join hr.work_positions wp on wp.id = wc.work_position_id
			    left join hr.recursive_company_sectors rcs on rcs.id = wp.company_sector_id
			    left join public.companies c on c.head_sector_id = rcs.head_sector_id
			    where 
			        c.id = system_company_id AND 
				wc.employee_id = person.id AND 
				wc.start_date <= now()::DATE AND 
				base.min_dates(wc.calculated_end_date, now()::DATE)=now()::DATE
			    limit 1
		            INTO active_work_contract;
		            
                            IF active_work_contract is not null
                            THEN
				IF (SELECT COUNT(*)=0 FROM public.persons_to_companies where person_id=person.id and company_id=system_company_id)
				THEN
					INSERT INTO public.persons_to_companies(person_id, company_id) VALUES(person.id, system_company_id);
				END IF;
				IF (SELECT COUNT(*)=0 FROM hr.persons_to_work_positions where person_id=person.id and fictive = false)
				THEN
					INSERT INTO hr.persons_to_work_positions(person_id, work_position_id, fictive) VALUES(person.id, active_work_contract.work_position_id, false);
				END IF;
                            END IF;
                        END LOOP;

                        FOR person_to_company IN (select * from public.persons_to_companies where company_id = system_company_id)
                        LOOP
			    select *
                            from hr.work_contracts wc
                            left join hr.work_positions wp on wp.id = wc.work_position_id
                            left join hr.recursive_company_sectors rcs on rcs.id = wp.company_sector_id
                            left join public.companies c on c.head_sector_id = rcs.head_sector_id
                            where 
                                c.id = person_to_company.company_id AND 
                                wc.employee_id = person_to_company.person_id AND 
                                wc.start_date <= now()::DATE AND 
                                base.min_dates(wc.calculated_end_date, now()::DATE)=now()::DATE
                            limit 1
                            INTO active_work_contract;
                                
                            IF active_work_contract is null
                            THEN
                                DELETE from public.persons_to_companies where person_id = person_to_company.person_id AND company_id = person_to_company.company_id;
                                IF (SELECT COUNT(*)=1 FROM hr.persons_to_work_positions where person_id=person_to_company.person_id and fictive = false)
				THEN
					DELETE from hr.persons_to_work_positions where person_id = person_to_company.person_id AND fictive = false;
				END IF;
                            END IF;
                        END LOOP;
                    END IF;
                END;
                $BODY$
                LANGUAGE plpgsql;

                select hr.temp_check_persons_to_companies_with_work_contracts();

            DROP FUNCTION hr.temp_check_persons_to_companies_with_work_contracts();
SIMAMIGRATESQL
        )->execute();
    }

    public function safeDown()
    {
//        Yii::app()->db->createCommand(
//<<<'SIMAMIGRATESQL'
//
//SIMAMIGRATESQL
//        )->execute();
        echo "m0000000023_00003_check_persons_to_companies_with_work_contracts does not support migration down.\n";
        return false;
    }
}