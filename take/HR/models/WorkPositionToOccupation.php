<?php

class WorkPositionToOccupation extends SIMAActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'hr.work_positions_to_occupations';
    }
    
    public function __get($column)
    {
        switch ($column)
        {
            case 'DisplayName':
            case 'SearchName': return $this->occupation->name.' ('.$this->occupation->code.')';
            default: return parent::__get($column);
        }
    }

    public function relations($child_relations = [])
    {
        return [
            'work_position' => [self::BELONGS_TO, WorkPosition::class, 'work_position_id'],
            'occupation' => [self::BELONGS_TO, Occupation::class, 'occupation_id'],
        ];
    }

    public function rules()
    {
        return [
            ['work_position_id, occupation_id', 'required'],
            ['work_position_id','unique_with','with' => ['occupation_id']]
        ];
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters' : return [
                'work_position' => 'relation',
                'occupation' => 'relation'
            ];
            case 'textSearch' : return [
                'work_position' => 'relation',
                'occupation' => 'relation'
            ];
            case 'update_relations': return ['work_position'];
            default: return parent::modelSettings($column);
        }
    }
    
}

