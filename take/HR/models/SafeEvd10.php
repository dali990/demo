<?php

class SafeEvd10 extends SIMAActiveRecord
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }   

    public function tableName()
    {
        return 'hr.safe_evd_10';
    }

    public function moduleName()
    {
        return 'HR';
    }

    public function __get($column)
    {
        switch ($column)
        {
            default: return parent::__get($column);
        }
    }

    public function rules()
    {
        return array(
            array('data, date_control, date_next_control, comment, type_evd, description', 'safe'),
            array('date_control, date_next_control', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => array('insert', 'update'))
        );
    }

    public function relations($child_relations = [])
    {
        return array(
        );
    }

    public function scopes()
    {
        return array(
            'byName' => array('order' => 'id'),
        );
    }

    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters' : return [
                    'data' => 'text',                    
                    'date_control' => 'date_range',
                    'date_next_control' => 'date_range',
                    'comment' => 'text',
                    'description' => 'text'
                ];
            case 'options': return ['form','delete'];
            default: return parent::modelSettings($column);
        }
    }

}
