<?php

/**
 * Description of SafeEvd2
 *
 * @author goran
 */
class SafeEvd2 extends SIMAActiveRecord
{

    public $file_version_id;
    public $file_id;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'hr.safe_evd_2';
    }

    public function moduleName()
    {
        return 'HR';
    }

    public function __get($column)
    {
        switch ($column)
        {
            case 'path2': return 'files/file';
            case 'DisplayName': return isset($this->file)?$this->file->DisplayName:'';
            case 'SearchName': return $this->DisplayName;
            default: return parent::__get($column);
        }
    }

    public function rules()
    {
        return array(
            array('person_id, work_position_id', 'required'),
            array('work_position_id, date_previous_report, date_next_report, healthy_mark, actions, description, report_code, medical_referral_id', 'safe'),
            array('person_id, work_position_id, date_previous_report, date_next_report, medical_report_period_id', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => array('insert', 'update')),
            array('add_not_passed_medical_abilities', 'checkNotPassedMedicalAbilities')
        );
    }

    public function checkNotPassedMedicalAbilities($attribute, $params)
    {
        $not_passed_abilities = [];
        if (gettype($this->not_passed_medical_abilities) === 'string')
        {
            $not_passed_abilities_array = CJSON::decode($this->not_passed_medical_abilities);
            $not_passed_abilities = $not_passed_abilities_array['ids'];
        }
        else
        {
            $not_passed_abilities = $this->not_passed_medical_abilities;
        }
        if (empty($this->healthy_mark) && count($not_passed_abilities) > 0)
        {
            $this->addError('add_not_passed_medical_abilities', 'Morate prvo odabrati ocenu zdravlja!');
        }
        else if ($this->healthy_mark === 'ABLE' && count($not_passed_abilities) > 0)
        {
            $this->addError('add_not_passed_medical_abilities', 'Ako je osoba sposobna onda ne može imati lekarske preglede koje nije prošla!');
        }
        else if ($this->healthy_mark === 'NOT_ABLE' && count($not_passed_abilities) === 0)
        {
            $this->addError('add_not_passed_medical_abilities', 'Ako osoba nije sposobna onda mora imati lekarske preglede koje nije prošla!');
        }
    }

    public function relations($child_relations = [])
    {
        $safe_evd2_to_not_passed_medical_abilities = SafeEvd2ToNotPassedMedicalAbility::model()->tableName();
        return array(
            'file' => array(self::BELONGS_TO, 'File', 'id'),
            'medical_referral' => array(self::BELONGS_TO, 'MedicalExaminationReferral', 'medical_referral_id'),
            'person' => array(self::BELONGS_TO, 'Person', 'person_id'),
            'work_position' => array(self::BELONGS_TO, 'WorkPosition', 'work_position_id'),
            'not_passed_medical_abilities' => array(self::MANY_MANY, 'RequiredMedicalAbility',
                "$safe_evd2_to_not_passed_medical_abilities(safe_evd2_id,medical_ability_id)"),
            'medical_report_period' => array(self::BELONGS_TO, 'Period', 'medical_report_period_id'),            
        );
    }

    public function scopes()
    {
        $uniq = SIMAHtml::uniqid();
        $alias = $this->getTableAlias();
        $wc_table = WorkContract::model()->tableName();
        $cwc_table = CloseWorkContract::model()->tableName();
        $safe2_table = SafeEvd2::model()->tableName();
        $persons_table_name = Person::model()->tableName();

        return array(            
            'forReportDocument' => array(
                'condition' =>
                //posmatra se samo poslednji
                "$alias.id in (select id from (select id,rank() over (partition by person_id ORDER BY date_previous_report DESC) from $safe2_table ) aa where rank=1) "
                //samo sa aktivnim ugovorima @TODO: bolje resiti, odnosno uslov prebaciti u bazu
                . " and $alias.person_id in (select employee_id from $wc_table where "
                . "start_date<now() " //ugovor mora da je poceo
                . " and id not in (select work_contract_id from $cwc_table where end_date<now() and work_contract_id is not null) " //ne sme da ima prestanak radnog odnosa
                . " and id in (select id from (select id,rank() over (partition by employee_id ORDER BY start_date DESC) from $wc_table ) aa where rank=1)" //zadnji ugovor
                . " and (end_date>now() or end_date is null)" //ugovor mora da je u toku
                . ")"
            ),
            'byName' => [
                'select'=>"$alias.*, persons$uniq.lastname, persons$uniq.firstname",
                'join'=>"join $persons_table_name persons$uniq on persons$uniq.id=$alias.person_id",
                'order'=>"persons$uniq.lastname ASC, persons$uniq.firstname ASC"
            ],
            'withoutMedicalReferral' => [
                'condition' => "$alias.medical_referral_id is null"
            ],
            'byTime' => [
                'order' => "$alias.date_previous_report DESC"
            ]
        );
    }

    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters' : return array_merge(parent::modelSettings($column), array(
                    'person' => 'relation',
                    'work_position' => 'relation',
                    'date_previous_report' => 'date_range',
                    'date_next_report' => 'date_range',
                    'healthy_mark' => 'dropdown',
                    'actions' => 'text',
                    'description' => 'text',
                    'report_code' => 'text',
                    'medical_report_period' => 'relation',
                    'medical_referral' => 'relation'
                ));
            case 'textSearch' : return array(
                    'person' => 'relation'
                );
            case 'options' : return array('open', 'form', 'delete', 'download');
            case 'mutual_forms' : return array(
                    'base_relation' => 'file'
                );
            case 'form_list_relations' : return array('not_passed_medical_abilities');
            case 'GuiTable': return array(
                'scopes' => array('byName')
            );
            default: return parent::modelSettings($column);
        }
    }
    
    public function beforeSave()
    {
        if (empty($this->id) && empty($this->file))
        {            
            $file = new File();
            $file->name = $this->getSafeEvd2Filename();
            if (Yii::app()->isWebApplication())
            {
                $file->responsible_id = Yii::app()->user->id;
            }
            $file->save();
            $this->id = $file->id;
            $this->file = $file;
        }                
        
        if (!empty($this->medical_report_period_id) && !empty($this->date_previous_report))
        {
            $period = $this->medical_report_period;            
            $this->date_next_report = SIMAHtml::getDateByPeriod($this->date_previous_report, $period->years, $period->months, $period->days);
        }
        return parent::beforeSave();
    }
    
    public function beforeMutualSave()
    {
        $old_person = Person::model()->findByPk($this->__old['person_id']);
        if (!is_null($old_person) && isset($old_person->employee))
        {
            $this->file->removeTagInBeforeMutualSave($old_person->employee->tag);
        }
        if (isset($this->person->employee))
        {
            $this->file->addTagInBeforeMutualSave($this->person->employee->tag);
        }
        
        return parent::beforeMutualSave();
    }
    
    public function afterSave()
    {
        parent::afterSave();

        if ($this->isNewRecord)
        {
            $this->file->name = $this->getSafeEvd2Filename();
            $this->file->save();
        }
    }
    
    public function getMedicalExaminationsForDateRange($begin_date, $end_date)
    {
        $filter = SafeEvd2::filter(array(
            'date_next_report' => $begin_date . '<>' . $end_date
        ));
        $criteria = new SIMADbCriteria();
        $criteria->applyModel($filter);

        $work_contracts = SafeEvd2::model()->findAll($criteria);

        return $work_contracts;
    }
    
    private function getSafeEvd2Filename()
    {
        return Yii::t('HRModule.SafeEvd2', 'MedicalReportFilename', [
            '{report_code}' => !empty($this->report_code) ? "($this->report_code)" : '',
            '{person_name}' => $this->person->DisplayName
        ]);
    }
}
