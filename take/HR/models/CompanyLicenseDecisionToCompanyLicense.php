<?php

class CompanyLicenseDecisionToCompanyLicense extends SIMAActiveRecord
{   

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'hr.company_license_decisions_to_company_licenses';
    }

    public function moduleName()
    {
        return 'HR';
    }

    public function rules()
    {
        return array(
            array('company_license_decision_id, company_license_id', 'required'),
            array('company_license_decision_id', 'checkUniq'),
            array('company_license_decision_id, company_license_id', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => array('insert', 'update'))
        );
    }
    
    function checkUniq()
    {
        if (
                intval($this->__old['company_license_decision_id']) !== intval($this->company_license_decision_id) || 
                intval($this->__old['company_license_id']) !== intval($this->company_license_id)
            )
        {
            $company_license_decision_to_company_license = CompanyLicenseDecisionToCompanyLicense::model()->findByAttributes([
                'company_license_decision_id'=>$this->company_license_decision_id,
                'company_license_id'=>$this->company_license_id
            ]);
            if (!is_null($company_license_decision_to_company_license))
            {
                $this->addError('company_license_decision_id', Yii::t('HRModule.CompanyLicense', 'CompanyLicenseDecisionToCompanyLicenseAlreadyExist',[
                    '{company_license_decision_name}'=>$this->company_license_decision->DisplayName,
                    '{company_license_name}'=>$this->company_license->DisplayName
                ]));
            }
        }
    }
    
    public function relations($child_relations = [])
    {
        return array(
            'company_license_decision' => array(self::BELONGS_TO, 'CompanyLicenseDecision', 'company_license_decision_id'),
            'company_license' => array(self::BELONGS_TO, 'CompanyLicense', 'company_license_id')            
        );
    }

    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters' : return array(
                'company_license_decision' => 'relation',
                'company_license' => 'relation'                
            );
            default : return parent::modelSettings($column);
        }
    }
    
    public static function add($company_license_decision_id, $company_license_id)
    {
        $company_license_decision_to_company_license = CompanyLicenseDecisionToCompanyLicense::model()->findByAttributes([
            'company_license_decision_id'=>$company_license_decision_id,
            'company_license_id'=>$company_license_id
        ]);
        if (is_null($company_license_decision_to_company_license))
        {
            $company_license_decision_to_company_license = new CompanyLicenseDecisionToCompanyLicense();
            $company_license_decision_to_company_license->company_license_decision_id = $company_license_decision_id;
            $company_license_decision_to_company_license->company_license_id = $company_license_id;
            $company_license_decision_to_company_license->save();
        }
    }
}
