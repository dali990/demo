<?php

class AbsenceFreeDays extends Absence
{
    public static $SUBTYPE_PAID_LEAVE_WEDDING = 'PAID_LEAVE_WEDDING';
    public static $SUBTYPE_PAID_LEAVE_DEATH_IN_FAMILY = 'PAID_LEAVE_DEATH_IN_FAMILY';
    public static $SUBTYPE_PAID_LEAVE_BLOOD_DONATION = 'PAID_LEAVE_BLOOD_DONATION';
    public static $SUBTYPE_PAID_LEAVE_CHILD_BIRTH = 'PAID_LEAVE_CHILD_BIRTH';
    public static $SUBTYPE_PAID_LEAVE_NATURAL_DISASTER = 'PAID_LEAVE_NATURAL_DISASTER';
    public static $SUBTYPE_PAID_LEAVE_SLAVA = 'PAID_LEAVE_SLAVA';
    public static $SUBTYPE_PAID_LEAVE_MILITARY_SERVICE = 'PAID_LEAVE_MILITARY_SERVICE';

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    
    public function rules()
    {
        return array_merge(parent::rules(), [
            array('subtype', 'required')
        ]);
    }

    public function defaultScope()
    {
        $alias = $this->getTableAlias(FALSE, FALSE);
        $type = self::$FREE_DAYS;
        return [
            'condition' => "$alias.type='$type'"
        ];
    }

    public function __get($column)
    {
        switch ($column)
        {            
            case 'DisplayName': return "Plaćeno odsustvo za ".$this->employee->DisplayName.' od '.$this->begin.' do '.$this->end;
            case 'SearchName': return $this->DisplayName;
            default: return parent::__get($column);
        }
    }
    
    public function relations($child_relations = [])
    {
        return array(
            'absence' => array(self::HAS_ONE, 'Absence', 'id')
        )+parent::relations();
    }
    
    public function beforeSave()
    {
        $this->type = self::$FREE_DAYS;
        
        return parent::beforeSave();
    }
    
    public function afterSave()
    {
        parent::afterSave();
        
        Yii::app()->warnManager->sendWarns('LegalShowWarnForNotConfirmedEmployeesSickLeavesAndFreeDays', 'WARN_NOT_CONFIRMED_EMPLOYEES_SICK_LEAVES_AND_FREE_DAYS');
    }
    
    public function afterDelete() 
    {
        parent::afterDelete();
        
        Yii::app()->warnManager->sendWarns('LegalShowWarnForNotConfirmedEmployeesSickLeavesAndFreeDays', 'WARN_NOT_CONFIRMED_EMPLOYEES_SICK_LEAVES_AND_FREE_DAYS');
    }
    
    public static function getSubTypeData()
    {
        return [
            AbsenceFreeDays::$SUBTYPE_PAID_LEAVE_WEDDING => Yii::t('HRModule.Absence', 'PAID_LEAVE_WEDDING'),
            AbsenceFreeDays::$SUBTYPE_PAID_LEAVE_DEATH_IN_FAMILY => Yii::t('HRModule.Absence', 'PAID_LEAVE_DEATH_IN_FAMILY'),
            AbsenceFreeDays::$SUBTYPE_PAID_LEAVE_BLOOD_DONATION => Yii::t('HRModule.Absence', 'PAID_LEAVE_BLOOD_DONATION'),
            AbsenceFreeDays::$SUBTYPE_PAID_LEAVE_CHILD_BIRTH => Yii::t('HRModule.Absence', 'PAID_LEAVE_CHILD_BIRTH'),
            AbsenceFreeDays::$SUBTYPE_PAID_LEAVE_NATURAL_DISASTER => Yii::t('HRModule.Absence', 'PAID_LEAVE_NATURAL_DISASTER'),
            AbsenceFreeDays::$SUBTYPE_PAID_LEAVE_SLAVA => Yii::t('HRModule.Absence', 'PAID_LEAVE_SLAVA'),
            AbsenceFreeDays::$SUBTYPE_PAID_LEAVE_MILITARY_SERVICE => Yii::t('HRModule.Absence', 'PAID_LEAVE_MILITARY_SERVICE')
        ];
    }
}
