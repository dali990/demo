<?php

class Absence extends SIMAActiveRecord
{
    public static $ANNUAL_LEAVE = 'ANNUAL_LEAVE';
    public static $SICK_LEAVE = 'SICK_LEAVE';
    public static $FREE_DAYS = 'FREE_DAYS';
    
    public $file_version_id;
    public $absent_on_date = null;
    
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
 
    public function tableName()
    {
       return 'hr.absences';
    }
    
    public function moduleName() 
    {
        return 'HR';
    }
    
    public function __get($column)
    {
        switch ($column)
        {
//            case 'path2': return 'HR/absence/absence';
            case 'work_hours': return $this->getWorkHours();
            case 'DisplayName': return "Izostanak (".  $this->columnDisplays('type')."):".$this->employee->DisplayName.' od '.$this->begin;
            case 'SearchName': return $this->DisplayName;
            case 'isAnnualLeave': return $this->type===Absence::$ANNUAL_LEAVE;
            case 'isFreeDays': return $this->type===Absence::$FREE_DAYS;
            case 'isSickLeave': return $this->type===Absence::$SICK_LEAVE;
            case 'isAccepted': return $this->confirmed === true;
            case 'isDeclined': return $this->confirmed === false;
            case 'isNotResolved': return $this->confirmed === null;
            default: return parent::__get($column);
        }
    }
    
    public function scopes()
    {
        $alias = $this->getTableAlias();
        return [
            'byName' => [
                'order' => $alias.'.begin DESC'
            ],
            'byBeginDateAscOrder' => [
                'order' => $alias.'.begin ASC'
            ],
            'confirmed' => [
                'condition' => $alias.'.confirmed=TRUE'
            ],
            'not_military_service' => [
                'condition' => $alias.'.subtype!=\''.AbsenceFreeDays::$SUBTYPE_PAID_LEAVE_MILITARY_SERVICE.'\''
            ]
        ];
    }
    
    public function rules()
    {
        return array(
            ['id','changesFromSubType'],
            array('employee_id , begin, type', 'required'),
            array('begin, end,confirmed,confirmed_timestamp, type, confirmed_user_id, description, year_id', 'safe'), 
            ['neto', 'safe'],
            ['neto', 'numerical', 'min'=>1],
            ['begin, end', 'date', 'format' => Yii::app()->params['date_format']],
            array('end', 'checkDates', 'on' => array('insert', 'update')), 
            array('end', 'checkActiveOnDatesDates', 'on' => array('insert', 'update')), 
            array('number_work_days', 'checkNumberWorkDays', 'on' => array('insert', 'update')),
            ['begin', 'checkAbsenceIntersection', 'on' => ['insert', 'update']],
            ['begin', 'checkIfDateChangedOnConfirmedAbsence', 'on' => ['insert', 'update']],
            array('employee_id,confirmed_timestamp,confirmed_user_id, document_id, begin, end', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => array('insert', 'update')),
//            ['begin', 'simaActivity_checkConfirmDate'],
//            ['confirm', 'simaActivity_checkConfirm']
        );
    }
    
    //MilosS(7.7.2017) Ovo sam dodao da se ne bi vrsile provere osnovnog tipa jer su prave provere u modelSettings izvedenih klasa
    //potrebno je uvek otvara
    public function changesFromSubType()
    {
        if (get_class($this) === 'Absence')
        {
            throw new SIMAException('izmene moraju da se izvrse iz podtipa'.  get_class($this));
        }
    }
    
    public function relations($child_relations = [])
    {
        return parent::relations(array_merge([
            'employee' => array(self::BELONGS_TO, 'Employee', 'employee_id'),
            'confirmed_user' => array(self::BELONGS_TO, 'User', 'confirmed_user_id'),
            'comment_thread'=>array(self::BELONGS_TO,'CommentThread', 'comment_thread_id'),
            'document'=>array(self::BELONGS_TO, 'File', 'document_id'),
            'year' => array(self::BELONGS_TO, 'Year', 'year_id'),
            'collective_annual_leave' => array(self::BELONGS_TO, 'CollectiveAnnualLeave', 'collective_annual_leave_id'),            
            'annual_leave' => array(self::HAS_ONE, 'AbsenceAnnualLeave', 'id'),
            'free_days' => array(self::HAS_ONE, 'AbsenceFreeDays', 'id'),
            'sick_leave' => array(self::HAS_ONE, 'AbsenceSickLeave', 'id')
        ], $child_relations));
    }
    
    public function netoForSubtypeCheck()
    {
        if(!empty($this->neto) && !empty($this->subtype))
        {
            $subtypes_with_alowed_neto = [
                AbsenceSickLeave::$SUBTYPE_SICK_LEAVE_CHILD_CARE_TILL_3
            ];
                        
            if(!in_array($this->subtype, $subtypes_with_alowed_neto))
            {
                $error_message = Yii::t('HRModule.Absence', 'ThisAbsenceCannotHaveNeto', [
                    '{subtype}' => Yii::t('HRModule.Absence', $this->subtype)
                ]);
                
                $this->addError('neto', $error_message);
                $this->addError('subtype', $error_message);
            }
        }
    }
    
    public function checkIfDateChangedOnConfirmedAbsence() 
    {
        if(
                !$this->isNotResolved && 
                (
                    $this->begin !== $this->__old['begin'] || 
                    $this->end !== $this->__old['end'] 
                    || intval($this->number_work_days) !== intval($this->__old['number_work_days'])
                )
          )
        {
            $this->addError('begin', Yii::t('HRModule.Absence', 'CannotChangePeriodOffConfirmedAbsence'));
        }
    }
 
//    public function simaActivity_checkConfirmDate()
//    {
//        if(!empty($this->begin)
//                && !empty($this->employee)
//                && !empty($this->employee->user))
//        {
//            $begin_day = Day::getByDate($this->begin);
//            
//            $last_confirm_day = $this->employee->user->last_report_day;
//
//            if(!empty($last_confirm_day)
//                    && ($begin_day->compare($last_confirm_day) <= 0))
//            {
//                $this->addError('begin', Yii::t('SIMAActivityManager.Activity', 'CannotCreateActivityInUserConfirmedDay'));
//                $this->addError('end', Yii::t('SIMAActivityManager.Activity', 'CannotCreateActivityInUserConfirmedDay'));
//            }
//        }
//    }
    
//    public function simaActivity_checkConfirm()
//    {
//        if(!empty($this->begin)
//                && !empty($this->employee)
//                && !empty($this->employee->user))
//        {
//            $begin_day = Day::getByDate($this->begin);
//            $last_boss_confirm_day = $this->employee->user->last_boss_confirm_day;
//
//            if(!empty($last_boss_confirm_day)
//                    && ($begin_day->compare($last_boss_confirm_day) <= 0))
//            {
//                $this->addError('begin', Yii::t('SIMAActivityManager.Activity', 'CannotConfirmActivityInBossConfirmedDay'));
//                $this->addError('end', Yii::t('SIMAActivityManager.Activity', 'CannotConfirmActivityInBossConfirmedDay'));
//            }
//        }
//    }
    
    public function checkNumberWorkDays()
    {
        if(!$this->hasErrors())
        {
            if(empty($this->number_work_days) && empty($this->end))
            {
                $this->addError('number_work_days', "Morate uneti ili broj radnih dana ili kraj.");
                $this->addError('end', "Morate uneti ili broj radnih dana ili kraj.");
            }
//            else if(!empty($this->number_work_days) && !empty($this->end))
//            {
//                $number_work_days = $this->CalculateNumberWorkDaysBetweenDates($this->begin, $this->end);
//                
//                if($number_work_days != $this->number_work_days)
//                {
//                    $this->addError('number_work_days', "Uneti broj radnih dana se ne poklapa sa datumom koji ste odabrali.");
//                }
//            }
        }
    }    
    
    public function checkDates()
    {        
        if(!$this->hasErrors() && !empty($this->end))
        {   
            if(strtotime($this->begin)>strtotime($this->end))
            {
               $this->addError('end', "Datum kraja odmora mora biti veci od datuma pocetka.");
            }
            else 
            {
                Day::getDayID($this->end);
            }
        }
    }
    
    public function checkActiveOnDatesDates()
    {
        if(!$this->hasErrors() && !empty($this->end))
        {                          
            $days = Day::getBetweenDates($this->begin, $this->end);

            $is_active = true;
            foreach($days as $day)
            {
                if(!$this->employee->isActiveAt($day))
                {
                    $is_active = false;
                    break;
                }
            }
                        
            if($is_active !== true)
            {
                $this->addError('end', Yii::t('HRModule.Absence', 'EmployeeNotFullyActiveInDateInterval', [
                    '{emp}' => $this->employee,
                    '{begin}' => $this->begin,
                    '{end}' => $this->end
                ]));
            }
        }
    }
    
    public function checkAbsenceIntersection()
    {
        if(!$this->hasErrors() && !empty($this->end) 
            && $this->__old['confirmed']!=$this->confirmed && $this->confirmed
            )
        {
            if($this->HaveIntersections === true)
            {
                $this->addError('begin', Yii::t('HRModule.Absence', 'ExistsAbsenceInInterval', [
                    '{emp}' => $this->employee->DisplayName
                ]));
            }
        }
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'textSearch': return array(
                'employee' => 'relation'
            );
            case 'options':
                $options = ['open'];
                if (Yii::app()->isWebApplication() && $this->isNotResolved === true)
                {
                    switch ($this->type)
                    {
                        case Absence::$ANNUAL_LEAVE:
                            $if_curr_user_absence_owner = Yii::app()->user->id === intval($this->employee_id);
                            $is_boss = Person::isBossOf(Yii::app()->user->model->person, $this->employee->person);
                            if ($if_curr_user_absence_owner || $is_boss)
                            {
                                $options[] = 'form_custom';
                                $options[] = 'delete_custom';
                            }
                            break;
                        case Absence::$SICK_LEAVE: 
                        case Absence::$FREE_DAYS: 
                            if (Yii::app()->user->checkAccess('modelAbsenceConfirm'))
                            {
                                $options[] = 'form_custom';
                            }
                            if (Yii::app()->user->checkAccess('modelAbsenceDelete'))
                            {
                                $options[] = 'delete_custom';
                            }
                            break;
                    }
                }

                return $options;
            case 'filters': return array(
                'type'=>array('dropdown'),
                'subtype'=>array('dropdown'),
                'year_id' => array('dropdown'),
                'employee' => 'relation',
                'year' => 'relation',
                'begin' => 'date_range',
                'end' => 'date_range',
                'document' => 'relation',
                'absent_on_date' => array('date_range', 'func' => 'filter_absent_on_date'),
                'collective_annual_leave' => 'relation',
                'confirmed' => 'boolean'
            );
            case 'statuses': return array(
                'confirmed' => array(
                    'title' => Yii::t('HRModule.Absence', 'Confirmed'),
                    'timestamp' => 'confirmed_timestamp',
                    'user' => array('confirmed_user_id','relName'=>'confirmed_user'),
                    'checkAccessConfirm' => 'canConfirmAbsence',
                    'checkAccessRevert' => 'checkRevertAccess',
                    'onConfirm' => 'onConfirm',
                    'onRevert' => 'onRevert',
                ));
            case'update_relations': return array(
                     'document', 'employee'
            );
            default: return parent::modelSettings($column);
        }
    }
    
    public function getreal_model()
    {
        switch ($this->type) 
        {
            case self::$ANNUAL_LEAVE:   return AbsenceAnnualLeave::model()->findByPk($this->id);
            case self::$FREE_DAYS:      return AbsenceFreeDays::model()->findByPk($this->id);
            case self::$SICK_LEAVE:     return AbsenceSickLeave::model()->findByPk($this->id);
            default:
                throw new SIMAException('Ne postoji tip -> '.$this->type);
        }
    }
    
    public function filter_absent_on_date($condition, $alias)
    {
        if(isset($this->absent_on_date))
        {
            $dateRangeParamFormated = SIMAHtml::FormatDateRangeParam($this->absent_on_date);
            
            $uniq = SIMAHtml::uniqid();
            
            $temp_condition = new CDbCriteria();
            $temp_condition->condition = "$alias.begin <= :enddate$uniq"
                    . " AND base.min_dates($alias.end, :startdate$uniq)=:startdate$uniq";
            $temp_condition->params = [
                ":startdate$uniq" => $dateRangeParamFormated['startDate'], 
                ":enddate$uniq" => $dateRangeParamFormated['endDate']
            ];
            
            $condition->mergeWith($temp_condition, true);
        }
    }

    public function forBossSubordinates($boss_id)
    {        
        $alias = $this->getTableAlias();
                        
        $this->getDbCriteria()->mergeWith([
            'condition'=>"$alias.employee_id::text in (select * from unnest(regexp_split_to_array(web_sima.get_employee_subordinate_ids(:boss_id, true), ',')))",
            'params'=>[
                ':boss_id'=>$boss_id            
            ]
        ]);
        
        return $this;
    }
    
    public function canConfirmAbsence()
    {
        $msgs = [];
        if(!isset($this->type))
        {
            $msgs[] = 'Morate postaviti tip  izostanka da biste ga potvrdili!';
        }
        
        /// ne moze da se potvrdi ako je u isplacenom periodu
        $last_pay_day = PaycheckPeriodComponent::GetLastPayOutDayInFinalPeriod();
        if(!empty($last_pay_day))
        {
            $begin_day = Day::getByDate($this->begin);
            
            if($begin_day->compare($last_pay_day) <= 0 )
            {
                $msgs[] = 'Ne možete potvrditi odsustvo u isplaćenom mesecu!';
            }
        }
        
        if(isset($this->employee_id)) //ako se ovo menja proveri situaciju za glavnog direktora
        {
            switch ($this->type)
            {
                case Absence::$ANNUAL_LEAVE:
                    if (!Person::isBossOf(Yii::app()->user->model->person, $this->employee->person))
                    {
                        $msgs[] = 'Samo direktan ili indirektan šef mogu potvrditi slobodne dane!';
                    }
                    break;
                case Absence::$SICK_LEAVE: 
                case Absence::$FREE_DAYS: 
                    if (!Yii::app()->user->checkAccess('modelAbsenceConfirm'))
                    {
                        $msgs[] = 'Nemate privilegiju za ovu potvrdu!';
                    }
                    
                    break;

                default: $msgs[] = 'Morate postaviti tip  izostanka da biste ga potvrdili!';
                    break;
            }
        }
        else
        {
            $msgs[] = 'Nije postavljen zaposleni';
        }
        
        return empty($msgs)?true:$msgs;
    }    
    
    public function checkRevertAccess()
    {
        $msgs = [];
        
        if (!Yii::app()->user->checkAccess('modelAbsenceRevertConfirm'))
        {
            $msgs[] = 'Za skidanje potvrde je potrebna posebna privilegija.';
        }
        if (get_class($this) === 'Absence')
        {
            $_msg = 'Skidanje potvrde se ne moze raditi nad klasom kojoj nije definisan tip odmora';
            $msgs[] = $_msg;
            error_log($_msg);
        }
        
        return empty($msgs)?true:$msgs;
    }
    
    //redefinise se u konkretnom odsustvu
    public function onConfirm()
    {
        
    }
    
    //redefinise se u konkretnom odsustvu
    public function onRevert()
    {
        
    }
    
    public function getBeginDate()
    {
        $day_id=Day::model()->getDayID($this->begin);
        $begin_date=Day::model()->findByPk($day_id);
        
        return $begin_date;
    }
    
    public function getEndDate()
    {
        $day_id=Day::model()->getDayID($this->end);
        $end_date=Day::model()->findByPk($day_id);
        
        return $end_date;
    }
    
    public function beforeSave()
    {
        if(isset($this->end) && !isset($this->number_work_days)) /// postavljen kranji datum
        {
            /// na osnovu datuma izracunaj broj radnih dana
            $this->number_work_days = AbsenceComponent::CalculateNumberWorkDaysBetweenDates($this);
            
        }
        else if(!isset($this->end) && isset($this->number_work_days)) /// postavljen samo broj
        {
            /// na osnovu broja odredi kranji datum
            $this->CalculateEndDateFromBegining();
                        
        }
        else if(isset($this->number_work_days) && isset($this->end))
        {            
            $this->number_work_days = AbsenceComponent::CalculateNumberWorkDaysBetweenDates($this);
        }
        
        return parent::beforeSave();
    }
    
    public function afterSave()
    {
        parent::afterSave();
//        if(isset($this->employee->person->user))
//        {
//            SIMASQLFunctions::recheckIrregularDays(
//                    false, 
//                    SIMAHtml::UserToDbDate($this->begin),
//                    SIMAHtml::UserToDbDate($this->end),
//                    $this->employee->id
//            );
//        }
        
        $this->afterSave_sendNotifications();
        $this->afterSaveDelete_sendWarns();     
    }
    
    public function afterDelete()
    {
        parent::afterDelete();
        
        $this->afterSaveDelete_sendWarns();  
    }
    
    public function validationErrorsToHTMLStr()
    {
        $result_message = '';
        
        if($this->validate() !== true)
        {
            $errors = $this->getErrors();
            $error_message = '';
            foreach($errors as $value)
            {
                if(!empty($error_message))
                {
                    $error_message .= '</br>';
                }

                $error_message .= implode('</br>', $value);
            }
            
            $result_message = Yii::t('HRModule.Absence', 'AbsenceNotValid', [
                '{emp}' => $this->employee->DisplayName,
                '{errors}' => $error_message
            ]);
        }
        
        return $result_message;
    }
    
    public function CalculateEndDateFromBegining()
    {
        if(!isset($this->begin))
        {
            throw new Exception(Yii::t('HRModule.Absence', 'EmptyBegin'));
        }
        if(!isset($this->number_work_days))
        {
            throw new Exception(Yii::t('HRModule.Absence', 'EmptyNumberDays'));
        }
        
        $dayModel = Day::get($this->begin);
        
        $count = 0;
        if(!$dayModel->IsWeekend)
        {
            $count++; 
        }
        
        while($count < $this->number_work_days)
        {
            $dayModel = $dayModel->next();
            
            if($dayModel->IsRegularWorkDay)
            {
                $count++; 
            }
        }
        
        $this->end = $dayModel->day_date;
    }
    
    public function getHaveIntersections()
    {
        $criteria = new SIMADbCriteria([
            'Model' => 'Absence',
            'model_filter' => [
                'confirmed' => true,
                'employee' => [
                    'ids' => $this->employee_id
                ],
//                'type' => $this->type,
                'absent_on_date' => $this->begin.'<>'.$this->end
            ]
        ], true);

        $hasIntersection = false;
        if(!empty($criteria->ids))
        {
            $hasIntersection = true;
        }
        
        return $hasIntersection;
    }
    
    public function recheckAndResaveWorkDays($runValidation=true)
    {
        $number_work_days = AbsenceComponent::CalculateNumberWorkDaysBetweenDates($this);
                
        if($number_work_days != $this->number_work_days)
        {
            $this->number_work_days = $number_work_days;
            $this->save($runValidation);
        }
    }
    
    private function afterSave_sendNotifications()
    {
        if($this->isNewRecord)
        {
            $this->afterSave_sendNotifications_newrecord();
        }
        else
        {
            if($this->columnChanged('confirmed'))
            {
                $this->afterSave_sendNotifications_oldrecord();
            }
        }
    }
    
    private function afterSaveDelete_sendWarns()
    {
        Yii::app()->warnManager->sendWarns('LegalEmployeeAbsences', 'WARN_CONFIRMED_ABSENCES_WITHOUT_DOCUMENT');        
        $boss_ids = $this->employee->person->getSectorDirectors();
        foreach ($boss_ids as $boss_id) 
        {            
            if ($this->isPersonUser($boss_id))
            {
                Yii::app()->warnManager->sendWarns($boss_id, 'WARN_DIRECTOR_NOT_CONFIRMED_ABSENCES');
            }
        }
    }
    
    private function afterSave_sendNotifications_newrecord()
    {
        if(empty($this->collective_annual_leave_id))
        {
            if($this->employee_id!=Yii::app()->user->id
                    && isset($this->employee->person->user))
            {
                $text = '';
                $text_short = '';
                if(Yii::app()->isWebApplication())
                {
                    $text = 'Osoba '.Yii::app()->user->model->DisplayName.' Vam je dodala odsustvo('.$this->columnDisplays('type').') od '.$this->begin.' do '.$this->end.'.';
                    $text_short = 'Osoba '.Yii::app()->user->model->DisplayName.' Vam je dodala odsustvo-'.$this->columnDisplays('type');
                }
                else
                {
                    $text = 'Dodato Vam je odsustvo('.$this->columnDisplays('type').') od '.$this->begin.' do '.$this->end.'.';
                    $text_short = 'Dodato Vam je odsustvo-'.$this->columnDisplays('type');
                }
                Yii::app()->notifManager->sendNotif($this->employee_id, $text_short, 
                    array(
                        'info' => $text,
                        'action' => array(
                            'action' => 'defaultLayout',
                            'action_id' => [
                                'model_name' => Absence::class,
                                'model_id' => $this->id
                            ]
                        ),
                        'update_tags' => array(SIMAHtml::getTag($this))
                    ));
            }

            $boss_ids = $this->employee->person->getSectorDirectors();
            foreach ($boss_ids as $boss_id)
            {
                if (
                        $boss_id == Yii::app()->user->id || //zbog privremenih pozicija, moze da se desi da je osoba sama sebi sef
                        !$this->isPersonUser($boss_id)
                   )
                {
                    continue;
                }

                $text = 'Zaposleni '.$this->employee->DisplayName.' zahteva odsustvo ('.$this->columnDisplays('type').') od '.$this->begin.' do '.$this->end.'.';
                $text_short = 'Zaposleni '.$this->employee->DisplayName.' zahteva odsustvo ('.$this->columnDisplays('type').')';
                Yii::app()->notifManager->sendNotif($boss_id, $text_short, 
                    array(
                        'info' => $text,
                        'action' => [
                            'action' => 'defaultLayout',
                            'action_id' => [
                                'model_name' => Absence::class,
                                'model_id' => $this->id
                            ]
                        ],
                        'update_tags' => array(SIMAHtml::getTag($this))
                ));
                if (in_array($this->type,[Absence::$FREE_DAYS]))
                {
                    Yii::app()->notifManager->sendNotif('modelAbsenceConfirm', $text_short, 
                    array(
                        'info' => $text,
                        'action' => array(
                            'action' => 'defaultLayout',
                            'action_id' => [
                                'model_name' => Absence::class,
                                'model_id' => $this->id
                            ]
                        ),
                        'update_tags' => array(SIMAHtml::getTag($this))
                    ));
                }
            }
        }
    }
    
    private function afterSave_sendNotifications_oldrecord()
    {
        $granted = 'Odobreno';
        if ($this->confirmed === false)
        {
            $granted = 'Nije odobreno';
        }
        else if (is_null($this->confirmed))
        {
            $granted = Yii::t('HRModule.Absence', 'RevertedAbsenceConfirmation');
        }
        $text = "$granted odsustvo - ".$this->columnDisplays('type').' od '.$this->begin.' do '.$this->end.'.';  
        if(isset($this->employee->person->user) && intval($this->employee->person->user->id) !== Yii::app()->user->id )
        {
//                if(empty($this->collective_annual_leave_id))
//                {
                Yii::app()->notifManager->sendNotif($this->employee->person->user->id, $text, 
                    array(
                        'info' => $text,
                        'action' => array(
                            'action' => 'defaultLayout',
                            'action_id' => [
                                'model_name' => Absence::class,
                                'model_id' => $this->id
                            ]
                        ),
                        'update_tags' => array(SIMAHtml::getTag($this))
                ));
//                }
        }

        /// obavestavati sefove
        $text .= ' - Za zaposlenog '.$this->employee->person->DisplayName;
        $boss_ids = $this->employee->person->getSectorDirectors();
        foreach ($boss_ids as $boss_id)
        {
            if(
                    intval($boss_id) !== intval(Yii::app()->user->id) && 
                    $this->isPersonUser($boss_id)
              )
            {
                Yii::app()->notifManager->sendNotif($boss_id, $text, 
                    array(
                        'info' => $text,
                        'action' => array(
                            'action' => 'defaultLayout',
                            'action_id' => [
                                'model_name' => Absence::class,
                                'model_id' => $this->id
                            ]
                        ),
                        'update_tags' => array(SIMAHtml::getTag($this))
                ));
            }
        }
    }
    
    public function typeIn($types_array)
    {
        $alias = $this->getTableAlias();

        $types_array_string = '';
        foreach ($types_array as $type)
        {
            $types_array_string .= "'$type'" . ',';
        }
        $types_array_string = rtrim($types_array_string, ",");
        
        $this->getDbCriteria()->mergeWith([
            'condition' => "$alias.type in ($types_array_string)",
//            'params' => [ //Sasa A. - nisam uspeo da napakujem da radi preko parametra
//                ':param1' => $types_array_string
//            ]
        ]);

        return $this;
    }
    
    public function inMonth($month_id)
    {
        $uniq = SIMAHtml::uniqid();
        $alias = $this->getTableAlias();
        $month = Month::model()->findByPkWithCheck($month_id);

        $this->getDbCriteria()->mergeWith([
            'condition' => "($alias.begin >= :param1$uniq and $alias.begin <= :param2$uniq) or ($alias.end >= :param1$uniq and $alias.end <= :param2$uniq)",
            'params' => [
                ":param1$uniq" => SIMAHtml::UserToDbDate($month->firstDay()->day_date),
                ":param2$uniq" => SIMAHtml::UserToDbDate($month->lastDay()->day_date)
            ]
        ]);

        return $this;
    }
    
    private function isPersonUser($person_id)
    {
        return User::model()->count([
            'model_filter' => [
                'ids' => $person_id
            ]
        ]) > 0;
    }
}
