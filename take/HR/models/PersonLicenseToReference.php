<?php

/**
 * @package SIMA
 * @subpackage Legal
 */
class PersonLicenseToReference extends SIMAActiveRecord
{
    public $file_version_id;
    public $file_id;    

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'hr.persons_working_licences_to_jobs_references';
    }

    public function moduleName()
    {
        return 'HR';
    }

    public function __get($column)
    {
        switch ($column)
        {
            default: return parent::__get($column);
        }
    }

    public function rules()
    {
        return array(
            array('person_license_id, reference_id', 'required'),
            array('person_license_id, reference_id', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => array('insert', 'update'))
        );
    }

    public function relations($child_relations = [])
    {
        return array(
            'file' => array(self::BELONGS_TO, 'File', 'id'),
            'person_license' => array(self::BELONGS_TO, 'PersonToWorkLicense', 'person_license_id'),
            'reference' => array(self::BELONGS_TO, 'Reference', 'reference_id'),
        );
    }

    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters' : return array(
                'person_license' => 'relation',
                'reference' => 'relation'
            );
            case 'update_relations':  return array(
                  'reference','person_license'
            );
            default : return parent::modelSettings($column);
        }
    }

    public function BeforeSave()
    {
        if (!isset($this->file) && ($this->id == null || $this->id == ''))
        {
            $reference = Reference::model()->findByPk(intval($this->reference_id));
            $person_license = PersonToWorkLicense::model()->findByPk(intval($this->person_license_id));
            $file = new File();
            $file->name = $reference->DisplayName.'_'.$person_license->DisplayName;
            $file->save();
            $file->refresh();
            $this->id = $file->id;
            $this->file = $file;
        }  
           
        return parent::BeforeSave();
    }
    
}
