<?php

/**
 * Description of SafeEvd1
 *
 * @author goran
 */
class SafeEvd15 extends SIMAActiveRecord
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'hr.safe_evd_15';
    }

    public function moduleName()
    {
        return 'HR';
    }

    public function __get($column)
    {
        switch ($column)
        {
            case 'DisplayName': return $this->name;
            case 'SearchName': return $this->name;
            default: return parent::__get($column);
        }
    }

    public function rules()
    {
        return array(
            array('name', 'required'),
            array('description', 'safe')            
        );
    }

    public function relations($child_relations = [])
    {
        return array(
            'employees'=>array(self::MANY_MANY, 'Employee', 
                'hr.employee_to_safe_evd15(safe_evd15_id, employee_id)'),            
        );
    }

    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters' : return [
                'name' => 'text',
                'description' => 'text'                    
            ];
            case 'form_list_relations' : return array('employees');
            default: return parent::modelSettings($column);
        }
    }

}
