<?php

class AnnualLeaveDecision extends SIMAActiveRecord
{    
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
 
    public function tableName()
    {
       return 'hr.annual_leave_decisions';
    }
    
    public function moduleName() 
    {
        return 'HR';
    }
    
    public function __get($column)
    {
        switch ($column)
        {
            case 'path2': return 'files/file';
            case 'DisplayName': return (isset($this->employee) && isset($this->year)) ? "Rešenje o godišnjem odmoru za ".
                    $this->employee->DisplayName.' za godinu '.
                    $this->year->DisplayName.' po ugovoru '.$this->work_contract->DisplayName:'';
            case 'SearchName': return $this->DisplayName;
            case 'first_part_must_be_used': return isset($this->year) ? SIMAHtml::DbToUserDate('31.12.'.$this->year->DisplayName) : '';
            case 'must_be_used_until': return isset($this->year) ? $this->year->param('hr.must_be_used_until') : '';
            default: return parent::__get($column);
        }
    }
    
    public function rules()
    {
        return array(
            array('employee_id, year_id, work_contract_id, days_number', 'required'),
            array('description', 'safe'),
            array('employee_id', 'checkUniq'),
            array('year_id', 'checkYear'),
            array('employee_id, work_contract_id, year_id', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => array('insert', 'update'))
        );
    }
    
    public function checkYear()
    {
        if (
                !$this->hasErrors() &&
                (
                    intval($this->__old['year_id']) !== intval($this->year_id) ||
                    intval($this->__old['work_contract_id']) !== intval($this->work_contract_id)
                )
           )
        {
            $work_contract_start_date = Day::getByDate($this->work_contract->start_date);
            $work_contract_end_date = Day::getByDate($this->work_contract->getEndDate());
            $curr_year_date = Day::getByDate("01.01.".$this->year->year);
            $curr_year = intval($curr_year_date->getyear()->year);
            
            if (
                    intval($work_contract_start_date->getyear()->year) !== $curr_year && 
                    intval($work_contract_end_date->getyear()->year) !== $curr_year &&
                    !Day::checkInRange($work_contract_start_date, $work_contract_end_date, $curr_year_date)
               )
            {
                $this->addError('year_id', 'Godina nije u opsegu ugovora o radu. Promenite ili godinu ili ugovor o radu.');
            }            
        }
    }
    
    public function checkUniq()
    {
        if (
                !$this->hasErrors() &&
                intval($this->__old['employee_id']) !== intval($this->employee_id) ||
                intval($this->__old['year_id']) !== intval($this->year_id) ||
                intval($this->__old['work_contract_id']) !== intval($this->work_contract_id)
           )
        {
            $annual_leave_decision = AnnualLeaveDecision::model()->findByAttributes([
                'employee_id'=>  $this->employee_id,
                'year_id'=>  $this->year_id,
                'work_contract_id'=>  $this->work_contract_id
            ]);
            if (!is_null($annual_leave_decision))
            {
                $this->addError('employee_id', 'Već postoji rešenje za zaposlenog '.$this->employee->DisplayName.' za '
                        .$this->year->year.' godinu za ugovor '.$this->work_contract->DisplayName);
            }
        }
    }
    
    public function relations($child_relations = [])
    {
        return array(
            'file' => array(self::BELONGS_TO, 'File', 'id'),
            'employee' => array(self::BELONGS_TO, 'Employee', 'employee_id'),
            'work_contract' => array(self::BELONGS_TO, 'WorkContract', 'work_contract_id'),
            'year' => array(self::BELONGS_TO, 'Year', 'year_id')
        );
    }    
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters': return array(
                'employee' => 'relation',
                'work_contract' => 'relation',
                'year' => 'relation',
                'description' => 'text'
            );
            case 'textSearch': return array(
                'employee' => 'relation',
                'work_contract' => 'relation'
            );
            case 'options': 
                $options = ['form', 'open', 'download'];      
                if (!empty($this->file) && !$this->file->has_filing)
                {
                    $options[] = 'delete';
                }
                
                return $options;
            case 'mutual_forms' : return array(
                'base_relation' => 'file'
            );
            default: return parent::modelSettings($column);
        }
    }    
    
    /**
     * skidamo odredjeni broj dana, ako nije zadat broj dana onda skidamo sve preostale dane
     * @param int $annual_leave_id - id godisnjeg odmora
     * @param int $remove_days - broj dana koji se skida
     * @throws Exception
     */
    public function removeDays($annual_leave_id, $remove_days=null)
    {
        if (!isset($remove_days))
        {            
            $to_remove = $this->getRemainingDays();
        }
        else
        {
            $to_remove = $remove_days;
        }
        
        if ($to_remove < 0)
        {
            throw new SIMAWarnException($this->DisplayName .' ima broj dana na raspolaganju manji od traženog');
        }
        else
        {
            $annual_leave_decisions_to_annual_leaves = AnnualLeaveDecisionToAnnualLeave::model()->findByAttributes([
                'annual_leave_decision_id'=>$this->id,
                'annual_leave_id'=>$annual_leave_id
            ]);
            if (is_null($annual_leave_decisions_to_annual_leaves))
            {
                $annual_leave_decisions_to_annual_leaves = new AnnualLeaveDecisionToAnnualLeave();
                $annual_leave_decisions_to_annual_leaves->annual_leave_decision_id = $this->id;
                $annual_leave_decisions_to_annual_leaves->annual_leave_id = $annual_leave_id;
                $annual_leave_decisions_to_annual_leaves->days_number = $to_remove;
                $annual_leave_decisions_to_annual_leaves->save();
            }
        }
    }
    
    /**
     * Vraca broj dana koji je na raspolaganju
     * @return int
     */
    public function getRemainingDays()
    {
        $used_days = 0;
        $annual_leave_decisions_to_annual_leaves = AnnualLeaveDecisionToAnnualLeave::model()->findAllByAttributes([
            'annual_leave_decision_id'=>$this->id
        ]);
        if (count($annual_leave_decisions_to_annual_leaves) > 0)
        {
            foreach ($annual_leave_decisions_to_annual_leaves as $annual_leave_decision_to_annual_leave)
            {
                $used_days += $annual_leave_decision_to_annual_leave->days_number;
            }
        }
        
        return ($this->days_number - $used_days);
    }
    
    public function generateDocument()
    {
        if (isset($this->file))
        {
            $template_id = Yii::app()->configManager->get('HR.annual_leave_decision_template');
            if (empty($template_id))
            {
                throw new SIMAWarnException('Nije postavljen template za rešenje o godišnjem odmoru. Molimo Vas da kontaktirate administratora.');
            }

            $templated_file = TemplatedFile::model()->with('templated_file_data')->findByPk($this->id);
            if ($templated_file === null || intval($templated_file->template_id) !== intval($template_id))
            {
                $template_file_id = TemplateController::setTemplate($this->id, $template_id);
            }
            else
            {
                $template_file_id = $templated_file->id;
            }
            TemplateController::addVersion($template_file_id);
        }
    }
    
    public function calcAnnualLeaveDays($until_today=false)
    {         
        $from_day = $this->getFromDay($this->work_contract);
        $to_day = $this->getToDay($this->work_contract);
        
        if ($until_today === true)
        {
            $today_date = Day::getByDate(date('Y-m-d'));
            if (Day::checkInRange($from_day, $to_day, $today_date) === true)
            {
                $to_day = $today_date;
            }
        }
        
        $days_number = WorkContractComponent::getWorkExpBetweenTwoDates($from_day, $to_day);        
        $months_number = floor($days_number/30);
        
        $annual_leave_days_number_per_year = $this->year->param('hr.number_of_annual_leave_days');
        $annual_leave_days_number_per_month = round($annual_leave_days_number_per_year/12, 2);
        $annual_leave_days_number_round_type = Yii::app()->configManager->get('HR.annual_leave_days_number_round_type', false);
        if (is_null($annual_leave_days_number_round_type) || $annual_leave_days_number_round_type === 'UP')
        {
            $annual_leave_days_number = round($annual_leave_days_number_per_month * $months_number);
        }
        else
        {
            $annual_leave_days_number = floor($annual_leave_days_number_per_month * $months_number);
        }
        
        return $annual_leave_days_number;
    }
    
    private function getFromDay($work_contract)
    {
        $year = $this->year->year;
        $min_from_day = Day::getByDate("01.01.$year");        
        $from_day = Day::getByDate($work_contract->start_date);
        $prev_work_contract = $work_contract->prev();
        
        if ($from_day->compare($min_from_day) === -1)
        {
            return $min_from_day;
        }
        else if (is_null($prev_work_contract))
        {
            return $from_day;
        }
        else
        {
            return $this->getFromDay($prev_work_contract);
        }                
    }
    
    private function getToDay($work_contract)
    {
        $year = $this->year->year;
        $max_to_day = Day::getByDate("31.12.$year");        
        $to_day = Day::getByDate($work_contract->getEndDate());
        $next_work_contract = $work_contract->next();
        
        if ($to_day->compare($max_to_day) === 1)
        {
            return $max_to_day;
        }
        else if (is_null($next_work_contract))
        {
            return $to_day;
        }
        else
        {
            return $this->getToDay($next_work_contract);
        }                
    }
    
    public function getLastRelatedWorkContract($work_contract)
    {
        $year = $this->year->year;
        $max_to_day = Day::getByDate("31.12.$year");        
        $to_day = Day::getByDate($work_contract->getEndDate());
        $next_work_contract = $work_contract->next();
        
        if ($to_day->compare($max_to_day) === 1 || is_null($next_work_contract))
        {
            return $work_contract;
        }
        else
        {
            return $this->getLastRelatedWorkContract($next_work_contract);
        }                
    }    
}
