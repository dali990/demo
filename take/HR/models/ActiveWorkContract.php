<?php

/**
 * @package SIMA
 * @subpackage Legal
 */
class ActiveWorkContract extends WorkContract
{
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    
    public function defaultScope()
    {
        $alias = $this->getTableAlias(FALSE, FALSE);
        $cwc_table = CloseWorkContract::model()->tableName();
        $wc_table = WorkContract::model()->tableName();
        
        return array(
            'condition' => "$alias.start_date<now() " //ugovor mora da je poceo
                . " and $alias.id not in (select work_contract_id from $cwc_table where end_date<now() and work_contract_id is not null) " //ne sme da ima prestanak radnog odnosa
                . " and $alias.id in (select id from (select id,rank() over (partition by employee_id ORDER BY start_date DESC) from $wc_table ) aa where rank=1)" //zadnji ugovor
                . " and ($alias.end_date>now() or $alias.end_date is null)" //ugovor mora da je u toku
        );
    }    
}
