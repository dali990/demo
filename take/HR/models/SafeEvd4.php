<?php

/**
 * Description of SafeEvd4
 *
 * @author goran
 */
class SafeEvd4 extends SIMAActiveRecord
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'hr.safe_evd_4';
    }

    public function moduleName()
    {
        return 'HR';
    }

    public function __get($column)
    {
        switch ($column)
        {
            case 'person_name': return isset($this->person) ? $this->person . SIMAHtml::modelDialog(Person::model()->findByPk($this->person->id)) : '';
            case 'company_name': return isset($this->person) ? $this->company . SIMAHtml::modelDialog(Company::model()->findByPk($this->company->id)) : '';
            case 'DisplayName': return $this->id . '-' . $this->person;
            case 'SearchName': return $this->DisplayName;
            default: return parent::__get($column);
        }
    }

    public function rules()
    {
        return array(
            array('person_id, work_position_id, company_id', 'required'),
            array('international_diagnosis_id, degree, person_ability, description', 'safe'),
            array('work_position_id, person_id, company_id', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => array('insert', 'update'))
        );
    }

    public function relations($child_relations = [])
    {
        return array(
            'person' => array(self::BELONGS_TO, 'Person', 'person_id',
                'condition' => 'company_id=' . Yii::app()->company->id),
            'work_position' => array(self::BELONGS_TO, 'WorkPosition', 'work_position_id'),
            'company' => array(self::BELONGS_TO, 'Company', 'company_id'),
            'international_diagnosis' => array(self::BELONGS_TO, 'InternationalDiagnosisOfProfessionalDiseases', 'international_diagnosis_id')
        );
    }

    public function scopes()
    {
        return array(
            'byName' => array('order' => 'id'),
        );
    }

    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters' : return [
                    'person' => 'relation',
                    'work_position' => 'relation',
                    'company' => 'relation',
                    'international_diagnosis' => 'relation',
                    'degree' => 'text',
                    'person_ability' => 'text',
                    'description' => 'text'
                ];
            case 'options': return ['form','delete'];
            default: return parent::modelSettings($column);
        }
    }

}
