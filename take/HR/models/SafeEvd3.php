<?php

/**
 * Description of SafeEvd3
 *
 * @author goran
 */
class SafeEvd3 extends SIMAActiveRecord
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'hr.safe_evd_3';
    }

    public function moduleName()
    {
        return 'HR';
    }

    public function __get($column)
    {
        switch ($column)
        {
            case 'path2': return 'files/file';
            case 'DisplayName': return $this->employee->DisplayName . '-' . $this->type_injury . '-' . $this->date;
            case 'SearchName': return $this->DisplayName;
            default: return parent::__get($column);
        }
    }

    public function rules()
    {
        return array(
            array('employee_id, work_position_id', 'required'),
            array('date, type_injury, mark_injury, description, source_injury_id, mode_injury_id', 'safe'),
            ['confirmed, citizenship, employment_status, work_exp_years, work_exp_months, work_exp_days, '
                . 'beginning_working_hour, '
                . 'commuting_accidents, work_environment, work_process, specific_physical_activity, '
                . 'contact_injury, increased_risk, boss_of_injured, eyewitness_id, form_filling_date, '
                . 'injury_diagnosis, external_cause_of_injury, injury_part_of_body, doctor_remarks, mt3_days_unable_work, '
                . 'estimated_lost_days, doctor_examined_date, date_of_received_report, violation_date, insurance_settlement_id, '
                . 'form_filling_settlement_id, hospital_id, doctor_examined_settlement_id, unique_injury_number, injury_location_address_id', 'safe'],
            ['employee_id', 'checkEmployeeContracts', 'on' => ['insert', 'update']],
            array('work_position_id, date, employee_id', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => array('insert', 'update'))
        );
    }

    public function relations($child_relations = [])
    {
        $safe_evd_3_work_contracts = SafeEvd3ToWorkContract::model()->tableName();
        return array(
            'employee' => array(self::BELONGS_TO, 'Employee', 'employee_id'),
            'work_position' => array(self::BELONGS_TO, 'WorkPosition', 'work_position_id'),
            'file' => array(self::BELONGS_TO, 'File', 'id'),
            'source_injury' => array(self::BELONGS_TO, 'SourceInjury', 'source_injury_id'),
            'mode_injury' => array(self::BELONGS_TO, 'ModeInjury', 'mode_injury_id'),
            'safe_evd_3_work_contracts' => [self::MANY_MANY, WorkContract::class, "$safe_evd_3_work_contracts(safe_evd_3_id, work_contract_id)"],
            'confirmed_user' => [self::BELONGS_TO, User::class, 'confirmed_user_id'],
            'boss_of_injured' => [self::BELONGS_TO, Employee::class, 'boss_of_injured_id'],
            'form_filling_settlement' => [self::BELONGS_TO, City::class, 'form_filling_settlement_id'],
            'doctor_examined_settlement' => [self::BELONGS_TO, City::class, 'doctor_examined_settlement_id'],
            'insurance_settlement' => [self::BELONGS_TO, City::class, 'insurance_settlement_id'],
            'hospital' => [self::BELONGS_TO, Company::class, 'hospital_id'],
            'eyewitness' => [self::BELONGS_TO, Person::class, 'eyewitness_id'],
            'injury_location_address' => [self::BELONGS_TO, Address::class, 'injury_location_address_id'],
        );
    }

    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'options': return [
                'delete','form','open'
            ];
            case 'filters' : return [
                    'employee' => 'relation',
                    'work_position' => 'relation',
                    'date' => 'date_range',
                    'type_injury' => 'dropdown',
                    'mark_injury' => 'dropdown',                                        
                    'description' => 'text',
                    'source_injury' => 'relation',
                    'mode_injury' => 'relation',
                    'display_evd_number' => 'text'
                ];
            case 'options': return ['form','delete'];
            case 'mutual_forms': return ['base_relation' => 'file'];
            case 'textSearch' : return [
                'employee' => 'relation',
                'type_injury' => 'text'
            ];
            case 'form_list_relations' : return ['safe_evd_3_work_contracts'];
            case 'update_relations': return ['file', 'employee'];
            case 'statuses': return [
                'confirmed' => [
                    'title' => SafeRecordController::$SAFE_RECORDS[3]['name'],
                    'timestamp' => 'confirmed_timestamp',
                    'user' => ['confirmed_user_id', 'relName' => 'confirmed_user'],
//                    'checkAccessConfirm' => 'Confirm',
//                    'checkAccessRevert' => 'Revert',
                    'onConfirm' => 'onConfirm'
                ]
            ];    
            default: return parent::modelSettings($column);
        }
    }
    
    public function beforeMutualSave()
    {
        $this->file->name = $this->DisplayName;
        
        if ($this->isNewRecord)
        {
            $this->file->addTagInBeforeMutualSave($this->employee->tag);
        }

        return parent::beforeMutualSave();
    }
    
    public function afterDelete() 
    {
        parent::afterDelete();
        
        $this->file->removeTagByModel($this->employee->tag);
        $this->file->save();
    }
    
    public function beforeSave()
    {
        if (
                $this->isNewRecord ||
                $this->columnChanged('employee_id') ||
                $this->hasEmployeeWorkContractsChanged()
            )
        {
            $safe_evd_3_work_contracts_ids = $this->getEmployeeWorkContractsListIds();
            $worked_days = 0;
            foreach($safe_evd_3_work_contracts_ids as $ewc_id)
            {
                $wc = WorkContract::model()->findByPkWithCheck($ewc_id);
                $worked_days += $wc->getWorkExperience();
            }
            $result = WorkContractComponent::formatWorkExp($worked_days, true);
            $this->work_exp_years = $result['y'];
            $this->work_exp_months = $result['m'];
            $this->work_exp_days = $result['d'];
        }
        return parent::beforeSave();
    }
    
    public function checkEmployeeContracts()
    {
        if (
                !$this->hasErrors() && 
                (
                    $this->isNewRecord ||
                    $this->columnChanged('employee_id') ||
                    $this->hasEmployeeWorkContractsChanged()
                )
            )
        {
            $safe_evd_3_work_contracts = new SIMADbCriteria([
                'Model' => WorkContract::class,
                'model_filter' => [
                    'employee' => ['ids' => $this->employee_id]
                ]
            ], true);
            $safe_evd_3_work_contracts_list_ids = $this->getEmployeeWorkContractsListIds();

            if(!empty($safe_evd_3_work_contracts_list_ids))
            {
                $ewc_ids = explode(",", $safe_evd_3_work_contracts->ids);
                //vraca entries iz niza $a koji ne postoje u nizu $b
                $entries = array_diff($safe_evd_3_work_contracts_list_ids, $ewc_ids);
                if(!empty($entries))
                {
                    $this->addError('safe_evd_3_work_contracts_list',  Yii::t('HRModule.SafeEvd3','WrongWorkContractsForEmployee'));
                }
            }
        }
    }
    
    /**
     * Proverava da li je izmenja lista izabranih ugovora o radu za izracunavanja radnog staza u izvestaju o povredi na radu
     * @return bool
     */
    private function hasEmployeeWorkContractsChanged(): bool
    {
        $old_safe_evd_3_work_contracts = new SIMADbCriteria([
            'Model' => WorkContract::class,
            'model_filter' => [
                'safe_evd_3_work_contracts' => [
                    'safe_evd_3' => ['ids' => $this->id]
                ]
            ]
        ], true);
        $old_safe_evd_3_work_contracts_ids = explode(",", $old_safe_evd_3_work_contracts->ids);        
        
        $new_safe_evd_3_work_contracts_ids = $this->getEmployeeWorkContractsListIds();
        
        // Potreban array_diff sa obrnutim redosledom kako bi pretraga razlike bila simetricna
        $diffs = array_merge(
                    array_diff($old_safe_evd_3_work_contracts_ids, $new_safe_evd_3_work_contracts_ids),
                    array_diff($new_safe_evd_3_work_contracts_ids, $old_safe_evd_3_work_contracts_ids)
                );
        
        return !empty($diffs);
    }
    
    /**
     * Vraca niz id-a izabranih ugovora o radu za preracunavanje radnog staza u formi SafeEvd3
     * @return array
     */
    private function getEmployeeWorkContractsListIds(): array
    {
        $safe_evd_3_work_contracts_list_ids = [];
        if(gettype($this->safe_evd_3_work_contracts) === 'string')
        {
            $safe_evd_3_work_contracts_list = CJSON::decode($this->safe_evd_3_work_contracts);
            $safe_evd_3_work_contracts_list_ids = array_column($safe_evd_3_work_contracts_list['ids'], 'id');
        }
        else
        {
            foreach ($this->safe_evd_3_work_contracts as $work_contract)
            {
                $safe_evd_3_work_contracts_list_ids[] = $work_contract->id;
            }
        }
        return $safe_evd_3_work_contracts_list_ids;
    }
    
    public function onConfirm()
    {        
        if($this->confirmed)
        {           
            $se3r = new SafeEvd3Report(["save_evd_3" => $this]);
            $se3r->validateSegments();
            $temp_file = $se3r->getPDF();        
            $this->file->appendTempFile(
                $temp_file->getFileName(), 
                $this->DisplayName . '.pdf'
            );
        }
    }
    
}
