<?php

/**
 * @package SIMA
 * @subpackage HR
 */
class IKSPoint extends SIMAActiveRecord
{
    public $group;
    public $number_of_points;
    
    public static $MIN_POINTS_YEAR_QUOTA = 15;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'hr.iks_points';
    }

    public function moduleName()
    {
        return 'HR';
    }

    public function __get($column)
    {
        switch ($column)
        {
            case 'activity_name':
                return $this->activity->DisplayHtml;
            case 'activity':
                $Model = $this->model;
                $model = $Model::model()->findByPk($this->model_id);
                return $model;
            default: return parent::__get($column);
        }
    }

    public function rules()
    {
        return array(
            array('date, model, model_id', 'required'),
            array('national_regulations_and_standards, new_materials_technologies_in_nacional_ind,'
                . 'international_regulations_and_standards, new_materials_technologies_in_internacional_ind, group, number_of_points', 'safe')
        );
    }
    
    public function relations($child_relations = [])
    {        
        return array(
            'work_licenses'=>array(self::MANY_MANY, 'WorkLicense', 'hr.iks_points_to_work_licenses(iks_point_id,work_license_id)'),
            'persons'=>array(self::HAS_MANY, 'PersonToIKSPoint', 'iks_point_id'),
        );
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters': return array_merge(parent::modelSettings($column),array(
                    'date' => 'date_range',
                    'persons' => 'relation'
                ));
            case 'options': return array();
            default: return parent::modelSettings($column);
        }
    }
    
    public function beforeSave()
    {
        if (isset($this->group) && isset($this->number_of_points) && $this->group !== '' && $this->number_of_points !== '')
        {
            $column = $this->group;
            $this->$column = $this->number_of_points;
        }
            
        return parent::beforeSave();
    }
    
    public function calcPoints($persons)
    {
        foreach ($persons as $person)
        {
            foreach ($person->work_licenses as $license) //za svaku osobu dohvatamo sve njene licence
            {
                //ako se neka licenca nalazi na spisku licenca za tu aktivnost onda toj osobi dodajemo bodove
                if ($this->hasLicense($license->id)) 
                {
                    $person_to_iks_point = PersonToIKSPoint::model()->findByAttributes(array('person_id'=>$person->id, 'iks_point_id'=>$this->id));
                    if ($person_to_iks_point === null)
                    {
                        $person_to_iks_point = new PersonToIKSPoint();
                        $person_to_iks_point->person_id = $person->id;
                        $person_to_iks_point->iks_point_id = $this->id;
                        $person_to_iks_point->save();
                    }
                    break;
                }
            }
        }
    }
    
    public function revertPoints($persons)
    {
        foreach ($persons as $person)
        {
            $person_to_iks_point = PersonToIKSPoint::model()->findByAttributes(array('person_id'=>$person->id, 'iks_point_id'=>$this->id));
            if ($person_to_iks_point !== null)
                $person_to_iks_point->delete();
        }
    }
    
    private function hasLicense($license_id)
    {
        foreach ($this->work_licenses as $license)
        {
            if ($license->id === $license_id)
                return true;
        }
        
        return false;
    }
    
    /**
     * vraca niz koji se sastoji od broja bodova i oblasti za koju se dobijaju ti bodovi
     * @return array
     */
    public function getPoints()
    {
        $points = 0;
        $section = '';
        if ($this->national_regulations_and_standards > 0)
        {
            $points = $this->national_regulations_and_standards;
            $section = $this->getAttributeLabel('national_regulations_and_standards');
        }
        else if ($this->new_materials_technologies_in_nacional_ind > 0)
        {
            $points = $this->new_materials_technologies_in_nacional_ind;
            $section = $this->getAttributeLabel('new_materials_technologies_in_nacional_ind');
        }
        else if ($this->international_regulations_and_standards > 0)
        {
            $points = $this->international_regulations_and_standards;
            $section = $this->getAttributeLabel('international_regulations_and_standards');
        }
        else if ($this->new_materials_technologies_in_internacional_ind > 0)
        {
            $points = $this->new_materials_technologies_in_internacional_ind;
            $section = $this->getAttributeLabel('new_materials_technologies_in_internacional_ind');
        }
        
        return array(
            'points'=>$points,
            'section'=>$section
        );
    }
    
    /**
     * proverava da li je postavljeno odgovorno lice unutar aktivnosti i da li je ulogovani korisnik odgovorno lice. 
     * @return boolean
     */
    public function isResponsible()
    {
        $Model = $this->model;
        $model = $Model::model()->findByPk($this->model_id);
        if ($model->owner)
            return true;
        else
            return false;
    }
    
}
