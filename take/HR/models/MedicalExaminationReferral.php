<?php

class MedicalExaminationReferral extends SIMAActiveRecord
{    
    public static $SAFE_EVD_2 = 'SAFE_EVD_2';
    public static $SAFE_EVD_13 = 'SAFE_EVD_13';    
    
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'hr.medical_examination_referrals';
    }

    public function moduleName()
    {
        return 'HR';
    }

    public function __get($column)
    {
        switch ($column)
        {
            case 'DisplayName':
            case 'SearchName': return isset($this->file)?$this->file->DisplayName:'';
            default: return parent::__get($column);
        }
    }

    public function rules()
    {
        return [
            ['person_id, work_position_id', 'required'],
            ['referral_type, description', 'safe']
        ];
    }

    public function relations($child_relations = [])
    {
        return [
            'file' => array(self::BELONGS_TO, 'File', 'id'),
            'person' => [self::BELONGS_TO, 'Person', 'person_id'],
            'work_position' => [self::BELONGS_TO, 'WorkPosition', 'work_position_id'],
            'safe_evd2_medical_reports' => [self::HAS_MANY, 'SafeEvd2', 'medical_referral_id'],
            'safe_evd13_medical_reports' => [self::HAS_MANY, 'SafeEvd13', 'medical_referral_id']
        ];
    }       

    public function scopes()
    {
        $uniq = SIMAHtml::uniqid();
        $alias = $this->getTableAlias();
        $safe_evd_2_table_name = SafeEvd2::model()->tableName();
        $safe_evd_13_table_name = SafeEvd13::model()->tableName();

        return array(
            'noConnectionToSafeEvd2' => [
                'condition'=>"$alias.id not in (select se$uniq.medical_referral_id from $safe_evd_2_table_name se$uniq where se$uniq.medical_referral_id is not null)"
            ],
            'noConnectionToSafeEvd13' => [
                'condition'=>"$alias.id not in (select se$uniq.medical_referral_id from $safe_evd_13_table_name se$uniq where se$uniq.medical_referral_id is not null)"
            ]
        );
    }    
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters' : return array(
                'file' => 'relation',
                'description'=>'text',
                'referral_type'=>'text',
                'person' => 'relation',
                'work_position' => 'relation'
            );
            case 'textSearch' : return array(
                'description'=>'text',
                'file'=>'relation'
            );
            case 'mutual_forms' : return array(
                'base_relation' => 'file'
            );
            default: return parent::modelSettings($column);
        }
    }
    
    public function beforeSave()
    {
        if (empty($this->id) && empty($this->file))
        {
            $file = new File();
            $file->name = $this->person->DisplayName;
            $file->save();
            $this->id = $file->id;
            $this->file = $file;
        }

        return parent::beforeSave();
    }
    
    public function afterSave()
    {
        parent::afterSave();

        if ($this->isNewRecord)
        {
            $this->file->name = $this->person->DisplayName;
            $this->file->save();

            if (isset($this->person->employee))
            {
                $this->file->refresh(); //mora refresh, inace nece da doda tag
                $this->file->addTagByModel($this->person->employee->tag);
            }
        }
    }
}
