<?php

/**
 * @package SIMA
 * @subpackage Legal
 */
class PersonToWorkLicense extends SIMAActiveRecord
{

    public $file_version_id;
    public $file_id;
    public $expiration_period = null;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'hr.persons_to_working_licences';
    }

    public function moduleName()
    {
        return 'HR';
    }

    public function __get($column)
    {
        switch ($column)
        {
            case 'path2': return 'files/file';
            case 'end_date': return isset($this->last_certificate->end_date)?$this->last_certificate->end_date:'';
            case 'DisplayName': 
                $return = '';
                if (isset($this->working_license) && isset($this->person))
                {
                    $return = 
                        $this->working_license->code .' | '
                        .$this->person->DisplayName .' | '
                        .$this->working_license->name ;
                }
                return $return;
            case 'SearchName': return $this->DisplayName;
            default: return parent::__get($column);
        }
    }

    public function rules()
    {
        return array(
            array('person_id, working_license_id', 'required'),
            array('file_version_id, start_date, number, description', 'safe'),
            array('person_id, working_license_id,start_date', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => array('insert', 'update'))
        );
    }

    public function relations($child_relations = [])
    {
        return array(
            'file' => array(self::BELONGS_TO, 'File', 'id'),
            'person' => array(self::BELONGS_TO, 'Person', 'person_id'),
            'working_license' => array(self::BELONGS_TO, 'WorkLicense', 'working_license_id'),
            'certificates'=>array(self::HAS_MANY, 'WorkingLicenceCertificate', 'licence_id'),
            'last_certificate'=>array(self::HAS_ONE, 'WorkingLicenceCertificate', 'licence_id', 'order'=>'end_date DESC'),
            'person_license_to_reference'=> array(self::HAS_MANY, 'PersonLicenseToReference', 'person_license_id'),
        );
    }

    public function scopes()
    {
        return array(
//            'byName' => array('order' => 'name, id'),
        );
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters': return [
                'person' => 'relation',
                'working_license' => 'relation',
                'person_license_to_reference' => 'relation',
                'last_certificate' => 'relation',
                'certificates' => 'relation',
                'expiration_period' => array('dropdown','func'=>'filter_expiration_period'),
                'person'=>'relation',
                'file'=>'relation',
                'start_date'=>'date_range',
                'end_date'=>'date_range'
            ];
            case 'textSearch': return [
                'person' => 'relation',
                'working_license' => 'relation',
            ];
            case 'options': 
                $options = ['open'];
                return $options;
            default: return parent::modelSettings($column);
        }   
    }
    
    public function filter_expiration_period($condition) 
    {
        $alias = $this->getTableAlias();
        if($this->expiration_period !== '') 
        {            
            $working_licence_certificates_table_name = WorkingLicenceCertificate::model()->tableName();
            switch($this->expiration_period) 
            {    
                case '0':
                    $cond = "$alias.id in (select licence_id from $working_licence_certificates_table_name where end_date<=(now()+'5days'::interval) and end_date is not null)"; break;                    
                case '1':     
                    $cond = "$alias.id in (select licence_id from $working_licence_certificates_table_name where end_date<=(now()+'10days'::interval) and end_date is not null)"; break;
                case '2': 
                    $cond = "$alias.id in (select licence_id from $working_licence_certificates_table_name where end_date<=(now()+'30days'::interval) and end_date is not null)"; break;
                default: 
                    $cond = ""; break;
            }
            
            $temp_cond = new CDbCriteria();
            $temp_cond->join = "left join $working_licence_certificates_table_name wlc on $alias.id=wlc.licence_id";            
            $temp_cond->condition = $cond;
            $condition->mergeWith($temp_cond);
        }
    }

    public function BeforeSave()
    {
        if(!isset($this->last_certificate))
        {
            Yii::app()->warnManager->sendWarns(Yii::app()->user->id, 'WARN_LICENSE_NOCERTIFICATE');
        }
        
        if (!isset($this->file) && ($this->id == null || $this->id == ''))
        {
            $file = new File();
            $file->name = $this->working_license->name . '_' . $this->person->DisplayName;
            $file->document_type_id=23;//HARDCODE
            $file->save();
            $file->refresh();
            $this->id = $file->id;
            $this->file = $file;
        }
        
        /** ubacivanje upload-a fajla */
        $this->file->appendUpload($this->file_version_id);
                     
        return parent::BeforeSave();
    }

    /**
     * funkcija proverava da li odredjena licenca istice
     */
    public static function checkExpire($params)
    {
        if (!isset($params['id']) || isset($params['id']))
            return;
        $person_id = $params['person_id'];
        $working_license_id = $params['working_license_id'];

        $lic = PersonToWorkLicense::model()->findByAttributes(array('person_id'=>$person_id,'working_license_id'=>$working_license_id));
        if ($lic!=null)
        {
            if ($lic->end_date!='' && (strtotime($lic->end_date)<time()))
                error_log(__METHOD__.' - '.'istice!!!');
        }
    }
    
}
