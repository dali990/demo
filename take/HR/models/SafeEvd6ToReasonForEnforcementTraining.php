<?php

class SafeEvd6ToReasonForEnforcementTraining extends SIMAActiveRecord
{   

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'hr.safe_evd6_to_reasons_for_enforcement_training';
    }

    public function moduleName()
    {
        return 'HR';
    }

    public function rules()
    {
        return array(
            array('reason_for_enforcement_training_id, safe_evd6_id', 'required'),
            array('reason_for_enforcement_training_id, safe_evd6_id', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => array('insert', 'update'))
        );
    }

    public function relations($child_relations = [])
    {
        return array(
            'reason_for_enforcement_training' => array(self::BELONGS_TO, 'ReasonForEnforcementTraining', 'reason_for_enforcement_training_id'),
            'safe_evd6' => array(self::BELONGS_TO, 'SafeEvd6', 'safe_evd6_id'),
        );
    }

    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters' : return array(
                'reason_for_enforcement_training' => 'relation',
                'safe_evd6' => 'relation'
            );
            case 'options' : return array();            
            default : return parent::modelSettings($column);
        }
    }
    
}
