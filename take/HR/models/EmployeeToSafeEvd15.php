<?php

class EmployeeToSafeEvd15 extends SIMAActiveRecord
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'hr.employee_to_safe_evd15';
    }

    public function moduleName()
    {
        return 'HR';
    }

    public function relations($child_relations = [])
    {
        return array(
            'safe_evd15' => array(self::BELONGS_TO, 'SafeEvd15', 'safe_evd15_id'),
            'employee' => array(self::BELONGS_TO, 'Employee', 'employee_id'),
        );
    }

}