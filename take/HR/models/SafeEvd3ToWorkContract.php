<?php

class SafeEvd3ToWorkContract extends SIMAActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    
    public function tableName()
    {
        return 'hr.safe_evd_3_to_work_contracts';
    }

    public function moduleName()
    {
        return 'HR';
    }

    public function __get($column)
    {
        switch ($column)
        {
            default: return parent::__get($column);
        }
    }

    public function rules()
    {
        return [
            ['safe_evd_3_id, work_contract_id', 'required'],
            ['safe_evd_3_id','unique_with','with' => ['work_contract_id']]
        ];
    }

    public function relations($child_relations = [])
    {        
        return [
            'safe_evd_3' => [self::BELONGS_TO, SafeEvd3::class, 'safe_evd_3_id'],
            'work_contract' => [self::BELONGS_TO, WorkContract::class, 'work_contract_id']
        ];
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters': return [
                    'safe_evd_3' => 'relation',
                    'work_contract' => 'relation'
                ];
            case 'textSearch': return [
                    'safe_evd_3' => 'relation',
                    'work_contract' => 'relation'
                ];
            default: return parent::modelSettings($column);
        }
    }   
}
  
