<?php

/**
 * Description of WorkContract
 *
 * @author goran-set
 */
class WorkContract extends SIMAActiveRecord
{
    public $work_time;

    public $file_version_id;
    public $with_out_position = false;    
    public $generate_work_contract_template; //template za ugovor o radu    
    public $generate_notice_of_mobbing_template; //template za obavestenje na mobing
    public $active_at = null;
    public $activeFromTo = null;
    public $with_filing = false; 
    public $currently_active = null;
    public static $benefits = [];
    public static $benefited_work_experiences = [];
    public static $employment_codes = [];
    
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'hr.work_contracts';
    }

    public function moduleName()
    {
        return 'HR';
    }

    public function __get($column)
    {
        switch ($column)
        {
            case 'path2': return 'files/file';
            case 'SearchName': 
                $file = File::model()->resetScope()->findByPk($this->id);
                return ((isset($file->filing)) ? $file->filing->filing_number . ' od ' . $file->filing->date . ' - ' : '') . $file->DisplayName;
            case 'DisplayName':
                $file = File::model()->resetScope()->findByPk($this->id);
                if (isset($file->filing))
                {
                    return $file->filing->filing_number . ' od ' . $file->filing->date;                    
                }
                else
                {
                    return $file->DisplayName;
                }
            case 'DisplayCloseContract' : return Yii::app()->controller->renderModelView($this->file, 'closeWorkContract');
            case 'endDate' : return $this->end_date; //koristi se kod generisanja template, jer je end_date predefinisan u gui delu a za ispis u template treba samo datum
            case 'duration':
                $duration = $this->getDuration();
                return $duration;
            case 'duration_text':
                $result = 'NULL';
                $duration = $this->duration;
                if(empty($duration))
                {
                    $result = Yii::t('HRModule.WorkContract', 'DurationIndefinite');
                }
                else
                {
                    $result = WorkContractComponent::formatWorkExp($duration);
                }
                return $result;
            default: return parent::__get($column);
        }
    }

    public function rules()
    {
        return array(
            array('employee_id, work_position_id, start_date, working_percent, working_percent_at_other_companies', 'required'),
            ['employment_code', 'required','message'=>Yii::t('HRModule.WorkContract', 'EmploymentCodeMess')],
            ['benefit', 'required','message'=>Yii::t('HRModule.WorkContract', 'BenefitsMess')],
            ['type_of_payer', 'required','message'=>Yii::t('HRModule.WorkContract', 'TypeOfPayerMess')],
            ['benefited_work_experience', 'required','message'=>Yii::t('HRModule.WorkContract', 'BenefitedWorkExperienceMess')],
            array('id, end_date,work_position_id, file_version_id, '
                . 'description, notice_of_mobbing_file_id, for_payout, for_work_exp, is_probationer, '
                . 'introduction_into_business_program_id, signed, occupation_id', 'safe'),
            ['working_percent', 'numerical', 'integerOnly' => 'true', 'min'=>0, 'max'=>100],
            ['working_percent_at_other_companies', 'numerical', 'integerOnly' => 'true', 'min'=>0, 'max'=>100],
            ['working_percent', 'checkWorkingPercent'],
            ['start_date', 'checkStartDate', 'on' => ['insert', 'update']],
            ['start_date', 'checkStartEndDates', 'on' => ['insert', 'update']],
            array('start_date,id,work_position_id, end_date', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => array('insert', 'update'))
        );
    }

    public function relations($child_relations = [])
    {        
        return array(
            'file' => array(self::BELONGS_TO, 'File', 'id'),
            'employee' => array(self::BELONGS_TO, 'Employee', 'employee_id'),
            'close_work_contract' => array(self::HAS_ONE, 'CloseWorkContract', 'work_contract_id'),
            'work_position' => array(self::BELONGS_TO, 'WorkPosition', 'work_position_id'),
            'director_work_position' => array(self::BELONGS_TO, 'WorkPosition', 'work_position_id', 'condition' => 'is_director=true'),
            'notice_of_mobbing' => array(self::BELONGS_TO, 'File', 'notice_of_mobbing_file_id'),
            'introduction_into_business_program' => array(self::BELONGS_TO, 'IntroductionIntoBusinessProgram', 'introduction_into_business_program_id'),
            'occupation' => [self::BELONGS_TO, Occupation::class, 'occupation_id'],
            'safe_evd_3_work_contracts' => [self::HAS_MANY, SafeEvd3ToWorkContract::class, 'work_contract_id']
        );
    }

    public function scopes()
    {
        $alias = $this->getTableAlias();
        $uniq = SIMAHtml::uniqid();
        $work_contracts_table_name = WorkContract::model()->tableName();
        
        return array(
            'byName' => array('order' => $alias.'.start_date ASC, '.$alias.'.id'),
            'orderByStartDateASC' => array('order' => $alias.'.start_date ASC, '.$alias.'.id'),
            'hasDuplicatedStartDate' => [
                'condition' => "
                    exists (
                        select 1 from $work_contracts_table_name wc2$uniq 
                        where $alias.id != wc2$uniq.id and $alias.start_date = wc2$uniq.start_date and $alias.employee_id = wc2$uniq.employee_id
                    )
                "
            ]
        );
    }
    
    public function checkWorkingPercent()
    {
        if (!$this->hasErrors())
        {
            if (($this->working_percent + $this->working_percent_at_other_companies) > 100)
            {
                $this->addError('working_percent', Yii::t('HRModule.WorkContract', 'WorkingPercentWrongSum'));
            }
        }
    }

    public function checkStartDate()
    {
        if (
                !$this->hasErrors()
                && (
                    $this->isNewRecord || $this->columnChanged([
                        'employee_id', 'start_date'
                    ])  
                )
        )
        {
            $work_contract = WorkContract::model()->findByAttributes([
                'employee_id' => $this->employee_id,
                'start_date' => $this->start_date
            ]);
            if (!empty($work_contract))
            {
                $this->addError('start_date', Yii::t('HRModule.WorkContract', 'EmployeeAlreadyHasWorkContractOnThisStartDate'));
            }
        }
    }
    
    public function checkStartEndDates()
    {
        if($this->hasErrors() || empty($this->end_date))
        {
            return;
        }
        
        $start_date_day = Day::getByDate($this->start_date);
        $end_date_day = Day::getByDate($this->end_date);
        if($start_date_day->compare($end_date_day) > 0)
        {
            $this->addError('end_date', Yii::t('HRModule.WorkContract', 'EndDateCanNotBeBeforeStartDate'));
        }
    }
    
    /**
     * 
     * @param type $startDate
     * @param type $endDate
     * @return \WorkContract
     */
    public function activeFromTo($startDate = null, $endDate = null)
    {
        if(empty($startDate))
        {
            $startDate = SIMAHtml::UserToDbDate(date('Y-m-d', strtotime("now")));
        }
        
        if(empty($endDate))
        {
            $endDate = $startDate;
        }
        
        $criteria = new SIMADbCriteria();
        
        $uniq = SIMAHtml::uniqid();
        $alias = $this->getTableAlias();
//        $wc_tablename = WorkContract::model()->tableName();
        
//        error_log(__METHOD__.' - $alias: '.$alias);

//        $active_contract_criteria = "
//            ($alias.start_date <= :enddate$uniq 
//                AND 
//                (
//                    base.min_dates(base.min_dates(
//                        base.min_dates(base.min_dates(base.min_dates($alias.end_date, (select end_date 
//                                                    from hr.close_work_contracts cwc 
//                                                    where cwc.work_contract_id=$alias.id 
//                                                    limit 1)),
//                                    (select (start_date - INTERVAL '1 day')::date from $wc_tablename wc_next
//                                    where wc_next.employee_id=$alias.employee_id 
//                                    and wc_next.start_date>$alias.start_date
//                                    order by wc_next.start_date ASC
//                                    limit 1)), 
//                        (select max(end_date) from hr.work_contracts wc_max 
//                            where wc_max.employee_id=$alias.employee_id and wc_max.start_date=$alias.start_date and wc_max.id>$alias.id 
//                                and end_date is not null)),
//                        (select max(end_date) from hr.close_work_contracts cwc 
//                                where cwc.work_contract_id in (select id from hr.work_contracts wc_max 
//                                                                where wc_max.employee_id=$alias.employee_id and wc_max.start_date=$alias.start_date  
//                                                                    and wc_max.id>$alias.id))
//                    )
//                    , :startdate$uniq)=:startdate$uniq
//                )
//            )
//        ";
        $active_contract_criteria = "
            ($alias.start_date <= :enddate$uniq 
                AND 
                (
                    base.min_dates($alias.calculated_end_date, :startdate$uniq)=:startdate$uniq
                )
            )
        ";
        
//        if($active === false)
//        {
//            error_log(__METHOD__.' - $active: '.$active);
//            $active_contract_criteria = " NOT (".$active_contract_criteria.")";
//        }

        $criteria->condition = $active_contract_criteria;
            
        $criteria->params = [
            ":startdate$uniq" => $startDate, 
            ":enddate$uniq" => $endDate
        ];
        
//        error_log(__METHOD__.' - $criteria->condition: '.$criteria->condition);
        
        $this->getDbCriteria()->mergeWith($criteria);
        
        return $this;
    }
    
    /**
     * Vraca prethodni povezan ugovor. Ako je $only_active_at_day_before=false onda vraca prethodni ugovor
     * @param boolean $only_active_at_day_before
     * @return WorkContract ili null
     */
    public function prev($only_active_at_day_before=true)
    {
        $wcCriteria = new SIMADbCriteria([
            'Model' => 'WorkContract',
            'model_filter' => [
                'employee' => [
                    'ids' => $this->employee->id
                ],
                'order_by' => 'start_date DESC',
                'start_date' => ['<',SIMAHtml::UserToDbDate($this->start_date)]
            ]
        ]);

        $prev_work_contract = WorkContract::model()->find($wcCriteria);
        
        if($only_active_at_day_before === true)
        {            
            if (is_null($prev_work_contract))
            {
                return null;
            }
            else
            {                
                $wc_start_date = $this->start_date;
                $start_date_day = Day::getByDate($wc_start_date);
                $prev_day = $start_date_day->prev();                
                $prev_work_contract_end_date = Day::getByDate($prev_work_contract->getEndDate());
                if ($prev_work_contract_end_date->compare($prev_day) === 0)
                {
                    return $prev_work_contract;
                }
                else
                {
                    return null;
                }
            }
        }
        else
        {
            return $prev_work_contract;
        }                        
    }
    
    /**
     * Vraca sledeci povezan ugovor. Ako je connected=false onda vraca sledeci ugovor
     * @param boolean $connected
     * @return WorkContract ili null
     */
    public function next($connected=true)
    {   
        if ($connected === true)
        {
            $wc_end_date = $this->getEndDate();
            $end_date_day = Day::getByDate($wc_end_date);
            $next_day = $end_date_day->next();
            $wcCriteria = new SIMADbCriteria([
                'Model' => 'WorkContract',
                'model_filter' => [
                    'employee' => [
                        'ids' => $this->employee->id
                    ],
                    'start_date'=>$next_day
                ]
            ]);
        }
        else
        {
            $wcCriteria = new SIMADbCriteria([
                'Model' => 'WorkContract',
                'model_filter' => [
                    'employee' => [
                        'ids' => $this->employee->id
                    ],
                    'order_by' => 'start_date ASC',
                    'start_date' => ['>',SIMAHtml::UserToDbDate($this->start_date)]
                ]
            ]);            
        }
        
        return WorkContract::model()->find($wcCriteria);                
    }
    
    public function getEndDate(Day $enddatelimit=null)
    {
        if (isset($enddatelimit))
        {
            $limit_date = $enddatelimit;
        }
        else
        {
            $limit_date = Day::get();
        }
        $limit_date_date = new DateTime($limit_date->day_date);
        if (isset($this->calculated_end_date))
        {
            $calculated_end_date_date =  new DateTime($this->calculated_end_date);
            if ($calculated_end_date_date < $limit_date_date)
            {
                return $this->calculated_end_date;
            }
            else
            {
                return $limit_date->day_date;
            }
        }
        else
        {
            return $limit_date->day_date;
        }
        
    }
    
    public function getStartDate(Day $startdatelimit=null)
    {
        if (isset($startdatelimit))
        {
            $calculated_limit_start_date_date =  new DateTime($startdatelimit->day_date);
            $calculated_start_date_date =  new DateTime($this->start_date);
            if ($calculated_start_date_date < $calculated_limit_start_date_date)
            {
                return $startdatelimit->day_date;
            }
            else
            {
                return $this->start_date;
            }
        }
        else
        {
            return $this->start_date;
        }
    }
    
    public function getWorkedHoursInMonth(Month $month)
    {
        return $this->getWorkedHours($month->firstDay(), $month->lastDay());
    }
    
    public function getWorkedHours(Day $first_day, Day $last_day)
    {        
        $worked_hours = 0;
        
        $wc_start_date = new DateTime($this->start_date);
        $first_day_date = new DateTime($first_day->day_date);
        if($first_day_date > $wc_start_date)
        {
            $wc_start_date = $first_day_date;
        }
        $db_start_date = SIMAHtml::UserToDbDate($wc_start_date->format('d.m.Y'));
        
        $db_end_date = $this->getEndDate($last_day);
        
        $date_range = $db_start_date.'<>'.$db_end_date;
                
        $daysCriteria = new SIMADbCriteria([
            'Model' => 'Day',
            'model_filter' => [
                'day_date' => $date_range
            ]
        ]);
        $daysCriteria->order = 'day_date ASC';
        $dayModels = Day::model()->findAll($daysCriteria);
        
        foreach($dayModels as $dayModel)
        {
            $wc_work_hours = $dayModel->getWorkHours();
            $worked_hours += $wc_work_hours;
        }
              
        return $worked_hours;
    }
    
    public function getWorkExperience(Day $startdatelimit=null, Day $enddatelimit=null)
    {
        if(isset($enddatelimit))
        {
            $curDateTimeObj = new DateTime($enddatelimit->day_date);
        }
        else
        {
            $curDateTimeObj = new DateTime();
        }
        $final_end_date_str = $curDateTimeObj->format('d.m.Y');
        if(isset($this->end_date))
        {
            $end_dateObj = new DateTime($this->end_date);
            
            if($end_dateObj < $curDateTimeObj)
            {
                $final_end_date_str = $end_dateObj->format('d.m.Y');
            }
        }
        $wc_start_date = $this->getStartDate($startdatelimit);
        $wc_end_date = $this->getEndDate($enddatelimit);
        
        $cache_tag = 'WorkContract_getWorkExperience_'.$this->id.'_'.$wc_start_date.'_'.$wc_end_date;
        $worked_days = Yii::app()->simacache->get($cache_tag);

        if ($worked_days===FALSE)
        {
            
            $worked_days = WorkContractComponent::getWorkExpBetweenTwoDates($wc_start_date, $wc_end_date, $this->haveWorkContractInDayBeforeThisStart());
            
            $keywords = [[$this->employee, 'work_contracts']];
            Yii::app()->simacache->set($cache_tag, $worked_days, $keywords, true);
        }
        
        return $worked_days;
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters': return array_merge(parent::modelSettings('filters'), array(
                    'employee' => 'relation',
                    'employee_id' => 'dropdown',
                    'start_date' => 'date_range',
                    'end_date' => 'date_range',
                    'with_out_position' => array('func' => 'filter_with_out_position'),
                    'currently_active' => array('func' => 'filter_currently_active'),
                    'active_at' => array('func' => 'filter_active_at'),
                    'activeFromTo' => array('func' => 'filter_activeFromTo'),
                    'with_filing' => ['func' => 'filter_with_filing'],
                    'work_position' => 'relation',
                    'signed' => 'boolean',
                    'for_payout' => 'boolean',
                    'file' => 'relation',
                    'working_percent' => 'numeric',
                    'working_percent_at_other_companies' => 'numeric',
                    'employment_code' => 'dropdown',
                    'benefit' => 'dropdown',
                    'benefited_work_experience'=> 'dropdown',
                    'director_work_position' => 'relation',
                    'work_positions_to_occupations' => 'relation',
                    'safe_evd_3_work_contracts' => 'relation'
                ));
            case 'textSearch': return array(
                    'employee' => 'relation',
                    'file' => 'relation'
                );
            case 'options': 
                $options =  ['form', 'open', 'download'];
                if(!$this->signed)
                {
                    $options[] = 'delete'; 
                }
                return $options;
            case 'mutual_forms': return array(
                    'base_relation' => 'file',
                );
            case 'form_list_relations' : return array('work_positions');
            default: return parent::modelSettings($column);
        }
    }
        
    public function filter_currently_active($condition, $alias)
    {
        if(!is_null($this->currently_active))
        {
            $_currently_active = filter_var($this->currently_active, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);

            if ($_currently_active === true)
            {
                $curDay = Day::get();
                $temp_condition = new SIMADbCriteria([
                    'alias' => $alias,
                    'Model' => 'WorkContract',
                    'model_filter' => [
                        'active_at' => $curDay
                    ]
                ]);

                $condition->mergeWith($temp_condition, true);
                
            }
        }
    }
    
    public function filter_activeFromTo($condition, $alias)
    {
        if(isset($this->activeFromTo))
        {            
            $temp_condition = new SIMADbCriteria([
                'alias' => $alias,
                'Model' => 'WorkContract',
                'model_filter' => [
                    'scopes' => [
                        'activeFromTo' => $this->activeFromTo
                    ]
                ]
            ]);
            
            $condition->mergeWith($temp_condition, true);
        }
    }
    
    public function filter_active_at($condition, $alias)
    {        
        if(isset($this->active_at))
        {
            $active_at_type = gettype($this->active_at);
            if($active_at_type === 'string')
            {
                $dateRangeParam = $this->active_at;
            }
            else if($active_at_type === 'object' && get_class($this->active_at)==='Month')
            {
                $fist_month_day = $this->active_at->firstDay();
                $last_month_day = $this->active_at->lastDay();
                $period = $fist_month_day->day_date.'<>'.$last_month_day->day_date;
                
                $dateRangeParam = $period;
            }
            else if($active_at_type === 'object' && get_class($this->active_at)==='Day')
            {
                $fist_month_day = $this->active_at->day_date;
                $last_month_day = $this->active_at->day_date;
                $period = $fist_month_day.'<>'.$last_month_day;
                
                $dateRangeParam = $period;
            }
            else if($active_at_type==='array' && count($this->active_at)===2
                    && get_class($this->active_at[0])==='Month' && get_class($this->active_at[1])==='Month')
            {
                $fist_month_day = $this->active_at[0]->firstDay();
                $last_month_day = $this->active_at[1]->lastDay();
                $period = $fist_month_day->day_date.'<>'.$last_month_day->day_date;
                
                $dateRangeParam = $period;
            }
            else if($active_at_type==='array' && count($this->active_at)===2
                    && get_class($this->active_at[0])==='Day' && get_class($this->active_at[1])==='Day')
            {
                $fist_month_day = $this->active_at[0]->day_date;
                $last_month_day = $this->active_at[1]->day_date;
                $period = $fist_month_day.'<>'.$last_month_day;
                
                $dateRangeParam = $period;
            }
            else
            {
                throw new Exception('invalid param type: '.CJSON::encode($this->active_at));
            }
            
            $startDate = null;
            $endDate = null;

            $exploded = explode('<>', $dateRangeParam);
            if (count($exploded) == 1)
            {
                $dateRangeParamToDb = SIMAHtml::UserToDbDate(trim($dateRangeParam));

                $startDate = $dateRangeParamToDb;
                $endDate = $dateRangeParamToDb;
            }
            else if (count($exploded) == 2)
            {
                $startDate = SIMAHtml::UserToDbDate(trim($exploded[0]));
                $endDate = SIMAHtml::UserToDbDate(trim($exploded[1]));
            }
            else
            {
                throw new Exception(Yii::t('LegalModule.Employee', 'IllegalDateRangeParamFormat'));
            }
            
            $temp_condition = new SIMADbCriteria([
                'alias' => $alias,
                'Model' => 'WorkContract',
                'model_filter' => [
                    'scopes' => [
                        'activeFromTo' => [$startDate, $endDate]
                    ]
                ]
            ]);

            $condition->mergeWith($temp_condition, true);
        }
    }

    public function filter_with_out_position($condition)
    {
        if (isset($this->with_out_position) && $this->with_out_position)
        {
            $temp_condition = new CDbCriteria();
            $temp_condition->condition = "work_position_id is null";
            $condition->mergeWith($temp_condition, true);
        }
    }
    
    public function filter_with_filing($condition, $alias)
    {
        if (isset($this->with_filing) && $this->with_filing)
        {
            $filing_table = Filing::model()->tableName();
            
            $temp_condition = new CDbCriteria();
            $temp_condition->condition = "exists (SELECT 1 FROM $filing_table WHERE id=$alias.id LIMIT 1)";
            $condition->mergeWith($temp_condition, true);
        }
    }
    
    public function afterSave()
    {
        /** TRENUTNO SE PODRZAVA SAMO JEDNO RADNO MESTO PO OSOBI */
        if (strtotime($this->start_date) > strtotime(date('d.m.Y')))
        {
            SystemEvent::add(
                    'WorkContract', 'ReSave', date("d.m.Y. H:i:s", strtotime('+1 hour', strtotime($this->start_date))), array(
                'class' => 'WorkContract',
                'id' => $this->id
                    ), "Ubacivanje zaposlenog '" . $this->employee->person->DisplayName . "'u radni odnos pri startu ugovora", true
            );
        }
        
        $wcEndDate = null;
        if (isset($this->close_work_contract))
            $wcEndDate = $this->close_work_contract->end_date;
        elseif (!empty($this->end_date))
            $wcEndDate = $this->end_date;
        if (!empty($wcEndDate) && strtotime($wcEndDate) >= strtotime(date('d.m.Y')))
        {
            SystemEvent::add(
                    'WorkContract', 'ReSave', date("d.m.Y. H:i:s", strtotime('+1 day 1 hour', strtotime($wcEndDate))), array(
                'class' => 'WorkContract',
                'id' => $this->id
                    ), "Izbacivanje zaposlenog '" . $this->employee->person->DisplayName . "'iz radnog odnosa po isteku ugovora", true
            );
        }
        
        $employee = Employee::model()->findByPk($this->employee_id);
        if (!is_null($employee) && isset($employee->active_work_contract))
        {
            $this->employee->person->from_work_contract = true;
            $this->employee->person->company_id = Yii::app()->company->id;
            $this->employee->person->save();

            $new_work_position_id = $employee->active_work_contract->work_position_id;
            //brisu se sve prave pozicije
            $person_to_work_positions = PersonToWorkPosition::model()->deleteAllByAttributes(array('person_id' => $this->employee_id, 'fictive'=>false));

            $person_to_work_position = PersonToWorkPosition::model()->findByAttributes(array(
                'person_id' => $this->employee_id,
                'work_position_id' => $new_work_position_id
            ));            
            //ako ne postoji pozicija onda se kreira
            if (is_null($person_to_work_position))
            {
                $ptwp = new PersonToWorkPosition();
                $ptwp->person_id = $this->employee_id;
                $ptwp->work_position_id = $new_work_position_id;
                $ptwp->fictive = false;
                $ptwp->save();
            }
            //ako postoji fiktivna, onda se ona prebacuje u pravu
            else if ($person_to_work_position->fictive === true)
            {                
                $person_to_work_position->fictive = false;
                $person_to_work_position->save();
            }

//            $end_date = null;
//            if (isset($this->close_work_contract))
//                $end_date = $this->close_work_contract->end_date;
//            elseif ($this->end_date != null && $this->end_date != '')
//                $end_date = $this->end_date;
//            if ($this->end_date != null && $this->end_date != '')
//            if(!empty($end_date))
//            {
//                SystemEvent::add(
//                        'WorkContract', 'ReSave', date("d.m.Y. H:i:s", strtotime('+1 day 1 hour', strtotime($end_date))), array(
//                    'class' => 'WorkContract',
//                    'id' => $this->id
//                        ), "Izbacivanje zaposlenog '" . $this->employee->person->DisplayName . "'iz radnog odnosa po isteku ugovora", true
//                );
//            }
        }
        else        
        {
            $p2c = PersonToCompanies::model()->findAllByAttributes([
                'person_id' => $this->employee_id, 
                'company_id' => Yii::app()->company->id
            ]);
            if (count($p2c) > 0) //ukoliko je postojao u firmi, slobodno moze da se brise i radna pozicija
            {
                $this->employee->person->from_work_contract = true;
                $this->employee->person->company_id = null;
                $this->employee->person->save();
            }
            
            $p2wp = PersonToWorkPosition::model()->countByAttributes([
                'person_id' => $this->employee_id,
                'fictive' => false
            ]);
            if ($p2wp > 0)
            {
                $person_to_work_positions = PersonToWorkPosition::model()->deleteAllByAttributes([
                    'person_id' => $this->employee_id, 
                    'fictive'=>false
                ]);
            }
        }

        Yii::app()->warnManager->sendWarns('HRWorkContractTakeCare', 'WARN_WORK_CONTRACT_WITH_OUT_POSITION');
        Yii::app()->warnManager->sendWarns('HRWorkContractTakeCare', 'WARN_UNSIGNED_WORK_CONTRACTS');
        Yii::app()->warnManager->sendWarns('HRWorkContractTakeCare', 'WARN_NOT_RETURNED_WORK_CONTRACTS');
        
        //generise se dokument ugovor o radu
//        $this->generateWorkContractTemplate(); // zakomentarisano jer postoji dugme generisi

        if ($this->isNewRecord || $this->columnChanged('employee_id'))
        {
            if (!empty($this->__old['employee_id']))
            {
                $old_employee = Employee::model()->findByPk($this->__old['employee_id']);
                if (!empty($old_employee))
                {
                    $old_employee_work_contracts_cnt = $old_employee->work_contracts_cnt;
                    if (empty($old_employee->active_work_contract))
                    {
                        PartnerToPartnerList::remove(PartnerList::getSystemList('employees'), $old_employee->person->partner);
                        if ($old_employee_work_contracts_cnt > 0)
                        {
                            PartnerToPartnerList::add(PartnerList::getSystemList('former_employees'), $old_employee->person->partner);
                        }
                    }

                    if ($old_employee_work_contracts_cnt === 0)
                    {
                        PartnerToPartnerList::remove(PartnerList::getSystemList('former_employees'), $old_employee->person->partner);
                    }
                }
            }

            if (!empty($this->employee_id))
            {
                if (!empty($employee->active_work_contract))
                {
                    PartnerToPartnerList::remove(PartnerList::getSystemList('former_employees'), $employee->person->partner);
                    PartnerToPartnerList::add(PartnerList::getSystemList('employees'), $employee->person->partner);
                }
                else if ($employee->work_contracts_cnt > 0)
                {
                    PartnerToPartnerList::remove(PartnerList::getSystemList('employees'), $employee->person->partner);
                    PartnerToPartnerList::add(PartnerList::getSystemList('former_employees'), $employee->person->partner);
                }
            }
        }

        return parent::afterSave();
    }
    
    public function afterDelete()
    {
        parent::afterDelete();
        
        $employee_work_contracts_cnt = $this->employee->work_contracts_cnt;
        
        //ako zaposleni nema aktivan ugovor, uklanja se iz listu zaposlenih
        if (empty($this->employee->active_work_contract))
        {
            PartnerToPartnerList::remove(PartnerList::getSystemList('employees'), $this->employee->person->partner);
            if ($employee_work_contracts_cnt > 0)
            {
                PartnerToPartnerList::add(PartnerList::getSystemList('former_employees'), $this->employee->person->partner);
            }
        }

        //ako zaposleni nema nijedan ugovor, uklanja se iz listu bivsih zaposlenih
        if ($employee_work_contracts_cnt === 0)
        {
            PartnerToPartnerList::remove(PartnerList::getSystemList('former_employees'), $this->employee->person->partner);
        }
    }

    public function generateWorkContractTemplate()
    {
        if ($this->end_date === null) //ako je ugovor na neodredjeno
        {
            $template_id = Yii::app()->configManager->get('HR.indefinite_work_contract_template', false);
        }
        else //ako je ugovor na odredjeno
        {
            $template_id = Yii::app()->configManager->get('HR.fixed_work_contract_template', false);
        }
        if (!empty($template_id))
        {
            $templated_file = TemplatedFile::model()->with('templated_file_data')->findByPk($this->id);
            if ($templated_file === null)
            {
                $template_file_id = TemplateController::setTemplate($this->id, $template_id);
            }
            else
            {
                $template_file_id = $templated_file->id;
            }
            TemplateController::addVersion($template_file_id);
        }
    }

    public function getWorkContractsForDateRange($begin_date, $end_date)
    {
        $filter = WorkContract::filter(array(
                    'end_date' => $begin_date . '<>' . $end_date
        ));
        $criteria = new SIMADbCriteria();
        $criteria->applyModel($filter);

        $work_contracts = WorkContract::model()->findAll($criteria);

        return $work_contracts;
    }

    public function haveWorkContractInDayBeforeThisStart()
    {
        $result = false;
        
        $prevWc = $this->prev();
        
        if($this->have_work_contract_in_prev_day || isset($prevWc))
        {
            $result = true;
        }
        
        return $result;
    }
    
    public function isIndefinite()
    {
        $result = false;
        
        if($this->end_date == null || $this->end_date == '')
        {
            $result = true;
        }
        
        return $result;
    }
    
    public function generateIntroductionIntoBusinessProgram()
    {
        $introductionIntoBusinessProgram = new IntroductionIntoBusinessProgram();
        $introductionIntoBusinessProgram->year = (new DateTime($this->start_date))->format('Y');
        $introductionIntoBusinessProgram->save();
        $introductionIntoBusinessProgram->refresh();
        
        $this->introduction_into_business_program_id = $introductionIntoBusinessProgram->id;
        $this->save();
        
        $introductionIntoBusinessProgram->generateTemplate();
    }
    
    public function noWorkDaysSinceLastWorkContract()
    {
        $number_of_days = null;
        
        $prew_wc = $this->prev(false);
        if(isset($prew_wc))
        {
            $start_date = $prew_wc->getEndDate();
            $end_date = $this->start_date;
            
            $number_of_days = WorkContractComponent::getWorkExpBetweenTwoDates($start_date, $end_date) - 2;
        }
        
        return $number_of_days;
    }
    
    public function getDuration()
    {
        $result = null;
        
        if(!empty($this->end_date))
        {
            $result = WorkContractComponent::getWorkExpBetweenTwoDates($this->start_date, $this->getEndDate());
        }
        
        return $result;
    }
}
 
    WorkContract::$employment_codes = [
            '01' => Yii::t('HRModule.WorkContract', 'EmploymentCode01'),
            '02' => Yii::t('HRModule.WorkContract', 'EmploymentCode02'),
            '03' => Yii::t('HRModule.WorkContract', 'EmploymentCode03'),
            '04' => Yii::t('HRModule.WorkContract', 'EmploymentCode04'),
            '05' => Yii::t('HRModule.WorkContract', 'EmploymentCode05'),
            '06' => Yii::t('HRModule.WorkContract', 'EmploymentCode06'),
            '07' => Yii::t('HRModule.WorkContract', 'EmploymentCode07'),
            '08' => Yii::t('HRModule.WorkContract', 'EmploymentCode08'),
            '09' => Yii::t('HRModule.WorkContract', 'EmploymentCode09'),
            '10' => Yii::t('HRModule.WorkContract', 'EmploymentCode10'),
            '11' => Yii::t('HRModule.WorkContract', 'EmploymentCode11'),
            '12' => Yii::t('HRModule.WorkContract', 'EmploymentCode12'),
            '13' => Yii::t('HRModule.WorkContract', 'EmploymentCode13')
    ];
       
    WorkContract::$benefits = [
        '00' => Yii::t('HRModule.WorkContract', 'Benefit00'),
        '01' => Yii::t('HRModule.WorkContract', 'Benefit01'),
        '02' => Yii::t('HRModule.WorkContract', 'Benefit02'),
        '03' => Yii::t('HRModule.WorkContract', 'Benefit03'),
        '04' => Yii::t('HRModule.WorkContract', 'Benefit04'),
        '05' => Yii::t('HRModule.WorkContract', 'Benefit05'),
        '06' => Yii::t('HRModule.WorkContract', 'Benefit06'),
        '07' => Yii::t('HRModule.WorkContract', 'Benefit07'),
        '08' => Yii::t('HRModule.WorkContract', 'Benefit08'),
        '09' => Yii::t('HRModule.WorkContract', 'Benefit09'),
        '10' => Yii::t('HRModule.WorkContract', 'Benefit10'),
        '11' => Yii::t('HRModule.WorkContract', 'Benefit11'),
        '18' => Yii::t('HRModule.WorkContract', 'Benefit18'),
        '19' => Yii::t('HRModule.WorkContract', 'Benefit19'),
        '20' => Yii::t('HRModule.WorkContract', 'Benefit20'),
        '21' => Yii::t('HRModule.WorkContract', 'Benefit21')
    ];
        
    WorkContract::$benefited_work_experiences = [
            '0' => '0',
            '1' => '1',
            '2' => '2',
            '3' => '3',
            '4' => '4'
    ];
  
