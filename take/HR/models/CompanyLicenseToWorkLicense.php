<?php

/**
 * @package SIMA
 * @subpackage Legal
 */
class CompanyLicenseToWorkLicense extends SIMAActiveRecord
{   

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'hr.company_licenses_to_work_licenses';
    }

    public function moduleName()
    {
        return 'HR';
    }

    public function __get($column)
    {
        switch ($column)
        {
            default: return parent::__get($column);
        }
    }

    public function rules()
    {
        return array(
            array('company_license_id, work_license_id', 'required'),
            array('work_license_id, company_license_id', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => array('insert', 'update'))
        );
    }

    public function relations($child_relations = [])
    {
        return array(
            'company_license' => array(self::BELONGS_TO, 'CompanyLicense', 'company_license_id'),
            'work_license' => array(self::BELONGS_TO, 'WorkLicense', 'work_license_id')
        );
    }

    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters' : return array(
                'company_license' => 'relation',
                'work_license' => 'relation'
            );
            default : return parent::modelSettings($column);
        }
    }
    
}
