<?php


class SchoolCourse extends SIMAActiveRecord
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'hr.school_courses';
    }

    public function moduleName()
    {
        return 'HR';
    }

    public function __get($column)
    {
        switch ($column)
        {
            case 'DisplayName':
            case 'SearchName': 
                $ret = '';
                if (isset($this->school) && (!empty($this->school->short_name)))
                {
                    $ret = $this->school->short_name.' | ';
                }
                return $ret.$this->name;
            default: return parent::__get($column);
        }
    }

    public function rules()
    {
        return array(
            array('name, school_id', 'required'),
            array('description', 'safe')
        );
    }

    public function relations($child_relations = [])
    {
        return [
            'school' => [self::BELONGS_TO, 'School', 'school_id']
        ];
    }

    public function scopes()
    {
        $alias = $this->getTableAlias();
        return array(
            'byName' => array('order' => $alias . '.name'),
        );
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters' : return array(
                    'school' => 'relation',
                    'name' => 'text',
                    'description' => 'text'
                );
            case 'textSearch' : return array(
                    'school' => 'relation',
                    'name' => 'text',
                    'description' => 'text'
                );
            default: return parent::modelSettings($column);
        }
    }

}
