<?php

/**
 * Description of SafeEvd7
 *
 * @author goran
 */
class SafeEvd7 extends SIMAActiveRecord
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'hr.safe_evd_7';
    }

    public function moduleName()
    {
        return 'HR';
    }

    public function __get($column)
    {
        switch ($column)
        {
            default: return parent::__get($column);
        }
    }

    public function rules()
    {
        return array(
            array('work_position_id, name', 'required'),
            array('name, hemical_name, un_number, adr_number, rid_number, class_material, mode_use, quantity, comment, description', 'safe'),
            array('work_position_id', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => array('insert', 'update'))
        );
    }

    public function relations($child_relations = [])
    {
        return array(
            'work_position' => array(self::BELONGS_TO, 'WorkPosition', 'work_position_id'),
        );
    }

    public function scopes()
    {
        return array(
            'byName' => array('order' => 'id'),
        );
    }

    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters' : return [
                    'work_position' => 'relation',
                    'name' => 'text',
                    'hemical_name' => 'text',
                    'un_number' => 'text',
                    'adr_number' => 'text',
                    'rid_number' => 'text',
                    'class_material' => 'text',
                    'mode_use' => 'text',
                    'quantity' => 'text',
                    'comment' => 'text',
                    'description' => 'text'
                ];
            case 'options': return ['form','delete'];
            default: return parent::modelSettings($column);
        }
    }

}
