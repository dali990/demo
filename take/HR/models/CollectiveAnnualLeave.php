<?php

class CollectiveAnnualLeave extends SIMAActiveRecord
{
    public $_date_from = null;
    public $_date_to = null;
    
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'hr.collective_annual_leaves';
    }

    public function moduleName()
    {
        return 'HR';
    }
    
    public function __get($column)
    {
        switch ($column)
        {
            case 'DisplayName':
            case 'SearchName': return Yii::t('HRModule.Absence', 'CollectiveAnnualLeave').' - '.$this->sector.' - '.$this->from_day.'-'.$this->to_day;
            default: return parent::__get($column);
        }
    }
    
    public function rules()
    {
        return array(
            array('sector_id, _date_from, _date_to', 'required'),
            array('comment, confirmed', 'safe'),
            array('from_day_id', 'checkDates'),
            array('confirm', 'checkConfirm')
        );
    }
    
    public function checkDates()
    {        
        if(!$this->hasErrors())
        {
            if(strtotime($this->_date_from) > strtotime($this->_date_to))
            {
                $this->addError('from_day_id', Yii::t('HRModule.Absence', 'FromDayMustBeLessThanToDate'));
                $this->addError('to_day_id', Yii::t('HRModule.Absence', 'FromDayMustBeLessThanToDate'));
            }
        }
    }
    
    public function checkConfirm()
    {
        if($this->__old['confirmed']!=$this->confirmed && $this->confirmed)
        {
            $error_messages = [];
            $annual_leaves_cnt = 0;
            foreach($this->annual_leaves as $al)
            {
                if (!$al->confirmed)
                {
                    $al->confirmed = true; //ovo se ne cuva, samo setuje na true, da bi proverio da li moze
                    $error_msg = $al->validationErrorsToHTMLStr();
                    if($al->HaveIntersections === true)
                    {
                        $error_msg .= Yii::t('HRModule.Absence', 'ExistsAbsenceInInterval', [
                            '{emp}' => $al->employee->DisplayName
                        ]);
                    }

                    if(!empty($error_msg))
                    {
                        $error_messages[] = $error_msg;
                    }
                }
                $annual_leaves_cnt++;
            }
            
            if(count($error_messages) > 0)
            {                
                $this->addError('confirm', Yii::t('HRModule.Absence', 'CollectiveLeaveConfirmErrors', [
                    '{errors}' => implode('</br>', $error_messages)
                ]));
            }
            if($annual_leaves_cnt === 0)
            {
               $this->addError('confirm', Yii::t('HRModule.Absence', 'CantConfirmCollectiveLeaveWithoutAnnualLeaves'));
            }
        }
    }
    
    public function relations($child_relations = [])
    {
        return array(
            'from_day' => array(self::BELONGS_TO, 'Day', 'from_day_id'),
            'to_day' => array(self::BELONGS_TO, 'Day', 'to_day_id'),
            'sector' => array(self::BELONGS_TO, 'CompanySector', 'sector_id'),
            'employees'=>array(self::MANY_MANY, 'Employee', 'hr.empl_to_col_annual_leaves(collective_annual_leave_id,employees_id)'),
            'annual_leaves' => array(self::HAS_MANY, 'AbsenceAnnualLeave', 'collective_annual_leave_id'),
        );
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters' : return array(
                'from_day' => 'relation',
                'to_day' => 'relation'
            );
            case 'options': 
                $options = ['open'];
                if(!$this->confirmed)
                {
                    $options[] = 'delete';
                    $options[] = 'form';
                }
                return $options;
            case 'statuses':  return array(
                'confirmed' => array(
                    'title' => Yii::t('BaseModule.Common', 'Confirmed'),
                    'onConfirm' => 'onConfirm',
                    'onRevert' => 'onRevert',
                    'checkAccessRevert' => 'RevertConfirm',
                ),
            );
            default: return parent::modelSettings($column);
        }
    }
    
    public function afterFind()
    {
        $from_day = $this->from_day;
        if(isset($from_day))
        {
            $this->_date_from = $from_day->day_date;
        }
        $to_day = $this->to_day;
        if(isset($to_day))
        {
            $this->_date_to = $to_day->day_date;
        }
        
        return parent::afterFind();
    }
    
    public function beforeSave()
    {
        $day_from = Day::get($this->_date_from);
        $day_to = Day::get($this->_date_to);
        
        $this->from_day_id = $day_from->id;
        $this->to_day_id = $day_to->id;
            
        return parent::beforeSave();
    }
    
    public function afterSave()
    {
        parent::afterSave();
           
        if($this->from_day_id !== $this->__old['from_day_id']
                || $this->to_day_id !== $this->__old['to_day_id'])
        {
            Yii::app()->controller->raiseAction('sima.hr.collectiveLeaveAfterSaveResaveAnnualLeaves',[$this->id], true);
        }
    }
    
    public function beforeDelete()
    {
        if(!$this->confirmed)
        {
            foreach($this->annual_leaves as $al)
            {
                $al->delete();
            }
        }
        
        return parent::beforeDelete();
    }
    
    public function onConfirm()
    {
        if($this->confirmed)
        {
            $anual_leaves = $this->annual_leaves;
            foreach($anual_leaves as $al)
            {
                $al->confirmed = TRUE;
                $al->save();
            }
        }
    }
    
    public function onRevert()
    {
        if(!$this->confirmed)
        {
            $anual_leaves = $this->annual_leaves;
            foreach($anual_leaves as $al)
            {
                $al->confirmed = null;
                $al->save();
            }           
        }
    }
    
//    private function GetUnsavedAbsencesForEmployees()
//    {
//        $collectiveLeave = CollectiveAnnualLeave::model()->findByPk($this->id);
//        
//        $employees = $collectiveLeave->getEmployeeIdsActiveForThisLeave();
//                
//        $new_absences = [];
//        $error_messages = [];
//        
//        foreach($employees as $emp_id)
//        {
//            $absCriteria = new SIMADbCriteria([
//                'Model' => 'AbsenceAnnualLeave',
//                'model_filter' => [
//                    'employee' => [
//                        'ids' => $emp_id
//                    ],
//                    'collective_annual_leave' => [
//                        'ids' => $collectiveLeave->id
//                    ]
//                ]
//            ], true);
//                        
//            if(empty($absCriteria->ids))
//            {
//                $newAbsence = new AbsenceAnnualLeave();
//                $newAbsence->employee_id = $emp_id;
//                $newAbsence->begin = $collectiveLeave->from_day->day_date;
//                $newAbsence->end = $collectiveLeave->to_day->day_date;
//                $newAbsence->type = Absence::$ANNUAL_LEAVE;
//                $newAbsence->collective_annual_leave_id = $collectiveLeave->id;
//                $newAbsence->description = Yii::t('HRModule.Absence', 'CollectiveAnnualLeave');
//                
//                $error_msg = $newAbsence->validationErrorsToHTMLStr();
//                if(!empty($error_msg))
//                {
//                    $error_messages[] = $error_msg;
//                }
//                else
//                {
//                    $new_absences[] = $newAbsence;
//                }
//            }
//            else
//            {
//                $absence = AbsenceAnnualLeave::model()->find($absCriteria);
//                
//                $dirty_data = false;
//                
//                if($absence->begin !== $collectiveLeave->from_day->day_date)
//                {
//                    $absence->begin = $collectiveLeave->from_day->day_date;
//                    $dirty_data = true;
//                }
//                if($absence->end !== $collectiveLeave->to_day->day_date)
//                {
//                    $absence->end = $collectiveLeave->to_day->day_date;
//                    $dirty_data = true;
//                }
//                
//                if($collectiveLeave->confirmed && !$absence->confirmed)
//                {
//                    $absence->confirmed = true;
//                    $dirty_data = true;
//                }
//                                
//                if($dirty_data === true)
//                {                    
//                    $error_msg = $absence->validationErrorsToHTMLStr();
//                    if(!empty($error_msg))
//                    {                        
//                        $error_messages[] = $error_msg;
//                    }
//                    else
//                    {                        
//                        $new_absences[] = $absence;
//                    }
//                }
//            }
//        }
//        
//        if(count($error_messages) > 0)
//        {
//            Yii::app()->raiseNote(implode('</br>', $error_messages));
//        }
//        
//        return $new_absences;
//    }
    
    public function getEmployeeIdsActiveForThisLeave()
    {
        $sectorIds = SIMASQLFunctions::getCompanySectorChildrenIds($this->sector);
        $sectorIds[] = $this->sector->id;
        
        $workContractsCriteria = new SIMADbCriteria([
            'Model' => 'WorkContract',
            'model_filter' => [
                'signed' => true,
                'work_position' => [
                    'company_sector' => [
                        'ids' => $sectorIds
                    ]
                ],
                'active_at' => [$this->from_day, $this->to_day]
            ]
        ]);
        
        $workContracts = WorkContract::model()->findAll($workContractsCriteria);
        
        $employees = [];
        foreach($workContracts as $wc)
        {
            if(!isset($employees[$wc->employee->id]))
            {
                $employees[$wc->employee->id] = $wc->employee->id;
            }
        }
        
        return $employees;
    }
}
