<?php

class RequiredMedicalAbility extends SIMAActiveRecord
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'hr.required_medical_abilities';
    }

    public function moduleName()
    {
        return 'HR';
    }

    public function __get($column)
    {
        switch ($column)
        {
            case 'DisplayName':
            case 'SearchName': return $this->name;
            default: return parent::__get($column);
        }
    }

    public function rules()
    {
        return array(
            array('name', 'required'),
            array('description', 'safe')
        );
    }

    public function relations($child_relations = [])
    {
        return array();
    }

    public function scopes()
    {
        $alias = $this->getTableAlias();
        return array(
            'byName' => array('order' => $alias . '.name'),
        );
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters' : return array(
                    'name' => 'text',
                    'description' => 'text'
                );
            case 'textSearch' : return array(
                    'name' => 'text',
                    'description' => 'text'
                );
            default: return parent::modelSettings($column);
        }
    }

}
