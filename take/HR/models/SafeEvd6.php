<?php

/**
 * Description of SafeEvd6
 *
 * @author goran
 */
class SafeEvd6 extends SIMAActiveRecord
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'hr.safe_evd_6';
    }

    public function moduleName()
    {
        return 'HR';
    }

    public function __get($column)
    {
        switch ($column)
        {
            case 'path2': return 'files/file';
            case 'DisplayName': return $this->file->DisplayName;
            case 'SearchName': return $this->file->DisplayName;
            default: return parent::__get($column);
        }
    }

    public function rules()
    {
        return array(
            array('work_position_id, employee_id', 'required'),
            array('date_theoretical_training, date_practical_training, date_check_theoretical_training, '
                . 'date_check_practical_training, description', 'safe'),
            array('work_position_id, date_theoretical_training, date_practical_training,
                 date_check_theoretical_training, date_check_practical_training, employee_id', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => array('insert', 'update'))
        );
    }

    public function relations($child_relations = [])
    {
        $safe_evd6_to_reasons_for_enforcement_training = SafeEvd6ToReasonForEnforcementTraining::model()->tableName();
        return array(
            'employee' => array(self::BELONGS_TO, 'Employee', 'employee_id'),                
            'work_position' => array(self::BELONGS_TO, 'WorkPosition', 'work_position_id'),
            'file' => array(self::BELONGS_TO, 'File', 'id'),
            'reasons_for_enforcement_training' => array(self::MANY_MANY, 'ReasonForEnforcementTraining',
                "$safe_evd6_to_reasons_for_enforcement_training(safe_evd6_id,reason_for_enforcement_training_id)"),
        );
    }

    public function scopes()
    {
        return array(
            'byName' => array('order' => 'id'),
        );
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters' : return array(
                'employee' => 'relation',
                'work_position' => 'relation',                
                'date_theoretical_training' => 'date_range',
                'date_practical_training' => 'date_range',
                'date_check_theoretical_training' => 'date_range',
                'date_check_practical_training' => 'date_range',
                'description'=>'text',                              
            );
            case 'options': return ['form','download','delete','open'];
            case 'mutual_forms' : return array(
                'base_relation' => 'file',
            );
            case 'form_list_relations' : return array('reasons_for_enforcement_training');
            default : return parent::modelSettings($column);
        }
    }

    public function BeforeSave()
    {   
        if (empty($this->id) && empty($this->file))
        {
            $file = new File();
            $file->name = isset($this->employee)?'Osposobljen za rad - ' . $this->employee->DisplayName:'';
            $file->responsible_id = Yii::app()->user->id;
            $file->save();
            $file->refresh();
            $this->id = $file->id;
            $this->file = $file;            
        }        
        
        return parent::BeforeSave();
    }
    
    public function getTheoreticalTrainingsForDateRange($begin_date, $end_date)
    {        
        $criteria = new SIMADbCriteria([
            'Model'=>'SafeEvd6',
            'model_filter'=>[
                'date_check_theoretical_training' => $begin_date . '<>' . $end_date
            ]
        ]);        

        $theoretical_trainings = SafeEvd6::model()->findAll($criteria);

        return $theoretical_trainings;
    }
    
    public function getPracticalTrainingsForDateRange($begin_date, $end_date)
    {        
        $criteria = new SIMADbCriteria([
            'Model'=>'SafeEvd6',
            'model_filter'=>[
                'date_check_practical_training' => $begin_date . '<>' . $end_date
            ]
        ]);        

        $practical_trainings = SafeEvd6::model()->findAll($criteria);

        return $practical_trainings;
    }

}
