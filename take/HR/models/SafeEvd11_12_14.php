<?php

class SafeEvd11_12_14 extends SIMAActiveRecord
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'hr.safe_evd_11_12_14';
    }

    public function moduleName()
    {
        return 'HR';
    }

    public function __get($column)
    {
        switch ($column)
        {
            default: return parent::__get($column);
        }
    }

    public function rules()
    {
        return array(            
            array('safe_evd_3_id, safe_evd_4_id, safe_evd_5_id, date_request, date_request_writing,
                inspection_company_id, inspection_company_id_writing, 
                inspection_person_id, inspection_person_id_writing,oup_company_id, oup_company_id_writing,
                oup_person_id, oup_person_id_writing, comment, type_evd, description_appearance, description', 'safe'),
            array('date_request, date_request_writing, safe_evd_3_id, safe_evd_4_id, safe_evd_5_id, inspection_company_id, inspection_company_id_writing, inspection_person_id, inspection_person_id_writing,
                oup_company_id, oup_company_id_writing, oup_person_id, oup_person_id_writing', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => array('insert', 'update'))
        );
    }

    public function relations($child_relations = [])
    {
        return array(
            'safe_evd_3' => array(self::BELONGS_TO, 'SafeEvd3', 'safe_evd_3_id'),
            'safe_evd_4' => array(self::BELONGS_TO, 'SafeEvd4', 'safe_evd_4_id'),
            'safe_evd_5' => array(self::BELONGS_TO, 'SafeEvd5', 'safe_evd_5_id'),
            'inspection_company' => array(self::BELONGS_TO, 'Company', 'inspection_company_id'),
            'inspection_company_writing' => array(self::BELONGS_TO, 'Company', 'inspection_company_id_writing'),
            'inspection_person' => array(self::BELONGS_TO, 'Person', 'inspection_person_id'),
            'inspection_person_writing' => array(self::BELONGS_TO, 'Person', 'inspection_person_id_writing'),
            'oup_company' => array(self::BELONGS_TO, 'Company', 'oup_company_id'),
            'oup_company_writing' => array(self::BELONGS_TO, 'Company', 'oup_company_id_writing'),
            'oup_person' => array(self::BELONGS_TO, 'Person', 'oup_person_id'),
            'oup_person_writing' => array(self::BELONGS_TO, 'Person', 'oup_person_id_writing'),
        );
    }

    public function scopes()
    {
        return array(
            'byName' => array('order' => 'id'),
        );
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters' : return [
                'safe_evd_3' => 'relation',
                'safe_evd_4' => 'relation',
                'safe_evd_5' => 'relation',
                'inspection_company' => 'relation',
                'inspection_company_writing' => 'relation',
                'inspection_person' => 'relation',
                'inspection_person_writing' => 'relation',
                'oup_company' => 'relation',
                'oup_company_writing' => 'relation',
                'oup_person' => 'relation',
                'oup_person_writing' => 'relation',
                'date_request' => 'date_range',
                'date_request_writing' => 'date_range',
                'comment' => 'text',
                'description' => 'text'
            ];
            case 'options': return ['form', 'delete'];
            default: return parent::modelSettings($column);
        }
    }
}
