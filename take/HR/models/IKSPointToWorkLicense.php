<?php

/**
 * @package SIMA
 * @subpackage HR
 */
class IKSPointToWorkLicense extends SIMAActiveRecord
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'hr.iks_points_to_work_licenses';
    }

    public function moduleName()
    {
        return 'HR';
    }

    public function __get($column)
    {
        switch ($column)
        {
            case 'work_license_name': return $this->work_license->DisplayHtml;
            default: return parent::__get($column);
        }
    }
    
    public function rules()
    {
        return array(
            array('work_license_id, iks_point_id', 'required'),
            array('work_license_id, iks_point_id','default',
                'value'=>null,
                'setOnEmpty'=>true,'on'=>array('insert','update')),
        );
    }
    
    public function relations($child_relations = [])
    {
        return array(
            'work_license' => array(self::BELONGS_TO, 'WorkLicense', 'work_license_id'),
            'iks_point' => array(self::BELONGS_TO, 'IKSPoint', 'iks_point_id'),
        );
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters': return array(
                'iks_point'=>'relation'
            );
            case 'options': return array('delete');
            default: return parent::modelSettings($column);
        }
    }
}