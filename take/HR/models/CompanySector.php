<?php

/**
 * Description of CompanySector
 *
 * @author goran
 */
class CompanySector extends SIMAActiveRecord
{

    public $company_id;
    public $child_of_sector;
    public $fictive_people_position_count = 0;
    public $not_fictive_people_position_count = 0;

    public function tableName()
    {
        return 'hr.company_sectors';
    }

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function moduleName()
    {
        return 'HR';
    }

    public function __get($column)
    {
        switch ($column)
        {
            case 'DisplayName': 
                if (isset($this->parent) && !isset($this->parent->company))
                {
                    return $this->parent->DisplayName.' | '.$this->name;
                }
                else
                {
                    return $this->name;
                }
            case 'SearchName': return $this->code . ' ' . $this->type . ": " . $this->name;
            case 'fullName': return $this->getFullName();
            default: return parent::__get($column);
        }
    }

    public function rules()
    {
        return array(
            array('name, type, code, description', 'safe'),
            array('name, type, code', 'required'),
            ['parent_id', 'required', 'on' => ['insert', 'update']],
            ['parent_id', 'safe', 'on' => 'getHeadSector'],
            array('parent_id', 'default',
                'value' => null,
                'setOnEmpty' => true,
                'on' => array('insert', 'update'))
                ) + parent::rules();
    }

    public function relations($child_relations = [])
    {
        $alias = $this->getTableAlias();
        return array(
            'parent' => array(self::BELONGS_TO, 'CompanySector', 'parent_id'),
            'childrens' => array(self::HAS_MANY, 'CompanySector', 'parent_id', 'order' => 'childrens.code'),
            'work_positions' => array(self::HAS_MANY, 'WorkPosition', 'company_sector_id', 'order' => 'work_positions.code'),
            'director_work_positions'=>array(self::HAS_MANY, 'WorkPosition', 'company_sector_id',
                'condition'=>'is_director=true'),
            'not_director_work_positions'=>array(self::HAS_MANY, 'WorkPosition', 'company_sector_id',
                'condition'=>'is_director=false'),
            'company' => [self::HAS_ONE, 'Company', 'head_sector_id']
        );
    }

    public function scopes()
    {
        $alias = $this->getTableAlias();
        return array(
            'byName' => array('order' => 'name, id'),
            'withHeadSector' => array(
                'select' => "$alias.*, recursive_company_sectors.head_sector_id",
                'join' => "left join hr.recursive_company_sectors recursive_company_sectors on $alias.id=recursive_company_sectors.id
                ",
            ),
            'orderBySearchNameASC' => [
                'order' => "$alias.code, $alias.type, $alias.name"
            ]
        );
    }

    public function hasGrandChildrens()
    {
        $found = false;
        if ($this->childrens != null)
        {
            foreach ($this->childrens as $child)
            {
                if ($child->childrens != null)
                {
                    $found = true;
                }
            }
        }
        return $found;
    }

    public function modelSettings($column)
    {
        $options = ['open'];
        if (Yii::app()->user->checkAccess('CompanySchemaWrite'))
        {
            $have_form_option = true;
            if(!empty($this->id) && $this->isHeadSector())
            {
                $have_form_option = false;
            }
            if($have_form_option)
            {
                $options[] = 'form';
                $options[] = 'delete';
            }
        }
        switch ($column)
        {
            case 'filters' : return array_merge(parent::modelSettings($column),array(
                'company_id' => array('func' => 'filter_company_id'),
            ));
            case 'options':  return $options;
            case 'textSearch' : return array(
                'name' => 'text',
                'code' => 'text'
            );
            default: return parent::modelSettings($column);
        } 
    }

    public function filter_company_id($condition)
    {
        $alias = $this->getTableAlias();
        if (isset($this->company_id) && $this->company_id != null)
        {
            $company = Company::model()->findByPk($this->company_id);
            if ($company !== null && isset($company->head_sector_id))
                $uniq = SIMAHtml::uniqid();
            $temp_condition = new CDbCriteria();
            $temp_condition->condition = $alias . '.id in (select id from hr.recursive_company_sectors where head_sector_id=:head_sector' . $uniq . ')';
            $temp_condition->params = array(':head_sector' . $uniq => $company->head_sector_id);
            $condition->mergeWith($temp_condition, true);
        }
    }
    
    public function isDirector(Person $person)
    {
        $_is_director = false;
        foreach ($this->director_work_positions as $positions)
        {
            foreach ($positions->persons as $_person)
            {
                if ($_person->id === $person->id)
                {
                    $_is_director = true;
                    break 2;
                }
            }
        }
        return $_is_director;
    }
    
    public function getFullName()
    {
        if (isset($this->parent))
        {            
            return $this->type.': '.$this->name.', '.$this->parent->getFullName();
        }
        else
        {
            return $this->type.': '.$this->name;
        }
    }
    
    public function get_not_fictive_people_position_count()
    {
        $peoples_ids = [];
        foreach ($this->childrens as $children)
        {
            $_peoples_ids = $children->get_not_fictive_people_position_count();
            $peoples_ids = array_unique(array_merge($peoples_ids, $_peoples_ids));
        }
        
        foreach ($this->work_positions as $work_position) 
        {
            foreach ($work_position->not_fictive_position_to_persons as $not_fictive_position_to_person)
            {
                if (!in_array($not_fictive_position_to_person->person_id, $peoples_ids))
                {
                    array_push($peoples_ids, $not_fictive_position_to_person->person_id);
                }                
            }
        }

        return $peoples_ids;
    }
    
    public function get_fictive_people_position_count()
    {
        $peoples_ids = [];
        foreach ($this->childrens as $children)
        {
            $_peoples_ids = $children->get_fictive_people_position_count();
            $peoples_ids = array_unique(array_merge($peoples_ids, $_peoples_ids));
        }
        
        foreach ($this->work_positions as $work_position) 
        {
            foreach ($work_position->fictive_position_to_persons as $fictive_position_to_person)
            {
                if (!in_array($fictive_position_to_person->person_id, $peoples_ids))
                {
                    array_push($peoples_ids, $fictive_position_to_person->person_id);
                }                
            }
        }

        return $peoples_ids;
    }
    
    public function isHeadSector()
    {
        $count = Company::model()->count([
            'model_filter' => [
                'head_sector' => [
                    'ids' => $this->id
                ]
            ]
        ]);
        
        return $count > 0;
    }
}
