<?php

/**
 * @package SIMA
 * @subpackage Legal
 */
class CompanyLicenseToCompany extends SIMAActiveRecord
{   

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'hr.company_licenses_to_companies';
    }

    public function moduleName()
    {
        return 'HR';
    }

    public function __get($column)
    {
        switch ($column)
        {
            case 'DisplayName':
                $return = '';
                if (isset($this->company_license) && isset($this->company))
                {
                    $return = $this->company_license->DisplayName.'('.$this->company->DisplayName.')';
                }
                return $return;
            case 'SearchName': return $this->DisplayName;
            default: return parent::__get($column);
        }
    }

    public function rules()
    {
        return array(
            array('company_license_id, company_id', 'required'),
            array('is_active', 'safe'),
            array('company_license_id', 'checkUniq'),
            array('company_license_id, company_id', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => array('insert', 'update'))
        );
    }

    function checkUniq()
    {
        if (
                intval($this->__old['company_license_id']) !== intval($this->company_license_id) || 
                intval($this->__old['company_id']) !== intval($this->company_id)
            )
        {
            $company_license_to_company = CompanyLicenseToCompany::model()->findByAttributes([
                'company_license_id'=>$this->company_license_id,
                'company_id'=>$this->company_id
            ]);
            if (!is_null($company_license_to_company))
            {
                $this->addError('company_license_id', Yii::t('HRModule.CompanyLicense', 'CompanyLicenseAlreadyExist',[
                    '{license_name}'=>$this->company_license->DisplayName,
                    '{company_name}'=>$this->company->DisplayName
                ]));
            }
        }
    }
    
    public function relations($child_relations = [])
    {
        return parent::relations(array_merge(array(
            'company_license' => array(self::BELONGS_TO, 'CompanyLicense', 'company_license_id'),
            'company' => array(self::BELONGS_TO, 'Company', 'company_id'),
            'partners'=>array(self::MANY_MANY, 'Partner', 'hr.company_licenses_to_partners(company_license_id, partner_id)','scopes'=>'byName'),
            'persons'=>array(self::MANY_MANY, 'Person', 'hr.company_licenses_to_partners(company_license_id, partner_id)'),
            'references'=>array(self::MANY_MANY, 'Reference', 'hr.company_licenses_to_jobs_references(company_license_id, reference_id)'),
            'references_cnt'=>array(self::STAT, 'Reference', 'hr.company_licenses_to_jobs_references(company_license_id, reference_id)', 'select' => 'count(*)>0'),
            'is_active_submitter' => [self::BELONGS_TO, 'User', 'is_active_submitter_id']
        ), $child_relations));
    }

    public function scopes()
    {
//        $uniq = SIMAHtml::uniqid();
        $alias = $this->getTableAlias();        
        $company_licenses_to_partners_table_name = CompanyLicenseToPartner::model()->tableName();
        $company_licenses_table_name = CompanyLicense::model()->tableName();
        return array(            
            'notConnected' => array(
                'join'=>"join $company_licenses_table_name cl on cl.id=$alias.company_license_id",
                'condition'=>"cl.persons_number!=(select count(*) from $company_licenses_to_partners_table_name clp where clp.company_license_id=$alias.id)"
            )
        );
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters' : return array(
                'company_license' => 'relation',
                'company' => 'relation'
            );
            case 'statuses':  return [
                'is_active' => [
                    'title' => Yii::t('HRModule.CompanyLicense', 'CompanyLicenseActive'),
                    'timestamp' => 'is_active_timestamp',
                    'user' => array('is_active_submitter_id','relName'=>'is_active_submitter'),
                    'checkAccessConfirm' => 'Confirm',
                    'checkAccessRevert' => 'Revert'
                ]
            ];
            default : return parent::modelSettings($column);
        }
    }
    
    public function afterSave()
    {
        parent::afterSave();
        $this->sendNotifOnChange();
    }
    
    public function afterDelete()
    {
        parent::afterDelete();
        $this->sendNotifOnChange(true);
    }
    
    private function sendNotifOnChange($is_delete=false)
    {
        $company = Yii::app()->company;        
        if (intval($this->company_id) === intval($company->id))
        {
            $action_name = 'Dodata';
            if ($is_delete === true)
            {
                $action_name = 'Skinuta';
            }
            $company_license_name = $this->company_license->DisplayName;
            $title = "$action_name je velika licenca sa nazivom $company_license_name kompaniji $company->DisplayName";
            $receiver_id = Yii::app()->configManager->get('HR.company_license_to_company_change_receiver_id', false);            
            if (!empty($receiver_id) && intval($receiver_id) !== Yii::app()->user->id)
            {                    
                Yii::app()->notifManager->sendNotif($receiver_id, $title, [
                    'info' => $title,
                    'action' => array(
                        'action' => 'HR/WorkLicense/companyLicenseByCompanies'                    
                    )                        
                ]);
            }
        }
    }
    
    public function getReferencesIds()
    {
        $references_ids = [];
        foreach ($this->references as $reference)
        {
            array_push($references_ids, $reference->id);
        }
        
        return $references_ids;
    }
    
    public function getReferencesForPartner($partner)
    {
        $references_ids = [];
        //ako je partner kompanija onda se drugacije racunaju reference
        if (isset($partner->company_model))
        {
            $references_ids = $this->getReferencesIds();
        }
        //inace je osoba
        else if (isset ($partner->person))
        {
            $references_ids = $partner->person->getReferencesIds();
        }
        
        return $references_ids;
    }
}
