<?php

class AbsenceSickLeave extends Absence
{
    public static $SUBTYPE_SICK_LEAVE_INJURY_AT_WORK = 'SICK_LEAVE_INJURY_AT_WORK';
    public static $SUBTYPE_SICK_LEAVE_PREGNANCY_TILL_30 = 'SICK_LEAVE_PREGNANCY_TILL_30';
    public static $SUBTYPE_SICK_LEAVE_PREGNANCY_OVER_30 = 'SICK_LEAVE_PREGNANCY_OVER_30';
    public static $SUBTYPE_SICK_LEAVE_MATERNITY = 'SICK_LEAVE_MATERNITY';
    public static $SUBTYPE_SICK_LEAVE_CHILD_CARE_TILL_3 = 'SICK_LEAVE_CHILD_CARE_TILL_3';
    public static $SUBTYPE_SICK_LEAVE_TILL_30 = 'SICK_LEAVE_TILL_30';
    public static $SUBTYPE_SICK_LEAVE_OVER_30 = 'SICK_LEAVE_OVER_30';
    
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    
    public function rules()
    {
        return array_merge(parent::rules(), [
            array('subtype', 'required')
        ]);
    }
    
    public function simaActivity_checkConfirmDate()
    {
        if(!empty($this->begin)
                && !empty($this->employee)
                && !empty($this->employee->user))
        {
            $begin_day = Day::getByDate($this->begin);
            $last_boss_confirm_day = $this->employee->user->last_boss_confirm_day;

            if(!empty($last_boss_confirm_day)
                    && ($begin_day->compare($last_boss_confirm_day) <= 0))
            {
                $this->addError('begin', Yii::t('SIMAActivityManager.Activity', 'CannotCreateActivityInBossConfirmedDay'));
                $this->addError('end', Yii::t('SIMAActivityManager.Activity', 'CannotCreateActivityInBossConfirmedDay'));
            }
        }
    }
    
    public function simaActivity_checkConfirm()
    {
        if(!empty($this->begin)
                && !empty($this->employee)
                && !empty($this->employee->user))
        {
            $begin_day = Day::getByDate($this->begin);
            $last_pay_day = PaycheckPeriodComponent::GetLastPayOutDayInFinalPeriod();

            if(!empty($last_pay_day)
                    && ($begin_day->compare($last_pay_day) <= 0))
            {
                $this->addError('begin', Yii::t('SIMAActivityManager.Activity', 'CannotConfirmActivityInPayConfirmedDay'));
                $this->addError('end', Yii::t('SIMAActivityManager.Activity', 'CannotConfirmActivityInPayConfirmedDay'));
            }
        }
    }

    public function defaultScope()
    {
        $alias = $this->getTableAlias(FALSE, FALSE);
        $type = self::$SICK_LEAVE;
        return [
            'condition' => "$alias.type='$type'"
        ];
    }
    
    public function __get($column)
    {
        switch ($column)
        {                 
            case 'DisplayName': return "Bolovanje za ".$this->employee->DisplayName.' od '.$this->begin.' do '.$this->end;
            case 'SearchName': return $this->DisplayName;
            default: return parent::__get($column);
        }
    }
    
    public function relations($child_relations = [])
    {
        return array(
            'absence' => array(self::HAS_ONE, 'Absence', 'id')
        )+parent::relations();
    }

    public function beforeSave()
    {
        $this->type = self::$SICK_LEAVE;
        
        return parent::beforeSave();
    }
    
    public function afterSave()
    {
        parent::afterSave();
        
        Yii::app()->warnManager->sendWarns('LegalShowWarnForNotConfirmedEmployeesSickLeavesAndFreeDays', 'WARN_NOT_CONFIRMED_EMPLOYEES_SICK_LEAVES_AND_FREE_DAYS');
    }
    
    public function afterDelete() 
    {
        parent::afterDelete();
        
        Yii::app()->warnManager->sendWarns('LegalShowWarnForNotConfirmedEmployeesSickLeavesAndFreeDays', 'WARN_NOT_CONFIRMED_EMPLOYEES_SICK_LEAVES_AND_FREE_DAYS');
    }
    
    public static function getSubTypeData()
    {
        return [
            AbsenceSickLeave::$SUBTYPE_SICK_LEAVE_INJURY_AT_WORK => Yii::t('HRModule.Absence', 'SICK_LEAVE_INJURY_AT_WORK'),
            AbsenceSickLeave::$SUBTYPE_SICK_LEAVE_PREGNANCY_TILL_30 => Yii::t('HRModule.Absence', 'SICK_LEAVE_PREGNANCY_TILL_30'),
            AbsenceSickLeave::$SUBTYPE_SICK_LEAVE_PREGNANCY_OVER_30 => Yii::t('HRModule.Absence', 'SICK_LEAVE_PREGNANCY_OVER_30'),
            AbsenceSickLeave::$SUBTYPE_SICK_LEAVE_MATERNITY => Yii::t('HRModule.Absence', 'SICK_LEAVE_MATERNITY'),
            AbsenceSickLeave::$SUBTYPE_SICK_LEAVE_CHILD_CARE_TILL_3 => Yii::t('HRModule.Absence', 'SICK_LEAVE_CHILD_CARE_TILL_3'),
            AbsenceSickLeave::$SUBTYPE_SICK_LEAVE_TILL_30 => Yii::t('HRModule.Absence', 'SICK_LEAVE_TILL_30'),
            AbsenceSickLeave::$SUBTYPE_SICK_LEAVE_OVER_30 => Yii::t('HRModule.Absence', 'SICK_LEAVE_OVER_30')
        ];
    }
}
