<?php
/**
 * @author sasa
 */
class HRYear extends SIMAActiveRecord
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
 
    public function tableName()
    {
       return 'hr.years';
    }
    
    public function moduleName() 
    {
        return 'HR';
    }
    
    public function rules()
    {
        return array(
            array('year, must_be_used_until, number_of_annual_leave_days', 'required'),            
            array('year', 'numerical', 'integerOnly' => TRUE,'max' => 2100,'min' => 1900),
            array('year', 'unique')
        );
    }
    
    public function relations($child_relations = [])
    {
        return array(
            'base_year' => array(self::BELONGS_TO, 'Year', 'id')
        );
    }
    
    public function __get($column)
    {
        switch ($column)
        {
          
            case 'DisplayName': return $this->year;
            case 'SearchName': return $this->year;
            default: return parent::__get($column);
        }
    }
    
    public function scopes()
    {
        return array(
            'byName' => array('order' => 'year DESC'),
        );
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'options': return ['form'];
            case 'filters' : return [
                'year' => 'integer',
                'base_year' => 'relation',
                'must_be_used_until' => 'date_range',
                'number_of_annual_leave_days'=>'integer'
            ];
            default: return parent::modelSettings($column);
        }
    }
    
    public function beforeSave() 
    {
        if ($this->getIsNewRecord())
        {
            $this->id = Year::get($this->year)->id;
        }
        return parent::beforeSave();
    }
}
    