<?php


class School extends SIMAActiveRecord
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'hr.schools';
    }

    public function moduleName()
    {
        return 'HR';
    }

    public function __get($column)
    {
        switch ($column)
        {
            case 'DisplayName':
            case 'SearchName': return isset($this->company)?$this->company->DisplayName:'';
            default: return parent::__get($column);
        }
    }

    public function rules()
    {
        return array(
            array('id', 'required'),
            array('id', 'unique', 'message'=>isset($this->company) ? "Već postoji škola {$this->company->DisplayName}" : ''),
            array('short_name', 'safe')
        );
    }

    public function relations($child_relations = [])
    {
        return [
            'company' => [self::BELONGS_TO, 'Company', 'id']
        ];
    }

//    public function scopes()
//    {
//        $alias = $this->getTableAlias();
//        return array(
//            'byName' => array('order' => $alias . '.name'),
//        );
//    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters' : return array(
                    'short_name' => 'text',
                    'company' => 'relation'
                );
            case 'textSearch' : return array(
                    'short_name' => 'text',
                    'company' => 'relation'
                );
            default: return parent::modelSettings($column);
        }
    }

}
