<?php

class CompanyLicenseDecision extends SIMAActiveRecord
{
    public $automatically_fill_with_the_current_state = false;
    
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
 
    public function tableName()
    {
       return 'hr.company_license_decisions';
    }
    
    public function moduleName() 
    {
        return 'HR';
    }
    
    public function __get($column)
    {
        switch ($column)
        {
            case 'path2': return 'files/file';
            case 'DisplayName':
                $return = '';
                if (isset($this->file))
                {
                    $return = $this->file->DisplayName;
                }
                $return .= '('.$this->decision_date.')';
                return $return;
            case 'SearchName': return $this->DisplayName;
            default: return parent::__get($column);
        }
    }
    
    public function rules()
    {
        return array(
            array('decision_date', 'required'),
            ['automatically_fill_with_the_current_state', 'safe']
        );
    }
    
    public function relations($child_relations = [])
    {
        $company_license_decisions_to_company_licenses_table_name = CompanyLicenseDecisionToCompanyLicense::model()->tableName();
        return array(
            'file' => array(self::BELONGS_TO, 'File', 'id'),
            'company_licenses'=>array(self::MANY_MANY, 'CompanyLicense', "$company_license_decisions_to_company_licenses_table_name(company_license_decision_id,company_license_id)")
        );
    }    
    
    public function modelSettings($column)
    {   
        switch ($column)
        {
            case 'filters': return array(
                'file' => 'relation',
                'decision_date' => 'date_range'
            );            
            case 'options': return ['form', 'open', 'download'];
            case 'mutual_forms' : return array(
                'base_relation' => 'file'
            );
            case 'form_list_relations' : return array('company_licenses');
            default: return parent::modelSettings($column);
        }
    }
    
    public function afterSave() 
    {
        parent::afterSave();
        
        if ($this->isNewRecord)
        {
            if ($this->automatically_fill_with_the_current_state == true)
            {
                $company = Yii::app()->company;
                $company_licenses = CompanyLicenseToCompany::model()->findAllByAttributes([
                    'company_id'=>$company->id,
                    'is_active'=>true
                ]);
                foreach ($company_licenses as $company_license)
                {
                    CompanyLicenseDecisionToCompanyLicense::add($this->id, $company_license->company_license->id);
                }
            }
        }
    }
    
    public static function getLastDecision()
    {
//        $last_decision = CompanyLicenseDecision::model()->find(new SIMADbCriteria([
//            'Model'=>'CompanyLicenseDecision',
//            'model_filter'=>[
//                'order_by'=>"decision_date DESC"
//            ]
//        ]));
        $last_decision = CompanyLicenseDecision::model()->find([
            'order'=>"decision_date DESC"
        ]);
        
        return $last_decision;
    }
    
    public static function getCompanyLicenseDifferencesByDecision($company_license_decision=null)
    {
        //ako resenje nije zadato, onda se uzima poslednje
        if (empty($company_license_decision))
        {
            $company_license_decision = CompanyLicenseDecision::getLastDecision();;
        }
        
        $differences = [];
        $company = Yii::app()->company;
        
        //ako ne postoji resenje onda se brisu sve velike licence
        if (is_null($company_license_decision))
        {
            $company_licenses_to_company = CompanyLicenseToCompany::model()->findAllByAttributes([            
                'company_id'=>$company->id,
                'is_active'=>true
            ]);
            foreach ($company_licenses_to_company as $company_license_to_company)
            {
                array_push($differences, [
                    'operation'=>'REMOVE',
                    'company_license'=>$company_license_to_company->company_license,
                    'company'=>$company
                ]);
            }
        }
        else
        {
            //trazimo razliku za sve licence koje se nalaze na resenju ali ih nema u sistemu
            foreach ($company_license_decision->company_licenses as $company_license)
            {
                $company_license_to_company = CompanyLicenseToCompany::model()->findByAttributes([
                    'company_license_id'=>$company_license->id,
                    'company_id'=>$company->id,
                    'is_active'=>true
                ]);
                if (is_null($company_license_to_company))
                {
                    array_push($differences, [
                        'operation'=>'ADD',
                        'company_license'=>$company_license,
                        'company'=>$company
                    ]);
                }
            }
        
            //trazimo razliku za sve licence koje se nalaze u sistemu ali ih nema na resenju
            $company_licenses_to_company = CompanyLicenseToCompany::model()->findAllByAttributes([            
                'company_id'=>$company->id,
                'is_active'=>true
            ]);
            foreach ($company_licenses_to_company as $company_license_to_company)
            {
                if (!CompanyLicenseDecision::isDecisionHaveCompanyLicense($company_license_decision->id, $company_license_to_company->company_license->id))
                {
                    array_push($differences, [
                    'operation'=>'REMOVE',
                    'company_license'=>$company_license_to_company->company_license,
                    'company'=>$company
                ]);
                }
            }
        }
        
        return $differences;
    }
    
    public static function isDecisionHaveCompanyLicense($decision_id, $company_license_id)
    {
        $founded = false;
        $decision = CompanyLicenseDecision::model()->findByPk($decision_id);
        foreach ($decision->company_licenses as $company_license)
        {
            if (intval($company_license->id) === intval($company_license_id))
            {
                $founded = true;
            }
        }
        
        return $founded;
    }
}
