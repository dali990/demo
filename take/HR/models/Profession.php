<?php

class Profession extends SIMAActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'hr.professions';
    }

    public function moduleName()
    {
        return 'HR';
    }

    public function __get($column)
    {
        switch ($column)
        {
            case 'DisplayName': return $this->name;
            case 'SearchName': return $this->name;
            default: return parent::__get($column);
        }
    }

    public function rules()
    {
        return array(
            array('name', 'required')
        );
    }

    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters': return array_merge(parent::modelSettings($column),array(
                    'name' => 'text'
                ));
            case 'textSearch': return array(
                    'name' => 'text'
                );
            default: return parent::modelSettings($column);
        }
    }

}
