<?php

/**
 * @package SIMA
 * @subpackage Legal
 */
class CompanyLicenseToPartner extends SIMAActiveRecord
{   

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'hr.company_licenses_to_partners';
    }

    public function moduleName()
    {
        return 'HR';
    }

    public function __get($column)
    {
        switch ($column)
        {
            case 'DisplayName':                
                $company_license = isset($this->company_license)?$this->company_license->DisplayName:'';
                $partner = isset($this->partner)?$this->partner->DisplayName:'';
                return $company_license.'('.$partner.')';
            case 'SearchName': return $this->DisplayName;
            default: return parent::__get($column);
        }
    }

    public function rules()
    {
        return array(
            array('partner_id, company_license_id', 'required'),
            array('accepted_references_number', 'safe'),
            array('partner_id, company_license_id', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => array('insert', 'update'))
        );
    }

    public function relations($child_relations = [])
    {
        return array(
            'partner' => array(self::BELONGS_TO, 'Partner', 'partner_id'),
            'company_license' => array(self::BELONGS_TO, 'CompanyLicenseToCompany', 'company_license_id'),
        );
    }

    public function scopes()
    {
        $uniq = SIMAHtml::uniqid();
        $alias = $this->getTableAlias();        
        $persons_table_name = Person::model()->tableName();
        $companies_table_name = Company::model()->tableName();
        return array(
            'onlyPersons' => array(
                'condition'=>"EXISTS (SELECT 1 FROM $persons_table_name as p$uniq WHERE $alias.partner_id = p$uniq.id)"
            ),
            'onlyCompanies' => array(
                'condition'=>"EXISTS (SELECT 1 FROM $companies_table_name as c$uniq WHERE $alias.partner_id = c$uniq.id)"
            )
        );
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters' : return array(
                'partner' => 'relation',
                'company_license' => 'relation',
                'accepted_references_number' => 'text'
            );
            case 'options' : return array();
            default : return parent::modelSettings($column);
        }
    }
    
    public static function getModelByPartnerAndCompanyLicense($partner, $company_license)
    {
        $company_license_to_partner = CompanyLicenseToPartner::model()->findByAttributes([
            'partner_id'=>$partner->id,
            'company_license_id'=>$company_license->id
        ]);
        
        return $company_license_to_partner;
    }
    
}
