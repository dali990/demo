<?php

class NationalEventSlavaToDay extends NationalEventToDayYear
{
    public $for_employee;
    
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    
    public function defaultScope()
    {
        $uniq = SIMAHtml::uniqid();
        $alias = $this->getTableAlias(FALSE, FALSE);
        $nationalEventsTable = NationalEvent::model()->tableName();
        $type = NationalEvent::$TYPE_SLAVA;
        
        return array(
            'condition' => "EXISTS ("
                . "SELECT 1 "
                . "FROM $nationalEventsTable net$uniq "
                . "WHERE $alias.national_event_id=net$uniq.id AND type='$type' "
                . "LIMIT 1"
            . ")",
        );
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters' : return parent::modelSettings($column)+[
                'for_employee' => ['func' => 'filter_for_employee']
            ];
            default: return parent::modelSettings($column);
        }
    }
    
    public function filter_for_employee($condition, $alias)
    {
        if(!empty($this->for_employee))
        {
            $employee_id = $this->for_employee['ids'];
            $employeeTable = Employee::model()->tableName();
            
            $temp_condition = new CDbCriteria();
            $temp_condition->condition = 'EXISTS (SELECT 1 FROM '.$employeeTable.' et WHERE et.national_event_slava_id='.$alias.'.national_event_id AND et.id='.$employee_id.' LIMIT 1)';

            $condition->mergeWith($temp_condition, true);
        }
    }
}

