<?php

/**
 * Description of SafeEvd14
 *
 * @author goran
 */
class SafeEvd14 extends SafeEvd11_12_14
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function BeforeSave()
    {
        $this->type_evd = '14';

        return parent::BeforeSave();
    }

    public function defaultScope()
    {
        return array(
            'condition' => 'type_evd=14',
        );
    }

}
