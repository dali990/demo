<?php

class RequiredMedicalAbilityToWorkPosition extends SIMAActiveRecord
{   

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'hr.required_medical_abilities_to_work_positions';
    }

    public function moduleName()
    {
        return 'HR';
    }

    public function rules()
    {
        return array(
            array('required_medical_ability_id, work_position_id', 'required'),
            array('required_medical_ability_id, work_position_id', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => array('insert', 'update'))
        );
    }

    public function relations($child_relations = [])
    {
        return array(
            'required_medical_ability' => array(self::BELONGS_TO, 'RequiredMedicalAbility', 'required_medical_ability_id'),
            'work_position' => array(self::BELONGS_TO, 'WorkPosition', 'work_position_id'),
        );
    }

    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters' : return array(
                'required_medical_ability' => 'relation',
                'work_position' => 'relation'
            );
            case 'options' : return array();
            case 'update_relations':  return ['work_position'];
            default : return parent::modelSettings($column);
        }
    }
    
}
