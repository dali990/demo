<?php

/**
 * @package SIMA
 * @subpackage HR
 */
class EmployeeCandidateToWorkPositionCompetition extends SIMAActiveRecord
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'hr.employee_candidate_to_work_position_competition';
    }

    public function moduleName()
    {
        return 'HR';
    }

//    public function __get($column)
//    {
//        switch ($column)
//        {
//            default: return parent::__get($column);
//        }
//    }
    
    public function see_all_grades($user_id = null)
    {
        if (is_null($user_id))
        {
            $user_id = Yii::app()->user->id;
        }
        $jury = WorkPositionCompetitionJury::model()->findByAttributes([
            'work_position_competition_id' => $this->work_position_competition_id,
            'jury_id' => Yii::app()->user->id
        ]);
        $is_jury = isset($jury); 

        $is_graded = $this->work_position_competition->is_graded;

        return ($is_jury && $is_graded) || (!$is_jury && Yii::app()->user->checkAccess('HRPositionCompetitionSeeAll'));
    }   
    
    public function relations($child_relations = [])
    {
        return array(
            'employee_candidate' => array(self::BELONGS_TO, 'EmployeeCandidate', 'employee_candidate_id'),
            'work_position_competition' => array(self::BELONGS_TO, 'WorkPositionCompetition', 'work_position_competition_id'),
            'competition_grades' => array(self::HAS_MANY, 'CompetitionGrade', 'employee_candidate_to_work_position_competition_id'),
            'avarage_cv_grade' => array(self::STAT, 'CompetitionGrade', 'employee_candidate_to_work_position_competition_id',
                'select' => 'avg(cv_grade)'
            ),
            'avarage_interview_grade' => array(self::STAT, 'CompetitionGrade', 'employee_candidate_to_work_position_competition_id',
                'select' => 'avg(interview_grade)'
            ),
//            'avarage_interview_grade' => array(self::BELONGS_TO, 'WorkPositionCompetition', 'work_position_competition_id'),
//            'avarage_cv_grade' => array(self::BELONGS_TO, 'WorkPositionCompetition', 'work_position_competition_id'),
        );
    }

    public function rules()
    {
        return array(
//            array('amount_neto, amount_bruto, amount_total', 'numerical', 'message' => 'Polje "{attribute}" mora biti broj!'),
//            array('status_id, interview_time', 'safe'),
            array('status, interview_time', 'safe'),
//            array('paycheck_period_id', 'default',
//                'value' => null,
//                'setOnEmpty' => true, 'on' => array('insert', 'update'))
        );
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters': return [
                'employee_candidate' => 'relation',
                'work_position_competition' => 'relation',
                'status' => 'dropdown',
                'competition_grades' => 'relation'
            ];
            case 'textSearch': return [
//                'employee_candidate' => 'relation', //TODO: beskonacan loop
                'work_position_competition' => 'relation',
            ];
            case 'number_fields': return [
                'avarage_cv_grade',
                'avarage_interview_grade'
            ];
            case 'options': return ['form'];
            default: return parent::modelSettings($column);
        }
    }
    
//    public function afterSave()
//    {
//        parent::afterSave();
//        //prebaceno u trigger
////        DependentFileTag::setDependent($this->work_position_competition->tag->id, $this->employee_candidate->tag->id);
//    }
//    
//    public function afterDelete()
//    {
//        parent::afterSave();
//        //prebaceno u trigger
////        DependentFileTag::unsetDependent($this->work_position_competition->tag->id, $this->employee_candidate->tag->id);
//    }

    public function getStatusData()
    {
        return [
            'APPLIED' => Yii::t('HRModule.EmployeeCandidateToWorkPositionCompetition', 'STATUS_APPLIED'),
            'NOT_INVITED_TO_INTERVIEW' => Yii::t('HRModule.EmployeeCandidateToWorkPositionCompetition', 'STATUS_NOT_INVITED_TO_INTERVIEW'),
            'TO_INVITE_TO_INTERVIEW' => Yii::t('HRModule.EmployeeCandidateToWorkPositionCompetition', 'STATUS_TO_INVITE_TO_INTERVIEW'),
            'INVITED_TO_INTERVIEW' => Yii::t('HRModule.EmployeeCandidateToWorkPositionCompetition', 'STATUS_INVITED_TO_INTERVIEW'),
            'CONFIRMED_INTERVIEW' => Yii::t('HRModule.EmployeeCandidateToWorkPositionCompetition', 'STATUS_CONFIRMED_INTERVIEW'),
            'NOT_CAME' => Yii::t('HRModule.EmployeeCandidateToWorkPositionCompetition', 'STATUS_NOT_CAME'),
            'DONE_INTERVIEW' => Yii::t('HRModule.EmployeeCandidateToWorkPositionCompetition', 'STATUS_DONE_INTERVIEW'),
            'NOT_PASSED_INTERVIEW' => Yii::t('HRModule.EmployeeCandidateToWorkPositionCompetition', 'STATUS_NOT_PASSED_INTERVIEW'),
            'OFFERED_JOB' => Yii::t('HRModule.EmployeeCandidateToWorkPositionCompetition', 'STATUS_OFFERED_JOB'),
            'REJECTED_JOB' => Yii::t('HRModule.EmployeeCandidateToWorkPositionCompetition', 'STATUS_REJECTED_JOB'),
            'ACCEPTED_JOB' => Yii::t('HRModule.EmployeeCandidateToWorkPositionCompetition', 'STATUS_ACCEPTED_JOB')
        ];
    }
    
    public function scopes()
    {
        $ectwpcTable = EmployeeCandidateToWorkPositionCompetition::model()->tableName();
        $cgTable = CompetitionGrade::model()->tableName();
        $uniq = SIMAHtml::uniqid();
        $alias = $this->getTableAlias();
        return array(
            'byAvgCVGradeDESC' => array(
                'select' => "$alias.*, (select avg(cg.cv_grade)
from $ectwpcTable ectwpc, $cgTable cg
where ectwpc.id=cg.employee_candidate_to_work_position_competition_id and ectwpc.id=$alias.id) as avg_cv_grade$uniq",
                'order' => "avg_cv_grade$uniq DESC"
            ),
            'byIdASC' => [
                'order' => "$alias.id ASC"
            ]
        );
    }
    
    public function notGradedByUser($user_id)
    {
        $alias = $this->getTableAlias();
        $competition_grades_table = CompetitionGrade::model()->tableName();
        $work_position_competition_juries_table = WorkPositionCompetitionJury::model()->tableName();
        $uniq = SIMAHtml::uniqid();
        
        $this->getDbCriteria()->mergeWith([
            'condition'=>"not exists ("
                . "select 1 from $competition_grades_table cg$uniq "
                . "left join $work_position_competition_juries_table wpcj$uniq on cg$uniq.work_position_competition_juries_id=wpcj$uniq.id "
                . "where cg$uniq.employee_candidate_to_work_position_competition_id=$alias.id and wpcj$uniq.jury_id=:param1$uniq)",
            'params'=>[
                ":param1$uniq"=>$user_id
            ]
        ]);
        
        return $this;
    }
    
    public function hasCVGradeByUser($user_id)
    {
        $alias = $this->getTableAlias();
        $competition_grades_table = CompetitionGrade::model()->tableName();
        $work_position_competition_juries_table = WorkPositionCompetitionJury::model()->tableName();
        $uniq = SIMAHtml::uniqid();

        $this->getDbCriteria()->mergeWith([
            'join' => "
                left join $competition_grades_table cg$uniq on cg$uniq.employee_candidate_to_work_position_competition_id=$alias.id 
                left join $work_position_competition_juries_table wpcj$uniq on cg$uniq.work_position_competition_juries_id=wpcj$uniq.id
            ",
            'condition'=>"wpcj$uniq.jury_id=:param1$uniq and cg$uniq.cv_grade is not null",
            'params'=>[
                ":param1$uniq"=>$user_id
            ]
        ]);
        
        return $this;
    }
    
    public function hasNotCVGradeByUser($user_id)
    {
        $alias = $this->getTableAlias();
        $competition_grades_table = CompetitionGrade::model()->tableName();
        $work_position_competition_juries_table = WorkPositionCompetitionJury::model()->tableName();
        $uniq = SIMAHtml::uniqid();
        
        $this->getDbCriteria()->mergeWith([
            'condition'=>"
                not exists (
                    select 1 from $competition_grades_table cg$uniq 
                    left join $work_position_competition_juries_table wpcj$uniq on cg$uniq.work_position_competition_juries_id=wpcj$uniq.id 
                    where cg$uniq.employee_candidate_to_work_position_competition_id=$alias.id and wpcj$uniq.jury_id=:param1$uniq
                ) or exists
                (
                    select 1 from $competition_grades_table cg$uniq 
                    left join $work_position_competition_juries_table wpcj$uniq on cg$uniq.work_position_competition_juries_id=wpcj$uniq.id 
                    where cg$uniq.employee_candidate_to_work_position_competition_id=$alias.id and wpcj$uniq.jury_id=:param1$uniq and cg$uniq.cv_grade is null
                )",
            'params'=>[
                ":param1$uniq"=>$user_id
            ]
        ]);
        
        return $this;
    }
    
    public function hasInterviewGradeByUser($user_id)
    {
        $alias = $this->getTableAlias();
        $competition_grades_table = CompetitionGrade::model()->tableName();
        $work_position_competition_juries_table = WorkPositionCompetitionJury::model()->tableName();
        $uniq = SIMAHtml::uniqid();

        $this->getDbCriteria()->mergeWith([
            'join' => "
                left join $competition_grades_table cg$uniq on cg$uniq.employee_candidate_to_work_position_competition_id=$alias.id 
                left join $work_position_competition_juries_table wpcj$uniq on cg$uniq.work_position_competition_juries_id=wpcj$uniq.id
            ",
            'condition'=>"wpcj$uniq.jury_id=:param1$uniq and cg$uniq.interview_grade is not null",
            'params'=>[
                ":param1$uniq"=>$user_id
            ]
        ]);
        
        return $this;
    }
    
    public function hasNotInterviewGradeByUser($user_id)
    {
        $alias = $this->getTableAlias();
        $competition_grades_table = CompetitionGrade::model()->tableName();
        $work_position_competition_juries_table = WorkPositionCompetitionJury::model()->tableName();
        $uniq = SIMAHtml::uniqid();
        
        $this->getDbCriteria()->mergeWith([
            'condition'=>"
                not exists (
                    select 1 from $competition_grades_table cg$uniq 
                    left join $work_position_competition_juries_table wpcj$uniq on cg$uniq.work_position_competition_juries_id=wpcj$uniq.id 
                    where cg$uniq.employee_candidate_to_work_position_competition_id=$alias.id and wpcj$uniq.jury_id=:param1$uniq
                ) or exists
                (
                    select 1 from $competition_grades_table cg$uniq 
                    left join $work_position_competition_juries_table wpcj$uniq on cg$uniq.work_position_competition_juries_id=wpcj$uniq.id 
                    where cg$uniq.employee_candidate_to_work_position_competition_id=$alias.id and wpcj$uniq.jury_id=:param1$uniq and cg$uniq.interview_grade is null
                )",
            'params'=>[
                ":param1$uniq"=>$user_id
            ]
        ]);
        
        return $this;
    }
    
    public function afterSave() 
    {
        parent::afterSave();
        
        if ($this->isNewRecord)
        {
            foreach ($this->work_position_competition->juries as $jury)
            {
                Yii::app()->warnManager->sendWarns($jury->id, 'WARN_NOT_REVIEWED_EMPLOYEE_CANDIDATES');
            }
        }
    }
    
    public function afterDelete()
    {
        parent::afterDelete();
        
        foreach ($this->work_position_competition->juries as $jury)
        {
            Yii::app()->warnManager->sendWarns($jury->id, 'WARN_NOT_REVIEWED_EMPLOYEE_CANDIDATES');
        }
    }
}