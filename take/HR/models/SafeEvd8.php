<?php

class SafeEvd8 extends SIMAActiveRecord {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }   

    public function tableName()
    {
        return 'hr.safe_evd_8';
    }

    public function moduleName()
    {
        return 'HR';
    }

    public function __get($column)
    {
        switch ($column)
        {
            default: return parent::__get($column);
        }
    }

    public function rules()
    {
        return array(
            array('data, num_expert_report, date_control, date_next_control, comment, description', 'safe'),
            array('date_control, date_next_control', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => array('insert', 'update'))
        );
    }

    public function relations($child_relations = [])
    {
        return array(
            'file' => array(self::BELONGS_TO, 'File', 'id'),
        );
    }   

    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters' : return [
                    'data' => 'text',
                    'num_expert_report' => 'text',
                    'date_control' => 'date_range',
                    'date_next_control' => 'date_range',
                    'comment' => 'text',
                    'description' => 'text',
                    'file'=>'relation'
                ];
            case 'mutual_forms': return ['base_relation' => 'file'];            
            default: return parent::modelSettings($column);
        }
    }
    
    public function beforeSave() 
    {        
        if (empty($this->id) && empty($this->file))
        {            
            $file = new File();
            $file->name = $this->modelLabel();
            $file->save();
            $this->id = $file->id;
            $this->file = $file;
        }
        
        return parent::beforeSave();
    }
}
