<?php

class SourceInjury extends SIMAActiveRecord
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'hr.source_injuries';
    }

    public function moduleName()
    {
        return 'HR';
    }

    public function __get($column)
    {
        switch ($column)
        {
            case 'DisplayName':
            case 'SearchName': return '('.$this->code.') '.$this->name;
            default: return parent::__get($column);
        }
    }

    public function rules()
    {
        return [
            ['name, code', 'required'],
            ['description, parent_id', 'safe']
        ];
    }

    public function relations($child_relations = [])
    {
        return parent::relations(array_merge([
            'parent' => [self::BELONGS_TO, 'SourceInjury', 'parent_id']
        ], $child_relations));
    }

    public function scopes()
    {
        $alias = $this->getTableAlias();
        return array(
            'byName' => array('order' => $alias . '.name ASC'),
        );
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters' : return array(
                    'code' => 'text',
                    'name' => 'text',
                    'description' => 'text',
                    'parent' => 'relation', 
                );
            case 'textSearch' : return array(
                    'code' => 'text',
                    'name' => 'text',
                    'description' => 'text'
                );
            default: return parent::modelSettings($column);
        }
    }
    
    public function withoutUniqueFields($codebook_unique_fields)
    {
        $alias = $this->getTableAlias();
        $condition = "";
        if (!empty($codebook_unique_fields))
        {           
            $unique_fields_packages = "'".implode("','", $codebook_unique_fields)."'";
            
            $condition = $alias.".code not in ($unique_fields_packages)";
        }
        $criteria = new CDbCriteria();
        $criteria->condition = $condition;
        $this->getDbCriteria()->mergeWith($criteria);
        return $this;
    }

}
