<?php

class QualificationLevel extends SIMAActiveRecord
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
    
    public function tableName()
    {
        return 'hr.qualification_levels';
    }
    
    public function moduleName()
    {
        return 'HR';
    }
    
    public function __get($column)
    {
        switch ($column)
        {           
            case 'DisplayName': return $this->code." ({$this->name})";
            case 'SearchName': return $this->DisplayName;

            default: return parent::__get($column);
        }
    }
    
    public function rules()
    {
        return [
            ['name, code', 'required'],
            ['description', 'safe'],
            ['code', 'length', 'max'=>16],
            ['code', 'numerical', 'integerOnly' => true],
            ['code', 'unique']
        ];
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters' : return [
                'name' => ['text', 'use_cyrillic' => true],
                'code' => 'integer',
                'description' => ['text', 'use_cyrillic' => true],    
            ];
            case 'textSearch' : return [
                'name' => ['text', 'use_cyrillic' => true],
                'code' => 'integer'
            ];
            default: return parent::modelSettings($column);
        }
    }
    
    public function withoutUniqueFields($unique_fields)
    {
        $alias = $this->getTableAlias();
        $condition = "";
        if (!empty($unique_fields))
        {           
            $unique_fields_packages = "'".implode("','", $unique_fields)."'";
            
            $condition = $alias.".code not in ($unique_fields_packages)";
        }
        $criteria = new CDbCriteria();
        $criteria->condition = $condition;
        $this->getDbCriteria()->mergeWith($criteria);
        return $this;
    }
}