<?php

/**
 * @package SIMA
 * @subpackage HR
 */
class WorkPositionCompetitionJury extends SIMAActiveRecord
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'hr.work_position_competition_juries';
    }

    public function moduleName()
    {
        return 'HR';
    }

//    public function __get($column)
//    {
//        switch ($column)
//        {
//            default: return parent::__get($column);
//        }
//    }

    public function relations($child_relations = [])
    {
        return array(
            'jury' => array(self::BELONGS_TO, 'User', 'jury_id'),
            'work_position_competition' => array(self::BELONGS_TO, 'WorkPositionCompetition', 'work_position_competition_id'),
            'competition_grades' => array(self::HAS_MANY, 'CompetitionGrade', 'work_position_competition_juries_id'),
            'competition_grades_count' => array(self::STAT, 'CompetitionGrade', 'work_position_competition_juries_id','select'=>'count(*)'),
        );
    }
    
    public function getnot_graded_count()
    {
        $total = $this->work_position_competition->employee_candidate_count;
        $graded = $this->competition_grades_count;
        return $total - $graded;
    }

    public function rules()
    {
        return [
            ['jury_id, work_position_competition_id', 'required']
//            array('amount_neto, amount_bruto, amount_total', 'numerical', 'message' => 'Polje "{attribute}" mora biti broj!'),
////            array('comment', 'safe'),
//            array('paycheck_period_id', 'default',
//                'value' => null,
//                'setOnEmpty' => true, 'on' => array('insert', 'update'))
        ];
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters': return [
                'jury' => 'relation',
                'work_position_competition' => 'relation',
            ];
            case 'options': return ['delete'];
            default: return parent::modelSettings($column);
        }
    }
    
    public function afterSave()
    {
        parent::afterSave();
        EmployeeTQLLimit::add($this->jury_id, 20, $this->work_position_competition->tag->query);

        $comment_thread = CommentThread::getCommonForModel($this->work_position_competition);
        $comment_thread->addListener($this->jury_id);
        
        if ($this->isNewRecord)
        {
            if (Yii::app()->user->id !== intval($this->jury_id))
            {
                $notif_text = Yii::t('HRModule.WorkPositionCompetitionJury', 'JuryAddedNotif', [
                    '{work_position_competition_name}' => $this->work_position_competition->DisplayName
                ]);
                Yii::app()->notifManager->sendNotif($this->jury_id, $notif_text, 
                    [
                        'info' => $notif_text,
                        'action' => [
                           'action' => 'HR/WPCompetitions/index'
                        ]
                    ]
                );
            }
            
            Yii::app()->warnManager->sendWarns($this->jury_id, 'WARN_NOT_REVIEWED_EMPLOYEE_CANDIDATES');
        }
    }
    
    public function afterDelete()
    {
        parent::afterDelete();
        EmployeeTQLLimit::remove($this->jury_id, 20, $this->work_position_competition->tag->query);
        
        $comment_thread = CommentThread::getCommonForModel($this->work_position_competition);
        $comment_thread->removeListener($this->jury_id);
        
        Yii::app()->warnManager->sendWarns($this->jury_id, 'WARN_NOT_REVIEWED_EMPLOYEE_CANDIDATES');
    }

}