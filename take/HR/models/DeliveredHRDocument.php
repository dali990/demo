<?php

class DeliveredHRDocument extends SIMAActiveRecord
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'hr.delivered_hr_documents';
    }

    public function moduleName()
    {
        return 'HR';
    }   

    public function rules()
    {
        return array(
            array('sent, sender_id, sent_timestamp, file_id', 'required'),
            array('received, recipient_id, received_timestamp, email_id, link, file_signature_view_id, delivery_type', 'safe'),
            array('file_signature_view_id, sender_id, recipient_id, file_id','default',
                'value'=>null,
                'setOnEmpty'=>true,'on'=>array('insert','update'))
        );
    }

    public function relations($child_relations = [])
    {
        return [
            'file' => [self::BELONGS_TO, 'File', 'file_id'],
            'file_signature_view' => [self::BELONGS_TO, 'FileSignatureView', 'file_signature_view_id'],
            'email' => [self::BELONGS_TO, 'Email', 'email_id'],
            'sender' => [self::BELONGS_TO, 'User', 'sender_id'],
            'recipient' => [self::BELONGS_TO, 'Person', 'recipient_id']
        ];
    }   
    
    public function scopes()
    {
        $local_alias = $this->getTableAlias();
        return array(
            'byName' => array('order' => "$local_alias.sent_timestamp DESC")
        );
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters' : return array(
                'file_signature_view' => 'relation',
                'email' => 'relation',
                'sender' => 'relation',
                'recipient' => 'relation',
                'delivery_type' => 'dropdown',
                'link' => 'text',
                'file' => 'relation',
                'sent_timestamp'=>'date_time_range',
                'received_timestamp'=>'date_time_range'
            );
            case 'statuses':  return array(
                'sent' => array(
                    'title' => 'Poslato',
                    'timestamp' => 'sent_timestamp',
                    'user' => array('sender_id','relName'=>'sender')                    
                ),
                'received' => array(
                    'title' => 'Primljeno',
                    'timestamp' => 'received_timestamp',
                    'user' => array('recipient_id','relName'=>'recipient'),
                    'checkAccessConfirm' => 'checkReceivedConfirm',                    
                )
            );
            default: return parent::modelSettings($column);
        }
    }

    public function checkReceivedConfirm()
    {
        $errors = [];
        
        if (!Yii::app()->user->checkAccess('HRDeliveryPrintConfirm'))
        {
            $errors[] = 'Nemate privilegiju da potvrdite.';
        }
        
        if ($this->delivery_type !== Employee::$PRINT)
        {
            $errors[] = 'Može se potvrditi samo za zaposlene za koje se štampaju dokumenta.';
        }
        
        return (count($errors) > 0) ? $errors : true;
    }
    
    public function withoutDeliveryIds($document_delivery_ids)
    {        
        if (is_array($document_delivery_ids))
        {            
            $ids = implode(',', $document_delivery_ids);
        }
        else
        {
            $ids = $document_delivery_ids;
        }
        $alias = $this->getTableAlias();
        $this->getDbCriteria()->mergeWith([            
            'condition'=>"$alias.id not in ($ids)"
        ]);

        return $this;
    }
}
