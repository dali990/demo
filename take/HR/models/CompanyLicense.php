<?php

class CompanyLicense extends SIMAActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'hr.company_licenses';
    }
    
    public function moduleName()
    {
        return 'HR';
    }
    
    public function __get($column)
    {
        switch ($column)
        {
            case 'DisplayName': return $this->code.' - '.$this->name;
            case 'SearchName': return $this->DisplayName;
            default: return parent::__get($column);
        }
    }
    
    public function rules()
    {
        return array(
            array('name, persons_number, references_number, code', 'required'),
            array('code', 'unique'),
            array('description, workLicenses','safe'),
            array('persons_number','checkPersonsNumber'),
            array('references_number','checkReferencesNumber'),
            array('workLicenses','checkWorkLicenses'),
        );
    }
    
    public function scopes()
    {
        return array(
            'byName' => array('order' => 'code, name')   
        );
    }
    
    public function checkPersonsNumber($attribute, $params)
    {
        if (intval($this->persons_number) < 2)
        {
            $this->addError('persons_number', 'Moraju biti bar 2 osobe.');
        }
    }
    
    public function checkReferencesNumber($attribute, $params)
    {
        if (intval($this->references_number) < 4)
        {
            $this->addError('references_number', 'Moraju biti bar 4 reference.');
        }
    }
    
    public function checkWorkLicenses($attribute, $params)
    {
        if (isset($this->work_licenses) && is_string($this->work_licenses))
        {
            $work_licenses = CJSON::decode($this->work_licenses);
            if (!isset($work_licenses['ids']) || count($work_licenses['ids']) === 0)
                $this->addError('workLicenses', 'Velika licenca mora da ima bar jednu malu licencu!');
        }
    }
    
    public function relations($child_relations = [])
    {
        $company_license_decisions_to_company_licenses_table_name = CompanyLicenseDecisionToCompanyLicense::model()->tableName();
    	return array(
            'work_licenses'=>array(self::MANY_MANY, 'WorkLicense', 'hr.company_licenses_to_work_licenses(company_license_id, work_license_id)'),
            'companies'=>array(self::MANY_MANY, 'Company', 'hr.company_licenses_to_companies(company_license_id, company_id)'),
            'company_license_decisions'=>array(self::MANY_MANY, 'CompanyLicenseDecision', "$company_license_decisions_to_company_licenses_table_name(company_license_id,company_license_decision_id)")
    	);
    }    

    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters' : return array(
                'code' => 'text',
                'name' => 'text',
                'description' => 'text',                
            );
            case 'textSearch' : return array(
                'code' => 'text',
                'name' => 'text',
                'description' => 'text',                
            );
            case 'options': 
                $options = array();
                if (isset($this->id))
                {
                    if (Yii::app()->user->checkAccess('FormOpen',[],$this))    
                    {
                        $options[] = 'form';
                    }
                    if (Yii::app()->user->checkAccess('Delete',[],$this))    
                    {
                        $options[] = 'delete';
                    }
                }
                return $options;
            case 'form_list_relations' : return array('work_licenses');
            case 'default_order' : return 'code';
            default : return parent::modelSettings($column);
        }
    }
}
