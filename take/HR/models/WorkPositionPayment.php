<?php

class WorkPositionPayment extends SIMAActiveRecord
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'hr.work_position_payments';
    }

    public function moduleName()
    {
        return 'HR';
    }

    public function __get($column)
    {
        switch ($column)
        {
            case 'DisplayName': return $this->name;
            case 'SearchName': return $this->name;
            default: return parent::__get($column);
        }
    }

    public function rules()
    {
        return array(
            array('name, amount','required'),
            array('description', 'safe')            
        );
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters' : return array_merge(parent::modelSettings($column),array(
                'name' => 'text',
                'description' => 'text'
            ));
            case 'number_fields': return [
                'amount'
            ];
            default: return parent::modelSettings($column);
        }
    }
}
