<?php

class NationalEventToDayYear extends SIMAActiveRecord
{
    public $_day_date = null;
    public $on_date = null;
    
    public $_before_save_day_date = null;
        
    public static function model($className = __CLASS__)
    {        
        return parent::model($className);
    }

    public function tableName()
    {
        return 'hr.national_event_to_day_year';
    }

    public function moduleName()
    {
        return 'HR';
    }
    
    public function __get($column)
    {
        switch ($column)
        {
            case 'path2': 
                $result = 'HR/nationalEvents';
                return $result;
            case 'DisplayName': 
                $type = 'UNKNOWN';
                $displayName = 'UNKNOWN';
                if(isset($this->national_event))
                {
                    $type = $this->national_event->columnDisplays('type');
                    $displayName = $this->national_event->DisplayName;
                }
                else
                {
                    error_log(__METHOD__.' - national_event not set');
                    error_log(__METHOD__.' - '.CJSON::encode($this));
                }
                
                $day_date = 'UNKNOWN';
                if(isset($this->day))
                {
                    $day_date = $this->day->day_date;
                }
//                else
//                {
//                    error_log(__METHOD__.' - day not set');
//                    error_log(__METHOD__.' - '.CJSON::encode($this));
//                }
                
                $result = $type.": ".$displayName.' - '.$day_date;
                return $result;
            default: return parent::__get($column);
        }
    }
    
    public function rules()
    {
        return array(
            array('national_event_id, _day_date', 'required'),
            array('national_event_id', 'checkEventOnDateUnique')
        );
    }
    
    public function checkEventOnDateUnique()
    {
        if (!$this->hasErrors() && $this->getIsNewRecord()
            && isset($this->national_event_id) && isset($this->_day_date))
        {
            $criteria = new SIMADbCriteria([
                'Model' => NationalEventToDayYear::model(),
                'model_filter' => [
                    'national_event' => [
                        'ids' => $this->national_event_id
                    ],
                    'day' => [
                        'day_date' => SIMAHtml::UserToDbDate($this->_day_date)
                    ]
                ]
            ], true);
            
            if(!empty($criteria->ids))
            {
                $this->addError('_day_date', Yii::t('HRModule.NationalEvent', 'NationalEventOnDateAlreadyExists', [
                    '{event}' => $this->national_event->DisplayName,
                    '{date}' => $this->_day_date
                ]));
            }
        }
    }
    
    public function relations($child_relations = [])
    {
        return array(
            'national_event' => array(self::BELONGS_TO, 'NationalEvent', 'national_event_id'),
            'day' => array(self::BELONGS_TO, 'Day', 'day_id'),
            'year' => array(self::BELONGS_TO, 'Year', 'year_id')
        );
    }
    
    public function filter_on_date($condition)
    {
        if(isset($this->on_date))
        {
            $dateRangeParamFormated = SIMAHtml::FormatDateRangeParam($this->on_date);
            $temp_condition = new SIMADbCriteria([
                'Model' => NationalEventToDayYear::model(),
                'model_filter' => [
                    'day' => [
                        'day_date' => $dateRangeParamFormated['startDate'].'<>'.$dateRangeParamFormated['endDate']
                    ]
                ]
            ]);
            
            $condition->mergeWith($temp_condition, true);
        }
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters' : return array(
                'national_event' => 'relation',
                'day' => 'relation',
                'year' => 'relation',
                'on_date' => ['func' => 'filter_on_date'],
            );
            case 'options': return ['form', 'delete'];
            default: return parent::modelSettings($column);
        }
    }
    
    public function afterFind()
    {
        $day = $this->day;
        if(isset($day))
        {
            $this->_day_date = $day->day_date;
        }
        return parent::afterFind();
    }
    
    public function BeforeSave()
    {
        if(isset($this->day))
        {
            $this->_before_save_day_date = $this->day->day_date;
        }
        
        $date_matches = null;
        preg_match("/^(0[1-9]|[1-2][0-9]|3[0-1])\.(0[1-9]|1[0-2])\.[0-9]{3,4}\.$/", $this->_day_date, $date_matches);
        if(count($date_matches) === 0)
        {
            throw new Exception(Yii::t('HRModule.NationalEvent', 'DateInvalidFormat'));
        }
        $dayModel = Day::getByDate($date_matches[0]);
        if(is_null($dayModel))
        {
            throw new Exception(Yii::t('HRModule.NationalEvent', 'DayNotFound'));
        }

        $year_matches = null;
        preg_match("/\.[0-9]{3,4}\.$/", $this->_day_date, $year_matches);
        if(count($year_matches) === 0)
        {
            throw new Exception(Yii::t('HRModule.NationalEvent', 'DateInvalidFormat'));
        }
        $year_num = substr($year_matches[0], 1, -1);
        $yearModel = Year::get($year_num);
        if(is_null($yearModel))
        {
            throw new Exception(Yii::t('HRModule.NationalEvent', 'YearNotFound'));
        }

        $this->day_id = $dayModel->id;
        $this->year_id = $yearModel->id;
            
        return parent::BeforeSave();
    }
    
    public function afterSave()
    {
        parent::afterSave();
    }
    
    public function beforeDelete()
    {
        if(isset($this->day))
        {
            $this->_before_save_day_date = $this->day->day_date;
        }
        
        return parent::beforeDelete();
    }
}

