<?php

class EntryDoorLog extends SIMAActiveRecord
{
    public static $TYPE_ENTRY = 'ENTRY';
    public static $TYPE_EXIT = 'EXIT';
    
    public $at_day = null;
        
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
 
    public function tableName()
    {
       return 'hr.entry_door_logs';
    }
    
    public function moduleName() 
    {
        return 'HR';
    }
    
    public function rules()
    {
        return [
            ['time, entry_door_card_id, type', 'required'],
            ['partner_id, additional_data', 'safe']
        ];
    }
    
    public function relations($child_relations = [])
    {
        return [
            'partner' => [self::BELONGS_TO, 'Partner', 'partner_id'],
            'card' => [self::BELONGS_TO, 'EntryDoorCard', 'entry_door_card_id']
        ];
    }
    
    public function scopes()
    {
        $alias = $this->getTableAlias();
        return [
            'orderByTimeDesc' => [
                'order' => $alias.'.time DESC',
            ],
            'orderByTimeAsc' => [
                'order' => $alias.'.time ASC',
            ],
            'today' => [
                'condition' => $alias.'.time::DATE=CURRENT_DATE'
            ],
            'confirmed' => [
                'condition' => $alias.'.confirmed=TRUE'
            ]
        ];
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters':
                return [
                    'type' => ['dropdown'],
                    'partner' => 'relation',
                    'partner_id' => 'dropdown',
                    'card' => 'relation',
                    'at_day' => ['func' => 'filter_at_day']
                ];
            case 'options':
                return [];
            default: return parent::modelSettings($column);
        }
    }
    
    public function filter_at_day($condition)
    {
        if (!empty($this->at_day))
        {
            if(get_class($this->at_day) !== 'Day')
            {
                throw new Exception('at_day must be of class "Day"');
            }
            
            $date = SIMAHtml::UserToDbDate($this->at_day->day_date);
            
            $alias = $this->getTableAlias();
            $condition->mergeWith([
                'condition' => "$alias.time::date='$date'::date"
            ], true);
        }
    }
    
    public static function getTypeData()
    {
        return [
            EntryDoorLog::$TYPE_ENTRY => Yii::t('HRModule.EntryDoor', 'TYPE_ENTRY'),
            EntryDoorLog::$TYPE_EXIT => Yii::t('HRModule.EntryDoor', 'TYPE_EXIT'),
        ];
    }
}
