<?php

/**
 * Description of PersonToWorkPosition
 *
 * @author goran
 */
class PersonToWorkPosition extends SIMAActiveRecord
{

//    public $from_work_contract = false;
    
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'hr.persons_to_work_positions';
    }

    public function moduleName()
    {
        return 'HR';
    }

    public function relations($child_relations = [])
    {
        return array(
            'person' => array(self::BELONGS_TO, 'Person', 'person_id'),
            'work_position' => array(self::BELONGS_TO, 'WorkPosition', 'work_position_id'),
        );
    }

    public function rules()
    {
        return array(
            array('person_id, work_position_id', 'required'),
            array('person_id,work_position_id, fictive', 'safe'),
            array('person_id', 'checkUniq'),
            array('person_id', 'checkNotFictiveWorkPositions'),
            array('person_id, work_position_id', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => array('insert', 'update'))
        );
    }
    
    public function checkUniq()
    {
        if (
                intval($this->__old['person_id']) !== intval($this->person_id) || 
                intval($this->__old['work_position_id']) !== intval($this->work_position_id)
           )
        {
            $person_to_work_position = PersonToWorkPosition::model()->findByAttributes([
                'person_id'=>$this->person_id,
                'work_position_id'=>$this->work_position_id                
            ]);
            if (!is_null($person_to_work_position))
            {
                $this->addError('person_id', "Osoba ".$this->person->DisplayName." je već postavljena na radnoj poziciji ".$this->work_position->DisplayName);
            }
        }
    }
    
    public function checkNotFictiveWorkPositions()
    {
        if (
                boolval($this->fictive) === false && 
                (
                    intval($this->__old['person_id']) !== intval($this->person_id) || 
                    boolval($this->__old['fictive']) !== boolval($this->fictive)
                )
           )
        {
            $person_to_work_position = PersonToWorkPosition::model()->findByAttributes([
                'person_id'=>$this->person_id,
                'fictive'=>$this->fictive                
            ]);
            if (!is_null($person_to_work_position))
            {
                $this->addError('person_id', "Osoba ".$this->person->DisplayName." može da ima samo jednu glavnu poziciju.");
            }
        }
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'options' : 
                $options = [];
                if (isset($this->id) && $this->fictive === true)
                {
                    $options = ['form', 'delete'];
                }
                return $options;           
            case 'filters' : return array_merge(parent::modelSettings($column),array(
                'person' => 'relation',
                'work_position' => 'relation',
                'fictive' => 'boolean'
            ));
            default: return parent::modelSettings($column);
        } 
    }
    
    public function withOutWorkPositions($ids=[])
    {
        if (!is_array($ids))
        {
            $ids = [$ids];
        }
        
        $alias = $this->getTableAlias();        
        $ids_as_string = implode(',', $ids);        
        $this->getDbCriteria()->mergeWith(array(
            'condition'=> "$alias.work_position_id not in ($ids_as_string)",
            //Sasa - nisam uspeo preko parametra
//            'params' => [
//                ":ids$uniq" => $ids,
//            ]
        ));
        
        return $this;
    }
}
    
?>
