<?php

class AbsenceAnnualLeave extends Absence
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function defaultScope()
    {
        $alias = $this->getTableAlias(FALSE, FALSE);
        $type = self::$ANNUAL_LEAVE;
        return [
            'condition' => "$alias.type='$type'"
        ];
    }
    
    public function __get($column)
    {
        switch ($column)
        {
            case 'DisplayName': return "Godišnji odmor za ".$this->employee->DisplayName.' od '.$this->begin.' do '.$this->end;
            case 'SearchName': return $this->DisplayName;
            default: return parent::__get($column);
        }
    }
    
    public function rules()
    {
        return array_merge(parent::rules(), array(
            array('confirmed', 'checkRemainingDays', 'on' => array('insert', 'update'))
        ));
    }
    
    public function relations($child_relations = [])
    {
        return parent::relations(array_merge([
            'annual_leave_to_annual_leave_decisions' => array(self::HAS_MANY, 'AnnualLeaveDecisionToAnnualLeave', 'annual_leave_id'),
            'absence' => array(self::HAS_ONE, 'Absence', 'id'),
        ], $child_relations));
    }
    
    public function checkRemainingDays() 
    {
        if (!$this->hasErrors())
        {
            if($this->confirmed && $this->checkEmployeeAnnualLeaveDays() === null)
            {
                $this->addError('confirmed', Yii::t('HRModule.Absence', 'EmployeeDoNotHaveAnnualLeaveDays', [
                    '{emp}' => $this->employee->DisplayName
                ]));
            }
        }
    }
    
    public function beforeSave()
    {
        $this->type = self::$ANNUAL_LEAVE;
        
        return parent::beforeSave();
    }
    
    public function onConfirm()
    {
        if ($this->confirmed === true)
        {
            $empoyee_annual_leave_days = $this->checkEmployeeAnnualLeaveDays();
            if (!is_null($empoyee_annual_leave_days))
            {
                foreach ($empoyee_annual_leave_days as $value)
                {                
                    $value['annual_leave_decision']->removeDays($this->id, $value['number_days']);
                }
            }
            else
            {
                throw new SIMAWarnException('Nemate dovoljno dana na raspolaganju.');
            }
        }
    }
    
    public function onRevert()
    {
        foreach ($this->annual_leave_to_annual_leave_decisions as $annual_leave_to_annual_leave_decision)
        {
            $annual_leave_to_annual_leave_decision->delete();
        }                       
    }
    
    /**
     * Proverava da li zaposleni ima dane na raspolaganju. Ako nema onda vraca null, inace vraca
     * niz stavki gde je svaka stavka tipa $item['annual_leave_decision']=resenje_sa_kog_se_skidaju_dani i $item['number_days']=broj_dana_koji_se_skida
     * @return null/array
     */
    public function checkEmployeeAnnualLeaveDays()
    {
        $return = [];
        $active_annual_leave_decisions = $this->employee->getActiveAnnualLeaveDecisions($this->begin);
        
        $curr_number_work_days = intval($this->number_work_days);
        foreach ($active_annual_leave_decisions as $annual_leave_decision)
        {
            $annual_leave_decisions_to_annual_leaves = AnnualLeaveDecisionToAnnualLeave::model()->findByAttributes([
                'annual_leave_decision_id'=>$annual_leave_decision->id,
                'annual_leave_id'=>$this->id,
                'days_number'=>$curr_number_work_days
            ]);
            //u slucaju da smo vec skinuli samo vratimo prazan niz
            if (!is_null($annual_leave_decisions_to_annual_leaves))
            {
                return [];
            }
            else
            {
                $annual_leave_decision_remaining_days = $annual_leave_decision->getRemainingDays();
                //ako najstarije resenje ima dovoljno neiskoriscenih dana skidamo sa njega i prekidamo petlju
                if (intval($annual_leave_decision_remaining_days) >= intval($curr_number_work_days))
                {
                    $item['annual_leave_decision'] = $annual_leave_decision;
                    $item['number_days'] = $curr_number_work_days;
                    array_push($return, $item); 
                    $curr_number_work_days = 0;
                    break;
                }
                //inace skidamo sa njega sve neiskoriscene dane i idemo dalje
                else
                {
                    $item['annual_leave_decision'] = $annual_leave_decision;
                    $item['number_days'] = $annual_leave_decision_remaining_days;
                    array_push($return, $item);                    
                    $curr_number_work_days -= $annual_leave_decision_remaining_days;
                }
            }
        }
        
        if ($curr_number_work_days === 0)
        {
            return $return;
        }
        else
        {
            return null;
        }
    }
}
