<?php

class SafeEvd9 extends SIMAActiveRecord
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'hr.safe_evd_9';
    }

    public function moduleName()
    {
        return 'HR';
    }

    public function __get($column)
    {
        switch ($column)
        {
            case 'DisplayName': return isset($this->fixed_asset)?$this->fixed_asset->DisplayName:'';
            case 'SearchName': return $this->DisplayName;            
            default: return parent::__get($column);
        }
    }

    public function rules()
    {
        return array(
            array('fixed_asset_id, location, num_expert_report, date_control, date_next_control, comment, description', 'safe')            
        );
    }

    public function relations($child_relations = []) {
        return array(
            'fixed_asset'=>array(self::BELONGS_TO, 'FixedAsset', 'fixed_asset_id'),
            'file'=>array(self::BELONGS_TO, 'File', 'id')            
        );
    }
    
    public function scopes()
    {
        return array(
            'byName' => array('order' => 'id'),
        );
    }

    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters' : return [
                'fixed_asset' => 'relation',
                'location' => 'text',
                'num_expert_report' => 'text',
                'date_control' => 'date_range',
                'date_next_control' => 'date_range',
                'comment' => 'text',
                'description' => 'text',
                'file'=>'relation'
            ];
            case 'options': return ['form','delete'];
            case 'mutual_forms' : return array(
                'base_relation' => 'file',
            );
            default: return parent::modelSettings($column);
        }
    }
    
    public function beforeSave() 
    {        
        if (empty($this->id) && empty($this->file))
        {
            $file = new File();
            $file->name = $this->modelLabel();
            $file->save();
            $this->id = $file->id;
            $this->file = $file;
        }
        
        return parent::beforeSave();
    }
}
