<?php

/**
 * @package SIMA
 * @subpackage HR
 */
class EmployeeCandidate extends SIMAActiveRecord
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'hr.employee_candidates';
    }

    public function moduleName()
    {
        return 'HR';
    }

    public function __get($column)
    {
        switch ($column)
        {
            case 'path2': return 'addressbook/partner';
            default: return parent::__get($column);
        }
    }

    public function relations($child_relations = [])
    {
        return array(
            'work_position_competitions' => array(self::MANY_MANY, 'WorkPositionCompetition', 
                'hr.employee_candidate_to_work_position_competition(employee_candidate_id,work_position_competition_id)'
            ),
            'employee_candidate_to_work_position_competition' => array(self::HAS_MANY, 'EmployeeCandidateToWorkPositionCompetition', 'employee_candidate_id'),
            'person' => array(self::BELONGS_TO, 'Person', 'id'),
            'school_course' => array(self::BELONGS_TO, 'SchoolCourse', 'school_course_id'),
        );
    }

    public function rules()
    {
        return array(
//            array('id', 'required', 'message' => 'Polje "{attribute}" mora biti uneto'),
//            array('grade, amount_bruto, amount_total', 'numerical', 'message' => 'Polje "{attribute}" mora biti broj!'),
            array('note, school_course_id', 'safe'),
          array('school_course_id', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => array('insert', 'update'))
        );
    }

    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters' : return array(
                'grade_id' => 'dropdown',
                'person' => 'relation',
//                'work_position_competitions' => 'relation', //TODO: ne postoji pretraga vise na vise
                'employee_candidate_to_work_position_competition' => 'relation',
                'school_course' => 'relation',
                'note' => 'text',
            );
            case 'textSearch' : return [
                'person' => 'relation',
//                'work_position_competitions' => 'relation', //TODO: ne postoji pretraga vise na vise
                'employee_candidate_to_work_position_competition' => 'relation',
                'school_course' => 'relation',
                'note' => 'text',
            ];
            case 'options' : return array('delete','form','open');
            case 'mutual_forms': return [
                'base_relation' => 'person'
            ];
            case 'form_list_relations': return array('work_position_competitions');
////            case 'order_by' : return array('amount_neto, amount_bruto, amount_total');
            default: return parent::modelSettings($column);
        }
    }
    
    public function afterSave() 
    {
        parent::afterSave();
        
        if ($this->isNewRecord)
        {
            PartnerToPartnerList::add(PartnerList::getSystemList('hr_candidates'), $this->person->partner);
        }
    }

    public function afterDelete() 
    {
        parent::afterDelete();
        
        PartnerToPartnerList::remove(PartnerList::getSystemList('hr_candidates'), $this->person->partner);
    }
}