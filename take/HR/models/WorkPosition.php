<?php

class WorkPosition extends SIMAActiveRecord
{

    public $head_sector_id;
    public $people_count = 0;
    public $recursive_company_sector_id = null;
    
    //napravljeno zbog filtera u Ugovorima
    public $company_id = null;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'hr.work_positions';
    }

    public function moduleName()
    {
        return 'HR';
    }

    public function __get($column)
    {
        switch ($column)
        {
            case 'number_person': return count($this->persons_positions);
            case 'DisplayName': 
                $result = $this->name;
                if(!empty($this->company_sector))
                {
                    $result = $this->company_sector->DisplayName.' | '.$this->name;
                }
                return $result;
            case 'SearchName': return $this->code . ' ' . $this->name;
            case 'fullWorkName': return $this->name." ($this->code), ".$this->company_sector->fullName;
            default: return parent::__get($column);
        }
    }

    public function rules()
    {
        return array(
            array('code, name, company_sector_id, benefited_work_experience', 'required'),
            array('name, code, company_sector_id, paygrade_id, description,is_director, enlarged_risk,reason_of_training, safe_actions, notice_safe, '
                . 'medical_report_interval, description_of_activities, payment_amount_id, increased_risk', 'safe'),
            array('company_sector_id, paygrade_id, payment_amount_id', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => array('insert', 'update'))
        );
    }
    
    public function scopes()
    {
        $alias = $this->getTableAlias();

        return array(
            'byName' => array(
                'order' => "$alias.code, $alias.name, $alias.id",
            ),
            'withHeadSector' => array(
                'select' => "$alias.*, recursive_company_sectors.head_sector_id",
                'join' => "left join hr.recursive_company_sectors recursive_company_sectors on $alias.id=recursive_company_sectors.id",
            ),
//            'local_company_positions' => array(
//                'join' => "left join hr.company_sectors cs on $alias.company_sector_id=cs.id left join hr.recursive_company_sectors rcs on cs.id=rcs.id",
//                'condition' => "rcs.head_sector_id='1'"
//            ),
        );
    }

    public function getcompany()
    {
        $pos = $this::model()->withHeadSector()->findByPk($this->id);
        if ($pos===null)
            return null;
        return Company::model()->findByAttributes(array('head_sector_id' => $pos->head_sector_id));
    }

    public function relations($child_relations = [])
    {
        $persons_to_work_positions_table_name = PersonToWorkPosition::model()->tableName();
        $work_positions_to_occupations = WorkPositionToOccupation::model()->tableName();
        return array(
            'work_contracts' => array(self::HAS_MANY, 'WorkContract', 'work_position_id'),
            'active_work_contracts' => array(self::HAS_MANY, 'WorkContract', 'work_position_id', 'scopes'=>['activeFromTo'=>[null,null]]),
            'company_sector' => array(self::BELONGS_TO, 'CompanySector', 'company_sector_id'),
            'paygrade' => array(self::BELONGS_TO, 'Paygrade', 'paygrade_id'),
            'persons'=>array(self::MANY_MANY, 'Person', "$persons_to_work_positions_table_name(work_position_id,person_id)"), 
            'persons_positions' => array(self::HAS_MANY, 'PersonToWorkPosition', 'work_position_id'),
            'fictive_position_to_persons' => array(self::HAS_MANY, 'PersonToWorkPosition', 'work_position_id', 'condition'=>'fictive=true'),
            'not_fictive_position_to_persons' => array(self::HAS_MANY, 'PersonToWorkPosition', 'work_position_id', 'condition'=>'fictive=false'),
            'count_fictive_position_to_persons' => array(self::STAT, 'PersonToWorkPosition', 'work_position_id', 'condition'=>'fictive=true', 'select'=>'count(*)'),
            'count_not_fictive_position_to_persons' => array(self::STAT, 'PersonToWorkPosition', 'work_position_id', 'condition'=>'fictive=false', 'select'=>'count(*)'),
            'risks'=>array(self::MANY_MANY, 'SafeWorkDanger', 'hr.safe_evd_1(work_position_id,work_danger_id)'),
            'work_position_to_risks' => array(self::HAS_MANY, 'SafeEvd1', 'work_position_id'),
            'payment_amount' => array(self::BELONGS_TO, 'WorkPositionPayment', 'payment_amount_id'),
            'required_medical_abilities'=>array(self::MANY_MANY, 'RequiredMedicalAbility', 
            'hr.required_medical_abilities_to_work_positions(work_position_id,required_medical_ability_id)'),
            'work_position_to_required_medical_abilities' => array(self::HAS_MANY, 'RequiredMedicalAbilityToWorkPosition', 'work_position_id'),
            'occupations'=>[self::MANY_MANY, Occupation::class, "$work_positions_to_occupations(work_position_id, occupation_id)"],
            'work_position_to_occupations'=>[self::HAS_MANY, WorkPositionToOccupation::class, 'work_position_id'],
        );
    }
    
    public function modelSettings($column)
    {
        $options = ['open'];
        if (Yii::app()->user->checkAccess('CompanySchemaWrite'))
        {
            $options[] = 'form';
            $options[] = 'delete';
        }
        
        switch ($column)
        {
            case 'options':  return $options;
            case 'filters' : return array_merge(parent::modelSettings($column),array(
                'company_id' => array('dropdown','func' => 'filter_company_id'),
                'description' => 'text',
                'company_sector' => 'relation',
                'paygrade' => 'relation',
                'work_contracts' => 'relation',
                'recursive_company_sector_id' => ['func' => 'filter_recursive_company_sector_id']
            ));
            case 'textSearch' : return array(
                    'code' => 'text',
                    'name' => 'text'
            );
            case 'form_list_relations' : return array('risks','required_medical_abilities','occupations');
            case 'update_relations': return ['company_sector'];
            default: return parent::modelSettings($column);
        }
    }
    
    public function filter_company_id($condition)
    {
        if (isset($this->company_id) && $this->company_id != null)
        {
            $work_position_table_name = WorkPosition::model()->tableName();
            $uniq = SIMAHtml::uniqid();
            $alias = $this->getTableAlias();
            $temp_condition = new CDbCriteria();
            $temp_condition->condition = $alias.'.id in (select wp.id from '.$work_position_table_name.' wp left join hr.recursive_company_sectors rcs on wp.company_sector_id=rcs.id 
                                where head_sector_id=(select head_sector_id from companies where id=:param'.$uniq.'))';
        
            $temp_condition->params = array(':param'.$uniq=>$this->company_id);
            $condition->mergeWith($temp_condition, true);
        }
    }
    
    public function filter_recursive_company_sector_id($criteria, $alias)
    {
        if (!empty($this->recursive_company_sector_id))
        {
            $uniq = SIMAHtml::uniqid();
            $temp_criteria = new SIMADbCriteria();
            $temp_criteria->condition = "$alias.company_sector_id=:param1$uniq or $alias.company_sector_id in (select unnest(rcs$uniq.children_ids) from hr.recursive_company_sectors rcs$uniq where rcs$uniq.id=:param1$uniq)";
            $temp_criteria->params = [
                ":param1$uniq" => $this->recursive_company_sector_id
            ];
            $criteria->mergeWith($temp_criteria, true);
        }
    }
    
    public function withoutIncreasedRisk()
    {    
        $alias = $this->getTableAlias();
        $this->getDbCriteria()->mergeWith([
            'condition' => "$alias.increased_risk = FALSE"
        ]);
        
        return $this;
    }
}
