<?php

/**
 * Description of SafeWorkDanger
 *
 * @author goran
 */
class SafeWorkDanger extends SIMAActiveRecord
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'hr.safe_work_dangers';
    }

    public function moduleName()
    {
        return 'HR';
    }

    public function __get($column)
    {
        switch ($column)
        {
            case 'DisplayName':
            case 'SearchName': return $this->code . ' - ' . $this->description;
            default: return parent::__get($column);
        }
    }

    public function rules()
    {
        return array(
            array('code', 'required'),
            array('description', 'safe')
        );
    }

    public function relations($child_relations = [])
    {
        return array();
    }

    public function scopes()
    {
        $alias = $this->getTableAlias();
        return array(
            'byName' => array('order' => $alias . '.code'),
        );
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters' : return array(
                    'code' => 'text',
                    'description' => 'text'
                );
            case 'textSearch' : return array(
                    'code' => 'text',
                    'description' => 'text'
                );
            default: return parent::modelSettings($column);
        }
    }

}
