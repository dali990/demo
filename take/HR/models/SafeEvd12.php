<?php

/**
 * Description of SafeEvd12
 *
 * @author goran
 */
class SafeEvd12 extends SafeEvd11_12_14
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function BeforeSave()
    {
        $this->type_evd = '12';

        return parent::BeforeSave();
    }

    public function defaultScope()
    {
        return array(
            'condition' => 'type_evd=12',
        );
    }

}
