<?php

/**
 * @package SIMA
 * @subpackage HR
 */
class CompetitionGrade extends SIMAActiveRecord
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'hr.competition_grades';
    }

    public function moduleName()
    {
        return 'HR';
    }

    public function __get($column)
    {
        switch ($column)
        {
            default: return parent::__get($column);
        }
    }

    public function relations($child_relations = [])
    {
        return array(
//            'work_position_competitions' => array(self::MANY_MANY, 'WorkPositionCompetition', 
//                'hr.employee_candidate_to_work_position_competition(employee_candidate_id,work_position_competition_id)'
//            ),
            'employee_candidate_to_work_position_competition' => array(self::BELONGS_TO, 'EmployeeCandidateToWorkPositionCompetition', 'employee_candidate_to_work_position_competition_id'),
            'work_position_competition_juries' => array(self::BELONGS_TO, 'WorkPositionCompetitionJury', 'work_position_competition_juries_id'),
//            'person' => array(self::BELONGS_TO, 'Person', 'id'),
//            'school_course' => array(self::BELONGS_TO, 'SchoolCourse', 'school_course_id'),
        );
    }

    public function rules()
    {
        return array(
//            array('id', 'required', 'message' => 'Polje "{attribute}" mora biti uneto'),
//            array('grade, amount_bruto, amount_total', 'numerical', 'message' => 'Polje "{attribute}" mora biti broj!'),
            ['cv_grade, cv_text_grade, interview_grade, interview_text_grade', 'safe'],
            array('employee_candidate_to_work_position_competition_id, work_position_competition_juries_id', 'required'),
            ['employee_candidate_to_work_position_competition_id', 'checkUniq'],
            array('employee_candidate_to_work_position_competition_id, work_position_competition_juries_id', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => array('insert', 'update'))
        );
    }

    public function checkUniq()
    {
        if (
                intval($this->__old['employee_candidate_to_work_position_competition_id']) !== intval($this->employee_candidate_to_work_position_competition_id) ||
                intval($this->__old['work_position_competition_juries_id']) !== intval($this->work_position_competition_juries_id)
            )
        {
            $competition_grade = CompetitionGrade::model()->findByAttributes([
                'employee_candidate_to_work_position_competition_id' => $this->employee_candidate_to_work_position_competition_id,
                'work_position_competition_juries_id' => $this->work_position_competition_juries_id
            ]);
            if (!is_null($competition_grade))
            {
                $this->addError('employee_candidate_to_work_position_competition_id', Yii::t('HRModule.EmployeeCandidateToWorkPositionCompetition','CompetitionGradeUniq',[
                    '{competition_jury}'=>$this->work_position_competition_juries->jury->DisplayName,
                    '{competition_candidate}'=>$this->employee_candidate_to_work_position_competition->employee_candidate->person->DisplayName
                ]));
            }
        }
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'options': 
                if ($this->work_position_competition_juries->jury_id == Yii::app()->user->id)
                {
                    return ['form'];
                }
                else
                {
                    return [];
                }
            case 'filters' : return array(
                'employee_candidate_to_work_position_competition' => 'relation',
                'work_position_competition_juries' => 'relation',
                'cv_grade' => 'integer',
                'interview_grade' => 'integer',
                'cv_text_grade' => 'text',
                'interview_text_grade' => 'text'
//                'work_position_competitions' => 'relation',
//                'employee_candidate_to_work_position_competition' => 'relation',
//                'note' => 'text',
//                'interview_time' => 'date_range',
//                'status_id' => 'dropdown'
            );
            case 'update_relations': return [
                'employee_candidate_to_work_position_competition'
            ];
////            case 'textSearch' : return array();
//            case 'options' : return array('delete','form','open');
//            case 'mutual_forms': return [
//                'base_relation' => 'person'
//            ];
//            case 'form_list_relations': return array('work_position_competitions');
//////            case 'order_by' : return array('amount_neto, amount_bruto, amount_total');
            default: return parent::modelSettings($column);
        }
    }
    
    public function afterSave()
    {
        parent::afterSave();
        
        if ($this->isNewRecord || $this->columnChanged('cv_grade'))
        {
            Yii::app()->warnManager->sendWarns($this->work_position_competition_juries->jury_id, 'WARN_NOT_REVIEWED_EMPLOYEE_CANDIDATES');
        }
    }
    
    public function afterDelete()
    {
        parent::afterDelete();
        
        Yii::app()->warnManager->sendWarns($this->work_position_competition_juries->jury_id, 'WARN_NOT_REVIEWED_EMPLOYEE_CANDIDATES');
    }
}