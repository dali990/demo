<?php

/**
 * @package SIMA
 * @subpackage Legal
 */
class CompanyLicenseToReference extends SIMAActiveRecord
{   

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'hr.company_licenses_to_jobs_references';
    }

    public function moduleName()
    {
        return 'HR';
    }

    public function __get($column)
    {
        switch ($column)
        {
            default: return parent::__get($column);
        }
    }

    public function rules()
    {
        return array(
            array('company_license_id, reference_id', 'required'),
            array('company_license_id, reference_id', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => array('insert', 'update'))
        );
    }

    public function relations($child_relations = [])
    {
        return array(
            'company_license' => array(self::BELONGS_TO, 'CompanyLicenseToCompany', 'company_license_id'),
            'reference' => array(self::BELONGS_TO, 'Reference', 'reference_id'),
        );
    }

    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters' : return array(
                'company_license' => 'relation',
                'reference' => 'relation',
            );
            case 'update_relations':  return array(
                'reference','company_license'
            );
            default : return parent::modelSettings($column);
        }
    }
    
}
