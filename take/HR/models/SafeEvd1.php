<?php

/**
 * Description of SafeEvd1
 *
 * @author goran
 */
class SafeEvd1 extends SIMAActiveRecord
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'hr.safe_evd_1';
    }

    public function moduleName()
    {
        return 'HR';
    }

    public function __get($column)
    {
        switch ($column)
        {
            case 'count_employees': return $this->work_position->number_person;
            case 'SearchName': return $this->id;
            default: return parent::__get($column);
        }
    }

    public function rules()
    {
        return array(
            array('work_danger_id, work_position_id', 'required'),
            array('comment', 'safe'),
            array('work_danger_id, work_position_id', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => array('insert', 'update'))
        );
    }

    public function relations($child_relations = [])
    {
        return array(
            'work_danger' => array(self::BELONGS_TO, 'SafeWorkDanger', 'work_danger_id'),
            'work_position' => array(self::BELONGS_TO, 'WorkPosition', 'work_position_id'),
        );
    }

    public function scopes()
    {
        $alias = $this->getTableAlias(FALSE, FALSE);

        return array(
            'byName' => array('order' => 'id'),
            'forReportDocument' => array(
                'select' => "distinct 
                    rank() over() as id , 
                    work_position_id, 
                    array_agg( swd.code::text)  as work_danger_id, 
                    array_to_string(array_agg( case  when $alias.comment <> '' and $alias.comment is not null then $alias.comment end ),', ' ) as comment ",
                'join' => "left join hr.safe_work_dangers swd on $alias.work_danger_id=swd.id",
                'group' => 'work_position_id',
                'order' => 'work_position_id'
            ),
        );
    }

    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters' : return [
                    'work_position' => 'relation',
                    'work_danger' => 'relation',
                    'comment' => 'text'
                ];
            case 'options': return ['delete', 'form'];
            case 'update_relations': return ['work_position'];
            default: return parent::modelSettings($column);
        }
    }

}
