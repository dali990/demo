<?php

/**
 * @package SIMA
 * @subpackage Legal
 */
class WorkLicense extends SIMAActiveRecord
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'hr.work_licenses';
    }

    public function moduleName()
    {
        return 'HR';
    }

    public function __get($column)
    {
        switch ($column)
        {
            case 'DisplayName': return $this->code.' - '.$this->name;
            case 'SearchName': return $this->code.' - '.$this->name;
            case 'all_persons_count': return count($this->all_persons);
            case 'all_employees_count':return count($this->all_employees);
            default: return parent::__get($column);
        }
    }

    public function rules()
    {
        return array(
            array('name', 'required'),
            array('description,code, id,conditions', 'safe'),
            array('issuer_id', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => array('insert', 'update'))
        );
    }

    public function relations($child_relations = [])
    {
//        $id=  SIMAHtml::uniqid();
        return array(
            'issuer' => array(self::BELONGS_TO, 'Company', 'issuer_id'),
            'all_persons'=> array(self::HAS_MANY, 'PersonToWorkLicense', 'working_license_id'),
            'all_employees'=> array(self::HAS_MANY, 'PersonToWorkLicense', 'working_license_id',
                'condition'=>"person_id in (select empl.id from employees empl where person_id=empl.id)"
                ),
            'iks_points'=>array(self::MANY_MANY, 'IKSPoint', 'hr.iks_points_to_work_licenses(work_license_id,iks_point_id)'),
        );
    }

    public function scopes()
    {
        $persons_to_working_licences_table_name = PersonToWorkLicense::model()->tableName();
        $working_licence_certificates_table_name = WorkingLicenceCertificate::model()->tableName();

        return array(
            'byName' => array('order' => 'issuer_id, code'),
            'licensesExpire'=>array(
                'select'=>'*',
                'join'=>"left join $persons_to_working_licences_table_name ptwl on t.id=ptwl.working_license_id left join $working_licence_certificates_table_name wlc on ptwl.id=wlc.licence_id",
                'condition'=>"ptwl.id not in (select licence_id from $working_licence_certificates_table_name where (now()+'5days'::interval)<end_date)"
            )
        );
    }

    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters': return array_merge(parent::modelSettings($column),array(
                    'code' => 'text',
                    'name' => 'text',
                    'conditions' => 'text',
                    'description' => 'text',
                    'all_persons' => 'relation',
                    'all_employees' => 'relation',
                    'issuer'=>'relation'
                ));
            case 'textSearch': return array(
                    'code' => 'text',
                    'name' => 'text',
                    'conditions' => 'text',
                    'description' => 'text'
                );
            default: return parent::modelSettings($column);
        }
    }

}
