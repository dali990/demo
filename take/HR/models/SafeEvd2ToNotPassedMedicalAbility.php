<?php

class SafeEvd2ToNotPassedMedicalAbility extends SIMAActiveRecord
{   

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'hr.safe_evd2_to_not_passed_medical_abilities';
    }

    public function moduleName()
    {
        return 'HR';
    }

    public function rules()
    {
        return array(
            array('medical_ability_id, safe_evd2_id', 'required'),
            array('medical_ability_id, safe_evd2_id', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => array('insert', 'update'))
        );
    }

    public function relations($child_relations = [])
    {
        return array(
            'medical_ability' => array(self::BELONGS_TO, 'RequiredMedicalAbility', 'medical_ability_id'),
            'safe_evd2' => array(self::BELONGS_TO, 'SafeEvd2', 'safe_evd2_id'),
        );
    }

    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters' : return array(
                'medical_ability' => 'relation',
                'safe_evd2' => 'relation'
            );
            case 'options' : return array();            
            default : return parent::modelSettings($column);
        }
    }
    
}
