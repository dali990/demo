<?php

/**
 * Description of CloseWorkContract
 *
 * @author goran
 */
class CloseWorkContract extends SIMAActiveRecord
{

    public $document_type_id;
    public $generate_close_work_contract_template; //raskid ugovora o radu

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'hr.close_work_contracts';
    }

    public function moduleName()
    {
        return 'HR';
    }

    public function __get($column)
    {
        switch ($column)
        {
            case 'path2': return 'files/file';
            case 'DisplayName': return $this->end_date;
            case 'SearchName': return $this->file->DisplayName;
            default: return parent::__get($column);
        }
    }

    public function rules()
    {
        return array(
            array('end_date, work_contract_id', 'required'),
            array('id, end_date, work_contract_id,document_type_id,description', 'safe'),
            array('id,work_contract_id', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => array('insert', 'update'))
        );
    }

    public function relations($child_relations = [])
    {
        return array(
            'file' => array(self::BELONGS_TO, 'File', 'id'),
            'work_contract' => array(self::BELONGS_TO, 'WorkContract', 'work_contract_id')
        );
    }

    public function scopes()
    {
        return array(
            'byName' => array('order' => 'id'),
        );
    }
    
    protected function modelOptions(User $user = null):array
    {
        return ['open','form','delete'];
    }
    

    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'mutual_forms': return array(
                'base_relation'=>'file',
            );
            default: return parent::modelSettings($column);
        }
    }

    public function BeforeSave()
    {

        return parent::BeforeSave();
    }

    public function afterSave()
    {
        $this->work_contract->save();
        return parent::afterSave();
    }

}
