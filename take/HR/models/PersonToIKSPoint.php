<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class PersonToIKSPoint extends SIMAActiveRecord
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
 
    public function tableName()
    {
        return 'hr.persons_to_iks_points';
    }
    
    public function moduleName()
    {
        return 'HR';
    }
    
    public function primaryKey() {
        return array('person_id', 'iks_point_id');
    }
    
    public function relations($child_relations = []) {
        return array(
            'person' => array(self::BELONGS_TO, 'Person', 'person_id'),
            'iks_point' => array(self::BELONGS_TO, 'IKSPoint', 'iks_point_id'),
        );
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters': return array_merge(parent::modelSettings($column),array(
                    'person_id' => 'dropdown',
                    'iks_point_id' => 'dropdown'
                ));
            default: return parent::modelSettings($column);
        }
    }
}
