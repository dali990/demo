<?php

class SafeEvd13 extends SIMAActiveRecord
{
    public $file_version_id;
    public $file_id;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'hr.safe_evd_13';
    }

    public function moduleName()
    {
        return 'HR';
    }

    public function __get($column)
    {
        switch ($column)
        {
            case 'path2': return 'files/file';
            case 'DisplayName': return isset($this->file)?$this->file->DisplayName:'';
            case 'SearchName': return $this->DisplayName;
            default: return parent::__get($column);
        }
    }

    public function rules()
    {
        return array(
            array('person_id, work_position_id', 'required'),
            array('report_code, report_date, next_report_date, medical_referral_id, actions, description', 'safe'),
            array('person_id, work_position_id, medical_referral_id', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => array('insert', 'update'))            
        );
    }

    public function relations($child_relations = [])
    {
//        $safe_evd2_to_not_passed_medical_abilities = SafeEvd2ToNotPassedMedicalAbility::model()->tableName();
        return array(
            'file' => array(self::BELONGS_TO, 'File', 'id'),
            'medical_referral' => array(self::BELONGS_TO, 'MedicalExaminationReferral', 'medical_referral_id'),
            'person' => array(self::BELONGS_TO, 'Person', 'person_id'),
            'work_position' => array(self::BELONGS_TO, 'WorkPosition', 'work_position_id')            
        );
    }

    public function scopes()
    {
        $uniq = SIMAHtml::uniqid();
        $alias = $this->getTableAlias();        
        $persons_table_name = Person::model()->tableName();

        return array(
            'byName' => [
                'select'=>"$alias.*, persons$uniq.lastname, persons$uniq.firstname",
                'join'=>"join $persons_table_name persons$uniq on persons$uniq.id=$alias.person_id",
                'order'=>"persons$uniq.lastname ASC, persons$uniq.firstname ASC"
            ],
            'withoutMedicalReferral' => [
                'condition' => "$alias.medical_referral_id is null"
            ]
        );
    }

    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters' : return array_merge(parent::modelSettings($column), array(
                'person' => 'relation',
                'work_position' => 'relation',
                'report_code' => 'text',
                'report_date' => 'date_range',
                'next_report_date' => 'date_range',
                'medical_referral' => 'relation',
                'actions' => 'text',
                'description' => 'text'
            ));
            case 'textSearch' : return array(
                'person' => 'relation',
                'work_position' => 'relation'
            );
//            case 'options' : return array('open', 'form', 'delete', 'download');
            case 'mutual_forms' : return array(
                'base_relation' => 'file'
            );            
            case 'GuiTable': return array(
                'scopes' => array('byName')
            );
            default: return parent::modelSettings($column);
        }
    }
    
    public function beforeSave()
    {
        if (empty($this->id) && empty($this->file))
        {            
            $file = new File();
            $file->name = $this->getSafeEvd13Filename();
            if (Yii::app()->isWebApplication())
            {
                $file->responsible_id = Yii::app()->user->id;
            }
            $file->save();
            $this->id = $file->id;
            $this->file = $file;
        }

        return parent::beforeSave();
    }
    
    public function afterSave()
    {
        parent::afterSave();

        if ($this->isNewRecord)
        {
            $this->file->name = $this->getSafeEvd13Filename();
            $this->file->save();
        }
    }
    
    private function getSafeEvd13Filename()
    {
        return Yii::t('HRModule.SafeEvd2', 'MedicalReportFilename', [
            '{report_code}' => !empty($this->report_code) ? "($this->report_code)" : '',
            '{person_name}' => $this->person->DisplayName
        ]);
    }
}
