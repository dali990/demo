<?php

class IntroductionIntoBusinessProgram extends SIMAActiveRecord
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
 
    public function tableName()
    {
       return 'hr.introduction_into_business_programs';
    }
    
    public function moduleName() 
    {
        return 'HR';
    }
    
    public function __get($column)
    {
        switch ($column)
        {
            case 'DisplayName':
                $result = $this->number.'/';
                if(isset($this->work_contract))
                {
                    $result .= (new DateTime($this->work_contract->start_date))->format('Y');
                }
                else
                {
                    $result .= 'NULL';
                }
                return $result;
            default: return parent::__get($column);
        }
    }
    
//    public function scopes()
//    {
//        $alias = $this->getTableAlias();
//        return [
//            'byName' => [
//                'order' => $alias.'.begin DESC'
//            ],
//            'byBeginDateAscOrder' => [
//                'order' => $alias.'.begin ASC'
//            ]
//        ];
//    }
    
    public function rules()
    {
        return array(
            ['year', 'required'],
            ['number', 'safe'],
            ['number', 'checkNumberYear']
        );
    }
    
    public function checkNumberYear()
    {
        if(!empty($this->year) && !empty($this->number))
        {
            $is_unique = true;
            
            $criteria = new SIMADbCriteria([
                'Model' => IntroductionIntoBusinessProgram::model(),
                'model_filter' => [
                    'number' => $this->number,
                    'year' => $this->year
                ]
            ], true);
            
            if ($this->getIsNewRecord())
            {
                if(!empty($criteria->ids))
                {
                    $is_unique = false;
                }
            }
            else
            {
                $introductions = IntroductionIntoBusinessProgram::model()->findAll($criteria);
                foreach($introductions as $introduction)
                {
                    if(intval($introduction->id) !== intval($this->id))
                    {
                        $is_unique = false;
                        break;
                    }
                }
            }
            
            if($is_unique === false)
            {
                $this->addError('begin', Yii::t('HRModule.IntroductionIntoBusinessProgram', 'NumberYearIsNotUnique', [
                    '{number}' => $this->number,
                    '{year}' => $this->year
                ]));
            }
        }
    }
    
    public function relations($child_relations = [])
    {
        return array(
            'file' => array(self::BELONGS_TO, 'File', 'id'),
            'work_contract' => array(self::HAS_ONE, 'WorkContract', 'introduction_into_business_program_id')
//            'employee' => array(self::BELONGS_TO, 'Employee', 'employee_id'),
//            'confirmed_user' => array(self::BELONGS_TO, 'Employee', 'confirmed_user_id'),
//            'comment_thread'=>array(self::BELONGS_TO,'CommentThread', 'comment_thread_id'),
//            'document'=>array(self::BELONGS_TO, 'File', 'document_id'),
//            'year' => array(self::BELONGS_TO, 'Year', 'year_id')
        );
    }  
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'options': return [];
            case 'filters': return [
                'number' => 'integer',
                'year' => 'integer'
            ];
            default: return parent::modelSettings($column);
        }
    }
    
    public function beforeSave()
    {
        if ($this->getIsNewRecord())
        {
            $this->number = $this->getNextNumber();
            
            $user_id = null;
            if(Yii::app()->isWebApplication())
            {
                $user_id = Yii::app()->user->id;
            }
            
            $document_type_id = Yii::app()->configManager->get('HR.introduction_into_business_program_document_type', false);
            
            $file = new File();
            $file->document_type_id = $document_type_id;
            $file->responsible_id = $user_id;
            $file->save();
            $file->refresh();
        
            $this->id = $file->id;
            $this->file = $file;
        }
        
        return parent::beforeSave();
    }
    
    public function afterSave()
    {        
        return parent::afterSave();
    }
    
    public function generateTemplate()
    {
        $file = $this->file;

        $template_id = Yii::app()->configManager->get('HR.introduction_into_business_program_template', false);
        if (empty($template_id))
        {
            throw new SIMAWarnException(Yii::t('HRModule.IntroductionIntoBusinessProgram', 'IntroductionIntoBusinessProgramTemplateConfigurationNotSet'));
        }
        
        $template_file_id = TemplateController::setTemplate($file->id, $template_id);
        TemplateController::addVersion($template_file_id);
    }
    
    private function getNextNumber()
    {
        $max_num_row = IntroductionIntoBusinessProgram::model()->find([
            'select' => 'max(number) as number',
            'condition' => 'year='.$this->year
        ]);
        $result = $max_num_row->number+1;
        return $result;
    }
}
