<?php

class NationalEventStateHolidayToDay extends NationalEventToDayYear
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    
    public function defaultScope()
    {
        $uniq = SIMAHtml::uniqid();
        $alias = $this->getTableAlias(FALSE, FALSE);
        $nationalEventsTable = NationalEvent::model()->tableName();
        $type = NationalEvent::$TYPE_STATE_HOLIDAY;
        
        return array(
            'condition' => "EXISTS ("
                . "SELECT 1 "
                . "FROM $nationalEventsTable net$uniq "
                . "WHERE $alias.national_event_id=net$uniq.id AND type='$type' "
                . "LIMIT 1"
            . ")",
        );
    }
}

