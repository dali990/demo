<?php

/**
 * @package SIMA
 * @subpackage HR
 */
class WorkPositionCompetition extends SIMAActiveRecord
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'hr.work_position_competitions';
    }

    public function moduleName()
    {
        return 'HR';
    }
    
    public function __get($column)
    {
        switch ($column)
        {
            case 'DisplayName': return $this->name;
            default: return parent::__get($column);
        }
    }
    
    public function getis_graded()
    {
        $graded = true;
        foreach ($this->work_position_competition_juries as $jury)
        {
            if ( ($this->employee_candidate_count - $jury->competition_grades_count) > 0)
            {
                $graded = false;
            }
        }
        return $graded;
    }

    public function relations($child_relations = [])
    {
        return array(
            'employee_candidates' => array(self::MANY_MANY, 'EmployeeCandidate', 
                'hr.employee_candidate_to_work_position_competition(work_position_competition_id,employee_candidate_id)'
            ),
            'juries' => array(self::MANY_MANY, 'User', 
                'hr.work_position_competition_juries(work_position_competition_id,jury_id)'
            ),
            'work_position_competition_juries' => array(self::HAS_MANY, 'WorkPositionCompetitionJury', 'work_position_competition_id'),
            'employee_candidate_to_work_position_competition' => array(self::HAS_MANY, 'EmployeeCandidateToWorkPositionCompetition', 'work_position_competition_id'),
            'employee_candidate_count' => array(self::STAT, 'EmployeeCandidateToWorkPositionCompetition', 'work_position_competition_id',
                'select' => 'count(*)'
            ),
            'comment_thread' => array(self::BELONGS_TO, 'CommentThread', 'comment_thread_id'),
//            'paycheck_period' => array(self::BELONGS_TO, 'PaycheckPeriod', 'paycheck_period_id'), 
        );
    }

    public function rules()
    {
        return array(
            array('name, apply_date', 'required', 'message' => 'Polje "{attribute}" mora biti uneto'),
//            array('amount_neto, amount_bruto, amount_total', 'numerical', 'message' => 'Polje "{attribute}" mora biti broj!'),
            array('active, description, comment_thread_id', 'safe'),
//            array('paycheck_period_id', 'default',
//                'value' => null,
//                'setOnEmpty' => true, 'on' => array('insert', 'update'))
        );
    }
    
    public function scopes()
    {
        $alias = $this->getTableAlias();
        return [
            'byDateName' => [
                'order' => "$alias.apply_date DESC, $alias.name"
            ],
            'onlyExpired' => [
                'condition' => "$alias.apply_date < now()"
            ]
        ];
    }
            

    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters' : return [
                'work_position_competition_juries' => 'relation',
                'juries' => 'relation',
                'employee_candidate_to_work_position_competition' => 'relation',
                'active' => 'boolean',
                'name' => 'text',
                'apply_date' => 'date_range',
                'description' => 'text'
            ];
            case 'textSearch' : return [
                'name' => 'text'
            ];
            case 'options' : return ['open', 'form', 'delete'];
            case 'default_order_scope' : return 'byDateName';
            default: return parent::modelSettings($column);
        }
    }
    
    public function notGradedByUser($user_id)
    {
        $alias = $this->getTableAlias();
        $uniq = SIMAHtml::uniqid();

        $competition_grades_table = CompetitionGrade::model()->tableName();
        $work_position_competition_juries_table = WorkPositionCompetitionJury::model()->tableName();
        $employee_candidate_to_work_position_competitions_table = EmployeeCandidateToWorkPositionCompetition::model()->tableName();
        $work_position_competitions_table = WorkPositionCompetition::model()->tableName();
        
        $this->getDbCriteria()->mergeWith([
            'condition' => "
                (
                    select count(*) from $employee_candidate_to_work_position_competitions_table ectwpc$uniq 
                    left join $work_position_competitions_table wpc$uniq on wpc$uniq.id=ectwpc$uniq.work_position_competition_id 
                    left join $work_position_competition_juries_table wpcj$uniq on wpcj$uniq.work_position_competition_id=wpc$uniq.id 
                    where ectwpc$uniq.work_position_competition_id = $alias.id and wpcj$uniq.jury_id=:param1$uniq
                )
                !=
                (
                    select count(*) from $competition_grades_table cg$uniq 
                    left join $work_position_competition_juries_table wpcj$uniq on wpcj$uniq.id=cg$uniq.work_position_competition_juries_id 
                    where wpcj$uniq.work_position_competition_id=$alias.id and wpcj$uniq.jury_id=:param1$uniq
                )
                ",
            'params'=>[
                ":param1$uniq"=>$user_id
            ]
        ]);
        
        return $this;
    }
    
    public function afterSave()
    {
        parent::afterSave();
        
        if ($this->columnChanged('active'))
        {
            foreach ($this->juries as $jury)
            {
                Yii::app()->warnManager->sendWarns($jury->id, 'WARN_NOT_REVIEWED_EMPLOYEE_CANDIDATES');
            }
        }
    }
}