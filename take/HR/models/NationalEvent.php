<?php

class NationalEvent extends SIMAActiveRecord
{
    static public $TYPE_STATE_HOLIDAY = 'STATE_HOLIDAY';
    static public $TYPE_RELIGIOUS_HOLIDAY = 'RELIGIOUS_HOLIDAY';
    static public $TYPE_SLAVA = 'SLAVA';
    
    public $_day_date = null;
    
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'hr.national_events';
    }

    public function moduleName()
    {
        return 'HR';
    }
    
    public function rules()
    {
        return array(
            array('name, type', 'required'),
            array('date, _day_date', 'safe'),
            array('date', 'checkDate', 'on' => ['insert', 'update']),
            array('type', 'checkType')
        );
    }
    
    public function __get($column)
    {
        switch ($column)
        {
            case 'DisplayName': return $this->name;
            case 'SearchName': return $this->DisplayName;
            default: return parent::__get($column);
        }
    }
    
    public function checkType()
    {
        if(!$this->isNewRecord 
            && $this->type !== $this->__old['type']
            && $this->__old['type'] === NationalEvent::$TYPE_SLAVA)
        {            
            $criteria = new SIMADbCriteria([
                'Model' => 'Employee',
                'model_filter' => [
                    'slava' => [
                        'ids' => $this->id
                    ]
                ]
            ], true);
                        
            if(!empty($criteria->ids))
            {
                $this->addError('type', Yii::t('HRModule.NationalEvent', 'EmployeesHaveThisSlava'));
            }
        }
    }
    
    public function checkDate()
    {        
        if(!empty($this->date))
        {
            if (!preg_match("/^(0[1-9]|[1-2][0-9]|3[0-1])\.(0[1-9]|1[0-2])\.$/", $this->date)) 
            {
                $this->addError('date', Yii::t('HRModule.NationalEvent', 'DateInvalidFormat'));
            }
        }
        if(!empty($this->_day_date))
        {
            if (!preg_match("/^(0[1-9]|[1-2][0-9]|3[0-1])\.(0[1-9]|1[0-2])\.([0-9]{3,4}\.)?$/", $this->_day_date)) 
            {
                $this->addError('_day_date', Yii::t('HRModule.NationalEvent', 'DateInvalidFormat'));
            }
        }
    }
    
    public function relations($child_relations = [])
    {
        return array(
            'national_event_to_days' => array(self::HAS_MANY, 'NationalEventToDayYear', 'national_event_id')
        );
    }
    
    public function scopes()
    {
        return [
            'orderByDateASC'=>[
                'order'=>'date ASC'
            ],
        ];
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters' : return array(
                'name' => 'text',
                'date' => 'text',
                'type' => 'dropdown',
            );
            case 'textSearch' : return array(
                'name' => 'text'
            );
            case 'options': return ['form', 'delete'];
            default: return parent::modelSettings($column);
        }
    }
    
    public function withSetDate($invert=false)
    {
        $alias = $this->getTableAlias();
        
        $condition = $alias.'.date IS NOT NULL AND '.$alias.'.date!=\'\'';
        if($invert === true)
        {
            $condition = $alias.'.date IS NULL OR '.$alias.'.date=\'\'';
        }
        
        $this->getDbCriteria()->mergeWith([
            'condition' => $condition
        ]);
        return $this;
    }
    
    public function unusedInCurrentYear(Year $yearModel, $invert=false)
    {
        $uniqid = SIMAHtml::uniqid();
        $netdyTable = NationalEventToDayYear::model()->tableName();
        $alias = $this->getTableAlias();
        
        $not_str = 'not';
        if($invert === true)
        {
            $not_str = '';
        }
        
        $this->getDbCriteria()->mergeWith([
            'condition' => $not_str.' exists ('
                .'SELECT 1 '
                .'FROM '.$netdyTable.' netdy'.$uniqid.' '
                .'WHERE netdy'.$uniqid.'.national_event_id='.$alias.'.id '
                    .'AND netdy'.$uniqid.'.year_id='.$yearModel->id.' LIMIT 1)'
        ]);
        return $this;
    }
    
    public function afterFind()
    {
        $this->_day_date = $this->date;
        if(!empty($this->_day_date))
        {
            $this->_day_date .= date("Y").'.';
        }
        return parent::afterFind();
    }
    
    public function BeforeSave()
    {            
        if(empty($this->_day_date))
        {
            $this->date = $this->_day_date;
        }
        else
        {
            $date_matches = null;
            preg_match("/^(0[1-9]|[1-2][0-9]|3[0-1])\.(0[1-9]|1[0-2])\./", $this->_day_date, $date_matches);
            if(is_null($date_matches) || count($date_matches) === 0)
            {
                throw new Exception(Yii::t('HRModule.NationalEvent', 'DateInvalidFormat'));
            }
            $this->date = $date_matches[0];
        }
        
        return parent::BeforeSave();
    }
    
    public function getTypesDisplayArray()
    {
        $type_displays = [
            NationalEvent::$TYPE_STATE_HOLIDAY => Yii::t('HRModule.NationalEvent', 'StateHoliday'),
            NationalEvent::$TYPE_RELIGIOUS_HOLIDAY => Yii::t('HRModule.NationalEvent', 'ReligiousHoliday'),
            NationalEvent::$TYPE_SLAVA => Yii::t('HRModule.NationalEvent', 'Slava'),
        ];
        return $type_displays;
    }
    
    public function getClasses()
    {        
        return parent::getClasses() .
                ((!$this->IsUsedInCurrentYear) ? ' color_98F5FF ' : '');
    }
    
    public function getIsUsedInCurrentYear()
    {        
        $criteria = new SIMADbCriteria([
            'Model' => NationalEventToDayYear::model(),
            'model_filter' => [
                'national_event' => [
                    'ids' => $this->id
                ],
                'year' => [
                    'year' => date('Y')
                ]
            ]
        ], true);
        
        return !empty($criteria->ids);
    }
    
    public function getIsOnDay(Day $day)
    {
        $criteria = new SIMADbCriteria([
            'Model' => NationalEventToDayYear::model(),
            'model_filter' => [
                'national_event' => [
                    'ids' => $this->id
                ],
                'day' => [
                    'ids' => $day->id
                ]
            ]
        ], true);
        
        return !empty($criteria->ids);
    }
}

