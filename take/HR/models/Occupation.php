<?php

class Occupation extends SIMAActiveRecord
{    
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
    
    public function tableName()
    {
        return 'hr.occupations';
    }
    public function moduleName()
    {
        return 'HR';
    }
    
    public function __get($column)
    {
        switch ($column)
        {           
            case 'DisplayName': return $this->name;
            case 'SearchName': return $this->name;

            default: return parent::__get($column);
        }
    }
    
    public function rules()
    {
        return [
            ['name, code', 'required'],
            ['code', 'unique'],
            ['code','length', 'max'=>16],
            ['parent_id', 'safe']
        ];
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters' : return [
                'name' => ['text', 'use_cyrillic' => true],
                'code' => 'text',
                'parent' => 'relation',
                'occupation_to_work_positions' => 'relation'
            ];
            case 'textSearch' : return [
                'name' => ['text', 'use_cyrillic' => true]
            ];
            default: return parent::modelSettings($column);
        }
    }
    
    public function relations($child_relations = [])
    {
        return parent::relations(array_merge([
            'parent' => [self::BELONGS_TO, 'Occupation', 'parent_id'],
            'occupation_to_work_positions' => [self::HAS_MANY, WorkPositionToOccupation::class, 'occupation_id'],
        ], $child_relations));
    }
    
    public function afterValidate()
    {
        if($this->getScenario() == 'delete')
        {
            $occupations = Occupation::model()->findAllByAttributes(['parent_id' => $this->id]);
            if(count($occupations) > 0)
            {
                $msg = Yii::t('HRModule.Occupation', 'ToDeleteOccupationDeleteTheFollowingSubtypes');
                foreach ($occupations as $occupation) 
                {
                    $msg .= $occupation->name."<br>";
                }
                throw new SIMAWarnException($msg);
            }
        }
        return parent::afterValidate();
    }
    
    public function withoutUniqueFields($codebook_unique_fields)
    {
        $alias = $this->getTableAlias();
        $condition = "";
        if (!empty($codebook_unique_fields))
        {           
            $unique_fields_packages = "'".implode("','", $codebook_unique_fields)."'";
            
            $condition = $alias.".code not in ($unique_fields_packages)";
        }
        $criteria = new CDbCriteria();
        $criteria->condition = $condition;
        $this->getDbCriteria()->mergeWith($criteria);
        return $this;
    }
}
