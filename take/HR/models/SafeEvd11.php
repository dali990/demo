<?php

/**
 * Description of SafeEvd11
 *
 * @author goran
 */
class SafeEvd11 extends SafeEvd11_12_14
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function BeforeSave()
    {
        $this->type_evd = '11';

        return parent::BeforeSave();
    }

    public function defaultScope()
    {
        return array(
            'condition' => 'type_evd=11',
        );
    }

}
