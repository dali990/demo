<?php

class EntryDoorCard extends SIMAActiveRecord
{
    public $ocupied = null;
    public $card_family_number_exact = null;
    
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
 
    public function tableName()
    {
       return 'hr.entry_door_cards';
    }
    
    public function moduleName() 
    {
        return 'HR';
    }
    
    public function __get($column)
    {
        switch ($column)
        {
            case 'DisplayName': 
            case 'SearchName': return $this->card_number;
            default: return parent::__get($column);
        }
    }
    
    public function rules()
    {
        return [
            ['card_number', 'required'],
            ['partner_id, comment', 'safe'],
            ['card_number', 'checkCardNumber','on'=>['insert','update']]
        ];
    }
    
    public function checkCardNumber()
    {
        if(!empty($this->card_number))
        {
            try
            {
                Yii::app()->entryDoorLog->validateCardNumber($this->card_number);
            }
            catch(Exception $e)
            {
                $this->addError('card_number', $e->getMessage());
            }
        }
    }
    
    public function relations($child_relations = [])
    {
        return [
            'partner' => [self::BELONGS_TO, 'Partner', 'partner_id'],
            'confirmed_user' => [self::BELONGS_TO, 'User', 'confirmed_user_id']
        ];
    }
    
    public function scopes()
    {
        $alias = $this->getTableAlias();
        return [
            'orderByCardNumberASC' => [
                'order' => $alias.'.card_number ASC'
            ],
            'confirmed' => [
                'condition' => $alias.'.confirmed=TRUE'
            ],
            'notConfirmed' => [
                'condition' => $alias.'.confirmed!=TRUE'
            ]
        ];
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters':
                return [
                    'card_number' => ['text'],
                    'partner' => 'relation',
                    'partner_id' => 'dropdown',
                    'ocupied' => ['dropdown', 'func' => 'filter_ocupied'],
                    'card_family_number_exact' => ['text', 'func' => 'filter_card_family_number_exact']
                ];
            case 'textSearch': return [
                'card_number' => ['text'],
                'partner' => 'relation',
            ];
            case 'options':
                return ['form','delete'];
            case 'statuses': return array(
                'confirmed' => array(
                    'title' => 'U upotrebi',
                    'timestamp' => 'confirmed_timestamp',
                    'user' => array('confirmed_user_id','relName'=>'confirmed_user'),
                    'checkAccessConfirm' => 'checkConfirmAccess',
                    'checkAccessRevert' => 'checkRevertAccess'
                ));
            default: return parent::modelSettings($column);
        }
    }
    
    function checkConfirmAccess()
    {
        return true;
    }
    function checkRevertAccess()
    {
        return true;
    }
    
    public function filter_ocupied($condition, $alias)
    {
        if(isset($this->ocupied))
        {
            $ocupied_bool = filter_var($this->ocupied, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
            
            $is_not = '';
            if($ocupied_bool === true)
            {
                $is_not = ' NOT ';
            }
            
            $temp_condition = new CDbCriteria();
            $temp_condition->condition = "$alias.partner_id IS $is_not NULL";
            $condition->mergeWith($temp_condition, true);
        }
    }
    
    public function filter_card_family_number_exact($condition, $alias)
    {
        if(isset($this->card_family_number_exact))
        {
            $condition->mergeWith([
                'condition' => "$alias.card_number='$this->card_family_number_exact'"
            ]);
        }
    }
    
    public function getlast_entry_log()
    {
        $criteria = new SIMADbCriteria([
            'Model' => EntryDoorLog::model(),
            'model_filter' => [
                'card' => [
                    'ids' => $this->id
                ],
                'order_by' => 'time DESC',
                'limit' => '1'
            ]
        ]);
        
        $entryDoorLog = EntryDoorLog::model()->find($criteria);
        
        return $entryDoorLog;
    }
}
