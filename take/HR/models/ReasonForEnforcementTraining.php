<?php

class ReasonForEnforcementTraining extends SIMAActiveRecord
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'hr.reasons_for_enforcement_training';
    }

    public function moduleName()
    {
        return 'HR';
    }

    public function __get($column)
    {
        switch ($column)
        {
            case 'DisplayName':
            case 'SearchName': return $this->name.'('.$this->code.')';
            default: return parent::__get($column);
        }
    }

    public function rules()
    {
        return array(
            array('name, code', 'required'),
            array('description', 'safe')
        );
    }

    public function relations($child_relations = [])
    {
        return array();
    }

    public function scopes()
    {
        $alias = $this->getTableAlias();
        return array(
            'byName' => array('order' => $alias . '.name ASC'),
        );
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters' : return array(
                    'code' => 'text',
                    'name' => 'text',
                    'description' => 'text'
                );
            case 'textSearch' : return array(
                    'code' => 'text',
                    'name' => 'text',
                    'description' => 'text'
                );
            default: return parent::modelSettings($column);
        }
    }

}
