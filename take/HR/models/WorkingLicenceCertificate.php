<?php
/**
 * Description of WorkingLicenceCertificate
 *
 * @author goran
 */
class WorkingLicenceCertificate extends SIMAActiveRecord
{
    public $file_version_id;
    public $file_id;
    
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'hr.working_licence_certificates';
    }

    public function moduleName()
    {
        return 'HR';
    }
        
    public function __get($column)
    {
        switch ($column)
        {
            case 'path2': return 'files/file';
            case 'DisplayName': return 'Potvrda_'.$this->license->DisplayName.'_od_'.$this->begin_date.'_do_'.$this->end_date;
            case 'SearchName': return $this->DisplayName;
            default: return parent::__get($column);
        }
    }

    public function rules()
    {
        return array(
            array('licence_id, begin_date, end_date', 'required'),
            array('file_version_id,licence_id, begin_date, end_date, description', 'safe'),
            array('licence_id', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => array('insert', 'update'))
        );
    }

    public function relations($child_relations = [])
    {
        return array(
            'file' => array(self::BELONGS_TO, 'File', 'id'),
            'license'=> array(self::BELONGS_TO, 'PersonToWorkLicense', 'licence_id')
        );
    }
    
    

    public function modelSettings($column)
    {
        switch ($column) 
        {
            case 'filters': return [
                'license' => 'relation',
                'end_date' => 'date_range',
            ];
            case 'options': return ['form','open'];
            default: return parent::modelSettings($column);
        }
        
    }
    
    public function BeforeSave()
    {
        
        if (!isset($this->file) && ($this->id == null || $this->id == ''))
        {
            $file = new File();
            $file->name = $this->DisplayName;
            $file->document_type_id=32;//HARDCODE
            $file->save();
            $file->refresh();
            $this->id = $file->id;
            $this->file = $file;
        }
        
        /** ubacivanje upload-a fajla */
        $this->file->appendUpload($this->file_version_id);
        
        return parent::BeforeSave();
    }
    
    public function afterSave()
    {
        parent::afterSave();
        
        Yii::app()->warnManager->sendWarns('HRLicenceWarning', 'WARN_LICENSE_NOCERTIFICATE');
    }
    
    public function getWorkingLicenceCertificatesForDateRange($begin_date, $end_date)
    {
        $filter = WorkingLicenceCertificate::filter(array(
            'end_date' => $begin_date.'<>'.$end_date
        ));
        $criteria = new SIMADbCriteria();
        $criteria->applyModel($filter);

        $working_licence_certificates = WorkingLicenceCertificate::model()->findAll($criteria);
        
        return $working_licence_certificates;
    }
}
