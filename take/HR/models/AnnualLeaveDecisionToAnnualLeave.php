<?php

class AnnualLeaveDecisionToAnnualLeave extends SIMAActiveRecord
{   

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'hr.annual_leave_decisions_to_annual_leaves';
    }

    public function moduleName()
    {
        return 'HR';
    }

    public function rules()
    {
        return array(
            array('annual_leave_decision_id, annual_leave_id, days_number', 'required'),
            array('annual_leave_decision_id, annual_leave_id', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => array('insert', 'update'))
        );
    }

    public function relations($child_relations = [])
    {
        return array(
            'annual_leave_decision' => array(self::BELONGS_TO, 'AnnualLeaveDecision', 'annual_leave_decision_id'),
            'annual_leave' => array(self::BELONGS_TO, 'AbsenceAnnualLeave', 'annual_leave_id')            
        );
    }

    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters' : return array(
                'annual_leave_decision' => 'relation',
                'annual_leave' => 'relation',
                'days_number' => 'integer'
            );
            default : return parent::modelSettings($column);
        }
    }
    
}
