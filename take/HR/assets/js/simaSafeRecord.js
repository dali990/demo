/* global sima */

function SIMASafeRecord()
{
    this.syncModeInjuriesFromCodebook = function(obj)
    {
        sima.ajaxLong.start('HR/safeRecord/getModeInjuryDiffBetweenLocalAndCodebook', {
            showProgressBar: $('body'),
            onEnd: function(response) {
                if (sima.isEmpty(response.html))
                {
                    sima.dialog.openInfo(sima.translate('NoDifferences'));
                }
                else
                {
                    sima.diffViewer.renderFromHtml(response.html, {
                        save_action: 'HR/safeRecord/saveDiffBetweenLocalAndCodebook',
                        after_save_func: function () {
                            var model = $(obj).parents('.sima-guitable:first');
                            model.simaGuiTable('populate');
                        }
                    });
                }
            }
        });
    };
    
    this.syncSourceInjuriesFromCodebook = function(obj)
    {
        sima.ajaxLong.start('HR/safeRecord/getSourceInjuryDiffBetweenLocalAndCodebook', {
            showProgressBar: $('body'),
            onEnd: function(response) {
                if (sima.isEmpty(response.html))
                {
                    sima.dialog.openInfo(sima.translate('NoDifferences'));
                }
                else
                {
                    sima.diffViewer.renderFromHtml(response.html, {
                        save_action: 'HR/safeRecord/saveDiffBetweenLocalAndCodebook',
                        after_save_func: function () {
                            var model = $(obj).parents('.sima-guitable:first');
                            model.simaGuiTable('populate');
                        }
                    });
                }
            }
        });
    };
}