/* global sima */

function SIMAEmployeeOfficeEntryLog()
{
    this.getNewEntryDoorLogs = function(_this)
    {
        sima.ajaxLong.start('HR/employeeOfficeEntryLog/getNewEntryDoorLogs', {
            showProgressBar:$('body'),
            onEnd: function(response) {
                var saved_count = response.saved_count;
                
                if(saved_count > 0)
                {
                    sima.guiTable.repopulateGuiTableForObj(_this);
                }
                
                sima.dialog.openInfo('Broj dodatih redova: '+saved_count);
            }
        });
    };
    
    this.getNonExistingEntryDoorLogCards = function(_this)
    {
        sima.ajax.get('HR/employeeOfficeEntryLog/getNonExistingEntryDoorLogCards',{
            success_function: function(response){
                var saved_count = response.saved_count;
                
                if(saved_count > 0)
                {
                    sima.guiTable.repopulateGuiTableForObj(_this);
                }
                
                sima.dialog.openInfo('Broj dodatih redova: '+saved_count);
            }
        });
    };
}

