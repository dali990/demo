
/* global sima */

function SIMAWorkContract()
{
    this.generateWorkContractTemplates = function(work_contract_id, template_type)
    {
        sima.ajax.get('HR/workContract/generateWorkContractTemplates', {
            get_params:{
                work_contract_id : work_contract_id,
                template_type : template_type
            },
            success_function: function()
            {
                sima.dialog.openInfo('Generisano!');
            }
        });
    };
    
    this.addWorkContractFilingNumber = function(file_id, file_init_data, filing_init_data)
    {
        sima.model.form('Filing',file_id,{
            init_data:{
                Filing: filing_init_data,
                File: file_init_data
            }
        });
    };
    
    this.workPositionOnBenefitedWorkExperienceHendler = function(form, dependent_on_field, dependent_field, apply_actions_func, additional_params)
    {
        var is_init = additional_params.is_init ? true : false;
        var work_position_row = form.find(".sima-model-form-row[data-column='work_position_id']");
        var svp_row = form.find(".sima-model-form-row[data-column='svp']");
        var work_position_val = work_position_row.find('.sima-ui-sf:first').simaSearchField('getValue'); 
    
        var benefited_work_experience = svp_row.simaSVPField('getValue').benefited_work_experience; 

        if (!sima.isEmpty(work_position_val.id))
        {
            sima.ajax.get('HR/workContract/getBenefitedWorkExperience', {
                get_params:{
                    work_position_id : work_position_val.id
                },
                success_function: function(response)
                {
                    if( response.benefited_work_experience !== benefited_work_experience )
                    {
                        svp_row.find('.sima-svp-field:first').simaSVPField('setValue', {
                            benefited_work_experience: response.benefited_work_experience
                        });
                        if(is_init === true)
                        {
                            sima.dialog.openWarn(response.benefited_mess);
                        }
                    }
                }
            });
        }
        else if (is_init === false)
        {
            svp_row.find('.sima-svp-field:first').simaSVPField('setValue', {
                benefited_work_experience: '0'
            });
        }
        svp_row.find('.benefited-work-experience:first').trigger('change');
    };
    
    this.filterOccupations = function(form, dependent_on_field, dependent_field, apply_actions_func, event){
        var filter = {
            'occupation_to_work_positions': {
                'work_position': {
                    ids: dependent_on_field.simaSearchField('getValue').id
                }
            }
        };
        dependent_field.simaSearchField('setFilter', filter);
    };
}
