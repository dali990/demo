/* global sima */

function SIMAHR()
{
    this.addPartnersToCompanyLicense = function(_this, company_license_to_company_id)
    {
        if (!_this.hasClass('_disabled'))
        {
            var wrap = _this.parents('.choose_persons_to_company_licence:first');
//            if (!wrap.find('.curr_references_number_wrap').hasClass('overflow') && !wrap.find('.curr_persons_number_wrap').hasClass('overflow'))
//            {
                var partner_ids = [];
                wrap.find('.body .choose_persons_to_company_licence_row').each(function(){
                    if ($(this).find('.add input').is(':checked'))
                    {
                        partner_ids.push($(this).data('id'));
                    }
                });
                sima.ajax.get('HR/WorkLicense/addPartnersToCompanyLicense', {
                    get_params:{
                        company_license_to_company_id : company_license_to_company_id                            
                    },
                    data:{partner_ids:partner_ids},
                    success_function: function(response)
                    {
                        var tab = _this.parents('.sima-ui-tabs:first');
                        tab.simaTabs('select_tab', tab.find('.sima-ui-tabs-nav .sima-ui-active').index()+1, true);
                    }
                });
//            }
//            else
//            {
//                sima.dialog.openInfo('Niste popunili broj osoba ili broj referenci!');
//            }
        }
    };
    
    this.removePartnersFromCompanyLicense = function(_this, company_license_to_company_id)
    {
        if (!_this.hasClass('_disabled'))
        {
            sima.ajax.get('HR/WorkLicense/removePartnersFromCompanyLicense',{
                get_params:{
                    company_license_to_company_id : company_license_to_company_id
                },
                success_function: function(response){
                    var tab = _this.parents('.sima-ui-tabs:first');
                    tab.simaTabs('select_tab', tab.find('.sima-ui-tabs-nav .sima-ui-active').index()+1, true);
                }
            });
        }
    };
        
//    this.generateC6Document = function(id)
//    {
//        sima.ajax.get('HR/safeRecord/generateC6Document',{
//            get_params:{
//                id : id,
//            },
//            success_function: function(response){
//                sima.dialog.openInfo('Generisano!');
//            }
//        });
//    }
    
    this.addWorkPositionRisks = function(work_position_id, work_position_risks_ids)
    {
        var params = {};
        params.view = "guiTable";
        params.multiselect = true;
        params.ids = work_position_risks_ids;
        sima.model.choose('SafeWorkDanger', function(data){
            sima.ajax.get('HR/safeRecord/addWorkPositionRisks', {
                get_params:{
                    work_position_id : work_position_id                   
                },
                data:{work_position_risks:data}
            });
        }, params);
    };
    
    this.addWorkPositionMedicalAbilities = function(work_position_id, work_position_medical_abilities_ids)
    {
        var params = {};
        params.view = "guiTable";
        params.multiselect = true;
        params.ids = work_position_medical_abilities_ids;
        sima.model.choose('RequiredMedicalAbility', function(data){
            sima.ajax.get('HR/safeRecord/addWorkPositionMedicalAbilities', {
                get_params:{
                    work_position_id : work_position_id                
                },
                data:{work_position_medical_abilities:data}
            });
        }, params);
    };
    
    this.generateMedicalReferral = function(safe_evd2_id, filter, medical_referral_id)
    {        
        if (sima.isEmpty(medical_referral_id))
        {
            sima.dialog.openChooseOption('Dodati kao:',[
                {
                    title: 'Novi uput',
                    function: function(){
                        sima.dialog.close();
                        addMedicalReferral(safe_evd2_id);
                    }
                },
                {
                    title: 'Izaberi postojeći uput',
                    function: function(){
                        sima.dialog.close();
                        var params = {};
                        params.view = "guiTable";
                        params.fixed_filter = filter;
                        params.params = {
                            add_button:{
                                init_data:{
                                    MedicalExaminationReferral:{
                                        referral_type:'SAFE_EVD_2'
                                    }
                                }
                            }
                        };
                        sima.model.choose('MedicalExaminationReferral', function(data){
                            if (data.length > 0)
                            {                
                                var medical_examination_referral_id = data[0].id;
                                addMedicalReferral(safe_evd2_id, medical_examination_referral_id);
                            }
                        }, params);
                    }
                }
            ]);
        }
        else
        {
            addMedicalReferral(safe_evd2_id, medical_referral_id);
        }
    };
    
    function addMedicalReferral(safe_evd2_id, medical_examination_referral_id)
    {        
        sima.ajax.get('HR/safeRecord/generateMedicalReferral', {
            get_params:{
                safe_evd2_id : safe_evd2_id,
                file_id : medical_examination_referral_id
            },
            success_function: function(response){
                sima.dialog.openInfo('Generisano!');
            }
        });
    }
    
    this.generateMedicalReferralForSafeEvd13 = function(safe_evd13_id, filter, medical_referral_id)
    {
        if (sima.isEmpty(medical_referral_id))
        {
            sima.dialog.openChooseOption('Dodati kao:',[
                {
                    title: 'Novi uput',
                    function: function(){
                        sima.dialog.close();
                        addMedicalReferralForSafeEvd13(safe_evd13_id);
                    }
                },
                {
                    title: 'Izaberi postojeći uput',
                    function: function(){
                        sima.dialog.close();
                        var params = {};
                        params.view = "guiTable";
                        params.fixed_filter = filter;
                        params.params = {
                            add_button:{
                                init_data:{
                                    MedicalExaminationReferral:{
                                        referral_type:'SAFE_EVD_13'
                                    }
                                }
                            }
                        };
                        sima.model.choose('MedicalExaminationReferral', function(data){
                            if (data.length > 0)
                            {                
                                var medical_examination_referral_id = data[0].id;
                                addMedicalReferralForSafeEvd13(safe_evd13_id, medical_examination_referral_id);
                            }
                        }, params);
                    }
                }
            ]);
        }
        else
        {
            addMedicalReferralForSafeEvd13(safe_evd13_id, medical_referral_id);
        }
    };
    
    function addMedicalReferralForSafeEvd13(safe_evd13_id, medical_examination_referral_id)
    {        
        sima.ajax.get('HR/safeRecord/generateMedicalReferralForSafeEvd13', {
            get_params:{
                safe_evd13_id : safe_evd13_id,
                file_id : medical_examination_referral_id
            },
            success_function: function(response){
                sima.dialog.openInfo('Generisano!');
            }
        });
    }
    
    this.generateSafeEvd6Report = function(id)
    {
        sima.ajax.get('HR/safeRecord/generateSafeEvd6Report', {
            get_params:{
                id : id                
            },
            success_function: function(response){
                sima.dialog.openInfo('Generisano!');
            }
        });
    };
    
    this.generateAbsenceTemplates = function(absence_id, template_type)
    {
        sima.ajax.get('HR/absence/generateAbsenceTemplates', {
            get_params:{
                absence_id : absence_id,
                template_type : template_type
            },
            success_function: function(response)
            {
                sima.dialog.openInfo('Generisano!');
            }
        });
    };
    
    this.addSickLeaveDocument = function(absence_id)
    {
        var params = {};
        params.view = "absence_sick_leave";
        sima.model.choose('File', function(data){
            var file_id = data[0].id;
            sima.ajax.get('HR/absence/addSickLeaveDocument', {
                get_params:{
                    absence_id : absence_id,
                    file_id : file_id
                },
                success_function: function(response)
                {
                }
            });
        }, params);
    };
    
    this.generateAnnualLeaveDecisionDocument = function(annual_leave_decision_id)
    {
        sima.ajax.get('HR/absence/generateAnnualLeaveDecisionDocument', {
            get_params:{
                annual_leave_decision_id : annual_leave_decision_id               
            },
            success_function: function(response)
            {
                sima.dialog.openInfo('Generisano!');
            }
        });
    };
    
    this.addAnnualLeaveDecisionFilingNumber = function(file_id, file_init_data, filing_init_data, update_tag)
    {
        sima.model.form('Filing',file_id,{
            init_data:{
                Filing: filing_init_data,
                File: file_init_data
            },
            onSave: function(){
                 sima.model.updateViews(update_tag);
            }
        });
    };
    
    this.autoFillNationalEventsForCurrentYear = function(table_id)
    {
        sima.ajax.get('HR/nationalEvents/autoFillForCurrentYear', {
            success_function: function()
            {
                $('#'+table_id).simaGuiTable('populate');
            }
        });
    };
    
    this.employeesToCollectiveAnnualLeavesChooser = function(collective_annual_leave_id, guitable_id)
    {
        var params = {};
        params.view = "guiTable";
        params.multiselect = true;
        params.params = {add_button:false};
        sima.model.choose('Employee', function(data){
            if (data.length > 0)
            {
                var ids = [];
                var data_length = data.length;
                for ( var i = 0; i < data_length; i++ ) 
                {
                    ids.push(data[i].id);
                }
                sima.ajax.get('HR/absence/addEmployeesToCollectiveAnnualLeaves', {
                    data:{
                        collective_annual_leave_id: collective_annual_leave_id,
                        employee_ids: ids
                    },
                    success_function: function(response){
                        $('#'+guitable_id).simaGuiTable('populate');
                    }
                });
            }
        }, params);
    };
    
    this.onChangeNationalEvent = function(guitable_id, span1_id, span2_id)
    {
        sima.ajax.get('HR/nationalEvents/getUnusedAndWithoutNullDatesNationalEvents', {
            success_function: function(response){
                
                var span1 = $('#'+span1_id);
                var span2 = $('#'+span2_id);
                
                span1.text(response.number_of_unused_national_events);
                span2.text(response.number_of_unused_national_events_with_notnulldates);
            }
        });

        $('#'+guitable_id).simaGuiTable('populate');
    };    
        
    this.onAddAllEmployeesToCollectiveLeave = function(collective_annual_leave_id, guitable_id)
    {
        sima.ajaxLong.start('HR/absence/addAllEmployeesToCollectiveAnnualLeave', {
            showProgressBar:$('body'),
            data:{
                collective_annual_leave_id : collective_annual_leave_id
            },
            onEnd: function(response) {
                sima.dialog.openInfo(response.message);
                $('#'+guitable_id).simaGuiTable('populate');
            }
        });
    };
    
    this.collectiveLeaveAfterSaveResaveAnnualLeaves = function(collective_annual_leave_id)
    {
        sima.ajaxLong.start('HR/absence/collectiveLeaveAfterSaveResaveAnnualLeaves', {
            showProgressBar:$('body'),
            data:{
                collective_annual_leave_id : collective_annual_leave_id
            },
            onEnd: function(response) {
                sima.dialog.openInfo(response.message);
            }
        });
    };
    
    this.changeAnnualLeaveDaysNumber = function(annual_leave_decision_id)
    {        
        sima.model.form('AnnualLeaveDecision',annual_leave_decision_id,{
            formName:'only_days_number'
        });
    };
    
    this.annualLeaveDecisionsByYear = function(_this, year_id)
    {
        sima.ajax.get('HR/absence/getAnnualLeaveDecisionsForYear',{
            get_params:{
                year_id : year_id
            },
            success_function: function(response){
                _this.empty().append(response.html).show();
                _this.parent().trigger('sima-layout-allign');
            }
        });
    };
    
    this.createAbsenceAnnualLeave = function(calendar_id, person_id)
    {
        sima.ajax.get('HR/absence/getAnnualLeavesForEmployee',{
            get_params:{
                employee_id : person_id
            },
            success_function: function(response){
                sima.dialog.openInFullScreen(response.html, response.title, {
                    'close_func': function(){
                        $('#'+calendar_id).simaActivityCalendar('reloadCalendar');
                    }
                });
            }
        });
    };
    
    this.createAbsenceFreeDays = function(calendar_id, person_id)
    {
        sima.ajax.get('HR/absence/getFreeDaysForEmployee',{
            get_params:{
                employee_id : person_id
            },
            success_function: function(response){
                sima.dialog.openInFullScreen(response.html, response.title, {
                    'close_func': function(){
                        $('#'+calendar_id).simaActivityCalendar('reloadCalendar');
                    }
                });
            }
        });
    };
    
    this.createAbsenceSickLeave = function(calendar_id, person_id)
    {
        sima.ajax.get('HR/absence/getSickLeavesForEmployee',{
            get_params:{
                employee_id : person_id
            },
            success_function: function(response){
                sima.dialog.openInFullScreen(response.html, response.title, {
                    'close_func': function(){
                        $('#'+calendar_id).simaActivityCalendar('reloadCalendar');
                    }
                });
            }
        });
    };
    
    this.addSafeEvd2 = function(btn_uniq_id, medical_examination_referral_id, safe_evd_2_document_type_id)
    {        
        sima.dialog.openChooseOption('Dodati kao:',[
            {
                title: 'Novi pregled',
                function: function(){
                    sima.dialog.close();
                    addSafeEvd2(btn_uniq_id, medical_examination_referral_id, '', safe_evd_2_document_type_id);
                }
            },
            {
                title: 'Izaberi postojeći pregled',
                function: function(){
                    sima.dialog.close();
                    var params = {};
                    params.view = "guiTable";
                    params.fixed_filter = {
                        filter_scopes:'withoutMedicalReferral'
                    };
                    sima.model.choose('SafeEvd2', function(data){                        
                        if (data.length > 0)
                        {
                            var safe_evd_2_id = data[0].id;                            
                            addSafeEvd2(btn_uniq_id, medical_examination_referral_id, safe_evd_2_id, safe_evd_2_document_type_id);
                        }
                    }, params);
                }
            }
        ]);
    };
    
    function addSafeEvd2(btn_uniq_id, medical_examination_referral_id, safe_evd_2_id, safe_evd_2_document_type_id)
    {        
        sima.model.form('SafeEvd2',safe_evd_2_id,{
            init_data:{
                SafeEvd2:{
                    medical_referral_id:{
                        disabled:true,
                        init:medical_examination_referral_id
                    }
                },
                File: {
                    document_type_id: {
                        disabled: !sima.isEmpty(safe_evd_2_document_type_id) ? true : false,
                        init: safe_evd_2_document_type_id
                    }
                }
            },
            onSave: function()
            {
                $('#' + btn_uniq_id).parents('.sima-guitable:first').simaGuiTable('populate');
            }
        });
    }
    
    this.addSafeEvd13 = function(btn_uniq_id, medical_examination_referral_id, safe_evd_13_document_type_id)
    {        
        sima.dialog.openChooseOption('Dodati kao:',[
            {
                title: 'Novi pregled',
                function: function(){
                    sima.dialog.close();
                    addSafeEvd13(btn_uniq_id, medical_examination_referral_id, '', safe_evd_13_document_type_id);
                }
            },
            {
                title: 'Izaberi postojeći pregled',
                function: function(){
                    sima.dialog.close();
                    var params = {};
                    params.view = "guiTable";
                    params.fixed_filter = {
                        filter_scopes:'withoutMedicalReferral'
                    };
                    sima.model.choose('SafeEvd13', function(data){                        
                        if (data.length > 0)
                        {
                            var safe_evd_13_id = data[0].id;                            
                            addSafeEvd13(btn_uniq_id, medical_examination_referral_id, safe_evd_13_id, safe_evd_13_document_type_id);
                        }
                    }, params);
                }
            }
        ]);
    };
    
    function addSafeEvd13(btn_uniq_id, medical_examination_referral_id, safe_evd_13_id, safe_evd_13_document_type_id)
    {        
        sima.model.form('SafeEvd13',safe_evd_13_id,{
            init_data:{
                SafeEvd13:{
                    medical_referral_id:{
                        disabled:true,
                        init:medical_examination_referral_id
                    }
                },
                File: {
                    document_type_id: {
                        disabled: !sima.isEmpty(safe_evd_13_document_type_id) ? true : false,
                        init: safe_evd_13_document_type_id
                    }
                }
            },
            onSave: function()
            {
                $('#' + btn_uniq_id).parents('.sima-guitable:first').simaGuiTable('populate');
            }
        });
    }
    
    this.addAbsenceAnnualLeaveDecisionFromExistingFile = function(table_id, employee_id, annual_leave_decision_document_type_id)
    {
        sima.model.choose('File', function(data){
            if (data.length > 0)
            {                            
                var file_id = data[0].id;               
                sima.model.form('AnnualLeaveDecision', file_id, {
                    init_data:{
                        AnnualLeaveDecision:{
                            employee_id:{
                                disabled:true,
                                init:employee_id
                            }
                        },
                        File:{
                            document_type_id:{
                                disabled:true,
                                init:annual_leave_decision_document_type_id
                            }
                        }
                    },
                    onSave: function()
                    {                        
                        $('#'+table_id).simaGuiTable('populate');
                    }
                });
            }
        }, {view:'guiTable'});
    };
    
    this.showDeliveredDocumentPreviousStates = function(file_id, last_document_delivery_id)
    {
        sima.model.openGuiTable({
            model:'DeliveredHRDocument',
            fixed_filter:{
                file:{
                    ids:file_id
                },
                filter_scopes:{
                    withoutDeliveryIds:last_document_delivery_id
                },
                display_scopes:'byName'
            }
        },{title:'Prethodna stanja'});
    };
    
    this.nationalEventToDayAfterSaveRecheck = function(national_event_to_day_date, before_save_day_date)
    {
        sima.ajaxLong.start('HR/nationalEvents/nationalEventToDayAfterSaveRecheck', {
            showProgressBar:$('body'),
            data:{
                national_event_to_day_date : national_event_to_day_date,
                before_save_day_date: before_save_day_date
            },
            onEnd: function(response) {
                sima.dialog.openInfo(response.message);
            }
        });
    };
    
    this.addReferencesToCompanyLicense = function(_this, company_license_id, partner_id)
    {
        var row = _this.parents('.choose_persons_to_company_licence_row:first');
        var references_list = _this.parents('.add_references:first').find('.references_list');
        var old_references_ids = [];
        var ids = [];
        references_list.find('.reference_item').each(function(){
            old_references_ids.push($(this).data('id'));
            ids.push({id:$(this).data('id'), display_name:$(this).data('display_name')});
        });
        var params = {};
        params.view = "guiTable";
        params.multiselect = true;
        params.ids = ids;
        params.fixed_filter = {ids:row.data('references_ids')};
        params.params = {
            add_button:false
        };
        sima.model.choose('Reference', function(data){
            var remove_references_ids = [];
            var add_references_ids = [];
            var new_references_ids = [];
            $.each(data, function(index, value) {
                new_references_ids.push(value.id);
                if (jQuery.inArray(value.id, old_references_ids) === -1)
                {
                    add_references_ids.push(value.id);
                }
            });
            for(var i=0; i<old_references_ids.length; i++)
            {
                if (jQuery.inArray(old_references_ids[i], new_references_ids) === -1)
                {
                    remove_references_ids.push(old_references_ids[i]);
                }
            }
            if (add_references_ids.length > 0 || remove_references_ids.length > 0)
            {
                sima.ajax.get('HR/WorkLicense/addReferencesToCompanyLicense', {
                    get_params:{
                        company_license_id : company_license_id,
                        partner_id: partner_id
                    },
                    data:{
                        remove_references_ids:remove_references_ids,
                        add_references_ids:add_references_ids
                    },
                    success_function: function(response){
                        sima.refreshActiveTab(_this);
                    }
                });
            }
        }, params);
    };
    
    this.companyLicenseDecisionToCompanyLicensesDisagreements = function(_this, company_license_decision_id)
    {
        var uniq = 'check_company_license_decision_disagreements_'+sima.uniqid();
        sima.ajax.get('HR/WorkLicense/companyLicenseDecisionToCompanyLicensesDisagreements', {
            get_params: {
                company_license_decision_id:company_license_decision_id,
                uniq:uniq
            },
            success_function: function(response) {
                if (response.has_disagreements)
                {
                    var html = {};
                    var yes_func_params = {};
                    var no_func_params = {};
                    html.title = sima.translate('CompanyLicenseDecisionCheckDisagreements');
                    html.html = response.html;
                    yes_func_params.func = function onConfirm(obj) {
                        var wrap = $('#'+uniq);
                        var disagreements = [];
                        wrap.find('.body .row').each(function(){
                            var disagreement = {};
                            disagreement.operation = $(this).data('operation');
                            disagreement.company_license_id = $(this).data('company_license_id');
                            disagreement.company_id = $(this).data('company_id');
                            disagreements.push(disagreement);
                        });
                        sima.dialog.close();
                        if (disagreements.length > 0)
                        {
                            sima.ajax.get('HR/WorkLicense/applyCompanyLicenseDecisionToCompanyLicensesDisagreements',{
                                data:{disagreements:disagreements},
                                success_function: function(response){
                                    sima.misc.refreshMainTabForObject(_this);
                                }
                            });
                        }
                    };
                    yes_func_params.title = sima.translate('CompanyLicenseDecisionApplyDisagreements');
                    no_func_params.hidden = true;
                    sima.dialog.openYesNo(html, yes_func_params, no_func_params);
                }
                else
                {
                    sima.dialog.openInfo(sima.translate('CompanyLicenseDecisionNoDisagreements'));
                }
            }
        });
    };
    
    this.chooseAndAddCompanyLicensesToCompany = function(uniq_id)
    {
        var company_licenses_to_companies_table = $('#company_licenses_to_companies_'+uniq_id);
        var company_id = company_licenses_to_companies_table.data('company_id');
        if (!sima.isEmpty(company_id))
        {
            var params = {};
            params.view = "guiTable";
            params.multiselect = true;
            sima.model.choose('CompanyLicense', function(data){
                var company_licenses_ids = [];
                $.each(data, function(key, value) {
                    company_licenses_ids.push(value.id);
                });
                if (company_licenses_ids.length > 0)
                {
                    sima.ajax.get('HR/WorkLicense/addCompanyLicensesToCompany',{
                        get_params:{
                            company_id : company_id
                        },
                        data:{
                            company_licenses_ids : company_licenses_ids
                        },
                        success_function: function(response){
                            company_licenses_to_companies_table.simaGuiTable('populate');
                        }
                    });
                }
            }, params);
        }
    };
    
    this.onWorkPositionCompetitionSelect = function(uniq_id, model_tag, row)
    {
        var tabs = $('#tabs'+uniq_id);
        var model_id = row.attr('model_id');
        
        tabs.simaTabs('set_list_tabs_params',{
            list_tabs_trigger_tag: sima.getTag(model_tag, model_id),
            list_tabs_get_params:{id:model_id}
        });
        tabs.show();
    };
    
    this.onWorkPositionCompetitionUnSelect = function(uniq_id)
    {
        $('#tabs'+uniq_id).hide();
    };

    this.onWorkPositionCompetitionCandidateSelect = function(uniq_id, model_tag, row)
    {
        var tabs = $('#tabs'+uniq_id);
        var model_id = row.attr('model_id');
        
        tabs.simaTabs('set_list_tabs_params',{
            list_tabs_trigger_tag: sima.getTag(model_tag, model_id),
            list_tabs_get_params:{id:model_id}
        });
        tabs.show();
    };
    
    this.onWorkPositionCompetitionCandidateUnSelect = function(uniq_id)
    {
        $('#tabs'+uniq_id).hide();
    };
    
    this.filterGradeForCurrUser = function(dropdown_obj, curr_user_id, guitable_id)
    {
        var guitable = $('#'+guitable_id);
        var guitable_fixed_filter = guitable.simaGuiTable('getFixedFilter');
        var filter_scope = dropdown_obj.val();
        
        
        var filter_scopes = {};
        if (!sima.isEmpty(guitable_fixed_filter['filter_scopes']))
        {
            filter_scopes = guitable_fixed_filter['filter_scopes'];
            if (typeof filter_scopes === 'string')
            {
                filter_scopes = [filter_scopes];
            }
        }
        
        if (!sima.isEmpty(filter_scopes['hasCVGradeByUser']))
        {
            delete filter_scopes['hasCVGradeByUser'];
        }
        if (!sima.isEmpty(filter_scopes['hasNotCVGradeByUser']))
        {
            delete filter_scopes['hasNotCVGradeByUser'];
        }
        if (!sima.isEmpty(filter_scopes['hasInterviewGradeByUser']))
        {
            delete filter_scopes['hasInterviewGradeByUser'];
        }
        if (!sima.isEmpty(filter_scopes['hasNotInterviewGradeByUser']))
        {
            delete filter_scopes['hasNotInterviewGradeByUser'];
        }
        
        if (filter_scope !== '')
        {
            filter_scopes[filter_scope] = curr_user_id;
        }
        
        guitable_fixed_filter['filter_scopes'] = filter_scopes;
        guitable.simaGuiTable('setFixedFilter', guitable_fixed_filter, true);
    };
    
    this.applyActiveAbsencesFilter = function(uniq_id)
    {
        var absence_table = $('#absence'+uniq_id);
        var btn_toggle_activity = $('#btn-toggle-activity'+uniq_id);
        
        btn_toggle_activity.toggleClass( "_pressed" );
        
        var fixed_filter = absence_table.simaGuiTable('getFixedFilter');
        if (btn_toggle_activity.hasClass('_pressed')) 
        {
            fixed_filter['absent_on_date'] = sima.misc.today();
        }
        else
        {
            fixed_filter['absent_on_date'] = '';
        }

        absence_table.simaGuiTable('setFixedFilter', fixed_filter, true);
    };
    
    this.initAbsencesMenuEvents = function(uniq_id)
    {
        $("#absence_menu"+uniq_id).on('selectItem', function(event, item){
            sima.ajax.get(item.data('url'), {
                success_function: function(response) {
                    sima.set_html($("#absence_wrap"+uniq_id),response.html);
                }
            });
        });
    };
    
    this.onSelectCollectiveAnnualLeave = function(uniq_id, row)
    {
        sima.ajax.get('HR/absence/getCollectiveAnnualLeaveFullInfo/', {
            get_params: {model_id: $(row).attr('model_id')},
            success_function: function(response) {               
                sima.set_html($("#collective-annual-leave-full-info"+uniq_id),response.html);
            }
        });
    };
    
    this.onUnselectCollectiveAnnualLeave = function(uniq_id)
    {   
        sima.set_html($("#collective-annula-leave-full-info"+uniq_id), "");
    };
    
    this.medicalExaminationReferralFormPersonChooseHandler = function(form, dependent_on_field, dependent_field, apply_actions_func, additional_params)
    {
        var person_row = form.find(".sima-model-form-row[data-column='person_id']");
        var work_position_row = form.find(".sima-model-form-row[data-column='work_position_id']");
        var person_val = person_row.find('.sima-ui-sf:first').simaSearchField('getValue'); 

        if (!sima.isEmpty(person_val.id))
        {
            sima.ajax.get('HR/safeRecord/getPersonWorkPositionInSystemCompany', {
                get_params: {
                    person_id: person_val.id 
                },
                success_function: function(response) {
                    if (typeof response.id !== 'undefined')
                    {
                        work_position_row.find('.sima-ui-sf:first').simaSearchField('setValue', response.id, response.display_name);
                    }
                }
            });
        }
    };
    
    this.syncOccupationsFromCodebook = function(guitable_id)
    {
        var progress_bar_obj = sima.getLastActiveFunctionalElement();
        if (!sima.isEmpty(guitable_id))
        {
            progress_bar_obj = $('#'+guitable_id);
        }
        else if (sima.isEmpty(progress_bar_obj))
        {
            progress_bar_obj = $('body');
        }

        sima.ajaxLong.start('HR/HR/getOccupationDiffBetweenLocalAndCodebook', {
            showProgressBar: progress_bar_obj,
            onEnd: function(response) {
                if (sima.isEmpty(response.html))
                {
                    sima.dialog.openInfo(sima.translate('NoDifferences'));
                }
                else
                {
                    sima.diffViewer.renderFromHtml(response.html, {
                        save_action: 'HR/HR/saveOccupationsDiffBetweenLocalAndCodebook',
                        after_save_func: function () {
                            if (!sima.isEmpty(guitable_id))
                            {
                                $('#'+guitable_id).simaGuiTable('populate');
                            }
                        }
                    });
                }
            }
        });
    };
    
    this.syncQualificationLevelsFromCodebook = function(guitable_id)
    {
        var progress_bar_obj = sima.getLastActiveFunctionalElement();
        if (!sima.isEmpty(guitable_id))
        {
            progress_bar_obj = $('#'+guitable_id);
        }
        else if (sima.isEmpty(progress_bar_obj))
        {
            progress_bar_obj = $('body');
        }

        sima.ajaxLong.start('HR/HR/getQualificationLevelsDiffBetweenLocalAndCodebook', {
            showProgressBar: progress_bar_obj,
            onEnd: function(response) {
                if (sima.isEmpty(response.html))
                {
                    sima.dialog.openInfo(sima.translate('NoDifferences'));
                }
                else
                {
                    sima.diffViewer.renderFromHtml(response.html, {
                        after_save_func: function () {
                            if (!sima.isEmpty(guitable_id))
                            {
                                $('#'+guitable_id).simaGuiTable('populate');
                            }
                        }
                    });
                }
            }
        });
    };
}
