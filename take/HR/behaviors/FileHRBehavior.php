<?php

/**
 * @package SIMA
 * @subpackage Accounting
 */
class FileHRBehavior extends SIMAActiveRecordBehavior
{
    
    public function relations($event)
    {
        $event->addResult(array(
            'safe_evd_6'=>array(SIMAActiveRecord::HAS_ONE, 'SafeEvd6', 'id'),
            'safe_evd_3'=>array(SIMAActiveRecord::HAS_ONE, 'SafeEvd3', 'id'),
            'safe_evd_2'=>array(SIMAActiveRecord::HAS_ONE, 'SafeEvd2', 'id'),
            'has_safe_evd_2'=>array(SIMAActiveRecord::STAT, 'SafeEvd2', 'id', 'select' => 'count(*)>0'),
            'safe_evd_2_medical_referral'=>array(SIMAActiveRecord::HAS_ONE, 'SafeEvd2', 'medical_referral_id'),
            'annual_leave_decision'=>array(SIMAActiveRecord::HAS_ONE, 'AnnualLeaveDecision', 'id'),
            'introduction_into_business_program'=>array(SIMAActiveRecord::HAS_ONE, 'IntroductionIntoBusinessProgram', 'id'),
            'company_license_decision'=>array(SIMAActiveRecord::HAS_ONE, 'CompanyLicenseDecision', 'id'),
            'safe_evd_8'=>array(SIMAActiveRecord::HAS_ONE, 'SafeEvd8', 'id'),
            'safe_evd_9'=>array(SIMAActiveRecord::HAS_ONE, 'SafeEvd9', 'id'),
            'medical_examination_referral'=>array(SIMAActiveRecord::HAS_ONE, 'MedicalExaminationReferral', 'id'),
            'safe_evd_13'=>array(SIMAActiveRecord::HAS_ONE, 'SafeEvd13', 'id'),
            'document_deliveries'=>array(SIMAActiveRecord::HAS_MANY, 'DeliveredHRDocument', 'file_id'),
            'has_document_deliveries'=>array(SIMAActiveRecord::STAT, 'DeliveredHRDocument', 'file_id', 'select' => 'count(*)>0'),
            'last_document_delivery'=>array(SIMAActiveRecord::HAS_ONE, 'DeliveredHRDocument', 'file_id', 'order'=>'sent_timestamp DESC'),
            'document_deliveries_num'=>array(SIMAActiveRecord::STAT, 'DeliveredHRDocument', 'file_id', 'select' => 'count(*)')
        ));
    }
    
    public function basicInfo($event)
    {
        $owner = $this->owner;
        
        if($owner->isDeleted())
        {
            return;
        }
        
        if(isset($owner->work_contract))
        {
            $work_contract = $owner->work_contract;
            $event->addResult([
                [
                    'type' => SIMABasicInfo::$TYPE_HTML,
                    'html' => '<h3>Ugovor o radu'.SIMAHtml::modelFormOpen($work_contract).'</h3>'
                ],
                [
                    'type' => SIMABasicInfo::$TYPE_EXPANDABLE_INFORMATIONS_FOR_MODEL,
                    'model' => $work_contract,
                    'basic_informations' => [
                        'employee_id',
                        'end_date',
                        'work_position_id',
                    ],
                    'expanded_informations' => [
                        'employee_id',
                        'start_date',
                        'end_date',
                        'work_position_id',
                        'generate_work_contract_template',
                        'generate_notice_of_mobbing_template',
                        'add_filing_number',
                    ]
                ]
            ]);
        }
        else
        {
            $event->addResult([
                [
                    'type' => SIMABasicInfo::$TYPE_HTML,
                    'html' => "<h3>" . $owner->getAttributeDisplay('document_type_id')
                            . $owner->getAttributeDisplay('addDocument') . "</h3>"
                ]
            ]);
        }
    }
    
    public function fullInfo($event)
    {        
        $owner = $this->owner;
        if(!empty($owner->safe_evd_3))
        {
            $event->addResult([
                [
                    'model' => $owner->safe_evd_3,
                    'priority' => 10
                ]
            ]);
        }
    }
    
    public function views($event)
    {
        $owner = $this->owner;

        if($event->params['view'] === 'basic_info')
        {
            if(!empty($owner->medical_examination_referral))
            {
                $event->addResult([
                    [
                        'view' => 'basic_info',
                        'order' => 10,
                        'html' => Yii::app()->controller->renderModelView($owner->medical_examination_referral, 'inFile')
                    ]
                ]);
            }
        }
    }
}