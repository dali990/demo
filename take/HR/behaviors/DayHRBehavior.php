<?php

class DayHRBehavior extends SIMAActiveRecordBehavior
{
    public function relations($event)
    {
        $event->addResult(array(
            'national_events'=>array(SIMAActiveRecord::MANY_MANY, 'NationalEvent', 
                'hr.national_event_to_day_year(day_id,national_event_id)'),
        ));
    }
    
    public function getIsReligiousHoliday()
    {
        $result = false;
                
        foreach($this->owner->national_events as $na)
        {            
            if($na->type === NationalEvent::$TYPE_RELIGIOUS_HOLIDAY)
            {
                $result = true;
                break;
            }
        }
        
        return $result;
    }
    
    public function getIsStateHoliday()
    {
        $result = false;
                
        foreach($this->owner->national_events as $na)
        {            
            if($na->type === NationalEvent::$TYPE_STATE_HOLIDAY)
            {
                $result = true;
                break;
            }
        }
        
        return $result;
    }
    
    /**
     * not weekend && not religious holiday
     * @return boolean
     */
    public function getIsRegularWorkDay()
    {
        $result = true;
        
        if($this->owner->IsWeekend 
                || $this->owner->IsReligiousHoliday
                || $this->owner->IsStateHoliday)
        {
            $result = false;
        }
        
        return $result;
    }
}