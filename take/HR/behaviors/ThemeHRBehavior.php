<?php

class ThemeHRBehavior extends SIMAActiveRecordBehavior
{
    public function tabs($event)
    {
        $owner = $this->owner;

        if ($owner->type === Theme::$BID)
        {
            $event->addResult([
                [
                    'title' => Yii::t('BaseModule.Theme', 'TabHRLicenses'),
                    'code' => 'theme_hr_licenses',
                    'module_origin' => 'HR'
                ],
            ]);
        }
    }
}
