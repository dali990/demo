<?php

class NationalEventReligiousHolidayToDayActivityBehavior extends NationalEventToDayActivityBehavior
{
    /// reqiured
    protected $_simaActivity_display_class = '_national_event _religious_holiday';
    
    protected $_simaActivity_priorityNumber = 40;
}
