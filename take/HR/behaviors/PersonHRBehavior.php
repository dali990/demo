<?php

class PersonHRBehavior extends SIMAActiveRecordBehavior
{
    public function relations($event)
    {
        $event->addResult([
            'person_to_work_positions'=>[SIMAActiveRecord::HAS_MANY, 'PersonToWorkPosition', 'person_id'],
            'employee_candidate' => [SIMAActiveRecord::HAS_ONE, 'EmployeeCandidate', 'id'],
            'safe_evd_2_records'=>array(SIMAActiveRecord::HAS_MANY, 'SafeEvd2', 'person_id', 'order'=>'date_previous_report DESC'),
            'safe_evd_2_records_cnt'=>array(SIMAActiveRecord::STAT, 'SafeEvd2', 'person_id', 'select' => 'count(*)'),
            'safe_evd_2'=>array(SIMAActiveRecord::HAS_MANY, 'SafeEvd2', 'person_id'),
            'safe_evd_13_records'=>array(SIMAActiveRecord::HAS_MANY, 'SafeEvd13', 'person_id', 'order'=>'report_date DESC'),
            'safe_evd_13_records_cnt'=>array(SIMAActiveRecord::STAT, 'SafeEvd13', 'person_id', 'select' => 'count(*)'),
            'qualification_level' => [SIMAActiveRecord::BELONGS_TO, QualificationLevel::class, 'qualification_level_id']
        ]);
    }
}