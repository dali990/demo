<?php

class WorkPositionTabs extends SIMAActiveRecordTabs {

    protected function tabsDefinition($code = null) {
        $owner = $this->owner;        
        $work_position_tabs = array(
            array(
                'title' => Yii::t('HRModule.WorkPosition', 'PersonsForWhoIsMainPosition'),
                'code'=>'not_fictive_work_positions',
                'action'=>'guitable',
                'get_params'=>array(
                    'settings'=>array(
                        'model' => 'PersonToWorkPosition',
                        'columns_type' => 'fromWorkPosition',
                        'add_button' => false,
                        'fixed_filter' => [
                            'work_position'=>[
                                'ids'=>$owner->id
                            ],
                            'fictive'=>false
                        ]
                    )
                ),
            ),
            array(
//                'title' => 'Osobe čija je privremena pozicija',
                'title' => Yii::t('HRModule.WorkPosition', 'PersonsForWhoIsTempPosition'),
                'code'=>'fictive_work_positions',
                'action'=>'guitable',
                'get_params'=>array(
                    'settings'=>array(
                        'model' => 'PersonToWorkPosition',
                        'columns_type' => 'fromWorkPosition',
                        'add_button' => [
                            'init_data' => [
                                'PersonToWorkPosition' => [
                                    'work_position_id'=>[
                                        'disabled',
                                        'init'=>$owner->id
                                    ],
                                    'fictive'=>true
                                ]
                            ]
                        ],
                        'fixed_filter' => [
                            'work_position'=>[
                                'ids'=>$owner->id
                            ],
                            'fictive'=>true
                        ]
                    )
                ),
            )
        );

        return $work_position_tabs;
    }
}
