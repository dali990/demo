<?php

class FixedAssetHRBehavior extends SIMAActiveRecordBehavior
{
    
    public function relations($event)
    {
        $event->addResult(array(
            'safe_evd_9_records'=>array(SIMAActiveRecord::HAS_MANY, 'SafeEvd9', 'fixed_asset_id'),
            'safe_evd_9_records_cnt'=>array(SIMAActiveRecord::STAT, 'SafeEvd9', 'fixed_asset_id', 'select' => 'count(*)'),
        ));
    }
}