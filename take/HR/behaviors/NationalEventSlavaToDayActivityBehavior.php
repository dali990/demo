<?php

class NationalEventSlavaToDayActivityBehavior extends NationalEventToDayActivityBehavior
{
    /// reqiured
    protected $_simaActivity_display_class = '_national_event _slava';
    
    protected $_simaActivity_priorityNumber = 30;
    
    protected $_simaActivity_personal = 'for_employee';
}
