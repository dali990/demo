<?php

class AbsenceBehavior extends SIMAActiveRecordBehavior
{
    public function GetFirstDayInMonth(Month $month)
    {
        $owner = $this->owner;
        
        $monthFirstDay = $month->firstDay();
        
        $beginDay = Day::getByDate($owner->begin);
        
        $result = $beginDay;
        if($beginDay->compare($monthFirstDay) < 0)
        {
            $result = $monthFirstDay;
        }
        
        return $result;
    }
    
    public function GetLastDayInMonth(Month $month)
    {
        $owner = $this->owner;
        
        $monthLastDay = $month->lastDay();
        
        $endDay = Day::getByDate($owner->end);
        
        $result = $endDay;
        if($endDay->compare($monthLastDay) > 0)
        {
            $result = $monthLastDay;
        }
        
        return $result;
    }
    
    public function GetNumberOfCalendarDaysInMonth(Month $month)
    {
        $owner = $this->owner;
        
        $date1 = new DateTime($owner->GetFirstDayInMonth($month));
        $date2 = new DateTime($owner->GetLastDayInMonth($month));
        
        $number_of_calendar_days = $date2->diff($date1)->format("%a")+1;
                
        return $number_of_calendar_days;
    }
    
    public function GetNumberOfActiveWorkDaysInMonth(Month $month)
    {
        $owner = $this->owner;
        
        $firstDay = $owner->GetFirstDayInMonth($month);
        $lastDay = $owner->GetLastDayInMonth($month);
        
        $days = Day::getBetweenDates($firstDay, $lastDay);
        
        $result_number = 0;
                
        foreach ($days as $day)
        {
            if(
//                (
//                    $owner->type === Absence::$SICK_LEAVE 
//                    && $owner->subtype === AbsenceSickLeave::$SUBTYPE_SICK_LEAVE_OVER_30
//                    && $day->IsWeekend === false
//                ) 
//                || 
//                (
//                    $owner->subtype !== AbsenceSickLeave::$SUBTYPE_SICK_LEAVE_OVER_30 
//                    && $day->IsRegularWorkDay === true
//                )
                $owner->simaActivity_isActiveOnDay($day)
            )
            {
                $result_number++;
            }
        }
        
        return $result_number;
    }
    
    public function fullInfo($event)
    {
        $owner = $this->owner;
        
        $event->addResult([
            [
                'model' => $owner->real_model,
                'priority'=>1
            ]
        ]);
    }
}

