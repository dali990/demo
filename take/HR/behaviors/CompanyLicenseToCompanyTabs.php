<?php

class CompanyLicenseToCompanyTabs extends SIMAActiveRecordTabs
{
    protected function tabsDefinition($code = null)
    {
        $owner = $this->owner;
        $tabs = array(
            array(
                'title'=>'Informacije',
                'code'=>'info',
                'params_function' => 'tabInfo'
            ),
        );
        
        return $tabs;
    }
    
    protected function tabInfo()
    {
        $owner = $this->owner;
        $partners = [];
        if (count($owner->partners) === 0)
        {
            $licenses_ids = '';
            foreach ($owner->company_license->work_licenses as $license)
            {
                $licenses_ids .= $license->id.',';
            }
            $licenses_ids = rtrim($licenses_ids, ",");
            $criteria = new SIMADbCriteria(array(
                'Model'=>'Person',
                'model_filter'=>array(
    //                'work_license' => array(
    //                  'person_license_to_reference'=>array(
    //                      'reference'=>array(
    //                          'reference_to_company_license'=>array(
    //                              'company_license'
    //                          )
    //                      )
    //                  )  
    //                ),
                    'filter_scopes' => array(
                        'has_licenses' => $licenses_ids,
                    ),
                    'display_scopes' => array(
                        'byName'
                    )
                )
            ));
            $persons = Person::model()->findAll($criteria);
            $partners = [$owner->company->partner];
            foreach ($persons as $person) 
            {
                array_push($partners, $person->partner);
            }
        }
        else
        {
            $partners = $owner->partners;
        }
        
        
        return array(
            'model'=>$owner,
            'partners'=>$partners,
        );
    }
}