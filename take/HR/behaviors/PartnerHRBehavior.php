<?php

class PartnerHRBehavior extends SIMAActiveRecordBehavior
{
    public function tabs($event)
    {
        $owner = $this->owner;
        
        if(Yii::app()->user->checkAccess('menuShowLegalEmployees') && isset($owner->person)  && isset($owner->person->employee))
        {
            $event->addResult([
                [
                    'title' => Yii::t('HRModule.Partner', 'HREmployee'),
                    'code' => 'partner_files.hr_employee',
                    'action' => 'HR/hRPartnerTabs/employeeFilesTab',
                    'get_params' => ['id' => $owner->id]
                ]
            ]);
        }
        
        if(Yii::app()->user->checkAccess('menuShowLegalWorkPositionCompetition') && isset($owner->person)  && isset($owner->person->employee_candidate))
        {
            $event->addResult([
                [
                    'title' => Yii::t('HRModule.Partner', 'HRCandidate'),
                    'code' => 'partner_files.hr_employee',
                    'action' => 'HR/hRPartnerTabs/candidateFilesTab',
                    'get_params' => ['id' => $owner->id]
                ]
            ]);
        }
    }
}

