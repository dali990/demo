<?php

/**
 * @package SIMA
 * @subpackage Accounting
 */
class UserHRBehavior extends SIMAActiveRecordBehavior
{
    
    public function relations($event)
    {
        $event->addResult(array(
            'work_position_competition_juries'=>array(SIMAActiveRecord::HAS_MANY, 'WorkPositionCompetitionJury','jury_id'),
//            'jury_in_work_position_competitions' => array(self::MANY_MANY, 'WorkPositionCompetition', 
//                'hr.work_position_competition_juries(jury_id,work_position_competition_id)'
//            ),
        ));
    } 
    
    public function checkIsJury()
    {
        $cnt = WorkPositionCompetitionJury::model()->countByAttributes(['jury_id' => $this->owner->id]);
        return $cnt>0;
    }
}