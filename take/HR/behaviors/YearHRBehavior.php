<?php

/**
 * @package SIMA
 * @subpackage Accounting
 */
class YearHRBehavior extends SIMAActiveRecordBehavior
{
    
    public function relations($event)
    {
        $event->addResult(array(
            'hr'=>array(SIMAActiveRecord::HAS_ONE, 'HRYear','id')
        ));
    } 
}