<?php

class EmployeeHRBehavior extends SIMAActiveRecordBehavior
{
    public function relations($event)
    {
        $event->addResult(array(
            'slava'=>array(SIMAActiveRecord::BELONGS_TO, 'NationalEvent', 'national_event_slava_id'),
            'annual_leave_decisions'=>array(SIMAActiveRecord::HAS_MANY, 'AnnualLeaveDecision', 'employee_id'),
            'safe_evd_3'=>array(SIMAActiveRecord::HAS_MANY, 'SafeEvd3', 'employee_id'),
            'safe_evd_6'=>array(SIMAActiveRecord::HAS_MANY, 'SafeEvd6', 'employee_id')
        ));
    }   
    
    public function canLoggedInUserEditSlavaForThisEmp()
    {
        if ((empty($this->owner->national_event_slava_id) && $this->owner->id === Yii::app()->user->id) || Yii::app()->user->checkAccess('HREmployeeSlava'))
        {
            return true;
        }
        
        return false;
    }
}