<?php

class AbsenceTabs extends SIMAActiveRecordTabs
{
    protected function tabsDefinition($code = null)
    {
        $owner = $this->owner;
        $file_tab_title=(isset($owner->type))?($owner->type==0)?'Odobrenje':'Doznaka':'Dokument';
        $tabs = array(
            array(
                'title' => Yii::t('MessagesModule.Common','Comments'),
                'code'=>'comments',
                'params_function' => 'tabComments',
                'origin' => 'Absence'
            )
        );
        
        if (isset($owner->document))
        {
            $tabs[] = array(
                'title'=>$file_tab_title,
                'code'=>'document',
                'action'=>'files/file',
                'get_params'=>array('id'=>$owner->document_id),
                'origin' => 'Absence'
            );
        }

        return $tabs;
    }
    
    
    
    protected function tabComments()
    {
        $owner = $this->owner;

        $comment_thread=CommentThread::getCommonForModel($owner);
        
        if(!is_null($owner->employee->person->user))
        {
            $comment_thread->addListener($owner->employee->person->user->id);
        }
        
        $boss_ids=$owner->employee->person->getSectorDirectors(false);
        foreach ($boss_ids as $boss_id)
        {
            $user = User::model()->findByPk($boss_id);
            if(!is_null($user))
            {
                $comment_thread->addListener($user->id);
            }
        }
        
        return array(
            'model' => $owner,
            'comment_thread' => $comment_thread
        );
    }     
}
