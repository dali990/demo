<?php

class AbsenceFreeDaysActivityBehavior extends AbsenceActivityBehavior
{
    /// reqiured
    protected $_simaActivity_display_class = '_absence _paid_leave';
    
    /// optional
    protected $_simaActivity_create_action = 'sima.hr.createAbsenceFreeDays';
    
    protected $_simaActivity_priorityNumber = 30;
}