<?php

class AbsenceActivityBehavior extends ActivityBehavior
{
    /// reqiured
    protected $_simaActivity_daily = true;
    
    protected $_simaActivity_date_filter = 'absent_on_date';
    
    protected $_simaActivity_checkConfirmDateColumn = 'begin, end';
        
    /// optional
    protected $_simaActivity_personal = 'employee.person';
    
    protected $_simaActivity_confirmed_condition_scope = true;
        
    public function simaActivity_getActivityHours(Day $day)
    {
        $owner = $this->owner;
        
        $hours = 0;
        
        if($owner->simaActivity_isActiveOnDay($day))
        {
            $hours = Yii::app()->activityManager->GetNeededWorkHoursInDay($day);
        }
        
        return $hours;
    }
    
    public function simaActivity_getCalendarShortText()
    {
        $text = $this->owner->DisplayName;
        return $text;
    }
    
    public function simaActivity_confirmedConditionScope($person_id)
    {
        $owner = $this->owner;
        
        $alias = $owner->getTableAlias();
                
        $owner->getDbCriteria()->mergeWith(array(
            'condition' => $alias.'.confirmed=TRUE'
        ));
           
        return $owner;
    }
    
    public function simaActivity_isActiveOnDay(Day $day)
    {
        $result = false;
        
        if(!$day->IsWeekend)
        {
            $owner = $this->owner;

            $is_on_date = false;

            $begin_day = Day::getByDate($owner->begin);
            $end_day = Day::getByDate($owner->end);

            $begin_day_compare = $begin_day->compare($day);
            $end_day_compare = $end_day->compare($day);

            if($begin_day_compare <= 0 && $end_day_compare >= 0)
            {
                $is_on_date = true;
            }

            if($is_on_date && $owner->confirmed === true
            )
            {
                $result = true;
                
                $person_activities_in_day = Yii::app()->activityManager->GetActivities(
                    $owner->employee->person, 
                    null,
                    $day,
                    true
                );

//                $to_calc_result = true;
                foreach($person_activities_in_day as $person_activity)
                {
                    if($person_activity->id !== $owner->id 
                        && $owner->comparePriorities($person_activity) < 0)
                    {
                        $result = false;
                        break;
                    }
                }
            }
        }
        
        return $result;
    }
    
    public function getWorkHours()
    {
        $owner = $this->owner;
        
        $person_activities_in_period = Yii::app()->activityManager->GetActivities(
                $owner->employee->person, 
                null,
                [$owner->begin, $owner->end],
                true
        );
                
        $result = 0;
        
        $dayModels = Day::getBetweenDates($owner->begin, $owner->end);
        foreach($dayModels as $dayModel)
        {
            $to_raise_result = true;
            
            foreach($person_activities_in_period as $person_activity)
            {
                if($person_activity->id !== $owner->id 
                    && $person_activity->simaActivity_isActiveOnDay($dayModel, true)
                    && $owner->comparePriorities($person_activity) < 0)
                {
                    $to_raise_result = false;
                    break;
                }
            }
            
            if($to_raise_result === true)
            {
                $work_hours_in_day = Yii::app()->activityManager->GetNeededWorkHoursInDay($dayModel);
                $result += $work_hours_in_day;
            }
        }        
        
        return $result;
    }
    
    public function simaActivity_afterSave()
    {
        $owner = $this->owner;
        
        $end_day = Day::getByDate($owner->end);
        $last_pay_day = PaycheckPeriodComponent::GetLastPayOutDayInFinalPeriod();
        
        /// krajnji datum je posle poslednje isplate
        if(isset($owner->employee->user) 
                && 
                (
                    empty($last_pay_day) /// ili nema potvrdjene isplate
                    || 
                    (!empty($last_pay_day) && $end_day->compare($last_pay_day) > 0) /// ili ima, ali je ona pre kraja naseg odmora
                )
        )
        {
            $begin_day = Day::getByDate($owner->begin);
            $day_to_set = $begin_day->prev();
            
            $user = $owner->employee->user;
            
            $last_confirm_day = $user->last_report_day;
            $last_boss_confirm_day = $user->last_boss_confirm_day;
            
            $have_dirty_data = false;
            
            /// pocetni datum odsustva je pre (ili na) potdjenog datuma
            if(!empty($last_boss_confirm_day) && $begin_day->compare($last_boss_confirm_day) <= 0 )
            {
                $user->last_boss_confirm_day_id = $day_to_set->id;
                $have_dirty_data = true;

//                $message = Yii::t('SIMAActivityManager.Activity', 'RevertedConfirmTitle', [
//                    '{date}' => $owner->day->day_date
//                ]);
//                Yii::app()->notifManager->sendNotif($user->id, $message, 
//                    array(
//                        'dialog_text' => Yii::t('SIMAActivityManager.Activity', 'RevertedConfirmDialogText', [
//                            '{date}' => $owner->day->day_date,
//                            '{additional_text}' => $user->DisplayName
//                        ])
//                    ),
//                    'simaActivity_RevertedConfirm_'.$owner->day->day_date
//                );
            }

            /// pocetni datum odsustva je pre (ili na) potdjenog datuma
            if(!empty($last_confirm_day) && $begin_day->compare($last_confirm_day) <= 0)
            {
                $user->last_report_day_id = $day_to_set->id;
                $have_dirty_data = true;

                $message = Yii::t('SIMAActivityManager.Activity', 'RevertedConfirmTitle', [
                    '{date}' => $day_to_set->day_date
                ]);
                Yii::app()->notifManager->sendNotif($user->id, $message, 
                    array(
                        'dialog_text' => Yii::t('SIMAActivityManager.Activity', 'RevertedConfirmDialogText', [
                            '{date}' => $day_to_set->day_date,
                            '{additional_text}' => $owner->DisplayName
                        ])
                    ),
                    'simaActivity_RevertedConfirm_'.$day_to_set->day_date
                );
            }
            
            if($have_dirty_data === true)
            {
                $user->save();
            }
        }
    }
}