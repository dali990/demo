<?php

class AbsenceSickLeaveActivityBehavior extends AbsenceActivityBehavior
{
    /// reqiured    
    
    protected $_simaActivity_display_class = '_absence _sick_leave';
    
    /// optional
    
    protected $_simaActivity_create_action = 'sima.hr.createAbsenceSickLeave';
    
    protected $_simaActivity_priorityNumber = 60;
}