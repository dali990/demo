<?php

/**
 * Description of CompanySectorTabs
 *
 * @author goran
 */
class CompanySectorTabs extends SIMAActiveRecordTabs 
{

    protected function tabsDefinition($code = null) 
    {
        $owner = $this->owner;
        $_is_director = $owner->isDirector(Yii::app()->user->model->person);
        $_can_change_companySchema = Yii::app()->user->checkAccess('CompanySchemaWrite');
        $_can_view_activity = Yii::app()->user->checkAccess('PersonActivityOverview');
        
        $sector_tabs = array(
            [
                'title' => Yii::t('LegalModule.Legal', 'OrganizationScheme'),
                'code' => 'organization_scheme'
            ]
        );
        
        if ($_can_change_companySchema)
        {
            $sector_tabs[] = [
                'title' => 'Radne pozicije',
                'code' => 'work_positions'
            ];
        }
        
        if ($_is_director)
        {
            $sector_tabs[] = [
                'title' => Yii::t('HRModule.CompanySector', 'WarnsByEmployeesOverview'),
                'code' => 'warns_by_employees_overview',
                'action' => 'HR/HR/warnsByEmployeesOverview',
                'get_params' => ['company_sector_id' => $owner->id]
            ];
            $sector_tabs[] = [
                'title' => 'Putni nalozi',
                'code' => 'travel_order',
                'params_function' => 'tabTravelOrder'
            ];
        }

        if ($_can_view_activity || $_is_director) 
        {
            $sector_tabs[] = [
                'title' => Yii::t('SIMAPersonActivityOverview.PersonActivityOverview', 'WorkHoursOverview'),
                'code' => 'company_sector_person_activity_overview'
            ];
            $sector_tabs[] = [
                'title' => 'Zadaci',
                'code' => 'tasks',
                'action' => 'jobs/task/tabTasks',
                'get_params' => array('id' => $owner->id, 'view' => 'sector'),
                'pre_path' => 'company_sector_person_activity_overview'
            ];
            $sector_tabs[] = [
                'title' => 'Aktivni zadaci',
                'code' => 'active_tasks',
                'params_function' => 'tabActiveTasks',
                'pre_path' => 'company_sector_person_activity_overview'
            ];
        }

        return $sector_tabs;
    }

    protected function tabTravelOrder() {
        $owner = $this->owner;
        return array(
            'model' => $owner
        );
    }

    protected function tabActiveTasks() {
        $owner = $this->owner;
        return array(
            'model' => $owner
        );
    }
}
