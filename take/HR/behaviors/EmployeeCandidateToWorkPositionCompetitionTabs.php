<?php

class EmployeeCandidateToWorkPositionCompetitionTabs extends SIMAActiveRecordTabs
{
    protected function tabsDefinition($code = null)
    {
        $owner = $this->owner;
        $tabs = array(
            array(
                'title' => 'Dokumenta',
                'code'=>'documents',
//                'params_function' => 'tabComments'
            ),
//            array(
//                'title' => Yii::t('MessagesModule.Common','Comments'),
//                'code'=>'comments',
//                'params_function' => 'tabComments'
//            ),
        );
        
        $jury = WorkPositionCompetitionJury::model()->findByAttributes([
            'work_position_competition_id' => $owner->work_position_competition_id,
            'jury_id' => Yii::app()->user->id
        ]);
        $is_jury = isset($jury); 
        if ($is_jury)
        {
            $tabs[] = [
                'title' => 'Ocene',
                'code'=>'grades',
                'params_function' => 'paramsGrades'
            ];
        }
        
        return $tabs;
    }
    
    protected function paramsGrades()
    {
        $owner = $this->owner;
        
        $work_position_competition_juries = WorkPositionCompetitionJury::model()->findByAttributes([
            'work_position_competition_id' => $owner->work_position_competition_id,
            'jury_id' => Yii::app()->user->id,
        ]);
        
        if (isset($work_position_competition_juries))
        {
            $grade = CompetitionGrade::model()->findByAttributes([
                'work_position_competition_juries_id' => $work_position_competition_juries->id,
                'employee_candidate_to_work_position_competition_id' => $owner->id
            ]);
            if (isset($grade))
            {
                $add_button = false;
            }
            else
            {
                $add_button = [
                    'init_data' => [
                        'CompetitionGrade' => [
                            'employee_candidate_to_work_position_competition_id' => $owner->id,
                            'work_position_competition_juries_id' => $work_position_competition_juries->id,

                        ]
                    ]
                ];
            }
        }
        else
        {
            $add_button = false;
        }
        
        return [
            'model' => $owner,
            'add_button' => $add_button,
        ];
    }


//    protected function tabComments()
//    {
//        $owner = $this->owner;
//        
//        $base_ct = CommentThread::getCommonForModel($owner);
//        
////        $base_ct->addListener($owner->responsible_id);
//        
//        return array(
//            'model' => $owner  ,
//            'comment_thread' => $base_ct
//        );
//    }
//    
//    protected function paramsFiles()
//    {
//        $owner = $this->owner;
//        
//        
//        return array(
//            'model' => $owner,
//        );
//    }     
}
