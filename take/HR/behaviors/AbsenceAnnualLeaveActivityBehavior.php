<?php

class AbsenceAnnualLeaveActivityBehavior extends AbsenceActivityBehavior
{
    /// reqiured    
    protected $_simaActivity_display_class = '_absence _annual_leave';
    
    /// optional
    protected $_simaActivity_create_action = 'sima.hr.createAbsenceAnnualLeave';
    
    protected $_simaActivity_priorityNumber = 10;
}