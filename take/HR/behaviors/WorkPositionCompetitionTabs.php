<?php

class WorkPositionCompetitionTabs extends SIMAActiveRecordTabs
{
    protected function tabsDefinition($code = null)
    {
        $owner = $this->owner;
        $tabs = array(
            array(
                'title'=>'Info&Stat',
                'code'=>'info',
                'params_function' => 'tabInfo'
            ),
            array(
                'title' => Yii::t('MessagesModule.Common','Comments'),
                'code'=>'comments',
                'params_function' => 'tabComments',
//                'selector_used' => true
            ),
            array(
                'title' => 'Kandidati',
                'code'=>'candidates',
                'params_function' => 'tabCandidates',
//                'selector_used' => true
            )
        );
        
        return $tabs;
    }
    
    protected function tabInfo()
    {
        $owner = $this->owner;
        
        if ($owner->is_graded)
        {
            $graded_msg = 'Sve je ocenjeno';
        }
        else
        {
            $graded_msg = 'Nije sve je ocenjeno';
        }
        
        
        return array(
            'model'=>$owner,
            'uniq' => SIMAHtml::uniqid(),
            'candidate_count' => count($owner->employee_candidates),
            'graded_msg' => $graded_msg
        );
    }
    
    protected function tabComments()
    {
        $owner = $this->owner;
        
        $comment_thread = CommentThread::getCommonForModel($owner);
//        if(isset($owner->submitter_id))
//        {
//            $comment_thread->addListener($owner->submitter_id);
//        }
//        if(isset($owner->debugger_id))
//        {
//            $comment_thread->addListener($owner->debugger_id);
//        }

        return array(
            'model' => $owner,
            'comment_thread'=>$comment_thread
        );
    }   
    
    protected function tabCandidates()
    {
        $owner = $this->owner;
        
        
        
        return array(
            'model' => $owner,
            'uniq' => SIMAHtml::uniqid()
        );
    }
}