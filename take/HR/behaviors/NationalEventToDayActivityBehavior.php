<?php

class NationalEventToDayActivityBehavior extends ActivityBehavior
{
    /// reqiured
    protected $_simaActivity_daily = true;
    
    protected $_simaActivity_date_filter = 'on_date';
    
//    protected $_simaActivity_display_class = '_national_event';
        
    protected $_simaActivity_checkConfirmDateColumn = 'day_id';
    
    public function simaActivity_getActivityHours(Day $day)
    {
        $owner = $this->owner;
        
        $hours = 0;
        
        if($day->id === $owner->day_id)
        {
            $hours = Yii::app()->activityManager->GetNeededWorkHoursInDay($day);
        }
        
        return $hours;
    }
    
    public function simaActivity_getCalendarShortText()
    {
        $result = $this->owner->national_event->DisplayName;
        return $result;
    }
    
    public function simaActivity_afterSave()
    {
        $owner = $this->owner;
        
        /// ne radimo za slave
        if($owner->national_event->type === NationalEvent::$TYPE_SLAVA)
        {
            return;
        }
        
        $owner->refresh();
        $event_day = $owner->day;
        $day_to_set = $event_day->prev();
        
        if(Yii::app()->isWebApplication())
        {
            /// TODO: prepraviti da se radi u sledecem sysevent-u, a ne novim ajaxlong-om
            Yii::app()->controller->raiseAction('sima.hr.nationalEventToDayAfterSaveRecheck',[
                $event_day->day_date,
                $owner->_before_save_day_date
            ], false);
        }
        else
        {
            error_log(__METHOD__.' - pozvano, a nije iz web app');
        }
        
        $last_pay_day = PaycheckPeriodComponent::GetLastPayOutDayInFinalPeriod();
        
        /// ne radimo ako nije posle poslednje isplate
        if(!is_null($last_pay_day) && $event_day->compare($last_pay_day) <= 0)
        {
            return;
        }
        
        $users = User::model()->findAll();
        
        foreach($users as $user)
        {
            $last_confirm_day = $user->last_report_day;
            $last_boss_confirm_day = $user->last_boss_confirm_day;
                
            $have_dirty_data = false;
            
            /**
             * sef ima potvrdjen dan i
             * dan dogadja je tada ili pre
             */
            if(!empty($last_boss_confirm_day) && $event_day->compare($last_boss_confirm_day) <= 0)
            {
                $user->last_boss_confirm_day_id = $day_to_set->id;
                $have_dirty_data = true;
                
//                $message = Yii::t('SIMAActivityManager.Activity', 'RevertedConfirmTitle', [
//                    '{date}' => $owner->day->day_date
//                ]);
//                Yii::app()->notifManager->sendNotif($user->id, $message, 
//                    array(
//                        'dialog_text' => Yii::t('SIMAActivityManager.Activity', 'RevertedConfirmDialogText', [
//                            '{date}' => $owner->day->day_date,
//                            '{additional_text}' => $user->DisplayName
//                        ])
//                    ),
//                    'simaActivity_RevertedConfirm_'.$owner->day->day_date
//                );
            }
            
            /**
             * zaposleni je potvrdio
             * dan dogadjaja je pre (ili na) potdjenog datuma
             */
            if(!empty($last_confirm_day) && $event_day->compare($last_confirm_day) <= 0)
            {
                $user->last_report_day_id = $day_to_set->id;
                $have_dirty_data = true;
                
                $message = Yii::t('SIMAActivityManager.Activity', 'RevertedConfirmTitle', [
                    '{date}' => $owner->day->day_date
                ]);
                Yii::app()->notifManager->sendNotif($user->id, $message, 
                    array(
                        'dialog_text' => Yii::t('SIMAActivityManager.Activity', 'RevertedConfirmDialogText', [
                            '{date}' => $owner->day->day_date,
                            '{additional_text}' => $owner->DisplayName
                        ])
                    ),
                    'simaActivity_RevertedConfirm_'.$day_to_set->day_date
                );
            }
            
            if($have_dirty_data === true)
            {
                $user->save();
            }
        }
    }
    
    public function simaActivity_afterDelete()
    {
        $this->simaActivity_afterSave();
    }
    
    public function simaActivity_isActiveOnDay(Day $day)
    {
        $result = false;
        
        if(!$day->IsWeekend)
        {
            $owner = $this->owner;

            $event_day = Day::getByDate($owner->day);

            if($event_day->compare($day) == 0)
            {
                $result = true;
            }
        }
        
        return $result;
    }
}