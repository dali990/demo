<?php

class EmployeeCandidateTabs extends SIMAActiveRecordTabs
{
    protected function tabsDefinition($code = null)
    {
        $owner = $this->owner;
        $tabs = array(
            array(
                'title' => 'Info',
                'code'=>'info',
//                'params_function' => 'tabComments'
            ),
            array(
                'title' => Yii::t('MessagesModule.Common','Comments'),
                'code'=>'comments',
                'params_function' => 'tabComments'
            ),
            array(
                'title' => 'Fajlovi',
                'code'=>'files',
                'params_function' => 'paramsFiles'
            ),
            
        );
        
        
        return $tabs;
    }
    
    protected function tabComments()
    {
        $owner = $this->owner;
        
        $base_ct = CommentThread::getCommonForModel($owner);
        
//        $base_ct->addListener($owner->responsible_id);
        
        return array(
            'model' => $owner  ,
            'comment_thread' => $base_ct
        );
    }
    
    protected function paramsFiles()
    {
        $owner = $this->owner;
        
        
        return array(
            'model' => $owner,
        );
    }     
}
