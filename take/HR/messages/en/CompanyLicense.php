<?php

return [
    'CompanyLicenseActive'=>'Active',
    'CompanyLicensesByCompanies' => 'Company licenses by companies',
    'AlreadyAddedReferences' => 'The next references already added:',
    'CompanyLicensesByDecisions' => 'Company licenses by decisions',
    'CompanyLicenseAlreadyExist'=>'There is already a license {license_name} to company {company_name}. It is possible that it is inactive and then try to activate it.',
    'CompanyLicenseDecisionAutomaticallyFill'=>'Automatically fill with the current state of company licenses',
    'CompanyLicenseDecisionToCompanyLicenseAlreadyExist'=>'There is already a company license {company_license_name} to company license decision {company_license_decision_name}.',
    'CompanyLicenseDecisionCheckDisagreements'=>'Check disagreements',
    'CompanyLicenseDecisionCheckDisagreementsWithCurrState'=>'Check disagreements with current company licenses state',
    'CompanyLicenseDecisionDisagreementsTitle'=>'The differences between the company licenses on the decision {company_license_decision_name} and in the company {company_name}',
    'CompanyLicenseDecisionDisagreementsCompanyLicense'=>'Company license',
    'CompanyLicenseDecisionDisagreementsOperation'=>'Operation',
    'CompanyLicenseDecisionDisagreementsAdd'=>'will be added',
    'CompanyLicenseDecisionDisagreementsRemove'=>'will be removed',
    'CompanyLicenseDecisionDisagreementsView'=>'The differences between the company licenses on the last decision and in the current company',
    'NoCompanyLicenseDecision'=>'There is no company license decisions.',
    'CompanyLicensesOwnsByCompany'=>'Company licenses owns by company {company_name}',
    'LastCompanyLicenseDecision'=>'Last company license decision',
    'CompanyLicensesOnDecision'=>'Company licenses on decision',
    'CompanyLicensesDecision'=>'Company licenses decision',
    'CompanyLicenses'=>'Company licenses',
    'Decision'=>'Decision',
    'CompanyLicensesToCompanies' => 'Company licenses to companies',
    'Companies' => 'Companies',
    'CompanyLicensesByCompanies2' => 'Company licenses to companies 2'
];