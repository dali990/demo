<?php

return [
    'MedicalExaminationReferral' => 'Medical examination referral',
    'MedicalExaminationReferrals' => 'Medical examination referrals',
    'Person' => 'Person',
    'WorkPosition' => 'Work position',
    'ReferralType' => 'Referral type',
    'MedicalExaminationReferralDocumentType' => 'Medical examination referral',
    'MedicalExaminationReferralDocumentTypeDesc' => 'Medical examination referral document type',
    'ForEmployee' => 'For employee'
];