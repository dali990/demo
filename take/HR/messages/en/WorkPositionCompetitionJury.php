<?php

return [
    'JuryAddedNotif' => 'You are added to the jury at the work position competition "{work_position_competition_name}"',
    'ExpiredWorkPositionCompetitionWithoutCompetitionGrade' => 'You have candidates for review at the expiring work position competition "{work_position_competition_name}"'
];