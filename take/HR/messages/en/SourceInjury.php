<?php

return [
    'SourceInjury' => 'Source injury',
    'SourceInjuries' => 'Source injuries',
    'Code' => 'Code',
    'Meaning' => 'Meaning',
    'Note' => 'Note',
    'Type' => 'Type'
];

