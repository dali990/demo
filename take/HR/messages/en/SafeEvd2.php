<?php

return [
    'MedicalReport' => 'Medical report',
    'MedicalReports' => 'Medical reports',
    'MedicalReportFilename' => 'Medical report{report_code} for {person_name}',
    'SafeEvd2DocumentType' => 'Document type for form 2',
];