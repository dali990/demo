<?php

return [
    'ModeInjury' => 'Mode injury',
    'ModeInjuries' => 'Mode injuries',
    'Code' => 'Code',
    'Meaning' => 'Meaning',
    'Note' => 'Note',
    'Type' => 'Type'
];

