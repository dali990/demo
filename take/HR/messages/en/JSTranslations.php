<?php

return [
    'CompanyLicenseDecisionCheckDisagreements'=>'Check disagreements',
    'CompanyLicenseDecisionApplyDisagreements'=>'Apply',
    'CompanyLicenseDecisionNoDisagreements'=>'No disagreements',
    'CompanyLicenseDecisionApplied'=>'Applied!'
];