<?php

return [
    'Occupation' => 'Occupation',
    'Occupations' => 'Occupations',
    'Code' => 'Code',
    'Parent' => 'Parent',
    'SyncOccupations' => 'Sync occupations',
    'ToDeleteOccupationDeleteTheFollowingSubtypes' => 'To delete occupation, you need to delete the following subtypes: <br>',
    'HROccupationPrivilege' => 'Privilege to show occupations',
];