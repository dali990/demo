<?php

return [
    'CompetitionGradeUniq' => '{competition_jury} has already assessed {competition_candidate}',
    'STATUS_CONFIRMED_INTERVIEW' => 'Confirmed interview',
    'IsGradedByCurrUser' => 'Filter the grade',
    'HasCVGradeByCurrUser' => 'I graded CV',
    'HasNotCVGradeByCurrUser' => 'I did not grade CV',
    'HasInterviewGradeByCurrUser' => 'I graded interview',
    'HasNotInterviewGradeByCurrUser' => 'I did not grade interview'
];