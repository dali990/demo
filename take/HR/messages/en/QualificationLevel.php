<?php

return [
    'QualificationLevel' => 'Qualification level',
    'QualificationLevels' => 'Qualification levels',
    'SyncQualificationLevels' => 'Sync qualification levels',
    'LevelAndType' => 'Level and type',
    'Code' => 'Code',
    'Description' => 'Description',
];

