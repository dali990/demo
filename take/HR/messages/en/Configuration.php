<?php

return array(
    'HR' => 'Human resources',
    'WorkContracts' => 'Work Contracts',
    'IntroductionIntoBusinessProgramDocumentTypeConfiguration' => 'Job introduction program',
    'IntroductionIntoBusinessProgramDocumentTypeConfigurationDescription' => 'Job introduction program',
    'IntroductionIntoBusinessProgramTemplateConfiguration' => 'Template for a job introduction program',
    'IntroductionIntoBusinessProgramTemplateConfigurationDescription' => 'Template for a job introduction program',
    'Absence' => 'Absence',
    'Absences' => 'Absences',
    'NationalEvent' => 'Holiday',
    'NationalEvents' => 'Holidays',
    'CompanyLicenseDecisionDocumentTypeId' => 'Document type for large-scale solution',
    'RevertConfirmedAbsenceOnNationalEventAdd' => 'Undelete credentials after adding holidays',
    'RevertConfirmedAbsenceOnNationalEventAddDescription' => 'When a holiday is added, and there is some confirmed absence on that day, whether to cancel a confirmation of absence',
    'BenefitedWorkExperience' => 'Possibility of choosing a paid work experience in the SVP field',
    'BenefitedWorkExperienceDescription' => 'You can select a pay-as-you-earned service while entering the SVP number'
);


