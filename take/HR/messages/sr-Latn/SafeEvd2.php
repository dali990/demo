<?php

return [
    'MedicalReport' => 'Lekarski izveštaj',
    'MedicalReports' => 'Lekarski izveštaji',
    'MedicalReportFilename' => 'Lekarski izveštaj{report_code} za {person_name}',
    'SafeEvd2DocumentType' => 'Tip dokumenta za obrazac 2',
];