<?php

return [
    'SourceInjury' => 'Izvor povrede',
    'SourceInjuries' => 'Izvori povreda',
    'Code' => 'Šifra',
    'Meaning' => 'Značenje',
    'Note' => 'Napomena',
    'Type' => 'Tip'
];

