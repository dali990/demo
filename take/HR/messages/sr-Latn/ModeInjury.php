<?php

return [
    'ModeInjury' => 'Uzrok povrede',
    'ModeInjuries' => 'Uzroci povreda',
    'Code' => 'Šifra',
    'Meaning' => 'Značenje',
    'Note' => 'Napomena',
    'Type' => 'Tip'
];

