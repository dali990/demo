<?php

return [
    'Occupation' => 'Zanimanje',
    'Occupations' => 'Zanimanja',
    'Code' => 'Šifra',
    'Parent' => 'Nadtip',
    'SyncOccupations' => 'Sinhronizacija zanimanja',
    'ToDeleteOccupationDeleteTheFollowingSubtypes' => 'Da bi ste obrisali zanimanje, morate obrisati sledeće podtipove: <br>',
    'HROccupationPrivilege' => 'Privilegija za prikaz zanimanja',
];

