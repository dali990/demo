<?php

return [
    'CompanyLicenseActive'=>'Aktivna',
    'CompanyLicensesByCompanies' => 'Velike licence po kompanijama',
    'AlreadyAddedReferences' => 'Sledeće reference su već dodate:',
    'CompanyLicensesByDecisions' => 'Velike licence po rešenjima',
    'CompanyLicenseAlreadyExist'=>'Već postoji licenca {license_name} za kompaniju {company_name}. Moguće da nije aktivna pa pokušajte da je aktivirate.',
    'CompanyLicenseDecisionAutomaticallyFill'=>'Popuni automatski sa trenutnim stanjem velikih licenci',
    'CompanyLicenseDecisionToCompanyLicenseAlreadyExist'=>'Već postoji velika licenca {company_license_name} za rešenje {company_license_decision_name}.',
    'CompanyLicenseDecisionCheckDisagreements'=>'Proveri razlike',
    'CompanyLicenseDecisionCheckDisagreementsWithCurrState'=>'Proveri neslaganja sa trenutnim stanjem velikih licenci',
    'CompanyLicenseDecisionDisagreementsTitle'=>'Razlike izmedju velikih licenci na rešenju {company_license_decision_name} i koje poseduje kompanija {company_name}',
    'CompanyLicenseDecisionDisagreementsCompanyLicense'=>'Velika licenca',
    'CompanyLicenseDecisionDisagreementsOperation'=>'Operacija',
    'CompanyLicenseDecisionDisagreementsAdd'=>'dodaje se',
    'CompanyLicenseDecisionDisagreementsRemove'=>'skida se',
    'CompanyLicenseDecisionDisagreementsView'=>'Razlika velikih licenci u trenutnoj kompaniji i onih koje se nalaze na poslednjem rešenju',
    'NoCompanyLicenseDecision'=>'Ne postoji nijedno rešenje.',
    'CompanyLicensesOwnsByCompany'=>'Velike licence koje poseduje kompanija {company_name}',
    'LastCompanyLicenseDecision'=>'Poslednje rešenje',
    'CompanyLicensesOnDecision'=>'Velike licence koje se nalaze na rešenju',
    'CompanyLicensesDecision'=>'Rešenje o velikim licencama',
    'CompanyLicenses'=>'Velike licence',
    'Decision'=>'Rešenje',
    'CompanyLicensesToCompanies' => 'Velike licence po kompanijama',
    'Companies' => 'Kompanije',
    'CompanyLicensesByCompanies2' => 'Velike licence po kompanijama 2'
];

