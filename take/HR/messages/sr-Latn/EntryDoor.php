<?php

return [
    'Card' => 'Broj kartice',
    'Partner' => 'Partner',
    'Ocupied' => 'Zauzeta',
    'TYPE_ENTRY' => 'Ulaz',
    'TYPE_EXIT' => 'Izlaz',
    'NoCardsToEnterFor' => 'Nema definisanih kartica po kojima bi se dovukli podaci',
    'NotConfirmedCardUsed' => 'Nepotvrdjena kartica sa brojem {card_number} koriscena {used_count} puta',
    'AdditionalData' => 'Dodatni podaci',
    'EntryDoorLog' => 'Evidencija ulazaka/izlazaka',
    'EntryDoorLogs' => 'Evidencije ulazaka/izlazaka',
    'DBConnectionFail' => 'Nije mogla da se otvori veza ka bazi:<br>{errmsg}',
    'EmployeeOfficeEntryLogs' => 'Evidencija ulazaka/izlazaka',
    'EmployeeOfficeEntryLogCards' => 'Kartice',
    'GetNewEmployeeOfficeEntryLogs' => 'Dovuci nove ulaske/izlaske',
    'GetNonExistingEmployeeOfficeEntryLogCards' => 'Dovuci nepostojece kartice',
];