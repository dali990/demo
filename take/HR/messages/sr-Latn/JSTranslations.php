<?php

return [
    'CompanyLicenseDecisionCheckDisagreements'=>'Provera razlika',
    'CompanyLicenseDecisionApplyDisagreements'=>'Primeni',
    'CompanyLicenseDecisionNoDisagreements'=>'Nema razlika',
    'CompanyLicenseDecisionApplied'=>'Primenjeno!'
];