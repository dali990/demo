<?php

return [
    'QualificationLevel' => 'Nivo kvalifikacije',
    'QualificationLevels' => 'Nivoi kvalifikacija',
    'SyncQualificationLevels' => 'Sinhronizujte nivoe kvalifikacija',
    'LevelAndType' => 'Nivo i vrsta',
    'Code' => 'Šifra',
    'Description' => 'Opis'
];

