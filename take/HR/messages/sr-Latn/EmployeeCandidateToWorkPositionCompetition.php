<?php

return [
    'STATUS_APPLIED' => 'Prijavio se',
    'STATUS_NOT_INVITED_TO_INTERVIEW' => 'Nije pozvan',
    'STATUS_TO_INVITE_TO_INTERVIEW' => 'Pozvati',
    'STATUS_INVITED_TO_INTERVIEW' => 'Pozvan',
    'STATUS_CONFIRMED_INTERVIEW' => 'Potvrđen intervju',
    'STATUS_NOT_CAME' => 'Nije dosao',
    'STATUS_DONE_INTERVIEW' => 'Odradjen intervju',
    'STATUS_NOT_PASSED_INTERVIEW' => 'Nije prosao',
    'STATUS_OFFERED_JOB' => 'Ponudjen posao',
    'STATUS_REJECTED_JOB' => 'Nije prihvatio posao',
    'STATUS_ACCEPTED_JOB' => 'Prihvatio posao',
    'CompetitionGradeUniq' => '{competition_jury} je već ocenio {competition_candidate}',
    'IsGradedByCurrUser' => 'Filtriraj ocenu',
    'HasCVGradeByCurrUser' => 'Ocenio sam CV',
    'HasNotCVGradeByCurrUser' => 'Nisam ocenio CV',
    'HasInterviewGradeByCurrUser' => 'Ocenio sam intervju',
    'HasNotInterviewGradeByCurrUser' => 'Nisam ocenio intervju'
];