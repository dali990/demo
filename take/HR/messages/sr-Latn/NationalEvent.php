<?php

return [
    'DateInvalidFormat' => 'Datum je nepravilnog formata ({dan}.{mesec}.)',
    'StateHoliday' => 'Drzavni praznik',
    'StateHolidays' => 'Drzavni praznici',
    'ReligiousHoliday' => 'Verski praznik',
    'ReligiousHolidays' => 'Verski praznici',
    'ForPayout' => 'Za isplatu',
    'NationalEvent' => 'Nacionalni dogadjaj',
    'NationalEventTitle' => 'Praznici',
    'NationalEventDay' => 'Nacionalni dogadjaj na dan',
    'NationalEventDayTitle' => 'Aktivni nacionalni dogadjaji',
    'AutoFillForCurrentYear' => 'Popuni za tekucu godinu',
    'AutoFillForCurrentYearTitle' => 'Praznici sa postavljenim datumima',
    'NumberOfUnusedNationalEvents' => 'Broj nekoriscenih dogadjaja',
    'NumberOfUnusedNationalEventsWithNotNullDates' => 'Broj nekoriscenih dogadjaja sa postavljenim datumima',
    'EmployeesHaveThisSlava' => 'Postoje zaposleni koji imaju ovaj praznik kao slavu',
    'NationalEventOnDateAlreadyExists' => 'Praznik {event} vec postoji na datum {date}',
    'StateAndReligiousHolidays' => 'Državni i verski praznici'
];
