<?php

return array(
    'HR' => 'Ljudski resursi',
    'WorkContracts' => 'Ugovori o radu',
    'IntroductionIntoBusinessProgramDocumentTypeConfiguration' => 'Program uvodjenja u posao',
    'IntroductionIntoBusinessProgramDocumentTypeConfigurationDescription' => 'Program uvodjenja u posao',
    'IntroductionIntoBusinessProgramTemplateConfiguration' => 'Template za program uvodjenja u posao',
    'IntroductionIntoBusinessProgramTemplateConfigurationDescription' => 'Template za program uvodjenja u posao',
    'Absence' => 'Odsustvo',
    'Absences' => 'Odsustva',
    'NationalEvent' => 'Praznik',
    'NationalEvents' => 'Praznici',
    'CompanyLicenseDecisionDocumentTypeId'=>'Tip dokumenta za rešenje o velikim licencama',
    'RevertConfirmedAbsenceOnNationalEventAdd' => 'Ponisti potvrde odstustva nakon dodavanja praznika',
    'RevertConfirmedAbsenceOnNationalEventAddDescription' => 'Kada se doda praznik, a postoji neko potvrdjeno odsustvo na taj dan, da li ponistiti potvrdu odsustva',
    'BenefitedWorkExperience' => 'Mogućnost biranja beneficirnanog radnog staža u ŠVP polju',
    'BenefitedWorkExperienceDescription' => 'Dozvoljava se odabir beneficiranog radnog staža pri unosu ŠVP broja'
);


