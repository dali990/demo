<?php

return [
    'JuryAddedNotif' => 'Dodati ste u žiriju na konkursu za posao "{work_position_competition_name}"',
    'ExpiredWorkPositionCompetitionWithoutCompetitionGrade' => 'Imate kandidate za pregled na isteklom konkursu za posao "{work_position_competition_name}"'
];