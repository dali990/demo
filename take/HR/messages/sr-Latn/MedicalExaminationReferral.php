<?php

return [
    'MedicalExaminationReferral' => 'Uput za lekarski pregled',
    'MedicalExaminationReferrals' => 'Uputi za lekarski pregled',
    'Person' => 'Osoba',
    'WorkPosition' => 'Radna pozicija',
    'ReferralType' => 'Tip uputa',
    'MedicalExaminationReferralDocumentType' => 'Uput za lekarski pregled',
    'MedicalExaminationReferralDocumentTypeDesc' => 'Tip dokumenta za uput za lekarski pregled',
    'ForEmployee' => 'Za zaposlenog'
];