<?php
/**
 * Description of PersonToWorkLicenseGUI
 *
 * @author goran
 */
class PersonToWorkLicenseGUI extends SIMAActiveRecordGUI
{
   public function columnLabels()
   {
        return array(
            'DisplayName'=>'Fajl',
            'person_id'=>'Vlasnik',
            'working_license_id'=>'Licenca',
            'number'=>'Broj',
            'start_date'=>'Početak važenja',
            'end_date'=>'Kraj važenja',
            'file_id'=>'Dokument',
            'code'=>'Oznaka',
            'conditions'=>'Uslovi',
            'description'=>'Opis',
            'issuer_name'=>'Izdavač',
 //           'person_name'=>'Vlasnik',
            'file_version_id'=>'Fajl',
            'person' => 'Osoba',
            'working_license' => 'Licenca',
            'expiration_period' => 'Period isticanja licence',
            'license_manual_wiki' => 'Uputstvo',
            'file'=>'Fajl'
        )+parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return Yii::t('HRModule.PersonToWorkLicense', $plural ? 'PersonsToWorkLicenses' : 'PersonToWorkLicense');
    }
    
    public function modelViews()
    {
        return array(
            'inFile'
        )+parent::modelViews();
    }
    
    public function columnDisplays($column)
    {
        $owner = $this->owner;
        switch ($column)
        {
            case 'person_name': return $owner->person . SIMAHtml::modelDialog(Person::model()->findByPk($owner->person->id));
            case 'issuer_name': return $owner->working_license->issuer . SIMAHtml::modelDialog(Company::model()->findByPk($owner->working_license->issuer->id));
            case 'expiration_period':
                $return = '';
                $end_date = $owner->end_date;                
                if (!empty($end_date))
                {
                    $curr_date = new DateTime("now");
                    $end_date_time = new DateTime($end_date);
                    $interval = $curr_date->diff($end_date_time);
                    $years = $interval->y;
                    $mounths = $interval->m;
                    $days = $interval->d;
                    $period = '';
                    if ($years !== 0)
                    {
                        if ($years === 1)
                            $period .= "$years godinu, ";
                        else
                            $period .= "$years godina, ";
                    }
                    if ($mounths !== 0)
                    {
                        if ($mounths === 1)
                            $period .= "$mounths mesec, ";
                        else
                            $period .= "$mounths meseca, ";
                    }
                    if ($days !== 0)
                    {
                        if ($days === 1)
                            $period .= "$days dan. ";
                        else
                            $period .= "$days dana. ";
                    }
                    //ako nema dana onda skidamo zarez na kraju i dodajemo tacku
                    else if ($period !== '')
                    {                        
                        $period = rtrim(trim($period), ",").'.';
                    }

                    if ($curr_date > $end_date_time)
                    {
                        $return = 'Istekla je pre '.$period;
                    }
                    else if ($curr_date <= $end_date_time)
                    {
                        $return = 'Ističe za '.$period;
                    }
                    else
                    {
                        $return = 'Istekla je.';
                    }
                }
                return $return;
            case 'license_manual_wiki':
                return '<span style="cursor:pointer; color:blue;" onclick="sima.icons.personalWiki($(this),\'Licence\')" target="_blank">Uputstvo</span>';
            default: return parent::columnDisplays($column);
        }
    }


    public function modelForms()
    {
        return array(
            'default'=>array(
                'type'=>'generic',
                'columns'=>array(
                    'id'=>'hidden', //mora postojati u svakoj formi
                    'number'=>'textField',
                    'file_version_id'=>'fileField',
                    'person_id'=>array('searchField','relName'=>'person'),
                    'working_license_id'=>array('searchField','relName'=>'working_license'),
                    'start_date'=>'datetimeField',
                )
            ),
            );
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            case 'work_licenses': return array(
                'columns'=>array('person', 'person.company', 'number', 'start_date', 'end_date', 
                    'person.national_regulations_and_standards','person.new_materials_technologies_in_nacional_ind',
                    'person.international_regulations_and_standards','person.new_materials_technologies_in_internacional_ind',
                    'person.activePeriod','person.lastYear'),
            );
            case 'person_to_work_license': return array(
                'columns'=>array('working_license.code', 'working_license', 'issuer_name', 'number', 'start_date', 'end_date'),
            );
            case 'expiration_licenses': return array(
                'columns'=>array('working_license', 'working_license.issuer', 'person', 'end_date', 'expiration_period', 'license_manual_wiki')                
            );
            default: return array(
                'columns'=>array('working_license', 'person', 'start_date', 'end_date', 'file'),
            );
        }
    }
    
    public function droplist($key, $relName='', $filter=null) 
    {    
        switch ($key) 
        {    
            case 'expiration_period': return array('0' => 'Ističu u roku od 5 dana', '1' => 'Ističu u roku od 10 dana', '2' => 'Ističu u roku od mesec dana');
            default: return parent::droplist($key, $relName, $filter);
        }
    }
}
