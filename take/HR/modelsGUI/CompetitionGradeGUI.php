<?php

class CompetitionGradeGUI extends SIMAActiveRecordGUI
{

    public function columnLabels()
    {
        return array(
            'cv_grade' => 'Ocena za CV',
            'interview_grade' => 'Ocena sa intervjua',
            'cv_text_grade' => 'Beleska za CV',
            'interview_text_grade' => 'Beleska sa intervjua',
            
        )+parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return 'Ocena kandidata';
    }

//    public function modelViews()
//    {
//        return array(
//            'basicInfoTitle',
//            'basicInfo',
//            'current_materials_status'=>array('params_function'=>'viewParamsCurrentMaterialsStatus')
////            'leaf'=>array(
//////                'style'=>'border: 1px solid;'
////                ),
////            'default_edit'
////            'tabTemplates'=>array('with'=>array('templated_file'),'style'=>'border: 1px solid;')  //.creator
//        );
//    }
    
   
//    public function columnDisplays($column)
//    {
//        $owner = $this->owner;
//        switch($column)
//        {
//            case 'status_id' : return $this->droplist('status_id')[$owner->$column];
//            default: return parent::columnDisplays($column);
//        }
//        
//    }
    
    public function modelForms()
    {
        return array(
            'default'=>array(
                'type'=>'generic',
                'columns'=>array(
                    'employee_candidate_to_work_position_competition_id' => 'hidden',
                    'work_position_competition_juries_id' => 'hidden',
                    'cv_grade' => ['dropdown', 'empty' => 'Nije ocenjen'],
                    'cv_text_grade' => 'textArea',
                    'interview_grade' => ['dropdown', 'empty' => 'Nije ocenjen'],
                    'interview_text_grade' => 'textArea',
//                    'id'=>array('searchField','relName'=>'person','add_button'=>true),
//                    'active' => 'checkBox',
//                    'school_course_id' => ['relation','relName'=>'school_course','add_button'=>true],
//                    'work_position_competitions'=>array('list',
//                        'model'=>'WorkPositionCompetition',
//                        'relName'=>'work_position_competitions',
//                        'multiselect' => true
//                    ),
//                    'grade_id'=>array('dropdown','empty' => 'Nije ocenjen'),
//                    'status_id' => array('dropdown'),
//                    'interview_time' => array('datetimeField','mode'=>'datetime'),
//                    'partner_id'=>array('relation','relName'=>'partner'),
//                    'note'=>'textArea'
                )
            )
        );
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            case 'inCandidate': return array(
                'columns' => array(
                    'cv_grade',
                    'cv_text_grade',
                    'interview_grade',
                    'interview_text_grade'
                ),
                'order_by' => array('cv_grade','interview_grade')
            );
            default: return array(
                'columns' => array(
                    'employee_candidate_to_work_position_competition.employee_candidate', 
                    'work_position_competition_juries.jury', 
                    'cv_grade',
                    'cv_text_grade',
                    'interview_grade',
                    'interview_text_grade'
                ),
                'order_by' => array('cv_grade','interview_grade')
            );
        }
    }
    
    public function droplist($key, $relName='', $filter=null)
    {
        switch ($key)
        {
            case 'cv_grade': case 'interview_grade':
                return array(
                    '1' => '1',
                    '2' => '2',
                    '3' => '3',
                    '4' => '4',
                    '5' => '5',
                    '6' => '6',
                );
           
            default: return parent::droplist($key, $relName, $filter);
        }
    }
    
}