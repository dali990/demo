<?php

class NationalEventGUI extends SIMAActiveRecordGUI
{
    public function columnLabels() {
        return array(
            '_day_date'=>Yii::t('BaseModule.Common', 'Day')
        )+parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return Yii::t('HRModule.NationalEvent', 'NationalEvent');
    }
    
    public function columnDisplays($column)
    {
        $owner = $this->owner;
        switch ($column)
        {
            case 'type':                
                $type_displays = $this->owner->getTypesDisplayArray();
                $result = $type_displays[$owner->type];
                return $result;
            default: return parent::columnDisplays($column); 
        }
    }
    
    public function modelForms()
    {
        return array(
            'default'=>array(
                'type'=>'generic',
                'columns'=>array(
                    'name' => 'textField',
                    'type' => array('dropdown', 'empty' => Yii::t('BaseModule.Common', 'Choose')),
                    '_day_date' => 'datetimeField',
                )
            )
        );
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default: return [
                'columns'=>[
//                    'DisplayName', 
                    'name', 
                    'type', 
                    'date',
                ]
            ];
        }
    }
    
    public function droplist($key, $relName='', $filter=null) 
    {
        switch ($key) 
        {
            case 'type':
                $type_displays = $this->owner->getTypesDisplayArray();
                return $type_displays;
            default: 
                return parent::droplist($key, $relName, $filter);
        }
    }
}