<?php

/**
 * Description of WorkPositionGUI
 *
 * @author goran
 */
class WorkPositionGUI extends SIMAActiveRecordDisplayGUI
{

    public function columnLabels()
    {
        return array(
            'code' => Yii::t('HRModule.WorkPosition','Code'),
            'name' =>  Yii::t('HRModule.WorkPosition','Name'),
            'description' => Yii::t('HRModule.WorkPosition','Description'),
            'company_sector_id' => Yii::t('HRModule.WorkPosition', 'CompanySector'),
            'enlarged_risk' => Yii::t('HRModule.WorkPosition','EnlargedRisk'),
            'reason_of_training' => Yii::t('HRModule.WorkPosition','ReasonOfTraining'),          
            'add_risks' => Yii::t('HRModule.WorkPosition','AddRisks'),
            'safe_actions' => Yii::t('HRModule.WorkPosition','SafeActions'),
            'notice_safe' => Yii::t('HRModule.WorkPosition','NoticeSafe'),
            'medical_report_interval' => Yii::t('HRModule.WorkPosition','MedicalReportInterval'),
            'paygrade_id' => Yii::t('HRModule.WorkPosition','PaygradeId'),
            'is_director' => Yii::t('HRModule.WorkPosition','IsDirector'),
            'description_of_activities' => Yii::t('HRModule.WorkPosition','DescriptionOfActivities'),
            'payment_amount_id' => Yii::t('HRModule.WorkPosition','PaymentAmountId'),
            'payment_amount' => Yii::t('HRModule.WorkPosition','PaymentAmount'),
            'add_required_medical_abilities' => Yii::t('HRModule.WorkPosition','AddRequiredMedicalAbilities'),
            'benefited_work_experience' => Yii::t('HRModule.WorkPosition','BenefitedWorkExperience'),
            'occupations' => Yii::t('HRModule.WorkPosition','Occupations'),
            'add_occupations' => Yii::t('HRModule.WorkPosition','AddOccupations'),
        ) + parent::columnLabels();
    }

    public function modelLabel($plural = false)
    {
        return 'Pozicija';
    }
    
    public function modelViews()
    {
        $owner = $this->owner;        
        return array(
            'positionStructure' => array(                
                'html_wrapper' => null
            ),
            'basic_info',
            'full_info',
            'full_info_solo',
            'basic_info_from_organization_scheme'
        ) + parent::modelViews();
    }
    
    public function columnDisplays($column) 
    {
        $owner = $this->owner;
        switch ($column) 
        {
            case 'description_of_activities_html':
                if (empty($owner->description_of_activities))
                {
                    return '<p></p>';
                }
                else
                {
                    return $owner->description_of_activities;
                }
            case 'enlarged_risk':
                $return = ($owner->enlarged_risk)?'Da':'Ne';
                return $return;
            case 'add_description_of_activities':                
                $title = $owner->getAttributeLabel('description_of_activities');
                $icon = ($owner->description_of_activities === '')?'_add':'_edit';                
                return "<span class='sima-icon $icon _16' onclick='sima.model.openTinyMce(\"WorkPosition\", $owner->id, \"description_of_activities\", \"$title\");'></span>";
            case 'add_safe_actions':
                $title = $owner->getAttributeLabel('safe_actions');
                $icon = ($owner->safe_actions === '')?'_add':'_edit';
                return "<span class='sima-icon $icon _16' onclick='sima.model.openTinyMce(\"WorkPosition\", $owner->id, \"safe_actions\", \"$title\");'></span>";
            case 'add_notice_safe':
                $title = $owner->getAttributeLabel('notice_safe');
                $icon = ($owner->notice_safe === '')?'_add':'_edit';
                return "<span class='sima-icon $icon _16' onclick='sima.model.openTinyMce(\"WorkPosition\", $owner->id, \"notice_safe\", \"$title\");'></span>";
            case 'add_risks':
                $work_position_risks = SafeEvd1::model()->findAllByAttributes(array('work_position_id'=>$owner->id));
                $work_position_risks_ids = [];
                foreach ($work_position_risks as $work_position_risk)
                {
                    $curr = [];
                    $curr['id'] = $work_position_risk->work_danger_id;
                    $curr['display_name'] = $work_position_risk->work_danger->DisplayName;
                    array_push($work_position_risks_ids, $curr);
                }
                $work_position_risks_ids_json = CJSON::encode($work_position_risks_ids);
                return "<span class='sima-icon _search_form _16' onclick='sima.hr.addWorkPositionRisks(".$owner->id.",$work_position_risks_ids_json);'></span>";
            case 'add_required_medical_abilities':
                $work_position_medical_abilities = RequiredMedicalAbilityToWorkPosition::model()->findAllByAttributes(array('work_position_id'=>$owner->id));
                $work_position_medical_abilities_ids = [];
                foreach ($work_position_medical_abilities as $work_position_medical_ability)
                {
                    $curr = [];
                    $curr['id'] = $work_position_medical_ability->required_medical_ability_id;
                    $curr['display_name'] = $work_position_medical_ability->required_medical_ability->DisplayName;
                    array_push($work_position_medical_abilities_ids, $curr);
                }
                $work_position_medical_abilities_ids_json = CJSON::encode($work_position_medical_abilities_ids);
                return "<span class='sima-icon _search_form _16' onclick='sima.hr.addWorkPositionMedicalAbilities(".$owner->id.",$work_position_medical_abilities_ids_json);'></span>";;
            case 'occupations':
                $occupations = '<ul>';
                foreach ($owner->work_position_to_occupations as $wpo)
                {
                    $occupations .= '<li>'.$wpo->occupation->name.'</li>';
                }
                return $occupations."</ul>";
            default: return parent::columnDisplays($column);
        }
    }

    public function modelForms($conf = array())
    {        
        $curr_company_id = Yii::app()->company->id;
        if (       isset($conf['filters']) 
                && isset($conf['filters']['company_sector_id']['company_id'])
                && $conf['filters']['company_sector_id']['company_id']==$curr_company_id
                && (Yii::app()->user->checkAccess('CompanySchemaWrite')))
        {
            return array(
                'default' => array(
                    'type' => 'generic',
                    'columns' => array(
                        'code' => 'textField',
                        'name' => 'textField',
                        'company_sector_id' => array('relation', 'relName' => 'company_sector', 'filters'=>[
                            'company_id' => $curr_company_id
                        ]),
                        'payment_amount_id' => array('relation', 'relName' => 'payment_amount', 'add_button'=>true),
                        'is_director'=>'checkBox',
                        'enlarged_risk' => 'dropdown',                        
                        'reason_of_training' => 'textField',
                        'medical_report_interval' => 'textField',
                        'description' => 'textArea',
                        'paygrade_id' => array('relation', 'relName' => 'paygrade'),
                        'benefited_work_experience' => 'dropdown',                      
                        'add_occupations' => ['list',
                            'model' => Occupation::class,
                            'relName' => 'occupations',
                            'multiselect' => true,
                        ]
                    )
                )
            );
        }
        else
        {
            return array(
                'default' => [
                    'type' => 'composite',
                    'forms' => [
                        'work_position' => [
                            'title' => 'Radna pozicija'
                        ],
                        'occupations' => [
                            'title' => Yii::t('HRModule.WorkPosition','Occupations'),
                        ],
                        'safe_evd' => [
                            'title' => 'Bezbednost i zdravlje na radu',
                            'visible' => Yii::app()->hasModule('HR')
                        ]
                    ],
                    'showFormWrapper' => false
                ],
                'work_position' => array(
                    'type' => 'generic',
                    'columns' => array(
                        'code' => 'textField',
                        'name' => 'textField',
                        'company_sector_id' => array('relation', 'relName' => 'company_sector', 'filters'=>[
                            'company_id' => $curr_company_id
                        ]),
                        'payment_amount_id' => array('relation', 'relName' => 'payment_amount', 'add_button'=>true),
                        'is_director'=>'checkBox',
                        'description' => 'textArea',
                        'paygrade_id' => array('relation', 'relName' => 'paygrade'),
                        'description_of_activities' => 'tinymce',
                        'safe_actions' => 'tinymce',
                        'notice_safe' => 'tinymce',
                        'benefited_work_experience' => ['dropdown']
                    )
                ),
                'occupations' => [
                    'type' => 'generic',
                    'columns' => [                        
                        'add_occupations' => ['list',
                            'model' => Occupation::class,
                            'relName' => 'occupations',
                            'multiselect' => true,
                        ] 
                    ]
                ],
                'safe_evd' => [
                    'type' => 'generic',
                    'columns' => [
                        'increased_risk' => 'checkBox',
                        'add_risks'=>array('list',
                            'model'=>'SafeWorkDanger',
                            'relName'=>'risks',
                            'multiselect'=>true
                        ),
                        'add_required_medical_abilities'=>array('list',
                            'model'=>'RequiredMedicalAbility',
                            'relName'=>'required_medical_abilities',
                            'multiselect'=>true
                        ),
                    ]
                ]
            );
        }
    }

    public function droplist($key, $relName='', $filter=null)
    {
        switch ($key)
        {
            case 'enlarged_risk': return array('true' => 'Da', 'false' => 'Ne');
            case 'benefited_work_experience': return WorkContract::$benefited_work_experiences;
            default: return parent::droplist($key, $relName, $filter);
        }
    }
    
    public function modelSearchViews() 
    {
        return array('work_contract_positions')+parent::modelSearchViews();
    } 
    
    public function guiTableSettings($type)
    {        
        switch ($type)
        {
            default: return [
                'columns' => [
                    'name', 'description', 'occupations'
                ],
                'order_by' => [
                    'name'
                ]
            ];
        }
    }
}

?>
