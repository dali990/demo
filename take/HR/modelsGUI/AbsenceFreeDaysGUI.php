<?php

class AbsenceFreeDaysGUI extends AbsenceGUI
{
    public function modelLabel($plural = false)
    {
        return Yii::t('HRModule.Absence', $plural?'PaidLeaves':'PaidLeave');
    }
    
    public function columnDisplays($column)
    {
        $owner = $this->owner;
        switch ($column)
        {
            case 'subtype': 
                $result = Yii::t('BaseModule.Common', 'Unknown');
                $subtypes_data = AbsenceFreeDays::getSubTypeData();
                if(isset($subtypes_data[$owner->subtype]))
                {
                    $result = $subtypes_data[$owner->subtype];
                }
                return $result;
            default: return parent::columnDisplays($column); 
        }
    }
    
    public function modelViews()
    {
        return array_merge(parent::modelViews(),[
            'full_info'
        ]);
    }
    
    public function modelForms()
    {       
        $employee_id = ['relation', 'relName'=>'employee'];
        if(isset($this->owner->employee_id))
        {
            $employee_id['disabled'] = 'disabled';
        }
        $columns = array(            
            'type'=>'hidden',
            'subtype' => ['dropdown', 'empty' => Yii::t('BaseModule.Common', 'Choose')],
            'employee_id'=>$employee_id,
            'begin'=>'datetimeField',
            'end'=>'datetimeField',                    
            'number_work_days'=>'textField',
            'description'=>'textArea'
        );
        if(!$this->owner->isNewRecord && $this->owner->canConfirmAbsence() === true) //jer moze da vrati niz
        {
            $columns['confirmed'] = 'status';            
        }
        return array(
            'default'=>array(
                'type'=>'generic',
                'columns'=>$columns
            )
        );
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            case 'from_user': return [
                'columns' => array('subtype', 'begin', 'end', 'number_work_days', 'year', 'set_document', 'description'),
                'order_by' => ['begin', 'end', 'number_work_days'],
                'sums'=>['number_work_days']
            ];
            default: return [
                'columns' => array('subtype', 'employee', 'begin', 'end', 'number_work_days', 'year', 'set_document', 'description'),
                'order_by' => ['begin', 'end', 'number_work_days'],
                'sums'=>['number_work_days']
            ];
        }
    }
    
    public function getDisplayModel()
    {
        return $this->owner->absence;
    }
}
