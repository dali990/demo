<?php

class WorkContractGUI extends SIMAActiveRecordGUI
{

    public function columnLabels()
    {
        return array(
            'name' => Yii::t('HRModule.WorkContract', 'Name'),
            'start_date' => Yii::t('HRModule.WorkContract', 'StartDate'),
            'end_date' => Yii::t('HRModule.WorkContract', 'EndDate'),
            'calculated_end_date' => Yii::t('HRModule.WorkContract', 'CalculatedEndDate'),
            'description' => Yii::t('HRModule.WorkContract', 'Description'),
            'employee_id' => Yii::t('HRModule.WorkContract', 'Employee'),
            'employee' => Yii::t('HRModule.WorkContract', 'Employee'),
            'work_position_id' => Yii::t('HRModule.WorkContract', 'WorkPosition'),
            'work_position' => Yii::t('HRModule.WorkContract', 'WorkPosition'),
            'DisplayName' => Yii::t('HRModule.WorkContract', 'DisplayName'),
            'FilingDisplayHTML' => Yii::t('HRModule.WorkContract', 'DisplayName'),
            'working_percent' => Yii::t('HRModule.WorkContract', 'WorkingPercent'),
            'working_percent_at_other_companies' => Yii::t('HRModule.WorkContract', 'WorkingPercentAtOtherCompanies'),
            'work_positions' => Yii::t('HRModule.WorkContract', 'WorkPositions'),
            'work_position_id' => Yii::t('HRModule.WorkContract', 'WorkPosition'),
            'generate_work_contract_template' => Yii::t('HRModule.WorkContract', 'WorkContractTemplate'),
            'generate_notice_of_mobbing_template' => Yii::t('HRModule.WorkContract', 'MobbingNotice'),
            'generate_introduction_into_business_program' => Yii::t('HRModule.WorkContract', 'IntroductionIntoBusinessProgram'),
            'add_filing_number'=> Yii::t('HRModule.WorkContract', 'AddFilingNumber'),
            'for_payout' => Yii::t('HRModule.WorkContract', 'ForPayout'),
            'for_work_exp' => Yii::t('HRModule.WorkContract', 'ForWorkExp'),
            'is_probationer' => Yii::t('HRModule.WorkContract', 'IsProbationer'),
            'signed' => Yii::t('HRModule.WorkContract', 'Signed'),
            'work_time' => Yii::t('HRModule.WorkContract', 'WorkTime'),
            'svp' => Yii::t('HRModule.WorkContract', 'Svp'),
            'employment_code' => Yii::t('HRModule.WorkContract', 'EmploymentCode'),
            'benefit' => Yii::t('HRModule.WorkContract', 'Benefit'),
            'type_of_payer' => Yii::t('HRModule.WorkContract', 'TypeOfPayer'),
            'benefited_work_experience' => Yii::t('HRModule.WorkContract', 'BenefitedWorkExperience'),
            'occupation_id' => Yii::t('HRModule.WorkContract','Occupation'),
        ) + parent::columnLabels();
    }

    public function modelLabel($plural = false)
    {
        return 'Ugovor o radu';
    }

    public function columnDisplays($column)
    {
        $owner = $this->owner;
        $close_work_contract_document_type_id = Yii::app()->configManager->get('HR.close_work_contract', false);
        switch ($column)
        {
            case 'end_date':
                $result = '';
                
                $wc_end_date = SIMAHtml::DbToUserDate($owner->getEndDate());
                $show_additional = false;
                
                if (isset($owner->close_work_contract))
                {
                    $result = $owner->close_work_contract->end_date
                            . '(' . $owner->close_work_contract->DisplayName
                            . SIMAHtml::modelDialog(CloseWorkContract::model()->findByPk($owner->close_work_contract->id)) . ')';
                    
                    if($owner->close_work_contract->end_date !== $wc_end_date)
                    {
                        $show_additional = true;
                    }
                }
                else
                {
                    $result = ((($owner->end_date == null || $owner->end_date == '')) ? 'neodređeno' : $owner->end_date)
                            . ((isset($owner->employee->active_work_contract) && $owner->employee->active_work_contract->id == $owner->id) ? SIMAHtml::modelFormOpen(CloseWorkContract::model(), array(
                                        'init_data' => array(
                                            'work_contract_id' => $owner->id,
                                            'document_type_id' => $close_work_contract_document_type_id
                                        ),
                                        'updateModelViews' => SIMAHtml::getTag($owner)
                                    )) : '');
                    
                    if($owner->end_date !== $wc_end_date)
                    {
                        $show_additional = true;
                    }
                }
                
                if($show_additional === true)
                {
                    $result = SIMAHtml::spanWithHover($result.'*', $wc_end_date);
                }
                                
                return $result;
            case 'working_percent': 
            case 'working_percent_at_other_companies': 
                return $owner->$column . '%';
            case 'work_positions':                    
                    return $this->work_position->DisplayName . SIMAHtml::modelDialog($this->work_position);                    
            case 'generate_work_contract_template':
                if (isset($owner->file->filing))
                {
                    return $this->renderGenerateWorkContractButton();
                }
                else 
                {
                    return 'prvo zavedite da biste generisali dokument';
                }
            case 'generate_notice_of_mobbing_template':
                if (isset($owner->file->filing))
                {
                    return "<button class='sima-ui-button' onclick='sima.workContract.generateWorkContractTemplates(" . $owner->id . ",3);'>Generiši</button>"
                        . SIMAHtml::fileDownload($owner->notice_of_mobbing);
                }
                else 
                {
                    return 'prvo zavedite da biste generisali obaveštenje na mobing';
                }
            case 'add_filing_number':
                $result = '';
                
                $file = File::model()->resetScope()->findByPk($owner->id);
                $isset_filing = isset($file->filing);
                if($isset_filing === true)
                {
                    $filing = $file->filing;
                    $filing_number = $filing->filing_number;
                    $filing_date = $filing->date;
                    
                    $result = $filing_number . ' od ' . $filing_date;
                }
                else
                {
                    $result = $owner->renderFilingNumber();
                }
                
                $result .= ' - ' . $file->DisplayName;
                
                return $result;
//                return ((isset($owner->file->filing)) ? $owner->file->filing->filing_number . ' od ' . $owner->file->filing->date : $owner->renderFilingNumber()) . ' - ' . $owner->file->DisplayName;
            case 'work_time':                
                $worked_days = $owner->getWorkExperience();
                
                $result = WorkContractComponent::formatWorkExp($worked_days);

                if($owner->for_work_exp === false)
                {
                    $result .= '('.Yii::t('HRModule.WorkContract', 'NotForWorkExp').')';
                }
                
                return $result;              
            case 'generate_introduction_into_business_program':
                $owner = $this->owner;
                $result = '';
                if(isset($owner->introduction_into_business_program))
                {
                    if(!empty($owner->introduction_into_business_program->file))
                    {
                        $result = SIMAHtml::modelDialog($owner->introduction_into_business_program->file)
                                .SIMAHtml::fileDownload($owner->introduction_into_business_program);
                        
                    }
                }
                else
                {
                    $result = "<button class='sima-ui-button' onclick='sima.legal.generateIntroductionIntoBusinessProgram(" . $owner->id . ",1);'>"
                            . Yii::t('BaseModule.Common', 'Generate')
                            . "</button>";
                }
                return $result;
            case 'signed':
                if($owner->signed === true)
                {
                    return Yii::t('BaseModule.Common', 'Yes');
                }
                else
                {
                    return Yii::t('BaseModule.Common', 'No');
                }
            case 'FilingDisplayHTML':
                $result = $owner->DisplayName;
                $file = File::model()->resetScope()->findByPk($owner->id);
                if (!isset($file->filing))
                {
                    $result = $owner->renderFilingNumber() . ' - ' . $result;
                }
                return $result;
            default: return parent::columnDisplays($column);
        }
    }
    
    public function renderGenerateWorkContractButton()
    {
        $owner = $this->owner;
        return "<button class='sima-ui-button' onclick='sima.workContract.generateWorkContractTemplates(" . $owner->id . ",1);'>".Yii::t('BaseModule.Common', 'Generate')."</button>"
                        . SIMAHtml::fileDownload($owner);
    }
    
    public function modelViews()
    {
        return array(
            'inFile' => array('class' => 'basic_info'),
            'work_positions',
            'full_info'
        );
    }

    public function droplist($key, $relName='', $filter=null)
    {
        switch ($key)
        {
            case 'belongs': 
                return FileTag::model()->getModelDropList();  
            case 'for_payout':
            case 'for_work_exp':
            case 'is_probationer':
            case 'signed':
                return array(
                    true => Yii::t('BaseModule.Common', 'Yes'),
                    false => Yii::t('BaseModule.Common', 'No'),
                );
            default: return parent::droplist($key, $relName, $filter);
        }
    }

    public function modelForms()
    {
        $owner = $this->owner;

        if ($owner->signed)
        {
            $signed = 'display';
        }
        else
        {
            $signed = 'dropdown';
        }

        $svp = ['svpField'];
        if ($owner->isNewRecord || !$owner->signed)
        {
            $svp['dependent_on'] = [
                'work_position_id' => [
                    'onValue' => ['trigger', 'func' => ['sima.workContract.workPositionOnBenefitedWorkExperienceHendler']],
                    'onEmpty' => ['trigger', 'func' => ['sima.workContract.workPositionOnBenefitedWorkExperienceHendler']]
                ]
            ];
        }
        
        $company_sector_id = null;
        if(!empty($owner->work_position->company_sector))
        {
            $company_sector_id = $owner->work_position->company_sector->id;
        }
        
        $default_columns = [
            'signed' => $signed,
            'employee_id' => ['relation', 'relName'=>'employee'],
            'work_position_id' => ['relation', 'relName'=>'work_position', 
                'add_button' => [
                    'params' => [
                        'add_button' => [
                            'init_data' => [
                                'WorkPosition' => [
                                    'company_sector_id' => $company_sector_id
                                ]  
                            ] 
                        ]                      
                    ]
                ],
                'filters' => [
                    'company_id' => Yii::app()->company->id
                ]
            ],
            'occupation_id'=>array('relation', 'relName'=>'occupation', 'dependent_on'=>[
                'work_position_id'=>[
                    'onValue'=>['enable',
                        ['trigger', 'func' => ['sima.workContract.filterOccupations']]
                    ],
                    'onEmpty'=>['disable']
                ]
            ]),
            'svp'=>$svp,
            'start_date' => 'datetimeField',
            'end_date' => 'datetimeField',
            'working_percent' => 'numberField',
            'working_percent_at_other_companies' => ['numberField', 'dependent_on' => [
                'working_percent' => [
                    'onValue'=>[
                        ['hide', 'for_values' => 100]
                    ]
                ]
            ]],
            'for_payout' => 'dropdown',
            'for_work_exp' => 'dropdown',
            'is_probationer' => 'dropdown',
//                    'work_position_id' => ['relation', 'relName'=>'work_position'],
        ];
        
        if($owner->signed === true && !Yii::app()->user->checkAccess('modelWorkContractEditSigned'))
        {
            foreach($default_columns as $key => $value)
            {
                if ($key === 'occupation_id')
                {
                    continue;
                }
                
                if(!is_array($value))
                {
                    $value = [$value];
                }
                $value['disabled'] = 'disabled';

                $default_columns[$key] = $value;
            }
        }

        return array(
            'default' => array(
                'type' => 'generic',
                'columns' => $default_columns
            )
        );
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            case 'with_employee': 
                return [
                    'columns' => [
                        'employee',
                        'work_position', 
                        'start_date', 
                        'end_date', 
                        'calculated_end_date',
                        'FilingDisplayHTML',
                        'generate_work_contract_template',
                        'working_percent',
                        'working_percent_at_other_companies',
                        'work_time',
                        'generate_introduction_into_business_program',
                        'signed'
                    ],
                    'order_by' => [
                        'start_date', 
                        'end_date'
                    ],
                    'sums' => [
                        'work_time'
                    ],
                    'sums_function' => 'defaultSumFunc'
                ];
            default:
                return array(
                    'columns' => array(
                        'FilingDisplayHTML',
                        'generate_work_contract_template',
                        'start_date', 
                        'end_date', 
                        'calculated_end_date',
                        'work_position', 
                        'working_percent',
                        'working_percent_at_other_companies',
                        'work_time',
                        'generate_introduction_into_business_program',
                        'signed'
                    ),
                    'order_by' => [
                        'start_date', 
                        'end_date', 
                    ],
                    'sums' => array(
                        'work_time'
                    ),
                    'sums_function'=>'defaultSumFunc'
            );
        }
    }
    
    public function renderFilingNumber()
    {        
        $owner = $this->owner;
        $file = File::model()->findByPk($owner->id);
        $registry_id = Yii::app()->configManager->get('legal.employee_history_register', false);
        $personal_delivery_type = Yii::app()->configManager->get('legal.personal_delivery_type', false);
        if(SIMAMisc::isVueComponentEnabled())
        {
            $filing_to_delivery_types = ($personal_delivery_type!==null) ? [
                'ids' => [
                    'ids' => [
                        [
                            'id' => $personal_delivery_type,
                            'display_name' => 'Lično',
                            'class' => '_visible'
                        ]
                    ],
                    'default_item' => []
                ]
            ] : null;
        }
        else
        {
            $filing_to_delivery_types = ($personal_delivery_type!==null) ? [
                'ids' => '{"ids":[{"id":'.$personal_delivery_type.',"display_name":"Lično","class":"_visible"}],"default_item":[]}'
            ] : null;
        }
        if ($file !== null)
        {
            $file_init_data = [
                'responsible_id' => ($owner->file->responsible_id !== null) ? $owner->file->responsible_id : Yii::app()->user->id
            ];
            $filing_init_data = [
                'confirmed' => true,
                'partner_id' => $owner->employee->id,
                'in' => false,
                'registry_id' => $registry_id,
                'filing_to_delivery_types' => $filing_to_delivery_types
            ];
            $file_init_data_json = CJSON::encode($file_init_data);
            $filing_init_data_json = CJSON::encode($filing_init_data);
            return "<span class='link' onclick='sima.workContract.addWorkContractFilingNumber($owner->id,$file_init_data_json,$filing_init_data_json)'>zavedi</span>";
        }
        else
        {
            return '';
        }
    }
    
    public function defaultSumFunc($criteria)
    {
        $owner = $this->owner;
        
        $work_contracts = $owner->resetScope()->findAll($criteria);
        
        $worked_days = 0;
        foreach($work_contracts as $wc)
        {
            if($wc->for_work_exp === true)
            {
                $worked_days += $wc->getWorkExperience();
            }
        }
        
        $result = WorkContractComponent::formatWorkExp($worked_days);
        
        $resultObj = new WorkContract();
        $resultObj->work_time = $result;
        
        return $resultObj;
    }
}
