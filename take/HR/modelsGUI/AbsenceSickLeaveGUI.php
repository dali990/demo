<?php

class AbsenceSickLeaveGUI extends AbsenceGUI
{
    public function modelLabel($plural = false)
    {
        return Yii::t('HRModule.Absence', $plural?'SickLeaves':'SickLeave');
    }
    
    public function modelForms()
    {
        $employee_id = ['relation', 'relName'=>'employee'];
        if(isset($this->owner->employee_id))
        {
            $employee_id['disabled'] = 'disabled';
        }
        $columns = [
            'type'=>'hidden',
            'subtype' => 'dropdown',
            'employee_id'=>$employee_id,
            'begin'=>'datetimeField',
            'end'=>'datetimeField',                    
            'number_work_days'=>'textField',
            'description'=>'textArea',
            'neto'=>['textField', 'dependent_on'=>[
                'subtype'=>[
                    'onValue'=>[
                        ['show', 'for_values'=>[
                            AbsenceSickLeave::$SUBTYPE_SICK_LEAVE_CHILD_CARE_TILL_3,
                            AbsenceSickLeave::$SUBTYPE_SICK_LEAVE_MATERNITY
                        ]]
                    ]
                ]
            ]]
        ];
        if(!$this->owner->isNewRecord && $this->owner->canConfirmAbsence() === true) //jer moze da vrati niz
        {
            $columns['confirmed'] = 'status';            
        }
        
        return array(
            'default'=>array(
                'type'=>'generic',
                'columns'=>$columns
            ),
        );
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            case 'from_user': return [
                'columns' => [
                    'subtype', 'begin', 'end', 'number_work_days', 'neto', 'year', 'set_document', 
                    'description'
                ],
                'order_by' => ['begin', 'end', 'number_work_days'],
                'sums'=>['number_work_days']
            ];
            default: return [
                'columns' => [
                    'employee', 'begin', 'end', 'number_work_days', 'year', 'set_document', 'description'
                ],
                'order_by' => ['begin', 'end', 'number_work_days'],
                'sums'=>['number_work_days']
            ];
        }
    }
    
    public function addSickLeaveDocument()
    {
        $owner = $this->owner;
        if (Yii::app()->user->checkAccess('modelAbsenceGenerateDocument'))
        {
            if (isset($owner->document))
            {
                return "<span class='sima-icon _edit _16' onclick='sima.hr.addSickLeaveDocument(".$owner->id.");'></span>"
                    .SIMAHtml::fileDownload($owner->document).$owner->document->DisplayHTML;
            }
            else
            {
                return "<span class='sima-icon _add _16' onclick='sima.hr.addSickLeaveDocument(".$owner->id.");'></span>";
            }
        }
        else
        {
            return 'Nemate privilegiju';
        }
    }
    
    public function getDisplayModel()
    {
        return $this->owner->absence;
    }
}
