<?php

/**
 * Description of SafeEvd1GUI
 *
 * @author goran
 */
class SafeEvd1GUI extends SIMAActiveRecordGUI
{

    public function columnLabels()
    {
        return array(
            'id' => 'Redni broj',
            'work_position_id' => 'Radno mesto',
            'work_position' => 'Radno mesto',
            'count_employees' => 'Broj zaposlenih',
            'work_danger_id' => 'Šifra opasnosti/štetnosti',
            'work_danger' => 'Šifra opasnosti/štetnosti',
            'comment' => 'Napomena',
        ) + parent::columnLabels();
    }

    public function modelLabel($plural = false)
    {
        return SafeRecordController::$SAFE_RECORDS[1]['name'];
    }

    public function modelViews()
    {
        return array() + parent::modelViews();
    }

    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default: return array(
                'columns' => array(
                    'work_position', 'count_employees', 'work_danger', 'comment'
                )
            );
        }
    }

    public function modelForms()
    {
        return array(
            'default' => array(
                'type' => 'generic',
                'columns' => array(
                    'work_danger_id' => array('searchField', 'relName' => 'work_danger'),
                    'work_position_id' => array('searchField', 'relName' => 'work_position', 'filters' => ['scopes' => 'withoutIncreasedRisk']),
                    'comment' => 'textField',
                )
            ),
        );
    }

}
