<?php

/**
 * Description of CloseWokrContractGUI
 *
 * @author goran
 */
class CloseWorkContractGUI extends SIMAActiveRecordGUI
{
  public function columnLabels()
    {
        return array(
            'end_date' => 'Raskinut',
            'description' => 'Opis',
            'work_contract_id'=>'Ugovor o radu',
            'document_type_id'=>'Tip dokumenta',
            'generate_close_work_contract_template'=>'Raskid ugovora o radu'
        )+parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return 'Raskid ugovora o radu';
    }
    
    public function modelViews()
    {
        return array(
           'inFile'=>array('class'=>'basic_info'),
//            'tabTemplates'=>array('with'=>array('templated_file'),'style'=>'border: 1px solid;')  //.creator
        );
    }
    
    public function columnDisplays($column)
    {
        $owner = $this->owner;
        switch ($column)
        {
            case 'generate_close_work_contract_template':
                    return "<button class='sima-ui-button' onclick='sima.workContract.generateWorkContractTemplates(".$owner->work_contract->id.",4);'>Generiši</button>"
                    .SIMAHtml::fileDownload($owner);
                       
            default:         return parent::columnDisplays($column);
        }
    }

    public function modelForms() 
    {
        return array(
            'default'=>array(
                'type'=>'generic',
                'columns'=>array(
                    'id'=>'hidden',
                    'document_type_id'=>'hidden',
                    'end_date'=>'datetimeField',
                    'work_contract_id'=>array('searchField','relName'=>'work_contract'),
                    'description'=>'textArea',
                )
            )
        );
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default:
                return [
                    'columns' => ['DisplayName', 'end_date', 'work_contract_id']
                ];
        }
    }
    
}
