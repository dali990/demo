<?php

/**
 * Description of WorkingLicenceCertificateGUI
 *
 * @author goran
 */
class WorkingLicenceCertificateGUI extends SIMAActiveRecordGUI
{
   public function columnLabels()
   {
        return array(
            'DisplayName'=>'Fajl',
            'begin_date'=>'Datum izdavanja',
            'end_date'=>'Kraj važenja',
            'licence_id'=>'Licenca',
            'description'=>'Napomena',
            'file_version_id'=>'Fajl'
        )+parent::columnLabels();
    }
    
    public function modelViews()
    {
        return array(
           'inFile'
        )+parent::modelViews();
    }
    
    public function modelForms()
    {
        return array(
            'default'=>array(
                'type'=>'generic',
                'columns'=>array(
                    'id'=>'hidden', //mora postojati u svakoj formi,
                    'begin_date'=>'datetimeField',
                    'end_date'=>'datetimeField',
                    'licence_id'=>'display',//array('searchField','relName'=>'license'),
                    'file_version_id'=>'fileField',
                    'description'=>'textArea'
                )
            ),
        );
    } 
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default: return [
                'columns' => [
                    'DisplayName', 'begin_date', 'end_date', 'description'
                ]
            ];
        }
    }
    
}
