<?php
/**
 * Description of CompanySectorGUI
 *
 * @author goran
 */
class IKSPointGUI extends SIMAActiveRecordGUI
{
    public function columnLabels() {
        return array(
            'national_regulations_and_standards'=>'Oblast nacionalnih propisa i standarda',
            'new_materials_technologies_in_nacional_ind'=>'Oblast novih materijala i tehnologija u nacionalnoj privredi i indrustriji',
            'international_regulations_and_standards'=>'Oblast internacionalnih propisa i standarda',
            'new_materials_technologies_in_internacional_ind'=>'Oblast novih materijala i tehnologija u internacionalnoj privredi i indrustriji',
            'group'=>'Grupa',
            'number_of_points'=>'Broj IKS poena',
            'activity_name'=>'Aktivnost'
        ) + parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return 'IKS poeni';
    }
    
    public function modelForms() {
        return array(
            'default' => array(
                'type' => 'generic',
                'columns' => array(
                    'group' => 'dropdown',
                    'number_of_points' => 'textField',
                    'model'=>'hidden',
                    'model_id'=>'hidden',
                    'date'=>'hidden'
                )
            )
        );
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default: return array(
                'columns'=>array('activity_name','national_regulations_and_standards','new_materials_technologies_in_nacional_ind',
                    'international_regulations_and_standards','new_materials_technologies_in_internacional_ind'),
                'sums' => array(
                    'national_regulations_and_standards', 'new_materials_technologies_in_nacional_ind',
                    'international_regulations_and_standards', 'new_materials_technologies_in_internacional_ind'
                ),
            );
        }
    }
    
    public function droplist($key, $relName='', $filter=null) 
    {
        switch ($key) {            
            case 'group': return array(
                    'national_regulations_and_standards'=>'Oblast nacionalnih propisa i standarda',
                    'new_materials_technologies_in_nacional_ind'=>'Oblast novih materijala i tehnologija u nacionalnoj privredi i indrustriji',
                    'international_regulations_and_standards'=>'Oblast internacionalnih propisa i standarda',
                    'new_materials_technologies_in_internacional_ind'=>'Oblast novih materijala i tehnologija u internacionalnoj privredi i indrustriji'
                );
            default: return parent::droplist($key, $relName, $filter);
        }
    }
    
}

?>
