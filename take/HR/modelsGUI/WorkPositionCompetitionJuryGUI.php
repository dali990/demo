<?php

class WorkPositionCompetitionJuryGUI extends SIMAActiveRecordGUI
{

    public function columnLabels()
    {
        return array(
            'name' => 'Pozicija',
            'text' => 'Tekst',
            'active' => 'Aktivan',
            'apply_date' => 'Rok za prijavu',
            'jury' => 'Ziri',
            'jury_id' => 'Ziri',
            'work_position_competition' => 'Konkurs',
            'work_position_competition_id' => 'Konkurs',
            'not_graded_count' => 'Broj neocenjenih',
        )+parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return 'Konkurs';
    }

//    public function modelViews()
//    {
//        return array(
//            'basicInfoTitle',
//            'basicInfo',
//            'current_materials_status'=>array('params_function'=>'viewParamsCurrentMaterialsStatus')
////            'leaf'=>array(
//////                'style'=>'border: 1px solid;'
////                ),
////            'default_edit'
////            'tabTemplates'=>array('with'=>array('templated_file'),'style'=>'border: 1px solid;')  //.creator
//        );
//    }
    
    public function columnDisplays($column)
    {
        $owner = $this->owner;
        switch ($column)
        {
                
            default: return parent::columnDisplays($column);
        }
    }
    
    
    public function modelForms()
    {
        return array(
            'default'=>array(
                'type'=>'generic',
                'columns'=>array(
                    'work_position_competition_id' => 'hidden',
                    'jury_id' => ['relation', 'relName'=>'jury'],
                )
            )
        );
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {

            default: return array(
                'columns' => array(
                    'jury','not_graded_count'
                )
            );
        }
    }
    
}