<?php

/**
 * Description of SafeEvd6GUI
 *
 * @author goran
 */
class SafeEvd6GUI extends SIMAActiveRecordGUI
{

    public function columnLabels()
    {
        return array(
            'work_position_id' => 'Radno mesto',
            'work_position' => 'Radno mesto',
            'employee_id' => 'Zaposleni',
            'employee' => 'Zaposleni',
            'description' => 'Opis',
            'reasons_for_enforcement_training_list' => 'Razlozi za osposobljavanje',
            'date_theoretical_training' => 'Datum teoretskog osposobljavanja',
            'date_practical_training' => 'Datum praktičnog osposobljavanja',
            'date_check_theoretical_training' => 'Datum provere teoretskog osposobljavanja',
            'date_check_practical_training' => 'Datum provere praktičnog osposobljavanja',
            'generate_report' => 'Generiši izveštaj',
            'theoretical_training_period_id'=>'Period teoretske provere',
            'theoretical_training_period'=>'Period teoretske provere'            
        ) + parent::columnLabels();
    }

    public function modelLabel($plural = false)
    {
        return SafeRecordController::$SAFE_RECORDS[6]['name'];
    }

    public function modelViews()
    {
        return array() + parent::modelViews();
    }

    public function columnDisplays($column)
    {
        $owner = $this->owner;
        switch ($column)
        {
            case 'generate_report':
                $action = [                
                    'title'=>'Generiši izveštaj',
                    'tooltip'=>'Generiši izveštaj',
                    'onclick'=>["sima.hr.generateSafeEvd6Report", "$owner->id"]
                ];
                $button = '';
                if(SIMAMisc::isVueComponentEnabled())
                {
                    $button = Yii::app()->controller->widget(SIMAButtonVue::class, $action, true);
                }
                else
                {
                    $button = Yii::app()->controller->widget(SIMAButton::class,[
                        'action' => $action
                    ], true);  
                }
                if (isset($owner->file))
                {
                    $button = SIMAHtml::fileDownload($owner->file).$owner->file->DisplayHtml.$button;
                }
                return $button;
            case 'reasons_for_enforcement_training_list':
                $return = '';
                foreach ($owner->reasons_for_enforcement_training as $reason) {
                    $return .= $reason->DisplayName. ' | ';
                }
                return substr($return, 0, -3);
            default: return parent::columnDisplays($column);
        }
    }

    public function modelForms()
    {
        return array(
            'default' => array(
                'type' => 'generic',
                'columns' => array(
                    'employee_id' => array('searchField', 'relName' => 'employee'),
                    'work_position_id' => array('searchField', 'relName' => 'work_position'),
                    'date_theoretical_training' => 'datetimeField',
                    'date_check_theoretical_training' => 'datetimeField',
                    'date_practical_training' => 'datetimeField',
                    'date_check_practical_training' => 'datetimeField',
                    'reasons_for_enforcement_training_list'=>array('list',
                        'model'=>'ReasonForEnforcementTraining',
                        'relName'=>'reasons_for_enforcement_training',                       
                        'multiselect'=>true
                    ),
                    'description' => 'textArea',
                )
            ),
        );
    }

    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default:
                return array(
                    'columns' => array(
                        'employee', 'work_position', 'generate_report', 'reasons_for_enforcement_training_list', 'date_theoretical_training', 'date_check_theoretical_training',
                        'date_practical_training', 'date_check_practical_training', 'description'
                    ),
                );
        }
    }

}
