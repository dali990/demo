<?php

/**
 * Description of SafeEvd5GUI
 *
 * @author goran
 */
class SafeEvd5GUI extends SIMAActiveRecordGUI
{

    public function columnLabels()
    {
        return array(
            'id' => 'Redni broj',
            'work_position_id' => 'Radno mesto',
            'work_position' => 'Radno mesto',
            'person_id' => 'Zaposleni',
            'person' => 'Zaposleni',
            'person_name' => 'Zaposleni',
            'company_id' => 'Naziv zdravstvene ustanove',
            'company' => 'Naziv zdravstvene ustanove',
            'company_name' => 'Naziv zdravstvene ustanove',
            'international_diagnosis_id' => 'Dijagnoza',
            'international_diagnosis' => 'Dijagnoza',
            'degree' => Yii::t('HRModule.SafeEvd5','DiagnosisAndInternationalCodeOfDisease'),
            'person_ability' => 'Preostala sposobnost',
            'description' => 'Opis'
        ) + parent::columnLabels();
    }

    public function modelLabel($plural = false)
    {
        return SafeRecordController::$SAFE_RECORDS[5]['name'];
    }

    public function modelViews()
    {
        return array() + parent::modelViews();
    }

    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default: return array(
                    'columns' => array(
                        'person', 'work_position', 'international_diagnosis', 'company', 'degree', 'person_ability', 'description'
                    )
                );
        }
    }

    public function modelForms()
    {
        return array(
            'default' => array(
                'type' => 'generic',
                'columns' => array(
                    'person_id' => array('searchField', 'relName' => 'person'),
                    'work_position_id' => array('searchField', 'relName' => 'work_position'),
                    'international_diagnosis_id' => array('searchField', 'relName' => 'international_diagnosis', 'add_button'=>true),
                    'company_id' => array('searchField', 'relName' => 'company'),
                    'degree' => 'textField',
                    'person_ability' => 'textField',
                    'description' => 'textArea'
                )
            ),
        );
    }

}
