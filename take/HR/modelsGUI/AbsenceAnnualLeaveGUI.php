<?php

class AbsenceAnnualLeaveGUI extends AbsenceGUI
{
    public function columnLabels() 
    {
        return array_merge(parent::columnLabels(), [
            'employee.last_work_contract.duration_text'=>'Trajanje poslednjeg ugovora'
        ]);
    }
    
    public function modelLabel($plural = false)
    {
        return Yii::t('HRModule.Absence', $plural?'AnnualLeaves':'AnnualLeave');
    }
    
    public function columnDisplays($column)
    {
        $owner = $this->owner;
        switch ($column)
        {            
            case 'approval_documents':
                $return = '';
                foreach ($owner->annual_leave_to_annual_leave_decisions as $annual_leave_to_annual_leave_decision)
                {
                    if ($annual_leave_to_annual_leave_decision->days_number > 0)
                    {
                        $days_label = 'dana';
                        if ($annual_leave_to_annual_leave_decision->days_number === 1)
                        {
                            $days_label = 'dan';
                        }
                        $return .= $annual_leave_to_annual_leave_decision->days_number
                                ."-$days_label - "
                                .$annual_leave_to_annual_leave_decision->annual_leave_decision->DisplayHTML.'<br />';
                    }
                }
                return $return;
                   
            default : return parent::columnDisplays($column);
        }
    }
    
    public function modelForms()
    {
        $employee_id = ['relation', 'relName'=>'employee'];
        if(isset($this->owner->employee_id))
        {
            $employee_id['disabled'] = 'disabled';
        }
        $disable_edit = false;
        if (!$this->owner->isNotResolved)
        {
            $disable_edit = true;
        }
        $columns = [
            'type'=>'hidden',
            'employee_id'=>$employee_id,                    
            'begin'=>['datetimeField', 'disabled'=>$disable_edit],
            'end'=>['datetimeField','disabled'=>$disable_edit],
            'number_work_days'=>['textField','disabled'=>$disable_edit],
            'description'=>'textArea'
        ];
        if(!$this->owner->isNewRecord && $this->owner->canConfirmAbsence() === true) //jer moze da vrati niz
        {
            $columns['confirmed'] = 'status';            
        }
        return array(
            'default'=>array(
                'type'=>'generic',
                'columns'=>$columns
            ),
        );
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            case 'from_user': return [
                'columns' => array('begin', 'end', 'number_work_days', 'approval_documents', 'set_document', 'description'),
                'order_by' => ['begin', 'end', 'number_work_days'],
                'sums'=>['number_work_days']
            ];
            case 'from_collective': return [
                'columns' => ['employee', 'begin', 'end', 'number_work_days', 
                    'employee.last_work_contract.duration_text',
                    'employee.last_work_contract.work_position',
                    'employee.WorkExperience'
                ]
            ];
            default: return [
                'columns' => array('employee', 'begin', 'end', 'number_work_days', 'approval_documents', 'set_document', 'description'),
                'order_by' => ['begin', 'end', 'number_work_days'],
                'sums'=>['number_work_days']
            ];
        }
    }
    
    public function getDisplayModel()
    {
        return $this->owner->absence;
    }
}
