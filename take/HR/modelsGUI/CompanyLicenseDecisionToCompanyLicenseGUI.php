<?php

class CompanyLicenseDecisionToCompanyLicenseGUI extends SIMAActiveRecordGUI
{
    public function columnLabels()
    {
        return array(
            'company_license_decision_id' => 'Rešenje o velikoj licenci',
            'company_license_decision' => 'Rešenje o velikoj licenci',
            'company_license_id' => 'Velika licenca',
            'company_license' => 'Velika licenca'           
        ) + parent::columnLabels();
    }

    public function modelLabel($plural = false)
    {
        return 'Rešenje velike licence';
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            case 'byCompanyLicenseDecision': return [
                'columns'=>[
                    'company_license'
                ]
            ];
            default: return array(
                'columns'=>array(
                    'company_license_decision', 'company_license'
                )                
            );
        }
    }
    
    public function modelForms() {
        return array(
            'default' => array(
                'type' => 'generic',
                'columns' => array(
                    'company_license_decision_id'=>array('relation','relName'=>'company_license_decision'),
                    'company_license_id'=>array('relation','relName'=>'company_license'),
                )
            )
        );
    }

}

?>


