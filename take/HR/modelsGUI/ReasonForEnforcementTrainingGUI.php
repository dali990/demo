<?php

class ReasonForEnforcementTrainingGUI extends SIMAActiveRecordGUI
{

    public function columnLabels()
    {
        return array(
            'name' => 'Naziv',
            'code' => 'Oznaka',
            'description' => 'Opis'
        ) + parent::columnLabels();
    }

    public function modelLabel($plural = false)
    {
        return 'Razlog za izvršenje osposobljavanja';
    }

    public function modelViews()
    {
        return array(
                ) + parent::modelViews();
    }

    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default: return array(
                    'columns' => array(
                        'name', 'code', 'description'
                    )
                );
        }
    }

    public function modelForms()
    {
        return array(
            'default' => array(
                'type' => 'generic',
                'columns' => array(
                    'name' => 'textField',
                    'code' => 'textField',
                    'description' => 'textArea',
                )
            )
        );
    }

}
