<?php

class SchoolCourseGUI extends SIMAActiveRecordGUI
{

    public function columnLabels()
    {
        return array(
            'school' => 'Skola',
            'school_id' => 'Skola',
            'code' => 'Oznaka'
        ) + parent::columnLabels();
    }

    public function modelLabel($plural = false)
    {
        return 'Smer';
    }

//    public function modelViews()
//    {
//        return array(
//                ) + parent::modelViews();
//    }

    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default: return array(
                'columns' => array(
                    'school.company.name', 'name', 'description'
                )
            );
        }
    }

    public function modelForms()
    {
        return array(
            'default' => array(
                'type' => 'generic',
                'columns' => array(
                    'school_id' => ['relation','relName'=>'school', 'add_button' => true],
                    'name' => 'textField',
                    'description' => 'textArea',
                )
            )
        );
    }

}
