<?php

/**
 * Description of SafeEvd2GUI
 *
 * @author goran
 */
class SafeEvd2GUI extends SIMAActiveRecordGUI
{

    public function columnLabels()
    {
        return array(
            'id' => 'Redni broj',
            'work_position_id' => 'Radno mesto',
            'work_position' => 'Radno mesto',
            'medical_report_period_id' => 'Interval periodičnih pregleda',
            'medical_report_period' => 'Interval periodičnih pregleda',
            'person' => 'Osoba',
            'person_id' => 'Osoba',
            'date_previous_report' => 'Datum pregleda',
            'date_next_report' => 'Datum sledećeg pregleda',
            'medical_referral_id' => 'Lekarski uput',
            'medical_referral' => 'Lekarski uput',
            'generate_medical_referral' => 'Generiši lekarski uput',
            'healthy_mark' => 'Ocena zdravlja',
            'actions' => 'Preduzete mere',
            'description' => 'Opis',
            'file_id' => 'Dokument',
            'report_code' => 'Broj lekarskog izveštaja',
            'add_not_passed_medical_abilities' => 'Lekarski pregledi koje osoba nije prošla'
        ) + parent::columnLabels();
    }

    public function modelLabel($plural = false)
    {
        return Yii::t('HRModule.SafeEvd2', $plural ? 'MedicalReports' : 'MedicalReport');
    }

    public function columnDisplays($column)
    {
        $owner = $this->owner;
        switch ($column)
        {
            case 'person': 
                $html = $owner->person->DisplayHtml;
                if (!isset($owner->person->employee->active_work_contract))
                {
                    $html .= ' (ne radi)';
                }
                return $html;
            case 'generate_medical_referral':
                $button = '';
                $action = [
                    'title'=>'Generiši',
                    'tooltip'=>'Generiši',
                    'onclick'=>[
                        'sima.hr.generateMedicalReferral',$owner->id,[
                            'referral_type'=>MedicalExaminationReferral::$SAFE_EVD_2,
                            'filter_scopes'=>'noConnectionToSafeEvd2'
                        ],$owner->medical_referral_id
                    ]
                ];
                if(SIMAMisc::isVueComponentEnabled())
                {
                    $button = Yii::app()->controller->widget(SIMAButtonVue::class, $action, true);
                }
                else
                {
                    $button = Yii::app()->controller->widget(SIMAButton::class, [
                        'action' => $action
                    ], true);
                }
                
                if (isset($owner->medical_referral))
                {
                    $button .= SIMAHtml::fileDownload($owner->medical_referral).$owner->medical_referral->DisplayHtml;
                }
                return $button;
            case 'healthy_mark': 
                switch ($owner->healthy_mark) 
                {
                    case 'ABLE': return 'sposoban';
                    case 'NOT_ABLE':
                        $html = '';
                        foreach ($owner->not_passed_medical_abilities as $not_passed_medical_ability)
                        {
                            $html .= $not_passed_medical_ability->DisplayName.'<br/>';
                        }
                        return $html;
                    default: return '';
                };
            default: return parent::columnDisplays($column);
        }
    }

    public function modelViews()
    {
        return array(
            'inFile'
                ) + parent::modelViews();
    }

    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default: return array(
                    'columns' => [
                        'person', 'work_position', 'generate_medical_referral', 'report_code', 'date_previous_report', 'date_next_report', 'healthy_mark', 'actions', 'description'
                    ],
                    'order_by' => ['date_previous_report','date_next_report']
                );
        }
    }

    public function modelForms()
    {
        return array(
            'default' => array(
                'type' => 'generic',
                'columns' => array(
                    'person_id' => array('searchField', 'relName' => 'person'),
                    'work_position_id' => array('searchField', 'relName' => 'work_position'),
                    'date_previous_report' => 'datetimeField',
                    'medical_report_period_id' => array('searchField', 'relName' => 'medical_report_period', 'add_button'=>true),
                    'report_code' => 'textField',
                    'healthy_mark' => 'dropdown',
                    'add_not_passed_medical_abilities'=>array('list',
                        'model'=>'RequiredMedicalAbility',
                        'relName'=>'not_passed_medical_abilities',
                        'multiselect'=>true
                    ),
                    'actions' => 'textField',
                    'medical_referral_id' => [
                        'relation', 
                        'relName' => 'medical_referral',
                        'filters' => [
                            'referral_type'=>MedicalExaminationReferral::$SAFE_EVD_2,
                            'filter_scopes'=>'noConnectionToSafeEvd2',
                        ],
                        'dependent_on' => [
                            'person_id' => [
                                'onValue' => [
                                    'enable',
                                    ['condition', 'model_filter' => 'person.ids']
                                ],
                                'onEmpty' => 'disable'
                            ]
                        ]
                    ],
                    'description' => 'textArea',
                )
            ),
        );
    }
    
    public function droplist($key, $relName='', $filter=null) 
    {
        switch ($key) {
            case 'healthy_mark': return array('ABLE' => 'sposoban', 'NOT_ABLE' => 'nije sposoban');            
            default: return parent::droplist($key, $relName, $filter);
        }
    }
}
