<?php

class MedicalExaminationReferralGUI extends SIMAActiveRecordDisplayGUI
{
    public function columnLabels()
    {
        return array(            
            'description' => 'Opis',
            'add_safe_evd' => 'Dodaj lekarski pregled',
            'file' => 'Dokument',
            'person_id' => Yii::t('HRModule.MedicalExaminationReferral', 'Person'),
            'person' => Yii::t('HRModule.MedicalExaminationReferral', 'Person'),
            'work_position_id' => Yii::t('HRModule.MedicalExaminationReferral', 'WorkPosition'),
            'work_position' => Yii::t('HRModule.MedicalExaminationReferral', 'WorkPosition'),
            'referral_type' => Yii::t('HRModule.MedicalExaminationReferral', 'ReferralType')
        ) + parent::columnLabels();
    }

    public function modelLabel($plural = false)
    {
        return Yii::t('HRModule.MedicalExaminationReferral', $plural ? 'MedicalExaminationReferrals' : 'MedicalExaminationReferral');
    }
    
    public function modelViews()
    {
        return [
            'inFile'
        ] + parent::modelViews();
    }
    
    public function columnDisplays($column)
    {
        $owner = $this->owner;
        switch ($column)
        {
            case 'add_safe_evd':
                $action = 'sima.hr.addSafeEvd2';
                if ($owner->referral_type === MedicalExaminationReferral::$SAFE_EVD_13)
                {
                    $action = 'sima.hr.addSafeEvd13';
                }
                $btn_uniq_id = SIMAHtml::uniqid();
                $btn_params = [
                    'id' => $btn_uniq_id,
                    'title' => Yii::t('BaseModule.Common', 'Add'),
                    'onclick' => [$action, "$btn_uniq_id", $owner->id, Yii::app()->configManager->get('HR.safe_evd_2', false)]
                ];
                if(SIMAMisc::isVueComponentEnabled())
                {
                    $return = Yii::app()->controller->widget(SIMAButtonVue::class, $btn_params, true);
                }
                else
                {
                    $return = Yii::app()->controller->widget(SIMAButton::class, [
                        'id' => $btn_params['id'],
                        'action' => $btn_params
                    ], true);
                }

                return $return;
            default: return parent::columnDisplays($column);
        }
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default: return array(
                    'columns' => array(
                        'person', 'work_position', 'add_safe_evd', 'description'
                    )
                );
        }
    }

    public function modelForms()
    {
        return array(
            'default' => array(
                'type' => 'generic',
                'columns' => array(
                    'person_id' => ['relation', 'relName' => 'person'],
                    'work_position_id' => [
                        'relation', 
                        'relName' => 'work_position',
                        'dependent_on' => [
                            'person_id' => [
                                'onValue' => [
                                    'enable',
                                    ['trigger', 'func' => ['sima.hr.medicalExaminationReferralFormPersonChooseHandler']]
                                ],
                                'onEmpty' => ['disable']
                            ]
                        ]
                    ],
                    'description' => 'textArea',
                    'referral_type' => 'hidden'
                )
            )
        );
    }
    
    public function getDisplayModel()
    {
        $owner = $this->owner;

        return File::model()->resetScope()->findByPkWithCheck($owner->id);
    }
}
