<?php

class NationalEventSlavaToDayGUI extends SIMAActiveRecordGUI
{
    public function modelLabel($plural = false)
    {
        return Yii::t('HRModule.NationalEvent', $plural?'Slavas':'Slava');
    }
}

