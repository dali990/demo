<?php

/**
 * Description of SafeEvd12GUI
 *
 * @author goran
 */
class SafeEvd12GUI extends SafeEvd11_12_14GUI
{

    public function columnLabels()
    {
        return array() + parent::columnLabels();
    }

    public function modelLabel($plural = false)
    {
        return SafeRecordController::$SAFE_RECORDS[12]['name'];
    }

    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default: return array(
                    'columns' => array(
                        'safe_evd_4', 'date_request', 'date_request_writing',
                        'inspection_company', 'inspection_company_writing',
                        'inspection_person', 'inspection_person_writing', 'comment', 'description'
                    )
                );
        }
    }

    public function modelForms()
    {
        return array(
            'default' => array(
                'type' => 'generic',
                'columns' => array(
                    'safe_evd_4_id' => array('searchField', 'relName' => 'safe_evd_4'),
                    'date_request' => 'datetimeField',
                    'date_request_writing' => 'datetimeField',
                    'inspection_company_id' => array('searchField', 'relName' => 'inspection_company'),
                    'inspection_company_id_writing' => array('searchField', 'relName' => 'inspection_company_writing'),
                    'inspection_person_id' => array('searchField', 'relName' => 'inspection_person'),
                    'inspection_person_id_writing' => array('searchField', 'relName' => 'inspection_person_writing'),
                    'comment' => 'textArea',
                    'description' => 'textArea',
                )
            ),
        );
    }

}
