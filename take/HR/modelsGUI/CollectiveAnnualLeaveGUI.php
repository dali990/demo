<?php

class CollectiveAnnualLeaveGUI extends SIMAActiveRecordDisplayGUI
{
    public function columnLabels() {
        return array(
            'from_day_id'=>Yii::t('HRModule.Absence', 'FromDay'),
            'from_day'=>Yii::t('HRModule.Absence', 'FromDay'),
            '_date_from'=>Yii::t('HRModule.Absence', 'FromDay'),
            'to_day_id'=>Yii::t('HRModule.Absence', 'ToDay'),
            'to_day'=>Yii::t('HRModule.Absence', 'ToDay'),
            '_date_to'=>Yii::t('HRModule.Absence', 'ToDay'),
            'sector'=>Yii::t('HRModule.Absence', 'Sector'),
            'sector_id'=>Yii::t('HRModule.Absence', 'Sector'),
            'confirm' => Yii::t('BaseModule.Common', 'Confirm')
        )+parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return Yii::t('HRModule.Absence', 'CollectiveAnnualLeave');
    }
    
    public function modelForms()
    {
        return array(
            'default'=>array(
                'type'=>'generic',
                'columns'=>array(
                    '_date_from'=>'datetimeField',
                    '_date_to'=>'datetimeField',
                    'sector_id' => array('searchField','relName'=>'sector','filters'=>['company_id'=>Yii::app()->company->id]),    
                    'comment' => 'textArea',
                )
            )
        );
    }
    
    public function modelViews() {
        return array(
            'basicInfo',
            'basic_info',
            'full_info' => [
                'class' => 'sima-layout-panel'
            ],
            'title',
            'options'
         ) + parent::modelViews();
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default: return [
                'columns'=>[
                    'sector',
                    'from_day', 
                    'to_day', 
                    'comment'
                ],
            ];
        }
    }
}

