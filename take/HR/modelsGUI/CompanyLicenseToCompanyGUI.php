<?php

class CompanyLicenseToCompanyGUI extends SIMAActiveRecordGUI
{

    public function columnLabels()
    {
        return array(
            'company_license' => 'Velika licenca',
            'company_license_id' => 'Velika licenca',
            'company' => 'Kompanija',
            'company_id' => 'Kompanija',
        ) + parent::columnLabels();
    }

    public function modelLabel($plural = false)
    {
        return 'Velika licenca kompanije';
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            case 'byCompany': return [
                'columns'=>[
                    'company_license'
                ]
            ];
            default: return array(
                'columns'=>array(
                    'company_license','company'
                ),
                'order_by' => array('company_license')
            );
        }
    }
    
    public function modelForms() {
        return array(
            'default' => array(
                'type' => 'generic',
                'columns' => array(
                    'company_license_id'=>array('relation','relName'=>'company_license'),
                    'company_id'=>array('relation','relName'=>'company'),
                )
            )
        );
    }

}

?>
