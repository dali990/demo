<?php

class QualificationLevelGUI extends SIMAActiveRecordGUI
{
    
    public function columnLabels()
    {
        return [
            'code' => Yii::t('HRModule.QualificationLevel', 'Code'),
            'name' => Yii::t('HRModule.QualificationLevel', 'LevelAndType')
        ]+parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return Yii::t('HRModule.QualificationLevel', $plural ? 'QualificationLevels' : 'QualificationLevel');
    }
    
    public function modelForms()
    {
        return [
            'default'=>[
                'type'=>'generic',
                'columns'=>[
                    'name' => 'textField',
                    'code'=>'textField',
                    'description'=>'textArea'
                ]
            ]
        ];
    }
    
    public function guiTableSettings($type)
    {
        switch($type)
        {                
            default: return [
                'columns' => [
                    'name', 'code', 'description'
                ],
                'order_by'=>['code', 'name']
            ];
        }
    }
}
?>