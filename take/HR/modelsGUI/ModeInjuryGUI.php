<?php

class ModeInjuryGUI extends SIMAActiveRecordGUI
{

    public function columnLabels()
    {
        return array(
            'name' => Yii::t('HRModule.ModeInjury', 'Meaning'),
            'code' => Yii::t('HRModule.ModeInjury', 'Code'),
            'description' => Yii::t('HRModule.ModeInjury', 'Note'),
            'parent_id' => Yii::t('HRModule.ModeInjury', 'Type'),
            'parent' => Yii::t('HRModule.ModeInjury', 'Type'),
            'parent.name' => Yii::t('HRModule.ModeInjury', 'Type'),
        ) + parent::columnLabels();
    }

    public function modelLabel($plural = false)
    {
        return Yii::t('HRModule.ModeInjury', $plural ? 'ModeInjuries' : 'ModeInjury');
    }

    public function modelViews()
    {
        return array(
                ) + parent::modelViews();
    }

    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default: return [
                    'columns' => [
                        'name', 'code', 'description', 'parent.name'
                    ],
                    'order_by'=>['code']
                ];
        }
    }

    public function modelForms()
    {
        return array(
            'default' => array(
                'type' => 'generic',
                'columns' => array(
                    'name' => 'textField',
                    'code' => 'textField',
                    'description' => 'textArea',
                    'parent_id'=>['relation', 'relName' => 'parent', 'add_button' => true],
                )
            )
        );
    }

}
