<?php

class EntryDoorLogGUI extends SIMAActiveRecordGUI
{
    public function columnLabels()
    {
        return [
            'employee' => Yii::t('BaseModule.Common', 'Employee'),
            'time' => Yii::t('BaseModule.Common', 'Time'),
            'card' => Yii::t('HRModule.EntryDoor', 'Card'),
            'additional_data' => Yii::t('HRModule.EntryDoor', 'AdditionalData')
        ] + parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return Yii::t('HRModule.EntryDoor', $plural?'EntryDoorLogs':'EntryDoorLog');
    }
    
    public function columnDisplays($column)
    {
        $owner = $this->owner;
        switch ($column)
        {            
            case 'ocupied':
                $return = 'UNKNOWN';
                
                if(empty($owner->partner_id))
                {
                    $return = Yii::t('BaseModule.Common', 'No');
                }
                else
                {
                    $return = Yii::t('BaseModule.Common', 'Yes');
                }
                
                return $return;
            case 'type': 
                return $owner->getTypeData()[$owner->type]; 
            default : return parent::columnDisplays($column);
        }
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default: 
                $columns = [
                    'partner',
                    'time',
                    'type',
                    'additional_data',
                ];
                
                if(Yii::app()->entryDoorLog->haveEmployeeCard())
                {
                    $columns[] = 'card';
                }
                
                return array(
                    'columns' => $columns,
                    'order_by' => [
                        'time'
                    ]
                );
        }
    }
    
    public function droplist($key, $relName='', $filter=null) {
        switch ($key) {
            case 'type':
                $type_displays = $this->owner->getTypeData();
                return $type_displays;
            default: return parent::droplist($key, $relName, $filter);
        }
    }
}
