<?php

class AnnualLeaveDecisionGUI extends SIMAActiveRecordGUI
{
    public function columnLabels() {
        return array(            
            'employee_id'=>'Zaposleni',
            'employee'=>'Zaposleni',
            'work_contract_id'=>'Radni ugovor',
            'work_contract'=>'Radni ugovor',
            'year_id'=>'Godina',
            'year'=>'Godina',
            'days_number'=>'Broj dana',
            'description'=>'Napomena',
            'generate_document'=>'Generišite dokument',
            'calculated_days_number'=>'Preračunat broj dana',
            'calculated_days_number_until_today'=>'Preračunat broj dana do danas',
            'last_related_contract'=>'Poslednji vezani ugovor',
            'remaining_days_number'=>'Broj neiskorišćenih dana'
        )+parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return Yii::t('HRModule.AnnualLeaveDecision', $plural ? 'AnnualLeaveDecisions' : 'AnnualLeaveDecision');
    }
    
    public function columnDisplays($column)
    {
        $owner = $this->owner;
        switch ($column)
        {            
            case 'generate_document':
                return $owner->generateDocumentTemplate();
            case 'remaining_days_number':
                return $owner->getRemainingDays();
            case 'calculated_days_number':
            case 'calculated_days_number_until_today':
                $until_today = false;
                if ($column === 'calculated_days_number_until_today')
                {
                    $until_today = true;
                }
                $days_number = intval($owner->calcAnnualLeaveDays($until_today));
                if ($column === 'calculated_days_number_until_today' && $days_number !== intval($owner->days_number))
                {
                    $warn = '';
                    if ($days_number < intval($owner->days_number))
                    {                        
                        $used_days = intval($owner->days_number) - intval($owner->getRemainingDays());
                        if ($used_days > $days_number)
                        {
                            $warn = 'Iskoristili ste više dana nego sto imate praračunato.';
                        }
                    }
                    if(SIMAMisc::isVueComponentEnabled())
                    {
                        $days_number .= ' '.Yii::app()->controller->widget(SIMAButtonVue::class, [
                            'title'=>'Postavi',
                            'tooltip'=>'Postavi',
                            'onclick'=>['sima.hr.changeAnnualLeaveDaysNumber', $owner->id]
                        ], true);
                    }
                    else
                    {
                        $days_number .= ' '.Yii::app()->controller->widget('SIMAButton', [
                            'action'=>[ 
                                'title'=>'Postavi',
                                'tooltip'=>'Postavi',
                                'onclick'=>['sima.hr.changeAnnualLeaveDaysNumber', $owner->id]
                            ]
                        ], true);
                    }
                    
                    $days_number .= ' '.$warn;

                }                
                return $days_number;
            case 'last_related_contract':                
                $last_related_contract = $owner->getLastRelatedWorkContract($owner->work_contract);
                return $last_related_contract->DisplayHtml;
            default : return parent::columnDisplays($column);
        }
    }
    
    public function generateDocumentTemplate()
    {
        $owner = $this->owner;
        if (Yii::app()->user->checkAccess('GenerateDocument',[], $owner))
        {
            if (isset($owner->file->filing))
            {                
                return SIMAHtml::fileDownload($owner->file).
                    "<button class='sima-ui-button' onclick='sima.hr.generateAnnualLeaveDecisionDocument(".$owner->id.");'>Generiši</button>";
                
            }
            else
            {
                return $owner->renderFilingNumber();
            }
        }
        else
        {
            return 'Nemate privilegiju';
        }
    }
    
    public function renderFilingNumber()
    {        
        $owner = $this->owner;
        $file = File::model()->findByPk($owner->id);
        $registry_id = Yii::app()->configManager->get('legal.employee_history_register', false);
        $personal_delivery_type = Yii::app()->configManager->get('legal.personal_delivery_type', false);
        $filing_to_delivery_types = ($personal_delivery_type!==null) ? [
            'ids' => '{"ids":[{"id":'.$personal_delivery_type.',"display_name":"Lično","class":"_visible"}],"default_item":[]}'
        ] : null;
        if ($file !== null)
        {            
            $file_init_data = [
                'responsible_id' => ($owner->file->responsible_id !== null) ? $owner->file->responsible_id : Yii::app()->user->id
            ];
            $filing_init_data = [
                'confirmed' => true,
                'partner_id' => $owner->employee_id,
                'in' => false,
                'filing_to_delivery_types' => $filing_to_delivery_types,
                'registry_id' => $registry_id,
            ];
            $file_init_data_json = CJSON::encode($file_init_data);
            $filing_init_data_json = CJSON::encode($filing_init_data);
            $update_tag = SIMAHtml::getTag($owner);
            return "<span class='link' onclick='sima.hr.addAnnualLeaveDecisionFilingNumber($owner->id,$file_init_data_json,$filing_init_data_json,\"$update_tag\")'>zavedi</span>";
        }
        else
        {
            return '';
        }
    }
    
    public function modelForms()
    {
        $owner = $this->owner;
        $work_contract_model_filter = [];
        if (!empty($owner->employee_id))
        {
            $work_contract_model_filter = [
                'employee' => [
                    'ids' => $owner->employee_id
                ],
                'order_by' => "start_date DESC"
            ];
        }
        $employee_id = array('relation', 'relName'=>'employee');
        if (!empty($owner->employee_id))
        {
            $employee_id = array('relation', 'relName'=>'employee', 'disabled'=>true);
        }                
        
        return array(
            'default'=>array(
                'type'=>'generic',
                'columns'=>array(                    
                    'employee_id'=>$employee_id,
                    'work_contract_id'=>array('relation', 'relName'=>'work_contract', 'filters'=>$work_contract_model_filter, 'dependent_on'=>[
                        'employee_id'=>[
                            'onValue'=>[                                
                                ['condition', 'model_filter' => 'employee.ids']
                            ],
                            'onEmpty'=>[                                
                                ['condition', 'model_filter' => 'employee.ids']
                            ]
                        ]
                    ]),
                    'year_id'=>array('relation', 'relName'=>'year', 'filters'=>['order_by' => "year DESC"]),
                    'days_number'=>'textField',
                    'description'=>'textArea'
                )
            ),
            'only_days_number'=>[
                'type'=>'generic',
                'columns'=>array(                    
                    'days_number'=>'textField'
                )
            ]
        );
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            case 'byYear': return [
                'columns' => array('employee' ,'work_contract', 'last_related_contract', 'days_number', 'remaining_days_number', 
                    'calculated_days_number', 'calculated_days_number_until_today', 'generate_document', 'description'),
                'order_by'=>'employee'
            ];
            case 'from_user': return [
                'columns' => array('work_contract', 'last_related_contract', 'days_number', 'remaining_days_number', 'calculated_days_number', 
                    'calculated_days_number_until_today', 'generate_document', 'description'),
                'sums'=>['days_number'],
                'filters'=>['year']
            ];
            default: return [
                'columns' => array('employee' ,'work_contract', 'last_related_contract', 'year', 'days_number', 'remaining_days_number', 
                    'calculated_days_number', 'calculated_days_number_until_today', 'generate_document', 'description'),
                'filters'=>['year']
            ];
        }

    }
}
