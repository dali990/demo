<?php

class EmployeeCandidateGUI extends SIMAActiveRecordGUI
{

    public function columnLabels()
    {
        return array(
            'id' => 'Osoba',
            'school_course_id' => 'Zavrsena skola',
            'school_course' => 'Zavrsena skola',
            'work_position_competitions' => 'Konkursi',
            'note' => 'Beleska'
        )+parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return 'Kandidat';
    }

//    public function modelViews()
//    {
//        return array(
//            'basicInfoTitle',
//            'basicInfo',
//            'current_materials_status'=>array('params_function'=>'viewParamsCurrentMaterialsStatus')
////            'leaf'=>array(
//////                'style'=>'border: 1px solid;'
////                ),
////            'default_edit'
////            'tabTemplates'=>array('with'=>array('templated_file'),'style'=>'border: 1px solid;')  //.creator
//        );
//    }
    
   
    public function columnDisplays($column)
    {
        $owner = $this->owner;
        switch($column)
        {
            case 'status_id' : return $this->droplist('status_id')[$owner->$column];
            case 'work_position_competitions' : 
                $ret = '';
                foreach ($owner->work_position_competitions as $position)
                {
                    $ret .= '<div>';
                    $ret .= $position->DisplayName;
                    $ret .= '</div>';     
                }
                return $ret;
            default: return parent::columnDisplays($column);
        }
        
    }
    
    public function modelForms()
    {
        return array(
            'default'=>array(
                'type'=>'generic',
                'columns'=>array(
//                    'id'=>array('searchField','relName'=>'person','add_button'=>true),
//                    'active' => 'checkBox',
                    'school_course_id' => ['relation','relName'=>'school_course','add_button'=>true],
                    'work_position_competitions'=>array('list',
                        'model'=>'WorkPositionCompetition',
                        'relName'=>'work_position_competitions',
                        'multiselect' => true
                    ),
//                    'grade_id'=>array('dropdown','empty' => 'Nije ocenjen'),
//                    'status_id' => array('dropdown'),
//                    'interview_time' => array('datetimeField','mode'=>'datetime'),
//                    'partner_id'=>array('relation','relName'=>'partner'),
                    'note'=>'textArea'
                )
            )
        );
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {

            default: return array(
                'columns' => array(
                    'person.firstname', 'person.lastname', 'note', 'work_position_competitions' //'grade_id','status_id','interview_time', 'note'
                ),
                //'order_by' => array('grade_id','interview_time')
            );
        }
    }
    
    public function droplist($key, $relName='', $filter=null)
    {
        switch ($key)
        {
            case 'grade_id': return array(
                '1' => '1',
                '2' => '2',
                '3' => '3',
                '4' => '4',
                '5' => '5',
                '6' => '6',
            );
            case 'status_id': return array(
                '1' => 'Prijavio se',
                '2' => 'Pozvan',
                '3' => 'Nije dosao',
                '4' => 'Dosao - nije prosao',
                '5' => 'Dosao - prosao - nije prihvatio',
                '6' => 'Dosao - prosao - prihvatio',
            );
            default: return parent::droplist($key, $relName, $filter);
        }
    }
    
}