<?php

class SafeEvd8_10GUI extends SIMAActiveRecordGUI
{

    public function columnLabels()
    {
        return array(
            'id' => 'Redni broj',
            'data' => 'Podaci',
            'num_expert_report' => 'Broj stručnog nalaza',
            'date_control' => 'Datum ispitivanja',
            'date_next_control' => 'Datum sledećeg ispitivanja',
            'comment' => 'Napomena',
            'description' => 'Opis'
                ) + parent::columnLabels();
    }

    public function modelViews()
    {
        return array() + parent::modelViews();
    }

    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default: return array(
                    'columns' => array(
                        'data', 'num_expert_report', 'date_control', 'date_next_control', 'comment', 'description'
                    )
                );
        }
    }

    public function modelForms()
    {
        return array(
            'default' => array(
                'type' => 'generic',
                'columns' => array(
                    'data' => 'textField',
                    'num_expert_report' => 'textField',
                    'date_control' => 'datetimeField',
                    'date_next_control' => 'datetimeField',
                    'comment' => 'textArea',
                    'description' => 'textArea'
                )
            ),
        );
    }

}
