<?php

class SchoolGUI extends SIMAActiveRecordGUI
{

    public function columnLabels()
    {
        return array(
            'id' => 'Naziv',
            'code' => 'Oznaka',
            'short_name' => 'Skraceno ime'
        ) + parent::columnLabels();
    }

    public function modelLabel($plural = false)
    {
        return 'Obrazovna institucija';
    }

//    public function modelViews()
//    {
//        return array(
//                ) + parent::modelViews();
//    }

    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default: return array(
                'columns' => array(
                    'company.name'
                )
            );
        }
    }

    public function modelForms()
    {
        return array(
            'default' => array(
                'type' => 'generic',
                'columns' => array(
                    'id' => ['relation','relName'=>'company', 'add_button' => true],
                    'short_name' => ['textField'],
                )
            )
        );
    }

}
