<?php

/**
 * Description of SafeWorkDangerGUI
 *
 * @author goran
 */
class SafeWorkDangerGUI extends SIMAActiveRecordGUI
{

    public function columnLabels()
    {
        return array(
            'description' => 'Opis',
            'code' => 'Oznaka'
                ) + parent::columnLabels();
    }

    public function modelLabel($plural = false)
    {
        return 'Šifra opasnosti/štetnosti';
    }

    public function modelViews()
    {
        return array(
                ) + parent::modelViews();
    }

    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default: return array(
                    'columns' => array(
                        'code', 'description'
                    )
                );
        }
    }

    public function modelForms()
    {
        return array(
            'default' => array(
                'type' => 'generic',
                'columns' => array(
                    'code' => 'textField',
                    'description' => 'textArea',
                )
            )
        );
    }

}
