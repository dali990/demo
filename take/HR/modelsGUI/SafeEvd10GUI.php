<?php

/**
 * Description of SafeEvd10GUI
 *
 * @author goran
 */
class SafeEvd10GUI extends SIMAActiveRecordGUI
{
    public function columnLabels()
    {
        return array(
            'id' => 'Redni broj',
            'data' => 'Podaci',            
            'date_control' => 'Datum ispitivanja',
            'date_next_control' => 'Datum sledećeg ispitivanja',
            'comment' => 'Napomena',
            'description' => 'Opis'
        ) + parent::columnLabels();
    }

    public function modelLabel($plural = false)
    {
        return SafeRecordController::$SAFE_RECORDS[10]['name'];
    }
 
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default: return array(
                    'columns' => array(
                        'data', 'date_control', 'date_next_control', 'comment', 'description'
                    )
                );
        }
    }
    
    public function modelForms()
    {
        return array(
            'default' => array(
                'type' => 'generic',
                'columns' => array(
                    'data' => 'textField',
                    'date_control' => 'datetimeField',
                    'date_next_control' => 'datetimeField',
                    'comment' => 'textArea',
                    'description' => 'textArea'
                )
            ),
        );
    }

}
