<?php

/**
 * Description of SafeEvd14GUI
 *
 * @author goran
 */
class SafeEvd14GUI extends SafeEvd11_12_14GUI
{

    public function columnLabels()
    {
        return array() + parent::columnLabels();
    }

    public function modelLabel($plural = false)
    {
        return SafeRecordController::$SAFE_RECORDS[14]['name'];
    }

    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default: return array(
                    'columns' => array(
                        'description_appearance', 'date_request', 'date_request_writing',
                        'inspection_company', 'inspection_company_writing',
                        'inspection_person', 'inspection_person_writing',
                        'oup_company', 'oup_company_writing',
                        'oup_person', 'oup_person_writing', 'comment', 'description'
                    )
                );
        }
    }

    public function modelForms()
    {
        return array(
            'default' => array(
                'type' => 'generic',
                'columns' => array(
                    'description_appearance' => 'textArea',
                    'date_request' => 'datetimeField',
                    'date_request_writing' => 'datetimeField',
                    'inspection_company_id' => array('searchField', 'relName' => 'inspection_company'),
                    'inspection_company_id_writing' => array('searchField', 'relName' => 'inspection_company_writing'),
                    'inspection_person_id' => array('searchField', 'relName' => 'inspection_person'),
                    'inspection_person_id_writing' => array('searchField', 'relName' => 'inspection_person_writing'),
                    'oup_company_id' => array('searchField', 'relName' => 'oup_company'),
                    'oup_company_id_writing' => array('searchField', 'relName' => 'oup_company_writing'),
                    'oup_person_id' => array('searchField', 'relName' => 'oup_person'),
                    'oup_person_id_writing' => array('searchField', 'relName' => 'oup_person_writing'),
                    'comment' => 'textArea',
                    'description' => 'textArea',
                )
            ),
        );
    }

}
