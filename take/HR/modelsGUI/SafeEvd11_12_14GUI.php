<?php

class SafeEvd11_12_14GUI extends SIMAActiveRecordGUI
{

    public function columnLabels()
    {
        return array(
            'id' => 'Redni broj',
            'safe_evd_3_id' => 'Povreda na radu',
            'safe_evd_4_id' => 'Profesionalno oboljenje/bolest',
            'safe_evd_5_id' => 'Bolest u vezi sa radom',
            'safe_evd_3' => 'Povreda na radu',
            'safe_evd_4' => 'Profesionalno oboljenje/bolest',
            'safe_evd_5' => 'Bolest u vezi sa radom',
            'date_request' => 'Datum podnošenja usmeno',
            'date_request_writing' => 'Datum podnošenja pismeno',
            'inspection_company_id' => 'Inspekcija-Sedište kome je podneta prijava usmeno',
            'inspection_company' => 'Inspekcija-Sedište kome je podneta prijava usmeno',
            'inspection_company_id_writing' => 'Inspekcija-Sedište kome je podneta prijava pismeno',
            'inspection_company_writing' => 'Inspekcija-Sedište kome je podneta prijava pismeno',
            'inspection_person_id' => 'Inspekcija-Lice koje je primilo prijavu usmeno',
            'inspection_person_id_writing' => 'Inspekcija-Lice koje je primilo prijavu pismeno',
            'inspection_person' => 'Inspekcija-Lice koje je primilo prijavu usmeno',
            'inspection_person_writing' => 'Inspekcija-Lice koje je primilo prijavu pismeno',
            'oup_company_id' => 'OUP-Sedište kome je podneta prijava usmeno',
            'oup_company_id_writing' => 'OUP-Sedište kome je podneta prijava pismeno',
            'oup_person_id' => 'OUP-Lice koje je primilo prijavu usmeno',
            'oup_person_id_writing' => 'OUP-Lice koje je primilo prijavu pismeno',
            'oup_company' => 'OUP-Sedište kome je podneta prijava usmeno',
            'oup_company_writing' => 'OUP-Sedište kome je podneta prijava pismeno',
            'oup_person' => 'OUP-Lice koje je primilo prijavu usmeno',
            'oup_person_writing' => 'OUP-Lice koje je primilo prijavu pismeno',
            'comment' => 'Napomena',
            'description_appearance' => 'Opis opasne pojave',
            'description' => 'Opis'
                ) + parent::columnLabels();
    }

    public function modelViews()
    {
        return array() + parent::modelViews();
    }

    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default: return array(
                    'columns' => array(
                        'safe_evd_3', 'safe_evd_4', 'safe_evd_5', 'date_request', 'date_request_writing', 'inspection_company',
                        'inspection_company_writing', 'inspection_person', 'inspection_person_writing', 'oup_company', 'oup_company_writing',
                        'oup_person', 'oup_person_writing', 'comment', 'description_appearance', 'description'
                    )
                );
        }
    }

    public function modelForms()
    {
        return array(
            'default' => array(
                'type' => 'generic',
                'columns' => array(
                    'safe_evd_3_id' => array('searchField', 'relName' => 'safe_evd_3'),
                    'safe_evd_4_id' => array('searchField', 'relName' => 'safe_evd_4'),
                    'safe_evd_5_id' => array('searchField', 'relName' => 'safe_evd_5'),
                    'date_request' => 'datetimeField',
                    'date_request_writing' => 'datetimeField',
                    'inspection_company_id' => array('searchField', 'relName' => 'inspection_company'),
                    'inspection_company_id_writing' => array('searchField', 'relName' => 'inspection_company_writing'),
                    'inspection_person_id' => array('searchField', 'relName' => 'inspection_person'),
                    'inspection_person_id_writing' => array('searchField', 'relName' => 'inspection_person_writing'),
                    'oup_company_id' => array('searchField', 'relName' => 'oup_company'),
                    'oup_company_id_writing' => array('searchField', 'relName' => 'oup_company_writing'),
                    'oup_person_id' => array('searchField', 'relName' => 'oup_person'),
                    'oup_person_id_writing' => array('searchField', 'relName' => 'oup_person_writing'),
                    'comment' => 'textArea',
                )
            ),
        );
    }

}
