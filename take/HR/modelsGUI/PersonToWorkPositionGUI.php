<?php

/**
 * Description of PersonToWorkPositionGUI
 *
 * @author goran
 */
class PersonToWorkPositionGUI extends SIMAActiveRecordGUI {

    public function columnLabels()
    {
        return array(
            'person_id'=>'Osoba',
            'person'=>'Osoba',
            'work_position_id'=>'Radna pozicija',
            'work_position'=>'Radna pozicija',
            'fictive'=>'Fiktivna'
        ) + parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return 'Radna pozicija za osobu';
    }

    public function modelViews() 
    {
        $owner = $this->owner;        
        return array(
            'personBasicView' => array(
                'html_wrapper' => null                
            ),
        );
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            case 'fromPerson': return [
                'columns'=>[
                    'work_position'
                ]
            ];
            case 'fromWorkPosition': return [
                'columns'=>[
                    'person'
                ]
            ];
            default: return array(
                'columns' => array(
                    'person','work_position'
                )                
            );
        }
    }
    
    public function modelForms() {
        $owner = $this->owner;
        $person_id = ['relation', 'relName'=>'person'];
        $work_position_id = ['relation', 'relName'=>'work_position'];
//        if (!empty($owner->person_id))
//        {
//            $person_id['disabled'] = true;
//        }
//        if (!empty($owner->work_position_id))
//        {
//            $work_position_id['disabled'] = true;
//        }
        return array(
            'default'=>array(
                'type'=>'generic',
                'columns'=>array(                    
                    'person_id'=>$person_id,
                    'work_position_id'=>$work_position_id,
                    'fictive'=>'hidden'
                )
            )
        );
    }

}

?>
