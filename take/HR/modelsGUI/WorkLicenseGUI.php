<?php
/**
 * Description of CompanySectorGUI
 *
 * @author goran
 */
class WorkLicenseGUI extends SIMAActiveRecordGUI
{
    public function columnLabels() {
        return array(
            'name' => 'Naziv',
            'conditions' => 'Uslovi',
            'issuer_id' => 'Izdavač',
            'issuer' => 'Izdavač',
            'all_persons_count'=>'Broj osoba',
            'all_employees_count'=>'Broj zaposlenih',
            'code' => 'Oznaka'
        ) + parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return 'Licenca';
    }
    
    public function modelViews() {
         return array(
           'basicInfoTitle'=>array(),
           'basicInfo'=>array()
        ) + parent::modelViews();
    }
    
    public function modelForms() {
        return array(
            'default' => array(
                'type' => 'generic',
                'columns' => array(
                    'issuer_id' => array('relation', 'relName' => 'issuer'),
                    'code' => 'textField',
                    'name' => 'textField',
                    'conditions' => 'textArea',
                    'description' => 'textArea',
                )
            )
        );
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default: return array(
                'columns' => array(
                    'code', 'name', 'issuer'
                )
            );
        }
    }
    
//    public function droplist($key, $relName='', $filter=null)
//    {
//    	switch ($key)
//    	{
//    		case 'issuer_id': 	return CompanySector::model()->getModelDropList();    
//    		default:		return parent::droplist($key, $relName, $filter);
//    	}
//    }
}

?>
