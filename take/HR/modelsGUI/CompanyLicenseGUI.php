<?php

class CompanyLicenseGUI extends SIMAActiveRecordGUI
{

    public function columnLabels()
    {
        return array(
            'name' => 'Naziv',
            'description' => 'Opis',            
            'workLicenses' => 'Male licence',
            'persons_number' => 'Broj osoba',
            'references_number' => 'Broj referenci',
            'code' => 'Šifra',
            'person_work_licenses' => 'Male licence'
        ) + parent::columnLabels();
    }

    public function modelLabel($plural = false)
    {
        return 'Velika licenca';
    }
    
    public function modelViews()
    {
        return array(
            'info'            
        );
    }
    
    public function columnDisplays($column) 
    {
        $owner = $this->owner;
        switch ($column) {
            case 'person_work_licenses':
                $licenses = '';
                foreach ($owner->work_licenses as $work_license)
                {
                    $licenses .= "<span title='$work_license->DisplayName'>$work_license->code</span><br />";
                }
                return $licenses;
            default: return parent::columnDisplays($column);
        }
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default: return array(
                'columns'=>array(
                    'code','name','description','persons_number','references_number','person_work_licenses'
                ),
            );
        }
    }
    
    public function modelForms() {
        return array(
            'default' => array(
                'type' => 'generic',
                'columns' => array(
                    'code' => 'textField',
                    'name' => 'textField',                    
                    'description' => 'textArea',
                    'persons_number'=>'textField',
                    'workLicenses'=>array('list',
                        'model'=>'WorkLicense',
                        'relName'=>'work_licenses'
                    ),
                    'references_number'=>'textField'
                )
            )
        );
    }

}

?>
