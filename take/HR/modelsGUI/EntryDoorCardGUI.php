<?php

class EntryDoorCardGUI extends SIMAActiveRecordGUI
{
    public function columnLabels()
    {
        return [
            'card_number' => Yii::t('HRModule.EntryDoor', 'Card'),
            'partner' => Yii::t('HRModule.EntryDoor', 'Partner'),
            'ocupied' => Yii::t('HRModule.EntryDoor', 'Ocupied'),
        ] + parent::columnLabels();
    }
    
    public function columnDisplays($column)
    {
        $owner = $this->owner;
        switch ($column)
        {            
            case 'ocupied':
                $return = 'UNKNOWN';
                
                if(empty($owner->partner_id))
                {
                    $return = Yii::t('BaseModule.Common', 'No');
                }
                else
                {
                    $return = Yii::t('BaseModule.Common', 'Yes');
                }
                
                return $return;
                   
            default : return parent::columnDisplays($column);
        }
    }
    
    public function modelForms()
    {
        return array(
            'default'=>array(
                'type'=>'generic',
                'columns' => [
                    'card_number'=>'textField',
                    'partner_id'=>array('relation', 'relName'=>'partner'),
                    'comment' => 'textArea'
                ]
            ),
        );
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default: return array(
                'columns' => [
                    'card_number',
                    'partner',
                    'ocupied',
                    'comment'
                ],
                'order_by' => [
                    'card_number',
                    'partner'
                ]
            );
        }
    }
    
    public function droplist($key, $relName='', $filter=null) {
        switch ($key) {
            case 'ocupied':
                return [
                    1 => Yii::t('BaseModule.Common', 'Yes'),
                    0 => Yii::t('BaseModule.Common', 'No')
                ];
            default: return parent::droplist($key, $relName, $filter);
        }
    }
}
