<?php

class NationalEventStateHolidayToDayGUI extends SIMAActiveRecordGUI
{
    public function modelLabel($plural = false)
    {
        return Yii::t('HRModule.NationalEvent', $plural?'StateHolidays':'StateHoliday');
    }
}

