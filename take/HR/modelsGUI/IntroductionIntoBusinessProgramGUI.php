<?php

class IntroductionIntoBusinessProgramGUI extends SIMAActiveRecordGUI
{
    public function columnLabels() {
        return array(
            'number'=>Yii::t('BaseModule.Common', 'Number'),
            'year'=>Yii::t('BaseModule.Common', 'Year'),
            'file'=>Yii::t('BaseModule.Common', 'File')
        )+parent::columnLabels();
    }
//    
//    public function modelLabel($plural = false)
//    {
//        return Yii::t('HRModule.NationalEvent', 'NationalEvent');
//    }
//    
//    public function columnDisplays($column)
//    {
//        $owner = $this->owner;
//        switch ($column)
//        {
////            case 'type':                
////                $type_displays = $this->owner->getTypesDisplayArray();
////                $result = $type_displays[$owner->type];
////                return $result;
////            case 'year':
////                $result = (new DateTime($owner->work_contract->start_date))->format('Y');
////                return $result;
//            default: return parent::columnDisplays($column); 
//        }
//    }
//    
//    public function modelForms()
//    {
//        return array(
//            'default'=>array(
//                'type'=>'generic',
//                'columns'=>array(
//                    'name' => 'textField',
//                    'type' => array('dropdown', 'empty' => Yii::t('BaseModule.Common', 'Choose')),
//                    '_day_date' => 'datetimeField',
//                )
//            )
//        );
//    }
//    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default: return [
                'columns'=>[
                    'number',
                    'year',
                    'file'
                ],
                'order_by' => [
                    'number',
                    'year'
                ]
            ];
        }
    }
//    
//    public function droplist($key, $relName='', $filter=null) 
//    {
//        switch ($key) 
//        {
//            case 'type':
//                $type_displays = $this->owner->getTypesDisplayArray();
//                return $type_displays;
//            default: 
//                return parent::droplist($key, $relName, $filter);
//        }
//    }
}