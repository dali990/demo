<?php

/**
 * Description of CompanySectorGUI
 *
 * @author goran
 */
class CompanySectorGUI extends SIMAActiveRecordDisplayGUI
{

    public function columnLabels()
    {
        return array(
            'name' => 'Naziv',
            'type' => 'Tip',
            'parent_id' => 'Nad sektor',
            'code' => Yii::t('HRModule.CompanySector', 'Code'),
            'description' => 'Opis'
                ) + parent::columnLabels();
    }

    public function modelLabel($plural = false)
    {
        return 'Sektor';
    }
    
    public function columnDisplays($column)
    {
        return parent::columnDisplays($column);
    }

    public function modelViews()
    {
        return array(
            'sectorStructure' => array(
                'with' => 'childrens.childrens',                
                'html_wrapper' => null
            ),
            'basic_info',
            'full_info' => ['html_wrapper' => null],
            'full_info_solo' => ['html_wrapper' => null],
            'basic_info_from_organization_scheme'
        ) + parent::modelViews();
    }

    public function modelForms()
    {
        $owner = $this->owner;
        $name = ['textField'];
        if(!$owner->isNewRecord && $owner->isHeadSector())
        {
            $name['disabled'] = 'disabled';
        }
        else if(empty($this->parent_id))
        {
            $owner->parent_id = Yii::app()->company->head_sector->id;
        }
        
        return array(
            'default' => array(
                'type' => 'generic',
                'columns' => array(
                    'code' => 'textField',
                    'type' => ['textField', 'placeholder' => Yii::t('HRModule.CompanySector', 'TypePlaceholder')],
                    'name' => $name,
                    'parent_id' => ['searchField', 'relName' => 'parent', 'filters'=>[
                        'company_id' => Yii::app()->company->id,
                        'scopes' => [
                            'orderBySearchNameASC'
                        ]
                    ]],
                    'description' => 'textArea',
                )
            )
        );
    }

    public function droplist($key, $relName='', $filter=null)
    {
        switch ($key)
        {
            case 'parent_id': return CompanySector::model()->getModelDropList();
            default: return parent::droplist($key, $relName, $filter);
        }
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default:
                return [
                    'columns' => ['name', 'type', 'parent_id']
                ];
        }
    }

}

?>
