<?php

class OccupationGUI extends SIMAActiveRecordGUI
{
    
    public function columnLabels()
    {
        return [
            'code' => Yii::t('HRModule.Occupation', 'Code'),
            'parent' => Yii::t('HRModule.Occupation', 'Parent'),
            'parent_id' => Yii::t('HRModule.Occupation', 'Parent'),
            'parent.name' => Yii::t('HRModule.Occupation', 'Parent')
        ]+parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return Yii::t('HRModule.Occupation', $plural ? 'Occupations' : 'Occupation');
    }
    
    public function modelForms()
    {
        return [
            'default'=>[
                'type'=>'generic',
                'columns'=>[
                    'name' => 'textField',
                    'code'=>'textField',
                    'parent_id'=>['relation', 'relName' => 'parent', 'add_button' => true],
                ]
            ]
        ];
    }
    
    public function guiTableSettings($type)
    {
        switch($type)
        {                
            default: return [
                'columns' => [
                    'name', 'code', 'parent.name'
                ],
                'order_by'=>['code', 'name']
            ];
        }
    }
}
?>