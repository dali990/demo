<?php

class RequiredMedicalAbilityGUI extends SIMAActiveRecordGUI
{

    public function columnLabels()
    {
        return array(
            'description' => 'Opis',
            'name' => 'Naziv'
        ) + parent::columnLabels();
    }

    public function modelLabel($plural = false)
    {
        return 'Zdravstvena sposobnost';
    }

    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default: return array(
                    'columns' => array(
                        'name', 'description'
                    )
                );
        }
    }

    public function modelForms()
    {
        return array(
            'default' => array(
                'type' => 'generic',
                'columns' => array(
                    'name' => 'textField',
                    'description' => 'textArea',
                )
            )
        );
    }

}
