<?php

class HRYearGUI extends SIMAActiveRecordGUI
{

    public function columnLabels()
    {
        return array(
            'year' => 'Godina',
            'must_be_used_until' => 'Datum do kada može da se iskoristi godišnji odmor',
            'number_of_annual_leave_days' => 'Broj dana godišnjeg odmora',
        ) + parent::columnLabels();
    }

    public function modelLabel($plural = false)
    {
        return Yii::t('HRModule.HRYear', $plural ? 'LegalYears' : 'LegalYear');
    }
    
    public function columnDisplays($column)
    {
        $owner = $this->owner;
        switch($column)
        {
            default: return parent::columnDisplays($column);
        }
        
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default: return array(
                'columns' => array(
                    'year','must_be_used_until','number_of_annual_leave_days'
                )
            );
        }
    }
    
    public function modelForms()
    {
        return array(
            'default' => array(
                'type' => 'generic',
                'columns' => array(
                    'year' => 'textField',
                    'must_be_used_until' => 'datetimeField',
                    'number_of_annual_leave_days' => 'textField'
                )
            )
        );
    }

}