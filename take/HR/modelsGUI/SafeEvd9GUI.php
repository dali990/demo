<?php

class SafeEvd9GUI extends SIMAActiveRecordGUI
{

    public function columnLabels()
    {
        return array(            
            'fixed_asset_id' => 'Mašina',
            'fixed_asset' => 'Mašina',
            'location' => 'Lokacija',
            'num_expert_report' => 'Broj stručnog nalaza',
            'date_control' => Yii::t('HRModule.SafeEvd9','ReviewDate'),
            'date_next_control' => Yii::t('HRModule.SafeEvd9','DateOfNextReview'),
            'comment' => 'Napomena',
            'description' => 'Opis',
            'file' => 'Dokument'
        ) + parent::columnLabels();
    }

    public function modelLabel($plural = false)
    {
        return SafeRecordController::$SAFE_RECORDS[9]['name'];
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default: return array(
                    'columns' => array(
                        'fixed_asset', 'file', 'location', 'num_expert_report', 'date_control', 'date_next_control', 'comment', 'description'
                    )
                );
        }
    }

    public function modelForms()
    {
        return array(
            'default' => array(
                'type' => 'generic',
                'columns' => array(
                    'fixed_asset_id' => array('relation', 'relName'=>'fixed_asset', 'add_button'=>true),
                    'location' => 'textField',                    
                    'num_expert_report' => 'textField',
                    'date_control' => 'datetimeField',
                    'date_next_control' => 'datetimeField',
                    'comment' => 'textField',
                    'description' => 'textArea'
                )
            ),
        );
    }

}
