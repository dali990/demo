<?php

class NationalEventToDayYearGUI extends SIMAActiveRecordGUI
{
    public function columnLabels() {
        return array(
            'national_event'=>Yii::t('HRModule.NationalEvent', 'NationalEvent'),
            'national_event_id'=>Yii::t('HRModule.NationalEvent', 'NationalEvent'),
            'day'=>Yii::t('BaseModule.Common', 'Day'),
            'day_id'=>Yii::t('BaseModule.Common', 'Day'),
            'year'=>Yii::t('BaseModule.Common', 'Year'),
            'year_id'=>Yii::t('BaseModule.Common', 'Year'),
            '_day_date'=>Yii::t('BaseModule.Common', 'Day')
        )+parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return Yii::t('HRModule.NationalEvent', 'NationalEventDay');
    }
    
    public function modelViews() 
    {
        return [
            'title'
        ];
    }
    
    public function modelForms()
    {
        return array(
            'default'=>array(
                'type'=>'generic',
                'columns'=>array(
                    'national_event_id'=>array('relation', 'relName'=>'national_event'),
                    '_day_date'=>'datetimeField'
                )
            )
        );
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default: return [
                'columns'=>[
                    'national_event', 
                    'day', 
                    'year',
                    'national_event.type'
                ],
                'order_by' => [
                    'day', 
                    'year'
                ],
            ];
        }
    }
}

