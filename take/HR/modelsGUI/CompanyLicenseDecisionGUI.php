<?php

class CompanyLicenseDecisionGUI extends SIMAActiveRecordGUI
{
    public function columnLabels() {
        return array(
            'file_id'=>'Rešenje',
            'file'=>'Rešenje',
            'decision_date'=>'Datum rešenja',
            'company_licenses_list'=>'Velike licence',
            'automatically_fill_with_the_current_state'=>Yii::t('HRModule.CompanyLicense', 'CompanyLicenseDecisionAutomaticallyFill'),
            'check_disagreements_with_curr_state'=>Yii::t('HRModule.CompanyLicense', 'CompanyLicenseDecisionCheckDisagreementsWithCurrState')
        )+parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return Yii::t('HRModule.CompanyLicense', 'CompanyLicensesDecision');
    }    
    
    public function modelViews() 
    {
        return [
            'company_licenses', 'inFile'
        ];
    }
    
    public function columnDisplays($column)
    {
        $owner = $this->owner;
        switch ($column)
        {
            case 'check_disagreements_with_curr_state':
                return Yii::app()->controller->widget('SIMAButton',[
                    'action'=>[                
                        'title'=>Yii::t('HRModule.CompanyLicense', 'CompanyLicenseDecisionCheckDisagreements'),
                        'tooltip'=>Yii::t('HRModule.CompanyLicense', 'CompanyLicenseDecisionCheckDisagreements'),
                        'onclick'=>['sima.hr.companyLicenseDecisionToCompanyLicensesDisagreements','$(this)',"$owner->id"]
                    ]
                ], true);
                
            default : return parent::columnDisplays($column);
        }
    }
    
    public function modelForms()
    {
        $owner = $this->owner;
        $columns = [
            'decision_date'=>'datetimeField',
            'company_licenses_list'=>array('list',
                'model'=>'CompanyLicense',
                'relName'=>'company_licenses',                         
                'multiselect'=>true
            )
        ];
        if ($owner->isNewRecord)
        {
            $columns['automatically_fill_with_the_current_state'] = 'checkBox';
        }
        return array(
            'default'=>array(
                'type'=>'generic',
                'columns'=>$columns
            )
        );
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default: return [
                'columns' => array('file', 'decision_date')
            ];
        }

    }
}
