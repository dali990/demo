<?php

class AnnualLeaveDecisionToAnnualLeaveGUI extends SIMAActiveRecordGUI
{
    public function columnLabels()
    {
        return array(
            'annual_leave_decision_id' => 'Rešenje o godišnjem odmoru',
            'annual_leave_decision' => 'Rešenje o godišnjem odmoru',
            'annual_leave_id' => 'Godišnji odmor',
            'annual_leave' => 'Godišnji odmor',
            'days_number' => 'Broj dana'
        ) + parent::columnLabels();
    }

    public function modelLabel($plural = false)
    {
        return 'Broj dana koji se skida sa rešenja za godišnji odmor';
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default: return array(
                'columns'=>array(
                    'annual_leave_decision', 'annual_leave', 'days_number'
                )                
            );
        }
    }
    
    public function modelForms() {
        return array(
            'default' => array(
                'type' => 'generic',
                'columns' => array(
                    'annual_leave_decision_id'=>array('relation','relName'=>'annual_leave_decision'),
                    'annual_leave_id'=>array('relation','relName'=>'annual_leave'),
                )
            )
        );
    }

}

?>


