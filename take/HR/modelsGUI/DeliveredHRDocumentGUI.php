<?php

class DeliveredHRDocumentGUI extends SIMAActiveRecordGUI
{

    public function columnLabels()
    {
        return array(
            'email_id' => 'Email',
            'email' => 'Email',
            'link' => 'Link',
            'delivery_type' => 'Način dostave',
            'sent' => 'Poslato',
            'sender_id' => 'Poslao',
            'sender' => 'Poslao',
            'sent_timestamp' => 'Vreme slanja',
            'received' => 'Primljeno',
            'recipient_id' => 'Primio',
            'recipient' => 'Primio',
            'received_timestamp' => 'Vreme primanja',
            'file' => 'Dokument',
            'file_id' => 'Dokument',
            'delivered_status' => 'Poslednje stanje'
        ) + parent::columnLabels();
    }

    public function modelLabel($plural = false)
    {
        return 'Poslat hr dokument';
    }
    
    public function columnDisplays($column)
    {
        $owner = $this->owner;
        switch ($column) 
        {
            case 'delivery_type':
                return Employee::$document_send_types[$owner->$column].SIMAHtml::modelFormOpen($owner);
            default: return parent::columnDisplays($column);
        }
        
    }
    
    public function modelForms()
    {
        return [
            'default' => [
                'type' => 'generic',
                'columns' => [
                    'delivery_type' => 'dropdown'
                ]
            ]
        ];
    }

    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default: return array(
                'columns' => array(
                    'file.name', 'delivery_type', 'sender', 'sent_timestamp', 'recipient', 'received_timestamp', 'email', 'link'
                )
            );
        }
    }
    
    public function droplist($key, $relName = '', $filter = null)
    {
        switch ($key) 
        {
            case 'delivery_type':   return Employee::$document_send_types;
            default:                return parent::droplist($key, $relName, $filter);
        }
        
    }
}
