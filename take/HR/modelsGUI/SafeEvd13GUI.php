<?php

class SafeEvd13GUI extends SIMAActiveRecordGUI
{   
    public function columnLabels()
    {
        return array(
            'id' => 'Redni broj',
            'work_position_id' => 'Radno mesto',
            'work_position' => 'Radno mesto',            
            'person' => 'Osoba',
            'person_id' => 'Osoba',
            'report_code' => 'Broj lekarskog izveštaja',
            'report_date' => 'Datum pregleda',
            'next_report_date' => 'Datum sledećeg pregleda',
            'medical_referral_id' => 'Lekarski uput',
            'medical_referral' => 'Lekarski uput',
            'generate_medical_referral' => 'Generiši lekarski uput',            
            'actions' => 'Preduzete mere',
            'description' => 'Opis',
            'file' => 'Dokument',
            'file_id' => 'Dokument'            
        ) + parent::columnLabels();
    }

    public function modelLabel($plural = false)
    {
        return Yii::t('HRModule.SafeEvd2', $plural ? 'MedicalReports' : 'MedicalReport');
    }

    public function columnDisplays($column)
    {
        $owner = $this->owner;
        switch ($column)
        {
            case 'person': 
                $html = $owner->person->DisplayHtml;
                if (!isset($owner->person->employee->active_work_contract))
                {
                    $html .= ' (ne radi)';
                }
                return $html;
            case 'generate_medical_referral':
                $button = '';
                $action = [
                    'title'=>'Generiši',
                    'tooltip'=>'Generiši',
                    'onclick'=>[
                        'sima.hr.generateMedicalReferralForSafeEvd13',$owner->id,[
                            'referral_type'=>MedicalExaminationReferral::$SAFE_EVD_13,
                            'filter_scopes'=>'noConnectionToSafeEvd13'
                        ],$owner->medical_referral_id
                    ]
                ];
                if(SIMAMisc::isVueComponentEnabled())
                {
                    $button = Yii::app()->controller->widget(SIMAButtonVue::class, $action, true);
                }
                else
                {
                    $button = Yii::app()->controller->widget(SIMAButton::class, [
                        'action' => $action
                    ], true);
                }
                if (isset($owner->medical_referral))
                {
                    $button .= SIMAHtml::fileDownload($owner->medical_referral).$owner->medical_referral->DisplayHtml;
                }
                return $button;
            default: return parent::columnDisplays($column);
        }
    }

    public function modelViews()
    {
        return array(
        ) + parent::modelViews();
    }

    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default: return array(
                    'columns' => [
                        'person', 'work_position', 'generate_medical_referral', 'report_code', 'report_date', 'next_report_date', 'actions', 'description'
                    ],
                    'order_by' => ['report_date','next_report_date']
                );
        }
    }

    public function modelForms()
    {
        return array(
            'default' => array(
                'type' => 'generic',
                'columns' => array(
                    'person_id' => array('searchField', 'relName' => 'person'),
                    'work_position_id' => array('searchField', 'relName' => 'work_position'),
                    'report_code' => 'textField',
                    'report_date' => 'datetimeField',
                    'next_report_date' => 'datetimeField', 
                    'medical_referral_id' => [
                        'relation', 
                        'relName' => 'medical_referral',
                        'filters' => [
                            'referral_type'=>MedicalExaminationReferral::$SAFE_EVD_13,
                            'filter_scopes'=>'noConnectionToSafeEvd13'
                        ],
                        'dependent_on' => [
                            'person_id' => [
                                'onValue' => [
                                    'enable',
                                    ['condition', 'model_filter' => 'person.ids']
                                ],
                                'onEmpty' => 'disable'
                            ]
                        ]
                    ],
                    'actions' => 'textField',
                    'description' => 'textArea',
                )
            ),
        );
    }
}
