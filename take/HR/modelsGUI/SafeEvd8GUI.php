<?php

class SafeEvd8GUI extends SIMAActiveRecordGUI
{    
    public function columnLabels()
    {
        return array(
            'id' => 'Redni broj',
            'data' => 'Podaci',
            'num_expert_report' => 'Broj stručnog nalaza',
            'date_control' => 'Datum ispitivanja',
            'date_next_control' => 'Datum sledećeg ispitivanja',
            'comment' => 'Napomena',
            'description' => 'Opis',
            'file' => 'Dokument'
        ) + parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return SafeRecordController::$SAFE_RECORDS[8]['name'];
    }    

    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default: return array(
                    'columns' => array(
                        'file','data', 'num_expert_report', 'date_control', 'date_next_control', 'comment', 'description'
                    )
                );
        }
    }

    public function modelForms()
    {
        return array(
            'default' => array(
                'type' => 'generic',
                'columns' => array(
                    'data' => 'textField',
                    'num_expert_report' => 'textField',
                    'date_control' => 'datetimeField',
                    'date_next_control' => 'datetimeField',
                    'comment' => 'textArea',
                    'description' => 'textArea'
                )
            ),
        );
    }
}
