<?php

class NationalEventReligiousHolidayToDayGUI extends SIMAActiveRecordGUI
{
    public function modelLabel($plural = false)
    {
        return Yii::t('HRModule.NationalEvent', $plural?'ReligiousHolidays':'ReligiousHoliday');
    }
}

