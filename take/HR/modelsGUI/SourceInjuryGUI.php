<?php

class SourceInjuryGUI extends SIMAActiveRecordGUI
{

    public function columnLabels()
    {
        return array(
            'name' => Yii::t('HRModule.SourceInjury', 'Meaning'),
            'code' => Yii::t('HRModule.SourceInjury', 'Code'),
            'description' => Yii::t('HRModule.SourceInjury', 'Note'),
            'parent_id' => Yii::t('HRModule.SourceInjury', 'Type'),
            'parent' => Yii::t('HRModule.SourceInjury', 'Type'),
            'parent.name' => Yii::t('HRModule.SourceInjury', 'Type')
        ) + parent::columnLabels();
    }

    public function modelLabel($plural = false)
    {
        return Yii::t('HRModule.SourceInjury', $plural ? 'SourceInjuries' : 'SourceInjury');
    }

    public function modelViews()
    {
        return array(
                ) + parent::modelViews();
    }

    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default: return [
                    'columns' => [
                        'name', 'code', 'description', 'parent.name'
                    ],
                    'order_by'=>['code']
                ];
        }
    }

    public function modelForms()
    {
        return array(
            'default' => array(
                'type' => 'generic',
                'columns' => array(
                    'name' => 'textField',
                    'code' => 'textField',
                    'description' => 'textArea',
                    'parent_id'=>['relation', 'relName' => 'parent', 'add_button' => true],
                )
            )
        );
    }

}
