<?php

class CompanyLicenseToPartnerGUI extends SIMAActiveRecordGUI
{

    public function columnLabels()
    {
        return array(
            'company_license' => 'Velika licenca',
            'company_license_id' => 'Velika licenca',
            'partner' => 'Partner',
            'partner_id' => 'Partner',
            'accepted_references_number' => 'Broj prihvaćenih referenci'
        ) + parent::columnLabels();
    }

    public function modelLabel($plural = false)
    {
        return 'Velika licenca partnera';
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            case 'notCollectMinIksPoints': return [
                'columns'=>[
                    'partner','company_license'
                ]
            ];
            default: return array(
                'columns'=>array(
                    'company_license.company_license'
                ),
            );
        }
    }
    
    public function modelForms()
    {
        return array(
            'onlyReferencesNumber'=>array(
                'type'=>'generic',
                'columns'=>array(
                    'accepted_references_number' => 'textField'
                )
            )
        );
    }
}

?>
