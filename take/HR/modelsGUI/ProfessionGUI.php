<?php

class ProfessionGUI extends SIMAActiveRecordGUI
{
    public function columnLabels() {
        return array(
            'name' => 'Naziv'
        ) + parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return 'Zanimanje';
    }
    
    public function modelForms() 
    {
        return array(
            'default' => array(
                'type' => 'generic',
                'columns' => array(                    
                    'name' => 'textField'
                )
            )
        );
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default: return array(
                'columns' => array(
                    'name'
                )
            );
        }
    }
}
