<?php

class WorkPositionCompetitionGUI extends SIMAActiveRecordDisplayGUI
{

    public function columnLabels()
    {
        return array(
            'name' => 'Pozicija',
            'description' => 'Tekst',
            'active' => 'Aktivan',
            'apply_date' => 'Rok za prijavu'
        )+parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return 'Konkurs';
    }

//    public function modelViews()
//    {
//        return array(
//            'basicInfoTitle',
//            'basicInfo',
//            'current_materials_status'=>array('params_function'=>'viewParamsCurrentMaterialsStatus')
////            'leaf'=>array(
//////                'style'=>'border: 1px solid;'
////                ),
////            'default_edit'
////            'tabTemplates'=>array('with'=>array('templated_file'),'style'=>'border: 1px solid;')  //.creator
//        );
//    }
    
   
    
    public function modelForms()
    {
        return array(
            'default'=>array(
                'type'=>'generic',
                'columns'=>array(
                    'active' => 'checkBox',
                    'name'=>'textField',
                    'apply_date' => ['datetimeField', 'mode' => 'date'],
//                    'id'=>array('searchField','relName'=>'company'),
//                    'partner_id'=>array('searchField','relName'=>'partner'),
                    'description'=>'textArea'
                )
            )
        );
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {

            default: return array(
                'columns' => array(
                    'name',
                    'apply_date',
                    'description'
                )// 'title', 'description'
            );
        }
    }
    
}