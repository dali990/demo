<?php

/**
 * Description of WorkPositionGUI
 *
 * @author goran
 */
class WorkPositionPaymentGUI extends SIMAActiveRecordGUI
{

    public function columnLabels()
    {
        return array(
            'name' => 'Naziv',
            'amount' => 'Iznos',
            'description' => 'Opis',
        ) + parent::columnLabels();
    }

    public function modelLabel($plural = false)
    {
        return 'Platni razredi';
    }

    public function modelForms($conf = array())
    {
        return array(
            'default' => array(
                'type' => 'generic',
                'columns' => array(                    
                    'name' => 'textField',
                    'amount' => 'numberField',
                    'description' => 'textArea'
                )
            )
        );
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default:
                return array(
                'columns' => array(
                    'name','amount','description'
                ),
            );
        }
    }
}

?>
