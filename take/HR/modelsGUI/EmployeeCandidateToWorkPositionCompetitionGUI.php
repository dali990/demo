<?php

class EmployeeCandidateToWorkPositionCompetitionGUI extends SIMAActiveRecordGUI
{
    public function columnLabels()
    {
        return array(
            'employee_candidate_id' => 'Kandidat',
            'employee_candidate' => 'Kandidat',
            'status_id' => 'Status',
            'status' => 'Status',
            'interview_time' => 'Zakazan intervju',
            'avarage_cv_grade' => 'Prosecna ocena za CV',
            'cv_text_grades' => 'Komentari za CV',
            'avarage_interview_grade' => 'Prosecna ocena sa intervjua',
            'interview_text_grades' => 'Komentari sa intervjua',       
            'cv_grades' => 'Ocene CV',       
            'interview_grades' => 'Ocene intervju',
            'other_competitions' => 'Prethodni konkursi'
        ) + parent::columnLabels();
    }

    public function modelLabel($plural = false)
    {
        return 'Kandidat konkursa';
    }
    
    public function guiTableSettings($type)
    {
        $grade_columns = [
            'cv_grades',
            'avarage_cv_grade',
            'interview_grades',
            'avarage_interview_grade',
        ];
        switch ($type)
        {
            default: return [
                'columns'=>  array_merge(array(
                    'status', 
                    'employee_candidate', 
                    'employee_candidate.person.birthdate', //'status_id', 'interview_time'
                    'employee_candidate.person.addresses', //'status_id', 'interview_time'
                    'employee_candidate.school_course', //'status_id', 'interview_time'
                    'other_competitions',
                    'interview_time'
                ),$grade_columns  )   ,
                'order_by' => 'interview_time'
            ];
        }
    }
    
    private function gradeDisplay($num_grade_column,$text_grade_column)
    {
        $owner = $this->owner;
        $see_all_grades = $owner->see_all_grades();
        $ret = "<table>";
        foreach ($owner->competition_grades as $grade)
        {
            if ( $see_all_grades
                    || $grade->work_position_competition_juries->jury_id == Yii::app()->user->id
                )
            {
                $ret .='<tr><td>';
                $ret .= $grade->work_position_competition_juries->jury->DisplayName;
                $ret .= '</td><td>';
                $ret .= SIMAHtml::number_format($grade->$num_grade_column);
                $ret .= ' - '.$grade->$text_grade_column;
                $ret .='</td></tr>';
            }                    
        }
        
        $ret .= "</table>";
        
        return $ret;
    }
    
    public function columnDisplays($column)
    {
        $owner = $this->owner;
        switch($column)
        {
            case 'status_id' : return $this->droplist('status_id')[$owner->$column];
            case 'avarage_cv_grade': case 'avarage_interview_grade':
                $owner = $this->owner;
                $see_all_grades = $owner->see_all_grades();
                if ($see_all_grades)
                {
                    return SIMAHtml::number_format($owner->$column);
                }
                else
                {
                    return '-';
                }
            case 'cv_text_grades': case 'interview_text_grades':
                $text_grades = [];
                foreach ($owner->competition_grades as $grade)
                {
                    $text_grades[] = $grade->work_position_competition_juries->jury->DisplayName
                            .': '.$grade->cv_text_grade;
                }
                return '<p>'.implode(' </p><p> ', $text_grades).'</p>';
            case 'cv_grades':
                return $this->gradeDisplay('cv_grade','cv_text_grade');
            case 'interview_grades':
                return $this->gradeDisplay('interview_grade','interview_text_grade');
            case 'other_competitions':
                $ret = [];
                foreach ($owner->employee_candidate->work_position_competitions as $competition)
                {
                    if ($competition->id != $owner->work_position_competition_id)
                    {
                        $jury = WorkPositionCompetitionJury::model()->findByAttributes([
                            'work_position_competition_id' => $competition->id,
                            'jury_id' => Yii::app()->user->id
                        ]);
                        $was_jury = isset($jury); 

                        $avg_grade_model = EmployeeCandidateToWorkPositionCompetition::model()->
                            findByAttributes([
                                'work_position_competition_id' => $competition->id,
                                'employee_candidate_id' => $owner->employee_candidate_id
                            ]);
                        $grade_text = '-|-';
                        if ($was_jury)
                        {
                            $grade_model = CompetitionGrade::model()->findByAttributes([
                                'employee_candidate_to_work_position_competition_id' => $avg_grade_model->id,
                                'work_position_competition_juries_id' => $jury->id
                            ]);
                            if (isset($grade_model))
                            {
                                $grade_text = $grade_model->cv_grade.'|'.$grade_model->interview_grade;
                            }
                        }
                        $see_all_grades = $owner->see_all_grades();
                        if ($see_all_grades)
                        {
                            $grade_text .= '('.SIMAHtml::number_format($avg_grade_model->avarage_cv_grade)
                                    .'|'
                                    .SIMAHtml::number_format($avg_grade_model->avarage_interview_grade).')';
                        }
                        $ret[] = $competition->DisplayName.'|'.$grade_text;
                    }
                }
                return implode('<br />', $ret);
            case 'status':
                return EmployeeCandidateToWorkPositionCompetition::model()->getStatusData()[$owner->status];
            default: return parent::columnDisplays($column);
        }
        
    }
    
    public function droplist($key, $relName='', $filter=null)
    {
        switch ($key)
        {
            case 'status_id': return array(
                '1' => 'Prijavio se',
                '2' => 'Nije pozvan',
                '3' => 'Pozvati',
                '4' => 'Pozvan',
                '5' => 'Nije dosao',
                '6' => 'Odradjen intervju',
                '7' => 'Nije prosao',
                '8' => 'Ponudjen posao',
                '9' => 'Nije prihvatio posao',
                '10' => 'Prihvatio posao',
            );
            case 'status': 
                return EmployeeCandidateToWorkPositionCompetition::model()->getStatusData();  
            default: return parent::droplist($key, $relName, $filter);
        }
    }
    
    public function modelForms() {
        return array(
            'default' => array(
                'type' => 'generic',
                'columns' => array(
                    'employee_candidate_id'=>array('relation','relName'=>'employee_candidate','disabled'=>'disabled'),
//                    'status_id'=>array('dropdown'),
                    'status'=>array('dropdown'),
                    'interview_time' => ['datetimeField','mode'=>'datetime']
                )
            )
        );
    }

}

?>


