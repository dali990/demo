<?php
/**
 * Description of IKSPointToWorkLicenseGUI
 *
 * @author sasa
 */
class IKSPointToWorkLicenseGUI extends SIMAActiveRecordGUI
{
    public function columnLabels() 
    {
        return array(
            'work_license_name'=>'Naziv',
            'work_license_id'=>'Licenca',
        ) + parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return 'Licence za IKS poene';
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default: return array(
                'columns'=>array(
                    'work_license_name'
                )
            );
        }
    }
    
    public function modelForms()
    {
        return array(
            'default'=>array(
                'type'=>'generic',
                'columns'=>array(
                    'work_license_id'=>array('searchField','relName'=>'work_license'),
                    'iks_point_id'=>array('searchField','relName'=>'iks_point'),
                )
              ),
            );
    }
}

?>
