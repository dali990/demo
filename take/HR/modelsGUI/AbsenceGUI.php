<?php

class AbsenceGUI extends SIMAActiveRecordDisplayGUI
{
    public function columnLabels() {
        return array(
            'id'=>'Redni broj',
            'employee_id'=>'Zaposleni',
            'employee'=>'Zaposleni',
            'begin'=>'Prvi dan',
            'number_work_days'=>'Broj radnih dana',
            'end'=>'Poslednji dan',
            'confirmed' => Yii::t('HRModule.Absence', 'Confirmed'),
            'confirmed_timestamp'=>'Vreme odobravanja',
            'confirmed_user_id'=>'Odobrio',
            'work_hours'=>'Broj radnih sati',
            'number_work_days'=>'Broj dana',
            'year' => 'Godina',
            'type'=>'Tip odsustva',
            'subtype' => Yii::t('HRModule.Absence', 'Subtype'),
            'description'=>'Opis',
            'year_id' => 'Godina',
            'set_document'=>'Postavite/Generišite dokument',
            'absent_on_date' => Yii::t('HRModule.Absence', 'AbsentOnDate'),
            'approval_documents' => Yii::t('HRModule.Absence','ApprovalDocuments'),
        )+parent::columnLabels();
    }
    
    public function modelLabel($plural=false)
    {
        return Yii::t('HRModule.Absence',$plural?'Absences':'Absence');
    }
    
    public function columnDisplays($column)
    {
        $owner = $this->owner;
        $real_model = $owner->real_model;
        $statuses = $owner->modelSettings('statuses');
        if (isset($statuses[$column]))
        {
            return SIMAHtml::status($real_model, $column);
        }
        switch ($column)
        {
            case 'approval_documents':
                if (isset($owner->annual_leave))
                {
                    return $owner->annual_leave->getAttributeDisplay('approval_documents');
                }
                else
                {
                    if (isset($owner->document))
                    {
                        return $owner->document->DisplayHTML;
                    }
                    else 
                    {
                        return Yii::t('HRModule.Absence','DocumentNotSet');
                    }
                }
            case 'form_custom': 
                return SIMAHtmlButtons::modelFormOpenWidgetAction($owner->real_model);
            case 'delete_custom': 
                return SIMAHtmlButtons::modelDeleteWidgetAction($owner->real_model);
            case 'type':
                switch ($owner->type)
                {
                    case Absence::$ANNUAL_LEAVE: return 'Godišnji odmor';
                    case Absence::$SICK_LEAVE: return 'Bolovanje';
                    case Absence::$FREE_DAYS: return 'Plaćeno odsustvo';
                    default: return 'nije odredjen'.$owner->type;
                }
            case 'set_document': 
                switch ($owner->type)
                {
                    case Absence::$ANNUAL_LEAVE: return $owner->generateAnnualLeaveDocument();
                    case Absence::$SICK_LEAVE: return $owner->sick_leave->addSickLeaveDocument();
                    case Absence::$FREE_DAYS: return $owner->generateFreeDaysDocument();
                    default: return 'nije odredjen'.$owner->type;
                }
            case 'subtype': return isset($owner->getSubTypeData()[$owner->subtype])?$owner->getSubTypeData()[$owner->subtype]:'';
            case 'statuses':
            {
                $result = '';
                
                $statuses_keys = array_keys($statuses);
                foreach ($statuses_keys as $status_key)
                {
                    $result .= SIMAHtml::status($real_model, $status_key);
                }

                return $result;
            }
            default : return parent::columnDisplays($column);
        }
    }
    
    public function generateAnnualLeaveDocument()
    {
        $owner = $this->owner;
        if (Yii::app()->user->checkAccess('GenerateDocument',[], $owner))
        {
            return SIMAHtml::fileDownload($owner->document).
                "<button class='sima-ui-button' onclick='sima.hr.generateAbsenceTemplates(".$owner->id.",1);'>Generiši</button>";
        }
        else
        {
            return 'Nemate privilegiju';
        }
    }
    
    public function generateFreeDaysDocument()
    {
        $owner = $this->owner;        
        if (Yii::app()->user->checkAccess('GenerateDocument',[], $owner))
        {
            return SIMAHtml::fileDownload($owner->document).
                    "<button class='sima-ui-button' onclick='sima.hr.generateAbsenceTemplates(".$owner->id.",2);'>Generiši</button>";
        }
        else
        {
            return 'Nemate privilegiju';
        }
    }
    
    
    
    public function modelViews() {
        return array_merge(parent::modelViews(),[
            'title' => ['origin' => 'Absence'],
            'full_info' => ['origin' => 'Absence'],
            'basic_info' => ['origin' => 'Absence']
         ]);
    }
    
    public function modelForms()
    {
        $can_change_type = Yii::app()->user->checkAccess('ChangeType',[],$this->owner);
        $employee_id = ['relation', 'relName'=>'employee'];
        if(isset($this->owner->employee_id))
        {
            $employee_id['disabled'] = 'disabled';
        }
        $columns = array(            
            'id'=>'hidden',
            'document_id'=>'hidden',
            'employee_id'=>$employee_id,
            'begin'=>'datetimeField',
            'end'=>'datetimeField',
            'type'=>array('dropdown', 'empty'=>'Izaberi', 'disabled'=>(!$can_change_type)),
            'year_id'=>array('dropdown', 'empty'=>'Izaberi godinu'),
            'number_work_days'=>'textField',
            'description'=>'textArea'
        );
        if(!$this->owner->isNewRecord && $this->owner->canConfirmAbsence() === true) //jer moze da vrati niz
        {
            $columns['confirmed'] = 'status';            
        }
        return array(
            'default'=>array(
                'type'=>'generic',
                'columns'=>$columns
            )
        );
    }
    
    public function droplist($key, $relName='', $filter=null) {
        switch ($key) {
            case 'type': return array(Absence::$ANNUAL_LEAVE => 'Godišnji odmor', Absence::$SICK_LEAVE => 'Bolovanje', Absence::$FREE_DAYS=>'Plaćeno odsustvo');
            case 'subtype':
                $type_displays = $this->owner->getSubTypeData();
                return $type_displays;
            default: return parent::droplist($key, $relName, $filter);
        }
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            case 'from_user': return [
                'columns' => array('type', 'begin', 'end', 'number_work_days', 'year', 'set_document', 'description'),
                'order_by' => ['begin','end','number_work_days','type'],
                'sums'=>['number_work_days']
            ];
            case 'from_all_absences': return [
                'columns' => ['employee', 'type', 'begin', 'end', 'number_work_days', 'year', 'set_document', 'description', 'approval_documents'],
                'order_by' => ['begin','end','number_work_days','type'],
                'sums'=>['number_work_days']
            ];
            default: return [
                'columns' => array(
                    'employee', 'type', 'begin', 'end', 'number_work_days', 'year', 'set_document', 'description',
                    'absent_on_date','approval_documents'
                ),
                'order_by' => ['begin','end','number_work_days','type'],
                'sums'=>['number_work_days']
            ];
        }
    }
}
