<?php

/**
 * Description of SafeEvd3GUI
 *
 * @author goran
 */
class SafeEvd3GUI extends SIMAActiveRecordGUI
{

    public function columnLabels()
    {
        return array(
            'id' => Yii::t('HRModule.SafeEvd3','Id'),
            'work_position_id' => Yii::t('HRModule.SafeEvd3','WorkPosition'),
            'work_position' => Yii::t('HRModule.SafeEvd3','WorkPosition'),
            'employee_id' => Yii::t('HRModule.SafeEvd3','Employee'),
            'employee' => Yii::t('HRModule.SafeEvd3','Employee'),
            'date' => Yii::t('HRModule.SafeEvd3','WhenTheInjuriesOccurred'),
            'mark_injury' => Yii::t('HRModule.SafeEvd3','MarkInjury'),
            'source_injury' => Yii::t('HRModule.SafeEvd3','SourceInjury'),
            'source_injury_id' => Yii::t('HRModule.SafeEvd3','SourceInjury'),
            'mode_injury' => Yii::t('HRModule.SafeEvd3','ModeInjury'),
            'mode_injury_id' => Yii::t('HRModule.SafeEvd3','ModeInjury'),
            'description' => Yii::t('HRModule.SafeEvd3','Note'),
            'citizenship' => Yii::t('HRModule.SafeEvd3','Citizenship'),
            'employment_status' => Yii::t('HRModule.SafeEvd3','EmploymentStatus'),
            'safe_evd_3_work_contracts' => Yii::t('HRModule.SafeEvd3','WorkExperience'),
            'safe_evd_3_work_contracts_list' => Yii::t('HRModule.SafeEvd3','WorkExperience'),
            'beginning_working_hour' => Yii::t('HRModule.SafeEvd3','BeginningWorkingHour'),
            'injury_location_address_id' => Yii::t('HRModule.SafeEvd3','AddressStreetAndNumber'),
            'injury_location_address' => Yii::t('HRModule.SafeEvd3','AddressStreetAndNumber'),
            'municipality' => Yii::t('HRModule.SafeEvd3','Municipality'),
            'settlement' => Yii::t('HRModule.SafeEvd3','Settlement'),
            'country' => Yii::t('HRModule.SafeEvd3','Country'),
            'commuting_accidents' => Yii::t('HRModule.SafeEvd3','CommutingAccidents'),
            'work_environment' => Yii::t('HRModule.SafeEvd3','WorkEnvironment'),
            'work_process' => Yii::t('HRModule.SafeEvd3','WorkProcess'),
            'specific_physical_activity' => Yii::t('HRModule.SafeEvd3','SpecificPhysicalActivity'),
            'contact_injury' => Yii::t('HRModule.SafeEvd3','ContactInjury'),
            'increased_risk' => Yii::t('HRModule.SafeEvd3','InjuryAtWorkIncreasedRiskOrNot'),
            'boss_of_injured' => Yii::t('HRModule.SafeEvd3','NameAndSurname'),
            'boss_of_injured_id' => Yii::t('HRModule.SafeEvd3','NameAndSurname'),
            'eyewitness' => Yii::t('HRModule.SafeEvd3','NameAndSurname'),
            'eyewitness_id' => Yii::t('HRModule.SafeEvd3','NameAndSurname'),
            'eyewitness_address' => Yii::t('HRModule.SafeEvd3','ResidenceAddress'),
            'form_filling_date' => Yii::t('HRModule.SafeEvd3','FormFillingDate'),
            'form_filling_settlement' => Yii::t('HRModule.SafeEvd3','Settlement'),
            'form_filling_settlement_id' => Yii::t('HRModule.SafeEvd3','Settlement'),
            'hospital' => Yii::t('HRModule.SafeEvd3','HospitalAddressWhereInjuredChecked'),
            'hospital_id' => Yii::t('HRModule.SafeEvd3','HospitalAddressWhereInjuredChecked'),
            'injury_diagnosis' => Yii::t('HRModule.SafeEvd3','DiagnosisOfInjury'),
            'external_cause_of_injury' => Yii::t('HRModule.SafeEvd3','ExternalCauseOfInjury'),
            'type_injury' => Yii::t('HRModule.SafeEvd3','TypeOfInjury'),
            'injury_part_of_body' => Yii::t('HRModule.SafeEvd3','InjuredPartOfBody'),
            'doctor_remarks' => Yii::t('HRModule.SafeEvd3','DoctorRemarks'),
            'mt3_days_unable_work' => Yii::t('HRModule.SafeEvd3','MT3DaysUnableWork'),
            'estimated_lost_days' => Yii::t('HRModule.SafeEvd3','EstimatedLostDays'),
            'doctor_examined_date' => Yii::t('HRModule.SafeEvd3','Date'),
            'doctor_examined_settlement' => Yii::t('HRModule.SafeEvd3','Settlement'),
            'doctor_examined_settlement_id' => Yii::t('HRModule.SafeEvd3','Settlement'),
            'date_of_received_report' => Yii::t('HRModule.SafeEvd3','DateOfReceivedReport'),
            'violation_date' => Yii::t('HRModule.SafeEvd3','ViolationDate'),
            'insurance_settlement' => Yii::t('HRModule.SafeEvd3','Settlement'),
            'insurance_settlement_id' => Yii::t('HRModule.SafeEvd3','Settlement'),
            'display_evd_number' => Yii::t('HRModule.SafeEvd3','EvdNumber'),
            'unique_injury_number' => Yii::t('HRModule.SafeEvd3','UniqueNumberOfInjuriesAtWork'),
        ) + parent::columnLabels();
    }

    public function modelLabel($plural = false)
    {
        return SafeRecordController::$SAFE_RECORDS[3]['name'];
    }

    public function modelViews()
    {
        return [
            'inFile',
            'full_info',
            'full_info_view' => ['with'=>array('employee')]
        ] + parent::modelViews();
    }

    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default: return array(
                    'columns' => array(
                        'employee', 'date', 'work_position', 'type_injury', 'mark_injury', 'source_injury', 'mode_injury', 'description', 'display_evd_number'
                    )
                );
        }
    }

    public function modelForms()
    {
        $disabled = $this->owner->confirmed;
        $default_forms = [
            'general',
            'safe_evd_3_report' => ['title' => Yii::t('HRModule.SafeEvd3','ReportOnInjuryAtWork')],
            'general2',
            'doctor_and_health_insurance'
        ];
        $safe_evd_3_report_forms = [
            'injured_worker' => ['title' => Yii::t('HRModule.SafeEvd3','InjuredPersonInformation'), 'disabled' => true],
            'injury_at_work' => ['title' => Yii::t('HRModule.SafeEvd3','InjuryInformationAtWork')],
            'info_boss_of_injured' => ['title' => Yii::t('HRModule.SafeEvd3','BossOfInjured')],
            'eyewitness_information' => ['title' => Yii::t('HRModule.SafeEvd3','EyewitnessInformation')]           
        ];
        $safe_evd_3_work_contracts_filter = [];
        if(!empty($this->owner->employee_id))
        {
            $safe_evd_3_work_contracts_filter = [
                'employee' => ['ids' => $this->owner->employee_id]
            ];
        }
        
        return array(
            'default' => [
                'type' => 'composite',
                'forms' => $default_forms                
            ],
            'doctor_and_health_insurance' => [
                'type' => 'composite',
                'forms' => [
                    'doctor' => ['title' => Yii::t('HRModule.SafeEvd3','AnalysisOfDoctor')],
                    'health_insurance_organization' => ['title' => Yii::t('HRModule.SafeEvd3','HealthInsuranceOrganization')],
                ],            
            ],
            'general' => [
                'type' => 'generic',
                'columns' => [
                    'employee_id' => ['searchField', 'relName' => 'employee', 'disabled' => $disabled],
                    'work_position_id' => ['searchField', 'relName' => 'work_position', 'disabled' => $disabled],
                    'date' => ['datetimeField', 'mode' => 'datetime', 'disabled' => $disabled],
                ],
                'withoutBorder' => true
            ],
            'safe_evd_3_report' => [
                'type' => 'composite',
                'forms' => $safe_evd_3_report_forms
            ],
            'injured_worker' => [
                'type' => 'generic',
                'columns' => [
                    'citizenship' => ['dropdown', 'disabled' => $disabled, 'empty'=>Yii::t('BaseModule.Common', 'Choose')],
                    'employment_status' => ['dropdown', 'disabled' => $disabled],
                    'safe_evd_3_work_contracts_list' => ['list',
                        'params' => [
                            'add_button' => false,
                            'columns_type' => 'with_employee'
                        ],
                        'model' => WorkContract::class,
                        'relName' => 'safe_evd_3_work_contracts',
                        'multiselect' => true,
                        'filter' => $safe_evd_3_work_contracts_filter,
                        'disabled' => $disabled
                    ],
                ],
                'withoutBorder' => true
            ],
            'injury_at_work' => [
                'type' => 'generic',
                'columns' => [
                    'beginning_working_hour' => ['dropdown', 'disabled' => $disabled, 'empty'=>Yii::t('BaseModule.Common', 'Choose')],
                    'injury_location_address_id' => ['relation', 'relName'=>'injury_location_address', 'disabled' => $disabled, 'add_button' => true],
                    'commuting_accidents' => ['dropdown', 'disabled' => $disabled],
                    'work_environment' => ['dropdown', 'disabled' => $disabled],
                    'work_process' => ['dropdown', 'disabled' => $disabled],
                    'specific_physical_activity' => ['dropdown', 'disabled' => $disabled],
                    'source_injury_id' => ['searchField', 'relName' => 'source_injury', 
                        'add_button'=>[
                            'params' => [
                                'custom_buttons' => [
                                    Yii::t('BaseModule.Misc','TakeDataFromCodeBook') => [
                                        'func' => 'sima.safeRecord.syncSourceInjuriesFromCodebook(this)',
                                    ],
                                ],
                            ]
                        ],
                        'disabled' => $disabled
                    ],
                    'contact_injury' => ['dropdown', 'disabled' => $disabled],
                    'mode_injury_id' => ['searchField', 'relName' => 'mode_injury', 
                        'add_button'=>[
                            'params' => [
                                'custom_buttons' => [
                                    Yii::t('BaseModule.Misc','TakeDataFromCodeBook') => [
                                        'func' => 'sima.safeRecord.syncModeInjuriesFromCodebook(this)',
                                    ],
                                ],
                            ]
                        ], 
                        'disabled' => $disabled
                    ],
                    'increased_risk' => ['dropdown', 'disabled' => $disabled, 'empty'=>Yii::t('BaseModule.Common', 'Choose')]
                ],
                'withoutBorder' => true
            ],
            'info_boss_of_injured' => [
               'type' => 'generic',
                'columns' => [
                    'boss_of_injured_id' => ['relation', 'relName'=>'boss_of_injured', 'disabled' => $disabled, 'add_button' => true]
                ],
                'withoutBorder' => true,
            ],
            'eyewitness_information' => [
                'type' => 'generic',
                'columns' => [
                    'eyewitness_id' => ['relation', 'relName'=>'eyewitness', 'disabled' => $disabled, 'add_button' => 'true'],
                ],
                'withoutBorder' => true,
            ],
            'general2' => [
                'type' => 'generic',
                'columns' => [
                    'form_filling_date' => ['datetimeField', 'mode' => 'date', 'disabled' => $disabled],
                    'form_filling_settlement_id' => ['relation', 'relName' => 'form_filling_settlement', 'disabled' => $disabled],
                    'description' => ['textArea', 'disabled' => $disabled]
                ],
                'withoutBorder' => true,
            ],
            'doctor' => [
                'type' => 'generic',
                'columns' => [
                    'hospital_id' => ['relation', 'relName'=>'hospital', 'disabled' => $disabled, 'add_button' => 'true'],
                    'injury_diagnosis' => ['textField', 'disabled' => $disabled],
                    'external_cause_of_injury' => ['textField', 'disabled' => $disabled],
                    'type_injury' => ['dropdown', 'disabled' => $disabled, 'empty'=>Yii::t('BaseModule.Common', 'Choose'), 'model_filter' => 'type_injuries_without_old_items'],
                    'injury_part_of_body' => ['dropdown', 'disabled' => $disabled, 'empty'=>Yii::t('BaseModule.Common', 'Choose')],
                    'mark_injury' => ['dropdown', 'disabled' => $disabled, 'empty'=>Yii::t('BaseModule.Common', 'Choose'), 'model_filter' => 'mark_injuries_without_old_items'],
                    'doctor_remarks' => ['textArea', 'disabled' => $disabled],
                    'mt3_days_unable_work' => ['dropdown', 'disabled' => $disabled, 'empty'=>Yii::t('BaseModule.Common', 'Choose')],
                    'estimated_lost_days' => ['dropdown', 'disabled' => $disabled, 'empty'=>Yii::t('BaseModule.Common', 'Choose')],
                    'doctor_examined_date' => ['datetimeField', 'mode' => 'date', 'disabled' => $disabled],
                    'doctor_examined_settlement_id' => ['relation', 'relName'=>'doctor_examined_settlement', 'disabled' => $disabled, 'add_button' => 'true'],                   
                ],
                'withoutBorder' => true
            ],
            'health_insurance_organization' => [
                'type' => 'generic',
                'columns' => [
                    'date_of_received_report' => ['datetimeField', 'mode' => 'date', 'disabled' => $disabled],
                    'violation_date' => ['datetimeField', 'mode' => 'date', 'disabled' => $disabled],
                    'insurance_settlement_id' => ['relation', 'relName'=>'insurance_settlement', 'disabled' => $disabled, 'add_button' => 'true'],
                    'unique_injury_number' => ['textField', 'disabled' => $disabled],
                ],
                'withoutBorder' => true,
            ]
        );
    }

    public function droplist($key, $relName='', $filter=null)
    {
        switch ($key)
        {
           case 'mark_injury':
                if ($filter === 'mark_injuries_without_old_items')
                {
                    return SafeEvd3Lists::markInjuriesWithoutOldItems();
                }
                return SafeEvd3Lists::$mark_injury_list;
            case 'citizenship': return SafeEvd3Lists::$citizenship_list;
            case 'employment_status': return SafeEvd3Lists::$employment_status_list;
            case 'beginning_working_hour': return SafeEvd3Lists::$beginning_working_hour_list;
            case 'commuting_accidents': return SafeEvd3Lists::$commuting_accidents_list;
            case 'work_environment': return SafeEvd3Lists::$work_environment_list;
            case 'work_process': return SafeEvd3Lists::$work_process_list;
            case 'specific_physical_activity': return SafeEvd3Lists::$specific_physical_activity_list;
            case 'contact_injury': return SafeEvd3Lists::$contact_injury_list;
            case 'increased_risk': return SafeEvd3Lists::$increased_risk_list;
            case 'type_injury':
                if ($filter === 'type_injuries_without_old_items')
                {
                    return SafeEvd3Lists::typeInjuriesWithoutOldItems();                   
                }
                return SafeEvd3Lists::$injury_type_list;
            case 'injury_part_of_body': return SafeEvd3Lists::$injury_part_of_body_list;
            case 'mt3_days_unable_work': return SafeEvd3Lists::$mt3_days_unable_work_list;
            case 'estimated_lost_days': return SafeEvd3Lists::$estimated_lost_days_list;
            default: return parent::droplist($key, $relName, $filter);
        }
    }
    
    public function columnDisplays($column)
    {
        $owner = $this->owner;
        switch ($column)
        {
            case 'mark_injury': return SafeEvd3Lists::$mark_injury_list[$owner->mark_injury] ?? '';
            case 'type_injury': return SafeEvd3Lists::$injury_type_list[$owner->type_injury] ?? '';
            default: return parent::columnDisplays($column);
        }
    }

}
