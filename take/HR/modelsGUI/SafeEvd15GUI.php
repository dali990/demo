<?php

class SafeEvd15GUI extends SIMAActiveRecordGUI
{

    public function columnLabels()
    {
        return array(            
            'name' => 'Naziv',
            'description' => 'Opis',
            'add_employees' => 'Zaposleni'
        ) + parent::columnLabels();
    }

    public function modelLabel($plural = false)
    {
        return SafeRecordController::$SAFE_RECORDS[15]['name'];
    }

    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default: return array(
                'columns' => array(
                    'name', 'description'
                )
            );
        }
    }

    public function modelForms()
    {
        return array(
            'default' => array(
                'type' => 'generic',
                'columns' => array(                    
                    'name' => 'textField',
                    'add_employees'=>array('list',
                        'model'=>'Employee',
                        'relName'=>'employees',
                        'multiselect'=>true
                    ),
                    'description' => 'textArea'
                )
            ),
        );
    }

}
