<?php

/**
 * Description of SafeEvd7GUI
 *
 * @author goran
 */
class SafeEvd7GUI extends SIMAActiveRecordGUI
{

    public function columnLabels()
    {
        return array(
            'id' => 'Redni broj',
            'work_position_id' => 'Radno mesto',
            'work_position' => 'Radno mesto',
            'name' => 'Naziv',
            'hemical_name' => 'Hemijski naziv',
            'un_number' => 'UN broj',
            'adr_number' => 'ADR broj',
            'rid_number' => 'RID broj',
            'class_material' => 'Klasa opasnosti opasne materije',
            'mode_use' => 'Način upotrebe',
            'quantity' => 'Dnevna količina',
            'comment' => 'Napomena',
            'description' => 'Opis'
                ) + parent::columnLabels();
    }

    public function modelLabel($plural = false)
    {
        return SafeRecordController::$SAFE_RECORDS[7]['name'];
    }

    public function modelViews()
    {
        return array() + parent::modelViews();
    }

    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default: return array(
                    'columns' => array(
                        'work_position', 'name', 'hemical_name', 'un_number', 'adr_number', 'rid_number', 'class_material', 'mode_use', 'quantity', 'comment', 'description'
                    )
                );
        }
    }

    public function modelForms()
    {
        return array(
            'default' => array(
                'type' => 'generic',
                'columns' => array(
                    'work_position_id' => array('searchField', 'relName' => 'work_position'),
                    'name' => 'textField',
                    'hemical_name' => 'textField',
                    'un_number' => 'textField',
                    'adr_number' => 'textField',
                    'rid_number' => 'textField',
                    'class_material' => 'textField',
                    'mode_use' => 'textField',
                    'quantity' => 'textField',
                    'comment' => 'textArea',
                    'description' => 'textArea'
                )
            ),
        );
    }

}
