<?php

/**
 * Klasa za definisanje statickih funkcija
 */

class SIMAMisc
{
    /**
     * Vraca niz model tagova za prosledjeni model ili niz modela u obliku <shema_name>-<table_name>_<id>
     * @param SIMAActiveRecord/array SIMAActiveRecord $models
     * @return array
     */
    public static function getModelTags($models)
    {
        $model_tags = [];
        if (!is_array($models))
        {
            $models = array($models);
        }
        
        foreach($models as $model)
        {
            if (is_object($model)) 
            {
                $model_tag = str_replace(".", "-", $model->tableName());

                $primaryKey = $model->getPrimaryKey();
                if ($primaryKey !== NULL)
                {
                    if (!is_array($primaryKey))
                    {
                        $model_tag .= '_'.$primaryKey;
                    }
                    else
                    {
                        foreach ($primaryKey as $val)
                        {
                            $model_tag .= '_'.$val;
                        }
                    }
                }
                else if (isset($model->id))
                {
                    error_log("Tabela {$model->tableName()} nema postavljenu vrednost za primarni kljuc, a ima postavljenu vrednost za kolonu id");
                }
                
                array_push($model_tags, $model_tag);
            }
        }
        
        return $model_tags;
    }
    
    /**
     * Vraca za niz modela niz vue model tagova
     * @param array $models
     * @return array
     */
    public static function getVueModelTags(array $models)
    {
        $vue_model_tags = [];
        
        foreach ($models as $model) 
        {
            array_push($vue_model_tags, SIMAMisc::getVueModelTag($model));
        }
        
        return $vue_model_tags;
    }
    
    /**
     * izracunavanje VueModelTag
     * @param SIMAActiveRecord $model - nije nullable
     * @return string
     */
    public static function getVueModelTag(SIMAActiveRecord $model)
    {
        //TODO: dodati koji je modul
        $tag = get_class($model);

        $primaryKey = $model->getPrimaryKey();
        if ($primaryKey !== NULL)
        {
            if (!is_array($primaryKey))
            {
                $tag .= '_'.$primaryKey;
            }
            else
            {
                foreach ($primaryKey as $val)
                {
                    $tag .= '_'.$val;
                }
            }
        }
        else if (isset($model->id))
        {
            error_log("Tabela {$model->tableName()} nema postavljenu vrednost za primarni kljuc, a ima postavljenu vrednost za kolonu id");
        }
        
        return $tag;
    }
    
    
    /**
     * prosledjeni niz stringova formatira i prikazuje kao gresku
     * @param array $errors
     * @throws SIMAWarnException
     */
    public static function parseAndDisplayErrorsStringArray(array $errors)
    {
        if(count($errors) > 0)
        {
            $new_line_delimiter = "</br>";
            $string_errors = implode($new_line_delimiter, $errors);
            throw new SIMAWarnException("Vracene sledece greske:".$new_line_delimiter.$string_errors);
        }
    }
    
    public static function includeModelAccesses($accesses)
    {
        $model_accesses = Yii::app()->params['modelsAccess'];        
        Yii::app()->params['modelsAccess'] = array_merge($model_accesses, $accesses);
    }
    
    public static function includeMainMenuAccesses($accesses)
    {
        $mainmenu_items = Yii::app()->params['mainMenuItems'];
        Yii::app()->params['mainMenuItems'] = array_merge_recursive($mainmenu_items, $accesses);
    }
    
    public static function includeOperations($accesses, $modul_name)
    {
        $operations = Yii::app()->params['operations'];
        $operations[$modul_name] = $accesses;        
        Yii::app()->params['operations'] = $operations;
    }
    
    public static function includeCheckOperations($accesses, $modul_name)
    {
        $checkOperations = Yii::app()->params['checkOperations'];
        $checkOperations[$modul_name] = $accesses;        
        Yii::app()->params['checkOperations'] = $checkOperations;
    }
    
    public static function includeApiFunctions(array $api_functions_array, $modul_name)
    {
        $apiFunctions = Yii::app()->params['apiFunctions'];
        foreach($api_functions_array as $afa)
        {
            $api_func = '';
            if(!empty($modul_name))
            {
                $api_func = $modul_name . '/';
            }
            $api_func .= $afa;
            
            $apiFunctions[] = $api_func;
        }
        Yii::app()->params['apiFunctions'] = $apiFunctions;
    }
    
    public static function includeConfigMapper($configMappings, $modul_name)
    {
        $configMapper = Yii::app()->params['configMapper'];
        
        foreach($configMappings as $key => $value)
        {
            $configMapper[$modul_name.'.'.$key] = $value;
        }
        
        Yii::app()->params['configMapper'] = $configMapper;
    }
    
    public static function numberFieldParams($model, $column)
    {
        $amount_field_thousands = Yii::app()->configManager->get('base.amount_field_thousands');
        $amount_field_decimals = Yii::app()->configManager->get('base.amount_field_decimals');
            
        $columns = $model->getMetaData()->tableSchema->columns;
        
        $scale = '2';
        $precision = null;
        if (isset($columns[$column]) && isset($columns[$column]->dbType))
        {
            $db_type = $columns[$column]->dbType;
            if (strpos($db_type, 'numeric') !== false)
            {
                $precision = $columns[$column]->precision;
                $scale = $columns[$column]->scale;
            }
            else if (strpos($db_type, 'integer') !== false)
            {
                $scale = 0;
            }
        }
        
        $thousend_separator = ($amount_field_thousands=='space')?' ':
                (($amount_field_decimals=='.')?',':'.');                       
        
        $value = (isset($model->$column) ? $model->$column : 0);
        if ($amount_field_decimals==',')
        { 
            $value = str_replace('.',',', $value);
        }
        
        $params = [
            'scale' => $scale,
            'precision' => $precision,
            'decimal' => $amount_field_decimals,
            'thousands' => $thousend_separator,
            'value' => $value,
            'minus' => ($value instanceof SIMABigNumber) ? $value->isLessThan(SIMABigNumber::fromInteger(0)) : floatval($value) < 0
        ];
        
        return $params;
    }
    
    public static function userToDbNumeric($value)
    {
        $amount_field_thousands = Yii::app()->configManager->get('base.amount_field_thousands');
        $amount_field_decimals = Yii::app()->configManager->get('base.amount_field_decimals');
        if ($amount_field_thousands == 'space')
        {
            $value = str_replace(' ', '', $value);
        }
        if ($amount_field_decimals == '.')
        {
            $value = str_replace(',', '', $value);
        }
        else
        {
            $value = str_replace(array('.', ','), array('', '.'), $value);
        }
        
        return $value;
    }
    
    /**
     * Za imena dve klase proverava da li su iste ili nasljedjuju jedna drugu
     * Mogu da se stave u bio kom redosledu
     * @param string $class1
     * @param string $class2
     * @return boolean
     */
    public static function classMatch($class1, $class2)
    {
        return $class1 === $class2 || is_subclass_of($class1, $class2) || is_subclass_of($class2, $class1);
    }
    
    /**
     * Za dva modela proverava istu klasu i isti ID
     * Mogu da se stave u bio kom redosledu
     * @param SIMAActiveRecord $model1
     * @param SIMAActiveRecord $model2
     * @param boolean $true_on_both_null = true
     * @return boolean
     */
    public static function modelMatch($model1, $model2, $true_on_both_null = true)
    {
        return 
            (
                !is_null($model1) && !is_null($model2)
                &&    
                get_class($model1) === get_class($model2)
                && 
                $model1->id === $model2->id
            )
            || ($true_on_both_null && is_null($model1) && is_null($model2))
        ;
    }
    
    /**
     * Konvertuje cirilicni tekst u latinicni. U slucaju da je latinicno slovo samo vraca latinicno
     * @param string $cyrillic_text
     */
    public static function convertCyrillicToLatin($cyrillic_text)
    {
        $cyrillic_to_latin = [
            'а'=>'a','б'=>'b','в'=>'v','г'=>'g','д'=>'d','ђ'=>'đ','е'=>'e','ж'=>'ž','з'=>'z','и'=>'i','ј'=>'j','к'=>'k','л'=>'l','љ'=>'lj',
            'м'=>'m','н'=>'n','њ'=>'nj','о'=>'o','п'=>'p','р'=>'r','с'=>'s','т'=>'t','ћ'=>'ć','у'=>'u','ф'=>'f','х'=>'h','ц'=>'c','ч'=>'č',
            'џ'=>'dž','ш'=>'š',
            'А'=>'A','Б'=>'B','В'=>'V','Г'=>'G','Д'=>'D','Ђ'=>'Đ','Е'=>'E','Ж'=>'Ž','З'=>'Z','И'=>'I','Ј'=>'J','К'=>'K','Л'=>'L','Љ'=>'Lj',
            'М'=>'M','Н'=>'N','Њ'=>'Nj','О'=>'O','П'=>'P','Р'=>'R','С'=>'S','Т'=>'T','Ћ'=>'Ć','У'=>'U','Ф'=>'F','Х'=>'H','Ц'=>'C','Ч'=>'Č',
            'Џ'=>'Dž','Ш'=>'Š'
        ];
        $latin_text = '';
        $cyrillic_text_strlen = strlen($cyrillic_text);
        for ($i=0; $i<$cyrillic_text_strlen; $i++)
        {            
            $char = substr($cyrillic_text, $i, 2);
            //ako je stvarno cirilicno slovo onda ga zamenjujemo odgovarajucim latinicnim i povecavamo jos jednom brojac,
            //jer cirilicna slova zauzimaju dva karaktera
            if (isset($cyrillic_to_latin[$char]))
            {
                $latin_text .= $cyrillic_to_latin[$char];
                $i++;                
            }
            else
            {
                $latin_text .= $cyrillic_text[$i];
            }
        }
        
        return $latin_text;
    }
    
    /**
     * Konvertuje latinicni tekst u cirilicni. U slucaju da je cirilicno slovo samo vraca cirilicno
     * @param string $latin_text
     */
    public static function convertLatinToCyrillic($latin_text)
    {
        $latin_to_cyrillic = [
            'a'=>'а','b'=>'б','v'=>'в','g'=>'г','d'=>'д','đ'=>'ђ','e'=>'е','ž'=>'ж','z'=>'з','i'=>'и','j'=>'ј','k'=>'к','l'=>'л','lj'=>'љ',
            'm'=>'м','n'=>'н','nj'=>'њ','o'=>'о','p'=>'п','r'=>'р','s'=>'с','t'=>'т','ć'=>'ћ','u'=>'у','f'=>'ф','h'=>'х','c'=>'ц','č'=>'ч',
            'dž'=>'џ','š'=>'ш',
            'A'=>'А','B'=>'Б','V'=>'В','G'=>'Г','D'=>'Д','Đ'=>'Ђ','E'=>'Е','Ž'=>'Ж','Z'=>'З','I'=>'И','J'=>'Ј','K'=>'К','L'=>'Л','Lj'=>'Љ',
            'M'=>'М','N'=>'Н','Nj'=>'Њ','O'=>'О','P'=>'П','R'=>'Р','S'=>'С','T'=>'Т','Ć'=>'Ћ','U'=>'У','F'=>'Ф','H'=>'Х','C'=>'Ц','Č'=>'Ч',
            'Dž'=>'Џ','Š'=>'Ш'
        ];
        $cyrillic_text = '';
        $latin_text_strlen = strlen($latin_text);
        for ($i=0; $i<$latin_text_strlen; $i++)
        {
            $char = substr($latin_text, $i, 1);
            $char1 = substr($latin_text, $i, 2); //slucaj kada su dva slova(lj, nj)
            
            $letter_to_include = $latin_text[$i];
            if (isset($latin_to_cyrillic[$char1]))
            {
                $letter_to_include = $latin_to_cyrillic[$char1];
                $i++;
            }
            else if (isset($latin_to_cyrillic[$char]))
            {
                $letter_to_include = $latin_to_cyrillic[$char];
            }
            
            $cyrillic_text .= $letter_to_include;
        }
        
        return $cyrillic_text;
    }
    
    /**
     * Vraca id adresu klijenta sa kog se pristupa
     * @return string
     */
    public static function getClientIpAddress()
    {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
        {
            $ipaddress = getenv('HTTP_CLIENT_IP');
        }
        else if(getenv('HTTP_X_FORWARDED_FOR'))
        {
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        }
        else if(getenv('HTTP_X_FORWARDED'))
        {
            $ipaddress = getenv('HTTP_X_FORWARDED');
        }
        else if(getenv('HTTP_FORWARDED_FOR'))
        {
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        }
        else if(getenv('HTTP_FORWARDED'))
        {
           $ipaddress = getenv('HTTP_FORWARDED');
        }
        else if(getenv('REMOTE_ADDR'))
        {
            $ipaddress = getenv('REMOTE_ADDR');
        }
        else
        {
            $ipaddress = 'UNKNOWN';
        }
        
        return $ipaddress;
    }
    
    public static function isLocalhost()
    {
        $local_ips = array(
            '127.0.0.1',
            '::1'
        );

        return in_array(SIMAMisc::getClientIpAddress(), $local_ips);
    }
    
    public static function isIpAddressInRange($ip, $range) 
    {
        if (strpos($range, '/') !== false) 
        {
            // $range is in IP/NETMASK format
            list($range, $netmask) = explode('/', $range, 2);
            if (strpos($netmask, '.') !== false) 
            {
                // $netmask is a 255.255.0.0 format
                $netmask = str_replace('*', '0', $netmask);
                $netmask_dec = ip2long($netmask);
                return ( (ip2long($ip) & $netmask_dec) == (ip2long($range) & $netmask_dec) );
            } 
            else 
            {
                // $netmask is a CIDR size block
                // fix the range argument
                $x = explode('.', $range);
                while (count($x) < 4)
                    $x[] = '0';
                list($a, $b, $c, $d) = $x;
                $range = sprintf("%u.%u.%u.%u", empty($a) ? '0' : $a, empty($b) ? '0' : $b, empty($c) ? '0' : $c, empty($d) ? '0' : $d);
                $range_dec = ip2long($range);
                $ip_dec = ip2long($ip);

                # Strategy 1 - Create the netmask with 'netmask' 1s and then fill it to 32 with 0s
                #$netmask_dec = bindec(str_pad('', $netmask, '1') . str_pad('', 32-$netmask, '0'));
                # Strategy 2 - Use math to create it
                $wildcard_dec = pow(2, (32 - $netmask)) - 1;
                $netmask_dec = ~ $wildcard_dec;

                return (($ip_dec & $netmask_dec) == ($range_dec & $netmask_dec));
            }
        } 
        else 
        {
            // range might be 255.255.*.* or 1.2.3.0-1.2.3.255
            if (strpos($range, '*') !== false) 
            { // a.b.*.* format
                // Just convert to A-B format by setting * to 0 for A and 255 for B
                $lower = str_replace('*', '0', $range);
                $upper = str_replace('*', '255', $range);
                $range = "$lower-$upper";
            }

            if (strpos($range, '-') !== false) 
            { // A-B format
                list($lower, $upper) = explode('-', $range, 2);
                $lower_dec = (float) sprintf("%u", ip2long($lower));
                $upper_dec = (float) sprintf("%u", ip2long($upper));
                $ip_dec = (float) sprintf("%u", ip2long($ip));
                return ( ($ip_dec >= $lower_dec) && ($ip_dec <= $upper_dec) );
            }
            else if($ip === $range)
            {
                /**
                 * u slucaju da range i nije range, vec konkretan ip
                 */
                return true;
            }
            
            return false;
        }
    }

    /**
     * 
     * @param array $files - niz koji se sastoji od podnizova parova path-displayname
     * [
     *  ['path'=>'/asd/qwe/fajl1', 'display_name'=>'imefajla'],
     *  ['path'=>'/qwe/xcv/fajl2', 'display_name'=>'imefajla2']
     * ]
     */
    public static function CreateTempZipArchive(array $files)
    {
        if(count($files) <= 0)
        {
            throw new Exception(__METHOD__.' - $files empty');
        }
        
        $zip = new ZipArchive();

        $tempFile = new TemporaryFile();
        $temp_full_path = $tempFile->getFullPath();

        if ($zip->open($temp_full_path, ZIPARCHIVE::CREATE) !== TRUE)
        {
            throw new Exception(__METHOD__.' - cannot open <$temp_full_path>');
        }

        foreach ($files as $file)
        {
            $file_path = $file['path'];
            $file_display_name = $file['display_name'];

            $zip->addFile($file_path, $file_display_name);
        }

        $zip->close();
        
        return $tempFile;
    }
    
    public static function RemoveSerbianLetters($input_string, $remove_only_latin_letters = false)
    {        
        /// latin
        $serbian_letters = [
            'Dj' => ['Đ'],
            'dj' => ['đ'],
            'Z' => ['Ž'],
            'z' => ['ž'],
            'C' => ['Ć', 'Č'],
            'c' => ['ć', 'č'],
            'S' => ['Š'],
            's' => ['š'],
        ];
        
        if($remove_only_latin_letters === false)
        {
            $serbian_cyr_letters = [
                'a' => ['а'],
                'b' => ['б'],
                'v' => ['в'],
                'g' => ['г'],
                'd' => ['д'],
                'dj' => ['ђ'],
                'e' => ['е'],
                'z' => ['ж', 'з'],
                'I' => ['И'],
                'i' => ['и'], 
                'j' => ['ј'],
                'k' => ['к'],
                'l' => ['л'],
                'lj' => ['љ'],
                'm' => ['м'],
                'n' => ['н'],
                'nj' => ['њ'],
                'o' => ['о'],
                'p' => ['п'],
                'r' => ['р'],
                's' => ['с', 'ш'],
                't' => ['т'],
                'c' => ['ћ', 'ц', 'ч'],
                'u' => ['у'],
                'f' => ['ф'],
                'h' => ['х'],
                'dz' => ['џ']
            ];
            
            foreach($serbian_cyr_letters as $key => $value)
            {
                foreach($value as $val)
                {
                    $serbian_letters[$key][] = $val;
                }
            }
        }
        
        $output = '';
                
        $input_string_strlen = strlen($input_string);
        $input_string_strlen_minus_one = $input_string_strlen-1;
        for ($i = 0; $i < $input_string_strlen; $i++)
        {            
            $removed = false;
            if($i < $input_string_strlen_minus_one)
            {                
                $whole_serbian_letter = $input_string[$i] . $input_string[$i + 1];
                                
                foreach($serbian_letters as $key => $value)
                {
                    if(in_array($whole_serbian_letter, $value))
                    {
                        $output .= $key;
                        
                        $i++;
                        
                        $removed = true;
                        break;
                    }
                }
            }
                        
            if($removed === false)
            {
                $output .= $input_string[$i];
            }
        }
        
        return $output;
    }
    
    public static function stringEndsWith($haystack, $needle)
    {
        // search forward starting from end minus needle length characters
        return $needle === "" || (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== FALSE);
    }
    public static function stringStartsWith($haystack, $needle)
    {
        return substr($haystack, 0, strlen($needle)) === $needle;
    }
    
    /**
     * Uporedjuje da li su dva broja jednaka
     * @param mixed $num1
     * @param mixed $num2
     * @param float $less_then - apsolutna razlika manja od
     * @return boolean ukoliko su jednaka, onda TRUE, ako nisu onda FALSE
     */
    public static function areEqual($num1, $num2, $less_then = 0.0001)
    {
        $_num1 = floatval($num1);
        $_num2 = floatval($num2);
        return abs($_num1 - $_num2) < $less_then;
    }
    
    /**
     * Uporedjuje da li je prvi broj manji od drugog
     * @param mixed $num1
     * @param mixed $num2
     * @param float $less_then - apsolutna razlika manja od
     * @return boolean ukoliko je mrvi manji, onda TRUE, ako ne, onda FALSE
     */
    public static function lessThen($num1, $num2, $less_then = 0.0001)
    {
        $_num1 = floatval($num1);
        $_num2 = floatval($num2);
        return ($num1 < $num2) && abs($_num1 - $_num2) > $less_then;
    }
    
    /**
     * Uporedjuje da li je prvi broj manji ili jednak sa drugim
     * @param mixed $num1
     * @param mixed $num2
     * @param float $less_then - apsolutna razlika manja od
     * @return boolean ukoliko je mrvi manji ili su jednaki, onda TRUE, ako nisu onda FALSE
     */
    public static function lessOrEqualThen($num1, $num2, $less_then = 0.0001)
    {
        $_num1 = floatval($num1);
        $_num2 = floatval($num2);
        return ($num1 < $num2) || abs($_num1 - $_num2) < $less_then;
    }
    
    /**
     * Vraca da li je prvi datum posle drugog
     * @param type $date1
     * @param type $date2
     * @return boolean
     */
    public static function laterThen($date1, $date2)
    {
        $date1_time = strtotime($date1);
        $date2_time = strtotime($date2);
        return $date1_time > $date2_time;
    }
    
    /**
     * Vraca da li su datumi jednaki ali bas dani, nije bitno koliko je sati i minuta
     * @param type $date1
     * @param type $date2
     * @return boolean
     */
    public static function sameDay($date1, $date2)
    {
        $date1_time = strtotime($date1);
        $date1_str = date ("d.m.Y.", $date1_time);
        $day1_time = strtotime($date1_str);
        $date2_time = strtotime($date2);
        $date2_str = date ("d.m.Y.", $date2_time);
        $day2_time = strtotime($date2_str);
        return $day1_time === $day2_time;
    }

    /**
     * Postavlja memory limit na prosledjeni broj bajtova
     * @param int $new_size - broj bajtova na koji se postavlja memory limit
     */
    public static function increaseMemoryLimit($new_size)
    {
        $_new_size = intval($new_size);
        $memory_limit = ini_get('memory_limit');
        if (preg_match('/^(\d+)(.)$/', $memory_limit, $matches)) 
        {
            if ($matches[2] == 'M') 
            {
                $memory_limit = $matches[1] * 1024 * 1024;
            } 
            else if ($matches[2] == 'K') 
            {
                $memory_limit = $matches[1] * 1024;
            }
        }
        
        if ($memory_limit < $_new_size)
        {            
            $new_memory_limit = round(($_new_size/1024)/1024);                     
            ini_set('memory_limit',$new_memory_limit.'M');
        }
    }
    
    public static function filter_bool_var($var)
    {
        $var_parsed = filter_var($var, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
        if(is_null($var_parsed))
        {
            throw new Exception(Yii::t('BaseModule.Misc', 'ProblemParsingBoolVar'));
        }
        
        return $var_parsed;
    }
    
    public static function stackTrace()
    {
        $stack = debug_backtrace();
        $output = '';

        $stackLen = count($stack);
        for ($i = 1; $i < $stackLen; $i++) 
        {
            $entry = $stack[$i];

            $func = $entry['function'] . '(';
            $argsLen = count($entry['args']);
            for ($j = 0; $j < $argsLen; $j++) 
            {
                $my_entry = $entry['args'][$j];
                if (is_string($my_entry)) 
                {
                    $func .= $my_entry;
                }
                if ($j < $argsLen - 1)
                {
                    $func .= ', ';
                }
            }
            $func .= ')';

            $entry_file = 'NO_FILE';
            if (array_key_exists('file', $entry)) 
            {
                $entry_file = $entry['file'];
            }
            $entry_line = 'NO_LINE';
            if (array_key_exists('line', $entry)) 
            {
                $entry_line = $entry['line'];
            }
            $output .= $entry_file . ':' . $entry_line . ' - ' . $func . PHP_EOL;
        }

        return $output;
    }
    
    public static function decodeText($text)
    {
        $detected_encoding = mb_detect_encoding($text);        
        if($detected_encoding === 'UTF-8')
        {            
            return utf8_decode($text);
        }
        else
        {
            return $text;
        }
    }
    
    public static function encodeText($text)
    {
        $detected_encoding = mb_detect_encoding($text);        
        if($detected_encoding === 'UTF-8')
        {            
            return utf8_encode($text);
        }
        else
        {
            return $text;
        }
    }
    
    /**
     * Proverava da li je datum ispravan i ako nije ispravlja ga.
     * Za proveru ispravnosti koristi istu proveru kao i php funkcija checkdate.
     * U slucaju da (mesec ili godina) nije ispravna, onda uzimamo trenutni(mesec, godinu). Ako dan nije ispravan uzimamo 1
     * @param DateTime $date
     * @return DateTime $date
     */
    public static function checkAndCorrectDate(DateTime $date)
    {
        $year = intval($date->format('Y'));
        $month = intval($date->format('m'));
        $day = intval($date->format('d'));

        $have_changes = false;
        if ($year < 1 || $year > 32767)
        {
            $have_changes = true;
            $year = intval(date('Y'));
        }

        if ($month < 1 || $month > 12)
        {
            $have_changes = true;
            $month = intval(date('m'));
        }

        if ($have_changes === true)
        {            
            $date->setDate($year, $month, $day);
        }

        //ako i dalje nije validan datum, znaci da dan nije dobar, pa onda dan setujemo na 1
        if (checkdate($month, $day, $year) === false)
        {            
            $date->setDate($year, $month, 1);
        }        
        
        return $date;
    }
    
    public static function generateFunctionStringFromArray($func_as_array)
    {
        $func_name = $func_as_array[0];
        array_shift($func_as_array);
        $func_params = [];        
        foreach ($func_as_array as $value) 
        {
            if (gettype($value) === 'integer' || $value === '$(this)')
            {
                $func_params[] = $value;
            }
            else if (gettype($value) === 'boolean')
            {                
                $func_params[] = ($value)?'true':'false';                
            }
            else if (is_array($value))
            {
                $func_params[] = htmlspecialchars(CJSON::encode($value));
            }
            else
            {
                $value = addslashes($value);
                $func_params[] = htmlspecialchars("'$value'");
            }
        }
        
        return $func_name.'('.implode(',',$func_params).');';
    }
    
    public static function renderHtmlOptionsFromArray($htmlOptions)
    {
        $html_options = '';
        foreach ($htmlOptions as $option_key=>$option_value)
        {
            $html_options .= " $option_key='$option_value'";
        }
        
        return $html_options;
    }
    
    /**
     * 70x90
     * @return string
     */
    public static function getPersonDefaultImgSrc()
    {
        return 'images/person_default.png';
//        return 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEYAAABaCAYAAAAFOiBkAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyJpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoV2luZG93cykiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6NTYwM0ZBMjhGQUZEMTFFNUFFNTFBQzk0RTc2RTNEQzIiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6NTYwM0ZBMjlGQUZEMTFFNUFFNTFBQzk0RTc2RTNEQzIiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDo1NjAzRkEyNkZBRkQxMUU1QUU1MUFDOTRFNzZFM0RDMiIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDo1NjAzRkEyN0ZBRkQxMUU1QUU1MUFDOTRFNzZFM0RDMiIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PpB4/qkAAAdCSURBVHja7J1tb9s2EMeP1LMfkjRp0K4YtgEDhn3/r7E3w7Bh694MLdb1KUnt2JZlW+Lt7kjb6dqubURZCioCgl1Flqgf/3e84wOqFlfPFsBFqQH0ZVdCOiwQxJ7GjaJ7BD2YHkwPpgfTg+nB9GB6MD2YHsyXniu1UBQnrfbTJmo3/ob7a3aXK5fL4cFyusOCUdoepgTYLOlzA1iuAMqlnENj6MUNcaBrtLs2zOhI6GssnxDEco0cdx4MvyArYDMHXFwArq4B1jOCwkDMW+Lgf+BN5aBTTJCCSoYAyRHo7BQgPbGeQAD5V5FaXD3DZoGAQDCTp4DLK4CqsOawVc+nFv7NFkJACkrGoI6+ATU4tffxrKCwUShkHmb6FIAOrDa25VXwH4V86v3cb7mw2S0vAYs3BOYc9L3vSUkjOl91HAxDKQuoXv8OQC8gJHTg8f5M1t4PFy+gItPU938ENTz3Bkc3AoUcq3n5C0B+4UxGNei/QnHe5uWvgLPn3hrALxjpVsl8Lv4gmU/9quR/nxu45z4GzC+9PNezYjQ52SdUuYvDQXnLp63BXP4pZlxXpdprxcjWcfp3s6bzMeWsbA/4WT1es4pRYOb/UHe8ql2pug2E8+eA63mtemhvailz2wOpltMvVitH0/OXXQBD0SpHsqtF+2CkPgiGYhyrXtUiGI5Ki+sO5cbsa+Y2H4NWwRib97TldN9nTqQWSVBbVQyXquzeoApubp1fegKDdiihU0W5xsI2TYndTHVre26s1MibtL/2wc4Jps44jTcw2DW1INRSsPbWOqpjYKBenbQv3SoZROqYOUki23Z3rSPonC3psG3FcCYQdc+UGIxqE4yM4kcdMiWUIQjFYNoN8AhMlHSKC4SxHfaE1hWTdkoxYtriY9qMfJkNzxDWqIhfLmhnLIO2waDrlcK0I6JBa0pcJ2xVMUacr+K5ZTDtcyHTVjzn3foIHjcKS1cUg+2rhYPNcNiFXMnORatw0H5qgC5+iYe15rP9Rb6slHjkImBslwzVQQkY7AIYap1k7GYF2zUnxQ3UnXklih3Ix6io/d09KrtXP5vwXisPlaqZtMnios6BUWmLYHiZWjwW5db1c/7BxAPrhLGFeAYrMqMTt06vS4phGLwMjFVzaDBoeyNIjp3j7ZhiJJ5hP3PwvImeRY5f8aJFrLrnY3jKQmWntnc6qGoQNCs18BN960Zajm18cH7g3igANX7oLVdrZmkC9w7jryygQxReK5ydkdMfe1Npc2DIlPTw4QGmbm2epsePvC5va3AxCyWTx1+LQ2zU15Ba1PA+qMGZ1+c0B4ZjimgE+uRbuzegqR4qiOgZ37nV4XgXFGPhwOgRFJVuZAIXSSGLZQXYQEDZ+LowpRRcz3JY5IV893NPe9/pZAbz5aoR6AdZMKdUBBcXbyDPl6DlreqB5htMJtd0zHhyuJE6H0Qx2fhY7P/1qyuYTGe2I7mFevg3VVUR5IlAYSbJcOxNiTdL4/uV2B+GYQCj4QDmZE5Xb6awKtZwfDKGJImlvWXv2gccJ780v7cxSIorRCnr9UZ+k6UJJNmgEbd+oB1uCHEcw5AEOl9Yf1Os1jAYpAIsikOCZ3OrLZ+tCDabikCsYUZ+qihWApBhJXS/QUYJa0OjheEBHIxdPUklpRZmiSzynBRgYM5OebEU5cRxBFEUirq2QDabjahjRRC3sZFASRIYDjLpicxmZWMmz7ppFgxFolX+ho5rqTy3NkMItCY4SyjLUs6xEpbUu2itdv6CTQdlf6Q9x9fx37MshTRJdqDMcgqmmILmcRiPG7l0UypRQQi4Jvk/+22/u207OkDKOBqPyBQyetlgZzr88qwkPqRyel89Vgn/JkvTt9VI987pGWa9lGf6mr7xtyeSx2G0jT5NuYFy9gqKF4/BrD682WHby7C5rMls+Ltj51avKYijSPwTm9hWOe86+AqC9AjSBz9AOD4HLSsd1G5X7oHB2L3T23C/Wi0EQpVPYD19IRLfAvtYsLY1MzYfQ+aATi2BU8yHgLwzesgZAplUdPwAgsEJ6GQEAe+8lfub/d5tr2BYEU6m0hL0ArwXsZxfQUl+xAiYXKSttL71vI7jdPu0h5fv82BZEBOYgcAJCVI4OiNIY/F7ygHH3c7czwXjzINvgGTDZpNDVcwsjMUVIPcIJGNpDXDq6crqTURXL3Qb3ANZdBAOTy2kbAyah0ai9L1mtwfjQm0rW5sAMgTuVcqcPT99L66p61079eh9896Jgm4FuzUpRX4oyI5IRSMxO1aVzo6kP5KsJZ++QpTdIyt56Yp8Qzm/hGpB5lEWdI6iTB5s2sG4KyA+UVEcEuiQQEWinoAVNTwDNfnrJywpzjDLCamDlOG6VrVzsHdJFfXUtI+8EcL8yc97RWg+Avjyyl4AW4MI5VwQQV/eiXxVT6GtgaoeTA+mB9OD6UsPpgfTg+nB9GB6MF0tPH2SS16pw/4/bLhR/hVgAMSGJCNQM183AAAAAElFTkSuQmCC';
    }
    
    public static function getCompanyImgBase64()
    {
        //return 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEASABIAAD/2wBDAAMCAgICAgMCAgIDAwMDBAYEBAQEBAgGBgUGCQgKCgkICQkKDA8MCgsOCwkJDRENDg8QEBEQCgwSExIQEw8QEBD/2wBDAQMDAwQDBAgEBAgQCwkLEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBD/wgARCABaAEYDAREAAhEBAxEB/8QAHAABAAEFAQEAAAAAAAAAAAAAAAMBBAUGBwII/8QAFwEBAQEBAAAAAAAAAAAAAAAAAAECA//aAAwDAQACEAMQAAAB+l9ZAAAAhFaXuYyzK5u5YoAhocp64x1ZOMtLcxuuNCGwChU59uTG94ogoUBU55vM8b5nQiQaTuZePBganN5xRb1Q5huUIUuFvo6DmiKvJomp5S7XHA6LmiCzyc5sFTwXZv8AKIbPJzsAF4b0CEFAACoIqAAAA//EACYQAAICAgEDAwUBAAAAAAAAAAARAwQBAgUSITQGEzUQIDAyM0T/2gAIAQEAAQUCYxjGMYxjGS+pdNLGnqW17tL1FrPMxjGMZJw97WfSrZklpcRZszSy3Y+W4Oa7vYYxjGMZamlxzfAzS72mMYzqOoZa+c4DymMYyW/JS5Pkp9ZeN4zlMWMWvmuC8ljGZ27TzbWJdZpNY8ZzjObEm0/GT7RW2MZnPan/AB/wSfvd8ij5bGMzntrvvrjr36fdkybbbb5p+UxjM57fZU8ljGP6MYxjH+L/xAAYEQADAQEAAAAAAAAAAAAAAAABETAAYP/aAAgBAwEBPwGbzzmIiI4L/8QAHxEAAgEEAgMAAAAAAAAAAAAAAAEwERIgMQIhEEFQ/9oACAECAQE/AY7CwfHOvhs6oPUC0OBaHjToQ0ehwv6H/8QALhAAAQIDBAgGAwAAAAAAAAAAAQIDABESICEiMQQTMDJBUXLBI1JhcYGDQJHC/9oACAEBAAY/AtmUJYqaBlVPOKnGkFvyjONVpLYbCt1U7ZZQyVDgrhKNSlldfKUUutqbQN4mAy0t/UBaQBfKUhDg0lbxTRdXPYUBxVOtRdP2hwLcUrBxPrsPtR2hzo72XeLZIqT8QXmV3TSQR7xqXzJ3gfNH2I7Q50d7Knl5q5QpoKwLzETEDSFXrmD+oSEy8TCbBh7ADd/JjcGecaJ4acx87sHCBcnL2hrqsGCEqInnFFRp5QnGcOXpFSjMw11WDZb6vxf/xAAmEAEAAgEDAwQCAwAAAAAAAAABABExICFRQZHwEGGBoTDRcbHh/9oACAEBAAE/Ifx/+We1Rv8AcEPcx4AfzLw8oLA8P71ep1lzjPO5XLR1q4IEY+3r4PeFlCxUweatgXZzqPS76+munQo6xBAgwR6NOAcN1tEGWvQ8vCodeLDSaiX2sNz3luhIG40gIgHg5iz8qh15MNHQTwQIA3noiHNh6FjmAkRNxIhYFh6tP1CwQizi9G7UwPzuuPh8qJl/b5gcNfX45beXBXsGwxn0+g9uYKQxDny2f5QRUl3n+nYlpfIz6zRu1afrtJ2cSnEpxKcSnH5P/9oADAMBAAIAAwAAABC3222+gr/9IgzpEmlQn/8A/wD+lWeUtgvNsoLJEpG/zI+/+2222+22222//8QAHREAAwACAwEBAAAAAAAAAAAAAAERMDEQIEEhUP/aAAgBAwEBPxDHvxV9aUjIxJsrTGdKXm8U9G+4PTbrYPQmei3gosK/H//EAB0RAAMAAgMBAQAAAAAAAAAAAAABESAhEDAxQVH/2gAIAQIBAT8QhCEIQhCcrQiEVrGEEpUlRKEmoVLQhOYQgiCqLpPKxVBIz8BDzitLBNcwgz6IQ/CE4WL7IQhCEIQnV//EACUQAQACAgEEAgMAAwAAAAAAAAEAETFRIUFhcZGBsRChwSDw8f/aAAgBAQABPxCm5TcpuU3KblNym5TcpuUhdo6zh5IJVxwKq8c4j9TLN6FTeXyU9oZuLGpi2q/Tx+FJSUldxQrB4YLe1FHRLo4ybnMCSbFGbXgO8yoOqAyLlejMCxuWAEXCZv5i5oA6emwurldyu5TcpuU3AUAFlrllNxFw0oq2lXXVjOaoMHJSym5Tf57Guy1OHUoa7IFtWvT8eaoti7/yflPKZZlHPoB+8M4bdGA+YRMZlJ0ScBPrc65OssUSS++HlPKW3Ce8o/qKxkENEA49SzsG+aBBp49RJ7iFImEdxor8OAAUN0uVLQNhB478S25bc851/wD5RDlRWYetOXnJyPIlOBs38VX9gungA7LoX85fLBepKx1cGc46xUmvtnnPOUhf7WIfNqoAUnPwHyzuMvnlnEcMAtyFU6fwI0RgFLUCj0EdPr75SUnnOudX1DH+Dp9ffPOecpKTtvU7L1Oy9TsvUBbB6lJSW7lu5buW7lu5buW7lu5buf/Z';
        return 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOgAAADoCAIAAABqyz8vAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMDY3IDc5LjE1Nzc0NywgMjAxNS8wMy8zMC0yMzo0MDo0MiAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTUgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOkI4N0VCNkQ3MjFGRDExRThBMDA3OEE5ODA1NUM0N0NBIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOkI4N0VCNkQ4MjFGRDExRThBMDA3OEE5ODA1NUM0N0NBIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6Qjg3RUI2RDUyMUZEMTFFOEEwMDc4QTk4MDU1QzQ3Q0EiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6Qjg3RUI2RDYyMUZEMTFFOEEwMDc4QTk4MDU1QzQ3Q0EiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz7dzAcoAAAExUlEQVR42uzd4U8TZxzAcUqBFrNuDNDhG4xkAxMXdUvUvXPvtv3XumxLlESXTPdiCIlCJwVaWsvaO8q1W7K97WmerrW9fj5vn9yP0n65HC33kOv1ejMwaWY9BQgXhAvCRbggXBAuwgXhgnBBuAgXhAvCRbggXBAuwgXhgnBBuAgXRmzOU/Cvh48etdvR+D/OxcXitw8eeL2ccREuCBeEi3BBuCBchAvjLZeZ/XGfbG8Pcvhprd7tdSfgTJOb/XR5aZAJ9+7ezcDLnZ1PzqrV2jScaf756ZqS79SlAsIF4YJwES4IF4QLwkW4IFx4t+z8rUKz2fRyvo9SqSRc+DCy80c2T5/96uV8H19/dUe4Y6RSqYjSL2cgXBAuCBfhgnBBuGTQVOyPu7qyUiwWw45NkuTN4WG/1curq4VCIWxyHMfHJyf9Vq+ureXz+bDJURSdVKvCnXjr19Y/u3Il7Nh2u50S7sbG9eXl5bDJtVotJdytrc3FxcWwyZWjo8yH61IB4YJwQbgIF4QLwkW4IFwQLggX4YJwQbgIF4QLwkW4IFwQLgiXiTYVd/m+bTSCj+2cd1JWT0/rnYuLsMlnzbOU1Wq1Nr8wP/rvV7hj5OXu3pAm/7GzM6TJvz1/7rTqUgHhgnBBuAgXxttUvKtQWFiYzQf+iPZ6vSiK+04uFGZnc2GTu91eHPedXCwWcrnQyUk3Pj8X7sS7+eXNQXZrfPjox36rd27fGmS3xsdPtvutfnP//iC7NT59+sylAggXhItwQbggXBAuwgXhgnARLggXhItwQbggXBAuwgXhgnARLggX/k9TcXv68fFx1G6HHdvppG0zelipNJvNsMl/tVopq+VyeX4+cJvR5tmZcLNgf/9gSJNfvXo9pMk7L3edVl0qIFwQLggX4cJ4m4p3FUoflRYKgW8tJUm3Xq/3W/3k49Jc6JtWF51O423ft9KWlpbyoVujnsed5llTuBPvi83Ph7TN6I0bN4a0zeid27dsM+pSAeGCcEG4CBeEC8IF4SJcEC4IF+GCcEG4CBeEC8IF4SJcEC4IF+HCGJmK29PLB+XTWi3s2IuLJGX19ev9o6OjsMlRFKes7u7uzc3lwya3Wm3hZkEltK13enN4OKTJ+wcHM7hUQLggXBAuwgXhgnBBuEyqqfgAYnVlpVgshh2bJEnKpwyXV1cLhULY5DiOj09O+q1eXVvL5wM/OYui6KRaFe7EW7+2PsiO5CnhbmxcH2RH8pRwt7Y2B9mRPPPhulRAuCBcEC7CBeGCcBEuCBeEC8JFuCBcEC7CBeGCcBEuCBeEC8JFuDBqU3F7+t7eXrn8Z9ix3SRtR/KdnZfzCwthkzvn5ymrL178Phu6r0IcR8LNgnq9MTPTGMbk2unpkB5zypYLuFRAuCBcEC7CBeGCcBEuCBeEC//Jzke+83NzXs7pkev1ep4FnHE/mGazOfoveunSpeD/jZMkSavVGv1jLpVKwh0jP/38y+i/6P17d4P/606j0Xj8ZHv0j/mH77/zyxkIF4SLcEG4IFyEC8IF4YJwES4IF4SLcEG4IFyEC8IF4YJwyRC3p+OMC8IF4SJcEC4IF+GCcEG4IFyEC8IF4SJcEC4IF+GCcEG4IFyEC6P2twADAOWtS88e+9y3AAAAAElFTkSuQmCC';
    }
    
    public static function toTypeAndJsonString($var)
    {
        $_type = gettype($var);
        if ($_type === 'object')
        {
            $_type .= ' - '.get_class($var);
        }
        return '('.$_type.' ) '.CJSON::encode($var);
    }
    
    /**
     * Spaja nizove u jedan niz
     * @param array $array_of_arrays niz nizova ciji elementi treba da se spoje
     * @return array
     */
    public static function joinArrays($array_of_arrays)
    {
        if (count($array_of_arrays)>0)
        {
            return call_user_func_array ( 'array_merge' , $array_of_arrays );
        }
        else
        {
            return [];
        }
    }
    
    /**
     * Vraca hesiranu vrednost niza tako sto hesira svaku njegovu stavku i sortira ih. Radi samo za jedno dimenzionalne nizove
     * @param array $array
     * @return string
     */
    public static function hashArray(array $array)
    {
        $hashed_items = [];
        foreach($array as $item)
        {
            $hashed_items[] = md5(CJSON::encode($item));
        }
        sort($hashed_items);

        return md5(CJSON::encode($hashed_items));
    }
    
    public static function isValidNumericNumber($number, $precision, $scale) 
    {
        $isValid = false;
        
        $max = str_repeat('9',$precision - $scale).'.'.str_repeat('9',$scale);
        
        if (is_numeric($number)) 
        {
            $num = round(abs($number), $scale);
            $isValid = $num <= $max;
        }
        else if ($number instanceof SIMABigNumber)
        {
            $max_number = SIMABigNumber::fromString($max);
            $isValid = $number->isLessOrEqualTo($max_number);
        }

        return $isValid;
    }
    
    public static function isProduction()
    {
        return isset(Yii::app()->params['is_production']) && SIMAMisc::filter_bool_var(Yii::app()->params['is_production']);
    }
    
    // Time format is UNIX timestamp or
    // PHP strtotime compatible strings
    // https://www.if-not-true-then-false.com/2010/php-calculate-real-differences-between-two-dates-or-timestamps/
    public static function dateDiff($time1, $time2, $precision = 6)
    {
        // If not numeric then convert texts to unix timestamps
        if (!is_int($time1)) {
          $time1 = strtotime($time1);
        }
        if (!is_int($time2)) {
          $time2 = strtotime($time2);
        }

        // If time1 is bigger than time2
        // Then swap time1 and time2
        if ($time1 > $time2) {
          $ttime = $time1;
          $time1 = $time2;
          $time2 = $ttime;
        }

        // Set up intervals and diffs arrays
        $intervals = array('year','month','day','hour','minute','second');
        $diffs = array();

        // Loop thru all intervals
        foreach ($intervals as $interval) {
          // Create temp time from time1 and interval
          $ttime = strtotime('+1 ' . $interval, $time1);
          // Set initial values
          $add = 1;
          $looped = 0;
          // Loop until temp time is smaller than time2
          while ($time2 >= $ttime) {
            // Create new temp time from time1 and interval
            $add++;
            $ttime = strtotime("+" . $add . " " . $interval, $time1);
            $looped++;
          }

          $time1 = strtotime("+" . $looped . " " . $interval, $time1);
          $diffs[$interval] = $looped;
        }

        $count = 0;
        $times = array();
        // Loop thru all diffs
        foreach ($diffs as $interval => $value) {
          // Break if we have needed precission
          if ($count >= $precision) {
            break;
          }
          // Add value and interval 
          // if value is bigger than 0
          if ($value > 0) {
            // Add s if value is not 1
            if ($value != 1) {
              $interval .= "s";
            }
            // Add value and interval to times array
            $times[] = $value . " " . $interval;
            $count++;
          }
        }

        // Return string with times
        return implode(", ", $times);
    }
    
    public static function GetProjectRootDir()
    {
        $cur_dir = __DIR__;
        $root_dir = substr($cur_dir, 0, strrpos($cur_dir, '/protected/'));
        return $root_dir;
    }
    
    public static function limitString($string, $limit = 100)
    {
        if(strlen($string) > $limit)
        {
            $string = substr($string, 0, $limit).'...';
        }

        return $string;
    }
    
    /**
     * vraca broj redova u csv fajlu
     * @param type $file_path
     * @return int
     */
    public static function numberOfRowsInCSV($file_path)
    {
        $number_of_rows = 0;
        $file = fopen($file_path,"r");
        while (($csv_row = fgetcsv($file)) !== false)
        {
            $number_of_rows++;
        }
        fclose($file);
        return $number_of_rows;
    }
    
    /**
     * Konvertuje postgresql array(zapisanog kao php string) u php array
     * @param string $pg_array
     * @return array
     */
    public static function pgArrayToPhpArray($pg_array)
    {
        return CJSON::decode(str_replace(['{}', '{', '}', ","], ["[]", '["', '"]', '","'], $pg_array));
    }
    
    /**
     * 
     * @param type $glue
     * @param type $array
     * @param type $array_begin_marker
     * @param type $array_end_marker
     * @param type $with_key
     * @param type $item_wrapper
     * @param type $chars_to_eskape
     * @param type $with_key_just_this_pass
     * @param type $item_wrapper_level_escaping
     * @param type $current_level
     * @param type $wrapp_array
     * @return string
     */
    public static function multiDimensionalImplode(
            $glue, $array, $array_begin_marker, $array_end_marker, 
            $with_key=false, $item_wrapper='"', $chars_to_eskape=[], 
            $with_key_just_this_pass=true, $item_wrapper_level_escaping=false, 
            $current_level=0, $wrapp_array=false
    )
    {
        $temp_ret = '';
        
        if($item_wrapper_level_escaping == true)
        {
            $item_wrapper = str_repeat('\\', $current_level).$item_wrapper;
        }

        foreach ($array as $key => $item)
        {
            $item_to_add = null;
            
            if (is_array($item)) 
            {
                $item_to_add = '';
                if($wrapp_array === true)
                {
                    $item_to_add .= $item_wrapper;
                }
                $item_to_add .= SIMAMisc::multiDimensionalImplode($glue, $item, $array_begin_marker, $array_end_marker, 
                                                                $with_key, $item_wrapper, $chars_to_eskape, $with_key, 
                                                                $item_wrapper_level_escaping, $current_level+1, $wrapp_array);
                if($wrapp_array === true)
                {
                    $item_to_add .= $item_wrapper;
                }
            }
            else if(is_bool($item))
            {
                $item_to_add = 'false';
                if($item)
                {
                    $item_to_add = 'true';
                }
            }
            else if(is_int($item))
            {
                $item_to_add = $item;
            }
            else
            {
                $item_to_add = $item;
                foreach($chars_to_eskape as $char_to_eskape)
                {
                    $item_to_add = str_replace($char_to_eskape, '\\'.$char_to_eskape, $item_to_add);
                }
                $item_to_add = $item_wrapper.$item_to_add.$item_wrapper;
            }
            
            if($with_key === true && $with_key_just_this_pass !== FALSE)
            {
                $temp_ret .= $item_wrapper.$key.$item_wrapper.':';
            }
            $temp_ret .= $item_to_add;
            $temp_ret .= $glue;
        }

        $ret = $array_begin_marker.substr($temp_ret, 0, 0-strlen($glue)).$array_end_marker;

        return $ret;
    }
    
    /**
     * Proverava da li je ukljucen vue. Kada se vue uvek bude koristio, treba da se obrise
     * @return boolean
     */
    public static function isVueComponentEnabled()
    {        
        if(isset(Yii::app()->params['use_vue_components']))
        {
            return Yii::app()->params['use_vue_components'];
        }
        
        return Yii::app()->configManager->get('base.use_vue_components') === true;
    }
    
    /**
     * Proverava da li je ukljucen vue i ako nije baca upozorenje. Koristi se na mestima gde dalji kod zavidi od vue.
     * Kada se vue uvek bude koristio, treba da se obrise
     * @throws SIMAWarnException
     */
    public static function checkIsVueComponentEnabled()
    {
        if(!SIMAMisc::isVueComponentEnabled())
        {
            throw new SIMAWarnException('Morate prvo uključiti Vue komponentu u korisničkim podešavanjima!');
        }
    }
    
    /**
     * Dodaje prosledjenu vrednost ukoliko ne postoji u nizu
     * @param array $array
     * @param mixed $value
     */
    public static function pushToArrayIfNotExist(&$array, $value) 
    {
        if (!in_array($value, $array))
        {
            $array[] = $value;
        } 
    }
    
    /**
     * Konvertovanje memorijskih jedinca
     * @param integer $value
     * @param string $convert_from
     * @param string $convert_to
     * @return integer
     */
    public static function convertMemoryUnit($value, $convert_from, $convert_to) 
    {   
        $units = ['B'=>0, 'KB'=>1, 'MB'=>2, 'GB'=>3, 'TB'=>4];

        $convert_from = strtoupper($convert_from);
        $convert_to = strtoupper($convert_to);
        
        if(isset($units[$convert_from]))
        {
            $from_unit_size = $units[$convert_from];
        }
        else
        {
            throw new SIMAWarnException("'{$convert_from}' ".Yii::t('BaseModule.Misc', 'UndefinedMemoryUnit'));
        }
        
        if(isset($units[$convert_to]))
        {
            $to_unit_size = $units[$convert_to];
        }
        else
        {
            throw new SIMAWarnException("'{$convert_to}' ".Yii::t('BaseModule.Misc', 'UndefinedMemoryUnit'));
        }
        
        $result = $value;

        if($from_unit_size > $to_unit_size)
        {
            $result = $value * pow(1024, $from_unit_size);
        }
        else if($from_unit_size < $to_unit_size)
        {
            $result = $value / pow(1024, $to_unit_size);
        }
        return $result;
    }
    
    public static function getScaleFromDbNumericField(string $numeric_definition) : string
    {
        return trim(str_replace(')','',substr($numeric_definition, strripos($numeric_definition, ',')+1)));
    }
    
    public static function hasModule($moduleString, $parent_module=null)
    {
        if(empty($parent_module))
        {
            $parent_module = Yii::app();
        }
        
        $exploded = explode('.', $moduleString);
        
        if(count($exploded) === 0)
        {
            return false;
        }
        
        $submodules = $parent_module->getModules();
        $submodules_keys = array_keys($submodules);
        
        $submodule = null;
        foreach ($submodules_keys as $key)
        {
            if($key === $exploded[0])
            {
                $submodule = $parent_module->getModule($key);
                break;
            }
        }
        if(empty($submodule))
        {
            return false;
        }
        
        if(count($exploded) > 1)
        {
            $subArray = array_slice($exploded, 1);
            $imploded = implode('.', $subArray);
            
            return Yii::app()->hasModule($imploded, $submodule);
        }
        
        return true;
    }
    
    public function secondsDisplayFormater($seconds)
    {
        $days = intval($seconds/86400);
        $remain = $seconds%86400;
        $hours = intval($remain/3600);
        $remain = $remain%3600;
        $mins = intval($remain/60);
        $secs = $remain%60;
        
        $timestring = '';

        if ($secs>=0) $timestring = $secs."s";
        if ($mins>0) $timestring = $mins."m ".$secs."s";
        if ($hours>0) $timestring = $hours."h ".$mins."m";
        if ($days>0) $timestring = $days."d ".$hours."h";
        
        if(empty($timestring))
        {
            $e = new Exception();
            $stack_trace = $e->getTraceAsString();
            $autobafreq_message = 'empty $timestring'
                                    .' </br>- $seconds: '.SIMAMisc::toTypeAndJsonString($seconds)
                                    .' </br>- $stack_trace: '.$stack_trace;
            error_log(__METHOD__.' - $autobafreq_message: '.$autobafreq_message);
            Yii::app()->errorReport->createAutoClientBafRequest($autobafreq_message, 3415, 'AutoBafReq SIMAMisc::secondsDisplayFormater empty $timestring');
        }

        return $timestring; 
    }
    
    /**
     * Converts a number to its roman presentation.
     */
    public static function numberToRoman($num) 
    {
        // Be sure to convert the given parameter into an integer
        $n = intval($num);
        $result = '';

        // Declare a lookup array that we will use to traverse the number: 
        $lookup = [
            'M' => 1000, 'CM' => 900, 'D' => 500, 'CD' => 400,
            'C' => 100, 'XC' => 90, 'L' => 50, 'XL' => 40,
            'X' => 10, 'IX' => 9, 'V' => 5, 'IV' => 4, 'I' => 1
        ];

        foreach ($lookup as $roman => $value) 
        {
            // Look for number of matches
            $matches = intval($n / $value);

            // Concatenate characters
            $result .= str_repeat($roman, $matches);

            // Substract that from the number 
            $n = $n % $value;
        }

        return $result;
    }
}