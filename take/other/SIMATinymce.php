<?php

class SIMATinymce
{
    public function init(){} 
    
    /**
     * Dohvata sve img tagove sa data-file-preview-params atributom, pronalazi fajl veziju i zamenjuje img src 
     * sa novim download linkom
     * @param type $content string
     * @return type string
     */
    private function parseImgLinks($content) 
    {
        preg_match_all('/<img[^>]+data-file-preview-params.*?>/i', $content, $images);
        foreach ($images[0] as $image) {
            $encoded_img = $image;
            preg_match('/data-file-preview-params="{(.*?)}"/i', $image, $data_file_preview_params);
            $data_file_preview_params_decoded = html_entity_decode($data_file_preview_params[1]);
            $has_model_name = preg_match('/"model_name":"FileVersion"/', $data_file_preview_params_decoded);
            $has_model_id = preg_match('/"model_id":"(.*?)"/', $data_file_preview_params_decoded, $model_match);
            if($has_model_name && $has_model_id)
            {
                $file_id = $model_match[1];
                $file_version = FileVersion::model()->findByPkWithCheck($file_id);
                $link = $file_version->getDownloadLink();
                $search = '/src=\"(.*?)\"/i';
                $replace = "src=\"$link\"";
                $secureImg = preg_replace($search, $replace, $image);
                $content = str_replace($encoded_img, $secureImg, $content);
            }
            else
            {
                error_log(__METHOD__." - *Nije bilo moguce pronaci 'model_name:FileVersion' ili 'model_id' u 'data-file-preview-params' atributu img elementa. \n"
                            ."*img element: ".$image."\n" 
                            ."*Ceo sadrzaj za pretragu: \n".$content);
            }
        }
        return $content;
    }
    public function parseContent($content)
    {
        return $this->parseImgLinks($content);
    }
}
