
/* global Vue, sima */

Vue.component('sima-search-field-result-list', {
    template: '#sima-search-field-result-list',
    props: {
        results: {
            type: Array,
            default() { return []; }
        },
        search: { type: String, default: '' },
        selected_item: { type: Object, default: null }, //Selektovana stavka
        model_name: { type: String, default: '' },
        show_add_new_item: { type: Boolean, default: false },//Prikaz elementa za otvaranje forme
        ssf_bus: {type: Object}
    },
    data: function () {
        return {
            key_navigation_position: -1 // navigacija za selektovanje stavke u listi rezultata. -1 - deselect
        };
    },
    computed: {
        
    },
    watch: {
        search(new_val, old_val) {
            if(!this.selected_item && (new_val !== old_val))
            {
                this.key_navigation_position = -1; //deselect
            }
        },
        selected_item(new_val, old_val) {
            if(this.selected_item)
            {
                this.key_navigation_position = -1; //deselect
            }
        }
    },
    created() {
        window.addEventListener('keyup', this.onKeyUp);
        window.addEventListener('keydown', this.onKeyDown);
    },
    destroyed() {
        window.removeEventListener('keyup', this.onKeyUp);
        window.removeEventListener('keydown', this.onKeyDown);
    },
    mounted: function () {
        var _this = this;

        $(this.$el).on('destroyed', function () {
            _this.$destroy();
        });
        
        this.init();
    },
    methods: {
        hits(text, i) {
            return text.replace(this.search, "<b>" + this.search + "</b>");
        },
        selectItem(i) {
            this.$emit('selectedItem', i);
        },
        init(){
            this.ssf_bus.$on('bus_event_enter', (data) => {
                if(this.key_navigation_position >= 0)
                {
                    this.selectItem(this.key_navigation_position);
                    this.ssf_bus.$emit('bus_event_close_result_list');
                }
            });
        },
        onKeyDown(e){
            //Izvrsavanje eventova kada je otvorena lista rezultata ili forma
            if(this.$el.style.display !== 'none')
            {
                //Izvrsavanje eventova samo za listu rezultata
                if(!this.show_form && this.results.length > 0)
                {
                    if(e.keyCode === 38)//up
                    {
                        if(this.key_navigation_position > -1)
                        {
                            var ul_list = this.$refs.results;
                            for (let item of ul_list.children) {
                                item.classList.remove('active-item');
                            }
                            this.key_navigation_position--;
                            if(this.key_navigation_position > -1)
                            {
                                var list_item = ul_list.children[this.key_navigation_position];
                                list_item.classList.add('active-item');
                                if(!this.isScrolledIntoView(list_item))
                                {
                                    list_item.scrollIntoView(false);
                                }
                            }
                        }
                        e.stopPropagation();
                        e.preventDefault();
                    }
                    else if(e.keyCode === 40)//down
                    {
                        var ul_list = this.$refs.results;
                        if(this.key_navigation_position+1 < this.results.length)
                        {
                            for (let item of ul_list.children) {
                                item.classList.remove('active-item');
                            }
                            this.key_navigation_position++;
                            var list_item = ul_list.children[this.key_navigation_position];
                            list_item.classList.add('active-item');
                            if(!this.isScrolledIntoView(list_item))
                            {
                                list_item.scrollIntoView(true);
                            }
                        }
                        e.stopPropagation();
                        e.preventDefault();
                    }
                }
            }
        },
        onKeyUp(e){
            //Izvrsavanje eventova kada je otvorena lista rezultata ili forma
            if(this.$el.style.display !== 'none')
            {
                if(e.keyCode === 27)//esc
                {
                    if(e.target.tagName === "BODY")//ako je ESC nad poljem adresa onda zatvaramo listu rezultata
                    {
                        this.ssf_bus.$emit('bus_event_close_parent_list');
                    }
                    else
                    {
                        this.ssf_bus.$emit('bus_event_blur_field', e.target);
                    }
                }
                else if(e.keyCode === 13)//enter
                {
                    if(this.key_navigation_position > -1)
                    {
                        this.selectItem(this.key_navigation_position);
                        this.key_navigation_position = -1;
                    }
                }
                e.preventDefault();
                e.stopPropagation();
            }
        },
        onClickOutside: function (e) {
            var _this = this;
            if (!_this.$el.contains(e.target) && _this.$el !== e.target) {
                _this.$el.remove();
            }
        },
        isScrolledIntoView(el) {
            var rect = el.getBoundingClientRect();
            return (
                    (rect.top >= 0) && (rect.bottom <= el.parentElement.parentElement.getBoundingClientRect().bottom) && 
                    (rect.bottom >= 0) && (rect.top >= el.parentElement.parentElement.getBoundingClientRect().top)
            );
        }
    }
});