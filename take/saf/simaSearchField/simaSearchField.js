
/* global Vue, sima, mixin_search_simaSearchField */
Vue.component('sima-search-field', {
    template: '#sima-search-field',
    name: 'sima-search-field',
    inheritAttrs: false,
    props: {
        value: {default: () => ({ label: '', id: '' }) },
        model_name: { type: String, default: '' },
        model_filter: { type: Object, default() { return {}; } },
        search_min_char: { type: Number, default: 2 },
        settings: {type: Object, default: null}
    },
    data: function () {
        return {
            search: "",
            input_value: '', // Internal value managed by saf if no `value` prop is passed
            ssf_result_list: null, //sima_address_field_result_list dom object
            open_result_list: false,
            results: [],
            selected_item: null,
            result_list_html: null,
            initial_text_search_value: "",
            initial_id_value: "",
            text_search_prev_value: "",
            id_prev_value: "",
            can_show_list: true, //dodatna provera prikaza rezultata zbog asinhrone pretrage (kada prodje fokus na drugo polje, on ipak prikaze listu zbog asinhronog poziva)
            ssf_bus: new Vue(),
            stop_async_render: false //za stopiranje renderovanja result liste kada se okine blur nad poljem a ajax poziv se zavrsi kasnije
        };
    },
    computed: {
        scope() {
            return {
                events: {
                    'keydown': this.onKeyDown,
                    'keyup': this.onKeyUp,
                    'blur': this.onBlur,
                    'focus': this.onFocus
                }
            };
        },
        open_dialog(){
            if(this.settings.hasOwnProperty('open_dialog'))
            {
                return this.settings.open_dialog;
            }
            return true;
        },
        update_label(){
            if(
                    (!this.value.hasOwnProperty('label') || this.value.label === "") && 
                    typeof this.value.id !== 'undefined' && this.value.id !== ""
                )
            {
                return true;
            }
            return false;
        },
        update_label_on_select(){
            if(typeof this.settings.display_label !== 'undefined' && this.selected_item)
            {
                return true;
            }
            return false;
        },
        vue_model(){
            if(this.update_label || this.update_label_on_select)
            {
                return sima.vue.shot.getters.model(this.model_name+'_'+this.value.id);
            }
            return null;
        },
        display_label(){
            if(this.vue_model)
            {
                if(this.settings.display_label)
                {
                    return this.vue_model[this.settings.display_label];
                }
                return this.vue_model.DisplayName;
            }
            return this.search;
        }
    },
    watch: {
        value(new_val, old_val){
            this.updateField(new_val);
        },
        display_label(new_val, old_val){
            if(this.update_label || this.update_label_on_select)
            {
                this.updateField(new_val, this.value.id);
            }
        }
    },
    created() {
        window.addEventListener('mousedown', this.onMouseDown);
    },
    mounted: function () {
        var _this = this;
        this.result_list_html = this.$refs.safResultListComponent.$el;
        var _result_list_html = this.result_list_html;
        
        this.ssf_result_list = document.body.appendChild(this.result_list_html);

        $(this.$el).on('destroyed', function () {
            _this.$destroy();
            _result_list_html.remove();
        });
        this.init();
    },
    destroyed() {
        if(this.ssf_result_list)
        {
            this.ssf_result_list.remove();
        }
        window.removeEventListener('mousedown', this.onMouseDown);
    },
    methods: {
        init(){
            this.updateField(this.value);          
            this.initial_id_value = this.value.id;//inicijalno postavljanje id bilo kog modela
            this.text_search_prev_value = this.search;
            this.initial_text_search_value = this.search;
          
            this.ssf_bus.$on('bus_event_close_result_list', (data) => {
                if(this.ssf_result_list)
                {
                    this.open_result_list = false;
                }
            });
        },
        searchText() {
            var _this = this;
            this.results = [];
            
            var globalTimeout = this.search_timeout;
            if (globalTimeout !== null) {
                clearTimeout(globalTimeout);
                this.search_timeout = globalTimeout;
            }
            this.search_timeout = setTimeout(function () {
                if (_this.search.trim() !== '' && _this.search.length >= _this.search_min_char) 
                {
                    sima.ajax.get('base/model/simaSearchField', {
                        get_params: {
                            model_name: _this.model_name,
                            display_label: _this.settings.display_label || 'DisplayName'
                        },
                        data: {
                            search_text: _this.search.replace(/\,/g, ''),
                            model_filter: _this.model_filter
                        },
                        async: true,
                        success_function: function (response) {
                            _this.results = response.result;
                        }
                    });
                }
                    _this.renderResults();
            }, 500);
        },
        renderResults() {
            if(!this.stop_async_render)
            {
                var address_field_position = this.$el.getBoundingClientRect();
                this.ssf_result_list.style.top = address_field_position.top + 18 + "px";
                this.ssf_result_list.style.left = address_field_position.left + "px";        
                this.open_result_list = true;
            }
        },
        onKeyDown(e) {
            this.text_search_prev_value = e.target.value;
        },
        onKeyUp(e) {
            var _this = this;
            
            if(e.keyCode === 27)//esc
            {
                if(this.ssf_result_list && this.open_result_list)
                {
                    this.open_result_list = false;
                    e.stopPropagation();
                    e.preventDefault();
                }
                e.target.blur();
            }
            else if(e.keyCode === 9)//tab
            {
                
            }
            else if(e.keyCode === 13)//enter
            {
                if(this.ssf_result_list && this.open_result_list)
                {
                    var _this = this;
                    //ako se nakon pretrage brzo izabere stavka i pritisne enter, zbog setTimeout kasnije ce se zvrsiti emitovanje inputa koji ce pogresno da
                    //update-uje v-model, vazi samo ako je definisano text_field ponasanje
                    setTimeout(function () {
                        _this.ssf_bus.$emit('bus_event_enter', {});
                    }, _this.settings.text_field ? 500:0);
                    e.stopPropagation();
                    e.preventDefault();
                }
            }
            else if(this.validKeyCodes(e.keyCode, e.ctrlKey, e.shiftKey))
            {
                this.selected_item = null;
                this.searchText();
                this.$emit('input', {id: "", label: e.target.value});
            }
        },
        onMouseDown(e){
            if(!e.target.classList.contains("ssf-search-field"))
            {
                this.onClickOutside(e);
            }
        },
        onBlur(e) {
            if(e.relatedTarget)
            {
                this.open_result_list = false;
                this.stop_async_render = true; 
            }
            if(this.input_value === "" && !this.settings.text_field)
            {
                this.updateField("", "");
            }
        },
        onFocus(e) {
            this.stop_async_render = false;
            this.open_result_list = true;
            this.searchText();
        },
        onInput(e) {
            //this.$emit('input', {id: "", label: ""});
        },
        onClickOutside: function (e) {
            if(this.ssf_result_list)
            {
                if (
                        !e.target.closest(".sima-search-field") && !e.target.closest(".sima-search-field-result-list") && 
                        !this.isMe(e.target)
                    ) 
                {
                    this.open_result_list = false;
                    this.$emit('clickOutside');
                }
            }
        },
        selectedItem(i) {
            var label = this.results[i].DisplayName;
            var id = this.results[i].id;
            this.updateField(label, id);
            if(this.initial_id_value !== this.input_value || this.initial_text_search_value !== this.search)
            {
                this.$emit('input', {id: id, label: label});
                this.selected_item = this.results[i];
            }
            this.open_result_list = false;
        },
        openDialog(){
            sima.dialog.openActionInDialogAsync('guitable', {
                get_params: {
                    label: this.settings.dialog_label || "",
                    settings: {
                        model: this.model_name,
                        add_button: true
                    }
                }
            });
        },
        updateField(label, id) {
            if(typeof label === "object")
            {
                this.updateFieldFromVModel(label);
            }
            else
            {
                this.search = label;
                this.input_value = id;
            }
        },
        updateFieldFromVModel(v_model){
            var label = "";
            var id = v_model.id;
            if(v_model.hasOwnProperty('label'))
            {
                label = v_model.label;
            }
            this.search = label;
            this.input_value = id;
        },
        hasSearchTextChanged(){
            return this.text_search_prev_value !== this.search || false;
        },
        isMe(el){
            var _el = null;
            if(el.tagName === "INPUT")
            {
                _el = el.parentElement.querySelector('input[type="hidden"]');
            }
            return (_el && _el.name && (_el.name === this.$attrs.name)) || false;
        },
        validKeyCodes(keyCode, ctrl, shift){
            if(
                (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (ctrl === false && (keyCode >= 65 && keyCode <= 90)) || 
                shift || 
                keyCode === 46 || keyCode === 8 ||
                (ctrl === true && (keyCode === 86 || keyCode === 88 || keyCode === 90)) //paste,cut,undo
            )
            {
                return  true;
            }
            return false;
        }
    }
});