<script type="text/x-template" id="sima-search-field">
    <div class="sima-search-field" v-bind:class="{'_disabled': settings.disabled}" v-click-outside="onClickOutside">
        <input
            class="ssf-search-field"
            v-bind:placeholder="$attrs.placeholder"
            v-model="search"
            v-on="scope.events"
            ref="searchInptuField"
            v-bind:disabled="settings.disabled"
        />
        <span class="sima-icon _search_form _16" 
            v-if="open_dialog"
            @click="openDialog">
        </span>
        <input type="hidden" v-bind:name="$attrs.name" v-bind:value="input_value"/>
        
        <sima-search-field-result-list ref="safResultListComponent"             
            v-show="open_result_list"
            v-bind:results="results"
            v-bind:selected_item="selected_item"
            v-bind:search="search"
            v-bind:ssf_bus="ssf_bus"
            v-bind:model_name="model_name"
            @selectedItem="selectedItem">
        </sima-search-field-result-list>
        
    </div>
</script>