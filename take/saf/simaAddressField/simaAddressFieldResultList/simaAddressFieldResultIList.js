
/* global Vue, sima */
Vue.component('sima-address-field-result-list', {
    template: '#sima-address-field-result-list',
    props: {
        results: {
            type: Array,
            default() { return []; }
        },
        street_model: {
            validator: sima.vue.ProxyValidator
        },
        search: { type: String, default: '' },
        selected_item: { type: Object, default: null }, //Selektovana adresa
        show_add_new_item: { type: Boolean, default: false },//Prikaz elementa za otvaranje forme
        saf_bus: {type: Object}
    },
    data: function () {
        return {
            show_form: false,
            search_params: {}, //cuva parametre za odgovarajuce modele koje izvlaci iz 'search' propa
            street_number: "",
            floor: "",
            door_number: "",
            street_number_label: sima.translate('GeoNumber'),
            floor_label: sima.translate('GeoFloor'),
            door_number_label: sima.translate('GeoAppartmentNumber'),
            description: "",
            Street: {
                model: {},
                model_filter: {},
                settings: {
                    display_label: 'name',
                    open_dialog: false,
                    text_field: true
                }
            },
            PostalCode: {
                model: {},
                model_filter: {},
                settings: {
                    dialog_label: sima.translate('GeoPostalCode'),
                    display_label: 'code'
                }
            },
            City: {
                model: {},
                model_filter: {},
                selected_id: "",
                settings: {
                    dialog_label: sima.translate('GeoCity'),
                    display_label: 'name'
                }
            },
            Municipality: {
                model: {},
                model_filter: {},
                settings: {
                    dialog_label: sima.translate('GeoMunicipality'),
                    display_label: 'name',
                    disabled: true
                }
            },
            Country: {
                model: {},
                model_filter: {},
                settings: {
                    dialog_label: sima.translate('GeoCountry'),
                    display_label: 'name',
                    disabled: true
                }
            },
            // navigacija za selektovanje stavke u listi rezultata. -1 deselect, 0 add_new_item/addNewAddress/dodavanje nove stavke, 1> stavke liste rezultata
            key_navigation_position: -1, 
            add_new_display: ""
        };
    },
    computed: {
        can_save(){
            if(
                    this.street_number.trim() !== "" &&
                    (this.Street.model.id || this.Street.model.label) &&
                    (this.PostalCode.model.id) &&
                    (this.City.model.id) &&
                    (this.Country.model.id)
                )
            {
                return true;
            }
            return false;
        },
        street_name(){
            if(this.street_model && this.street_model.name)
            {
                return this.street_model.name;
            }
        },
        city(){
            var city = null;
            console.log('city');
            if(this.street_model && this.street_model.city)
            {
                console.log('city vue model');
                city = this.street_model.city;
            }
            if(this.City.selected_id !== "")
            {
                city = sima.vue.shot.getters.model('City_'+this.City.selected_id);
            }
            return city;
        },
        postalCode(){
            var street_postal_code = null;
            var city_postal_code = null;
            if(this.street_model && this.street_model.postal_code)
            {
                street_postal_code = this.street_model.postal_code;
            }
            if (this.city)
            {
                if(this.city.default_postal_code)
                {
                    city_postal_code = this.city.default_postal_code;
                }
                else if(this.city.postal_codes)
                {
                    city_postal_code = this.city.postal_codes[0];
                }
            }
            
            if(this.City.selected_id)
            {
                return city_postal_code;
            }
            return street_postal_code || city_postal_code;
        },
        municipality(){
            if(this.city && this.city.municipality)
            {
                return this.city.municipality;
            }
            return null;
        },
        country(){
            if(this.city && this.city.country)
            {
                return this.city.country;
            }
            return null;
        }
    },
    watch: {
        search(new_val, old_val) {
            if(!this.selected_item && (new_val !== old_val))
            {
                this.show_form = false;
                this.key_navigation_position = -1; //deselect
            }
            this.add_new_display = this.generateAddNewItemString();
        },
        street_model(){
            if(this.street_model)
            {
                this.key_navigation_position = -1; //deselect
                this.show_form = true;
                this.focusStreetNumber();
            }
        },
        street_name(){
            var model = {};
            if(this.street_name)
            {
                model = {
                    id: this.street_model.id,
                    label: this.street_name
                };
                
                this.Street.model = model;
            }
        },
        city(){
            var model = {};
            if(this.city)
            {
                model = {
                    id: this.city.id,
                    label: this.city.name
                };
            } 
            this.City.model = model;
            if(this.City.selected_id !== "")
            {
                //this.Street.model = {id: "", label: ""};
                this.Street.model.id = "";
            }
        },
        postalCode(){
            var model = {};
            if(this.postalCode)
            {
                model = {
                    id: this.postalCode.id,
                    label: this.postalCode.code
                };
            }
            this.PostalCode.model = model;
        },
        municipality(){
            var model = {};
            if(this.municipality)
            {
                model = {
                    id: this.municipality.id,
                    label: this.municipality.name
                };
            }
            this.Municipality.model = model;
        },
        country(){
            var model = {};
            if(this.country)
            {
                model = {
                    id: this.country.id,
                    label: this.country.name
                };
            }
            this.Country.model = model;
        }
    },
    created() {
        window.addEventListener('keyup', this.onKeyUp);
        window.addEventListener('keydown', this.onKeyDown);
    },
    destroyed() {
        window.removeEventListener('keyup', this.onKeyUp);
        window.removeEventListener('keydown', this.onKeyDown);
    },
    mounted: function () {
        var _this = this;

        $(this.$el).on('destroyed', function () {
            _this.$destroy();
        });
        
        this.init();
    },
    methods: {
        hits(text, i) {
            return text.replace(this.search, "<b>" + this.search + "</b>");
        },
        selectItem(i, id) {
            this.City.selected_id = "";
            if(!id)
            {
                id = this.results[i].id;
            }
            this.$emit('selectedItem', i, id);
        },
        init(){
            this.saf_bus.$on('bus_event_enter', (data) => {
                if(this.key_navigation_position > 0)
                {
                    this.selectItem(this.key_navigation_position-1);
                }
                else if(this.key_navigation_position === 0)
                {
                    this.key_navigation_position = -1;
                    this.$refs.add_new_item.classList.remove('active-item');
                    this.addNewAddress();
                }
            });
        },
        onKeyDown(e){
            //Izvrsavanje eventova kada je otvorena lista rezultata ili forma
            if(this.$el.style.display !== 'none')
            {
                //Izvrsavanje eventova samo za listu rezultata
                if(!this.show_form)
                {
                    if(e.keyCode === 38)//up
                    {
                        var add_new_item = this.$refs.add_new_item;
                        var ul_list = this.$refs.results;
                        if(this.key_navigation_position >= 1)
                        {
                            for (let item of ul_list.children) {
                                item.classList.remove('active-item');
                            }
                            this.key_navigation_position--;
                            if(this.key_navigation_position > 0)
                            {
                                var list_item = ul_list.children[this.key_navigation_position-1];
                                list_item.classList.add('active-item');
                                if(!this.isScrolledIntoView(list_item))
                                {
                                    list_item.scrollIntoView(false);
                                }
                            }
                            else if(this.key_navigation_position === 0)
                            {
                                add_new_item.classList.add('active-item');
                            }
                        }
                        else if(this.key_navigation_position === 0)
                        {
                            this.key_navigation_position--;
                            add_new_item.classList.remove('active-item');
                            this.saf_bus.$emit('bus_event_focus_field', e.target);
                        }
                        e.stopPropagation();
                        e.preventDefault();
                    }
                    else if(e.keyCode === 40)//down
                    {
                        var ul_list = this.$refs.results;
                        var add_new_item = this.$refs.add_new_item;
                        //focus/blur polja pretrage i selectovanje add_new_item
                        if(this.key_navigation_position === -1)
                        {
                            this.key_navigation_position++;
                            add_new_item.classList.add('active-item');
                            this.saf_bus.$emit('bus_event_blur_field', e.target);
                        }
                        else if(this.key_navigation_position < this.results.length)
                        {
                            add_new_item.classList.remove('active-item');
                            for (let item of ul_list.children) {
                                item.classList.remove('active-item');
                            }
                            this.key_navigation_position++;
                            var list_item = ul_list.children[this.key_navigation_position-1];
                            list_item.classList.add('active-item');
                            if(!this.isScrolledIntoView(list_item))
                            {
                                list_item.scrollIntoView(true);
                            }
                        }
                        e.stopPropagation();
                        e.preventDefault();
                    }
                    
                }
            }
        },
        onKeyUp(e){
            //Izvrsavanje eventova kada je otvorena lista rezultata ili forma
            if(this.$el.style.display !== 'none')
            {
                if(e.keyCode === 27)//esc
                {
                    if(e.target.tagName === "BODY")//ako je ESC nad poljem adresa onda zatvaramo listu rezultata
                    {
                        this.saf_bus.$emit('bus_event_close_result_list');
                    }
                    else
                    {
                        this.saf_bus.$emit('bus_event_blur_field', e.target);
                    }
                    
                }
                else if(e.keyCode === 13)//enter
                {
                    if(this.key_navigation_position > 0)
                    {
                        this.selectItem(this.key_navigation_position-1);
                        this.key_navigation_position = -1;
                    }
                    else if(this.key_navigation_position === 0)
                    {
                        this.key_navigation_position = -1;
                        this.$refs.add_new_item.classList.remove('active-item');
                        this.addNewAddress();
                    }
                }
                e.stopPropagation();
                e.preventDefault();
            }
        },
        cityClickOutside(){
            if(!this.City.model.id)
            {
                this.City.model = {};
            }
        },
        postalCodeClickOutside(){
            if(!this.PostalCode.model.id)
            {
                this.PostalCode.model = {};
            }
        },
        municipalityClickOutside(){
            if(!this.Municipality.model.id)
            {
                this.Municipality.model = {};
            }
        },
        countryClickOutside(){
            if(!this.Country.model.id)
            {
                this.Country.model = {};
            }
        },
        addNewAddress() {
            var _this = this;
            this.City.selected_id = "";
            this.resetParams();
            this.fillSearchParams();
            sima.ajax.get('geo/addressField/simaAddressFieldAddItem', {
                data: {
                    model_data: {
                        search_params: _this.search_params
                    }
                },
                async: true,
                success_function: function (response) {                       
                    /*Setovanje polja*/

                    //Ulica
                    var model_status_street = response.models_status.Street;
                    if (model_status_street && model_status_street.status === 'new') 
                    {
                        //Dodavanje nove stavke, definisanje ponasanje polja kao text field
                        _this.Street.model = {label: _this.search_params.street_name, id: ""};
                    }
                    else if (model_status_street && model_status_street.status === 'multiple') 
                    {
                        _this.Street.model = {label: _this.search_params.street_name};
                        //Postavljamo filter za pretragu stavke
                        _this.Street.model_filter = { name: _this.search_params.street_name };
                    }
                    else if(model_status_street && model_status_street.model_data  && model_status_street.status === 'select') 
                    {
                        //Selektujemo pronadjenu stavku
                        _this.Street.model = {id: model_status_street.model_data.id, label: model_status_street.model_data.name};
                    }                  

                    //Postanski borj
                    var model_status_postal_code = response.models_status.PostalCode;
                    if (model_status_postal_code && model_status_postal_code.status === 'new') 
                    {
                        //Dodavanje nove stavke, definisanje ponasanje polja kao text field
                        _this.PostalCode.model = {label: _this.search_params.postal_code, id: ""};
                    }
                    else if (model_status_postal_code && model_status_postal_code.status === 'multiple') 
                    {
                        //Postavljamo filter za pretragu stavke
                        _this.PostalCode.model_filter = { name: model_status_postal_code.search_text };
                    }
                    else if(model_status_postal_code && model_status_postal_code.model_data  && model_status_postal_code.status === 'select') 
                    {
                        //Selektujemo pronadjenu stavku
                        _this.PostalCode.model = {id: model_status_postal_code.model_data.id, label: model_status_postal_code.model_data.code};
                    }

                    //Grad
                    var model_status_city = response.models_status.City;
                    if (model_status_city && model_status_city.status === 'multiple') {
                        _this.City.model_filter = { name: model_status_city.search_text };
                    }
                    else if(model_status_city && model_status_city.status === 'new')
                    {  
                        _this.City.model = {label: _this.search_params.city_name, id: ""};
                    }
                    else if(model_status_city && model_status_city.status === 'select')
                    {
                        _this.City.model = {id: model_status_city.model_data.id, label: model_status_city.model_data.name};
                        if(model_status_city.model_data.postal_code)
                        {
                            _this.PostalCode.model = {
                                id: model_status_city.model_data.postal_code.id, 
                                label: model_status_city.model_data.postal_code.code
                            };
                        }
                        //Ako je selektovan grad i ako imamo vise ulica sa istim nazivom postavljamo model_filter ulice za naziv grada
                        if(model_status_street && model_status_street.status === 'multiple')
                        {
                            _this.Street.model_filter = { name: model_status_street.search_text, city:{ids: model_status_city.model_data.id} };

                        }
                    }
                        _this.floor = _this.search_params.floor;
                        _this.door_number = _this.search_params.door_number;

                    //Broj ulice
                    _this.street_number = _this.search_params.street_number;

                    //Opstina
                    var model_status_municipality = response.models_status.Municipality;
                    if (model_status_municipality && model_status_municipality.status === 'multiple') {
                        _this.Municipality.model_filter = { name: model_status_municipality.search_text };
                    }
                    else if(model_status_municipality && model_status_municipality.model_data)//new
                    {
                        _this.Municipality.model = {id: model_status_municipality.model_data.id, label: model_status_municipality.model_data.name};
                    }

                    //Drzava
                    var model_status_country = response.models_status.Country;
                    if (model_status_country && model_status_country.status === 'multiple') {
                        _this.Country.model_filter = { name: model_status_country.search_text };
                    }
                    else if(model_status_country && model_status_country.model_data)
                    {
                        _this.Country.model = {id: model_status_country.model_data.id, label: model_status_country.model_data.name};
                        _this.PostalCode.model_filter = {country:{ids:model_status_country.model_data.id}};
                    }
                }
            });
            this.show_form = true;
            this.focusStreetNumber();
        },
        focusStreetNumber(){
            var _this = this;
            setTimeout(function(){
                if(_this.$refs.streetNumber)
                {
                    _this.$refs.streetNumber.focus();
                }
            },500);
        },
        saveNewItem(){
            var _this = this;
            var address_data = {};
            
            //Ulica
            var street = this.Street.model;
            address_data.street_number = this.street_number;
            address_data.floor = this.floor;
            address_data.door_number = this.door_number;
            address_data.description = this.description;
            if (street.id)
            {
                address_data.street_id = street.id;
            }
            else if(street.label)
            {
                address_data.street = {
                    name: street.label
                };
            }
            
            //Grad
            var city = this.City.model;
            if (city.id)
            {
                address_data.city_id = city.id;
            }
            else if(city.label)
            {
                address_data.city = {
                    name: city.label,
                    postal_code: {},
                    country_id: "",
                    default_postal_code_id: "",
                    municipality_id: this.Municipality.model.id ? this.Municipality.model.id : ""
                };
            }
                       
            //Drzava
            var country = this.Country.model;
            if (country.id && address_data.city)//ako je izabrana drzava i uneto novo naselje
            {
                address_data.city.country_id = country.id;
                address_data.city.postal_code.country_id = country.id;
            }
            
            //Postanski broj
            var postal_code = this.PostalCode.model;
            if (postal_code.id)
            {
                address_data.postal_code_id = postal_code.id;
            }
            else if(!postal_code.id && postal_code.label && country.id) //Ako je unet novi postanski on se kasnije u kontroleru vezuje za ulicu
            {
                address_data.postal_code = {
                    code: postal_code.label,
                    country_id: country.id
                };
            }
            console.log(address_data);
            sima.ajax.get('geo/addressField/simaAddressFieldSaveItem', {
                data: {
                    address_data: address_data
                },
                async: true,
                success_function: function (response) {
                    if(response.address_result)
                    {
                        _this.show_form = false;
                        _this.$emit('savedAddressForm', response.address_result);
                    }
                }
            });
        },
        resetParams(){
            this.search_params = {};
            this.street_number = "";
            this.floor = "";
            this.door_number = "";
            this.description = "";
            
            this.Street.model = {};
            this.Street.model_filter = {};
            this.Street.search_text = "";
            
            this.City.model = {};
            this.City.model_filter = {};
            this.City.search_text = "";
            this.City.selected_id = "";
                      
            this.PostalCode.model = {};
            this.PostalCode.model_filter = {};
            this.PostalCode.search_text = "";
            
            this.Municipality.model = {};
            this.Municipality.model_filter = {};
            this.Municipality.search_text = "";
            
            this.Country.model = {};
            this.Country.model_filter = {};
            this.Country.search_text = "";
        },
        cityChange(e){
            if(e.id)
            {
                this.City.selected_id = e.id;
                this.Street.model_filter = {city:{ids:e.id}};
            }
            else
            {
                this.City.selected_id = "";
            }
        },
        onClickOutside: function (e) {
            var _this = this;
            if (!_this.$el.contains(e.target) && _this.$el !== e.target) {
                _this.$el.remove();
            }
        },
        extractDoorNumber(string){
            var result = {
                street_name: string,
                street_number: "",
                floor: "",
                door_number: ""
            };
            
            var n = string.split(" ");
            if (n.length > 1)
            {
                var last_element = n[n.length - 1];
                
                if(last_element.match(/^[\/0-9]*$/)) //ako je samo broj ili kosa crta
                {
                    var last_element_parts = last_element.split('/');
                    if (typeof last_element_parts[0] !== 'undefined')
                    {
                        result.street_number = last_element_parts[0];
                    }
                    if (typeof last_element_parts[1] !== 'undefined')
                    {
                        result.floor = last_element_parts[1];
                    }
                    if (typeof last_element_parts[2] !== 'undefined')
                    {
                        result.door_number = last_element_parts[2];
                    }
                    
                    n.pop();
                    result.street_name = n.join(" ");
                }
            }

            return result;
        },
        extractPostalCode(string){
            var result = {
                city_name: string,
                postal_code: ""
            };
            var n = string.split(" ");
            if(n && n.length > 0)
            {
                var postal_code = n[0];
                if(!isNaN(postal_code))
                {
                    n.shift();//izbacujemo prvi element
                    result.city_name = n.join(" ");
                    result.postal_code = postal_code;
                }
            }
            return result;
        },
        generateAddNewItemString(){           
            this.fillSearchParams();
            
            if(this.search_params.street_name)
            {            
                var display_string = sima.translate('GeoAddStreet')+" ("+this.search_params.street_name;
                
                display_string += ' ' + this.search_params.street_number;
                if (!sima.isEmpty(this.search_params.floor))
                {
                    display_string += '/' + this.search_params.floor;
                }
                if (!sima.isEmpty(this.search_params.door_number))
                {
                    display_string += '/' + this.search_params.door_number;
                }

                display_string += ')';
                
                if(this.search_params.city_name)
                {
                    display_string += ", "+sima.translate('GeoCity')+" ("+this.search_params.city_name+") ";
                }
                if(this.search_params.municiplaity_name)
                {
                    display_string += ", "+sima.translate('GeoMunicipality')+" ("+this.search_params.municiplaity_name+")";
                }
                return display_string;
            }
            else
            {
                return this.search;
            }
        },
        fillSearchParams(){
            if(this.search.trim() !== "")
            {
                var search_params_array = this.search.split(",").map(item => item.trim());
                var extracted_street = this.extractDoorNumber(search_params_array[0]);
                var city_param_string = search_params_array[1] ? search_params_array[1] : "";
                var extracted_city = this.extractPostalCode(city_param_string);
                this.search_params.street_name = extracted_street.street_name;
                this.search_params.street_number = extracted_street.street_number;
                this.search_params.floor = extracted_street.floor;
                this.search_params.door_number = extracted_street.door_number;
                this.search_params.city_name = extracted_city.city_name;
                this.search_params.postal_code = extracted_city.postal_code;
                this.search_params.municiplaity_name = search_params_array[2] ? search_params_array[2] : "";
                this.search_params.municiplaity_id = this.Municipality.model.id;
                this.search_params.country_name = search_params_array[3] ? search_params_array[3] : "";
                this.search_params.country_id = this.Country.model.id;
            }
            else
            {
                this.search_params = {};
            }
        },
        isScrolledIntoView(el) { //Proverava da li je element vidljiv
            var rect = el.getBoundingClientRect();
            return (
                    (rect.top >= 0) && (rect.bottom <= el.parentElement.parentElement.getBoundingClientRect().bottom) && 
                    (rect.bottom >= 0) && (rect.top >= el.parentElement.parentElement.getBoundingClientRect().top)
            );
        }
    }
});