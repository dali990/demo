<script type="text/x-template" id="sima-address-field">
    <div class="sima-address-field" v-click-outside="onClickOutside" v-bind:id="$attrs.id">
        <input
            class="saf-search-field"
            v-bind:placeholder="$attrs.placeholder"
            v-model="search"
            v-on="scope.events"
            ref="searchInptuField"
        />
        <input class="sima-address-field-input-value" type="hidden" v-bind:name="$attrs.name" v-bind:value="input_value" ref="hiddenInputValue"/>
        
        <sima-address-field-result-list ref="safResultListComponent"             
            v-show="open_result_list"
            v-bind:results="results"
            v-bind:selected_item="selected_item"
            v-bind:search="search"
            v-bind:show_add_new_item="show_add_new_item"
            v-bind:saf_bus="saf_bus"
            v-bind:street_model="street_model"
            @savedAddressForm="savedAddressForm"
            @selectedItem="selectedItem">
        </sima-address-field-result-list>
        
    </div>
</script>