<?php

return [
    'legal' => [
        'order' => 20,
        'visible' => Yii::app()->user->checkAccess('menuShowLegal'),
        'label' => Yii::t('LegalModule.Configuration', 'Legal'),
        'icon' => 'fas fa-stamp',
        'subitems' => [
            [
                'label' => 'Ugovori',
                'visible' => Yii::app()->user->checkAccess('menuShowLegalContracts'),
                'url' => 'javascript:sima.mainTabs.openNewTab("guitable", "legal_11");',
                'action' => ['guitable', 'legal_11']
            ],
            [
                'label' => Yii::t('LegalModule.Legal', 'Employees'),
                'visible' => Yii::app()->user->checkAccess('menuShowLegalEmployees'),
                'subitems' => [
                    [
                        'label' => Yii::t('LegalModule.Legal', 'Listing'),
                        'visible' => Yii::app()->user->checkAccess('menuShowLegalEmployeesWorkContracts'),
                        'url' => 'javascript:sima.mainTabs.openNewTab("legal/legal/employees");',
                        'action' => 'legal/legal/employees',
                        'subitems' => [
                            [
                                'label' => 'Platni razredi',                                        
                                'url' => 'javascript:sima.mainTabs.openNewTab("guitable", "HR_13");',
                                'action' => ['guitable', 'HR_13']
                            ]
                        ]
                    ],
                    [
                        'label' => Yii::t('HRModule.Absence', 'Absences'),
                        'visible' => Yii::app()->user->checkAccess('menuShowLegalEmployeesAbsencesOverview'),
                        'url' => 'javascript:sima.mainTabs.openNewTab("HR/absence/absencesOverview");',
                        'action' => 'HR/absence/absencesOverview'
                    ],
                    [
                        'label' => Yii::t('LegalModule.Legal', 'OrganizationScheme'),
                        'visible' => Yii::app()->user->checkAccess('menuShowLegalEmployeesOrganizationScheme'),
                        'url' => 'javascript:sima.mainTabs.openNewTab("organizationScheme/index");',
                        'action' => 'organizationScheme/index'
                    ],
                    [
                        'label' => 'Statistika',
                        'visible' => Yii::app()->user->checkAccess('menuShowLegalEmployeesStatistic'),
                        'url' => 'javascript:sima.mainTabs.openNewTab("legal/legal/statistic");',
                        'action' => 'legal/legal/statistic'
                    ],
                    [
                        'label' => 'Reference',
                        'visible' => Yii::app()->user->checkAccess('menuShowLegalEmployeesReference'),
                        'url' => 'javascript:sima.mainTabs.openNewTab("legal/legal/references");',
                        'action' => 'legal/legal/references'
                    ],
                    [
                        'label'=>'Licence',
                        'visible' => Yii::app()->user->checkAccess('menuShowLegalEmployeesLicence'),
                        'url'=>'javascript:sima.mainTabs.openNewTab("HR/workLicense/index");',
                        'action' => 'HR/workLicense/index'
                    ],
                    [
                        'label' => Yii::t('MainMenu', 'IntroductionsIntoBusinessProgram'),
                        'visible' => Yii::app()->user->checkAccess('menuShowHRIntroductionsIntoBusinessProgram'),
                        'url' => 'javascript:sima.mainTabs.openNewTab("guitable", "HR_3");',
                        'action' => ['guitable', 'HR_3']
                    ],
                    [
                        'label' => Yii::t('MainMenu', 'WorkHoursOverview'),
                        'visible' => Yii::app()->user->checkAccess('PersonActivityOverview'),
                        'url' => "javascript:sima.mainTabs.openNewTab('legal/legal/personActivityOverview');",
                        'action' => 'legal/legal/personActivityOverview'
                    ]
                ]                                    
            ],
            [
                'label' => 'Konkursi za posao',
                'visible' => Yii::app()->user->checkAccess('menuShowLegalWorkPositionCompetition'),
                'subitems' => [
                    [
                        'label' => 'Konkursi',
                        'visible' => Yii::app()->user->checkAccess('menuShowLegalWorkPositionCompetitionGrades'),
                        'url' => 'javascript:sima.mainTabs.openNewTab("HR/WPCompetitions/index");',
                        'action' => 'HR/WPCompetitions/index'
                    ],
                    [
                        'label' => 'Kandidati',
                        'visible' => Yii::app()->user->checkAccess('menuShowLegalWorkPositionCompetitionCandidates'),
                        'url' => 'javascript:sima.mainTabs.openNewTab("HR/WPCompetitions/candidates");',
                        'action' => 'HR/WPCompetitions/candidates'
                    ]
                ]
            ],
            [
                'label' => 'Evd.Bezbednosti',
                'visible' => Yii::app()->user->checkAccess('menuShowLegalSafeRecord'),
                'url'=>'javascript:sima.mainTabs.openNewTab("HR/safeRecord/index");',
                'action' => 'HR/safeRecord/index'
            ],
            [
                'label' => 'Velike licence',
                'visible' => Yii::app()->user->checkAccess('menuShowLegalCompanyLicence'),
                'subitems' => [
                    [
                        'label' => 'Spisak velikih licenci',
                        'url' => 'javascript:sima.mainTabs.openNewTab("guitable", "HR_4");',
                        'action' => ['guitable', 'HR_4']
                    ],
                    [
                        'label' => 'Velike licence po kompanijama',
                        'url' => 'javascript:sima.mainTabs.openNewTab("HR/WorkLicense/companyLicenseByCompanies");',
                        'action' => 'HR/WorkLicense/companyLicenseByCompanies'
                    ],
                    [
                        'label' => 'Rešenja velikih licenci',
                        'url' => 'javascript:sima.mainTabs.openNewTab("HR/WorkLicense/CompanyLicensesByDecisions");',
                        'action' => 'HR/WorkLicense/CompanyLicensesByDecisions'
                    ],
                    [
                        'label' => Yii::t('HRModule.CompanyLicense', 'CompanyLicenseDecisionDisagreementsView'),
                        'url' => 'javascript:sima.mainTabs.openNewTab("HR/WorkLicense/CompanyLicenseToCompanyDifferences");',
                        'action' => 'HR/WorkLicense/CompanyLicenseToCompanyDifferences'
                    ],
                    [
                        'label' => 'Osobe koje su nosioci velike licence a nemaju godišnju kvotu od 15 bodova',
                        'url' => 'javascript:sima.mainTabs.openNewTab("guitable", "HR_5");',
                        'action' => ['guitable', 'HR_5']
                    ],
                    [
                        'label' => Yii::t('HRModule.CompanyLicense', 'CompanyLicensesByCompanies2'),
                        'url' => 'javascript:sima.mainTabs.openNewTab("HR/WorkLicense/companyLicensesByCompaniesView");',
                        'action' => 'HR/WorkLicense/companyLicensesByCompaniesView'
                    ]
                ]
            ],
            [
                'label' => 'Šifarnici',
                'visible' => Yii::app()->user->checkAccess('menuShowLegalConfig'),
                'subitems' => [
                    [
                        'label' => 'Pravne godine',
                        'url' => 'javascript:sima.mainTabs.openNewTab("guitable", "HR_6");',
                        'action' => ['guitable', 'HR_6']
                    ],
                    [
                        'label' => 'Skole i smerovi',
                        'url' => 'javascript:sima.mainTabs.openNewTab("HR/WPCompetitions/schools");',
                        'action' => 'HR/WPCompetitions/schools'
                    ],
                    [
                        'label' => Yii::t('MainMenu', 'EducationTitles'),
                        'url' => 'javascript:sima.mainTabs.openNewTab("guitable", "legal_9");',
                        'action' => ['guitable', 'legal_9']
                    ],
                    [
                        'label' => Yii::t('HRModule.Occupation', 'Occupations'),
                        'url' => 'javascript:sima.mainTabs.openNewTab("guitable", "HR_15");',
                        'action' => ['guitable', 'HR_15']
                    ],
                    [
                        'label' => Yii::t('HRModule.QualificationLevel', 'QualificationLevels'),
                        'url' => 'javascript:sima.mainTabs.openNewTab("guitable", "HR_16");',
                        'action' => ['guitable', 'HR_16']
                    ]
                ],
            ],
            [
                'label' => Yii::t('MainMenu', 'NationalEvents'),
                'visible' => Yii::app()->user->checkAccess('menuShowNationalEvents'),
                'url' => 'javascript:sima.mainTabs.openNewTab("HR/nationalEvents/nationalEvents");',
                'action' => 'HR/nationalEvents/nationalEvents'
            ]
        ]
    ],
    'legal_records' => [
        'order' => 50,
        'visible' => Yii::app()->user->checkAccess('menuShowRecords'),
        'label' => Yii::t('MainMenu', 'Records'),
        'icon' => 'fa fa-table',
        'subitems' => [
            array(
                'label' => Yii::t('LegalModule.Filing', 'FilingBook'),
                'url' => 'javascript:sima.mainTabs.openNewTab("legal/filingBook");',
                'action' => 'legal/filingBook',
                'visible' => Yii::app()->user->checkAccess('menuShowRecords_filingBook')
            ),
            array(
                'label' => Yii::t('BaseModule.Common', 'Themes').' | '.Yii::t('BaseModule.Common', 'Jobs'),
                'visible' => Yii::app()->user->checkAccess('menuShowRecords_themes'),
                'url' => 'javascript:sima.mainTabs.openNewTab("guitable", "base_7");',
                'action' => ['guitable', 'base_7'],
                'subitems' => [
                    [
                        'label' => Yii::t('BaseModule.Common', 'Themes').' | '.Yii::t('BaseModule.Common', 'Jobs'),
                        'url' => 'javascript:sima.mainTabs.openNewTab("guitable", "base_7");',
                        'action' => ['guitable', 'base_7'],
                        'visible' => Yii::app()->configManager->get('base.develop.use_new_layout', false)
                    ],
                    [
                        'label' => 'Vrste poslova',
                        'url' => 'javascript:sima.mainTabs.openNewTab("guitable", "jobs_2");',
                        'visible' => Yii::app()->user->checkAccess('menuShowRecords_themes_types'),
                        'action' => ['guitable', 'jobs_2']
                    ],
                    [
                        'label' => 'Statusi ponuda',
                        'url' => 'javascript:sima.mainTabs.openNewTab("guitable", "legal_3");',
                        'visible' => Yii::app()->user->checkAccess('menuShowRecords_themes_types'),
                        'action' => ['guitable', 'legal_3']
                    ]
                ]
            ),
            array(
                'label' => 'Ponude',
                'visible' => Yii::app()->user->checkAccess('menuShowBids'),
                'subitems' => array(
                    array(
                        'label' => 'Tabela ponuda',
                        'visible' => Yii::app()->user->checkAccess('menuShowBids_table'),
                        'url' => 'javascript:sima.mainTabs.openNewTab("guitable", "legal_7");',
                        'action' => ['guitable', 'legal_7']
                    ),
                    array(
                        'label' => 'Potencijalne ponude',
                        'visible' => Yii::app()->user->checkAccess('menuShowPotentialBids_table'),
                        'url' => 'javascript:sima.mainTabs.openNewTab("legal/legal/potentialBidsView");',
                        'action' => 'legal/legal/potentialBidsView'
                    ),
                    array(
                        'label' => 'Tipovi dodatnih uslova za ponude',
                        'visible' => Yii::app()->user->checkAccess('menuShowPotentialBids_table'),
                        'url' => 'javascript:sima.mainTabs.openNewTab("guitable", "legal_6");',
                        'action' => ['guitable', 'legal_6']
                    ),
                    array(
                        'label' => 'Kvalifikacione Ponude',
                        'visible' => Yii::app()->user->checkAccess('menuShowBids_table'),
                        'url' => 'javascript:sima.mainTabs.openNewTab("guitable", "legal_5");',
                        'action' => ['guitable', 'legal_5']
                    ),
                    array(
                        'label' => 'Menice',
                        'visible' => Yii::app()->user->checkAccess('menuShowBids_table'),
                        'url' => 'javascript:sima.mainTabs.openNewTab("guitable", "accounting_2");',
                        'action' => ['guitable', 'accounting_2']
                    ),
                )
            ),
            array(
                'label' => 'Geografski objekti',
                'visible' => Yii::app()->user->checkAccess('menuShowRecords_GeoObjects'),
                'url' => 'javascript:sima.mainTabs.openNewTab("geo/objects/index");',
                'action' => 'geo/objects/index'
            ),
            [
                'label' => Yii::app()->hasModule('buildings')?Yii::t('BuildingsModule.MainMenu','BookOfProjects'):'Include module Buildings',
                'url' => 'javascript:sima.mainTabs.openNewTab("buildings/bookOfProjects");',
                'action' => 'buildings/bookOfProjects',
                'visible' => Yii::app()->user->checkAccess('menuShowRecords_book_of_projects')
            ],

            array(
                'label' => 'Narudžbine betona',
                'visible' => Yii::app()->hasModule('CF') && Yii::app()->user->checkAccess('menuShowRecords_concrete'),
                'url' => 'javascript:sima.mainTabs.openNewTab("CF/CF/index");',
                'action' => 'CF/CF/index',
                'subitems' => array(
                    array(
                        'label' => 'Narudžbine betona',
                        'url' => 'javascript:sima.mainTabs.openNewTab("CF/CF/index");',
                        'action' => 'CF/CF/index'
                    ),
                    [
                        'label' => 'Magacin',
                        'url' => 'javascript:sima.mainTabs.openNewTab("CF/CFWarehouse");',
                        'action' => 'CF/CFWarehouse'
                    ],
                    [
//                        'label' => Yii::t('CFModule.Warehouse', 'Warehouse2'),
                        'label' => 'Magacin 2',
                        'url' => 'javascript:sima.mainTabs.openNewTab("CF/CFWarehouse/warehouse2");',
                        'action' => 'CF/CFWarehouse/warehouse2'
                    ],
                    array(
                        'label' => 'Knjiga uzorkovanja',
                        'url' => 'javascript:sima.mainTabs.openNewTab("guitable", "CF_1");',
                        'action' => ['guitable', 'CF_1']
                    ),
                    array(
                        'label' => 'Knjiga betona',
                        'url' => 'javascript:sima.mainTabs.openNewTab("CF/cubes");',
                        'action' => 'CF/cubes'
                    ),
                    array(
                        'label' => 'Lomljenje kocki',
                        'url' => 'javascript:sima.mainTabs.openNewTab("guitable", "CF_2");',
                        'action' => ['guitable', 'CF_2']
                    ),
                    array(
                        'label' => 'Atesti',
                        'url' => 'javascript:sima.mainTabs.openNewTab("CF/CF/atests");',
                        'action' => 'CF/CF/atests'
                    ),
                    array(
                        'label' => 'Odobrenja za gradilista',
                        'url' => 'javascript:sima.mainTabs.openNewTab("CF/CF/managerIndex");',
                        'action' => 'CF/CF/managerIndex'
                    ),
                    array(
                        'label' => 'Recepti beton',
                        'url' => 'javascript:sima.mainTabs.openNewTab("guitable", "CF_0");',
                        'action' => ['guitable', 'CF_0']
                    ),
                    array(
                        'label' => 'Aditivi za beton',
                        'url' => 'javascript:sima.mainTabs.openNewTab("guitable", "CF_3");',
                        'action' => ['guitable', 'CF_3']
                    ),
                    array(
                        'label' => 'Cementi',
                        'url' => 'javascript:sima.mainTabs.openNewTab("guitable", "CF_4");',
                        'action' => ['guitable', 'CF_4']
                    ),
                    array(
                        'label' => 'Gradilista',
                        'url' => 'javascript:sima.mainTabs.openNewTab("base/BuildingSites");',
                        'action' => 'base/BuildingSites'
                    ),
                )
            ),
            array('label' => 'Osnovna sredstva',
                'visible' => Yii::app()->user->checkAccess('menuShowRecords_fixedAssets'),
                'url' => 'javascript:sima.mainTabs.openNewTab("fixedAssets/fixedAsset/fixedAssetsList");',
                'action' => 'fixedAssets/fixedAsset/fixedAssetsList',
                'subitems' => array(
                    array(
                        'label' => 'Spisak',
                        'url' => 'javascript:sima.mainTabs.openNewTab("fixedAssets/fixedAsset/fixedAssetsList");',
                        'action' => 'fixedAssets/fixedAsset/fixedAssetsList',
                    ),
                    [
                        'label' => 'Tipovi osnovnih sredstava',
                        'url' => 'javascript:sima.mainTabs.openNewTab("guitable", "fixedAssets_15");',
                        'action' => ['guitable', 'fixedAssets_15']
                    ],
                    array(
                        'label' => 'Vozila',
                        'url' => 'javascript:sima.mainTabs.openNewTab("guitable", "fixedAssets_14");',
                        'action' => ['guitable', 'fixedAssets_14'],
                        'subitems' => array(
                            array(
                                'label' => 'Tabela',
                                'url' => 'javascript:sima.mainTabs.openNewTab("guitable", "fixedAssets_14");',
                                'action' => ['guitable', 'fixedAssets_14']
                            ),
                            array(
                                'label' => 'Registracije',
                                'url' => 'javascript:sima.mainTabs.openNewTab("guitable", "fixedAssets_13");',
                                'action' => ['guitable', 'fixedAssets_13']
                            ),
                            array(
                                'label' => 'Tehničke kontrole',
                                'url' => 'javascript:sima.mainTabs.openNewTab("guitable", "fixedAssets_12");',
                                'action' => ['guitable', 'fixedAssets_12']
                            ),
                            array(
                                'label' => 'Sertifikati',
                                'url' => 'javascript:sima.mainTabs.openNewTab("guitable", "fixedAssets_10");',
                                'action' => ['guitable', 'fixedAssets_10']
                            ),
                            array(
                                'label' => 'Kreditne kartice',
                                'url' => 'javascript:sima.mainTabs.openNewTab("guitable", "fixedAssets_9");',
                                'action' => ['guitable', 'fixedAssets_9']
                            ),
                            array(
                                'label' => 'Servisi',
                                'url' => 'javascript:sima.mainTabs.openNewTab("guitable", "fixedAssets_8");',
                                'action' => ['guitable', 'fixedAssets_8']
                            ),
                            array(
                                'label' => 'Dodatni agregati',
                                'url' => 'javascript:sima.mainTabs.openNewTab("guitable", "fixedAssets_7");',
                                'action' => ['guitable', 'fixedAssets_7']
                            ),
                            array(
                                'label' => 'Registar popravki vozila',
                                'url' => 'javascript:sima.mainTabs.openNewTab("guitable", "fixedAssets_6");',
                                'action' => ['guitable', 'fixedAssets_6']
                            )
                        )
                    ),
                    array(
                        'label' => 'Putni nalozi',
//                        'visible' => Yii::app()->user->checkAccess('menuShowRecords_fixedAssets') && (Yii::app()->user->checkAccess('travelOrderMaster') || Yii::app()->user->checkAccess('travelOrderAdministrator')),
                        'url' => 'javascript:sima.mainTabs.openNewTab("fixedAssets/fixedAsset/travelOrders");',
                        'action' => 'fixedAssets/fixedAsset/travelOrders'
                    ),
                    array(
                        'label' => 'Spisak PP aparata',
                        'url' => 'javascript:sima.mainTabs.openNewTab("guitable", "fixedAssets_5");',
                        'action' => ['guitable', 'fixedAssets_5']
                    ),
                    array(
                        'label' => 'Računari',
                        'url' => 'javascript:sima.mainTabs.openNewTab("guitable", "fixedAssets_4");',
                        'action' => ['guitable', 'fixedAssets_4']
                    ),
                    array(
                        'label' => 'Zaduženja osnovnih sredstava',
                        'url' => 'javascript:sima.mainTabs.openNewTab("guitable", "fixedAssets_3");',
                        'action' => ['guitable', 'fixedAssets_3']
                    ),
                    array(
                        'label' => Yii::t('MainMenu', 'InventoryNumbers'),
                        'url' => 'javascript:sima.mainTabs.openNewTab("fixedAssets/fixedAsset/inventoryNumbers");',
                        'action' => 'fixedAssets/fixedAsset/inventoryNumbers'
                    ),
                    [
                        'label' => 'Popisi',
                        'url' => 'javascript:sima.mainTabs.openNewTab("fixedAssets/fixedAsset/inventoryLists");',
                        'action' => 'fixedAssets/fixedAsset/inventoryLists'
                    ],
                    [
                        'label' => Yii::t('BaseModule.Common', 'Merging'),
                        'visible' => Yii::app()->user->checkAccess('menuShowRecords_fixedAssets_merging'),
                        'url' => 'javascript:sima.mainTabs.openNewTab("fixedAssets/FAMerging");',
                        'action' => 'fixedAssets/FAMerging'
                    ],
                    [
                        'label' => 'Odluke o deaktiviranju osnovnih sredstava',
                        'visible' => Yii::app()->user->checkAccess('menuShowFixedAssetsDeactivationDecisions'),
                        'url' => 'javascript:sima.mainTabs.openNewTab("fixedAssets/fixedAsset/fixedAssetsDeactivationDecisions");',
                        'action' => 'fixedAssets/fixedAsset/fixedAssetsDeactivationDecisions'
                    ]
                )
            ),
            array(
                'label' => 'IKS poeni svih inžinjera',
                'visible' => Yii::app()->user->checkAccess('menuShowRecords_iks_points_review'),
                'url' => 'javascript:sima.mainTabs.openNewTab("HR/HR/iksPointsReview");',
                'action' => 'HR/HR/iksPointsReview'
            ),
            array(
                'label' => Yii::t('MainMenu', 'QRCodes'),
                'visible' => Yii::app()->user->checkAccess('menuShowRecords_QRcodes'),
                'url' => 'javascript:sima.mainTabs.openNewTab("guitable", "base_8");',
                'action' => ['guitable', 'base_8']
            ),
            [
                'label' => Yii::t('LegalModule.InsurancePolicy', 'InsurancePolicies'),
                'visible' => Yii::app()->user->checkAccess('menuShowRecords_insurancePolicies'),
                'url' => 'javascript:sima.mainTabs.openNewTab("guitable", "legal_27");',
                'action' => ['guitable', 'legal_27']
            ]
        ]
    ]
];