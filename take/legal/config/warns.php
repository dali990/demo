<?php

return [
    'WARN_EMPLOYEES_SLAVA_NOT_SET' => [
        'title' => Yii::t('MainMenu', 'EmployeesSlavaNotSet'),
        'onclick' => ['sima.mainTabs.openNewTab','guitable','legal_15'],        
        'countFunc' => function($user) {
            $cnt = 0;
            if (Yii::app()->authManager->checkAccess('HREmployeeSlava',$user->id))
            {
//                $cnt = Employee::model()->count(new SIMADbCriteria([
//                    'Model'=>'Employee',
//                    'model_filter'=>[
//                        'have_slava' => true,
//                        'filter_scopes'=>[
//                            'slavaIsNull'
//                        ]
//                    ]
//                ]));
                $cnt = Employee::model()->count([
                    'scopes' => [
                        'slavaIsNull'
                    ],
                    'model_filter'=>[
                        'have_slava' => true
                    ]
                ]);
            }
            
            return $cnt;
        }
    ],
    'WARN_EMPLOYEES_EXPIRING_INSURANCES' => [
        'title' => 'Broj osoba kojima ističe zdravstveno osiguranje',
        'onclick' => ['sima.mainTabs.openNewTab','guitable','legal_16'],        
        'countFunc' => function($user) {
            $cnt = 0;
            if (Yii::app()->authManager->checkAccess('LegalEmployeeExpiringInsuranceWarn',$user->id))
            {
                $criteria = new SIMADbCriteria([
                    'Model'=>'Employee',
                    'model_filter'=>[
                        'expiration_date_of_insurance'=>['<',SIMAHtml::UserToDbDate(date('Y-m-d', strtotime("+1 week")))]
                    ]
                ]);
                $cnt = Employee::model()->count($criteria);
            }
            
            return $cnt;
        }
    ],
    'WARN_INSUFFICIENT_DATA_EMPOLOYEES' => [
        'title' => Yii::t('MainMenu', 'InsuficientDataForEmployees'),
        'onclick' => ['sima.mainTabs.openNewTab','guitable','legal_17'],        
        'countFunc' => function($user) {
            $cnt = 0;
            if (Yii::app()->authManager->checkAccess('HRInsufficientDataForEmployees',$user->id))
            {
                $cnt = Employee::model()->withInsufficientDataForAccountingMonthsAndToday()->count();
            }
            
            return $cnt;
        }
    ],
    'WARN_INSUFFICIENT_DATA_MUNICIPALITIES' => [
        'title' => Yii::t('MainMenu', 'InsuficientDataForMunicipality'),
        'onclick' => ['sima.mainTabs.openNewTab','guitable','legal_18'],        
        'countFunc' => function($user) {
            $cnt = 0;
            if (Yii::app()->authManager->checkAccess('HRInsufficientDataForMunicipalities',$user->id))
            {
                $criteria = new SIMADbCriteria([
                    'Model' => 'Municipality',
                    'model_filter' => [
                        'code' => 'null'
                    ],
                ]);
                $cnt = Municipality::model()->count($criteria);
            }
            
            return $cnt;
        }
    ],
    'WARN_NEW_POTENTIAL_BIDS' => [
        'title' => 'Broj novih potencijalnih ponuda',
        'onclick' => ['sima.mainTabs.openNewTab','guitable','legal_20'],        
        'countFunc' => function($user) {
            $cnt = 0;
            if (Yii::app()->authManager->checkAccess('LegalNewPotentialBidsWarn',$user->id))
            {
                $cnt = PotentialBid::model()->count(new SIMADbCriteria([
                    'Model'=>'PotentialBid',
                    'model_filter'=>[
                        'accepted_filter' => PotentialBid::$NEW
                    ]
                ]));
            }
            
            return $cnt;
        }
    ],
    'WARN_NEW_BIDS' => [
        'title' => 'Broj novih ponuda',
        'onclick' => ['sima.mainTabs.openNewTab','guitable','legal_21'],        
        'countFunc' => function($user) {
            $cnt = 0;
            if (Yii::app()->authManager->checkAccess('LegalNewBidsWarn',$user->id))
            {
                $cnt = Bid::model()->count(new SIMADbCriteria([
                    'Model'=>'Bid',
                    'model_filter'=>[
                        'bid_status' => Bid::$NEW
                    ]
                ]));
            }
            
            return $cnt;
        }
    ],
    'WARN_NOT_CONFIRMED_EMPLOYEES_SICK_LEAVES_AND_FREE_DAYS' => [
        'title' => Yii::t('LegalModule.Warns', 'NotConfirmedEmployeesSickLeavesAndFreeDays'),
        'onclick' => ['sima.mainTabs.openNewTab', 'guitable', 'legal_25'],
        'countFunc' => function($user) {
            $cnt = 0;
            if (Yii::app()->hasModule('HR') && Yii::app()->authManager->checkAccess('LegalShowWarnForNotConfirmedEmployeesSickLeavesAndFreeDays', $user->id))
            {
                $cnt = Absence::model()->count(new SIMADbCriteria([
                    'Model'=>'Absence',
                    'model_filter'=>[
                        'filter_scopes' => ['typeIn' => [[Absence::$SICK_LEAVE, Absence::$FREE_DAYS]]],
                        'confirmed' => 'null'
                    ]
                ]));
            }
            
            return $cnt;
        }
    ],
    'WARN_FILE_TRANSFERS_IN' => [
        'title' => Yii::t('FilesModule.Warns', 'warnFileTransfersInTitle'),
        'onclick' => ['sima.dialog.openModel','legal/filingTransfer/fileTransfers','IN'],
        'countFunc' => function($user) {
            return $user->file_transfers_in_count;
        },
        'forModel' => [
            'model' => 'File',
            'title' => Yii::t('FilesModule.Warns', 'warnFileTransfersInByModelTitle'),
            'description' => Yii::t('FilesModule.Warns', 'warnFileTransfersInByModelDesc'),
            'countFunc' => function (User $user, File $model){
                $cnt = FileTransfer::model()->count(new SIMADbCriteria([
                    'Model'=>'FileTransfer',
                    'model_filter'=>[
                        'file'=>['ids'=>$model->id],
                        'recipient'=>['ids'=>$user->id]
                    ]
                ]));

                return $cnt;
            },
            'checkResolveFunc' => function (User $user, $model) {
                $status = 'OK';
                $message = Yii::t('FilesModule.Warns', 'warnFileTransfersInByModelCheckResolve', [
                    '{file_display_name}' => $model->DisplayName
                ]);
                
                return [
                    'status' => $status,
                    'message' => $message
                ];
            },
            'resolveFunc' => function (User $user, $model) {
                $ftin = FileTransfer::model()->findByAttributes(array(
                    'file_id' => $model->id,
                    'recipient_id' => $user->id
                ));
                if (is_null($ftin))
                {
                    throw new SIMAWarnExceptionModelNotFound('FileTransfer');
                }
                $ftin->delete();
            }
        ]
    ],
    'WARN_FILE_TRANSFERS_OUT' => [
        'title' => Yii::t('FilesModule.Warns', 'warnFileTransfersOutTitle'),
        'onclick' => ['sima.dialog.openModel','legal/filingTransfer/fileTransfers','OUT'],
        'countFunc' => function($user) {
            return $user->file_transfers_out_count;
        },
        'forModel' => [
            'model' => 'File',
            'title' => Yii::t('FilesModule.Warns', 'warnFileTransfersOutByModelTitle'),
            'description' => Yii::t('FilesModule.Warns', 'warnFileTransfersOutByModelDesc'),
            'countFunc' => function (User $user, File $model){
                $cnt = FileTransfer::model()->count(new SIMADbCriteria([
                    'Model'=>'FileTransfer',
                    'model_filter'=>[
                        'file'=>['ids'=>$model->id],
                        'sender'=>['ids'=>$user->id]
                    ]
                ]));

                return $cnt;
            },
            'checkResolveFunc' => function (User $user, $model) {
                return [
                    'status' => 'OK',
                    'message' => Yii::t('FilesModule.Warns', 'warnFileTransfersOutByModelCheckResolve', [
                        '{file_display_name}' => $model->DisplayName
                    ])
                ];
            },
            'resolveFunc' => function (User $user, $model) {
                $ftout = FileTransfer::model()->findByAttributes(array(
                    'file_id' => $model->id,
                    'sender_id' => $user->id
                ));
                if (is_null($ftout))
                {
                    throw new SIMAWarnExceptionModelNotFound('FileTransfer');
                }
                $ftout->cancelTransfer();
            }
        ]
    ],
    'WARN_FILE_NORETURNED' => [
        'title' => Yii::t('FilesModule.Warns', 'warnFileNoReturnedTitle'),
        'onclick' => ['sima.mainTabs.openNewTab','guitable','files_23',[
                [
                    'guiTableRepopulate'=>[]
                ]
            ]
        ],
        'countFunc' => function($user) {
            return File::model()->noReturnedFilesScope($user->id)->count();
        },
        'forModel' => [
            [
                'model' => 'File',
                'title' => Yii::t('FilesModule.Warns', 'warnFileNoReturnedByModelTitle'),
                'description' => Yii::t('FilesModule.Warns', 'warnFileNoReturnedByModelDesc'),
                'countFunc' => function (User $user, File $model){
                    $cnt = 0;
                    if ($model->has_filing && $model->filing->located_at_id === $user->id && $model->filing->returned === false)
                    {
                        $cnt = 1;
                    }

                    return $cnt;
                },
                'checkResolveFunc' => function (User $user, File $model) {
                    return [
                        'status' => 'OK'
                    ];
                },
                'resolveFunc' => function (User $user, File $model) {
                    Yii::app()->controller->raiseAction('sima.model.form',[get_class($model), $model->id], true);
                }
            ],
            [
                'model' => 'Filing',
                'title' => Yii::t('FilesModule.Warns', 'warnFileNoReturnedByModelTitle'),
                'description' => Yii::t('FilesModule.Warns', 'warnFileNoReturnedByModelDesc'),
                'countFunc' => function (User $user, Filing $model){
                    $cnt = 0;
                    if ($model->located_at_id === $user->id && $model->returned === false)
                    {
                        $cnt = 1;
                    }

                    return $cnt;
                },
                'checkResolveFunc' => function (User $user, Filing $model) {
                    return [
                        'status' => 'OK'
                    ];
                },
                'resolveFunc' => function (User $user, Filing $model) {
                    Yii::app()->controller->raiseAction('sima.model.form',[get_class($model), $model->id], true);
                }
            ]
        ]
    ],
    'WARN_UNCONFIRMED_ARCHIVE_FILINGS' => [
        'title' => 'Broj arhivski zavedenih fajlova koje niste potvrdili',
        'onclick' => ['sima.mainTabs.openNewTab','guitable','files_24',[
                [
                    'guiTableRepopulate'=>[]
                ]
            ]
        ],
        'countFunc' => function($user) {
            $cnt = File::model()->unconfirmedArchiveFilingsScope($user->id)->count();

            return $cnt;
        },
        'forModel' => [
            [
                'model' => 'File',
                'title' => 'Arhivski zaveden fajl koji niste potvrdili',
                'countFunc' => function (User $user, File $model){
                    $cnt = File::model()->count(new SIMADbCriteria([
                        'Model'=>'File',
                        'model_filter'=>[
                            'ids' => $model->id,
                            'filter_scopes' => ['unconfirmedArchiveFilingsScope'=>$user->id]
                        ]
                    ]));

                    return $cnt;
                }
            ],
            [
                'model' => 'Filing',
                'title' => 'Arhivski zaveden fajl koji niste potvrdili',
                'countFunc' => function (User $user, Filing $model){
                    $cnt = File::model()->count(new SIMADbCriteria([
                        'Model'=>'File',
                        'model_filter'=>[
                            'ids' => $model->id,
                            'filter_scopes' => ['unconfirmedArchiveFilingsScope'=>$user->id]
                        ]
                    ]));

                    return $cnt;
                }
            ]
        ]
    ],
    'WARN_UNCONFIRMED_IN_FILINGS' => [
        'title' => 'Broj ulaznih zavedenih fajlova koje niste potvrdili',
        'onclick' => ['sima.mainTabs.openNewTab','guitable','files_25',[
                [
                    'guiTableRepopulate'=>[]
                ]
            ]
        ],
        'countFunc' => function($user) {
            $cnt = File::model()->unconfirmedInFilingsScope($user->id)->count();

            return $cnt;
        },
        'forModel' => [
            [
                'model' => 'File',
                'title' => 'Ulazni zavedeni fajl koji niste potvrdili',
                'countFunc' => function (User $user, File $model){
                    $cnt = File::model()->count(new SIMADbCriteria([
                        'Model'=>'File',
                        'model_filter'=>[
                            'ids' => $model->id,
                            'filter_scopes' => ['unconfirmedInFilingsScope'=>$user->id]
                        ]
                    ]));

                    return $cnt;
                },
                'checkResolveFunc' => function (User $user, File $model) {
                    return [
                        'status' => 'OK'
                    ];
                },
                'resolveFunc' => function (User $user, File $model) {
                    Yii::app()->controller->raiseAction('sima.model.form',[get_class($model->filing), $model->id], true);
                }
            ],
            [
                'model' => 'Filing',
                'title' => 'Ulazni zavedeni fajl koji niste potvrdili',
                'countFunc' => function (User $user, Filing $model){
                    $cnt = File::model()->count(new SIMADbCriteria([
                        'Model'=>'File',
                        'model_filter'=>[
                            'ids' => $model->id,
                            'filter_scopes' => ['unconfirmedInFilingsScope'=>$user->id]
                        ]
                    ]));

                    return $cnt;
                },
                'checkResolveFunc' => function (User $user, Filing $model) {
                    return [
                        'status' => 'OK'
                    ];
                },
                'resolveFunc' => function (User $user, Filing $model) {
                    Yii::app()->controller->raiseAction('sima.model.form',[get_class($model), $model->id], true);
                }
            ]
        ]
    ],
    'WARN_UNCONFIRMED_OUT_FILINGS' => [
        'title' => 'Broj izlaznih zavedenih fajlova koje niste potvrdili',
        'onclick' => ['sima.mainTabs.openNewTab','guitable','files_26',[
                [
                    'guiTableRepopulate'=>[]
                ]
            ]
        ],
        'countFunc' => function($user) {
            $cnt = File::model()->unconfirmedOutFilingsScope($user->id)->count();

            return $cnt;
        },
        'forModel' => [
            [
                'model' => 'File',
                'title' => 'Izlazni zavedeni fajl koji niste potvrdili',
                'countFunc' => function (User $user, File $model){
                    $cnt = File::model()->count(new SIMADbCriteria([
                        'Model'=>'File',
                        'model_filter'=>[
                            'ids' => $model->id,
                            'filter_scopes' => ['unconfirmedOutFilingsScope'=>$user->id]
                        ]
                    ]));

                    return $cnt;
                },
                'checkResolveFunc' => function (User $user, File $model) {
                    return [
                        'status' => 'OK'
                    ];
                },
                'resolveFunc' => function (User $user, File $model) {
                    Yii::app()->controller->raiseAction('sima.model.form',[get_class($model->filing), $model->id], true);
                }
            ],
            [
                'model' => 'Filing',
                'title' => 'Izlazni zavedeni fajl koji niste potvrdili',
                'countFunc' => function (User $user, Filing $model){
                    $cnt = File::model()->count(new SIMADbCriteria([
                        'Model'=>'File',
                        'model_filter'=>[
                            'ids' => $model->id,
                            'filter_scopes' => ['unconfirmedOutFilingsScope'=>$user->id]
                        ]
                    ]));

                    return $cnt;
                },
                'checkResolveFunc' => function (User $user, Filing $model) {
                    return [
                        'status' => 'OK'
                    ];
                },
                'resolveFunc' => function (User $user, Filing $model) {
                    Yii::app()->controller->raiseAction('sima.model.form',[get_class($model), $model->id], true);
                }
            ]
        ]
    ],
];

