<?php

return array(
    'LegalAddInsurancePersons' => [
        'description' => 'Pristup dodavanju clanova osiguranja nekog zdravstvenog osiguranika. Koristi se u pravnoj sluzbi.'
    ],
    'LegalEmployeeExpiringInsuranceWarn' => [
        'description' => 'Prikaz upozorenja osoba kojima istice zdravstveno osiguranje'
    ],
    'addEmployee',
    'LegalEmployeeAbsences',
    'LegalViewReferences' => [
        'description' => 'Prikaz taba referenci za tu osobu u profilu osobe'
    ],
    'LegalNewPotentialBidsWarn' => [
        'description' => 'Prikaz upozorenja sa svim novim potencijalnim ponudama'
    ],
    'LegalNewBidsWarn' => [
        'description' => 'Prikaz upozorenja sa svim novim ponudama'
    ],
    'LegalEmployeePaychecksAccess' => [
        'description' => Yii::t('LegalModule.Employee', 'LegalEmployeePaychecksAccessDescription')
    ],
    'LegalShowWarnForNotConfirmedEmployeesSickLeavesAndFreeDays' => [
        'description' => Yii::t('LegalModule.Warns', 'ShowWarnForNotConfirmedEmployeesSickLeavesAndFreeDays')
    ],
    'LegalContractsTakeCare' => [
        'description' => Yii::t('LegalModule.Contract', 'ContractsTakeCareConfigOperation')
    ]
);
