<?php

return [
    [
        'name' => 'potential_bid',
        'display_name' => 'Potencijalna ponuda',
        'type' => 'searchField',
        'type_params' => array(
            'modelName' => 'DocumentType',
        ),
        'value_not_null' => false,
        'user_change_forbidden' => true,
        'description' => 'Tip dokumenta za potencijalne ponude'
    ],
    [
        'name' => 'insurance_policy_document_type_id',
        'display_name' => Yii::t('LegalModule.InsurancePolicy', 'DocumentTypeForInsurancePolicy'),
        'type' => 'searchField',
        'type_params' => [
            'modelName' => 'DocumentType'
        ],
        'value_not_null' => false,
        'user_change_forbidden' => true,
        'description' => Yii::t('LegalModule.InsurancePolicy', 'DocumentTypeForInsurancePolicy')
    ],
    [
        'name' => 'contract',
        'display_name' => 'Ugovori',
        'type' => 'searchField',
        'type_params' => [
            'modelName' => 'DocumentType',
        ],
        'value_not_null' => false,
        'value_is_array' => true,
        'condition' => [
            'contract' => []
        ],
        'user_change_forbidden' => true,
        'description' => 'Tipovi dokumenata za Ugovore.'
    ],
    [
        'name' => 'bid',
        'display_name' => 'Ponuda',
        'type' => 'searchField',
        'type_params' => [
            'modelName' => 'DocumentType',
        ],
        'value_not_null' => false,
        'bid' => [
            'contract' => []
        ],
        'user_change_forbidden' => true,
        'description' => 'Tipovi dokumenata za Ponude.'
    ],
];

