<?php

return array(
    'electronic_register' => 101,
    'notice_of_mobbing_template' => 105,
    'employee_history_register' => 142,
    'personal_delivery_type' => 143,
    'potential_bid'=>181,
    'email_delivery_type_id'=>189,
    'post_delivery_type_id'=>214,
    'insurance_policy_document_type_id' => 376,
    'contract' => 80,
    'bid' => 81,
);