<?php

return array(
    'Filing'=>array(
        'modelFilingAdd', // rezervacija broja -> NAPRAVITI
        
//        'modelFilingDelete', //treba izbaciti
        'modelFilingFormOpen',
        //polja u formi
            'modelFilingLocatedAt',
            'modelFilingSubnumber',
            'modelFilingReturn',
//            'modelFilingOriginal',
            'modelFilingUnconfirmedEdit', //kolone koje mogu da se edituju pre potvrdjivanja (biz rule)
            'modelFilingUnreturnedEdit', //prvo potvrdjivanje
        
        'modelFilingView', //prikaz zavodnog broja?? ne koristi se trenutno
        'modelFilingRegistryAll',
        'modelFilingAddGuiTable2', // da omogucava drugo dugme u guitabeli
        'modelFilingEnableFormInput', //moze da enejbluje sva polja
        'modelFilingRevertConfirm', //da se vrati potvrda
        'modelFilingRevertReturn', //da se vrati potvrda
    ),
    'Reference' => array(
        'modelReferenceFormOpen',
        'modelReferenceOpenDialog',
        'modelReferenceDelete'
    ),
    'Bid' => array(
        'modelBidFormOpen',
        'modelBidOpenDialog',
        'modelBidDelete'
    ),
    'BidRequirement' => [
        'modelBidRequirementConfirm',
        'modelBidRequirementRevertConfirm'
    ],
    'PotentialBid' => [
        'modelPotentialBidAccept',
        'modelPotentialBidRevertAccept',
    ],
    'Employee' => [
        'modelEmployeeFormOpen',
        'modelEmployeeDelete'
    ],
    'InsurancePolicy' => [
        'modelInsurancePolicyExpireShowInCalendar'
    ],
);