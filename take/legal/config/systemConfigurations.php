<?php

return array(
    'display_name' => Yii::t('LegalModule.Configuration', 'Legal'),
    array(
        'name' => 'electronic_register',
        'display_name'=>'Elektronski registar',
        'type'=>'searchField',
        'type_params' => array(
            'modelName'=>'Registry',
        ),
        'value_not_null'=>true,
        'default' => [
            'name' => 'Elektronski registrator'
        ],
        'description'=>'Registar za dokumenta u elektronskom obliku (ne fizičkom)'
    ),
    array(
        'name' => 'notice_of_mobbing_template',
        'display_name'=>'Template za obavestenje na mobing',
        'type'=>'searchField',
        'type_params' => array(
            'modelName'=>'Template',
        ),
        'value_not_null'=>false,
        'description'=>'Template za obavestenje na mobing'
    ),
    array(
        'name' => 'employee_history_register',
        'display_name'=>'Registrator za dosije radnika',
        'type'=>'searchField',
        'type_params' => array(
            'modelName'=>'Registry',
        ),        
        'description'=>'Registrator za dosije radnika'
    ),
    array(
        'name' => 'personal_delivery_type',
        'display_name'=>'Način dostave - Lično',
        'type'=>'searchField',
        'type_params' => array(
            'modelName'=>'DeliveryType',
        ),        
        'value_not_null'=>true,
        'default' => [
            'name' => 'licno'
        ],
        'description'=>'Način dostave - Lično'
    ),
    array(
        'name' => 'email_delivery_type_id',
        'display_name'=>'Način dostave - Email',
        'type'=>'searchField',
        'type_params' => array(
            'modelName'=>'DeliveryType',
        ),        
        'value_not_null'=>true,
        'default' => [
            'name' => 'e-mail'
        ],
        'description'=>'Način dostave - Email'
    ),
    array(
        'name' => 'post_delivery_type_id',
        'display_name'=>'Način dostave - Pošta',
        'type'=>'searchField',
        'type_params' => array(
            'modelName'=>'DeliveryType',
        ),       
        'value_not_null'=>true,
        'default' => [
            'name' => 'posta'
        ],
        'description'=>'Način dostave - Pošta'
    ),
);