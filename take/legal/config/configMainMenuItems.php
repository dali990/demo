<?php

return array(
    'menuShowLegal'=>array(
        'menuShowLegalEmployees'=>array(
            'menuShowLegalEmployeesLicence',
            'menuShowLegalEmployeesReference',
            'menuShowLegalEmployeesWorkContracts',
            'menuShowLegalEmployeesAbsences',
            'menuShowLegalEmployeesOrganizationScheme',
            'menuShowLegalEmployeesStatistic'
        ),
        'menuShowLegalContracts',
        'menuShowLegalSafeRecord',
        'menuShowLegalRegisters',
        'menuShowLegalWorkPositionCompetition' => array(
            'menuShowLegalWorkPositionCompetitionCandidates',
            'menuShowLegalWorkPositionCompetitionGrades'
        ),
        'menuShowLegalCompanyLicence',
        'menuShowLegalConfig'
    ),
    'menuShowRecords'=>array(
        'menuShowBids'=>array(
            'menuShowBids_table',
            'menuShowPotentialBids_table',
            'menuShowRecords_insurancePolicies'
        ),
        'menuShowRecords_filingBook',
    )
);