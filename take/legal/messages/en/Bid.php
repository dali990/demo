<?php

return [
    'StatusNew' => 'New',
    'StatusInPreparation' => 'In preparation',
    'StatusSubmitted' => 'Submitted',
    'StatusAccepted' => 'Accepted',
    'StatusDeclined' => 'Declined',
    'StatusDropOut' => 'Drop out',
    'Refer' => 'Refer',
    'TextOfBid' => 'Text of bid',
    'BaseOfBid' => 'Base of bid',
    'BidCompose' => 'Bid compose',
    'LimitExecutionWork' => 'Limit for execution of work',
    'NumberOfBid' => 'Number of bid',
    'TextOfBidGenericTextValue' => 'Hello,       
        {curr_company} is grateful for the possibility of sending you this offer for the above topic',
    'BaseOfBidGenericTextValue' => '{curr_company} relying on the following documents submitted to us on the basis of which this offer was made, any departure from the submitted information / documents will be processed as additional works:',
    'EnableTemplate' => 'Enable template',
    'PdfSuggestion' => 'PDF suggestion',
    'AddBelongsAndDocumentTypeOfFile' => 'Add belongs and document type of file'
];