<?php

return [
//    'Legal' => 'Prava',
//    'Employees' => 'Zaposleni',
    'ByDecision' => 'By decision',
    'AnnualLeavesDaysLeft' => 'annual leaves days left is',
    'OrganizationScheme' => 'Organization scheme',
    'OrganizationSchemeShort' => 'Org. scheme',
    'PersonActivityOverviewForPersonsInOrganizationScheme' => 'Persons in the organization scheme',
    'PersonActivityOverviewForPersonsNotInOrganizationScheme' => 'Persons outside the organization scheme',
    'WorkedHoursEvidences' => 'Worked hours evidences',
    'ActiveWorkedHoursEvidences' => 'Active worked hours evidences',
    'AllWorkedHoursEvidences' => 'All worked hours evidences'
];
