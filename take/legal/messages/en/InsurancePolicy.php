<?php

return [
    'InsurancePolicy' => 'Insurance policy',
    'InsurancePolicies' => 'Insurance policies',
    'PolicyNumber' => 'Policy number',
    'StartDate' => 'Start date',
    'ExpireDate' => 'Expire date',
    'Bill' => 'Bill',
    'PremiumAmount' => 'Premium amount',
    'InsuranceCompany' => 'Insurance company',
    'Vinkulation' => 'Vinkulation',
    'DocumentTypeForInsurancePolicy' => 'Document type for insurance policy',
    'StartUseDateMustBeLessThanEndUseDate' => 'Start use date must be less than end use date',
]

?>