<?php

return [
//    'Archive' => 'Arhivska',
//    'Out' => 'Izlazna',
//    'In' => 'Ulazna',
//    'NumberIsNotNumeric' => 'Polje "Zavodni broj" treba biti ceo broj',
//    'DoYouWantToReserveFilingNumber' => 'Da li zaista želite da rezervišete broj u zavodnoj knjizi?',
    'FilingNumber' => 'Filing Number',
    'LocatedAt' => 'Located at',
    'AddToFiling' => 'Add to filing',
    'FilingBook' => 'Filing book',
    'AddTagsToFilingsAndContribsTitle' => 'Do you want the attachments to have the same belongs list?'
];