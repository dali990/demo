<?php

return [
    'NotConfirmedEmployeesSickLeavesAndFreeDays' => 'Bolovanja i slobodni dani koji nisu potvrdjeni',
    'ShowWarnForNotConfirmedEmployeesSickLeavesAndFreeDays' => 'Prikazuje upozorenje za bolovanja i slobodne dane koji nisu potvrdjeni'
];