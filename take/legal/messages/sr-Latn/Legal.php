<?php

return [
    'Legal' => 'Prava',
    'Employees' => 'Zaposleni',
    'ByDecision' => 'Po rešenju',
    'AnnualLeavesDaysLeft' => 'broj preostalih dana godisnjeg odmora je',
    'Listing' => 'Spisak',
    'OrganizationScheme' => 'Organizaciona šema',
    'OrganizationSchemeShort' => 'Org. šema',
    'PersonActivityOverviewForPersonsInOrganizationScheme' => 'Osobe u organizacionoj šemi',
    'PersonActivityOverviewForPersonsNotInOrganizationScheme' => 'Osobe van organizacione šeme',
    'WorkedHoursEvidences' => 'Karneti',
    'ActiveWorkedHoursEvidences' => 'Aktivni karneti',
    'AllWorkedHoursEvidences' => 'Svi karneti'
];
