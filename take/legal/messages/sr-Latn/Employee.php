<?php

return array(
    'FirstName' => 'Ime',
    'Lastname'=>'Prezime',
    'JMBG'=>'JMBG',
    'Degree'=>'Stručna sprema',
    'Location'=>'Lokacija',
    'Company'=>'Firma u kojoj je zaposlen',
    'Municipality'=>'Opština',
    'WorkingUntil'=>'Radi do',
    'UnderContract'=>'Radi/ne radi',
    'EmployeeOrder'=>'Red.br.',
    'YearExperience' => 'Radni staz',
    'EmailAccount'=>'Email nalog',
    'DisplayName'=>'Ime i prezime',
    'OrderInCompany' => 'Redni broj',
    'BloodGroup' => 'Krvna grupa',
    'DriverLicenseCategory'=>'Vozačke kategorije',
    'DriverLicenseExpire'=>'Datum isteka vozačke dozvole',
    'DriverLicenseIssuedBy'=>'Vozačku dozvolu izdao',
    'DriverLicenseNumber'=>'Broj vozačke dozvole',
    'IdCardNumber'=>'Broj lične karte',
    'PassportNumber'=>'Broj pasoša',
    'BirthDate'=>'Datum rodjenja',
    'EmploymentCode'=>'Šifra zaposlenja',
    'StartDate'=>'Datum početka',
    'EndDate'=>'Datum završetka',
    'WorkPosition'=>'Radna pozicija',
    'Employee' => 'Zaposleni',
    'Gender' => 'Pol',
    'ExpirationDateOfInsurance' => 'Datum isteka osiguranja',
    'SVP' => 'Šifra vrste placanja',
    'Profession' => 'Zanimanje',
    'NoInsufficientData' => 'Svi podaci popunjeni',
    'InsufficientData' => 'Nepotpuni podaci',
    'MissingYearExperience' => 'Zaposlenom "{employee}" nije zadata godina staža',
    'MissingEmploymentCode' => 'Zaposlenom "{employee}" nije zadat kod zaposlenja',
    'MissingPaycheckSvp' => 'Zaposlenom "{employee}" nije zadata šifra vrste plaćanja',
    'MissingMunicipality' => 'Zaposlenom "{employee}" nije zadata opština',
    'MissingMainBankAccount' => 'Zaposlenom "{employee}" nije zadat primarni bankovni racun',
    'MissingMainAddress' => 'Zaposlenom "{employee}" nije zadata glavna adresa',
    'MissingJMBG' => 'Zaposlenom "{employee}" nije zadat JMBG',
    'MissingQualificationLevel' => 'Zaposlenom "{employee}" nije zadat nivo kvalifikacije',
    'MissingOccupation' => 'Zaposlenom "{employee}" nije zadato zanimanje',
    'ShortJMBG' => 'Zaposlenom "{employee}" duzina JMBG-a nije 13 brojeva',
    'HaveWorkContractsWithSameStartDate' => 'Zaposleni "{employee}" ima dva ili vise ugovora koji imaju isti pocetni datum',
    'NoPayoutIdentificationValue' => 'Zaposlenom "{employee}" nije zadata vrednost indentifikatora prilikom isplate plate',
    'ActiveContracts' => 'Aktivni ugovori',
    'PayoutType' => 'Način isplate',
    'LastWorkDate' => 'Radi do (poslednji ugovor)',
    'PersonalPoints' => 'Lični poeni',
    'Age' => 'Godina',
    'Paychecks' => 'Plate',
    'EmplouyeesWithUnseenConfirmedDays' => 'Zaposleni sa potvrdjenim danima',
    'UnseenConfirmedDays' => 'Broj nepregledanih porvrdjenih dana',
    'PayoutIdentificationType' => 'Vrsta indentifikatora prilikom isplate plate',
    'PayoutIdentificationValue' => 'Vrednost indentifikatora prilikom isplate plate',
    'PAYOUT_IDENTIFICATION_TYPE_JMBG' => 'JMBG ili EBS',
    'PAYOUT_IDENTIFICATION_TYPE_REFUGEE_CARD' => 'Izbeglička Legitimacija',
    'PAYOUT_IDENTIFICATION_TYPE_PASSPORT' => 'Pasoš',
    'PAYOUT_IDENTIFICATION_TYPE_EXTERNAL_ID' => 'Eksterni Identifikator',
    'PAYOUT_IDENTIFICATION_TYPE_OTHER' => 'Ostalo',
    'CurrPaygrade' => 'Trenutni platni razred',
    'Employment' => 'Radni odnos',
    'OldWorkExp' => 'Prethodni radni staž',
    'WorkContractsWorkExp' => 'Radni staž na osnovu ugovora o radu'
);

