<?php

return [
    'Bid' => 'Ponuda',
    'Bids' => 'Ponude',
    'JobBid' => 'Poslovna ponuda',
    'JobBids' => 'Poslovne ponude',
    'QualificationBid' => 'Kvalifikacione ponuda',
    'QualificationBids' => 'Kvalifikacione ponude',
    'StatusNew' => 'Nova',
    'StatusInPreparation' => 'U pripremi',
    'StatusSubmitted' => 'Predata',
    'StatusAccepted' => 'Prihvaćena',
    'StatusDeclined' => 'Nije prihvaćena',
    'StatusDropOut' => 'Odustala',
    'Refer' => 'Upućeno',
    'TextOfBid' => 'Tekst ponude',
    'BaseOfBid' => 'Osnova ponude',
    'BidCompose' => 'Ponudu sastavio',
    'LimitExecutionWork' => 'Rok za izvršenje radova',
    'NumberOfBid' => 'Broj ponude',
    'TextOfBidGenericTextValue' => "Poštovani,
        {curr_company} je zahvalna za mogućnost da vam pošalje ovu ponudu za gore pomenutu temu",
    'BaseOfBidGenericTextValue' => '{curr_company} se oslonio na sledeća nama dostavljena dokumenta na osnovu kojih je ova ponuda izrađena, svako odstupanje od dostavljenih informacija/dokumenata biće obrađeno kao dodatni radovi:',
    'EnableTemplate' => 'Omogući šablon',
    'PdfSuggestion' => 'Predloži PDF',
    'AddBelongsAndDocumentTypeOfFile' => 'Fajlu je dodata pripadnost i tip dokumenta'
];