<?php

return [
    'InsurancePolicy' => 'Polisa osiguranja',
    'InsurancePolicies' => 'Polise osiguranja',
    'PolicyNumber' => 'Broj polise',
    'StartDate' => 'Početak polise',
    'ExpireDate' => 'Datum završetka',
    'Bill' => 'Račun',
    'PremiumAmount' => 'Iznos premije',
    'InsuranceCompany' => 'Osiguravajuća kuća',
    'Vinkulation' => 'Vinkulacija',
    'DocumentTypeForInsurancePolicy' => 'Tip dokumenta za polisu osiguranja',
    'StartUseDateMustBeLessThanEndUseDate' => 'Početak mora biti pre kraja',
]

?>