<?php

return [
    'TemplateDisplayName' => 'broj {contract_number} od {contract_date} godine (naš broj {filing_number} od {filing_date} godine)',
    'ContractsTakeCareConfigOperation' => 'Privilegija za rad sa ugovorima'
];
