<?php

class BusinessOfferVariationReport extends SIMAReport
{
    protected $css_file_name = 'css.css';
    
    protected function getModuleName()
    {
        return 'legal';
    }
    
    protected function getHTMLContent()
    {
        $business_offer_variation = $this->params['business_offer_variation'];
        
        return [
            'html' => $this->render('html', [
                'business_offer_variation' => $business_offer_variation
            ], true, false)
        ];
    }
}