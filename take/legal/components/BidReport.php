<?php
class BidReport extends SIMAReport
{
    protected $bid;
    protected $css_file_name = 'css.css';
    protected $margin_top = 26;
    protected $include_page_numbering = false;
    
    protected function getModuleName()
    {
        return 'legal';
    }
    
    public function __construct($bid)
    {  
        $this->bid = $bid;
        
        parent::__construct();

    }
    
    protected function getHTMLContent()
    {
        return $this->render('html', [
            'curr_company' => Yii::app()->company,
            'bid' => $this->bid
        ], true, false);
    }
    
    protected function getHTMLHeader()
    {
        return  $this->render('header', [
            'curr_company' => Yii::app()->company,
        ], true, false);
    }
}