<?php

class EmployeeComponent extends SIMAComponent
{
    public static function employeesWithInsufficientData()
    {
        $problemEmployees = array();
        
        $allEmployees = Employee::model()->findAll();
                
        foreach($allEmployees as $employee)
        {
            if(!isset($employee->active_work_contract))
            {
                continue;
            }
            
            $problems = EmployeeComponent::employeeInsufficientData($employee);
            
            if(count($problems) > 0)
            {
                $problemEmployees[] = $employee;
            }
        }
                        
        return $problemEmployees;
    }
    public static function employeesIdsWithInsufficientData()
    {
        $problemEmployees_ids = array();
        
        $problemEmployees = EmployeeComponent::employeesWithInsufficientData();
        
        foreach($problemEmployees as $employee)
        {
            $problemEmployees_ids[] = $employee->id;
        }
                        
        return $problemEmployees_ids;
    }
    
    public static function employeeInsufficientData(Employee $employee, $with_model_form_open=true)
    {
        $error_messages = array();
        
        if (!isset($employee->employment_code))
        {
            $message = '';
            if($with_model_form_open === true)
            {
                $message = SIMAHtml::modelFormOpen($employee);
            }
            $message .= Yii::t('LegalModule.Employee', 'MissingEmploymentCode', array('{employee}' => $employee));
            
            $error_messages[] = $message;
        }
        if (!isset($employee->paycheck_svp))
        {
            $message = '';
            if($with_model_form_open === true)
            {
                $message = SIMAHtml::modelFormOpen($employee);
            }
            $message .= Yii::t('LegalModule.Employee', 'MissingPaycheckSvp', array('{employee}' => $employee));
            
            $error_messages[] = $message;
        }
        
        if (!isset($employee->municipality))
        {
            $message = '';
            if($with_model_form_open === true)
            {
                $message = SIMAHtml::modelFormOpen($employee);
            }
            $message .= Yii::t('LegalModule.Employee', 'MissingMunicipality', array('{employee}' => $employee));
            
            $error_messages[] = $message;
        }
        
        if($employee->payout_type === Employee::$PAYOUT_TYPE_BANK_ACCOUNT && !isset($employee->person->main_bank_account))
        {
            $message = '';
            if($with_model_form_open === true)
            {
                $message = SIMAHtml::modelFormOpen($employee->person);
            }
            $message .= Yii::t('LegalModule.Employee', 'MissingMainBankAccount', array('{employee}' => $employee));
            
            $error_messages[] = $message;
        }
        
        if(!isset($employee->person->main_address))
        {
            $message = '';
            if($with_model_form_open === true)
            {
                $message = SIMAHtml::modelFormOpen($employee->person);
            }
            $message .= Yii::t('LegalModule.Employee', 'MissingMainAddress', array('{employee}' => $employee));
            
            $error_messages[] = $message;
        }
        
        if(!isset($employee->person->JMBG) || empty($employee->person->JMBG))
        {
            $message = '';
            if($with_model_form_open === true)
            {
                $message = SIMAHtml::modelFormOpen($employee->person);
            }
            $message .= Yii::t('LegalModule.Employee', 'MissingJMBG', array('{employee}' => $employee));
            
            $error_messages[] = $message;
        }
        
        if(isset($employee->person->JMBG) && strlen($employee->person->JMBG) != 13)
        {
            $message = '';
            if($with_model_form_open === true)
            {
                $message = SIMAHtml::modelFormOpen($employee->person);
            }
            $message .= Yii::t('LegalModule.Employee', 'ShortJMBG', array('{employee}' => $employee));
            
            $error_messages[] = $message;
        }
        
        if(empty($employee->payout_identification_value))
        {
            $message = '';
            if($with_model_form_open === true)
            {
                $message = SIMAHtml::modelFormOpen($employee);
            }
            $message .= Yii::t('LegalModule.Employee', 'NoPayoutIdentificationValue', array('{employee}' => $employee));
            
            $error_messages[] = $message;
        }
        
        $have_work_contracts_with_same_startdate = false;
        $work_contracts = $employee->work_contracts;
        $work_contracts_length = count($work_contracts);
        for($i=0; $i<$work_contracts_length-1; $i++)
        {
            for($j=$i+1; $j<$work_contracts_length; $j++)
            {
                if($work_contracts[$i]->start_date === $work_contracts[$j]->start_date)
                {
                    $have_work_contracts_with_same_startdate = true;
                    break;
                }
            }
            
            if($have_work_contracts_with_same_startdate === true)
            {
                break;
            }
        }
        if($have_work_contracts_with_same_startdate)
        {
            $message = '';
            if($with_model_form_open === true)
            {
                $message = SIMAHtml::modelFormOpen($employee->person);
            }
            $message .= Yii::t('LegalModule.Employee', 'HaveWorkContractsWithSameStartDate', array('{employee}' => $employee));
            
            $error_messages[] = $message;
        }
        
        return $error_messages;
    }
    
    /**
     * Privremena metoda dok se ne refaktorise Employee komponenta
     * @param Employee $employee
     * @param bool $with_model_form_open
     * @return array
     */
    public static function employeeInsufficientDataWithQualificationLevelCheck(Employee $employee, bool $with_model_form_open=true): array 
    {
        $error_messages = EmployeeComponent::employeeInsufficientData($employee, $with_model_form_open);
        
        if(empty($employee->person->qualification_level))
        {
            $message = '';
            if($with_model_form_open === true)
            {
                $message = SIMAHtml::modelFormOpen($employee->person);
            }
            $message .= Yii::t('LegalModule.Employee', 'MissingQualificationLevel', array('{employee}' => $employee));

            $error_messages[] = $message;
        }
        
        return $error_messages;
    }
}