/* global sima */

function SIMALegal()
{
    this.generateIntroductionIntoBusinessProgram = function(work_contract_id, template_type)
    {
        sima.ajax.get('legal/legal/generateIntroductionIntoBusinessProgram', {
            get_params:{
                work_contract_id : work_contract_id
            },
            success_function: function(response)
            {
                sima.dialog.openInfo('Generisano!');
            }
        });
    };
    
    this.filingIOChange = function(form)
    {
        var _selected_value = form.find('select#Filing_in option:selected').val();
        var _target_span = form.find('span#filing_number_io');
        if (_selected_value==='1')    
            _target_span.html("U");
        else if (_selected_value==='0') 
            _target_span.html("I");
        else
            _target_span.html("A");

    };
    
    this.addBidDocument = function(bid_id, belong_tag_query, column, document_type_id = '', belong_tag_id = '', belong_tag_display_name = '')
    {
        var params = {};
        params.view = "guiTable";
        params.filter = {belongs: belong_tag_query};
        params.params = {
            add_button: {
                init_data: {
                    File: {}
                }
            }
        };

        if (!sima.isEmpty(document_type_id))
        {
            params.params.add_button.init_data.File.document_type_id = document_type_id;
        }
        if (!sima.isEmpty(belong_tag_id) && !sima.isEmpty(belong_tag_display_name))
        {
            params.params.add_button.init_data.File.file_belongs = {
                'ids': {
                    'ids' : [{
                        id: belong_tag_id,
                        display_name: belong_tag_display_name,
                        class: '_non_editable'
                    }]
                }
            };
        }
        sima.model.choose('File', function(data){
            var file_id = data[0].id;
            sima.ajax.get('legal/legal/addBidDocument', {
                get_params:{
                    bid_id : bid_id,
                    file_id : file_id,
                    column : column
                }
            });
        }, params);
    };
    
    this.addLicenseToBid = function(bid_id, license_model, license_id)
    {
        sima.ajax.get('legal/legal/addLicenseToBid',{
            get_params:{
                bid_id:bid_id,
                license_model:license_model,
                license_id:license_id
            },
            success_function:function(response){
            }
        });
    };
    
    this.addReferenceFile = function(reference_id)
    {
        var params = {};
        params.view = "guiTable";
        sima.model.choose('File', function(data){
            var file_id = data[0].id;
            sima.ajax.get('legal/legal/addReferenceFile', {
                get_params:{
                    reference_id : reference_id,
                    file_id : file_id
                }
            });
        }, params);
    };
    
    this.addCompanyLicenseToReference = function(reference_id, company_licenses_ids)
    {        
        var params = {};
        params.view = "guiTable";
        params.multiselect = true;
        params.ids = company_licenses_ids;
        sima.model.choose('CompanyLicenseToCompany', function(data){
            sima.ajax.get('legal/legal/addCompanyLicenseToReference', {
                get_params:{
                    reference_id : reference_id                
                },
                data:{company_licenses:data}
            });
        }, params);
    };
    
    this.addWorkLicenseToReference = function(reference_id, work_licenses_ids)
    {        
        var params = {};
        params.view = "guiTable";
        params.multiselect = true;
        params.ids = work_licenses_ids;
        sima.model.choose('PersonToWorkLicense', function(data){
            sima.ajax.get('legal/legal/addWorkLicenseToReference', {
                get_params:{
                    reference_id : reference_id               
                },
                data:{work_licenses:data}
            });
        }, params);
    };
    
    this.addFileFilingNumber = function(file_id, partner_id, responsible_id)
    {
        sima.ajax.get('legal/filingBook/addFileFilingNumberLockRequest', {
            data:{
                file_id: file_id
            },
            success_function: function()
            {
                sima.model.form('Filing', file_id,{
                    init_data:{
                        Filing: { 
                            partner_id: partner_id
                        },
                        File: { 
                            responsible_id: responsible_id
                        }
                    },
                    onClose: function(){
                        sima.ajax.get('legal/filingBook/addFileFilingNumberUnLockRequest', {
                            data:{
                                file_id: file_id
                            }
                        });
                    }
                });
            }
        });
    };

    this.confirmFilingTransfer = function(filing_id)
    {
        sima.ajax.get('legal/filingTransfer/ConfirmTransfer', {
            data:{
                ids : [filing_id]
            }
        });
    };
    
    this.cancelFilingTransfer = function(filing_id)
    {
        sima.ajax.get('legal/filingTransfer/CancelTransfer', {
            data:{
                ids : [filing_id]
            }
        });
    };
    
    this.generateBidReport = function(bid_id)
    {
        var progress_bar_obj = sima.getLastActiveFunctionalElement();
        if (sima.isEmpty(progress_bar_obj))
        {
            progress_bar_obj = $('body');
        }
        sima.ajaxLong.start(
            'legal/legal/bidReportSuggestion',
            {
                showProgressBar: progress_bar_obj,
                data: {
                    bid_id: bid_id
                },
                onEnd: function(response)
                {
                    sima.dialog.openYesNo(
                        {
                            title: sima.translate('BidPdfSuggestion'),
                            html: response.html
                        }, 
                        {
                            title: sima.translate('Save'),
                            func: function onConfirm() {
                                sima.dialog.close();
                                sima.ajax.get('legal/legal/bidReportAddNewVersionFromTempFile', {
                                    get_params:{
                                        bid_id: bid_id,
                                        temp_file_name: response.temp_file_name
                                    },
                                    success_function: function(response){

                                    }
                                });
                            }
                        },
                        {
                            title: sima.translate('Cancel')
                        }, 
                        null, null, true
                    );
                }
            }
        );
        
    };
}

