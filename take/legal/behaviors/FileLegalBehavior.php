<?php

/**
 * @package SIMA
 * @subpackage Legal
 */
class FileLegalBehavior extends SIMAActiveRecordBehavior
{

    public function relations($event)
    {
        $event->addResult(array(
            'has_filing'=>array(SIMAActiveRecord::STAT, 'Filing', 'id', 'select' => 'count(*)>0'),
            'filing'=>array(SIMAActiveRecord::HAS_ONE, 'Filing', 'id',
                'alias'=>"filinginfile",
                'order'=>"filinginfile.date DESC, filinginfile.number DESC, filinginfile.subnumber DESC, filinginfile.id"
            ),
            'safe_evd2' => [SIMAActiveRecord::HAS_ONE, 'SafeEvd2', 'id'],
            'potential_bid' => [SIMAActiveRecord::HAS_ONE, 'PotentialBid', 'id'],
            'has_work_contract'=>array(SIMAActiveRecord::STAT, 'WorkContract', 'id', 'select' => 'count(*)>0'),
            'work_contract'=>array(SIMAActiveRecord::HAS_ONE, 'WorkContract', 'id'),
            'insurance_policy' => [SIMAActiveRecord::HAS_ONE, InsurancePolicy::class, 'id'],
            'business_offer_variation' => [SIMAActiveRecord::HAS_ONE, BusinessOfferVariation::class, 'id'],
        ));
    }
    
    public function views($event)
    {
        //prebaceno u basic_info od fajla, jer nam treba mogucnost da se ovaj view ubaci na tacno odredjenoj lokaciji u basic info od fajla
//        if($event->params['view'] === 'basic_info')
//        {
//            $file = $this->owner;
//            if (!$file->isDeleted() && !is_null($file->filing))
//            {
//                $view_html = Yii::app()->controller->renderModelView($file->filing,'inFile');
//                $event->addResult([
//                    [
//                        'view'=>'basic_info',
//                        'order'=>1,
//                        'html'=>$view_html
//                    ]
//                ]);
//            }
//        }
    }
    
    /// treba odkomentarisati kad se napravi fullinfo za filing
    public function fullInfo($event)
    {
        $owner = $this->owner;
        
//        if ($owner->has_filing)
//        {
//            $event->addResult([
//                [
//                    'model'=>$owner->filing,
//                    'priority'=>10
//                ]
//            ]);
//        }
        ///Full info za contract
        if ($owner->has_work_contract)
        {
            $event->addResult([
                [
                    'model'=>$owner->work_contract,
                    'priority'=>20
                ]
            ]);
        }
    }
    
    public function basicInfo($event)
    {
        $owner = $this->owner;
        
        if($owner->isDeleted())
        {
            return;
        }
        
        if(!is_null($owner->filing))
        {
            $filing = $owner->filing;
            
            $event->addResult([
                [
                    'type' => SIMABasicInfo::$TYPE_HTML,
                    'html' => '<h3>Zavodna knjiga</h3>'
                ],
                [
                    'type' => SIMABasicInfo::$TYPE_EXPANDABLE_INFORMATIONS_FOR_MODEL,
                    'model' => $filing,
                    'basic_informations' => [
                        'confirmed',
                        [
                            'type' => SIMABasicInfo::$TYPE_LABEL_VALUE,
                            'label' => $filing->getAttributeLabel('filing_number'),
                            'value' => $filing->DisplayNameWithDate
                        ],
                        'located_at_id',
                    ],
                    'expanded_informations' => [
                        'confirmed',
                        'returned',
                        [
                            'type' => SIMABasicInfo::$TYPE_LABEL_VALUE,
                            'label' => $filing->getAttributeLabel('filing_number'),
                            'value' => $filing->DisplayNameWithDate
                        ],
                        'partner_id',
                        'file.name',
                        'delivery_types',
                        'registry_id',
                        'located_at_id',
                        'creator_id',
                        'comment',
                    ]
                ]
            ]);
        }
    }
}