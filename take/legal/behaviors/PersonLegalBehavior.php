<?php

class PersonLegalBehavior extends SIMAActiveRecordBehavior
{
    public function relations($event)
    {
        $event->addResult(array(
            'education_title' => [SIMAActiveRecord::BELONGS_TO, 'EducationTitle', 'education_title_id']
        ));
    }      
    
    public function afterSave($event)
    {
        parent::afterSave($event);
        
        $owner = $this->owner;
        
        if (isset($owner->employee))
        {
            Yii::app()->warnManager->sendWarns('HRInsufficientDataForEmployees', 'WARN_INSUFFICIENT_DATA_EMPOLOYEES');
            
            if (intval($owner->employee->payout_identification_type) === Employee::$PAYOUT_IDENTIFICATION_TYPE_JMBG
                    && $owner->employee->payout_identification_value !== $owner->JMBG
                    )
            {
                $owner->employee->payout_identification_value = $owner->JMBG;
                $owner->employee->save();
            }
            
        }
    }
}