<?php

class PartnerLegalBehavior extends SIMAActiveRecordBehavior
{
    public function tabs($event)
    {
        $owner = $this->owner;
        $owner_is_current_user = Yii::app()->user->id == $owner->id;
        $owner_is_employee = isset($owner->person)  && isset($owner->person->employee);
        $user_is_director = false;
        if(isset($owner->person) && isset($owner->person->employee))
        {
            $sector_heads=$owner->person->getSectorDirectors(true);    
            $user_is_director = in_array(Yii::app()->user->id, $sector_heads) ? true : false;
        }
        
        $privilege_HRread = Yii::app()->user->checkAccess('HRWorkContractRead');
        $privilege_HRLicenceManager = Yii::app()->user->checkAccess('HRLicenceRead');
        $privilege_HRWorkContractRead = Yii::app()->user->checkAccess('HRWorkContractRead');
        $privilege_SuspensionAccess = Yii::app()->user->checkAccess('PaychecksSuspensionAccess');
        $privilege_HRLicenceRead = ((Yii::app()->user->checkAccess('HRLicenceRead')) || (Yii::app()->user->id === $owner->id));
        $privilege_PaychecksRead = (
            (Yii::app()->user->checkAccess('PaychecksUserRead')) 
            &&
            (
                (Yii::app()->user->checkAccess('PaychecksRead')) 
                || 
                (Yii::app()->user->id === $owner->id)
            )
        );
        
        $visible1 = ($privilege_HRWorkContractRead || Yii::app()->user->id == $owner->id || $user_is_director) && $owner_is_employee;
        
        $event->addResult([
            [
//                'title' => 'Zavodna knjiga',
                'title' => Yii::t('LegalModule.Filing', 'FilingBook'),
                'code' => 'partner_files.filing_book',
                'action' => 'legal/legalPartnerTabs/filingBook',
                'get_params' => ['id' => $owner->id]
            ],
            [
                'title' => Yii::t('LegalModule.Employee', 'Employment'),
                'code' => 'work_relation',
                'visible' => (($privilege_HRread || $owner_is_current_user || $privilege_HRLicenceManager || $user_is_director) && $owner_is_employee),
                'module_origin' => 'legal',
                'selectable' => false,
                'order'=>10,
                'subtree' => [
                    [
                        'title' => Yii::t('HRModule.WorkContract', 'WorkContracts'),
                        'code' => 'work_relation.work_contracts',
                        'visible' => $visible1,
                        'action' => 'HR/workContract/tabWorkContracts',
                        'get_params' => ['id' => $owner->id]
                    ],
                    [
                        'title' => Yii::t('JobsModule.Training','Trainings'),
                        'code' => 'work_relation.trainings',
                        'visible' => $visible1,
                        'action' => 'jobs/job/trainingTabs',
                        'get_params' => [
                            'person_id' => $owner->id,
                        ]
                    ],
                    [
//                        'title' => 'Odsustva',
                        'title' => Yii::t('HRModule.Absence', 'Absences'),
                        'code' => 'work_relation.days_off',
                        'visible' => $visible1,
                        'action' => 'HR/absence/absenceTabs',
                        'get_params' => [
                            'employee_id' => $owner->id
                        ]
                    ],
                    [
                        'title' => 'Radne pozicije',
                        'code' => 'work_relation.work_positions',
                        'visible' => $visible1 && Yii::app()->user->checkAccess('LegalWorkPositionManager'),
                        'action' => 'HR/workContract/personWorkPositions',
                        'get_params' => [
                            'person_id' => $owner->id
                        ]
                    ],
                    [
                        'title' => Yii::t('HRModule.SafeEvd', 'SafeEvds'),
                        'code' => 'work_relation.safe_evd',
                        'visible' => $visible1 && (isset(Yii::app()->company) && $owner->person->company_id == Yii::app()->company->id),
                        'action' => 'HR/safeRecord/tabSafeEvd',
                        'get_params' => ['id' => $owner->id]
                    ],
                    [
                        'title' => 'Članovi osiguranja',
                        'code' => 'work_relation.insurance_persons',
                        'visible' => $visible1 && (Yii::app()->user->checkAccess('LegalAddInsurancePersons') && $owner_is_employee && isset($owner->person->employee->expiration_date_of_insurance)),
                        'action' => 'legal/legal/tabInsurancePersons',
                        'get_params' => ['id' => $owner->id]
                    ],
                    [
                        'title' => Yii::t('PaychecksModule.Suspension', 'Suspensions'),
                        'code' => 'work_relation.suspensions',
                        'visible' => ($owner_is_employee && ($privilege_SuspensionAccess || Yii::app()->user->id == $owner->id)),
                        'action'=>'guitable',
                        'get_params'=>[
                            'settings'=>[
                                'columns_type' => 'for_employee',
                                'model' => 'Suspension',
                                'fixed_filter' => [
                                    'employee'=>[
                                        'ids' => $owner->id
                                    ]
                                ],
                                'add_button' => ($privilege_SuspensionAccess) ? [
                                    'formName'=>'for_employee',
                                    'init_data'=>[
                                        'Suspension' => [
                                            'employee'=>[
                                                'ids' => $owner->id
                                            ]
                                        ]
                                    ]
                                ] : false
                            ]
                        ]
                    ],
                    [
                        'title' => Yii::t('HRModule.WorkLicense', 'WorkLicenses'),
                        'code' => 'work_relation.person_to_work_license',
                        'visible' => $privilege_HRLicenceRead,
                        'action' => 'HR/workLicense/tabPersonWorkLicense',
                        'get_params' => ['id' => $owner->id]
                    ],
                    [
                        'title' => Yii::t('LegalModule.Employee', 'Paychecks'),
                        'code' => 'work_relation.paychecks',
                        'visible' => Yii::app()->hasModule('accounting.paychecks') === true && $privilege_PaychecksRead === true,
                        'action' => 'accounting/paychecks/paycheck/employeePaychecks',
                        'get_params' => [
                            'model_id' => $owner->id,
                        ]
                    ],
                    [
                        'title' => 'Reference',
                        'code' => 'work_relation.person_references',
                        'visible' => isset($owner->person) && Yii::app()->user->checkAccess('LegalViewReferences'),
                        'action' => 'legal/legal/tabPersonReferences',
                        'get_params' => ['person_id' => $owner->id]
                    ]
                ]
            ]
        ]);
        
        if ($visible1 === true)
        {
            foreach ($owner->person->person_companies as $company)
            {
                $event->addResult([
                    [
//                        'title' => 'Org. šema ' . $company->DisplayName,
                        'title' => Yii::t('LegalModule.Legal', 'OrganizationSchemeShort').' '.$company->DisplayName,
                        'code' => 'work_relation.person_org_scheme',
                        'action' => 'organizationScheme/tabEmployeeOrg',
                        'get_params' => ['company_id' => $company->id, 'id' => $owner->id]
                    ]
                ]);
            }
        }
    }
}
