<?php

class ThemeLegalBehavior extends SIMAActiveRecordBehavior
{
    public function relations($event)
    {
        $event->addResult(array(
            'bid'=>array(SIMAActiveRecord::HAS_ONE, 'Bid', 'id'),
            'job_bid' => [SIMAActiveRecord::HAS_ONE, 'JobBid', 'id'],
            'qualification_bid' => [SIMAActiveRecord::HAS_ONE, 'QualificationBid', 'id']
        ));
    }

    public function tabs($event)
    {
        $owner = $this->owner;

        if ($owner->type === Theme::$JOB)
        {
            $event->addResult([
                [
                    'title' => Yii::t('BaseModule.Theme', 'TabLegalReferences'),
                    'code' => 'theme_legal_references',
                    'module_origin' => 'legal',
                    'pre_path' => 'others',
                    'action'=>'legal/legal/references',
                        'params'=>[
                            'filter'=>['job'=>['ids'=>$owner->id]],
                            'add_button'=>[
                                'init_data'=>[
                                    'Reference'=>[
                                        'job_id'=>[
                                            'disabled',
                                            'init'=>$owner->id
                                        ]
                                    ]
                                ]
                            ]
                        ]
                ],
            ]);
        }
        
        $event->addResult([
            [
                'title' => Yii::t('BaseModule.Theme', 'TabJobArchive'),
                'code' => 'theme_job_archive',
                'module_origin' => 'jobs',
                'pre_path' => 'others',
            ],
        ]);
    }
    
    public function views($event)
    {
        if($event->params['view'] === 'options')
        {
            $theme = $this->owner;
            if (!empty($theme->bid) && $theme->bid->is_enable_template && (!empty($theme->job_bid) || !empty($theme->qualification_bid)))
            {
                $view_html = Yii::app()->controller->renderModelView($theme->bid, 'options_in_theme');
                $event->addResult([
                    [
                        'view' => 'options',
                        'html' => $view_html
                    ]
                ]);
            }
        }
    }
}
