<?php

class BusinessOfferTabs extends SIMAActiveRecordTabs
{
    protected function tabsDefinition($code = null)
    {
        $owner = $this->owner;
        
        $business_offer_tabs = [
            [
                'title' => Yii::t('LegalModule.BusinessOfferVariation', 'BusinessOfferVariations'),
                'code' => 'business_offer_variations',
                'visible' => !empty($owner->business_offer_variation_id)
            ]
        ];
        
        return $business_offer_tabs;
     }
}
