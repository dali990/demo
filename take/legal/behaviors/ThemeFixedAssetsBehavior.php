<?php

class ThemeFixedAssetsBehavior extends SIMAActiveRecordBehavior
{
    public function tabs($event)
    {
        $owner = $this->owner;
        $event->addResult([
            [
                'title' => Yii::t('LegalModule.InsurancePolicy', 'InsurancePolicies'),
                'code' => 'insurance_policies',
                'module_origin' => 'legal',
                'pre_path' => 'theme_accounting_finance',
                'get_params' => ['model' => $owner]
            ]
        ]);      
    }  
}
