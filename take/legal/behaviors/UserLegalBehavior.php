<?php
/**
 * Description of CompanySectorTabs
 *
 * @author goran
 */
class UserLegalBehavior extends SIMAActiveRecordBehavior
{
    public function relations($event)
    {
        $event->addResult(array(
            'employee' => array(SIMAActiveRecord::HAS_ONE, 'Employee', 'id'),
            'registries' => array(SIMAActiveRecord::MANY_MANY, 'Registry', 'private.users_to_registries(user_id, registry_id)'),
        ));
    }
    
    public function getAvailableRegistriesHTML()
    {
        $res = 'Vama dostupni registri su:<ul><li>Elektronski</li>';
        foreach ($this->owner->registries as $reg)
        {
            $res .= "<li>$reg->DisplayName - $reg->description</li>";
        }
        $res .= '</ul>';
        return $res;
    }
}
