<?php

class ReferenceTabs extends SIMAActiveRecordTabs
{
    protected function tabsDefinition($code = null)
    {
        $owner = $this->owner;
        $reference_tabs = [
            [
                'title' => 'Info',
                'code'=>'info',
                'reload_on_update' => true
            ]
        ];
        
        return $reference_tabs;
     }
}

