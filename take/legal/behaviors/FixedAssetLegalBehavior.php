<?php

class FixedAssetLegalBehavior extends SIMAActiveRecordBehavior
{   
    public function tabs($event)
    {
        if ( Yii::app()->user->checkAccess('accountingFixedAssetStat'))
        {
            $owner = $this->owner;
            $event->addResult([
                [
                    'title' => Yii::t('LegalModule.InsurancePolicy', 'InsurancePolicies'),
                    'code' => 'insurance_policies',
                    'module_origin' => 'legal',
                    'pre_path' => 'finance',
                    'get_params' => ['model' => $owner]
                ]
            ]);
        }
    }
    
}