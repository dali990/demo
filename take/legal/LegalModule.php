<?php

/**
 * Description of LegalModule
 *
 * @author goran
 *
 * @package SIMA
 * @subpackage Legal
 * 
 */
class LegalModule extends SIMAModule
{

    public function init()
    {
        parent::init();
    }
    
    public function permanentBehaviors()
    {
        return [
            'File' => 'FileLegalBehavior',
            'User' => 'UserLegalBehavior',
            'Person' => 'PersonLegalBehavior',
            'Theme' => 'ThemeLegalBehavior',
            'Partner' => 'PartnerLegalBehavior',
            'FixedAsset' => 'FixedAssetLegalBehavior'
        ];
    }

    public function getBaseUrl()
    {
        return Yii::app()->assetManager->publish(Yii::getPathOfAlias('legal.assets'));
    }

    public function registerManual()
    {
        $urlScript = Yii::app()->assetManager->publish(Yii::getPathOfAlias('legal.assets'));
        
        Yii::app()->clientScript->registerScriptFile($urlScript .'/js/simaLegal.js', CClientScript::POS_HEAD);
        $sima_init_script = "sima.legal = new SIMALegal();";
        Yii::app()->clientScript->registerScript(SIMAHtml::uniqid(),$sima_init_script, CClientScript::POS_READY);
    }

    public function getGuiTableDefinition($index)
    {
        switch($index)
        {
            case '1': return [
                'label' => Yii::t('BaseModule.GuiTable', 'ProfessionalDegrees'),
                'settings' => array(
                    'model' => ProfessionalDegree::model(),
                    'add_button' => true,
                    'custom_buttons' => [
                        Yii::t('BaseModule.Misc','TakeDataFromCodeBook') => [
                            'func' => 'sima.adminMisc.syncProfessionalDegreesFromCodebook(this)',
                        ],
                    ],
                )
            ];
            case '2': return [
                'label' => Yii::t('BaseModule.GuiTable', 'DeliveryTypes'),
                'settings' => array(
                    'model' => DeliveryType::model(),
                    'add_button' => true
                )
            ];
            case '3': return [
                'label' => Yii::t('BaseModule.GuiTable', 'BidStatuses'),
                'settings' => array(
                    'model' => BidStatus::model(),
                    'add_button' => true
                )
            ];
            case '4': return [
                'label' => 'Prava - Registrator',
                'settings' => array(
                    'model' => Registry::model(),
                    'add_button' => true
                )
            ];
            case '5': return [
                'label' => 'Kvalifikacione ponude',
                'settings' => array(
                    'model' => QualificationBid::model(),
                    'add_button' => array(
                        'init_data' => array(
                            'Theme' => array(
                                'coordinator_id' => Yii::app()->user->id
                            )
                        )
                    )
                )
            ];
            case '6': return [
                'label' => Yii::t('BaseModule.GuiTable', 'BidRequirementTypes'),
                'settings' => array(
                    'model' => BidRequirementType::model(),
                    'add_button' => true
                )
            ];
            case '7': return [
                'label' => 'Tabela ponuda',
                'settings' => array(
                    'model' => JobBid::model(),
                    'add_button' => array(
                        'init_data' => array(
                            'Theme' => array(
                                'coordinator_id' => Yii::app()->user->id
                            ), 
                            'JobBid' => [
                                'text_of_bid' => Yii::t('LegalModule.Bid', 'TextOfBidGenericTextValue', ['{curr_company}' => Yii::app()->company->DisplayName]),
                                'base_of_bid' => Yii::t('LegalModule.Bid', 'BaseOfBidGenericTextValue', ['{curr_company}' => Yii::app()->company->DisplayName])
                            ]
                        )
                    )
                )
            ];
            case '9': return [
                    'label' => Yii::t('BaseModule.GuiTable', 'EducationTitles'),
                    'settings' => array(
                        'model' => EducationTitle::model(),
                        'add_button' => true
                    )
                ];
            case '10': return [
                    'label' => Yii::t('MainMenu', 'EmployeeSlave'),
                    'settings' => array(
                        'model' => Employee::model(),
                        'columns_type' => 'only_slava'
                    )
                ];
            case '11': 
                $file_document_type_id = null;
                $contract_document_types = Yii::app()->configManager->get('legal.contract', false);
                if(!empty($contract_document_types))
                {
                    $file_document_type_id = $contract_document_types[0];
                }
                return [
                    'label' => 'Prava - Ugovori',
                    'settings' => array(
                        'model' => Contract::model(),
                        'add_button' => [
                            'init_data' => [
                                'File' => [
                                    'document_type_id' => $file_document_type_id
                                ]
                            ]
                        ]
                    )
                ];
            case '14': return [
                    'label' => Yii::t('LegalModule.Employee', 'EmplouyeesWithUnseenConfirmedDays'),
                    'settings' => [
                        'model' => Employee::model(),
                        'columns_type' => 'with_unseen_confirmed_days',
                        'fixed_filter' => [
                            'filter_scopes' => [
                                'isSubordinateOfBoss' => [Yii::app()->user->id, false],
                                'haveCofirmedUncheckedDays',
                                'needToWriteReport'
                            ]
                        ]
                    ]
                ];
            case '15': return [
                    'label' => Yii::t('MainMenu', 'EmployeesSlavaNotSet'),
                    'settings' => array(
                        'model' => Employee::model(),
                        'columns_type' => 'only_slava',
                        'fixed_filter' => array(
                            'have_slava' => true,
                            'filter_scopes' => [
                                'slavaIsNull'
                            ]
                        ),
                    )
                ];
            case '16': return [
                    'label' => 'Osobe kojima ističe zdravstveno osiguranje',
                    'settings' => array(
                        'model' => Employee::model(),
                        'fixed_filter' => [
                            'expiration_date_of_insurance' => ['<', SIMAHtml::UserToDbDate(date('Y-m-d', strtotime("+1 week")))]
                        ]
                    ),
                ];
            case '17': return [
                    'label' => Yii::t('BaseModule.GuiTable', 'EmployeesInsuficientData'),
                    'settings' => array(
                        'model' => Employee::model(),
                        'columns_type' => 'insufficient_data',
                        'fixed_filter' => [
                            'filter_scopes' => [
                                'withInsufficientDataForAccountingMonthsAndToday',
                            ]
                        ]
                    )
                ];
            case '18': return [
                    'label' => Yii::t('BaseModule.GuiTable', 'MunicipalityInsuficientData'),
                    'settings' => array(
                        'model' => Municipality::model(),
                        'columns_type' => 'insufficient_data',
                        'fixed_filter' => [
                            'code' => 'null'
                        ]
                    )
                ];
            case '20': return [
                    'label' => 'Nove potencijalne ponude',
                    'settings' => array(
                        'model' => PotentialBid::model(),
                        'fixed_filter' => [
                            'accepted_filter' => PotentialBid::$NEW
                        ]
                    )
                ];
            case '21': return [
                    'label' => 'Nove ponude',
                    'settings' => array(
                        'model' => Bid::model(),
                        'fixed_filter' => [
                            'bid_status' => Bid::$NEW
                        ]
                    )
                ];
            case '25':
                return [
                    'label' => Yii::t('LegalModule.Warns', 'NotConfirmedEmployeesSickLeavesAndFreeDays'),
                    'settings' => [
                        'model'=>'Absence',
                        'fixed_filter'=>[
                            'filter_scopes' => ['typeIn' => [[Absence::$SICK_LEAVE, Absence::$FREE_DAYS]]],
                            'confirmed' => 'null'
                        ]
                    ]
                ];
            case '26':
                return [
                    'label' => Yii::t('LegalModule.Employee', 'EmplouyeesWithUnseenConfirmedDays'),
                    'settings' => [
                        'model' => Employee::model(),
                        'columns_type' => 'with_unseen_confirmed_days',
                        'fixed_filter' => [
                            'filter_scopes' => [
//                                'isSubordinateOfBoss' => [Yii::app()->user->id, false],
                                'haveCofirmedUncheckedDays',
                                'needToWriteReport'
                            ]
                        ]
                    ]
                ];
            case '27': return [
                'label' => Yii::t('LegalModule.InsurancePolicy', 'InsurancePolicies'),
                'settings' => [
                    'model' => InsurancePolicy::class,
                    'add_button' => [
                        'init_data' => [
                            'File' => [
                                'document_type_id' => Yii::app()->configManager->get('legal.insurance_policy_document_type_id', false),
                            ],
                        ]
                    ]
                ]
            ];
            case '28': return [
                'label' => Yii::t('LegalModule.BusinessOffer', 'BusinessOffers'),
                'settings' => [
                    'model' => BusinessOffer::class,
                    'title' => Yii::t('LegalModule.BusinessOffer', 'BusinessOffers'),
                    'add_button' => [
                        'onhover' => Yii::t('LegalModule.BusinessOffer', 'AddBusinessOffer')
                    ],
                    'additional_options_html' => Yii::app()->controller->widget(SIMAButtonVue::class, [
                        'title' => Yii::t('LegalModule.BusinessOfferVariationPredefinedItem', 'BusinessOfferVariationPredefinedItems'),
                        'onclick' => ["sima.dialog.openActionInDialogAsync", 'guitable', [
                            'get_params' => [
                                'label' => Yii::t('LegalModule.BusinessOfferVariationPredefinedItem', 'BusinessOfferVariationPredefinedItems'),
                                'settings' => [
                                    'model' => BusinessOfferVariationPredefinedItem::class,
                                    'title' => Yii::t('LegalModule.BusinessOfferVariationPredefinedItem', 'BusinessOfferVariationPredefinedItems'),
                                    'add_button' => [
                                        'onhover' => Yii::t('LegalModule.BusinessOfferVariationPredefinedItem', 'AddBusinessOfferVariationPredefinedItem')
                                    ]
                                ]
                            ]
                        ]]
                    ], true)
                ]
            ];
            default:
                throw new SIMAGuiTableIndexNotFound('legal', $index);
        }
    }
}
