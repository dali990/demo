<?php 
class DeliveryType extends SIMAActiveRecord
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
 
    public function tableName()
    {
        return 'public.delivery_types';
    }
    
    public function moduleName()
    {
        return 'legal';
    }
    
    public function __get($column)
    {
        switch ($column)
        {
            case 'DisplayName': return $this->name;
            case 'SearchName': return $this->name;
            default: return parent::__get($column);
        }
    }
    
    public function rules()
    {
    	return array(
    			array('name', 'required', 'message'=>'Polje "{attribute}" mora biti uneto'),
    			array('description', 'safe'),
    	);
    }
    
    public function scopes()
    {
    	return array(
    			'byName'=>array(
    					'order'=>'name, id',
    			),
    	);
    }
    
    public function relations($child_relations = [])
    {
    	return array(
            'elements'=>array(self::HAS_MANY, 'Filing', 'delivery_type_id')
    	);
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'textSearch' : return array(
                'name' => 'text'
            );
            default: return parent::modelSettings($column);
        }
    }
    
//     public function getModelDropList()
//     {
//     	$Model = $this::model();
//     	$scopes = $this->scopes();
//     	if (isset($scopes['recently']))
//     		$Model = $Model->recently();
//     	elseif (isset($scopes['byName']))
//     	$Model = $Model->byName();
//     	$jobOrders = $Model->findAll();
//     	$ret = array();
//     	foreach ($jobOrders as $jobOrder) $ret+=array($jobOrder->id=>$jobOrder->name.'('.$jobOrder->description.')');
//     	return $ret;
//     }

}