<?php

class Contract extends SIMAActiveRecord
{

    public $is_annex;

    /**
     * prilikom filtriranja, ovde se smestaju tagovi
     */
    public $belongs;
    public $belongs_recursive;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'private.contracts';
    }

    public function moduleName()
    {
        return 'legal';
    }

    public function __get($column)
    {
        switch ($column)
        {
            case 'path2': return 'files/file';
            case 'DisplayName': return isset($this->filing) ? $this->filing->filing_number : $this->subject;
            case 'SearchName': return isset($this->filing) ? $this->filing->filing_number . ' (' . $this->subject . ')' : $this->subject;
            case 'TemplateDisplayName':
                $result = Yii::t('LegalModule.Contract', 'TemplateDisplayName', [
                    '{contract_number}' => $this->number,
                    '{contract_date}' => $this->sign_date,
                    '{filing_number}' => $this->partner_filing_number,
                    '{filing_date}' => $this->partner_filing_date
                ]);
                return $result;
            default: return parent::__get($column);
        }
    }

    public function setscan_order($bool)
    {
        if (isset($this->file))
        {
            $this->file->scan_order = $bool;
            $this->file->save();
        }
    }

    public function getscan_order()
    {
        if (isset($this->file))
            return $this->file->scan_order;
        else
            return false;
    }

    public function rules()
    {
        return array(
            array('partner_id', 'required', 'message' => 'Polje "{attribute}" mora biti uneto'),
            array('old, scan_order, confirmed_timestamp, confirmed_user_id', 'safe'),
            array('sign_date, description, contract_partner_status_id, deadline, deadline_description, value_text, subject, vat_text, advance_text, number, subnumber, 
                    partner_filing_number,partner_filing_date, annex_number, registry_id, job_type_id,
                    contract_number,parent_id, confirmed, contract_type_id, currency_id,
                    value, vat, advance, id', 'safe'),
            array('partner_id, deadline, partner_filing_date, number, deadline, parent_id,
                    value, vat, advance, sign_date, id', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => array('insert', 'update')),
            array('value, vat, advance', 'numerical', 'message' => 'Polje "{attribute}" mora biti broj'),
            array('confirmed', 'confirm_check'),
            array('order_by', 'safe'),
            array('belongs, belongs_recursive', 'safe'),
            array('vat_included', 'vat_check'),
            array('is_annex', 'is_annex_check')
        );
    }

    public function confirm_check($attribute, $params)
    {
        if (!$this->hasErrors() && $this->confirmed)
        {
            $contract = Contract::model()->findByPk($this->id);
            if ($contract != null && !$contract->confirmed)
            {
                if ($this->sign_date == null)
                    $this->addError('sign_date', 'Nije upisan datum zaključivanja!');
// 				if ($this->contract_partner_status_id==null) $this->addError('contract_partner_status_id','Nije upisan status ugovarača!');
                if ($this->subject == null)
                    $this->addError('subject', 'Nije upisan predmet ugovora!');
                if ($this->registry_id == null)
                    $this->addError('registry_id', 'Nije upisan registrator!');
                if ($this->contract_type_id == null)
                    $this->addError('contract_type_id', 'Nije upisan tip ugovora!');
            }
        }
    }

    public function vat_check($attribute, $params)
    {
        if (!$this->hasErrors() && !$this->vat_included && $this->value != null && $this->vat == null)
        {
            $this->addError('vat', 'Vrednost PDV se mora upisati ukoliko nije uključen u vrednost');
        }
    }

    public function is_annex_check($attribute, $params)
    {
        if (!$this->hasErrors() && $this->is_annex == 'yes')
        {
            $parent_contract = Contract::model()->findByPk($this->parent_id);
            if ($parent_contract == null || $parent_contract->parent_id != null)
                $this->addError('parent_id', 'Izabrani ugovor nije osnovni');
        }
    }

    public function scopes()
    {
        return array(
            'byName' => array(
                'with' => array(
//                    'partner',
                    'contract_type',
                    'registry',
                ),
            ),
        );
    }

    public function relations($child_relations = [])
    {
        return array(
            'file' => array(self::BELONGS_TO, 'File', 'id'),
            'contract_obligations1' => array(self::HAS_MANY, 'ContractObligation', 'contract_id', 'condition' => 'contract_obligation_type_id=1'),
            'contract_obligations2' => array(self::HAS_MANY, 'ContractObligation', 'contract_id', 'condition' => 'contract_obligation_type_id=2'),
            'partner' => array(self::BELONGS_TO, 'Partner', 'partner_id'),
            'filing' => array(self::BELONGS_TO, 'Filing', 'id',
                'alias' => 'filingincontract',
                'order' => 'filingincontract.date DESC, filingincontract.number DESC'
            ),
            'contract_type' => array(self::BELONGS_TO, 'ContractType', 'contract_type_id'),
            'registry' => array(self::BELONGS_TO, 'Registry', 'registry_id'),
            'parent' => array(self::BELONGS_TO, 'Contract', 'parent_id'),
            'currency' => array(self::BELONGS_TO, 'Currency', 'currency_id'),
// 				'contract_partner_status'=>array(self::BELONGS_TO, 'ContractPartnerStatus', 'contract_partner_status_id'),
            'annexes' => array(self::HAS_MANY, 'Contract', 'parent_id'),
            'confirmed_user' => array(self::BELONGS_TO, 'Employee', 'confirmed_user_id'),
        );
    }

    
    protected function modelOptions(\User $user = null):array
    {
        return ['open', 'download', 'form','delete'];
    }
    
    public function modelSettings($column)
    {
        switch ($column) 
        {
            case 'textSearch': return array_merge(parent::modelSettings($column),[
                'contract_number' => 'text',
                'subject' => 'text',
                'file' => 'relation',
            ]);
            case 'filters': return array_merge(parent::modelSettings($column),[
                'contract_number' => 'text',
                'old_number' => 'text',
                'subject' => 'text',
                'sign_date' => 'date_range',
                'deadline' => 'date_range',
                'deadline_description' => 'text',
                'value_text' => 'text',
                'vat_text' => 'text',
                'advance_text' => 'text',
                'partner_filing_number' => 'text',
                'partner_filing_date' => 'date_range',
                'registry_id' => 'dropdown',
                'contract_type_id' => 'dropdown',
                'belongs' => array('belongs', 'func' => 'filter_belongs'),
                'filing' => 'relation',
                'partner'=>'relation',
                'file' => 'relation'
            ]);
            case 'multiselect_options': return ['orderScan'];
            case 'statuses': return [
                'confirmed' => array(
                    'title' => 'Potvrdjeno',
                    'timestamp' => 'confirmed_timestamp',
                    'user' => array('confirmed_user_id','relName'=>'confirmed_user'),
                ),
            ];
            case 'mutual_forms': return ['base_relation'=>'file'];
            default: return parent::modelSettings($column);
        }
    }

    public function BeforeSave()
    {

        if (isset($this->is_annex) && $this->is_annex == 'no')
        {
            $this->parent_id = null;
        }
        
        return parent::BeforeSave();
    }

    public function filter_belongs($condition)
    {
        if ($this->belongs != null)
        {
            $query = $this->belongs;
            if ($this->belongs_recursive === 'true')
                $query.='$R';

            $condition->mergeWith(SIMAFileStorage::applyQuery($query));
        }
    }

}
