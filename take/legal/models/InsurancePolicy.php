<?php

class InsurancePolicy extends SIMAActiveRecord
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
    
    public function tableName()
    {
        return 'legal.insurance_policies';
    }
    public function moduleName()
    {
        return 'legal';
    }
    
    public function __get($column)
    {
        switch ($column)
        {           
            case 'DisplayName': return $this->policy_number;
            case 'SearchName': return $this->policy_number;

            default: return parent::__get($column);
        }
    }
    
    public function rules()
    {
        return [
            ['policy_number, start_date, expire_date, premium_amount, insurance_company_id', 'required'],
            ['bill_in_id, vinkulation_partner_id', 'safe'],
            ['premium_amount', 'numerical', 'max' => 99999999999999999],
            ['start_date', 'checkStartLessThanEnd', 'on' => ['insert', 'update']],
        ];
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters' : return [
                'policy_number' => 'text',
                'start_date' => 'date_range',
                'expire_date' => 'date_range',
                'file' => 'relation',
                'premium_amount' => 'numeric',
                'insurance_company' => 'relation',
                'vinkulation_partner' => 'relation',
                'bill_in' => 'relation',               
            ];  
            case 'mutual_forms' : return [
                'base_relation' => 'file'
            ];
            default: return parent::modelSettings($column);
        }
    }
    
    public function relations($child_relations = [])
    {
        return parent::relations(array_merge([
            'file' => [self::BELONGS_TO, File::class, 'id'],
            'bill_in' => [self::BELONGS_TO, BillIn::class, 'bill_in_id'],
            'insurance_company' => [self::BELONGS_TO, Company::class, 'insurance_company_id'],
            'vinkulation_partner' => [self::BELONGS_TO, Partner::class, 'vinkulation_partner_id'],
        ], $child_relations));
    }
    
    public function scopes()
    {
        return [
            'last' => ['order' => 'expire_date DESC']
        ];
    }

    public static function getAllForDateRange($begin_date, $end_date)
    {
        return InsurancePolicy::model()->findAll([
            'model_filter' => [
                'expire_date' => $begin_date . '<>' . $end_date
            ]
        ]);
    }

    public function byFixedAssetScope($tag_id, $last=false) 
    {
        $criteria = [];
        if (!empty($tag_id))
        {
            $criteria = [
                'with' => 'file.tags',
                'condition' => 'tags.id = '.$tag_id
            ];
        }
        if($last)
        {   
            $criteria['order'] = 'expire_date DESC';
        }
        $this->getDbCriteria()->mergeWith($criteria);
        return $this;
    }
    
    public function checkStartLessThanEnd()
    {
        if(!$this->hasErrors() && !empty($this->expire_date))
        {
            $start_date = Day::getByDate($this->start_date);
            $expire_date = Day::getByDate($this->expire_date);
            $compare_result = $start_date->compare($expire_date);

            if($compare_result>=0)
            {
                $this->addError('start_date', Yii::t('LegalModule.InsurancePolicy', 'StartUseDateMustBeLessThanEndUseDate'));
            }
        }
    }
    
}
