<?php

class FilingContrib extends SIMAActiveRecord {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'private.filing_contribs';
    }

    public function moduleName() {
        return 'legal';
    }
    
    public function rules() {
        return array(
            array('note_id, contrib_id', 'required', 'message' => 'Polje "{attribute}" mora biti uneto'),
            array('contrib_id','checkUniq')
// 			array('description', 'safe'),
        );
    }
    
    public function checkUniq()
    {
        if (
                intval($this->__old['note_id']) !== intval($this->note_id) || 
                intval($this->__old['contrib_id']) !== intval($this->contrib_id)
           )
        {
            $link = FilingContrib::model()->findByAttributes(array('note_id'=>$this->note_id,'contrib_id'=>$this->contrib_id));
            if (!is_null($link))
            {
                throw new SIMAWarnException('Fajl je vec zadat kao prilog');
            }
        }
    }

//  	public function scopes()
//     {
//     	return array(
//     			'byName'=>array(
//     					'order'=>'name, id',
//     			),
//     	);
//     }

    public function relations($child_relations = [])
    {
        return array(
            'note' => array(self::BELONGS_TO, 'Filing', 'note_id'),
            'contrib' => array(self::BELONGS_TO, 'File', 'contrib_id'),
        );
    }
    
    public function modelSettings($column) 
    {
        switch ($column)
        {
            case 'update_relations' : return array(
                'note',
                'contrib'
            );
            default: return parent::modelSettings($column);
        }
    }
    
    public function beforeSave()
    {               
        $filing = Filing::model()->findByPk($this->note_id);
        $contribs = array();
        foreach ($filing->contribs as $contrib)
        {
            $_contrib = array();
            $_contrib['id'] = $contrib->id;            
            array_push($contribs, $_contrib);
        }
        $_contrib = array();
        $_contrib['id'] = $this->contrib_id;            
        array_push($contribs, $_contrib);
        $filing->contribs = CJSON::decode(CJSON::encode($contribs),false);
        $filing->checkContribsBelongsDiff();
        
        return parent::beforeSave();
    }
    
// 	public function getTypeList()
// 	{
// 		return CHtml::listData(ContactType::model()->findAll(),'id','name');
// 	}
}