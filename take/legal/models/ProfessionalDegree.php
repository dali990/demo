<?php

/**
 * @package SIMA
 * @subpackage Legal
 */
class ProfessionalDegree extends SIMAActiveRecord {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'public.professional_degrees';
    }

    public function rules() {
        return array(
            array('name', 'required', 'message' => 'Polje "{attribute}" mora biti uneto'),
            array('description', 'safe'),
        );
    }

    public function scopes() {
        return array(
            'byName' => array(
                'order' => 'name',
            ),
        );
    }

    public function relations($child_relations = []) 
    {
        if (Yii::app()->isWebApplication() && isset(Yii::app()->company))
        {
            $cid = Yii::app()->company->id;
        }
        else
        {
            $cid = -1;
        }
        return array(
            'employees' => array(self::HAS_MANY, 'Person', 'professional_degree_id', 'condition' => "company_id=$cid"),
            'employees_count'=>array(self::STAT, 'Person', 'professional_degree_id', 'select'=>'count(*)','condition'=>"company_id=$cid"),
        );
    }
    
}
