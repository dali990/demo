<?php
/**
 * Description of ArchiveShelf
 *
 * @author goran-set
 */
class ArchiveShelf extends SIMAActiveRecord
{   
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'legal.archive_shelfs';
    }

    public function moduleName() {
        return 'legal';
    }

    public function __get($column) {
        switch ($column) {
            case 'DisplayName': return "Arhivska polica: ".$this->name;
            case 'SearchName': return $this->DisplayName;
            default: return parent::__get($column);
        }
    }
    
    public function rules() {
        return array(
            array('name','required','message' => 'Polje "{attribute}" mora biti uneto'),
            array('name, description', 'safe'),
            array('id', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => array('insert', 'update'))
        );
    }

    public function relations($child_relations = []) 
    {
        return array(
            'archive_boxes' => array(self::HAS_MANY, 'ArchiveBox', 'archive_shelf_id')
        );
    }
    
    public function scopes() 
    {
        return array(
            'recently' => array(
                'order' => 'id',
            )
        );
    }
    public function modelSettings($column)
    {        
        switch ($column)
        {
            case 'textSearch': return array(
                'name' => 'text'
            );
//            case 'options':  return array_merge($options, array('open'));
         
            default: return parent::modelSettings($column);
        }
    }
    
    public function AfterSave()
    {   
        return parent::AfterSave();
    }

    public function BeforeSave()
    {
        
        return parent::BeforeSave();
    }    
}
