<?php

class BusinessOfferVariationItem extends SIMAActiveRecord
{
    public $is_model_copy = false;
    public $business_offer_variation_predefined_items = null;
    
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
 
    public function tableName()
    {
        return 'legal.business_offer_variation_items';
    }
    
    public function moduleName()
    {
        return 'legal';
    }
    
    public function __get($column) 
    {
        switch ($column) {
            case 'DisplayName': return $this->order_full_path . (!empty($this->description) ? " ({$this->short_description_as_string})" : '');
            case 'SearchName': return $this->DisplayName;
            case 'short_description_as_string': 
                $description = html_entity_decode(strip_tags($this->description));

                return strlen($description) > 100 ? substr($description, 0, 50)."..." : $description;
            case 'has_discount': return !empty($this->discount_percentage) || !empty($this->discount_value);
            case 'value_with_discount': 
                $return = $this->value;
                if (!empty($this->discount_percentage))
                {
                    $return *= ((100.00 - $this->discount_percentage)/100);
                }
                else if (!empty($this->discount_value))
                {
                    $return -= $this->discount_value;
                }
                
                return $return;              
            case 'total_value': 
                $total_value = $this->value;
                foreach ($this->children as $child)
                {
                    $total_value += ($this->quantity * $child->total_value);
                }

                return $total_value;
                
            case 'total_value_for_one': 
                $total_value = $this->total_value;  
                if($this->quantity > 0)
                {
                    return $total_value/$this->quantity;                   
                }
                return $total_value;
                
            case 'total_value_with_discount': 
                $total_value_with_discount = $this->value_with_discount;
                foreach ($this->children as $child)
                {
                    $total_value_with_discount += ($this->quantity * $child->total_value_with_discount);
                }

                return $total_value_with_discount;
            default: return parent::__get($column);
        }
    }
    
    public function rules()
    {
        return [
            ['business_offer_variation_id', 'required'],
            ['order_number, parent_id, value_per_unit, quantity, value, discount_percentage, discount_value, description, order_full_path, short_description', 'safe'],
            ['order_number', 'checkOrderUniq'],
            ['order_number', 'numerical', 'integerOnly' => true, 'min' => 1],
            ['order_number', 'length', 'max' => 8],
            ['value_per_unit', 'checkNumericNumber', 'precision' => 20, 'scale' => 10],
            ['quantity', 'checkNumericNumber', 'precision' => 20, 'scale' => 2],
            ['value', 'checkNumericNumber', 'precision' => 20, 'scale' => 2],
            ['discount_percentage', 'checkNumericNumber', 'precision' => 5, 'scale' => 2],
            ['discount_percentage', 'numerical', 'max' => 100],
            ['discount_value', 'checkNumericNumber', 'precision' => 20, 'scale' => 2],
            ['discount_value', 'checkDiscount'],
            ['parent_id', 'checkParent'],
            ['business_offer_variation_id, parent_id', 'default',
                'value' => null,
                'setOnEmpty' => true,
                'on' => ['insert', 'update']
            ]
        ];
    }
    
    public function checkOrderUniq()
    {
        if (!$this->hasErrors() && $this->isOrderNumberAutomatic() === false)
        {
            if (!isset($this->order_number))
            {
                $this->addError('order_number', Yii::t('LegalModule.BusinessOfferVariationItem', 'OrderNumberCanNotBeEmpty'));
            }
            
            $item_count = BusinessOfferVariationItem::model()->countByAttributes([
                'business_offer_variation_id' => $this->business_offer_variation_id,
                'parent_id' => $this->parent_id,
                'order_number' => $this->order_number
            ]);
            if (($this->isNewRecord && $item_count > 0) || (!$this->isNewRecord && $item_count > 1))
            {
                $this->addError('order_number', Yii::t('LegalModule.BusinessOfferVariationItem', 'OrderNumberAlreadyExists'));
            }
        }
    }
    
    public function checkDiscount()
    {
        if (!$this->hasErrors() && ($this->isNewRecord || $this->columnChanged('discount_percentage') || $this->columnChanged('discount_value')))
        {
            if (!empty($this->discount_percentage) && !empty($this->discount_value))
            {
                $this->addError('discount_value', Yii::t('LegalModule.BusinessOfferVariationItem', 'CanNotFillTogetherDiscountPercentageAndDiscountValue'));
            }
        }
    }
    
    public function checkParent()
    {
        if (!$this->hasErrors() && !empty($this->parent_id) && ($this->isNewRecord || $this->columnChanged('parent_id')))
        {
            if (intval($this->business_offer_variation_id) !== intval($this->parent->business_offer_variation_id))
            {
                $this->addError('parent_id', Yii::t('LegalModule.BusinessOfferVariationItem', 'VariationItemAndParentDontBelongsToSameVariation'));
            }
        }
    }
    
    public function relations($child_relations = [])
    {
        return [
            'business_offer_variation' => [self::BELONGS_TO, BusinessOfferVariation::class, 'business_offer_variation_id'],
            'parent' => [self::BELONGS_TO, BusinessOfferVariationItem::class, 'parent_id'],
            'children' => [self::HAS_MANY, BusinessOfferVariationItem::class, 'parent_id'],
            'children_count' => [self::STAT, BusinessOfferVariationItem::class, 'parent_id', 'select' => 'count(*)']
        ];
    }
    
    public function scopes()
    {
        $alias = $this->getTableAlias();

        return [
            'byName' => [
                'with' => 'business_offer_variation',
                'order' => "business_offer_variation.id, $alias.order_full_path ASC"
            ],
            'byFullPath' => [
                'with' => 'business_offer_variation',
                'order' => "business_offer_variation.id, $alias.order_full_path ASC"
            ],
            'withoutParent' => [
                'condition' => "$alias.parent_id is null"
            ]
        ];
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters' : return array_merge(parent::modelSettings('filters'), [
                'order_number' => 'integer',
                'business_offer_variation' => 'relation',
                'parent' => 'relation',
                'value_per_unit' => 'numeric',
                'quantity' => 'numeric',
                'value' => 'numeric',
                'discount_percentage' => 'numeric',
                'discount_value' => 'numeric',
                'description' => 'text',
                'children' => 'relation',
                'order_full_path' => 'text',
                'short_description' => 'text'
            ]);
            case 'textSearch': return [
                'description' => 'text',
                'order_full_path' => 'text'
            ];
            case 'number_fields': return ['value_per_unit', 'quantity', 'value', 'discount_percentage', 'discount_value', 'total_value_display'];
            case 'GuiTable': return [
                'without_order_number' => true
            ];
            default: return parent::modelSettings($column);
        }
    }
    
    protected function modelOptions(\User $user = null):array
    {
        $options = [];
        
        if ($this->business_offer_variation->confirmed === false && $this->business_offer_variation->sent === false)
        {
            $options[] = 'form';
            $options[] = 'delete';
        }
        
        return $options;
    }
    
    public function beforeSave()
    {
        if ($this->is_model_copy === false)
        {
            if ($this->isOrderNumberAutomatic() === true && ($this->isNewRecord || $this->columnChanged('parent_id')))
            {
                $this->order_number = $this->getMaxOrderForVariationAndGroup($this->business_offer_variation_id, $this->parent_id) + 1;
            }

            if ($this->isNewRecord || $this->columnChanged('order_number') || $this->columnChanged('parent_id'))
            {
                $this->order_full_path = $this->order_number;
                if (!empty($this->parent))
                {
                    $this->order_full_path = $this->parent->order_full_path . '.' . $this->order_number;
                }
            }

            if (
                    $this->isNewRecord || 
                    $this->columnChanged('value_per_unit') || 
                    $this->columnChanged('quantity')
                )
            {
                $this->value = $this->value_per_unit * $this->quantity;
            }
        }

        return parent::beforeSave();
    }
    
    public function afterSave()
    {
        parent::afterSave();
     
        if ($this->is_model_copy === false)
        {
            if (
                    $this->isNewRecord || 
                    $this->columnChanged('value') ||
                    $this->columnChanged('discount_percentage') || 
                    $this->columnChanged('discount_value')
                )
            {
                $this->business_offer_variation->calcValue();
            }

            if ($this->isOrderNumberAutomatic() === true && $this->columnChanged('parent_id'))
            {
                $this->fixAfterOrderNumberForVariationAndGroup($this->__old['order_number'], $this->business_offer_variation_id, $this->__old['parent_id']);
            }
        }
    }
    
    public function afterDelete()
    {
        parent::afterDelete();
        
        $this->business_offer_variation->calcValue();
        
        if ($this->isOrderNumberAutomatic() === true)
        {
            $this->fixAfterOrderNumberForVariationAndGroup($this->order_number, $this->business_offer_variation_id, $this->parent_id);
        }
    }
    
    public function fixAfterOrderNumberForVariationAndGroup($order_number, $business_offer_variation_id, $parent_id = null)
    {
        $model_filter = [
            'business_offer_variation' => [
                'ids' => $business_offer_variation_id
            ]
        ];
        if (!empty($parent_id))
        {
            $model_filter['parent'] = ['ids' => $parent_id];
        }
        else
        {
            $model_filter['filter_scopes'] = 'withoutParent';
        }
        
        $next_items = BusinessOfferVariationItem::model()->findAll([
            'order' => 'order_number ASC',
            'model_filter' => $model_filter,
            'condition' => "order_number > $order_number"
        ]);
        
        $start_order_number = $order_number;
        foreach ($next_items as $next_item)
        {
            $next_item->order_number = $start_order_number;
            $next_item->save();
            $start_order_number++;
        }
    }
    
    public function getMaxOrderForVariationAndGroup($business_offer_variation_id, $parent_id = null)
    {
        $model_filter = [
            'business_offer_variation' => [
                'ids' => $business_offer_variation_id
            ]
        ];
        if (!empty($parent_id))
        {
            $model_filter['parent'] = ['ids' => $parent_id];
        }
        else
        {
            $model_filter['filter_scopes'] = 'withoutParent';
        }

        $last_item = BusinessOfferVariationItem::model()->find([
            'order' => 'order_number DESC',
            'model_filter' => $model_filter
        ]);

        return !empty($last_item) ? $last_item->order_number : 0;
    }
    
    public function isOrderNumberAutomatic()
    {
        return SIMAMisc::filter_bool_var(Yii::app()->configManager->get('legal.business_offer_variation_item_automatic_order_number', false));
    }
    
    public function copyRecursive(BusinessOfferVariation $new_variation, $parent_id = null)
    {
        $new_item = new BusinessOfferVariationItem();
        $new_item->setAttributes($this->getAttributes());
        $new_item->business_offer_variation_id = $new_variation->id;
        $new_item->parent_id = $parent_id;
        $new_item->is_model_copy = true;
        $new_item->save();
        
        foreach ($this->children as $children_item)
        {
            $children_item->copyRecursive($new_variation, $new_item->id);
        }
    }
}