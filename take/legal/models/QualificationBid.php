<?php

class QualificationBid extends Bid
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function BeforeSave()
    {
        $this->qualification = true;
        return parent::BeforeSave();
    }

    public function rules()
    {
    	return array(
            array('partner_id, subject, bid_status', 'required','message'=>'Polje "{attribute}" mora biti uneto'),
//            array('value, best_bid_value', 'numerical','message'=>'Polje "{attribute}" mora biti broj!'),
            array('essential_elements, delivering_time, opening_time, id, building_structure_id', 'safe'),
            array('archive, best_bid_value, best_bid_partner_id, qualification_end', 'safe'),
            array('belongs, belongs_recursive', 'safe'),
            array('partner_id, best_bid_partner_id', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => array('insert', 'update')),
            array('delivering_time, opening_time, qualification_end', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => array('insert', 'update')),
            array('best_bid_value', 'default',
                'value' => 0,
                'setOnEmpty' => true, 'on' => array('insert', 'update')),
            array('qualification', 'default',
                'value' => true,
                'setOnEmpty' => true, 'on' => array('insert', 'update'))//ponuda je uvek kvalifikaciona
        );
    }

    public function defaultScope()
    {
        return array(
            'condition' => $this->getTableAlias(FALSE, FALSE) . '.qualification=true'
        );
    }

//  razlikuje se u view-u (proveri)
//    public function columnOrder($type)
//    {
//        switch ($type)
//        {
//            case 'inFile': return array('partner_id', 'display_name', 'subject');
//
//            default: return array(
//                    'filing_id',
//                    'partner_id', 'display_name', 'subject',
//                    'theme.job_type',
//                    'delivering_time', 'opening_time', 'qualification_end',
//                    'bid_status', 'opening_record_file_id', 'decision_record_file_id',
//                );
//        }
//    }

}
