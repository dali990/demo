<?php 

class RelationshipType extends SIMAActiveRecord
{

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
 
    public function tableName()
    {
        return 'public.relationship_types';
    }
    
    public function moduleName()
    {
        return 'legal';
    }
    
    public function __get($column)
    {
        switch ($column)
        {
            case 'DisplayName': return $this->name;
            default: return parent::__get($column);
        }
    }
    
    public function rules()
    {
    	return array(
            array('name', 'required'),
            array('description', 'safe'),
    	);
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters' : return array(
                'name' => 'text',
                'description' => 'text'
            );
            default: return parent::modelSettings($column);
        }
    }
}