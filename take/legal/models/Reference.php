<?php

class Reference extends SIMAActiveRecord
{
    
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'legal.jobs_references';
    }
    
    public function moduleName()
    {
        return 'legal';
    }
    
    public function __get($column)
    {
        switch ($column)
        {
            case 'DisplayName': return $this->name;
            case 'SearchName': return $this->name;
            case 'path2': return 'legal/legal/reference';      
            default: return parent::__get($column);
        }
    }
    
    public function rules()
    {
        return array(
            array('year_id, name', 'required', 'message' => 'Polje "{attribute}" mora biti uneto'),
            array('description, job_id','safe'),
            array('year_id', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => array('insert', 'update'))
        );
    }
    
    public function relations($child_relations = [])
    {
    	return array(
            'year'=>array(self::BELONGS_TO, 'Year', 'year_id'),
            'file'=>array(self::BELONGS_TO, 'File', 'file_id'),
            'reference_to_company_license'=> array(self::HAS_MANY, 'CompanyLicenseToReference', 'reference_id'),
            'reference_to_person_license'=> array(self::HAS_MANY, 'PersonLicenseToReference', 'reference_id'),
            'company_licenses'=>array(self::MANY_MANY, 'CompanyLicenseToCompany', 'hr.company_licenses_to_jobs_references(reference_id, company_license_id)'),
            'work_licenses'=>array(self::MANY_MANY, 'PersonToWorkLicense', 'hr.persons_working_licences_to_jobs_references(reference_id, person_license_id)'),
            'job'=>array(self::BELONGS_TO, 'Job', 'job_id')
    	);
    }
    
    public function modelSettings($column)
    {
        $options = array();
        $options[] = 'form';
        if (isset($this->id))
        {
            if (Yii::app()->user->checkAccess('OpenDialog',[],$this))    
            {
                $options[] = 'open';
            }
            if (Yii::app()->user->checkAccess('Delete',[],$this))    
            {
                $options[] = 'delete';
            }
        }
        switch ($column)
        {
            case 'filters' : return array(
                'name' => 'text',
                'description' => 'text',                
                'year' => 'relation',
                'file' => 'relation',
                'reference_to_company_license' => 'relation',
                'reference_to_person_license' => 'relation',
                'job' => 'relation'
            );
            case 'options' : return $options;
            case 'form_list_relations' : return array('company_licenses');
            default : return parent::modelSettings($column);
        }
    }
    
}
