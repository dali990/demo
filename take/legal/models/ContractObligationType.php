<?php

class ContractObligationType extends SIMAActiveRecord
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'public.contract_obligation_types';
    }

    public function rules()
    {
        return array(
            array('name', 'required', 'message' => 'Polje "{attribute}" mora biti uneto'),
            array('description', 'safe'),
        );
    }


// 	public function relations($child_relations = [])
// 	{
// 		return array(
// 				'contracts'=>array(self::HAS_MANY, 'Contract', 'job_type_id'),
// 		);
// 	}
// 	public function merge_model($child_model)
// 	{
// 		foreach ($child_model->contracts as $contract)
// 		{
// 			$contract->job_type_id = $this->id;
// 			$contract->update();
// 		}
// 		$child_model->delete();
// 	}
}
