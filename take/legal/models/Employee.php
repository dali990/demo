<?php

/**
 * @package SIMA
 * @subpackage Base
 * klasa koja predstavlja jednog zaposlenog koji ima nalog
 */
class Employee extends SIMAActiveRecord
{
    public static $PAYOUT_TYPE_BANK_ACCOUNT = 'bank_account';
    public static $PAYOUT_TYPE_CACHE = 'cache';
    
    public static $payout_types = [
        'bank_account' => 'Bankovni račun', 
        'cache' => 'Keš'
    ];
    
    public static $SIMA = 'SIMA';
    public static $E_MAIL = 'E_MAIL';
    public static $PRINT = 'PRINT';
    
    public static $document_send_types = [
        'SIMA' => 'U SIMI',
        'E_MAIL' => 'E-mail',
        'PRINT' => 'Štampanje',
    ];
    
    public static $PAYOUT_IDENTIFICATION_TYPE_JMBG = 1;
    public static $PAYOUT_IDENTIFICATION_TYPE_REFUGEE_CARD = 2;
    public static $PAYOUT_IDENTIFICATION_TYPE_PASSPORT = 3;
    public static $PAYOUT_IDENTIFICATION_TYPE_EXTERNAL_ID = 4;
    public static $PAYOUT_IDENTIFICATION_TYPE_OTHER = 9;
    
    
    var $access;
    public $shared_mail_id;
    public $age;
    public $under_contract = null;
    public $active_contracts = null;
    public $last_work_date = null;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'public.employees';
    }

    public function moduleName()
    {
        return 'legal';
    }
    
    public function __get($column)
    {
        switch ($column)
        {
            case 'path2': return 'addressbook/partner';
                
//            case 'DisplayName': return isset($this->person)?$this->person->firstname . ' ' . $this->person->lastname:'';
            case 'SearchName': return isset($this->person)?$this->person->SearchName:'';
//            case 'firstname': 		return $this->person->firstname;
//            case 'lastname': 		return $this->person->lastname;
            default: return parent::__get($column);
        }
    }
    
    public function relations($child_relations = [])
    {
//        $uniq = SIMAHtml::uniqid();
        
//        $sima_session = 
        
        return parent::relations(array_merge([
            'person' => array(self::BELONGS_TO, 'Person', 'id'),
            'user' => array(self::BELONGS_TO, 'User', 'id'),
            'medical_reports' => array(self::HAS_MANY, 'MedicalReports', 'id'),
            'municipality' => array(self::BELONGS_TO, 'Municipality', 'municipality_id'),
            'last_work_contract' => array(self::HAS_ONE, 'WorkContract', 'employee_id', 'order' => 'start_date DESC'),
            'work_contracts' => array(self::HAS_MANY, 'WorkContract', 'employee_id'),
            'work_contracts_cnt' => array(self::STAT, 'WorkContract', 'employee_id', 'select' => 'count(*)'),
            'active_work_contract' => array(self::HAS_ONE, 'WorkContract', 'employee_id', 'scopes'=>['activeFromTo'=>[null,null]]),
            'first_work_contract' => array(self::HAS_ONE, 'WorkContract', 'employee_id',
                'order' => 'first_work_contract.start_date ASC'
            ),
            'last_work_contract' => array(self::HAS_ONE, 'WorkContract', 'employee_id',
                'order' => 'last_work_contract.start_date DESC'
            ),
//            'unconfirmed_files_count' => array(self::STAT, 'File', 'responsible_id', 'condition' => 'confirmed=false', 'select' => 'count(*)'),
            
//            'dayreports'=>array(self::HAS_MANY, 'TaskReport', 'employee_id'),
            
            'paycheck_bonus_paygrade' => [self::BELONGS_TO, 'BonusPaygrade', 'paycheck_bonus_paygrade_id'],
            
            'absences'=>array(self::HAS_MANY, 'Absence', 'employee_id'),
            'confirmed_absences'=>array(self::HAS_MANY, 'Absence', 'employee_id',
                'condition'=>'confirmed=true'
            ),
            'driver_license_categories'=>array(self::MANY_MANY, 'DriverLicenseCategory','employees_to_driver_license_categories(employee_id, driver_license_category_id)'),
//            'qualitychecking_tasks' => array(self::HAS_MANY, 'Task', 'qualitychecker_id',
//                'condition'=>"qualitycheck_time is not null"),
            'profession' => array(self::BELONGS_TO, 'Profession', 'profession_id'),
            'email_account'=>array(self::BELONGS_TO, 'EmailAddress', 'email_account_id'),
        ], $child_relations));
    }

    public function rules()
    {
        return array(
            ['order_in_company', 'required'],
            array('company, professional_degree_id, medical_reports_id, blood_group, driver_license_category_id, '
                . 'driver_license_expire, driver_license_issued_by, driver_license_number, id_card_number, '
                .'passport_number, gender, have_slava, national_event_slava_id, email_account_id, document_send_type', 'safe'),
            array('employment_code, order_by, scopes,config_params,access, municipality_id, under_contract, scopes, '
                . 'expiration_date_of_insurance, paycheck_svp, profession_id, payout_type, personal_points, '
                . 'payout_identification_type, payout_identification_value, paycheck_bonus_paygrade_id', 'safe'),
            array('old_work_exp', 'numerical', 'message' => 'Polje {attribute} mora biti broj', 'min' => 0),
            array('paycheck_svp', 'numerical', 'integerOnly'=>true),
            array('paycheck_svp', 'length', 'min'=>6, 'max'=>6),
            array('id','safe'),
            array('blood_group, gender','default',
                'value'=>null,
                'setOnEmpty'=>true,'on'=>array('insert','update')),
            array('document_send_type', 'checkDocumentSendType')
        );
    }
    
    public function checkDocumentSendType()
    {
        $check_document_send_by_email = false;
        if($this->columnChangedOrNew('document_send_type'))
        {
            if ($this->document_send_type === Employee::$SIMA && !isset($this->person->user))
            {
                $this->addError('document_send_type', 'Način slanja dokumentacije ne može biti u SIMI jer zaposleni nije korisnik sistema.');
            }
            else
            {
                $check_document_send_by_email = true;
            }
        }
//        else if (intval($this->__old['email_account_id']) !== intval($this->email_account_id))
        else if (
            (
                $this->isNewRecord
                &&
                !empty($this->email_account_id)
            )
            ||
            $this->columnChanged('email_account_id')
        )
        {
            $check_document_send_by_email = true;
        }
        if ($check_document_send_by_email === true)
        {
            if ($this->document_send_type === Employee::$E_MAIL && !isset($this->email_account))
            {
                $this->addError('document_send_type', 'Način slanja dokumentacije ne može biti email-om jer nije postavljen email nalog za zaposlenog.');
            }
        }
    }
    
    public function afterFind()
    {
        parent::afterFind();
        
        if (!empty($this->person->birthdate))
        {
            $begin = new DateTime( $this->person->birthdate );
            $end = new DateTime(  );

            $interval = new DateInterval('P1Y');
            $daterange = new DatePeriod($begin, $interval ,$end);

            $this->age = 0;
            foreach($daterange as $date){
                $this->age ++;
            }
        }
    }
    
    
    public function BeforeSave()
    {
        if(is_null($this->personal_points))
        {
            $this->personal_points = 0;
        }
        
        if(intval($this->payout_identification_type) === Employee::$PAYOUT_IDENTIFICATION_TYPE_JMBG)
        {
            $this->payout_identification_value = $this->person->JMBG;
        }
        
        return parent::BeforeSave();
    }
    
    public function AfterSave()
    {  
//        if($this->last_report_day_id!=$this->__old['last_report_day_id'])
        if(!$this->isNewRecord && $this->columnChanged('last_report_day_id'))
        {
            Yii::app()->warnManager->sendWarns(Yii::app()->user->id, 'WARN_UNCONFIRMED_DAYS');
        }
        
        if(
//            (isset( $this->employment_code) && $this->employment_code!=$this->__old['employment_code'])
//            || (isset( $this->paycheck_svp) && $this->paycheck_svp!=$this->__old['paycheck_svp'])
//            || (isset( $this->municipality_id) && $this->municipality_id!=$this->__old['municipality_id']))
            (isset( $this->employment_code) && $this->columnChangedOrNew('employment_code'))
            || (isset( $this->paycheck_svp) && $this->columnChangedOrNew('paycheck_svp'))
            || (isset( $this->municipality_id) && $this->columnChangedOrNew('municipality_id')))
        {
            Yii::app()->warnManager->sendWarns('HRInsufficientDataForEmployees', 'WARN_INSUFFICIENT_DATA_EMPOLOYEES');
        }
        
//        if($this->national_event_slava_id!=$this->__old['national_event_slava_id'])
        if($this->columnChangedOrNew('national_event_slava_id'))
        {
            Yii::app()->warnManager->sendWarns('HREmployeeSlava', 'WARN_EMPLOYEES_SLAVA_NOT_SET');
        }
        
        return parent::AfterSave();
    }
    
    public function afterDelete()
    {
        parent::afterDelete();
        
        PartnerToPartnerList::remove(PartnerList::getSystemList('former_employees'), $this->person->partner);
        PartnerToPartnerList::remove(PartnerList::getSystemList('employees'), $this->person->partner);
    }
    
    public function scopes()
    {
        $uniq = SIMAHtml::uniqid();
        $local_alias = $this->getTableAlias();
        $work_contracts_table_name = WorkContract::model()->tableName();
        $alias = $this->getTableAlias();
//        $curr_user = Yii::app()->user->id;
        return array(
            'byName' => array('order' => "$local_alias.username, $local_alias.id"),
            'byCompanyOrder' => array(
                'order' => "$local_alias.order_in_company"
            ),
            'withInsufficientData' => array(
                'join' => 'left join '.Person::model()->tableName().' per'.$uniq.' on '.$local_alias.'.id=per'.$uniq.'.id '
                        .'left join '.Partner::model()->tableName().' par'.$uniq.' on '.$local_alias.'.id=par'.$uniq.'.id ',
                'condition' => $local_alias.'.employment_code IS NULL OR '
                            .$local_alias.'.paycheck_svp IS NULL OR '
                            .$local_alias.'.municipality_id IS NULL OR '
                            .'per'.$uniq.'."JMBG" IS NULL OR '
                            .'length(per'.$uniq.'."JMBG") < 13 OR '
                            .'par'.$uniq.'.main_address_id IS NULL OR '
                            .'(payout_type=\''.Employee::$PAYOUT_TYPE_BANK_ACCOUNT.'\' AND par'.$uniq.'.main_bank_account_id IS NULL)'
                            .'OR exists (select 1 from '.$work_contracts_table_name.' wc1, '.$work_contracts_table_name.' wc2 '
                                        . 'where per'.$uniq.'.id=wc1.employee_id and per'.$uniq.'.id=wc2.employee_id '
                                        . 'and wc1.start_date=wc2.start_date and wc1.id<wc2.id limit 1)'
            ),
            'active' => array(
                'condition' => 'exists (SELECT 1 FROM '.WorkContract::model()->tableName().' '.$uniq.'wc '
                                        . 'WHERE '.$uniq.'wc.employee_id='.$local_alias.'.id '
                                            . ' AND '.$uniq.'wc.start_date<=now() '
                                            . ' and '.$uniq.'wc.id not in (select work_contract_id from '.CloseWorkContract::model()->tableName().' where end_date<=now() and work_contract_id is not null) '
                                            . ' and '.$uniq.'wc.id in (select id from (select id,rank() over (partition by employee_id ORDER BY start_date DESC) from '.WorkContract::model()->tableName().' where start_date<=now()) aa where rank=1)'
                                            . ' and ('.$uniq.'wc.end_date>now() or '.$uniq.'wc.end_date is null))'
            ),
            'slavaIsNull' => [
                'condition' => $local_alias.'.national_event_slava_id IS NULL'
            ]
        );
    }
    
    public function needToWriteReport()
    {
        $no_day_reports_person_ids = Yii::app()->configManager->get('admin.no_day_reports_persons', false);
        if(!empty($no_day_reports_person_ids))
        {
            $no_day_reports_person_ids_string = implode(',', $no_day_reports_person_ids);

            $alias = $this->getTableAlias();

            $this->getDbCriteria()->mergeWith([
                'condition' => $alias.'.id NOT IN ('.$no_day_reports_person_ids_string.')'
            ]);
        }
        
        return $this;
    }
    
    /**
     * zaposleni koji nemaju administrativnu zabranu u grupi
     * @param type $suspension_group_id
     * @return \Employee
     */
    public function noSuspensionInSuspensionGroups($suspension_group_id)
    {        
        $uniq = SIMAHtml::uniqid();
        $alias = $this->getTableAlias();
        $suspensionTable = Suspension::model()->tableName();
        
        $this->getDbCriteria()->mergeWith( [
            'condition' =>"NOT EXISTS (SELECT 1 "
                . "FROM $suspensionTable s$uniq "
                . "WHERE s$uniq.employee_id=$alias.id "
                    . "AND s$uniq.suspension_group_id=$suspension_group_id "
                . "LIMIT 1)"
        ] );
        
        return $this;
    }
    
    /**
     * Skope gleda za danasnji datum i prosiruje opseg za postojece finansijske mesece
     * Ukoliko postoji samo jedan finansijski mesec pre godinu dana, onda posmatra celu godinu
     * @return \Employee
     */
    public function withInsufficientDataForAccountingMonthsAndToday()
    {
        $allAccountingMonths = AccountingMonth::model()->findAll();
        
        if(!isset($allAccountingMonths) 
                || count($allAccountingMonths)===0)
        {
//            $criteria = new SIMADbCriteria([
//                'Model' => 'Employee',
//                'model_filter' => [
//                    'filter_scopes' => [
//                        'withInsufficientData',
//                    ],
//                ],
//            ]);
            $criteria = new CDbCriteria([
                'scopes' => [
                    'withInsufficientData'
                ]
            ]);
        }
        else
        {
            $minDate = Day::get()->day_date;
            $minDateTime = new DateTime();
            $maxDate = Day::get()->day_date;
            $maxDateTime = new DateTime();

            foreach($allAccountingMonths as $am)
            {       
                $first_month_day = $am->base_month->firstDay()->day_date;
                $firstDateTime = new DateTime($first_month_day);
                $last_month_day = $am->base_month->lastDay()->day_date;
                $lastDateTime = new DateTime($last_month_day);

                if(!isset($minDate) || $minDateTime > $firstDateTime)
                {
                    $minDate = $first_month_day;
                    $minDateTime = $firstDateTime;
                }
                if(!isset($maxDate) || $maxDateTime < $lastDateTime)
                {
                    $maxDate = $last_month_day;
                    $maxDateTime = $lastDateTime;
                }
            }

            $period = $minDate.'<>'.$maxDate;

//            $criteria = new SIMADbCriteria([
//                'Model' => 'Employee',
//                'model_filter' => [
//                    'filter_scopes' => [
//                        'withInsufficientData',
//                    ],
//                    'active_contracts' => $period
//                ]
//            ]);
            $criteria = new SIMADbCriteria([
                'Model' => 'Employee',
                'model_filter' => [
                    'active_contracts' => $period
                ]
            ]);
            $criteria->mergeWith([
                'scopes' => [
                    'withInsufficientData'
                ]
            ]);
        }
        
        $this->getDbCriteria()->mergeWith($criteria);
        
        return $this;
    }
    
    /**
     * Samo osobe za koje postoji vazeci ugovor o radu
     * @param Month $m
     */
    public function haveWorkContractInMonth($m)
    {
        if(gettype($m) !== "object")
        {
            $m = ModelController::GetModel('Month', $m);
        }
        
        $alias = $this->getTableAlias();
        $first_day = SIMAHtml::UserToDbDate($m->firstDay()->day_date);
        $last_day = SIMAHtml::UserToDbDate($m->lastDay()->day_date);
        
        $close_work_contracts_table_name = CloseWorkContract::model()->tableName();
        $work_contracts_table_name = WorkContract::model()->tableName();
        
        $this->getDbCriteria()->mergeWith( [
            'condition' => "$alias.id in (
                    select e.id
                    from $work_contracts_table_name wc --svi ugovori o radu
                        left join public.employees e on wc.employee_id=e.id -- sa zaposlenim
                        left join $close_work_contracts_table_name cwc on cwc.work_contract_id = wc.id -- i ugovoru o raskidu
                    where                 
                        start_date<=:last_day
                        and 
                        (
                            ( -- ugovori na odredjeno
                                wc.end_date is not null 
                                and
                                wc.end_date >= :first_day --raskid je u mesecu obracuna ili kasnije
                                and
                                (
                                    cwc.end_date is null --nemaju ugovor o raskidu
                                    or
                                    cwc.end_date >= :first_day ---raskid je u mesecu obracuna ili kasnije
                                )
                            )
                            or 
                            ( -- ugovori na neodredjeno
                                wc.end_date is null 
                                and
                                (
                                    cwc.end_date is null --nemaju ugovor o raskidu
                                    or
                                    cwc.end_date >= :first_day --imaju ugovor o raskidu, ali je raskid u mesecu obracuna ili kasnije
                                )
                            )
                        )
                        and wc.id in (select id from private.filing_book)
                )",
            'params' => [ 
                ':first_day' => $first_day,
                ':last_day' => $last_day,
            ],
        ] );
        return $this; 
    }
    
//    public function haveActiveWorkContractInMonth(Month $month)
    public function haveActiveWorkContractInMonth($month)
    {
        if(gettype($month) !== "object")
        {
            $month = ModelController::GetModel('Month', $month);
        }
        
        $criteria = new SIMADbCriteria([
            'Model' => Employee::model(),
            'model_filter' => [
                'work_contracts' => [
                    'signed' => true,
                    'active_at' => $month
                ]
            ]
        ]);
        
        $this->getDbCriteria()->mergeWith($criteria);
        
        return $this; 
    }
    
    public function haveActiveWorkContractInMonthPeriod(array $month_period)
    {
        $criteria = new SIMADbCriteria([
            'Model' => Employee::model(),
            'model_filter' => [
                'work_contracts' => [
                    'signed' => true,
                    'active_at' => $month_period
                ]
            ]
        ]);
        
        $this->getDbCriteria()->mergeWith($criteria);
        
        return $this; 
    }
    
    public function haveCofirmedUncheckedDays()
    {
        $alias = $this->getTableAlias();
        $userTable = User::model()->tableName();
        $daysTable = Day::model()->tableName();
        $uniq = SIMAHtml::uniqid();
        
        $this->getDbCriteria()->mergeWith([
            'condition' => "EXISTS ("
                . " SELECT 1 "
                . " FROM $userTable ut$uniq "
                    . " LEFT JOIN $daysTable dt1$uniq ON dt1$uniq.id=ut$uniq.last_report_day_id "
                    . " LEFT JOIN $daysTable dt2$uniq ON dt2$uniq.id=ut$uniq.last_boss_confirm_day_id"
                . " WHERE ut$uniq.id=$alias.id "
                    . " AND ut$uniq.last_report_day_id IS NOT NULL "
                    . " AND (last_boss_confirm_day_id IS NULL OR ("
                            . " last_boss_confirm_day_id IS NOT NULL "
                            . " AND "
                            . " dt1$uniq.day_date>dt2$uniq.day_date))"
                . " LIMIT 1"
            . ")"
        ]);
        
        return $this; 
    }
    
    public function isSubordinateOfBoss($boss_id, $all_employees=false)
    {
        $boss_emp = ModelController::GetModel('Employee', $boss_id);
        $subordinate_ids = $boss_emp->getSubordinate($all_employees);
        
        if(empty($subordinate_ids)
            || (count($subordinate_ids) === 1 && empty($subordinate_ids[0])))
        {
            $subordinate_ids = [-1];
        }
        
        $criteria = new SIMADbCriteria([
            'Model' => Employee::model(),
            'model_filter' => [
                'ids' => $subordinate_ids
            ]
        ]);
        
        $this->getDbCriteria()->mergeWith($criteria);
        
        return $this; 
    }

    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters' : return array_merge(parent::modelSettings($column),array(
                'order_in_company' => 'integer',
                'maildir' => 'text',
                'person' => 'relation',
                'display_name' => 'text',
                'under_contract' => array('dropdown', 'func' => 'filter_under_contract'),
                'active_work_contract' => 'relation',
                'access' => array('func' => 'filter_access'),
//                'shared_mail_id'=>array('func'=>'filter_shared_mail_id'),
                'blood_group'=>'dropdown',                
                'driver_license_expire'=>'date_range',
                'driver_license_issued_by'=>'text',
                'driver_license_number'=>'text',
                'id_card_number'=>'text',
                'passport_number'=>'text',
                'gender'=>'dropdown',
                'expiration_date_of_insurance'=>'date_range',
                'profession'=>'relation',
                'work_contracts'=>'relation',
                'active_contracts' => array('date_range', 'func' => 'filter_active_contracts'),
                'payout_type' => 'dropdown',
                'last_work_contract' => 'relation',
                'last_work_date' => array('date_range', 'func' => 'filter_last_work_date'),
                'slava' => 'relation',
                'paychecks' => 'relation',
                'have_slava' => 'boolean',
                'email_account' => 'relation',
                'document_send_type' => 'dropdown'
            ));
            case 'textSearch' : return array(
                'person' => 'relation',
            );
            case 'options' :  
                $options=array('open');
                if(Yii::app()->isWebApplication() && Yii::app()->user->checkAccess('modelEmployeeFormOpen'))
                {
                    array_push($options,'form');   
                }
                if(Yii::app()->isWebApplication() && Yii::app()->user->checkAccess('modelEmployeeDelete'))
                {
                    array_push($options,'delete');   
                }
                return $options;
            case 'form_list_relations' : return array('driver_license_categories');
            case 'update_relations': return [
                'person'
            ];
            default: return parent::modelSettings($column);
        }
    }
    
    public function filter_active_contracts($condition)
    {
        if(isset($this->active_contracts))
        {
            $temp_condition = $this->getWorkContractActiveAtDateRangeCondition($this->active_contracts);

            $condition->mergeWith($temp_condition, true);
        }
    }
    
    public function filter_last_work_date($condition)
    {
        if(isset($this->last_work_date))
        {
            $temp_condition = $this->getLastWorkDayAtDateRangeCondition($this->last_work_date);
            
            $condition->mergeWith($temp_condition, true);
        }
    }
    
    public function getWorkContractActiveAtDateRangeCondition($dateRangeParam, $uniq=null)
    {        
        $temp_condition = new CDbCriteria();
        if(!isset($uniq))
        {
            $uniq = SIMAHtml::uniqid();
        }
        $alias = $this->getTableAlias();
        $wc_tablename = WorkContract::model()->tableName();
        
        $startDate = null;
        $endDate = null;
        
        $exploded = explode('<>', $dateRangeParam);
        if (count($exploded) == 1)
        {
            $dateRangeParamToDb = SIMAHtml::UserToDbDate(trim($dateRangeParam));
            
            $startDate = $dateRangeParamToDb;
            $endDate = $dateRangeParamToDb;
        }
        else if (count($exploded) == 2)
        {
            $startDate = SIMAHtml::UserToDbDate(trim($exploded[0]));
            $endDate = SIMAHtml::UserToDbDate(trim($exploded[1]));
        }
        else
        {
            throw new Exception(Yii::t('LegalModule.Employee', 'IllegalDateRangeParamFormat'));
        }
        
//        $temp_condition->condition = "exists (select 1 
//                from $wc_tablename wc$uniq
//                where wc$uniq.employee_id=$alias.id and
//                (
//                    (wc$uniq.start_date <= :enddate$uniq 
//                        AND 
//                        (
//                            base.min_dates(base.min_dates(base.min_dates(wc$uniq.end_date, (select end_date 
//                                                            from hr.close_work_contracts cwc 
//                                                            where cwc.work_contract_id=wc$uniq.id 
//                                                            limit 1)),
//                                            (select start_date from $wc_tablename wc_next
//                                            where wc_next.employee_id=wc$uniq.employee_id 
//                                            and wc_next.start_date>wc$uniq.start_date
//                                            order by wc_next.start_date ASC
//                                            limit 1)), :startdate$uniq)=:startdate$uniq
//                        )
//                    )
//
//                )
//                limit 1
//        )";
        $temp_condition->condition = "exists (select 1 
                from $wc_tablename wc$uniq
                where wc$uniq.employee_id=$alias.id and
                (
                    (wc$uniq.start_date <= :enddate$uniq 
                        AND 
                        (
                            base.min_dates(wc$uniq.calculated_end_date, :startdate$uniq)=:startdate$uniq
                        )
                    )

                )
                limit 1
        )";
        $temp_condition->params = [
            ":startdate$uniq" => $startDate, 
            ":enddate$uniq" => $endDate
        ];
        
        return $temp_condition;
    }
    
    public function getWorkContractActiveAt($active_at, $for_payout=true)
    {
        $model_filter = [
            'employee' => [
                'ids' => $this->id
            ],
            'signed' => true,
            'for_payout' => $for_payout,
            'active_at' => $active_at,
        ];

        if($for_payout === true || $for_payout === false)
        {
            $model_filter['for_payout'] = $for_payout;
        }

        $wcCriteria = new SIMADbCriteria([
            'Model' => 'WorkContract',
            'model_filter' => $model_filter
        ]);
        
        $workContract = WorkContract::model()->find($wcCriteria);
        
        return $workContract;
    }
    
    public function getLastWorkDayAtDateRangeCondition($dateRangeParam, $uniq=null)
    {
        $temp_condition = new CDbCriteria();
        if(!isset($uniq))
        {
            $uniq = SIMAHtml::uniqid();
        }
        $alias = $this->getTableAlias();
        $wc_tablename = WorkContract::model()->tableName();
        $cwc_tablename = CloseWorkContract::model()->tableName();
        $work_contracts_table_name = WorkContract::model()->tableName();
        
        $temp_condition->join = "left join $wc_tablename wc$uniq on $alias.id=wc$uniq.employee_id "
                . "left join $cwc_tablename cwc$uniq on wc$uniq.id=cwc$uniq.work_contract_id";
        
        $condition = "wc$uniq.id IN (select id "
                                    . "from $wc_tablename wc2$uniq "
                                    . "where wc2$uniq.employee_id=$alias.id order by wc2$uniq.start_date desc limit 1)";

        $exploded = explode('<>', $dateRangeParam);
        if (count($exploded) == 1)
        {
            $condition .= " AND ((wc$uniq.end_date IS NOT NULl AND cwc$uniq.end_date IS NULL AND wc$uniq.end_date = :param$uniq) "
                                        . "OR (cwc$uniq.end_date IS NOT NULL AND cwc$uniq.end_date = :param$uniq))
                and wc$uniq.id in (select id from (select id,rank() over (partition by employee_id ORDER BY start_date DESC) from $work_contracts_table_name where start_date<= :param$uniq ) aa where rank=1)";
            $params[':param'.$uniq] = SIMAHtml::UserToDbDate($dateRangeParam);
        }
        else if (count($exploded) == 2)
        {
            $condition .= " AND ((wc$uniq.end_date IS NOT NULL AND cwc$uniq.end_date IS NULL AND wc$uniq.end_date >= :param1$uniq AND wc$uniq.end_date <= :param2$uniq) "
                                            . "OR (cwc$uniq.end_date IS NOT NULL AND cwc$uniq.end_date >= :param1$uniq AND cwc$uniq.end_date <= :param2$uniq))
                and wc$uniq.id in (select id from (select id,rank() over (partition by employee_id ORDER BY start_date DESC) from $work_contracts_table_name where start_date<= :param2$uniq ) aa where rank=1)";
            $params[":param1$uniq"] = SIMAHtml::UserToDbDate(trim($exploded[0]));
            $params[":param2$uniq"] = SIMAHtml::UserToDbDate(trim($exploded[1]));
        }
        
        $temp_condition->condition = $condition;
        $temp_condition->params = $params;
                
        return $temp_condition;
    }
    
    public function filter_under_contract($condition)
    {
        if (!is_null($this->under_contract))
        {
            $temp_condition = null;
                                    
            if ($this->under_contract == 'true')
            {                
                $temp_condition = new SIMADbCriteria([
                    'Model' => 'Employee',
                    'model_filter' => [
                        'active_work_contract' => [
                            'activeFromTo' => [null, null]
                        ]
                    ]
                ], true);
            }
            else
            {
                $active_condition = new SIMADbCriteria([
                    'Model' => 'Employee',
                    'model_filter' => [
                        'active_work_contract' => [
                            'activeFromTo' => [null, null]
                        ]
                    ]
                ], true);
                
                $all_condition = new SIMADbCriteria([
                    'Model' => 'Employee',
                    'model_filter' => []
                ], true);
                
                $active_employees_ids = explode(',', $active_condition->ids);
                $all_employees_ids = explode(',', $all_condition->ids);
                
                
                $inactive_ids = [];
                foreach($all_employees_ids as $alle)
                {
                    $found = false;
                    foreach($active_employees_ids as $activee)
                    {
                        if($alle === $activee)
                        {
                            $found = true;
                            unset($activee);
                            break;
                        }
                    }
                    
                    if($found === false)
                    {
                        $inactive_ids[] = $alle;
                    }
                    
                    unset($alle);
                }
                                
                $temp_condition = new SIMADbCriteria([
                    'Model' => 'Employee',
                    'model_filter' => [
                        'ids' => $inactive_ids
                    ]
                ]);
            }
            
            $condition->mergeWith($temp_condition, true);
        }
        
        if (isset($this->under_contract))
            $this->under_contract = null;
    }

    public function filter_access($condition)
    {
        if ($this->access != null)
        {
            $condition->mergeWith(Yii::app()->authManager->getUserConditionByAcces($this->access));
        }
    }
    
    public function workInSector($sector_id)
    {
        $work_in_sector=false;
        if(isset($this->active_work_contract))
        {
            $work_position=$this->active_work_contract->work_position;
            if($work_position->company_sector_id==$sector_id)
            {
                $work_in_sector=true;
            }
        }
        
        return $work_in_sector;
    }
    
    public function getSubordinate($all_employees=false)
    {
        return SIMASQLFunctions::getEmployeeSubordinateIds($this, $all_employees);
    }
    
    public function getSubordinateConfirmedUnseenDaysCount()
    {
        $result = 0;
        
        $criteria = new SIMADbCriteria([
            'Model' => Employee::model(),
            'model_filter' => [
                'filter_scopes' => [
                    'isSubordinateOfBoss' => [$this->id],
                    'haveCofirmedUncheckedDays',
                    'needToWriteReport'
                ]
            ]
        ]);
        $subordinate_employees = Employee::model()->findAll($criteria);
        foreach($subordinate_employees as $subordinate_employee)
        {
            $confirmedUnseenDaysCount = $subordinate_employee->getConfirmedUnseenDaysCount();

            $result += $confirmedUnseenDaysCount;
        }
        
        return $result;
    }
    
    public function getConfirmedUnseenDaysCount()
    {
        $result = 0;
                
        if(isset($this->user->first_report_day) && isset($this->user->last_report_day))
        {
            $dStart = new DateTime($this->user->last_report_day->day_date);
            
            $dEnd = new DateTime($this->user->first_report_day->day_date);
            if(isset($this->user->last_boss_confirm_day))
            {
                $dEnd = new DateTime($this->user->last_boss_confirm_day->day_date);
            }
            $dDiff = $dStart->diff($dEnd);

            $result = $dDiff->days;
        }
        
        return $result;
    }
    
    public function isPayoutByCache()
    {
        return $this->payout_type === Employee::$PAYOUT_TYPE_CACHE;
    }
    
    public function getWorkExperience(Day $first_day=null, Day $last_day=null)
    {
        $worked_days = 0;
        
        $work_contracts = $this->work_contracts;
        foreach($work_contracts as $wc)
        {
            if($wc->for_payout)
            {
                $worked_days += $wc->getWorkExperience($first_day, $last_day);
            }
        }
        
        return $worked_days;
    }
    
    /**
     * Vraca sva aktivna resenja za zaposlenog. 
     * @param string $start_date - ako nije prosledjen onda se gleda za trenutni datum
     * @return arrays
     */
    public function getActiveAnnualLeaveDecisions($start_date=null)
    {
//        $annual_leave_decisions = [];
//        if (isset($this->active_work_contract))
//        {
            if (!isset($start_date))
            {
                $start_date = date("Y-m-d");
            }
            $criteria = new SIMADbCriteria([
                'Model'=>'AnnualLeaveDecision',
                'model_filter'=>[
                    'employee'=>[
                        'ids'=>$this->id
                    ],
                    'year'=>[
                        'hr' => [
                            'must_be_used_until'=>['>=', SIMAHtml::UserToDbDate($start_date)]
                        ]
                    ],
                    //MilosS: ovo ne treba da bude, zato sam zakomentarisao
//                    'work_contract'=>[
//                        'ids'=>$this->active_work_contract->id
//                    ],
                    'order_by'=>'year.hr.must_be_used_until ASC' //sortiramo resenja tako da prvo bude najstarije
                ]
            ]);

            $annual_leave_decisions = AnnualLeaveDecision::model()->findAll($criteria);
//        }
        
        return $annual_leave_decisions;
    }
    
    public static function createEmployee($id)
    {
        $person = Person::model()->findByPk($id);
        if ($person==null)
            throw new Exception('Osoba sa zadatim ID ne postoji');
        
        if (isset($person->employee))
            throw new Exception('Osoba je vec u sistemu');
        
        $cond = new CDbCriteria();
        $cond->select = 'COALESCE(max(order_in_company),0)+1 as order_in_company';
        
        $_max_emp = Employee::model()->find($cond);
        
        //DODAVANJE OSOBE
        $empl = new Employee();
        $empl->id = $person->id;
        $empl->order_in_company = $_max_emp->order_in_company;
        $empl->save();
        $empl->refresh();
        
        if (isset($empl->person->user))
        {
            $empl->person->user->addEmployeePrivileges();
        }
        
        return $empl;
    }
    
    public function isActiveAt($active_at, $for_payout=null)
    {
        $cache_key = __METHOD__.'_'.$this->tableName().'_'.$this->id
                                .'_'.CJSON::encode($active_at).'_'.CJSON::encode($for_payout);
        $cache_result = Yii::app()->simacache->get($cache_key);
        
        if($cache_result === false)
        {
            $keywords = [
                [$this, 'work_contracts']
            ];
            
            $model_filter = [
                'employee' => [
                    'ids' => $this->id
                ],
                'signed' => true,
                'for_payout' => $for_payout,
                'active_at' => $active_at,
            ];
            
            if($for_payout === true || $for_payout === false)
            {
                $model_filter['for_payout'] = $for_payout;
            }
            
            $wcCriteria = new SIMADbCriteria([
                'Model' => 'WorkContract',
                'model_filter' => $model_filter
            ], true);
            
            $cache_result = 'NO';
            if(!empty($wcCriteria->ids))
            {
                $cache_result = 'YES';
            }
            
            Yii::app()->simacache->set($cache_key, $cache_result, $keywords, true);
            
            unset($keywords);
            unset($model_filter);
            unset($wcCriteria);
        }
        
        unset($cache_key);
                
        return $cache_result === 'YES';
    }
    
    public function isAbsentOnDate(Day $day, $type=null, $subtype=null)
    {
        $model_filter = [
            'employee' => [
                'ids' => $this->id
            ],
            'confirmed' => true,
            'absent_on_date' => $day
        ];
        
        if(isset($type))
        {
            $model_filter['type'] = $type;
        }
        
        if(isset($subtype))
        {
            $model_filter['subtype'] = $subtype;
        }
        
        $criteria = new SIMADbCriteria([
            'Model' => Absence::model(),
            'model_filter' => $model_filter
        ], true);
                
        return !empty($criteria->ids);
    }
    
    /**
     * Za zaposlene koji su prosledjeni merguje dokumente koji treba da se posalju
     * @param array $employees_documents - niz gde je svaki element sastavljen iz dva elementa(Employee employee, array documents(svaki element niza je tipa File))
     * @param string $title - subject email-a u slucaju da se salje email, ili ime zip fajla u slucaju da se pravi temp zip
     * @param boolean $send_emails - da li se salju dokumenta na email
     * @param func $callback_func - callback funkcija kojoj se prosledjuje trenutni procenat obrade(uglavnom koristi da bi se update-ovao ajax long)
     * @return - Vraca ime temp fajla ili null
     */
    public static function mergeEmployeesDocuments($employees_documents, $title='SIMA-automatsko slanje dokumentacije', $send_emails = true, $callback_func=null, $send_from_email=null)
    {        
        $documents_for_print = [];
        if (empty($send_from_email))
        {
            $email_account_id = Yii::app()->configManager->get('HR.hr_documentation_email', true);                
            $email_account = EmailAccount::model()->findByPk($email_account_id);
            if (is_null($email_account))
            {
                throw new SIMAException("Ne postoji email nalog sa kog treba slati. Kontaktirajte administratora.");
            }
            $send_from_email = $email_account->email;
        }
        else
        {
            $email_account = EmailAccount::model()->findByAttributes(['email'=>$send_from_email]);
            if (empty($email_account))
            {
                throw new SIMAException("Ne postoji email nalog $send_from_email sa koga se šalju dokumenta. Kontaktirajte administratora.");
            }
        }

        $max_while_cycle_exec_time_seconds = ini_get('max_execution_time');
        $i = 1;
        $total_rows = count($employees_documents);
        foreach ($employees_documents as $employee_documents) 
        {
            set_time_limit($max_while_cycle_exec_time_seconds);
            if (!isset($employee_documents['employee']) || !isset($employee_documents['documents']))
            {
                throw new SIMAException('Funkciji mergeEmployeesDocuments nepravilno su zadati parametri. Kontaktirajte administratora.');
            }
            
            $employee = $employee_documents['employee'];
            $employee_documents = $employee_documents['documents'];
            
            if ($employee->document_send_type === Employee::$E_MAIL && $send_emails === true && empty($employee->email_account_id))
            {
                Yii::app()->raiseNote("Za zaposlenog $employee->DisplayName nije poslat email jer nije zadat email nalog. Kontaktirajete administratora.");
            }
            
            if ($employee->document_send_type === Employee::$SIMA && !isset($employee->person->user))
            {
                Yii::app()->raiseNote("Zaposlenom $employee->DisplayName nije poslata dokumentacija u SIMI jer nije korisnik SIME. Kontaktirajete administratora.");
            }
            
            $is_email_delivery_type = $employee->document_send_type === Employee::$E_MAIL && !empty($employee->email_account_id) && $send_emails === true;
            $is_sima_delivery_type = $employee->document_send_type === Employee::$SIMA && isset($employee->person->user);
            $is_print_delivery_type = $employee->document_send_type === Employee::$PRINT;
            
            if (!is_array($employee_documents))
            {
                $employee_documents = [$employee_documents];
            }
            
            $message = null;
            $email_employee_documents_ids = [];            
            if ($is_email_delivery_type)
            {
                $message = new YiiMailMessage;
                $message->subject = $title;  
                $message->addTo($employee->email_account->email);
                $message->from = $send_from_email;
            }
            
            $temp_files_to_delete = [];
            
            foreach ($employee_documents as $employee_document) 
            {
                //kraira se novi dokument za slanje
                $delivered_hr_document = new DeliveredHRDocument();
                $delivered_hr_document->file_id = $employee_document->id;
                $delivered_hr_document->delivery_type = $employee->document_send_type;
                $delivered_hr_document->sent = true;
                $delivered_hr_document->sender_id = Yii::app()->user->id;
                $delivered_hr_document->sent_timestamp = 'now()';
                $delivered_hr_document->received = false;
                $delivered_hr_document->save();
                
                //dokumenta se merguju u zip i vraca se putanja zip fajla
                if ($is_print_delivery_type)
                {
                    array_push($documents_for_print, $employee_document);
                }
                //dokumenta se salju na email
                else if ($is_email_delivery_type)
                {
                    if (is_null($employee_document->last_version))
                    {
                        throw new SIMAException("Fajl sa ID: $employee_document->id nema nijednu verziju.");
                    }
                    if(Yii::app()->repManager->use_new_repmanager === true)
                    {
                        $temporaryFile = TemporaryFile::CreateFromFileVersion($employee_document->last_version);
                        $file_path = $temporaryFile->getFullPath();
                        $temp_files_to_delete[] = $temporaryFile;
                    }
                    else
                    {
                        $file_path = $employee_document->last_version->getFullPath();
                    }
                    $file_name = $employee_document->DisplayName;
                    $message->attach(Swift_Attachment::fromPath($file_path,mime_content_type($file_path))->setFilename($file_name));
                    $delivered_hr_document->link = Yii::app()->getBaseUrl(true)."?r=HR/HR/emailConfirmHRDocumentDelivery&delivery_hr_document_id=$delivered_hr_document->id&employee_id=$employee->id";                    
                    $delivered_hr_document->save();
                    array_push($email_employee_documents_ids, $delivered_hr_document->id);
                }
                //ukoliko je zaposleni korisnik i tip slanja dokumenta je preko sime, onda se zaposleni dodaje na pregled                    
                else if ($is_sima_delivery_type)
                {
                    $file_signature_view = $employee->person->user->addToFileSignatureView($employee_document->id);                        
                    $delivered_hr_document->file_signature_view_id = $file_signature_view->id;
                    $delivered_hr_document->save();
                }

                //postavlja se pripadnost zaposlenom
                $tag = $employee->tag;
                if(is_null($tag))
                {
                    throw new SIMAException("Za zaposlenog sa ID: $employee->id nije postavljen tag. Kontaktirajte administratora.");
                }
                $employee_document->addTagByModel($tag);
            }
            
            if ($is_email_delivery_type)
            {
                $message->view = 'email_hr_documentation_view';
                $message->setBody([
                    'employee_id'=>$employee->id,
                    'email_employee_documents_ids'=>$email_employee_documents_ids
                ], 'text/html');
                try
                {
                    Yii::app()->mail->send($message);
                }
                catch(Exception $e)
                {
                    $autobafreq_message = '$employee: '.SIMAMisc::toTypeAndJsonString($employee)
                                            .'$email_employee_documents_ids: '.SIMAMisc::toTypeAndJsonString($email_employee_documents_ids)
                                            .'$e->getMessage(): '.$e->getMessage()
                                            .'$e->getTraceAsString(): '.$e->getTraceAsString();
                    Yii::app()->errorReport->createAutoClientBafRequest($autobafreq_message, 3140, 'AutoBafReq dokumentacija problema u slanju na email');
                    Yii::app()->raiseNote("Za zaposlenog $employee->DisplayName je bilo problema u slanju na email. Molimo kontaktirajte administratora.");
                }
            }
            
            foreach($temp_files_to_delete as $temp_file_to_delete)
            {
                $temp_file_to_delete->delete();
            }
            
            if (!is_null($callback_func))
            {                
                $callback_func(round(($i/$total_rows)*70, 0, PHP_ROUND_HALF_DOWN));
                $i++;
            }
        }
        
        if (count($documents_for_print) > 0)
        {
            $temp_file = new TemporaryFile('',null,'zip');
            $temp_file->createZip($documents_for_print, function($percent) use ($callback_func){
                if (!is_null($callback_func))
                {
                    $callback_func(70+$percent*(30/100));
                }
            });
            
            return $temp_file->getFileName();
        }
        
        return null;
    }
    
    public function GetSVP()
    {
        return '1'.$this->employment_code.$this->paycheck_svp;
    }
    
    public function isRetiree()
    {
        return in_array($this->employment_code,['09','10','12','13']);
    }
}
