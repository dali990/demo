<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PersonToCompanies
 *
 * @author goran
 */
class PersonToCompanies extends SIMAActiveRecord
{
    public $from_work_contract = false;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'public.persons_to_companies';
    }

    public function relations($child_relations = [])
    {
        return array(
            'person' => array(self::BELONGS_TO, 'Person', 'person_id'),
            'company' => array(self::BELONGS_TO, 'Company', 'company_id'),
        );
    }

    public function rules()
    {
        return array(
            array('person_id, company_id', 'required'),
        );
    }
    
}

?>
