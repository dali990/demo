<?php

/**
 * Description of EmployeeToDriverLicenseCategory
 *
 * @author goran-set
 */
class EmployeeToDriverLicenseCategory extends SIMAActiveRecord
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'public.employees_to_driver_license_categories';
    }

    public function moduleName()
    {
        return 'application';
    }

    public function rules()
    {
        return array(
            array('employee_id, driver_license_category_id', 'required'),
            array('employee_id, driver_license_category_id', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => array('insert', 'update'))
        );
    }

    public function relations($child_relations = [])
    {
        return array(
            'employee' => array(self::BELONGS_TO, 'Employee', 'employee_id'),
            'driver_license_category' => array(self::BELONGS_TO, 'DriverLicenseCategory', 'driver_license_category_id')
        );
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {            
            case 'filters' : return array(
                'employee' => 'relation',
                'driver_license_category' => 'relation'
            );
            default: return parent::modelSettings($column);
        }
    }
    
}
