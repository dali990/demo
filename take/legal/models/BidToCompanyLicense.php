<?php

class BidToCompanyLicense extends SIMAActiveRecord
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'legal.bids_to_company_licenses';
    }

    public function moduleName()
    {
        return 'legal';
    }
    
    public function relations($child_relations = [])
    {
        return array(
            'bid' => array(self::BELONGS_TO, 'Bid', 'bid_id'),
            'company_license' => array(self::BELONGS_TO, 'CompanyLicenseToCompany', 'company_license_id'),
        );
    }

    public function rules()
    {
        return array(
            array('bid_id, company_license_id', 'safe'),
        );
    }
    
    public function modelSettings($column)
    {   
        switch ($column)
        {
            case 'update_relations' : return array('bid','company_license');
            default: return parent::modelSettings($column);
        }
    }
    
}

?>
