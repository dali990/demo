<?php 

class BidRequirementType extends SIMAActiveRecord
{
   
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
 
    public function tableName()
    {
        return 'legal.bid_requirement_types';
    }
    
    public function moduleName()
    {
        return 'legal';
    }
    
    public function __get($column)
    {
    	switch ($column)
    	{             
            case 'DisplayName':	return $this->name;
            case 'SearchName':	return $this->DisplayName;            
            default: return parent::__get($column);
    	}
    }   
    
    public function rules()
    {
    	return array(
            array('name', 'required'),            
            array('description','safe')
    	);
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters': return array_merge(parent::modelSettings($column),[
                'name'=>'text',
                'description'=>'text'
            ]);
            case 'textSearch': return array_merge(parent::modelSettings($column),[
                'name'=>'text'
            ]);
            case 'options':
                return ['form','delete'];            
            default: return parent::modelSettings($column);
        }
    }
}
