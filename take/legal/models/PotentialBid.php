<?php 

class PotentialBid extends SIMAActiveRecord
{   
    public static $NEW = 'NEW';
    public static $ACCEPTED = 'ACCEPTED';
    public static $DECLINED = 'DECLINED';
    
    public static $potential_bid_statuses = [
        'NEW' => 'Nova',
        'ACCEPTED' => 'Prihvaćena',
        'DECLINED' => 'Nije prihvaćena'        
    ];
    
    public $accepted_filter = null;    
    
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
 
    public function tableName()
    {
        return 'legal.potential_bids';
    }
    
    public function moduleName()
    {
        return 'legal';
    }
    
    public function __get($column)
    {
    	switch ($column)
    	{             
            case 'DisplayName':	return $this->bid_name;
            case 'SearchName':	return $this->DisplayName;
            case 'path2': return 'files/file';
            default: return parent::__get($column);
    	}
    }    
    
    public function relations($child_relations = [])
    {
    	return array(
            'file'=>array(self::BELONGS_TO, 'File', 'id'),
            'orderer_by'=>array(self::BELONGS_TO, 'Partner', 'orderer_by_id'),            
            'job_type'=>array(self::BELONGS_TO, 'JobType', 'job_type_id'),            
            'accepted_user' => array(self::BELONGS_TO, 'User', 'accepted_user_id'),
            'bid'=>array(self::HAS_ONE, 'Bid','potential_bid_id'),
            'job_bid'=>array(self::HAS_ONE, 'JobBid','potential_bid_id'),
    	);
    }
    
    public function rules()
    {
    	return array(
            array('bid_name, orderer_by_id, deadline, estimated_value, '
                . 'created_date, job_type_id', 'required'),            
            array('description, creator_id, bid_link,'
                . 'can_be_used_bank_statament_from_apr, accepted, accepted_user_id, accepted_time','safe'),
            array('orderer_by_id', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => array('insert', 'update'))
    	);
    }
    
    
    public function scopes()
    {
        $alias = $this->getTableAlias();
    	return array(    	
            'byName' =>array(
                'order'=>"${alias}.bid_name DESC, ${alias}.deadline DESC",
            )            
    	);
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters': return array_merge(parent::modelSettings($column),[
                'bid_name'=>'text',
                'orderer_by'=>'relation',
                'job_type'=>'relation',
                'deadline'=>'date_range',
                'estimated_value'=>'numeric',                
                'can_be_used_bank_statament_from_apr'=>'boolean',                
                'description'=>'text',
                'created_date'=>'date_range',
                'bid_link'=>'text',
                'accepted_filter'=>['dropdown', 'func'=>'accepted_filter_func']
            ]);
            case 'textSearch': return array_merge(parent::modelSettings($column),[
                'bid_name'=>'text'
            ]);
            case 'options':
                return ['form','delete','open','download'];
            case 'default_order_scope': return 'byName';
            case 'statuses':  return array(
                'accepted' => array(
                    'title' => 'Prihvaćena',
                    'timestamp' => 'accepted_time',
                    'user' => array('accepted_user_id','relName'=>'accepted_user'),
                    'checkAccessConfirm' => 'Accept',
                    'checkAccessRevert' => 'RevertAccept'
                )
            );
            case 'mutual_forms' : return array(
                'base_relation' => 'file',
            );
            case 'number_fields': return ['estimated_value'];
            default: return parent::modelSettings($column);
        }
    }
    
    public function accepted_filter_func($criteria, $alias)
    {        
        $cond = '';
        if (!is_null($this->accepted_filter))
        {
            switch($this->accepted_filter) 
            {    
                case PotentialBid::$NEW:       
                    $cond = "$alias.accepted is null"; break;
                case PotentialBid::$ACCEPTED: 
                    $cond = "$alias.accepted=true"; break;
                case PotentialBid::$DECLINED: 
                    $cond = "$alias.accepted=false"; break;
                default: 
                    $cond = ""; break;
            }
        }
        $temp_criteria = new CDbCriteria();        
        $temp_criteria->condition = $cond;
        
        $criteria->mergeWith($temp_criteria);
    }
    
    public function beforeSave()
    {
        if (empty($this->creator_id))
        {
            $this->creator_id = Yii::app()->user->model->id;
        }
        
        $this->bid_name = SIMAMisc::convertCyrillicToLatin($this->bid_name);
        
        return parent::beforeSave();
    }
    
    public function afterSave()
    {
        if (is_null($this->accepted) || is_null($this->__old['accepted']))
        {            
            Yii::app()->warnManager->sendWarns(Yii::app()->user->id, 'WARN_NEW_POTENTIAL_BIDS');
        }
        
        return parent::afterSave();
    }
}
