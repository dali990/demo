<?php

class ContractType extends SIMAActiveRecord
{

    public $pathname;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'public.contract_types';
    }

    public function __get($column)
    {
        switch ($column)
        {
            case 'DisplayName': return (isset($this->parent) ? $this->parent->DisplayName . '<br />-' : '') . $this->name;
            case 'SearchName': return $this->contract_number . '(' . $this->subject . ')';
            default: return parent::__get($column);
        }
    }

    public function rules()
    {
        return array(
            array('name', 'required', 'message' => 'Polje "{attribute}" mora biti uneto'),
            array('description', 'safe'),
        );
    }

    public function relations($child_relations = [])
    {
        return array(
            'contracts' => array(self::HAS_MANY, 'Contract', 'contract_type_id'),
            'parent' => array(self::BELONGS_TO, 'ContractType', 'parent_id'),
        );
    }

    public function getModelDropList()
    {
        $sql = "WITH RECURSIVE reccur(id,name,parent_id,path,depth) AS (
		select id,name,parent_id,'--'||name,''
		from contract_types a where parent_id is null
		UNION ALL
		SELECT pt.id, pt.name, pt.parent_id,reccur.path||'0'||pt.name,reccur.depth||'--' FROM contract_types pt, reccur  WHERE reccur.id=pt.parent_id
		)
		select r.id,r.depth||'--'||r.name as pathname , pt.name, pt.description
		from reccur r left join contract_types pt on r.id=pt.id
		order by r.path;";
        return CHtml::listData($this->findAllBySql($sql), 'id', 'pathname');
    }

}
