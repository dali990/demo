<?php

class ActiveEmployee extends Employee {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function defaultScope() {
        $alias = $this->getTableAlias(FALSE, FALSE);
        return array(
            'condition' => $alias . '.active=true'
        );
    }

}