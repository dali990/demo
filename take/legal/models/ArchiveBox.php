<?php
/**
 * Description of ArchiveBox
 *
 * @author goran-set
 */
class ArchiveBox extends SIMAActiveRecord
{
    private $_scopes_uniq = null;
    
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() 
    {
        return 'legal.archive_boxes';
    }

    public function moduleName() {
        return 'legal';
    }

    public function __get($column) {
        switch ($column) {
            case 'DisplayName': return "Arhivska kutija: ".$this->name;
            case 'SearchName': return $this->DisplayName;
            default: return parent::__get($column);
        }
    }
    
    public function rules() {
        return array(
            array('name','required','message' => 'Polje "{attribute}" mora biti uneto'),
            array('name,archive_shelf_id, description', 'safe'),
            array('archive_shelf_id', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => array('insert', 'update'))
        );
    }

    public function relations($child_relations = [])
    {
        return array(
            'archive_shelf' => array(self::BELONGS_TO, 'ArchiveShelf', 'archive_shelf_id')
        );
    }
    
    public function scopes() {
        if ($this->_scopes_uniq==null)
        {
            $this->_scopes_uniq = SIMAHtml::uniqid();
        }
        $uniq = $this->_scopes_uniq;
        $alias = $this->getTableAlias();
        
        
        return array(
            'recently' => array(
                'order' => 'id',
            )
        );
    }
    
    public function modelSettings($column)
    {        
        switch ($column)
        {
            case 'textSearch': return array(
                'name' => 'text'
            );
//            case 'options':  return array_merge($options, array('open'));
            
            case 'filters': return array(
                'archive_shelf_id'=>array('func'=>'filter_archive_shelf_id')
            );
            default: return parent::modelSettings($column);
        }
    }
    
    public function filter_archive_shelf_id($condition)
    {        
        if(isset($this->archive_shelf_id) && $this->archive_shelf_id!=null)
        {
            $param_id=  SIMAHtml::uniqid();
            $alias = $this->getTableAlias();
            $temp_condition=new CDbCriteria();
            $temp_condition->condition="${alias}.archive_shelf_id=:param${param_id}";
            $temp_condition->params=array(':param'.$param_id=>$this->archive_shelf_id);
            $condition->mergeWith($temp_condition, true);
        }
    }
      
    public function AfterSave()
    {   
        return parent::AfterSave();
    }

    public function BeforeSave()
    {
        
        return parent::BeforeSave();
    }    
}
