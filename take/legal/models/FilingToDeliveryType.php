<?php 

class FilingToDeliveryType extends SIMAActiveRecord
{
    
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
    
    public function moduleName() 
    {
        return 'legal';
    }
    
    public function tableName()
    {
        return 'private.filing_book_to_delivery_types';
    }
    
    public function __get($column) {
        switch ($column) {
            case 'DisplayName': return $this->delivery_type->DisplayName;
            default: return parent::__get($column);
        }
    }
    
    public function relations($child_relations = [])
    {
    	return array(
            'filing' => array(self::BELONGS_TO, 'Filing', 'filing_book_id'),
            'delivery_type' => array(self::BELONGS_TO, 'DeliveryType', 'delivery_type_id'),
    	);
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'options' : return  array(
                 'delete'
            );
            default: return parent::modelSettings($column);
        }
    }

}