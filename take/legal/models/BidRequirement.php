<?php 

class BidRequirement extends SIMAActiveRecord
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
 
    public function tableName()
    {
        return 'legal.bid_requirements';
    }
    
    public function moduleName()
    {
        return 'legal';
    }
    
    public function __get($column)
    {
    	switch ($column)
    	{             
            case 'DisplayName':	return $this->name;
            case 'SearchName':	return $this->DisplayName;            
            default: return parent::__get($column);
    	}
    }    
    
    public function relations($child_relations = [])
    {
    	return array(
            'potential_bid'=>array(self::BELONGS_TO, 'PotentialBid', 'potential_bid_id'),            
            'bid_requirement_type'=>array(self::BELONGS_TO, 'BidRequirementType', 'bid_requirement_type_id'),
            'confirmed_user'=>array(self::BELONGS_TO, 'User', 'confirmed_user_id')
    	);
    }
    
    public function rules()
    {
    	return array(
            array('name, potential_bid_id, bid_requirement_type_id', 'required'),            
            array('description','safe'),
            array('potential_bid_id, bid_requirement_type_id', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => array('insert', 'update'))
    	);
    }    
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters': return array_merge(parent::modelSettings($column),[
                'name'=>'text',
                'potential_bid'=>'relation',
                'bid_requirement_type'=>'relation',
                'description'=>'text'
            ]);
            case 'textSearch': return array_merge(parent::modelSettings($column),[
                'name'=>'text'
            ]);
            case 'options':
                return ['form','delete'];
            case 'statuses':  return array(
                'confirmed' => array(
                    'title' => 'Potvrđeno',
                    'timestamp' => 'confirmed_timestamp',
                    'user' => array('confirmed_user_id','relName'=>'confirmed_user'),
                    'checkAccessConfirm' => 'Confirm',
                    'checkAccessRevert' => 'RevertConfirm'
                )
            );
            default: return parent::modelSettings($column);
        }
    }
}
