<?php

/**
 * Description of UserToRegistry
 *
 * @author sasa
 */
class UserToRegistry extends SIMAActiveRecord
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'private.users_to_registries';
    }

    public function relations($child_relations = [])
    {
        return array(
            'user' => array(self::BELONGS_TO, 'User', 'user_id'),
            'registry' => array(self::BELONGS_TO, 'Registry', 'registry_id'),
        );
    }

    public function rules()
    {
        return array(
            array('user_id, registry_id', 'required'),
        );
    }
    
}

?>
