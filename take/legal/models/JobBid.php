<?php

class JobBid extends Bid
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function BeforeSave()
    {
        $this->qualification = 0;
        return parent::BeforeSave();
    }
    
    public function defaultScope()
    {
        return array(
            'condition' => $this->getTableAlias(FALSE, FALSE) . '.qualification=false'
        );
    }

}
