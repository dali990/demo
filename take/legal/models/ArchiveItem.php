<?php
/**
 * Description of ArchiveBox
 *
 * @author goran-set
 */
class ArchiveItem extends SIMAActiveRecord
{   
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() 
    {
        return 'legal.archive_items';
    }

    public function moduleName() 
    {
        return 'legal';
    }

    public function __get($column) {
        switch ($column) {
            case 'DisplayName': return "Arhivska stavka: ".$this->name;
            case 'SearchName': return $this->DisplayName;
            default: return parent::__get($column);
        }
    }
    
    public function rules() {
        return array(
            array('name','required','message' => 'Polje "{attribute}" mora biti uneto'),
            array('name, archive_box_id, description, theme_id', 'safe'),
            array('archive_box_id, theme_id', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => array('insert', 'update'))
        );
    }

    public function relations($child_relations = [])
    {
        return array(
            'archive_box' => array(self::BELONGS_TO, 'ArchiveBox', 'archive_box_id'),
            'theme' => array(self::BELONGS_TO, 'Theme', 'theme_id')
        );
    }
    
    public function modelSettings($column)
    {        
        switch ($column)
        {
            case 'textSearch': return array(
                'name' => 'text'
            );
            case 'filters': return array(
                'name'=>'text',
                'theme_id'=>'dropdown',
                'theme'=>'relation',
                'archive_box_id'=>'dropdown',
                'archive_box'=>'relation'
            );
            default: return parent::modelSettings($column);
        }
    }
      
    public function AfterSave()
    {   
        return parent::AfterSave();
    }

    public function BeforeSave()
    {
        
        return parent::BeforeSave();
    }    
    

    
}
