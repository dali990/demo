<?php

class BusinessOffer extends SIMAActiveRecord
{
    public static $PREPARATION = 'PREPARATION';
    public static $SENT = 'SENT';
    public static $ACCEPTED = 'ACCEPTED';
    public static $NOT_ACCEPTED = 'NOT_ACCEPTED';

    public static $statuses = [];
    
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
 
    public function tableName()
    {
        return 'legal.business_offers';
    }
    
    public function moduleName()
    {
        return 'legal';
    }
    
    public function __get($column) 
    {
        switch ($column) {
            case 'DisplayName': return $this->name . (!empty($this->partner) ? " ({$this->partner->DisplayName})" : '');
            case 'SearchName': return $this->DisplayName;

            default: return parent::__get($column);
        }
    }
    
    public function rules()
    {
        return [
            ['name, partner_id, status, currency_id, offer_number', 'required'],
            ['created_by_user_id, created_timestamp, theme_id, business_offer_variation_id, description', 'safe'],
            ['offer_number', 'unique_with', 'with' => ['partner_id']],
            ['partner_id, currency_id, created_by_user_id, theme_id, business_offer_variation_id', 'default',
                'value' => null,
                'setOnEmpty' => true,
                'on' => ['insert', 'update']
            ]
        ];
    }
    
    public function relations($child_relations = [])
    {
        return [
            'partner' => [self::BELONGS_TO, Partner::class, 'partner_id'],
            'currency' => [self::BELONGS_TO, Currency::class, 'currency_id'],
            'created_by_user' => [self::BELONGS_TO, User::class, 'created_by_user_id'],
            'theme' => [self::BELONGS_TO, Theme::class, 'theme_id'],
            'business_offer_variation' => [self::BELONGS_TO, BusinessOfferVariation::class, 'business_offer_variation_id']
        ];
    }
    
    public function scopes()
    {
        $alias = $this->getTableAlias();

        return [
            'byName' => [
                'order' => "$alias.name ASC"
            ]
        ];
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters' : return array_merge(parent::modelSettings('filters'), [
                'name' => 'text',
                'status' => 'dropdown',
                'partner' => 'relation',
                'currency' => 'relation',
                'created_by_user' => 'relation',
                'created_timestamp' => 'date_time_range',
                'theme' => 'relation',
                'business_offer_variation' => 'relation',
                'description' => 'text',
                'offer_number' => 'text'
            ]);
            case 'textSearch': return [
                'partner' => 'relation',
                'name' => 'text'
            ];
            default: return parent::modelSettings($column);
        }
    }
    
    protected function modelOptions(\User $user = null):array
    {
        return ['open', 'form', 'delete'];
    }
    
    public function beforeSave()
    {
        if ($this->isNewRecord && Yii::app()->isWebApplication())
        {
            $this->created_by_user_id = Yii::app()->user->id;
            $this->created_timestamp = 'now()';
        }
        
        return parent::beforeSave();
    }
}

BusinessOffer::$statuses = [
    'PREPARATION' => Yii::t('LegalModule.BusinessOffer', 'Preparation'),
    'SENT' => Yii::t('LegalModule.BusinessOffer', 'Sent'),
    'ACCEPTED' => Yii::t('LegalModule.BusinessOffer', 'Accepted'),
    'NOT_ACCEPTED' => Yii::t('LegalModule.BusinessOffer', 'NotAccepted')
];
