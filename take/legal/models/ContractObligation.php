<?php

class ContractObligation extends SIMAActiveRecord
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'private.contract_obligations';
    }

    public function relations($child_relations = [])
    {
        return array(
            'contract_obligation_type' => array(self::BELONGS_TO, 'ContractObligationType', 'contract_obligation_type_id'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'contract_obligation_type_id' => 'Tip obaveze',
                ) + parent::attributeLabels();
    }

    public function droplist($key)
    {
        switch ($key)
        {
            case 'contract_obligation_type_id': return ContractObligationType::model()->getModelDropList();
            default: return parent::droplist($key);
        }
    }

    public function rules()
    {
        return array(
            array('name, description, contract_id, contract_obligation_type_id', 'required'),
        );
    }

    public function scopes()
    {
        return array(
            'byName' => array('order' => 'contract_obligation_type_id, id'),
        );
    }
    
    public function getAttributeDisplay($column)
    {
        switch ($column)
        {
            case 'contract_obligation_type_id': return (isset($this->contract_obligation_type)) ? $this->contract_obligation_type->name : '';
            default: return parent::getAttributeDisplay($column);
        }
    }

    
    public function modelSettings($column)
    {
        switch ($column) 
        {
            case 'filters': return [
                'contract_obligation_type_id' => 'dropdown',
                'contract_id' => 'dropdown',
            ];
            default: return parent::modelSettings($column);
        }
        
    }
    

}
