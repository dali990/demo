<?php 

Bid::$statuses = [
    'NEW' => Yii::t('LegalModule.Bid', 'StatusNew'), 
    'IN_PREPARATION' => Yii::t('LegalModule.Bid', 'StatusInPreparation'), 
    'SUBMITTED' => Yii::t('LegalModule.Bid', 'StatusSubmitted'), 
    'ACCEPTED' => Yii::t('LegalModule.Bid', 'StatusAccepted'), 
    'DECLINED' => Yii::t('LegalModule.Bid', 'StatusDeclined'),
    'DROP_OUT' => Yii::t('LegalModule.Bid', 'StatusDropOut')
];

class Bid extends SIMAActiveRecord
{
    /**
     * prilikom filtriranja, ovde se smestaju tagovi
     */
    public $belongs;
    public $belongs_recursive;
    
    public $value_in_currency;
    
    public static $NEW = 'Nova';
    public static $IN_PREPARATION = 'U pripremi';
    public static $SUBMITTED = 'Predata';
    public static $ACCEPTED = 'Prihvacena';
    public static $DECLINED = 'Nije prihvacena';
    public static $DROP_OUT = 'Odustala';
    
    public static $bid_statuses = [
        'Nova' => 'Nova',
        'U pripremi' => 'U pripremi',
        'Predata' => 'Predata',
        'Prihvacena' => 'Prihvaćena',
        'Nije prihvacena' => 'Nije prihvaćena',
        'Odustala' => 'Odustala'
    ];
    
    public static $statuses = []; //Sasa A. - dodao sam ovo jer ne mogu da prepravim $bid_statuses jer su u bazi sacuvani statusi na srpskom.
                                //kad se bude prepravljalo, treba i baza da se prepravi
    
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
 
    public function tableName()
    {
        return 'private.bids';
    }
    
    public function moduleName()
    {
        return 'legal';
    }
    
    public function __get($column)
    {
    	switch ($column)
    	{
             //case 'qualification':       return true;
    		case 'SearchName':				return $this->display_name.(isset($this->file) && $this->file->hasTag('filing_book')?$this->file->filing_book->filing_number:'');
//    		case 'DisplayName':  case 'SearchName': 
//    			return (isset($this->file->filing)?$this->file->filing->filing_number:'nije zavedeno');
    		default: 			return parent::__get($column);
    	}
    }
    
    public function setscan_order($bool)
    {
    	if (isset($this->file))
    	{
    		$this->file->scan_order=$bool;
    		$this->file->save();
    	}
    }
    public function getscan_order()
    {
    	if (isset($this->file))
        {
    		return $this->file->scan_order;
        }
    	else
        {
    		return false;
        }
    }
    
    public function hasAccess($user_id)
    {
        if (!Yii::app()->user->checkAccess('modelThemeSeeAll'))
        {
            $alias = $this->getTableAlias();
            $_person_to_themes_table = PersonToTheme::model()->tableName();
            $uniq = SIMAHtml::uniqid();
            $user_id = Yii::app()->user->id;
            $this->getDbCriteria()->mergeWith([
                'condition' => "exists (select 1 from $_person_to_themes_table "
                    . "where person_id = :person_id$uniq and theme_id=$alias.id)",
                'params' => [":person_id$uniq" => $user_id]
            ]);
        }

        return $this;
    }

    public function gettag()
    {
    	return FileTag::model()->findByAttributes(array('model_table'=>$this->theme->tableName(),'model_id'=>$this->id));
    }
    
    public function relations($child_relations = [])
    {
    	return array(
            'theme' => [self::BELONGS_TO, 'Theme', 'id'],
            
            'partner'=>array(self::BELONGS_TO, 'Partner', 'partner_id'),
            'best_bid_partner'=>array(self::BELONGS_TO, 'Partner', 'best_bid_partner_id'),
//            'job_type'=>array(self::BELONGS_TO, 'JobType', 'job_type_id'),            
            'file'=>array(self::BELONGS_TO, 'File', 'file_id'),
            'opening_record'=>array(self::BELONGS_TO, 'File', 'opening_record_file_id'),
            'decision_record'=>array(self::BELONGS_TO, 'File', 'decision_record_file_id'),            
            'currency'=>array(self::BELONGS_TO, 'Currency', 'currency_id'),
            'building_structure'=>array(self::BELONGS_TO, 'BuildingStructure', 'building_structure_id'),
            'company_licenses'=>array(self::MANY_MANY, 'CompanyLicense', 'legal.bids_to_company_licenses(bid_id,company_license_id)'),
            'bid_company_licenses' => array(self::HAS_MANY, 'BidToCompanyLicense', 'bid_id'),
            'work_licenses'=>array(self::MANY_MANY, 'WorkLicense', 'legal.bids_to_work_licenses(bid_id,work_license_id)'),
            'bid_work_licenses' => array(self::HAS_MANY, 'BidToWorkLicense', 'bid_id'),
            'coordinator' => array(self::BELONGS_TO, 'User', 'coordinator_id'),
            'job' => array(self::BELONGS_TO, 'Job', 'job_id'),
            'potential_bid' => array(self::BELONGS_TO, 'PotentialBid', 'potential_bid_id'),
            'refer' => [self::BELONGS_TO, 'Person', 'refer_id'],
            'compose' => [self::BELONGS_TO, 'Person', 'compose_id']
    	);
    }
    
    public function rules()
    {
    	return array(
            array('partner_id, subject, value, bid_status', 'required'),
            array('best_bid_value', 'numerical'),
            array('value', 'numerical', 'max' => 99999999999999999),
            array('essential_elements, delivering_time, opening_time, id, building_structure_id, qualification, is_enable_template', 'safe'),
            array('archive, best_bid_value, best_bid_partner_id, currency_id, description, potential_bid_id', 'safe'),
            array('belongs, belongs_recursive, order_by','safe'),
            ['refer_id, text_of_bid, base_of_bid, compose_id, limit_execution_work, number_of_bid','safe'],
            array('bid_status', 'checkStatus'),
            array('partner_id, best_bid_partner_id, id, building_structure_id, coordinator_id','default',
                    'value'=>null,
                    'setOnEmpty'=>true,'on'=>array('insert','update')),
            array('delivering_time, opening_time, qualification_end','default',
                    'value'=>null,
                    'setOnEmpty'=>true,'on'=>array('insert','update')),
            array('best_bid_value','default',
                    'value'=>0,
                    'setOnEmpty'=>true,'on'=>array('insert','update')),
            ['id','canDelete','on'=>['delete']]
    	);
    }
    
    public function beforeValidate()
    {
        if ($this->isNewRecord)
        {
            if (empty($this->bid_status))
            {
                $this->bid_status = Bid::$NEW;
            }
            $this->theme->status = $this->getThemeStatus();
        }
        return parent::beforeValidate();
    }
    
    public function checkStatus()
    {
        if ($this->isNewRecord)
        {
            if (!$this->theme->validate(['status']))
            {
                $this->addError('bid_status', $this->theme->getError('status'));
            }
        }
        else
        {
            $_check_theme = Theme::model()->findByPk($this->id);
            $_check_theme->status = $this->getThemeStatus();
            if (!$_check_theme->validate(['status']))
            {
                $this->addError('bid_status', $_check_theme->getError('status'));
            }
        }
    }
    
    public function canDelete()
    {
        if ($this->theme->type === Theme::$PLAIN) //konvertovanje teme iz BID u PLAIN
        {
//            foreach ([
//                'base_document',
//                'value',
//                'advance',
//                'deadline',
//                'comment',
//                'working_directory',
//                ] as $column)
//            {
//                if (!empty($this->$column)) 
//                {$this->addError($column, Yii::t('JobsModule.Job', 'ColumnMustBeEmpty',['{column_name}' => $this->getAttributeLabel($column)]));}
//            }
        }
        elseif ($this->theme->type === Theme::$BID) //ako je postavljen Theme::$PLAIN, to znaci da je konvertovanje
        {
//            $this->theme->canDelete();
//            $this->addErrors($this->theme->getErrors());
        }
        return parent::canDelete();
    }    
    
    public function getThemeStatus()
    {
        return self::convertBidToThemeStatus($this->bid_status);
    }
    
    public static function convertBidToThemeStatus($bid_status)
    {
        switch ($bid_status) 
        {
            case Bid::$NEW:
            case Bid::$IN_PREPARATION:
            case Bid::$SUBMITTED:
                return Theme::$ACTIVE;
                
            case Bid::$DECLINED:
            case Bid::$DROP_OUT:
            case Bid::$ACCEPTED: 
                return Theme::$NOT_ACTIVE;
                
            default:
                throw new SIMAException('Prosao bid_status koji ne postoji -> '.CJSON::encode($bid_status));
        }
    }
    
    
    public function scopes()
    {
//        $alias = $this->getTableAlias();
    	return array(
    	// 				'priorty_desc'=>array('order'=>'todo_priority_id DESC'),
    	// 				'priorty_asc' =>array('order'=>'todo_priority_id  ASC'),
//            'recently' =>array(
//                'order'=>"delivering_time DESC",
//                /** zakomentarisano zbog filtriranja i sortiranja*/
////                'with'=>array('file.filing','file.bid', 'job_type','building_structure','opening_record','decision_record')
//                ),
            'byName' =>array(
                'order'=>"delivering_time DESC",
//                'with'=>array('filing','file.bid', 'job_type','building_structure','opening_record','decision_record')
            )            
    	);
    }
    
    public function modelOptions(\User $user = null): array
    {
        if (is_null($user) && Yii::app()->isWebApplication())
        {
            $user = Yii::app()->user->model;
        }
        $options = [];
        //ne treba ubacivati delete jer se brise iz teme
        if ($user->checkAccess('modelBidDelete')) 
        {
            $options[] = ['delete','model' => 'Theme'];
        }
        if ($user->checkAccess('modelBidFormOpen'))
        {
            $options[] = 'form';
        }
        if ($user->checkAccess('modelBidOpenDialog')) 
        {
            $options[] = 'new_tab2';
        }
        if ($user->checkAccess('Download',[],$this->file)) 
        {
            $options[] = 'download';
        }

        return $options;
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'clicked_form_first' : return false;
            case 'filters': return [
//                'job_type_id'=>'dropdown',                
                'bid_status'=>'dropdown',
                'essential_elements'=>'text',
                'archive'=>'text',
                'subject'=>'text',
                'delivering_time'=>'date_range',
                'opening_time'=>'date_range',                
                'partner'=>'relation',
                'coordinator'=>'relation',
                'potential_bid'=>'relation',
                'theme' => 'relation'
            ];
            case 'textSearch': return [
                'file'=>'relation',
                'essential_elements'=>'text',
                'subject'=>'text',
                'partner'=>'relation',
                'coordinator'=>'relation',
                'refer' => 'relation',
                'compose' => 'relation',
            ];
            case 'default_order_scope': return 'byName';
            case 'mutual_forms': return [
                'base_relation' => 'theme'
            ];
            case 'number_fields': return ['value','value_in_currency'];
            default: return parent::modelSettings($column);
        }   
    }
    
    public function beforeMutualSave() 
    {
        $this->coordinator_id = $this->theme->coordinator_id;
        $this->theme->type = Theme::$BID;
        $this->theme->status = $this->getThemeStatus();
        return parent::beforeMutualSave();
    }

    public function afterSave()
    {
        parent::afterSave();

        $theme = Theme::model()->findByPk($this->id);
        if ($theme->status != $this->getThemeStatus())
        {
            $theme->status = $this->getThemeStatus();
            $theme->save();
        }
        
        if (!empty($this->potential_bid_id))
        {
            $tag = FileTag::model()->findByAttributes(array('model_table' => Theme::model()->tableName(), 'model_id' => $this->id));
            if ($tag !== null && isset($this->potential_bid->file))
            {
                $this->potential_bid->file->addTagByModel($tag);
            }
        }
        if ($this->bid_status === Bid::$NEW || is_null($this->__old['bid_status']) || $this->__old['bid_status'] === Bid::$NEW)
        {
            Yii::app()->warnManager->sendWarns(Yii::app()->user->id, 'WARN_NEW_BIDS');
        }
        
        $tag = FileTag::model()->findByAttributes(['model_table' => Theme::model()->tableName(), 'model_id' => $this->id]);
        $old_file_id = $this->__old['file_id']; //mora pre refresh-a, jer nakon toga old_file_id i this_file_id budu isti i ne prolazi uslov ispod
        $this->refresh();
        if (!empty($this->file) && $tag !== null)
        {
            $file_to_file_tag = FiletoFileTag::model()->findByAttributes(['file_id' => $this->file->id, 'file_tag_id' => $tag->id]);
            if (empty($file_to_file_tag) && ($this->isNewRecord || $old_file_id !== $this->file_id))
            {
                $this->file->addTagByModel($tag);
                $this->file->document_type_id = Yii::app()->configManager->get('legal.bid', false);
                $this->file->save();
                if(Yii::app()->isWebApplication())
                {
                    Yii::app()->raiseNote(Yii::t('LegalModule.Bid', 'AddBelongsAndDocumentTypeOfFile'));
                }
            }
        }
    }
    
    public function afterDelete()
    {
        parent::afterDelete();
        if (!empty($this->potential_bid_id))
        {
            $tag = FileTag::model()->findByAttributes(array('model_table' => Bid::model()->tableName(), 'model_id' => $this->id));
            if ($tag !== null && isset($this->potential_bid->file))
            {
                $this->potential_bid->file->removeTagByModel($tag);
            }
        }                
        $theme_for_delete = Theme::model()->findByPkWithCheck($this->id);
        if ($theme_for_delete->type === Theme::$BID) //ako je postavljen Theme::$PLAIN, to znaci da je konvertovanje
        {
            $theme_for_delete->type = Theme::$PLAIN;
            $theme_for_delete->save();
            $theme_for_delete->delete();
        }
    }
    
    public function getFileForBid($curr_user_id)
    {
        if (!empty($this->file))
        {
            $return_file = $this->file;
        }
        else
        {
            $return_file = new File();
            $return_file->name = $this->subject;
            $return_file->responsible_id = $curr_user_id;
            $return_file->save();
            
            $this->file_id = $return_file->id;
            $this->save();
        }
        return $return_file;
    }
}
