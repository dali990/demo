<?php 



class BidStatus extends SIMAActiveRecord
{

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
 
    public function tableName()
    {
        return 'public.bid_statuses';
    }
    
    public function moduleName()
    {
        return 'legal';
    }
    
    public function __get($column)
    {
        switch ($column)
        {
            case 'DisplayName': return $this->name;
            default: return parent::__get($column);
        }
    }
    
    public function rules()
    {
    	return array(
            array('name', 'required', 'message'=>'Polje "{attribute}" mora biti uneto'),
            array('description', 'safe'),
    	);
    }
    
    public function scopes()
    {
        return array(
            'byName'=>array(
                'order'=>'name, id',
            ),
        );
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'textSearch' : return array('name' => 'text');
            default: return parent::modelSettings($column);
        }
    }
    
// 	public function relations($child_relations = [])
//     {
//     	return array(
//     		'contacts'=>array(self::HAS_MANY, 'Contact', 'type_id')
//     	);
//     }
    //zbog kompatibilnosti
//     public function gettitle()
//     {
//     	return $this->name;
//     }

}