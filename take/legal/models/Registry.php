<?php

class Registry extends SIMAActiveRecord {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'private.registries';
    }

    public function moduleName()
    {
        return 'legal';
    }

    public function __get($column)
    {
        switch ($column)
        {
            case 'DisplayName':
            case 'SearchName': return $this->name;
            default: return parent::__get($column);
        }
    }


    public function rules() {
        return array(
            array('name', 'required', 'message' => 'Polje "{attribute}" mora biti uneto'),
            array('description,archived, archive_box_id', 'safe'),
            array('archive_box_id', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => array('insert', 'update'))
        );
    }

    public function scopes() {
        return array(
            'byName' => array(
                'order' => 'name, id',
            ),
        );
    }   
    
    public function modelSettings($column)
    {        
        switch ($column)
        {
            case 'textSearch': return array(
                'name' => 'text',
                'description' => 'text',
                'location' => 'text',
                'archived' => 'boolean'
            );
            case 'filters': return array(
                'archive_box_id'=>array('func'=>'filter_archive_box_id'),
                'name'=>'text',
                'archived'=>'boolean',
                'location' => 'text'
            );
            case 'form_list_relations' : return array('users');
            default: return parent::modelSettings($column);
        }
    }
    
    public function filter_archive_box_id($condition)
    {        
        if(isset($this->archive_box_id) && $this->archive_box_id!=null)
        {
            $param_id=  SIMAHtml::uniqid();
            $alias = $this->getTableAlias();
            $temp_condition=new CDbCriteria();
            $temp_condition->condition="${alias}.archive_box_id=:param${param_id}";
            $temp_condition->params=array(':param'.$param_id=>$this->archive_box_id);
            $condition->mergeWith($temp_condition, true);
        }
    }

    public function relations($child_relations = []) {
        return array(
            'archive_box' => array(self::BELONGS_TO, 'ArchiveBox', 'archive_box_id'),
            'users' => array(self::MANY_MANY, 'User', 'private.users_to_registries(registry_id, user_id)')
        );
    }

    public function isFinance()
    {
        Yii::app()->raiseNote('Nije napravljena podrska za prepoznavanje finansijskog registratora');
        return true;
    }
}