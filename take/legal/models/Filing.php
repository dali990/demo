<?php

class Filing extends SIMAActiveRecord {

    private $_old_file_belongs = '[]';
    public $max;
    public $image;

    /**
     * prilikom filtriranja, ovde se smestaju tagovi
     */
    public $belongs;
    public $belongs_recursive;

    /**
     * 	za narucivanje skeniranja iz zavodne knjige
     *  */
    public function setscan_order($bool) 
    {
        if (isset($this->file)) 
        {
            $this->file->scan_order = $bool;
            $this->file->save();
        }
    }

    public function getscan_order() 
    {
        if (isset($this->file))
            return $this->file->scan_order;
        else
        {
            return null;
        }
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'private.filing_book';
    }

    public function moduleName() {
        return 'legal';
    }

    public function __get($column) {
        switch ($column) {
            case 'path2': return 'files/file';
            case 'DisplayName': return $this->filing_number;
            case 'DisplayNameWithDate': return $this->filing_number.' '.Yii::t('BaseModule.Common', 'from').' '.$this->date;
            case 'SearchName': return $this->filing_number . ' = ' . $this->file->name;
            case 'old_file_belongs': return $this->_old_file_belongs;
            default: return parent::__get($column);
        }
    }
    
    public function __set($name, $value)
    {           
        switch ($name)
        {
            case 'old_file_belongs': 
                if (is_array($value))
                {
                    $this->_old_file_belongs = CJSON::encode($value);
                }
                else
                {
                    $this->_old_file_belongs = $value;
                }
                    
                    break;
            default: parent::__set($name, $value); break;
        }   
    }
    
    public function afterFind()
    {
        parent::afterFind();
        $this->old_file_belongs = CJSON::encode(isset($this->file)?$this->file->file_belongs:array());
    }
    
//     public function getDisplayName() {return $this->filing_number;}
    
    public function relations($child_relations = []) 
    {
        $user_id = -1;
        if(Yii::app()->isWebApplication())
        {
            $user_id = Yii::app()->user->id;
        }
        $cache_key = 'MODEL_FILING_RELATIONS_'.$user_id;
        $_result = Yii::app()->cache->get($cache_key);
        if ($_result===false)
        {
            $condition = '';
            //Sasa A. - U dogovoru sa Milos J. je stavljena provera da li je controller postavljen(jer inace puca prilikom ucitavanja upozorenja, 
            //jer se ona ucitavaju u init aplikacije, pre kreiranja kontrolera). 
            //Ono sto moze da se javi kao problem je ako se ne pozove funkcija
            //File::calcCondition koja unutar if-a, u slucaju da imamo neki kod koji zavisi od toga, a izvrsava se pre kreiranja kontrolera 
            if(Yii::app()->isWebApplication() && !is_null(Yii::app()->controller) && !Yii::app()->controller->isApiAction())
            {
                $condition = ' and '.File::calcCondition(Yii::app()->user->id, 10, 'ff');
            }
        
            $_result = array(
                'file' => array(self::BELONGS_TO, 'File', 'id'),
                'partner' => array(self::BELONGS_TO, 'Partner', 'partner_id'),
                'delivery_types' => array(self::HAS_MANY, 'FilingToDeliveryType', 'filing_book_id'),
                'filing_to_delivery_types'=>array(self::MANY_MANY, 'DeliveryType', 'private.filing_book_to_delivery_types(filing_book_id,delivery_type_id)'),
                'creator' => array(self::BELONGS_TO, 'User', 'creator_id'),
                'confirmed_user' => array(self::BELONGS_TO, 'User', 'confirmed_user_id'),
                'returned_user' => array(self::BELONGS_TO, 'User', 'returned_user_id'),
                'registry' => array(self::BELONGS_TO, 'Registry', 'registry_id'),
                'contribs' => array(self::MANY_MANY, 'File', 'private.filing_contribs(note_id, contrib_id)'),
                'filing_contribs' => array(self::HAS_MANY, 'FilingContrib', 'note_id',
                    'alias' => 'filingcontribsinfiling',
                    'condition' => 'exists (select 1 from files.files ff where ff.id=filingcontribsinfiling.contrib_id '
                    . $condition
                    . ')'
                ),
                'located_at' => array(self::BELONGS_TO, 'User', 'located_at_id'),
            );
            
            Yii::app()->cache->set($cache_key,$_result,24*60*60);
        }

        return parent::relations(array_merge($_result, $child_relations));
    }

    public function rules() {
        return array(
            array('email_id, contrib_ids, scopes, returned, old_file_belongs, date', 'safe'),
            array('filing_number, creator_id, registry_id, id, located_at_id', 'safe'), //neophodno zbog pretrage
            array('partner_id, located_at_id', 'required'),
            array('located_at_id', 'checkLocatedAt'),
            array('number, subnumber, comment, in', 'safe'),
            ['id', 'numerical', 'integerOnly' => true],
            array('confirmed', 'checkConfirm'),
            array('returned', 'checkReturned'),
            ['date', 'date', 'format' => Yii::app()->params['date_format']],
            array('registry_id', 'checkRegister'), 
            array('belongs, belongs_recursive', 'safe'),
            array('number', 'numerical'),
            array('date', 'required', 'on'=>'addButton2'),
            array('number','checkNumber','on'=>'addButton2'),
            array('partner_id, email_id, number, located_at_id, in', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => array('insert', 'update'))
        );
    }
    
    public function checkNumber($attribute, $params)
    {        
        if (empty($this->number))
        {
            $this->addError('number', 'Broj je obavezan');
        }
        if (empty($this->date))
        {
            $this->addError('date', 'Datum je obavezan');
        }
        if(is_string($this->number) && (string)((int)$this->number) !== $this->number)
        {
            $this->addError('number', Yii::t('LegalModule.Filing', 'NumberIsNotNumeric'));
        }
        if (!empty($this->number) && !empty($this->date) && (string)((int)$this->number) === (string)$this->number)
        {
            $year = date('Y', strtotime($this->date));
            $criteria = new SIMADbCriteria();
            if (!empty($this->subnumber))
            {
                $criteria->condition = "number=:number and date_part('year', date)=:year and subnumber=:subnumber";
                $criteria->params = [':number'=>$this->number, ':year'=>$year, ':subnumber'=>$this->subnumber];
            }
            else
            {
                $criteria->condition = "number=:number and date_part('year', date)=:year";
                $criteria->params = [':number'=>$this->number, ':year'=>$year];
            }
            $filing_number = Filing::model()->find($criteria);
            
            if ($filing_number !== null)
            {
                $this->addError('number', 'Već postoji ovaj broj!');
            }
        }
    }
    
    public function checkLocatedAt()
    {
        if (!empty($this->located_at_id))//ako je prazno, postavlja se na trenutno ulogovanog korisnika
        {
            if (!$this->isNewRecord) //ako je novi, moze da se prebaci na bilo koga
            {
                if ($this->__old['located_at_id'] != $this->located_at_id)
                {
                    if (Yii::app()->isWebApplication()) //vazi samo za web aplikaciju
                    {
                        if ($this->located_at_id != Yii::app()->user->id && $this->__old['located_at_id'] != Yii::app()->user->id)
                        {
                            $this->addError('located_at_id', Yii::t('LegalModule.Filing','FilingLocatedAtChange'));
                        }
                    }
                }
            }
        }
    }
    
    public function checkRegister()
    {
        if  (
                !empty($this->registry_id)
                &&
                intval($this->__old['registry_id'])!==intval($this->registry_id)
            )
        {
            if ($this->located_at_id != Yii::app()->user->id && !empty($this->located_at_id))
            {
                $this->addError('registry_id', 'Dokument mora biti kod vas da bi promenili registrator');
            }
            if (!Yii::app()->user->checkAccess('RegistryAll',[],$this) 
                        && !$this->isElectronicRegister() && !$this->canUserChangeRegistry())
            {
                $this->addError('registry_id', 'Ne mozete da izaberete ovaj registar');
            }
        }
    }
    
    public function checkReturned($attribute, $params)
    {
        if ($this->columnChangedOrNew('returned') && $this->returned)
        {
            if (!$this->isElectronicRegister() && $this->located_at_id != Yii::app()->user->id)
            {
                $this->addError('returned', Yii::t('LegalModule.Filing','ReturnLocationAtPerson'));
                $this->addError('located_at_id', Yii::t('LegalModule.Filing','ReturnLocationAtPerson'));
            }
            if (empty($this->registry_id))
            {
                $this->addError('returned', Yii::t('LegalModule.Filing','ReturnMustFillRegistry'));
                $this->addError('registry_id', Yii::t('LegalModule.Filing','ReturnMustFillRegistry'));
            }
//            if (!Yii::app()->user->checkAccess('modelFilingReturn') )
//            {
//                $this->addError('returned', 'Nemate privilegiju da vratite dokument!');
//            }
        }
    }

    public function checkConfirm()
    {
        if (!$this->hasErrors() && $this->confirmed && !$this->__old['confirmed'])
        {
            $types_for_any_filing_direction = Yii::app()->configManager->get('base.types_for_any_filing_direction', false);
            if(!isset($types_for_any_filing_direction))
            {
                $types_for_any_filing_direction = [];
            }
            if ($this->isArchive())
            {
                if ($this->partner_id != Yii::app()->company->id 
                    && (!isset($this->file) || !in_array($this->file->document_type_id,$types_for_any_filing_direction)))
                {
                    $this->addError('in', Yii::t('LegalModule.Filing','ConfirmMustBeCompany',['{company}'=>Yii::app()->company->DisplayName]));
                    $this->addError('partner_id', Yii::t('LegalModule.Filing','ConfirmMustBeCompany',['{company}'=>Yii::app()->company->DisplayName]));
                }
                if (gettype($this->filing_to_delivery_types)==='string')
                {
                    $_dt = CJSON::decode($this->filing_to_delivery_types)['ids'];
                }
                else
                {
                    $_dt = $this->filing_to_delivery_types;
                }

                if  (
                        (count($_dt) > 0)
                    )
                {
                    $this->addError('in', Yii::t('LegalModule.Filing','ArchiveDontHaveDelivery'));
                    $this->addError('delivery_types', Yii::t('LegalModule.Filing','ArchiveDontHaveDelivery'));
                }
            }
            elseif ($this->isIN() || $this->isOUT())
            {
                if ($this->partner_id == Yii::app()->company->id
                    && !in_array($this->file->document_type_id,$types_for_any_filing_direction))
                {
                    $this->addError('in',  Yii::t('LegalModule.Filing','ConfirmCanNotBeCompany',['{company}'=>Yii::app()->company->DisplayName]));
                    $this->addError('partner_id',  Yii::t('LegalModule.Filing','ConfirmCanNotBeCompany',['{company}'=>Yii::app()->company->DisplayName]));
                }
                if (gettype($this->filing_to_delivery_types)==='string')
                {
                    $_dt = CJSON::decode($this->filing_to_delivery_types)['ids'];
                }
                else
                {
                    $_dt = $this->filing_to_delivery_types;
                }

                if  (count($_dt) == 0)
                {
                    $this->addError('confirmed', Yii::t('LegalModule.Filing','ConfirmedDeliveryType'));
                    $this->addError('delivery_types', Yii::t('LegalModule.Filing','ConfirmedDeliveryType'));
                }
            }
            else
            {
                throw new Exception('Zavodni broj za ID: '.$this->id.' nema smer - type:'.  gettype($this->id).' value: '.CJSON::encode($this->id));
            }
        }
    }
    
    private function isElectronicRegister()
    {
        $electronic_register = Yii::app()->configManager->get('legal.electronic_register');
        if (isset($this->registry_id) && $this->registry_id != null && $electronic_register != '')
        {
            if ($electronic_register == $this->registry_id)
            {
                return true;
            }
        }
        return false;
    }
    
    private function canUserChangeRegistry()
    {
        if ($this->registry_id !== null)
        {
            foreach ($this->located_at->registries as $registry)
            {
                if ($registry->id===intval($this->registry_id))
                {
                    return true;
                }
            }
        }
        if (isset(Yii::app()->controller))
        {
            Yii::app()->raiseNote(Yii::app()->user->model->AvailableRegistriesHTML);
        }
        return false;
    }
    
    public function beforeValidate()
    {
        if (empty($this->located_at_id))
        {
            $this->located_at_id = Yii::app()->user->id;
        }
        if (empty($this->located_at))
        {
            $this->located_at = User::model()->findByPkWithCheck($this->located_at_id);
        }
        
        return parent::beforeValidate();
    }

    public function BeforeSave() 
    {
        
        /** ukoliko je broj novi, setuje ko je zaveo */
        if ($this->isNewRecord) {
            $this->creator_id = Yii::app()->user->id;
        }
        
        /** ukoliko je postavljen elektronski registrator */
//        if (empty($this->located_at_id))
//        {
//            $this->located_at_id = Yii::app()->user->id;
//        }

        /** svaki zavodni broj mora da ima fajl - ovo se uglavnom desava kad je novi bro
         * svaki zavodni broj mora da bude u try catch delu i da hvata dupli zavodni broj
         * to se desava prilikom rezervacije novog broja i prilikom zavodjenja 
         */
        if (!isset($this->file) && ($this->id == null || $this->id == '')) 
        {
//            throw new SIMAWarnException('Zavodni broj mora da ima svoj fajl, ne moze da se pravi naknadno!!');
            $newFile = new File();
            $newFile->responsible_id = $this->located_at_id;
            $newFile->save();
            $this->id = $newFile->id;
        }
        
        else if ($this->getIsNewRecord())
        {
            //$this->subject = $this->file->name;
            if ($this->file->responsible_id === null && !$this->file->isSystemDocument())
            {
                $this->file->responsible_id = Yii::app()->user->id;
                $this->file->save();
            }
        }
        
        if (isset($this->file))
        {
            $this->checkContribsBelongsDiff();
        }
        
        return parent::BeforeSave();
    }    

    public function afterSave() 
    {
        parent::afterSave();
        
        $_old_located_at_id = $this->columnOldValue('located_at_id');
        /** kacenje FileTag-a filing_book */
        if (isset($this->file)) 
        {
            //located_at_id je obavezno
//            if (isset($this->located_at_id) && $this->__old['located_at_id'] != $this->located_at_id && $this->located_at_id != Yii::app()->user->id)
            if ($this->columnChangedOrNew('located_at_id'))
            {
                if (!empty($_old_located_at_id))
                {
                    Yii::app()->warnManager->sendWarns($_old_located_at_id, 'WARN_FILE_NORETURNED');
                }
                Yii::app()->warnManager->sendWarns($this->located_at_id, 'WARN_FILE_NORETURNED');
            }
        }
        
        $_file = File::model()->resetScope()->findByPk($this->id);
        //svima saljemo obavestenja, previse deluje zamrseno da ovde pisemo sile ifove
        $_warns_to_send = [
            'WARN_UNCONFIRMED_ARCHIVE_FILINGS', 
            'WARN_UNCONFIRMED_IN_FILINGS', 
            'WARN_UNCONFIRMED_OUT_FILINGS'
        ];
        $warn_users = [
            $this->located_at_id,
            $this->creator_id
        ];
        if (!empty($_file->responsible_id))
        {
            array_push($warn_users, $_file->responsible_id);
        }
        Yii::app()->warnManager->sendWarns($warn_users,  $_warns_to_send);
        
        $bills = isset($_file->authorization_file_for)?$_file->authorization_file_for:array();
        foreach ($bills as $bill)
        {
            if(FilingContrib::model()->findByAttributes(array('note_id'=>  $this->id, 'contrib_id'=>$bill->id))==null)
            {
                $link = new FilingContrib();
                $link->note_id = $this->id;
                $link->contrib_id = $bill->id;
                $link->save();
            }
        }
        //ako je promenjeno polje kod koga je onda se dokumenat ubacuje u listu transfera
        //ako je stavka nova, opet, onaj koji je zaveo, on u sustini slaje dokument
        if (
                $this->columnChangedOrNew('located_at_id')
            )
        {
            $file_transfers = FileTransfer::model()->findByAttributes(array('file_id'=> $this->id));
        
            if ($this->located_at_id == Yii::app()->user->id )
            {
                if (isset($file_transfers))
                {
                    $file_transfers->delete();
                }
            }
            elseif ($_old_located_at_id === Yii::app()->user->id || empty($_old_located_at_id))//ako je NEW, onda je $_old_located_at_id prazan
            {
                if ($file_transfers == null)
                {
                    $file_transfers = new FileTransfer();
                    $file_transfers->file_id = $this->id;   
                }
                $file_transfers->sender_id = Yii::app()->user->id;
                $file_transfers->recipient_id = $this->located_at_id;
                $file_transfers->save();
            }
            else
            {
                throw new SIMAWarnException('Mozete da promenite samo ako je kod vas, ili na sebe');
            }
            
        }
        
        //ako je promenjen korisnik kod koga je dokument onda se staroj skida tag a novoj dodaje
//        if ($this->__old['located_at_id'] !== intval($this->located_at_id))
        if ($this->columnChangedOrNew('located_at_id'))
        {
//            if ($this->__old['located_at_id']!==null)
            if (!empty($_old_located_at_id))
            {
                EmployeeTQLLimit::model()->removeFileFromTag($_old_located_at_id, 'LocatedAt', $this->id);
            }
            if ($this->located_at_id!==null)
            {
                EmployeeTQLLimit::model()->addFileToTag($this->located_at_id, 'LocatedAt', $this->id);
            }
        }
        
    }
    
    public function checkContribsBelongsDiff()
    {
        $old_belongs = [];
        $old_contribs = [];
        
        if (!$this->isNewRecord)
        {                
            $old_belongs = CJSON::decode($this->old_file_belongs, false);
            $old_filing = $this::model()->findByPk($this->id);
            if (!empty($old_filing))
            {
                $old_contribs = $old_filing->contribs;
            }
        }
//            $this->file->refresh();
        //provera da li postoji razlika u pripadnostima            
        $new_belongs = $this->file->file_belongs;
        $belongs_diff = $this->checkDiff($old_belongs, $new_belongs);
        
        //provera da li postoji razlika u prilozima
        $new_contribs = $this->contribs;
        $contribs_diff = $this->checkDiff($old_contribs, $new_contribs);
        if (
                count($belongs_diff['to_add']) > 0 || count($belongs_diff['to_del']) > 0 ||
                count($contribs_diff['to_add']) > 0 || count($contribs_diff['to_del']) > 0
           )
        {
           
            $changes = $this->configChanges($belongs_diff['to_add'], $belongs_diff['to_del'], $contribs_diff['to_add'], $contribs_diff['to_del']); 
            if (isset(Yii::app()->controller) && !empty($changes))
            {
                Yii::app()->controller->raiseAction('sima.misc.reCheckContribTagsInFiling', array($changes), true);
            }
        }

        //proveravamo da postoje novo dodati prilozi. Ako postoje onda pozivamo akciju koja ce ponuditi korisniku da odabere
        //osobe(koje pregledaju zavodni broj) koje ce da doda u preglede za svaki prilog
        if (count($contribs_diff['to_add']) > 0)
        {
            if (isset(Yii::app()->controller) && count($this->file->signature_views) > 0)
                Yii::app()->controller->raiseAction('sima.files.addUsersToContribSignatureViews', array($this->id,$contribs_diff['to_add']));
        }
    }
    
    private function configChanges($belongs_to_add_ids, $belongs_to_del_ids, $contribs_to_add_ids, $contribs_to_del_ids)
    {
        $contribs_ids = $this->getContribs();
        $filing_key = "{$this->id}_filing";
        //pakovanje niza koji se salje
        $changes = [
            $filing_key => []
        ];

        //prolazimo skroz sve priloge koji su vidljivi(stari koji su ostali ili novododati) i dodajemo/skidamo pripadnosti
        foreach ($contribs_ids as $contrib_id)
        {
            $contrib = File::model()->resetScope()->findByPk($contrib_id);
            if (!empty($contrib))
            {
                $tags = [];
                //ako je prilog novo dodati onda mu se dodaju sve nove pripadnosti
                if (in_array($contrib->id, $contribs_to_add_ids))
                {
                    $file_belongs = $this->file->file_belongs; 
                    if (is_string($file_belongs))
                    {
                        $file_belongs = CJSON::decode($file_belongs,false);
                        $file_belongs = $file_belongs->ids;
                        foreach ($file_belongs as $value) 
                        {
                            $value->DisplayName = isset($value->display_name)?$value->display_name:'';
                        }
                    }
                    foreach($file_belongs as $belong_tag)
                    {
                        $tag_conn = FiletoFileTag::model()->findByAttributes(array('file_tag_id' => $belong_tag->id, 'file_id' => $contrib->id));
                        if ($tag_conn === null)
                        {
                            $_tag = array(
                                'add' => true,
                                'id' => $belong_tag->id,
                                'display_name' => $belong_tag->DisplayName
                            );
                            array_push($tags, $_tag);
                        }
                    }
                }
                //inace ako prilog nije promenjen dodaju mu se pripadnosti razlike starih i novih
                else
                {
                    foreach($belongs_to_add_ids as $belong_tag_id)
                    {
                        $tag_conn = FiletoFileTag::model()->findByAttributes(array('file_tag_id' => $belong_tag_id, 'file_id' => $contrib->id));
                        if ($tag_conn === null)
                        {
                            $belong_tag = FileTag::model()->findByPk($belong_tag_id);
                            if (!empty($belong_tag))
                            {
                                $_tag = array(
                                    'add' => true,
                                    'id' => $belong_tag->id,
                                    'display_name' => $belong_tag->DisplayName
                                );
                                array_push($tags, $_tag);
                            }
                        }
                    }
                    foreach($belongs_to_del_ids as $belong_tag_id)
                    {
                        $tag_conn = FiletoFileTag::model()->findByAttributes(array('file_tag_id' => $belong_tag_id, 'file_id' => $contrib->id));
                        if ($tag_conn !== null)
                        {
                            $belong_tag = FileTag::model()->findByPk($belong_tag_id);
                            if (!empty($belong_tag))
                            {
                                $_tag = array(
                                    'add' => false,
                                    'id' => $belong_tag->id,
                                    'display_name' => $belong_tag->DisplayName
                                );
                                array_push($tags, $_tag);
                            }
                        }
                    }
                }
                if (count($tags) > 0)
                {
                    $changes[$filing_key]["{$contrib->id}_contrib"]['tags'] = $tags;
                }
            }
        }
        //prolazimo kroz sve priloge koji su skinuti i dodajemo tagove za skidanje
        foreach ($contribs_to_del_ids as $contrib_to_del_id)
        {
            $contrib = File::model()->resetScope()->findByPk($contrib_to_del_id);
            if (!empty($contrib))
            {
                $tags = [];
                //ako je prilog skinut onda mu se skidaju sve stare pripadnosti
                $old_belongs = CJSON::decode($this->old_file_belongs, false);
                if (isset($old_belongs->ids))
                {
                    $old_belongs = $old_belongs->ids;
                    foreach ($old_belongs as $value) 
                    {
                        $value->DisplayName = isset($value->display_name)?$value->display_name:'';
                    }
                }
                foreach($old_belongs as $belong_tag)
                {
                    $tag_conn = FiletoFileTag::model()->findByAttributes(array('file_tag_id' => $belong_tag->id, 'file_id' => $contrib->id));
                    if ($tag_conn !== null)
                    {
                        $_belong_tag = FileTag::model()->findByPk($belong_tag->id);
                        if(!empty($_belong_tag))
                        {
                            $_tag = array(
                                'add' => false,
                                'id' => $_belong_tag->id,
                                'display_name' => $_belong_tag->DisplayName
                            );
                            array_push($tags, $_tag);
                        }
                    }
                }
                //ako ima tagova za menjanje onda se dodaje prilog inace se ne dodaje
                if (count($tags) > 0)
                {
                    $changes[$filing_key]["{$contrib->id}_contrib"]['tags'] = $tags;
                }
            }
        }
        
        return count($changes[$filing_key]) > 0 ? $changes : null;
    }
    
    private function getContribs()
    {
        $contribs_ids = array();
        if (is_string($this->contribs))
        {
            $contribs = CJSON::decode($this->contribs);
            foreach ($contribs['ids'] as $_id)
            {
                array_push($contribs_ids, $_id['id']);
            }
        }
        else
        {
            foreach ($this->contribs as $contrib)
            {
                array_push($contribs_ids, $contrib->id);
            }
        }
        
        return $contribs_ids;
    }
    
    private function checkDiff($old, $new)
    {
        $old_ids = array();
        $new_ids = array();
        if (is_string($new))
        {
            $new = CJSON::decode($new);
            foreach ($new['ids'] as $_new)
            {
                array_push($new_ids, $_new['id']);
            }
        }
        else
        {
            foreach ($new as $_new)
            {
                array_push($new_ids, $_new->id);
            }
        }
        
        if (isset($old->ids))
        {
            $old = $old->ids;
        }
        foreach ($old as $_old)
        {
            array_push($old_ids, $_old->id);
        }

        $to_add = array_diff($new_ids, $old_ids);
        $to_del = array_diff($old_ids, $new_ids);

        $diff = array(
            'to_add' => $to_add,
            'to_del' => $to_del
        );
        
        return $diff;
    }

    public function forDate($date) {
        //TODO(sercko): sql injection fix
        $this->getDbCriteria()->mergeWith(array(
            'condition' => 'date=' . $date,
        ));
        return $this;
    }

    public function scopes() {
        $alias = $this->getTableAlias();
        return array(
            'recently' => array(
                'order' => "${alias}.date DESC, ${alias}.number DESC, ${alias}.subnumber DESC, ${alias}.id",
//                'with' => array('partner', 'delivery_type', 'creator', 'responsible', 'registry'),
            ),
            'forGuiTable' => array(
//                'with' => array('partner', 'delivery_types', 'creator', 'responsible', 'registry'),
//                'with' => array(),
            ),
        );
    }

    public function modelSettings($column)
    {
        switch ($column) 
        {
            case 'options': 
                $options = array();
                if (Yii::app()->user->checkAccess('OpenDialog', [], $this->file))
                {
                    $options[] = 'open';
                }
                if (Yii::app()->user->checkAccess('FormOpen', [], $this))
                {
                    $options[] = 'form';
                }
                if (Yii::app()->user->checkAccess('ScanOrder', [], $this->file))    
                {
                    $options[] = 'orderScan';
                }
                if (Yii::app()->user->checkAccess('Download', [], $this->file))
                {
                    $options[] = 'download';
                }
                return $options;
            case 'textSearch': return array_merge(parent::modelSettings($column),[
                'filing_number' => 'text',
            ]);
            case 'filters': return array_merge(parent::modelSettings($column),[
                'filing_number' => 'text',
                'partner'=>'relation',
                'date' => 'date_range',
                'in' => 'boolean',
                'located_at' => 'relation',
                'located_at_id' => 'dropdown',
                'registry_id' => 'dropdown',
                'registry' => 'relation',
                'creator_id' => 'dropdown',
                'creator' => 'relation',
                'returned' => 'boolean',
                'scan_order' => 'boolean',
                'comment' => 'text',
                'belongs'=>array('belongs','func'=>'filter_belongs'),
                'file'=>'relation'
            ]);
            case 'multiselect_options': return ['orderScan', 'addTags'];
            case 'statuses': return [
                'confirmed' => array(
                    'title' => 'Potvrdjeno',
                    'timestamp' => 'confirmed_timestamp',
                    'user' => array('confirmed_user_id','relName'=>'confirmed_user'),
                    'checkAccessConfirm' => 'canConfirmFilingNumber', 
                    'checkAccessRevert' => 'RevertConfirm',
                ),
                'returned' => array(
                    'title' => 'Vraceno',
                    'timestamp' => 'returned_timestamp',
                    'user' => array('returned_user_id','relName'=>'returned_user'),
                    'checkAccessConfirm' => 'Return',
                    'checkAccessRevert' => 'RevertReturn',
                ),
            ];
            case 'GuiTable': return ['scopes' => ['forGuiTable']];
            case 'update_relations': return ['file'];
            case 'form_list_relations': return ['contribs', 'filing_to_delivery_types'];
            case 'mutual_forms': return [
                'base_relation'=>'file', 'relations' => []
            ];
            case 'warn_default_model': return File::class;

            default: return parent::modelSettings($column);
        }
    }
    
    
    public function canConfirmFilingNumber($user_id = null)
    {
        $errors = [];
        if ($user_id === null)
        {
            $user_id = Yii::app()->user->id;
        }
        if ($this->isArchive())
        {
            if (!isset($this->file))
            {
                $errors[] = 'Za potvrdu, mora biti postavljen fajl';
            }
            elseif ($this->file->responsible_id!=$user_id)
            {
                $errors[] =  Yii::t('LegalModule.Filing', 'ArchiveConfirmsResponsible',[':responsible'=>$this->file->responsible->DisplayName]);
            }
        }
        if ($this->isIN() && $this->creator_id!=$user_id)
        {
            $errors[] =  Yii::t('LegalModule.Filing', 'IncomeConfirmsCreator',[':creator'=>$this->creator->DisplayName]);
        }
        if ($this->isOUT() && $this->located_at_id!=$user_id)
        {
            $errors[] =  Yii::t('LegalModule.Filing', 'OutcomConfirmLocatedAt',[':located_at'=>$this->located_at->DisplayName]);
        }

        return $errors;
    }
    
    public function checkReturnedAccess($user_id = null)
    {
        if ($user_id === null)
        {
            $user_id = Yii::app()->user->id;
        }   
        return $this->located_at_id == $user_id;
    }
    
    public function isArchive()
    {
        return $this->in === NULL;
    }
    
    public function isIN()
    {
        return (gettype($this->in)=='string' && $this->in==='1') || $this->in===TRUE;
    }
    
    public function isOUT()
    {
        return (gettype($this->in)=='string' && $this->in==='0') || $this->in===FALSE;
    }
        
    
    public function filter_belongs($condition)
    {
        if ($this->belongs != null) 
        {
            $query = $this->belongs;
            if ($this->belongs_recursive === 'true')
                $query.='$R';
            $condition->mergeWith(SIMAFileStorage::applyQuery($query));
        }
    }    
    /**
     * ova funkcija je redefinisana zbog zavodnog broja. 
     * Ukoliko se slucajno desi da iskoci greska zbog duplog zavodnog broja
     * da se pokusa ponovo save
     * http://www.postgresql.org/docs/9.1/static/errcodes-appendix.html
     */
    public function save($runValidation = true, $attributes = null, $throwExceptionOnFail = true)
    {
        $_saved = false;
        $_result = false;
        $i = 10;
        while (!$_saved && $i>0)
        {
            try
            {
                $_result = parent::save($runValidation, $attributes, $throwExceptionOnFail);
                $_saved = true;
            }
            catch (CDbException $e)
            {
                if ($e->getCode()!=23505)
                {
                    
                    throw $e; //rethrow
                }
            }
            $i--;
        }
        return $_result;
    }

}
