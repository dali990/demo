<?php

class ContractPartnerStatus extends SIMAActiveRecord
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'public.contract_partner_statuses';
    }

    public function rules()
    {
        return array(
            array('name', 'required', 'message' => 'Polje "{attribute}" mora biti uneto'),
            array('description', 'safe'),
        );
    }

    public function scopes()
    {
        return array(
            'byName' => array(
                'order' => 'name, id',
            ),
        );
    }

    public function relations($child_relations = [])
    {
        return array(
            'contracts' => array(self::HAS_MANY, 'Contract', 'contract_partner_status_id'),
        );
    }

}
