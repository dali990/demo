<?php 

class EmployeeInsurancePerson extends SIMAActiveRecord
{

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
 
    public function tableName()
    {
        return 'public.employees_insurance_persons';
    }
    
    public function moduleName()
    {
        return 'legal';
    }
    
    public function __get($column)
    {
        switch ($column)
        {            
            default: return parent::__get($column);
        }
    }
    
    public function relations($child_relations = [])
    {
        return array(
            'person' => array(self::BELONGS_TO, 'Person', 'person_id'),
            'employee' => array(self::BELONGS_TO, 'Employee', 'employee_id'),
            'relationship_type' => array(self::BELONGS_TO, 'RelationshipType', 'relationship_type_id')
        );
    }
    
    public function rules()
    {
    	return array(
            array('employee_id, person_id, relationship_type_id, end_date', 'required'),
            array('description', 'safe'),
            array('person_id', 'checkPerson'),
    	);
    }
    
    public function checkPerson($attribute, $params)
    {
        if (!empty($this->employee_id) && !empty($this->person_id) && $this->__old['person_id'] !== intval($this->person_id))
        {
            $employee_insurance_person = EmployeeInsurancePerson::model()->findByAttributes([
                'employee_id'=>$this->employee_id,
                'person_id'=>$this->person_id
            ]);
            if ($employee_insurance_person !== null)
            {
                $this->addError('person_id', 'Već je dodat ovaj član osiguranja.');
            }
        }
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters' : return array(
                'person' => 'relation',
                'employee' => 'relation',
                'relationship_type' => 'relation',
                'end_date' => 'date_range',
                'description' => 'text'
            );
            default: return parent::modelSettings($column);
        }
    }
}