<?php

class BusinessOfferVariationPredefinedItem extends SIMAActiveRecord
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
 
    public function tableName()
    {
        return 'legal.business_offer_variation_predefined_items';
    }
    
    public function moduleName()
    {
        return 'legal';
    }
    
    public function __get($column) 
    {
        switch ($column) {
            case 'DisplayName': return $this->name . (!empty($this->description) ? " ({$this->short_description_as_string})" : '');
            case 'SearchName': return $this->DisplayName;
            case 'short_description_as_string': 
                $description = html_entity_decode(strip_tags($this->description));

                return strlen($description) > 100 ? substr($description, 0, 50)."..." : $description;
            default: return parent::__get($column);
        }
    }
    
    public function rules()
    {
        return [
            ['name, description', 'required'],
            ['name', 'unique']
        ];
    }
    
    public function scopes()
    {
        $alias = $this->getTableAlias();

        return [
            'byName' => [
                'order' => "$alias.name ASC"
            ]
        ];
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters' : return array_merge(parent::modelSettings('filters'), [
                'name' => 'text',
                'description' => 'text'
            ]);
            case 'textSearch': return [
                'name' => 'text',
                'description' => 'text'
            ];
            default: return parent::modelSettings($column);
        }
    }
    
    protected function modelOptions(\User $user = null):array
    {
        return ['form', 'delete'];
    }
}