<?php

class BusinessOfferVariation extends SIMAActiveRecord
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
 
    public function tableName()
    {
        return 'legal.business_offer_variations';
    }
    
    public function moduleName()
    {
        return 'legal';
    }
    
    public function __get($column) 
    {
        switch ($column) {
            case 'DisplayName': return $this->name;
            case 'SearchName': return $this->DisplayName;
            case 'has_discount': return !empty($this->discount_percentage) || !empty($this->discount_value);
            case 'value_with_additional_discount': 
                $return = $this->value_with_discount;
                if (!empty($this->discount_percentage))
                {
                    $return *= ((100.00 - $this->discount_percentage)/100);
                }
                else if (!empty($this->discount_value))
                {
                    $return -= $this->discount_value;
                }
                
                return $return;
            case 'calculated_advance': 
                if(!empty($this->advance_value))
                {
                    return $this->advance_value;
                }
                else if(!empty($this->advance_percentage))
                {
                    return $this->value_with_additional_discount * ((100.00 - $this->advance_percentage)/100);
                }
                return 0.00;
            default: return parent::__get($column);
        }
    }
    
    public function rules()
    {
        return [
            ['name, business_offer_id', 'required'],
            ['value, confirmed, confirmed_by_user_id, confirmed_timestamp, sent, sent_by_user_id, sent_timestamp, description, created_by_user_id, created_timestamp, '
                . 'discount_percentage, discount_value, value_with_discount, order_number, footer, advance_percentage, advance_value', 'safe'],
            ['value', 'checkNumericNumber', 'precision' => 20, 'scale' => 2],
            ['value_with_discount', 'checkNumericNumber', 'precision' => 20, 'scale' => 2],
            ['discount_percentage', 'checkNumericNumber', 'precision' => 5, 'scale' => 2],
            ['discount_percentage', 'numerical', 'max' => 100],
            ['discount_value', 'checkNumericNumber', 'precision' => 20, 'scale' => 2],
            ['discount_value', 'checkDiscount'],
            ['advance_percentage', 'checkNumericNumber', 'precision' => 5, 'scale' => 2],
            ['advance_percentage', 'numerical', 'max' => 100],
            ['advance_value', 'checkNumericNumber', 'precision' => 20, 'scale' => 2],
            ['advance_value', 'checkAdvance'],
            ['business_offer_id, confirmed_by_user_id, sent_by_user_id, created_by_user_id', 'default',
                'value' => null,
                'setOnEmpty' => true,
                'on' => ['insert', 'update']
            ]
        ];
    }
    
    public function checkDiscount()
    {
        if (!$this->hasErrors() && ($this->isNewRecord || $this->columnChanged('discount_percentage') || $this->columnChanged('discount_value')))
        {
            if (!empty($this->discount_percentage) && !empty($this->discount_value))
            {
                $this->addError('discount_value', Yii::t('LegalModule.BusinessOfferVariation', 'CanNotFillTogetherDiscountPercentageAndDiscountValue'));
            }
        }
    }

    public function checkAdvance()
    {
        if (!$this->hasErrors() && ($this->isNewRecord || $this->columnChanged('advance_percentage') || $this->columnChanged('advance_value')))
        {
            if (!empty($this->advance_percentage) && !empty($this->advance_value))
            {
                $this->addError('advance_value', Yii::t('LegalModule.BusinessOfferVariation', 'CanNotFillTogetherAdvancePercentageAndAdvanceValue'));
            }
        }
    }
    
    public function relations($child_relations = [])
    {
        return [
            'file' => [self::BELONGS_TO, File::class, 'id'],
            'business_offer' => [self::BELONGS_TO, BusinessOffer::class, 'business_offer_id'],
            'confirmed_by_user' => [self::BELONGS_TO, User::class, 'confirmed_by_user_id'],
            'sent_by_user' => [self::BELONGS_TO, User::class, 'sent_by_user_id'],
            'created_by_user' => [self::BELONGS_TO, User::class, 'created_by_user_id'],
            'variation_items' => [self::HAS_MANY, BusinessOfferVariationItem::class, 'business_offer_variation_id'],
            'root_variation_items' => [self::HAS_MANY, BusinessOfferVariationItem::class, 'business_offer_variation_id', 'condition' => 'parent_id is null', 'scopes' => 'byFullPath']
        ];
    }
    
    public function scopes()
    {
        $alias = $this->getTableAlias();

        return [
            'byName' => [
                'with' => 'business_offer',
                'order' => "business_offer.id, $alias.name ASC"
            ],
            'byTime' => [
                'with' => 'business_offer',
                'order' => "business_offer.id, $alias.created_timestamp ASC"
            ],
            'byTimeDesc' => [
                'with' => 'business_offer',
                'order' => "business_offer.id, $alias.created_timestamp DESC"
            ]
        ];
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters' : return array_merge(parent::modelSettings('filters'), [
                'name' => 'text',
                'business_offer' => 'relation',
                'value' => 'numeric',
                'value_with_discount' => 'numeric',
                'confirmed' => 'boolean',
                'confirmed_by_user' => 'relation',
                'confirmed_timestamp' => 'date_time_range',
                'sent' => 'boolean',
                'sent_by_user' => 'relation',
                'sent_timestamp' => 'date_time_range',
                'description' => 'text',
                'created_by_user' => 'relation',
                'created_timestamp' => 'date_time_range',
                'variation_items' => 'relation',
                'root_variation_items' => 'relation',
                'discount_percentage' => 'numeric',
                'discount_value' => 'numeric',
                'order_number' => 'integer',
                'footer' => 'text',
                'advance_percentage' => 'numeric',
                'advance_value' => 'numeric'
            ]);
            case 'textSearch': return [
                'name' => 'text'
            ];
            case 'number_fields': return ['value', 'discount_percentage', 'discount_value', 'advance_percentage', 'advance_value'];
            case 'statuses': return [
                'confirmed' => [
                    'title' => Yii::t('LegalModule.BusinessOfferVariation', 'Confirmed'),
                    'timestamp' => 'confirmed_timestamp',
                    'user' => ['confirmed_by_user_id', 'relName' => 'confirmed_by_user'],
                    'checkAccessConfirm' => 'checkConfirmRevert',
                    'checkAccessRevert' => 'checkConfirmRevert',
                    'onConfirm' => 'onConfirm',
                ],
                'sent' => [
                    'title' => Yii::t('LegalModule.BusinessOfferVariation', 'Sent'),
                    'timestamp' => 'sent_timestamp',
                    'user' => ['sent_by_user_id', 'relName' => 'sent_by_user'],
                    'checkAccessConfirm' => 'checkSent',
                    'onConfirm' => 'onSent',
                    'onRevert' => 'onSentRevert'
                ]
            ];
            case 'update_relations': return [
                'business_offer'
            ];
            case 'GuiTable': return [
                'without_order_number' => true
            ];
            default: return parent::modelSettings($column);
        }
    }
    
    protected function modelOptions(\User $user = null):array
    {
        $options = ['open'];
        
        if ($this->confirmed === false && $this->sent === false)
        {
            $options[] = 'form';
            $options[] = 'delete';
        }
        
        return $options;
    }
    
    public function checkConfirmRevert()
    {
        $errs = [];
        
        if ($this->sent === true)
        {
            $errs[] = Yii::t('LegalModule.BusinessOfferVariation', 'CanNotChangeConfirmationBecauseVariationIsSent');
        }

        if (count($errs) > 0)
        {
            return $errs;
        }
        else
        {
            return true;
        }
    }
    
    public function onConfirm()
    {
        $this->generatePdf();
    }
    
    public function checkSent()
    {
        $errs = [];
        
        if ($this->confirmed === false)
        {
            $errs[] = Yii::t('LegalModule.BusinessOfferVariation', 'CanNotSendBecauseVariationIsNotConfirmed');
        }

        if (count($errs) > 0)
        {
            return $errs;
        }
        else
        {
            return true;
        }
    }
    
    public function onSent()
    {
        if (!empty($this->business_offer->business_offer_variation_id) && $this->business_offer->business_offer_variation->sent === true)
        {
            $this->business_offer->business_offer_variation->sent = false;
            $this->business_offer->business_offer_variation->save();
        }

        $this->business_offer->business_offer_variation_id = $this->id;
        $this->business_offer->save();
        //potrebno kako bi popunio datum
        $this->refresh();
        //kada se potvrdi da je ponuda poslata potrebno je ponovo genrisati pdf kako bi se upisao novi datum
        $this->generatePdf();
    }
    
    public function onSentRevert()
    {
        if ($this->business_offer->business_offer_variation_id === $this->id)
        {
            $this->business_offer->business_offer_variation_id = null;
            $this->business_offer->save();
        }
    }
    
    public function beforeSave()
    {
        if ($this->isNewRecord && empty($this->id) && empty($this->file))
        {            
            $file = new File();
            $file->name = $this->name;
            if (Yii::app()->isWebApplication())
            {
                $file->responsible_id = Yii::app()->user->id;
            }
            $file->document_type_id = Yii::app()->configManager->get('legal.bid', false);
            $file->save();
            $this->id = $file->id;
            $this->file = $file;
            
            $offer_variations_count = BusinessOfferVariation::model()->count([
                'model_filter' => [
                    'business_offer' => [
                        'ids' => $this->business_offer_id
                    ]
                ]
            ]);
            $this->order_number = $offer_variations_count + 1;
        }
        
        if ($this->isNewRecord && Yii::app()->isWebApplication())
        {
            $this->created_by_user_id = Yii::app()->user->id;
            $this->created_timestamp = 'now()';
        }

        return parent::beforeSave();
    }
    
    public function calcValue()
    {
        $new_value = 0;
        $value_with_discount = 0;
        foreach ($this->root_variation_items as $root_variation_item)
        {
            $new_value += $root_variation_item->total_value;
            $value_with_discount += $root_variation_item->total_value_with_discount;
        }
        $this->value = $new_value;
        $this->value_with_discount = $value_with_discount;
        $this->save();
    }
    
    public function copyVariationItems(BusinessOfferVariation $new_variation)
    {
        foreach ($this->root_variation_items as $root_variation_item)
        {
            $root_variation_item->copyRecursive($new_variation);
        }
        
        $new_variation->calcValue();
    }
}