<?php

class EducationTitle extends SIMAActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'legal.education_titles';
    }

    public function moduleName()
    {
        return 'legal';
    }
    
    public function __get($column)
    {
        switch ($column)
        {
            case 'DisplayName': return $this->display_name;
            case 'SearchName': return $this->DisplayName;
            default: return parent::__get($column);
        }
    }
    
    public function scopes()
    {
        $alias = $this->getTableAlias();
        return [
            'byName' => [
                'order' => "$alias.name"
            ]
        ];
    }
    
    public function rules()
    {
        return array(
            array('name', 'required'),
            array('name', 'unique')
        );
    }
    
    public function relations($child_relations = [])
    {
        return array(
        );
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters': return array_merge(parent::modelSettings($column),array(
                'name' => 'text'
            ));
            case 'default_order_scope': return 'byName';
            default: return parent::modelSettings($column);
        }
    }
}

