<?php

class m0000000010_00106_contract_number_bugfix extends CDbMigration
{
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        Yii::app()->db->createCommand(
"
CREATE OR REPLACE FUNCTION contracts_number()
  RETURNS trigger AS
\$BODY\$
DECLARE
	cnt INT;
	exec_sql TEXT;
BEGIN
        -- Check that empname and salary are given
        IF NEW.number IS NULL THEN
		exec_sql := 'select COALESCE(MAX(number),0)+1 from ' || TG_TABLE_SCHEMA || '.contracts';
		EXECUTE exec_sql into NEW.number;
        END IF;
        IF NEW.parent_id IS NOT NULL THEN
		exec_sql := 'select number from ' || TG_TABLE_SCHEMA || '.contracts where id=' || NEW.parent_id;
		EXECUTE exec_sql into NEW.number;
		
		exec_sql := 'select count(*)+1 from ' || TG_TABLE_SCHEMA || '.contracts c where c.parent_id=' || NEW.parent_id || ' and 
			((select date from ' || TG_TABLE_SCHEMA || '.filing_book where id=c.id)
				<=
			(select date from ' || TG_TABLE_SCHEMA || '.filing_book where id=' || NEW.id ||'))
			and
			((select number from ' || TG_TABLE_SCHEMA || '.filing_book where id=c.id)
				<
			(select number from ' || TG_TABLE_SCHEMA || '.filing_book where id=' || NEW.id ||'))';
		EXECUTE exec_sql into NEW.annex_number;

        END IF;
        IF NEW.parent_id IS NULL THEN
		NEW.annex_number = 0;
        END IF;
	IF NEW.old THEN
		NEW.contract_number := 'S_'||NEW.number;
        ELSE
		NEW.contract_number := NEW.number;
        END IF;
        --NEW.contract_number := NEW.number;
        IF NEW.subnumber IS NOT NULL THEN
		NEW.contract_number := NEW.contract_number||'_'||NEW.subnumber;
        END IF;

        IF NEW.annex_number IS NOT NULL and NEW.annex_number>0 THEN
		NEW.contract_number := NEW.contract_number||'-A'||NEW.annex_number;
        END IF;
        
        RETURN NEW;
END;
\$BODY\$
  LANGUAGE plpgsql VOLATILE
  COST 100;
"
        )->execute();
    }

    public function safeDown()
    {
//        Yii::app()->db->createCommand(
//"
//
//"
//        )->execute();
        echo "m0000000010_00106_contract_number_bugfix does not support migration down.\n";
        return false;
    }
}