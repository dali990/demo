<?php

class m0000000034_00011_filing_book_drop_subject_and_type extends CDbMigration
{
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        Yii::app()->db->createCommand(
<<<'SIMAMIGRATESQL'

ALTER TABLE private.filing_book
  DROP COLUMN subject;
ALTER TABLE private.filing_book
  DROP COLUMN filing_type_id;
   


SIMAMIGRATESQL
        )->execute();
    }

    public function safeDown()
    {
//        Yii::app()->db->createCommand(
//<<<'SIMAMIGRATESQL'
//
//SIMAMIGRATESQL
//        )->execute();
        echo "m0000000034_00011_filing_book_drop_subject_and_type does not support migration down.\n";
        return false;
    }
}