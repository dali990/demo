<?php

class m0000000056_00002_bid_template_fixes extends CDbMigration
{
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        Yii::app()->db->createCommand(
<<<'SIMAMIGRATESQL'
    ALTER TABLE private.bids
        ADD COLUMN is_enable_template BOOLEAN NOT NULL DEFAULT FALSE;
        
    DO $$
        DECLARE bid_record record;
        DECLARE file_tag_id integer;
        BEGIN
           FOR bid_record IN 
           (
                select b.id, b.file_id from private.bids as b 
                where b.file_id is not null and not exists
                (
                    select 1 from files.files_to_file_tags as ftft
                    where ftft.file_id = b.file_id and ftft.file_tag_id in
                    (
                        select id from files.file_tags where model_table = 'base.themes' and model_id = b.id
                    )
                )
            )	
            LOOP
                file_tag_id := (select id from files.file_tags where model_table = 'base.themes' and model_id = bid_record.id);
                INSERT INTO files.files_to_file_tags (file_id, file_tag_id) VALUES (bid_record.file_id, file_tag_id);
            END LOOP;
    END; $$
SIMAMIGRATESQL
        )->execute();
    }

    public function safeDown()
    {
//        Yii::app()->db->createCommand(
//<<<'SIMAMIGRATESQL'
//
//SIMAMIGRATESQL
//        )->execute();
        echo "m0000000056_00002_bid_template_fixes does not support migration down.\n";
        return false;
    }
}