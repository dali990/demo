<?php

class m0000000007_00008_add_bids_tag_from_job extends EDbMigration
{
    public function safeUp()
    {
        Yii::app()->db
            ->createCommand("
    insert into files.files_to_file_tags (file_id,file_tag_id) 
	select file_id, (select id from files.file_tags where model_table='private.jobs' and model_id = job_id) 
        from private.bids 
        where job_id is not null and file_id is not null;")
            ->execute();
    }

    public function safeDown()
    {
        return false;
    }
	
}