<?php

class m0000000059_00000_create_occupations extends CDbMigration
{
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        Yii::app()->db->createCommand(
<<<'SIMAMIGRATESQL'
CREATE TABLE legal.occupations
(
    id serial NOT NULL,
    name text NOT NULL,
    code varchar(16) NOT NULL,
    parent_id integer,
    CONSTRAINT occupations_pkey PRIMARY KEY (id),
    CONSTRAINT occupations_code_key UNIQUE (code),
    CONSTRAINT occupations_parent_id_fkey FOREIGN KEY (parent_id)
      REFERENCES legal.occupations (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE RESTRICT
);
CREATE TRIGGER i_am_called_by_everyone
  AFTER INSERT OR UPDATE OR DELETE
  ON legal.occupations
  FOR EACH ROW
  EXECUTE PROCEDURE public.i_am_called_by_everyone();
SIMAMIGRATESQL
        )->execute();
    }

    public function safeDown()
    {
//        Yii::app()->db->createCommand(
//<<<'SIMAMIGRATESQL'
//
//SIMAMIGRATESQL
//        )->execute();
        echo "m0000000059_00000_create_occupations does not support migration down.\n";
        return false;
    }
}