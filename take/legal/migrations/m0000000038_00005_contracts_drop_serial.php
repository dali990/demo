<?php

class m0000000038_00005_contracts_drop_serial extends CDbMigration
{
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        Yii::app()->db->createCommand(
<<<'SIMAMIGRATESQL'
            ALTER TABLE private.contracts 
                ALTER COLUMN id DROP DEFAULT,
                ADD CONSTRAINT contracts_id_fkey FOREIGN KEY (id)
                    REFERENCES files.files (id) MATCH SIMPLE
                    ON UPDATE CASCADE ON DELETE RESTRICT;
            DROP SEQUENCE private.contracts_id_seq;
SIMAMIGRATESQL
        )->execute();
    }

    public function safeDown()
    {
//        Yii::app()->db->createCommand(
//<<<'SIMAMIGRATESQL'
//
//SIMAMIGRATESQL
//        )->execute();
        echo "m0000000038_00005_contracts_drop_serial does not support migration down.\n";
        return false;
    }
}