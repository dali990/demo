<?php

class m0000000020_00000_emp_remove_birthdate extends CDbMigration
{
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        Yii::app()->db->createCommand(
<<<'SIMAMIGRATESQL'
UPDATE persons p 
SET birthdate=(SELECT birth_date FROM employees e WHERE e.id=p.id) 
WHERE id IN (SELECT id FROM employees WHERE birth_date IS NOT NULL) and birthdate IS NULL;

ALTER TABLE employees DROP COLUMN birth_date;
SIMAMIGRATESQL
        )->execute();
    }

    public function safeDown()
    {
        echo "m0000000020_00000_emp_remove_birthdate does not support migration down.\n";
        return false;
    }
}