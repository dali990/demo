<?php

class m0000000056_00001_bid_currency_fixes extends CDbMigration
{
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        Yii::app()->db->createCommand(
<<<'SIMAMIGRATESQL'
    ALTER TABLE private.bids
        ALTER COLUMN currency_id DROP DEFAULT;
SIMAMIGRATESQL
        )->execute();
    }

    public function safeDown()
    {
//        Yii::app()->db->createCommand(
//<<<'SIMAMIGRATESQL'
//
//SIMAMIGRATESQL
//        )->execute();
        echo "m0000000056_00001_bid_currency_fixes does not support migration down.\n";
        return false;
    }
}