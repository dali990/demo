<?php

class m0000000055_00004_create_table_insurance_policies extends CDbMigration
{
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        Yii::app()->db->createCommand(
<<<'SIMAMIGRATESQL'
CREATE TABLE legal.insurance_policies
(
    id integer NOT NULL,
    policy_number text NOT NULL,
    insurance_company_id integer,
    bill_in_id integer,
    premium_amount numeric(20,2),
    start_date date NOT NULL,
    expire_date date NOT NULL,
    vinkulation_partner_id integer,
    CONSTRAINT insurance_policies_pkey PRIMARY KEY (id),
    CONSTRAINT insurance_policies_id_fkey FOREIGN KEY (id)
      REFERENCES files.files (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE RESTRICT,
    CONSTRAINT insurance_policies_insurance_company_id_fkey 
        FOREIGN KEY (insurance_company_id)
        REFERENCES public.companies (id) MATCH SIMPLE
        ON UPDATE CASCADE ON DELETE RESTRICT,
    CONSTRAINT insurance_policies_bill_in_id_fkey 
        FOREIGN KEY (bill_in_id)
        REFERENCES private.bills (id) MATCH SIMPLE
        ON UPDATE CASCADE ON DELETE RESTRICT,
    CONSTRAINT insurance_policies_vinkulation_partner_id_fkey 
        FOREIGN KEY (vinkulation_partner_id)
        REFERENCES public.partners (id) MATCH SIMPLE
        ON UPDATE CASCADE ON DELETE RESTRICT
);

DO $$ 
DECLARE
    vip RECORD;
    l_id integer;
BEGIN
    FOR vip IN (SELECT * FROM fixed_assets.vehicle_insurance_policies) LOOP
        INSERT INTO files.files (name) VALUES ('generic_name') RETURNING files.id into l_id;
        INSERT INTO legal.insurance_policies (id, policy_number, start_date, expire_date) VALUES (l_id, vip.number, vip.issue_date, vip.expire_date);
        PERFORM web_sima.create_new_file_version(l_id, 'generic_name', 0 , md5(''), 'shell', 2);	
    END LOOP;
END $$;

DROP TABLE fixed_assets.vehicle_insurance_policies;

UPDATE public.auth_items 
SET name = 'modelInsurancePolicyExpireShowInCalendar' 
WHERE name='modelVehicleInsurancePolicyExpireShowInCalendar';

SIMAMIGRATESQL
        )->execute();
    }

    public function safeDown()
    {
//        Yii::app()->db->createCommand(
//<<<'SIMAMIGRATESQL'
//
//SIMAMIGRATESQL
//        )->execute();
        echo "m0000000055_00004_create_table_insurance_policies does not support migration down.\n";
        return false;
    }
}