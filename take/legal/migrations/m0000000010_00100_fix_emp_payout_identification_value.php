<?php

class m0000000010_00100_fix_emp_payout_identification_value extends CDbMigration
{
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        Yii::app()->db->createCommand(
"
UPDATE employees e 
SET payout_identification_value=(SELECT \"JMBG\" FROM persons p WHERE p.id=e.id) 
WHERE payout_identification_type=1 AND EXISTS (SELECT 1 FROM persons p WHERE p.id=e.id AND p.\"JMBG\" IS NOT NULL) 
    AND payout_identification_value!=(SELECT \"JMBG\" FROM persons p WHERE p.id=e.id);
"
        )->execute();
    }

    public function safeDown()
    {
//        Yii::app()->db->createCommand(
//"
//
//"
//        )->execute();
        echo "m0000000010_00100_fix_emp_payout_identification_value does not support migration down.\n";
        return false;
    }
}