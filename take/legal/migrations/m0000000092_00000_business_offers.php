<?php

class m0000000092_00000_business_offers extends CDbMigration
{
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        Yii::app()->db->createCommand(
<<<'SIMAMIGRATESQL'
            CREATE TABLE legal.business_offers
            (
                id serial NOT NULL,
                offer_number text NOT NULL,
                name text NOT NULL,
                partner_id integer NOT NULL,
                status text NOT NULL DEFAULT 'PREPARATION'::text,
                currency_id integer NOT NULL,
                created_by_user_id integer NOT NULL,
                created_timestamp timestamp without time zone NOT NULL DEFAULT now(),
                theme_id integer,
                business_offer_variation_id integer,
                description text,
                CONSTRAINT business_offers_pkey PRIMARY KEY (id),
                CONSTRAINT business_offers_partner_id_name_key UNIQUE (partner_id, offer_number),
                CONSTRAINT business_offers_status_check CHECK (status = ANY (ARRAY['PREPARATION'::text, 'SENT'::text, 'ACCEPTED'::text, 'NOT_ACCEPTED'::text])),
                CONSTRAINT business_offers_partner_id_fkey FOREIGN KEY (partner_id)
                  REFERENCES public.partners (id) MATCH SIMPLE
                  ON UPDATE CASCADE ON DELETE RESTRICT,
                CONSTRAINT business_offers_currency_id_fkey FOREIGN KEY (currency_id)
                  REFERENCES base.currencies (id) MATCH SIMPLE
                  ON UPDATE CASCADE ON DELETE RESTRICT,
                CONSTRAINT business_offers_created_by_user_id_fkey FOREIGN KEY (created_by_user_id)
                  REFERENCES admin.users (id) MATCH SIMPLE
                  ON UPDATE CASCADE ON DELETE RESTRICT,
                CONSTRAINT business_offers_theme_id_fkey FOREIGN KEY (theme_id)
                  REFERENCES base.themes (id) MATCH SIMPLE
                  ON UPDATE CASCADE ON DELETE RESTRICT
            );
            CREATE TRIGGER i_am_called_by_everyone
              AFTER INSERT OR UPDATE OR DELETE
              ON legal.business_offers
              FOR EACH ROW
              EXECUTE PROCEDURE public.i_am_called_by_everyone();

            CREATE TABLE legal.business_offer_variations
            (
                id integer NOT NULL,
                name text NOT NULL,
                order_number integer NOT NULL,
                business_offer_id integer NOT NULL,
                value numeric(20,2) NOT NULL DEFAULT 0,
                value_with_discount numeric(20,2) NOT NULL DEFAULT 0,
                discount_percentage numeric(5,2) NOT NULL DEFAULT 0,
                discount_value numeric(20,2) NOT NULL DEFAULT 0,
                confirmed boolean NOT NULL DEFAULT false,
                confirmed_by_user_id integer,
                confirmed_timestamp timestamp without time zone,
                sent boolean NOT NULL DEFAULT false,
                sent_by_user_id integer,
                sent_timestamp timestamp without time zone,
                created_by_user_id integer NOT NULL,
                created_timestamp timestamp without time zone NOT NULL DEFAULT now(),
                description text,
                footer text,
                advance_percentage numeric(5,2) NOT NULL DEFAULT 0,
                advance_value numeric(20,2) NOT NULL DEFAULT 0,
                CONSTRAINT business_offer_variations_pkey PRIMARY KEY (id),
                CONSTRAINT business_offer_variations_business_offer_id_name_key UNIQUE (business_offer_id, order_number),
                CONSTRAINT business_offer_variations_id_fkey FOREIGN KEY (id)
                  REFERENCES files.files (id) MATCH SIMPLE
                  ON UPDATE CASCADE ON DELETE RESTRICT,
                CONSTRAINT business_offer_variations_business_offer_id_fkey FOREIGN KEY (business_offer_id)
                  REFERENCES legal.business_offers (id) MATCH SIMPLE
                  ON UPDATE CASCADE ON DELETE RESTRICT,
                CONSTRAINT business_offer_variations_confirmed_by_user_id_fkey FOREIGN KEY (confirmed_by_user_id)
                  REFERENCES admin.users (id) MATCH SIMPLE
                  ON UPDATE CASCADE ON DELETE RESTRICT,
                CONSTRAINT business_offer_variations_sent_by_user_id_fkey FOREIGN KEY (sent_by_user_id)
                  REFERENCES admin.users (id) MATCH SIMPLE
                  ON UPDATE CASCADE ON DELETE RESTRICT,
                CONSTRAINT business_offer_variations_created_by_user_id_fkey FOREIGN KEY (created_by_user_id)
                  REFERENCES admin.users (id) MATCH SIMPLE
                  ON UPDATE CASCADE ON DELETE RESTRICT
            );
            CREATE TRIGGER i_am_called_by_everyone
              AFTER INSERT OR UPDATE OR DELETE
              ON legal.business_offer_variations
              FOR EACH ROW
              EXECUTE PROCEDURE public.i_am_called_by_everyone();
              
            ALTER TABLE legal.business_offers 
                ADD CONSTRAINT business_offers_business_offer_variation_id_fkey FOREIGN KEY (business_offer_variation_id)
                  REFERENCES legal.business_offer_variations (id) MATCH SIMPLE
                  ON UPDATE CASCADE ON DELETE RESTRICT;
                  
            CREATE TABLE legal.business_offer_variation_items
            (
                id serial NOT NULL,
                order_number integer NOT NULL,
                order_full_path text NOT NULL,
                business_offer_variation_id integer NOT NULL,
                value_per_unit numeric(20,10) NOT NULL DEFAULT 0,
                quantity numeric(20,2) NOT NULL DEFAULT 0,
                value numeric(20,2) NOT NULL DEFAULT 0,
                discount_percentage numeric(5,2) NOT NULL DEFAULT 0,
                discount_value numeric(20,2) NOT NULL DEFAULT 0,
                parent_id integer,
                description text,
                short_description text,
                CONSTRAINT business_offer_variation_items_pkey PRIMARY KEY (id),
                CONSTRAINT business_offer_variation_items_business_offer_variation_id_order_number_key UNIQUE (business_offer_variation_id, parent_id, order_number),
                CONSTRAINT business_offer_variation_items_business_offer_variation_id_fkey FOREIGN KEY (business_offer_variation_id)
                  REFERENCES legal.business_offer_variations (id) MATCH SIMPLE
                  ON UPDATE CASCADE ON DELETE RESTRICT,
                CONSTRAINT business_offer_variation_items_parent_id_fkey FOREIGN KEY (parent_id)
                  REFERENCES legal.business_offer_variation_items (id) MATCH SIMPLE
                  ON UPDATE CASCADE ON DELETE RESTRICT
            );
            CREATE TRIGGER i_am_called_by_everyone
              AFTER INSERT OR UPDATE OR DELETE
              ON legal.business_offer_variation_items
              FOR EACH ROW
              EXECUTE PROCEDURE public.i_am_called_by_everyone();
              
            CREATE TABLE legal.business_offer_variation_predefined_items
            (
                id serial NOT NULL,
                name text NOT NULL,
                description text NOT NULL,
                CONSTRAINT business_offer_variation_predefined_items_pkey PRIMARY KEY (id),
                CONSTRAINT business_offer_variation_predefined_items_description_key UNIQUE (name)
            );
            CREATE TRIGGER i_am_called_by_everyone
              AFTER INSERT OR UPDATE OR DELETE
              ON legal.business_offer_variation_predefined_items
              FOR EACH ROW
              EXECUTE PROCEDURE public.i_am_called_by_everyone();
SIMAMIGRATESQL
        )->execute();
    }

    public function safeDown()
    {
//        Yii::app()->db->createCommand(
//<<<'SIMAMIGRATESQL'
//
//SIMAMIGRATESQL
//        )->execute();
        echo "m0000000092_00000_business_offers does not support migration down.\n";
        return false;
    }
}