<?php

class m0000000011_00038_removejob_id_and_bid_id_from_contract extends CDbMigration
{
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        Yii::app()->db->createCommand(
"
ALTER TABLE private.contracts
  DROP COLUMN job_id;
ALTER TABLE private.contracts
  DROP COLUMN bid_id;
ALTER TABLE private.contracts
  DROP COLUMN file_id;

"
        )->execute();
    }

    public function safeDown()
    {
//        Yii::app()->db->createCommand(
//"
//
//"
//        )->execute();
        echo "m0000000011_00038_removejob_id_and_bid_id_from_contract does not support migration down.\n";
        return false;
    }
}