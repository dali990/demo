<?php

class m0000000055_00005_bid_fixes extends CDbMigration
{
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        Yii::app()->db->createCommand(
<<<'SIMAMIGRATESQL'
        ALTER TABLE private.bids
            ADD COLUMN refer_id integer,
            ADD COLUMN text_of_bid text,
            ADD COLUMN base_of_bid text,
            ADD COLUMN compose_id integer,
            ADD COLUMN limit_execution_work text,
            ADD COLUMN number_of_bid text,
            ADD CONSTRAINT bids_refer_id_fkey FOREIGN KEY (refer_id)
                REFERENCES public.persons (id) MATCH SIMPLE
                ON UPDATE CASCADE ON DELETE RESTRICT,
            ADD CONSTRAINT bids_compose_id_fkey FOREIGN KEY (compose_id)
                REFERENCES public.persons (id) MATCH SIMPLE
                ON UPDATE CASCADE ON DELETE RESTRICT;          
SIMAMIGRATESQL
        )->execute();
    }
    public function safeDown()
    {
//        Yii::app()->db->createCommand(
//<<<'SIMAMIGRATESQL'
//
//SIMAMIGRATESQL
//        )->execute();
        echo "m0000000055_00005_bid_fixes does not support migration down.\n";
        return false;
    }
}