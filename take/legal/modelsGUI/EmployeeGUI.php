<?php

class EmployeeGUI extends SIMAActiveRecordGUI {

    public function columnLabels() {
        return array(
            'firstname' => Yii::t('LegalModule.Employee','FirstName'),
            'lastname' => Yii::t('LegalModule.Employee','Lastname'),
            'JMBG' => Yii::t('LegalModule.Employee','JMBG'),
            'degree' => Yii::t('LegalModule.Employee','Degree'),
            'location_id' => Yii::t('LegalModule.Employee','Location'),
            'company' => Yii::t('LegalModule.Employee','Company'),
            'municipality_id' => Yii::t('LegalModule.Employee','Municipality'),
            'working_until' => Yii::t('LegalModule.Employee','WorkingUntil'),
            'under_contract' => Yii::t('LegalModule.Employee','UnderContract'),
            'employee_order' => Yii::t('LegalModule.Employee','EmployeeOrder'),
            'year_experience' => Yii::t('LegalModule.Employee','YearExperience'),
            'DisplayName' => Yii::t('LegalModule.Employee','DisplayName'),
            'display_name' => Yii::t('LegalModule.Employee','DisplayName'),
            'order_in_company' => Yii::t('LegalModule.Employee','OrderInCompany'),
            'blood_group' => Yii::t('LegalModule.Employee','BloodGroup'),
            'driver_license_categories_list' => Yii::t('LegalModule.Employee','DriverLicenseCategory'),            
            'driver_license_expire' => Yii::t('LegalModule.Employee','DriverLicenseExpire'),
            'driver_license_issued_by' => Yii::t('LegalModule.Employee','DriverLicenseIssuedBy'),
            'driver_license_number' => Yii::t('LegalModule.Employee','DriverLicenseNumber'),
            'id_card_number' => Yii::t('LegalModule.Employee','IdCardNumber'),
            'passport_number' => Yii::t('LegalModule.Employee','PassportNumber'),
            'employment_code' => Yii::t('LegalModule.Employee','EmploymentCode'),
            'active_work_contract.start_date' => Yii::t('LegalModule.Employee','StartDate'),
            'active_work_contract.end_date' => Yii::t('LegalModule.Employee','EndDate'),
            'active_work_contract.work_position' => Yii::t('LegalModule.Employee','WorkPosition'),
            'reason_for_not_ok' => Yii::t('PaychecksModule.PaychecksEmployee','ReasonForNotOk'),
            'gender' => Yii::t('LegalModule.Employee','Gender'),
            'expiration_date_of_insurance' => Yii::t('LegalModule.Employee','ExpirationDateOfInsurance'),
            'paycheck_svp' => Yii::t('LegalModule.Employee','SVP'),
            'profession_id' => Yii::t('LegalModule.Employee','Profession'),
            'profession' => Yii::t('LegalModule.Employee','Profession'),
            'insufficient_data' => Yii::t('LegalModule.Employee','InsufficientData'),
            'active_contracts' => Yii::t('LegalModule.Employee','ActiveContracts'),
            'payout_type' => Yii::t('LegalModule.Employee','PayoutType'),
            'last_work_date' => Yii::t('LegalModule.Employee','LastWorkDate'),
            'personal_points' => Yii::t('LegalModule.Employee','PersonalPoints'),
            'age' => Yii::t('LegalModule.Employee','Age'),
            'have_slava' => Yii::t('HRModule.Absence', 'HaveSlava'),
            'national_event_slava_id' => Yii::t('HRModule.Absence', 'Slava'),
            'WorkExperience' => 'Radno iskustvo u firmi',
            'document_send_type' => 'Način slanja dokumentacije',
            'email_account' => 'Email nalog',
            'email_account_id' => 'Email nalog',
            'unseen_confirmed_days' => Yii::t('LegalModule.Employee','UnseenConfirmedDays'),
            'payout_identification_type' => Yii::t('LegalModule.Employee','PayoutIdentificationType'),
            'payout_identification_value' => Yii::t('LegalModule.Employee','PayoutIdentificationValue'),
            'curr_paygrade' => Yii::t('LegalModule.Employee','CurrPaygrade'),
            'old_work_exp' => Yii::t('LegalModule.Employee','OldWorkExp')
        ) + parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return Yii::t('LegalModule.Employee','Employee');
    }


    public function columnDisplays($column) 
    {
        $owner = $this->owner;
        switch ($column) {
            case 'working_until':           return (isset($owner->last_work_contract))?$owner->last_work_contract->getAttributeDisplay('end_date'):'nema ugovora';
            case 'firstname':               return $owner->person->firstname;
            case 'lastname':                return $owner->person->lastname;
            case 'JMBG':                    return $owner->person->JMBG;
            case 'professional_degree_id':  return $owner->person->professional_degree;
            case 'company':                 return $owner->person->company;
            case 'under_contract':          return (isset($owner->active_work_contract)?'radi':'ne radi');
            case 'active_work_contract':    if (!isset($owner->active_work_contract)) return 'nema aktivan ugovor';
            case 'WorkExperience':          return WorkContractComponent::formatWorkExp($owner->WorkExperience);
            case 'payout_type': return Employee::$payout_types[$owner->payout_type];
            case 'reason_for_not_ok':
                $result = Yii::t('BaseModule.Common', 'AllOk');

                $months = Month::previousNmonths(12);

                $problem_messages = PaychecksEmployee::getReasonsForNotOkForPaycheckRecalc($owner, $months);

                if(count($problem_messages) > 0)
                {
                    $result = implode('</br>', $problem_messages);
                }
                return $result;
            case 'insufficient_data':
                $result = '';
                $problem_messages = EmployeeComponent::employeeInsufficientDataWithQualificationLevelCheck($owner, true);
                if(count($problem_messages) > 0)
                {
                    $result = implode('</br>', $problem_messages);
                }
                else
                {
                    $result = Yii::t('LegalModule.Employee', 'NoInsufficientData');
                }
                return $result;
            case 'gender': 
                $result = '';
                
                if ($owner->gender === 'male')
                    $result = 'muški';
                else if ($owner->gender === 'female')
                    $result = 'ženski';
                
                return $result;
            case 'active_contracts':
                $result = '';
                $work_contracts = $owner->work_contracts;
                $active_wc_id = null;
                $active_wcs = $owner->active_work_contract;
                if(!is_null($active_wcs))
                {
                    $active_wc_id = $active_wcs->id;
                }
                
                if(count($work_contracts) > 0)
                {
                    usort($work_contracts, function($a, $b)
                    {
                        $dateA = new DateTime($a->start_date);
                        $dateB = new DateTime($b->start_date);
                        return $dateA >= $dateB;
                    });

                    $work_contracts_dates = array();
                    foreach($work_contracts as $wc)
                    {
                        $close_wc = $wc->close_work_contract;
                        if(isset($close_wc))
                        {
                            $end_date = $close_wc->end_date;
                        }
                        else
                        {
                            $end_date = $wc->end_date;
                        }
                        if(!isset($end_date))
                        {
                            $end_date = 'neodredjeno';
                        }
                        
                        $date_string = $wc->start_date.' - '.$end_date;
                        
                        if($active_wc_id === $wc->id)
                        {
                            $date_string = '<b>'.$wc->start_date.' - '.$end_date.'</b>';
                        }
                        
                        $work_contracts_dates[] = $date_string;
                    }

                    $result = implode('</br>', $work_contracts_dates);
                }
                
                return $result;
            case 'last_work_date':
                return isset($owner->last_work_contract)?$owner->last_work_contract->getAttributeDisplay('end_date'):'??????';
//            case 'personal_points':
//                $form = SIMAHtml::modelFormOpen($owner, ['formName'=>'paychecks_view']);
//                $result = $owner->personal_points.' '.$form;
//                return $result;
            case 'document_send_type':
                return Employee::$document_send_types[$owner->$column];
            case 'unseen_confirmed_days':
                return $owner->getConfirmedUnseenDaysCount();
            case 'curr_paygrade':
                $return = '';
                if (isset($owner->active_work_contract->work_position->paygrade))
                {
                    $return = $owner->active_work_contract->work_position->paygrade->DisplayName;
                }
                return $return;
            default: return parent::columnDisplays($column);
        }
    }

    public function modelViews() {
        return array(            
            'config_emails',
            'active_annual_leave_decision'=>array('params_function'=>'paramsRemainingAbsence'),
            'left_top_info'=>array('params_function'=>'leftTopInfoParams')
        );
    }

    public function modelForms() {
        $municipality_field = ['relation','relName'=>'municipality'];
        if(Yii::app()->user->checkAccess('menuShowLegalConfig'))
        {
            $municipality_field['add_button'] = true;
        }
        
        $general_form_columns = [
            'order_in_company'=>'textField',
            'employment_code'=>array('dropdown', 'empty'=>'Izaberi'),
            'old_work_exp'=>'numberField',
            'municipality_id'=>$municipality_field,
            'expiration_date_of_insurance'=>'datetimeField',
            'id_card_number'=>'textField',
            'passport_number'=>'textField',
            'driver_license_number'=>'textField',
            'driver_license_issued_by'=>'textField',
            'driver_license_expire'=>'datetimeField',
            'driver_license_categories_list'=>array('list',
                'model'=>'DriverLicenseCategory',
                'relName'=>'driver_license_categories', 
            ),                    
            'blood_group'=>array('dropdown', 'empty'=>'Izaberi'),
            'gender'=>array('dropdown', 'empty'=>'Izaberi'),
            'profession_id'=>['relation','relName'=>'profession','add_button'=>true],            
            'document_send_type'=>'dropdown',
            'email_account_id'=>['relation','relName'=>'email_account','add_button'=>true],
            //MilosS(25.4.2019): trenutno zakoemnatrisano, ne koristi se nigde 
//            'paycheck_bonus_paygrade_id' => ['relation', 'relName' => 'paycheck_bonus_paygrade']
        ];
        
        $national_event_slava_id1 = 'hidden';
        if($this->owner->haveRelation('slava'))
        {
            $have_slava = ['dropdown'];
            $national_event_slava_id = $national_event_slava_id1 = [
                'relation', 'relName'=>'slava', 'filters'=>['type' => NationalEvent::$TYPE_SLAVA], 
                'add_button'=>[
                    'params'=>[
                        'add_button' => [
                            'init_data' => [
                                'NationalEvent' => [
                                    'type' => [
                                        'disabled',
                                        'init' => NationalEvent::$TYPE_SLAVA
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ];
            $national_event_slava_id['dependent_on'] = [
                'have_slava' => [
                    'onValue'=>[
                        ['enable', 'for_values' => '1'],
                        ['disable', 'for_values' => '0'],
                    ],
                ]
            ];
            if (!$this->owner->canLoggedInUserEditSlavaForThisEmp())
            {
                $have_slava['disabled'] = 'disabled';
                $national_event_slava_id['disabled'] = 'disabled';
                $national_event_slava_id1['disabled'] = 'disabled';
            }
            $general_form_columns['have_slava'] = $have_slava;
            $general_form_columns['national_event_slava_id'] = $national_event_slava_id;
        }
        
        $default_forms = ['general'=>['title' => 'Opšte']];
        if (Yii::app()->user->checkAccess('menuShowAccounts_paychecks'))
        {
            $default_forms['financial'] = ['title' => 'Finansije'];
        }
        
        return array(
            'default' => [
                'type' => 'composite',
                'forms' => $default_forms
            ],
            'financial' => [
                'type' => 'generic',                
                'columns' => [
                    'paycheck_svp'=>'textField',
                    'payout_identification_type' => ['dropdown'],
                    'payout_identification_value' => [
                        'textField', 
                        'dependent_on' => [
                            'payout_identification_type' => [
                                'onValue'=>[
                                    ['hide', 'for_values' => [(string)Employee::$PAYOUT_IDENTIFICATION_TYPE_JMBG]],
                                ],
                            ]
                        ]
                    ],
                    'payout_type'=>'dropdown',
                    'personal_points'=>'textField'
                ]
            ],
            'general' => array(
                'type' => 'generic',
                'columns'=>$general_form_columns
            ),
            'paycheck' => array(
                'type' => 'generic',
                'columns'=>array(
                    'old_work_exp'=>'numberField',
                    'municipality_id'=>$municipality_field
                )
            ),
            'paychecks_view' => array(
                'type' => 'generic',
                'columns'=>array(
                    'display_name' => ['textField', 'disabled'=>'disabled'],
                    'personal_points'=>'textField',
                )
            ),
            'only_slava' => array(
                'type' => 'generic',
                'columns'=>array(
                    'display_name' => ['textField', 'disabled'=>'disabled'],
                    'national_event_slava_id' => $national_event_slava_id1
                )
            )
        );
    }
    
    public function droplist($key, $relName='', $filter=null) {
        switch ($key) {
            case 'under_contract': return array('false' => 'Ne radi', 'true' => 'Radi');
            case 'blood_group': return array('A+' => 'A+', 'A-' => 'A-', 'B+' => 'B+', 'B-' => 'B-', 'AB+' => 'AB+', 'AB-' => 'AB-', '0+' => '0+'
                . '', '0-' => '0-');
            case 'employment_code': return WorkContract::$employment_codes;
            case 'gender': return array('male' => 'Muški', 'female' => 'Ženski');
            case 'payout_type': return Employee::$payout_types;
            case 'have_slava': return [
                '1' => Yii::t('BaseModule.Common', 'Yes'),
                '0' => Yii::t('BaseModule.Common', 'No')
            ];
            case 'document_send_type': return Employee::$document_send_types;
            case 'payout_identification_type': return [
                Employee::$PAYOUT_IDENTIFICATION_TYPE_JMBG => Yii::t('LegalModule.Employee', 'PAYOUT_IDENTIFICATION_TYPE_JMBG'),
                Employee::$PAYOUT_IDENTIFICATION_TYPE_REFUGEE_CARD => Yii::t('LegalModule.Employee', 'PAYOUT_IDENTIFICATION_TYPE_REFUGEE_CARD'),
                Employee::$PAYOUT_IDENTIFICATION_TYPE_PASSPORT => Yii::t('LegalModule.Employee', 'PAYOUT_IDENTIFICATION_TYPE_PASSPORT'),
                Employee::$PAYOUT_IDENTIFICATION_TYPE_EXTERNAL_ID => Yii::t('LegalModule.Employee', 'PAYOUT_IDENTIFICATION_TYPE_EXTERNAL_ID'),
                Employee::$PAYOUT_IDENTIFICATION_TYPE_OTHER => Yii::t('LegalModule.Employee', 'PAYOUT_IDENTIFICATION_TYPE_OTHER'),
            ];
            default: return parent::droplist($key, $relName, $filter);
        }
    }  
    
     public function paramsRemainingAbsence()
    {
        $model = $this->owner;
        
        $active_annual_leave_decisions = $model->getActiveAnnualLeaveDecisions();
        
        return array(
            'model' => $model,
            'active_annual_leave_decisions' => $active_annual_leave_decisions,            
        );
        
        
    }
    // here is custom filter
    public function leftTopInfoParams()
    {
        $model = $this->owner;
        $criteria = new SIMADbCriteria([
            'Model' => Absence::model(),
            'model_filter'=>[
                'order_by'=>'begin DESC',
                'limit'=>5,
                'employee' => [
                    'ids' => $model->id
                ]
            ]
        ]);
        $absences = Absence::model()->findAll($criteria);
        return array(
            'model' => $model,
            'absences' => $absences,            
        );       
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            case 'inFile': case 'persons': 
                return array(
                    'columns' => array('order_in_company','display_name','person.firstname', 'person.lastname','person.comment')
                );
            case 'legal':
                return array(
                    'columns' => [
                        'order_in_company', 
                        'person.firstname', 
                        'person.lastname', 
                        'last_work_contract.start_date', 
                        'last_work_date', 
                        'under_contract',
                        'active_work_contract.work_position',
                        'expiration_date_of_insurance',
                        'person.JMBG',
                        'person.birthdate',
                        'person.partner.main_address_display',
                        'age',
                        'gender',
                        'profession',
                        'insufficient_data',
                        'active_contracts',
                        'document_send_type',
                        'email_account'
                    ],
                    'sums' => [
                        'age'
                    ],
                    'sums_function' => 'sumsLegal'
                );
            case 'paycheck':
                return array(
                    'columns' => array('person.firstname', 'person.lastname', 'working_until', 'under_contract')
                );
            case 'file_transfers':
                return array(
                    'columns' => array('person.firstname','person.lastname')
                );
            case 'in_task':
                return array(
                    'columns' => array('person.firstname', 'person.lastname')
                );
            case 'in_finances':
                return array(
                    'columns' => array('display_name', 'paycheck_svp', 'personal_points', 'curr_paygrade')
                );
            case 'old_paychecks':
                return array(
                    'columns' => array('display_name', 'reason_for_not_ok')
                );
            case 'insufficient_data':
                return array(
                    'columns' => array('display_name', 'insufficient_data'),
                );
            case 'paychecks_view':
                return array(
                    'columns' => array('display_name', 'personal_points'),
                );
            case 'only_slava':
                return array(
                    'columns' => array('display_name', 'slava'),
                );
            case 'with_unseen_confirmed_days':
                return array(
                    'columns' => array('display_name', 'unseen_confirmed_days'),
                );
            default:
                return array(
                    'columns' => array('person.firstname', 'person.lastname')
                );
        }
    }
    
    public function sumsLegal($criteria)
    {
        
        $emps = Employee::model()->findAll($criteria);
        $sum = 0;
        $cnt = 0;
        foreach ($emps as $_emp)
        {
            if (!empty($_emp))
            {
                $sum += $_emp->age;
                $cnt++;
            }
        }
        $emp = new Employee();
        if ($cnt>0)
        {
            $emp->age = $sum/$cnt;
        }
        else
        {
            $emp->age = 0;
        }
                
        return $emp;
    }
    
}