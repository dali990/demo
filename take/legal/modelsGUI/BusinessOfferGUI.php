<?php

class BusinessOfferGUI extends SIMAActiveRecordDisplayGUI
{   
    public function columnLabels()
    {
        return [
            'name' => Yii::t('LegalModule.BusinessOffer', 'Name'),
            'status' => Yii::t('LegalModule.BusinessOffer', 'Status'),
            'partner_id' => Yii::t('LegalModule.BusinessOffer', 'Partner'),
            'partner' => Yii::t('LegalModule.BusinessOffer', 'Partner'),
            'currency_id' => Yii::t('LegalModule.BusinessOffer', 'Currency'),
            'currency' => Yii::t('LegalModule.BusinessOffer', 'Currency'),
            'created_by_user_id' => Yii::t('LegalModule.BusinessOffer', 'CreatedByUser'),
            'created_by_user' => Yii::t('LegalModule.BusinessOffer', 'CreatedByUser'),
            'created_timestamp' => Yii::t('LegalModule.BusinessOffer', 'CreatedTimestamp'),
            'theme_id' => Yii::t('LegalModule.BusinessOffer', 'Theme'),
            'theme' => Yii::t('LegalModule.BusinessOffer', 'Theme'),
            'business_offer_variation_id' => Yii::t('LegalModule.BusinessOfferVariation', 'BusinessOfferVariation'),
            'business_offer_variation' => Yii::t('LegalModule.BusinessOfferVariation', 'BusinessOfferVariation'),
            'description' => Yii::t('LegalModule.BusinessOffer', 'Description'),
            'value' => Yii::t('LegalModule.BusinessOffer', 'Value'),
            'offer_number' => Yii::t('LegalModule.BusinessOffer', 'OfferNumber')
        ] + parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return Yii::t('LegalModule.BusinessOffer', $plural ? 'BusinessOffers': 'BusinessOffer');
    }
    
    public function columnDisplays($column)
    {
        $owner = $this->owner;

        switch ($column)
        {
            case 'status': return BusinessOffer::$statuses[$owner->status];
            case 'value': 
                $return = SIMAHtml::number_format(0, 2);
                if (!empty($owner->business_offer_variation_id))
                {
                    $return = $owner->business_offer_variation->getAttributeDisplay('value');
                }

                return $return;
            default: return parent::columnDisplays($column);
        }
    }
    
    public function modelViews()
    {
        return [
           'basic_info', 'full_info', 'full_info_inner', 'variations'
        ] + parent::modelViews();
    }
    
    public function modelForms()
    {
        $owner = $this->owner;

        return [
            'default' => [
                'type' => 'generic',
                'columns' => [
                    'offer_number' => 'textField',
                    'name' => 'textField',
                    'partner_id' => ['relation', 'relName' => 'partner', 'add_button' => true],
                    'status' => 'dropdown',
                    'currency_id' => ['relation', 'relName' => 'currency', 'add_button' => true],
                    'description' => 'textArea',
                    'theme_id' => ['relation', 'relName' => 'theme', 'add_button' => true]
                ]
            ]
        ];
    }
    
    public function droplist($key, $relName='', $filter=null)
    {
        switch ($key)
        {
            case 'status': return BusinessOffer::$statuses;

            default: return parent::droplist($key, $relName, $filter);
        }
    }

    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default:
                return [
                    'columns' => ['offer_number', 'name', 'partner', 'status', 'value', 'currency', 'created_by_user', 'created_timestamp', 'description', 'theme']
                ];
        }
    }
}
