<?php

class BidRequirementGUI extends SIMAActiveRecordGUI
{

    public function columnLabels()
    {
        return array(
            'name'=>'Naziv',
            'potential_bid_id'=>'Potencijalna ponuda',
            'potential_bid'=>'Potencijalna ponuda',
            'bid_requirement_type_id'=>'Tip uslova ponude',
            'bid_requirement_type'=>'Tip uslova ponude',
            'description'=>'Napomena'
        ) + parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return 'Dodatni uslov ponude';
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            case 'from_potential_bids': return [
                'columns' => array(
                    'name'=>['edit'=>'text'],
                    'bid_requirement_type'=>['edit'=>'relation'],
                    'description'=>['edit'=>'text']
                )
            ];
            default: return array(
                'columns' => array(
                    'name'=>['edit'=>'text'],
                    'potential_bid'=>['edit'=>'relation'],
                    'bid_requirement_type'=>['edit'=>'relation'],
                    'description'=>['edit'=>'text']                    
                )
            );
        }
    }
    
    public function modelForms() 
    {        
        $owner = $this->owner;
        $potential_bid = ['relation', 'relName'=>'potential_bid'];
        if (isset($owner->potential_bid_id))
        {            
            $potential_bid = ['relation', 'relName'=>'potential_bid', 'disabled'=>true];
        }
        return array(
            'default'=>array(
                'type'=>'generic',
                'columns'=>array(
                    'name'=>'textField',
                    'potential_bid_id'=>$potential_bid,
                    'bid_requirement_type_id'=>['relation', 'relName'=>'bid_requirement_type', 'add_button'=>true],
                    'description'=>'textArea'
                )
            )
        );
    }        
}