<?php
/**
 * Description of ArchiveBoxGUI
 *
 * @author goran-set
 */
class ArchiveItemGUI extends SIMAActiveRecordGUI
{
    public function columnLabels()
    {
        return array(
            'description'=>'Opis',
            'name'=>'Naziv',
            'id'=>'Redni broj',
            'archive_box'=>'Kutija',
            'archive_box_id'=>'Kutija',
            'theme'=>'Tema',
            'theme_id'=>'Tema',
        )+parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return 'Arhivska stavka';
    }
    
    public function modelViews()
    {
        return array();
    }
    
    public function modelForms()
    {
        return array(
            'default'=>array(
                'type'=>'generic',
                'columns'=>array(
                    'name'=>'textField',
                    'description'=>'textArea',
                    'archive_box_id'=>array('relation', 'relName'=>'archive_box', 'add_button'=>true),
                    'theme_id'=>'hidden'
                )
            )
        );
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            case 'inTheme':
                return array(
                'columns' => array(
                    'name', 
                    'archive_box', 
                    'description', 
                ),
            );
            default:
                return array(
                'columns' => array(
                    'name', 
                    'archive_box', 
                    'theme',
                    'description', 
                ),
            );
        }
    }
    
}
