<?php

class BidGUI extends SIMAActiveRecordDisplayGUI
{

    public function columnLabels()
    {
        return array(
            'file_version_id' => 'Skenirano',
            'filing_id' => 'Zavodni broj',
            'filing' => 'Zavodni broj',
            'essential_elements' => 'Bitan element ponude',
            'delivering_time' => 'Vreme dostave',
            'opening_time' => 'Vreme otvaranje',            
            'add_opening_record' => 'Zapisnik o otvaranju',
            'opening_record_file_id' => 'Zapisnik o otvaranju',
            'add_decision_record' => 'Zapisnik o izboru',
            'decision_record_file_id' => 'Zapisnik o izboru',
            'archive' => 'Arhiva',
            'best_bid_value' => 'Najpovoljnija cena',
            'best_bid_partner_id' => 'Najpovoljniji ponuđač',
            'building_structure_id' => 'Objekat',
            'qualification_end' => 'Vazenje kvalifikacija',
            'coordinator_id' => 'Koordinator',
            'coordinator' => 'Koordinator',
            'add_document' => 'Dokument',
            'job' => 'Posao',
            'job_id' => 'Posao',
            'description' => 'Napomena',
            'bid_status'=>'Status ponude',
            'potential_bid_id'=>'Potencijalna ponuda',
            'potential_bid'=>'Potencijalna ponuda',
            'value_in_currency' => Yii::t('BaseModule.Common', 'Value'),
            'refer_id' => Yii::t('LegalModule.Bid', 'Refer'),
            'refer' => Yii::t('LegalModule.Bid', 'Refer'),
            'text_of_bid' => Yii::t('LegalModule.Bid', 'TextOfBid'),
            'base_of_bid' => Yii::t('LegalModule.Bid', 'BaseOfBid'),
            'compose_id' => Yii::t('LegalModule.Bid', 'BidCompose'),
            'compose' => Yii::t('LegalModule.Bid', 'BidCompose'),
            'limit_execution_work' => Yii::t('LegalModule.Bid', 'LimitExecutionWork'),
            'number_of_bid' => Yii::t('LegalModule.Bid', 'NumberOfBid'),
            'is_enable_template' => Yii::t('LegalModule.Bid', 'EnableTemplate')
        ) + parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return Yii::t('LegalModule.Bid', $plural?'Bids':'Bid');
    }

    public function columnDisplays($column)
    {
        $owner = $this->owner;
        switch ($column)
        {
            
            case 'bid_status':return Bid::$bid_statuses[$owner->bid_status];
            case 'best_bid_partner_id': return (isset($owner->best_bid_partner)) ? $owner->best_bid_partner->DisplayName : '';
            case 'filing':
                return (isset($owner->file)) ?
                        Yii::app()->controller->renderModelView($owner->file, 'filingNumber') : 'Da biste zaveli, prvo dodajte dokument';
            case 'value_in_currency': 
            case 'value':
                    $column = 'value';
            case 'best_bid_value':
                return SIMAHtml::money_format($owner->$column, (isset($owner->currency)) ? $owner->currency->DisplayName : '');

            case 'opening_record_file_id':
                return (isset($owner->opening_record)) ? $owner->opening_record->DisplayName . SIMAHtml::fileDownload($owner->opening_record->id) : '';
            case 'decision_record_file_id':
                return (isset($owner->decision_record)) ? $owner->decision_record->DisplayName . SIMAHtml::fileDownload($owner->decision_record->id) : '';
            case 'building_structure_id':
                if (isset($owner->building_structure))
                {
                    return $owner->building_structure->name . '<br />' . $owner->building_structure->size . ' '
                            . ((isset($owner->building_structure->size_unit)) ? $owner->building_structure->size_unit->name : '');
                }
                else
                    return '---';
            case 'add_opening_record':
                $tag_query = $owner->gettag()->query;
                if (isset($owner->opening_record))
                {
                    return "<span class='sima-icon _search_form _16' onclick='sima.legal.addBidDocument(".$owner->id.",\"$tag_query\",\"opening_record_file_id\");'></span>"
                        .SIMAHtml::fileDownload($owner->opening_record).$owner->opening_record->DisplayHtml;
                }
                else
                {
                    return "<span class='sima-icon _search_form _16' onclick='sima.legal.addBidDocument(".$owner->id.",\"$tag_query\",\"opening_record_file_id\");'></span>";
                }
            case 'add_decision_record':
                $tag_query = $owner->gettag()->query;
                if (isset($owner->decision_record))
                {
                    return "<span class='sima-icon _search_form _16' onclick='sima.legal.addBidDocument(".$owner->id.",\"$tag_query\",\"decision_record_file_id\");'></span>"
                        .SIMAHtml::fileDownload($owner->decision_record).$owner->decision_record->DisplayHtml;
                }
                else
                {
                    return "<span class='sima-icon _search_form _16' onclick='sima.legal.addBidDocument(".$owner->id.",\"$tag_query\",\"decision_record_file_id\");'></span>";
                }
            case 'add_document':
                $tag = $owner->tag;
                $tag_query = $tag->query;    
                
                $document_type_id = Yii::app()->configManager->get('legal.bid', false);
                if (isset($owner->file))
                {
                    return 
                        (!$owner->is_enable_template ? "<span class='sima-icon _search_form _16' onclick='sima.legal.addBidDocument(".$owner->id.",\"$tag_query\",\"file_id\",\"$document_type_id\",".$tag->id.",\"$tag->name\");'></span>" : "")
                        .SIMAHtml::fileDownload($owner->file)
                        . (isset($owner->file->last_version) ? 
                            "<span class='sima-display-html'><span class='sima-model-link' onclick='sima.dialog.openModel(\"files/file\", " . $owner->file->id . ");'>" . $owner->file->last_version->DisplayName . "</span></span>" : 
                            $owner->file->DisplayHtml);
                }
                else
                {
                    return (!$owner->is_enable_template ? "<span class='sima-icon _search_form _16' onclick='sima.legal.addBidDocument(".$owner->id.",\"$tag_query\",\"file_id\",\"$document_type_id\",".$tag->id.",\"$tag->name\");'></span>" : "");
                }
            default: return parent::columnDisplays($column);
        }
    }
    
    public function modelViews()
    {
        return array_merge(parent::modelViews(), array(
            'indexTitle'=>array('origin'=>'Bid'),
            'info'=>array('origin'=>'Bid'),
            'inTheme',
            'options_in_theme' => ['html_wrapper' => null]
        ));
    }
    
//    public function modelViews()
//    {
//        return array(
////            'inFile' => array('style' => 'border: 1px solid;'),
////            'tabTemplates'=>array('with'=>array('templated_file'),'style'=>'border: 1px solid;')  //.creator
//        );
//    }
    
    public function modelForms() 
    {
        Yii::app()->errorReport->createAutoClientBafRequest('Otvorena forma ponude neodredjenog tipa',3066,'AutoBafReq Otvorena forma ponude neodredjenog tipa');
        $owner = $this->owner;
        
        if ($owner->isNewRecord)
        {
            $owner->theme->type = Theme::$BID;
        }
        
        return array(
            'default'=>array(
                'type'=>'generic',
                'columns'=>array(                    
//                    'coordinator_id' => array('searchField','relName'=>'coordinator'),
                    'partner_id' => array('searchField','relName'=>'partner', 'add_button' => ['action'=>'simaAddressbook/index']),
                    'subject' => 'textArea',
                    'description' => 'textArea',
                    'value'=>array('unitMeasureField', 'unit'=>'currency_id'),
//                    'building_structure_id'=>array('searchField','relName'=>'building_structure'),
                    'essential_elements' => 'textArea',
                    'delivering_time' => array('datetimeField', 'mode'=>'datetime'),
                    'opening_time' => array('datetimeField', 'mode'=>'datetime'),
                    'bid_status'=>'dropdown',
                    'archive'=>'textArea',
                    'best_bid_value' => 'textField',
                    'best_bid_partner_id' => array('searchField','relName'=>'best_bid_partner'),
                    'potential_bid_id'=>'hidden',
                    'refer_id' => ['searchField','relName' => 'refer', 'add_button' => ['action' => 'simaAddressbook/index']],
                    'text_of_bid' => 'textArea',
                    'base_of_bid' => 'textArea',
                    'compose_id' => ['searchField','relName' => 'compose', 'add_button' => ['action' => 'simaAddressbook/index']],
                    'limit_execution_work' => 'textField',
                    'number_of_bid' => 'textField'
                )
            )
        );
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default: return array(
                'columns' => array(
                    'theme.display_name',
                    'filing',
                    'subject',
                    'partner',
                    'theme.job_type',
                    'value',
//                    'building_structure_id',
                    'essential_elements','delivering_time','opening_time','bid_status',
                    'opening_record_file_id','decision_record_file_id',
                    'archive','best_bid_value','best_bid_partner_id'
                ),
                'order_by'=>['delivering_time', 'opening_time']
            );
        }
    }
    
    public function droplist($key, $relName='', $filter=null)
    {        
        switch ($key)
        {
            case 'bid_status':
                return Bid::$bid_statuses;
            default: return parent::droplist($key, $relName, $filter);
        }
    }
    
    public function getDisplayModel()
    {
        return $this->owner->theme;
    }
}