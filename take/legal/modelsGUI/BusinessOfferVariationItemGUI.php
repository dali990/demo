<?php

class BusinessOfferVariationItemGUI extends SIMAActiveRecordGUI
{   
    public function columnLabels()
    {
        return [
            'order_number' => Yii::t('LegalModule.BusinessOfferVariationItem', 'OrderNumber'),
            'business_offer_variation_id' => Yii::t('LegalModule.BusinessOfferVariation', 'BusinessOfferVariation'),
            'business_offer_variation' => Yii::t('LegalModule.BusinessOfferVariation', 'BusinessOfferVariation'),
            'parent_id' => Yii::t('LegalModule.BusinessOfferVariationItem', 'Parent'),
            'parent' => Yii::t('LegalModule.BusinessOfferVariationItem', 'Parent'),
            'value_per_unit' => Yii::t('LegalModule.BusinessOfferVariationItem', 'ValuePerUnit'),
            'quantity' => Yii::t('LegalModule.BusinessOfferVariationItem', 'Quantity'),
            'value' => Yii::t('LegalModule.BusinessOfferVariationItem', 'Value'),
            'discount_percentage' => Yii::t('LegalModule.BusinessOfferVariationItem', 'DiscountPercentage'),
            'discount_value' => Yii::t('LegalModule.BusinessOfferVariationItem', 'DiscountValue'),
            'description' => Yii::t('LegalModule.BusinessOfferVariationItem', 'Description'),
            'order_full_path' => Yii::t('LegalModule.BusinessOfferVariationItem', 'OrderFullPath'),
            'total_value_display' => Yii::t('LegalModule.BusinessOfferVariationItem', 'TotalValue'),
            'business_offer_variation_predefined_items' => Yii::t('LegalModule.BusinessOfferVariationItem', 'BusinessOfferVariationPredefinedItems'),
            'short_description' => Yii::t('LegalModule.BusinessOfferVariationItem', 'ShortDescription')
        ] + parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return Yii::t('LegalModule.BusinessOfferVariationItem', $plural ? 'BusinessOfferVariationItems': 'BusinessOfferVariationItem');
    }
    
    public function columnDisplays($column)
    {
        $owner = $this->owner;

        switch ($column)
        {
            case 'value_per_unit': return SIMAHtml::number_format($owner->value_per_unit, 2);
            case 'value':
                $return = SIMAHtml::number_format($owner->value_with_discount, 2);
                
                return $return;
            case 'total_value_display': 
                $return = '';
                if ($owner->children_count > 0)
                {
                    $return = SIMAHtml::number_format($owner->total_value_with_discount, 2);
                }
                
                return $return;
            default: return parent::columnDisplays($column);
        }
    }
    
    public function modelForms()
    {
        $owner = $this->owner;
        
        $parent_column_filter = ['display_scopes' => 'byFullPath'];
        if (!empty($owner->business_offer_variation_id))
        {
            $parent_column_filter['business_offer_variation'] = [
                'ids' => $owner->business_offer_variation_id
            ];
        }
        
        return [
            'default' => [
                'type' => 'generic',
                'columns' => [
                    'business_offer_variation_id' => 'hidden',
                    'business_offer_variation_predefined_items' => 'dropdown',
                    'parent_id' => ['relation', 'relName' => 'parent', 'filters' => $parent_column_filter],
                    'order_number' => $owner->isOrderNumberAutomatic() === true ? 'hidden' : 'textField',
                    'value_per_unit' => 'numberField',
                    'quantity' => 'numberField',
                    'discount_percentage' => 'numberField',
                    'discount_value' => 'numberField',
                    'short_description' => 'textField',
                    'description' => ['tinymce', 
                        'dependent_on' => [
                            'business_offer_variation_predefined_items' => [
                                'onValue' => [
                                    'trigger', 
                                    'func' => ['sima.legal.businessOfferVariationItemFillPredefinedDescription'],

                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ];
    }

    public function guiTableSettings($type)
    {
        switch ($type)
        {
            case 'from_business_offer_variation':
                return [
                    'columns' => ['order_full_path', 'description', 'short_description', 'value_per_unit', 'quantity', 'value', 'total_value_display', 'discount_percentage', 'discount_value', 'parent']
                ];
            default:
                return [
                    'columns' => ['order_full_path', 'description', 'short_description', 'value_per_unit', 'quantity', 'value', 'total_value_display', 'discount_percentage', 'discount_value', 'parent', 'business_offer_variation']
                ];
        }
    }
    
    public function droplist($key, $relName='', $filter=null)
    {
        switch ($key)
        {
            case 'business_offer_variation_predefined_items': 
                return [0 => Yii::t('LegalModule.BusinessOfferVariationItem', 'Choose')] + CHtml::listData(BusinessOfferVariationPredefinedItem::model()->findAll(), 'id', 'DisplayName');

            default: return parent::droplist($key, $relName, $filter);
        }
    }
}
