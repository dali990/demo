<?php

class EmployeeInsurancePersonGUI extends SIMAActiveRecordGUI
{

    public function columnLabels()
    {
        return array(
            'employee_id' => 'Osiguranik',
            'employee' => 'Osiguranik',
            'person_id' => 'Član osiguranja',
            'person' => 'Član osiguranja',
            'relationship_type_id'=>'Rodbinska veza',
            'relationship_type'=>'Rodbinska veza',
            'end_date'=>'Datum isteka',
            'description' => 'Opis'
        ) + parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return 'Član osiguranja';
    }
    
    public function modelForms()
    {
        $owner = $this->owner;
        $employee_id = array('relation', 'relName'=>'employee');
        if (isset($owner->employee))
        {
            $employee_id = array('relation', 'relName'=>'employee', 'disabled'=>'disabled');
        }
        $end_date = 'datetimeField';
        if (isset($owner->end_date))
        {
            $end_date = array('datetimeField', 'disabled'=>'disabled');
        }
        return array(
            'default' => array(
                'type' => 'generic',
                'columns' => array(
                    'employee_id'=>$employee_id,
                    'person_id'=>array('relation', 'relName'=>'person'),
                    'relationship_type_id'=>array('relation', 'relName'=>'relationship_type', 'add_button'=>true),
                    'end_date'=>$end_date,
                    'description' => 'textArea'
                )
            )
        );
    }

    public function guiTableSettings($type)
    {
        switch ($type)
        {
            case 'from_legal':
                return [
                    'columns' => [
                        'person', 'relationship_type', 'end_date', 'description'
                    ]
                ];
            default:
                return [
                    'columns' => [
                        'employee', 'person', 'relationship_type', 'end_date', 'description'
                    ]
                ];
        }
    }
}
