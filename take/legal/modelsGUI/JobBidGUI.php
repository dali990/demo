<?php

class JobBidGUI extends BidGUI
{

//    public function columnLabels()
//    {
//        return array(
//            
//        ) + parent::columnLabels();
//    }

    public function modelLabel($plural = false)
    {
        return Yii::t('LegalModule.Bid', $plural?'JobBids':'JobBid');
    }
    
    public function columnDisplays($column)
    {
        $owner = $this->owner;
        switch ($column)
        {
            default: return parent::columnDisplays($column);
        }
    }

    public function modelViews()
    {
        return array(
            'inFile'// => array('style' => 'border: 1px solid;'),
//            'tabTemplates'=>array('with'=>array('templated_file'),'style'=>'border: 1px solid;')  //.creator
        )+parent::modelViews();
    }

    public function modelForms() 
    {
        $owner = $this->owner;
        
        if ($owner->isNewRecord)
        {
            $owner->theme->type = Theme::$BID;
        }
        
        if (empty($owner->currency_id))
        {
            $owner->currency_id = Yii::app()->configManager->get('base.country_currency', false);
        }
        
        return array(
            'default'=>array(
                'type'=>'generic',
                'columns'=>array(
                    'partner_id' => array('searchField','relName'=>'partner', 'add_button' => ['action'=>'simaAddressbook/index']),
                    'subject' => 'textArea',
                    'description' => 'textArea',
                    'value'=>array('unitMeasureField', 'unit'=>'currency_id'),
//                    'building_structure_id'=>array('searchField','relName'=>'building_structure'),
                    'essential_elements' => 'textArea',
                    'delivering_time' => array('datetimeField', 'mode'=>'datetime'),
                    'opening_time' => array('datetimeField', 'mode'=>'datetime'),
                    'bid_status' => 'dropdown',
                    'archive'=>'textArea',
                    'best_bid_value' => 'textField',
                    'best_bid_partner_id' => array('searchField','relName'=>'best_bid_partner'),
                    'refer_id' => ['searchField','relName' => 'refer', 'add_button' => ['action' => 'simaAddressbook/index']],
                    'text_of_bid' => 'textArea',
                    'base_of_bid' => 'textArea',
                    'compose_id' => ['searchField','relName' => 'compose', 'add_button' => ['action' => 'simaAddressbook/index']],
                    'limit_execution_work' => 'textField',
                    'number_of_bid' => 'textField',
                    'is_enable_template' => 'checkBox',
                    'potential_bid_id'=>'hidden',
                )
            )
        );
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            case 'inFile': return [
                'columns' => [
                    'filing',
                    'partner',
                    'display_name',
                    'subject',
                    'value',
                ]
            ];

            default: return [
                'columns' => [
                    'filing',
                    'partner',
                    'display_name','subject',
                    'theme.job_type',
//                    'value',
                    'value_in_currency',
//                    'building_structure_id',
                    'essential_elements','delivering_time','opening_time',
                        'bid_status','opening_record_file_id','decision_record_file_id',
                    'archive','best_bid_value','best_bid_partner_id',
                ],
                'order_by' => ['value','display_name','delivering_time','opening_time'],
                'sums' => ['value_in_currency'],
                'sums_function' => 'sumFunc',
            ];
        }

    }
    
    public function sumFunc(SIMADbCriteria $criteria)
    {
        $alias = $criteria->alias;
        $_crit = clone $criteria;
        $_crit->select = "sum($alias.value) as value, $alias.currency_id as currency_id";
        $_crit->group = "$alias.currency_id";
        $_crit->order = '';
        $bids = JobBid::model()->findAll($_crit);
        $return_bid = new JobBid();
        if (count($bids) > 0)
        {
            $value_sum = [];
            foreach ($bids as $bid)
            {
                $value_sum[] = SIMAHtml::money_format($bid->value, $bid->currency->DisplayName);
            }
            $return_bid->value_in_currency = implode('<br />', $value_sum);
        }
        return $return_bid;
    }
    
}