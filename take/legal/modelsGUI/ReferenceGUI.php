<?php

class ReferenceGUI extends SIMAActiveRecordGUI
{
    public function columnLabels()
    {
        return array(
            'name' => 'Naziv',
            'description' => 'Opis',
            'year_id' => 'Godina',
            'year' => 'Godina',            
            'file'=>'Dokument',
            'companyLicenses'=>'Velike licence',
            'add_file'=>'Dodaj fajl',
            'add_company_license' => 'Velike licence',
            'add_work_license' => 'Male licence',
            'job_id' => 'Posao',
            'job' => 'Posao'            
        ) + parent::columnLabels();
    }

    public function modelLabel($plural = false)
    {
        return 'Referenca';
    }
    
    public function columnDisplays($column) 
    {
        $owner = $this->owner;
        switch ($column) {
            case 'add_file':
                if (isset($owner->file))
                {
                    return "<span class='sima-icon _search_form _16' onclick='sima.legal.addReferenceFile(".$owner->id.");'></span>"
                        .SIMAHtml::fileDownload($owner->file).$owner->file->DisplayHtml;
                }
                else
                {
                    return "<span class='sima-icon _search_form _16' onclick='sima.legal.addReferenceFile(".$owner->id.");'></span>";
                };
            case 'add_company_license':
                $company_licenses_to_references = CompanyLicenseToReference::model()->findAllByAttributes(array('reference_id'=>$owner->id));
                $company_licenses_ids = [];
                foreach ($company_licenses_to_references as $company_license_to_reference)
                {
                    $curr = [];
                    $curr['id'] = $company_license_to_reference->company_license_id;
                    $curr['display_name'] = $company_license_to_reference->company_license->company_license->DisplayName;
                    array_push($company_licenses_ids, $curr);
                }
                $company_licenses_ids_json = CJSON::encode($company_licenses_ids);
                return "<span class='sima-icon _search_form _16' onclick='sima.legal.addCompanyLicenseToReference(".$owner->id.",$company_licenses_ids_json);'></span>";                        
            case 'add_work_license':
                $references_to_person_licenses = PersonLicenseToReference::model()->findAllByAttributes(array('reference_id'=>$owner->id));
                $work_licenses_ids = [];
                foreach ($references_to_person_licenses as $reference_to_person_license)
                {
                    $curr = [];
                    $curr['id'] = $reference_to_person_license->person_license_id;
                    $curr['display_name'] = $reference_to_person_license->person_license->DisplayName;
                    array_push($work_licenses_ids, $curr);
                }
                $work_licenses_ids_json = CJSON::encode($work_licenses_ids);
                return "<span class='sima-icon _search_form _16' onclick='sima.legal.addWorkLicenseToReference(".$owner->id.",$work_licenses_ids_json);'></span>";
            default: return parent::columnDisplays($column);
        }
    }
    
    public function modelViews()
    {
        return array(
           'info','indexTitle'
        );
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            case 'person_references': return [
                'columns'=>array(
                    'name','job'
                )
            ];
            default: return array(
                'columns'=>array(
                    'name','year','file','job','description'
                )
            );
        }
    }
    
    public function modelForms() {
        return array(
            'default' => array(
                'type' => 'generic',
                'columns' => array(
                    'name' => 'textField',
                    'year_id'=>array('relation','relName'=>'year'),
                    'job_id'=>array('relation','relName'=>'job'),
                    'description' => 'textArea',
                )
            )
        );
    }

}

?>
