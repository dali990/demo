<?php

class QualificationBidGUI extends BidGUI
{

    public function columnLabels()
    {
        return array(
            'qualification_end' => 'Trajanje kvalifikacija'
        ) + parent::columnLabels();
    }

    public function modelLabel($plural = false)
    {
        return Yii::t('LegalModule.Bid', $plural?'QualificationBids':'QualificationBid');
    }
    
    public function columnDisplays($column)
    {
        $owner = $this->owner;
        switch ($column)
        {
            case 'value': return money_format('%!i', $owner->value) . ((isset($owner->currency)) ? ' ' . $owner->currency->DisplayName : '');
//            case 'job_type_id': return (isset($owner->job_type)) ? $owner->job_type->DisplayName : 'Projekat';            
            case 'best_bid_partner_id': return (isset($owner->best_bid_partner)) ? $owner->best_bid_partner->DisplayName : '';
            case 'filing_id':
                return (isset($owner->file)) ?
                        Yii::app()->controller->renderModelView($owner->file, 'filingNumber') : 'Da biste zaveli, prvo sacuvajte ponudu';
            case 'value': case 'best_bid_value':
                return money_format('%!i', $owner->$column);

            case 'opening_record_file_id':
                return (isset($owner->opening_record)) ? $owner->opening_record->DisplayName . SIMAHtml::fileDownload($owner->opening_record->id) : '';
            case 'decision_record_file_id':
                return (isset($owner->decision_record)) ? $owner->decision_record->DisplayName . SIMAHtml::fileDownload($owner->decision_record->id) : '';
            case 'building_structure_id':
                if (isset($owner->building_structure))
                {
                    return $owner->building_structure->name . '<br />' . $owner->building_structure->size . ' ' . $owner->building_structure->size_unit->name;
                }
                else
                    return '---';
            default: return parent::columnDisplays($column);
        }
    }

    public function modelViews()
    {
        return array_merge(parent::modelViews(),[
            'inFile'// => array('style' => 'border: 1px solid;'),
//            'tabTemplates'=>array('with'=>array('templated_file'),'style'=>'border: 1px solid;')  //.creator
        ]);
    }
    
    public function modelForms()
    {
        $owner = $this->owner;
        
        if ($owner->isNewRecord)
        {
            $owner->theme->type = Theme::$BID;
        }
        
        if (empty($owner->currency_id))
        {
            $owner->currency_id = Yii::app()->configManager->get('base.country_currency', false);
        }

        return array(
            'default'=>array(
                'type'=>'generic',
                'columns'=>array(
                    'filing_id' => 'display',
                    'partner_id' => array('searchField','relName'=>'partner'),
                    'subject' => 'textArea',
//                    'job_type_id' => array('dropdown','relName'=>'job_type','empty'=>'Projekat'),
                    'delivering_time' => array('datetimeField', 'mode'=>'datetime'),
                    'opening_time' => array('datetimeField', 'mode'=>'datetime'),
                    'bid_status' => 'dropdown',
                    'qualification_end' => 'datetimeField',
                    'archive'=>'textArea',
                    'refer_id' => ['searchField','relName' => 'refer', 'add_button' => ['action' => 'simaAddressbook/index']],
                    'text_of_bid' => 'textArea',
                    'base_of_bid' => 'textArea',
                    'compose_id' => ['searchField','relName' => 'compose', 'add_button' => ['action' => 'simaAddressbook/index']],
                    'limit_execution_work' => 'textField',
                    'number_of_bid' => 'textField',
                    'is_enable_template' => 'checkBox'
                    /** @todo dodati vise podizvodjaca i vise koalicionih partnera*/
                )
            )
        );
    }
    
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {   
            case 'inFile': return [
                'columns' => [
                    'filing',
                    'partner',
                    'display_name',
                    'subject',
                    'value',
                ]
            ];
            default:
                return [
                    'columns' => [
                            'filing_id',
                            'partner_id', 'display_name', 'subject',
                            'theme.job_type',
                            'delivering_time', 'opening_time', 'qualification_end',
                            'bid_status', 'opening_record_file_id', 'decision_record_file_id'
                        ]
                ];
        }
    }

}