<?php

class BidRequirementTypeGUI extends SIMAActiveRecordGUI
{

    public function columnLabels()
    {
        return array(
            'name'=>'Naziv',
            'description'=>'Napomena'
        ) + parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return 'Tip dodatnog uslova ponude';
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default: return array(
                'columns' => array(
                    'name','description'
                )
            );
        }
    }
    
    public function modelForms() {
        return array(
            'default'=>array(
                'type'=>'generic',
                'columns'=>array(                    
                    'name'=>'textField',
                    'description'=>'textArea'
                )
            )
        );
    }        
}