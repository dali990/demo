<?php
/**
 * Description of ArchiveBoxGUI
 *
 * @author goran-set
 */
class ArchiveBoxGUI extends SIMAActiveRecordGUI
{
    public function columnLabels()
    {
        return array(
            'description'=>'Opis',
            'name'=>'Naziv',
            'id'=>'Redni broj',
            'archive_shelf_id'=>'Polica'
        )+parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return 'Kutija';
    }
    
    public function modelViews()
    {
        return array(
           'content'
        );
    }
    
    public function modelForms()
    {
        return array(
            'default'=>array(
                'type'=>'generic',
                'columns'=>array(
                    'id'=>'hidden',
                    'name'=>'textField',
                    'archive_shelf_id'=>array('relation', 'relName'=>'archive_shelf'),
                    'description'=>'textArea'
                )
            )
        );
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default:
                return [
                    'columns' => ['name','description']
                ];
        }
    }
    
}
