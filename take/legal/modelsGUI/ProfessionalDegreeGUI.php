<?php

class ProfessionalDegreeGUI extends SIMAActiveRecordGUI
{
    public function columnLabels() {
        return array(
            'name' => 'Stepen',
            'description' => 'Opis'
        ) + parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return  'Stručne spreme';
    }
    
//    public function modelForms() {
//        return array(
//            'default' => array(
//                'type' => 'generic',
//                'columns' => array(
//                    'name' => 'textField'
//                )
//            )
//        );
//    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default: return array(
                'columns' => array(
                    'name', 'description'
                )
            );
        }
    }
}

