<?php
/**
 * Description of ArchiveShelfGUI
 *
 * @author goran-set
 */
class ArchiveShelfGUI extends SIMAActiveRecordGUI
{
    public function columnLabels()
    {
        return array(
            'description'=>'Opis',
            'name'=>'Naziv',
            'id'=>'Redni broj',
        )+parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return 'Polica';
    }
    
    public function modelViews()
    {
        return array(
            'content'
        );
    }
    
    public function modelForms()
    {
        return array(
            'default'=>array(
                'type'=>'generic',
                'columns'=>array(
                    'id'=>'hidden',
                    'name'=>'textField',
                    'description'=>'textArea'
                )
            )
        );
    }
    
      public function guiTableSettings($type)
    {
        switch ($type)
        {
            default:
                return [
                    'columns' => ['name','description']
                ];
        }
    }
}
