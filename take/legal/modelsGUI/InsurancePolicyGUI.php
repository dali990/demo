<?php

class InsurancePolicyGUI extends SIMAActiveRecordDisplayGUI
{
    public function getDisplayModel()
    {
        return $this->owner->file;
    }
    
    public function columnLabels()
    {
        return [
            'policy_number' => Yii::t('LegalModule.InsurancePolicy', 'PolicyNumber'),
            'start_date' => Yii::t('LegalModule.InsurancePolicy', 'StartDate'),
            'expire_date' => Yii::t('LegalModule.InsurancePolicy', 'ExpireDate'),
            'bill_in_id' => Yii::t('LegalModule.InsurancePolicy', 'Bill'),
            'bill_in' => Yii::t('LegalModule.InsurancePolicy', 'Bill'),
            'premium_amount' => Yii::t('LegalModule.InsurancePolicy', 'PremiumAmount'),
            'insurance_company_id' => Yii::t('LegalModule.InsurancePolicy', 'InsuranceCompany'),
            'insurance_company' => Yii::t('LegalModule.InsurancePolicy', 'InsuranceCompany'),
            'vinkulation_partner_id' => Yii::t('LegalModule.InsurancePolicy', 'Vinkulation'),
            'vinkulation_partner' => Yii::t('LegalModule.InsurancePolicy', 'Vinkulation')
        ]+parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return Yii::t('LegalModule.InsurancePolicy', $plural ? 'InsurancePolicies' : 'InsurancePolicy');
    }
    
    public function modelForms()
    {
        return [
            'default'=>[
                'type'=>'generic',
                'columns'=>[
                    'policy_number' => 'textField',
                    'start_date'=>'datetimeField',
                    'expire_date'=>'datetimeField',
                    'premium_amount' => 'numberField',
                    'bill_in_id'=>['relation','relName'=>'bill_in', 'add_button'=>true],
                    'insurance_company_id'=>['relation','relName'=>'insurance_company', 'add_button'=>true],
                    'vinkulation_partner_id'=>['relation','relName'=>'vinkulation_partner', 'add_button'=>true],
                ]
            ]
        ];
    }
    
    public function guiTableSettings($type)
    {
        switch($type)
        {                
            case 'without_file_belongs':
                return [
                    'columns' => [
                        'policy_number', 'start_date', 'expire_date', 'insurance_company', 'vinkulation_partner', 'premium_amount',
                        'file.filing.filing_number', 'file.name', 'file.note', 'bill_in.bill_number'
                    ],
                    'order_by'=>['expire_date']
                ];
            default: return [
                'columns' => [
                    'policy_number', 'start_date', 'expire_date', 'insurance_company', 'vinkulation_partner', 'premium_amount',
                    'file.filing.filing_number', 'file.name', 'file.belongs', 'file.note', 'bill_in.bill_number'
                ],
                'order_by'=>['expire_date']
            ];
        }
    }
    
    public function modelViews()
    {
        return ['inFile'];
    }
}
?>