<?php

class ContractGUI extends SIMAActiveRecordGUI
{

    public function columnLabels()
    {
        return array(
            'contract_number'=>'Broj ugovora',
            'filing_date'=>'Zavodni datum',
            'partner_filing_number'=>'Partn. zav. broj',
            'partner_filing_date'=>'Partn. zav. datum',
            'contract_partner_status_id'=>'Partnerov status',
            'subject'=>'Predmet',
            'sign_date'=>'Datum potpisivanja',
            'deadline'=>'Rok',
            'deadline_description'=>'Opisni rok',
            'value_text'=>'Vrednost opisno',
            'vat_text'=>'PDV opisno',
            'advance_text'=>'Avans opisno',
            'parent_id'=>'Osnovni ugovor',
            'old_number'=>'Stari broj',
            'confirmed'=>'Potvrdi',
            'contract_type_id'=>'Tip ugovora',
            'location_id'=>'Lokacija',
            'vat'=>'PDV - vrednost',
            'vat_included'=>'uključen PDV',
            'advance'=>'Avans',
            'value'=>'Vrednost ugovora'
        )+parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return 'Ugovor';
    }
    
    public function columnDisplays($column)
    {
        $owner = $this->owner;
        switch ($column)
        {
// 			case 'contract_number':		return CHtml::link(($owner->contract_number=='')?'????':$owner->contract_number,array('legal/contract','id'=>$owner->id),array('target'=>'_blank'));
            case 'contract_number': return ($owner->contract_number == '') ? '????' : $owner->contract_number;
            case 'old': return ($owner->old) ? 'jeste' : 'nije';
            case 'filing_number': return (isset($owner->file) && isset($owner->file->filing)) ? $owner->file->filing->filing_number : '?';
            case 'filing_date': return (isset($owner->file) && isset($owner->file->filing)) ? $owner->file->filing->date : '?';
            case 'registry_id': return (isset($owner->registry)) ? $owner->registry->DisplayName : '?';
            case 'contract_type_id': return (isset($owner->contract_type)) ? $owner->contract_type->DisplayName : '?';
            case 'value':
                return SIMAHtml::money_format($owner->$column, $owner->currency->DisplayName);
            case 'vat':
                return SIMAHtml::money_format($owner->$column, $owner->currency->DisplayName)
                        . ((is_numeric($owner->value) && is_numeric($owner->vat) && ($owner->value > 0) && ($owner->vat > 0)) ? (' (' . (100 * $owner->vat / ($owner->value - $owner->vat)) . '%)') : '');
            case 'advance':
                return SIMAHtml::money_format($owner->$column, $owner->currency->DisplayName)
                        . ((is_numeric($owner->value) && is_numeric($owner->advance) && ($owner->value > 0) && ($owner->advance > 0)) ? (' (' . (100 * $owner->advance / ($owner->value)) . '%)') : '');
            case 'parent_id': 
                return (isset($owner->parent)) ?
                        CHtml::link($owner->parent->contract_number . ': ' . $owner->parent->subject, array('legal/legal/contract', 'id' => $owner->parent_id)) :
                        '?';
            default: return parent::columnDisplays($column);
        }
    }

    public function modelViews()
    {
        return array(
            'inFile'//=>array('style'=>'border: 1px solid;'),
//            'tabTemplates'=>array('with'=>array('templated_file'),'style'=>'border: 1px solid;')  //.creator
        );
    }
    
    public function modelForms()
    {
        return array(
            'default'=>array(
                'type'=>'file',
                'fileName'=>'default'
            )
        );
    }
    
    public function droplist($key, $relName='', $filter=null)
    {
        switch ($key)
        {
            case 'confirmed': return array('0' => 'Nije', '1' => 'Jeste');
            case 'belongs': return FileTag::model()->getModelDropList();
            default: return parent::droplist($key, $relName, $filter);
        }
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default: return [
                'columns' => [
                    'filing.filing_number',
                    'filing.date',
                    'partner_filing_number',
                    'partner_filing_date',
                    'sign_date',
                    'partner',
                    'contract_type_id',
                    'subject',
                    'deadline',
//				'belongs',
                    'file.filing.registry'
                ],
                'order_by' => ['contract_type_id','subject','sign_date','partner_filing_date','filing.date']
            ];
        }

    }
}