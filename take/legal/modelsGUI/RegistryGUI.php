<?php

/**
 * Description of CloseWokrContractGUI
 *
 * @author goran
 */
class RegistryGUI extends SIMAActiveRecordGUI {

    public function columnLabels() {
        return array(
            'archived' => 'Arhiviran',
            'location' => 'lokacija',
            'archive_box_id'=>'Arhivska kutija',
            'users'=>'Korisnici'
        ) + parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return Yii::t('LegalModule.Registry', 'Registry');
    }
    
    public function columnDisplays($column) {
        $owner = $this->owner;
        switch($column)
        {
            case 'archived': return ($owner->archived)?'Jeste':'Nije';
            default : return parent::columnDisplays($column);
        }
    }

//    public function modelViews()
//    {
//        return array(
//           'inFile'=>array('class'=>'basic_info'),
////            'tabTemplates'=>array('with'=>array('templated_file'),'style'=>'border: 1px solid;')  //.creator
//        );
//    }


    public function modelForms() {
        return array(
            'default' => array(
                'type' => 'generic',
                'columns' => array(
                    'name' => 'textField',
                    'archived' => 'checkBox',
                    'archive_box_id'=>array('relation', 'relName'=>'archive_box'),
                    'description' => 'textArea',
                    'users'=>array('list',
                        'model'=>'User',
                        'relName'=>'users',
                        'multiselect'=>true
                    ),
                )
            )
        );
    }
    
    public function guiTableSettings($type) 
    {
        return [
            'columns' => [
                'name','archived','description','location'
            ],
            'order_by' => ['name','archived']
        ];
    }
    
    public function droplist($key, $relName='', $filter=null)
    {
        switch ($key)
        {
            case 'archived': return ['0' => Yii::t('BaseModule.Common', 'No'), '1' => Yii::t('BaseModule.Common', 'Yes')];


                break;

            default: return parent::droplist($key, $relName, $filter);
        }
        
    }

}
