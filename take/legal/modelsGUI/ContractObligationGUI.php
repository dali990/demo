<?php

class ContractObligationGUI extends SIMAActiveRecordGUI
{
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default:
                return [
                    'columns' => ['name', 'description', 'contract_obligation_type_id']
                ];
        }
    }
    
}