<?php

class EmployeeToDriverLicenseCategoryGUI extends SIMAActiveRecordGUI
{

    public function columnLabels()
    {
        return array(
            'employee_id' => 'Zaposleni',
            'employee' => 'Zaposleni',
            'driver_license_category_id' => 'Vozačka kategorija',
            'driver_license_category' => 'Vozačka kategorija',
        ) + parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return 'Vozačka kategorija zaposlenog';
    }
    
    public function modelForms()
    {
        return array(
            'default' => array(
                'type' => 'generic',
                'columns' => array(
                    'employee_id'=>array('relation', 'relName'=>'employee'),
                    'driver_license_category_id'=>array('relation', 'relName'=>'driver_license_category', 'add_button'=>true),
                )
            )
        );
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default:
                return [
                    'columns' => [
                        'employee', 'driver_license_category'
                    ]
                ];
        }
    }
}
