<?php

class BusinessOfferVariationPredefinedItemGUI extends SIMAActiveRecordGUI
{   
    public function columnLabels()
    {
        return [
            'name' => Yii::t('LegalModule.BusinessOfferVariationPredefinedItem', 'Name'),
            'description' => Yii::t('LegalModule.BusinessOfferVariationPredefinedItem', 'Description')
        ] + parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return Yii::t('LegalModule.BusinessOfferVariationPredefinedItem', $plural ? 'BusinessOfferVariationPredefinedItems': 'BusinessOfferVariationPredefinedItem');
    }

    public function modelForms()
    {
        $owner = $this->owner;

        return [
            'default' => [
                'type' => 'generic',
                'columns' => [
                    'name' => 'textField',
                    'description' => 'tinymce'
                ]
            ]
        ];
    }

    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default:
                return [
                    'columns' => ['name', 'description']
                ];
        }
    }
}
