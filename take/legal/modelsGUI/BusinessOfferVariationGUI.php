<?php

class BusinessOfferVariationGUI extends SIMAActiveRecordDisplayGUI
{   
    public function columnLabels()
    {
        return [
            'name' => Yii::t('LegalModule.BusinessOfferVariation', 'Name'),
            'file' => Yii::t('LegalModule.BusinessOfferVariation', 'File'),
            'business_offer_id' => Yii::t('LegalModule.BusinessOffer', 'BusinessOffer'),
            'business_offer' => Yii::t('LegalModule.BusinessOffer', 'BusinessOffer'),
            'value' => Yii::t('LegalModule.BusinessOfferVariation', 'Value'),
            'confirmed' => Yii::t('LegalModule.BusinessOfferVariation', 'Confirmed'),
            'sent' => Yii::t('LegalModule.BusinessOfferVariation', 'Sent'),
            'created_by_user_id' => Yii::t('LegalModule.BusinessOfferVariation', 'CreatedByUser'),
            'created_by_user' => Yii::t('LegalModule.BusinessOfferVariation', 'CreatedByUser'),
            'created_timestamp' => Yii::t('LegalModule.BusinessOfferVariation', 'CreatedTimestamp'),
            'description' => Yii::t('LegalModule.BusinessOfferVariation', 'Description'),
            'discount_percentage' => Yii::t('LegalModule.BusinessOfferVariation', 'DiscountPercentage'),
            'discount_value' => Yii::t('LegalModule.BusinessOfferVariation', 'DiscountValue'),
            'copy' => Yii::t('LegalModule.BusinessOfferVariation', 'CopyToNewVariation'),
            'order_number' => Yii::t('LegalModule.BusinessOfferVariation', 'OrderNumber'),
            'footer' => Yii::t('LegalModule.BusinessOfferVariation', 'Footer'),
            'advance_percentage' => Yii::t('LegalModule.BusinessOfferVariation', 'AdvancePercentage'),
            'advance_value' => Yii::t('LegalModule.BusinessOfferVariation', 'AdvanceValue')
        ] + parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return Yii::t('LegalModule.BusinessOfferVariation', $plural ? 'BusinessOfferVariations': 'BusinessOfferVariation');
    }
    
    public function columnDisplays($column)
    {
        $owner = $this->owner;

        switch ($column)
        {
            case 'copy': 
                return Yii::app()->controller->widget(SIMAButtonVue::class, [
                    'title' => Yii::t('LegalModule.BusinessOfferVariation', 'Copy'),
                    'onclick'=>["sima.legal.copyBusinessOfferVariation", $owner->business_offer_id, $owner->id]
                ], true);
            case 'value':
                $return = SIMAHtml::number_format($owner->value_with_additional_discount, 2);
                
                return $return;
            default: return parent::columnDisplays($column);
        }
    }
    
    public function modelViews()
    {
        return [
           'basic_info', 'full_info'
        ] + parent::modelViews();
    }
    
    public function modelForms()
    {
        $owner = $this->owner;

        return [
            'default' => [
                'type' => 'generic',
                'columns' => [
                    'business_offer_id' => 'hidden',
                    'name' => 'textField',
                    'discount_percentage' => 'numberField',
                    'discount_value' => 'numberField',
                    'advance_percentage' => 'numberField',
                    'advance_value' => 'numberField',
                    'description' => 'textArea',
                    'footer' => 'tinymce'
                ]
            ]
        ];
    }

    public function guiTableSettings($type)
    {
        switch ($type)
        {
            case 'from_business_offer':
                return [
                    'columns' => ['order_number', 'name', 'value', 'discount_percentage', 'discount_value', 'advance_percentage', 'advance_value', 'description', 'file', 'created_by_user', 'created_timestamp', 'copy']
                ];
            default:
                return [
                    'columns' => ['order_number', 'name', 'value', 'discount_percentage', 'discount_value', 'advance_percentage', 'advance_value', 'description', 'file', 'business_offer', 'created_by_user', 'created_timestamp', 'copy']
                ];
        }
    }
    
    public function generatePdf()
    {
        $owner = $this->owner;
        
        $business_offer_variation_report = SIMAReport::factory(BusinessOfferVariationReport::class,['business_offer_variation' => $owner]);  
        file_put_contents("/var/www/html/test/report.html", $business_offer_variation_report->getHTML());
        $temp_file = $business_offer_variation_report->getPDF();
        
        $version_order = 1;
        if (!empty($owner->file->last_version))
        {
            $version_order = $owner->file->last_version->version + 1;
        }

        $owner->file->appendTempFile(
            $temp_file->getFileName(),
            Yii::t('LegalModule.BusinessOfferVariation', 'BusinessOfferVariation') . '_' . $version_order . '.pdf'
        );

        return $temp_file;
    }
}
