<?php

class PotentialBidGUI extends SIMAActiveRecordGUI
{

    public function columnLabels()
    {
        return array(
            'bid_name'=>'Naziv',
            'orderer_by'=>'Naručilac',
            'orderer_by_id'=>'Naručilac',
            'job_type'=>'Vrsta posla',
            'job_type_id'=>'Vrsta posla',
            'deadline'=>'Rok za predaju',
            'estimated_value'=>'Procenjena vrednost',            
            'can_be_used_bank_statament_from_apr'=>'Može da se koristi izvod iz APR',            
            'description'=>'Napomena',            
            'created_date'=>'Datum kreiranja',
            'bid_link'=>'Link',
            'accepted'=>'Prihvaćeno',
            'accepted_filter'=>'Status'
        ) + parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return 'Potencijalna ponuda';
    }
    
    public function modelViews()
    {
        return array(
            'indexTitle','inFile'
        );
    }
    
    public function columnDisplays($column)
    {
        $owner = $this->owner;
        switch ($column)
        {
            case 'bid_link':
                if (!empty($owner->bid_link))
                {
                    return SIMAHtml::link('Pogledaj ponudu',$owner->bid_link,['target'=>'_blank']);
                }
                else
                {
                    return 'link nije unesen';
                }
            case 'accepted':
            case 'accepted_filter':
                $return = parent::columnDisplays('accepted');
                if ($owner->accepted === true)
                {
                    if (isset($owner->job_bid))
                    {
                        $return .= SIMAHtml::modelFormOpen($owner->job_bid);
                    }
                    else
                    {
                        $return .= SIMAHtml::modelFormOpen(JobBid::model(),[
                            'init_data'=>[
                                'JobBid'=>[
                                    'potential_bid_id' => $owner->id,
                                    'partner_id' => $owner->orderer_by_id,
                                    'subject' => $owner->bid_name,
                                    'job_type_id' => $owner->job_type_id,
                                    'value' => $owner->estimated_value,
                                    'delivering_time' => $owner->deadline
                                ]
                            ],
                            'button_title'=>'Kreiraj ponudu'
                        ]);
                    }
                }
                return $return;
            default: return parent::columnDisplays($column);
        }
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default: return array(
                'columns' => array(
                    'bid_name','created_date','orderer_by','job_type','deadline','estimated_value','accepted_filter','bid_link',
                    'can_be_used_bank_statament_from_apr','description'
                ),
                'order_by'=>['created_date', 'deadline']
            );
        }
    }
    
    public function modelForms() {
        return array(
            'default'=>array(
                'type'=>'generic',
                'columns'=>array(                    
                    'bid_name'=>'textField',
                    'created_date'=>'datetimeField',
                    'orderer_by_id'=>['relation', 'relName'=>'orderer_by', 'add_button'=>['action'=>'simaAddressbook/index']],
//                    'orderer_by_id'=>['relation', 'relName'=>'orderer_by', 'add_button'=>['action'=>'simaAddressbook/index']], /// milosj 14.06.2017. predlog za upotrebu novog imenika
                    'job_type_id'=>['relation', 'relName'=>'job_type'],
                    'deadline'=>'datetimeField',
                    'estimated_value'=>'numberField',
                    'accepted'=>'status',
                    'bid_link'=>'textField',
                    'can_be_used_bank_statament_from_apr'=>'checkBox',                    
                    'description'=>'textArea'
                )
            )
        );
    }   
    
    public function droplist($key, $relName='', $filter=null)
    {        
        switch ($key)
        {
            case 'accepted_filter':
                return PotentialBid::$potential_bid_statuses;
            default: return parent::droplist($key, $relName, $filter);
        }
    }
}
