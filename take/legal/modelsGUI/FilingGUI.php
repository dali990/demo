<?php

class FilingGUI extends SIMAActiveRecordGUI
{

    public function columnLabels()
    {
        return array(
            'in' => Yii::t('LegalModule.Filing', 'Direction'),
            'file.name' => 'Predmet',
            'delivery_types' => Yii::t('LegalModule.Filing', 'DeliveryTypes'),
            'filing_to_delivery_types' => Yii::t('LegalModule.Filing', 'DeliveryTypes'),
            'comment' => Yii::t('BaseModule.Common', 'Comment'),
            'number' => 'Zavodni broj',
            'subnumber' => 'Podbroj',
            'creator_id' => 'Zaveo/la',
//            'responsible_id' => 'Odgovorno lice',
//            'responsible' => 'Odgovorno lice',
            'file_id' => 'Skenirano',
            'addBill' => '',
            'job_id' => 'Posao',
            'contrib_ids' => Yii::t('FilesModule.File', 'Contribs'),
            'contrib_id' => '-',
            'confirmed' => Yii::t('BaseModule.Common', 'Confirmed'),
            'returned' => Yii::t('LegalModule.Filing', 'Returned'),
            'contribs' => Yii::t('FilesModule.File', 'Contribs'),
            'located_at_id' => Yii::t('LegalModule.Filing', 'LocatedAt'),
            'located_at' => Yii::t('LegalModule.Filing', 'LocatedAt'),
            'registry' => 'Registrator',
            'filing_number' => Yii::t('LegalModule.Filing', 'FilingNumber')
        )+parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return Yii::t('LegalModule.Filing', 'FilingNumber');
    }
    
    public function columnDisplays($column)
    {
        $owner = $this->owner;
        switch ($column)
        {
            case 'in': 
                if (gettype($owner->in)==='NULL') return 'Arhivski';
                else return ($owner->in)?'Ulazna':'Izlazna';
            case 'delivery_types': return (isset($owner->delivery_types)) ? Yii::app()->controller->renderModelView($owner,'deliveryTypesView') : '?';
            default: return parent::columnDisplays($column);
        }
    }
    
    public function droplist($key, $relName = '', $condition = null) {
        switch ($key) {
            case 'in': return array(
                'null'=>Yii::t('LegalModule.Filing', 'Archive'),
                '0' => Yii::t('LegalModule.Filing', 'Out'),
                '1' => Yii::t('LegalModule.Filing', 'In'),
            );
            case 'belongs': return FileTag::model()->getModelDropList();
            default: return parent::droplist($key,$relName,$condition);
        }
    }

    public function modelViews()
    {
        return array(
            'inFile'=>array('with'=>'file','params_function' => 'paramsInFile'),
            'contribsList'=>array('with'=>'filing_contribs.contrib'),
            'deliveryTypes',
            'deliveryTypesView' => ['html_wrapper' => 'span'],
            'full_info'
//            'belongsEdit'
//            'tabTemplates'=>array('with'=>array('templated_file'),'style'=>'border: 1px solid;')  //.creator
        );
    }
    
    public function paramsInFile()
    {
        $owner = $this->owner;
        
        $located_at_display = $owner->getAttributeDisplay('located_at');
        
        $transfer = null;
        if(empty($owner->file))
        {
            $autobafreq_message = 'file relation is empty'
                                    .'</br> - $owner->id: '.SIMAMisc::toTypeAndJsonString($owner->id)
                                    .'</br> - $owner: '.SIMAMisc::toTypeAndJsonString($owner)
                                    .'</br> - $owner->file: '.SIMAMisc::toTypeAndJsonString($owner->file)
                                    .'</br> - Yii::app()->user->id: '.SIMAMisc::toTypeAndJsonString(Yii::app()->user->id);
            $theme_id = 3500;
            $theme_name = "AutoBafReq FilingGui::paramsInFile file relation empty";
            Yii::app()->errorReport->createAutoClientBafRequest($autobafreq_message, $theme_id ,$theme_name);
        }
        else
        {
            $transfer = $owner->file->file_transfer;
        }
        
        if (!empty($transfer))
        {
            $located_at_display .= ' - poslato od '. $transfer->sender->DisplayHTML;
            if (Yii::app()->user->id == $transfer->recipient_id)
            {
                if(SIMAMisc::isVueComponentEnabled())
                {
                    $located_at_display .= Yii::app()->controller->widget(SIMAButtonVue::class, [
                        'title' => Yii::t('BaseModule.Common','Confirm'),
                        'onclick'=>['sima.legal.confirmFilingTransfer', $transfer->id],
                    ], true);
                }
                else
                {
                    $located_at_display .= Yii::app()->controller->widget('SIMAButton', [
                        'action' => [
                            'title' => Yii::t('BaseModule.Common','Confirm'),
                            'onclick'=>['sima.legal.confirmFilingTransfer', $transfer->id],
                        ]
                    ], true);
                }
            }
            else if (Yii::app()->user->id == $transfer->sender_id)
            {
                if(SIMAMisc::isVueComponentEnabled())
                {
                    $located_at_display .= Yii::app()->controller->widget(SIMAButtonVue::class, [
                        'title' => Yii::t('BaseModule.Common','Cancel'),
                        'onclick'=>['sima.legal.cancelFilingTransfer', $transfer->id],
                    ], true);
                }
                else
                {
                    $located_at_display .= Yii::app()->controller->widget('SIMAButton', [
                        'action' => [
                            'title' => Yii::t('BaseModule.Common','Cancel'),
                            'onclick'=>['sima.legal.cancelFilingTransfer', $transfer->id],
                        ]
                    ], true);
                }
            }
        }
        
        return [
            'model' => $owner,
            'located_at_display' => $located_at_display
        ];
    }
    
    public function modelForms()
    {
        $owner = $this->owner;
        if (empty($owner->located_at_id))
        {
            $owner->located_at_id = Yii::app()->user->id;
        }
        if ($owner->registry_id === null)
        {            
            $model_filter = [
                'archived'=>false
            ];
        }
        else
        {            
            $model_filter = [
                'OR',
                ['ids' => [$owner->registry_id]],
                ['archived'=>false]
            ];
        }
        
        $partner_id = ['searchField', 'relName' => 'partner', 'add_button'=>['action'=>'simaAddressbook/index']];
        
        return array(
            'guiTable2'=>array(
                'type'=>'generic',
                'columns'=>array(
                    'id'=>'textField',
                    'confirmed'=>'status',
                    'returned'=>'status',
                    'number'=>'textField',
                    'subnumber'=>'textField',
                    'in'=>array('dropdown'),
                    'date'=>'datetimeField',
                    'comment'=>'textArea',
                    'partner_id' => $partner_id,
                    'filing_to_delivery_types'=>array('list',
                        'model'=>'DeliveryType',
                        'relName'=>'filing_to_delivery_types', 
                    ),
                    'located_at_id' => array('searchField', 'relName' => 'located_at'),
                    'registry_id'=>array(
                        'dropdown',
                        'empty'=>'Izaberi',
                        'model_filter' => $model_filter
                    ),
                    'contribs'=>array('list',
                        'model'=>'File',
                        'relName'=>'contribs',
                        'view'=>'filing_contribs',
                        'multiselect'=>true
                    ),
                )
            ),
            'default'=>array(
                'type'=>'file',
                'fileName' => 'default'
            )
        );
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {   
            case 'scanning': return [
                'columns' => ['filing_number']
            ];
            case 'mine': return [
                'columns' => ['filing_number', 'in', 'date', 'file.name', 'comment', 'file.document_type', 'partner_id',
                    'delivery_types', 'located_at', 'file.responsible', 'registry_id', 'belongs']
            ];
            case 'mine_responsability': return [
                'columns' => ['filing_number', 'in', 'date', 'file.name', 'comment', 'file.document_type', 'partner_id',
                    'delivery_types', 'located_at', 'creator_id', 'registry_id', 'belongs']
            ];
            default:
                return [
                    'columns' => [
                        'filing_number', 
                        'in', 
                        'date', 
                        'file.name' => ['min_width' => 100, 'default_width' => 300], 
                        'file.document_type' => ['min_width' => 100, 'default_width' => 200], 
                        'partner' => ['min_width' => 100, 'default_width' => 300],
                        'belongs' => ['min_width' => 100, 'default_width' => 300], 
                        'comment' => ['min_width' => 100, 'default_width' => 300], 
                        'delivery_types', 
                        'located_at' => ['min_width' => 100, 'default_width' => 200], 
                        'file.responsible', 
                        'registry_id', 
                        'creator_id' => ['min_width' => 100, 'default_width' => 200],
                        
                    ]
                ];
        }
    }
    
}