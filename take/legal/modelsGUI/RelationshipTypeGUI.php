<?php

class RelationshipTypeGUI extends SIMAActiveRecordGUI
{

    public function columnLabels()
    {
        return array(
            'name' => 'Naziv',
            'description' => 'Opis'
        ) + parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return 'Rodbinska veza';
    }
    
    public function modelForms()
    {
        return array(
            'default' => array(
                'type' => 'generic',
                'columns' => array(
                    'name' => 'textField',
                    'description' => 'textArea'
                )
            )
        );
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default:
                return [
                    'columns' => [
                        'name', 'description'
                    ]
                ];
        }
    }
}
