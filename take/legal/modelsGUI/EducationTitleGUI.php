<?php

class EducationTitleGUI extends SIMAActiveRecordGUI
{
    public function columnLabels() {
        return array(
            'name' => Yii::t('BaseModule.Common', 'Name'),
            'display_name' => Yii::t('BaseModule.Common', 'DisplayName')
        ) + parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return  Yii::t('HRModule.EducationTitle', $plural?'EducationTitles':'EducationTitle');
    }
    
    public function modelForms() {
        return array(
            'default' => array(
                'type' => 'generic',
                'columns' => array(
                    'name' => 'textField'
                )
            )
        );
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default: return array(
                'columns' => array(
                    'name'
                )
            );
        }
    }
}
