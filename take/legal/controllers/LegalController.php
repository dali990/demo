<?php

class LegalController extends SIMAController
{

//    public function accessRules()
//    {
//        return array(
//            array(
//                'allow',
//                'roles' => array('legal')
//            ),
//            array(
//                'allow',
//                'actions' => array('tabWorkRelation','partnerWorkRelationTabs','tabWorkContracts','tabAbsents','tabWorkTrainings'),
//                'roles' => array('employee')
//            ),
//            array(
//                'allow',
//                'actions' => array('statistic'),
//                'roles' => array('menuShowLegalEmployeesLicence')
//            ),
//            array(
//                'deny'
//            )
//        );
//    }

    public function actionEmployees()
    {
        $html = $this->renderPartial('employees', array(), true, true);

        $this->respondOK([
            'html' => $html,
            'title' => 'Prava-Zaposleni',
        ]);
    }

    public function actionAbsence()
    {
        $html = $this->renderPartial('absence', array(), true, true);

        $this->respondOK([
            'html' => $html,
            'title' => 'Zaposleni-Odsustva',
        ]);
    }

    public function actionStatistic()
    {
        $persons = Person::model()->with('employee')->findAllByAttributes(array('company_id' => Yii::app()->company->id));
        $count = 0;
        $count_system = 0;
        $working_count = 0;
        $not_working_count = 0;
        $permanent_count = 0;
        $not_permanent_working_count = 0;
        $not_permanent_not_working_count = 0;
        $non_contract = 0;

        foreach ($persons as $person)
        {
            $employee = $person->employee;
            $count++;
            if (isset($person->employee))
            {
                $count_system++;
                if (isset($employee->active_work_contract))
                    $working_count++;
                else
                    $not_working_count++;
                if (isset($employee->last_work_contract) && ($employee->last_work_contract->end_date == null || $employee->last_work_contract->end_date == ''))
                    $permanent_count++;
                if (isset($employee->last_work_contract) && !($employee->last_work_contract->end_date == null || $employee->last_work_contract->end_date == ''))
                {
                    if (isset($employee->active_work_contract))
                        $not_permanent_working_count++;
                    else
                        $not_permanent_not_working_count++;
                }
                if (!isset($employee->last_work_contract))
                    $non_contract++;
            }
        }

        $degrees = ProfessionalDegree::model()->byName()->with('employees')->findAll();
        $employees_degree_not_entered = Person::model()->findAllByAttributes(array('company_id' => Yii::app()->company->id, 'professional_degree_id' => null));
        $degree_not_entered = 0;
        foreach ($employees_degree_not_entered as $employee)
        {
            if (isset($employee->employee))
                $degree_not_entered++;
        }

        $html = $this->renderPartial('statistic', array(
            'count' => $count,
            'count_system' => $count_system,
            'working_count' => $working_count,
            'not_working_count' => $not_working_count,
            'permanent_count' => $permanent_count,
            'not_permanent_working_count' => $not_permanent_working_count,
            'not_permanent_not_working_count' => $not_permanent_not_working_count,
            'non_contract' => $non_contract,
            'degree_not_entered' => $degree_not_entered,
            'degrees' => $degrees,
                ), true, true);

        $this->respondOK([
            'html' => $html,
            'title' => 'Zaposleni - statistika',
        ]);
    }

    /// MilosJ: nije nadjeno da se igde koristi
//    public function actionContractNote()
//    {
//        try
//        {
//            $id = filter_input(INPUT_GET, 'id');
//            if (!isset($id))
//                throw new Exception('nije stavljen ID');
//            $model = Contract::model()->findByPk($id);
//            if ($model == null)
//                throw new Exception('ugovor nije nadjen');
//            switch ($model->contract_type_id)
//            {
//                case '1': $official_name = 'SET.QP.12-03 Beleska o Ugovorenom poslu ';
//                    $official_contract_name = 'SET.QP.12 Izrada ponude i ugovaranje ';
//                    break;
//                case '2': $official_name = 'SET.QP.12-04 Beleska o Aneksu Ugovorenog posla ';
//                    $official_contract_name = 'SET.QP.12 Izrada ponude i ugovaranje ';
//                    break;
//                case '3': $official_name = 'SET.QI.05-02 Beleska o Ugovorenom poslu za poslovne partnere ';
//                    $official_contract_name = 'SET.QI.05 Angazovanje poslovnih partnera ';
//                    break;
//                case '4': $official_name = 'SET.QI.05-03 Beleska o Aneksu Ugovorenog posla za poslovne partnere ';
//                    $official_contract_name = 'SET.QI.05 Angazovanje poslovnih partnera ';
//                    break;
//                case '5': $official_name = 'SET.QP.07-03 Beleska o Ugovorenom poslu ';
//                    $official_contract_name = 'SET.QP.07 Nabavka ';
//                    break;
//                default: echo 'Nije selektovan tip ugovora';
//                    break;
//            }
//            echo '<h4>' . $official_name;
//            echo '(' . CHtml::link('PDF', array('reports/ContractNote', 'id' => $model->id, 'type' => 'pdf'), array('class' => 'link')) . ')</h4>';
//            $this->renderPartial('../reports/ContractNote_pdf', array('model' => $model, 'official_name' => $official_contract_name));
//        }
//        catch (Exception $e)
//        {
//            echo $e->getMessage();
//        }
//    }
    
    public function actionTabPersonReferences($person_id)
    {
        $person = Person::model()->findByPk($person_id);
        $company_licenses = [];
        $company_licenses_models = CompanyLicenseToCompany::model()->findAll();
        foreach ($company_licenses_models as $value)
        {            
            $company_licenses[$value->id] = $value->DisplayName;                        
        }
        $html = $this->renderPartial('person_references', [
            'person'=>$person,
            'company_licenses'=>$company_licenses
        ], true, true);

        $this->respondOK(array(
            'html' => $html
        ));
    }

    public function actionAddBidDocument($bid_id, $file_id, $column)
    {
        if (!isset($bid_id) || !isset($file_id) || !isset($column))
            throw new Exception('actionAddBidDocument - nisu lepo zadati parametri.');

        $bid = Bid::model()->findByPk($bid_id);
        $bid->$column = $file_id;
        $bid->save();

        $this->respondOK(array(), array($bid));
    }

    public function actionBid($id)
    {
        if (!isset($id))
            throw new Exception('actionBid - nisu lepo zadati parametri');
        
        $bid = JobBid::model()->findByPk($id);
        if (is_null($bid))
        {
            $bid = QualificationBid::model()->findByPk($id);
        }
        if (is_null($bid))
        {
            $this->respondOK(array(
                'html' => 'Ponuda vise ne postoji',
            ));
        }
        $selector = $this->filter_post_input('selector', false);
        if (empty($selector))
        {
            $selector = [];
        }

        if (gettype($selector) === 'string')
            $selector = CJSON::decode($selector, true);

        $default_selected = array_shift($selector);
        if ($default_selected === null || !isset($default_selected['simaTabs']))
            $default_selected = 'comments';
        else
            $default_selected = $default_selected['simaTabs'];

        $html = $this->renderPartial('bid_new', array(
            'model' => $bid,
            'default_selected' => $default_selected,
            'selector' => $selector
        ), true, false);

        $this->respondOK(array(
            'html' => $html,
            'title' => ($bid == null) ? 'Nije ponuda' : $bid->DisplayName,
            'color' => '#FED39E',
            'selected' => true
        ));
    }

    public function actionAddLicenseToBid($bid_id, $license_model, $license_id)
    {
        if (!isset($bid_id) || !isset($license_model) || !isset($license_id))
            throw new Exception('actionAddLicenseToBid - nisu lepo zadati parametri');

        $bid = Bid::model()->findByPk($bid_id);
        if ($license_model === 'CompanyLicenseToCompany')
        {
            $bid_to_company_license = BidToCompanyLicense::model()->findByAttributes(array('bid_id' => $bid_id, 'company_license_id' => $license_id));
            if ($bid_to_company_license === null)
            {
                $bid_to_company_license = new BidToCompanyLicense();
                $bid_to_company_license->bid_id = $bid_id;
                $bid_to_company_license->company_license_id = $license_id;
                $bid_to_company_license->save();
            }
            else
            {
                $this->raiseNote('Već je dodata ova velika licenca!');
            }
        }
        else if ($license_model === 'PersonToWorkLicense')
        {
            $bid_to_work_license = BidToWorkLicense::model()->findByAttributes(array('bid_id' => $bid_id, 'work_license_id' => $license_id));
            if ($bid_to_work_license === null)
            {
                $bid_to_work_license = new BidToWorkLicense();
                $bid_to_work_license->bid_id = $bid_id;
                $bid_to_work_license->work_license_id = $license_id;
                $bid_to_work_license->save();
            }
            else
            {
                $this->raiseNote('Već je dodata ova radna licenca!');
            }
        }

        $this->respondOK(array(), $bid);
    }

    public function actionAddReferenceFile($reference_id, $file_id)
    {
        if (!isset($reference_id) || !isset($file_id))
            throw new Exception('actionAddReferenceFile - nisu lepo zadati parametri');

        $reference = Reference::model()->findByPk($reference_id);
        $reference->file_id = $file_id;
        $reference->save();

        $this->respondOK();
    }

    public function actionReference($id)
    {
        if (!isset($id))
            throw new Exception('actionReference - nisu lepo zadati parametri');

        $reference = Reference::model()->findByPk($id);
        $selector = $this->filter_post_input('selector', false);
        if (empty($selector))
            $selector = [];

        if (gettype($selector) === 'string')
            $selector = CJSON::decode($selector, true);

        $default_selected = array_shift($selector);
        if ($default_selected === null || !isset($default_selected['simaTabs']))
            $default_selected = 'info';
        else
            $default_selected = $default_selected['simaTabs'];

        $html = $this->renderPartial('reference', array(
            'model' => $reference,
            'default_selected' => $default_selected,
            'selector' => $selector
        ), true, true);

        $this->respondOK(array(
            'html' => $html,
            'title' => $reference->DisplayName,
            'color' => '#FED39E',
            'selected' => true
        ));
    }
    
    public function actionReferences()
    {
        $params = $this->filter_post_input('params', false);
        if(empty($params))
        {
            $params = [];
        }
        
        $filter = isset($params['filter'])?$params['filter']:[];
        $add_button = isset($params['add_button'])?$params['add_button']:true;
        
        $html = $this->renderPartial('references', [
            'filter'=>$filter,
            'add_button'=>$add_button
        ], true, true);

        $this->respondOK(array(
            'html' => $html,
            'title' => 'Reference',
            'color' => '#FED39E',
        ));
    }

    public function actionAddCompanyLicenseToReference($reference_id)
    {
        if (!isset($reference_id))
            throw new Exception('actionAddCompanyLicenseToReference - nisu lepo zadati parametri');

        $company_licenses = $this->filter_post_input('company_licenses', false);
        if(empty($company_licenses))
        {
            $company_licenses = [];
        }
        $reference = Reference::model()->findByPk($reference_id);
        $new_company_license_ids = [];
        foreach ($company_licenses as $company_license)
        {
            $reference_to_company_license = CompanyLicenseToReference::model()->findByAttributes(array('reference_id' => $reference_id, 'company_license_id' => $company_license['id']));
            if ($reference_to_company_license === null)
            {
                $reference_to_company_license = new CompanyLicenseToReference();
                $reference_to_company_license->reference_id = $reference_id;
                $reference_to_company_license->company_license_id = $company_license['id'];
                $reference_to_company_license->save();
            }
            array_push($new_company_license_ids, $company_license['id']);
        }

        $old_references_to_company_licenses = CompanyLicenseToReference::model()->findAllByAttributes(array('reference_id' => $reference_id));
        foreach ($old_references_to_company_licenses as $old_reference_to_company_license)
        {
            if (!in_array($old_reference_to_company_license->company_license_id, $new_company_license_ids))
            {
                $old_reference_to_company_license->delete();
            }
        }
        $this->respondOK(array(), $reference);
    }

    public function actionAddWorkLicenseToReference($reference_id)
    {
        if (!isset($reference_id))
            throw new Exception('actionAddWorkLicenseToReference - nisu lepo zadati parametri');

        $work_licenses = $this->filter_post_input('work_licenses', false);
        if(empty($work_licenses))
        {
            $work_licenses = [];
        }
        $reference = Reference::model()->findByPk($reference_id);
        $new_work_license_ids = [];
        foreach ($work_licenses as $work_license)
        {
            $reference_to_person_license = PersonLicenseToReference::model()->findByAttributes(array('reference_id' => $reference_id, 'person_license_id' => $work_license['id']));
            if ($reference_to_person_license === null)
            {
                $reference_to_person_license = new PersonLicenseToReference();
                $reference_to_person_license->reference_id = $reference_id;
                $reference_to_person_license->person_license_id = $work_license['id'];
                $reference_to_person_license->save();
            }
            array_push($new_work_license_ids, $work_license['id']);
        }

        $old_references_to_persons_licenses = PersonLicenseToReference::model()->findAllByAttributes(array('reference_id' => $reference_id));
        foreach ($old_references_to_persons_licenses as $old_reference_to_person_license)
        {
            if (!in_array($old_reference_to_person_license->person_license_id, $new_work_license_ids))
            {
                $old_reference_to_person_license->delete();
            }
        }
        $this->respondOK(array(), $reference);
    }
    
    public function actionTabInsurancePersons($id)
    {
        if (!isset($id))
        {
            throw new Exception('actionTabInsurancePersons - nisu lepo zadati parametri!');
        }
        
        $partner = Partner::model()->findByPk($id);
        if (!isset($partner->person->employee))
        {
            throw new Exception('actionTabInsurancePersons - ne postoji zaposleni!');
        }
        
        $html = $this->renderPartial('insurance_persons', ['model' => $partner->person->employee], true, true);

        $this->respondOK([
            'html' => $html
        ]);
    }
    
    public function actionGenerateIntroductionIntoBusinessProgram($work_contract_id)
    {
        $workContract = WorkContract::model()->findByPkWithCheck($work_contract_id);
        
        $workContract->generateIntroductionIntoBusinessProgram();
        
        $this->respondOK();
    }
    
    public function actionPotentialBidsView($filter=[])
    {
        $html = $this->renderPartial('potential_bids_view', [
            'filter'=>$filter
        ], true, true);

        $this->respondOK(array(
            'html' => $html,
            'title' => 'Potencijalne ponude',
            'color' => '#FED39E'
        ));
    }
    
    public function actionPersonActivityOverview()
    {

        $uniq = SIMAHtml::uniqid();
        $title = Yii::t('SIMAPersonActivityOverview.PersonActivityOverview', 'WorkHoursOverview');
        
        $content_panel_html = $this->renderPartial('person_activity_overview', [
            'uniq' => $uniq,
            'sector_id' => Yii::app()->company->head_sector_id
        ], true);
        
        Yii::app()->clientScript->registerScript("sima_tabs_init_$uniq", "new Vue({el: '#$uniq'});", CClientScript::POS_END);
        
        $html = Yii::app()->controller->renderContentLayout(
            [
                'name' => $title
            ], 
            [
                [
                    'html' => $content_panel_html
                ]
            ]
        );
        
        $this->respondOK([
            'title' => $title,
            'html' => $html
        ]);
    }
    
    public function actionRenderActiveWorkedHoursEvidences()
    {
        $title = Yii::t('LegalModule.Legal', 'ActiveWorkedHoursEvidences');
        $html = $this->widget(SIMAGuiTable::class, [
            'model' => WorkedHoursEvidence::model(),
            'fixed_filter' => [
                'active' => true
            ]
        ], true);
        
        $this->respondOK([
            'title' => $title,
            'html' => $html
        ]);
    }

    public function actionRenderAllWorkedHoursEvidences()
    {
        $title = Yii::t('LegalModule.Legal', 'AllWorkedHoursEvidences');
        $html = $this->widget(SIMAGuiTable::class, [
            'model' => WorkedHoursEvidence::model()
        ], true);
        
        $this->respondOK([
            'title' => $title,
            'html' => $html
        ]);
    }
    
    public function actionBidReportSuggestion()
    {
        $data = $this->setEventHeader();
        $bid_id = $this->filter_input($data, 'bid_id');
        $bid = Bid::model()->findByPkWithCheck($bid_id);

        $bid_report = new BidReport($bid);
        
        $this->sendUpdateEvent(10);
        
        $temp_file = $bid_report->getPDF();
        
        $this->sendUpdateEvent(90);
        
        $pdf_as_image = $temp_file->renderPdfAsImage($temp_file->getFullPath());
        
        $this->sendUpdateEvent(100);
        
        $this->sendEndEvent([
            'html' => $pdf_as_image,
            'temp_file_name' => $temp_file->getFileName()
        ]);
    }
    
    public function actionBidReportAddNewVersionFromTempFile($bid_id, $temp_file_name)
    {
        $curr_user_id = Yii::app()->user->id;
        $bid = Bid::model()->findByPkWithCheck($bid_id);
        $download_name = $bid->subject . '.pdf';
        
        $file_for_bid = $bid->getFileForBid($curr_user_id);
        $file_for_bid->appendTempFile($temp_file_name, $download_name);
        
        $this->respondOK([], $bid);
    }
    
    public function actionCopyBusinessOfferVariationItems($new_business_offer_variation_id, $old_business_offer_variation_id)
    {
        $old_business_offer_variation = BusinessOfferVariation::model()->findByPkWithCheck($old_business_offer_variation_id);
        $new_business_offer_variation = BusinessOfferVariation::model()->findByPkWithCheck($new_business_offer_variation_id);

        $old_business_offer_variation->copyVariationItems($new_business_offer_variation);
        
        $this->respondOK();
    }
    
    public function actionGetBusinessOfferVariationPredefinedItem($item_id)
    {
        $item = BusinessOfferVariationPredefinedItem::model()->findByPkWithCheck($item_id);
        
        $this->respondOK([
            'item_name' => $item->name,
            'item_content' => $item->description
        ]);
    }
}
