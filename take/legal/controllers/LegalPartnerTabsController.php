<?php

class LegalPartnerTabsController extends SIMAController
{
    public function actionFilingBook($id)
    {
        $partner = Partner::model()->findByPkWithCheck($id);
        
        $html = $this->widget(SIMAFileBrowser::class, [
            'location_code' => 'partner_filing_book',
            'default_json' => '{"name":"partneri","children":[[{"model":"DocumentType","type":"tag_group","attributes":{"show_unlisted":"true","tag_group_tree_view":"true"},"conditions":"","alias":"Tipovi dokumenta","clas":"group_table","highest_level_num":"1","children":[[{"type_column":"partner_id","type":"file_attribute","model":"Filing","attributes":{"show_unlisted":"true","null_name":""},"conditions":"","alias":"Partner","clas":"group_attribute"}]]}]],"attributes":{"show_unlisted":"false"}}',
            'init_tql' => 'FILE$private.filing_book$partner_id$'.$partner->id,
        ], true);
        $this->respondOK([
            'html' => $html
        ]);
    }
}
