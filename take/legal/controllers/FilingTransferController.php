<?php

/**
 * Controler za transfer i prikaz fajlova (Download/Upload/ZIP)
 * @package SIMA
 * @subpackage Legal
 */
class FilingTransferController extends SIMAController
{
   
    public function actionFileTransfers($id)
    {      
        if ($id == 'IN')
        {
            $title = 'Transfer ulaznih fajlova';
            $filter_scopes = 'file_transfers_in';
            $js_func = 'confirm_transfer';
            $ajax_call = 'confirmTransfer';
            $button_name = Yii::t('BaseModule.Common','Confirm');
        }
        elseif ($id == 'OUT')
        {
            $title = 'Transfer izlaznih fajlova';
            $filter_scopes = 'file_transfers_out';
            $js_func = 'cancel_transfer';
            $ajax_call = 'cancelTransfer';
            $button_name = Yii::t('BaseModule.Common','Cancel');
        }
        else
        {
            throw new Exception('SMER NE POSTOJI');
        }
        $html = $this->renderPartial('fileTransfers', array(
            'direction' => $id,
            'filter_scopes' => $filter_scopes,
            'js_func' => $js_func,
            'ajax_call' => $ajax_call,
            'button_name' => $button_name
        ), true, true);
        
        $this->respondOK([
            'html'=>$html,
            'title' => $title,
        ]);
    }
    
    public function actionListEmployeeTransferFiles($user_id, $type)
    {
        if ($type == 'IN')
        {
            $sender_id = $user_id;
            $recipient_id = Yii::app()->user->id;
        }
        else if ($type == 'OUT')
        {
            $sender_id = Yii::app()->user->id;
            $recipient_id = $user_id;
        }
        
        $html = $this->renderPartial('transferFilesList', array(
            'sender_id' => $sender_id,
            'recipient_id' => $recipient_id,            
        ), true, true);
            
        $this->respondOK([
            'html'=>$html
        ]);
    }
    
    public function actionConfirmTransfer()
    {
        $ids = $this->filter_post_input('ids', false);
        
        if(!empty($ids))
        {
            foreach ($ids as $id) 
            {
                $model = FileTransfer::model()->findByPk($id);
                if (!is_null($model))
                {
                    $model->delete();
                }
            }
        }
        
        $this->respondOK();
    }
    
    public function actionCancelTransfer()
    {
        $ids = $this->filter_post_input('ids', false);
        
        if(!empty($ids))
        {
            foreach ($ids as $id) 
            {
                $model = FileTransfer::model()->findByPk($id);
                if (is_null($model))
                {
                    throw new SIMAWarnExceptionModelNotFound('FileTransfer');
                }
                $model->cancelTransfer();
            }
        }
        
        $this->respondOK();
    }

}