<?php

class FilingBookController extends SIMAController
{
	
// 	public function accessRules()
// 	{
// 		return array(
// 				array(
// 						'allow',
// 						'actions'=>array('filingBook2_config'),
// 						'roles'=>array('secretary','filingBook')
// 				),
// 				array(
// 						'allow',
// 						'actions'=>array('FilingReserve','filingBookTab????'),
// 						'roles'=>array('modelFilingAdd') //treba dodati da se i ne prikazuje rezervacija
// 				),
// 				array(
// 						'allow',
// 						'actions'=>array('jobs'),
// 						'roles'=>array('employee')
// 				),
// 				array(
// 						'allow',
// 						'actions'=>array('fixedAssets'),
// 						'roles'=>array('Developer','records')
// 				),
// 				array('deny')
// 		);
// 	}

	
    public function actionIndex()
    {
        $uniq = SIMAHtml::uniqid();
        $title = Yii::t('LegalModule.Filing', 'FilingBook');
        
//        if(SIMAMisc::isVueComponentEnabled())
//        {
            $settings_button = Yii::app()->controller->widget(SIMAButtonVue::class, [
                'icon' => '_main-settings',
                'iconsize' => 18,
                'subactions_data' => [
                    [
                        'title' => Yii::t('LegalModule.ArchiveShelf', 'ArchiveShelfs'),
                        'onclick' => [
                            'sima.dialog.openActionInDialog', 'jobs/job/tabArchiveShelfs'
                        ]

                    ],
                    [
                        'title' => Yii::t('LegalModule.ArchiveBox', 'ArchiveBoxes'),
                        'onclick' => [
                            'sima.dialog.openActionInDialog', 'jobs/job/tabArchiveBoxes'
                        ]

                    ],
                    [
                        'title' => Yii::t('LegalModule.Registry', 'Registrators'),
                        'onclick' => [
                            'sima.dialog.openActionInDialogAsync', 'guitable', [
                                'get_params'=>[
                                    'id'=>'legal_4'
                                ]
                            ]
                        ]

                    ]
                ]
            ],true);
//        }
//        else
//        {
//            $settings_button = Yii::app()->controller->widget('SIMAButton', [
//                'action' => [
//                    'icon'=>'sima-icon _main-settings _18',
//                    'subactions' => [
//                        [
//                            'title' => Yii::t('LegalModule.ArchiveShelf', 'ArchiveShelfs'),
//                            'onclick' => [
//                                'sima.dialog.openActionInDialog', 'jobs/job/tabArchiveShelfs'
//                            ]
//
//                        ],
//                        [
//                            'title' => Yii::t('LegalModule.ArchiveBox', 'ArchiveBoxes'),
//                            'onclick' => [
//                                'sima.dialog.openActionInDialog', 'jobs/job/tabArchiveBoxes'
//                            ]
//
//                        ],
//                        [
//                            'title' => Yii::t('LegalModule.Registry', 'Registrators'),
//                            'onclick' => [
//                                'sima.dialog.openActionInDialog', 'guitable', [
//                                    'get_params'=>[
//                                        'id'=>'legal_4'
//                                    ]
//                                ]
//                            ]
//
//                        ]
//                    ]
//                 ]
//            ],true);
//        }
        
        $html = Yii::app()->controller->renderContentLayout([
            'name' => $title,
            'title_width'=>'20%',
            'options_width'=>'80%',
            'options'=>[
                [
                    'html'=> $settings_button
                ],
            ]
        ], [
            [
                'html' => $this->renderPartial('index', [
                    'uniq' => $uniq
                ], true, false)
            ]
        ], [
            'id'=>$uniq,
            'class'=>'sima-filing-book'
        ]);
        
        $this->respondOK([
            'title' => $title,
            'html' => $html
        ]);
    }
 
    public function actionReserve()
    {	
        $file = new File();
        $file->name = 'rezervisano';
        $file->responsible_id = Yii::app()->user->id;
        $file->save();

        $number_set = false;
        $filing = new Filing();	
        $filing->id = $file->id;
        $filing->file = $file;
        $filing->in = false;
        $filing->partner_id = Yii::app()->company->id;
        $filing->located_at_id = Yii::app()->user->id;
        
        while(!$number_set)
        {
            try
            {
                $filing->save();
                $number_set = true;
            } catch (Exception $e)
            {
                //ne brisati, bitan!!
                error_log(__METHOD__.' - '.'usao u catch');
            }
        }

        $filing->refresh();
        
        $this->respondOK(array(
            'filing_number'=>$filing->filing_number,
        ));
    }
    
    public function actionAddFileFilingNumberLockRequest()
    {
        $file_id = $this->filter_post_input('file_id');
        $lockKey = 'AddFileFilingNumber_'.$file_id;
        
        $lockData = CJSON::encode([
            'session_id' => Yii::app()->user->db_session->id
        ]);
        
        try
        {
            Yii::app()->interprocessMutex->Lock($lockKey, $lockData);
        }
        catch (SIMAInterprocessMutexLockFileExists $e) 
        {
            throw new SIMAWarnException(Yii::t('LegalModule.Filing', 'CanNotAddFileFilingNumber'));
        }
        
        $this->respondOK();
    }
    
    public function actionAddFileFilingNumberUnLockRequest()
    {
        $file_id = $this->filter_post_input('file_id');
        $lockKey = 'AddFileFilingNumber_'.$file_id;
        
        Yii::app()->interprocessMutex->Unlock($lockKey);
        
        $this->respondOK();
    }
}