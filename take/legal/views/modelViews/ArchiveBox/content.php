<?php $id=  SIMAHtml::uniqid();?>
<h4>Registratori:</h4>
    <?php 

    echo GuiTableController::renderEmbeddedSimple(array(
        'id' => $id,
        'Model'=> Registry::model(),
        'filter_params'=>array('archive_box_id'=>$model->id)
    ));?>

<h4>Korice:</h4>
    <?php 

    echo GuiTableController::renderEmbeddedSimple(array(
        'id' => $id,
        'Model'=> BuildingProjectSheet::model(),
        'filter_params'=>array('archive_box_id'=>$model->id)
));?>

<h4>Stavke:</h4>
    <?php 

    echo GuiTableController::renderEmbeddedSimple(array(
        'id' => $id,
        'Model'=> ArchiveItem::model(),
        'filter_params'=>array('archive_box_id'=>$model->id)
));?>