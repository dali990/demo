<h4>Potencijalna ponuda - Konkursna dokumentacija</h4>
    <p class='row partner_name'>
            <span class='title'><?php echo $model->getAttributeLabel('accepted')?>:</span> 
            <span class='data'><?php echo $model->getAttributeDisplay('accepted');?></span>
    </p>
    <p class='row partner_name'>
            <span class='title'><?php echo $model->getAttributeLabel('bid_name')?>:</span> 
            <span class='data'><?php echo $model->getAttributeDisplay('bid_name');?></span>
    </p>
    <p class='row created_date'>
            <span class='title'><?php echo $model->getAttributeLabel('created_date')?>:</span> 
            <span class='data'><?php echo $model->getAttributeDisplay('created_date');?></span>
    </p>
    <p class='row partner_name'>
            <span class='title'><?php echo $model->getAttributeLabel('orderer_by_id')?>:</span> 
            <span class='data'><?php echo $model->getAttributeDisplay('orderer_by_id');?></span>
    </p>
    <p class='row partner_name'>
            <span class='title'><?php echo $model->getAttributeLabel('job_type_id')?>:</span> 
            <span class='data'><?php echo $model->getAttributeDisplay('job_type_id');?></span>
    </p>
    <p class='row description'>
            <span class='title'><?php echo $model->getAttributeLabel('deadline')?>:</span> 
            <span class='data'><?php echo $model->getAttributeDisplay('deadline');?></span>
    </p>
    <p class='row partner_name'>
            <span class='title'><?php echo $model->getAttributeLabel('estimated_value')?>:</span> 
            <span class='data'><?php echo $model->getAttributeDisplay('estimated_value');?></span>
    </p>
    <p class='row bid_link'>
            <span class='title'><?php echo $model->getAttributeLabel('bid_link')?>:</span> 
            <span class='data'><?php echo $model->getAttributeDisplay('bid_link');?></span>
    </p>
    <p class='row partner_name'>
            <span class='title'><?php echo $model->getAttributeLabel('can_be_used_bank_statament_from_apr')?>:</span> 
            <span class='data'><?php echo $model->getAttributeDisplay('can_be_used_bank_statament_from_apr');?></span>
    </p>    
    <p class='row partner_name'>
            <span class='title'><?php echo $model->getAttributeLabel('description')?>:</span> 
            <span class='data'><?php echo $model->getAttributeDisplay('description');?></span>
    </p>
