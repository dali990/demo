
<?php $uniq = SIMAHtml::uniqid();?>

<div id='<?php echo $uniq;?>' class='basic_info'>
	
	
	<h3>
		<span class='data'><?php echo $model->DisplayName?></span>
		<span class='edit'>(<?php echo SIMAHtml::modelFormOpen($model);?>)</span>
	</h3>


	<p class='row partner_name'>
		<span class='title'><?php echo $model->getAttributeLabel('partner_id')?>:</span> 
		<span class='data'><?php echo $model->getAttributeDisplay('partner_id');?></span>
	</p>
	<p class='row partner_name'>
		<span class='title'><?php echo $model->getAttributeLabel('bid_status')?>:</span> 
		<span class='data'><?php echo $model->getAttributeDisplay('bid_status');?></span>
	</p>
	<p class='row partner_name'>
		<span class='title'><?php echo $model->getAttributeLabel('subject')?>:</span> 
		<span class='data'><?php echo $model->getAttributeDisplay('subject');?></span>
	</p>
	<p class='row partner_name'>
		<span class='title'><?php echo $model->getAttributeLabel('theme.job_type')?>:</span> 
		<span class='data'><?php echo $model->getAttributeDisplay('theme.job_type');?></span>
	</p>
	
	<p class='row partner_name'>
		<span class='title'><?php echo $model->getAttributeLabel('delivering_time')?>:</span> 
		<span class='data'><?php echo $model->getAttributeDisplay('delivering_time');?></span>
	</p>
	<p class='row partner_name'>
		<span class='title'><?php echo $model->getAttributeLabel('opening_time')?>:</span> 
		<span class='data'><?php echo $model->getAttributeDisplay('opening_time');?></span>
	</p>
	<p class='row partner_name'>
		<span class='title'><?php echo $model->getAttributeLabel('archive')?>:</span> 
		<span class='data'><?php echo $model->getAttributeDisplay('archive');?></span>
	</p>

        <p class='row'>
		<span class='title'><?php echo $model->getAttributeLabel('qualification_end')?>:</span> 
		<span class='data'><?php echo $model->getAttributeDisplay('qualification_end');?></span>
	</p>

	<p class='row add_opening_record'>
		<span class='title'><?php echo $model->getAttributeLabel('add_opening_record')?>:</span> 
		<span class='data'><?php echo $model->getAttributeDisplay('add_opening_record');?></span>
	</p>
        <p class='row add_decision_record'>
		<span class='title'><?php echo $model->getAttributeLabel('add_decision_record')?>:</span> 
		<span class='data'><?php echo $model->getAttributeDisplay('add_decision_record');?></span>
	</p>

	
</div>
