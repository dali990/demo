
<h4>
    <div class='row add_document'>
        <span class='title'><?php echo $model->getAttributeLabel('add_document')?>:</span> 
        <span class='data'>
            <?php echo $model->getAttributeDisplay('add_document');?>
            <span style="display:inline-block;">
                <?php echo $model->getAttributeDisplay('filing');?>
            </span>
        </span>            
    </div>
</h4>
<hr>
<p class='row partner_name'>
        <span class='title'><?php echo $model->getAttributeLabel('partner_id')?>:</span> 
        <span class='data'><?php echo $model->getAttributeDisplay('partner_id');?></span>
</p>
<p class='row partner_name'>
        <span class='title'><?php echo $model->getAttributeLabel('bid_status')?>:</span> 
        <span class='data'><?php echo $model->getAttributeDisplay('bid_status');?></span>
</p>
<p class='row description'>
        <span class='title'><?php echo $model->getAttributeLabel('description')?>:</span> 
        <span class='data'><?php echo $model->getAttributeDisplay('description');?></span>
</p>
<p class='row partner_name'>
        <span class='title'><?php echo $model->getAttributeLabel('theme.job_type')?>:</span> 
        <span class='data'><?php echo $model->getAttributeDisplay('theme.job_type');?></span>
</p>
<p class='row partner_name'>
        <span class='title'><?php echo $model->getAttributeLabel('delivering_time')?>:</span> 
        <span class='data'><?php echo $model->getAttributeDisplay('delivering_time');?></span>
</p>
<p class='row partner_name'>
        <span class='title'><?php echo $model->getAttributeLabel('opening_time')?>:</span> 
        <span class='data'><?php echo $model->getAttributeDisplay('opening_time');?></span>
</p>
<p class='row partner_name'>
        <span class='title'><?php echo $model->getAttributeLabel('archive')?>:</span> 
        <span class='data'><?php echo $model->getAttributeDisplay('archive');?></span>
</p>
<p class='row partner_name'>
        <span class='title'><?php echo $model->getAttributeLabel('value')?>:</span> 
        <span class='data'><?php echo $model->getAttributeDisplay('value');?></span>
</p>
<p class='row partner_name'>
        <span class='title'><?php echo $model->getAttributeLabel('essential_elements')?>:</span> 
        <span class='data'><?php echo $model->getAttributeDisplay('essential_elements');?></span>
</p>
<p class='row partner_name'>
        <span class='title'><?php echo $model->getAttributeLabel('best_bid_value')?>:</span> 
        <span class='data'><?php echo $model->getAttributeDisplay('best_bid_value');?></span>
</p>
<p class='row partner_name'>
        <span class='title'><?php echo $model->getAttributeLabel('best_bid_partner_id')?>:</span> 
        <span class='data'><?php echo $model->getAttributeDisplay('best_bid_partner_id');?></span>
</p>
<p class='row add_opening_record'>
        <span class='title'><?php echo $model->getAttributeLabel('add_opening_record')?>:</span> 
        <span class='data'><?php echo $model->getAttributeDisplay('add_opening_record');?></span>
</p>
<p class='row add_decision_record'>
        <span class='title'><?php echo $model->getAttributeLabel('add_decision_record')?>:</span> 
        <span class='data'><?php echo $model->getAttributeDisplay('add_decision_record');?></span>
</p>
<?php
    $count = 0;
    foreach ($model->bid_company_licenses as $bid_company_license) 
    {
        ?>
        <p class='row'>
            <span class='title'><?php echo $count===0?'Velike licence:':''; $count++; ?></span>
            <span class='data'>
                <?php
                    echo SIMAHtml::modelDelete($bid_company_license).$bid_company_license->company_license->company_license->DisplayHtml;
                ?>
            </span>
        </p>
        <?php
    }
?>
<?php
    $count = 0;
    foreach ($model->bid_work_licenses as $bid_work_license) 
    {                
        ?>
        <p class='row'>
            <span class='title'><?php echo $count===0?'Radne licence:':''; $count++; ?></span>
            <span class='data'>
                <?php
                    echo SIMAHtml::modelDelete($bid_work_license).$bid_work_license->work_license->DisplayHtml;
                ?>
            </span>
        </p>
        <?php
    }
?>
    