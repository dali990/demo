<?php
    $this->widget(SIMAButtonVue::class, [
        'title' => Yii::t('LegalModule.Bid', 'PdfSuggestion'),
        'onclick' => ['sima.legal.generateBidReport', $model->id]
    ]);
