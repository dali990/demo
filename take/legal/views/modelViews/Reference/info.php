
<div class="basic_info">
    <p class='row description'>
            <span class='title'><?php echo $model->getAttributeLabel('description')?>:</span> 
            <span class='data'><?php echo $model->getAttributeDisplay('description');?></span>
    </p>
    <p class='row year'>
            <span class='title'><?php echo $model->getAttributeLabel('year')?>:</span> 
            <span class='data'><?php echo $model->getAttributeDisplay('year');?></span>
    </p>
    <p class='row add_file'>
            <span class='title'><?php echo $model->getAttributeLabel('add_file')?>:</span> 
            <span class='data'><?php echo $model->getAttributeDisplay('add_file');?></span>
    </p>
    <?php
        $count = 0;
        if (count($model->reference_to_company_license) > 0)
        {
            foreach ($model->reference_to_company_license as $reference_company_license) 
            {
                ?>
                <p class='row'>
                    <span class='title'><?php echo $count===0?$model->getAttributeLabel('add_company_license').$model->getAttributeDisplay('add_company_license'):''; $count++; ?></span>
                    <?php $display_name = $reference_company_license->company_license->DisplayName; ?>
                    <span class='data sima-text-dots' style="width: 100%;" title="<?php echo $display_name; ?>">
                        <?php
                            echo SIMAHtml::modelDelete($reference_company_license).$display_name;
                        ?>
                    </span>
                </p>
                <?php
            }
        }
        else
        {
            ?>
                <p class='row'>
                    <span class='title'><?php echo $model->getAttributeLabel('add_company_license').$model->getAttributeDisplay('add_company_license'); ?></span>                     
                </p>
            <?php
        }
        $count = 0;
        if (count($model->reference_to_person_license) > 0)
        {
            foreach ($model->reference_to_person_license as $reference_person_license) 
            {
                ?>
                <p class='row'>
                    <span class='title'><?php echo $count===0?$model->getAttributeLabel('add_work_license').$model->getAttributeDisplay('add_work_license'):''; $count++; ?></span>
                    <?php $display_name = $reference_person_license->person_license->DisplayName; ?>
                    <span class='data sima-text-dots' style="width: 100%;" title="<?php echo $display_name; ?>">
                        <?php
                            echo SIMAHtml::modelDelete($reference_person_license).$display_name;
                        ?>
                    </span>
                </p>
                <?php
            }
        }
        else
        {
            ?>
                <p class='row'>
                    <span class='title'><?php echo $model->getAttributeLabel('add_work_license').$model->getAttributeDisplay('add_work_license'); ?></span>                     
                </p>
            <?php
        }
    ?>
</div>