
<h3 class='sima-bi-info_heading'>Zavodna knjiga</h3>

	
	<div class='sima-bi-row'>
		<div><?php echo $model->getAttributeLabel('confirmed')?>:</div> 
                <div><?php echo SIMAHtml::status($model, 'confirmed'); ?></div>
	</div>
	<div class='sima-bi-row'>
		<div><?php echo $model->getAttributeLabel('returned')?>:</div> 
		<div><?php echo SIMAHtml::status($model, 'returned');?></div>
	</div>
	<div class='sima-bi-row'>
		
		<div><?php echo $model->getAttributeLabel('filing_number')?>:</div> 
                <div><?php echo $model->DisplayNameWithDate ?></div>
	</div>
	
	<div class='sima-bi-row'>
		<div><?php echo $model->getAttributeLabel('partner_id')?>:</div> 
		<div><?php echo $model->getAttributeDisplay('partner_id');?></div>
	</div>
	
	<div class='sima-bi-row'>
		<div><?php echo $model->getAttributeLabel('delivery_types')?>:</div> 
		<div><?php echo $model->getAttributeDisplay('delivery_types');?></div>
	</div>
	<div class='sima-bi-row'>
		<div><?php echo $model->getAttributeLabel('registry_id')?>:</div> 
		<div><?php echo $model->getAttributeDisplay('registry_id');?></div>
	</div>
        <div class='sima-bi-row'>
		<div><?php echo $model->getAttributeLabel('located_at_id')?>:</div> 
		<div><?php echo $located_at_display?></div>
	</div>
	<div class='sima-bi-row'>
		<div><?php echo $model->getAttributeLabel('creator_id')?>:</div> 
		<div><?php echo $model->getAttributeDisplay('creator_id');?></div>
	</div>
	<div class='sima-bi-row'>
		<div><?php echo $model->getAttributeLabel('comment')?>:</div> 
		<div><?php echo $model->getAttributeDisplay('comment');?></div>
	</div>