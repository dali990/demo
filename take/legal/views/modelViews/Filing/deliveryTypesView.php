<?php  
    $result = [];
    foreach ($model->delivery_types as $type) 
    {
        $result[] = $type->DisplayName;
    }
    $res = implode(' | ', $result);
    
    $email_id = null;
    if (isset($model->file->email))
    {
        $email_id = $model->file->email->id;
    }
    else if (isset($model->file->last_version->email_attachment))
    {
        $email_id = $model->file->last_version->email_attachment->getEmail()->id;
    }
    if (!is_null($email_id))
    {
        if(SIMAMisc::isVueComponentEnabled())
        {
            $res .= Yii::app()->controller->widget(SIMAButtonVue::class, [
                'title'=>'Otvorite email',
                'onclick'=>['sima.messagesMisc.viewEmailFromSystem', $email_id]
            ],true);
        }
        else
        {
            $res .= Yii::app()->controller->widget('SIMAButton', [
                'action'=>[ 
                    'title'=>'Otvorite email',
                    'onclick'=>['sima.messagesMisc.viewEmailFromSystem', $email_id]
                ]
            ],true);
        }
    }
    
    echo $res;
?>