<h3><?php echo Yii::t('LegalModule.InsurancePolicy', 'InsurancePolicy') ?></h3>
<p class='row'>
    <span class='title'><?php echo $model->getAttributeLabel('policy_number')?>:</span> 
    <span class='data'><?php echo $model->getAttributeDisplay('policy_number')?></span>    
</p>
<p class='row'>
    <span class='title'><?php echo $model->getAttributeLabel('premium_amount')?>:</span> 
    <span class='data'><?php echo $model->getAttributeDisplay('premium_amount')?></span>    
</p>
<p class='row'>
    <span class='title'><?php echo $model->getAttributeLabel('start_date')?>:</span> 
    <span class='data'><?php echo $model->getAttributeDisplay('start_date')?></span>  
</p>
<p class='row'>
    <span class='title'><?php echo $model->getAttributeLabel('expire_date')?>:</span> 
    <span class='data'><?php echo $model->getAttributeDisplay('expire_date')?></span>    
</p>
<p class='row'>
    <span class='title'><?php echo $model->getAttributeLabel('insurance_company')?>:</span> 
    <span class='data'><?php echo $model->getAttributeDisplay('insurance_company')?></span>    
</p>
<p class='row'>
    <span class='title'><?php echo $model->getAttributeLabel('vinkulation_partner')?>:</span> 
    <span class='data'><?php echo $model->getAttributeDisplay('vinkulation_partner')?></span>    
</p>
<p class='row'>
    <span class='title'><?php echo $model->getAttributeLabel('bill_in')?>:</span> 
    <span class='data'><?php echo $model->getAttributeDisplay('bill_in')?></span>    
</p>
