<div style="margin: 10px;">
    <?php
    if (isset($active_annual_leave_decisions) && $active_annual_leave_decisions!=null) {
        foreach ($active_annual_leave_decisions as $active_annual_leave_decision) {
            $remaining_days = $active_annual_leave_decision->getRemainingDays();
            if (intval($remaining_days) > 0) {
                echo Yii::t('LegalModule.Legal', 'ByDecision')." <strong>'" . $active_annual_leave_decision->DisplayName . "'</strong> ".Yii::t('LegalModule.Legal', 'AnnualLeavesDaysLeft')." " .
                $remaining_days . "<br />";
            }
        }
    } else {
        echo Yii::t('Person','NoStatisticsOnEmployment');
    }
    ?>
</div>
