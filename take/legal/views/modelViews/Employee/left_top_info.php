
<?php 
    $partner = $model->person->partner;
?>

<!-- LEVI gornji deo full info ZAPOSLENI-->
<div style="padding:20px">
    <div class="row">
        <div >
            <img class="sima-bi-radius-photo" title='<?php echo $model->DisplayName ?>' src='<?php echo $model->person->getAttributeDisplay('image_id'); ?>'width="100" height="100" />
            <div style="display: inline-block;
                        padding-left: 20px;">
                <h2 class='data'> 
                    <?php
                    echo $model->columnDisplays('firstname') . " ";
                    echo $model->columnDisplays('lastname');
                    ?>
                </h2>
                <span class='data'> 
                    <?php
                    echo $model->person->getAttributeLabel('company') . ": ";
                    echo $model->person->getAttributeDisplay('company');
                    ?>
                </span></br>
                <span class="data">
                    <?php
                    echo $model->person->getAttributeLabel('work_positions') . ": ";
                    echo $model->person->getAttributeDisplay('work_positions');
                    ?>
                </span></br>
            </div>

        </div> 
    </div>
    <?php if (Yii::app()->user->checkAccess('HRWorkContractRead')) {?>
    <div style="padding:20px;">
        <div>
            <h3><?php echo Yii::t('Person', 'BasicInfo'); ?>:</h3>

            <!--telefoni-->
            <?php 
                $phone_numbers = $partner->phone_numbers;
                if (!empty($phone_numbers))
                {
            ?>
                <p><?=Yii::t('Partner', 'Phones')?>:</p>
                <ul>
                    <?php 
                        foreach ($phone_numbers as $phone_number)
                        {
                            $comment = '';
                            if(!empty($phone_number->comment))
                            {
                                $comment = " - {$phone_number->comment}";
                            }
                            echo "<li>{$phone_number->DisplayName}{$comment}</li>";
                        }
                    ?>
                </ul>
            <?php
                }
            ?>
                
            <!--email adrese-->
            <?php 
                $email_addresses = $partner->email_addresses;
                if (!empty($email_addresses))
                {
            ?>
                <p><?=Yii::t('Partner', 'EmailAddresses')?>:</p>
                <ul>
                    <?php 
                        foreach ($email_addresses as $email_address)
                        {
                            $comment = '';
                            if(!empty($email_address->comment))
                            {
                                $comment = " - {$email_address->comment}";
                            }
                            echo "<li>{$email_address->DisplayName}{$comment}</li>";
                        }
                    ?>
                </ul>
            <?php
                }
            ?>
                
            <!--kontakti-->
            <?php 
                $contacts = $model->person->contacts;
                if (!empty($contacts))
                {
            ?>
                <p><?=Yii::t('Partner', 'OtherContacts')?>:</p>
                <ul>
                    <?php 
                        foreach ($contacts as $contact)
                        {
                            echo "<li>{$contact->DisplayName}</li>";
                        }
                    ?>
                </ul>
            <?php
                }
            ?>
            
            <!--adrese-->
            <p><?=Yii::t('Person', 'Addresses')?>:
                <?php
                echo "<ul>";
                if ($model->person->addresses != null) {
                    $addreses = $model->person->addresses;
                    foreach ($addreses as $addr) {
                        echo "<li>" . $addr . "</li>";
                    }
                } else {
                    echo "<li>" . Yii::t('Person', 'DoesNotHaveAddr') . "</li>";
                }
                echo "</ul>"
            ?>

        </div>
        <!-- place for map -->
        <h3><?php echo Yii::t('Person','StatisticsOfEmployment'); ?>:</h3>
        <div>
            <h4><?php echo Yii::t('Person','ActiveAnnualLeaveDecision'); ?>:</h4>
            <?php
                echo Yii::app()->controller->renderModelView($model, 'active_annual_leave_decision');
            ?>
            <h4><?php echo Yii::t('Person','LastAbsence'); ?>: </h4>
            <?php
            echo "<ul>";
            if (isset($absences) && $absences != null) {
                $filteredAbsences = $absences;
                $html="";
                foreach ($filteredAbsences as $singleAbsence) {
                    $html .= '<l1>';
                    $html .= Yii::t('BaseModule.Common','from').' ' . $singleAbsence->begin;
                    $html .= ' '.Yii::t('BaseModule.Common','to').' ' . $singleAbsence->end;
                    $html .= '</li></br>';
                    echo $html;
                    $html = ''; 
                }
            } else {
                echo "<li>".Yii::t('Person','DoNotHaveAbsence')."</li>"; 
            }
            echo "</ul>";
            ?>
        </div>
    </div>
    <?php } ?>


</div>
