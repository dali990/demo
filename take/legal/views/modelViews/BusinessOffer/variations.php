<?php 
    $uniq = SIMAHtml::uniqid();
    $this->widget(SIMAGuiTable::class, [
        'id' => 'business_offer_variations_'.$uniq,
        'model'=> BusinessOfferVariation::class,            
        'columns_type' => 'from_business_offer',
        'title' => Yii::t('LegalModule.BusinessOfferVariation', 'BusinessOfferVariations'),
        'add_button' => [
            'init_data' => [
                'BusinessOfferVariation' => [
                    'business_offer_id' => [
                        'disabled',
                        'init' => $model->id
                    ]
                ]
            ],
            'onhover' => Yii::t('LegalModule.BusinessOfferVariation', 'AddBusinessOfferVariation')
        ],
        'fixed_filter' => [
            'display_scopes' => 'byTimeDesc',
            'business_offer' => [
                'ids' => $model->id
            ]
        ]
    ]);
