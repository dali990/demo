<div class='basic_info'>
    <p class='row'>
        <span class='title'><?=$model->getAttributeLabel('offer_number')?>:</span> 
        <span class='data'><?=$model->getAttributeDisplay('offer_number')?></span>
    </p>
    <p class='row'>
        <span class='title'><?=$model->getAttributeLabel('name')?>:</span> 
        <span class='data'><?=$model->getAttributeDisplay('name')?></span>
    </p>
    <p class='row'>
        <span class='title'><?=$model->getAttributeLabel('partner')?>:</span> 
        <span class='data'><?=$model->getAttributeDisplay('partner')?></span>
    </p>
    <p class='row'>
        <span class='title'><?=$model->getAttributeLabel('status')?>:</span> 
        <span class='data'><?=$model->getAttributeDisplay('status')?></span>
    </p>
    <p class='row'>
        <span class='title'><?=$model->getAttributeLabel('value')?>:</span> 
        <span class='data'><?=$model->getAttributeDisplay('value').' '.$model->getAttributeDisplay('currency')?></span>
    </p>
    <?php if (!empty($model->business_offer_variation)) { ?>
        <p class='row'>
            <span class='title'><?=Yii::t('LegalModule.BusinessOffer', 'AcceptedBusinessOfferVariation')?>:</span> 
            <span class='data'><?php
                echo $model->business_offer_variation->getAttributeDisplay('statuses') . ' ' . $model->business_offer_variation->DisplayHtml;
            ?></span>
        </p>
    <?php } ?>
    <p class='row'>
        <span class='title'><?=$model->getAttributeLabel('description')?>:</span> 
        <span class='data'><?=$model->getAttributeDisplay('description')?></span>
    </p>
    <p class='row'>
        <span class='title'><?=$model->getAttributeLabel('theme')?>:</span> 
        <span class='data'><?=$model->getAttributeDisplay('theme')?></span>
    </p>
    <p class='row'>
        <span class='title'><?=$model->getAttributeLabel('created_by_user')?>:</span> 
        <span class='data'><?=$model->getAttributeDisplay('created_by_user')?></span>
    </p>
    <p class='row'>
        <span class='title'><?=$model->getAttributeLabel('created_timestamp')?>:</span> 
        <span class='data'><?=$model->getAttributeDisplay('created_timestamp')?></span>
    </p>
</div>