<?php

if (empty($model->business_offer_variation_id))
{
    echo $this->renderModelView($model, 'variations', ['class' => 'sima-layout-panel']);
}
else
{
    $this->widget('SIMAFilePreview', [
        'file' => $model->business_offer_variation->file
    ]);
}