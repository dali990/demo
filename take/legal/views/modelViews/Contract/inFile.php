
	
	<?php if (isset($model->contract_type)) echo "<h3>Tip:<br />".$model->contract_type->DisplayName.SIMAHtml::modelFormOpen($model)."</h3>";
			else echo "<h3>Nije upisan tip ugovora".SIMAHtml::modelFormOpen($model)."</h3>";?>
	
	<?php if (isset($model->parent)){?>
		<p class='row parent_id'>
			<span class='title'><?php echo $model->getAttributeLabel('parent_id')?>:</span> 
			<span class='data'><?php echo $model->getAttributeDisplay('parent_id');?></span>
		</p>
	<?php }?>
	<p class='row partner_id'>
		<span class='title'><?php echo $model->getAttributeLabel('partner_id')?>:</span> 
		<span class='data'><?php echo $model->getAttributeDisplay('partner_id');?></span>
	</p>
	<p class='row partner_filing_number'>
		<span class='title'><?php echo $model->getAttributeLabel('partner_filing_number')?>:</span> 
		<span class='data'><?php echo $model->getAttributeDisplay('partner_filing_number');?> od <?php echo $model->getAttributeDisplay('partner_filing_date');?></span>
	</p>
	<p class='row sign_date'>
		<span class='title'><?php echo $model->getAttributeLabel('sign_date')?>:</span> 
		<span class='data'><?php echo $model->getAttributeDisplay('sign_date');?></span>
	</p>
	<p class='row deadline'>
		<span class='title'><?php echo $model->getAttributeLabel('deadline')?>:</span> 
		<span class='data'><?php echo $model->getAttributeDisplay('deadline');?></span>
		<span class='link' <?php if (!isset($model->deadline_description) || $model->deadline_description=='') echo "style='display: none;'"?> 
			onclick='$(this).parent().siblings(".deadline_description").toggle(200);'>
			opisno
		</span>
	</p>
	<p class='row deadline_description' style='display: none;'>
		<span class='title'><?php echo $model->getAttributeLabel('deadline_description')?>:</span> 
		<span class='data'><?php echo $model->getAttributeDisplay('deadline_description');?></span>
	</p>
	<p class='row value'>
		<span class='title'><?php echo $model->getAttributeLabel('value')?>:</span> 
		<span class='data'><?php echo $model->getAttributeDisplay('value');?></span>
		<span class='link' <?php if (!isset($model->value_text) || $model->value_text=='') echo "style='display: none;'"?> 
			onclick='$(this).parent().siblings(".value_text").toggle(200);'>
			opisno
		</span>
	</p>
	<p class='row value_text' style='display: none;'>
		<span class='title'><?php echo $model->getAttributeLabel('value_text')?>:</span> 
		<span class='data'><?php echo $model->getAttributeDisplay('value_text');?></span>
	</p>
	<p class='row vat'>
		<span class='title'><?php echo $model->getAttributeLabel('vat')?>:</span> 
		<span class='data'><?php echo $model->getAttributeDisplay('vat');?></span>
		<span class='link' <?php if (!isset($model->vat_text) || $model->vat_text=='') echo "style='display: none;'"?> 
			onclick='$(this).parent().siblings(".vat_text").toggle(200);'>
			opisno
		</span>
	</p>
	<p class='row vat_text' style='display: none;'>
		<span class='title'><?php echo $model->getAttributeLabel('vat_text')?>:</span> 
		<span class='data'><?php echo $model->getAttributeDisplay('vat_text');?></span>
	</p>
	<p class='row advance'>
		<span class='title'><?php echo $model->getAttributeLabel('advance')?>:</span> 
		<span class='data'><?php echo $model->getAttributeDisplay('advance');?></span>
		<span class='link' <?php if (!isset($model->advance_text) || $model->advance_text=='') echo "style='display: none;'"?> 
			onclick='$(this).parent().siblings(".advance_text").toggle(200);'>
			opisno
		</span>
	</p>
	<p class='row advance_text' style='display: none;'>
		<span class='title'><?php echo $model->getAttributeLabel('advance_text')?>:</span> 
		<span class='data'><?php echo $model->getAttributeDisplay('advance_text');?></span>
	</p>
	<p class='row registry_id'>
		<span class='title'><?php echo $model->getAttributeLabel('registry_id')?>:</span> 
		<span class='data'><?php echo $model->getAttributeDisplay('registry_id');?></span>
	</p>
	

