<?php 
    $uniq = SIMAHtml::uniqid();
    $this->widget(SIMAGuiTable::class, [
        'id' => 'business_offer_variation_items_'.$uniq,
        'model'=> BusinessOfferVariationItem::class,            
        'columns_type' => 'from_business_offer_variation',
        'title' => Yii::t('LegalModule.BusinessOfferVariationItem', 'BusinessOfferVariationItems'),
        'add_button' => $model->confirmed ? false : [
            'init_data' => [
                'BusinessOfferVariationItem' => [
                    'business_offer_variation_id' => [
                        'disabled',
                        'init' => $model->id
                    ]
                ]
            ],
            'onhover' => Yii::t('LegalModule.BusinessOfferVariationItem', 'AddBusinessOfferVariationItem')
        ],
        'fixed_filter' => [
            'business_offer_variation' => [
                'ids' => $model->id
            ]               
        ]
    ]);