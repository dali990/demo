<div class='basic_info'>
    <p class='row'>
        <span class='title'><?=$model->getAttributeLabel('order_number')?>:</span> 
        <span class='data'><?=$model->getAttributeDisplay('order_number')?></span>
    </p>
    <p class='row'>
        <span class='title'><?=$model->getAttributeLabel('name')?>:</span> 
        <span class='data'><?=$model->getAttributeDisplay('name')?></span>
    </p>
    <p class='row'>
        <span class='title'><?=$model->getAttributeLabel('value')?>:</span> 
        <span class='data'><?=$model->getAttributeDisplay('value').' '.$model->business_offer->getAttributeDisplay('currency')?></span>
    </p>
    <p class='row'>
        <span class='title'><?=$model->getAttributeLabel('confirmed')?>:</span> 
        <span class='data'><?=$model->getAttributeDisplay('confirmed')?></span>
    </p>
    <p class='row'>
        <span class='title'><?=$model->getAttributeLabel('sent')?>:</span> 
        <span class='data'><?=$model->getAttributeDisplay('sent')?></span>
    </p>
    <p class='row'>
        <span class='title'><?=$model->getAttributeLabel('description')?>:</span> 
        <span class='data'><?=$model->getAttributeDisplay('description')?></span>
    </p>
    <p class='row'>
        <span class='title'><?=$model->getAttributeLabel('file')?>:</span> 
        <span class='data'><?=$model->getAttributeDisplay('file')?></span>
    </p>
    <p class='row'>
        <span class='title'><?=$model->getAttributeLabel('business_offer')?>:</span> 
        <span class='data'><?=$model->getAttributeDisplay('business_offer')?></span>
    </p>
    <p class='row'>
        <span class='title'><?=$model->getAttributeLabel('created_by_user')?>:</span> 
        <span class='data'><?=$model->getAttributeDisplay('created_by_user')?></span>
    </p>
    <p class='row'>
        <span class='title'><?=$model->getAttributeLabel('created_timestamp')?>:</span> 
        <span class='data'><?=$model->getAttributeDisplay('created_timestamp')?></span>
    </p>
    <p class='row'>
        <span class='title'><?=$model->getAttributeLabel('copy')?>:</span> 
        <span class='data'><?=$model->getAttributeDisplay('copy')?></span>
    </p>
</div>