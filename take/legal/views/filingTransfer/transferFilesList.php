<?php
    if (Yii::app()->request->isAjaxRequest) Yii::app()->clientScript->clearScriptFiles();
?>
<?php $uniq = SIMAHtml::uniqid();?>
<script type='text/javascript'>
    function select_file<?php echo $uniq; ?>(obj)
    {
        var selected = $('#'+'<?php echo $uniq; ?>').simaGuiTable('getSelected');
        if (selected.length > 0)
        {
            var button = obj.parents('.files_list:first').find('.confirm_transfer');
            if (button.length > 0)
            {
                button.removeClass('_disabled');
            }
            else
            {
                obj.parents('.files_list:first').find('.cancel_transfer').removeClass('_disabled');
            }
        }
        else
        {
            var button = obj.parents('.files_list:first').find('.confirm_transfer');
            if (button.length > 0)
            {
                button.addClass('_disabled');
            }
            else
            {
                obj.parents('.files_list:first').find('.cancel_transfer').addClass('_disabled');
            }
        }
    }
</script>

<?php
    $this->widget('base.extensions.SIMAGuiTable.SIMAGuiTable', [
        'id' => $uniq,
        'model'=>  FileTransfer::model(),        
        'fixed_filter'=>array(
            'sender_id'=>$sender_id,
            'recipient_id' => $recipient_id
        ),
        'setRowSelect' => "select_file$uniq",
        'setRowUnSelect' => "unselect_file$uniq"
    ]);
?>
<?php
    if (Yii::app()->request->isAjaxRequest) Yii::app()->clientScript->clearScriptFiles();
?>
<script type='text/javascript'>
    function unselect_file<?php echo $uniq; ?>(obj)
    {
        select_file<?php echo $uniq; ?>(obj);
    }
</script>