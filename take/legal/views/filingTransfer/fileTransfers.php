<?php
    if (Yii::app()->request->isAjaxRequest) Yii::app()->clientScript->clearScriptFiles();
?>
<?php $uniq = SIMAHtml::uniqid();?>
<script type='text/javascript'>
    function select_employee<?php echo $uniq; ?>(obj)
    {        
        var selected = $('#employees'+'<?php echo $uniq; ?>').simaGuiTable('getSelected');
        if (selected.length === 1)
        {
            sima.ajax.get('legal/filingTransfer/listEmployeeTransferFiles', {
                get_params: {
                    user_id: obj.attr('model_id'),
                    type:'<?php echo $direction?>'
                },
                async:true,
                loadingCircle:[$('#<?php echo $uniq; ?>'+'_panel').find('.files_list')],
                success_function: function(response) {
                    sima.set_html($('#<?php echo $uniq; ?>'+'_panel').find('.files_list .files'), response.html);
                }
            });
        }
        else
        {
            $('#<?php echo $uniq; ?>'+'_panel').find('.files_list .files').html('');
        }
    }
</script>

<div id='<?=$uniq.'_panel'?>' class="sima-layout-panel _vertical _splitter sima-ui-file-transfer">

    <div class="sima-layout-panel">
        <?php
            $this->widget('base.extensions.SIMAGuiTable.SIMAGuiTable', [
                'id'=>'employees'.$uniq,
                'model'=> User::model(),                
                'fixed_filter'=>array(
                    'filter_scopes'=>$filter_scopes,
                ),
                'columns_type'=>'file_transfers',
                'setRowSelect' => "select_employee$uniq",
                'setRowUnSelect' => "unselect_employee$uniq"
            ]);
        ?>
    </div>
    <div class="files sima-layout-panel">
        <div class="sima-layout-panel files_list _horizontal">
            <div class="options sima-layout-fixed-panel" style="padding: 20px; height: 50px;">
                <button class='sima-ui-button round <?php echo $js_func?> _disabled' onclick="<?php echo $js_func.$uniq; ?>($(this))" title='Potvrdi'><?php echo $button_name?></button>
            </div>
            <div class="files sima-layout-panel"></div>
        </div>
    </div>

</div>

<script>
    function <?php echo $js_func.$uniq; ?>(obj)
    {
        if (!obj.hasClass('_disabled'))
        {
            var table = $('#<?php echo $uniq; ?>'+'_panel').find('.files_list .sima-guitable:first');
            var files = table.simaGuiTable('getSelected');
            var ids = [];
            files.each(function(){
                ids.push($(this).attr('model_id'));
            });
            
            sima.ajax.get('legal/filingTransfer/<?php echo $ajax_call; ?>', {
                data:{ids:ids},
                success_function: function(response) {
                    obj.addClass('_disabled');
                }
            });
        }
    }
    function unselect_employee<?php echo $uniq; ?>(obj)
    {
        select_employee<?php echo $uniq; ?>(obj);
    }
</script>