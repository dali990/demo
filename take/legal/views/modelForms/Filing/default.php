
<?php 
    $uniqidContribIds = SIMAHtml::uniqid();
	/**
	 * privilegije
	 */
    $real_model = $model::model()->findByPk($model->id);
    if (empty($real_model))
    {
        $real_model = $model;
    }
    if (empty($real_model))
    {
        throw new Exception('PRAZAN JE MODEL');
    }
    $access_located_at          = Yii::app()->user->checkAccess('LocatedAt',[],$real_model);
    $access_subnumber           = Yii::app()->user->checkAccess('Subnumber',[],$real_model);
    $access_confirm             = Yii::app()->user->checkAccess('Confirm',[],$real_model);
    $access_return              = Yii::app()->user->checkAccess('Return',[],$real_model);
    $access_unconfirmed_edit    = Yii::app()->user->checkAccess('UnconfirmedEdit', [],$real_model);
    $access_unreturned_edit     = Yii::app()->user->checkAccess('UnreturnedEdit', [], $real_model);
    
?>

	
	<div id="<?php echo $form_id?>" class="sima-model-form">
		<h2></h2>
		<?php $form=$this->beginWidget('CActiveForm', array(
				'id'=>SIMAHtml::uniqid(),
				'enableClientValidation'=>true,
		)); ?>

                <?php echo $form->hiddenField($model, 'old_file_belongs', array('value'=>CJSON::encode(isset($model->file)?$model->file->file_belongs:array()))); ?>
                
                <div class="sima-model-form-row number">                    
			<?php echo $form->labelEx($model,'number'); ?>
			<?php echo $form->textField($model,'number',array('placeholder'=>'?????','disabled'=>'disabled')); ?>/<?php echo $mysqldate = date( 'y');?>/<span id='filing_number_io'><?php   if (gettype($model->in) === 'NULL') 
                                    echo 'A'; 
                                else 
                                    echo ($model->in)?'U':'I'; 
                        ?></span>
                        <?php echo $form->error($model,'number'); ?>
		</div>
                
                <?php if ($access_subnumber){?>
                    <div class="sima-model-form-row subnumber" >
                            <?php echo $form->labelEx($model,'subnumber'); ?>
                            <?php echo $form->textField($model,'subnumber',array('disabled'=>'disabled')); ?>
                            <?php echo $form->error($model,'subnumber'); ?>
                    </div>
                <?php }?>
                
                <div class="sima-model-form-row">
			<?php if ($model->getIsNewRecord()) $model->date = date( 'd.m.Y');?>
			<?php echo $form->labelEx($model,'date'); ?>
			<?php echo SIMAHtml::datetimeField($model, 'date',array('htmlOptions'=>array('disabled'=>'disabled'))); ?>
			<?php echo $form->error($model,'date'); ?>
		</div>
                
 		<div class="sima-model-form-row">
			<?php echo $form->labelEx($model,'confirmed'); ?>
			<?php echo SIMAHtml::status($model, 'confirmed', 16, false, $status) ?>
			<?php echo $form->error($model,'confirmed'); ?>
		</div>
                
                <div class="sima-model-form-row">
			<?php echo $form->labelEx($model,'in'); ?>
			<?php
                            $default_selected = '';
                            if (gettype($model->in) === 'NULL') 
                            {
                                 $default_selected = 'null';
                            }
                            else 
                            {
                                $default_selected = ($model->in)?'1':'0'; 
                            }
                        ?>
                        <?php $htmlOptions = array('onchange'=>"sima.legal.filingIOChange($('#$form_id'))",'options'=>array($default_selected=>array('selected'=>true))); ?>
			<?php if (!$access_unconfirmed_edit) $htmlOptions['disabled'] = 'disabled'; ?>
			<?php echo $form->dropDownList($model,'in',$model->droplist('in'),$htmlOptions); ?>
			<?php echo $form->error($model,'in'); ?>
		</div>
                
                <div class="sima-model-form-row">
                    <?php
                        $partner_searchfield_options = [
                            'model' => $model, 
                            'relation' => 'partner'
                        ];
                        $partner_add_btn_html_options = [];
                        if (!$access_unconfirmed_edit) 
                        {
                            $partner_searchfield_options['htmlOptions'] = ['disabled' => 'disabled'];
                            $partner_add_btn_html_options['class'] = '_disabled';
                        }
                        
                        echo $form->labelEx($model, 'partner_id');
                        $this->widget('ext.SIMASearchField.SIMASearchField', $partner_searchfield_options);
                        echo SIMAHtml::openActionInDialog('simaAddressbook/index', [], $partner_add_btn_html_options);
                        echo $form->error($model, 'partner_id'); 
                    ?>
		</div>
                
                <?php if (false){//izbacuje se iz upotrebe?>
                    <div class="sima-model-form-row">
                            <?php echo $form->labelEx($model,'delivery_type_id'); ?>
                            <?php $htmlOptions = array('empty'=>'Izaberi'); ?>
                            <?php if (!$access_unconfirmed_edit) $htmlOptions['disabled'] = 'disabled'; ?>
                            <?php echo $form->dropDownList($model,'delivery_type_id',$model->droplist('delivery_type_id'), $htmlOptions); ?>
                            <?php echo $form->error($model,'delivery_type_id'); ?>
                    </div>
                <?php }?>
                
                <div class="sima-model-form-row <?=Yii::app()->docWiki->isInstalled() ? 'has_wiki' : ''?>">
                        <?php if(Yii::app()->docWiki->isInstalled()) {?>
                            <?php $wiki_page = 'SIMA_uputstvo_-_Način_dostave'; ?>
                            <span style='position:relative; z-index:5; margin-right:5px;vertical-align:top;' class='sima-icon _wiki _16' onclick='sima.icons.docWiki($(this),"<?php echo $wiki_page; ?>");'></span>
                        <?php } ?>
			<?php echo $form->labelEx($model,'delivery_types'); ?>
                        <?php
                            echo SIMAHtml::searchFormList(array(
                                'base_model'=>$model,
                                'search_model'=>'DeliveryType',
                                'relName'=>'filing_to_delivery_types',
                                'disabled' => !$access_unconfirmed_edit
                            ));
                         ?>
			<?php echo $form->error($model,'delivery_types'); ?>
		</div>
		
                <div class='sima-model-form-row'>
			<?php echo $form->labelEx($model,'contrib_ids'); ?>
                            <?php  
                                echo SIMAHtml::searchFormList(array(
                                    'base_model'=>$model,
                                    'search_model'=>'File',
                                    'relName'=>'contribs',
                                    'view'=>'filing_contribs',
                                    'multiselect'=>true
                                ));
                            ?>
		</div>
		
		<div class="sima-model-form-row">
                    <?php echo $form->labelEx($model,'returned'); ?>
                    <?php echo SIMAHtml::status($model, 'returned', 16, false, $status); ?>
                    <?php echo $form->error($model,'returned'); ?>
		</div>
                
                <div class="sima-model-form-row <?=Yii::app()->docWiki->isInstalled() ? 'has_wiki' : ''?>">
                        <?php if(Yii::app()->docWiki->isInstalled()) {?>
                            <?php $wiki_page = 'SIMA_uputstvo_-_Način_dostave'; ?>
                            <span style='position:relative; z-index:5; margin-right:5px;vertical-align:top;' class='sima-icon _wiki _16' onclick='sima.icons.docWiki($(this),"<?php echo $wiki_page; ?>");'></span>
                        <?php } ?>
                        <?php echo $form->labelEx($model, 'located_at_id'); ?>
			<?php $options = array('model'=>$model, 'relation'=>'located_at'); ?>
			<?php if (!$access_located_at) $options['htmlOptions'] = array('disabled'=>'disabled'); ?>
			<?php $this->widget('ext.SIMASearchField.SIMASearchField', $options);?>
			<?php echo $form->error($model, 'located_at_id'); ?>
		</div>
                
                <div class="sima-model-form-row">
			<?php echo $form->labelEx($model,'registry_id'); ?>
			<?php $htmlOptions = array('empty'=>'Izaberi'); ?>
			<?php if (!$access_located_at) $htmlOptions['disabled'] = 'disabled'; ?>
                    <?php 
                        if ($model->registry_id === null)
                        {
                            $model_filter = [
                                'archived'=>false
                            ];
                        }
                        else
                        {
                            $model_filter = [
                                'OR',
                                ['ids' => [$model->registry_id]],
                                ['archived'=>false]
                            ];
                        }
                    ?>
			<?php echo $form->dropDownList($model,'registry_id',$model->droplist('registry_id','registry',$model_filter), $htmlOptions);?>
			<?php echo $form->error($model,'registry_id'); ?>
		</div>

		<div class="sima-model-form-row">
			<?php echo $form->labelEx($model,'comment'); ?>
			<?php echo $form->textArea($model,'comment'); ?>
			<?php echo $form->error($model,'comment'); ?>
		</div>
		
		<?php $this->endWidget(); ?>
	
	
		
	</div>

<script>
    
<?php if (Yii::app()->user->checkAccess('EnableFormInput',[],$model)) {?>
    $("#<?php echo $form_id?> div.sima-model-form-row").on('dblclick','label',function(){
        sima.model.enableFormInput($(this));
    });
<?php }?>

</script>


