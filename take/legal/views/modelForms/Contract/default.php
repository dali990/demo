
<div id="<?php echo $form_id?>" class="sima-model-form belongs">		
    <?php $form=$this->beginWidget('CActiveForm', array(
            'id'=>SIMAHtml::uniqid(),
            'enableClientValidation'=>true,
    )); ?>	
    <?php 
        echo $form->hiddenField($model,'number'); 
        echo $form->hiddenField($model,'old'); 
        echo $form->hiddenField($model,'id');?>
				
        <div class="sima-model-form-row is_annex">
            <label for="is_annex">Aneks</label>
            <input type="radio" name="Contract[is_annex]" value="no"  <?php if ($model->parent_id==null && $model->is_annex!='yes') echo "checked='checked'";?>/> Osnovni ugovor
            <input type="radio" name="Contract[is_annex]" value="yes" <?php if ($model->parent_id!=null || ($model->parent_id==null && $model->is_annex=='yes')) echo "checked='checked'";?>/> Aneks ugovora
        </div>

        <div class="sima-model-form-row parent_id" <?php if ($model->parent_id==null && $model->is_annex!='yes') echo "style='display: none;'";?>>
            <?php echo $form->labelEx($model,'parent_id'); ?>
            <?php 
                $this->widget('ext.SIMASearchField.SIMASearchField', array(
                    'model' => $model,
                    'relation' => 'parent'
                )); 
            ?>
        </div><div class="sima-model-form-row">
            <?php echo $form->error($model,'parent_id'); ?>	
        </div>

        <script>
            function fun_is_annex()
            {
                if ($(this).val()=='yes')
                {
                    $('div#<?php echo $form_id?> div.sima-model-form-row.parent_id').show();
//                    $('div#<?php echo $form_id?> input[name="Contract[number]"]').prop('value','0');
                    //$('div.form.wide input[name="Contract[parent_id]"]').prop('value','0');
                }
                else
                {
                    $('div#<?php echo $form_id?> div.sima-model-form-row.parent_id').hide();
//                    $('div#<?php echo $form_id?> input[name="Contract[parent_id]"]').prop('value','');
//                    $('div#<?php echo $form_id?> input[name="Contract[parent_id]"]').parent().children('input[type="text"]').prop('value','');
//                    $('div#<?php echo $form_id?> input[name="Contract[number]"]').prop('value','');
                    //$('div.form.wide input[name="Contract[annex_number]"]').prop('value','');
                }
            };
            $("div#<?php echo $form_id?> div.sima-model-form-row.is_annex input").on('change',fun_is_annex);
        </script>

        <div class="sima-model-form-row">
            <?php echo $form->labelEx($model,'confirmed'); ?>
            <?php echo $form->checkBox($model,'confirmed'); ?>
            <?php echo $form->error($model,'confirmed'); ?>
        </div>
				
        <div class="sima-model-form-row">
            <?php echo $form->labelEx($model,'partner_filing_number'); ?>
            <?php echo $form->textField($model,'partner_filing_number'); ?>
            <?php echo $form->error($model,'partner_filing_number'); ?>
        </div>	
				
        <div class="sima-model-form-row">
            <?php echo $form->labelEx($model,'partner_filing_date'); ?>
            <?php echo SIMAHtml::datetimeField($model, 'partner_filing_date'); ?>
            <?php echo $form->error($model,'partner_filing_date'); ?>
        </div>

        <div class="sima-model-form-row">
            <?php echo $form->labelEx($model,'subject'); ?>
            <?php echo $form->textArea($model,'subject'); ?>
            <?php echo $form->error($model,'subject'); ?>	
        </div>	
							
        <div class="sima-model-form-row">
            <?php echo $form->labelEx($model,'sign_date'); ?>
            <?php echo SIMAHtml::datetimeField($model, 'sign_date'); ?>
            <?php echo $form->error($model,'sign_date'); ?>
        </div>
                    
        <div class="sima-model-form-row">
            <?php 
                echo $form->labelEx($model, 'partner_id');
                $this->widget('ext.SIMASearchField.SIMASearchField', array(
                    'model' => $model,
                    'relation' => 'partner'
                )); 
                echo SIMAHtml::openActionInDialog('simaAddressbook/index');
                echo $form->error($model, 'partner_id');
            ?>
        </div>		
				
        <div class="sima-model-form-row contract_type_id">
            <?php echo $form->labelEx($model,'contract_type_id'); ?>
            <?php echo $form->dropDownList($model,'contract_type_id',$model->droplist('contract_type_id'), array('empty'=>'Izaberi')); ?>
            <?php echo $form->error($model,'contract_type_id'); ?>				
        </div>				
				
        <div class="sima-model-form-row deadline">
            <?php echo $form->labelEx($model,'deadline'); ?> 
            <?php echo SIMAHtml::datetimeField($model, 'deadline'); ?>
            <span class='link' style='font-size:10px;' onclick="$('div#<?php echo $form_id?> div.sima-model-form-row.deadline_description').toggle(300);">opisno</span>
            <span class='link calc' style='font-size:10px;' onclick='deadline_function<?php echo $form_id?>();'>izracunaj</span>
            <?php echo $form->error($model,'deadline'); ?>
        </div>
				
        <div class="sima-model-form-row deadline_description" <?php if ($model->deadline_description==null || $model->deadline!=null) echo "style='display:none'";?>>
            <?php echo $form->labelEx($model,'deadline_description'); ?>
            <?php echo $form->textArea($model,'deadline_description'); ?>
            <?php echo $form->error($model,'deadline_description'); ?>	
        </div>
				
        <div class="sima-model-form-row value">
            <?php echo $form->labelEx($model,'value'); ?>
            <span class='short'>
                <?= SIMAHtml::numberField($model, 'value'); ?>
            </span>
            <span class='currency'>
                <?php 
                    $model->currency_id = Yii::app()->configManager->get('base.country_currency', false);
                    echo $form->dropDownList($model,'currency_id',$model->droplist('currency_id')); 
                ?>
            </span>
            <span class='link' style='font-size:10px;' onclick="$('div#<?php echo $form_id?> div.sima-model-form-row.value_text').toggle(300);">opisno</span>
            <?php echo $form->error($model,'value'); ?>	
        </div>
				
        <div class="sima-model-form-row value_text" <?php if ($model->value_text==null || $model->value!=null) echo "style='display:none'"?>>
            <?php echo $form->labelEx($model,'value_text'); ?>
            <?php echo $form->textArea($model,'value_text'); ?>
            <?php echo $form->error($model,'value_text'); ?>	
        </div>
				
        <div class="sima-model-form-row">
            <?php echo $form->labelEx($model,'vat_included'); ?>
            <?php echo $form->checkBox($model,'vat_included'); ?>
            <?php echo $form->error($model,'vat_included'); ?>	
        </div>
				
        <div class="sima-model-form-row vat">
            <?php echo $form->labelEx($model,'vat'); ?>
            <span class='short'>
                <?= SIMAHtml::numberField($model, 'vat'); ?>
            </span>
            <span class='currency'><button type='button' 
                    onclick='fun_percent("<?php echo $form_id?>?>")'
            >%</button></span>
            <span class='link' style='font-size:10px;' onclick="$('div#<?php echo $form_id?> div.sima-model-form-row.vat_text').toggle(300);">opisno</span>
            <?php echo $form->error($model,'vat'); ?>	
        </div>
				
        <script>
            function fun_percent(form_id)
            {
                var vat = $("div#<?php echo $form_id?> div.sima-model-form-row.value input").prop("value");
                if (vat=='') vat=0;
                sima.dialog.openYesNo('<input id="<?php $vat_per_id = SIMAHtml::uniqid(); echo $vat_per_id; ?>"></input>%',function(){
                    var vat_per = $("input#<?php echo $vat_per_id?>").prop("value");
                    if (vat_per==="") vat_per=0; else vat_per = parseFloat(vat_per);
                    var vat_local=vat*(vat_per/(100+vat_per));
                    $("div#<?php echo $form_id?> div.sima-model-form-row.vat input").prop("value",vat_local);
                    sima.dialog.close();
                });
            };
        </script>
				
        <div class="sima-model-form-row vat_text" <?php if ($model->vat_text==null || $model->vat!=null) echo "style='display:none'";?> >
            <?php echo $form->labelEx($model,'vat_text'); ?>
            <?php echo $form->textArea($model,'vat_text'); ?>
            <?php echo $form->error($model,'vat_text'); ?>	
        </div>
				
        <div class="sima-model-form-row advance">
            <?php echo $form->labelEx($model,'advance'); ?>
            <span class='short'>
                <?= SIMAHtml::numberField($model, 'advance'); ?>
            </span>
            <span class='currency'><button type='button' 
                    onclick='fun_advance("<?php echo $form_id?>?>")'
            >%</button></span>
            <span class='link' style='font-size:10px;' onclick="$('div#<?php echo $form_id?> div.sima-model-form-row.advance_text').toggle(300);">opisno</span>
            <?php echo $form->error($model,'advance'); ?>	
        </div>	
				
        <script>
            function fun_advance(form_id)
            {
                var advance_up = $("div#<?php echo $form_id?> div.sima-model-form-row.value input").prop("value");
                if (advance_up=='') advance_up=0;
                sima.dialog.openYesNo('<input id="<?php $advance_per_id = SIMAHtml::uniqid(); echo $advance_per_id; ?>"></input>%',function(){
                    var advance = $("input#<?php echo $advance_per_id?>").prop("value");
                    if (advance=="") advance=0;
                    advance*=advance_up;
                    advance/=100;
                    $("div#<?php echo $form_id?> div.sima-model-form-row.advance input").prop("value",advance);
                    sima.dialog.close();
                });
            };
        </script>

        <div class="sima-model-form-row advance_text" <?php if ($model->advance_text==null || $model->advance!=null) echo "style='display:none'";?>>
            <?php echo $form->labelEx($model,'advance_text'); ?>
            <?php echo $form->textArea($model,'advance_text'); ?>
            <?php echo $form->error($model,'advance_text'); ?>	
        </div>				
				
<!--        <div class="sima-model-form-row">
            <?php // Regisrtatori se zadaju u zavodnoj knjizi jer svi ugovori moraju biti zavedeni ?>
            <?php // echo $form->labelEx($model,'registry_id'); ?>
            <?php // echo $form->dropDownList($model,'registry_id',$model->droplist('registry_id'), array('empty'=>'Izaberi')); ?>
            <?php // echo $form->error($model,'registry_id'); ?>
        </div>-->

 <?php $this->endWidget('CActiveForm'); ?>
       
</div>




<script>
    function deadline_function<?php echo $form_id?>(){
        $('div#<?php echo $form_id?> div.sima-model-form-row.deadline_description').show(300);
        sima.misc.calcDeadlineDialog(
            'div#<?php echo $form_id?> div.sima-model-form-row input[name="Contract[deadline]"]',
            'div#<?php echo $form_id?> div.sima-model-form-row textarea[name="Contract[deadline_description]"]');
    };
    
</script>
