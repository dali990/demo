
<?php
    $this->widget('base.extensions.SIMAGuiTable.SIMAGuiTable', [
        'id' => "filing_book_$uniq",
        'model' => Filing::model(),
        'add_button' => [
            'button1' => "sima.dialog.openYesNo('".Yii::t('LegalModule.Filing', 'DoYouWantToReserveFilingNumber')."',
                function() {
                    sima.filing_reserve('#filing_book_$uniq')
                });",
            'button2' => [
                'access' => 'AddGuiTable2',
                'formName' => 'guiTable2',
                'scenario' => 'addButton2'
            ]
        ]
    ]);

