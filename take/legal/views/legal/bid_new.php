<?php	
$panels = [
    [
        'min_width' => 300,
        'max_width' => 300,
        'html' => $this->renderModelView($model, 'info')
    ],
    [
        'html'=> $this->widget('SIMATabs', array(
            'list_tabs_model' => $model,
            'default_selected' => $default_selected,
            'selector' => $selector
        ), true)
    ]
];

echo Yii::app()->controller->renderContentLayout([
        'name'=> $this->renderModelView($model, 'title'),                
        'options_class'=>'sima-fb-options',
        'options'=>[
            [
                'html' => $this->renderModelView($model, 'options')
            ]
        ]
    ], 
    $panels
);
