<?php
 	if (Yii::app()->request->isAjaxRequest) Yii::app()->clientScript->clearScriptFiles();
?>

U imeniku u firmi: <?php echo $count?><br />
od toga u sistemu:  <?php echo $count_system?><br />
Radi:       <?php echo $working_count?><br />
ne radi:        <?php echo $not_working_count?><br />
neodredjeno:        <?php echo $permanent_count?>(nema jos uvek rakid ugovora)<br /> 
na odredjeno radi:        <?php echo $not_permanent_working_count?><br />
na odredjeno radilo:        <?php echo $not_permanent_not_working_count?><br />
nema ugovor:        <?php echo $non_contract?><br />

<?php
foreach($degrees as $degree)
{
    $count=0;
    foreach($degree->employees as $person)
    {
        if (isset($person->employee)) $count++;
    }
    echo $degree->DisplayName.' -> '.$count.'<br/>';
}
?>
nije uneto -> <?php echo $degree_not_entered?><br />

<?php
 	if (Yii::app()->request->isAjaxRequest) Yii::app()->clientScript->clearScriptFiles();
?>