<?php
 	if (Yii::app()->request->isAjaxRequest) Yii::app()->clientScript->clearScriptFiles();
?>

<?php $uniq = SIMAHtml::uniqid();?>

<?php 
    $this->beginWidget('ext.SIMAResizablePanel.SIMAResizablePanel',
        array(
            'id'=>'panel_'.$uniq,
            'type'=>'vertical',
            'class'=>  'sima-ui-tabs',
            'proportions'=>array(0.5,0.5)
            ));
?>
    <div class='employees sima-layout-panel'>
        <?php
            $this->widget('base.extensions.SIMAGuiTable.SIMAGuiTable', [
                'id'=>'persons'.$uniq,
                'model'=>Employee::model(),
                'columns_type'=>'legal',
                'fixed_filter'=>array(
                    'display_scopes' => 'byCompanyOrder'
                ),
                'setRowSelect' => "on_select$uniq"                
            ]);
        ?>
    </div>
    <div class='employees additional_info sima-layout-panel'>
    <?php 	
            $id_tab=SIMAHtml::uniqid();
            $this->widget('ext.SIMATabs.SIMATabs', array(
                'id'=>$id_tab,
                'list_tabs_model_name' => 'Partner',
                'default_selected'=>'work_relation'
                
            ));  
    ?>
    </div>
<?php $this->endWidget();?>


<script type="text/javascript">
    function on_select<?php echo $uniq; ?>(obj)
    {
        $('#<?php echo $id_tab?>').simaTabs('set_list_tabs_params',{
            list_tabs_trigger_tag: sima.getTag('<?php echo SIMAHtml::getTag(Employee::model()); ?>', obj.attr('model_id')),
            list_tabs_get_params:{id:obj.attr('model_id')}
        });
        setTimeout(function(){
            sima.selectSubObj($('#<?php echo $id_tab; ?>').parent(), [{simaTabs:"work_relation"},{simaTabs:"days_off"}]);
        },200);
    }
</script>

<?php
 	if (Yii::app()->request->isAjaxRequest) Yii::app()->clientScript->clearScriptFiles();
?>