<?php
 	if (Yii::app()->request->isAjaxRequest) Yii::app()->clientScript->clearScriptFiles();
?>

<?php $uniq = SIMAHtml::uniqid();?>

<div class="sima-layout-panel _vertical _splitter">
    <div class='sima-layout-panel'>
        <?php
            $this->widget('SIMAGuiTable', [
                'model'=>Employee::model(),
                'columns_type'=>'legal',
                'fixed_filter'=>array(
                    'display_scopes' => 'byCompanyOrder'
                ),
                'select_filter' => [
                    'active_contracts' => SIMAHtml::DbToUserDate(date('d.m.Y.'))
                ],
                'setRowSelect' => "on_select$uniq"                
            ]);
        ?>
    </div>
    <div id="<?=$uniq?>" class='sima-layout-panel'>
        
    </div>
</div>


<script type="text/javascript">
    function on_select<?=$uniq?>(obj)
    {
        sima.set_html( $('#<?=$uniq?>'), sima.model.renderFullInfo('<?=Person::class?>', obj.attr('model_id')));
    }
</script>

<?php
 	if (Yii::app()->request->isAjaxRequest) Yii::app()->clientScript->clearScriptFiles();
?>