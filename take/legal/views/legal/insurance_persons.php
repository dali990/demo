<?php
    $uniq = SIMAHtml::uniqid();
    $this->widget('SIMAGuiTable', [
        'id'=>'insurance_persons'.$uniq,
        'model'=>EmployeeInsurancePerson::model(),
        'columns_type'=>'from_legal',
        'add_button'=>[
            'init_data'=>[
                'EmployeeInsurancePerson'=>[
                    'employee_id'=>[
                        'disabled',
                        'init'=>$model->id
                    ],
                    'end_date'=>[
                        'disabled',
                        'init'=>$model->expiration_date_of_insurance
                    ]
                ]
            ]
        ],
        'fixed_filter'=>[
            'employee'=>[
                'ids'=>$model->id
            ]
        ]
    ]);
    
?>