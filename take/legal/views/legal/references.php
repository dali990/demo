<?php
    if (Yii::app()->request->isAjaxRequest) Yii::app()->clientScript->clearScriptFiles();
?>
<?php $uniq = SIMAHtml::uniqid(); ?>
<?php $this->beginWidget('ext.SIMAResizablePanel.SIMAResizablePanel',
    array(
        'id'=>'panel_'.$uniq,
        'type'=>'vertical',
        'proportions'=>array(0.4, 0.6)        
        )); ?>

    <div class='sima-layout-panel left_side'>
        <?php
            $this->widget('base.extensions.SIMAGuiTable.SIMAGuiTable', [
                'id' => 'orders'.$uniq,
                'model' => Reference::model(),
                'fixed_filter' => $filter,
                'add_button' => $add_button,
                'setRowSelect' => "on_select$uniq",                
                'setRowUnSelect' => "on_unselect$uniq",
                'setMultiSelect' => "on_unselect$uniq"                
            ]);
        ?>
    </div>


    <div class='sima-layout-panel right_side'>
        <?php
            $id_tab = SIMAHtml::uniqid();
            $this->widget('ext.SIMATabs.SIMATabs', array(
                'id' => $id_tab,
                'list_tabs_model_name' => 'Reference',
                'default_selected' => 'info'
            ));
        ?>
    </div>   
   
<?php $this->endWidget(); ?>

<script type='text/javascript'>
    function on_select<?php echo $uniq; ?>(obj)
    {
        $('#panel_<?php echo $uniq; ?>').find('.right_side').show();
        $('#<?php echo $id_tab; ?>').simaTabs('set_list_tabs_params',{
            list_tabs_trigger_tag: sima.getTag('<?php echo SIMAHtml::getTag(Reference::model()); ?>', obj.attr('model_id')),
            list_tabs_get_params:{id:obj.attr('model_id')}
        });
    }
    function on_unselect<?php echo $uniq; ?>(obj)
    {   
        $('#panel_<?php echo $uniq; ?>').find('.right_side').hide();
    }
</script>

<?php
    if (Yii::app()->request->isAjaxRequest) Yii::app()->clientScript->clearScriptFiles();
?>