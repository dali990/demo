<?php
 	if (Yii::app()->request->isAjaxRequest) Yii::app()->clientScript->clearScriptFiles();
?>

<?php $uniq = SIMAHtml::uniqid();?>
<div class="company_licenses_choose" style="margin: 10px 0px;">
    <?php
        $this->widget('ext.SIMASearchField.SIMASearchField', array(
            'id'=>"choose_company_license$uniq",
            'model' => CompanyLicenseToCompany::model(),            
            'onchange' => "on_choose_company_license$uniq",
            'htmlOptions' => array(
                'placeholder' => 'Velika licenca',
            )            
       ));
    ?>
</div>
<div class='person_references'>
    <?php
        $this->widget('SIMAGuiTable', [
            'id'=>'person_references'.$uniq,
            'model'=>  Reference::model(),
            'columns_type'=>'person_references',
            'fixed_filter'=>array(
                'reference_to_person_license'=>[
                    'person_license'=>[
                        'person'=>[
                            'ids'=>$person->id
                        ]
                    ]
                ]
            )               
        ]);
    ?>
</div>

<?php
    $script = "$('#choose_company_license$uniq').simaSearchField('onChangeFunc',function(params){"
            . "on_choose_company_license$uniq(params)"
            . "});";
    Yii::app()->clientScript->registerScript(SIMAHtml::uniqid(), $script, CClientScript::POS_READY);
?>
<script type="text/javascript">
    function on_choose_company_license<?php echo $uniq; ?>(model_id)
    {        
        var filter = {};
        if (typeof model_id !== 'undefined')
        {
            filter = {
                reference_to_company_license:{
                    company_license:{
                        ids:model_id
                    }
                },
                reference_to_person_license:{
                    person_license:{
                        person:{
                            ids:<?php echo $person->id; ?>
                        }
                    }
                }
            };
        }
        else
        {
            filter = {
                reference_to_person_license:{
                    person_license:{
                        person:{
                            ids:<?php echo $person->id; ?>
                        }
                    }
                }
            };
        }
        $('#person_references<?php echo $uniq; ?>').simaGuiTable('setFixedFilter', filter, true);
    }
</script>

<?php
 	if (Yii::app()->request->isAjaxRequest) Yii::app()->clientScript->clearScriptFiles();
?>