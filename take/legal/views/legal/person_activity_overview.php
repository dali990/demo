<sima-tabs 
    id = "<?=$uniq?>"
    :default_selected = "'1'"
    :tabs = "[
        {
            title: '<?=Yii::t('LegalModule.Legal', 'PersonActivityOverviewForPersonsInOrganizationScheme')?>',
            code: '1',
            content_action: {
                action: 'jobs/personActivityOverview/renderPersonActivityOverviewForMonth',
                post_params: {
                    sector_id: <?=$sector_id?>
                }
            }
        },
        {
            title: '<?=Yii::t('LegalModule.Legal', 'PersonActivityOverviewForPersonsNotInOrganizationScheme')?>',
            code: '2',
            content_action: {
                action: 'jobs/personActivityOverview/renderPersonActivityOverviewForMonth',
                post_params: {
                    not_in_sector_id: <?=$sector_id?>
                }
            }
        },
        {
            title: '<?=Yii::t('LegalModule.Legal', 'WorkedHoursEvidences')?>',
            code: '3',
            selectable: false,
            select_sub_tree_item_code: '1',
            subtree: [
                {
                    title: '<?=Yii::t('LegalModule.Legal', 'ActiveWorkedHoursEvidences')?>',
                    code: '3.1',
                    content_action: {
                        action: 'legal/legal/renderActiveWorkedHoursEvidences'
                    }
                },
                {
                    title: '<?=Yii::t('LegalModule.Legal', 'AllWorkedHoursEvidences')?>',
                    code: '3.2',
                    content_action: {
                        action: 'legal/legal/renderAllWorkedHoursEvidences'
                    }
                }
            ]
        }
    ]"
></sima-tabs>