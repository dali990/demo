<?php
    if (Yii::app()->request->isAjaxRequest) Yii::app()->clientScript->clearScriptFiles();
?>
<?php $uniq = SIMAHtml::uniqid(); ?>   
    <div class='sima-layout-panel left_side'>
        <?php
            $filter = array_merge(['order_by' => 'created_date DESC'],$filter);
            $potential_bid_document_type_id = Yii::app()->configManager->get('legal.potential_bid', false);
            $this->widget('base.extensions.SIMAGuiTable.SIMAGuiTable', [
                'id' => 'orders'.$uniq,
                'model' => PotentialBid::model(),                
                'add_button' => [
                    'init_data' => [
                        'File' => [
                            'document_type_id'=>$potential_bid_document_type_id,
                            'responsible_id'=>Yii::app()->user->id
                        ]
                    ]
                ],
                'fixed_filter' => $filter,
                'select_filter' => [
                    'accepted_filter' => PotentialBid::$NEW
                ]
            ]);
        ?>
    </div>   
<?php
    if (Yii::app()->request->isAjaxRequest) Yii::app()->clientScript->clearScriptFiles();
?>