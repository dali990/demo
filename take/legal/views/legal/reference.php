<div class="sima-layout-panel _splitter _vertical">

    <div class="sima-layout-panel">
        <?php echo $this->renderModelView($model, 'indexTitle'); ?>
        <?php echo $this->renderModelView($model, 'info'); ?>
    </div>

    <div class="sima-layout-panel">
        <?php
            $this->widget('ext.SIMATabs.SIMATabs', array(
                'list_tabs_model' => $model,
                'default_selected' => $default_selected,
                'selector' => $selector,
                'ignore_tabs'=>'info'
            ));
        ?>
    </div>

</div>
