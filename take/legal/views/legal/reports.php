
<div class='content reports'>

	<ul class='tabs'>
		<li class='KPR'><span onclick='ShowBillInKPR()'>KPR</span></li>
		<li class='KIR'><span onclick='ShowBillOutKIR()'>KIR</span></li>
	</ul>

	<div id='KPR_report' style='display: none;'>
		<h1>KPR</h1>
		<?php 
			$this->widget('base.extensions.SIMAGuiTable.SIMAGuiTable', [
                            'id'=>'KPR',
                            'model'=>BillIn::model(),
                            'columns_type'=>'KPR'
                        ]);
		?>
	</div>
	
	<div id='KIR_report' style='display: none;'>
		<h1>KIR</h1>
		<?php 
			$this->widget('base.extensions.SIMAGuiTable.SIMAGuiTable', [
                            'id'=>'KIR',
                            'model'=>BillOut::model(),
                            'columns_type'=>'KIR'
                        ]);
		?>
	</div>
	
</div>

<script type='text/javascript'>

function ShowBillInKPR()
{
	KPR_populate_paged_table();
	hideAllReports(); 
	$("#KPR_report").show();
	$("ul.tabs li.KPR").addClass('active');
}
function ShowBillOutKIR()
{
	KIR_populate_paged_table();
	hideAllReports(); 
	$("#KIR_report").show();
	$("ul.tabs li.KIR").addClass('active');
}

function hideAllReports()
{
	$("ul.tabs li").removeClass('active');
	$("#KPR_report").hide();
	$("#KIR_report").hide();
}

hideAllReports();

Show<?php echo $last_page?>();

</script>