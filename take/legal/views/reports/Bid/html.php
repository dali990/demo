
<page size="A4">
<div id="container">
    <div class="main">
        <table class="left">
            <tr>
                <td>Broj Ponude:</td>
                <td><strong><?=$bid->number_of_bid?></strong></td>
            </tr>
            <tr>
                <td>Datum:</td>
                <td><?=SIMAHtml::DbToUserDate($bid->delivering_time)?></td>
            </tr>
        </table>

        <div class="right">
            <p><?=$bid->partner->DisplayName?></p>
            <p><?=$bid->partner->DisplayMainAddress?></p>
            <p>Upućeno: <?=!empty($bid->refer) ? $bid->refer->DisplayName : '';?></p>
            <p>E-mail: <?=!empty($bid->refer) ? $bid->refer->MainEmailAddressDisplay : '';?></p>
        </div>
        <div class="clear"></div>
        <div class="title">
        <h1>TEMA: <?=$bid->subject?><span>Projekat: <?=$bid->theme->DisplayName?></span></h1>
        </div><!-- /title -->

        <div class="content">
            <div>
                <p><?=$bid->text_of_bid?></p>
                <p><strong><?=$curr_company->DisplayName?></strong> je kompanija sposobna da izvrši potrebne radove za sledeću sumu, ova suma je strogo zavisna od Uslova Dogovora koje će biti deo ove ponude</p>
                <table class="main-table">
                    <tr>
                        <td><strong>Vrsta Radova prema Ponudi</strong></td>
                        <td>Ukupna Suma</td>
                    </tr>
                    <tr>
                        <td><?=$bid->theme->DisplayName?></td>
                        <td><?=!empty($bid->currency) ? $bid->currency->DisplayName : ''?> <?=SIMAHtml::number_format($bid->value)?></td>
                    </tr>
                </table>
            <p><strong>Osnova Ponude:</strong> <?=$bid->base_of_bid?></p>
            <p><strong>Rok za Izvršenje Radova:</strong> <?=$bid->limit_execution_work?></p>
            </div>
            <div class="clear"></div>
        </div><!-- /content -->
    </div> <!-- /main -->

    <p><?=$curr_company->DisplayName?> se zahvaljuje vama i vašoj kompaniji za ovu priliku da vam dostavi ponudu za ovaj
Projekat. Ukoliko imate bilo kakvih pitanja ili su vam potrebne dodatne informacije molimo vas da se
obratite dole potpisanom.</p>
<div class="clear"></div>

<div class="signature">
    <p>U ime kompanije, <strong><?=$curr_company->DisplayName?></strong></p>
    <br><br><br><br><br><br>
    <p><strong><?=!empty($bid->compose) ? $bid->compose->DisplayName : '';?></strong><br><?=!empty($bid->compose) ? $bid->compose->columnDisplays('work_positions') : '';?></p>
    <table class="info">
        <tr>
            <td>Mobilni:</td>
            <td><?=!empty($bid->compose) ? $bid->compose->MainPhoneNumberDisplay : '';?></td>
        </tr>
        <tr>
            <td>Email</td>
            <td><?=!empty($bid->compose) ? $bid->compose->MainEmailAddressDisplay : '';?></td>
        </tr>
    </table>
</div><!-- signature -->
<div class="signature">
    <strong>Primalac prihvata navedene Uslove u ovoj ponudi:</strong>
    <table class="signature-table">
        <tr>
            <td>Potpisao:</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>Name:</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>Pozicija:</td>
            <td></td>
            <td></td>
        </tr>
    </table>
</div><!-- .signature -->
</div><!-- /container -->
</page>