<header>
    <img src="<?=$curr_company->getLogoImagePath()?>">
    <div class="content">
        <ul>
            <li><span><?=$curr_company->DisplayName?>:</span></li>
            <li><?=!empty($curr_company->partner) ? $curr_company->partner->DisplayMainAddress : '';?></li>
            <li><span>Email:</span> <?=!empty($curr_company->partner) ? $curr_company->partner->MainEmailAddressDisplay : '';?> | 
                <span>Tel/Fax:</span> <?=!empty($curr_company->partner) ? $curr_company->partner->MainPhoneNumberDisplay : '';?></li>
            <li><span>PIB:</span> <?=$curr_company->PIB?> | <span>Matični broj:</span> <?=$curr_company->MB?> | <span>Poslovni broj:</span> <?=!empty($curr_company->work_code) ? $curr_company->work_code->code : '';?></li>
        </ul>
    </div>	
</header>