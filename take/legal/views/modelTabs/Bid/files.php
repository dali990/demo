<?php

$tag = $model->gettag();
$this->widget('base.extensions.SIMAFileBrowser.SIMAFileBrowser', array(
    'location_code' => 'bids_files',
    'default_json' => '{"name":"ponude","children":[[{"model":"DocumentType","type":"tag_group","attributes":{"show_unlisted":"true","tag_group_tree_view":"true"},"conditions":"","alias":"Tipovi dokumenta","clas":"group_table","highest_level_num":"1","children":[[{"type_column":"partner_id","type":"file_attribute","model":"Filing","attributes":{"show_unlisted":"true","null_name":""},"conditions":"","alias":"Partner","clas":"group_attribute"}]]}]],"attributes":{"show_unlisted":"false"}}',
    'init_tql' => $tag->query.'!!TAG_PARENT$'.$tag->id,
));