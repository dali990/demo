<?php

$this->widget('SIMAGuiTable',[
    'model' => PersonToTheme::model(),
    'add_button' => [
        'init_data' => [
            'PersonToTheme' => [
                'theme' => ['ids' => $model->id]
            ]
        ]
    ],
    'fixed_filter' => [
        'theme' => ['ids' => $model->id]
    ]
]);