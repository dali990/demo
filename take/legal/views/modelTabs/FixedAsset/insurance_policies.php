<?php
    $uniq = SIMAHtml::uniqid();
    $this->widget(SIMAGuiTable::class, [
        'id' => 'insurance_policy'.$uniq,
        'model'=> InsurancePolicy::class,  
        'columns_type' => 'without_file_belongs',
        'add_button' => [
            'init_data' => [
                'File' => [
                    'document_type_id' => Yii::app()->configManager->get('legal.insurance_policy_document_type_id', false),
                    'responsible' => [
                        'ids' => Yii::app()->user->id
                    ],
                    'file_belongs' => [
                        'ids'=> [
                            'ids'=> [
                                [
                                    'class'=> '_non_editable',
                                    'id'=>$model->tag->id,
                                    'display_name'=> $model->tag->DisplayName,
                                ]
                            ]
                        ]
                    ]
                ]
            ],
        ],
        'fixed_filter' => [
            'file' => [
                'belongs' => $model->tag->queryR
            ],
        ],
    ]);
