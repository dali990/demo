<?php

abstract class SIMAReport extends SIMAComponent
{
    protected $class_name = null;
    protected $views_path_alias = null;
    protected $css_file_name = null;
    protected $margin_top = null;
    protected $margin_bottom = null;
    protected $orientation = 'Portrait';
    protected $include_page_numbering = true;

    public $params = [];
    public $download_name = '';

    public function __construct($params = [])
    {
        parent::__construct();

        $this->class_name = get_class($this);
        $this->params = $params;

        $reflector = new ReflectionClass($this->class_name);
        $this->views_path_alias = $this->getModuleName() . '.views.reports.' . str_replace('Report', '', $reflector->getName());
    }
    
    abstract protected function getModuleName();
    
    public function getHTML($return_temp_file = false)
    {
        $html = $this->parseHTMLContent($this->getHTMLContent()) . $this->parseHTMLContent($this->getHTMLFooter());
        
        if (!empty($this->css_file_name))
        {
            $css_content = file_get_contents($this->getReportFileFullPath($this->css_file_name));
            $html = "<style>$css_content</style>$html";
        }

        if ($return_temp_file === true)
        {
            return new TemporaryFile($html, null, 'html');
        }
        else
        {
            return $html;
        }
    }
    
    public function getPDF()
    {
        $temporary_file = new TemporaryFile('', null, 'pdf');
        
        $pdf_params = [];
        if (!empty($this->css_file_name))
        {
            $pdf_params['user-style-sheet'] = $this->getReportFileFullPath($this->css_file_name);
        }
        
        $margin_parameters = [];
        if (isset($this->margin_top))
        {
            $margin_parameters['pdf_margin_top'] = $this->margin_top;
        }
        if (isset($this->margin_bottom))
        {
            $margin_parameters['pdf_margin_bottom'] = $this->margin_bottom;
        }

        $temporary_file->createPdfFromHtml(
            [
                'headerHtml' => $this->parseHTMLContent($this->getHTMLHeader()),
                'bodyHtml' => $this->parseHTMLContent($this->getHTMLContent()),
                'footerHtml' => $this->parseHTMLContent($this->getHTMLFooter()),
                'marginParameters' => $margin_parameters,
                'orientation' => $this->orientation,
                'includePageNumbering' =>$this->include_page_numbering
            ],
            $pdf_params
        );
        
        return $temporary_file;
    }
    
    public function getXML($return_temp_file = false)
    {
        if ($return_temp_file === true)
        {
            return new TemporaryFile($this->getXMLContent(), null, 'xml');
        }
        else
        {
            return $this->getXMLContent();
        }
    }

    public function render($view, $data = [], $return = false, $processOutput = false)
    {
        if ($return === true)
        {
            return Yii::app()->controller->renderPartial($this->views_path_alias . ".$view", $data, $return, $processOutput);
        }
        
        Yii::app()->controller->renderPartial($this->views_path_alias . ".$view", $data, $return, $processOutput);
    }
    
    public function getDownloadName()
    {
        return $this->download_name;
    }
    
    protected function getHTMLContent()
    {
        throw new SIMAException(Yii::t('BaseModule.SIMAReport', 'GetHTMLContentNotDefined', [
            '{class_name}' => $this->class_name
        ]));
    }
    
    protected function getHTMLFooter()
    {
        return '';
    }
    
    protected function getHTMLHeader()
    {
        return '';
    }

    protected function getXMLContent()
    {
        throw new SIMAException(Yii::t('BaseModule.SIMAReport', 'GetXMLContentNotDefined', [
            '{class_name}' => $this->class_name
        ]));
    }
    
    private function parseHTMLContent($html_content)
    {
        if (is_array($html_content))
        {
            if (!empty($html_content['css']))
            {
                return "<style>{$html_content['css']}</style>{$html_content['html']}";
            }
            else if (!empty($html_content['css_file_name']))
            {
                $css_content = file_get_contents($this->getReportFileFullPath($html_content['css_file_name'] . '.css'));
                return "<style>$css_content</style>{$html_content['html']}";
            }
            else
            {
                return $html_content['html'];
            }
        }
        else
        {
            return $html_content;
        }
    }
    
    private function getReportFileFullPath($file_name)
    {
        return Yii::getPathOfAlias($this->views_path_alias) . DIRECTORY_SEPARATOR . $file_name;
    }
}

