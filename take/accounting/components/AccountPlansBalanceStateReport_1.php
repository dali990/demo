<?php

class AccountPlansBalanceStateReport extends AccountPlansBalanceReport
{
    public function getReportName() : string
    {
        return 'Bilans stanja';
    }
    
    protected function getAOPCodes() : array
    {
        return [
            '0001' => '00',
            '0002' => '0',
            '0003' => '01',
            '0004' => '',
            '0005' => '',
            '0006' => '',
            '0007' => '',
            '0008' => '',
            '0009' => '',
            '0010' => '02',
            '0011' => '',
            '0012' => '',
            '0013' => '',
            '0014' => '',
            '0015' => '',
            '0016' => '',
            '0017' => '',
            '0018' => '',
            '0019' => '03',
            '0020' => '',
            '0021' => '',
            '0022' => '',
            '0023' => '',
            '0024' => [
                'plus' => ['04'],
                'minus' => ['047']
            ],
            '0025' => '',
            '0026' => '',
            '0027' => '',
            '0028' => '',
            '0029' => '',
            '0030' => '',
            '0031' => '',
            '0032' => '',
            '0033' => '',
            '0034' => '05',
            '0035' => '',
            '0036' => '',
            '0037' => '',
            '0038' => '',
            '0039' => '',
            '0040' => '',
            '0041' => '',
            '0042' => '288',
            '0043' => [
                'plus' => ['1', '2']
            ],
            '0044' => '1',
            '0045' => '10',
            '0046' => '11',
            '0047' => '12',
            '0048' => '13',
            '0049' => '14',
            '0050' => '15',
            '0051' => '20',
            '0052' => '',
            '0053' => '',
            '0054' => '',
            '0055' => '',
            '0056' => '',
            '0057' => '',
            '0058' => '',
            '0059' => '21',
            '0060' => '22',
            '0061' => '236',
            '0062' => [
                'plus' => ['23'],
                'minus' => ['236', '237']
            ],
            '0063' => '',
            '0064' => '',
            '0065' => '',
            '0066' => '',
            '0067' => '',
            '0068' => '24',
            '0069' => '27',
            '0070' => [
                'plus' => ['28'],
                'minus' => ['288']
            ],
            '0071' => [
                'plus' => ['0', '1', '2']
            ],
            '0072' => '88',
            '0401' => '3',
            '0402' => '30',
            '0403' => '300',
            '0404' => '301',
            '0405' => '302',
            '0406' => '303',
            '0407' => '304',
            '0408' => '305',
            '0409' => '306',
            '0410' => '309',
            '0411' => '31',
            '0412' => [
                'plus' => ['047', '237']
            ],
            '0413' => '32',
            '0414' => '330',
            '0415' => [
                'plus' => ['33'],
                'minus' => ['330'],
                'only_positive' => true
            ],
            '0416' => [
                'plus' => ['33'],
                'minus' => ['330'],
                'only_negative' => true
            ],
            '0417' => '34',
            '0418' => '340',
            '0419' => '341',
            '0420' => '',
            '0421' => '35',
            '0422' => '350',
            '0423' => '351',
            '0424' => [
                'plus' => ['40', '41']
            ],
            '0425' => '40',
            '0426' => '400',
            '0427' => '401',
            '0428' => '403',
            '0429' => '404',
            '0430' => '405',
            '0431' => [
                'plus' => ['402', '409']
            ],
            '0432' => '41',
            '0433' => '410',
            '0434' => '411',
            '0435' => '412',
            '0436' => '413',
            '0437' => '414',
            '0438' => '415',
            '0439' => '416',
            '0440' => '419',
            '0441' => '498',
            '0442' => [
                'plus' => ['42', '43', '44', '45', '46', '47', '48', '49'],
                'minus' => ['498']
            ],
            '0443' => '42',
            '0444' => '420',
            '0445' => '421',
            '0446' => '422',
            '0447' => '423',
            '0448' => '427',
            '0449' => [
                'plus' => ['424', '425', '426', '429']
            ],
            '0450' => '430',
            '0451' => [
                'plus' => ['43'],
                'minus' => ['430']
            ],
            '0452' => '431',
            '0453' => '432',
            '0454' => '433',
            '0455' => '434',
            '0456' => '435',
            '0457' => '436',
            '0458' => '439',
            '0459' => [
                'plus' => ['44', '45', '46']
            ],
            '0460' => '47',
            '0461' => '48',
            '0462' => [
                'plus' => ['49'],
                'minus' => ['498']
            ],
            '0463' => function() {
                $first_value_5 = $this->getXMLValue0463_1(5);
                $second_value_5 = $this->getXMLValue0463_2(5);
                $first_value_6 = $this->getXMLValue0463_1(6);
                $second_value_6 = $this->getXMLValue0463_2(6);
                $first_value_7 = $this->getXMLValue0463_1(7);
                $second_value_7 = $this->getXMLValue0463_2(7);

                return [
                    'aop-0463-5' => ($first_value_5 >= 0 && $second_value_5 >= 0 && $first_value_5 === $second_value_5) ? $first_value_5 : 0,
                    'aop-0463-6' => ($first_value_6 >= 0 && $second_value_6 >= 0 && $first_value_6 === $second_value_6) ? $first_value_6 : 0,
                    'aop-0463-7' => ($first_value_7 >= 0 && $second_value_7 >= 0 && $first_value_7 === $second_value_7) ? $first_value_7 : 0
                ];
            },
            '0464' => [
                'plus' => ['3', '4']
            ],
            '0465' => '89'
        ];
    }

    protected function getXMLValueFromAccount(string $aop_code, string $account_code) : array
    {
        return [
            "aop-$aop_code-5" => $this->getXMLValueForColumn($account_code, 5),
            "aop-$aop_code-6" => $this->getXMLValueForColumn($account_code, 6),
            "aop-$aop_code-7" => $this->getXMLValueForColumn($account_code, 7),
        ];
    }
    
    protected function getXMLValueFromAccounts(string $aop_code, array $account_codes, array $aop_params) : array
    {
        $aop_5 = 0;
        $aop_6 = 0;
        $aop_7 = 0;
        foreach ($account_codes as $account_code) 
        {
            $aop_5 += $this->getXMLValueForColumn($account_code, 5);
            $aop_6 += $this->getXMLValueForColumn($account_code, 6);
            $aop_7 += $this->getXMLValueForColumn($account_code, 7);
        }
        
        return [
            "aop-$aop_code-5" => $aop_5,
            "aop-$aop_code-6" => $aop_6,
            "aop-$aop_code-7" => $aop_7
        ];
    }
    
    protected function getXMLValueFromAOPs(string $aop_code, array $aops, array $aop_params) : array
    {
        $aop_5 = 0;
        $aop_6 = 0;
        $aop_7 = 0;
        foreach ($aops as $aop) 
        {
            $aop_5 += $this->xml_values["aop-$aop-5"];
            $aop_6 += $this->xml_values["aop-$aop-6"];
            $aop_7 += $this->xml_values["aop-$aop-7"];
        }
        
        return [
            "aop-$aop_code-5" => $aop_5,
            "aop-$aop_code-6" => $aop_6,
            "aop-$aop_code-7" => $aop_7
        ];
    }
    
    private function getXMLValueForColumn(string $account_code, int $column)
    {
        if ($column === 5)
        {
            $account = $this->getAccountForYear($account_code, $this->curr_year);
        }
        else
        {
            $account = $this->getAccountForYear($account_code, $this->prev_year);
        }
        
        if (empty($account))
        {
            return 0;
        }
        
        if ($column === 5)
        {
            return $account->saldo;
        }
        else if ($column === 6)
        {
            return $account->saldo;
        }
        else if ($column === 7)
        {
            $code = (string)$account->code;
            $code_first_char = $code[0];
            //ako krece sa 0 onda ide debit_start - credit_start
            if ($code_first_char === '0')
            {
                return $account->debit_start - $account->credit_start;
            }
            //inace ako je aktiva onda debit_start a ako je pasiva onda credit_start
            else
            {
                return $account->isAssets() ? $account->debit_start : $account->credit_start;
            }
        }
        
        return 0;
    }
    
    private function getXMLValue0463_1(int $column)
    {
        return $this->xml_values["aop-0412-$column"] + $this->xml_values["aop-0416-$column"] + $this->xml_values["aop-0421-$column"] - $this->xml_values["aop-0420-$column"] - 
               $this->xml_values["aop-0417-$column"] - $this->xml_values["aop-0415-$column"] - $this->xml_values["aop-0414-$column"] - $this->xml_values["aop-0413-$column"] - 
               $this->xml_values["aop-0411-$column"] - $this->xml_values["aop-0402-$column"];
    }
    
    private function getXMLValue0463_2(int $column)
    {
        return $this->xml_values["aop-0441-$column"] + $this->xml_values["aop-0424-$column"] + $this->xml_values["aop-0442-$column"] - $this->xml_values["aop-0071-$column"];
    }
}
