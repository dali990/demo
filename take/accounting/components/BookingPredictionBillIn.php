<?php

/**
 * Staticka klasa u kojoj se predvidjaju sva proknjizavanja
 */
class BookingPredictionBillIn
{
    /**
     * 
     * @param type $document
     * @param type $carry_saldo prenos za zavisne usluge - vec prepracunat u zavisnosti od algoritma
     * @return type
     */
    public static function Booking($document, $carry_saldo = null)
    {
        $booking_transactions = [];
        
        $bill = $document->file->bill;

        $_statement_comment = $bill->bill_number . ' - ' . $bill->partner->DisplayName;
        
        /**
         * Main booking
         */
        if (!empty($carry_saldo))
        {
            self::FixWarehouseTransfers($document->account_order, $bill, $carry_saldo);
        }
//                $booking_transactions[] = self::BookingMaterials($document, $bill, $_statement_comment, $carry_saldo);    
//                $booking_transactions[] = self::BookingGoods($document, $bill, $_statement_comment, $carry_saldo); 

        return SIMAMisc::joinArrays($booking_transactions);
    }
    
    private static function FixWarehouseTransfers($account_order, $bill, $carry_saldo)
    {
        $_bill_prices = [];
        $_inc_costs = [];        
        $_algorithm = Yii::app()->configManager->get('accounting.warehouse.incremental_costs_algorithm');
        
        
        foreach ($bill->warehouse_bill_items as $item)
        {
            if (!isset($item->warehouse_material))
            {
                $_o = $item->order;
                throw new SIMAWarnException("Stavka u racunu pod brojem $_o nije povezana sa magacinskim materijalom");
            }
            $_bill_prices[$item->warehouse_material_id] = $item->value_per_unit_with_rabat; //ovo radim zbog rabata
            if ($_algorithm !== 'PER_QNT')
            {
                $_c_corr = $carry_saldo/$item->quantity;
                $_inc_costs[$item->warehouse_material_id] = $_c_corr;
            }
        }
        
        if ($_algorithm === 'PER_QNT')
        {
            $_inc_costs = $carry_saldo;
        }
        return self::correctWarehouseTransfers($account_order, $bill, $_bill_prices, $_inc_costs);
    }
    

    
    /**
     * 
     * @param AccountOrder $order - Order na koji treba da se dodaju
     * @param type $bill
     * @param type $_material_prices
     * @return array
     */
    private function correctWarehouseTransfers($order, $bill, $_payed_prices, $_in_costs = [])
    {
        foreach ($bill->warehouse_transfers as $transfer)
        {
            /**
             * setovanje vrednosti
             */
            foreach ($transfer->warehouse_transfer_items as $item)
            {
                if (is_array($_in_costs))
                {
                    $_inc_cost_per_unit = isset($_in_costs[$item->warehouse_material_id])? $_in_costs[$item->warehouse_material_id]: 0;
                }
                else
                {
                    $_inc_cost_per_unit = $_in_costs;
                }
                $item->value_per_unit = $_payed_prices[$item->warehouse_material_id] + $_inc_cost_per_unit;
                $item->corr_value_per_unit = $_inc_cost_per_unit;
                $item->save();
            }
            /**
             * pravljenje AccountDocumenta-a
             */
            if (!isset($transfer->file->account_document))
            {
                $transfer->file->account_document = new AccountDocument();
                $transfer->file->account_document->id = $transfer->id;
            }
            if ($transfer->file->account_document->account_order_id != $order->id)
            {
                $transfer->file->account_document->account_order_id = $order->id;
                $transfer->file->account_document->save();
            }
        }
    }
   
//    private function correctWarehouseCalculations(AccountOrder $order, $calcs, $_calc_data)
//    {
//        $items_to_save = [];
//        foreach ($calcs as $calc)
//        {
//            /**
//             * setovanje vrednosti
//             */
//            foreach ($calc->warehouse_transfer_items as $item)
//            {
//                $item->value_per_unit = $_calc_data[$item->warehouse_material_id]['value_per_unit'];
//                $item->value_per_unit_out = $_calc_data[$item->warehouse_material_id]['value_per_unit_out'][$calc->warehouse_to_id];
//                $items_to_save[] = $item;
//            }
//            /**
//             * pravljenje AccountDocumenta-a
//             */
//            if (!isset($calc->file->account_document))
//            {
//                $calc->file->account_document = new AccountDocument();
//                $calc->file->account_document->id = $calc->id;
//            }
//            $calc->file->account_document->account_order_id = $order->id;
//            $items_to_save[] = $calc->file->account_document;
//        }
//        return $items_to_save;
//    }
    
    
}