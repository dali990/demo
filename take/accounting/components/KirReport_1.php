<?php
class KirReport extends SIMAReport
{
    protected $month;
    protected $year;
    protected $name;
    protected $update_func = null;
    protected $css_file_name = 'css.css';
    protected $orientation = 'Landscape';
    
    protected function getModuleName()
    {
        return 'accounting';
    }
    
    public function __construct($month, $year, $name, $update_func)
    {  
        $this->month = $month;
        $this->year = $year;
        $this->name = $name;
        $this->update_func = $update_func;
        
        parent::__construct();

    }
    
    protected function getHTMLContent()
    {
        $month_model = Month::get($this->month, $this->year);
        $html_params = $this->getHtmlRows($month_model->id);
        $month_disp = $month_model->toString() . " " .  $this->year . " godine";
        
        return [
            'html' => $this->render('html', [
                'html_rows' => $html_params['html_rows'],
                'html_sum_rows' => $html_params['html_sum_rows'],
                'html_pdv_income'=> $html_params['html_pdv_income'],
                'month_disp' => $month_disp
            ], true, false)
        ];
    }
    
    public function getCSV()
    {
        $params = KIR::model()->getReportParams($this->month, $this->year, $this->update_func, true);
        $temp_file = new TemporaryFile($this->render('csv', $params, true));
        
        return $temp_file;
    }
    
    private function getHtmlRows($month_model_id)
    {
        $rows = ($this->name === KIR::class) ?
            $this->name::model()->byOrderInYear()->inMonth($this->month, $this->year)->findAll() :
            $this->name::model()->findAll([
                'model_filter' => [
                    'scopes' => ['byOrder'],
                    'month' => [
                        'ids' => $month_model_id
                    ],
                    'order_by' => 'order_in_year desc'
                ]
            ]);
        
        $sum_rows = ($this->name === KIR::class) ?
            [
                'KIR_7' => 0,
                'KIR_8' => 0,
                'KIR_9' => 0,
                'KIR_10' => 0,
                'KIR_11' => 0,
                'KIR_12' => 0,
                'KIR_13' => 0,
                'KIR_14' => 0,
                'KIR_15' => 0,
                'KIR_16' => 0,
                'KIR_17' => 0,
            ] :
            [
                'column_7' => 0,
                'column_8' => 0,
                'column_9' => 0,
                'column_10' => 0,
                'column_11' => 0,
                'column_12' => 0,
                'column_13' => 0,
                'column_14' => 0,
                'column_15' => 0,
                'column_16' => 0,
                'column_17' => 0,
            ];
        
        $i = 1;
        $rows_cnt = count($rows);
        $html_rows = "";
        $html_sum_rows = "";
        
        foreach ($rows as $row)
        {
            if (!empty($this->update_func))
            {
                call_user_func($this->update_func, round(($i++/$rows_cnt)*100, 0, PHP_ROUND_HALF_DOWN));
            }
            
            $html_rows .= "<tr class='{$row->getClasses()}'>
                <td>{$row->order_in_year}</td>
                <td>{$row->bill->booking_date}</td>
                <td>{$row->bill->bill_number}</td>
                <td>{$row->bill->income_date}</td>
                <td>{$row->bill->partner->DisplayName}</td>
                <td>{$row->bill->partner->pib_jmbg}</td>
                <td style='text-align: right;'>{$row->getAttributeDisplay(($this->name === KIR::class) ? 'KIR_7' : 'column_7')}</td>
                <td style='text-align: right;'>{$row->getAttributeDisplay(($this->name === KIR::class) ? 'KIR_8' : 'column_8')}</td>
                <td style='text-align: right;'>{$row->getAttributeDisplay(($this->name === KIR::class) ? 'KIR_9' : 'column_9')}</td>
                <td style='text-align: right;'>{$row->getAttributeDisplay(($this->name === KIR::class) ? 'KIR_10' : 'column_10')}</td>
                <td style='text-align: right;'>{$row->getAttributeDisplay(($this->name === KIR::class) ? 'KIR_11' : 'column_11')}</td>
                <td style='text-align: right;'>{$row->getAttributeDisplay(($this->name === KIR::class) ? 'KIR_12' : 'column_12')}</td>
                <td style='text-align: right;'>{$row->getAttributeDisplay(($this->name === KIR::class) ? 'KIR_13' : 'column_13')}</td>
                <td style='text-align: right;'>{$row->getAttributeDisplay(($this->name === KIR::class) ? 'KIR_14' : 'column_14')}</td>
                <td style='text-align: right;'>{$row->getAttributeDisplay(($this->name === KIR::class) ? 'KIR_15' : 'column_15')}</td>
                <td style='text-align: right;'>{$row->getAttributeDisplay(($this->name === KIR::class) ? 'KIR_16' : 'column_16')}</td>
                <td style='text-align: right;'>{$row->getAttributeDisplay(($this->name === KIR::class) ? 'KIR_17' : 'column_17')}</td>
            </tr>";

            foreach ($sum_rows as $key => $value)
            {
                $sum_rows[$key] += $row->$key;
            }
        }

        $html_sum_rows .= "<tr>
            <td colspan='6' style='text-align: center;'>&sum;</td>
            <td style='text-align: right;'>" . SIMAHtml::number_format($sum_rows[($this->name === KIR::class) ? 'KIR_7' : 'column_7']) . "</td>
            <td style='text-align: right;'>" . SIMAHtml::number_format($sum_rows[($this->name === KIR::class) ? 'KIR_8' : 'column_8']) . "</td>
            <td style='text-align: right;'>" . SIMAHtml::number_format($sum_rows[($this->name === KIR::class) ? 'KIR_9' : 'column_9']) . "</td>
            <td style='text-align: right;'>" . SIMAHtml::number_format($sum_rows[($this->name === KIR::class) ? 'KIR_10' : 'column_10']) . "</td>
            <td style='text-align: right;'>" . SIMAHtml::number_format($sum_rows[($this->name === KIR::class) ? 'KIR_11' : 'column_11']) . "</td>
            <td style='text-align: right;'>" . SIMAHtml::number_format($sum_rows[($this->name === KIR::class) ? 'KIR_12' : 'column_12']) . "</td>
            <td style='text-align: right;'>" . SIMAHtml::number_format($sum_rows[($this->name === KIR::class) ? 'KIR_13' : 'column_13']) . "</td>
            <td style='text-align: right;'>" . SIMAHtml::number_format($sum_rows[($this->name === KIR::class) ? 'KIR_14' : 'column_14']) . "</td>
            <td style='text-align: right;'>" . SIMAHtml::number_format($sum_rows[($this->name === KIR::class) ? 'KIR_15' : 'column_15']) . "</td>
            <td style='text-align: right;'>" . SIMAHtml::number_format($sum_rows[($this->name === KIR::class) ? 'KIR_16' : 'column_16']) . "</td>
            <td style='text-align: right;'>" . SIMAHtml::number_format($sum_rows[($this->name === KIR::class) ? 'KIR_17' : 'column_17']) . "</td>
        </tr>";
        
        if ($this->name === KIRManual::class)
        {
            $html_pdv_income = "<table class='pdv-income'><tr><td>Ukupan pdv:</td><td style='text-align: right;'> " . SIMAHtml::number_format($sum_rows['column_13'] + $sum_rows['column_15']) . "</td></tr>";
            $html_pdv_income .= "<tr><td>Ukupan prihod:</td><td style='text-align: right;'>" . SIMAHtml::number_format(
                $sum_rows['column_8'] + 
                $sum_rows['column_9'] + 
                $sum_rows['column_10'] + 
                $sum_rows['column_11'] + 
                $sum_rows['column_12'] + 
                $sum_rows['column_14']
            ) . "</td></tr></table>";
        }
        else
        {
            $html_pdv_income = "";
        }
            
        return [
            'html_rows'=> $html_rows,
            'html_sum_rows' => $html_sum_rows,
            'html_pdv_income'=> $html_pdv_income
        ];
    }
}