<?php

class ConnectAccountingDocuments
{
    
    /**
     * 
     * @param Bill $bill
     * @param Payment $payment
     */
    public static function BillToPayment(Bill $bill, Payment $payment)
    {
        $_br = BillRelease::model()->findByAttributes(['bill_id' => $bill->id,'payment_id' => $payment->id]);
        if (is_null($_br))
        {
            $_br = new BillRelease();
            $_br->bill_id = $bill->id;
            $_br->payment_id = $payment->id;
        }
        $_br->amount = 0;
        if ($_br->validate())
        {
            $_br->save();
        }
        else
        {
//            $this->respondFail(SIMAHtml::showErrorsInHTML($_br));
            throw new SIMAWarnException(SIMAHtml::showErrorsInHTML($_br));
        }
    }
    
    /**
     * 
     * @param PreBill $pre_bill
     * @param Payment $payment
     */
    public static function PreBillToPayment(PreBill $pre_bill, Payment $payment)
    {
        $_br = PreBillPayment::model()->findByAttributes(['pre_bill_id' => $pre_bill->id,'payment_id' => $payment->id]);
        if (is_null($_br))
        {
            $_br = new PreBillPayment();
            $_br->pre_bill_id = $pre_bill->id;
            $_br->payment_id = $payment->id;
        }
        if ($_br->validate())
        {
            $_br->save();
        }
        else
        {
            $this->respondFail(SIMAHtml::showErrorsInHTML($_br));
        }
    }
    
    /**
     * 
     * @param Bill $bill
     * @param ReliefBill $relief_bill
     */
    public static function BillToReliefBill(Bill $bill,  ReliefBill $relief_bill, $is_change = true)
    {
        $_sums_bill = $bill->remainingVatItemsArray();
        $_sums_rel =  $relief_bill->remainingVatItemsArray();
        $connection_happend = false;
        $remaining_vat_items = [];

        $bill_unreleased_amount = $bill->unreleased_amount;
        foreach (['I','R'] as $I_R_code)
        {
            $remaining_vat_items[$I_R_code] = [];
            foreach (['0','10','20'] as $_VAT_rate)
            {
                $remaining_vat_items[$I_R_code][$_VAT_rate] = [];
                $_base_release = min([
                    -$_sums_rel[$I_R_code][$_VAT_rate]['base'],
                    $_sums_bill[$I_R_code][$_VAT_rate]['base']
                ]);
                $_vat_release = min([
                    -$_sums_rel[$I_R_code][$_VAT_rate]['vat'],
                    $_sums_bill[$I_R_code][$_VAT_rate]['vat']
                ]);
                
                if ($I_R_code === 'I')
                {
                    if ($_base_release > $bill_unreleased_amount)
                    {
                        $_base_release = $bill_unreleased_amount;
                        $_vat_release = min([$_vat_release, round($_base_release*($_VAT_rate/100), 2)]);
                    }
                }
                else
                {
                    if (($_base_release + $_vat_release) > $bill_unreleased_amount)
                    {
                        $_base_release = round($bill_unreleased_amount/(1+$_VAT_rate/100),2);
                        $_vat_release = min([$_vat_release, $bill_unreleased_amount - $_base_release]);
                    }
                }
                
                $remaining_vat_items[$I_R_code][$_VAT_rate]['base'] = $_base_release;
                $remaining_vat_items[$I_R_code][$_VAT_rate]['vat'] = $_vat_release;
                
                if (SIMAMisc::lessThen(0, $_base_release))
                {
                    if ($I_R_code === 'I')
                    {
                        $_amount_release = $_base_release;
                    }
                    else
                    {
                        $_amount_release = $_base_release + $_vat_release;
                    }
                    
                    if($is_change)
                    {
                        $_conn = new BillRelief();
                        $_conn->bill_id = $bill->id;
                        $_conn->relief_bill_id = $relief_bill->id;
                        $_conn->amount = -$_amount_release;
                        $_conn->base_amount = -$_base_release;
                        $_conn->vat_rate = $_VAT_rate;
                        $_conn->vat_amount = -$_vat_release;
                        $_conn->internal_vat = $I_R_code == 'I';
                        $_conn->save();
                        $connection_happend = true;
                        $bill_unreleased_amount -= $_amount_release;
                    }
                }
            }
        }
        if (!$connection_happend && $is_change)
        {
            Yii::app()->raiseNote(Yii::t('AccountingModule.ConnectAccountingDocuments','NoConnectionBillToReliefBill'));
        }
        return $remaining_vat_items;
    }
    
    /**
     * Povezivanje racuna sa avansnim racunom
     * @param Bill $bill racun
     * @param AdvanceBill $advance_bill avansni racun
     */
    public static function BillToAdvanceBill(Bill $bill,AdvanceBill $advance_bill, $is_change = true)
    {
        $_sums_bill = $bill->remainingVatItemsArray();
        $_sums_adv =  $advance_bill->remainingVatItemsArray();
        $connection_happend = false;
        $bill_unreleased_amount = $bill->unreleased_amount;
        $remaining_vat_items = [];
        
        foreach (['I','R'] as $I_R_code)
        {
            $remaining_vat_items[$I_R_code] = [];
            foreach (['0','10','20'] as $_VAT_rate)
            {
                $remaining_vat_items[$I_R_code][$_VAT_rate] = [];
                $_base_release = min([
                    $_sums_adv[$I_R_code][$_VAT_rate]['base'],
                    $_sums_bill[$I_R_code][$_VAT_rate]['base']
                ]);
                $_vat_release = min([
                    $_sums_adv[$I_R_code][$_VAT_rate]['vat'],
                    $_sums_bill[$I_R_code][$_VAT_rate]['vat']
                ]);
                
                if ($I_R_code === 'I')
                {
                    if ($_base_release > $bill_unreleased_amount)
                    {
                        $_base_release = $bill_unreleased_amount;
                        $_vat_release = min([$_vat_release, round($_base_release*($_VAT_rate/100), 2)]);
                    }
                }
                else
                {
                    if (($_base_release + $_vat_release) > $bill_unreleased_amount)
                    {
                        $_base_release = round($bill_unreleased_amount/(1+$_VAT_rate/100),2);
                        $_vat_release = min([$_vat_release, $bill_unreleased_amount - $_base_release]);
                    }
                }
                
                $remaining_vat_items[$I_R_code][$_VAT_rate]['base'] = $_base_release;
                $remaining_vat_items[$I_R_code][$_VAT_rate]['vat'] = $_vat_release;
                
                if ($_base_release > 0)
                {
                    
                    if ($I_R_code === 'I')
                    {
                        $_amount_release = $_base_release;
                    }
                    else
                    {
                        $_amount_release = $_base_release + $_vat_release;
                    }
                    if($is_change)
                    {
                        $_conn = new AdvanceBillRelease();
                        $_conn->bill_id = $bill->id;
                        $_conn->advance_bill_id = $advance_bill->id;
                        $_conn->amount = $_amount_release;
                        $_conn->base_amount = $_base_release;
                        $_conn->vat_rate = $_VAT_rate;
                        $_conn->vat_amount = $_vat_release;
                        $_conn->internal_vat = $I_R_code == 'I';
                        $_conn->save();
                    }
                    $connection_happend = true;
                }
            }
        }
        if (!$connection_happend && $is_change)
        {
            Yii::app()->raiseNote(Yii::t('AccountingModule.ConnectAccountingDocuments','NoConnectionBillToAdvanceBill'));
        }
        return $remaining_vat_items;
    }
    
    
    /**
     * Povezivanje avansnog racuna i Knjiznog odobrenja
     * @param AdvanceBill $advance_bill
     * @param ReliefBill $relief_bill
     */
    public static function AdvanceBillToReliefInternal(AdvanceBill $advance_bill,  ReliefBill $relief_bill)
    {
        $_sums_adv = $advance_bill->remainingReliefsVatItemsArray();
        $_sums_rel =  $relief_bill->remainingVatItemsArray();
        $connection_happend = false;
        
        foreach (['I','R'] as $I_R_code)
        {
            foreach (['0','10','20'] as $_VAT_rate)
            {
                $_base_release = min([
                    -$_sums_rel[$I_R_code][$_VAT_rate]['base'],
                    $_sums_adv[$I_R_code][$_VAT_rate]['base']
                ]);
                
                
                if ($_base_release > 0)
                {
                    $_vat_release = min([
                        -$_sums_rel[$I_R_code][$_VAT_rate]['vat'],
                        $_sums_adv[$I_R_code][$_VAT_rate]['vat']
                    ]);
                    if ($I_R_code === 'I')
                    {
                        $_amount_release = $_base_release;
                    }
                    else
                    {
                        $_amount_release = $_base_release + $_vat_release;
                    }
                    $_conn = new AdvanceBillRelief();
                    $_conn->advance_bill_id = $advance_bill->id;
                    $_conn->relief_bill_id = $relief_bill->id;
                    $_conn->amount = -$_amount_release;
                    $_conn->base_amount = -$_base_release;
                    $_conn->vat_rate = $_VAT_rate;
                    $_conn->vat_amount = -$_vat_release;
                    $_conn->internal_vat = $I_R_code == 'I';
                    $_conn->save();
                    $connection_happend = true;
                }
            }
        }
        if (!$connection_happend)
        {
            Yii::app()->raiseNote(Yii::t('AccountingModule.ConnectAccountingDocuments','NoConnectionAdvanceBillToReliefInternal'));
        }
    }
    
    /**
     * Povezivanje racuna sa ugovorom o preuzimanju duga
     * @param Bill $bill racun
     * @param AdvanceBill $advance_bill avansni racun
     */
    public static function BillToDebtTakeOver(Bill $bill, DebtTakeOver $debt_take_over)
    {
        
        
        $_conn = new BillDebtTakeOver();
        $_conn->bill_id = $bill->id;
        $_conn->debt_take_over_id = $debt_take_over->id;
        if (
                ($debt_take_over->isSystemCompanyCreditor &&  $bill->invoice && $bill->partner_id === $debt_take_over->debtor_id   )
                ||
                ($debt_take_over->isSystemCompanyDebtor   && !$bill->invoice && $bill->partner_id === $debt_take_over->creditor_id )
            )
        {
            $_amount = min([
                $bill->unreleased_amount,
                $debt_take_over->unreleased_debt_value
            ]);
            
            if (SIMAMisc::lessOrEqualThen($_amount, 0))
            {
                throw new SIMAWarnException('nema dovoljno preostalo');
            }
            
            $_conn->debt_value = $_amount;
            $_conn->take_over_value = 0;
            $_conn->save();
        }
//        elseif (
//                false //za sada nije implementiran ovakav slucaj
//            )
//        {
//            $_conn->debt_value = 0;
//            $_conn->take_over_value = $_amount;
//            $_conn->save();
//        }
        else 
        {
            $msg = "Ovakvo rasknjizavanje racuna nije predvidjeno. Racun: $bill->id";
            Yii::app()->raiseNote($msg);
            error_log($msg);
        }
        
        
    }
    /**
     * Povezivanje racuna sa ugovorom o preuzimanju duga
     * @param ReliefBill $relief_bill racun
     * @param AdvanceBill $advance_bill avansni racun
     */
    public static function ReliefBillToDebtTakeOver(ReliefBill $relief_bill, DebtTakeOver $debt_take_over)
    {
        $_conn = new BillDebtTakeOver();
        $_conn->bill_id = $relief_bill->id;
        $_conn->debt_take_over_id = $debt_take_over->id;
        if (
                ($debt_take_over->isSystemCompanyCreditor  &&  $relief_bill->invoice && $relief_bill->partner_id === $debt_take_over->takes_over_id   )
                ||
                ($debt_take_over->isSystemCompanyTakesOver && !$relief_bill->invoice && $relief_bill->partner_id === $debt_take_over->creditor_id   )
            )
        {
            $_amount = min([
                -$relief_bill->unreleased_amount, //nekacija jer je knjizno odobrenje
                $debt_take_over->unreleased_take_over_value
            ]);
            
            if (SIMAMisc::lessOrEqualThen($_amount, 0))
            {
                throw new SIMAWarnException('nema dovoljno preostalo');
            }
            
            $_conn->debt_value = 0;
            $_conn->take_over_value = $_amount;
            $_conn->save();
        }
//        elseif (
//                false //nije implementirano
//            )
//        {
//            $_amount = min([
//                -$relief_bill->unreleased_amount, //nekacija jer je knjizno odobrenje
//                $debt_take_over->unreleased_debt_value
//            ]);
//            
//            if (SIMAMisc::lessOrEqualThen($_amount, 0))
//            {
//                throw new SIMAWarnException('nema dovoljno preostalo');
//            }
//            
//            $_conn->debt_value = $_amount;
//            $_conn->take_over_value = 0;
//            $_conn->save();
//        }
        else 
        {
            $msg = "Ovakvo rasknjizavanje racuna nije predvidjeno. Racun: $bill->id";
            Yii::app()->raiseNote($msg);
            error_log($msg);
        }
        
        
    }
    
    
    /**
     * Povezivanje racuna sa ugovorom o preuzimanju duga
     * @param Bill $bill racun
     * @param AdvanceBill $advance_bill avansni racun
     */
    public static function PaymentToDebtTakeOver(Payment $payment, DebtTakeOver $debt_take_over)
    {        
        $_conn = new PaymentDebtTakeOver();
        $_conn->payment_id = $payment->id;
        $_conn->debt_take_over_id = $debt_take_over->id;
        
        if (
                ($debt_take_over->isSystemCompanyCreditor &&   $payment->invoice && $debt_take_over->takes_over_id === $payment->partner_id)
                || //gore i dole conn2
                ($debt_take_over->isSystemCompanyTakesOver && !$payment->invoice && $debt_take_over->creditor_id   === $payment->partner_id)
                ||
                ($debt_take_over->isSystemCompanyDebtor &&    !$payment->invoice && $debt_take_over->takes_over_id === $payment->partner_id)

            )
        {
            $_amount = min([
                $payment->unreleased_amount,
                $debt_take_over->unreleased_take_over_value
            ]);
            
            if (SIMAMisc::lessOrEqualThen($_amount, 0))
            {
                throw new SIMAWarnException('nema dovoljno preostalo');
            }
            
            $_conn->debt_value = 0;
            $_conn->take_over_value = $_amount;
            $_conn->save();
        }
        elseif( //dole conn3
                ($debt_take_over->isSystemCompanyTakesOver && $payment->invoice && $debt_take_over->debtor_id     === $payment->partner_id)
//                ||
//                ($debt_take_over->isSystemCompanyTakesOver && $payment->invoice && $debt_take_over->debtor_id     === $payment->partner_id)
//                ||
//                ($debt_take_over->isSystemCompanyTakesOver && $payment->invoice && $debt_take_over->debtor_id     === $payment->partner_id)
            )
            
        {
            $_amount = min([
                $payment->unreleased_amount,
                $debt_take_over->unreleased_debt_value
            ]);
            
            if (SIMAMisc::lessOrEqualThen($_amount, 0))
            {
                throw new SIMAWarnException('nema dovoljno preostalo');
            }
            
            $_conn->debt_value = $_amount;
            $_conn->take_over_value = 0;
            $_conn->save();
        }
        else
        {
            $msg = "Ovakvo rasknjizavanje placanja nije predvidjeno. Placanje: $payment->id";
            Yii::app()->raiseNote($msg);
            error_log($msg);
        }
    }
    
    
}