<?php

/**
 * Staticka klasa u kojoj se povezuju dokumenti koji treba zajedno da se knjize
 */
class ConnectAccountOrderDocuments
{
    /**
     * Povezuje dokument na nalog za knjizenje. 
     * Ukoliko je jedan od parametara 
     * @param type $document_id
     * @return boolean Da li je uspeo da nadje AccountOrder
     */
    public static function predictDocumentsAccountOrder($document_id)
    {
        //ukoliko je fajl sam nalog za knjizenje
        $file = File::model()->findByPk($document_id);
        $ao = AccountOrder::model()->findByPk($document_id);
        
        
        
        $auto_connect_advance_bill_account_order_to_bank_statement_account_order = 
                Yii::app()->configManager->get('accounting.auto_connect_advance_bill_account_order_to_bank_statement_account_order');;
        if (!is_null($ao)) //UKOLIKO JE DOKUMENT NALOG ZA KNJIZENJE
        {
            $ao->save(); //samo uradi resave i dodace svoj AccountDocument na svoj AccountOrder
        }
        elseif //ukoliko je dokument bilo koji magacinski dokuemnt i ima povezan racun
            (
                isset($file->warehouse_transfer)
                && isset($file->warehouse_transfer->bill)
                && isset($file->warehouse_transfer->bill->file->account_document)
            )
        {
            $guess_account_order_id = $file->warehouse_transfer->bill->file->account_document->account_order_id;
        }
        elseif (isset($file->bill_in))//ukoliko je dokument ulazni racun
        {
            foreach ($file->bill_in->warehouse_receives as $rec)
            {
                if (isset($rec->file->account_document))
                {
                    if (empty($guess_account_order_id))
                    {
                        $guess_account_order_id = $rec->file->account_document->account_order_id;
                    }
                    else
                    {
                        throw new SIMAWarnException('Racun '.$file->bill_in->DisplayName
                                .' ima prijemnice koje su na razlicitim nalozima za knjizenje');
                    }
                }
            }
            //ovo mozda treba izbaciti ukoliko se predvidja samo za trenutni dokument
            if (!empty($guess_account_order_id)) //povezi i ostale prijemnice
            {
                foreach ($file->bill_in->warehouse_receives as $rec)
                {
                    self::addDocumentToOrder($guess_account_order_id, $rec->id);
                }
            }
        }
        elseif (isset($file->bill_out))//ukoliko je dokument izlazni racun
        {
            foreach ($file->bill_out->warehouse_dispatches as $rec)
            {
                if (isset($rec->file->account_document))
                {
                    if (empty($guess_account_order_id))
                    {
                        $guess_account_order_id = $rec->file->account_document->account_order_id;
                    }
                    else
                    {
                        throw new SIMAWarnException('Racun '.$file->bill_out->DisplayName
                                .' ima prijemnice koje su na razlicitim nalozima za knjizenje');
                    }
                }
            }
            //ovo mozda treba izbaciti ukoliko se predvidja samo za trenutni dokument
            if (!empty($guess_account_order_id)) //povezi i ostale prijemnice
            {
                foreach ($file->bill_out->warehouse_dispatch as $rec)
                {
                    self::addDocumentToOrder($guess_account_order_id, $rec->id);
                }
            }
        }
        elseif (isset($file->advance_bill))//ukoliko je dokument avansni racun
        {
            if ($auto_connect_advance_bill_account_order_to_bank_statement_account_order)
            {
                foreach ($file->advance_bill->releases as $_rel)
                {
                    if (isset($_rel->payment->bank_statement->file->account_document))
                    {
                        if (empty($guess_account_order_id))
                        {
                            $guess_account_order_id = $_rel->payment->bank_statement->file->account_document->account_order_id;
                        }
                        else
                        {
                            unset($guess_account_order_id);
                            Yii::app()->raiseNote('Avansni racun '.$file->advance_bill->DisplayName
                                    .' ima avanse koje su na razlicitim nalozima za knjizenje '
                                    . '- izaberite na kom nalogu ce biti proknjizen');
                            break;
                        }
                    }
                }
            }
        }
        elseif (isset($file->bank_statement))
        {
            if ($auto_connect_advance_bill_account_order_to_bank_statement_account_order)
            {
                foreach ($file->bank_statement->payments as $payment)
                {
                    foreach ($payment->releases as $_rel)
                    {
                        if ($_rel->bill->isAdvanceBill && isset($_rel->bill->file->account_document))
                        {
                            if (empty($guess_account_order_id))
                            {
                                $guess_account_order_id = $_rel->bill->file->account_document->account_order_id;
                            }
                            else
                            {
                                throw new SIMAWarnException('Avansni racun '.$file->advance_bill->DisplayName
                                        .' ima avanse koje su na razlicitim nalozima za knjizenje');
                            }
                        }
                    }
                }
            }
        }
        elseif (isset($file->cash_desk_log))
        {
            foreach($file->cash_desk_log->cash_desk_orders as $order)
            {
                if (isset($order->file->account_document))
                {
                    $_new_order_id = $order->file->account_document->account_order_id;
                    if (empty($guess_account_order_id) || (intval($guess_account_order_id) == intval($_new_order_id)))
                    {
                        $guess_account_order_id = $_new_order_id;
                    }
                    else
                    {
                        throw new SIMAWarnException('Dnevni izvestaj blagajne '.$file->cash_desk_log->DisplayName
                                .' ima naloge koji su na razlicitim nalozima za knjizenje');
                    }
                }
            }
        }
        elseif (isset($file->cash_desk_order))
        {
            if (isset($file->cash_desk_order->cash_desk_log->file->account_document))
            {
                $_rel = $file->cash_desk_order->cash_desk_log->file->account_document;
                if (empty($guess_account_order_id))
                {
                    $guess_account_order_id = $_rel->account_order_id;
                }
                else
                {
                    throw new SIMAException('Nalog blagajni '.$file->cash_desk_order->DisplayName
                            .' vec nekako ima predvidjanje??');
                }
            }
        }

        if (isset($guess_account_order_id))
        {
            return self::addDocumentToOrder($guess_account_order_id, $document_id);
        }
        else
        {
            return false;
        }
    } 
        
        
    /**
     * Za zadati nalog za knjizenje dodaje sve povezane dokumente
     * @param integer $account_order_id ID naloga za knjizenje
     * @return int broj dodatih dokumenata
     * @throws Exception
     */
    public static function addDocumentsToAccountOrder($account_order_id, $updateFunction)
    {
        $total_count = 0;
        
        $_order = AccountOrder::model()->findByPk($account_order_id);
        if (is_null($_order))
        {
            throw new Exception('ne postoji nalog');
        }
        $count = -1;
        while($count>0 || $count==-1)
        {
            $count = 0;
            $_order->refresh();
            foreach ($_order->account_documents as $_document)
            {
                $file = $_document->file;
                if //ukoliko je dokument prijemnica
                    (
                        isset($file->warehouse_transfer) 
                        && !empty($file->warehouse_transfer->bill_id)
                    )
                {
                    if (self::addDocumentToOrder($account_order_id, $file->warehouse_transfer->bill_id))
                    {
                        $count++;
                    }
                }
                elseif (isset($file->bill_in))//ukoliko je dokument ulazni racun
                {
                    foreach ($file->bill_in->warehouse_receives as $rec)
                    {
                        if (self::addDocumentToOrder($account_order_id, $rec->id))
                        {
                            $count++;
                        }
                    }
                }
                elseif (isset($file->bill_out))//ukoliko je dokument ulazni racun
                {
                    foreach ($file->bill_out->warehouse_dispatches as $rec)
                    {
                        if (self::addDocumentToOrder($account_order_id, $rec->id))
                        {
                            $count++;
                        }
                    }
                }
                elseif (isset($file->cash_desk_log))
                {
                    foreach ($file->cash_desk_log->cash_desk_orders as $order)
                    {
                        if (self::addDocumentToOrder($account_order_id, $order->id))
                        {
                            $count++;
                        }
                    }
                }
                //MilosS: ne prikljucuje se izvod avansu, nego avans izvodu
//                elseif (isset($file->advance_bill))//ukoliko je dokument avansni racun
//                {
//                    foreach ($file->advance_bill->releases as $_rel)
//                    {
//                        if (self::addDocumentToOrder($account_order_id, $_rel->payment->bank_statement_id, false))
//                        {
//                            $count++;
//                        }
//                    }
//                }
                elseif (isset($file->bank_statement))
                {
                    foreach ($file->bank_statement->payments as $payment)
                    {
                        foreach ($payment->releases as $_rel)
                        {
                            if ($_rel->bill->isAdvanceBill)
                            {
                                if (!isset($_rel->bill->file->account_document))
                                {
                                    Yii::app()->raiseNote('Avansni racun '.$_rel->bill->DisplayName.' je vezan za '
                                            . ' placanje na ovom izvodu, a ne nalazi se ni na jednom nalogu za knjizenje');
                                }
                            }
                        }
                    }
                }
                elseif (isset($file->regular_paycheck_period))
                {
                    foreach ($file->regular_paycheck_period->paychecks as $paycheck)
                    {
                        if (isset($paycheck->file))
                        {
                            if (self::addDocumentToOrder($account_order_id, $paycheck->file_id))
                            {
                                $count++;
                            }
                        }
                        else
                        {
//                            throw new SIMAWarnNoOZPapers($file->regular_paycheck_period);
                        }
//                        foreach ($payment->releases as $_rel)
//                        {
//                            if ($_rel->bill->isAdvanceBill)
//                            {
//                                if (!isset($paycheck->file->account_document))
//                                {
//                                    Yii::app()->raiseNote('Avansni racun '.$_rel->bill->DisplayName.' je vezan za '
//                                            . ' placanje na ovom izvodu, a ne nalazi se ni na jednom nalogu za knjizenje');
//                                }
//                            }
//                        }
                    }
                }
            }
            
            $total_count+=$count;
        }
        
        return $total_count;
    }
    
    /**
     * Povezuje dokument na nalog. 
     * Ukoliko je dokument vec povezan na nalog, nikom nista, 
     * ali ako je povezan na drugi, baca Warn
     * @param type $account_order_id ID naloga za knjizenje
     * @param type $document_id ID fajla koji se dodaje na nalog
     * @param $throw_exception - da li da baci Exception ako ne uspe dodavanje. Ako ne, onda setuje obavestenje
     * @throws Exception ukoliko nisu dobro prosledjeni parametri
     */
    public static function addDocumentToOrder($account_order_id, $document_id, $throw_exception = true, $forced_change = false)
    {
        $account_order = AccountOrder::model()->findByPk($account_order_id);
        $file = File::model()->findByPk($document_id);
        
        if (is_null($account_order) || is_null($file))
        {
            throw new Exception('Nisu navedeni nalog za knjizenje ili dokument');
        }
        if (!isset($file->account_document))
        {
            $file->beforeChangeLock();
            if ($file->id != $account_order_id && $account_order->booked)
            {
                $account_order->booked = false;
                $account_order->save();
                Yii::app()->raiseNote('Nalogu '.$account_order->DisplayName
                        .' je skinuta potvrda jer mu je dodat dokument '.$file->DisplayName);
            }
            $file->account_document = new AccountDocument();
            $file->account_document->id = $file->id;   
            $file->account_document->account_order_id = $account_order_id;
            $file->account_document->save();
            $file->save();
            $file->afterChangeUnLock();
            return true;
        }
        elseif($file->account_document->account_order_id != $account_order_id)
        {
            if ($forced_change)
            {
                $file->account_document->account_order_id = $account_order_id;
                $file->account_document->save();
            }
            else
            {
                $_warn_text = 'Dokument '.$file->DisplayName.' se vec nalazi na nalogu za knjizenje '
                    .$file->account_document->account_order->DisplayName
                    .' tako da ne moze biti dodat na nalog '
                    .$account_order->DisplayName;
        
                if ($throw_exception)
                {
                    throw new SIMAWarnException($_warn_text);
                }
                else
                {
                    Yii::app()->raiseNote($_warn_text);
                }
            }
        }
        return false;
    }
    
    /**
     * Povezuje dokument na storniran nalog. 
     * Ukoliko je dokument vec povezan na nalog, nikom nista, 
     * ali ako je povezan na drugi, baca Warn
     * @param type $account_order_id ID naloga za knjizenje
     * @param type $document_id ID fajla koji se dodaje na nalog
     * @param $throw_exception - da li da baci Exception ako ne uspe dodavanje. Ako ne, onda setuje obavestenje
     * @throws Exception ukoliko nisu dobro prosledjeni parametri
     */
    public static function addCanceledDocumentToOrder(AccountOrder $account_order, File $file, $throw_exception = true, $forced_change = false)
    {
        $_warns = [];
        $note_code = SIMAHtml::uniqid();
        $account_document = $file->account_document;
        $account_order_id = $account_order->id;
        if (is_null($account_document))
        {
            $this->respondFail("Nalog za storno može da se postavi samo na proknjiženi dokument");
        }

        if($account_document->account_order_id == $account_order_id)
        {
            $_warns[] = "Nalog za storno ne može biti isti kao nalog za knjiženje";
        }
        elseif  (
                    !empty($account_document->canceled_account_order_id)
                    &&
                    $account_document->canceled_account_order_id != $account_order_id
                    &&
                    !$forced_change
                )
        {

            $_warns = 'Dokument '.$account_document->DisplayName.' se vec nalazi na nalogu za knjizenje '
                .$account_document->account_order->DisplayName
                .' tako da ne moze biti dodat na nalog '
                .$account_order->DisplayName;

        }
        
        if (!empty($_warns))
        {
            if ($throw_exception)
            {
                throw new SIMAWarnException(implode(', ', $_warns));
            }
            else
            {
                foreach ($_warns as $_warn_text)
                {
                    Yii::app()->raiseNote($_warn_text,$note_code);
                }
                
            }
        }
        else
        {
            $account_document->canceled_account_order_id = $account_order->id;
            $account_document->save();
            if ($account_order->booked)
            {
                $account_order->booked = false;
                $account_order->save();
                Yii::app()->raiseNote('Nalogu '.$account_order->DisplayName.' je skinuta potvrda', $note_code);
            }
        }
    }
   
}