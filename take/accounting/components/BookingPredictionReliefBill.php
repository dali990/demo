<?php

/**
 * Staticka klasa u kojoj se predvidjaju sva proknjizavanja
 */
class BookingPredictionReliefBill
{
    /**
     * 
     * @param type $document
     * @param type $carry_saldo prenos za zavisne usluge
     * @return type
     */
    public static function Booking($document)
    {
        $booking_transactions = [];
        
        $relief_bill = $document->file->relief_bill;
        if (!isset($relief_bill))
        {
            $relief_bill = $document->file->un_relief_bill;
        }
        if (!isset($relief_bill))
        {
            throw new SIMAException('doslo je do knjizenja odobrenja a nije trebalo');
        }
        
//        if ($relief_bill->bill_items_count > 1)
//        {
//            $note = 'Knjižno odobrenje: '.$relief_bill->DisplayName.' ima više stavki, a to trenutno nije podržano. Knjižno odobrenje nije proknjiženo';
//            Yii::app()->raiseNote($note);
//            Yii::app()->errorReport->createAutoClientBafRequest($note);
//            return [];
//        }
        
        $_statement_comment = $relief_bill->bill_number . ' - ' . $relief_bill->partner->DisplayName;
        
        /**
         * vat booking 
         */
//        $vat_bookings = [];
//        if (self::bookVat($relief_bill))
//        {
//            //zaduzenje PDV-a
//            $vat_bookings = self::BookingVat($document, $relief_bill, $_statement_comment);
//        }

        /**
         * Partner booking
         */
        $partner_bookings = self::BookingPartner($document, $relief_bill, $_statement_comment);

        /**
         * advance VAT booking - veza sa avansnim racunom
         */
//        $AR_bookings = [];
//        if (Yii::app()->company->inVat && $bill->vat_refusal)
//        {
//            $AR_bookings = self::BookingAdvanceBillLink($document, $bill, $_statement_comment.' - AR');
//        }
//        
//        /**
//         * advance payment booking
//         */
//        $A_bookings = self::BookingAdvancePaymentLink($document, $bill, $_statement_comment.' - A');

        /**
         * Main booking
         */
//        if ($relief_bill->invoice)
//        {
            $booking_transactions[] = self::BookingServices($document, $relief_bill, $_statement_comment);//izlazni racuni nemaju zavisne troskove
//            $booking_transactions[] = $vat_bookings;
            $booking_transactions[] = $partner_bookings;
//        }
//        else
//        {
//            $booking_transactions[] = $vat_bookings;
//            $booking_transactions[] = $partner_bookings;
//        }
        
        
        
        return SIMAMisc::joinArrays($booking_transactions);
    }
   
    /**
     * 
     * @param ReliefBill/UnReliefBill $relief_bill
     * @return bool
     */
//    private static function bookVat(Bill $relief_bill):bool
//    {
//        return Yii::app()->company->inVat && 
//                (
//                    $relief_bill->vat_refusal
//                    ||
//                    $relief_bill->invoice
//                );
//    }
 
        
    /**
     * Zaduzivanje partnera - partner se uvek zaduzuje
     * @param type $document
     * @param type $bill
     * @param type $_statement_comment
     */
    private static function BookingPartner($document, $bill, $_statement_comment)
    {
        $booking_transactions = [];
        $partner_account = null;
        
        //godina iz naloga za knjizenje
        $_year = $document->account_order->year;
        
        if ( $bill->invoice) 
        {
            $partner_account = $bill->partner->getCustomerAccount($_year);
        }
        if (!$bill->invoice) 
        {
            $partner_account = $bill->partner->getSupplierAccount($_year);
        }    
        
        $tr = new AccountTransaction();
        $tr->account_id = $partner_account->id;
        $tr->account_document_id = $document->id;
        $tr->comment = $_statement_comment;
        if ($bill->invoice)
        {
            $tr->debit = $bill->amount;
        }
        else
        {
            $tr->credit = $bill->amount;
        }
        $booking_transactions[] = $tr;
        
        return $booking_transactions;
    }
    
    /**
     * 
     * @param AccountDocument $document
     * @param type $bill
     * @param type $_statement_comment
     * @return \AccountTransaction
     */
//    private static function BookingVat(AccountDocument $document, $bill, $_statement_comment)
//    {
//        $booking_transactions = [];
//        $vat_small_account = null;
//        $vat_big_account = null;
//        
//        //godina iz naloga za knjizenje
//        $_year = $document->account_order->year;
//        
//        if ( $bill->invoice) 
//        {
//            $vat_small_account  = Account::getFromParam('accounting.codes.vat.outcome_small',$_year)->getLeafAccount();
//            $vat_big_account    = Account::getFromParam('accounting.codes.vat.outcome_big',$_year)->getLeafAccount();
//        }
//        if (!$bill->invoice) //kupac
//        {
//            $vat_small_account  = Account::getFromParam('accounting.codes.vat.income_small',$_year)->getLeafAccount();
//            $vat_big_account    = Account::getFromParam('accounting.codes.vat.income_big',$_year)->getLeafAccount();
//        }    
//        
//        if (abs($bill->vat10) > 0) //ukoliko ima PDV
//        {
//            $tr = new AccountTransaction();
//            $tr->account_id = $vat_small_account->id;
//            $tr->account_document_id = $document->id;
//            $tr->comment = $_statement_comment;
//            if ($bill->invoice)
//            {
//                $tr->credit = $bill->vat10;
//            }
//            else
//            {
//                $tr->debit = $bill->vat10;
//            }
//            $booking_transactions[] = $tr;
//        }
//
//        if (abs($bill->vat20) > 0) //ukoliko ima PDV
//        {
//            $tr = new AccountTransaction();
//            $tr->account_id = $vat_big_account->id;
//            $tr->account_document_id = $document->id;
//            $tr->comment = $_statement_comment;
//            if ($bill->invoice)
//            {
//                $tr->credit = $bill->vat20;
//            }
//            else
//            {
//                $tr->debit = $bill->vat20;
//            }
//            $booking_transactions[] = $tr;
//        }
//
//        return $booking_transactions;
//    }

    /**
     * 
     * @param AccountDocument $document
     * @param ReliefBill/UnReliefBill $bill
     * @param string $_statement_comment
     * @return array
     */
    private static function BookingServices(AccountDocument $document, Bill $bill, string $_statement_comment):array
    {
        $booking_transactions = [];
        
        $_year = $document->account_order->year;
        $company_in_vat = Yii::app()->company->inVat;
        $_VATs_for_bookings = [];
        $_book_separatly = boolval(Yii::app()->configManager->get('accounting.book_services_separately'));

        foreach ($bill->bill_items as $item)
        {
            $job_order_account_base = $item->getBookingAccount($_year);
            $use_vat = $company_in_vat && $item->vat_refusal;
            $_local_used_amount1 = $item->value;
                
            if (!$use_vat)
            {
                $_local_used_amount1 += $item->value_vat;
            }
        
            if (!is_null($job_order_account_base))
            {
                if (!isset($item->cost_location))
                {
                    $cost_location_account = $job_order_account_base;
                }
                else
                {
                    $cost_location_account = $job_order_account_base->getLeafAccount($item->cost_location);
                }

                $booking_side = $bill->invoice?'credit':'debit';
                
                $tr = new AccountTransaction();
                $tr->account_id = $cost_location_account->id;
                $tr->account_document_id = $document->id;
                $tr->comment = $_statement_comment;
                $tr->$booking_side = $_local_used_amount1;
                
                $booking_transactions[] = $tr;
            }
            if ($use_vat)
            {
                $vat_bookings = self::BookingVat($document, $bill, $item, $_statement_comment);
                foreach ($vat_bookings as $_one_booking_item)
                {
                    if (!$_book_separatly)
                    {
                        $pair_found = false;
                        foreach ($_VATs_for_bookings as $key => $pair)
                        {
                            if ($pair['account']->id == $_one_booking_item->account_id)
                            {
                                $_VATs_for_bookings[$key]['credit'] += $_one_booking_item->credit;
                                $_VATs_for_bookings[$key]['debit']  += $_one_booking_item->debit;
                                $pair_found = true;
                            }
                        }
                        if (!$pair_found)
                        {
                            $_VATs_for_bookings[] = [
                                'account' => $_one_booking_item->account,
                                'credit' => $_one_booking_item->credit,
                                'debit' => $_one_booking_item->debit
                            ];
                        }
                    }
                    else 
                    {
                        $booking_transactions[] = $_one_booking_item;
                    }
                }
            }
            
        }
        
        foreach ($_VATs_for_bookings as $pair)
        {
            $tr = new AccountTransaction();
            $tr->account_id = $pair['account']->id;
            $tr->account_document_id = $document->id;
            $tr->comment = $_statement_comment;
            $tr->credit = $pair['credit'];
            $tr->debit = $pair['debit'];
            $booking_transactions[] = $tr;
        }

        return $booking_transactions;
    }   
    
    private static function BookingVat($document,$bill, $bill_item, $_statement_comment)
    {
        $booking_transactions = [];
        
        //godina iz naloga za knjizenje, mada mora da bude ista i iz izvoda
        $_year = $document->account_order->year;
        
        /**TODO(miloss): SPECIJALNA VEZA ZA MAGACIN*/
        $_conn_model = null;
        $ao = $document->account_order;
        foreach ($ao->account_documents as $_ad) 
        {
            if (isset($_ad->file->warehouse_receiving))
            {
                $_conn_model = $_ad->file->warehouse_receiving->warehouse_to;
            }
        }
        /** */
        if ($bill_item->is_internal_vat)
        {
            //Za izlazne racune se ne obracunava interni PDV
            if ( ! $bill->invoice)
            {
                $internal_vat_small_account_credit  = Account::getFromParam('accounting.codes.vat.income_small_internal',$_year)->getLeafAccount($_conn_model);
                $internal_vat_small_account_debit  = Account::getFromParam('accounting.codes.vat.outcome_small_internal',$_year)->getLeafAccount($_conn_model);
                $internal_vat_big_account_credit    = Account::getFromParam('accounting.codes.vat.income_big_internal',$_year)->getLeafAccount($_conn_model);
                $internal_vat_big_account_debit    = Account::getFromParam('accounting.codes.vat.outcome_big_internal',$_year)->getLeafAccount($_conn_model);

    //            if ($bill->vat10 > 0) //ukoliko ima PDV
                if ($bill_item->internal_vat == '10')
                {

                    $tr2 = new AccountTransaction();
                    $tr2->account_id = $internal_vat_small_account_credit->id;
                    $tr2->account_document_id = $document->id;
                    $tr2->comment = $_statement_comment;
                    $tr2->debit = $bill_item->value_internal_vat;
                    $booking_transactions[] = $tr2;

                    $tr1 = new AccountTransaction();
                    $tr1->account_id = $internal_vat_small_account_debit->id;
                    $tr1->account_document_id = $document->id;
                    $tr1->comment = $_statement_comment;
                    $tr1->credit = $bill_item->value_internal_vat;
                    $booking_transactions[] = $tr1;
                }

    //            if ($bill->vat20 > 0) //ukoliko ima PDV
                if ($bill_item->internal_vat == '20')
                {
                    $tr2 = new AccountTransaction();
                    $tr2->account_id = $internal_vat_big_account_credit->id;
                    $tr2->account_document_id = $document->id;
                    $tr2->comment = $_statement_comment;
                    $tr2->debit = $bill_item->value_internal_vat;
                    $booking_transactions[] = $tr2;

                    $tr1 = new AccountTransaction();
                    $tr1->account_id = $internal_vat_big_account_debit->id;
                    $tr1->account_document_id = $document->id;
                    $tr1->comment = $_statement_comment;
                    $tr1->credit = $bill_item->value_internal_vat;
                    $booking_transactions[] = $tr1;
                }
            }
        }
        else
        {
            $vat_small_account = null;
            $vat_big_account = null;
            if ( $bill->invoice) 
            {
                $vat_small_account  = Account::getFromParam('accounting.codes.vat.outcome_small',$_year)->getLeafAccount($_conn_model);
                $vat_big_account    = Account::getFromParam('accounting.codes.vat.outcome_big',$_year)->getLeafAccount($_conn_model);
            }
            if (!$bill->invoice) //kupac
            {
                $vat_small_account  = Account::getFromParam('accounting.codes.vat.income_small',$_year)->getLeafAccount($_conn_model);
                $vat_big_account    = Account::getFromParam('accounting.codes.vat.income_big',$_year)->getLeafAccount($_conn_model);
            }    

//            if ($bill->vat10 > 0) //ukoliko ima PDV
            if ($bill_item->vat == '10')
            {
                $tr = new AccountTransaction();
                $tr->account_id = $vat_small_account->id;
                $tr->account_document_id = $document->id;
                $tr->comment = $_statement_comment;
                if ($bill->invoice)
                {
                    $tr->credit = $bill_item->value_vat;
                }
                else
                {
                    $tr->debit = $bill_item->value_vat;
                }
                $booking_transactions[] = $tr;
            }

//            if ($bill->vat20 > 0) //ukoliko ima PDV
            if ($bill_item->vat == '20')
            {
                $tr = new AccountTransaction();
                $tr->account_id = $vat_big_account->id;
                $tr->account_document_id = $document->id;
                $tr->comment = $_statement_comment;
                if ($bill->invoice)
                {
                    $tr->credit = $bill_item->value_vat;
                }
                else
                {
                    $tr->debit = $bill_item->value_vat;
                }
                $booking_transactions[] = $tr;
            }
        }
        
        return $booking_transactions;
    }
    

}