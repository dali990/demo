<?php

/**
 * Staticka klasa u kojoj se predvidjaju sva proknjizavanja
 */
class BookingPredictionCashDeskLog
{
    public static function Booking($document)
    {
        $booking_transactions = [];
        $cd = $document->file->cash_desk_log;

        if (!is_null($cd))
        {
            
            $booking_transactions[] = self::BookingOrders($document,$cd);
        }
        
        return SIMAMisc::joinArrays($booking_transactions);
    }
    
    public static function BookingSums($document,$_cd_log, $_cd_order, $_statement_comment)
    {
        $booking_transactions = [];
        
//        
        $cash_desk_account = $_cd_log->account->getLeafAccount();
        $_statement_comment = $_cd_log->DisplayName;
        
        
//        if ($_cd_order->in > 0)
//        {

            $tr = new AccountTransaction();
            $tr->account_id = $cash_desk_account->id;
            $tr->account_document_id = $document->id;
            $tr->comment = $_statement_comment;
            if ($_cd_order->isIN)
            {
                $tr->debit = $_cd_order->calcAmount();
            }
            else
            {
                $tr->credit = $_cd_order->calcAmount();
            }
            $booking_transactions[] = $tr;
//        }
//        
//        if ($_cd_log->out > 0)
//        {
//
//            $tr = new AccountTransaction();
//            $tr->account_id = $cash_desk_account->id;
//            $tr->account_document_id = $document->id;
//            $tr->comment = $_statement_comment;
//            $tr->credit = $_cd_log->out;
//            $booking_transactions[] = $tr;
//        }
        
        return $booking_transactions;
    }
    
    public static function BookingOrders($document,$cdl)
    {
        $booking_transactions = [];
        
        foreach ($cdl->cash_desk_orders_in as $_cd_order)
        {
            $_statement_comment = $_cd_order->DisplayName;
            $booking_transactions[] = self::BookingSums($document, $cdl, $_cd_order, $_statement_comment);
            switch ($_cd_order->type) 
            {
                case CashDeskOrder::$PAYIN:     $booking_transactions[] = self::BookingPayin($document, $_cd_order, $_statement_comment); break;
                case CashDeskOrder::$SUBMIT:    Yii::app()->raiseNote('Knjizenje isplata na ziro racun nije implementirano');break;
            }
        }
        
        foreach ($cdl->cash_desk_orders_out as $_cd_order)
        {
            $_statement_comment = $_cd_order->DisplayName;
            
            switch ($_cd_order->type) 
            {
                case CashDeskOrder::$PAYOUT:    $booking_transactions[] = self::BookingPayout($document, $_cd_order, $_statement_comment); break;
                case CashDeskOrder::$WITHDRAW:  $booking_transactions[] = self::BookingWithdraw($document, $_cd_order, $_statement_comment); break;
            }
            $booking_transactions[] = self::BookingSums($document, $cdl, $_cd_order, $_statement_comment);
        }
        
        return SIMAMisc::joinArrays($booking_transactions);
    }

    public static function BookingPayout($document, $_cd_order, $_statement_comment)
    {
        $booking_transactions = [];
        
        //godina iz naloga za knjizenje
        $_year = $document->account_order->year;
        
        foreach ($_cd_order->cash_desk_items as $_cd_item)
        {
            if (!isset($_cd_item->cost_type))
            {
                Yii::app()->raiseNote('Niste podesili vrstu troska za stavku "'.$_cd_item->text.'" u nalogu "'.$_cd_order->DisplayName.'"');
            }
            else
            {
                $costs_acount = $_cd_item->cost_type->accountForYear($_year)->getLeafAccount();
                if(is_null($costs_acount))
                {
                    Yii::app()->raiseNote("Ne postoji vrsta troška '{$_cd_item->cost_type->DisplayName}' za godinu {$_year->year}.");
                }
                else
                {
                    $tr = new AccountTransaction();
                    $tr->account_id = $costs_acount->id;
                    $tr->account_document_id = $document->id;
                    $tr->comment = $_statement_comment;
                    $tr->debit = $_cd_item->amount;
                    $booking_transactions[] = $tr;
                }
            }
        }
        
        return $booking_transactions;
    }
    
    public static function BookingPayin($document, $_cd_order, $_statement_comment)
    {
//        Yii::app()->raiseNote('Knjizenje uplata u blagajnu nije implementirano'); 
        $booking_transactions = [];
        
        //godina iz naloga za knjizenje
        $_year = $document->account_order->year;
        
        foreach ($_cd_order->cash_desk_items as $_cd_item)
        {
            if (!isset($_cd_item->cost_type))
            {
                Yii::app()->raiseNote('Niste podesili vrstu troska za stavku "'.$_cd_item->text.'" u nalogu "'.$_cd_order->DisplayName.'"');
            }
            else
            {
                $costs_acount = $_cd_item->cost_type->accountForYear($_year)->getLeafAccount();
                if(is_null($costs_acount))
                {
                    Yii::app()->raiseNote("Ne postoji vrsta troška '{$_cd_item->cost_type->DisplayName}' za godinu {$_year->year}.");
                }
                else
                {
                    $tr = new AccountTransaction();
                    $tr->account_id = $costs_acount->id;
                    $tr->account_document_id = $document->id;
                    $tr->comment = $_statement_comment;
                    $tr->credit = $_cd_item->amount;
                    $booking_transactions[] = $tr;
                }
            }
        }
        
        return $booking_transactions;
    }
    
    public static function BookingWithdraw($document, $_cd_order, $_statement_comment)
    {
        $booking_transactions = [];
        
        //godina iz naloga za knjizenje
        $_year = $document->account_order->year;
        
        foreach ($_cd_order->cash_desk_items as $_cd_item)
        {
            $cash_desk_payment_type_id = Yii::app()->configManager->get('accounting.cash_desk_payment_type', false);
            if (empty($cash_desk_payment_type_id))
            {
                Yii::app()->raiseNote('Nije podesena vrsta placanja za prenos na blagajnu');
            }
            else
            {
                $cash_desk_payment_type = PaymentType::model()->findByPk($cash_desk_payment_type_id);
                if (empty($cash_desk_payment_type))
                {
                    throw new SIMAWarnException('Postavljen je neposojeci tip placanja pod parametrom prenos blagajne');
                }
                $cash_desk_account = $cash_desk_payment_type->accountForYear($_year)->getLeafAccount();
                
                $tr = new AccountTransaction();
                $tr->account_id = $cash_desk_account->id;
                $tr->account_document_id = $document->id;
                $tr->comment = $_statement_comment;
                $tr->credit = $_cd_item->amount;
                $booking_transactions[] = $tr;
            }
        }
        
        return $booking_transactions;
    }
    
}