<?php

/**
 * Staticka klasa u kojoj se predvidjaju sva proknjizavanja
 */
class BookingPredictionVatMonth
{
    public static function Booking($document)
    {
//        $booking_transactions = [];
        $a_month = $document->vat_month;

        $_statement_comment = 'PDV ' . $a_month->year . ' - ' . $a_month->month;

        $booking_transactions = self::BookingVatMonth($document, $a_month, $_statement_comment);
        
        
        return $booking_transactions;
    }

    /**
     * TODO: ubaciti parametre!!
     * @param AccountDocument $document
     * @param type $bill
     * @param type $_statement_comment
     */
    private static function BookingVatMonth(AccountDocument $document, AccountingMonth $a_month, $_statement_comment)
    {
        $booking_transactions = [];
        
        //godina iz naloga za knjizenje
        $_year = $document->account_order->year;
        $_ao_id = $document->account_order_id;
        $_year_id = $_year->id;
        
        $start_date = $a_month->base_month->firstDay()->day_date;
        $end_date = $a_month->base_month->lastDay()->day_date;
        
        $outcome_vat_sum = Account::getFromParam('accounting.codes.vat.outcome_month_sum',$_year)->getLeafAccount();
        $income_vat_sum = Account::getFromParam('accounting.codes.vat.income_month_sum',$_year)->getLeafAccount();   
        
        $_outcome_code = Yii::app()->configManager->get('accounting.codes.vat.outcome',false).'*';
        $_income_code = Yii::app()->configManager->get('accounting.codes.vat.income',false).'*';
        
        $_sum = 0;
        
        $crit1 = new SIMADbCriteria([
            'Model' => 'Account',
            'model_filter' => [
                'year' => ['ids' => $_year_id],
                'code' => $_outcome_code
            ]
        ]);
        $accounts1 = Account::model()
            ->withDCCurrents($_year_id, [
                'start_date' => $start_date, 
                'end_date' => $end_date,
                'exclude_orders' => [$_ao_id]
            ])
            ->onlyLeafAccounts() 
            ->byName()
            ->findAll($crit1);
        
        foreach ($accounts1 as $account)
        {
            $_saldo = -($account->debit_current - $account->credit_current);
            if (abs($_saldo) > 0.001 && !in_array($account->id,[$outcome_vat_sum->id, $income_vat_sum->id]))
            {
                $tr = new AccountTransaction();
                $tr->account_id = $account->id;
                $tr->account_document_id = $document->id;
                $tr->comment = $_statement_comment;
                $tr->debit = $_saldo;
                $booking_transactions[] = $tr;
                
                $_sum += -$account->saldo;
            }
        }
        
        $crit2 = new SIMADbCriteria([
            'Model' => 'Account',
            'model_filter' => [
                'year' => ['ids' => $_year_id],
                'code' => $_income_code
            ]
        ]);
        $accounts2 = Account::model()
            ->withDCCurrents($_year_id, [
                'start_date' => $start_date, 
                'end_date' => $end_date,
                'exclude_orders' => [$_ao_id]
            ])
            ->onlyLeafAccounts()
            ->byName()
            ->findAll($crit2);
        
        foreach ($accounts2 as $account)
        {
            $_saldo = ($account->debit_current - $account->credit_current);
            if (abs($_saldo) > 0.001 && !in_array($account->id,[$outcome_vat_sum->id, $income_vat_sum->id]))
            {
                $tr = new AccountTransaction();
                $tr->account_id = $account->id;
                $tr->account_document_id = $document->id;
                $tr->comment = $_statement_comment;
                $tr->credit = $_saldo;
                $booking_transactions[] = $tr;
                
                $_sum -= $account->saldo;
            }
        }
        
        if ($_sum > 0)
        {
            $_sum_account = $outcome_vat_sum;
            $_sum_side = 'credit';
        }
        else
        {
            $_sum_account = $income_vat_sum;
            $_sum = $_sum * -1.0;
            $_sum_side = 'debit';
        }
        
        $tr = new AccountTransaction();
        $tr->account_id = $_sum_account->id;
        $tr->account_document_id = $document->id;
        $tr->comment = $_statement_comment;
        $tr->$_sum_side = $_sum;
        $booking_transactions[] = $tr;
        
        
        return $booking_transactions;
    }
}