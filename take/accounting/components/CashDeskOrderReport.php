<?php
class CashDeskOrderReport extends SIMAReport
{
    protected function getModuleName()
    {
        return 'accounting';
    }
    
    public function getPDF()
    {
        $cash_desk_order = $this->params['cash_desk_order'];
        $template_id = null;
        switch ($cash_desk_order->type)
        {
            case CashDeskOrder::$PAYOUT:     $template_id = Yii::app()->configManager->get('accounting.templates.cash_desk_order_out', false);
            case CashDeskOrder::$PAYIN:      $template_id = Yii::app()->configManager->get('accounting.templates.cash_desk_order_in', false);
            case CashDeskOrder::$SUBMIT:     $template_id = Yii::app()->configManager->get('accounting.templates.cash_desk_order_out', false);
            case CashDeskOrder::$WITHDRAW:   $template_id = Yii::app()->configManager->get('accounting.templates.cash_desk_order_in', false);
        }
        if(!empty($template_id))
        {
            $templated_file = TemplatedFile::model()->with('templated_file_data')->findByPk($cash_desk_order->file->id);
            if ($templated_file === null)
            {
                $template_file_id = TemplateController::setTemplate($cash_desk_order->file->id, $template_id);
                $templated_file = TemplatedFile::model()->findByPkWithCheck($template_file_id); 
            }  
            return $templated_file->createTempPdf();
        }
        else 
        {
            return parent::getPDF();
        }
    }
    
    protected function getHTMLContent()
    {
        $cash_desk_order = $this->params['cash_desk_order'];
        $i = 1;
        $cash_desk_items_cnt = count($cash_desk_order->cash_desk_items);
        $max_while_cycle_exec_time_seconds = ini_get('max_execution_time');
        $html = "";
        foreach ($cash_desk_order->cash_desk_items as $cash_desk_items)  
        {
            set_time_limit($max_while_cycle_exec_time_seconds);
            if (!empty($this->params['update_func']))
            {
                call_user_func($this->params['update_func'], round(($i/$cash_desk_items_cnt)*100, 0, PHP_ROUND_HALF_DOWN));
            }
            $html .= "<tr>
                <td>{$cash_desk_items->text}</td>
                <td>".SIMAHtml::number_format($cash_desk_order->isIN ? $cash_desk_items->amount_in : $cash_desk_items->amount_out)."</td>
                <td>{$cash_desk_order->number}</td>
            </tr>";
            $i++;
        }
        
        return [
            'html' => $this->render('html', [
                'cash_desk_order' => $cash_desk_order,
                'curr_company' => Yii::app()->company,
                'curr_date' => SIMAHtml::DbToUserDate(SIMAHtml::currentDateTime(false)),
                'html' => $html,
            ], true, false),
            'css_file_name' => 'css'
        ];
    }
}