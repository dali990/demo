<?php

/**
 * Staticka klasa u kojoj se predvidjaju sva proknjizavanja
 */
class BookingPredictionBillOut
{
    /**
     * 
     * @param type $document
     * @return type
     */
    public static function Booking($document)
    {
        $booking_transactions = [];
        
        $bill = $document->file->any_bill;

        $_statement_comment = $bill->bill_number . ' - ' . $bill->partner->DisplayName;
        
        /**
         * Main booking
         */

                $booking_transactions[] = self::BookingProducts($document, $bill, $_statement_comment);


        return SIMAMisc::joinArrays($booking_transactions);
    }
   

    private static function BookingProducts($document, $bill, $_statement_comment)
    {
        $booking_transactions = [];

//        $booking_transactions = self::appendWarehouseTransfers($document->account_order, $bill);

        //setovanje izlazne cene
        foreach ($bill->warehouse_transfers as $_transfer)
        {
            foreach ($_transfer->warehouse_transfer_items as $_item)
            {
                //trazenje cene iz racuna
                $bill_price = null;
                foreach ($bill->bill_items as $b_item)
                {
                    if ($b_item->warehouse_material_id == $_item->warehouse_material_id)
                    {
                        $bill_price = $b_item->value_per_unit;
                    }
                }
                if (is_null($b_item))
                {
                    throw new SIMAWarnException('stavka iz otpremnice ne postoji u racunu - '.
                            $_item->DisplayName);
                }
                $predicted_price = $_transfer->warehouse_from->materialPrice($_item->warehouse_material, $_transfer, true);
//                $avarage_price = $_transfer->warehouse_from->materialAvaragePrice(
//                        $_item->warehouse_material, 
//                        $_transfer->transfer_time,
//                        true,
//                        $_transfer->id);
//                if ($_item->value_per_unit == 0)
//                {
                    $_item->value_per_unit = $predicted_price;
                    $_item->value_per_unit_out = $bill_price;
                    $_item->save();
//                }
//                else
//                {
//                    if ($_item->value_per_unit != $predicted_price)
//                    {
//                        $_iDN = $_item->warehouse_material->DisplayName;
////                        Yii::app()->raiseNote('Stavka '.$_iDN.' nema cenu koja je predvidjena');
//                    }
//                }
            }
        }
        
        return $booking_transactions;
    }
    
    /**
     * 
     * @param AccountOrder $order - Order na koji treba da se dodaju
     * @param type $bill
     * @param type $_material_prices
     * @return array
     */
//    private static function appendWarehouseTransfers(AccountOrder $order, $bill, $_material_prices = null)
//    {
//        $items_to_save = [];
//        foreach ($bill->warehouse_transfers as $transfer)
//        {
//            /**
//             * setovanje vrednosti
//             */
////            foreach ($transfer->warehouse_transfer_items as $item)
////            {
////                $item->value_per_unit = $_material_prices[$item->warehouse_material_id];
////                $items_to_save[] = $item;
////            }
//            /**
//             * pravljenje AccountDocumenta-a
//             */
//            if (!isset($transfer->file->account_document))
//            {
//                $transfer->file->account_document = new AccountDocument();
//                $transfer->file->account_document->id = $transfer->id;
//            }
//            else
//            {
//                
//            }
//            if ($transfer->file->account_document->account_order_id = $order->id;)
//            $transfer->file->account_document->account_order_id = $order->id;
//            $items_to_save[] = $transfer->file->account_document;
//        }
//        return $items_to_save;
//    }
  
    
    private function BookingGoods($document, $bill, $_statement_comment)
    {
        $booking_transactions = [];
        return $booking_transactions;
    }
    
}