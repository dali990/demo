<?php
class POPDVExport extends SIMAComponent implements ArrayAccess
{
    const PDF = 'PDF';
    const XML = 'XML';
    
    const BOOKING_TYPE_BOTH = 'BOTH';
    const BOOKING_TYPE_REGULER = 'REGULAR';
    const BOOKING_TYPE_CANCELED = 'CANCELED';
    
    private $booking_type = null; //jedan od gore 3 ponudjena
    
    /**
     * u zavisnosti od kombinacije ova dva polja se razlicito racunaj ustvari
     * account_document - NULL gleda se ceo mesec
     *      
     * account_document - NOT NULL ima vrednost samo jedan dokument
     *      popdv_manual - TRUE rucno se popunjava
     *      popdv_manual - FALSE gleda se automatika
     */
    private $account_document = null; //NULL ili konkretan dokument
    private $popdv_manual = false; //TRUE || FALSE
    
    private $bill_calc_component = null;
    private $bs_calc_component = null;
    
    
    
    private $month = null;
    
    private $views_path = null;
    
    private $xml_info_params = [];
    private $xml_popdv_params = [];
    private $xml_pppdv_params = [];
    
    private $popdv_segments = ['1','2','3','3a','4','5','6','7','8','9','9a','10','11'];
    
    private $params_display = []; //prikaz za PDF/HTML
    private $params_html = []; //dodat HTML kod po potrebi
    private $params = [];
    private $params_notes = [];
    private $params_manual_edited = [];

    
    private function paramHTMLwithNotes($text, $notes)
    {
        $result = "<span title='".implode(', ', $notes)."'>$text</span>";
        return $result;
    }
    
    /**
     * 
     * @param Month $month
     * @param type $document_id
     * @param string $booking_type
     * @throws SIMAExceptionPOPDVBookingTypeNotExists
     */
    public function __construct(Month $month, AccountDocument $account_document = null, string $booking_type = self::BOOKING_TYPE_BOTH)
    {
        parent::__construct();
        
        //popunjava sve sa 0
        $this->params = array_fill_keys(POPDVLists::PARAMS, 0.0);
        
        if (!in_array($booking_type, [
            self::BOOKING_TYPE_BOTH,
            self::BOOKING_TYPE_REGULER,
            self::BOOKING_TYPE_CANCELED
        ]))
        {
            throw new SIMAExceptionPOPDVBookingTypeNotExists();
        }
        
        $this->booking_type = $booking_type;
        
        $this->account_document = $account_document;    
        if (!is_null($account_document))
        {
            if ($account_document->id === $account_document->account_order->id)
            {
                throw new SIMAExceptionPOPDVNoAccountOrder();
            }
        }
        $this->views_path = Yii::app()->getModule('accounting')->getBasePath() . '/views';
        $this->month = $month;        
        
        $this->popdv_manual = !is_null($this->account_document) && $this->account_document->popdv_manual === true;
        //cisto racunanje i zapisivanje notes
        $this->calcParams();
        
        foreach ($this->params as $code => $value) 
        {
            $this->params_display[$code] = SIMAHtml::number_format($this->params[$code], 0);   
            $this->params_html[$code] = SIMAHtml::number_format($this->params[$code], 0);
            if (isset($this->params_notes[$code]))
            {
                $this->params_display[$code] = '* ' . $this->params_display[$code];   
                $this->params_html[$code] = $this->paramHTMLwithNotes($this->params_display[$code],$this->params_notes[$code]);   
            }
            
        }
        $this->fillXMLInfoParams();
        $this->fillXMLPPPDVParams();
        $this->fillXMLPOPDVParams();
    }
    
    
    private function fillXMLInfoParams()
    {
        $company = Yii::app()->company;
        $first_day = SIMAHtml::UserToDbDate($this->month->firstDay());
        $last_day = SIMAHtml::UserToDbDate($this->month->lastDay());
        $company_municipality_id = Yii::app()->configManager->get('accounting.paychecks.company_municipality_id');
        $company_municipality = Municipality::model()->findByPkWithCheck($company_municipality_id);
        $company_municipality_code = $company_municipality->code;
        
        if (Yii::app()->isWebApplication())
        {
            $responsible_person = Yii::app()->user->model->DisplayName;
        }
        else
        {
            $responsible_person = '-';
        }
        
        if (!isset($company->partner->main_email_address))
        {
            Yii::app()->raiseNote('Nemate postavljenu glavnu elektronsku adresu kompanije');
            $e_mail = '-';
        }
        else
        {
            $e_mail = $company->partner->main_email_address->DisplayName;
        }
        
        if (!isset($company->partner->main_address))
        {
            Yii::app()->raiseNote('Nemate postavljenu glavnu adresu kompanije');
            $address = '-';
            $city = '-';
        }
        else
        {
            $address = $company->partner->main_address->DisplayName;
            $city = $company->partner->main_address->city->DisplayName;
        }
        
        $this->xml_info_params = [
            'PIB' => $company->PIB,
            'company' => $company->DisplayName,
            'Opstina' => $company_municipality_code,
            'address' => $address,
            'date_from' => $first_day,
            'date_to' => $last_day,
            'e_mail' => $e_mail,
            'city' => $city,
            'date_of_application' => (new DateTime)->format('Y-m-d'),
            'timestamp_of_application' => (new DateTime)->format('Y-m-d\TH:i:s'),
            'responsible_person' => $responsible_person,
            'tip_podnosioca' => 1, //obveznik - 2 je duznik
            'tip_perioda' => 1, //mesecni - 3 je tromesecni
        ];
        
    }
    private function fillXMLPPPDVParams()
    {
        $VAT_return = null; 
        if (SIMAMisc::lessThen($this->params['10'], 0.0))
        {
            $VAT_return = $this->month->param('accounting.vat_return');
        }
        $this->xml_pppdv_params = [
            '001' => $this->params['15'],
            '002' => $this->params['25'],
            '003' => $this->params['51'],
            '103' => $this->params['53'],
            '004' => $this->params['54'],
            '104' => $this->params['55'],
            '005' => $this->params['56'],
            '105' => $this->params['57'],
            '006' => $this->params['63'],
            '106' => $this->params['64'],
            '007' => $this->params['711'],
            '107' => $this->params['742'],
            '008' => $this->params['8dj'],
            '108' => $this->params['8e6'],
            '009' => $this->params['9'],
            '109' => $this->params['9a4'],
            '110' => $this->params['10'],
            'vat_return' => $VAT_return,
            
            '201' => 0.0,//$this->params['201'],
            '206' => 0.0,//$this->params['206'],
            '306' => 0.0,//$this->params['306'],
        ];
    }
    
    private function fillXMLPOPDVParams()
    {
        $_segments = new ArrayObject($this->popdv_segments);
        $_seg_iter = $_segments->getIterator();
        $_seg_iter_next = $_segments->getIterator();
        $_seg_iter_next->next();
        foreach ($this->params as $_param_key => $_param)
        {
            $max_count = 2;
            while (
                    (
                        strpos((string)$_param_key, $_seg_iter->current())!==0 
                        ||
                        //mora da se uvede uslov zbog 3a i 9a
                        (
                                strpos((string)$_param_key, $_seg_iter->current()) ===0 
                                &&
                                strpos((string)$_param_key, $_seg_iter_next->current()) ===0 
                        )
                    )
                    && $max_count>0 
                )
            {
                $_seg_iter->next();
                $_seg_iter_next->next();
                $max_count--;
            }
            if ($max_count === 0)
            {
                throw new SIMAWarnAutoBafException('Nisu lepo slozeni parametri za POPDV: '.$_param_key);
            }
            if (!empty($_param) && !in_array($_param_key, POPDVLists::PARAMS_HELPER))
            {
                if (!isset($this->xml_popdv_params[$_seg_iter->current()]))
                {
                    $this->xml_popdv_params[$_seg_iter->current()] = [];
                }
                $this->xml_popdv_params[$_seg_iter->current()][$_param_key] = $_param;
            }
        }
    }
    
    private function addNote($_param_code, $note_text)
    {
        if (!isset($this->params_notes[$_param_code]))
        {
            $this->params_notes[$_param_code] = [$note_text];
        }
        else
        {
            $this->params_notes[$_param_code][] = $note_text;
        }
    }

    private function autoCalcValue(string $param_code)
    {
        return    $this->bill_calc_component[$param_code] 
                + $this->bs_calc_component[$param_code];
    }
    
    /**
     * Racunanje parametara i zapisivanje note-ova
     */
    private function calcParams()
    {
        
        $this->bill_calc_component = new POPDVBillCalc($this->month, $this->account_document, $this->booking_type);
        $this->bs_calc_component = new POPDVBankStatementCalc($this->month, $this->account_document, $this->booking_type);
        
        if (!is_null($this->account_document))//ZA JEDAN DOKUMENT
        {
            if ($this->popdv_manual)
            {
                //popuni sa manual
                foreach ($this->account_document->popdv_items as $_item)
                {
                    $this->params[$_item->param_code] = $_item->param_value;
                }
                
                //proveri sa automatikom
                foreach (POPDVLists::PARAMS as $param_code)
                {
                    if (!SIMAMisc::areEqual($this->params[$param_code], $this->autoCalcValue($param_code)))
                    {
                        $note_text = "Iza automatskoe racunice za racune je ".SIMAHtml::number_format($this->autoCalcValue($param_code));
                        $this->addNote($param_code, $note_text);
                        $this->params_manual_edited[] = strval($param_code);
                    }
                }
            }
            else
            {
                //popuni sa automatikom
                foreach (POPDVLists::PARAMS as $param_code)
                {
                    if (!empty($this->autoCalcValue($param_code)))
                    {
                        $this->params[$param_code] = $this->autoCalcValue($param_code);
                    }
                }
            }
        }
        else //ZA CEO MESEC
        {
            //popuni sa manuelnim
            $popdv_items = POPDVItem::model()->findAll([
                'model_filter' => [
                    'account_document' => [
                        'popdv_manual' => true,
                        'account_order' => [
                            'month' => ['ids' => $this->month->id]
                        ]
                    ]
                ]
            ]);
            //popuni sa manual
            foreach ($popdv_items as $_item)
            {
                $this->params[$_item->param_code] = $_item->param_value;
            }
            
            //popuni sa automatikom iz racuna i izvoda i svega ostalog
            foreach (POPDVLists::PARAMS as $param_code)
            {
                $this->params[$param_code] += $this->autoCalcValue($param_code);
            }
        }
        
        
        
        //provera vrednosti na kraju izracunavanja
        foreach ($this->params as $param_code => $bill_param_value)
        {
            //PROVERI
            if (in_array($param_code, POPDVLists::MUST_BE_POSITIVE) && SIMAMisc::lessThen($this->params[$param_code], 0.0))
            {
                Yii::app()->raiseNote('NIJE POZITIVAN A TREBA DA BUDE - '.$param_code);
//                        throw new Exception();
            }
            elseif (in_array($param_code, POPDVLists::MUST_BE_NEGATIVE) && SIMAMisc::lessThen(0.0, $this->params[$param_code]))
            {
                Yii::app()->raiseNote('NIJE NEGATIVAN A TREBA DA BUDE - '.$param_code);
            }
            elseif (in_array($param_code, POPDVLists::TURN_TO_POSITIVE) && SIMAMisc::lessThen($this->params[$param_code], 0.0))
            {
                $note_text = "vrednost je $bill_param_value, ali je upisana 0 ";
                $this->params[$param_code] = 0.0;
                $this->addNote($param_code, $note_text);
            }
            else
            {
                $this->params[$param_code] = $this->params[$param_code];
            }    
        }
        
        
        foreach (POPDVLists::SUM_PARAMS as $sum_code => $sum_parts_array)
        {
            if ($this->params[$sum_code] !== 0.0)
            {
                error_log(__METHOD__."prosla je suma za $sum_code");
            }
            $new_sum = 0;
            if ($sum_parts_array === POPDVLists::CUSTOM_SUM)
            {
                continue;
            }
            
            foreach ($sum_parts_array as $sum_part)
            {
                if (SIMAMisc::lessThen($this->params[$sum_part], 0.0) && in_array($sum_part, POPDVLists::TURN_TO_POSITIVE))
                {
                    $note_text = "Stavka $sum_part nije sabrana jer je manja od nule";
                    $this->addNote($sum_code, $note_text);
                }
                else
                {
                    $new_sum += $this->params[$sum_part];
                }
            }

            if (in_array($sum_code, POPDVLists::TURN_TO_POSITIVE) && SIMAMisc::lessThen($new_sum, 0.0))
            {
                $note_text = "vrednost je $new_sum, ali je upisana 0 ";
                $this->params[$sum_code] = 0.0;
                $this->addNote($sum_code, $note_text);
            }
            else
            {
                $this->params[$sum_code] = $new_sum;
            }
        }
        
        //SPECIJALAN PRENOS
        $this->params['8e6'] = $this->params['8e5'];
        $this->params['53'] = 0;
        $this->params['57'] = 0;
        $this->params['9a3'] = 0;
        
        /**
         * ako su 5.2 ili 5.5 (pojedinacno) u minusu, oni se NE sabiraju  u 5.6 
         * nego idu kao negacije u 8e6 (odnosno kao pozitivne vrednosti)
         * Posto je i taj iznos moguce da bude u minusu, 
         * ako ovaj dodatak nije dovoljan, onda se negacija 8e6 vraca u 53 ali kao invertovan
         */
        
        /*
         * ako je u minusu ide absolutan u 8e6, ako je u plusu ide u 53(pa preko njega u 57)
         */
        if (SIMAMisc::lessThen($this->params['52'], 0))
        {
            $_value = $this->params['52'] * -1;
            $this->params['8e6'] += $_value;
            $this->params['52'] = 0.0;
            $this->addNote('52', "Apsolutni iznos $_value se sabira u polju 8e6");
            $this->addNote('8e6', "Sabran apsolutni iznos $_value iz polja 52");
        }
        else
        {
            $this->params['53'] += $this->params['52'];
        }
        
        
        
        /*
         * ako je u minusu ide absolutan u 8e6, ako je u plusu ide u 57
         */
        if (SIMAMisc::lessThen($this->params['55'], 0))
        {
            $_value = $this->params['55'] * -1;
            $this->params['55'] = 0.0;
            $this->params['8e6'] += $_value;
            $this->addNote('55', "Apsolutni iznos $_value se sabira u polju 8e6");
            $this->addNote('8e6', "Sabran apsolutni iznos $_value iz polja 55");
        }
        else
        {
            $this->params['57'] += $this->params['55'];
        }
        
        
        if (SIMAMisc::lessThen($this->params['8e6'], 0))
        {
            $_value = $this->params['8e6'] * -1;
            $this->params['53'] += $_value;
            $this->params['8e6'] = 0.0;
            
            $this->addNote('8e6', "Apsolutni iznos $_value se sabira u polju 53");
            $this->addNote('53', "Sabran apsolutni iznos $_value iz polja 8e6");
        }
        
        $this->params['57'] +=  $this->params['53'];
        
        $this->params['9a1'] = $this->params['64'];
        $this->params['9a2'] = $this->params['742'];
        $this->params['9a3'] = $this->params['8e6'];
        $this->params['9a4'] = $this->params['9a1'] + $this->params['9a2'] + $this->params['9a3'];
        
        
        $this->params['10'] = $this->params['57'] - $this->params['9a4'];
    }

    /**
     * 
     * @param type $with_questionmark
     * @param type $with_links
     * @return type
     */
    public function getDisplayHTML($with_questionmark = false, $with_links = true)
    {
        foreach ($this->params_display as $code => $value)
        {
            /**
             * mora da se reimplementira posto sad postoje i custom
             */
//            if ($with_links)
//            {
//                try
//                {
//                    $this->getDBCriteriaParams($code);
//                    if (!is_null($this->params_display[$code]))
//                    {
//                        $_month_id = $this->month->id;
//                        $this->params_html[$code] = "<a href='javascript:parent.sima.accounting.POPDVBillsDisplay("
//                                . "\"$code\",\"$_month_id\""
//                                . ")'>$value</a>";
//                    }
//                }
//                catch (SIMAExceptionPOPDVCodeJustSum $ex)
//                {
//                    //nista spec, samo nece biti linka
//                }
//                catch (SIMAExceptionPOPDVCodeNotImplemented $ex)
//                {
//                    //nista ne treba da se obradi
//                }
//            }
            if ($this->popdv_manual && !isset(POPDVLists::SUM_PARAMS[$code]))
            {
                $popdv_item = POPDVItem::model()->find([
                    'model_filter' => [
                        'account_document' => ['ids' => $this->account_document->id],
                        'param_code' => strval($code)
                    ]
                ]);
                $_id_to_send = is_null($popdv_item)?'':$popdv_item->id;
                $_account_document_id = $this->account_document->id;
                $_code = strval($code);
                $title = '';
                if (isset($this->params_notes[$_code]))
                {
                    $title = implode(', ',$this->params_notes[$_code]);
                }
                $this->params_html[$code] = "<a title='$title' href='javascript:parent.sima.accounting.POPDVEditManual("
                                . "\"$_id_to_send\", \"$_account_document_id\", \"$_code\""
                                . ")'>$value</a>";
            }
            if (!isset($this->params_html[$code]))
            {
                $this->params_html[$code] = $this->params_display[$code];
            }

            if (isset(Yii::app()->params['custom_lotex_popdv_items_show']) && Yii::app()->params['custom_lotex_popdv_items_show']===true)
            {
                try
                {
                    $filter_params = $this->bill_calc_component->getModelFilterParams($code);
                    $_month_id = $this->month->id;
                    $this->params_html[$code] = "<a href='javascript:parent.sima.accounting.POPDVBillsDisplay("
                                    . "\"$code\",\"$_month_id\""
                                    . ")'>$value</a>";
                }
                catch (SIMAExceptionPOPDVCodeJustSum $e)
                {
                    //nista ne treba, samo se nece postaviti params_html
                }
            }
        }
        
        return 
            '<style>' . Yii::app()->controller->renderFile($this->views_path . '/vat_export/pdf/vat_css.css', [], true) . '</style>' . 
            Yii::app()->controller->renderFile(
                $this->views_path . '/vat_export/pdf/vat_html.php',
                [
                    'month' => $this->month,
                    'params' => $this->params_html,
                    'with_questionmark' => false//$with_questionmark
                ],
                true
            );
    }
    
    /**
     * vraca Temporarty fajl koji je PDF
     * @param type $with_questionmark
     * @return \TemporaryFile
     */
    public function exportToPDF($with_questionmark = false)
    {
        $html = Yii::app()->controller->renderFile(
            $this->views_path . '/vat_export/pdf/vat_html.php',
            [
                'month' => $this->month,
                'params' => $this->params_display,
                'with_questionmark' => false//$with_questionmark
            ],
            true
        );

        $temporary_file = new TemporaryFile('', null, 'pdf');
        $temporary_file->createPdfFromHtml(
            ['bodyHtml' => $html],
            ['user-style-sheet' => $this->views_path . '/vat_export/pdf/vat_css.css']
        );
        
        return $temporary_file;
    }

    /**
     * vraca Temporarty fajl koji je XML
     * @param type $with_questionmark
     * @return \TemporaryFile
     */
    public function exportToXML()
    {
        $xml = Yii::app()->controller->renderFile(
            $this->views_path . '/vat_export/xml/vat_xml.php',
            [
                'month' => $this->month,
                'xml_info_params' => $this->xml_info_params,
                'xml_popdv_params' => $this->xml_popdv_params,
                'xml_pppdv_params' => $this->xml_pppdv_params,
            ],
            true
        );

        $temporary_file = new TemporaryFile($xml, null, 'xml');
        
        return $temporary_file;
    }
    
    public static function renderQuestionmark($field)
    {
        if (is_null($field))
        {
            throw new Exception('Please enter valid field ID.');
        }
        
        echo '<div class="question-div"><a href="' . $field . '.php">?</a></div>';
    }
    
    
    public function getListOfFilledParams()
    {
        $filled_params = [];
        $filled_sums = [];
        foreach (POPDVLists::PARAMS as $param_code)
        {
            if ($this->params[$param_code] !== 0.0)
            {
                if (isset(POPDVLists::SUM_PARAMS[$param_code]))
                {
                    $filled_sums[] = strval($param_code);
                }
                else
                {
                    $filled_params[] = strval($param_code);
                }
                
            }
        }
        return [$filled_params, $filled_sums];
    }
    
    public function getListOfManualEditedParams()
    {
        return $this->params_manual_edited;
    }
    
    /**
     * 
     * @param string $param_code - kod koji predstavlja parametar
     * @return numeric
     */
    public function getParamValue($param_code)
    {
        return $this[$param_code];
    }
    
    public function getPPPDVParamValue($param_code)
    {
        return $this->xml_pppdv_params[$param_code];
    }
    
    public function offsetExists ( $offset ) : bool
    {
        return isset($this->params[$offset]);
    }
    public function offsetGet ( $offset )
    {
        return $this->params[$offset];
    }
    public function offsetSet ( $offset , $value ) : void
    {
        if (isset($this->params[$offset]))
        {
            //trenutno ne postavljamo 
//            $this->params[$offset] = $value;
        }
        else
        {
            throw new SIMAException("kod $offset ne postoji u POPDV prijavi");
        }
    }
    public function offsetUnset ( $offset ) : void
    {
        $this->offsetSet($offset, 0);
    }
}