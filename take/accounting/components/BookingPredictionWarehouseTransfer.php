<?php

/**
 * Staticka klasa u kojoj se predvidjaju sva proknjizavanja
 */
class BookingPredictionWarehouseTransfer
{
    public static function Booking($document)
    {
        $booking_transactions = [];
        $file = $document->file;
        $_wt = $document->file->warehouse_transfer;

        if (!$_wt->confirmed)
        {
            throw new SIMAWarnException('Prvo potvrdite magacinski dokument '.$_wt->DisplayName);
        }
        
        $warehouse_from = $_wt->warehouse_from;
        $warehouse_to = $_wt->warehouse_to;
        
        $_statement_comment = $_wt->name;
        
        if (isset($file->warehouse_requisition_location))
        {
            $booking_transactions[] =  BookingPredictionWarehouseRequisition::Booking($document, $file->warehouse_requisition_location, $_statement_comment);
        }
        elseif (isset($file->warehouse_requisition))
        {
            $booking_transactions[] = BookingPredictionWarehouseRequisition::Booking($document, $file->warehouse_requisition, $_statement_comment);
        }
        elseif (isset($file->warehouse_receiving))
        {
            $booking_transactions[] = self::BookingReceiving($document, $file->warehouse_receiving, $_statement_comment);
        }
        elseif (isset($file->warehouse_transmitting))
        {
            $booking_transactions[] = self::BookingTransmitting($document, $file->warehouse_transmitting, $_statement_comment);
        }
        elseif (isset($file->warehouse_dispatch))
        {
            $booking_transactions[] = self::BookingDispatch($document, $file->warehouse_dispatch, $_statement_comment);
        }
        elseif (isset($file->warehouse_internal_dispatch))
        {
            $booking_transactions[] = self::BookingInternalDispatch($document, $file->warehouse_internal_dispatch, $_statement_comment);
        }
        elseif ($warehouse_to->isWarehouseStorageWholesail)
        {
            $booking_transactions[] = self::BookingCalculationToWholesail($document, $file->warehouse_calculation, $_statement_comment);
        }
        
        elseif ($warehouse_to->isWarehouseStorageRetail || $warehouse_from->isWarehouseStorageRetail)
        {
            $booking_transactions[] = BookingPredictionWarehouseRetail::Booking($document);
        }
        else
        {
            Yii::app()->raiseNote('dokument se ne knjizi samostalno, nego samo kao ceo nalog');
        }
        
        return SIMAMisc::joinArrays($booking_transactions);
    }
    
    /**
     * PRIJEMNICA
     * @param type $document
     * @param WarehouseReceiving $rec
     * @param type $_statement_comment
     * @return \AccountTransaction
     */
    private static function BookingReceiving(AccountDocument $document, WarehouseReceiving $rec, $_statement_comment)
    {
        $booking_transactions = []; 
        $year = $document->account_order->year;
        $booking_materials_type = $year->param('accounting.booking_materials_type');
        switch ($booking_materials_type)
        {
            case 'PLANNED_PRICE':
                foreach ($rec->warehouse_transfer_items as $_item)
                {
                    $_item->value_per_unit_out = $rec->warehouse_to->materialPrice($_item->warehouse_material,$rec,true);
                    $_item->save();
                }
                $_diff =  $rec->sum_value_diff;
                $_value = $rec->sum_value_out;
                break;
            case 'AVERAGE_PRICE':
                $_value = $rec->sum_value;
                $_diff = 0;
                break;
        }
        
        /**
         *      | 101
         *      | 1019
         */
        $account_to = $rec->warehouse_to->getMainAccount($year);
        $tr1 = new AccountTransaction();
        $tr1->account_id = $account_to->id;
        $tr1->account_document_id = $document->id;
        $tr1->comment = $_statement_comment;
        $tr1->debit = $_value;
        $booking_transactions[] = $tr1;

        if (abs($_diff)>0)
        {
            $account_correction = $rec->warehouse_to->getCorrectionAccount($year);
            $tr2 = new AccountTransaction();
            $tr2->account_id = $account_correction->id;
            $tr2->account_document_id = $document->id;
            $tr2->comment = $_statement_comment;
            //trazeno je da bude u minusu
            $tr2->debit = -$_diff;
            $booking_transactions[] = $tr2;
        }
        return $booking_transactions;
    }
    
    /**
     * PREDAJNICA
     * @param type $document
     * @param WarehouseTransmitting $trans
     * @param type $_statement_comment
     * @return \AccountTransaction
     * @throws SIMAWarnException
     */
    private static function BookingTransmitting($document, WarehouseTransmitting $trans, $_statement_comment)
    {
        $booking_transactions = [];
        $reqs = [];
        if (isset($trans->production_order))
        {
            if (isset($trans->production_order->warehouse_requisition))
            {
                $reqs[] = $trans->production_order->warehouse_requisition;
            }
        }
        elseif(isset($trans->sales_order))
        {
            if (isset($trans->sales_order->warehouse_requisition))
            {
                $reqs[] = $trans->sales_order->warehouse_requisition;
            }
            if (isset($trans->sales_order->warehouse_requisition2))
            {
                $reqs[] = $trans->sales_order->warehouse_requisition2;
            }
        }
        else
        {
            throw new SIMAWarnException('za predajnicu nije moguce naci trebovanje');
        }
        
        $year = $document->account_order->year;

//        $items = Warehouse
        
        $req_sum = 0.0;
        foreach ($reqs as $_req)
        {
            foreach ($_req->warehouse_transfer_items as $item)
            {
                $req_sum += $item->value_out;   
            }
        }
        
        
        $trans_items = $trans->warehouse_transfer_items;
        $req_sum_fixer = count($trans_items);
        
        $cost_price = Yii::app()->configManager->get('accounting.warehouse.finished_products_evidence_type');
        
        if ($cost_price == 'COST_PRICE')//vodi se po ceni kostanja
        {
            foreach ($trans_items as $item)
            {
                $item->value_per_unit = $req_sum / ($req_sum_fixer * $item->quantity);
                $item->value_per_unit_out = $item->value_per_unit;
                $item->save();
            }
        }
        else //vodi se po predefinisanoj ceni
        {
            foreach ($trans_items as $item)
            {
//                $avarage_price = $trans->warehouse_to->(
//                        $_item->warehouse_material_id, 
//                        $_transfer->transfer_time,
//                        true,
//                        $_transfer->id);
//                $_price = WarehouseMaterialPrice::model()->inWarehouseCondition($trans->warehouse_to_id)->byLevelings()->findByAttributes([
//                    'warehouse_material_id' => $item->warehouse_material_id
//                ]);
                $predicted_price = $trans->warehouse_to->materialPrice($item->warehouse_material, $trans, true);
                if (!isset($predicted_price))
                {
                    $mat = $item->warehouse_material->DisplayName;
                    $mag = $trans->warehouse_to->DisplayHtml;
                    throw new SIMAWarnException("Nije zadata cena za materijal $mat u magacinu $mag");
                }
                else
                {
                    $item->value_per_unit = $req_sum / ($req_sum_fixer * $item->quantity);
                    $item->value_per_unit_out = $predicted_price;
                    $item->save();
                }

            }
        }
        $total_value = $trans->sum_value_out;
        $correction = $trans->sum_value_diff;

        /**
         * 9600 | 9500
         *      | 9690--ovi u SET vode bez ovog konta, pa je tako hardkodovano
         */
        $warehouse_finished = $trans->warehouse_to; 
        $cost_location = $trans->warehouse_from->cost_location; 
        //9600
        $_finished_products    = Account::getFromParam('accounting.codes.nine.finished_products',$year)->getLeafAccount($warehouse_finished);
//        $job_order_account = $job_order_account_base->getLeafAccount($job_order);
        $tr1 = new AccountTransaction();
        $tr1->account_id = $_finished_products->id;
        $tr1->account_document_id = $document->id;
        $tr1->comment = $_statement_comment;
        $tr1->debit = $total_value;//$trans
        $booking_transactions[] = $tr1;
        
        //9500
        $book_cost_location = null;
        $_book_nine_by_cost_location = Yii::app()->configManager->get('accounting.warehouse.book_nine_by_cost_location');
        if ($_book_nine_by_cost_location)
        {
            $book_cost_location = $cost_location;
        }
        $_cost_carier    = Account::getFromParam('accounting.codes.nine.cost_cariers',$year)->getLeafAccount($book_cost_location);
//        $job_order_account = $job_order_account_base->getLeafAccount($job_order);
        $tr2 = new AccountTransaction();
        $tr2->account_id = $_cost_carier->id;
        $tr2->account_document_id = $document->id;
        $tr2->comment = $_statement_comment;
//        $tr->credit = $req_sum;
        $tr2->credit = $total_value;// u SET vode bez 9690 - pise gore komentar
        $booking_transactions[] = $tr2;
        
        // u SET vode bez 9690 - pise gore komentar
//        if (abs($correction)>0)
//        {
//            //9690
//            $_finished_products_correction    = Account::getFromParam('accounting.codes.nine.finished_products_correction',$year) ???;
//    //        $job_order_account = $job_order_account_base->getLeafAccount($job_order);
//            $tr = new AccountTransaction();
//            $tr->account_id = $_finished_products_correction->id;
//            $tr->account_document_id = $document->id;
//            $tr->comment = $_statement_comment;
//            $tr->credit = $correction;
//            $booking_transactions[] = $tr;
//        }
        return $booking_transactions;
    }
    
    private static function BookingDispatch($document, WarehouseDispatch $disp, $_statement_comment)
    {   
        $year = $document->account_order->year;
        $warehouse_from = $disp->warehouse_from;
        $cost_location = $disp->warehouse_from; 
        
        $_value = $disp->sum_value;
        $_value_out = $disp->sum_value_out;
        $_value_diff = $_value - $_value_out;
        
        if ($warehouse_from->isWarehouseStorageProducts)
        {
            /**
             * 980  | 960
             * 969  | 980
             * 903  | 980
             */
            
            $book_cost_location = null;
            $_book_nine_by_cost_location = Yii::app()->configManager->get('accounting.warehouse.book_nine_by_cost_location');
            if ($_book_nine_by_cost_location)
            {
                $book_cost_location = $cost_location;
            }
            
            //980
            $_finished_products_expensess    = Account::getFromParam('accounting.codes.nine.finished_product_expensess',$year)
                                                    ->getLeafAccount($book_cost_location);
            $tr1 = new AccountTransaction();
            $tr1->account_id = $_finished_products_expensess->id;
            $tr1->account_document_id = $document->id;
            $tr1->comment = $_statement_comment;
            $tr1->debit = $_value;
            $booking_transactions[] = $tr1;

            //960
            $_finished_products    = Account::getFromParam('accounting.codes.nine.finished_products',$year)
                                        ->getLeafAccount($book_cost_location);
            $tr2 = new AccountTransaction();
            $tr2->account_id = $_finished_products->id;
            $tr2->account_document_id = $document->id;
            $tr2->comment = $_statement_comment;
            $tr2->credit = $_value;
            $booking_transactions[] = $tr2;

            if (abs($_value_diff) > 0)
            {
                //969
                $_finished_products_correction    = Account::getFromParam('accounting.codes.nine.finished_products_correction',$year)
                                                        ->getLeafAccount($book_cost_location);
                $tr3 = new AccountTransaction();
                $tr3->account_id = $_finished_products_correction->id;
                $tr3->account_document_id = $document->id;
                $tr3->comment = $_statement_comment;
                $tr3->debit = $_value_diff;
                $booking_transactions[] = $tr3;

                //980
                $tr4 = new AccountTransaction();
                $tr4->account_id = $_finished_products_expensess->id;
                $tr4->account_document_id = $document->id;
                $tr4->comment = $_statement_comment;
                $tr4->credit = $_value_diff;
                $booking_transactions[] = $tr4;
            }
            
            
            
            //903
             $_excange_income    = Account::getFromParam('accounting.codes.nine.excange_income',$year)
                                        ->getLeafAccount($book_cost_location);
            $tr5 = new AccountTransaction();
            $tr5->account_id = $_excange_income->id;
            $tr5->account_document_id = $document->id;
            $tr5->comment = $_statement_comment;
            $tr5->debit = $_value_out;
            $booking_transactions[] = $tr5;

            //980
            $tr6 = new AccountTransaction();
            $tr6->account_id = $_finished_products_expensess->id;
            $tr6->account_document_id = $document->id;
            $tr6->comment = $_statement_comment;
            $tr6->credit = $_value_out;
            $booking_transactions[] = $tr6;
        }
        else
        {
            /**
             *      | 132  - potrazuje se iz magacina
             * 1329 |      - duguje se 
             * 501  |
             */
            //960
            
            $warehouse = $disp->warehouse_from;
            $account_from = $warehouse->getMainAccount($document->account_order->year);

            //postavljanja izlaznih cena u kalkulaciji
            foreach ($disp->warehouse_transfer_items as $item)
            {
                $item->value_per_unit = $warehouse->materialAvaragePrice($item->warehouse_material, $disp->transfer_time, true, $disp->id);
                $item->value_per_unit_out = $warehouse->materialPrice($item->warehouse_material, $disp, true);
                $item->save();
            }
            
            $_value_out = $disp->sum_value_out;
            $_value = $disp->sum_value;
            $_value_diff = $_value_out - $_value;
            $tr1 = new AccountTransaction();
            $tr1->account_id = $account_from->id;
            $tr1->account_document_id = $document->id;
            $tr1->comment = $_statement_comment;
            $tr1->credit = $_value_out;
            $booking_transactions[] = $tr1;

            
            if (abs($_value_diff)>0)
            {
                $account_correction = $warehouse->getCorrectionAccount($document->account_order->year);
                $tr2 = new AccountTransaction();
                $tr2->account_id = $account_correction->id;
                $tr2->account_document_id = $document->id;
                $tr2->comment = $_statement_comment;
                //trazeno je da bude u minusu
                $tr2->debit = $_value_diff;
                $booking_transactions[] = $tr2;
                
                
            }
            
            $material_costs = Account::getFromParam('accounting.codes.warehouse.goods_initial_value',$year)->getLeafAccount($warehouse);
//            $material_costs
            $tr2 = new AccountTransaction();
            $tr2->account_id = $material_costs->id;
            $tr2->account_document_id = $document->id;
            $tr2->comment = $_statement_comment;
            //trazeno je da bude u minusu
            $tr2->debit = $_value;
            $booking_transactions[] = $tr2;
        }
        


        return $booking_transactions;
    }
    
    
    private static function BookingInternalDispatch($document, WarehouseInternalDispatch $disp, $_statement_comment)
    {   
        $year = $document->account_order->year;        

        /**
         *      | 132  - potrazuje se iz magacina
         * 1329 |      - duguje se 
         * 132  |
         *      | 1329
         */

        $warehouse_from = $disp->warehouse_from;
        $account_from = $warehouse_from->getMainAccount($document->account_order->year);
        $account_from_correction = $warehouse_from->getCorrectionAccount($document->account_order->year);
        
        $warehouse_to = $disp->warehouse_to;
        $account_to = $warehouse_to->getMainAccount($document->account_order->year);
        $account_to_correction = $warehouse_to->getCorrectionAccount($document->account_order->year);

        //postavljanja izlaznih cena u kalkulaciji
        foreach ($disp->warehouse_transfer_items as $item)
        {
            $item->value_per_unit = $warehouse_from->materialAvaragePrice($item->warehouse_material, $disp->transfer_time, true, $disp->id);
            $item->value_per_unit_out = $warehouse_from->materialPrice($item->warehouse_material, $disp, true);
            $item->save();
        }

        $_value_out = $disp->sum_value_out;
        $_value = $disp->sum_value;
        $_value_diff = $_value_out - $_value;
        
        
        $tr1 = new AccountTransaction();
        $tr1->account_id = $account_from->id;
        $tr1->account_document_id = $document->id;
        $tr1->comment = $_statement_comment;
        $tr1->credit = $_value_out;
        $booking_transactions[] = $tr1;


        if (abs($_value_diff)>0)
        {
            $tr2 = new AccountTransaction();
            $tr2->account_id = $account_from_correction->id;
            $tr2->account_document_id = $document->id;
            $tr2->comment = $_statement_comment;
            //trazeno je da bude u minusu
            $tr2->debit = $_value_diff;
            $booking_transactions[] = $tr2;
        }
        
        $tr3 = new AccountTransaction();
        $tr3->account_id = $account_to->id;
        $tr3->account_document_id = $document->id;
        $tr3->comment = $_statement_comment;
        $tr3->debit = $_value_out;
        $booking_transactions[] = $tr3;


        if (abs($_value_diff)>0)
        {
            $tr4 = new AccountTransaction();
            $tr4->account_id = $account_to_correction->id;
            $tr4->account_document_id = $document->id;
            $tr4->comment = $_statement_comment;
            //trazeno je da bude u minusu
            $tr4->credit = $_value_diff;
            $booking_transactions[] = $tr4;
        }

        
        return $booking_transactions;
    }
    
    private static function BookingCalculationToWholesail(AccountDocument $document, WarehouseCalculation $calc, $_statement_comment)
    {
        $booking_transactions = [];
        //pravljenje stava za svaki magacin

        $warehouse = $calc->warehouse_to;
        $account_to = $warehouse->getMainAccount($document->account_order->year);
        
        //postavljanja izlaznih cena u kalkulaciji
        foreach ($calc->warehouse_transfer_items as $item)
        {
            $item->value_per_unit_out = $warehouse->materialPrice($item->warehouse_material, $calc, true);
            $item->save();
        }

        $tr1 = new AccountTransaction();
        $tr1->account_id = $account_to->id;
        $tr1->account_document_id = $document->id;
        $tr1->comment = $_statement_comment;
        $tr1->debit = $calc->sum_value_out;
        $booking_transactions[] = $tr1;

        $_value_diff = $calc->sum_value_diff;
        if (abs($_value_diff)>0)
        {
            $account_correction = $warehouse->getCorrectionAccount($document->account_order->year);
            $tr2 = new AccountTransaction();
            $tr2->account_id = $account_correction->id;
            $tr2->account_document_id = $document->id;
            $tr2->comment = $_statement_comment;
            $tr2->credit = $_value_diff;
            $booking_transactions[] = $tr2;
        }
        
        return $booking_transactions;
    }
    
}