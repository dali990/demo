<?php
class IosReport extends SIMAReport
{
    protected function getModuleName()
    {
        return 'accounting';
    }
    
    public function getPDF()
    {
        $ios = $this->params['ios'];
        $template_id = Yii::app()->configManager->get('accounting.codes.ios.ios_template', false);
        if(!empty($template_id))
        {
            $templated_file = TemplatedFile::model()->with('templated_file_data')->findByPk($ios->id);
            if ($templated_file === null)
            {
                $template_file_id = TemplateController::setTemplate($ios->id, $template_id);
                $templated_file = TemplatedFile::model()->findByPkWithCheck($template_file_id); 
            }                    
            return $templated_file->createTempPdf();
        }
        else 
        {
            return parent::getPDF();
        }
    }
    
    protected function getHTMLContent()
    {
        $ios = $this->params['ios'];
        $i = 1;
        $ios_items_opened_cnt = count($ios->ios_items_opened);
        $max_while_cycle_exec_time_seconds = ini_get('max_execution_time');
        $html = "";
        foreach ($ios->ios_items_opened as $ios_item)
        {
            set_time_limit($max_while_cycle_exec_time_seconds);
            if (!empty($this->params['update_func']))
            {
                call_user_func($this->params['update_func'], round(($i/$ios_items_opened_cnt)*100, 0, PHP_ROUND_HALF_DOWN));
            }  
            $html .= "<tr class='ios_items'>
                <td class='helper_text'>{$i}</td>
                <td class='helper_text'>{$ios_item->bill_number_display}</td>
                <td class='helper_text'>".SIMAHtml::DbToUserDate($ios_item->income_date_display)."</td>
                <td class='helper_text'>".SIMAHtml::DbToUserDate($ios_item->payment_date_display)."</td> 
                <td class='helper_text'>".SIMAHtml::number_format($ios_item->debit)."</td>
                <td class='helper_text'>".SIMAHtml::number_format($ios_item->credit)."</td>
                <td class='helper_text'>".SIMAHtml::number_format($ios_item->saldo)."</td>
            </tr>";
            $i++;
        }
        
        $curr_company = Yii::app()->company;
        if (!is_null($curr_company->partner->MainAddress))
        {
            $company_city = $curr_company->partner->MainAddress->city->name;
        }
        else 
        {
            $company_city = 'Beograd';
        }
        
        return [
            'html' => $this->render('html', [
                'ios' => $ios,
                'html' => $html,
                'curr_company' => $curr_company,
                'company_city' => $company_city
            ], true, false),
            'css_file_name' => 'css'
        ];
    }
}