<?php

class BillReport extends SIMAReport
{
    protected $css_file_name = 'css.css';
    protected $margin_bottom = 25;

    protected function getModuleName()
    {
        return 'accounting';
    }
    
    protected function getHTMLContent()
    {
        $bill_id = $this->params['bill_id'];
        $bill = Bill::model()->findByPkWithCheck($bill_id);
        
        $curr_company = Yii::app()->company;
        $curr_date = SIMAHtml::DbToUserDate(SIMAHtml::currentDateTime(false));
        $curr_company_main_phone_number = $curr_company->partner->MainPhoneNumber;
        $curr_company_main_email_address = $curr_company->partner->MainEmailAddress;
        
        $this->download_name = $bill->file->display_name . '.pdf';
        
        $bill_title = '';
        if (!empty($bill->file->document_type))
        {
            $bill_title = $bill->file->document_type->DisplayName;
        }

        $bill_items = BillItem::model()->findAll([
            'model_filter' => [
                'bill' => [
                    'ids' => $bill_id
                ],
                'display_scopes' => 'byName'
            ]
        ]);
        
        $responsible_person = null;
        $has_bill_responsible_person = Yii::app()->configManager->get('accounting.templates.bill_templates.has_bill_responsible_person', false);
        if ($has_bill_responsible_person === true)
        {
            $bill_responsible_person_id = Yii::app()->configManager->get('accounting.templates.bill_templates.bill_responsible_person', false);
            if (!empty($bill_responsible_person_id))
            {
                $responsible_person = Person::model()->findByPkWithCheck($bill_responsible_person_id);
            }
            if (empty($responsible_person) && Yii::app()->isWebApplication())
            {
                $responsible_person = Yii::app()->user->model;
            }
        }
        
        $bill_additional_info = Yii::app()->configManager->get('accounting.templates.bill_templates.bill_additional_info', false);
        
        $call_to_number = $bill->bill_number;
        if (!empty($bill->call_for_number))
        {
            $call_to_number = $bill->call_for_number;
        }

        $bill_bank_accounts_ids = Yii::app()->configManager->get('accounting.templates.bill_templates.bill_bank_accounts', false);
        $bill_bank_accounts = null;
        if (!empty($bill_bank_accounts_ids))
        {
            $bill_bank_accounts = BankAccount::model()->findAll([
                'model_filter' => [
                    'ids' => $bill_bank_accounts_ids
                ]
            ]);
        }
        
        return $this->render('html', [
            'bill' => $bill,
            'bill_title' => $bill_title,
            'bill_items' => $bill_items,
            'curr_date' => $curr_date,
            'curr_company' => $curr_company,
            'company_logo_path' => $curr_company->getLogoImagePath(),
            'curr_company_main_phone_number' => !empty($curr_company_main_phone_number) ? $curr_company_main_phone_number->display_name : '',
            'curr_company_main_email_address' => !empty($curr_company_main_email_address) ? $curr_company_main_email_address->DisplayName : '',
            'responsible_person' => $responsible_person,
            'bill_additional_info' => $bill_additional_info,
            'call_to_number' => $call_to_number,
            'bill_bank_accounts' => $bill_bank_accounts,
            'show_phone_in_header' => Yii::app()->configManager->get('accounting.templates.bill_templates.bill_show_phone_in_header', false)
        ], true, false);
    }
    
    protected function getHTMLFooter()
    {
        return $this->render('footer', [
            'bill_footer_text' => Yii::app()->configManager->get('accounting.bill_footer_text', false)
        ], true, false);
    }
}

