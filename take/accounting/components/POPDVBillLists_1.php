<?php

class POPDVBillLists 
{
    public static $I_0_1 = 'I_0_1';
    public static $I_0_2 = 'I_0_2';
    public static $I_0_3 = 'I_0_3';
    public static $I_0_4 = 'I_0_4';
    public static $I_0_5 = 'I_0_5';
    public static $I_0_5b= 'I_0_5b';
    public static $I_0_6 = 'I_0_6';
    public static $I_0_7 = 'I_0_7';
    public static $I_0_8 = 'I_0_8';
    public static $I_1_1 = 'I_1_1';
    public static $I_1_2 = 'I_1_2';
    public static $I_1_3 = 'I_1_3';
    public static $I_1_4 = 'I_1_4';
    public static $U_0_1 = 'U_0_1';
    public static $U_0_2 = 'U_0_2';
    public static $U_0_3 = 'U_0_3';
    public static $U_0_4 = 'U_0_4';
    public static $U_0_5 = 'U_0_5';
    public static $U_1_1 = 'U_1_1';
    public static $U_1_2 = 'U_1_2';
    public static $U_1_3 = 'U_1_3';
    public static $U_1_4 = 'U_1_4';
    public static $U_1_5 = 'U_1_5';
    public static $U_2_Ca= 'U_2_Ca';
    public static $U_2_Cb= 'U_2_Cb';
    public static $U_2_IGNORE = 'U_2_IGNORE';
    
    public static $names = []; //inicijalizovano prilikom pokretanja
    
    public static $LIST_I_0 = [
        'I_0_2',
        'I_0_3',
        'I_0_4',
        'I_0_1',
        'I_0_6',
        'I_0_7',
        'I_0_8',
        'I_0_5',
        'I_0_5b',
    ];
    
    public static $LIST_I_1 = [
        'I_1_2',
        'I_1_1',
        'I_1_4',
        'I_1_3',
    ];
    
    public static $LIST_U_0 = [
        'U_0_3',
        'U_0_4',
        'U_0_2',
        'U_0_1',
        'U_0_5',
        'U_2_Ca'
    ];
    
    public static $LIST_U_1 = [
        'U_1_2',
        'U_1_1',
        'U_1_4',
        'U_1_3',
        'U_1_5',
        'U_2_Cb'
    ];
    
    //Lista koja se nalazi u svim procentima
    public static $LIST_U_2 = [
        'U_2_IGNORE'
    ];
    
    public static $LIST_ALL = [];
    
    public static $noVatValueList = [
        'I_1_3',
        'I_1_4',
        //naredna tri idu, oni imaju interni PDV
//        'U_1_3',
//        'U_1_4',
//        'U_1_5',
        'U_2_Ca'
    ];
    
    public static $internalVATList = [
        //na naredna dva ce se prebaciti na interni, ali se nigde nece videti PDV, nego ce samo biti obracunat
        'I_1_3', 
        'I_1_4',
        //bice ubacen pravi interni PDV
        'U_1_3',
        'U_1_4',
        'U_1_5',
        'U_2_Ca'
    ];
    
    public static $autoVatRefusalTRUEList = [
        'I_0_1',
        'I_0_2',
        'I_0_3',
        'I_0_4',
    ];
    
    public static $autoVatRefusalFALSEList = [
        'I_0_5',
        'I_0_5b',
        'I_0_6',
        'I_0_7',
        'I_0_8'
    ];
    
    //biras da li ces odbiti PDV ili ne - uglavnom ulazni racun 10/20% PDV
    public static $chooseVatRefusalList = [
        'U_1_1',
        'U_1_2',
        'U_1_3',
        'U_1_4',
        'U_1_5'
    ];
    
    public static function init()
    {
        self::$LIST_ALL = array_merge(
            self::$LIST_I_0,
            self::$LIST_I_1,
            self::$LIST_U_0,
            self::$LIST_U_1,
            self::$LIST_U_2
        );
        
        self::$noVatValueList = array_merge(
            self::$LIST_I_0,
            self::$noVatValueList,
            self::$LIST_U_0 
        );
        
        self::$names = [
            'I_0_1' => Yii::t('AccountingModule.POPDVBillLists','I_0_1'),
            'I_0_2' => Yii::t('AccountingModule.POPDVBillLists','I_0_2'),
            'I_0_3' => Yii::t('AccountingModule.POPDVBillLists','I_0_3'),
            'I_0_4' => Yii::t('AccountingModule.POPDVBillLists','I_0_4'),
            'I_0_5' => Yii::t('AccountingModule.POPDVBillLists','I_0_5'),
            'I_0_5b'=> Yii::t('AccountingModule.POPDVBillLists','I_0_5b'),
            'I_0_6' => Yii::t('AccountingModule.POPDVBillLists','I_0_6'),
            'I_0_7' => Yii::t('AccountingModule.POPDVBillLists','I_0_7'),
            'I_0_8' => Yii::t('AccountingModule.POPDVBillLists','I_0_8'),
            'I_1_1' => Yii::t('AccountingModule.POPDVBillLists','I_1_1'),
            'I_1_2' => Yii::t('AccountingModule.POPDVBillLists','I_1_2'),
            'I_1_3' => Yii::t('AccountingModule.POPDVBillLists','I_1_3'),
            'I_1_4' => Yii::t('AccountingModule.POPDVBillLists','I_1_4'),
            'U_0_1' => Yii::t('AccountingModule.POPDVBillLists','U_0_1'),
            'U_0_2' => Yii::t('AccountingModule.POPDVBillLists','U_0_2'),
            'U_0_3' => Yii::t('AccountingModule.POPDVBillLists','U_0_3'),
            'U_0_4' => Yii::t('AccountingModule.POPDVBillLists','U_0_4'),
            'U_0_5' => Yii::t('AccountingModule.POPDVBillLists','U_0_5'),
            'U_1_1' => Yii::t('AccountingModule.POPDVBillLists','U_1_1'),
            'U_1_2' => Yii::t('AccountingModule.POPDVBillLists','U_1_2'),
            'U_1_3' => Yii::t('AccountingModule.POPDVBillLists','U_1_3'),
            'U_1_4' => Yii::t('AccountingModule.POPDVBillLists','U_1_4'),
            'U_1_5' => Yii::t('AccountingModule.POPDVBillLists','U_1_5'),
            'U_2_Ca'=> Yii::t('AccountingModule.POPDVBillLists','U_2_Ca'),
            'U_2_Cb'=> Yii::t('AccountingModule.POPDVBillLists','U_2_Cb'),
            'U_2_IGNORE' => Yii::t('AccountingModule.POPDVBillLists','U_2_IGNORE')
        ];
    }
    
    public static function getFormLists($is_invoice)
    {
        if ($is_invoice)
        {
            return [
                '0'  => self::setTitles(self::$LIST_I_0),
                '10' => self::setTitles(self::$LIST_I_1),
                '20' => self::setTitles(self::$LIST_I_1),
            ];
        }
        else
        {
            return [
                '0'  => self::setTitles(array_merge(self::$LIST_U_0,self::$LIST_U_2)),
                '10' => self::setTitles(array_merge(self::$LIST_U_1,self::$LIST_U_2)),
                '20' => self::setTitles(array_merge(self::$LIST_U_1,self::$LIST_U_2)),
            ];
        }
    }
    
    public static function setTitles($_list)
    {
        $_ret_list = [];
        foreach ($_list as $code) 
        {
            $_ret_list[] = [
                'code' => $code,
                'title' => self::$names[$code]
            ];
        }
        return $_ret_list;
    }
    
        
}
