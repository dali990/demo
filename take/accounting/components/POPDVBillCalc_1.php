<?php
class POPDVBillCalc extends SIMAComponent implements ArrayAccess
{
   
    private $booking_type = null;
    private $month = null;
    private $params = [];
    private $account_document = null;
    
    private function fillNulls()
    {
        $this->params['16'] = null;
        $this->params['26'] = null;
        
        $this->params['371'] = null;
        $this->params['372'] = null;
        $this->params['373'] = null;
        $this->params['374'] = null;
        
        $this->params['3a61'] = null;
        $this->params['3a62'] = null;
        
        $this->params['4111'] = null;
        $this->params['4121'] = null;
        $this->params['4131'] = null;
        $this->params['4142'] = null;
        $this->params['4211'] = null;
        $this->params['4212'] = null;
        $this->params['4221'] = null;
        $this->params['4222'] = null;
        $this->params['4231'] = null;
        $this->params['4232'] = null;
        $this->params['4243'] = null;
        $this->params['4244'] = null;
        
        $this->params['711'] = null;
        $this->params['721'] = null;
        $this->params['732'] = null;
        $this->params['742'] = null;
        
        
        $this->params['8a31'] = null;
        $this->params['8a32'] = null;
        $this->params['8a33'] = null;
        $this->params['8a34'] = null;
        
        
        
        $this->params['8b31'] = null;
        $this->params['8b32'] = null;
        
        
        $this->params['8v3'] = null;
        $this->params['8g21'] = null;
        $this->params['8g22'] = null;
        $this->params['9'] = null;
        $this->params['111'] = null;
        $this->params['112'] = null;
        $this->params['113'] = null;
    }
    
    public function __construct(Month $month, AccountDocument $account_document = null,string $booking_type = POPDVExport::BOOKING_TYPE_BOTH)
    {
        parent::__construct();
        
        //popunjava sve sa 0
        $this->params = array_fill_keys(POPDVLists::PARAMS, 0.0);
        //popunjava sa null koji nisu implementirani
        $this->fillNulls();
        
        if (!in_array($booking_type, [
                POPDVExport::BOOKING_TYPE_BOTH,
                POPDVExport::BOOKING_TYPE_REGULER,
                POPDVExport::BOOKING_TYPE_CANCELED
        ]))
        {
            throw new SIMAException('NISTE NAVELI DOBAR TIP KNJZENJA ZA POPDV');
        }
        
        $this->booking_type = $booking_type;
        
        $this->account_document = $account_document;        
        $this->month = $month;        

        $uniq = SIMAHtml::uniqid();
        $this->alias1 = 'alias1'.$uniq;
        $this->alias2 = 'alias2'.$uniq;
        $this->alias3 = 'alias3'.$uniq;
        
        $this->calcParams();
    }
    
    
    private $bill_ids;
    private $adv_bill_ids;
    private $relief_bill_ids;
    private $unrelief_bill_ids;
    
    private $canceled_bill_ids;
    private $canceled_adv_bill_ids;
    private $canceled_relief_bill_ids;
    private $canceled_unrelief_bill_ids;
    
    
    private function calcIDSesConditionParams($bill_type, $canceled = false)
    {
        $month_id = $this->month->id;
        $alias = 't';
        $manual_additional = '';
        $popdv_manual_field = $canceled?'popdv_canceled_manual':'popdv_manual';
        
        if (is_null($this->account_document)) // CEO MESEC
        {
            //AKO JE CEO MESEC ISKLJUCUJU SE MANUELNI
            $_ad_table = AccountDocument::tableName();
            $manual_additional = "AND exists(select 1 from $_ad_table where id = $alias.id and NOT $popdv_manual_field) ";
            
        }
        else
        {
            //AKO JE SAMO JEDAN DOKUMENT, RACUNA SE UVEK I TO SAMO ON
            $account_document_id = $this->account_document->id;
            $manual_additional = "AND $alias.id = $account_document_id";
        }
        
        return [
            //stavljam AnyBill, jer je ok da bude storniran, postoji uslov da je proknjizen u mesecu
            //ako je neki storniran, a nije proknjizen, nece proci tu dole funkciju
            'Model' => 'AnyBill',
//            'scopes' => [
//                'inMonth' => [$this->month->month,$this->month->year->year]
//            ],
            'model_filter' => [
                'bill_type' => $bill_type,
            ],
            'condition' => 
                (
                    $canceled?
                    "(select canceled_month_id = $month_id from accounting.bill_vat_month_id where id = $alias.id)":
                    "(select month_id = $month_id from accounting.bill_vat_month_id where id = $alias.id) "   
                )
                . $manual_additional
            ,
            'alias' => $alias
        ];
    }
    
    private function calcIDSes()
    {
        if (in_array($this->booking_type, [
                POPDVExport::BOOKING_TYPE_BOTH,
                POPDVExport::BOOKING_TYPE_REGULER
        ]))
        {
            $_bills = new SIMADbCriteria($this->calcIDSesConditionParams(Bill::$BILL),true);//save_ids
            $this->bill_ids = empty($_bills->ids)?[]:explode(',', $_bills->ids);

            $_adv_bills = new SIMADbCriteria($this->calcIDSesConditionParams(Bill::$ADVANCE_BILL),true);//save_ids
            $this->adv_bill_ids = empty($_adv_bills->ids)?[]:explode(',', $_adv_bills->ids);

            $_relief_bills = new SIMADbCriteria($this->calcIDSesConditionParams(Bill::$RELIEF_BILL),true);//save_ids
            $this->relief_bill_ids = empty($_relief_bills->ids)?[]:explode(',', $_relief_bills->ids);

            $_unrelief_bills = new SIMADbCriteria($this->calcIDSesConditionParams(Bill::$UNRELIEF_BILL),true);//save_ids
            $this->unrelief_bill_ids = empty($_unrelief_bills->ids)?[]:explode(',', $_unrelief_bills->ids);
        }
        else
        {
            $this->bill_ids = [];
            $this->adv_bill_ids = [];
            $this->relief_bill_ids = [];
            $this->unrelief_bill_ids = [];
        }
        
        if (in_array($this->booking_type, [
                POPDVExport::BOOKING_TYPE_BOTH,
                POPDVExport::BOOKING_TYPE_CANCELED
        ]))
        {
            $_canceled_bills = new SIMADbCriteria($this->calcIDSesConditionParams(Bill::$BILL, true),true);//save_ids
            $this->canceled_bill_ids = empty($_canceled_bills->ids)?[]:explode(',', $_canceled_bills->ids);

            $_canceled_adv_bills = new SIMADbCriteria($this->calcIDSesConditionParams(Bill::$ADVANCE_BILL, true),true);//save_ids
            $this->canceled_adv_bill_ids = empty($_canceled_adv_bills->ids)?[]:explode(',', $_canceled_adv_bills->ids);

            $_canceled_relief_bills = new SIMADbCriteria($this->calcIDSesConditionParams(Bill::$RELIEF_BILL, true),true);//save_ids
            $this->canceled_relief_bill_ids = empty($_canceled_relief_bills->ids)?[]:explode(',', $_canceled_relief_bills->ids);

            $_canceled_unrelief_bills = new SIMADbCriteria($this->calcIDSesConditionParams(Bill::$UNRELIEF_BILL, true),true);//save_ids
            $this->canceled_unrelief_bill_ids = empty($_canceled_unrelief_bills->ids)?[]:explode(',', $_canceled_unrelief_bills->ids);
        }
        else
        {
            $this->canceled_bill_ids = [];
            $this->canceled_adv_bill_ids = [];
            $this->canceled_relief_bill_ids = [];
            $this->canceled_unrelief_bill_ids = [];
        }
        
    }
    
    private static $ID_COMBINATION_R            = 'R';
    private static $ID_COMBINATION_A            = 'A';
    private static $ID_COMBINATION_KO           = 'KO';
    private static $ID_COMBINATION_KZ           = 'KZ';
    private static $ID_COMBINATION_R_A_KO_KZ    = 'R_A_KO_KZ';
    private static $ID_COMBINATION_R_KO_KZ      = 'R_KO_KZ';
    
    private static $ID_COMBINATION_CR           = 'CR';
    private static $ID_COMBINATION_CA           = 'CA';
    private static $ID_COMBINATION_CRA          = 'CRA';
//    private static $ID_COMBINATION_KO           = 'KO';
//    private static $ID_COMBINATION_KZ           = 'KZ';
//    private static $ID_COMBINATION_R_A_KO_KZ    = 'R_A_KO_KZ';
//    private static $ID_COMBINATION_R_KO_KZ      = 'R_KO_KZ';
    
    private static $ID_NONE                     = 'NONE';
    
    private function getIDSes($types)
    {
        $result = [];
        switch ($types) 
        {
            case self::$ID_COMBINATION_R:
                $result = empty($this->bill_ids)?'-1':$this->bill_ids;
                break;
            case self::$ID_COMBINATION_A:
                $result = empty($this->adv_bill_ids)?'-1':$this->adv_bill_ids;
                break;
            case self::$ID_COMBINATION_KO:
                $result = empty($this->relief_bill_ids)?'-1':$this->relief_bill_ids;
                break;
            case self::$ID_COMBINATION_KZ:
                $result = empty($this->unrelief_bill_ids)?'-1':$this->unrelief_bill_ids;
                break;
            case self::$ID_COMBINATION_R_A_KO_KZ:
                $_arr = array_merge($this->bill_ids,$this->adv_bill_ids,$this->relief_bill_ids,$this->unrelief_bill_ids);
                $result = empty($_arr)?'-1':$_arr;
                break;
            case self::$ID_COMBINATION_R_KO_KZ:
                $_arr = array_merge($this->bill_ids,$this->relief_bill_ids,$this->unrelief_bill_ids);
                $result = empty($_arr)?'-1':$_arr;
                break;
            case self::$ID_COMBINATION_CR:
                $result = empty($this->canceled_bill_ids)?'-1':$this->canceled_bill_ids;
                break;
            case self::$ID_COMBINATION_CA:
                $result = empty($this->canceled_adv_bill_ids)?'-1':$this->canceled_adv_bill_ids;
                break;
//            case self::$ID_COMBINATION_KO:
//                $result = empty($this->relief_bill_ids)?'-1':$this->relief_bill_ids;
//                break;
//            case self::$ID_COMBINATION_KZ:
//                $result = empty($this->unrelief_bill_ids)?'-1':$this->unrelief_bill_ids;
//                break;
            case self::$ID_COMBINATION_CRA:
                $_arr = array_merge($this->canceled_bill_ids,$this->canceled_adv_bill_ids);
                $result = empty($_arr)?'-1':$_arr;
                break;
//            case self::$ID_COMBINATION_R_A_KO_KZ:
//                $_arr = array_merge($this->bill_ids,$this->adv_bill_ids,$this->relief_bill_ids,$this->unrelief_bill_ids);
//                $result = empty($_arr)?'-1':$_arr;
//                break;
//            case self::$ID_COMBINATION_R_KO_KZ:
//                $_arr = array_merge($this->bill_ids,$this->relief_bill_ids,$this->unrelief_bill_ids);
//                $result = empty($_arr)?'-1':$_arr;
//                break;
            case self::$ID_NONE;
                $result = '-1';
                break;
            default:
                throw new Exception('POPDVExport -> pogresna kombinazija: '.$types);
        }

        return $result;
    }
    
    public function getModelFilterParams($code)
    {
        return $this->getDBCriteriaParams($code)['model_filter'];
    }
    
    private $alias1;
    private $alias2;
    private $alias3;
    
    private function sameMonthCondition($column1, $column2, $time_sign)
    {
        return " and "
                . "(select month_id from accounting.bill_vat_month_id where id = $column1)"
                . " $time_sign "
                . "(select month_id from accounting.bill_vat_month_id where id = $column2)";
    }
    
    /**
     * 
     * @param type $bill_item_value_column
     * @param type $bill_item_vat_rate_column
     * @param type $connection_value_column
     * @return type
     */
    private function selectWithAdvance(
            $bill_item_value_column, 
            $bill_item_vat_rate_column, 
            $connection_value_column = 'vat_amount'
    )
    {
        $advance_bill_release_table = AdvanceBillRelease::tableName(); 
        $alias1 = $this->alias1;
        $alias2 = $this->alias2;
        $alias3 = $this->alias3;
        
        if (Yii::app()->configManager->get('accounting.popdv_show_advances_in_same_month'))
        {
            $_sub_part1 = '';
            $_sub_part2 = '';
        }
        else
        {
            $_sub_part1 = $this->sameMonthCondition($alias2.'.bill_id', $alias2.'.advance_bill_id', '!=');
            $_sub_part2 = ' -'
                . " (   SELECT COALESCE(sum($alias3.$connection_value_column),0::NUMERIC)"
                    . " FROM $advance_bill_release_table $alias3"
                    . " WHERE $alias3.advance_bill_id = $alias1.bill_id and vat_rate = $alias1.$bill_item_vat_rate_column"
                        . ' '.$this->sameMonthCondition($alias3.'.advance_bill_id', $alias3.'.bill_id', '=')
                . ')';
        }

        $is_internal_vat = ($bill_item_vat_rate_column=='internal_vat')?'TRUE':'FALSE';
        return 'COALESCE(sum( '. $bill_item_value_column
                //ako je racun, skida se avans koji nije u istom periodu
                . ' -'
                . " (   SELECT COALESCE(sum($alias2.$connection_value_column),0::NUMERIC)"
                    . " FROM $advance_bill_release_table $alias2"
                    . " WHERE $alias2.bill_id = $alias1.bill_id "
                //IZABRATI KOJE STAVKE UMANJENJE SE KORISTE
                    . "and $alias2.vat_rate = $alias1.$bill_item_vat_rate_column "
                    . "and $alias2.internal_vat = $is_internal_vat "
//                    . " and $alias1.order = (select min(abiabi.\"order\") from accounting.bill_items abiabi where bill_id = $alias1.bill_id) "
                
                //IZABRATI KOJA JE PRVA JEDINSTVENA STAVKA
                    . " and $alias1.order = ("
                                    . "select min(abiabi.\"order\") "
                                    . "from accounting.bill_items abiabi "
                                    . "where bill_id = $alias1.bill_id "
                                    . "and abiabi.$bill_item_vat_rate_column = $alias1.$bill_item_vat_rate_column "
                                    . "and abiabi.is_internal_vat = $is_internal_vat "
                    . ") "
                    . $_sub_part1
                . ')'
                
                //ako je avansni racun, skida se racun koji je u istom periodu
                . $_sub_part2
                
                
                . '),0::NUMERIC) as value';
    }
    
    private function selectAdvance($main_column, $bill_item_vat_column, $connection_value)
    {
        return $this->selectWithAdvance($main_column, $bill_item_vat_column, $connection_value);
    }
    
    /**
     * ova funkcija se koristi kada je storno u istom polju kao i regularna vrednost
     * funkcija koja generise sta treba da se sumira. OVO SLUZI SAMO ZA POLJA KOJA SU SUMA pa je ukljuceno i normalno knjizenje i storniranje
     * @param string $base_select_value
     * @return string
     */
    private function selectWithCanceled(string $base_select_value):string
    {
        $month_id = $this->month->id;
        $_parts = [];
        if (in_array($this->booking_type, [
                POPDVExport::BOOKING_TYPE_BOTH,
                POPDVExport::BOOKING_TYPE_REGULER
        ]))
        {
            $_parts[] =   " CASE WHEN (select month_id = $month_id from accounting.bill_vat_month_id where id = bill_id) "
                        . ' THEN'
                        . "     $base_select_value"
                        . ' ELSE '
                        . '     0'
                        . ' END';
        }
        
        if (in_array($this->booking_type, [
                POPDVExport::BOOKING_TYPE_BOTH,
                POPDVExport::BOOKING_TYPE_CANCELED
        ]))
        {
            $_parts[] =   " CASE WHEN (select canceled_month_id = $month_id from accounting.bill_vat_month_id where id = bill_id) "
                        . ' THEN'
                        . "     -$base_select_value"
                        . ' ELSE '
                        . '     0'
                        . ' END';
        }
        
        return '( ' . implode(' + ', $_parts).' )';        
    }
    
    public function getDBCriteriaParams($code)
    {
        switch ($code) 
        {
            case '11': 
            case '12': 
            case '13': 
            case '14': 
            case '21': 
            case '22': 
            case '23': 

                switch ($code)
                {
                    case '11': $_no_vat_type = POPDVBillLists::$I_0_2; break;
                    case '12': $_no_vat_type = POPDVBillLists::$I_0_3; break;
                    case '13': $_no_vat_type = POPDVBillLists::$I_0_4; break;
                    case '14': $_no_vat_type = POPDVBillLists::$I_0_1; break;
                    case '21': $_no_vat_type = POPDVBillLists::$I_0_6; break;
                    case '22': $_no_vat_type = POPDVBillLists::$I_0_7; break;
                    case '23': $_no_vat_type = POPDVBillLists::$I_0_8; break;
                    
                }
                
                $select_value = $this->selectWithCanceled('value');
                $canceled_bill_ids = $this->getIDSes(self::$ID_COMBINATION_CR);

                return [
                    'Model' => 'BillItem',
                    'alias' => $this->alias1,
                    'model_filter' => [
                        'OR',
                        [
                            'bill' => [
                                'ids' => $this->getIDSes(self::$ID_COMBINATION_R_KO_KZ)
                            ],
                            'no_vat_type' => $_no_vat_type
                        ],
                        [
                            'bill' => [
                                'ids' => $canceled_bill_ids
                            ],
                            'no_vat_type' => $_no_vat_type
                        ]
                        
                    ],
                    'select' => "COALESCE(sum($select_value),0::numeric) as value",
                ];
                
                
            case '15': 
            case '24': 
            case '25': 
                
            case '17': 
            case '27': 
                
                switch ($code)
                {
                    case '15': case '17': $_no_vat_type =  [
                                                    'OR',
                                                    ['no_vat_type' => POPDVBillLists::$I_0_1],
                                                    ['no_vat_type' => POPDVBillLists::$I_0_2],
                                                    ['no_vat_type' => POPDVBillLists::$I_0_3],
                                                    ['no_vat_type' => POPDVBillLists::$I_0_4]
                                                ]; break;
                    case '24': $_no_vat_type =  [
                                                    'OR',
                                                    ['no_vat_type' => POPDVBillLists::$I_0_5],
                                                    ['no_vat_type' => POPDVBillLists::$I_0_5b]
                                                ]; break;
                    case '25': case '27': $_no_vat_type =  [
                                                    'OR',
                                                    ['no_vat_type' => POPDVBillLists::$I_0_5],
                                                    ['no_vat_type' => POPDVBillLists::$I_0_5b],
                                                    ['no_vat_type' => POPDVBillLists::$I_0_6],
                                                    ['no_vat_type' => POPDVBillLists::$I_0_7],
                                                    ['no_vat_type' => POPDVBillLists::$I_0_8]
                                                ]; break;
                }
                
                $_bill_type = (in_array($code, ['17','27']))? self::$ID_COMBINATION_A: self::$ID_COMBINATION_R_KO_KZ;
            
                $_canceled_bill_type = (in_array($code, ['17','27']))? self::$ID_NONE: self::$ID_COMBINATION_CR;
                
                $_select = (in_array($code, ['17','27']))? 
                        $this->selectWithAdvance('value', 'vat', 'base_amount') : 
                        "COALESCE(sum(".$this->selectWithCanceled('value')."),0::numeric) as value";
                
                return [
                    'Model' => 'BillItem',
                    'alias' => $this->alias1,
                    'model_filter' => [
                        'OR',
                        [
                            'AND',
                            ['bill' => [
                                    'ids' => $this->getIDSes($_bill_type)
                            ]],
                            $_no_vat_type
                        ],
                        [
                            'AND',
                            ['bill' => [
                                    'ids' => $this->getIDSes($_canceled_bill_type)
                            ]],
                            $_no_vat_type
                        ]  
                    ],
                    'select' => $_select,
                ];
            case '16': case '26': throw new SIMAExceptionPOPDVCodeJustSum($code);
                
        case '311': case '313': case '321': case '323': 
            $_vat_rate = (in_array($code, ['311','321']))?BillItem::$VAT_20:BillItem::$VAT_10;
            $_no_vat_type = (in_array($code, ['311','313']))? POPDVBillLists::$I_1_2: POPDVBillLists::$I_1_1;
            return [ //isti kao ispod, samo osnovica
                'Model' => 'BillItem',
                'model_filter' => [
                    'bill' => [
                        'ids' => $this->getIDSes(self::$ID_COMBINATION_R)
                    ],
                    'no_vat_type' => $_no_vat_type,
                    'vat' => $_vat_rate
                ],
                'select' => 'COALESCE(sum(value),0::NUMERIC) as value',
            ];
        case '312': case '314': case '322':  case '324':
            $_vat_rate = (in_array($code, ['322','312']))?BillItem::$VAT_20:BillItem::$VAT_10;
            $_no_vat_type = (in_array($code, ['312','314']))? POPDVBillLists::$I_1_2: POPDVBillLists::$I_1_1;

            return [ //isti kao ispod, samo PDV
                'Model' => 'BillItem',
                'alias' => $this->alias1,
                'model_filter' => [
                    'bill' => [
                        'ids' => $this->getIDSes(self::$ID_COMBINATION_R)
                    ],
                    'no_vat_type' => $_no_vat_type,
                    'vat' => $_vat_rate
                ],
                'select' => $this->selectWithAdvance('value_vat','vat'),
            ];
        case '331': case '333': case '341': case '343':
            $_vat_rate = (in_array($code, ['331','341']))?BillItem::$VAT_20:BillItem::$VAT_10;
            $_no_vat_type = (in_array($code, ['331','333']))? POPDVBillLists::$I_1_4: POPDVBillLists::$I_1_3;
            return [ 
                'Model' => 'BillItem',
                'model_filter' => [
                    'bill' => [
                        'ids' => $this->getIDSes(self::$ID_COMBINATION_R)
                    ],
                    'no_vat_type' => $_no_vat_type,
                    'internal_vat' => $_vat_rate
                ],
                'select' => 'COALESCE(sum(value),0::numeric) as value',
            ];
        case '351': case '352': case '353': case '354':
        case '361': case '362': case '363': case '364':
            $_vat_rate = (in_array($code, ['351','352','361','362']))?BillItem::$VAT_20:BillItem::$VAT_10;
            $_bill_type = (in_array($code, ['351','352','353','354']))? self::$ID_COMBINATION_KZ: self::$ID_COMBINATION_KO;
            
            switch ($code)
            {
                case '361':
                case '363':
                    $_canceled_bill_type = self::$ID_COMBINATION_CR;    break;
                case '362':
                case '364':
                    $_canceled_bill_type = self::$ID_COMBINATION_CRA;   break;
                default: 
                    $_canceled_bill_type = self::$ID_NONE;              break;
            }
            
            $_sum_of = (in_array($code, ['351','353','361','363']))? 'value': 'value_vat';
            
            /**
             * ovde se gleda samo negacija, ne moze da se iskoristi funkcija $this->selectWithCanceled
             * jer je moguce da je dokument storniran u istom mesecu
             */
            $canceled_bill_ids = $this->getIDSes($_canceled_bill_type);
            if (is_array($canceled_bill_ids))
            {
                $canceled_bill_ids = implode(',', $canceled_bill_ids);
            }
            
            $select_value = " CASE WHEN bill_id in ($canceled_bill_ids) "
            . ' THEN'
            . "     -$_sum_of"
            . ' ELSE '
            . "     $_sum_of"
            . ' END';
            
            return [ 
                'Model' => 'BillItem',
                'model_filter' => [
                    'OR',
                    [
                        //deo za regularne racune
                    'AND',
                    ['bill' => [
                            'ids' => $this->getIDSes($_bill_type)
                    ]],
                    [
                        'OR',
                        ['no_vat_type' => POPDVBillLists::$I_1_1],
                        ['no_vat_type' => POPDVBillLists::$I_1_2],
                        ['no_vat_type' => POPDVBillLists::$I_1_3],
                        ['no_vat_type' => POPDVBillLists::$I_1_4]
                    ],
                    [   
                        'OR',
                        ['vat' => $_vat_rate],
                        ['internal_vat' => $_vat_rate],
                    ]
                        
                    ],
                    [
                        //stornirani racuni
                        'AND',
                        ['bill' => [
                                'ids' => $this->getIDSes($_canceled_bill_type)
                        ]],
                        [
                            'OR',
                            ['no_vat_type' => POPDVBillLists::$I_1_1],
                            ['no_vat_type' => POPDVBillLists::$I_1_2],
                            ['no_vat_type' => POPDVBillLists::$I_1_3],
                            ['no_vat_type' => POPDVBillLists::$I_1_4]
                        ],
                        [   
                            'OR',
                            ['vat' => $_vat_rate],
                            ['internal_vat' => $_vat_rate],
                        ]
                    ]
                    

                ],
                'select' => "COALESCE(sum($select_value),0::NUMERIC) as value",
            ];
        case '381': case '383':
            $_vat_rate = (in_array($code, ['381']))?BillItem::$VAT_20:BillItem::$VAT_10;

            $select_value = $this->selectWithCanceled('value');
            $canceled_bill_ids = $this->getIDSes(self::$ID_COMBINATION_CR);
            
            return [ 
                'Model' => 'BillItem',
                'model_filter' => [
                    'OR',
                    [
                        'AND',
                        ['bill' => [
                                'ids' => $this->getIDSes(self::$ID_COMBINATION_R_KO_KZ)
                        ]],
                        [
                            'OR',
                            ['no_vat_type' => POPDVBillLists::$I_1_1],
                            ['no_vat_type' => POPDVBillLists::$I_1_2],
                            ['no_vat_type' => POPDVBillLists::$I_1_3],
                            ['no_vat_type' => POPDVBillLists::$I_1_4]
                        ],
                        [   
                            'OR',
                            ['vat' => $_vat_rate],
                            ['internal_vat' => $_vat_rate],
                        ]  
                    ],
                    [
                        'AND',
                        ['bill' => [
                                'ids' => $canceled_bill_ids
                        ]],
                        [
                            'OR',
                            ['no_vat_type' => POPDVBillLists::$I_1_1],
                            ['no_vat_type' => POPDVBillLists::$I_1_2],
                            ['no_vat_type' => POPDVBillLists::$I_1_3],
                            ['no_vat_type' => POPDVBillLists::$I_1_4]
                        ],
                        [   
                            'OR',
                            ['vat' => $_vat_rate],
                            ['internal_vat' => $_vat_rate],
                        ]  
                    ]
                    
                ],
                'select' => "COALESCE(sum($select_value),0::NUMERIC) as value",
            ];
        case '382':  case '384': 
        case '3102': case '3104':
            $_vat_rate = (in_array($code, ['382','3102']))?BillItem::$VAT_20:BillItem::$VAT_10;
            $_bill_type = (in_array($code, ['382','384']))? self::$ID_COMBINATION_R_KO_KZ: self::$ID_COMBINATION_R_A_KO_KZ;
            
            $select_value = $this->selectWithCanceled('value_vat');
            $canceled_bill_ids = $this->getIDSes(self::$ID_COMBINATION_CRA);
            
            
            return [ 
                'Model' => 'BillItem',
                'alias' => $this->alias1,
                'model_filter' => [
                    'OR',
                    [
                        'AND',
                        ['bill' => [
                            'ids' => $this->getIDSes($_bill_type)
                        ]],
                        [
                            'OR',
                            ['no_vat_type' => POPDVBillLists::$I_1_1],
                            ['no_vat_type' => POPDVBillLists::$I_1_2],
                            ['no_vat_type' => POPDVBillLists::$I_1_3],
                            ['no_vat_type' => POPDVBillLists::$I_1_4]
                        ],
                        ['vat' => $_vat_rate] //ide samo klasican PDV, posto se za izlazne racune ne gleda interni PDV
                    
                    ],
                    [
                        'AND',
                        ['bill' => [
                                'ids' => $canceled_bill_ids
                        ]],
                        [
                            'OR',
                            ['no_vat_type' => POPDVBillLists::$I_1_1],
                            ['no_vat_type' => POPDVBillLists::$I_1_2],
                            ['no_vat_type' => POPDVBillLists::$I_1_3],
                            ['no_vat_type' => POPDVBillLists::$I_1_4]
                        ],
                        ['vat' => $_vat_rate] //ide samo klasican PDV, posto se za izlazne racune ne gleda interni PDV 
                    ]
                    

                ],
                'select' => $this->selectWithAdvance($select_value, 'vat'),
            ];
        case '391': case '392': case '393': case '394':
            $_vat_rate = (in_array($code, ['391','392']))?BillItem::$VAT_20:BillItem::$VAT_10;
            $_value_column = (in_array($code, ['391','393']))?'value':'value_vat';
            $_connection_value_column = (in_array($code, ['391','393']))?'base_amount':'vat_amount';
            return [ 
                'Model' => 'BillItem',
                'alias' => $this->alias1,
                'model_filter' => [
                    'AND',
                    ['bill' => [
                            'ids' => $this->getIDSes(self::$ID_COMBINATION_A)
                    ]],
                    [
                        'OR',
                        ['no_vat_type' => POPDVBillLists::$I_1_1],
                        ['no_vat_type' => POPDVBillLists::$I_1_2],
                        ['no_vat_type' => POPDVBillLists::$I_1_3],
                        ['no_vat_type' => POPDVBillLists::$I_1_4]
                    ],
                    [   
                        'OR',
                        ['vat' => $_vat_rate],
                        ['internal_vat' => $_vat_rate],
                    ]
                ],
                'select' => $this->selectAdvance($_value_column, 'vat', $_connection_value_column)
            ];
        case '3a11': case '3a12':
        case '3a21': case '3a22':
        case '3a31': case '3a32':
            $_vat_rate = (in_array($code, ['3a11','3a21','3a31']))?BillItem::$VAT_20:BillItem::$VAT_10;
            switch ($code)
            {
                case '3a11': case '3a12': $_no_vat_type = POPDVBillLists::$U_1_4; break;
                case '3a21': case '3a22': $_no_vat_type = POPDVBillLists::$U_1_5; break;
                case '3a31': case '3a32': $_no_vat_type = POPDVBillLists::$U_1_3; break;
            }
            return [ //isti kao ispod, samo PDV
                'Model' => 'BillItem',
                'alias' => $this->alias1,
                'model_filter' => [
                    'AND',
                    ['bill' => [
                            'ids' => $this->getIDSes(self::$ID_COMBINATION_R)
                    ]],
                    [
                        'OR',
                        ['no_vat_type' => $_no_vat_type]
                    ],
                    ['internal_vat' => $_vat_rate]
                ],
                'select' => $this->selectWithAdvance('value_internal_vat','internal_vat'),
            ];
        case '3a41': case '3a42':
        case '3a51': case '3a52': 
            $_vat_rate = (in_array($code, ['3a41','3a51']))?BillItem::$VAT_20:BillItem::$VAT_10;
            
            $_bill_type =          (in_array($code, ['3a51','3a52']))? self::$ID_COMBINATION_KO: self::$ID_COMBINATION_KZ;
            $_canceled_bill_type = (in_array($code, ['3a51','3a52']))? self::$ID_COMBINATION_CRA: self::$ID_NONE;
            
            $_sum_of = 'value_internal_vat';
            $canceled_bill_ids = $this->getIDSes($_canceled_bill_type);
            if (is_array($canceled_bill_ids))
            {
                $canceled_bill_ids = implode(',', $canceled_bill_ids);
            }
            
            $select_value = " CASE WHEN bill_id in ($canceled_bill_ids) "
            . ' THEN'
            . "     -$_sum_of"
            . ' ELSE '
            . "     $_sum_of"
            . ' END';
            
            return [ 
            'Model' => 'BillItem',
            'model_filter' => [
                'OR',
                [
                    'AND',
                    ['bill' => [
                            'ids' => $this->getIDSes($_bill_type)
                    ]],
                    [
                        'OR',
                        ['no_vat_type' => POPDVBillLists::$U_1_3],
                        ['no_vat_type' => POPDVBillLists::$U_1_4],
                        ['no_vat_type' => POPDVBillLists::$U_1_5]
                    ],
                    ['internal_vat' => $_vat_rate]
                ],
                [
                    'AND',
                    ['bill' => [
                            'ids' => $canceled_bill_ids
                    ]],
                    [
                        'OR',
                        ['no_vat_type' => POPDVBillLists::$U_1_3],
                        ['no_vat_type' => POPDVBillLists::$U_1_4],
                        ['no_vat_type' => POPDVBillLists::$U_1_5]
                    ],
                    ['internal_vat' => $_vat_rate]
                ]
                
            ],
            'select' => "COALESCE(sum($select_value),0::NUMERIC) as value",
            ];
        case '3a61': throw new SIMAExceptionPOPDVCodeJustSum($code);
        case '3a62': throw new SIMAExceptionPOPDVCodeJustSum($code);
        case '3a71':  case '3a72':
        case '3a91':  case '3a92':
        case '3a91r': case '3a92r':
            $_vat_rate = (in_array($code, ['3a71','3a91','3a91r']))?BillItem::$VAT_20:BillItem::$VAT_10;
            $_bill_type = (in_array($code, ['3a71','3a72']))? self::$ID_COMBINATION_R_KO_KZ: self::$ID_COMBINATION_R_A_KO_KZ;
            
            $_vat_refusal = (in_array($code, ['3a91r','3a92r']))?['vat_refusal' => true]:[];
            
            $select_value = $this->selectWithCanceled('value_internal_vat');
            $canceled_bill_ids = $this->getIDSes(self::$ID_COMBINATION_CRA);
            
            return [ 
                'Model' => 'BillItem',
                'alias' => $this->alias1,
                'model_filter' => [
                    'OR',
                    [
                        'AND',
                        ['bill' => [
                                'ids' => $this->getIDSes($_bill_type)
                        ]],
                        [
                            'OR',
                            ['no_vat_type' => POPDVBillLists::$U_1_3],
                            ['no_vat_type' => POPDVBillLists::$U_1_4],
                            ['no_vat_type' => POPDVBillLists::$U_1_5]
                        ],
                        ['internal_vat' => $_vat_rate],
                        $_vat_refusal
                    ],
                    [
                        'AND',
                        ['bill' => [
                                'ids' => $canceled_bill_ids
                        ]],
                        [
                            'OR',
                            ['no_vat_type' => POPDVBillLists::$U_1_3],
                            ['no_vat_type' => POPDVBillLists::$U_1_4],
                            ['no_vat_type' => POPDVBillLists::$U_1_5]
                        ],
                        ['internal_vat' => $_vat_rate],
                        $_vat_refusal
                    ]
                        
                ],
                'select' => $this->selectWithAdvance($select_value, 'internal_vat')
            ];
        case '3a81': case '3a82':
            $_vat_rate = (in_array($code, ['3a81']))?BillItem::$VAT_20:BillItem::$VAT_10;
            return [ 
                'Model' => 'BillItem',
                'alias' => $this->alias1,
                'model_filter' => [
                    'AND',
                    ['bill' => [
                            'ids' => $this->getIDSes(self::$ID_COMBINATION_A)
                    ]],
                    [
                        'OR',
                        ['no_vat_type' => POPDVBillLists::$U_1_3],
                        ['no_vat_type' => POPDVBillLists::$U_1_4],
                        ['no_vat_type' => POPDVBillLists::$U_1_5]
                    ],
                    ['internal_vat' => $_vat_rate]
                ],
//                'select' => 'COALESCE(sum(value_internal_vat),0::NUMERIC) as value',
                'select' => $this->selectAdvance('value_internal_vat', 'internal_vat', 'vat_amount'),
            ];
            
        case '51': case '54':
//            throw new SIMAExceptionPOPDVCodeJustSum($code);
            //radi za 381, ali je pitanje sta ce biti za posebne stope oporezivanja
            
            $select_value = $this->selectWithCanceled('value');
            $canceled_bill_ids = $this->getIDSes(self::$ID_COMBINATION_CR);
            
            $_vat_rate = (in_array($code, ['51']))?BillItem::$VAT_20:BillItem::$VAT_10;
            
            return [ 
                'Model' => 'BillItem',
                'model_filter' => [
                    'OR',
                    [
                    'AND',
                    ['bill' => [
                            'ids' => $this->getIDSes(self::$ID_COMBINATION_R_KO_KZ)
                    ]],
                    [
                        'OR',
                        ['no_vat_type' => POPDVBillLists::$I_1_1],
                        ['no_vat_type' => POPDVBillLists::$I_1_2],
                        ['no_vat_type' => POPDVBillLists::$I_1_3],
                        ['no_vat_type' => POPDVBillLists::$I_1_4]
                    ],
                    [
                        'OR',
                        ['vat' => $_vat_rate],
                        ['internal_vat' => $_vat_rate]
                    ]
                        
                    ],
                    [
                        'AND',
                        ['bill' => [
                                'ids' => $canceled_bill_ids
                        ]],
                        [
                            'OR',
                            ['no_vat_type' => POPDVBillLists::$I_1_1],
                            ['no_vat_type' => POPDVBillLists::$I_1_2],
                            ['no_vat_type' => POPDVBillLists::$I_1_3],
                            ['no_vat_type' => POPDVBillLists::$I_1_4]
                        ],
                        [
                            'OR',
                            ['vat' => $_vat_rate],
                            ['internal_vat' => $_vat_rate]
                        ]
                        
                    ]
                    
                ],
                'select' => "COALESCE(sum($select_value),0::NUMERIC) as value",
            ];   
        
        case '52': case '55':
            
            throw new SIMAExceptionPOPDVCodeJustSum($code);
            
//            $_vat_rate = (in_array($code, ['52']))?BillItem::$VAT_20:BillItem::$VAT_10;
////            $_bill_type = (in_array($code, ['382','384']))? self::$ID_COMBINATION_R_KO_KZ: self::$ID_COMBINATION_R_A_KO_KZ;
//            $arra1 = [ 
//                'Model' => 'BillItem',
//                'alias' => $this->alias1,
//                'model_filter' => [
//                    'AND',
//                    ['bill' => [
//                            'ids' => $this->getIDSes(self::$ID_COMBINATION_R_A_KO_KZ)
//                    ]],
//                    [
//                        'OR',
//                        ['no_vat_type' => POPDVBillLists::$I_1_1],
//                        ['no_vat_type' => POPDVBillLists::$I_1_2],
//                        ['no_vat_type' => POPDVBillLists::$I_1_3],
//                        ['no_vat_type' => POPDVBillLists::$I_1_4]
//                    ],
//                    ['vat' => $_vat_rate]
//
//                ],
//                'select' => $this->selectWithAdvance('value_vat', 'vat'),
//            ];
//            
//            
//            
//            $_bill_type = (in_array($code, ['3a71','3a72']))? self::$ID_COMBINATION_R_KO_KZ: self::$ID_COMBINATION_R_A_KO_KZ;
//            $arra2 = [ 
//                'Model' => 'BillItem',
//                'alias' => $this->alias1,
//                'model_filter' => [
//                    'AND',
//                    ['bill' => [
//                            'ids' => $this->getIDSes( self::$ID_COMBINATION_R_A_KO_KZ)
//                    ]],
//                    [
//                        'OR',
//                        ['no_vat_type' => POPDVBillLists::$U_1_3],
//                        ['no_vat_type' => POPDVBillLists::$U_1_4],
//                        ['no_vat_type' => POPDVBillLists::$U_1_5]
//                    ],
//                    ['internal_vat' => $_vat_rate]
//                ],
//                'select' => $this->selectWithAdvance('value_internal_vat', 'internal_vat')
//            ];
        case '56': throw new SIMAExceptionPOPDVCodeJustSum($code);
        case '61':
            return [ 
                'Model' => 'BillItem',
                'model_filter' => [
                    'AND',
                    ['bill' => [
                            'ids' => $this->getIDSes(self::$ID_COMBINATION_R_KO_KZ)
                    ]],
                    [
                        'OR',
                        ['no_vat_type' => POPDVBillLists::$U_2_Cb]
                    ],
                    ['vat_customs' => BillItem::$VAT_0]
                ],
                'select' => 'COALESCE(sum(value_vat_customs),0::NUMERIC) as value',
            ];        
        case '6211': case '6212':
        case '6221': case '6222':
        case '6231': case '6232':
            $_vat_rate = (in_array($code, ['6211','6221','6231']))?BillItem::$VAT_20:BillItem::$VAT_10;
            switch ($code)
            {
                case '6211': case '6212': $_bill_type = self::$ID_COMBINATION_R; break;
                case '6221': case '6222': $_bill_type = self::$ID_COMBINATION_KZ; break;
                case '6231': case '6232': $_bill_type = self::$ID_COMBINATION_KO; break;
            }
            return [ 
                'Model' => 'BillItem',
                'model_filter' => [
                    'AND',
                    ['bill' => [
                            'ids' => $this->getIDSes($_bill_type)
                    ]],
                    [
                        'OR',
                        ['no_vat_type' => POPDVBillLists::$U_2_Cb]
                    ],
                    ['vat_customs' => $_vat_rate]

                ],
                'select' => 'COALESCE(sum(value_customs),0::NUMERIC) as value',
            ];
        case '63':
            return [ 
                'Model' => 'BillItem',
                'model_filter' => [
                    'AND',
                    ['bill' => [
                            'ids' => $this->getIDSes(self::$ID_COMBINATION_R_KO_KZ)
                    ]],
                    [
                        'OR',
                        ['no_vat_type' => POPDVBillLists::$U_2_Cb]
                    ],
                ],
                'select' => 'COALESCE(sum(value_customs),0::NUMERIC) as value',
            ];        
            
        case '64':
            return [ 
                'Model' => 'BillItem',
                'model_filter' => [
                    'AND',
                    ['bill' => [
                            'ids' => $this->getIDSes(self::$ID_COMBINATION_R)
                    ]],
                    [
                        'OR',
                        ['no_vat_type' => POPDVBillLists::$U_2_Cb]
                    ],
                ],
                'select' => 'COALESCE(sum(value_vat_customs),0::NUMERIC) as value',
            ];        
            
        case '8a11': case '8a13': 
        case '8a21': case '8a23': 
            $_vat_rate = (in_array($code, ['8a11','8a21']))?BillItem::$VAT_20:BillItem::$VAT_10;
            $_no_vat_type = (in_array($code, ['8a11','8a13']))? POPDVBillLists::$U_1_2: POPDVBillLists::$U_1_1;
            return [ 
                'Model' => 'BillItem',
                'model_filter' => [
                    'AND',
                    ['bill' => [
                            'ids' => $this->getIDSes(self::$ID_COMBINATION_R)
                    ]],
                    [
                        'OR',
                        ['no_vat_type' => $_no_vat_type]
                    ],
                    ['vat' => $_vat_rate]

                ],
                'select' => 'COALESCE(sum(value),0::NUMERIC) as value',
            ];
        case '8a12': case '8a14':
        case '8a22': case '8a24':
            $_vat_rate = (in_array($code, ['8a12','8a22']))?BillItem::$VAT_20:BillItem::$VAT_10;
            $_no_vat_type = (in_array($code, ['8a12','8a14']))? POPDVBillLists::$U_1_2: POPDVBillLists::$U_1_1;
            return [ 
                'Model' => 'BillItem',
                'alias' => $this->alias1,
                'model_filter' => [
                    'AND',
                    ['bill' => [
                            'ids' => $this->getIDSes(self::$ID_COMBINATION_R)
                    ]],
                    [
                        'OR',
                        ['no_vat_type' => $_no_vat_type]
                    ],
                    ['vat' => $_vat_rate]

                ],
                'select' => $this->selectWithAdvance('value_vat', 'vat')
            ];
        case '8a41': return [ 
            'Model' => 'BillItem',
            'model_filter' => [
                'AND',
                ['bill' => [
                        'ids' => $this->getIDSes(self::$ID_COMBINATION_KZ)
                ]],
                [
                    'OR',
                    ['no_vat_type' => POPDVBillLists::$U_1_1],
                    ['no_vat_type' => POPDVBillLists::$U_1_2]
                ],
                ['vat' => BillItem::$VAT_20]
                
            ],
            'select' => 'COALESCE(sum(value),0::NUMERIC) as value',
        ];
        case '8a42': return [ 
            'Model' => 'BillItem',
            'model_filter' => [
                'AND',
                ['bill' => [
                        'ids' => $this->getIDSes(self::$ID_COMBINATION_KZ)
                ]],
                [
                    'OR',
                    ['no_vat_type' => POPDVBillLists::$U_1_1],
                    ['no_vat_type' => POPDVBillLists::$U_1_2]
                ],
                ['vat' => BillItem::$VAT_20]
                
            ],
            'select' => 'COALESCE(sum(value_vat),0::NUMERIC) as value',
        ];
        case '8a43': return [ 
            'Model' => 'BillItem',
            'model_filter' => [
                'AND',
                ['bill' => [
                        'ids' => $this->getIDSes(self::$ID_COMBINATION_KZ)
                ]],
                [
                    'OR',
                    ['no_vat_type' => POPDVBillLists::$U_1_1],
                    ['no_vat_type' => POPDVBillLists::$U_1_2]
                ],
                ['vat' => BillItem::$VAT_10]
                
            ],
            'select' => 'COALESCE(sum(value),0::NUMERIC) as value',
        ];
        case '8a44': return [ 
            'Model' => 'BillItem',
            'model_filter' => [
                'AND',
                ['bill' => [
                        'ids' => $this->getIDSes(self::$ID_COMBINATION_KZ)
                ]],
                [
                    'OR',
                    ['no_vat_type' => POPDVBillLists::$U_1_1],
                    ['no_vat_type' => POPDVBillLists::$U_1_2]
                ],
                ['vat' => BillItem::$VAT_10]
                
            ],
            'select' => 'COALESCE(sum(value_vat),0::NUMERIC) as value',
        ];
        case '8a51': case '8a52': 
        case '8a53': case '8a54':
         
            $_vat_rate = (in_array($code, ['8a51','8a52']))?BillItem::$VAT_20:BillItem::$VAT_10;
//            $select_value = $this->selectWithCanceled('value_vat');
            $_sum_of = (in_array($code, ['8a51','8a53']))?'value':'value_vat';;
            $canceled_bill_ids = $this->getIDSes(
                    (in_array($code, ['8a51','8a53']))?
                        self::$ID_COMBINATION_CR:
                        self::$ID_COMBINATION_CRA
                    );
            
            if (is_array($canceled_bill_ids))
            {
                $canceled_bill_ids = implode(',', $canceled_bill_ids);
            }
            
            $select_value = " CASE WHEN bill_id in ($canceled_bill_ids) "
            . ' THEN'
            . "     -$_sum_of"
            . ' ELSE '
            . "     $_sum_of"
            . ' END';
            
            return [ 
            'Model' => 'BillItem',
            'model_filter' => [
                'OR',
                [
                    'AND',
                    ['bill' => [
                            'ids' => $this->getIDSes(self::$ID_COMBINATION_KO)
                    ]],
                    [
                        'OR',
                        ['no_vat_type' => POPDVBillLists::$U_1_1],
                        ['no_vat_type' => POPDVBillLists::$U_1_2]
                    ],
                    ['vat' => $_vat_rate]
                ],
                [
                    'AND',
                    ['bill' => [
                            'ids' => $canceled_bill_ids
                    ]],
                    [
                        'OR',
                        ['no_vat_type' => POPDVBillLists::$U_1_1],
                        ['no_vat_type' => POPDVBillLists::$U_1_2]
                    ],
                    ['vat' => $_vat_rate]
                ]
            ],
            'select' => "COALESCE(sum($select_value),0::NUMERIC) as value",
        ];
        case '8a61': case '8a63':
            $_vat_rate = (in_array($code, ['8a61']))?BillItem::$VAT_20:BillItem::$VAT_10;
            
            $select_value = $this->selectWithCanceled('value');
            $canceled_bill_ids = $this->getIDSes(self::$ID_COMBINATION_CR);
            
            return [ 
                'Model' => 'BillItem',
                'model_filter' => [
                    'OR',
                    [
                        'AND',
                        ['bill' => [
                                'ids' => $this->getIDSes(self::$ID_COMBINATION_R_KO_KZ)
                        ]],
                        [
                            'OR',
                            ['no_vat_type' => POPDVBillLists::$U_1_1],
                            ['no_vat_type' => POPDVBillLists::$U_1_2]
                        ],
                        ['vat' => $_vat_rate]
                    ],
                    [
                        'AND',
                        ['bill' => [
                                'ids' => $canceled_bill_ids
                        ]],
                        [
                            'OR',
                            ['no_vat_type' => POPDVBillLists::$U_1_1],
                            ['no_vat_type' => POPDVBillLists::$U_1_2]
                        ],
                        ['vat' => $_vat_rate]
                    ]
                ],
                'select' => "COALESCE(sum($select_value),0::numeric) as value",
            ];
        case '8a71': case '8a73':
            $_vat_rate = (in_array($code, ['8a71']))?BillItem::$VAT_20:BillItem::$VAT_10;
            return [ 
                'Model' => 'BillItem',
                'alias' => $this->alias1,
                'model_filter' => [
                    'AND',
                    ['bill' => [
                            'ids' => $this->getIDSes(self::$ID_COMBINATION_A)
                    ]],
                    [
                        'OR',
                        ['no_vat_type' => POPDVBillLists::$U_1_1],
                        ['no_vat_type' => POPDVBillLists::$U_1_2]
                    ],
                    ['vat' => $_vat_rate]
                ],
                'select' => $this->selectWithAdvance('value', 'vat', 'base_amount'),
            ];
        case '8a72': case '8a74':
            $_vat_rate = (in_array($code, ['8a72']))?BillItem::$VAT_20:BillItem::$VAT_10;
            return [ 
                'Model' => 'BillItem',
                'alias' => $this->alias1,
                'model_filter' => [
                    'AND',
                    ['bill' => [
                            'ids' => $this->getIDSes(self::$ID_COMBINATION_A)
                    ]],
                    [
                        'OR',
                        ['no_vat_type' => POPDVBillLists::$U_1_1],
                        ['no_vat_type' => POPDVBillLists::$U_1_2]
                    ],
                    ['vat' => $_vat_rate]
                ],
                'select' => $this->selectWithAdvance('value_vat', 'vat', 'vat_amount'),
            ];
        case '8a82':  case '8a84':
        case '8a82r': case '8a84r':
            
            $select_value = $this->selectWithCanceled('value_vat');
            $canceled_bill_ids = $this->getIDSes(self::$ID_COMBINATION_CRA);
            
            
            $_vat_rate = (in_array($code, ['8a82','8a82r']))?BillItem::$VAT_20:BillItem::$VAT_10;
            $_vat_refusal = (in_array($code, ['8a82','8a84']))?[]:['vat_refusal' => true];
            return [ 
                'Model' => 'BillItem',
                'alias' => $this->alias1,
                'model_filter' => [
                    'OR',
                    [
                        'AND',
                        ['bill' => [
                                'ids' => $this->getIDSes(self::$ID_COMBINATION_R_A_KO_KZ)
                        ]],
                        [
                            'OR',
                            ['no_vat_type' => POPDVBillLists::$U_1_1],
                            ['no_vat_type' => POPDVBillLists::$U_1_2]
                        ],
                        ['vat' => $_vat_rate],
                        $_vat_refusal
                    ],
                    [
                        'AND',
                        ['bill' => [
                                'ids' => $canceled_bill_ids
                        ]],
                        [
                            'OR',
                            ['no_vat_type' => POPDVBillLists::$U_1_1],
                            ['no_vat_type' => POPDVBillLists::$U_1_2]
                        ],
                        ['vat' => $_vat_rate],
                        $_vat_refusal
                    ]
                    
                ],
                'select' => $this->selectWithAdvance($select_value, 'vat', 'vat_amount')
            ];
        case '8b11': case '8b12':
        case '8b21': case '8b22':
            $_vat_rate = (in_array($code, ['8b11','8b21']))?BillItem::$VAT_20:BillItem::$VAT_10;
            $_no_vat_type = (in_array($code, ['8b11','8b12']))?POPDVBillLists::$U_1_4:POPDVBillLists::$U_1_3;
            return [ 
                'Model' => 'BillItem',
                'model_filter' => [
                    'AND',
                    ['bill' => [
                            'ids' => $this->getIDSes(self::$ID_COMBINATION_R)
                    ]],
                    [
                        'OR',
                        ['no_vat_type' => $_no_vat_type]
                    ],
                    ['internal_vat' => $_vat_rate]

                ],
                'select' => 'COALESCE(sum(value),0::numeric) as value',
            ];
        case '8b41': case '8b42': 
            $_vat_rate = (in_array($code, ['8b41']))?BillItem::$VAT_20:BillItem::$VAT_10;
            return [ 
                'Model' => 'BillItem',
                'model_filter' => [
                    'AND',
                    ['bill' => [
                            'ids' => $this->getIDSes(self::$ID_COMBINATION_KZ)
                    ]],
                    [
                        'OR',
                        ['no_vat_type' => POPDVBillLists::$U_1_3],
                        ['no_vat_type' => POPDVBillLists::$U_1_4]
                    ],
                    ['internal_vat' => $_vat_rate]

                ],
                'select' => 'COALESCE(sum(value),0::numeric) as value',
            ];
        case '8b51': case '8b52': 
            $_vat_rate = (in_array($code, ['8b51']))?BillItem::$VAT_20:BillItem::$VAT_10;
            
            $_sum_of = 'value';
            $canceled_bill_ids = $this->getIDSes(self::$ID_COMBINATION_CR);
            
            if (is_array($canceled_bill_ids))
            {
                $canceled_bill_ids = implode(',', $canceled_bill_ids);
            }
            
            $select_value = " CASE WHEN bill_id in ($canceled_bill_ids) "
            . ' THEN'
            . "     -$_sum_of"
            . ' ELSE '
            . "     $_sum_of"
            . ' END';
            
            return [ 
                'Model' => 'BillItem',
                'model_filter' => [
                    'OR',
                    [
                        'AND',
                        ['bill' => [
                                'ids' => $this->getIDSes(self::$ID_COMBINATION_KO)
                        ]],
                        [
                            'OR',
                            ['no_vat_type' => POPDVBillLists::$U_1_3],
                            ['no_vat_type' => POPDVBillLists::$U_1_4]
                        ],
                        ['internal_vat' => $_vat_rate]
                    ],
                    [
                        'AND',
                        ['bill' => [
                                'ids' => $canceled_bill_ids
                        ]],
                        [
                            'OR',
                            ['no_vat_type' => POPDVBillLists::$U_1_3],
                            ['no_vat_type' => POPDVBillLists::$U_1_4]
                        ],
                        ['internal_vat' => $_vat_rate]
                    ]
                    

                ],
                'select' => "COALESCE(sum($select_value),0::numeric) as value",
            ];
        case '8b61': case '8b62': 
            $_vat_rate = (in_array($code, ['8b61']))?BillItem::$VAT_20:BillItem::$VAT_10;
            
            $canceled_bill_ids = $this->getIDSes(self::$ID_COMBINATION_CR);
            
            if (is_array($canceled_bill_ids))
            {
                $canceled_bill_ids = implode(',', $canceled_bill_ids);
            }
            
            $select_value = $this->selectWithCanceled('value');
            
            return [ 
                'Model' => 'BillItem',
                'model_filter' => [
                    'OR',
                    [
                        'AND',
                        ['bill' => [
                                'ids' => $this->getIDSes(self::$ID_COMBINATION_R_KO_KZ)
                        ]],
                        [
                            'OR',
                            ['no_vat_type' => POPDVBillLists::$U_1_3],
                            ['no_vat_type' => POPDVBillLists::$U_1_4]
                        ],
                        ['internal_vat' => $_vat_rate]
                    ],
                    [
                        'AND',
                        ['bill' => [
                                'ids' => $canceled_bill_ids
                        ]],
                        [
                            'OR',
                            ['no_vat_type' => POPDVBillLists::$U_1_3],
                            ['no_vat_type' => POPDVBillLists::$U_1_4]
                        ],
                        ['internal_vat' => $_vat_rate]
                    ]
                    
                ],
                'select' => "COALESCE(sum($select_value),0::numeric) as value",
            ];
        case '8b71': case '8b72': 
            $_vat_rate = (in_array($code, ['8b71']))?BillItem::$VAT_20:BillItem::$VAT_10;
            return [ 
                'Model' => 'BillItem',
                'model_filter' => [
                    'AND',
                    ['bill' => [
                            'ids' => $this->getIDSes(self::$ID_COMBINATION_A)
                    ]],
                    [
                        'OR',
                        ['no_vat_type' => POPDVBillLists::$U_1_3],
                        ['no_vat_type' => POPDVBillLists::$U_1_4]
                    ],
                    ['internal_vat' => $_vat_rate]
                ],
                'select' => 'COALESCE(sum(value),0::numeric) as value',
            ];
            
            
        case '8v1': case '8v2':
            $_no_vat_type = (in_array($code, ['8v1']))?POPDVBillLists::$U_0_3:POPDVBillLists::$U_0_4;
            
            $canceled_bill_ids = $this->getIDSes(self::$ID_COMBINATION_CR);
            if (is_array($canceled_bill_ids))
            {
                $canceled_bill_ids = implode(',', $canceled_bill_ids);
            }
            
            $select_value = $this->selectWithCanceled('value');
            
            return [ 
                'Model' => 'BillItem',
                'model_filter' => [
                    'OR',
                    [
                        'AND',
                        ['bill' => [
                                'ids' => $this->getIDSes(self::$ID_COMBINATION_R_KO_KZ)
                        ]],
                        [
                            'OR',
                            ['no_vat_type' => $_no_vat_type]
                        ]
                    ],
                    [
                        'AND',
                            ['bill' => [
                                    'ids' => $canceled_bill_ids
                            ]],
                            [
                                'OR',
                                ['no_vat_type' => $_no_vat_type],
                            ]
                    ]
                ],
                'select' => "COALESCE(sum($select_value),0::numeric) as value",
            ];

        case '8v4': 
            $canceled_bill_ids = $this->getIDSes(self::$ID_COMBINATION_CR);
            if (is_array($canceled_bill_ids))
            {
                $canceled_bill_ids = implode(',', $canceled_bill_ids);
            }
            
            $select_value = $this->selectWithCanceled('value');
            return [ 
                'Model' => 'BillItem',
                'model_filter' => [
                    'OR',
                    [
                        'AND',
                        ['bill' => [
                                'ids' => $this->getIDSes(self::$ID_COMBINATION_R_KO_KZ)
                        ]],
                        [
                            'OR',
                            ['no_vat_type' => POPDVBillLists::$U_0_3],
                            ['no_vat_type' => POPDVBillLists::$U_0_4]
                        ]
                    ],
                    [
                        'AND',
                        ['bill' => [
                                'ids' => $canceled_bill_ids
                        ]],
                        [
                            'OR',
                            ['no_vat_type' => POPDVBillLists::$U_0_3],
                            ['no_vat_type' => POPDVBillLists::$U_0_4]
                        ]
                    ]
                    
                ],
                'select' => "COALESCE(sum($select_value),0::numeric) as value",
            ];
        case '8g11': case '8g12':
        case '8g31': case '8g32':
            $_vat_rate = (in_array($code, ['8g11', '8g31']))?BillItem::$VAT_20:BillItem::$VAT_10;
            switch ($code)
            {
                case '8g11': case '8g12': $_bill_type = self::$ID_COMBINATION_R; break;
                case '8g31': case '8g32': $_bill_type = self::$ID_COMBINATION_KZ; break;
            }
            return [ 
                'Model' => 'BillItem',
                'model_filter' => [
                    'AND',
                    ['bill' => [
                            'ids' => $this->getIDSes($_bill_type)
                    ]],
                    [
                        'OR',
                        ['no_vat_type' => POPDVBillLists::$U_1_5]
                    ],
                    ['internal_vat' => $_vat_rate]
                ],
                'select' => 'COALESCE(sum(value),0::numeric) as value',
            ];
        case '8g41': case '8g42':
            $_vat_rate = (in_array($code, ['8g41']))?BillItem::$VAT_20:BillItem::$VAT_10;
            
            $_sum_of = 'value';
            $canceled_bill_ids = $this->getIDSes(self::$ID_COMBINATION_CR);
            
            if (is_array($canceled_bill_ids))
            {
                $canceled_bill_ids = implode(',', $canceled_bill_ids);
            }
            
            $select_value = " CASE WHEN bill_id in ($canceled_bill_ids) "
            . ' THEN'
            . "     -$_sum_of"
            . ' ELSE '
            . "     $_sum_of"
            . ' END';
            
            
            switch ($code)
            {
                case '8g11': case '8g12': $_bill_type = self::$ID_COMBINATION_R; break;
                case '8g31': case '8g32': $_bill_type = self::$ID_COMBINATION_KZ; break;
                case '8g41': case '8g42': $_bill_type = self::$ID_COMBINATION_KO; break;
            }
            return [ 
                'Model' => 'BillItem',
                'model_filter' => [
                    'OR',
                    [
                        'AND',
                        ['bill' => [
                                'ids' => $this->getIDSes(self::$ID_COMBINATION_KO)
                        ]],
                        [
                            'OR',
                            ['no_vat_type' => POPDVBillLists::$U_1_5]
                        ],
                        ['internal_vat' => $_vat_rate]
                    ],
                    [
                        'AND',
                        ['bill' => [
                                'ids' => $canceled_bill_ids
                        ]],
                        [
                            'OR',
                            ['no_vat_type' => POPDVBillLists::$U_1_5]
                        ],
                        ['internal_vat' => $_vat_rate]
                    ]
                ],
                'select' => "COALESCE(sum($select_value),0::numeric) as value",
            ];
        case '8g51': case '8g52':
            $_vat_rate = (in_array($code, ['8g51']))?BillItem::$VAT_20:BillItem::$VAT_10;
            $select_value = $this->selectWithCanceled('value');
            return [ 
                'Model' => 'BillItem',
                'model_filter' => [
                    'OR',
                    [
                        'AND',
                        ['bill' => [
                                'ids' => $this->getIDSes(self::$ID_COMBINATION_R_KO_KZ)
                        ]],
                        [
                            'OR',
                            ['no_vat_type' => POPDVBillLists::$U_1_5]
                        ],
                        ['internal_vat' => $_vat_rate]
                    ],
                    [
                        'AND',
                        ['bill' => [
                                'ids' => $this->getIDSes(self::$ID_COMBINATION_CR)
                        ]],
                        [
                            'OR',
                            ['no_vat_type' => POPDVBillLists::$U_1_5]
                        ],
                        ['internal_vat' => $_vat_rate]
                    ]
                    
                ],
                'select' => "COALESCE(sum($select_value),0::numeric) as value",
            ];
        case '8g61': case '8g62':
            $_vat_rate = (in_array($code, ['8g61']))?BillItem::$VAT_20:BillItem::$VAT_10;
            return [ 
                'Model' => 'BillItem',
                'alias' => $this->alias1,
                'model_filter' => [
                    'AND',
                    ['bill' => [
                            'ids' => $this->getIDSes(self::$ID_COMBINATION_A)
                    ]],
                    [
                        'OR',
                        ['no_vat_type' => POPDVBillLists::$U_1_5]
                    ],
                    ['internal_vat' => $_vat_rate]
                ],
                'select' => $this->selectWithAdvance('value', 'internal_vat', 'base_amount'),
            ];
        case '8d1': case '8d2': case '8d3':
            switch ($code)
            {
                case '8d1': $_no_vat_type = POPDVBillLists::$U_0_2; break;
                case '8d2': $_no_vat_type = POPDVBillLists::$U_0_1; break;
                case '8d3': $_no_vat_type = POPDVBillLists::$U_0_5; break;
            }
            
            $canceled_bill_ids = $this->getIDSes(self::$ID_COMBINATION_CR);
            if (is_array($canceled_bill_ids))
            {
                $canceled_bill_ids = implode(',', $canceled_bill_ids);
            }
            
            $select_value = $this->selectWithCanceled('value');
            return [ 
                'Model' => 'BillItem',
                'model_filter' => [
                    'OR',
                    [
                        'AND',
                        ['bill' => [
                                'ids' => $this->getIDSes(self::$ID_COMBINATION_R_KO_KZ)
                        ]],
                        [
                            'OR',
                            ['no_vat_type' => $_no_vat_type]
                        ]
                    ],
                    [
                        'AND',
                        ['bill' => [
                                'ids' => $canceled_bill_ids
                        ]],
                        [
                            'OR',
                            ['no_vat_type' => $_no_vat_type]
                        ]
                    ]
                    
                ],
                'select' => "COALESCE(sum($select_value),0::numeric) as value",
            ];
        case '8dj': 
            $canceled_bill_ids = $this->getIDSes(self::$ID_COMBINATION_CR);
            if (is_array($canceled_bill_ids))
            {
                $canceled_bill_ids = implode(',', $canceled_bill_ids);
            }
            
            $select_value = $this->selectWithCanceled('value');
            return [ 
                'Model' => 'BillItem',
                'model_filter' => [
                    'OR',
                    [
                        'AND',
                        ['bill' => [
                                'ids' => $this->getIDSes(self::$ID_COMBINATION_R_KO_KZ)
                        ]],
                        [
                            'OR',
                            ['no_vat_type' => POPDVBillLists::$U_0_1],
                            ['no_vat_type' => POPDVBillLists::$U_0_2],
                            ['no_vat_type' => POPDVBillLists::$U_0_3],
                            ['no_vat_type' => POPDVBillLists::$U_0_4],
                            ['no_vat_type' => POPDVBillLists::$U_0_5],
                            ['no_vat_type' => POPDVBillLists::$U_1_1],
                            ['no_vat_type' => POPDVBillLists::$U_1_2],
                            ['no_vat_type' => POPDVBillLists::$U_1_3],
                            ['no_vat_type' => POPDVBillLists::$U_1_4],
                            ['no_vat_type' => POPDVBillLists::$U_1_5]
                        ]
                    ],
                    [
                        'AND',
                        ['bill' => [
                                'ids' => $canceled_bill_ids
                        ]],
                        [
                            'OR',
                            ['no_vat_type' => POPDVBillLists::$U_0_1],
                            ['no_vat_type' => POPDVBillLists::$U_0_2],
                            ['no_vat_type' => POPDVBillLists::$U_0_3],
                            ['no_vat_type' => POPDVBillLists::$U_0_4],
                            ['no_vat_type' => POPDVBillLists::$U_0_5],
                            ['no_vat_type' => POPDVBillLists::$U_1_1],
                            ['no_vat_type' => POPDVBillLists::$U_1_2],
                            ['no_vat_type' => POPDVBillLists::$U_1_3],
                            ['no_vat_type' => POPDVBillLists::$U_1_4],
                            ['no_vat_type' => POPDVBillLists::$U_1_5],
                        ]
                    ]
                    
                ],
                'select' => "COALESCE(sum($select_value),0::numeric) as value",
            ];
//        case '8e1': throw new SIMAExceptionPOPDVCodeJustSum($code);
        case '8e1': 

            $select_value = $this->selectWithCanceled('value_vat');
            $canceled_bill_ids = $this->getIDSes(self::$ID_COMBINATION_CRA);
            return [ 
                'Model' => 'BillItem',
                'alias' => $this->alias1,
                'model_filter' => [
                    'OR',
                    [
                        'AND',
                        ['bill' => [
                                'ids' => $this->getIDSes(self::$ID_COMBINATION_R_A_KO_KZ)
                        ]],
                        [
                            'OR',
                            ['no_vat_type' => POPDVBillLists::$U_1_1],
                            ['no_vat_type' => POPDVBillLists::$U_1_2]
                        ],
                        ['vat_refusal' => true]
                    ],
                    [
                        'AND',
                        ['bill' => [
                                'ids' => $canceled_bill_ids
                        ]],
                        [
                            'OR',
                            ['no_vat_type' => POPDVBillLists::$U_1_1],
                            ['no_vat_type' => POPDVBillLists::$U_1_2]
                        ],
                        ['vat_refusal' => true]
                    ]
                    
                ],
                'select' => $this->selectWithAdvance($select_value, 'vat', 'vat_amount')
            ];
            
            
        case '8e2': 
            $select_value = $this->selectWithCanceled('value_internal_vat');
            $canceled_bill_ids = $this->getIDSes(self::$ID_COMBINATION_CRA);
            return [ 
                'Model' => 'BillItem',
                'alias' => $this->alias1,
                'model_filter' => [
                    'OR',
                    [
                        'AND',
                        ['bill' => [
                                'ids' => $this->getIDSes(self::$ID_COMBINATION_R_A_KO_KZ)
                        ]],
                        [
                            'OR',
                            ['no_vat_type' => POPDVBillLists::$U_1_3],
                            ['no_vat_type' => POPDVBillLists::$U_1_4],
                            ['no_vat_type' => POPDVBillLists::$U_1_5]
                        ],
                        ['vat_refusal' => true]
                    ],
                    [
                        'AND',
                        ['bill' => [
                                'ids' => $canceled_bill_ids
                        ]],
                        [
                            'OR',
                            ['no_vat_type' => POPDVBillLists::$U_1_3],
                            ['no_vat_type' => POPDVBillLists::$U_1_4],
                            ['no_vat_type' => POPDVBillLists::$U_1_5]
                        ],
                        ['vat_refusal' => true]
                    ]

                ],
                'select' => $this->selectWithAdvance($select_value, 'internal_vat'),
            ];

            
        case '9': 
            
            throw new SIMAExceptionPOPDVCodeJustSum($code);;
        case 'xxx': return [ 
            
        ];
        case 'xxx': return [ 
            
        ];
        case 'xxx': return [ 
            
        ];
        case 'xxx': return [ 
            
        ];
        default: throw new SIMAExceptionPOPDVCodeJustSum($code);
        case 'xxx': throw new SIMAExceptionPOPDVCodeJustSum($code);
        }
    }
    
    private function calcParams()
    {
        $this->calcIDSes();
        
        foreach ($this->params as $code => $value)
        {
            try
            {
                $db_crit = new SIMADbCriteria($this->getDBCriteriaParams($code));
                $this->params[$code] = round(BillItem::model()->find($db_crit)->value);
            } 
            catch (SIMAExceptionPOPDVCodeJustSum $ex)
            {
                $this->params[$code] = null;
            }
            catch (SIMAException $ex) 
            {
                error_log(__METHOD__.' - exception za kod: '.$ex->getMessage());
            }
        }
        
        foreach (POPDVLists::SUM_PARAMS as $sum_code => $sum_parts_array)
        {
            if ($sum_parts_array === POPDVLists::CUSTOM_SUM)
            {
                continue;
            }
            $new_sum = 0;
            foreach ($sum_parts_array as $sum_part)
            {
                if (!is_null($this->params[$sum_part]))
                {
                    $new_sum += round($this->params[$sum_part]);
                }
            }
            
            if (!is_null($this->params[$sum_code]))
            {
                //dinara tolerancije
                $rsd_diff_accaptable = 5;
                if (!SIMAMisc::areEqual($this->params[$sum_code], $new_sum, $rsd_diff_accaptable))
                {
                    $calc = $this->params[$sum_code];
                    $diff = $calc - $new_sum;
                    Yii::app()->raiseNote("Na poziciji $sum_code se izracunata suma ne poklapa sa sabranom. "
                            . "Obracunata: $calc - Sabrana: $new_sum - diff: $diff" );
                }
            }
        }
        //ne prosledjuje se suma dalje
        foreach (POPDVLists::SUM_PARAMS as $sum_code => $sum_parts_array)
        {
            $this->params[$sum_code] = 0.0;
        }
    }

    /**
     * implementacija ArrayAccess
     */

    public function offsetExists ( $offset ) : bool
    {
        return isset($this->params[$offset]);
    }
    public function offsetGet ( $offset )
    {
        return $this->params[$offset];
    }
    public function offsetSet ( $offset , $value ) : void
    {
        if (isset($this->params[$offset]))
        {
            //trenutno ne postavljamo 
//            $this->params[$offset] = $value;
        }
        else
        {
            throw new SIMAException("kod $offset ne postoji u POPDV prijavi");
        }
    }
    public function offsetUnset ( $offset ) : void
    {
        $this->offsetSet($offset, 0);
    }
}