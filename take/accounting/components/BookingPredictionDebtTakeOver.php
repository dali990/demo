<?php

/**
 * Staticka klasa u kojoj se predvidjaju sva proknjizavanja
 */
class BookingPredictionDebtTakeOver
{
    public static function Booking($document)
    {
        $booking_transactions = [];
        $file = $document->file;
        $debt_take_over = $document->file->debt_take_over;
        $_year = $debt_take_over->year;

        $_statement_comment = $file->DisplayName . ' - ' . $debt_take_over->debt_value.' - '.$debt_take_over->sign_date;
        
        if ($debt_take_over->isSystemCompanyCreditor)
        {
            $debtor_account     = $debt_take_over->debtor->getCustomerAccount($_year);
            $tr1 = new AccountTransaction();
            $tr1->account_id = $debtor_account->id;
            $tr1->account_document_id = $document->id;
            $tr1->comment = $_statement_comment;
            $tr1->credit = $debt_take_over->debt_value;
            $booking_transactions[] = $tr1;
            
            $takes_over_account = $debt_take_over->takes_over->getCustomerAccount($_year);
            $tr2 = new AccountTransaction();
            $tr2->account_id = $takes_over_account->id;
            $tr2->account_document_id = $document->id;
            $tr2->comment = $_statement_comment;
            $tr2->debit = $debt_take_over->debt_value;
            $booking_transactions[] = $tr2;
            
        }
        elseif ($debt_take_over->isSystemCompanyDebtor)
        {
            
            $creditor_account   = $debt_take_over->creditor->getSupplierAccount($_year);
            $tr1 = new AccountTransaction();
            $tr1->account_id = $creditor_account->id;
            $tr1->account_document_id = $document->id;
            $tr1->comment = $_statement_comment;
            $tr1->debit = $debt_take_over->debt_value;
            $booking_transactions[] = $tr1;
            
            $takes_over_account = $debt_take_over->takes_over->getSupplierAccount($_year);
            $tr2 = new AccountTransaction();
            $tr2->account_id = $takes_over_account->id;
            $tr2->account_document_id = $document->id;
            $tr2->comment = $_statement_comment;
            $tr2->credit = $debt_take_over->debt_value;
            $booking_transactions[] = $tr2;
        }
        elseif ($debt_take_over->isSystemCompanyTakesOver)
        {
            $debtor_account     = $debt_take_over->debtor->getCustomerAccount($_year);
            $tr1 = new AccountTransaction();
            $tr1->account_id = $debtor_account->id;
            $tr1->account_document_id = $document->id;
            $tr1->comment = $_statement_comment;
            $tr1->debit = $debt_take_over->debt_value;
            $booking_transactions[] = $tr1;
            
            $creditor_account   = $debt_take_over->creditor->getSupplierAccount($_year);
            $tr2 = new AccountTransaction();
            $tr2->account_id = $creditor_account->id;
            $tr2->account_document_id = $document->id;
            $tr2->comment = $_statement_comment;
            $tr2->credit = $debt_take_over->debt_value;
            $booking_transactions[] = $tr2;
        }
        else
        {
            error_log(__METHOD__.' SISTEMSKA KOMPANIJA NIJE NISTA!!!');
        }
        
//        return SIMAMisc::joinArrays($booking_transactions);
        return $booking_transactions;
    }

}