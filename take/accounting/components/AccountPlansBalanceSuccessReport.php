<?php

class AccountPlansBalanceSuccessReport extends AccountPlansBalanceReport
{
    public function getReportName() : string
    {
        return 'Bilans uspeha';
    }
    
    protected function getAOPCodes() : array
    {
        return [
            '1001' => [
                'plus' => ['60', '61', '64', '65']
            ],
            '1002' => '60',
            '1003' => '600',
            '1004' => '601',
            '1005' => '602',
            '1006' => '603',
            '1007' => '604',
            '1008' => '605',
            '1009' => '61',
            '1010' => '610',
            '1011' => '611',
            '1012' => '612',
            '1013' => '613',
            '1014' => '614',
            '1015' => '615',
            '1016' => '64',
            '1017' => '65',
            '1018' => [
                'plus' => ['50', '51', '52', '53', '54', '55', '62', '63']
            ],
            '1019' => '50',
            '1020' => '62',
            '1021' => '630',
            '1022' => '631',
            '1023' => [
                'plus' => ['51'],
                'minus' => ['513']
            ],
            '1024' => '513',
            '1025' => '52',
            '1026' => '53',
            '1027' => '540',
            '1028' => [
                'plus' => ['541', '542', '543', '544', '545', '546', '547', '548', '549']
            ],
            '1029' => '55',
            '1030' => [
                'type' => 'AOP',
                'plus' => ['1001'],
                'minus' => ['1018'],
                'only_positive' => true
            ],
            '1031' => [
                'type' => 'AOP',
                'plus' => ['1018'],
                'minus' => ['1001'],
                'only_positive' => true
            ],
            '1032' => '66',
            '1033' => [
                'plus' => ['66'],
                'minus' => ['662', '663', '664']
            ],
            '1034' => '660',
            '1035' => '661',
            '1036' => '665',
            '1037' => '669',
            '1038' => '662',
            '1039' => [
                'plus' => ['663', '664']
            ],
            '1040' => '56',
            '1041' => [
                'plus' => ['56'],
                'minus' => ['562', '563', '564']
            ],
            '1042' => '560',
            '1043' => '561',
            '1044' => '565',
            '1045' => [
                'plus' => ['566', '569']
            ],
            '1046' => '562',
            '1047' => [
                'plus' => ['563', '564']
            ],
            '1048' => [
                'type' => 'AOP',
                'plus' => ['1032'],
                'minus' => ['1040'],
                'only_positive' => true
            ],
            '1049' => [
                'type' => 'AOP',
                'plus' => ['1040'],
                'minus' => ['1032'],
                'only_positive' => true
            ],
            '1050' => [
                'plus' => ['683', '685']
            ],
            '1051' => [
                'plus' => ['583', '585']
            ],
            '1052' => [
                'plus' => ['67', '68'],
                'minus' => ['683', '685']
            ],
            '1053' => [
                'plus' => ['57', '58'],
                'minus' => ['583', '585']
            ],
            '1054' => [
                'double_check' => [
                    function() {
                        //ako je prva transakcija u potraznoj strani onda ide na 1054
                        $curr_account = Account::getFromParam('accounting.codes.profit_calcs.profit', $this->curr_year)->getLeafAccount()->withDCCurrents($this->curr_year->id);
                        $prev_account = Account::getFromParam('accounting.codes.profit_calcs.profit', $this->prev_year)->getLeafAccount()->withDCCurrents($this->prev_year->id);
                        
                        $aop_5 = 0;
                        if (!empty($curr_account) && !empty($curr_account->account_transactions))
                        {
                            $first_transaction = $curr_account->account_transactions[0];
                            if ($first_transaction->credit > 0)
                            {
                                $aop_5 = $first_transaction->credit;
                            }
                        }
                        
                        $aop_6 = 0;
                        if (!empty($prev_account) && !empty($prev_account->account_transactions))
                        {
                            $first_transaction = $prev_account->account_transactions[0];
                            if ($first_transaction->credit > 0)
                            {
                                $aop_6 = $first_transaction->credit;
                            }
                        }
                        
                        return [
                            'aop-1054-5' => $aop_5,
                            'aop-1054-6' => $aop_6
                        ];
                    },
                    [
                        'type' => 'AOP',
                        'plus' => ['1030', '1048', '1050', '1052'],
                        'minus' => ['1031', '1049', '1051', '1053'],
                        'only_positive' => true
                    ]
                ]
            ],
            '1055' => [
                'double_check' => [
                    function() {
                        //ako je prva transakcija u dugovnoj strani onda ide na 1055
                        $curr_account = Account::getFromParam('accounting.codes.profit_calcs.loss', $this->curr_year)->getLeafAccount()->withDCCurrents($this->curr_year->id);
                        $prev_account = Account::getFromParam('accounting.codes.profit_calcs.loss', $this->prev_year)->getLeafAccount()->withDCCurrents($this->prev_year->id);
                        
                        $aop_5 = 0;
                        if (!empty($curr_account) && !empty($curr_account->account_transactions))
                        {
                            $first_transaction = $curr_account->account_transactions[0];
                            if ($first_transaction->debit > 0)
                            {
                                $aop_5 = $first_transaction->debit;
                            }
                        }
                        
                        $aop_6 = 0;
                        if (!empty($prev_account) && !empty($prev_account->account_transactions))
                        {
                            $first_transaction = $prev_account->account_transactions[0];
                            if ($first_transaction->debit > 0)
                            {
                                $aop_6 = $first_transaction->debit;
                            }
                        }
                        
                        return [
                            'aop-1055-5' => $aop_5,
                            'aop-1055-6' => $aop_6
                        ];
                    },
                    [
                        'type' => 'AOP',
                        'plus' => ['1031', '1049', '1051', '1053'],
                        'minus' => ['1030', '1048', '1050', '1052'],
                        'only_positive' => true
                    ]
                ]
            ],
            '1056' => [
                'plus' => ['69'],
                'minus' => ['699']
            ],
            '1057' => [
                'plus' => ['59'],
                'minus' => ['599']
            ],
            '1058' => [
                'type' => 'AOP',
                'plus' => ['1054', '1056'],
                'minus' => ['1055', '1057'],
                'only_positive' => true
            ],
            '1059' => [
                'type' => 'AOP',
                'plus' => ['1055', '1057'],
                'minus' => ['1054', '1056'],
                'only_positive' => true
            ],
            '1060' => function() {
                $curr_account = $this->getAccountForYear('721000', $this->curr_year);
                $prev_account = $this->getAccountForYear('721000', $this->prev_year);
                
                $aop_5 = 0;
                if (!empty($curr_account) && !empty($curr_account->account_transactions))
                {
                    $first_transaction = $curr_account->account_transactions[0];
                    if ($first_transaction->debit > 0)
                    {
                        $aop_5 = $first_transaction->debit;
                    }
                    else if ($first_transaction->credit > 0)
                    {
                        $aop_5 = $first_transaction->credit;
                    }
                }
                
                $aop_6 = 0;
                if (!empty($prev_account) && !empty($prev_account->account_transactions))
                {
                    $first_transaction = $prev_account->account_transactions[0];
                    if ($first_transaction->debit > 0)
                    {
                        $aop_6 = $first_transaction->debit;
                    }
                    else if ($first_transaction->credit > 0)
                    {
                        $aop_6 = $first_transaction->credit;
                    }
                }

                return [
                    'aop-1060-5' => $aop_5,
                    'aop-1060-6' => $aop_6
                ];
            },
            '1061' => function() {
                $curr_account = $this->getAccountForYear('7220', $this->curr_year);
                $prev_account = $this->getAccountForYear('7220', $this->prev_year);

                return [
                    'aop-1061-5' => !empty($curr_account) ? $curr_account->credit_current : 0,
                    'aop-1061-6' => !empty($prev_account) ? $prev_account->credit_current : 0
                ];
            },
            '1062' => function() {
                $curr_account = $this->getAccountForYear('7221', $this->curr_year);
                $prev_account = $this->getAccountForYear('7221', $this->prev_year);

                return [
                    'aop-1062-5' => !empty($curr_account) ? $curr_account->debit_current : 0,
                    'aop-1062-6' => !empty($prev_account) ? $prev_account->debit_current : 0
                ];
            },
            '1063' => function() {
                $curr_account = $this->getAccountForYear('723', $this->curr_year);
                $prev_account = $this->getAccountForYear('723', $this->prev_year);

                return [
                    'aop-1063-5' => !empty($curr_account) ? $curr_account->debit_current : 0,
                    'aop-1063-6' => !empty($prev_account) ? $prev_account->debit_current : 0
                ];
            },
            '1064' => [
                'double_check' => [
                    function() {
                        //ako je prva transakcija u potraznoj strani onda ide na 1064
                        $curr_account = $this->getAccountForYear('724000', $this->curr_year);
                        $prev_account = $this->getAccountForYear('724000', $this->prev_year);
                        
                        $aop_5 = 0;
                        if (!empty($curr_account) && !empty($curr_account->account_transactions))
                        {
                            $first_transaction = $curr_account->account_transactions[0];
                            if ($first_transaction->credit > 0)
                            {
                                $aop_5 = $first_transaction->credit;
                            }
                        }
                        
                        $aop_6 = 0;
                        if (!empty($prev_account) && !empty($prev_account->account_transactions))
                        {
                            $first_transaction = $prev_account->account_transactions[0];
                            if ($first_transaction->credit > 0)
                            {
                                $aop_6 = $first_transaction->credit;
                            }
                        }
                        
                        return [
                            'aop-1064-5' => $aop_5,
                            'aop-1064-6' => $aop_6
                        ];
                    },
                    [
                        'type' => 'AOP',
                        'plus' => ['1058', '1062'],
                        'minus' => ['1059', '1060', '1061', '1063'],
                        'only_positive' => true
                    ]
                ]
            ],
            '1065' => [
                'double_check' => [
                    function() {
                        //ako je prva transakcija u dugovnoj strani onda ide na 1065
                        $curr_account = $this->getAccountForYear('724100', $this->curr_year);
                        $prev_account = $this->getAccountForYear('724100', $this->prev_year);
                        
                        $aop_5 = 0;
                        if (!empty($curr_account) && !empty($curr_account->account_transactions))
                        {
                            $first_transaction = $curr_account->account_transactions[0];
                            if ($first_transaction->debit > 0)
                            {
                                $aop_5 = $first_transaction->debit;
                            }
                        }
                        
                        $aop_6 = 0;
                        if (!empty($prev_account) && !empty($prev_account->account_transactions))
                        {
                            $first_transaction = $prev_account->account_transactions[0];
                            if ($first_transaction->debit > 0)
                            {
                                $aop_6 = $first_transaction->debit;
                            }
                        }
                        
                        return [
                            'aop-1065-5' => $aop_5,
                            'aop-1065-6' => $aop_6
                        ];
                    },
                    [
                        'type' => 'AOP',
                        'plus' => ['1059', '1060', '1061', '1063'],
                        'minus' => ['1058', '1062'],
                        'only_positive' => true
                    ]
                ]
            ],
            '1066' => '',
            '1067' => '',
            '1068' => '',
            '1069' => '',
            '1070' => '',
            '1071' => ''
        ];
    }

    protected function getXMLValueFromAccount(string $aop_code, string $account_code) : array
    {
        return [
            "aop-$aop_code-5" => $this->getXMLValueForColumn($account_code, 5),
            "aop-$aop_code-6" => $this->getXMLValueForColumn($account_code, 6)
        ];
    }
    
    protected function getXMLValueFromAccounts(string $aop_code, array $account_codes, array $aop_params) : array
    {
        $aop_5 = 0;
        $aop_6 = 0;
        foreach ($account_codes as $account_code) 
        {
            $aop_5 += $this->getXMLValueForColumn($account_code, 5);
            $aop_6 += $this->getXMLValueForColumn($account_code, 6);
        }
        
        return [
            "aop-$aop_code-5" => $aop_5,
            "aop-$aop_code-6" => $aop_6
        ];
    }
    
    protected function getXMLValueFromAOPs(string $aop_code, array $aops, array $aop_params) : array
    {
        $aop_5 = 0;
        $aop_6 = 0;
        foreach ($aops as $aop) 
        {
            $aop_5 += $this->xml_values["aop-$aop-5"];
            $aop_6 += $this->xml_values["aop-$aop-6"];
        }
        
        return [
            "aop-$aop_code-5" => $aop_5,
            "aop-$aop_code-6" => $aop_6
        ];
    }
    
    private function getXMLValueForColumn(string $account_code, int $column)
    {
        if ($column === 5)
        {
            $account = $this->getAccountForYear($account_code, $this->curr_year);
        }
        else
        {
            $account = $this->getAccountForYear($account_code, $this->prev_year);
        }
        
        if (empty($account))
        {
            return 0;
        }
        
        $code = (string)$account->code;
        $code_first_char = $code[0];

        if ($code_first_char === '5')
        {
            return $account->debit_current;
        }
        else if ($code_first_char === '6')
        {
            return $account->credit_current;
        }
        
        return 0;
    }
}
