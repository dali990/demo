<?php

/**
 * Staticka klasa u kojoj se predvidjaju sva proknjizavanja
 */
class BookingPredictionWarehouseRetail
{
    public static function Booking($document)
    {
        $booking_transactions = [];
        $file = $document->file;
        $_wt = $document->file->warehouse_calculation;
        $warehouse_from = $_wt->warehouse_from;
        $warehouse_to = $_wt->warehouse_to;
        
        $_statement_comment = $_wt->name;
        
        throw new SIMAException(' NIJE IMPLEMENTIRANO KNJIZENJE ZA TRGOVINU');
        
//        if ($warehouse_from->isWarehouseStorageProducts && $warehouse_to->isWarehouseStorageRetail)
//        {
//            return self::BookingCalculationFromProducts($document, $_wt, $_statement_comment);
//        }
//        elseif ($warehouse_from->isWarehouseStorageWholesail && $warehouse_to->isWarehouseStorageRetail)
//        {
//            return self::BookingCalculationFromWholesail($document, $_wt, $_statement_comment);
//        }
//        elseif ($warehouse_from->isWarehouseStorageRetail && $warehouse_to->isWarehousePartnerOut)
//        {
//            //ovo trenutno ne knjizimo, nego podrazumevamo
//            return self::BookingFiscalBill($document, $_wt, $_statement_comment);
//        }
//        else
//        {
//            Yii::app()->raiseNote('dokument se ne knjizi samostalno, nego samo kao ceo nalog');
//        }
        
        return [];
//        return SIMAMisc::joinArrays($booking_transactions);
    }

    
//    private static function BookingCalculationFromWholesail(AccountDocument $document, WarehouseCalculation $calc, $_statement_comment)
//    {
//        $booking_transactions = [];
//        //pravljenje stava za svaki magacin
//
//        $warehouse_from = $calc->warehouse_from;
//        $warehouse_to   = $calc->warehouse_to;
//        $year = $document->account_order->year;
//        
//        /**
//         * Kalkulacija
//         * 1340 |       
//         *      | 1320
//         *      | 1344
//         *      | 1349
//         * 
//         *      | 1349
//         * 1349 | 
//         */
//        foreach ($calc->warehouse_transfer_items as $item)
//        {
//            $_avg = $warehouse_from->materialAvaragePrice($item->warehouse_material,$calc->transfer_time,true,$calc->id);
//            $_planned = $warehouse_from->materialPrice($item->warehouse_material,$calc,true);
//            
//            error_log(__METHOD__.' $_avg:'. $_avg);
//            error_log(__METHOD__.' $_planned:'. $_planned);
//            
//            $item->value_per_unit = $_avg;
//            $item->corr_value_per_unit = $_planned - $_avg;
//            $item->value_per_unit_out = $warehouse_to->materialPrice($item->warehouse_material,$calc,true);
//            $item->save();
//        }
//        $value = $calc->sum_value;
//        $value_out = $calc->sum_value_out;
//        $vat = $calc->sum_value_vat_out;
//        $corr_value = $calc->sum_corr_value;
//        
//        error_log(__METHOD__.' $value:'. $value);
//        error_log(__METHOD__.' $value_out:'. $value_out);
//        error_log(__METHOD__.' $vat:'. $vat);
//        error_log(__METHOD__.' $corr_value:'. $corr_value);
//        
//        //1340
//        $_retail_storage    = Account::getFromParam('accounting.codes.warehouse.retail_storage',$year)->getLeafAccount($warehouse_to);
//        //1344
//        $_retail_storage_correction    = Account::getFromParam('accounting.codes.warehouse.retail_storage_correction',$year)->getLeafAccount($warehouse_to);
//        //1349
//        $_retail_storage_vat    = Account::getFromParam('accounting.codes.warehouse.retail_storage_vat',$year)->getLeafAccount($warehouse_to);
//        //1320
//        $_finished_products    = Account::getFromParam('accounting.codes.warehouse.wholesail_storage',$year)->getLeafAccount($warehouse_from);
//        //1329
//        $_finished_products_correction    = Account::getFromParam('accounting.codes.warehouse.wholesail_storage_correction',$year)->getLeafAccount($warehouse_from);
//        //5010
//        $_finished_product_expensess    = Account::getFromParam('accounting.codes.warehouse.goods_initial_value',$year)->getLeafAccount($warehouse_from);
////        //902
////        $_excange_expences    = Account::getFromParam('accounting.codes.nine.excange_expences',$year)->getLeafAccount($warehouse_from);
//        //6040
//        $_income_account    = Account::getFromParam('accounting.codes.warehouse.income_from_goods',$year)->getLeafAccount($warehouse_to);
//        //4760
//        $_retail_vat    = Account::getFromParam('accounting.codes.vat.retail_outcome',$year)->getLeafAccount($warehouse_to);
//        
//        $tr1 = new AccountTransaction();
//        $tr1->account_id = $_retail_storage->id;
//        $tr1->account_document_id = $document->id;
//        $tr1->comment = $_statement_comment;
//        $tr1->debit = $value_out + $vat;
//        $booking_transactions[] = $tr1;
//        
//        $tr2 = new AccountTransaction();
//        $tr2->account_id = $_finished_products->id;
//        $tr2->account_document_id = $document->id;
//        $tr2->comment = $_statement_comment;
//        $tr2->credit = $value + $corr_value;
//        $booking_transactions[] = $tr2;
//        
//        $tr3 = new AccountTransaction();
//        $tr3->account_id = $_retail_storage_vat->id;
//        $tr3->account_document_id = $document->id;
//        $tr3->comment = $_statement_comment;
//        $tr3->credit = $vat;
//        $booking_transactions[] = $tr3;
//        
//        $diff = $value_out - ($value + $corr_value);
//        if (abs($diff)>0)
//        {
//            $tr4 = new AccountTransaction();
//            $tr4->account_id = $_retail_storage_correction->id;
//            $tr4->account_document_id = $document->id;
//            $tr4->comment = $_statement_comment;
//            $tr4->credit = $diff;
//            $booking_transactions[] = $tr4;
//        }
//        
//        if (abs($corr_value)>0)
//        {
//            $tr3 = new AccountTransaction();
//            $tr3->account_id = $_retail_storage_correction->id;
//            $tr3->account_document_id = $document->id;
//            $tr3->comment = $_statement_comment;
//            $tr3->credit = $corr_value;
//            $booking_transactions[] = $tr3;
//            
//            $tr4 = new AccountTransaction();
//            $tr4->account_id = $_finished_products_correction->id;
//            $tr4->account_document_id = $document->id;
//            $tr4->comment = $_statement_comment;
//            $tr4->debit = $corr_value;
//            $booking_transactions[] = $tr4;
//        }
//
//        
//        /**
//         * Fiskalni racun
//         *      | 9120
//         * 6140 |
//         * 9800 |
//         * 9122 |
//         *      | 4760
//         * 9129 |
//         * 9020 |
//         *      | 9800
//         */
//        $_statement_comment2 = $_statement_comment.' FISC';
//        
//        $trF1 = new AccountTransaction();
//        $trF1->account_id = $_retail_storage->id;
//        $trF1->account_document_id = $document->id;
//        $trF1->comment = $_statement_comment2;
//        $trF1->credit = $value_out + $vat;
//        $booking_transactions[] = $trF1;
// 
//        $trF2 = new AccountTransaction();
//        $trF2->account_id = $_income_account->id;
//        $trF2->account_document_id = $document->id;
//        $trF2->comment = $_statement_comment2;
//        $trF2->debit = $vat;
//        $booking_transactions[] = $trF2;
//          
//        //9800
//        $trF3 = new AccountTransaction();
//        $trF3->account_id = $_finished_product_expensess->id;
//        $trF3->account_document_id = $document->id;
//        $trF3->comment = $_statement_comment2;
//        $trF3->debit = $value;
//        $booking_transactions[] = $trF3;
//        
//        $trF4 = new AccountTransaction();
//        $trF4->account_id = $_retail_storage_vat->id;
//        $trF4->account_document_id = $document->id;
//        $trF4->comment = $_statement_comment2;
//        $trF4->debit = $vat;
//        $booking_transactions[] = $trF4;
//        
//        $trF5 = new AccountTransaction();
//        $trF5->account_id = $_retail_vat->id;
//        $trF5->account_document_id = $document->id;
//        $trF5->comment = $_statement_comment2;
//        $trF5->credit = $vat;
//        $booking_transactions[] = $trF5;
//        
//        $diff2 = $value_out - $value;
//        if (abs($diff2)>0)
//        {
//            $tr4 = new AccountTransaction();
//            $tr4->account_id = $_retail_storage_correction->id;
//            $tr4->account_document_id = $document->id;
//            $tr4->comment = $_statement_comment2;
//            $tr4->debit = $diff2;
//            $booking_transactions[] = $tr4;
//        }
//        
//        return $booking_transactions;
//    }
    
//    private static function BookingCalculationFromProducts(AccountDocument $document, WarehouseCalculation $calc, $_statement_comment)
//    {
//        $booking_transactions = [];
//        //pravljenje stava za svaki magacin
//
//        $warehouse_from = $calc->warehouse_from;
//        $warehouse_to   = $calc->warehouse_to;
//        $year = $document->account_order->year;
//        
//        /**
//         * Kalkulacija
//         * 9120 |       
//         *      | 9600
//         *      | 9122
//         *      | 9129
//         * 
//         *      | 9129
//         * 9690 | 
//         */
//        foreach ($calc->warehouse_transfer_items as $item)
//        {
//            $_avg = $warehouse_from->materialAvaragePrice($item->warehouse_material,$calc->transfer_time,true,$calc->id);
//            $cost_price = Yii::app()->configManager->get('accounting.warehouse.finished_products_evidence_type');;
//            if ($cost_price == 'COST_PRICE')//vodi se po ceni kostanja
//            {
//                $_planned = $_avg;
//            }
//            else
//            {
//                $_planned = $warehouse_from->materialPrice($item->warehouse_material,$calc,true);
//            }
//            
//            
//            $item->value_per_unit = $_avg;
//            $item->corr_value = $_planned - $_avg;
//            $item->value_per_unit_out = $warehouse_to->materialPrice($item->warehouse_material,$calc,true);
//            $item->save();
//        }
//        $value = $calc->sum_value;
//        $value_out = $calc->sum_value_out;
//        $vat = $calc->sum_value_vat_out;
//        $corr_value = $calc->sum_corr_value;
//        
//        
//        
//        //9120
//        $_retail_storage    = Account::getFromParam('accounting.codes.nine.retail_storage',$year)->getLeafAccount($warehouse_to);
//        //9129
//        $_retail_storage_correction    = Account::getFromParam('accounting.codes.nine.retail_storage_correction',$year)->getLeafAccount($warehouse_to);
//        //9122
//        $_retail_storage_vat    = Account::getFromParam('accounting.codes.nine.retail_storage_vat',$year)->getLeafAccount($warehouse_to);
//        //9600
//        $_finished_products    = Account::getFromParam('accounting.codes.nine.finished_products',$year)->getLeafAccount($warehouse_from);
//        //9690
//        $_finished_products_correction    = Account::getFromParam('accounting.codes.nine.finished_products_correction',$year)->getLeafAccount($warehouse_from);
//        //9800
//        $_finished_product_expensess    = Account::getFromParam('accounting.codes.nine.finished_product_expensess',$year)->getLeafAccount($warehouse_from);
//        //902
//        $_excange_expences    = Account::getFromParam('accounting.codes.nine.excange_expences',$year)->getLeafAccount($warehouse_from);
//        //6140
//        $_income_account    = Account::getFromParam('accounting.codes.warehouse.income_from_products_and_services',$year)->getLeafAccount($warehouse_to);
//        //4760
//        $_retail_vat    = Account::getFromParam('accounting.codes.vat.retail_outcome',$year)->getLeafAccount($warehouse_to);
//        
//        
//        
//        $tr1 = new AccountTransaction();
//        $tr1->account_id = $_retail_storage->id;
//        $tr1->account_document_id = $document->id;
//        $tr1->comment = $_statement_comment;
//        $tr1->debit = $value_out + $vat;
//        $booking_transactions[] = $tr1;
//        
//        $tr2 = new AccountTransaction();
//        $tr2->account_id = $_finished_products->id;
//        $tr2->account_document_id = $document->id;
//        $tr2->comment = $_statement_comment;
//        $tr2->credit = $value + $corr_value;
//        $booking_transactions[] = $tr2;
//        
//        $tr3 = new AccountTransaction();
//        $tr3->account_id = $_retail_storage_vat->id;
//        $tr3->account_document_id = $document->id;
//        $tr3->comment = $_statement_comment;
//        $tr3->credit = $vat;
//        $booking_transactions[] = $tr3;
//        
//        $diff = $value_out - ($value - $corr_value);
//        if (abs($diff)>0)
//        {
//            $tr4 = new AccountTransaction();
//            $tr4->account_id = $_retail_storage_correction->id;
//            $tr4->account_document_id = $document->id;
//            $tr4->comment = $_statement_comment;
//            $tr4->credit = $diff;
//            $booking_transactions[] = $tr4;
//        }
//        
//        if (abs($corr_value)>0)
//        {
//            $tr3 = new AccountTransaction();
//            $tr3->account_id = $_retail_storage_correction->id;
//            $tr3->account_document_id = $document->id;
//            $tr3->comment = $_statement_comment;
//            $tr3->credit = $corr_value;
//            $booking_transactions[] = $tr3;
//            
//            $tr4 = new AccountTransaction();
//            $tr4->account_id = $_finished_products_correction->id;
//            $tr4->account_document_id = $document->id;
//            $tr4->comment = $_statement_comment;
//            $tr4->debit = $corr_value;
//            $booking_transactions[] = $tr4;
//        }
//
//        
//        /**
//         * Fiskalni racun
//         *      | 9120
//         * 6140 |
//         * 9800 |
//         * 9122 |
//         *      | 4760
//         * 9129 |
//         * 9020 |
//         *      | 9800
//         */
//        $_statement_comment2 = $_statement_comment.' FISC';
//        
//        $trF1 = new AccountTransaction();
//        $trF1->account_id = $_retail_storage->id;
//        $trF1->account_document_id = $document->id;
//        $trF1->comment = $_statement_comment2;
//        $trF1->credit = $value_out + $vat;
//        $booking_transactions[] = $trF1;
// 
//        $trF2 = new AccountTransaction();
//        $trF2->account_id = $_income_account->id;
//        $trF2->account_document_id = $document->id;
//        $trF2->comment = $_statement_comment2;
//        $trF2->debit = $vat;
//        $booking_transactions[] = $trF2;
//          
//        //9800
//        $trF3 = new AccountTransaction();
//        $trF3->account_id = $_finished_product_expensess->id;
//        $trF3->account_document_id = $document->id;
//        $trF3->comment = $_statement_comment2;
//        $trF3->debit = $value;
//        $booking_transactions[] = $trF3;
//        
//        $trF4 = new AccountTransaction();
//        $trF4->account_id = $_retail_storage_vat->id;
//        $trF4->account_document_id = $document->id;
//        $trF4->comment = $_statement_comment2;
//        $trF4->debit = $vat;
//        $booking_transactions[] = $trF4;
//        
//        $trF5 = new AccountTransaction();
//        $trF5->account_id = $_retail_vat->id;
//        $trF5->account_document_id = $document->id;
//        $trF5->comment = $_statement_comment2;
//        $trF5->credit = $vat;
//        $booking_transactions[] = $trF5;
//        
//        $diff2 = $value_out - $value;
//        if (abs($diff2)>0)
//        {
//            $tr4 = new AccountTransaction();
//            $tr4->account_id = $_retail_storage_correction->id;
//            $tr4->account_document_id = $document->id;
//            $tr4->comment = $_statement_comment2;
//            $tr4->debit = $diff2;
//            $booking_transactions[] = $tr4;
//        }
//        
//        $trF6 = new AccountTransaction();
//        $trF6->account_id = $_excange_expences->id;
//        $trF6->account_document_id = $document->id;
//        $trF6->comment = $_statement_comment2;
//        $trF6->debit = $value_out;
//        $booking_transactions[] = $trF6;
//        
//        //9800
//        $trF7 = new AccountTransaction();
//        $trF7->account_id = $_finished_product_expensess->id;
//        $trF7->account_document_id = $document->id;
//        $trF7->comment = $_statement_comment2;
//        $trF7->credit = $value_out;
//        $booking_transactions[] = $trF7;
//        
//        return $booking_transactions;
//    }
    
}