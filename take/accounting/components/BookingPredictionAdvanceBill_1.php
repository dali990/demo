<?php

/**
 * Staticka klasa u kojoj se predvidjaju sva proknjizavanja
 */
class BookingPredictionAdvanceBill
{
    public static function Booking($document)
    {
        $booking_transactions = [];
        $bill = $document->file->bill;

        $_statement_comment = $bill->bill_number . ' - ' . $bill->partner->DisplayName;
        if (Yii::app()->company->inVat)
        {
            $booking_transactions[] = self::BookingAdvanceBillVat($document, $bill, $_statement_comment);
        }
        
        return SIMAMisc::joinArrays($booking_transactions);
    }

    /**
     * 
     * @param AccountDocument $document
     * @param type $bill
     * @param type $_statement_comment
     */
    private static function BookingAdvanceBillVat(AccountDocument $document, $bill, $_statement_comment)
    {
        $booking_transactions = [];
        
        //godina iz naloga za knjizenje
        $_year = $document->account_order->year;
        

        if ( $bill->invoice) //primljeni avans
        {
            $partner_account    = $bill->partner->getCustomerAdvanceAccount($_year);
            $vat_small_account  = Account::getFromParam('accounting.codes.vat.advance_outcome_small',$_year);
            $vat_big_account    = Account::getFromParam('accounting.codes.vat.advance_outcome_big',$_year);
        }
        else
        {
            $internal_vat_small_account_debit  = Account::getFromParam('accounting.codes.vat.advance_income_small_internal',$_year)->getLeafAccount();
            $internal_vat_small_account_credit  = Account::getFromParam('accounting.codes.vat.advance_outcome_small_internal',$_year)->getLeafAccount();
            $internal_vat_big_account_debit    = Account::getFromParam('accounting.codes.vat.advance_income_big_internal',$_year)->getLeafAccount();
            $internal_vat_big_account_credit    = Account::getFromParam('accounting.codes.vat.advance_outcome_big_internal',$_year)->getLeafAccount();
            
            $partner_account     = $bill->partner->getSupplierAdvanceAccount($_year);
            $vat_small_account  = Account::getFromParam('accounting.codes.vat.advance_income_small',$_year)->getLeafAccount();
            $vat_big_account    = Account::getFromParam('accounting.codes.vat.advance_income_big',$_year)->getLeafAccount();
        }     
        
        if (
                is_null($partner_account)
                        ||
                is_null($vat_small_account)
                        ||
                is_null($vat_big_account)
            )
        {
            Yii::app()->raiseNote('Nisu postavljeni osnovni konti za PDV');
        }
        else
        {
            foreach ($bill->bill_items as $bill_item)
            {
                if (!$bill_item->vat_refusal)
                {
                    continue;
                }
                if ($bill_item->is_internal_vat)
                {
                    if (!$bill->invoice)
                    {
                        if ($bill_item->internal_vat == '10') //ukoliko ima 10% interni PDV
                        {
                            $tr1 = new AccountTransaction();
                            $tr1->account_id = $internal_vat_small_account_debit->id;
                            $tr1->account_document_id = $document->id;
                            $tr1->comment = $_statement_comment;
                            $tr1->debit = $bill_item->value_internal_vat;
                            $booking_transactions[] = $tr1;

                            $tr2 = new AccountTransaction();
                            $tr2->account_id = $internal_vat_small_account_credit->id;
                            $tr2->account_document_id = $document->id;
                            $tr2->comment = $_statement_comment;
                            $tr2->credit = $bill_item->value_internal_vat;
                            $booking_transactions[] = $tr2;
                        }
                        elseif ($bill_item->internal_vat == '20') //ukoliko ima PDV
                        {
                            $tr1 = new AccountTransaction();
                            $tr1->account_id = $internal_vat_big_account_debit->id;
                            $tr1->account_document_id = $document->id;
                            $tr1->comment = $_statement_comment;
                            $tr1->debit = $bill_item->value_internal_vat;
                            $booking_transactions[] = $tr1;

                            $tr2 = new AccountTransaction();
                            $tr2->account_id = $internal_vat_big_account_credit->id;
                            $tr2->account_document_id = $document->id;
                            $tr2->comment = $_statement_comment;
                            $tr2->credit = $bill_item->value_internal_vat;
                            $booking_transactions[] = $tr2;
                        }
                    }
                }
                elseif ($bill_item->vat !== '0')
                {
                    $tr = new AccountTransaction();
                    $tr->account_id = $partner_account->id;
                    $tr->account_document_id = $document->id;
                    $tr->comment = $_statement_comment;
                    if ($bill->invoice)
                    {
                        $tr->debit = $bill_item->value_vat;
                    }
                    else
                    {
                        $tr->credit = $bill_item->value_vat;
                    }
                    $booking_transactions[] = $tr;

                    if ($bill_item->vat === '10') //ukoliko ima 10% interni PDV
                    {
                        $tr = new AccountTransaction();
                        $tr->account_id = $vat_small_account->id;
                        $tr->account_document_id = $document->id;
                        $tr->comment = $_statement_comment;
                        if ($bill->invoice)
                        {
                            $tr->credit = $bill_item->value_vat;
                        }
                        else
                        {
                            $tr->debit = $bill_item->value_vat;
                        }
                        $booking_transactions[] = $tr;
                    }
                    elseif ($bill_item->vat === '20') //ukoliko ima PDV
                    {
                        $tr = new AccountTransaction();
                        $tr->account_id = $vat_big_account->id;
                        $tr->account_document_id = $document->id;
                        $tr->comment = $_statement_comment;
                        if ($bill->invoice)
                        {
                            $tr->credit = $bill_item->value_vat;
                        }
                        else
                        {
                            $tr->debit = $bill_item->value_vat;
                        }
                        $booking_transactions[] = $tr;
                    }
                }
            }
        }
        return $booking_transactions;
    }
}