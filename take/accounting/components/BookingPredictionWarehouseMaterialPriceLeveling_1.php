<?php

/**
 * Staticka klasa u kojoj se predvidjaju sva proknjizavanja
 */
class BookingPredictionWarehouseMaterialPriceLeveling
{
    public static function Booking($document)
    {
        $booking_transactions = [];
        $_pl = $document->file->warehouse_material_price_leveling;

        $_statement_comment = $_pl->DisplayName;
        
        if (!is_null($_pl))
        {
            $booking_transactions = self::BookingFull($document, $_pl, $_statement_comment);
        }
        
        return $booking_transactions;
    }
    
    public static function BookingFull($document,  WarehouseMaterialPriceLeveling $_pl, $_statement_comment)
    {
        $booking_transactions = [];
        
        $year = $document->account_order->year;
        /**
         * calculate difference
         */
        $diff = 0;
        foreach ($_pl->warehouse_material_prices as $m_price)
        {
            $prev_price = $_pl->warehouse->previousPrice($m_price);
            $curr_status = $_pl->warehouse->materialStatus($m_price->warehouse_material_id, $_pl->leveling_time);
            
            if (is_null($prev_price))
            {
                throw new SIMAWarnException('Prva nivelacija ne moze da se knjizi');
            }
            
            $diff += $curr_status * ($m_price->value_per_unit - $prev_price->value_per_unit);
        }
        
        /**
         * add transactions
         */
        $main_account = $_pl->warehouse->getMainAccount($year);
        $correction_account = $_pl->warehouse->getCorrectionAccount($year);
        
        $tr1 = new AccountTransaction();
        $tr1->account_id = $main_account->id;
        $tr1->account_document_id = $document->id;
        $tr1->comment = $_statement_comment;
        $tr1->debit = $diff;
        $booking_transactions[] = $tr1;
        
        $tr2 = new AccountTransaction();
        $tr2->account_id = $correction_account->id;
        $tr2->account_document_id = $document->id;
        $tr2->comment = $_statement_comment;
        $tr2->credit = $diff;
        $booking_transactions[] = $tr2;
        
        
        return $booking_transactions;
    }

}