<?php

/**
 * Staticka klasa u kojoj se predvidjaju sva proknjizavanja
 */
class BookingPredictionBill
{
    /**
     * 
     * @param type $document
     * @param type $carry_saldo prenos za zavisne usluge
     * @return type
     */
    public static function Booking($document, &$carry_saldo = null, $pickup_carry = false)
    {
        $booking_transactions = [];
        $bill = $document->file->any_bill;
        $_statement_comment = $bill->bill_number . ' - ' . $bill->partner->DisplayName;
        if (!$bill->isReliefBill)
        {
            foreach ($bill->bill_items as $item)
            {
                if (!($item->quantity)>0)
                {
                    throw new SIMAWarnException('Sve stavke u racunu moraju da imaju kolicinu vecu od 0. Racun: '.$bill->DisplayName);
                }
                if  (
                        !$item->is_discount_item 
                        && $item->value_per_unit_with_rabat <= 0
                        && $item->value_vat_customs <= 0
                    )
                {
                    throw new SIMAWarnException('Stavka u racunu ima jedinicnu cenu manju ili jednaku nuli. Racun: '.$bill->DisplayName);
                }
            }
        }
        //MilosS: da se sredi sistem u bill_items do kraja, pa onda
//        if ($bill->services_bill_items_unconfirmed_count > 0)
//        {
//            throw new SIMAWarnException('Nisu sve stavke racuna obrađene. Racun: '.$bill->DisplayName);
//        }
        
        /**
         * vat booking 
         */
        $vat_bookings_temp = [];
        if (Yii::app()->company->inVat)
        {
            foreach ($bill->warehouse_bill_items as $bill_item)
            {
                if ($bill_item->vat_refusal)
                {
                    $vat_bookings_temp[] = self::BookingVat($document, $bill, $bill_item, $_statement_comment);
                }
            }
            foreach ($bill->fixed_assets_bill_items as $bill_item)
            {
                if ($bill_item->vat_refusal)
                {
                    $vat_bookings_temp[] = self::BookingVat($document, $bill, $bill_item, $_statement_comment);
                }
            }
            //zaduzenje PDV-a
            
        }
        $vat_bookings_fixed_and_warehouse = BookingPrediction::MergeTransactions(SIMAMisc::joinArrays($vat_bookings_temp));

        /**
         * Partner booking
         */
        $partner_bookings = self::BookingPartner($document, $bill, $_statement_comment);
        
        /**
         * Interest booking
         */
        $interest_bookings = self::BookingInterest($document, $bill, $_statement_comment);
        /**
         * Interest booking
         */
        $customs_bookings = self::BookingCustoms($document, $bill, $_statement_comment);

        /**
         * advance VAT booking - veza sa avansnim racunom
         */
//        $AR_bookings = [];
//        if (Yii::app()->company->inVat && $bill->vat_refusal)
//        {
            $AR_bookings = self::BookingAdvanceBillLink($document, $bill, $_statement_comment.' - AR');
//        }
        
        /**
         * advance payment booking
         */
        $A_bookings = self::BookingAdvancePaymentLink($document, $bill, $_statement_comment.' - A');

        /**
         * Main booking
         */
        if ($bill->invoice)
        {
            $booking_transactions[] = $partner_bookings;
            $booking_transactions[] = self::BookingServices($document, $bill, $_statement_comment);//izlazni racuni nemaju zavisne troskove
            //prebaceno u Services da bi se gledalo po stavkama
            //ovo ovde su magacin i osnovna sredstva
            $booking_transactions[] = $vat_bookings_fixed_and_warehouse; 
            $booking_transactions[] = $A_bookings;
            $booking_transactions[] = $AR_bookings;
            //nines
            $booking_transactions[] = BookingPredictionBillOut::Booking($document);
        }
        else
        {
            if ($pickup_carry)
            {
                $booking_transactions[] = self::BookingServices($document, $bill, $_statement_comment, $carry_saldo); 
            }
            else 
            {
                if(is_null($carry_saldo))
                {
                    $booking_transactions[] = self::BookingServices($document, $bill, $_statement_comment); 
//                    $booking_transactions[] = BookingPredictionBillIn::Booking($document);
                    $booking_transactions[] = BookingPredictionBill::BookingFixedAssets($document, $bill, $_statement_comment);
                }
//                else
//                {
//                    $booking_transactions[] = BookingPredictionBillIn::Booking($document, $carry_saldo);
//                    $booking_transactions[] = BookingPredictionBill::BookingFixedAssets($document, $bill, $_statement_comment, $carry_saldo);
//                }
                
                $booking_transactions[] = $customs_bookings;
                $booking_transactions[] = $interest_bookings;
                $booking_transactions[] = $vat_bookings_fixed_and_warehouse;
                $booking_transactions[] = $partner_bookings;
                $booking_transactions[] = $A_bookings;
                $booking_transactions[] = $AR_bookings;
            }
            

            
        }
        
        return SIMAMisc::joinArrays($booking_transactions);
    }
    
    /**
     * 
     * @param AccountDocument $document
     * @param Bill $bill
     * @param type $_statement_comment
     * @return \AccountTransaction
     */
    private static function BookingInterest(AccountDocument $document,Bill $bill, $_statement_comment)
    {
        $booking_transactions = [];
        
        $amount = $bill->interest;
        if ($amount > 0)
        {
            //godina iz naloga za knjizenje
            $_year = $document->account_order->year;

            $bill_in_interest     = Account::getFromParam('accounting.codes.bill_in_interest',$_year)->getLeafAccount();
            $tr = new AccountTransaction();
            $tr->account_id = $bill_in_interest->id;
            $tr->account_document_id = $document->id;
            $tr->comment = $_statement_comment;
            $tr->debit = $amount;
            $booking_transactions[] = $tr;

        }

        return $booking_transactions;
    }
    
    /**
     * 
     * @param AccountDocument $document
     * @param Bill $bill
     * @param type $_statement_comment
     * @return \AccountTransaction
     */
    private static function BookingCustoms(AccountDocument $document,Bill $bill, $_statement_comment)
    {
        $booking_transactions = [];
        
        $amount = $bill->value_customs;
        if ($amount > 0)
        {
            //godina iz naloga za knjizenje
            $_year = $document->account_order->year;

            $customs_costs     = Account::getFromParam('accounting.codes.vat.customs_costs',$_year)->getLeafAccount();
            $tr = new AccountTransaction();
            $tr->account_id = $customs_costs->id;
            $tr->account_document_id = $document->id;
            $tr->comment = $_statement_comment;
            $tr->debit = $amount;
            $booking_transactions[] = $tr;

        }

        return $booking_transactions;
    }
        
    /**
     * Zaduzivanje partnera - partner se uvek zaduzuje
     * @param type $document
     * @param type $bill
     * @param type $_statement_comment
     */
    private static function BookingPartner($document, $bill, $_statement_comment)
    {
        if (SIMAMisc::areEqual($bill->amount, 0))
        {
            return [];
        }
        $booking_transactions = [];
        $partner_account = null;
        //godina iz naloga za knjizenje
        $_year = $document->account_order->year;
        
        if ( $bill->invoice) 
        {
            $partner_account = $bill->partner->getCustomerAccount($_year);
        }
        if (!$bill->invoice) 
        {
            $partner_account = $bill->partner->getSupplierAccount($_year);
        }    
        
        $tr = new AccountTransaction();
        $tr->account_id = $partner_account->id;
        $tr->account_document_id = $document->id;
        $tr->comment = $_statement_comment;
        if ($bill->invoice)
        {
            $tr->debit = $bill->amount;
        }
        else
        {
            $tr->credit = $bill->amount;
        }
        $booking_transactions[] = $tr;
        
        return $booking_transactions;
    }
    
    private static function BookingVat($document,$bill, $bill_item, $_statement_comment)
    {
        $booking_transactions = [];
        
        //godina iz naloga za knjizenje, mada mora da bude ista i iz izvoda
        $_year = $document->account_order->year;
        
        $_conn_model = null;
        if (Yii::app()->configManager->get('accounting.warehouse.book_warehouse_VAT_separately') === true)
        {
            $ao = $document->account_order;
            foreach ($ao->account_documents as $_ad) 
            {
                if (isset($_ad->file->warehouse_receiving))
                {
                    $_conn_model = $_ad->file->warehouse_receiving->warehouse_to;
                }
            }
        }
        if ($bill_item->is_internal_vat)
        {
            //Za izlazne racune se ne obracunava interni PDV
            if ( ! $bill->invoice)
            {
                $internal_vat_small_account_credit  = Account::getFromParam('accounting.codes.vat.income_small_internal',$_year)->getLeafAccount($_conn_model);
                $internal_vat_small_account_debit  = Account::getFromParam('accounting.codes.vat.outcome_small_internal',$_year)->getLeafAccount($_conn_model);
                $internal_vat_big_account_credit    = Account::getFromParam('accounting.codes.vat.income_big_internal',$_year)->getLeafAccount($_conn_model);
                $internal_vat_big_account_debit    = Account::getFromParam('accounting.codes.vat.outcome_big_internal',$_year)->getLeafAccount($_conn_model);

    //            if ($bill->vat10 > 0) //ukoliko ima PDV
                if ($bill_item->internal_vat == '10')
                {

                    $tr2 = new AccountTransaction();
                    $tr2->account_id = $internal_vat_small_account_credit->id;
                    $tr2->account_document_id = $document->id;
                    $tr2->comment = $_statement_comment;
                    $tr2->debit = $bill_item->value_internal_vat;
                    $booking_transactions[] = $tr2;

                    $tr1 = new AccountTransaction();
                    $tr1->account_id = $internal_vat_small_account_debit->id;
                    $tr1->account_document_id = $document->id;
                    $tr1->comment = $_statement_comment;
                    $tr1->credit = $bill_item->value_internal_vat;
                    $booking_transactions[] = $tr1;
                }

    //            if ($bill->vat20 > 0) //ukoliko ima PDV
                if ($bill_item->internal_vat == '20')
                {
                    $tr2 = new AccountTransaction();
                    $tr2->account_id = $internal_vat_big_account_credit->id;
                    $tr2->account_document_id = $document->id;
                    $tr2->comment = $_statement_comment;
                    $tr2->debit = $bill_item->value_internal_vat;
                    $booking_transactions[] = $tr2;

                    $tr1 = new AccountTransaction();
                    $tr1->account_id = $internal_vat_big_account_debit->id;
                    $tr1->account_document_id = $document->id;
                    $tr1->comment = $_statement_comment;
                    $tr1->credit = $bill_item->value_internal_vat;
                    $booking_transactions[] = $tr1;
                }
            }
        }
        else
        {
            $vat_small_account = null;
            $vat_big_account = null;
            if ( $bill->invoice) 
            {
                $vat_small_account  = Account::getFromParam('accounting.codes.vat.outcome_small',$_year)->getLeafAccount($_conn_model);
                $vat_big_account    = Account::getFromParam('accounting.codes.vat.outcome_big',$_year)->getLeafAccount($_conn_model);
            }
            if (!$bill->invoice) //kupac
            {
                $vat_small_account  = Account::getFromParam('accounting.codes.vat.income_small',$_year)->getLeafAccount($_conn_model);
                $vat_big_account    = Account::getFromParam('accounting.codes.vat.income_big',$_year)->getLeafAccount($_conn_model);
            }    

//            if ($bill->vat10 > 0) //ukoliko ima PDV
            if ($bill_item->vat == '10')
            {
                $tr = new AccountTransaction();
                $tr->account_id = $vat_small_account->id;
                $tr->account_document_id = $document->id;
                $tr->comment = $_statement_comment;
                if ($bill->invoice)
                {
                    $tr->credit = $bill_item->value_vat;
                }
                else
                {
                    $tr->debit = $bill_item->value_vat;
                }
                $booking_transactions[] = $tr;
            }

//            if ($bill->vat20 > 0) //ukoliko ima PDV
            if ($bill_item->vat == '20')
            {
                $tr = new AccountTransaction();
                $tr->account_id = $vat_big_account->id;
                $tr->account_document_id = $document->id;
                $tr->comment = $_statement_comment;
                if ($bill->invoice)
                {
                    $tr->credit = $bill_item->value_vat;
                }
                else
                {
                    $tr->debit = $bill_item->value_vat;
                }
                $booking_transactions[] = $tr;
            }
        }
        
        return $booking_transactions;
    }
    
    /**
     * Zaduzivanje partnera
     * Ukoliko je skinuto po avansu, onda skinuti i sa avansa
     * @param type $document
     * @param type $bill
     * @param type $_statement_comment
     */
    private static function BookingAdvanceBillLink($document, $bill, $_statement_comment)
    {
        $booking_transactions = [];
        
        if ($bill->advance_releases_count === 0)
        {
            return [];
        }

        if (!$bill->invoice) //ulazni racuni
        {
            if (gettype($bill->vat_refusal) === 'NULL')
            {
                $msg = 'Račun ima mešovite stavke po pitanju korišćenja PDV. '
                        . 'Molimo vas RUČNO rasknjižite iznos rasknjiženog avansa. '
                        . 'SIMA će dati predlog na PUN iznos likvidacije.';
                Yii::app()->raiseNote($msg);
    //            return $booking_transactions;
            }
        }
        
        //godina iz naloga za knjizenje
        $_year = $document->account_order->year;
        
        
//        if (abs($bill->vat)>0) //ne sme ovo jer su moguci avansni racuni bez pdv-a

        if ( $bill->invoice) //primljeni avans
        {
            $partner_account     = $bill->partner->getCustomerAccount($_year);
            $partner_adv_account = $bill->partner->getCustomerAdvanceAccount($_year);
            
            $advance_vat_small_account  = Account::getFromParam('accounting.codes.vat.advance_outcome_small',$_year)->getLeafAccount();
            $advance_vat_big_account    = Account::getFromParam('accounting.codes.vat.advance_outcome_big',$_year)->getLeafAccount();
        }
        else
        {
            $partner_account     = $bill->partner->getSupplierAccount($_year);
            $partner_adv_account = $bill->partner->getSupplierAdvanceAccount($_year);
            $internal_vat_small_account_credit  = Account::getFromParam('accounting.codes.vat.advance_income_small_internal',$_year)->getLeafAccount();
            $internal_vat_small_account_debit  = Account::getFromParam('accounting.codes.vat.advance_outcome_small_internal',$_year)->getLeafAccount();
            $internal_vat_big_account_credit    = Account::getFromParam('accounting.codes.vat.advance_income_big_internal',$_year)->getLeafAccount();
            $internal_vat_big_account_debit    = Account::getFromParam('accounting.codes.vat.advance_outcome_big_internal',$_year)->getLeafAccount();
            
            $advance_vat_small_account  = Account::getFromParam('accounting.codes.vat.advance_income_small',$_year)->getLeafAccount();
            $advance_vat_big_account    = Account::getFromParam('accounting.codes.vat.advance_income_big',$_year)->getLeafAccount();
        }    
        
        if (
                is_null($partner_account)
                        ||
                is_null($advance_vat_small_account)
                        ||
                is_null($advance_vat_big_account)
            )
        {
            Yii::app()->raiseNote('Nisu postavljeni osnovni konti za PDV');
        }
        else
        {
            foreach ($bill->advance_releases as $advance_release)
            {
                if ($advance_release->internal_vat)
                {
                    /**
                     * AVANSNI PDV
                     */
//                    $advance_vat10_amount       = $bill->advance_released_vat10_sum; //PDV10%
//                    $advance_vat20_amount       = $bill->advance_released_vat20_sum; //PDV20%
//                    $advance_bills_base_amount  = $bill->advance_released_base_amount_sum; //OSNOVICA NA AVANSNOM RACUNU
                    
                    if ($advance_release->base_amount > 0)
                    {
                        $tr_partner = new AccountTransaction();
                        $tr_partner->account_id = $partner_account->id;
                        $tr_partner->account_document_id = $document->id;
                        $tr_partner->comment = $_statement_comment;
                        if ($bill->invoice)
                        {
                            $tr_partner->credit = $advance_release->base_amount;
                        }
                        else
                        {
                            $tr_partner->debit = $advance_release->base_amount;
                        }
                        $booking_transactions[] = $tr_partner;

                        $tr = new AccountTransaction();
                        $tr->account_id = $partner_adv_account->id;
                        $tr->account_document_id = $document->id;
                        $tr->comment = $_statement_comment;
                        if ($bill->invoice)
                        {
                            $tr->debit = $advance_release->base_amount;
                        }
                        else
                        {
                            $tr->credit = $advance_release->base_amount;
                        }
                        $booking_transactions[] = $tr;
                    }
                    if (!$bill->invoice)
                    {
                        if ($advance_release->vat_rate == '10') //ukoliko ima 10% interni PDV
                        {
                            $tr1 = new AccountTransaction();
                            $tr1->account_id = $internal_vat_small_account_debit->id;
                            $tr1->account_document_id = $document->id;
                            $tr1->comment = $_statement_comment;
                            $tr1->debit = $advance_release->vat_amount;
                            $booking_transactions[] = $tr1;

                            $tr2 = new AccountTransaction();
                            $tr2->account_id = $internal_vat_small_account_credit->id;
                            $tr2->account_document_id = $document->id;
                            $tr2->comment = $_statement_comment;
                            $tr2->credit = $advance_release->vat_amount;
                            $booking_transactions[] = $tr2;
                        }
                        elseif ($advance_release->vat_rate == '20') //ukoliko ima PDV
                        {
                            $tr1 = new AccountTransaction();
                            $tr1->account_id = $internal_vat_big_account_debit->id;
                            $tr1->account_document_id = $document->id;
                            $tr1->comment = $_statement_comment;
                            $tr1->debit = $advance_release->vat_amount;
                            $booking_transactions[] = $tr1;

                            $tr2 = new AccountTransaction();
                            $tr2->account_id = $internal_vat_big_account_credit->id;
                            $tr2->account_document_id = $document->id;
                            $tr2->comment = $_statement_comment;
                            $tr2->credit = $advance_release->vat_amount;
                            $booking_transactions[] = $tr2;
                        }
                    }
                }
                else
                {
                    /**
                     * AVANSNI PDV
                     */
//                    if (Yii::app()->company->inVat)
//                    {
//                        $advance_vat10_amount       = $bill->advance_released_vat10_sum; //PDV10%
//                        $advance_vat20_amount       = $bill->advance_released_vat20_sum; //PDV20%
//                        $advance_bills_base_amount  = $bill->advance_released_base_amount_sum; //OSNOVICA NA AVANSNOM RACUNU
//                    }
//                    else 
//                    { //nema knjizenja PDV-a
//                        $advance_vat10_amount       = 0; //PDV10%
//                        $advance_vat20_amount       = 0; //PDV20%
//                        $advance_bills_base_amount  = $bill->advance_released_amount_sum; //OSNOVICA NA AVANSNOM RACUNU
//                    }
//                    $partner_amount = $advance_vat10_amount + $advance_vat20_amount + $advance_bills_base_amount;
//                    $partner_amount = $bill_item->value + $bill_item->value_vat;
                    
                    if ($advance_release->amount > 0)
                    {
                        $tr = new AccountTransaction();
                        $tr->account_id = $partner_account->id;
                        $tr->account_document_id = $document->id;
                        $tr->comment = $_statement_comment;
                        if ($bill->invoice)
                        {
                            $tr->credit = $advance_release->amount;
                        }
                        else
                        {
                            $tr->debit = $advance_release->amount;
                        }
                        $booking_transactions[] = $tr;
                    }
                    
                    //izlazni racun se uvek PDV gleda odvojeno
                    
                    //na ovaj nacin ukljucujemo i NULL, odnosno kada su komplikovane stavke
//                    if($bill->invoice || $bill->vat_refusal === true)
                    if($bill->invoice || $bill->vat_refusal !== false)
                    {
                        $_base_amount = $advance_release->base_amount;
                        $_vat_amount = $advance_release->vat_amount;
                    }
                    else
                    {
                        $_base_amount = $advance_release->base_amount + $advance_release->vat_amount;
                        $_vat_amount = 0;
                    }
                    
                    if ($_base_amount > 0)
                    {
                        $tr = new AccountTransaction();
                        $tr->account_id = $partner_adv_account->id;
                        $tr->account_document_id = $document->id;
                        $tr->comment = $_statement_comment;
                        if ($bill->invoice)
                        {
                            $tr->debit = $_base_amount;
                        }
                        else
                        {
                            $tr->credit = $_base_amount;
                        }
                        $booking_transactions[] = $tr;
                    }

                    if ($_vat_amount > 0)
                    {
                        $tr = new AccountTransaction();
                        if ($advance_release->vat_rate == BillItem::$VAT_10) //ukoliko ima 10%  PDV
                        {
                            $tr->account_id = $advance_vat_small_account->id;
                        }
                        elseif ($advance_release->vat_rate == BillItem::$VAT_20) //ukoliko ima PDV
                        {
                            $tr->account_id = $advance_vat_big_account->id;
                        }
                        else
                        {
                            throw new SIMAWarnAutoBafException("racun id: $id ima likvidaciju sa 0% PDV, a iznos PDV nije 0");
                        }
                        $tr->account_document_id = $document->id;
                        $tr->comment = $_statement_comment;
                        if ($bill->invoice)
                        {
                            $tr->debit = $_vat_amount;
                        }
                        else
                        {
                            $tr->credit = $_vat_amount;
                        }
                        $booking_transactions[] = $tr;
                    }
                }
            }
            
        }
        return $booking_transactions;
    }
        
    
    /**
     * Help function for BookingPartner
     * @param type $bill_id ID of Bill
     */
    private function advances_by_payments_sum(Bill $bill)
    {
        if (Yii::app()->configManager->get('accounting.book_bill_paymnet_connections_by_date_or_type') === 'BY_DATE')
        {
            $_same_day_advance = boolval(Yii::app()->configManager->get('accounting.paymnet_same_day_as_bill_is_advance'));
            $payment_condition = [
                'payment_date' => [$_same_day_advance?'<=':'<',$bill->income_date]
            ];
        }
        else
        {
            $adv_payment_type = Yii::app()->configManager->get('accounting.advance_payment_type');
            $payment_condition = [
                'payment_type_id' => $adv_payment_type,
            ];
        }
        //zbir avansa direktno sa avansnih uplata
        $cond2 = new SIMADbCriteria([
            'select' => 'sum(amount) as amount',
            'Model' => 'BillRelease',
            'model_filter' => [
                'payment' => $payment_condition,
                'bill' => ['ids' => $bill->id ]
            ]
        ]);
        $adv_bills2 = BillRelease::model()->find($cond2);
        return $adv_bills2->amount;
    }

    /**
     * Zaduzivanje partnera
     * Ukoliko je skinuto po avansu, onda skinuti i sa avansa
     * @param type $document
     * @param type $bill
     * @param type $_statement_comment
     */
    private static function BookingAdvancePaymentLink($document, $bill, $_statement_comment)
    {
        $booking_transactions = [];
        
        //godina iz naloga za knjizenje
        $_year = $document->account_order->year;
        
        if ( $bill->invoice) 
        {
            $partner_account = $bill->partner->getCustomerAccount($_year);
            $partner_adv_account = $bill->partner->getCustomerAdvanceAccount($_year);
        }
        else
        {
            $partner_account = $bill->partner->getSupplierAccount($_year);
            $partner_adv_account = $bill->partner->getSupplierAdvanceAccount($_year);
        }  

        /**
         * Dohvatanje uplacenih avansa samo preko avansne uplate
         */
        $base_amount_advance = self::advances_by_payments_sum($bill);
        

        if ($base_amount_advance>0)
        {
            $tr1 = new AccountTransaction();
            $tr1->account_id = $partner_adv_account->id;
            $tr1->account_document_id = $document->id;
            $tr1->comment = $_statement_comment;
            if ($bill->invoice)
            {
                $tr1->debit = $base_amount_advance;
            }
            else
            {
                $tr1->credit = $base_amount_advance;
            }
            $booking_transactions[] = $tr1;
            
            $tr2 = new AccountTransaction();
            $tr2->account_id = $partner_account->id;
            $tr2->account_document_id = $document->id;
            $tr2->comment = $_statement_comment;
            if ($bill->invoice)
            {
                $tr2->credit = $base_amount_advance;
            }
            else
            {
                $tr2->debit = $base_amount_advance;
            }
            $booking_transactions[] = $tr2;
            
        }

        

        return $booking_transactions;
    }
    
    /**
     * 
     * @param AccountDocument $document
     * @param type $bill
     * @param type $_statement_comment
     * @param integer $carry_saldo if NULL means is booked in WarehouseTransfer
     * @return \AccountTransaction
     */
    private static function BookingServices($document, $bill, $_statement_comment, &$carry_saldo = null)
    {
        $booking_transactions = [];
        $_book_separatly = boolval(Yii::app()->configManager->get('accounting.book_services_separately'));
        //godina iz naloga za knjizenje
        $_year = $document->account_order->year;
        $company_in_vat = Yii::app()->company->inVat;
        
        $_for_bookings = []; //kombinacija konto / iznos za knjizenje
        $used_amount = 0;
        //prolazak kroz sve servisne stavke
        
//       MILosS(TODO): da se izbace ne potvrdjene stavke
//        $services_to_book = $bill->services_bill_items;
        $services_to_book = array_merge($bill->services_bill_items,$bill->services_bill_items_unconfirmed);
        
        foreach ($services_to_book as $item) 
        {
            $use_vat = $company_in_vat && $item->vat_refusal;
            $_local_used_amount1 = $item->value;
                
            if (!$use_vat)
            {
                $_local_used_amount1 += $item->value_vat;
            }
            if (!is_null($carry_saldo))
            {
                $carry_saldo += $_local_used_amount1;
            }
            else
            {
                $job_order_account = $item->getBookingAccount($_year);

                if ((!SIMAMisc::areEqual($_local_used_amount1, 0)) && !is_null($job_order_account))
                {
                    if (!$_book_separatly)
                    {
                        $pair_found = false;
                        foreach ($_for_bookings as $key => $pair)
                        {
                            if ($pair['account']->id == $job_order_account->id)
                            {
                                if ($bill->invoice)
                                {
                                    $_for_bookings[$key]['credit'] += $_local_used_amount1;
                                }
                                else
                                {
                                    $_for_bookings[$key]['debit'] += $_local_used_amount1;
                                }
                                $pair_found = true;
                            }
                        }
                        if (!$pair_found)
                        {
                            $_for_bookings[] = [
                                'account' => $job_order_account,
                                'credit' => ($bill->invoice)? $_local_used_amount1:0,
                                'debit' =>  ($bill->invoice)? 0:$_local_used_amount1
                            ];
                        }
                    }
                    else 
                    {
                        $tr = new AccountTransaction();
                        $tr->account_id = $job_order_account->id;
                        $tr->account_document_id = $document->id;
                        $tr->comment = $_statement_comment;
                        if ($bill->invoice)
                        {
                            $tr->credit = $_local_used_amount1;
                        }
                        else
                        {
                            $tr->debit = $_local_used_amount1;
                        }
                        $booking_transactions[] = $tr;
                    }
                }
                else
                {
                    $used_amount += $_local_used_amount1;
                }
            }
            
            if ($use_vat)
            {
                $vat_bookings = self::BookingVat($document, $bill, $item, $_statement_comment);
                foreach ($vat_bookings as $_one_booking_item)
                {
                    if (!$_book_separatly)
                    {
                        $pair_found = false;
                        foreach ($_for_bookings as $key => $pair)
                        {
                            if ($pair['account']->id == $_one_booking_item->account_id)
                            {
                                $_for_bookings[$key]['credit'] += $_one_booking_item->credit;
                                $_for_bookings[$key]['debit']  += $_one_booking_item->debit;
                                $pair_found = true;
                            }
                        }
                        if (!$pair_found)
                        {
                            $_for_bookings[] = [
                                'account' => $_one_booking_item->account,
                                'credit' => $_one_booking_item->credit,
                                'debit' => $_one_booking_item->debit
                            ];
                        }
                    }
                    else 
                    {
                        $booking_transactions[] = $_one_booking_item;
                    }
                }
            }
        }
        
        if ($bill->invoice)
        {
            foreach ($bill->warehouse_bill_items as $item) 
            {

                if (!is_null($carry_saldo))
                {
                    $carry_saldo += $_local_used_amount2;
                }
                else
                {
                    $_local_used_amount2 = $item->value;
                    $use_vat = $company_in_vat && $item->vat_refusal;
                    if (!$use_vat)
                    {
                        $_local_used_amount2 += $item->value_vat;
                    }

                    $job_order_account = $item->getBookingAccount($_year);

                    if ($_local_used_amount2 > 0.001 && !is_null($job_order_account))
                    {
                        if (!$_book_separatly)
                        {
                            $pair_found = false;
                            foreach ($_for_bookings as $key => $pair)
                            {
                                if ($pair['account']->id == $job_order_account->id)
                                {
                                    if ($bill->invoice)
                                    {
                                        $_for_bookings[$key]['credit'] += $_local_used_amount2;
                                    }
                                    else
                                    {
                                        $_for_bookings[$key]['debit'] += $_local_used_amount2;
                                    }
                                    $pair_found = true;
                                }
                            }
                            if (!$pair_found)
                            {
                                $_for_bookings[] = [
                                    'account' => $job_order_account,
                                    'credit' => ($bill->invoice)? $_local_used_amount2:0,
                                    'debit' =>  ($bill->invoice)? 0:$_local_used_amount2
                                ];
                            }
                        }
                        else 
                        {
                            $tr = new AccountTransaction();
                            $tr->account_id = $job_order_account->id;
                            $tr->account_document_id = $document->id;
                            $tr->comment = $_statement_comment;
                            if ($bill->invoice)
                            {
                                $tr->credit = $_local_used_amount2;
                            }
                            else
                            {
                                $tr->debit = $_local_used_amount2;
                            }
                            $booking_transactions[] = $tr;
                        }
                    }
                    else
                    {
                        $used_amount += $_local_used_amount2;
                    }
                }
                
                //MilosS(20.10.2018): ovo knjizenje se radi u promenljivu "$vat_bookings_temp", tako da je ostao visak
                //treba proveriti
//                $vat_bookings = self::BookingVat($document, $bill, $item, $_statement_comment);
//                foreach ($vat_bookings as $value)
//                {
//                    $booking_transactions[] = $value;
//                }
            }
        }
        
        $_income_rounding_cost_type = Yii::app()->configManager->get('accounting.income_rounding_cost_type', false);
        
        if ($bill->invoice && !is_null($_income_rounding_cost_type))
        {
            $used_amount = 0;
            
            $_cost_type = CostType::model()->findByPk($_income_rounding_cost_type);
            if (is_null($_cost_type))
            {
                throw new SIMAException('Postavljen parametar income_rounding_cost_type koji ne postoji -> ID: '.$_income_rounding_cost_type);
            }
            $_account = $_cost_type->accountForYear($_year);
            $tr = new AccountTransaction();
            $tr->account_id = $_account->id;
            $tr->account_document_id = $document->id;
            $tr->comment = $_statement_comment;
            $tr->debit = -$bill->rounding;
            $booking_transactions[] = $tr;
        }
        else
        {
            if (!is_null($carry_saldo))
            {
                $carry_saldo += $bill->rounding;
            }
            else
            {
                $used_amount = $bill->rounding;
            }
        }
        
        if (!is_null($carry_saldo))
        {
            return [];
        }
        else
        {
            if (!SIMAMisc::areEqual($used_amount, 0))
            {
                $raise_note = '';
                if (isset($bill->cost_type))
                {
                    $job_order_account_base = $bill->cost_type->accountForYear($_year);                
                    if (is_null($job_order_account_base))
                    {
                        $raise_note = "Ne postoji vrsta troška '{$bill->cost_type->DisplayName}' za godinu {$_year->year}.";
                    }
                }
                else
                {
                    $job_order_account_base = ($bill->invoice) ? 
                            Account::getFromParam('accounting.codes.default_income_account',$_year)
                            :
                            Account::getFromParam('accounting.codes.default_costs_account',$_year);
                    if (is_null($job_order_account_base))
                    {
                        $raise_note = 'Nisu postavljeni osnovni konti za radne naloge';
                    }
                }
                if (is_null($job_order_account_base))
                {
                    Yii::app()->raiseNote($raise_note);
                }
                else
                {

                    if (!isset($bill->job_order) || !$bill->job_order->for_analytics)
                    {
                        $job_order_account = $job_order_account_base->getLeafAccount(null, true);
                    }
                    else
                    {
                        $job_order_account = $job_order_account_base->getLeafAccount($bill->job_order);
                    }

                    if (!is_null($job_order_account))
                    {

                        if (!$_book_separatly)
                        {
                            $pair_found = false;
                            foreach ($_for_bookings as $key => $pair)
                            {
                                if ($pair['account']->id == $job_order_account->id)
                                {
                                    if ($bill->invoice)
                                    {
                                        $_for_bookings[$key]['credit'] += $used_amount;
                                    }
                                    else
                                    {
                                        $_for_bookings[$key]['debit'] += $used_amount;
                                    }
                                    $pair_found = true;
                                }
                            }
                            if (!$pair_found)
                            {
                                $_for_bookings[] = [
                                    'account' => $job_order_account,
                                    'credit' => ($bill->invoice)? $used_amount:0,
                                    'debit' =>  ($bill->invoice)? 0:$used_amount
                                ];
                            }
                        }
                        else 
                        {
                            $tr = new AccountTransaction();
                            $tr->account_id = $job_order_account->id;
                            $tr->account_document_id = $document->id;
                            $tr->comment = $_statement_comment;
                            if ($bill->invoice)
                            {
                                $tr->credit = $used_amount;
                            }
                            else
                            {
                                $tr->debit = $used_amount;
                            }
                            $booking_transactions[] = $tr;
                        }
                    }
                }
            }

            foreach ($_for_bookings as $pair)
            {
                $tr = new AccountTransaction();
                $tr->account_id = $pair['account']->id;
                $tr->account_document_id = $document->id;
                $tr->comment = $_statement_comment;
                $tr->credit = $pair['credit'];
                $tr->debit = $pair['debit'];
                $booking_transactions[] = $tr;
            }

            return $booking_transactions;
        }
    }   
    
    /**
     * 
     * @param type $document
     * @param type $bill
     * @param type $_statement_comment
     * @param type $carry_saldo uvecanje po item-u
     * @return \AccountTransaction
     * @throws SIMAWarnException
     */
    private static function BookingFixedAssets($document, $bill, $_statement_comment, $carry_saldo = null)
    {
        $booking_transactions = [];
        
        
        foreach($bill->fixed_assets_bill_items as $item)
        {
            $use_vat = Yii::app()->company->inVat && $item->vat_refusal;
            $used_amount = $item->value + $carry_saldo*$item->quantity;
            if (!$use_vat)
            {
                $used_amount += $item->value_vat;
            }
            
            $_fas = $item->fixed_assets;
            if (count($_fas) < 1)
            {
                throw new SIMAWarnException('Ne postoji osnovno sredstvo na stavci racuna');
            }
            
            if (!is_null($_fas[0]->getAccountForBooking()))
            {
                $_fixed_assets_account = $_fas[0]->getAccountForBooking()->forYear($document->account_order->year);
            }
            else
            {
                throw new SIMAWarnException('Postavite konto za knjizenje osnovnih sredstava tipa '
                        .'<strong>'.$_fas[0]->fixed_asset_type->DisplayName.'</strong>');
            }
            
            $tr = new AccountTransaction();
            $tr->account_id = $_fixed_assets_account->id;
            $tr->account_document_id = $document->id;
            $tr->comment = $_statement_comment;
            $tr->debit = $used_amount;
            $booking_transactions[] = $tr;
            
        }
        

        return $booking_transactions;
    }   
   
}