<?php

/**
 * Staticka klasa u kojoj se predvidjaju sva proknjizavanja
 */
class BookingPredictionPaycheckPeriod
{
    /**
     * 
     * @param AccountDocument $document
     * @param Payment $payment - knjizenje objedinjene uplate drzavi
     * @return type
     */
    public static function Booking(AccountDocument $document)
    {
        $booking_transactions = [];
        
        if (isset($document->file->regular_paycheck_period)) //knjizenje obracuna plate
        {
            $paycheck_period = $document->file->regular_paycheck_period;
            foreach ($paycheck_period->paychecks as $paycheck)
            {
                if ($paycheck->is_valid && !isset($paycheck->file))
                {
//                    throw new SIMAWarnNoOZPapers($paycheck_period);
                }
            }
            $_statement_comment = $document->file->DisplayName;
            $booking_transactions[] = self::BookingRegularPeriodSimple($document, $paycheck_period, $_statement_comment);
        }
        
        
        return SIMAMisc::joinArrays($booking_transactions);
    }
    
    /**
     * 
     * @param AccountDocument $document
     * @param Payment $payment - knjizenje objedinjene uplate drzavi
     * @param $_statement_comment - komentar
     * @return type
     */
    public static function BookingForPayment(AccountDocument $document, Payment $payment, $_statement_comment = '')
    {
        $booking_transactions = [];

        //knjizenje izvoda po plati
        if (isset($payment->paycheck_period))
        {
            $paycheck_period = $payment->paycheck_period;
            $_statement_comment .= ' - objedinjena uplata - '.$paycheck_period->DisplayName;
            $booking_transactions[] = self::BookingPayment($document, $paycheck_period, $payment, $_statement_comment);
        }
        else
        {
            Yii::app()->raiseNote('Uplata nije vezana sa obracunom plata, pa nece ni biti proknjizena'.$payment->id);
        }
            
        return SIMAMisc::joinArrays($booking_transactions);
    }
    

    /**
     * Knjizenje jednostavno
     * 520 |     - mali bruto
     * 521 |     - bruto - mali bruto
     *     | 450 - neto
     *     | 451 - porez zaposleni
     *     | 452 - doprinosi zaposleni
     *     | 453 - poslodavac
     * 
     * @param AccountDocument $document
     * @param PaycheckPeriod $paycheck_period - 
     * @param string $_statement_comment
     */
    private static function BookingRegularPeriodSimple(AccountDocument $document,  PaycheckPeriod $paycheck_period, $_statement_comment)
    {
        $booking_transactions = [];
        
        //godina iz naloga za knjizenje
        $_year = $document->account_order->year;
        
        //520
        $costs_employee = 0;
        //521
        $costs_company = 0;
        //450
        $obligation_emp_neto = 0;
        //450901
        $obligation_emp_neto_suspensions = 0;
        //451
        $obligation_emp_tax = 0;
        //4520
        $obligation_emp_pio = 0;
        //4521
        $obligation_emp_zdr = 0;
        //4522
        $obligation_emp_nez = 0;
        //4530
        $obligation_company_pio = 0;
        //4531
        $obligation_company_zdr = 0;
        //4532
        $obligation_company_nez = 0;
        
        foreach ($paycheck_period->paychecks as $paycheck)
        {
            if ($paycheck->is_valid)
            {
                $costs_employee += $paycheck->amount_bruto;
                $costs_company += $paycheck->amount_total - $paycheck->amount_bruto;
                $obligation_emp_neto += $paycheck->amount_neto_for_payout;
                $obligation_emp_neto_suspensions += $paycheck->amount_neto - $paycheck->amount_neto_for_payout;
                $obligation_emp_tax += $paycheck->tax_empl;
                $obligation_emp_pio += $paycheck->pio_empl;
                $obligation_emp_zdr += $paycheck->zdr_empl;
                $obligation_emp_nez += $paycheck->nez_empl;
    //            $obligation_company += $paycheck->amount_total;
                $obligation_company_pio += $paycheck->pio_comp;
                $obligation_company_zdr += $paycheck->zdr_comp;
                $obligation_company_nez += $paycheck->nez_comp;
            }
        }
                
        
        //520
        if (SIMAMisc::lessThen(0, $costs_employee))
        {
            $costs_employee_account  = Account::getFromParam('accounting.codes.paychecks.costs_employee',$_year)->getLeafAccount();
            $tr1 = new AccountTransaction();
            $tr1->account_id = $costs_employee_account->id;
            $tr1->account_document_id = $document->id;
            $tr1->comment = $_statement_comment;
            $tr1->debit = $costs_employee;
            $booking_transactions[] = $tr1;
        }

        //450
        if (SIMAMisc::lessThen(0, $obligation_emp_neto))
        {
            $obligation_emp_neto_account  = Account::getFromParam('accounting.codes.paychecks.obligation_emp_neto',$_year)->getLeafAccount();
            $tr2 = new AccountTransaction();
            $tr2->account_id = $obligation_emp_neto_account->id;
            $tr2->account_document_id = $document->id;
            $tr2->comment = $_statement_comment;
            $tr2->credit = $obligation_emp_neto;
            $booking_transactions[] = $tr2;
        }
        
        //450901
        if (SIMAMisc::lessThen(0, $obligation_emp_neto_suspensions))
        {
            $obligation_emp_neto_suspensions_account  = Account::getFromParam('accounting.codes.paychecks.obligation_emp_neto_stops',$_year)->getLeafAccount();
            $tr2_s = new AccountTransaction();
            $tr2_s->account_id = $obligation_emp_neto_suspensions_account->id;
            $tr2_s->account_document_id = $document->id;
            $tr2_s->comment = $_statement_comment;
            $tr2_s->credit = $obligation_emp_neto_suspensions;
            $booking_transactions[] = $tr2_s;
        }
        
        //451
        if (SIMAMisc::lessThen(0, $obligation_emp_tax))
        {
            $obligation_emp_tax_account  = Account::getFromParam('accounting.codes.paychecks.obligation_emp_tax',$_year)->getLeafAccount();
            $tr3 = new AccountTransaction();
            $tr3->account_id = $obligation_emp_tax_account->id;
            $tr3->account_document_id = $document->id;
            $tr3->comment = $_statement_comment;
            $tr3->credit = $obligation_emp_tax;
            $booking_transactions[] = $tr3;
        }
        
        //4520
        if (SIMAMisc::lessThen(0, $obligation_emp_pio))
        {
            $obligation_emp_pio_account  = Account::getFromParam('accounting.codes.paychecks.obligation_emp_pio',$_year)->getLeafAccount();
            $tr4 = new AccountTransaction();
            $tr4->account_id = $obligation_emp_pio_account->id;
            $tr4->account_document_id = $document->id;
            $tr4->comment = $_statement_comment;
            $tr4->credit = $obligation_emp_pio;
            $booking_transactions[] = $tr4;
        }
        //4521
        if (SIMAMisc::lessThen(0, $obligation_emp_zdr))
        {
            $obligation_emp_zdr_account  = Account::getFromParam('accounting.codes.paychecks.obligation_emp_zdr',$_year)->getLeafAccount();
            $tr5 = new AccountTransaction();
            $tr5->account_id = $obligation_emp_zdr_account->id;
            $tr5->account_document_id = $document->id;
            $tr5->comment = $_statement_comment;
            $tr5->credit = $obligation_emp_zdr;
            $booking_transactions[] = $tr5;
        }
        //4522
        if (SIMAMisc::lessThen(0, $obligation_emp_nez))
        {
            $obligation_emp_nez_account  = Account::getFromParam('accounting.codes.paychecks.obligation_emp_nez',$_year)->getLeafAccount();
            $tr6 = new AccountTransaction();
            $tr6->account_id = $obligation_emp_nez_account->id;
            $tr6->account_document_id = $document->id;
            $tr6->comment = $_statement_comment;
            $tr6->credit = $obligation_emp_nez;
            $booking_transactions[] = $tr6;
        }
        
        
        
        //521
        if (SIMAMisc::lessThen(0, $costs_company))
        {
            $costs_company_account  = Account::getFromParam('accounting.codes.paychecks.costs_company',$_year)->getLeafAccount();
            $tr7 = new AccountTransaction();
            $tr7->account_id = $costs_company_account->id;
            $tr7->account_document_id = $document->id;
            $tr7->comment = $_statement_comment;
            $tr7->debit = $costs_company;
            $booking_transactions[] = $tr7;
        }
        
        
        //4530
        if (SIMAMisc::lessThen(0, $obligation_company_pio))
        {
            $obligation_company_pio_account  = Account::getFromParam('accounting.codes.paychecks.obligation_company_pio',$_year)->getLeafAccount();
            $tr8 = new AccountTransaction();
            $tr8->account_id = $obligation_company_pio_account->id;
            $tr8->account_document_id = $document->id;
            $tr8->comment = $_statement_comment;
            $tr8->credit = $obligation_company_pio;
            $booking_transactions[] = $tr8;
        }
        //4531
        if (SIMAMisc::lessThen(0, $obligation_company_zdr))
        {
            $obligation_company_zdr_account  = Account::getFromParam('accounting.codes.paychecks.obligation_company_zdr',$_year)->getLeafAccount();
            $tr9 = new AccountTransaction();
            $tr9->account_id = $obligation_company_zdr_account->id;
            $tr9->account_document_id = $document->id;
            $tr9->comment = $_statement_comment;
            $tr9->credit = $obligation_company_zdr;
            $booking_transactions[] = $tr9;
        }
        //4532
        if (SIMAMisc::lessThen(0, $obligation_company_nez))
        {
            $obligation_company_nez_account  = Account::getFromParam('accounting.codes.paychecks.obligation_company_nez',$_year)->getLeafAccount();
            $tr = new AccountTransaction();
            $tr->account_id = $obligation_company_nez_account->id;
            $tr->account_document_id = $document->id;
            $tr->comment = $_statement_comment;
            $tr->credit = $obligation_company_nez;
            $booking_transactions[] = $tr;
        }

        return $booking_transactions;
    }
    
    
    private static function BookingPayment(AccountDocument $document,  PaycheckPeriod $paycheck_period, Payment $payment, $_statement_comment)
    {
        $booking_transactions = [];
        
        //godina iz naloga za knjizenje
        $_year = $document->account_order->year;
        
        //451
        $obligation_emp_tax = 0;
        //4520
        $obligation_emp_pio = 0;
        //4521
        $obligation_emp_zdr = 0;
        //4522
        $obligation_emp_nez = 0;
        //4530
        $obligation_company_pio = 0;
        //4531
        $obligation_company_zdr = 0;
        //4532
        $obligation_company_nez = 0;
        
        $_check_sum = 0;
        
        foreach ($paycheck_period->paychecks as $paycheck)
        {
            $obligation_emp_tax += $paycheck->tax_empl;
            $obligation_emp_pio += $paycheck->pio_empl;
            $obligation_emp_zdr += $paycheck->zdr_empl;
            $obligation_emp_nez += $paycheck->nez_empl;
//            $obligation_company += $paycheck->amount_total;
            $obligation_company_pio += $paycheck->pio_comp;
            $obligation_company_zdr += $paycheck->zdr_comp;
            $obligation_company_nez += $paycheck->nez_comp;
            
        }
        
        $_check_sum += 
                $obligation_emp_tax 
                + $obligation_emp_pio 
                + $obligation_emp_zdr
                + $obligation_emp_nez
                + $obligation_company_pio
                + $obligation_company_zdr
                + $obligation_company_nez;
        
        if (abs(floatval($_check_sum) - floatval($payment->amount))>0.001)
        {
            $note = 'Uplata se ne poklapa sa obracunatom platom!!!!'."$_check_sum != $payment->amount";
            Yii::app()->raiseNote($note);
//            throw new SIMAWarnException('Uplata se ne poklapa sa obracunatom platom!!!!'."$_check_sum != $payment->amount");
        }
        
        //451
        if (SIMAMisc::lessThen(0, $obligation_emp_tax))
        {
            $obligation_emp_tax_account  = Account::getFromParam('accounting.codes.paychecks.obligation_emp_tax',$_year)->getLeafAccount();
            $tr3 = new AccountTransaction();
            $tr3->account_id = $obligation_emp_tax_account->id;
            $tr3->account_document_id = $document->id;
            $tr3->comment = $_statement_comment;
            $tr3->debit = $obligation_emp_tax;
            $booking_transactions[] = $tr3;
        }
        
        //4520
        if (SIMAMisc::lessThen(0, $obligation_emp_pio))
        {
            $obligation_emp_pio_account  = Account::getFromParam('accounting.codes.paychecks.obligation_emp_pio',$_year)->getLeafAccount();
            $tr4 = new AccountTransaction();
            $tr4->account_id = $obligation_emp_pio_account->id;
            $tr4->account_document_id = $document->id;
            $tr4->comment = $_statement_comment;
            $tr4->debit = $obligation_emp_pio;
            $booking_transactions[] = $tr4;
        }
        //4521
        if (SIMAMisc::lessThen(0, $obligation_emp_zdr))
        {
            $obligation_emp_zdr_account  = Account::getFromParam('accounting.codes.paychecks.obligation_emp_zdr',$_year)->getLeafAccount();
            $tr5 = new AccountTransaction();
            $tr5->account_id = $obligation_emp_zdr_account->id;
            $tr5->account_document_id = $document->id;
            $tr5->comment = $_statement_comment;
            $tr5->debit = $obligation_emp_zdr;
            $booking_transactions[] = $tr5;
        }
        //4522
        if (SIMAMisc::lessThen(0, $obligation_emp_nez))
        {
            $obligation_emp_nez_account  = Account::getFromParam('accounting.codes.paychecks.obligation_emp_nez',$_year)->getLeafAccount();
            $tr6 = new AccountTransaction();
            $tr6->account_id = $obligation_emp_nez_account->id;
            $tr6->account_document_id = $document->id;
            $tr6->comment = $_statement_comment;
            $tr6->debit = $obligation_emp_nez;
            $booking_transactions[] = $tr6;
        }
        
        //4530
        if (SIMAMisc::lessThen(0, $obligation_company_pio))
        {
            $obligation_company_pio_account  = Account::getFromParam('accounting.codes.paychecks.obligation_company_pio',$_year)->getLeafAccount();
            $tr8 = new AccountTransaction();
            $tr8->account_id = $obligation_company_pio_account->id;
            $tr8->account_document_id = $document->id;
            $tr8->comment = $_statement_comment;
            $tr8->debit = $obligation_company_pio;
            $booking_transactions[] = $tr8;
        }
        //4531
        if (SIMAMisc::lessThen(0, $obligation_company_zdr))
        {
            $obligation_company_zdr_account  = Account::getFromParam('accounting.codes.paychecks.obligation_company_zdr',$_year)->getLeafAccount();
            $tr9 = new AccountTransaction();
            $tr9->account_id = $obligation_company_zdr_account->id;
            $tr9->account_document_id = $document->id;
            $tr9->comment = $_statement_comment;
            $tr9->debit = $obligation_company_zdr;
            $booking_transactions[] = $tr9;
        }
        //4532
        if (SIMAMisc::lessThen(0, $obligation_company_nez))
        {
            $obligation_company_nez_account  = Account::getFromParam('accounting.codes.paychecks.obligation_company_nez',$_year)->getLeafAccount();
            $tr = new AccountTransaction();
            $tr->account_id = $obligation_company_nez_account->id;
            $tr->account_document_id = $document->id;
            $tr->comment = $_statement_comment;
            $tr->debit = $obligation_company_nez;
            $booking_transactions[] = $tr;
        }

        return $booking_transactions;
    }
    
}