<?php

class PPPPDComponent
{
    public static function generateTempPPPDItemsFromTempXML(TemporaryFile $tempXmlFile)
    {
        $tempPPPDItems = [];
        
        $xml = simplexml_load_file($tempXmlFile->getFullPath());
        
        $namespaces = $xml->getNameSpaces(true);
        if(empty($namespaces))
        {
            if(empty($xml->DeklarisaniPrihodi))
            {
                throw new Exception(Yii::t('AccountingModule.PPPPD', 'InvalidXML'));
            }
            $xml_DeklarisaniPrihodi_children = $xml->DeklarisaniPrihodi->children();
        }
        else
        {
            $namespace = key($namespaces);
            
            $xml_children = $xml->children($namespaces[$namespace]);
            $xml_DeklarisaniPrihodi_children = $xml_children->DeklarisaniPrihodi->children($namespaces[$namespace]);
        }
        
        foreach ($xml_DeklarisaniPrihodi_children as $child)
        {            
            $order_number = $child->RedniBroj->__toString();
            $indentification_type = $child->VrstaIdentifikatoraPrimaoca->__toString();
            $indentification_value = $child->IdentifikatorPrimaoca->__toString();
            $firstname = $child->Ime->__toString();
            $lastname = $child->Prezime->__toString();
            $SVP = $child->SVP->__toString();
            $municipality_code = $child->OznakaPrebivalista->__toString();
            $number_of_calendar_days = $child->BrojKalendarskihDana->__toString();
            $effective_hours = $child->BrojEfektivnihSati->__toString();
            $month_hours = $child->MesecniFondSati->__toString();
            $bruto = $child->Bruto->__toString();
            $tax_base = $child->OsnovicaPorez->__toString();
            $tax_empl = $child->Porez->__toString();
            $doprinosi_base = $child->OsnovicaDoprinosi->__toString();
            $pio = $child->PIO->__toString();
            $zdr = $child->ZDR->__toString();
            $nez = $child->NEZ->__toString();
            $pio_ben = $child->PIOBen->__toString();
            
            $tempPPPDItem = new PPPPDItem();
            $tempPPPDItem->svp = $SVP;
            $tempPPPDItem->order_number = $order_number;
            $tempPPPDItem->indentification_type = $indentification_type;
            $tempPPPDItem->indentification_value = $indentification_value;
            $tempPPPDItem->lastname = $lastname;
            $tempPPPDItem->firstname = $firstname;
            $tempPPPDItem->municipality_code = $municipality_code;
            $tempPPPDItem->number_of_calendar_days = $number_of_calendar_days;
            $tempPPPDItem->effective_hours = $effective_hours;
            $tempPPPDItem->month_hours = $month_hours;
            $tempPPPDItem->bruto = $bruto;
            $tempPPPDItem->tax_base = $tax_base;
            $tempPPPDItem->tax_empl = $tax_empl;
            $tempPPPDItem->doprinosi_base = $doprinosi_base;
            $tempPPPDItem->pio = $pio;
            $tempPPPDItem->zdr = $zdr;
            $tempPPPDItem->nez = $nez;
            $tempPPPDItem->pio_ben = $pio_ben;
            $tempPPPDItem->mfp1 = $paycheck_xml->mfp1;
            $tempPPPDItem->mfp2 = $paycheck_xml->mfp2;
            $tempPPPDItem->mfp3 = $paycheck_xml->mfp3;
            $tempPPPDItem->mfp4 = $paycheck_xml->mfp4;
            $tempPPPDItem->mfp5 = $paycheck_xml->mfp5;
            $tempPPPDItem->mfp6 = $paycheck_xml->mfp6;
            $tempPPPDItem->mfp7 = $paycheck_xml->mfp7;
            $tempPPPDItem->mfp8 = $paycheck_xml->mfp8;
            $tempPPPDItem->mfp9 = $paycheck_xml->mfp9;
            $tempPPPDItem->mfp10 = $paycheck_xml->mfp10;
            $tempPPPDItem->mfp11 = $paycheck_xml->mfp11;
            $tempPPPDItem->mfp12 = $paycheck_xml->mfp12;
            
            $tempPPPDItems[] = $tempPPPDItem;
        }
        
        return $tempPPPDItems;
    }
    
    public static function GenerateDiffForPPPPDs(PPPPD $ppppd1, PPPPD $ppppd2)
    {
        $diffs = [];
        $diff_items = [];
        
        $ppppd1_items = $ppppd1->items;
        $ppppd2_items = $ppppd2->items;
        
        $ppppd1_items_count = count($ppppd1_items);
        $ppppd2_items_count = count($ppppd2_items);
        
        $columns = [
            'order_number', 'lastname', 'firstname', 'municipality_code', 'number_of_calendar_days', 'effective_hours', 'month_hours', 'bruto', 'tax_base', 'tax_empl', 
            'doprinosi_base', 'pio', 'zdr', 'nez', 'pio_ben', 'mfp1', 'mfp2', 'mfp3', 'mfp4', 'mfp5', 'mfp6', 'mfp7', 'mfp8', 'mfp9', 'mfp10', 'mfp11', 'mfp12'
        ];
        
        $eventSourceStepsSum = 0;
        $eventSourceSteps = 0;
        if($ppppd1_items_count > 0)
        {
            $eventSourceSteps = (70/$ppppd1_items_count);
        }
        foreach($ppppd1_items as $ppppd1_item)
        {
            $eventSourceStepsSum += $eventSourceSteps;
            Yii::app()->controller->sendUpdateEvent($eventSourceStepsSum);
            
            $have_item1_in_array2 = false;
            
            foreach($ppppd2_items as $ppppd2_item)
            {
                if(
                        $ppppd1_item->svp === $ppppd2_item->svp
                        && $ppppd1_item->indentification_type === $ppppd2_item->indentification_type
                        && $ppppd1_item->indentification_value === $ppppd2_item->indentification_value
                )
                {
                    $columns_for_modify = [];
                    foreach($columns as $column)
                    {
                        if($ppppd1_item->$column !== $ppppd2_item->$column)
                        {
                            $columns_for_modify[$column] = [
                                'old_value' => $ppppd1_item->$column,
                                'new_value' => $ppppd2_item->$column
                            ];
                        }
                    }
                    
                    if(!empty($columns_for_modify))
                    {
                        $diff_items[] = [
                            'status' => 'MODIFY', //status jos moze biti ADD, REMOVE, SAME, MODIFY
                            'columns' => $columns_for_modify
                        ];
                    }
                    
                    $have_item1_in_array2 = true;
                    break;
                }
            }
            
            if($have_item1_in_array2 === false)
            {
                $columns_for_remove = [];
                foreach($columns as $column)
                {
                    $columns_for_remove[$column] = [
                        'old_value' => $ppppd1_item->$column
                    ];
                }
                
                $diff_items[] = [
                    'status' => 'REMOVE', //status jos moze biti ADD, REMOVE, SAME, MODIFY
                    'columns' => $columns_for_remove
                ];
            }
        }
        Yii::app()->controller->sendUpdateEvent(70);
        
        if($ppppd2_items_count > 0)
        {
            $eventSourceSteps = (30/$ppppd2_items_count);
        }
        foreach($ppppd2_items as $ppppd2_item)
        {
            $eventSourceStepsSum += $eventSourceSteps;
            Yii::app()->controller->sendUpdateEvent($eventSourceStepsSum);
            
            $have_item2_in_array1 = false;
            
            foreach($ppppd1_items as $ppppd1_item)
            {
                if(
                        $ppppd1_item->svp === $ppppd2_item->svp
                        && $ppppd1_item->indentification_type === $ppppd2_item->indentification_type
                        && $ppppd1_item->indentification_value === $ppppd2_item->indentification_value
                )
                {
                    $have_item2_in_array1 = true;
                }
            }
            
            if($have_item1_in_array2 === false)
            {
                $columns_for_add = [];
                foreach($columns as $column)
                {
                    $columns_for_add[$column] = [
                        'new_value' => $ppppd1_item->$column
                    ];
                }
                
                $diff_items[] = [
                    'status' => 'ADD', //status jos moze biti ADD, REMOVE, SAME, MODIFY
                    'columns' => $columns_for_add
                ];
            }
        }
        
        if(!empty($diff_items))
        {
            $diffs[] = [
                'type' => 'group',
                'group_id' => $ppppd1->id.'_'.$ppppd2->id,
                'group_name' => $ppppd1->DisplayName.' -> '.$ppppd2->DisplayName,
                'columns' => $columns,
                'items' => $diff_items
            ];
        }
        
        return $diffs;
    }
}
