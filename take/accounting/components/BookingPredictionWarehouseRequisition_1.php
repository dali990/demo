<?php

/**
 * Staticka klasa u kojoj se predvidjaju sva proknjizavanja
 */
class BookingPredictionWarehouseRequisition
{
    public static function Booking(AccountDocument $document, WarehouseTransfer $req, $_statement_comment)
    {
        $booking_transactions = [];
        /**
         * PRETPOSTAVKA JE DA IMA NA STANJU
         */
        $year = $document->account_order->year;
        $booking_materials_type = $year->param('accounting.booking_materials_type');
        switch ($booking_materials_type)
        {
            case 'PLANNED_PRICE':
                $booking_transactions = self::BookingRequisitionPlannedPrice($document, $req, $_statement_comment);
                break;
            case 'AVERAGE_PRICE':
                $booking_transactions = self::BookingRequisitionAvaragePrice($document, $req, $_statement_comment);
                break;
        }
        
        return $booking_transactions;
    }
    
    /**
     * Po prosecnoj ceni
     * @param type $document
     * @param WarehouseRequisitionLocation $req
     * @param type $_statement_comment
     * @return type
     */
    private static function BookingRequisitionPlannedPrice($document, WarehouseTransfer $req, $_statement_comment)
    {
        /**
         * trebovanje po planskoj ceni
         */
        
        $year = $document->account_order->year;

        $_warehouse_from_main_account_temp = $req->warehouse_from->getMainAccount($year);
        $_warehouse_from_correction_account_temp = $req->warehouse_from->getCorrectionAccount($year);
        $items = $req->warehouse_transfer_items;
        
        //TODO: common_cost_ration cemo trenutno da zaboravimo, to treba da se impl,ementira u proizvodnji (dodavanje troskova)
//        $common_cost_ratio = Yii::app()->configManager->get('accounting.warehouse.production_common_cost_percent');
        //planska cena se smesta u out jer ne sluzi sustinski nicemu
        foreach ($items as $item)
        {
            $item->value_per_unit_out = $req->warehouse_from->materialPrice($item->warehouse_material,$req,true);
//            $item->corr_value = $req->warehouse_from->materialPrice($item->warehouse_material,$req,true);
//            $item->value_per_unit_out = $item->value_per_unit;// * ($common_cost_ratio+100)/100;
            $item->save(false);
//            $sum += $item->warehouse_material->predicted_value * $item->quantity;
        }
        $planned_sum = $req->sum_value_out; //planska cena
//        withDCCurrents
        
        $_order_date = Day::getByDate($document->account_order->date)->prev();
        $_order_date_display = $_order_date->day_date;
        
        $_warehouse_from_main_account = Account::model()
                ->withDCCurrents($year->id,['end_date'=>$_order_date_display])
//                ->withDCCurrents($year->id,['end_date'=>$req->transfer_time])
                ->findByPk($_warehouse_from_main_account_temp->id);
        $_warehouse_from_correction_account = Account::model()
                ->withDCCurrents($year->id,['end_date'=>$_order_date_display])
//                ->withDCCurrents($year->id,['end_date'=>$req->transfer_time])
                ->findByPk($_warehouse_from_correction_account_temp->id);
        
        $current_saldo = $_warehouse_from_main_account->saldo;
        $correction_curr_saldo = $_warehouse_from_correction_account->saldo;
        if ($current_saldo < $planned_sum )
        {
//            $correction_curr_saldo_display = SIMAHtml::number_format($correction_curr_saldo);
            $current_saldo_display = SIMAHtml::number_format($current_saldo);
            $sum_display = SIMAHtml::number_format($planned_sum);
            Yii::app()->raiseNote("U magacinu ima manje stvari nego sto je predvidjeno: $current_saldo_display < $sum_display ");
//            throw new SIMAWarnException("U magacinu ima manje stvari nego sto je predvidjeno: $current_saldo < $sum ");
        }

        $percent = $planned_sum/$current_saldo;    
        $correction = $percent * $correction_curr_saldo;
        
        $items2 = WarehouseTransferItem::model()->findAllByAttributes([
            'warehouse_transfer_id' => $req->id
        ]);
        
        //cena je jednaka planska plus odstupanje
        foreach ($items2 as $item)
        {
            $item->corr_value_per_unit = ($correction / ($req->warehouse_transfer_items_count * $item->quantity));
            $item->value_per_unit      = $item->value_per_unit_out + $item->corr_value_per_unit;
            $item->save(false);
        }
        $req->save();

        
        return self::pure_booking($document, $req, $_statement_comment, $planned_sum, $correction);
    }
    
    private static function BookingRequisitionAvaragePrice($document, WarehouseTransfer $req, $_statement_comment)
    {
        
        /**
         * trebovanje po planskoj ceni
         */
//        $items = $req->warehouse_transfer_items;
//
//        $common_cost_ratio = Yii::app()->configManager->get('accounting.warehouse.production_common_cost_percent');
////        $sum = 0;
//        foreach ($items as $item)
//        {
//            $_avg_price = $req->warehouse_from->materialAvaragePrice(
//                    $item->warehouse_material,
//                    $req->transfer_time,
//                    true, //samo trebovanje se ne uzima u obzir
//                    $req->id
//                    );
////            $sum += $_avg_price * $item->quantity;
////            if (!SIMAMisc::areEqual($_avg_price, $item->value_per_unit,0.00001))
////            {
////                error_log($item->warehouse_transfer->DisplayName);
////                error_log($item->warehouse_material->DisplayName);
////                error_log("$item->value_per_unit != $_avg_price");
////            }
//            $item->value_per_unit = $_avg_price;
//            $item->value_per_unit_out = $_avg_price * ($common_cost_ratio+100)/100;
//            $item->save();
//        }
        $sum = $req->sum_value;
        
        return self::pure_booking($document, $req, $_statement_comment, $sum);
    }
    
    private static function pure_booking($document, $req, $_statement_comment, $planned_sum, $correction=0)
    {

        $booking_transactions = [];
        $year = $document->account_order->year;
        $_warehouse_from_main_account = $req->warehouse_from->getMainAccount($year);
        $_warehouse_from_correction_account = $req->warehouse_from->getCorrectionAccount($year);
        
        /**
         *  511 | 101
         *      | 1019
         * knjizenje troska materijala
         */
        $job_order = $req->warehouse_to->cost_location->findForAnalytics(); // ukoliko je null, nije postavljen cost_location, pa nema analitiku
        $job_order_account_base    = Account::getFromParam('accounting.codes.warehouse.material_expenses',$year);
        $job_order_account = $job_order_account_base->getLeafAccount($job_order);
        $tr1 = new AccountTransaction();
        $tr1->account_id = $job_order_account->id;
        $tr1->account_document_id = $document->id;
        $tr1->comment = $_statement_comment;
        $tr1->debit = $planned_sum + $correction;
        $booking_transactions[] = $tr1;

        $tr2 = new AccountTransaction();
        $tr2->account_id = $_warehouse_from_main_account->id;
        $tr2->account_document_id = $document->id;
        $tr2->comment = $_statement_comment;
        $tr2->credit = $planned_sum;
        $booking_transactions[] = $tr2;

        if (abs($correction)>0)
        {
            $tr3 = new AccountTransaction();
            $tr3->account_id = $_warehouse_from_correction_account->id;
            $tr3->account_document_id = $document->id;
            $tr3->comment = $_statement_comment;
            $tr3->credit = $correction;
            $booking_transactions[] = $tr3;
        }

        /**
         * 9500 | 9020
         */
        $book_cost_location = null;
        $_book_nine_by_cost_location = Yii::app()->configManager->get('accounting.warehouse.book_nine_by_cost_location');
        if ($_book_nine_by_cost_location)
        {
            $book_cost_location = $job_order;
        }
        
        $_cost_carier    = Account::getFromParam('accounting.codes.nine.cost_cariers',$year)->getLeafAccount($book_cost_location);
        $tr4 = new AccountTransaction();
        $tr4->account_id = $_cost_carier->id;
        $tr4->account_document_id = $document->id;
        $tr4->comment = $_statement_comment;
        $tr4->debit = $planned_sum + $correction;
        $booking_transactions[] = $tr4;
        
        $_excange_expences    = Account::getFromParam('accounting.codes.nine.excange_expences',$year)->getLeafAccount($book_cost_location);
        $tr5 = new AccountTransaction();
        $tr5->account_id = $_excange_expences->id;
        $tr5->account_document_id = $document->id;
        $tr5->comment = $_statement_comment;
        $tr5->credit = $planned_sum + $correction;
        $booking_transactions[] = $tr5;
        
        
        /**
         * 9500 | 9026
         */
        $common_cost_ratio = Yii::app()->configManager->get('accounting.warehouse.production_common_cost_percent');
        $common_cost_sum = ($planned_sum + $correction) * $common_cost_ratio/100;
        
        if (abs($common_cost_sum)>0)
        {
            $tr6 = new AccountTransaction();
            $tr6->account_id = $_cost_carier->id;
            $tr6->account_document_id = $document->id;
            $tr6->comment = $_statement_comment;
            $tr6->debit = $common_cost_sum;
            $booking_transactions[] = $tr6;

            $_excange_expences_common_costs    = 
                Account::getFromParam('accounting.codes.nine.excange_expences_common_costs',$year)
                    ->getLeafAccount($book_cost_location);
            $tr7 = new AccountTransaction();
            $tr7->account_id = $_excange_expences_common_costs->id;
            $tr7->account_document_id = $document->id;
            $tr7->comment = $_statement_comment;
            $tr7->credit = $common_cost_sum;
            $booking_transactions[] = $tr7;
        }
        
        return $booking_transactions;
    }
}