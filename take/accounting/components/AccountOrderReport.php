<?php
class AccountOrderReport extends SIMAReport
{
    protected function getModuleName()
    {
        return 'accounting';
    }
    
    public function getPDF()
    {
        $account_order = $this->params['account_order'];
        $template_id = Yii::app()->configManager->get('accounting.account_order_template', false);
        if(!empty($template_id))
        {
            $templated_file = TemplatedFile::model()->with('templated_file_data')->findByPk($account_order->id);
            if ($templated_file === null)
            {
                $template_file_id = TemplateController::setTemplate($account_order->id, $template_id);
                $templated_file = TemplatedFile::model()->findByPkWithCheck($template_file_id); 
            }  
            return $templated_file->createTempPdf();
        }
        else 
        {
            return parent::getPDF();
        }
    }
    
    protected function getHTMLContent()
    {
        $account_order = $this->params['account_order'];
        
        $show_booked_timestamp = Yii::app()->configManager->get('accounting.show_booked_timestamp', false);
        $show_transaction_comments = Yii::app()->configManager->get('accounting.show_transaction_comments', false);
        if ($show_transaction_comments)
        {
            $debit_column = 'account_debit_display';
            $credit_column = 'account_credit_display';
            $comment_column = 'comment';
            $account_column_width = '15%';
            $comment_column_width = '20%';
        }
        else
        {
            $debit_column = 'account_debit_display_just_number';
            $credit_column = 'account_credit_display_just_number';
            $comment_column = 'account_just_name';
            $account_column_width = '10%';
            $comment_column_width = '30%';
        }
        
        $i = 1;
        $account_transactions_cnt = count($account_order->account_transactions);
        $max_while_cycle_exec_time_seconds = ini_get('max_execution_time');
        $html = "";
        foreach ($account_order->account_transactions as $account_transaction)  
        {
            set_time_limit($max_while_cycle_exec_time_seconds);
            if (!empty($this->params['update_func']))
            {
                call_user_func($this->params['update_func'], round(($i/$account_transactions_cnt)*100, 0, PHP_ROUND_HALF_DOWN));
            }  
            $html .= "<tr class='row'>
                <td width='5%' class='align-center'>{$account_transaction->getAttributeDisplay('account_order_order')}</td>
                <td width='$account_column_width'>{$account_transaction->getAttributeDisplay($debit_column)}</td>
                <td width='$account_column_width'>{$account_transaction->getAttributeDisplay($credit_column)}</td>
                <td width='$comment_column_width'>{$account_transaction->getAttributeDisplay($comment_column)}</td>
                <td width='15%' class='align-right'>{$account_transaction->getAttributeDisplay('debit')}</td>
                <td width='15%' class='align-right'>{$account_transaction->getAttributeDisplay('credit')}</td>
                <td width='15%' class='align-right'>{$account_transaction->getAttributeDisplay('saldo')}</td>
            </tr>";
            $i++;
        }
                
        return [
            'html' => $this->render('html', [
                'account_order' => $account_order,
                'html' => $html,
                'company_logo_path' => Yii::app()->company->getLogoImagePath(),
                'debit_column' => $debit_column,
                'credit_column' => $credit_column,
                'comment_column' => $comment_column,
                'account_column_width' => $account_column_width,
                'comment_column_width' => $comment_column_width,
                'show_booked_timestamp' => $show_booked_timestamp
            ], true, false),
            'css_file_name' => 'css'
        ];
    }
}