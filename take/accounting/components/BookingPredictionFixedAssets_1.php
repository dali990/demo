<?php

/**
 * Staticka klasa u kojoj se predvidjaju sva proknjizavanja
 */
class BookingPredictionFixedAssets
{
    
    public static function Booking(AccountDocument $document)
    {
        $booking_transactions = [];
        
        $_statement_comment = $document->DisplayName;
        
        if (!is_null($document->file->fixed_assets_depreciation))
        {
            $booking_transactions = self::BookingDepreciation($document, $document->file->fixed_assets_depreciation, $_statement_comment);
        }
        
        if (!is_null($document->file->fixed_asset_deactivation_decision))
        {
            $booking_transactions = self::BookingDeactivation($document, $document->file->fixed_asset_deactivation_decision, $_statement_comment);
        }

        return $booking_transactions;
    }
    
    public static function BookingDepreciation($document, FixedAssetsDepreciation $_fad, $_statement_comment)
    {
        $booking_transactions = [];
        
        $year = $document->account_order->year;
        /**
         * calculate difference
         */
        $accounts = [];
        $total_sum = 0;
        foreach ($_fad->fixed_assets_depreciation_items as $_item)
        {
            $account = $_item->accounting_fixed_asset->fixed_asset->getAccountForBooking();
            if (is_null($account))
            {
                continue;
            }
            $account = $account->forYear($year);
            if (!isset($accounts[$account->id]))
            {
                $accounts[$account->id] = [
                    'sum' => 0,
                    'account' => $account
                ];
            }
            
            $accounts[$account->id]['sum'] += $_item->depreciation;
            $total_sum += $_item->depreciation;
        }
        
        /**
         * add transactions
         */
        
        $fa_depriciation_expense = Account::getFromParam('accounting.codes.fixed_assets.depreciation_expenses_account', $year);
        $tr1 = new AccountTransaction();
        $tr1->account_id = $fa_depriciation_expense->id;
        $tr1->account_document_id = $document->id;
        $tr1->comment = $_statement_comment;
        $tr1->debit = $total_sum;
        $booking_transactions[] = $tr1;
        
        foreach ($accounts as $acc)
        {
            $tr2 = new AccountTransaction();
            $tr2->account_id = $acc['account']->id;
            $tr2->account_document_id = $document->id;
            $tr2->comment = $_statement_comment;
            $tr2->credit = $acc['sum'];
            $booking_transactions[] = $tr2;
        }
        
        return $booking_transactions;
    }
    
    
    public static function BookingDeactivation($document, FixedAssetDeactivationDecision $_fadd, $_statement_comment)
    {
        $booking_transactions = [];
        
        $year = $document->account_order->year;
        /**
         * calculate difference
         */
        $deprec_sum_from_year_start = 0;
        $deprec_sum = 0;
        $cost_sum = 0;
        $accounts = [];
        foreach ($_fadd->fixed_assets as $_fixed_asset)
        {
            $fixed_asset_to_deactivation = FixedAssetToDeactivationDecision::model()->findByAttributes([
                'fixed_asset_id' => $_fixed_asset->id,
                'deactivation_decision_id' => $_fadd->id
            ]);
            
            $deprec_sum_from_year_start += $fixed_asset_to_deactivation->depreciation_value_curr_year;
            
            $deprec_sum += ($fixed_asset_to_deactivation->depreciation_value_prev_years + $fixed_asset_to_deactivation->depreciation_value_curr_year);

            $cost_sum += ($fixed_asset_to_deactivation->deactivation_value - ($fixed_asset_to_deactivation->depreciation_value_prev_years + $fixed_asset_to_deactivation->depreciation_value_curr_year));

            $account = $_fixed_asset->getAccountForBooking();
            if (is_null($account))
            {
                continue;
            }
            $account = $account->forYear($year);
            if (!isset($accounts[$account->id]))
            {
                $accounts[$account->id] = [
                    'sum' => 0,
                    'account' => $account
                ];
            }
            $accounts[$account->id]['sum'] += $fixed_asset_to_deactivation->deactivation_value;
        }
        
        /**
         * add transactions
         */
        if (!SIMAMisc::areEqual($deprec_sum_from_year_start, 0.0))
        {
            $depreciation_account = Account::getFromParam('accounting.codes.fixed_assets.depreciation_expenses_account', $year);
            $tr1 = new AccountTransaction();
            $tr1->account_id = $depreciation_account->id;
            $tr1->account_document_id = $document->id;
            $tr1->comment = $_statement_comment;
            $tr1->debit = $deprec_sum_from_year_start;
            $booking_transactions[] = $tr1;
            
            $deactivation_account = Account::getFromParam('accounting.codes.fixed_assets.deactivation_account', $year);
            $tr2 = new AccountTransaction();
            $tr2->account_id = $deactivation_account->id;
            $tr2->account_document_id = $document->id;
            $tr2->comment = $_statement_comment;
            $tr2->credit = $deprec_sum_from_year_start;
            $booking_transactions[] = $tr2;
        }
        
        if (!SIMAMisc::areEqual($deprec_sum, 0.0))
        {
            $deactivation_account = Account::getFromParam('accounting.codes.fixed_assets.deactivation_account', $year);
            $tr3 = new AccountTransaction();
            $tr3->account_id = $deactivation_account->id;
            $tr3->account_document_id = $document->id;
            $tr3->comment = $_statement_comment;
            $tr3->debit = $deprec_sum;
            $booking_transactions[] = $tr3;
        }
        
        if (!SIMAMisc::areEqual($cost_sum, 0.0))
        {
            $deactivation_cost_account = Account::getFromParam('accounting.codes.fixed_assets.deactivation_cost_account', $year);
            $tr4 = new AccountTransaction();
            $tr4->account_id = $deactivation_cost_account->id;
            $tr4->account_document_id = $document->id;
            $tr4->comment = $_statement_comment;
            if ($cost_sum > 0)
            {
                $tr4->debit = $cost_sum;
            }
            else
            {
                $tr4->credit = $cost_sum;
            }
            $booking_transactions[] = $tr4;
        }
        
        foreach ($accounts as $acc)
        {
            $tr5 = new AccountTransaction();
            $tr5->account_id = $acc['account']->id;
            $tr5->account_document_id = $document->id;
            $tr5->comment = $_statement_comment;
            $tr5->credit = $acc['sum'];
            $booking_transactions[] = $tr5;
        }
        
        return $booking_transactions;
    }

}