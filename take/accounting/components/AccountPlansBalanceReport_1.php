<?php

abstract class AccountPlansBalanceReport
{
    protected $curr_year = null;
    protected $prev_year = null;
    
    protected $xml_values = [];
    protected $aop_codes_with_original_value = []; //niz aop-a za cije vrednosti se ne radi deljenje sa 1000 i zaokruzivanje, vec se ispisuju originalne vrednosti

    private $double_check_failures_for_aops = [];
    
    abstract public function getReportName() : string;

    abstract protected function getAOPCodes() : array;
    
    abstract protected function getXMLValueFromAccount(string $aop_code, string $account_code) : array;
    abstract protected function getXMLValueFromAccounts(string $aop_code, array $account_codes, array $aop_params) : array;
    abstract protected function getXMLValueFromAOPs(string $aop_code, array $aops, array $aop_params) : array;
    
    public function __construct(Year $curr_year)
    {
        $this->curr_year = $curr_year;
        $this->prev_year = $curr_year->prev();
    }
    
    public function getXML(bool $return_temp_file = true)
    {
        if ($return_temp_file === true)
        {
            return new TemporaryFile($this->getXMLContent(), null, 'xml');
        }
        else
        {
            return $this->getXMLContent();
        }
    }
    
    public function getXMLContent() : string
    {
        $this->calcXMLValues();

        return $this->render('xml_report', [
            'report_name' => $this->getReportName(),
            'xml_values' => $this->xml_values,
            'aop_codes_with_original_value' => $this->aop_codes_with_original_value
        ], true);
    }
    
    protected function calcXMLValues()
    {
        $this->double_check_failures_for_aops = [];
        $i = 1;
        $aop_codes = $this->getAOPCodes();
        $aop_codes_cnt = count($aop_codes);
        foreach($aop_codes as $aop_code => $aop_params)
        {
            $percent = round(($i/$aop_codes_cnt)*100, 0, PHP_ROUND_HALF_DOWN);
            Yii::app()->controller->sendUpdateEvent($percent);
            
            $this->xml_values = array_merge($this->xml_values, $this->getXMLValue($aop_code, $aop_params));
            
            $i++;
        }
        
        if (!empty($this->double_check_failures_for_aops))
        {
            $aop_codes_string = implode(', ', $this->double_check_failures_for_aops);
            Yii::app()->controller->raiseNote("Duplom proverom nisu dobijene iste vrednosti za sledeće AOP kodove: $aop_codes_string");
        }
    }
    
    private function getXMLValue(string $aop_code, $aop_params) : array
    {
        $aop_values = [];

        if (is_callable($aop_params))
        {
            $aop_values = call_user_func($aop_params);
        }
        else if (is_array($aop_params))
        {
            if (isset($aop_params['double_check']))
            {
                $aop_values = $this->getXMLValue($aop_code, $aop_params['double_check'][0]);
                $aop_values1 = $this->getXMLValue($aop_code, $aop_params['double_check'][1]);
                if ($aop_values !== $aop_values1)
                {
                    array_push($this->double_check_failures_for_aops, $aop_code);
                }
            }
            else
            {
                $aop_values = $this->getXMLValueFromArray($aop_code, $aop_params);
            }

            if (isset($aop_params['only_positive']))
            {
                $aop_values = $this->getXMLValuePositive($aop_values);
            }

            if (isset($aop_params['only_negative']))
            {
                $aop_values = $this->getXMLValueNegative($aop_values);
            }
        }
        else
        {
            $aop_values = $this->getXMLValueFromAccount($aop_code, $aop_params);
        }
        
        return $aop_values;
    }
    
    private function getXMLValueFromArray(string $aop_code, array $aop_params)
    {
        $is_aop_value = isset($aop_params['type']) && $aop_params['type'] === 'AOP';

        $aop_plus_values = [];
        if (isset($aop_params['plus']))
        {
            if ($is_aop_value)
            {
                $aop_plus_values = $this->getXMLValueFromAOPs($aop_code, $aop_params['plus'], $aop_params);
            }
            else
            {
                $aop_plus_values = $this->getXMLValueFromAccounts($aop_code, $aop_params['plus'], $aop_params);
            }
        }
        
        $aop_minus_values = [];
        if (isset($aop_params['minus']))
        {
            if ($is_aop_value)
            {
                $aop_minus_values = $this->getXMLValueFromAOPs($aop_code, $aop_params['minus'], $aop_params);
            }
            else
            {
                $aop_minus_values = $this->getXMLValueFromAccounts($aop_code, $aop_params['minus'], $aop_params);
            }
        }
        
        $aop_values = [];
        foreach ($aop_plus_values as $key => $value) 
        {
            $new_value = $value;
            if (isset($aop_minus_values[$key]))
            {
                $new_value -= $aop_minus_values[$key];
            }
            $aop_values[$key] = $new_value;
        }
        
        return $aop_values;
    }
    
    protected function getAccountForYear(string $account_code, Year $year)
    {
        if ($account_code === '')
        {
            return null;
        }

        return Account::model()->find([
            'model_filter' => [
                'code' => $account_code,
                'year' => [
                    'ids' => $year->id
                ],
                'scopes' => [
                    'withDCCurrents' => $year->id
                ]
            ]
        ]);
    }
    
    private function getXMLValuePositive(array $aop_values) : array
    {
        $only_positive_aop_values = [];
        foreach ($aop_values as $key => $value)
        {
            $only_positive_aop_values[$key] = $value < 0 ? 0 : $value;
        }
        
        return $only_positive_aop_values;
    }
    
    private function getXMLValueNegative(array $aop_values) : array
    {
        $only_negative_aop_values = [];
        foreach ($aop_values as $key => $value)
        {
            $only_negative_aop_values[$key] = $value >= 0 ? 0 : $value;
        }
        
        return $only_negative_aop_values;
    }
    
    private function render(string $view, array $data = [], bool $return = false)
    {
        $views_path = 'accounting.views.accountPlans';

        if ($return === true)
        {
            return Yii::app()->controller->renderPartial("$views_path.$view", $data, $return, false);
        }
        
        Yii::app()->controller->renderPartial("$views_path.$view", $data, $return, false);
    }
}
