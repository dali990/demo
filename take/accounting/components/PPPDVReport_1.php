<?php

class PPPDVReport extends SIMAReport
{
    protected function getModuleName()
    {
        return 'accounting';
    }
    
    protected function getHTMLContent()
    {
        if (empty($this->params['month_id']))
        {
            throw new SIMAException(Yii::t('AccountingModule.PPPDVReport', 'NoMonthId'));
        }
        
        $month = Month::model()->findByPkWithCheck($this->params['month_id']);
        $this->download_name = "PPPDV_" .  $month->month ."_" . $month->year .'.pdf';
        $popdv = new POPDVExport($month);//getPPPDVParamValue
        $vat_return = $popdv->getPPPDVParamValue('vat_return');
        return [
            'html' => $this->render('html', [
                'month' => $month,
                'popdv' => $popdv,
                'vat_return' => $vat_return,
            ], true, false),
            'css_file_name' => 'css'
        ];
    }
}

