<?php

class WorkBillItems extends SIMAWork
{   
    public function run(array $params){
        foreach ($params as $param)
        {
            $this->updateOrderOfBillItems($param);
        }
    }
    
    private function updateOrderOfBillItems($param){
        $bill_id = !empty($param['bill_id']) ? $param['bill_id'] : -1;
        $bill_items_ids_with_new_orders = !empty($param['bill_items_ids_with_new_orders']) ? $param['bill_items_ids_with_new_orders'] : [];
        
        $transaction = Yii::app()->db->beginTransaction();
            try
            {
                $bill_items = BillItem::model()->findAll([
                    'model_filter' => [
                        'scopes' => ['byOrder'],
                        'bill' => [
                            'ids' => $bill_id
                        ]
                    ]
                ]);
                
                if(!empty($bill_items_ids_with_new_orders))
                {
                    //Prepakivanje niza stavki ako bi se obezbedilo da one stavke koje treba da se prepakuju budu na kraju niza
                    foreach ($bill_items as $key => $bill_item)
                    {
                        $new_position = $this->newPositionOfBillItem($bill_item->id, $bill_items_ids_with_new_orders);
                        if(!is_null($new_position) && $new_position > $key)
                        {
                            array_push($bill_items, $bill_item);
                            unset($bill_items[$key]);
                        }
                    }
                    
                    //Pakovanje novog order-ovanog niza id-jeva stavki racuna
                    $ordered_bill_items = [];
                    foreach ($bill_items as $bill_item)
                    {
                        $new_position = $this->newPositionOfBillItem($bill_item->id, $bill_items_ids_with_new_orders);
                        if (!is_null($new_position))
                        {
                            array_splice($ordered_bill_items, $new_position, 0, [$bill_item]);
                        }
                        else
                        {
                            array_push($ordered_bill_items, $bill_item);
                        }
                    }
                }
                else 
                {
                    $ordered_bill_items = $bill_items;
                }

                //Update order-a stavki racuna
                Yii::app()->db->createCommand('ALTER TABLE accounting.bill_items DROP CONSTRAINT bill_items_bill_id_order_key;')->queryAll();
                
                $new_order = 1;
                foreach ($ordered_bill_items as $ordered_bill_item)
                {
                    if(intval($ordered_bill_item->order) !== intval($new_order))
                    {
                        $ordered_bill_item->scenario = 'update_order_from_after_work';
                        $ordered_bill_item->order = $new_order;
                        $ordered_bill_item->save();
                    }
                    $new_order++;
                }
                
                Yii::app()->db->createCommand('ALTER TABLE accounting.bill_items ADD CONSTRAINT bill_items_bill_id_order_key UNIQUE (bill_id, "order");')->queryAll();
            }
            catch(Exception $e)
            {
                $transaction->rollback();
                throw $e;
            }
        $transaction->commit();
    }
    
    private function newPositionOfBillItem($bill_item_id, $bill_items_ids_with_orders) {
        $new_position = null;
        foreach ($bill_items_ids_with_orders as $bill_item_id_with_order)
        {
            if(intval($bill_item_id_with_order["bill_item_id"]) === intval($bill_item_id))
            {
                $new_position = intval($bill_item_id_with_order["new_order"]) - 1;
                break;
            }
        }
        return $new_position;
    } 
    
}

