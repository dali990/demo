<?php
class AccountingPartnerReport extends SIMAReport
{
    protected $is_buyers;
    protected $is_suppliers;
    protected $date;
    protected $update_func = null;
    protected $css_file_name = 'css.css';
    
    protected function getModuleName()
    {
        return 'accounting';
    }
    
    public function __construct($is_buyers, $is_suppliers, $date, $update_func)
    {  
        $this->is_buyers = $is_buyers;
        $this->is_suppliers = $is_suppliers;
        $this->date = $date;
        $this->update_func = $update_func;
        
        parent::__construct();

    }
    
    protected function getHTMLContent()
    {
        if(!empty($this->update_func))
        {
            call_user_func($this->update_func, 1);
        }
        
        $html = '';
        $buyers_ids = $this->is_buyers ? $this->getBuyers() : [];
        $suppliers_ids = $this->is_suppliers ? $this->getSuppliers() : [];
        $bills_count = count($buyers_ids) + count($suppliers_ids);

        if(!empty($buyers_ids))
        {
            $html .= "<p2>Kupci</p2>";
            $html .= "<table id='table'>
                <tr>
                    <th width='3%'>R.br.</th>
                    <th width='36%'>Naziv komitenta</th>
                    <th width='15%'>Ukupno potraživanje</th>
                    <th width='15%'>Dospela potraživanja</th>
                    <th width='15%'>Uplate koje nisu pokrivene</th>
                    <th width='15%'>Neiskorišćen avans</th>
                </tr>";
            $html .= $this->generateTable($buyers_ids, 2, $bills_count, true, 'pdf') . '<br>';
        }
        if(!empty($suppliers_ids))
        {
            $html .= "<p2>Dobavljači</p2>";
            $html .= "<table id='table'>
                <tr>
                    <th width='3%'>R.br.</th>
                    <th width='36%'>Naziv komitenta</th>
                    <th width='15%'>Ukupno dugovanje</th>
                    <th width='15%'>Dospele obaveze</th>
                    <th width='15%'>Isplate koje nisu pokrivene</th>
                    <th width='15%'>Neiskorišćen avans</th>
                </tr>";
            $html .= $this->generateTable($suppliers_ids, count($buyers_ids) + 2, $bills_count, false, 'pdf');
        }
        
        return [
            'html' => $this->render('html', [
                'html' => $html,
                'date' => $this->date,
                'curr_company' => Yii::app()->company,
                'curr_user' => Yii::app()->user->model->DisplayName
            ], true, false)
        ];
    }
    
    public function getCSV()
    {
        if(!empty($this->update_func))
        {
            call_user_func($this->update_func, 1);
        }
        
        $curr_company = Yii::app()->company;
        $curr_user = Yii::app()->user->model->DisplayName;
                
        $csv = '"' . $curr_company->DisplayName . '"' . "\n";
        $csv .= 'PIB:' . ',"' .$curr_company->PIB . '"' . "\n";
        $csv .= 'Matični broj:' . ',"' . $curr_company->MB . '"' . "\n";
        $csv .= 'Šifra delatnosti:' . ',"' . (!empty($curr_company->work_code) ? $curr_company->work_code->code : '' ). '"' . "\n\n";
        $csv .= 'IOS po komitentima' . "\n";
        $csv .= 'Na dan:' . ',"' . SIMAHtml::DbToUserDate($this->date) . '"' . "\n";
        $csv .= 'Izveštaj izradio/la:' . ',"' . $curr_user . '"' . "\n\n";

        $buyers_ids = $this->is_buyers ? $this->getBuyers() : [];
        $suppliers_ids = $this->is_suppliers ? $this->getSuppliers() : [];
        $bills_count = count($buyers_ids) + count($suppliers_ids);
        
        if(!empty($buyers_ids))
        {
            $csv .= 'Kupci' . "\n";
            $csv .= 'R.br.' . ',';
            $csv .= 'Naziv komitenta' . ',';
            $csv .= 'Dospelo potraživanje' . ',';
            $csv .= 'Ukupana potraživanja' . ',';
            $csv .= 'Uplate koje nisu pokrivene' . ",";
            $csv .= 'Neiskorišćen avans' . "\n";
            $csv .= $this->generateTable($buyers_ids, 2, $bills_count, true, 'csv') . "\n" ;
        }
        if(!empty($suppliers_ids))
        {
            $csv .= 'Dobavljači' . "\n" ;
            $csv .= 'R.br.' . ',';
            $csv .= 'Naziv komitenta' . ',';
            $csv .= 'Dospele obaveze' . ',';
            $csv .= 'Ukupano dugovanje' . ',';
            $csv .= 'Isplate koje nisu pokrivene' . ",";
            $csv .= 'Neiskorišćen avans' . "\n";
            $csv .= $this->generateTable($suppliers_ids, count($buyers_ids) + 2, $bills_count, false, 'csv');
        }
        
        $temporary_file = new TemporaryFile($csv, null, 'csv');
        
        return $temporary_file;
    }
    
    private function getBuyers()
    {
        $criteria = new SIMADbCriteria([
            'Model' => BillOut::class,
            'select' => 'partner_id, sum(unreleased_amount) as unreleased_amount',
            'model_filter' => [
                'filter_scopes' => 'unreleased',
            ],
            'group' => 'partner_id',
            'order' => 'unreleased_amount desc'
        ]);
        
        $buyers_ids = [];
        BillOut::model()->findAllByParts(function($bills) use (&$buyers_ids) {
            foreach ($bills as $bill)
            {
                array_push($buyers_ids, $bill->partner_id);
            }
            unset($bills);
        }, $criteria, 0, 30);

        return $buyers_ids;
    }
    
    private function getSuppliers()
    {
        $criteria = new SIMADbCriteria([
            'Model' => BillIn::class,
            'select' => 'partner_id, sum(unreleased_amount) as unreleased_amount',
            'model_filter' => [
                'filter_scopes' => 'unreleased',
            ],
            'group' => 'partner_id',
            'order' => 'unreleased_amount desc'
        ]);
        
        $suppliers_ids = [];
        BillIn::model()->findAllByParts(function($bills) use (&$suppliers_ids) {
            foreach ($bills as $bill)
            {
                array_push($suppliers_ids, $bill->partner_id);
            }
            unset($bills);
        }, $criteria, 0, 30);
        
        return $suppliers_ids;
    }
    
    private function generateTable($partners_ids, $i, $bills_cnt, $invoice, $export_type)
    {
        $max_while_cycle_exec_time_seconds = ini_get('max_execution_time');
        $default_payment_type = Yii::app()->configManager->get('accounting.default_payment_type');
        $advance_payment_type = Yii::app()->configManager->get('accounting.advance_payment_type');
        $return_string = "";
        $debit_sum_all = 0;
        $obligation_sum_all = 0;
        $payment_unreleased_amount_all = 0;
        $advance_for_partner_amount_all = 0;
        $rb = 1;
        
        foreach ($partners_ids as $partner_id)
        {
            set_time_limit($max_while_cycle_exec_time_seconds);
            if(!empty($this->update_func))
            {
                call_user_func($this->update_func, round(($i/$bills_cnt)*100, 0, PHP_ROUND_HALF_DOWN));
            }  
            
            $payment = Payment::model()->find([
                'select' => 'partner_id, sum(unreleased_amount) as unreleased_amount, invoice as invoice',
                'model_filter' => [
                    'payment_type' => ['ids' => [$default_payment_type, $advance_payment_type]],
                    'filter_scopes' => 'unreleased',
                    'partner' => [
                        'ids' => $partner_id
                    ],
                    'invoice' => $invoice
                ],
                'group' => 'partner_id, invoice',
                'order' => 'unreleased_amount desc'
            ]);

            $payment_unreleased_amount = !empty($payment) ? $payment->unreleased_amount : 0;
            
            $criteria = new SIMADbCriteria([
                'Model' => Bill::class,
                'model_filter' => [
                    'bill_type' => Bill::$BILL,
                    'filter_scopes' => 'unreleased',
                    'partner' => [
                        'ids' => $partner_id
                    ],
                    'invoice' => $invoice
                ]
            ]);
            
            $bills = [];
            Bill::model()->findAllByParts(function($bills_for_partner) use (&$bills) {
                foreach ($bills_for_partner as $bill_for_partner)
                {
                    array_push($bills, [
                        'bill_number' => $bill_for_partner->bill_number,
                        'payment_date' => $bill_for_partner->payment_date,
                        'unreleased_amount' => $bill_for_partner->unreleased_amount
                    ]);
                }
                unset($bills_for_partner);
            }, $criteria, 0, 30);
            
            $advance_for_partner = AdvanceBill::model()->find([
                'select' => 'partner_id, sum(unreleased_amount) as unreleased_amount, invoice as invoice',
                'model_filter' => [
                    'filter_scopes' => 'unreleased',
                    'partner' => [
                        'ids' => $partner_id
                    ],
                    'invoice' => $invoice
                ],
                'group' => 'partner_id, invoice',
                'order' => 'unreleased_amount desc'
            ]);
            
            $advance_unreleased_amount = !empty($advance_for_partner) ? $advance_for_partner->unreleased_amount : 0;
                    
            $string_inter = '';
            $debit_sum = 0;
            $obligation_sum = 0;
            
            foreach ($bills as $bill)
            {
                $obligation = SIMAHtml::number_format(0);
                if($bill['payment_date'] === NULL || strtotime($bill['payment_date']) < strtotime($this->date))
                {
                    $obligation = SIMAHtml::number_format($bill['unreleased_amount']);
                    $obligation_sum += $bill['unreleased_amount'];
                }
                $debit = SIMAHtml::number_format($bill['unreleased_amount']);
                $debit_sum += $bill['unreleased_amount'];
                
                if($export_type === 'pdf')
                {
                    $string_inter .= "
                        <tr>
                            <td></td>
                            <td style='padding-left: 8mm !important;'>{$bill['bill_number']} - {$bill['payment_date']}</td>
                            <td>$debit</td>
                            <td>$obligation</td> 
                            <td></td>
                            <td></td>
                        </tr>";
                }
                else if($export_type === 'csv')
                {
                    $string_inter .= ' ' . ',';
                    $string_inter .= '"' . $bill['bill_number'] . '"' . ',';
                    $string_inter .= '"' . $debit . '"' . ',';
                    $string_inter .= '"' . $obligation . '"' . ',';
                    $string_inter .= '' . "\n";
                }
                
            }
            $debit_sum_all += $debit_sum;
            $obligation_sum_all += $obligation_sum;
            $payment_unreleased_amount_all += $payment_unreleased_amount;
            $advance_for_partner_amount_all += $advance_unreleased_amount;
            $partner = Partner::model()->findByPkWithCheck($partner_id);
            
            if($export_type === 'pdf')
            {
            $return_string .= "
                <tr class='tr-partner'>
                    <td style='text-align: center;'>{$rb}.</td>
                    <td>{$partner->display_name}</td>
                    <td>" . SIMAHtml::number_format($debit_sum) . "</td>
                    <td>" . SIMAHtml::number_format($obligation_sum) . "</td> 
                    <td>" . SIMAHtml::number_format($payment_unreleased_amount) . "</td>
                    <td>" . SIMAHtml::number_format($advance_unreleased_amount) . "</td>
                </tr>" . $string_inter;
            }
            else if($export_type === 'csv')
            {
                $return_string .= '"' . $i . '"' . ',';
                $return_string .= '"' . $partner->display_name . '"' . ',';
                $return_string .= '"' . SIMAHtml::number_format($debit_sum) . '"' . ',';
                $return_string .= '"' . SIMAHtml::number_format($obligation_sum) . '"' . ',';
                $return_string .= '"' . SIMAHtml::number_format($payment_unreleased_amount) . '"' . ",";
                $return_string .= '"' . SIMAHtml::number_format($advance_unreleased_amount) . '"' . "\n";
                $return_string .= $string_inter;
            }
            
            $i++;
            $rb++;    
        }
        
        if($export_type === 'pdf')
        {
            $return_string .= "
                <tr style='padding-bottom:5mm;'>
                    <td colspan='2' style='font-weight: bold;'><b>UKUPNO:</b></td>
                    <td style='text-align:right; font-weight: bold; padding: 1.3mm !important;'>" . SIMAHtml::number_format($debit_sum_all) . "</td>
                    <td style='font-weight: bold;'>" . SIMAHtml::number_format($obligation_sum_all) . "</td> 
                    <td style='font-weight: bold;'>" . SIMAHtml::number_format($payment_unreleased_amount_all) . "</td>
                    <td style='font-weight: bold;'>" . SIMAHtml::number_format($advance_for_partner_amount_all) . "</td>
                </tr></table>";
        }
        else if($export_type === 'csv')
        {
            $return_string .= '' . ',';
            $return_string .= 'UKUPNO:' . ',';
            $return_string .= '"' . SIMAHtml::number_format($debit_sum_all) . '"' . ',';
            $return_string .= '"' . SIMAHtml::number_format($obligation_sum_all) . '"' . ',';
            $return_string .= '"' . SIMAHtml::number_format($payment_unreleased_amount_all) . '"' . ",";
            $return_string .= '"' . SIMAHtml::number_format($advance_for_partner_amount_all) . '"' . "\n";
        }
                
        return $return_string;
    }
}