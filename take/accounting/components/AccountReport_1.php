<?php
class AccountReport extends SIMAReport
{
    protected function getModuleName()
    {
        return 'accounting';
    }
    
    protected function getHTMLContent()
    {
        $account_id = $this->params['account']->id;
        $guitable_filter = isset($this->params['guitable_filter'])?$this->params['guitable_filter']:[];
        $fixed_filter = isset($guitable_filter['fixed_filter'])?$guitable_filter['fixed_filter']:[];
        $select_filter = isset($guitable_filter['AccountTransaction'])?$guitable_filter['AccountTransaction']:[];
        
        $used_filters = [];
        GuiTableController::getRelationFilterParams(AccountTransaction::model(), $fixed_filter, $used_filters);
        GuiTableController::getRelationFilterParams(AccountTransaction::model(), $select_filter, $used_filters);
        
        $criteria = new SIMADbCriteria([
            'Model' => 'AccountTransaction',
            'model_filter' => $fixed_filter
        ]);
        $select_criteria = new SIMADbCriteria([
            'Model' => 'AccountTransaction',
            'model_filter' => $select_filter
        ]);
        $criteria->mergeWith($select_criteria);       
        $account_transactions = AccountTransaction::model()->findAll($criteria);
        $partner = null;
        $_account_temp = Account::model()->findByPk($account_id);
        $account = Account::model()->withDCCurrents($_account_temp->year_id)->findByPk($account_id);
        if (is_null($account))
        {
            //mora ovo jer scope withDCCurrents nice vratiti account ako nema ni jednu transakciju
            $account = $_account_temp;
        }
        if (!empty($account->LinkedModel) && !empty($account->model_id))
        {
            $account_model = $account->LinkedModel;
            $partner = $account_model::model()->findByPk($account->model_id);
        }
        
        $html = '';
        
        $html .= $this->render('html_header', [
            'account' => $account,            
            'partner' => $partner,
            'used_filters_html'=>SIMAGuiTable::renderUsedFiltersHtmlForPdf($used_filters)
        ], true, false);
        
        $html .= '<table width="100%" cellpadding="2" cellspacing="0" class="account-pdf-content">
                    <tr class="account-pdf-content-head">
                        <td width="6%" class="_first _text-left"><strong>Nalog</strong></td>
                        <td width="4%" class="_text-left"><strong>#</strong></td>
                        <td width="10%" class="_text-left"><strong>Datum</strong></td>
                        <td width="35%" class="_text-left"><strong>Opis</strong></td>
                        <td width="15%" class="_text-right"><strong>Duguje</strong></td>
                        <td width="15%" class="_text-right"><strong>Potražuje</strong></td>
                        <td width="15%" class="_text-right _last"><strong>Saldo</strong></td>        
                    </tr>
                </table>';
        
        $i = 1;
        $debit_per_month = 0;
        $credit_per_month = 0;
        $curr_month = null;
        $account_transactions_cnt = count($account_transactions);
        $max_while_cycle_exec_time_seconds = ini_get('max_execution_time');
        $html .= '<table width="100%" cellpadding="2" cellspacing="0" class="account-pdf-content">';
        foreach ($account_transactions as $transaction) 
        {
            set_time_limit($max_while_cycle_exec_time_seconds);
            if (!empty($this->params['update_func']))
            {
                call_user_func($this->params['update_func'], round(($i/$account_transactions_cnt)*100, 0, PHP_ROUND_HALF_DOWN));
            }
            $transaction_date = $transaction->account_document->account_order->date;
            $transaction_month = date("m", strtotime($transaction_date));
            if (is_null($curr_month))
            {
                $curr_month = $transaction_month;
            }
            
//            $comment = $transaction->comment;
//            $height = ceil(strlen($comment)/27) * 4;
            if ($curr_month !== $transaction_month)
            {
                $html .= '<tr class="month_line">
                            <td width="20%" colspan="3"></td>
                            <td width="35%" class="_text-left">Ukupno za mesec <span style="font-weight: bold;">'.$curr_month.'</span></td>
                            <td width="15%" class="_text-right _total" style="font-weight: bold;">'.SIMAHtml::number_format($debit_per_month).'</td>
                            <td width="15%" class="_text-right _total" style="font-weight: bold;">'.SIMAHtml::number_format($credit_per_month).'</td>
                            <td width="15%" class="_text-right _total" style="border: 0.3mm solid black; font-weight: bold;">'.SIMAHtml::number_format($debit_per_month - $credit_per_month).'</td>
                          </tr>';
                
                $debit_per_month = $transaction->debit;
                $credit_per_month = $transaction->credit;
                $curr_month = $transaction_month;
            }
            else
            {
                $debit_per_month += $transaction->debit;
                $credit_per_month += $transaction->credit;
            }            
            
            $html .= '<tr class="account-pdf-content-body" nobr="true">
                        <td width="6%" class="_text-left">'.$transaction->account_document->account_order->order.'</td>
                        <td width="4%" class="_text-left">'.$transaction->account_order_order.'</td>
                        <td width="10%" class="_text-left">'.$transaction_date.'</td>
                        <td width="35%" class="_text-left">'.$transaction->comment.'</td>
                        <td width="15%" class="_text-right">'.SIMAHtml::number_format($transaction->debit).'</td>
                        <td width="15%" class="_text-right">'.SIMAHtml::number_format($transaction->credit).'</td>
                        <td width="15%" class="_text-right">'.SIMAHtml::number_format($transaction->saldo).'</td>
                     </tr>';
            $i++;            
        }
        $html .= '</table>';
        
        $html .= $this->render('html_footer', [
            'account' => $account,            
            'curr_month'=>$curr_month,
            'debit_per_month'=>$debit_per_month,
            'credit_per_month'=>$credit_per_month
        ], true, false);
                
        return [
            'html' => $html,
            'css_file_name' => 'css'
        ];
    }
}