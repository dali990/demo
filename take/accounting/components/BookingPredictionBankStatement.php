<?php

/**
 * Staticka klasa u kojoj se predvidjaju sva proknjizavanja
 */
class BookingPredictionBankStatement
{
    public static function Booking(AccountDocument $document)
    {
        $booking_transactions = [];
        $bs = $document->file->bank_statement;
        
        $_statement_comment = $document->file->bank_statement->DisplayName;
        
        $debit_sum = 0;
        $credit_sum = 0;
        
        $default_payment_type = Yii::app()->configManager->get('accounting.default_payment_type');
        $advance_payment_type = Yii::app()->configManager->get('accounting.advance_payment_type');
        
        foreach ($bs->payments as $payment)
        {
            if ($payment->invoice)
            {
                $debit_sum += $payment->amount;
            }
            else
            {
                $credit_sum += $payment->amount;
            }
        }
        $booking_transactions[] = self::BookingSums($document, $bs, $debit_sum, $credit_sum, $_statement_comment);
        
        foreach ($bs->payments as $payment)
        {
            switch ($payment->payment_type_id)
            {
                case $advance_payment_type:
                case $default_payment_type:
                    $booking_transactions[] = self::BookingBill($document, $bs, $payment, $_statement_comment);
                    break;
                //Sustinski mogu da se posmatraju isto, gleda se datum 
//                case $advance_payment_type:
//                    $booking_transactions[] = self::BookingAdvance($document, $bs, $payment, $_statement_comment);
//                    break;
                default:
                    
                    if (isset($payment->payment_code) && in_array($payment->payment_code->code, ['140','141','240','241','254']))
                    {
                        $booking_transactions[] = self::BookingPaycheck($document, $bs, $payment, $_statement_comment);
                    }
                    else
                    {
                        $booking_transactions[] = self::BookingDefault($document, $bs, $payment, $_statement_comment);
                    }
                    break;
            }
        } 
        
        return SIMAMisc::joinArrays($booking_transactions);
    }
    
    /**
     * Knjizenje default
     * @param AccountDocument $document - dokument koji se knjizi/izvod
     * @param BankStatement $bs
     * @param Payment $payment
     */
    private static function BookingDefault(AccountDocument $document,BankStatement $bs,Payment $payment, $_statement_comment)
    {
        $booking_transactions = [];
        //godina iz naloga za knjizenje, mada mora da bude ista i iz izvoda
        $_year = $document->account_order->year;
        if (isset($payment->payment_type->account))
        {
            $payment_account = $payment->payment_type->accountForYear($_year);
        }
        else
        {
            if ($payment->invoice)
            {
                $payment_account    = Account::getFromParam('accounting.codes.default_income_account',$_year);
            }
            else
            {
                $payment_account    = Account::getFromParam('accounting.codes.default_costs_account',$_year);
            }
        }
        
        if ($payment_account==null)
        {
            Yii::app()->raiseNote('Nisu postavljeni osnovni konti za troskove i prihode');
        }
        else
        {
            if (isset($payment->payment_type) && $payment->payment_type->partner_analitic)
            {
                $_model = $payment->partner;
            }
            elseif (isset($payment->job_order) && $payment->job_order->for_analytics)
            {
                $_model = $payment->job_order;
            }
            else
            {
                $_model = null;
            }
            $_account = $payment_account->getLeafAccount($_model);
//
            $tr = new AccountTransaction();
            $tr->account_id = $_account->id;
            $tr->account_document_id = $document->id;
            $tr->comment = $_statement_comment;
            if ($payment->invoice)
            {
                $tr->credit = $payment->amount;
            }
            else
            {
                $tr->debit = $payment->amount;
            }
            $booking_transactions[] = $tr;
        }
        
        return $booking_transactions;
    }
    
    private static function BookingSums(AccountDocument $document, BankStatement $bs, $debit_sum, $credit_sum, $_statement_comment)
    {
        $booking_transactions = [];
        
        //godina iz naloga za knjizenje, mada mora da bude ista i iz izvoda
        $_year = $document->account_order->year;
        
        $bank_account_parent = Account::getFromParam('accounting.codes.default_bank_account_account',$_year);
        if ($bank_account_parent==null)
        {
            Yii::app()->raiseNote('Niste postavili osnovni konto za ziro/tekuce racune');
        }
        else
        {
            $bank_account = $bank_account_parent->getLeafAccount($bs->account);

            if ($debit_sum>0)
            {
                $tr1 = new AccountTransaction();
                $tr1->account_id = $bank_account->id;
                $tr1->account_document_id = $document->id;
                $tr1->comment = $_statement_comment;
                $tr1->debit = $debit_sum;
                $booking_transactions[] = $tr1;
            }
            
            if ($credit_sum > 0)
            {
                $tr2 = new AccountTransaction();
                $tr2->account_id = $bank_account->id;
                $tr2->account_document_id = $document->id;
                $tr2->comment = $_statement_comment;
                $tr2->credit = $credit_sum;
                $booking_transactions[] = $tr2;
            }
        }
        return $booking_transactions;
    }
    
    /**
     * Knjizenje po racunu
     * @param AccountDocument $document - dokument koji se knjizi/izvod
     * @param BankStatement $bs
     * @param Payment $payment
     */
    private static function BookingBill(AccountDocument $document,BankStatement $bs,Payment $payment, $_statement_comment)
    {
        $booking_transactions = [];
        
        //godina iz naloga za knjizenje, mada mora da bude ista i iz izvoda
        $_year = $document->account_order->year;
        
        $_fully_connected = SIMAMisc::areEqual ($payment->unreleased_amount, 0);
        $_is_recurring_bill = !empty($payment->recurring_bill_id);
        $_is_advance_payment = $payment->payment_type_id == Yii::app()->configManager->get('accounting.advance_payment_type');

        if (!$_is_recurring_bill && !$_fully_connected && !$_is_advance_payment)
        {
            Yii::app()->raiseNote('"placanje po racunu" od/ka partneru '.$payment->partner->DisplayName.' nije rasknjizeno do kraja');
        }

        $_currency_diff = $payment->exchange_rate_difference_sum;
        $_other_diff = $payment->other_difference_sum;
        $_booking_amount = 0; //knjizi se na dobavljaca/kupca
        $_advance_amount = 0; //knjizi se na avans

        ////ODVAJANJE STA JE AVANSNO - POCETAK
        if (Yii::app()->configManager->get('accounting.book_bill_paymnet_connections_by_date_or_type') === 'BY_DATE')
        {
            if ($_is_advance_payment)
            {
                $_advance_amount += $payment->unreleased_amount;
            }
            else
            {
                $_booking_amount += $payment->unreleased_amount;
            }
            $_same_day_advance = boolval(Yii::app()->configManager->get('accounting.paymnet_same_day_as_bill_is_advance'));
            foreach ($payment->releases as $_bill_release)
            {
                $_bill = $_bill_release->bill;
                if (
                        $_bill->isAdvanceBill
                        ||
                        SIMAMisc::laterThen($_bill->income_date,$bs->date) 
                        || 
                        ($_same_day_advance && SIMAMisc::sameDay($_bill->income_date,$bs->date))
                    )
                {
                    $_advance_amount += $_bill_release->amount;
                }
                else
                {
                    $_booking_amount += $_bill_release->amount;
                }
            }
        }
        else
        {
            if ($_is_advance_payment)
            {
                $_advance_amount = $payment->unreleased_amount + $payment->released_sum;
            }
            else
            {
                $_booking_amount = $payment->unreleased_amount + $payment->released_sum;
            }
        }
        ////ODVAJANJE STA JE AVANSNO - KRAJ

        if (abs($_booking_amount)>0.001)
        {
            if (!is_null($payment->recurring_bill))
            {
                $_partner = $payment->recurring_bill->partner;
            }
            else
            {
                $_partner = $payment->partner;
            }
            if ($payment->invoice)
            {
                $_by_bill_side = 'credit';
                $_by_bill_account = $_partner->getCustomerAccount($_year);
            }
            else
            {
                $_by_bill_side = 'debit';
                $_by_bill_account = $_partner->getSupplierAccount($_year);
            }
            $tr = new AccountTransaction();
            $tr->account_id = $_by_bill_account->id;
            $tr->account_document_id = $document->id;
            $tr->comment = $_statement_comment;
            $tr->$_by_bill_side = $_booking_amount;
            $booking_transactions[] = $tr;
        }

        if (abs($_advance_amount)>0.001)
        {
            if ($payment->invoice)
            {
                $_side = 'credit';
                $_advance_account = $payment->partner->getCustomerAdvanceAccount($_year);
            }
            else
            {
                $_side = 'debit';
                $_advance_account = $payment->partner->getSupplierAdvanceAccount($_year);

            }
            $tr_d = new AccountTransaction();
            $tr_d->account_id = $_advance_account->id;
            $tr_d->account_document_id = $document->id;
            $tr_d->comment = $_statement_comment.' - A';
            $tr_d->$_side = abs($_advance_amount);
            $booking_transactions[] = $tr_d;
        }

        if (abs($_currency_diff)>0.001)
        {
            $_positive_diff = true;//pozitivna kursna razlika

            //nije pozitivna ukoliko je manje uplaceno i faktura je
            if ($_currency_diff < 0 && $payment->invoice)
            {
                $_positive_diff = false;
            }
            //nije pozitivno ako je vise placeno a izlazni je racun
            if ($_currency_diff > 0 && !$payment->invoice)
            {
                $_positive_diff = false;
            }
            if ($_positive_diff)
            {
                $_side = 'credit';
                $_curr_acc = Account::getFromParam('accounting.codes.positive_currency_diff',$_year);
            }
            else
            {
                $_side = 'debit';
                $_curr_acc = Account::getFromParam('accounting.codes.negative_currency_diff',$_year);

            }
            $tr_d = new AccountTransaction();
            $tr_d->account_id = $_curr_acc->id;
            $tr_d->account_document_id = $document->id;
            $tr_d->comment = $_statement_comment.' - kursna razlika';
            $tr_d->$_side = abs($_currency_diff);
            $booking_transactions[] = $tr_d;
        }

        if (abs($_other_diff)>0.001)
        {
            $_positive_diff = true;//pozitivna ostala razlika

            //nije pozitivna ukoliko je manje uplaceno i faktura je
            if ($_other_diff < 0 && $payment->invoice)
            {
                $_positive_diff = false;
            }
            //nije pozitivno ako je vise placeno a izlazni je racun
            if ($_other_diff > 0 && !$payment->invoice)
            {
                $_positive_diff = false;
            }

            if ($payment->invoice)
            {

            }
            else
            {

            }
            if ($_positive_diff)
            {
                $_side = 'credit';
                $_curr_acc    = Account::getFromParam('accounting.codes.default_income_account',$_year);
            }
            else
            {
                $_side = 'debit';
                $_curr_acc    = Account::getFromParam('accounting.codes.default_costs_account',$_year);
            }
            $tr_d = new AccountTransaction();
            $tr_d->account_id = $_curr_acc->id;
            $tr_d->account_document_id = $document->id;
            $tr_d->comment = $_statement_comment.' - kursna razlika';
            $tr_d->$_side = abs($_other_diff);
            $booking_transactions[] = $tr_d;
        }
        
        return $booking_transactions;
    }
    
    /**
     * Knjizenje po avansu
     * @param AccountDocument $document - dokument koji se knjizi/izvod
     * @param BankStatement $bs
     * @param Payment $payment
     */
    private static function BookingAdvance(AccountDocument $document,BankStatement $bs,Payment $payment, $_statement_comment)
    {
        $booking_transactions = [];
        //dok ne smislimo pametnije
        
        //godina iz naloga za knjizenje, mada mora da bude ista i iz izvoda
        $_year = $document->account_order->year;
        
        if ($payment->invoice)
        {
            $partner_account     = $payment->partner->getCustomerAdvanceAccount($_year);
        }
        else
        {
            $partner_account     = $payment->partner->getSupplierAdvanceAccount($_year);
        }
        
        $tr = new AccountTransaction();
        $tr->account_id = $partner_account->id;
        $tr->account_document_id = $document->id;
        $tr->comment = $_statement_comment;
        if ($payment->invoice)
        {
            $tr->credit = $payment->amount;
        }
        else
        {
            $tr->debit = $payment->amount;
        }
        $booking_transactions[] = $tr;
        return $booking_transactions;
    }
    
    /**
     * Knjizenje plate
     * @param AccountDocument $document - dokument koji se knjizi/izvod
     * @param BankStatement $bs
     * @param Payment $payment
     */
    private static function BookingPaycheck(AccountDocument $document, BankStatement $bs,Payment $payment, $_statement_comment)
    {
        $booking_transactions = [];
        
        //godina iz naloga za knjizenje, mada mora da bude ista i iz izvoda
        $_year = $document->account_order->year;
        
        switch ($payment->payment_code->code)
        {
            //klasicna plata
            case '140': case '240':
                $parent_account = Account::getFromParam('accounting.codes.paychecks.obligation_emp_neto',$_year);
                if ($parent_account == null)
                {
                    Yii::app()->raiseNote('Niste postavili konto za plate');
                }
                else
                {
                    $account = $parent_account->getLeafAccount();
                    $tr = new AccountTransaction();
                    $tr->account_id = $account->id;
                    $tr->account_document_id = $document->id;
                    $tr->comment = $_statement_comment;
                    if ($payment->invoice)
                    {
                        $tr->credit = $payment->amount;
                    }
                    else
                    {
                        $tr->debit = $payment->amount;
                    }
                    $booking_transactions[] = $tr;
                }
                break;
            case '141': case '241':
                $parent_account = Account::getFromParam('accounting.codes.paychecks.obligation_emp_neto_stops',$_year);
                if ($parent_account == null)
                {
                    Yii::app()->raiseNote('Niste postavili konto za administrativne zabrane od plate');
                }
                else
                {
                    $account = $parent_account->getLeafAccount();
                    $tr = new AccountTransaction();
                    $tr->account_id = $account->id;
                    $tr->account_document_id = $document->id;
                    $tr->comment = $_statement_comment;
                    if ($payment->invoice)
                    {
                        $tr->credit = $payment->amount;
                    }
                    else
                    {
                        $tr->debit = $payment->amount;
                    }
                    $booking_transactions[] = $tr;
                }
                break;
            case '254':
                $booking_transactions = array_merge(
                        $booking_transactions,
                        BookingPredictionPaycheckPeriod::BookingForPayment($document, $payment, $_statement_comment)
                );

                
                break;

            default:
                Yii::app()->raiseNote('nije implementirano rasknjizavanje plate po kodu '. $payment->payment_code->code);
                break;
        }
        return $booking_transactions;
    }
    
}