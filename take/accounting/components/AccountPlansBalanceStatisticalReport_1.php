<?php

class AccountPlansBalanceStatisticalReport extends AccountPlansBalanceReport
{
    protected $aop_codes_with_original_value = ['aop-9001-3', 'aop-9001-4', 'aop-9002-3', 'aop-9002-4', 'aop-9005-3', 'aop-9005-4'];
    
    public function getReportName() : string
    {
        return 'Statistički izveštaj';
    }
    
    protected function getAOPCodes() : array
    {
        return [
            '9001' => function() {
                return [
                    'aop-9001-3' => !empty($this->curr_year->accounting) ? $this->curr_year->accounting->getAccountingMonthsCount() : 0,
                    'aop-9001-4' => !empty($this->prev_year->accounting) ? $this->prev_year->accounting->getAccountingMonthsCount() : 0
                ];
            },
            '9002' => function() {
                $accounting_statistical_report_ownership_designation = Yii::app()->configManager->get('accounting.accounting_statistical_report_ownership_designation', false);
                if (empty($accounting_statistical_report_ownership_designation))
                {
                    $accounting_statistical_report_ownership_designation = 0;
                }
                return [
                    'aop-9002-3' => $accounting_statistical_report_ownership_designation,
                    'aop-9002-4' => $accounting_statistical_report_ownership_designation,
                ];
            },
            '9003' => [
                'columns_type' => 1,
                'plus' => []
            ],
            '9004' => [
                'columns_type' => 1,
                'plus' => []
            ],
            '9005' => function() {
                return [
                    'aop-9005-3' => $this->getActiveEmployeesMonthlyAverageForYear($this->curr_year),
                    'aop-9005-4' => $this->getActiveEmployeesMonthlyAverageForYear($this->prev_year),
                ];
            },
            '9006' => function() {
                $aop_5 = $this->getAccoountColumnValue('019', $this->curr_year, 'debit_start');
                $aop_4 = $this->getAccoountColumnValue('01', $this->curr_year, 'debit_start') - $aop_5;
                
                return [
                    'aop-9006-4' => $aop_4,
                    'aop-9006-5' => $aop_5,
                    'aop-9006-6' => abs($aop_4) - abs($aop_5)
                ];
            },
            '9007' => function() {
                $aop_4 = $this->getAccoountColumnValue('01', $this->curr_year, 'debit_current') - $this->getAccoountColumnValue('019', $this->curr_year, 'debit_current');
                $aop_5 = $this->getAccoountColumnValue('019', $this->curr_year, 'credit_current');
                
                return [
                    'aop-9007-4' => $aop_4,
                    'aop-9007-5' => $aop_5,
                    'aop-9007-6' => abs($aop_4) - abs($aop_5)
                ];
            },
            '9008' => function() {
                $aop_4 = $this->getAccoountColumnValue('01', $this->curr_year, 'credit_current') - $this->getAccoountColumnValue('019', $this->curr_year, 'credit_current');
                $aop_5 = $this->getAccoountColumnValue('019', $this->curr_year, 'debit_current');
                
                return [
                    'aop-9008-4' => $aop_4,
                    'aop-9008-5' => $aop_5,
                    'aop-9008-6' => abs($aop_4) - abs($aop_5)
                ];
            },
            '9009' => [
                'columns_type' => 2,
                'plus' => []
            ],
            '9010' => [
                'double_check' => [
                    function() {
                        $aop_4 = $this->getAccoountColumnValue('01', $this->curr_year, 'debit_sum') -
                                 $this->getAccoountColumnValue('019', $this->curr_year, 'debit_sum') -
                                 $this->getAccoountColumnValue('01', $this->curr_year, 'credit_sum') + 
                                 $this->getAccoountColumnValue('019', $this->curr_year, 'credit_sum');
                        $aop_5 = $this->getAccoountColumnValue('019', $this->curr_year, 'saldo');
                        $aop_6 = $this->getAccoountColumnValue('01', $this->curr_year, 'saldo');

                        return [
                            'aop-9010-4' => $aop_4,
                            'aop-9010-5' => $aop_5,
                            'aop-9010-6' => $aop_6
                        ];
                    },
                    [
                        'columns_type' => 2,
                        'type' => 'AOP',
                        'plus' => ['9006', '9007', '9009'],
                        'minus' => ['9008']
                    ]
                ]
            ],
            '9011' => function() {
                $aop_5 = $this->getAccoountColumnValue('029', $this->curr_year, 'debit_start');
                $aop_4 = $this->getAccoountColumnValue('02', $this->curr_year, 'debit_start') - $aop_5;
                
                return [
                    'aop-9011-4' => $aop_4,
                    'aop-9011-5' => $aop_5,
                    'aop-9011-6' => abs($aop_4) - abs($aop_5)
                ];
            },
            '9012' => function() {
                $aop_4 = $this->getAccoountColumnValue('02', $this->curr_year, 'debit_current') - $this->getAccoountColumnValue('029', $this->curr_year, 'debit_current');
                $aop_5 = $this->getAccoountColumnValue('029', $this->curr_year, 'credit_current');
                
                return [
                    'aop-9012-4' => $aop_4,
                    'aop-9012-5' => $aop_5,
                    'aop-9012-6' => abs($aop_4) - abs($aop_5)
                ];
            },
            '9013' => function() {
                $aop_4 = $this->getAccoountColumnValue('02', $this->curr_year, 'credit_current') - $this->getAccoountColumnValue('029', $this->curr_year, 'credit_current');
                $aop_5 = $this->getAccoountColumnValue('029', $this->curr_year, 'debit_current');
                
                return [
                    'aop-9013-4' => $aop_4,
                    'aop-9013-5' => $aop_5,
                    'aop-9013-6' => abs($aop_4) - abs($aop_5)
                ];
            },
            '9014' => [
                'columns_type' => 2,
                'plus' => []
            ],
            '9015' => [
                'double_check' => [
                    function() {
                        $aop_4 = $this->getAccoountColumnValue('02', $this->curr_year, 'debit_sum') -
                                 $this->getAccoountColumnValue('029', $this->curr_year, 'debit_sum') -
                                 $this->getAccoountColumnValue('02', $this->curr_year, 'credit_sum') + 
                                 $this->getAccoountColumnValue('029', $this->curr_year, 'credit_sum');
                        $aop_5 = $this->getAccoountColumnValue('029', $this->curr_year, 'saldo');
                        $aop_6 = $this->getAccoountColumnValue('02', $this->curr_year, 'saldo');

                        return [
                            'aop-9015-4' => $aop_4,
                            'aop-9015-5' => $aop_5,
                            'aop-9015-6' => $aop_6
                        ];
                    },
                    [
                        'columns_type' => 2,
                        'type' => 'AOP',
                        'plus' => ['9011', '9012', '9014'],
                        'minus' => ['9013']
                    ]
                ]
            ],
            '9016' => function() {
                $aop_4 = $this->getAccoountColumnValue('03', $this->curr_year, 'debit_start');
                $aop_5 = $this->getAccoountColumnValue('039', $this->curr_year, 'credit_start');
                
                return [
                    'aop-9016-4' => $aop_4,
                    'aop-9016-5' => $aop_5,
                    'aop-9016-6' => abs($aop_4) - abs($aop_5)
                ];
            },
            '9017' => function() {
                $aop_4 = $this->getAccoountColumnValue('03', $this->curr_year, 'debit_current');
                $aop_5 = $this->getAccoountColumnValue('039', $this->curr_year, 'credit_current');
                
                return [
                    'aop-9017-4' => $aop_4,
                    'aop-9017-5' => $aop_5,
                    'aop-9017-6' => abs($aop_4) - abs($aop_5)
                ];
            },
            '9018' => function() {
                $aop_4 = $this->getAccoountColumnValue('03', $this->curr_year, 'credit_current');
                $aop_5 = $this->getAccoountColumnValue('039', $this->curr_year, 'debit_current');
                
                return [
                    'aop-9018-4' => $aop_4,
                    'aop-9018-5' => $aop_5,
                    'aop-9018-6' => abs($aop_4) - abs($aop_5)
                ];
            },
            '9019' => [
                'columns_type' => 2,
                'plus' => []
            ],
            '9020' => [
                'double_check' => [
                    function() {
                        $aop_4 = $this->getAccoountColumnValue('03', $this->curr_year, 'debit_sum') -
                                 $this->getAccoountColumnValue('039', $this->curr_year, 'debit_sum') -
                                 $this->getAccoountColumnValue('03', $this->curr_year, 'credit_sum') + 
                                 $this->getAccoountColumnValue('039', $this->curr_year, 'credit_sum');
                        $aop_5 = $this->getAccoountColumnValue('039', $this->curr_year, 'saldo');
                        $aop_6 = $this->getAccoountColumnValue('03', $this->curr_year, 'saldo');
                        
                        return [
                            'aop-9020-4' => $aop_4,
                            'aop-9020-5' => $aop_5,
                            'aop-9020-6' => $aop_6
                        ];
                    },
                    [
                        'columns_type' => 2,
                        'type' => 'AOP',
                        'plus' => ['9016', '9017', '9019'],
                        'minus' => ['9018']
                    ]
                ]
            ],
            '9021' => [
                'columns_type' => 3,
                'column' => 'saldo',
                'plus' => ['300']
            ],
            '9022' => [
                'columns_type' => 3,
                'plus' => []
            ],
            '9023' => [
                'columns_type' => 3,
                'column' => 'saldo',
                'plus' => ['301']
            ],
            '9024' => [
                'columns_type' => 3,
                'plus' => []
            ],
            '9025' => [
                'columns_type' => 3,
                'column' => 'saldo',
                'plus' => ['302']
            ],
            '9026' => [
                'columns_type' => 3,
                'plus' => []
            ],
            '9027' => [
                'columns_type' => 3,
                'column' => 'saldo',
                'plus' => ['303']
            ],
            '9028' => [
                'columns_type' => 3,
                'column' => 'saldo',
                'plus' => ['304']
            ],
            '9029' => [
                'columns_type' => 3,
                'column' => 'saldo',
                'plus' => ['305']
            ],
            '9030' => [
                'columns_type' => 3,
                'column' => 'saldo',
                'plus' => ['306']
            ],
            '9031' => [
                'columns_type' => 3,
                'column' => 'saldo',
                'plus' => ['309']
            ],
            '9032' => [
                'double_check' => [
                    [
                        'columns_type' => 3,
                        'column' => 'saldo',
                        'plus' => ['30']
                    ],
                    [
                        'columns_type' => 3,
                        'type' => 'AOP',
                        'plus' => ['9021', '9023', '9025', '9027', '9028', '9029', '9030', '9031']
                    ]
                ]
            ],
            '9033' => [
                'columns_type' => 3,
                'plus' => []
            ],
            '9034' => [
                'columns_type' => 3,
                'plus' => []
            ],
            '9035' => [
                'columns_type' => 3,
                'plus' => []
            ],
            '9036' => [
                'columns_type' => 3,
                'plus' => []
            ],
            '9037' => [
                'double_check' => [
                    [
                        'columns_type' => 3,
                        'column' => 'saldo',
                        'plus' => ['300']
                    ],
                    [
                        'columns_type' => 3,
                        'type' => 'AOP',
                        'plus' => ['9034', '9036']
                    ]
                ]
            ],
            '9038' => [
                'columns_type' => 3,
                'plus' => []
            ],
            '9039' => [
                'columns_type' => 3,
                'plus' => []
            ],
            '9040' => [
                'columns_type' => 3,
                'plus' => []
            ],
            '9041' => [
                'columns_type' => 3,
                'plus' => []
            ],
            '9042' => [
                'columns_type' => 3,
                'plus' => []
            ],
            '9043' => [
                'columns_type' => 3,
                'plus' => []
            ],
            '9044' => [
                'columns_type' => 3,
                'plus' => []
            ],
            '9045' => [
                'columns_type' => 3,
                'plus' => []
            ],
            '9046' => [
                'columns_type' => 3,
                'plus' => []
            ],
            '9047' => [
                'columns_type' => 3,
                'column' => 'debit_current',
                'plus' => ['226']
            ],
            '9048' => [
                'columns_type' => 3,
                'column' => 'credit_current',
                'plus' => ['450']
            ],
            '9049' => [
                'columns_type' => 3,
                'column' => 'credit_current',
                'plus' => ['451']
            ],
            '9050' => [
                'columns_type' => 3,
                'column' => 'credit_current',
                'plus' => ['452']
            ],
            '9051' => [
                'columns_type' => 3,
                'column' => 'credit_current',
                'plus' => ['461', '462', '723']
            ],
            '9052' => [
                'columns_type' => 3,
                'column' => 'credit_current',
                'plus' => ['465']
            ],
            '9053' => [
                'columns_type' => 3,
                'type' => 'AOP',
                'plus' => ['9047', '9048', '9049', '9050', '9051', '9052']
            ],
            '9054' => [
                'columns_type' => 3,
                'column' => 'debit_current',
                'plus' => ['520']
            ],
            '9055' => [
                'columns_type' => 3,
                'column' => 'debit_current',
                'plus' => ['521']
            ],
            '9056' => [
                'columns_type' => 3,
                'column' => 'debit_current',
                'plus' => ['522', '523', '524', '525']
            ],
            '9057' => [
                'columns_type' => 3,
                'column' => 'debit_current',
                'plus' => ['526']
            ],
            '9058' => [
                'columns_type' => 3,
                'column' => 'debit_current',
                'plus' => ['529']
            ],
            '9059' => [
                'columns_type' => 3,
                'column' => 'debit_current',
                'plus' => []
            ],
            '9060' => [
                'columns_type' => 3,
                'column' => 'debit_current',
                'plus' => []
            ],
            '9061' => [
                'columns_type' => 3,
                'column' => 'debit_current',
                'plus' => ['536', '537']
            ],
            '9062' => [
                'columns_type' => 3,
                'column' => 'debit_current',
                'plus' => ['552']
            ],
            '9063' => [
                'columns_type' => 3,
                'column' => 'debit_current',
                'plus' => ['553']
            ],
            '9064' => [
                'columns_type' => 3,
                'column' => 'debit_current',
                'plus' => ['554']
            ],
            '9065' => [
                'columns_type' => 3,
                'column' => 'debit_current',
                'plus' => ['555']
            ],
            '9066' => [
                'columns_type' => 3,
                'column' => 'debit_current',
                'plus' => ['556']
            ],
            '9067' => [
                'columns_type' => 3,
                'column' => 'debit_current',
                'plus' => []
            ],
            '9068' => [
                'columns_type' => 3,
                'column' => 'debit_current',
                'plus' => []
            ],
            '9069' => [
                'columns_type' => 3,
                'plus' => []
            ],
            '9070' => [
                'columns_type' => 3,
                'plus' => []
            ],
            '9071' => [
                'columns_type' => 3,
                'plus' => []
            ],
            '9072' => [
                'columns_type' => 3,
                'plus' => []
            ],
            '9073' => [
                'columns_type' => 3,
                'plus' => []
            ],
            '9074' => [
                'columns_type' => 3,
                'column' => 'debit_current',
                'plus' => ['579']
            ],
            '9075' => [
                'columns_type' => 3,
                'type' => 'AOP',
                'plus' => [
                    '9054', '9055', '9056', '9057', '9058', '9059', '9060', '9061', '9062', '9063', '9064', '9065', '9066', '9067',
                    '9068', '9069', '9070', '9071', '9072', '9073', '9074'
                ]
            ],
            '9076' => [
                'columns_type' => 3,
                'column' => 'debit_current',
                'plus' => ['640']
            ],
            '9077' => [
                'columns_type' => 3,
                'column' => 'debit_current',
                'plus' => ['641']
            ],
            '9078' => [
                'columns_type' => 3,
                'column' => 'debit_current',
                'plus' => ['650']
            ],
            '9079' => [
                'columns_type' => 3,
                'column' => 'debit_current',
                'plus' => ['651']
            ],
            '9080' => [
                'columns_type' => 3,
                'plus' => []
            ],
            '9081' => [
                'columns_type' => 3,
                'plus' => []
            ],
            '9082' => [
                'columns_type' => 3,
                'plus' => []
            ],
            '9083' => [
                'columns_type' => 3,
                'type' => 'AOP',
                'plus' => [
                    '9076', '9077', '9078', '9079', '9080', '9081', '9082'
                ]
            ],
            '9084' => [
                'columns_type' => 1,
                'plus' => []
            ],
            '9085' => [
                'columns_type' => 1,
                'plus' => []
            ],
            '9086' => [
                'columns_type' => 1,
                'plus' => []
            ],
            '9087' => [
                'columns_type' => 1,
                'plus' => []
            ],
            '9088' => [
                'columns_type' => 1,
                'plus' => []
            ],
            '9089' => [
                'columns_type' => 1,
                'plus' => []
            ],
            '9090' => [
                'columns_type' => 1,
                'plus' => []
            ],
            '9091' => [
                'columns_type' => 1,
                'plus' => []
            ],
            '9092' => [
                'columns_type' => 1,
                'plus' => []
            ],
            '9093' => [
                'columns_type' => 1,
                'plus' => []
            ],
            '9094' => [
                'columns_type' => 1,
                'plus' => []
            ],
            '9095' => [
                'columns_type' => 1,
                'plus' => []
            ],
            '9096' => [
                'columns_type' => 1,
                'plus' => []
            ],
            '9097' => [
                'columns_type' => 1,
                'plus' => []
            ],
            '9098' => [
                'columns_type' => 1,
                'plus' => []
            ],
            '9099' => [
                'columns_type' => 1,
                'plus' => []
            ],
            '9100' => [
                'columns_type' => 1,
                'plus' => []
            ],
            '9101' => [
                'columns_type' => 1,
                'plus' => []
            ],
            '9102' => [
                'columns_type' => 1,
                'plus' => []
            ],
            '9103' => [
                'columns_type' => 1,
                'plus' => []
            ],
            '9104' => [
                'columns_type' => 1,
                'plus' => []
            ],
            '9105' => [
                'columns_type' => 1,
                'plus' => []
            ],
            '9106' => [
                'columns_type' => 1,
                'plus' => []
            ],
            '9107' => [
                'columns_type' => 1,
                'plus' => []
            ],
            '9108' => [
                'columns_type' => 2,
                'plus' => []
            ],
            '9109' => [
                'columns_type' => 2,
                'plus' => []
            ],
            '9110' => [
                'columns_type' => 2,
                'plus' => []
            ],
            '9111' => [
                'columns_type' => 2,
                'plus' => []
            ],
            '9112' => [
                'columns_type' => 2,
                'plus' => []
            ],
            '9113' => [
                'columns_type' => 2,
                'plus' => []
            ],
            '9114' => [
                'columns_type' => 2,
                'plus' => []
            ],
            '9115' => [
                'columns_type' => 2,
                'plus' => []
            ],
            '9116' => [
                'columns_type' => 2,
                'plus' => []
            ],
            '9117' => [
                'columns_type' => 2,
                'plus' => []
            ],
            '9118' => [
                'columns_type' => 2,
                'plus' => []
            ],
            '9119' => [
                'columns_type' => 2,
                'plus' => []
            ],
            '9120' => [
                'columns_type' => 2,
                'plus' => []
            ],
            '9121' => [
                'columns_type' => 2,
                'plus' => []
            ],
            '9122' => [
                'columns_type' => 2,
                'plus' => []
            ],
            '9123' => [
                'columns_type' => 2,
                'plus' => []
            ],
            '9124' => [
                'columns_type' => 2,
                'plus' => []
            ],
            '9125' => [
                'columns_type' => 2,
                'plus' => []
            ],
            '9126' => [
                'columns_type' => 2,
                'plus' => []
            ],
            '9127' => [
                'columns_type' => 2,
                'plus' => []
            ],
            '9128' => [
                'columns_type' => 2,
                'plus' => []
            ],
            '9129' => [
                'columns_type' => 2,
                'plus' => []
            ],
            '9130' => [
                'columns_type' => 2,
                'plus' => []
            ]
        ];
    }

    protected function getXMLValueFromAccount(string $aop_code, string $account_code) : array
    {
        return [];
    }
    
    protected function getXMLValueFromAccounts(string $aop_code, array $account_codes, array $aop_params) : array
    {
        $aop_columns = [];

        if ($aop_params['columns_type'] === 1)
        {
            $aop_columns[3] = 0;
            $aop_columns[4] = 0;
        }
        else if ($aop_params['columns_type'] === 2)
        {
            $aop_columns[4] = 0;
            $aop_columns[5] = 0;
            $aop_columns[6] = 0;
        }
        else if ($aop_params['columns_type'] === 3)
        {
            $aop_columns[4] = 0;
            $aop_columns[5] = 0;
        }

        foreach ($account_codes as $account_code) 
        {
            foreach ($aop_columns as $aop_column_key => $aop_column_value) 
            {
                $aop_columns[$aop_column_key] += $this->getXMLValueForColumn($account_code, $aop_column_key, $aop_params);
            }
        }
        
        $new_aop_columns = [];
        foreach ($aop_columns as $key => $value) 
        {
            $new_aop_columns["aop-$aop_code-$key"] = $value;
        }
        
        return $new_aop_columns;
    }
    
    protected function getXMLValueFromAOPs(string $aop_code, array $aops, array $aop_params) : array
    {
        $aop_columns = [];

        if ($aop_params['columns_type'] === 1)
        {
            $aop_columns[3] = 0;
            $aop_columns[4] = 0;
        }
        else if ($aop_params['columns_type'] === 2)
        {
            $aop_columns[4] = 0;
            $aop_columns[5] = 0;
            $aop_columns[6] = 0;
        }
        else if ($aop_params['columns_type'] === 3)
        {
            $aop_columns[4] = 0;
            $aop_columns[5] = 0;
        }

        foreach ($aops as $aop) 
        {
            foreach ($aop_columns as $aop_column_key => $aop_column_value) 
            {
                $aop_columns[$aop_column_key] += $this->xml_values["aop-$aop-$aop_column_key"];
            }
        }
        
        $new_aop_columns = [];
        foreach ($aop_columns as $key => $value) 
        {
            $new_aop_columns["aop-$aop_code-$key"] = $value;
        }
        
        return $new_aop_columns;
    }
    
    private function getXMLValueForColumn(string $account_code, int $aop_column_key, array $aop_params)
    {
        $account_column = $aop_params['column'];
        $account_year = null;
        if ($aop_params['columns_type'] === 1)
        {
            if ($aop_column_key === 3)
            {
                $account_year = $this->curr_year;
            }
            else if ($aop_column_key === 4)
            {
                $account_year = $this->prev_year;
            }
        }
        else if ($aop_params['columns_type'] === 2)
        {
            $account_year = $this->curr_year;
        }
        else if ($aop_params['columns_type'] === 3)
        {
            if ($aop_column_key === 4)
            {
                $account_year = $this->curr_year;
            }
            else if ($aop_column_key === 5)
            {
                $account_year = $this->prev_year;
            }
        }
        
        return $this->getAccoountColumnValue($account_code, $account_year, $account_column);
    }
    
    private function getAccoountColumnValue(string $account_code, Year $year, string $column_name)
    {
        $account = $this->getAccountForYear($account_code, $year);
        
        if (empty($account))
        {
            return 0;
        }
        
        return $account->$column_name;
    }
    
    /**
     * 
     * @param Year $year
     * @return string/int
     */
    private function getActiveEmployeesMonthlyAverageForYear(Year $year)
    {
        $average_value = 0;
        
        if (!empty($year->accounting))
        {
            $employees_count = 0;
            $accounting_months = $year->accounting->getAccountingMonths();
            foreach ($accounting_months as $accounting_month) 
            {
                $employees_count += Employee::model()->count([
                    'model_filter' => [
                        'work_contracts' => [
                            'signed' => true,
                            'active_at' => $accounting_month->base_month
                        ]
                    ]
                ]);
            }
            
            $average_value = round($employees_count / count($accounting_months));
        }
        
        return $average_value;
    }
}
