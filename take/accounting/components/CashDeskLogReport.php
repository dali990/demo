<?php
class CashDeskLogReport extends SIMAReport
{
    protected function getModuleName()
    {
        return 'accounting';
    }
    
    public function getPDF()
    {
        $cash_desk_log = $this->params['cash_desk_log'];
        $template_id = Yii::app()->configManager->get('accounting.templates.cash_desk_log', false);
        if(!empty($template_id))
        {
            $templated_file = TemplatedFile::model()->with('templated_file_data')->findByPk($cash_desk_log->file->id);
            if ($templated_file === null)
            {
                $template_file_id = TemplateController::setTemplate($cash_desk_log->file->id, $template_id);
                $templated_file = TemplatedFile::model()->findByPkWithCheck($template_file_id); 
            }  
            return $templated_file->createTempPdf();
        }
        else 
        {
            return parent::getPDF();
        }
    }
    
    protected function getHTMLContent()
    {
        $cash_desk_log = $this->params['cash_desk_log'];
        $i = 1;
        $account_number = 0;
        $amount_in_sum = 0;
        $amount_out_sum = 0;
        $cash_desk_orders_cnt = count($cash_desk_log->cash_desk_orders);
        $max_while_cycle_exec_time_seconds = ini_get('max_execution_time');
        $html = "";
        foreach ($cash_desk_log->cash_desk_orders as $cash_desk_orders)  
        {
            set_time_limit($max_while_cycle_exec_time_seconds);
            if (!empty($this->params['update_func']))
            {
                call_user_func($this->params['update_func'], round(($i/$cash_desk_orders_cnt)*100, 0, PHP_ROUND_HALF_DOWN));
            }
            foreach ($cash_desk_orders->cash_desk_items as $cash_desk_items)  
            {
                $html .= "<tr>
                    <td>$i</td>
                    <td width='13.2mm'>".($cash_desk_orders->isIN ? $cash_desk_orders->number : "")."</td>
                    <td width='13.2mm'>".(!$cash_desk_orders->isIN ? $cash_desk_orders->number : "")."</td>
                    <td>{$cash_desk_items->text}</td>
                    <td>".SIMAHtml::number_format($cash_desk_items->amount_in)."</td>
                    <td>".SIMAHtml::number_format($cash_desk_items->amount_out)."</td>
                    <td></td>
                </tr>";
                $i++;
                $amount_in_sum += $cash_desk_items->amount_in;
                $amount_out_sum += $cash_desk_items->amount_out;
                $account_number++;
            }
        }
        $cash_desk_log_preview = CashDeskLog::model()->find([
            'model_filter' => [
                'scopes' => 'byDateDESC',
                'date' => ['<', $cash_desk_log->date]
            ]
        ]);
        return [
            'html' => $this->render('html', [
                'cash_desk_log' => $cash_desk_log,
                'amount_in_sum' => SIMAHtml::number_format($amount_in_sum),
                'amount_out_sum' => SIMAHtml::number_format($amount_out_sum),
                'curr_company' => Yii::app()->company,
                'curr_date' => SIMAHtml::DbToUserDate(SIMAHtml::currentDateTime(false)),
                'style_tax_table' => Yii::app()->configManager->get('accounting.display_denomination_table_in_cash_desk_log_report', false) ?  'display: inline;' : 'display: none;',
                'account_number' => $account_number,
                'date_preview' => !empty($cash_desk_log_preview) ? $cash_desk_log_preview->date : $cash_desk_log->date,
                'html' => $html,
            ], true, false),
            'css_file_name' => 'css'
        ];
    }
}