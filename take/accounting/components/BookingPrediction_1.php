<?php

/**
 * Staticka klasa u kojoj se predvidjaju sva proknjizavanja
 */
class BookingPrediction
{
    public static $BOOKING_TYPE_DOCUMENT = 'account_document';
    public static $BOOKING_TYPE_ORDER = 'account_order';
    
    
    /**
     * 
     * @param type $id
     * @param type $style account_document | account_order
     * @return type
     * @throws Exception
     */
    public static function Booking($id, $style, $updateFunction = null)
    {
        if (is_null($updateFunction))
        {
            $updateFunction = function($percent){};
        }
        
        $booking_transactions = [];
        
        if ($style === self::$BOOKING_TYPE_ORDER)
        {
            /**
             * Booking whole account_order
             */
            $_ad = AccountDocument::model()->findByPk($id);
            if (isset($_ad))
            {
                $_ao = $_ad->account_order;
            }
            else
            {
                $_ao = AccountOrder::model()->findByPk($id);
            }
            
            if (isset($_ao))
            {
                $_ao->attachBehavior('DocumentCombination', 'AccountOrderDocumentCombinationBehavior');
                $_ao->hasRegularAccountDocumentsCombination();
                ConnectAccountOrderDocuments::addDocumentsToAccountOrder($_ao->id, $updateFunction);//povezi sve dokument
                $_ao->refresh();//za svaki slucaj da pokupi sve novo iz baze
                $booking_transactions = self::BookingAccountOrder($_ao, $updateFunction);
            }
        }
        else
        {
            /**
             * Booking only one document
             */
            $ad = AccountDocument::model()->findByPk($id);
            if (isset($ad))
            {
                $booking_transactions = self::BookingOneDocument($ad, null, $updateFunction);
            }
        }        
        return $booking_transactions;
    }
    
    
    private static function countDocumentTypes(AccountOrder $ao)
    {
        $_types_cnt = [
            'bank_statements' => 0,
            
            'bills' => 0,
            'non_service_bills' => 0,
            
            'advance_bills' => 0,
            'relief_bills' => 0,
            'unrelief_bills' => 0,
            
            'relief_bills' => 0,
            'unrelief_bills' => 0,
            
            'price_leveling' => 0,
            
            'warehouse_transfers' => 0,
            
            'paycheck_period' => 0,
            'paycheck' => 0,
            
            'cash_desk_log' => 0,
            'cash_desk_order' => 0,
            
            'account_order' => 0,
            
            'fixed_assets_depreciation' => 0,
            
            'total' => 0
        ];
        $_warehouses_from = [];
        $_warehouses_to = [];
        
        foreach ($ao->account_documents as $_ad)
        {
            
            $_types_cnt['total']++;
            if (isset($_ad->file->bank_statement))
            {
                $_types_cnt['bank_statements']++;
            }
            elseif (isset($_ad->file->any_bill))
            {
                $bill = $_ad->file->any_bill;//moze biti da je u tom trenutku  vec storniran
                switch ($bill->bill_type)
                {
                    case Bill::$BILL:
                        $_types_cnt['bills']++;
                        if ($bill->hasWarehouseItems || $bill->hasFixedAssetsItems)
                        {
                            $_types_cnt['non_service_bills']++;
                        }
                        break;
                    case Bill::$ADVANCE_BILL:       $_types_cnt['advance_bills']++; break;
                    case Bill::$RELIEF_BILL :       $_types_cnt['relief_bills']++; break;
                    case Bill::$UNRELIEF_BILL:      $_types_cnt['unrelief_bills']++; break;
                }
            }
            elseif (isset($_ad->file->warehouse_transfer))
            {
                $_types_cnt['warehouse_transfers']++;
                if (
                        in_array($_ad->file->warehouse_transfer->warehouse_from_id, $_warehouses_to)
                        ||
                        in_array($_ad->file->warehouse_transfer->warehouse_to_id, $_warehouses_from)
                    )
                {
                    throw new SIMAWarnException('Ne mogu zajedno da se knjize ulaz i izlaz iz istog magacina');
                }
                $_warehouses_to[] = $_ad->file->warehouse_transfer->warehouse_to_id;
                $_warehouses_from[] = $_ad->file->warehouse_transfer->warehouse_from_id;
            }
            elseif (isset($_ad->file->warehouse_material_price_leveling))
            {
                $_types_cnt['price_leveling']++;
            }
            elseif (isset($_ad->file->account_order))
            {
                $_types_cnt['account_order']++;
            }
            elseif (isset($_ad->file->regular_paycheck_period))
            {
                $_types_cnt['paycheck_period']++;
            }
            elseif (isset($_ad->file->paycheck))
            {
                $_types_cnt['paycheck']++;
            }
            elseif (isset($_ad->file->cash_desk_log))
            {
                $_types_cnt['cash_desk_log']++;
            }
            elseif (isset($_ad->file->cash_desk_order))
            {
                $_types_cnt['cash_desk_order']++;
            }
            elseif (isset($_ad->file->fixed_assets_depreciation))
            {
                $_types_cnt['fixed_assets_depreciation']++;
            }
            else
            {
                //nista. Prosto je ok da postoji nalog za koji nije predvidjeno knjizenje
            }
        }
        
        return $_types_cnt;
    }
    
    /**
     * Booking all documents that are on this account order
     * @param type $id AccountOrder id
     * @return array
     */
    private static function BookingAccountOrder(AccountOrder $ao, $updateFunction)
    {
        $booking_transactions = [];
        
        //prvo proknjizimo sve stornirano
        foreach ($ao->canceled_account_documents as $_ad)
        {
            $booking_transactions[] = self::BookingCanceledDocument($_ad, $updateFunction);
        }
        /**
         * first: count_types
         */
        $_types_cnt = self::countDocumentTypes($ao);
        
        /**
         * rules
         * 
         * if bank_account/advance_bills, only bank_accounts/advance bills can be added
         */
        if ($_types_cnt['bank_statements'] + $_types_cnt['advance_bills'] > 0)
        {
            if ($_types_cnt['total'] - $_types_cnt['advance_bills'] - $_types_cnt['bank_statements'] - $_types_cnt['account_order'] > 0)
            {
                throw new SIMAWarnException('Izvod iz banke moze da se rasknjizi zajedno samo sa avansnim racunima');
            }
            
            $booking_transactions[] = self::BookingAllDocuments($ao->account_documents, 'bank_statement', $updateFunction);
            $booking_transactions[] = self::BookingAllDocuments($ao->account_documents, 'advance_bill', $updateFunction);
            
        }
        //KNJIZENJE NIVELACIJA
        elseif ($_types_cnt['price_leveling'] > 0)
        {
            if ($_types_cnt['price_leveling'] > 1)
            {
                throw new SIMAWarnException('Svaka nivelacija mora posebno da se knjizi');
            }
            if ($_types_cnt['total'] - $_types_cnt['price_leveling'] > 0)
            {
                throw new SIMAWarnException('Nivelacije se knjize zasebno');
            }
            foreach ($ao->account_documents as $_ad)
            {
                $booking_transactions[] = BookingPredictionWarehouseMaterialPriceLeveling::Booking($_ad);
            }
        }
        else if ($_types_cnt['bills'] > 0)
        {
            //da li se knjize zavisni troskovi
//            if($_types_cnt['non_service_bills'] > 0)
//            {
//                $carry_saldo = 0;//KNJIZE SE ZAVISNI
//                $with_carry = true;//KNJIZE SE ZAVISNI
//            }
//            else
//            {
                $carry_saldo = null; //NE KNJIZE SE ZAVISNI
                $with_carry = false; //NE KNJIZE SE ZAVISNI
//            }
            //knjizenje zavisnih troskova
            if ($with_carry)
            {
                foreach ($ao->account_documents as $_ad)
                {                
                    if (isset($_ad->file->any_bill))
                    {
                        $booking_transactions[] = BookingPredictionBill::Booking($_ad, $carry_saldo, true);
                    }
                }
            }
            
            //izracunavanje zavisnih troskova
            //dovlacenje broja stavki za zavisne troskove
            $inc_costs = null;
            if ($with_carry)
            {
                $inc_costs = 0;
                $_algorithm = Yii::app()->configManager->get('accounting.warehouse.incremental_costs_algorithm');
        
                $_devide_with = 0;
                foreach ($ao->account_documents as $_ad)
                {
                    if (isset($_ad->file->any_bill) && 
                            ($_ad->file->any_bill->hasWarehouseItems || $_ad->file->any_bill->hasFixedAssetsItems)
                        )
                    {
                        if ($_algorithm === 'PER_QNT')
                        {
                            $_devide_with = $_ad->file->any_bill->warehouse_bill_items_quantity + $_ad->file->any_bill->fixed_assets_bill_items_quantity;
                        }
                        else
                        {
                            $_devide_with = $_ad->file->any_bill->warehouse_bill_items_count + $_ad->file->any_bill->fixed_assets_bill_items_count;
                        }
                        
                    }

                }
                if ($_devide_with == 0)
                {
                    throw new SIMAWarnException('racuni nemaju stavke');
                }
                else
                {
                    $inc_costs = $carry_saldo/$_devide_with;
                }
                
            }
            
            /**
             * TODO: MilosS: 15.1.2019 stavljeno je da prijemnice idu prve, ali treba srediti sortiranje dokumenata koji se zajedno knjize
             */
            foreach ($ao->account_documents as $_ad)
            {
                if (isset($_ad->file->warehouse_transfer))
                {
                    $booking_transactions[] = BookingPredictionWarehouseTransfer::Booking($_ad);
                }

            }
            
            //knjizenje osnovnih racuna i popunjavanje prijemnica/kalkulacija
            foreach ($ao->account_documents as $_ad)
            {
                if (isset($_ad->file->any_bill) && $_ad->file->any_bill->isBill)
                {
                    $booking_transactions[] = BookingPredictionBill::Booking($_ad, $inc_costs);
                }

            }
            
            /**
             * dodato jer nista na nalogu sto nije bio racun nije knjizeno
             * dodatno, sve sto je u tabeli racuna a nije BILL, knjizeno je kao racun ukoliko ima obicnih racuna...
             * velika greska
             */
            foreach ($ao->account_documents as $_ad)
            {
                if (
                        !(isset($_ad->file->any_bill) && $_ad->file->any_bill->isBill)
                        &&
                        !isset($_ad->file->warehouse_transfer) 
                    )
                {
                    $booking_transactions[] = self::BookingOneDocument($_ad, null, $updateFunction);
                }
            }
            
        }
        else
        {
            $booking_transactions[] = self::BookingAllDocuments($ao->account_documents, null, $updateFunction);
            
        }
        
        return SIMAMisc::joinArrays($booking_transactions);
    }
    
    private static function BookingCanceledDocument(AccountDocument $ad, $updateFunction)
    {
        $booking_transactions = [];
        
        foreach ($ad->account_transactions as $_trans)
        {
            $tr = new AccountTransaction();
            $tr->account_id = $_trans->account_id;
            $tr->account_document_id = $ad->canceled_account_order_id;
            $tr->comment = '!! - '.$_trans->comment;
            $tr->debit = -$_trans->debit;
            $tr->credit = -$_trans->credit;
            $booking_transactions[] = $tr;
        }
        
        return $booking_transactions;
    }
    
    private static function BookingAllDocuments($account_documents, $document_type = null, $updateFunction)
    {
        $booking_transactions = [];
        foreach ($account_documents as $_ad)
        {
            $booking_transactions[] = self::BookingOneDocument($_ad, $document_type, $updateFunction);
        }
        return SIMAMisc::joinArrays($booking_transactions);
    }
    
    private static $booking_class_map = [
        'bank_statement' => 'BookingPredictionBankStatement',
        'advance_bill' => 'BookingPredictionAdvanceBill',
        'debt_take_over' => 'BookingPredictionDebtTakeOver',
        'warehouse_transfer' => 'BookingPredictionWarehouseTransfer',
        'bank_statement' => 'BookingPredictionBankStatement',
        'regular_paycheck_period' => 'BookingPredictionPaycheckPeriod',
        'cash_desk_log' => 'BookingPredictionCashDeskLog',
        'vat_month' => 'BookingPredictionVatMonth',
        'start_order_for_year' => 'BookingPredictionYear',
        'profit_calc_order_for_year' => 'BookingPredictionYear',
        'finish_order_for_year' => 'BookingPredictionYear',
        'fixed_assets_depreciation' => 'BookingPredictionFixedAssets',
        'fixed_asset_deactivation_decision' => 'BookingPredictionFixedAssets',
//        'paycheck_period' => 'BookingPredictionPaycheckPeriod',
    ];
    
    /**
     * 
     * @param AccountDocument $id
     * @return array
     * @throws Exception
     */
    private static function BookingOneDocument(AccountDocument $ad, $document_type, $updateFunction)
    {
        $booking_transactions = [];
        if (!is_null($document_type))
        {
            $document_types = [$document_type];
        }
        else
        {
            $document_types = array_keys(self::$booking_class_map);
        }
        $booked = false;
        foreach ($document_types as $relation)
        {
            if (isset($ad->file->$relation) || isset($ad->$relation))
            {
                $class_name = self::$booking_class_map[$relation];
                $booking_transactions = $class_name::Booking($ad, $updateFunction);
                $booked = true;
            }
        }
        if (!$booked)
        {
            if (isset($ad->file->any_bill)
                    && (in_array($document_type, ['bill','advance_bill']) || is_null($document_type))
                    && isset($ad->file->any_bill)
                )
            {
                $bill = $ad->file->any_bill;
                if ($bill->isBill)
                {
                    $booking_transactions = BookingPredictionBill::Booking($ad);
                }
                elseif ($bill->isAdvanceBill)
                {
                    $booking_transactions = BookingPredictionAdvanceBill::Booking($ad);
                }
                elseif ($bill->isReliefBill)
                {
                    $booking_transactions = BookingPredictionReliefBill::Booking($ad);
                }
                elseif ($bill->isUnReliefBill)
                {
                    $booking_transactions = BookingPredictionReliefBill::Booking($ad);
                }

            }
            elseif  (isset($ad->file->warehouse_transfer) 
                    && ($document_type == 'warehouse_transfer' || is_null($document_type))
                )
            {
                $booking_transactions = BookingPredictionWarehouseTransfer::Booking($ad);
            }
            elseif (isset($ad->file->warehouse_material_price_leveling) 
                    && ($document_type == 'warehouse_material_price_leveling' || is_null($document_type))
                )
            {
                $booking_transactions = BookingPredictionWarehouseMaterialPriceLeveling::Booking($ad);
            }
        }
        
        return $booking_transactions;
        
    }
    
    /**
     * Spajanje vise transakcija koje imaju isti konto, komentar i debit/credit u jednu
     * ukoliko su nadjene
     * @param array $booking_transactions - niz transakcija pre spajanja
     * @return array - niz transakcija posle spajanja
     */
    public static function MergeTransactions($booking_transactions, $options = [])
    {
        $new_array = [];
        foreach ($booking_transactions as $key => $_transaction)
        {
            $found = false;
            foreach ($new_array as $_new_transaction)
            {
                if ($_new_transaction->addTransaction($_transaction))
                {
                    $found = true;
                    break;
                }
            }
            if (!$found)
            {
                $new_array[] = $_transaction;
            }
        }
        return $new_array;
        
    }
}