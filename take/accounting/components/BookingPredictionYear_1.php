<?php

/**
 * Staticka klasa u kojoj se predvidjaju sva proknjizavanja
 */
class BookingPredictionYear
{
    public static function Booking($document, $updateFunction)
    {
        $booking_transactions = [];
        if (isset($document->start_order_for_year))
        {
            $a_year = $document->start_order_for_year;
            $_statement_comment = 'Početno stanje';
            $booking_transactions = self::BookingStart($document, $a_year, $_statement_comment, $updateFunction);
        }
        elseif (isset($document->profit_calc_order_for_year))
        {
            $a_year = $document->profit_calc_order_for_year;
            $_statement_comment = 'Obračun dobiti';
            $booking_transactions = self::BookingProfitCalc($document, $a_year, $_statement_comment, $updateFunction);
        }
        elseif (isset($document->finish_order_for_year))
        {
            $a_year = $document->finish_order_for_year;
            $_statement_comment = 'Završno knjiženje';
            $booking_transactions = self::BookingFinish($document, $a_year, $_statement_comment, $updateFunction);
        }        
//        $booking_transactions = self::BookingVatMonth($document, $a_month, $_statement_comment);
        
        
        return $booking_transactions;
    }

    /**
     * 
     * @param AccountDocument $document
     * @param AccountingYear $a_year
     * @param type $_statement_comment
     * @return array
     */
    private static function BookingStart(AccountDocument $document, AccountingYear $a_year, $_statement_comment, $updateFunction)
    {
        $booking_transactions = [];
        $year = $a_year->base_year;
        $prev_year = $year->prev();
        if (is_null($prev_year) || is_null($prev_year->accounting))
        {
            throw new SIMAWarnException(Yii::t('AccountingModule.BookingPrediction','PreviousYearDoesntExists'));
        }

        $prev_finish_order = $prev_year->accounting->finish_booking_order;
        if (is_null($prev_finish_order))
        {
            throw new SIMAWarnException(Yii::t('AccountingModule.BookingPrediction','NoFinalBookingForPreviousYear'));
        }
        $i = 1;
        $i_max = count($prev_finish_order->account_transactions);
        foreach ($prev_finish_order->account_transactions as $old_transaction)
        {
            $new_account = $old_transaction->account->forYear($year);
            $tr = new AccountTransaction();
            $tr->account_id = $new_account->id;
            $tr->account_document_id = $document->id;
            $tr->comment = $_statement_comment;
            $tr->debit = $old_transaction->credit;
            $tr->credit = $old_transaction->debit;
            $booking_transactions[] = $tr;
            $i++;
            $updateFunction( ($i/$i_max) * 100 );
        }
        
        return $booking_transactions;
    }
    
    /**
     * 
     * @param AccountDocument $document
     * @param AccountingYear $a_year
     * @param type $_statement_comment
     * @return array
     */
    private static function BookingProfitCalc(AccountDocument $document, AccountingYear $a_year, $_statement_comment, $updateFunction)
    {
        $booking_transactions = [];
        
        //godina iz naloga za knjizenje
        $_year = $a_year->base_year;
        $_year_id = $a_year->id;
        
        $_outcome_sum_account = Account::getFromParam('accounting.codes.profit_calcs.outcome_sum',$_year)->getLeafAccount();
        $_income_sum_account = Account::getFromParam('accounting.codes.profit_calcs.income_sum',$_year)->getLeafAccount();
        $_outcome_sum = 0.0;
        $_income_sum = 0.0;
//        
        $_outcome_codes_selector = Yii::app()->configManager->get('accounting.codes.profit_calcs.outcome_selector',false);
        $_income_codes_selector = Yii::app()->configManager->get('accounting.codes.profit_calcs.income_selector',false);
        
        $income_and_outcome_account = Account::getFromParam('accounting.codes.profit_calcs.income_and_outcome', $_year)->getLeafAccount();
        $transfer_total_result_account = Account::getFromParam('accounting.codes.profit_calcs.transfer_total_result', $_year)->getLeafAccount();
        $profit_account = Account::getFromParam('accounting.codes.profit_calcs.profit', $_year)->getLeafAccount();
        $loss_account = Account::getFromParam('accounting.codes.profit_calcs.loss', $_year)->getLeafAccount();
        $tax_outcome_of_period_account = Account::getFromParam('accounting.codes.profit_calcs.tax_outcome_of_period', $_year)->getLeafAccount();
//        $delayed_tax_outcome_and_income_of_period_account = Account::getFromParam('accounting.codes.profit_calcs.delayed_tax_outcome_and_income_of_period', $_year)->getLeafAccount();
        $transfer_profit_or_loss_account = Account::getFromParam('accounting.codes.profit_calcs.transfer_profit_or_loss', $_year)->getLeafAccount();
//        $obligation_for_tax_from_result_account = Account::getFromParam('accounting.codes.profit_calcs.obligation_for_tax_from_result', $_year)->getLeafAccount();
        $unallocated_profit_for_the_current_year_account = Account::getFromParam('accounting.codes.profit_calcs.unallocated_profit_for_the_current_year', $_year)->getLeafAccount();
        $loss_of_the_current_year_account = Account::getFromParam('accounting.codes.profit_calcs.loss_of_the_current_year', $_year)->getLeafAccount();
          
        /**
         * TROSAK
         */
        $outcome_crit = new SIMADbCriteria([
            'Model' => 'Account',
            'model_filter' => [
                'year' => ['ids' => $_year_id],
                'code' => $_outcome_codes_selector
            ]
        ]);

        $exclude_orders = [];
        if (isset($a_year->finish_booking_order_id))
        {
             $exclude_orders[] = $a_year->finish_booking_order_id;
        }
        if (isset($a_year->profit_calc_booking_order_id))
        {
             $exclude_orders[] = $a_year->profit_calc_booking_order_id;
        }
        
        $outcome_accounts = Account::model()
            ->withDCCurrents($_year_id, [
                'exclude_orders' => $exclude_orders
            ])
            ->onlyLeafAccounts() 
            ->byName()
            ->findAll($outcome_crit);
        
        $i1 = 1;
        $i1_max = count($outcome_accounts);
        foreach ($outcome_accounts as $account)
        {
            if (!SIMAMisc::areEqual($account->saldo, 0.0))
            {
                $tr = new AccountTransaction();
                $tr->account_id = $account->id;
                $tr->account_document_id = $document->id;
                $tr->comment = $_statement_comment;
                $tr->credit = $account->saldo;
                $booking_transactions[] = $tr;
                
                $_outcome_sum += $account->saldo;
            }
            $updateFunction(($i1/$i1_max)*50);
            $i1++;
        }
        
        $tr1_sum = new AccountTransaction();
        $tr1_sum->account_id = $_outcome_sum_account->id;
        $tr1_sum->account_document_id = $document->id;
        $tr1_sum->comment = $_statement_comment;
        $tr1_sum->debit = $_outcome_sum;
        $booking_transactions[] = $tr1_sum;

        
        /**
         * PRIHOD
         */
        $income_crit = new SIMADbCriteria([
            'Model' => 'Account',
            'model_filter' => [
                'year' => ['ids' => $_year_id],
                'code' => $_income_codes_selector
            ]
        ]);
        $income_accounts = Account::model()
            ->withDCCurrents($_year_id, [
                'exclude_orders' => $exclude_orders
            ])
            ->onlyLeafAccounts()
            ->byName()
            ->findAll($income_crit);
        
        $i2 = 1;
        $i2_max = count($outcome_accounts);
        foreach ($income_accounts as $account)
        {
            if (!SIMAMisc::areEqual($account->saldo, 0.0))
            {
                $tr = new AccountTransaction();
                $tr->account_id = $account->id;
                $tr->account_document_id = $document->id;
                $tr->comment = $_statement_comment;
                $tr->debit = -$account->saldo;
                $booking_transactions[] = $tr;
                
                $_income_sum -= $account->saldo;
            }
            $updateFunction(50 + ($i2/$i2_max)*50);
            $i2++;
        }
        
        $tr2_sum = new AccountTransaction();
        $tr2_sum->account_id = $_income_sum_account->id;
        $tr2_sum->account_document_id = $document->id;
        $tr2_sum->comment = $_statement_comment;
        $tr2_sum->credit = $_income_sum;
        $booking_transactions[] = $tr2_sum;
        
        //zatvara se 599
        $_outcome_sum_account_close = new AccountTransaction();
        $_outcome_sum_account_close->account_id = $_outcome_sum_account->id;
        $_outcome_sum_account_close->account_document_id = $document->id;
        $_outcome_sum_account_close->comment = $_statement_comment;
        $_outcome_sum_account_close->credit = $tr1_sum->debit;
        $booking_transactions[] = $_outcome_sum_account_close;
        
        //prenos 599 na 710
        $income_and_outcome_account_open = new AccountTransaction();
        $income_and_outcome_account_open->account_id = $income_and_outcome_account->id;
        $income_and_outcome_account_open->account_document_id = $document->id;
        $income_and_outcome_account_open->comment = $_statement_comment;
        $income_and_outcome_account_open->debit = $_outcome_sum_account_close->credit;
        $booking_transactions[] = $income_and_outcome_account_open;
        
        //zatvara se 699
        $_income_sum_account_close = new AccountTransaction();
        $_income_sum_account_close->account_id = $_income_sum_account->id;
        $_income_sum_account_close->account_document_id = $document->id;
        $_income_sum_account_close->comment = $_statement_comment;
        $_income_sum_account_close->debit = $tr2_sum->credit;
        $booking_transactions[] = $_income_sum_account_close;
        
        //prenos 699 na 710
        $income_and_outcome_account_open2 = new AccountTransaction();
        $income_and_outcome_account_open2->account_id = $income_and_outcome_account->id;
        $income_and_outcome_account_open2->account_document_id = $document->id;
        $income_and_outcome_account_open2->comment = $_statement_comment;
        $income_and_outcome_account_open2->credit = $_income_sum_account_close->debit;
        $booking_transactions[] = $income_and_outcome_account_open2;

        
        //DOBITAK - ako je 599 manje od 699
        if ($tr1_sum->debit < $tr2_sum->credit)
        {
            //ako je potrazna strana 710 veca od dugovne
            if ($income_and_outcome_account_open2->credit > $income_and_outcome_account_open->debit)
            {
                //zatvara se 710
                $income_and_outcome_account_close = new AccountTransaction();
                $income_and_outcome_account_close->account_id = $income_and_outcome_account->id;
                $income_and_outcome_account_close->account_document_id = $document->id;
                $income_and_outcome_account_close->comment = $_statement_comment;
                $income_and_outcome_account_close->debit = abs($income_and_outcome_account_open->debit - $income_and_outcome_account_open2->credit);
                $booking_transactions[] = $income_and_outcome_account_close;

                //prenos 710 na 712
                $transfer_total_result_account_open = new AccountTransaction();
                $transfer_total_result_account_open->account_id = $transfer_total_result_account->id;
                $transfer_total_result_account_open->account_document_id = $document->id;
                $transfer_total_result_account_open->comment = $_statement_comment;        
                $transfer_total_result_account_open->credit = $income_and_outcome_account_close->debit;
                $booking_transactions[] = $transfer_total_result_account_open;
            }

            //zatvara se 712
            $transfer_total_result_account_close = new AccountTransaction();
            $transfer_total_result_account_close->account_id = $transfer_total_result_account->id;
            $transfer_total_result_account_close->account_document_id = $document->id;
            $transfer_total_result_account_close->comment = $_statement_comment;
            $transfer_total_result_account_close->debit = $transfer_total_result_account_open->credit;
            $booking_transactions[] = $transfer_total_result_account_close;
            
            //prenos 712 na 720000
            $profit_account_open = new AccountTransaction();
            $profit_account_open->account_id = $profit_account->id;
            $profit_account_open->account_document_id = $document->id;
            $profit_account_open->comment = $_statement_comment;        
            $profit_account_open->credit = $transfer_total_result_account_close->debit;
            $booking_transactions[] = $profit_account_open;

            //zatvara se 720000
            $profit_account_close = new AccountTransaction();
            $profit_account_close->account_id = $profit_account->id;
            $profit_account_close->account_document_id = $document->id;
            $profit_account_close->comment = $_statement_comment;
            $profit_account_close->debit = $profit_account_open->credit;
            $booking_transactions[] = $profit_account_close;

            //prenos 720000(15%) na 721
            $tax_outcome_of_period_account_open = new AccountTransaction();
            $tax_outcome_of_period_account_open->account_id = $tax_outcome_of_period_account->id;
            $tax_outcome_of_period_account_open->account_document_id = $document->id;
            $tax_outcome_of_period_account_open->comment = $_statement_comment;        
            $tax_outcome_of_period_account_open->credit = $profit_account_close->debit * 15/100;
            $booking_transactions[] = $tax_outcome_of_period_account_open;

            //prenos 720000(85%) na 724
            $transfer_profit_or_loss_account_open = new AccountTransaction();
            $transfer_profit_or_loss_account_open->account_id = $transfer_profit_or_loss_account->id;
            $transfer_profit_or_loss_account_open->account_document_id = $document->id;
            $transfer_profit_or_loss_account_open->comment = $_statement_comment;        
            $transfer_profit_or_loss_account_open->credit = $profit_account_close->debit * 85/100;
            $booking_transactions[] = $transfer_profit_or_loss_account_open;

            //zatvara se 724
            $transfer_profit_or_loss_account_close = new AccountTransaction();
            $transfer_profit_or_loss_account_close->account_id = $transfer_profit_or_loss_account->id;
            $transfer_profit_or_loss_account_close->account_document_id = $document->id;
            $transfer_profit_or_loss_account_close->comment = $_statement_comment;
            $transfer_profit_or_loss_account_close->debit = $transfer_profit_or_loss_account_open->credit;
            $booking_transactions[] = $transfer_profit_or_loss_account_close;

            //prenos 724 na 341
            $unallocated_profit_for_the_current_year_account_open = new AccountTransaction();
            $unallocated_profit_for_the_current_year_account_open->account_id = $unallocated_profit_for_the_current_year_account->id;
            $unallocated_profit_for_the_current_year_account_open->account_document_id = $document->id;
            $unallocated_profit_for_the_current_year_account_open->comment = $_statement_comment;        
            $unallocated_profit_for_the_current_year_account_open->credit = $transfer_profit_or_loss_account_close->debit;
            $booking_transactions[] = $unallocated_profit_for_the_current_year_account_open;
        }
        //GUBITAK - ako je 699 manje od 599
        else if ($tr1_sum->debit > $tr2_sum->credit)
        {
            //ako je potrazna strana 710 manja od dugovne
            if ($income_and_outcome_account_open2->credit < $income_and_outcome_account_open->debit)
            {
                //zatvara se 710
                $income_and_outcome_account_close = new AccountTransaction();
                $income_and_outcome_account_close->account_id = $income_and_outcome_account->id;
                $income_and_outcome_account_close->account_document_id = $document->id;
                $income_and_outcome_account_close->comment = $_statement_comment;
                $income_and_outcome_account_close->credit = abs($income_and_outcome_account_open->debit - $income_and_outcome_account_open2->credit);
                $booking_transactions[] = $income_and_outcome_account_close;

                //prenos 710 na 712
                $transfer_total_result_account_open = new AccountTransaction();
                $transfer_total_result_account_open->account_id = $transfer_total_result_account->id;
                $transfer_total_result_account_open->account_document_id = $document->id;
                $transfer_total_result_account_open->comment = $_statement_comment;        
                $transfer_total_result_account_open->debit = $income_and_outcome_account_close->credit;
                $booking_transactions[] = $transfer_total_result_account_open;
            }
            
            //zatvara se 712
            $transfer_total_result_account_close = new AccountTransaction();
            $transfer_total_result_account_close->account_id = $transfer_total_result_account->id;
            $transfer_total_result_account_close->account_document_id = $document->id;
            $transfer_total_result_account_close->comment = $_statement_comment;
            $transfer_total_result_account_close->credit = $transfer_total_result_account_open->debit;
            $booking_transactions[] = $transfer_total_result_account_close;

            //prenos 712 na 720100
            $loss_account_open = new AccountTransaction();
            $loss_account_open->account_id = $loss_account->id;
            $loss_account_open->account_document_id = $document->id;
            $loss_account_open->comment = $_statement_comment;        
            $loss_account_open->debit = $transfer_total_result_account_close->credit;
            $booking_transactions[] = $loss_account_open;

            //zatvara se 720100
            $loss_account_close = new AccountTransaction();
            $loss_account_close->account_id = $loss_account->id;
            $loss_account_close->account_document_id = $document->id;
            $loss_account_close->comment = $_statement_comment;
            $loss_account_close->credit = $loss_account_open->debit;
            $booking_transactions[] = $loss_account_close;

            //prenos 720100 na 724
            $transfer_profit_or_loss_account_open = new AccountTransaction();
            $transfer_profit_or_loss_account_open->account_id = $transfer_profit_or_loss_account->id;
            $transfer_profit_or_loss_account_open->account_document_id = $document->id;
            $transfer_profit_or_loss_account_open->comment = $_statement_comment;        
            $transfer_profit_or_loss_account_open->debit = $loss_account_close->credit;
            $booking_transactions[] = $transfer_profit_or_loss_account_open;

            //zatvara se 724
            $transfer_profit_or_loss_account_close = new AccountTransaction();
            $transfer_profit_or_loss_account_close->account_id = $transfer_profit_or_loss_account->id;
            $transfer_profit_or_loss_account_close->account_document_id = $document->id;
            $transfer_profit_or_loss_account_close->comment = $_statement_comment;
            $transfer_profit_or_loss_account_close->credit = $transfer_profit_or_loss_account_open->debit;
            $booking_transactions[] = $transfer_profit_or_loss_account_close;
  
            //prenos 724 na 351
            $loss_of_the_current_year_account_open = new AccountTransaction();
            $loss_of_the_current_year_account_open->account_id = $loss_of_the_current_year_account->id;
            $loss_of_the_current_year_account_open->account_document_id = $document->id;
            $loss_of_the_current_year_account_open->comment = $_statement_comment;        
            $loss_of_the_current_year_account_open->debit = $transfer_profit_or_loss_account_close->credit;
            $booking_transactions[] = $loss_of_the_current_year_account_open;
        }

        return $booking_transactions;
    }
    
    /**
     * 
     * @param AccountDocument $document
     * @param AccountingYear $a_year
     * @param type $_statement_comment
     * @return array
     */
    private static function BookingFinish(AccountDocument $document, AccountingYear $a_year, $_statement_comment, $updateFunction)
    {
        $booking_transactions = [];
        
        //godina iz naloga za knjizenje
        $_year = $a_year->base_year;
        $_year_id = $a_year->id;
        
        $outcome_crit = new SIMADbCriteria([
            'Model' => 'Account',
            'model_filter' => [
                'year' => ['ids' => $_year_id],
                'code' => '*'
            ]
        ]);

        $exclude_orders = [];
        if (isset($a_year->finish_booking_order_id))
        {
             $exclude_orders[] = $a_year->finish_booking_order_id;
        }
        
        $outcome_accounts = Account::model()
            ->withDCCurrents($_year_id, [
                'exclude_orders' => $exclude_orders
            ])
            ->onlyLeafAccounts() 
            ->byName()
            ->findAll($outcome_crit);
        
        $i1 = 1;
        $i1_max = count($outcome_accounts);
        foreach ($outcome_accounts as $account)
        {
            if (!SIMAMisc::areEqual($account->saldo, 0.0))
            {
                
                $tr = new AccountTransaction();
                $tr->account_id = $account->id;
                $tr->account_document_id = $document->id;
                $tr->comment = $_statement_comment;
                if ($account->saldo > 0)
                {
                    $tr->credit = $account->saldo;
                }
                else
                {
                    $tr->debit = -$account->saldo;
                }
                
                $booking_transactions[] = $tr;
            }
            $updateFunction(($i1/$i1_max)*50);
            $i1++;
        }

        return $booking_transactions;
    }
}
