<?php

class POPDVBankStatementCalc extends SIMAComponent implements ArrayAccess
{
    private $booking_type = null;
    private $month = null;
    private $params = [];
    private $account_document = null;
    
    public function __construct(Month $month, AccountDocument $account_document = null,string $booking_type = POPDVExport::BOOKING_TYPE_BOTH)
    {
        parent::__construct();
        
        if (!in_array($booking_type, [
                POPDVExport::BOOKING_TYPE_BOTH,
                POPDVExport::BOOKING_TYPE_REGULER,
                POPDVExport::BOOKING_TYPE_CANCELED
        ]))
        {
            throw new SIMAException('NISTE NAVELI DOBAR TIP KNJZENJA ZA POPDV');
        }
        
        $this->booking_type = $booking_type;
        
        $this->account_document = $account_document;        
        $this->month = $month;        
        
        $this->calcParams();
    }
    
    private function calcParams()
    {
        $bank_commission_payment_type = Yii::app()->configManager->get('accounting.bank_commission_payment_type',false);
        if (empty($bank_commission_payment_type))
        {
            Yii::app()->raiseNote('postavite nacin placanja za proviziju banke');
        }
        else
        {
            $model_filter = [
                'payment_type' => ['ids' => $bank_commission_payment_type],
                'bank_statement' => [
                    'date' => ($this->month->firstDay()->day_date).' <> '.($this->month->lastDay()->day_date)
                ]
            ];
            
            if (!is_null($this->account_document))
            {
                $model_filter['bank_statement']['ids'] = $this->account_document->id;
            }
            
            $payments_sum = Payment::model()->find([
                'model_filter' => $model_filter,
                'select' => 'sum(amount) as amount'
            ]);
            $this->params['8v2'] = round($payments_sum->amount);
        }
    }
    
    /**
     * implementacija ArrayAccess
     */

    public function offsetExists ( $offset ) : bool
    {
        return in_array($offset, POPDVLists::PARAMS);
    }
    public function offsetGet ( $offset )
    {
        return isset($this->params[$offset])?$this->params[$offset]:0.00;
    }
    public function offsetSet ( $offset , $value ) : void
    {
        if (isset($this->params[$offset]))
        {
            //trenutno ne postavljamo 
//            $this->params[$offset] = $value;
        }
        else
        {
            throw new SIMAException("kod $offset ne postoji u POPDV prijavi");
        }
    }
    public function offsetUnset ( $offset ) : void
    {
        $this->offsetSet($offset, 0);
    }
    
}

