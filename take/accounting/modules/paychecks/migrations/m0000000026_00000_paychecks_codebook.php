<?php

class m0000000026_00000_paychecks_codebook extends CDbMigration
{
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        Yii::app()->db->createCommand(
<<<'SIMAMIGRATESQL'
CREATE TABLE accounting.paychecks_codebook
(
  id serial NOT NULL,
  start_use date NOT NULL,
  end_use date,
  pio_empl numeric(10,4),
  zdr_empl numeric(10,4),
  nez_empl numeric(10,4),
  tax_empl numeric(10,4),
  tax_release numeric(8,0),
  pio_comp numeric(10,4),
  zdr_comp numeric(10,4),
  nez_comp numeric(10,4),
  min_tax_base numeric(10,2),
  max_tax_base numeric(10,2),
  CONSTRAINT paychecks_codebook_pkey PRIMARY KEY (id)
);

CREATE TRIGGER i_am_called_by_everyone
  AFTER INSERT OR UPDATE OR DELETE
  ON accounting.paychecks_codebook
  FOR EACH ROW
  EXECUTE PROCEDURE public.i_am_called_by_everyone();
SIMAMIGRATESQL
        )->execute();
    }

    public function safeDown()
    {
//        Yii::app()->db->createCommand(
//<<<'SIMAMIGRATESQL'
//
//SIMAMIGRATESQL
//        )->execute();
        echo "m0000000026_00000_paychecks_codebook does not support migration down.\n";
        return false;
    }
}