<?php

class m0000000010_00017_add_ignoring_additionals extends CDbMigration
{
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        Yii::app()->db->createCommand(
"
ALTER TABLE accounting.paychecks
  ADD COLUMN additionals_ignored_calendar_days numeric(2,0);
ALTER TABLE accounting.paychecks
  ADD COLUMN additionals_ignored_hours numeric(10,2);


"
        )->execute();
    }

    public function safeDown()
    {
//        Yii::app()->db->createCommand(
//"
//
//"
//        )->execute();
        echo "m0000000010_00017_add_ignoring_additionals does not support migration down.\n";
        return false;
    }
}