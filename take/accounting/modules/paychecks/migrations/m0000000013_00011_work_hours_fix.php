<?php

class m0000000013_00011_work_hours_fix extends CDbMigration
{
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        Yii::app()->db->createCommand(
<<<'SIMAMIGRATESQL'

update accounting.paychecks set regular_work_value = worked_hours_value + work_on_state_holiday_value + overtime_value
	where
	worked_hours_value >0
	and
	regular_work_value = 0;


update accounting.paychecks set regular_work_hours = worked_hours + work_on_state_holiday_hours + overtime_hours
	where
	worked_hours >0
	and
	regular_work_hours = 0;

SIMAMIGRATESQL
        )->execute();
    }

    public function safeDown()
    {
//        Yii::app()->db->createCommand(
//<<<'SIMAMIGRATESQL'
//
//SIMAMIGRATESQL
//        )->execute();
        echo "m0000000013_00011_work_hours_fix does not support migration down.\n";
        return false;
    }
}