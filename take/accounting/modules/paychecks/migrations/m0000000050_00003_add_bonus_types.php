<?php

class m0000000050_00003_add_bonus_types extends CDbMigration
{
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        Yii::app()->db->createCommand(
<<<'SIMAMIGRATESQL'

ALTER TABLE accounting.paycheck_group_bonuses
  ADD COLUMN group_bonus_type text NOT NULL DEFAULT 'EQUAL_AMOUNT';
  
ALTER TABLE accounting.paycheck_group_bonuses
  ADD CONSTRAINT group_bonus_type_check CHECK (group_bonus_type = ANY (ARRAY['EQUAL_AMOUNT'::text, 'EQUAL_SUM'::text, 'SALARY_POINTS_SUM'::text, 'BONUS_POINTS_SUM'::text]));

CREATE TABLE accounting.bonus_paygrades
(
   id serial NOT NULL, 
   name text NOT NULL, 
   points numeric(10,0) NOT NULL, 
   PRIMARY KEY (id)
);

ALTER TABLE public.employees
  ADD COLUMN paycheck_bonus_paygrade_id integer;
ALTER TABLE public.employees
  ADD FOREIGN KEY (paycheck_bonus_paygrade_id) REFERENCES accounting.bonus_paygrades (id) ON UPDATE CASCADE ON DELETE RESTRICT;

   

SIMAMIGRATESQL
        )->execute();
    }

    public function safeDown()
    {
//        Yii::app()->db->createCommand(
//<<<'SIMAMIGRATESQL'
//
//SIMAMIGRATESQL
//        )->execute();
        echo "m0000000050_00003_add_bonus_types does not support migration down.\n";
        return false;
    }
}