<?php

class m0000000004_00003_paycheck_store_empdisp_payouttype extends EDbMigration
{
    public function safeUp()
    {
        ini_set('memory_limit', '200M');
        
        Yii::app()->db->createCommand(
'
ALTER TABLE accounting.paychecks ADD COLUMN employee_display_name text;
ALTER TABLE accounting.paychecks ADD COLUMN employee_payout_type text;
'
        )->execute();
        
        $employees = Employee::model()->findAll();
        $employees_count = count($employees);
        $cur_emp_count = 0;
        foreach($employees as $employee)
        {
            $cur_emp_count++;
            $paychecks = $employee->paychecks;
            $paychecks_count = count($paychecks);
            $cur_pay_count = 0;
            foreach($paychecks as $paycheck)
            {
                $cur_pay_count++;
                echo "\r$cur_emp_count/$employees_count - $cur_pay_count/$paychecks_count";
                $paycheck->employee_display_name = $employee->DisplayName;
                $paycheck->employee_payout_type = $employee->payout_type;
                $paycheck->save();
                unset($paycheck);
            }
            
            unset($paychecks);
            unset($employee->paychecks);
            unset($employee);
        }
        
        echo "\n";
        
        Yii::app()->db->createCommand(
'
ALTER TABLE accounting.paychecks ALTER COLUMN employee_display_name SET NOT NULL;
ALTER TABLE accounting.paychecks ALTER COLUMN employee_payout_type SET NOT NULL;
'
            )->execute();
    }

    public function safeDown()
    {
        Yii::app()->db->createCommand(
'
ALTER TABLE accounting.paychecks DROP COLUMN employee_display_name;
ALTER TABLE accounting.paychecks DROP COLUMN employee_payout_type;
'
        )->execute();
    }
}