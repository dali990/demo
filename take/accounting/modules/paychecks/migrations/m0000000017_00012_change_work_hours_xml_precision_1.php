<?php

class m0000000017_00012_change_work_hours_xml_precision extends CDbMigration
{
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        Yii::app()->db->createCommand(
<<<'SIMAMIGRATESQL'

ALTER TABLE accounting.paycheck_xmls
   ALTER COLUMN effective_hours TYPE numeric(5,2);
   

SIMAMIGRATESQL
        )->execute();
    }

    public function safeDown()
    {
//        Yii::app()->db->createCommand(
//<<<'SIMAMIGRATESQL'
//
//SIMAMIGRATESQL
//        )->execute();
        echo "m0000000017_00012_change_work_hours_xml_precision does not support migration down.\n";
        return false;
    }
}