<?php

class m0000000041_00003_paycheckperiod_tufefix_paramadd extends CDbMigration
{
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        Yii::app()->db->createCommand(
<<<'SIMAMIGRATESQL'
ALTER TABLE accounting.paycheck_periods ADD COLUMN calculated_with_tufe_fix BOOLEAN DEFAULT FALSE;
SIMAMIGRATESQL
        )->execute();
    }

    public function safeDown()
    {
//        Yii::app()->db->createCommand(
//<<<'SIMAMIGRATESQL'
//
//SIMAMIGRATESQL
//        )->execute();
        echo "m0000000041_00003_paycheckperiod_tufefix_paramadd does not support migration down.\n";
        return false;
    }
}