<?php

class m0000000055_00003_conf_company_municipality_code_model extends CDbMigration
{
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        Yii::app()->db->createCommand(
<<<'SIMAMIGRATESQL'
UPDATE base.configuration_parameters 
SET value=(SELECT '"'||id||'"' 
            FROM public.municipalities 
            WHERE code=(SELECT trim(both '"' from value) 
                        FROM base.configuration_parameters 
                        WHERE mapping_id=15)) 
WHERE mapping_id=15;   
SIMAMIGRATESQL
        )->execute();
    }

    public function safeDown()
    {
//        Yii::app()->db->createCommand(
//<<<'SIMAMIGRATESQL'
//
//SIMAMIGRATESQL
//        )->execute();
        echo "m0000000055_00003_conf_company_municipality_code_model does not support migration down.\n";
        return false;
    }
}