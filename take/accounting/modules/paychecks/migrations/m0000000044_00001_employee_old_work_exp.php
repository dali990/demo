<?php

class m0000000044_00001_employee_old_work_exp extends CDbMigration
{
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        Yii::app()->db->createCommand(
<<<'SIMAMIGRATESQL'
ALTER TABLE employees DROP COLUMN year_experience;
ALTER TABLE employees ADD COLUMN old_work_exp numeric(2,0) NOT NULL DEFAULT 0;
ALTER TABLE accounting.paychecks ADD COLUMN old_work_exp numeric(2,0) NOT NULL DEFAULT 0;
ALTER TABLE accounting.paychecks ADD COLUMN work_contracts_work_exp numeric(2,0) NOT NULL DEFAULT 0;
SIMAMIGRATESQL
        )->execute();
    }

    public function safeDown()
    {
//        Yii::app()->db->createCommand(
//<<<'SIMAMIGRATESQL'
//
//SIMAMIGRATESQL
//        )->execute();
        echo "m0000000044_00001_employee_old_work_exp does not support migration down.\n";
        return false;
    }
}