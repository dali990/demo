<?php

class m0000000026_00001_remove_month_hours extends CDbMigration
{
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        Yii::app()->db->createCommand(
<<<'SIMAMIGRATESQL'

ALTER TABLE accounting.months
   ALTER COLUMN hours DROP NOT NULL;
   

SIMAMIGRATESQL
        )->execute();
    }

    public function safeDown()
    {
//        Yii::app()->db->createCommand(
//<<<'SIMAMIGRATESQL'
//
//SIMAMIGRATESQL
//        )->execute();
        echo "m0000000026_00001_remove_month_hours does not support migration down.\n";
        return false;
    }
}