<?php

class m0000000021_00003_store_work_position_name extends CDbMigration
{
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        Yii::app()->db->createCommand(
<<<'SIMAMIGRATESQL'
ALTER TABLE accounting.paychecks
    ADD COLUMN work_position_name text;
SIMAMIGRATESQL
        )->execute();
    }

    public function safeDown()
    {
//        Yii::app()->db->createCommand(
//<<<'SIMAMIGRATESQL'
//
//SIMAMIGRATESQL
//        )->execute();
        echo "m0000000021_00003_store_work_position_name does not support migration down.\n";
        return false;
    }
}