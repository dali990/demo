<?php

class m0000000062_00003_events_absences_working_percentage extends CDbMigration
{
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        Yii::app()->db->createCommand(
<<<'SIMAMIGRATESQL'
ALTER TABLE accounting.paycheck_additionals ALTER COLUMN hours TYPE numeric(6,2);
SIMAMIGRATESQL
        )->execute();
    }

    public function safeDown()
    {
//        Yii::app()->db->createCommand(
//<<<'SIMAMIGRATESQL'
//
//SIMAMIGRATESQL
//        )->execute();
        echo "m0000000062_00003_events_absences_working_percentage does not support migration down.\n";
        return false;
    }
}