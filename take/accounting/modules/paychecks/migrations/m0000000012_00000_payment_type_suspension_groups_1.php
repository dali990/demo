<?php

class m0000000012_00000_payment_type_suspension_groups extends CDbMigration
{
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        Yii::app()->db->createCommand(
<<<'SIMAMIGRATESQL'

ALTER TABLE accounting.suspension_groups
  ADD COLUMN payment_type text;

ALTER TABLE accounting.suspension_groups
  ADD CONSTRAINT suspension_groups_payment_type 
  CHECK (payment_type = ANY (ARRAY['ARBITRARLY'::text, 'AUTO_ALL'::text, 'AUTO_INSTALLMENT'::text]));
  
UPDATE accounting.suspension_groups SET payment_type = 'AUTO_ALL';
  
ALTER TABLE accounting.suspension_groups
   ALTER COLUMN payment_type SET NOT NULL;

ALTER TABLE accounting.suspension_groups
  ADD COLUMN installment_amount numeric(10,2) NOT NULL DEFAULT 0;


SIMAMIGRATESQL
        )->execute();
    }

    public function safeDown()
    {
//        Yii::app()->db->createCommand(
//<<<'SIMAMIGRATESQL'
//
//SIMAMIGRATESQL
//        )->execute();
        echo "m0000000012_00000_payment_type_suspension_groups does not support migration down.\n";
        return false;
    }
}