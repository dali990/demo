<?php

class m0000000010_00086_work_on_state_holiday extends CDbMigration
{
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        Yii::app()->db->createCommand(
"
ALTER TABLE accounting.paychecks 
    ADD COLUMN value_per_hour numeric(10,4),
    ADD COLUMN work_on_state_holiday_hours numeric(10,4) DEFAULT 0,
    ADD COLUMN work_on_state_holiday_percentage INTEGER NOT NULL DEFAULT 100,
    ADD COLUMN work_on_state_holiday_value numeric(10,2) NOT NULL DEFAULT 0;
"
        )->execute();
    }

    public function safeDown()
    {
//        Yii::app()->db->createCommand(
//"
//
//"
//        )->execute();
        echo "m0000000010_00086_work_on_state_holiday does not support migration down.\n";
        return false;
    }
}