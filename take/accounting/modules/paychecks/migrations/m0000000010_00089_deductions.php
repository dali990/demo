<?php

class m0000000010_00089_deductions extends CDbMigration
{
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        Yii::app()->db->createCommand(
"
CREATE TABLE accounting.paycheck_deductions
(
  id serial NOT NULL,
  paycheck_id integer NOT NULL,
  deduction_type text NOT NULL,
  deduction_name text,
  amount numeric(10,4) NOT NULL,
  CONSTRAINT paycheck_deductions_pkey PRIMARY KEY (id),
  CONSTRAINT paycheck_deductions_paycheck_id_fkey FOREIGN KEY (paycheck_id)
      REFERENCES accounting.paychecks (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE RESTRICT,
  CONSTRAINT paycheck_deductions_deduction_type_check CHECK (deduction_type = ANY (ARRAY['HOT_MEAL'::text, 'OTHER'::text]))
);

ALTER TABLE accounting.paychecks 
    ADD COLUMN deductions_sum numeric(10,4) NOT NULL DEFAULT 0,
    ADD COLUMN hotmeal_fieldwork_per_hour numeric(8,2) NOT NULL DEFAULT 0;
"
        )->execute();
    }

    public function safeDown()
    {
//        Yii::app()->db->createCommand(
//"
//
//"
//        )->execute();
        echo "m0000000010_00089_deductions does not support migration down.\n";
        return false;
    }
}