<?php

class m0000000010_00088_overtime extends CDbMigration
{
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        Yii::app()->db->createCommand(
"
ALTER TABLE accounting.paychecks 
    ADD COLUMN overtime_hours numeric(8,2) NOT NULL DEFAULT 0,
    ADD COLUMN overtime_percentage numeric(5,2) NOT NULL DEFAULT 0,
    ADD COLUMN overtime_value numeric(10,4) NOT NULL DEFAULT 0,
    ADD COLUMN regular_work_hours numeric(8,2) NOT NULL DEFAULT 0,
    ADD COLUMN regular_work_value numeric(10,4) NOT NULL DEFAULT 0,
    ADD COLUMN hotmeal_fieldwork_value numeric(10,4) NOT NULL DEFAULT 0;
"
        )->execute();
    }

    public function safeDown()
    {
//        Yii::app()->db->createCommand(
//"
//
//"
//        )->execute();
        echo "m0000000010_00088_overtime does not support migration down.\n";
        return false;
    }
}