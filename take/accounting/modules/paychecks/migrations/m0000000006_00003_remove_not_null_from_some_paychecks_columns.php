<?php

class m0000000006_00003_remove_not_null_from_some_paychecks_columns extends EDbMigration
{
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        Yii::app()->db->createCommand(
'
ALTER TABLE accounting.paychecks
	ALTER COLUMN employee_display_name DROP NOT NULL,
	ALTER COLUMN employee_payout_type DROP NOT NULL;
'
            )->execute();
    }

    public function safeDown()
    {
        Yii::app()->db->createCommand(
'
ALTER TABLE accounting.paychecks
	ALTER COLUMN employee_display_name SET NOT NULL,
	ALTER COLUMN employee_payout_type SET NOT NULL;
'
            )->execute();
    }
}