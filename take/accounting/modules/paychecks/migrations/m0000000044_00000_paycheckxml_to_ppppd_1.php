<?php

class m0000000044_00000_paycheckxml_to_ppppd extends CDbMigration
{
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        Yii::app()->db->createCommand(
<<<'SIMAMIGRATESQL'
ALTER TABLE accounting.paycheck_xmls ADD COLUMN ppppd_id integer;

ALTER TABLE accounting.ppppds DROP CONSTRAINT ppppds_creation_type_check;
ALTER TABLE accounting.ppppds
  ADD CONSTRAINT ppppds_creation_type_check CHECK (creation_type = ANY (ARRAY['USER_UPLOAD'::text, 'SIMA_GENERATE'::text, 'USER_MANUAL'::text]));
SIMAMIGRATESQL
        )->execute();
    }

    public function safeDown()
    {
//        Yii::app()->db->createCommand(
//<<<'SIMAMIGRATESQL'
//
//SIMAMIGRATESQL
//        )->execute();
        echo "m0000000044_00000_paycheckxml_to_ppppd does not support migration down.\n";
        return false;
    }
}