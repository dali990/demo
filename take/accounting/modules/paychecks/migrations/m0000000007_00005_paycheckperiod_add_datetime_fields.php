<?php

class m0000000007_00005_paycheckperiod_add_datetime_fields extends EDbMigration
{
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        Yii::app()->db->createCommand(
'
ALTER TABLE accounting.paycheck_periods 
    ADD COLUMN last_modification_time timestamp without time zone,
    ADD COLUMN last_calculation_start_time timestamp without time zone,
    ADD COLUMN last_calculation_end_time timestamp without time zone;

UPDATE accounting.paycheck_periods 
    SET last_modification_time=(TIMESTAMP \'epoch\' + 0 * INTERVAL \'1 second\'), 
	last_calculation_start_time=(TIMESTAMP \'epoch\' + 0 * INTERVAL \'1 second\'), 
	last_calculation_end_time=(TIMESTAMP \'epoch\' + 0 * INTERVAL \'1 second\');

ALTER TABLE accounting.paycheck_periods
	ALTER COLUMN last_modification_time SET NOT NULL;
'
            )->execute();
    }

    public function safeDown()
    {
        Yii::app()->db->createCommand(
'
ALTER TABLE accounting.paycheck_periods 
    DROP COLUMN last_modification_time,
    DROP COLUMN last_calculation_start_time,
    DROP COLUMN last_calculation_end_time;
'
            )->execute();
    }
}