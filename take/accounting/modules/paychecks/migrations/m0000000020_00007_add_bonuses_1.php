<?php

class m0000000020_00007_add_bonuses extends CDbMigration
{
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        Yii::app()->db->createCommand(
<<<'SIMAMIGRATESQL'

CREATE TABLE accounting.paycheck_group_bonuses
(
   id serial NOT NULL, 
   name text NOT NULL, 
   value numeric(20,2) NOT NULL,
   paycheck_period_id integer,
   description text,
   PRIMARY KEY (id),
   FOREIGN KEY (paycheck_period_id) REFERENCES accounting.paycheck_periods (id) ON UPDATE CASCADE ON DELETE RESTRICT
) 
WITH (
  OIDS = FALSE
);
   

CREATE TABLE accounting.paycheck_bonuses
(
   id serial NOT NULL, 
   name text, 
   paycheck_id integer NOT NULL, 
   value numeric(20,2) NOT NULL,
   group_bonus_id integer,
   paycheck_period_id integer NOT NULL,
   description text,
   PRIMARY KEY (id), 
   FOREIGN KEY (paycheck_id) REFERENCES accounting.paychecks (id) ON UPDATE CASCADE ON DELETE RESTRICT,
   FOREIGN KEY (group_bonus_id) REFERENCES accounting.paycheck_group_bonuses (id) ON UPDATE CASCADE ON DELETE RESTRICT,
   FOREIGN KEY (paycheck_period_id) REFERENCES accounting.paycheck_periods (id) ON UPDATE CASCADE ON DELETE RESTRICT
) 
WITH (
  OIDS = FALSE
);


SIMAMIGRATESQL
        )->execute();
    }

    public function safeDown()
    {
//        Yii::app()->db->createCommand(
//<<<'SIMAMIGRATESQL'
//
//SIMAMIGRATESQL
//        )->execute();
        echo "m0000000020_00007_add_bonuses does not support migration down.\n";
        return false;
    }
}