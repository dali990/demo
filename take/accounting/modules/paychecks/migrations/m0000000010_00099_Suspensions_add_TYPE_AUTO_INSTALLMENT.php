<?php

class m0000000010_00099_Suspensions_add_TYPE_AUTO_INSTALLMENT extends CDbMigration
{
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        Yii::app()->db->createCommand(
"
ALTER TABLE accounting.suspensions DROP CONSTRAINT suspensions_payment_type;

ALTER TABLE accounting.suspensions
  ADD CONSTRAINT suspensions_payment_type CHECK (payment_type = ANY (ARRAY['ARBITRARLY'::text, 'AUTO_ALL'::text, 'AUTO_INSTALLMENT'::text]));
"
        )->execute();
    }

    public function safeDown()
    {
//        Yii::app()->db->createCommand(
//"
//
//"
//        )->execute();
        echo "m0000000010_00099_Suspensions_add_TYPE_AUTO_INSTALLMENT does not support migration down.\n";
        return false;
    }
}