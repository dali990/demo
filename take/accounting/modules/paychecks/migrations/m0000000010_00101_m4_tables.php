<?php

class m0000000010_00101_m4_tables extends CDbMigration
{
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        Yii::app()->db->createCommand(
"
        CREATE TABLE accounting.m4mf_rolls
        (
          id integer NOT NULL,
          roll_number integer NOT NULL,
          position_number integer NOT NULL,
          year_id integer NOT NULL,
          CONSTRAINT m4mf_rolls_pkey PRIMARY KEY (id),
          CONSTRAINT m4mf_rolls_id_fkey FOREIGN KEY (id)
                REFERENCES files.files (id) MATCH SIMPLE
                ON UPDATE CASCADE ON DELETE RESTRICT,
          CONSTRAINT m4mf_rolls_year_id_fkey FOREIGN KEY (year_id)
                REFERENCES base.years (id) MATCH SIMPLE
                ON UPDATE CASCADE ON DELETE RESTRICT,
          CONSTRAINT m4mf_rolls_uniq_key UNIQUE(year_id,roll_number,position_number)
        );
        CREATE TRIGGER i_am_called_by_everyone
            AFTER INSERT OR UPDATE OR DELETE
            ON accounting.m4mf_rolls
            FOR EACH ROW
            EXECUTE PROCEDURE public.i_am_called_by_everyone();
            
        CREATE TABLE accounting.m4employee_rows
        (
          id serial NOT NULL,
          m4mf_roll_id integer NOT NULL,
          employee_id integer NOT NULL,
          employee_name text NOT NULL,
          jmbg character varying(13) NOT NULL,
          work_exp_months character varying(2) NOT NULL,
          work_exp_days character varying(2) NOT NULL,
          bruto numeric(10,2) NOT NULL,
          pio_doprinos numeric(10,2) NOT NULL,
          zdrav_bruto numeric(10,2) NOT NULL,
          zdrav_pio_doprinos numeric(10,2) NOT NULL,
          CONSTRAINT m4employee_rows_pkey PRIMARY KEY (id),
          CONSTRAINT m4employee_rows_m4mf_roll_id_fkey FOREIGN KEY (m4mf_roll_id)
                REFERENCES accounting.m4mf_rolls (id) MATCH SIMPLE
                ON UPDATE CASCADE ON DELETE RESTRICT,
          CONSTRAINT m4employee_rows_employee_id_fkey FOREIGN KEY (employee_id)
                REFERENCES public.employees (id) MATCH SIMPLE
                ON UPDATE CASCADE ON DELETE RESTRICT,
          CONSTRAINT m4employee_rows_uniq_key UNIQUE(m4mf_roll_id,employee_id)
        );
        CREATE TRIGGER i_am_called_by_everyone
            AFTER INSERT OR UPDATE OR DELETE
            ON accounting.m4employee_rows
            FOR EACH ROW
            EXECUTE PROCEDURE public.i_am_called_by_everyone();
"
        )->execute();
    }

    public function safeDown()
    {
        Yii::app()->db->createCommand(
"
        DROP TABLE accounting.m4employee_rows;
        DROP TABLE accounting.m4mf_rolls;
"
        )->execute();
//        echo "m0000000010_00101_m4_tables does not support migration down.\n";
//        return false;
    }
}