<?php

class m0000000042_00001_resize_paycheck_numbers extends CDbMigration
{
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        Yii::app()->db->createCommand(
<<<'SIMAMIGRATESQL'

ALTER TABLE accounting.paycheck_periods
   ALTER COLUMN hours_percentage TYPE numeric(10,2);
   

ALTER TABLE accounting.paychecks
    ALTER COLUMN amount_neto TYPE numeric(14,2),
    ALTER COLUMN amount_bruto TYPE numeric(14,2),
    ALTER COLUMN amount_total TYPE numeric(14,2),
    ALTER COLUMN regres TYPE numeric(14,4),
    ALTER COLUMN pio_empl TYPE numeric(14,4),
    ALTER COLUMN zdr_empl TYPE numeric(14,4),
    ALTER COLUMN nez_empl TYPE numeric(14,4),
    ALTER COLUMN tax_empl TYPE numeric(14,4),
    ALTER COLUMN pio_comp TYPE numeric(14,4),
    ALTER COLUMN zdr_comp TYPE numeric(14,4),
    ALTER COLUMN nez_comp TYPE numeric(14,4),
    ALTER COLUMN pio_empl_perc TYPE numeric(14,2),
    ALTER COLUMN zdr_empl_perc TYPE numeric(14,2),
    ALTER COLUMN nez_empl_perc TYPE numeric(14,2),
    ALTER COLUMN tax_empl_perc TYPE numeric(14,2),
    ALTER COLUMN pio_comp_perc TYPE numeric(14,2),
    ALTER COLUMN zdr_comp_perc TYPE numeric(14,2),
    ALTER COLUMN nez_comp_perc TYPE numeric(14,2),
    ALTER COLUMN bonus TYPE numeric(14,2),
    ALTER COLUMN worked_hours_value TYPE numeric(14,2),
    ALTER COLUMN past_exp_value TYPE numeric(14,2),
    ALTER COLUMN hot_meal_value TYPE numeric(14,2),
    ALTER COLUMN tax_base TYPE numeric(14,2),
    ALTER COLUMN base_bruto TYPE numeric(14,2),
    ALTER COLUMN base_tax_base TYPE numeric(14,2),
    ALTER COLUMN base_tax_empl TYPE numeric(14,2),
    ALTER COLUMN base_pio TYPE numeric(14,2),
    ALTER COLUMN base_zdr TYPE numeric(14,2),
    ALTER COLUMN base_nez TYPE numeric(14,2),
    ALTER COLUMN work_sum TYPE numeric(14,2),
    ALTER COLUMN performance_value TYPE numeric(14,2),
    ALTER COLUMN min_tax_base TYPE numeric(14,2),
    ALTER COLUMN month_min_tax_base TYPE numeric(14,2),
    ALTER COLUMN probationer_value TYPE numeric(14,2),
    ALTER COLUMN additionals_hours TYPE numeric(14,2),
    ALTER COLUMN personal_points TYPE numeric(14,0),
    ALTER COLUMN worked_hours_fix_for_average_hours TYPE numeric(14,4),
    ALTER COLUMN xml_bruto TYPE numeric(14,2),
    ALTER COLUMN xml_tax_base TYPE numeric(14,2),
    ALTER COLUMN xml_tax_empl TYPE numeric(14,2),
    ALTER COLUMN xml_pio TYPE numeric(14,2),
    ALTER COLUMN xml_zdr TYPE numeric(14,2),
    ALTER COLUMN xml_nez TYPE numeric(14,2),
    ALTER COLUMN additionals_used_tax_release TYPE numeric(14,2),
    ALTER COLUMN additionals_used_doprinosi TYPE numeric(14,2),
    ALTER COLUMN base_pio_empl TYPE numeric(14,2),
    ALTER COLUMN base_pio_comp TYPE numeric(14,2),
    ALTER COLUMN base_zdr_empl TYPE numeric(14,2),
    ALTER COLUMN base_zdr_comp TYPE numeric(14,2),
    ALTER COLUMN base_nez_empl TYPE numeric(14,2),
    ALTER COLUMN base_nez_comp TYPE numeric(14,2),
    ALTER COLUMN prev_bruto_sum TYPE numeric(14,2),
    ALTER COLUMN prev_tax_base TYPE numeric(14,2),
    ALTER COLUMN prev_tax_empl TYPE numeric(14,2),
    ALTER COLUMN prev_doprinosi_base TYPE numeric(14,2),
    ALTER COLUMN prev_pio_empl TYPE numeric(14,2),
    ALTER COLUMN prev_zdr_empl TYPE numeric(14,2),
    ALTER COLUMN prev_nez_empl TYPE numeric(14,2),
    ALTER COLUMN prev_pio_comp TYPE numeric(14,2),
    ALTER COLUMN prev_zdr_comp TYPE numeric(14,2),
    ALTER COLUMN prev_nez_comp TYPE numeric(14,2),
    ALTER COLUMN suspensions_sum TYPE numeric(14,2),
    ALTER COLUMN amount_neto_for_payout TYPE numeric(14,2),
    ALTER COLUMN prev_tax_release TYPE numeric(14,2),
    ALTER COLUMN value_per_hour TYPE numeric(14,4),
    ALTER COLUMN overtime_value TYPE numeric(14,4),
    ALTER COLUMN regular_work_value TYPE numeric(14,4),
    ALTER COLUMN hotmeal_fieldwork_value TYPE numeric(14,4),
    ALTER COLUMN hours_percentage TYPE numeric(10,2),
    ALTER COLUMN deductions_sum TYPE numeric(14,4);   

SIMAMIGRATESQL
        )->execute();
    }

    public function safeDown()
    {
//        Yii::app()->db->createCommand(
//<<<'SIMAMIGRATESQL'
//
//SIMAMIGRATESQL
//        )->execute();
        echo "m0000000042_00001_resize_paycheck_numbers does not support migration down.\n";
        return false;
    }
}