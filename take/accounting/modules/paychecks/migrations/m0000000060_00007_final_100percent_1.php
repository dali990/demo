<?php

class m0000000060_00007_final_100percent extends CDbMigration
{
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        Yii::app()->db->createCommand(
<<<'SIMAMIGRATESQL'
ALTER TABLE accounting.paycheck_periods
  ADD CONSTRAINT paycheck_periods_pp_type_check CHECK (pp_type = ANY (ARRAY['REGULAR'::text, 'FINAL'::text, 'BONUS'::text]));
   
ALTER TABLE accounting.paycheck_periods
  ADD CONSTRAINT paycheck_periods_pp_type_hours_percentage_check CHECK (
CASE
    WHEN pp_type = 'FINAL'::text THEN hours_percentage = 100
    ELSE true
END);
SIMAMIGRATESQL
        )->execute();
    }

    public function safeDown()
    {
//        Yii::app()->db->createCommand(
//<<<'SIMAMIGRATESQL'
//
//SIMAMIGRATESQL
//        )->execute();
        echo "m0000000060_00007_final_100percent does not support migration down.\n";
        return false;
    }
}