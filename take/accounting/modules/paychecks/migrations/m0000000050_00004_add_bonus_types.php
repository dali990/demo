<?php

class m0000000050_00004_add_bonus_types extends CDbMigration
{
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        Yii::app()->db->createCommand(
<<<'SIMAMIGRATESQL'

ALTER TABLE accounting.paycheck_group_bonuses DROP CONSTRAINT group_bonus_type_check;   

ALTER TABLE accounting.paycheck_group_bonuses
    ADD CONSTRAINT group_bonus_type_check CHECK (group_bonus_type = ANY (ARRAY[
                'EQUAL_AMOUNT'::text, 
                'EQUAL_SUM'::text, 
                'SALARY_POINTS_SUM'::text, 
                'SALARY_POINTS'::text, 
                'BONUS_POINTS_SUM'::text
    ]));


SIMAMIGRATESQL
        )->execute();
    }

    public function safeDown()
    {
//        Yii::app()->db->createCommand(
//<<<'SIMAMIGRATESQL'
//
//SIMAMIGRATESQL
//        )->execute();
        echo "m0000000050_00004_add_bonus_types does not support migration down.\n";
        return false;
    }
}