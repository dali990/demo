<?php

class m0000000042_00002_codebook_additional_columns extends CDbMigration
{
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        Yii::app()->db->createCommand(
<<<'SIMAMIGRATESQL'
CREATE OR REPLACE FUNCTION accounting.temp_create_paycheck_codebooks()
  RETURNS void AS
$BODY$
DECLARE
	acc_month_rec RECORD;
	start_date date;
	end_date date;
BEGIN
	FOR acc_month_rec IN (select * from accounting.months)
	LOOP
		start_date := (acc_month_rec.year || '-' || acc_month_rec.month || '-01')::date;
		end_date := (date_trunc('month', start_date) + interval '1 month' - interval '1 day')::date;

		INSERT INTO accounting.paychecks_codebook(start_use, end_use, pio_empl, zdr_empl, nez_empl, tax_empl, tax_release, pio_comp, zdr_comp, nez_comp, min_tax_base)
		VALUES (start_date, end_date, acc_month_rec.pio_empl, acc_month_rec.zdr_empl, acc_month_rec.nez_empl, acc_month_rec.tax_empl, acc_month_rec.tax_release, acc_month_rec.pio_comp, 
				acc_month_rec.zdr_comp, acc_month_rec.nez_comp, acc_month_rec.min_tax_base);
	END LOOP;
END;
$BODY$
  LANGUAGE plpgsql;
select accounting.temp_create_paycheck_codebooks();
DROP FUNCTION accounting.temp_create_paycheck_codebooks();
SIMAMIGRATESQL
        )->execute();
    }

    public function safeDown()
    {
//        Yii::app()->db->createCommand(
//<<<'SIMAMIGRATESQL'
//
//SIMAMIGRATESQL
//        )->execute();
        echo "m0000000042_00002_codebook_additional_columns does not support migration down.\n";
        return false;
    }
}