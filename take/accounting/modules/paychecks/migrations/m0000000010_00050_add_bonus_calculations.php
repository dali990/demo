<?php

class m0000000010_00050_add_bonus_calculations extends CDbMigration
{
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        Yii::app()->db->createCommand(
"
ALTER TABLE accounting.paycheck_periods
  ADD COLUMN pp_type text;

update accounting.paycheck_periods set pp_type='REGULAR' where is_final=false;
update accounting.paycheck_periods set pp_type='FINAL' where is_final=true;

ALTER TABLE accounting.paycheck_periods
  DROP COLUMN is_final;


"
        )->execute();
    }

    public function safeDown()
    {
//        Yii::app()->db->createCommand(
//"
//
//"
//        )->execute();
        echo "m0000000010_00050_add_bonus_calculations does not support migration down.\n";
        return false;
    }
}