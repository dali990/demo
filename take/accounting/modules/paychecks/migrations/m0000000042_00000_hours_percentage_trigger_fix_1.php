<?php

class m0000000042_00000_hours_percentage_trigger_fix extends CDbMigration
{
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        Yii::app()->db->createCommand(
<<<'SIMAMIGRATESQL'

CREATE OR REPLACE FUNCTION accounting.paycheck_periods_before_insert_update()
  RETURNS trigger AS
$BODY$
DECLARE
	sum_hours_percentage numeric(6,2);
	sum_tax_release_used numeric(8,0);
	new_tax_release_used numeric(8,0);
	new_ordering_number INTEGER;
BEGIN
/*
	IF NEW.hours_percentage IS NULL
	THEN
		SELECT SUM(hours_percentage) FROM accounting.paycheck_periods WHERE month_id=NEW.month_id AND id!=NEW.id
		INTO sum_hours_percentage;

		IF sum_hours_percentage IS NULL
		THEN
			sum_hours_percentage := 0;
		END IF;

		NEW.hours_percentage := 100-sum_hours_percentage;
	END IF;
*/
	IF NEW.tax_release_used IS NULL
	THEN
		SELECT SUM(tax_release_used) FROM accounting.paycheck_periods WHERE month_id=NEW.month_id AND id!=NEW.id
		INTO sum_tax_release_used;

		IF sum_tax_release_used IS NULL
		THEN
			sum_tax_release_used := 0;
		END IF;
		
		SELECT tax_release-sum_tax_release_used FROM accounting.months WHERE id=NEW.month_id
		INTO new_tax_release_used;

		IF new_tax_release_used IS NULL OR new_tax_release_used < 0
		THEN
			new_tax_release_used := 0;
		END IF;

		NEW.tax_release_used := new_tax_release_used;
	END IF;
	IF NEW.ordering_number IS NULL
	THEN
		SELECT MAX(ordering_number)+1 FROM accounting.paycheck_periods WHERE month_id=NEW.month_id AND id!=NEW.id
		INTO new_ordering_number;

		IF new_ordering_number IS NULL
		THEN
			new_ordering_number := 1;
		END IF;

		NEW.ordering_number := new_ordering_number;
	END IF;

	RETURN NEW;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;   

SIMAMIGRATESQL
        )->execute();
    }

    public function safeDown()
    {
//        Yii::app()->db->createCommand(
//<<<'SIMAMIGRATESQL'
//
//SIMAMIGRATESQL
//        )->execute();
        echo "m0000000042_00000_hours_percentage_trigger_fix does not support migration down.\n";
        return false;
    }
}