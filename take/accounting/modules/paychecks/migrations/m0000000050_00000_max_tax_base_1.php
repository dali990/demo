<?php

class m0000000050_00000_max_tax_base extends CDbMigration
{
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        Yii::app()->db->createCommand(
<<<'SIMAMIGRATESQL'
ALTER TABLE accounting.paychecks ADD COLUMN month_max_contribution_base numeric(10,2);
ALTER TABLE accounting.paychecks ADD COLUMN month_doprinosi_base numeric(10,4);
ALTER TABLE accounting.paychecks ADD COLUMN bonus_bruto numeric(10,4);
ALTER TABLE accounting.paychecks ADD COLUMN work_contract_hours_percent numeric(10,4);
ALTER TABLE accounting.paychecks ADD COLUMN dependent_working_percent numeric(10,4);
ALTER TABLE accounting.paychecks ADD COLUMN is_by_max_tax_base BOOLEAN NOT NULL DEFAULT FALSE;

--update accounting.paychecks_codebook set max_tax_base=min_tax_base where max_tax_base is null;

ALTER TABLE accounting.paychecks_codebook RENAME COLUMN min_tax_base TO min_contribution_base;
ALTER TABLE accounting.paychecks_codebook RENAME COLUMN max_tax_base TO max_contribution_base;
ALTER TABLE accounting.months RENAME COLUMN min_tax_base TO min_contribution_base;
ALTER TABLE accounting.paychecks RENAME COLUMN min_tax_base TO month_min_contribution_base;
SIMAMIGRATESQL
        )->execute();
    }

    public function safeDown()
    {
//        Yii::app()->db->createCommand(
//<<<'SIMAMIGRATESQL'
//
//SIMAMIGRATESQL
//        )->execute();
        echo "m0000000050_00000_max_tax_base does not support migration down.\n";
        return false;
    }
}