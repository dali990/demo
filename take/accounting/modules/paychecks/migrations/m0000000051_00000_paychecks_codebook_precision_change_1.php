<?php

class m0000000051_00000_paychecks_codebook_precision_change extends CDbMigration
{
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        Yii::app()->db->createCommand(
<<<'SIMAMIGRATESQL'
ALTER TABLE accounting.paychecks_codebook
ALTER COLUMN pio_empl TYPE NUMERIC(5,2),
ALTER COLUMN zdr_empl TYPE NUMERIC(5,2),
ALTER COLUMN nez_empl TYPE NUMERIC(5,2),
ALTER COLUMN tax_empl TYPE NUMERIC(5,2),
ALTER COLUMN tax_release TYPE NUMERIC(8,2),
ALTER COLUMN pio_comp TYPE NUMERIC(5,2),
ALTER COLUMN zdr_comp TYPE NUMERIC(5,2), 
ALTER COLUMN nez_comp TYPE NUMERIC(5,2)
SIMAMIGRATESQL
        )->execute();
    }

    public function safeDown()
    {
//        Yii::app()->db->createCommand(
//<<<'SIMAMIGRATESQL'
//
//SIMAMIGRATESQL
//        )->execute();
        echo "m0000000051_00000_paychecks_codebook_precision_change does not support migration down.\n";
        return false;
    }
}