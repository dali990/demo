<?php

return array(
    'paycheck_additional_types' => array(
        array(
            'value' => PaycheckAdditional::$TYPE_SICK_LEAVE_INJURY_AT_WORK,
            'display' => Yii::t('PaychecksModule.PaycheckAdditional', 'SICK_LEAVE_INJURY_AT_WORK'),
            'separate_field_in_xml' => true,
            'for_payout' => true,
            'percentage' => 100,
            'OZ' => true,
            'refund' => false
        ),
        array(
            'value' => PaycheckAdditional::$TYPE_SICK_LEAVE_PREGNANCY_TILL_30,
            'display' => Yii::t('PaychecksModule.PaycheckAdditional', 'SICK_LEAVE_PREGNANCY_TILL_30'),
            'separate_field_in_xml' => true,
            'for_payout' => true,
            'percentage' => 100,
            'OZ' => true,
            'refund' => false
        ),
        array(
            'value' => PaycheckAdditional::$TYPE_SICK_LEAVE_PREGNANCY_OVER_30,
            'display' => Yii::t('PaychecksModule.PaycheckAdditional', 'SICK_LEAVE_PREGNANCY_OVER_30'),
            'separate_field_in_xml' => true,
            'for_payout' => false,
            'percentage' => 100,
            'OZ' => false,
            'refund' => true
        ),
        array(
            'value' => PaycheckAdditional::$TYPE_SICK_LEAVE_MATERNITY,
            'display' => Yii::t('PaychecksModule.PaycheckAdditional', 'SICK_LEAVE_MATERNITY'),
            'separate_field_in_xml' => true,
            'for_payout' => true,
            'percentage' => 100,
            'OZ' => false,
            'refund' => false,
            'default_svp' => 206000
        ),
        array(
            'value' => PaycheckAdditional::$TYPE_SICK_LEAVE_CHILD_CARE_TILL_3,
            'display' => Yii::t('PaychecksModule.PaycheckAdditional', 'SICK_LEAVE_CHILD_CARE_TILL_3'),
            'separate_field_in_xml' => true,
            'for_payout' => true,
            'percentage' => 100,
            'OZ' => false,
            'refund' => false,
            'default_svp' => 206000
        ),
        array(
            'value' => PaycheckAdditional::$TYPE_SICK_LEAVE_TILL_30,
            'display' => Yii::t('PaychecksModule.PaycheckAdditional', 'SICK_LEAVE_TILL_30'),
            'separate_field_in_xml' => true,
            'for_payout' => true,
            'percentage' => 65,
            'OZ' => true,
            'refund' => false,
            'default_svp' => 101000
        ),
        array(
            'value' => PaycheckAdditional::$TYPE_SICK_LEAVE_OVER_30,
            'display' => Yii::t('PaychecksModule.PaycheckAdditional', 'SICK_LEAVE_OVER_30'),
            'separate_field_in_xml' => true,
            'for_payout' => false,
            'percentage' => 65,
            'OZ' => false,
            'refund' => true,
            'default_svp' => 204000
        ),
        array(
            'value' => PaycheckAdditional::$TYPE_ANNUAL_LEAVE,
            'display' => Yii::t('PaychecksModule.PaycheckAdditional', 'ANNUAL_LEAVE'),
            'separate_field_in_xml' => false,
            'for_payout' => true,
            'percentage' => 100
        ),
        array(
            'value' => PaycheckAdditional::$TYPE_STATE_AND_RELIGIOUS_HOLIDAY,
            'display' => Yii::t('PaychecksModule.PaycheckAdditional', 'STATE_AND_RELIGIOUS_HOLIDAY'),
            'separate_field_in_xml' => false, 
            'for_payout' => true,
            'percentage' => 100
        ),
        array(
            'value' => PaycheckAdditional::$TYPE_WORK_ON_NON_WORKING_HOLIDAY,
            'display' => Yii::t('PaychecksModule.PaycheckAdditional', 'WORK_ON_NON_WORKING_HOLIDAY'),
            'separate_field_in_xml' => false, 
            'for_payout' => true,
            'percentage' => 100
        ),
        array(
            'value' => PaycheckAdditional::$TYPE_PAID_LEAVE,
            'display' => Yii::t('PaychecksModule.PaycheckAdditional', 'PAID_LEAVE'),
            'separate_field_in_xml' => false, 
            'for_payout' => true,
            'percentage' => 100
        ),
        array(
            'value' => PaycheckAdditional::$TYPE_STANDBY,
            'display' => Yii::t('PaychecksModule.PaycheckAdditional', 'STANDBY'),
            'separate_field_in_xml' => false, 
            'for_payout' => true,
            'percentage' => 100
        ),
//        array(
//            'value' => PaycheckAdditional::$TYPE_OVERTIME,
//            'display' => Yii::t('PaychecksModule.PaycheckAdditional', 'OVERTIME'),
//            'separate_field_in_xml' => false, 
//            'for_payout' => true,
//            'percentage' => 100
//        ),
        [
            'value' => PaycheckAdditional::$TYPE_PAID_LEAVE_MILITARY_SERVICE,
            'display' => Yii::t('PaychecksModule.PaycheckAdditional', 'PAID_LEAVE_MILITARY_SERVICE'),
            'separate_field_in_xml' => false, 
            'for_payout' => true,
            'percentage' => 100
        ],
    )
);

