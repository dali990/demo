<?php

return array(
    array(
        'name' => 'taxes_paid_confirmation_document_type_id',
        'display_name' => 'Tip dokumenta za potvrdu o placenim porezima i doprinosima po odbitku',
        'type' => 'searchField',
        'type_params' => array(
            'modelName' => 'DocumentType',
        ),
        'user_change_forbidden' => true,
        'description' => 'Tip dokumenta za potvrdu o placenim porezima i doprinosima po odbitku',
    ),
);

