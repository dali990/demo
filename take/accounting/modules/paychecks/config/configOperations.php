<?php

return array(
    'PaychecksUserRead',
    'PaychecksRead',
    'PaychecksSuspensionAccess' => [
        'description' => Yii::t('PaychecksModule.Sispension', 'PaychecksSuspensionAccessDescription')
    ],
    'PaychecksNotifyWhenSync' => [
        'description' => Yii::t('PaychecksModule.PaaycheckCodebook', 'NotifyIfPaycheckCodebookChanges')
    ]
);