<?php

return array(
    'display_name' => Yii::t('PaychecksModule.Configuration', 'Paychecks'),
    array(
        'name' => 'estimated_average_hours_in_month',
        'display_name'=>'Procenjen prosecan broj radnih sati u mesecu',
        'type'=>'textField',
        'value_not_null'=>true,
        'default' => '174',
        'description'=>'Procenjen prosecan broj radnih sati u mesecu'
    ),
    array(
        'name' => 'company_type',
        'display_name'=>'Tip Isplatioca',
        'type'=>'dropdown',
        'type_params'=>array(
            array(
                'title'=>'Pravno lice, nije iz budžeta',
                'value'=>1
            ),
            array(
                'title'=>'Pravno lice iz budžeta',
                'value'=>2
            ),
            array(
                'title'=>'Strano predstavništvo',
                'value'=>3
            ),
            array(
                'title'=>'Preduzetnik',
                'value'=>4
            ),
            array(
                'title'=>'Fizičko lice',
                'value'=>5
            ),
            array(
                'title'=>'Vojska RS',
                'value'=>6
            ),
            array(
                'title'=>'Poljoprivredno gazdinstvo',
                'value'=>7
            ),
        ),
        'value_not_null' => true,
        'default' => '1',
        'description'=>'Vrsta isplatioca prihoda'
    ),
    array(
        'name' => 'company_municipality_id',
        'display_name'=>'Šifra opštine kompanije',
        'type'=>'searchField',
        'type_params' => [
            'modelName' => Municipality::class,
        ],
        'value_not_null' => false,
        'description'=>'Šifra opštine u kojoj se kompanija nalazi potrebna za generisanje xml-a zarada'
    ),
    array(
        'name' => 'employee_paycheck_payment_code',
        'display_name'=>'Kod placanja za zarade',
        'type'=>'searchField',
        'type_params'=>array(
            'modelName'=>'PaymentCode',
        ),
        'value_not_null'=>true,
        'description'=>'Kod placanja koji se postavlja kad nije izabran ni jedan drugi pri generisanju zarada (240)'
    ),
    array(
        'name' => 'call_for_number_modul_default',
        'display_name'=>'Default model poziva na broj',
        'type'=>'textField',
        'value_not_null'=>true,
        'default' => '97',
        'description'=>'Model poziva na broj za uplatu drzavi pri generisanju zarada'
    ),
    array(
        'name' => 'state_partner',
        'display_name' => Yii::t('PaychecksModule.Configuration', 'StatePartner'),
        'type'=>'searchField',
        'type_params'=>array(
            'modelName'=>'Partner',
        ),
        'value_not_null'=>true,
        'description' => Yii::t('PaychecksModule.Configuration', 'StatePartnerDescription')
    ),
    array(
        'name' => 'probationer_work_percent',
        'display_name' => Yii::t('PaychecksModule.Configuration', 'ProbationerWorkPercent'),
        'type'=>'textField',
        'value_not_null'=>true,
        'default' => '80',
        'description' => Yii::t('PaychecksModule.Configuration', 'ProbationerWorkPercentDescription'),
    ),
    array(
        'name' => 'state_paycheck_payment_code',
        'display_name' => Yii::t('PaychecksModule.Configuration', 'StatePaycheckPaymentCode'),
        'type'=>'searchField',
        'type_params'=>array(
            'modelName'=>'PaymentCode',
        ),
        'value_not_null'=>true,
//        'default' => '254',
        'description' => Yii::t('PaychecksModule.Configuration', 'StatePaycheckPaymentCodeDescription'),
    ),
    array(
        'name' => 'taxes_paid_confirmation_template_id',
        'display_name'=>'Template za potvrdu o plaćenim porezima i doprinosima po odbitku',
        'type'=>'searchField',
        'type_params' => array(
            'modelName'=>'Template',
        ),
        'description'=>'Template za potvrdu o plaćenim porezima i doprinosima po odbitku'
    ),
    array(
        'name' => 'tufe_fix',
        'user_change_forbidden' => true,
        'display_name'=>Yii::t('PaychecksModule.Configuration', 'TufeFix'),
        'type'=>'dropdown',
        'type_params'=>array(
          array(
              'title'=>Yii::t('BaseModule.Common', 'Yes'),
              'value'=>'true'
          ),
          array(
              'title'=>Yii::t('BaseModule.Common', 'No'),
              'value'=>'false'
          )  
        ),
        'default'=>'false',
        'description' => Yii::t('PaychecksModule.Configuration', 'TufeFixDescription')
    ),
    [
        'name' => 'work_on_state_holiday_percentage',
        'display_name' => Yii::t('PaychecksModule.PaycheckAdditional', 'WorkOnStateHolidayPercentage'),
        'type'=>'textField',
        'value_not_null'=>true,
        'default' => '120',
        'description' => Yii::t('PaychecksModule.PaycheckAdditional', 'WorkOnStateHolidayPercentageDescription')
    ],
    [
        'name' => 'calculate_with_work_on_state_holiday',
        'user_change_forbidden' => true,
        'display_name'=>Yii::t('PaychecksModule.Configuration', 'CalculateWithWorkOnStateHoliday'),
        'type'=>'dropdown',
        'type_params' => [
          [
              'title'=>Yii::t('BaseModule.Common', 'Yes'),
              'value'=>'true'
          ],
          [
              'title'=>Yii::t('BaseModule.Common', 'No'),
              'value'=>'false'
          ]
        ],
        'default' => 'true',
        'description' => Yii::t('PaychecksModule.Configuration', 'CalculateWithWorkOnStateHolidayDescription')
    ],
    [
        'name' => 'overtime_percentage',
        'display_name' => Yii::t('PaychecksModule.Paycheck', 'OvertimePercentage'),
        'type'=>'textField',
        'value_not_null'=>true,
        'default' => '120',
        'description' => Yii::t('PaychecksModule.Configuration', 'OvertimePercentageDescription')
    ],
    [
        'name' => 'non_taxable_part',
        'display_name' => Yii::t('PaychecksModule.Configuration', 'NonTaxablePart'),
        'type' => 'textField',
        'value_not_null' => true,
        'default' => '0',
        'description' => Yii::t('PaychecksModule.Configuration', 'NonTaxablePartDesc')
    ],
    [
        'name' => 'payout_value_per_day',
        'display_name' => Yii::t('PaychecksModule.Configuration', 'PayoutValuePerDay'),
        'type' => 'textField',
        'value_not_null' => true,
        'default' => '0',
        'description' => Yii::t('PaychecksModule.Configuration', 'PayoutValuePerDay')
    ],
    [
        'name' => 'bruto_hour_calc_type',
        'display_name' => Yii::t('PaychecksModule.Configuration', 'BrutoHourCalcType'),
        'description' => Yii::t('PaychecksModule.Configuration', 'BrutoHourCalcTypeDescription'),
        'type' => 'dropdown',
        'type_params' => [
            [
                'title' => Yii::t('PaychecksModule.Configuration', 'BrutoDivideByPoints'),
                'value' => 1
            ],
            [
                'title' => Yii::t('PaychecksModule.Configuration', 'BrutoDivideByPercent'),
                'value' => 2
            ],
            [
                'title' => Yii::t('PaychecksModule.Configuration', 'BrutoByPointsForHourValue'),
                'value' => 3
            ],
            [
                'title' => Yii::t('PaychecksModule.Configuration', 'BrutoByHourValue'),
                'value' => 4
            ],
        ],
        'default' => 1,
        'value_not_null' => true,
        'user_changable' => false,
    ],
//    [
//        'name' => 'use_new_coodebooks',
//        'display_name' => Yii::t('PaychecksModule.Configuration', 'UseNewCoodebooks'),
//        'type' => 'boolean',
//        'value_not_null' => true,
//        'default' => false,
//        'description' => Yii::t('PaychecksModule.Configuration', 'UseNewCoodebooksDesc')
//    ],
    [
        'name' => 'use_max_contribution_base',
        'display_name' => Yii::t('PaychecksModule.Configuration', 'UseMaxContributionBase'),
        'type' => 'boolean',
        'value_not_null' => true,
        'default' => false,
        'description' => Yii::t('PaychecksModule.Configuration', 'UseMaxContributionBaseDesc')
    ],
    [
        'name' => 'to_validate_employees_confirmed_days',
        'display_name' => Yii::t('PaychecksModule.Configuration', 'ToValidateEmployeesConfirmedDays'),
        'description' => Yii::t('PaychecksModule.Configuration', 'ToValidateEmployeesConfirmedDaysDesc'),
        'type' => 'boolean',
        'value_not_null' => true,
        'default' => true,
        'user_changable' => false
    ],
    [
        'name' => 'paycheck_codebook_sync_type',
        'user_changable' => false,
        'display_name' => Yii::t('PaychecksModule.PaycheckCodebook', 'SyncTypeForPaychecksCodebook'),
        'type' => 'dropdown',
        'type_params' => [
            [
                'title' => Yii::t('PaychecksModule.PaycheckCodebook', 'ManualSync'),
                'value' => 'manual_sync'
            ],
            [
                'title' => Yii::t('PaychecksModule.PaycheckCodebook', 'ManualSyncWithNotification'),
                'value' => 'manual_sync_with_notification'
            ],
            [
                'title' => Yii::t('PaychecksModule.PaycheckCodebook', 'AutomaticSynchronizationWithNotification'),                
                'value' => 'automatic_synchronization_with_notification'
            ],
            [
                'title' => Yii::t('PaychecksModule.PaycheckCodebook', 'AutomaticSynchronizationWithoutNotification'),
                'value' => 'automatic_synchronization_without_notification'
            ],
        ],
        'value_not_null' => true,
        'default' => 'manual_sync_with_notification',
        'description' => Yii::t('PaychecksModule.PaycheckCodebook', 'SyncTypeForPaychecksCodebook'),
    ],
);
