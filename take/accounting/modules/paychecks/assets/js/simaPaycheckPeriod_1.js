/* global sima */

function SIMAPaycheckPeriod()
{
    this.paycheckPeriodFormOnTypeChange = function(form, dependent_on_field, dependent_field, apply_actions_func)
    {
        var dependent_on_field_val = dependent_on_field.val();
        
        if(dependent_on_field_val === 'FINAL')
        {
            apply_actions_func('disable');
            dependent_field.val('100');
        }
        else
        {
            apply_actions_func('enable');
        }
    };
}