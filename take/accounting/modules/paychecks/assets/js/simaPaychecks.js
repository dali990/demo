/* global sima */

function SIMAPaychecks()
{
    /// MilosJ: nije nadjeno da se igde koristi
//    this.insertAccountingMonths = function(_this, uniq)
//    {
//        if (!_this.hasClass('_disabled')) 
//        {
//            createAccountingMonths(_this, uniq);
//        }
//    };
    
    /// MilosJ: nije nadjeno da se igde koristi
//    function createAccountingMonths(calling_object, uniq, init_data)
//    {
//        var params = {
//            formName: 'createOldPeriods',
//            scenario: 'createOldPeriods',
//            status: 'validate',
//            onSave: function(response){
//                var attributes = response.form_data.AccountingMonth.attributes;
//                sima.ajax.get('accounting/paychecks/paychecksEmployee/createFinancialMonths', {
//                    data: 
//                    {
//                        attributes: attributes
//                    },
//                    async:true,
//                    loadingCircle: $('#old-paychecks'+uniq),
//                    success_function:function(response)
//                    {
//                        if(response.message === "success")
//                        {
//                            sima.dialog.openInfo("Uspesno kreirani finansijski meseci");
//                            sima.misc.refreshUiTabForObject(calling_object);
//                        }
//                        else
//                        {
//                            createAccountingMonths(calling_object, uniq, attributes);
//                            sima.dialog.openWarn("Doslo je do greske:</br>"+response.message);
//                        }
//                    }
//               });
//            }
//        };
//        
//        if(typeof init_data !== "undefined")
//        {
//            params['init_data'] = {
//                AccountingMonth: init_data
//            };
//        }
//        
//        sima.model.form('AccountingMonth', '', params);
//    }
    
    this.recalcPeriod = function(refresh_obj_id, model_id, with_additionals, for_compare_without_save, for_compare_without_save_detailed)
    {
        sima.ajax.get('accounting/paychecks/paycheckPeriod/preRecalcPeriodValidation',{
            data: {
                paycheckPeriodId: model_id
            },
            async: true,
            loadingCircle: $('#body_sync_ajax_overlay'),
//            loadingCircle: $('body'),
            success_function: function(response){
                if(response.status === 'OK')
                {
                    recalcPeriod(refresh_obj_id, model_id, with_additionals, for_compare_without_save, for_compare_without_save_detailed);
                }
                else
                {
                    sima.dialog.openYesNo(response.status, 
                        function(){
                            sima.dialog.close();
                            recalcPeriod(refresh_obj_id, model_id, with_additionals, for_compare_without_save, for_compare_without_save_detailed);
                        },
                        function(){
                            sima.dialog.close();
                        }
                    );
                }
            }
        });
    };
    
    function recalcPeriod(refresh_obj_id, model_id, with_additionals, for_compare_without_save, for_compare_without_save_detailed)
    {
        sima.ajaxLong.start("accounting/paychecks/paycheckPeriod/recalcPeriod", {
            data: {
                paycheckPeriodId: model_id,
                withAdditionals: with_additionals,
                forCompareWithoutSave: for_compare_without_save,
                forCompareWithoutSave_detailed: for_compare_without_save_detailed
            },
            showProgressBar: $('body'),
            onEnd: function(data){
                if(for_compare_without_save === true)
                {
                    sima.dialog.openInFullScreen(data.compare_html);
                }
                else
                {
                    sima.misc.refreshUiTabForObject($('#'+refresh_obj_id));
                }
            },
            failure_function: function(response){
                sima.dialog.openWarn(response);
                sima.misc.refreshUiTabForObject($('#'+refresh_obj_id));
            }
        });
    }
    
    this.recalcPaycheck = function(paycheck_id, model_id, with_additionals, for_compare_without_save, for_compare_without_save_detailed)
    {
        sima.ajax.get('accounting/paychecks/paycheckPeriod/recalcPaycheck',{
            get_params:{
                paycheckId: model_id,
                withAdditionals: with_additionals,
                forCompareWithoutSave: for_compare_without_save,
                forCompareWithoutSave_detailed: for_compare_without_save_detailed
            },
            async: true,
            loadingCircle: $('body'),
            success_function:function(data)
            {
                if(for_compare_without_save === true)
                {
                    sima.dialog.openInFullScreen(data.compare_html);
                }
                else
                {
                    sima.misc.refreshUiTabForObject($('#'+paycheck_id));
                    sima.misc.refreshMainTabForObject($('#'+paycheck_id));
                }
            }
        });
    };
    
    this.recalcPaycheckForEmployee = function(statistics_div_id, employee_id, paycheck_period_id)
    {
        sima.ajax.get('accounting/paychecks/paycheckPeriod/recalcPaycheckForEmployee',{
            get_params:{
                employee_id: employee_id,
                paycheck_period_id: paycheck_period_id
            },
            async: true,
            loadingCircle: $('body'),
            success_function:function()
            {
                sima.misc.refreshUiTabForObject($('#'+statistics_div_id));
            }
        });
    };
    
    this.importPaychecksXML = function(button_wrapper_id, model_id)
    {
        var params = {
            status: 'validate',
            onSave: function(response){
                sima.ajax.get('accounting/paychecks/paycheckPeriod/parseUploadedPaycheckXML', {
                    data: {
                        paycheckPeriodId: model_id,
                        upload_session_key: response.form_data.File.attributes.file_version_id
                    },
                    async:true,
                    loadingCircle: $('body'),
                    success_function:function(response)
                    {
                        sima.misc.refreshUiTabForObject($('#'+button_wrapper_id));
                    }
               });
            },
            formName: 'justupload'
        };
        sima.model.form('File', '', params);
    };
    
    this.createPayments = function(button_wrapper_id, model_id)
    {
        sima.ajax.get('accounting/paychecks/paycheckPeriod/createPayments', {
            data: {
                paycheckPeriodId: model_id
            },
            async:true,
            loadingCircle: $('body'),
            success_function:function()
            {
                sima.misc.refreshUiTabForObject($('#'+button_wrapper_id));
            }
       });
    };
    
    this.confirmPeriod = function(_this, uniq_id, model_id)
    {
        if (!_this.hasClass('_disabled')) 
        {
            sima.ajax.get('accounting/paychecks/paycheckPeriod/confirmPeriod',{
                data:
                {
                    paycheckPeriodId: model_id
                },
                async: true,
                loadingCircle: $('#paychecks'+uniq_id),
                success_function:function()
                {
                    sima.misc.refreshUiTabForObject(_this);
//                    $('#paychecks'+uniq_id).simaGuiTable('populate');
                }
            });
        }
    };
    
    this.insertPaycheckPeriods = function(_this, uniq)
    {
        if (!_this.hasClass('_disabled')) 
        {
            sima.ajax.get('accounting/paychecks/paychecksEmployee/createPaycheckPeriods', {
                async:true,
                loadingCircle: $('#old-paychecks'+uniq),
                success_function:function(response)
                {
                    sima.dialog.openInfo("Uspesno kreirane isplate zarada");
                    sima.misc.refreshUiTabForObject(_this);
                }
            });
        }
    };
    
    this.createMissingPaychecks = function(_this, uniq)
    {
        if (!_this.hasClass('_disabled')) 
        {
            sima.ajax.get('accounting/paychecks/paychecksEmployee/createMissingPaychecks', {
                async:true,
                loadingCircle: $('#old-paychecks'+uniq),
                success_function:function(response)
                {
                    sima.dialog.openInfo("Uspesno kreirana placanja");
                    $('#employees'+uniq).simaGuiTable('populate');
                }
            });
        }
    };
    
    this.populateMissingPaycheckData = function(_this, uniq)
    {
        if (!_this.hasClass('_disabled')) 
        {
            var type = null;
            if (_this.hasClass('work-hours'))
            {
                type = 'worked_hours';
            }
            else if (_this.hasClass('work-hours-values'))
            {
                type = 'worked_hours_value';
            }
            else if (_this.hasClass('past-experience'))
            {
                type = 'past_exp_value';
            }
                        
            var uniq_id = sima.uniqid();
            var dateFromName = 'from'+uniq_id;
            var dateToName = 'to'+uniq_id;

            populateMissingPaycheckDataDialog(_this, uniq, uniq_id, dateFromName, dateToName, null, type);
        }
    };
    
    this.createMissingPaycheck = function(guitable_id)
    {
        var params = {
            status: 'validate',
            onSave: function(response){                
                var paycheck_period_id = response.form_data.Paycheck.attributes.paycheck_period.ids;
                var employee_id = response.form_data.Paycheck.attributes.employee.ids;
                var worked_hours = response.form_data.Paycheck.attributes.worked_hours;
                var worked_hours_value = response.form_data.Paycheck.attributes.worked_hours_value;
                var past_exp_value = response.form_data.Paycheck.attributes.past_exp_value;

                sima.ajax.get('accounting/paychecks/paychecksEmployee/createOldPaycheck', {
                    data:{
                        paycheck_period_id: paycheck_period_id,
                        employee_id: employee_id,
                        worked_hours: worked_hours,
                        worked_hours_value: worked_hours_value,
                        past_exp_value: past_exp_value
                    },
                    async:true,
                    loadingCircle: $('body'),
                    success_function:function(response)
                    {
                        $('#'+guitable_id).simaGuiTable('populate');
                    }
                });
            },
            formName: 'old_paycheck'
        };
        
        sima.model.form('Paycheck', '', params);
    };
    
    function populateMissingPaycheckDataDialog(calling_object, tab_uniq, uniq_id, dateFromName, dateToName, init_data, type)
    {
        var initData = {};
        if(typeof init_data !== "undefined")
        {
            initData = init_data;
        }
                        
        sima.ajax.get('accounting/paychecks/paychecksEmployee/generateDialogForMissingPaycheckData', {
            data:{
                uniq_id: uniq_id,
                dateFromName: dateFromName,
                dateToName: dateToName,
                type: type,
                initData: initData
            },
            async:true,
            loadingCircle: $('#old-paychecks'+tab_uniq),
            success_function:function(response)
            {
                sima.dialog.openYesNo(response.html, function(){
                    var selected_employees = extractSelectedEmployeesFromChooseList($('#employee_choose_result'+uniq_id));
                    var data_value = $('#data_input'+uniq_id).val();
                    
                    var dateFrom_value = {
                        id: $('#'+dateFromName).find('.sima-ui-sf-input').val(),
                        display: $('#'+dateFromName).find('.sima-ui-sf-display').val()
                    };
                    var dateTo_value = {
                        id: $('#'+dateToName).find('.sima-ui-sf-input').val(),
                        display: $('#'+dateToName).find('.sima-ui-sf-display').val()
                    };
                                        
                    sima.dialog.close();
                    
                    var post_data = {
                        selected_employees: selected_employees,
                        data_column: type,
                        data_value: data_value,
                        dateFrom_value: dateFrom_value,
                        dateTo_value: dateTo_value
                    };

                    sima.ajax.get('accounting/paychecks/paychecksEmployee/populateMissingPaycheckData', {
                        data: post_data,
                        async:true,
                        loadingCircle: $('#old-paychecks'+tab_uniq),
                        success_function:function(response)
                        {
                            if(response.message === 'success')
                            {
                                $('#employees'+tab_uniq).simaGuiTable('populate');
                            }
                            else
                            {
                                populateMissingPaycheckDataDialog(calling_object, tab_uniq, uniq_id, dateFromName, dateToName, post_data, type);
                            }
                        }
                    });
                });
            }
        });
    };
    
    this.onMissingWorkHoursChooseEmployees = function(uniq_id)
    {
        var employee_choose_list = $('#employee_choose_result'+uniq_id);
        
        var init_ids = extractSelectedEmployeesFromChooseList(employee_choose_list);
            
        var params = {
            view: "guiTable",
            multiselect: true,
            ids: init_ids
        };
        
        sima.model.choose('Employee', function(data){
            employee_choose_list.html('');
            var data_length = data.length;
            for ( var i = 0; i < data_length; i++ ) 
            {
                var emp_div = $('<div class="emp_div" style="width:100%"></div>');
                emp_div.attr('emp_id', data[i].id);
                emp_div.attr('display_name', data[i].display_name);
                emp_div.html(data[i].display_name);
                employee_choose_list.append(emp_div);
            }
            
        }, params);
    };
    
    /// MilosJ 20191003 - nije nadjeno da se igde koristi
//    this.unifiedPaychecksDialog = function(unifiedPaychecksTableId)
//    {
//        var guitable_params = {
//            id: 'unifiedPaychecks' + sima.uniqid(),
//            model: 'UnifiedPaycheck',
//            add_button: true
//        };
//        var dialog_params = {
//            title: 'Objedinjena plaćanja',
//            close_func: function(){
//                $(unifiedPaychecksTableId).simaGuiTable('populate');
//            }
//        };
//
//        sima.model.openGuiTable(guitable_params, dialog_params);
//    };
    
    function extractSelectedEmployeesFromChooseList(employee_choose_list)
    {
        var init_ids = [];
        
        var existing_divs = employee_choose_list.find('.emp_div');
        var existing_divs_len = existing_divs.length;
        for(var i=0; i<existing_divs_len; i++)
        {
            var element = {
                id: $(existing_divs[i]).attr('emp_id'),
                display_name: $(existing_divs[i]).attr('display_name')
            };
            init_ids.push(element);
        }
        
        return init_ids;
    }
    
    this.recalculatePeriodFromCompareView = function(div_for_problems, hidden_div_id)
    {        
        div_for_problems.html("");
        
        var paycheck_period_id = hidden_div_id.data('paycheck_period_id');
        
        if(paycheck_period_id !== null && typeof paycheck_period_id !== 'undefined')
        {            
            sima.paychecks.recalcPeriod($(this), paycheck_period_id);
        }
    };
    
    this.createOZFiles = function(paycheck_period_id)
    {
        sima.ajaxLong.start('accounting/paychecks/paycheckPeriod/createOZFiles', {
            showProgressBar:$('body'),
            data:{
                paycheckPeriodId : paycheck_period_id
            },
            onEnd: function(response) {
                console.log(response.message);
            }
        });
    };
    
    this.recreateOZFile = function(button_id, paycheck_id)
    {        
        var button = $('#'+button_id);
        sima.ajax.get('accounting/paychecks/paycheck/recreateOZFile', {
            get_params:{
                paycheckId: paycheck_id
            },
            async: true,
            loadingCircle: $('body'),
            success_function:function(response)
            {
                if(!sima.misc.isInDialog(button))
                {
                    sima.misc.refreshMainTabForObject(button);
                }
                else
                {
                    sima.misc.getDialogParent(button).find('.sima-ui-tabs').simaTabs('select_tab', 'pdfPreview', true);
                }
            }
        });
    };
    
    this.generateTaxesPaidConfirmationDocument = function(taxes_paid_confirmation_id)
    {
        sima.ajax.get('accounting/paychecks/paycheck/generateTaxesPaidConfirmationDocument', {
            get_params:{
                taxes_paid_confirmation_id : taxes_paid_confirmation_id               
            },
            success_function: function(response)
            {
                sima.dialog.openInfo('Generisano!');
            }
        });
    };
    
    this.generateDocumentsForAllTaxesPaidConfirmationsForYear = function(table_id)
    {
        var value = $('#'+table_id).find('.sima-gui-table-filter[column="year"] .sima-ui-sf:first').simaSearchField('getValue');
        var year_id = value.id;
        if (typeof year_id === 'undefined' || year_id === '' || year_id === null)
        {
            sima.dialog.openWarn('Morate prvo izabrati godinu.');
        }
        else
        {
            sima.dialog.openYesNo("Da li ste sigurni da želite da kreirate dokumente za "+value.display_name+" godinu?", function(){
                sima.dialog.close();
                sima.ajaxLong.start('accounting/paychecks/paycheck/generateDocumentsForAllTaxesPaidConfirmationsForYear', {
                    showProgressBar:$('#'+table_id),
                    data:{
                        year_id : year_id
                    },
                    onEnd: function(response) {                                        
                        if (response.has_created_documents === false)
                        {
                            sima.dialog.openWarn('Dokumenti su već kreirani.');
                        }
                        else
                        {                            
                            $('#'+table_id).simaGuiTable('populate');
                        }
                    }
                });
            });
        }
    };
    
    this.filingAllTaxesPaidConfirmationsForYear = function(table_id)
    {
        var value = $('#'+table_id).find('.sima-gui-table-filter[column="year"] .sima-ui-sf:first').simaSearchField('getValue');
        var year_id = value.id;
        if (typeof year_id === 'undefined' || year_id === '' || year_id === null)
        {
            sima.dialog.openWarn('Morate prvo izabrati godinu.');
        }
        else
        {
            var _question = "Da li ste sigurni da želite da regenerišete dokumente za "+value.display_name+" godinu?";
            _question += '<br/> NAPOMENA: <ul>';
            _question += '<li>Fajl na izabranom zavodnom broju će biti izbrisan.</li>';
            _question += '<li>Morate vi biti odgovorno lice</li>';
            _question += '<li>Potvrde sa zavodnog broja moraju biti skinute</li>';
            _question += '</ul><br /><br />';
            sima.dialog.openYesNo(_question, function(){
                sima.dialog.close();        
                sima.model.choose('Filing',function(pick){
                    sima.ajaxLong.start('accounting/paychecks/paycheck/filingAllTaxesPaidConfirmationsForYear', {
                        showProgressBar:$('#'+table_id),
                        data:{
                            year_id : year_id,
                            new_filing_id : pick.id,
                        },
                        onEnd: function(response) {                                        
                            if (response.has_changes === false)
                            {
                                sima.dialog.openWarn('Svi dokumenti su već zavedeni.');
                            }
                            else
                            {                            
                                $('#'+table_id).simaGuiTable('populate');
                            }
                        }
                    });
                });
            });
        }
    };
    
    this.reGenerateDocumentsPdfForAllTaxesPaidConfirmationsForYear = function(table_id)
    {
        var value = $('#'+table_id).find('.sima-gui-table-filter[column="year"] .sima-ui-sf:first').simaSearchField('getValue');
        var year_id = value.id;
        if (typeof year_id === 'undefined' || year_id === '' || year_id === null)
        {
            sima.dialog.openWarn('Morate prvo izabrati godinu.');
        }
        else
        {
            sima.dialog.openYesNo("Da li ste sigurni da želite da regenerišete dokumente za "+value.display_name+" godinu?", function(){
                sima.dialog.close();
                sima.ajaxLong.start('accounting/paychecks/paycheck/reGenerateDocumentsPdfForAllTaxesPaidConfirmationsForYear', {
                    showProgressBar:$('#'+table_id),
                    data:{
                        year_id : year_id
                    },
                    onEnd: function(response) {                        
                        $('#'+table_id).simaGuiTable('populate');
                    }
                });
            });
        }
    };
    
    this.sendDocumentsPdfForAllTaxesPaidConfirmationsForYear = function(table_id, only_download)
    {
        var value = $('#'+table_id).find('.sima-gui-table-filter[column="year"] .sima-ui-sf:first').simaSearchField('getValue');
        var year_id = value.id;
        if (typeof year_id === 'undefined' || year_id === '' || year_id === null)
        {
            sima.dialog.openWarn('Morate prvo izabrati godinu.');
        }
        else
        {
            sima.dialog.openYesNo("Da li ste sigurni da želite da pošaljete/preuzmete sva dokumenta za "+value.display_name+" godinu?", function(){
                sima.dialog.close();
                sima.ajaxLong.start('accounting/paychecks/paycheck/sendDocumentsPdfForAllTaxesPaidConfirmationsForYear', {
                    showProgressBar:$('#'+table_id),
                    data:{
                        year_id : year_id,
                        only_download: only_download
                    },
                    onEnd: function(response) {
                        if (response.tn !== null)
                        {
                            $('#'+table_id).simaGuiTable('populate');
                            sima.temp.download(response.tn, response.dn);
                        }
                    }
                });
            });
        }
    };
    
    this.afterConfirmPeriod = function(paycheck_period_id)
    {
        sima.paychecksPPPPD.generatePPPPD(paycheck_period_id);
    };
    
    this.overwriteSickLeavesFromXML = function(paycheck_period_id)
    {
        var params = {
            status: 'validate',
            onSave: function(response){
                sima.ajax.get('accounting/paychecks/paycheckPeriod/overwriteSickLeavesFromXML', {
                    data: {
                        paycheckPeriodId: paycheck_period_id,
                        upload_session_key: response.form_data.File.attributes.file_version_id
                    },
                    async:true,
                    loadingCircle: $('body'),
                    success_function:function(response)
                    {
                        sima.dialog.openInfo('Učitano');
                    }
               });
            },
            formName: 'justupload'
        };
        sima.model.form('File', '', params);
    };
    
    this.sendTaxesPaidConfirmationToUser = function(taxes_paid_confirmation_id)
    {
        sima.ajax.get('accounting/paychecks/paycheck/sendTaxesPaidConfirmationToUser', {
            get_params:{
                taxes_paid_confirmation_id : taxes_paid_confirmation_id               
            },
            success_function: function(response)
            {
                if (response.tn !== null)
                {
                    sima.temp.download(response.tn, response.dn);
                }
            }
        });
    };
    
    this.exportPaycheckForBank = function(paycheck_period_id, bank_id, type)
    {
        sima.ajax.get('accounting/paychecks/paycheckPeriod/exportPaycheckForBank', {
            get_params:{
                paycheck_period_id : paycheck_period_id,
                bank_id: bank_id,
                type: type
            },
            success_function: function(response)
            {
                sima.temp.download(response.tn, response.dn);
            }
        });
    };
    
    this.exportAllPaychecks = function(paycheck_period_id)
    {
        sima.ajaxLong.start('accounting/paychecks/paycheckPeriod/exportAllPaychecks', {
            showProgressBar: $('body'),
            data:{
                paycheck_period_id : paycheck_period_id
            },
            onEnd: function(response) {
                sima.temp.download(response.tn, response.dn);
            }
        });
    };
    
    this.sendOZFiles = function(paycheck_period_id, only_download)
    {
        sima.ajaxLong.start('accounting/paychecks/paycheckPeriod/sendOZFiles', {
            showProgressBar: $('body'),
            data:{
                paycheck_period_id : paycheck_period_id,               
                only_download : only_download
            },
            onEnd: function(response) {
                sima.dialog.openInfo(response.message);
                if(response.tn !== null)
                {
                    sima.temp.download(response.tn, response.dn);
                }
            }
        });
    };
    
    this.sendOZFileForPaycheck = function(paycheck_id)
    {
        sima.ajax.get('accounting/paychecks/paycheck/sendOZFileForPaycheck', {
            get_params:{
                paycheck_id : paycheck_id               
            },
            success_function: function(response)
            {
                if (response.tn !== null)
                {
                    sima.temp.download(response.tn, response.dn);
                }
                else
                {
                    sima.dialog.openInfo(sima.translate('Success'));
                }
            }
        });
    };
    
    this.addAllSuspensionsForActiveEmployees = function(suspension_group_id)
    {
        sima.ajax.get('accounting/paychecks/suspension/addAllSuspensionsForActiveEmployees', {
            get_params:{
                suspension_group_id : suspension_group_id
            },
            success_function: function(response)
            {
                sima.dialog.openInfo(response.message);
            }
        });
    };
    
    this.m4EmployeesPrediction = function(year_id, panel_id)
    {
        var _panel = $(panel_id);
        sima.ajaxLong.start('accounting/paychecks/paycheckPeriod/calcM4ForEmployeesForYear', {
            showProgressBar: _panel,
            data: {year_id: year_id},
            onEnd: function(response) {
                sima.diffViewer.renderFromHtml(response.html, {
                    after_save_func: function () {
                        _panel.simaGuiTable('populate');
                    }
                });
            }
        });
    };
    
    this.exportM4 = function(year_id, document_type, panel_id)
    {
        var _panel = $(panel_id);
        sima.ajaxLong.start('accounting/paychecks/paycheckPeriod/exportM4', {
            showProgressBar: _panel,
            data:{
                document_type: document_type,
                year_id: year_id
            },
            onEnd: function(response) {                                        
                sima.temp.download(response.filename, response.downloadname);
            }
        });
    };
    
    this.calcGroupBonus = function(group_bonus_id)
    {
        sima.ajaxLong.start('accounting/paychecks/paycheckPeriod/calcGroupBonus', {
            showProgressBar: $('body'),
            data:{
                group_bonus_id: group_bonus_id
            },
            onEnd: function() {     
//               _panel.simaGuiTable('populate');
            }
        });
    };
    
    this.fillGroupBonus = function(group_bonus_id)
    {
        sima.ajaxLong.start('accounting/paychecks/paycheckPeriod/fillGroupBonus', {
            showProgressBar: $('body'),
            data:{
                group_bonus_id: group_bonus_id
            },
            onEnd: function() {     
//               _panel.simaGuiTable('populate');
            }
        });
    };
    this.fillAllGroupBonus = function(group_bonus_id)
    {
        sima.ajaxLong.start('accounting/paychecks/paycheckPeriod/fillAllGroupBonus', {
            showProgressBar: $('body'),
            data:{
                group_bonus_id: group_bonus_id
            },
            onEnd: function() {     
//               _panel.simaGuiTable('populate');
            }
        });
    };
    
    this.fillCustomGroupBonus = function(group_bonus_id)
    {
        var params = {};
        params.view = "guiTable";
        params.multiselect = true;
        params.params = {add_button:false};
        sima.model.choose('Employee', function(data){
            if (data.length > 0)
            {
                var employee_ids = [];
                var data_length = data.length;
                for ( var i = 0; i < data_length; i++ ) 
                {
                    employee_ids.push(data[i].id);
                }
                sima.ajaxLong.start('accounting/paychecks/paycheckPeriod/fillCustomGroupBonus', {
                    showProgressBar: $('body'),
                    data:{
                        group_bonus_id: group_bonus_id,
                        employee_ids: employee_ids
                    },
                    onEnd: function() {     
        //               _panel.simaGuiTable('populate');
                    }
                });
            }
        }, params);
        
    };
    
    this.syncPaychecksFromCodebook = function(guitable_id)
    {
        var progress_bar_obj = sima.getLastActiveFunctionalElement();
        if (!sima.isEmpty(guitable_id))
        {
            progress_bar_obj = $('#'+guitable_id);
        }
        else if (sima.isEmpty(progress_bar_obj))
        {
            progress_bar_obj = $('body');
        }

        sima.ajaxLong.start('accounting/paychecks/paycheck/getPaychecksDiffBetweenLocalAndCodebook', {
            showProgressBar: progress_bar_obj,
            onEnd: function(response) {
                if (sima.isEmpty(response.html))
                {
                    sima.dialog.openInfo(sima.translate('NoDifferences'));
                }
                else
                {
                    sima.diffViewer.renderFromHtml(response.html, {
                        after_save_func: function () {
                            if (!sima.isEmpty(guitable_id))
                            {
                                $('#'+guitable_id).simaGuiTable('populate');
                            }
                        },
                        validate_after_save: true
                    });
                }
            }
        });
    };
}