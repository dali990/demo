/* global sima */

function SIMAPaychecksPPPPD()
{
    this.generatePPPPD = function(paycheck_period_id, onsuccess_func, regenerate_completely)
    {
        if(regenerate_completely === true)
        {
            sima.dialog.openYesNo('Da li ste sigurni da zelite kompletno regenerisete PPPPD?',function(){
                sima.dialog.close();
                generatePPPPD_ajax(paycheck_period_id, onsuccess_func, regenerate_completely);
            });
        }
        else
        {
            generatePPPPD_ajax(paycheck_period_id, onsuccess_func, regenerate_completely);
        }
    };
    
    function generatePPPPD_ajax(paycheck_period_id, onsuccess_func, regenerate_completely)
    {
        sima.ajaxLong.start('accounting/paychecks/paycheckPeriod/regeneratePPPPD', {
            showProgressBar:$('body'),
            data:{
                paycheckPeriodId: paycheck_period_id,
                regenerate_completely: regenerate_completely
            },
            onEnd: function(response) {
                if(sima.isEmpty(onsuccess_func))
                {
                    return;
                }
                    
                var success_func = onsuccess_func;
                if(typeof onsuccess_func === 'string')
                {
                    success_func = function(){
                        sima.refreshActiveTab($(onsuccess_func));
                    };
                }
                
                sima.callFunction(success_func);
            }
        });
    }
    
    this.choosePPPDXmlForCompare = function(div_for_problems_id, hidden_div_id)
    {
        sima.model.form('File', '', {
            status: 'validate',
            onSave: function(response){
                var parsed_json = JSON.parse(response.form_data.File.attributes.file_version_id);
                $('#'+hidden_div_id).data('upload_key', response.form_data.File.attributes.file_version_id);
                $('#'+hidden_div_id).data('upload_file_display_name', parsed_json.file_display_name);
                updateCompareProblemsDiv(div_for_problems_id, hidden_div_id);
            },
            formName: 'justupload'
        });
    };
    this.choosePaycheckPeriodForCompare = function(div_for_problems_id, hidden_div_id)
    {
        sima.model.choose('PaycheckPeriod', function(data){
            $('#'+hidden_div_id).data('paycheck_period_id', data.id);
            $('#'+hidden_div_id).data('paycheck_period_display_name', data.display_name);
            updateCompareProblemsDiv(div_for_problems_id, hidden_div_id);
        });
    };
    
    this.compareXMLContent = function(action, div_for_problems_id, hidden_div_id)
    {
        var div_for_problems = $('#'+div_for_problems_id);
        var hidden_div = $('#'+hidden_div_id);
                
        var paycheck_period_id = hidden_div.data('paycheck_period_id');
        var upload_key = hidden_div.data('upload_key');
        
        var error_message = '';
        
        if(sima.isEmpty(upload_key))
        {
            error_message += 'Nije izabran xml</br>';
        }
        if(sima.isEmpty(paycheck_period_id))
        {
            error_message += 'Nije izabrana isplata zarade</br>';
        }
        
        if(!sima.isEmpty(error_message))
        {
            div_for_problems.html(error_message);
            sima.dialog.openWarn(error_message);
        }
        else
        {
            updateCompareProblemsDiv(div_for_problems_id, hidden_div_id);
            
            sima.ajax.get('accounting/paychecks/PPPPDPaychecks/'+action, {
                data:{
                    upload_key: upload_key,
                    paycheck_period_id: paycheck_period_id
                },
                async: true,
                loadingCircle: $('body'),
                success_function:function(response)
                {
                    var error_messages = response.error_messages;
                    if(error_messages.length === 0)
                    {
                        div_for_problems.append('</br>All ok');
                    }
                    else
                    {
                        div_for_problems.append('</br>'+error_messages);
                    }
                },
                failure_function: function(response){
                    div_for_problems.append("</br>There was error:<br>"+JSON.stringify(response));
                }
            });
        }
    };
    
    this.bindExistingPPPPD = function(paycheck_period_id, ppppd_div)
    {
        sima.model.choose('PPPPD',function(data){
            var ppppd_ids = [];
            $.each(data, function(index, value){
                ppppd_ids.push(value.id);
            });
            sima.ajax.get('accounting/paychecks/PPPPDPaychecks/bindPPPPDsToPaycheckPeriod', {
                data:{
                    paycheck_period_id: paycheck_period_id,
                    ppppd_ids: ppppd_ids
                },
                async: true,
                loadingCircle: $('body'),
                success_function: function()
                {
                    sima.refreshActiveTab($(ppppd_div));
                }
            });
        },{
            title: sima.translate('ChoosePPPPDForBinding'),
            view: 'guiTable',
            fixed_filter: {
                filter_scopes: [
                    'notInPaycheckPeriodsToPpppdsScope'
                ]
            },
            multiselect: true
        });
    };
    
    this.onPPPDTabXMLSelectionChange = function(ppppd_xmls_guitable_id, row)
    {
        const ppppd_xmls_guitable = $('#'+ppppd_xmls_guitable_id);

        if(sima.isEmpty(row))
        {
            ppppd_xmls_guitable.simaGuiTable('setFixedFilter',{
                id: -1
            }).simaGuiTable('populate');
        }
        else
        {
            const model_id = row.attr('model_id');

            ppppd_xmls_guitable.simaGuiTable('setFixedFilter',{
                ppppd: {
                    ids: model_id
                }
            }).simaGuiTable('populate');
        }
    };

    this.moveXMLsToOtherPPPD = function(paycheck_period_id, ppppd_xmls_guitable_id)
    {
        const ppppd_xmls_guitable = $('#'+ppppd_xmls_guitable_id);
        
        const selected_ids = ppppd_xmls_guitable.simaGuiTable('getSelectedIds');
        const fixed_filter = ppppd_xmls_guitable.simaGuiTable('getFixedFilter');
        
        if(sima.isEmpty(selected_ids))
        {
            sima.dialog.openWarn('niste izabrali nijedan element');
            return;
        }
        
        sima.modelChoose.create({
            model: 'PPPPD',
            ok_function: function(data){
                const ppppd_id = data[0].id;
                sima.ajax.get('accounting/paychecks/PPPPDPaychecks/changePPPPDIdForPPPPDXMLS', {
                    data:{
                        ppppd_id: ppppd_id,
                        paycheck_xml_ids: selected_ids,
                        paycheck_period_id: paycheck_period_id
                    },
                    async: true,
                    loadingCircle: $('body'),
                    success_function: function()
                    {
                        sima.refreshActiveTab(ppppd_xmls_guitable);
                    }
                });
            },
            view: 'guiTable',
            fixed_filter: {
                paycheck_periods_to_ppppds: {
                    paycheck_period: {
                        ids: paycheck_period_id
                    }
                },
                scopes: {
                    notWithPPPPDIdsScope: fixed_filter.ppppd.ids
                }
            }
        });
    };
    
    function updateCompareProblemsDiv(div_for_problems_id, hidden_div_id)
    {
        var div_for_problems = $('#'+div_for_problems_id);
        var hidden_div = $('#'+hidden_div_id);
        
        var message = '';
        if(sima.isEmpty(hidden_div.data('upload_key')))
        {
            message += 'Nije izabran xml</br>';
        }
        else
        {
            message += 'XML: '+hidden_div.data('upload_file_display_name')+'</br>';
        }
        if(sima.isEmpty(hidden_div.data('paycheck_period_id')))
        {
            message += 'Nije izabrana isplata zarade</br>';
        }
        else
        {
            message += 'Isplata zarade: '+hidden_div.data('paycheck_period_display_name');
        }
        
        div_for_problems.html(message);
    };
}
