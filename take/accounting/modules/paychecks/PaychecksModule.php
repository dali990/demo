<?php

class PaychecksModule extends SIMAModule
{
    public function init()
    {
        parent::init();
               
//        File::registerPermanentBehavior('FilePaychecksBehavior');
//        Payment::registerPermanentBehavior('PaymentPaychecksBehavior');
//        Employee::registerPermanentBehavior('EmployeePaychecksBehavior');
//        Company::registerPermanentBehavior('CompanyPaychecksBehavior');
//        PaycheckPeriod::registerPermanentBehavior('PaycheckPeriodBehavior');
        
        Yii::app()->setParams(include(dirname(__FILE__).'/config/main.php'));
        $this->formatPaycheckAdditionalTypesConf();
    }
    
    protected function permanentBehaviors(){
        return [
            'File' => 'FilePaychecksBehavior',
            'Payment' => 'PaymentPaychecksBehavior',
            'Employee' => 'EmployeePaychecksBehavior',
            'Company' => 'CompanyPaychecksBehavior',
            'PaycheckPeriod' => 'PaycheckPeriodBehavior',
            'PPPPD' => 'PPPPDPaychecksBehavior',
            'M4EmployeeRow' => 'M4EmployeeRowBehavior'
        ];
    }
    
    public function getBaseUrl()
    {
        return Yii::app()->assetManager->publish(Yii::getPathOfAlias('paychecks.assets'));
    }
    
    public function registerManual()
    {
        $urlScript = Yii::app()->assetManager->publish(Yii::getPathOfAlias('paychecks.assets'));

        Yii::app()->clientScript->registerCssFile($urlScript . '/css/default.css');
        Yii::app()->clientScript->registerScriptFile($urlScript .'/js/simaPaycheckPeriod.js');
        Yii::app()->clientScript->registerScriptFile($urlScript .'/js/simaPaychecks.js');
        Yii::app()->clientScript->registerScriptFile($urlScript .'/js/simaPaychecksPPPPD.js');
        
        $sima_init_script = "sima.paychecks = new SIMAPaychecks();";
        Yii::app()->clientScript->registerScript(SIMAHtml::uniqid(),$sima_init_script, CClientScript::POS_READY); 
        Yii::app()->clientScript->registerScript(SIMAHtml::uniqid(),"sima.paychecksPPPPD = new SIMAPaychecksPPPPD();", CClientScript::POS_READY);    
        Yii::app()->clientScript->registerScript(SIMAHtml::uniqid(),"sima.paycheckPeriod = new SIMAPaycheckPeriod();", CClientScript::POS_READY);    
    }
    
    public function getGuiTableDefinition($index)
    {
        switch($index)
        {
            case '0': return [
                'label' => Yii::t('PaychecksModule.Suspension', 'SuspensionGroups'),
                'settings' => array(
                    'model' => SuspensionGroup::model(),
                    'add_button' => true
                )
            ];
            case '1': 
                $uniq = SIMAHtml::uniqid();
                $add_button = false;
                $custom_buttons = [];
                $additional_options_html = null;
                $sync_type = Yii::app()->configManager->get('accounting.paychecks.paycheck_codebook_sync_type', false);
                if($sync_type === 'manual_sync' || $sync_type === 'manual_sync_with_notification')
                {
                    $add_button = true;
                    $custom_buttons = [
                        Yii::t('PaychecksModule.Codebook', 'SplitPeriod') => [
                            'func' => 'sima.adminCodebook.splitPaycheckCodebookPeriod($(this))',
                        ],
                        Yii::t('PaychecksModule.Codebook', 'MergePeriods') => [
                            'func' => 'sima.adminCodebook.mergePaycheckCodebookPeriods($(this))',
                        ],
                    ];
                    $button_parameters = [
                        'title' => Yii::t('PaychecksModule.Codebook', 'SyncPaychecksCodebook'),
                        'onclick' => ["sima.paychecks.syncPaychecksFromCodebook","paychecks_$uniq"],
                    ];
                    $additional_options_html = Yii::app()->controller->widget(SIMAButtonVue::class, $button_parameters, true);
                }
                
                return [
                    'label' => Yii::t('PaychecksModule.Codebook', 'Codebook'),
                    'settings' => [
                        'id' => "paychecks_$uniq",
                        'model' => PaycheckCodebook::class,
                        'add_button' => $add_button,
                        'custom_buttons' => $custom_buttons,
                        'select_filter' => [
                            'scopes' => [
                                'order_start_use_DESC'
                            ]
                        ],
                        'additional_options_html' => $additional_options_html
                    ]
                ];
            default:
                throw new SIMAGuiTableIndexNotFound('warehouse', $index);
        }
    }
    
    private function formatPaycheckAdditionalTypesConf()
    {
        $paycheck_additional_types = Yii::app()->params['paycheck_additional_types'];
        $paycheck_additional_types_formated = [];
        foreach($paycheck_additional_types as $pat)
        {
            $paycheck_additional_types_formated[$pat['value']] = $pat;
        }
        Yii::app()->params['paycheck_additional_types_formated'] = $paycheck_additional_types_formated;
    }
}
