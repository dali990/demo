<?php

class PaycheckController extends SIMAController
{
    public function filters()
    {
        return array(
            'ajaxOnly-GenerateDocumentsForAllTaxesPaidConfirmationsForYear-filingAllTaxesPaidConfirmationsForYear-'
            . 'reGenerateDocumentsPdfForAllTaxesPaidConfirmationsForYear-sendDocumentsPdfForAllTaxesPaidConfirmationsForYear-'
            . 'getPaychecksDiffBetweenLocalAndCodebook'
                ) + parent::filters();
    }

    public function actionIndex($id)
    {
        $response = $this->renderPaycheckIndexResponse($id);

        $this->respondOk($response);
    }

    public function actionEmployeePaychecks($model_id)
    {
        $model = ModelController::GetModel('Employee', $model_id);
        
        $html = $this->renderPartial('paychecksForEmployee', array(
            'model' => $model,
        ), true, true);
        
        $this->respondOK(['html'=>$html]);
    }
    
    private function renderPaycheckIndexResponse($id)
    {
        $html = "";
        $title = "Nije placanje";
        
        $paycheck = Paycheck::model()->findByPk($id);
        
        if(isset($paycheck))
        {
//            $html = $this->renderPartial('index', array(
            $html = $this->renderPartial('index_new', array(
                'model' => $paycheck,
            ), true, false);
            
            $title = $paycheck->DisplayName;
        }

        return array(
            'html' => $html,
            'title' => $title
        );
    }
    
    public function actionComparePaychecksTab()
    {
        $html = $this->renderPartial('compare_paychecks', array(), true, true);
        $title = Yii::t('PaychecksModule.Paycheck', 'ComparePaychecks');
        
        $this->respondOK(['html' => $html, 'title' => $title]);
    }
    
    public function actionGenerateTaxesPaidConfirmationDocument($taxes_paid_confirmation_id)
    {
        if (!isset($taxes_paid_confirmation_id))
        {
            throw new Exception('actionGenerateTaxesPaidConfirmationDocument - nisu lepo zadati parametri');
        }
        
        $taxes_paid_confirmation = ModelController::GetModel('TaxesPaidConfirmation', $taxes_paid_confirmation_id);
        $taxes_paid_confirmation->generateDocument();
        
        $this->respondOK([], $taxes_paid_confirmation);
    }
    
    public function actionTaxesPaidConfirmationsByYear()
    {
        $curr_year = Year::get(date('Y'));
        $prev_year = $curr_year->prev();
        
        $html = $this->renderPartial('taxes_paid_confirmations_by_year', [
            'prev_year'=>$prev_year
        ], true, true);

        $this->respondOK(array(
            'html' => $html,
            'title' => 'Pregled potvrda o plaćenim porezima i doprinosima po odbitku'            
        ));
    }
    
    public function actionGenerateDocumentsForAllTaxesPaidConfirmationsForYear()
    {
        $data = $this->setEventHeader();
        
        if (!isset($data['year_id']))
        {
            throw new SIMAException('Morate zadati godinu.');
        }
        
        $year_id = $data['year_id'];
        $year = Year::model()->findByPk($year_id)->year;
        
        $criteria = new SIMADbCriteria([
            'Model' => Employee::model(),
            'model_filter' => [
                'paychecks' => [
                    'paycheck_period' => [
                        'payment_date' => "1.1.$year. <> 31.12.$year.",
                    ],
                ],
                
            ],
            'order' => 't.id'
        ]);
        $employees = Employee::model()->findAll($criteria);
        
//        error_log(__METHOD__.count($employees));
        
        $has_created_documents = true;
        $cnt = count($employees);
        $i = 0;
        $max_while_cycle_exec_time_seconds = ini_get('max_execution_time');
        foreach ($employees as $employee) 
        {
            set_time_limit($max_while_cycle_exec_time_seconds);
            $percent = round(($i/$cnt)*100, 0, PHP_ROUND_HALF_DOWN);
            $this->sendUpdateEvent($percent);
            $taxes_paid_confirmation = TaxesPaidConfirmation::model()->findByAttributes([
                'year_id'=>$year_id,
                'employee_id'=>$employee->id
            ]);
            if (is_null($taxes_paid_confirmation))
            {
                $taxes_paid_confirmation = new TaxesPaidConfirmation();
                $taxes_paid_confirmation->year_id = $year_id;
                $taxes_paid_confirmation->employee_id = $employee->id;
                $taxes_paid_confirmation->save();
                $taxes_paid_confirmation->refresh();

                $has_created_documents = true;
            }
            $taxes_paid_confirmation->generateItems();
            unset($taxes_paid_confirmation);
            $i++;
        }
        
        $this->sendEndEvent([
            'has_created_documents'=>$has_created_documents
        ]);
    }
    
    public function actionFilingAllTaxesPaidConfirmationsForYear()
    {
        $data = $this->setEventHeader();
        
        Yii::app()->setUpdateModelViews(false);
        $curr_user_id = Yii::app()->user->id;
        
        $year_id = $this->filter_input($data, 'year_id');
        $new_filing_id = $this->filter_input($data, 'new_filing_id');
        
        $new_filing = Filing::model()->findByPk($new_filing_id);
        if (empty($new_filing->file->responsible_id) || $new_filing->file->responsible_id !== $curr_user_id)
        {
            throw new SIMAWarnException('Morate biti odgovrni za dokument');
        }
        if ($new_filing->confirmed || $new_filing->returned)
        {
            throw new SIMAWarnException('Dokumentu morate da skinete potvrde');
        }
        if (!empty($new_filing->subnumber))
        {
            throw new SIMAWarnException('korisceni zavodni broj ne moze imati pod broj');
        }
        
        $old_filing_number = null;
        $old_filing_date = null;
        
        $taxes_paid_confirmations = TaxesPaidConfirmation::model()->byID()->findAllByAttributes([
            'year_id'=>$year_id
        ]);
        $has_changes = false;
        $cnt = count($taxes_paid_confirmations);
        if ($cnt === 0)
        {
            throw new SIMAWarnException('Morate prvo kreirati dokumente.');
        }
        else
        {
            $i = 0;
            $max_while_cycle_exec_time_seconds = ini_get('max_execution_time');
            foreach ($taxes_paid_confirmations as $taxes_paid_confirmation) 
            {
                $info_changed = false;
                set_time_limit($max_while_cycle_exec_time_seconds);
                $percent = round(($i/$cnt)*100, 0, PHP_ROUND_HALF_DOWN);
                $this->sendUpdateEvent($percent);
                if (!isset($taxes_paid_confirmation->file->filing))
                {
                    if (isset($taxes_paid_confirmation->employee->person->user))
                    {
                        $delivery_type_id = Yii::app()->configManager->get('legal.email_delivery_type_id', true);
                        $delivery_type_name = 'email';
                    }
                    else
                    {
                        $delivery_type_id = Yii::app()->configManager->get('legal.post_delivery_type_id', true);
                        $delivery_type_name = 'posta';
                    }
                    $filing_to_delivery_types = null;
                    if (!is_null($delivery_type_id))
                    {            
                        $delivery_ids = [];
                        $id['id'] = $delivery_type_id;
                        $id['display_name'] = $delivery_type_name;
                        $id['class'] = '_non-editable';
                        array_push($delivery_ids, $id);
                        $filing_to_delivery_types = [
                            'ids'=>$delivery_ids,
                            'default_item'=>[]                
                        ];
                    }
                    
                    $filing = new Filing();
                    $filing->id = $taxes_paid_confirmation->file->id;
                    $filing->confirmed = true;
                    $filing->located_at_id = $curr_user_id;
                    $filing->creator_id = $curr_user_id;
                    $filing->partner_id = $taxes_paid_confirmation->employee_id;
                    $filing->in = 0;
                    $filing->filing_to_delivery_types = CJSON::encode($filing_to_delivery_types);                  
                                       
                    $taxes_paid_confirmation->file->filing = $filing;
                    unset($filing);
                    $info_changed = true;
                }
                
                if ($new_filing->number != $taxes_paid_confirmation->file->filing->number)
                {
                    if (empty($old_filing_number) && !empty($taxes_paid_confirmation->file->filing->number))
                    {
                        $old_filing_number = $taxes_paid_confirmation->file->filing->number;
                        $old_filing_date = $taxes_paid_confirmation->file->filing->date;
                    }
                    
                    $taxes_paid_confirmation->file->filing->number = $new_filing->number;
                    $taxes_paid_confirmation->file->filing->subnumber = $i+1;//!!!!!!!!1
                    $taxes_paid_confirmation->file->filing->date = $new_filing->date;
                    
                    $info_changed = true;
                }
                
                if ($info_changed)
                {
                    $taxes_paid_confirmation->file->filing->save();
                    $has_changes = true;
                }

                unset($taxes_paid_confirmation->employee);
                unset($taxes_paid_confirmation->file);
                $i++;
            }
        }
        foreach ($new_filing->delivery_types as $dl_type)
        {
            $dl_type->delete();
        }
        $_file = $new_filing->file;
        $new_filing->delete();
        $_file->recycle();
        
        if (!empty($old_filing_number))
        {
            $file = new File();
            $file->responsible_id = $curr_user_id;
            $file->name = 'regeneretad from PPP-PO';
            $file->save();
                    
            $filing = new Filing();
            $filing->id = $file->id;
            $filing->located_at_id = $curr_user_id;
            $filing->creator_id = $curr_user_id;
            $filing->partner_id = $curr_user_id;
            $filing->in = null;
            $filing->number = $old_filing_number;
            $filing->date = $old_filing_date;
            $filing->save();
        }
        
        $this->sendEndEvent([
            'has_changes'=>$has_changes
        ]);
    }
    
    public function actionReGenerateDocumentsPdfForAllTaxesPaidConfirmationsForYear()
    {        
        $data = $this->setEventHeader();
        
        if (!isset($data['year_id']))
        {
            throw new SIMAException('Morate zadati godinu.');
        }
        
        $year_id = $data['year_id'];
        $taxes_paid_confirmations = TaxesPaidConfirmation::model()->findAllByAttributes([
            'year_id'=>$year_id
        ]);
        
        $cnt = count($taxes_paid_confirmations);
        if (count($cnt) === 0)
        {
            throw new SIMAWarnException('Ne postoji nijedan dokument.');
        }
        else
        {
            $i = 0;
            $max_while_cycle_exec_time_seconds = ini_get('max_execution_time');
            foreach ($taxes_paid_confirmations as $taxes_paid_confirmation) 
            {
                set_time_limit($max_while_cycle_exec_time_seconds);
                $percent = round(($i/$cnt)*100, 0, PHP_ROUND_HALF_DOWN);
                $this->sendUpdateEvent($percent);
                $taxes_paid_confirmation->generateDocument();
                $i++;
            }
        }
        
        $this->sendEndEvent();
    }
    
    public function actionSendDocumentsPdfForAllTaxesPaidConfirmationsForYear()
    {
        $data = $this->setEventHeader();
        
        if (!isset($data['year_id']))
        {
            throw new SIMAException('Morate zadati godinu.');
        }
        
        $year_id = $data['year_id'];
        $only_download = filter_var($data['only_download'], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
        $year = Year::model()->findByPk($year_id);
        $title = 'PPPPO za '.$year->DisplayName.' godinu';
        $taxes_paid_confirmations = TaxesPaidConfirmation::model()->findAllByAttributes([
            'year_id'=>$year_id
        ]);
        
        $cnt = count($taxes_paid_confirmations);
        if (count($cnt) === 0)
        {
            throw new SIMAWarnException('Ne postoji nijedan dokument.');
        }
        else
        {
            $employees_documents = [];
            $i = 0;
            $max_while_cycle_exec_time_seconds = ini_get('max_execution_time');            
            foreach ($taxes_paid_confirmations as $taxes_paid_confirmation) 
            {
                set_time_limit($max_while_cycle_exec_time_seconds);
                $percent = round(($i/$cnt)*60, 0, PHP_ROUND_HALF_DOWN);
                $this->sendUpdateEvent($percent);
                if (!isset($taxes_paid_confirmation->file->last_version))
                {
                    $taxes_paid_confirmation->generateDocument();
                }
                $employee_documents = [];
                $employee_documents['employee'] = $taxes_paid_confirmation->employee;
                $employee_documents['documents'] = $taxes_paid_confirmation->file;
                array_push($employees_documents, $employee_documents);
                $i++;
            }            
            $merged_documents_temp_file_name = Employee::mergeEmployeesDocuments($employees_documents, 
                    $title, !$only_download, function($percent){                        
                        $this->sendUpdateEvent(60+$percent*(40/100));
                    });
            $this->sendUpdateEvent(100);
        }
        
        if (is_null($merged_documents_temp_file_name) && $only_download)
        {
            throw new SIMAWarnException('Nema dokumenta za preuzimanje.');
        }
        else
        {
            $this->sendEndEvent([
                'tn'=>$merged_documents_temp_file_name,
                'dn'=>$title.'.zip'
            ]);
        }
    }
    
    public function actionSendTaxesPaidConfirmationToUser($taxes_paid_confirmation_id)
    {
        $taxes_paid_confirmation = TaxesPaidConfirmation::model()->findByPk($taxes_paid_confirmation_id);
        $employee_documents = [];
        $employee_document['employee'] = $taxes_paid_confirmation->employee;
        $employee_document['documents'] = $taxes_paid_confirmation->file;
        array_push($employee_documents, $employee_document);        
        $title = 'PPPPO za '.$taxes_paid_confirmation->year->year.' godinu';        
        $merged_documents_temp_file_name = Employee::mergeEmployeesDocuments($employee_documents, $title);
        
        $this->respondOK([
            'tn'=>$merged_documents_temp_file_name,
            'dn'=>$title.'.zip'
        ], $taxes_paid_confirmation);
    }
    
    public function actionRecreateOZFile($paycheckId)
    {
        $paycheck = Paycheck::model()->findByPk($paycheckId);
        
        PaycheckPeriodComponent::CreatePdfForPaycheck($paycheck);
        
        $this->respondOk([
            'message' => Yii::t('BaseModule.Common', 'Success')
        ]);
    }
    
    public function actionSendOZFileForPaycheck($paycheck_id)
    {
        $paycheck = Paycheck::model()->findByPkWithCheck($paycheck_id);
        
        $reasons = [];
        if($paycheck->is_valid === false)
        {
            $reasons[] = 'Zarada nije validna';
        }
        if ($paycheck->confirmed === false)
        {
            $reasons[] = 'Zarada nije potvrdjena';
        }
        if (!isset($paycheck->file_id))
        {
            $reasons[] = 'Nije kreiran OZ listić';
        }
        
        if (!empty($reasons))
        {
            throw new SIMAWarnException('Nije poslat OZ listić iz sledećih razloga: '.implode(', ', $reasons));   
        }
        
        $employee_documents = [
            [
                'employee'=>$paycheck->employee,
                'documents'=>$paycheck->file
            ]
        ];
        
        $oz_email_title = $paycheck->paycheck_period->getAttributeDisplay('oz_email_title');
        
        $merged_documents_temp_file_name = Employee::mergeEmployeesDocuments($employee_documents, $oz_email_title);
        
        $this->respondOK([
            'tn'=>$merged_documents_temp_file_name,
            'dn' => 'OZ.zip'
        ], $paycheck);
    }
    
    public function actionUnifiedPaycheck()
    {
        $uniq =  SIMAHtml::uniqid();
        $html = Yii::app()->controller->renderContentLayout([
            'name'=>Yii::t('MainMenu', 'UnifiedPaychecks')
        ], [
            [                                         
                'html'=>$this->widget('SIMAGuiTable', [
                    'id' => 'unified_paychecks'.$uniq,
                    'model' =>  UnifiedPaycheck::model(),
                    'add_button' => true
                ], true),
            ]            
        ], [
            'id'=>$uniq,
        ]);                

        $this->respondOK([
            'html' => $html,
            'title' => Yii::t('MainMenu', 'UnifiedPaychecks')
        ]);
    }
    
    public function actionPaygrades()
    {
        $uniq =  SIMAHtml::uniqid();
        $html = Yii::app()->controller->renderContentLayout([
            'name'=>'Platni razredi'
        ], [
            [
                'proportion'=>0.4,
                'min_width'=>300,                
                'html'=>$this->renderPartial('employees_paygrades_left_side', [
                    'uniq'=>$uniq,                    
                ], true, false)
            ],
            [
                'proportion'=>0.6,
                'min_width'=>300,
                'html'=>$this->renderPartial('employees_paygrades_right_side', [
                    'uniq'=>$uniq
                ], true, false)
            ]           
        ], [
            'id'=>$uniq,
        ]);                

        $this->respondOK([
            'html' => $html,
            'title' => 'Platni razredi'
        ]);
    }
  
    
    public function actionBonusPaygrades()
    {
        $tab_name = 'Bonus platni razredi';
        $uniq =  SIMAHtml::uniqid();
        $html = Yii::app()->controller->renderContentLayout([
            'name' => $tab_name
        ], [
            [
//                'proportion'=>0.4,
//                'min_width'=>300,                
                'html'=>$this->renderPartial('employees_bonus_paygrades_left_side', [
                    'uniq'=>$uniq,                    
                ], true, false)
            ],
//            [
//                'proportion'=>0.6,
//                'min_width'=>300,
//                'html'=>$this->renderPartial('employees_bonus_paygrades_right_side', [
//                    'uniq'=>$uniq
//                ], true, false)
//            ]           
        ], [
            'id'=>$uniq,
        ]);                

        $this->respondOK([
            'html' => $html,
            'title' => $tab_name
        ]);
    }

    
    public function actionGetPaychecksDiffBetweenLocalAndCodebook()
    {
        $this->setEventHeader();
        
        $codebook_paychecks_codebook = AdminController::getDataFromCodebook(Yii::app()->params['codebook_base'].AdminController::$codebook_const_path_part.'gePaychecksCodebookFromCodebook');
        
        $start_use_column_display = Yii::t('PaychecksModule.PaycheckCodebook', 'StartUse');
        $end_use_column_display = Yii::t('PaychecksModule.PaycheckCodebook', 'EndUse');
        $pio_empl_column_display = Yii::t('PaychecksModule.PaycheckCodebook', 'PioEmpl');
        $zdr_empl_column_display = Yii::t('PaychecksModule.PaycheckCodebook', 'ZdrEmpl');
        $nez_empl_column_display = Yii::t('PaychecksModule.PaycheckCodebook', 'NezEmpl');
        $tax_empl_column_display = Yii::t('PaychecksModule.PaycheckCodebook', 'TaxEmpl');
        $tax_release_column_display = Yii::t('PaychecksModule.PaycheckCodebook', 'TaxRelease');
        $pio_comp_column_display = Yii::t('PaychecksModule.PaycheckCodebook', 'PioComp');
        $zdr_comp_column_display = Yii::t('PaychecksModule.PaycheckCodebook', 'ZdrComp');
        $nez_comp_column_display = Yii::t('PaychecksModule.PaycheckCodebook', 'NezComp');
        $min_contribution_base_column_display = Yii::t('PaychecksModule.PaycheckCodebook', 'MinTaxBase');
        $max_contribution_base_column_display = Yii::t('PaychecksModule.PaycheckCodebook', 'MaxTaxBase');
                
        $codebook_unique_fields = [];
        $items = [];
        $i = 1;
        
        $codebook_paychecks_codebook_cnt = count($codebook_paychecks_codebook);
        
        foreach ($codebook_paychecks_codebook as $codebook_paycheck_codebook)
        {
            $percent = round(($i/$codebook_paychecks_codebook_cnt)*70, 0, PHP_ROUND_HALF_DOWN);
            $this->sendUpdateEvent($percent);
            
            $local_pc = PaycheckCodebook::model()->findByAttributes([
                'start_use' => $codebook_paycheck_codebook['start_use']
            ]);
            if (empty($local_pc))
            {               
                $items[] = [
                    'status' => 'ADD',
                    'columns' => [
                        $start_use_column_display => ['new_value' =>  SIMAHtml::DbToUserDate($codebook_paycheck_codebook["start_use"])],
                        $end_use_column_display => ['new_value' =>  SIMAHtml::DbToUserDate($codebook_paycheck_codebook["end_use"])],
                        $pio_empl_column_display => ['new_value' => SIMAHtml::number_format($codebook_paycheck_codebook['pio_empl'],2)],
                        $zdr_empl_column_display => ['new_value' => SIMAHtml::number_format($codebook_paycheck_codebook['zdr_empl'],2)],
                        $nez_empl_column_display => ['new_value' => SIMAHtml::number_format($codebook_paycheck_codebook['nez_empl'],2)],
                        $tax_empl_column_display => ['new_value' => SIMAHtml::number_format($codebook_paycheck_codebook['tax_empl'],2)],
                        $tax_release_column_display => ['new_value' => SIMAHtml::number_format($codebook_paycheck_codebook['tax_release'],2)],
                        $pio_comp_column_display => ['new_value' => SIMAHtml::number_format($codebook_paycheck_codebook['pio_comp'],2)],
                        $zdr_comp_column_display => ['new_value' => SIMAHtml::number_format($codebook_paycheck_codebook['zdr_comp'],2)],
                        $nez_comp_column_display => ['new_value' => SIMAHtml::number_format($codebook_paycheck_codebook['nez_comp'],2)],
                        $min_contribution_base_column_display => ['new_value' => SIMAHtml::number_format($codebook_paycheck_codebook['min_contribution_base'],2)],
                        $max_contribution_base_column_display => ['new_value' => SIMAHtml::number_format($codebook_paycheck_codebook['max_contribution_base'],2)],
                    ],
                    'data' => [
                        'status' => 'ADD',
                        'model' => 'PaycheckCodebook',
                        'attributes' => [
                            'start_use' => $codebook_paycheck_codebook["start_use"],
                            'end_use' => $codebook_paycheck_codebook['end_use'],
                            'pio_empl' => $codebook_paycheck_codebook['pio_empl'],
                            'zdr_empl' => $codebook_paycheck_codebook['zdr_empl'],
                            'nez_empl' => $codebook_paycheck_codebook['nez_empl'],
                            'tax_empl' => $codebook_paycheck_codebook['tax_empl'],
                            'tax_release' => $codebook_paycheck_codebook['tax_release'],
                            'pio_comp' => $codebook_paycheck_codebook['pio_comp'],
                            'zdr_comp' => $codebook_paycheck_codebook['zdr_comp'],
                            'nez_comp' => $codebook_paycheck_codebook['nez_comp'],
                            'min_contribution_base' => $codebook_paycheck_codebook['min_contribution_base'],
                            'max_contribution_base' => $codebook_paycheck_codebook['max_contribution_base']
                        ]
                    ]
                ];               
            }
            else
            {
                if (
                        $codebook_paycheck_codebook["start_use"] !== $local_pc->getAttribute('start_use') ||
                        $codebook_paycheck_codebook["end_use"] !== $local_pc->getAttribute('end_use') ||
                        doubleval($codebook_paycheck_codebook['pio_empl']) !== $local_pc->pio_empl ||
                        doubleval($codebook_paycheck_codebook['zdr_empl']) !== $local_pc->zdr_empl ||
                        doubleval($codebook_paycheck_codebook['nez_empl']) !== $local_pc->nez_empl ||
                        doubleval($codebook_paycheck_codebook['tax_empl']) !== $local_pc->tax_empl ||
                        doubleval($codebook_paycheck_codebook['tax_release']) !== $local_pc->tax_release ||
                        doubleval($codebook_paycheck_codebook['pio_comp']) !== $local_pc->pio_comp ||
                        doubleval($codebook_paycheck_codebook['zdr_comp']) !== $local_pc->zdr_comp ||
                        doubleval($codebook_paycheck_codebook['nez_comp']) !== $local_pc->nez_comp ||
                        doubleval($codebook_paycheck_codebook['min_contribution_base']) !== $local_pc->min_contribution_base ||
                        doubleval($codebook_paycheck_codebook['max_contribution_base']) !== $local_pc->max_contribution_base 
 
                    )
                {                    
                    $items[] = [
                        'status' => 'MODIFY',
                        'columns' => [
                            $start_use_column_display => [
                                'old_value' => $local_pc->start_use,
                                'new_value' => SIMAHtml::DbToUserDate($codebook_paycheck_codebook["start_use"])
                            ],
                            $end_use_column_display => [
                                'old_value' => $local_pc->end_use,
                                'new_value' => SIMAHtml::DbToUserDate($codebook_paycheck_codebook["end_use"])
                            ],
                            $pio_empl_column_display => [
                                'old_value' => $local_pc->getAttributeDisplay('pio_empl'),
                                'new_value' => SIMAHtml::number_format($codebook_paycheck_codebook['pio_empl'],2)
                            ],
                            $zdr_empl_column_display => [
                                'old_value' => $local_pc->getAttributeDisplay('zdr_empl'),
                                'new_value' => SIMAHtml::number_format($codebook_paycheck_codebook['zdr_empl'],2)
                            ],
                            $nez_empl_column_display => [
                                'old_value' => $local_pc->getAttributeDisplay('nez_empl'),
                                'new_value' => SIMAHtml::number_format($codebook_paycheck_codebook['nez_empl'],2)
                            ],
                            $tax_empl_column_display => [
                                'old_value' => $local_pc->getAttributeDisplay('tax_empl'),
                                'new_value' => SIMAHtml::number_format($codebook_paycheck_codebook['tax_empl'],2)
                            ],
                            $tax_release_column_display => [
                                'old_value' => $local_pc->getAttributeDisplay('tax_release'),
                                'new_value' => SIMAHtml::number_format($codebook_paycheck_codebook['tax_release'],2)
                            ],
                            $pio_comp_column_display => [
                                'old_value' => $local_pc->getAttributeDisplay('pio_comp'),
                                'new_value' => SIMAHtml::number_format($codebook_paycheck_codebook['pio_comp'],2)
                            ],
                            $zdr_comp_column_display => [
                                'old_value' => $local_pc->getAttributeDisplay('zdr_comp'),
                                'new_value' => SIMAHtml::number_format($codebook_paycheck_codebook['zdr_comp'],2)
                            ],
                            $nez_comp_column_display => [
                                'old_value' => $local_pc->getAttributeDisplay('nez_comp'),
                                'new_value' => SIMAHtml::number_format($codebook_paycheck_codebook['nez_comp'],2)
                            ],
                            $min_contribution_base_column_display => [
                                'old_value' => $local_pc->getAttributeDisplay('min_contribution_base'),
                                'new_value' => SIMAHtml::number_format($codebook_paycheck_codebook['min_contribution_base'],2)
                            ],
                            $max_contribution_base_column_display => [
                                'old_value' => $local_pc->getAttributeDisplay('max_contribution_base'),
                                'new_value' => SIMAHtml::number_format($codebook_paycheck_codebook['max_contribution_base'],2)
                            ],
                        ],
                        'data' => [
                            'status' => 'MODIFY',
                            'model' => 'PaycheckCodebook',
                            'model_id' => $local_pc->id,
                            'attributes' => [
                                'start_use' => $codebook_paycheck_codebook["start_use"],
                                'end_use' => $codebook_paycheck_codebook['end_use'],
                                'pio_empl' => $codebook_paycheck_codebook['pio_empl'],
                                'zdr_empl' => $codebook_paycheck_codebook['zdr_empl'],
                                'nez_empl' => $codebook_paycheck_codebook['nez_empl'],
                                'tax_empl' => $codebook_paycheck_codebook['tax_empl'],
                                'tax_release' => $codebook_paycheck_codebook['tax_release'],
                                'pio_comp' => $codebook_paycheck_codebook['pio_comp'],
                                'zdr_comp' => $codebook_paycheck_codebook['zdr_comp'],
                                'nez_comp' => $codebook_paycheck_codebook['nez_comp'],
                                'min_contribution_base' => $codebook_paycheck_codebook['min_contribution_base'],
                                'max_contribution_base' => $codebook_paycheck_codebook['max_contribution_base']
                            ]
                        ]
                    ]; 

                }   
            }

            $i++;
            $codebook_unique_fields[] = $codebook_paycheck_codebook["start_use"];
        }       
                
        $local_pcs = PaycheckCodebook::model()->findAll(new SIMADbCriteria([
            'Model' => 'PaycheckCodebook',
            'model_filter' => [
                'filter_scopes' => [
                    'withoutUniqueFields' => [$codebook_unique_fields]
                ]
            ]
        ]));
        $j = 1;
        $local_pcs_cnt = count($local_pcs);     
        foreach ($local_pcs as $local_pc)
        {
            $percent = 70 + round(($j/$local_pcs_cnt)*30, 0, PHP_ROUND_HALF_DOWN);
            $this->sendUpdateEvent($percent);
            $items[] = [
                'status' => 'REMOVE',
                'columns' => [
                    $start_use_column_display => ['old_value' => $local_pc->start_use],
                    $end_use_column_display => ['old_value' => $local_pc->end_use],
                    $pio_empl_column_display => ['old_value' => $local_pc->getAttributeDisplay('pio_empl')],
                    $zdr_empl_column_display => ['old_value' => $local_pc->getAttributeDisplay('zdr_empl')],
                    $nez_empl_column_display => ['old_value' => $local_pc->getAttributeDisplay('nez_empl')],
                    $tax_empl_column_display => ['old_value' => $local_pc->getAttributeDisplay('tax_empl')],
                    $tax_release_column_display => ['old_value' => $local_pc->getAttributeDisplay('tax_release')],
                    $pio_comp_column_display => ['old_value' => $local_pc->getAttributeDisplay('pio_comp')],
                    $zdr_comp_column_display => ['old_value' => $local_pc->getAttributeDisplay('zdr_comp')],
                    $nez_comp_column_display => ['old_value' => $local_pc->getAttributeDisplay('nez_comp')],
                    $min_contribution_base_column_display => ['old_value' => $local_pc->getAttributeDisplay('min_contribution_base')],
                    $max_contribution_base_column_display => ['old_value' => $local_pc->getAttributeDisplay('max_contribution_base')]
                ],
                'data' => [
                    'status' => 'REMOVE',
                    'model' => 'PaycheckCodebook',
                    'model_id' => $local_pc->id,
                ]
            ];
            $j++;
        }
        
        $html = '';      
        if (!empty($items))
        {
            $html = $this->widget('SIMADiffViewer', [                   
                'diffs' => [
                    [
                        'type' => 'group',
                        'group_id' => SIMAHtml::uniqid(),
                        'group_name' => Yii::t('PaychecksModule.Codebook', 'SyncPaychecksCodebook'),
                        'columns' => [
                            $start_use_column_display,
                            $end_use_column_display,
                            $pio_empl_column_display,
                            $zdr_empl_column_display,
                            $nez_empl_column_display,
                            $tax_empl_column_display,
                            $tax_release_column_display,
                            $pio_comp_column_display,
                            $zdr_comp_column_display,
                            $nez_comp_column_display,
                            $min_contribution_base_column_display,
                            $max_contribution_base_column_display
                        ],
                        'items' => $items
                    ]
                ]
            ], true);
        }
        
        $this->sendEndEvent([
            'html' => $html
        ]);
    }
}

