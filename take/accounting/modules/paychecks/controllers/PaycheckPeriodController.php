<?php

class PaycheckPeriodController extends SIMAController
{
    public function filters()
    {
        return [
            'ajaxOnly-CalcM4ForEmployeesForYear-RecalcPeriod-RegeneratePPPPD-GeneratePaychecksXML-CreateOZFiles-SendOZFiles-exportM4-FillGroupBonus-FillAllGroupBonus-FillCustomGroupBonus-CalcGroupBonus-ExportAllPaychecks'
        ] + parent::filters();
    }
    
    public function actionIndex()
    {
        $uniq = SIMAHtml::uniqid();
        $vertical_menu_id = $uniq.'vmenu';
        $guitable_id = $uniq.'guitable';
        $tabs_id = $uniq.'tabs';
        
        $register_script = "function on_select$uniq(row) { $('#$tabs_id').show();";
        $register_script .= "$('#$tabs_id').simaTabs('set_list_tabs_params',{
            list_tabs_trigger_tag: sima.getTag('".SIMAHtml::getTag(PaycheckPeriod::model())."', row.attr('model_id')),
            list_tabs_get_params:{id:row.attr('model_id')}
        }); } ";
        $register_script .= "function on_unselect$uniq(row) { $('#$tabs_id').hide(); }";
        Yii::app()->clientScript->registerScript($uniq, $register_script, CClientScript::POS_READY);
        
        $subactions = [
            [
                'title' => 'Podaci o zaposlenima',
                'onclick' => ['sima.dialog.openActionInDialogAsync', 'accounting/paychecks/paychecksEmployee/index']
            ],
            [
                'title' => 'Platni razredi',
                'onclick' => ['sima.dialog.openActionInDialogAsync', 'accounting/paychecks/paycheck/paygrades']
            ],
            [
                'title' => 'Bonus platni razredi',
                'onclick' => ['sima.dialog.openActionInDialogAsync', 'accounting/paychecks/paycheck/bonusPaygrades']
            ],
            [
                'title' => Yii::t('MainMenu', 'UnifiedPaychecks'),
                'onclick' => ['sima.dialog.openActionInDialogAsync', 'accounting/paychecks/paycheck/unifiedPaycheck']
            ],
            [
                'title' => 'Potvrde o plaćenim porezima i doprinosima po odbitku',
                'onclick' => ['sima.dialog.openActionInDialogAsync', 'accounting/paychecks/paycheck/taxesPaidConfirmationsByYear']
            ],
            [
                'title' => Yii::t('PaychecksModule.Suspension', 'SuspensionGroups'),
                'onclick' => ['sima.dialog.openActionInDialogAsync', 'guitable', [
                    'get_params'=>[
                        'id'=>'paychecks_0'
                    ]
                ]]
            ],
            [
                'title' => 'Godisnje statistike',
                'onclick' => ['sima.dialog.openActionInDialogAsync', 'accounting/paychecks/paycheckPeriod/years']
            ],
            [
                'title' => 'Stare zarade',
                'onclick' => ['sima.dialog.openActionInDialogAsync', 'accounting/paychecks/paychecksEmployee/oldPaychecks']
            ],
            [
                'title' => Yii::t('PaychecksModule.Codebook', 'Codebook'),
                'onclick' => ['sima.dialog.openActionInDialogAsync', 'guitable', [
                    'get_params'=>[
                        'id'=>'paychecks_1'
                    ]
                ]]
            ]
        ];
        
//        if(SIMAMisc::isVueComponentEnabled())
//        {
            $settings_button = $this->widget(SIMAButtonVue::class, [
                'icon' => '_main-settings',
                'iconsize' => 18,
                'subactions_data' => $subactions
            ], true);
//        }
//        else
//        {
//            $settings_button = $this->widget('SIMAButton', [
//                'action' => [
//                    'icon' => 'sima-icon _main-settings _18',
//                    'subactions' => $subactions
//                ]
//            ], true);
//        }
        
        $options = [
            [
                'html' => $settings_button
            ]
        ];

        $crit = new SIMADbCriteria([
            'Model' => 'Year',
            'model_filter' => [
                'accounting' => []
            ]
        ]);
        $years = Year::model()->byYear()->findAll($crit);
        $_menu_array = [];
        foreach ($years as $year)
        {
            $_menu_array[] = [
                'display_name' => $year->DisplayName,
                'datas' => [
                    'type' => 'year',
                    'id' => $year->id
                ],
//                'submenu' => $_year_submenu
            ];
        }
        
        $menu_panel = 
            [
                'proportion'=>0.1,
                'max_width' => 120,
                'min_width' => 120,
                'html' => $this->widget('SIMAVMenu', array(
                    'id' => $vertical_menu_id,
                    'menu_array' => $_menu_array
                ),true)
            ];
        
        $periods_panel = [
            'proportion'=>0.3,                                
            'html'=>$this->widget('SIMAGuiTable', [
                'id' => $guitable_id,
                'model' =>  PaycheckPeriod::model(),
                'setRowSelect' => "on_select$uniq",
                'setMultiSelect' => "on_unselect$uniq",
                'setRowUnSelect' => "on_unselect$uniq",
                'add_button' => array(
                    'init_data' => array(
                        'PaycheckPeriod' => array(
                            'creator_user_id' => array(
                                'disabled',
                                'init' => Yii::app()->user->id
                            )
                        )
                    )
                ),
                'fixed_filter'=>array(
                    'filter_scopes' => [
                        'without_old'
                    ], 
                    'display_scopes' => [
                        'byPaymentDateDESC'
                    ], 
                    'ids' => [
                        -1
                    ]
                )
            ], true),
        ];
        
        $tabs_panel = [
            'proportion'=>0.6,                
            'html'=>$this->widget('SIMATabs', array(
                'id' => $tabs_id,
                'list_tabs_model_name' => 'PaycheckPeriod',
                'default_selected'=>'paychecks'
            ), true)           
        ];
        
        $html = Yii::app()->controller->renderContentLayout(
            [
                'name'=>'Obracun zarada',
                'options'=> $options
            ], 
            [$menu_panel,$periods_panel,$tabs_panel], 
            [
                'id'=>$uniq,
                'processOutput' => false
            ]
        );
        
        Yii::app()->clientScript->registerScript(SIMAHtml::uniqid(), 
<<<AAA

$('#$vertical_menu_id').on('selectItem', function(event, item){
        var type = item.data('type');
        var id = item.data('id');
        if (type === 'year')
        {
            $('#$guitable_id').simaGuiTable('setFixedFilter',{
                month: {
                    year: {
                        ids: id
                    }
                },
                filter_scopes:['without_old'], 
                display_scopes:['byPaymentDateDESC'] 
                
            }, true);
//            $('#$guitable_id').simaGuiTable('populate');
        }
            
//            console.log(type);
//        sima.ajax.get('accounting/vat/rightPanel', {
//            get_params: {
//                type: type,
//                id: id
//            },
//            success_function: function(response) {
//                sima.set_html($("#"),response.html,'TRIGGER_right_panel_layout');
//            }
//        });
    });
   
AAA
   ,
            
            CClientScript::POS_READY);
        

        $this->respondOK([
            'html' => $html,
            'title' => 'Obracun zarada'
        ]);
    }
    
    public function actionYears()
    {
        $uniq = SIMAHtml::uniqid();
        $vertical_menu_id = $uniq.'vmenu';
        $display_panel_id = $uniq.'display_div';

        if(SIMAMisc::isVueComponentEnabled())
        {
            $settings_button = $this->widget(SIMAButtonVue::class, [
                'icon' => '_main-settings',
                'iconsize' => 18,
                'subactions_data' => [
                ]
            ], true);
        }
        else
        {
            $settings_button = $this->widget('SIMAButton', [
                'action' => [
                    'icon' => 'sima-icon _main-settings _18',
                    'subactions' => [
                    ]
                ]
            ], true);
        }
        
        $options = [
            [
                'html' => $settings_button
            ]
        ];

        $crit = new SIMADbCriteria([
            'Model' => 'Year',
            'model_filter' => [
                'accounting' => []
            ]
        ]);
        $years = Year::model()->byYear()->findAll($crit);
        $_menu_array = [];
        foreach ($years as $year)
        {
            $_menu_array[] = [
                'display_name' => $year->DisplayName,
                'datas' => [
                    'type' => 'year',
                    'id' => $year->id
                ],
//                'submenu' => $_year_submenu
            ];
        }
        
        $menu_panel = 
            [
                'proportion'=>0.1,
                'max_width' => 120,
                'min_width' => 120,
                'html' => $this->widget('SIMAVMenu', array(
                    'id' => $vertical_menu_id,
                    'menu_array' => $_menu_array
                ),true)
            ];
        
        $tabs_panel = [
            'proportion'=>0.9,                
            'html'=>"<div id='$display_panel_id' class='sima-layout-panel'></div>"          
        ];
        
        $html = Yii::app()->controller->renderContentLayout(
            [
                'name'=>'Godisnje statistike',
                'options'=> $options
            ], 
            [$menu_panel,$tabs_panel], 
            [
                'id'=>$uniq,
                'processOutput' => false
            ]
        );
        
        
        $_script = <<<AAA

$('#$vertical_menu_id').on('selectItem', function(event, item){
        var type = item.data('type');
        var id = item.data('id');
        if (type === 'year')
        {
            sima.ajax.get('accounting/paychecks/paycheckPeriod/yearStatistic', {
                get_params: {
                    id: id
                },
                success_function: function(response) {
                    sima.set_html($("#$display_panel_id"),response.html,'TRIGGER_right_panel_layout');
                }
            });
        };
    });
   
AAA;
            
           Yii::app()->clientScript->registerScript(SIMAHtml::uniqid(), $_script , CClientScript::POS_READY);
        

        $this->respondOK([
            'html' => $html,
            'title' => 'Zarade - godisnje statistike'
        ]);
    }
    
    public function actionYearStatistic($id)
    {
        $html = $this->widget('SIMATabs', [
            'tabs' => [
                [
                    'title' => 'M4 obrazac',
                    'code' => 'm4tab',
                    'action' => 'accounting/paychecks/paycheckPeriod/m4tab',
                    'get_params' => ['year_id' => $id]
                ],
                [
                    'title' => Yii::t('PaychecksModule.M4MFRoll', 'M4MFRolls'),
                    'code' => 'm4mfrolls',
                    'action' => 'accounting/paychecks/paycheckPeriod/M4MFRolls',
                    'get_params' => ['year_id' => $id]
                ]
            ],
            'default_selected' => 'm4tab'
        ], true);
        
        $this->respondOK([
            'html' => $html
        ]);
    }
    
    
    public function actionM4tab($year_id)
    {
        $html = $this->renderPartial('m4tab', [
            'year_id' => $year_id
        ], true);
        
        
        $this->respondOK([
            'html' => $html
        ]);
    }
    
    public function actionM4MFRolls($year_id)
    {
        $html = $this->renderPartial('m4mfrolls', [
            'year_id' => $year_id
        ], true);
        
        
        $this->respondOK([
            'html' => $html
        ]);
    }
    
    public function actionCalcM4ForEmployeesForYear()
    {
        $data = $this->setEventHeader();
        
        $year_id = $this->filter_input($data, 'year_id');
        
        $controller = $this;
        $diffs = M4EmployeeRowBehavior::calcM4ForEmployeesForYear($year_id, function($percent) use ($controller){
            $controller->sendUpdateEvent($percent);
        });

        $html = $this->widget('SIMADiffViewer', [
            'diffs'=>$diffs
        ], true);
        
        $this->sendEndEvent([
            'html' => $html
        ]);
    }

    public function actionExportM4()
    {
        $data = $this->setEventHeader();
        
        $document_type = $this->filter_input($data, 'document_type');
        $year_id = $this->filter_input($data, 'year_id');
        $year_model = Year::model()->findByPkWithCheck($year_id);
        $document_type_extension = '';
        
        if ($document_type === 'pdf')
        {
            $html = $this->renderPartial('m4pdf/m4pdf_css', [], true, false);

            $m4roll_positions = M4MFRoll::model()->byName()->findAllByAttributes([
                'year_id' => $year_id
            ]);

            $i = 1;
            $m4roll_positions_cnt = count($m4roll_positions);
            $max_while_cycle_exec_time_seconds = ini_get('max_execution_time');
            foreach ($m4roll_positions as $m4roll_position) 
            {
                set_time_limit($max_while_cycle_exec_time_seconds);
                $percent = round(($i/$m4roll_positions_cnt)*100, 0, PHP_ROUND_HALF_DOWN);            
                $this->sendUpdateEvent($percent);            

                $html .= $this->renderPartial('m4pdf/m4pdf_html', [
                    'year' => $year_model->year,
                    'm4roll_position' => $m4roll_position
                ], true, false);

                $i++;            
            }
        
            $temporary_file = new TemporaryFile('', null, 'pdf');

            $temporary_file->createPdfFromHtml(
                [
                    'bodyHtml' => $html
                ],
                [
                    'page-size' => 'A3',
                    'orientation' => 'Landscape'
                ]
            );
            $document_type_extension = $document_type;
        }
        elseif ($document_type === 'txt')
        {
            $txt_content = '';
            $m4roll_positions = M4MFRoll::model()->byName()->findAllByAttributes([
                'year_id' => $year_id
            ]);
            $i = 1;
            $m4roll_positions_cnt = count($m4roll_positions);
            $_year = $year_model->year;
            $_pib = Yii::app()->company->PIB;
            $_work_code = Yii::app()->company->work_code->code;
            $max_while_cycle_exec_time_seconds = ini_get('max_execution_time');
            foreach ($m4roll_positions as $m4roll_position) 
            {
                set_time_limit($max_while_cycle_exec_time_seconds);
                $percent = round(($i/$m4roll_positions_cnt)*100, 0, PHP_ROUND_HALF_DOWN);            
                $this->sendUpdateEvent($percent);            

                foreach ($m4roll_position->m4employee_rows as $m4employee_row)
                {
                    $txt_content .= '6' //1 - vrsta obrasca
                            . $_year //4 - godina
                            . str_pad($m4roll_position->roll_number,            4, '0', STR_PAD_LEFT) //4 - mf_rolna
                            . str_pad($m4roll_position->position_number,        4, '0', STR_PAD_LEFT) //4 - mf_pozicija
                            . $_pib //9 - PIB
                            . '6198082869' //10 - registarski broj
                            . str_pad($_work_code,                              6, '0', STR_PAD_LEFT) //6 - delatnost - 
                            . str_pad($m4employee_row->jmbg,                    13, '0', STR_PAD_LEFT) //13 - jmbg
                            . str_pad($m4employee_row->work_exp_months,         2, '0',  STR_PAD_LEFT) //2 - meseci staza
                            . str_pad($m4employee_row->work_exp_days,           2, '0',  STR_PAD_LEFT) //2 - dani staza
                            . '0000' //4 - casovi rada
                            . str_pad($m4employee_row->bruto*100,               10, '0', STR_PAD_LEFT) //10 - bruto zarada
                            . str_pad($m4employee_row->pio_doprinos*100,        10, '0', STR_PAD_LEFT) //10 - PIO doprinos
                            . str_pad($m4employee_row->zdrav_bruto*100,         10, '0', STR_PAD_LEFT) //10 - naknada zdravstveno
                            . str_pad($m4employee_row->zdrav_pio_doprinos*100,  10, '0', STR_PAD_LEFT) //10 - PIO doprinos na zdravstveno
                            . '00' //2 - meseci staza beneficirano
                            . '00' //2 - dani staza
                            . '0000' //4 - sifra beneficiranog staza
                            . '0000000000' //10 - PIO doprinos - beneficxirano
                            . $m4employee_row->employee_name // ime
                            . "\r\n";
                }

                $i++;            
            }
            $temporary_file = new TemporaryFile($txt_content, null, 'txt');
            $document_type_extension = $document_type;
        }
        else
        {
            throw new SIMAException('document_type '.$document_type.' not supported');
        }
        
        $this->sendEndEvent([
            'filename' => $temporary_file->getFileName(),
            'downloadname' => Yii::t('PaychecksModule.M4MFRoll', 'M4PdfForYear', [
                '{year}' => $year_model->year
            ]) . '.'.$document_type_extension
        ]);
    }
    
//    public function actionRecalcPeriod()
//    {
//        Yii::app()->setUpdateModelViews(false);
//        
//        set_time_limit(10);
//                
//        $data = $this->setEventHeader(); 
//        
//        $eventSourceStepsSum = 0.0;
//        Yii::app()->controller->sendUpdateEvent($eventSourceStepsSum);
//        
//        $paycheckPeriodId = $this->filter_input($data, 'paycheckPeriodId');
//        $withAdditionals = filter_var($data['withAdditionals'], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
//        $forCompareWithoutSave = filter_var($data['forCompareWithoutSave'], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
//        
//        $paycheckPeriod = ModelController::GetModel('PaycheckPeriod', $paycheckPeriodId);
//        
//        $paycheckPeriod->last_calculation_start_time = SIMAHtml::UserToDbDate(date('Y-m-d H:i:s'), true);
//        $paycheckPeriod->save();
//        
//        $employees = PaycheckPeriodComponent::EmployeesActiveInPeriod($paycheckPeriod);
//                
//        $employeesCount = count($employees);
//        if($employeesCount < 1)
//        {
//            $employeesCount = 1;
//        }
//        
//        \SIMAMisc::increaseMemoryLimit(memory_get_usage(true) + ($employeesCount*400*1024)); /// 400KB po zaposlenom
//        
//        $error_messages = [];
//        $raise_note_messages = [];
//                
//        $paychecks_for_compare = [];
//        $recalculated_paycheck_ids = [];
//                                
//        $eventSourceSteps = (88/$employeesCount);
//
//        $lastUsedOrderNumber = new SIMALocalCounter();
//        $max_execution_time = ini_get('max_execution_time');
//        foreach ($employees as $employee)
//        {
//            set_time_limit(9);
//            
////            $employee_error_messages = $this->getErrorBeforePaycheckForEmployee($employee, $paycheckPeriod);
//            $employee_error_messages = $paycheckPeriod->getErrorBeforePaycheckForEmployee($employee);
//            if(isset($employee_error_messages) && count($employee_error_messages)>0)
//            {
//                $raise_note_messages = array_merge($raise_note_messages, $employee_error_messages);
//                continue;
//            }
//
//            $eventSourceStepsSum += $eventSourceSteps;
//            Yii::app()->controller->sendUpdateEvent($eventSourceStepsSum);
//            
//            try
//            {
////                $employeePaycheck = $this->getPaycheckForEmployeeInPeriod(
////                        $employee, 
////                        $paycheckPeriod
////                );
//                $employeePaycheck = $paycheckPeriod->getPaycheckForEmployeeInPeriod($employee);
//
//                $employeePaycheck->recalculate($lastUsedOrderNumber, $withAdditionals);
//
//                $recalculated_paycheck_ids[$employeePaycheck->id] = $employeePaycheck->id;
//
//                if($forCompareWithoutSave === true)
//                {
//                    $paychecks_for_compare[] = $employeePaycheck;
//                }
//                else
//                {
//                    $employeePaycheck->save();
//                    if (isset($employeePaycheck->file))
//                    {
//                        $employeePaycheck->file->appendShellVersion();
//                    }
//                    unset($employeePaycheck);
//                }                
//            }
//            catch (SIMAWarnPaycheckCalculationFailed $warn)
//            {
//                $raise_note_messages[] = $warn->getMessage();
//            }
//            catch (Exception $ex) 
//            {
//                $error_messages[] = $ex->getMessage();
//            }
//        }
//        set_time_limit($max_execution_time);
//        
//        if(count($error_messages) > 0)
//        {
//            $imploded_messages = implode('</br>', $error_messages);
//
//            $error_message = 'Doslo je do greske:</br>'.$imploded_messages;
//
//            $this->sendFailureEvent(['message' => $error_message]);
//        }
//        
//        $paychecksForDevalidationCriteria = new SIMADbCriteria([
//            'Model' => Paycheck::model(),
//            'model_filter' => [
//                'paycheck_period' => [
//                    'ids' => $paycheckPeriod->id
//                ]
//            ]
//        ]);
//        $paychecksForDevalidation = Paycheck::model()->findAll($paychecksForDevalidationCriteria);
//        $paychecksForDevalidationCount = count($paychecksForDevalidation);
//        if($paychecksForDevalidationCount<1)
//        {
//            $paychecksForDevalidationCount = 1;
//        }
//        $eventSourceSteps = (2/$paychecksForDevalidationCount);
//        foreach($paychecksForDevalidation as $paycheck)
//        {
//            if(!isset($recalculated_paycheck_ids[$paycheck->id]))
//            {
//                if($forCompareWithoutSave === true)
//                {
//                    $paychecks_for_compare[] = $paycheck;
//                }
//                else
//                {
//                    $paycheck->delete();
//                }
//            }
//            
//            $eventSourceStepsSum += $eventSourceSteps;
//            Yii::app()->controller->sendUpdateEvent($eventSourceStepsSum);
//        }
//        
//        Yii::app()->controller->sendUpdateEvent(100);
//
//        $end_event_params = [];
//        if($forCompareWithoutSave === true)
//        {
//            $max_execution_time = ini_get('max_execution_time');
//            set_time_limit(500);
//            
//            $compare_html = $this->renderPartial('comparison_all', [
//                'paychecks_for_compare' => $paychecks_for_compare
//            ], true, false);
//            $end_event_params['compare_html'] = $compare_html;
//            set_time_limit($max_execution_time);
//        }
//        
//        $paycheckPeriod->last_calculation_end_time = SIMAHtml::UserToDbDate(date('Y-m-d H:i:s'), true);
//        $paycheckPeriod->save();
//        
//        if(!empty($raise_note_messages))
//        {
//            $raise_note_messages_imploded = implode('</br>', $raise_note_messages);
//
//            $raise_note_message = Yii::t('PaychecksModule.PaycheckPeriod', 'CouldNotRecalculatePaychecksForEmployees', [
//                '{error_messages}' => $raise_note_messages_imploded
//            ]);
//
//            $this->raiseNote($raise_note_message);
//        }
//                
//        $this->sendEndEvent($end_event_params);
//    }
    public function actionRecalcPeriod()
    {                
        $data = $this->setEventHeader(); 

        Yii::app()->setUpdateModelViews(false);
        
        set_time_limit(10);
                
        $paycheckPeriodId = $this->filter_input($data, 'paycheckPeriodId');
        $withAdditionals = filter_var($data['withAdditionals'], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
        $forCompareWithoutSave = filter_var($data['forCompareWithoutSave'], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
        $forCompareWithoutSave_detailed = false;
        if(isset($data['forCompareWithoutSave_detailed']))
        {
            $forCompareWithoutSave_detailed = filter_var($data['forCompareWithoutSave_detailed'], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
        }
        
        $paycheckPeriod = ModelController::GetModel('PaycheckPeriod', $paycheckPeriodId);
                
        try
        {
            /// za debug-ovanje prilikom re-obracuna
            if($forCompareWithoutSave_detailed === true)
            {
                Yii::app()->params['RecalcPeriodsCommand_keep_some_old_paycheck_data'] = true;
            }
            
            $paychecks_for_compare = $paycheckPeriod->recalculate($withAdditionals, $forCompareWithoutSave, function($percent){
                Yii::app()->controller->sendUpdateEvent($percent);
            });
            
            $end_event_params = [];
            if($forCompareWithoutSave === true)
            {
                $max_execution_time = ini_get('max_execution_time');
                set_time_limit(500);
                
                $compare_html = '';
                
                if($forCompareWithoutSave_detailed === true)
                {
                    $paychecks_for_compare_parsed = PaycheckPeriodComponent::GetPaychecksWhithComparisonDiff($paychecks_for_compare, 2);
                    $compare_html = Yii::app()->controller->renderPartial('comparison_all_detailed', [
                        'paychecks_for_compare_parsed' => $paychecks_for_compare_parsed
                    ], true, false);
                }
                else
                {
                    $compare_html = Yii::app()->controller->renderPartial('comparison_all', [
                        'paychecks_for_compare' => $paychecks_for_compare
                    ], true, false);
                }
                $end_event_params['compare_html'] = $compare_html;
                set_time_limit($max_execution_time);
            }
        }
        catch(SIMAWarnPaycheckPeriodCalculationNote $e)
        {
            $this->raiseNote($e->getMessage());
            $end_event_params = [];
        }
        catch(SIMAExceptionPaycheckPeriodCalculation $e)
        {
            $this->sendFailureEvent(['message' => $e->getMessage()]);
        }
        
        $this->sendEndEvent($end_event_params);
    }
    
    public function actionRecalcPaycheck($paycheckId, $withAdditionals, $forCompareWithoutSave, $forCompareWithoutSave_detailed=null)
    {
        $paycheck = ModelController::GetModel('Paycheck', $paycheckId);
        $withAdditionals_filtered = filter_var($withAdditionals, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
        $forCompareWithoutSave_filtered = filter_var($forCompareWithoutSave, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
        $forCompareWithoutSave_detailed = filter_var($forCompareWithoutSave_detailed, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
        
        $respond_array = $this->recalcSinglePaycheck($paycheck, $withAdditionals_filtered, $forCompareWithoutSave_filtered, $forCompareWithoutSave_detailed);
                
        $this->respondOK($respond_array);
    }
    
    public function actionRecalcPaycheckForEmployee($employee_id, $paycheck_period_id)
    {
        $employee = ModelController::GetModel('Employee', $employee_id);
        $paycheckPeriod = ModelController::GetModel('PaycheckPeriod', $paycheck_period_id);
        
//        $paycheck = $this->getPaycheckForEmployeeInPeriod(
//                $employee, 
//                $paycheckPeriod
//        );
        $paycheck = $paycheckPeriod->getPaycheckForEmployeeInPeriod($employee);
        
        $this->recalcSinglePaycheck($paycheck, true, false);
        
        $this->respondOK();
    }
    
    private function recalcSinglePaycheck(Paycheck $paycheck, $withAdditionals, $forCompareWithoutSave, $forCompareWithoutSave_detailed=false)
    {        
        if(!$paycheck->employee->isActiveAt($paycheck->paycheck_period->month, true))
        {
            $error_message = Yii::t('PaychecksModule.PaycheckPeriod', 'EmployeeNotActiveAt', [
                '{emp}' => $paycheck->employee_display_name,
                '{period}' => $paycheck->paycheck_period->DisplayName
            ]);
            throw new SIMAWarnException($error_message);
        }
        
//        $employee_error_messages = $this->getErrorBeforePaycheckForEmployee($paycheck->employee, $paycheck->paycheck_period);
        $employee_error_messages = $paycheck->paycheck_period->getErrorBeforePaycheckForEmployee($paycheck->employee);
        if(isset($employee_error_messages) && count($employee_error_messages)>0)
        {
            $imploded_messages = implode('</br>', $employee_error_messages);
            $error_message = 'Doslo je do greske:</br>'.$imploded_messages;
            
            throw new SIMAWarnException($error_message);
        }
        
        /// za debug-ovanje prilikom re-obracuna
        if($forCompareWithoutSave_detailed === true)
        {
            Yii::app()->params['RecalcPeriodsCommand_keep_some_old_paycheck_data'] = true;
        }
        
        $lastUsedOrderNumber = new SIMALocalCounter();
        $paycheck->recalculate($lastUsedOrderNumber, $withAdditionals);
        
        $respond_array = [];
        if($forCompareWithoutSave === true)
        {
            $compare_html = '';
            if($forCompareWithoutSave_detailed === true)
            {
                $paychecks_for_compare_parsed = PaycheckPeriodComponent::GetPaychecksWhithComparisonDiff([$paycheck], 2);
                $compare_html = Yii::app()->controller->renderPartial('comparison_all_detailed', [
                    'paychecks_for_compare_parsed' => $paychecks_for_compare_parsed
                ], true, false);
            }
            else
            {
                $compare_html = $this->renderPartial('comparison', [
                    'paycheck' => $paycheck
                ], true, true);
            }
            $respond_array['compare_html'] = $compare_html;
        }
        else
        {
            $paycheck->save();
            if (isset($paycheck->file))
            {
                $paycheck->file->appendShellVersion();
            }
        }
        
        return $respond_array;
    }
    
    public function actionRegeneratePPPPD()
    {
        $data = $this->setEventHeader();
        
        Yii::app()->controller->sendUpdateEvent(1);
        
        $paycheckPeriodId = $this->filter_input($data, 'paycheckPeriodId');
        $regenerate_completely = filter_var($this->filter_input($data, 'regenerate_completely', false), FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
        $paycheckPeriod = PaycheckPeriod::model()->findByPkWithCheck($paycheckPeriodId);
        
        $ppppds = $paycheckPeriod->ppppds;
        
        if(empty($ppppds))
        {
            $file = new File();
            $file->save();

            $ppppd = new PPPPD();
            $ppppd->creation_type = PPPPD::$CREATE_TYPE_SIMA_GENERATE;
            $ppppd->id=$file->id;
            $ppppd->year_id = $paycheckPeriod->year->id;
            $ppppd->save();
            
            $paycheckPeriodToPPPPD = new PaycheckPeriodToPPPPD();
            $paycheckPeriodToPPPPD->paycheck_period_id = $paycheckPeriod->id;
            $paycheckPeriodToPPPPD->ppppd_id = $ppppd->id;
            $paycheckPeriodToPPPPD->save();
            
            $ppppds = [$ppppd];
        }
        else if($regenerate_completely === true)
        {
            $sima_generate_ppppd = null;
            foreach($ppppds as $ppppd)
            {
                if($ppppd->creation_type === PPPPD::$CREATE_TYPE_SIMA_GENERATE)
                {
                    $sima_generate_ppppd = $ppppd;
                    break;
                }
            }
            if(empty($sima_generate_ppppd))
            {
                throw new Exception('no simagenerate ppppd');
            }
            
            $paychecks = $paycheckPeriod->valid_paychecks;
            foreach($paychecks as $paycheck)
            {
                $paycheck_xmls = $paycheck->paycheck_xmls;
                foreach($paycheck_xmls as $paycheck_xml)
                {
                    $paycheck_xml->ppppd_id = $sima_generate_ppppd->id;
                    $paycheck_xml->save();
                }
            }
        }
        
        Yii::app()->controller->sendUpdateEvent(50);
                  
        foreach($ppppds as $ppppd)
        {
            $ppppd->populateFromPaycheckPeriod($paycheckPeriod);
        }
        
        Yii::app()->controller->sendUpdateEvent(99);
        
        $this->sendEndEvent([
            'message' => Yii::t('BaseModule.Common', 'Success')
        ]);
    }
    
    public function actionParseUploadedPaycheckXML()
    {
        $paycheckPeriodId = $this->filter_post_input('paycheckPeriodId');
        $upload_session_key = $this->filter_post_input('upload_session_key');
        
        $paycheckPeriod = ModelController::GetModel('PaycheckPeriod', $paycheckPeriodId);
        
        $callForNumber = $this->getCallForNumberFromUploadedXML(Yii::app()->sima_session[$upload_session_key]['filename']);
        
        if(!isset($callForNumber))
        {
            throw new SIMAWarnException(Yii::t('PaychecksModule.PaycheckPeriod', 'ImportXmlProblem'));
        }
        
        $paycheckPeriod->call_for_number_partner = $callForNumber;
        $paycheckPeriod->save();
        
        $this->respondOK();
    }
    
    public function actionCreatePayments()
    {      
        $paycheckPeriod = ModelController::GetModel(
                'PaycheckPeriod', 
                $this->filter_post_input('paycheckPeriodId')
        );
        
        $this->createPaymentsForPaycheckPeriod($paycheckPeriod);
        
        $this->respondOK();
    }
    
    public function actionCreateOZFiles()
    {
        $data = $this->setEventHeader();
        
        $eventSourceStepsSum = 1;
        Yii::app()->controller->sendUpdateEvent($eventSourceStepsSum);
        
        $paycheckPeriodId = $data['paycheckPeriodId'];
        $paycheckPeriod = PaycheckPeriod::model()->findByPk($paycheckPeriodId);
        
        PaycheckPeriodComponent::CreatePdfsForPaycheckPeriod($paycheckPeriod);
        
        $this->sendEndEvent([
            'message' => Yii::t('BaseModule.Common', 'Success')
        ]);
    }
    
    public function actionAllPaychecks($model_id)
    {        
        $paycheckPeriod = PaycheckPeriod::model()->findByPkWithCheck($model_id);

        $html = $this->renderPartial('all_paychecks', array(
            'model' => $paycheckPeriod
        ), true, true);
        
        $this->respondOK(array('html'=>$html));
    }
    
    public function actionPaycheckSuspensions($paycheck_period_id)
    {
        $paycheck_period = PaycheckPeriod::model()->findByPkWithCheck($paycheck_period_id);
        $html = $this->renderPartial('paycheck_suspensions', [
            'paycheck_period' => $paycheck_period
        ], true, true);
        
        $this->respondOK([
            'html' => $html
        ]);
    }
    
    public function actionPaycheckDeductions($paycheck_period_id)
    {
        $paycheck_period = PaycheckPeriod::model()->findByPkWithCheck($paycheck_period_id);
        $html = $this->renderPartial('paycheck_deductions', [
            'paycheck_period' => $paycheck_period
        ], true, true);
        
        $this->respondOK([
            'html' => $html
        ]);
    }
    
    public function actionPaycheckBonuses($paycheck_period_id)
    {
        $paycheck_period = PaycheckPeriod::model()->findByPkWithCheck($paycheck_period_id);
        $html = $this->renderPartial('paycheck_bonuses', [
            'paycheck_period' => $paycheck_period
        ], true, true);
        
        $this->respondOK([
            'html' => $html
        ]);
    }
    
    public function actionFillGroupBonus()
    {
        $data = $this->setEventHeader();
        
        $group_bonus_id = $this->filter_input($data, 'group_bonus_id');
        $group_bonus = PaycheckGroupBonus::model()->findByPkWithCheck($group_bonus_id);
        $group_bonus->attachBehavior('fill', 'PaycheckGroupBonusFill');
        $group_bonus->fillBonuses();
        
        $this->sendEndEvent([
            'message' => Yii::t('BaseModule.Common', 'Success')
        ]);
    }
    
    public function actionFillAllGroupBonus()
    {
        $data = $this->setEventHeader();
        
        $group_bonus_id = $this->filter_input($data, 'group_bonus_id');
        $group_bonus = PaycheckGroupBonus::model()->findByPkWithCheck($group_bonus_id);
        $group_bonus->attachBehavior('fill', 'PaycheckGroupBonusFill');
        $group_bonus->fillBonuses(true);
        
        $this->sendEndEvent([
            'message' => Yii::t('BaseModule.Common', 'Success')
        ]);
    }
    
    public function actionFillCustomGroupBonus()
    {
        $data = $this->setEventHeader();
        
        $group_bonus_id = $this->filter_input($data, 'group_bonus_id');
        $employee_ids = $this->filter_input($data, 'employee_ids');
        $group_bonus = PaycheckGroupBonus::model()->findByPkWithCheck($group_bonus_id);
        $group_bonus->attachBehavior('fill', 'PaycheckGroupBonusFill');
        $group_bonus->fillCustomBonuses($employee_ids);
        
        $this->sendEndEvent([
            'message' => Yii::t('BaseModule.Common', 'Success')
        ]);
    }
    
    public function actionCalcGroupBonus()
    {
        $data = $this->setEventHeader();
        
        $group_bonus_id = $this->filter_input($data, 'group_bonus_id');
        $group_bonus = PaycheckGroupBonus::model()->findByPkWithCheck($group_bonus_id);
        $group_bonus->attachBehavior('fill', 'PaycheckGroupBonusFill');
        $group_bonus->calcBonuses();
        
        $this->sendEndEvent([
            'message' => Yii::t('BaseModule.Common', 'Success')
        ]);
    }
    
    public function actionUnifiedPaychecksBanks($model_id)
    {
        $paycheckPeriod = PaycheckPeriod::model()->findByPkWithCheck($model_id);
        
        $bank_ids = [];
        foreach($paycheckPeriod->paychecks as $paycheck)
        {
            if(
                $paycheck->isPayoutByBankAccount()
                &&
                !in_array($paycheck->bank_account->bank_id, $bank_ids)
            )
            {
                $bank_ids[] = $paycheck->bank_account->bank_id;
            }
        }

        if(empty($bank_ids))
        {
            $bank_ids = [-1];
        }

        $html = $this->renderPartial('paychecks_banks', [
            'bank_ids' => $bank_ids,
            'paycheckPeriodId' => $paycheckPeriod->id,
            'paychecksId' => SIMAHtml::uniqid()
        ], true, true);
        
        $this->respondOK(array('html'=>$html));
    }
    
    public function actionPaychecksForBank()
    {        
        $bank = ModelController::GetModel('Bank', $this->filter_post_input('bankId'));
        $paycheckPeriodId = $this->filter_post_input('paycheckPeriodId');
        
        $html = $this->renderPartial('paychecks_for_bank', array(
            'bank' => $bank,
            'paycheckPeriodId' => $paycheckPeriodId
        ), true, true);
        
        $this->respondOK(array('html'=>$html));
    }
    
    public function actionPaychecksByCache($model_id)
    {
        $paycheckPeriod = PaycheckPeriod::model()->findByPkWithCheck($model_id);
        
        $html = $this->renderPartial('paychecks_by_cache', array(
            'paycheckPeriod' => $paycheckPeriod,
        ), true, true);
        
        $this->respondOK(array('html'=>$html));
    }
    
    private function getCallForNumberFromUploadedXML($filename)
    {
        $call_for_number_from_xml = null;
        
        if(!TempDir::verifyExistance($filename))
        {
            throw new Exception('uploaded xml could not be found');
        }
        
        $real_path = TempDir::getFullPath($filename);
        
        $fileContent = trim(file_get_contents($real_path));
        
        $xmlVals = array();
        $p = xml_parser_create();
        xml_parse_into_struct($p, $fileContent, $xmlVals);
        xml_parser_free($p);
        
        foreach($xmlVals as $xmlVal)
        {
            if($xmlVal['tag'] === 'PID:POZIVNABROJODOBRENJA')
            {
                $call_for_number_from_xml = $xmlVal['value'];
            }
        }
        
        return $call_for_number_from_xml;
    }
    
    private function createPaymentsForPaycheckPeriod(PaycheckPeriod $paycheckPeriod)
    {        
        $this->generateEmployeePaychecksForPaycheckPeriod($paycheckPeriod);
        
        $this->generateCompanyPaymentForPaycheckPeriod($paycheckPeriod);
    }
    
    private function generateEmployeePaychecksForPaycheckPeriod(PaycheckPeriod $paycheckPeriod)
    {
        $unified_paychecks = [];
        
        $paychecks = $paycheckPeriod->valid_paychecks;
        $unifiedPaycheckModels = UnifiedPaycheck::model()->active()->findAll();
        $unifiedPaychecks = [];
        foreach($unifiedPaycheckModels as $unifiedPaycheckModel)
        {
            $unifiedPaychecks[$unifiedPaycheckModel->bank->id] = $unifiedPaycheckModel->id;
        }
                
        foreach($paychecks as $paycheck)
        {            
            if(!$paycheck->employee->isPayoutByCache())
            {
                $bank_id = $paycheck->employee->person->MainBankAccount->bank->id;
                
                if(isset($unifiedPaychecks[$bank_id]))
                {
                    if(!isset($unified_paychecks[$unifiedPaychecks[$bank_id]]))
                    {
                        $unified_paychecks[$unifiedPaychecks[$bank_id]] = array();
                    }

                    $unified_paychecks[$unifiedPaychecks[$bank_id]][] = $paycheck;
                }
                else
                {
                    $this->createPaymentForEmployeePaycheck($paycheck);
                }
            }
        }
                        
        if(count($unified_paychecks) > 0)
        {
            UnifiedPaycheckComponent::generatePayments($paycheckPeriod, $unified_paychecks);
        }
    }
    
    private function generateCompanyPaymentForPaycheckPeriod(PaycheckPeriod $paycheckPeriod)
    {
        $paychecks = $paycheckPeriod->valid_paychecks;
        $companyPaymentSum = 0;
        
        foreach($paychecks as $paycheck)
        {
            $companyPaymentSum += ($paycheck->amount_total - $paycheck->amount_neto);
        }
        
        $payment_date = $paycheckPeriod->payment_date;
        $partner_id = Yii::app()->configManager->get('accounting.paychecks.state_partner');
        $invoice = false;
        $payment_type_id = Yii::app()->configManager->get('accounting.paycheck_payment_type');
        $call_for_number_modul_partner = Yii::app()->configManager->get('accounting.paychecks.call_for_number_modul_default');
        $state_paycheck_payment_code = Yii::app()->configManager->get('accounting.paychecks.state_paycheck_payment_code');
        $call_for_number_partner = $paycheckPeriod->call_for_number_partner;
        
        $payment = null;
        $is_new_payment = false;
        
        if(empty($paycheckPeriod->payment))
        {
            $payment = new Payment();
            $is_new_payment = true;
        }
        else
        {
            $payment = $paycheckPeriod->payment;
        }
            
        $payment->payment_date = $payment_date;
        $payment->partner_id = $partner_id;
        $payment->account_id = null;
        $payment->amount = $companyPaymentSum;
        $payment->invoice = $invoice;
        $payment->payment_code_id = $state_paycheck_payment_code; //254
        $payment->payment_type_id = $payment_type_id;
        $payment->call_for_number_modul_partner = $call_for_number_modul_partner;
        $payment->call_for_number_partner = $call_for_number_partner;
        $payment->save();
        
        if($is_new_payment)
        {
            $paycheckPeriod->payment_id = $payment->id;
            $paycheckPeriod->save();
        }
    }
    
    private function createPaymentForEmployeePaycheck(Paycheck $paycheck)
    {
        $payment = $paycheck->payment();
                
        $paycheck_had_payment = false;
        if(!is_null($payment))
        {
            $paycheck_had_payment = true;
        }
                
        if($paycheck_had_payment === false)
        {
            $payment = new Payment();
        }
        else
        {
            if(!is_null($payment->bank_statement))
            {
                throw new SIMAWarnException(Yii::t('PaychecksModule.PaycheckPeriod', 'PaymentHaveBankStatement'));
            }
        }
        
        /**
         * MilosJ: zakrpa za platu koja ima null za vrednosti
         * to se desava u PaycheckCalculation::recalculatePaycheckNEW
         *      if($worked_hours < 0)
         * a niko ne hvata taj return
         */
        $amount_neto_for_payout = $paycheck->amount_neto_for_payout;
        if(is_null($amount_neto_for_payout))
        {
            $amount_neto_for_payout = 0;
        }
        
        $payment->payment_date = $paycheck->paycheck_period->payment_date;
        $payment->partner_id = $paycheck->employee_id;
        $payment->amount = $amount_neto_for_payout;
        $payment->invoice = false;
        $payment->payment_code_id = Yii::app()->configManager->get('accounting.paychecks.employee_paycheck_payment_code'); // ovaj treba biti 240 (default za isplacivanje radniku), treba dodati u conf za 241 (neoporeziva zarada) i 254 (porez po zaradi)
        $payment->payment_type_id = Yii::app()->configManager->get('accounting.paycheck_payment_type');
        $payment->call_for_number_modul_partner = Yii::app()->configManager->get('accounting.paychecks.call_for_number_modul_default');
        $payment->call_for_number_partner = $paycheck->paycheck_period->call_for_number_partner;
        
        if(!empty($paycheck->employee->person->partner->main_bank_account_id))
        {
            $payment->account_id = $paycheck->employee->person->partner->main_bank_account_id;
        }
        
        $payment->save();
        
        if($paycheck_had_payment === false)
        {
            $paycheck->payment_id = $payment->id;
            $paycheck->save();
        }
        
//        if(is_null($payment))
//        {
//            $payment = new Payment();
//            $payment->payment_date = $paycheck->paycheck_period->payment_date;
//            $payment->partner_id = $paycheck->employee_id;
//            $payment->amount = $paycheck->amount_neto_for_payout;
//            $payment->invoice = false;
//            $payment->payment_code_id = Yii::app()->configManager->get('accounting.paychecks.employee_paycheck_payment_code'); // ovaj treba biti 240 (default za isplacivanje radniku), treba dodati u conf za 241 (neoporeziva zarada) i 254 (porez po zaradi)
//            $payment->payment_type_id = Yii::app()->configManager->get('accounting.paycheck_payment_type');
//            $payment->call_for_number_modul_partner = Yii::app()->configManager->get('accounting.paychecks.call_for_number_modul_default');
//            $payment->call_for_number_partner = $paycheck->paycheck_period->call_for_number_partner;
//            $payment->save();
//            
//            $paycheck->payment_id = $payment->id;
//            $paycheck->save();
//        }
//        else
//        {
//            if(is_null($payment->bank_statement))
//            {
//                $payment->payment_date = $paycheck->paycheck_period->payment_date;
//                $payment->partner_id = $paycheck->employee_id;
//                $payment->amount = $paycheck->amount_neto;
//                $payment->invoice = false;
//                $payment->payment_code_id = Yii::app()->configManager->get('accounting.paychecks.employee_paycheck_payment_code'); // ovaj treba biti 240 (default za isplacivanje radniku), treba dodati u conf za 241 (neoporeziva zarada) i 254 (porez po zaradi)
//                $payment->payment_type_id = Yii::app()->configManager->get('accounting.paycheck_payment_type');
//                $payment->call_for_number_modul_partner = Yii::app()->configManager->get('accounting.paychecks.call_for_number_modul_default');
//                $payment->call_for_number_partner = $paycheck->paycheck_period->call_for_number_partner;
//
//                $payment->save();
//            }
//            else
//            {
//                throw new SIMAWarnException(Yii::t('PaychecksModule.PaycheckPeriod', 'PaymentHaveBankStatement'));
//            }
//        }
    }
    
//    private function getPaycheckForEmployeeInPeriod(Employee $employee, PaycheckPeriod $paycheckPeriod)
//    {
//        $paycheckCriteria = new SIMADbCriteria([
//            'Model' => 'Paycheck',
//            'model_filter' => [
//                'paycheck_period' => array(
//                    'ids' => $paycheckPeriod->id
//                ),
//                'employee' => array(
//                    'ids' => $employee->id
//                )
//            ]
//        ]);
//        
//        $paycheck = Paycheck::model()->with('employee.work_contracts', 'additionals')->find($paycheckCriteria);
//        if(!isset($paycheck))
//        {
//            $paycheck = new Paycheck();
//            $paycheck->employee_id = $employee->id;
//            $paycheck->paycheck_period_id = $paycheckPeriod->id;
//            $paycheck->worked_hours = 0;
//            $paycheck->worked_hours_value = 0;
//            $paycheck->past_exp_value = 0;
//            $paycheck->save();
//            $paycheck->refresh();
//        }
//        
//        unset($paycheckCriteria);
//                
//        return $paycheck;
//    }
    
    public function actionPreRecalcPeriodValidation()
    {
        $paycheckPeriodId = $this->filter_post_input('paycheckPeriodId');
        
        $paycheckPeriod = PaycheckPeriod::model()->findByPkWithCheck($paycheckPeriodId);
        
        $error_message = $this->preRecalcPeriodValidation_errors($paycheckPeriod);
        
        $status = 'OK';
        
        if(count($error_message) > 0)
        {
            $status = implode('</br>', $error_message);
            $status .= '</br></br>'.Yii::t('BaseModule.Common', 'AreYouSureYouWantToContinue').'?';
        }
        
        $this->respondOK(['status' => $status]);
    }
    
    private function preRecalcPeriodValidation_errors(PaycheckPeriod $paycheckPeriod)
    {
        $error_messages = [];
        
        $wcCriteria = new SIMADbCriteria([
            'Model' => 'WorkContract',
            'model_filter' => [
                'for_payout' => true,
                'active_at' => $paycheckPeriod->month
            ]
        ]);
        $employees_work_contracts = WorkContract::model()->with('employee')->findAll($wcCriteria);
        
        $employees = [];
        
        foreach($employees_work_contracts as $wc)
        {
            if(!isset($employees[$wc->employee->id]))
            {
                $employees[$wc->employee->id] = [
                    'model' => $wc->employee,
                    'work_contracts' => []
                ];
                
                $error_messages = array_merge(
                    $error_messages,
//                    $this->getErrorBeforePaycheckForEmployee($wc->employee, $paycheckPeriod)
                    $paycheckPeriod->getErrorBeforePaycheckForEmployee($wc->employee)
                );
            }
            
            $employees[$wc->employee->id]['work_contracts'][] = $wc;
        }
        
        $error_messages = array_merge(
                $error_messages,
                
                /// da li ima zaposlenih sa nepotpisanim ugovorima u periodu
                $this->validateEmployeesWorkContractsSigned($employees)
        );
        
        if ($paycheckPeriod->isFinal)
        {
            $error_messages = array_merge(
                $error_messages,
                /// da li su za sve zaposlene potvrdjeni dani
                $this->validateEmployeesConfirmedDays($paycheckPeriod->month->lastDay(), $employees)
            );
        }
        
        return $error_messages;
    }
    
    private function validateEmployeesWorkContractsSigned(array $employees)
    {
        $result = [];
        
        foreach($employees as $employee)
        {
            foreach($employee['work_contracts'] as $work_contract)
            {
                if($work_contract->signed === false)
                {
                    $result[] = Yii::t('PaychecksModule.PaycheckPeriod', 'EmployeeUnsignedWorkContract', [
                        '{emp}' => $work_contract->employee->display_name
                    ]);
                    break;
                }
            }
        }
        
        return $result;
    }
    
    private function validateEmployeesConfirmedDays(Day $lastPeriodDay, array $employees)
    {
        $to_validate_employees_confirmed_days = Yii::app()->configManager->get('accounting.paychecks.to_validate_employees_confirmed_days');
        if($to_validate_employees_confirmed_days === false)
        {
            return [];
        }
        
        $error_messages = [];
        
        foreach($employees as $employee)
        {
            if($employee['model']->person->needToWriteReports())
            {
                $employee_last_boss_confirm_day = $employee['model']->user->last_boss_confirm_day;
                if(empty($employee_last_boss_confirm_day) || $employee_last_boss_confirm_day->compare($lastPeriodDay) < 0)
                {
                    $error_message = Yii::t('PaychecksModule.PaycheckPeriod', 'EmpDoesNotHaveAllConfirmedDaysExists');
                    $error_message .= $this->widget(SIMAButtonVue::class, [
                        'title' => Yii::t('BaseModule.Common', 'SeeMoreDetail'),
                        'onclick'=>['sima.dialog.openActionAsync', 'guitable', 'legal_26']
                    ], true);
                    
                    $error_messages[] = $error_message;
                    break;
                }
            }
        }
        
        return $error_messages;
    }
    
    public function actionOverwriteSickLeavesFromXML()
    {
        $paycheckPeriodId = $this->filter_post_input('paycheckPeriodId');
        $upload_session_key = $this->filter_post_input('upload_session_key');
        
        $paycheckPeriod = ModelController::GetModel('PaycheckPeriod', $paycheckPeriodId);
        
        $temp_file_name = SIMAUpload::ParseUploadKey($upload_session_key);
        $tempFile = TemporaryFile::GetByName($temp_file_name);
        
        $this->overwriteSickLeavesFromXML($paycheckPeriod, $tempFile);
        
        $this->respondOk();
    }
    
    private function overwriteSickLeavesFromXML(PaycheckPeriod $paycheckPeriod, TemporaryFile $tempFile)
    {
        $svps_to_search = $this->overwriteSickLeavesFromXML_getSVPsToSearch();
        
        $additionalss_by_jmbg = $this->overwriteSickLeavesFromXML_getAdditionalsByJMBG($paycheckPeriod);
        
        $xml=simplexml_load_file($tempFile->getFullPath());
                        
        $deklarisaniPrihodi = $xml->DeklarisaniPrihodi;
        
        foreach ($deklarisaniPrihodi->children() as $deklarisaniPrihod)
        {
            $svp = $deklarisaniPrihod->SVP->__toString();
            foreach($svps_to_search as $svp_to_search)
            {
                if(SIMAMisc::stringEndsWith($svp, $svp_to_search))
                {
                    $this->overwriteSickLeavesFromXML_checkDeklarisaniPrihod($deklarisaniPrihod, $additionalss_by_jmbg);
                }
            }
        }
    }
    
    private function overwriteSickLeavesFromXML_getSVPsToSearch()
    {
        $svps_to_search = [];
        $paycheck_additional_types = Yii::app()->params['paycheck_additional_types_formated'];
        foreach($paycheck_additional_types as $paycheck_additional_type)
        {
            if($paycheck_additional_type['separate_field_in_xml'] === true 
                    && isset($paycheck_additional_type['default_svp']))
            {
                $svps_to_search[] = (string)$paycheck_additional_type['default_svp'];
            }
        }
        
        return $svps_to_search;
    }
    
    private function overwriteSickLeavesFromXML_getAdditionalsByJMBG(PaycheckPeriod $paycheckPeriod)
    {
        $additionalss_by_jmbg = [];
        $paychecks = $paycheckPeriod->paychecks;
        foreach($paychecks as $paycheck)
        {
            $emp_jmbg = $paycheck->employee->person->JMBG;
            if(!empty($paycheck->additionals))
            {
                $additionalss_by_jmbg[$emp_jmbg] = $paycheck->additionals;
            }
        }
        return $additionalss_by_jmbg;
    }
    
    private function overwriteSickLeavesFromXML_checkDeklarisaniPrihod($deklarisaniPrihod, array $additionalss_by_jmbg)
    {
        $svp = $deklarisaniPrihod->SVP->__toString();
        $idp = $deklarisaniPrihod->IdentifikatorPrimaoca->__toString();
        if(!isset($additionalss_by_jmbg[$idp]))
        {
            throw new Exception(Yii::t('PaychecksModule.PaycheckPeriod', 'NoAdditionalsForEmp', [
                '{idp}' => $idp
            ]));
        }

        $additionals = $additionalss_by_jmbg[$idp];

        $found_by_svp = false;
        foreach($additionals as $additional)
        {
            if(!empty($additional->svp) && (string)$additional->svp === $svp)
            {
                $new_bruto = floatval($deklarisaniPrihod->Bruto->__toString());
                $osnovicaPorez = floatval($deklarisaniPrihod->OsnovicaPorez->__toString());
                $new_tax_release = $new_bruto - $osnovicaPorez;

                $additional->bruto = $new_bruto;
                $additional->tax_release = $new_tax_release;
                $additional->save();
                $found_by_svp = true;
            }
        }
        if($found_by_svp === false)
        {
            throw new Exception(Yii::t('PaychecksModule.PaycheckPeriod', 'NoAdditionalsWithSVPForEmp', [
                '{idp}' => $idp,
                '{svp}' => $svp
            ]));
        }
    }
    
    public function actionExportPaycheckForBank($paycheck_period_id, $bank_id, $type)
    {
        $paycheckPeriod = ModelController::GetModel('PaycheckPeriod', $paycheck_period_id);
        $bank = null;
        if(!empty($bank_id))
        {
            $bank = ModelController::GetModel('Bank', $bank_id);
        }
        
        $bank_export_temporary_file = $paycheckPeriod->generateBankExportFile($type, $bank);
                
        $this->respondOK([
            'tn' => $bank_export_temporary_file->getFileName(),
            'dn' => 'BankExport.'.$bank_export_temporary_file->getExtension()
        ]);
    }
    
    public function actionExportAllPaychecks()
    {
        $data = $this->setEventHeader();
                
        $paycheck_period_id = $this->filter_input($data, 'paycheck_period_id');
                
        $paycheckPeriod = PaycheckPeriod::model()->findByPkWithCheck($paycheck_period_id);
        
        $this->sendUpdateEvent(1);
        
        $paychecks_temporary_file = $paycheckPeriod->generateAllPaychecksExportFile();
        
        $this->sendEndEvent([
            'tn' => $paychecks_temporary_file->getFileName(),
            'dn' => 'Evidencija zarade zaposlenih '.$paycheckPeriod->DisplayName.'.'.$paychecks_temporary_file->getExtension()
        ]);
    }
    
    public function actionSendOZFiles()
    {
        $data = $this->setEventHeader();
        
        $paycheck_period_id = $this->filter_input($data, 'paycheck_period_id');
        $only_download_input = $this->filter_input($data, 'only_download');
        
        $paycheckPeriod = PaycheckPeriod::model()->findByPkWithCheck($paycheck_period_id);
        $only_download = filter_var($only_download_input, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
                
        $employees_documents = [];
        $unsent_emps = [];
        
        $eventSourceStepsSum = 0;
        $this->sendUpdateEvent($eventSourceStepsSum);
        
        $paychecks = $paycheckPeriod->valid_paychecks;
        $paychecks_count = count($paychecks);
        if($paychecks_count < 1)
        {
            $paychecks_count = 1;
        }
        $eventSourceSteps = (40/$paychecks_count);
        foreach($paychecks as $paycheck)
        {
            $eventSourceStepsSum += $eventSourceSteps;
            Yii::app()->controller->sendUpdateEvent($eventSourceStepsSum);
            
            $employee = $paycheck->employee;
            $reasons = [];
            if($paycheck->is_valid === false)
            {
                $reasons[] = 'Zarada nije validna';
            }
            if ($paycheck->confirmed === false)
            {
                $reasons[] = 'Zarada nije potvrdjena';
            }
            if (!isset($paycheck->file_id))
            {
                $reasons[] = 'Nije kreiran OZ listić';
            }

            if (!empty($reasons))
            {
                $unsent_emps[$employee->DisplayName] = $reasons;
            }
            else
            {
                $employees_documents[] = [
                    'employee' => $employee,
                    'documents' => [
                        $paycheck->file
                    ]
                ];
            }
        }
        
        $message = 'Nepoznata greska';
        $merged_documents_temp_file_name = null;
        
        if(empty($unsent_emps))
        {
            $message = 'Poslato za sve';
            
            $oz_email_title = $paycheckPeriod->getAttributeDisplay('oz_email_title');
            
            $merged_documents_temp_file_name = Employee::mergeEmployeesDocuments($employees_documents, $oz_email_title, !$only_download, function($percent){                        
                $this->sendUpdateEvent(40+$percent*(60/100));
            });
        }
        else
        {
            $unsent_emps_html = '<b>Nije poslato za:</b><br />';
            foreach ($unsent_emps as $key => $value)
            {
                $unsent_emps_html .= "<b>$key:</b> ".implode(', ', $value).'<br />';
            }
            $message = $unsent_emps_html;
        }
        
        Yii::app()->controller->sendUpdateEvent(99);
        
        $this->sendEndEvent([
            'message' => $message,
            'tn' => $merged_documents_temp_file_name,
            'dn' => 'OZ.zip'
        ]);
    }
}