<?php

class PPPPDPaychecksController extends SIMAController
{
    public function filters()
    {
        return [
            'ajaxOnly-GenerateInitialPPPPDsForPaycheckPeriod-CompareXmlWithPaychecks-ComapreXMLWithPPPPD'
        ] + parent::filters();
    }
    
    public function actionCompareXmlWithPaychecks()
    {
        $upload_key = $this->filter_post_input('upload_key');
        $paycheck_period_id = $this->filter_post_input('paycheck_period_id');
        
        $error_messages = $this->comparePaychecksWithXmlContent($upload_key, $paycheck_period_id);
        
        $this->respondOK(['error_messages' => $error_messages]);
    }
    
    public function actionComapreXMLWithPPPPD()
    {
        $upload_key = $this->filter_post_input('upload_key');
        $paycheck_period_id = $this->filter_post_input('paycheck_period_id');
        
        $error_messages = $this->comparePPPDWithXmlContent($upload_key, $paycheck_period_id);
        
        $this->respondOK(['error_messages' => $error_messages]);
    }
    
    public function actionBindPPPPDsToPaycheckPeriod()
    {        
        $paycheck_period_id = $this->filter_post_input('paycheck_period_id');
        $ppppd_ids = $this->filter_post_input('ppppd_ids');
                
        $paycheck_period = PaycheckPeriod::model()->findByPkWithCheck($paycheck_period_id);
        $ppppds = PPPPD::model()->findAllByPkWithCheck($ppppd_ids);
        
        foreach($ppppds as $ppppd)
        {
            $ppppdtpp = new PaycheckPeriodToPPPPD();
            $ppppdtpp->paycheck_period_id = $paycheck_period->id;
            $ppppdtpp->ppppd_id = $ppppd->id;
            $ppppdtpp->save();
        }
        
        $this->respondOK();
    }
    
    private function loadUploadedXml($upload_key)
    {
        $parsedKey = SIMAUpload::ParseUploadKey($upload_key);
        if($parsedKey === false)
        {
            throw new SIMAException('parse fail');
        }
        
        $temp_file_name = $parsedKey['temp_file_name'];
                
        $tempFile = TemporaryFile::GetByName($temp_file_name);
        $temp_file_path = $tempFile->getFullPath();
                
        $xml = simplexml_load_file($temp_file_path);
                
        return $xml;
    }
    
    private function loadPaychecks($paycheck_period_id)
    {
        $paychecksCriteria = new SIMADbCriteria([
            'Model' => 'Paycheck',
            'model_filter' => [
                'is_valid' => true,
                'paycheck_period' => [
                    'ids' => $paycheck_period_id
                ]
            ]
        ]);
        $paychecks = Paycheck::model()->findAll($paychecksCriteria);
        
        return $paychecks;
    }
    
    private function comparePaychecksWithXmlContent($upload_key, $paycheck_period_id)
    {
        $error_messages = [];
        
        $xml = $this->loadUploadedXml($upload_key);
        $paychecks = $this->loadPaychecks($paycheck_period_id);
        
        $namespaces = $xml->getNameSpaces(true);
        if(empty($namespaces))
        {
            $xml_DeklarisaniPrihodi_children = $xml->DeklarisaniPrihodi->children();
        }
        else
        {
            $namespace = key($namespaces);
            
            $xml_children = $xml->children($namespaces[$namespace]);
            $xml_DeklarisaniPrihodi_children = $xml_children->DeklarisaniPrihodi->children($namespaces[$namespace]);
        }
        
        foreach ($xml_DeklarisaniPrihodi_children as $child)
        {
            $have_xml_paycheck_in_db = false;
            
            $redniBroj = $child->RedniBroj->__toString();
            $vrstaIdentifikatoraPrimaoca = $child->VrstaIdentifikatoraPrimaoca->__toString();
            $identifikatorPrimaoca = $child->IdentifikatorPrimaoca->__toString();
            $Ime = $child->Ime->__toString();
            $Prezime = $child->Prezime->__toString();
            $SVP = $child->SVP->__toString();
            
            if($vrstaIdentifikatoraPrimaoca !== '1')
            {
                $error_messages[] = Yii::t('PaychecksModule.Paycheck', 'UnknownVrstaIdentifikatoraPrimaoca', ['{vrednost}' => $vrstaIdentifikatoraPrimaoca]);
                continue;
            }
            
            $paycheck_additional_types = Yii::app()->params['paycheck_additional_types'];
            $paycheck_additional_types_formated = [];
            foreach($paycheck_additional_types as $pat)
            {
                $paycheck_additional_types_formated[$pat['value']] = $pat;
            }
            
            foreach($paychecks as $db_paycheck)
            {
                if($identifikatorPrimaoca === $db_paycheck->employee->person->JMBG)
                {
//                    $oznakaPrebivalista = $child->OznakaPrebivalista->__toString();
//                    $SVP = $child->SVP->__toString();
//                    $BrojKalendarskihDana = $child->BrojKalendarskihDana->__toString();
//                    $BrojEfektivnihSati = $child->BrojEfektivnihSati->__toString();
//                    $MesecniFondSati = $child->MesecniFondSati->__toString();
//                    $bruto = (string)$child->Bruto->__toString();
                    $bruto = number_format((float)$child->Bruto->__toString(), 2, '.', '');
//                    $db_paycheck_bruto = (string)$db_paycheck->amount_bruto;
                    
                    $db_bruto_for_compare = 0;
                    if($SVP[3] === '1')
                    {
                        $db_bruto_for_compare = $db_paycheck->amount_bruto;
                        $additionals = $db_paycheck->additionals;
                        foreach($additionals as $additional)
                        {
                            if($paycheck_additional_types_formated[$additional->type]['separate_field_in_xml'] === true)
                            {
                                $db_bruto_for_compare -= $additional->bruto;
                            }
                        }
                    }
                    else if($SVP[3] === '2')
                    {
                        $db_bruto_for_compare = $db_paycheck->additionals_sum;
                    }
                    
                    $db_paycheck_bruto = number_format((float)$db_bruto_for_compare, 2, '.', '');
//                    $OsnovicaPorez = $child->OsnovicaPorez->__toString();
//                    $Porez = $child->Porez->__toString();
//                    $OsnovicaDoprinosi = $child->OsnovicaDoprinosi->__toString();
//                    $PIO = $child->PIO->__toString();
//                    $ZDR = $child->ZDR->__toString();
//                    $NEZ = $child->NEZ->__toString();
                    
                    if($bruto !== $db_paycheck_bruto && abs($bruto-$db_paycheck_bruto)>1)
                    {
                        $error_messages[] = Yii::t('PaychecksModule.Paycheck', 'BrutoNotSame', [
                            '{xml_bruto}' => $bruto,
                            '{db_bruto}' => $db_paycheck_bruto,
                            '{emp_name}' => $db_paycheck->employee->DisplayHTML
                        ]) . ' - god. staza: ' . $db_paycheck->year_experience
                                . ' - poena po radnom mestu: '. $db_paycheck->points
                                . ' ' . SIMAHtml::modelDialog($db_paycheck);
                    }
                    
                    $have_xml_paycheck_in_db = true;
                    break;
                }
            }
            
            if($have_xml_paycheck_in_db === false)
            {
                $error_messages[] = Yii::t('PaychecksModule.Paycheck', 'NotInDbXMLPaycheck', [
                    '{rbr}' => $redniBroj,
                    '{ime}' => $Ime,
                    '{prezime}' => $Prezime
                ]);
            }
        }
        
        foreach($paychecks as $db_paycheck)
        {
            $have_db_paycheck_in_xml = false;
            
            foreach ($xml_DeklarisaniPrihodi_children as $child)
            {
                $identifikatorPrimaoca = $child->IdentifikatorPrimaoca->__toString();
                if($identifikatorPrimaoca === $db_paycheck->employee->person->JMBG)
                {
                    $have_db_paycheck_in_xml = true;
                    break;
                }
            }
            
            if($have_db_paycheck_in_xml === false)
            {
                $error_messages[] = Yii::t('PaychecksModule.Paycheck', 'NotInXMLDbPaycheck', [
                    '{emp}' => $db_paycheck->employee->DisplayHTML
                ]);
            }
        }
        
        $error_messages_html = $this->renderPartial('compare_error_messages', array(
            'error_messages' => $error_messages,
        ), true, true);
        
        return $error_messages_html;
    }
    
    private function comparePPPDWithXmlContent($upload_key, $paycheck_period_id)
    {
        $error_messages = [];
        
        $xml = $this->loadUploadedXml($upload_key);
        $ppppd_items = $this->loadPPPPDItems($paycheck_period_id);
        
        $namespaces = $xml->getNameSpaces(true);
        if(empty($namespaces))
        {
            $xml_DeklarisaniPrihodi_children = $xml->DeklarisaniPrihodi->children();
        }
        else
        {
            $namespace = key($namespaces);
            
            $xml_children = $xml->children($namespaces[$namespace]);
            $xml_DeklarisaniPrihodi_children = $xml_children->DeklarisaniPrihodi->children($namespaces[$namespace]);
        }
        
        foreach ($xml_DeklarisaniPrihodi_children as $child)
        {
            $have_ppppd_item_in_db = false;
            
            $redniBroj = $child->RedniBroj->__toString();
            $vrstaIdentifikatoraPrimaoca = $child->VrstaIdentifikatoraPrimaoca->__toString();
            $identifikatorPrimaoca = $child->IdentifikatorPrimaoca->__toString();
            $Ime = $child->Ime->__toString();
            $Prezime = $child->Prezime->__toString();
//            $SVP = $child->SVP->__toString();
            
            foreach($ppppd_items as $ppppd_item)
            {
                if(intval($vrstaIdentifikatoraPrimaoca) === intval($ppppd_item->indentification_type) && $identifikatorPrimaoca === $ppppd_item->indentification_value)
                {
                    //                    $oznakaPrebivalista = $child->OznakaPrebivalista->__toString();
//                    $SVP = $child->SVP->__toString();
//                    $BrojKalendarskihDana = $child->BrojKalendarskihDana->__toString();
//                    $BrojEfektivnihSati = $child->BrojEfektivnihSati->__toString();
//                    $MesecniFondSati = $child->MesecniFondSati->__toString();
//                    $bruto = (string)$child->Bruto->__toString();
//                    $bruto = number_format((float)$child->Bruto->__toString(), 2, '.', '');
//                    $db_paycheck_bruto = (string)$db_paycheck->amount_bruto;
                    
                    
//                    $OsnovicaPorez = $child->OsnovicaPorez->__toString();
//                    $Porez = $child->Porez->__toString();
//                    $OsnovicaDoprinosi = $child->OsnovicaDoprinosi->__toString();
//                    $PIO = $child->PIO->__toString();
//                    $ZDR = $child->ZDR->__toString();
//                    $NEZ = $child->NEZ->__toString();
                    
                    if(!SIMAMisc::areEqual($child->Bruto->__toString(), $ppppd_item->bruto))
                    {
                        $error_messages[] = Yii::t('PaychecksModule.PPPPD', 'BrutoNotSame');
                    }
                    
                    $have_ppppd_item_in_db = true;
                    break;
                }
            }
            
            if($have_ppppd_item_in_db === false)
            {
                $error_messages[] = Yii::t('PaychecksModule.PPPPD', 'NotInDbPPPPDItem', [
                    '{rbr}' => $redniBroj,
                    '{jmbg}' => $identifikatorPrimaoca,
                    '{ime}' => $Ime,
                    '{prezime}' => $Prezime
                ]);
            }
        }
        
        foreach($ppppd_items as $ppppd_item)
        {
            $have_ppppd_item_in_xml = false;
            
            foreach ($xml_DeklarisaniPrihodi_children as $child)
            {
                $vrstaIdentifikatoraPrimaoca = $child->VrstaIdentifikatoraPrimaoca->__toString();
                $identifikatorPrimaoca = $child->IdentifikatorPrimaoca->__toString();
                if(intval($vrstaIdentifikatoraPrimaoca) === intval($ppppd_item->indentification_type) && $identifikatorPrimaoca === $ppppd_item->indentification_value)
                {
                    $have_ppppd_item_in_xml = true;
                    break;
                }
            }
            
            if($have_ppppd_item_in_xml === false)
            {
                $error_messages[] = Yii::t('PaychecksModule.PPPPD', 'NotInXMLPPPDItem', [
                    '{jmbg}' => $ppppd_item->indentification_value
                ]);
            }
        }
        
        $error_messages_html = $this->renderPartial('compare_error_messages', array(
            'error_messages' => $error_messages,
        ), true, true);
        
        return $error_messages_html;
    }
    
    private function loadPPPPDItems($paycheck_period_id)
    {
        $criteria = new SIMADbCriteria([
            'Model' => PPPPDItem::model(),
            'model_filter' => [
                'ppppd' => [
                    'paycheck_periods_to_ppppds' => [
                        'paycheck_period' => [
                            'ids' => $paycheck_period_id
                        ]
                    ]
                ]
            ]
        ]);
        $ppppd_items = PPPPDItem::model()->findAll($criteria);
        
        return $ppppd_items;
    }
    
    public function actionChangePPPPDIdForPPPPDXMLS()
    {
        $ppppd_ids_to_regenerate = $this->actionChangePPPPDIdForPPPPDXMLS_moveXMLs();
        
        $this->actionChangePPPPDIdForPPPPDXMLS_regeneratePPPPDs($ppppd_ids_to_regenerate);
        
        $this->respondOK();
    }
    
    private function actionChangePPPPDIdForPPPPDXMLS_moveXMLs()
    {
        $paycheck_xml_ids = $this->filter_post_input('paycheck_xml_ids');
        $ppppd_id = $this->filter_post_input('ppppd_id');
        
        $ppppd = PPPPD::model()->findByPkWithCheck($ppppd_id);
        
        $ppppd_ids_to_regenerate = [
            $ppppd->id
        ];
        
        foreach($paycheck_xml_ids as $paycheck_xml_id)
        {
            $paycheck_xml = PaycheckXML::model()->findByPkWithCheck($paycheck_xml_id);
            
            if(!is_null($paycheck_xml->ppppd_id))
            {
                $ppppd_ids_to_regenerate[] = $paycheck_xml->ppppd_id;
            }
            
            $paycheck_xml->ppppd_id = $ppppd->id;
            $paycheck_xml->save();
        }
        
        return array_unique($ppppd_ids_to_regenerate);
    }
    
    private function actionChangePPPPDIdForPPPPDXMLS_regeneratePPPPDs(array $ppppd_ids_to_regenerate)
    {
        $paycheck_period_id = $this->filter_post_input('paycheck_period_id');
        $paycheck_period = PaycheckPeriod::model()->findByPkWithCheck($paycheck_period_id);
        
        foreach($ppppd_ids_to_regenerate as $ppppd_id_to_regenerate)
        {
            $ppppd_to_regenerate = PPPPD::model()->findByPkWithCheck($ppppd_id_to_regenerate);
            
            $ppppd_to_regenerate->populateFromPaycheckPeriod($paycheck_period);
        }
    }
}

