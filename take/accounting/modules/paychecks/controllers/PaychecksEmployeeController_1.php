<?php

class PaychecksEmployeeController extends SIMAController
{
    public function actionIndex()
    {
        $html = $this->renderPartial('index', array(), true, true);

        $this->respondOk([
            'html' => $html,
            'title' => 'Zaposleni',
        ]);
    }
    
    public function actionOldPaychecksTabData($view)
    {
        $params = array();

        if($view === 'old_paychecks_view')
        {
            
        }
        else if($view === 'old_paychecks_errors')
        {
            $params = array(
                'problem_messages' => array(),
                'employeesNotOkIds'=>-1
            );

            $problem_messages = $this->checkAccountingMonthsForErrors();
            if(count($problem_messages) === 0)
            {
                $problem_messages = $this->checkPaycheckPeriodsForErrors();
            }

            if(count($problem_messages) > 0)
            {
                $params['problem_messages'] = $problem_messages;
            }
            else 
            {
                $months = Month::previousNmonths(12);
                $employeesNotOkIds = array();

    //            $workingEmployees = Employee::model()->haveActiveWorkContractInMonthPeriod([end($months), $months[0]])->findAll();
                $workingEmployees = Employee::model()->haveActiveWorkContractInMonth($months[0])->findAll();

                foreach($workingEmployees as $employee)
                {
                    if(!in_array($employee->id, $employeesNotOkIds) 
                            && !PaychecksEmployee::isEmployeeOkForPaycheckRecalc($employee, $months))
                    {
                        $employeesNotOkIds[] = $employee->id;
                    }
                }

                if(count($employeesNotOkIds) > 0)
                {
                    $params = array(
                        'employeesNotOkIds'=>$employeesNotOkIds
                    );
                }
            }
        }
        
        $html=  $this->renderPartial($view, $params, true, true);

        $this->respondOK(array('html'=>$html));
    }
    
    public function actionOldPaychecks()
    {        
//        $params = array(
//            'problem_messages' => array(),
//            'employeesNotOkIds'=>-1
//        );
//        
//        $problem_messages = $this->checkAccountingMonthsForErrors();
//        if(count($problem_messages) === 0)
//        {
//            $problem_messages = $this->checkPaycheckPeriodsForErrors();
//        }
//
//        if(count($problem_messages) > 0)
//        {
//            $params['problem_messages'] = $problem_messages;
//        }
//        else 
//        {
//            $months = Month::previousNmonths(12);
//            $employeesNotOkIds = array();
//            
////            $workingEmployees = Employee::model()->haveActiveWorkContractInMonthPeriod([end($months), $months[0]])->findAll();
//            $workingEmployees = Employee::model()->haveActiveWorkContractInMonth($months[0])->findAll();
//            
//            foreach($workingEmployees as $employee)
//            {
//                if(!in_array($employee->id, $employeesNotOkIds) 
//                        && !PaychecksEmployee::isEmployeeOkForPaycheckRecalc($employee, $months))
//                {
//                    $employeesNotOkIds[] = $employee->id;
//                }
//            }
//
//            if(count($employeesNotOkIds) > 0)
//            {
//                $params = array(
//                    'employeesNotOkIds'=>$employeesNotOkIds
//                );
//            }
//        }
        $params = [];
        $html=  $this->renderPartial('oldPaychecks', $params, true, true);

        $this->respondOk([
            'html' => $html,
            'title' => 'Stare zarade',
        ]);
    }
    
    private function checkAccountingMonthsForErrors()
    {
        $accounting_months_problem_messages = array();
        
        $months = Month::previousNmonths(12);
        foreach($months as $month)
        {
            $accountingMonth = AccountingMonth::model()->findByPk($month->id);
            if(!isset($accountingMonth))
            {
                $accounting_months_problem_messages[] = Yii::t(
                        'PaychecksModule.PaychecksEmployee', 
                        'NoAccountingMonth',
                        array(
                            'month' => $month
                        )
                    );
            }
        }
        
        return $accounting_months_problem_messages;
    }
    
    private function checkPaycheckPeriodsForErrors()
    {
        $paycheckPeriodErrorMessages = array();
        
        $months = Month::previousNmonths(12);
        foreach($months as $month)
        {
            $accountingMonth = AccountingMonth::model()->findByPk($month->id);
            $paycheckPeriods = $accountingMonth->paycheck_periods;
            
            $error_message = null;
            
            if(count($paycheckPeriods) === 0)
            {
                $error_message = Yii::t(
                        'PaychecksModule.PaychecksEmployee', 
                        'NoPaycheckPeriodsForMonth',
                        array(
                            '{month}' => $month
                        )
                    );
            }
            
            if(isset($error_message))
            {
                $paycheckPeriodErrorMessages[] = $error_message;
            }
        }
        
        return $paycheckPeriodErrorMessages;
    }
    
    public function actionCreatePaycheckPeriods()
    {
        $months = Month::previousNmonths(12);
        foreach($months as $month)
        {
            $accountingMonth = AccountingMonth::model()->findByPk($month->id);
            $paycheckPeriods = $accountingMonth->paycheck_periods;
                        
            if(count($paycheckPeriods) === 0)
            {
                $paycheckPeriod = new PaycheckPeriod();
                $paycheckPeriod->month_id = $month->id;
                $paycheckPeriod->payment_date = $month->lastDay();
                $paycheckPeriod->pp_type = PaycheckPeriod::$FINAL;
                $paycheckPeriod->old_period = true;
                $paycheckPeriod->hours_percentage = 100;
                $paycheckPeriod->save();
            }
        }
        
        $this->respondOK();
    }
    
    public function actionCreateOldPaycheck()
    {
        $paycheck_period_id = $this->filter_post_input('paycheck_period_id');
        $employee_id = $this->filter_post_input('employee_id');
        $worked_hours = $this->filter_post_input('worked_hours');
        $worked_hours_value = $this->filter_post_input('worked_hours_value');
        $past_exp_value = $this->filter_post_input('past_exp_value');
                
        $new_paycheck = new Paycheck();
        $new_paycheck->employee_id = $employee_id;
        $new_paycheck->paycheck_period_id = $paycheck_period_id;
        $new_paycheck->worked_hours = $worked_hours;
        $new_paycheck->worked_hours_value = $worked_hours_value;
        $new_paycheck->past_exp_value = $past_exp_value;
        $new_paycheck->save();
        
        $this->respondOK();
    }
    
    public function actionGenerateDialogForMissingPaycheckData()
    {
        $uniq_id = $this->filter_post_input('uniq_id');
        $dateFromName = $this->filter_post_input('dateFromName');
        $dateToName = $this->filter_post_input('dateToName');
        $type = $this->filter_post_input('type');
        $initData = $this->filter_post_input('initData', false);
        
        $dateFrom_value = array('model'=>'PaycheckPeriod');
        $dateTo_value = array('model'=>'PaycheckPeriod');
        $selected_employees = array();
        $data_value = '';
        if(isset($initData))
        {
            if(isset($initData['dateFrom_value']) && isset($initData['dateFrom_value']['id']) && isset($initData['dateFrom_value']['display']))
            {
                $dateFrom_value['real_value'] = $initData['dateFrom_value']['id'];
                $dateFrom_value['display_value'] = $initData['dateFrom_value']['display'];
            }
            if(isset($initData['dateTo_value']) && isset($initData['dateTo_value']['id']) && isset($initData['dateTo_value']['display']))
            {
                $dateTo_value['real_value'] = $initData['dateTo_value']['id'];
                $dateTo_value['display_value'] = $initData['dateTo_value']['display'];
            }
            if(isset($initData['selected_employees']))
            {
                $selected_employees = $initData['selected_employees'];
            }
            if(isset($initData['data_value']))
            {
                $data_value = 'value="'.$initData['data_value'].'"';
            }
        }
        
        $params = array(
            'uniq' => $uniq_id,
            'dateFromName' => $dateFromName,
            'dateFrom_value' => $dateFrom_value,
            'dateToName' => $dateToName,
            'dateTo_value' => $dateTo_value,
            'selected_employees' => $selected_employees,
            'data_display' => $type,
            'data_value' => $data_value
        );
        
        $html = $this->renderPartial('missingPaycheckDataDialog', $params, true, true);
        
        $this->respondOK(array(
            'html' => $html
        ));
    }
    
    public function actionCreateMissingPaycheckDialog()
    {
        $uniq_id = SIMAHtml::uniqid();
        
        $params = [
            'uniq' => $uniq_id,
        ];
        
//        $html = 'asd';
        $html = $this->renderPartial('missingPaycheckDialog', $params, true, true);
        
        $this->respondOK(array(
            'html' => $html
        ));
    }
    
    public function actionPopulateMissingPaycheckData()
    {
        $selected_employees = $this->filter_post_input('selected_employees', false);
        $data_column = $this->filter_post_input('data_column', false);
        $data_value = $this->filter_post_input('data_value', false);
        $dateFrom_value = $this->filter_post_input('dateFrom_value', false);
        $dateTo_value = $this->filter_post_input('dateTo_value', false);
        
        if(!isset($selected_employees) || empty($selected_employees)
                || !isset($data_column) || empty($data_column)
                || !isset($data_value) || (empty($data_value) && $data_value != 0)
                || !isset($dateFrom_value) || empty($dateFrom_value)
                || !isset($dateTo_value) || empty($dateTo_value))
        {
            $this->respondOK(array(
                    'message' => 'failure'
            ));
        }
        
        $selected_employees_ids = array();
        foreach($selected_employees as $selected_employee)
        {
            $selected_employees_ids[] = $selected_employee['id'];
        }
        
        $fromPaycheckPeriod = PaycheckPeriod::model()->findByPk($dateFrom_value['id']);
        $toPaycheckPeriod = PaycheckPeriod::model()->findByPk($dateTo_value['id']);
        $months = Month::getFromToMonths($fromPaycheckPeriod->month, $toPaycheckPeriod->month);
        
        $this->populateMissingPaycheckDataForSelectedEmployees($selected_employees_ids, $months, $data_column, $data_value);
        
        $this->respondOK(array(
                'message' => 'success'
        ));
    }
    
    private function populateMissingPaycheckDataForSelectedEmployees(array $selected_employees_ids, array $months, $data_column, $hours_value)
    {
        $criteriaEmployees = new SIMADbCriteria([
            'Model' => 'Employee',
            'model_filter' => [
                'ids' => $selected_employees_ids
            ]
        ]);
        
        $employees = Employee::model()->findAll($criteriaEmployees);
        
        foreach($employees as $employee)
        {
            foreach($months as $month)
            {
                $criteriaPaycheckPeriod = new SIMADbCriteria([
                    'Model' => 'PaycheckPeriod',
                    'model_filter' => [
                        'month' => array(
                            'ids' => $month->id
                        )
                    ]
                ]);
                $paycheckPeriod = PaycheckPeriod::model()->find($criteriaPaycheckPeriod);
                
                if(!isset($paycheckPeriod))
                {
                    throw new SIMAWarnException(Yii::t(
                            'PaychecksModule.PaychecksEmployee', 
                            'NoPaycheckPeriodsForMonth', 
                            array(
                                '{month}' => $month
                            )));
                }
                
                $criteriaPaycheck = new SIMADbCriteria([
                    'Model' => 'Paycheck',
                    'model_filter' => [
                        'employee' => array(
                            'ids' => $employee->id
                        ),
                        'paycheck_period' => array(
                            'ids' => $paycheckPeriod->id
                        )
                    ]
                ]);
                $paychecks = Paycheck::model()->findAll($criteriaPaycheck);
                $paychecksCount = count($paychecks);
                
                foreach($paychecks as $paycheck)
                {
                    $paycheck->scenario = 'oldPaycheckAdd';
                    $paycheck->$data_column = ($hours_value)/$paychecksCount;
                    $paycheck->save();
                }
            }
        }
    }
}
