<?php

class SuspensionController extends SIMAController
{
    public function actionGroupIndex($id)
    {
        $model = ModelController::GetModel('SuspensionGroup', $id);
        
        $html = $this->renderDefaultModelView($model, [
            'tabs_params' => [
                'default_selected' => 'suspensions'
            ]
        ]);
        
        $title = $model->DisplayName;
                
        $this->respondOK([
            'html' => $html,
            'title' => $title
        ]);
    }
    
    public function actionAddAllSuspensionsForActiveEmployees($suspension_group_id)
    {
        $suspensionGroup = ModelController::GetModel('SuspensionGroup', $suspension_group_id);
        
        $employeesCriteria = new SIMADbCriteria([
            'Model' => Employee::model(),
            'model_filter' => [
                'filter_scopes' => [
                    'haveActiveWorkContractInMonth' => $suspensionGroup->month->id,
                    'noSuspensionInSuspensionGroups' => $suspensionGroup->id
                ]
            ]
        ]);
        
        $employees = Employee::model()->findAll($employeesCriteria);
        
        foreach($employees as $employee)
        {
//            error_log(__METHOD__.' -  $employee->DisplayName: '.$employee->DisplayName);
            $suspension = new Suspension();
//            $suspension->setScenario('forGroup');
            $suspension->name = $suspensionGroup->name;
            $suspension->creditor_id = $suspensionGroup->creditor_id;
            $suspension->begin_month_id = $suspensionGroup->month_id;
            $suspension->total_amount = $suspensionGroup->amount;
            $suspension->payment_type = $suspensionGroup->payment_type;
            $suspension->installment_amount = $suspensionGroup->installment_amount;
            $suspension->creditor_bank_account_id = $suspensionGroup->creditor_bank_account_id;
            $suspension->employee_id = $employee->id;
            $suspension->suspension_group_id = $suspensionGroup->id;
            $suspension->save();
        }
        
        $this->respondOK([
            'message' => Yii::t('BaseModule.Common', 'Success')
        ]);
    }
}

