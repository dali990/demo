<?php

return [
    'StartUse' => 'Date of application',
    'EndUse' => 'End date of application',
    'PioEmpl' => 'PIO employee',
    'ZdrEmpl' => 'Healthy employee',
    'NezEmpl' => 'Unemployment insurance employee',
    'TaxEmpl' => 'Tax',
    'TaxRelease' => 'Tax release',
    'PioComp' => 'PIO comapny',
    'ZdrComp' => 'Healthy comapny',
    'NezComp' => 'Unemployment insurance company',
    'MinTaxBase' => 'Minimum contribution base',
    'MaxTaxBase' => 'Maximum base for contributions',
    'YouCanDeleteOnlyFirstOrLast' => 'You can delete only the first or last paycheck',
    'SyncTypeForPaychecksCodebook' => 'Synchronization type for paychecks codebook',
    'SyncType' => 'Synchronization type',
    'ManualSync' => 'Manual synchronization',
    'ManualSyncWithNotification' => 'Manual sync with notification',
    'AutomaticSynchronizationWithNotification' => 'Automatic synchronization with notification',
    'AutomaticSynchronizationWithoutNotification' => 'Automatic synchronization without notification',
    'NotifyIfPaycheckCodebookChanges' => 'Notify if paycheck codebook changes',
    'PaycheckCodebookHasChangedPleaseUpdateYourCodebook' => 'Paycheck codebook has changed. Please update your codebook.',
    'PaycheckCodebookHasChangedk' => 'Paycheck codebook has changed.',
    'TheChangesAreAsFollows:' => 'The changes are as follows:',
    'NewPaycheckPeriod' => 'New peycheck perion ',
    'PaycheckPeriod' => 'Peycheck perion ',
    'OldValue' => 'Old value',
    'ForRemove' => 'For remove',
    'Removed' => 'Removed',
    'DifferencesInAccountingPeriodsCanNotBeMerged' => 'There are differences in accounting periods and can not be merged.<br>Differences in:<br>'
];