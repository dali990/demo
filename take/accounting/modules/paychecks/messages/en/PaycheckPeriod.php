<?php

return array(
    'PaymentDateMustBeWorkDay' => 'Payment date must be work day',
    'DeletePaycheckPeriodWithPaychecks' => 'Can not delete paycheck period because there are paychecks for it'
);