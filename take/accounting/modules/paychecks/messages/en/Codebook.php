<?php

return [
    'SyncPaychecksCodebook' => 'Sync paychecks codebook',
    'StartUseDateMustBeLessThanEndUseDate' => 'The start use date must be after the given start date of the period, but before the target date of the end of the period.'
];

