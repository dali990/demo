<?php

return array(
    'NoAccountingMonth' => 'Nije unet finansijski mesec: {month}!',
    'NoPaycheckPeriodsForMonth' => 'Nema isplata zarada za finansijski mesec: {month}!',
    'NoPaycheckForMonth' => 'Ne postoji zarada za mesec {month}',
    'NoWorkedHoursForMonth' => 'Ne postoje sati redovnog rada za mesec: {month}',
    'NoWorkedHoursValueForMonth' => 'Ne postoji vrednost redovnog rada za mesec: {month}',
    'NoPastExperienceValueForMonth' => 'Ne postoji vrednost minulog rada za mesec: {month}',
    'worked_hours' => 'Sati redovnog rada',
    'worked_hours_value' => 'Vrednost redovnog rada',
    'past_exp_value' => 'Vrednost minulog rada',
    'OldPaycheckFor12MonthsNeed' => 'Za zaposlenog {employee} postoje vrednosti starih zarada koje moraju da se popune!',
    'ReasonForNotOk' => 'Nepravilnosti',
    'StatePartnerMainBankAccountNotSet' => 'Primarni racun drzave za zarade nije postavljen',
    'NoPaychecks' => 'Nema starih zarada',
    'ModifiedAfterLastCalculationStart' => 'Modifikovan nakon poslednjeg obracuna',
    'NoCalculationStarted' => 'Nema uspesno zapocetog obracuna',
    'NoCalculationEnded' => 'Nema uspesno zavrsenog obracuna',
    'NoCalculationSuccessfullyEnded' => 'Nema uspesno zavrsenog obracuna',
);