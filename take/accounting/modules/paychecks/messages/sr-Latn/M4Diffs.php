<?php

return [
    'NotEnoughRolls' => 'Nema dovoljno rolni. Morate prvo dodati još {rest}.',
    'PositionNumber' => 'Broj pozicije',
    'RollNumber' => 'Broj rolne',
    'Cnt' => 'Redni broj'
];
