<?php

return [
    'DeductionsTab' => 'Odbici',
    'AllDeductionsTab' => 'Svi odbici',
    'DeductionType' => 'Tip',
    'TypeHotMeal' => 'Topli obrok',
    'TypeOther' => 'Ostalo'
];