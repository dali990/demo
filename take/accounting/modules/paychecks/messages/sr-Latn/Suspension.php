<?php

return [
    'Creditor' => 'Kreditor',
    'BeginMonth' => 'Početni mesec',
    'TotalAmount' => 'Ukupan iznos',
    'InstallmentAmount' => 'Vrednost rate',
    'TotalAmountIsZero' => 'Ukupna vrednost je 0 (ili manja)',
    'InstallmentAmountIsZero' => 'Vrednost rate je 0 (ili manja)',
    'TotalAmountIsLessThanInstallmentAmount' => 'Ukupna vrednost je manja od vrednosti rate',
    'PaidAmount' => 'Isplaćeno',
    'PaidAmountIsMoreThanTotalAmount' => 'Isplaćeno je vise od ukupne vrednosti',
    'LeftToPayAmount' => 'Preostalo za isplatu',
    'Suspension' => 'Administrativna zabrana',
    'Suspensions' => 'Administrativne zabrane',
    'PaymentType' => 'Način isplate',
    'Arbitrarly' => 'Proizvoljno',
    'CreditorBankAccount' => 'Žiro račun kreditora',
    'CallForNumber' => 'Poziv na broj',
    'CallForNumberModul' => 'Poziv na broj - modul',
    'AllSuspensions' => 'Spisak svih administrativnih zabrana',
    'SuspensionGroups' => 'Grupe administrativnih zabrana',
    'SuspensionGroup' => 'Grupa administrativnih zabrana',
    'ActiveEmployeesInGroupMonthWithoutSuspensions' => 'Zaposleni aktivni u mesecu a bez administrativne zabrane u grupi',
    'Employee' => 'Zaposleni',
    'AddAllSuspensionsForActiveEmployees' => 'Kreiraj svim zaposlenim aktivnim u mesecu',
    'TypeArbitrarly' => 'Proizvoljno',
    'TypeAutoAll' => 'Automatski sve',
    'TypeAutoInstallment' => 'Automatski rata',
    'AmountMoreThanSuspensionLeftToPay' => 'Vrednost je veća nego što je ostalo za isplatu administrativne zabrane',
    'PaycheckSuspension' => 'Administrativna zabrana',
    'PaycheckSuspensions' => 'Administrativne zabrane',
    'ValueNotChanged' => 'Zaposlenom ->{employee}<- nije promenjena vrednost za ->{column}<- jer se razlikovao od prethodno podešenog'
];
