<?php

return [
    'GroupBonus' => 'Grupni bonus',
    'GroupBonuses' => 'Grupni bonusi',
    'Bonus' => 'Bonus',
    'Bonuses' => 'Bonusi',
    'Employee' => 'Zaposleni',
    'FillPersonalBonuses' => 'Popuni lične bonuse',
    'GroupBonusType' => 'Tip grupnog bonusa'
];
