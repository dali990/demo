<?php

return [
    'M4EmployeeRow' => 'M4 zaposleni',
    'M4MFRoll' => 'Pozicija u rolni',
    'Employee' => 'Zaposleni',
    'EmployeeName' => 'Ime i prezime',
    'JMBG' => 'JMBG',
    'WorkExpMonths' => 'Meseci radnog staža',
    'WorkExpDays' => 'Dani radnog staža',
    'Bruto' => 'Bruto zarada',
    'PioDoprinos' => 'PIO doprinos',
    'ZdravBruto' => 'Zdravstveni bruto',
    'ZdravPioDoprinos' => 'Zdravstveni PIO doprinos',
    'NotValidNumericNumber' => 'Nevalidan broj. Broj može imati najviše {precision} cifara, a iza decimalnog separatora najviše {scale}.',
    'UniqEmployee' => 'Već je dodat ovaj zaposleni',
    'Predict' => 'Predloži',
    'MaxEmployeesNumber' => 'Na ovoj poziciji u rolni je dostignut maksimalan broj zaposlenih od {max_number}'
];

