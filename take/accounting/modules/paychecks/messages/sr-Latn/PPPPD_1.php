<?php

return [
    'NotInXMLPPPDItem' => 'U SIMA-i se nalazi stavka PPP-PD-a koja se ne nalazi u XML-u | JMBG: {jmbg}',
    'NotInDbPPPPDItem' => 'Stavka se nalazi u XML-u, a ne nalazi se u bazi | JMBG: {jmbg} | Ime: {ime} | Prezime: {prezime}',
    'RegenerateFromPaychecks' => 'Generisi od zarada',
    'Regenerate' => 'Regenerisi',
    'RegenerateCompletely' => 'Regenerisi kompletno',
    'BindExistingPPPPD' => 'Povezi postojeci PPP-PD',
    'MoveXMLsToOtherPPPD' => 'Premesti XML-ove u drugi PPP-PD'
];