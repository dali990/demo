<?php

return [
    'Codebook' => 'Šifarnik za obračun zarada',
    'StartUseDateMustBeLessThanEndUseDate' => 'Datum kraja primene mora biti posle zadatog datuma početka primene.',
    'ThereIsAlreadyPaycheckCodebookInThisInterval' => 'Postoji sifarnik zarada u ovom intervalu - {dates}',
    'StartUseMustBeOneDayAfterLastEnd' => 'Pocetak mora biti 1 dan nakon prethodnog kraja',
    'MaintainSyncTitle' => 'Šifarnik za obračun zarada - {to_add_count} za dodati i {to_update_count} za izmeniti',
    'ExpPerc' => 'Minuli rad(%)',
    'HotMealPerHour' => 'Topli obrok(rsd/h)',
    'Regres' => 'Regres (rsd)',
    'EndUseMustBeOneDayBeforeNextStart' => 'Datum zavrsetka mora biti dan pre sledeceg pocetka',
    'SplitPeriod' => 'Podeli period',
    'MergePeriods' => 'Spoji periode',
    'SyncPaychecksCodebook' => 'Sinhronizujte obračun zarada iz šifarnika',
    'MustBeLessThanMaxTaxBase' => 'mora biti manje od maksimalne osnovice za doprinose',
    'MMustBeLargerThanMinTaxBase' => 'mora biti vece od minimalne osnovice za doprinose'
];

