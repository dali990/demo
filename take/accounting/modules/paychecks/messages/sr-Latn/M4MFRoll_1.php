<?php

return [
    'UniqPositionNumberInRoll' => 'Već postoji ova pozicija za ovu godinu i rolnu',
    'PositionNumber' => 'Broj pozicije',
    'RollNumber' => 'Broj rolne',
    'Year' => 'Godina',
    'M4MFRoll' => 'M4MF pozicija u rolni',
    'M4MFRolls' => 'Pozicija u rolni',
    'PositionNumber' => 'Broj pozicije',
    'RollNumber' => 'Broj rolne',
    'M4PdfForYear' => 'M4 obrazac za {year}'
];
