<?php

return array(
    'DisplayName' => 'Naziv',
    'Type' => 'Tip',
    'Bank' => 'Banka',
    'BankAccount' => 'Bankovni račun',
    'ExistsActiveForBank' => 'Postoji aktivno objedinjeno plaćanje za banku "{bank}"',
    'BankExportFile' => 'Izvozni fajl za banku',
    'DisplayFile' => 'Fajl za Prikaz',
    'UnifiedPaycheck' => 'Objedinjeno placanje',
    'UnifiedPaycheckToPaycheckPeriod' => 'Veza objedinjenog placanja i isplate zarade',
    'TYPE_PDF' => 'PDF',
    'TYPE_TXT' => 'TXT',
);
