<?php

return array(
    'Paychecks' => 'Zarade',
    'StatePartner' => 'Drzava partner',
    'StatePartnerDescription' => 'Partner koji predstavlja drzavu, obavezno za kreiranje placanja u obracunu zarada',
    'ProbationerWorkPercent' => 'Pripravnicki radni sati (procenat)',
    'ProbationerWorkPercentDescription' => 'Koliki procenat od ukupnih radnih sati se uzima u obzir za pripravnike',
    'StatePaycheckPaymentCode' => 'Kod placanja za drzavu',
    'StatePaycheckPaymentCodeDescription' => 'Prilikom kreiranja jednog placanja za drzavu potrebn je i kod (254)',
    'TufeFix' => 'Tufe FIX',
    'TufeFixDescription' => 'Da li u obracunu zarade koristiti fix koji koristi i Tufe program'
        .'</br>- Regres se zaokruzuje na ceo broj'
        .'</br>- Topli obrok se skalira'
        .'</br>- Ukoliko je zaposleni aktivnim ugovorom na novoj radnoj poziciji, ne racunaj vrednosti prethodnih 12 meseci',
    'CalculateWithWorkOnStateHoliday' => 'Obracunati rad na drzavni praznik',
    'CalculateWithWorkOnStateHolidayDescription' => 'Da li pri obracunu zarade racunati i rad na drzavni praznik',
    'OvertimePercentageDescription' => 'Procenat vrednosti sata prekovremeno',
    'NonTaxablePart' => 'Deo koji nije oporeziv',
    'NonTaxablePartDesc' => 'Deo koji nije oporeziv. Zadaje se u dinarima.',
    'PayoutValuePerDay' => 'Iznos dnevnice u dinarima',
    'BrutoHourCalcType' => 'Nacin izracunavanja bruto sata',
    'BrutoHourCalcTypeDescription' => ' ',
    'BrutoDivideByPoints' => 'poeni za prosecnu zaradu - delovi bruto zarade se dele pomocu poena',
    'BrutoDivideByPercent' => 'poeni za prosecnu zaradu - delovi bruto zarade se dele pomocu procenta',
    'BrutoByPointsForHourValue' => 'poeni za satnicu',
    'BrutoByHourValue' => 'po satnici',
    'UseMaxTaxBase' => 'Koristiti maksimalnu osnovicu pri obracunu plata',
    'UseMaxTaxBaseDesc' => 'Obavezno uz upotrebu novih sifarnika plata',
    'UseNewCoodebooks' => 'Koristiti novi šifarnik plata',
    'UseNewCoodebooksDesc' => 'Koristiti novi šifarnik plata',
    'UseMaxContributionBase' => 'Koristiti max osnovicu doprinosa',
    'UseMaxContributionBaseDesc' => 'Obracun plata',
);
