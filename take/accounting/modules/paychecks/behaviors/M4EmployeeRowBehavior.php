<?php

class M4EmployeeRowBehavior extends SIMAActiveRecordBehavior
{
    public static function calcM4ForEmployeesForYear($year_id, $update_func=null)
    {
        $diffs = [];
        $employees = [];

        $year = Year::model()->findByPk($year_id);
        
        $crit = new SIMADbCriteria([
            'Model' => 'Paycheck',
            'model_filter' => [
                'display_scopes' => 'orderByEmpJMBGASC',
                'paycheck_period' => [
                    'month' => [
                        'year' => ['ids' => $year_id]
                    ]
                ]
            ]
        ]);
        $cnt = 1;
        $_pay_cnt = 1;
        $_total =  Paycheck::model()->count($crit);
        $first_day = Day::getByDate('01.01.'.$year->year);
        $last_day = Day::getByDate('31.12.'.($year->year));

        Paycheck::model()->findAllByParts(function($rows) use (&$employees, &$cnt, $first_day, $last_day, $update_func, $_total, &$_pay_cnt) {
            foreach ($rows as $paycheck)
            {
                if (!isset($employees[$paycheck->employee_id]))
                {
                    $work_exp = $paycheck->employee->getWorkExperience($first_day, $last_day);

                    $employees[$paycheck->employee_id] = [
                        'cnt' => $cnt++,
                        'employee_name' => $paycheck->employee->person->lastname . $paycheck->employee->person->firstname ,
                        'jmbg' => $paycheck->employee->person->JMBG,
                        'work_exp_months' => (string)(floor($work_exp/30)),
                        'work_exp_days' => (string)($work_exp%30),
                        'bruto' => $paycheck->amount_bruto,
                        'pio_doprinos' => $paycheck->pio_empl + $paycheck->pio_comp,
                        'zdrav_bruto' => 0,
                        'zdrav_pio_doprinos' => 0,
                    ];
                }
                else
                {
                    $employees[$paycheck->employee_id]['bruto'] += $paycheck->amount_bruto;
                    $employees[$paycheck->employee_id]['pio_doprinos'] += $paycheck->pio_empl + $paycheck->pio_comp;
                }
                $_pay_cnt++;
                if (!empty($update_func))
                {
                    call_user_func($update_func, $_pay_cnt*60/$_total);
                }
            }
        }, $crit);
        
        $rolls_cnt = M4MFRoll::model()->count(new SIMADbCriteria([
            'Model' => M4MFRoll::class,
            'model_filter' => [
                'year' => [
                    'ids' => $year->id
                ]
            ]
        ]));

        //ako nema dovoljno rolni baca se upozorenje
        $employees_cnt = count($employees);
        if ($rolls_cnt * M4MFRoll::MAX_M4_ROLL_EMPLOYEES < $employees_cnt)
        {
            $rest = ceil($employees_cnt/M4MFRoll::MAX_M4_ROLL_EMPLOYEES) - $rolls_cnt;
            throw new SIMAWarnException(Yii::t('PaychecksModule.M4Diffs', 'NotEnoughRolls', ['{rest}' => $rest]));
        }
        
        $rolls_by_employees = M4EmployeeRowBehavior::getRollsByEmployeesForYear($year->id);
        
        $m4employee_rows = M4EmployeeRow::model()->findAll(new SIMADbCriteria([
            'Model' => M4EmployeeRow::class,
            'model_filter' => [
                'm4mf_roll' => [
                    'year' => [
                        'ids' => $year->id
                    ]
                ]
            ]
        ]));
        
        $m4employee_rows_cnt = count($m4employee_rows);

        $i = 1;
        foreach ($employees as $employee_key => $employee_value)
        {
            $m4employee_row = M4EmployeeRow::model()->find(new SIMADbCriteria([
                'Model' => M4EmployeeRow::class,
                'model_filter' => [
                    'employee' => [
                        'ids' => $employee_key
                    ],
                    'm4mf_roll' => [
                        'year' => [
                            'ids' => $year->id
                        ]
                    ]
                ]
            ]));
            
            //ako ne postoji u bazi dodaje se
            if (empty($m4employee_row))
            {
                $roll = M4EmployeeRowBehavior::getNextFreeRoll($rolls_by_employees);
                if (!empty($roll))
                {
                    M4EmployeeRowBehavior::addItemToDiffsGroup($diffs, [
                        'group_id' => $roll->id,
                        'group_name' => Yii::t('PaychecksModule.M4Diffs', 'PositionNumber') . ": {$roll->position_number}" . ", " . Yii::t('PaychecksModule.M4Diffs', 'RollNumber') . ": {$roll->roll_number}",
                    ], [
                        'status' => 'ADD',
                        'columns' => [
                            Yii::t('PaychecksModule.M4Diffs', 'EmployeeName')=> [
                                'new_value'=>$employee_value['employee_name']
                            ],
                            Yii::t('PaychecksModule.M4Diffs', 'JMBG')=> [
                                'new_value'=>$employee_value['jmbg']
                            ],
                            Yii::t('PaychecksModule.M4Diffs', 'WorkExpMonths')=>[
                                'new_value'=>$employee_value['work_exp_months']
                            ],
                            Yii::t('PaychecksModule.M4Diffs', 'WorkExpDays')=>[
                                'new_value'=>$employee_value['work_exp_days']
                            ],
                            Yii::t('PaychecksModule.M4Diffs', 'Bruto')=>[
                                'new_value'=>SIMAHtml::number_format($employee_value['bruto'])
                            ],
                            Yii::t('PaychecksModule.M4Diffs', 'PioDoprinos')=>[
                                'new_value'=>SIMAHtml::number_format($employee_value['pio_doprinos'])
                            ],
                            Yii::t('PaychecksModule.M4Diffs', 'ZdravBruto')=>[
                                'new_value'=>SIMAHtml::number_format($employee_value['zdrav_bruto'])
                            ],
                            Yii::t('PaychecksModule.M4Diffs', 'ZdravPioDoprinos')=>[
                                'new_value'=>SIMAHtml::number_format($employee_value['zdrav_pio_doprinos'])
                            ]
                        ],
                        'data' => [
                            'status' => 'ADD',
                            'model' => M4EmployeeRow::class,
                            'attributes' => [
                                'm4mf_roll_id' => $roll->id,
                                'employee_id' => $employee_key,
                                'employee_name' => $employee_value['employee_name'],
                                'jmbg' => $employee_value['jmbg'],
                                'work_exp_months' => $employee_value['work_exp_months'],
                                'work_exp_days' => $employee_value['work_exp_days'],
                                'bruto' => $employee_value['bruto'],
                                'pio_doprinos' => $employee_value['pio_doprinos'],
                                'zdrav_bruto' => $employee_value['zdrav_bruto'],
                                'zdrav_pio_doprinos' => $employee_value['zdrav_pio_doprinos']
                            ]
                        ] 
                    ]);
                }
            }
            
            if (!empty($update_func))
            {
                $percent = round(($i/$employees_cnt)*20, 0, PHP_ROUND_HALF_DOWN);
                call_user_func($update_func, 60 + $percent);
            }

            $i++;
        }
        
        $j = 1;
        foreach ($m4employee_rows as $m4employee_row)
        {
            $roll = $m4employee_row->m4mf_roll;
            $employees_params = null;
            if (isset($employees[$m4employee_row->employee_id]))
            {
                $employees_params = $employees[$m4employee_row->employee_id];
            }

            //ako ne postoji u predlozima brise se
            if (empty($employees_params))
            {
                M4EmployeeRowBehavior::addItemToDiffsGroup($diffs, [
                    'group_id' => $roll->id,
                    'group_name' => Yii::t('PaychecksModule.M4Diffs', 'PositionNumber') . ": {$roll->position_number}" . ", " . Yii::t('PaychecksModule.M4Diffs', 'RollNumber') . ": {$roll->roll_number}",
                ], [
                    'status' => 'REMOVE',
                    'columns' => [
                        Yii::t('PaychecksModule.M4Diffs', 'EmployeeName')=> [
                            'new_value'=>$m4employee_row->employee_name
                        ],
                        Yii::t('PaychecksModule.M4Diffs', 'JMBG')=> [
                            'new_value'=>$m4employee_row->jmbg
                        ],
                        Yii::t('PaychecksModule.M4Diffs', 'WorkExpMonths')=>[
                            'new_value'=>$m4employee_row->work_exp_months
                        ],
                        Yii::t('PaychecksModule.M4Diffs', 'WorkExpDays')=>[
                            'new_value'=>$m4employee_row->work_exp_days
                        ],
                        Yii::t('PaychecksModule.M4Diffs', 'Bruto')=>[
                            'new_value'=>SIMAHtml::number_format($m4employee_row->bruto)
                        ],
                        Yii::t('PaychecksModule.M4Diffs', 'PioDoprinos')=>[
                            'new_value'=>SIMAHtml::number_format($m4employee_row->pio_doprinos)
                        ],
                        Yii::t('PaychecksModule.M4Diffs', 'ZdravBruto')=>[
                            'new_value'=>SIMAHtml::number_format($m4employee_row->zdrav_bruto)
                        ],
                        Yii::t('PaychecksModule.M4Diffs', 'ZdravPioDoprinos')=>[
                            'new_value'=>SIMAHtml::number_format($m4employee_row->zdrav_pio_doprinos)
                        ]
                    ],
                    'data' => [
                        'status' => 'REMOVE',
                        'model' => M4EmployeeRow::class,
                        'model_id' => $m4employee_row->id,
                    ]
                ]);
            }
            //ako postoji i u bazi i u predlozima proveravamo da li ima izmena
            else if (M4EmployeeRowBehavior::hasM4EmployeeRowChanges($m4employee_row, $employees_params) === true)
            {
                $employee_name = ['old_value' => $m4employee_row->employee_name];
                if ($m4employee_row->employee_name !== $employees_params['employee_name'])
                {
                    $employee_name['new_value'] = $employees_params['employee_name'];
                }
                
                $jmbg = ['old_value' => $m4employee_row->jmbg];
                if ($m4employee_row->jmbg !== $employees_params['jmbg'])
                {
                    $jmbg['new_value'] = $employees_params['jmbg'];
                }
                
                $work_exp_months = ['old_value' => $m4employee_row->work_exp_months];
                if ($m4employee_row->work_exp_months !== $employees_params['work_exp_months'])
                {
                    $work_exp_months['new_value'] = $employees_params['work_exp_months'];
                }
                
                $work_exp_days = ['old_value' => $m4employee_row->work_exp_days];
                if ($m4employee_row->work_exp_days !== $employees_params['work_exp_days'])
                {
                    $work_exp_days['new_value'] = $employees_params['work_exp_days'];
                }
                
                $bruto = ['old_value' => $m4employee_row->bruto];
                if (!SIMAMisc::areEqual($m4employee_row->bruto, $employees_params['bruto'], 0.01))
                {
                    $bruto['new_value'] = $employees_params['bruto'];
                }
                
                $pio_doprinos = ['old_value' => $m4employee_row->pio_doprinos];
                if (!SIMAMisc::areEqual($m4employee_row->pio_doprinos, $employees_params['pio_doprinos'], 0.01))
                {
                    $pio_doprinos['new_value'] = $employees_params['pio_doprinos'];
                }
                
                $zdrav_bruto = ['old_value' => $m4employee_row->zdrav_bruto];
                if (!SIMAMisc::areEqual($m4employee_row->zdrav_bruto, $employees_params['zdrav_bruto'], 0.01))
                {
                    $zdrav_bruto['new_value'] = $employees_params['zdrav_bruto'];
                }
                
                $zdrav_pio_doprinos = ['old_value' => $m4employee_row->zdrav_pio_doprinos];
                if (!SIMAMisc::areEqual($m4employee_row->zdrav_pio_doprinos, $employees_params['zdrav_pio_doprinos'], 0.01))
                {
                    $zdrav_pio_doprinos['new_value'] = $employees_params['zdrav_pio_doprinos'];
                }
                
                M4EmployeeRowBehavior::addItemToDiffsGroup($diffs, [
                    'group_id' => $roll->id,
                    'group_name' => Yii::t('PaychecksModule.M4Diffs', 'PositionNumber') . ": {$roll->position_number}" . ", " . Yii::t('PaychecksModule.M4Diffs', 'RollNumber') . ": {$roll->roll_number}",
                ], [
                    'status' => 'MODIFY',
                    'columns' => [
                        Yii::t('PaychecksModule.M4Diffs', 'EmployeeName')=> $employee_name,
                        Yii::t('PaychecksModule.M4Diffs', 'JMBG')=> $jmbg,
                        Yii::t('PaychecksModule.M4Diffs', 'WorkExpMonths')=>$work_exp_months,
                        Yii::t('PaychecksModule.M4Diffs', 'WorkExpDays')=>$work_exp_days,
                        Yii::t('PaychecksModule.M4Diffs', 'Bruto')=>$bruto,
                        Yii::t('PaychecksModule.M4Diffs', 'PioDoprinos')=>$pio_doprinos,
                        Yii::t('PaychecksModule.M4Diffs', 'ZdravBruto')=>$zdrav_bruto,
                        Yii::t('PaychecksModule.M4Diffs', 'ZdravPioDoprinos')=>$zdrav_pio_doprinos
                    ],
                    'data' => [
                        'status' => 'MODIFY',
                        'model' => M4EmployeeRow::class,
                        'model_id' => $m4employee_row->id,
                        'attributes' => [
                            'm4mf_roll_id' => $roll->id,
                            'employee_id' => $m4employee_row->employee_id,
                            'employee_name' => $employees_params['employee_name'],
                            'jmbg' => $employees_params['jmbg'],
                            'work_exp_months' => $employees_params['work_exp_months'],
                            'work_exp_days' => $employees_params['work_exp_days'],
                            'bruto' => $employees_params['bruto'],
                            'pio_doprinos' => $employees_params['pio_doprinos'],
                            'zdrav_bruto' => $employees_params['zdrav_bruto'],
                            'zdrav_pio_doprinos' => $employees_params['zdrav_pio_doprinos']
                        ]
                    ]
                ]);
            }
            else
            {
                M4EmployeeRowBehavior::addItemToDiffsGroup($diffs, [
                    'group_id' => $roll->id,
                    'group_name' => Yii::t('PaychecksModule.M4Diffs', 'PositionNumber') . ": {$roll->position_number}" . ", " . Yii::t('PaychecksModule.M4Diffs', 'RollNumber') . ": {$roll->roll_number}",
                ], [
                    'status' => 'SAME',
                    'columns' => [
                        Yii::t('PaychecksModule.M4Diffs', 'EmployeeName') => [
                            'new_value' => $m4employee_row->employee_name
                        ],
                        Yii::t('PaychecksModule.M4Diffs', 'JMBG') => [
                            'new_value' => $m4employee_row->jmbg
                        ],
                        Yii::t('PaychecksModule.M4Diffs', 'WorkExpMonths') => [
                            'new_value' => $m4employee_row->work_exp_months
                        ],
                        Yii::t('PaychecksModule.M4Diffs', 'WorkExpDays') => [
                            'new_value' => $m4employee_row->work_exp_days
                        ],
                        Yii::t('PaychecksModule.M4Diffs', 'Bruto') => [
                            'new_value' => $m4employee_row->bruto
                        ],
                        Yii::t('PaychecksModule.M4Diffs', 'PioDoprinos') => [
                            'new_value' => $m4employee_row->pio_doprinos
                        ],
                        Yii::t('PaychecksModule.M4Diffs', 'ZdravBruto') => [
                            'new_value' => $m4employee_row->zdrav_bruto
                        ],
                        Yii::t('PaychecksModule.M4Diffs', 'ZdravPioDoprinos') => [
                            'new_value' => $m4employee_row->zdrav_pio_doprinos
                        ]
                    ],
                    'data' => []
                ]);
            }
            
            if (!empty($update_func))
            {
                $percent = round(($j/$m4employee_rows_cnt)*20, 0, PHP_ROUND_HALF_DOWN);
                call_user_func($update_func, 80 + $percent);
            }
            
            $j++;
        }
        
        return $diffs;
    }
    
    private static function hasM4EmployeeRowChanges($m4employee_row, $employees_params)
    {
        if (
                $m4employee_row->employee_name !== $employees_params['employee_name'] || 
                $m4employee_row->jmbg !== $employees_params['jmbg'] || 
                $m4employee_row->work_exp_months !== $employees_params['work_exp_months'] || 
                $m4employee_row->work_exp_days !== $employees_params['work_exp_days'] || 
                !SIMAMisc::areEqual($m4employee_row->bruto, $employees_params['bruto'], 0.01) || 
                !SIMAMisc::areEqual($m4employee_row->pio_doprinos, $employees_params['pio_doprinos'], 0.01) || 
                !SIMAMisc::areEqual($m4employee_row->zdrav_bruto, $employees_params['zdrav_bruto'], 0.01) || 
                !SIMAMisc::areEqual($m4employee_row->zdrav_pio_doprinos, $employees_params['zdrav_pio_doprinos'], 0.01)
           )
        {
            return true;
        }
        
        return false;
    }
    
    private static function addItemToDiffsGroup(&$diffs, $group, $item)
    {
        $added = false;
        foreach ($diffs as $key => $diff)
        {
            if (intval($diff['group_id']) === intval($group['group_id']))
            {
                if (!isset($diff['items']))
                {
                    $diffs[$key]['items'] = [];
                }
                
                array_push($diffs[$key]['items'], $item);
                $diffs[$key]['changes']++;
                
                $added = true;
            }
        }
        
        if ($added === false)
        {
            $diffs[] = [
                'type' => 'group',
                'group_id' => $group['group_id'],
                'group_name' => $group['group_name'],
                'columns' => [
                    Yii::t('PaychecksModule.M4EmployeeRow', 'EmployeeName'),
                    Yii::t('PaychecksModule.M4EmployeeRow', 'JMBG'),
                    Yii::t('PaychecksModule.M4EmployeeRow', 'WorkExpMonths'),
                    Yii::t('PaychecksModule.M4EmployeeRow', 'WorkExpDays'),
                    [Yii::t('PaychecksModule.M4EmployeeRow', 'Bruto'), 'align'=>'left'],
                    [Yii::t('PaychecksModule.M4EmployeeRow', 'PioDoprinos'), 'align'=>'left'],
                    [Yii::t('PaychecksModule.M4EmployeeRow', 'ZdravBruto'), 'align'=>'left'],
                    [Yii::t('PaychecksModule.M4EmployeeRow', 'ZdravPioDoprinos'), 'align'=>'left']
                ],
                'changes' => 1,
                'items' => [$item]
            ];
        }
    }

    private static function getRollsByEmployeesForYear($year_id)
    {
        $rolls_by_employees = [];
        $rolls = M4MFRoll::model()->findAll(new SIMADbCriteria([
            'Model' => M4MFRoll::class,
            'model_filter' => [
                'year' => [
                    'ids' => $year_id
                ],
                'order_by' => 'position_number ASC',
            ]
        ]));
        
        foreach ($rolls as $roll)
        {
            $rest_cnt = M4MFRoll::MAX_M4_ROLL_EMPLOYEES - $roll->m4employee_rows_cnt;
            if ($rest_cnt > 0)
            {
                $rolls_by_employees[$roll->id] = [
                    'cnt' => $rest_cnt,
                    'roll' => $roll
                ];
            }
        }

        return $rolls_by_employees;
    }
    
    private static function getNextFreeRoll(&$rolls_by_employees)
    {
        foreach ($rolls_by_employees as $key => $value)
        {
            if ($value['cnt'] > 0)
            {
                $rolls_by_employees[$key]['cnt']--;
                return $value['roll'];
            }
        }
        
        return null;
    }
}