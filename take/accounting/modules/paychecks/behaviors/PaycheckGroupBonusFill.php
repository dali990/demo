<?php

/**
 * @package SIMA
 * @subpackage Accounting
 * @subpackage Paycheck
 */
class PaycheckGroupBonusFill extends CActiveRecordBehavior
{
    private $group_bonus = null;   
    private $paycheck_period = null;   
    
    private function isForFill(Employee $empl)
    {
        $active_contract = $empl->active_work_contract;
        if (!is_null($active_contract))
        {
            $_last_day = Day::get('31', '12', '2020');
            $last_date = $active_contract->getEndDate($_last_day);
            return ($last_date == '31.12.2020.' && $empl->employment_code=='01');
        }
        return false;
    }
    
    public function fillBonuses($all=false)
    {
        $this->group_bonus = $this->owner;
        $this->paycheck_period = $this->group_bonus->paycheck_period;
        

//        $employees = Employee::model()->findAll();
        foreach ($this->paycheck_period->paychecks as $paycheck)
        {    
            $_bonus = PaycheckBonus::model()->findByModelFilter([
                'paycheck' => ['ids' => $paycheck->id],
                'group_bonus' => ['ids' => $this->group_bonus->id]
            ]);
            if ($all || $this->isForBonus($paycheck))
            {
                if (is_null($_bonus))
                {
                    $_bonus = new PaycheckBonus();
                    $_bonus->group_bonus_id = $this->group_bonus->id;
                    $_bonus->paycheck_id = $paycheck->id;
                    $_bonus->employee_id = $paycheck->employee_id;
                    $_bonus->paycheck_period_id = $this->paycheck_period->id;
                    $_bonus->paycheck = $paycheck;
                    $_bonus->name = $this->group_bonus->name;
                    $_bonus->value = 0;
                    $_bonus->save();
                }
            }
            else 
            {
                if (!is_null($_bonus))
                {
                    $_bonus->delete();
                }
            }
        }
    }
    public function fillCustomBonuses($employee_ids)
    {
        $this->group_bonus = $this->owner;
        $this->paycheck_period = $this->group_bonus->paycheck_period;
        

        $paychecks = Paycheck::model()->findAll([
            'model_filter' => [
                'employee' => ['ids' => $employee_ids],
                'paycheck_period' => ['ids' => $this->paycheck_period->id]
            ]
        ]);
        foreach ($paychecks as $paycheck)
        {    
            $_bonus = PaycheckBonus::model()->findByModelFilter([
                'paycheck' => ['ids' => $paycheck->id],
                'group_bonus' => ['ids' => $this->group_bonus->id]
            ]);

            if (is_null($_bonus))
            {
                $_bonus = new PaycheckBonus();
                $_bonus->group_bonus_id = $this->group_bonus->id;
                $_bonus->paycheck_id = $paycheck->id;
                $_bonus->employee_id = $paycheck->employee_id;
                $_bonus->paycheck_period_id = $this->paycheck_period->id;
                $_bonus->paycheck = $paycheck;
                $_bonus->name = $this->group_bonus->name;
                $_bonus->value = 0;
                $_bonus->save();
            }
        }
    }
    public function calcBonuses()
    {
        $this->group_bonus = $this->owner;
        $this->paycheck_period = $this->group_bonus->paycheck_period;
        switch ($this->group_bonus->group_bonus_type)
        {
            case PaycheckGroupBonus::$GROUP_BONUS_TYPE_EQUAL_AMOUNT:
                $this->fillGroupBonusEqual(false); break;
//            case PaycheckGroupBonus::$GROUP_BONUS_TYPE_EQUAL_SUM:
//                $this->fillGroupBonusEqual(true); break;
            case PaycheckGroupBonus::$GROUP_BONUS_TYPE_SALARY_POINTS:
                $this->fillGroupBonusBySalaryPoints(); break;
//            case PaycheckGroupBonus::$GROUP_BONUS_TYPE_SALARY_POINTS_SUM:
//                $this->fillGroupBonusBySalaryPointsSum(); break;
//            case PaycheckGroupBonus::$GROUP_BONUS_TYPE_BONUS_POINTS_SUM:
//                $this->fillGroupBonusByBonusPoints(); break;
            default:
                throw new SIMAWarnException('Nije implemnetiran nacin');
                break;
        }
    }
    
    private function sendUpdateEvent($percent)
    {
        if (Yii::app()->isWebApplication())
        {
            Yii::app()->controller->sendUpdateEvent($percent);
        }
        else 
        {
            echo "$percent% \n";
        }
    }
    
    private function updateBonusValue($paycheck, $_value)
    {
        $_bonus = PaycheckBonus::model()->findByModelFilter([
            'paycheck' => ['ids' => $paycheck->id],
            'group_bonus' => ['ids' => $this->group_bonus->id]
        ]);

        if (SIMAMisc::areEqual($_value, 0))
        {
            if (!is_null($_bonus))
            {
                $_bonus->delete();
            }
        }
        else
        {
            if (is_null($_bonus))
            {
                $_bonus = new PaycheckBonus();
                $_bonus->group_bonus_id = $this->group_bonus->id;
                $_bonus->paycheck_id = $paycheck->id;
                $_bonus->employee_id = $paycheck->employee_id;
                $_bonus->paycheck_period_id = $this->paycheck_period->id;
                $_bonus->paycheck = $paycheck;
            }
            $_bonus->name = $this->group_bonus->name;
            $_bonus->value = $_value;
            if ($_bonus->isNewRecord || $_bonus->columnChanged(['name','value']))
            {
                $_bonus->save();
            }
        }
    }
    
    private function isForBonus(Paycheck $paycheck)
    {
        $employee = $paycheck->employee;
        $active_contract = $employee->active_work_contract;
        if (!is_null($active_contract))
        {
            $_last_day = Day::get('31', '12', '2020');
            $last_date = $active_contract->getEndDate($_last_day);
            return ($last_date == '31.12.2020.' && $employee->employment_code=='01');

        }
        return false;
    }
    
//    private function fillGroupBonusBySalaryPointsSum()
//    {
//        $_cnt = count($this->paycheck_period->paychecks);
//        $_i = 0;
//        
//        $_real_cnt = 0;
//        $_sum_points = 0;
//        
//        foreach ($this->paycheck_period->paychecks as $paycheck)
//        {
//            if ($this->isForBonus($paycheck))
//            {
//                $_real_cnt++;
//                $_sum_points += $paycheck->points;
//            }
//        }
//        foreach ($this->paycheck_period->paychecks as $paycheck)
//        {
//            if (isset($paycheck->employee->bonus_paygrade))
//            {
//                $_sum_points += $paycheck->employee->bonus_paygrade->points;
//            }
//        }
//        
//        $_point_value = $this->group_bonus->value / $_sum_points;
//        
//        foreach ($this->paycheck_period->paychecks as $paycheck)
//        {            
//            
//            if ($this->isForBonus($paycheck))
//            {
//                $_value = $_point_value * $paycheck->points;
//            }
//            else
//            {
//                $_value = 0;
//            }
//            
//            if (isset($paycheck->employee->bonus_paygrade))
//            {
//                $_value += $_point_value * $paycheck->employee->bonus_paygrade->points;
//            }
//            
//            $this->updateBonusValue($paycheck, $_value);
//            $_i++;
//            $this->sendUpdateEvent(100*$_i/$_cnt);
//        }
//    }
    
    
    private function fillGroupBonusBySalaryPoints()
    {
        $_cnt = count($this->paycheck_period->paychecks);
        $_i = 0;

        $_point_value = $this->group_bonus->value;
        
        $_bonuses = PaycheckBonus::model()->findAllByAttributes([
            'group_bonus_id' => $this->owner->id
        ]);
        
        foreach ($_bonuses as $_bonus)
        {
            $_bonus->value = $_point_value * $_bonus->paycheck->points;
            $_bonus->save();
//            $this->updateBonusValue($paycheck, $_value);
            $_i++;
            $this->sendUpdateEvent(100*$_i/$_cnt);
        }
        
//        foreach ($this->paycheck_period->paychecks as $paycheck)
//        {        
//            if ($this->isForBonus($paycheck))
//            {
//                $_value = $_point_value * $paycheck->points;
//            }
//            else
//            {
//                $_value = 0;
//            }
//            
//            if (isset($paycheck->employee->bonus_paygrade))
//            {
//                $_value += $_point_value * $paycheck->employee->bonus_paygrade->points;
//            }
//            
//            $this->updateBonusValue($paycheck, $_value);
//            $_i++;
//            $this->sendUpdateEvent(100*$_i/$_cnt);
//        }
    }
    
    
    private function fillGroupBonusByBonusPoints()
    {
        $_cnt = count($this->paycheck_period->paychecks);
        $_i = 0;
        
        $_sum_points = 0;
        foreach ($this->paycheck_period->paychecks as $paycheck)
        {
            if (isset($paycheck->employee->bonus_paygrade))
            {
                $_sum_points += $paycheck->employee->bonus_paygrade->points;
            }
        }
        
        $_point_value = $this->group_bonus->value / $_sum_points;
        
        foreach ($this->paycheck_period->paychecks as $paycheck)
        {            
            $_value = 0;
            if (isset($paycheck->employee->bonus_paygrade))
            {
                $_value += $_point_value * $paycheck->employee->bonus_paygrade->points;
            }
             
            $this->updateBonusValue($paycheck, $_value);
            $_i++;
            $this->sendUpdateEvent(100*$_i/$_cnt);
        }
    }
    
    private function fillGroupBonusEqual($devide_by_count)
    {
        $_cnt = count($this->paycheck_period->paychecks);
        $_i = 0;
        
        $_devider = 1;
        if ($devide_by_count && $_cnt > 0)
        {
            $_devider = $_cnt;
        }
        $_value = $this->group_bonus->value / $_devider;
        $_bonuses = PaycheckBonus::model()->findAllByAttributes([
            'group_bonus_id' => $this->owner->id
        ]);
        
        foreach ($_bonuses as $_bonus)
        {
            $_bonus->value = $_value;
            $_bonus->save();
//            $this->updateBonusValue($paycheck, $_value);
            $_i++;
            $this->sendUpdateEvent(100*$_i/$_cnt);
        }
    }
    
}