<?php

/**
 * @package SIMA
 * @subpackage Paychecks
 */
class CompanyPaychecksBehavior extends SIMAActiveRecordBehavior
{
    public function getCompanyMunicipalityCode()
    {
        $company_municipality_id = Yii::app()->configManager->get('accounting.paychecks.company_municipality_id');
        $company_municipality = Municipality::model()->findByPkWithCheck($company_municipality_id);
        $code = $company_municipality->code;
        return $code;
    }
}