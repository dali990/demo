<?php

class SuspensionGroupTabs extends SIMAActiveRecordTabs
{
    protected function tabsDefinition()
    {
        $tabs = [
            array(
                'title' => Yii::t('PaychecksModule.Suspension', 'Suspensions'),
                'code'=>'suspensions'
            )
        ];
        
        return $tabs;
    }
}