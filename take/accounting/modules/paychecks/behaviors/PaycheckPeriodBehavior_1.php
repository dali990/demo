<?php

class PaycheckPeriodBehavior extends SIMAActiveRecordBehavior
{
    public function getActiveEmployeesWithoutPaychecks()
    {
        $result = [];
        
        $owner = $this->owner;
        
        $employees = $owner->employeesActiveInThisPeriod();
        
        foreach($employees as $employee)
        {
            $criteria = new SIMADbCriteria([
                'Model' => Paycheck::model(),
                'model_filter' => [
                    'is_valid' => true,
                    'paycheck_period' => [
                        'ids' => $owner->id
                    ],
                    'employee' => [
                        'ids' => $employee->id
                    ]
                ]
            ], true);
            
            if(empty($criteria->ids))
            {
                $result[] = $employee;
            }
        }
        
        return $result;
    }
    
    public function employeesActiveInThisPeriod()
    {
        $owner = $this->owner;
        
        $wcCriteria = new SIMADbCriteria([
            'Model' => 'WorkContract',
            'model_filter' => [
                'signed' => true,
                'for_payout' => true,
                'active_at' => $owner->month
            ]
        ]);
        $work_contracts = WorkContract::model()->with('employee')->findAll($wcCriteria);
        $work_contracts_count = count($work_contracts);
        if($work_contracts_count < 1)
        {
            $work_contracts_count = 1;
        }
        $employees = [];
        foreach($work_contracts as $wc)
        {
            if(!isset($employees[$wc->employee->id]))
            {
                $employees[$wc->employee->id] = $wc->employee;
            }
        }
        unset($work_contracts);
        unset($wcCriteria);
        
        return $employees;
    }
    
    public function generateBankExportFile($type, Bank $bank=null)
    {
        $owner = $this->owner;
        $isset_bank = false;
        if(!is_null($bank))
        {
           $isset_bank = true; 
        }
        
        $view_file = null;
        switch($type)
        {
            case UnifiedPaycheck::$TYPE_TXT:
                $view_file = 'bank_txt_export';
                break;
            case UnifiedPaycheck::$TYPE_PDF:
                $view_file = 'bank_pdf_export';
                break;
            default: 
                throw new Exception(Yii::t('PaychecksModule.UnifiedPaycheck', 'InvalidType', [
                    '{type}' => $type
                ]));
        }
        
        $model_filter = [
            'paycheck_period' => [
                'ids' => $owner->id
            ],
            'is_valid' => true,
        ];
        if($isset_bank)
        {
            $model_filter['employee_payout_type'] = Employee::$PAYOUT_TYPE_BANK_ACCOUNT;
            $model_filter['bank_account'] = [
                'bank' => [
                    'ids' => $bank->id
                ]
            ];
        }
        else
        {
            $model_filter['employee_payout_type'] = Employee::$PAYOUT_TYPE_CACHE;
        }
        $paychecksCriteria = new SIMADbCriteria([
            'Model' => Paycheck::model(),
            'model_filter' => $model_filter
        ]);
        $paychecks = Paycheck::model()->orderByOfficial()->findAll($paychecksCriteria);

        $unifiedPaycheck = null;
        if($isset_bank)
        {
            $unifiedPaycheckCriteria = new SIMADbCriteria([
                'Model' => UnifiedPaycheck::model(),
                'model_filter' => [
                    'bank' => [
                        'ids' => $bank->id
                    ]
                ]
            ]);
            $unifiedPaycheck = UnifiedPaycheck::model()->find($unifiedPaycheckCriteria);
        }
        $bank_export_htmlcode = Yii::app()->controller->renderModelView($owner, $view_file, [
            'params' => [
                'paychecks' => $paychecks,
                'unifiedPaycheck' => $unifiedPaycheck,
                'with_bank_account' => $isset_bank,
                'calc_date' => $owner->payment_date
            ]
        ]);
                        
        $bank_export_temporaryFile = null;
        switch($type)
        {
            case UnifiedPaycheck::$TYPE_PDF:
                $bank_export_temporaryHTMLFile = new TemporaryFile($bank_export_htmlcode, null);
                $bank_export_temporaryFile = $bank_export_temporaryHTMLFile->createTemporaryPdfFile();
                break;
            case UnifiedPaycheck::$TYPE_TXT:
                $bank_export_temporaryFile = new TemporaryFile($bank_export_htmlcode, null, 'txt');
                break;
            default: 
                throw new Exception(Yii::t('PaychecksModule.UnifiedPaycheck', 'InvalidType', [
                    '{type}' => $type
                ]));
        }
        
        return $bank_export_temporaryFile;
    }
    
    public function generateAllPaychecksExportFile()
    {
        $owner = $this->owner;       
        
        $paychecksCriteria = new SIMADbCriteria([
            'Model' => Paycheck::model(),
            'model_filter' => [
                'paycheck_period' => [
                    'ids' => $owner->id
                ],
                'is_valid' => true,
            ]
        ]);
        $paychecks = Paycheck::model()->orderByOfficial()->findAll($paychecksCriteria);

        $header_path = YiiBase::getPathOfAlias('webroot').'/images/header.png';
        
        $eventSourceStepsSum = 1;
        
        $paychecks_count = count($paychecks);
        if($paychecks_count < 1)
        {
            $paychecks_count = 1;
        }
        $eventSourceSteps = (90/$paychecks_count);
        
        $sum_neto = 0;
        $sum_bruto = 0;
        $sum_neto_for_payout = 0;
        $paychecks_html = '';
        foreach($paychecks as $paycheck)
        {
            $eventSourceStepsSum += $eventSourceSteps;
            Yii::app()->controller->sendUpdateEvent($eventSourceStepsSum);
            
            $_neto  = $paycheck->getPaychecksPeriodsSum('amount_neto');
            $_bruto = $paycheck->getPaychecksPeriodsSum('amount_bruto');
            $_neto_for_payout = $paycheck->getPaychecksPeriodsSum('amount_neto_for_payout');
            
            $paychecks_html .= '
            <tr>
                <td>'.$paycheck->paygrade_code.'</td> 
                <td>'.$paycheck->employee_display_name.'</td> 
                <td>'.$paycheck->employee->person->JMBG.'</td> 
                <td align="right"><b>'.number_format($_bruto, 2, ',', '.').'</b></td> 
                <td align="right"><b>'.number_format($_neto, 2, ',', '.').'</b></td> 
                <td align="right"><b>'.number_format($_neto_for_payout, 2, ',', '.').'</b></td> 
            </tr>
';
            
            $sum_neto += $_neto;
            $sum_bruto += $_bruto;
            $sum_neto_for_payout += $_neto_for_payout;
        }
        
        Yii::app()->controller->sendUpdateEvent(94);
        
        $bank_export_htmlcode = Yii::app()->controller->renderModelView($owner, 'all_paychecks_pdf_export', [
            'params' => [
                'month' => $owner->month,
                'header_path' => $header_path,
                'paychecks_html' => $paychecks_html,
                'sum_neto' => $sum_neto,
                'sum_bruto' => $sum_bruto,
                'sum_neto_for_payout' => $sum_neto_for_payout,
            ]
        ]);
        
        Yii::app()->controller->sendUpdateEvent(98);

        $bank_export_temporaryHTMLFile = new TemporaryFile($bank_export_htmlcode, null);
        $bank_export_temporaryFile = $bank_export_temporaryHTMLFile->createTemporaryPdfFile();

        
        return $bank_export_temporaryFile;
    }
    
    public function recalculate($with_additionals, $for_compare_without_save, $callback_func=null)
    {
        set_time_limit(10);
        
        $paycheck_period = $this->owner;
                
        $eventSourceStepsSum = 0.0;
        if(!is_null($callback_func))
        {
            $callback_func($eventSourceStepsSum);
        }
                        
        if($for_compare_without_save === true)
        {
            
        }
        else
        {
            $paycheck_period->last_calculation_start_time = SIMAHtml::UserToDbDate(date('Y-m-d H:i:s'), true);
            $paycheck_period->save();
        }
        
        $employees = PaycheckPeriodComponent::EmployeesActiveInPeriod($paycheck_period);
                
        $employeesCount = count($employees);
        if($employeesCount < 1)
        {
            $employeesCount = 1;
        }
        
        \SIMAMisc::increaseMemoryLimit(memory_get_usage(true) + ($employeesCount*400*1024)); /// 400KB po zaposlenom
        
        $error_messages = [];
        $raise_note_messages = [];
                
        $paychecks_for_compare = [];
        $recalculated_paycheck_ids = [];
                                
        $eventSourceSteps = (88/$employeesCount);

        $lastUsedOrderNumber = new SIMALocalCounter();
        $max_execution_time = ini_get('max_execution_time');
        foreach ($employees as $employee)
        {
            set_time_limit(9);
            
            $employee_error_messages = $paycheck_period->getErrorBeforePaycheckForEmployee($employee);
            if(isset($employee_error_messages) && count($employee_error_messages)>0)
            {
                $raise_note_messages = array_merge($raise_note_messages, $employee_error_messages);
                continue;
            }

            $eventSourceStepsSum += $eventSourceSteps;
            if(!is_null($callback_func))
            {
                $callback_func($eventSourceStepsSum);
            }
            
            try
            {
                $create_if_not_exists = true;
                if(isset(Yii::app()->params['RecalcPeriodsCommand_keep_some_old_paycheck_data']) && Yii::app()->params['RecalcPeriodsCommand_keep_some_old_paycheck_data'] === true)
                {
                    $create_if_not_exists = false;
                }
                
                $employeePaycheck = $paycheck_period->getPaycheckForEmployeeInPeriod($employee, $create_if_not_exists);
                
                if(
                        is_null($employeePaycheck)
                        && isset(Yii::app()->params['RecalcPeriodsCommand_keep_some_old_paycheck_data']) 
                        && Yii::app()->params['RecalcPeriodsCommand_keep_some_old_paycheck_data'] === true
                        )
                {
                    continue;
                }

                $employeePaycheck->recalculate($lastUsedOrderNumber, $with_additionals);

                $recalculated_paycheck_ids[$employeePaycheck->id] = $employeePaycheck->id;

                if($for_compare_without_save === true)
                {
                    $paychecks_for_compare[] = $employeePaycheck;
                }
                else
                {
                    $employeePaycheck->save();
                    if (isset($employeePaycheck->file))
                    {
                        $employeePaycheck->file->appendShellVersion();
                    }
                    unset($employeePaycheck);
                }                
            }
            catch (SIMAWarnPaycheckCalculationFailed $warn)
            {
                $raise_note_messages[] = $warn->getMessage();
            }
            catch (Exception $ex) 
            {
                $error_messages[] = $ex->getMessage();
            }
        }
        set_time_limit($max_execution_time);
        
        if(count($error_messages) > 0)
        {
            $imploded_messages = implode('</br>', $error_messages);

            $error_message = 'Doslo je do greske:</br>'.$imploded_messages;

            throw new SIMAExceptionPaycheckPeriodCalculation($error_message);
        }
//        /// ostvljeno radi debug-ovanja prilikom re-obracuna
//        error_log(__METHOD__.' - $error_messages: '.SIMAMisc::toTypeAndJsonString($error_messages));
        
        $paychecksForDevalidationCriteria = new SIMADbCriteria([
            'Model' => Paycheck::model(),
            'model_filter' => [
                'paycheck_period' => [
                    'ids' => $paycheck_period->id
                ]
            ]
        ]);
        $paychecksForDevalidation = Paycheck::model()->findAll($paychecksForDevalidationCriteria);
        $paychecksForDevalidationCount = count($paychecksForDevalidation);
        if($paychecksForDevalidationCount<1)
        {
            $paychecksForDevalidationCount = 1;
        }
        $eventSourceSteps = (2/$paychecksForDevalidationCount);
        foreach($paychecksForDevalidation as $paycheck)
        {
            if(!isset($recalculated_paycheck_ids[$paycheck->id]))
            {
                if($for_compare_without_save === true)
                {
                    $paychecks_for_compare[] = $paycheck;
                }
                else
                {
                    $paycheck->delete();
                }
            }
            
            $eventSourceStepsSum += $eventSourceSteps;
//            Yii::app()->controller->sendUpdateEvent($eventSourceStepsSum);
            if(!is_null($callback_func))
            {
                $callback_func($eventSourceStepsSum);
            }
        }
        
        if(!is_null($callback_func))
        {
            $callback_func(100);
        }

        $end_event_params = [];
        if($for_compare_without_save === true)
        {
            $end_event_params = $paychecks_for_compare;
        }
        else
        {
            $paycheck_period->last_calculation_end_time = SIMAHtml::UserToDbDate(date('Y-m-d H:i:s'), true);
            $paycheck_period->save();
        }
        
        if(!empty($raise_note_messages))
        {
            $raise_note_messages_imploded = implode('</br>', $raise_note_messages);

            $raise_note_message = Yii::t('PaychecksModule.PaycheckPeriod', 'CouldNotRecalculatePaychecksForEmployees', [
                '{error_messages}' => $raise_note_messages_imploded
            ]);

            throw new SIMAWarnPaycheckPeriodCalculationNote($raise_note_message);
        }
        
        return $end_event_params;
    }
    
    public function getErrorBeforePaycheckForEmployee(Employee $employee)
    {
//        $error_messages = array();
        $error_messages = EmployeeComponent::employeeInsufficientData($employee, false);
        
//        if (!isset($employee->employment_code))
//        {
//            $error_messages[] = 'Zaposlenom "' . $employee . '" nije zadat kod zaposlenja';
//        }
//        if (!isset($employee->paycheck_svp))
//        {
//            $error_messages[] = 'Zaposlenom "' . $employee . '" nije zadata sifra vrste placanja';
//        }
//        
//        if (!isset($employee->municipality))
//        {
//            $error_messages[] = 'Zaposlenom "' . $employee . '" nije zadata opstina';
//        }
//        elseif (!isset($employee->municipality->code) || empty($employee->municipality->code))
//        {
//            $error_messages[] = 'Opstini "' . $employee->municipality . '" nije zadata sifra';
//        }
//        
        if (!isset($employee->last_work_contract))
        {
            $error_messages[] = 'Zaposlenom "' . $employee . '" nije zadat ni jedan ugovor';
        }
        else if (!isset($employee->last_work_contract->work_position))
        {
            $error_messages[] = 'Zaposlenom "' . $employee . '" nije ugovorom zadata radna pozicija';
        }
        else if (!isset( $employee->last_work_contract->work_position->paygrade))
        {
            $error_messages[] = 'Poziciji "' . $employee->last_work_contract->work_position . '" nije zadat platni razred';
        }
//        
//        if(!isset($employee->person->main_bank_account))
//        {
//            $error_messages[] = 'Zaposlenom "' . $employee . '" nije zadat primarni bankovni racun';
//        }
//        
//        if(!isset($employee->person->main_address))
//        {
//            $error_messages[] = 'Zaposlenom "' . $employee . '" nije zadata glavna adresa';
//        }
//        
//        if(!isset($employee->person->JMBG) || empty($employee->person->JMBG))
//        {
//            $error_messages[] = 'Zaposlenom "' . $employee . '" nije zadat JMBG';
//        }
        return $error_messages;
    }
    
    public function getPaycheckForEmployeeInPeriod(Employee $employee, $create_if_not_exists=true)
    {
        $paycheckPeriod = $this->owner;
        
        $paycheckCriteria = new SIMADbCriteria([
            'Model' => 'Paycheck',
            'model_filter' => [
                'paycheck_period' => array(
                    'ids' => $paycheckPeriod->id
                ),
                'employee' => array(
                    'ids' => $employee->id
                )
            ]
        ]);
        
        $paycheck = Paycheck::model()->with('employee.work_contracts', 'additionals')->find($paycheckCriteria);
        if(!isset($paycheck) && $create_if_not_exists)
        {
            $paycheck = new Paycheck();
            $paycheck->employee_id = $employee->id;
            $paycheck->paycheck_period_id = $paycheckPeriod->id;
            $paycheck->worked_hours = 0;
            $paycheck->worked_hours_value = 0;
            $paycheck->past_exp_value = 0;
            $paycheck->save();
            $paycheck->refresh();
        }
        
        unset($paycheckCriteria);
                
        return $paycheck;
    }
    
    public function getCodebook()
    {
        $owner = $this->owner;
        
        $paycheck_codebook = PaycheckCodebook::model()->find([
            'model_filter' => [
//                'between_daterange' => $owner->month
                'between_daterange' => $owner->payment_date
            ]
        ]);
        
//        if(is_null($paycheck_codebook) && PaychecksCodebookComponent::useNewCodebooks())
        if(is_null($paycheck_codebook))
        {
            throw new Exception('not found codebook for paycheck period '.$owner->DisplayName);
        }
        
        return $paycheck_codebook;
    }
}
