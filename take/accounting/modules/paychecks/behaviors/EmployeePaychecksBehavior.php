<?php

/**
 * @package SIMA
 * @subpackage Accounting
 */
class EmployeePaychecksBehavior extends SIMAActiveRecordBehavior
{
    
    public function relations($event)
    {
        $event->addResult([
            'paychecks'=>array(SIMAActiveRecord::HAS_MANY, 'Paycheck', 'employee_id'),
            'suspensions' => [SIMAActiveRecord::HAS_MANY, 'Suspension', 'employee_id']
        ]);
    }
    
    public function getWorkOnStateHolidayHours(Day $this_period_first_day, Day $this_period_last_day)
    {
        $result = 0;
        
        $owner = $this->owner;
        
        $days_to_check = [];
        
        /// racunaj drzavne praznike
        $criteria = new SIMADbCriteria([
            'Model' => NationalEventStateHolidayToDay::class,
            'model_filter' => [
                'on_date' => [$this_period_first_day, $this_period_last_day]
            ]
        ]);
        $nationalEventsStateHolidayToDay = NationalEventStateHolidayToDay::model()->findAll($criteria);
        foreach($nationalEventsStateHolidayToDay as $nationalEventStateHolidayToDay)
        {
            $day = $nationalEventStateHolidayToDay->day;
            if(!isset($days_to_check[$day->id]))
            {
                $days_to_check[$day->id] = $day;
            }
        }
        
        /// racunaj verske praznike
        $criteria = new SIMADbCriteria([
            'Model' => NationalEventReligiousHolidayToDay::class,
            'model_filter' => [
                'on_date' => [$this_period_first_day, $this_period_last_day]
            ]
        ]);
        $nationalEventsReligiousHolidayToDay = NationalEventReligiousHolidayToDay::model()->findAll($criteria);
        foreach($nationalEventsReligiousHolidayToDay as $nationalEventReligiousHolidayToDay)
        {
            $day = $nationalEventReligiousHolidayToDay->day;
            if(!isset($days_to_check[$day->id]))
            {
                $days_to_check[$day->id] = $day;
            }
        }
        
        foreach($days_to_check as $day_to_check)
        {
            $classes_for_activity = [
                TaskReport::class, 
                Meeting::class
            ];
            $activityHoursTaskReports = Yii::app()->activityManager->getPersonActivityHoursInDay($owner->person, $day_to_check, null, $classes_for_activity);
            $result += $activityHoursTaskReports;
        }
        
        return $result;
    }
}