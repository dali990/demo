<?php

/**
 * @package SIMA
 * @subpackage Paychecks
 */
class FilePaychecksBehavior extends SIMAActiveRecordBehavior
{
    public function relations($event)
    {
        $event->addResult(array(
            'regular_paycheck_period'=>array(SIMAActiveRecord::HAS_ONE, 'PaycheckPeriod', 'regular_booking_file_id'),
            'paycheck'=>array(SIMAActiveRecord::HAS_ONE, 'Paycheck', 'file_id'),
            'taxes_paid_confirmation'=>array(SIMAActiveRecord::HAS_ONE, 'TaxesPaidConfirmation', 'id')
        ));
    }
}