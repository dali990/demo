<?php

/**
 * @package SIMA
 * @subpackage Accounting
 * @subpackage Paycheck
 */
class PaycheckCalculations extends CActiveRecordBehavior
{
    private $additionals_recalculated = false;
    private $suspensions_recalculated = false;
    private $suspensions_to_delete = [];
    private $xmls_recalculated = false;
    private $_additionals_for_ignore = [];
    
    public function getadditionals_for_ignore()
    {
        if (empty($this->_additionals_for_ignore))
        {
            $this->_additionals_for_ignore = [
                PaycheckAdditional::$TYPE_SICK_LEAVE_OVER_30,
                PaycheckAdditional::$TYPE_SICK_LEAVE_PREGNANCY_OVER_30,
                PaycheckAdditional::$TYPE_SICK_LEAVE_MATERNITY,
            ];
        }
        return $this->_additionals_for_ignore;
    }
    


    public static function isTufeFix()
    {
        if(isset(Yii::app()->params['RecalcPeriodsCommand_explicit_use_tufe_fix']) && Yii::app()->params['RecalcPeriodsCommand_explicit_use_tufe_fix'] === true)
        {
            return true;
        }
        return Yii::app()->configManager->get('accounting.paychecks.tufe_fix')=='true';
    }
    
    /**
     * Preracunava zaradu
     * @param SIMALocalCounter $lastUsedOrderNumber Redni broj u XML-u koji je poslednji koriscen. Broj se povecava ukoliko se XML napravi
     * @param boolean $withAdditionals - da li da preracunava i dodatke ili da ih uzima takve kakvi su
     * @param booelan $save - da li da sacuva zaradu, ili samo da izmeni objekat
     */
    public function recalculate(SIMALocalCounter $lastUsedOrderNumber, $withAdditionals = true, $save = false)
    {
        $paycheck = $this->owner;
        $paycheckPeriod = $paycheck->paycheck_period;
        $paycheckPeriodMonth = $paycheckPeriod->month;
        
        if(intval($paycheckPeriod->month_hours) === 0)
        {
            throw new Exception('Broj sati u mesecu ne sme biti 0!');
        }

        $employeesDatas = PaychecksEmployee::getEmployeesDataInPaycheckPeriod($paycheckPeriod, $paycheck->employee);
        
        $employeeData = $employeesDatas[$paycheck->employee->id];
        
        /// ako se radi uporedjivanje, postavlja podatke na ono sto je imala plata u tom (nekada) trenutku
        $first_work_contract = $employeeData['work_contracts'][0];
        $paygrade = $old_paygrade_points = $first_work_contract->work_position->paygrade;
        $old_paygrade_points = $first_work_contract->work_position->paygrade->points;
        $employee = $paycheck->employee;
        $old_personal_points = $employee->personal_points;
        $old_employment_code = $employee->employment_code;
        $old_is_probationer = $first_work_contract->is_probationer;
        if(isset(Yii::app()->params['RecalcPeriodsCommand_keep_some_old_paycheck_data']) && Yii::app()->params['RecalcPeriodsCommand_keep_some_old_paycheck_data'] === true)
        {
            $paygrade->points = $paycheck->work_position_points;
            $paygrade->save();
            
            $employee->personal_points = $paycheck->personal_points;
            
            if($paycheck->is_retiree)
            {
                $employee->employment_code = '09';
            }
            else
            {
                $employee->employment_code = '01';
            }
            
            if($paycheck->probationer_percent !== 100)
            {
                $first_work_contract->is_probationer = true;
            }
            
            $employee->save();
        }

        if($paycheckPeriod->without_additionals === true)
        {
            $paycheck->additionals = [];
            $this->additionals_recalculated = true;
            if ($withAdditionals)
            {
                Yii::app()->raiseNote('Dodaci u zaradi nisu obracunati posto je za isplatu zarade postavljeno da se radi bez dodataka');
            }
        }
        else
        {
            if ($withAdditionals)
            {
                $paycheck->additionals = PaycheckCalculation::recalculateAdditionalsForPaycheck(
                        $paycheck, 
                        $employeeData,
                        $withAdditionals, 
                        $paycheckPeriod->tax_release_used, 
                        $paycheckPeriodMonth
                );
                $this->additionals_recalculated = true;
                
                $_suspensions_pair = PaycheckCalculation::recalculateSuspensions(
                        $paycheck, 
                        $employeeData, 
                        $paycheckPeriodMonth
                );
                $paycheck->suspensions = $_suspensions_pair['new'];
                $this->suspensions_to_delete = $_suspensions_pair['delete'];
                $this->suspensions_recalculated = true;
            }
        }
                
        PaycheckCalculation::recalculatePaycheckNEW(
                $paycheck, 
                $paycheckPeriod,
                $paycheckPeriodMonth,
                $employeeData
        );
            
        /// ako se radi uporedjivanje, postavlja podatke na ono sto je imala plata u tom (nekada) trenutku
        if(isset(Yii::app()->params['RecalcPeriodsCommand_keep_some_old_paycheck_data']) && Yii::app()->params['RecalcPeriodsCommand_keep_some_old_paycheck_data'] === true)
        {
            $paygrade->points = $old_paygrade_points;
            $paygrade->save();
            
            $employee->personal_points = $old_personal_points;
            $employee->employment_code = $old_employment_code;
            $employee->save();
            
            $first_work_contract->is_probationer = $old_is_probationer;
            $first_work_contract->save();
        }
        
        $paycheck->paycheck_xmls = $this->recalculateXMLs($lastUsedOrderNumber);
        $this->xmls_recalculated = true;
    }
    
    public function afterSave($event)
    {
        parent::afterSave($event);
        if ($this->additionals_recalculated)
        {
            PaycheckAdditional::model()->deleteAllByAttributes(['paycheck_id'=>$this->owner->id]);
            foreach ($this->owner->additionals as $additional)
            {
                $additional->save();
            }
        }
        if ($this->suspensions_recalculated)
        {
            //TODO(14.6.2017): MilosS -> zakomentarisano jer se preslo na sistem da za svaku administrativnu zabranu 
            //korisnik moze da izmeni visinu, tako da ona uvek ostaje
//            $_olds = PaycheckSuspension::model()->findAllByAttributes(['paycheck_id'=>$this->owner->id]);
//            foreach ($_olds as $ps)
//            {
//                if($ps->suspension->payment_type != Suspension::$TYPE_ARBITRARLY)
//                {
//                    $ps->delete();
//                }
//            }
            foreach ($this->suspensions_to_delete as $_to_delete) 
            {
                $_to_delete->delete();
            }
            foreach ($this->owner->suspensions as $additional)
            {
                $additional->save();
            }
        }
        if ($this->xmls_recalculated)
        {
            PaycheckXML::model()->deleteAllByAttributes(['paycheck_id'=>$this->owner->id]);
            foreach ($this->owner->paycheck_xmls as $xml)
            {
                $xml->save();
            }
        }
    }
    
    /**
     * Pravljenje XML-eva za zaradu - IDENTIFIKATOR JE UVEK i FIKSIRAN JMBG
     * @param SIMALocalCounter $lastUsedOrderNumber prenosi se po referenci | poslednji iskoriscen orderNum
     * @return array niz PaycheckXML elemenata koji su novoobracunati
     */
    private function recalculateXMLs(SIMALocalCounter $lastUsedOrderNumber)
    {
        $paycheck = $this->owner;
        $_isBonus = $paycheck->paycheck_period->isBonus;
        $_month_hours = $paycheck->month_period_hours; //sati u mesecu. Ako se neki dodatak igonorise, onda se ignorise i ovde
        
        //osnovni base zarade
        if ($paycheck->base_bruto > 0 || $paycheck->base_doprinosi_base > 0)
        {
            $_xml_values = [
                $paycheck->employee_svp => [
                    'worked_hours' => $paycheck->worked_hours,
                    'number_of_calendar_days' => $paycheck->number_of_calendar_days,
                    'bruto' => $paycheck->base_bruto,
                    'tax_base' => $paycheck->base_tax_base,
                    'tax_empl' => $paycheck->base_tax_empl,
                    'doprinosi_base' => $paycheck->base_doprinosi_base,
                    'pio_empl' => $paycheck->base_pio_empl,
                    'pio_comp' => $paycheck->base_pio_comp,
                    'zdr_empl' => $paycheck->base_zdr_empl,
                    'zdr_comp' => $paycheck->base_zdr_comp,
                    'nez_empl' => $paycheck->base_nez_empl,
                    'nez_comp' => $paycheck->base_nez_comp,
                    'pio_ben' => 0             
                ]
            ];
        }
        else
        {
            $_xml_values = [];
        }
        
        foreach($paycheck->additionals as $additional)
        {
            if (!in_array($additional->type, $this->additionals_for_ignore))
            {
                if (!isset($_xml_values[$additional->svp]))
                {
                    $_xml_values[$additional->svp] = [
                        'worked_hours' => 0,
                        'number_of_calendar_days' => 0,
                        'bruto' => 0,
                        'tax_base' => 0,
                        'tax_empl' => 0,
                        'doprinosi_base' => 0,
                        'pio_empl' => 0,
                        'pio_comp' => 0,
                        'zdr_empl' => 0,
                        'zdr_comp' => 0,
                        'nez_empl' => 0,
                        'nez_comp' => 0,
                        'pio_ben' => 0      
                    ];
                }

                $_xml_values[$additional->svp]['worked_hours'] += $additional->hours;
                $_xml_values[$additional->svp]['number_of_calendar_days'] += $additional->number_of_calendar_days;
                $_xml_values[$additional->svp]['bruto'] += $additional->bruto;
                $_xml_values[$additional->svp]['tax_base'] += $additional->bruto - $additional->tax_release;
                $_xml_values[$additional->svp]['tax_empl'] += $additional->tax_empl;
                $_xml_values[$additional->svp]['doprinosi_base'] += $additional->doprinosi_base;
                $_xml_values[$additional->svp]['pio_empl'] += $additional->pio_empl;
                $_xml_values[$additional->svp]['pio_comp'] += $additional->pio_comp;
                $_xml_values[$additional->svp]['zdr_empl'] += $additional->zdr_empl;
                $_xml_values[$additional->svp]['zdr_comp'] += $additional->zdr_comp;
                $_xml_values[$additional->svp]['nez_empl'] += $additional->nez_empl;
                $_xml_values[$additional->svp]['nez_comp'] += $additional->nez_comp;
                $_xml_values[$additional->svp]['pio_ben'] += 0;
            }
            else
            {
                //ako se ignorise, onda se ignore i ukupno
//                $_month_hours -= $additional->hours;
            }
        }
        
        $xmls = [];
        
        foreach ($_xml_values as $_xml_svp => $_xml_array)
        {
            $_new_xml = new PaycheckXML();
            $_new_xml->paycheck_id = $paycheck->id;
            $_new_xml->paycheck = $paycheck;
            $_new_xml->svp = $_xml_svp; //osnovni SVP
            $_new_xml->order_number = $lastUsedOrderNumber->getLastValue();
            $_new_xml->indentification_type = $paycheck->employee->payout_identification_type;
            $_new_xml->indentification_value = $paycheck->employee->payout_identification_value;
            $_new_xml->lastname = SIMAMisc::RemoveSerbianLetters(trim($paycheck->employee->person->lastname));
            $_new_xml->firstname = SIMAMisc::RemoveSerbianLetters(trim($paycheck->employee->person->firstname));
            $_new_xml->municipality_code = $paycheck->employee->municipality->code;
            $_new_xml->month_hours = $_month_hours;
            
            $_new_xml->number_of_calendar_days = $_xml_array['number_of_calendar_days'];
            $_new_xml->effective_hours = $_xml_array['worked_hours'];
            $_new_xml->bruto           = $_xml_array['bruto'];
            $_new_xml->tax_base        = $_xml_array['tax_base'];
            $_new_xml->tax_empl        = $_xml_array['tax_empl'];
            $_new_xml->doprinosi_base  = $_xml_array['doprinosi_base'];
            $_new_xml->pio             = round($_xml_array['pio_empl'],2) + round($_xml_array['pio_comp'],2);
            $_new_xml->zdr             = round($_xml_array['zdr_empl'],2) + round($_xml_array['zdr_comp'],2);
            $_new_xml->nez             = round($_xml_array['nez_empl'],2) + round($_xml_array['nez_comp'],2);
            $_new_xml->pio_ben         = $_xml_array['pio_ben'];
            
            $crit = new SIMADbCriteria([
                'Model' => 'PaycheckXML',
                'model_filter' => [
                    'svp' => $_xml_svp,
                    'paycheck' => [
                        'employee' => ['ids' => $paycheck->employee_id],
                        'paycheck_period' => [
                            'month' => [
                                'ids' => $paycheck->paycheck_period->month->id
                            ],
                            'order_num_lower_than' => $paycheck->paycheck_period->ordering_number
                        ],

                    ]
                ]
            ]);
            $previous_xmls_same_svp = PaycheckXML::model()->findAll($crit);
        
            $_new_xml->mfp1 = 0; //tax_release_used_in_previous_paychecks_with_same_svp
            $_new_xml->mfp2 = 0; //doprinosi_base_in_previous_paychecks_with_same_svp
            if (PaycheckCalculations::isTufeFix()) //procnenat radnog vremena
            {
                $_new_xml->mfp3 = ($paycheck->additionals_hours + $paycheck->worked_hours)/$paycheck->month_period_hours * 100;
            }
            else
            {
                $_new_xml->mfp3 = $paycheck->working_percent;
            } 
            if (abs($_new_xml->mfp3 - 100) < 0.001 ) $_new_xml->mfp3 = 0;//da se ne prikazuje
            $_new_xml->mfp4 = 0; //bruto_in_previous_paychecks_with_same_svp
            $_new_xml->mfp5 = 0; //TODO: igre na srecu
            $_new_xml->mfp6 = 0; //TODO: dobrovoljno zdravstveno, dobrovoljni penzijski fond
            $_new_xml->mfp7 = 0; //TODO: nije rezident ili van radnog odnosa
            ////nepuno radno vreme samo kod jednog poslodavca
            $_new_xml->mfp8 = (($paycheck->working_percent < 100) && ($paycheck->working_percent_at_other_companies == 0))?1:0; 
            $_new_xml->mfp9 = 0; //TODO: najvisa mesecna osnovica
            $_new_xml->mfp10 = (count($_xml_values)>1)?1:0; //vise XML polja
            $_new_xml->mfp11 = $paycheck->prev_bruto_sum - $paycheck->prev_tax_base;
            $_new_xml->mfp12 = $paycheck->prev_doprinosi_base;
            foreach ($previous_xmls_same_svp as $prev_xml_same_svp)
            {
                if (!$_isBonus)
                {
                    $_new_xml->bruto -= $prev_xml_same_svp->bruto;
                    $_new_xml->tax_base -= $prev_xml_same_svp->tax_base;
                    $_new_xml->tax_empl -= $prev_xml_same_svp->tax_empl;
                    $_new_xml->doprinosi_base -= $prev_xml_same_svp->doprinosi_base;
                    $_new_xml->pio -= $prev_xml_same_svp->pio;
                    $_new_xml->zdr -= $prev_xml_same_svp->zdr;
                    $_new_xml->nez -= $prev_xml_same_svp->nez;
                    $_new_xml->pio_ben -= $prev_xml_same_svp->pio_ben;
                }
                
                
                
                $_new_xml->mfp1 += $prev_xml_same_svp->bruto - $prev_xml_same_svp->tax_base;
                $_new_xml->mfp2 += $prev_xml_same_svp->doprinosi_base;
                $_new_xml->mfp4 += $prev_xml_same_svp->bruto;
            }
            //specijalno za polje 12
            if($paycheck->paycheck_period->isFirstPaycheckPeriodInMonth())
            {
                $_new_xml->mfp12 = 0;//ovo mozda i ne mora jer ako je prvi period, onda i nema prethodne XML
                if($_new_xml->mfp10 == 1)
                {
                    //ovaj for je u istom ovakvom foru, voditi acuna o nazivu 
                    foreach ($_xml_values as $_local_xml_svp => $_local_xml_array)
                    {
                        if ($_xml_svp !== $_local_xml_svp)
                        {
                            $_new_xml->mfp12 += $_local_xml_array['doprinosi_base'];
                        }
                    }
                }
            }
            
            
            $xmls[] = $_new_xml;
        }
        
        return $xmls;
    }
    
    /**
     * iz niza ->additionals popuniti vrednosti promenljivih additionals
     * poziva se dva puta, jednom zarad sati, jednom zarad doprinosa i poreza (posle poravnanja - levelDoprinosiBase)
     */
    public function populateAdditionals()
    {
        $paycheck = $this->owner;
        
        $additionals_sum = 0;
        $additionals_hours = 0;
        $additionals_calendar_days = 0;
        $additionals_used_tax_release = 0;
        $additionals_tax_empl = 0;
        $additionals_doprinosi_base = 0;
        $additionals_pio_empl = 0;
        $additionals_zdr_empl = 0;
        $additionals_nez_empl = 0;
        $additionals_pio_comp = 0;
        $additionals_zdr_comp = 0;
        $additionals_nez_comp = 0;
        $_ignored_hours_from_additionals = 0;
        $_ignored_calendar_days_from_additionals = 0;
        
        
        foreach($paycheck->additionals as $additional)
        {
            if (!in_array($additional->type, $this->additionals_for_ignore))
            {
                //ne mora da se zaokruzije na 2 decimale jer se zaokruzuje u obracunu dodatka
                $additionals_sum                += $additional->bruto;
                $additionals_hours              += $additional->hours;
                $additionals_calendar_days      += $additional->number_of_calendar_days;
                $additionals_used_tax_release   += $additional->tax_release;
                $additionals_doprinosi_base     += $additional->doprinosi_base;
                $additionals_tax_empl           += $additional->tax_empl;
                $additionals_pio_empl           += $additional->pio_empl;
                $additionals_zdr_empl           += $additional->zdr_empl;
                $additionals_nez_empl           += $additional->nez_empl;
                $additionals_pio_comp           += $additional->pio_comp;
                $additionals_zdr_comp           += $additional->zdr_comp;
                $additionals_nez_comp           += $additional->nez_comp;
            }
            else
            {
                $_ignored_hours_from_additionals            += $additional->hours;
                $_ignored_calendar_days_from_additionals    += $additional->number_of_calendar_days;
            }
        }
        
        $paycheck->additionals_ignored_hours            = $_ignored_hours_from_additionals;
        $paycheck->additionals_ignored_calendar_days    = $_ignored_calendar_days_from_additionals;
        $paycheck->additionals_sum                      = $additionals_sum;
        $paycheck->additionals_hours                    = $additionals_hours; //treba
        $paycheck->additionals_calendar_days            = $additionals_calendar_days; //treba
        $paycheck->additionals_used_tax_release         = $additionals_used_tax_release;
        $paycheck->additionals_tax_empl                 = $additionals_tax_empl;
        $paycheck->additionals_doprinosi_base           = $additionals_doprinosi_base;
        $paycheck->additionals_pio_empl                 = $additionals_pio_empl;
        $paycheck->additionals_zdr_empl                 = $additionals_zdr_empl;
        $paycheck->additionals_nez_empl                 = $additionals_nez_empl;
        $paycheck->additionals_pio_comp                 = $additionals_pio_comp;
        $paycheck->additionals_zdr_comp                 = $additionals_zdr_comp;
        $paycheck->additionals_nez_comp                 = $additionals_nez_comp;
        
        
        //LEVEL 
        
        //TODO: da li ima smisla unsetovti float??
//        unset($additionals_hours);
//        unset($additionals_sum);
        
    }
    
    /**
     * leveluje osnovicu za doprinose po svim XML-ovima
     * svaki deo (svi additionals i base) moraju da imaju osnovicu u iznosu minimum bruto, a maksimum -> minimalna osnovica, minus osnovice u ostalim delovima
     * OBAVEZNO - visak ostaje gde je i napravljen, ne moze da se obracuna u nekom drugom delu
     * Prvo se visak rasporedi medju istim SVP (jer su u XML to ista stvar), pa se onda rasporedjuje izmedju razlicitih SVP-ova
     */
    public function levelDoprinosiBase()
    {
        $by_min_or_max = null;
        
        $paycheck = $this->owner;
        
        $this->levelDoprinosiBasePure(true); //same SVP
        $this->levelDoprinosiBasePure(false); //diff SVP
        
        //KORAK 4: prolazi kroz sve dodatke podize osnovicu za doprinose do bruto iznosa
        if ($paycheck->base_doprinosi_base < $paycheck->base_bruto)
        {
//            $is_by_min_doprinosi_base = round($paycheck->base_bruto - $paycheck->base_doprinosi_base, 2) < 0 : 'MIN';
            $by_min_or_max = round($paycheck->base_bruto - $paycheck->base_doprinosi_base, 2) < 0 ? 'MIN' : NULL;
            $paycheck->base_doprinosi_base = $paycheck->base_bruto;
            
            if(PaychecksCodebookComponent::useMaxTaxBase())
            {
                /**
                 * MilosJ:
                 * ovo nije mesto za ovakvo postavljanje jer ne uzima u obzir dodatke
                 * koji su level-ovani prethodnim pozivima levelDoprinosiBasePure
                 */
//                if($paycheck->base_doprinosi_base < $paycheck->paycheck_period->codebook->min_contribution_base)
//                {
//                    $paycheck->base_doprinosi_base = $paycheck->paycheck_period->codebook->min_contribution_base;
//                    $by_min_or_max = 'MIN';
//                }
//                else 
                if($paycheck->base_doprinosi_base > $paycheck->paycheck_period->codebook->max_contribution_base)
                {
                    $paycheck->base_doprinosi_base = $paycheck->paycheck_period->codebook->max_contribution_base;
                    $by_min_or_max = 'MAX';
                }
            }
        }
        else 
        {
//            $is_by_min_doprinosi_base = true;
            $by_min_or_max = 'MIN';
        }
        foreach($paycheck->additionals as $additional)
        {
            if ($additional->doprinosi_base < $additional->bruto)
            {
                $additional->doprinosi_base = $additional->bruto;
            }
            $additional->setValues($additional->bruto, $additional->tax_release, $additional->doprinosi_base);
        }

//        return $is_by_min_doprinosi_base;
        return $by_min_or_max;
    }
    
    /**
     * Leveluje doprinose izmedju zarade i dodataka zarade
     * Algoritam - efektivno svako svakome pokusa da prebaci visak
     * * prodje kroz sve dodatke i 
     * * prvo pokusa da preda visak zaradi ili da uzme visak od zarade
     * * pokusa da preda visak nekom drugom dodatku - obrnuto ne mora jer ce se taj smer desiti
     * @param type $same_svp - da li leveluje samo izmedju istih SVP
     */
    private function levelDoprinosiBasePure($same_svp)
    {
        $paycheck = $this->owner;
        
        foreach ($paycheck->additionals as $_ad_1)
        {
            if ($this->isSameSVP($same_svp,$_ad_1,$paycheck))
            {
                //ako je bruto zarade manji od minimalnog doprinosa && dodatkov doprinos manji od dodatkovog bruta
                //onda prebaci doprinos sa zarade na dodatak (jer ce se tu sigurno obracunati)
                if ($paycheck->base_doprinosi_base > $paycheck->base_bruto && $_ad_1->doprinosi_base < $_ad_1->bruto)
                {
                    $_transfer_amount = min($paycheck->base_doprinosi_base - $paycheck->base_bruto, $_ad_1->bruto - $_ad_1->doprinosi_base);
                    if (round($_transfer_amount,2) > 0)
                    {
                        $paycheck->base_doprinosi_base -= $_transfer_amount;
                        $_ad_1->doprinosi_base += $_transfer_amount;
                    }
                }

                //ako je bruto zarade VECI od minimalnog doprinosa && dodatkov doprinos VECI od dodatkovog bruta
                //onda prebaci doprinos sa dodatka na zaradu (jer ce se tu sigurno obracunati)
                if ($_ad_1->doprinosi_base > $_ad_1->bruto && $paycheck->base_doprinosi_base < $paycheck->base_bruto)
                {
                    $_transfer_amount = min($_ad_1->doprinosi_base - $_ad_1->bruto, $paycheck->base_bruto - $paycheck->base_doprinosi_base);
                    if (round($_transfer_amount,2) > 0)
                    {
                        $_ad_1->doprinosi_base -= $_transfer_amount;
                        $paycheck->base_doprinosi_base += $_transfer_amount;
                    }
                }
            }
            
            foreach ($paycheck->additionals as $_ad_2)
            {
                if ($this->isSameSVP($same_svp,$_ad_1,$_ad_2) && 
                    $_ad_1->doprinosi_base > $_ad_1->bruto && $_ad_2->doprinosi_base < $_ad_2->bruto)
                {
                    $_transfer_amount = min($_ad_1->doprinosi_base - $_ad_1->bruto, $_ad_2->bruto - $_ad_2->doprinosi_base);
                    if (round($_transfer_amount,2) > 0)
                    {
                        $_ad_1->doprinosi_base -= $_transfer_amount;
                        $_ad_2->doprinosi_base += $_transfer_amount;
                    }
                }
            }
        }
    }
    
    /**
     * Ukoliko je potrebno da SVP bude isti, on proverava, ukoliko ne, vraca da jeste isti
     * u zavisnosti da li je zarada ili dodatak, vadi SVP na razliciti nacin
     * @param boolean $same_svp_needed
     * @param SIMAActiveRecord $model1
     * @param SIMAActiveRecord $model2
     * @return boolean
     */
    private function isSameSVP($same_svp_needed, $model1, $model2)
    {
        if ($same_svp_needed)
        {
            $svp1 = (get_class($model1)=='Paycheck')?$model1->employee_svp:$model1->svp;
            $svp2 = (get_class($model2)=='Paycheck')?$model2->employee_svp:$model2->svp;
            return $svp1===$svp2;
        }
        else
        {
            return true;
        }
    }
    
}