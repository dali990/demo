<?php

class PaycheckTabs extends SIMAActiveRecordTabs
{

    protected function tabsDefinition()
    {
        $tabs = array();
        
        $owner = $this->owner;
        if($owner->confirmed)
        {
            $tabs = array_merge(array(
                array(
                    'title' => Yii::t('PaychecksModule.Paycheck','Preview'),
                    'code'=>'pdfPreview',
                    'origin' => 'Paycheck',
                )
            ));
            
            if(!empty($owner->file_id))
            {
                $tabs[] = [
                    'title' => Yii::t('MessagesModule.Common','Comments'),
                    'code'=>'comments',
                    'origin' => 'File',
                    'params_function' => 'tabComments',
                    'selector_used' => true
                ];
            }
        }
        else
        {
            $tabs = array_merge(array(
                
                [
                    'title' => 'Info',
                    'code'=>'htmlPreview',
                    'origin' => 'Paycheck',
                ],
                array(
                    'title' => 'Dodaci',
                    'code'=>'additionals',
                    'origin' => 'Paycheck',
                ),
                array(
                    'title' => Yii::t('PaychecksModule.Suspension', 'Suspensions'),
                    'code'=>'suspensions',
                    'origin' => 'Paycheck',
                ),
                [
                    'title' => Yii::t('PaychecksModule.Deduction', 'DeductionsTab'),
                    'code'=>'deductions',
                    'origin' => 'Paycheck',
                ],
                [
                    'title' => Yii::t('PaychecksModule.Deduction', 'XMLsTab'),
                    'code'=>'xmls',
                    'origin' => 'Paycheck',
                ],
                [
                    'title' => 'Kalkulacije',
                    'code'=>'calculations',
                    'origin' => 'Paycheck',
                    'params_function' => 'tabCalculations',
                ],
            ));
        }
        return $tabs;
    }
    
    protected function tabCalculations()
    {
        $owner = $this->owner;
        
        return array(
            'model' => $owner,
            'parametersEmployee' => PaychecksEmployee::createArrayForEmployeeParametersTable($owner),
            'parametersPaycheckPeriod' => PaychecksEmployee::createArrayForPaycheckPeriodParametersTable($owner),
            'parametersAccountingMonth' => PaychecksEmployee::createArrayForAccountingMonthParametersTable($owner),
            'systemParameters' => PaychecksEmployee::createArrayForSystemParametersTable($owner),
            'calculated' => PaychecksEmployee::createArrayForCalculatedParametersTable($owner),
            'calculated_final' => PaychecksEmployee::createArrayForFinalCalculatedParametersTable($owner),
            'employeeSVP' => $owner->employee_svp
        );
    }
    
    protected function tabComments()
    {
        $file = $this->owner->file;
        
        $base_ct = CommentThread::getCommonForModel($file);
                
        return array(
            'model' => $file  ,
            'comment_thread' => $base_ct
        );
    }
    
    
}