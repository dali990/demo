<?php

class PPPPDPaychecksBehavior extends SIMAActiveRecordBehavior
{
    public function relations($event)
    {
        $event->addResult([
            'paycheck_periods_to_ppppds' => [SIMAActiveRecord::HAS_MANY, PaycheckPeriodToPPPPD::class, 'ppppd_id']
        ]);
    }
    
    public function populateFromPaycheckPeriod(PaycheckPeriod $paycheckPeriod)
    {
        $owner = $this->owner;
        
        $deleteItemsCriteria = new SIMADbCriteria([
            'Model' => PPPPDItem::model(),
            'model_filter' => [
                'ppppd' => [
                    'ids' => $owner->id
                ]
            ]
        ]);
        PPPPDItem::model()->deleteAll($deleteItemsCriteria);
        
        $paychecks = $paycheckPeriod->valid_paychecks;
        foreach($paychecks as $paycheck)
        {
            $paycheck_xmls = $paycheck->paycheck_xmls;
            foreach($paycheck_xmls as $paycheck_xml)
            {
                if(!is_null($paycheck_xml->ppppd_id) && $paycheck_xml->ppppd_id !== $owner->id)
                {
                    continue;
                }
                if(is_null($paycheck_xml->ppppd_id))
                {
                    $paycheck_xml->ppppd_id = $owner->id;
                    $paycheck_xml->save();
                }
                
                $newPPPPDItem = new PPPPDItem();
                $newPPPPDItem->ppppd_id = $owner->id;
                $newPPPPDItem->svp = $paycheck_xml->svp;
                $newPPPPDItem->order_number = $paycheck_xml->order_number;
                $newPPPPDItem->indentification_type = $paycheck_xml->indentification_type;
                $newPPPPDItem->indentification_value = $paycheck_xml->indentification_value;
                $newPPPPDItem->lastname = $paycheck_xml->lastname;
                $newPPPPDItem->firstname = $paycheck_xml->firstname;
                $newPPPPDItem->municipality_code = $paycheck_xml->municipality_code;
                $newPPPPDItem->number_of_calendar_days = $paycheck_xml->number_of_calendar_days;
                $newPPPPDItem->effective_hours = $paycheck_xml->effective_hours;
                $newPPPPDItem->month_hours = $paycheck_xml->month_hours;
                $newPPPPDItem->bruto = $paycheck_xml->bruto;
                $newPPPPDItem->tax_base = $paycheck_xml->tax_base;
                $newPPPPDItem->tax_empl = $paycheck_xml->tax_empl;
                $newPPPPDItem->doprinosi_base = $paycheck_xml->doprinosi_base;
                $newPPPPDItem->pio = $paycheck_xml->pio;
                $newPPPPDItem->zdr = $paycheck_xml->zdr;
                $newPPPPDItem->nez = $paycheck_xml->nez;
                $newPPPPDItem->pio_ben = $paycheck_xml->pio_ben;
                $newPPPPDItem->mfp1 = $paycheck_xml->mfp1;
                $newPPPPDItem->mfp2 = $paycheck_xml->mfp2;
                $newPPPPDItem->mfp3 = $paycheck_xml->mfp3;
                $newPPPPDItem->mfp4 = $paycheck_xml->mfp4;
                $newPPPPDItem->mfp5 = $paycheck_xml->mfp5;
                $newPPPPDItem->mfp6 = $paycheck_xml->mfp6;
                $newPPPPDItem->mfp7 = $paycheck_xml->mfp7;
                $newPPPPDItem->mfp8 = $paycheck_xml->mfp8;
                $newPPPPDItem->mfp9 = $paycheck_xml->mfp9;
                $newPPPPDItem->mfp10 = $paycheck_xml->mfp10;
                $newPPPPDItem->mfp11 = $paycheck_xml->mfp11;
                $newPPPPDItem->mfp12 = $paycheck_xml->mfp12;
                $newPPPPDItem->save();
            }
        }
        
        $xmlContent = Yii::app()->controller->renderModelView($paycheckPeriod,'xml_eporezi', [
            'params' => [
                'paycheck_period_ppppd' => $owner
            ]
        ]);
        $tempFileName = TempDir::createNewTempFile($xmlContent);
        $owner->file->appendTempFile($tempFileName, 'ppppd.xml');
        
        $owner->save();
    }
    
    public function notInPaycheckPeriodsToPpppdsScope()
    {
        $owner = $this->owner;
        
        $alias = $owner->getTableAlias();
        $paycheckPeriodsToPpppdsTable = PaycheckPeriodToPPPPD::model()->tableName();
        
        $owner->getDbCriteria()->mergeWith([            
            'condition'=>"NOT EXISTS (SELECT 1 FROM $paycheckPeriodsToPpppdsTable pptp WHERE pptp.ppppd_id=$alias.id)"
        ]);
        
        return $owner;
    }
    
    /**
     * koristi se u js-u u postavljanju fixedfilter-a za modelchoose
     * jer tamo ne moze da se zada nesto kao
     *              0:'not',
                    1:{
                        'ids': fixed_filter.ppppd.ids
                    }
     * zali se na indeks 0 i 1 jer ih tretira kao kolone
     * @param type $ids
     * @return type
     */
    public function notWithPPPPDIdsScope($ids)
    {
        $owner = $this->owner;
        
        $alias = $owner->getTableAlias();
        
        $owner->getDbCriteria()->mergeWith([            
            'condition'=>"$alias.id NOT IN ($ids)"
        ]);
        
        return $owner;
    }
}
