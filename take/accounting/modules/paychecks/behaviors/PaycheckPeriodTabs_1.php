<?php

class PaycheckPeriodTabs extends SIMAActiveRecordTabs
{
    protected function tabsDefinition()
    {
//        $owner = $this->owner;
//        $tabs = array();
        
        $tabs = array(
            array(
                'title' => 'Zarade',
                'code'=>'paychecks',
                'origin' => 'PaycheckPeriod',
            ),
            array(
                'title' => 'Knjigovodstvo',
                'code'=>'bookings',
                'origin' => 'PaycheckPeriod',
            ),
            array(
                'title' => Yii::t('BaseModule.Common', 'Statistics'),
                'code'=>'statistics',
                'origin' => 'PaycheckPeriod',
                'params_function' => 'StatisticsFunctions',
            ),
            [
                'title' => Yii::t('PaychecksModule.PaycheckPeriod', 'PPP-PD'),
                'code'=>'ppppd',
                'origin' => 'PaycheckPeriod',
            ]
        );
        
        return $tabs;
    }
    
    public function StatisticsFunctions()
    {
        $owner = $this->owner;
        
        $differences = $owner->getDifferencesFromPreviousPeriod();
        $unused_suspensions = $owner->getUnusedSuspensions();
        $active_employees_without_paychecks = $owner->getActiveEmployeesWithoutPaychecks();
        
        return [
            'model' => $owner,
            'differences' => $differences,
            'unused_suspensions' => $unused_suspensions,
            'active_employees_without_paychecks' => $active_employees_without_paychecks
        ];
    }
}