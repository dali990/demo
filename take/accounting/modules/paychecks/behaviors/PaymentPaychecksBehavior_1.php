<?php

/**
 * @package SIMA
 * @subpackage Paychecks
 */
class PaymentPaychecksBehavior extends SIMAActiveRecordBehavior
{
    public function relations($event)
    {
        $event->addResult(array(
            'paycheck_period'=>array(SIMAActiveRecord::HAS_ONE, 'PaycheckPeriod', 'payment_id'),
            'paycheck'=>array(SIMAActiveRecord::HAS_ONE, 'Paycheck', 'payment_id'),
        ));
    }
}