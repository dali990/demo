<?php

echo SIMAHtml::modelIcons($model,16,array('open'));

if($model->confirmed)
{
    $recreate_oz_files_button_id = SIMAHtml::uniqid().'_rofbi';
    if(SIMAMisc::isVueComponentEnabled())
    {
        $this->widget(SIMAButtonVue::class, [
            'id' => $recreate_oz_files_button_id,
            'title' => Yii::t('PaychecksModule.Paycheck', 'RecreateOZFile'),
            'onclick' => [
                "sima.paychecks.recreateOZFile", "$recreate_oz_files_button_id", $model->id
            ]
        ]);
    }
    else
    {
        $this->widget('SIMAButton', [
            'id' => $recreate_oz_files_button_id,
            'action' => [
                'title' => Yii::t('PaychecksModule.Paycheck', 'RecreateOZFile'),
                'onclick' => [
                    "sima.paychecks.recreateOZFile", "$recreate_oz_files_button_id", $model->id
                ]
            ]
        ]);
    }
    
    if(isset($model->file))
    {
        $file_open_button = SIMAHtmlButtons::fileOpen($model);
        if(!empty($file_open_button))
        {
            echo $file_open_button;
        }
    }
}
else
{
    $button_id = 'recalcPaycheck_'.SIMAHtml::uniqid();
    $button_id_diff = 'recalcPaycheck_diff_'.SIMAHtml::uniqid();
    if(SIMAMisc::isVueComponentEnabled())
    {
        $this->widget(SIMAButtonVue::class, [
            'id' => $button_id,
            'title' => Yii::t('PaychecksModule.Paycheck', 'Recalc'),
            'onclick' => [
                "sima.paychecks.recalcPaycheck", $button_id, $model->id, true, false
            ],
            'subactions_data' => [
                [
                    'title' => Yii::t('PaychecksModule.Paycheck', 'RecalcWithoutChangingAdditionals'),
                    'onclick' => [
                        "sima.paychecks.recalcPaycheck", $button_id, $model->id, false, false
                    ]
                ],
//                [
//                    'title' => Yii::t('PaychecksModule.Paycheck', 'RecalcForCompareWithoutSave'),
//                    'onclick' => [
//                        "sima.paychecks.recalcPaycheck", $button_id, $model->id, true, true
//                    ],
//                    'subactions_data' => [
//                        [
//                            'title' => Yii::t('PaychecksModule.Paycheck', 'RecalcForCompareWithoutSaveDetailed'),
//                            'onclick' => ["sima.paychecks.recalcPaycheck", $button_id, $model->id, true, true, true]
//                        ]
//                    ]
//                ]
            ]
        ]);
        $this->widget(SIMAButtonVue::class, [
            'id' => $button_id_diff,
            'title' => Yii::t('PaychecksModule.Paycheck', 'RecalcForCompareWithoutSave'),
            'onclick' => [
                "sima.paychecks.recalcPaycheck", $button_id, $model->id, true, true
            ],
            'subactions_data' => [
                [
                    'title' => Yii::t('PaychecksModule.Paycheck', 'RecalcForCompareWithoutSaveDetailed'),
                    'onclick' => ["sima.paychecks.recalcPaycheck", $button_id, $model->id, true, true, true]
                ]
            ]
        ]);
    }
    else
    {
        $this->widget('SIMAButton', [
            'id' => $button_id,
            'action' => [
                'title' => Yii::t('PaychecksModule.Paycheck', 'Recalc'),
                'onclick' => [
                    "sima.paychecks.recalcPaycheck", $button_id, $model->id, true, false
                ],
                'subactions' => [
                    [
                        'title' => Yii::t('PaychecksModule.Paycheck', 'RecalcWithoutChangingAdditionals'),
                        'onclick' => [
                            "sima.paychecks.recalcPaycheck", $button_id, $model->id, false, false
                        ]
                    ],
                    [
                        'title' => Yii::t('PaychecksModule.Paycheck', 'RecalcForCompareWithoutSave'),
                        'onclick' => [
                            "sima.paychecks.recalcPaycheck", $button_id, $model->id, true, true
                        ],
                        'subactions' => [
                            [
                                'title' => Yii::t('PaychecksModule.Paycheck', 'RecalcForCompareWithoutSaveDetailed'),
                                'onclick' => ["sima.paychecks.recalcPaycheck", $button_id, $model->id, true, true, true]
                            ]
                        ]
                    ]
                ]
            ]
        ]);
    }
}
