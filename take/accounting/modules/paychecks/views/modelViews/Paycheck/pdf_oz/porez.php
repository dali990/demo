<?php 
$porez_num = $section_counter; 
$section_counter++;
?>

<table width="100%" cellpadding="0" cellspacing="0" style="font-size: 7px; line-height: 10px;">
<tr>
    <td width="100%">

        <!--Nazivi kolona za obustave--> 
        <table width="100%" cellpadding="1" cellspacing="0" style="border: 1px solid #939597; border-bottom: none; text-align: right;  font-size: 7px; line-height: 10px;">
            <tr>
                <td width="4%">Br.</td>
                <td width="19.2%" style="text-align: left;"> Elementi</td>
                <td width="15.2%">Procenat %</td>
                <td width="19.2%">Obračunato</td>
                <td width="21.2%">Prethodno isplaćeno</td>
                <td width="21.2%">Za isplatu</td>
            </tr>
        </table>
        <!--Nazivi kolona za obustave--> 



        <table width="100%" cellpadding="1" cellspacing="0" style="border: 1px solid #939597; border-bottom: none; font-weight: bold; background-color: #e6e7e8; text-align: left;  font-size: 7px; line-height: 10px;">
            <tr>
                <td width="4%"><?=$porez_num;?>.</td>
                <td width="96%"> POREZ</td>
            </tr>
        </table>

        <!--Topli obrok-->  
        <table width="100%" cellpadding="1" cellspacing="0" style="border: 1px solid #939597; border-bottom: none; text-align: right;  font-size: 7px; line-height: 10px;  font-size: 7px; line-height: 10px;">
            <tr>
                <td width="4%"><?=$porez_num;?>.1</td>
                <td width="19.2%" style="text-align: left;"> Obračunat bruto</td>
                <td width="15.2%"></td>
                <td width="19.2%"><?php echo SIMAHtml::number_format($model->base_bruto + $model->additionals_sum); ?></td>
                <td width="21.2%"><?php echo SIMAHtml::number_format($model->prev_bruto_sum); ?></td>
                <td width="21.2%"><?php echo SIMAHtml::number_format($model->amount_bruto); ?></td>
            </tr>
        </table>
        <!--Topli obrok--> 
        <!--Regres--> 
        <table width="100%" cellpadding="1" cellspacing="0" style="border: 1px solid #939597; border-bottom: none; text-align: right;  font-size: 7px; line-height: 10px; font-size: 7px; line-height: 10px;">
            <tr>
                <td width="4%"><?=$porez_num;?>.2</td>
                <td width="19.2%" style="text-align: left;"> Ukupna olakšica</td>
                <td width="15.2%"></td>
                <td width="19.2%"><?php echo SIMAHtml::number_format($model->base_bruto - $model->base_tax_base + $model->additionals_used_tax_release); ?></td>
                <td width="21.2%"><?php echo SIMAHtml::number_format($model->prev_bruto_sum - $model->prev_tax_base); ?></td>
                <td width="21.2%"><?php echo SIMAHtml::number_format($model->amount_bruto - $model->tax_base); ?></td>
            </tr>
        </table>
        <!--Regres--> 
        <!--Bolovanje do 30 dana--> 
        <table width="100%" cellpadding="1" cellspacing="0" style="border: 1px solid #939597; border-bottom: none; text-align: right;  font-size: 7px; line-height: 10px;  font-size: 7px; line-height: 10px;">
            <tr>
                <td width="4%"><?=$porez_num;?>.3</td>
                <td width="19.2%" style="text-align: left;"> Osnovica za porez</td>
                <td width="15.2%"></td>
                <td width="19.2%"><?php echo SIMAHtml::number_format($model->base_tax_base + ($model->additionals_sum - $model->additionals_used_tax_release)); ?></td>
                <td width="21.2%"><?php echo SIMAHtml::number_format($model->prev_tax_base); ?></td>
                <td width="21.2%"><?php echo SIMAHtml::number_format($model->tax_base); ?></td>
            </tr>
        </table>
        <!--Bolovanje do 30 dana--> 
        <!--Porez--> 
        <table width="100%" cellpadding="1" cellspacing="0" style="border: 1px solid #939597; text-align: right;  font-size: 7px; line-height: 10px;  font-size: 7px; line-height: 10px;">
            <tr>
                <td width="4%"><?=$porez_num;?>.4</td>
                <td width="19.2%" style="text-align: left;"> Porez</td>
                <td width="15.2%"><?php echo $model->tax_empl_perc; ?>%</td>
                <td width="19.2%"><?php echo SIMAHtml::number_format($model->base_tax_empl + $model->additionals_tax_empl); ?></td>
                <td width="21.2%"><?php echo SIMAHtml::number_format($model->prev_tax_empl); ?></td>
                <td width="21.2%"><?php echo SIMAHtml::number_format($model->tax_empl); ?></td>
            </tr>
        </table>
        <!--Porez--> 
    </td>
</tr>
</table>