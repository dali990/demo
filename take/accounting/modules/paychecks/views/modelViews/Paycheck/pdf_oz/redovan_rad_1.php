<?php
$redovan_rad_num = $section_counter;
$section_counter++;

$attributes_in_section = [
    [
        'title' => 'Redovan rad',
        'hours' => $model->worked_hours,
        'coeficient' => '100',
        'value' => SIMAHtml::number_format($model->worked_hours_value)
    ],
    [
        'title' => Yii::t('PaychecksModule.Paycheck', 'OvertimeHours'),
        'hours' => $model->overtime_hours,
        'coeficient' => SIMAHtml::number_format($model->overtime_percentage, 0),
        'value' => SIMAHtml::number_format($model->overtime_value)
    ],
    [
        'title' => Yii::t('PaychecksModule.PaycheckAdditional', 'WORK_ON_NON_WORKING_HOLIDAY'),
        'hours' => $model->work_on_state_holiday_hours,
        'coeficient' => SIMAHtml::number_format($model->work_on_state_holiday_percentage, 0),
        'value' => SIMAHtml::number_format($model->work_on_state_holiday_value)
    ]
];
?>

<!--Nazivi kolona u tabeli--> 
<table width="100%" cellpadding="1" cellspacing="0" style="border: 1px solid #939597; border-bottom: none; text-align: right; font-size: 7px; line-height: 10px; vertical-align: top;">
    <tr>
        <td width="7%">Br.</td>
        <td width="49%" style="text-align: left;"> Elementi</td>
        <td width="11%">Sati</td>
        <td width="10%">Koeficijenti</td>
        <td width="23%">Iznos</td>
    </tr>
</table>
<!--Nazivi kolona u tabeli--> 

<table width="100%" cellpadding="1" cellspacing="0" style="border: 1px solid #939597; border-bottom: none; font-weight: bold; background-color: #e6e7e8; text-align: left; font-size: 7px; line-height: 10px;">
    <tr>
        <td  width="7%"><?= $redovan_rad_num; ?>.</td>
        <td width="93%"> REDOVAN RAD</td>
    </tr>
</table>

<?php
$redovan_rad_counter = 1;
foreach($attributes_in_section as $attribute_in_section)
{
    ?>
    <table width="100%" cellpadding="1" cellspacing="0" style="border: 1px solid #939597; border-bottom: none; text-align: right; font-size: 7px; line-height: 10px;">
       <tr>
           <td width="7%"><?=$redovan_rad_num.'.'.$redovan_rad_counter;?></td>
           <td width="49%" style="text-align: left;"><?php echo $attribute_in_section['title']; ?></td>
           <td width="11%"><?php echo $attribute_in_section['hours']; ?></td>
           <td width="10%"><?php echo $attribute_in_section['coeficient']; ?>%</td>
           <td width="23%"><?php echo $attribute_in_section['value']; ?></td>
       </tr>
    </table>
    <?php
    $redovan_rad_counter++;
}
?>

<!--Pripravnost--> 
<!--                   <table width="100%" cellpadding="1" cellspacing="0" style="border: 1px solid #939597; text-align: right; font-size: 7px; line-height: 10px;">
   <tr>
       <td width="7%">2.8</td>
       <td width="49%" style="text-align: left;"> Pripravnost</td>
       <td width="11%"><?php // echo $pripravnost_hours;  ?></td>
       <td width="10%"><?php // echo SIMAHtml::number_format($pripravnost_percentage, 0);  ?>%</td>
       <td width="23%"><?php // echo SIMAHtml::number_format($pripravnost_value);  ?></td>
   </tr>
</table>-->
<!--Pripravnost--> 
<!--Rad nocu--> 
<!--                   <table width="100%" cellpadding="1" cellspacing="0" style="border: 1px solid #939597; text-align: right; font-size: 7px; line-height: 10px;">
   <tr>
       <td width="7%">2.2</td>
       <td width="49%" style="text-align: left;"> Rad nocu</td> 
       <td width="11%"><?php // echo $work_at_night_hours;  ?></td>
       <td width="10%"><?php // echo SIMAHtml::number_format($work_at_night_percentage, 0);  ?>%</td>
       <td width="23%"><?php // echo SIMAHtml::number_format($work_at_night_value);  ?></td>
   </tr>
</table>-->
<!--Rad nocu--> 
<!--Rad u smeni--> 
<!--                   <table width="100%" cellpadding="1" cellspacing="0" style="border: 1px solid #939597; text-align: right; font-size: 7px; line-height: 10px;">
   <tr>
       <td width="7%">2.3</td>
       <td width="49%" style="text-align: left;"> Rad u smeni</td> 
       <td width="11%"><?php // echo $work_in_shifts_hours;  ?></td>
       <td width="10%"><?php // echo SIMAHtml::number_format($work_in_shifts_percentage, 0);  ?>%</td>
       <td width="23%"><?php // echo SIMAHtml::number_format($work_in_shifts_value);  ?></td>
   </tr>
</table>-->
<!--Rad smeni--> 

<?php
//$redovan_rad_hours = $model->worked_hours + $model->overtime_hours + $model->work_on_state_holiday_hours;
//$redovan_rad_value = $model->worked_hours_value + $model->work_on_state_holiday_value;
?>
<table width="100%" cellpadding="1" cellspacing="0" style="border: 1px solid #939597; border-botom: none; text-align: right; font-weight: bold; font-size: 7px; line-height: 10px;">
    <tr>
        <td width="7%"><?= $redovan_rad_num; ?>.SUM</td>
        <td width="49%" style="text-align: left;"> Ukupno</td>
        <td width="11%"><?php echo $model->regular_work_hours; ?></td>
        <td width="10%"></td>
        <td width="23%"><?php echo SIMAHtml::number_format($model->regular_work_value); ?></td>
    </tr>
</table>