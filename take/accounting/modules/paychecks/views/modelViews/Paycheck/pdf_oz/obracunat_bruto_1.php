<?php 
$obracunat_bruto_num = $section_counter; 
$section_counter++;
?>

<table width="100%" cellpadding="1" cellspacing="0" style="border: 1px solid #939597; border-bottom: none; text-align: right;  font-size: 7px; line-height: 10px;">
    <tr>
        <td width="7%"> Br.</td>
        <td width="49%" style="text-align: left;"> Elementi</td>
        <td width="16%">Sati</td>
        <td width="28%">Iznos</td>
    </tr>
</table>
<!--Nazivi kolona u tabeli--> 


<!--Obracunat bruto--> 
<table width="100%" cellpadding="1" cellspacing="0" style="border: 1px solid #939597; border-bottom: none; font-weight: bold; background-color: #e6e7e8; text-align: left;  font-size: 7px; line-height: 10px;">
    <tr>
        <td width="7%"><?=$obracunat_bruto_num;?>.</td>
        <td width="93%" style="text-align: left;"> OBRAČUNAT BRUTO</td>
    </tr>
</table>
<table width="100%" cellpadding="1" cellspacing="0" style="border: 1px solid #939597; border-bottom: none; text-align: right;  font-size: 7px; line-height: 10px;">
    <tr>
        <td width="7%"><?=$redovan_rad_num?>.SUM</td>
        <td width="49%" style="text-align: left;"> Redovan rad</td>
        <td width="16%"><?php echo $model->regular_work_hours; ?></td>
        <td width="28%"><?php echo SIMAHtml::number_format($model->regular_work_value); ?></td>
    </tr>
</table>
<table width="100%" cellpadding="1" cellspacing="0" style="border: 1px solid #939597; border-bottom: none; text-align: right;  font-size: 7px; line-height: 10px;">
    <tr>
        <td width="7%"><?=$korekcija_redovnog_rada?>.SUM</td>
        <td width="49%" style="text-align: left;"> Korekcija redovnog rada</td>
        <td width="16%"></td>
        <td width="28%"><?php echo SIMAHtml::number_format($korekcija_redovnog_rada_value); ?></td>
    </tr>
</table>
<table width="100%" cellpadding="1" cellspacing="0" style="border: 1px solid #939597; border-bottom: none; text-align: right;  font-size: 7px; line-height: 10px;">
    <tr>
        <td width="7%"><?=$dodaci_na_redovan_rad_num?>.SUM</td>
        <td width="49%" style="text-align: left;"> Dodaci na redovan rad</td>
        <td width="16%"></td>
        <td width="28%"><?php echo SIMAHtml::number_format($dodaci_na_redovan_rad_value); ?></td>
    </tr>
</table>
<table width="100%" cellpadding="1" cellspacing="0" style="border: 1px solid #939597; border-bottom: none; text-align: right;  font-size: 7px; line-height: 10px;">
    <tr>
        <td width="7%"><?=$naknade_zarade_num?>.SUM</td>
        <td width="49%" style="text-align: left;"> Naknade zarade</td>
        <td width="16%"><?php echo $naknade_zarade_hours; ?></td>
        <td width="28%"><?php echo SIMAHtml::number_format($naknade_zarade_value); ?></td>
    </tr>
</table>
<?php if ($show_bonuses) {?>
<table width="100%" cellpadding="1" cellspacing="0" style="border: 1px solid #939597; border-bottom: none; text-align: right;  font-size: 7px; line-height: 10px;">
    <tr>
        <td width="7%"><?=$naknade_zarade_num?>.SUM</td>
        <td width="49%" style="text-align: left;"> Bonusi</td>
        <td width="44%"><?php echo SIMAHtml::number_format($_bonus_sum_value); ?></td>
    </tr>
</table>
<?php }?>

<?php
$obracunat_bruto_hours = $model->regular_work_hours + $naknade_zarade_hours;
//$obracunat_bruto_value = $model->regular_work_value + $korekcija_redovnog_rada_value + $dodaci_na_redovan_rad_value + $naknade_zarade_value + $_bonus_sum_value;
$obracunat_bruto_value = $model->regular_work_value + $korekcija_redovnog_rada_value + $dodaci_na_redovan_rad_value + $naknade_zarade_value;
if ($show_bonuses) 
{
    $obracunat_bruto_value += $_bonus_sum_value;
}
?>
<table width="100%" cellpadding="1" cellspacing="0" style="border: 1px solid #939597; text-align: right; font-weight: bold;  font-size: 7px; line-height: 10px;">
    <tr>
        <td width="7%"><?=$obracunat_bruto_num;?>.SUM</td>
        <td width="49%" style="text-align: left;"> Ukupno</td>
        <td width="16%"><?php echo $obracunat_bruto_hours; ?></td>
        <td width="28%"> <?php echo SIMAHtml::number_format($obracunat_bruto_value); ?></td>
    </tr>
</table>
