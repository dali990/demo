<?php 
$dodaci_na_redovan_rad_num = $section_counter; 
$section_counter++;

$attributes_in_section = [
    [
        'title' => 'Topli obrok',
        'hours' => $model->regular_work_hours,
        'coeficient' => $model->hot_meal_per_hour.'din',
        'value' => SIMAHtml::number_format($model->hot_meal_value)
    ],
    [
        'title' => Yii::t('PaychecksModule.Paycheck', 'HotmealFieldwork'),
        'hours' => $model->regular_work_hours,
        'coeficient' => $model->hotmeal_fieldwork_per_hour.'din',
        'value' => SIMAHtml::number_format($model->hotmeal_fieldwork_value)
    ],
    [
        'title' => 'Regres',
        'hours' => $model->worked_hours,
        'coeficient' => 'din',
        'value' => SIMAHtml::number_format($model->regres)
    ]
];
?>

<table width="100%" cellpadding="1" cellspacing="0" style="border: 1px solid #939597; border-bottom: none; text-align: right; font-size: 7px; line-height: 10px; vertical-align: top;">
   <tr>
       <td width="7%"> Br.</td>
       <td width="44%" style="text-align: left;"> Elementi</td>
       <td width="12%">Sati</td>
       <td width="14%">Jed. Vrednost</td>
       <td width="23%">Iznos</td>
   </tr>
</table>
<!--Nazivi kolona u tabeli--> 


<table width="100%" cellpadding="1" cellspacing="0" style="border: 1px solid #939597; border-bottom: none; font-weight: bold; background-color: #e6e7e8; text-align: left; font-size: 7px; line-height: 10px;">
   <tr>
       <td  width="7%"><?=$dodaci_na_redovan_rad_num;?>.</td>
       <td width="93%"> DODACI NA REDOVAN RAD</td>
   </tr>
</table>

<?php
$dodaci_na_redovan_rad_counter = 1;
foreach($attributes_in_section as $attribute_in_section)
{
    ?>
    <table width="100%" cellpadding="1" cellspacing="0" style="border: 1px solid #939597; border-bottom: none; text-align: right; font-size: 7px; line-height: 10px;">
        <tr>
           <td width="7%"><?=$dodaci_na_redovan_rad_num.'.'.$dodaci_na_redovan_rad_counter;?></td>
            <td width="44%" style="text-align: left;"><?php echo $attribute_in_section['title']; ?></td>
            <td width="12%"><?php echo $attribute_in_section['hours']; ?></td>
            <td width="14%"><?php echo $attribute_in_section['coeficient']; ?></td>
            <td width="23%"><?php echo $attribute_in_section['value']; ?></td>
        </tr>
    </table>
    <?php
    $dodaci_na_redovan_rad_counter++;
}
?>

<?php
$dodaci_na_redovan_rad_value = $model->hot_meal_value + $model->hotmeal_fieldwork_value + $model->regres;
?>
<table width="100%" cellpadding="1" cellspacing="0" style="border: 1px solid #939597; text-align: right; font-weight: bold; font-size: 7px; line-height: 10px;">
   <tr>
       <td width="7%"><?=$dodaci_na_redovan_rad_num;?>.SUM</td> 
       <td width="44%" style="text-align: left;"> Ukupno</td>
       <td width="12%"></td>
       <td width="14%"></td>
       <td width="23%"><?php echo SIMAHtml::number_format($dodaci_na_redovan_rad_value); ?></td>
   </tr>
</table>
