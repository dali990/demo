<?php 
$show_bonuses = count($model->bonuses) > 0;
if ($show_bonuses)
{

    $naknade_zarade_num = $section_counter; 
    $section_counter++;
    $item_counter = 1;


?>

    <table width="100%" cellpadding="1" cellspacing="0" style="border: 1px solid #939597; border-bottom: none; text-align: right; font-size: 7px; line-height: 10px;">
    <tr>
       <td width="7%"> Br.</td>
       <td width="49%" style="text-align: left;"> Naziv</td>
       <td width="44%">Iznos</td>
    </tr>
    </table>
    <!--Nazivi kolona u tabeli--> 


    <table width="100%" cellpadding="1" cellspacing="0" style="border: 1px solid #939597; border-bottom: none; font-weight: bold; background-color: #e6e7e8; text-align: left; font-size: 7px; line-height: 10px;">
    <tr>
       <td width="7%"><?=$naknade_zarade_num;?>.</td>
       <td width="93%"> Bonusi</td>
    </tr>
    </table>


    <?php $_bonus_sum_value=0; foreach ($model->bonuses as $_bonus) { 
        $_bonus_sum_value += $_bonus->value;

        ?>

        <table width="100%" cellpadding="1" cellspacing="0" style="border: 1px solid #939597; border-bottom: none; text-align: right; font-size: 7px; line-height: 10px;">
        <tr>
           <td width="7%"><?=$naknade_zarade_num.'.'.$item_counter++;?></td>
           <td width="49%" style="text-align: left;"><?=$_bonus->DisplayName?></td>
           <td width="44%"><?php echo SIMAHtml::number_format($_bonus->value); ?></td>
        </tr>
        </table>
    <?php  }?>


    <table width="100%" cellpadding="1" cellspacing="0" style="border: 1px solid #939597; text-align: right; font-weight: bold; font-size: 7px; line-height: 10px;">
    <tr>
       <td width="7%"><?=$naknade_zarade_num;?>.SUM</td>
       <td width="49%" style="text-align: left;"> Ukupno bonus</td>
       <td width="44%"><?php echo SIMAHtml::number_format($_bonus_sum_value); ?>
       </td>
    </tr>
    </table>

<?php }?>