<?php
$section_counter = 1;
?>

<?php 
include('header.php');
?>
<table>
    <tr>
        <td height="10px"></td>
    </tr>
</table>
<table width="100%" cellpadding="0" cellspacing="0" style="font-size: 7px; line-height: 10px;">
    <tr>
    <table width="100%" cellpadding="1" cellspacing="0">
        <tr>
            <td width="49%" style="font-size: 7px; line-height: 10px;">
                <?php include('redovan_rad.php'); ?>
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td height="10px"></td>
                    </tr>
                </table>
                <?php include('korekcije_redovnog_rada.php'); ?>
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td height="10px"></td>
                    </tr>
                </table>
                <?php include('dodaci_na_redovan_rad.php'); ?>
            </td>

            <!--         Horizontalni razmak     -->
            <td width="2%"></td>
            <!--Horizontalni razmak--> 

            <td width="49%" style="vertical-align: top">
                <?php include('naknade_zarade.php'); ?>
                <table>
                    <tr>
                        <td height="10px"></td>
                    </tr>
                </table>
                <?php include('bonusi.php'); ?>
                <table>
                    <tr>
                        <td height="10px"></td>
                    </tr>
                </table>
                <?php include('obracunat_bruto.php'); ?>
            </td>
        </tr>

    </table>
</tr>

<table>
    <tr>
        <td height="10px"></td>
    </tr>
</table>

<?php include('porez.php'); ?>

<table>
    <tr>
        <td height="10px"></td>
    </tr>
</table>

<?php include('doprinosi.php'); ?>

<table>
    <tr>
        <td height="10px"></td>
    </tr>
</table>

<?php include('administrativne_zabrane.php'); ?>

<table>
    <tr>
        <td height="10px"></td>
    </tr>
</table>

<?php include('odbici.php'); ?>

<table>
    <tr>
        <td height="10px"></td>
    </tr>
</table>

<table width="100%" cellpadding="0" cellspacing="0" style="font-size: 7px; line-height: 10px;">
<tr>
    <td width="100%">
        <!--ZA ISPLATU--> 
        <table width="100%" cellpadding="1" cellspacing="0" style="border: 1px solid #939597; text-align: left;  font-size: 7px; line-height: 10px;">
            <tr>
                <td> Obračunski radnik: <?php echo $model->paycheck_period->creator_user->DisplayName; ?></td>
                <td style="text-align: right; background-color: #e6e7e8; border-left: 1px solid #939597;"> Za isplatu zaposlenom: <span style="font-weight: bold; font-size: 8px;"> <?php echo SIMAHtml::money_format($model->amount_neto_for_payout); ?> </span></td>
            </tr>
        </table>
        <!--ZA ISPLATU--> 
    </td>
</tr> 

</table>