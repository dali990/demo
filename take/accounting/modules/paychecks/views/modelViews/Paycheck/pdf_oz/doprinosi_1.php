<?php 
$doprinosi_num = $section_counter; 
$section_counter++;
?>

<table width="100%" cellpadding="0" cellspacing="0" style="font-size: 7px; line-height: 10px;">
    <tr>
        <td width="100%"> 
            <!--Doprinosi--> 
            <table width="100%" cellpadding="1" cellspacing="0" style="border: 1px solid #939597; border-bottom: none; font-weight: bold; background-color: #e6e7e8; text-align: left;  font-size: 7px; line-height: 10px;">
                <tr>
                    <td width="4%"><?=$doprinosi_num;?>.</td>
                    <td width="96%">DOPRINOSI</td>
                </tr>
            </table>
            <!--Doprinosi--> 

            <!--OSNOVICA--> 
            <?php if($model->is_by_min_tax_base || $model->is_by_max_tax_base) { ?>
            <table width="100%" cellpadding="1" cellspacing="0" style="border: 1px solid #939597; border-bottom: none; text-align: right;  font-size: 7px; line-height: 10px;">
                <tr>
                    <td width="4%"><?=$doprinosi_num;?>.1</td>
                    <td width="19.2%" style="text-align: left;"><?php
                        if($model->is_by_min_tax_base)
                        {
                            echo 'Min. osnovica za doprinose';
                        }
                        else
                        {
                            echo 'Max. osnovica za doprinose';
                        }
                    ?></td>
                    <td width="15.2%"></td>
                    <td width="19.2%"><?php 
                        if($model->is_by_min_tax_base)
                        {
                            echo SIMAHtml::number_format($model->month_min_contribution_base); 
                        }
                        else
                        {
                            echo SIMAHtml::number_format($model->month_max_contribution_base); 
                        }
                    ?></td>
                    <td width="21.2%"></td>
                    <td width="21.2%"></td>
                </tr>
            </table>
            <?php } ?>
            <!--OSNOVICA--> 

            <!--OSNOVICA--> 
            <table width="100%" cellpadding="1" cellspacing="0" style="border: 1px solid #939597; border-bottom: none; text-align: right;  font-size: 7px; line-height: 10px;">
                <tr>
                    <td width="4%"><?=$doprinosi_num;?>.1</td>
                    <td width="19.2%" style="text-align: left;"> Obračunata osnovica za dopr.</td>
                    <td width="15.2%"></td>
                    <td width="19.2%"><?php echo SIMAHtml::number_format($model->base_doprinosi_base + $model->additionals_doprinosi_base); ?></td>
                    <td width="21.2%"><?php echo SIMAHtml::number_format($model->prev_doprinosi_base); ?></td>
                    <td width="21.2%"><?php echo SIMAHtml::number_format($model->doprinosi_base); ?></td>
                </tr>
            </table>
            <!--OSNOVICA--> 


            <!--Doprinosi na teret zaposlenog--> 
            <table width="100%" cellpadding="1" cellspacing="0" style="border: 1px solid #939597; border-bottom: none; background-color: #e6e7e8; text-align: right; font-weight: bold;  font-size: 7px; line-height: 10px;">
                <tr>
                    <td width="4%"><?=$doprinosi_num;?>.2</td>
                    <td width="96%" style="text-align: left;"> Doprinosi na teret zaposlenog</td>
                </tr>
            </table>
            <!--Doprinosi na teret zaposlenog--> 
            <!--Doprinos za pio radnika--> 
            <table width="100%" cellpadding="1" cellspacing="0" style="border: 1px solid #939597; border-bottom: none; text-align: right;  font-size: 7px; line-height: 10px;">
                <tr>
                    <td width="4%"><?=$doprinosi_num;?>.3</td>
                    <td width="19.2%" style="text-align: left;"> Doprinos za PIO radnika</td>
                    <td width="15.2%"><?php echo $model->pio_empl_perc; ?>%</td>
                    <td width="19.2%"><?php echo SIMAHtml::number_format($model->base_pio_empl + $model->additionals_pio_empl); ?></td>
                    <td width="21.2%"><?php echo SIMAHtml::number_format($model->prev_pio_empl); ?></td>
                    <td width="21.2%"><?php echo SIMAHtml::number_format($model->pio_empl); ?></td>
                </tr>
            </table>
            <!--Doprinos za PIO radnika--> 



            <!--Doprinos za ZDR RADNIKA--> 
            <table width="100%" cellpadding="1" cellspacing="0" style="border: 1px solid #939597; border-bottom: none; text-align: right;  font-size: 7px; line-height: 10px;">
                <tr>
                    <td width="4%"><?=$doprinosi_num;?>.4</td>
                    <td width="19.2%" style="text-align: left;"> Doprinos za zdr. radnika</td>
                    <td width="15.2%"><?php echo $model->zdr_empl_perc; ?>%</td>
                    <td width="19.2%"><?php echo SIMAHtml::number_format($model->base_zdr_empl + $model->additionals_zdr_empl); ?></td>
                    <td width="21.2%"><?php echo SIMAHtml::number_format($model->prev_zdr_empl); ?></td>
                    <td width="21.2%"><?php echo SIMAHtml::number_format($model->zdr_empl); ?></td>
                </tr>
            </table>
            <!--Doprinos za ZDR RADNIKA--> 

            <!--Doprinos za osiguranje od nezaposlenosti--> 
            <table width="100%" cellpadding="1" cellspacing="0" style="border: 1px solid #939597; border-bottom: none; text-align: right;  font-size: 7px; line-height: 10px;">
                <tr>
                    <td width="4%"><?=$doprinosi_num;?>.5</td>
                    <td width="19.2%" style="text-align: left;"> Doprinos za osig. od nezapos.</td>
                    <td width="15.2%"><?php echo $model->nez_empl_perc; ?>%</td>
                    <td width="19.2%"><?php echo SIMAHtml::number_format($model->base_nez_empl + $model->additionals_nez_empl); ?></td>
                    <td width="21.2%"><?php echo SIMAHtml::number_format($model->prev_nez_empl); ?></td>
                    <td width="21.2%"><?php echo SIMAHtml::number_format($model->nez_empl); ?></td>
                </tr>
            </table>
            <!--Doprinos za osiguranje od nezaposlenosti--> 
            <!--Ukupno doprinosi na teret zaposlenog--> 
            <table width="100%" cellpadding="1" cellspacing="0" style="border: 1px solid #939597; border-bottom: none; text-align: right;  font-size: 7px; line-height: 10px;">
                <tr>
                    <td width="4%"><?=$doprinosi_num;?>.6</td>
                    <td width="19.2%" style="text-align: left;"> Ukupno:</td>
                    <td width="15.2%"></td>
                    <td width="19.2%"><?php
                        echo SIMAHtml::number_format(
//                        $model->base_pio_empl + $model->base_zdr_empl + $model->base_nez_empl +
//                        $model->additionals_pio_empl + $model->additionals_zdr_empl + $model->additionals_nez_empl
                                $model->getEmployeeContributions()
                        );
                        ?></td>
                    <td width="21.2%"><?php echo SIMAHtml::number_format($model->prev_pio_empl + $model->prev_zdr_empl + $model->prev_nez_empl); ?></td>
                    <td width="21.2%">  <?php
                        echo SIMAHtml::number_format($model->pio_empl + $model->zdr_empl + $model->nez_empl);
                        ?>
                    </td>
                </tr>
            </table>
            <!--Ukupno doprinosi na teret zaposlenog--> 

            <!--Doprinosi na teret poslodavca--> 
            <table width="100%" cellpadding="1" cellspacing="0" style="border: 1px solid #939597; border-bottom: none; background-color: #e6e7e8; text-align: right; font-weight: bold;  font-size: 7px; line-height: 10px;">
                <tr>
                    <td width="4%"><?=$doprinosi_num;?>.7</td>
                    <td width="96%" style="text-align: left;">Doprinosi na teret poslodavca</td>
                </tr>
            </table>
            <!--Doprinosi na teret poslodavca--> 
            <!--Doprinosi za PIO poslodavac-->   
            <table width="100%" cellpadding="1" cellspacing="0" style="border: 1px solid #939597; border-bottom: none; text-align: right;  font-size: 7px; line-height: 10px;">
                <tr>
                    <td width="4%"><?=$doprinosi_num;?>.8</td>
                    <td width="19.2%" style="text-align: left;"> Doprinosi za PIO poslodavac</td>
                    <td width="15.2%"><?php echo $model->pio_comp_perc; ?>%</td>
                    <td width="19.2%"><?php echo SIMAHtml::number_format($model->base_pio_comp + $model->additionals_pio_comp); ?></td>
                    <td width="21.2%"><?php echo SIMAHtml::number_format($model->prev_pio_comp); ?></td>
                    <td width="21.2%"><?php echo SIMAHtml::number_format($model->pio_comp); ?></td>
                </tr>
            </table>
            <!--Doprinosi za PIO poslodavac--> 
            <!--Doprinosi za zdravstveno osiguranje poslodavac-->   
            <table width="100%" cellpadding="1" cellspacing="0" style="border: 1px solid #939597; border-bottom: none; text-align: right;  font-size: 7px; line-height: 10px;">
                <tr>
                    <td width="4%"><?=$doprinosi_num;?>.9</td>
                    <td width="19.2%" style="text-align: left;"> Doprin. za zdr. osig. poslodavac</td>
                    <td width="15.2%"><?php echo $model->zdr_comp_perc; ?>%</td>
                    <td width="19.2%"><?php echo SIMAHtml::number_format($model->base_zdr_comp + $model->additionals_zdr_comp); ?></td>
                    <td width="21.2%"><?php echo SIMAHtml::number_format($model->prev_zdr_comp); ?></td>
                    <td width="21.2%"><?php echo SIMAHtml::number_format($model->zdr_comp); ?></td>
                </tr>
            </table>
            <!--Doprinosi za zdravstveno osiguranje poslodavac--> 
            <!--Doprinos za osiguranje od nezaposlenosti--> 
            <table width="100%" cellpadding="1" cellspacing="0" style="border: 1px solid #939597; border-bottom: none; text-align: right;  font-size: 7px; line-height: 10px;">
                <tr>
                    <td width="4%"><?=$doprinosi_num;?>.10</td>
                    <td width="19.2%" style="text-align: left;"> Doprinos za osig. od nezapos.</td>
                    <td width="15.2%"><?php echo $model->nez_comp_perc; ?>%</td>
                    <td width="19.2%"><?php echo SIMAHtml::number_format($model->base_nez_comp + $model->additionals_nez_comp); ?></td>
                    <td width="21.2%"><?php echo SIMAHtml::number_format($model->prev_nez_comp); ?></td>
                    <td width="21.2%"><?php echo SIMAHtml::number_format($model->nez_comp); ?></td>
                </tr>
            </table>
            <!--Doprinos za osiguranje od nezaposlenosti--> 

            <!--Ukupno doprinosi na teret poslodavca--> 
            <table width="100%" cellpadding="1" cellspacing="0" style="border: 1px solid #939597; text-align: right;  font-size: 7px; line-height: 10px;">
                <tr>
                    <td width="4%"><?=$doprinosi_num;?>.11</td>
                    <td width="19.2%" style="text-align: left;">Ukupno:</td>
                    <td width="15.2%"></td>
                    <td width="19.2%"><?php
                        echo SIMAHtml::number_format(
                                $model->base_pio_comp + $model->base_zdr_comp + $model->base_nez_comp +
                                $model->additionals_pio_comp + $model->additionals_zdr_comp + $model->additionals_nez_comp
                        );
                        ?></td>
                    <td width="21.2%"><?php echo SIMAHtml::number_format($model->prev_pio_comp + $model->prev_zdr_comp + $model->prev_nez_comp); ?></td>
                    <td width="21.2%">  <?php
                        echo SIMAHtml::number_format($model->pio_comp + $model->zdr_comp + $model->nez_comp);
                        ?>
                    </td>
                    </td>
                </tr>
            </table>
            <!--Ukupno doprinosi na teret poslodavca-->  

        </td>
    </tr>
</table>
<!--  
  
<!--NOVA TABELA--> 


<!--vertikalni razmak--> 
<table>
    <tr>
        <td height="10px"></td>
    </tr>
</table>
<table width="100%" cellpadding="0" cellspacing="0" style="font-size: 7px; line-height: 10px;">
    <!--vertikalni razmak--> 
    <?php
    $prev_neto = $model->prev_bruto_sum - $model->prev_tax_empl - $model->prev_pio_empl - $model->prev_zdr_empl - $model->prev_nez_empl;
    $calc_neto = 
            $model->base_bruto + $model->additionals_sum 
            - $model->getEmployeeContributions() 
            - ($model->base_tax_empl + $model->additionals_tax_empl);
//    error_log("$prev_neto = $model->prev_bruto_sum - $model->prev_tax_empl - $model->prev_pio_empl - $model->prev_zdr_empl - $model->prev_nez_empl;");
    ?>
    <tr>
        <td width="100%">
            <!--Umanjenje zarade zaposlenog (NETO)--> 
            <table width="100%" cellpadding="1" cellspacing="0" style="border: 1px solid #939597; text-align: right;  font-size: 7px; line-height: 10px;">
                <tr>
                    <td width="4%" style="vertical-align: top"><?=$doprinosi_num;?>.12</td>
                    <td width="19.2%" style="text-align: left;"> Zarada zaposlenog umanjena za porez i doprinose (NETO)</td>
                    <td width="15.2%"></td>
                    <td width="19.2%"><?php echo SIMAHtml::number_format($calc_neto); ?></td>
                    <td width="21.2%"><?php echo SIMAHtml::number_format($prev_neto); ?></td>
                    <td width="21.2%" style="font-weight: bold;"><?php echo SIMAHtml::number_format($model->amount_neto); ?></td>
                </tr>
            </table>
            <!--Umanjenje zarade zaposlenog (NETO)--> 
        </td>
    </tr>
</table>