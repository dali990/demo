<?php 
$odbici_num = $section_counter; 
$section_counter++;
?>

<table width="100%" cellpadding="0" cellspacing="0" style="font-size: 7px; line-height: 10px;">
    <tr>
        <td width="100%">
            <table width="100%" cellpadding="1" cellspacing="0" style="border: 1px solid #939597; border-bottom: none; font-weight: bold; background-color: #e6e7e8; text-align: left;  font-size: 7px; line-height: 10px;">
                <tr>
                    <td width="4%"><?=$odbici_num;?>.</td>
                    <td width="96%">ODBICI</td>
                </tr>
            </table>
            <table width="100%" cellpadding="1" cellspacing="0" style="border: 1px solid #939597; text-align: right;  font-size: 7px; line-height: 10px;">
                <tr>
                    <td width="5%" style="text-align: right;">Br.</td>
                    <td width="50%" style="text-align: center;">Naziv</td>
                    <td width="25%">Iznos</td>
                </tr>
                <?php
                $counter = 0;
//                foreach ($model->deductions_not_hotmeal as $deduction) {
                foreach ($model->deductions as $deduction) {
                    $counter++;
                    ?>
                    <tr>
                        <td style="text-align: right;"><?php echo $counter; ?>.</td>
                        <td style="text-align: center;"><?php echo $deduction->DisplayName; ?></td> 
                        <td style="text-align: right;"><?php echo SIMAHtml::number_format($deduction->amount); ?></td>
                    </tr>
                    <?php
                }
                ?>
            </table>
        </td>
    </tr>
</table>