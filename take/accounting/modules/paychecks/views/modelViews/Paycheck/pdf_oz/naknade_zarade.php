<?php 
$naknade_zarade_num = $section_counter; 
$section_counter++;
$item_counter = 1;

$naknade_zarade_hours = 0;
$naknade_zarade_value = 0;

?>

<table width="100%" cellpadding="1" cellspacing="0" style="border: 1px solid #939597; border-bottom: none; text-align: right; font-size: 7px; line-height: 10px;">
<tr>
   <td width="7%"> Br.</td>
   <td width="49%" style="text-align: left;"> Elementi</td>
   <td width="11%">Sati</td>
   <td width="10%">Koeficijenti</td>
   <td width="23%">Iznos</td>
</tr>
</table>
<!--Nazivi kolona u tabeli--> 


<table width="100%" cellpadding="1" cellspacing="0" style="border: 1px solid #939597; border-bottom: none; font-weight: bold; background-color: #e6e7e8; text-align: left; font-size: 7px; line-height: 10px;">
<tr>
   <td width="7%"><?=$naknade_zarade_num;?>.</td>
   <td width="93%"> NAKNADE ZARADE</td>
</tr>
</table>


<!--Bolovanje do 30 dana--> 
<?php if (count($model->additionals_sick_leave_till_30) > 0) {?>
<table width="100%" cellpadding="1" cellspacing="0" style="border: 1px solid #939597; border-bottom: none; text-align: right; font-size: 7px; line-height: 10px;">
<tr>
   <td width="7%"><?=$naknade_zarade_num.'.'.$item_counter++;?></td>
   <td width="49%" style="text-align: left;"> Bolovanje do 30 dana</td>
   <?php 
        $sick_leave_till_30_hours = 0;
        $sick_leave_till_30_percent = 0;
        $sick_leave_till_30_value = 0;
        $additionals_sick_leave_till_30 = $model->additionals_sick_leave_till_30;
        foreach($additionals_sick_leave_till_30 as $additional)
        {
            $sick_leave_till_30_hours += $additional->hours;
            $sick_leave_till_30_value += $additional->bruto;
            $sick_leave_till_30_percent = $additional->hours_percentage;
        }
        $naknade_zarade_hours += $sick_leave_till_30_hours;
        $naknade_zarade_value += $sick_leave_till_30_value;
    ?>
   <td width="11%"><?php echo $sick_leave_till_30_hours; ?></td>
   <td width="10%"><?php echo SIMAHtml::number_format($sick_leave_till_30_percent, 0); ?>%</td>
   <td width="23%"><?php echo SIMAHtml::number_format($sick_leave_till_30_value); ?></td>
</tr>
</table>
<!--Bolovanje do 30 dana--> 
<?php }?>

<!--Godišnji odmor--> 
<?php if (count($model->additionals_anual_leave) > 0) {?>
<table width="100%" cellpadding="1" cellspacing="0" style="border: 1px solid #939597; border-bottom: none; text-align: right; font-size: 7px; line-height: 10px;">
<tr>
   <td width="7%"><?=$naknade_zarade_num.'.'.$item_counter++;?></td>
   <td width="49%" style="text-align: left;"> Godišnji odmor</td>
   <?php 
        $anual_leave_hours = 0;
        $anual_leave_percent = 0;
        $anual_leave_value = 0;
        $additionals_anual_leave = $model->additionals_anual_leave;
        foreach($additionals_anual_leave as $additional)
        {
            $anual_leave_hours += $additional->hours;
            $anual_leave_value += $additional->bruto;
        }
        $naknade_zarade_hours += $anual_leave_hours;
        $naknade_zarade_value += $anual_leave_value;
    ?>
   <td width="11%"><?php echo $anual_leave_hours; ?></td>
   <td width="10%"><?php echo SIMAHtml::number_format($anual_leave_percent, 0); ?>%</td>
   <td width="23%"><?php echo SIMAHtml::number_format($anual_leave_value); ?></td>
</tr>
</table>
<!--Godišnji odmor--> 
<?php }?>

<!--Bolovanje 100%--> 
<?php if (count($model->additionals_sick_leave_100) > 0) {?>
<table width="100%" cellpadding="1" cellspacing="0" style="border: 1px solid #939597; border-bottom: none; text-align: right; font-size: 7px; line-height: 10px;">
<tr>
   <td width="7%"><?=$naknade_zarade_num.'.'.$item_counter++;?></td>
   <td width="49%" style="text-align: left;"> Bolovanje 100%</td>
   <?php 
        $sick_leave_100_hours = 0;
        $sick_leave_100_percent = 0;
        $sick_leave_100_value = 0;
        $additionals_sick_leave_100 = $model->additionals_sick_leave_100;
        foreach($additionals_sick_leave_100 as $sl)
        {
            $sick_leave_100_hours += $sl->hours;
            $sick_leave_100_value += $sl->bruto;
            $sick_leave_100_percent = $sl->hours_percentage;
        }
        $naknade_zarade_hours += $sick_leave_100_hours;
        $naknade_zarade_value += $sick_leave_100_value;
    ?>
   <td width="11%"><?php echo $sick_leave_100_hours; ?></td>
   <td width="10%"><?php echo SIMAHtml::number_format($sick_leave_100_percent, 0); ?>%</td>
   <td width="23%"><?php echo SIMAHtml::number_format($sick_leave_100_value); ?></td>
</tr>
</table>
<!--Bolovanje 100%--> 
<?php }?>

<!--Državni i verski praznici--> 
<?php if (count($model->additionals_state_and_religious_holiday) > 0) {?>
<table width="100%" cellpadding="1" cellspacing="0" style="border: 1px solid #939597; border-bottom: none; text-align: right; font-size: 7px; line-height: 10px;">
<tr>
   <td width="7%"><?=$naknade_zarade_num.'.'.$item_counter++;?></td>
   <td width="49%" style="text-align: left;"> Državni i verski praznici</td>
   <?php 
        $state_religious_hours = 0;
        $state_religious_percent = 0;
        $state_religious_value = 0;
        $additionals_state_and_religious_holiday = $model->additionals_state_and_religious_holiday;
        foreach($additionals_state_and_religious_holiday as $additional)
        {
            $state_religious_hours += $additional->hours;
            $state_religious_value += $additional->bruto;
        }
        $naknade_zarade_hours += $state_religious_hours;
        $naknade_zarade_value += $state_religious_value;
    ?>
   <td width="11%"><?php echo $state_religious_hours; ?></td>
   <td width="10%"><?php echo SIMAHtml::number_format($state_religious_percent, 0); ?>%</td>
   <td width="23%"><?php echo SIMAHtml::number_format($state_religious_value); ?></td>
</tr>
</table>
<!--Državni i verski praznici--> 
<?php }?>

<!--Placeno odsustvo--> 
<?php if (count($model->additionals_paid_leave) > 0) {?>
<table width="100%" cellpadding="1" cellspacing="0" style="border: 1px solid #939597; border-bottom: none; text-align: right; font-size: 7px; line-height: 10px;">
<tr>
   <td width="7%"><?=$naknade_zarade_num.'.'.$item_counter++;?></td>
   <td width="49%" style="text-align: left;"> Plaćeno odsustvo</td>
   <?php 
        $paidleave_hours = 0;
        $paidleave_percent = 0;
        $paidleave_value = 0;
        $additionals_paid_leave = $model->additionals_paid_leave;
        foreach($additionals_paid_leave as $additional)
        {
            $paidleave_hours += $additional->hours;
            $paidleave_value += $additional->bruto;
        }
        $naknade_zarade_hours += $paidleave_hours;
        $naknade_zarade_value += $paidleave_value;
    ?>
   <td width="11%"><?php echo $paidleave_hours; ?></td>
   <td width="10%"><?php echo SIMAHtml::number_format($paidleave_percent, 0); ?>%</td>
   <td width="23%"><?php echo SIMAHtml::number_format($paidleave_value); ?></td>
</tr>
</table>
<?php }?>

<!--Vojna sluzba--> 
<?php if (count($model->additionals_paid_leave_military_service) > 0) {?>
    <table width="100%" cellpadding="1" cellspacing="0" style="border: 1px solid #939597; border-bottom: none; text-align: right; font-size: 7px; line-height: 10px;">
    <tr>
       <td width="7%"><?=$naknade_zarade_num.'.'.$item_counter++;?></td>
       <td width="49%" style="text-align: left;"> Vojna služba</td>
       <?php 
            $paidleave_militaryservice_hours = 0;
            $paidleave_militaryservice_percent = 0;
            $paidleave_militaryservice_value = 0;
            $additionals_paid_leave_militaryservice = $model->additionals_paid_leave_military_service;
            foreach($additionals_paid_leave_militaryservice as $additional)
            {
                $paidleave_militaryservice_hours += $additional->hours;
                $paidleave_militaryservice_value += $additional->bruto;
            }
            $naknade_zarade_hours += $paidleave_militaryservice_hours;
            $naknade_zarade_value += $paidleave_militaryservice_value;
        ?>
       <td width="11%"><?php echo $paidleave_militaryservice_hours; ?></td>
       <td width="10%"><?php echo SIMAHtml::number_format($paidleave_militaryservice_percent, 0); ?>%</td>
       <td width="23%"><?php echo SIMAHtml::number_format($paidleave_militaryservice_value); ?></td>
    </tr>
    </table>
<?php }?>


<table width="100%" cellpadding="1" cellspacing="0" style="border: 1px solid #939597; text-align: right; font-weight: bold; font-size: 7px; line-height: 10px;">
<tr>
   <td width="7%"><?=$naknade_zarade_num;?>.SUM</td>
   <td width="49%" style="text-align: left;"> Ukupno naknade</td>
   <td width="11%"><?php echo $naknade_zarade_hours; ?></td>
   <td width="10%"></td>
   <td width="23%"><?php echo SIMAHtml::number_format($naknade_zarade_value); ?>
   </td>
</tr>
</table>
