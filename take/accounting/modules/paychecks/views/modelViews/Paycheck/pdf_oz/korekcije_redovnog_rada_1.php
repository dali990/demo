<?php 
$korekcija_redovnog_rada = $section_counter; 
$section_counter++;

$attributes_in_section = [
    [
        'title' => 'Minuli rad',
        'hours' => $model->regular_work_hours,
        'coeficient' => SIMAHtml::number_format($model->exp_perc, 1),
        'value' => $model->past_exp_value
    ],
    [
        'title' => 'Ucinak',
        'hours' => $model->regular_work_hours,
        'coeficient' => SIMAHtml::number_format($model->performance_percent, 0),
        'value' => $model->performance_value
    ],
    [
        'title' => 'Pripravnicki',
        'hours' => $model->regular_work_hours,
        'coeficient' => SIMAHtml::number_format($model->probationer_percent, 0),
        'value' => $model->probationer_value
    ]
];
?>

<table width="100%" cellpadding="1" cellspacing="0" style="border: 1px solid #939597; border-bottom: none; text-align: right; font-size: 7px; line-height: 10px; vertical-align: top;">
   <tr>
       <td width="7%">Br.</td>
       <td width="49%" style="text-align: left;"> Elementi</td>
       <td width="11%">Sati</td>
       <td width="10%">Koeficijenti</td>
       <td width="23%">Iznos</td>
   </tr>
</table>

<table width="100%" cellpadding="1" cellspacing="0" style="border: 1px solid #939597; border-bottom: none; font-weight: bold; background-color: #e6e7e8; text-align: left; font-size: 7px; line-height: 10px;">
   <tr>
       <td  width="7%"><?=$korekcija_redovnog_rada;?>.</td>
       <td width="93%">KOREKCIJE REDOVNOG RADA</td>
   </tr>
</table>

<?php
$korekcija_redovnog_rada_counter = 1;
foreach($attributes_in_section as $attribute_in_section)
{
    ?>
    <table width="100%" cellpadding="1" cellspacing="0" style="border: 1px solid #939597; border-bottom: none; text-align: right; font-size: 7px; line-height: 10px;">
       <tr>
           <td width="7%"><?=$korekcija_redovnog_rada.'.'.$korekcija_redovnog_rada_counter;?></td>
           <td width="49%" style="text-align: left;"><?php echo $attribute_in_section['title']; ?></td>
           <td width="11%"><?php echo $attribute_in_section['hours']; ?></td>
           <td width="10%"><?php echo $attribute_in_section['coeficient']; ?>%</td>
           <td width="23%"><?php echo SIMAHtml::number_format($attribute_in_section['value']); ?></td>
       </tr>
    </table>
    <?php
    $korekcija_redovnog_rada_counter++;
}
?>

<?php 
$korekcija_redovnog_rada_value = $model->past_exp_value + $model->performance_value + $model->probationer_value; 
?>
<table width="100%" cellpadding="1" cellspacing="0" style="border: 1px solid #939597; border-botom: none; text-align: right; font-weight: bold; font-size: 7px; line-height: 10px;">
   <tr>
       <td width="7%"><?=$korekcija_redovnog_rada;?>.SUM</td>
       <td width="49%" style="text-align: left;"> Ukupno</td>
       <td width="11%"></td>
       <td width="10%"></td>
       <td width="23%"><?php echo SIMAHtml::number_format($korekcija_redovnog_rada_value); ?></td>
   </tr>
</table>
