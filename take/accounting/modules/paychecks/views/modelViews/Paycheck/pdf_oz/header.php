
<table width="100%" cellpadding="1" cellspacing="0" style="text-align: left; font-size: 7px; line-height: 10px;" >
    <tr>
        <td>Naziv i sedište poslodavca: <span style="font-weight: bold; font-size: 8px;"><?php 
        echo Yii::app()->company->DisplayName.', '.Yii::app()->company->MainAddress->DisplayName;
    ?></span></td>
    </tr>
    <tr>
        <td>PIB: <?php echo Yii::app()->company->PIB; ?></td>
    </tr>
    <tr>
        <td>Maticni broj: <?php echo Yii::app()->company->MB; ?></td>
    </tr>
    <tr>
        <td>Broj racuna: <?php echo Yii::app()->company->partner->MainBankAccount->number.' '.Yii::app()->company->partner->MainBankAccount->bank; ?></td>
    </tr>
</table>

<table>
    <tr>
        <td height="10px"></td>
    </tr>
</table>

<?php

$title = $model->paycheck_period->getAttributeDisplay('oz_header_title');

?>

<table width="100%" cellpadding="0" cellspacing="0" style="font-size: 7px; line-height: 10px;">
    <tr style="color: #000">
        <td>
            <table width="100%" cellpadding="1" cellspacing="0" style="font-size: 7px; line-height: 10px;">
                <tr>
                    <td  style="font-weight: bold; font-size: 11px; text-align: left;"><?php echo $title; ?></td>
                    <td style="text-align: right;">Datum dospeća: <?php echo $model->paycheck_period->payment_date ?></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table width="100%" cellpadding="0" cellspacing="0" style="font-size: 7px; line-height: 10px;">
                <tr>
                    <td style="text-align: left;">Isplata za mesec: <span style="font-weight: bold;"><?php echo $model->paycheck_period->month->month; ?>. <?php echo $model->paycheck_period->month->year ?>.</span></td>
                    <td style="text-align: right;">Ime i prezime zaposlenog: <span style="font-weight: bold;"> <?php echo $model->employee_display_name; ?></span></td>
                </tr>
                <tr>
                    <td style="text-align: left;">Mogući sati rada: <span style="font-weight: bold;"> <?php echo $model->paycheck_period->month_hours; ?></span></td>
                    <td style="text-align: right;">JMBG: <?php echo $model->employee->person->JMBG; ?></td>
                </tr>
                <tr>
                    <td style="text-align: left;">Broj radnika: <?php echo $model->employee->order_in_company; ?></td>
                    <td style="text-align: right;">Adresa: <?php echo $model->employee->person->MainAddress->DisplayName; ?></td>
                </tr>
                <?php 
                    if($model->isPayoutByCache() === false)
                    {
                ?>
                    <tr>
                        <td style="text-align: left;"></td>
                        <td style="text-align: right;">Banka i tekući račun: <?php 
                        $mba = $model->employee->person->partner->MainBankAccount;
                        echo $mba->number.' '.$mba->bank; 
                        ?></td>
                    </tr>
                <?php 
                    } 
                ?>
            </table>
        </td>
    </tr>
</table>

<table>
    <tr>
        <td height="10px"></td>
    </tr>
</table>
<table width="100%" cellpadding="0" cellspacing="0" style="font-size: 7px; line-height: 10px;">
    <tr>
        <td>
            <table border="1" rules="rows" width="100%" cellpadding="1" cellspacing="0" style="border: 1px solid #939597; text-align: right; font-size: 7px; line-height: 10px;">
                <tr style="vertical-align: top;">
                    <td width="2%"><?=$section_counter;?>.1</td>
                    <td width="19%" style="text-align: left;"> Naziv radnog mesta</td>
                    <td width="2%"><?=$section_counter;?>.2</td>
                    <td width="10%" >Broj bodova radne pozicije</td>
                    <td width="2%"><?=$section_counter;?>.3</td>
                    <td width="10%" style="border-right: 1px solid #aaa;">Broj odradjenih bodova</td>
                    
                    <td width="2%"><?=$section_counter;?>.4</td>
                    <td width="7%" style="border-right: 1px solid #aaa;">Broj kalendarskih dana</td>
                    
                    <td width="2%" style="background-color: #e6e7e8;"><?=$section_counter;?>.5</td>
                    <td width="4%" style="border-right: 1px solid #aaa; font-weight: bold; background-color: #e6e7e8;">Vrednost Boda</td>
                    <td width="2%" style="border-left: 1px solid #aaa;"><?=$section_counter;?>.6</td>
                    <td width="30%" colspan="3" style="border-right: 1px solid #aaa;">Prethodnih 12 meseci - Prosečna vrednost sata</td>
                    <td width="2%"><?=$section_counter;?>.7</td>
                    <td width="5%">Radni staž</td>
                </tr>
            <!--</table>-->           
            <!--<table width="100%" cellpadding="1" cellspacing="0" style="border: 1px solid #939597; text-align: right; font-size: 7px; line-height: 10px;">-->
                <tr>
                    <td width="2%"></td>
                	<td width="19%" style="text-align: left;"> 
                        <?php 
                            /// privremeno resenje jer je za dosadasnje zarade ovo polje prazno
                            /// kasnije ukloniti if i staviti da je polje obavezno
                            $work_position_name = $model->work_position_name;
                            if(empty($work_position_name))
                            {
                                $active_wc = $model->employee->getWorkContractActiveAt($model->paycheck_period->month);
                                if(!empty($active_wc))
                                {
                                    $work_position_name = $active_wc->work_position->DisplayName;
                                }
                            }
                            
                            echo $work_position_name;
                        ?>
                        </td>
                    <td width="2%"></td>
                    <td width="10%"><?php echo $model->work_position_points; ?></td>
                    <td width="2%"></td>
                    <td width="9%" style="border-right: 1px solid #aaa;"><?php echo $model->points; ?></td>
                    
                    <td width="2%"></td>
                    <td width=7%" style="border-right: 1px solid #aaa;">
                        <?php
                            $number_of_calendar_days = $model->number_of_calendar_days;
                            foreach($model->additionals as $pa)
                            {
                                $number_of_calendar_days += $pa->number_of_calendar_days;
                            }
                            echo $number_of_calendar_days.' dana';
                        ?>
                    </td>
                    
                    <td width="2%" style="background-color: #e6e7e8;"></td>
                    <td width="5%" style="border-right: 1px solid #aaa; font-weight: bold; background-color: #e6e7e8;"><?php echo $model->point_value; ?></td>
                    
                    
                    <td width="2%" style="border-left: 1px solid #aaa;"></td>
                    <td width="10%" style="border-right: 1px solid #aaa;"> <?php echo SIMAHtml::number_format($model->last_12_months_value); ?> din</td>
                    <td width="8%" style="border-right: 1px solid #aaa;">  <?php echo $model->last_12_months_hours; ?> sati</td>
                    <td width="11%" style="border-right: 1px solid #aaa;">
                        <?php  
                            if (!empty($model->last_12_months_value) && !empty($model->last_12_months_hours))
                            {
                                $hour_value = $model->last_12_months_value / $model->last_12_months_hours;
                            } else {
                                $hour_value = 0;
                            }
                            echo SIMAHtml::number_format($hour_value);
                        ?> din/h</td>
                    <td width="2%"></td>
                    <td width="5%"><?php echo $model->year_experience ?> god.</td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<?php $section_counter++; ?>