<?php 
$administrativne_zabrane_num = $section_counter; 
$section_counter++;
?>

<table width="100%" cellpadding="0" cellspacing="0" style="font-size: 7px; line-height: 10px;">
    <tr>
        <td width="100%">
            <!--Nazivi kolona za obustave--> 
            <table width="100%" cellpadding="1" cellspacing="0" style="border: 1px solid #939597; border-bottom: none; font-weight: bold; background-color: #e6e7e8; text-align: left;  font-size: 7px; line-height: 10px;">
                <tr>
                    <td width="4%"><?=$administrativne_zabrane_num;?>.</td>
                    <td width="96%"> ADMINISTRATIVNE ZABRANE</td>
                </tr>
            </table>
            <!--Nazivi kolona za obustave--> 
            <table width="100%" cellpadding="1" cellspacing="0" style="border: 1px solid #939597; text-align: right;  font-size: 7px; line-height: 10px;">
                <tr>
                    <td width="4%" style="text-align: right;">Br.</td>
                    <td width="19.2%" style="text-align: center;"> Kreditor</td>
                    <td width="15.2%" style="text-align: center;">Naziv</td>
                    <td width="19.2%">Ukupno</td>
                    <td width="21.2%">Preostalo nakon isplate</td>
                    <td width="21.2%">Iznos rate</td>
                </tr>
                <?php
                $suspensions_sum = 0;
                $counter = 0;
                foreach ($model->suspensions as $suspension) {
                    $counter++;
                    $suspensions_sum += $suspension->amount;
                    ?>
                    <tr>
                        <td style="text-align: right;"><?php echo $counter; ?>.</td>
                        <td style="text-align: center;"><?php echo $suspension->suspension->creditor->DisplayName; ?></td> 
                        <td style="text-align: center;"><?php echo $suspension->suspension->DisplayName; ?></td> 
                        <td style="text-align: right;"><?php echo SIMAHtml::number_format($suspension->suspension->total_amount); ?></td>
                        <td style="text-align: right;"><?php echo SIMAHtml::number_format($suspension->suspension->left_to_pay_amount); ?></td>
                        <td style="text-align: right;"><?php echo SIMAHtml::number_format($suspension->amount); ?></td>
                    </tr>
                    <?php
                }
                ?>
            </table>

            <!--OBUSTAVE--> 
        </td>
    </tr>
    <!--NOVA TABELA--> 
</table>