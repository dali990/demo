<PodaciOPrihodima>
<RedniBroj><?php echo $params['orderNum']; ?></RedniBroj>
<VrstaIdentifikatoraPrimaoca><?php echo 1; ?></VrstaIdentifikatoraPrimaoca>
<IdentifikatorPrimaoca><?php echo $model->employee->person->JMBG; ?></IdentifikatorPrimaoca>
<Prezime><?php echo strtoupper(SIMAMisc::RemoveSerbianLetters(trim($model->employee->person->lastname))); ?></Prezime>
<Ime><?php echo strtoupper(SIMAMisc::RemoveSerbianLetters(trim($model->employee->person->firstname))); ?></Ime>
<OznakaPrebivalista><?php echo $model->employee->municipality->code; ?></OznakaPrebivalista>
<SVP><?php echo $model->employee_svp; ?></SVP>
<BrojKalendarskihDana><?php echo $model->number_of_calendar_days; ?></BrojKalendarskihDana>
<BrojEfektivnihSati><?=$efective_worked_hours?></BrojEfektivnihSati>
<MesecniFondSati><?php echo number_format($model->month_period_hours, 1); ?></MesecniFondSati>
<Bruto><?=$bruto?></Bruto>
<OsnovicaPorez><?=$tax_base?></OsnovicaPorez>
<Porez><?=$porez?></Porez>
<OsnovicaDoprinosi><?=$doprinosi_base?></OsnovicaDoprinosi>
<PIO><?=$pio?></PIO>
<ZDR><?=$zdr?></ZDR>
<NEZ><?=$nez?></NEZ>
<PIOBen>0.00</PIOBen>
<?php 
if(count($mfps) > 0)
{
?>
<DeklarisaniMFP>
<?php
    foreach($mfps as $mfp)
    {
?>
<MFP>
<Oznaka><?php echo $mfp['code']; ?></Oznaka>
<Vrednost><?php echo $mfp['value']; ?></Vrednost>
</MFP>
<?php
    }
?>
</DeklarisaniMFP>
<?php
}
?>
</PodaciOPrihodima>
