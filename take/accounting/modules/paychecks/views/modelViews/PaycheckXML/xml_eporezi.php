<?php $_isBonus = $model->paycheck->paycheck_period->isBonus;?>

<PodaciOPrihodima>
<RedniBroj><?=$params['orderNum']?></RedniBroj>
<VrstaIdentifikatoraPrimaoca><?=$model->indentification_type?></VrstaIdentifikatoraPrimaoca>
<IdentifikatorPrimaoca><?=$model->indentification_value?></IdentifikatorPrimaoca>
<Prezime><?=strtoupper($model->lastname)?></Prezime>
<Ime><?=strtoupper($model->firstname)?></Ime>
<OznakaPrebivalista><?=$model->municipality_code?></OznakaPrebivalista>
<SVP><?=$model->svp?></SVP>
<BrojKalendarskihDana><?=$model->number_of_calendar_days?></BrojKalendarskihDana>
<?php if (!$_isBonus){ ?>
<BrojEfektivnihSati><?=$model->getAttributeDisplay('effective_hours')?></BrojEfektivnihSati>
<MesecniFondSati><?=$model->getAttributeDisplay('month_hours')?></MesecniFondSati>
<?php } ?>
<Bruto><?=$model->getAttributeDisplay('bruto')?></Bruto>
<OsnovicaPorez><?=$model->getAttributeDisplay('tax_base')?></OsnovicaPorez>
<Porez><?=$model->getAttributeDisplay('tax_empl')?></Porez>
<OsnovicaDoprinosi><?=$model->getAttributeDisplay('doprinosi_base')?></OsnovicaDoprinosi>
<PIO><?=$model->getAttributeDisplay('pio')?></PIO>
<ZDR><?=$model->getAttributeDisplay('zdr')?></ZDR>
<NEZ><?=$model->getAttributeDisplay('nez')?></NEZ>
<PIOBen><?=$model->getAttributeDisplay('pio_ben')?></PIOBen>
<?php 
if(count($mfps) > 0)
{
?>
<DeklarisaniMFP>
<?php
    foreach($mfps as $mfp)
    {
?>
<MFP>
<Oznaka><?php echo $mfp['code']; ?></Oznaka>
<Vrednost><?php echo $mfp['value']; ?></Vrednost>
</MFP>
<?php
    }
?>
</DeklarisaniMFP>
<?php
}
?>
</PodaciOPrihodima>
