<?php

$uniq = SIMAHtml::uniqid();
$ppppd_div_id = $uniq.'regenerate_ppppd_div';

$generatePPPPDfunc = "sima.paychecksPPPPD.generatePPPPD({$model->id}, function(){"
                        ."sima.refreshActiveTab($('#{$ppppd_div_id}'));"
                    . "});";

?>
<div id="<?= $ppppd_div_id; ?>" class="sima-layout-panel">
<?php
    
if($model->ppppds_count > 0)
{
    $this->widget(SIMAGuiTable::class, [
        'model' => PPPPD::model(),            
        'fixed_filter'=>[
            'paycheck_periods_to_ppppds' => [
                'paycheck_period' => [
                    'ids' => $model->id
                ]
            ]
        ],
        'custom_buttons' => [
            Yii::t('PaychecksModule.PPPPD', 'Regenerate') => [
                'func' => "sima.paychecksPPPPD.generatePPPPD({$model->id}, function(){"
                        ."sima.refreshActiveTab($('#{$ppppd_div_id}'));"
                    . "});"
            ],
            Yii::t('PaychecksModule.PPPPD', 'RegenerateCompletely') => [
                'func' => "sima.paychecksPPPPD.generatePPPPD({$model->id}, function(){"
                        ."sima.refreshActiveTab($('#{$ppppd_div_id}'));"
                    . "}, true);"
            ],
            Yii::t('PaychecksModule.PPPPD', 'BindExistingPPPPD') => [
                'func' => "sima.paychecksPPPPD.bindExistingPPPPD({$model->id}, {$ppppd_div_id});"
            ],
        ],
        'onSelectionChange' => [
            'sima.paychecksPPPPD.onPPPDTabXMLSelectionChange',
            $params['ppppd_xmls_guitable_id']
        ],
        'add_button' => [
            'formName' => 'paycheck_period_ppppd_tab',
            'scenario' => 'paycheck_period_ppppd_tab',
            'init_data' => [
                'PPPPD' => [
                    'creation_type' => PPPPD::$CREATE_TYPE_USER_MANUAL,
                    'year_id' => $model->year->id,
                    '_paycheck_period_id' => $model->id
                ]
            ]
        ]
    ]);
}
else
{
    echo Yii::t('PaychecksModule.PaycheckPeriod', 'NOPPPDS');
    if($model->confirmed === true)
    {
        $buttonId = SIMAHtml::uniqid();
        $action = [
            'title' => Yii::t('PaychecksModule.PaycheckPeriod', 'GenerateInitialPPPPD'),
            'onclick' => [
                'sima.paychecksPPPPD.generatePPPPD',
                $model->id, "#$ppppd_div_id"
            ]
        ];
        if(SIMAMisc::isVueComponentEnabled())
        {
            $action['id'] = $buttonId;
            $this->widget(SIMAButtonVue::class, $action);
        }
        else
        {
            $this->widget('SIMAButton', [
                'id' => $buttonId,
                'action' => $action
            ]);
        }
    }
}
?>

</div>