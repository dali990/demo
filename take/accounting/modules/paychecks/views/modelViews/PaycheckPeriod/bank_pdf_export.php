<div style="width: 100%; margin-bottom: 70px;">
    Datum obračuna: <?=$params['calc_date']; ?>
    <center>SPECIFIKACIJA TEKUĆIH RAČUNA</center>
    <div style="float:left; width: 60%">
        <?php if(isset($params['unifiedPaycheck']) && !is_null($params['unifiedPaycheck'])) { ?>
        Banka: <?php echo $params['unifiedPaycheck']->bank->DisplayName ?></br>
        Tek. Račun: <?php echo $params['unifiedPaycheck']->bank_account->number ?>
        <?php } ?>
    </div>
    <div style="float:left; width: 40%">
        Svrha: zarade </br>
        Sedište: <?php echo Yii::app()->company->partner->MainAddress->city->DisplayName; ?>
    </div>
</div>
<div style="width: 100%">
    <table style="width: 100%">
        <tr>
            <?php if($params['with_bank_account'] === true) { ?>
            <td align="center">Broj Računa</td>
            <?php } ?>
            <td align="center">Ime i prezime</td> 
            <td align="center">Matični broj</td> 
            <td align="center">Za isplatu</td>
        </tr>
    <?php 
    $sum = 0;

    foreach($params['paychecks'] as $paycheck)
    {
    ?>
        <tr>
            <?php if($params['with_bank_account'] === true) { ?>
            <td><b><?php 
                $main_bank_acc = $paycheck->employee->person->MainBankAccount;
                echo isset($main_bank_acc)?$main_bank_acc:'tekuci racun nije postavljen'; 
            ?></b></td>
            <?php } ?>
            <td><?php echo $paycheck->employee_display_name; ?></td> 
            <td><?php echo $paycheck->employee->person->JMBG ?></td> 
            <td align="right"><b><?php echo number_format($paycheck->amount_neto_for_payout, 2, ',', '.') ?></b></td> 
        </tr>
    <?php
        $sum += $paycheck->amount_neto_for_payout;
    }
    ?>
        <tr>
            <?php if($params['with_bank_account'] === true) { ?>
            <td></td>
            <?php } ?>
            <td></td> 
            <td>ukupno:</td> 
            <td align="right" style="border-top: 1px solid black;"><b><?php echo number_format($sum, 2, ',', '.') ?></b></td> 
        </tr>
    </table>
</div>