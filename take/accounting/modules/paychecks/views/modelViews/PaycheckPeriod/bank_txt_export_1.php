<?php

$sum = 0;

foreach($params['paychecks'] as $paycheck)
{
    $row = '';
    
    if($params['with_bank_account'] === true)
    {
        $row .= str_replace('-', '', $paycheck->employee->person->MainBankAccount->number).'  ';
    }
    
    $row .= $paycheck->employee->person->JMBG
            .str_pad(number_format($paycheck->amount_neto_for_payout, 2, '', ''), 19, '0', STR_PAD_LEFT)."\n";
    
    echo $row;
    
    $sum += $paycheck->amount_neto;
}

echo '000000000000000000               '.str_pad(number_format($sum, 2, '', ''), 19, '0', STR_PAD_LEFT)."\n";

?>
