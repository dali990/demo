<?php 
$uniq = SIMAHtml::uniqid(); 
$buttons_id = 'buttons'.$uniq;
?>

<div id='<?php echo $buttons_id; ?>' class='buttons top-paychecks'>
<?php
    if($model->confirmed === true)
    {
        if(SIMAMisc::isVueComponentEnabled())
        {
            echo Yii::app()->controller->widget(SIMAButtonVue::class, [
                'title'=>Yii::t('PaychecksModule.PaycheckPeriod', 'ImportPaychecksXML'),
                'onclick'=>['sima.paychecks.importPaychecksXML',"$buttons_id","$model->id"]
            ],true);
        }
        else
        {
            echo Yii::app()->controller->widget('SIMAButton', [
                'action'=>[ 
                    'title'=>Yii::t('PaychecksModule.PaycheckPeriod', 'ImportPaychecksXML'),
                    'onclick'=>['sima.paychecks.importPaychecksXML',"$buttons_id","$model->id"]
                ]
            ],true);
        }
        ?>
        |
        <?php
        if(SIMAMisc::isVueComponentEnabled())
        {
            echo Yii::app()->controller->widget(SIMAButtonVue::class, [
                'title'=>Yii::t('PaychecksModule.PaycheckPeriod', 'CreatePayments'),
                'onclick'=>['sima.paychecks.createPayments',"$buttons_id","$model->id"]
            ],true);
        }
        else
        {
            echo Yii::app()->controller->widget('SIMAButton', [
                'action'=>[ 
                    'title'=>Yii::t('PaychecksModule.PaycheckPeriod', 'CreatePayments'),
                    'onclick'=>['sima.paychecks.createPayments',"$buttons_id","$model->id"]
                ]
            ],true);
        }
        ?>
        |
        <?php
        if(SIMAMisc::isVueComponentEnabled())
        {
            echo Yii::app()->controller->widget(SIMAButtonVue::class, [
                'title'=>Yii::t('PaychecksModule.PaycheckPeriod', 'CreateOZFiles'),
                'onclick'=>['sima.paychecks.createOZFiles',"$model->id"]
            ],true);
        }
        else
        {
            echo Yii::app()->controller->widget('SIMAButton', [
                'action'=>[ 
                    'title'=>Yii::t('PaychecksModule.PaycheckPeriod', 'CreateOZFiles'),
                    'onclick'=>['sima.paychecks.createOZFiles',"$model->id"]
                ]
            ],true);
        }
        ?>
        |
        <?php
        if(SIMAMisc::isVueComponentEnabled())
        {
            echo Yii::app()->controller->widget(SIMAButtonVue::class, [
                'title'=>Yii::t('PaychecksModule.PaycheckPeriod', 'SendOZFiles'),
                'onclick'=>['sima.paychecks.sendOZFiles', $model->id, false],
                'subactions_data' => [
                    [
                        'title' => Yii::t('PaychecksModule.PaycheckPeriod', 'GetPaychecksForPrint'),
                        'onclick' => [
                            "sima.paychecks.sendOZFiles", $model->id, true
                        ]             
                    ],
                ]
            ], true);
        }
        else
        {
            echo Yii::app()->controller->widget('SIMAButton', [
                'action'=>[ 
                    'title'=>Yii::t('PaychecksModule.PaycheckPeriod', 'SendOZFiles'),
                    'onclick'=>['sima.paychecks.sendOZFiles', $model->id, false],
                    'subactions' => [
                        [
                            'title' => Yii::t('PaychecksModule.PaycheckPeriod', 'GetPaychecksForPrint'),
                            'onclick' => [
                                "sima.paychecks.sendOZFiles", $model->id, true
                            ]             
                        ],
                    ]
                ]
            ], true);
        }
        ?>
        
        <?php
    }
    else
    {
        if(SIMAMisc::isVueComponentEnabled())
        {
            $this->widget(SIMAButtonVue::class, [
                'title' => Yii::t('PaychecksModule.Paycheck', 'Recalc'),
                'onclick' => ["sima.paychecks.recalcPeriod", "$buttons_id", $model->id, true, false],
                'subactions_data' => [
                    [
                        'title' => Yii::t('PaychecksModule.Paycheck', 'RecalcWithoutChangingAdditionals'),
                        'onclick' => ["sima.paychecks.recalcPeriod", "$buttons_id", $model->id, false, false]
                    ],
                    [
                        'title' => Yii::t('PaychecksModule.Paycheck', 'RecalcForCompareWithoutSave'),
                        'onclick' => ["sima.paychecks.recalcPeriod", "$buttons_id", $model->id, true, true],
                        'subactions_data' => [
                            [
                                'title' => Yii::t('PaychecksModule.Paycheck', 'RecalcForCompareWithoutSaveDetailed'),
                                'onclick' => ["sima.paychecks.recalcPeriod", "$buttons_id", $model->id, true, true, true]
                            ]
                        ]
                    ]
                ]
            ]);
        }
        else
        {
            $this->widget('SIMAButton', [
                'action' => [
                    'title' => Yii::t('PaychecksModule.Paycheck', 'Recalc'),
                    'onclick' => ["sima.paychecks.recalcPeriod", "$buttons_id", $model->id, true, false],
                    'subactions' => [
                        [
                            'title' => Yii::t('PaychecksModule.Paycheck', 'RecalcWithoutChangingAdditionals'),
                            'onclick' => ["sima.paychecks.recalcPeriod", "$buttons_id", $model->id, false, false]
                        ],
                        [
                            'title' => Yii::t('PaychecksModule.Paycheck', 'RecalcForCompareWithoutSave'),
                            'onclick' => ["sima.paychecks.recalcPeriod", "$buttons_id", $model->id, true, true],
                            'subactions' => [
                                [
                                    'title' => Yii::t('PaychecksModule.Paycheck', 'RecalcForCompareWithoutSaveDetailed'),
                                    'onclick' => ["sima.paychecks.recalcPeriod", "$buttons_id", $model->id, true, true, true]
                                ]
                            ]
                        ]
                    ]
                ]
            ]);
        }
    }
?>
</div>