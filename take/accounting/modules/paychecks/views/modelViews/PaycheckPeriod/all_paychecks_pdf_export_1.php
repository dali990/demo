<img style="width: 17cm;" src='<?=$params['header_path']?>'></img>

<div style="width: 100%; margin-bottom: 70px;">
    <center>EVIDENCIJE ZARADE ZAPOSLENIH</center>
    <div style="float:left; width: 40%">
        Mesec: <?=$params['month']?> <br />
    </div>
</div>
<div style="width: 100%">
    <table style="width: 100%">
        <tr>
            <th align="center" style="border-bottom: 1px solid black;">PR</th> 
            <th align="center" style="border-bottom: 1px solid black;">Ime i prezime</th> 
            <th align="center" style="border-bottom: 1px solid black;">Matični broj</th> 
            <th align="center" style="border-bottom: 1px solid black;">Bruto</th>
            <th align="center" style="border-bottom: 1px solid black;">Neto</th>
            <th align="center" style="border-bottom: 1px solid black;">Neto za isplatu</th>
        </tr>
    <?php
        echo $params['paychecks_html'];
    ?>
        <tr>
            <td></td> 
            <td></td> 
            <td>ukupno:</td> 
            <td align="right" style="border-top: 1px solid black;"><b><?php echo number_format($params['sum_bruto'], 2, ',', '.') ?></b></td> 
            <td align="right" style="border-top: 1px solid black;"><b><?php echo number_format($params['sum_neto'], 2, ',', '.') ?></b></td> 
            <td align="right" style="border-top: 1px solid black;"><b><?php echo number_format($params['sum_neto_for_payout'], 2, ',', '.') ?></b></td> 
        </tr>
    </table>
</div>