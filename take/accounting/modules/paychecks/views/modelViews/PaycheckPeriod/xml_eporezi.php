<?='<?xml version="1.0" encoding="utf-8" ?>'//mora ovako jer puca?>

<?php
    $paycheck_period_ppppd = $params['paycheck_period_ppppd'];
?>

<PodaciPoreskeDeklaracije>
    <PodaciOPrijavi>
        <?php
            if(!empty($paycheck_period_ppppd)){
                $client_identificator = $paycheck_period_ppppd->client_identificator;
        ?>
        <KlijentskaOznakaDeklaracije><?= $client_identificator; ?></KlijentskaOznakaDeklaracije>
        <?php } ?>
        <VrstaPrijave>1</VrstaPrijave>
        <ObracunskiPeriod><?php echo $model->month; ?></ObracunskiPeriod>
        <?php if($model->isFinal) { ?>
        <OznakaZaKonacnu>K</OznakaZaKonacnu>
        <?php } ?>
        <DatumPlacanja><?=$paymentDate?></DatumPlacanja>
        <NajnizaOsnovica><?=$by_min_tax_base?></NajnizaOsnovica>
    </PodaciOPrijavi>
    <PodaciOIsplatiocu>
        <TipIsplatioca><?=Yii::app()->configManager->get('accounting.paychecks.company_type')?></TipIsplatioca>
        <PoreskiIdentifikacioniBroj><?=Yii::app()->company->PIB?></PoreskiIdentifikacioniBroj>
        <BrojZaposlenih><?php 
            $wcCriteria = new SIMADbCriteria([
                'Model' => 'WorkContract',
                'model_filter' => [
                    'signed' => true,
                    'for_payout' => true,
                    'active_at' => $model->month->lastDay()
                ]
            ]);
            $number_of_employees_active_on_last_date = WorkContract::model()->count($wcCriteria);
            echo $number_of_employees_active_on_last_date;
        ?></BrojZaposlenih>
        <MaticniBrojisplatioca><?php echo Yii::app()->company->MB; ?></MaticniBrojisplatioca>
        <NazivPrezimeIme><?php echo Yii::app()->company->DisplayName; ?></NazivPrezimeIme>
        <SedistePrebivaliste><?php echo Yii::app()->company->getCompanyMunicipalityCode(); ?></SedistePrebivaliste>
        <?php
            $mainPhoneNumber = Yii::app()->company->MainPhoneNumber;
            if(!empty($mainPhoneNumber))
            {
        ?>
        <Telefon><?php echo $mainPhoneNumber->display_name; ?></Telefon>
        <?php
            }
        ?>
        <UlicaIBroj><?php echo Yii::app()->company->main_address; ?></UlicaIBroj>
        <eMail><?php echo Yii::app()->company->main_or_any_email_display; ?></eMail>
    </PodaciOIsplatiocu>
    <DeklarisaniPrihodi>
    <?php
        $orderNum = 0;
        foreach($paychecks as $paycheck)
        {
            foreach($paycheck->paycheck_xmls as $_xml)
            {
                /**
                 * mora da se cast-uje da se obezbedi da su istog tipa da bi se proverilo da su iste vrednosti
                 * jer je prilikom potvrdjivanja inicijalno desi se da je
                 *  $_xml->ppppd_id - int
                 *  $paycheck_period_ppppd->id - string
                 */
                if(!is_null($_xml->ppppd_id) && (string)($_xml->ppppd_id) !== (string)($paycheck_period_ppppd->id))
                {
                    continue;
                }
                
                $orderNum++;
                echo Yii::app()->controller->renderModelView($_xml,'xml_eporezi',[
                    'params' => [
                        'orderNum' => $orderNum
                    ]
                ]);
            }
        }
    ?>
    </DeklarisaniPrihodi>
</PodaciPoreskeDeklaracije>
