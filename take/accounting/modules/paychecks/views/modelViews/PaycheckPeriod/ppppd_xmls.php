<?php
$ppppd_xmls_guitable_id = $params['ppppd_xmls_guitable_id'];
$this->widget(SIMAGuiTable::class, [
    'id' => $ppppd_xmls_guitable_id,
    'model' => PaycheckXML::model(),            
    'fixed_filter'=>[
        'id' => -1
    ],
    'custom_buttons' => [
        Yii::t('PaychecksModule.PPPPD', 'MoveXMLsToOtherPPPD') => [
            'func' => "sima.paychecksPPPPD.moveXMLsToOtherPPPD({$model->id}, '$ppppd_xmls_guitable_id');"
        ]
    ],
]);