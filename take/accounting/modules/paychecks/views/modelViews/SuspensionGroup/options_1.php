<?php
echo SIMAHtml::modelIcons($model,16,array('open'));
echo $this->widget('SIMAButton', [
    'action' => [
        'title' => Yii::t('PaychecksModule.Suspension', 'AddAllSuspensionsForActiveEmployees'),
        'onclick' => [
            'sima.paychecks.addAllSuspensionsForActiveEmployees',
            $model->id
        ]
    ]
], true);
