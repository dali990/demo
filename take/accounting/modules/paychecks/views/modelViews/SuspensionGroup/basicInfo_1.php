<p class="row">
       <span class="title"><?php echo $model->getAttributeLabel('amount'); ?>:</span>
       <span class='data'><?php echo $model->getAttributeDisplay('amount'); ?> </span>
</p>
<p class="row">
       <span class="title"><?php echo $model->getAttributeLabel('month'); ?>:</span>
       <span class='data'><?php echo $model->getAttributeDisplay('month'); ?> </span>
</p>
<p class="row">
       <span class="title"><?php echo $model->getAttributeLabel('creditor'); ?>:</span>
       <span class='data'><?php echo $model->getAttributeDisplay('creditor'); ?> </span>
</p>
<p class="row">
    <span class="title"><?php echo $model->getAttributeLabel('creditor_bank_account')?>:</span>
    <span class='data'><?php echo $model->getAttributeDisplay('creditor_bank_account');?> </span>
</p>