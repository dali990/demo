<?php
    $recalculated_values = $model->getRecalculatedValuesWithoutPrevious();
    
    $tax_release_used_in_previous_paychecks = 0;
    $tax_base_in_previous_paychecks = 0;
    $tax_release_used_in_previous_paychecks_with_same_svp = 0;
    $tax_base_in_previous_paychecks_with_same_svp = 0;
    
    $previous_paychecks = $model->paycheck->getPreviousPaychecks();
    foreach($previous_paychecks as $previous_paycheck)
    {
        $tax_release_used_in_previous_paychecks += $previous_paycheck->tax_release;
        $tax_base_in_previous_paychecks += $previous_paycheck->amount_bruto;
        foreach($previous_paycheck->additionals as $additional)
        {
            if((string)$additional->svp === (string)$model->svp)
            {
                $tax_release_used_in_previous_paychecks_with_same_svp += $additional['tax_release'];
                $tax_base_in_previous_paychecks_with_same_svp += $additional['bruto'];
            }
            else
            {
            }
        }
    }
    
    $mfps = [];
    if($tax_release_used_in_previous_paychecks_with_same_svp > 0)
    {
        $mfps[] = [
            'code' => 'MFP.1',
            'value' => number_format($tax_release_used_in_previous_paychecks_with_same_svp, 2, '.', '')
        ];
    }
    if($tax_base_in_previous_paychecks_with_same_svp > 0)
    {
        $mfps[] = [
            'code' => 'MFP.2',
            'value' => number_format($tax_base_in_previous_paychecks_with_same_svp, 2, '.', '')
        ];
        $mfps[] = [
            'code' => 'MFP.4',
            'value' => number_format($tax_base_in_previous_paychecks_with_same_svp, 2, '.', '')
        ];
    }
    $mfps[] = [
        'code' => 'MFP.10',
        'value' => 1
    ];
    if($tax_release_used_in_previous_paychecks > 0)
    {
        $mfps[] = [
            'code' => 'MFP.11',
            'value' => number_format($tax_release_used_in_previous_paychecks, 2, '.', '')
        ];
    }
    if($model->paycheck->paycheck_period->isFirstPaycheckPeriodInMonth())
    {
        $paycheck_recalculated_values = $model->paycheck->getRecalculatedValuesByParsedAdditionals();
        $mfps[] = [
            'code' => 'MFP.12',
            'value' => number_format($paycheck_recalculated_values['bruto'], 2, '.', '')
        ];
    }
    else if($tax_base_in_previous_paychecks > 0)
    {
        $mfps[] = [
            'code' => 'MFP.12',
            'value' => number_format($tax_base_in_previous_paychecks, 2, '.', '')
        ];
    }
    
?>
<PodaciOPrihodima>
<RedniBroj><?php echo $params['orderNum']; ?></RedniBroj>
<VrstaIdentifikatoraPrimaoca><?php echo 1; ?></VrstaIdentifikatoraPrimaoca>
<IdentifikatorPrimaoca><?php echo $model->paycheck->employee->person->JMBG; ?></IdentifikatorPrimaoca>
<Prezime><?php echo strtoupper(SIMAMisc::RemoveSerbianLetters(trim($model->paycheck->employee->person->lastname))); ?></Prezime>
<Ime><?php echo strtoupper(SIMAMisc::RemoveSerbianLetters(trim($model->paycheck->employee->person->firstname))); ?></Ime>
<OznakaPrebivalista><?php echo $model->paycheck->employee->municipality->code; ?></OznakaPrebivalista>
<SVP><?php echo $model->svp; ?></SVP>
<BrojKalendarskihDana><?php echo $model->number_of_calendar_days; ?></BrojKalendarskihDana>
<BrojEfektivnihSati><?php echo number_format($model->hours, 1); ?></BrojEfektivnihSati>
<MesecniFondSati><?php echo number_format($model->paycheck->month_period_hours, 1); ?></MesecniFondSati>
<Bruto><?php echo number_format($recalculated_values['bruto'], 2, '.', ''); ?></Bruto>
<OsnovicaPorez><?php echo number_format($recalculated_values['tax_base'], 2, '.', ''); ?></OsnovicaPorez>
<Porez><?php echo number_format($recalculated_values['tax_empl'], 2, '.', ''); ?></Porez>
<OsnovicaDoprinosi><?php echo number_format($recalculated_values['bruto'], 2, '.', ''); ?></OsnovicaDoprinosi>
<PIO><?php echo number_format($recalculated_values['pio'], 2, '.', ''); ?></PIO>
<ZDR><?php echo number_format($recalculated_values['zdr'], 2, '.', ''); ?></ZDR>
<NEZ><?php echo number_format($recalculated_values['nez'], 2, '.', ''); ?></NEZ>
<PIOBen><?php echo number_format(0, 2, '.', ''); ?></PIOBen>
<?php
if(count($mfps) > 0)
{
?>
<DeklarisaniMFP>
<?php
    foreach($mfps as $mfp)
    {
?>
<MFP>
<Oznaka><?php echo $mfp['code']; ?></Oznaka>
<Vrednost><?php echo $mfp['value']; ?></Vrednost>
</MFP>
<?php
    }
?>
</DeklarisaniMFP>
<?php
}
?>
</PodaciOPrihodima>
