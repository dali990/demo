
<?php
$have_diff = false;
foreach($paychecks_for_compare as $paycheck)
{
//    $columns_in_diff = $paycheck->compareWithDB();
    $columns_in_diff = ['amount_neto','amount_bruto','amount_total'];
    $db_paycheck = Paycheck::model()->findByPk($paycheck->id);
    $_max_diff = 0.001;
//    $_max_diff = 1;
//    if(count($columns_in_diff) > 0)
    if(
        abs($db_paycheck->amount_neto - $paycheck->amount_neto) > $_max_diff
        ||
        abs($db_paycheck->amount_bruto - $paycheck->amount_bruto) > $_max_diff
        ||
        abs($db_paycheck->amount_total - $paycheck->amount_total) > $_max_diff
        
        )
    {
        if($have_diff === false)
        {
            $have_diff = true;
        }
?>
/////////////////////////////////////////////////////////////////////////////
<div style="width:100%;">
    <p><b><?php echo $paycheck->DisplayName; ?></b>
    <?php
        echo 
        Yii::app()->controller->widget('SIMAButton', [
            'action'=>[ 
                'title'=>  Yii::t('PaychecksModule.Paycheck', 'Recalc'),
                'onclick'=>['sima.paychecks.recalcPaycheck',"",$paycheck->id,true,true],
//                'subactions' => [
//                    [
//                        'title' => Yii::t('PaychecksModule.Paycheck', 'RecalcWithAdditionals'),
//                        'onclick' => [
//                            "sima.paychecks.recalcPaycheck", $paycheck_id, $model->id, true, false
//                        ]                
//                    ],
//                    [
//                        'title' => Yii::t('PaychecksModule.Paycheck', 'RecalcForCompareWithoutSave'),
//                        'onclick' => [
//                            "sima.paychecks.recalcPaycheck", $paycheck_id, $model->id, true, true
//                        ]             
//                    ]
//                ]
            ]
        ],true);

    ?>
    </p>
    <table>
        <tr>
            <td><b>Ime polja</b></td>
            <td><b>Stara vrednost</b></td>
            <td><b>Nova vrednost</b></td>
        </tr>
    <?php
    foreach($columns_in_diff as $column)
    {
    ?>
        <tr>
            <td><?php echo $paycheck->getAttributeLabel($column); ?></td>
            <td><?php echo  $db_paycheck->getAttributeDisplay($column); ?></td>
            <td><?php echo  $paycheck->getAttributeDisplay($column); ?></td>
        </tr>
    <?php
    }
    ?>
    </table>
</div>
/////////////////////////////////////////////////////////////////////////////
<?php
    }
}

if($have_diff === false)
{
?>
    NEMA RAZLIKA
<?php
}