<div class="sima-layout-panel _horizontal">
    <div class="sima-layout-fixed-panel">
        <?php
        if(SIMAMisc::isVueComponentEnabled())
        {
            Yii::app()->controller->widget(SIMAButtonVue::class, [
                'title'=> Yii::t('PaychecksModule.PaycheckPeriod', 'ExportForBank'),
                'subactions_data' => [
                    [
                        'title' => Yii::t('PaychecksModule.PaycheckPeriod', 'ExportForBankPDF'),
                        'onclick' => [
                            "sima.paychecks.exportPaycheckForBank", $paycheckPeriod->id, null, 'PDF'
                        ]           
                    ],
                    [
                        'title' => Yii::t('PaychecksModule.PaycheckPeriod', 'ExportForBankTXT'),
                        'onclick' => [
                            "sima.paychecks.exportPaycheckForBank", $paycheckPeriod->id, null, 'TXT'
                        ]            
                    ],
                ]
            ]);
        }
        else
        {
            Yii::app()->controller->widget('SIMAButton', [
                'action'=>[ 
                    'title'=> Yii::t('PaychecksModule.PaycheckPeriod', 'ExportForBank'),
                    'subactions' => [
                        [
                            'title' => Yii::t('PaychecksModule.PaycheckPeriod', 'ExportForBankPDF'),
                            'onclick' => [
                                "sima.paychecks.exportPaycheckForBank", $paycheckPeriod->id, null, 'PDF'
                            ]           
                        ],
                        [
                            'title' => Yii::t('PaychecksModule.PaycheckPeriod', 'ExportForBankTXT'),
                            'onclick' => [
                                "sima.paychecks.exportPaycheckForBank", $paycheckPeriod->id, null, 'TXT'
                            ]            
                        ],
                    ]
                ]
            ]);
        }
        ?>
    </div>
    <div class="sima-layout-panel">
        <?php
        $this->widget('SIMAGuiTable', [
            'model' => Paycheck::model(),
            'columns_type' => 'inPeriod',
            'fixed_filter'=>array(
                'paycheck_period' => array(
                    'ids' => $paycheckPeriod->id
                ),
                'is_valid' => true,
                'employee_payout_type' => Employee::$PAYOUT_TYPE_CACHE
            ),
            'multiselect_options' => false
        ]);
        ?>
    </div>
</div>


