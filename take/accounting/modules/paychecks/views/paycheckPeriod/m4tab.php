
<?php $uniq = SIMAHtml::uniqid(); ?>

<div id="<?php echo $uniq; ?>" class="sima-layout-panel _horizontal">
    <?php
        $this->widget('SIMAGuiTable', [
            'id' => "m4employees_rows$uniq",
            'model' => M4EmployeeRow::class,
            'add_button' => [
                'init_data' => [
                    M4EmployeeRow::class => [
                        'm4mf_roll_id' => [
                            'model_filter' => [
                                'year' => [
                                    'ids' => $year_id
                                ]
                            ]
                        ]
                    ]
                ]
            ],
            'fixed_filter' => [
                'm4mf_roll' => [
                    'year' => [
                        'ids' => $year_id
                    ]
                ]
            ],
            'custom_buttons' => [
                Yii::t('PaychecksModule.M4EmployeeRow', 'Predict') =>[
                    'func'=>"sima.paychecks.m4EmployeesPrediction($year_id, '#m4employees_rows$uniq');"
                ],
                'Pdf'=>[
                    'func'=>"sima.paychecks.exportM4($year_id, 'pdf', '#m4employees_rows$uniq');"
                ],
                'txt'=>[
                    'func'=>"sima.paychecks.exportM4($year_id, 'txt', '#m4employees_rows$uniq');"
                ]
            ]
        ]);
    ?>
</div>