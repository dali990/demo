
<?php
//    $columns_in_diff = $paycheck->compareWithDB();
    $columns_in_diff = [
        'worked_hours_value' => '',
        'performance_value' => '',
        'probationer_value' => '',
        'past_exp_value' => '',
        'work_sum' => '$workedHoursValue + $performance_value + $probationer_value + $past_exp_value',
        'hot_meal_value' => '',
        'regres' => '',
        'base_bruto' => '$work_sum + $hot_meal_value + $regres_value',
        'additionals_sum' => '',
        'prev_bruto_sum' => '',
        'amount_bruto' => '$base_bruto + $additionals_sum - $prev_bruto',
        'amount_neto' => 'klasican neto',
        'tax_release' => '',
        'additionals_used_tax_release' => '',
        'base_tax_base' => '$base_bruto - ($tax_release_for_use - $additionals_used_tax_release);',
        'tax_base' => '$base_tax_base        + ($additionals_sum - $additionals_used_tax_release) - $prev_tax_base',
        'tax_empl' => '',
        'pio_empl' => '',
        'zdr_empl' => '',
        'nez_empl' => '',
    ];
    $columns_in_add_diff = [
                'bruto' => '',
                'tax_release' => ''
            ];
    $old_paycheck = Paycheck::model()->findByPk($paycheck->id);
    
if(count($columns_in_diff) > 0)
{

?>
/////////////////////////////////////////////////////////////////////////////
        <div style="width:100%;">
            <p><b><?php echo $paycheck->DisplayName; ?></b></p>
            <table>
                <tr>
                    <td><b>Ime polja</b></td>
                    <td><b>Stara vrednost</b></td>
                    <td><b>Nova vrednost</b></td>
                    <td><b>Komentar</b></td>
                </tr>
            <?php
            foreach($columns_in_diff as $column => $comment)
            {
            ?>
                <tr>
                    <td><?php echo $paycheck->getAttributeLabel($column); ?></td>
                    <td><?php echo $old_paycheck->getAttributeDisplay($column); ?></td>
                    <td><?php echo $paycheck->getAttributeDisplay($column); ?></td>
                    <td><?=$comment?></td>
                </tr>
            <?php
            }
            ?>
            </table>
        </div>

        DODACI: -------------------------------------------------------------
        <?php foreach ($paycheck->additionals as $_additional)
        {
            ?>
            <div style="width:100%;">
            <p><b><?php echo $_additional->DisplayName; ?></b></p>
            <table>
                <tr>
                    <td><b>Ime polja</b></td>
                    <td><b>Nova vrednost</b></td>
                    <td><b>Komentar</b></td>
                </tr>
            <?php
            foreach($columns_in_add_diff as $column => $comment)
            {
            ?>
                <tr>
                    <td><?php echo $_additional->getAttributeLabel($column); ?></td>
                    <td><?php echo $_additional->getAttributeDisplay($column); ?></td>
                    <td><?=$comment?></td>
                </tr>
            <?php
            }
            ?>
            </table>
        </div>
        <?php } ?>
        
        <?php foreach ($old_paycheck->additionals as $_old_additional)
        {
            ?>
            <div style="width:100%;">
            <p><b><?php echo $_additional->DisplayName; ?></b></p>
            <table>
                <tr>
                    <td><b>Ime polja</b></td>
                    <td><b>Stara vrednost</b></td>
                    <td><b>Komentar</b></td>
                </tr>
            <?php
            foreach($columns_in_add_diff as $column => $comment)
            {
            ?>
                <tr>
                    <td><?php echo $_old_additional->getAttributeLabel($column); ?></td>
                    <td><?php echo $_old_additional->getAttributeDisplay($column); ?></td>
                    <td><?=$comment?></td>
                </tr>
            <?php
            }
            ?>
            </table>
        </div>
        <?php } ?>

/////////////////////////////////////////////////////////////////////////////
<?php
}
else
{
?>
    NEMA RAZLIKA
<?php
}