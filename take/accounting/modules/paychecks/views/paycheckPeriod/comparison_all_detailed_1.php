
<?php
$have_diff = false;
foreach($paychecks_for_compare_parsed as $paycheck_id => $paycheck_diff)
{
    $paycheck = Paycheck::model()->findByPk($paycheck_id);
    $fields_in_diff = $paycheck_diff['fields_in_diff'];
?>
/////////////////////////////////////////////////////////////////////////////
<div style="width:100%;">
    <p><b><?php echo $paycheck->DisplayName; ?></b>
    <?php
        if(SIMAMisc::isVueComponentEnabled())
        {
            echo Yii::app()->controller->widget(SIMAButtonVue::class, [
                    'title'=>  Yii::t('PaychecksModule.Paycheck', 'Recalc'),
                    'onclick'=>['sima.paychecks.recalcPaycheck',"",$paycheck->id,true,true]
                ],true);
        }
        else
        {
            echo Yii::app()->controller->widget('SIMAButton', [
                    'action'=>[ 
                        'title'=>  Yii::t('PaychecksModule.Paycheck', 'Recalc'),
                        'onclick'=>['sima.paychecks.recalcPaycheck',"",$paycheck->id,true,true]
                    ]
                ],true);
        }
    ?>
    </p>
    <div style="width: 3000px;">
        <div>
            kolone u razlici:
            <?php
            foreach($fields_in_diff as $field_in_diff)
            {
                echo $field_in_diff['field_name'].' | ';
            }
            ?>
        </div>
        <div>
    <?php
        $module_name = Paycheck::model()->moduleName();
        $modelclass = Paycheck::class;
        $tab_code = 'calculations';
        $processOutput = false;
        echo $this->renderPartial(
            $module_name . '.views.modelTabs.' . $modelclass . '.' . str_replace('.', '_', $tab_code), [
                'model' => $paycheck,
                'parametersEmployee' => PaychecksEmployee::createArrayForEmployeeParametersTable($paycheck),
                'parametersPaycheckPeriod' => PaychecksEmployee::createArrayForPaycheckPeriodParametersTable($paycheck),
                'parametersAccountingMonth' => PaychecksEmployee::createArrayForAccountingMonthParametersTable($paycheck),
                'systemParameters' => PaychecksEmployee::createArrayForSystemParametersTable($paycheck),
                'calculated' => PaychecksEmployee::createArrayForCalculatedParametersTable($paycheck),
                'calculated_final' => PaychecksEmployee::createArrayForFinalCalculatedParametersTable($paycheck),
                'employeeSVP' => $paycheck->employee_svp
            ], true, $processOutput
        );
        $diff_paycheck_model = $paycheck_diff['model'];
        echo $this->renderPartial(
            $module_name . '.views.modelTabs.' . $modelclass . '.' . str_replace('.', '_', $tab_code), [
                'model' => $diff_paycheck_model,
                'parametersEmployee' => PaychecksEmployee::createArrayForEmployeeParametersTable($diff_paycheck_model),
                'parametersPaycheckPeriod' => PaychecksEmployee::createArrayForPaycheckPeriodParametersTable($diff_paycheck_model),
                'parametersAccountingMonth' => PaychecksEmployee::createArrayForAccountingMonthParametersTable($diff_paycheck_model),
                'systemParameters' => PaychecksEmployee::createArrayForSystemParametersTable($diff_paycheck_model),
                'calculated' => PaychecksEmployee::createArrayForCalculatedParametersTable($diff_paycheck_model),
                'calculated_final' => PaychecksEmployee::createArrayForFinalCalculatedParametersTable($diff_paycheck_model),
                'employeeSVP' => $diff_paycheck_model->employee_svp
            ], true, $processOutput
        );
    ?>
        </div>
    </div>
</div>
/////////////////////////////////////////////////////////////////////////////
<?php
}
