<?php $this->widget('SIMAGuiTable', [
    'model' => PaycheckDeduction::class,
    'columns_type' => 'inPeriod',
    'fixed_filter' => [
        'paycheck' => [
            'paycheck_period' => [
                'ids' => $paycheck_period->id
            ]
        ]
    ]
]);