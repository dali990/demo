<?php $uniq = SIMAHtml::uniqid(); ?>

<div class="sima-layout-panel _vertical _splitter">

    <div class='sima-layout-panel'>
        <?php
            $this->widget('SIMAGuiTable', [
                'model' => Bank::model(),
                'columns_type' => 'inPeriod',
                'setRowSelect' => "on_select$uniq",
                'setMultiSelect' => "on_unselect$uniq",
                'setRowUnSelect' => "on_unselect$uniq",
                'fixed_filter'=>array(
                    'ids' => $bank_ids
                ),
                'headerOptionRows' => [
                    ['title'],
                    ['table_settings','custom_buttons','pagination','filters_in','export','filters_out']
                ]
            ]);
        ?>
    </div>

    <div id="<?=$paychecksId?>" class='sima-layout-panel'>
    </div>
</div>
<script type='text/javascript'>
    function on_select<?php echo $uniq; ?>(row)
    {
        sima.ajax.get('accounting/paychecks/paycheckPeriod/paychecksForBank', {
            data: 
            {
                bankId: row.attr('model_id'),
                paycheckPeriodId: <?php echo $paycheckPeriodId; ?>
            },
            async:true,
            loadingCircle: $('#<?=$paychecksId?>'),
            success_function:function(response)
            {
                sima.set_html($('#<?=$paychecksId?>'),response.html,'TRIGGER_paychecks_banks');
            }
        });
    }
    function on_unselect<?php echo $uniq; ?>(row)
    {
        $('#<?=$paychecksId?>').html('');
    }
</script>