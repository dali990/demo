
<?php $uniq = SIMAHtml::uniqid(); ?>

<div id="<?php echo $uniq; ?>" class="sima-layout-panel _horizontal">
    <?php
        $this->widget('SIMAGuiTable', [
            'model' => M4MFRoll::class,
            'columns_type' => 'from_year',
            'add_button' => [
                'init_data' => [
                    M4MFRoll::class => [
                        'year_id' => [
                            'disabled',
                            'init' => $year_id
                        ]
                    ]
                ]
            ],
            'fixed_filter' => [
                'year' => [
                    'ids' => $year_id
                ]
            ]
        ]);
    ?>
</div>