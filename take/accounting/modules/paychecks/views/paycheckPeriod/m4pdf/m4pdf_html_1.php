<div id='wrap'>

    <div id='top'>
        <div class='top-name'>
            PRIJAVA PODATAKA ZA UTVRĐIVANJE STAŽA OSIGURANJA, ZARADE, NAKNADE ZARADE,ODNOSNO OSNOVICE OSIGURANJA I VISINE UPLAĆENOG DOPRINOSA ZA VIŠE LICA ZA 
                <?php echo $year; ?> GODINU ZA OSIGURANIKE ZAPOSLENE
        </div>
        <div class="top-m4-name">
            Obrazac M-4K
        </div>
        <div class='top-data'>
            <div class='top-data-left'>
                <div class='top-data-left-title'>
                    <span class="top-data-left-title-top"> naziv (prezime i ime) </span>
                    <span class="top-data-left-title-bottom"> sedište obveznika plaćanja doprinosa</span>
                </div>
                <div class='top-data-left-value'>
                    <div class="top-data-left-value-top"><?php echo Yii::app()->company->DisplayName; ?></div>
                    <div class="top-data-left-value-bottom">
                        <?php echo !empty(Yii::app()->company->partner->main_address_id) ? Yii::app()->company->partner->MainAddress->DisplayName : ''; ?>
                    </div>
                </div>
            </div>
            <div class='top-data-right'>
                <table class='top-data-right-table'>
                    <tr>
                        <td rowspan="2">poreski identifikacioni broj</td>
                        <td>broj mf rolne i pozicije</td>
                        <td><?php echo $m4roll_position->roll_number; ?> : <?php echo $m4roll_position->position_number; ?></td>
                    </tr>
                    <tr>
                        <td>registarski broj</td>
                        <td>šifra delatnosti</td>
                    </tr>
                    <tr>
                        <td style="height: 20px;"><?php echo Yii::app()->company->PIB; ?></td>
                        <td style="height: 20px;">6198082869</td>
                        <td style="height: 20px;">
                            <?php echo !empty(Yii::app()->company->work_code) ? Yii::app()->company->work_code->code : ''; ?>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

    <div id='mid'>
        <table class="tg">
            <tr>
                <td class="tg-yw4l" rowspan="3" width='1%'>redni broj</td>
                <td class="tg-yw4l" rowspan="3" width='8%'>jedinstveni matični broj građanina, odnosno lični broj osiguranika</td>
                <td class="tg-yw4l" rowspan="3" width='10%'>prezime i ime</td>
                <td class="tg-yw4l" colspan="6" >podaci o stažu osiguranja, zaradi, naknadi zarade, odnosno osnovici osiguranja i o uplaćenim doprinosima</td>
                <td class="tg-yw4l" colspan="5" >radna mesta - poslovi na kojima se staž osiguranja računa sa uvećanim trajanjem, 
                    osnova za računanje staža sa uvećanim trajanjem i visina uplaćenog doprinosa </td>
            </tr>
            <tr>
                <td class="tg-yw4l" colspan="2">podaci o stažu</td>
                <td class="tg-yw4l" rowspan="2">zarada</td>
                <td class="tg-yw4l" rowspan="2">uplaćeni doprinosi</td>
                <td class="tg-yw4l" rowspan="2">iznos naknade zarade po osnovu zdravstvenog osiguranja i porodiljskog odsustva</td>
                <td class="tg-yw4l" rowspan="2">uplaćeni doprinosi</td>
                <td class="tg-yw4l" colspan="2">efektivno trajanje</td>
                <td class="tg-yw4l" rowspan="2">naziv radnog mesta</td>
                <td class="tg-yw4l" rowspan="2">šifra</td>
                <td class="tg-yw4l" rowspan="2">uplaćeni doprinosi</td>
            </tr>
            <tr>
                <td class="tg-yw4l">mesec</td>
                <td class="tg-yw4l">dan</td>
                <td class="tg-yw4l">mesec</td>
                <td class="tg-yw4l">dan</td>
            </tr>

            <?php
                $i = 1;
                foreach ($m4roll_position->m4employee_rows as $m4employee_row) 
                {
            ?>
                    <tr>
                        <td class="tg-yw4l"><?php echo $i; ?></td>
                        <td class="tg-yw4l"><?php echo $m4employee_row->jmbg; ?></td>
                        <td class="tg-yw4l"><?php echo $m4employee_row->employee_name; ?></td>
                        <td class="tg-yw4l"><?php echo $m4employee_row->work_exp_months; ?></td>
                        <td class="tg-yw4l"><?php echo $m4employee_row->work_exp_days; ?></td>
                        <td class="tg-yw4l _right"><?php echo $m4employee_row->bruto; ?></td>
                        <td class="tg-yw4l _right"><?php echo $m4employee_row->pio_doprinos; ?></td>
                        <td class="tg-yw4l _right"><?php echo $m4employee_row->zdrav_bruto; ?></td>
                        <td class="tg-yw4l _right"><?php echo $m4employee_row->zdrav_pio_doprinos; ?></td>
                        <td class="tg-yw4l"><?php echo ''; ?></td>
                        <td class="tg-yw4l"><?php echo ''; ?></td>
                        <td class="tg-yw4l"><?php ?></td>
                        <td class="tg-yw4l"><?php echo ''; ?></td>
                        <td class="tg-yw4l"><?php echo ''; ?></td>
                    </tr>
            <?php
                    $i++;
                }
            ?>
        </table>
    </div>

    <div id='bot'>
        <div class='bot-left'>
            <table class='bot-left-table'>
                <tr>
                    <td class="bot-left-table-row">Broj i datum prijave:</td>
                </tr>
                <tr>
                    <td class="bot-left-table-row">Primio:</td>
                </tr>
                <tr>
                    <td class='bot-left-table-row'>Uneo:</td>
                </tr>
            </table>
        </div>

        <div class='bot-middle'> (M.P.)</div>

        <div class='bot-right'>
            U _____________, dana  ___________ godine <br /> <br />
            <b>Podnosilac prijave</b> <br /> <br />
            ________________________________ <br />
                (potpis ovlašćenog lica)
        </div>
    </div>

</div>
