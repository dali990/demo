<style>
    
    #wrap {
        width: 100%;
        margin: 0 auto;
        display: block;
        font-weight: bold;
        box-sizing: border-box;
        page-break-after: always;
    }
    
    table, th, td {
        border: 1px solid black;
        border-collapse: collapse;
        padding: 5px; 
        height: 100%;
    }
    
    #top {
        display:block;
        height: 160px;
        text-transform: uppercase;
        margin-bottom: 10mm;
    }
    
    .top-name {
        margin-bottom: 5mm;
        font-weight: bold;
        font-size: 14px;
        text-align: center;
    }
    
    .top-m4-name {
        text-align: right;
        margin-bottom: 5mm;
        font-size: 14px;
    }
    
    .top-data {
        
    }
    
    .top-data-left {
        width: 50%;
        float: left;
        border: 1px solid black;
        height: 95px;
        font-size:12px;
    }
    
    .top-data-left-title {
        float: left;
        width: 30%;
        height: 100%;
        border-right: 1px solid black;
        text-align: center;
    }
    
    .top-data-left-title-top {
        display: block;
        text-transform: uppercase;
        width: 100%;
        height: 45%;
        margin-top: 10px;
    }
    
    .top-data-left-title-bottom {
        display: block;
        text-transform: uppercase;
        width: 100%;
        height: 45%;
        margin-top: -10px;
    }
    
    .top-data-left-value {
        float: left;
        width: 60%;
        height: 100%;
        margin-left: 5%;
    }
    
    .top-data-left-value-top {
        border-bottom: 1px solid black;
        display: block;
        width: 100%;
        height: 45%;
        text-align: center;
        line-height: 42px;
    }
    
    .top-data-left-value-bottom {
        border-bottom: 1px solid black;
        display: block;
        width: 100%;
        height: 45%;
        text-align: center;
        line-height: 42px;
    }
    
    .top-data-right {
        width: 40%;
        float: right;
        height: 98px;
    }
    
    .top-data-right-table {
        width: 100%;
    }
    
    .top-data-right-table td {
        font-family:Arial, sans-serif;
        font-size: 12px;
        text-align: center;
    }
    
    #mid {
        text-transform: uppercase;
    }

    .tg  {
        border-collapse:collapse;
        border-spacing:0;
        width: 100%;
    }

    .tg td {
        font-family:Arial, sans-serif;
        font-size:10px;
        padding:5px 2px;
        border-style:solid;
        border-width:1px;
        overflow:hidden;
        word-break:normal;
    }

    .tg th {
        font-family:Arial, sans-serif;
        font-size:12px;
        font-weight:normal;
        padding:10px 5px;
        border-style:solid;
        border-width:1px;
        overflow:hidden;
        word-break:normal;
    }

    .tg .tg-yw4l {
        vertical-align: middle;
        text-align: center;
    }
    
    .tg .tg-yw4l._right {
        text-align: right;
    }

    .d10 {
        width:10%;
    }

    .d20 {
        width:20%;
    }

    #bot {
        margin-top: 25px;
    }
    
    .bot-left {
        display: inline-block;
        vertical-align: middle;
        text-align: left;
        width: 30%;
    }
    
    .bot-left-table {
        border-collapse:collapse;
        border-spacing:0;
        width:70%;
    }
    
    .bot-left-table th {
        font-family:Arial, sans-serif;
        font-size:12px;
        font-weight:normal;
        border-style:solid;
        border-width:1px;
        overflow:hidden;
        word-break:normal;
    }
    
    .bot-left-table td {
        font-family:Arial, sans-serif;
        font-size:12px;
        border-style:solid;
        border-width:1px;
        overflow:hidden;
        word-break:normal;
        vertical-align:middle;
        text-align: left;
    }
    
    .bot-left-table-row {
        
    }
    
    .bot-middle {
        display: inline-block;
        vertical-align: middle;
        text-align:center;
        width: 35%;
    }
    
    .bot-right {
        display: inline-block;
        vertical-align: middle;
        text-align: right;
        width: 33%;
        font-size:12px;
    }

</style>
