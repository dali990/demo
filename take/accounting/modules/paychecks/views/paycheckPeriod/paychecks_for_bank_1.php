<?php

Yii::app()->controller->widget('SIMAButton', [
    'action'=>[ 
        'title'=>Yii::t('PaychecksModule.PaycheckPeriod', 'ExportForBank'),
        'subactions' => [
            [
                'title' => Yii::t('PaychecksModule.PaycheckPeriod', 'ExportForBankPDF'),
                'onclick' => [
                    "sima.paychecks.exportPaycheckForBank", $paycheckPeriodId, $bank->id, 'PDF'
                ]                 
            ],
            [
                'title' => Yii::t('PaychecksModule.PaycheckPeriod', 'ExportForBankTXT'),
                'onclick' => [
                    "sima.paychecks.exportPaycheckForBank", $paycheckPeriodId, $bank->id, 'TXT'
                ]              
            ],
        ]
    ]
]);

$this->widget('SIMAGuiTable', [
    'model' => Paycheck::model(),
    'columns_type' => 'inPeriod',
    'fixed_filter'=>array(
        'paycheck_period' => [
            'ids' => $paycheckPeriodId
        ],
        'bank_account' => [
            'bank' => [
                'ids' => $bank->id
            ]
        ]
    ),
    'multiselect_options' => false,    
]);
