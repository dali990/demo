<?php
$uniq = SIMAHtml::uniqid();

$add_button = false;
$multiselect_options = false;
if($model->canEdit())
{
    $add_button = array(
//        'formName' => 'inPeriod',
        'init_data' => array(
            'Paycheck' => array(
                'paycheck_period_id' => array(
                    'disabled',
                    'init' => $model->id
                ),
                'employee_id' => array(
                    'model_filter' => array(
                        'scopes' => array(
                            'haveWorkContractInMonth' => array(
                                $model->month->id
                            )
                        )
                    )
                ),
                'worked_hours' => Yii::app()->configManager->get('accounting.paychecks.estimated_average_hours_in_month'),
            )
        )
    );

    $multiselect_options = true;
}
?>

<div class='sima-layout-panel _horizontal'>
    <div class='sima-layout-fixed-panel'>
    <?php
    if(SIMAMisc::isVueComponentEnabled())
    {
        $this->widget(SIMAButtonVue::class, [
            'title' => 'evidencija zarada',
            'subactions_data' => [
                [
                    'title' => Yii::t('PaychecksModule.PaycheckPeriod', 'ExportForBankPDF'),
                    'onclick' => ["sima.paychecks.exportAllPaychecks", $model->id]             
                ],
            ]
        ]);
    }
    else
    {
        $this->widget('SIMAButton', [
            'action' => [
                'title' => 'evidencija zarada',
                'subactions' => [
                    [
                        'title' => Yii::t('PaychecksModule.PaycheckPeriod', 'ExportForBankPDF'),
                        'onclick' => ["sima.paychecks.exportAllPaychecks", $model->id]             
                    ],
                ]
            ]
        ]);
    }
    ?>
    </div>
    <div class="sima-layout-panel">
    <?php
        $this->widget('SIMAGuiTable', [
            'id' => 'paychecks' . $uniq,
            'model' => Paycheck::model(),
            'columns_type' => 'inPeriod',
            'fixed_filter'=>array(
                'paycheck_period' => array(
                    'ids' => $model->id
                ),
                'is_valid' => true,
                'order_by' => 'paygrade_code ASC'
            ),
            'add_button' => $add_button,
            'multiselect_options' => $multiselect_options
        ]);
    ?>
    </div>
</div>
