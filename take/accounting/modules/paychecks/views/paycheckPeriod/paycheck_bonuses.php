<div class="sima-layout-panel _vertical _splitter">
    <div class="sima-layout-panel">
        <?php $this->widget('SIMAGuiTable',[
            'model' => 'PaycheckBonus',
            'title' => 'Po osobi',
            'add_button' => [
                'init_data' => [
                    'PaycheckBonus' => [
                        'paycheck_period' => ['ids' => $paycheck_period->id]
                    ]
                ]
            ],
            'fixed_filter' => [
                'paycheck' => [
                    'paycheck_period' => ['ids' => $paycheck_period->id]
                ]
            ]
        ]);?>
    </div>
    <div class="sima-layout-panel">
        <?php $this->widget('SIMAGuiTable',[
            'model' => 'PaycheckGroupBonus',
            'title' => 'Grupni',
            'add_button' => [
                'init_data' => [
                    'PaycheckGroupBonus' => [
                        'paycheck_period' => ['ids' => $paycheck_period->id]
                    ]
                ]
            ],
            'fixed_filter' => [
                'paycheck_period' => ['ids' => $paycheck_period->id]
            ]
        ]);?>
    </div>
</div>