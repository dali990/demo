<?php
$uniq = SIMAHtml::uniqid();
$this->widget('SIMAGuiTable', [
    'id' => 'oldpaychecks' . $uniq,
    'model' => Paycheck::model(),
    'columns_type' => 'old_paychecks',
    'fixed_filter' => [
        'display_scopes' => ['orderByPeriodDesc']
    ]
]);

