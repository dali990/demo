<div id="missing_work_hours<?php echo $uniq; ?>">
    <table style='margin: 0 auto;'>
        <tr>
            <td>Od*</td>
            <td>
                <div id='<?php echo $dateFromName; ?>'><?php 
                    $this->beginWidget('SIMASearchField', $dateFrom_value);
                    $this->endWidget();
                ?>
                </div>
            </td>
        </tr>
        <tr>
            <td>Do*</td>
            <td>
                <div id='<?php echo $dateToName; ?>'><?php 
                    $this->beginWidget('SIMASearchField', $dateTo_value);
                    $this->endWidget();
                ?></div>
            </td>
        </tr>
        <tr>
            <td>
                Zaposleni*
                <span class='addListeners sima-icon _search_form _16' onclick='sima.paychecks.onMissingWorkHoursChooseEmployees("<?php echo $uniq; ?>")'></span>
            </td>
            <td>
                <div id='employee_choose_result<?php echo $uniq; ?>'>
                <?php
                    foreach($selected_employees as $emp)
                    {
                        echo '<div class="emp_div" style="width:100%" emp_id='.$emp['id'].' display_name='.$emp['display_name'].'>'.$emp['display_name'].'</div>';
                    }
                ?>
                </div>
            </td>
        </tr>
        <tr>
            <td><?php echo Yii::t('PaychecksModule.PaychecksEmployee', $data_display); ?>*</td>
            <td>
                <div id='hours<?php echo $uniq; ?>'>
                    <input style='text-align:right;' id='data_input<?php echo $uniq; ?>' type='text' <?php echo $data_value; ?>/>
                </div>
            </td>
        </tr>
    </table>
</div>
