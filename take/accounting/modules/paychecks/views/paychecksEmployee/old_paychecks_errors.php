<?php 
$uniq = SIMAHtml::uniqid(); 
$guitable_id = 'employees' . $uniq;
?>
<div id='old-paychecks<?php echo $uniq; ?>'>
    <div id='buttons<?php echo $uniq; ?>' class='buttons top-paychecks'>
    <button class="sima-ui-button paycheck" onclick="sima.paychecks.createMissingPaycheck('<?php echo $guitable_id; ?>')">Kreiraj staru zaradu</button>
    
    </div>

    <?php
    if(isset($problem_messages) 
            && is_array($problem_messages) 
            && count($problem_messages) > 0)
    {
        foreach($problem_messages as $message)
        {
            echo $message.'</br>';
        }
    }
    else if(is_array($employeesNotOkIds) && count($employeesNotOkIds) > 0)
    {
        $this->widget('SIMAGuiTable', [
            'id' => $guitable_id,
            'model' => Employee::model(),
            'columns_type' => 'old_paychecks',
            'fixed_filter' => array(
                'ids' => $employeesNotOkIds,
            )
        ]);
    }

    ?>
</div>
