<?php

$uniq = SIMAHtml::uniqid();

$tab_action = 'accounting/paychecks/paychecksEmployee/oldPaychecksTabData';

$this->widget('SIMATabs', array(
    'id' => 'oldpaychecktabs'.$uniq,
    'tabs' => array(
        array(
            'title' => 'Pregled',
            'code' => 'old_paychecks_view',
            'action' => $tab_action,
            'get_params' => array('view' => 'old_paychecks_view')
        ),
        array(
            'title' => 'Greške',
            'code' => 'old_paychecks_errors',
            'action' => $tab_action,
            'get_params' => array('view' => 'old_paychecks_errors')
        )
    ),
    'default_selected' => 'old_paychecks_view',
));
