<?php
$uniq = SIMAHtml::uniqid();

$this->widget('SIMAGuiTable', [
    'id' => 'employees' . $uniq,
    'model' => Employee::model(),
    'add_button' => true,
    'columns_type' => 'in_finances',
]);
    