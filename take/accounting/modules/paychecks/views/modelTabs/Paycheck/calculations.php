<div>
    <div class="paycheck-table-div">
        Parametri zaposlenog
        <table>
            <tr>
                <th>Kod</th>
                <th>Naziv</th>
                <th>Vrednost</th>
            </tr>
            <?php
                foreach($parametersEmployee as $row)
                {
                    echo "<tr>";
                    echo "<td>".$row['ordernum']."</td>";
                    echo "<td>".$row['name']."</td>";
                    echo "<td align='right'>".$row['value']."</td>";
                    echo "</tr>";
                }
            ?>
        </table>
        Parametri isplate zarade
        <table>
            <tr>
                <th>Kod</th>
                <th>Naziv</th>
                <th>Vrednost</th>
            </tr>
            <?php
                foreach($parametersPaycheckPeriod as $row)
                {
                    echo "<tr>";
                    echo "<td>".$row['ordernum']."</td>";
                    echo "<td>".$row['name']."</td>";
                    echo "<td align='right'>".$row['value']."</td>";
                    echo "</tr>";
                }
            ?>
        </table>
        Parametri mesecnog perioda
        <table>
            <tr>
                <th>Kod</th>
                <th>Naziv</th>
                <th>Vrednost</th>
            </tr>
            <?php
                foreach($parametersAccountingMonth as $row)
                {
                    echo "<tr>";
                    echo "<td>".$row['ordernum']."</td>";
                    echo "<td>".$row['name']."</td>";
                    echo "<td align='right'>".$row['value']."</td>";
                    echo "</tr>";
                }
            ?>
        </table>
        Sistemski parametri
        <table>
            <tr>
                <th>Kod</th>
                <th>Naziv</th>
                <th>Vrednost</th>
            </tr>
            <?php
                foreach($systemParameters as $row)
                {
                    echo "<tr>";
                    echo "<td>".$row['ordernum']."</td>";
                    echo "<td>".$row['name']."</td>";
                    echo "<td align='right'>".$row['value']."</td>";
                    echo "</tr>";
                }
            ?>
        </table>
        Dodatne vrednosti
        <table>
            <tr>
                <th>Naziv</th>
                <th>Vrednost</th>
            </tr>
            <tr>
                <td>SVP</td>
                <td><?php echo $employeeSVP ?></td>
            </tr>
        </table>
    </div>
    <div class="paycheck-table-div">
        Preracunate vrednosti
        <table>
            <tr>
                <th>Kod</th>
                <th>Naziv</th>
                <th>Vrednost</th>
                <th>Formula</th>
            </tr>
            <?php
                foreach($calculated as $row)
                {
                    echo "<tr>";
                    echo "<td>".$row['ordernum']."</td>";
                    if(isset($row['bolded']) && $row['bolded'] == true)
                    {
                        echo "<td><b>".$row['name']."</b></td>";
                    }
                    else
                    {
                        echo "<td>".$row['name']."</td>";
                    }
                    echo "<td align='right'>".$row['value']."</td>";
                    echo "<td>".$row['formula']."</td>";
                    echo "</tr>";
                }
            ?>
        </table>
        Preracunate finalne vrednosti
        <table>
            <tr>
                <th>Kod</th>
                <th>Naziv</th>
                <th>Vrednost</th>
                <th>Formula</th>
            </tr>
            <?php
                foreach($calculated_final as $row)
                {                
                    echo "<tr>";
                    echo "<td>".$row['ordernum']."</td>";
                    if(isset($row['bolded']) && $row['bolded'] == true)
                    {
                        echo "<td><b>".$row['name']."</b></td>";
                    }
                    else
                    {
                        echo "<td>".$row['name']."</td>";
                    }
                    echo "<td align='right'>".$row['value']."</td>";
                    echo "<td>".$row['formula']."</td>";
                    echo "</tr>";
                }
            ?>
        </table>
    </div>
    <div class="paycheck-table-div">
    </div>
</div>