<?php
if(isset($model->file))
{
?>
<div class="sima-layout-panel _horizontal">
    <div class="sima-layout-fixed-panel">
        <p><?php echo $model->getAttributeLabel('file.hr_delivered_status').': '.$model->getAttributeDisplay('file.hr_delivered_status'); ?></p>
    </div>
    <div class="sima-layout-panel">
        <?php
            $this->widget('SIMAFilePreview', [
                'file' => $model->file
            ]);
        ?> 
    </div>
</div>
<?php
}
else
{
    echo Yii::t('PaychecksModule.Paycheck', 'OZFileNotCreated');
}