<?php
$uniq = SIMAHtml::uniqid();
$this->widget('SIMAGuiTable', [
    'id' => 'paychecksuspensions'.$uniq,
    'model' => PaycheckSuspension::model(),
    'add_button' => array(
        'init_data' => array(
            'PaycheckSuspension' => array(
                'paycheck_id' => array(
                    'disabled',
                    'init' => $model->id
                ),
                'suspension_id' => [
                    'model_filter' => [
                        'employee' => [
                            'ids' => $model->employee->id
                        ]
                    ]
                ]
            )
        )
    ),
    'fixed_filter'=>array(
          'paycheck' => array(
              'ids' => $model->id
          )
    )
]);
