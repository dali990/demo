<?php
$this->widget('SIMAGuiTable', [
    'model' => PaycheckDeduction::class,
    'add_button' => [
        'init_data' => [
            'PaycheckDeduction' => [
                'paycheck_id' => [
                    'disabled',
                    'init' => $model->id
                ]
            ]
        ]
    ],
    'fixed_filter' => [
        'paycheck' => [
            'ids' => $model->id
        ]
    ]
]);
