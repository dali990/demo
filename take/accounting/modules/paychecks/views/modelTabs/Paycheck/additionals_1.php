<?php 
$uniq = SIMAHtml::uniqid();
  
$this->widget('SIMAGuiTable', [
    'id' => 'paycheckadditionals'.$uniq,
    'model' =>  PaycheckAdditional::model(),
    'add_button' => array(
        'init_data' => array(
            'PaycheckAdditional' => array(
                'paycheck_id' => array(
                    'disabled',
                    'init' => $model->id
                )
            )
        )
    ),
    'fixed_filter'=>array(
          'paycheck' => array(
              'ids' => $model->id
          )
    )
]);