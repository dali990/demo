<?php 

if (isset($model->regular_booking_file))
{
//    $regular_html = $this->renderModelTab($model->regular_booking_file,'bookkeeping');
}
else
{
    $model->regular_booking_file = new File();
    $model->regular_booking_file->name = 'Redovne zarade '.$model->DisplayName;
    $model->regular_booking_file->save();
    $model->regular_booking_file_id = $model->regular_booking_file->id;
    $model->save(true, ['regular_booking_file_id']);
//    $model->save();
//    $regular_html = 'nije zadat fajl';
}

//$regular_html = $this->renderModelTab($model->regular_booking_file,'bookkeeping');

$payment =  SIMAHtml::modelFormOpen($model,['formName' => 'just_payment']);
if (isset($model->payment))
{
    $payment .= $model->payment->DisplayHTML;
}
else
{
    $payment .= "<- nije postavljena uplata";
}
?>

<div class="sima-layout-panel _horizontal">

    <div class="sima-layout-fixed-panel">
        <?=$payment?>
    </div>
    <div class="sima-layout-panel">
        <?php

        $this->widget('SIMATabs',[
            'tabs' => [
                [
                    'title' => 'Regularno knjizenje',
                    'code' => 'regular',
                    'action' => 'accounting/books/documentTab',
                    'get_params' => array('id' => $model->regular_booking_file_id)
        //            'html' => $regular_html
                ]
            ],
            'default_selected' => 'regular'
        ]);?>
    </div>
</div>