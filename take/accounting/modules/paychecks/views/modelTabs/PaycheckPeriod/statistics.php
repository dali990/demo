<?php
$uniq = SIMAHtml::uniqid();
$div_id = 'statistics_'.$uniq;
?>

<div id="<?php echo $div_id; ?>" style="margin-left: 10px;">
    
<?php
//$datetime_last_calculation_start_time = new DateTime($model->last_calculation_start_time);
//$datetime_last_calculation_end_time = new DateTime($model->last_calculation_end_time);
//$last_calculation_interval = $datetime_last_calculation_end_time->diff($datetime_last_calculation_start_time);

//echo '<p>'.Yii::t('PaychecksModule.PaycheckPeriod', 'CalculationTimeSpent')
//        .': '.$last_calculation_interval->format('%H:%I:%S').'</p>';
echo '<p>'.Yii::t('PaychecksModule.PaycheckPeriod', 'AverageNetoSalary')
        .': '.SIMAHtml::money_format($model->average_neto_salary).'</p>';
echo '<p>'.Yii::t('PaychecksModule.PaycheckPeriod', 'MinNetoSalary')
        .': '.SIMAHtml::money_format($model->min_neto_salary).'</p>';
echo '<p>'.Yii::t('PaychecksModule.PaycheckPeriod', 'MaxNetoSalary')
        .': '.SIMAHtml::money_format($model->max_neto_salary).'</p>';

$neto_paid_to_state = 0;
if($model->sum_neto_salary>0)
{
    $neto_paid_to_state = (($model->sum_total_salary - $model->sum_neto_salary)/$model->sum_neto_salary) * 100;
}
echo '<p>'.Yii::t('PaychecksModule.PaycheckPeriod', 'NetoPercentPaidToState')
        .': '.SIMAHtml::number_format($neto_paid_to_state).'%</p>';
echo '<p>'.Yii::t('PaychecksModule.PaycheckPeriod', 'NumberOfEmployees')
        .': '.SIMAHtml::number_format($model->number_of_paychecks, 0).'</p>';
echo '<p>'.Yii::t('PaychecksModule.PaycheckPeriod', 'NumberOfEmployeesWithoutFullWorkHours')
        .': '.SIMAHtml::number_format($model->number_of_paychecks_without_full_work_hours, 0).'</p>';
echo '<p>'.Yii::t('PaychecksModule.PaycheckPeriod', 'NumberOfEmployeesOnSickLeave')
        .': '.SIMAHtml::number_format($model->number_of_paychecks_with_sickleave, 0).'</p>';
echo '<p>'.Yii::t('PaychecksModule.PaycheckPeriod', 'NumberOfAbsencesPerCategory').':</p>';
echo '<ol>'
        .'<li>'.Yii::t('PaychecksModule.PaycheckPeriod', 'NumberOfDaysOnAnnualLeave')
            .': '.SIMAHtml::number_format($model->days_on_annual_leave, 0)
        .'</li>'
        .'<li>'.Yii::t('PaychecksModule.PaycheckPeriod', 'NumberOfDaysOnPaidLeave')
            .': '.SIMAHtml::number_format($model->days_on_paid_leave, 0)
        .'</li>'
        .'<li>'.Yii::t('PaychecksModule.PaycheckPeriod', 'NumberOfDaysOnStateAndReligiousHoliday')
            .': '.SIMAHtml::number_format($model->days_on_state_and_religious_holiday, 0)
        .'</li>'
        .'<li>'.Yii::t('PaychecksModule.PaycheckPeriod', 'NumberOfDaysOnSickLeaveTill30')
            .': '.SIMAHtml::number_format($model->days_on_sick_leave_till_30, 0)
        .'</li>'
        .'<li>'.Yii::t('PaychecksModule.PaycheckPeriod', 'NumberOfDaysOnSickLeaveOver30')
            .': '.SIMAHtml::number_format($model->days_on_sick_leave_over_30, 0)
        .'</li>'
        .'<li>'.Yii::t('PaychecksModule.PaycheckPeriod', 'NumberOfDaysOnSickLeaveInjuryAtWork')
            .': '.SIMAHtml::number_format($model->days_on_sick_leave_injury_at_work, 0)
        .'</li>'
        .'<li>'.Yii::t('PaychecksModule.PaycheckPeriod', 'NumberOfDaysOnSickLeaveMaternity')
            .': '.SIMAHtml::number_format($model->days_on_sick_leave_maternity, 0)
        .'</li>'
        .'<li>'.Yii::t('PaychecksModule.PaycheckPeriod', 'NumberOfDaysOnSickLeavePregnancyTill30')
            .': '.SIMAHtml::number_format($model->days_on_sick_leave_pregnancy_till_30, 0)
        .'</li>'
        .'<li>'.Yii::t('PaychecksModule.PaycheckPeriod', 'NumberOfDaysOnSickLeavePregnancyOver30')
            .': '.SIMAHtml::number_format($model->days_on_sick_leave_pregnancy_over_30, 0)
        .'</li>'
    .'</ol>';

?>
    
Neiskoriscene administrativne zabrane:
<ul>
    <?php 
    foreach($unused_suspensions as $unused_suspension)
    {
    ?>
    <li><?php echo $unused_suspension['emp'].' - '.$unused_suspension['susp']; ?></li>
    <?php
    }
    ?>
</ul>
    
Razlike u odnosu na prethodni obracun:
<ul>
    <?php 
    foreach($differences as $difference)
    {
    ?>
    <li><?php echo $difference; ?></li>
    <?php
    }
    ?>
</ul>

Zaposleni aktivni u ovom periodu, a nemaju obracunate zarade:
<ul>
    <?php
    foreach($active_employees_without_paychecks as $employee)
    {
    ?>
        <li><?php 
            echo $employee->DisplayHTML; 
            $this->widget(SIMAButtonVue::class, [
                'title' => Yii::t('PaychecksModule.PaycheckPeriod', 'Recalculate'),
                'onclick' => [
                    'sima.paychecks.recalcPaycheckForEmployee', $div_id, $employee->id, $model->id
                ]
            ]);
        ?></li>
    <?php
    }
    ?>
</ul>

</div>