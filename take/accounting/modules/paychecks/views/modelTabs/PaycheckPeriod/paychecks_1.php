<?php 
$uniq = SIMAHtml::uniqid(); 
?>

<div class="sima-layout-panel _horizontal">
    <div class="sima-layout-fixed-panel">
        <?php echo $this->renderModelView($model, 'recalc_buttons'); ?>
    </div>

    <div id="<?php echo 'subtabs'.$uniq?>" class="sima-layout-panel">
        <?php
            $tabs = [
                [
                    'title' => Yii::t('PaychecksModule.PaycheckPeriod', 'AllPaychecks'),
                    'code' => 'all_paychecks',
                    'action' => 'accounting/paychecks/paycheckPeriod/allPaychecks',
                    'get_params' => array(
                        'model_id' => $model->id
                    )
                ],
                [
                    'title' => Yii::t('PaychecksModule.PaycheckPeriod', 'PaycheckSuspensionsTab'),
                    'code' => 'paycheck_suspensions',
                    'action' => 'accounting/paychecks/paycheckPeriod/paycheckSuspensions',
                    'get_params' => array(
                        'paycheck_period_id' => $model->id
                    )
                ],
                [
                    'title' => Yii::t('PaychecksModule.Deduction', 'AllDeductionsTab'),
                    'code' => 'paycheck_deductions',
                    'action' => 'accounting/paychecks/paycheckPeriod/paycheckDeductions',
                    'get_params' => [
                        'paycheck_period_id' => $model->id
                    ]
                ]
            ];

            if($model->isFinal || $model->isBonus)
            {
                $tabs[] = [
                    'title' => Yii::t('PaychecksModule.PaycheckPeriod', 'AllBonusesTab'),
                    'code' => 'paycheck_bonuses',
                    'action' => 'accounting/paychecks/paycheckPeriod/paycheckBonuses',
                    'get_params' => [
                        'paycheck_period_id' => $model->id
                    ]
                ];
            }

            if($model->confirmed === true)
            {
                $tabs[] = array(
                    'title' => Yii::t('PaychecksModule.PaycheckPeriod', 'PaychecksPerBank'),
                    'code' => 'paychecks_per_bank',
                    'action' => 'accounting/paychecks/paycheckPeriod/unifiedPaychecksBanks',
                    'get_params' => array(
                        'model_id' => $model->id
                    )
                );

                $tabs[] = array(
                    'title' => Yii::t('PaychecksModule.PaycheckPeriod', 'PaychecksByCache'),
                    'code' => 'paychecks_by_cache',
                    'action' => 'accounting/paychecks/paycheckPeriod/paychecksByCache',
                    'get_params' => array(
                        'model_id' => $model->id
                    )
                );
            }

            $id_tab = SIMAHtml::uniqid();
            $this->widget('SIMATabs', array(
                'id' => $uniq.'tabs',
                'tabs' => $tabs,
                'default_selected'=>'all_paychecks'
            ));
        ?>
    </div>
</div>