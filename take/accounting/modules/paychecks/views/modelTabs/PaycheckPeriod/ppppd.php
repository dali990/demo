<?php
$uniq = SIMAHtml::uniqid();
$ppppd_xmls_guitable_id = $uniq.'_pxgi';
?>
<div class="sima-layout-panel _vertical _splitter">
    <div class='sima-layout-panel'>
        <?php echo $this->renderModelView($model, 'ppppds', [
            'params' => [
                'ppppd_xmls_guitable_id' => $ppppd_xmls_guitable_id
            ],
            'class' => 'sima-layout-panel'
        ]); ?>
    </div>
    <div class='sima-layout-panel'>
        <?php echo $this->renderModelView($model, 'ppppd_xmls', [
            'params' => [
                'ppppd_xmls_guitable_id' => $ppppd_xmls_guitable_id
            ],
            'class' => 'sima-layout-panel'
        ]); ?>
    </div>
</div>