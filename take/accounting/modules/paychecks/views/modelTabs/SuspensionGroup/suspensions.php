<div class='sima-layout-panel _splitter _vertical'>
    <div class='sima-layout-panel'
         data-sima-layout-init='<?=CJSON::encode([
                'min_width' => 500,
                'proportion' => 0.5
            ])?>'>
    <?php
    $this->widget('SIMAGuiTable', [
        'model' => Suspension::model(),    
        'columns_type' => 'inGroup',
        'fixed_filter'=>array(                
            'suspension_group' => [
                'ids' => $model->id
            ]
        ),
        'title' => Yii::t('PaychecksModule.Suspension', 'Suspensions'),
        'add_button' => [
            'init_data' => [
                'Suspension' => [
                    'name' => $model->DisplayName,
                    'creditor_id' => ['disabled','init'=>$model->creditor_id],
                    'creditor_id' => ['disabled','init'=>$model->creditor_id],
                    'creditor_bank_account_id' =>$model->creditor_bank_account_id,
                    'begin_month_id' =>$model->month_id,
                    'total_amount' =>$model->amount,
                    'installment_amount' =>$model->installment_amount,
                    'payment_type' =>$model->payment_type,
                    'suspension_group_id' => $model->id
                ]
            ]
        ]
    ]);
    ?>
    </div>
    <div class='sima-layout-panel'
         data-sima-layout-init='<?=CJSON::encode([
                'min_width' => 500,
                'proportion' => 0.5
            ])?>'>
    <?php
    $this->widget('SIMAGuiTable', [
        'model' => Employee::model(),            
        'fixed_filter'=>array(
            'filter_scopes' => [
                'haveActiveWorkContractInMonth' => $model->month->id,
                'noSuspensionInSuspensionGroups' => $model->id
            ]
        ),
        'title' => Yii::t('PaychecksModule.Suspension', 'ActiveEmployeesInGroupMonthWithoutSuspensions')
    ]);
    ?>
    </div>
</div>
