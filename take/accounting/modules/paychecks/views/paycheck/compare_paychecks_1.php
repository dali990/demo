<?php
    $uniq = SIMAHtml::uniqid();
    
    $div_for_problems_id = 'div_for_problems'.$uniq;
    $hidden_div_id = 'hidden_div_id'.$uniq;
?>

<div class="sima-layout-panel _horizontal">
    
    <div class="sima-layout-fixed-panel">
    <?php
        $this->widget(SIMAButtonVue::class, [
            'title' => 'Izaberi ppppd xml',
            'onclick' => [
                'sima.paychecksPPPPD.choosePPPDXmlForCompare',
                $div_for_problems_id,
                $hidden_div_id
            ]
        ]);
        $this->widget(SIMAButtonVue::class, [
            'title' => 'Izaberi isplatu zarade',
            'onclick' => [
                'sima.paychecksPPPPD.choosePaycheckPeriodForCompare',
                $div_for_problems_id,
                $hidden_div_id
            ]
        ]);
        $this->widget(SIMAButtonVue::class, [
            'title' => 'Uporedi',
            'subactions_data' => [
                [
                    'title' => 'Uporedi sa obracunatim zaradama',
                    'onclick' => [
                        "sima.paychecksPPPPD.compareXMLContent", "compareXmlWithPaychecks", $div_for_problems_id, $hidden_div_id
                    ]
                ],
                [
                    'title' => 'Uporedi sa izracunatim PPP-PD',
                    'onclick' => [
                        "sima.paychecksPPPPD.compareXMLContent", "comapreXMLWithPPPPD", $div_for_problems_id, $hidden_div_id
                    ]
                ]
            ]
        ]);
    ?>
    </div>
    
    <div id="<?php echo $hidden_div_id ?>"></div>

</div>
<div id="<?php echo $div_for_problems_id ?>" class="sima-layout-panel">
    Nije izabran xml</br>
    Nije izabrana isplata zarade
</div>