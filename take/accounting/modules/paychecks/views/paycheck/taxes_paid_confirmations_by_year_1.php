
<?php $uniq = SIMAHtml::uniqid(); ?>
<?php
    $this->widget('base.extensions.SIMAGuiTable.SIMAGuiTable', [
        'id' => 'taxes_paid_confirmations'.$uniq,
        'model' => TaxesPaidConfirmation::model(),
        'columns_type' => 'byYear',
        'select_filter'=>array(
            'year' => ['ids' => $prev_year->id]            
        ),
        'custom_buttons'=>[
            'Obračunaj dokumenta'=>[
                'title'=>'Kreirajte sva dokumenta za izabranu godinu',
                'func'=>"sima.paychecks.generateDocumentsForAllTaxesPaidConfirmationsForYear('taxes_paid_confirmations$uniq')"
            ],
            'Zavedite dokumenta'=>[
                'title'=>'Zavedite sva dokumenta za izabranu godinu',
                'func'=>"sima.paychecks.filingAllTaxesPaidConfirmationsForYear('taxes_paid_confirmations$uniq')"
            ],
            'Regenerišite pdf'=>[
                'title'=>'Regenerišite pdf za sva dokumenta za izabranu godinu',
                'func'=>"sima.paychecks.reGenerateDocumentsPdfForAllTaxesPaidConfirmationsForYear('taxes_paid_confirmations$uniq')"
            ],
            'Pošaljite dokumenta'=>[
                'title'=>'Pošaljite sva dokumenta za izabranu godinu. Dokumenta za štampanje će biti preuzeta.',
                'func'=>"sima.paychecks.sendDocumentsPdfForAllTaxesPaidConfirmationsForYear('taxes_paid_confirmations$uniq', false)"
            ],
            'Preuzmite dokumenta za štampanje'=>[
                'title'=>'Preuzmite sva dokumenta za izabranu godinu',
                'func'=>"sima.paychecks.sendDocumentsPdfForAllTaxesPaidConfirmationsForYear('taxes_paid_confirmations$uniq', true)"
            ]
        ],
    ]);
?>