<?php	
$panels = [
    [
        'html'=> $this->widget('SIMATabs', array(
            'list_tabs_model' => $model,
            'default_selected' => $model->confirmed?'pdfPreview':'htmlPreview',
        ), true)
    ]
];

echo Yii::app()->controller->renderContentLayout([
        'name'=> $this->renderModelView($model, 'title'),                
        'options' => [
            [
                'html' => $this->renderModelView($model, 'options')
            ]
        ]
    ],
    $panels
);
