<?php

    $this->widget('base.extensions.SIMAGuiTable.SIMAGuiTable', [
        'id' => 'paygrades'.$uniq,
        'model' => BonusPaygrade::model(),                
        'add_button' => true,
        'setRowSelect' => "on_select$uniq",                
        'setRowUnSelect' => "on_unselect$uniq",
        'setMultiSelect' => "on_unselect$uniq"
    ]);
    
?>

<script type='text/javascript'>
    function on_select<?php echo $uniq; ?>(obj)
    {
        $('#right_side<?php echo $uniq; ?>').show();
        $('#employees<?php echo $uniq; ?>').simaGuiTable('setFixedFilter', {
            work_contracts: {
                currently_active: true,
                work_position: {
                    paygrade: {
                        ids: obj.attr('model_id')
                    }
                }
            }
        }).simaGuiTable('populate');
    }
    function on_unselect<?php echo $uniq; ?>(obj)
    {   
        $('#right_side<?php echo $uniq; ?>').hide();
        $('#employees<?php echo $uniq; ?>').simaGuiTable('setFixedFilter', {
            work_contracts: {
                ids: -1
            }
        }).simaGuiTable('populate');
    }
</script>