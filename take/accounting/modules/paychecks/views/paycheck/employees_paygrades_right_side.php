
<div id="right_side<?php echo $uniq; ?>" class="sima-layout-panel" style="display: none;">
    <?php            
        $this->widget('base.extensions.SIMAGuiTable.SIMAGuiTable', [
            'id'=>'employees'.$uniq,
            'model'=> Employee::model(),
            'columns_type'=>'in_finances',
            'add_button'=>false,
            'fixed_filter'=>[                    
                'ids'=>-1,
            ]    
        ]);
    ?>
</div>

