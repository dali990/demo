<?php

$uniq = SIMAHtml::uniqid();

$this->widget('SIMAGuiTable', [
    'id' => 'paychecks' . $uniq,
    'model' => Paycheck::model(),
    'columns_type' => 'forEmployee',
    'fixed_filter'=>array(
        'employee' => array(
            'ids' => $model->id
        ),
        'paycheck_period' => [
            'confirmed' => true,
            'pp_type' => PaycheckPeriod::$FINAL,
        ],
        'is_valid' => true,
        'scopes' => [
            'with_generated_file',
        ],
        'display_scopes' => [
            'orderByPeriodDesc'
        ]
    )
]);
