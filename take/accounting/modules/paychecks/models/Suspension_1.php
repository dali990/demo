<?php

class Suspension extends SIMAActiveRecord
{
    public static $TYPE_ARBITRARLY = 'ARBITRARLY';
    public static $TYPE_AUTO_ALL = 'AUTO_ALL';
    public static $TYPE_AUTO_INSTALLMENT = 'AUTO_INSTALLMENT';
    
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'accounting.suspensions';
    }

    public function moduleName()
    {
        return 'paychecks';
    }
    
    public function __get($column)
    {
        switch ($column)
        {
            case 'DisplayName': 
            case 'SearchName': 
                $result = $this->name;
                return $result;
            default: return parent::__get($column);
        }
    }
    
    public function rules()
    {
        return array(
            array('name, employee_id, creditor_id, begin_month_id, total_amount, installment_amount, payment_type', 'required'),
            array('paid_amount, left_to_pay_amount, creditor_bank_account_id, suspension_group_id', 'safe'),
            array('total_amount', 'numerical'),
            array('installment_amount', 'numerical'),
            array('total_amount', 'checkTotalAmount', 'on'=>['insert', 'update']),
            array('installment_amount', 'checkInstallmentAmount', 'on'=>['insert', 'update']),
            array('paid_amount', 'checkPaidAmount', 'on'=>['insert', 'update'])
        );
    }
    
    public function checkTotalAmount()
    {
        if($this->total_amount <= 0)
        {
            $this->addError('total_amount', Yii::t('PaychecksModule.Suspension', 'TotalAmountIsZero'));
        }
        else if($this->total_amount < $this->installment_amount)
        {
            $this->addError('total_amount', Yii::t('PaychecksModule.Suspension', 'TotalAmountIsLessThanInstallmentAmount'));
        }
    }
    
    public function checkInstallmentAmount()
    {
        if($this->payment_type === Suspension::$TYPE_AUTO_INSTALLMENT && $this->installment_amount <= 0 )
        {
            $this->addError('installment_amount', Yii::t('PaychecksModule.Suspension', 'InstallmentAmountIsZero'));
        }
    }
    
    public function checkPaidAmount()
    {
        if($this->paid_amount > $this->total_amount)
        {
            $this->addError('paid_amount', Yii::t('PaychecksModule.Suspension', 'PaidAmountIsMoreThanTotalAmount'));
        }
    }
    
    public function relations($child_relations = [])
    {
        return [
            'employee' => [self::BELONGS_TO, 'Employee', 'employee_id'],
            'creditor' => [self::BELONGS_TO, 'Partner', 'creditor_id'],
            'creditor_bank_account' => [self::BELONGS_TO, 'BankAccount', 'creditor_bank_account_id'],
            'begin_month' => [self::BELONGS_TO, 'Month', 'begin_month_id'],
            'suspension_group' => [self::BELONGS_TO, 'SuspensionGroup', 'suspension_group_id']
        ];
    }
    
    public function BeforeSave()
    {
//        if($this->isNewRecord && !empty($this->suspension_group_id))
//        if(!empty($this->suspension_group_id))
//        {
//            $suspensionGroup = $this->suspension_group;
//            
//            if($this->name !== $suspensionGroup->name)
//            {
//                $this->name = $suspensionGroup->name;
//            }
//            if($this->creditor_id !== $suspensionGroup->creditor_id)
//            {
//                $this->creditor_id = $suspensionGroup->creditor_id;
//            }
//            if($this->begin_month_id !== $suspensionGroup->month_id)
//            {
//                $this->begin_month_id = $suspensionGroup->month_id;
//            }
//            if($this->creditor_bank_account_id !== $suspensionGroup->creditor_bank_account_id)
//            {
//                $this->creditor_bank_account_id = $suspensionGroup->creditor_bank_account_id;
//            }
//            if($this->payment_type !== Suspension::$TYPE_AUTO_ALL)
//            {
//                $this->payment_type = Suspension::$TYPE_AUTO_ALL;
//            }
////            $suspensionGroup = ModelController::GetModel('SuspensionGroup', $this->suspension_group_id);
////            $this->name = $suspensionGroup->name;
////            $this->creditor_id = $suspensionGroup->creditor_id;
////            $this->begin_month_id = $suspensionGroup->month_id;
////            $this->total_amount = $suspensionGroup->amount;
////            $this->installment_amount = $suspensionGroup->amount;
////            $this->creditor_bank_account_id = $suspensionGroup->creditor_bank_account_id;
////            $this->payment_type = Suspension::$TYPE_AUTO_ALL;
//        }
        
        $confirmed_paycheck_suspensions_amount = 0;
        if(!$this->isNewRecord)
        {
            $confirmed_paycheck_suspensions_criteria = new SIMADbCriteria([
                'Model' => PaycheckSuspension::model(),
                'model_filter' => [
                    'suspension' => [
                        'ids' => $this->id
                    ],
                    'paycheck' => [
                        'confirmed' => true
                    ]
                ]
            ]);
            $confirmed_paycheck_suspensions_criteria->select = 'sum(amount) as amount';
            
            $confirmed_paycheck_suspensions_sum = PaycheckSuspension::model()->find($confirmed_paycheck_suspensions_criteria);
            if(!empty($confirmed_paycheck_suspensions_sum))
            {
                $confirmed_paycheck_suspensions_amount = $confirmed_paycheck_suspensions_sum->amount;
            }
        }
        
        $this->left_to_pay_amount = $this->total_amount - $this->paid_amount - $confirmed_paycheck_suspensions_amount;

        return parent::BeforeSave();
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters': return array_merge(parent::modelSettings('filters'), array(
                'employee' => 'relation',
                'employee_id' => 'dropdown',
                'creditor' => 'relation',
                'creditor_id' => 'dropdown',
                'creditor_bank_account' => 'relation',
                'creditor_bank_account_id' => 'dropdown',
                'begin_month' => 'relation',
                'begin_month_id' => 'dropdown',
                'total_amount' => 'numeric',
                'installment_amount' => 'numeric',
                'paid_amount' => 'numeric',
                'left_to_pay_amount' => 'numeric',
                'suspension_group' => 'relation',
                'suspension_group_id' => 'dropdown',
                'payment_type' => 'dropdown'
            ));
            case 'number_fields': return [
                'total_amount',
                'installment_amount',
                'paid_amount',
                'left_to_pay_amount'
            ];
            case 'options':
                $options = [];
                
                if(Yii::app()->user->checkAccess('PaychecksSuspensionAccess'))
                {
                    $options = ['form', 'delete'];
                }
                
                return $options;
            case 'textSearch' : return array(
                'name' => 'text'
            );
            case 'multiselect_options' : return [
                'delete'
            ];
            default: return parent::modelSettings($column);
        }
    }
    
    public function getTypeData()
    {
        return [
            Suspension::$TYPE_ARBITRARLY => Yii::t('PaychecksModule.Suspension', 'TypeArbitrarly'),
            Suspension::$TYPE_AUTO_ALL => Yii::t('PaychecksModule.Suspension', 'TypeAutoAll'),
            Suspension::$TYPE_AUTO_INSTALLMENT => Yii::t('PaychecksModule.Suspension', 'TypeAutoInstallment'),
        ];
    }
}
