<?php

class PaycheckPeriod extends SIMAActiveRecord
{
    public $order_num_lower_than = null;
    public $confirmed_period_changes = [];
    
    public static $REGULAR = 'REGULAR';
    public static $FINAL = 'FINAL';
    public static $BONUS = 'BONUS';
    
    public static $pp_types = [
        'REGULAR' => 'Regularna',
        'FINAL' => 'Konačna',
        'BONUS' => 'Bonus',
    ];
    
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'accounting.paycheck_periods';
    }

    public function moduleName()
    {
        return 'paychecks';
    }

    public function __get($column)
    {
        switch ($column)
        {
//            case 'DisplayName': return $this->display_name;
            case 'DisplayName':
            case 'SearchName': return $this->month->DisplayName.'('.$this->ordering_number.')';
            case 'year': return $this->month->year;
            case 'isRegular': return $this->pp_type == PaycheckPeriod::$REGULAR;
            case 'isFinal': return $this->pp_type == PaycheckPeriod::$FINAL;
            case 'isBonus': return $this->pp_type == PaycheckPeriod::$BONUS;
            default: return parent::__get($column);
        }
    }

    public function relations($child_relations = [])
    {
        return array(
            'regular_booking_file' => [self::BELONGS_TO, 'FILE', 'regular_booking_file_id'],
            'paychecks_by_min_tax_base' => array(self::STAT, 'Paycheck', 'paycheck_period_id',
                'condition' => 'is_by_min_tax_base = TRUE',
                'select' => 'count(*)'
            ),
//            'paycheck_xmls' => array(self::HAS_MANY, 'PaycheckXML', 'paycheck_period_id'),
            'paychecks' => array(self::HAS_MANY, 'Paycheck', 'paycheck_period_id'),
            'valid_paychecks' => array(self::HAS_MANY, 'Paycheck', 'paycheck_period_id', 'condition'=>'is_valid=TRUE'),
            'month' => array(self::BELONGS_TO, 'Month', 'month_id', 'scopes'=>'onlyWithAccountingMonth'),
            'creator_user' => array(self::BELONGS_TO, 'User', 'creator_user_id'),
            'payment' => array(self::BELONGS_TO, 'Payment', 'payment_id'),
            'number_of_paychecks' => [self::STAT, 'Paycheck', 'paycheck_period_id', 'select'=>'count(*)'],
            'number_of_paychecks_without_full_work_hours' => [self::STAT, 'Paycheck', 'paycheck_period_id', 'select'=>'count(*)', 'condition'=>'worked_hours<month_period_hours'],
            'total_bruto' => [self::STAT, 'Paycheck', 'paycheck_period_id', 'select'=>'sum(amount_bruto)'],
            'total_tax_base' => [self::STAT, 'Paycheck', 'paycheck_period_id', 'select'=>'sum(tax_base)'],
            'total_tax_emp' => [self::STAT, 'Paycheck', 'paycheck_period_id', 'select'=>'sum(tax_empl)'],
            'total_base_bruto' => [self::STAT, 'Paycheck', 'paycheck_period_id', 'select'=>'sum(base_bruto)'],
            'total_pio' => [self::STAT, 'Paycheck', 'paycheck_period_id', 'select'=>'sum(pio_empl+pio_comp)'],
            'total_zdr' => [self::STAT, 'Paycheck', 'paycheck_period_id', 'select'=>'sum(zdr_empl+zdr_comp)'],
            'total_nez' => [self::STAT, 'Paycheck', 'paycheck_period_id', 'select'=>'sum(nez_empl+nez_comp)'],
            'average_neto_salary' => [self::STAT, 'Paycheck', 'paycheck_period_id', 'select'=>'avg(amount_neto)', 'condition'=>'worked_hours=month_period_hours'],
            'sum_total_salary' => [self::STAT, 'Paycheck', 'paycheck_period_id', 'select'=>'sum(amount_total)'],
            'sum_neto_salary' => [self::STAT, 'Paycheck', 'paycheck_period_id', 'select'=>'sum(amount_neto)'],
            'min_neto_salary' => [self::STAT, 'Paycheck', 'paycheck_period_id', 'select'=>'min(amount_neto)', 'condition'=>'worked_hours=month_period_hours'],
            'max_neto_salary' => [self::STAT, 'Paycheck', 'paycheck_period_id', 'select'=>'max(amount_neto)', 'condition'=>'worked_hours=month_period_hours'],
            'number_of_paychecks' => [self::STAT, 'Paycheck', 'paycheck_period_id', 'select'=>'count(*)'],
            'number_of_paychecks_with_sickleave' => [self::STAT, 'Paycheck', 'paycheck_period_id', 
                'select' => 'count(*)',
                'condition' => 'EXISTS (SELECT 1'
                    .' FROM '.PaycheckAdditional::model()->tableName().' pa'
                    .' WHERE pa.paycheck_id=t.id AND type IN ('
                        .'\''.PaycheckAdditional::$TYPE_SICK_LEAVE_INJURY_AT_WORK.'\''
                        .', \''.PaycheckAdditional::$TYPE_SICK_LEAVE_MATERNITY.'\''
                        .', \''.PaycheckAdditional::$TYPE_SICK_LEAVE_OVER_30.'\''
                        .', \''.PaycheckAdditional::$TYPE_SICK_LEAVE_PREGNANCY_OVER_30.'\''
                        .', \''.PaycheckAdditional::$TYPE_SICK_LEAVE_PREGNANCY_TILL_30.'\''
                        .', \''.PaycheckAdditional::$TYPE_SICK_LEAVE_TILL_30.'\''
                    .') LIMIT 1)'
            ],
            'days_on_annual_leave' => [self::STAT, 'Paycheck', 'paycheck_period_id', 
                'select' => 'sum(coalesce(pa.number_of_calendar_days, pa.hours/8))',
                'join' => ', '.PaycheckAdditional::model()->tableName().' pa',
                'condition' => 'pa.paycheck_id=t.id AND type=\''.PaycheckAdditional::$TYPE_ANNUAL_LEAVE.'\''
            ],
            'days_on_paid_leave' => [self::STAT, 'Paycheck', 'paycheck_period_id', 
                'select' => 'sum(coalesce(pa.number_of_calendar_days, pa.hours/8))',
                'join' => ', '.PaycheckAdditional::model()->tableName().' pa',
                'condition' => 'pa.paycheck_id=t.id AND type=\''.PaycheckAdditional::$TYPE_PAID_LEAVE.'\''
            ],
            'days_on_state_and_religious_holiday' => [self::STAT, 'Paycheck', 'paycheck_period_id', 
                'select' => 'sum(coalesce(pa.number_of_calendar_days, pa.hours/8))',
                'join' => ', '.PaycheckAdditional::model()->tableName().' pa',
                'condition' => 'pa.paycheck_id=t.id AND type=\''.PaycheckAdditional::$TYPE_STATE_AND_RELIGIOUS_HOLIDAY.'\''
            ],
            'days_on_sick_leave_till_30' => [self::STAT, 'Paycheck', 'paycheck_period_id', 
                'select' => 'sum(coalesce(pa.number_of_calendar_days, pa.hours/8))',
                'join' => ', '.PaycheckAdditional::model()->tableName().' pa',
                'condition' => 'pa.paycheck_id=t.id AND type=\''.PaycheckAdditional::$TYPE_SICK_LEAVE_TILL_30.'\''
            ],
            'days_on_sick_leave_over_30' => [self::STAT, 'Paycheck', 'paycheck_period_id', 
                'select' => 'sum(coalesce(pa.number_of_calendar_days, pa.hours/8))',
                'join' => ', '.PaycheckAdditional::model()->tableName().' pa',
                'condition' => 'pa.paycheck_id=t.id AND type=\''.PaycheckAdditional::$TYPE_SICK_LEAVE_OVER_30.'\''
            ],
            'days_on_sick_leave_injury_at_work' => [self::STAT, 'Paycheck', 'paycheck_period_id', 
                'select' => 'sum(coalesce(pa.number_of_calendar_days, pa.hours/8))',
                'join' => ', '.PaycheckAdditional::model()->tableName().' pa',
                'condition' => 'pa.paycheck_id=t.id AND type=\''.PaycheckAdditional::$TYPE_SICK_LEAVE_INJURY_AT_WORK.'\''
            ],
            'days_on_sick_leave_maternity' => [self::STAT, 'Paycheck', 'paycheck_period_id', 
                'select' => 'sum(coalesce(pa.number_of_calendar_days, pa.hours/8))',
                'join' => ', '.PaycheckAdditional::model()->tableName().' pa',
                'condition' => 'pa.paycheck_id=t.id AND type=\''.PaycheckAdditional::$TYPE_SICK_LEAVE_MATERNITY.'\''
            ],
            'days_on_sick_leave_pregnancy_till_30' => [self::STAT, 'Paycheck', 'paycheck_period_id', 
                'select' => 'sum(coalesce(pa.number_of_calendar_days, pa.hours/8))',
                'join' => ', '.PaycheckAdditional::model()->tableName().' pa',
                'condition' => 'pa.paycheck_id=t.id AND type=\''.PaycheckAdditional::$TYPE_SICK_LEAVE_PREGNANCY_TILL_30.'\''
            ],
            'days_on_sick_leave_pregnancy_over_30' => [self::STAT, 'Paycheck', 'paycheck_period_id', 
                'select' => 'sum(coalesce(pa.number_of_calendar_days, pa.hours/8))',
                'join' => ', '.PaycheckAdditional::model()->tableName().' pa',
                'condition' => 'pa.paycheck_id=t.id AND type=\''.PaycheckAdditional::$TYPE_SICK_LEAVE_PREGNANCY_OVER_30.'\''
            ],
            'ppppds' => [self::MANY_MANY, 'PPPPD', PaycheckPeriodToPPPPD::model()->tableName().'(paycheck_period_id, ppppd_id)'],
            'ppppds_count' => [self::STAT, 'PPPPD', PaycheckPeriodToPPPPD::model()->tableName().'(paycheck_period_id, ppppd_id)', 'select' => 'count(*)']
        );
    }

    public function rules()
    {
        return array(
            array('month_id, payment_date, creator_user_id', 'required'),
            array('ordering_number, payment_date, pp_type, hours_percentage, tax_release_used', 'safe'),
            array('call_for_number_partner, old_period, payment_id', 'safe'),
//            array('month_point_value, month_hours, without_additionals, employees_work_full_month', 'safe'),
            array('month_point_value, without_additionals, employees_work_full_month', 'safe'),
            ['regular_booking_file_id','safe'],
            array('pp_type','checkFinal'),
            ['hours_percentage', 'required'],
//            array('hours_percentage','checkHoursSum'),
//            array('tax_release_used','checkTaxRelease'),
            array('hours_percentage', 'numerical','max' => 100,'min' => 0),
            array('tax_release_used', 'numerical', 'min' => 0),
            array('ordering_number', 'numerical'),
            array('ordering_number','checkOrderingNumber'),
            array('month_id','checkMonth'),
            ['confirmed', 'checkConfirm'],
            ['payment_date', 'checkPaymentDate'],
            ['hours_percentage', 'checkValuesHigherThanPrev'],
            ['hours_percentage', 'checkValuesLowerThanNext'],
            ['id', 'checkDeleteWithPaychecks', 'on' => ['delete']]
        );
    }
    
    public function scopes()
    {
        $alias = $this->getTableAlias();
        return array(
            'byName' => array(
                'order' => $alias.'.display_name'
            ),
            'byPaymentDateDESC' => [
                'order' => $alias.'.payment_date DESC'
            ],
            'byOrderingNumberDESC' => [
                'order' => $alias.'.ordering_number DESC'
            ],
            'without_old' => array(
                'condition' => $this->getTableAlias(FALSE, FALSE) . '.old_period=false'
            )
        );
    }
    
    public function afterValidate() 
    {
        if ($this->getIsNewRecord())
        {
            $criteria = new SIMADbCriteria([
                'Model' => 'PaycheckPeriod',
                'model_filter' => array(
                    'month' => array(
                        'ids' => $this->month_id
                    ),
                    'old_period' => true
                )
            ]);
            $oldPeriod = PaycheckPeriod::model()->find($criteria);
            
            if(isset($oldPeriod))
            {
                $this->setIsNewRecord(false);
                $this->id = $oldPeriod->id;
            }
        }
    }
    
    public function afterSave()
    {
        parent::afterSave();
                
        if(
                $this->last_calculation_start_time !== $this->__old['last_calculation_start_time']
                ||
                $this->last_calculation_end_time !== $this->__old['last_calculation_end_time']
                ||
                $this->last_modification_time !== $this->__old['last_modification_time']
        )
        {
            if(Yii::app()->isWebApplication())
            {
                Yii::app()->controller->raiseUpdateModels($this);
            }
            
            if($this->confirmed === true && !empty($this->getLastModificationCalculationErrors()))
            {
                $info_text = implode('</br>', $this->confirmed_period_changes);
                Yii::app()->notifManager->sendNotif($this->creator_user_id, Yii::t('PaychecksModule.PaycheckPeriod', 'ConfirmedPeriodChanged', [
                    '{pp}' => $this->DisplayName
                ]), [
                    'info' => $info_text,
                ]);
            }
        }
    }
    
    public function checkValuesHigherThanPrev()
    {
        if(!$this->isFirstPaycheckPeriodInMonth())
        {
            $prev = $this->prev();
            
            if(!empty($prev) && $this->confirmed===true && $prev->confirmed===false )
            {
                if(isset($this->hours_percentage) && $this->hours_percentage < $prev->hours_percentage)
                {
                    $this->addError('hours_percentage', Yii::t('PaychecksModule.PaycheckPeriod', 'ValueMustBeGreaterThanPrev', [
                        '{param}' => $this->getAttributeLabel('hours_percentage'),
                        '{this_val}' => $this->hours_percentage,
                        '{prev_val}' => $prev->hours_percentage
                    ]));
                }
                if(!($this->isBonus) && isset($this->tax_release_used) && $this->tax_release_used < $prev->tax_release_used)
                {
                    $this->addError('tax_release_used', Yii::t('PaychecksModule.PaycheckPeriod', 'ValueMustBeGreaterThanPrev', [
                        '{param}' => $this->getAttributeLabel('tax_release_used'),
                        '{this_val}' => $this->tax_release_used,
                        '{prev_val}' => $prev->tax_release_used
                    ]));
                }
                if(isset($this->payment_date) && Day::getByDate($this->payment_date)->compare(Day::getByDate($prev->payment_date)) < 0)
                {
                    $this->addError('payment_date', Yii::t('PaychecksModule.PaycheckPeriod', 'ValueMustBeGreaterThanPrev', [
                        '{param}' => $this->getAttributeLabel('payment_date'),
                        '{this_val}' => $this->payment_date,
                        '{prev_val}' => $prev->payment_date
                    ]));
                }
                if(!($this->isBonus) && isset($this->month_point_value) && $this->month_point_value < $prev->month_point_value)
                {
                    $this->addError('month_point_value', Yii::t('PaychecksModule.PaycheckPeriod', 'ValueMustBeGreaterThanPrev', [
                        '{param}' => $this->getAttributeLabel('month_point_value'),
                        '{this_val}' => $this->month_point_value,
                        '{prev_val}' => $prev->month_point_value
                    ]));
                }
            }
        }
    }
    
    public function checkValuesLowerThanNext()
    {
        if($this->isRegular)
        {
            $next = $this->next();
            if(!empty($next))
            {
//                if(isset($this->hours_percentage) && $this->hours_percentage < $prev->hours_percentage)
//                {
//                    $this->addError('hours_percentage', Yii::t('PaychecksModule.PaycheckPeriod', 'ValueMustBeGreaterThanPrev', [
//                        '{param}' => $this->getAttributeLabel('hours_percentage'),
//                        '{this_val}' => $this->hours_percentage,
//                        '{prev_val}' => $prev->hours_percentage
//                    ]));
//                }
//                if(isset($this->tax_release_used) && $this->tax_release_used < $prev->tax_release_used)
//                {
//                    $this->addError('tax_release_used', Yii::t('PaychecksModule.PaycheckPeriod', 'ValueMustBeGreaterThanPrev', [
//                        '{param}' => $this->getAttributeLabel('tax_release_used'),
//                        '{this_val}' => $this->tax_release_used,
//                        '{prev_val}' => $prev->tax_release_used
//                    ]));
//                }
                if(isset($this->payment_date) && Day::getByDate($this->payment_date)->compare(Day::getByDate($next->payment_date)) > 0)
                {
                    $this->addError('payment_date', Yii::t('PaychecksModule.PaycheckPeriod', 'ValueMustBeLesserThanNext', [
                        '{param}' => $this->getAttributeLabel('payment_date'),
                        '{this_val}' => $this->payment_date,
                        '{next_val}' => $next->payment_date
                    ]));
                }
//                if(isset($this->month_point_value) && $this->month_point_value < $prev->month_point_value)
//                {
//                    $this->addError('month_point_value', Yii::t('PaychecksModule.PaycheckPeriod', 'ValueMustBeGreaterThanPrev', [
//                        '{param}' => $this->getAttributeLabel('month_point_value'),
//                        '{this_val}' => $this->month_point_value,
//                        '{prev_val}' => $prev->month_point_value
//                    ]));
//                }
            }
        }
    }
    
    public function checkDeleteWithPaychecks()
    {
        if($this->number_of_paychecks > 0)
        {
            throw new SIMAWarnDeletePaycheckPeriodWithPaychecks();
        }
    }
    
    public function checkPaymentDate()
    {
        if(isset($this->payment_date))
        {
            $day = Day::getByDate($this->payment_date);
            if($day->IsWeekend)
            {
                $this->addError('payment_date', Yii::t('PaychecksModule.PaycheckPeriod', 'PaymentDateMustBeWorkDay'));
            }
            $month = $this->month;
            if(!is_null($month) && $day->compare($month->firstDay()) < 0)
            {
                $this->addError('payment_date', Yii::t('PaychecksModule.PaycheckPeriod', 'PaymentDateCanNotBeBeforeMonth'));
            }
        }
    }
    
    public function checkConfirm()
    {
        if($this->__old['confirmed'] !== true && $this->confirmed === true && !$this->hasErrors())
        {
            $error_messages = [];
            
            /// provera save timestamp-ova
            $last_modification_calculation_errors = $this->getLastModificationCalculationErrors();
            foreach($last_modification_calculation_errors as $last_modification_calculation_error)
            {
                $error_messages[] = Yii::t('PaychecksModule.PaychecksEmployee', $last_modification_calculation_error);
            }
            
            if(!isset(Yii::app()->company->work_code))
            {
//                $errors[] = 'Kompaniji "' . Yii::app()->company . '" nije zadata sifra delatnosti';
                $error_messages[] = Yii::t('PaychecksModule.PaycheckPeriod', 'CompanyDoesNotHaveWorkCode', [
                    '{company}' => Yii::app()->company->company->DisplayName
                ]);
            }
            
            if(!isset(Yii::app()->company->MB))
            {
//                $errors[] = 'Kompaniji "' . Yii::app()->company->company . '" nije zadat maticni broj';
                $error_messages[] = Yii::t('PaychecksModule.PaycheckPeriod', 'CompanyDoesNotHaveMB', [
                    '{company}' => Yii::app()->company->company->DisplayName
                ]);
            }
            
            if(!isset(Yii::app()->company->PIB))
            {
//                $errors[] = 'Kompaniji "' . Yii::app()->company->company . '" nije zadat PIB';
                $error_messages[] = Yii::t('PaychecksModule.PaycheckPeriod', 'CompanyDoesNotHavePIB', [
                    '{company}' => Yii::app()->company->company->DisplayName
                ]);
            }
            
//            if(!isset(Yii::app()->company->main_address))
            $ma = Yii::app()->company->MainAddress;
            if(!isset($ma))
            {
//                $errors[] = 'Kompaniji "' . Yii::app()->company->company . '" nije zadata glavna adresa';
                $error_messages[] = Yii::t('PaychecksModule.PaycheckPeriod', 'CompanyDoesNotHaveMainAddress', [
                    '{company}' => Yii::app()->company->company->DisplayName
                ]);
            }
            
//            if(!isset(Yii::app()->company->main_bank_account))
            $mba = Yii::app()->company->MainBankAccount;
            if(!isset($mba))
            {
                $error_messages[] = Yii::t('PaychecksModule.PaycheckPeriod', 'CompanyDoesNotHaveMainBankAccount', [
                    '{company}' => Yii::app()->company->company->DisplayName
                ]);
            }
            
            if(empty(Yii::app()->company->main_or_any_email_display))
            {
//                $errors[] = 'Kompaniji "' . Yii::app()->company->company . '" nije zadata email adresa';
                $error_messages[] = Yii::t('PaychecksModule.PaycheckPeriod', 'CompanyDoesNotHaveMainEmailAddress', [
                    '{company}' => Yii::app()->company->company->DisplayName
                ]);
            }
            
            $paychecks = $this->paychecks;
            foreach($paychecks as $paycheck)
            {
                if($paycheck->validate() !== true)
                {
//                    $error_messages = array_merge(
//                            $error_messages,
//                            
//                    );
                    $error_messages[] = Yii::t('PaychecksModule.PaycheckPeriod', 'PaycheckError');
                }
                
                $error_messages = array_merge(
                        $error_messages
                        ,
                        $paycheck->GetConfirmErrors()
                );
            }
            
            $partner_id = Yii::app()->configManager->get('accounting.paychecks.state_partner');
            if(empty($partner_id))
            {
                $error_messages[] = Yii::t('PaychecksModule.PaychecksEmployee', 'StatePartnerConfigParamNotSet');
            }
            else
            {
                $partner = Partner::model()->findByPk($partner_id);
                if(empty($partner))
                {
                    $error_messages[] = Yii::t('PaychecksModule.PaychecksEmployee', 'StatePartnerNotFound');
                }
                else
                {
                    $partner_main_account = $partner->MainBankAccount;
                    if(empty($partner_main_account))
                    {
                        $error_messages[] = Yii::t('PaychecksModule.PaychecksEmployee', 'StatePartnerMainBankAccountNotSet');
                    }
                }
            }
            
            if(count($error_messages) > 0)
            {
                $this->addError('confirmed', Yii::t('PaychecksModule.PaycheckPeriod', 'CannotConfirm', [
                    '{error_messages}' => implode('</br>', $error_messages)
                ]));
            }
        }
    }
    
    private function getLastModificationCalculationErrors()
    {
        $result = [];
        
        /// provera save timestamp-ova
        if(isset($this->last_modification_time)) /// ako je postavljen, nije newrecord
        {
            /// ako je modifikovano nakon poslednjeg obracuna
            if(isset($this->last_calculation_start_time)
                    && strtotime($this->last_calculation_start_time) < strtotime($this->last_modification_time))
            {
                $result[] = 'ModifiedAfterLastCalculationStart';
            }
        }
        if(!isset($this->last_calculation_start_time)) /// nema zapocetog obracuna
        {
            $result[] = 'NoCalculationStarted';
        }
        else if(!isset($this->last_calculation_end_time)) /// nema zavrsenog obracuna
        {
                $result[] = 'NoCalculationEnded';
        }
        else if(strtotime($this->last_calculation_end_time) < strtotime($this->last_calculation_start_time))
        {
            $result[] = 'NoCalculationSuccessfullyEnded';
        }
        
        return $result;
    }
    
    public function checkMonth()
    {
        if(isset($this->month_id))
        {
            $accountingMonth = AccountingMonth::model()->findByPk($this->month_id);
            
            if(!isset($accountingMonth))
            {
                $this->addError('month_id', Yii::t('PaychecksModule.PaycheckPeriod', 'ChosenMonthNotFinacial'));
            }
        }
    }
    
    public function checkOrderingNumber()
    {
        if(isset($this->ordering_number))
        {
            $criteria = new SIMADbCriteria([
                'Model' => 'PaycheckPeriod',
                'model_filter' => array(
                    'month' => array(
                        'ids' => $this->month_id
                    ),
                    'ordering_number' => $this->ordering_number
                )
            ]);
            $criteria->scopes = array('without_old');
            $periods = PaycheckPeriod::model()->findAll($criteria);
            foreach($periods as $period)
            {
                if($this->id !== $period->id)
                {
                    $this->addError('ordering_number', 
                            Yii::t('PaychecksModule.PaycheckPeriod', 'PaycheckPeriodNumForMonthExists')
                    );
                    break;
                }
            }
        }
    }
    
    public function checkTaxRelease()
    {
        if($this->tax_release_used !== null)
        {
            $calculatedTaxRelease = $this->tax_release_used;

            $criteria = new SIMADbCriteria([
                'Model' => 'PaycheckPeriod',
                'model_filter' => array(
                    'month' => array(
                        'ids' => $this->month_id
                    ),
                )
            ]);
            $periods = PaycheckPeriod::model()->findAll($criteria);
            foreach($periods as $period)
            {
                if($this->getIsNewRecord() || $this->id !== $period->id)
                {
                    $calculatedTaxRelease += $period->tax_release_used;
                }
            }
            
            $month_tax_release = $this->month->param('accounting.tax_release');
            if($calculatedTaxRelease > $month_tax_release)
            { 
                $this->addError('tax_release_used', 
                    Yii::t(
                        'PaychecksModule.PaycheckPeriod', 
                        'Tax release higher', 
                        array('{calculated}'=>$calculatedTaxRelease, '{highest}'=>$month_tax_release)
                    ));
            }
        }
    }
    public function checkHoursSum()
    {
        if($this->hours_percentage !== null)
        {
            $sum = $this->hours_percentage;

            $criteria = new SIMADbCriteria([
                'Model' => 'PaycheckPeriod',
                'model_filter' => array(
                    'month' => array(
                        'ids' => $this->month_id
                    ),
                )
            ]);
            $periods = PaycheckPeriod::model()->findAll($criteria);
            foreach($periods as $period)
            {
                if($this->getIsNewRecord() || $this->id !== $period->id)
                {
                    $sum += $period->hours_percentage;
                }
            }
            if($sum > 100)
            {
                $this->addError('hours_percentage', 'Procenat sati za mesec bi bio iznad 100%');
            }
        }
    }
    
    public function checkFinal()
    {
        if($this->isFinal)
        {            
            if($this->checkFinalDoesAlreadyExists())
            {
                $this->addError('pp_type', Yii::t('PaychecksModule.PaycheckPeriod', 'FinalPaycheckPeriodAlreadyExists'));
            }
            if(!$this->isOrderNumBiggerThenAllRegulars())
            {
                $this->addError('pp_type', Yii::t('PaychecksModule.PaycheckPeriod', 'FinalPaycheckPeriodMustBeLast'));
            }
        }
        elseif ($this->isBonus)
        {
            //?????????
        }
        else
        {
            if(!isset($this->hours_percentage))
            {
                $this->addError('pp_type', 'Mora biti postavljeno da li je finalno');
                $this->addError('hours_percentage', 'ili pretpostavljeni procenat');
            }
            
//            if($this->checkFinalDoesAlreadyExists())
//            {
//                $this->addError('is_final', Yii::t('PaychecksModule.PaycheckPeriod', 'FinalPaycheckPeriodMustBeLast'));
//            }
        }
        
        if((int)($this->hours_percentage) === 100 && !$this->isFinal)
        {
            $this->addError('pp_type', 'Isplata mora biti Konačna da bi procenat sati mogao biti 100%');
            $this->addError('hours_percentage', 'Isplata mora biti Konačna da bi procenat sati mogao biti 100%');
        }
    }
    
    private function checkFinalDoesAlreadyExists()
    {
        $criteria = new SIMADbCriteria(array(
            'Model' => 'PaycheckPeriod',
            'model_filter' => array(
                'month' => array(
                    'ids' => $this->month_id
                ),
                'pp_type' => PaycheckPeriod::$FINAL,
            )
        ));
        $criteria->scopes = array('without_old');
        $periods = PaycheckPeriod::model()->findAll($criteria);
        
        $already_exists = false;
            
        if($this->getIsNewRecord() && count($periods)>0)
        {
            $already_exists = true;
        }
        else if(!$this->getIsNewRecord())
        {
            foreach($periods as $period)
            {
                if($period->id !== $this->id)
                {
                    $already_exists = true;
                    break;
                }
            }
        }
        
        return $already_exists;
    }
    
    public function beforeSave() 
    {
        if(empty($this->month_point_value) && !$this->isBonus)
        {
            $this->month_point_value = $this->month->param('accounting.point_value');
        }

        //MilosS: izracunavanje ce se raditi svaki put
            $hours = 0;
            $days = $this->month->getDays();
            foreach($days as $day)
            {
                $hours += $day->getWorkHours();
            }
            $this->month_hours = $hours;
        ////////
        
        if($this->confirmed === false)
        {
            $toSetLastModificationTime = false;
            if($this->isNewRecord === true
                || ($this->isNewRecord === false 
                    && (
                        strval($this->__old['month_id']) !== strval($this->month_id)
                        ||
                        floatval($this->__old['hours_percentage']) !== floatval($this->hours_percentage)
                        ||
                        strval($this->__old['pp_type']) !== strval($this->pp_type)
                        ||
                        strval($this->__old['tax_release_used']) !== strval($this->tax_release_used)
                        ||
                        strval($this->__old['ordering_number']) !== strval($this->ordering_number)
                        ||
                        strval($this->__old['payment_date']) !== strval($this->payment_date)
                        ||
                        floatval($this->__old['month_point_value']) !== floatval($this->month_point_value)
//                        ||
//                        strval($this->__old['month_hours']) !== strval($this->month_hours)
                        ||
                        strval($this->__old['without_additionals']) !== strval($this->without_additionals)
                        ||
                        strval($this->__old['employees_work_full_month']) !== strval($this->employees_work_full_month)
                    )))
            {
                $toSetLastModificationTime = true;
            }
            if($toSetLastModificationTime === true)
            {
//                $this->last_modification_time = SIMAHtml::UserToDbDate(date('Y-m-d H:i:s'), true);
                $this->updateLastModificationTime();
            }
        }
        
        return parent::beforeSave();
    }

    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters' : return array_merge(parent::modelSettings('filters'),array(
                'month' => 'relation',
                'pp_type' => 'dropdown',
                'old_period' => 'boolean',
                'ordering_number' => 'integer',
                'creator_user' => 'relation',
                'confirmed' => 'boolean',
                'payment_date' => 'date_time_range',
                'order_num_lower_than' => ['func' => 'filter_order_num_lower_than']
            ));
            case 'searchField': return array(
                'scopes' => array('byName')
            );
            case 'textSearch': return array(
                'display_name' => 'text'
            );
            case 'statuses':  return array(
                'confirmed' => array(
                    'title' => 'Potvrdjeno',
                    'checkAccessConfirm' => 'Confirm',
                    'checkAccessRevert' => 'Revert',
                    'onConfirm' => 'onConfirm',
                    'onRevert' => 'onConfirm',
                ),
            );
            case 'options' : if($this->confirmed){return array('form');}else{return array('form','delete');}
            default: return parent::modelSettings($column);
        }
    }
    
    public function filter_order_num_lower_than($condition, $alias)
    {
        if(isset($this->order_num_lower_than))
        {
            $temp_condition = new SIMADbCriteria([
                'Model' => PaycheckPeriod::model(),
                'alias' => $alias,
                'model_filter' => [
                    'filter_scopes' => [
                        'orderNumLowerThan' => $this->order_num_lower_than,
                    ]
                ]
            ]);
            $condition->mergeWith($temp_condition);
        }
    }
    
    public function onConfirm()
    {
        if($this->confirmed !== $this->__old['confirmed'])
        {
            $paychecks = $this->paychecks;
            foreach($paychecks as $paycheck)
            {
                $paycheck->confirmed = $this->confirmed;
                $paycheck->save();
            }
            
            if($this->confirmed === true)
            {
                Yii::app()->controller->raiseAction('sima.paychecks.afterConfirmPeriod',[$this->id], true);
            }
        }
    }
    
    public function canEdit()
    {
        $result = true;
//        if($this->creator_user_id !== Yii::app()->user->id 
//            || $this->confirmed === true)
        if($this->confirmed === true)
        {
            $result = false;
        }
        
        return $result;
    }
    
    public function haveGeneratedPayments()
    {
        $result = false;
        
//        if($this->state_num >= 4)
        if(isset($this->payment_id))
        {
            $result = true;
        }
        
        return $result;
    }
    
    public function allPaymentsBooked()
    {
        $result = false;
        
        if(isset($this->payment->bank_statement))
        {
            // ako su sva placanja uplacenja
            $number_of_booked_paychecks = 0;
            $valid_paychecks = $this->valid_paychecks;
            $number_of_valid_paychecks = count($valid_paychecks);
            foreach($valid_paychecks as $paycheck)
            {
                if(isset($paycheck->payment->bank_statement))
                {
                    $number_of_booked_paychecks++;
                }
            }

            if($number_of_booked_paychecks === $number_of_valid_paychecks)
            {
                $this->state_num = 5;
            }
        }
                
        return $result;
    }
    
    public function isFirstPaycheckPeriodInMonth()
    {
        $result = false;
        
        if($this->isNewRecord && isset($this->month))
        {
            $criteria = new SIMADbCriteria([
                'Model' => PaycheckPeriod::model(),
                'model_filter' => [
                    'month' => [
                        'ids' => $this->month->id
                    ]
                ]
            ], true);

            $result = empty($criteria->ids);
        }
        else if(intval ($this->ordering_number) === 1)
        {
            return true;
        }
                
        return $result;
    }
    
    /**
     * 
     * @return type
     */
    public function isOrderNumBiggerThenAllRegulars()
    {
        $result = false;
        
        if(empty($this->ordering_number))
        {
            $result = true;
        }
        else
        {
            $criteria = new SIMADbCriteria([
                'Model' => PaycheckPeriod::model(),
                'model_filter' => [
                    'month' => [
                        'ids' => $this->month->id
                    ],
                    'pp_type' => PaycheckPeriod::$REGULAR,
                    'filter_scopes' => array(
                        'orderNumHigherThan' => $this->ordering_number,
                    )
                ]
            ], true);
            
            $result = empty($criteria->ids);
        }
        
        return $result;
    }
    
    public function orderNumLowerThan($num)
    {
        $alias = $this->getTableAlias();
        $this->getDbCriteria()->mergeWith(array(
            'condition'=>"$alias.ordering_number<$num",
        ));
        return $this;
    }
    public function orderNumHigherThan($num)
    {
        $alias = $this->getTableAlias();
        $this->getDbCriteria()->mergeWith(array(
            'condition'=>"$alias.ordering_number>$num",
        ));
        return $this;
    }
    
    /**
     * functija vraca prvu prethodnu isplatu zarade
     * nije limit samo na trenutni mesec
     * ako je ovo prvi period, onda vraca poslednji iz prethodnog meseca
     */
    public function prev()
    {
        $result = null;
        
        if($this->isNewRecord)
        {
            if(!empty($this->month))
            {
                $criteria = new SIMADbCriteria([
                    'Model' => PaycheckPeriod::model(),
                    'model_filter' => [
                        'month' => [
                            'ids' => $this->month->id
                        ],
                        'display_scopes' => [
                            'byOrderingNumberDESC'
                        ],
                        'limit' => 1
                    ]
                ]);
                
                $result = PaycheckPeriod::model()->find($criteria);
            }
        }
        else
        {
            $month_id = null;

            $is_first_period_in_month = $this->isFirstPaycheckPeriodInMonth();

            if($is_first_period_in_month === true)
            {
                $month_id = $this->month->prev()->id;
            }
            else
            {
                $month_id = $this->month->id;
            }

            $criteria = new SIMADbCriteria([
                'Model' => PaycheckPeriod::model(),
                'model_filter' => [
                    'month' => [
                        'ids' => $month_id
                    ],
                ]
            ]);

            $paycheckPeriods = PaycheckPeriod::model()->findAll($criteria);

            foreach($paycheckPeriods as $paycheckPeriod)
            {
                if(
                    (
                        ($is_first_period_in_month === true) 
                        || 
                        ($paycheckPeriod->ordering_number < $this->ordering_number)
                    )
                    && 
                    (
                        empty($result) 
                        || 
                        ($paycheckPeriod->ordering_number > $result->ordering_number)
                    )
                )
                {
                    $result = $paycheckPeriod;
                }
            }
        }
        
        return $result;
    }
    
    /**
     * functija vraca prvu sledecu isplatu zarade
     * nije limit samo na trenutni mesec
     * ako je ovo poslednji period, onda vraca prvi iz sledeceg meseca
     */
    public function next()
    {
        $result = null;
        
        if($this->isNewRecord)
        {
        }
        else
        {
//            $model_filter = [];
            $model_filter = [
                'month' => [
                    'ids' => $this->month->id
                ],
                'filter_scopes' => [
                    'orderNumHigherThan' => $this->ordering_number
                ],
                'order_by' => 'ordering_number DESC',
                'limit' => 1
            ];
            $criteria = new SIMADbCriteria([
                'Model' => PaycheckPeriod::model(),
                'model_filter' => $model_filter
            ]);
            $result = PaycheckPeriod::model()->find($criteria);
            if (is_null($result))
            {
                $model_filter = [
                    'month' => [
                        'ids' => $this->month->next()->id
                    ],
                    'order_by' => 'ordering_number ASC',
                    'limit' => 1
                ];
                $criteria = new SIMADbCriteria([
                    'Model' => PaycheckPeriod::model(),
                    'model_filter' => $model_filter
                ]);
                $result = PaycheckPeriod::model()->find($criteria);
            }            
        }
        
        return $result;
    }
    
    public function getDifferencesFromPreviousPeriod()
    {
        $result = [];
        
        $previousPeriod = $this->prev();
        
        $previous_paychecks = [];
        $previous_month_point_value = '???';
        if(!empty($previousPeriod))
        {
            $previous_paychecks = $previousPeriod->valid_paychecks;
            $previous_month_point_value = $previousPeriod->month_point_value;
        }
        
        $current_paychecks = $this->valid_paychecks;
        $current_paychecks_count = count($current_paychecks);
        $previous_paychecks_count = count($previous_paychecks);
        
        if($current_paychecks_count !== $previous_paychecks_count)
        {
            $result[] = Yii::t('PaychecksModule.PaycheckPeriod', 'PaychecksCountNotSame', [
                '{current_paychecks_count}' => $current_paychecks_count,
                '{previous_paychecks_count}' => $previous_paychecks_count
            ]);
        }
        
        if($this->month_point_value !== $previous_month_point_value)
        {
            $result[] = Yii::t('PaychecksModule.PaycheckPeriod', 'PointValueNotSame', [
                '{current_point_value}' => $this->month_point_value,
                '{previous_point_value}' => $previous_month_point_value
            ]);
        }
        
        $new_employees = [];
        foreach($current_paychecks as $current_paycheck)
        {
            $found = false;
            
            foreach($previous_paychecks as $previous_paycheck)
            {
                if($current_paycheck->employee_id === $previous_paycheck->employee_id)
                {
                    $found = true;
                    break;
                }
            }
            
            if($found === false)
            {
                $new_employees[$current_paycheck->employee->id] = '<li>'.$current_paycheck->employee->DisplayHTML.'</li>';
            }
        }
        if(count($new_employees) > 0)
        {
            $result[] = Yii::t('PaychecksModule.PaycheckPeriod', 'NewEmployees', [
                '{new_employees}' => implode('', $new_employees),
            ]);
        }
        
        $old_employees = [];
        foreach($previous_paychecks as $previous_paycheck)
        {
            $found = false;
            
            foreach($current_paychecks as $current_paycheck)
            {
                if($current_paycheck->employee_id === $previous_paycheck->employee_id)
                {
                    $found = true;
                    break;
                }
            }
            
            if($found === false)
            {
                $old_employees[] = '<li>'.$previous_paycheck->employee->DisplayHTML.'</li>';
            }
        }
        if(count($old_employees) > 0)
        {
            $result[] = Yii::t('PaychecksModule.PaycheckPeriod', 'OldEmployees', [
                '{old_employees}' => implode('', $old_employees),
            ]);
        }
        
        return $result;
    }
    
    /**
     * 
     * @return array - niz ciji je svaki element oblika:
     *  ['emp' => emp_disp, 'susp' => suspension_disp]
     */
    public function getUnusedSuspensions()
    {
        $unused_suspensions = [];
        
        $employees = PaycheckPeriodComponent::EmployeesActiveInPeriod($this);
        foreach($employees as $employee)
        {
            $suspensions = $employee->suspensions;
            foreach($suspensions as $suspension)
            {
                if($this->month->compare($suspension->begin_month) < 0)
                {
                    continue;
                }
                
                $criteria = new SIMADbCriteria([
                    'Model' => PaycheckSuspension::model(),
                    'model_filter' => [
                        'paycheck' => [
                            'employee' => [
                                'ids'=> $employee->id
                            ]
                        ],
                        'suspension' => [
                            'ids' => $suspension->id
                        ]
                    ]
                ], true);
                
                if(empty($criteria->ids))
                {
                    $unused_suspensions[] = [
                        'emp' => $employee->DisplayHTML,
                        'susp' => $suspension->DisplayHTML
                    ];
                }
            }
        }
        
        return $unused_suspensions;
    }
    
    public function getClasses()
    {
        $classes = '';
        $last_modification_calculation_errors = $this->getLastModificationCalculationErrors();
        foreach($last_modification_calculation_errors as $last_modification_calculation_error)
        {
            $classes .= ' _'.$last_modification_calculation_error.' ';
        }
        
        return parent::getClasses() . $classes;
    }
    
    public function updateLastModificationTime()
    {
        $this->last_modification_time = SIMAHtml::UserToDbDate(date('Y-m-d H:i:s'), true);
    }
}
