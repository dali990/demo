<?php

class PaycheckDeduction extends SIMAActiveRecord
{
    public static $TYPE_HOT_MEAL = 'HOT_MEAL';
    public static $TYPE_OTHER = 'OTHER';
    
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'accounting.paycheck_deductions';
    }

    public function moduleName()
    {
        return 'paychecks';
    }
    
    public function __get($column)
    {
        switch ($column)
        {
            case 'DisplayName': 
                $result = $this->getAttributeDisplay('deduction_type');
                if(!empty($this->deduction_name))
                {
                    $result .= ' '.$this->deduction_name;
                }
                return $result;
            default: return parent::__get($column);
        }
    }
    
    public function rules()
    {
        return [
            ['paycheck_id, amount, deduction_type', 'required']
        ];
    }
    
    public function relations($child_relations = [])
    {
        return [
            'paycheck' => [self::BELONGS_TO, 'Paycheck', 'paycheck_id']
        ];
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters' : return [
                'paycheck' => 'relation',
                'amount' => 'numeric'
            ];
            case 'number_fields': return [
                'amount'
            ];
            case 'multiselect_options': return ['delete'];
            default: return parent::modelSettings($column);
        }
    }
}

