<?php

class UnifiedPaycheck extends SIMAActiveRecord
{
    static public $TYPE_PDF = 'PDF';
    static public $TYPE_TXT = 'TXT';

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'accounting.unified_paychecks';
    }

    public function moduleName()
    {
        return 'paychecks';
    }

    public function __get($column)
    {
        switch ($column)
        {
            case 'DisplayName': 
            case 'SearchName': return $this->display_name;
            default: return parent::__get($column);
        }
    }

    public function relations($child_relations = [])
    {
        return array(
            'bank_account' => array(self::BELONGS_TO, 'BankAccount', 'bank_account_id'),
            'bank' => array(self::BELONGS_TO, 'Bank', 'bank_id')
        );
    }

    public function rules()
    {
        return array(
            array('bank_account_id, bank_id', 'required', 
                'on' => array('insert', 'update')
            ),
            array(
                'bank_account_id', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => array('insert', 'update')
            ),
            array('bank_id','checkBankUnique')
        );
    }
    
    public function checkBankUnique()
    {
        if(isset($this->bank_id) && $this->active === true)
        {
            $new_bank_id = $this->bank->id;
            
            $criteria = new SIMADbCriteria([
                'Model' => 'UnifiedPaycheck',
                'model_filter' => [
                    'bank_account' => [
                        'bank' => [
                            'ids' => $new_bank_id
                        ]
                    ],
                    'scopes' => array(
                        'active'
                    ) 
                ]
            ]);
            
            if(!$this->getIsNewRecord())
            {
                $alias = $this->getTableAlias();
                
                $temp_criteria = new SIMADbCriteria();
                $temp_criteria->condition = "$alias.id!=:id";
                $temp_criteria->params = array(':id' => $this->id);
                
                $criteria->mergeWith($temp_criteria, true);
            }
            
            $count = UnifiedPaycheck::model()->count($criteria);
            
            if($count > 0)
            {
                $this->addError('bank_id', Yii::t(
                        'PaychecksModule.UnifiedPaycheck', 
                        'ExistsActiveForBank',
                        array('{bank}' => $this->bank)
                ));
            }
        }
    }
    
    public function scopes()
    {
        $alias = $this->getTableAlias();
        return array(
            'active' => array(
                'condition'=>"$alias.active=TRUE"
            ),
        );
    }
    
    public function beforeSave()
    {
        $this->display_name = $this->bank->DisplayName.'('.$this->bank_account->number.')';
        
        return parent::beforeSave();
    }

    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters' : return array_merge(parent::modelSettings('filters'),array(
                'bank_account' => 'relation',
                'bank' => 'relation',
                'display_name' => 'text',
            ));
            case 'options' : return array('form');
            case 'statuses':  return array(
                'active' => array(
                    'title' => 'Aktivno',
                    'checkAccessConfirm' => 'Confirm',
                    'checkAccessRevert' => 'Revert',
                ),
            );
            default: return parent::modelSettings($column);
        }
    }
}