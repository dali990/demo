<?php

class PaycheckBonus extends SIMAActiveRecord
{
    public $employee_id = null;
    public $paycheck_period_id = null;
    
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'accounting.paycheck_bonuses';
    }

    public function moduleName()
    {
        return 'paychecks';
    }
    
    public function __get($column)
    {
        switch ($column)
        {
            case 'DisplayName': $this->name;
            default: return parent::__get($column);
        }
    }
    
    public function rules()
    {
        return [
            ['employee_id, paycheck_period_id, name, value', 'required'],
            ['employee_id','checkPaycheckCalculated'],
            ['group_bonus_id, description', 'safe']
        ];
    }
    
    public function checkPaycheckCalculated()
    {
        if (!$this->hasErrors())
        {
            $paycheck = Paycheck::model()->findByModelFilter([
                'employee' => ['ids' => $this->employee_id],
                'paycheck_period' => ['ids' => $this->paycheck_period_id],
            ]);
            if (is_null($paycheck))
            {
                $this->addError('employee_id', 'Zaposlenom nije obracunata zarada u ovom periodu');
            }
        }
        
    }
    
    public function afterFind()
    {
        parent::afterFind();
        if (!empty($this->paycheck_id))
        {
            $paycheck = Paycheck::model()->findByPk($this->paycheck_id);
            $this->employee_id = $paycheck->employee_id;
            $this->paycheck_period_id = $paycheck->paycheck_period_id;
            $this->__old['employee_id'] = $paycheck->employee_id;
            $this->__old['paycheck_period_id'] = $paycheck->paycheck_period_id;
        }
    }
    
    public function beforeSave()
    {
        if (empty($this->paycheck_id))
        {
            $paycheck = Paycheck::model()->findByModelFilter([
                'employee' => ['ids' => $this->employee_id],
                'paycheck_period' => ['ids' => $this->paycheck_period_id],
            ]);
            if (is_null($paycheck))
            {
                throw new SIMAException('ne bi smelo da prodje, ima proveru u rules');
            }
            $this->paycheck_id = $paycheck->id;
        }
        return parent::beforeSave();
    }
    
    public function relations($child_relations = [])
    {
        return [
            'paycheck' => [self::BELONGS_TO, 'Paycheck', 'paycheck_id'],
            'employee' => [self::BELONGS_TO, 'Employee', 'employee_id'],
            'paycheck_period' => [self::BELONGS_TO, 'PaycheckPeriod', 'paycheck_period_id'],
            'group_bonus' => [self::BELONGS_TO, 'PaycheckGroupBonus', 'group_bonus_id']
        ];
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters' : return [
                'name' => 'text',
                'description' => 'text',
                'paycheck' => 'relation',
                'group_bonus' => 'relation',
                'value' => 'numeric'
            ];
            case 'textSearch' : return [
                'name' => 'text',
                'description' => 'text',
                'group_bonus' => 'relation'
            ];
            case 'number_fields': return [
                'value'
            ];
            case 'multiselect_options': return ['delete'];
            default: return parent::modelSettings($column);
        }
    }
}

