<?php

class PaycheckGroupBonus extends SIMAActiveRecord
{
    public static $GROUP_BONUS_TYPE_EQUAL_AMOUNT = 'EQUAL_AMOUNT';
    public static $GROUP_BONUS_TYPE_EQUAL_SUM = 'EQUAL_SUM';
    public static $GROUP_BONUS_TYPE_SALARY_POINTS_SUM = 'SALARY_POINTS_SUM';
    public static $GROUP_BONUS_TYPE_SALARY_POINTS = 'SALARY_POINTS';
    public static $GROUP_BONUS_TYPE_BONUS_POINTS_SUM = 'BONUS_POINTS_SUM';
    
    public static $GROUP_BONUS_TYPES = [
        'EQUAL_AMOUNT' => 'Vrednost podeljena svima',
        'EQUAL_SUM' => 'Suma podeljena svima jednako',
        'SALARY_POINTS_SUM' => 'Suma podeljena svima po poenima iz plate',
        'SALARY_POINTS' => 'vrednot poena',
        'BONUS_POINTS_SUM' => 'Suma podeljena svima po poenima iz bonus kategorija',
    ];
    
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'accounting.paycheck_group_bonuses';
    }

    public function moduleName()
    {
        return 'paychecks';
    }
    
    public function __get($column)
    {
        switch ($column)
        {
            case 'DisplayName': $this->name;
            default: return parent::__get($column);
        }
    }
    
    public function rules()
    {
        return [
            ['name, value, paycheck_period_id, group_bonus_type', 'required'],
            ['description', 'safe']
        ];
    }
    
    public function relations($child_relations = [])
    {
        return [
            'paycheck_period' => [self::BELONGS_TO, 'PaycheckPeriod', 'paycheck_period_id'],
        ];
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters' : return [
                'name' => 'text',
                'value' => 'numeric',
                'description' => 'text',
                'paycheck_period' => 'relation',
//                'group_bonus_type' => 'dropdown'
            ];
            case 'textSearch' : return [
                'name' => 'text',
                'description' => 'text'
            ];
            case 'number_fields': return [
                'value'
            ];
            default: return parent::modelSettings($column);
        }
    }
}

