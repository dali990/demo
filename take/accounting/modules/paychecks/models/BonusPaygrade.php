<?php

class BonusPaygrade extends SIMAActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'accounting.bonus_paygrades';
    }

    public function moduleName()
    {
        return 'paychecks';
    }
    
    public function __get($column)
    {
        switch ($column)
        {
            case 'DisplayName': $this->name;
            default: return parent::__get($column);
        }
    }
    
    public function rules()
    {
        return [
            ['name, points', 'required'],
//            ['description', 'safe']
        ];
    }
    
    public function scopes()
    {
        $alias = $this->getTableAlias();
        return [
            'byName' => [
                'order' => $alias.'.name'
            ]
        ];
    }
    
    public function relations($child_relations = [])
    {
        return [
            'employees' => [self::HAS_MANY, 'Employee', 'paycheck_bonus_paygrade_id'],
        ];
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters' : return [
                'name' => 'text',
//                'value' => 'numeric',
//                'description' => 'text',
//                'paycheck_period' => 'relation',
//                'group_bonus_type' => 'dropdown'
            ];
            case 'textSearch' : return [
                'name' => 'text',
//                'description' => 'text'
            ];
            case 'number_fields': return [
                'points'
            ];
            case 'default_order_scope': return 'byName';
            default: return parent::modelSettings($column);
        }
    }
}

