<?php

class PaycheckXML extends SIMAActiveRecord
{
    public $sum_ammount_neto;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'accounting.paycheck_xmls';
    }

    public function moduleName()
    {
        return 'paychecks';
    }

    public function __get($column)
    {
        switch ($column)
        {
//            case 'DisplayName': 
//            case 'SearchName': return $this->employee->DisplayName . '(' . $this->paycheck_period->DisplayName . ')';
            default: return parent::__get($column);
        }
    }
    
//    public function behaviors()
//    {
//        return array_merge([
//            'Calculations' => [
//                'class' => 'PaycheckCalculations'
//            ],
//        ],parent::behaviors());
//    }

    public function relations($child_relations = [])
    {
        return array(
//            'employee' => array(self::BELONGS_TO, 'Employee', 'employee_id'),
//            'paycheck_period' => array(self::BELONGS_TO, 'Paycheck', 'paycheck_id'),
            'paycheck' => array(self::BELONGS_TO, 'Paycheck', 'paycheck_id'),
            'ppppd' => array(self::BELONGS_TO, PPPPD::class, 'ppppd_id'),
        );
    }

    public function rules()
    {
        return array(
            array('paycheck_id', 
                'required', 
                'on' => array('insert', 'update')
            ),
            array('employee_svp, hot_meal_value, regres', 'safe'),
//            array('amount_neto, amount_bruto, amount_total', 'numerical', 'message' => 'Polje "{attribute}" mora biti broj!'),
            array('paycheck_id', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => array('insert', 'update')),
        );
    }
    
    
    
//    public function scopes()
//    {
//        $alias = $this->getTableAlias(FALSE, FALSE);
//        $person_table = Person::model()->tableName();
//        $paycheck_period_table = PaycheckPeriod::model()->tableName();
//        $uniq = SIMAHtml::uniqid();
//        
//        return array(
//            'with_generated_file' => array(
//                'condition' => $alias.'.file_id IS NOT NULL'
//            ),
//            'orderByEmpJMBGASC' => [
//                'select' => $alias.'.*, person'.$uniq.'."JMBG"',
//                'join' => 'LEFT JOIN '.$person_table.' person'.$uniq.' ON person'.$uniq.'.id='.$alias.'.employee_id',
//                'order' => 'person'.$uniq.'."JMBG" ASC'
//            ],
//            'orderByPeriodDesc' => [
//                'select' => $alias.'.*, pp'.$uniq.'.display_name',
//                'join' => 'LEFT JOIN '.$paycheck_period_table.' pp'.$uniq.' ON pp'.$uniq.'.id='.$alias.'.paycheck_period_id',
//                'order' => 'pp'.$uniq.'.display_name DESC'
//            ]
//        );
//    }
    
   

    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters' : return array_merge(parent::modelSettings('filters'), [
                'id' => 'dropdown',
                'paycheck' => 'relation',
                'svp' => 'text',
                'ppppd' => 'relation'
//                'employee' => 'relation',
            ]);
            case 'options' : return [];
            case 'number_fields': return [
//                'performance_percent', 
//                'amount_neto', 
//                'amount_bruto', 
//                'amount_total', 
            ];
            default: return parent::modelSettings($column);
        }
    }
    
}