<?php

class PaycheckSuspension extends SIMAActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'accounting.paycheck_suspensions';
    }

    public function moduleName()
    {
        return 'paychecks';
    }
    
    public function relations($child_relations = [])
    {
        return [
            'paycheck' => [self::BELONGS_TO, 'Paycheck', 'paycheck_id'],
            'suspension' => [self::BELONGS_TO, 'Suspension', 'suspension_id']
        ];
    }
    
    public function rules()
    {
        return [
            ['paycheck_id, suspension_id, amount', 'required'],
            ['call_for_number, call_for_number_modul', 'safe'],
            ['amount', 'checkAmount', 'on' => ['insert','update']]
        ];
    }
    
    public function checkAmount()
    {
        if(!empty($this->amount) && !empty($this->suspension) && $this->paycheck->confirmed===false 
                && $this->amount > $this->suspension->left_to_pay_amount)
        {
            $this->addError('amount', Yii::t('PaychecksModule.Suspension', 'AmountMoreThanSuspensionLeftToPay', [
//                '{error_messages}' => implode('</br>', $errors)
            ]));
        }
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'options': return ['form','delete'];
            case 'filters' : return array(
                'paycheck' => 'relation',
                'suspension' => 'relation',
                'amount' => 'numeric'
            );
            case 'number_fields': return [
                'amount'
            ];
            default: return parent::modelSettings($column);
        }
    }
}

