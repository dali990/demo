<?php

class PaycheckAdditional extends SIMAActiveRecord
{
    public static $TYPE_SICK_LEAVE_INJURY_AT_WORK = 'SICK_LEAVE_INJURY_AT_WORK';
    public static $TYPE_SICK_LEAVE_PREGNANCY_TILL_30 = 'SICK_LEAVE_PREGNANCY_TILL_30';
    public static $TYPE_SICK_LEAVE_PREGNANCY_OVER_30 = 'SICK_LEAVE_PREGNANCY_OVER_30';
    public static $TYPE_SICK_LEAVE_MATERNITY = 'SICK_LEAVE_MATERNITY';
    public static $TYPE_SICK_LEAVE_CHILD_CARE_TILL_3 = 'SICK_LEAVE_CHILD_CARE_TILL_3';
    public static $TYPE_SICK_LEAVE_TILL_30 = 'SICK_LEAVE_TILL_30';
    public static $TYPE_SICK_LEAVE_OVER_30 = 'SICK_LEAVE_OVER_30';
    public static $TYPE_ANNUAL_LEAVE = 'ANNUAL_LEAVE';
    public static $TYPE_STATE_AND_RELIGIOUS_HOLIDAY = 'STATE_AND_RELIGIOUS_HOLIDAY';
    public static $TYPE_WORK_ON_NON_WORKING_HOLIDAY = 'WORK_ON_NON_WORKING_HOLIDAY';
    public static $TYPE_PAID_LEAVE = 'PAID_LEAVE';
    public static $TYPE_STANDBY = 'STANDBY';
//    public static $TYPE_OVERTIME = 'OVERTIME';
    public static $TYPE_PAID_LEAVE_MILITARY_SERVICE = 'PAID_LEAVE_MILITARY_SERVICE';
        
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'accounting.paycheck_additionals';
    }

    public function moduleName()
    {
        return 'paychecks';
    }
    
    public function __get($column)
    {
        switch ($column) 
        {
            case 'DisplayName': return $this->type;
            default: return parent::__get($column);
        }
    }
    
    public function relations($child_relations = [])
    {
        return array(
            'paycheck' => array(self::BELONGS_TO, 'Paycheck', 'paycheck_id'),
        );
    }
    
    public function rules()
    {
        return array(
            array('hours, bruto', 'required'),
            array('paycheck_id, description, type, number_of_calendar_days, confirmed', 'safe'),
            array('hours, hours_percentage, bruto, svp', 'numerical'),
        );
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters' : return array_merge(parent::modelSettings('filters'),array(
                'paycheck' => 'relation',
                'type' => 'dropdown'
            ));
            case 'number_fields': return [
                'bruto', 
                'unreleased_amount',
                'pio_empl',
                'zdr_empl',
                'nez_empl',
                'tax_empl',
                'pio_comp',
                'zdr_comp',
                'nez_comp',
            ];
            case 'statuses':  return array(
                'confirmed' => array(
                    'title' => Yii::t('BaseModule.Common', 'Confirmed')
                ),
            );
            default: return parent::modelSettings($column);
        }
    }
    
    /**
     * 
     * @param type $bruto
     * @param type $tax_release
     * @param type $doprinosi_base - doprinosi moraju spolja da se izjednace sa brutom
     * @param type $round
     */
    public function setValues($bruto, $tax_release, $doprinosi_base, $round = false)
    {
        $this->bruto = $bruto;
        $this->tax_release = $tax_release;
        $this->doprinosi_base = $doprinosi_base;
        $tax_base = $this->bruto - $this->tax_release;
        $month = $this->paycheck->paycheck_period->month;
        
        $is_penzioner = $this->paycheck->employee->isRetiree();
        
        $pio_empl = null;
        $zdr_empl = null;
        $nez_empl = null;
        $tax_empl = null;
        $pio_comp = null;
        $zdr_comp = null;
        $nez_comp = null;
        if(PaychecksCodebookComponent::useNewCodebooks())
        {
            $codebook = $this->paycheck->paycheck_period->codebook;
            $pio_empl = $codebook->pio_empl;
            $zdr_empl = $codebook->zdr_empl;
            $nez_empl = $codebook->nez_empl;
            $tax_empl = $codebook->tax_empl;
            $pio_comp = $codebook->pio_comp;
            $zdr_comp = $codebook->zdr_comp;
            $nez_comp = $codebook->nez_comp;
        }
        else
        {
            $pio_empl = $month->param('accounting.pio_empl');
            $zdr_empl = $month->param('accounting.zdr_empl');
            $nez_empl = $month->param('accounting.nez_empl');
            $tax_empl = $month->param('accounting.tax_empl');
            $pio_comp = $month->param('accounting.pio_comp');
            $zdr_comp = $month->param('accounting.zdr_comp');
            $nez_comp = $month->param('accounting.nez_comp');
        }
        
        if ($this->type == self::$TYPE_SICK_LEAVE_TILL_30)
        {
            $this->pio_empl = round($this->doprinosi_base * $pio_empl / 100,2);
            $this->zdr_empl = $is_penzioner ? 0 : round($this->doprinosi_base * $zdr_empl / 100,2);
            $this->nez_empl = $is_penzioner ? 0 : round($this->doprinosi_base * $nez_empl / 100,2);
            $this->tax_empl = round($tax_base * $tax_empl / 100,2);
            $this->pio_comp = round($this->doprinosi_base * $pio_comp / 100,2);
            $this->zdr_comp = $is_penzioner ? 0 : round($this->doprinosi_base * $zdr_comp / 100,2);
            $this->nez_comp = $is_penzioner ? 0 : round($this->doprinosi_base * $nez_comp / 100,2);
        }
        else
        {
            $this->pio_empl = ($this->doprinosi_base * $pio_empl / 100); 
            $this->zdr_empl = $is_penzioner ? 0 : ($this->doprinosi_base * $zdr_empl / 100);
            $this->nez_empl = $is_penzioner ? 0 : ($this->doprinosi_base * $nez_empl / 100);
            $this->tax_empl = ($tax_base * $tax_empl / 100);
            $this->pio_comp = ($this->doprinosi_base * $pio_comp / 100);
            $this->zdr_comp = $is_penzioner ? 0 : ($this->doprinosi_base * $zdr_comp / 100);
            $this->nez_comp = $is_penzioner ? 0 : ($this->doprinosi_base * $nez_comp / 100);
        }
    }
    
    public function setValuesByNeto($neto_sum, $tax_release, $doprinosi_base)
    {
        $this->tax_release = $tax_release;
        $this->doprinosi_base = $doprinosi_base;
        $month = $this->paycheck->paycheck_period->month;
        
        $is_penzioner = $this->paycheck->employee->isRetiree();
        
        $this->pio_empl = ($this->doprinosi_base * $month->param('accounting.pio_empl') / 100); 
        $this->zdr_empl = $is_penzioner ? 0 : ($this->doprinosi_base * $month->param('accounting.zdr_empl') / 100);
        $this->nez_empl = $is_penzioner ? 0 : ($this->doprinosi_base * $month->param('accounting.nez_empl') / 100);
        $this->pio_comp = ($this->doprinosi_base * $month->param('accounting.pio_comp') / 100);
        $this->zdr_comp = $is_penzioner ? 0 : ($this->doprinosi_base * $month->param('accounting.zdr_comp') / 100);
        $this->nez_comp = $is_penzioner ? 0 : ($this->doprinosi_base * $month->param('accounting.nez_comp') / 100);
        
        /**
         * TAX = (B - TR)*TE
         * 
         * B - B*TE = N + B*PIO + B*ZDR + B*NEZ - TR*TE
         * 
         * B - B*TE - B*PIO - B*ZDR - B*NEZ = N - TR*TE
         * 
         * B * (1 - TE - PIO - ZDR - NEZ) = N - TR*TE
         * 
         * B = (N - TR*TE) / (1 - TE - PIO - ZDR - NEZ)
         * 
         */
        
        $first = ($neto_sum - ($month->param('accounting.tax_empl')*$tax_release)/100);
        $second = (1 
                - ($month->param('accounting.tax_empl')/100) 
                - ($month->param('accounting.pio_empl')/100) 
                - ($month->param('accounting.zdr_empl')/100) 
                - ($month->param('accounting.nez_empl')/100));

        $bruto = $first/$second;
                
        $this->bruto = $bruto;
        
        $tax_base = $this->bruto - $this->tax_release;
        
        $this->tax_empl = ($tax_base * $month->param('accounting.tax_empl') / 100);
    }
    
    public function beforeSave()
    {
        $this->hours_percentage = $this->getTypeData()['percentage'];
        return parent::beforeSave();
    }
    
    public function isSeparateFieldInXML()
    {
        $typeData = $this->getTypeData();
                
        return $typeData['separate_field_in_xml'];
    }
    
    public function isForPayout()
    {
        $typeData = $this->getTypeData();
        
        return $typeData['for_payout'];
    }
    
    public function getTypeData()
    {
        $result = null;
        
        foreach(Yii::app()->params['paycheck_additional_types'] as $additional)
        {
            if($this->type === $additional['value'])
            {
                $result = $additional;
                break;
            }
        }
        
        return $result;
    }
    
    public function getRecalculatedValuesWithoutPrevious()
    {
        $bruto = $this->bruto;
        
        $previous_paychecks = $this->paycheck->getPreviousPaychecks();
        foreach($previous_paychecks as $previous_paycheck)
        {
            foreach($previous_paycheck->additionals as $additional)
            {
                if($additional->type === $this->type)
                {
                    $bruto -= $additional->bruto;
                }
            }
        }
        
        $tax_base = $bruto - $this->tax_release;
        
        $tax_empl = round(($tax_base * $this->paycheck->tax_empl_perc / 100), 2);
        
        $pio_empl = $bruto * $this->paycheck->pio_empl_perc / 100;
        $pio_comp = $bruto * $this->paycheck->pio_comp_perc / 100;
        $pio = round(($pio_empl+$pio_comp), 2);
        
        $zdr_empl = $bruto * $this->paycheck->zdr_empl_perc / 100;
        $zdr_comp = $bruto * $this->paycheck->zdr_comp_perc / 100;
        $zdr = round(($zdr_empl+$zdr_comp), 2);
        
        $nez_empl = $bruto * $this->paycheck->nez_empl_perc / 100;
        $nez_comp = $bruto * $this->paycheck->nez_comp_perc / 100;
        $nez = round(($nez_empl+$nez_comp), 2);
        
        $result = [
            'bruto' => $bruto,
            'tax_empl' => $tax_empl,
            'tax_base' => $tax_base,
            'pio' => $pio,
            'zdr' => $zdr,
            'nez' => $nez
        ];
        
        return $result;
    }
}
