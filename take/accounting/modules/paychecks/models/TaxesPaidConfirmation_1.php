<?php

class TaxesPaidConfirmation extends SIMAActiveRecord
{
    public $delivery_type = null;
    
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'accounting.taxes_paid_confirmations';
    }
    
    public function moduleName()
    {
        return 'paychecks';
    }
    
    public function __get($column)
    {
        switch ($column)
        {
            case 'DisplayName':
                $return = '';
                if (isset($this->employee) && isset($this->year))
                {
                    $return = 'Potvrda o plaćenim porezima i doprinosima po odbitku za '.$this->employee->display_name.' za '.$this->year->year;
                }
                return $return;
            case 'SearchName': return $this->DisplayName;
            case 'path2': return 'files/file';
            default: return parent::__get($column);
        }
    }
    
    public function scopes()
    {
        $alias = $this->getTableAlias();
        return [
            'byID' => [
                'order' => $alias.'.id'
            ]
        ];
    }
    
    public function rules()
    {
        return array(
            array('year_id, employee_id', 'required'),
            array('year_id', 'checkEmployeeYear'),
            array('year_id, employee_id', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => array('insert', 'update')
            )
        );
    }    
    
    public function checkEmployeeYear()
    {
        if (
                intval($this->year_id) !== intval($this->__old['year_id']) || 
                intval($this->employee_id) !== intval($this->__old['employee_id'])
           )
            {
                $taxes_paid_confirmation = TaxesPaidConfirmation::model()->findByAttributes([
                    'year_id'=>$this->year_id,
                    'employee_id'=>$this->employee_id
                ]);
                if (!is_null($taxes_paid_confirmation))
                {
                    $year = isset($this->year)?$this->year->DisplayName:$this->year_id;
                    $display_name = isset($this->employee)?$this->employee->display_name:$this->employee_id;
                    $this->addError('year_id', 'Već postoji potvrda o plaćenim porezima i doprinosima po odbitku za '.$display_name.' za '.$year);
                }
            }
    }
    
    public function relations($child_relations = [])
    {        
    	return array(
            'file'=>array(self::BELONGS_TO, 'File', 'id'),
            'employee'=>array(self::BELONGS_TO, 'Employee', 'employee_id'),
            'year'=>array(self::BELONGS_TO, 'Year', 'year_id'),
            'items' => array(self::HAS_MANY, 'TaxesPaidConfirmationItem', 'taxes_paid_confirmation_id'),
            'items_in_employment' => array(self::HAS_MANY, 'TaxesPaidConfirmationItem', 'taxes_paid_confirmation_id', 'condition' => 'in_employment=true'),
            'items_out_employment' => array(self::HAS_MANY, 'TaxesPaidConfirmationItem', 'taxes_paid_confirmation_id', 'condition' => 'in_employment=false')            
    	);
    }    

    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'options': return ['form', 'delete', 'download'];
            case 'filters' : return array(
                'file' => 'relation',
                'employee' => 'relation',
                'year' => 'relation',
                'delivery_type' => array('dropdown','func'=>'delivery_type_filter_func')
            );
            case 'textSearch' : return array(
                'file' => 'relation',
                'employee' => 'relation',
                'year' => 'relation'                
            );            
            default : return parent::modelSettings($column);
        }
    }
    
    public function delivery_type_filter_func($condition) 
    {        
        if(!empty($this->delivery_type))
        {
            $alias = $this->getTableAlias(FALSE,FALSE);
            $employees_table_name = Employee::model()->tableName();            
            $uniq = SIMAHtml::uniqid();
            
            $temp_cond = new CDbCriteria();
            $temp_cond->join = "join $employees_table_name e$uniq on $alias.employee_id=e$uniq.id";
            $temp_cond->condition="e$uniq.document_send_type='$this->delivery_type'";
            $condition->mergeWith($temp_cond);
        }        
    }    
    
    public function beforeSave()
    {
        if (!isset($this->file) && empty($this->id))
        {            
            $taxes_paid_confirmation_document_type_id = Yii::app()->configManager->get('accounting.paychecks.taxes_paid_confirmation_document_type_id', false);
            $file = new File();
            $file->name = $this->DisplayName;
            $file->document_type_id = $taxes_paid_confirmation_document_type_id;
            
            $file_tag = FileTag::model()->findByAttributes([
                'model_id'=>$this->employee_id,
                'model_table'=>Employee::model()->tableName(),
            ]);
            if (!is_null($file_tag))
            {                
                $belong_ids = [];
                $id['id'] = $file_tag->id;
                $id['display_name'] = $file_tag->DisplayName;
                $id['class'] = '_non-editable';
                array_push($belong_ids, $id);
                $file_belongs = [
                    'ids'=>$belong_ids,
                    'default_item'=>[]                
                ];
                $file->file_belongs = CJSON::encode($file_belongs);
            }
            
            $file->save();
            $file->refresh();
            $this->id = $file->id;
            $this->file = $file;            
        }
        
        return parent::beforeSave();
    }
    
    public function generateDocument()
    {
        $template_id = Yii::app()->configManager->get('accounting.paychecks.taxes_paid_confirmation_template_id', false);
        if (empty($template_id))
        {
            throw new SIMAWarnException('Nije zadat konfiguracioni parametar za kreiranje pdf za potvrdu o plaćenim porezima. Kontaktirajte administratora.');
        }

        $templated_file = TemplatedFile::model()->with('templated_file_data')->findByPk($this->id);
        if ($templated_file === null)
        {
            $template_file_id = TemplateController::setTemplate($this->id, $template_id);
        }
        else
        {
            $template_file_id = $templated_file->id;
        }
        unset($templated_file);
        TemplateController::addVersion($template_file_id);
    }
    
    private function anulate_item(& $item)
    {
        if (isset($item))
        {
//            error_log(__METHOD__.' anulira');
            $item->gross_income = 0; //bruto prihod
            $item->tax_relief = 0; //poreske olaksice
            $item->tax_base = 0; //poreska osnovica
            $item->tax = 0; //porez 10%
            $item->recipient_social_security_contribs = 0; //doprinosi za socijalno na teret primaoca
            $item->payer_social_security_contribs = 0; //doprinosi za socijalno na teret isplatioca
            $item->in_employment = TRUE;
        }
//        $item->save();
    }
    
    public function generateItems()
    {
        $items = [];
        $employee = $this->employee;
        $year = Year::model()->findByPk($this->year_id);
//        $paychecks = $employee->paychecks;
        
        $criteria = new SIMADbCriteria([
            'Model' => Paycheck::model(),
            'model_filter' => [
                'paycheck_period' => [
                    'payment_date' => "1.1.$year. <> 31.12.$year.",
                ],
                'employee' => ['ids' => $employee->id]
            ],
            'order' => 't.id'
        ]);
        $paychecks = Paycheck::model()->findAll($criteria);
        
        //vadjenje iz zarada
        foreach ($paychecks as $pay)
        {
            if (empty($pay->employee_svp))
            {
                throw new SIMAWarnException("prazan SVP za zaposlenog ".$employee->DisplayName.' u mesecu '.
                        $pay->paycheck_period->DisplayName);
            }

            if (!isset($items[$pay->employee_svp]))
            {
//                    error_log(__METHOD__.' trazi '.$this->id.'  '.$pay->employee_svp);
                $items[$pay->employee_svp] = TaxesPaidConfirmationItem::model()->findByAttributes([
                    'taxes_paid_confirmation_id' => $this->id,
                    'svp' => $pay->employee_svp
                ]);
                $this->anulate_item($items[$pay->employee_svp]);
            }
            if (!isset($items[$pay->employee_svp]))
            {
//                error_log(__METHOD__.'nije nasao');
                $items[$pay->employee_svp] = new TaxesPaidConfirmationItem();
                $items[$pay->employee_svp]->taxes_paid_confirmation_id = $this->id;
                $items[$pay->employee_svp]->svp = $pay->employee_svp;
                $this->anulate_item($items[$pay->employee_svp]);
            }
            
//            $_period = $pay->paycheck_period->DisplayName.'-'.$pay->paycheck_period->ordering_number;
//            error_log(__METHOD__.' - '.$_period.' BRUTO: '.$pay->amount_bruto.' '.$pay->tax_release);
            if (empty($pay->amount_bruto))
            {
                Yii::app()->raiseNote("prazan bruto za zaposlenog ".$employee->DisplayName.' u mesecu '.
                        $pay->paycheck_period->DisplayName);
            }
            else
            {
                if ($pay->tax_empl == 0) //WARN:ovo bi trebalo da se izbaci, ovo je samo za stare zarade
                {
                    $_a = $items[$pay->employee_svp]->gross_income += $pay->amount_bruto; //bruto prihod
                    $_b = $items[$pay->employee_svp]->tax_relief += $pay->tax_release; //poreske olaksice
                    $_c = $items[$pay->employee_svp]->tax_base = $_a - $_b; //poreska osnovica
                    $items[$pay->employee_svp]->tax = $_c / 10; //porez 10%
                    $items[$pay->employee_svp]->recipient_social_security_contribs = $_a * 19.9 / 100; //doprinosi za socijalno na teret primaoca
                    $items[$pay->employee_svp]->payer_social_security_contribs = $_a * 17.9 / 100; //doprinosi za socijalno na teret isplatioca
                    $items[$pay->employee_svp]->in_employment = TRUE;
                    $items[$pay->employee_svp]->save();
                }
                else
                {
                    $items[$pay->employee_svp]->gross_income += $pay->amount_bruto; //bruto prihod
                    $items[$pay->employee_svp]->tax_relief += $pay->amount_bruto - $pay->tax_base; //poreske olaksice
                    $items[$pay->employee_svp]->tax_base += $pay->tax_base; //poreska osnovica
                    $items[$pay->employee_svp]->tax += $pay->tax_empl; //porez 10%
                    $items[$pay->employee_svp]->recipient_social_security_contribs += $pay->pio_empl + $pay->zdr_empl + $pay->nez_empl; //doprinosi za socijalno na teret primaoca
                    $items[$pay->employee_svp]->payer_social_security_contribs += $pay->pio_comp + $pay->zdr_comp + $pay->nez_comp; //doprinosi za socijalno na teret isplatioca
                    $items[$pay->employee_svp]->in_employment = TRUE;
                    $items[$pay->employee_svp]->save();
                }
//                    $_a = $items[$pay->employee_svp]->gross_income += $pay->amount_bruto; //bruto prihod
//                    $_b = $items[$pay->employee_svp]->tax_relief = 11242+11*11433; //poreske olaksice
//                    $_c = $items[$pay->employee_svp]->tax_base = $_a - $_b; //poreska osnovica
//                    $items[$pay->employee_svp]->tax = $_c / 10; //porez 10%
//                    $items[$pay->employee_svp]->recipient_social_security_contribs = $_a * 19.9 / 100; //doprinosi za socijalno na teret primaoca
//                    $items[$pay->employee_svp]->payer_social_security_contribs = $_a * 17.9 / 100; //doprinosi za socijalno na teret isplatioca
//                    $items[$pay->employee_svp]->in_employment = TRUE;
//                    $items[$pay->employee_svp]->save();
            }
        }
        
//        $employee = $this->employee;
//        $year = $this->year;
//        $in_employment = true;
//        $taxes_paid_confirmation_item = new TaxesPaidConfirmationItem();
//        $taxes_paid_confirmation_item->taxes_paid_confirmation_id = $this->id;
//        $taxes_paid_confirmation_item->svp = '';
//        $taxes_paid_confirmation_item->gross_income = 0; //bruto prihod
//        $taxes_paid_confirmation_item->tax_relief = 0; //poreske olaksice
//        $taxes_paid_confirmation_item->tax_base = 0; //poreska osnovica
//        $taxes_paid_confirmation_item->tax = 0; //porez
//        $taxes_paid_confirmation_item->recipient_social_security_contribs = 0; //doprinosi za socijalno na teret primaoca
//        $taxes_paid_confirmation_item->payer_social_security_contribs = 0; //doprinosi za socijalno na teret isplatioca
//        $taxes_paid_confirmation_item->in_employment = $in_employment;
//        $taxes_paid_confirmation_item->save();
    }
}
