<?php

class Paygrade extends SIMAActiveRecord
{


    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'accounting.paygrades';
    }

    public function moduleName()
    {
        return 'paychecks';
    }

    public function __get($column)
    {
        switch ($column)
        {
            case 'DisplayName': 
            case 'SearchName': return $this->code . ' - ' . $this->name .'('. $this->points.')';
            default: return parent::__get($column);
        }
    }

    public function relations($child_relations = [])
    {
        return array(
            'paychecks' => array(self::HAS_MANY, 'Paychecks', 'paycheck_period_id'),
            'work_positions' => array(self::HAS_MANY, 'WorkPosition', 'paygrade_id'),
        );
    }

    public function rules()
    {
        return array(
            array('code, name, points', 'required'),
            array('points', 'numerical'),
//            array('description', 'safe')
//            array('paycheck_period_id', 'default',
//                'value' => null,
//                'setOnEmpty' => true, 'on' => array('insert', 'update'))
        );
    }
    
    public function scopes()
    {
        $alias = $this->getTableAlias();
        return array(
            'byName'=>array(
                'order' => $alias.'.code'
            )
        );
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters' : return array_merge(parent::modelSettings($column),array(
                'name' => 'text',
                'code' => 'text',
                'work_positions' => 'relation'
            ));
            case 'textSearch' : return array(
                'name' => 'text',
                'code' => 'text',
            );
            default: return parent::modelSettings($column);
        }
    }

}