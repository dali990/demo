<?php

class Paycheck extends SIMAActiveRecord
{
    public $sum_ammount_neto;
    public $period_sum_neto;
    public $period_sum_bruto;
    public $period_sum_total;
    public $period_sum_deductions;
    public $period_sum_neto_without_deductions;
    
    public $sick_leave_injury_at_work;
    public $sick_leave_pregnancy_till_30;
    public $sick_leave_pregnancy_over_30;
    public $sick_leave_maternity;
    public $sick_leave_till_30;
    public $sick_leave_over_30;
    public $annual_leave;
    public $state_and_religious_holiday;
    public $work_on_non_working_holiday;
    public $paid_leave;
    public $standby;
    public $total_worked_hours;
//    public $overtime;
    public $employee_contributions;
    public $tax;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'accounting.paychecks';
    }

    public function moduleName()
    {
        return 'paychecks';
    }

    public function __get($column)
    {
        switch ($column)
        {
            case 'DisplayName': 
            case 'SearchName': return $this->employee_display_name . '(' . $this->paycheck_period->DisplayName . ')';
            case 'path2': return 'accounting/paychecks/paycheck';   
            default: return parent::__get($column);
        }
    }
    
    public function behaviors()
    {
        return array_merge([
            'Calculations' => [
                'class' => 'PaycheckCalculations'
            ],
        ],parent::behaviors());
    }

    public function relations($child_relations = [])
    {
        return array(
            'employee' => array(self::BELONGS_TO, 'Employee', 'employee_id'),
            'paycheck_period' => array(self::BELONGS_TO, 'PaycheckPeriod', 'paycheck_period_id'),
            'file' => array(self::BELONGS_TO, 'File', 'file_id'),
            'paycheck_xmls' => array(self::HAS_MANY, 'PaycheckXML', 'paycheck_id'),
            
            'bonuses' => array(self::HAS_MANY, 'PaycheckBonus', 'paycheck_id'),
            
            'suspensions' => array(self::HAS_MANY, 'PaycheckSuspension', 'paycheck_id'),
            'additionals' => array(self::HAS_MANY, 'PaycheckAdditional', 'paycheck_id'),
            'additionals_anual_leave' => array(self::HAS_MANY, 'PaycheckAdditional', 'paycheck_id', 
                'condition' => "type='".PaycheckAdditional::$TYPE_ANNUAL_LEAVE."'"),
            'additionals_state_and_religious_holiday' => array(self::HAS_MANY, 'PaycheckAdditional', 'paycheck_id', 
                'condition' => "type='".PaycheckAdditional::$TYPE_STATE_AND_RELIGIOUS_HOLIDAY."'"),
            'additionals_paid_leave' => array(self::HAS_MANY, 'PaycheckAdditional', 'paycheck_id', 
                'condition' => "type='".PaycheckAdditional::$TYPE_PAID_LEAVE."'"),
            'additionals_paid_leave_military_service' => array(self::HAS_MANY, 'PaycheckAdditional', 'paycheck_id', 
                'condition' => "type='".PaycheckAdditional::$TYPE_PAID_LEAVE_MILITARY_SERVICE."'"),
            'additionals_sick_leave_injury_at_work' => array(self::HAS_MANY, 'PaycheckAdditional', 'paycheck_id', 
                'condition' => "type='".PaycheckAdditional::$TYPE_SICK_LEAVE_INJURY_AT_WORK."'"),
            'additionals_sick_leave_pregnancy_till_30' => array(self::HAS_MANY, 'PaycheckAdditional', 'paycheck_id', 
                'condition' => "type='".PaycheckAdditional::$TYPE_SICK_LEAVE_PREGNANCY_TILL_30."'"),
            'additionals_sick_leave_pregnancy_over_30' => array(self::HAS_MANY, 'PaycheckAdditional', 'paycheck_id', 
                'condition' => "type='".PaycheckAdditional::$TYPE_SICK_LEAVE_PREGNANCY_OVER_30."'"),
            'additionals_sick_leave_maternity' => array(self::HAS_MANY, 'PaycheckAdditional', 'paycheck_id', 
                'condition' => "type='".PaycheckAdditional::$TYPE_SICK_LEAVE_MATERNITY."'"),
            'additionals_sick_leave_child_care_till_3' => array(self::HAS_MANY, 'PaycheckAdditional', 'paycheck_id', 
                'condition' => "type='".PaycheckAdditional::$TYPE_SICK_LEAVE_CHILD_CARE_TILL_3."'"),
            'additionals_sick_leave_till_30' => array(self::HAS_MANY, 'PaycheckAdditional', 'paycheck_id', 
                'condition' => "type='".PaycheckAdditional::$TYPE_SICK_LEAVE_TILL_30."'"),
            'additionals_sick_leave_over_30' => array(self::HAS_MANY, 'PaycheckAdditional', 'paycheck_id', 
                'condition' => "type='".PaycheckAdditional::$TYPE_SICK_LEAVE_OVER_30."'"),
            'additionals_sick_leave_100' => array(self::HAS_MANY, 'PaycheckAdditional', 'paycheck_id', 
                'condition' => "type IN ('".PaycheckAdditional::$TYPE_SICK_LEAVE_INJURY_AT_WORK."', "
                    . "'".PaycheckAdditional::$TYPE_SICK_LEAVE_PREGNANCY_TILL_30."', "
                    . "'".PaycheckAdditional::$TYPE_SICK_LEAVE_MATERNITY."')"),
            'payment' => array(self::BELONGS_TO, 'Payment', 'payment_id'),
            'delivered_hr_documents' => [self::HAS_MANY, 'DeliveredHRDocument', 'file_id'],
            'bank_account' => array(self::BELONGS_TO, 'BankAccount', 'bank_account_id'),
            'deductions' => [self::HAS_MANY, 'PaycheckDeduction', 'paycheck_id'],
            'deductions_not_hotmeal' => [self::HAS_MANY, 'PaycheckDeduction', 'paycheck_id',
                'condition' => "deduction_type != '".PaycheckDeduction::$TYPE_HOT_MEAL."'"],
            'deductions_hotmeal' => [self::HAS_MANY, 'PaycheckDeduction', 'paycheck_id',
                'condition' => "deduction_type='".PaycheckDeduction::$TYPE_HOT_MEAL."'"]
        );
    }

    public function rules()
    {
        return array(
            array('employee_id, paycheck_period_id, worked_hours, worked_hours_value, '
                    . 'past_exp_value, performance_percent', 
                'required', 'on' => array('insert', 'update')
            ),
            ['performance_percent', 'numerical', 'max' => 1000, 'min' => 1],
            array('employee_svp, hot_meal_value, regres, overtime_hours, hotmeal_fieldwork_value', 'safe'),
            array('amount_neto, amount_bruto, amount_total', 'numerical', 'message' => 'Polje "{attribute}" mora biti broj!'),
            array('employee_id, paycheck_period_id', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => array('insert', 'update')),
            ['employee_id', 'checkUniqueEmployeePeriod'],
            ['confirmed', 'checkConfirm']
        );
    }
    
    public function checkUniqueEmployeePeriod()
    {
        if ($this->isNewRecord)
        {
            $criteria = new SIMADbCriteria([
                'Model' => Paycheck::model(),
                'model_filter' => [
                    'employee' => [
                        'ids' => $this->employee_id
                    ],
                    'paycheck_period' => [
                        'ids' => $this->paycheck_period_id
                    ]
                ]
            ], true);
            
            if(!empty($criteria->ids))
            {
                $this->addError('employee_id', Yii::t('PaychecksModule.Paycheck', 'ExistsPaycheckForEmployeeInPeriod', [
                    '{emp}' => $this->employee,
                    '{paycheck_period}' => $this->paycheck_period
                ]));
            }
        }
    }
    
    public function checkConfirm()
    {
        if($this->confirmed === true && $this->is_valid === true)
        {
            $errors = $this->GetConfirmErrors();
            
            if(count($errors) > 0)
            {
                $this->addError('confirmed', Yii::t('PaychecksModule.Paycheck', 'CannotConfirm', [
                    '{error_messages}' => implode('</br>', $errors)
                ]));
            }
        }
    }
    
    public function GetConfirmErrors()
    {
        $errors = [];
        
        if(empty($this->employee))
        {
            $errors[] =  Yii::t('PaychecksModule.Paycheck', 'PaycheckDoesNotHaveEmployee');
        }
        else
        {
            $errors = EmployeeComponent::employeeInsufficientData($this->employee, false);
        }
        
        $additionals = $this->additionals;
        foreach($additionals as $additional)
        {
            if($additional->validate() === false)
            {
                $errors[] =  Yii::t('PaychecksModule.Paycheck', 'AdditionalNotValid', [
//                        '{emp}' => $this->employee,
//                        '{additional_type}' => $additional->getAttributeDisplay('type')
                ]);
            }
            
            if($additional->confirmed === false)
            {
                $errors[] = Yii::t('PaychecksModule.Paycheck', 'UncofirmedAdditional', [
                    '{emp}' => $this->employee
                ]);
            }
            
//                if($additional->isSeparateFieldInXML() && empty($additional->svp))
//                {
//                    $errors[] =  Yii::t('PaychecksModule.PaycheckPeriod', 'SvpMustBeSetForAdditional', [
//                        '{emp}' => $this->employee,
//                        '{additional_type}' => $additional->getAttributeDisplay('type')
//                    ]);
//                }
        }
        
        $paycheckSuspensions = $this->suspensions;
        foreach($paycheckSuspensions as $paycheckSuspension)
        {
            if($paycheckSuspension->validate() === false)
            {
                $paycheckSuspensionErrors = [];
                foreach($paycheckSuspension->getErrors() as $paycheckSuspensionError)
                {
                    $paycheckSuspensionErrors[] = implode('</br>', $paycheckSuspensionError);
                }
                $paycheckSuspensionErrorsParsed = implode('</br>', $paycheckSuspensionErrors);
                
                $errors[] =  Yii::t('PaychecksModule.Paycheck', 'SuspensionNotValid', [
                    '{emp}' => $this->employee,
                    '{errors}' => $paycheckSuspensionErrorsParsed
                ]);
            }
        }
        
        return $errors;
    }
    
    public function beforeDelete()
    {
        foreach($this->additionals as $additional)
        {
            $additional->delete();
        }
        foreach($this->paycheck_xmls as $_xmls)
        {
            $_xmls->delete();
        }
        
        return parent::beforeDelete();
    }
    
    public function scopes()
    {
        $alias = $this->getTableAlias(FALSE, FALSE);
        $person_table = Person::model()->tableName();
        $employee_table = Employee::model()->tableName();
        $paycheck_period_table = PaycheckPeriod::model()->tableName();
        $uniq = SIMAHtml::uniqid();
        
        return array(
            'with_generated_file' => array(
                'condition' => $alias.'.file_id IS NOT NULL'
            ),
            'orderByEmpJMBGASC' => [
                'select' => "$alias.*, person$uniq.\"JMBG\"",
                'join' => "LEFT JOIN $person_table person$uniq ON person$uniq.id=$alias.employee_id",
                'order' => 'person'.$uniq.'."JMBG" ASC'
            ],
            'orderByEmpOrderASC' => [
                'select' => "$alias.*, emp$uniq.order_in_company",
                'join' => "LEFT JOIN $employee_table emp$uniq ON emp$uniq.id=$alias.employee_id",
                'order' => "emp$uniq.order_in_company ASC"
            ],
            'orderByOfficial' => [
                'select' => "$alias.*, person$uniq.lastname, person$uniq.firstname",
                'join' => "LEFT JOIN $person_table person$uniq ON person$uniq.id=$alias.employee_id",
                'order' => "$alias.paygrade_code, person$uniq.lastname, person$uniq.firstname"
            ],
            'orderByPeriodDesc' => [
                'select' => $alias.'.*, pp'.$uniq.'.display_name',
                'join' => 'LEFT JOIN '.$paycheck_period_table.' pp'.$uniq.' ON pp'.$uniq.'.id='.$alias.'.paycheck_period_id',
                'order' => 'pp'.$uniq.'.display_name DESC'
            ]
        );
    }
    
    /// TODO: izbaciti, nigde se ne koristi
//    public function belongsToUnifiedPaycheckToPeriod($unifiedPaycheckToPaycheckPeriodId)
//    {        
//        $unifiedPaycheckToPaycheckPeriod = ModelController::GetModel('UnifiedPaycheckToPaycheckPeriod', $unifiedPaycheckToPaycheckPeriodId);
//                
//        $criteria = new SIMADbCriteria([
//            'Model' => Paycheck::model(),
//            'model_filter' => [
//                'paycheck_period' => array(
//                    'ids' => $unifiedPaycheckToPaycheckPeriod->paycheck_period->id
//                ),
//                'is_valid' => true,
//                'employee' => [
//                    'person' => [
//                        'partner' => [
//                            'main_bank_account_in_bank' => $unifiedPaycheckToPaycheckPeriod->unified_paycheck->bank->id
//                        ]
//                    ]
//                ]
//            ]
//        ]);
//                
//        $this->getDbCriteria()->mergeWith($criteria);
//                
//        return $this;
//    }

    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters' : return array_merge(parent::modelSettings('filters'),array(
                'paycheck_period' => 'relation',
                'employee' => 'relation',
                'paycheck_period_id' => 'dropdown',
                'is_valid' => 'boolean',
                'payment' => 'relation',
                'confirmed' => 'boolean',
                'bank_account' => 'relation',
                'file' => 'relation',
                'employee_payout_type' => 'text',
                'deductions' => 'relation'
            ));
            case 'options' :
                $result = array('open');
                if($this->paycheck_period->canEdit())
                {
                    $result = array('open', 'form', 'delete');
                }
                return $result;
            case 'statuses':  return array(
                'confirmed' => array(
                    'title' => 'Potvrdjeno',
                    'checkAccessConfirm' => 'checkAccessConfirmFunc',
                    'onConfirm' => 'onConfirm',
                    'onRevert' => 'onConfirm',
                ),
            );
            case 'number_fields': return [
                'performance_percent', 
                'amount_neto', 
                'amount_neto_for_payout', 
                'amount_bruto', 
                'amount_total', 
            ];
            case 'multiselect_options': return array('delete');
            default: return parent::modelSettings($column);
        }
    }
    
    public function onConfirm()
    {
        if($this->confirmed !== $this->__old['confirmed'])
        {
            $paycheckSuspensions = $this->suspensions;
            foreach($paycheckSuspensions as $paycheckSuspension)
            {
                $paycheckSuspension->suspension->save();
            }
        }
    }
    
    public function haveGeneratedPdfReport()
    {
        return isset($this->file_id);
    }
    
    public function checkAccessConfirmFunc()
    {
        $msgs = [];
        if ($this->employee->id !== Yii::app()->user->id)
        {
            $msgs[] = 'Potvrdu moze da izvrsi samo primalac zarade';
        }
        
        return (count($msgs)>0)?$msgs:true;
    }
    
    /**
     * vraca zarade u iz prethodnih isplata zarada u istom mesecu za istog zaposlenog
     */
    public function getPreviousPaychecks()
    {
        $result = [];
        
        $criteria = new SIMADbCriteria([
            'Model' => Paycheck::model(),
            'model_filter' => [
                'employee' => [
                    'ids' => $this->employee_id
                ],
                'paycheck_period' => [
                    'month' => [
                        'ids' => $this->paycheck_period->month->id
                    ],
                ]
            ]
        ]);
        
        $paychecks = Paycheck::model()->findAll($criteria);
        foreach($paychecks as $paycheck)
        {
            if($paycheck->paycheck_period->ordering_number < $this->paycheck_period->ordering_number)
            {
                $result[] = $paycheck;
            }
        }
        
        return $result;
    }
    
    public function getRecalculatedValuesByParsedAdditionals()
    {
        $bruto_in_separate_field_additionals = 0;
        $tax_release_in_separate_field_additionals = 0;
        $fully_active_hours = $this->worked_hours;
        $efective_worked_hours = $this->worked_hours;
        $bruto = $this->amount_bruto;
        $multiple_svps_in_period = false;

        foreach($this->additionals as $additional)
        {
            if(!$additional->isSeparateFieldInXML())
            {
                $efective_worked_hours += $additional->hours;
            }
            else
            {
                $multiple_svps_in_period = true;
                $recalculated_values = $additional->getRecalculatedValuesWithoutPrevious();
                $bruto -= $recalculated_values['bruto'];
                $bruto_in_separate_field_additionals += $additional->bruto;
                $tax_release_in_separate_field_additionals += $additional->tax_release;
            }
            $fully_active_hours += $additional->hours;
        }
        
        $tax_release = $this->tax_release-$tax_release_in_separate_field_additionals;

        $tax_base = $bruto - $tax_release;
        
        $porez = round(($tax_base * $this->tax_empl_perc / 100), 2);
        
        $pio_empl = $bruto * $this->pio_empl_perc / 100;
        $pio_comp = $bruto * $this->pio_comp_perc / 100;
        $pio = round(($pio_empl+$pio_comp), 2);
        
        $zdr_empl = $bruto * $this->zdr_empl_perc / 100;
        $zdr_comp = $bruto * $this->zdr_comp_perc / 100;
        $zdr = round(($zdr_empl+$zdr_comp), 2);
        
        $nez_empl = $bruto * $this->nez_empl_perc / 100;
        $nez_comp = $bruto * $this->nez_comp_perc / 100;
        $nez = round(($nez_empl+$nez_comp), 2);
        
        $result = [
            'efective_worked_hours' => $efective_worked_hours,
            'multiple_svps_in_period' => $multiple_svps_in_period,
            'bruto' => $bruto,
            'tax_base' => $tax_base,
            'porez' => $porez,
            'pio' => $pio,
            'zdr' => $zdr,
            'nez' => $nez,
            'bruto_in_separate_field_additionals' => $bruto_in_separate_field_additionals,
            'tax_release_in_separate_field_additionals' => $tax_release_in_separate_field_additionals,
            'fully_active_hours' => $fully_active_hours
        ];
        
        return $result;
    }

    public function getDeviations()
    {
        $result = [];
        
        if(!empty($this->is_retiree))
        {
            $result[] = Yii::t('PaychecksModule.Paycheck', 'IsRetiree');
        }
        
        if(!empty($this->personal_points))
        {
            $result[] = Yii::t('PaychecksModule.Paycheck', 'HavePersonalPoints', [
                '{personal_points}' => $this->personal_points
            ]);
        }
        
        if($this->performance_percent !== 100)
        {
            $result[] = Yii::t('PaychecksModule.Paycheck', 'PerformanceIsNot100%', [
                '{performance_percent}' => $this->performance_percent
            ]);
        }
        
        if($this->probationer_percent !== 100)
        {
            $result[] = Yii::t('PaychecksModule.Paycheck', 'IsProbationer', [
                '{probationer_percent}' => $this->probationer_percent
            ]);
        }
        
        $hours_on_annual_leave = 0;
        foreach($this->additionals_anual_leave as $additional)
        {
            $hours_on_annual_leave += $additional->hours;
        }
        if($hours_on_annual_leave>0)
        {
            $result[] = Yii::t('PaychecksModule.Paycheck', 'OnAnnualLeave', [
                '{days_on_annual_leave}' => $hours_on_annual_leave/8
            ]);
        }
        
        $hours_on_sick_leave_till_30 = 0;
        foreach($this->additionals_sick_leave_till_30 as $additional)
        {
            $hours_on_sick_leave_till_30 += $additional->hours;
        }
        if($hours_on_sick_leave_till_30>0)
        {
            $result[] = Yii::t('PaychecksModule.Paycheck', 'OnSickLeaveTill30', [
                '{days_on_annual_leave}' => $hours_on_sick_leave_till_30/8
            ]);
        }
        
        $hours_sick_leave_100 = 0;
        foreach($this->additionals_sick_leave_100 as $additional)
        {
            $hours_sick_leave_100 += $additional->hours;
        }
        if($hours_sick_leave_100>0)
        {
            $result[] = Yii::t('PaychecksModule.Paycheck', 'OnSickLeave100p', [
                '{days_on_sick_leave_100}' => $hours_sick_leave_100/8
            ]);
        }
        
        if($this->overtime_hours > 0)
        {
            $result[] = Yii::t('PaychecksModule.Paycheck', 'OverTimeHoursDeviation', [
                '{overtime_hours}' => $this->overtime_hours
            ]);
        }
        
        if($this->work_on_state_holiday_hours > 0)
        {
            $result[] = Yii::t('PaychecksModule.Paycheck', 'WorkOnStateHolidayHoursDeviation', [
                '{work_on_state_holiday_hours}' => $this->work_on_state_holiday_hours
            ]);
        }
        
        $hours_on_sick_leave_injury_at_work = 0;
        foreach($this->additionals_sick_leave_injury_at_work as $additional)
        {
            $hours_on_sick_leave_injury_at_work += $additional->hours;
        }
        if($hours_on_sick_leave_injury_at_work>0)
        {
            $result[] = Yii::t('PaychecksModule.Paycheck', 'OnSickLeaveInjuryAtWork', [
                '{days_on_sick_leave}' => $hours_on_sick_leave_injury_at_work/8
            ]);
        }
        
        $hours_on_sick_leave_pregnancy_till_30 = 0;
        foreach($this->additionals_sick_leave_pregnancy_till_30 as $additional)
        {
            $hours_on_sick_leave_pregnancy_till_30 += $additional->hours;
        }
        if($hours_on_sick_leave_pregnancy_till_30>0)
        {
            $result[] = Yii::t('PaychecksModule.Paycheck', 'OnSickLeave', [
                '{sick_leave}' => $this->getAttributeLabel('sick_leave_pregnancy_till_30'),
                '{days_on_sick_leave}' => $hours_on_sick_leave_pregnancy_till_30/8
            ]);
        }
        
        $hours_on_sick_leave_pregnancy_over_30 = 0;
        foreach($this->additionals_sick_leave_pregnancy_over_30 as $additional)
        {
            $hours_on_sick_leave_pregnancy_over_30 += $additional->hours;
        }
        if($hours_on_sick_leave_pregnancy_over_30>0)
        {
            $result[] = Yii::t('PaychecksModule.Paycheck', 'OnSickLeave', [
                '{sick_leave}' => $this->getAttributeLabel('sick_leave_pregnancy_over_30'),
                '{days_on_sick_leave}' => $hours_on_sick_leave_pregnancy_over_30/8
            ]);
        }
        
        $hours_on_sick_leave_maternity = 0;
        foreach($this->additionals_sick_leave_maternity as $additional)
        {
            $hours_on_sick_leave_maternity += $additional->hours;
        }
        if($hours_on_sick_leave_maternity>0)
        {
            $result[] = Yii::t('PaychecksModule.Paycheck', 'OnSickLeave', [
                '{sick_leave}' => $this->getAttributeLabel('sick_leave_maternity'),
                '{days_on_sick_leave}' => $hours_on_sick_leave_maternity/8
            ]);
        }
        
        $hours_on_sick_leave_child_care_till_3 = 0;
        foreach($this->additionals_sick_leave_child_care_till_3 as $additional)
        {
            $hours_on_sick_leave_child_care_till_3 += $additional->hours;
        }
        if($hours_on_sick_leave_child_care_till_3>0)
        {
            $result[] = Yii::t('PaychecksModule.Paycheck', 'OnSickLeave', [
                '{sick_leave}' => $this->getAttributeLabel('sick_leave_child_care_till_3'),
                '{days_on_sick_leave}' => $hours_on_sick_leave_child_care_till_3/8
            ]);
        }
        
        $hours_on_sick_leave_over_30 = 0;
        foreach($this->additionals_sick_leave_over_30 as $additional)
        {
            $hours_on_sick_leave_over_30 += $additional->hours;
        }
        if($hours_on_sick_leave_over_30>0)
        {
            $result[] = Yii::t('PaychecksModule.Paycheck', 'OnSickLeave', [
                '{sick_leave}' => $this->getAttributeLabel('sick_leave_over_30'),
                '{days_on_sick_leave}' => $hours_on_sick_leave_over_30/8
            ]);
        }
        
        return $result;
    }
    
    public function getPaychecksPeriodsSum($column_name)
    {
        $criteria = new SIMADbCriteria([
            'Model' => Paycheck::model(),
            'model_filter' => [
                'is_valid' => true,
                'paycheck_period' => [
                    'month' => [
                        'ids' => $this->paycheck_period->month->id
                    ]
                ],
                'employee' => [
                    'ids' => $this->employee_id
                ]
            ]
        ]);
        $criteria->select = 'sum('.$column_name.') as '.$column_name;
        $paychecks_sum_neto = Paycheck::model()->findAll($criteria);

        $result = 0;

        if (count($paychecks_sum_neto) > 0 )
        {
            $result = $paychecks_sum_neto[0][$column_name];
        }
        
        return $result;
    }
    
    public function getTotalWorkedHours()
    {
        $sick_leave_till_30_hours = 0;
        $additionals_sick_leave_till_30 = $this->additionals_sick_leave_till_30;
        foreach($additionals_sick_leave_till_30 as $additional)
        {
            $sick_leave_till_30_hours += $additional->hours;
        }
        
        $anual_leave_hours = 0;
        $additionals_anual_leave = $this->additionals_anual_leave;
        foreach($additionals_anual_leave as $additional)
        {
            $anual_leave_hours += $additional->hours;
        }
        
        $sick_leave_100_hours = 0;
        $additionals_sick_leave_100 = $this->additionals_sick_leave_100;
        foreach($additionals_sick_leave_100 as $sl)
        {
            $sick_leave_100_hours += $sl->hours;
        }
        
        $state_religious_hours = 0;
        $additionals_state_and_religious_holiday = $this->additionals_state_and_religious_holiday;
        foreach($additionals_state_and_religious_holiday as $additional)
        {
            $state_religious_hours += $additional->hours;
        }
        
        $paidleave_hours = 0;
        $additionals_paid_leave = $this->additionals_paid_leave;
        foreach($additionals_paid_leave as $additional)
        {
            $paidleave_hours += $additional->hours;
        }
        
        $leave_hours_sum = $sick_leave_till_30_hours + $anual_leave_hours + $sick_leave_100_hours + $state_religious_hours + $paidleave_hours;

        $result = $this->worked_hours + $leave_hours_sum;
        
        return $result;
    }
    
    public function getEmployeeContributions()
    {
        return $this->base_pio_empl + $this->base_zdr_empl + $this->base_nez_empl +
                $this->additionals_pio_empl + $this->additionals_zdr_empl + $this->additionals_nez_empl;
    }
    
    public function isPayoutByCache()
    {
        return $this->employee_payout_type === Employee::$PAYOUT_TYPE_CACHE;
    }
    
    public function isPayoutByBankAccount()
    {
        return $this->employee_payout_type === Employee::$PAYOUT_TYPE_BANK_ACCOUNT;
    }
}