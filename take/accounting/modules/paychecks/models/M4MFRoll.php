<?php

class M4MFRoll extends SIMAActiveRecord
{
    const MAX_M4_ROLL_EMPLOYEES = 20;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function moduleName()
    {
        return 'accounting';
    }
    
    public function tableName()
    {
        return 'accounting.m4mf_rolls';
    }
    
    public function __get($column) {
        switch ($column)
        {
//           case 'DisplayName': return Yii::t('PaychecksModule.M4MFRoll', 'PositionNumber').': '.$this->position_number . "(".Yii::t('PaychecksModule.M4MFRoll', 'RollNumber').": {$this->roll_number})";
           case 'DisplayName': return $this->roll_number.':'.$this->position_number;
           case 'SearchName':return $this->DisplayName;
           default: return parent::__get($column);
        }
    }
      
    public function rules()
    {
        return array(
            ['position_number, roll_number, year_id', 'required'],
            ['position_number', 'checkUniq'],
            ['year_id','default',
                'value'=>null,
                'setOnEmpty'=>true,'on'=>['insert','update']
            ]
        );
    }
    
    public function relations($child_relations = [])
    {
        return [
            'file' => array(self::BELONGS_TO, 'File', 'id'),
            'year' => array(self::BELONGS_TO, 'Year', 'year_id'),
            'm4employee_rows_cnt' => array(self::STAT, 'M4EmployeeRow', 'm4mf_roll_id', 'select' => 'count(*)'),
            'm4employee_rows' => array(self::HAS_MANY, 'M4EmployeeRow', 'm4mf_roll_id', 'scopes' => 'byName')
        ];
    }
    
    public function scopes()
    {
        $alias = $this->getTableAlias();
        return array(
            'byName' => array(
                'with'=>array('year'),
                'order' => "year.year DESC, $alias.roll_number ASC, $alias.position_number ASC"
            )
        );
    }
    
    public function checkUniq()
    {
        if (
                !$this->hasErrors() && 
                (
                    intval($this->__old['year_id']) !== intval($this->year_id) || 
                    intval($this->__old['roll_number']) !== intval($this->roll_number) || 
                    intval($this->__old['position_number']) !== intval($this->position_number)
                )
           )
        {
            $m4mf_roll = M4MFRoll::model()->findByAttributes([
                'year_id' => $this->year_id,
                'roll_number' => $this->roll_number,
                'position_number' => $this->position_number
            ]);
            if (!is_null($m4mf_roll))
            {
                $this->addError('position_number', Yii::t('PaychecksModule.M4MFRoll', 'UniqPositionNumberInRoll'));
            }
        }
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters': return [
                'file'=>'relation',
                'position_number'=>'integer',
                'roll_number'=>'integer',
                'year'=>'relation'
            ];
            case 'options':  return ['delete','form'];
            case 'textSearch': return [
                'position_number'=>'integer',
                'roll_number' => 'integer'
            ];
            case 'number_fields': return [
                'position_number',
                'roll_number'
            ];
            default: return parent::modelSettings($column);
        }
    }
    
    public function beforeSave() 
    {        
        if (empty($this->id) && empty($this->file))
        {
            $file = new File();
            $file->name = $this->DisplayName;
            $file->save();
            $this->id = $file->id;
            $this->file = $file;
        }
        
        return parent::beforeSave();
    }
 }
