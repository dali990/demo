<?php

class TaxesPaidConfirmationItem extends SIMAActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'accounting.taxes_paid_confirmation_items';
    }
    
    public function moduleName()
    {
        return 'paychecks';
    }
    
    public function rules()
    {
        return array(
            array('svp, taxes_paid_confirmation_id, gross_income, tax_relief, tax_base, tax, recipient_social_security_contribs, '
                . 'payer_social_security_contribs, in_employment', 'required'),
            array('svp', 'checkUniq'),
            array('taxes_paid_confirmation_id', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => array('insert', 'update')
            )
        );
    }    
    
    public function checkUniq()
    {
        if (
                intval($this->__old['taxes_paid_confirmation_id']) !== intval($this->taxes_paid_confirmation_id) ||
                $this->__old['svp'] !== $this->svp
           )
        {
            $taxes_paid_confirmation_item = TaxesPaidConfirmationItem::model()->findByAttributes([
                'taxes_paid_confirmation_id'=>$this->taxes_paid_confirmation_id,
                'svp'=>$this->svp
            ]);
            
            if (!is_null($taxes_paid_confirmation_item) && $taxes_paid_confirmation_item->id != $this->id)
            {
                $this->addError('svp', 'Već postoji svp '.$this->svp);
            }
        }
    }
    
    public function relations($child_relations = [])
    {        
    	return array(
            'taxes_paid_confirmation'=>array(self::BELONGS_TO, 'TaxesPaidConfirmation', 'taxes_paid_confirmation_id')            
    	);
    }    

    public function modelSettings($column)
    {
        switch ($column)
        {            
            case 'filters' : return array(
                'taxes_paid_confirmation' => 'relation'
            );          
            case 'number_fields': return [
                'gross_income', 
                'tax_relief', 
                'tax_base', 
                'tax', 
                'recipient_social_security_contribs', 
                'payer_social_security_contribs'
            ];
            default : return parent::modelSettings($column);
        }
    }    
}
