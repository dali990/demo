<?php

class PaycheckCodebook extends SIMAActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'accounting.paychecks_codebook';
    }

    public function moduleName()
    {
        return 'paychecks';
    }
    
    public function rules()
    {
        return [
            ['start_use, pio_empl, zdr_empl, nez_empl, tax_empl, tax_release, pio_comp, zdr_comp, nez_comp, min_contribution_base', 'required'],
            ['tax_release', 'numerical', 'max' => 999999.99],
            ['pio_empl, zdr_empl, nez_empl, tax_empl, pio_comp, zdr_comp, nez_comp', 'numerical', 'max'=>100],
            ['min_contribution_base, max_contribution_base', 'numerical', 'max' => 99999999.99],
            ['max_contribution_base', 'safe'],
            ['end_use', 'safe'],
            ['start_use', 'checkStartLessThanEnd', 'on' => ['insert', 'update']],
            ['start_use', 'checkIntervalIntersect', 'on' => ['insert', 'update']],
            ['start_use', 'checkStartOneDayAfterPreviousEnd', 'on' => ['insert', 'update']],
//            ['end_use', 'checkEndNullLast', 'on' => ['insert', 'update']],
            ['end_use', 'checkEndOneDayAfterNextStart', 'on' => ['insert', 'update']],
            ['start_use', 'checkDeleteOnlyFirstOrLast', 'on' => ['delete']],
            ['max_contribution_base', 'checkMaxLargerThanMinTaxBase', 'on' => ['insert', 'update']],
        ];
    }
    
    public function checkMaxLargerThanMinTaxBase()
    {
        if(!PaychecksCodebookComponent::useMaxTaxBase())
        {
            return;
        }
        if($this->min_contribution_base >= $this->max_contribution_base)
        {
            $this->addError('min_contribution_base', Yii::t('PaychecksModule.Codebook', 'MustBeLessThanMaxTaxBase'));
            $this->addError('max_contribution_base', Yii::t('PaychecksModule.Codebook', 'MMustBeLargerThanMinTaxBase'));
        }
    }
    
    public function checkStartLessThanEnd()
    {
        if($this->hasErrors('start_use') || $this->hasErrors('end_use'))
        {
            return;
        }
        if(!is_null($this->end_use))
        {
            $begin_day = Day::getByDate($this->start_use);
            $end_day = Day::getByDate($this->end_use);
            $compare_result = $begin_day->compare($end_day);

            if($compare_result>=0)
            {
                $this->addError('end_use', Yii::t('PaychecksModule.Codebook', 'StartUseDateMustBeLessThanEndUseDate'));
            }
        }
    }
    
    public function checkIntervalIntersect()
    {
        if($this->hasErrors('start_use') || $this->hasErrors('end_use'))
        {
            return;
        }
        
        $end_date = $this->end_use;
        if(is_null($end_date))
        {
            /**
             * ne moze danasnji datum, jer nije tacna situacija
             * npr imamo sifarnik u periodu 1.2.2019-1.3.2019, a danasnji datum je 1.4.2019
             * ako za pocetni datum stavimo 1.1.2019 izbacivace gresku, naci ce presek, a ne bi trebalo
             */
            $end_date = $this->start_use;
        }
        
        $between_daterange = $this->start_use.'<>'.$end_date;
        
        $existing_pcs = $this::model()->findAll([
            'model_filter' => [
                'between_daterange' => $between_daterange
            ]
        ]);
        $dates = '';
        $have_intersect = false;
        foreach($existing_pcs as $existing_pc)
        {
            if($existing_pc->id !== (int)$this->id)
            {
                $have_intersect = true;
                $dates = $existing_pc->start_use." : ".$existing_pc->end_use;
                break;
            }
        }
        if($have_intersect === true)
        {
            $this->addError('start_use', Yii::t('PaychecksModule.Codebook', 'ThereIsAlreadyPaycheckCodebookInThisInterval',['{dates}' => $dates]));
            $this->addError('end_use', Yii::t('PaychecksModule.Codebook', 'ThereIsAlreadyPaycheckCodebookInThisInterval',['{dates}' => $dates]));
        }
    }
    
    public function checkStartOneDayAfterPreviousEnd()
    {
        if($this->hasErrors('start_use') || $this->hasErrors('end_use'))
        {
            return;
        }
        
        $last_pc = $this::model()->find([
            'model_filter' => [
                'end_use' => [
                    '<',
                    $this->start_use
                ]               
            ],
            'order' => 'start_use DESC'
        ]);
        if(is_null($last_pc))
        {
            return;
        }
        
        $thisDate = new DateTime($this->start_use);
        $dayDate = new DateTime($last_pc->end_use);
        if($thisDate > $dayDate)
        {
            $day_diff = $diff = $thisDate->diff($dayDate)->format("%a");
            if((int)$day_diff !== 1)
            {
                $this->addError('start_use', Yii::t('PaychecksModule.Codebook', 'StartUseMustBeOneDayAfterLastEnd'));
            }
        }
    }
    
    public function checkEndOneDayAfterNextStart()
    {
        if($this->hasErrors('start_use') || $this->hasErrors('end_use'))
        {
            return;
        }
        
        if(is_null($this->end_use))
        {
            return;
        }
        
        $first_next_pc = $this::model()->find([
            'model_filter' => [
                'start_use' => [
                    '>=',
                    $this->end_use
                ],
                'scopes' => [
                    'order_start_use_ASC'
                ]
            ]
        ]);
        
        if(is_null($first_next_pc))
        {
            return;
        }
        
        $this_end_date = new DateTime($this->end_use);
        $next_start_date = new DateTime($first_next_pc->start_use);
        
        $day_diff = $this_end_date->diff($next_start_date)->format("%a");
        if((int)$day_diff !== 1)
        {
            $this->addError('end_use', Yii::t('PaychecksModule.Codebook', 'EndUseMustBeOneDayBeforeNextStart'));
        }
    }
    
    public function checkDeleteOnlyFirstOrLast() 
    {
        $paycheck_last = PaycheckCodebook::model()->find([
            "select" => "max(start_use) AS start_use"
        ]);
        
        $paycheck_first = PaycheckCodebook::model()->find([
            "select" => "min(start_use) AS start_use"
        ]);
        
        if  (
                $this->getAttribute('start_use') !== $paycheck_first->getAttribute('start_use') && 
                $this->getAttribute('start_use') !== $paycheck_last->getAttribute('start_use')
            )
        {
            $this->addError('start_use', Yii::t('PaychecksModule.PaycheckCodebook', 'YouCanDeleteOnlyFirstOrLast'));
        }
    }
    
//    public function checkEndNullLast()
//    {
////        $this->addError('start_use', 'checkEndNullLast - not implemented');
//    }
    
    
    public function scopes()
    {
        return [
            'with_end_use_not_null' => [
                'condition' => 'end_use IS NOT NULL'
            ],
            'order_start_use_DESC' => [
                'order' => 'start_use DESC'
            ],
            'order_start_use_ASC' => [
                'order' => 'start_use ASC'
            ]
        ];
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters': return [
                'start_use' => ['date_range'],
                'end_use' => ['date_range'],
                'between_daterange' => ['date_range', 'filter_function' => 'filter_between_daterange'],
            ];
            case 'number_fields': return ['min_contribution_base', 'max_contribution_base'];
            case 'options' : 
                $sync_type = Yii::app()->configManager->get('accounting.paychecks.paycheck_codebook_sync_type', false);
                if($sync_type === 'automatic_synchronization_with_notification' || $sync_type === 'automatic_synchronization_without_notification')
                {
                    return [];
                }
            default: return parent::modelSettings($column);
        }
    }
    
    public function filter_between_daterange(&$model_filter_value, $alias, $model_filter_key, $filter_type)
    {
        $dateRangeParamFormated = SIMAHtml::FormatDateRangeParam($model_filter_value);
        $model_filter_value = null;

        $uniq = SIMAHtml::uniqid();

        $temp_condition = new CDbCriteria();
//        $temp_condition->condition = "$alias.start_use <= :enddate$uniq"
//                . " AND base.min_dates($alias.end_use, :startdate$uniq)=:startdate$uniq";
        $temp_condition->condition = $this->filter_between_daterange_condition($alias, $uniq);
        $temp_condition->params = [
            ":startdate$uniq" => $dateRangeParamFormated['startDate'], 
            ":enddate$uniq" => $dateRangeParamFormated['endDate']
        ];
        return $temp_condition;
    }
    
    protected function filter_between_daterange_condition($alias, $uniq)
    {
        return "$alias.start_use <= :enddate$uniq"
                . " AND base.min_dates($alias.end_use, :startdate$uniq)=:startdate$uniq";
    }
    
    public function withoutUniqueFields($codebook_unique_fields)
    {
        $condition = "";
        if (!empty($codebook_unique_fields))
        {           
            $unique_fields_packages = "'".implode("','", $codebook_unique_fields)."'";
            
            $condition = "start_use not in ($unique_fields_packages)";
        }
        $criteria = new CDbCriteria();
        $criteria->condition = $condition;
        $this->getDbCriteria()->mergeWith($criteria);
        return $this;
    }
    
}
