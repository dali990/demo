<?php

class PaycheckPeriodToPPPPD extends SIMAActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'accounting.paycheck_periods_to_ppppds';
    }

    public function moduleName()
    {
        return 'paychecks';
    }
    
    public function rules()
    {
        return [
            ['paycheck_period_id, ppppd_id', 'required'],
        ];
    }
    
    public function relations($child_relations = [])
    {
        return [
            'file' => [self::BELONGS_TO, 'File', 'id'],
//            'items' => [self::HAS_MANY, 'PPPPDItem', 'ppppd_id'], /// MilosJ: ppppd ima items, a ne veza pp2ppppd
//            'paycheck_periods' => [self::BELONGS_TO, 'PaycheckPeriod', 'paycheck_period_id'],
            'paycheck_period' => [self::BELONGS_TO, 'PaycheckPeriod', 'paycheck_period_id'],
//            'ppppds' => [self::BELONGS_TO, 'PPPPD', 'ppppd_id']
            'ppppd' => [self::BELONGS_TO, 'PPPPD', 'ppppd_id']
        ];
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters' : return array_merge(parent::modelSettings('filters'),[
                'paycheck_period' => 'relation',
                'ppppd' => 'relation'
            ]);
            default: return parent::modelSettings($column);
        }
    }
}
