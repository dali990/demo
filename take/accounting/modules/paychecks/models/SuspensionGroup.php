<?php

class SuspensionGroup extends SIMAActiveRecord
{    
    
    public static $TYPE_ARBITRARLY = 'ARBITRARLY';
    public static $TYPE_AUTO_ALL = 'AUTO_ALL';
    public static $TYPE_AUTO_INSTALLMENT = 'AUTO_INSTALLMENT';
    
    //kolone koje mogu da se menjaju naknadno za svaku suspenziju - 
    //'<column_in_group:text>' => ['<columns_in_suspension:text>',<can_change_per_suspension:boolean>]
    public static $columns_to_sync_with_suspensions = [
        'name' => ['name', false], 
        'creditor_id' => ['creditor_id', false],
        'month_id' => ['begin_month_id', false],
        'amount' => ['total_amount', true],                             //TRUE
        'installment_amount' => ['installment_amount', true],           //TRUE
        'payment_type' => ['payment_type', true],                       //TRUE
        'creditor_bank_account_id' => ['creditor_bank_account_id', false],
        'name' => ['name', false],
    ];
    
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'accounting.suspension_groups';
    }

    public function moduleName()
    {
        return 'paychecks';
    }
    
    public function __get($column)
    {
        switch ($column)
        {
            case 'path2': return 'accounting/paychecks/suspension/groupIndex';
            case 'DisplayName': 
            case 'SearchName': 
                $result = $this->name;
                return $result;
            default: return parent::__get($column);
        }
    }
    
    public function rules()
    {
        return array(
            ['name, amount, month_id, creditor_id', 'required'],
            ['creditor_bank_account_id, payment_type, installment_amount', 'required'],
        );
    }
    
    public function relations($child_relations = [])
    {
        return [
            'creditor' => [self::BELONGS_TO, 'Partner', 'creditor_id'],
            'creditor_bank_account' => [self::BELONGS_TO, 'BankAccount', 'creditor_bank_account_id'],
            'month' => [self::BELONGS_TO, 'Month', 'month_id'],
            'suspensions' => [self::HAS_MANY, 'Suspension', 'suspension_group_id']
        ];
    }
    
    public function afterSave()
    {
        parent::afterSave();
        
        if(!$this->isNewRecord)
        {
            $params_to_set_to_suspensions = [];
         
            //izdvaja samo one grupe koje su zapravo promenjene
            foreach (self::$columns_to_sync_with_suspensions as $group_column => $suspenson_column_pair)
            {
                if($this->columnChanged($group_column))
                {
                    $params_to_set_to_suspensions[$group_column] = $suspenson_column_pair;
                }
            }
                 
            foreach($this->suspensions as $suspension)
            {
                $have_dirty_data = false;
                foreach($params_to_set_to_suspensions as $group_column => $suspenson_column_pair)
                {
                    $_sus_col = $suspenson_column_pair[0] ;
                    $_sus_change = $suspenson_column_pair[1] ;
                    if($suspension->$_sus_col !== $this->$group_column)
                    {
                        //vrednosti koje mogu da se rucno menjaju mozda ne treba da se diraju
                        if (
                            $_sus_change
                            && 
                            $suspension->$_sus_col != $this->__old[$group_column]
                            )
                        {
                            Yii::app()->raiseNote(Yii::t('PaychecksModule.Suspension', 'ValueNotChanged',[
                                '{employee}' => $suspension->employee->DisplayName,
                                '{column}' => $suspension->getAttributeLabel($_sus_col)
                            ]));
                        }
                        else 
                        {
                            $suspension->$_sus_col = $this->$group_column;
                            $have_dirty_data = true; 
                        }   
                    }
                }

                if($have_dirty_data)
                {
                    $suspension->save();
                }
            }
        }
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters': return array_merge(parent::modelSettings('filters'), array(
                'creditor' => 'relation',
                'creditor_id' => 'dropdown',
                'creditor_bank_account' => 'relation',
                'creditor_bank_account_id' => 'dropdown',
                'month' => 'relation',
                'month_id' => 'dropdown',
                'amount' => 'numeric'
            ));
            case 'options':
                return ['form', 'open', 'delete'];
            default: return parent::modelSettings($column);
        }
    }
    
    public function getTypeData()
    {
        return [
            SuspensionGroup::$TYPE_ARBITRARLY => Yii::t('PaychecksModule.Suspension', 'TypeArbitrarly'),
            SuspensionGroup::$TYPE_AUTO_ALL => Yii::t('PaychecksModule.Suspension', 'TypeAutoAll'),
            SuspensionGroup::$TYPE_AUTO_INSTALLMENT => Yii::t('PaychecksModule.Suspension', 'TypeAutoInstallment'),
        ];
    }
}
