<?php

class M4EmployeeRow extends SIMAActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function moduleName()
    {
        return 'accounting';
    }
    
    public function tableName()
    {
        return 'accounting.m4employee_rows';
    }
    
    public function __get($column) {
        switch ($column)
        {
           case 'DisplayName': return $this->employee_name;
           case 'SearchName':return $this->DisplayName;
           default: return parent::__get($column);
        }
    }
      
    public function rules()
    {
        return array(
            ['m4mf_roll_id, employee_id, employee_name, jmbg, work_exp_months, work_exp_days, bruto, pio_doprinos, zdrav_bruto, zdrav_pio_doprinos', 'required'],
            ['work_exp_months', 'numerical', 'integerOnly'=>true, 'min'=>0, 'max'=>12],
            ['work_exp_days', 'numerical', 'integerOnly'=>true, 'min'=>0, 'max'=>31],
            ['jmbg', 'length', 'min'=>13, 'max'=>13],
            ['bruto', 'checkNumericNumber', 'precision' => 10, 'scale' => 2],
            ['pio_doprinos', 'checkNumericNumber', 'precision' => 10, 'scale' => 2],
            ['zdrav_bruto', 'checkNumericNumber', 'precision' => 10, 'scale' => 2],
            ['zdrav_pio_doprinos', 'checkNumericNumber', 'precision' => 10, 'scale' => 2],
            ['employee_id', 'checkUniq'],
            ['m4mf_roll_id', 'checkMaxEmployeesNumber'],
            ['m4mf_roll_id, employee_id','default',
                'value'=>null,
                'setOnEmpty'=>true,'on'=>['insert','update']
            ]
        );
    }
    
    public function relations($child_relations = [])
    {
        return [
            'm4mf_roll' => array(self::BELONGS_TO, 'M4MFRoll', 'm4mf_roll_id'),
            'employee'=>array(self::BELONGS_TO, 'Employee', 'employee_id')
        ];
    }
    
    public function scopes()
    {
        $alias = $this->getTableAlias();
        return array(
            'byName' => array(
                'with'=>array('m4mf_roll', 'm4mf_roll.year'),
                'order' => "year.year DESC, m4mf_roll.roll_number ASC, m4mf_roll.position_number ASC, $alias.jmbg ASC"
            )
        );
    }
    
    public function checkNumericNumber($attribute, $params)
    {
        if (!is_null($this->$attribute) && SIMAMisc::isValidNumericNumber($this->$attribute, $params['precision'], $params['scale']) === false)
        {
            $this->addError($attribute, Yii::t('PaychecksModule.M4EmployeeRow', 'NotValidNumericNumber', [
                '{precision}' => $params['precision'],
                '{scale}' => $params['scale']
            ]));
        }
    }
    
    public function checkMaxEmployeesNumber()
    {
        if (
                !$this->hasErrors() && 
                (intval($this->__old['m4mf_roll_id']) !== intval($this->m4mf_roll_id))
           )
        {
            $m4mf_roll = M4MFRoll::model()->findByPkWithCheck($this->m4mf_roll_id);
            if ($m4mf_roll->m4employee_rows_cnt >= M4MFRoll::MAX_M4_ROLL_EMPLOYEES)
            {
                $this->addError('m4mf_roll_id', Yii::t('PaychecksModule.M4EmployeeRow', 'MaxEmployeesNumber', ['{max_number}' => M4MFRoll::MAX_M4_ROLL_EMPLOYEES]));
            }
        }
    }
    
    public function checkUniq()
    {
        if (
                !$this->hasErrors() && 
                (
                    intval($this->__old['m4mf_roll_id']) !== intval($this->m4mf_roll_id) || 
                    intval($this->__old['employee_id']) !== intval($this->employee_id)
                )
           )
        {
            $m4employee_row = M4EmployeeRow::model()->findByAttributes([
                'm4mf_roll_id' => $this->m4mf_roll_id,
                'employee_id' => $this->employee_id
            ]);
            if (!is_null($m4employee_row))
            {
                $this->addError('employee_id', Yii::t('PaychecksModule.M4EmployeeRow', 'UniqEmployee'));
            }
        }
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters': return [
                'm4mf_roll' => 'relation',
                'employee' => 'relation',
                'employee_name' => 'text',
                'jmbg' => 'text',
                'work_exp_months' => 'text',
                'work_exp_days' => 'text',
                'bruto' => 'numeric',
                'pio_doprinos' => 'numeric',
                'zdrav_bruto' => 'numeric',
                'zdrav_pio_doprinos' => 'numeric'
            ];
            case 'options':  return ['delete','form'];
            case 'textSearch': return [
                'employee_name'=>'text'
            ];
            case 'number_fields': return [
                'bruto', 'pio_doprinos', 'zdrav_bruto', 'zdrav_pio_doprinos'
            ];
            default: return parent::modelSettings($column);
        }
    }
 }
