<?php

class BonusPaygradeGUI extends SIMAActiveRecordGUI
{
    public function columnLabels()
    {
        return array(
            'points' => Yii::t('PaychecksModule.Paygrade','Points'),
            'employees' => EmployeeGUI::modelLabel(true)
        )+parent::columnLabels();
    }
    
    public function columnDisplays($column) 
    {
        $owner = $this->owner;
        switch ($column) {
            case 'employees':
                $return = [];
                foreach ($owner->employees as $employee) 
                {
                    $return[] = $employee->DisplayHTML;
                }
                
                return implode('<br />', $return);
            default: return parent::columnDisplays($column);
        }
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default:
                return array(
                    'columns' => array('name', 'points', 'employees')
                );
        }
    }
    
    public function modelLabel($plural = false)
    {
        return  Yii::t('PaychecksModule.Paygrade','Paygrade');
    }
    
    public function modelForms()
    {
        return array(
            'default'=>array(
                'type'=>'generic',
                'columns'=>array(
                    'name'=>'textField',
//                    'code'=>'textField',
                    'points'=>'numberField',
                    'employees' => [
                        'list',
                        'multiselect' => true,
                        'model' => 'Employee',
                        'relName' => 'employees', 
//                        'add_button_params' => [ 
////                            'formName' => 'from_partner'
//                        ],
//                        'default_item' => [
//                            'model' => 'Partner',
//                            'column' => 'main_phone_number_id'
//                        ] 
                    ],
                )
            )
        );
    }
}