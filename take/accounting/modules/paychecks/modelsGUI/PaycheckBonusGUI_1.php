<?php

class PaycheckBonusGUI extends SIMAActiveRecordGUI
{
    public function columnLabels()
    {
        return [
            'group_bonus_id' => Yii::t('PaychecksModule.PaycheckBonus','GroupBonus'),
            'group_bonus' => Yii::t('PaychecksModule.PaycheckBonus','GroupBonus'),
            'employee_id' => Yii::t('PaychecksModule.PaycheckBonus','Employee'),
            'employee' => Yii::t('PaychecksModule.PaycheckBonus','Employee'),
            'paycheck.employee' => Yii::t('PaychecksModule.PaycheckBonus','Employee')
        ] + parent::columnLabels();
    }
    
//    public function columnDisplays($column)
//    {
//        $owner = $this->owner;
//        switch ($column)
//        {
//            default: return parent::columnDisplays($column); 
//        }
//    }
    
    public function modelLabel($plural = false)
    {
        return Yii::t('PaychecksModule.PaycheckBonus',$plural?'Bonuses':'Bonus');
    }
    
    public function modelForms()
    {
        $_employee_id = ['searchField', 'relName'=>'employee'];
        if (!empty($this->owner->employee_id))
        {
            $_employee_id['disabled'] = 'disabled';
        }
        return [
            'default' => [
                'type'=>'generic',
                'columns' => [
                    'name' => 'textField',
//                    'paycheck_id' => ['searchField', 'relName'=>'paycheck', 'disabled'=>'disabled'],
//                    'paycheck_id' => ['searchField', 'relName'=>'paycheck'],
                    'employee_id' => $_employee_id,
//                    'paycheck_period_id' => ['searchField', 'relName'=>'paycheck_period'],
                    'paycheck_period_id' => 'hidden',
                    'value' => 'numberField',
                    'group_bonus_id' =>  ['relation', 'relName' => 'group_bonus'],
                    'description' => 'textArea'
                ]
            ]
        ];
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default: return [
                'columns' => [
                    'name',
                    'paycheck.employee',
                    'value' => ['min_width' => '100px'],
                    'group_bonus',
                    'description'
                ],
                'order_by' => ['value'],
                'sums' => [
                    'value'
                ],
            ];
        }
    }
}
