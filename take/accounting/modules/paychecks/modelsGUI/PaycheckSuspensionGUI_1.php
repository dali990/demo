<?php

class PaycheckSuspensionGUI extends SIMAActiveRecordGUI
{
    public function columnLabels()
    {
        return array(
            'suspension' => Yii::t('PaychecksModule.Suspension', 'Suspension'),
            'suspension_id' => Yii::t('PaychecksModule.Suspension', 'Suspension'),
            'call_for_number' => Yii::t('PaychecksModule.Suspension', 'CallForNumber'),
            'call_for_number_modul' => Yii::t('PaychecksModule.Suspension', 'CallForNumberModul')
        )+parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return Yii::t('PaychecksModule.Suspension',$plural?'PaycheckSuspensions':'PaycheckSuspension');
    }
    
    public function modelForms()
    {
        return array(
            'default' => array(
                'type'=>'generic',
                'columns'=>array(
                    'paycheck_id' => array('searchField', 'relName'=>'paycheck', 'disabled'=>'disabled'),
                    'suspension_id' => array('searchField', 'relName'=>'suspension'),
                    'amount' => 'numberField',
                    'call_for_number' => 'textField',
                    'call_for_number_modul' => 'textField'
                )
            )
        );
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            case 'inPeriod': return [
                'columns' => [
                    'paycheck.employee',
                    'suspension.creditor',
                    'suspension.name',
                    'suspension.creditor_bank_account',
                    'call_for_number',
                    'call_for_number_modul',
                    'amount'
                ],
                'sums' => [
                    'amount'
                ]
            ];
            default: return array(
                'columns'=>array(
                    'suspension.name',
                    'amount'
                ),
                'sums' => [
                    'amount'
                ]
            );
        }
    }
}

