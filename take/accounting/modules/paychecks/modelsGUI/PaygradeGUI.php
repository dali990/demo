<?php

class PaygradeGUI extends SIMAActiveRecordGUI
{
    public function columnLabels()
    {
        return array(
            'points' => Yii::t('PaychecksModule.Paygrade','Points'),
            'code' => Yii::t('PaychecksModule.Paygrade','Code'),
            'employees_num' => Yii::t('PaychecksModule.Paygrade','EmployeesNumber')
        )+parent::columnLabels();
    }
    
    public function columnDisplays($column) 
    {
        $owner = $this->owner;
        switch ($column) {
            case 'employees_num':
                $return = 0;
                foreach ($owner->work_positions as $work_position) 
                {
                    foreach ($work_position->active_work_contracts as $work_contract) 
                    {
                        $return++;
                    }
                }
                
                return $return;
            default: return parent::columnDisplays($column);
        }
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default:
                return array(
                    'columns' => array('code', 'name', 'points', 'employees_num'),
                    'order_by' => ['points'],
                );
        }
    }
    
    public function modelLabel($plural = false)
    {
        return  Yii::t('PaychecksModule.Paygrade','Paygrade');
    }
    
    public function modelForms()
    {
        return array(
            'default'=>array(
                'type'=>'generic',
                'columns'=>array(
                    'name'=>'textField',
                    'code'=>'textField',
                    'points'=>'textField',
                )
            )
        );
    }
}