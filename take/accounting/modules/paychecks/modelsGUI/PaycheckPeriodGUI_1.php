<?php

class PaycheckPeriodGUI extends SIMAActiveRecordGUI
{
    public function columnLabels()
    {
        return array(
            'ordering_number' => Yii::t('PaychecksModule.PaycheckPeriod','Ordering num.'),
            'month_id' => Yii::t('PaychecksModule.PaycheckPeriod','Month'),
            'month' => Yii::t('PaychecksModule.PaycheckPeriod','Month'),
            'year' => Yii::t('PaychecksModule.PaycheckPeriod','Year'),
//            'is_final' => Yii::t('PaychecksModule.PaycheckPeriod','Final paycheck'),
            'pp_type' => Yii::t('PaychecksModule.PaycheckPeriod','Paycheck type'),
            'hours_percentage' => Yii::t('PaychecksModule.PaycheckPeriod','Hours percent'),
            'tax_release_used' => Yii::t('PaychecksModule.PaycheckPeriod','Tax release used'),
            'payment_date' => Yii::t('PaychecksModule.PaycheckPeriod','Payment date'),
            'call_for_number_partner' => Yii::t('PaychecksModule.PaycheckPeriod','CallForNumberPU'),
            'old_period' => Yii::t('PaychecksModule.PaycheckPeriod','OldPeriod'),
            'creator_user_id' => Yii::t('PaychecksModule.PaycheckPeriod','CreatorUser'),
            'creator_user' => Yii::t('PaychecksModule.PaycheckPeriod','CreatorUser'),
            'payment' => Yii::t('PaychecksModule.PaycheckPeriod','Payment'),
            'payment_id' => Yii::t('PaychecksModule.PaycheckPeriod','Payment'),
            'month_point_value' => Yii::t('PaychecksModule.PaycheckPeriod','MonthPointValue'),
            'month_hours' => Yii::t('PaychecksModule.PaycheckPeriod','MonthHours'),
            'without_additionals' => Yii::t('PaychecksModule.PaycheckPeriod','WithoutAdditionals'),
            'employees_work_full_month' => Yii::t('PaychecksModule.PaycheckPeriod','EmployeesWorkFullMonth')
        )+parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return  Yii::t('PaychecksModule.PaycheckPeriod','PaycheckPeriod');
    }
    
    public function modelViews()
    {
        return array(
            'paychecks',
            'recalc_buttons',
            'paychecks_xml',
            'bank_pdf_export' => ['html_wrapper' => null],
            'all_paychecks_pdf_export' => ['html_wrapper' => null],
            'bank_txt_export' => ['html_wrapper' => null],
            'xml_eporezi' => ['html_wrapper' => null,'params_function'=>'paramsXMLePorezi'],
            'ppppds',
            'ppppd_xmls'
        );
    }
    
    public function paramsXMLePorezi()
    {
        $owner = $this->owner;
        
        $paychecksCriteria = new SIMADbCriteria([
            'Model' => Paycheck::model(),
            'model_filter' => [
                'paycheck_period' => [
                    'ids' => $owner->id
                ],
                'is_valid' => true,
                'display_scopes' => [
                    'orderByEmpJMBGASC'
                ]
            ]
        ]);
        $paychecks = Paycheck::model()->findAll($paychecksCriteria);

        $by_min_tax_base = 0;
        foreach($paychecks as $paycheck)
        {
            if($paycheck->is_by_min_tax_base === true)
            {
                $by_min_tax_base++;
            }
        }
        
        $dt = new DateTime($owner->payment_date);
        $paymentDate = $dt->format('Y-m-d');
        
        return [
            'model' => $owner,
            'paychecks' => $paychecks,
            'by_min_tax_base' => $owner->paychecks_by_min_tax_base,
            'paymentDate' => $paymentDate
        ];
    }
    
    public function modelForms()
    {
        $owner = $this->owner;
        
        $default_colums = [
            'creator_user_id' => array('searchField','relName'=>'creator_user', 'disabled'=>'disabled'),
            'ordering_number' => array('textField', 'placeholder'=>Yii::t('PaychecksModule.PaycheckPeriod','Leave empty for calculated next value')),
            'month_id' => [
                'searchField',
                'relName'=>'month', 
                'add_button'=> ['model'=>'AccountingMonth', 'filter' => []], 
                'filters' => [
                    'scopes' => [
                        'withBeingAccountingMonth'
                    ]
                ]
            ],
            'payment_date' => ['datetimeField'],
            'pp_type' => ['dropdown'],
            'hours_percentage' => [
                'numberField', 
                'dependent_on' => [
                    'pp_type' => [
                        'onValue'=>[
                             'trigger', 'func' => ['sima.paycheckPeriod.paycheckPeriodFormOnTypeChange']
                        ]
                    ]
                ]
            ],
            'tax_release_used' => array('numberField', 'placeholder'=>Yii::t('PaychecksModule.PaycheckPeriod','Leave empty for maximum possible')),
            'call_for_number_partner' => 'textField',
            'month_point_value' => ['numberField'],
            //MilosS(19.3.2018.) nema rucnog menjanja, mora da izracunava sistem
//            'month_hours' => ['numberField'],
            'without_additionals' => ['checkBox'],
            'employees_work_full_month' => ['checkBox']
        ];
        
        if($owner->confirmed === true)
        {
            $default_colums['ordering_number']['disabled'] = 'disabled';
            $default_colums['month_id']['disabled'] = 'disabled';
            $default_colums['payment_date']['disabled'] = 'disabled';
            $default_colums['pp_type']['disabled'] = 'disabled';
            $default_colums['hours_percentage']['disabled'] = 'disabled';
            $default_colums['tax_release_used']['disabled'] = 'disabled';
            $default_colums['month_point_value']['disabled'] = 'disabled';
//            $default_colums['month_hours']['disabled'] = 'disabled';
            $default_colums['without_additionals']['disabled'] = 'disabled';
            $default_colums['employees_work_full_month']['disabled'] = 'disabled';
        }
        
        return array(
            'default'=>array(
                'type'=>'generic',
                'columns' => $default_colums
            ),
            'just_payment' => [
                'type'=>'generic',
                'columns' => [
                    'payment_id' => ['relation','relName' => 'payment','add_button'=>true],
                ]
            ]
        );
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default: return array(
                'columns'=>array(
                    'ordering_number',
                    'month',
                    'year',
                    'pp_type',
                    'hours_percentage',
                    'tax_release_used',
                    'payment_date',
                    'call_for_number_partner',
                    'creator_user',
                    'month_point_value',
                    'month_hours',
                ),
                'order_by' => array(
                    'payment_date',
                    'month',
                    'year',
                    'ordering_number'
                )
            );
        }
    }
    
    public function columnDisplays($column)
    {
        $owner = $this->owner;
        switch ($column)
        {
            case 'pp_type':     return PaycheckPeriod::$pp_types[$owner->pp_type];
            case 'old_period':  return ($owner->old_period)?'Jeste':'Nije'; 
            case 'oz_header_title':
                $title = 'UNKNOWN';
                if ($owner->isFinal)
                {
                    $title = "KONAČNI OBRAČUN ZARADE";
                } else if ($owner->isBonus){
                    $title = "OBRAČUN BONUS ZARADE"; 
                } else {
                    $title = "DEO OBRAČUNA ZARADE - ".$owner->hours_percentage."%"; 
                }
                return $title;
            case 'oz_email_title':
                $oz_email_title = 'OZ - '.$owner->DisplayName.' - '.$owner->getAttributeDisplay('oz_header_title');
                return $oz_email_title;
            default:            return parent::columnDisplays($column); 
        }
        
    }
    
    public function droplist($key, $relName = '', $filter = null)
    {
        switch ($key) 
        {
            case 'pp_type': return PaycheckPeriod::$pp_types;
            default: return parent::droplist($key, $relName, $filter);
        }
    }
}
