<?php

class PaycheckDeductionGUI extends SIMAActiveRecordGUI
{
    public function columnLabels()
    {
        return array(
            'deduction_type' => Yii::t('PaychecksModule.Deduction', 'DeductionType')
        ) + parent::columnLabels();
    }
    
    public function columnDisplays($column)
    {
        $owner = $this->owner;
        switch ($column)
        {
            case 'deduction_type': return $owner->getTypeData()[$owner->deduction_type];
            default: return parent::columnDisplays($column); 
        }
    }
    
    public function modelForms()
    {
        return [
            'default' => [
                'type'=>'generic',
                'columns' => [
                    'paycheck_id' => ['searchField', 'relName'=>'paycheck', 'disabled'=>'disabled'],
                    'deduction_type' => 'dropdown',
                    'amount' => 'numberField'
                ]
            ]
        ];
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default: return [
                'columns' => [
                    'paycheck.employee',
                    'deduction_type',
                    'amount'
                ],
                'sums' => [
                    'amount'
                ],
            ];
        }
    }
    
    public function droplist($key, $relName='', $filter=null) {
        switch ($key) {
            case 'deduction_type': 
                $type_displays = $this->getTypeData();
                return $type_displays;
            default: return parent::droplist($key, $relName, $filter);
        }
    }
    
    public function getTypeData()
    {
        return [
            PaycheckDeduction::$TYPE_HOT_MEAL => Yii::t('PaychecksModule.Deduction', 'TypeHotMeal'),
            PaycheckDeduction::$TYPE_OTHER => Yii::t('PaychecksModule.Deduction', 'TypeOther')
        ];
    }
}
