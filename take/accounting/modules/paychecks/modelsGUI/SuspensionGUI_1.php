<?php

class SuspensionGUI extends SIMAActiveRecordGUI
{
    public function columnLabels()
    {
        return array(
            'creditor' => Yii::t('PaychecksModule.Suspension', 'Creditor'),
            'creditor_id' => Yii::t('PaychecksModule.Suspension', 'Creditor'),
            'begin_month' => Yii::t('PaychecksModule.Suspension', 'BeginMonth'),
            'begin_month_id' => Yii::t('PaychecksModule.Suspension', 'BeginMonth'),
            'total_amount' => Yii::t('PaychecksModule.Suspension', 'TotalAmount'),
            'installment_amount' => Yii::t('PaychecksModule.Suspension', 'InstallmentAmount'),
            'paid_amount' => Yii::t('PaychecksModule.Suspension', 'PaidAmount'),
            'left_to_pay_amount' => Yii::t('PaychecksModule.Suspension', 'LeftToPayAmount'),
            'payment_type' => Yii::t('PaychecksModule.Suspension', 'PaymentType'),
            'creditor_bank_account_id' => Yii::t('PaychecksModule.Suspension', 'CreditorBankAccount'),
            'creditor_bank_account' => Yii::t('PaychecksModule.Suspension', 'CreditorBankAccount'),
            'employee_id' => Yii::t('PaychecksModule.Suspension', 'Employee'),
            'employee' => Yii::t('PaychecksModule.Suspension', 'Employee'),
            'suspension_group_id' => Yii::t('PaychecksModule.Suspension', 'SuspensionGroup'),
            'suspension_group' => Yii::t('PaychecksModule.Suspension', 'SuspensionGroup'),
        ) + parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return Yii::t('PaychecksModule.Suspension', 'Suspension');
    }
    
    public function columnDisplays($column)
    {
        $owner = $this->owner;
        switch ($column)
        {
            case 'payment_type': return $owner->getTypeData()[$owner->payment_type];
            default: return parent::columnDisplays($column); 
        }
    }
    
    public function modelForms()
    {
        $owner = $this->owner;
        $name = ['textField'];
        $creditor_id = [
            'searchField',
            'relName'=>'creditor'
        ];
        $creditor_bank_account_id = [
            'searchField',
            'relName'=>'creditor_bank_account',
            'dependent_on'=>[
                'creditor_id'=>[
                    'onValue'=>[
                        'enable',
                        ['condition', 'model_filter' => 'partner.ids']
                    ],
                    'onEmpty'=>'disable'
                ]
            ]
        ];
        $begin_month_id = ['searchField','relName'=>'begin_month'];
        $suspension_group_id = ['searchField','relName'=>'suspension_group'];
        $payment_type = ['dropdown'];
        if(!empty($owner->suspension_group_id))
        {
            $name['disabled'] = 'disabled';
            $creditor_id['disabled'] = 'disabled';
            $creditor_bank_account_id['disabled'] = 'disabled';
            $begin_month_id['disabled'] = 'disabled';
            
            if(!empty($owner->suspension_group_id))
            {
                $suspension_group_id['disabled'] = 'disabled';
            }
        }
        
        
        $default_employee_id = ['searchField','relName'=>'employee'];
        if(!$owner->isNewRecord)
        {
            $default_employee_id['disabled'] = 'disabled';
        }
        
        return array(
            'for_employee'=>array(
                'type'=>'generic',
                'columns'=>array(
                    'name' => $name,
                    'employee_id' => ['searchField','relName'=>'employee', 'disabled'=>'disabled'],
                    'creditor_id' => $creditor_id,
                    'creditor_bank_account_id' => $creditor_bank_account_id,
                    'begin_month_id' => $begin_month_id,
                    'total_amount' => ['numberField'],
                    'installment_amount' => ['numberField'],
                    'paid_amount' => ['numberField'],
                    'payment_type' => $payment_type,
                    'suspension_group_id' => $suspension_group_id
                )
            ),
            'default'=>array(
                'type'=>'generic',
                'columns'=>array(
                    'name' => $name,
                    'employee_id' => $default_employee_id,
                    'creditor_id' => $creditor_id,
                    'creditor_bank_account_id' => $creditor_bank_account_id,
                    'begin_month_id' => $begin_month_id,
                    'total_amount' => ['numberField'],
                    'installment_amount' => ['numberField'],
                    'paid_amount' => ['numberField'],
                    'payment_type' => $payment_type,
                    'suspension_group_id' => $suspension_group_id
                )
            )
        );
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            case 'for_employee':
                return array(
                    'columns' => array(
                        'name',
                        'creditor',
                        'creditor_bank_account',
                        'begin_month',
                        'total_amount',
                        'installment_amount',
                        'paid_amount',
                        'left_to_pay_amount',
                        'payment_type',
                        'payment_type'
                    ),
                    'order_by' => [
                        'total_amount', 
                        'installment_amount',
                        'paid_amount',
                        'left_to_pay_amount'
                    ],
                    'sums' => [
                        'total_amount', 
                        'installment_amount',
                        'paid_amount',
                        'left_to_pay_amount'
                    ]
                );
            case 'inGroup': return [
                'columns' => [
                    'employee',
                    'total_amount' => ['max_width' => '100px'],
                    'left_to_pay_amount' => ['max_width' => '100px'],
                    'installment_amount' => ['max_width' => '100px'],
                    'payment_type'
                ],
                'sums' => [
                    'total_amount',
                    'installment_amount',
                    'paid_amount',
                    'left_to_pay_amount'
                ]
            ];
            default:
                return array(
                    'columns' => array(
                        'name',
                        'employee',
                        'creditor',
                        'creditor_bank_account',
                        'begin_month',
                        'total_amount',
                        'installment_amount',
                        'paid_amount',
                        'left_to_pay_amount',
                        'payment_type'
                    ),
                    'order_by' => [
                        'total_amount', 
                        'installment_amount',
                        'paid_amount',
                        'left_to_pay_amount'
                    ],
                    'sums' => [
                        'total_amount', 
                        'installment_amount',
                        'paid_amount',
                        'left_to_pay_amount'
                    ]
                );
        }
    }
    
    public function droplist($key, $relName='', $filter=null) {
        switch ($key) {
            case 'payment_type': 
                $type_displays = $this->owner->getTypeData();
                return $type_displays;
            default: return parent::droplist($key, $relName, $filter);
        }
    }
}