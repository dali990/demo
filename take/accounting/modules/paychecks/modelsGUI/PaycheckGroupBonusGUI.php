<?php

class PaycheckGroupBonusGUI extends SIMAActiveRecordGUI
{
    public function columnLabels()
    {
        return [
            'option_buttons' => Yii::t('BaseModule.Common','Options'),
            'group_bonus_type' => Yii::t('PaychecksModule.PaycheckBonus','GroupBonusType'),
        ] + parent::columnLabels();
    }
    
    public function columnDisplays($column)
    {
        $owner = $this->owner;
        switch ($column)
        {
            case 'group_bonus_type': 
                return PaycheckGroupBonus::$GROUP_BONUS_TYPES[$owner->group_bonus_type];
            case 'option_buttons':
                $buttons = Yii::app()->controller->widget('SIMAButton', [
                    'action'=>[
                        'title' => 'Preracunaj',
                        'onclick'=>['sima.paychecks.calcGroupBonus', $owner->id],
                    ]
                ], true);
                $buttons .= Yii::app()->controller->widget('SIMAButton', [
                    'action'=>[
                        'title' => 'Popuni sve',
                        'onclick'=>['sima.paychecks.fillAllGroupBonus', $owner->id],
                    ]
                ], true);
                $buttons .= Yii::app()->controller->widget('SIMAButton', [
                    'action'=>[
                        'title' => 'Popuni stalno zaposlenim bez penzionera',
                        'onclick'=>['sima.paychecks.fillGroupBonus', $owner->id],
                    ]
                ], true);
                $buttons .= Yii::app()->controller->widget('SIMAButton', [
                    'action'=>[
                        'title' => 'Dodaj proizvoljne zaposlene',
                        'onclick'=>['sima.paychecks.fillCustomGroupBonus', $owner->id],
                    ]
                ], true);
                return $buttons;
            default: return parent::columnDisplays($column); 
        }
    }
    
    public function modelLabel($plural = false)
    {
        return Yii::t('PaychecksModule.PaycheckBonus',$plural?'GroupBonuses':'GroupBonus');
    }
    
    public function modelForms()
    {
        return [
            'default' => [
                'type'=>'generic',
                'columns' => [
                    'name' => 'textField',
                    'group_bonus_type' => ['dropdown'],
                    'value' => 'numberField',
                    'description' => 'textArea',
                    'paycheck_period_id' => 'hidden'
                ]
            ]
        ];
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default: return [
                'columns' => [
                    'name',
                    'group_bonus_type',
                    'value' => ['min_width' => '100px'],
                    'option_buttons',
                    'description'
                ],
                'sums' => [
                    'value'
                ],
            ];
        }
    }
    
    public function droplist($key, $relName = '', $filter = null)
    {
        switch ($key)
        {
            case 'group_bonus_type': return PaycheckGroupBonus::$GROUP_BONUS_TYPES;
            default: return parent::droplist($key, $relName, $filter);
        }
    }
    
}
