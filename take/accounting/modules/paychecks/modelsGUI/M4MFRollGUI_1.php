<?php

class M4MFRollGUI extends SIMAActiveRecordGUI
{
    public function columnLabels()
    {
        return [
            'position_number' => Yii::t('PaychecksModule.M4MFRoll', 'PositionNumber'),
            'roll_number' => Yii::t('PaychecksModule.M4MFRoll', 'RollNumber'),
            'year_id' => Yii::t('PaychecksModule.M4MFRoll', 'Year'),
            'year' => Yii::t('PaychecksModule.M4MFRoll', 'Year')
        ]+parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return Yii::t('PaychecksModule.M4MFRoll', 'M4MFRoll');
    }
    
    public function columnDisplays($column)
    {
        $owner = $this->owner;
        switch ($column)
        {
            default: return parent::columnDisplays($column);
        }
    }
    
    public function modelForms()
    {
        $owner = $this->owner;
        return [
            'default' => [
                'type' => 'generic',
                'columns' => [
                    'roll_number'=>'numberField',
                    'position_number'=>'numberField',
                    'year_id'=>['relation', 'relName' => 'year']
                    
                ]
            ]
        ];
    }
    
    public function guiTableSettings($type)
    {   
        switch ($type)
        {
            case 'from_year': return [
                'columns' => array(
                    'roll_number', 'position_number', 
                )
            ];
            default:
                return array(
                    'columns' => array(
                        'roll_number', 'position_number', 'year'
                    )
            );
        }
    }
}