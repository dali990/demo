<?php

class PaycheckAdditionalGUI extends SIMAActiveRecordGUI
{
    public function columnLabels()
    {
        return array(
            'hours' => Yii::t('PaychecksModule.PaycheckAdditional', 'Hours'),
            'hours_percentage' => Yii::t('PaychecksModule.PaycheckAdditional', 'HoursPercentage'),
            'bruto' => Yii::t('PaychecksModule.PaycheckAdditional', 'Bruto'),
            'svp' => Yii::t('PaychecksModule.PaycheckAdditional', 'SVP'),
            'type' => Yii::t('PaychecksModule.PaycheckAdditional', 'Type'),
            'paycheck_id' => Yii::t('PaychecksModule.PaycheckAdditional', 'PaycheckId'),
            'tax_release' => Yii::t('PaychecksModule.PaycheckAdditional', 'TaxRelease'),
            'pio_empl' => Yii::t('PaychecksModule.PaycheckAdditional', 'PioEmpl'),
            'number_of_calendar_days' => Yii::t('PaychecksModule.PaycheckAdditional', 'NumberOfCalendarDays'),
        )+parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return Yii::t('PaychecksModule.PaycheckAdditional', $plural?'PaycheckAdditionals':'PaycheckAdditional');
    }
    
    public function columnDisplays($column)
    {
        $owner = $this->owner;
        switch ($column)
        {
            case 'hours_percentage': return $owner->$column.'%';
            case 'tax_release': return SIMAHtml::money_format($owner->tax_release);
            case 'type': return $owner->getTypeData()['display'];
            default: return parent::columnDisplays($column); 
        }
    }
    
    public function modelViews()
    {
        return [
            'xml_eporezi' => ['html_wrapper' => null,'params_function'=>'paramsXMLePorezi']
        ];
    }
    
    public function paramsXMLePorezi($model)
    {
        $owner = $this->owner;
        return [
            'model' => $owner
        ];
    }
    
    public function modelForms()
    {
        return array(
            'default' => array(
                'type'=>'generic',
                'columns'=>array(
                    'paycheck_id' => array('searchField', 'relName'=>'paycheck', 'disabled'=>'disabled'),
                    'type'=>array('dropdown'),
                    'number_of_calendar_days' => 'textField',
                    'hours' => 'textField',
                    'bruto' => 'textField',
                    'svp' => 'textField',
                    'description' => 'textField',
                )
            )
        );
    }
    
    public function droplist($key, $relName='', $filter=null) {
        switch ($key) {
            case 'type':
                $type_displays = array();
                foreach(Yii::app()->params['paycheck_additional_types'] as $additional)
                {
                    $type_displays[$additional['value']] = $additional['display'];
                }
                return $type_displays;
            default: 
                return parent::droplist($key, $relName, $filter);
        }
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default: return array(
                'columns'=>array(
                    'type',
                    'hours', 
                    'number_of_calendar_days',
                    'hours_percentage', 
                    'bruto', 
                    'svp',
                    'description',
                    'tax_release',
                    'pio_empl',
                    'zdr_empl',
                    'nez_empl',
                    'tax_empl',
                    'pio_comp',
                    'zdr_comp',
                    'nez_comp',
                )
            );
        }
    }
}