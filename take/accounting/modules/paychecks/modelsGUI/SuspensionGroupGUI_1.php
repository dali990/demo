<?php

class SuspensionGroupGUI extends SIMAActiveRecordGUI
{
    public function columnLabels()
    {
        return array(
            'creditor' => Yii::t('PaychecksModule.Suspension', 'Creditor'),
            'creditor_id' => Yii::t('PaychecksModule.Suspension', 'Creditor'),
            'month' => Yii::t('PaychecksModule.Suspension', 'BeginMonth'),
            'month_id' => Yii::t('PaychecksModule.Suspension', 'BeginMonth'),
            'amount' => Yii::t('PaychecksModule.Suspension', 'TotalAmount'),
            'creditor_bank_account_id' => Yii::t('PaychecksModule.Suspension', 'CreditorBankAccount'),
            'creditor_bank_account' => Yii::t('PaychecksModule.Suspension', 'CreditorBankAccount'),
            'installment_amount' => Yii::t('PaychecksModule.Suspension', 'InstallmentAmount'),
            'payment_type' => Yii::t('PaychecksModule.Suspension', 'PaymentType'),
        ) + parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return Yii::t('PaychecksModule.Suspension', 'SuspensionGroup');
    }
    
    public function modelViews()
    {
        return array_merge(parent::modelViews(), [
            'basicInfo',
            'options' => ['origin' => 'SuspensionGroup']
        ]);
    }
    
//    public function columnDisplays($column)
//    {
//        $owner = $this->owner;
//        switch ($column)
//        {
//            case 'payment_type': return $owner->getTypeData()[$owner->payment_type];
//            default: return parent::columnDisplays($column); 
//        }
//    }
//    
    public function modelForms()
    {
        return array(
            'default'=>array(
                'type'=>'generic',
                'columns'=>array(
                    'name' => 'textField',
                    'creditor_id' => ['searchField','relName'=>'creditor'],
                    'creditor_bank_account_id' => ['searchField','relName'=>'creditor_bank_account', 'dependent_on'=>[
                        'creditor_id'=>[
                            'onValue'=>[
                                'enable',
                                ['condition', 'model_filter' => 'partner.ids']
                            ],
                            'onEmpty'=>'disable'
                        ]
                    ]],
                    'month_id' => ['searchField','relName'=>'month'],
                    'amount' => ['numberField'],
                    'payment_type' => ['dropdown'],
                    'installment_amount' => [
                        'numberField',
                        'dependent_on'=>[
                            'payment_type'=>[
                                'onValue'=>[
                                    'show',
                                    'for_values' => [SuspensionGroup::$TYPE_AUTO_INSTALLMENT]
                                ],
                                'onValue'=>[
                                    'hide',
                                    'for_values' => [SuspensionGroup::$TYPE_ARBITRARLY, SuspensionGroup::$TYPE_AUTO_ALL]
                                ],
                            ]
                        ]
                    ]
                    
                )
            )
        );
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
//            case 'for_employee':
//                return array(
//                    'columns' => array(
//                        'name',
//                        'creditor',
//                        'creditor_bank_account',
//                        'begin_month',
//                        'total_amount',
//                        'installment_amount',
//                        'paid_amount',
//                        'left_to_pay_amount',
//                        'payment_type'
//                    ),
//                    'order_by' => [
//                        'total_amount', 
//                        'installment_amount',
//                        'paid_amount',
//                        'left_to_pay_amount'
//                    ],
//                    'sums' => [
//                        'total_amount', 
//                        'installment_amount',
//                        'paid_amount',
//                        'left_to_pay_amount'
//                    ]
//                );
            default:
                return array(
                    'columns' => array(
                        'name',
                        'amount',
                        'payment_type',
                        'installment_amount',
                        'month',
                        'creditor',
                        'creditor_bank_account'
                    ),
//                    'order_by' => [
//                        'total_amount', 
//                        'installment_amount',
//                        'paid_amount',
//                        'left_to_pay_amount'
//                    ],
//                    'sums' => [
//                        'total_amount', 
//                        'installment_amount',
//                        'paid_amount',
//                        'left_to_pay_amount'
//                    ]
                );
        }
    }
//    
    public function droplist($key, $relName='', $filter=null) 
    {
        switch ($key) 
        {
            case 'payment_type': 
                $type_displays = $this->owner->getTypeData();
                return $type_displays;
            default: return parent::droplist($key, $relName, $filter);
        }
    }
}