<?php

class UnifiedPaycheckGUI extends SIMAActiveRecordGUI
{
    public function columnLabels()
    {
        return array(
            'display_name' => Yii::t('PaychecksModule.UnifiedPaycheck','DisplayName'), 
            'bank_account' => Yii::t('PaychecksModule.UnifiedPaycheck','BankAccount'),
            'bank_account_id' => Yii::t('PaychecksModule.UnifiedPaycheck','BankAccount'),
            'bank_id' => Yii::t('PaychecksModule.UnifiedPaycheck','Bank'),
            'bank' => Yii::t('PaychecksModule.UnifiedPaycheck','Bank'),
        )+parent::columnLabels();
    }
    
    public function modelForms()
    {
        return array(
            'default'=>array(
                'type'=>'generic',
                'columns'=>array(
                    'bank_id' => array('searchField','relName'=>'bank'),
                    'bank_account_id' => array('searchField','relName'=>'bank_account')
                )
            )
        );
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default: return array(
                'columns'=>array(
                    'display_name', 
                    'bank',
                    'bank_account'
                )
            );
        }
    }
}