<?php

class M4EmployeeRowGUI extends SIMAActiveRecordGUI
{
    public function columnLabels()
    {
        return [
            'm4mf_roll_id' => Yii::t('PaychecksModule.M4EmployeeRow', 'M4MFRoll'),
            'm4mf_roll' => Yii::t('PaychecksModule.M4EmployeeRow', 'M4MFRoll'),
            'employee_id' => Yii::t('PaychecksModule.M4EmployeeRow', 'Employee'),
            'employee' => Yii::t('PaychecksModule.M4EmployeeRow', 'Employee'),
            'employee_name' => Yii::t('PaychecksModule.M4EmployeeRow', 'EmployeeName'),
            'jmbg' => Yii::t('PaychecksModule.M4EmployeeRow', 'JMBG'),
            'work_exp_months' => Yii::t('PaychecksModule.M4EmployeeRow', 'WorkExpMonths'),
            'work_exp_days' => Yii::t('PaychecksModule.M4EmployeeRow', 'WorkExpDays'),
            'bruto' => Yii::t('PaychecksModule.M4EmployeeRow', 'Bruto'),
            'pio_doprinos' => Yii::t('PaychecksModule.M4EmployeeRow', 'PioDoprinos'),
            'zdrav_bruto' => Yii::t('PaychecksModule.M4EmployeeRow', 'ZdravBruto'),
            'zdrav_pio_doprinos' => Yii::t('PaychecksModule.M4EmployeeRow', 'ZdravPioDoprinos')
        ]+parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return Yii::t('PaychecksModule.M4EmployeeRow', 'M4EmployeeRow');
    }
    
    public function columnDisplays($column)
    {
        $owner = $this->owner;
        switch ($column)
        {
            default: return parent::columnDisplays($column);
        }
    }
    
    public function modelForms()
    {
        $owner = $this->owner;
        return [
            'default' => [
                'type' => 'generic',
                'columns' => [
                    'm4mf_roll_id' => ['relation', 'relName' => 'm4mf_roll'],
                    'employee_id' => ['relation', 'relName' => 'employee'],
                    'employee_name' => 'textField',
                    'jmbg' => 'textField',
                    'work_exp_months' => 'textField',
                    'work_exp_days' => 'textField',
                    'bruto' => 'numberField',
                    'pio_doprinos' => 'numberField',
                    'zdrav_bruto' => 'numberField',
                    'zdrav_pio_doprinos' => 'numberField'
                    
                ]
            ]
        ];
    }
    
    public function guiTableSettings($type)
    {   
        switch ($type)
        {
            default:
                return array(
                'columns' => array(
                    'm4mf_roll', 'employee', 'employee_name', 'jmbg', 'work_exp_months', 'work_exp_days', 'bruto', 'pio_doprinos', 'zdrav_bruto', 'zdrav_pio_doprinos'
                )
            );
        }
    }
}