<?php

class TaxesPaidConfirmationItemGUI extends SIMAActiveRecordGUI
{
//    public function columnLabels()
//    {
//        return array(
//
//        ) + parent::columnLabels();
//    }

    public function columnDisplays($column)
    {
        $owner = $this->owner;
        switch ($column)
        {            
            case 'generate_document':
                    return $owner->generateDocumentTemplate();
                //ZAOKRUZIVANJE NA DINAR
            case 'gross_income':
            case 'tax_relief':
            case 'tax_base':
            case 'tax':
            case 'recipient_social_security_contribs':
            case 'payer_social_security_contribs':
                return SIMAHtml::number_format($owner->$column, 0);
            default : return parent::columnDisplays($column);
        }
    }
    
}