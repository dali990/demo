<?php

class PaycheckGUI extends SIMAActiveRecordGUI
{

    public function columnLabels()
    {
        $additionals_display = [];
        foreach(Yii::app()->params['paycheck_additional_types_formated'] as $key => $value)
        {
            $additionals_display[$key] = $value['display'];
        }
        
        return array(
            'employee' => Yii::t('PaychecksModule.Paycheck', 'Employee'),
            'employee_id' => Yii::t('PaychecksModule.Paycheck', 'Employee'),
            'paycheck_period' => Yii::t('PaychecksModule.Paycheck', 'PaycheckPeriod'),
            'paycheck_period_id' => Yii::t('PaychecksModule.Paycheck', 'PaycheckPeriod'),
            'amount_neto' => Yii::t('PaychecksModule.Paycheck', 'AmountNeto'),
            'amount_neto_for_payout' => Yii::t('PaychecksModule.Paycheck', 'AmountNetoForPayout'),
            'amount_bruto' => Yii::t('PaychecksModule.Paycheck', 'AmountBruto'),
            'amount_total' => Yii::t('PaychecksModule.Paycheck', 'AmountTotal'),
            'employee_svp' => Yii::t('PaychecksModule.Paycheck', 'EmployeeSvp'),
            'worked_hours' => Yii::t('PaychecksModule.Paycheck', 'WorkedHours'),
            'worked_hours_value' => Yii::t('PaychecksModule.Paycheck', 'WorkedHoursValue'),
            'past_exp_value' => Yii::t('PaychecksModule.Paycheck', 'PastExpValue'),
            'performance_percent' => Yii::t('PaychecksModule.Paycheck', 'PerformancePercent'),
            'download' => Yii::t('PaychecksModule.Paycheck', 'Download'),
            'paygrade_code' => Yii::t('PaychecksModule.Paycheck', 'PaygradeCode'),
            'tax_release' => Yii::t('PaychecksModule.Paycheck', 'TaxRelease'),
            'hot_meal_value' => Yii::t('PaychecksModule.Paycheck', 'HotMealValue'),
            'work_position_points' => Yii::t('PaychecksModule.Paycheck', 'WorkPositionPoints'),
            'personal_points' => Yii::t('PaychecksModule.Paycheck', 'PersonalPoints'),
            'deviations' => Yii::t('PaychecksModule.Paycheck', 'Deviations'),
            'base_tax_empl' => Yii::t('PaychecksModule.Paycheck', 'BaseTaxEmpl'),
            'base_pio' => Yii::t('PaychecksModule.Paycheck', 'BasePio'),
            'base_zdr' => Yii::t('PaychecksModule.Paycheck', 'BaseZdr'),
            'base_nez' => Yii::t('PaychecksModule.Paycheck', 'BaseNez'),
            'worked_hours_fix_for_average_hours' => Yii::t('PaychecksModule.Paycheck', 'WorkedHoursFixForAverageHours'),
            'xml_tax_empl' => Yii::t('PaychecksModule.Paycheck', 'XmlTaxEmpl'),
            'xml_pio' => Yii::t('PaychecksModule.Paycheck', 'XmlPio'),
            'xml_zdr' => Yii::t('PaychecksModule.Paycheck', 'XmlZdr'),
            'xml_nez' => Yii::t('PaychecksModule.Paycheck', 'XmlNez'),
            'year_experience' => Yii::t('PaychecksModule.Paycheck', 'YearExperience'),
            'pio_empl' => Yii::t('PaychecksModule.Paycheck', 'PioEmpl'),
            'zdr_empl' => Yii::t('PaychecksModule.Paycheck', 'ZdrEmpl'),
            'nez_empl' => Yii::t('PaychecksModule.Paycheck', 'NezEmpl'),
            'tax_empl' => Yii::t('PaychecksModule.Paycheck', 'TaxEmpl'),
            'pio_comp' => Yii::t('PaychecksModule.Paycheck', 'PioComp'),
            'zdr_comp' => Yii::t('PaychecksModule.Paycheck', 'ZdrComp'),
            'nez_comp' => Yii::t('PaychecksModule.Paycheck', 'NezComp'),
            'tax_base' => Yii::t('PaychecksModule.Paycheck', 'TaxBase'),
            'base_bruto' => Yii::t('PaychecksModule.Paycheck', 'BaseBruto'),
            'base_tax_base' => Yii::t('PaychecksModule.Paycheck', 'BaseTaxBase'),
            'work_sum' => Yii::t('PaychecksModule.Paycheck', 'WorkSum'),
            'xml_bruto' => Yii::t('PaychecksModule.Paycheck', 'XmlBruto'),
            'xml_tax_base' => Yii::t('PaychecksModule.Paycheck', 'XmlTaxBase'),
            'month_min_contribution_base' => Yii::t('PaychecksModule.Paycheck', 'MonthMinTaxBase'),
            'bank_account_display' => Yii::t('PaychecksModule.Paycheck', 'BankAccountDisplay'),
            'delivered_status' => Yii::t('PaychecksModule.Paycheck', 'DeliveredStatus'),
            'period_sum_neto' => Yii::t('PaychecksModule.Paycheck', 'PeriodSumNeto'),
            'period_sum_bruto' => Yii::t('PaychecksModule.Paycheck', 'PeriodSumBruto'),
            'period_sum_total' => Yii::t('PaychecksModule.Paycheck', 'PeriodSumTotal'),
            'total_worked_hours' => Yii::t('PaychecksModule.Paycheck', 'TotalWorkedHours'),
            'overtime' => Yii::t('PaychecksModule.Paycheck', 'Overtime'),
            'employee_contributions' => Yii::t('PaychecksModule.Paycheck', 'EmployeeContributions'),
            'tax' => Yii::t('PaychecksModule.Paycheck', 'Tax'),
            'send_documents' => Yii::t('PaychecksModule.Paycheck', 'SendDocumentsAgain'),
            'sick_leave_injury_at_work' => Yii::t('PaychecksModule.PaycheckAdditional', 'SICK_LEAVE_INJURY_AT_WORK'),
            'sick_leave_pregnancy_till_30' => Yii::t('PaychecksModule.PaycheckAdditional', 'SICK_LEAVE_PREGNANCY_TILL_30'),
            'sick_leave_pregnancy_over_30' => Yii::t('PaychecksModule.PaycheckAdditional', 'SICK_LEAVE_PREGNANCY_OVER_30'),
            'sick_leave_maternity' => Yii::t('PaychecksModule.PaycheckAdditional', 'SICK_LEAVE_MATERNITY'),
            'sick_leave_till_30' => Yii::t('PaychecksModule.PaycheckAdditional', 'SICK_LEAVE_TILL_30'),
            'sick_leave_over_30' => Yii::t('PaychecksModule.PaycheckAdditional', 'SICK_LEAVE_OVER_30'),
            'sick_leave_child_care_till_3' => Yii::t('PaychecksModule.PaycheckAdditional', 'SICK_LEAVE_CHILD_CARE_TILL_3'),
            'annual_leave' => Yii::t('PaychecksModule.PaycheckAdditional', 'ANNUAL_LEAVE'),
            'state_and_religious_holiday' => Yii::t('PaychecksModule.PaycheckAdditional', 'STATE_AND_RELIGIOUS_HOLIDAY'),
            'work_on_non_working_holiday' => Yii::t('PaychecksModule.PaycheckAdditional', 'WORK_ON_NON_WORKING_HOLIDAY'),
            'paid_leave' => Yii::t('PaychecksModule.PaycheckAdditional', 'PAID_LEAVE'),
            'standby' => Yii::t('PaychecksModule.PaycheckAdditional', 'STANDBY'),
            'overtime_hours' => Yii::t('PaychecksModule.Paycheck', 'OvertimeHours'),
//            'hotmeal_fieldwork_value' => Yii::t('PaychecksModule.Paycheck', 'HotmealFieldwork'),
            'period_sum_deductions' => Yii::t('PaychecksModule.Paycheck', 'PeriodSumDeductions'),
            'period_sum_neto_without_deductions' => Yii::t('PaychecksModule.Paycheck', 'PeriodSumNetoWithoutDeductions'),
            'suspensions_sum' => Yii::t('PaychecksModule.Paycheck', 'SuspensionsSum'),
            'is_retiree' => Yii::t('PaychecksModule.Paycheck', 'IsRetiree'),
            'additionals_nez_empl' => Yii::t('PaychecksModule.Paycheck', 'AdditionalsNezEmpl'),
            'old_work_exp' => Yii::t('LegalModule.Employee','OldWorkExp'),
            'work_contracts_work_exp' => Yii::t('LegalModule.Employee','WorkContractsWorkExp'),
        )+$additionals_display+parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return Yii::t('PaychecksModule.Paycheck',$plural?'Paychecks':'Paycheck');
    }
    
    public function columnDisplays($column)
    {
        $owner = $this->owner;
        switch ($column)
        {
            case 'amount_neto': case 'amount_bruto': case 'amount_total': 
                return SIMAHtml::money_format($owner->$column);
            case 'download':
                return ($owner->confirmed === true && isset($owner->file) && $owner->file->last_version->canDownload()) ? SIMAHtmlButtons::fileOpen($owner->file) : '';
            case 'employee_id':
                return $owner->employee_id;
            case 'previos_12_months_hours':
                $getPrevious12MonthsValues = PaychecksEmployee::GetEmployeesPrevious12MonthsValuesForPeriod($owner->employee, $owner->paycheck_period);
                
                $result = $getPrevious12MonthsValues['hours'];
                
                return number_format($result, 0, '.', ' ');
            case 'previos_12_months_value':
                $getPrevious12MonthsValues = PaychecksEmployee::GetEmployeesPrevious12MonthsValuesForPeriod($owner->employee, $owner->paycheck_period);
                
                $result = $getPrevious12MonthsValues['value'];
                                
                return SIMAHtml::money_format($result);
            case 'work_position_points':
                $_now = '';
                if (isset($owner->employee->last_work_contract->work_position->paygrade->points) 
                    && $owner->employee->last_work_contract->work_position->paygrade->points != $owner->$column)
                {
                    $_now = '('.$owner->employee->last_work_contract->work_position->paygrade->points.')';
                }
                return parent::columnDisplays($column).$_now;
            case 'personal_points':
                $_now = '';
                if (isset($owner->employee->last_work_contract->work_position->paygrade->points) 
                    && $owner->employee->personal_points != $owner->$column)
                {
                    $_now = '('.$owner->employee->personal_points.')';
                }
                return parent::columnDisplays($column).$_now;
            case 'paygrade_code': 
                $_now = '';
                if (isset($owner->employee->last_work_contract->work_position->paygrade->paygrade_code) 
                    && $owner->employee->last_work_contract->work_position->paygrade->paygrade_code != $owner->$column)
                {
                    $_now = '('.$owner->employee->last_work_contract->work_position->paygrade->points.')';
                }
                return parent::columnDisplays($column).$_now;
            case 'deviations':
                $result = Yii::t('BaseModule.Common', 'DoNotHave');
                
                $deviations = $owner->getDeviations();
                if(count($deviations) > 0)
                {
                    $result = implode('</br>', $deviations);
                }
                
                return $result;
            case 'send_documents':
                $return = '';
                if(!isset($owner->file))
                {
                    $return = Yii::t('PaychecksModule.Paycheck', 'OZFileNotCreated');
                }
                else
                {
                    if ($owner->employee->document_send_type === Employee::$E_MAIL)
                    {
                        if(SIMAMisc::isVueComponentEnabled())
                        {
                            $return .= Yii::app()->controller->widget(SIMAButtonVue::class,[
                                'title'=>'Pošaljite dokumenta ponovo',
                                'tooltip'=>'Pošaljite dokumenta ponovo',
                                'onclick'=>['sima.paychecks.sendOZFileForPaycheck',"$owner->id"]
                            ], true);
                        }
                        else
                        {
                            $return .= Yii::app()->controller->widget('SIMAButton',[
                                'action'=>[
                                    'title'=>'Pošaljite dokumenta ponovo',
                                    'tooltip'=>'Pošaljite dokumenta ponovo',
                                    'onclick'=>['sima.paychecks.sendOZFileForPaycheck',"$owner->id"]
                                ]
                            ], true); 
                        }
                    }     
                }           
                return $return;
            case 'period_sum_neto':
                $result = $owner->getPaychecksPeriodsSum('amount_neto');
                                
                return SIMAHtml::money_format($result);
            case 'period_sum_bruto':
                $result = $owner->getPaychecksPeriodsSum('amount_bruto');
                                
                return SIMAHtml::money_format($result);
            case 'period_sum_total':
                $result = $owner->getPaychecksPeriodsSum('amount_total');
                                
                return SIMAHtml::money_format($result);
            case 'period_sum_deductions':
                $result = $owner->getPaychecksPeriodsSum('deductions_sum');
                return SIMAHtml::money_format($result);
            case 'period_sum_neto_without_deductions':
                $period_sum_neto = $owner->getPaychecksPeriodsSum('amount_neto');
                $period_sum_deductions = $owner->getPaychecksPeriodsSum('deductions_sum');
                $result = $period_sum_neto - $period_sum_deductions;
                return SIMAHtml::money_format($result);
            case 'total_worked_hours':
                $result = $owner->getTotalWorkedHours();
                return $result;
//            case 'overtime':
//                $result = (int)($owner->getTotalWorkedHours()) - (int)($owner->paycheck_period->month_hours);
//                return $result;
            case 'employee_contributions':
                $result = SIMAHtml::money_format($owner->getEmployeeContributions());
                return $result;
            case 'tax':
                $result = SIMAHtml::money_format($owner->base_tax_empl + $owner->additionals_tax_empl);
                return $result;
            case 'paycheck_period.month.month_range':
                $result = $owner->paycheck_period->month;
                return $result;
            case 'sick_leave_injury_at_work':
            case 'sick_leave_pregnancy_till_30':
            case 'sick_leave_pregnancy_over_30':
            case 'sick_leave_maternity':
            case 'sick_leave_till_30':
            case 'sick_leave_over_30':
            case 'annual_leave':
            case 'state_and_religious_holiday':
            case 'work_on_non_working_holiday':
            case 'paid_leave':
            case 'standby':
                $criteria = new SIMADbCriteria([
                    'Model' => PaycheckAdditional::model(),
                    'model_filter' => [
                        'paycheck' => [
                            'ids' => $owner->id
                        ],
                        'type' => strtoupper($column)
                    ],
                ]);
                $criteria->select = 'sum(hours) as hours';
                
                $additional = PaycheckAdditional::model()->find($criteria);
                
                $result = $additional->hours;
                
                if(empty($result))
                {
                    $result = 0;
                }
                
                return $result;
            case 'file.hr_delivered_status':
                $result = '';
                if(!isset($owner->file))
                {
                    $result = Yii::t('PaychecksModule.Paycheck', 'OZFileNotCreated');
                }
                else
                {
                    $result = $owner->file->getAttributeDisplay('hr_delivered_status');
                }
                return $result;
            case 'file.last_document_delivery.delivery_type':
                $result = '';
                if(!isset($owner->file))
                {
                    $result = Yii::t('PaychecksModule.Paycheck', 'OZFileNotCreated');
                }
                else
                {
                    if(!isset($owner->file->last_document_delivery))
                    {
                        $result = Yii::t('FilesModule.File', 'NotSent');
                    }
                    else
                    {
                        $result = $owner->file->last_document_delivery->delivery_type;
                    }
                }
                return $result;
            default: return parent::columnDisplays($column); 
        }
        
    }

    public function modelViews()
    {
        return array(
            'indexTitle',
            'title',
            'options',
            'pdfPreview',
            'paychecks',
            'pdf_oz_content',
//            'paycheck_xml',//MilosS: depracted????
//            //MilosS: XML se vise ne generise iz Paycheck nego se prilikom obracuna prave XML-ovi
//            //odnosno PaycheckXML
//            'xml_eporezi' => ['html_wrapper' => null,'params_function'=>'paramsXMLePorezi'],
            'pdf_oz/pdf_oz_content'
        );
    }
    
//    public function paramsXMLePorezi($model)
//    {
//        $owner = $this->owner;
//        
//        $efective_worked_hours = $model->worked_hours;
//        foreach($owner->additionals as $additional)
//        {
//            
//            if(!$additional->isSeparateFieldInXML())
//            {
//                $efective_worked_hours += $additional->hours;
////                error_log("$efective_worked_hours += $additional->hours;");
//            }
//            else
//            {
////                $multiple_svps_in_period = true;
////                $recalculated_values = $additional->getRecalculatedValuesWithoutPrevious();
////                $bruto -= $recalculated_values['bruto'];
////                $bruto_in_separate_field_additionals += $additional->bruto;
////                $tax_release_in_separate_field_additionals += $additional->tax_release;
//            }
////            $fully_active_hours += $additional->hours;
//        }
//        
//        
//        $tax_release_used_in_previous_paychecks = 0;
//        $tax_base_in_previous_paychecks = 0;
//        $tax_release_used_in_previous_paychecks_with_same_svp = 0;
//        $tax_base_in_previous_paychecks_with_same_svp = 0;
//
//        $recalculated_values = $model->getRecalculatedValuesByParsedAdditionals();
//        $multiple_svps_in_period = $recalculated_values['multiple_svps_in_period'];
//        $bruto_in_separate_field_additionals = $recalculated_values['bruto_in_separate_field_additionals'];
////        $tax_release_in_separate_field_additionals = $recalculated_values['tax_release_in_separate_field_additionals'];
//        $fully_active_hours = $recalculated_values['fully_active_hours'];
//        $bruto = $recalculated_values['bruto'];
//        $porez = $recalculated_values['porez'];
//        $doprinosi_base = $recalculated_values['bruto'];
//        //TODO:
////        $doprinosi_base = $recalculated_values['doprinosi_base'];
//        $pio = $recalculated_values['pio'];
//        $zdr = $recalculated_values['zdr'];
//        $nez = $recalculated_values['nez'];
//        $tax_base = $recalculated_values['tax_base'];
//
//        $previous_paychecks = $model->getPreviousPaychecks();
//        foreach($previous_paychecks as $previous_paycheck)
//        {
//            $tax_release_used_in_previous_paychecks += $previous_paycheck->tax_release;
//            $tax_release_used_in_previous_paychecks_with_same_svp += $tax_release_used_in_previous_paychecks;
//            $tax_base_in_previous_paychecks += $previous_paycheck->amount_bruto;
//            $tax_base_in_previous_paychecks_with_same_svp += $tax_base_in_previous_paychecks;
//            foreach($previous_paycheck->additionals as $additional)
//            {
//                if((string)$additional->svp === (string)$model->employee_svp)
//                {
//                }
//                else
//                {
//                    $tax_release_used_in_previous_paychecks_with_same_svp -= $additional['tax_release'];
//                    $tax_base_in_previous_paychecks_with_same_svp -= $additional['bruto'];
//                }
//            }
//        }
//
//        $mfps = [];
//        if($tax_release_used_in_previous_paychecks_with_same_svp > 0)
//        {
//            $mfps[] = [
//                'code' => 'MFP.1',
//                'value' => number_format($tax_release_used_in_previous_paychecks_with_same_svp, 2, '.', '')
//            ];
//        }
//        if($tax_base_in_previous_paychecks_with_same_svp > 0)
//        {
//            $mfps[] = [
//                'code' => 'MFP.2',
//                'value' => number_format($tax_base_in_previous_paychecks_with_same_svp, 2, '.', '')
//            ];
//        }
//        if($fully_active_hours !== $model->month_period_hours)
//        {
//            $mfps[] = [
//                'code' => 'MFP.3',
//                'value' => number_format(($fully_active_hours/$model->month_period_hours)*100, 2, '.', '')
//            ];
//        }
//        if($tax_base_in_previous_paychecks_with_same_svp > 0)
//        {
//            $mfps[] = [
//                'code' => 'MFP.4',
//                'value' => number_format($tax_base_in_previous_paychecks_with_same_svp, 2, '.', '')
//            ];
//        }
//        if($multiple_svps_in_period === true)
//        {
//            $mfps[] = [
//                'code' => 'MFP.10',
//                'value' => 1
//            ];
//        }
//        if($tax_release_used_in_previous_paychecks > 0)
//        {
//            $mfps[] = [
//                'code' => 'MFP.11',
//                'value' => number_format($tax_release_used_in_previous_paychecks, 2, '.', '')
//            ];
//        }
//        if($model->paycheck_period->isFirstPaycheckPeriodInMonth())
//        {
//            if($multiple_svps_in_period === true && $bruto_in_separate_field_additionals>0)
//            {
//                $mfps[] = [
//                    'code' => 'MFP.12',
//                    'value' => number_format($bruto_in_separate_field_additionals, 2, '.', '')
//                ];
//            }
//        }
//        else if($tax_base_in_previous_paychecks > 0)
//        {
//            $mfps[] = [
//                'code' => 'MFP.12',
//                'value' => number_format($tax_base_in_previous_paychecks, 2, '.', '')
//            ];
//        }
//        
//        return [
//            'model' => $owner,
//            'mfps' => $mfps,
//            'efective_worked_hours' => number_format($efective_worked_hours, 1),
//            'bruto' => number_format($bruto, 2, '.', ''),
//            'tax_base' => number_format($tax_base, 2, '.', ''),
//            'porez' => number_format($porez, 2, '.', ''),
//            'doprinosi_base' => number_format($doprinosi_base, 2, '.', ''),
//            'pio' => number_format($pio, 2, '.', ''),
//            'zdr' => number_format($zdr, 2, '.', ''),
//            'nez' => number_format($nez, 2, '.', ''),
////            'efective_worked_hours' => number_format($efective_worked_hours, 1),
////            'efective_worked_hours' => number_format($efective_worked_hours, 1),
////            'orderNum' => $params['orderNum']
//        ];
//    }
    
    public function modelForms()
    {
        return array(
            //MilosS(21.3.2018): zakomentarisao sam polja koja se izracunavaju
            'default' => array(
                'type'=>'generic',
                'columns'=>array(
                    'paycheck_period_id' => array('searchField','relName'=>'paycheck_period', 'disabled'=>'disabled'), 
                    'employee_id' => array('searchField','relName'=>'employee', 'disabled' => 'disabled'), 
//                    'employee_svp'=>'textField',
                    'performance_percent' => 'numberField',
//                    'worked_hours'=>'textField',
//                    'worked_hours_value' => 'textField',
//                    'past_exp_value' => 'textField',
//                    'tax_release' => 'textField',
//                    'amount_bruto' => 'textField',
//                    'regres' => 'textField',
//                    'hot_meal_value' => 'textField',
                    'overtime_hours' => 'numberField',
//                    'hotmeal_fieldwork_value' => 'numberField'
                )
            ),
            //MilosS(21.3.2018.)
//            'inPeriod' => array(
//                'type'=>'generic',
//                'columns'=>array(
//                    'paycheck_period_id' => array('searchField','relName'=>'paycheck_period', 'disabled'=>'disabled'), 
//                    'employee_id' => array('searchField','relName'=>'employee'), 
//                    'worked_hours'=>'textField',
//                    'worked_hours_value' => 'textField',
//                    'past_exp_value' => 'textField',
//                    'amount_bruto' => 'textField'
//                )
//            ),
            'old_paycheck' => [
                'type'=>'generic',
                'columns' => [
                    'paycheck_period_id' => array('searchField','relName'=>'paycheck_period'), 
                    'employee_id' => array('searchField','relName'=>'employee'), 
                    'worked_hours'=>'textField',
                    'worked_hours_value' => 'textField',
                    'past_exp_value' => 'textField',
                    'amount_bruto' => 'textField'
                ]
            ]
        );
    }
    
    public function guiTableSettings($type)
    {        
        switch ($type)
        {
            case 'inPeriod': return array(
                'columns'=>array(
                    'employee.display_name',
                    'employee.person.JMBG',
                    'amount_neto_for_payout',
                    'amount_neto', 
                    'amount_bruto', 
                    'amount_total',
                    'download',
                    'paygrade_code',
                    'work_position_points',
                    'personal_points',
//                    'employee_id',
                    'deviations',
                    'file.last_document_delivery.delivery_type',
                    'employee.document_send_type',
                    'file.hr_delivered_status',
                    'send_documents',
                    'period_sum_neto',
                    'period_sum_bruto',
                    'period_sum_total',
                    'period_sum_deductions',
                    'period_sum_neto_without_deductions',
                    'employee.payout_type'
                ),
                'order_by' => array(
//                    'employee', 
                    'amount_neto', 
                    'amount_bruto', 
                    'amount_total',
                    'paygrade_code',
                    'work_position_points',
                    'personal_points',
                ),
                'sums' => array(
                    'amount_neto',
                    'amount_neto_for_payout',
                    'amount_bruto',
                    'amount_total',
                    'period_sum_neto',
                    'period_sum_bruto',
                    'period_sum_total',
                    'period_sum_deductions',
                    'period_sum_neto_without_deductions'
                ),
                'sums_function'=>'defaultSumFunc'
            );
            case 'old_paychecks': return array(
                'columns'=>array(
                    'employee', 
                    'paycheck_period', 
                    'worked_hours', 
                    'worked_hours_value', 
                    'past_exp_value',
                    'regres',
                    'hot_meal_value',
                    'previos_12_months_hours',
                    'previos_12_months_value'
                ),
                'sums' => array(
                    'worked_hours',
                    'worked_hours_value',
                    'past_exp_value',
                    'regres',
                    'hot_meal_value'
                ),
            );
            case 'forEmployee': return [
                'columns'=>array(
                    'download',
                    'paycheck_period.month.month_range',
                    'worked_hours',
                    'sick_leave_injury_at_work',
                    'sick_leave_pregnancy_till_30',
                    'sick_leave_pregnancy_over_30',
                    'sick_leave_maternity',
                    'sick_leave_till_30',
                    'sick_leave_over_30',
                    'annual_leave',
                    'state_and_religious_holiday',
                    'work_on_non_working_holiday',
                    'paid_leave',
                    'standby',
                    'total_worked_hours',
//                    'overtime',
                    'overtime_hours',
                    'period_sum_bruto',
                    'employee_contributions',
                    'tax',
                    'period_sum_neto',
                ),
                'sums' => array(
                    'worked_hours',
                    'sick_leave_injury_at_work',
                    'sick_leave_pregnancy_till_30',
                    'sick_leave_pregnancy_over_30',
                    'sick_leave_maternity',
                    'sick_leave_till_30',
                    'sick_leave_over_30',
                    'annual_leave',
                    'state_and_religious_holiday',
                    'work_on_non_working_holiday',
                    'paid_leave',
                    'standby',
                    'total_worked_hours',
//                    'overtime',
                    'overtime_hours',
                    'period_sum_bruto',
                    'employee_contributions',
                    'tax',
                    'period_sum_neto',
                ),
                'sums_function'=>'forEmployeeSumFunc'
            ];
            default: return array(
                'columns'=>array(
//                    'employee', 
                    'paycheck_period', 
                    'amount_neto_for_payout',
                    'amount_neto', 
                    'amount_bruto', 
                    'amount_total'
                ),
                'order_by' => array('paycheck_period')
            );
        }
    }
    
    public function defaultSumFunc($criteria)
    {
        $owner = $this->owner;
        
        $period_sum_neto = 0;
        $period_sum_bruto = 0;
        $period_sum_total = 0;
        $period_sum_deductions = 0;
        $period_sum_neto_without_deductions = 0;
        
        $paycheck_table = Paycheck::model()->tableName();
        $paycheck_period_table = PaycheckPeriod::model()->tableName();
        $month_table = Month::model()->tableName();
//        $paycheck_deductions_table = PaycheckDeduction::model()->tableName();
        $paycheck = Paycheck::model()->find($criteria);
        if(!empty($paycheck))
        {
            $period_sum_neto_sql = 'sum((select coalesce(sum(amount_neto), 0) '
                . 'from '.$paycheck_table.' p, '.$paycheck_period_table.' pp, '.$month_table.' m'
                . ' where p.paycheck_period_id=pp.id and pp.month_id=m.id'
                    . ' and p.is_valid and m.id='.$paycheck->paycheck_period->month->id.''
                    . ' and p.employee_id=t.employee_id)) as amount_neto, '
                . 'sum((select coalesce(sum(amount_bruto), 0) '
                . 'from '.$paycheck_table.' p, '.$paycheck_period_table.' pp, '.$month_table.' m'
                . ' where p.paycheck_period_id=pp.id and pp.month_id=m.id'
                    . ' and p.is_valid and m.id='.$paycheck->paycheck_period->month->id.''
                    . ' and p.employee_id=t.employee_id)) as amount_bruto, '
                . 'sum((select coalesce(sum(amount_total), 0) '
                . 'from '.$paycheck_table.' p, '.$paycheck_period_table.' pp, '.$month_table.' m'
                . ' where p.paycheck_period_id=pp.id and pp.month_id=m.id'
                    . ' and p.is_valid and m.id='.$paycheck->paycheck_period->month->id.''
                    . ' and p.employee_id=t.employee_id)) as amount_total, '
                . 'sum((select coalesce(sum(deductions_sum), 0) '
                . 'from '.$paycheck_table.' p, '.$paycheck_period_table.' pp, '.$month_table.' m'
                . ' where p.paycheck_period_id=pp.id and pp.month_id=m.id'
                    . ' and p.is_valid and m.id='.$paycheck->paycheck_period->month->id.''
                    . ' and p.employee_id=t.employee_id)) as deductions_sum';
            $criteria_neto = clone($criteria);
            $criteria_neto->select = $period_sum_neto_sql;
            $criteria_neto->order = '';
            $paycheck_neto = $owner->resetScope()->find($criteria_neto);
            $period_sum_neto = $paycheck_neto->amount_neto;
            $period_sum_bruto = $paycheck_neto->amount_bruto;
            $period_sum_total = $paycheck_neto->amount_total;
            $period_sum_deductions = $paycheck_neto->deductions_sum;
            $period_sum_neto_without_deductions = $period_sum_neto - $period_sum_deductions;
        }
        
//        $period_sum_neto = 0;
//        $period_sum_bruto = 0;
//        $period_sum_total = 0;
//        $paychecks = Paycheck::model()->findAll($criteria);
//        foreach($paychecks as $paycheck)
//        {
//            $period_sum_neto += $paycheck->getPaychecksPeriodsSum('amount_neto');
//            $period_sum_bruto += $paycheck->getPaychecksPeriodsSum('amount_bruto');
//            $period_sum_total += $paycheck->getPaychecksPeriodsSum('amount_total');
//        }
        
        $temp_criteria = new SIMADbCriteria();        
        $temp_criteria->select = "sum(amount_neto) as amount_neto, sum(amount_neto_for_payout) as amount_neto_for_payout, sum(amount_bruto) as amount_bruto, sum(amount_total) as amount_total, "
                . " $period_sum_neto AS period_sum_neto, $period_sum_bruto as period_sum_bruto, $period_sum_total as period_sum_total,"
                . " $period_sum_deductions as period_sum_deductions, $period_sum_neto_without_deductions as period_sum_neto_without_deductions";
        $criteria->mergeWith($temp_criteria);
        $criteria->order = '';
        $sum_result = $owner->resetScope()->find($criteria);
        return $sum_result;
    }
    
    public function forEmployeeSumFunc($criteria)
    {
        $owner = $this->owner;
        
        $additional_append_str = '';
        $period_sum_bruto = 0;
        $employee_contributions = 0;
        $tax = 0;
        $period_sum_neto = 0;
        $allPaychecks = Paycheck::model()->findAll($criteria);
        if(!empty($allPaychecks))
        {
            $additionals = [
                'sick_leave_injury_at_work' => 0,
                'sick_leave_pregnancy_till_30' => 0,
                'sick_leave_pregnancy_over_30' => 0,
                'sick_leave_maternity' => 0,
                'sick_leave_till_30' => 0,
                'sick_leave_over_30' => 0,
                'annual_leave' => 0,
                'state_and_religious_holiday' => 0,
                'work_on_non_working_holiday' => 0,
                'paid_leave' => 0,
                'standby' => 0,
            ];
            
            foreach($allPaychecks as $paycheck)
            {
                foreach($additionals as $additionalName => $curSum)
                {
                    $additionalCriteria = new SIMADbCriteria([
                        'Model' => PaycheckAdditional::model(),
                        'model_filter' => [
                            'paycheck' => [
                                'ids' => $paycheck->id
                            ],
                            'type' => strtoupper($additionalName)
                        ],
                    ]);
                    $additionalCriteria->select = 'sum(hours) as hours';
                    $additional = PaycheckAdditional::model()->find($additionalCriteria);
                    $additionals[$additionalName] = $curSum + $additional->hours;
                }
                
                $period_sum_bruto += $paycheck->getPaychecksPeriodsSum('amount_bruto');
                $employee_contributions += $paycheck->getEmployeeContributions();
                $tax += ($paycheck->base_tax_empl + $paycheck->additionals_tax_empl);
                $period_sum_neto += $paycheck->getPaychecksPeriodsSum('amount_neto');
            }
            
            foreach($additionals as $additionalName => $val)
            {
                $additional_append_str .= ', '.$val.' AS '.$additionalName;
            }
        }
                
        $temp_criteria = new SIMADbCriteria();
        $temp_criteria->select = "sum(worked_hours) as worked_hours, sum(overtime_hours) as overtime_hours, "
                . "$period_sum_bruto as period_sum_bruto, $employee_contributions as employee_contributions, "
                . "$tax as tax, $period_sum_neto as period_sum_neto "
                . "$additional_append_str";
        $criteria->mergeWith($temp_criteria);
        $criteria->order = '';
        $sum_result = $owner->resetScope()->find($criteria);
        return $sum_result;
    }
    
    public function afterValidateDependencyError(array $foreign_dependencies)
    {
        $allowed_tables = [
            PaycheckAdditional::model()->tableName().':paycheck_id',
            PaycheckXML::model()->tableName().':paycheck_id'
        ];

        $parsed_foreign_dependencies = array_diff($foreign_dependencies, $allowed_tables);
        
        parent::afterValidateDependencyError($parsed_foreign_dependencies);
    }
}