<?php

class PaycheckCodebookGUI extends SIMAActiveRecordGUI
{
    public function columnLabels()
    {
        return [
            'start_use' => Yii::t('PaychecksModule.PaycheckCodebook', 'StartUse'),
            'end_use' => Yii::t('PaychecksModule.PaycheckCodebook', 'EndUse'),
            'pio_empl' => Yii::t('PaychecksModule.PaycheckCodebook', 'PioEmpl'),
            'zdr_empl' => Yii::t('PaychecksModule.PaycheckCodebook', 'ZdrEmpl'),
            'nez_empl' => Yii::t('PaychecksModule.PaycheckCodebook', 'NezEmpl'),
            'tax_empl' => Yii::t('PaychecksModule.PaycheckCodebook', 'TaxEmpl'),
            'tax_release' => Yii::t('PaychecksModule.PaycheckCodebook', 'TaxRelease'),
            'pio_comp' => Yii::t('PaychecksModule.PaycheckCodebook', 'PioComp'),
            'zdr_comp' => Yii::t('PaychecksModule.PaycheckCodebook', 'ZdrComp'),
            'nez_comp' => Yii::t('PaychecksModule.PaycheckCodebook', 'NezComp'),
            'min_contribution_base' => Yii::t('PaychecksModule.PaycheckCodebook', 'MinTaxBase'),
            'max_contribution_base' => Yii::t('PaychecksModule.PaycheckCodebook', 'MaxTaxBase')
        ];
    }
    
    public function columnDisplays($column) 
    {
        $owner = $this->owner;
        switch ($column) {
            case 'pio_empl':
            case 'zdr_empl':
            case 'nez_empl':
            case 'tax_empl':
            case 'tax_release':
            case 'pio_comp':
            case 'zdr_comp':
            case 'nez_comp':
            case 'min_contribution_base':
            case 'max_contribution_base':
                return SIMAHtml::modelNumberFormat($owner, $column);
            default: return parent::columnDisplays($column);
        }
    }
    
    public function modelLabel($plural = false)
    {
        return Yii::t('PaychecksModule.Codebook',$plural?'Codebooks':'Codebook');
    }
    
    public function modelForms()
    {
        return array(
            'default' => [
                'type'=>'generic',
                'columns' => [
                    'start_use' => ['datetimeField'],
                    'end_use' => ['datetimeField'],
                    'pio_empl' => ['numberField', 'number_validation' => true, 'max_value' => 100],
                    'zdr_empl' => ['numberField', 'number_validation' => true, 'max_value' => 100],
                    'nez_empl' => ['numberField', 'number_validation' => true, 'max_value' => 100],
                    'tax_empl' => ['numberField', 'number_validation' => true, 'max_value' => 100],
                    'tax_release' => ['numberField', 'number_validation' => true, 'max_value' => 999999.99],
                    'pio_comp' => ['numberField', 'number_validation' => true, 'max_value' => 100],
                    'zdr_comp' => ['numberField', 'number_validation' => true, 'max_value' => 100],
                    'nez_comp' => ['numberField', 'number_validation' => true, 'max_value' => 100],
                    'min_contribution_base' => ['numberField', 'number_validation' => true, 'max_value' => 99999999.99],
                    'max_contribution_base' => ['numberField', 'number_validation' => true, 'max_value' => 99999999.99]
                ]
            ],
        );
    }
    
    public function guiTableSettings($type)
    {        
        switch ($type)
        {
            default: return [
                'columns' => [
                    'start_use', 
                    'end_use',
                    'pio_empl', 
                    'zdr_empl', 
                    'nez_empl',
                    'tax_empl',
                    'tax_release',
                    'pio_comp',
                    'zdr_comp',
                    'nez_comp',
                    'min_contribution_base',
                    'max_contribution_base'
                ],
                'order_by' => [
                    'start_use'
                ]
            ];
        }
    }
}