<?php

class PaycheckXMLGUI extends SIMAActiveRecordGUI
{

    public function columnLabels()
    {
        return array(
            
        )+parent::columnLabels();
    }
    
    public function columnDisplays($column)
    {
        $owner = $this->owner;
        switch ($column)
        {
//            case 'amount_neto': case 'amount_bruto': case 'amount_total': 
//                return SIMAHtml::money_format($owner->$column);
//            case 'download':
//                return isset($owner->file) ? SIMAHtmlButtons::fileOpen($owner->file) : '';
//            case 'employee_id':
//                return $owner->employee_id;
//            case 'previos_12_months_hours':
//                $getPrevious12MonthsValues = PaychecksEmployee::GetEmployeesPrevious12MonthsValuesForPeriod($owner->employee, $owner->paycheck_period);
//                
//                $result = $getPrevious12MonthsValues['hours'];
//                
//                return number_format($result, 0, '.', ' ');
//            case 'previos_12_months_value':
//                $getPrevious12MonthsValues = PaychecksEmployee::GetEmployeesPrevious12MonthsValuesForPeriod($owner->employee, $owner->paycheck_period);
//                
//                $result = $getPrevious12MonthsValues['value'];
//                                
//                return SIMAHtml::money_format($result);
//            case 'work_position_points':
//                $_now = '';
//                if (isset($owner->employee->last_work_contract->work_position->paygrade->points) 
//                    && $owner->employee->last_work_contract->work_position->paygrade->points != $owner->$column)
//                {
//                    $_now = '('.$owner->employee->last_work_contract->work_position->paygrade->points.')';
//                }
//                return parent::columnDisplays($column).$_now;
//            case 'personal_points':
//                $_now = '';
//                if (isset($owner->employee->last_work_contract->work_position->paygrade->points) 
//                    && $owner->employee->personal_points != $owner->$column)
//                {
//                    $_now = '('.$owner->employee->personal_points.')';
//                }
//                return parent::columnDisplays($column).$_now;
//            case 'paygrade_code': 
//                $_now = '';
//                if (isset($owner->employee->last_work_contract->work_position->paygrade->paygrade_code) 
//                    && $owner->employee->last_work_contract->work_position->paygrade->paygrade_code != $owner->$column)
//                {
//                    $_now = '('.$owner->employee->last_work_contract->work_position->paygrade->points.')';
//                }
//                return parent::columnDisplays($column).$_now;
//            case 'deviations':
//                $result = Yii::t('BaseModule.Common', 'DoNotHave');
//                
//                $deviations = $owner->getDeviations();
//                if(count($deviations) > 0)
//                {
//                    $result = implode('</br>', $deviations);
//                }
//                
//                return $result;
            
            
            case 'effective_hours': case 'month_hours': 
                return number_format ($owner->$column, 1,'.', '');
            case 'pio': case 'zdr': case 'nez': case 'pio_ben':
            case 'bruto': case 'tax_base': case 'tax_empl': case 'doprinosi_base':
            case 'mfp1': 
            case 'mfp2': 
            case 'mfp3': 
            case 'mfp4': 
            case 'mfp5': 
            case 'mfp6': 
//            case 'mfp7': 
//            case 'mfp8': 
//            case 'mfp9': 
//            case 'mfp10': 
            case 'mfp11': 
            case 'mfp12': 
                return number_format ($owner->$column, 2,'.', '');
            
            default: return parent::columnDisplays($column); 
        }
        
    }

    public function modelViews()
    {
        return array(
            'xml_eporezi' => ['html_wrapper' => null,'params_function'=>'paramsXMLePorezi']
        );
    }
    
    public function paramsXMLePorezi($model)
    {
        $owner = $this->owner;
        $columns = [
            'mfp1' => 'MFP.1',
            'mfp2' => 'MFP.2',
            'mfp3' => 'MFP.3',
            'mfp4' => 'MFP.4',
            'mfp5' => 'MFP.5',
            'mfp6' => 'MFP.6',
            'mfp7' => 'MFP.7',
            'mfp8' => 'MFP.8',
            'mfp9' => 'MFP.9',
            'mfp10' => 'MFP.10',
            'mfp11' => 'MFP.11',
            'mfp12' => 'MFP.12',
        ];
        $mfps = [];
        
        foreach ($columns as $column_name => $xml_name)
        {
            if (!empty($owner->$column_name))
            {
                $mfps[] = [
                    'code' => $xml_name,
                    'value' => $owner->getAttributeDisplay($column_name)
                ];
            }
        }
        
        return [
            'model' => $owner,
            'mfps' => $mfps,
        ];
    }
    
    public function modelForms()
    {
        return array(
            'default' => array(
                'type'=>'generic',
                'columns'=>array(
                    'paycheck_period_id' => array('searchField','relName'=>'paycheck_period', 'disabled'=>'disabled'), 
                    'employee_id' => array('searchField','relName'=>'employee'), 
                    'employee_svp'=>'textField',
                    'performance_percent' => 'textField',
                    'worked_hours'=>'textField',
                    'worked_hours_value' => 'textField',
                    'past_exp_value' => 'textField',
                    'tax_release' => 'textField',
                    'amount_bruto' => 'textField',
                    'regres' => 'textField',
                    'hot_meal_value' => 'textField'
                )
            ),
            'inPeriod' => array(
                'type'=>'generic',
                'columns'=>array(
                    'paycheck_period_id' => array('searchField','relName'=>'paycheck_period', 'disabled'=>'disabled'), 
                    'employee_id' => array('searchField','relName'=>'employee'), 
                    'worked_hours'=>'textField',
                    'worked_hours_value' => 'textField',
                    'past_exp_value' => 'textField',
                    'amount_bruto' => 'textField'
                )
            ),
            'old_paycheck' => [
                'type'=>'generic',
                'columns' => [
                    'paycheck_period_id' => array('searchField','relName'=>'paycheck_period'), 
                    'employee_id' => array('searchField','relName'=>'employee'), 
                    'worked_hours'=>'textField',
                    'worked_hours_value' => 'textField',
                    'past_exp_value' => 'textField',
                    'amount_bruto' => 'textField'
                ]
            ]
        );
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default: return [
                'columns' => [
                    'order_number',
                    'svp',
                    'indentification_type',
                    'indentification_value',
                    'lastname',
                    'firstname',
                    'municipality_code',
                    'number_of_calendar_days',
                    'effective_hours',
                    'month_hours',
                    'bruto',
                    'tax_base',
                    'tax_empl',
                    'doprinosi_base',
                    'pio',
                    'zdr',
                    'nez',
                    'pio_ben',
                    'paycheck',
                    'mfp1',
                    'mfp2',
                    'mfp3',
                    'mfp4',
                    'mfp5',
                    'mfp6',
                    'mfp7',
                    'mfp8',
                    'mfp9',
                    'mfp10',
                    'mfp11',
                    'mfp12'
                ],
                'order_by' => ['order_number']
            ];
        }
    }
    
    public function defaultSumFunc($criteria)
    {
        $owner = $this->owner;
        $temp_criteria = new SIMADbCriteria();        
        $temp_criteria->select = "sum(amount_neto) as amount_neto, sum(amount_bruto) as amount_bruto, sum(amount_total) as amount_total";
        $criteria->mergeWith($temp_criteria);
        $criteria->order = '';
        $sum_result = $owner->resetScope()->find($criteria);
        return $sum_result;
    }
}