<?php

class TaxesPaidConfirmationGUI extends SIMAActiveRecordGUI
{
    public function columnLabels()
    {
        return array(
            'employee' => 'Zaposleni',
            'employee_id' => 'Zaposleni',
            'year' => 'Godina',
            'year_id' => 'Godina',
            'file' => 'Dokument',
            'file_id' => 'Dokument',
            'generate_document' => 'Generišite dokument',
            'delivered_status' => 'Status dostave',            
            'delivery_type' => 'Način dostave',
            'send_documents' => 'Pošaljite dokument ponovo'
        ) + parent::columnLabels();
    }

    public function modelLabel($plural = false)
    {
        return 'Potvrda o plaćenim porezima i doprinosima po odbitku';
    }
    
    public function columnDisplays($column)
    {
        $owner = $this->owner;
        switch ($column)
        {            
            case 'generate_document':
                    return $owner->generateDocumentTemplate();
            case 'send_documents':
                $return = '';
                if ($owner->employee->document_send_type === Employee::$E_MAIL)
                {
                    if(SIMAMisc::isVueComponentEnabled())
                    {
                        $return .= Yii::app()->controller->widget(SIMAButtonVue::class,[
                            'title'=>'Pošaljite dokumenta ponovo',
                            'tooltip'=>'Pošaljite dokumenta ponovo',
                            'onclick'=>['sima.paychecks.sendTaxesPaidConfirmationToUser',"$owner->id"]
                        ], true);
                    }
                    else
                    {
                        $return .= Yii::app()->controller->widget('SIMAButton',[
                            'action'=>[
                                'title'=>'Pošaljite dokumenta ponovo',
                                'tooltip'=>'Pošaljite dokumenta ponovo',
                                'onclick'=>['sima.paychecks.sendTaxesPaidConfirmationToUser',"$owner->id"]
                            ]
                        ], true);
                    }
                }
                return $return;
            default : return parent::columnDisplays($column);
        }
    }
    
    public function generateDocumentTemplate()
    {
        $owner = $this->owner;
        
        $result = SIMAHtml::fileDownload($owner->file);
        
        if(SIMAMisc::isVueComponentEnabled())
        {
            $result .= Yii::app()->controller->widget(SIMAButtonVue::class, [
                'title'=>'Generišite',
                'onclick'=>['sima.paychecks.generateTaxesPaidConfirmationDocument',$owner->id],
            ],true);
        }
        else
        {
            $result .= Yii::app()->controller->widget('SIMAButton', [
                'action'=>[ 
                    'title'=>'Generišite',
                    'onclick'=>['sima.paychecks.generateTaxesPaidConfirmationDocument',$owner->id],
                ]
            ],true);
        }
        
        return $result;
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            case 'byYear': return [
                'columns' => array(
                    'file','employee','generate_document',
                    'file.last_document_delivery.delivery_type',
                    'employee.document_send_type',
                    'file.hr_delivered_status', 'send_documents'
                ),
                'filters'=>array(
                    'year'
                ),
            ];
            default: return array(
                'columns' => array(
                    'file','employee','year','generate_document', 'delivery_type', 'delivered_status', 'send_documents'
                )
            );
        }
    }
    
    public function modelForms()
    {
        $owner = $this->owner;
        $year_id = ['relation', 'relName'=>'year'];
        if (isset($owner->year))
        {
            $year_id = ['relation', 'relName'=>'year', 'disabled'=>'disabled'];
        }
        return array(
            'default' => array(
                'type' => 'generic',
                'columns' => array(
                    'employee_id' => ['relation', 'relName'=>'employee'],
                    'year_id' => $year_id
                )
            )
        );
    }
    
    public function droplist($key, $relName='', $filter=null)
    {
        switch ($key)
        {
            case 'delivery_type':
                return Employee::$document_send_types;
            default: return parent::droplist($key, $relName, $filter);
        }
        
    }
}