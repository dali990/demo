<?php

class SIMAWarnHotMealDeductionNoRegularHours extends SIMAWarnPaycheckCalculationFailed
{
    public function __construct(Employee $employee)
    {
        $err_msg = Yii::t('PaychecksModule.Paycheck', 'HotMealDeductionNoRegularHours', [
            '{emp}' => $employee->DisplayName
        ]);
        parent::__construct($employee, $err_msg);
    }
}