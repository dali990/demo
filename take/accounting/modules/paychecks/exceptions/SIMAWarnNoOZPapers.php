<?php

class SIMAWarnNoOZPapers extends SIMAWarnException
{
    public function __construct(PaycheckPeriod $pp=null)
    {
        $err_msg = "Nisu generisani OZ listici";
        if (isset($pp))
        {
            $err_msg .= " za period $pp->DisplayName";
        }
        parent::__construct($err_msg);
    }
}