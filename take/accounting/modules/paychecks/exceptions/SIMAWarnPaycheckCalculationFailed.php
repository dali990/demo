<?php

class SIMAWarnPaycheckCalculationFailed extends SIMAWarnException
{
    protected $_emp = null;
    
    public function __construct(Employee $employee, $_err_msg = '')
    {
        $this->_emp = $employee;
        if (empty($_err_msg))
        {
            $err_msg = Yii::t('PaychecksModule.Paycheck', 'PaycheckFailed', [
                '{emp}' => $employee->DisplayName
            ]);
        }
        else
        {
            $err_msg = $_err_msg;
        }
        parent::__construct($err_msg);
    }
    
    public function getEmployee()
    {
        return $this->_emp;
    }
}