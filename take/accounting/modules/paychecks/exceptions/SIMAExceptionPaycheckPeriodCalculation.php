<?php

class SIMAExceptionPaycheckPeriodCalculation extends SIMAWarnException
{
    public function __construct($message)
    {
        parent::__construct($message);
    }
}