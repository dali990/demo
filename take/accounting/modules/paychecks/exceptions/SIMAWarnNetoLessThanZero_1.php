<?php

class SIMAWarnNetoLessThanZero extends SIMAWarnPaycheckCalculationFailed
{
    public function __construct(Employee $employee)
    {
        $err_msg = Yii::t('PaychecksModule.Paycheck', 'AmountNetoLessThanZero', [
            '{emp}' => $employee->DisplayName
        ]);
        parent::__construct($employee, $err_msg);
    }
}