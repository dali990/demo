<?php

class SIMAWarnDeletePaycheckPeriodWithPaychecks extends SIMAWarnException
{
    public function __construct()
    {
        $err_msg = Yii::t('PaychecksModule.PaycheckPeriod', 'DeletePaycheckPeriodWithPaychecks');
        parent::__construct($err_msg);
    }
}