<?php

class SIMAWarnPaycheckPeriodCalculationNote extends SIMAWarnException
{
    public function __construct($message)
    {
        parent::__construct($message);
    }
}