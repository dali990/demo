<?php

class PaycheckPeriodComponent
{
    /**
     * Kreiranje svih OZ listica u isplati zarade
     * @param PaycheckPeriod $paycheckPeriod
     */
    public static function CreatePdfsForPaycheckPeriod(PaycheckPeriod $paycheckPeriod)
    {
        $paychecks = $paycheckPeriod->valid_paychecks;
        
        $paychecks_count = count($paychecks);
        if($paychecks_count>0)
        {
            $eventSourceStepsSum = 1;
            $eventSourceSteps = (99/$paychecks_count);
            foreach($paychecks as $paycheck)
            {
                set_time_limit(5);
                $eventSourceStepsSum += $eventSourceSteps;
                Yii::app()->controller->sendUpdateEvent($eventSourceStepsSum);
                
                PaycheckPeriodComponent::CreatePdfForPaycheck($paycheck);
            }
        }
    }
    
    /**
     * Kreiranje OZ listica za jednu zaradu
     * @param Paycheck $paycheck
     */
    public static function CreatePdfForPaycheck(Paycheck $paycheck)
    {
        $htmlcode = Yii::app()->controller->renderModelView($paycheck, 'pdf_oz/pdf_oz_content');

        $tempPDFFile = new TemporaryFile(null, null, 'pdf');
        $tempPDFFile->createPdfFromHtml([
            'bodyHtml' => $htmlcode
        ], [
            'zoom' => 1.3
        ]);
        $tempFileName = $tempPDFFile->getFileName();
        
        $name_without_extension = Yii::t('PaychecksModule.Paycheck', 'FileName', array(
            '{employee}' => $paycheck->employee,
            '{date}' => $paycheck->paycheck_period->month,
            '{part}' => $paycheck->paycheck_period->ordering_number
        ));
        
        $name_full_with_extension = $name_without_extension.'.pdf';

        $paycheck_have_file = false;
        if (isset($paycheck->file))
        {
            $paycheck_have_file = true;
        }
        
        $file = null;
        if ($paycheck_have_file)
        {
            $file = $paycheck->file;
            $file->beforeChangeLock();
        }
        else
        {
            $file = new File();
        }
        
        $file->name = $name_without_extension;
        $file->save();
        
        if ($paycheck_have_file)
        {
            $file->afterChangeUnLock();
        }
        
        $file->appendTempFile($tempFileName, $name_full_with_extension);
        
        if (!$paycheck_have_file)
        {
            $paycheck->file_id = $file->id;
            $paycheck->save();
        }
    }
    
    /**
     * 
     * @param PaycheckPeriod $paycheckPeriod
     * @return array - niz ciji je svaki element oblika:
     *  [key => empid, value => empmodel]
     */
    public static function EmployeesActiveInPeriod(PaycheckPeriod $paycheckPeriod)
    {
        $wcCriteria = new SIMADbCriteria([
            'Model' => 'WorkContract',
            'model_filter' => [
                'signed' => true,
                'for_payout' => true,
                'active_at' => $paycheckPeriod->month
            ]
        ]);
        $work_contracts = WorkContract::model()->with('employee')->findAll($wcCriteria);
        $work_contracts_count = count($work_contracts);
        if($work_contracts_count < 1)
        {
            $work_contracts_count = 1;
        }
        $employees = [];
        foreach($work_contracts as $wc)
        {
            if(!isset($employees[$wc->employee->id]))
            {
                $employees[$wc->employee->id] = $wc->employee;
            }
        }
        unset($work_contracts);
        unset($wcCriteria);
        
        return $employees;
    }
    
    public static function GetLastPayOutDayInFinalPeriod()
    {
        $result = null;
        
        $last_confirmed_paycheck_period_criteria = new SIMADbCriteria([
            'Model' => PaycheckPeriod::model(),
            'model_filter' => [
                'pp_type' => PaycheckPeriod::$FINAL,
                'confirmed' => true,
            ]
        ]);
        $last_confirmed_paycheck_period_criteria->order = 'display_name DESC';
        $last_confirmed_paycheck_period_criteria->limit = 1;
        $last_confirmed_paycheck_period = PaycheckPeriod::model()->find($last_confirmed_paycheck_period_criteria);
        
        if(!empty($last_confirmed_paycheck_period))
        {
            $result = $last_confirmed_paycheck_period->month->lastDay();
        }
        
        return $result;
    }
    
    public static function GetPaychecksWhithComparisonDiff(array $paychecks_for_compare, $verbose_level=0, $input_db_paycheck=null)
    {
        $result = [];
        
        foreach($paychecks_for_compare as $paycheck)
        {
            $paycheck_id = $paycheck->id;
            
            $columns_in_diff = ['amount_neto','amount_bruto','amount_total'];
            if($verbose_level > 1)
            {
                $columns_in_diff = array_diff(Paycheck::model()->getTableSchema()->getColumnNames(), [
                    'tax_release', 'suspensions_sum', 'amount_neto_for_payout'
                ]);
            }
            $columns_metadata = Paycheck::model()->getMetaData()->tableSchema->columns;
            
            $db_paycheck = null;
            if(!is_null($input_db_paycheck))
            {
                $db_paycheck = $input_db_paycheck;
            }
            else
            {
                $db_paycheck = Paycheck::model()->findByPk($paycheck_id);
            }
            
            $_max_diff = 0.01;
                        
            $fields_in_diff = [];
            foreach($columns_in_diff as $column_in_diff)
            {
                if(
                        isset($columns_metadata[$column_in_diff]) 
                        && 
                        strpos(($columns_metadata[$column_in_diff])->dbType, 'numeric') !== false
                )
                {
                    $db_paycheck_col_val = $db_paycheck->$column_in_diff;
                    $paycheck_col_val = $paycheck->$column_in_diff;
                    if(abs($db_paycheck_col_val - $paycheck_col_val) > $_max_diff)
                    {
                        if($verbose_level === 0)
                        {
                            $result = true;
                            return true;
                        }
                        $fields_in_diff[] = [
                            'field_name' => $paycheck->getAttributeLabel($column_in_diff),
                            'old_val' => $db_paycheck->getAttributeDisplay($column_in_diff),
                            'new_val' => $paycheck->getAttributeDisplay($column_in_diff),
                        ];
                    }
                }
            }
            
            if(!empty($fields_in_diff))
            {
                $result[$paycheck_id] = [
                    'have_diff' => true,
                    'model' => $paycheck,
                    'fields_in_diff' => $fields_in_diff
                ];
            }
        }
        
        return $result;
    }
}
