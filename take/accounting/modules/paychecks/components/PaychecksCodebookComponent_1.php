<?php

class PaychecksCodebookComponent
{
    public static function maintainCommand()
    {        
        $codebook_data = PaychecksCodebookComponent::getCodebookData();
        
        $rets_to_add = $codebook_data['rets_to_add'];
        $rets_to_update = $codebook_data['rets_to_update'];
        
        if(!empty($rets_to_add) || !empty($rets_to_update))
        {
            $rets_to_add_count = count($rets_to_add);
            $rets_to_update_count = count($rets_to_update);
            
            Yii::app()->notifManager->sendNotif('modelPaycheckCodebookSync',  Yii::t('PaychecksModule.Codebook', 'MaintainSyncTitle', [
                '{to_add_count}' => $rets_to_add_count,
                '{to_update_count}' => $rets_to_update_count
            ]), [
                'action' => [
                    'action' => 'guitable',
                    'action_id' => 'paychecks_1'
                ]
            ], 'PaycheckCodebookSync');
        }
        
        return [];
    }
    
    public static function getCodebookData()
    {
        $converted_codebook_rets = AdminCodebookComponent::getDataFromCodebook('gePaychecksCodebookFromCodebook');
        
        $rets_to_add = [];
        $rets_to_update = [];
        foreach($converted_codebook_rets as $codebook_ret)
        {            
            $codebook_ret_start_use = $codebook_ret['start_use'];
            $local_pc = PaycheckCodebook::model()->find([
                'model_filter' => [
                    'start_use' => $codebook_ret_start_use
                ]
            ]);
            
            if(empty($local_pc))
            {
                $rets_to_add[] = $codebook_ret;
            }
            else
            {
                $to_update = false;
                foreach($codebook_ret as $key => $value)
                {
                    if($key === 'start_use')
                    {
                        continue;
                    }
                    
                    /// TODO: treba preimenovati u sifarniku i ovo izbaciti
                    if($key === 'min_tax_base')
                    {
                        $key = 'min_contribution_base';
                    }
                    else if($key === 'max_tax_base')
                    {
                        $key = 'max_contribution_base';
                    }
                    /// TODO end
                    
                    if($key === 'end_use')
                    {
                        $value = SIMAHtml::DbToUserDate($value);
                    }
                    else
                    {
                    
                        $column_type = gettype($local_pc->$key);
                        settype($value, $column_type);
                    }
                    if($local_pc->$key !== $value)
                    {
                        $local_pc->$key = $value;
                        $to_update = true;
                        $rets_to_update[] = $local_pc;
                    }
                }
            }
        }
        
        return [
            'rets_to_add' => $rets_to_add,
            'rets_to_update' => $rets_to_update
        ];
    }
    
    /**
     * f-ja koju treba ukloniti kada se bude uklonila i konfiguracija
     */
//    public static function useNewCodebooks()
//    {
//        if(isset(Yii::app()->params['use_new_codebooks']))
//        {
//            return Yii::app()->params['use_new_codebooks'];
//        }
//        
//        return Yii::app()->configManager->get('accounting.paychecks.use_new_coodebooks') === true;
//    }
    
    /**
     * f-ja koju treba ukloniti kada se bude uklonila i konfiguracija
     */
    public static function useMaxTaxBase()
    {
//        if(PaychecksCodebookComponent::useNewCodebooks() === false)
//        {
//            return false;
//        }
        
        if(isset(Yii::app()->params['use_max_contribution_base']))
        {
            return Yii::app()->params['use_max_contribution_base'];
        }
        
        return Yii::app()->configManager->get('accounting.paychecks.use_max_contribution_base') === true;
    }
    
    public static function autoSyncPaychecksDiffBetweenLocalAndCodebook()
    {
        $model_name = PaycheckCodebook::class;
        $codebook_paychecks_codebook = AdminController::getDataFromCodebook(Yii::app()->params['codebook_base'].AdminController::$codebook_const_path_part.'gePaychecksCodebookFromCodebook');      
        $codebook_unique_fields = [];
        
        $transaction = Yii::app()->db->beginTransaction();
        try
        {
            foreach ($codebook_paychecks_codebook as $codebook_paycheck_codebook)
            {
                $local_pc = $model_name::model()->findByAttributes([
                    'start_use' => $codebook_paycheck_codebook['start_use']
                ]);
                if (empty($local_pc))
                {                
                    $model = new $model_name();
                    $model->start_use = $codebook_paycheck_codebook["start_use"];
                    $model->end_use = $codebook_paycheck_codebook["end_use"];
                    $model->pio_empl = $codebook_paycheck_codebook["pio_empl"];
                    $model->zdr_empl = $codebook_paycheck_codebook["zdr_empl"];
                    $model->nez_empl = $codebook_paycheck_codebook["nez_empl"];
                    $model->tax_empl = $codebook_paycheck_codebook["tax_empl"];
                    $model->tax_release = $codebook_paycheck_codebook["tax_release"];
                    $model->pio_comp = $codebook_paycheck_codebook["pio_comp"];
                    $model->zdr_comp = $codebook_paycheck_codebook["zdr_comp"];
                    $model->nez_comp = $codebook_paycheck_codebook["nez_comp"];
                    $model->min_contribution_base = $codebook_paycheck_codebook["min_contribution_base"];
                    $model->max_contribution_base = $codebook_paycheck_codebook["max_contribution_base"];
                    $model->save(false);
                }
                else
                {
                    if (
                            $codebook_paycheck_codebook["start_use"] !== $local_pc->getAttribute('start_use') ||
                            $codebook_paycheck_codebook["end_use"] !== $local_pc->getAttribute('end_use') ||
                            doubleval($codebook_paycheck_codebook['pio_empl']) !== $local_pc->pio_empl ||
                            doubleval($codebook_paycheck_codebook['zdr_empl']) !== $local_pc->zdr_empl ||
                            doubleval($codebook_paycheck_codebook['nez_empl']) !== $local_pc->nez_empl ||
                            doubleval($codebook_paycheck_codebook['tax_empl']) !== $local_pc->tax_empl ||
                            doubleval($codebook_paycheck_codebook['tax_release']) !== $local_pc->tax_release ||
                            doubleval($codebook_paycheck_codebook['pio_comp']) !== $local_pc->pio_comp ||
                            doubleval($codebook_paycheck_codebook['zdr_comp']) !== $local_pc->zdr_comp ||
                            doubleval($codebook_paycheck_codebook['nez_comp']) !== $local_pc->nez_comp ||
                            doubleval($codebook_paycheck_codebook['min_contribution_base']) !== $local_pc->min_contribution_base ||
                            doubleval($codebook_paycheck_codebook['max_contribution_base']) !== $local_pc->max_contribution_base 
                        )
                    {
                        $local_pc->start_use = $codebook_paycheck_codebook["start_use"];
                        $local_pc->end_use = $codebook_paycheck_codebook["end_use"];
                        $local_pc->pio_empl = $codebook_paycheck_codebook["pio_empl"];
                        $local_pc->zdr_empl = $codebook_paycheck_codebook["zdr_empl"];
                        $local_pc->nez_empl = $codebook_paycheck_codebook["nez_empl"];
                        $local_pc->tax_empl = $codebook_paycheck_codebook["tax_empl"];
                        $local_pc->tax_release = $codebook_paycheck_codebook["tax_release"];
                        $local_pc->pio_comp = $codebook_paycheck_codebook["pio_comp"];
                        $local_pc->zdr_comp = $codebook_paycheck_codebook["zdr_comp"];
                        $local_pc->nez_comp = $codebook_paycheck_codebook["nez_comp"];
                        $local_pc->min_contribution_base = $codebook_paycheck_codebook["min_contribution_base"];
                        $local_pc->max_contribution_base = $codebook_paycheck_codebook["max_contribution_base"];
                        $local_pc->save(false);
                    }   
                }

                $codebook_unique_fields[] = $codebook_paycheck_codebook["start_use"];
            }       

            $local_pcs = $model_name::model()->findAll(new SIMADbCriteria([
                'Model' => $model_name,
                'model_filter' => [
                    'filter_scopes' => [
                        'withoutUniqueFields' => [$codebook_unique_fields]
                    ]
                ]
            ]));   
            foreach ($local_pcs as $local_pc)
            {
                $local_pc->delete(false);
            }
            PaychecksCodebookComponent::validateModels($codebook_paychecks_codebook);
        }
        catch(Exception $e)
        {
            $transaction->rollback();
            throw $e;
        }
        $transaction->commit();
    }
    
    private static function validateModels($codebook_paychecks_codebook) 
    {
        foreach ($codebook_paychecks_codebook as $codebook_paycheck_codebook) 
        {
            $local_pc = PaycheckCodebook::model()->findByAttributes([
                'start_use' => $codebook_paycheck_codebook['start_use']
            ]);
            if (!empty($local_pc))
            {
                if (!$local_pc->validate()) 
                {
                    $error_msg = SIMAHtml::showErrorsInHTML($local_pc);                              
                    throw new SIMAWarnException($error_msg);
                }
            }           
        }
    }
    
    public static function checkHasPaychecksCodebookChanged() 
    {
        $codebook_paychecks_codebook = AdminController::getDataFromCodebook(Yii::app()->params['codebook_base'].AdminController::$codebook_const_path_part.'gePaychecksCodebookFromCodebook');
        $changes = [];
        $codebook_unique_fields = [];
        $start_use_column_display = Yii::t('PaychecksModule.PaycheckCodebook', 'StartUse');
        $end_use_column_display = Yii::t('PaychecksModule.PaycheckCodebook', 'EndUse');
        $pio_empl_column_display = Yii::t('PaychecksModule.PaycheckCodebook', 'PioEmpl');
        $zdr_empl_column_display = Yii::t('PaychecksModule.PaycheckCodebook', 'ZdrEmpl');
        $nez_empl_column_display = Yii::t('PaychecksModule.PaycheckCodebook', 'NezEmpl');
        $tax_empl_column_display = Yii::t('PaychecksModule.PaycheckCodebook', 'TaxEmpl');
        $tax_release_column_display = Yii::t('PaychecksModule.PaycheckCodebook', 'TaxRelease');
        $pio_comp_column_display = Yii::t('PaychecksModule.PaycheckCodebook', 'PioComp');
        $zdr_comp_column_display = Yii::t('PaychecksModule.PaycheckCodebook', 'ZdrComp');
        $nez_comp_column_display = Yii::t('PaychecksModule.PaycheckCodebook', 'NezComp');
        $min_contribution_base_column_display = Yii::t('PaychecksModule.PaycheckCodebook', 'MinTaxBase');
        $max_contribution_base_column_display = Yii::t('PaychecksModule.PaycheckCodebook', 'MaxTaxBase');
        $old_value = Yii::t('PaychecksModule.PaycheckCodebook', 'OldValue');
        
        foreach ($codebook_paychecks_codebook as $codebook_paycheck_codebook)
        {
            $codebook_unique_fields[] = $codebook_paycheck_codebook["start_use"];
            $local_pc = PaycheckCodebook::model()->findByAttributes([
                'start_use' => $codebook_paycheck_codebook['start_use']
            ]);
            if (empty($local_pc))
            {
                $changes[] = 
                    "<div><strong>".Yii::t('PaychecksModule.PaycheckCodebook', 'NewPaycheckPerion').SIMAHtml::DbToUserDate($codebook_paycheck_codebook["start_use"])
                    ." - ".SIMAHtml::DbToUserDate($codebook_paycheck_codebook["end_use"])."</strong></div>".
                    "<div>".$pio_empl_column_display.": ".$codebook_paycheck_codebook["pio_empl"]."</div>".
                    "<div>".$zdr_empl_column_display.": ".$codebook_paycheck_codebook["zdr_empl"]."</div>".
                    "<div>".$nez_empl_column_display.": ".$codebook_paycheck_codebook["nez_empl"]."</div>".
                    "<div>".$tax_empl_column_display.": ".$codebook_paycheck_codebook["tax_empl"]."</div>".
                    "<div>".$tax_release_column_display.": ".$codebook_paycheck_codebook["tax_release"]."</div>".
                    "<div>".$pio_comp_column_display.": ".$codebook_paycheck_codebook["pio_comp"]."</div>".
                    "<div>".$zdr_comp_column_display.": ".$codebook_paycheck_codebook["zdr_comp"]."</div>".
                    "<div>".$nez_comp_column_display.": ".$codebook_paycheck_codebook["nez_comp"]."</div>".
                    "<div>".$min_contribution_base_column_display.": ".$codebook_paycheck_codebook["min_contribution_base"]."</div>".
                    "<div>".$max_contribution_base_column_display.": ".$codebook_paycheck_codebook["max_contribution_base"]."</div>";
            }
            else
            {
                $change = "";
                    
                if($codebook_paycheck_codebook['start_use'] !== $local_pc->getAttribute('start_use'))
                {
                    $change .= "<div>".$start_use_column_display.": ".SIMAHtml::DbToUserDate($codebook_paycheck_codebook["start_use"])." ({$old_value} {$local_pc->getAttributeDisplay('start_use')})</div>";
                }
                if($codebook_paycheck_codebook['end_use'] !== $local_pc->getAttribute('end_use'))
                {
                    $change .= "<div>".$end_use_column_display.": ".SIMAHtml::DbToUserDate($codebook_paycheck_codebook["end_use"])." ({$old_value} {$local_pc->getAttributeDisplay('end_use')})</div>";
                }
                if(doubleval($codebook_paycheck_codebook['pio_empl']) !== $local_pc->pio_empl)
                {
                    $change .= "<div>".$pio_empl_column_display.": ".$codebook_paycheck_codebook["pio_empl"]." ({$old_value} {$local_pc->getAttributeDisplay('pio_empl')})</div>";
                }
                if(doubleval($codebook_paycheck_codebook['zdr_empl']) !== $local_pc->zdr_empl)
                {
                    $change .= "<div>".$zdr_empl_column_display.": ".$codebook_paycheck_codebook["zdr_empl"]." ({$old_value} {$local_pc->getAttributeDisplay('zdr_empl')})</div>";
                }
                if(doubleval($codebook_paycheck_codebook['nez_empl']) !== $local_pc->nez_empl)
                {
                    $change .= "<div>".$nez_empl_column_display.": ".$codebook_paycheck_codebook["nez_empl"]." ({$old_value} {$local_pc->getAttributeDisplay('nez_empl')})</div>";
                }
                if(doubleval($codebook_paycheck_codebook['tax_empl']) !== $local_pc->tax_empl)
                {
                    $change .= "<div>".$tax_empl_column_display.": ".$codebook_paycheck_codebook["tax_empl"]." ({$old_value} {$local_pc->getAttributeDisplay('tax_empl')})</div>";
                }
                if(doubleval($codebook_paycheck_codebook['tax_release']) !== $local_pc->tax_release)
                {
                    $change .= "<div>".$tax_release_column_display.": ".$codebook_paycheck_codebook["tax_release"]." ({$old_value} {$local_pc->getAttributeDisplay('tax_release')})</div>";
                }
                if(doubleval($codebook_paycheck_codebook['pio_comp']) !== $local_pc->pio_comp)
                {
                    $change .= "<div>".$pio_comp_column_display.": ".$codebook_paycheck_codebook["pio_comp"]." ({$old_value} {$local_pc->getAttributeDisplay('pio_comp')})</div>";
                }
                if(doubleval($codebook_paycheck_codebook['zdr_comp']) !== $local_pc->zdr_comp)
                {
                    $change .= "<div>".$zdr_comp_column_display.": ".$codebook_paycheck_codebook["zdr_comp"]." ({$old_value} {$local_pc->getAttributeDisplay('zdr_comp')})</div>";
                }
                if(doubleval($codebook_paycheck_codebook['nez_comp']) !== $local_pc->nez_comp)
                {
                    $change .= "<div>".$nez_comp_column_display.": ".$codebook_paycheck_codebook["nez_comp"]." ({$old_value} {$local_pc->getAttributeDisplay('nez_comp')})</div>";
                }
                if(doubleval($codebook_paycheck_codebook['min_contribution_base']) !== $local_pc->min_contribution_base)
                {
                    $change .= "<div>".$min_contribution_base_column_display.": ".$codebook_paycheck_codebook["min_contribution_base"]." ({$old_value} {$local_pc->getAttributeDisplay('min_contribution_base')})</div>";
                }
                if(doubleval($codebook_paycheck_codebook['max_contribution_base']) !== $local_pc->max_contribution_base)
                {
                    $change .= "<div>".$max_contribution_base_column_display.": ".$codebook_paycheck_codebook["max_contribution_base"]." ({$old_value} {$local_pc->getAttributeDisplay('max_contribution_base')})</div>";
                }
                if (!empty($change))
                {
                    $change = "<div><strong>".Yii::t('PaychecksModule.PaycheckCodebook', 'PeycheckPerion')."{$local_pc->getAttributeDisplay('start_use')} - {$local_pc->getAttributeDisplay('end_use')}</strong><br>" . $change;
                    $changes[] = $change;
                }
                
            }
        }
        $local_pcs = PaycheckCodebook::model()->findAll(new SIMADbCriteria([
            'Model' => PaycheckCodebook::class,
            'model_filter' => [
                'filter_scopes' => [
                    'withoutUniqueFields' => [$codebook_unique_fields]
                ]
            ]
        ]));
        if(count($local_pcs))
        {
            $sync_type = Yii::app()->configManager->get('accounting.paychecks.paycheck_codebook_sync_type', false);
            $remove_msg = Yii::t('PaychecksModule.PaycheckCodebook', 'ForRemove');
            if($sync_type === 'automatic_synchronization_with_notification')
            {
                $remove_msg = Yii::t('PaychecksModule.PaycheckCodebook', 'Removed');
            }
            $change .= "<br><div><strong>".$remove_msg."</strong></div>";
            foreach ($local_pcs as $local_pc)
            {
                $change .= "<div>".$local_pc->getAttribute('start_use')." - ".$local_pc->getAttribute('end_use')."</div>";
            }
            $changes[] = $change;
        }
            
        return $changes;
    }
}

