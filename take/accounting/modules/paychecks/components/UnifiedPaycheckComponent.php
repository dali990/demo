<?php

class UnifiedPaycheckComponent
{
    /**
     * 
     * @param PaycheckPeriod $paycheckPeriod
     * @param array $unifiedPaychecks - $key je id unifiedpaycheck-a, a $value niz paycheck-ova
     */
    public static function generatePayments(PaycheckPeriod $paycheckPeriod, array $unifiedPaychecks)
    {
        $payments = [];
                
        foreach($unifiedPaychecks as $key => $value)
        {            
            if(count($value) > 0)
            {                
                $criteria = new SIMADbCriteria([
                    'Model' => 'UnifiedPaycheck',
                    'model_filter' => [
                        'ids' => $key
                    ]
                ]);
                
                $unifiedPaycheck = UnifiedPaycheck::model()->find($criteria);
                
                $payments[] = UnifiedPaycheckComponent::generatePayment($paycheckPeriod, $unifiedPaycheck, $value);
            }
        }
                
        return $payments;
    }
    
    /**
     * 
     * @param PaycheckPeriod $paycheckPeriod
     * @param UnifiedPaycheck $unifiedPaycheck
     * @param array $paychecks
     */
    public static function generatePayment(PaycheckPeriod $paycheckPeriod, UnifiedPaycheck $unifiedPaycheck, array $paychecks)
    {        
        $payment_amount_neto = 0;
        foreach($paychecks as $paycheck)
        {
            $payment_amount_neto += $paycheck->amount_neto_for_payout;
        }
        
        $new_payment = new Payment();
        $new_payment->payment_date = $paycheckPeriod->payment_date;
        $new_payment->partner_id = $unifiedPaycheck->bank->id;
        $new_payment->account_id = $unifiedPaycheck->bank_account->id;
        $new_payment->amount = $payment_amount_neto;
        $new_payment->invoice = false;
        // ovaj treba biti 240 (default za isplacivanje radniku), 
        // treba dodati u conf za 241 (neoporeziva zarada) i 254 (porez po zaradi)
        $new_payment->payment_code_id = Yii::app()->configManager->get('accounting.paychecks.employee_paycheck_payment_code');
        $new_payment->payment_type_id = Yii::app()->configManager->get('accounting.paycheck_payment_type');
        $new_payment->save();
                
        foreach($paychecks as $paycheck)
        {
            $paycheck->payment_id = $new_payment->id;
            $paycheck->save();
        }
                        
        return $new_payment;
    }
}

