<?php

class PaycheckCalculation
{
    public static function recalculateAdditionalsForPaycheck(Paycheck $paycheck, array $employee_params, $withAdditionalsRecalc=false, $taxRelease=0, Month $month)
    {        
        $_additionals = [];
        
        /// godisnji odmor
        $_holiday1 = PaycheckCalculation::recalculatePaycheckAnualLeave($paycheck, $employee_params, $taxRelease, $month);
        if (!is_null($_holiday1))
        {
            $_additionals[] = $_holiday1;
        }
        
        /// bolovanje
        $_additionals = array_merge(
            $_additionals,
            PaycheckCalculation::recalculatePaycheckSickLeave($paycheck, $employee_params, $taxRelease, $month)
        );
        
        /// drzavni i verski praznik
        $_holiday3 = PaycheckCalculation::recalculatePaycheckStateAndReligiousHoliday($paycheck, $employee_params, $taxRelease, $month);
        if (!is_null($_holiday3))
        {
            $_additionals[] = $_holiday3;
        }

        /// placeno odsustvo
        $_holiday4 = PaycheckCalculation::recalculatePaycheckPaidLeave($paycheck, $employee_params, $taxRelease, $month);
        if (!is_null($_holiday4))
        {
            $_additionals[] = $_holiday4;
        }
        
        /// placeno odsustvo vojna sluzba
        $_holiday5 = PaycheckCalculation::recalculatePaycheckPaidLeaveMilitaryService($paycheck, $employee_params, $taxRelease, $month);
        if (!is_null($_holiday5))
        {
            $_additionals[] = $_holiday5;
        }
        
        return $_additionals;
    }
    
    public static function recalculateSuspensions($paycheck, array $employee_params, $month)
    {
        /**
         * TODO: MilosJ 20170927
         * treba revidirati da li je ovo najbolji nacin
         * mozda iskoristiti paychecks/config/main.php sto neki imaju for_payout=false
         */
        $have_sick_leave_whole_month = false;
        foreach($paycheck->additionals as $additional)
        {
            if(
                $additional->type === PaycheckAdditional::$TYPE_SICK_LEAVE_PREGNANCY_OVER_30
                ||
                (
                        $additional->type === PaycheckAdditional::$TYPE_SICK_LEAVE_OVER_30
                        && 
                        (int)($additional->hours/8) === (int)($month->countWorkDays())
                        
                )
            )
            {
                $have_sick_leave_whole_month = true;
            }
        }
        
        $_paycheckSuspension = PaycheckSuspension::model()->findAllByAttributes([
            'paycheck_id' => $paycheck->id
        ]);
        
        if($have_sick_leave_whole_month || $paycheck->paycheck_period->isBonus)
        {
            /// TODO - brzi fix
            return [
                'new' => [],
                'delete' => $_paycheckSuspension
            ];
        }
        
        $newPaycheckSuspensions = [];
        
        $suspensionsCriteria = new SIMADbCriteria([
            'Model' => Suspension::model(),
            'model_filter' => [
                'employee' => [
                    'ids' => $employee_params['emp_model']->id
                ],
                'left_to_pay_amount' => '>SEPARATOR0',
                'begin_month' => [
                    'period' => [
                        '<=', $month
                    ]
                ]
            ]
        ]);
        
        $suspensions = Suspension::model()->findAll($suspensionsCriteria);
        
        foreach($suspensions as $suspension)
        {
            $amount = 1;
            switch ($suspension->payment_type)
            {
                case Suspension::$TYPE_AUTO_ALL:
                    $amount = $suspension->left_to_pay_amount;
                    break;
                case Suspension::$TYPE_AUTO_INSTALLMENT:
                    $amount = min([$suspension->left_to_pay_amount, $suspension->installment_amount]);
                    break;
                case Suspension::$TYPE_ARBITRARLY:
                    $amount = 1;
                    break;
            }
            $paycheckSuspension = null;
            foreach ($_paycheckSuspension as $_i => $_ps) 
            {
                if ($_ps->suspension_id === $suspension->id)
                {
                    $paycheckSuspension = $_ps;
                    unset($_paycheckSuspension[$_i]);
                    break;
                }
            }
            if (is_null($paycheckSuspension))
            {
                $paycheckSuspension = new PaycheckSuspension();
                $paycheckSuspension->paycheck_id = $paycheck->id;
                $paycheckSuspension->suspension_id = $suspension->id;
                $paycheckSuspension->amount = $amount;
            }

            $newPaycheckSuspensions[] = $paycheckSuspension;
        }
        
        return [
            'new' => $newPaycheckSuspensions,
            'delete' => $_paycheckSuspension
        ];
    }
    
    private static function calculateAdditionalDoprinosiBase(Month $month, Paycheck $paycheck, $bruto, $proportion_fix)
    {
        $doprinosi_base = null;
        
//        if(PaychecksCodebookComponent::useNewCodebooks())
//        {
            $min_contribution_base_proportion = $paycheck->paycheck_period->codebook->min_contribution_base * $proportion_fix;
            $max_contribution_base_proportion = $paycheck->paycheck_period->codebook->max_contribution_base * $proportion_fix;
            $doprinosi_base = $bruto * $proportion_fix;

            if($doprinosi_base < $min_contribution_base_proportion)
            {
                $doprinosi_base = $min_contribution_base_proportion;
            }
            else if($doprinosi_base > $max_contribution_base_proportion)
            {
                $doprinosi_base = $max_contribution_base_proportion;
            }
//        }
//        else
//        {
//            $doprinosi_base = $month->param('accounting.min_contribution_base') * $proportion_fix;
//        }
        
        return $doprinosi_base;
    }
    
    private static function recalculatePaycheckAnualLeave(Paycheck $paycheck, array $employee_params, $taxRelease, Month $month)
    {          

        $anual_leaves = PaycheckCalculation::getPaycheckAnnualLeaves($paycheck);

        if(empty($anual_leaves))
        {
            return null;
        }
        else
        {
            $first_work_contract = $employee_params['work_contracts'][0];
            $first_work_contract_working_percent = $first_work_contract->working_percent;
            $value_per_hour = $employee_params['last_12_months_value_per_hour'];
            $number_of_calendar_days = 0;
            foreach($anual_leaves as $al)
            {
                $number_of_calendar_days += $al->GetNumberOfActiveWorkDaysInMonth($month);
            }
            $hours_per_day = 8; //// treba ispraviti da cita parametar koliko je radni dan sati
            $number_work_hours = $number_of_calendar_days * $hours_per_day * $first_work_contract_working_percent/100; 
            $bruto = $number_work_hours * $value_per_hour * $paycheck->paycheck_period->hours_percentage/100;

            $proportion_fix = $number_work_hours/$paycheck->paycheck_period->month_hours;
            
            $tax_release = $taxRelease * $proportion_fix;
            if(PaychecksCodebookComponent::useMaxTaxBase())
            {
                $doprinosi_base = PaycheckCalculation::calculateAdditionalDoprinosiBase($month, $paycheck, $bruto, $proportion_fix);
            }
            else
            {
                $doprinosi_base = null;
//                if(PaychecksCodebookComponent::useNewCodebooks())
//                {
                    $doprinosi_base = $paycheck->paycheck_period->codebook->min_contribution_base * $proportion_fix;
//                }
//                else
//                {
//                    $doprinosi_base = $month->param('accounting.min_contribution_base') * $proportion_fix;
//                }
            }
            
            $svp = $employee_params['emp_model']->GetSVP();
            //TODO: srediti SVP i tipove additionals
//            if(isset($type['default_svp']))
//            {
//                $svp = '1'.$employee_params['emp_model']->employment_code.$type['default_svp'];
//            }

            $anual_leave_additional = new PaycheckAdditional();
            $anual_leave_additional->hours = $number_work_hours;
            $anual_leave_additional->paycheck_id = $paycheck->id;
            $anual_leave_additional->type = PaycheckAdditional::$TYPE_ANNUAL_LEAVE;
            $anual_leave_additional->svp = $svp;
            $anual_leave_additional->number_of_calendar_days = $number_of_calendar_days;
            $anual_leave_additional->setValues($bruto, $tax_release, $doprinosi_base);
            return $anual_leave_additional;
        }
    }
    
    private static function recalculatePaycheckSickLeave(Paycheck $paycheck, array $employee_params, $taxRelease, Month $month)
    {
        $_results = [];
        $paycheck_additional_types = Yii::app()->params['paycheck_additional_types_formated'];
                
        $typeDatas = AbsenceSickLeave::getSubTypeData();
        $typeDatasKeys = array_keys($typeDatas);
        foreach($typeDatasKeys as $tdk)
        {
            if(!isset($paycheck_additional_types[$tdk]))
            {
                throw new Exception('NoPaycheckAdditionalTypeInSickLeaves');
            }
            
            $_result = PaycheckCalculation::recalculatePaycheckSickLeaveType(
                    $paycheck_additional_types[$tdk], $paycheck, $employee_params, 
                    $taxRelease, $month
            );
            if (!is_null($_result))
            {
                $_results[] = $_result;
            }
        }
        
        unset($paycheck_additional_types);
        unset($typeDatas);
        unset($typeDatasKeys);
        return $_results;
    }
    
    private static function recalculatePaycheckSickLeaveType(array $type, Paycheck $paycheck, array $employee_params, $taxRelease, Month $month)
    {
        $criteria = new SIMADbCriteria([
            'Model' => AbsenceSickLeave::model(),
            'model_filter' => [
                'confirmed' => true,
                'employee' => [
                    'ids' => $employee_params['emp_model']->id
                ],
                'subtype' => $type['value'],
                'absent_on_date' => $month
            ]
        ]);

        $sick_leaves = AbsenceSickLeave::model()->findAll($criteria);

        if(count($sick_leaves) === 0)
        {
            return null;
        }
        else
        {
            $number_of_calendar_days = 0;
            $number_of_work_calendar_days = 0;
            $neto_sum = 0;
                        
            foreach($sick_leaves as $sl)
            {
                if(!empty($sl->neto))
                {
                    $neto_sum += $sl->neto;
                }
                
                $number_of_calendar_days += $sl->GetNumberOfCalendarDaysInMonth($month);
                $number_of_work_calendar_days += $sl->GetNumberOfActiveWorkDaysInMonth($month);
            }
            
            $first_work_contract = $employee_params['work_contracts'][0];
            $first_work_contract_working_percent = $first_work_contract->working_percent;
            
            $hours_per_day = 8; //// treba ispraviti da cita parametar koliko je radni dan sati
            
            $number_work_hours = $number_of_work_calendar_days * $hours_per_day * $first_work_contract_working_percent/100;
            
            $value_per_hour = $employee_params['last_12_months_value_per_hour'];
            
            $bruto = $number_work_hours * $value_per_hour * $paycheck->paycheck_period->hours_percentage/100;
            $neto_sum *= $paycheck->paycheck_period->hours_percentage/100;
                        
            $hours_percentage = $type['percentage'];
            $bruto *= $hours_percentage/100;
            
            $proportion_fix = $number_work_hours/$paycheck->paycheck_period->month_hours;

            $tax_release = $taxRelease * $proportion_fix;
            $doprinosi_base = null;
            if(PaychecksCodebookComponent::useMaxTaxBase())
            {
                $doprinosi_base = PaycheckCalculation::calculateAdditionalDoprinosiBase($month, $paycheck, $bruto, $proportion_fix);
            }
            else
            {
//                if(PaychecksCodebookComponent::useNewCodebooks())
//                {
                    $doprinosi_base = $paycheck->paycheck_period->codebook->min_contribution_base * $proportion_fix;
//                }
//                else
//                {
//                    $doprinosi_base = $month->param('accounting.min_contribution_base') * $proportion_fix;
//                }
            }

            $svp = $employee_params['emp_model']->GetSVP();
            if(isset($type['default_svp']))
            {
                $svp = '1'.$employee_params['emp_model']->employment_code.$type['default_svp'];
            }

            $sick_leave_additional = new PaycheckAdditional();
            $sick_leave_additional->hours = $number_work_hours;
            $sick_leave_additional->paycheck_id = $paycheck->id;
            $sick_leave_additional->type = $type['value'];
            $sick_leave_additional->hours_percentage = $hours_percentage;
            $sick_leave_additional->number_of_calendar_days = $number_of_calendar_days;
            $sick_leave_additional->svp = $svp;

            if($neto_sum > 0)
            {
                if(floatval($proportion_fix) !== floatval(1))
                {
                    Yii::app()->raiseNote(Yii::t('PaychecksModule.Paycheck', 'SickLeaveWithNetoNotWholeMonthRequireManualSet', [
                        '{emp}' => $employee_params['emp_model']
                    ]));
                    $sick_leave_additional->confirmed = false;
                }
                
                $sick_leave_additional->setValuesByNeto($neto_sum, $tax_release, $doprinosi_base);
            }
            else
            {
                $sick_leave_additional->setValues($bruto, $tax_release, $doprinosi_base, true);
            }
            
            unset($criteria);
            unset($sick_leaves);
            unset($value_per_hour);
            unset($number_work_hours);
            unset($bruto);
            unset($neto_sum);
            unset($tax_release);
                        
            return $sick_leave_additional;
        }
    }
    
    private static function recalculatePaycheckStateAndReligiousHoliday(Paycheck $paycheck, array $employee_params, $taxRelease, Month $month)
    {

        $monthDaysIds = $paycheck->paycheck_period->month->getDaysIds();

        $total_result = array_merge(
                PaycheckCalculation::recalculatePaycheckStateHolidays($monthDaysIds, $employee_params['emp_model']),
                PaycheckCalculation::recalculatePaycheckReligiousHolidays($monthDaysIds, $employee_params['emp_model'])
        );

        if(count($total_result) > 0)
//        {
//            $value_per_hour = $employee_params['last_12_months_value_per_hour'];
//
//            $number_work_hours = count($total_result) * 8; //// treba ispraviti da cita parametar koliko je radni dan sati
//            $bruto = $number_work_hours * $value_per_hour;
//
//            if(Yii::app()->configManager->get('accounting.paychecks.tufe_fix')=='true'
////                    && $paycheck->paycheck_period->ordering_number === 1
//                && isset($employee_params['last_12_months_hours']) && isset($employee_params['last_12_months_value']) 
//                && $employee_params['last_12_months_hours'] > 0 && $employee_params['last_12_months_value']>0)
//            {
//                $bruto *= $paycheck->paycheck_period->hours_percentage/100; /// privremeno zbog uporedjivanja sa tufetom;
//            }
//
//            $worked_hours = $employee_params['work_contracts_hours_sum'];
//            $tax_release_fix = $worked_hours/$paycheck->paycheck_period->month_hours;
//            $tax_release = $taxRelease * $tax_release_fix * ($number_work_hours/$worked_hours);
//            $svp = $employee_params['emp_model']->GetSVP();
//            if(isset($type['default_svp']))
//            {
//                $svp = '1'.$employee_params['emp_model']->employment_code.$type['default_svp'];
//            }
//
//            $state_and_religious_holiday_additional = new PaycheckAdditional();
//            $state_and_religious_holiday_additional->hours = $number_work_hours;
//            $state_and_religious_holiday_additional->paycheck_id = $paycheck->id;
//            $state_and_religious_holiday_additional->type = PaycheckAdditional::$TYPE_STATE_AND_RELIGIOUS_HOLIDAY;
//            $state_and_religious_holiday_additional->svp = $svp;
//            $state_and_religious_holiday_additional->setValues($bruto, $tax_release, $bruto);
//
//
//
//            unset($value_per_hour);
//            unset($number_work_hours);
//            unset($bruto);
//            unset($worked_hours);
//            unset($tax_release_fix);
//            unset($tax_release);
////                $state_and_religious_holiday_additional->unsetAttributes();
////                unset($state_and_religious_holiday_additional);
//
//
//            return $state_and_religious_holiday_additional;
//        }
        {
            $value_per_hour = $employee_params['last_12_months_value_per_hour'];
            $first_work_contract = $employee_params['work_contracts'][0];
            $first_work_contract_working_percent = $first_work_contract->working_percent;
            
            $hours_per_day = 8; //// treba ispraviti da cita parametar koliko je radni dan sati
            
            $number_of_calendar_days = count($total_result);

            $number_work_hours = $number_of_calendar_days * $hours_per_day * $first_work_contract_working_percent/100;
            $bruto = $number_work_hours * $value_per_hour * $paycheck->paycheck_period->hours_percentage/100; /// privremeno zbog uporedjivanja sa tufetom;

            $proportion_fix = $number_work_hours/$paycheck->paycheck_period->month_hours;
            
            $tax_release = $taxRelease * $proportion_fix;
            $doprinosi_base = null;
            if(PaychecksCodebookComponent::useMaxTaxBase())
            {
                $doprinosi_base = PaycheckCalculation::calculateAdditionalDoprinosiBase($month, $paycheck, $bruto, $proportion_fix);
            }
            else
            {
//                if(PaychecksCodebookComponent::useNewCodebooks())
//                {
                    $doprinosi_base = $paycheck->paycheck_period->codebook->min_contribution_base * $proportion_fix;
//                }
//                else
//                {
//                    $doprinosi_base = $month->param('accounting.min_contribution_base') * $proportion_fix;
//                }
            }

            $svp = $employee_params['emp_model']->GetSVP();
//            if(isset($type['default_svp']))
//            {
//                $svp = '1'.$employee_params['emp_model']->employment_code.$type['default_svp'];
//            }

            $state_and_religious_holiday_additional = new PaycheckAdditional();
            $state_and_religious_holiday_additional->hours = $number_work_hours;
            $state_and_religious_holiday_additional->paycheck_id = $paycheck->id;
            $state_and_religious_holiday_additional->type = PaycheckAdditional::$TYPE_STATE_AND_RELIGIOUS_HOLIDAY;
            $state_and_religious_holiday_additional->svp = $svp;
            $state_and_religious_holiday_additional->number_of_calendar_days = $number_of_calendar_days;
            $state_and_religious_holiday_additional->setValues($bruto, $tax_release, $doprinosi_base);



            unset($value_per_hour);
            unset($number_work_hours);
            unset($bruto);
//            unset($worked_hours);
//            unset($tax_release_fix);
            unset($tax_release);
//                $state_and_religious_holiday_additional->unsetAttributes();
//                unset($state_and_religious_holiday_additional);


            return $state_and_religious_holiday_additional;
        }
        else
        {
            return null;
        }

        unset($paycheck->paycheck_period->month);
        unset($paycheck->paycheck_period);
        unset($monthDaysIds);
        
    }
    
    private static function recalculatePaycheckPaidLeave(Paycheck $paycheck, array $employee_params, $taxRelease, Month $month)
    {

        $firstDay = $paycheck->paycheck_period->month->firstDay();
        $lastDay = $paycheck->paycheck_period->month->lastDay();

        $criteria = new SIMADbCriteria([
            'Model' => Absence::model(),
            'model_filter' => [
                'employee' => [
                    'ids' => $paycheck->employee->id
                ],
                'type' => Absence::$FREE_DAYS,
                'confirmed' => true,
                'absent_on_date' => $firstDay->day_date.'<>'.$lastDay->day_date,
                'scopes' => [
                    'not_military_service'
                ]
            ]
        ]);

        $paid_leaves = Absence::model()->findAll($criteria);

        if(count($paid_leaves) === 0)
        {
            return;
        }

        $absent_days = [];

        foreach($paid_leaves as $pl)
        {
            if($pl->number_work_days === 0)
            {
                continue;
            }

            $begin_day = Day::getByDate($pl->begin);
            $end_day = Day::getByDate($pl->end);

            while($begin_day->compare($end_day) <= 0)
            {
                if(!$begin_day->IsWeekend)
                {
                    $absent_days[$begin_day->day_date] = true;
                }
                $begin_day = $begin_day->next();
            }
        }

        $number_of_absent_days = count($absent_days);
        if($number_of_absent_days > 0)
        {
            $value_per_hour = $employee_params['last_12_months_value_per_hour'];
            $first_work_contract = $employee_params['work_contracts'][0];
            $first_work_contract_working_percent = $first_work_contract->working_percent;
            $number_work_hours = $number_of_absent_days * 8 * $first_work_contract_working_percent/100;
            $bruto = $number_work_hours * $value_per_hour * $paycheck->paycheck_period->hours_percentage/100;

            $proportion_fix = $number_work_hours/$paycheck->paycheck_period->month_hours;

            $tax_release = $taxRelease * $proportion_fix;
            $doprinosi_base = null;
            if(PaychecksCodebookComponent::useMaxTaxBase())
            {
                $doprinosi_base = PaycheckCalculation::calculateAdditionalDoprinosiBase($month, $paycheck, $bruto, $proportion_fix);
            }
            else
            {
//                if(PaychecksCodebookComponent::useNewCodebooks())
//                {
                    $doprinosi_base = $paycheck->paycheck_period->codebook->min_contribution_base * $proportion_fix;
//                }
//                else
//                {
//                    $doprinosi_base = $month->param('accounting.min_contribution_base') * $proportion_fix;
//                }
            }

            $svp = $employee_params['emp_model']->GetSVP();
//                if(isset($type['default_svp']))
//                {
//                    $svp = '1'.$employee_params['emp_model']->employment_code.$type['default_svp'];
//                }

            $paid_leave_additional = new PaycheckAdditional();
            $paid_leave_additional->hours = $number_work_hours;
            $paid_leave_additional->paycheck_id = $paycheck->id;
            $paid_leave_additional->type = PaycheckAdditional::$TYPE_PAID_LEAVE;
            $paid_leave_additional->svp = $svp;
            $paid_leave_additional->number_of_calendar_days = $number_of_absent_days;
            $paid_leave_additional->setValues($bruto, $tax_release, $doprinosi_base);

            return $paid_leave_additional;
        }
        else
        {
            return null;
        }
    }
    
    private static function recalculatePaycheckPaidLeaveMilitaryService(Paycheck $paycheck, array $employee_params, $taxRelease, Month $month)
    {

        $firstDay = $paycheck->paycheck_period->month->firstDay();
        $lastDay = $paycheck->paycheck_period->month->lastDay();

        $criteria = new SIMADbCriteria([
            'Model' => Absence::model(),
            'model_filter' => [
                'employee' => [
                    'ids' => $paycheck->employee->id
                ],
                'type' => Absence::$FREE_DAYS,
                'subtype' => AbsenceFreeDays::$SUBTYPE_PAID_LEAVE_MILITARY_SERVICE,
                'confirmed' => true,
                'absent_on_date' => $firstDay->day_date.'<>'.$lastDay->day_date
            ]
        ]);

        $paid_leaves = Absence::model()->findAll($criteria);

        if(count($paid_leaves) === 0)
        {
            return;
        }

        $absent_days = [];

        foreach($paid_leaves as $pl)
        {
            if($pl->number_work_days === 0)
            {
                continue;
            }

            $begin_day = Day::getByDate($pl->begin);
            $end_day = Day::getByDate($pl->end);

            while($begin_day->compare($end_day) <= 0)
            {
                if(!$begin_day->IsWeekend)
                {
                    $absent_days[$begin_day->day_date] = true;
                }
                $begin_day = $begin_day->next();
            }
        }

        $number_of_absent_days = count($absent_days);
        if($number_of_absent_days > 0)
        {
            $value_per_hour = $employee_params['last_12_months_value_per_hour'];
            $first_work_contract = $employee_params['work_contracts'][0];
            $first_work_contract_working_percent = $first_work_contract->working_percent;
            $number_work_hours = $number_of_absent_days * 8 * $first_work_contract_working_percent/100;
            $bruto = $number_work_hours * $value_per_hour * $paycheck->paycheck_period->hours_percentage/100;

            $proportion_fix = $number_work_hours/$paycheck->paycheck_period->month_hours;

            $tax_release = $taxRelease * $proportion_fix;
            $doprinosi_base = null;
            if(PaychecksCodebookComponent::useMaxTaxBase())
            {
                $doprinosi_base = PaycheckCalculation::calculateAdditionalDoprinosiBase($month, $paycheck, $bruto, $proportion_fix);
            }
            else
            {
//                if(PaychecksCodebookComponent::useNewCodebooks())
//                {
                    $doprinosi_base = $paycheck->paycheck_period->codebook->min_contribution_base * $proportion_fix;
//                }
//                else
//                {
//                    $doprinosi_base = $month->param('accounting.min_contribution_base') * $proportion_fix;
//                }
            }

            $svp = $employee_params['emp_model']->GetSVP();
//                if(isset($type['default_svp']))
//                {
//                    $svp = '1'.$employee_params['emp_model']->employment_code.$type['default_svp'];
//                }

            $paid_leave_additional = new PaycheckAdditional();
            $paid_leave_additional->hours = $number_work_hours;
            $paid_leave_additional->paycheck_id = $paycheck->id;
            $paid_leave_additional->type = PaycheckAdditional::$TYPE_PAID_LEAVE_MILITARY_SERVICE;
            $paid_leave_additional->svp = $svp;
            $paid_leave_additional->number_of_calendar_days = $number_of_absent_days;
            $paid_leave_additional->setValues($bruto, $tax_release, $doprinosi_base);

            return $paid_leave_additional;
        }
        else
        {
            return null;
        }
    }

    private static function recalculatePaycheckStateHolidays(array $monthDaysIds, Employee $emp)
    {
        $result = [];
                
        $criteria = new SIMADbCriteria([
            'Model' => NationalEventToDayYear::model(),
            'model_filter' => [
                'national_event' => [
                    'type' => NationalEvent::$TYPE_STATE_HOLIDAY
                ],
                'day' => [
                    'ids' => $monthDaysIds
                ]
            ]
        ]);

        $state_holidays = NationalEventToDayYear::model()->findAll($criteria);
        
        unset($criteria);
        
        foreach($state_holidays as $sh)
        {
            if(!$sh->day->IsWeekend 
                    && $emp->isActiveAt($sh->day, true) 
                    && !$emp->isAbsentOnDate($sh->day, AbsenceSickLeave::$SICK_LEAVE))
            {
                $result[$sh->day->id] = $sh->day;
            }
            
            unset($sh->day);
        }
        
        unset($state_holidays);
        
        return $result;
    }
    
    private static function recalculatePaycheckReligiousHolidays(array $monthDaysIds, Employee $emp)
    {
        $result = [];
                
        $criteria = new SIMADbCriteria([
            'Model' => NationalEventToDayYear::model(),
            'model_filter' => [
                'national_event' => [
                    'type' => NationalEvent::$TYPE_RELIGIOUS_HOLIDAY
                ],
                'day' => [
                    'ids' => $monthDaysIds
                ]
            ]
        ]);

        $state_holidays = NationalEventToDayYear::model()->findAll($criteria);
        
        unset($criteria);
        
        foreach($state_holidays as $sh)
        {
            if(!$sh->day->IsWeekend && $emp->isActiveAt($sh->day, true)
                    && !$emp->isAbsentOnDate($sh->day, AbsenceSickLeave::$SICK_LEAVE))
            {
                $result[$sh->day->id] = $sh->day;
            }
            
            unset($sh->day);
        }
        
        unset($state_holidays);
        
        return $result;
    }
    
    private static function getPaycheckAnnualLeaves(Paycheck $paycheck)
    {
        $firstDay = $paycheck->paycheck_period->month->firstDay();
        $lastDay = $paycheck->paycheck_period->month->lastDay();
        
        $anual_leave_criteria = new SIMADbCriteria([
            'Model' => 'AbsenceAnnualLeave',
            'model_filter' => [
                'employee' => [
                    'ids' => $paycheck->employee->id
                ],
                'absent_on_date' => $firstDay->day_date.'<>'.$lastDay->day_date,
                'confirmed' => true
            ]
        ]);
        $anual_leaves = AbsenceAnnualLeave::model()->findAll($anual_leave_criteria);
        
        return $anual_leaves;
    }
    
    public static function recalculatePaycheckNEW(Paycheck $paycheck, PaycheckPeriod $paycheckPeriod, Month $month, array $employee_params)
    {
//        $pio_empl_percentage = null;
//        $zdr_empl = null;
//        $nez_empl = null;
//        $tax_empl_perc = null;
//        $pio_comp_percentage = null;
//        $zdr_comp = null;
//        $nez_comp = null;
//        if(PaychecksCodebookComponent::useNewCodebooks())
//        {
            $pio_empl_percentage = $paycheck->paycheck_period->codebook->pio_empl;
            $zdr_empl =  $paycheck->paycheck_period->codebook->zdr_empl;
            $nez_empl =  $paycheck->paycheck_period->codebook->nez_empl;
            $tax_empl_perc =  $paycheck->paycheck_period->codebook->tax_empl;
            $pio_comp_percentage =  $paycheck->paycheck_period->codebook->pio_comp;
            $zdr_comp =  $paycheck->paycheck_period->codebook->zdr_comp;
            $nez_comp =  $paycheck->paycheck_period->codebook->nez_comp;
//        }
//        else
//        {
//            $pio_empl_percentage = $month->param('accounting.pio_empl');
//            $zdr_empl = $month->param('accounting.zdr_empl');
//            $nez_empl = $month->param('accounting.nez_empl');
//            $tax_empl_perc = $month->param('accounting.tax_empl');
//            $pio_comp_percentage = $month->param('accounting.pio_comp');
//            $zdr_comp = $month->param('accounting.zdr_comp');
//            $nez_comp = $month->param('accounting.nez_comp');
//        }
        $hot_meal_per_hour = $month->param('accounting.hot_meal_per_hour');
        $regres = $month->param('accounting.regres');
        
        /** PODACI IZ PERIODA */
        $first_work_contract = $employee_params['work_contracts'][0];
        //procenat radnog vremena iz ugovora - MFP.3
        $paycheck->working_percent = $first_work_contract->working_percent;//$period_hours_percentage;working_percent
//        error_log();
        //TODO: dodati ovu opciju i obraditi je!!!
        //procenat rada u drugim kompanijama - potrebno zbog minimalne osnovice
        $paycheck->working_percent_at_other_companies = $first_work_contract->working_percent_at_other_companies;
        
        //procenat zarade koji se isplacuje u ovom delu
        $paycheck->hours_percentage = $paycheckPeriod->hours_percentage * $paycheck->working_percent * 0.01;//$period_hours_percentage;working_percent
        
        $paycheck->work_position_name = $first_work_contract->work_position->DisplayName;
        
        /** OSNOVNI PODACI ZAPOSLENOG */
        $paycheck->employee_display_name = $paycheck->employee->DisplayName;
        $paycheck->employee_payout_type = $paycheck->employee->payout_type;
        $paycheck->employee_svp = $paycheck->employee->GetSVP();
                        
        $estimated_average_hours_in_month = Yii::app()->configManager->get('accounting.paychecks.estimated_average_hours_in_month');
        $paycheck->estimated_average_hours_in_month = $estimated_average_hours_in_month;
        
        $paycheck->paygrade_code = $employee_params['work_contracts'][0]->work_position->paygrade->code;
        
        //procenat za pripravnike
        $probationer_percent = 100;
        if($first_work_contract->is_probationer === true)
        {
            $probationer_percent = Yii::app()->configManager->get('accounting.paychecks.probationer_work_percent');
        }
        
        //poslednjih 12 meseci
        $paycheck->last_12_months_hours = $employee_params['last_12_months_hours'];
        $paycheck->last_12_months_value = $employee_params['last_12_months_value'];
        
        /** DODACI */
        
        $paycheck->populateAdditionals();

        /** OBRACUN REDOVNOG RADA */
        
        /**
         * SRAZMERNI DELOVI
         */
         
        //oduzimamo ignorisane sate da bi tacno utvrdili srazmerne delove
        $month_period_hours = $paycheckPeriod->month_hours;
        $work_contract_hours_percent = 
            ($employee_params['work_contracts_hours_sum'] - $paycheck->additionals_ignored_hours)
            /
            $month_period_hours;
             
        $paycheck->work_contract_hours_percent = $work_contract_hours_percent;

        //TODO: broj sati od zaposlenog!!!
        //IDE SE U ODNOSU NA CEO MESEC
        //broj sati koji zaposleni radi
//        if (PaycheckCalculations::isTufeFix())
//        {
            //ceo mesec bez obzira na prijave
            
//        }
//        else
//        {
//            //samo sati u delu meseca kada je zaposleni prijavljen
//            $month_period_hours = $employee_params['work_contracts_hours_sum'];
//        }
        $paycheck->month_period_hours = $month_period_hours;
        
        //oslobadjanje od poreza koje moze da se iskoristi u obracunu
        $tax_release_for_use = $paycheckPeriod->tax_release_used * $work_contract_hours_percent * 0.01 * $paycheck->working_percent;
        $paycheck->tax_release = $tax_release_for_use;
        
        
        //TODO: ovo treba preracunati u zavisnosti od toga da li je osoba zaposlena i kod drugih firmi
        $dependent_working_percent = 1;
        if (
            !SIMAMisc::areEqual($paycheck->working_percent,100.0) &&
            !SIMAMisc::areEqual($paycheck->working_percent_at_other_companies,0.0))
        {
            $dependent_working_percent = 0.01 * $paycheck->working_percent  / ($paycheck->working_percent + $paycheck->working_percent_at_other_companies);
        }
        
        $paycheck->dependent_working_percent = $dependent_working_percent;
        
        if(PaychecksCodebookComponent::useMaxTaxBase())
        {
            $month_doprinosi_base = null;
//            if(PaychecksCodebookComponent::useNewCodebooks())
//            {
                $month_doprinosi_base = $paycheck->paycheck_period->codebook->min_contribution_base * $work_contract_hours_percent * $dependent_working_percent;

                if(PaychecksCodebookComponent::useMaxTaxBase())
                {
                    if($month_doprinosi_base > $paycheck->paycheck_period->codebook->max_contribution_base)
                    {
                        $month_doprinosi_base = $paycheck->paycheck_period->codebook->max_contribution_base;
                    }
                }
//            }
//            else
//            {
//                $month_doprinosi_base = $month->param('accounting.min_contribution_base') * $work_contract_hours_percent * $dependent_working_percent;
//            }

            if ($paycheck->paycheck_period->isBonus)
            {
                $month_doprinosi_base = 0;
            }
            $paycheck->month_doprinosi_base = $month_doprinosi_base;
        }
        else
        {
            $month_min_doprinosi_base = null;
//            if(PaychecksCodebookComponent::useNewCodebooks())
//            {
                $month_min_doprinosi_base = $paycheck->paycheck_period->codebook->min_contribution_base * $work_contract_hours_percent * $dependent_working_percent;
//            }
//            else
//            {
//                $month_min_doprinosi_base = $month->param('accounting.min_contribution_base') * $work_contract_hours_percent * $dependent_working_percent;
//            }

            if ($paycheck->paycheck_period->isBonus)
            {
                $month_min_doprinosi_base = 0;
            }
            $paycheck->month_doprinosi_base = $month_min_doprinosi_base;
        }
        /**
         * KRAJ SRAZMERNIH DELOVA
         */
        
        //TODO: ovde treba oduzeti broj dana iz dodataka
        //broj kalendarskih dana redovnog rada
        $worked_days = $employee_params['worked_days'];
        
        //BROJ RADNIH SATI - BROJ IZ DODATAKA
        $worked_hours = $employee_params['work_contracts_hours_sum'];
        //zanemarivanje da ljudi nisu prijavljen ceo mesec (sluzi za pregled zarada)
        if($paycheckPeriod->employees_work_full_month === true)
        {
            $worked_hours = $month_period_hours;
        }
        
        /// prvo se oduzima od ukupnog moguceg procenat jer je dodacima vec oduzeto procentualno
        $worked_hours *= $paycheck->working_percent * 0.01;
        
        //oduzimamo ignorisane sate da bi tacno izracunali koliko je radnih sati
        $worked_hours -= $paycheck->additionals_hours + $paycheck->additionals_ignored_hours;
        //ako ima vise dodataka nego sto je zaposleni prijavljen
        if($worked_hours < 0)
        {
            return Yii::t('PaychecksModule.Paycheck', 'WorkedHoursNegative', [
                '{emp}' => $employee_params['emp_model']->DisplayName
            ]);
        }
        $paycheck->worked_hours = $worked_hours;
        
        //oduzimamo ignorisane dane, da bi tacno izracunali broj radnih dana
        $paycheck->number_of_calendar_days = $employee_params['number_of_calendar_days'] 
                - $paycheck->additionals_calendar_days
                - $paycheck->additionals_ignored_calendar_days;
        
        //vrednost rada
//        $point_value = $paycheckPeriod->month_point_value;//vrednost boda
//        $value_per_hour = ($worked_points_in_month/$estimated_average_hours_in_month) * $point_value; //vrednost sata
        $value_per_hour = PaycheckCalculation::recalculateValuePerHour($paycheck, $paycheckPeriod, $first_work_contract);
        $paycheck->value_per_hour = $value_per_hour;
        $workedHoursValue = $worked_hours * $value_per_hour;//ukupna vrednost
        $paycheck->worked_hours_value = $workedHoursValue;
//        $paycheck->point_value = $point_value;
        
        /// broj sati na drzavni praznik
        $work_on_state_holiday_hours = 0;
        $work_on_state_holiday_percentage = 0;
        $work_on_state_holiday_value = 0;
        if(Yii::app()->configManager->get('accounting.paychecks.calculate_with_work_on_state_holiday') === 'true')
        {
            $work_on_state_holiday_hours = $employee_params['work_on_state_holiday_hours'];
            $work_on_state_holiday_percentage = Yii::app()->configManager->get('accounting.paychecks.work_on_state_holiday_percentage');
            
            $work_on_state_holiday_value = $work_on_state_holiday_hours * $value_per_hour * $work_on_state_holiday_percentage/100;
        }
        $paycheck->work_on_state_holiday_hours = $work_on_state_holiday_hours;
        $paycheck->work_on_state_holiday_percentage = $work_on_state_holiday_percentage;
        $paycheck->work_on_state_holiday_value = $work_on_state_holiday_value;
        
        $overtime_percentage = Yii::app()->configManager->get('accounting.paychecks.overtime_percentage');
        $overtime_value = $paycheck->overtime_hours * $value_per_hour * $overtime_percentage/100;
        $paycheck->overtime_percentage = $overtime_percentage;
        $paycheck->overtime_value = $overtime_value;
                
        $regular_work_hours = $paycheck->worked_hours + $paycheck->overtime_hours + $work_on_state_holiday_hours;
        $regular_work_value = $workedHoursValue + $overtime_value + $work_on_state_holiday_value;
        $paycheck->regular_work_hours = $regular_work_hours;
        $paycheck->regular_work_value = $regular_work_value;
                
        //vrednost na osnovu minulog rada
        $paycheck->old_work_exp = $paycheck->employee->old_work_exp;
        $work_contracts_work_exp = 0;//godine minulog rada
        if($worked_days >= 360)
        {
            $work_contracts_work_exp = floor($worked_days/360);
        }
        $paycheck->work_contracts_work_exp = $work_contracts_work_exp;
        $year_experience = $paycheck->old_work_exp + $paycheck->work_contracts_work_exp;
        
        $exp_perc = $month->param('accounting.exp_perc');
        $past_exp_value = $regular_work_value * ($exp_perc/100) * $year_experience;
        
        $paycheck->past_exp_value = $past_exp_value;
        $paycheck->year_experience = $year_experience;
        $paycheck->exp_perc = $exp_perc;
        
        //vrednost dobijena za ucinak
        //od ucinka se oduzima 100% da bi se obracunala samo razlika
        $actual_performance_percent = $paycheck->performance_percent - 100;
        $performance_value = $regular_work_value * $actual_performance_percent / 100;
        $paycheck->performance_value = $performance_value;
        //procenat ucinka se rucno zadaje
        
        //vrednost za probni rad - odnosno umanjenje
        $actual_probationer_percent = $probationer_percent - 100;
        $probationer_value = $regular_work_value * $actual_probationer_percent / 100;
        $paycheck->probationer_percent = $probationer_percent;
        $paycheck->probationer_value = $probationer_value;
                
        //zbir vrednosti za redovan rad
        $work_sum = $regular_work_value + $performance_value + $probationer_value + $past_exp_value;
        
        $paycheck->work_sum = $work_sum;
        
        //topli obrok
        if (PaycheckCalculations::isTufeFix())
        {
            $_tufe_fix_hot_meal = $paycheckPeriod->month_hours/$estimated_average_hours_in_month;
        }
        else
        {
            $_tufe_fix_hot_meal = 1;
        }
        if ($paycheck->paycheck_period->isBonus)
        {
            $hot_meal_per_hour = 0;
        }
        $paycheck->hot_meal_per_hour = $hot_meal_per_hour;
        
        $is_penzioner = $paycheck->employee->isRetiree();
        $paycheck->is_retiree = $is_penzioner;
        
        $zdr_empl_perc = $is_penzioner ? 0 : $zdr_empl;
        $nez_empl_perc = $is_penzioner ? 0 : $nez_empl;
        $zdr_comp_perc = $is_penzioner ? 0 : $zdr_comp;
        $nez_comp_perc = $is_penzioner ? 0 : $nez_comp;
        
        $hotmeal_fieldwork_per_hour = 0;
        $deductions_hotmeal_sum = 0;
        $deductions_hotmeal = $paycheck->deductions_hotmeal;
        foreach($deductions_hotmeal as $deduction_hotmeal)
        {
            $deductions_hotmeal_sum += $deduction_hotmeal->amount;
        }
        if($deductions_hotmeal_sum > 0)
        {
            if ($regular_work_hours == 0)
            {
                throw new SIMAWarnHotMealDeductionNoRegularHours($paycheck->employee);
            }
            $taxes_sum = $pio_empl_percentage/100 + $zdr_empl_perc/100 + $nez_empl_perc/100 + $tax_empl_perc/100;
            $hotmeal_fieldwork_per_hour = ($deductions_hotmeal_sum / ((1-$taxes_sum) * $regular_work_hours)) - $hot_meal_per_hour;
            
            if($hotmeal_fieldwork_per_hour < 0)
            {
                $hotmeal_fieldwork_per_hour = 0;
            }
        }
        $paycheck->hotmeal_fieldwork_per_hour = $hotmeal_fieldwork_per_hour;
        
        $hotmeal_fieldwork_value = $hotmeal_fieldwork_per_hour * $regular_work_hours;
        $paycheck->hotmeal_fieldwork_value = $hotmeal_fieldwork_value;
        
        $hot_meal_value = $hot_meal_per_hour * $regular_work_hours * $_tufe_fix_hot_meal;
        $paycheck->hot_meal_value = $hot_meal_value;
        
        
        //regres - uzima se samo za radne sate
        $regres_working_hours_fix = ($worked_hours)/$month_period_hours;
        if ($paycheck->paycheck_period->isBonus)
        {
            $regres_value = 0;
        }
        else
        {
            $regres_value = $regres * $regres_working_hours_fix;
        }
        $paycheck->regres = $regres_value;
        
        //ovo mora da se izracuna ranije zbog tufefix-a
                
        $bonus_bruto = 0;
        foreach ($paycheck->bonuses as $_bonus)
        {
            
            $bonus_bruto += $_bonus->value;
        }
        
        $paycheck->bonus_bruto = $bonus_bruto;
        
        //bruto za redovan rad
        $base_bruto = $work_sum + $hot_meal_value + $hotmeal_fieldwork_value + $regres_value + $bonus_bruto;

        $paycheck->base_bruto = $base_bruto;
        
        if(PaychecksCodebookComponent::useMaxTaxBase())
        {
            $paycheck->base_doprinosi_base = $month_doprinosi_base - $paycheck->additionals_doprinosi_base;
        }
        else
        {
            $paycheck->base_doprinosi_base = $month_min_doprinosi_base - $paycheck->additionals_doprinosi_base;
        }

        if(PaychecksCodebookComponent::useMaxTaxBase())
        {
//            $is_by_min_doprinosi_base = $paycheck->levelDoprinosiBase();
            $by_min_or_max = $paycheck->levelDoprinosiBase();
        }
        else
        {
//        $is_by_min_doprinosi_base = $paycheck->levelDoprinosiBase($month_min_doprinosi_base);
            $by_min_or_max = $paycheck->levelDoprinosiBase($month_min_doprinosi_base);
        }
        
        
        $MAX_DOPRINOSI = 341725;

        if (($paycheck->base_doprinosi_base + $paycheck->prev_doprinosi_base) > $MAX_DOPRINOSI)
        {
            $paycheck->base_doprinosi_base = $MAX_DOPRINOSI - $paycheck->prev_doprinosi_base;
        }
        //popunjava se posle levelovanja, u sustini, samo ce se osnovica za porez izmeniti, ne bi smelo nista vise
        $paycheck->populateAdditionals();
        
        self::sumPreviousPayouts($paycheck);
        
        
        $base_doprinosi_base = $paycheck->base_doprinosi_base;
        
        $paycheck->is_by_min_tax_base = $by_min_or_max === 'MIN';
        $paycheck->is_by_max_tax_base = $by_min_or_max === 'MAX';
        
        
        //PIO na teret radnika i na teret firme za osnovnu zaradu
        $base_pio_empl = $base_doprinosi_base * $pio_empl_percentage / 100;
//        $base_pio_empl = round($base_doprinosi_base * $month->param('accounting.pio_empl') / 100,2);
        $paycheck->base_pio_empl = $base_pio_empl;
        $paycheck->pio_empl_perc = $pio_empl_percentage;

        $base_pio_comp = $base_doprinosi_base * $pio_comp_percentage / 100;
//        $base_pio_comp = round($base_doprinosi_base * $month->param('accounting.pio_comp') / 100,2);
        $paycheck->base_pio_comp = $base_pio_comp;
        $paycheck->pio_comp_perc = $pio_comp_percentage;

        $paycheck->base_pio = $base_pio_empl + $base_pio_comp;
        
        //ZDR na teret radnika i na teret firme za osnovnu zaradu
        $base_zdr_empl = $is_penzioner ? 0 : $base_doprinosi_base * $zdr_empl_perc / 100;
//        $base_zdr_empl = round($base_doprinosi_base * $month->param('accounting.zdr_empl') / 100,2);
        $paycheck->base_zdr_empl = $base_zdr_empl;
        $paycheck->zdr_empl_perc = $zdr_empl_perc;
        
        $base_zdr_comp = $is_penzioner ? 0 : $base_doprinosi_base * $zdr_comp_perc / 100;
//        $base_zdr_comp = round($base_doprinosi_base * $month->param('accounting.zdr_comp') / 100,2);
        $paycheck->base_zdr_comp = $base_zdr_comp;
        $paycheck->zdr_comp_perc = $zdr_comp_perc;
        
        $paycheck->base_zdr = $base_zdr_empl + $base_zdr_comp;
        
        //NEZ na teret radnika i na teret firme za osnovnu zaradu
        $base_nez_empl = $is_penzioner ? 0 : $base_doprinosi_base * $nez_empl_perc / 100;
//        $base_nez_empl = round($base_doprinosi_base * $month->param('accounting.nez_empl') / 100,2);
        $paycheck->base_nez_empl = $base_nez_empl;
        $paycheck->nez_empl_perc = $nez_empl_perc;
        
        $base_nez_comp = $is_penzioner ? 0 : $base_doprinosi_base * $nez_comp_perc / 100;
//        $base_nez_comp = round($base_doprinosi_base * $month->param('accounting.nez_comp') / 100,2);
        $paycheck->base_nez_comp = $base_nez_comp;
        $paycheck->nez_comp_perc = $nez_comp_perc;
        
        $paycheck->base_nez = $base_nez_empl + $base_nez_comp;
        
        //osnovica za porez za redovan rad = preostali deo koji moze da se iskoristi
        $base_tax_base = $base_bruto - ($tax_release_for_use - $paycheck->additionals_used_tax_release);
        
        if (SIMAMisc::lessThen($base_tax_base, 0))
        {
            $base_tax_base = 0;
            $error_message = 'ne placa se porez na zaradu!!!!';
            Yii::app()->errorReport->createAutoClientBafRequest($error_message.' - paycheck: '.SIMAMisc::toTypeAndJsonString($paycheck));
            throw new SIMAWarnException($error_message);
        }
        
        if(PaychecksCodebookComponent::useMaxTaxBase())
        {
            $paycheck->month_min_contribution_base = $paycheck->paycheck_period->codebook->min_contribution_base;
            $paycheck->month_max_contribution_base = $paycheck->paycheck_period->codebook->max_contribution_base;
        }
        
        $paycheck->base_tax_base = $base_tax_base;
        
        //porez koji se placa
        $base_tax_empl = $base_tax_base * $tax_empl_perc / 100;
        $paycheck->tax_empl_perc = $tax_empl_perc;
        $paycheck->base_tax_empl = $base_tax_empl;
        
        /** UKUPNE VREDNOSTI (sabiranja) - ovo ide samo za OZ */
        
        if ($paycheck->paycheck_period->isBonus)
        {
            //trenutna zarada je osnovan zarada + dodaci - dosada isplaceno
            //za bonus, ne gledaju se prethodne isplate
            $paycheck->amount_bruto    = round($base_bruto,2);
            $paycheck->tax_base        = round($base_tax_base,2);
            $paycheck->tax_empl        = round($base_tax_empl,2);
            $paycheck->doprinosi_base  = round($base_doprinosi_base,2);
            $paycheck->pio_empl        = round($base_pio_empl,2);
            $paycheck->zdr_empl        = round($base_zdr_empl,2);
            $paycheck->nez_empl        = round($base_nez_empl,2);
            $paycheck->pio_comp        = round($base_pio_comp,2);
            $paycheck->zdr_comp        = round($base_zdr_comp,2);
            $paycheck->nez_comp        = round($base_nez_comp,2);

            $paycheck->amount_neto  = 
                    round($paycheck->amount_bruto - $paycheck->tax_empl - $paycheck->pio_empl - $paycheck->zdr_empl - $paycheck->nez_empl,2);
            $paycheck->amount_total = 
                    round($paycheck->amount_bruto                       + $paycheck->pio_comp + $paycheck->zdr_comp + $paycheck->nez_comp,2);
        }
        else
        {
            //trenutna zarada je osnovan zarada + dodaci - dosada isplaceno
            $paycheck->amount_bruto    = round($base_bruto           + $paycheck->additionals_sum                                   - $paycheck->prev_bruto_sum,2);
            $paycheck->tax_base        = round($base_tax_base        + ($paycheck->additionals_sum - $paycheck->additionals_used_tax_release) - $paycheck->prev_tax_base,2);
            $paycheck->tax_empl        = round($base_tax_empl        + $paycheck->additionals_tax_empl                              - $paycheck->prev_tax_empl,2);
            $paycheck->doprinosi_base  = round($base_doprinosi_base  + $paycheck->additionals_doprinosi_base                        - $paycheck->prev_doprinosi_base,2);
            $paycheck->pio_empl        = round($base_pio_empl        + $paycheck->additionals_pio_empl                              - $paycheck->prev_pio_empl,2);
            $paycheck->zdr_empl        = round($base_zdr_empl        + $paycheck->additionals_zdr_empl                              - $paycheck->prev_zdr_empl,2);
            $paycheck->nez_empl        = round($base_nez_empl        + $paycheck->additionals_nez_empl                              - $paycheck->prev_nez_empl,2);
            $paycheck->pio_comp        = round($base_pio_comp        + $paycheck->additionals_pio_comp                              - $paycheck->prev_pio_comp,2);
            $paycheck->zdr_comp        = round($base_zdr_comp        + $paycheck->additionals_zdr_comp                              - $paycheck->prev_zdr_comp,2);
            $paycheck->nez_comp        = round($base_nez_comp        + $paycheck->additionals_nez_comp                              - $paycheck->prev_nez_comp,2);
            
            $paycheck->amount_neto  = 
                    round($paycheck->amount_bruto - $paycheck->tax_empl - $paycheck->pio_empl - $paycheck->zdr_empl - $paycheck->nez_empl,2);
            $paycheck->amount_total = 
                    round($paycheck->amount_bruto                       + $paycheck->pio_comp + $paycheck->zdr_comp + $paycheck->nez_comp,2);
           
        }
        if($paycheck->amount_neto < 0)
        {
            throw new SIMAWarnNetoLessThanZero($paycheck->employee);
        }
        if($paycheck->doprinosi_base < 0)
        {
            throw new SIMAWarnPaycheckCalculationFailed($paycheck->employee, 'doprinosi_base < 0');
        }
        
        $suspensions_sum = 0;
        $suspensions = $paycheck->suspensions;
        foreach($suspensions as $suspension)
        {
            $suspensions_sum += $suspension->amount;
        }
        if($suspensions_sum > $paycheck->amount_neto)
        {
            throw new SIMAWarnException(Yii::t('PaychecksModule.Paycheck', 'SuspensionSumMoreThanNeto', [
                '{emp}' => $paycheck->employee_display_name
            ]));
        }
        
        $deductions_sum = 0;
//        $deductions = $paycheck->deductions_not_hotmeal;
        $deductions = $paycheck->deductions;
        foreach($deductions as $deduction)
        {
            $deductions_sum += $deduction->amount;
        }
        if($deductions_sum > $paycheck->amount_neto)
        {
            throw new SIMAWarnException(Yii::t('PaychecksModule.Paycheck', 'DeductionSumMoreThanNeto', [
                '{emp}' => $paycheck->employee_display_name,
                '{amount}' => SIMAHtml::number_format($deductions_sum - $paycheck->amount_neto)
            ]));
        }
        
        $amount_neto_for_payout = $paycheck->amount_neto - $suspensions_sum - $deductions_sum;
        
        if($amount_neto_for_payout < 0)
        {
            throw new SIMAWarnException(Yii::t('PaychecksModule.Paycheck', 'AmountNetoForPayoutLessThanZero', [
                '{emp}' => $paycheck->employee_display_name
            ]));
        }
        
        $paycheck->amount_neto_for_payout = $amount_neto_for_payout;
        $paycheck->suspensions_sum = $suspensions_sum;
        $paycheck->deductions_sum = $deductions_sum;
        
        if(!$paycheck->employee->isPayoutByCache())
        {
            $paycheck->bank_account_id = $paycheck->employee->person->main_bank_account->id;
            $paycheck->bank_account_display = $paycheck->employee->person->main_bank_account->DisplayName;
        }
        else if($paycheck->employee->isPayoutByCache() && (!empty($paycheck->bank_account_id) || !empty($paycheck->bank_account_display)))
        {
            $paycheck->bank_account_id = null;
            $paycheck->bank_account_display = null;
        }
                
        $paycheck->is_valid = true;
        
        unset($paycheck->employee->person->main_bank_account);
        unset($paycheck->employee->person);
        unset($paycheck->employee);
        unset($paycheckPeriod->month);
        
        
        unset($actual_performance_percent);

        unset($base_bruto);
        unset($base_tax_base);
        unset($exp_perc);
        unset($first_work_contract->work_position->paygrade);
        unset($first_work_contract->work_position);
        unset($first_work_contract);
        unset($hot_meal_per_hour);   
        unset($hot_meal_value);
        if(PaychecksCodebookComponent::useMaxTaxBase())
        {
            unset($month_doprinosi_base);
        }
        else
        {
            unset($month_min_doprinosi_base);
        }
        unset($month_period_hours);
        unset($performance_value);
        unset($past_exp_value);
        unset($regres_value);
        unset($regres_working_hours_fix);
        unset($tax_empl_perc);
        unset($value_per_hour);
        unset($work_sum);
        unset($worked_days);
        unset($worked_hours);
        unset($workedHoursValue);
        unset($year_experience);
        unset($probationer_percent);
        
        return 'success';
    }
    
    private function sumPreviousPayouts(Paycheck $paycheck)
    {
        //PRETHODNE ISPLATE
        $prev_bruto = 0;
        $prev_tax_base = 0;
        $prev_tax_empl = 0;
        $prev_doprinosi_base = 0;
        $prev_pio_empl = 0;
        $prev_zdr_empl = 0;
        $prev_nez_empl = 0;
        $prev_pio_comp = 0;
        $prev_zdr_comp = 0;
        $prev_nez_comp = 0;
        
        $paycheckPeriod = $paycheck->paycheck_period;
        
        if($paycheckPeriod->ordering_number !== 1)
        {            
            $criteriaPreviousPaychecks = new SIMADbCriteria([
                'Model' => 'Paycheck',
                'model_filter' => [
                    'employee' => array(
                        'ids' => $paycheck->employee->id
                    ),
                    'paycheck_period' => array(
                        'month' => [
                            'ids' => $paycheckPeriod->month->id
                        ]
                    )
                ]
            ]);
            $previousPaychecks = Paycheck::model()->findAll($criteriaPreviousPaychecks);
            
            foreach($previousPaychecks as $previousPaycheck)
            {
                
                if($previousPaycheck->paycheck_period->ordering_number < $paycheck->paycheck_period->ordering_number)
                {
                    $prev_bruto             += $previousPaycheck->amount_bruto;
                    $prev_tax_base          += $previousPaycheck->tax_base;
                    $prev_tax_empl          += $previousPaycheck->tax_empl;
                    $prev_doprinosi_base    += $previousPaycheck->doprinosi_base;
                    $prev_pio_empl          += $previousPaycheck->pio_empl;
                    $prev_zdr_empl          += $previousPaycheck->zdr_empl;
                    $prev_nez_empl          += $previousPaycheck->nez_empl;
                    $prev_pio_comp          += $previousPaycheck->pio_comp;
                    $prev_zdr_comp          += $previousPaycheck->zdr_comp;
                    $prev_nez_comp          += $previousPaycheck->nez_comp;
                }
            }
            
            unset($previousPaychecks);
            unset($criteriaPreviousPaychecks);
        }
        
        $paycheck->prev_bruto_sum       = $prev_bruto;
        $paycheck->prev_tax_base        = $prev_tax_base;
        $paycheck->prev_tax_empl        = $prev_tax_empl;
        $paycheck->prev_doprinosi_base  = $prev_doprinosi_base;
        $paycheck->prev_pio_empl        = $prev_pio_empl;
        $paycheck->prev_zdr_empl        = $prev_zdr_empl;
        $paycheck->prev_nez_empl        = $prev_nez_empl;
        $paycheck->prev_pio_comp        = $prev_pio_comp;
        $paycheck->prev_zdr_comp        = $prev_zdr_comp;
        $paycheck->prev_nez_comp        = $prev_nez_comp;
        
    }
    
    public static function recalculateValuePerHour(Paycheck $paycheck, PaycheckPeriod $paycheckPeriod, WorkContract $first_work_contract)
    {
        $bruto_hour_calc_type = (int)(Yii::app()->configManager->get('accounting.paychecks.bruto_hour_calc_type'));
        
        if($bruto_hour_calc_type === 1)
        {
            $value_per_hour = PaycheckCalculation::recalculateValuePerHour1($paycheck, $paycheckPeriod, $first_work_contract);
        }
        else if($bruto_hour_calc_type === 2)
        {
            $value_per_hour = PaycheckCalculation::recalculateValuePerHour2($paycheck, $paycheckPeriod, $first_work_contract);
        }
        else if($bruto_hour_calc_type === 3)
        {
            $value_per_hour = PaycheckCalculation::recalculateValuePerHour3($paycheck, $paycheckPeriod, $first_work_contract);
        }
        else if($bruto_hour_calc_type === 4)
        {
            $value_per_hour = PaycheckCalculation::recalculateValuePerHour4($paycheck, $paycheckPeriod, $first_work_contract);
        }
        else
        {
            throw new Exception(__METHOD__.' - not implemented');
        }
        
        return $value_per_hour;
    }
    
    public static function recalculateValuePerHour1(Paycheck $paycheck, PaycheckPeriod $paycheckPeriod, WorkContract $first_work_contract)
    {
        $work_position_points = $first_work_contract->work_position->paygrade->points;
        $paycheck->work_position_points = $work_position_points;
        
        $personal_points = $paycheck->employee->personal_points;
        $paycheck->personal_points = $personal_points;
        
        $under_contract_points_in_month_full = $work_position_points + $personal_points;
        $paycheck->points = $under_contract_points_in_month_full;
        
        $estimated_average_hours_in_month = $paycheck->estimated_average_hours_in_month;
        
        $point_value = $paycheckPeriod->month_point_value;//vrednost boda
        $paycheck->point_value = $point_value;
        
        $value_per_hour = ($under_contract_points_in_month_full/$estimated_average_hours_in_month) * $point_value; //vrednost sata
        return $value_per_hour;
    }
    
    public static function recalculateValuePerHour2(Paycheck $paycheck, PaycheckPeriod $paycheckPeriod, WorkContract $first_work_contract)
    {
        $value_per_hour = PaycheckCalculation::recalculateValuePerHour1($paycheck, $paycheckPeriod, $first_work_contract);
        
        $value_per_hour *= $paycheckPeriod->hours_percentage*0.01;
        
        return $value_per_hour;
    }
    
    public static function recalculateValuePerHour3(Paycheck $paycheck, PaycheckPeriod $paycheckPeriod, WorkContract $first_work_contract)
    {
        $work_position_points = $first_work_contract->work_position->paygrade->points;
        $paycheck->work_position_points = $work_position_points;
        
        $personal_points = $paycheck->employee->personal_points;
        $paycheck->personal_points = $personal_points;
        
        $under_contract_points_in_month_full = $work_position_points + $personal_points;
        $paycheck->points = $under_contract_points_in_month_full;
        
        $estimated_average_hours_in_month = $paycheck->estimated_average_hours_in_month;
        
        $point_value = $paycheckPeriod->month_point_value;//vrednost boda
        $paycheck->point_value = $point_value;
        
        $value_per_hour = $under_contract_points_in_month_full * $point_value; //vrednost sata
                
        return $value_per_hour;
    }
    
    public static function recalculateValuePerHour4(Paycheck $paycheck, PaycheckPeriod $paycheckPeriod, WorkContract $first_work_contract)
    {
        $work_position_work_hours_value = $first_work_contract->work_position->paygrade->points;
        $paycheck->work_position_points = $work_position_work_hours_value;
        
        $personal_work_hours_value = $paycheck->employee->personal_points;
        $paycheck->personal_points = $personal_work_hours_value;
        
        $point_value = $paycheckPeriod->month_point_value;//vrednost boda
        $paycheck->point_value = $point_value;
        
        $value_per_hour = $work_position_work_hours_value;
        if(!empty($personal_work_hours_value))
        {
            $value_per_hour = $personal_work_hours_value;
        }
        
        return $value_per_hour;
    }
}
