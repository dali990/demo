<?php

class PaychecksEmployee
{
    public static function isEmployeeOkForPaycheckRecalc(Employee $employee, array $months)
    {
        $is_ok = false;
        
        $problems = PaychecksEmployee::getReasonsForNotOkForPaycheckRecalc($employee, $months, true);

        if(count($problems) === 0)
        {
            $is_ok = true;
        }
        
        return $is_ok;
    }
    
    public static function getReasonsForNotOkForPaycheckRecalc(Employee $employee, array $months, $any_error=false)
    {        
        $problem_messages = array();
        
        $month_ids = [];
        foreach($months as $key => $month)
        {
            $month_ids[$key] = $month->id;
        }
        
        $paychecksCriteria = new SIMADbCriteria([
            'Model' => Paycheck::model(),
            'model_filter' => [
                'employee' => [
                    'ids' => $employee->id
                ],
                'paycheck_period' => [
                    'month' => [
                        'ids' => $month_ids
                    ]
                ]
            ]
        ]);
                
        $paychecks = Paycheck::model()->findAll($paychecksCriteria);

        $month_ids_passed = [];

        foreach($paychecks as $paycheck)
        {
//                $had_error = false;
//                if(empty($paycheck->worked_hours))
//                {
//                    $had_error = true;
//                    $problem_messages[] = Yii::t(
//                        'PaychecksModule.PaychecksEmployee', 
//                        'NoWorkedHoursValueForMonth', 
//                        array(
//                            '{month}' => $paycheck->paycheck_period->month
//                        )
//                    );
//                }
//                if(empty($paycheck->worked_hours_value))
//                {
//                    $had_error = true;
//                    $problem_messages[] = Yii::t(
//                        'PaychecksModule.PaychecksEmployee', 
//                        'NoWorkedHoursValueForMonth', 
//                        array(
//                            '{month}' => $paycheck->paycheck_period->month
//                        )
//                    );
//                }
//                if(empty($paycheck->past_exp_value))
//                {
//                    $had_error = true;
//                    $problem_messages[] = Yii::t(
//                        'PaychecksModule.PaychecksEmployee', 
//                        'NoPastExperienceValueForMonth', 
//                        array(
//                            '{month}' => $paycheck->paycheck_period->month
//                        )
//                    );
//                }
//                
//                if($any_error && $had_error)
//                {
//                    break;
//                }

            if(empty($month_ids_passed[$paycheck->paycheck_period->month->id]))
            {
                $month_ids_passed[$paycheck->paycheck_period->month->id] = $paycheck->paycheck_period->month->id;
            }
        }

        $array_diff = array_diff($month_ids, $month_ids_passed);

        if(!empty($array_diff))
        {
            foreach($array_diff as $key => $ad)
            {
                if($employee->isActiveAt($months[$key]))
                {
                    $problem_messages[] = Yii::t(
                        'PaychecksModule.PaychecksEmployee', 
                        'NoPaycheckForMonth',
                        [
                            '{month}' => $months[$key]
                        ]
                    );

                    if($any_error)
                    {
                        break;
                    }
                }
            }
        }
        
        return $problem_messages;
    }
    
    public static function doesEmployeeHaveWorkContractInMonth(Employee $employee, Month $month)
    {
        $result = false;
        
        $tempEmpsCriteria = new SIMADbCriteria([
            'Model' => 'Employee',
            'model_filter' => [
                'ids' => $employee->id
            ]
        ]);

        $tempEmpsCount = Employee::model()->haveActiveWorkContractInMonth($month)->count($tempEmpsCriteria);
        
        if($tempEmpsCount === 1)
        {
            $result = true;
        }
        
        return $result;
    }
    
    public static function getPaychecksForEmployeeInMonth(Employee $employee, Month $month)
    {
        $criteria = new SIMADbCriteria([
            'Model' => 'Paycheck',
            'model_filter' => [
                'employee' => array(
                    'ids' => $employee->id
                ),
                'paycheck_period' => array(
                    'month' => array(
                        'ids' => $month->id
                    )
                )
            ]
        ]);

        $paychecks = Paycheck::model()->findAll($criteria);
        
        if(!isset($paychecks))
        {
            $paychecks = array();
        }
        
        return $paychecks;
    }
    
    public static function createArrayForEmployeeParametersTable(Paycheck $paycheck)
    {        
        $paygrade_points = $paycheck->work_position_points;
        $year_experience = $paycheck->year_experience;
        $probationer_percent = $paycheck->probationer_percent;
        $probationer_display = 'Ne';
        if($probationer_percent !== 100)
        {
            $probationer_display = 'Da ('.$probationer_percent.')';
        }
        
        $parametersEmployee = array(
            "paygrade_points" => array(
                "ordernum" => "1.1",
                "name" => "Poena po radnoj poziciji",
                "value" => $paygrade_points,
            ),
            "old_work_exp" => [
                "ordernum" => "1.2",
                "name" => $paycheck->getAttributeLabel('old_work_exp'),
                "value" => $paycheck->old_work_exp,
            ],
            "work_contracts_work_exp" => array(
                "ordernum" => "1.3",
                "name" => $paycheck->getAttributeLabel('work_contracts_work_exp'),
                "value" => $paycheck->work_contracts_work_exp,
            ),
            "performance_percent" => array(
                "ordernum" => "1.4",
                "name" => "ucinak (%)",
                "value" => $paycheck->performance_percent,
                "formula" => ""
            ),
            "probationer_percent" => array(
                "ordernum" => "1.5",
                "name" => "pripravnik (%)",
                "value" => $probationer_display,
                "formula" => ""
            ),
            "personal_points" => array(
                "ordernum" => "1.6",
                "name" => "licnih poena",
                "value" => $paycheck->personal_points,
                "formula" => ""
            ),
            "work_on_state_holiday_hours" => array(
                "ordernum" => "1.7",
                "name" => Yii::t('PaychecksModule.PaycheckAdditional', 'WORK_ON_NON_WORKING_HOLIDAY'),
                "value" => $paycheck->work_on_state_holiday_hours,
                "formula" => ""
            ),
            "overtime_hours" => array(
                "ordernum" => "1.8",
                "name" => Yii::t('PaychecksModule.Paycheck', 'Overtime'),
                "value" => $paycheck->overtime_hours,
                "formula" => ""
            ),
            "is_retiree" => array(
                "ordernum" => "1.9",
                "name" => $paycheck->getAttributeLabel('is_retiree'),
                "value" => $paycheck->is_retiree?'Da':'Ne',
                "formula" => ""
            ),
        );
        
        $bruto_hour_calc_type = (int)(Yii::app()->configManager->get('accounting.paychecks.bruto_hour_calc_type'));
        if($bruto_hour_calc_type === 4)
        {
            $parametersEmployee['paygrade_points']['name'] = 'Satnica';
            $parametersEmployee['personal_points']['name'] = 'Licna satnica';
        }
        
        return $parametersEmployee;
    }
    
    public static function createArrayForPaycheckPeriodParametersTable(Paycheck $paycheck)
    {
        $paycheck_period_hours_percentage = $paycheck->hours_percentage;

        $parametersPaycheckPeriod = array(
            "paycheck_period_hours_percentage" => array(
                "ordernum" => "2.1",
                "name" => "Radnih sati u periodu (%)",
                "value" => $paycheck_period_hours_percentage,
            ),
            "tax_release" => array(
                "ordernum" => "2.2",
                "name" => $paycheck->getAttributeLabel('tax_release').' (rsd)',
                "value" => SIMAHtml::money_format($paycheck->tax_release),
            ),
        );
        
        return $parametersPaycheckPeriod;
    }
    
    public static function createArrayForAccountingMonthParametersTable(Paycheck $paycheck)
    {
        $month_period_hours = $paycheck->month_period_hours;
        $pio_empl_perc = $paycheck->pio_empl_perc;
        $zdr_empl_perc = $paycheck->zdr_empl_perc;
        $nez_empl_perc = $paycheck->nez_empl_perc;
        $tax_empl_perc = $paycheck->tax_empl_perc;
        $pio_comp_perc = $paycheck->pio_comp_perc;
        $zdr_comp_perc = $paycheck->zdr_comp_perc;
        $nez_comp_perc = $paycheck->nez_comp_perc;
        $regres = $paycheck->regres;
        $point_value = $paycheck->point_value;
        $exp_perc = $paycheck->exp_perc;
        
        $parametersAccountingMonth = array(
            "month_period_hours" => array(
                "ordernum" => "3.1",
                "name" => "Radnih sati u mesecu",
                "value" => $month_period_hours,
            ),
            "hot_meal_per_hour" => array(
                "ordernum" => "3.2",
                "name" => PaycheckCodebook::model()->getAttributeLabel('hot_meal_per_hour'),
                "value" => SIMAHtml::money_format($paycheck->hot_meal_per_hour),
            ),
            "point_value" => array(
                "ordernum" => "3.3",
                "name" => "vrednost po poenu",
                "value" => SIMAHtml::money_format($point_value),
            ),
            'exp_perc' => array(
                "ordernum" => "3.4",
                "name" => "procenat minulog rada (%)",
                "value" => $exp_perc,
            ),
            'regres' => array(
                "ordernum" => "3.5",
                "name" => "Regres",
                "value" => SIMAHtml::money_format($regres),
            ),
            'pio_empl_perc' => array(
                "ordernum" => "3.6",
                "name" => "PIO zaposleni (%)",
                "value" => $pio_empl_perc,
            ),
            'zdr_empl_perc' => array(
                "ordernum" => "3.7",
                "name" => "Zdrav. zapos. (%)",
                "value" => $zdr_empl_perc,
            ),
            'nez_empl_perc' => array(
                "ordernum" => "3.8",
                "name" => "Osig. zaposleni(%)",
                "value" => $nez_empl_perc,
            ),
            'tax_empl_perc' => array(
                "ordernum" => "3.9",
                "name" => "Porez (%)",
                "value" => $tax_empl_perc,
            ),
            'pio_comp_perc' => array(
                "ordernum" => "3.10",
                "name" => "PIO firma (%)",
                "value" => $pio_comp_perc,
            ),
            'zdr_comp_perc' => array(
                "ordernum" => "3.11",
                "name" => "Zdravs. firma (%)",
                "value" => $zdr_comp_perc,
            ),
            'nez_comp_perc' => array(
                "ordernum" => "3.12",
                "name" => "Osig. firma(%)",
                "value" => $nez_comp_perc,
            ),
            "month_min_contribution_base" => array(
                "ordernum" => "3.13",
                "name" => $paycheck->getAttributeLabel('month_min_contribution_base'),
                "value" => SIMAHtml::money_format($paycheck->month_min_contribution_base),
            ),
            "month_max_contribution_base" => [
                "ordernum" => "3.14",
                "name" => $paycheck->getAttributeLabel('month_max_contribution_base'),
                "value" => SIMAHtml::money_format($paycheck->month_max_contribution_base),
            ]
        );
        
        return $parametersAccountingMonth;
    }
    
    public static function createArrayForSystemParametersTable(Paycheck $paycheck)
    {
        $estimated_average_hours_in_month = $paycheck->estimated_average_hours_in_month;
        
        $systemParameters = array(
            "estimated_average_hours_in_month" => array(
                "ordernum" => "4.1",
                "name" => "Procenjen prosecan broj radnih sati u mesecu",
                "value" => $estimated_average_hours_in_month,
            ),
            "work_on_state_holiday_percentage" => array(
                "ordernum" => "4.2",
                "name" => Yii::t('PaychecksModule.PaycheckAdditional', 'WorkOnStateHolidayPercentage'),
                "value" => $paycheck->work_on_state_holiday_percentage,
            ),
            "overtime_percentage" => array(
                "ordernum" => "4.3",
                "name" => Yii::t('PaychecksModule.Paycheck', 'OvertimePercentage'),
                "value" => $paycheck->overtime_percentage,
            ),
        );
        
        
        $bruto_hour_calc_type = (int)(Yii::app()->configManager->get('accounting.paychecks.bruto_hour_calc_type'));
        if($bruto_hour_calc_type === 3)
        {
            unset($systemParameters['estimated_average_hours_in_month']);
        }
        
        $order_num_major = 4;
        $order_num_minor = 1;
        $systemParameters_keys = array_keys($systemParameters);
        foreach($systemParameters_keys as $key)
        {
            $systemParameters[$key]['ordernum'] = "$order_num_major.$order_num_minor";
            $order_num_minor++;
        }
        
        
        return $systemParameters;
    }
    
    public static function createArrayForCalculatedParametersTable(Paycheck $paycheck)
    {        
        // 1
        $employeeParameters = PaychecksEmployee::createArrayForEmployeeParametersTable($paycheck);
        
        // 2
        $paycheckPeriodParameters = PaychecksEmployee::createArrayForPaycheckPeriodParametersTable($paycheck);
        
        // 3
        $accountingMonthParameters = PaychecksEmployee::createArrayForAccountingMonthParametersTable($paycheck);
        
        // 4
        $systemParameters = PaychecksEmployee::createArrayForSystemParametersTable($paycheck);
        
        $calculatedParameters = array(
            'points' => array(
                "ordernum" => "5.1",
                "name" => "broj odradjeni poena",
                "value" => $paycheck->points,
                "formula" => ''
            ),
            'additionals_sum' => array(
                "ordernum" => "5.2",
                "name" => "suma vrednosti dodataka",
                "value" => SIMAHtml::money_format($paycheck->additionals_sum),
                "formula" => ""
            ),
            'additionals_hours' => array(
                "ordernum" => "5.3",
                "name" => "sati dodataka",
                "value" => $paycheck->additionals_hours,
                "formula" => ""
            ),
            'worked_hours' => array(
                "ordernum" => "5.4",
                "name" => "odradjenih radnih sati",
                "value" => $paycheck->worked_hours,
                "formula" => ''
            ),
            'value_per_hour' => array(
                "ordernum" => "5.5",
                "name" => "vrednost po satu",
                "value" => $paycheck->value_per_hour,
                "formula" => ''
            ),
            'worked_hours_value' => array(
                "ordernum" => "5.7",
                "name" => "vrednost odradjenih sati",
                "value" => SIMAHtml::money_format($paycheck->worked_hours_value),
                "formula" => ''
            ),
            'overtime_value' => array(
                "ordernum" => "5.8",
                "name" => "Prekovremeni rad",
                "value" => SIMAHtml::money_format($paycheck->overtime_value),
                "formula" => ''
            ),
            'work_on_state_holiday_value' => array(
                "ordernum" => "5.9",
                "name" => Yii::t('PaychecksModule.PaycheckAdditional', 'WorkOnStateHolidayValue'),
                "value" => SIMAHtml::money_format($paycheck->work_on_state_holiday_value),
                "formula" => ''
            ),
            'regular_work_hours' => array(
                "ordernum" => "5.10",
                "name" => "Sati redovnog rada",
                "value" => $paycheck->regular_work_hours,
                "formula" => ''
            ),
            'regular_work_value' => array(
                "ordernum" => "5.11",
                "name" => "Suma redovnog rada",
                "value" => SIMAHtml::money_format($paycheck->regular_work_value),
                "formula" => ''
            ),
            'hot_meal_value' => array(
                "ordernum" => "5.12",
                "name" => "Topli obrok",
                "value" => SIMAHtml::money_format($paycheck->hot_meal_value),
                "formula" => ''
            ),
            'year_experience' => [
                "ordernum" => "5",
                "name" => $paycheck->getAttributeLabel('year_experience'),
                "value" => $paycheck->year_experience,
                "formula" => ""
            ],
            'past_exp_value' => array(
                "ordernum" => "5.13",
                "name" => "minuli rad",
                "value" => SIMAHtml::money_format($paycheck->past_exp_value),
                "formula" => ''
            ),
            'performance_value' => array(
                "ordernum" => "5.14",
                "name" => "Vrednost ucinka",
                "value" => SIMAHtml::money_format($paycheck->performance_value),
                "formula" => ''
            ),
            'probationer_value' => array(
                "ordernum" => "5.15",
                "name" => "Vrednost pripravnickog",
                "value" => SIMAHtml::money_format($paycheck->probationer_value),
                "formula" => ''
            ),
            'work_sum' => array(
                "ordernum" => "5.16",
                "name" => "Redovan rad",
                "value" => SIMAHtml::money_format($paycheck->work_sum),
                "formula" => ''
            ),
            array(
                "ordernum" => "5.17",
                "name" => "Prethodnih 12 meseci",
                "value" => SIMAHtml::money_format($paycheck->last_12_months_value),
                "formula" => ""
            ),
            array(
                "ordernum" => "5.18",
                "name" => "Prethodnih 12 meseci sati",
                "value" => $paycheck->last_12_months_hours,
                "formula" => ""
            ),
            "suspensions_sum" => array(
                "ordernum" => "5.19",
                "name" => $paycheck->getAttributeLabel('suspensions_sum'),
                "value" => number_format($paycheck->suspensions_sum, 2, '.', ' '),
                "formula" => ""
            ),
            "deductions_sum" => array(
                "ordernum" => "5.20",
                "name" => "Suma odbitaka",
                "value" => number_format($paycheck->deductions_sum, 2, '.', ' '),
                "formula" => ""
            ),
            "hotmeal_fieldwork_per_hour" => array(
                "ordernum" => "5.21",
                "name" => "Dodaci na topli obrok za rad na terenu po satu",
                "value" => number_format($paycheck->hotmeal_fieldwork_per_hour, 2, '.', ' '),
                "formula" => ""
            ),
            "hotmeal_fieldwork_value" => array(
                "ordernum" => "5.22",
                "name" => "Suma dodataka na topli obrok za rad na terenu",
                "value" => number_format($paycheck->hotmeal_fieldwork_value, 2, '.', ' '),
                "formula" => ""
            ),
            "additionals_nez_empl" => array(
                "ordernum" => "5.23",
                "name" => $paycheck->getAttributeLabel('additionals_nez_empl'),
                "value" => number_format($paycheck->additionals_nez_empl, 2, '.', ' '),
                "formula" => ""
            ),
            "additionals_pio_empl" => array(
                "ordernum" => "5.24",
                "name" => $paycheck->getAttributeLabel('additionals_pio_empl'),
                "value" => number_format($paycheck->additionals_pio_empl, 2, '.', ' '),
                "formula" => ""
            ),
            "prev_pio_empl" => array(
                "ordernum" => "5.25",
                "name" => $paycheck->getAttributeLabel('prev_pio_empl'),
                "value" => number_format($paycheck->prev_pio_empl, 2, '.', ' '),
                "formula" => ""
            ),
            "month_doprinosi_base" => array(
                "ordernum" => "5.25",
                "name" => $paycheck->getAttributeLabel('month_doprinosi_base'),
                "value" => number_format($paycheck->month_doprinosi_base, 2, '.', ' '),
                "formula" => ""
            ),
            'additionals_doprinosi_base' => [
                "ordernum" => "5.27",
                "name" => $paycheck->getAttributeLabel('additionals_doprinosi_base'),
                "value" => number_format($paycheck->additionals_doprinosi_base, 2, '.', ' '),
                "formula" => ""
            ],
            "base_doprinosi_base" => array(
                "ordernum" => "5.25",
                "name" => $paycheck->getAttributeLabel('base_doprinosi_base'),
                "value" => number_format($paycheck->base_doprinosi_base, 2, '.', ' '),
                "formula" => ""
            ),
            "base_pio_empl" => array(
                "ordernum" => "5.25",
                "name" => $paycheck->getAttributeLabel('base_pio_empl'),
                "value" => number_format($paycheck->base_pio_empl, 2, '.', ' '),
                "formula" => ""
            ),
            "doprinosi_base" => array(
                "ordernum" => "5.25",
                "name" => $paycheck->getAttributeLabel('doprinosi_base'),
                "value" => number_format($paycheck->doprinosi_base, 2, '.', ' '),
                "formula" => ""
            ),
            "prev_tax_base" => [
                "ordernum" => "5.26",
                "name" => $paycheck->getAttributeLabel('prev_tax_base'),
                "value" => number_format($paycheck->prev_tax_base, 2, '.', ' '),
                "formula" => ""
            ],
            "additionals_used_tax_release" => [
                "ordernum" => "5.28",
                "name" => $paycheck->getAttributeLabel('additionals_used_tax_release'),
                "value" => number_format($paycheck->additionals_used_tax_release, 2, '.', ' '),
                "formula" => ""
            ],
            "base_bruto" => [
                "ordernum" => "5.28",
                "name" => $paycheck->getAttributeLabel('base_bruto'),
                "value" => number_format($paycheck->base_bruto, 2, '.', ' '),
                "formula" => ""
            ],
            "bonus_bruto" => [
                "ordernum" => "5.28",
                "name" => $paycheck->getAttributeLabel('bonus_bruto'),
                "value" => number_format($paycheck->bonus_bruto, 2, '.', ' '),
                "formula" => ""
            ],
            "base_tax_base" => [
                "ordernum" => "5.27",
                "name" => $paycheck->getAttributeLabel('base_tax_base'),
                "value" => number_format($paycheck->base_tax_base, 2, '.', ' '),
                "formula" => ""
            ],
            "prev_doprinosi_base" => [
                "ordernum" => "5",
                "name" => $paycheck->getAttributeLabel('prev_doprinosi_base'),
                "value" => number_format($paycheck->prev_doprinosi_base, 2, '.', ' '),
                "formula" => ""
            ],
            "work_contract_hours_percent" => [
                "ordernum" => "5",
                "name" => $paycheck->getAttributeLabel('work_contract_hours_percent'),
                "value" => number_format($paycheck->work_contract_hours_percent, 2, '.', ' '),
                "formula" => ""
            ],
            "dependent_working_percent" => [
                "ordernum" => "5",
                "name" => $paycheck->getAttributeLabel('dependent_working_percent'),
                "value" => number_format($paycheck->dependent_working_percent, 2, '.', ' '),
                "formula" => ""
            ],
            "base_zdr_empl" => [
                "ordernum" => "5",
                "name" => $paycheck->getAttributeLabel('base_zdr_empl'),
                "value" => number_format($paycheck->base_zdr_empl, 2, '.', ' '),
                "formula" => ""
            ],
            "additionals_zdr_empl" => [
                "ordernum" => "5",
                "name" => $paycheck->getAttributeLabel('additionals_zdr_empl'),
                "value" => number_format($paycheck->additionals_zdr_empl, 2, '.', ' '),
                "formula" => ""
            ],
            "prev_zdr_empl" => [
                "ordernum" => "5",
                "name" => $paycheck->getAttributeLabel('prev_zdr_empl'),
                "value" => number_format($paycheck->prev_zdr_empl, 2, '.', ' '),
                "formula" => ""
            ],
            "base_nez_empl" => [
                "ordernum" => "5",
                "name" => $paycheck->getAttributeLabel('base_nez_empl'),
                "value" => number_format($paycheck->base_nez_empl, 2, '.', ' '),
                "formula" => ""
            ],
            "prev_nez_empl" => [
                "ordernum" => "5",
                "name" => $paycheck->getAttributeLabel('prev_nez_empl'),
                "value" => number_format($paycheck->prev_nez_empl, 2, '.', ' '),
                "formula" => ""
            ],
            "base_pio_comp" => [
                "ordernum" => "5",
                "name" => $paycheck->getAttributeLabel('base_pio_comp'),
                "value" => number_format($paycheck->base_pio_comp, 2, '.', ' '),
                "formula" => ""
            ],
            "additionals_pio_comp" => [
                "ordernum" => "5",
                "name" => $paycheck->getAttributeLabel('additionals_pio_comp'),
                "value" => number_format($paycheck->additionals_pio_comp, 2, '.', ' '),
                "formula" => ""
            ],
            "prev_pio_comp" => [
                "ordernum" => "5",
                "name" => $paycheck->getAttributeLabel('prev_pio_comp'),
                "value" => number_format($paycheck->prev_pio_comp, 2, '.', ' '),
                "formula" => ""
            ],
            "base_zdr_comp" => [
                "ordernum" => "5",
                "name" => $paycheck->getAttributeLabel('base_zdr_comp'),
                "value" => number_format($paycheck->base_zdr_comp, 2, '.', ' '),
                "formula" => ""
            ],
            "additionals_zdr_comp" => [
                "ordernum" => "5",
                "name" => $paycheck->getAttributeLabel('additionals_zdr_comp'),
                "value" => number_format($paycheck->additionals_zdr_comp, 2, '.', ' '),
                "formula" => ""
            ],
            "prev_zdr_comp" => [
                "ordernum" => "5",
                "name" => $paycheck->getAttributeLabel('prev_zdr_comp'),
                "value" => number_format($paycheck->prev_zdr_comp, 2, '.', ' '),
                "formula" => ""
            ],
            "base_nez_comp" => [
                "ordernum" => "5",
                "name" => $paycheck->getAttributeLabel('base_nez_comp'),
                "value" => number_format($paycheck->base_nez_comp, 2, '.', ' '),
                "formula" => ""
            ],
            "additionals_nez_comp" => [
                "ordernum" => "5",
                "name" => $paycheck->getAttributeLabel('additionals_nez_comp'),
                "value" => number_format($paycheck->additionals_nez_comp, 2, '.', ' '),
                "formula" => ""
            ],
            "prev_nez_comp" => [
                "ordernum" => "5",
                "name" => $paycheck->getAttributeLabel('prev_nez_comp'),
                "value" => number_format($paycheck->prev_nez_comp, 2, '.', ' '),
                "formula" => ""
            ],
        );
        
        $bruto_hour_calc_type = (int)(Yii::app()->configManager->get('accounting.paychecks.bruto_hour_calc_type'));
        
        if($bruto_hour_calc_type === 4)
        {
            unset($calculatedParameters['points']);
        }
        
        $order_num_major = 5;
        $order_num_minor = 1;
        $calculatedParameters_keys = array_keys($calculatedParameters);
        foreach($calculatedParameters_keys as $key)
        {
            $calculatedParameters[$key]['ordernum'] = "$order_num_major.$order_num_minor";
            $order_num_minor++;
        }
        
        if($bruto_hour_calc_type !== 4)
        {
            $calculatedParameters['points']['formula'] = $employeeParameters['paygrade_points']['ordernum']." + ".$employeeParameters['personal_points']['ordernum'];
        }
        $calculatedParameters['worked_hours']['formula'] = 'po_aktivnim_ugovorima - '.$calculatedParameters['additionals_hours']['ordernum'];
//        $calculatedParameters['value_per_hour']['formula'] = '('.$calculatedParameters['points']['ordernum'].'/'.$systemParameters['estimated_average_hours_in_month']['ordernum'].') * '.$accountingMonthParameters['point_value']['ordernum'];
        
        if($bruto_hour_calc_type === 3)
        {
            $calculatedParameters['value_per_hour']['formula'] = $calculatedParameters['points']['ordernum'].' * '.$accountingMonthParameters['point_value']['ordernum'];

        }
        else if($bruto_hour_calc_type !== 4)
        {
            $calculatedParameters['value_per_hour']['formula'] = '('.$calculatedParameters['points']['ordernum'].'/'.$systemParameters['estimated_average_hours_in_month']['ordernum'].') * '.$accountingMonthParameters['point_value']['ordernum'];
        }
        
        
        $calculatedParameters['work_on_state_holiday_value']['formula'] = $employeeParameters['work_on_state_holiday_hours']['ordernum'].' * '.$calculatedParameters['value_per_hour']['ordernum'].' * '.$systemParameters['work_on_state_holiday_percentage']['ordernum'];
        $calculatedParameters['worked_hours_value']['formula'] = $calculatedParameters['worked_hours']['ordernum'].' * '.$calculatedParameters['value_per_hour']['ordernum'];
        $calculatedParameters['overtime_value']['formula'] = $employeeParameters['overtime_hours']['ordernum'].' * '.$calculatedParameters['value_per_hour']['ordernum'].' * '.$systemParameters['overtime_percentage']['ordernum'];
        $calculatedParameters['regular_work_hours']['formula'] = $calculatedParameters['worked_hours']['ordernum'].' + '.$employeeParameters['overtime_hours']['ordernum'].' + '.$employeeParameters['work_on_state_holiday_hours']['ordernum'];
        $calculatedParameters['regular_work_value']['formula'] = $calculatedParameters['worked_hours_value']['ordernum'].' + '.$calculatedParameters['overtime_value']['ordernum'].' + '.$calculatedParameters['work_on_state_holiday_value']['ordernum'];
        $calculatedParameters['hot_meal_value']['formula'] = $accountingMonthParameters['hot_meal_per_hour']['ordernum'].' * '.$calculatedParameters['regular_work_hours']['ordernum'];
        $calculatedParameters['past_exp_value']['formula'] = $calculatedParameters['regular_work_value']['ordernum'].' * '.$accountingMonthParameters['exp_perc']['ordernum'].'/100 * '.$calculatedParameters['year_experience']['ordernum'];
        $calculatedParameters['performance_value']['formula'] = $calculatedParameters['regular_work_value']['ordernum'].' * ('.$employeeParameters['performance_percent']['ordernum'].' - 100)/100';
        $calculatedParameters['probationer_value']['formula'] = $calculatedParameters['regular_work_value']['ordernum'].' * ('.$employeeParameters['probationer_percent']['ordernum'].' - 100)/100';
        $calculatedParameters['work_sum']['formula'] = $calculatedParameters['regular_work_value']['ordernum']
                .' + '.$calculatedParameters['past_exp_value']['ordernum']
                .' + '.$calculatedParameters['performance_value']['ordernum']
                .' + '.$calculatedParameters['probationer_value']['ordernum'];
        $calculatedParameters['hotmeal_fieldwork_value']['formula'] = $calculatedParameters['regular_work_hours']['ordernum']
                .' * '.$calculatedParameters['hotmeal_fieldwork_per_hour']['ordernum'];
        $calculatedParameters['base_tax_base']['formula'] = $calculatedParameters['base_bruto']['ordernum']
                .' + '.$calculatedParameters['hot_meal_value']['ordernum']
                .' + '.$calculatedParameters['hotmeal_fieldwork_value']['ordernum']
                .' + '.$accountingMonthParameters['regres']['ordernum']
                .' + '.$calculatedParameters['bonus_bruto']['ordernum'];
        $calculatedParameters['base_pio_empl']['formula'] = $calculatedParameters['base_doprinosi_base']['ordernum']
                .' * '.$accountingMonthParameters['pio_empl_perc']['ordernum'];
        $calculatedParameters['base_doprinosi_base']['formula'] = $calculatedParameters['month_doprinosi_base']['ordernum']
                .' - '.$calculatedParameters['additionals_doprinosi_base']['ordernum']
                .' - zatim se ovo "leveluje"';
        $calculatedParameters['doprinosi_base']['formula'] = $calculatedParameters['base_doprinosi_base']['ordernum']
                .' + '.$calculatedParameters['additionals_doprinosi_base']['ordernum']
                .' - '.$calculatedParameters['prev_doprinosi_base']['ordernum'];
        $calculatedParameters['month_doprinosi_base']['formula'] = $accountingMonthParameters['month_min_contribution_base']['ordernum']
                .' * '.$calculatedParameters['work_contract_hours_percent']['ordernum']
                .' * '.$calculatedParameters['dependent_working_percent']['ordernum'];
        $calculatedParameters['base_zdr_empl']['formula'] = $calculatedParameters['base_doprinosi_base']['ordernum']
                .' * '.$accountingMonthParameters['zdr_empl_perc']['ordernum'];
        
        if($bruto_hour_calc_type === 4)
        {
//            unset($calculatedParameters['points']);
            $calculatedParameters['value_per_hour']['formula'] = $employeeParameters['personal_points']['ordernum'].', ako je prethodno 0 onda '.$employeeParameters['paygrade_points']['ordernum'];
        }
        
        return $calculatedParameters;
    }
    
    public static function createArrayForFinalCalculatedParametersTable(Paycheck $paycheck)
    {
        $result = [
            'amount_bruto' => array(
                "ordernum" => "6.1",
                "name" => "Mali Bruto",
                "value" => SIMAHtml::money_format($paycheck->amount_bruto),
                "formula" => '',
                "bolded" => true
            ),
            'pio_empl' => array(
                "ordernum" => "6.2",
                "name" => "PIO zaposleni",
                "value" => SIMAHtml::money_format($paycheck->pio_empl),
                "formula" => ''
            ),
            'zdr_empl' => array(
                "ordernum" => "6.3",
                "name" => "Zdrav. zapos.",
                "value" => SIMAHtml::money_format($paycheck->zdr_empl),
                "formula" => ''
            ),
            'nez_empl' => array(
                "ordernum" => "6.4",
                "name" => "Osig. zaposleni",
                "value" => SIMAHtml::money_format($paycheck->nez_empl),
                "formula" => ''
            ),
            'tax_base' => array(
                "ordernum" => "6.5",
                "name" => "Osnovica poreza",
                "value" => SIMAHtml::money_format($paycheck->tax_base),
                "formula" => ''
            ),
            'tax_empl' => array(
                "ordernum" => "6.6",
                "name" => "Porez",
                "value" => SIMAHtml::money_format($paycheck->tax_empl),
                "formula" => ""
            ),
            'amount_neto' => array(
                "ordernum" => "6.7",
                "name" => "Neto",
                "value" => SIMAHtml::money_format($paycheck->amount_neto),
                "formula" => "",
                "bolded" => true
            ),
            'pio_comp' => array(
                "ordernum" => "6.8",
                "name" => "PIO firma",
                "value" => SIMAHtml::money_format($paycheck->pio_comp),
                "formula" => ""
            ),
            'zdr_comp' => array(
                "ordernum" => "6.9",
                "name" => "Zdrav. firma",
                "value" => SIMAHtml::money_format($paycheck->zdr_comp),
                "formula" => ""
            ),
            'nez_comp' => array(
                "ordernum" => "6.10",
                "name" => "Osig. firma",
                "value" => SIMAHtml::money_format($paycheck->nez_comp),
                "formula" => ""
            ),
            'amount_total' => array(
                "ordernum" => "6.11",
                "name" => "Total",
                "value" => SIMAHtml::money_format($paycheck->amount_total),
                "formula" => "",
                "bolded" => true
            ),
            'amount_neto_for_payout' => array(
                "ordernum" => "6.12",
                "name" => "Za isplatu",
                "value" => SIMAHtml::money_format($paycheck->amount_neto_for_payout),
                "formula" => "",
                "bolded" => true
            )
        ];
        
        // 3
        $accountingMonthParameters = PaychecksEmployee::createArrayForAccountingMonthParametersTable($paycheck);
        
        $calculatedParameters = PaychecksEmployee::createArrayForCalculatedParametersTable($paycheck);
        
        $result['pio_empl']['formula'] = $calculatedParameters['base_pio_empl']['ordernum']
                .' + '.$calculatedParameters['additionals_pio_empl']['ordernum']
                .' - '.$calculatedParameters['prev_pio_empl']['ordernum'];
        $result['zdr_empl']['formula'] = $calculatedParameters['base_zdr_empl']['ordernum']
                .' + '.$calculatedParameters['additionals_zdr_empl']['ordernum']
                .' - '.$calculatedParameters['prev_zdr_empl']['ordernum'];
        $result['nez_empl']['formula'] = $calculatedParameters['base_nez_empl']['ordernum']
                .' + '.$calculatedParameters['additionals_nez_empl']['ordernum']
                .' - '.$calculatedParameters['prev_nez_empl']['ordernum'];
        $result['tax_base']['formula'] = $calculatedParameters['base_tax_base']['ordernum']
                .' + ('.$calculatedParameters['additionals_sum']['ordernum'].' - '.$calculatedParameters['additionals_used_tax_release']['ordernum'].')'
                .' - '.$calculatedParameters['prev_tax_base']['ordernum'];
        $result['tax_empl']['formula'] = $result['tax_base']['ordernum']
                .' * '.$accountingMonthParameters['tax_empl_perc']['ordernum'].'/100';
        $result['amount_neto']['formula'] = $result['amount_bruto']['ordernum']
                .' - '.$result['pio_empl']['ordernum']
                .' - '.$result['zdr_empl']['ordernum']
                .' - '.$result['nez_empl']['ordernum']
                .' - '.$result['tax_empl']['ordernum'];
        $result['pio_comp']['formula'] = $calculatedParameters['base_pio_comp']['ordernum']
                .' + '.$calculatedParameters['additionals_pio_comp']['ordernum']
                .' - '.$calculatedParameters['prev_pio_comp']['ordernum'];
        $result['zdr_comp']['formula'] = $calculatedParameters['base_zdr_comp']['ordernum']
                .' + '.$calculatedParameters['additionals_zdr_comp']['ordernum']
                .' - '.$calculatedParameters['prev_zdr_comp']['ordernum'];
        $result['nez_comp']['formula'] = $calculatedParameters['base_nez_comp']['ordernum']
                .' + '.$calculatedParameters['additionals_nez_comp']['ordernum']
                .' - '.$calculatedParameters['prev_nez_comp']['ordernum'];
        $result['amount_total']['formula'] = $result['amount_bruto']['ordernum']
                .' + '.$result['pio_comp']['ordernum']
                .' + '.$result['zdr_comp']['ordernum']
                .' + '.$result['nez_comp']['ordernum'];
        $result['amount_neto_for_payout']['formula'] = $result['amount_neto']['ordernum']
                .' - '.$calculatedParameters['suspensions_sum']['ordernum']
                .' - '.$calculatedParameters['deductions_sum']['ordernum'];
        
        return $result;
    }
    
    //TODO: prepraviti da vraca samo za jednog zaposlenog, tako se i koristi
    public static function getEmployeesDataInPaycheckPeriod(PaycheckPeriod $paycheckPeriod, Employee $emp=null, $eventUpdatePercent=null, $signedWorkContracts=true)
    {        
        $eventUpdatePercentPerSegment = 0;
        if(isset($eventUpdatePercent))
        {
            $eventUpdatePercentPerSegment = $eventUpdatePercent/3;
            $eventSourceStepsSum = 0.0;
            Yii::app()->controller->sendUpdateEvent($eventSourceStepsSum);
        }
        
        $employees = [];
        
        $periodMonth = $paycheckPeriod->month;
        $previousMonth = $periodMonth->prev();
                
        $work_contracts_model_filter = [
            'signed' => $signedWorkContracts,
            'for_payout' => true,
            'active_at' => $paycheckPeriod->month,
            'display_scopes' => array(
                'orderByStartDateASC'
            )
        ];
                
        if(isset($emp))
        {
            $work_contracts_model_filter['employee'] = [
                'ids' => $emp->id
            ];
        }
                
        $wcCriteria = new SIMADbCriteria([
            'Model' => 'WorkContract',
            'model_filter' => $work_contracts_model_filter
        ]);
        
        $work_contracts = WorkContract::model()->with('employee')->findAll($wcCriteria);
        $this_period_first_day = $periodMonth->firstDay();
        $this_period_last_day = $periodMonth->lastDay();
        
        if(isset($eventUpdatePercent))
        {
            $work_contracts_count = count($work_contracts);
            if($work_contracts_count > 0)
            {
                $eventSourceSteps = ($eventUpdatePercentPerSegment/$work_contracts_count);
            }
            else
            {
                $eventSourceStepsSum += $eventUpdatePercentPerSegment;
                Yii::app()->controller->sendUpdateEvent($eventSourceStepsSum);
            }
        }
        
        $periodMonth_first_day = $periodMonth->firstDay();
        $periodMonth_first_day_datetime = new DateTime($periodMonth_first_day->day_date);
        $periodMonth_last_day = $periodMonth->lastDay();
                
        $max_execution_time = ini_get('max_execution_time');
        foreach($work_contracts as $wc)
        {
            set_time_limit(5);
            
            $employees[$wc->employee->id]['emp_model'] = $wc->employee;
            $employees[$wc->employee->id]['work_contracts'][] = $wc;
            
            if(!isset($employees[$wc->employee->id]['last_12_months_hours']))
            {
                $employees[$wc->employee->id]['last_12_months_hours'] = 0;
            }
            if(!isset($employees[$wc->employee->id]['last_12_months_value']))
            {
                $employees[$wc->employee->id]['last_12_months_value'] = 0;
            }
               
            $getPrevious12MonthsValues = PaychecksEmployee::GetEmployeesPrevious12MonthsValuesForPeriod($wc->employee, $paycheckPeriod);

            $employees[$wc->employee->id]['last_12_months_hours'] = $getPrevious12MonthsValues['hours'];
            $employees[$wc->employee->id]['last_12_months_value'] = $getPrevious12MonthsValues['value'];
            
            $date1 = new DateTime($wc->start_date);
            if($date1 < $periodMonth_first_day_datetime)
            {
                $date1 = $periodMonth_first_day_datetime;
            }
            $date2 = new DateTime($wc->getEndDate($periodMonth_last_day));
            $number_of_calendar_days = $date2->diff($date1)->format("%a")+1;
            if(isset($employees[$wc->employee->id]['number_of_calendar_days']))
            {
                $employees[$wc->employee->id]['number_of_calendar_days'] += $number_of_calendar_days;
            }
            else
            {
                $employees[$wc->employee->id]['number_of_calendar_days'] = $number_of_calendar_days;
            }
            
            if(!isset($employees[$wc->employee->id]['work_contracts_hours_sum']))
            {
                $employees[$wc->employee->id]['work_contracts_hours_sum'] = 0;
            }
            $employees[$wc->employee->id]['work_contracts_hours_sum'] += $wc->getWorkedHours($this_period_first_day, $this_period_last_day);
            
            if(isset($eventUpdatePercent))
            {
                $eventSourceStepsSum += $eventSourceSteps;
                Yii::app()->controller->sendUpdateEvent($eventSourceStepsSum);
            }
        }
        set_time_limit($max_execution_time);
        
        $lastDay = $previousMonth->lastDay();
        
        if(isset($eventUpdatePercent))
        {
            $employees_count = count($employees);
            if($employees_count > 0)
            {
                $eventSourceSteps = ($eventUpdatePercentPerSegment/$employees_count);
            }
            else
            {
                $eventSourceStepsSum += $eventUpdatePercentPerSegment;
                Yii::app()->controller->sendUpdateEvent($eventSourceStepsSum);
            }
        }
        
        foreach($employees as $key => $value)
        {
            set_time_limit(5);
            
            $worked_days = $value['emp_model']->getWorkExperience(null, $lastDay);
            $work_on_state_holiday_hours = $value['emp_model']->getWorkOnStateHolidayHours($this_period_first_day, $this_period_last_day);
                        
            $employees[$key]['worked_days'] = $worked_days;
            $employees[$key]['work_on_state_holiday_hours'] = $work_on_state_holiday_hours;
            
            $value_per_hour = 0;
            if(isset($value['last_12_months_hours']) && isset($value['last_12_months_value']) 
                && $value['last_12_months_hours'] > 0 && $value['last_12_months_value']>0)
            {
                $last_12_months_hours = $value['last_12_months_hours'];
                $last_12_months_value = $value['last_12_months_value'];

                $value_per_hour = $last_12_months_value/$last_12_months_hours;
            }
            else
            {
                $estimated_average_hours_in_month = Yii::app()->configManager->get('accounting.paychecks.estimated_average_hours_in_month');
                $point_value = $paycheckPeriod->month_point_value;
                $first_work_contract = $value['work_contracts'][0];
                $worked_points_in_month = $value['emp_model']->personal_points;
                $work_position_points = $first_work_contract->work_position->paygrade->points;
                $worked_points_in_month += $work_position_points;
                $value_per_hour = ($worked_points_in_month * $point_value)/$estimated_average_hours_in_month;
            }
                                    
            $employees[$key]['last_12_months_value_per_hour'] = $value_per_hour;
            
            if(isset($eventUpdatePercent))
            {
                $eventSourceStepsSum += $eventSourceSteps;
                Yii::app()->controller->sendUpdateEvent($eventSourceStepsSum);
            }
        }
        set_time_limit($max_execution_time);
                                
        return $employees;
    }
    
    /**
     * funckija racuna i vraca broj sati i vrednost sati iz prethodnih 12 meseci
     */
    public static function GetEmployeesPrevious12MonthsValuesForPeriod(Employee $emp, PaycheckPeriod $paycheckPeriod)
    {
        $result = [
            'hours' => 0,
            'value' => 0
        ];
        
        $periodMonth = $paycheckPeriod->month;
        $previousMonth = $periodMonth->prev();
        $previous12thMonth = Month::get($periodMonth->month, $periodMonth->year->prev());
        $paychecks_model_filter = [
            'paycheck_period' => [
                'month' => [
                    'period' => [$previous12thMonth, $previousMonth]
                ],
                'pp_type' => PaycheckPeriod::$FINAL,
            ],
            'employee' => [
                'ids' => $emp->id
            ]
        ];
        $paychecksCriteria = new SIMADbCriteria([
            'Model' => 'Paycheck',
            'model_filter' => $paychecks_model_filter
        ]);
        $paychecksInPrev12Months = Paycheck::model()->findAll($paychecksCriteria);
        foreach($paychecksInPrev12Months as $pc)
        {
            $result['hours'] += $pc->worked_hours;
//            $result['value'] += $pc->worked_hours_value + $pc->past_exp_value + $pc->regres + $pc->hot_meal_value;
            $result['value'] += 
                    $pc->worked_hours_value 
                    + $pc->performance_value 
                    + $pc->probationer_value 
                    + $pc->past_exp_value //work_sum
                    + $pc->regres 
                    + $pc->hot_meal_value //base_bruto - ali u prethodnim zaradama to nije base_bruto
                    ;
//            error_log("$pc->worked_hours_value 
//                    + $pc->performance_value 
//                    + $pc->probationer_value 
//                    + $pc->past_exp_value 
//                    + $pc->regres ");
//            error_log($result['value'].'  |   '.$result['hours']);
//            $result['value'] += $pc->work_sum + $pc->regres + $pc->hot_meal_value; //ovde su probni rad i ucinak
        }
        
        return $result;
    }
    
    public static function DidEmpChangedWorkPositionSincePreviousPeriod(Employee $emp, PaycheckPeriod $paycheckPeriod, WorkContract $current_wc=null)
    {
        $result = false;
        
        if(is_null($current_wc))
        {
            $currentWcCriteria = new SIMADbCriteria([
                'Model' => 'WorkContract',
                'model_filter' => [
                    'signed' => true,
                    'for_payout' => true,
                    'active_at' => $paycheckPeriod->month,
                    'employee' => [
                        'ids' => $emp->id
                    ]
                ]
            ]);
            $current_wc = WorkContract::model()->find($currentWcCriteria);
        }
        
        $previousWcCriteria = new SIMADbCriteria([
            'Model' => 'WorkContract',
            'model_filter' => [
                'signed' => true,
                'for_payout' => true,
                'active_at' => $paycheckPeriod->month->prev(),
                'employee' => [
                    'ids' => $emp->id
                ]
            ]
        ]);
        $previous_wc = WorkContract::model()->find($previousWcCriteria);
        
        if(isset($current_wc) && isset($previous_wc)
                && $current_wc->id !== $previous_wc->id
                && $current_wc->work_position_id !== $previous_wc->work_position_id)
        {
            $result = true;
        }
        
        return $result;
    }
}
