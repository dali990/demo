<?php

/**
 * @package SIMA
 * @subpackage Accounting
 */
class YearAccountingBehavior extends SIMAActiveRecordBehavior
{
    
    public function relations($event)
    {
        $event->addResult(array(
            'accounting'=>array(SIMAActiveRecord::HAS_ONE, 'AccountingYear','id')
        ));
    } 
}