<?php

/**
 * @package SIMA
 * @subpackage Accounting
 */
class AccountOrderReCheckBehavior extends SIMAActiveRecordBehavior
{
    private $allowedDiff = 0.01;
    
    private $old_transactions = []; //stare transakcije, ucitavaju se iz baze
    private $new_transactions = []; //nove transakcije, izracunavaju se
    private $pairs = [];//niz parova transakcija => ['old' => $stara_transakcija, 'new' => $nova transakcija] - 
    private $diffs = [];//razllike izmedju transakcija
    private $diffs_per_document = []; //razlike upakovane po dokumentu, spremno za diff_viewer
    private $diff_calculated = false; //postavlja se na true kada se obracunaju razlike. Nije realno da je potrebno dva puta racunati razlike u jednom php pozivu
    
    
    /**
     * uporedjuje da li su dva account-a ista. Ne gledaju se zadati account-i, nego njihova deca koja mogu da se knjize
     * @param Account $account1 prvi acocunt
     * @param Account $account2 drugi account
     * @return type
     * @throws CDbException
     */
    private function isSameAccount(Account $account1, Account $account2)
    {
        //TODO(MilosS): proveriti da li ima smisla i dalje da se koriste try catch
        //TODO(MilosS): ovo mislim da moze da se prebaci u Account.php
        try
        {
            $_account1 = $account1->getLeafAccount($account1->LinkedModel);
        }
        catch (CDbException $e)
        {
            error_log(__METHOD__.' -1- '.SIMAMisc::toTypeAndJsonString($account1->model));
            throw $e;
        }

        try
        {
            $_account2 = $account2->getLeafAccount($account2->LinkedModel);
        }
        catch (CDbException $e)
        {
            error_log(__METHOD__.' -2- '.SIMAMisc::toTypeAndJsonString($account2->model));
            throw $e;
        }
        
        return $_account1->id === $_account2->id;
    }
    
    /**
     * proverava da li su iste transakcije, ali ne po ID, nego po vrednostima
     * TODO(MilosS): prebaciti u AccountTransaction
     * @param AccountTransaction $old
     * @param AccountTransaction $new
     * @return type
     */
    private function areSAME(AccountTransaction $old, AccountTransaction $new)
    {
        return (
                $old->account_document_id == $new->account_document_id 
                && 
//                    substr($old->account->code, 0, 6) == substr($new->account->code, 0, 6)
//                $old->account_id == $new->account_id
                $this->isSameAccount($old->account, $new->account)
                &&
                $old->isDebit == $new->isDebit
                &&
                SIMAMisc::areEqual($old->debit, $new->debit, $this->allowedDiff)
                &&
                SIMAMisc::areEqual($old->credit, $new->credit, $this->allowedDiff)
                &&
                $old->comment === $new->comment
                );
    }
    
    /**
     * za niz parova transakcija vraca statuse ADD, REMOVE, SAME ili MODIFY
     * @param array $pairs niz parova ['old'=>$old, 'new'=>$new]
     * @return array niz elemenata koji su niz u obliku
     * 
     * 
     *          'document' => $new->account_document, - kom dokumentu pripada transakcija
                'changes' => 1, - broj promena u tom dokumentu (ova funkcija vraca uvek 1, ali se to kasnije merguje)
                'transactions' => [
                    [//samo jedan element
                        'status' => 'ADD', //status promene
                        'message' => $_msg, // tekst poruke (za konzolu i testiranje)
                        'old_model' => null, // stari model - ako postji
                        'new_model' => $new, // novi model - ako postoji
     * 
     *              // potvrbne vrednosti za izvrsenje promene - za brisanje ID, za novu - samo vrednosti, za izmenu ID i nove vrednosti
     *              // u sustini kada se radi primena izmena, samo je ovo dovoljno poslati
                        'changes' => [ 
                            'status' => 'ADD',
//                                'id' => $new->id, ovo ne treba za dodavanje
                            'debit' => $new->debit,
                            'credit' => $new->credit,
                            'account_document_id' => $new->account_document_id,
                            'account_id' => $new->account_id,
                        ]
                    ]
                ]
     * 
     */
    private function compareTransactions()
    {
        $this->diffs = [];
        foreach ($this->pairs as $pair)
        {
            $old = $pair['old'];
            $new = $pair['new'];
            if (is_null($new) && is_null($old))
            {
                continue;
            }
            elseif (is_null($old))
            {
                if ($new->debit>0)
                {
                    $_msg = "D $new->account: $new->debit";
                }
                else
                {
                    $_msg = "C $new->account: $new->credit";
                }
                $account_display_name = $new->getAttributeLabel('account');
                $debit_display_name = $new->getAttributeLabel('debit');
                $credit_display_name = $new->getAttributeLabel('credit');
                $comment = $new->getAttributeLabel('comment');
                $this->diffs[] = [
                    'type' => 'group',
                    'group_id' => $new->account_document_id,
                    'group_name' => $new->account_document->DisplayName,
                    'document_id' => $new->account_document_id,
                    'columns' => [$account_display_name, [$debit_display_name, 'align'=>'right'], [$credit_display_name, 'align'=>'right'], $comment],
                    'changes' => 1,
                    'items' => [
                        [
                            'status' => 'ADD',
                            'message' => $_msg,
                            'columns' => [
                                $account_display_name=> [
                                    'new_value'=>$new->account->DisplayName
                                ],
                                $debit_display_name=> [
                                    'new_value'=>SIMAHtml::number_format($new->debit)
                                ],
                                $credit_display_name=> [
                                    'new_value'=>SIMAHtml::number_format($new->credit)
                                ],
                                $comment=>[
                                    'new_value'=>$new->comment
                                ]
                            ],
                            'data' => [
                                'status' => 'ADD',
                                'model' => 'AccountTransaction',
                                'attributes' => [
                                    'debit' => $new->debit,
                                    'credit' => $new->credit,
                                    'account_document_id' => $new->account_document_id,
                                    'account_id' => $new->account_id,
                                    'comment' => $new->comment
                                ]
                            ]
                        ]
                    ]
                ];
            }
            elseif (is_null($new))
            {
                if ($old->debit>0)
                {
                    $_msg = "D $old->account: $old->debit";
                }
                else
                {
                    $_msg = "C $old->account: $old->credit";
                }
                $account_display_name = $old->getAttributeLabel('account');
                $debit_display_name = $old->getAttributeLabel('debit');
                $credit_display_name = $old->getAttributeLabel('credit');
                $comment = $old->getAttributeLabel('comment');
                $this->diffs[] = [
                    'type' => 'group',
                    'group_id' => $old->account_document_id,
                    'group_name' => $old->account_document->DisplayName,
                    'document_id' => $old->account_document_id,
                    'columns' => [$account_display_name, [$debit_display_name, 'align'=>'right'], [$credit_display_name, 'align'=>'right'], $comment],
                    'changes' => 1,
                    'items' => [
                        [
                            'status' => 'REMOVE',
                            'message' => $_msg,
                            'columns' => [
                                $account_display_name=> [
                                    'old_value'=>$old->account->DisplayName,
                                ],
                                $debit_display_name=> [
                                    'old_value'=>SIMAHtml::number_format($old->debit),
                                ],
                                $credit_display_name=> [
                                    'old_value'=>SIMAHtml::number_format($old->credit),
                                ],
                                $comment=> [
                                    'old_value'=>$old->comment,
                                ],
                            ],
                            'data' => [
                                'status' => 'REMOVE',
                                'model' => 'AccountTransaction',
                                'model_id' => $old->id,
                            ]
                        ]
                    ]
                ];
            }
            else
            {
                if ($this->areSAME($old,$new))
                {
                    $account_display_name = $new->getAttributeLabel('account');
                    $debit_display_name = $new->getAttributeLabel('debit');
                    $credit_display_name = $new->getAttributeLabel('credit');
                    $comment = $new->getAttributeLabel('comment');
                    $this->diffs[] = [
                        'type' => 'group',
                        'group_id' => $old->account_document_id,
                        'group_name' => $old->account_document->DisplayName,
                        'document_id' => $old->account_document_id,
                        'columns' => [$account_display_name, [$debit_display_name, 'align'=>'right'], [$credit_display_name, 'align'=>'right'], $comment],
                        'changes' => 0,
                        'items' => [
                            [
                                'status' => 'SAME',
                                'message' => '',
                                'columns' => [
                                    $account_display_name=> [
                                        'new_value'=>$new->account->DisplayName
                                    ],
                                    $debit_display_name=> [
                                        'new_value'=>SIMAHtml::number_format($new->debit)
                                    ],
                                    $credit_display_name=> [
                                        'new_value'=>SIMAHtml::number_format($new->credit)
                                    ],
                                    $comment=> [
                                        'new_value'=>$new->comment
                                    ],
                                ],
                                'data' => [
                                ]
                            ]
                        ]
                    ];
                }
                else
                {
                    $this->diffs[] = $this->getDiffJSON($old,$new);
                }
            }
        }
    }
    
    
    private function getDiffJSON($old,$new)
    {
        $_changed = [];
        if ($old->isDebit != $new->isDebit)
        {
            if ($old->isDebit)
            {
                $_msg = 'D -> C';
                $_old_side = 'debit';
                $_new_side = 'credit';
            }
            else
            {
                $_msg = 'C -> D';
                $_old_side = 'credit';
                $_new_side = 'debit';
            }
            $_changed[] = 'credit_debit';
        }
        else
        {
            if ($old->isDebit)
            {
                $_msg = 'D';
                $_old_side = 'debit';
                $_new_side = 'debit';
            }
            else
            {
                $_msg = 'C';
                $_old_side = 'credit';
                $_new_side = 'credit';
            }
        }
        
        $_msg .= ' | ACC: '.$old->account->DisplayName;
//        if ($old->account_id != $new->account_id)
        if (!$this->isSameAccount($old->account, $new->account))
        {
            $_msg .= ' -> '.$new->account->DisplayName;
            $_changed[] = 'account';
        }
        
        $_msg .= ' | VALUE: '.$old->$_old_side;
        if (!SIMAMisc::areEqual($old->$_old_side, $new->$_new_side, $this->allowedDiff))
        {
            $_msg .= ' -> '.$new->$_new_side;
            $_changed[] = 'value';
        }
        
        $_msg .= ' | COMMENT: '.$old->comment;
        if ($old->comment !== $new->comment)
        {
            $_msg .= ' -> '.$new->comment;
            $_changed[] = 'comment';
        }
        
        $account_display_name = $new->getAttributeLabel('account');
        $account_value = ['old_value'=>$old->account->DisplayName];
        if (in_array('account', $_changed))
        {
            $account_value['new_value'] = $new->account->DisplayName;
        }
        
        $debit_display_name = $new->getAttributeLabel('debit');
        $credit_display_name = $new->getAttributeLabel('credit');
        $debit_value = ['old_value'=>SIMAHtml::number_format($old->debit)];
        $credit_value = ['old_value'=>SIMAHtml::number_format($old->credit)];
        if (in_array('value', $_changed) || in_array('credit_debit', $_changed))
        {
            $debit_value['new_value'] = SIMAHtml::number_format($new->debit);
            $credit_value['new_value'] = SIMAHtml::number_format($new->credit);
        }
        
        $comment = $new->getAttributeLabel('comment');
        $comment_value = ['old_value'=>$old->comment];
        if (in_array('comment', $_changed))
        {
            $comment_value['new_value'] = $new->comment;
        }
        
        return [
            'type' => 'group',
            'group_id' => $old->account_document_id,
            'group_name' => $old->account_document->DisplayName,
            'document_id' => $old->account_document_id,
            'columns' => [$account_display_name, [$debit_display_name,'align'=>'right'], [$credit_display_name, 'align'=>'right'], $comment],
            'changes' => 1,
            'items' => [
                [
                    'status' => 'MODIFY',
                    'message' => $_msg,
                    'changed' => $_changed,
                    'columns' => [
                        $account_display_name=>$account_value,
                        $debit_display_name=>$debit_value,
                        $credit_display_name=>$credit_value,
                        $comment=>$comment_value,
                    ],
                    'data' => [
                        'status' => 'MODIFY',
                        'model' => 'AccountTransaction',
                        'model_id' => $old->id,
                        'attributes' => [
                            'debit' => $new->debit,
                            'credit' => $new->credit,
                            'account_id' => $new->account_id,
                            'comment' => $new->comment
                        ]
                    ]
                ]
            ]
        ];
    }
    
    private function mergeDiffsPerDocuments()
    {
        $this->diffs_per_document = [];
        foreach ($this->diffs as $_diff)
        {
            $doc_found = false;
            foreach ($this->diffs_per_document as $doc_key => $doc)
            {
                if (intval($doc['document_id']) == intval($_diff['document_id']))
                {
                    $this->diffs_per_document[$doc_key]['items'][] = $_diff['items'][0];
                    $this->diffs_per_document[$doc_key]['changes'] += $_diff['changes'];
                    $doc_found = true;
                }
            }
            if (!$doc_found)
            {
                $this->diffs_per_document[] = $_diff;
            }
        }
    }
    
    /**
     * Upari transakcije
     * @param type $updateFunction
     */
    private function pairTransactions($updateFunction)
    {
        $this->pairs = []; //niz parova transakcija
        //TOTALNO ISTI
        $checks_count = 4 * count($this->old_transactions) * count($this->new_transactions);
        $i = 0;

        $_match_exists1 = true;
        
        while ($_match_exists1)
        {
            $_match_exists1 = false;
            foreach ($this->old_transactions as $old_key => $old)
            {
                if ($_match_exists1) {break;}
                foreach ($this->new_transactions as $new_key => $new)
                {
                    if ($_match_exists1) {break;}
                    set_time_limit(1);
                    if ($this->areSAME($old,$new))
                    {
                        $this->pairs[] = [
                            'old' => $old,
                            'new' => $new
                        ];
                        unset($this->old_transactions[$old_key]);
                        unset($this->new_transactions[$new_key]);
                        $_match_exists1 = true;
                    }
                    $i++;
                    $updateFunction($i*100.0 / $checks_count);
                }
            }
        }
        
        //ista konta, isto D/C,  razlicie vrednosti
        $_match_exists2 = true;
        
        while ($_match_exists2)
        {
            $_match_exists2 = false;
            foreach ($this->old_transactions as $old_key => $old)
            {
                if ($_match_exists2) {break;}
                foreach ($this->new_transactions as $new_key => $new)
                {
                    if ($_match_exists2) {break;}
                    set_time_limit(1);
                    if (
                        $old->account_document_id == $new->account_document_id 
                        &&
                        $old->isDebit == $new->isDebit
                        &&
                        $this->isSameAccount($old->account, $new->account)
                        )
                    {
                        $this->pairs[] = [
                            'old' => $old,
                            'new' => $new
                        ];
                        unset($this->old_transactions[$old_key]);
                        unset($this->new_transactions[$new_key]);
                        $_match_exists2 = true;
                    }
                    $i++;
                    $updateFunction($i*100.0 / $checks_count);
                }
            }
        }
        
        
        //ista konta, razlicito D/C,  iste vrednosti
        
        $_match_exists3 = true;
        
        while ($_match_exists3)
        {
            $_match_exists3 = false;
            foreach ($this->old_transactions as $old_key => $old)
            {
                if ($_match_exists3) {break;}
                foreach ($this->new_transactions as $new_key => $new)
                {
                    if ($_match_exists3) {break;}
                    set_time_limit(1);
                    if (
                        $old->account_document_id == $new->account_document_id 
                        &&
                        $this->isSameAccount($old->account, $new->account)
                        &&
                        SIMAMisc::areEqual($old->debit, $new->credit, $this->allowedDiff)
                        &&
                        SIMAMisc::areEqual($old->credit, $new->debit, $this->allowedDiff)
                        )
                    {
                        $this->pairs[] = [
                            'old' => $old,
                            'new' => $new
                        ];
                        unset($this->old_transactions[$old_key]);
                        unset($this->new_transactions[$new_key]);
                        $_match_exists3 = true;
                    }
                    $i++;
                    $updateFunction($i*100.0 / $checks_count);
                }
            }
        }
        
         //razlicita konta, isto D/C,  iste vrednosti
        $_match_exists4 = true;
        
        while ($_match_exists4)
        {
            $_match_exists4 = false;
            foreach ($this->old_transactions as $old_key => $old)
            {
                if ($_match_exists4) {break;}
                foreach ($this->new_transactions as $new_key => $new)
                {
                    if ($_match_exists4) {break;}
                    set_time_limit(1);
                    if (
                        $old->account_document_id == $new->account_document_id 
                        &&
                        $old->isDebit == $new->isDebit
                        && 
                        SIMAMisc::areEqual($old->debit, $new->debit, $this->allowedDiff)
                        &&
                        SIMAMisc::areEqual($old->credit, $new->credit, $this->allowedDiff)
                        )
                    {
                        $this->pairs[] = [
                            'old' => $old,
                            'new' => $new
                        ];
                        unset($this->old_transactions[$old_key]);
                        unset($this->new_transactions[$new_key]);
                        $_match_exists4 = true;
                    }
                    $i++;
                    $updateFunction($i*100.0 / $checks_count);
                }
            }
        }
        
        set_time_limit(50);
        foreach ($this->old_transactions as $old)
        {
            $this->pairs[] = [
                'old' => $old,
                'new' => null
            ];
        }
        foreach ($this->new_transactions as $new)
        {
            $this->pairs[] = [
                'old' => null,
                'new' => $new
            ];
        }
    }
    
    private function calculateDiffs($updateFunction = null)
    {
        if (!$this->diff_calculated)
        {
            if (is_null($updateFunction))
            {
                $updateFunction = function(){};
            }
            $this->old_transactions = $this->owner->account_transactions;
            $this->new_transactions = BookingPrediction::Booking($this->owner->id, BookingPrediction::$BOOKING_TYPE_ORDER, $updateFunction);
            $this->pairTransactions($updateFunction);
            $this->compareTransactions();
            $this->mergeDiffsPerDocuments();
            
            foreach ($this->owner->account_documents as $account_document)
            {
                if (!empty($account_document->saved_diff_hash) 
                        && $account_document->saved_diff_hash === $this->getAccountDocumentCache($account_document))
                {
                    $this->setAccountDocumentCached($account_document);
                }

            }
            
            $this->diff_calculated = true;
        }
    }
    
    /**
     * Razlike u predvidjanju sistema
     * @return array - niz nizova. index prvog niza je string - ID fajla, a podniz su elementi razlike
     */
    public function getBookingPredictionDiffs($updateFunction = null)
    {
        $this->calculateDiffs($updateFunction);
        return $this->diffs_per_document;
    }
    
    /**
     * 
     * @param array $account_documents_cache
     */
    public function setDiffsCache($account_documents_cache)
    {
        foreach ($account_documents_cache as $account_document_cache)
        {
            $account_document_id = $account_document_cache['group_id'];
            $cache_account_document = SIMAMisc::filter_bool_var($account_document_cache['cache']);
            $account_document = AccountDocument::model()->findByPk($account_document_id);
            if (!is_null($account_document))
            {
                if ($cache_account_document)
                {
                    $this->calculateDiffs();//moze da se ponavlja poziv, izracunava se samo jednom, a ovako se vodi racuna da se pozove samo ako mora
                    $account_document->saved_diff_hash = $this->getAccountDocumentCache($account_document);
                }
                else
                {
                    $account_document->saved_diff_hash = '';
                }
                $account_document->save();
            }
        }
    }
    
    private function getAccountDocumentCache(AccountDocument $account_document)
    {
        $diff = null;
        foreach ($this->diffs_per_document as $_diff)
        {
            if (intval($_diff['document_id']) === intval($account_document->id))
            {
                $diff = $_diff;
                break;
            }
        }
        if (is_null($diff))
        {
            return '';
        }
        
        if(isset($diff['items']))
        {
            $diff['items'] = SIMAMisc::hashArray($diff['items']);
        }

        return SIMAMisc::hashArray($diff).SIMAMisc::hashArray($account_document->account_transactions);
    }
    
    /**
     * Dodaje u diff niz da je dokument kesiran
     * @param type $account_document dokument za koji se setuje da je kesiran
     * @param type $diffs niz u koji se setuje
     */
    private function setAccountDocumentCached($account_document)
    {
        foreach ($this->diffs_per_document as $diff_key => $diff_value)
        {
            if (intval($diff_value['document_id']) === intval($account_document->id))
            {
                $this->diffs_per_document[$diff_key] = array_merge($this->diffs_per_document[$diff_key], ['diff_confirmed'=>true]);
            }
        }
    }
    
    public function hasUnconfirmedDiffs()
    {
        $this->calculateDiffs();
        $has_unconfirmed_diffs = false;
        foreach ($this->diffs_per_document as $diff) 
        {
            if (
                    (!isset($diff['diff_confirmed']) || SIMAMisc::filter_bool_var($diff['diff_confirmed']) === false) && 
                    intval($diff['changes']) > 0
                )
            {
                $has_unconfirmed_diffs = true;
                break;
            }
        }
        return $has_unconfirmed_diffs;
    }
}