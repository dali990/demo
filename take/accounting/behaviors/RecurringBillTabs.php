<?php
class RecurringBillTabs extends SIMAActiveRecordTabs
{
    protected function tabsDefinition()
    {
        $recurring_bill_tabs = [
            [
                'title'=>'Komentari',
                'code'=>'comments',
                'params_function' => 'tabComments',
                'order' => 100
            ]
        ];
        
        return $recurring_bill_tabs;
    }
    
    protected function tabComments()
    {
        $owner = $this->owner;
        
        $comment_thread = CommentThread::getCommonForModel($owner);
        
        return array(
            'model' => $owner,
            'comment_thread'=>$comment_thread
        );
    }
}
    