<?php
class AccountingMonthTabs extends SIMAActiveRecordTabs
{
    protected function tabsDefinition()
    {
        $owner = $this->owner;
        $cost_locations_tabs = array(
            [
                'title' => 'KPR rucno',
                'code' => 'kpr_manual',
                'action' => 'accounting/vat/kprManual',
                'get_params' => [
                    'month' => $owner->month, 
                    'year' => $owner->year
                ]
            ],
            [
                'title' => 'KPR - SIMA',
                'code' => 'kpr_sima',
                'action' => 'accounting/Report/report_change',
                'get_params' => [
                    'month' => $owner->month, 
                    'year' => $owner->year, 
                    'name' => 'KPR'
                ]
            ],
            [
                'title' => 'KPR poredjenje',
                'code' => 'kpr_compare',
                'action' => 'accounting/vat/kprCompare',
                'get_params' => [
                    'month' => $owner->month, 
                    'year' => $owner->year
                ]
            ],
            [
                'title' => 'KIR rucno',
                'code' => 'kir_manual',
                'action' => 'accounting/vat/kirManual',
                'get_params' => [
                    'month' => $owner->month, 
                    'year' => $owner->year
                ]
            ],
            [
                'title' => 'KIR - SIMA',
                'code' => 'kir_sima',
                'action' => 'accounting/Report/report_change',
                'get_params' => [
                    'month' => $owner->month, 
                    'year' => $owner->year, 
                    'name' => 'KIR'
                ]
            ],
            [
                'title' => 'KIR poredjenje',
                'code' => 'kir_compare',
                'action' => 'accounting/vat/kirCompare',
                'get_params' => [
                    'month' => $owner->month, 
                    'year' => $owner->year
                ]
            ],
            [
                'title' => 'POPDV',
                'code' => 'popdv',
                'action' => 'accounting/vat/getPOPDVDisplayHTMLold',
                'reload_on_update' => true,
                'get_params' => [
                    'month' => $owner->month, 
                    'year' => $owner->year
                ]
            ],
            [
                'title' => 'PPPDV',
                'code' => 'pppdv',
                'action' => 'accounting/vat/getPPPDVReportHtml',
                'reload_on_update' => true,
                'get_params' => [
                    'month_id' => $owner->id
                ]
            ],
            [
                'title' => Yii::t('AccountingModule.AccountingMonth','ProblemsOverview'),
                'code' => 'problems',
                'params_function' => 'problemsFunction'
            ],
            [
                'title' => Yii::t('AccountingModule.AccountingMonth','UnreleasedAdvances'),
                'code' => 'unreleased_advances',
            ],
            [
                'title' => Yii::t('AccountingModule.AccountingMonth','BillsNotBooked'),
                'code' => 'bills_not_booked',
            ],
            [
                'title' => Yii::t('AccountingModule.AccountingMonth','BillsBadVatBookedCount'),
                'code' => 'bills_bad_vat_booked',
            ],
            [
                'title' => Yii::t('AccountingModule.AccountingMonth','VATKPRNotFound'),
                'code' => 'vat_kpr_not_found',
            ],
            [
                'title' => Yii::t('AccountingModule.AccountingMonth','VATKIRNotFound'),
                'code' => 'vat_kir_not_found',
            ],
        );
        
        if (isset($owner->vat_account_document))
        {
            $cost_locations_tabs[] = [
                'title' => Yii::t('AccountingModule.Common','Bookkeeping'),
                'code' => 'bookkeeping',
                'action' => 'accounting/books/documentTab',
                'reload_on_update' => true,
                'get_params' => [
                    'id' => $owner->vat_account_document_id
                ]
            ];
        }
        
        return $cost_locations_tabs;
    }
    
    
    public function problemsFunction()
    {
        $owner = $this->owner;
        $base_month = $owner->base_month;
        $month = $base_month->month;
        $year = $base_month->year->year;
        
        $first_day = $base_month->firstDay();
        $last_day = $base_month->lastDay();
        
        
        $advance_payment_type = $advance_payment_type = Yii::app()->configManager->get('accounting.advance_payment_type');
        if (empty($advance_payment_type))
        {
            $unreleased_advances = Yii::t('AccountingModule.Configuration','SetAdvancePayment');
        }
        else
        {
            $unreleased_advances = Payment::model()->countByModelFilter([
                'payment_type' => ['ids' => intval($advance_payment_type)],
                'bank_statement' => [
                    'date' => SIMAHtml::UserToDbDate($first_day->day_date).'<>'.SIMAHtml::UserToDbDate($last_day->day_date)
                ],
                'filter_scopes' => ['unreleased'],
            ]);
            if ($unreleased_advances > 0)
            {
                $unreleased_advances = SIMAHtml::showAsImportant($unreleased_advances);
            }
        }

        $bills_not_booked = Bill::model()->VATInMonth($month, $year)->notBooked()->count();
        if ($bills_not_booked > 0)
        {
            $bills_not_booked = SIMAHtml::showAsImportant($bills_not_booked);
        }

        $bills_bad_vat_booked_count = KPR::model()->BadVATBooked()->inMonth($month, $year)->count();
        if ($bills_bad_vat_booked_count > 0)
        {
            $bills_bad_vat_booked_count = SIMAHtml::showAsImportant($bills_bad_vat_booked_count);
        }

        $VATKIRNotFound = AccountTransaction::model()->VATNotInKIR($month, $year)->count();
        if ($VATKIRNotFound > 0)
        {
            $VATKIRNotFound = SIMAHtml::showAsImportant($VATKIRNotFound);
        }
        $VATKPRNotFound = AccountTransaction::model()->VATNotInKPR($month, $year)->count();;
        if ($VATKPRNotFound > 0)
        {
            $VATKPRNotFound = SIMAHtml::showAsImportant($VATKPRNotFound);
        }

        
        return [
            'model' => $owner,
            'unreleased_advances' => $unreleased_advances,
            'bills_not_booked' => $bills_not_booked,
            'bills_bad_vat_booked_count' => $bills_bad_vat_booked_count,
            'VATKIRNotFound' => $VATKIRNotFound,
            'VATKPRNotFound' => $VATKPRNotFound,
        ];
    }
}
    