<?php

/**
 * @package SIMA
 * @subpackage Accounting
 */
class FixedAssetAccountingBehavior extends SIMAActiveRecordBehavior
{
    
    public function relations($event)
    {
        $event->addResult(array(
            'accounting_fixed_asset' => [SIMAActiveRecord::HAS_ONE, 'AccountingFixedAsset', 'id']
        ));
    }
    
    /**
     * za OS brise i finansijski deo
     * postoji provera da ne moze da se obrise ukoliko je OS proknjizeno 
     * odnosno, ukoliko je proknjizen dokument nabavke
     */
    public function beforeDelete($event)
    {
        parent::beforeDelete($event);
        if (isset($this->owner->accounting_fixed_asset))
        {
            $this->owner->accounting_fixed_asset->delete();
        }
    }
    
    public function tabs($event)
    {
        if ( Yii::app()->user->checkAccess('accountingFixedAssetStat'))
        {
            $event->addResult(array(
                array(
                    'title' => 'Amortizacije',
                    'code' => 'depreciations',
                    'module_origin' => 'accounting',
                    'pre_path' => 'finance',
                    'order' => 20
    //                'action' => 'accounting/books/documentTab',
    //                'get_params' => array('id' => $file->id)
                )
            ));
        }
    }
    
    public function getAccountForBooking()
    {
        return $this->owner->fixed_asset_type->getAccountForBooking();
    }
}