<?php
class AdvanceTabs extends SIMAActiveRecordTabs
{
    protected function tabsDefinition()
    {
        $payments_tabs = array(
            array(
                'title'=>'Info',
                'code'=>'info',
                'params_function' => 'InfoFunction',
            )
        );
        
        return $payments_tabs;
    }
    
    public function InfoFunction()
    {
        $owner = $this->owner;
        if ($owner->unreleased_amount < 0.001)
        {
            $advance_bill_proposals_number = 0;
            $bill_proposals_number = 0;
        }
        else
        {
            $advance_bill_model = ($owner->invoice===true)?'AdvanceBillOut':'AdvanceBillIn';
            $advance_bill_criteria = new SIMADbCriteria([
                'Model'=>$advance_bill_model,
                'model_filter'=>[
                    'like_payment_id'=>$owner->id,
                    'scopes'=>[
                        'unreleased'
                    ],
                    'connected'=>false
                ]
            ]);
            $advance_bill_proposals_number = $advance_bill_model::model()->count($advance_bill_criteria);

            $bill_model = ($owner->invoice===true)?'BillOut':'BillIn';
            $bill_criteria = new SIMADbCriteria([
                'Model'=>$bill_model,
                'model_filter'=>[
                    'like_payment_id'=>$owner->id,
                    'scopes'=>[
                        'unreleased'
                    ]
                ]
            ]);
            $bill_proposals_number = $bill_model::model()->count($bill_criteria);
        }
        
        return [
            'model' => $owner,            
            'advance_bill_proposals_number' => $advance_bill_proposals_number,
            'bill_proposals_number' => $bill_proposals_number
        ];
    }
    
}
    