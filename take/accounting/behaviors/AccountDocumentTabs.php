<?php
class AccountDocumentTabs extends SIMAActiveRecordTabs
{
    protected function tabsDefinition()
    {
        $owner = $this->owner;      
        $tabs = [];
        
        //Nalog za knjizenje
        $tabs[] = [
            'title' => 'Stavovi',
            'code' => 'transactions',
            'params_function' => 'transactionParams',
            'reload_on_update' => true
        ];
        $tabs[] = [
            'title' => 'Dokumenti',
            'code' => 'docs',
            'reload_on_update' => true
        ];
        if (isset($owner->canceled_account_order))
        {
            $tabs[] = [
                'title' => 'Stavovi - storno',
                'code' => 'canceled_transactions',
                'params_function' => 'canceledTransactionParams',
                'reload_on_update' => true
            ];
            //OVAJ TAB MOZDA TREBA??
//            $tabs[] = [
//                'title' => 'Dokumenti - storno',
//                'code' => 'canceled_docs',
//                'html' => Yii::app()->controller->widget('SIMAGuiTable', array(
//                    'model' => 'AccountDocument',
//                    'pagination'=>false,
//                    'fixed_filter'=>array(
//                        'canceled_account_order' => array(
//                            'ids'=>$owner->canceled_account_order->id
//                        )
//                     ),
//                ),true)
//            ];
        }
        
        return $tabs;
    }
    
    public function transactionParams()
    {
        $owner = $this->owner;
        $uniq = SIMAHtml::uniqid();
        $model = $owner;        
        $year_id = $owner->account_order->year_id;
        $ao_id = $owner->account_order_id;
        $booked = $owner->isBooked;
        
        if (!$booked)
        {
            $custom_buttons = [
                'Proknjizi ovaj dokument'=>[
                    'func'=>"sima.accounting.BookDocument($model->id,'#$uniq');"
                ],
                'Proknjizi ceo nalog'=>[
                    'func'=>"sima.accounting.BookDocument($model->id,'#$uniq','".BookingPrediction::$BOOKING_TYPE_ORDER."');"
                ],
                Yii::t('AccountingModule.Booking', 'BookingPrediction') =>[
                    'func'=>"sima.accounting.book_document_diffs($ao_id,'#$uniq');"
                ],
                'Skupi stavove'=>[
                    'func'=>"sima.accounting.collapseAccountTransactionOrders($model->id,'#$uniq');"
                ],
                'Dodaj konto'=>[
                    'func'=>"sima.model.form('Account','',{init_data:{Account: {year_id:$year_id}}});"
                ]
            ];
        }
        $custom_buttons['Pdf'] = [
            'func'=>"sima.accountingMisc.exportAccountOrderToPdf(".$model->account_order->id.");"                                
        ];
        return [
            'uniq' => $uniq,
            'booked' => $booked,
            'year_id' => $year_id,
            'custom_buttons' => $custom_buttons,
            'model' => $owner
        ];
    }
    
    public function canceledTransactionParams()
    {
        $uniq = SIMAHtml::uniqid();
        $owner = $this->owner;
        $year_id = $owner->account_order->year_id;
        $booked = $owner->canceled_account_order->isBooked;
        $ao_id = $owner->canceled_account_order_id;
        if (!$booked)
        {
            $custom_buttons = [
//                'Proknjizi ovaj dokument'=>[
//                    'func'=>"sima.accounting.BookDocument($owner->id,'#$uniq');"
//                ],
                'Proknjizi ceo nalog'=>[
                    'func'=>"sima.accounting.BookDocument($ao_id,'#$uniq','".BookingPrediction::$BOOKING_TYPE_ORDER."');"
                ],
                Yii::t('AccountingModule.Booking', 'BookingPrediction') =>[
                    'func'=>"sima.accounting.book_document_diffs($ao_id,'#$uniq');"
                ],
                'Dodaj konto'=>[
                    'func'=>"sima.model.form('Account','',{init_data:{Account: {year_id:$year_id}}});"
                ]
            ];
        }
        $custom_buttons['Pdf'] = [
            'func'=>"sima.accountingMisc.exportAccountOrderToPdf(".$owner->canceled_account_order->id.");"                                
        ];
        
        return [
            'uniq' => $uniq,
            'booked' => $booked,
            'year_id' => $year_id,
            'custom_buttons' => $custom_buttons,
            'model' => $owner
        ];
    }
    
}
    