<?php

/**
 * @package SIMA
 * @subpackage Accounting
 */
class PartnerAccountingBehavior extends SIMAActiveRecordBehavior
{
    
    public function getSupplierAccount($year)
    {
        $owner = $this->owner;
        if ($owner->isDomestic())
        {
            $parent = Account::getFromParam('accounting.codes.partners.suppliers',$year);
        }
        else
        {
            $parent = Account::getFromParam('accounting.codes.partners.suppliers_ino',$year);
        }
        if ($parent==null)
        {
            throw new SIMAWarnException('Nisu dobro postavljeni osnovni konti za partnera');
        }
        else
        {
            return $parent->getLeafAccount($this->owner);
        }
    }
    
    public function getCustomerAccount($year)
    {
        $owner = $this->owner;
        if ($owner->isDomestic())
        {
            $parent = Account::getFromParam('accounting.codes.partners.customers',$year);
        }
        else
        {
            $parent = Account::getFromParam('accounting.codes.partners.customers_ino',$year);
        }
        if ($parent==null)
        {
            throw new SIMAWarnException('Nisu dobro postavljeni osnovni konti za partnera');
        }
        else
        {
            return $parent->getLeafAccount($this->owner);
        }
    }
    
    public function getSupplierAdvanceAccount($year)
    {
        $_l_type = 'materials';
        /**
         * TODO(MilosS):privremeno resenje - treba davati u zavisnosti od tipa racuna, ali je to bas komplikovano
         * to se ni rucno ne radi
         */

        $owner = $this->owner;
        if ($owner->isDomestic())
        {
            $parent = Account::getFromParam('accounting.codes.partners.advance_bill_in_'.$_l_type,$year);
        }
        else
        {
            $parent = Account::getFromParam('accounting.codes.partners.advance_bill_in_'.$_l_type.'_ino',$year);
        }
        if ($parent==null)
        {
            throw new SIMAWarnException('Nisu dobro postavljeni osnovni konti za partnera');
        }
        else
        {
            return $parent->getLeafAccount($this->owner);
        }
    }
    
    public function getCustomerAdvanceAccount($year)
    {
        $parent = Account::getFromParam('accounting.codes.partners.advance_bill_out',$year);
        if ($parent==null)
        {
            throw new SIMAWarnException('Nisu dobro postavljeni osnovni konti za partnera');
        }
        else
        {
            return $parent->getLeafAccount($this->owner);
        }
    }
    
    public function relations($event)
    {
        $event->addResult(array(
            'payments' =>  [SIMAActiveRecord::HAS_MANY, 'Payment', 'partner_id'],
            'payments_in' =>  [SIMAActiveRecord::HAS_MANY, 'PaymentIn', 'partner_id'],
            'payments_out' =>  [SIMAActiveRecord::HAS_MANY, 'PaymentOut', 'partner_id'],
            'recurring_bills' => [SIMAActiveRecord::HAS_MANY, 'RecurringBill', 'partner_id'],
            'recurring_bills_count' => [SIMAActiveRecord::STAT, 'RecurringBill', 'partner_id', 'select' => 'count(*)'],
            'bills_in' => [SIMAActiveRecord::HAS_MANY, 'BillIn', 'partner_id'],
            'bills_in_cnt' => [SIMAActiveRecord::STAT, 'BillIn', 'partner_id', 'select' => 'count(*)'],
            'bills_out' => [SIMAActiveRecord::HAS_MANY, 'BillOut', 'partner_id'],
            'bills_out_cnt' => [SIMAActiveRecord::STAT, 'BillOut', 'partner_id', 'select' => 'count(*)'],
            'advance_bills_in' => [SIMAActiveRecord::HAS_MANY, 'AdvanceBillIn', 'partner_id'],
            'advance_bills_out' => [SIMAActiveRecord::HAS_MANY, 'AdvanceBillOut', 'partner_id'],
            'relief_bills_in' => [SIMAActiveRecord::HAS_MANY, 'ReliefBillIn', 'partner_id'],
            'relief_bills_out' => [SIMAActiveRecord::HAS_MANY, 'ReliefBillOut', 'partner_id'],
            'unrelief_bills_in' => [SIMAActiveRecord::HAS_MANY, 'UnReliefBillIn', 'partner_id'],
            'unrelief_bills_out' => [SIMAActiveRecord::HAS_MANY, 'UnReliefBillOut', 'partner_id'],
            'partner_accounts' => array(SIMAActiveRecord::HAS_MANY, 'Account', 'model_id', 'condition' => "model_name='".Partner::class."'"),
            
            'debt_take_over_as_creditor' => [SIMAActiveRecord::HAS_MANY, 'DebtTakeOver', 'creditor_id'],
            'debt_take_over_as_debtor' => [SIMAActiveRecord::HAS_MANY, 'DebtTakeOver', 'debtor_id'],
            'debt_take_over_as_takes_over' => [SIMAActiveRecord::HAS_MANY, 'DebtTakeOver', 'takes_over_id'],
        ));
    } 
    
    public function tabs($event)
    {
        $owner = $this->owner;
        $owner_is_company = ($owner->company)?true:false;
        
        if (Yii::app()->user->checkAccess('accountingPartnerStat'))
        {
            $event->addResult([
                [
                    'title' => Yii::t('AccountingModule.Partner', 'Accounting'),
                    'code' => 'finance',
                    'module_origin' => 'accounting',
                    'params_function' => 'tabFinance',
                ]
            ]);
            
            $event->addResult([
                [
                    'title' => Yii::t('AccountingModule.Partner', 'UnreleasedPayments'),
                    'code' => 'unreleased_payments',
                    'module_origin' => 'accounting',
//                    'params_function' => 'tabUnreleasedPayments',
                    'pre_path'=>'finance'
                ],
                [
                    'title' => Yii::t('AccountingModule.Partner', 'AllPayments'),
                    'code' => 'all_payments',
                    'module_origin' => 'accounting',
//                    'params_function' => 'tabAllPayments',
                    'pre_path'=>'finance'
                ]
            ]);

            if($owner_is_company)
            {
                $event->addResult([
                    [
                        'title' => 'IOS',
                        'code' => 'ios',
                        'action' => 'accounting/ios/iosTabInCompany',
                        'get_params' => array('company_id' => $owner->id),
                        'module_origin' => 'accounting',
                        'pre_path'=>'finance'
                    ]
                ]);
            }
            
            foreach ($owner->recurring_bills as $_recurring_bill)
            {
                $tab_name = BillGUI::modelLabel(true);
                $event->addResult([
                    [
                        'title' => $tab_name.' - '.$_recurring_bill->DisplayName,
                        'code' => 'recurring_bill_'.$_recurring_bill->id,
                        'action' => 'base/model/renderModelView',
                        'get_params' => [
                            'model_name' => 'RecurringBill',
                            'model_id' => $_recurring_bill->id,
                            'view_name' => 'full_info_solo',
                        ],
//                        'module_origin' => 'accounting',
                        'pre_path'=>'finance'
                    ]
                ]);
            }
        }
    }
    
    protected function tabFinance()
    {
        $owner = $this->owner;
        return array(
            'model' => $owner
        );
    }
    
//    protected function tabUnreleasedPayments()
//    {
//        $owner = $this->owner;
//        return array(
//            'model' => $owner
//        );
//    }
//    protected function tabAllPayments()
//    {
//        $owner = $this->owner;
//        return array(
//            'model' => $owner
//        );
//    }
    
    
    
    public function withIncomeCostForCostLocation($cost_location_id)
    {
        $cost_location = CostLocation::model()->findByPkWithCheck($cost_location_id);
        $owner = $this->owner;
        $alias = $owner->getTableAlias();
        $uniq = SIMAHtml::uniqid();
        $criteria = new SIMADbCriteria([
            'select' => "$alias.*, "
            . "aclic$uniq.income            as income, "
            . "aclic$uniq.income_notpayed   as income_notpayed, "
            . "aclic$uniq.income_advance    as income_advance, "
            . "aclic$uniq.cost              as cost_services, "
            . "aclic$uniq.cost_notpayed     as cost_notpayed, "
            . "aclic$uniq.cost_advance      as cost_advance ",
            'join' => "join accounting.report_services_cost_location_partner aclic$uniq on $alias.id = aclic$uniq.partner_id",
            'condition' => "aclic$uniq.cost_location_id = :cost_location_id$uniq",
            'params' => [":cost_location_id$uniq" => $cost_location->id]
        ]);
        
        $owner->getDbCriteria()->mergeWith($criteria);
        return $owner;
    }
    
    
}