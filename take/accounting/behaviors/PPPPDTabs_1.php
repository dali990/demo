<?php
class PPPPDTabs extends SIMAActiveRecordTabs
{
    protected function tabsDefinition()
    {
        $ppppd_tabs = [
            [
                'title' => Yii::t('AccountingModule.PPPPD', 'PPPPDItems'),
                'code' => 'ppppd_items'
            ]
        ];
        
        return $ppppd_tabs;
    }
}
    