<?php
class CostLocationTabs extends SIMAActiveRecordTabs
{
    protected function tabsDefinition()
    {
        $cost_locations_tabs = [
            [
                'title' => Yii::t('AccountingModule.CostLocation', 'ThemeTabFinanceIncome'),
                'code' => 'cost_location_income'
            ],
            [
                'title' => Yii::t('AccountingModule.CostLocation', 'ThemeTabFinanceCost'),
                'code' => 'cost_location_cost'
            ]
//            [
//                'title' => 'Opste informacije',
//                'code' => 'info',
//                'action' => 'base/model/view',
//                'get_params' => array('model' => get_class($owner), 'model_id'=>$owner->id,'model_view' => 'financeInfo')
////                    'html' => Yii::app()->controller->renderModelView($job,'financeInfo'),
//            ],
//            [
//
//                'title' => 'Fakturisanje',
//                'code' => 'income',
//                'html' => Yii::app()->controller->widget('SIMAGuiTable', [
//                            'model'=> BillOut::model(),
//                            'columns_type' => 'income_in_job',
//                            'add_button' => true,
//                            'fixed_filter'=>array(
//                                'file'=> [
//                                    'belongs' => $owner->tag->query
//                                ]
//
//
////                                    'display_scopes' => array(
////                                        'byName'
////                                    ),
//                            )
//                        ], true)
//            ],
//            [
//                'title' => 'Direktni troskovi po podizvodjacima',
//                'code' => 'outcome',
//                'html' => Yii::app()->controller->widget('SIMAGuiTable', [
//                    'model'=> JobFinance::model(),
//                    'columns_type' => 'outcome',
//                    'fixed_filter'=>array(
//                        'display_scopes' => array(
//                            'withMainJoin',
//                            'withMainSelect',
//                            'byPartners',
//                            'forTheme' => array(
//                                $owner->id
//                            )
//                        ),
//                        
//                    )
//                ], true)
//            ],
//            [
//                'title' => 'Troskovi materijala',
//                'code' => 'warehouse_outcome',
//                'html' => (isset($owner->cost_location)) ? Yii::app()->controller->renderModelView($owner->cost_location, 'info') : ''
//            ]
        ];
        
        return $cost_locations_tabs;
    }
}
    