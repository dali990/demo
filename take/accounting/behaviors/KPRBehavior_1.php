<?php

/**
 * @package SIMA
 * @subpackage Accounting
 */
class KPRBehavior extends SIMAActiveRecordBehavior
{
    public function getReportParams($month, $year, $update_func=null, $for_pdf_or_csv = false)
    {
        $month_model = Month::get($month, $year);
        $rows = KPR::model()->byOrderInYear()->inMonth($month, $year)->findAll();
        
        $total_pdv = 0;
        $total_income = 0;
        
        $sum_row = [
            'KPR_8' => 0,
            'KPR_9' => 0,
            'KPR_9a' => 0,
            'KPR_10' => 0,
            'KPR_11' => 0,
            'KPR_12' => 0,
            'KPR_13' => 0,
            'KPR_14' => 0,
            'KPR_15' => 0,
            'KPR_16' => 0,
            'KPR_17' => 0,
            'KPR_18' => 0,
            'KPR_19' => 0,
        ];
        
        $i = 1;
        $rows_cnt = count($rows);
        $repacked_rows = [];
        foreach ($rows as $row)
        {
            if (!empty($update_func))
            {
                call_user_func($update_func, round(($i++/$rows_cnt)*100, 0, PHP_ROUND_HALF_DOWN));
            }
            
            $repacked_row = [
                'order_in_year' => $row->order_in_year,
                'booking_date' => $row->bill->booking_date,
                'bill_number' => $for_pdf_or_csv ? $row->bill->bill_number : $row->bill->DisplayHTML,
                'income_date' => $row->bill->income_date,
                'partner_display_name' => $for_pdf_or_csv ? $row->bill->partner->DisplayName : $row->bill->partner->DisplayHTML,
                'partner_pib_jmbg' => $row->bill->partner->pib_jmbg,
                'row_model' => $row,
                'KPR_8' => $row->getAttributeDisplay('KPR_8'),
                'KPR_9' => $row->getAttributeDisplay('KPR_9'),
                'KPR_9a' => $row->getAttributeDisplay('KPR_9a'),
                'KPR_10' => $row->getAttributeDisplay('KPR_10'),
                'KPR_11' => $row->getAttributeDisplay('KPR_11'),
                'KPR_12' => $row->getAttributeDisplay('KPR_12'),
                'KPR_13' => $row->getAttributeDisplay('KPR_13'),
                'KPR_14' => $row->getAttributeDisplay('KPR_14'),
                'KPR_15' => $row->getAttributeDisplay('KPR_15'),
                'KPR_16' => $row->getAttributeDisplay('KPR_16'),
                'KPR_17' => $row->getAttributeDisplay('KPR_17'),
                'KPR_18' => $row->getAttributeDisplay('KPR_18'),
                'KPR_19' => $row->getAttributeDisplay('KPR_19')
            ];
            
            array_push($repacked_rows, $repacked_row);
            foreach ($sum_row as $key => $value)
            {
                $sum_row[$key] += $row->$key;
            }
        }

        foreach ($sum_row as $key => $value)
        {
            $sum_row[$key] = SIMAHtml::number_format($value);
        }
        
        $month_disp = $month_model->toString();
        $month_disp .= " $year. godine";

        return [
            'rows'=> $repacked_rows,
            'sum_row' => $sum_row,
            'month' => $month_disp,
            'total_pdv' => $total_pdv,
            'total_income' => $total_income
        ];
    }
}