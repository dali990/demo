<?php

/**
 * @package SIMA
 * @subpackage Accounting
 */
class AutoCostLocationBehavior extends SIMAActiveRecordBehavior
{
    /**
     * za OS brise i finansijski deo
     * postoji provera da ne moze da se obrise ukoliko je OS proknjizeno 
     * odnosno, ukoliko je proknjizen dokument nabavke
     */
    public $prefix = '';
    public $has_parent = false;

    public function beforeSave($event)
    {
        $owner = $this->owner;
        $class_name = get_class($owner);
        if (empty($owner->cost_location_id))
        {
            $_cl = new CostLocation();
            $_cl->prefix = $this->prefix;
            if (CostLocationGUI::generateName())
            {
                $_cl->number = $owner->id;
            }
            else
            {
                $_cl->name = $this->prefix.$owner->id;
            }
            $_cl->work_name = $owner->name;
            $_cl->for_analytics = false;
            $_cl->model_name = $class_name;
            $_cl->model_id = $owner->id;
            
            if ($this->has_parent && isset($owner->parent))
            {
                $_cl->parent_id = $owner->parent->cost_location_id;
            }
            
            $_cl->save();
            
            $owner->cost_location_id = $_cl->id;
            $owner->cost_location = $_cl;
        }
    }
    
    public function afterSave($event)
    {
        $owner = $this->owner;
        $cost_location_changed = false;
        if ($owner->cost_location->prefix === $this->prefix)
        {
            if (CostLocationGUI::generateName())
            {
                $owner->cost_location->number = $owner->id;
            }
            else
            {
                $owner->cost_location->name = $this->prefix.$owner->id;
            }
            
            $owner->cost_location->work_name = $owner->name;
            $cost_location_changed = true;
        }
        
        if ($this->has_parent && $owner->columnChanged('parent_id'))
        {
            if (isset($owner->parent))
            {
                $owner->cost_location->parent_id = $owner->parent->cost_location_id;
            }
            else
            {
                $owner->cost_location->parent_id = null;
            }
        }
        
        if ($cost_location_changed)
        {
            $owner->cost_location->save();
        }
    }
    
    public function afterDelete($event)
    {
        $this->owner->cost_location->delete();
    }

}