<?php

class FileSignatureAccountingBehavior extends SIMAActiveRecordBehavior
{
    public $bill_in_apply_signature_waitings_from_org_scheme = true;

    public function afterSave($event) 
    {
        parent::afterSave($event);
        
        $owner = $this->owner;

        if  (
                isset($owner->file->bill_in) && 
                ($owner->isNewRecord || $owner->columnChanged('signed'))
            )
        {
            $this->checkBillInConfirmation1();
        }

        if  (
                isset($owner->file->bill_in) && 
                $owner->isNewRecord && 
                $owner->bill_in_apply_signature_waitings_from_org_scheme
            )
        {
            $this->checkBillInSignatureWaitingForSubordinates();
        }
    }

    public function afterDelete($event) 
    {
        parent::afterDelete($event);
        
        $owner = $this->owner;
        
        if (isset($owner->file->bill_in))
        {
            $this->checkBillInConfirmation1();
        }
    }
    
    private function checkBillInConfirmation1()
    {
        $owner = $this->owner;
        
        $file = File::model()->resetScope()->findByPk($owner->file_id);

        //ako je postavljena automatska potvrda 1 i ako je ulazni racun i ako su svi potpisali fajl onda se automatski stavlja potvrda 1 u racunu
        if (
                !empty($file->bill_in) && 
                Yii::app()->configManager->get('accounting.bill_in_automatic_confirmation1', false)
            )
        {
            $file_signatures_cnt = $file->signatures_cnt;
            if ($file_signatures_cnt > 0 && $file_signatures_cnt === $file->signatures_signed_cnt)
            {
                $file->bill_in->confirm1 = true;
            }
            else
            {
                $file->bill_in->confirm1 = false;
            }
            $file->bill_in->save();
        }
    }
    
    public function checkBillInSignatureWaitingForSubordinates()
    {
        $owner = $this->owner;

        $file = File::model()->resetScope()->findByPkWithCheck($owner->file_id);

        if (
                !empty($file->bill_in) && 
                Yii::app()->configManager->get('accounting.bill_in_file_signature_waiting_for_subordinates', false)
           )
        {
            foreach ($file->signatures as $signature) 
            {
                if (is_null($signature->signed) && intval($signature->id) !== intval($owner->id))
                {
                    if (Person::isBossOf($signature->user->person, $owner->user->person))
                    {
                       $file_signature_waiting = FileSignatureWaiting::model()->countByAttributes([
                            'file_signature_id' => $owner->id,
                            'user_id' => $signature->user_id
                        ]);
                        if ($file_signature_waiting === 0)
                        {
                            $file_signature_waiting = new FileSignatureWaiting();
                            $file_signature_waiting->file_signature_id = $owner->id;
                            $file_signature_waiting->user_id = $signature->user_id;
                            
                            try
                            {
                                $is_validated = $file_signature_waiting->validate();
                            } 
                            catch (SIMAWarnFileSignatureWaitingCircle $ex) {
                                $is_validated = false;
                            }

                            if ($is_validated)
                            {
                                $file_signature_waiting->save(false);
                            }
                        }
                    }
                    else if (Person::isBossOf($owner->user->person, $signature->user->person))
                    {
                         $file_signature_waiting = FileSignatureWaiting::model()->countByAttributes([
                             'file_signature_id' => $signature->id,
                             'user_id' => $owner->user_id
                         ]);
                         if ($file_signature_waiting === 0)
                         {
                             $file_signature_waiting = new FileSignatureWaiting();
                             $file_signature_waiting->file_signature_id = $signature->id;
                             $file_signature_waiting->user_id = $owner->user_id;

                             try
                             {
                                 $is_validated = $file_signature_waiting->validate();
                             } 
                             catch (SIMAWarnFileSignatureWaitingCircle $ex) {
                                 $is_validated = false;
                             }

                             if ($is_validated)
                             {
                                 $file_signature_waiting->save(false);
                             }
                         }
                    }
               }
            }
        }
    }
}