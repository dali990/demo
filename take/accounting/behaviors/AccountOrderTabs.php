<?php
class AccountOrderTabs extends SIMAActiveRecordTabs
{
    protected function tabsDefinition()
    {
        $account_orders_tabs = array(
            array(
                'title'=>'Stavke',
                'code'=>'transactions',
                'params_function' => 'TransactionFunctions',
            ),
            array(
                'title'=>'Proknjiženi dokumenti',
                'code'=>'documents'
            ),
        );
        
        return $account_orders_tabs;
    }
    
    //Dodaj AccountDocument za nalog
    public function TransactionFunctions()
    {
        $owner = $this->owner;
        $table_id = SIMAHtml::uniqid();
        $ad = AccountDocument::model()->findByPk($owner->id);
        if (is_null($ad))
        {
            $ad = new AccountDocument();
            $ad->id = $owner->id;
            $ad->account_order_id = $owner->id;
            $ad->save();
        }
        
        if ($owner->booked)
        {
            $columns_type = 'account_order_disabled';
            $add_button = false;
            $custom_buttons = [];
        }
        else
        {
            $custom_buttons = [
                'Proknjizi ovaj dokument'=>[
                    'func'=>"sima.accounting.BookDocument($owner->id,'#$table_id');"
                ],
                'Proknjizi ceo nalog'=>[
                    'func'=>"sima.accounting.BookDocument($owner->id,'#$table_id','".BookingPrediction::$BOOKING_TYPE_ORDER."');"
                ],
                'Skupi stavove'=>[
                    'func'=>"sima.accounting.collapseAccountTransactionOrders($owner->id,'#$table_id');"
                ],
                'Dodaj konto'=>[
                    'func'=>"sima.model.form('Account','',{init_data:{Account: {year_id:$owner->year_id}}});"
                ]
            ];
            $columns_type = 'account_order';
            $add_button = [
                'init_data' => [
                    'AccountTransaction' => [
                        'account_document_id' => [
                            'hidden',
                            'init'=>$owner->id
                        ],
                        'account_id' => array(
                            'model_filter' => array(
                                'year' => [
                                    'ids' => $owner->year_id
                                ]
                            )
                        )
                    ]
                ],
                'inline'=>true
            ];
        }
        
        $custom_buttons['Pdf'] = [
            'func'=>"sima.accountingMisc.exportAccountOrderToPdf(".$owner->id.");"                                
        ];
        
        return [
            'model' => $owner,
            'add_button' => $add_button,
            'columns_type' => $columns_type,
            'custom_buttons' => $custom_buttons,
            'table_id' => $table_id,
        ];
    }
    
}
    