<?php

/**
 * @package SIMA
 * @subpackage Accounting
 */
class FileAccountingBehavior extends SIMAActiveRecordBehavior
{
    public function options($event)
    {
        $owner = $this->owner;
        if (isset($owner->account_order))
        {
            $event->addResult([
                ['remove','form'],
                ['add',['form','model' => 'AccountOrder']],
                ['remove','recycle'],//obrisi default delete
                ['add',['delete','model' => 'AccountOrder']],
            ]);
        }
    }
    public function relations($event)
    {
        $event->addResult(array(
            'any_bill'=>array(SIMAActiveRecord::HAS_ONE, 'AnyBill', 'id'),
            'bill'=>array(SIMAActiveRecord::HAS_ONE, 'Bill', 'id'),
            'bill_canceled'=>array(SIMAActiveRecord::HAS_ONE, 'BillCanceled', 'id'),
            'bill_in'=>array(SIMAActiveRecord::HAS_ONE, 'BillIn', 'id'),
            'bill_out'=>array(SIMAActiveRecord::HAS_ONE, 'BillOut', 'id'),            
            'advance_bill'=>array(SIMAActiveRecord::HAS_ONE, 'AdvanceBill', 'id'),
            'advance_bill_in'=>array(SIMAActiveRecord::HAS_ONE, 'AdvanceBillIn', 'id'),
            'advance_bill_out'=>array(SIMAActiveRecord::HAS_ONE, 'AdvanceBillOut', 'id'),
            'relief_bill'=>array(SIMAActiveRecord::HAS_ONE, 'ReliefBill', 'id'),
            'relief_bill_in'=>array(SIMAActiveRecord::HAS_ONE, 'ReliefBillIn', 'id'),
            'relief_bill_out'=>array(SIMAActiveRecord::HAS_ONE, 'ReliefBillOut', 'id'),
            'un_relief_bill'=>array(SIMAActiveRecord::HAS_ONE, 'UnReliefBill', 'id'),
            'un_relief_bill_in'=>array(SIMAActiveRecord::HAS_ONE, 'UnReliefBillIn', 'id'),
            'un_relief_bill_out'=>array(SIMAActiveRecord::HAS_ONE, 'UnReliefBillOut', 'id'),
            'pre_bill'=>array(SIMAActiveRecord::HAS_ONE, 'PreBill', 'id'),
            'pre_bill_in'=>array(SIMAActiveRecord::HAS_ONE, 'PreBillIn', 'id'),
            'pre_bill_out'=>array(SIMAActiveRecord::HAS_ONE, 'PreBillOut', 'id'),
            
            'bill_of_exchange'=>array(SIMAActiveRecord::HAS_ONE, 'BillOfExchange', 'id'),
            'authorization_file_for'=>array(SIMAActiveRecord::HAS_MANY, 'BillOfExchange', 'authorization_file_id'),
            
            'bank_statement' => array(SIMAActiveRecord::HAS_ONE, 'BankStatement', 'id'),
            'fixed_assets_depreciation' => array(SIMAActiveRecord::HAS_ONE, 'FixedAssetsDepreciation', 'id'),
            'acquired_fixed_assets' => array(SIMAActiveRecord::HAS_MANY, 'FixedAsset', 'acquisition_document_id'),
            
            'cash_desk_log'=>array(SIMAActiveRecord::HAS_ONE, 'CashDeskLog', 'id'),
            'cash_desk_order'=>array(SIMAActiveRecord::HAS_ONE, 'CashDeskOrder', 'id'),
            
            'account_order' => array(SIMAActiveRecord::HAS_ONE, 'AccountOrder', 'id'),
            'account_document' => array(SIMAActiveRecord::HAS_ONE, 'AccountDocument', 'id'),
            'has_account_document' => [SIMAActiveRecord::STAT, 'AccountDocument', 'id', 'select' => 'count(*)>0'],
            'ios' => [SIMAActiveRecord::HAS_ONE, 'Ios', 'id'],
            
            'debt_take_over' => [SIMAActiveRecord::HAS_ONE, 'DebtTakeOver', 'id'],
        ));
    }
    
    public function tabs($event)
    {
        $file = $event->sender;
        
        if (!$file->in_use)
        {
            return;
        }
        
        $cash_desk_tab_access = Yii::app()->user->checkAccess('FileCashDeskTab');
//        $bank_statement_tab_access = Yii::app()->user->checkAccess('FileBankStatmentTab');
//        $bill_tab_access = Yii::app()->user->checkAccess('FileBillTab');
        
        if (Yii::app()->user->checkAccess('fileBookkeepingTab'))
        {
            if (isset($file->account_document))
            {
                $pre_path = '';
                $order = 50;
                
                if (!isset($file->account_order))
                {
                    $event->addResult([
                        [
                            'title' => 'POPDV',
                            'code' => 'popdv',
                            'module_origin' => 'accounting',
                            'pre_path' => $pre_path.'bookkeeping'
                        ]
                    ]);

                    if (isset($file->account_document->canceled_account_order))
                    {
                        $event->addResult(array(
                            [
                                'title' => 'POPDV - storno',
                                'code' => 'popdv_canceled',
                                'module_origin' => 'accounting',
                                'pre_path' => 'bookkeeping',
                                'reload_on_update' => true
                            ]
                        ));
                    }
                }
                
            }
            else {
                $pre_path = 'others';
                $order = null;
            }
            $event->addResult(array(
                [
                    'title' => 'Knjigovodstvo',
                    'code' => 'bookkeeping',
                    'action' => 'accounting/books/documentTab',
                    'get_params' => array('id' => $file->id),
                    'order' => $order,
                    'pre_path' => $pre_path
//                        'reload_on_update' => true
                ]
            ));
            
            
        }
         
        if (isset($file->any_bill))
        {
            $event->addResult([
                [
                    'title' => 'Stavke u računu',
                    'code' => 'bill_items',
                    'module_origin' => 'accounting',
//                            'action' => 'accounting/billItems',
//                            'get_params' => array('bill_id' => $file->id),
                    'order' => 30,
                    'visible' => true
                ]
            ]);
        }
        
        if (isset($file->bill))
        {
            $event->addResult([
                [
                    'title' => 'Likvidacija',
                    'code' => 'bill_releasing_old',
                    'action' => 'accounting/billReleasing',
                    'get_params' => array('bill_id' => $file->id),
                    'order' => 39,
                ]
            ]);

            if (!Yii::app()->configManager->get('base.develop.hide_old_tab_bill_items'))
            {
                $event->addResult([
                    [
                        'title' => 'Stavke u računu - staro',
                        'code' => 'bill_items_old',
                        'action' => 'accounting/billItems',
                        'get_params' => array('bill_id' => $file->id),
                        'order' => 31,
                    ],
                ]);
            }
            if (
                    (
                        //za svaki od ovih ide posebna komponenta
                        //kada se bude koristilo vue layout, treba posebna komponenta da se navede
                        $file->bill->isBill 
                        ||
                        $file->bill->isReliefBill
                    )
                    && Yii::app()->configManager->get('base.develop.new_tab_unreleased'))
            {
                $event->addResult([
                    //likvidacija racuna u Vue
                    [
                        'title' => 'Likvidacija računa - novo',
                        'code' => 'bill_releasing',
                        'module_origin' => 'accounting',
                        'order' => 40,
                    ]
                ]);
            }
        }
 
        if ($cash_desk_tab_access)
        {
            if (isset($file->cash_desk_order)) 
            {
                $event->addResult(array(
                    array(
                        'title'=>'Stavke naloga',
                        'code'=>'cash_desk_order_items',
                        'order' => 50,
                        'action'=>'guitable',
                        'get_params'=>array(
                            'settings' => array(
                                'model' => 'CashDeskItem',
                                'fixed_filter' => array(
                                    'cash_desk_order' => array(
                                        'id' => $file->id,
                                    )
                                ),
                                'add_button' => array(
                                    'init_data'=>array(
                                        'CashDeskItem' => array(
                                            'cash_desk_order_id' => $file->id
                                        )
                                    )
                                )
                            )
                        )
                    )
                ));
            }

            if (isset($file->cash_desk_log)) 
            {
                $event->addResult(array(
                    array(
                        'title'=>'Stavke naloga',
                        'code'=>'cash_desk_log_items',
                        'action'=>'guitable',
                        'order' => 50,
                        'get_params'=>array(
                            'settings' => array(
                                'model' => 'CashDeskItem',
                                'columns_type' => 'cash_desk_log',
                                'fixed_filter' => [
                                    'cash_desk_order' => [
                                        'cash_desk_log' => [
                                            'ids' => $file->id,
                                        ]
                                    ]
                                ],
                                'add_button' => false
                            )
                        )
                    )
                ));
            }
        }
        
        if (isset($file->bank_statement))
        {
            $bs = $file->bank_statement;
            $event->addResult(array(
                array(
                    'title'=>'Stavke izvoda',
                    'code'=>'bank_statement_items',
                    'params_function' => 'BankStatementItemsParams',
                    'module_origin' => 'accounting',
                    'order' => 50
                )
            ));
        }
        
        if (isset($file->fixed_assets_depreciation))
        {
            $bs = $file->fixed_assets_depreciation;
            $event->addResult(array(
                array(
                    'title'=>'Stavke amortizacije',
                    'code'=>'fixed_assets_depreciation_items',
                    'params_function' => 'FixedAssetDepreciationsParams',
                    'module_origin' => 'accounting',
                    'order' => 50,
                )
            ));
        }
        
    }
    
    public function BankStatementItemsParams()
    {
        $bs = $this->owner->bank_statement;
        $uniq = SIMAHtml::uniqid();
        
        $gtConf = array(
            'id' => 'bank_statement_items'.$uniq,
            'model' => Payment::model(),
            'columns_type'=>'inBankStatement',
            'fixed_filter'=>array(
                'bank_statement' => ['ids' => $bs->id],
                'display_scopes' => ['inBankStatement']
            ),
            'add_button' => array(
                'init_data'=>array(
                    'Payment' => array(
                        'bank_statement' => ['ids' => $bs->id],
                        'payment_date' => $bs->date
                    ),
                )
//                'inline'=>true
            ),
            'custom_buttons' => [
                Yii::t('AccountingModule.BankStatement', 'AddFlyingPayments') => [
                    'func' => "sima.accounting.addFlyingPaymentsToBankStatement({$bs->id},'bank_statement_items$uniq');"
                ],
            ]
//            'setRowDblClick' => "function(row){sima.accounting.warehouse.warehouseTransferToggleBill(row,null,'$uniq')}",
        );
        
        return array(
            'bs' => $bs,
            'gtConf'=>$gtConf,
            'uniq' => $uniq
        );
    }
    
    
    public function FixedAssetDepreciationsParams()
    {
        $uniq = SIMAHtml::uniqid();
        $_FA_dep = $this->owner->fixed_assets_depreciation;
        $gtConf = array(
            'id' => 'FAD'.$uniq,
            'model' => FixedAssetsDepreciationItem::model(),
            'fixed_filter'=>array(
                'fixed_assets_depreciation' => ['ids' => $_FA_dep->id],
            ),
            'custom_buttons' => [
                Yii::t('AccountingModule.Common','Calculate') => [
                    'func' => 'sima.accounting.calculateFADepreciations('.$_FA_dep->id.',\'FAD'.$uniq.'\');'
                ]
            ],
//            'add_button' => array(
//                'init_data'=>array(
//                    'Payment' => array(
//                        'bank_statement' => ['ids' => $bs->id],
//                        'payment_date' => $bs->date
//                    ),
//                )
////                'inline'=>true
//            ),
//            'setRowDblClick' => "function(row){sima.accounting.warehouse.warehouseTransferToggleBill(row,null,'$uniq')}",
        );
        
        return array(
            'gtConf'=>$gtConf,
            'uniq' => $uniq
        );
    }
    
    public function FixedAssetsParams()
    {
        $gtID = SIMAHtml::uniqid();
        $_FA_dep = $this->owner;
        $gtConf = array(
            'id' => 'FA'.$gtID,
            'model' => AccountingFixedAsset::model(),
            'columns_type' => 'inBill',
            'fixed_filter'=>array(
                'fixed_asset' => [
                    'acquisition_document' => ['ids' => $_FA_dep->id],
                ]
            ),
            'custom_buttons' => [
                //TODO
//                Yii::t('AccountingModule.FixedAssets','AddBillItemsAsFA') => [
//                    'func' => 'sima.accounting.addBillItemsAsFA('.$_FA_dep->id.','.$gtID.');'
//                ]
            ],
            'add_button' => array(
                'init_data'=>array(
                    'FixedAsset' => array(
                        'acquisition_document_id' => [
                            'init' => $_FA_dep->id,
                            'disabled' => true
                        ]
                    ),
                )
//                'inline'=>true
            ),
//            'setRowDblClick' => "function(row){sima.accounting.warehouse.warehouseTransferToggleBill(row,null,'$uniq')}",
        );
        
        return array(
            'gtConf'=>$gtConf
        );
    }
    
    public function fullInfo($event)
    {
        $owner = $this->owner;
        
        if (isset($owner->any_bill))
        {
            $bill = $owner->any_bill;
            $model = null;
            if (!$bill->canceled)
            {
                switch ($bill->bill_type) 
                {
                    case Bill::$BILL:  $model = ($owner->bill->invoice)?$owner->bill_out:$owner->bill_in;break;
//                    case Bill::$ADVANCE_BILL:   $model = $owner->advance_bill;break;
                    case Bill::$PRE_BILL:   $model = ($owner->bill->invoice)?$owner->bill:$owner->pre_bill_in;break;
//                    case Bill::$RELIEF_BILL:    $model = $owner->relief_bill;break;
//                    case Bill::$UNRELIEF_BILL:  $model = $owner->unrelief_bill;break;
                    default: $model = $owner->bill; break;
                }
            }
//            else
//            {
//                
//            }
            if (!is_null($model))
            {
                $event->addResult([
                    [
                        'model'=>$model,
                        'priority'=>20
                    ]
                ]);
            }
        }
    }
    
    public function views($event)
    {
        if($event->params['view'] === 'options')
        {
            $file = $this->owner;
            if (isset($file->bill))
            {
                $view_html = Yii::app()->controller->renderModelView($file->bill, 'options_in_file');
                $event->addResult([
                    [
                        'view' => 'options',
                        'order' => 10,
                        'html' => $view_html
                    ]
                ]);
            }
        }
    }
}