<?php
class PaymentTabs extends SIMAActiveRecordTabs
{
    protected function tabsDefinition()
    {
        $payments_tabs = array(
            array(
                'title'=>'Info',
                'code'=>'info',
                'params_function' => 'InfoFunction',
            ),
            [
                'title' => 'Likvidacija',
                'code' => 'releasing',
            ]
        );
        
        return $payments_tabs;
    }
    
    public function InfoFunction()
    {
        $owner = $this->owner;
        if ($owner->unreleased_amount < 0.001)
        {
            $bill_proposals_number = 0;
        }
        else
        {
            $bill_model = ($owner->invoice===true)?'BillOut':'BillIn';
            $bill_criteria = new SIMADbCriteria([
                'Model'=>$bill_model,
                'model_filter'=>[
                    'like_payment_id'=>$owner->id,
                    'scopes'=>[
                        'unreleased'
                    ]
                ]
            ]);
            $bill_proposals_number = $bill_model::model()->count($bill_criteria);
        }
        
        return array(
            'model' => $owner,
            'bill_proposals_number' => $bill_proposals_number
        );
    }
    
}
    