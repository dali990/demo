<?php

/**
 * @package SIMA
 * @subpackage Accounting
 */

class CompanyAccountingBehavior extends SIMAActiveRecordBehavior
{
    
    public function relations($event)
    {
        $event->addResult(array(
            'bank'=>array(SIMAActiveRecord::HAS_ONE, 'Bank','id')
        ));
    } 
    
    public function listCompaniesWithtransactionsForIos($year_id, $account_codes) //,$partner_id
    {
        $owner = $this->owner;
        $alias = $owner->getTableAlias();
        $n=0;
        foreach($account_codes as $account_code)
        {
            if($n===0)
            {
                $query_part = "and (code LIKE '$account_code%' ";
                $n++;
            } else {
                $query_part .= "or code LIKE '$account_code%' ";
            }
        }
        $query_part .= ")";
        $criteria = [
            'condition' => "$alias.id in (select model_id from accounting.accounts where model_name='Partner' $query_part and id in (
                                    select account_id from accounting.account_transactions where account_id is not null and year_id=$year_id 
                                        
                                )) ", //and account_document_id in (select id from private.bills where partner_id=$partner_id and year_id=$year_id)
        ];
        $owner->getDbCriteria()->mergeWith($criteria);
        return $owner;
    }
}

