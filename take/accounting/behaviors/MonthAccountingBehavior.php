<?php

/**
 * @package SIMA
 * @subpackage Accounting
 */
class MonthAccountingBehavior extends SIMAActiveRecordBehavior
{
    public function relations($event)
    {
        $event->addResult(array(
            'accounting'=>array(SIMAActiveRecord::HAS_ONE, 'AccountingMonth','id')
        ));
    } 
    
    public function withBeingAccountingMonth()
    {
        $owner = $this->owner;
        $uniq = SIMAHtml::uniqid();
        $alias = $owner->getTableAlias();
        $acc_month_table = AccountingMonth::model()->tableName();
        $owner->getDbCriteria()->mergeWith([
            'condition' => "EXISTS (SELECT 1 FROM $acc_month_table am$uniq WHERE $alias.id=am$uniq.id LIMIT 1)",
        ]);
        return $owner;
    }
}