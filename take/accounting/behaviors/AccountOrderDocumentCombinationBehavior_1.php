<?php

/**
 * @package SIMA
 * @subpackage Accounting
 */
class AccountOrderDocumentCombinationBehavior extends SIMAActiveRecordBehavior
{
    /**
     * Proveri da li je kombinacija dokumenata na jednom nalogu validna
     * @param array $with_documents - dodatna dokuemnta (iako nisu u bazi)
     * @param array $without_documents - izbaciti dokumenta (iako su u bazi)
     * @return boolean
     */
    public function hasRegularAccountDocumentsCombination($with_documents = [], $without_documents = [])
    {
        $owner = $this->owner;
        $is_regular = true;
        
//        if ($is_regular) {$is_regular = $owner->warehouseTransfersInCorrectTimeline($with_documents = [], $without_documents = []);}
        
        
        return $is_regular;
    }
    
    /**
     * 
     * @param array $with_documents - dodatna dokumenta (iako nisu u bazi vezanim ali moraju da imaju id)
     * @param array $without_documents - izbaciti dokumenta (iako su u bazi)
     * @return boolean
     */
    protected function warehouseTransfersInCorrectTimeline($with_documents = [], $without_documents = [])
    {
        $owner = $this->owner;
        $is_regular = true;
        
        $_account_documents = $with_documents;
        foreach ($owner->account_documents as $_with_ad) 
        {
            $include = true;
            foreach ($without_documents as $_without_ad) 
            {
                if (empty($_without_ad->id))
                {
                    throw new SIMAException('ne moze da se iskljuci dokument koji nema ID- '.$_without_ad->DisplayName);
                }
                if ($_with_ad->id == $_without_ad->id)
                {
                    $include = false;
                }
            }
            if ($include)
            {
                $_account_documents[] = $_with_ad;
            }
        }
        
        $_warehouses_found = [];
        foreach ($_account_documents as $_curr_ad) 
        {
            if (empty($_curr_ad->id))
            {
                throw new SIMAException('ne moze da se ukljuci dokument koji nema ID - '.$_curr_ad->DisplayName);
            }
            
            if (isset($_curr_ad->file->warehouse_transfer)) //posmatraju se samo magacinski dokumenti
            {
                $_wt = $_curr_ad->file->warehouse_transfer;
                $_w_from = $_wt->warehouse_from;
                $_w_to = $_wt->warehouse_to;
                if ($_w_from->isWarehouseStorage)
                {
                    $_warehouse_founded = false;
                    foreach ($_warehouses_found as $_w_key => $_w_local) 
                    {
                        if ($_w_local['model_id'] == $_w_from->id)
                        {
                            $_warehouse_founded = true;
                            if ($_warehouses_found[$_w_key]['direction']=='FROM')
                            {
                                $_warehouses_found[$_w_key]['documents'][] = $_wt;
                            }
                            else
                            {
                                throw new SIMAWarnException('dokument se ne uklapa u nalog -> '.$_wt->DisplayName);
                            }
                        }
                    }
                    
                    if (!$_warehouse_founded)
                    {
                        $_warehouses_found[] = [
                            'model_id' => $_w_from->id,
//                            'model' => $_w_from,
                            'direction' => 'FROM',
                            'documents' => [$_wt]
                        ];
                    }
                    
                }
                
                if ($_w_to->isWarehouseStorage)
                {
                    $_warehouse_founded = false;
                    foreach ($_warehouses_found as $_w_key => $_w_local) 
                    {
                        if ($_w_local['model_id'] == $_w_to->id)
                        {
                            $_warehouse_founded = true;
                            if ($_warehouses_found[$_w_key]['direction']=='TO')
                            {
                                $_warehouses_found[$_w_key]['documents'][] = $_wt;
                            }
                            else
                            {
                                throw new SIMAWarnException('dokument se ne uklapa u nalog -> '.$_wt->DisplayName);
                            }
                        }
                    }
                    
                    if (!$_warehouse_founded)
                    {
                        $_warehouses_found[] = [
                            'model_id' => $_w_to->id,
//                            'model' => $_w_to,
                            'direction' => 'TO',
                            'documents' => [$_wt]
                        ];
                    }
                    
                }
            }
            
        }
        
        /**
         *  * svaki magacin se posmatra odvojeno
         *  * proverava se da li se izmedju magacinskih dokumenata nalazi dokument suprotnog smeta, 
         *          ako da, onda njegov nalog za knjizenje mora da bude pre/posle ovog naloga u zavisnosti od smera
         *          prvo mora da bude ulaz, pa onda izlaz
         *  * proverava se da li se izmedju magacinskih dokumenata nalazi neki drugi dokument istog smera
         */
        foreach ($_warehouses_found as $_w_founded) 
        {
            $max_time = 0;
            $min_time = strtotime('+10 years');
            $document_ids = [];
            foreach ($_w_founded['documents'] as $doc) 
            {
                $_l_time = strtotime($doc->transfer_time);
                
                
                if ($min_time > $_l_time)
                {
                    $min_time = $_l_time;
                }
                if ($max_time < $_l_time)
                {
                    $max_time = $_l_time;
                }
                $document_ids[] = $doc->id;
                
            }
            $direction_attribute = ($_w_founded['direction']=='FROM')?'warehouse_from':'warehouse_to';
                
            $max_time_string = date('d.m.Y. H:i:s', $max_time);
            $max_date_string = date('d.m.Y. 23:59:59', $max_time);//pocetak sledeceg dana
//            $max_date_string = date('d.m.Y.',strtotime('+1 day', date('d.m.Y.', $max_time)));//pocetak sledeceg dana
            $min_time_string = date('d.m.Y. H:i:s', $min_time);
            $min_date_string = date('d.m.Y.', $min_time);
            
            $crit = new SIMADbCriteria([
                'Model' => 'WarehouseTransfer',
                'model_filter' => [
                    $direction_attribute => ['ids' => $_w_founded['model_id']],
                    'transfer_time' => ['<=',  $max_time_string],
                    'display_scopes' => [
                        'byOfficialASC'
                    ]
                ]
            ]);
            $crit2 = new SIMADbCriteria([
                'Model' => 'WarehouseTransfer',
                'model_filter' => [
                    'transfer_time' => ['>=',  $min_time_string]
                ]
            ]);
            $crit->mergeWith($crit2);

            $in_time_documents = WarehouseTransfer::model()->findAll($crit);
            
            
            $crit_per_ids = new SIMADbCriteria([
                'Model' => 'WarehouseTransfer',
                'model_filter' => [
                    'ids' => $document_ids,
                    'display_scopes' => [
                        'byOfficialASC'
                    ]
                ]
            ]);
            $per_ids_documents = WarehouseTransfer::model()->findAll($crit_per_ids);
            
            if (empty($in_time_documents) || empty($per_ids_documents))
            {
                throw new SIMAException('prazni nizovi!!!');
            }
            
            $per_id_document_key = 0;
            $in_time_document_key = 0;
            
            while (isset($in_time_documents[$in_time_document_key]) && isset($per_ids_documents[$per_id_document_key]))
            {
                $in_time_doc = $in_time_documents[$in_time_document_key];
                $per_ids_doc = $per_ids_documents[$per_id_document_key];
                if ($in_time_doc->id == $per_ids_doc->id)
                {
                    
                    $in_time_document_key++;
                    $per_id_document_key++;
                }
                else
                {
                    //postoji opcija da su u istom vremenu dokumenti, ali da nije u opsegu
                    if ($in_time_doc->transfer_time == $per_ids_doc->transfer_time)
                    {
                        if ($per_id_document_key == 0)//POCETAK
                        {
                            if ($in_time_doc->id < $per_ids_doc->id)
                            {
                                $in_time_document_key++;
                                continue;
                            }
                        }
                        else 
                        {
                            if ($in_time_doc->id > $per_ids_doc->id)
                            {
                                $in_time_document_key++;
                                continue;
                            }
                        }
                    }
                    if (isset($in_time_doc->file->account_document->account_order))
                    {
//                        throw new SIMAWarnException('dokument '.$in_time_doc->DisplayName
//                                .' se nalazi izmedju dokumenata koji su na ovom nalogu, a on se nalazi na nalogu '
//                                .$in_time_doc->file->account_document->account_order->DisplayName);
                        $in_time_document_key++;
                        continue;
                    }
                    else
                    {
//                        throw new SIMAWarnException('dokument '.$in_time_doc->DisplayName
//                                .' se nalazi izmedju dokumenata koji su na ovom nalogu, a nije ni na jednom nalogu ');
                        $in_time_document_key++;
                        continue;
                    }
//                    $in_time_ao = 
                    
                }
            }
            $direction_attribute_reverse = ($_w_founded['direction']=='FROM')?'warehouse_to':'warehouse_from';
            $crit_reverse = new SIMADbCriteria([
                'Model' => 'WarehouseTransfer',
                'model_filter' => [
                    $direction_attribute_reverse => ['ids' => $_w_founded['model_id']],
                    'transfer_time' => ['<=',  $max_date_string],
                    'display_scopes' => [
                        'byOfficialASC'
                    ]
                ]
            ]);
            $crit2_reverse = new SIMADbCriteria([
                'Model' => 'WarehouseTransfer',
                'model_filter' => [
                    'transfer_time' => ['>=',  $min_date_string]
                ]
            ]);
            $crit_reverse->mergeWith($crit2_reverse);

//            $reverse_documents_cnt = WarehouseTransfer::model()->count($crit_reverse);
//            if ($reverse_documents_cnt > 0)
//            {
//                if ($_w_founded['direction']=='FROM')
//                {
//                    throw new SIMAWarnException("ne moze biti ulaza u magacin ako se zajedno knjize izlazi ($min_time_string,$max_time_string) ");
//                }
//
//                if ($_w_founded['direction']=='TO')
//                {
//                    throw new SIMAWarnException("ne moze biti izlaza iz magacina ako se zajedno knjize ulazi ($min_time_string,$max_time_string) ");
//                }
//            }
            $reverse_documents = WarehouseTransfer::model()->findAll($crit_reverse);
            foreach ($reverse_documents as $_r_document) 
            {
                if (isset($_r_document->file->account_document->account_order))
                {
                    $_orgi_time = strtotime($owner->date);//vreme ovog dokumenta
                    $_reverse_time = strtotime($_r_document->file->account_document->account_order->date);
                    
                    if ($_w_founded['direction']=='FROM' && $_reverse_time > $_orgi_time)
                    {
                        throw new SIMAWarnException('ovaj nalog mora da bude proknjizen posle dokumenta '.$_r_document->DisplayName);
                    }
                    
                    if ($_w_founded['direction']=='TO' && $_reverse_time < $_orgi_time)
                    {
                        throw new SIMAWarnException('ovaj nalog mora da bude proknjizen pre dokumenta '.$_r_document->DisplayName);
                    }
                }
            }
            
        }
        
        return $is_regular;
    }
    
}