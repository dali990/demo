<?php

/**
 * @package SIMA
 * @subpackage Accounting
 */
class FileGenerateAccountOrderBehavior extends SIMAActiveRecordBehavior
{
    public function generateAccountOrder($just_check = false)
    {
        $owner = $this->owner;
        if (isset($owner->bill_in))
        {
            $_config_code = 'accounting.counters.account_order_bill_in';
            $_day = Day::getByDate($owner->bill_in->income_date);
            $model_label = BillInGUI::modelLabel();
        }
        elseif(isset($owner->bill_out))
        {
            $_config_code = 'accounting.counters.account_order_bill_out';
            $_day = Day::getByDate($owner->bill_out->income_date);
            $model_label = BillOutGUI::modelLabel();
        }
        elseif(isset($owner->bank_statement))
        {
            $_config_code = 'accounting.counters.account_order_bank_statement';
            $_day = Day::getByDate($owner->bank_statement->date);
            $model_label = BankStatementGUI::modelLabel();
        }
//        elseif(isset($owner->regular_paycheck_period))
//        {
//            
//        }
//        elseif(isset($owner->mesecni_pdv))
//        {
//            
//        }
        else
        {
            throw new SIMAWarnExceptionNotForAutoAccountOrder($owner);
        }
        
        $user_config_counter_id = Yii::app()->configManager->get($_config_code, false);
        if (is_null($user_config_counter_id))
        {
            throw new SIMAWarnExceptionNotForAutoAccountOrderConfig($owner, $model_label);
        }   
        
        if ($just_check)
        {
            return;
        }
        
        $counter = UserConfigCounter::model()->findByPk($user_config_counter_id);
        $account_order_number = $counter->getNextText($_day, true);
        
        $account_order = new AccountOrder();
        $account_order->order = $account_order_number;
        $account_order->date = $_day->day_date;
        $account_order->validate();
        if (!$account_order->hasErrors())
        {
            $account_order->save();
            $counter->getNextText($_day);
            ConnectAccountOrderDocuments::addDocumentToOrder($account_order->id, $owner->id);
        }
        else
        {
            Yii::app()->raiseNote(SIMAHtml::showErrors($account_order));
        }
        
        
    }
    
}