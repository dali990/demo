<?php

/**
 * @package SIMA
 * @subpackage Accounting
 */
class PaymentBehavior extends SIMAActiveRecordBehavior
{
    public function addDocumentWidgetAction($px_size=16)
    {
        $owner = $this->owner;
        
        $result = [];
        
        if($owner->canAddDocument())
        {
            $params = [
                'init_data'=>array(
                    'AdvanceBillOut'=>array(
                        'partner_id' => $owner->partner_id,
                        'amount' => $owner->amount,
                        'advance_id' => $owner->id
                    )
                ),
                'updateModelViews'=>SIMAHtml::getTag($owner)
            ];
            $result =  SIMAHtmlButtons::modelFormOpenWidgetAction(AdvanceBillOut::model(), $params, $px_size);
        }
        
        return $result;
    }
}