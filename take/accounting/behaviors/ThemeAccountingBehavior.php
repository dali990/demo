<?php

class ThemeAccountingBehavior extends SIMAActiveRecordBehavior
{
    public function tabs($event)
    {
        $owner = $this->owner;
        if ($owner->isThemeSettingSupported('has_cost_location') && Yii::app()->user->checkAccess('accountingJobStat'))
        {
            $event->addResult([
                [
                    'title' => Yii::t('AccountingModule.CostLocation', 'ThemeTabFinance'),
                    'code'=>'theme_accounting_finance',
                    'module_origin' => 'accounting',
                    'order' => 150
                ],
                [
                    'title' => Yii::t('AccountingModule.CostLocation', 'ThemeTabFinanceOld'),
                    'code' => 'theme_accounting_finance_old',
                    'module_origin' => 'accounting',
                    'params_function' => 'tabFinance',
                    'pre_path' => 'theme_accounting_finance'
                ],
                [
                    'title' => Yii::t('AccountingModule.CostLocation', 'ThemeTabFinanceIncome'),
                    'code' => 'theme_accounting_cost_location_income',
                    'module_origin' => 'accounting',
                    'pre_path' => 'theme_accounting_finance'
                ],
                [
                    'title' => Yii::t('AccountingModule.CostLocation', 'ThemeTabFinanceCost'),
                    'code' => 'theme_accounting_cost_location_cost',
                    'module_origin' => 'accounting',
                    'pre_path' => 'theme_accounting_finance'
                ]
            ]);
        }
    }
    
    protected function tabFinance()
    {
        $owner = $this->owner;
        $tabs = [
            [
                'title' => 'Opste informacije',
                'code' => 'info',
                'action' => 'base/model/view',
                'get_params' => array('model' => get_class($owner), 'model_id'=>$owner->id,'model_view' => 'financeInfo')
//                    'html' => Yii::app()->controller->renderModelView($job,'financeInfo'),
            ],
            [

                'title' => 'Fakturisanje',
                'code' => 'income',
                'html' => Yii::app()->controller->widget('SIMAGuiTable', [
                            'model'=> BillOut::model(),
                            'columns_type' => 'income_in_job',
                            'add_button' => true,
                            'fixed_filter'=>array(
                                'file'=> [
                                    'belongs' => $owner->tag->query
                                ]


//                                    'display_scopes' => array(
//                                        'byName'
//                                    ),
                            )
                        ], true)
            ],
            [
                'title' => 'Direktni troskovi po podizvodjacima',
                'code' => 'outcome',
                'html' => Yii::app()->controller->widget('SIMAGuiTable', [
                    'model'=> JobFinance::model(),
                    'columns_type' => 'outcome',
                    'fixed_filter'=>array(
                        'display_scopes' => array(
                            'withMainJoin',
                            'withMainSelect',
                            'byPartners',
                            'forTheme' => array(
                                $owner->id
                            )
                        ),
                        
                    )
                ], true)
            ],
            [
                'title' => 'Troskovi materijala',
                'code' => 'warehouse_outcome',
                'html' => (isset($owner->cost_location)) ? 
                            Yii::app()->controller->renderModelView($owner->cost_location, 'info') 
                            : 
                            Yii::t('AccountingModule.Theme','CostLocationNotSet')
            ]
        ];
        
        if (isset($owner->cost_location->warehouse_production_location))
        {
            $tabs[] = [
                'title' => 'Trebovanja',
                'code' => 'warehouse_requisitions',
                'html' => Yii::app()->controller->widget('SIMAGuiTable', [
                    'model'=> WarehouseRequisitionLocation::model(),
                    'columns_type' => 'inJob',
                    'fixed_filter'=>[
                        'warehouse_to' => [
                            'ids' => [$owner->cost_location->warehouse_production_location->id]
                        ]
                    ]
                ], true)
            ];
        }
        
        
        $html = Yii::app()->controller->widget('SIMATabs',[
            'tabs' => $tabs,
            'default_selected' => 'info'
        ],true);
        
//        $procesed_html = Yii::app()->controller->processOutput($html);
        
        return array(
            'html' => $html
        );
    }
    
    public function settings($event) 
    {
        $event->addResult([
            'has_cost_location' => [
                'type' => 'boolean',
                'title' => Yii::t('AccountingModule.Theme', 'IsCostLocation'),
                'default_value' => true,
                'check_access_func' => function(Theme $theme, User $user=null) {
                    return $theme->canRemoveCostLocation();
                }
            ]
        ]);
    }
    
    public function canRemoveCostLocation()
    {
//        $_id = $this->owner->cost_location->id;
        //ne moze ovo jer dovlacenje cost_location resetuje da je potrebno ucitavanje scope-a
//        $cost_location_with_total_income = CostLocation::model()->withIncomeCostTotal()->findByPk($this->owner->cost_location->id);
//        $cost_location_with_total_income = CostLocation::model()->withIncomeCostTotal()->findByPk($_id);
        return false;
        //MIlosS: ne vidim pravu primenu toga da se iskljuci mesto troska, a komplikovanije je za programiranje
//        return 
//            $cost_location_with_total_income->income_total == 0
//            &&
//            $cost_location_with_total_income->income_advance_total == 0
//            &&
//            $cost_location_with_total_income->cost_total == 0
//            &&
//            $cost_location_with_total_income->cost_advance_total == 0
//        ;
    }
    
    public function beforeSave($event)
    {
        $owner = $this->owner;
        if (empty($owner->cost_location_id))
        {
            $_cl = new CostLocation();
            $_cl->prefix = 'ST';
            if (CostLocationGUI::generateName())
            {
                $_cl->number = $owner->id;
            }
            else
            {
                $_cl->name = 'ST'.$owner->id;
            }
            $_cl->work_name = $owner->name;
            $_cl->for_analytics = false;
            $_cl->model_name = 'Theme';
            $_cl->model_id = $owner->id;
            $_cl->save();
            
            $owner->cost_location_id = $_cl->id;
            $owner->cost_location = $_cl;
        }
    }
    
    public function afterSave($event)
    {
        $owner = $this->owner;
        //promenjeno na pozitivno
//        if ($owner->settingsChanged('has_cost_location') && $owner->getSettingsValue('has_cost_location'))
//        {
//            
//        }
        if ($owner->cost_location->prefix === 'ST')
        {
            if (CostLocationGUI::generateName())
            {
                if (empty($owner->cost_location->number))
                {
                    $owner->cost_location->number = $owner->id;
                }
            }
            else
            {
                if (empty($owner->cost_location->name) 
                        || $owner->cost_location->name=== 'ST' ) //ako je nova tema u pitanju
                {
                    $owner->cost_location->name = 'ST_'.$owner->id;
                }
            }
            
            $owner->cost_location->work_name = $owner->name;
            $owner->cost_location->save();
        }
    }
    
    public function afterDelete($event)
    {
        $this->owner->cost_location->delete();
    }
    
}
