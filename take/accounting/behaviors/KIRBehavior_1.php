<?php

/**
 * @package SIMA
 * @subpackage Accounting
 */
class KIRBehavior extends SIMAActiveRecordBehavior
{
    public function getReportParams($month, $year, $update_func=null, $for_pdf_or_csv = false)
    {
        $month_model = Month::get($month, $year);
        $rows = KIR::model()->byOrderInYear()->inMonth($month, $year)->findAll();
        
        $sum_row = [
            'KIR_7' => 0,
            'KIR_8' => 0,
            'KIR_9' => 0,
            'KIR_10' => 0,
            'KIR_11' => 0,
            'KIR_12' => 0,
            'KIR_13' => 0,
            'KIR_14' => 0,
            'KIR_15' => 0,
            'KIR_16' => 0,
            'KIR_17' => 0,
            'KIR_18' => 0,
        ];
        
        $i = 1;
        $rows_cnt = count($rows);
        $repacked_rows = [];
        foreach ($rows as $row)
        {
            if (!empty($update_func))
            {
                call_user_func($update_func, round(($i++/$rows_cnt)*100, 0, PHP_ROUND_HALF_DOWN));
            }
            
            $repacked_row = [
                'order_in_year' => $row->order_in_year,
                'booking_date' => $row->bill->booking_date,
                'bill_number' => $for_pdf_or_csv ? $row->bill->bill_number : $row->bill->DisplayHTML,
                'income_date' => $row->bill->income_date,
                'partner_display_name' => $for_pdf_or_csv ? $row->bill->partner->DisplayName : $row->bill->partner->DisplayHTML,
                'partner_pib_jmbg' => $row->bill->partner->pib_jmbg,
                'row_model' => $row,
                'KIR_7' => $row->getAttributeDisplay('KIR_7'),
                'KIR_8' => $row->getAttributeDisplay('KIR_8'),
                'KIR_9' => $row->getAttributeDisplay('KIR_9'),
                'KIR_10' => $row->getAttributeDisplay('KIR_10'),
                'KIR_11' => $row->getAttributeDisplay('KIR_11'),
                'KIR_12' => $row->getAttributeDisplay('KIR_12'),
                'KIR_13' => $row->getAttributeDisplay('KIR_13'),
                'KIR_14' => $row->getAttributeDisplay('KIR_14'),
                'KIR_15' => $row->getAttributeDisplay('KIR_15'),
                'KIR_16' => $row->getAttributeDisplay('KIR_16'),
                'KIR_17' => $row->getAttributeDisplay('KIR_17')
            ];
            
            array_push($repacked_rows, $repacked_row);
            
            foreach ($sum_row as $key => $value)
            {
                $sum_row[$key] += $row->$key;
            }
        }
        
        $total_pdv = SIMAHtml::number_format($sum_row['KIR_13'] + $sum_row['KIR_15']);
        $total_income = SIMAHtml::number_format($sum_row['KIR_16']);
        foreach ($sum_row as $key => $value)
        {
            $sum_row[$key] = SIMAHtml::number_format($value);
        }
        
        $month_disp = $month_model->toString();
        $month_disp .= " $year. godine";

        return [
            'rows'=> $repacked_rows,
            'sum_row' => $sum_row,
            'month' => $month_disp,
            'total_pdv' => $total_pdv,
            'total_income' => $total_income
        ];
    }
}