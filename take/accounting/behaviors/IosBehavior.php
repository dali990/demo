<?php

/**
 * @package SIMA
 * @subpackage Accounting
 */
class IosBehavior extends SIMAActiveRecordBehavior
{
    static $ADD_TO_PERMANENT = 'permanent';
    static $ADD_TO_TEMP = 'temp';
    
    //stavke za trenutni IOS. Ubcuje se samo neophodno
    private $ios_items = [];
    private $_ios_items_recognize_problems = [];
    
    //ovo se koristi u algoritmu da se vidi da li je ulazni ili izlazni konto
    //zarad debit/credit orjentacije
    private $invoice;
    
    public function regenerateIosItems()
    {
        //brisanje stavki za ios koji se regenerise
        $owner = $this->owner;
        $account_id = $owner->account_id;
        $ios_date = $owner->ios_date;
        $year = Year::get(Date('Y',strtotime($owner->ios_date)));
        
        $this->calc_invoice();
        
        //mora da se stavka prepozna u prvoj fazi na osnovu redosleda, a u drugoj da se reimplementira i da se za svaku stavku prati kom stavu
        
        $criteria = new SIMADbCriteria([
            'Model' => 'AccountTransaction',
            'model_filter' => [
                'account' => [
                    'ids' => $account_id
                ],
                'display_scopes' => [
                    'byOrder',
                    'withSaldoAndNoStart',
                    
                ],
                'filter_scopes'=>[
                    'upToDateFilter' => [
                        'date' => $ios_date, 
                    ]
                ]
            ]
        ]);
        
        $account_transactions = AccountTransaction::model()->findAll($criteria);
        
        $start_account_transaction = null;
        
        foreach($account_transactions as $account_transaction)
        {
            if(!empty($account_transaction->account_document->file->bill))
            {
                $bill = $account_transaction->account_document->file->bill;
                $this->invoice = $bill->invoice; //TODO za brisanje kad se obradi funkcija calc_invoice
                $this->add_to_ios_items($bill, $account_transaction->debit, $account_transaction->credit);
            } 
            else if(!empty($account_transaction->account_document->file->bank_statement))
            {
                $this->add_to_ios_items_from_bank_statement($account_transaction, self::$ADD_TO_PERMANENT);
            } 
            else if(!empty($account_transaction->account_document->start_order_for_year))
            {
                $start_account_transaction = $account_transaction;
            }
            else if(!empty($account_transaction->account_document->finish_order_for_year))
            {
                //IGNORE
            }
            else
            {
                $this->report_unknown_transaction($account_transaction);
            }
            
        }
        
        if (!is_null($start_account_transaction))
        {
//            $this->startFromPrevYear($start_account_transaction, $year->prev());
            $needed_start_value = $start_account_transaction->debit + $start_account_transaction->credit;
            $this->analyzeAccount($start_account_transaction->account->forYear($year->prev()), $needed_start_value);
        }
        
        if (count($this->_ios_items_recognize_problems)>0)
        {
//            $msg = "Stav od $date u iznosu od $amount_display ima broj stavova razlicit od 1. Ima ih: $cnt. <br />IOS NECE biti tacno izracunat. Konto: ".$account_transaction->account->DisplayName;
            $msg = "Neki stavovi na kontu '".$account_transaction->account->DisplayName."' nisu mogli biti upareni, pa su izostavljeni iz IOS-a:<ul><li>"
                    . implode('</li><li>', $this->_ios_items_recognize_problems)
                    . '</li></ul>';
            
            Yii::app()->raiseNote($msg);
        }
        
        $this->save_new_ios_items();
        $owner->generatePdf();
    }
    
    private function analyzeAccount(Account $account, $needed_start_value)
    {
        $account_transactions = AccountTransaction::model()->findAll([
            'model_filter' => [
                'account' => [
                    'ids' => $account->id
                ]
            ]
        ]);
        
        //parametri se restartuju za ovu godinu
        $_curr_start_account_transaction = null;
        $this->_curr_year_ioses = [];
        
        foreach($account_transactions as $account_transaction)
        {
            if(!empty($account_transaction->account_document->file->bill))
            {
                $bill = $account_transaction->account_document->file->bill;
                $this->invoice = $bill->invoice; //TODO za brisanje kad se obradi funkcija calc_invoice
                $this->add_to_current_ios_items($bill, $account_transaction->debit, $account_transaction->credit);
            } 
            else if(!empty($account_transaction->account_document->file->bank_statement))
            {
                $this->add_to_ios_items_from_bank_statement($account_transaction, self::$ADD_TO_TEMP);
            } 
            else if(!empty($account_transaction->account_document->start_order_for_year))
            {
                $_curr_start_account_transaction = $account_transaction;
            }
            else if(!empty($account_transaction->account_document->finish_order_for_year))
            {
                //IGNORE
            }
            else
            {
                $this->report_unknown_transaction($account_transaction);
            }
            
        }
        
        //pocetno stanje sabiranjem ne placenih racuna u trenutnoj godini
        $_start_value_sum = 0;
        
        foreach ($this->_curr_year_ioses as $_temp_ios) 
        {
            $bill = $_temp_ios['bill'];
            $debit = $_temp_ios['debit'];
            $credit = $_temp_ios['credit'];
            
            if (
                    ($bill->invoice  && SIMAMisc::lessThen($_temp_ios['credit'], $_temp_ios['debit']))
                    ||
                    (!$bill->invoice && SIMAMisc::lessThen($_temp_ios['debit'], $_temp_ios['credit']))
                )
            {
                $this->add_to_ios_items($_temp_ios['bill'], $_temp_ios['debit'], $_temp_ios['credit']);
                $_start_value_sum += $_temp_ios['debit'] - $_temp_ios['credit'];
            }
        }
        
        if (SIMAMisc::areEqual($_start_value_sum, $needed_start_value))
        {
            return;
        }
        elseif(SIMAMisc::lessThen($_start_value_sum, $needed_start_value))
        {
            $new_needed_start_value = $needed_start_value - $_start_value_sum;
            $no_previous = false;
            $_prev_year = $account->year->prev();
            if (is_null($_prev_year))
            {
                $msg = 'TREBA POZVATI PRETHODNU GODINU, ali ona ne postoji. Trenutna je  '.$account->year->DisplayName;
                $this->_ios_items_recognize_problems[] = $msg;
                $no_previous = true;
            }
            $_prev_account = $account->forYear($_prev_year);
            if (is_null($_prev_account))
            {
                $msg = 'TREBA POZVATI PRETHODNU GODINU '.$_prev_year->DisplayName. ' ali u njoj nema potrebnog konta. ';
                $this->_ios_items_recognize_problems[] = $msg;
                $no_previous = true;
            }

            if (is_null($_curr_start_account_transaction))
            {
                $msg = 'TREBA POZVATI PRETHODNU GODINU '.$_prev_year->DisplayName. ' ali nema pocetne transakcije. '
                        . "preostala razlika je $new_needed_start_value";
                $this->_ios_items_recognize_problems[] = $msg;
                $no_previous = true;
            }
            if ($no_previous)
            {
                $this->add_to_ios_items(null, $this->invoice?$new_needed_start_value:0,  $this->invoice?0:$new_needed_start_value);
            }
            else
            {
                $this->analyzeAccount($account->forYear($_prev_year), $new_needed_start_value);
            }
        }
        else 
        {
            $year_display = $account->year->DisplayName;
            throw new SIMAWarnException("NE UKLAPA SE ($year_display). Pocetno stanje je $needed_start_value, a sabrano je vise  -> $_start_value_sum");
        }
    }
    
    private $_curr_year_ioses = [];
    
    private function add_to_current_ios_items(Bill $bill, $debit, $credit)
    {        
        if (!isset($this->_curr_year_ioses[$bill->id]))
        {
            $this->_curr_year_ioses[$bill->id] = [
                'bill' => $bill,
                'debit' => $debit,
                'credit' => $credit
            ];
        }
        else
        {
            $this->_curr_year_ioses[$bill->id]['debit'] += $debit;
            $this->_curr_year_ioses[$bill->id]['credit'] += $credit;
        }
        
    }
    
    public function add_to_ios_items(?Bill $bill, $debit, $credit)
    {
        $_index = is_null($bill)?-1:$bill->id;
        if (!isset($this->ios_items[$_index]))
        {
            $this->ios_items[$_index] = [
                'bill' => $bill,
                'debit' => $debit,
                'credit' => $credit
            ];
        }
        else
        {
            $this->ios_items[$_index]['debit'] += $debit;
            $this->ios_items[$_index]['credit'] += $credit;
        }
    }
    
    private function save_new_ios_items()
    {
        $old_ios_items = IosItem::model()->findAllByAttributes(['ios_id' => $this->owner->id]);
        foreach($old_ios_items as $old_ios_item)
        {
            $old_ios_item->delete();
        }
        
        foreach ($this->ios_items as $bill_id => $ios_item) 
        {
            if (!SIMAMisc::areEqual($ios_item['debit'], $ios_item['credit']))
            {    
                $new_ios_item = new IosItem();
                $new_ios_item->ios_id = $this->owner->id;
                $new_ios_item->bill_id = $bill_id===-1 ? null : $bill_id;
                $new_ios_item->debit = $ios_item['debit'];
                $new_ios_item->credit = $ios_item['credit'];
                $new_ios_item->save();
            }
        }

    }
    
    private function add_to_ios_items_from_bank_statement(AccountTransaction $account_transaction, $where_to_add)
    {
        $amount = $account_transaction->debit + $account_transaction->credit;
        $bank_statement = $account_transaction->account_document->file->bank_statement;
        $payments = Payment::model()->byOfficial()->findAllByAttributes([
            'bank_statement_id'=>$bank_statement->id, 
            'partner_id' => $this->owner->partner_id,
            'amount' => $amount,
            'invoice' => empty($account_transaction->debit)
        ]);

        //redni broj placanja koji se gleda. Default je 0 jer se pretpostavlja da ima samo jedna stavka
        //ukoliko je vise, onda se gleda sortirano i index se vadi posebnim upitom
        $_order = 0;
       
        if (count($payments) === 0)
        {
            $this->_ios_items_recognize_problems[] = 
                    ($account_transaction->account_document->account_order->booking_date)
                    .' - '
                    .SIMAHtml::number_format($amount);
        }
        else
        {
            $similar_transactions = AccountTransaction::model()->byOrder()->findAllByAttributes([
                'account_document_id' => $account_transaction->account_document_id,
                'account_id' => $account_transaction->account_id,
                'debit' => $account_transaction->debit,
                'credit' => $account_transaction->credit
            ]);
            
            while ($similar_transactions[$_order]->id !== $account_transaction->id) 
            {
                $_order++;
            }
            
        }
        
        foreach($payments[$_order]->releases as $bill_release)
        {
            if ($bill_release->bill->invoice)
            {
                $credit = $bill_release->amount;
                $debit = 0;//$bill_release->amount;
            }
            else
            {
                $credit = 0;//$bill_release->amount;
                $debit = $bill_release->amount;
            }
            if ($where_to_add === self::$ADD_TO_PERMANENT)
            {
                $this->add_to_ios_items($bill_release->bill, $debit, $credit);
            }
            else
            {
                $this->add_to_current_ios_items($bill_release->bill, $debit, $credit);
            }
        }
    }
    
    private function report_unknown_transaction(AccountTransaction $account_transaction)
    {
        $amount = $account_transaction->debit + $account_transaction->credit;
        $amount_display = SIMAHtml::number_format($amount);
        $date = $account_transaction->account_document->account_order->date;
//        $msg = "Stav od $date u iznosu od $amount_display nije vezan za konkretan racun, pa ce biti dodeljen pocetnoj vrednosti. Konto: ".$account_transaction->account->DisplayName;
        //MilosS(18.11.2019): privremeno zakemntarisno, ali treba reimplemnetirati sve vezano za IOS 
//                Yii::app()->raiseNote($msg);

//                $this->add_to_ios_items(null, $account_transaction->debit, $account_transaction->credit);
        $ad = $account_transaction->account_document;
        $note = "Nepoznata obrada za dokument: "
                ."($ad->id)".$ad->DisplayName
                .' STAV: '
                .$account_transaction->DisplayName;
        $theme_id = 3097;
        $theme_name = "SIMA_razvoj_modul finansije_AutoBAF_generisanje IOS-a";
        Yii::app()->errorReport->createAutoClientBafRequest($note, $theme_id ,$theme_name);
        $this->_ios_items_recognize_problems[] = $date.' - '.$amount_display;
    }
    
    private function calc_invoice()
    {
        //TODO ovo treba izracunati na osnovu konta i podesavanja u konfiguracijama
        //i onda pobrisati po kodu gde se usput postavlja na osnovu racuna
        $this->invoice = true;
    }
}