<?php

class SIMADbExceptionS0007UnreleasedAmountUnderZero extends SIMADbException
{

    public function __construct()
    {
        $err_msg = 'Neplaceni deo racuna ide ispod nule GRESKA';
        parent::__construct($err_msg);
    }
}