<?php

class SIMAWarnExceptionNotForAutoAccountOrderConfig extends SIMAWarnExceptionNotForAutoAccountOrder
{    
    public function __construct($document, $document_type)
    {        
        $err_msg = Yii::t('AccountingModule.GenerateAccountOrder','NotForAutoAccountOrderConfig',['{document_type}' => $document_type]);
        parent::__construct($document, $err_msg);
    }
}