<?php

class SIMAWarnUnreleasedAmountUnderZero extends SIMAWarnException
{
    private $bill = null;
            
    public function __construct(Bill $bill)
    {
        $this->bill = $bill;
        
        $err_msg = Yii::t('AccountingModule.BillItem','ChangeUnreleaseAmount',['{bill_name}' => $bill->DisplayName]);
        parent::__construct($err_msg);
    }
}