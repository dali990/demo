<?php

class SIMAWarnBillCancelAdvanceConneced extends SIMAWarnException
{
    private $bill = null;
            
    public function __construct(Bill $bill, $additional_message = '')
    {
        $this->bill = $bill;
        
        $err_msg = Yii::t('AccountingModule.Bill','BillCancelAdvanceConneced');
        
        /**
         * SAMO PRIVREMENO DOK NE VIDIMO STA JE
         */
        
//        $autoBAF_message = $additional_message.' ||||||||||||| '
//                .SIMAMisc::toTypeAndJsonString($bill)
//                .' ||||||||||||||| '
//                .SIMAMisc::toTypeAndJsonString($bill->getOld());
//        Yii::app()->errorReport->createAutoClientBafRequest($autoBAF_message);
        
        parent::__construct($err_msg);
    }
}