<?php

class SIMAExceptionPOPDVBookingTypeNotExists extends SIMAException
{
    public function __construct()
    {
        $err_msg = 'NISTE NAVELI DOBAR TIP KNJZENJA ZA POPDV';
        parent::__construct($err_msg);
    }
}