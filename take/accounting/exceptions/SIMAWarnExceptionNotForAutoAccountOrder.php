<?php

class SIMAWarnExceptionNotForAutoAccountOrder extends SIMAWarnException
{
    protected $document = null;
            
    public function __construct(File $document, $custom_error = '')
    {
        $this->document = $document;
        
        if (empty($custom_error))
        {
            $err_msg = Yii::t('AccountingModule.GenerateAccountOrder','NotForAutoAccountOrder',['{document}' => $document->DisplayName]);
        }
        else
        {
            $err_msg = $custom_error;
        }
        parent::__construct($err_msg);
    }
}