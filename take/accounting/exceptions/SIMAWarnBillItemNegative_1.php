<?php

class SIMAWarnBillItemNegative extends SIMAWarnException
{
    private $bill = null;
            
    public function __construct(Bill $bill, $additional_message, SIMAActiveRecord $model_changed)
    {
        $this->bill = $bill;
        
        $err_msg = Yii::t('AccountingModule.BillItem','BillItemNegative',['{bill_name}' => $bill->DisplayName]);
        
        /**
         * SAMO PRIVREMENO DOK NE VIDIMO STA JE
         */
        
        $autoBAF_message = $additional_message
                .'||||||||||||||||||| trenutno stanje stavki u racunu'
                .SIMAMisc::toTypeAndJsonString($bill->bill_items)
                .'|||||||||||||||||||'
                .'model je '. get_class($model_changed).' ||||||||||||| '
                .SIMAMisc::toTypeAndJsonString($model_changed)
                .' ||||||||||||||| '
                .SIMAMisc::toTypeAndJsonString($model_changed->getOld());
        
        Yii::app()->errorReport->createAutoClientBafRequest($autoBAF_message, 3068, 'AutoBafReq bill_item cannot be negativ if not discount_item or RELIEF_BILL');
        
        parent::__construct($err_msg);
    }
}