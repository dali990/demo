<?php

class SIMAExceptionBankNotFoundByCode extends SIMAException
{
    public function __construct($code)
    {
//        $err_msg = 'not found bank by code '.$code;
        $err_msg = Yii::t('AccountingModule.Bank', 'NotFoundByCodeException', [
            '{code}' => $code
        ]);
        parent::__construct($err_msg);
    }
}