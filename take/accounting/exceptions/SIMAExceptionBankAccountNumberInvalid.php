<?php

class SIMAExceptionBankAccountNumberInvalid extends SIMAException
{
    public $bank_account_number;
    public $db_err_message;
    
    public function __construct($bank_account_number, $db_err_message)
    {
        $this->bank_account_number = $bank_account_number;
        $this->db_err_message = $db_err_message;
        
        $err_msg = $db_err_message;
        parent::__construct($err_msg);
    }
}