<?php

class SIMAWarnNoCurrencyRate extends SIMAWarnException
{
    public function __construct(Currency $c, Day $d)
    {
        $err_msg = "Nije unet srednji kurs za valutu $c->DisplayName na dan: $d->day_date";
        parent::__construct($err_msg);
    }
}