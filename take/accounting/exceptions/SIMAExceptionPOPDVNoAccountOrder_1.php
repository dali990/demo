<?php

class SIMAExceptionPOPDVNoAccountOrder extends SIMAException
{
    public function __construct()
    {
        $err_msg = 'Ne moze da se racuna POPDV za sam nalog za knjizenje';
        parent::__construct($err_msg);
    }
}