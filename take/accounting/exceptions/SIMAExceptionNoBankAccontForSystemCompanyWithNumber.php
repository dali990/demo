<?php

class SIMAExceptionNoBankAccontForSystemCompanyWithNumber extends SIMAException
{
    public function __construct($bank_account_number)
    {
        $err_msg = Yii::t('AccountingModule.BankStatement', 'NoBankAccontForSystemCompanyWithNumber', [
            '{bank_account_number}' => $bank_account_number
        ]);
        parent::__construct($err_msg);
    }
}

