<?php

class SIMAExceptionBillNotRecurring extends SIMAException
{
    private $bill = null;
            
    public function __construct(Bill $bill)
    {
        $this->bill = $bill;
        
        $err_msg = BillGui::modelLabel().' '.$bill->DisplayName.' '.Yii::t('BaseModule.Common','IsNot').' '.RecurringBillGUI::modelLabel();
        parent::__construct($err_msg);
    }
}