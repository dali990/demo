<?php

class SIMAExceptionPOPDVCodeJustSum extends SIMAException
{
    private $popdv_code = null;
            
    public function __construct($code)
    {
        $this->popdv_code = $code;
        
        $err_msg = "U POPDV izvestaju za kod '$code' ne postoji izracunavanje sume, nego samo sabiranje";
        parent::__construct($err_msg);
    }
}