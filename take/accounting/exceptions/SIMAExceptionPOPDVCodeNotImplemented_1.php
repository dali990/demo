<?php

class SIMAExceptionPOPDVCodeNotImplemented extends SIMAException
{
    private $popdv_code = null;
            
    public function __construct($code)
    {
        $this->popdv_code = $code;
        
        $err_msg = "U POPDV izveštaju za kod '$code' nije implementirano izračunavanje";
        parent::__construct($err_msg);
    }
}