<?php
    $questionmark_td = $with_questionmark ? 'questionmark_td' : '';
?>

<!DOCTYPE html>
<html>
    <head>
        <title>PDV Pdf</title>        
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <div class='title-small'>Образац ПОПДВ</div>
        <div class='title-main'>
            <span class='title-main-line'>ПРЕГЛЕД ОБРАЧУНА ПДВ</span>
            <span class='title-main-line'>ЗА ПОРЕСКИ ПЕРИОД ОД <?=$month->firstDay()->DisplayName?> ДО <?=$month->lastDay()->DisplayName?></span>
        </div>
        <table class='table-pop'>        
            <tr><th colspan=2><b>ПОДАЦИ О ПОДНОСИОЦУ</b></th></tr>
            <tr>
                <td class='first-column'>Назив, односно име, презиме и адреса</td>
                <td>
                    <?=Yii::app()->company->DisplayName?>, <?=!empty(Yii::app()->company->main_address) ? Yii::app()->company->main_address->DisplayNameLong : ''?>
                </td>
            </tr>
            <tr>
                <td class='first-column'>ПИБ</td>
                <td><?=Yii::app()->company->PIB?></td>
            </tr>
        </table>
        <div class='table-legend'>У обрасцу ПОПДВ износи се уписују у динарима, без децимала</div>
        <table class='table-1'>
            <tr>
                <th colspan=2>1. ПРОМЕТ ДОБАРА И УСЛУГА ЗА КОЈИ ЈЕ ПРОПИСАНО ПОРЕСКО ОСЛОБАЂАЊЕ СА ПРАВОМ НА ОДБИТАК ПРЕТХОДНОГ ПОРЕЗА</th>
                <th class='fixed-width'>Накнада / Вредност</th>
            </tr>
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('11'); } ?>
                    <div class='id-div'>1.1</div>
                </td>
                <td>Промет добара која се отпремају у иностранство, укључујући и повећање, односно смањење накнаде за тај промет</td>
                <td class='numeric'><?= $params['11']; ?></td>
            </tr>
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('12'); } ?>
                    <div class='id-div'>1.2</div>
                </td>
                <td>Промет добара која се отпремају на територију Аутономне покрајине Косово и Метохија, укључујући и повећање, односно смањење накнаде за тај промет</td>
                <td class='numeric'><?= $params['12']; ?></td>
            </tr>
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('13'); } ?>
                    <div class='id-div'>1.3</div>
                </td>
                <td>Промет добара која се уносе у слободну зону и промет добара и услуга у слободној зони, укључујући и повећање, односно смањење накнаде за тај промет</td>
                <td class='numeric'><?= $params['13']; ?></td>
            </tr>
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('14'); } ?>
                    <div class='id-div'>1.4</div>
                </td>
                <td>Промет добара и услуга, осим из тачке 1.1 до 1.3, укључујући и повећање, односно смањење накнаде за тај промет</td>
                <td class='numeric'><?= $params['14']; ?></td>
            </tr>
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('15'); } ?>
                    <div class='id-div'><b>1.5</b></div>
                </td>
                <td><b>Укупан промет (1.1 + 1.2 + 1.3 + 1.4)</b></td>
                <td class='numeric'><?= $params['15']; ?></td>
            </tr>
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('16'); } ?>
                    <div class='id-div'>1.6</div>
                </td>
                <td>Промет добара и услуга без накнаде</td>
                <td class='numeric'><?= $params['16']; ?></td>
            </tr>
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('17'); } ?>
                    <div class='id-div'>1.7</div>
                </td>
                <td>Накнада или део накнаде наплаћен пре извршеног промета (аванс)</td>
                <td class='numeric'><?= $params['17']; ?></td>
            </tr>
        </table>
        <table class='table-2'>
            <tr>
                <th colspan=2>2. ПРОМЕТ ДОБАРА И УСЛУГА ЗА КОЈИ ЈЕ ПРОПИСАНО ПОРЕСКО ОСЛОБАЂАЊЕ БЕЗ ПРАВА НА ОДБИТАК ПРЕТХОДНОГ ПОРЕЗА</th>
                <th class='fixed-width'>Накнада / Вредност</th>
            </tr>
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('21'); } ?>
                    <div class='id-div'>2.1</div>
                </td>
                <td>Промет новца и капитала, укључујући и повећање, односно смањење накнаде за тај промет</td>
                <td class='numeric'><?= $params['21']; ?></td>
            </tr>
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('22'); } ?>
                    <div class='id-div'>2.2</div>
                </td>
                <td>Промет земљишта и давање у закуп земљишта, укључујући и повећање, односно смањење накнаде за тај промет</td>
                <td class='numeric'><?= $params['22']; ?></td>
            </tr>
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('23'); } ?>
                    <div class='id-div'>2.3</div>
                </td>
                <td>Промет објеката, укључујући и повећање, односно смањење накнаде за тај промет</td>
                <td class='numeric'><?= $params['23']; ?></td>
            </tr>
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('24'); } ?>
                    <div class='id-div'>2.4</div>
                </td>
                <td>Промет добара и услуга, осим из тачке 2.1 до 2.3, укључујући и повећање, односно смањење накнаде за тај промет</td>
                <td class='numeric'><?= $params['24']; ?></td>
            </tr>
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('25'); } ?>
                    <div class='id-div'><b>2.5</b></div>
                </td class='numeric'>
                
                <td><b>Укупан промет (2.1 + 2.2 + 2.3 + 2.4)</b></td>
                <td class='numeric'><?= $params['25']; ?></td>
            </tr>
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('26'); } ?>
                    <div class='id-div'>2.6</div>
                </td>
                <td>Промет добара и услуга без накнаде</td>
                <td class='numeric'><?= $params['26']; ?></td>
            </tr>
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('27'); } ?>
                    <div class='id-div'>2.7</div>
                </td>
                <td>Накнада или део накнаде наплаћен пре извршеног промета (аванс)</td>
                <td class='numeric'><?= $params['27']; ?></td>
            </tr>
        </table>
        <table class='table-3'>
            <tr>
                <th colspan=8 rowspan=2>3. ОПОРЕЗИВИ ПРОМЕТ ДОБАРА И УСЛУГА  КОЈИ ВРШИ ОБВЕЗНИК ПДВ И ОБРАЧУНАТИ ПДВ</th>
                <th colspan=2 class='fixed-width'>Општа стопа</th>
                <th colspan=2 class='fixed-width'>Посебна стопа</th>
            </tr>
            <tr>
                <th>Основица</th> <th>ПДВ</th>
                <th>Основица</th> <th>ПДВ</th>
            </tr>
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('31'); } ?>
                    <div class='id-div'>3.1</div>
                </td>
                <td colspan=7>Први пренос права располагања на новоизграђеним грађевинским објектима за који је порески дужник обвезник ПДВ који врши тај промет</td>
                <td class='numeric'><?= $params['311']; ?></td>
                <td class='numeric'><?= $params['312']; ?></td>
                <td class='numeric'><?= $params['313']; ?></td>
                <td class='numeric'><?= $params['314']; ?></td>
            </tr>
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('32'); } ?>
                    <div class='id-div'>3.2</div>
                </td>
                <td colspan=7>Промет за који је порески дужник обвезник ПДВ који врши тај промет, осим из тачке 3.1</td>
                <td class='numeric'><?= $params['321']; ?></td>
                <td class='numeric'><?= $params['322']; ?></td>
                <td class='numeric'><?= $params['323']; ?></td>
                <td class='numeric'><?= $params['324']; ?></td>
            </tr>
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('33'); } ?>
                    <div class='id-div'>3.3</div>
                </td>
                <td colspan=7>Пренос права располагања на грађевинским објектима за који обвезник ПДВ који врши тај промет није порески дужник</td>
                <td class='numeric'><?= $params['331']; ?></td>
                <td class='grey-cell'></td>
                <td class='numeric'><?= $params['333']; ?></td>
                <td class='grey-cell'></td>
            </tr>
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('34'); } ?>
                    <div class='id-div'>3.4</div>
                </td>
                <td colspan=7>Промет за који обвезник ПДВ који врши тај промет није порески дужник, осим из тачке 3.3</td>
                <td class='numeric'><?= $params['341']; ?></td>
                <td class='grey-cell'></td>
                <td class='numeric'><?= $params['343']; ?></td>
                <td class='grey-cell'></td>
            </tr>
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('35'); } ?>
                    <div class='id-div'>3.5</div>
                </td>
                <td colspan=7>Повећање основице, односно ПДВ</td>
                <td class='numeric'><?= $params['351']; ?></td>
                <td class='numeric'><?= $params['352']; ?></td>
                <td class='numeric'><?= $params['353']; ?></td>
                <td class='numeric'><?= $params['354']; ?></td>
            </tr>
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('36'); } ?>
                    <div class='id-div'>3.6</div>
                </td>
                <td colspan=7>Смањење основице, односно ПДВ</td>
                <td class='numeric'><?= $params['361']; ?></td>
                <td class='numeric'><?= $params['362']; ?></td>
                <td class='numeric'><?= $params['363']; ?></td>
                <td class='numeric'><?= $params['364']; ?></td>
            </tr>
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('37'); } ?>
                    <div class='id-div'>3.7</div>
                </td>
                <td colspan=7>Промет добара и услуга без накнаде</td>
                <td class='numeric'><?= $params['371']; ?></td>
                <td class='numeric'><?= $params['372']; ?></td>
                <td class='numeric'><?= $params['373']; ?></td>
                <td class='numeric'><?= $params['374']; ?></td>
            </tr>
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('38'); } ?>
                    <div class='id-div'><b>3.8</b></div>
                </td>
                <td colspan=7><b>Укупна основица и обрачунати ПДВ за промет добара и услуга (3.1 + 3.2 + 3.3 + 3.4 + 3.5 + 3.6 + 3.7)</b></td>
                <td class='numeric'><?= $params['381']; ?></td>
                <td class='numeric'><?= $params['382']; ?></td>
                <td class='numeric'><?= $params['383']; ?></td>
                <td class='numeric'><?= $params['384']; ?></td>
            </tr>
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('39'); } ?>
                    <div class='id-div'>3.9</div>
                </td>
                <td colspan=7>Накнада или део накнаде који је наплаћен пре извршеног промета и ПДВ обрачунат по том основу (аванс)</td>
                <td class='numeric'><?= $params['391']; ?></td>
                <td class='numeric'><?= $params['392']; ?></td>
                <td class='numeric'><?= $params['393']; ?></td>
                <td class='numeric'><?= $params['394']; ?></td>
            </tr>
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('310'); } ?>
                    <div class='id-div'><b>3.10</b></div>
                </td>
                <td colspan=7><b>Укупно обрачунати ПДВ (3.8 + 3.9)</b></td>
                <td class='grey-cell'></td>
                <td class='numeric'><?= $params['3102']; ?></td>
                <td class='grey-cell'></td>
                <td class='numeric'><?= $params['3104']; ?></td>
            </tr>
        </table>
        <table class='table-3a'>
            <tr>
                <th colspan=5 rowspan=2>3а ОБРАЧУНАТИ ПДВ ЗА ПРОМЕТ ДРУГОГ ЛИЦА</th>
                <th class='fixed-width'>Општа стопа</th>
                <th class='fixed-width'>Посебна стопа</th>
            </tr>
            <tr>
                <th>ПДВ</th>
                <th>ПДВ</th>
            </tr>
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('3a1'); } ?>
                    <div class='id-div'>3a.1</div>
                </td>
                <td colspan=4>ПДВ за пренос права располагања на грађевинским објектима за који је порески дужник обвезник ПДВ - прималац добара</td>
                <td class='numeric'><?= $params['3a11']; ?></td>
                <td class='numeric'><?= $params['3a12']; ?></td>
            </tr>
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('3a2'); } ?>
                    <div class='id-div'>3a.2</div>
                </td>
                <td colspan=4>ПДВ за промет добара и услуга који у Републици врши страно лице које није обвезник ПДВ, за који је порески дужник обвезник ПДВ - прималац добара и услуга</td>
                <td class='numeric'><?= $params['3a21']; ?></td>
                <td class='numeric'><?= $params['3a22']; ?></td>
            </tr>
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('3a3'); } ?>
                    <div class='id-div'>3a.3</div>
                </td>
                <td colspan=4>ПДВ за промет добара и услуга за који је порески дужник обвезник ПДВ - прималац добара и услуга, осим тачке 3а.1 и 3а.2, укључујући и ПДВ обрачунат у складу са чланом 10. став 3 Закона</td>
                <td class='numeric'><?= $params['3a31']; ?></td>
                <td class='numeric'><?= $params['3a32']; ?></td>
            </tr>
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('3a4'); } ?>
                    <div class='id-div'>3a.4</div>
                </td>
                <td colspan=4>Повећање обрачунатог ПДВ</td>
                <td class='numeric'><?= $params['3a41']; ?></td>
                <td class='numeric'><?= $params['3a42']; ?></td>
            </tr>
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('3a5'); } ?>
                    <div class='id-div'>3a.5</div>
                </td>
                <td colspan=4>Смањење обрачунатог ПДВ</td>
                <td class='numeric'><?= $params['3a51']; ?></td>
                <td class='numeric'><?= $params['3a52']; ?></td>
            </tr>
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('3a6'); } ?>
                    <div class='id-div'>3a.6</div>
                </td>
                <td colspan=4>ПДВ за промет добара и услуга без накнаде</td>
                <td class='numeric'><?= $params['3a61']; ?></td>
                <td class='numeric'><?= $params['3a62']; ?></td>
            </tr>
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('3a7'); } ?>
                    <div class='id-div'><b>3a.7</b></div>
                </td>
                <td colspan=4><b>Укупно обрачунати ПДВ за промет добара и услуга  (3а.1 + 3a.2 + 3а.3 + 3а.4 + 3а.5 + 3а.6)</b></td>
                <td class='numeric'><?= $params['3a71']; ?></td>
                <td class='numeric'><?= $params['3a72']; ?></td>
            </tr>
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('3a8'); } ?>
                    <div class='id-div'>3a.8</div>
                </td>
                <td colspan=4>ПДВ по основу накнаде или дела накнаде који је плаћен пре извршеног промета (аванс)</td>
                <td class='numeric'><?= $params['3a81'];   ?></td>
                <td class='numeric'><?= $params['3a82']; ?></td>
            </tr>
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('3a9'); } ?>
                    <div class='id-div'><b>3a.9</b></div>
                </td>
                <td colspan=4><b>Укупно обрачунати ПДВ (3а.7 + 3а.8)</b></td>
                <td class='numeric'><?= $params['3a91'];   ?></td>
                <td class='numeric'><?= $params['3a92']; ?></td>
            </tr>
        </table>
        <table class='table-4'>
            <tr>
                <th colspan=6>4. ПОСЕБНИ ПОСТУПЦИ ОПОРЕЗИВАЊА</th>
            </tr>
            <tr>
                <th colspan=2>4.1 Туристичке агенције</th>
                <th colspan=2 style='width:20%'>Утврђивање основице</th>
                <th colspan=2 style='width:20%'>ПДВ</th>
            </tr>            
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('411'); } ?>
                    <div class='id-div'>4.1.1</div>
                </td>
                <td>Накнада коју плаћају путници, укључујући и повећање, односно смањење те накнаде</td>
                <td colspan=2 class='numeric'><?= $params['4111']; ?></td>
                <td colspan=2 class='grey-cell'></td>
            </tr>
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('412'); } ?>
                    <div class='id-div'>4.1.2</div>
                </td>
                <td>Стварни трошкови за претходне туристичке услуге, укључујући и повећање, односно смањење тих трошкова</td>
                <td colspan=2 class='numeric'><?= $params['4121']; ?></td>
                <td colspan=2 class='grey-cell'></td>
            </tr>
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('413'); } ?>
                    <div class='id-div'>4.1.3</div>
                </td>
                <td>Разлика (4.1.1 - 4.1.2)</td>
                <td colspan=2 class='numeric'><?= $params['4131']; ?></td>
                <td colspan=2 class='grey-cell'></td>
            </tr>
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('414'); } ?>
                    <div class='id-div'>4.1.4</div>
                </td>
                <td>Обрачунати ПДВ</td>
                <td colspan=2 class='grey-cell'></td>
                <td colspan=2 class='numeric'><?= $params['4142']; ?></td>
            </tr>
            <tr>
                <th colspan=2 rowspan=2>4.2 Половна добра, уметничка дела, колекционарска добра и антиквитети</th>
                <th colspan=2>Утврђивање основице</th>
                <th colspan=2>ПДВ</th>
            </tr>
            <tr>
                <th>Општа стопа</th>
                <th>Посебна стопа</th>
                <th>Општа стопа</th>
                <th>Посебна стопа</th>
            </tr>
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('421'); } ?>
                    <div class='id-div'>4.2.1</div>
                </td>
                <td>Продајна цена добара, укључујући и повећање, односно смањење те цене</td>
                <td class='numeric'><?= $params['4211']; ?></td>
                <td class='numeric'><?= $params['4212']; ?></td>
                <td class='grey-cell'></td>
                <td class='grey-cell'></td>
            </tr>
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('422'); } ?>
                    <div class='id-div'>4.2.2</div>
                </td>
                <td>Набавна цена добара, укључујући и повећање, односно смањење те цене</td>
                <td class='numeric'><?= $params['4221']; ?></td>
                <td class='numeric'><?= $params['4222']; ?></td>
                <td class='grey-cell'></td>
                <td class='grey-cell'></td>
            </tr>
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('423'); } ?>
                    <div class='id-div'>4.2.3</div>
                </td>
                <td>Разлика (4.2.1 - 4.2.2)</td>
                <td class='numeric'><?= $params['4231']; ?></td>
                <td class='numeric'><?= $params['4232']; ?></td>
                <td class='grey-cell'></td>
                <td class='grey-cell'></td>
            </tr>
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('424'); } ?>
                    <div class='id-div'>4.2.4</div>
                </td>
                <td>Обрачунати ПДВ</td>
                <td class='grey-cell'></td>
                <td class='grey-cell'></td>
                <td class='numeric'><?= $params['4243']; ?></td>
                <td class='numeric'><?= $params['4244']; ?></td>
            </tr>
        </table>
        <table class='table-5'>        
            <tr>
                <th colspan=2>5. УКУПАН ПРОМЕТ ДОБАРА И УСЛУГА И УКУПНО ОБРАЧУНАТИ ПДВ</th>
                <th class='fixed-width'>Износ</th>
            </tr>
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('51'); } ?>
                    <div class='id-div'>5.1</div>
                </td>
                <td><b>Укупан опорезиви промет добара и услуга по општој стопи ПДВ (3.8 + 4.1.1 + 4.2.1)</b></td>
                <td class='numeric'><?= $params['51']; ?></td>
            </tr>
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('52'); } ?>
                    <div class='id-div'>5.2</div>
                </td>
                <td><b>Укупно обрачунати ПДВ по општој стопи ПДВ (3.10 + 3а.9 + 4.1.4 + 4.2.4)</b></td>
                <td class='numeric'><?= $params['52']; ?></td>
            </tr>
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('53'); } ?>
                    <div class='id-div'>5.3</div>
                </td>
                <td><b>Укупно обрачунати ПДВ по општој стопи ПДВ увећан за износ за који се не може умањити претходни порез из тачке 8е.6 (5.2 + (8е.6 у апсолутном износу))</b></td>
                <td class='numeric'><?= $params['53']; ?></td>
            </tr>
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('54'); } ?>
                    <div class='id-div'>5.4</div>
                </td>
                <td><b>Укупан опорезиви промет добара и услуга по посебној стопи ПДВ (3.8 + 4.2.1)</b></td>
                <td class='numeric'><?= $params['54']; ?></td>
            </tr>
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('55'); } ?>
                    <div class='id-div'>5.5</div>
                </td>
                <td><b>Укупно обрачунати ПДВ по посебној стопи ПДВ (3.10 + 3а.9 + 4.2.4)</b></td>
                <td class='numeric'><?= $params['55']; ?></td>
            </tr>
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('56'); } ?>
                    <div class='id-div'>5.6</div>
                </td>
                <td><b>Укупан промет добара и услуга (1.5 + 2.5 + 5.1 + 5.4)</b></td>
                <td class='numeric'><?= $params['56']; ?></td>
            </tr>
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('57'); } ?>
                    <div class='id-div'>5.7</div>
                </td>
                <td><b>Укупно обрачунати ПДВ (5.3 + 5.5)</b></td>
                <td class='numeric'><?= $params['57']; ?></td>
            </tr>
        </table>
        <table class='table-6'>            
            <tr>
                <th colspan=8>6. УВОЗ ДОБАРА СТАВЉЕНИХ У СЛОБОДАН ПРОМЕТ У СКЛАДУ СА ЦАРИНСКИМ ПРОПИСИМА</th>
            </tr>            
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('61'); } ?>
                    <div class='id-div'>6.1</div>                    
                </td>                
                <th colspan=3>Вредност добара за чији је увоз прописано пореско ослобађање, укључујући и повећање, односно смањење вредности тих добара</th>                
                <th colspan=3 style='width:40%'><?= $params['61']; ?></th>
            </tr>            
            <tr>
                <th colspan=4>6.2 Увоз добара на који се плаћа ПДВ</th>                
                <th colspan=2>Општа стопа</th>                
                <th colspan=2>Посебна стопа</th>
            </tr>            
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('621'); } ?>
                    <div class='id-div'>6.2.1</div>
                </td>                
                <td colspan=3>Основица за увоз добара</td>                
                <td colspan=2 class='numeric'><?= $params['6211']; ?></td>                
                <td colspan=2 class='numeric'><?= $params['6212']; ?></td>
            </tr>            
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('622'); } ?>
                    <div class='id-div'>6.2.2</div>
                </td>                
                <td colspan=3>Повећање основице за увоз добара</td>                
                <td colspan=2 class='numeric'><?= $params['6221']; ?></td>                
                <td colspan=2 class='numeric'><?= $params['6222']; ?></td>
            </tr>            
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('623'); } ?>
                    <div class='id-div'>6.2.3</div>
                </td>            
                <td colspan=3>Смањење основице за увоз добара</td>                
                <td colspan=2 class='numeric'><?= $params['6231']; ?></td>                
                <td colspan=2 class='numeric'><?= $params['6232']; ?></td>
            </tr>            
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('63'); } ?>
                    <div class='id-div'><b>6.3</b></div>
                </td>                
                <td colspan=3><b>Укупна вредност, односно основица за увоз добара  (6.1 + 6.2.1 + 6.2.2 + 6.2.3)</b></td>                
                <td colspan=4 class='numeric'><?= $params['63']; ?></td>
            </tr>            
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('64'); } ?>
                    <div class='id-div'><b>6.4</b></div>
                </td>            
                <td colspan=3><b>Укупан ПДВ плаћен при увозу добара који се може одбити као претходни порез</b></td>                
                <td colspan=4 class='numeric'><?= $params['64']; ?></td>
            </tr>
        </table>
        <table class='table-7'>
            <tr>
                <th colspan=2>7. НАБАВКА ДОБАРА И УСЛУГА ОД ПОЉОПРИВРЕДНИКА</th>
                <th class='fixed-width'>Вредност добара и услуга</th>
                <th class='fixed-width'>ПДВ надокнада</th>
            </tr>
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('71'); } ?>
                    <div class='id-div'>7.1</div>
                </td>
                <td>Вредност примљених добара и услуга, укључујући и повећање, односно смањење те вредности</td>
                <td class='numeric'><?= $params['711']; ?></td>
                <td></td>
            </tr>
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('72'); } ?>
                    <div class='id-div'>7.2</div>
                </td>
                <td>Вредност плаћених добара и услуга</td>
                <td class='numeric'><?= $params['721']; ?></td>
                <td></td>
            </tr>
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('73'); } ?>
                    <div class='id-div'>7.3</div>
                </td>
                <td>Плаћена ПДВ надокнада</td>
                <td></td>
                <td class='numeric'><?= $params['732']; ?></td>
            </tr>
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('74'); } ?>
                    <div class='id-div'><b>7.4</b></div>
                </td>
                <td><b>Плаћена ПДВ надокнада која се може одбити као претходни порез</b></td>
                <td></td>
                <td class='numeric'><?= $params['742']; ?></td>
            </tr>
        </table>
        <table class='table-8'>
            <tr>
                <th colspan=6>8. НАБАВКА ДОБАРА И УСЛУГА, ОСИМ НАБАВКЕ ДОБАРА И УСЛУГА ОД ПОЉОПРИВРЕДНИКА</th>
            </tr>            
            <tr>
                <th colspan=2 rowspan=2>8а Набавка добара и услуга у Републици од обвезника ПДВ - промет за који је порески дужник испоручилац добара, односно пружилац услуга</th>
                <th colspan=2 class='fixed-width'>Општа стопа</th>
                <th colspan=2 class='fixed-width'>Посебна стопа</th>
            </tr>
            <tr>
                <th>Основица</th> <th>ПДВ</th>
                <th>Основица</th> <th>ПДВ</th>
            </tr>
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('8a1'); } ?>
                    <div class='id-div'>8a.1</div>
                </td>
                <td>Први пренос права располагања на новоизграђеним грађевинским објектима</td>
                <td class='numeric'><?= $params['8a11']; ?></td>
                <td class='numeric'><?= $params['8a12']; ?></td>
                <td class='numeric'><?= $params['8a13']; ?></td>
                <td class='numeric'><?= $params['8a14']; ?></td>
            </tr>
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('8a2'); } ?>
                    <div class='id-div'>8a.2</div>
                </td>
                <td>Добра и услуге, осим добара из тачке 8а.1</td>
                <td class='numeric'><?= $params['8a21']; ?></td>
                <td class='numeric'><?= $params['8a22']; ?></td>
                <td class='numeric'><?= $params['8a23']; ?></td>
                <td class='numeric'><?= $params['8a24']; ?></td>
            </tr>
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('8a3'); } ?>
                    <div class='id-div'>8a.3</div>
                </td>
                <td>Добра и услуге без накнаде</td>
                <td class='numeric'><?= $params['8a31']; ?></td>
                <td class='numeric'><?= $params['8a32']; ?></td>
                <td class='numeric'><?= $params['8a33']; ?></td>
                <td class='numeric'><?= $params['8a34']; ?></td>
            </tr>
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('8a4'); } ?>
                    <div class='id-div'>8a.4</div>
                </td>
                <td>Измена основице за набављена добра и услуге и исправка одбитка претходног пореза по основу измене основице - повећање</td>
                <td class='numeric'><?= $params['8a41']; ?></td>
                <td class='numeric'><?= $params['8a42']; ?></td>
                <td class='numeric'><?= $params['8a43']; ?></td>
                <td class='numeric'><?= $params['8a44']; ?></td>
            </tr>
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('8a5'); } ?>
                    <div class='id-div'>8a.5</div>
                </td>
                <td>Измена основице за набављена добра и услуге и исправка одбитка претходног пореза по основу измене основице - смањење</td>
                <td class='numeric'><?= $params['8a51']; ?></td>
                <td class='numeric'><?= $params['8a52']; ?></td>
                <td class='numeric'><?= $params['8a53']; ?></td>
                <td class='numeric'><?= $params['8a54']; ?></td>
            </tr>
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('8a6'); } ?>
                    <div class='id-div'><b>8а.6</b></div>
                </td>
                <td><b>Укупна основица за набављена добра и услуге  (8а.1 + 8а.2 + 8а3 + 8а.4 + 8а.5)</b></td>
                <td class='numeric'><?= $params['8a61']; ?></td>
                <td class='grey-cell'></td>
                <td class='numeric'><?= $params['8a63']; ?></td>
                <td class='grey-cell'></td>
            </tr>
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('8a7'); } ?>
                    <div class='id-div'>8a.7</div>
                </td>
                <td>Накнада или део накнаде који је плаћен пре извршеног промета и ПДВ по том основу (аванс)</td>
                <td class='numeric'><?= $params['8a71']; ?></td>
                <td class='numeric'><?= $params['8a72']; ?></td>
                <td class='numeric'><?= $params['8a73']; ?></td>
                <td class='numeric'><?= $params['8a74']; ?></td>
            </tr>
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('8a8'); } ?>
                    <div class='id-div'><b>8а.8</b></div>
                </td>
                <td><b>Укупно обрачунати ПДВ од стране обвезника ПДВ - претходног учесника у промету (8а.1 + 8а.2 + 8а.3 + 8а.4 + 8а.5 + 8а.7)</b></td>
                <td class='grey-cell'></td>
                <td class='numeric'><?= $params['8a82']; ?></td>
                <td class='grey-cell'></td>
                <td class='numeric'><?= $params['8a84']; ?></td>
            </tr>
            <tr>
                <th colspan=2 rowspan=2>8б Набавка добара и услуга у Републици од обвезника ПДВ - промет за који је порески дужник прималац добара, односно услуга</th>
                <th colspan=4>Основица</th>
            </tr>
            <tr>
                <th colspan=2>Општа стопа</th>
                <th colspan=2>Посебна стопа</th>
            </tr>
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('8b1'); } ?>
                    <div class='id-div'>8б.1</div>
                </td>
                <td>Пренос права располагања на грађевинским објектима</td>
                <td colspan=2 class='numeric'><?= $params['8b11']; ?></td>
                <td colspan=2 class='numeric'><?= $params['8b12']; ?></td>
            </tr>
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('8b2'); } ?>
                    <div class='id-div'>8б.2</div>
                </td>
                <td>Добра и услуге, осим из тачке 8б.1</td>
                <td colspan=2 class='numeric'><?= $params['8b21']; ?></td>
                <td colspan=2 class='numeric'><?= $params['8b22']; ?></td>
            </tr>
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('8b3'); } ?>
                    <div class='id-div'>8б.3</div>
                </td>
                <td>Добра и услуге без накнаде</td>
                <td colspan=2 class='numeric'><?= $params['8b31']; ?></td>
                <td colspan=2 class='numeric'><?= $params['8b32']; ?></td>
            </tr>
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('8b4'); } ?>
                    <div class='id-div'>8б.4</div>
                </td>
                <td>Измена основице за набављена добра и услуге - повећање</td>
                <td colspan=2 class='numeric'><?= $params['8b41']; ?></td>
                <td colspan=2 class='numeric'><?= $params['8b42']; ?></td>
            </tr>
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('8b5'); } ?>
                    <div class='id-div'>8б.5</div>
                </td>
                <td>Измена основице за набављена добра и услуге - смањење</td>
                <td colspan=2 class='numeric'><?= $params['8b51']; ?></td>
                <td colspan=2 class='numeric'><?= $params['8b52']; ?></td>
            </tr>
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('8b6'); } ?>
                    <div class='id-div'><b>8б.6</b></div>
                </td>
                <td><b>Укупна основица за набављена добра и услуге (8б.1 + 8б.2 + 8б.3 + 8б.4 + 8б.5)</b></td>
                <td colspan=2 class='numeric'><?= $params['8b61']; ?></td>
                <td colspan=2 class='numeric'><?= $params['8b62']; ?></td>
            </tr>
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('8b7'); } ?>
                    <div class='id-div'>8б.7</div>
                </td>
                <td>Накнада или део накнаде који је плаћен пре извршеног промета (аванс)</td>
                <td colspan=2 class='numeric'><?= $params['8b71']; ?></td>
                <td colspan=2 class='numeric'><?= $params['8b72']; ?></td>
            </tr>
            <tr>
                <th colspan=2>8в Набавка добара и услуга у Републици од обвезника ПДВ, осим по основу промета за који постоји обавеза обрачунавања ПДВ из тачке 8а и 8б</th>
                <th colspan=4>Накнада / Вредност</th>
            </tr>
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('8v1'); } ?>
                    <div class='id-div'>8в.1</div>
                </td>
                <td>Пренос целокупне, односно дела имовине у складу са чланом 6. став 1. тачка 1. Закона, са или без накнаде или као улог, укључујући и повећање, односно смањење накнаде за тај пренос</td>
                <td colspan=4 class='numeric'><?= $params['8v1']; ?></td>
            </tr>
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('8v2'); } ?>
                    <div class='id-div'>8в.2</div>
                </td>
                <td>Добра и услуге уз накнаду, осим из тачке 8в.1, укључујући и повећање, односно смањење те накнаде</td>
                <td colspan=4 class='numeric'><?= $params['8v2']; ?></td>
            </tr>
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('8v3'); } ?>
                    <div class='id-div'>8в.3</div>
                </td>
                <td>Добра и услуге без накнаде, осим из тачке 8в.1</td>
                <td colspan=4 class='numeric'><?= $params['8v3']; ?></td>
            </tr>
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('8v4'); } ?>
                    <div class='id-div'><b>8в.4</b></div>
                </td>
                <td><b>Укупна накнада, односно вредност набављених добара и услуга (8в.1 + 8в.2 + 8в.3)</b></td>
                <td colspan=4 class='numeric'><?= $params['8v4']; ?></td>
            </tr>
            <tr>
                <th colspan=2 rowspan=2>8г Набавка добара и услуга у Републици од страних лица која нису обвезници ПДВ - промет за који постоји обавеза обрачунавања ПДВ</th>
                <th colspan=4>Основица</th>
            </tr>
            <tr>
                <th colspan=2>Општа стопа</th>
                <th colspan=4>Посебна стопа</th>
            </tr>
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('8g1'); } ?>
                    <div class='id-div'>8г.1</div>
                </td>
                <td>Добра и услуге</td>
                <td colspan=2 class='numeric'><?= $params['8g11']; ?></td>
                <td colspan=2 class='numeric'><?= $params['8g12']; ?></td>
            </tr>
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('8g2'); } ?>
                    <div class='id-div'>8г.2</div>
                </td>
                <td>Добра и услуге без накнаде</td>
                <td colspan=2 class='numeric'><?= $params['8g21']; ?></td>
                <td colspan=2 class='numeric'><?= $params['8g22']; ?></td>
            </tr>
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('8g3'); } ?>
                    <div class='id-div'>8г.3</div>
                </td>
                <td>Измена основице - повећање</td>
                <td colspan=2 class='numeric'><?= $params['8g31']; ?></td>
                <td colspan=2 class='numeric'><?= $params['8g32']; ?></td>
            </tr>
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('8g4'); } ?>
                    <div class='id-div'>8г.4</div>
                </td>
                <td>Измена основице - смањење</td>
                <td colspan=2 class='numeric'><?= $params['8g41']; ?></td>
                <td colspan=2 class='numeric'><?= $params['8g42']; ?></td>
            </tr>
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('8g5'); } ?>
                    <div class='id-div'><b>8г.5</b></div>
                </td>
                <td><b>Укупна основица за набављена добра и услуге (8г.1 + 8г.2 + 8г.3 + 8г.4)</b></td>
                <td colspan=2 class='numeric'><?= $params['8g51']; ?></td>
                <td colspan=2 class='numeric'><?= $params['8g52']; ?></td>
            </tr>
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('8g6'); } ?>
                    <div class='id-div'>8г.6</div>
                </td>
                <td>Накнада или део накнаде плаћен пре извршеног промета (аванс)</td>
                <td colspan=2 class='numeric'><?= $params['8g61']; ?></td>
                <td colspan=2 class='numeric'><?= $params['8g62']; ?></td>
            </tr>
            <tr>
                <th colspan=4>8д Набавка добара и услуга, осим из тачака 8а до 8г</th>
                <th colspan=2>Накнада / Вредност</th>
            </tr>
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('8d1'); } ?>
                    <div class='id-div'>8д.1</div>
                </td>
                <td colspan=3>Добра и услуге набављени у Републици од страних лица која нису обвезници ПДВ - промет за који не постоји обавеза обрачунавања ПДВ, као и повећање, односно смањење накнаде за та добра и услугге, укључујући и набавку без накнаде</td>
                <td colspan=3 class='numeric'><?= $params['8d1']; ?></td>
            </tr>
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('8d2'); } ?>
                    <div class='id-div'>8д.2</div>
                </td>
                <td colspan=3>Добра и услуге набављени у Републици од лица са територије Републике која нису обвезници ПДВ, као и повећање, односно смањење накнаде за та добра и услуге, укључујући инабавку без накнаде</td>
                <td colspan=3 class='numeric'><?= $params['8d2']; ?></td>
            </tr>
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('8d3'); } ?>
                    <div class='id-div'>8д.3</div>
                </td>
                <td colspan=3>Добра и услуге набављени ван Републике, као и повећање, односно смањење накнаде за та добра и услуге, укључујући и набавку без накнаде</td>
                <td colspan=3 class='numeric'><?= $params['8d3']; ?></td>
            </tr>            
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('8dj'); } ?>
                    <div class='id-div'><b>8ђ</b></div>
                </td>
                <td colspan=3><b>Укупна основица, накнада, односно вредност набављених добара и услуга  (8а.6 + 8б.6 + 8в.4 + 8г.5 + 8д.1 + 8д.2 + 8д.3)</b></td>
                <td colspan=3 class='numeric'><?= $params['8dj']; ?></td>
            </tr>
            <tr>
                <th colspan=4>8е ПДВ за промет добара и услуга који се може одбити као претходни порез и исправке одбитка претходног пореза</th>
                <th colspan=2>Износ</th>
            </tr>
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('8e1'); } ?>
                    <div class='id-div'>8e.1</div>
                </td>
                <td colspan=3>Укупно обрачунати ПДВ за промет набављених добара и услуга за који је порески дужник обвезник ПДВ - испоручилац добара, односно пружалац услуга а који се може одбити као претходни порез (8а.8 умањен за износ ПДВ који се не може одбити као претходни порез)</td>
                <td colspan=3 class='numeric'><?= $params['8e1']; ?></td>
            </tr>
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('8e2'); } ?>
                    <div class='id-div'>8e.2</div>
                </td>
                <td colspan=3>Укупно обрачунати ПДВ за промет набављених добара и услуга за који је порески дужник обвезник ПДВ - прималац добара, односно услуга, а који се може одбити као претходни порез (3а.9 умањен за износ ПДВ који се не може одбити као претходни порез)</td>
                <td colspan=3 class='numeric'><?= $params['8e2']; ?></td>
            </tr>
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('8e3'); } ?>
                    <div class='id-div'>8e.3</div>
                </td>
                <td colspan=3>Исправка одбитка - повећање претходног пореза , осим по основу измене основице за промет добара и услуга и по основу увоза добара</td>
                <td colspan=3 class='numeric'><?= $params['8e3']; ?></td>
            </tr>
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('8e4'); } ?>
                    <div class='id-div'>8e.4</div>
                </td>
                <td colspan=3>Исправка одбитка - смањење претходног пореза, осим по основу измене основице за промет добара и услуга</td>
                <td colspan=3 class='numeric'><?= $params['8e4']; ?></td>
            </tr>
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('8e5'); } ?>
                    <div class='id-div'><b>8е.5</b></div>
                </td>
                <td colspan=3><b>Укупно обрачунати ПДВ за промет добара и услуга који се може одбити као претходни порез (8е.1 + 8е.2 + 8е.3 + 8е.4)</b></td>
                <td colspan=3 class='numeric'><?= $params['8e5']; ?></td>
            </tr>
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('8e6'); } ?>
                    <div class='id-div'><b>8е.6</b></div>
                </td>
                <td colspan=3><b>Укупно обрачунати ПДВ за промет добара и услуга који се може одбити као претходни порез увећан за износ за који се не може умањити обрачунати ПДВ (8е.5 + (5.2 + 5.5 у апсолутном износу))</b></td>
                <td colspan=3 class='numeric'><?= $params['8e6']; ?></td>
            </tr>
        </table>
        <table class='table-9'>
            <tr>
                <th colspan=2>9. УКУПНА ВРЕДНОСТ НАБАВЉЕНИХ ДОБАРА И УСЛУГА, УКЉУЧУЈУЋИ И УВОЗ ДОБАРА СТАВЉЕНИХ У СЛОБОДАН ПРОМЕТ (6.3 + 7.1 + 8ђ)</th>
                <th class='fixed-width'><?= $params['9']; ?></th>
            </tr>
        </table>
        <table class='table-9a'>
            <tr>
                <th colspan=2>9a ПДВ који се у пореској пријави исказује као претходни порез</th>
                <th class='fixed-width'>Износ</th>
            </tr>
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('9a1'); } ?>
                    <div class='id-div'><b>9a.1</b></div>
                </td>
                <td><b>ПДВ плаћен при увозу добара</b></td>
                <td class='numeric'><?= $params['9a1']; ?></td>
            </tr>
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('9a2'); } ?>
                    <div class='id-div'><b>9a.2</b></div>
                </td>
                <td><b>ПДВ надокнада плаћена пољопривреднику</b></td>
                <td class='numeric'><?= $params['9a2']; ?></td>
            </tr>
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('9a3'); } ?>
                    <div class='id-div'><b>9a.3</b></div>
                </td>
                <td><b>ПДВ по основу набавки добара и услуга, осим из тачака 9а.1 и 9а.2</b></td>
                <td class='numeric'><?= $params['9a3']; ?></td>
            </tr>
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('9a4'); } ?>
                    <div class='id-div'><b>9a.4</b></div>
                </td>
                <td><b>Укупан ПДВ који се у пореској пријави исказује као претходи порез (9а.1 + 9а.2 + 9а.3)</b></td>
                <td class='numeric'><?= $params['9a4']; ?></td>
            </tr>
        </table>
        <table class='table-10'>
            <tr>
                <th colspan=4>10. ПОРЕСКА ОБАВЕЗА (5.7 - 9а.4)</th>
                <th class='fixed-width'><?= $params['10']; ?></th>
            </tr>
        </table>
        <table class='table-11'>
            <tr>
                <th colspan=2>11. ПРОМЕТ ДОБАРА И УСЛУГА ИЗВРШЕН ВАН РЕПУБЛИКЕ И ДРУГИ ПРОМЕТ КОЈИ НЕ ПОДЛЕЖЕ ПДВ</th>
                <th class='fixed-width'>Износ</th>
            </tr>
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('111'); } ?>
                    <div class='id-div'>11.1</div>
                </td>
                <td>Промет добара и услуга извршен ван Републике</td>
                <td class='numeric'><?= $params['111']; ?></td>
            </tr>
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('112'); } ?>
                    <div class='id-div'>11.2</div>
                </td>
                <td>Пренос целокупне, односно дела имовине у складу са чланом 6. став 1. тачка 1. Закона</td>
                <td class='numeric'><?= $params['112']; ?></td>
            </tr>
            <tr>
                <td class="<?= $questionmark_td; ?>">
                    <?php if ($with_questionmark) { POPDVExport::renderQuestionmark('113'); } ?>
                    <div class='id-div'>11.3</div>
                </td>
                <td>Промет добара и услуга из члана 6. Закона, осим из тачке 11.2</td>
                <td class='numeric'><?= $params['113']; ?></td>
            </tr>
        </table>
        <footer>
            Напомена: У поља 11.1 до 11.3 уносе се и износи по основу наплаћене накнаде, односно дела накнаде пре извршеног промета, односно преноса (аванс).
        </footer>
    </body>
</html>