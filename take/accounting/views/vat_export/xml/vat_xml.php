<ns1:EPPPDV xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:ns1='urn:poreskauprava.gov.rs/zim'>
    
  <signatures />
  
  <envelopa id="" timestamp="<?= $xml_info_params['timestamp_of_application'] ?>" nacinPodnosenja="elektronski">
      
    <sadrzaj>
        <OJ/>
        <PIB><?= $xml_info_params['PIB'] ?></PIB>
        <Firma><?= $xml_info_params['company'] ?></Firma>
        <Opstina><?= $xml_info_params['Opstina'] ?></Opstina>
        <Adresa><?= $xml_info_params['address'] ?></Adresa>
        <Od_Datum><?= $xml_info_params['date_from'] ?></Od_Datum>
        <Do_Datum><?= $xml_info_params['date_to'] ?></Do_Datum>
        <ElektronskaPosta><?= $xml_info_params['e_mail'] ?></ElektronskaPosta>
        <Mesto><?= $xml_info_params['city'] ?></Mesto>
        <Datum_Prijave><?= $xml_info_params['date_of_application'] ?></Datum_Prijave>
        <OdgovornoLice><?= $xml_info_params['responsible_person'] ?></OdgovornoLice>

        <Iznos_001><?= $xml_pppdv_params['001'] ?></Iznos_001>
        <Iznos_002><?= $xml_pppdv_params['002'] ?></Iznos_002>
        <Iznos_003><?= $xml_pppdv_params['003'] ?></Iznos_003>
        <Iznos_103><?= $xml_pppdv_params['103'] ?></Iznos_103>
        <Iznos_004><?= $xml_pppdv_params['004'] ?></Iznos_004>
        <Iznos_104><?= $xml_pppdv_params['104'] ?></Iznos_104>
        <Iznos_005><?= $xml_pppdv_params['005'] ?></Iznos_005>
        <Iznos_105><?= $xml_pppdv_params['105'] ?></Iznos_105>
        <Iznos_006><?= $xml_pppdv_params['006'] ?></Iznos_006>
        <Iznos_106><?= $xml_pppdv_params['106'] ?></Iznos_106>
        <Iznos_007><?= $xml_pppdv_params['007'] ?></Iznos_007>
        <Iznos_107><?= $xml_pppdv_params['107'] ?></Iznos_107>
        <Iznos_008><?= $xml_pppdv_params['008'] ?></Iznos_008>
        <Iznos_108><?= $xml_pppdv_params['108'] ?></Iznos_108>
        <Iznos_009><?= $xml_pppdv_params['009'] ?></Iznos_009>
        <Iznos_109><?= $xml_pppdv_params['109'] ?></Iznos_109>
        <Iznos_110><?= $xml_pppdv_params['110'] ?></Iznos_110>

        <?php if (!is_null($xml_pppdv_params['vat_return'])) {
            if ($xml_pppdv_params['vat_return']) {
                echo '<PovracajDA>1</PovracajDA>';
            }else{
                echo '<PovracajNE>1</PovracajNE>';
            }  
        }?>
        
        <PeriodPOB><?= $xml_info_params['tip_perioda'] ?></PeriodPOB>
        <TipPodnosioca><?= $xml_info_params['tip_podnosioca'] ?></TipPodnosioca>
        <IzmenaPrijave>0</IzmenaPrijave>
        <IdentifikacioniBrojPrijaveKojaSeMenja>0</IdentifikacioniBrojPrijaveKojaSeMenja>
      
    <?php if (false) {//MilosS: namerno je stavljen false jer ovo nije implementirano, a da ne bi brisao?>
        <prilogKMPDV>
            <OrgJed_PU/>
            <Od_Datum><?= $xml_info_params['date_from'] ?></Od_Datum>
            <Do_Datum><?= $xml_info_params['date_to'] ?></Do_Datum>
            <PIB><?= $xml_info_params['PIB'] ?></PIB>
            <Firma><?= $xml_info_params['company'] ?></Firma>
            <Adresa><?= $xml_info_params['address'] ?></Adresa>
            <Iznos_KMPDV_201><?= $xml_pppdv_params['201'] ?></Iznos_KMPDV_201>
            <Iznos_KMPDV_206><?= $xml_pppdv_params['206'] ?></Iznos_KMPDV_206>
            <Iznos_KMPDV_306><?= $xml_pppdv_params['306'] ?></Iznos_KMPDV_306>
            <Mesto><?= $xml_info_params['city'] ?></Mesto>
            <Datum><?= $xml_info_params['date_of_application'] ?></Datum>
            <OdgovornoLice><?= $xml_info_params['responsible_person'] ?></OdgovornoLice>
        </prilogKMPDV>
    <?php }?>  
    </sadrzaj>
    
    
    <obracuni>
        
    <?php foreach ($xml_popdv_params as $segment_code => $segment){?>
        <POPDV<?=$segment_code?>>
            <?php foreach ($segment as $_param_code => $param){ ?>
    <Iznos_<?=$_param_code?>><?=$param?></Iznos_<?=$_param_code?>>
            <?php } ?>
</POPDV<?=$segment_code?>>
    <?php }?>
        
    </obracuni>
    
  </envelopa>
  
</ns1:EPPPDV>