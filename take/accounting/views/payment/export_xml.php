<pmtorderrq>
<?php 
    foreach($payments as $payment) { ?>
    <pmtorder>
            <companyinfo><name><?php echo $company_name; ?></name><city><?php echo $company_address; ?></city></companyinfo>
            <accountinfo><acctid><?php echo $account_number; ?></acctid><bankid /><bankname><?php echo $bank_name; ?></bankname></accountinfo>
            <payeecompanyinfo>
                <name><?php echo $payment->partner->DisplayName; ?></name>
                <city><?php if (isset($payment->partner->MainAddress)){echo $payment->partner->MainAddress->DisplayName; };?></city>
            </payeecompanyinfo>
            <payeeaccountinfo><acctid><?php echo $payment->account->number; ?></acctid><bankid /><bankname /></payeeaccountinfo>
            <trnamt><?php echo SIMAHtml::payment_export_number_format($payment->amount); ?></trnamt>
            <purpose><?php echo $payment->payment_code->name; ?></purpose>
            <purposecode><?php echo $payment->payment_code->code; ?></purposecode>
            <curdef>RSD</curdef>
            <refnumber><?php echo $payment->call_for_number; ?></refnumber>
            <payeerefnumber><?php echo $payment->call_for_number_partner; ?></payeerefnumber>
            <payeerefmodel><?php echo $payment->call_for_number_modul_partner; ?></payeerefmodel>
    </pmtorder>
<?php } ?>
</pmtorderrq>