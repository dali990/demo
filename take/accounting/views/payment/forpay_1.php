<?php 
    if (Yii::app()->request->isAjaxRequest){ Yii::app()->clientScript->clearScriptFiles(); }
    
    $uniq = SIMAHtml::uniqid();
    
    $table_id = 'paymenttable'.$uniq;
    $div_id = 'paymentdiv'.$uniq;
?>

<div id='<?php echo $div_id; ?>' class="sima-layout-panel">
    <?php
        $this->widget('base.extensions.SIMAGuiTable.SIMAGuiTable', [
            'id'=>$table_id,
            'model'=> Payment::model(),            
            'fixed_filter'=>array(
                'filter_scopes'=>'for_pay',
            ),
            'custom_buttons' => [
                Yii::t('AccountingModule.Common','Izvezi za E-banking') => [
                    'func' => 'sima.accounting.paymentsExportForPay(\''.$uniq.'\', \''.(Yii::app()->company->id).'\');'
                ]
            ]
        ]);
    ?>
</div>

<?php if (Yii::app()->request->isAjaxRequest){ Yii::app()->clientScript->clearScriptFiles(); }