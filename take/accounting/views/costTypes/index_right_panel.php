<div class="sima-layout-panel _vertical _splitter">
    
    <div class="sima-layout-panel">
        <?php $this->widget('SIMAGuiTable',[
            'id' => $left_table_id,
            'model' => CostTypeToAccount::class,
            'title' => $left_title,
            'fixed_filter' => [
                'account' => [
                    'year' => ['ids' => $default_year->id]
                ],
                'cost_type' => [
                    'cost_or_income' => $cost_or_income
                ]
            ],
            'custom_buttons' => $custom_buttons1,
            'add_button' => $add_button
//            'id' => $left_table_id,
//            'model' => CostType::class,
//            'title' => $left_title,
//            'columns_type' => 'in_year',
//            'fixed_filter' => [
//                'scopes' => [
//                    'inYear' => [$default_year->id]
//                ],
//                'cost_or_income' => $cost_or_income
//            ],
//            'custom_buttons' => [
//                $transfer_all_button_title => [
//                    'title'=>$transfer_all_button_title,
//                    'func'=>"sima.accounting.transferCostTypesFromPreviousYear('$right_table_id','$left_table_id',$prev_year->id,$default_year->id)",
//                ]
//            ],
//            'add_button' => [
//                'init_data' => [
//                    'CostType' => [
//                        'cost_or_income' => $cost_or_income,
//                        'year' => ['ids' => $default_year->id]
//                    ]
//                ]
//            ]
        ])?>
    </div>
    
    <div class="sima-layout-panel">
        <?php $this->widget('SIMAGuiTable',[
            'id' => $right_table_id,
            'model' => CostType::class,
            'title' => $right_title,
            'columns_type' => 'not_in_year',
            'fixed_filter' => [
                'scopes' => [
                    'notInYear' => [$default_year->id]
                ],
                'cost_or_income' => $cost_or_income
            ],
            'custom_buttons' => $custom_buttons2
        ])?>
    </div>
    
</div>
