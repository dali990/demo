<div class="sima-layout-panel _vertical _splitter">
    
    <div class="sima-layout-panel" data-sima-layout-init='{"min_width": 90, "max_width":90}'>
        <?php
            $this->widget('SIMATree2', [
                'id' => $uniq.'_left-panel',
                'class'=>'sima-vm',
                'list_tree_data'=>$menu_array,
                'default_selected'=>$default_year->id,
                'disable_unselect_item'=>true
            ]);
        ?>
    </div>
    <div id="<?=$uniq?>_right_panel" class="sima-layout-panel">

    </div>
    
</div>

<script type="text/javascript">
    
    $('#<?=$uniq?>_left-panel').on('selectItem',function(event, extra){
        var selected_year_id = extra.data('code');
        sima.ajax.get('accounting/costTypes/indexRightPanel',{
            get_params: {
                year_id: selected_year_id,
                cost_or_income: '<?=$cost_or_income?>'
            },
            success_function: function(response){
                sima.set_html($('#<?=$uniq?>_right_panel'),response.html,'TRIGGER_cost_type_set_right_panel');
            }
        });
    });
    
</script>