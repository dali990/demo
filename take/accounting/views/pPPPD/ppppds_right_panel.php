<?php
$this->widget('SIMAGuiTable', [
    'id' => $ppppds_table_id,
    'model' => PPPPD::model(),
    'fixed_filter' => [
        'year' => [
            'ids' => $year_id
        ]
    ],
    'add_button' => [
        'formName' => 'user_upload',
        'init_data' => [
            'PPPPD' => [
                'creation_type' => PPPPD::$CREATE_TYPE_USER_UPLOAD,
                'year_id' => $year_id
            ]
        ]
    ],
]);