<?php
$vmenu_id = $uniq.'_left-panel';

$this->widget('SIMAVMenu', [
    'id' => $vmenu_id,
    'menu_array' => $menu_array
]);
?>

<script type='text/javascript'>
    $(function() {
        $("#<?php echo $vmenu_id; ?>").vmenu('selectItem',$('#<?php echo $uniq.'_left-panel' ?>').find("li[data-id='" + <?php echo $year_id; ?> + "']"));
        $("#<?php echo $vmenu_id; ?>").on('selectItem', function(event, item){            
            var year_id = item.data('id');
            var ppppds_table = $('#'+'<?=$ppppds_table_id;?>');

            ppppds_table.simaGuiTable('setFixedFilter',{
                year: {
                    ids: year_id
                }
            }).simaGuiTable('setAddButton',{
                formName: 'user_upload',
                init_data:{
                    PPPPD: {
                        creation_type: '<?php echo PPPPD::$CREATE_TYPE_USER_UPLOAD; ?>',
                        year_id: year_id
                    }
                }
            }).simaGuiTable('populate');
        });
    });
</script>