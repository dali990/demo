<?php
$uniq = SIMAHtml::uniqid();
$sima_generated_span_id = $uniq.'ssi';
$user_uploaded_span_id = $uniq.'usi';
?>

<div class="sima-layout-panel _horizontal">
    <div class="sima-layout-fixed-panel">
        <?php
            $this->widget('SIMAButton', [
                'action' => [
                    'title' => Yii::t('AccountingModule.PPPPD', 'PerformComparePPPPDs'),
                    'onclick' => [
                        'sima.ppppd.performComparePPPPDs',
                        "$sima_generated_span_id", "$user_uploaded_span_id"
                    ]
                ]
            ]);
        ?>
    </div>
    <div class="sima-layout-fixed-panel">
        <span>Izabran generisan u sima-i</span>: <span id="<?=$sima_generated_span_id?>">Nema</span>
            <?php
               $this->widget('SIMAButton', [
                    'action' => [
                        'title' => Yii::t('AccountingModule.PPPPD', 'ChoosePPPPDForCompare'),
                        'onclick' => [
                            'sima.ppppd.choosePPPPDForCompare',
                            "$sima_generated_span_id", PPPPD::$CREATE_TYPE_SIMA_GENERATE
                        ]
                    ]
                ]); 
            ?>
    </div>
    <div class="sima-layout-fixed-panel">
        <span>Izabran uvezen od korisnika</span>: <span id="<?=$user_uploaded_span_id?>">Nema</span>
            <?php
               $this->widget('SIMAButton', [
                    'action' => [
                        'title' => Yii::t('AccountingModule.PPPPD', 'ChoosePPPPDForCompare'),
                        'onclick' => [
                            'sima.ppppd.choosePPPPDForCompare',
                            "$user_uploaded_span_id", PPPPD::$CREATE_TYPE_USER_UPLOAD
                        ]
                    ]
                ]); 
            ?>
    </div>
</div>
