<div class='sima-layout-panel _horizontal'>

    <div class='sima-layout-fixed-panel'>

        <table style="margin-bottom: 10px; float: left">
            <tr>
                <td>Ulazni PDV</td>
                <td style="text-align: right; width:150px;"><?=$KPR_vat_total?></td>
                <td><?php 
                if(SIMAMisc::isVueComponentEnabled())
                {
                    Yii::app()->controller->widget(SIMAButtonVue::class, [
                        'title'=>Yii::t('AccountingModule.Reports', 'Pdf'),
                        'onclick'=>['sima.accounting.kprKirReport', $month, $year, KPR::class, 'pdf']
                    ]);
                }
                else
                {
                    Yii::app()->controller->widget('SIMAButton', [
                        'action'=>[ 
                            'title'=>Yii::t('AccountingModule.Reports', 'Pdf'),
                            'onclick'=>['sima.accounting.kprKirReport', $month, $year, KPR::class, 'pdf']
                        ]
                    ]);
                }
                ?></td>
                <td><?php 
                if(SIMAMisc::isVueComponentEnabled())
                {
                    Yii::app()->controller->widget(SIMAButtonVue::class, [
                        'title'=>Yii::t('AccountingModule.Reports', 'ManualPdf'),
                        'onclick'=>['sima.accounting.kprKirReport', $month, $year, KPRManual::class, 'pdf']
                    ]);
                }
                else
                {
                    Yii::app()->controller->widget('SIMAButton', [
                        'action'=>[ 
                            'title'=>Yii::t('AccountingModule.Reports', 'ManualPdf'),
                            'onclick'=>['sima.accounting.kprKirReport', $month, $year, KPRManual::class, 'pdf']
                        ]
                    ]);
                }
                ?></td>
                <td><?php 
                if(SIMAMisc::isVueComponentEnabled())
                {
                    Yii::app()->controller->widget(SIMAButtonVue::class, [
                        'title'=>Yii::t('AccountingModule.Reports', 'Csv'),
                        'onclick'=>['sima.accounting.kprKirReport', $month, $year, KPR::class, 'csv']
                    ]);
                }
                else
                {
                    Yii::app()->controller->widget('SIMAButton', [
                        'action'=>[ 
                            'title'=>Yii::t('AccountingModule.Reports', 'Csv'),
                            'onclick'=>['sima.accounting.kprKirReport', $month, $year, KPR::class, 'csv']
                        ]
                    ]);
                }
                ?></td>
            </tr>
            <tr>
                <td>Izlazni PDV</td>
                <td style="text-align: right; width:150px;"><?=$KIR_vat_total?></td>
                <td><?php 
                if(SIMAMisc::isVueComponentEnabled())
                {
                    Yii::app()->controller->widget(SIMAButtonVue::class, [
                        'title'=>Yii::t('AccountingModule.Reports', 'Pdf'),
                        'onclick'=>['sima.accounting.kprKirReport', $month, $year, KIR::class, 'pdf']
                    ]);
                }
                else
                {
                    Yii::app()->controller->widget('SIMAButton', [
                        'action'=>[ 
                            'title'=>Yii::t('AccountingModule.Reports', 'Pdf'),
                            'onclick'=>['sima.accounting.kprKirReport', $month, $year, KIR::class, 'pdf']
                        ]
                    ]);
                }
                ?></td>
                <td><?php 
                if(SIMAMisc::isVueComponentEnabled())
                {
                    Yii::app()->controller->widget(SIMAButtonVue::class, [
                        'title'=>Yii::t('AccountingModule.Reports', 'ManualPdf'),
                        'onclick'=>['sima.accounting.kprKirReport', $month, $year, KIRManual::class, 'pdf']
                    ]);
                }
                else
                {
                    Yii::app()->controller->widget('SIMAButton', [
                        'action'=>[ 
                            'title'=>Yii::t('AccountingModule.Reports', 'ManualPdf'),
                            'onclick'=>['sima.accounting.kprKirReport', $month, $year, KIRManual::class, 'pdf']
                        ]
                    ]);
                }
                ?></td>
                <td><?php 
                if(SIMAMisc::isVueComponentEnabled())
                {
                    Yii::app()->controller->widget(SIMAButtonVue::class, [
                        'title'=>Yii::t('AccountingModule.Reports', 'Csv'),
                        'tooltip'=>Yii::t('AccountingModule.Reports', 'Csv'),
                        'onclick'=>['sima.accounting.kprKirReport', $month, $year, KIR::class, 'csv']
                    ]);
                }
                else
                {
                    Yii::app()->controller->widget('SIMAButton', [
                        'action'=>[ 
                            'title'=>Yii::t('AccountingModule.Reports', 'Csv'),
                            'tooltip'=>Yii::t('AccountingModule.Reports', 'Csv'),
                            'onclick'=>['sima.accounting.kprKirReport', $month, $year, KIR::class, 'csv']
                        ]
                    ]);
                }
                ?></td>
            </tr>

        </table>

        <div style="margin-left: 10px; margin-bottom: 10px; float: left;">
            <?php
            if(SIMAMisc::isVueComponentEnabled())
            {
                Yii::app()->controller->widget(SIMAButtonVue::class, [
                    'title'=>'Izaberi nalog za knjizenje',
                    'onclick'=>['sima.accounting.chooseADforVAT', $month_id, $year_id]
                ]);
            }
            else
            {
                Yii::app()->controller->widget('SIMAButton', [
                    'action'=>[ 
                        'title'=>'Izaberi nalog za knjizenje',
                        'onclick'=>['sima.accounting.chooseADforVAT', $month_id, $year_id]
                    ]
                ]);
            }
            ?>
        </div>

        <div style="margin-left: 10px; margin-bottom: 10px; float: left;">
            <?php
            if(SIMAMisc::isVueComponentEnabled())
            {
                Yii::app()->controller->widget(SIMAButtonVue::class, [
                    'title'=>'POPDV - PDF',
                    'onclick'=>['sima.accountingMisc.exportVat', $month_id, POPDVExport::PDF, false]
                ]);
                Yii::app()->controller->widget(SIMAButtonVue::class, [
                    'title'=>'POPDV -XML',
                    'onclick'=>['sima.accountingMisc.exportVat', $month_id, POPDVExport::XML, false]
                ]);
            }
            else
            {
                Yii::app()->controller->widget('SIMAButton', [
                    'action'=>[ 
                        'title'=>'POPDV - PDF',
                        'onclick'=>['sima.accountingMisc.exportVat', $month_id, POPDVExport::PDF, false]
                    ]
                ]);
                Yii::app()->controller->widget('SIMAButton', [
                    'action'=>[ 
                        'title'=>'POPDV -XML',
                        'onclick'=>['sima.accountingMisc.exportVat', $month_id, POPDVExport::XML, false]
                    ]
                ]);
            }
            ?>
        </div>
                
        <div style="margin-left: 10px; margin-bottom: 10px; float: left;">
            <?php
            $pppdv_btn_params = [
                'title'=>'PPPDV - PDF',
                'onclick'=>['sima.report.downloadPDF', 'PPPDVReport', ['month_id'=> $month_id]]
            ];
            if(SIMAMisc::isVueComponentEnabled())
            {
                Yii::app()->controller->widget(SIMAButtonVue::class, $pppdv_btn_params);
            }
            else
            {
                Yii::app()->controller->widget('SIMAButton', [
                    'action'=> $pppdv_btn_params
                ]);
            }
            ?>
        </div>
        <div style="margin-left: 10px; margin-bottom: 10px; float: left; font-size: large;">
            <?=Yii::t('AccountingModule.AccountingMonth','CheckProblemsOverview')?>
        </div>
    </div>

    <div class="sima-layout-panel">
        <?php $this->widget('SIMATabs',[
            'list_tabs_model' => $a_month,
            'ignore_tabs' => [
                'unreleased_advances',
                'bills_not_booked',
                'bills_bad_vat_booked',
                'vat_kpr_not_found',
                'vat_kir_not_found',
            ]
    //        'default_selected' => 'kir' //ostavljamo zakomentarisano da se ne bi ucitavalo jer dugo traje
        ]);?>
    </div>
    
</div>
