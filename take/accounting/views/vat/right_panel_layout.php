<div id="<?=$right_panel_id?>" class="sima-layout-panel"></div>

<script>

$('#<?php echo $vertical_menu_id; ?>').on('selectItem', function(event, item){
        var type = item.data('type');
        var id = item.data('id');
        sima.ajax.get('accounting/vat/rightPanel', {
            get_params: {
                type: type,
                id: id
            },
            success_function: function(response) {
                sima.set_html($("#<?=$right_panel_id?>"),response.html,'TRIGGER_right_panel_layout');
            }
        });
    });

</script>