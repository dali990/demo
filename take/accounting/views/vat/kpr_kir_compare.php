<div class="sima-layout-panel _splitter _horizontal">
    
    <div class="sima-layout-panel">
        <?php $this->widget('SIMAGuiTable', $SIMAandManual); ?>
    </div>
    
    <div class="sima-layout-panel">
        <div class="sima-layout-panel _splitter _vertical">
            
            <div class="sima-layout-panel">
                <?php $this->widget('SIMAGuiTable', $ManualNotSIMA); ?>
            </div>
            
            <div class="sima-layout-panel">
                <?php $this->widget('SIMAGuiTable', $SIMANotManual); ?>
            </div>
            
        </div>
    </div>
    

</div>