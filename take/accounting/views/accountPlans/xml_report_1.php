<FiForma xmlns="http://schemas.datacontract.org/2004/07/Domain.Model" xmlns:i="http://www.w3.org/2001/XMLSchema-instance">
  <Naziv><?php echo $report_name; ?></Naziv>
  <NumerickaPoljaForme xmlns:a="http://schemas.datacontract.org/2004/07/AppDef">
    <?php 
        foreach($xml_values as $key => $value) 
        {
            $new_value = $value;
            if (!in_array($key, $aop_codes_with_original_value)) 
            {
                $new_value = abs(round($value / 1000));
            }
    ?>
    <a:NumerickoPolje>
      <a:Naziv><?php echo $key; ?></a:Naziv>
      <a:Vrednosti><?php echo $new_value; ?></a:Vrednosti>
    </a:NumerickoPolje>
    <?php } ?>
  </NumerickaPoljaForme>
  <TekstualnaPoljaForme/>
</FiForma>