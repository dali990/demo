
<?php
    $this->widget('SIMAGuiTable', [
        'id' => $table_id,
        'class' => '_low_profile_rows',
        'model' => AccountTransaction::model(),                
        'columns_type' => $columns_type,
        'add_button' => $add_button,
        'fixed_filter' => [
            'account_document' => [
                'account_order' => [
                    'ids' => $model->id
                ]
            ],
            //ovo je bilo samo da bi suzilo pretragu u gui table
            //ali se sad koristi druga pretraga, pa potencijalno vise ne treba
            //filterAccount -> textFilterAccount
//            'account' => array(
//                'year' => [
//                    'ids' => $model->year_id
//                ]
//            ),
//            'account_debit' => array(
//                'year' => [
//                    'ids' => $model->year_id
//                ]
//            ),
//            'account_credit' => array(
//                'year' => [
//                    'ids' => $model->year_id
//                ]
//            ),
            'display_scopes' => array('withSaldoAndNoStart')
        ],
        'custom_buttons' => $custom_buttons,
        'headerOptionRows' => [
            ['table_settings','show_add_button','pagination','filters_in','custom_buttons','filters_out']
        ]
    ]);

