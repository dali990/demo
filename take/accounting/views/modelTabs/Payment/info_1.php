<?php $uniq = SIMAHtml::uniqid(); ?>
<div id="<?php echo $uniq; ?>" class='sima-layout-panel bill_info'>
    <div class="sima-layout-panel bill_info_item bills">        
        <div class=" sima-layout-panel body _horizontal _splitter">
            <div class="sima-layout-panel main_table">
                <?php
                    $this->widget('base.extensions.SIMAGuiTable.SIMAGuiTable', [
                        'id'=>"bill_releases$uniq",
                        'model' => BillRelease::model(),
                        'columns_type' => 'inPayment',
                        'fixed_filter' => [                            
                            'payment'=>[
                                'ids'=>$model->id
                            ]
                        ],
                        'title'=>'Računi',
                        'table_settings'=>true,
                        'show_add_button'=>false,
                        'pagination'=>false,
                        'filters_in'=>false,
                        'export'=>false,
                        'filters_out'=>false,
                        'headerOptionRows'=>[
                            ['title','table_settings'],                                
                        ],
                    ]);
                ?>
            </div>
            <div class="sima-layout-panel proposals _horizontal">
                <?php if ($bill_proposals_number > 0) { ?>                    
                    <div class="sima-layout-fixed-panel current_unreleased_percent">
                        Nedostaje još: <span class="amount"></span>
                    </div>
                    <div class="sima-layout-panel">
                    <?php 
                        $bill_model = ($model->invoice===true)?'BillOut':'BillIn';
                        $this->widget('base.extensions.SIMAGuiTable.SIMAGuiTable', [
                            'id'=>"bills_proposals$uniq",
                            'model' => $bill_model::model(),
                            'columns_type' => 'inBillProposals',
                            'fixed_filter' => [
                                'like_payment_id'=>$model->id,
                                'display_scopes'=>[
                                    'unreleased_percent_of_amount'=>[
                                        $model->unreleased_amount
                                    ]
                                ],
                                'filter_scopes'=>[
                                    'unreleased'
                                ]
                            ],
                            'title'=>'Mogući računi',
                            'table_settings'=>true,
                            'show_add_button'=>false,
                            'pagination'=>false,
                            'filters_in'=>false,
                            'export'=>false,
                            'filters_out'=>false,
                            'custom_buttons'=>[
                                'Dodaj selektovane'=>[
                                    'func'=>"sima.accounting.unreleasing.paymentBill($model->id, 'bills_proposals$uniq')"
                                ]
                            ],
                            'headerOptionRows'=>[
                                ['title','table_settings','custom_buttons'],                                
                            ],
                            'setRowSelect'=>"onSelectBill$uniq",
                            'setMultiSelect'=>"onSelectBill$uniq",
                            'setRowUnSelect'=>"onUnSelectBill$uniq"
                        ]);
                    ?>
                        </div>
                <?php } ?>
            </div>        
        </div>
    </div>
</div>

<script type='text/javascript'>
    function onSelectBill<?php echo $uniq; ?>(obj)
    {
        sima.accountingMisc.calcUnreleasedPercentAmount($('#<?php echo $uniq; ?>').find('.bill_info_item.bills'));
    }
    function onUnSelectBill<?php echo $uniq; ?>(obj)
    {
        $('#<?php echo $uniq; ?>').find('.bill_info_item.bills .current_unreleased_percent').hide();
    }
</script>
