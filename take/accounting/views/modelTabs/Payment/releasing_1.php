<?php $uniq = SIMAHtml::uniqid();?>

<accounting-payment_tab-full_info id='<?=$uniq?>' v-bind:model='payment'>
</accounting-payment_tab-full_info>

<script type='text/javascript'>

    new Vue({
        el: '#<?=$uniq?>',
        data: {
            payment:sima.vue.shot.getters.model('<?=SIMAMisc::getVueModelTag($model)?>')
        }
    });

</script>
    