<?php $uniq = SIMAHtml::uniqid();?>

<?php if ($model->bill->isBill){?>
    <accounting-file_tab-bill_releasing id='<?=$uniq?>' v-bind:model='bill'>
    </accounting-file_tab-bill_releasing>

    <script type='text/javascript'>

        new Vue({
            el: '#<?=$uniq?>',
            data: {
                bill:sima.vue.shot.getters.model('<?=SIMAMisc::getVueModelTag($model->bill)?>')
            }
        });

    </script>
<?php }elseif (isset ($model->relief_bill)){?>
    <accounting-file_tab-relief_bill_releasing id='<?=$uniq?>' v-bind:model='bill'>
    </accounting-file_tab-relief_bill_releasing>

    <script type='text/javascript'>

        new Vue({
            el: '#<?=$uniq?>',
            data: {
                bill:sima.vue.shot.getters.model('<?=SIMAMisc::getVueModelTag($model->relief_bill)?>')
            }
        });

    </script>
<?php } ?>

