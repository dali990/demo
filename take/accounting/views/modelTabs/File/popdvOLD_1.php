<?php 

//if (isset($model->file->accounting_document->accounting_order->month))
if (!empty($model->bill->vat_in_diferent_month_id))
{
    $month = $model->bill->vat_in_diferent_month;
}
elseif (isset($model->account_document->account_order))
{
    $date = new DateTime($model->account_document->account_order->date);
    $month = Month::get($date->format('m'), $date->format('Y'));
    
}



if (isset($month)){
    
    $vat_export = new POPDVExport($month, $model, POPDVExport::BOOKING_TYPE_REGULER);
    
?>

<div class="sima-layout-panel _horizontal">
    <div class="sima-layout-fixed-panel" style="height: 50px;">
        <div style="border-bottom: 1px solid lightgray; width: 100%;height: calc(100% - 2px);">
            <span style="font-weight: bold; font-size: 1.5em; padding: 10px;">
                <?=Yii::t('AccountingModule.POPDV','BookedInMonth',['{month}' => $month->DisplayName]);?>
            </span>
        </div>
    </div>
    <div class="sima-layout-panel">
        <div style="padding: 20px;"><?=SIMAHtml::addContentToIFrame($vat_export->getDisplayHTML(false, false))?></div>
    </div>
<?php }else{ ?>
    <?=Yii::t('AccountingModule.POPDV','NotBooked');?>
<?php } ?>