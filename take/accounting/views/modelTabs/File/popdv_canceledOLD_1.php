<?php 

//if (isset($model->file->accounting_document->accounting_order->month))
if (isset($model->account_document->canceled_account_order))
{
    $date = new DateTime($model->account_document->canceled_account_order->date);
    $month = Month::get($date->format('m'), $date->format('Y'));
    $vat_export = new POPDVExport($month, $model, POPDVExport::BOOKING_TYPE_CANCELED);
?>

<div class="sima-layout-panel _horizontal">
    <div class="sima-layout-fixed-panel">
        <span style="font-weight: bold; padding: 10px;">storniran u mesecu <?=$month->DisplayName?></span>
    </div>
    <div class="sima-layout-panel">
        <div style="padding: 20px;"><?=SIMAHtml::addContentToIFrame($vat_export->getDisplayHTML(false, false))?></div>
    </div>
<?php }else{ ?>
    nije proknjizen
<?php } ?>