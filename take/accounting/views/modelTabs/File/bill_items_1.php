<?php $uniq = SIMAHtml::uniqid();?>

<accounting-file_tab-bill_items id='<?=$uniq?>' v-bind:model='bill' v-bind:read_only='read_only'>
</accounting-file_tab-bill_items>

<script type='text/javascript'>

    new Vue({
        el: '#<?=$uniq?>',
        data: {
            bill:sima.vue.shot.getters.model('<?=SIMAMisc::getVueModelTag($model->any_bill)?>'),
            read_only: <?=isset($model->bill)?'false':'true'?>
        }
    });

</script>
    