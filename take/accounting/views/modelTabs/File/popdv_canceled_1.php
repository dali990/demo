    
<?php $uniq = SIMAHtml::uniqid();?>

<accounting-file_tab-popdv id='<?=$uniq?>' v-bind:model='file' booking_type="<?= POPDVExport::BOOKING_TYPE_CANCELED?>">
</accounting-file_tab-popdv>

<script type='text/javascript'>

    new Vue({
        el: '#<?=$uniq?>',
        data: {
            file:sima.vue.shot.getters.model('<?=SIMAMisc::getVueModelTag($model)?>')
        }
    });

</script>
