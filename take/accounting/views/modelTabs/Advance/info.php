<?php $uniq = SIMAHtml::uniqid(); ?>
<div id="<?php echo $uniq; ?>" class='bill_info sima-layout-panel _vertical _splitter'>
    <div class="bill_info_item connect_to_avans sima-layout-panel">        
        <div class="sima-layout-panel body _horizontal _splitter">
            <div class="sima-layout-panel main_table">
                <?php
                    $this->widget('SIMAGuiTable', [
                        'id'=>"bill_releases1$uniq",
                        'model' => BillRelease::model(),
                        'columns_type' => 'inPayment',
                        'fixed_filter' => [                            
                            'payment'=>[
                                'ids'=>$model->id
                            ],
                            'bill'=>[
                                'bill_type'=>Bill::$ADVANCE_BILL
                            ]
                        ],
                        'title'=>'Povezan za avansnim računom',
                        'table_settings'=>true,
                        'show_add_button'=>false,
                        'pagination'=>false,
                        'filters_in'=>false,
                        'export'=>false,
                        'filters_out'=>false,
                        'headerOptionRows'=>[
                            ['title','table_settings'],                                
                        ],
                    ]);
                ?>
            </div>
            <div class="sima-layout-panel proposals _horizontal">
                <?php if ($advance_bill_proposals_number > 0) { ?>
                    <div class="current_unreleased_percent sima-layout-fixed-panel">
                        Nedostaje još: <span class="amount"></span>
                    </div>
                    <div class="sima-layout-panel">
                        <?php 
                            $advance_bill_model = ($model->invoice===true)?'AdvanceBillOut':'AdvanceBillIn';
                            $this->widget('base.extensions.SIMAGuiTable.SIMAGuiTable', [
                                'id'=>"advance_bills_proposals$uniq",
                                'model' => $advance_bill_model::model(),
                                'columns_type' => 'inBillProposals',
                                'fixed_filter' => [
                                    'like_payment_id'=>$model->id,
                                    'display_scopes'=>[
                                        'unreleased_percent_of_amount'=>[
                                            $model->unreleased_amount
                                        ]
                                    ],
                                    'filter_scopes'=>[
                                        'unreleased'
                                    ],
                                    'connected'=>false
                                ],
                                'title'=>'Mogući avansi',
                                'table_settings'=>true,
                                'show_add_button'=>false,
                                'pagination'=>false,
                                'filters_in'=>false,
                                'export'=>false,
                                'filters_out'=>false,
                                'headerOptionRows'=>[
                                    ['title','table_settings','custom_buttons'],                                
                                ],
                                'custom_buttons'=>[
                                    'Dodaj selektovane'=>[
                                        'func'=>"sima.accounting.unreleasing.advanceAdvanceBill($model->id, 'advance_bills_proposals$uniq')"
                                    ]
                                ],
                                'setRowSelect'=>"onSelectAdvanceBill$uniq",
                                'setMultiSelect'=>"onSelectAdvanceBill$uniq",
                                'setRowUnSelect'=>"onUnSelectAdvanceBill$uniq"
                            ]);
                        ?>
                    </div>
                <?php } ?>
            </div>        
        </div>
    </div>    
    <div class="bill_info_item bills sima-layout-panel">        
        <div class="body sima-layout-panel _horizontal _splitter">
            <div class="main_table sima-layout-panel">
                <?php
                    $this->widget('base.extensions.SIMAGuiTable.SIMAGuiTable', [
                        'id'=>"bill_releases2$uniq",
                        'model' => BillRelease::model(),
                        'columns_type' => 'inPayment',
                        'fixed_filter' => [                            
                            'payment'=>[
                                'ids'=>$model->id
                            ],
                            'bill'=>[
                                'bill_type'=>Bill::$BILL
                            ]
                        ],
                        'title'=>'Računi',
                        'table_settings'=>true,
                        'show_add_button'=>false,
                        'pagination'=>false,
                        'filters_in'=>false,
                        'export'=>false,
                        'filters_out'=>false,
                        'headerOptionRows'=>[
                            ['title','table_settings'],                                
                        ],
                    ]);
                ?>
            </div>
            <div class="proposals sima-layout-panel _horizontal">
                <?php if ($bill_proposals_number > 0) { ?>                    
                    <div class="current_unreleased_percent sima-layout-fixed-panel">
                        Nedostaje još: <span class="amount"></span>
                    </div>
                    <div class="sima-layout-panel">
                    <?php 
                        $bill_model = ($model->invoice===true)?'BillOut':'BillIn';
                        $this->widget('base.extensions.SIMAGuiTable.SIMAGuiTable', [
                            'id'=>"bills_proposals$uniq",
                            'model' => $bill_model::model(),
                            'columns_type' => 'inBillProposals',
                            'fixed_filter' => [
                                'like_payment_id'=>$model->id,
                                'display_scopes'=>[
                                    'unreleased_percent_of_amount'=>[
                                        $model->unreleased_amount
                                    ]
                                ],
                                'filter_scopes'=>[
                                    'unreleased'
                                ]
                            ],
                            'title'=>'Mogući računi',
                            'table_settings'=>true,
                            'show_add_button'=>false,
                            'pagination'=>false,
                            'filters_in'=>false,
                            'export'=>false,
                            'filters_out'=>false,
                            'custom_buttons'=>[
                                'Dodaj selektovane'=>[
                                    'func'=>"sima.accounting.unreleasing.paymentBill($model->id, 'bills_proposals$uniq')"
                                ]
                            ],
                            'headerOptionRows'=>[
                                ['title','table_settings','custom_buttons'],                                
                            ],
                            'setRowSelect'=>"onSelectBill$uniq",
                            'setMultiSelect'=>"onSelectBill$uniq",
                            'setRowUnSelect'=>"onUnSelectBill$uniq"
                        ]);
                    ?>
                    </div>
                <?php } ?>
            </div>        
        </div>
    </div>
</div>

<script type='text/javascript'>
    function onSelectAdvanceBill<?php echo $uniq; ?>(obj)
    {        
        sima.accountingMisc.calcUnreleasedPercentAmount($('#<?php echo $uniq; ?>').find('.bill_info_item.connect_to_avans'));
    }
    function onUnSelectAdvanceBill<?php echo $uniq; ?>(obj)
    {
        $('#<?php echo $uniq; ?>').find('.bill_info_item.connect_to_avans .current_unreleased_percent').hide();
    }
    function onSelectBill<?php echo $uniq; ?>(obj)
    {
        sima.accountingMisc.calcUnreleasedPercentAmount($('#<?php echo $uniq; ?>').find('.bill_info_item.bills'));
    }
    function onUnSelectBill<?php echo $uniq; ?>(obj)
    {
        $('#<?php echo $uniq; ?>').find('.bill_info_item.bills .current_unreleased_percent').hide();
    }
</script>


