<?php

$this->widget('SIMAGuiTable',[
    'model' => KPR::class,
    'columns_type' => 'bad_vat_booked',
    'fixed_filter' => [
        'filter_scopes' => ['BadVATBooked', 'inMonth' => [$model->base_month->month,$model->base_month->year->year]],
    ],
]);

?>
