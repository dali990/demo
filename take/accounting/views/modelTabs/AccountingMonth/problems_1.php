
<div class="sima-layout-panel _vertical">

    <div class="sima-layout-fixed-panel" style="width: 500px">

        <div style="margin-left: 10px; margin-bottom: 10px; font-size: large;">
            <?=Yii::t('AccountingModule.AccountingMonth','UnreleasedAdvances')?>: <?=$unreleased_advances?>
        </div>
        <div style="margin-left: 10px; margin-bottom: 10px; font-size: large;">
            <?=Yii::t('AccountingModule.AccountingMonth','BillsNotBooked')?>: <?=$bills_not_booked?>
        </div>
        <div style="margin-left: 10px; margin-bottom: 10px; font-size: large;">
            <?=Yii::t('AccountingModule.AccountingMonth','BillsBadVatBookedCount')?>: <?=$bills_bad_vat_booked_count?>
        </div>
        <div style="margin-left: 10px; margin-bottom: 10px; font-size: large;">
            <?=Yii::t('AccountingModule.AccountingMonth','VATKPRNotFound')?>: <?=$VATKPRNotFound?>
        </div>
        <div style="margin-left: 10px; margin-bottom: 10px; font-size: large;">
            <?=Yii::t('AccountingModule.AccountingMonth','VATKIRNotFound')?>: <?=$VATKIRNotFound?>
        </div>
    </div>

    <div class="sima-layout-panel">
        
        <?php $this->widget('SIMATabs',[
            'list_tabs_model' => $model,
            'show_tabs' => [
                'unreleased_advances',
                'bills_not_booked',
                'bills_bad_vat_booked',
                'vat_kpr_not_found',
                'vat_kir_not_found',
            ]
    //        'default_selected' => 'kir' //ostavljamo zakomentarisano da se ne bi ucitavalo jer dugo traje
        ]);?>
    </div>
    
</div>