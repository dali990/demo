<?php

$advance_payment_type = $advance_payment_type = Yii::app()->configManager->get('accounting.advance_payment_type');
if (empty($advance_payment_type))
{
    echo Yii::t('AccountingModule.Configuration','SetAdvancePayment');
}
else
{
    $first_day = $model->base_month->firstDay();
    $last_day = $model->base_month->lastDay();
    $this->widget('SIMAGuiTable',[
        'model' => Payment::class,
        'fixed_filter' => [
            'payment_type' => ['ids' => intval($advance_payment_type)],
            'bank_statement' => [
                'date' => SIMAHtml::UserToDbDate($first_day->day_date).'<>'.SIMAHtml::UserToDbDate($last_day->day_date)
            ],
            'filter_scopes' => ['unreleased'],
        ],
    ]);
}



?>
