<?php $uniq = SIMAHtml::uniqid(); ?>

<accounting-cost_location_tab-cost id="<?=$uniq?>" class="sima-layout-panel" v-bind:model="cost_location">
</accounting-cost_location_tab-cost>
        
<script type="text/javascript">

    new Vue({
        el: '#<?=$uniq?>',
        data: {
            cost_location: sima.vue.shot.getters.model('<?=SIMAMisc::getVueModelTag($model)?>')
        }
    });

</script>

