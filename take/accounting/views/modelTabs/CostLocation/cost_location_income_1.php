<?php $uniq = SIMAHtml::uniqid(); ?>

<accounting-cost_location_tab-income id="<?=$uniq?>" class="sima-layout-panel" v-bind:cost_location="cost_location">
</accounting-cost_location_tab-income>
        
<script type="text/javascript">

    new Vue({
        el: '#<?=$uniq?>',
        data: {
            cost_location: sima.vue.shot.getters.model('<?=SIMAMisc::getVueModelTag($model)?>')
        }
    });

</script>

