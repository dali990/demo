<?php

Yii::app()->controller->widget('SIMAGuiTable', array(
    'id' => $uniq,
    'class' => '_low_profile_rows',
    'model' => AccountTransaction::class,
    'custom_buttons'=>$custom_buttons,
    'pagination'=>true,
    'add_button' => ($booked)?false:array(
        'init_data'=>array(
            'AccountTransaction'=>array(
                'account_document_id' => array(
                    'hidden',
                    'init'=>$model->id
                ),
                'account_id' => array(
                    'model_filter' => array(
                        'year' => [
                            'ids' => $year_id
                        ]
                    )
                )
            ),
        ),
        'inline'=>true
    ),
    'fixed_filter'=>array(
        'account_document' => array(
            'account_order' => array(
                'ids'=>$model->account_order->id
            )
        ),
//        'account' => array(
//            'year' => [
//                'ids' => $year_id
//            ]
//        ),
        'account_debit' => array(
            'year' => [
                'ids' => $year_id
            ]
        ),
        'account_credit' => array(
            'year' => [
                'ids' => $year_id
            ]
        ),
        'display_scopes' => array('withSaldoAndNoStart')
     ),
    'columns_type' => ($booked)?'account_order_booked':'account_order',                    
    'headerOptionRows' => [
        ['table_settings','show_add_button','pagination','filters_in','custom_buttons','filters_out']
    ]
));