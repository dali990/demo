<?php

Yii::app()->controller->widget('SIMAGuiTable', array(
    'model' => 'AccountDocument',
    'pagination'=>false,
    'fixed_filter'=>array(
        'AND',
        [
            'NOT',
            ['ids' => $model->account_order->id]
        ],
        [
            'account_order' => array(
                'ids'=>$model->account_order->id
            )
        ]
        
     ),
));