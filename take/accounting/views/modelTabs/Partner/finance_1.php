<?php $uniq = SIMAHtml::uniqid();?>

<div class="sima-layout-panel _horizontal">
    <accounting-partner_tab-finance 
        id='<?=$uniq?>' 
        
        v-bind:model='model'
        class="sima-layout-fixed-panel"
    >
    </accounting-partner_tab-finance>
    <div class="sima-layout-panel">
        <?php
            $this->widget('base.extensions.SIMAGuiTable.SIMAGuiTable', [
                'model'=> PartnerFinance::model(),            
                'fixed_filter'=>array(
                    'display_scopes' => array(
                        'byName'
                    ),
                    'filter_scopes' => array(
                        'partner' => array(
                            $model->id
                        )
                    )
                )
            ]);
        ?>
    </div>
</div>

<script type='text/javascript'>

    new Vue({
        el: '#<?=$uniq?>',
        data: {
            model: sima.vue.shot.getters.model('<?=SIMAMisc::getVueModelTag($model)?>')
        }
    });

</script>
    