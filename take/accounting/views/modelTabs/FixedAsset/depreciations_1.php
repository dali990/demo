<?php
$this->widget('SIMAGuiTable',[
    'model' => 'FixedAssetsDepreciationItem',
    'columns_type' => 'inFixedAsset',
    'fixed_filter' => [
        'accounting_fixed_asset' => ['ids' => $model->id]
    ]
]);
