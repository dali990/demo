<?php ?>
<div style="width:100%">
    <table class="main-tbl header-tbl">
        <tbody  class="font-bold font-middle" style="text-align: center">
            <tr>
                <td style="text-align: left; padding-left: 29mm;"  width="55%">РЕПУБЛИКА СРБИЈА</td>
                <td style="text-align: right">Образац ПППДВ</td>
            </tr>
            <tr>
                <td style="text-align: left;"> МИНИСТАРСТВО ФИНАНСИЈА - ПОРЕСКА УПРАВА</td>
                <td></td>
            </tr>
            <tr>
                <td style="text-align: left;">Организациона јединица _________________</td>
                <td></td>
            </tr>
        </tbody>
    </table>

    <table class="main-tbl">
        <tbody>
            <tr class="green font-bold">
                <td align="center">ПОРЕСКА ПРИЈАВА <br> ПОРЕЗА НА ДОДАТУ ВРЕДНОСТ</td>
            </tr>
            <tr class="orange font-middle font-bold">
                <td align="center"> ЗА ПЕРИОД ОД <span class="white">____________</span> ДO <span class="white"> __________</span>  <span class="white"> <?php  echo "_________" ?></span><sub class="font-small"> (година)</sub></td>
            </tr>
            <tr class="orange font-middle">
                <td class="left-half">
                    <table>
                        <tr>
                            <td>Порески идентификациони број(ПИБ):</td>
                            <td class="white">________________</td>
                        </tr>
                    </table>
                </td>
                <td  class="right-half" align="center">
                    <table width="100%">
                        <tr>
                            <td class="white tall-td tbl-border"></td>
                        </tr>
                        <tr>
                            <td align="center" class="font-small">(назив и адреса)</td>
                        </tr>
                    </table>

                </td>
            </tr>
            <tr> <td  align="right" class="font-small">(у динарима, без децимала)</td></tr>
            <tr class="orange content-center font-small">
                <td>
                    <table class="full-width">
                        <thead>
                        <th width="40%">I. ПРОМЕТ ДОБАРА И УСЛУГА</th>
                        <th></th>
                        <th width="30%">Износ накнаде без ПДВ</th>
                        <th></th>
                        <th width="30%">ПДВ</th>
                        </thead>
                        <tbody class="col-5">
                            <tr>
                                <td>1. Промет добара и услуга који је ослобођен од ПДВ са правом на одбитак претходног пореза</td>
                                <td>001</td>
                                <td style="text-align: right;"><?= SIMAHtml::number_format($popdv->getPPPDVParamValue('001'),0)?></td>
                                <td></td>
                                <td style="background-color:  #ffcc9a"></td>
                            </tr>
                            <tr>
                                <td class="">2. Промет добара и услуга који је ослобођен од ПДВ без права на одбитак претходног пореза</td>
                                <td>002</td>
                                <td style="text-align: right;"><?= SIMAHtml::number_format($popdv->getPPPDVParamValue('002'),0)?></td>
                                <td></td>
                                <td style="background-color:  #ffcc9a"></td>
                            </tr>
                            <tr>
                                <td>3. Промет добара и услуга по општој стопи</td>
                                <td>003</td>
                                <td style="text-align: right;"><?= SIMAHtml::number_format($popdv->getPPPDVParamValue('003'),0)?></td>
                                <td>103</td>
                                <td style="text-align: right;"><?= SIMAHtml::number_format($popdv->getPPPDVParamValue('103'),0)?></td>
                            </tr>
                            <tr>
                                <td>4. Промет добара и услуга по посебној стопи</td>
                                <td>004</td>
                                <td style="text-align: right;"><?= SIMAHtml::number_format($popdv->getPPPDVParamValue('004'),0)?></td>
                                <td>104</td>
                                <td style="text-align: right;"><?= SIMAHtml::number_format($popdv->getPPPDVParamValue('104'),0)?></td>
                            </tr>
                            <tr class="font-bold">
                                <td>5. ЗБИР(1+2+3+4)</td>
                                <td>005</td>
                                <td style="text-align: right;"><?= SIMAHtml::number_format($popdv->getPPPDVParamValue('005'),0)?></td>
                                <td>105</td>
                                <td style="text-align: right;"><?= SIMAHtml::number_format($popdv->getPPPDVParamValue('105'),0)?></td>
                            </tr>
                        </tbody>
                        <thead class="col-5">
                        <th>II. ПРЕТХОДНИ ПОРЕЗ</th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        </thead>
                        <tbody class="col-5">
                            <tr>
                                <td>6. Претходни порез плаћен приликом увоза</td>
                                <td>006</td>
                                <td style="text-align: right;"><?= SIMAHtml::number_format($popdv->getPPPDVParamValue('006'),0)?></td>
                                <td>106</td>
                                <td style="text-align: right;"><?= SIMAHtml::number_format($popdv->getPPPDVParamValue('106'),0)?></td>
                            </tr>
                            <tr>
                                <td>7. ПДВ надокнада плаћена пољопривреднику</td>
                                <td>007</td>
                                <td style="text-align: right;"><?= SIMAHtml::number_format($popdv->getPPPDVParamValue('007'),0)?></td>
                                <td>107</td>
                                <td style="text-align: right;"><?= SIMAHtml::number_format($popdv->getPPPDVParamValue('107'),0)?></td>
                            </tr>
                            <tr>
                                <td>8. Претходни порез, осим претходног пореза са ред. бр. 6. и 7.</td>
                                <td>008</td>
                                <td style="text-align: right;"><?= SIMAHtml::number_format($popdv->getPPPDVParamValue('008'),0)?></td>
                                <td>108</td>
                                <td style="text-align: right;"><?= SIMAHtml::number_format($popdv->getPPPDVParamValue('108'),0)?></td>
                            </tr>
                            <tr class="font-bold">
                                <td>9. ЗБИР(6+7+8)</td>
                                <td>009</td>
                                <td style="text-align: right;"><?= SIMAHtml::number_format($popdv->getPPPDVParamValue('009'),0)?></td>
                                <td>109</td>
                                <td style="text-align: right;"><?= SIMAHtml::number_format($popdv->getPPPDVParamValue('109'),0)?></td>
                            </tr>
                        </tbody>
                        <thead class="col-5">
                        <th>III. ПOРЕСКА ОБАВЕЗА</th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        </thead>
                        <tbody class="col-5">
                            <tr>
                                <td>10. Износ ПДВ у пореском периоду(5-9)</td>
                                <td></td>
                                <td style="background-color:  #ffcc9a"></td>
                                <td>110</td>
                                <td style="text-align: right;"><?= SIMAHtml::number_format($popdv->getPPPDVParamValue('110'),0)?></td>
                            </tr>
                        </tbody>
                        <tbody class="col-5">
                            <tr>
                                <td>11. Повраћај</td>
                                <td></td>
                                <td style="background-color:  #ffcc9a"></td>
                                <td></td>
                                <td style="background-color:  #ffcc9a">
                                    <table style="width:100%">
                                        <tr style="<?=is_null($vat_return)?'color: gray; background-color: lightgray;':''?>">
                                            <td style="text-align: center;width:50%; <?=($vat_return===true)?'color: gray; cursor: pointer;':'font-weight: bold;'?>">
                                                <span class="font-middle"
                                                      <?php if ($vat_return===true){echo "onclick='parent.sima.accounting.toggleVATReturn($month->id)'";}?>
                                                      >НЕ</span>
                                            </td>
                                            <td style="text-align: center;width:50%; <?=($vat_return===false)?'color: gray; cursor: pointer;':'font-weight: bold;'?>">
                                                <span class="font-middle"
                                                      <?php if ($vat_return===false){echo "onclick='parent.sima.accounting.toggleVATReturn($month->id)'";}?>
                                                      
                                                      >ДА</span>
                                            </td>
                                            <!--<td><span class="white">ДА</span></td>-->
                                        </tr>
                                        <tr>
                                            <td colspan="2" style="text-align: center; "><span>(обавезно заокружити опцију)</span></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                        </tbody>
                    </table>
                </td>

            </tr>
        </tbody>
    </table>
    <table class="main-tbl tbl-border font-small">
        <tr class="bg-gray"><td colspan="3">попуњава подносилац пријаве:</td></tr>
        <tr><td colspan="3">Пријаву односно њен део припремио је порески саветник:</td></tr>
        <tr>
            <td>_______________________</td>
            <td>|_|_|_|_|_|_|_|_|_|_|_|_|_|</td>
            <td>|_|_|_|_|_|_|_|_|_|_|_|_|_|</td>
        </tr>
        <tr class="font-extra-small">
            <td>(Потпис пореског саветника)</td>
            <td>(ПИБ пореског саветник)</td>
            <td>(ЈМБГ пореског саветника)</td>
        </tr>
        <tr><td colspan="3">Под кривичном и материјалном одговорношћу изјављујем да су подаци у пријави потпуни и тачни:</td></tr>
        <tr>
            <td>__________________________</td>
            <td>__________________________</td>
            <td>__________________________________</td>
        </tr>
        <tr class="font-extra-small">
            <td>(Место)</td>
            <td>(Датум)</td>
            <td>(Потпис одговорног лица)</td>
        </tr>
        <tr  align="center"><td colspan="3">М.П.</td></tr>
        <tr class="bg-gray"><td colspan="3">попуњава Пореска управа</td></tr>
        <tr>
            <td colspan="2">Потврда о пријему евиденционе пријаве:</td>
        </tr>
        <tr style="height: 35px">

        </tr>
    </table>

    <!--</table>-->
</div><!-- /content -->




