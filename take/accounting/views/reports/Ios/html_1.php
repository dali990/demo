<table width="100%" cellspacing="0" cellpadding="0" class="wrapper">
    <tr>
        <td colspan="3">
            <table width="100%" cellspacing="0" cellpadding="0" class="heading_wrapper">
                <tr>
                    <td width="50%">
                        <div class="set_doo_info">
                            <b>poverilac</b><br>
                            <?=$curr_company->DisplayName?><br>
                            <?=empty($curr_company->partner->main_address) ? "" : $curr_company->partner->main_address->DisplayName?><br>
                            PIB: <?=$curr_company->PIB?><br>
                        </div>
                    </td>
                    <td width="50%">
                        <div class="partner_info" width="100%">
                            <b>dužnik</b> <br>
                            <?=$ios->partner->DisplayName?><br>
                            <?=empty($ios->partner->main_address) ? "" : $ios->partner->main_address->DisplayName?><br>
                            <?php if(!empty($ios->partner->company_model)): ?>
                                PIB: <?=$ios->partner->company_model->PIB?><br>
                            <?php elseif(!empty($ios->partner->person)): ?>
                                JMBG: <?=$ios->partner->person->JMBG?><br>
                            <?php endif ?>
                        </div>
                   </td>
                </tr>
             </table>
        </td>
    </tr>
    <tr class="io_heading_row">
        <td class="io_heading" colspan="3">
            <div class="io_heading_p"><br>U skladu sa članom 18 Zakona  o računovodstvu  (Službeni glasnik RS 062/2013) dostavljamo</div>
            <div  class="io_main_heading">IZVOD otvorenih stavki</div>
             <div class="io_heading_p"> Na dan <?=$ios->ios_date?> </div><br>
        </td>
    </tr>
    <tr>
        <td class="ios_description" colspan="3">
            Prema našim poslovnim knjigama, konto  <?=$ios->account->code?> <?=$ios->partner->DisplayName?>, Vaše obaveze-potraživanja na dan <?=$ios->ios_date?> godine, sastoje se iz sledecih otvorenih stavki:
            <br><br>
        </td>
    </tr>
    <tr>
        <td colspan="3"> 
           <table class="ios_items_table" cellpadding="1.6mm" width="100%" cellspacing="0">
               <tr class="ios_items_header">
                    <th width="10.1mm" class="helper_text">R.br.</th>
                    <th class="helper_text">Broj računa</th>
                    <th class="helper_text">Datum</th>
                    <th class="helper_text">Valuta</th>
                    <th class="helper_text">Duguje</th>
                    <th class="helper_text">Potražuje</th>
                    <th class="helper_text">Saldo</th>
                </tr>
                <?=$html?>
                <tr class="ios_items_header">
                    <td class="empty_cell"></td>
                    <td class="empty_cell"></td>
                    <td class="empty_cell"></td>
                    <td class="empty_cell"></td>
                    <td class="helper_text">
                        <?=SIMAHtml::number_format($ios->debit)?>
                    </td>
                    <td class="helper_text">
                        <?=SIMAHtml::number_format($ios->credit)?>
                    </td>
                    <td class="helper_text">
                        <?=SIMAHtml::number_format($ios->saldo)?>
                    </td>
                </tr>
            </table>
            <table width="100%" cellspacing="0">
                <tr class="ios_saldo" width="100%">
                    <td width="50%" style="padding-top: 1.6mm">
                        <div class="ios_saldo_text">
                            SALDO U NAŠU KORIST: 
                        </div>
                    </td>
                    <td width="50%" style="padding-top: 1.6mm">
                        <div class="ios_saldo_value">
                            <?=SIMAHtml::number_format($ios->saldo)?>
                        </div>
                    </td>
               </tr>
            </table>
        </td>
    </tr>
    <tr class="ios_signatures">
        <td class="ios_sender" width="33%">
            <br><br><br>Pošiljalac izvoda
        </td>
        <td class="ios_acceptance" width="33%"><br><br><br>Potvrđujemo iskazano stanje u celini </td>
        <td class="helper_text" width="33%"><br><br><br>_________________________(M.P.)</td>
    </tr>
    <tr class="ios_signatures">
        <td class="ios_sender" width="33%"></td>
        <td class="ios_sender" width="33%"></td>
        <td class="helper_text" width="33%">
            (mesto, datum, potpis)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        </td>
    </tr>
    <tr class="ios_signatures">
        <td class="ios_sender" width="33%">__________________________</td>
        <td class="ios_acceptance" width="33%">Osporavamo za  ___________ dinara</td>
        <td class="helper_text" width="33%">_________________________(M.P.)</td>
    </tr>
    <tr class="ios_signatures">
        <td class="ios_sender" width="33%"> &nbsp;&nbsp;&nbsp;&nbsp;<?=$company_city?>, <?=$ios->ios_date?> (M.P.)</td>
        <td class="ios_acceptance" width="33%"></td>
        <td class="helper_text" width="33%">(potpis)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
    </tr>
    <tr>
        <td class="ios_note" colspan="3">
            <br>Napomena u slučaju osporavanja:<br><br>
            <hr><br>
            <hr><br>
            <hr><br>
            <hr>
            <div><br>Ukoliko dužnik ne vrati potvrđen ili osporen IOS u roku od 15 dana, smatraće se da je saglasan sa iskazanim stanjem.</div>
        </td>
    </tr>
</table>