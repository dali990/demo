<?php
    echo "1#2#3#4#5#6#7#8#9#10#11#12#13#14#15#16#17";
    echo "\n";

    $i=1; 
    
    foreach ($rows as $row)
    {
        echo "#" . $row['order_in_year'] /// 1
            . "#" . $row['booking_date'] /// 2
            . "#" . $row['bill_number'] /// 3
            . "#" . $row['income_date'] /// 4
            . "#" . $row['partner_display_name'] /// 5
            . "#" . $row['partner_pib_jmbg'] /// 6
            . "#" . $row['KIR_7'] /// 7
            . "#" . $row['KIR_8'] /// 8
            . "#" . $row['KIR_9'] /// 9
            . "#" . $row['KIR_10'] /// 10
            . "#" . $row['KIR_11'] /// 11
            . "#" . $row['KIR_12'] /// 12
            . "#" . $row['KIR_13'] /// 13
            . "#" . $row['KIR_14'] /// 14
            . "#" . $row['KIR_15']  /// 15
            . "#" . $row['KIR_16'] /// 16
            . "#" . $row['KIR_17'] . "\n"; /// 17
    }
    
    if(isset($sum_row) && count($sum_row) > 0)
    {
        echo '######';
        for($i=7; $i<18; $i++)
        {
            echo '#' . $sum_row["KIR_" . ($i+1)];
        }
    }
?>