<page size="A4">
<p>Knjiga izlaznih računa za mesec <?php echo $month_disp;?></p>
<p>Štampao: <?php echo Yii::app()->user->model->DisplayName; ?></p>
<p>Datuma: <?php echo SIMAHtml::DbToUserDate(SIMAHtml::currentDateTime(false)); ?></p><br>

<table class="KIR Report">
    <thead>
	<tr>
            <th rowspan='3'>Redni broj</th>
            <th rowspan='3'>Datum knjiženja računa</th>
            <th colspan='5'>RAČUN ILI DRUGI DOKUMENT</th>
            <th colspan='4'>Oslobođen promet</th>
            <th colspan='4'>Oporezivi promet</th>
            <th rowspan='3'>Ukupan promet dobara i usluga sa pravom i bez prava na odbitak prethodnog poreza bez PDV(8 do 11+12+14)</th>
            <th rowspan='3'>Promet dobara i usluga sa pravom na odbitak prethodnog poreza bez PDV(8+10+12+14)</th>
	</tr>
	<tr>
            <th rowspan='2'>Broj računa</th>
            <th rowspan='2'>Datum izdavanja računa(ili drugog dokumenta)</th>
            <th colspan='2'>KUPAC</th>
            <th rowspan='2'>Ukupna naknada sa PDV</th>
            <th rowspan='2'>Oslobođen promet sa pravom na odbitak prethodnog poreza(čl.24 Zakona)</th>
            <th rowspan='2'>Oslobođen promet bez prava na odbitak prethodnog poreza(čl.25 Zakona)</th>
            <th colspan='2'>Promet u inostranstvu</th>
            <th colspan='2'>Po stopi od 20%</th>
            <th colspan='2'>Po stopi od 10%</th>
	</tr>
	<tr>
            <th>Naziv (ime i sedište)</th>
            <th>PIB ili JMBG</th>	
            <th>za koji bi postojalo pravo na prethodni porez da je promet izvršen u zemlji</th>
            <th>za koji ne bi postojalo pravo na prethodni porez da je promet izvršen u zemlji</th>	
            <th>osnovica</th>
            <th>iznos PDV</th>	
            <th>osnovica</th>
            <th>iznos PDV</th>	
	</tr>
	<tr>
		<?php for($i=1; $i<18; $i++) echo "<td>$i</td>"?>
	</tr>
    </thead>
    <tbody>
	<?=$html_rows?>
    </tbody>
    <?=$html_sum_rows?>
</table><br>
<?=$html_pdv_income?>
</page>