<table width="100%" class="account-pdf-content" cellpadding="2" cellspacing="0">
    <?php if (!is_null($curr_month)) { ?>
        <tr nobr="true" class="month_line">
            <td width="20%" colspan="3"></td>
            <td width="35%" class="_text-left">Ukupno za mesec <span style="font-weight: bold;"><?php echo $curr_month; ?></span></td>        
            <td width="15%" class="_text-right _total" style="font-weight: bold;"><?php echo SIMAHtml::number_format($debit_per_month); ?></td>
            <td width="15%" class="_text-right _total" style="font-weight: bold;"><?php echo SIMAHtml::number_format($credit_per_month); ?></td>
            <td width="15%" class="_text-right _total" style="border: 0.3mm solid black; font-weight: bold;"><?php echo SIMAHtml::number_format($debit_per_month - $credit_per_month); ?></td>          
        </tr>
        <tr nobr="true">
            <td colspan="7"></td>
        </tr>
        <tr nobr="true">
            <td colspan="7"></td>
        </tr>
    <?php } ?>
    <tr class="account-pdf-content-footer" nobr="true">
        <td width="20%" colspan="3"></td>
        <td width="35%" style="text-align: left;">Početno stanje:</td>        
        <td width="15%" class="_text-right _total"><?php echo SIMAHtml::number_format($account->debit_start); ?></td>
        <td width="15%" class="_text-right _total"><?php echo SIMAHtml::number_format($account->credit_start); ?></td>
        <td width="15%" class="_text-right _total"><?php echo SIMAHtml::number_format($account->debit_start-$account->credit_start); ?></td>        
    </tr>
    <tr class="account-pdf-content-footer" nobr="true">
        <td width="20%" colspan="3"></td>
        <td width="35%" style="text-align: left;">Promet:</td>        
        <td width="15%" class="_text-right _total"><?php echo SIMAHtml::number_format($account->debit_current); ?></td>
        <td width="15%" class="_text-right _total"><?php echo SIMAHtml::number_format($account->credit_current); ?></td>
        <td width="15%" class="_text-right _total"><?php echo SIMAHtml::number_format($account->debit_current-$account->credit_current); ?></td>        
    </tr>
    <tr class="account-pdf-content-footer" nobr="true">
        <td width="20%" colspan="3"></td>
        <td width="35%" style="text-align: left;">Promet sa početnim stanjem:</td>        
        <td width="15%" class="_text-right _total"><?php echo SIMAHtml::number_format($account->debit_sum); ?></td>
        <td width="15%" class="_text-right _total"><?php echo SIMAHtml::number_format($account->credit_sum); ?></td>
        <td width="15%" class="_text-right _total"><?php echo SIMAHtml::number_format($account->debit_sum-$account->credit_sum); ?></td>        
    </tr>
</table>