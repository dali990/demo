<div style="width: 100%; text-align: center;">
    <span style="font-size: 4.8mm;">
        <?php echo Yii::app()->company->DisplayName; ?>
    </span>
    <?php if (isset(Yii::app()->company->partner->main_address)) { ?>
        <br />
        <span style="color: #B0B0B0; font-size: 4.8mm;">
            <?php echo Yii::app()->company->partner->main_address->DisplayName; ?>
        </span>
    <?php } ?>        
</div>
<table width="100%" style="border-top: 0.1em solid #B0B0B0;" cellpadding="1.1mm">
    <tr>
        <td width="50%" style="text-align: left; font-weight: bold; font-size: 5.3mm;">KARTICA KONTA</td>
        <td width="50%" style="text-align: right">
            za: <span style="font-weight: bold;"><?php echo $account->year->DisplayName.'g.'; ?></span>
        </td>
    </tr>    
</table>
<table width="100%" style="border: 0.1em solid black;" cellpadding="2.1mm">
    <tr>
        <td width="25%" style="text-align: left;">
            <?php echo $account->code; ?> 
        </td>
        <td width="75%" style="text-align: left;">
            <?php if (!empty($partner)) { ?>                 
                <span style="font-size: 4.8mm; font-weight: bold;"><?php echo $partner->DisplayName; ?></span>
                <?php if (isset($partner->main_address)) { ?>
                    <br />
                    <span><?php echo $partner->main_address->DisplayName; ?></span>
                <?php } ?>
            <?php } ?>
        </td>
    </tr>
</table>
<div style="height: 2.6mm;"></div>
<?php
    if (!empty($used_filters_html))
    {
        echo $used_filters_html;
    }
?>
<div style="height: 2.6mm;"></div>