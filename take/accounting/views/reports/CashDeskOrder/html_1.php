<page size="A4">
<div id="container">
    <header>
        <img src='<?= $curr_company->getLogoImagePath() ?>' />
        <div class="content">
            <ul>
                <li><?= $curr_company->DisplayName ?></li>
                <li><?= !empty($curr_company->partner) ? $curr_company->partner->DisplayMainAddress : '' ?></li>
                <li>
                    <span>PIB: </span><?= $curr_company->PIB ?> |
                    <span>Matični broj: </span><?= $curr_company->MB ?> |
                    <span>Šifra delatnosti: </span><?= !empty($curr_company->work_code) ? $curr_company->work_code->code : ''; ?>
                </li>
            </ul>
        </div>
    </header>

    <div class="main">
        <div class="title">
            <table>
                <tr>
                    <td width="75%">
                        <h1>NALOG BLAGAJNI</h1>
                    </td>
                    <td width="5%" style="text-align: right; font-size:3.704mm;">
                        <p>Broj: </p>
                    </td>
                    <td width="20%" class="title-td">
                        &nbsp;<span><?=$cash_desk_order->DisplayName?></span>
                    </td>
                </tr>  
                <tr>
                    <td width="75%" style="text-align: left;">
                        <p><strong><?=($cash_desk_order->isIN ? "NAPLATITE" : "ISPLATITE")?></strong></p>
                    </td>
                    <td width="5%" style="text-align: right; font-size:3.704mm;">
                        <p>Datum: </p>
                    </td>
                    <td width="20%" class="title-td">
                        &nbsp;<?=SIMAHtml::DbToUserDate($cash_desk_order->date)?>
                    </td>
                </tr>
            </table>
        </div>
        <table class="bill">
            <tr>
                <td style="padding-top: 2.6mm;">Na TERET računa:</td>
                <td></td>
            </tr>
        </table>
        <div class="content">
            <table id="table">
                <tr>
                    <th>SADRŽAJ</th>
                    <th>DINARA</th>
                    <th>RAČUN BR.</th>
                </tr>
                <?=$html?>
                <tr>
                    <td><b>UKUPNO</b></td>
                    <td><?=SIMAHtml::number_format($cash_desk_order->amount)?></td>
                    <td></td>
                </tr>
            </table>
            <br>
            <table class="letter">
                <tr>
                    <td>SLOVIMA:</td>
                    <td><?=SIMAHTML::money2letters($cash_desk_order->amount, 'lat')?></td>
                </tr>
            </table>	
            <div class="clear"></div>
            <div class="signatures">
                <span>Kontrolisao</span>
                <span>__________________________________</span>
            </div><!-- /signatures -->
            <div class="signatures">
                <span>Primio</span>
                <span>__________________________________</span>
            </div><!-- /signatures -->
        </div><!-- /content -->
    </div> <!-- /main -->
</div>
</page>
