<page size="A4">
<div id="container">
    <header>
        <img src='<?= $curr_company->getLogoImagePath() ?>' />
        <div class="content">
            <ul>
                <li><?= $curr_company->DisplayName ?></li>
                <li><?= !empty($curr_company->partner) ? $curr_company->partner->DisplayMainAddress : '' ?></li>
                <li>
                    <span>PIB: </span><?= $curr_company->PIB ?> |
                    <span>Matični broj: </span><?= $curr_company->MB ?> |
                    <span>Šifra delatnosti: </span><?= !empty($curr_company->work_code) ? $curr_company->work_code->code : ''; ?>
                </li>
            </ul>
        </div>
    </header>

    <div class="main">
        <div class="title">
            <table width="100%">
                <tr>
                    <td width="70%" rowspan="2">
                        <h1>Pregled dugovanja po partnerima</h1>
                    </td>
                    <td width="15%">
                        <p>Na dan:</p>
                    </td>
                    <td width="15%" style="border-bottom: 0.3mm solid black;">
                        <p1>&nbsp;<?=SIMAHtml::DbToUserDate($date)?></p1>
                    </td>
                </tr>
                <tr>
                    <td width="15%">
                        <p>Izveštaj izradio/la:</p>
                    </td>
                    <td width="15%" style="border-bottom: 0.3mm solid black;">
                        <p1>&nbsp;<?=$curr_user?></p1>
                    </td>
                </tr>
            </table>
            <div style="clear: both;"></div>
        </div><!-- /title -->
        <div class="content">
                <?=$html?>
            </table> 
        </div>
    </div> <!-- /main -->
</div><!-- /container -->
</page>