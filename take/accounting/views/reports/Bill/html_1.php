<page size="A4">
    <div id="container" class="bill-report">
        <header>
            <img src='<?= $company_logo_path ?>' />
            <div class="content <?php echo $show_phone_in_header === true ? 'header-column1' : ''; ?>">
                <ul>
                    <li><?= $curr_company->DisplayName ?></li>
                    <li><?= $curr_company->partner->DisplayMainAddress ?></li>
                    <li>
                        <span>PIB: </span><?= $curr_company->PIB ?> |
                        <span>Matični broj: </span><?= $curr_company->MB ?> |
                        <span>Šifra delatnosti: </span><?= !empty($curr_company->work_code) ? $curr_company->work_code->code : ''; ?>
                    </li>
                </ul>
            </div>
            <?php if ($show_phone_in_header === true) { ?>
                <div class="content header-column2">
                    <ul>
                        <!--Mora nowrap jer pdf iz nekog razloga prebaci deo telefona iza - u sledeci red, iako ima prostora u trenutnom redu-->
                        <li style="white-space: nowrap;"><span>Telefon: </span> <?= $curr_company_main_phone_number ?></li>
                        <li><span>E-mail: </span> <?= $curr_company_main_email_address ?></li>
                    </ul>
                </div>
            <?php } ?>
        </header>

        <div class="main">
            <div class="main-info">
                <div style="display: inline-block; vertical-align: top; width: 35%;">
                    <?php 
                        if (
                            Yii::app()->configManager->get('accounting.templates.bill_templates.show_filing_number', false) && 
                            isset($bill->file->filing)
                        ) 
                        { ?>
                        <div>
                            <div class="main-info-item">
                                <span class="main-info-title">Zavodni broj: </span><?=$bill->file->filing->filing_number?>
                            </div>
                            <div class="main-info-item">
                                <span class="main-info-title">Datum: </span><?=SIMAHtml::DbToUserDate($bill->file->filing->date)?>
                            </div>
                        </div>
                        <br />
                    <?php } ?>
                    <div class="main-info-item">
                        <span class="main-info-title">Datum računa: </span><?=SIMAHtml::DbToUserDate($bill->income_date)?>
                    </div>
                    <?php if (!empty($bill->issue_location)) { ?>
                        <div class="main-info-item">
                            <span class="main-info-title"><?=Yii::t('AccountingModule.Bill', 'BillIssueLocation')?>:</span>
                            <?=$bill->issue_location->name?>
                        </div>
                    <?php } ?>
                    <div class="main-info-item">
                        <span class="main-info-title">Datum prometa: </span><?=SIMAHtml::DbToUserDate($bill->traffic_date)?>
                    </div>
                    <?php if (!empty($bill->traffic_location)) { ?>
                        <div class="main-info-item">
                            <span class="main-info-title"><?=Yii::t('AccountingModule.Bill', 'TrafficLocation')?>:</span>
                            <?=$bill->traffic_location->name?>
                        </div>
                    <?php } ?>
                    <div class="main-info-item">
                        <span class="main-info-title">Valuta: </span><?=SIMAHtml::DbToUserDate($bill->payment_date)?>
                    </div>
                </div>
                <div style="display: inline-block; vertical-align: top; width: 25%;"></div>
                <div style="display: inline-block; vertical-align: top; width: 35%;">
                    <div class="main-info-item">
                        <?=$bill->partner->DisplayName?>
                    </div>
                    <div class="main-info-item">
                        <?=$bill->partner->DisplayMainAddress?>
                    </div>
                    <div class="main-info-item">
                        <span class="main-info-title"><?= isset($bill->partner->person) ? 'JMBG' : 'PIB' ?>:</span>
                        <?= isset($bill->partner->person) ? $bill->partner->person->JMBG : $bill->partner->company_model->PIB ?>
                    </div>
                    <?php if(!empty($bill->partner->company_model->MB)): ?>
                        <div class="main-info-item">
                            <span class="main-info-title">MB: </span>
                            <?=$bill->partner->company_model->MB?>
                        </div>
                    <?php endif ?>
                </div>
            </div>
            <div class="title">
                <h1><?=$bill_title . ' ' . $bill->bill_number?></h1>
                <h3><?=$bill->invoicing_basis?></h3>
            </div><!-- /title -->

            <?php
                $is_discount = false;
                foreach ($bill_items as $bill_item) 
                {
                    if ($bill_item->discount > 0)
                    {
                        $is_discount = true;
                        break;
                    }
                }
            ?>
            <div class="content">
                <table id="table">
                    <tr>
                        <th>Red.<br>Br</th>
                        <th>Naziv usluge / robe</th>
                        <th>Jed.<br>mere</th>
                        <th>Kol.</th>
                        <th>Jedinična<br>cena</th>
                        <th>Poreska<br> osnovica</th>
                        <?php if($is_discount) {echo '<th>Rabat %</th>';}?>
                        <th>PDV %</th>	
                        <th>Iznos PDV-a</th>
                        <th>Vrednost<br>sa PDV-om</th>	
                    </tr>
                    <?php
                        $value_per_unit_sum = 0;
                        $value_sum = 0;
                        $value_vat_sum = 0;
                        $value_total_sum = 0;
                        foreach ($bill_items as $bill_item) 
                        {
                            $value_per_unit_sum += $bill_item->value_per_unit;
                            $value_sum += $bill_item->value;
                            $value_vat_sum += $bill_item->value_vat;
                            $value_total_sum += $bill_item->value_total;
                    ?>
                            <tr class="avoid-break">
                                <td class="order-column"><?=$bill_item->order?></td>
                                <td class="title-column"><?=$bill_item->DisplayName?></td>
                                <td><?=!empty($bill_item->unit) ? $bill_item->unit->name : ''?></td>
                                <td><?=SIMAHtml::number_format($bill_item->quantity)?></td>
                                <td style="text-align:right;"><?=SIMAHtml::number_format($bill_item->value_per_unit)?></td>
                                <td style="text-align:right;"><?=SIMAHtml::number_format($bill_item->value)?></td>
                                <?php if($is_discount) {echo '<td style="text-align:right;">' . SIMAHtml::number_format($bill_item->discount) . '</td>';}?>          
                                <td style="text-align:right;"><?=$bill_item->vat?></td>	
                                <td style="text-align:right;"><?=SIMAHtml::number_format($bill_item->value_vat)?></td>	
                                <td style="text-align:right;"><?=SIMAHtml::number_format($bill_item->value_total)?></td>
                            </tr>
                    <?php 
                        } 
                    ?>
                    <tr class="avoid-break">
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th style="text-align:right;"><?=SIMAHtml::number_format($value_per_unit_sum)?></th>
                        <th style="text-align:right;"><?=SIMAHtml::number_format($value_sum)?></th>
                        <?php if($is_discount) {echo '<th></th>';}?>
                        <th></th>
                        <th style="text-align:right;"><?=SIMAHtml::number_format($value_vat_sum)?></th>        
                        <th style="text-align:right;"><?=SIMAHtml::number_format($value_total_sum)?></th>
                    </tr>
                </table>
                <div class="balance avoid-break">
                    <table id="balance-table">
                        <tr>
                            <td>Ukupno</td>
                            <td><?=SIMAHtml::number_format($bill->base_amount)?></td>
                        </tr>
                        <tr>
                            <td>PDV</td>
                            <td><?=SIMAHtml::number_format($bill->vat)?></td>
                        </tr>
                        <tr>
                            <td>Ukupno sa PDV</td>
                            <td><?=SIMAHtml::number_format($bill->amount)?></td>
                        </tr>
                        <?php if (count($bill->advance_releases) > 0) {?>
                            <tr>
                                <td>Umanjenje iz avansa</td>
                                <td><?=SIMAHtml::number_format($bill->advance_released_amount_sum)?></td>
                            </tr>
                        <?php }?>
                        <tr>
                            <td>Ukupno za plaćanje</td>
                            <td><?=SIMAHtml::number_format($bill->unreleased_amount)?></td>
                        </tr>
                    </table>
                </div><!-- /balance -->

                <div class="tax avoid-break">
                    <table id="tax-table">
                        <tr>
                            <th></th>
                            <th>Osnovica</th>
                            <th>PDV</th>
                            <th>Ukupno</th>
                        </tr>
                        <tr>
                            <td>0%</td>
                            <td><?=SIMAHtml::number_format($bill->base_amount0)?></td>
                            <td><?=SIMAHtml::number_format(0)?></td>
                            <td><?=SIMAHtml::number_format($bill->base_amount0)?></td>
                        </tr>
                        <tr>
                            <td>10%</td>
                            <td><?=SIMAHtml::number_format($bill->base_amount10)?></td>
                            <td><?=SIMAHtml::number_format($bill->vat10)?></td>
                            <td><?=SIMAHtml::number_format($bill->total_amount10)?></td>
                        </tr>
                        <tr>
                            <td>20%</td>
                            <td><?=SIMAHtml::number_format($bill->base_amount20)?></td>
                            <td><?=SIMAHtml::number_format($bill->vat20)?></td>
                            <td><?=SIMAHtml::number_format($bill->total_amount20)?></td>
                        </tr>
                    </table>

                </div><!-- .tax -->
                
                <?php if (count($bill->advance_releases) > 0) {?>
                <div class="clear"></div>
                
                <div class="advance_release avoid-break">
                    <p>Specifikacija umanjenja po avansnim računima</p>
                    <table id="advance_release-table">
                        <tr>
                            <th>Broj računa</th>
                            <th>Osnovica</th>
                            <th>PDV</th>
                            <th>Ukupno</th>
                        </tr>
                        <?php foreach ($bill->advance_releases as $adv_release){ ?>
                            <tr>
                                <td><?=$adv_release->advance_bill->DisplayName?></td>
                                <td><?=SIMAHtml::number_format($adv_release->base_amount)?></td>
                                <td><?=SIMAHtml::number_format($adv_release->vat_amount)?></td>
                                <td><?=SIMAHtml::number_format($adv_release->amount)?></td>
                            </tr> 
                        <?php }?>
                        <tr>
                            <td>&Sigma;</td>
                            <td><?=SIMAHtml::number_format($bill->advance_released_base_amount_sum)?></td>
                            <td><?=SIMAHtml::number_format($bill->advance_released_vat_sum)?></td>
                            <td><?=SIMAHtml::number_format($bill->advance_released_amount_sum)?></td>
                        </tr> 
                        
                        
                    </table>

                </div><!-- .advences -->
                
                <?php } ?>
                <div class="clear"></div>
                <div class="note clear avoid-break">
                    <p><span>Slovima:</span> <?= SIMAHtml::money2letters($bill->unreleased_amount, 'lat'); ?></p>
                    <?php if ( $bill->days_for_payout > 0 ){?>
                        <p><span>Uslovi plaćanja:</span> u roku od <?= $bill->days_for_payout?> dana</p>
                    <?php }?>
                    <!--Treba da se preradi, postoji zahtev za to-->
                    <!--<p><span>Napomena o poreskom oslobođenju:</span> nema</p>-->
                    <p>Društvo <?=$bill->company_vat_info_for_template?> u sistemu PDV.</p>
                    <?= !empty($bill->note_of_tax_exemption) ? ("<p><b>" . Yii::t('AccountingModule.NoteTaxExemption', 'NoteOfTaxExemption') . ":</b> " . $bill->note_of_tax_exemption->text_of_note . "</p>") : ""?>
                    <?php if(!empty($responsible_person)) { ?>
                        <p>Odgovorno lice za izdavanje računovodstvene isprave <?=$responsible_person->DisplayName?></p>
                    <?php } ?>
                    <p><?=$bill_additional_info?></p>
                    <?php
                        if (!empty($bill->invoice_comment))
                        {
                            ?>
                            <p>
                                Posebne napomene: <br />
                                <?=nl2br($bill->invoice_comment)?>
                            </p>
                            <?php
                        }
                    ?>
                    <p>Prilikom plaćanja računa pozovite se na broj <?=$call_to_number?></p>
                    <?php
                        if (!empty($bill_bank_accounts))
                        { 
                            echo "Tekući računi za uplatu: <br />";
                            foreach ($bill_bank_accounts as $bill_bank_account) 
                            {
                                echo "{$bill_bank_account->number} <br />";
                            }
                        }
                    ?>
                </div><!-- .note -->

                <div class="avoid-break">
                    <div class="signatures _left">
                        <span>&nbsp;</span>
                        <span>__________________________________</span>
                        <span>Fakturisao</span>
                    </div><!-- /signatures -->

                    <div class="signatures _middle">
                        M.P.
                    </div>

                    <div class="signatures _right">
                        <span>&nbsp;</span>
                        <span>__________________________________</span>
                        <span>Primio</span>
                    </div>
                    <div class="clear"></div>
                </div>
            </div><!-- /content -->
        </div> <!-- /main -->

    </div><!-- /container -->
</page>