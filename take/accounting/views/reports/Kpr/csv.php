<?php
    echo "1#2#3#4#5#6#7#8#9#9a#10#11#12#13#14#15#16#17#18#19";
    echo "\n";
    
    $i=1; 

    foreach ($rows as $row)
    {
        echo "#" . $row['order_in_year'] /// 1
            . "#" .$row['booking_date'] /// 2
            . "#" /// 3
            . "#" . $row['bill_number'] /// 4
            . "#" . $row['income_date'] /// 5
            . "#" . $row['partner_display_name'] /// 6
            . "#" . $row['partner_pib_jmbg'] /// 7
            . "#" . $row['KPR_8']  /// 8
            . "#" . $row['KPR_9']  /// 9
            . "#" . $row['KPR_9a']  /// 9a
            . "#" . $row['KPR_10'] /// 10
            . "#" . $row['KPR_11'] /// 11
            . "#" . $row['KPR_12'] /// 12
            . "#" . $row['KPR_13'] /// 13
            . "#" . $row['KPR_14'] /// 14
            . "#" . $row['KPR_15'] /// 15
            . "#" . $row['KPR_16'] /// 16
            . "#" . $row['KPR_17'] /// 17
            . "#" . $row['KPR_18'] /// 18
            . "#" . $row['KPR_19'] . "\n"; /// 19
    }
    
    if(isset($sum_row) && count($sum_row) > 0)
    {
        echo '#######';
        for($i=8; $i<19; $i++)
        {
            echo '#' . $sum_row["KPR_" . ($i+1)];
        }
    }
?>