<page size="A4">
<p>Knjiga primljenih računa za mesec <?php echo $month_disp;?></p>
<p>Štampao: <?php echo Yii::app()->user->model->DisplayName; ?></p>
<p>Datuma: <?php echo SIMAHtml::DbToUserDate(SIMAHtml::currentDateTime(false)); ?></p><br>

<table class="KPR Report">
    <thead>
        <tr>
            <th rowspan='3'>Redni broj</th>
            <th rowspan='3'>Datum knjiženja isprave</th>
            <th rowspan='3'>Datum plaćanja pri uvozu i plaćanja naknada poljoprivredniku</th>
            <th colspan='10'>RAČUN ILI DRUGI DOKUMENT</th>
            <th rowspan='3'>Ukupan iznos obračunatog prethodnog PDV tač.17</th>
            <th rowspan='3'>Iznos prethodnog PDV koji se može odbiti</th>
            <th rowspan='3'>Iznos prethodnog PDV koji se ne može odbiti</th>
            <th colspan='2' rowspan='2'>Uvoz</th>
            <th colspan='2' rowspan='2'>Naknada poljoprivredniku</th>
	</tr>
	<tr>
            <th rowspan='2'>Broj računa</th>
            <th rowspan='2'>Datum izdavanja računa (ili drugog dokumenta)</th>
            <th colspan='2'>Dobavljač</th>
            <th rowspan='2'>Ukupna naknada sa PDV tač.16</th>
            <th rowspan='2'>Naknada bez PDV(na koju je obračunat PDV koji se može odbiti)</th>
            <th rowspan='2'>Naknada bez PDV(na koju je obračunat PDV koji se ne može odbiti)</th>
            <th rowspan='2'>Oslobođene nabavke tač.18</th>
            <th rowspan='2'>Nabavka od lica koja nisu obveznici PDV tač.15</th>
            <th rowspan='2'>Naknada za uvezena dobra na koje se ne plaća PDV tač.22</th>
	</tr>
	<tr>
            <th>Naziv (ime i sedište)</th>
            <th>PIB ili JMBG</th>
            <th>Vrednost dobara bez PDV tač.21</th>
            <th>Iznos PDV tač. 23</th>
            <th>Vrednost primljenih dobara i usluga</th>
            <th>Iznos naknade od 5% tač.24</th>
	</tr>
	<tr>
            <?php for($i=1; $i<20; $i++) {
                if ($i === 10)
                {
                    echo "<td>9a</td>";
                }

                echo "<td>$i</td>";
            }?>
	</tr>
    </thead>
    <tbody>
	<?=$html_rows?>
    </tbody>
    <?=$html_sum_rows?>
</table>
</page>