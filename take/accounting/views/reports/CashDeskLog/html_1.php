<page size="A4">
<div id="container">
    <header>
        <img src='<?= $curr_company->getLogoImagePath() ?>' />
        <div class="content">
            <ul>
                <li><?= $curr_company->DisplayName ?></li>
                <li><?= !empty($curr_company->partner) ? $curr_company->partner->DisplayMainAddress : '' ?></li>
                <li>
                    <span>PIB: </span><?= $curr_company->PIB ?> |
                    <span>Matični broj: </span><?= $curr_company->MB ?> |
                    <span>Šifra delatnosti: </span><?= !empty($curr_company->work_code) ? $curr_company->work_code->code : ''; ?>
                </li>
            </ul>
        </div>
    </header>

    <div class="main">
        <div class="title">
            <h1>DNEVNIK BLAGAJNE <span>blagajnički izveštaj</span></h1>
            <p>Od: <span><?=SIMAHtml::DbToUserDate($cash_desk_log->date)?></span></p>
            <div style="clear: both;"></div>
        </div><!-- /title -->
        
        <div class="content">
            <table id="table">
                <tr>
                    <th>Red.<br>Br</th>
                    <th colspan="2">Temeljnica</th>
                    <th>Opis</th>
                    <th>Ulaz</th>
                    <th>Izlaz</th>
                    <th>Broj<br> računa</th>
                </tr>
                <?=$html?>
            </table>

            <table style="margin-top: 5.3mm; margin-left: 8.5mm; <?=$style_tax_table?>" class="tax-table">
                <tr>
                    <th>Komada</th>
                    <th>Apoen</th>
                    <th>Ukupno</th>
                </tr>
                <?php $denominations = array('5000','2000','1000','500','200','100','50','20','10','5','2','1');?>
                <?php $i=0; foreach($denominations as $denomination){ $i++ ?>
                <tr>
                        <td></td>
                        <td><?=$denomination?></td>
                        <td></td>
                </tr>
                <?php }?>
            </table>
            <br>
            <table class="tax-table second">
                <tr>
                    <th>Promet blagajne</th>
                    <th>Iznos</th>
                </tr>
                <tr>
                    <td>Saldo od <?=SIMAHtml::DbToUserDate($date_preview)?></td>
                    <td><?=SIMAHtml::number_format($cash_desk_log->previous_saldo)?></td>
                </tr>
                <tr>
                    <td>Ukupni Primitak</td>
                    <td><?=$amount_in_sum?></td>
                </tr>
                <tr>
                    <td>Odbija se izdatak</td>
                    <td><?=$amount_out_sum?></td>
                </tr>
                <tr>
                    <td>Saldo od <?=SIMAHtml::DbToUserDate($cash_desk_log->date)?></td>
                    <td><?=SIMAHtml::number_format($cash_desk_log->saldo)?></td>
                </tr>
            </table>

            <div class="box">Broj priloga: <?=$account_number?></div>
            
            <table class="sum">
                <tr>
                    <td style="width: 26.5mm;">SLOVIMA:</td>
                    <td style="border-bottom: 0.3mm solid #000;"><?=SIMAHTML::money2letters($cash_desk_log->saldo, 'lat')?></td>
                </tr>
            </table>

            <div class="clear"></div>
            
            <div class="signatures">
                <span>Kontrolisao</span>
                <span>__________________________________</span>
            </div><!-- /signatures -->
            
            <div class="signatures">
                <span>Blagajnik</span>
                <span>__________________________________</span>
            </div><!-- /signatures -->
        </div><!-- /content -->
    </div> <!-- /main -->
</div><!-- /container -->
</page>



