<table class="table-1" cellspacing="0" cellpadding="4">
    <tr>
        <td width="35%" rowspan="2">
            <div class="company-logo">
                <img src="<?=$company_logo_path?>"/>
            </div>
            <div class="company-info">
                <?=Yii::app()->company->DisplayName?> <br />
                PIB: <?=Yii::app()->company->getAttributeDisplay('PIB')?>
            </div>
        </td>
        <td width="40%" rowspan="2" class="align-center">
            <strong>NALOG ZA KNJIŽENJE</strong>
            <?php if (!empty($account_order->note)){?>
                <p>Beleška: <?=$account_order->note?></p>
            <?php }?>
        </td>
        <td width="25%">
            Broj naloga: <strong><?=$account_order->getAttributeDisplay("order")?></strong>
        </td>
    </tr>
    <tr>
        <td>
            Datum: <strong><?=$account_order->getAttributeDisplay("date")?></strong>
        </td>
    </tr>
</table>

<table class="table-2" cellspacing="0" cellpadding="4">
    <tr class="header">
        <td width="5%" class="align-center"><strong>Br.</strong></td>        
        <td width="<?=$account_column_width?>"><strong>Konto duguje</strong></td>
        <td width="<?=$account_column_width?>"><strong>Konto potražuje</strong></td>
        <td width="<?=$comment_column_width?>"><strong>Opis</strong></td>
        <td width="15%" class="align-right"><strong>Duguje</strong></td>
        <td width="15%" class="align-right"><strong>Potražuje</strong></td>
        <td width="15%" class="align-right"><strong>Saldo</strong></td>
    </tr>
    <?=$html?>      
    <tr class="footer">
        <td colspan="3" class="plain"></td>
        <td>Ukupno:</td>
        <td class="align-right"><?=$account_order->getAttributeDisplay("debit")?></td>
        <td class="align-right"><?=$account_order->getAttributeDisplay("credit")?></td>
        <td class="align-right"><?=$account_order->getAttributeDisplay("saldo")?></td>
    </tr>    
</table>

<br><br>

<table class="table-3" cellspacing="0" cellpadding="4">
    <tr>
        <td>
            Status naloga: <?=$account_order->getAttributeDisplay("isBooked_status")?>
        </td>
    </tr>
    <tr>
        <td>
            Nalog potvrdio: <?=$account_order->getAttributeDisplay("bookerName")?>
        </td>
    </tr>
    <?php if ($show_booked_timestamp) { ?>
        <tr>
            <td>
                Datum potvrđivanja: <?=$account_order->getAttributeDisplay("bookedTimestamp")?>
            </td>
        </tr>
    <?php } ?>
    <tr>
        <td>
            Datum generisanja dokumenta: <?=$account_order->getAttributeDisplay("pdf_generation_time")?>
        </td>
    </tr>
</table>