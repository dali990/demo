
<div id="panel_<?=$uniq?>" class="sima-layout-panel _splitter _vertical">
    <div class='sima-layout-panel left_side' data-sima-layout-init='{"proportion":0.3,"min_width":300}'>
        <?php
            $this->widget('base.extensions.SIMAGuiTable.SIMAGuiTable', [
                'id' => 'account_layouts_laws'.$uniq,
                'model' => AccountLayoutLaw::model(),
                'add_button' => true,
                'custom_buttons' => [
                    Yii::t('BaseModule.Misc','TakeDataFromCodeBook') => [
                        'func' => 'sima.adminMisc.chooseAccountLayoutLawsFromCodebook(this)',
                    ],
                ],
                'setRowSelect' => "on_select$uniq",
                'setRowUnSelect' => "on_unselect$uniq",
            ]);
        ?>
    </div>

    <div class='sima-layout-panel right_side' style="display: none;" data-sima-layout-init='{"proportion":0.7,"min_width":300}'>
        <?php
            $this->widget('base.extensions.SIMAGuiTable.SIMAGuiTable', [
                'id' => 'account_layouts'.$uniq,
                'model' => AccountLayout::model(),
                'add_button' => false,
                'fixed_filter' => ['ids'=>-1],
                'custom_buttons'=>[
                    'Uvezi kontni okvir'=>[
                        'func'=>"sima.accountingMisc.importAccountLayoutsType('account_layouts$uniq')"
                    ],
                    'Izvezi kontni okvir'=>[
                        'func'=>"sima.accountingMisc.exportAccountLayouts('account_layouts$uniq')"
                    ]
                ],
            ]);
        ?>
    </div>
   
</div>


<script>
    function on_select<?=$uniq?>(row)
    {
        $('#account_layouts<?=$uniq?>')
                .data('law_id', row.attr('model_id'))
                .simaGuiTable('setFixedFilter', {
                        law: {
                            ids:row.attr('model_id')
                        }
                })
                .simaGuiTable('setAddButton',{
                    init_data:{
                        AccountLayout: {
                            law_id: {
                                disabled:true,
                                init:row.attr('model_id')
                            }
                        }
                    }
                })
                .simaGuiTable('populate');
        var _right_side = $('#panel_<?=$uniq?>').find('.right_side');
        _right_side.show();
        sima.layout.allignObject(_right_side,'TRIGGER_account_layout_right_side');
    }
    function on_unselect<?php echo $uniq; ?>(row)
    {
        $('#panel_<?=$uniq?>').find('.right_side').hide();
        $('#account_layouts<?=$uniq?>')
                .data('law_id', null)
                .simaGuiTable('setFixedFilter', {
                        law: {
                            ids: -1
                        }
                })
                .simaGuiTable('setAddButton',false)
                .simaGuiTable('populate');
    }
</script>