<?php
    if (Yii::app()->request->isAjaxRequest) Yii::app()->clientScript->clearScriptFiles();
?>
<?php $uniq = SIMAHtml::uniqid(); ?>
<div id='panel_<?=$uniq?>' class='sima-layout-panel _vertical _splitter'>

    <div class='sima-layout-panel left_side' data-sima-layout-init='{"proportion":0.35,"max_width":650}'>
        <?php
            $this->widget('base.extensions.SIMAGuiTable.SIMAGuiTable', [
                'id' => 'orders'.$uniq,
                'model' => AccountOrder::model(),                
                'add_button' => true,
                'setRowSelect' => "on_select$uniq",                
                'setRowUnSelect' => "on_unselect$uniq",
                'setMultiSelect' => "on_unselect$uniq",
                'select_filter'=>array(
                    'year' => array(
                        'ids' => $year_id
                    ),
                ),
            ]);
        ?>
    </div>


    <div class='sima-layout-panel right_side' data-sima-layout-init='{"proportion":0.65}'>
        <?php
            $id_tab = SIMAHtml::uniqid();
            $this->widget('ext.SIMATabs.SIMATabs', array(
                'id' => $id_tab,
                'list_tabs_model_name' => 'AccountDocument',
                'default_selected' => 'transactions'                
            ));
        ?>
    </div>   
   
</div>

<script type='text/javascript'>
    function on_select<?php echo $uniq; ?>(obj)
    {
        sima.ajax.get('accounting/books/getAccountDocumentIdFromAccountOrder',{
            get_params:{
                account_order_id:obj.attr('model_id')
            },
            success_function:function(response){
                if (!sima.isEmpty(response.account_document_id))
                {
                    $('#panel_<?php echo $uniq; ?>').find('.right_side').show();
                    $('#<?php echo $id_tab; ?>').simaTabs('set_list_tabs_params',{
                        list_tabs_trigger_tag: sima.getTag('<?php echo SIMAHtml::getTag(AccountDocument::model()); ?>', response.account_document_id),
                        list_tabs_get_params:{id:response.account_document_id}
                    });
                }
            }
        });
    }
    function on_unselect<?php echo $uniq; ?>(obj)
    {   
        $('#panel_<?php echo $uniq; ?>').find('.right_side').hide();
    }
</script>

<?php
    if (Yii::app()->request->isAjaxRequest) Yii::app()->clientScript->clearScriptFiles();
?>