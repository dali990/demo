<div class="sima-layout-panel _horizontal _splitter">
    <div class="sima-layout-fixed-panel">
        <?=$this->renderModelView($model->file,'book_this_document')?>
    </div>
    <div class="sima-layout-panel">
        <?php 
            $this->widget('SIMAGuiTable',[
                'model' => 'Advance',
                'title' => Yii::t('AccountingModule.AccountOrder','AccountOrdersOfConnectedPayments'),
                'columns_type' => 'booking_advance_bill',
                'fixed_filter' => [
                    'bill_id' => $model->id
                ]
            ]);
        ?>
    </div>
</div>

