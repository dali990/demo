

<div class="sima-layout-panel _horizontal">
    <div class="sima-layout-fixed-panel" style="min-height: 60px;">
        
        <?php $uniq = SIMAHtml::uniqid();?>

        <accounting-account_document_view-account_order_span id='<?=$uniq?>' v-bind:model='account_document'>
        </accounting-account_document_view-account_order_span>

        <?php
//            echo $this->renderModelView($model, 'account_order_span');
//            echo $this->renderModelView($model, 'canceled_account_order_span');
        ?> 
    </div>
    <div class="sima-layout-panel">
        <?php    
            $this->widget('SIMATabs',[
                'list_tabs_model' => $model,
                'default_selected' => $default_selected
            ]);

        ?>
    </div>

</div>

<script type='text/javascript'>

    new Vue({
        el: '#<?=$uniq?>',
        data: {
            account_document:sima.vue.shot.getters.model('<?=SIMAMisc::getVueModelTag($model)?>')
        }
    });

</script>