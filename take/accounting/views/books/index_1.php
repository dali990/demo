<?php $uniq = SIMAHtml::uniqid();?>
    
<?php    
$this->widget('SIMAGuiTable', [
    'id'=>'transaction'.$uniq,
    'class' => '_low_profile_rows',
    'model'=> AccountTransaction::model(),
    'add_button'=>false,
//            'add_button' => array(
//                'init_data'=>array(
//                    'AccountTransaction' => array(
//                        'account_id' => array(
//                            'init' => $model->id,
//                            'model_filter' => array(
//                                'year'=>['ids' => $model->year_id]
//                            )
//                        )
//                    ),
//                ),
//                'inline'=>true
//            ),
    'fixed_filter'=>array(
        'account' => array(
            'year'=> [
                'ids' => $model->year_id
            ]
        ),
        'account_booked' => array(
            'year'=> [
                'ids' => $model->year_id
            ]
        ),
        'display_scopes' => array(
            'withSaldoAndNoStart'
        )
    ),
    'select_filter'=>array(
        'account_booked' =>  ['ids' => $model->id],
        'account_document' => [
            'account_order' => [
                'date' => $daterange
            ]
        ]
    ),
    'columns_type' => 'account_card',
    'custom_buttons' => [
//                'Uračunaj start'=>[
//                    'func'=>"sima.accountingMisc.showAccountStart('transaction$uniq',$model->year_id,$model->id,$(this));"                                
//                ],
        'Pdf'=>[
            'func'=>"sima.accountingMisc.exportAccountToPdf('transaction$uniq');"                                
        ]
    ],
    //MilosS: dodao export i sad je u stvari default
//    'headerOptionRows' => [
//        ['table_settings','show_add_button','pagination','filters_in','export','custom_buttons','filters_out']
//    ]
]);
