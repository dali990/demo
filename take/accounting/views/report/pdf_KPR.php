<style type='text/css'>
	table
	{
		border-collapse:collapse;
	}
	table.KPR, table.KPR th, table.KPR td
	{
		border: 1px solid black;
	}
</style>


<table class="KPR Report">
    <thead>
        <tr>
		<th rowspan='3'>Redni broj</th>
		<th rowspan='3'>Datum knjiženja isprave</th>
		<th rowspan='3'>Datum plaćanja pri uvozu i plaćanja naknada poljo- privre- dniku</th>
		<th colspan='10'>RAČUN ILI DRUGI DOKUMENT</th>
		<th rowspan='3'>Ukupan iznos obračunatog prethodnog PDV tač.17</th>
		<th rowspan='3'>Iznos prethodnog PDV koji se može odbiti</th>
		<th rowspan='3'>Iznos prethodnog PDV koji se ne može odbiti</th>
		<th colspan='2' rowspan='2'>Uvoz</th>
		<th colspan='2' rowspan='2'>Naknada poljoprivredniku</th>
	</tr>
	<tr>
<!-- 		<th rowspan='3'>Redni broj</th> -->
<!-- 		<th>Datum knjiženja isprave</th> -->
<!-- 		<th>Datum plaćanja pri uvozu i plaćanja naknada poljoprivredniku</th> -->
<!-- 		<th colspan='9'>RAČUN ILI DRUGI DOKUMENT</th> -->
					<th rowspan='2'>Broj računa</th>
					<th rowspan='2'>Datum izdavanja računa (ili drugog dokumenta)</th>
					<th colspan='2'>Dobavljač</th>
					<th rowspan='2'>Ukupna naknada sa PDV tač.16</th>
					<th rowspan='2'>Naknada bez PDV(na koju je obračunat PDV koji se može odbiti)</th>
					<th rowspan='2'>Naknada bez PDV(na koju je obračunat PDV koji se ne može odbiti)</th>
					<th rowspan='2'>Oslobođene nabavke tač.18</th>
					<th rowspan='2'>Nabavka od lica koja nisu obveznici PDV tač.15</th>
					<th rowspan='2'>Naknada za uvezena dobra na koje se ne plaća PDV tač.22</th>
<!-- 		<th rowspan='3'>Ukupan iznos obračunatog prethodnog PDV tač.17</th> -->
<!-- 		<th rowspan='3'>Iznos prethodnog PDV koji se može odbiti</th> -->
<!-- 		<th rowspan='3' rowspan='2'>Iznos prethodnog PDV koji se ne može odbiti</th> -->
<!-- 		<th colspan='2' rowspan='2'>Redni broj</th> -->
<!-- 		<th colspan='2'>Naknada poljoprivredniku</th> -->
	</tr>
	<tr>
<!-- 		<th rowspan='3'>Redni broj</th> -->
<!-- 		<th>Datum knjiženja isprave</th> -->
<!-- 		<th>Datum plaćanja pri uvozu i plaćanja naknada poljoprivredniku</th> -->
<!-- 		<th colspan='9'>RAČUN ILI DRUGI DOKUMENT</th> -->
<!-- 					<th rowspan='2'>Broj računa</th> -->
<!-- 					<th rowspan='2'>Datum izdavanja računa (ili drugog dokumenta)</th> -->
<!-- 					<th colspan='2'>Dobavljač</th> -->
								<th>Naziv (ime i sedište)</th>
								<th>PIB ili JMBG</th>
<!-- 					<th rowspan='2'>Ukupna naknada sa PDV tač.16</th> -->
<!-- 					<th rowspan='2'>Naknada bez PDV(na koju je obračunat PDV koji se može odbiti)</th> -->
<!-- 					<th rowspan='2'>Oslobođene nabavke tač.18</th> -->
<!-- 					<th rowspan='2'>Nabavka od lica koja nisu obveznici PDV tač.15</th> -->
<!-- 					<th rowspan='2'>Naknada za uvezena dobra na koje se ne plaća PDV tač.22</th> -->
<!-- 		<th rowspan='3'>Ukupan iznos obračunatog prethodnog PDV tač.17</th> -->
<!-- 		<th rowspan='3'>Iznos prethodnog PDV koji se može odbiti</th> -->
<!-- 		<th rowspan='3' rowspan='2'>Iznos prethodnog PDV koji se ne može odbiti</th> -->
<!-- 		<th colspan='2' rowspan='2'>Redni broj</th> -->
					<th>Vrednost dobara bez PDV tač.21</th>
					<th>Iznos PDV tač. 23</th>
<!-- 		<th colspan='2'>Naknada poljoprivredniku</th> -->
					<th>Vrednost primljenih dobara i usluga</th>
					<th>Iznos naknade od 5% tač.24</th>
	</tr>
	<tr>
		<?php for($i=1;$i<20;$i++) {
                    if ($i === 10)
                    {
                        echo "<td>9a</td>";
                    }
                    
                    echo "<td>$i</td>";
                    
                }?>
	</tr>
    </thead>
    <tbody>
	<?php $i=1; foreach ($rows as $row){?>
		<tr  class="<?=$row['row_model']->getClasses()?>">
                    <td><?php 1;echo $row['order_in_year']; ?></td>
                    <td><?php 2;echo $row['booking_date']; ?></td>
                    <td><?php 3;/*not suported*/?></td>
                    <td><?php 4;echo $row['bill_number']; ?></td>
                    <td><?php 5;echo $row['income_date']; ?></td>
                    <td><?php 6;echo $row['partner_display_name']; ?></td>
                    <td><?php 7;echo $row['partner_pib_jmbg']; ?></td>
                    <td style="text-align: right;"><?php 8;echo $row['KPR_8']?></td>
                    <td style="text-align: right;"><?php 9;echo $row['KPR_9']?></td>
                    <td style="text-align: right;"><?php /*9a*/;echo $row['KPR_9a']?></td>
                    <td style="text-align: right;"><?php 10;echo $row['KPR_10']?></td>
                    <td style="text-align: right;"><?php 11;echo $row['KPR_11']?></td>
                    <td style="text-align: right;"><?php 12;echo $row['KPR_12']?></td>
                    <td style="text-align: right;"><?php 13;echo $row['KPR_13']?></td>
                    <td style="text-align: right;"><?php 14;echo $row['KPR_14']?></td>
                    <td style="text-align: right;"><?php 15;echo $row['KPR_15']?></td>
                    <td style="text-align: right;"><?php 16;echo $row['KPR_16']?></td>
                    <td style="text-align: right;"><?php 17;echo $row['KPR_17']?></td>
                    <td style="text-align: right;"><?php 18;echo $row['KPR_18']?></td>
                    <td style="text-align: right;"><?php 19;echo $row['KPR_19']?></td>
		</tr>
	<?php }?>
    </tbody>
    <tfoot>
        <tr>
            <td colspan="7" style="text-align: center;">&sum;</td>

            <td style="text-align: right;"><?php 8;echo $sum_row['KPR_8']?></td>
            <td style="text-align: right;"><?php 9;echo $sum_row['KPR_9']?></td>
            <td style="text-align: right;"><?php /*9a*/;echo $sum_row['KPR_9a']?></td>
            <td style="text-align: right;"><?php 10;echo $sum_row['KPR_10']?></td>
            <td style="text-align: right;"><?php 11;echo $sum_row['KPR_11']?></td>
            <td style="text-align: right;"><?php 12;echo $sum_row['KPR_12']?></td>
            <td style="text-align: right;"><?php 13;echo $sum_row['KPR_13']?></td>
            <td style="text-align: right;"><?php 14;echo $sum_row['KPR_14']?></td>
            <td style="text-align: right;"><?php 15;echo $sum_row['KPR_15']?></td>
            <td style="text-align: right;"><?php 15;echo $sum_row['KPR_16']?></td>
            <td style="text-align: right;"><?php 15;echo $sum_row['KPR_17']?></td>
            <td style="text-align: right;"><?php 15;echo $sum_row['KPR_18']?></td>
            <td style="text-align: right;"><?php 15;echo $sum_row['KPR_19']?></td>
        </tr>
    </tfoot>
</table>