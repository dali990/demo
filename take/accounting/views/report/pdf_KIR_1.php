<style type='text/css'>
	table
	{
		border-collapse:collapse;
	}
	table.KIR, table.KIR th, table.KIR td
	{
		border: 1px solid black;
	}
</style>


<table class="KIR Report">
    <thead>
	<tr>
		<th rowspan='3'>Redni broj</th>
		<th rowspan='3'>Datum knjiženja računa</th>
		<th colspan='5'>RAČUN ILI DRUGI DOKUMENT</th>
		<th colspan='4'>Oslobođen promet</th>
		<th colspan='4'>Oporezivi promet</th>
		<th rowspan='3'>Ukupan promet dobara i usluga sa pravom i bez prava na odbitak prethodnog poreza bez PDV(8 do 11+12+14)</th>
		<th rowspan='3'>Promet dobara i usluga sa pravom na odbitak prethodnog poreza bez PDV(8+10+12+14)</th>
                <th rowspan='3'>Prihod</th>
	</tr>
	<tr>
<!-- 		<th rowspan='3'>Redni broj</th> -->
<!-- 		<th rowspan='3'>Datum knjiženja računa</th> -->
<!-- 		<th colspan='5'>RAČUN ILI DRUGI DOKUMENT</th> -->
					<th rowspan='2'>Broj računa</th>
					<th rowspan='2'>Datum izdavanja računa(ili drugog dokumenta)</th>
					<th colspan='2'>KUPAC</th>
					<th rowspan='2'>Ukupna naknada sa PDV</th>
<!-- 		<th colspan='4'>Oslobođen promet</th> -->
					<th rowspan='2'>Oslobođen promet sa pravom na odbitak prethodnog poreza(čl.24 Zakona)</th>
					<th rowspan='2'>Oslobođen promet bez prava na odbitak prethodnog poreza(čl.25 Zakona)</th>
					<th colspan='2'>Promet u inostranstvu</th>
<!-- 		<th colspan='4'>Oporezivi promet</th> -->
					<th colspan='2'>Po stopi od 20%</th>
					<th colspan='2'>Po stopi od 10%</th>
<!-- 		<th rowspan='3'>Ukupan promet dobara i usluga sa pravom i bez prava na odbitak prethodnog poreza bez PDV(8 do 11+12+14)</th> -->
<!-- 		<th rowspan='3'>Promet dobara i usluga sa pravom na odbitak prethodnog poreza bez PDV(8+10+12+14)</th> -->
	</tr>
	<tr>
<!-- 		<th rowspan='3'>Redni broj</th> -->
<!-- 		<th rowspan='3'>Datum knjiženja računa</th> -->
<!-- 		<th colspan='5'>RAČUN ILI DRUGI DOKUMENT</th> -->
<!-- 					<th rowspan='2'>Broj računa</th> -->
<!-- 					<th rowspan='2'>Datum izdavanja računa(ili drugog dokumenta)</th> -->
<!-- 					<th colspan='2'>KUPAC</th> -->
							<th>Naziv (ime i sedište)</th>
							<th>PIB ili JMBG</th>	
<!-- 					<th rowspan='2'>Ukupna naknada sa PDV</th> -->
<!-- 		<th colspan='4'>Oslobođen promet</th> -->
<!-- 					<th rowspan='2'>Oslobođen promet sa pravom na odbitak prethodnog poreza(čl.24 Zakona)</th> -->
<!-- 					<th rowspan='2'>Oslobođen promet bez prava na odbitak prethodnog poreza(čl.25 Zakona)</th> -->
<!-- 					<th colspan='2'>Promet u inostranstvu</th> -->
							<th>za koji bi postojalo pravo na prethodni porez da je promet izvršen u zemlji</th>
							<th>za koji ne bi postojalo pravo na prethodni porez da je promet izvršen u zemlji</th>	
<!-- 		<th colspan='4'>Oporezivi promet</th> -->
<!-- 					<th colspan='2'>Po stopi od 18%</th> -->
							<th>osnovica</th>
							<th>iznos PDV</th>	
<!-- 					<th colspan='2'>Po stopi od 8%</th> -->
							<th>osnovica</th>
							<th>iznos PDV</th>	
<!-- 		<th rowspan='3'>Ukupan promet dobara i usluga sa pravom i bez prava na odbitak prethodnog poreza bez PDV(8 do 11+12+14)</th> -->
<!-- 		<th rowspan='3'>Promet dobara i usluga sa pravom na odbitak prethodnog poreza bez PDV(8+10+12+14)</th> -->
	</tr>
	<tr>
		<?php for($i=1;$i<18;$i++) echo "<td>$i</td>"?>
	</tr>
    </thead>
    <tbody
	<?php $i=1; foreach ($rows as $row){?>
		<tr  class="<?=$row['row_model']->getClasses()?>">
                    <td><?php 1;echo $row['order_in_year']; ?></td>
                    <td><?php 2;echo $row['booking_date']; ?></td>
                    <td><?php 3;echo $row['bill_number']; ?></td>
                    <td><?php 4;echo $row['income_date']; ?></td>
                    <td><?php 5;echo $row['partner_display_name']; ?></td>
                    <td><?php 6;echo $row['partner_pib_jmbg']; ?></td>
                    <td style="text-align: right;"><?php 7;echo $row['KIR_7']?></td>
                    <td style="text-align: right;"><?php 8;echo $row['KIR_8']?></td>
                    <td style="text-align: right;"><?php 9;echo $row['KIR_9']?></td>
                    <td style="text-align: right;"><?php 10;echo $row['KIR_10']?></td>
                    <td style="text-align: right;"><?php 11;echo $row['KIR_11']?></td>
                    <td style="text-align: right;"><?php 12;echo $row['KIR_12']?></td>
                    <td style="text-align: right;"><?php 13;echo $row['KIR_13']?></td>
                    <td style="text-align: right;"><?php 14;echo $row['KIR_14']?></td>
                    <td style="text-align: right;"><?php 15;echo $row['KIR_15']?></td>
                    <td style="text-align: right;"><?php 16;echo $row['KIR_16']?></td>
                    <td style="text-align: right;"><?php 17;echo $row['KIR_17']?></td>
                    <td style="text-align: right;"><?php 18;echo isset($row['KIR_18'])?$row['KIR_18']:''?></td>
		</tr>
	<?php }?>
    </tbody>
    <tfoot>
        <tr>
            <td colspan="6" style="text-align: center;">&sum;</td>
            <td style="text-align: right;"><?php 7;echo $sum_row['KIR_7']?></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td style="text-align: right;"><?php 12;echo $sum_row['KIR_12']?></td>
            <td style="text-align: right;"><?php 13;echo $sum_row['KIR_13']?></td>
            <td style="text-align: right;"><?php 14;echo $sum_row['KIR_14']?></td>
            <td style="text-align: right;"><?php 15;echo $sum_row['KIR_15']?></td>
            <td style="text-align: right;"><?php 16;echo $sum_row['KIR_16']?></td>
            <td style="text-align: right;"><?php 17;echo $sum_row['KIR_17']?></td>
            <td style="text-align: right;"><?php 18;echo $sum_row['KIR_18']?></td>
        </tr>
    </tfoot>
</table>