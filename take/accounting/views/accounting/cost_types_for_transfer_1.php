
<div id="<?php echo $uniq_id; ?>" class="cost_types_for_transfer sima-layout-panel _horizontal">
    <div class="sima-layout-fixed-panel" style="padding: 0px 15px; font-size: 16px; font-weight: bold; margin-bottom: 15px;">
        Označite vrste troška koje hoćete da prebacite iz <?php echo $prev_year->year; ?> u <?php echo $curr_year->year; ?> godinu:
    </div>
    <div class="cost_types sima-layout-panel _horizontal">
        <div class="header sima-layout-fixed-panel">
            <div class="row">
                <span class="checkbox">
                    <input onclick="onInputAllChange<?php echo $uniq_id; ?>($(this))" type="checkbox" checked>
                </span>
                <span class="cost_type">Vrsta troška</span>
                <span class="cost_type">Konto</span>
            </div>
        </div>
        <div class="body sima-layout-panel">
            <?php
                foreach ($cost_types as $cost_type)
                {
                    ?>
                    <div class="row">
                        <span class="checkbox">
                            <input type="checkbox" checked>
                        </span>
                        <span class="cost_type" 
                              data-id='<?php echo $cost_type['cost_type_id']; ?>'
                              title="<?php echo $cost_type['cost_type_display_name']; ?>">
                                <?php
                                    echo $cost_type['cost_type_display_name'];
                                ?>
                        </span>
                        <span class="account"                             
                             data-id='<?php echo $cost_type['prev_account_id']; ?>'>
                                <?php
                                    echo $cost_type['prev_account_display_name'];
                                ?>
                        </span>
                    </div>
                    <?php
                }
            ?>
        </div>
    </div>
</div>

<script type='text/javascript'>
    
    function onInputAllChange<?php echo $uniq_id; ?>(_this)
    {
        if (_this.is(':checked'))
        {
            $('#<?php echo $uniq_id; ?>').find('.body .checkbox input').prop("checked", true);
        }
        else
        {
            $('#<?php echo $uniq_id; ?>').find('.body .checkbox input').prop("checked", false);
        }
    }
    
</script>