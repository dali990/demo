
<div id="<?php echo "choose_accounts_for_transactions_changes$uniq"; ?>" class="choose-accounts-for-transactions-changes">
    <div>Prekontiranje svih stavova sa konta 1 na konto 2:</div>
    <div class="account1 sima-model-form-row">
        <span>Konto 1</span>
        <?php
            $this->widget('ext.SIMASearchField.SIMASearchField', array(
                'id'=>"account1_$uniq",
                'model'=>'Account',
                'filters' => [
                    'year' => [
                        'ids' => $year_id
                    ],
                    'scopes' => ['onlyLeafAccounts']
                ]                
            ));
        ?>
    </div>
    <div class="account2 sima-model-form-row">
        <span>Konto 2</span>
        <?php
            $this->widget('ext.SIMASearchField.SIMASearchField', array(
                'id'=>"account2_$uniq",
                'model'=>'Account',
                'filters' => [
                    'year' => [
                        'ids' => $year_id
                    ],
                    'scopes' => ['onlyLeafAccounts']
                ]                
            ));
        ?>
    </div>
</div>

