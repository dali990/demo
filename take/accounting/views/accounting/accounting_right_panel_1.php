
<div id="<?php echo $uniq.'_right-panel'; ?>" class="sima-layout-panel" 
     data-year_id="<?php echo $year_id; ?>" 
     data-show_all_accounts='false'
     data-hide_zero_accounts='false'
     >
    <?php
        $table_id = $uniq.'_accounts';
        $this->widget('base.extensions.SIMAGuiTable.SIMAGuiTable', [
            'id' => $table_id,
            'model' => Account::model(),
            'columns_type' => 'byYear',
            'add_button' => array(
                'init_data'=>array(
                    'Account'=>array(
                        'year_id'=>array(
                            'disabled',
                            'init'=>$year_id
                        ),
                    )
                )
            ),
            'fixed_filter'=>array(
                'year'=>array(
                    'ids'=>$year_id
                ),
                'filter_scopes' => 'showShortAccountsView',
                'display_scopes' => [
                    'withDCCurrents' => [$year_id, ['hide_zero_accounts'=>false]],
                    'byName'
                ]
            ),
            'custom_buttons'=>[
                'Izvezi2'=>[
                    'func' => "sima.accounting.exportAccountsToPdf('$table_id');"
                ],
//                'Prikaži sve'=>[
//                    'func' => "sima.accountingMisc.showAllAccounts('$uniq"."_accounts',$('#$uniq"."_right-panel').data('year_id'),$(this));"
//                ]
            ],
        ]);               
    ?>
</div>

