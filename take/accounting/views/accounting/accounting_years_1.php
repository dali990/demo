
<?php
    $this->widget('base.extensions.SIMAGuiTable.SIMAGuiTable', [
        'model' => AccountingYear::model(),
        'add_button' => ['init_data' => [
            'AccountingYear' => [
                'ios_date' => "31.12.".date("Y.")
            ]
        ]],
        'title' => 'Finansijske godine'
    ]);
?>