
<div id="<?=$uniq?>">
    <div style="display: flex; margin-bottom: 10px;">
        <span style="width: 40%; text-align: right; padding-right: 20px;">Tip izveštaja:</span>
        <div style="width: 60%; text-align: left;">
            <?php
                echo SIMAHtml::dropDownList('gross_balance_export_type', 'PDF', [
                    'pdf' => 'pdf',
                    'csv' => 'csv',
                    'ods' => 'ods',
                    'xls' => 'xls'
                ], ['id' => "gross_balance_export_type_$uniq"]);
            ?>
        </div>
    </div>
    <div style="display: flex;">
        <span style="width: 40%; text-align: right; padding-right: 20px;">Datum:</span>
        <div style="width: 60%; text-align: left;">
            <?php
                echo SIMAHtml::datetimeFieldByName('date_for_gross_balance','', [
                    'id' => "gross_balance_date_$uniq",
                    'htmlOptions' => [
                        'placeholder' => 'Na dan'
                    ]
                ])
            ?>
        </div>
    </div>
</div>