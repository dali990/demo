<style type="text/css">
    table {
        border: 0.1em solid black;
    }
    tr {
        border: 0.1em solid black;
    }
    .gross-balance-pdf-header tr {
        background-color: #EAEAEA;
    }
    tr.odd {
        
    }
    tr.even {
/*        background-color: #f2f2f2;
        border: 0.1em solid black;*/
    }
    th {
        border: 0.1em solid black;
        text-align: center;
        font-size: 10px;
        font-weight: bold;
    }
    td {
        text-align: center;
        font-size: 8px;
    }
    td._text-right {
        text-align: right;
    }
    td._text-left {
        text-align: left;
    }
</style>