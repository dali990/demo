
<div id="<?=$uniq?>">
    <div style="display: flex; margin-bottom: 10px;">
        <span style="width: 40%; text-align: right; padding-right: 20px;">Tip izveštaja:</span>
        <div style="width: 60%; text-align: left;">
            <?php
                echo SIMAHtml::dropDownList('ios_by_all_clients_export_type', 'PDF', [
                    'pdf' => 'pdf',
                    'xls' => 'xls'
                ], 
                [
                    'id' => "ios_by_all_clients_export_type_$uniq"
                ]);
            ?>
        </div>
    </div>
    <div style="display: flex;">
        <span style="width: 40%; text-align: right; padding-right: 20px;">Datum:</span>
        <div style="width: 60%; text-align: left;">
            <?php
                echo SIMAHtml::datetimeFieldByName('date_for_ios_by_all_clients', '', [
                    'id' => "ios_by_all_clients_date_$uniq",
                    'htmlOptions' => [
                        'placeholder' => 'Na dan'
                    ]
                ])
            ?>
        </div>
    </div>
    <div style="display: flex;">
        <span style="width: 40%; text-align: right; padding-right: 20px;">Kupci:</span>
        <div style="width: 60%; text-align: left;">
            <input type="checkbox" name="buyers" id='<?="buyers_$uniq"?>' checked="true">
        </div>
    </div>
    <div style="display: flex;">
        <span style="width: 40%; text-align: right; padding-right: 20px;">Dobavljači:</span>
        <div style="width: 60%; text-align: left;">
            <input type="checkbox" name="suppliers" id='<?="suppliers_$uniq"?>' checked="true">
        </div>
    </div>
</div>