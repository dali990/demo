<?php
    $this->widget('SIMATree2', [
        'id' => $uniq.'_left-panel',
        'class'=>'sima-vm',
        'list_tree_data'=>$menu_array,
        'default_selected'=>$year_id,
        'disable_unselect_item'=>true
    ]);
?>

<script type='text/javascript'>
    $(function() {
        $("#<?php echo $uniq.'_left-panel'; ?>").on('selectItem', function(event, item){
            var year_id = item.data('code');
            var accounts_table = $('#'+'<?php echo $uniq.'_accounts'; ?>');
            
            accounts_table.simaGuiTable('setAddButton',{                
                init_data:{
                    Account: {
                        year_id: {
                            disabled:'disabled',
                            init: year_id
                        }
                    }
                }
            });
            $("#<?=$uniq?>_right-panel").data('year_id',year_id);    
            sima.accounting.applyAccountsByFilters("<?=$uniq?>");
        });
    });
</script>