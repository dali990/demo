<div class="add-bank-account-from-bank-statement sima-layout-panel _horizontal">
    <div class="sima-layout-fixed-panel _payment-data">
        <p>
            Izaberite partnera na osnovu podataka
        </p>
        <h3>
            <?=($invoice)?'Uplata':'Isplata'?>
        </h3>

        <table>
            <tr>
                <td>Vlasnik</td>
                <td><?=$account_owner?></td>
            </tr>
            <tr>
                <td>Broj računa</td>
                <td><?=$account_number?></td>
            </tr>
            <tr>
                <td>Datum uplate</td>
                <td><?=$input_payment_date?></td>
            </tr>
            <tr>
                <td>Iznos uplate</td>
                <td><?=$input_amount?></td>
            </tr>
            <tr>
                <td>Šifra uplate</td>
                <td><?=$payment_code_display?></td>
            </tr>
            <tr>
                <td>Poziv na broj</td>
                <td><?=$call_for_number?></td>
            </tr>
            <tr>
                <td>Poziv na broj - moduo</td>
                <td><?=$call_for_number_modul?></td>
            </tr>
            <tr>
                <td>Partnerov poziv na broj</td>
                <td><?=$call_for_number_partner?></td>
            </tr>
            <tr>
                <td>Partnerov poziv na broj - moduo</td>
                <td><?=$call_for_number_modul_partner?></td>
            </tr>
        </table>
    </div>
    <?php if (!empty($display_message)) { ?>
        <div class="sima-layout-fixed-panel _display-message">
            <?=$display_message?>
        </div>
    <?php } ?>
    <?php if (!empty($add_partner_btn)) { ?>
        <div class="sima-layout-fixed-panel _add-partner-btn">
            <?=$add_partner_btn?>
        </div>
    <?php } ?>
    <div id="<?=$form_id?>" data-form_data='<?=$form_data?>' class="sima-model-forms sima-layout-fixed-panel _bank-account-form">
        <?=$bank_account_form_html?>
    </div>
</div>