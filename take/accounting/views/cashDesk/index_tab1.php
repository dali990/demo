<div class="sima-layout-panel _splitter _vertical">

    <div class='sima-layout-panel'>
        <?php
            $this->widget('SIMAGuiTable', [
                'id' => 'orders'.$uniq,
                'title' => 'Nalozi za uplatu/isplatu',
                'model' => CashDeskOrder::model(),
                'add_button' => [
                    'init_data' => [
                        'CashDeskOrder' => [
                            'cash_desk_id'=>$cash_desk->id
                        ]
                    ]
                ],
                'fixed_filter'=>array(
                    'display_scopes' => ['byOfficialDESC']
                ),
                'setRowSelect' => "select_order$uniq",
                'setRowUnSelect' => "unselect_order$uniq",
                'setMultiSelect' => "multiselect_order$uniq",
                'setAppendRowTrigger' => "setOrderAppendRowTrigger$uniq"                
            ]);
        ?>
    </div>

    <div class='sima-layout-panel cash_desk_file_tabs<?php echo $uniq; ?>'>
        <?php 	
            $this->widget('SIMAGuiTable', [
                'id' => 'items'.$uniq,
                'title' => 'Stavke naloga',
                'model' => CashDeskItem::model(),
                'add_button' => false,
                'fixed_filter'=>array(
                    'cash_desk_order'=>array(
                        'ids'=>-1
                    )
                ),         
            ]);
        ?>
    </div>
</div>

<script type='text/javascript'>
    function select_order<?php echo $uniq; ?>(obj)
    {
        $('#items'+'<?php echo $uniq; ?>').simaGuiTable('setFixedFilter', {
            'cash_desk_order': {
                'ids': obj.attr('model_id')
            }
        });
        $('#items'+'<?php echo $uniq; ?>').simaGuiTable('populate');
        $('#items'+'<?php echo $uniq; ?>').simaGuiTable('setAddButton',{
            inline: true,
            init_data:{
                CashDeskItem: {
                    cash_desk_order_id: obj.attr('model_id')
                }
            }
        });
    }
    function unselect_order<?php echo $uniq; ?>(obj)
    {
        $('#items'+'<?php echo $uniq; ?>').simaGuiTable('setFixedFilter', {ids:-1});
        $('#items'+'<?php echo $uniq; ?>').simaGuiTable('setAddButton');
        $('#items'+'<?php echo $uniq; ?>').simaGuiTable('populate');
    }
    function multiselect_order<?php echo $uniq; ?>(obj)
    {
        $('#items'+'<?php echo $uniq; ?>').simaGuiTable('setFixedFilter', {ids:-1});
        $('#items'+'<?php echo $uniq; ?>').simaGuiTable('setAddButton');
        $('#items'+'<?php echo $uniq; ?>').simaGuiTable('populate');
    }
</script>

<?php
 	if (Yii::app()->request->isAjaxRequest) Yii::app()->clientScript->clearScriptFiles();
?>