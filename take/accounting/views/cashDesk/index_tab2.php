<div class="sima-layout-panel _splitter _vertical">

    <div class='sima-layout-panel'>
        <?php
            $this->widget('SIMAGuiTable', [
                'id' => 'logs'.$uniq,
                'title' => 'Dnevni izvestaji - dodaju se automatski',
                'model' => CashDeskLog::model(),
                'setRowSelect' => "on_select$uniq",
                'setRowUnSelect' => "on_unselect$uniq",
                'setMultiSelect' => "on_unselect$uniq",
            ]);        
        ?>
    </div>

    <div class='sima-layout-panel'>
       <?php 
            $id_tab = SIMAHtml::uniqid();
            $this->widget('ext.SIMATabs.SIMATabs', array(
                'id' => $id_tab,
                'list_tabs_model_name' => 'File',
                'list_tabs_populate' => false,
                'show_tabs' => ['cash_desk_log_items','bookkeeping'],
                'default_selected' => 'cash_desk_log_items'                
            ));
       ?>
    </div>

</div>

<script type="text/javascript">
    function on_select<?php echo $uniq; ?>(obj)
    {
        $('#<?php echo $id_tab?>').simaTabs('set_list_tabs_params',{
            list_tabs_trigger_tag: sima.getTag('<?php echo SIMAHtml::getTag(CashDeskLog::model()); ?>', obj.attr('model_id')),
            list_tabs_get_params:{id:obj.attr('model_id')}
        });
    }
    function on_unselect<?php echo $uniq; ?>(obj)
    {
        $('#<?php echo $id_tab?>').simaTabs('set_list_tabs_params',{
            list_tabs_trigger_tag:'<?php echo SIMAHtml::getTag(CashDeskLog::model(), 0) ?>',
            list_tabs_get_params:{id:0}
        });
    }
</script>

