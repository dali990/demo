<?php 
    $uniq = SIMAHtml::uniqid();
    $custom_buttons = [];
    if (!$bill->connected)
    {
        $custom_buttons = [
            'Fiktivni početni avans' => [
                'func' => "sima.accounting.unreleasing.chooseFictiveAdvance('$bill->id')"
            ]
        ];
    }
    $account = null;
    if (count($bill->fictive_account_link) > 0)
    {
        $account = $bill->fictive_account_link[0]->account_transaction->account;
    }
    
?>




<div id="<?php echo $uniq; ?>" class='sima-layout-panel bill_info _splitter _vertical'>
    <?php if (!is_null($account)){ ?>
    <div class="sima-layout-panel">
        Avansni racun je fiktivan i predstavlja pocetno stanje za konto <br/> <?=$account->DisplayHTML?>
        <br />
        <br />
        Ukloni link -> <?= SIMAHtml::modelDelete($bill->fictive_account_link[0]);?>
    </div>
    <?php } else {?>
    <div class="sima-layout-panel">
        
        <div class="bill_info_item connect_to_avans sima-layout-panel _splitter _horizontal">
            <div class="sima-layout-panel">
            <?php
                $this->widget('base.extensions.SIMAGuiTable.SIMAGuiTable', [
                    'id'=>"bill_releases$uniq",
                    'model' => BillRelease::model(),
                    'columns_type' => 'inAdvanceBill',
                    'fixed_filter' => [                            
                        'bill'=>[
                            'ids'=>$bill->id
                        ]
                    ],
                    'title'=>'Povezan za avansom',
                    'table_settings'=>true,
                    'show_add_button'=>false,
                    'pagination'=>false,
                    'filters_in'=>false,
                    'export'=>false,
                    'filters_out'=>false,
                    'headerOptionRows'=>[
                        ['title','table_settings', 'custom_buttons'],                                
                    ],
                    'custom_buttons' => $custom_buttons
                ]);
            ?>
            </div>
            <?php if ($advance_proposals_number > 0) { ?>    
            <div class="sima-layout-panel _horizontal">
                <div class="current_unreleased_percent sima-layout-fixed-panel">
                    Nedostaje još: <span class="amount"></span>
                </div>
                <div class="sima-layout-panel">
                <?php 
                    $advance_model = ($bill->invoice===true)?'AdvanceIn':'AdvanceOut';
                    $this->widget('base.extensions.SIMAGuiTable.SIMAGuiTable', [
                        'id'=>"advance_proposals$uniq",
                        'model' => $advance_model::model(),
                        'columns_type' => 'inAdvanceProposals',
                        'fixed_filter' => [
                            'like_bill_id'=>$bill->id,
                            'display_scopes'=>[
                                'unreleased_percent_of_amount'=>[
                                    $bill->amount-$bill->released_sum
                                ]
                            ],
                            'filter_scopes'=>[
                                'unreleased'
                            ]
                        ],
                        'title'=>'Mogući avansi',
                        'table_settings'=>true,
                        'show_add_button'=>false,
                        'pagination'=>false,
                        'filters_in'=>false,
                        'export'=>false,
                        'filters_out'=>false,
                        'custom_buttons'=>[
                            'Dodaj selektovane'=>[
                                'func'=>"sima.accounting.unreleasing.advanceBillAdvance($bill->id, 'advance_proposals$uniq')"
                            ]
                        ],
                        'headerOptionRows'=>[
                            ['title','table_settings','custom_buttons'],                                
                        ],
                        'setRowSelect'=>"onSelectAdvance$uniq",
                        'setMultiSelect'=>"onSelectAdvance$uniq",
                        'setRowUnSelect'=>"onUnSelectAdvance$uniq"
                    ]);
                ?>
                </div>
            </div>
            <?php } ?>
        </div>
    </div>
    <div class="sima-layout-panel">
        <div class="bill_info_item relief_bills sima-layout-panel _horizontal _splitter">
            <div class="sima-layout-panel">
            <?php
                $this->widget('base.extensions.SIMAGuiTable.SIMAGuiTable', [
                    'id'=>"advance_bill_reliefs$uniq",
                    'model' => AdvanceBillRelief::model(),
                    'columns_type' => 'inAdvanceBill',
                    'fixed_filter' => [                            
                        'advance_bill'=>[
                            'ids'=>$bill->id
                        ]
                    ],
                    'title'=>'Knjižna odobrenja',
                    'table_settings'=>true,
                    'show_add_button'=>false,
                    'pagination'=>false,
                    'filters_in'=>false,
                    'export'=>false,
                    'filters_out'=>false,
                    'headerOptionRows'=>[
                        ['title','table_settings'],                                
                    ],
                ]);
            ?>
            </div>
            <?php if ($relief_bill_proposals_number > 0) { ?>                    
            <div class="sima-layout-panel _horizontal">
                <div class="current_unreleased_percent sima-layout-fixed-panel">
                    Nedostaje još: <span class="amount"></span>
                </div>
                <div class="sima-layout-panel">
                <?php 
                    $relief_bill_model = ($bill->invoice===true)?'ReliefBillOut':'ReliefBillIn';
                    $this->widget('base.extensions.SIMAGuiTable.SIMAGuiTable', [
                        'id'=>"relief_bills_proposals$uniq",
                        'model' => $relief_bill_model::model(),
                        'columns_type' => 'inAdvanceBill',
                        'fixed_filter' => [
                            'like_advance_bill_id'=>$bill->id,
                            'display_scopes'=>[
                                'unreleased_percent_of_amount'=>[
                                    $bill->unreleased_amount
                                ]
                            ],
                            'filter_scopes'=>[
                                'unreleased'
                            ]
                        ],
                        'title'=>'Moguća knjižna odobrenja',
                        'table_settings'=>true,
                        'show_add_button'=>false,
                        'pagination'=>false,
                        'filters_in'=>false,
                        'export'=>false,
                        'filters_out'=>false,
                        'custom_buttons'=>[
                            'Dodaj selektovane'=>[
                                'func'=>"sima.accounting.unreleasing.advanceBillReliefBill($bill->id, 'relief_bills_proposals$uniq')"
                            ]
                        ],
                        'headerOptionRows'=>[
                            ['title','table_settings','custom_buttons'],                                
                        ],
    //                    'setRowSelect'=>"onSelectReliefBill$uniq",
    //                    'setMultiSelect'=>"onSelectReliefBill$uniq",
    //                    'setRowUnSelect'=>"onUnSelectReliefBill$uniq"
                    ]);
                ?>
                </div>
            </div>
            <?php } ?>
        </div>
    </div>
    <?php } ?>
    <?php if ($bill->connected){?>
    <div class="sima-layout-panel">
        <div class="bill_info_item bills sima-layout-panel _splitter _horizontal">
            <div class="sima-layout-panel">
            <?php
                $this->widget('base.extensions.SIMAGuiTable.SIMAGuiTable', [
                    'id'=>"advance_bill_releases$uniq",
                    'model' => AdvanceBillRelease::model(),
                    'columns_type' => 'inAdvanceBill',
                    'fixed_filter' => [                            
                        'advance_bill'=>[
                            'ids'=>$bill->id
                        ]
                    ],
                    'title'=>'Računi',
                    'table_settings'=>true,
                    'show_add_button'=>false,
                    'pagination'=>false,
                    'filters_in'=>false,
                    'export'=>false,
                    'filters_out'=>false,
                    'headerOptionRows'=>[
                        ['title','table_settings'],                                
                    ],
                ]);
            ?>
            </div>

            <?php if ($bill_proposals_number > 0) { ?>                    
            <div class="sima-layout-panel _horizontal">
                <div class="current_unreleased_percent sima-layout-fixed-panel">
                    Nedostaje još: <span class="amount"></span>
                </div>
                <div class="sima-layout-panel">
                <?php 
                    $bill_model = ($bill->invoice===true)?'BillOut':'BillIn';
                    $this->widget('base.extensions.SIMAGuiTable.SIMAGuiTable', [
                        'id'=>"bills_proposals$uniq",
                        'model' => $bill_model::model(),
                        'columns_type' => 'inBillProposals',
                        'fixed_filter' => [
                            'like_advance_bill_id'=>$bill->id,
                            'display_scopes'=>[
                                'unreleased_percent_of_amount'=>[
                                    $bill->unreleased_amount
                                ]
                            ],
                            'filter_scopes'=>[
                                'unreleased'
                            ]
                        ],
                        'title'=>'Mogući računi',
                        'table_settings'=>true,
                        'show_add_button'=>false,
                        'pagination'=>false,
                        'filters_in'=>false,
                        'export'=>false,
                        'filters_out'=>false,
                        'custom_buttons'=>[
                            'Dodaj selektovane'=>[
                                'func'=>"sima.accounting.unreleasing.advanceBillBill($bill->id, 'bills_proposals$uniq')"
                            ]
                        ],
                        'headerOptionRows'=>[
                            ['title','table_settings','custom_buttons'],                                
                        ],
                        'setRowSelect'=>"onSelectBill$uniq",
                        'setMultiSelect'=>"onSelectBill$uniq",
                        'setRowUnSelect'=>"onUnSelectBill$uniq"
                    ]);
                ?>
                </div>
            </div>
            <?php } ?>
        </div>
    </div>
    
    <?php } else {?>
    <div class="sima-layout-panel">
        <?=SIMAHtml::showAsImportant('Povežite sa avansnom uplatom da biste povezali sa računom')?>
    </div>
        
    <?php }?>
</div>
    
<script type='text/javascript'>
    function onSelectBill<?php echo $uniq; ?>(obj)
    {
        sima.accountingMisc.calcUnreleasedPercentAmount($('#<?php echo $uniq; ?>').find('.bill_info_item.bills'));
    }
    function onUnSelectBill<?php echo $uniq; ?>(obj)
    {
        $('#<?php echo $uniq; ?>').find('.bill_info_item.bills .current_unreleased_percent').hide();
    }
    
//    function onSelectReliefBill<?php echo $uniq; ?>(obj)
//    {
////        sima.accountingMisc.calcUnreleasedPercentAmount($('#<?php echo $uniq; ?>').find('.bill_info_item.bills'));
//    }
//    function onUnSelectReliefBill<?php echo $uniq; ?>(obj)
//    {
//        $('#<?php echo $uniq; ?>').find('.bill_info_item.bills .current_unreleased_percent').hide();
//    }
    
    function onSelectAdvance<?php echo $uniq; ?>(obj)
    {
        sima.accountingMisc.calcUnreleasedPercentAmount($('#<?php echo $uniq; ?>').find('.bill_info_item.connect_to_avans'));
    }
    function onUnSelectAdvance<?php echo $uniq; ?>(obj)
    {
        $('#<?php echo $uniq; ?>').find('.bill_info_item.connect_to_avans .current_unreleased_percent').hide();
    }
</script>
