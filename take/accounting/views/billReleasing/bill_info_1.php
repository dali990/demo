<?php $uniq = SIMAHtml::uniqid(); ?>
<div class='sima-layout-panel'>
    <div id="<?php echo $uniq; ?>" class='sima-layout-panel bill_info _vertical _splitter'>
        <div class="bill_info_item bill_info_payment sima-layout-panel">
            <div class="body sima-layout-panel _horizontal _splitter">
                <div class="main_table sima-layout-panel">
                    <?php
                        $this->widget('base.extensions.SIMAGuiTable.SIMAGuiTable', [
                            'id'=>"bill_releases$uniq",
                            'model' => BillRelease::model(),
                            'columns_type' => 'inBill',
                            'fixed_filter' => [                            
                                'bill'=>[
                                    'ids'=>$bill->id
                                ]
                            ],
                            'title'=>"Plaćanja",
                            'auto_height'=>true,
                            'table_settings'=>true,
                            'show_add_button'=>false,
                            'pagination'=>false,
                            'filters_in'=>true,
                            'export'=>false,
                            'filters_out'=>false,
                            'headerOptionRows'=>[
                                ['title','table_settings','filters_in'],                                
                            ],
                        ]);
                    ?>
                </div>
                <div class="proposals sima-layout-panel _horizontal">
                    <?php if (isset($payment_proposals_number) && $payment_proposals_number > 0) { ?>                    
                        <div class="current_unreleased_percent sima-layout-fixed-panel">
                            Nedostaje još: <span class="amount"></span>
                        </div>
                        <div class="sima-layout-panel">
                        <?php 
                            $payment_model = ($bill->invoice===true)?'PaymentIn':'PaymentOut';
                            $this->widget('base.extensions.SIMAGuiTable.SIMAGuiTable', [
                                'id'=>"payments_proposals$uniq",
                                'model' => $payment_model::model(),
                                'columns_type' => 'inPaymentProposals',
                                'fixed_filter' => [
                                    'like_bill_id'=>$bill->id,
                                    'display_scopes'=>[
                                        'unreleased_percent_of_amount'=>[
                                            $bill->amount-$bill->released_sum
                                        ]
                                    ],
                                    'filter_scopes'=>[
                                        'unreleased'
                                    ]
                                ],
                                'title'=>"Moguća plaćanja",
                                'auto_height'=>true,
                                'table_settings'=>true,
                                'show_add_button'=>false,
                                'pagination'=>false,
                                'filters_in'=>true,
                                'export'=>false,
                                'filters_out'=>false,
                                'custom_buttons'=>[
                                    'Dodaj selektovane'=>[
                                        'func'=>"sima.accounting.unreleasing.billPayment($bill->id, 'payments_proposals$uniq')"
                                    ]
                                ],
                                'headerOptionRows'=>[
                                    ['title','table_settings','custom_buttons','filters_in'],                                
                                ],
                                'setRowSelect'=>"onSelectPayment$uniq",
                                'setMultiSelect'=>"onSelectPayment$uniq",
                                'setRowUnSelect'=>"onUnSelectPayment$uniq"
                            ]);
                        ?>
                        </div>
                    <?php } ?>
                </div>        
            </div>
        </div>
        <div class="bill_info_item bill_info_advance sima-layout-panel">
            <div class="body sima-layout-panel _horizontal _splitter">
                <div class="main_table sima-layout-panel">
                    <?php
                        $this->widget('base.extensions.SIMAGuiTable.SIMAGuiTable', [
                            'id'=>"advance_bill_releases$uniq",
                            'model' => AdvanceBillRelease::model(),
                            'columns_type' => 'inBill',
                            'fixed_filter' => [                            
                                'bill'=>[
                                    'ids'=>$bill->id
                                ]
                            ],
                            'title'=>"Avansi",
                            'auto_height'=>true,
                            'table_settings'=>true,
                            'show_add_button'=>false,
                            'pagination'=>false,
                            'filters_in'=>true,
                            'export'=>false,
                            'filters_out'=>false,
                            'headerOptionRows'=>[
                                ['title','table_settings','filters_in']                            
                            ],
                        ]);
                    ?>
                </div>
                <div class="proposals sima-layout-panel _horizontal">
                    <?php if (isset($advance_bill_proposals_number) && $advance_bill_proposals_number > 0) { ?>                    
                        <div class="current_unreleased_percent sima-layout-fixed-panel">
                            Nedostaje još: <span class="amount"></span>
                        </div>
                        <div class="sima-layout-panel">
                        <?php 
                            $advance_bill_model = ($bill->invoice===true)?'AdvanceBillOut':'AdvanceBillIn';
                            $this->widget('base.extensions.SIMAGuiTable.SIMAGuiTable', [
                                'id'=>"advance_bills_proposals$uniq",
                                'model' => $advance_bill_model::model(),
                                'columns_type' => 'inBillProposals',
                                'fixed_filter' => [
                                    'like_bill_id'=>$bill->id,
                                    'display_scopes'=>[
                                        'unreleased_percent_of_amount'=>[
                                            $bill->unreleased_amount
                                        ]
                                    ],
                                    'filter_scopes'=>[
                                        'unreleased'
                                    ],
                                    'connected'=>true
                                ],
                                'title'=>"Mogući avansi",                            
                                'auto_height'=>true,
                                'table_settings'=>true,
                                'show_add_button'=>false,
                                'pagination'=>false,
                                'filters_in'=>false,
                                'export'=>false,
                                'filters_out'=>false,
                                'headerOptionRows'=>[
                                    ['title','table_settings','custom_buttons'],                                
                                ],
                                'custom_buttons'=>[
                                    'Dodaj selektovane'=>[
                                        'func'=>"sima.accounting.unreleasing.billAdvance($bill->id, 'advance_bills_proposals$uniq')"
                                    ],
                                    'Dodaj za odredjeni iznos'=>[
                                        'func'=>"sima.accounting.unreleasing.billAdvanceByAmount($bill->id, 'advance_bills_proposals$uniq')"
                                    ]
                                ],
                                'setRowSelect'=>"onSelectBillAdvance$uniq",
                                'setMultiSelect'=>"onSelectBillAdvance$uniq",
                                'setRowUnSelect'=>"onUnSelectBillAdvance$uniq"
                            ]);
                        ?>
                        </div>
                    <?php } ?>
                </div>        
            </div>
        </div>
        <div class="bill_info_item bill_info_relief sima-layout-panel">        
            <div class="body sima-layout-panel _horizontal _splitter">
                <div class="main_table sima-layout-panel">
                    <?php
                        $this->widget('base.extensions.SIMAGuiTable.SIMAGuiTable', [
                            'id'=>"bill_reliefs$uniq",
                            'model' => BillRelief::model(),
                            'columns_type' => 'inBill',
                            'fixed_filter' => [                            
                                'bill'=>[
                                    'ids'=>$bill->id
                                ]
                            ],
                            'title'=>'Knjižna odobrenja',
                            'auto_height'=>true,
                            'table_settings'=>true,
                            'show_add_button'=>false,
                            'pagination'=>false,
                            'filters_in'=>true,
                            'export'=>false,
                            'filters_out'=>false,
                            'headerOptionRows'=>[
                                ['title','table_settings','filters_in'],                                
                            ],
                        ]);
                    ?>
                </div>
                <div class="proposals sima-layout-panel _horizontal">
                    <?php if (isset($relief_bill_proposals_number) && $relief_bill_proposals_number > 0) { ?>                    
                        <div class="current_unreleased_percent sima-layout-fixed-panel">
                            Nedostaje još: <span class="amount"></span>
                        </div>
                        <div class="sima-layout-panel">
                        <?php 
                            $relief_bill_model = ($bill->invoice===true)?'ReliefBillOut':'ReliefBillIn';
                            $this->widget('base.extensions.SIMAGuiTable.SIMAGuiTable', [
                                'id'=>"relief_bills_proposals$uniq",
                                'model' => $relief_bill_model::model(),
                                'columns_type' => 'inBillProposals',
                                'fixed_filter' => [
                                    'like_bill_id'=>$bill->id,
                                    'display_scopes'=>[
                                        'unreleased_percent_of_amount'=>[
                                            $bill->unreleased_amount
                                        ]
                                    ],
                                    'filter_scopes'=>[
                                        'unreleased'
                                    ]
                                ],
                                'title'=>'Moguća knjižna odobrenja',
                                'auto_height'=>true,
                                'table_settings'=>true,
                                'show_add_button'=>false,
                                'pagination'=>false,
                                'filters_in'=>false,
                                'export'=>false,
                                'filters_out'=>false,
                                'custom_buttons'=>[
                                    'Dodaj selektovane'=>[
                                        'func'=>"sima.accounting.unreleasing.billRelief($bill->id, 'relief_bills_proposals$uniq')"
                                    ]
                                ],
                                'headerOptionRows'=>[
                                    ['title','table_settings','custom_buttons'],                                
                                ],
                                'setRowSelect'=>"onSelectBillRelief$uniq",
                                'setMultiSelect'=>"onSelectBillRelief$uniq",
                                'setRowUnSelect'=>"onUnSelectBillRelief$uniq"
                            ]);
                        ?>
                            </div>
                    <?php } ?>
                </div>        
            </div>
        </div>
    </div>
</div>

<script type='text/javascript'>
    function onSelectPayment<?php echo $uniq; ?>(obj)
    {
        sima.accountingMisc.calcUnreleasedPercentAmount($('#<?php echo $uniq; ?>').find('.bill_info_item.bill_info_payment'));
    }
    function onUnSelectPayment<?php echo $uniq; ?>(obj)
    {
        $('#<?php echo $uniq; ?>').find('.bill_info_item.bill_info_payment .current_unreleased_percent').hide();
    }
    function onSelectBillAdvance<?php echo $uniq; ?>(obj)
    {
        sima.accountingMisc.calcUnreleasedPercentAmount($('#<?php echo $uniq; ?>').find('.bill_info_item.bill_info_advance'));
    }
    function onUnSelectBillAdvance<?php echo $uniq; ?>(obj)
    {
        $('#<?php echo $uniq; ?>').find('.bill_info_item.bill_info_advance .current_unreleased_percent').hide();
    }
    function onSelectBillRelief<?php echo $uniq; ?>(obj)
    {
        sima.accountingMisc.calcUnreleasedPercentAmount($('#<?php echo $uniq; ?>').find('.bill_info_item.bill_info_relief'));
    }
    function onUnSelectBillRelief<?php echo $uniq; ?>(obj)
    {
        $('#<?php echo $uniq; ?>').find('.bill_info_item.bill_info_relief .current_unreleased_percent').hide();
    }
</script>