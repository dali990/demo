<?php $uniq = SIMAHtml::uniqid(); ?>
<div id="<?php echo $uniq; ?>" class='sima-layout-panel sima-layout-panel bill_info'>
    <div class="sima-layout-panel  bill_info_item bill_info_payment">
        <div class="body sima-layout-panel _splitter _horizontal">
            <div class="main_table sima-layout-panel ">
                <?php
                    $this->widget('base.extensions.SIMAGuiTable.SIMAGuiTable', [
                        'id'=>"bill_releases$uniq",
                        'model' => BillRelease::model(),
                        'columns_type' => 'inBill',
                        'fixed_filter' => [                            
                            'bill'=>[
                                'ids'=>$bill->id
                            ]
                        ],
                        'title'=>"Plaćanja",
                        'auto_height'=>false,
                        'table_settings'=>true,
                        'show_add_button'=>false,
                        'pagination'=>false,
                        'filters_in'=>false,
                        'export'=>false,
                        'filters_out'=>false,
                        'headerOptionRows'=>[
                            ['title','table_settings'],                                
                        ],
                    ]);
                ?>
            </div>
            <div class="proposals sima-layout-panel ">
                <?php if (isset($payment_proposals_number) && $payment_proposals_number > 0) { ?>    
                <div class="sima-layout-panel _horizontal">                
                    <div class="current_unreleased_percent sima-layout-fixed-panel">
                        Nedostaje još: <span class="amount"></span>
                    </div>
                    <div class="sima-layout-panel">
                    <?php 
                        $payment_model = ($bill->invoice===true)?'PaymentIn':'PaymentOut';
                        $this->widget('base.extensions.SIMAGuiTable.SIMAGuiTable', [
                            'id'=>"payments_proposals$uniq",
                            'model' => $payment_model::model(),
                            'columns_type' => 'inPaymentProposals',
                            'fixed_filter' => [
                                'like_bill_id'=>$bill->id,
                                'display_scopes'=>[
                                    'unreleased_percent_of_amount'=>[
                                        $bill->amount-$bill->released_sum
                                    ]
                                ],
                                'filter_scopes'=>[
                                    'unreleased'
                                ]
                            ],
                            'title'=>"Moguća plaćanja",
                            'auto_height'=>false,
                            'table_settings'=>true,
                            'show_add_button'=>false,
                            'pagination'=>false,
                            'filters_in'=>false,
                            'export'=>false,
                            'filters_out'=>false,
                            'custom_buttons'=>[
                                'Dodaj selektovane'=>[
                                    'func'=>"sima.accounting.unreleasing.billPayment($bill->id, 'payments_proposals$uniq')"
                                ]
                            ],
                            'headerOptionRows'=>[
                                ['title','table_settings','custom_buttons'],                                
                            ],
                            'setRowSelect'=>"onSelectPayment$uniq",
                            'setMultiSelect'=>"onSelectPayment$uniq",
                            'setRowUnSelect'=>"onUnSelectPayment$uniq"
                        ]);
                    ?>
                    </div>
                </div>
                    
                <?php } ?>
            </div>        
        </div>
    </div>
    

</div>

<script type='text/javascript'>
    function onSelectPayment<?php echo $uniq; ?>(obj)
    {
        sima.accountingMisc.calcUnreleasedPercentAmount($('#<?php echo $uniq; ?>').find('.bill_info_item.bill_info_payment'));
    }
    function onUnSelectPayment<?php echo $uniq; ?>(obj)
    {
        $('#<?php echo $uniq; ?>').find('.bill_info_item.bill_info_payment .current_unreleased_percent').hide();
    }
</script>