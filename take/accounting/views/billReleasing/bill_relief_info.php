<?php $uniq = SIMAHtml::uniqid() ?>
<div id="<?php echo $uniq; ?>" class='sima-layout-panel bill_info _vertical _splitter'>
    <div class="sima-layout-panel _horizontal bill_info_item bills">        
        <div class=" sima-layout-panel body _splitter _horizontal">
            <div class="sima-layout-panel main_table">
                <?php
                    $this->widget('base.extensions.SIMAGuiTable.SIMAGuiTable', [
                        'id'=>"bill_reliefs$uniq",
                        'model' => BillRelief::model(),
                        'columns_type' => 'inReliefBill',
                        'fixed_filter' => [                            
                            'relief_bill'=>[
                                'ids'=>$bill->id
                            ]
                        ],
                        'title'=>'Računi',
                        'table_settings'=>true,
                        'show_add_button'=>false,
                        'pagination'=>false,
                        'filters_in'=>false,
                        'export'=>false,
                        'filters_out'=>false,
                        'headerOptionRows'=>[
                            ['title','table_settings'],                                
                        ],
                    ]);
                ?>
            </div>
            <div class=" sima-layout-panel _horizontal proposals">
                <?php if ($bill_proposals_number > 0) { ?>                    
                <div class="sima-layout-fixed-panel current_unreleased_percent">
                    Nedostaje još: <span class="amount"></span>
                </div>
                <div class="sima-layout-panel">
                    <?php 
                        $bill_model = ($bill->invoice===true)?'BillOut':'BillIn';
                        $this->widget('base.extensions.SIMAGuiTable.SIMAGuiTable', [
                            'id'=>"bills_proposals$uniq",
                            'model' => $bill_model::model(),
                            'columns_type' => 'inBillProposals',
                            'fixed_filter' => [
                                'like_relief_bill_id'=>$bill->id,
                                'display_scopes'=>[
                                    'unreleased_percent_of_amount'=>[
                                        $bill->unreleased_amount
                                    ]
                                ],
                                'filter_scopes'=>[
                                    'unreleased'
                                ]
                            ],
                            'title'=>'Mogući računi',
                            'table_settings'=>true,
                            'show_add_button'=>false,
                            'pagination'=>false,
                            'filters_in'=>false,
                            'export'=>false,
                            'filters_out'=>false,
                            'custom_buttons'=>[
                                'Dodaj selektovane'=>[
                                    'func'=>"sima.accounting.unreleasing.reliefBillBill($bill->id, 'bills_proposals$uniq')"
                                ]
                            ],
                            'headerOptionRows'=>[
                                ['title','table_settings','custom_buttons'],                                
                            ],
                            'setRowSelect'=>"onSelectBill$uniq",
                            'setMultiSelect'=>"onSelectBill$uniq",
                            'setRowUnSelect'=>"onUnSelectBill$uniq"
                        ]);
                    ?>
                </div>
                <?php } ?>
                
            </div>        
        </div>
    </div>
    <div class="sima-layout-panel _horizontal bill_info_item relief_bills">
        <div class="sima-layout-panel  _splitter _horizontal">
            <div class="sima-layout-panel">
            <?php
                $this->widget('base.extensions.SIMAGuiTable.SIMAGuiTable', [
                    'id'=>"advance_bill_reliefs$uniq",
                    'model' => AdvanceBillRelief::model(),
                    'columns_type' => 'inReliefBill',
                    'fixed_filter' => [                            
                        'relief_bill'=>[
                            'ids'=>$bill->id
                        ]
                    ],
                    'title'=>'Avansni računi',
                    'table_settings'=>true,
                    'show_add_button'=>false,
                    'pagination'=>false,
                    'filters_in'=>false,
                    'export'=>false,
                    'filters_out'=>false,
                    'headerOptionRows'=>[
                        ['title','table_settings'],                                
                    ],
                ]);
            ?>
            </div>
            <div class="sima-layout-panel" >
            <?php if ($advance_bill_proposals_number > 0) { ?>                    
<!--                <div class="current_unreleased_percent">
                    Nedostaje još: <span class="amount"></span>
                </div>-->
                <?php 
                    $relief_bill_model = ($bill->invoice===true)?'AdvanceBillOut':'AdvanceBillIn';
                    $this->widget('base.extensions.SIMAGuiTable.SIMAGuiTable', [
                        'id'=>"relief_bills_proposals$uniq",
                        'model' => $relief_bill_model::model(),
    //                    'columns_type' => 'inReliefBill',
                        'fixed_filter' => [
                            'like_bill_id'=>$bill->id,
                            'display_scopes'=>[
                                'unreleased_percent_of_amount'=>[
                                    $bill->unreleased_amount
                                ]
                            ],
                            'filter_scopes'=>[
                                'unreleased'
                            ]
                        ],
                        'title'=>'Mogući avansni računi',
                        'table_settings'=>true,
                        'show_add_button'=>false,
                        'pagination'=>false,
                        'filters_in'=>false,
                        'export'=>false,
                        'filters_out'=>false,
                        'custom_buttons'=>[
                            'Dodaj selektovane'=>[
                                'func'=>"sima.accounting.unreleasing.reliefBillAdvanceBill($bill->id, 'relief_bills_proposals$uniq')"
                            ]
                        ],
                        'headerOptionRows'=>[
                            ['title','table_settings','custom_buttons'],                                
                        ],
    //                    'setRowSelect'=>"onSelectReliefBill$uniq",
    //                    'setMultiSelect'=>"onSelectReliefBill$uniq",
    //                    'setRowUnSelect'=>"onUnSelectReliefBill$uniq"
                    ]);
                ?>
            <?php } ?>
            </div>
        </div>
    </div>
</div>

<script type='text/javascript'>
    function onSelectBill<?php echo $uniq; ?>(obj)
    {
        sima.accountingMisc.calcUnreleasedPercentAmount($('#<?php echo $uniq; ?>').find('.bill_info_item.bills'));
    }
    function onUnSelectBill<?php echo $uniq; ?>(obj)
    {
        $('#<?php echo $uniq; ?>').find('.bill_info_item.bills .current_unreleased_percent').hide();
    }
</script>
