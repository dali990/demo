
<?php $this->beginWidget('ext.SIMAResizablePanel.SIMAResizablePanel',
    array(
        'type'=>'vertical',
        'proportions'=>array(0.8,0.2),
        'min_widths' => [100,400]
        )); ?>
        
        <div class="sima-layout-panel">
            <?php 
                $fixed_assets_guitable_id = "fixed_assets_$uniq";
                
                $additional_options_html = '';
                if(SIMAMisc::isVueComponentEnabled())
                {
                    $additional_options_html = Yii::app()->controller->widget(SIMAButtonVue::class, [
                        'title' => Yii::t('AccountingModule.FixedAssets', 'ExportInventoryList'),
                        'onclick' => ['sima.accountingFixedAssets.exportInventoryList', "$fixed_assets_guitable_id"],
                        'subactions_data' => [
                            [
                                'title' => Yii::t('AccountingModule.FixedAssets', 'ExportInventoryListOnDay'),
                                'onclick' => ['sima.accountingFixedAssets.exportInventoryList', "$fixed_assets_guitable_id", true]
                            ]
                        ]
                    ], true);
                }
                else
                {
                    $additional_options_html = Yii::app()->controller->widget('SIMAButton', [
                        'action' => [
                            'title' => Yii::t('AccountingModule.FixedAssets', 'ExportInventoryList'),
                            'onclick' => ['sima.accountingFixedAssets.exportInventoryList', "$fixed_assets_guitable_id"],
                            'subactions' => [
                                [
                                    'title' => Yii::t('AccountingModule.FixedAssets', 'ExportInventoryListOnDay'),
                                    'onclick' => ['sima.accountingFixedAssets.exportInventoryList', "$fixed_assets_guitable_id", true]
                                ]
                            ]
                        ]
                    ], true);
                }
                
                $this->widget('SIMAGuiTable',[
                    'id' => $fixed_assets_guitable_id,
                    'model' => AccountingFixedAsset::model(),
                    'add_button' => $add_button,
                    'setRowSelect' => "on_select$uniq",
                    'setRowUnSelect' => "on_unselect$uniq",
                    'setMultiSelect' => "on_unselect$uniq",
                    'fixed_filter' => [
                        'display_scopes' => ['byOrderDESC']
                    ],
                    'select_filter' => [
                        'fixed_asset' => [
                            'active' => '1'
                        ]
                    ],
                    'additional_options_html' => $additional_options_html
                ]); 
                
                
            ?>
        </div>


        <div class="sima-layout-panel">
            <?php
                $id_tab = SIMAHtml::uniqid();
                $this->widget('ext.SIMATabs.SIMATabs', array(
                    'id' => $id_tab,
                    'list_tabs_model_name' => 'FixedAsset',
                    'list_tabs_populate' => false,
                    'default_selected' => 'depreciations'                
                ));
            ?>
        </div>
        
<?php $this->endWidget(); ?>

<script type="text/javascript">
    function on_select<?php echo $uniq; ?>(obj)
    {
        $('#<?php echo $id_tab?>').simaTabs('set_list_tabs_params',{
            list_tabs_trigger_tag: sima.getTag('<?php echo SIMAHtml::getTag(FixedAsset::model()); ?>', obj.attr('model_id')),
            list_tabs_get_params:{id:obj.attr('model_id')}
        });
    }
    function on_unselect<?php echo $uniq; ?>(obj)
    {
        $('#<?php echo $id_tab?>').simaTabs('set_list_tabs_params',{
            list_tabs_trigger_tag:'<?php echo SIMAHtml::getTag(FixedAsset::model(), 0) ?>',
            list_tabs_get_params:{id:0}
        });
    }
</script>

