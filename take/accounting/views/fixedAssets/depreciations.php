
<div class="sima-layout-panel _vertical _splitter">
        
        <div class="sima-layout-panel" data-sima-layout-init='{"proportion":0.2, "min_width":400}'>
            <?php 
            
                $this->widget('SIMAGuiTable',[
                    'model' => FixedAssetsDepreciation::model(),
                    'add_button' => true,
                    'title' => 'Amortizacije osnovnih sredstava',
                    'setRowSelect' => "on_select$uniq",
                    'setRowUnSelect' => "on_unselect$uniq",
                ]);
                
                
                ?>
        </div>


        <div class="sima-layout-panel" data-sima-layout-init='{"proportion":0.8, "min_width":200}'>
            <?php
                $id_tab = SIMAHtml::uniqid();
                $this->widget('ext.SIMATabs.SIMATabs', array(
                    'id' => 'tabs'.$uniq,
                    'list_tabs_model_name' => 'File',
                    'default_selected' => 'bookkeeping',
                    'ignore_tabs' => [
                        'belongs',
                        'others'
                    ]
                ));
            ?>
        </div>
        
</div>

<script type='text/javascript'>
    function on_select<?php echo $uniq; ?>(obj)
    {
        $('#tabs<?php echo $uniq?>').simaTabs('set_list_tabs_params',{
            list_tabs_trigger_tag: sima.getTag('<?php echo SIMAHtml::getTag(File::model()); ?>', obj.attr('model_id')),
            list_tabs_get_params:{id:obj.attr('model_id')}
        });
    }
    
    function on_unselect<?php echo $uniq; ?>(obj)
    {
        $('#tabs<?php echo $uniq?>').simaTabs('set_list_tabs_params',{
            list_tabs_trigger_tag:'<?php echo SIMAHtml::getTag(File::model(), 0) ?>',
            list_tabs_get_params:{id:0}
        });
    }
</script>
