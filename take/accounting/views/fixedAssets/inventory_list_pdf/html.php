<?php
    $company_main_address = !empty(Yii::app()->company->partner->main_address_id) ? Yii::app()->company->partner->MainAddress->DisplayName : '-';
?>

<div id="container">
    <header>
        <img src='<?=$company_logo_path?>' />
        <div class="content-wrap">
            <div class="content">
                <ul>
                    <li><span><?=Yii::t('BaseModule.Common', 'Address')?>:</span> <?=$company_main_address?></li>
                    <li><span><?=Yii::t('BaseModule.Common', 'Contact')?>:</span> <?=!empty(Yii::app()->company->partner->main_contact_id) ? Yii::app()->company->partner->MainContact->value : '-'?></li>
                </ul>
            </div>
            <div class="content">
                <ul>
                    <li><span><?=Yii::t('Company','PIB')?>:</span> <?=Yii::app()->company->PIB?></li>
                    <li><span><?=Yii::t('Company','MB')?>:</span> <?=Yii::app()->company->MB?></li>
                </ul>
            </div>
            <div class="content">
                <ul>
                    <li><span><?=Yii::t('BaseModule.Common', 'Date')?>:</span> <?=SIMAHtml::DbToUserDate(SIMAHtml::currentDateTime(false))?></li>
                </ul>
            </div>
        </div>
    </header>

    <div class="main">
        <div class="title">
            <h1><?=Yii::t('AccountingModule.FixedAssets', 'InventoryList')?> <span><?=SIMAHtml::DbToUserDate($date)?></span></h1>
        </div><!-- /title -->

        <div class="content">
            <?=$fixed_assets_html?>
        </div><!-- /content -->
    </div> <!-- /main -->

</div><!-- /container -->



