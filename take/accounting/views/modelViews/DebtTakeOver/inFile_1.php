<h3>Ugovor o asignaciji</h3>

<p class='row'>
        <span class='title' ><?php echo $model->getAttributeLabel('creditor')?>:</span>
        <span class='data'><?php echo $model->getAttributeDisplay('creditor');?></span>
</p>
<p class='row'>
        <span class='title' ><?php echo $model->getAttributeLabel('debtor')?>:</span>
        <span class='data'><?php echo $model->getAttributeDisplay('debtor');?></span>
</p>
<p class='row'>
        <span class='title' ><?php echo $model->getAttributeLabel('takes_over')?>:</span>
        <span class='data'><?php echo $model->getAttributeDisplay('takes_over');?></span>
</p>
<p class='row'>
        <span class='title' ><?php echo $model->getAttributeLabel('debt_value')?>:</span>
        <span class='data'><?php echo $model->getAttributeDisplay('debt_value');?></span>
</p>
<p class='row'>
        <span class='title' ><?php echo $model->getAttributeLabel('sign_date')?>:</span>
        <span class='data'><?php echo $model->getAttributeDisplay('sign_date');?></span>
</p>

	
