<div class="sima-layout-panel _horizontal">
<?php if (!empty($warehouse)) {?>
    <div class="sima-layout-fixed-panel">
        Magacin kao mesto troška: <?=$warehouse->DisplayName?>
    </div>
    <div class="sima-layout-panel">
        <?php 
            $this->widget('base.extensions.SIMAGuiTable.SIMAGuiTable', [
                'model' => WarehouseMaterial::model(),
                'columns_type' => 'inWarehouse',
                'add_button' => false,
                'fixed_filter' => [
                    'filter_scopes' => [
                        'inWarehouseSelect' => [
                            $warehouse->id
                        ],
                        'inWarehouseCondition' => [
                            $warehouse->id
                        ]
                    ]
                ],
                'select_filter' => [
                    'quantity' => 'gt0'
                ]
            ]);
            ?>
    </div>
<?php } else {?>

    <button class="sima-ui-button" onclick="sima.accountingMisc.addWarehouseAsCostLocation($(this),<?=$model->id?>)">Dodaj magacin kao mesto troška</button>
<?php } ?>

</div>
