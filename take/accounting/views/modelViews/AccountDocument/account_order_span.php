<?php
    
    echo $confirmed_text.$confirmed_button;
    if ($model->id !== $model->account_order_id && !$model->isBooked)
    {
        echo $model->getAttributeDisplay('option_buttons');
    }
    echo "<br />";
    
    if (isset($model->canceled_account_order))
    {
        echo $canceled_text.$canceled_button;
        if (
                $model->id !== $model->canceled_account_order_id 
                && !$model->canceled_account_order->booked
            )
        {
            echo $model->getAttributeDisplay('canceled_option_buttons');
        }
    }
    else
    {
        if (isset($model->file->bill_canceled) && !isset($model->canceled_account_order))
        {
            $year_id = $model->account_order->year_id;
            $action = [ 
                'title'=>'STORNIRAJ',
                'onclick'=>['sima.accounting.addCancelAccountDocumentChoose',$model->id, Year::get()->id]
            ];
            if(SIMAMisc::isVueComponentEnabled())
            {
                echo Yii::app()->controller->widget(SIMAButtonVue::class, $action,true);
            }
            else
            {
                echo Yii::app()->controller->widget('SIMAButton', [
                    'action'=>$action
                ],true);
            }
        }
    }
