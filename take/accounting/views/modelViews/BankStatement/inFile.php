<h3>Finansijski izvod iz banke <span class='edit'><?php echo SIMAHtml::modelFormOpen($model).SIMAHtml::modelDialog($model)?></span></h3>

<p class='row'>
        <span class='title' ><?php echo $model->getAttributeLabel('account')?>:</span>
        <span class='data'><?php echo $model->getAttributeDisplay('account');?></span>
</p>
<p class='row'>
        <span class='title' ><?php echo $model->getAttributeLabel('prevAmount')?>:</span>
        <span class='data'><?php echo $model->getAttributeDisplay('prevAmount');?></span>
</p>
<p class='row'>
        <span class='title'><?php echo $model->getAttributeLabel('amount')?>:</span>
        <span class='data'><?php echo $model->getAttributeDisplay('amount');?></span>
</p>
<p class='row'>
    <span class='title'><?php echo $model->getAttributeLabel('calcAmount')?>:</span>
    <span class='data'><?php echo $model->getAttributeDisplay('calcAmount');?></span>
</p>
<p class='row'>
    <span class='title'><?php echo $model->getAttributeLabel('date')?>:</span>
    <span class='data'><?php echo $model->getAttributeDisplay('date');?></span>
</p>
	
