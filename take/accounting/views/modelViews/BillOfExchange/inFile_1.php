
<h3>Menica - <?php echo $model->getAttributeDisplay('status');?></h3>

<p class='row'>
    <span class='title'><?php echo $model->getAttributeLabel('number')?>:</span> 
    <span class='data'><?php echo $model->getAttributeDisplay('number');?></span>
</p>
<p class='row'>
    <span class='title'><?php echo $model->getAttributeLabel('begin_date')?>:</span> 
    <span class='data'><?php echo $model->getAttributeDisplay('begin_date');?></span>
</p>
<p class='row '>
    <span class='title'><?php echo $model->getAttributeLabel('authorization_file_id')?>:</span> 
    <span class='data'><?php echo $model->getAttributeDisplay('authorization_file_id');?></span>
</p>
<p class='row '>
    <span class='title'><?php echo $model->getAttributeLabel('end_date')?>:</span> 
    <span class='data'><?php echo $model->getAttributeDisplay('end_date');?></span>
</p>
<p class='row '>
    <span class='title'><?php echo $model->getAttributeLabel('partner_id')?>:</span> 
    <span class='data'><?php echo $model->getAttributeDisplay('partner_id');?></span>
</p>
<p class='row '>
    <span class='title'><?php echo $model->getAttributeLabel('description')?>:</span> 
    <span class='data'><?php echo $model->getAttributeDisplay('description');?></span>
</p>
<p class='row '>
    <span class='title'><?php echo $model->getAttributeLabel('end_date')?>:</span> 
    <span class='data'><?php echo $model->getAttributeDisplay('end_date');?></span>
</p>