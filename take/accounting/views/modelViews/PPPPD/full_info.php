<div class="sima-layout-panel _horizontal">
    <div class="sima-layout-fixed-panel">
        <div class="ppppd-full-info-item">
            <?=Yii::app()->controller->renderModelView($model, 'dirty')?>
        </div>
    </div>
    <?php if ($model->creation_type === PPPPD::$CREATE_TYPE_SIMA_GENERATE) { ?>
        <div class="sima-layout-fixed-panel">
            <div class="ppppd-full-info-item">
                <?=$model->getAttributeDisplay('create_new_file_version')?>
            </div>
        </div>
    <?php } ?>
    <div class="sima-layout-panel">
        <?php
            $this->widget('SIMAGuiTable', [
                'model' => PPPPDItem::model(),
                'columns_type' => 'from_ppppd_full_info',
                'title' => Yii::t('AccountingModule.PPPPD', 'PPPPDItems'),
                'fixed_filter' => [
                    'ppppd' => [
                        'ids' => $model->id
                    ]
                ]
            ]);
        ?>
    </div>
</div>
