<div class="basic_info">
    <p class='row'>
        <span class='title'><?=$model->getAttributeLabel('client_identificator')?>:</span>
        <span class='data'><?=$model->getAttributeDisplay('client_identificator')?></span>
    </p>
    <p class='row'>
        <span class='title'><?=$model->getAttributeLabel('creation_type')?>:</span>
        <span class='data'><?=$model->getAttributeDisplay('creation_type')?></span>
    </p>
    <p class='row'>
        <span class='title'><?=$model->getAttributeLabel('create_time')?>:</span>
        <span class='data'><?=$model->getAttributeDisplay('create_time')?></span>
    </p>
    <p class='row'>
        <span class='title'><?= $model->getAttributeLabel('year_id') ?>:</span>
        <span class='data'><?= $model->getAttributeDisplay('year') ?></span>
    </p>
    <p class='row'>
        <span class='title'><?=$model->getAttributeLabel('note')?>:</span>
        <span class='data'><?=$model->getAttributeDisplay('note')?></span>
    </p>
</div>