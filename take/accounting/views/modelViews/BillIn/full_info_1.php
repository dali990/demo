<?php $uniq = SIMAHtml::uniqid();?>

<div class="sima-layout-panel _splitter _vertical">
    
    <div class="sima-layout-panel _horizontal _splitter" data-sima-layout-init='{"proportion":"0.7"}'>
        
        <div class="sima-layout-panel _horizontal">
            <div 
                style="border-bottom: dotted" 
                class="sima-layout-fixed-panel" 
                data-sima-layout-init='{"height_cut":3}'
            >
                <div style="float:left"><?=$this->renderModelView($model->file,'belongsList');?></div>
                <div style="float:right"><?=$this->renderModelView($model,'bill_items_not_confirmed');?></div>
            </div>
            
            <accounting-file_tab-bill_items 
                id='<?=$uniq?>' 
                class="sima-layout-panel" 
                v-bind:model='bill'
                v-bind:read_only='true'
            >
            </accounting-file_tab-bill_items>
        </div>
        
        <div class="sima-layout-panel">
            <?php
                $this->widget('SIMAFilePreview', [
                    'file' => $model->file
                ]);
            ?>
        </div>
        
    </div>
    
    <div class="sima-layout-panel _horizontal" data-sima-layout-init='{"proportion":"0.3","min_width":"300","max_width":"800"}'>
        
        <div class="sima-layout-fixed-panel">
            <div  style="border-bottom: dotted">
                <?=$this->renderModelView($model->file,'filing_number_in_file');?>
            </div>
        </div>
        <div class="sima-layout-fixed-panel">
            <div  style="border-bottom: dotted">
                <?=$this->renderModelView($model->file,'full_info_connected_documents');?>
            </div>
        </div>
        
        <div class="sima-layout-fixed-panel">
            <div  style="border-bottom: dotted">
                <?=$this->renderModelView($model->file,'booked_in_file_full_info');?>
            </div>
        </div>
        
        <div class="sima-layout-fixed-panel">
            <div  style="border-bottom: dotted">
                <?=$this->renderModelView($model,'payed_with');?>
            </div>
        </div>
        
        <div class="sima-layout-fixed-panel">
            <div  style="border-bottom: dotted">
                <?=$this->renderModelView($model->file,'full_info_signatures');?>
            </div>
        </div>
        <div class="sima-layout-fixed-panel">
            <div  style="border-bottom: dotted">
                <?=$this->renderModelView($model->file,'full_info_comments');?>
            </div>
        </div>
        
        
    </div>

</div>


<script type='text/javascript'>

    new Vue({
        el: '#<?=$uniq?>',
        data: {
            bill:sima.vue.shot.getters.model('<?=SIMAMisc::getVueModelTag($model)?>')
        }
    });

</script>