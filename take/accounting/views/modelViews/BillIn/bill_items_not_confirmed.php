
    <div>
        
        <?php 
        if(SIMAMisc::isVueComponentEnabled())
        {
            $this->widget(SIMAButtonVue::class,[
                'title' => 'Pregled i dodavanje prijemnica',
                'onclick' => ['sima.accounting.warehouse.BillWarehouseTransfersConnection',$model->id]
            ]);
        }
        else
        {
            $this->widget('SIMAButton',[
                'action' => [
                    'title' => 'Pregled i dodavanje prijemnica',
                    'onclick' => ['sima.accounting.warehouse.BillWarehouseTransfersConnection',$model->id]
                ]
            ]);
        }
        ?>
        
        <table style="float:right; margin: 0 10px; font-size: 1.2em">
            <tr><td style="text-align: right;">Povezanih: <?=$connected_transfers?></td></tr>
            <tr><td style="text-align: right;">Slobodnih: <?=$free_transfers?></td></tr>
        </table>
    
    
    </div>
