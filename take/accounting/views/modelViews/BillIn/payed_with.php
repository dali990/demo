<?=$base_html?>

<?php if($display_pay_button) { 
    if(SIMAMisc::isVueComponentEnabled())
    {
        $this->widget(SIMAButtonVue::class,[
            'title' => Yii::t('AccountingModule.Bill', 'Pay'),
            'onclick' => $onclick_array
        ]);
    }
    else
    {
        $this->widget('SIMAButton',[
            'action' => [
                'title' => Yii::t('AccountingModule.Bill', 'Pay'),
                'onclick' => $onclick_array
            ]
        ]);
    }
} ?>