
<h4>Stavke racuna 

    <?php 
    if(SIMAMisc::isVueComponentEnabled())
    {
    $this->widget(SIMAButtonVue::class,[
        'title' => 'Pregled',
        'onclick' => ['sima.dialog.openActionInDialog','accounting/billItems',[
            'get_params' => [
                'bill_id' => $model->id
            ]
        ]]
    ]);
    }
    else
    {
    $this->widget('SIMAButton',[
        'action' => [
            'title' => 'Pregled',
            'onclick' => ['sima.dialog.openActionInDialog','accounting/billItems',[
                'get_params' => [
                    'bill_id' => $model->id
                ]
            ]]
        ]
    ]);
    }
    ?>

</h4>

<?php if (!empty($warehouse_transfers)) {?>
    Magacinska dokumenta: 
    
    <?php 
    if(SIMAMisc::isVueComponentEnabled())
    {
        $this->widget(SIMAButtonVue::class,[
            'title' => 'uredi',
            'onclick' => ['sima.accounting.warehouse.BillWarehouseTransfersConnection',$model->id]
        ]);
    }
    else
    {
        $this->widget('SIMAButton',[
            'action' => [
                'title' => 'uredi',
                'onclick' => ['sima.accounting.warehouse.BillWarehouseTransfersConnection',$model->id]
            ]
        ]);
    }
    ?>
    
    <table class="cf_one_pixel_border">
        <tr>
            <td><?=WarehouseTransfer::model()->getAttributelabel('type')?></td>
            <td><?=WarehouseTransfer::model()->getAttributelabel('DisplayName')?></td>
            <td><?=WarehouseTransfer::model()->getAttributelabel('warehouse_from')?></td>
            <td><?=WarehouseTransfer::model()->getAttributelabel('warehouse_to')?></td>
            <td><?=WarehouseTransfer::model()->getAttributelabel('warehouse_transfer_value')?></td>
        </tr>

    <?php foreach ($warehouse_transfers as $w_transfer) {?>
        <tr>
            <td><?=$w_transfer->getAttributeDisplay('type')?></td>
            <td><?=$w_transfer->DisplayHtml?></td>
            <td><?=$w_transfer->getAttributeDisplay('warehouse_from')?></td>
            <td><?=$w_transfer->getAttributeDisplay('warehouse_to')?></td>
            <td><?=$w_transfer->getAttributeDisplay('warehouse_transfer_value')?></td>
        </tr>
    <?php } ?>

    </table>
<?php }?>

<?php if (!empty($service_bi)) {?>
Usluge:
<table class="cf_one_pixel_border">
    <tr>
        <td></td>
        <td><?=BillItem::model()->getAttributelabel('DisplayName')?></td>
        <td><?=BillItem::model()->getAttributelabel('value_per_unit')?></td>
        <td><?=BillItem::model()->getAttributelabel('quantity')?></td>
        <td><?=BillItem::model()->getAttributelabel('value')?></td>
        <td><?=BillItem::model()->getAttributelabel('vat_refusal')?></td>
    </tr>
    
<?php foreach ($service_bi as $w_bill_item) {?>
    <tr>
        <td><?= SIMAHtml::modelIcons($w_bill_item)?></td>
        <td><?=$w_bill_item->DisplayName?></td>
        <td><?=$w_bill_item->getAttributeDisplay('value_per_unit')?></td>
        <td><?=$w_bill_item->getAttributeDisplay('quantity')?></td>
        <td><?=$w_bill_item->getAttributeDisplay('value')?></td>
        <td><?=$w_bill_item->getAttributeDisplay('vat_refusal')?></td>
    </tr>
<?php } ?>

</table>

<?php } ?>

<?php if (!empty($fixed_assets_bi)) {?>
Osnovna sredstva:
<table class="cf_one_pixel_border">
    <tr>
        <td></td>
        <td><?=BillItem::model()->getAttributelabel('DisplayName')?></td>
        <td><?=BillItem::model()->getAttributelabel('value_per_unit')?></td>
        <td><?=BillItem::model()->getAttributelabel('quantity')?></td>
        <td><?=BillItem::model()->getAttributelabel('value')?></td>
    </tr>
    
<?php foreach ($fixed_assets_bi as $w_bill_item) {?>
    <tr>
        <td><?= SIMAHtml::modelIcons($w_bill_item)?></td>
        <td><?=$w_bill_item->DisplayName?></td>
        <td><?=$w_bill_item->getAttributeDisplay('value_per_unit')?></td>
        <td><?=$w_bill_item->getAttributeDisplay('quantity')?></td>
        <td><?=$w_bill_item->getAttributeDisplay('value')?></td>
    </tr>
<?php } ?>

</table>

<?php } ?>

