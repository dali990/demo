
	<h1><?php echo $model->getAttributeDisplay('payment_type');?>	
		<span class='edit'><?php echo SIMAHtml::modelFormOpen($model);?></span>
	</h1>	
	<p class='row'>
		<span class='title'><?php echo $model->getAttributeLabel('partner_id')?></span> 
		<span class='data'><?php echo $model->getAttributeDisplay('partner');?></span>
	</p>
	<p class='row'>
		<span class='title'><?php echo $model->getAttributeLabel('payment_date')?></span> 
		<span class='data'><?php echo $model->getAttributeDisplay('payment_date')?></span>
	</p>
	<p class='row'>
		<span class='title'><?php echo $model->getAttributeLabel('account')?></span> 
		<span class='data'><?php echo $model->getAttributeDisplay('account');?></span>
	</p>
        <p class='row'>
                <span class='title'><?php echo $model->getAttributeLabel('bank_statement')?></span> 
                <span class='data'><?php echo $model->getAttributeDisplay('bank_statement'); ?></span>
        </p>
	<p class='row'>
		<span class='title'><?php echo $model->getAttributeLabel('amount')?></span> 
		<span class='data'><?php echo $model->getAttributeDisplay('amount');?></span>
	</p>
	<p class='row'>
		<span class='title'><?php echo $model->getAttributeLabel('unreleased_amount')?></span> 
		<span class='data'><?php echo $model->getAttributeDisplay('unreleased_amount');?></span>
	</p>
	<p class='row'>
		<span class='title'><?php echo $model->getAttributeLabel('cost_location_id')?></span> 
		<span class='data'><?php echo $model->getAttributeDisplay('cost_location_id');?></span>
	</p>
        <p class='row'>
		<span class='title'><?php echo $model->getAttributeLabel('payment_code')?></span> 
		<span class='data'><?php echo $model->getAttributeDisplay('payment_code');?></span>
	</p>
        <p class='row'>
		<span class='title'><?php echo $model->getAttributeLabel('call_for_number')?></span> 
		<span class='data'><?php echo $model->getAttributeDisplay('call_for_number');?></span>
	</p>
        <p class='row'>
		<span class='title'><?php echo $model->getAttributeLabel('call_for_number_partner')?></span> 
		<span class='data'><?php echo $model->getAttributeDisplay('call_for_number_partner');?></span>
	</p>
        <p class='row'>
		<span class='title'><?php echo $model->getAttributeLabel('comment')?></span> 
		<span class='data'><?php echo $model->getAttributeDisplay('comment');?></span>
	</p>


