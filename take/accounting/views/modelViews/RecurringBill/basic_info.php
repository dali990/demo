
<p class="row name">
    <span class="title"><?php echo $model->getAttributeLabel('name')?>:</span> 
    <span class='data'><?php echo $model->getAttributeDisplay('name');?> </span>
</p>

<p class="row call_for_number">
    <span class="title"><?php echo $model->getAttributeLabel('call_for_number')?>:</span> 
    <span class='data'><?php echo $model->getAttributeDisplay('call_for_number');?> </span>
</p>

<p class="row partner_id">
    <span class="title"><?php echo $model->getAttributeLabel('partner_id')?>:</span> 
    <span class='data'><?php echo $model->getAttributeDisplay('partner_id');?> </span>
</p>

<p class="row description">
    <span class="title"><?php echo $model->getAttributeLabel('description')?>:</span> 
    <span class='data'><?php echo $model->getAttributeDisplay('description');?> </span>
</p>

