<div class="sima-layout-panel _horizontal">
    <div class="sima-layout-fixed-panel">
        <?=$this->renderModelView($model,'title_sums')?>
    </div>

    <div class="sima-layout-panel" >
        <div class="sima-layout-panel _vertical _splitter">
            <div class="sima-layout-panel">
                <?php $this->widget('SIMAGuiTable',[
                    'model' =>'BillOut',
                    'columns_type' => 'recurring_overview',
                    'fixed_filter' => [
                        'recurring_bill' => ['ids' => $model->id]
                    ]
                ]);?>
                </table>
            </div>
            <div class="sima-layout-panel" data-sima-layout-init='{"min_width":300,"max_width":400}'>
                Uplate: <br /><br />
                <?php
                foreach ($payments as $payment)
                {
                    echo $payment->DisplayHTML.'<br />';
                }
                ?>
            </div>
        </div>
    </div>
</div>