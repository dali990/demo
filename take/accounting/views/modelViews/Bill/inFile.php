<?php 
    if (!$model->canceled)
    {
        switch ($model->bill_type)
        {
            case Bill::$BILL:
            {
                if ($model->invoice)
                {
                    $model = BillOut::model()->findByPk($model->id);
                }
                else
                {
                    $model = BillIn::model()->findByPk($model->id);
                }
            }break;
            case Bill::$ADVANCE_BILL:
                if ($model->invoice)
                {
                    $model = AdvanceBillOut::model()->findByPk($model->id);
                }
                else
                {
                    $model = AdvanceBillIn::model()->findByPk($model->id);
                }
                break;
            case Bill::$RELIEF_BILL:
                if ($model->invoice)
                {
                    $model = ReliefBillOut::model()->findByPk($model->id);
                }
                else
                {
                    $model = ReliefBillIn::model()->findByPk($model->id);
                }
                break;
            case Bill::$UNRELIEF_BILL:
                if ($model->invoice)
                {
                    $model = UnReliefBillOut::model()->findByPk($model->id);
                }
                else
                {
                    $model = UnReliefBillIn::model()->findByPk($model->id);
                }
                break;
            case Bill::$PRE_BILL:
                if ($model->invoice)
                {
                    $model = PreBillOut::model()->findByPk($model->id);
                }
                else
                {
                    $model = PreBillIn::model()->findByPk($model->id);
                }
                break;
        }
    }
?>

<h3 class="sima-bi-info_heading"><?=$model->getAttributeDisplay('bill_type')?> <?=$model->DisplayName?></h3>
        

                <?php if ($model->canceled) {?>
            <div><span style="display:inline-block; margin: 5px 0; color: red; font-weight: bold">Dokument je storniran!!!</span></div>
                <?php }?>
        
    <div class='sima-bi-row amount'>
            <div><?php echo $model->getAttributelabel('amount'); ?>:</div>
            <div><?php echo $model->getAttributeDisplay('amount'); ?></div>
    </div>
    <?php if(isset($model->bound_currency)) { ?>
        <div class='sima-bi-row amount'>
                <div><?php echo $model->getAttributelabel('bound_currency'); ?>:</div>
                <div><?php echo $model->getAttributeDisplay('bound_currency'); ?></div>
        </div>
    <?php } ?>
    <div class='sima-bi-row job_order_name'>
        <div><?php echo $model->getAttributeLabel('confirm1')?>:</div>
        <div><?php echo SIMAHtml::status($model, 'confirm1');?></div>
    </div>
    <div class='sima-bi-row job_order_name'>
        <div><?php echo $model->getAttributeLabel('confirm2')?>:</div>
        <div><?php echo SIMAHtml::status($model, 'confirm2');?></div>
    </div>
    <div class='sima-bi-row job_order_name'>
            <div><?php echo $model->getAttributeLabel('cost_location')?>:</div>
            <div><?php echo $model->getAttributeDisplay('cost_location');?></div>
    </div>
    <div class='sima-bi-row partner_name'>
            <div><?php echo $model->getAttributeLabel('partner_id')?>:</div>
            <div><?php echo $model->getAttributeDisplay('partner_id');?></div>
    </div>
    <div class='sima-bi-row income_date'>
            <div><?php echo $model->getAttributelabel('income_date')?>: </div>
            <div><?php echo $model->income_date;?></div>
    </div>
    <div class='sima-bi-row payment_date'>
            <div><?php echo $model->getAttributelabel('payment_date')?>: </div>
            <div><?php echo $model->payment_date;?></div>
    </div>
    <div class='sima-bi-row income_date'>
            <div><?php echo $model->getAttributelabel('file.account_document.account_order.date')?>:</div>
            <div><?php echo $model->getAttributeDisplay('file.account_document.account_order.date');?></div>
    </div>
    <div class='sima-bi-row amount'>
            <div><?php echo $model->getAttributelabel('amount')?>:</div> 
            <div><?php echo $model->getAttributeDisplay('amount');?></div>
    </div>
    
    <?php if(isset($model->bound_currency)) { ?>
    <div class='sima-bi-row amount'>
            <div><?php echo $model->getAttributelabel('amount_in_currency'); ?>:</div> 
            <div><?php echo $model->getAttributeDisplay('amount_in_currency'); ?></div>
    </div>
    <?php } ?>
    
    <div class='sima-bi-row base_amount'>
            <div><?php echo $model->getAttributelabel('base_amount')?>:</div>
            <div><?php echo $model->getAttributeDisplay('base_amount');?></div>
    </div>
    <div class='sima-bi-row vat'>
            <div><?php echo $model->getAttributelabel('vat')?>:</div> 
            <div><?php echo $model->getAttributeDisplay('vat');?></div>
    </div>
    <?php if ($model->base_amount0 !== 0.00 && $model->base_amount0 !== 0) { ?>
        <div class='sima-bi-row base_amount'>
                <div><?php echo $model->getAttributelabel('base_amount0')?>:</div>
                <div><?php echo $model->getAttributeDisplay('base_amount0');?></div>
        </div>
    <?php } ?>
    <?php if ($model->base_amount10 !== 0.00 && $model->base_amount10 !== 0) { ?>
        <div class='sima-bi-row base_amount'>
                <div><?php echo $model->getAttributelabel('base_amount10')?>:</div> 
                <div><?php echo $model->getAttributeDisplay('base_amount10');?></div>
        </div>
    <?php } ?>
    <?php if ($model->vat10 !== 0.00 && $model->vat10 !== 0) { ?>
        <div class='sima-bi-row vat'>
                <div><?php echo $model->getAttributelabel('vat10')?>:</div>
                <div><?php echo $model->getAttributeDisplay('vat10');?></div>
        </div>
    <?php } ?>
    <?php if ($model->base_amount20 !== 0.00 && $model->base_amount20 !== 0) { ?>
        <div class='sima-bi-row base_amount'>
                <div><?php echo $model->getAttributelabel('base_amount20')?>:</div>
                <div><?php echo $model->getAttributeDisplay('base_amount20');?></div>
        </div>
    <?php } ?>
    <?php if ($model->vat20 !== 0.00 && $model->vat20 !== 0) { ?>
        <div class='sima-bi-row vat'>
                <div><?php echo $model->getAttributelabel('vat20')?>:</div> 
                <div><?php echo $model->getAttributeDisplay('vat20');?></div>
        </div>
    <?php } ?>
    <div class='sima-bi-row'>
            <div><?php echo $model->getAttributelabel('unreleased_amount')?>:</div>
            <div><?php echo $model->getAttributeDisplay('unreleased_amount');?></div>
    </div>
    <?php if(isset($model->bound_currency)) { ?>
            <div class='sima-bi-row amount'>
                    <div><?php echo $model->getAttributelabel('unreleased_amount_in_currency'); ?>:</div>
                    <div><?php echo $model->getAttributeDisplay('unreleased_amount_in_currency'); ?></div>
            </div>
    <?php } ?>
            
    <div class='sima-bi-row base_amount'>
            <div><?php echo $model->getAttributelabel('comment')?>:</div>
            <div><?php echo $model->getAttributeDisplay('comment');?></div>
    </div>  
 
<div class='sima-bi-row vat_refusal'>
    <?php if ($model->vat_refusal===true){?>
    <span class='sima_bi_notice'>Račun ulazi u obračun za odbitak PDV-a</span>
    <?php }elseif ($model->vat_refusal===false){?>
    <span class='sima_bi_notice'>Račun ne ulazi u obračun za odbitak PDV-a</span>
    <?php }else{?>
     <span class='sima_bi_notice'>Deo računa ulazi u obračun za odbitak PDV-a</span>
    <?php }?>
</div>
