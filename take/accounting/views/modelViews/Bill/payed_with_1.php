
<h4>Placanja:

    <?php 
    if(SIMAMisc::isVueComponentEnabled())
    {
        $this->widget(SIMAButtonVue::class,[
            'title' => 'Pregled',
            'onclick' => ['sima.dialog.openActionInDialog','accounting/billReleasing',[
                'get_params' => [
                    'bill_id' => $model->id
                ]
            ]]
        ]);
    }
    else
    {
        $this->widget('SIMAButton',[
            'action' => [
                'title' => 'Pregled',
                'onclick' => ['sima.dialog.openActionInDialog','accounting/billReleasing',[
                    'get_params' => [
                        'bill_id' => $model->id
                    ]
                ]]
            ]
        ]);
    }
    ?>

</h4>
<div>
<?php if (count($advances)>0) {?>
        Avansi:
        <ul>
            <?php foreach ($advances as $advance){?>
                <li><?=$advance?></li>
            <?php } ?>

        </ul>
<?php }?>


<?php if (count($prepared_payments)>0) {?>
        Placanja u pripremi:
        <ul>
            <?php foreach ($prepared_payments as $payment){?>
                <li><?=$payment?></li>
            <?php } ?>

        </ul>
<?php }?>
        
        
<?php if (count($payments)>0) {?>
        Placanja:
        <ul>
            <?php foreach ($payments as $payment){?>
                <li><?=$payment?></li>
            <?php } ?>

        </ul>
<?php }?>



<?php if (count($reliefs)>0) {?>
        Knjizna odobrenja:
        <ul>
            <?php foreach ($reliefs as $relief){?>
                <li><?=$relief?></li>
            <?php } ?>

        </ul>
<?php }?>

<?php if ($model->unreleased_amount > 0) {?>
        Preostalo:<?=$unreleased.' | '.$unreleased_percent?>
        <?php if (count($prepared_payments)>0){?>
            i plaćanja u pripremi
        <?php }?>
<?php } else if (count($prepared_payments)>0){?>
        Preostala samo plaćanja u pripremi
<?php }else{?>
        Sve placeno
<?php } ?>
</div>