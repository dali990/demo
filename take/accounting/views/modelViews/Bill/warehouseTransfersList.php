<?php if ($model->invoice) {?>

    <h3>Otpremnice:</h3>
    <ul>
    <?php foreach ($model->warehouse_dispatches as $req) {?>
        <li class=''>
            <?php echo $req->DisplayHtml?>
        </li>
    <?php } ?>
    </ul>
    
<?php }else{?>
    <h3>Prijemnice:</h3>
    <ul>
    <?php foreach ($model->warehouse_receives as $disp) {?>
        <li class=''>
            <?php echo $disp->DisplayHtml?>
        </li>
    <?php } ?>
    </ul>
<?php } ?>
