<ul>
<?php foreach ($model->warehouse_requisitions as $req) {?>
    <li>
        <?php echo SIMAHtml::modelFormOpen($req) ?>
        <img class="button delete" src="images/delete_16.png" onclick="sima.ajax.get('base/model/setColumnValue',{
            get_params:{
                model:'WarehouseRequisition',
                model_id:<?php echo $req->id?>,
                column:'bill_id'                
            },
            data:{value:''}
        });">
        <?php echo $req->DisplayName;?>
    </li>
<?php }?>
</ul>