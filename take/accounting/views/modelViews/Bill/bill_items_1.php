<?php 
    $uniq = SIMAHtml::uniqid();
    $bill = $model;
?>

<div class="sima-layout-panel _horizontal">
    <div class="sima-layout-fixed-panel">    
        <?=$buttons_row?>
    </div>
    <div class="sima-layout-panel">
        <?php
            $this->widget('base.extensions.SIMAGuiTable.SIMAGuiTable', [
                'id' => $gui_table_id,
                'model' => BillItem::model(),
                'add_button' => array(
                    'init_data'=>[
                        'BillItem'=>[
                            'bill'=>['ids' => $bill->id]
                        ]
                    ],
//                    'inline'=>true
                ),        
                'fixed_filter'=>array(
                    'bill'=>['ids' => $bill->id]
                ),
            ]);

        ?>
    </div>
</div>
    