<div class="sima-layout-panel _splitter _vertical">
    
    <div class="sima-layout-panel _horizontal" data-sima-layout-init='{"proportion":"0.7"}'>

        <div class="sima-layout-fixed-panel">
            <div  style="border-bottom: dotted">
                <?=$this->renderModelView($model->file,'belongsList');?>
            </div>
        </div>
        
        <div class="sima-layout-panel">
            <?php
                $this->widget('SIMAFilePreview', [
                    'file' => $model->file
                ]);
            ?>
        </div>

        
    </div>
    
    <div class="sima-layout-panel _horizontal" data-sima-layout-init='{"proportion":"0.3","min_width":"300","max_width":"800"}'>
        
        <div class="sima-layout-fixed-panel">
            <div  style="border-bottom: dotted">
                <?=$this->renderModelView($model->file,'filing_number_in_file');?>
            </div>
        </div>
        
        <div class="sima-layout-fixed-panel">
            <div  style="border-bottom: dotted">
                <?=$this->renderModelView($model,'payed_with');?>
            </div>
        </div>
        
        <div class="sima-layout-fixed-panel">
            <div  style="border-bottom: dotted">
                <?=$this->renderModelView($model->file,'full_info_signatures');?>
            </div>
        </div>
        
        
        
    </div>
    
</div>