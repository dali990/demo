
<h4>finansije</h4>
<?=$delete_button?>
    <p class='row'>
        <span class='title'><?php echo $model->getAttributeLabel('purchase_value'); ?>:</span>
        <span class='data'><?php echo $model->getAttributeDisplay('purchase_value'); ?></span>
    </p>
    
    <p class='row'>
        <span class='title'><?php echo $model->getAttributeLabel('current_purchase_value'); ?>:</span>
        <span class='data'><?php echo $model->getAttributeDisplay('current_purchase_value'); ?></span>
    </p>

    <p class='row'>
        <span class='title'><?php echo $model->getAttributeLabel('current_value'); ?>:</span>
        <span class='data'><?php echo $model->getAttributeDisplay('current_value'); ?></span>
    </p>
    
    <p class='row'>
        <span class='title'><?php echo $model->getAttributeLabel('depreciation_rate'); ?>:</span>
        <span class='data'><?php echo $model->getAttributeDisplay('depreciation_rate'); ?></span>
    </p>
    
    <p class='row'>
        <span class='title'><?php echo $model->getAttributeLabel('depreciation'); ?>:</span>
        <span class='data'><?php echo $model->getAttributeDisplay('depreciation'); ?></span>
    </p>
    
    <p class='row'>
        <span class='title'><?php echo $model->getAttributeLabel('usage_start_date'); ?>:</span>
        <span class='data'><?php echo $model->getAttributeDisplay('usage_start_date'); ?></span>
    </p>
