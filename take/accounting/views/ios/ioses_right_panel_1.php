<div class="sima-layout-panel _horizontal">

<div id="<?php echo $id_input_date;?>" data-year_id="<?php echo $year_id;?>" 
     class="sima-layout-fixed-panel"
     >
    <?php echo Yii::t('AccountingModule.Account', 'IosPublicDate').' : '; ?> <div style="display: inline; font-weight: bold;" class="ios_date_value"><?php echo $ios_date; ?></div>
    <?php 
    if(SIMAMisc::isVueComponentEnabled())
    {
        $this->widget(SIMAButtonVue::class,[
            'id'=>$ios_date_edit_btn,
            'half' => true,
            'icon' => '_edit',
            'icon_additional_classes' => ['_black'],
            'iconsize' => 16,
            'onclick'=>["sima.accountingMisc.setIosDate", $id_input_date],
        ]);
    }
    else
    {
        $this->widget('SIMAButton',[
            'id'=>$ios_date_edit_btn,
            'class'=>'_half',
            'action'=>[
                'icon'=>'_edit _16 _black sima-icon',
                'onclick'=>["sima.accountingMisc.setIosDate", $id_input_date],
            ]
        ]);
    }
    ?>
</div>

    <div class="sima-layout-panel">
<?php
$this->widget('base.extensions.SIMAGuiTable.SIMAGuiTable', [
    'id' => $id,
    'model' => Ios::model(),
    'title' => 'Izvodi otvorenih stavki',
    'fixed_filter'=>[
        'account'=>[
            'year'=>[
                'ids'=>$year_id
            ],
        ],
    ],
    'custom_buttons' => [
        Yii::t('AccountingModule.Account', 'GenerateIoses')=>[
            'func'=>"sima.accountingMisc.listIosCandidateCompanies('$id', $('#$id_input_date').data('year_id'));"
        ],
    ]
]);
?>
</div>
</div>