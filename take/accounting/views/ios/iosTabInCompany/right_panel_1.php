
<?php
    $this->widget('base.extensions.SIMAGuiTable.SIMAGuiTable', [
        'id' => "ioses_$uniq_id",
        'model' => Ios::model(),
        'title' => Yii::t('AccountingModule.Account','Ioses'),
        'columns_type' => 'single_company',
        'add_button'=>[
            'init_data'=>[
                'Ios' => [
                    'partner_id'=>[
                        'disabled',
                        'init'=>$company_id
                    ],
                    'ios_year_id' => $year_id
                ]
            ]
        ],
        'fixed_filter'=>[
            'partner' => [
                'ids' => $company_id,
            ],
            'account' => [
                'year' => [
                    'ids' => $year_id
                ]
            ]
        ],
        'custom_buttons' => [
            Yii::t('AccountingModule.Account', 'GenerateIos')=>[
                'func'=>"sima.accountingMisc.generateIosForCompany('ioses_$uniq_id', '$company_id');"
            ]
        ]
    ]);