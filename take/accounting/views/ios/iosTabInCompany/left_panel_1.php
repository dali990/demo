<?php
    $this->widget('SIMATree2', [
        'id' => "years_vmenu_$uniq_id",
        'class'=>'sima-vm',
        'list_tree_data'=>$menu_array,
        'default_selected'=>$year_id,
        'disable_unselect_item'=>true
    ]);
