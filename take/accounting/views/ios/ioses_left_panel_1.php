<?php

    $this->widget('SIMAVMenu', array(
        'id' => $id,
        'menu_array' => $menu_array
    ));
    
?>

<script type='text/javascript'>
    $(function() {
        $("#<?php echo $id; ?>").vmenu('selectItem',$('#<?php echo $id ?>').find("li[data-id='" + <?php echo $year_id; ?> + "']"));
        $("#<?php echo $right_panel_id; ?>").data('year_id', <?php echo $year_id; ?>);
        $("#<?php echo $id; ?>").on('selectItem', function(event, item){            
            var year_id = item.data('id');
            var ioses_table = $('#'+'<?php echo $right_panel_id; ?>');
            var curr_fixed_filter = ioses_table.simaGuiTable('getFixedFilter');
            curr_fixed_filter['account']['year']['ids'] = year_id;
            ioses_table.simaGuiTable('setFixedFilter', curr_fixed_filter).simaGuiTable('populate');
            $("#<?php echo $id_input_date; ?>").data('year_id',year_id);
            sima.ajax.get('accounting/ios/getIosDate', {
                get_params: {
                    year_id: year_id
                },
                success_function: function(response) {
                    $('#<?php echo $id_input_date;?>').find('.ios_date_value').html(response.html);
                }
            });
        });
    });
</script>
