<div class="sima-ioses-choose-companies">
    <div id="<?php echo $id; ?>" class='sima-ioses-choose-companies-settings sima-layout-panel'>
        <p style="text-align: left; padding-left: 10px;"><span style="font-weight:bold; font-size: 14px;">
                <?php echo Yii::t('AccountingModule.Account', 'ChooseCompaniesToGenerateIosFor').' '.$year;?> &nbsp;
        </span></p>
        <?php
        // global input
        $global_checked = 'checked';
        $global_disabled = '';
        ?>
        <ul class="sima-ioses-global">
            <li class="sima-ioses-global-checkbox">
                <input type="checkbox" name="global_checkbox" onclick="global_check_box_clicked<?php echo $uniq; ?>($(this))" <?php echo $global_checked; ?> <?php echo $global_disabled; ?>>
                (<?php echo Yii::t('AccountingModule.Account', 'ChooseAll');?>)
            </li>
        </ul>
        
        <ul class="sima-ioses-companies-list">
            <?php
            foreach ($companies as $company)
            {
                $checked = 'checked';
                $disabled = '';
                $company_id = $company->id;
            ?>
                <div class="sima-ioses-companies-list-item" data-company_id='<?php echo $company_id; ?>'>
                    <li>
                        <input type="checkbox" name="<?php echo $company_id; ?>" onclick="company_check_box_clicked<?php echo $uniq; ?>($(this))" <?php echo $checked; ?> <?php echo $disabled; ?>>
                    <?php echo $company->DisplayName; ?></li>
                </div>
            <?php } ?>
        </ul>
    </div>
</div>

<script>
    $("#<?php echo $uniq; ?>").find('.sima-ioses-companies-list').sortable();
    function company_check_box_clicked<?php echo $uniq; ?>(obj)
    {
        var count = 0;
        var checked;
        obj.parents('.sima-ioses-companies-list:first').find('input').each(function() {
            if ($(this).is(":checked"))
            {
                count++;
                checked = $(this);
            }
        });
        if (count === 0)
        {
            obj.prop('checked', true);
        }
    }
    
    function global_check_box_clicked<?php echo $uniq; ?>(obj)
    {
        $('.sima-ioses-companies-list:first').find('input').each(function() {
            
            if (obj.is(":checked"))
            {
                $(this).prop('checked', true);
            } else{
                $(this).prop('checked', false);
            }
        });
    }
    
</script>

