
<?php

class AccountingController extends SIMAController
{
    public function filters()
    {
        return array(
            'ajaxOnly'
            . '-generatePdfForGrossBalance'
            . '-exportAccountsToPdf'
            . '-generateAccountingPartnersReport',
        ) + parent::filters();
    }
    
    public function actionIndex()
    {
        $title = 'Finansijsko knjigovodstvo';
        $year_id = AccountingYear::getLastConfirmed()->id;        
        $uniq = SIMAHtml::uniqid();
        
        $years = AccountingYear::model()->findAll(new SIMADbCriteria([
            'Model'=>'AccountingYear',
            'model_filter'=>[
                'display_scopes'=>'byName'
            ]
        ]));

        $menu_array = [];
        foreach ($years as $year)
        {
            $menu_array[] = [
                'title' => $year->DisplayName,
                'code' => $year->id
            ];
        }
        
        //mora prvo da se izrenderuje desna strana, jer leva strana koristi desnu kada se prilikom renderovanja pozove automatsko selektovanje godine
        $right_panel = $this->renderPartial('accounting_right_panel', [
            'uniq'=>$uniq,
            'year_id'=>$year_id
        ], true, false);
        
        $left_menu = $this->renderPartial('accounting_left_panel', [
            'uniq'=>$uniq,
            'year_id'=>$year_id,
            'menu_array'=>$menu_array
        ], true, false);
        
        $show_all_accounts_button_id = $uniq.'_saabi';
        $hide_zeros_button_id = $uniq.'_hzbi';
        if(SIMAMisc::isVueComponentEnabled())
        {
            $show_all_accounts_button = Yii::app()->controller->widget(SIMAButtonVue::class, [
                'id' => $show_all_accounts_button_id,
                'title'=>Yii::t('AccountingModule.AccountsByYear','Show all'),
                'tooltip'=>Yii::t('AccountingModule.AccountsByYear','Show all'),
                'onclick'=>['sima.accounting.showAllAccounts', $uniq]
            ],true);
            $hide_zeros_button = Yii::app()->controller->widget(SIMAButtonVue::class, [
                'id' => $hide_zeros_button_id,
                'title'=>Yii::t('AccountingModule.AccountsByYear','Hide zeros'),
                'tooltip'=>Yii::t('AccountingModule.AccountsByYear','Hide zeros'),
                'onclick'=>['sima.accounting.hideZeroAccounts', $uniq]
            ],true);
            $open_book_orders_button = Yii::app()->controller->widget(SIMAButtonVue::class, [
                'title'=>'Nalozi za knjiženje',
                'onclick' => [
                    "sima.accounting.openBookOrdersInDialog", "$uniq"
                ]
            ],true);
            $reports_button = $this->widget(SIMAButtonVue::class, [
                'title' => 'Izveštaji',
                'subactions_data' => [
                    [
                        'title' => 'Bilans stanja xml',
                        'onclick' => [
                            "sima.accounting.prepareExportForApr", "$uniq", "balance_state"
                        ]
                    ],
                    //Sasa A - zakomentarisano dok se ne implementira
//                                [
//                                    'title' => 'Bilans stanja pdf',
//                                    'onclick' => [
//                                        "sima.notImplemented"
//                                    ]
//                                ],
                    [
                        'title' => 'Bilans uspeha xml',
                        'onclick' => [
                            "sima.accounting.prepareExportForApr", "$uniq", "balance_success"
                        ]
                    ],
//                                [
//                                    'title' => 'Bilans uspeha pdf',
//                                    'onclick' => [
//                                        "sima.notImplemented"
//                                    ]
//                                ],
                    [
                        'title' => 'Statistički izveštaj xml',
                        'onclick' => [
                            "sima.accounting.prepareExportForApr", "$uniq", "statistical_report"
                        ]
                    ],
//                                [
//                                    'title' => 'Statistički izveštaj pdf',
//                                    'onclick' => [
//                                        "sima.notImplemented"
//                                    ]
//                                ],
                    [
                        'title' => 'Bruto bilans',
                        'onclick' => [
                            "sima.accounting.grossBalance", "$uniq"
                        ]
                    ],
                    [
                        'title' => Yii::t('AccountingModule.AccountingPartnerReport', 'AccountingPartnersReport'),
                        'onclick' => [
                            "sima.accounting.accountingPartnersReport", "$uniq"
                        ]
                    ],
                    [
                        'title' => Yii::t('AccountingModule.Account','Ioses'),
                        'onclick' => [
                            "sima.dialog.openActionInDialog", "accounting/ios/listIoses"
                        ]
                    ]
                ]
            ],true);
            $settings_button = Yii::app()->controller->widget(SIMAButtonVue::class, [
                'icon' => '_main-settings',
                'iconsize' => 18,
                'subactions_data' => [
                    [
                        'title' => 'Kontni okviri',
                        'onclick' => [
                            "sima.dialog.openActionInDialog", "accounting/books/accountLayouts"
                        ]

                    ],
                    [
                        'title' => 'Finansijske godine',
                        'onclick' => [
                            "sima.dialog.openActionInDialog", "accounting/accounting/accountingYears"
                        ]

                    ],
                    [
                        'title' => Yii::t('AccountingModule.CostType','CostTypes'),
                        'onclick' => [
                            "sima.dialog.openActionInDialog", "accounting/costTypes", ['get_params' => ['cost_or_income' => 'COST']]
                        ]
                    ],
                    [
                        'title' => Yii::t('AccountingModule.CostType','IncomeTypes'),
                        'onclick' => [
                            "sima.dialog.openActionInDialog", "accounting/costTypes", ['get_params' => ['cost_or_income' => 'INCOME']]
                        ]
                    ],
                    [
                        'title' => Yii::t('AccountingModule.AccountsByYear','ChangeTransactionAccount'),
                        'onclick' => [
                            "sima.accounting.changeTransactionAccount", $uniq
                        ]

                    ]
                 ]
            ],true);
            $apply_daterange_button = Yii::app()->controller->widget(SIMAButtonVue::class, [
                'title'=>Yii::t('AccountingModule.AccountsByYear','Apply date range'),
                'tooltip'=>Yii::t('AccountingModule.AccountsByYear','Apply date range'),
                'onclick'=>['sima.accounting.applyAccountsByFilters', $uniq]
            ],true);
        }
        else
        {
            $show_all_accounts_button = Yii::app()->controller->widget('SIMAButton', [
                'id' => $show_all_accounts_button_id,
                'action'=>[ 
                    'title'=>Yii::t('AccountingModule.AccountsByYear','Show all'),
                    'tooltip'=>Yii::t('AccountingModule.AccountsByYear','Show all'),
                    'onclick'=>['sima.accounting.showAllAccounts', $uniq]
                ]
            ],true);
            $hide_zeros_button = Yii::app()->controller->widget('SIMAButton', [
                'id' => $hide_zeros_button_id,
                'action'=>[ 
                    'title'=>Yii::t('AccountingModule.AccountsByYear','Hide zeros'),
                    'tooltip'=>Yii::t('AccountingModule.AccountsByYear','Hide zeros'),
                    'onclick'=>['sima.accounting.hideZeroAccounts', $uniq]
                ]
            ],true);
            $open_book_orders_button = Yii::app()->controller->widget('SIMAButton', [
                'action'=>[ 
                    'title'=>'Nalozi za knjiženje',
                    'onclick' => [
                        "sima.accounting.openBookOrdersInDialog", "$uniq"
                    ]
                ]
            ],true);
            $reports_button = $this->widget('SIMAButton', [
                'action' => [
                    'title' => 'Izveštaji',
                    'subactions' => [
                        [
                            'title' => 'Bilans stanja xml',
                            'onclick' => [
                                "sima.accounting.prepareExportForApr", "$uniq", "balance_state"
                            ]
                        ],
                        //Sasa A - zakomentarisano dok se ne implementira
    //                                [
    //                                    'title' => 'Bilans stanja pdf',
    //                                    'onclick' => [
    //                                        "sima.notImplemented"
    //                                    ]
    //                                ],
                        [
                            'title' => 'Bilans uspeha xml',
                            'onclick' => [
                                "sima.accounting.prepareExportForApr", "$uniq", "balance_success"
                            ]
                        ],
    //                                [
    //                                    'title' => 'Bilans uspeha pdf',
    //                                    'onclick' => [
    //                                        "sima.notImplemented"
    //                                    ]
    //                                ],
                        [
                            'title' => 'Statistički izveštaj xml',
                            'onclick' => [
                                "sima.accounting.prepareExportForApr", "$uniq", "statistical_report"
                            ]
                        ],
    //                                [
    //                                    'title' => 'Statistički izveštaj pdf',
    //                                    'onclick' => [
    //                                        "sima.notImplemented"
    //                                    ]
    //                                ],
                        [
                            'title' => 'Bruto bilans',
                            'onclick' => [
                                "sima.accounting.grossBalance", "$uniq"
                            ]
                        ],
                        [
                            'title' => Yii::t('AccountingModule.AccountingPartnerReport', 'AccountingPartnersReport'),
                            'onclick' => [
                                "sima.accounting.accountingPartnersReport", "$uniq"
                            ]
                        ],
                        [
                            'title' => Yii::t('AccountingModule.Account','Ioses'),
                            'onclick' => [
                                "sima.dialog.openActionInDialog", "accounting/ios/listIoses"
                            ]
                        ]
                    ]
                ]
            ],true);
            $settings_button = Yii::app()->controller->widget('SIMAButton', [
                'action' => [
                    'icon'=>'sima-icon _main-settings _18',
                    'subactions' => [
                        [
                            'title' => 'Kontni okviri',
                            'onclick' => [
                                "sima.dialog.openActionInDialog", "accounting/books/accountLayouts"
                            ]

                        ],
                        [
                            'title' => 'Finansijske godine',
                            'onclick' => [
                                "sima.dialog.openActionInDialog", "accounting/accounting/accountingYears"
                            ]

                        ],
                        [
                        'title' => Yii::t('AccountingModule.CostType','CostTypes'),
                            'onclick' => [
                                "sima.dialog.openActionInDialog", "accounting/costTypes", ['get_params' => ['cost_or_income' => 'COST']]
                            ]
                        ],
                        [
                            'title' => Yii::t('AccountingModule.CostType','IncomeTypes'),
                            'onclick' => [
                                "sima.dialog.openActionInDialog", "accounting/costTypes", ['get_params' => ['cost_or_income' => 'INCOME']]
                            ]
                        ],
                        [
                            'title' => Yii::t('AccountingModule.AccountsByYear','ChangeTransactionAccount'),
                            'onclick' => [
                                "sima.accounting.changeTransactionAccount", $uniq
                            ]

                        ]
                    ]
                 ]
            ],true);
            $apply_daterange_button = Yii::app()->controller->widget('SIMAButton', [
                'action'=>[ 
                    'title'=>Yii::t('AccountingModule.AccountsByYear','Apply date range'),
                    'tooltip'=>Yii::t('AccountingModule.AccountsByYear','Apply date range'),
                    'onclick'=>['sima.accounting.applyAccountsByFilters', $uniq]
                ]
            ],true);
        }
        
        $html = Yii::app()->controller->renderContentLayout([
            'name' => $title,
            'title_width'=>'20%',
            'options_width'=>'80%',
            'options'=>[
                [
                    'html'=> $show_all_accounts_button
                ],
                [
                    'html'=> $hide_zeros_button
                ],
                [
                    'html'=>SIMAHtml::dateRange('accounts_by_date','',[
                        'id'=>$uniq.'_accounts_by_date',
                        'htmlOptions'=>[                            
                            'placeholder'=>Yii::t('AccountingModule.AccountsByYear','Input date range')
                        ]
                    ])
                ],
                [
                    'html'=> $apply_daterange_button
                ],
                [
                    'html'=> $open_book_orders_button
                ],
                [
                    'html' => $reports_button
                ],
                [
                    'html'=> $settings_button
                ],
            ]
        ], [
            [
                'proportion'=>0.1,
                'min_width'=>120,                
                'max_width'=>120,                
                'html'=>$left_menu
            ],
            [
                'proportion'=>0.9,
                'min_width'=>600,
                'max_width'=>2000,  
                'html'=>$right_panel
            ]
        ], [
            'id'=>$uniq,
            'class'=>'sima-accounting'
        ]);
        
        $this->respondOK([
            'title' => $title,
            'html' => $html
        ]);
    }
    
    public function actionAccountingYears()
    {
        $html = $this->renderPartial('accounting_years', [], true, false);
        
        $this->respondOK(array(
            'html' => $html,
            'title' => 'Finansijske godine',
            'color' => '#FED39E',
        ));
    }
    
    public function actionGetCostTypesByYear($year_id)
    {
        if (!isset($year_id))
            throw new Exception('actionGetCostTypesByYear - nisu lepo zadati parametri.');
        
        $year = Year::model()->findByPk($year_id);
        $prev_year = $year->prev();
        $html = $this->renderPartial('cost_types_by_year',array(
            'year'=>$year,
            'prev_year'=>$prev_year
        ),true,false);
        
        $this->respondOK(array(
            'html'=>$html
        ));
    }
    
    public function actionChangeAccountName($account_id)
    {
        $account = Account::model()->findByPkWithCheck($account_id);
        $account->name = $account->recalcName();
        $account->save();
        $this->respondOK();
    }
        
    public function actionGetGrossBalanceExportParams()
    {
        $params_chooser_uniq = SIMAHtml::uniqid();
        
        $html = $this->renderPartial('gross_balance/export_params_chooser', [
            'uniq' => $params_chooser_uniq
        ], true, false);
        
        $this->respondOK([
            'html' => $html,
            'params_chooser_uniq' => $params_chooser_uniq
        ]);
    }
    
    public function actionGetAccountingPartnersReportExportParams()
    {
        $params_chooser_uniq = SIMAHtml::uniqid();
        
        $html = $this->renderPartial('partners_report_params_chooser', [
            'uniq' => $params_chooser_uniq
        ], true, false);
        
        $this->respondOK([
            'html' => $html,
            'params_chooser_uniq' => $params_chooser_uniq
        ]);
    }
    
    public function actionGeneratePdfForGrossBalance()
    {
        $data = $this->setEventHeader();
        
        if (!isset($data['date']))
        {
            throw new SIMAException('actionGeneratePdfForGrossBalance - nije zadat datum');
        }
        
        $year_id = $data['year_id'];
        $date = $data['date'];
        $export_type = $this->filter_input($data, 'export_type');

        $year = date('Y', strtotime(SIMAHtml::UserToDbDate($date)));
        $start_date = "01.01.$year";
        $criteria = new SIMADbCriteria(array(
            'Model' => 'Account',
            'model_filter' => [
                'year' => [
                    'ids' => $year_id
                ],
                'display_scopes'=>[
                    'byName',
                    'withDCCurrents'=>[
                        $year_id, 
                        ['start_date'=>$start_date, 'end_date'=>$date]
                    ]
                ],
                'filter_scopes'=>'onlyOneAndThreeDigits'                
            ]
        ));                
        $accounts = Account::model()->findAll($criteria);
        
        if ($export_type === 'pdf')
        {
            $temporary_file = $this->generateAccountsPdf($accounts, [
                'title'=>'Bruto bilans',
                'date'=>$date,
                'cell_widths'=>[5,25,10,10,10,10,10,10,10]
            ]);
        }
        else if ($export_type === 'csv')
        {
            $temporary_file = $this->generateGrossBalanceCSV($accounts, $date);
        }
        else if ($export_type === 'ods' || $export_type === 'xls')
        {
            $temp_csv_file = $this->generateGrossBalanceCSV($accounts, $date);
            $temporary_file = $temp_csv_file->createNewTempFileConvertedTo($export_type, '44,34,76,1,,3081');
        }
        else
        {
            throw new SIMAException('Gross balance unknown export type.');
        }

        $this->sendEndEvent([
            'filename'=>$temporary_file->getFileName(),
            'downloadname'=>"Bruto bilans na dan {$date}$export_type"
        ]);
    }
    
    public function actionGenerateAccountingPartnersReport()
    {
        $data = $this->setEventHeader();
        $date = $this->filter_input($data, 'date');
        $export_type = $this->filter_input($data, 'export_type');
        $is_buyers = SIMAMisc::filter_bool_var($this->filter_input($data, 'is_buyers'));
        $is_suppliers = SIMAMisc::filter_bool_var($this->filter_input($data, 'is_suppliers'));
        $controller = $this;
        
        $accounting_partners_report = new AccountingPartnerReport($is_buyers, $is_suppliers, $date, 
            function($percent) use ($controller) {
                $controller->sendUpdateEvent($percent);
            }
        );
        
        if ($export_type === 'pdf')
        {
            $temp_file = $accounting_partners_report->getPDF();
        }
        else if ($export_type === 'xls')
        {
            $temp_csv_file = $accounting_partners_report->getCSV();
            $temp_file = $temp_csv_file->createNewTempFileConvertedTo($export_type, '44,34,76,1,,3081');
        }
        else
        {
            throw new SIMAException(Yii::t('AccountingModule.AccountingPartnerReport', 'UnknownExportType'));
        }

        $this->sendEndEvent([
            'filename' => $temp_file->getFileName(),
            'downloadname' => Yii::t('AccountingModule.AccountingPartnerReport', 'AccountingPartnersReportPerDay', ['{date}' => $date]) . $export_type
        ]);
    }
    
    public function actionExportAccountsToPdf()
    {
        $data = $this->setEventHeader();
        
        $guitable_filter = isset($data['guitable_filter'])?$data['guitable_filter']:[];
        $fixed_filter = isset($guitable_filter['fixed_filter'])?$guitable_filter['fixed_filter']:[];
        $select_filter = isset($guitable_filter['Account'])?$guitable_filter['Account']:[];
        
        $used_filters = [];
        GuiTableController::getRelationFilterParams(Account::model(), $fixed_filter, $used_filters);
        GuiTableController::getRelationFilterParams(Account::model(), $select_filter, $used_filters);
        $used_filters_html = SIMAGuiTable::renderUsedFiltersHtmlForPdf($used_filters);
        
        $criteria = new SIMADbCriteria(array(
            'Model' => 'Account',
            'model_filter' => $fixed_filter
        ));
        $select_criteria = new SIMADbCriteria(array(
            'Model' => 'Account',
            'model_filter' => $select_filter
        ));
        $criteria->mergeWith($select_criteria);       
        $accounts = Account::model()->findAll($criteria);

        $temporary_file = $this->generateAccountsPdf($accounts, [
            'title'=>'Bilans',
            'used_filters_html'=>$used_filters_html
        ]);
        
        $this->sendEndEvent([
            'filename'=>$temporary_file->getFileName(),
            'downloadname'=>'Bruto bilans.pdf'
        ]);        
    }
    
    private function generateAccountsPdf($accounts, $header_params=[])
    {
        $pdf = new SIMATCPdf('L', 'mm', 'A4', true, 'UTF-8', false);        
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetFont('freesans', '', 12);        
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);        
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        $pdf->SetMargins(PDF_MARGIN_LEFT, 10, PDF_MARGIN_RIGHT);
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $pdf->getAliasNbPages();
        $pdf->AddPage();

        $gross_balance_pdf_css = $this->renderPartial('gross_balance/pdf/gross_balance_pdf_css', [], true, false);
        
        $header_params['company'] = Yii::app()->company;
        $pdf_header_html = $this->renderPartial('gross_balance/pdf/gross_balance_pdf_header', $header_params, true, false);
        $pdf->writeHTML($pdf_header_html, true, false, true, false, 'C');
        
        if (!isset($header_params['cell_widths']))
        {
            $header_params['cell_widths'] = [8,22,10,10,10,10,10,10,10];
        }
        
        $table_header = $gross_balance_pdf_css.'<table width="100%" class="gross-balance-pdf-header" cellpadding="2">
            <tr class="account-pdf-content-head">
                <th width="'.$header_params['cell_widths'][0].'%">Konto</th>
                <th width="'.$header_params['cell_widths'][1].'%">Naziv i opis</th>
                <th width="'. 2*$header_params['cell_widths'][2].'%" colspan="2">Početno stanje</th>
                <th width="'. 2*$header_params['cell_widths'][4].'%" colspan="2">Promet</th>
                <th width="'. 2*$header_params['cell_widths'][6].'%" colspan="2">Ukupan promet</th>
                <th width="'.$header_params['cell_widths'][8].'%">Saldo</th>
            </tr>
            <tr class="account-pdf-content-head">
                <th width="'.$header_params['cell_widths'][0].'%"></th>
                <th width="'.$header_params['cell_widths'][1].'%"></th>
                <th width="'.$header_params['cell_widths'][2].'%">Duguje</th>
                <th width="'.$header_params['cell_widths'][3].'%">Potražuje</th>
                <th width="'.$header_params['cell_widths'][4].'%">Duguje</th>
                <th width="'.$header_params['cell_widths'][5].'%">Potražuje</th>
                <th width="'.$header_params['cell_widths'][6].'%">Duguje</th>
                <th width="'.$header_params['cell_widths'][7].'%">Potražuje</th>
                <th width="'.$header_params['cell_widths'][8].'%"></th>
            </tr>
        </table>';        
        $pdf->writeHTMLCell('', '', '', '', $table_header);
        $pdf->Ln();

        $i = 1;
        $accounts_cnt = count($accounts);
        $max_while_cycle_exec_time_seconds = ini_get('max_execution_time');
        foreach ($accounts as $account)
        {
            set_time_limit($max_while_cycle_exec_time_seconds);
            $percent = round(($i/$accounts_cnt)*100, 0, PHP_ROUND_HALF_DOWN);            
            $this->sendUpdateEvent($percent);                        
            $class = 'odd';
            if ($i % 2 == 0) 
            {
                $class = 'even';
            }            
            $height = ceil(strlen($account->name)/27) * 4;
            $curr_row = $gross_balance_pdf_css.'<table class="account-pdf-content"><tr class="account-pdf-content-body '.$class.'" nobr="true">
                            <td width="'.$header_params['cell_widths'][0].'%" class="_text-left">'.$account->code.'</td>
                            <td width="'.$header_params['cell_widths'][1].'%" class="_text-left">'.$account->name.'</td>
                            <td width="'.$header_params['cell_widths'][2].'%" class="_text-right">'.SIMAHtml::number_format($account->debit_start).'</td>
                            <td width="'.$header_params['cell_widths'][3].'%" class="_text-right">'.SIMAHtml::number_format($account->credit_start).'</td>
                            <td width="'.$header_params['cell_widths'][4].'%" class="_text-right">'.SIMAHtml::number_format($account->debit_current).'</td>
                            <td width="'.$header_params['cell_widths'][5].'%" class="_text-right">'.SIMAHtml::number_format($account->credit_current).'</td>
                            <td width="'.$header_params['cell_widths'][6].'%" class="_text-right">'.SIMAHtml::number_format($account->debit_sum).'</td>
                            <td width="'.$header_params['cell_widths'][7].'%" class="_text-right">'.SIMAHtml::number_format($account->credit_sum).'</td>
                            <td width="'.$header_params['cell_widths'][8].'%" class="_text-right">'.SIMAHtml::number_format($account->saldo).'</td>
                        </tr></table>';            
            
            $pdf->writeHTMLCell('', '', '', '', $curr_row);
            $pdf->Ln();
            $pdf->checkPageBreak($height,'',true);
            
            $i++;            
        }
        
        $pdf_footer_html = $this->renderPartial('gross_balance/pdf/gross_balance_pdf_footer', [
            
        ], true, false);
        $pdf->writeHTML($pdf_footer_html, true, false, true, false, 'C');
        
        $temporary_file = new TemporaryFile(null, null, 'pdf');
        $pdf->output($temporary_file->getFullPath(), 'F');
        
        return $temporary_file;
    }
    
    private function generateGrossBalanceCSV($accounts, $date)
    {
        $curr_company = Yii::app()->company;
        $csv = $curr_company->DisplayName . "\n";
        if (isset($curr_company->partner->main_address))
        {
            $csv .= $curr_company->partner->main_address->DisplayName . "\n";
        }
        $csv .= 'Datum: ' . SIMAHtml::DbToUserDate($date) . "\n";
        
        $csv .= "\nBruto bilans\n\n";

        $csv .= "Konto,Naziv i opis,Duguje početno stanje,Potražuje početno stanje,Duguje promet,Potražuje promet,Duguje ukupan promet,Potražuje ukupan promet,Saldo\n";

        $i = 1;
        $accounts_cnt = count($accounts);
        foreach ($accounts as $account)
        {
            $this->sendUpdateEvent(round(($i/$accounts_cnt)*100, 0, PHP_ROUND_HALF_DOWN));
            $csv .= '"' . str_replace(['"', "'"], '', $account->code) . '",';
            $csv .= '"' . str_replace(['"', "'"], '', $account->name) . '",';
            $csv .= $account->debit_start . ',';
            $csv .= $account->credit_start . ',';
            $csv .= $account->debit_current . ',';
            $csv .= $account->credit_current . ',';
            $csv .= $account->debit_sum . ',';
            $csv .= $account->credit_sum . ',';
            $csv .= $account->saldo . "\n";
            
            $i++;
        }

        $temporary_file = new TemporaryFile($csv, null, 'csv');
                
        return $temporary_file;
    }
    
    public function actionChooseAccountsForTransactionsChanges($year_id, $uniq)
    {
        $html = $this->renderPartial('choose_accounts_for_transactions_changes', [
            'year_id'=>$year_id,
            'uniq'=>$uniq
        ], true, false);
        
        $this->respondOK([
            'html' => $html
        ]);
    }
    
    public function actionChangeAccountTransactions($account_from_id, $account_to_id)
    {
        $account_from = Account::model()->findByPkWithCheck($account_from_id);
        $account_to = Account::model()->findByPkWithCheck($account_to_id);
        
        foreach ($account_from->account_transactions as $transaction)
        {
            $transaction->forceAccount($account_to);
            $transaction->save();
        }
        $this->respondOK([
            
        ]);
    }
 
    public function actionAddAuthorizationFileToBillOfExchange($bill_of_exchange_id)
    {
        $bill_of_exchange=  BillOfExchange::model()->findByPk($bill_of_exchange_id);
        if($bill_of_exchange==null)
        {
            throw new Exception("Ne postoji menica sa ID: ".$bill_of_exchange_id);
        }
        
        $document_type_id=Yii::app()->configManager->get('accounting.bill_of_exchange_authorization_file', false);
        
        $authorization_file=new File();
        $authorization_file->name = 'Menično ovlašćenje za menicu '.$bill_of_exchange->number;
        $authorization_file->responsible_id=Yii::app()->user->id;
        $authorization_file->document_type_id=$document_type_id;
        $authorization_file->save();
        $authorization_file->refresh();
        $bill_of_exchange->authorization_file_id = $authorization_file->id;
        $bill_of_exchange->save();
        
        $relevant_tags=array(
           Job::model()->tableName(),
           Employee::model()->tableName(),
           Bid::model()->tableName(),
           FixedAsset::model()->tableName()   
        );
        
        foreach ($bill_of_exchange->file->tags as $tag)
        {
            if(in_array($tag->model_table, $relevant_tags))
            {
                $authorization_file->addTagByModel($tag);
            }
            
        }
       
        $this->respondOK(array('updateViews'=>  SIMAHtml::getTag($bill_of_exchange)));
    }
    
    /**
     * akcija za multiselect od download naloga za knjizenje
     * ako nalog nije potvrdjen, vraca trenutno generisanu verziju
     * @throws Exception
     */
    public function actionFilesDownloadZipPrepare()
    {    
        $file_ids = $this->filter_post_input('file_ids');
        $filter = File::filter(array('ids'=>$file_ids));
        $filter->with = ['last_version','account_order'];
        $condition = new SIMADbCriteria();
        $condition->applyModel($filter);
        $files = File::model()->findAll($condition);

        $temporaryFile = new TemporaryFile('', null, 'zip');

        //kreiramo ili otvaramo zip folder u koji smestamo zeljene fajlove
        $zip = new ZipArchive();

        if ($zip->open($temporaryFile->getFullPath()) !== TRUE)
        {
            throw new Exception('cannot open'.$temporaryFile->getFullPath());
        }
        
        $errors = [];

        foreach ($files as $file)
        {
            try
            {
                if(empty($file) || empty($file->last_version))
                {
                    $errors['ProblemInFetchingFileDataForZip'][] = $file;
                    continue;
                }

                $last_version = $file->last_version;
                //ovo je specijalna situacija kada je shell 
                $_is_shell = false;
                
                if($last_version->type === FileVersion::$TYPE_SHELL)
                {
                    $_is_shell = true;
                }
                elseif(!$last_version->canDownload()) // shell se posebno obradjuje od ostalih nedostupnosti
                {
                    $errors['ProblemInFetchingFileByPathForZip'][] = $file;
                    continue;
                }

                if (empty($file->account_order))
                {
                    $errors['NotAO'][] = $file;
                    continue;
                }

                if ($file->account_order->booked && $_is_shell)
                {
                    $file->account_order->generatePdf($file->DisplayName.'_on_zip_download');
                    $file->refresh();
                    $last_version = $file->last_version;
                }
                
                if ($file->account_order->booked )
                {
                    $tempFile = TemporaryFile::CreateFromFileVersion($last_version);
                    $file_view = new FileView();
                    $file_view->user_id = Yii::app()->user->id;
                    $file_view->file_id = $file->id;
                    $file_view->file_version_id = $file->last_version_id;
                    $file_view->protocol = 'web'; //downloadzip
                    $file_view->save();
                    $download_name = $file->DisplayName;
                }
                else
                {
                    $account_order_report = new AccountOrderReport([
                        'account_order' => $file->account_order
                    ]);          
                    $tempFile = $account_order_report->getPDF();
                    $download_name = 'TEMP'.$file->DisplayName;
                    if (substr($download_name, -4) !== '.pdf')
                    {
                        $download_name .= '.pdf';
                    }
                }

                $download_name = str_replace('/', '_', $download_name);

                $zip->addFile($tempFile->getFullPath(), $download_name);
            }
            catch (SIMAExceptionFileVersionFileDontExists $e)
            {
                $errors['ProblemInFetchingFileByPathForZip'][] = $file;
                continue;
            }
            
        }//kraj forech

        $zip->close();
        
        $this->actionFilesDownloadZipPrepare_parseErrors($errors);
        
        $this->respondOK([
            'tn' => $temporaryFile->getFileName()
        ]);
    }
    
    private function actionFilesDownloadZipPrepare_parseErrors(array $errors)
    {
        if(empty($errors))
        {
            return;
        }
        
        $error_html = '';
        
        foreach($errors as $error_key => $error_files)
        {
            if(!empty($error_html))
            {
                $error_html .= '</br>';
            }
            
            $error_html .= Yii::t('AccountingModule.AccountOrder', $error_key);
            $error_html .= '<ul>';
            foreach($error_files as $error_file)
            {
                $error_html .= '<li>'.$error_file->DisplayName.'</li>';
            }
            $error_html .= '</ul>';
        }
        
        throw new SIMAWarnException($error_html);
    }
    
    public function actionGetBankModelByBankCode($code) 
    {        
        $bank_model = Bank::model()->find(new SIMADbCriteria([
            'Model' => Bank::model(),
            'model_filter' => [
                'bank_account_code' => $code
            ]
        ], true));
        
        if(empty($bank_model))
        {
            throw new SIMAWarnException(Yii::t('AccountingModule.Bank', 'NotFoundByCodeException', ['{code}' => $code]));
        }
        else
        {
            $this->respondOK([
                'id' => $bank_model->id,
                'display_name' => $bank_model->DisplayName,
            ]);
        }    
    }
}
