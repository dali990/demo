<?php

/**
 * @package SIMA
 * @subpackage Accounting
 */


class OverviewReportController extends SIMAController
{
    public function accessRules()
    {
        return array(
            array(
                'allow',
                'roles' => array('menuShowAccounts_OverviewReport')
            ),
            array(
                'deny'
            )
        );
    }
    
    public function actionIndex()
    {
        $title = Yii::t('AccountingModule.OverviewReport', 'OverviewReport');
        $html = $this->renderPartial('index',[
            
        ], true, false);
        
        $this->respondOK([
            'title' => $title,
            'html' => $html
        ]);
    }
}