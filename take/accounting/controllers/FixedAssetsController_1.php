<?php
/**
 * 
 */
class FixedAssetsController extends SIMAController
{
    
    public function filters()
    {
        return array(
            'ajaxOnly-exportInventoryList-calculateFADepreciations'
        ) + parent::filters();
    }
    
    public function actionIndex()
    {
        if (Yii::app()->user->checkAccess('AddWithOutBill',[],  FixedAsset::model()))
        {
            $add_button = true;
        }
        else
        {
            $add_button = false;
        }
        
        $html = $this->renderPartial('index',[
            'uniq' => SIMAHtml::uniqid(),
            'add_button' => $add_button
        ],true,true);
        
        $this->respondOK([
            'title' => 'Fin | OS',
            'html' => $html
        ]);
    }
    
    public function actionDepreciations()
    {
        
        $html = $this->renderPartial('depreciations',[
            'uniq' => SIMAHtml::uniqid()
        ],true,true);
        
        $this->respondOK([
            'title' => 'Fin | OS - amortizacija',
            'html' => $html
        ]);
    }
    
    public function actionCalculateFADepreciations()
    {
        $data = $this->setEventHeader();
        $depreciation_id = $this->filter_input($data, 'depreciation_id');
        
        $depreciation = FixedAssetsDepreciation::model()->findByPkWithCheck($depreciation_id);

        $start_date = "01.01.{$depreciation->year->year}";
        $end_date = "31.12.{$depreciation->year->year}";
        $_fixed_assets = AccountingFixedAsset::model()->byOrder()->findAll([
            'model_filter' => [
                'usage_start_date' => ['<=', $end_date],
                'fixed_asset' => [
                    'filter_scopes' => ['activeOnDate' => $end_date]
                ]
            ]
        ]);
        
        FixedAssetsDepreciationItem::model()->deleteAllByAttributes([
            'fixed_assets_depreciation_id' => $depreciation_id
        ]);
        
        $i = 1;
        $_fixed_assets_count = count($_fixed_assets);
        foreach ($_fixed_assets as $_fixed_asset)
        {
            $this->sendUpdateEvent(round(($i/$_fixed_assets_count)*100, 0, PHP_ROUND_HALF_DOWN));
            if (!empty($_fixed_asset->usage_start_date))
            {
                $begin = new SIMADateTime($_fixed_asset->usage_start_date);
                $year_start = new SIMADateTime($start_date);
                if ($year_start > $begin)
                {
                    $begin = $year_start;
                }
                $end = new SIMADateTime($end_date);

                $_dep_value = $_fixed_asset->current_value_start * 
                              ($_fixed_asset->depreciation_rate / 100) * 
                              ($_fixed_asset->fixed_asset->quantityOnDate($end) / $_fixed_asset->fixed_asset->quantity) * 
                              $_fixed_asset->fixed_asset->getDaysDiffPercent($begin, $end);
                
                $_note = '';
                if (SIMAMisc::lessThen(0.0, $_fixed_asset->current_value))
                {
                    if (SIMAMisc::lessOrEqualThen($_fixed_asset->current_value, $_dep_value))
                    {
                        $_dep_value = $_fixed_asset->current_value;
                        $_note = 'Nova vrednost je 0';
                    }

                    if (SIMAMisc::lessThen(0.0, $_dep_value))
                    {
                        $_dep_item = new FixedAssetsDepreciationItem();
                        $_dep_item->fixed_assets_depreciation_id = $depreciation_id;
                        $_dep_item->accounting_fixed_asset_id = $_fixed_asset->id;
                        $_dep_item->depreciation = $_dep_value;
                        $_dep_item->note = $_note;
                        $_dep_item->save();
                    }
                }
            }
            
            $i++;
        }
        
        $this->sendEndEvent();
    }
    
    public function actionGetDateFilterForExportInventoryList()
    {
        $this->respondOK([
            'title' => Yii::t('AccountingModule.FixedAssets', 'ChooseDateForExportInventoryList'),
            'html' => SIMAHtml::datetimeFieldByName('date_for_fixed_assets_inventory_list', '', [
                'readonly' => true,
                'htmlOptions' => [
                    'placeholder' => Yii::t('AccountingModule.FixedAssets', 'ExportInventoryListOnDayPlaceholder')
                ]
            ])
        ]);
    }
    
    public function actionExportInventoryList()
    {
        $data = $this->setEventHeader();
        $date = $this->filter_input($data, 'date', false);
        if (empty($date))
        {
            $date = SIMAHtml::currentDateTime(false);
        }
        
        $date_object = new SIMADateTime($date);

        $fixed_assets_html = "
            <table class='table'>
                <tr>
                    <th width='5%'>".Yii::t('FixedAssetsModule.FixedAsset', 'InventoryListFinancialNumber')."</th>
                    <th width='30%'>".Yii::t('FixedAssetsModule.FixedAsset', 'Name')."</th>
                    <th width='10%'>".Yii::t('FixedAssetsModule.FixedAsset', 'Quantity')."</th>
                    <th width='8%'>".Yii::t('FixedAssetsModule.FixedAsset', 'PurchaseDate')."</th>
                    <th width='8%'>".Yii::t('FixedAssetsModule.FixedAsset', 'DepreciationRate')."</th>
                    <th width='13%'>".Yii::t('FixedAssetsModule.FixedAsset', 'PurchaseValue')."</th>
                    <th width='13%'>".Yii::t('FixedAssetsModule.FixedAsset', 'ValueOff')."</th>
                    <th width='13%'>".Yii::t('FixedAssetsModule.FixedAsset', 'CurrentValue')."</th>
                </tr>
        ";
        
        $i = 1;
        $purchase_value_total_total = 0;
        $value_off_total_total = 0;
        $current_value_total_total = 0;
        $fixed_asset_types_cnt = FixedAssetType::model()->withAllNames()->count();
        $fixed_asset_type_part = round(100 / $fixed_asset_types_cnt, 0, PHP_ROUND_HALF_DOWN);
        $fixed_asset_type_prev_part_sum = 0;
        FixedAssetType::model()->findAllByParts(function($fixed_asset_types) use (
                &$fixed_assets_html, $date, $fixed_asset_type_part, &$fixed_asset_type_prev_part_sum, &$i,
                &$purchase_value_total_total, &$value_off_total_total, &$current_value_total_total, $date_object
        ) {
            $is_first = true;
            foreach ($fixed_asset_types as $fixed_asset_type)
            {
                $ac_fixed_assets_criteria = new SIMADbCriteria([
                    'Model' => AccountingFixedAsset::class,
                    'model_filter' => [
                        'usage_start_date' => ['<=', $date],
                        'fixed_asset' => [
                            'fixed_asset_type' => [
                                'ids' => $fixed_asset_type->id
                            ],
                            'filter_scopes' => ['activeOnDate' => $date]
                        ]
                    ]
                ]);
                $ac_fixed_assets_cnt = AccountingFixedAsset::model()->count($ac_fixed_assets_criteria);
                if ($ac_fixed_assets_cnt > 0)
                {
                    if ($is_first === true)
                    {
                        $fixed_assets_html .= "
                            <tr class='divider-half'>
                                <td colspan='7'></td>
                            </tr>
                        ";
                        $is_first = false;
                    }
                    
                    $fixed_assets_html .= "
                        <tr class='group-name'>
                            <td class='align-left' colspan='7'>{$fixed_asset_type->display_name_full}</td>
                        </tr>
                        <tr class='divider-half'>
                            <td colspan='7'></td>
                        </tr>
                    ";
                    
                    $purchase_value_total = 0;
                    $value_off_total = 0;
                    $current_value_total = 0;
                    
                    $j = 1;
                    AccountingFixedAsset::model()->findAllByParts(function($ac_fixed_assets) use (
                            &$fixed_assets_html, $ac_fixed_assets_cnt, $fixed_asset_type_part, &$fixed_asset_type_prev_part_sum, &$j,
                            &$purchase_value_total, &$value_off_total, &$current_value_total, $date_object
                    ) {
                        foreach ($ac_fixed_assets as $ac_fixed_asset)
                        {
                            $percent = round($fixed_asset_type_prev_part_sum + (($j/$ac_fixed_assets_cnt)*$fixed_asset_type_part), 0, PHP_ROUND_HALF_DOWN);
                            $this->sendUpdateEvent($percent);
                            
                            $fixed_asset = $ac_fixed_asset->fixed_asset;

                            $financial_number = $ac_fixed_asset->order;
                            $purchase_date = $ac_fixed_asset->usage_start_date;
                            $depreciation_rate = $ac_fixed_asset->depreciation_rate;
                            $purchase_value = $ac_fixed_asset->purchaseValueOnDate($date_object);
                            $value_off = $ac_fixed_asset->depreciationOnDate($date_object);
                            $current_value = $purchase_value - $value_off;
                            
                            $purchase_value_total += $purchase_value;
                            $value_off_total += $value_off;
                            $current_value_total += $current_value;
                            
                            $fixed_assets_html .= "
                                <tr>
                                    <td>$financial_number</td>
                                    <td class='align-left'>{$fixed_asset->DisplayName}</td>
                                    <td class='align-right'>".SIMAHtml::number_format($fixed_asset->quantityOnDate($date_object),3)."</td>
                                    <td>".SIMAHtml::DbToUserDate($purchase_date)."</td>
                                    <td class='align-right'>".SIMAHtml::number_format($depreciation_rate)."</td>
                                    <td class='align-right'>".SIMAHtml::number_format($purchase_value)."</td>
                                    <td class='align-right'>".SIMAHtml::number_format($value_off)."</td>
                                    <td class='align-right'>".SIMAHtml::number_format($current_value)."</td>
                                </tr>
                            ";

                            $j++;
                        }
                    }, $ac_fixed_assets_criteria);
                    
                    $fixed_assets_html .= "
                            <tr class='balance'>
                                <td class='no-border-right' colspan='3'></td>
                                <td class='align-left no-border-left-right'>".Yii::t('BaseModule.Common', 'Total')."</td>
                                <td class='no-border-left'></td>
                                <td class='align-right'>".SIMAHtml::number_format($purchase_value_total)."</td>
                                <td class='align-right'>".SIMAHtml::number_format($value_off_total)."</td>
                                <td class='align-right'>".SIMAHtml::number_format($current_value_total)."</td>
                            </tr>
                            <tr class='divider'>
                                <td colspan='7'></td>
                            </tr>
                    ";
                    
                    $purchase_value_total_total += $purchase_value_total;
                    $value_off_total_total += $value_off_total;
                    $current_value_total_total += $current_value_total;
                }
                
                $fixed_asset_type_prev_part_sum += $fixed_asset_type_part;
                
                $i++;
            }

            $fixed_assets_html .= "
                <tr class='balance'>
                    <td class='no-border-right' colspan='2'></td>
                    <td class='align-left no-border-left-right'>".Yii::t('FixedAssetsModule.FixedAsset', 'TotalTotal')."</td>
                    <td class='no-border-left'></td>
                    <td class='align-right'>".SIMAHtml::number_format($purchase_value_total_total)."</td>
                    <td class='align-right'>".SIMAHtml::number_format($value_off_total_total)."</td>
                    <td class='align-right'>".SIMAHtml::number_format($current_value_total_total)."</td>
                </tr>
            ";
        }, new SIMADbCriteria([
            'Model' => FixedAssetType::class,
            'model_filter' => [
                'display_scopes' => 'withAllNames',
                'fixed_assets' => []
            ]
        ]));
        
        $fixed_assets_html .= '</table>';

        $company_logo_path = Yii::app()->company->getLogoImagePath();
        
        $pdf_html = $this->renderPartial('inventory_list_pdf/html', [
            'date' => $date,
            'company_logo_path' => $company_logo_path,
            'fixed_assets_html' => $fixed_assets_html
        ], true, false);
        
        $temporary_file = new TemporaryFile('', null, 'pdf');

        $temporary_file->createPdfFromHtml(
            [
                'bodyHtml' => $pdf_html
            ],
            [
                'user-style-sheet' => $this->viewPath . '/inventory_list_pdf/css.css'
            ]
        );

        $this->sendEndEvent([
            'filename' => $temporary_file->getFileName(),
            'downloadname' => Yii::t('AccountingModule.FixedAssets', 'ExportInventoryListOnDayPdfName', [
                '{date}' => SIMAHtml::DbToUserDate($date)
            ]) . '.pdf'
        ]);
    }
}
