<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class PaymentController extends SIMAController
{
    
    public function actionIndex($id)
    {
        $payment = Payment::model()->findByPk($id);
        if ($payment == null)
        {
            throw new Exception('zadato Placanje ne postoji!!');
        }

        $advance_payment_type = Yii::app()->configManager->get('accounting.advance_payment_type');
        if ($payment->payment_type_id == $advance_payment_type)
        {
            $payment = Advance::model()->findByPk($payment->id);
        }
        
        $html = $this->renderDefaultModelView($payment, [
            'tabs_params' => [
                'default_selected' => 'info',
            ]
        ]);

        $this->respondOK(array(
            'html' => $html,
            'title' => 'Placanje ' . $payment->id
        ));
    }
    
    public function actionForPay()
    {
        $html = $this->renderPartial('forpay', array(),true,true);
        $name = 'Za platiti';
        
        $this->respondOK(array(
            'title' => $name,
            'html' => $html
        ));
    }
    
    public function actionExport()
    {
        $payment_ids = $this->filter_post_input('payment_ids');
        $conf_id = $this->filter_post_input('conf_id');
        $account_id = $this->filter_post_input('account_id');
        
        $configuration = PaymentConfiguration::model()->findByPk($conf_id);
        if($configuration == null)
        {
            throw new Exception("configuration is null");
        }
        
        $bankAccount = BankAccount::model()->findByPk($account_id);
        if($bankAccount == null)
        {
            throw new Exception("account is null");
        }
        
        $criteria = new SIMADbCriteria([
            'Model' => 'Payment',
            'model_filter' => [
                'ids' => $payment_ids
            ]
        ]);
        $payments = Payment::model()->findAll($criteria);
        if($payments == null || count($payments) == 0)
        {
            $this->respondFail("no payments");
        }
        
        $this->generateExport_validatePayments($payments);
        
        if($configuration->export_format === "xml")
        {
            $export_content = $this->generateExportContentXML($payments, $bankAccount);
        }
        else if($configuration->export_format === "txt" || $configuration->export_format === 'CSV')
        {
            $export_content = $this->generateExportContentTXT($payments, $bankAccount);
        }
        else if($configuration->type == PaymentConfiguration::$TYPE_RAIFFEISEN)
        {
            $exporter = new BankStatementExporter($bankAccount, $payments);
            $exporter->run();
            $export_content = $exporter->export_content;
            $configuration->export_format = 'XML';
        }
        else
        {
            throw new Exception("unknown export format");
        }
               
        $tempname = TempDir::createNewTempFile($export_content, '', $configuration->export_format);
        
        $this->respondOK([
            'tn' => $tempname,
            'dn' => 'Placanja.'.$configuration->export_format
        ]);
    }
    
    private function generateExport_validatePayments(array $payments)
    {
        $error_messages = [];
        
        foreach($payments as $payment)
        {
            if(!isset($payment->partner->main_or_any_address))
            {
                $error_messages[] = Yii::t(
                        'AccountingModule.Payment', 
                        'PartnerDoesNotHaveAddress',
                        array(
                            '{partner}' => $payment->partner
                        ));
            }
            if(empty($payment->account))
            {
                $error_messages[] = Yii::t('AccountingModule.Payment', 'PaymentDoNotHaveBankAccount', [
                    '{payment}' => $payment->DisplayName
                ]);
            }
        }
                
        SIMAMisc::parseAndDisplayErrorsStringArray($error_messages);
    }
    
    private function generateExportContentXML(array $payments, BankAccount $bankAccount)
    {
        $params = array(
            'payments' => $payments,
            'company_name' => Yii::app()->company->DisplayName,
            'company_address' => Yii::app()->company->getAttributeDisplay('MainAddress'),
            'account_number' => $bankAccount->number,
            'bank_name' => $bankAccount->bank->DisplayName,
        );
        
        $export_content = $this->renderPartial('export_xml',$params,true);
        
        return $export_content;
    }
    
    private function generateExportContentTXT(array $payments, BankAccount $bankAccount)
    {
        /// 20171023 MilosJ: greske bi trebalo da se validiraju pre ove f-je u generateExport_validatePayments
//        $error_messages = array();
        
        $payments_sum = 0;
        $payments_count = 0;
        $final_payments = array();
        foreach($payments as $payment)
        {
//            $payment_errors = [];
//        
//            if(!isset($payment->partner->main_or_any_address))
//            {
//                $payment_errors[] = Yii::t(
//                        'AccountingModule.Payment', 
//                        'PartnerDoesNotHaveAddress',
//                        array(
//                            '{partner}' => $payment->partner
//                        ));
//            }
//            if(empty($payment->account))
//            {
//                $payment_errors[] = Yii::t('AccountingModule.Payment', 'PaymentDoNotHaveBankAccount', [
//                    '{payment}' => $payment->DisplayName
//                ]);
//            }
//            if(count($payment_errors) > 0)
//            {
//                $error_messages = array_merge($error_messages, $payment_errors);
//                continue;
//            }
            
            $final_payments[] = array(
                "account_number" => $this->exportTXTformatAccountNumber($payment->account->number),
                "partner_display_name" => $this->exportTXTformatPadRightWithMaxLen($payment->partner->DisplayName, 35),
                "partner_city" => $this->exportTXTformatPadRightWithMaxLen($payment->partner->main_or_any_address->city->name, 10),
                "call_for_number" => $this->exportTXTformatPadRightWithMaxLen($payment->call_for_number, 23),
                "code_name" => $this->exportTXTformatPadRightWithMaxLen($payment->payment_code->name, 36),
                "code" => $payment->payment_code->code,
                "amount" => $this->exportTXTformatAmount($payment->amount),
                "call_for_number_modul_partner" => $this->exportTXTformatPadRightWithMaxLen($payment->call_for_number_modul_partner, 2),
                "call_for_number_partner" => $this->exportTXTformatPadRightWithMaxLen($payment->call_for_number_partner, 23),
                "date" => $this->exportTXTformatPaymentDate($payment->payment_date)
            );
            
            $payments_sum += $payment->amount;
            $payments_count++;
        }
        
//        SIMAMisc::parseAndDisplayErrorsStringArray($error_messages);
        
        $final_payments_sum = str_pad(str_replace('.', '', str_replace(',', '', $payments_sum)), 15, '0', STR_PAD_LEFT);
        $final_payments_count = str_pad($payments_count, 5, '0', STR_PAD_LEFT);
                
        $_dt = new DateTime();

        $params = array(
            'payments' => $final_payments,
            'payments_sum' => $final_payments_sum,
            'payments_count' => $final_payments_count,
            'company_account' => $this->exportTXTformatAccountNumber($bankAccount->number),
            'company_name' => $this->exportTXTformatPadRightWithMaxLen(Yii::app()->company->DisplayName,35),
            'company_address' => $this->exportTXTformatPadRightWithMaxLen(Yii::app()->company->main_or_any_address->city->name, 10),
            'payment_date' => $_dt->format('dmy'),
        );
        
        $export_content = $this->renderPartial('export_txt',$params,true);
        
        return $export_content;
    }
    
    private function exportTXTformatAccountNumber($account_number)
    {
        return str_replace('-', '', $account_number);
    }
    private function exportTXTformatAmount($amount)
    {
        $maxLen = 13;
        $result = str_pad(str_replace(['.',',',' '], '', SIMAHtml::number_format($amount,2)), 13, '0', STR_PAD_LEFT);
        if(strlen($result) > $maxLen)
        {
            $result = substr($result, 0, $maxLen);
        }
        
        return $result;
    }
    private function exportTXTformatPaymentDate($paymentDate)
    {
        $dt = new DateTime($paymentDate);
        $format = 'dmy';
            
        return $dt->format($format);
    }
    private function exportTXTformatPadRightWithMaxLen($inputString, $maxLen)
    {
        $result = str_pad(SIMAHtml::removeSerbianLettersAndUpper($inputString), $maxLen, ' ', STR_PAD_RIGHT);
        if(strlen($result) > $maxLen)
        {
            $result = substr($result, 0, $maxLen);
        }
        
        return $result;
    }
}