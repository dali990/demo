<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class BillController extends SIMAController
{
    public function actionBillPdfSuggestion()
    {
        $data = $this->setEventHeader();

        $bill_id = $this->filter_input($data, 'bill_id');

        $report_obj = new BillReport([
            'bill_id' => $bill_id
        ]);
        
        $this->sendUpdateEvent(10);
        
        $temp_file = $report_obj->getPDF();
        
        $this->sendUpdateEvent(90);
        
        $pdf_as_image = $temp_file->renderPdfAsImage($temp_file->getFullPath());
        
        $this->sendUpdateEvent(100);
        
        $this->sendEndEvent([
            'html' => $pdf_as_image,
            'temp_file_name' => $temp_file->getFileName(),
            'temp_file_downloadname' => $report_obj->getDownloadName()
        ]);
    }
    
    public function actionBillPdfAddNewVersionFromTempFile($bill_id, $temp_file_name, $temp_file_downloadname)
    {
        $bill = Bill::model()->findByPkWithCheck($bill_id);
        
        $bill->file->appendTempFile($temp_file_name, $temp_file_downloadname);
        
        $this->respondOK([], $bill);
    }
    
    public function actionConnectBillItemsAndCostLocations()
    {
        $not_selected_message = "Morate izabrati i stavku koja je usluga i mesto troska";
        try
        {
            $bill_item_ids = $this->filter_post_input('bill_item_ids');
            $cost_location_ids = $this->filter_post_input('cost_location_ids');
        }
        catch (SIMAException $e)
        {
            $this->respondFail($not_selected_message);
        }
        
        $bill_items = BillItem::model()->findAllByModelFilter([
            'ids' => $bill_item_ids
        ]);
        $cost_locations = CostLocation::model()->findAllByModelFilter([
            'ids' => $cost_location_ids
        ]);
        
        if (count($bill_items) < 1 || count($cost_locations) < 1)
        {
            $this->respondFail($not_selected_message);
        }
        if (count($cost_locations) > 1)
        {
            $this->respondFail("Trenutno ne postoji podrska za vise od jednog mesta troska");
        }

        
        foreach($bill_items as $bill_item)
        {
            $bill_item->cost_location_id = $cost_locations[0]->id;//MOZE NULA ZBOG PROVERA IZNAD
            if (!$bill_item->save())
            {
                error_log(SIMAHtml::showErrors($bill_item));
            }
        }
        
//        error_log(SIMAMisc::toTypeAndJsonString($bill_item_ids));
//        error_log(SIMAMisc::toTypeAndJsonString($cost_location_ids));
        
        $this->respondOK();
    }
}
