<?php
/**
 * Description of BooksController
 *
 * @author Milos Sreckovic
 */
class AccountPlansController extends SIMAController
{
    public function filters()
    {
        return array(
            'ajaxOnly-exportForApr'
        ) + parent::filters();
    }
    
    /**
     * akcija koja sluzi samo za pretragu konta
     * obavezno je poslati year_id u filterima
     * @throws Exception
     */
    public function actionSearchAccounts()
    {
        $for_filter = $this->filter_post_input('filters');
        if (!isset($for_filter['initial']['year_id']) && !isset($for_filter['initial']['year']['ids']))
        {
            throw new Exception('actionSearchAccounts - nije zadat year_id');
        }
        
        //izvlacimo year_id i text. Polje text brisemo iz niza jer cemo ga obradjivati posebno zbog wildcard-a
        $text = isset($for_filter['initial']['text'])?$for_filter['initial']['text']:'';        
        $year_id = isset($for_filter['initial']['year_id'])?$for_filter['initial']['year_id']:$for_filter['initial']['year']['ids'];

        //pretraga svih konta
        $account = Account::model();        
        $criteriaAccount = new SIMADbCriteria();
        foreach ($for_filter as $_filter_key=>$_filter_value) 
        {           
            $_temp_criteria = new SIMADbCriteria(array(
                'Model'=>'Account',
                'model_filter'=>$_filter_value
            ));
            $criteriaAccount->mergeWith($_temp_criteria);
        }
        $criteriaAccount->limit = 10;        
        $account_settings = $account->modelSettings('searchField');
        $account_scopes = (isset($account_settings['scopes']))?$account_settings['scopes']:array();
        foreach ($account_scopes as $account_scope)
        {
            $account = $account->$account_scope();
        }
        $accounts = $account->byName()->findAll($criteriaAccount);
        $accounts_codes = [];
        foreach ($accounts as $_account)
        {
            array_push($accounts_codes, $_account->code);
        }
        
        //izvlacimo law_id na osnovu godine
        $year = Year::model()->findByPk($year_id);
        $law_id = $year->param('accounting.account_layout_law_id');
                
        //pretraga svih okvira
        $account_layout = AccountLayout::model();
        $criteria = new SIMADbCriteria(array(
            'Model'=>'AccountLayout', 
            'model_filter'=>array(
                'law_id' => $law_id,
                'text' => $text
            )
        ));
        $criteria->limit = 10;        
        $account_layout_settings = $account_layout->modelSettings('searchField');
        $account_layout_scopes = (isset($account_layout_settings['scopes']))?$account_layout_settings['scopes']:array();
        foreach ($account_layout_scopes as $account_layout_scope)
        {
            $account_layout = $account_layout->$account_layout_scope();
        }
        $account_layouts = $account_layout->byName()->findAll($criteria);

        $new_account_layouts = [];
        foreach ($account_layouts as $account_layout)
        {
            if (!in_array($account_layout->code, $accounts_codes))
            {
                array_push($new_account_layouts, $account_layout);
            }
        }

        $html = $this->renderPartial('searchField', array(
            'accounts' => $accounts,
            'account_layouts' => $new_account_layouts,
            'text' => $text,
            'year_id' => $year_id
        ), true, true);
        
        $this->respondOK(array(
            'result'=>$html
        ));
    }
    
    
    public function actionExportForApr()
    {
        $data = $this->setEventHeader();

        $name = $this->filter_input($data, 'name');
        $year_id = $this->filter_input($data, 'year_id');
        
        $curr_year = Year::model()->findByPkWithCheck($year_id);
        
        $report_class = null;
        if($name == "balance_state")
        {
            $report_class = new AccountPlansBalanceStateReport($curr_year);
        }
        else if($name == "balance_success")
        {
            $report_class = new AccountPlansBalanceSuccessReport($curr_year);
        }
        else if($name == "statistical_report")
        {
            $report_class = new AccountPlansBalanceStatisticalReport($curr_year);
        }
        
        $xml_temp_file = $report_class->getXML();

        $this->sendEndEvent([
            'fn' => $xml_temp_file->getFileName(),
            'dn' => $report_class->getReportName() . '.xml'
        ]);
    }
    
    public function actionExportAccountLayouts($law_id)
    {
        $law = AccountLayoutLaw::model()->findByPk($law_id);
        $account_layouts = AccountLayout::model()->findAllByAttributes([
            'law_id'=>$law_id
        ]);
        $account_layouts_export = [];
        foreach ($account_layouts as $account_layout)
        {
            $temp = [];
            $temp['code'] = $account_layout->code;
            $temp['name'] = $account_layout->name;
            array_push($account_layouts_export, $temp);
        }
        
        $json_export = CJSON::encode($account_layouts_export);
        $temp_file_name = TempDir::createNewTempFile($json_export,'','txt');        
        
        $this->respondOK([
            'fn' => $temp_file_name,
            'dn' => 'kontni-okviri('.$law->DisplayName.')'
        ]);
    }
    
    public function actionImportAccountLayouts($law_id, $upload_session_key, $overwrite_plans)
    {
        $law = AccountLayoutLaw::model()->findByPk($law_id);
        $decoded_key = json_decode($upload_session_key);        
        $real_path = TempDir::getFullPath($decoded_key->temp_file_name);        
        $fileContent = trim(file_get_contents($real_path));
        if (empty($fileContent))
        {
            throw new SIMAException('Uvozni fajl je prazan.');
        }
        $overwrite_plans = filter_var($overwrite_plans, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
        
        //brisu se svi okviri
        if ($overwrite_plans === true)
        {
            $this->deleteAllAccountLayoutsRecursive($law->account_layouts_without_parent);
        }
        
        //importujemo
        $account_layouts = CJSON::decode($fileContent);
        //dodajemo kodove
        foreach ($account_layouts as $account_layout)
        {
            $_account_layout = AccountLayout::model()->findByAttributes(array('code'=>$account_layout['code'], 'law_id'=>$law->id));
            if (is_null($_account_layout))
            {
                $new_account_layout = new AccountLayout();
                $new_account_layout->law_id = $law->id;
                $new_account_layout->code = $account_layout['code'];
                $new_account_layout->name = $account_layout['name'];
                $new_account_layout->save();
            }
            else
            {                
                $_account_layout->name = $account_layout['name'];
                $_account_layout->save();
            }
        }
        
        
        //brisemo one koje se ne nalaze u spisku
        foreach ($law->account_layouts as $account_layout)
        {
            if ($this->isAccountLayoutInArray($account_layouts, $account_layout->code) === false)
            {
                $account_layout->delete();
            }
        }
        
        $this->respondOK();
    }
    
    private function deleteAllAccountLayoutsRecursive($account_layouts)
    {        
        foreach ($account_layouts as $account_layout)
        {            
            if (count($account_layout->children) > 0)
            {                
                $this->deleteAllAccountLayoutsRecursive($account_layout->children);
            }            
            $account_layout->delete();
        }
    }
    
    private function isAccountLayoutInArray($account_layouts, $account_layout_code)
    {
        $founded = false;
        foreach ($account_layouts as $account_layout)
        {
            if ($account_layout['code'] === $account_layout_code)
            {
                $founded = true;
            }
        }
        
        return $founded;
    }
}
