<?php
/**
 * Description of BaseController
 *
 * @author goran-set
 */
class BillReleasingController extends SIMAController
{
    
    public function filters()
    {
        return array(
            'ajaxOnly-AddOrEditAdvanceBillReleases-AddOrEditBillReliefs-CancelReleases'
        ) + parent::filters();
    }
    
    public function actionIndex($bill_id)
    {
        $bill = AnyBill::model()->findByPkWithCheck($bill_id);
        $render_view = 'bill_info';
        if (!$bill->canceled)
        {
            switch ($bill->bill_type) 
            {
                case Bill::$ADVANCE_BILL:
                    $render_view = 'bill_advance_info';
                    break;
                case Bill::$RELIEF_BILL:
                    $render_view = 'bill_relief_info';
                    break;
                case Bill::$UNRELIEF_BILL:
                    $render_view = 'bill_unrelief_customs_info';
                    break;
            }
        }
        
        $render_params = $this->getRenderParams($bill);

        $html = $this->renderPartial($render_view, $render_params, true);
        
        
        $this->respondOK([
            'title' => 'Info placanja | '.$bill->partner->DisplayName.' | '.$bill->DisplayName,
            'html' => $html
        ]);
    }
    
    
    private function getRenderParams($bill)
    {
        $ret = [
            'bill' => $bill
        ];
        switch ($bill->bill_type)
        {
            case Bill::$BILL:                
                $bill = Bill::model()->findByPk($bill->id);
                
                if ($bill->connected)
                {
                    $payment_proposals_number = 0;
                    $advance_bill_proposals_number = 0;
                    $relief_bill_proposals_number = 0;
                }
                else
                {
                    //izdvajamo model_filter jer je isti u sva tri slucaja
                    $model_filter = [
                        'scopes'=>[
                            'unreleased'
                        ],
                        'partner' => ['ids'=>$bill->partner_id],
                        'invoice' => $bill->invoice
                    ];
               
                    $payment_proposals_number = Payment::model()->count([
                        'model_filter' => $model_filter
                    ]);

                    $relief_bill_proposals_number = ReliefBill::model()->count([
                        'model_filter' => $model_filter
                    ]);
                    
                    $model_filter['connected'] = true;
                    $advance_bill_proposals_number = AdvanceBill::model()->count([
                        'model_filter' => $model_filter
                    ]);
                }
                
                $ret = [
                    'bill' => $bill,
                    'payment_proposals_number'=>$payment_proposals_number,
                    'advance_bill_proposals_number' => $advance_bill_proposals_number,
                    'relief_bill_proposals_number' => $relief_bill_proposals_number
                ];
                break;
            case Bill::$ADVANCE_BILL:                
                $bill = AdvanceBill::model()->findByPk($bill->id);
                
                if ($bill->connected)
                {
                    $advance_proposals_number = 0;
                    $relief_bill_proposals_number = 0;
                }
                else
                {
                    $advance_model = ($bill->invoice===true)?'AdvanceIn':'AdvanceOut';
                    $advance_criteria = new SIMADbCriteria([
                        'Model'=>$advance_model,
                        'model_filter'=>[
                            'like_bill_id'=>$bill->id,
                            'scopes'=>[
                                'unreleased'
                            ]
                        ]
                    ]);
                    $advance_proposals_number = $advance_model::model()->count($advance_criteria);
                    
                    $relief_model = ($bill->invoice===true)?'ReliefBillOut':'ReliefBillIn';
                    $relief_criteria = new SIMADbCriteria([
                        'Model'=>$relief_model,
                        'model_filter'=>[
                            'like_bill_id'=>$bill->id,
                            'scopes'=>[
                                'unreleased'
                            ]
                        ]
                    ]);
                    $relief_bill_proposals_number = $relief_model::model()->count($relief_criteria);
                }
                
                if ($bill->isReleased)
                {
                    $bill_proposals_number = 0;
                }
                else
                {                    
                    $bill_model = ($bill->invoice===true)?'BillOut':'BillIn';
                    $bill_criteria = new SIMADbCriteria([
                        'Model'=>$bill_model,
                        'model_filter'=>[
                            'like_advance_bill_id'=>$bill->id,
                            'scopes'=>[
                                'unreleased'
                            ]
                        ]
                    ]);
                    $bill_proposals_number = $bill_model::model()->count($bill_criteria);
                }
                
                $ret = [
                    'bill' => $bill,
                    'advance_proposals_number' => $advance_proposals_number,
                    'bill_proposals_number' => $bill_proposals_number,
                    'relief_bill_proposals_number' => $relief_bill_proposals_number,
                ];
                break;
            case Bill::$RELIEF_BILL:                
                $bill = ReliefBill::model()->findByPk($bill->id);
                if (abs($bill->unreleased_amount) < 0.001)
                {
                    $bill_proposals_number = 0;
                    $advance_bill_proposals_number = 0;
                }
                else
                {
                    $bill_model = ($bill->invoice===true)?'BillOut':'BillIn';
                    $bill_criteria = new SIMADbCriteria([
                        'Model'=>$bill_model,
                        'model_filter'=>[
                            'like_relief_bill_id'=>$bill->id,
                            'scopes'=>[
                                'unreleased'
                            ]
                        ]
                    ]);
                    $bill_proposals_number = $bill_model::model()->count($bill_criteria);
                    
                    $advance_bill_model = ($bill->invoice===true)?'AdvanceBillOut':'AdvanceBillIn';
                    $advance_bill_criteria = new SIMADbCriteria([
                        'Model'=>$advance_bill_model,
                        'model_filter'=>[
                            'like_relief_bill_id'=>$bill->id,
                            'scopes'=>[
                                'unreleased'
                            ]
                        ]
                    ]);
                    $advance_bill_proposals_number = $bill_model::model()->count($advance_bill_criteria);
                }
                
                $ret = [
                    'bill' => $bill,
                    'bill_proposals_number' => $bill_proposals_number,
                    'advance_bill_proposals_number' => $advance_bill_proposals_number
                ];
                break;
            
//                $bill = UnReliefBill::model()->findByPk($bill->id);
//                $ret = [
//                    'bill' => $bill
//                ];
//                break;
            case Bill::$UNRELIEF_BILL:
                $bill = Bill::model()->findByPk($bill->id);
                
                if ($bill->connected)
                {
                    $payment_proposals_number = 0;
                }
                else
                {
                    //izdvajamo model_filter jer je isti u sva tri slucaja
                    $model_filter = [
                        'like_bill_id'=>$bill->id,
                        'scopes'=>[
                            'unreleased'
                        ]
                    ];

                    $payment_model = ($bill->invoice===true)?'PaymentIn':'PaymentOut';
                    $criteria_payment = new SIMADbCriteria([
                        'Model'=>$payment_model,
                        'model_filter'=>$model_filter
                    ]);                
                    $payment_proposals_number = $payment_model::model()->count($criteria_payment);
                }
                
                $ret = [
                    'bill' => $bill,
                    'payment_proposals_number'=>$payment_proposals_number
                ];
                break;
        }
        
        return $ret;
    }
    
    /**
     * Povezivanje racuna i placanja
     */
    public function actionAdd() 
    {
        $bill_ids = $this->filter_post_input('bill_ids');
        $payment_ids = $this->filter_post_input('payment_ids');
        
        foreach ($bill_ids as $bill_id)
        {
            foreach ($payment_ids as $payment_id)
            {
                $bill = Bill::model()->findByPkWithCheck($bill_id);
                $payment = Payment::model()->findByPkWithCheck($payment_id);
                if ($bill->isPreBill)
                {
                    ConnectAccountingDocuments::PreBillToPayment($bill->file->pre_bill, $payment);
                }
                else
                {
                    ConnectAccountingDocuments::BillToPayment($bill, $payment);
                }
            }
        }
        $this->respondOK();
    }
    
    /**
     * povezivanje avansnih uplata i avansnih racuna
     */
    public function actionConnectAdvance() 
    {
        $advance_bill_ids = $this->filter_post_input('advance_bill_ids');
        $advance_ids = $this->filter_post_input('advance_ids');

        foreach ($advance_bill_ids as $advance_bill_id)
        {
            $advance_bill = AdvanceBill::model()->findByPk($advance_bill_id);
            foreach ($advance_ids as $advance_id)
            {
                $advance = Advance::model()->findByPk($advance_id);
                if ($advance_bill->connected)
                {
                    $this->respondFail('Račun je već povezan');
                }
                if ($advance->connected)
                {
                    $this->respondFail('Avans je već povezan');
                }
                $_br = BillRelease::model()->findByAttributes(['bill_id' => $advance_bill_id,'payment_id' => $advance_id]);
                if (is_null($_br))
                {
                    $_br = new BillRelease();
                    $_br->bill_id = $advance_bill_id;
                    $_br->payment_id = $advance_id;
                    $_br->amount = 0;
                    if ($_br->validate())
                    {
                        $_br->save();
                    }
                    else
                    {
                        $this->respondFail(SIMAHtml::showErrorsInHTML($_br));
                    }
                }
            }
        }
        
        $this->respondOK();
    }
    
    /**
     * Povezivanje racuna i knjiznih odobrenja
     */
    public function actionAddRelief() 
    {
        $bill_ids = $this->filter_post_input('bill_ids');
        $relief_bill_ids = $this->filter_post_input('relief_bill_ids');
        foreach ($bill_ids as $bill_id)
        {
            foreach ($relief_bill_ids as $relief_bill_id)
            {
                $bill = Bill::model()->findByPk($bill_id);
                $relief_bill = ReliefBill::model()->findByPk($relief_bill_id);
                
                ConnectAccountingDocuments::BillToReliefBill($bill,$relief_bill);
            }
        }
        $this->respondOK();
    }
    
    /**
     * povezivanje racuna sa avansnim racunima
     */
    public function actionAddAdvance() 
    {
        $bill_ids = $this->filter_post_input('bill_ids');
        $advance_bill_ids = $this->filter_post_input('advance_bill_ids');
        foreach ($bill_ids as $bill_id)
        {   
            foreach ($advance_bill_ids as $advance_bill_id)
            {
                $bill = Bill::model()->findByPk($bill_id);
                $advance_bill = AdvanceBill::model()->findByPk($advance_bill_id);
                
                ConnectAccountingDocuments::BillToAdvanceBill($bill,$advance_bill);
            }
        }
        $this->respondOK();
    }

    /**
     * povezivanje avansnih racuna sa knjiznim odobrenjima
     */
    public function actionAddAdvanceBillRelief() 
    {
        $relief_bill_ids = $this->filter_post_input('relief_bill_ids');
        $advance_bill_ids = $this->filter_post_input('advance_bill_ids');
        foreach ($relief_bill_ids as $relief_bill_id)
        {            
            foreach ($advance_bill_ids as $advance_bill_id)
            {
                $advance_bill = AdvanceBill::model()->findByPk($advance_bill_id);
                $relief_bill = ReliefBill::model()->findByPk($relief_bill_id);
                
                ConnectAccountingDocuments::AdvanceBillToReliefInternal($advance_bill,$relief_bill);
            }
        }
        $this->respondOK();
    }
    
    public function actionChooseFictiveAdvance($advance_bill_id)
    {
        $advance_bill = AdvanceBill::model()->findByPkWithCheck($advance_bill_id);
        
        $year_for_account_start = $advance_bill->year->next();
        
        $account = $advance_bill->partner->getSupplierAdvanceAccount($year_for_account_start);
        
        $account_w_saldo = Account::model()->withDCCurrents($year_for_account_start->id)->findByPk($account->id);
        
        if(is_null($account_w_saldo))
        {
            $note='nema naloga';
        }
        else if (!SIMAMisc::areEqual($advance_bill->amount, $account_w_saldo->debit_start))
        {
            $note = "Saldo naloga: ".SIMAHtml::number_format($account_w_saldo->debit_start)
                    .'<br/>Vrednost avansnog racuna: '. SIMAHtml::number_format($advance_bill->amount);
        }
        else if (is_null($account_w_saldo->StartAccountTransaction))
        {
            $note = "CUDAN BUG";
        }
        else
        {
            $link = AdvanceBillFictiveAccountLink::model()->findByAttributes([
                'advance_bill_id' => $advance_bill->id
            ]);
            if (!is_null($link))
            {
                $this->respondFail('Avansni racun je vec povezan sa fiktivnim linkom');
            }
            $link = new AdvanceBillFictiveAccountLink();
            $link->advance_bill_id = $advance_bill->id;
            $link->account_transaction_id = $account_w_saldo->StartAccountTransaction->id;
            $link->value = $advance_bill->amount;
            $link->save();
//        }
//        else
//        {
            $note = 'sve je ok';
        }
        
        $this->respondOK([],[$advance_bill,$account_w_saldo]);
    }
    
    
    /**
     * Povezivanje racuna i knjiznih odobrenja
     */
    public function actionAddDebtTakeOver() 
    {
        $bill_ids = $this->filter_post_input('bill_ids');
        $debt_take_over_ids = $this->filter_post_input('debt_take_over_ids');
        foreach ($bill_ids as $bill_id)
        {
            foreach ($debt_take_over_ids as $debt_take_over_id)
            {
                $bill = Bill::model()->findByPk($bill_id);
                $debt_take_over = DebtTakeOver::model()->findByPk($debt_take_over_id);
                
                if ($bill->isBill)
                {
                    ConnectAccountingDocuments::BillToDebtTakeOver($bill,$debt_take_over);
                }
                elseif($bill->isReliefBill)
                {
                    ConnectAccountingDocuments::ReliefBillToDebtTakeOver($bill->file->relief_bill,$debt_take_over);
                }
                else
                {
                    error_log(__METHOD__.' ne postoji implementiran nacin povezivanja za bill_id: '.$bill->id);
                }
            }
        }
        $this->respondOK();
    }
    /**
     * Povezivanje racuna i knjiznih odobrenja
     */
    public function actionAddDebtTakeOverToPayment() 
    {
        $payment_ids = $this->filter_post_input('payment_ids');
        $debt_take_over_ids = $this->filter_post_input('debt_take_over_ids');
        foreach ($payment_ids as $payment_id)
        {
            foreach ($debt_take_over_ids as $debt_take_over_id)
            {
                $payment = Payment::model()->findByPk($payment_id);
                $debt_take_over = DebtTakeOver::model()->findByPk($debt_take_over_id);
                
                ConnectAccountingDocuments::PaymentToDebtTakeOver($payment,$debt_take_over);
            }
        }
        $this->respondOK();
    }
    
    public function actionCancelRelease($model_name) 
    {
        $model_ids = $this->filter_post_input('model_ids');
        foreach ($model_ids as $model_id)
        {
            $model = $model_name::model()->findByPkWithCheck($model_id);
            $model->delete();
        }
        $this->respondOK();
    }
    
    public function actionCancelReleases() 
    {
        $data = $this->setEventHeader(); 
        $selected_releases = $this->filter_input($data, 'selected_releases');
        $bill_id = $this->filter_input($data, 'bill_id');
        $i = 1;
        $selected_releases_cnt = count($selected_releases);
        foreach ($selected_releases as $selected_release)
        {
            $percent = round(($i/$selected_releases_cnt)*100, 0, PHP_ROUND_HALF_DOWN);
            $this->sendUpdateEvent($percent);
            if($selected_release["model_name"] === 'AdvanceBill')
            {
                $advance_bill_releases = AdvanceBillRelease::model()->findAll([
                    'model_filter' => [
                        'bill' => [
                            'ids' => $bill_id
                        ],
                        'advance_bill' => [
                            'ids' => $selected_release["model_id"]
                        ],
                    ]
                ]);
                foreach ($advance_bill_releases as $advance_bill_release)
                {
                    $advance_bill_release->delete();
                }
            }
            else if($selected_release["model_name"] === 'ReliefBill')
            {
                $bill_reliefs = BillRelief::model()->findAll([
                    'model_filter' => [
                        'bill' => [
                            'ids' => $bill_id
                        ],
                        'relief_bill' => [
                            'ids' => $selected_release["model_id"]
                        ],
                    ]
                ]);
                
                foreach ($bill_reliefs as $bill_relief)
                {
                    $bill_relief->delete();
                }
            }
            else 
            {
                $model = $selected_release["model_name"]::model()->findByPkWithCheck($selected_release["model_id"]);
                $model->delete();
            }
            $i++;
        }
       
        $this->sendEndEvent([]);
    }
    
    public function actionAddOrEditAdvanceBillReleases() 
    {
        $data = $this->setEventHeader(); 
        $advance_bill_id = $this->filter_input($data, 'advance_bill_id');
        $bill_id = $this->filter_input($data, 'bill_id');
        $base_amount0r = $this->filter_input($data, 'base_amount0r');
        $base_amount10r = $this->filter_input($data, 'base_amount10r');
        $base_amount20r = $this->filter_input($data, 'base_amount20r');
        $vat10r = $this->filter_input($data, 'vat10r');
        $vat20r = $this->filter_input($data, 'vat20r');
        $base_amount0i = $this->filter_input($data, 'base_amount0i');
        $base_amount10i = $this->filter_input($data, 'base_amount10i');
        $base_amount20i = $this->filter_input($data, 'base_amount20i');
        $vat10i = $this->filter_input($data, 'vat10i');
        $vat20i = $this->filter_input($data, 'vat20i');
        
        if($base_amount0r > 0)
        {
            $this->addOrEditAdvanceBillRelease($bill_id, $advance_bill_id, 0, false, $base_amount0r, 0);
        }
        if($base_amount10r > 0)
        {
            $this->addOrEditAdvanceBillRelease($bill_id, $advance_bill_id, 10, false, $base_amount10r, $vat10r);
        }
        if($base_amount20r > 0)
        {
            $this->addOrEditAdvanceBillRelease($bill_id, $advance_bill_id, 20, false, $base_amount20r, $vat20r);
        }
        if($base_amount0i > 0)
        {
            $this->addOrEditAdvanceBillRelease($bill_id, $advance_bill_id, 0, true, $base_amount0i, 0);
        }
        if($base_amount10i > 0)
        {
            $this->addOrEditAdvanceBillRelease($bill_id, $advance_bill_id, 10, true, $base_amount10i, $vat10i);
        }
        if($base_amount20i > 0)
        {
            $this->addOrEditAdvanceBillRelease($bill_id, $advance_bill_id, 20, true, $base_amount20i, $vat20i);
        }
        $this->sendEndEvent([]);
    }
    
    private function addOrEditAdvanceBillRelease($bill_id, $advance_bill_id, $vat_rate, $internal_vat, $base_amount, $vat_amount)
    {
        $advance_bill_release = AdvanceBillRelease::model()->findByAttributes([
            'bill_id' => $bill_id,
            'advance_bill_id' => $advance_bill_id,
            'vat_rate' => $vat_rate, 
            'internal_vat' => $internal_vat
        ]);
        $amount = $internal_vat ? $base_amount : $base_amount + $vat_amount;       
        if (is_null($advance_bill_release))
        {
            $advance_bill_release = new AdvanceBillRelease();
            $advance_bill_release->bill_id = $bill_id;
            $advance_bill_release->advance_bill_id = $advance_bill_id;
            $advance_bill_release->vat_rate = $vat_rate;
            $advance_bill_release->internal_vat = $internal_vat;
        }
        $advance_bill_release->base_amount = $base_amount;
        $advance_bill_release->vat_amount = $vat_amount;
        $advance_bill_release->amount = $amount;
        $advance_bill_release->save();
    }
    
    public function actionAddOrEditBillReliefs() 
    {
        $data = $this->setEventHeader(); 
        $relief_bill_id = $this->filter_input($data, 'relief_bill_id');
        $bill_id = $this->filter_input($data, 'bill_id');
        $base_amount0r = $this->filter_input($data, 'base_amount0r');
        $base_amount10r = $this->filter_input($data, 'base_amount10r');
        $base_amount20r = $this->filter_input($data, 'base_amount20r');
        $vat10r = $this->filter_input($data, 'vat10r');
        $vat20r = $this->filter_input($data, 'vat20r');
        $base_amount0i = $this->filter_input($data, 'base_amount0i');
        $base_amount10i = $this->filter_input($data, 'base_amount10i');
        $base_amount20i = $this->filter_input($data, 'base_amount20i');
        $vat10i = $this->filter_input($data, 'vat10i');
        $vat20i = $this->filter_input($data, 'vat20i');
        
        if($base_amount0r !== 0)
        {
            $this->addOrEditBillRelief($bill_id, $relief_bill_id, 0, false, $base_amount0r, 0);
        }
        if($base_amount10r !== 0)
        {
            $this->addOrEditBillRelief($bill_id, $relief_bill_id, 10, false, $base_amount10r, $vat10r);
        }
        if($base_amount20r !== 0)
        {
            $this->addOrEditBillRelief($bill_id, $relief_bill_id, 20, false, $base_amount20r, $vat20r);
        }
        if($base_amount0i !== 0)
        {
            $this->addOrEditBillRelief($bill_id, $relief_bill_id, 0, true, $base_amount0i, 0);
        }
        if($base_amount10i !== 0)
        {
            $this->addOrEditBillRelief($bill_id, $relief_bill_id, 10, true, $base_amount10i, $vat10i);
        }
        if($base_amount20i !== 0)
        {
            $this->addOrEditBillRelief($bill_id, $relief_bill_id, 20, true, $base_amount20i, $vat20i);
        }
        $this->sendEndEvent([]);
    }
    
    private function addOrEditBillRelief($bill_id, $relief_bill_id, $vat_rate, $internal_vat, $base_amount, $vat_amount)
    {
        $bill_relief = BillRelief::model()->findByAttributes([
            'bill_id' => $bill_id,
            'relief_bill_id' => $relief_bill_id,
            'vat_rate' => $vat_rate,
            'internal_vat' => $internal_vat
        ]);
        $amount = $internal_vat ? $base_amount : $base_amount + $vat_amount;
        if (is_null($bill_relief))
        {
            $bill_relief = new BillRelief();
            $bill_relief->bill_id = $bill_id;
            $bill_relief->relief_bill_id = $relief_bill_id;
            $bill_relief->vat_rate = $vat_rate;
            $bill_relief->internal_vat = $internal_vat;
        }
        $bill_relief->base_amount = $base_amount;
        $bill_relief->vat_amount = $vat_amount;
        $bill_relief->amount = $amount;
        $bill_relief->save();
    }
}
