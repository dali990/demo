<?php

/**
 * @package SIMA
 * @subpackage Accounting
 */
class VatController extends SIMAController
{
    
    public function actionIndex()
    {
        $title = 'PDV pregled';
        $uniq = SIMAHtml::uniqid();
        $vertical_menu_id = 'v_menu_'.$uniq;
        $right_panel_id = 'right_panel_'.$uniq;
        
        $left_menu = $this->widget('SIMATree2', [
            'id' => $vertical_menu_id,
            'class'=>'sima-vm',
            'list_tree_data'=>$this->generateVMenuArray(),
            'disable_unselect_item'=>true
        ], true);
        
        $right_panel = $this->renderPartial('right_panel_layout', [
            'right_panel_id'=>$right_panel_id,
            'vertical_menu_id' => $vertical_menu_id
        ], true, false);
        
        $html = Yii::app()->controller->renderContentLayout([
            'name' => $title,            
//            'options_class'=>'sima-email-client-options',
//            'options'=>$this->getEmailClientOptions($uniq, Yii::app()->params['sima_mailbox_msg_number'], $user_email_accounts)
        ], 
        [
            [
                'proportion'=>0.1,
                'min_width'=>120,                
                'max_width'=>120,                
                'html'=>$left_menu,
//                'class'=>'sima-email-client-mailboxes'
            ],
            [
                'proportion'=>0.9,
                'min_width'=>600,
                'max_width'=>2000,
                'html'=>$right_panel,
//                'class'=>'sima-email-client-msg'
            ]
        ], 
        [
            'id'=>$uniq,
//            'class'=>'sima-email-client'
        ]);
        
        $this->respondOK([
            'title' => $title,
            'html' => $html
        ]);
    }
    
    public function actionRightPanel($type,$id)
    {
        if ($type == 'month')
        {
            $a_month = AccountingMonth::getByID($id);
            $month = $a_month->base_month->month;
            $year = $a_month->base_month->year->year;

            $kprs = KPR::model()->inMonth($month, $year)->find([
                'select' => 'sum(kpr_14 + kpr_17) as kpr_14'
            ]);
            $kirs = KIR::model()->inMonth($month, $year)->find([
                'select' => 'sum(kir_13 + kir_15) as kir_13'
            ]);
            $KPR_vat_total  = SIMAHtml::number_format($kprs->kpr_14);
            $KIR_vat_total  = SIMAHtml::number_format($kirs->kir_13);
            
            
            
            
            
            $html = $this->renderPartial('right_panel_content_month', [
                'month' => $a_month->month,
                'month_id' => $a_month->id,
                'a_month' => $a_month,
                'year' => $a_month->year,
                'year_id' => $a_month->base_month->year_id,
                
                'KPR_vat_total' => $KPR_vat_total,
                'KIR_vat_total' => $KIR_vat_total,
                
            ], true, true);
            
        }
        elseif ($type == 'year')
        {
            $html = $this->renderPartial('right_panel_content_year', [
                
            ], true, true);
        }
        else
        {
            $html = '';
        }
        $this->respondOK([
            'html' => $html
        ]);
    }
    
    public function actionKprManual($month, $year)
    {
        $m = Month::get($month, $year);
        $year_model = Year::get($year);
        
        $table_id = SIMAHtml::uniqid();
        $html = $this->widget('SIMAGuiTable', [
            'id' => $table_id,
            'model' => 'KPRManual',
            'class' => '_low_profile_rows',
            'add_button' => [
                'init_data' => [
                    'KPRManual' => [
                        'month' => ['ids' => $m->id]
                    ]
                ],
                'inline' => true
            ],
            'fixed_filter' => [
                'display_scopes' => 'byOrder',
                'month' => ['ids' => $m->id]
            ],
            'custom_buttons' => [
                Yii::t('AccountingModule.KPR', 'CollapseNumbers')=>[
                    'func'=>"sima.accounting.collapseOrderNumbersInKprKirManual('".KPRManual::class."',$year_model->id,'#$table_id');"
                ],
                Yii::t('AccountingModule.KPR', 'ReleaseNumber')=>[
                    'func'=>"sima.accounting.releaseOrderNumberInKprKirManual('".KPRManual::class."',$year_model->id,'#$table_id');"
                ],
            ]
        ], true);
        
        $this->respondOK([
            'html' => $html
        ]);
    }
    
    public function actionKirManual($month, $year)
    {
        $m = Month::get($month, $year);
        $year_model = Year::get($year);
        
        $table_id = SIMAHtml::uniqid();
        $html = $this->widget('SIMAGuiTable', [
            'id' => $table_id,
            'model' => 'KIRManual',
            'class' => '_low_profile_rows',
            'add_button' => [
                'init_data' => [
                    'KIRManual' => [
                        'month' => ['ids' => $m->id]
                    ]
                ],
                'inline' => true
            ],
            'fixed_filter' => [
                'display_scopes' => 'byOrder',
                'month' => ['ids' => $m->id]
            ],
            'custom_buttons' => [
                Yii::t('AccountingModule.KIR', 'CollapseNumbers')=>[
                    'func'=>"sima.accounting.collapseOrderNumbersInKprKirManual('".KIRManual::class."',$year_model->id,'#$table_id');"
                ],
                Yii::t('AccountingModule.KIR', 'ReleaseNumber')=>[
                    'func'=>"sima.accounting.releaseOrderNumberInKprKirManual('".KIRManual::class."',$year_model->id,'#$table_id');"
                ],
            ]
        ], true);
        
        $this->respondOK([
            'html' => $html
        ]);
    }
    
    public function actionKprCompare($month, $year)
    {
        $m = Month::get($month, $year);
        
        $SIMAandManual = [
            'model' => 'KPRManual',
            'columns_type' => 'kpr_compare',
            'class' => '_low_profile_rows',
            'fixed_filter' => [
                'display_scopes' => 'byOrder',
                'month' => ['ids' => $m->id],
                'filter_scopes' => [
                    'diffKPRAndInKPRMonth'
                ]
            ]
        ];
        $SIMANotManual = [
            'model' => 'KPR',
            'title' => 'Nije ručno upisan, samo SIMA',
            'fixed_filter' => [
                'filter_scopes' => [
                    'inMonth' => [$month, $year],
                    'notInKPRManualInMonth' => [$m->id]
                ],
                'display_scopes' => ['byOrderInYear']
            ]
        ];
        $ManualNotSIMA = [
            'model' => 'KPRManual',
            'title' => 'Samo ručno upisan',
            'fixed_filter' => [
                'month' => ['ids' => $m->id],
                'display_scopes' => 'byOrder',
                'filter_scopes' => [
                    'notInKPRInMonth' => [$m->id]
                ]
                
            ]
        ];
        
        $html = $this->renderPartial('kpr_kir_compare', [
            'month' => $m,
            'SIMAandManual' => $SIMAandManual,
            'SIMANotManual' => $SIMANotManual,
            'ManualNotSIMA' => $ManualNotSIMA,
        ], true);

        $this->respondOK([
            'html' => $html
        ]);
    }
    
    public function actionKirCompare($month, $year)
    {
        $m = Month::get($month, $year);
        
        $SIMAandManual = [
            'model' => 'KIRManual',
            'columns_type' => 'kir_compare',
            'class' => '_low_profile_rows',
            'fixed_filter' => [
                'display_scopes' => 'byOrder',
                'month' => ['ids' => $m->id],
                'filter_scopes' => [
                    'diffKIRAndInKIRMonth'
                ]
            ]
        ];
        $SIMANotManual = [
            'model' => 'KIR',
            'title' => 'Nije ručno upisan, samo SIMA',
            'fixed_filter' => [
                'filter_scopes' => [
                    'inMonth' => [$month, $year],
                    'notInKIRManualInMonth' => [$m->id]
                ],
                'display_scopes' => ['byOrderInYear']
            ]
        ];
        $ManualNotSIMA = [
            'model' => 'KIRManual',
            'title' => 'Samo ručno upisan',
            'fixed_filter' => [
                'month' => ['ids' => $m->id],
                'display_scopes' => 'byOrder',
                'filter_scopes' => [
                    'notInKIRInMonth' => [$m->id]
                ]
                
            ]
        ];
        
        $html = $this->renderPartial('kpr_kir_compare', [
            'month' => $m,
            'SIMAandManual' => $SIMAandManual,
            'SIMANotManual' => $SIMANotManual,
            'ManualNotSIMA' => $ManualNotSIMA,
        ], true);

        $this->respondOK([
            'html' => $html
        ]);
    }
    
    private function generateVMenuArray()
    {
        $_result = [];
        //samo one koje imaju finansijsku godinu
        $crit = new SIMADbCriteria([
            'Model' => 'Year',
            'model_filter' => [
                'accounting' => []
            ]
        ]);
        $years = Year::model()->byYear()->findAll($crit);
        
        foreach ($years as $year)
        {
            $_year_submenu = [];
            
            
            foreach ($year->months(['scopes' => 'byNameDESC']) as $month)
            {
                if (AccountingMonth::model()->findByPk($month->id))
                {
                    $_year_submenu[] = [
                        'title' => $month->month,
                        'data' => [
                            'type' => 'month',
                            'id' => $month->id
                        ],
                    ];
                }
            }
            if (count($_year_submenu)>0)
            {
                $_result[] = [
                    'title' => $year->DisplayName,
                    'data' => [
                        'type' => 'year',
                        'id' => $year->id
                    ],
                    'subtree' => $_year_submenu
                ];
            }
        }
        return $_result;
    }
    
    public function actionSetVatAccountDocument($month_id, $account_document_id)
    {
        $month = ModelController::GetModel('AccountingMonth', $month_id);
        $month->vat_account_document_id = $account_document_id;
        $month->save();
        
        $this->respondOK();
    }
    
    public function actionCollapseOrderNumbersInKprKirManual()
    {
        $data = $this->setEventHeader();
        $params_not_set = [];
        if (!isset($data['model_name']))
        {
            array_push($params_not_set, 'model_name');
        }
        if (!isset($data['year_id']))
        {
            array_push($params_not_set, 'year_id');
        }
        
        if (count($params_not_set) > 0)
        {
            throw new SIMAExceptionFunctionWrongParams('actionCollapseOrderNumbersInKprKirManual', $params_not_set);
        }
        
        $model_name = $data['model_name'];
        $year_id = $data['year_id'];
        $year_model = AccountingYear::model()->findByPk($year_id);

        $models = $model_name::model()->findAll(new SIMADbCriteria([
            'Model' => $model_name,
            'model_filter' => [
                'filter_scopes' => [
                    'inYear' => [$year_model->id]
                ],
                'display_scopes' => 'byOrderInYearAsc'
            ]
        ]));
        
        $numbers_by_months = [];
        $changed_order_numbers = [];
        $order = 1;
        $cnt = count($models);
        $max_while_cycle_exec_time_seconds = ini_get('max_execution_time');
        foreach ($models as $model)
        {
            set_time_limit($max_while_cycle_exec_time_seconds);
            $percent = round(($order/$cnt)*100, 0, PHP_ROUND_HALF_DOWN);            
            $this->sendUpdateEvent($percent);
            if (intval($model->order_in_year) !== $order)
            {
                if (!in_array($order, $changed_order_numbers))
                {
                    $month_number = $model->month->month;
                    if (empty($numbers_by_months[$month_number]))
                    {
                        $numbers_by_months[$month_number] = [];
                    }
                    array_push($numbers_by_months[$month_number], $order);
                }
                array_push($changed_order_numbers, $model->order_in_year);
                $model->order_in_year = $order;
                $model->save();
            }
            $order++;
        }
        
        if ($model_name === KPRManual::class)
        {
            $year_model->kpr_next_count = $cnt + 1;
            $year_model->save();
        }
        else if ($model_name === KIRManual::class)
        {
            $year_model->kir_next_count = $cnt + 1;
            $year_model->save();
        }
        
        $numbers_by_months_html = Yii::t('AccountingModule.KIR', 'NoOrderNumbersForCollapse');
        if (count($numbers_by_months) > 0)
        {
            krsort($numbers_by_months);
            $numbers_by_months_html = "<div class='kpr-kir-manual-collapsed-order-numbers'><table class='kpr-kir-manual-collapsed-order-numbers-tbl'><tr>
                <th style='width:20%;'>".Yii::t('AccountingModule.KIR', 'Month')."</th>
                <th style='width:80%;'>".Yii::t('AccountingModule.KIR', 'OrderNumbersThatWasSkipped')."</th>
            </tr>";
            
            foreach ($numbers_by_months as $key => $value)
            {
                $numbers_by_months_html .= "<tr>
                    <td style='width:20%;'>$key</td>
                    <td style='width:80%;'>".implode(', ', $value)."</td>
                </tr>";
            }
            
            $numbers_by_months_html .= '</table></div>';
        }

        $this->sendEndEvent([
            'numbers_by_months_html' => $numbers_by_months_html
        ]);
    }
    
    public function actionReleaseOrderNumberInKprKirManual()
    {
        $data = $this->setEventHeader();
        $params_not_set = [];
        if (!isset($data['model_name']))
        {
            array_push($params_not_set, 'model_name');
        }
        if (!isset($data['year_id']))
        {
            array_push($params_not_set, 'year_id');
        }
        if (!isset($data['order_in_year']))
        {
            array_push($params_not_set, 'order_in_year');
        }
        
        if (count($params_not_set) > 0)
        {
            throw new SIMAExceptionFunctionWrongParams('actionCollapseOrderNumbersInKprKirManual', $params_not_set);
        }
        
        $model_name = $data['model_name'];
        $year_id = $data['year_id'];
        $order_in_year = $data['order_in_year'];
        $year_model = AccountingYear::model()->findByPk($year_id);
        
        $model = $model_name::model()->find(new SIMADbCriteria([
            'Model' => $model_name,
            'model_filter' => [
                'order_in_year' => $order_in_year,
                'filter_scopes' => [
                    'inYear' => [$year_model->id]
                ],
            ]
        ]));
        
        if (!empty($model))
        {
            $models = $model_name::model()->findAll(new SIMADbCriteria([
                'Model' => $model_name,
                'model_filter' => [
                    'filter_scopes' => [
                        'orderInYearEqualGreaterThen' => [$order_in_year],
                        'inYear' => [$year_model->id]
                    ],
                    'display_scopes' => 'byOrderInYearAsc'
                ]
            ]));
            $models_reverse = array_reverse($models);
            $i = 1;
            $cnt = count($models_reverse);
            $max_while_cycle_exec_time_seconds = ini_get('max_execution_time');
            foreach ($models_reverse as $_model)
            {
                set_time_limit($max_while_cycle_exec_time_seconds);
                $percent = round(($i/$cnt)*100, 0, PHP_ROUND_HALF_DOWN);            
                $this->sendUpdateEvent($percent);
                
                $_model->order_in_year += 1;
                $_model->save();
                
                $i++;
            }

            if ($model_name === KPRManual::class)
            {
                $year_model->kpr_next_count += 1;
                $year_model->save();
            }
            else if ($model_name === KIRManual::class)
            {
                $year_model->kir_next_count += 1;
                $year_model->save();
            }
        }
        
        $this->sendUpdateEvent(100);
        
        $this->sendEndEvent();
    }

    public function actionExportVat()
    {
        $data              = $this->setEventHeader();
        $month_id          = $this->filter_input($data, 'month_id');
        $export_type       = $this->filter_input($data, 'export_type');
        $with_questionmark = $this->filter_input($data, 'with_questionmark', false);
        if (is_null($with_questionmark))
        {
            $with_questionmark = true;
        }
        else
        {
            $with_questionmark = SIMAMisc::filter_bool_var($with_questionmark);
        }
        
        $month = Month::model()->findByPkWithCheck($month_id);

        $end_event_params = [];
        
        $vat_export = new POPDVExport($month);
        if ($export_type === POPDVExport::PDF)
        {
            $temporary_file = $vat_export->exportToPDF($with_questionmark);

            $end_event_params = [
                'filename' => $temporary_file->getFileName(),
                'downloadname' => Yii::t('AccountingModule.Vat', 'POPDVForm') . '.pdf'
            ];
        }
        else if ($export_type === POPDVExport::XML)
        {
            $temporary_file = $vat_export->exportToXML();

            $end_event_params = [
                'filename' => $temporary_file->getFileName(),
                'downloadname' => Yii::t('AccountingModule.Vat', 'POPDVForm') . '.xml'
            ];
        }
        else
        {
            throw new SIMAException(Yii::t('AccountingModule.Vat', 'WrongTypeForPOPDVExport'));
        }
        
        $this->sendEndEvent($end_event_params);
    }
    
    public function actionGetPOPDVDisplayHTMLold($month, $year)
    {
        $month_model = Month::get($month, $year);

        $vat_export = new POPDVExport($month_model);
        
        $this->respondOK([
            'html' => '<div style="padding: 20px;">' . SIMAHtml::addContentToIFrame($vat_export->getDisplayHTML()) . '</div>'
        ]);
    }
    
    public function actionGetPOPDVDisplayHTML($month, $year, $account_document_id = null, $booking_type = POPDVExport::BOOKING_TYPE_BOTH)
    {
        $month_model = Month::get($month, $year);
        if (!is_null($account_document_id))
        {
            $account_document = AccountDocument::model()->findByPkWithCheck($account_document_id);
        }

        try
        {
            $vat_export = new POPDVExport($month_model, $account_document, $booking_type);
        }
        catch (SIMAExceptionPOPDVBookingTypeNotExists $e)
        {
            throw new SIMAWarnException($e->getMessage());
        }
        
        list($filled_params, $filled_sums) = $vat_export->getListOfFilledParams();
        
        $this->respondOK([
            'html' => $vat_export->getDisplayHTML(),
            'filled_params' => $filled_params,
            'sums_params' => $filled_sums,
            'manual_edited_params' => $vat_export->getListOfManualEditedParams(),
        ]);
    }
    
    public function actionPOPDVBillsDisplay($code, $month_id)
    {
        $month = Month::model()->findByPkWithCheck($month_id);
        try
        {
            $popdv = new POPDVBillCalc($month);
            $filter_params = $popdv->getModelFilterParams($code);
        }
        catch (SIMAExceptionPOPDVCodeJustSum $e)
        {
            throw new SIMAWarnException($e->getMessage());
        }
        
        $html = $this->renderPartial('POPDVBillsDisplay', [
            'month' => $month,
            'filter_params' => $filter_params,
            'code' => $code
        ], true, false);
        
        $this->respondOK([
            'html' => $html
        ]);
    }
    
    public function actionPOPDVFieldReport()
    {
        $month_id = $this->filter_post_input('month_id');
        $month = Month::model()->findByPkWithCheck($month_id);
        
        $popdv_bill_calc = new POPDVBillCalc($month);
        
        $code = $this->filter_post_input('code');
        $filter_params = $popdv_bill_calc->getModelFilterParams($code);
        
        $bill_items = BillItem::model()->findAll([
            'model_filter' => $filter_params
        ]);
                
        $update_model_tags = [];
        $DATA = [
            'Bills' => [],
            'Payments' => []
//            'CashDeskItem' => [],
//            'WarehouseTransferItem' => [],
//            'AccountTransaction' => [],
//            'Paycheck' => [],
//            'BillItem' => [],
        ];
        
        foreach ($bill_items as $bill_item) 
        {
            $bill = $bill_item->bill(['scopes' => ['withVatInMonth']]);
            
            
            
            if (!empty($bill->vat_in_month_id) && intval($bill->vat_in_month_id) === intval($month_id))
            {
                $_index = $bill->id.'REGULAR';
                if (!isset($DATA['Bills'][$_index]))
                {
                    $DATA['Bills'][$_index] = [
                        'bill_tag' => SIMAMisc::getVueModelTag($bill),
                        'included_items' => [],
                        'bill_items_count' => $bill->bill_items_count,

                        'value' => 0,
                        'value_vat' => 0,
                        'value_internal_vat' => 0,
                        'advance_vat_releases' => floatval($bill->advance_released_vat_sum),
    //                    'income_notpayed' => 0,
    //                    'income_advance' => 0,
    //                    'cost' => 0,
    //                    'cost_notpayed' => 0,
    //                    'cost_advance' => 0,
                    ];
                }
                $DATA['Bills'][$_index]['included_items'][]   = $bill_item->id;
                $DATA['Bills'][$_index]['value']              += $bill_item->value;
                $DATA['Bills'][$_index]['value_vat']          += $bill_item->value_vat;
                $DATA['Bills'][$_index]['value_internal_vat'] += $bill_item->value_internal_vat;
    //            $DATA['Bills'][$bill->id]['income_notpayed'] += $_item->income;
    //            $DATA['Bills'][$bill->id]['income_advance']  += $_item->income;
    //            $DATA['Bills'][$bill->id]['cost']            += $_item->cost;
    //            $DATA['Bills'][$bill->id]['cost_notpayed']   += $_item->cost_notpayed;
    //            $DATA['Bills'][$bill->id]['cost_advance']    += $_item->cost_advance;
            }
            if (!empty($bill->canceled_in_month_id) && intval($bill->canceled_in_month_id) === intval($month_id))
            {
                $_index = $bill->id.'CANCELED';
                if (!isset($DATA['Bills'][$_index]))
                {
                    $DATA['Bills'][$_index] = [
                        'bill_tag' => SIMAMisc::getVueModelTag($bill),
                        'included_items' => [],
                        'bill_items_count' => $bill->bill_items_count,

                        'value' => 0,
                        'value_vat' => 0,
                        'value_internal_vat' => 0,
                        'advance_vat_releases' => floatval($bill->advance_released_vat_sum),
    //                    'income_notpayed' => 0,
    //                    'income_advance' => 0,
    //                    'cost' => 0,
    //                    'cost_notpayed' => 0,
    //                    'cost_advance' => 0,
                    ];
                }
                
                $DATA['Bills'][$_index]['included_items'][]   = $bill_item->id;
                $DATA['Bills'][$_index]['value']              -= $bill_item->value;
                $DATA['Bills'][$_index]['value_vat']          -= $bill_item->value_vat;
                $DATA['Bills'][$_index]['value_internal_vat'] -= $bill_item->value_internal_vat;
    //            $DATA['Bills'][$bill->id]['income_notpayed'] += $_item->income;
    //            $DATA['Bills'][$bill->id]['income_advance']  += $_item->income;
    //            $DATA['Bills'][$bill->id]['cost']            += $_item->cost;
    //            $DATA['Bills'][$bill->id]['cost_notpayed']   += $_item->cost_notpayed;
    //            $DATA['Bills'][$bill->id]['cost_advance']    += $_item->cost_advance;
            }
        }
        
        
        if ($code === '8v2')
        {
        
            $bank_commission_payment_type = Yii::app()->configManager->get('accounting.bank_commission_payment_type',false);
            if (!empty($bank_commission_payment_type))
            {
                $model_filter = [
                    'payment_type' => ['ids' => $bank_commission_payment_type],
                    'bank_statement' => [
                        'date' => ($month->firstDay()->day_date).' <> '.($month->lastDay()->day_date)
                    ]
                ];

                $payments = Payment::model()->byOfficial()->findAll([
                    'model_filter' => $model_filter
                ]);

                foreach ($payments as $payment) 
                {
//                    $DATA['Payments'][] = SIMAMisc::getVueModelTag($payment);
                    $DATA['Payments'][$payment->id] = [
                        'payment_tag' => SIMAMisc::getVueModelTag($payment),
//                        'value_vat' => 0,
//                        'value_internal_vat' => 0,
//                        'advance_vat_releases' => floatval($bill->advance_released_vat_sum),
    //                    'income_notpayed' => 0,
    //                    'income_advance' => 0,
    //                    'cost' => 0,
    //                    'cost_notpayed' => 0,
    //                    'cost_advance' => 0,
                    ];
                }
            }
        }
        
        
        $this->respondOK([
            'setter_object' => [
                'DATA' => $DATA,
                'UPDATE_MODEL_TAGS' => $update_model_tags,
            ]
        ]);
    }
    
    public function actionGetPPPDVReportHtml($month_id)
    {
        $pppdv = new PPPDVReport(['month_id' => $month_id]);
        $html = $pppdv->getHTML();
        
        $this->respondOK([
            'html' => SIMAHtml::addContentToIFrame($html)
        ]);
    }
    
    public function actionKprKirReport()
    {
        $data = $this->setEventHeader();
        
        $month = $this->filter_input($data, 'month');
        $year = $this->filter_input($data, 'year');
        $name = $this->filter_input($data, 'name');
        $export_type = $this->filter_input($data, 'export_type');
        
        $month_model = Month::get($month, $year);
        $download_name = '';
        $controller = $this;
        
        if ($name === KPR::class || $name === KPRManual::class)
        {
            $download_name = 'Knjiga primljenih racuna' . (($name === KPRManual::class) ? '(Rucni)' : '');

            $kpr_report = new KprReport($month, $year, $name, 
                function($percent) use ($controller) {
                    $controller->sendUpdateEvent($percent);
                }
            );
            
            if ($export_type === 'pdf')
            {
                $temp_file = $kpr_report->getPDF();
            }
            else if ($export_type === 'csv')
            {
                $temp_file = $kpr_report->getCSV();
            }
            else
            {
                throw new SIMAException(Yii::t('AccountingModule.Reports', 'UnknownExportType'));
            }
        }
        else if ($name === KIR::class || $name === KIRManual::class)
        {
            $download_name = 'Knjiga izlaznih racuna' . (($name === KIRManual::class) ? '(Rucni)' : '');
            
            $kir_report = new KirReport($month, $year, $name, 
                function($percent) use ($controller) {
                    $controller->sendUpdateEvent($percent);
                }
            );
            
            if ($export_type === 'pdf')
            {
                $temp_file = $kir_report->getPDF();
            }
            else if ($export_type === 'csv')
            {
                $temp_file = $kir_report->getCSV();
            }
            else
            {
                throw new SIMAException(Yii::t('AccountingModule.Reports', 'UnknownExportType'));
            }
        }
        else
        {
            throw new SIMAException(Yii::t('AccountingModule.Reports', 'KprKirPdfInvalidName'));
        }

        $this->sendEndEvent([
            'filename' => $temp_file->getFileName(),
            'downloadname' => $download_name . " za mesec {$month_model->toString()} $year godine." . $export_type
        ]);
    }
    
    /**
     * 
     * @param int $account_document_id
     * @param bool $manual
     */
    public function actionSwitchPOPDVToManual(int $account_document_id, bool $manual)
    {
        $account_document = AccountDocument::model()->findByPkWithCheck($account_document_id);
        $month_model = $account_document->getVatInMonth();

        foreach ($account_document->popdv_items as $_item)
        {
            $_item->delete();
        }
        
        if ($manual)
        {
            if ($account_document->popdv_manual === true)
            {
                $this->respondFail("ovaj dokument je vec u rucnom statusu");
            }
            $vat_export = new POPDVExport($month_model, $account_document, POPDVExport::BOOKING_TYPE_REGULER);
            list($filled_params,) = $vat_export->getListOfFilledParams();

            foreach ($filled_params as $_param)
            {
                $_item = new POPDVItem();
                $_item->account_document_id = $account_document->id;
                $_item->param_code = $_param;
                $_item->param_value = $vat_export[$_param];
                $_item->note = "Automatski popunjeno prilikom konverzije";
                $_item->save();
            }
        }
        
        $account_document->popdv_manual = $manual;
        $account_document->update();
        
        $this->respondOK();
    }
    
    public function actionToggleVATReturn($month_id)
    {
        $accounting_month = AccountingMonth::model()->findByPkWithCheck($month_id);
        $accounting_month->vat_return = !$accounting_month->vat_return;
        $accounting_month->save();
        
        $this->respondOK();
    }
    
}