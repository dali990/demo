<?php

class PPPPDController extends SIMAController
{
    public function filters()
    {
        return [
            'ajaxOnly-Index-ComparePPPPDs-PerformComparePPPPDs'
        ] + parent::filters();
    }
    
    public function actionMainMenu()
    {
        $title = Yii::t('AccountingModule.PPPPD', 'PPPPDs');
        
        $year_id = AccountingYear::getLastConfirmed()->id;   
        $uniq = SIMAHtml::uniqid();
        $ppppds_table_id = $uniq.'_ppppds';
        
        $years = AccountingYear::model()->findAll(new SIMADbCriteria([
            'Model'=>'AccountingYear',
            'model_filter'=>[
                'display_scopes'=>'byName'
            ]
        ]));
        $menu_array = [];
        foreach ($years as $year)
        {
            $menu_array[] = [
                'display_name' => $year->DisplayName,
                'datas' => ['id' => $year->id]
            ];
        }
        
        $left_menu = $this->renderPartial('ppppds_left_panel', [
            'uniq'=>$uniq,
            'year_id'=>$year_id,
            'menu_array'=>$menu_array,
            'ppppds_table_id' => $ppppds_table_id
        ], true, false);
                
        $right_panel = $this->renderPartial('ppppds_right_panel', [
            'year_id' => $year_id,
            'ppppds_table_id' => $ppppds_table_id
        ], true, false);
        
        $html = Yii::app()->controller->renderContentLayout([
            'name' => $title,
            'title_width'=>'20%',
            'options_width'=>'80%',
            'options'=>[]
        ], [
            [
                'proportion'=>0.1,
                'min_width'=>120,                
                'max_width'=>120,                
                'html'=>$left_menu
            ],
            [
                'proportion'=>0.9,
                'min_width'=>600,
                'max_width'=>2000,  
                'html'=>$right_panel
            ]
        ]);
        
        $this->respondOK([
            'title' => $title,
            'html' => $html
        ]);
    }
    
    public function actionComparePPPPDs()
    {
        $html = $this->renderPartial('compare_ppppds', [], true, false);
        
        $this->respondOK([
            'html' => $html,
            'title' => Yii::t('AccountingModule.PPPPD', 'ComparePPPPDs')
        ]);
    }
    
    public function actionPerformComparePPPPDs()
    {
        $data = $this->setEventHeader(); 
        
        $ppppd1_id = $this->filter_input($data, 'ppppd1_id');
        $ppppd2_id = $this->filter_input($data, 'ppppd2_id');
        
        $ppppd1 = PPPPD::model()->findByPkWithCheck($ppppd1_id);
        $ppppd2 = PPPPD::model()->findByPkWithCheck($ppppd2_id);
        
        $diffs = PPPPDComponent::GenerateDiffForPPPPDs($ppppd1, $ppppd2);
        
        $html = $this->widget('SIMADiffViewer', [
            'diffs' => $diffs
        ], true);
        
        $this->sendEndEvent([
            'html' => $html
        ]);
    }
    
    public function actionCreatePPPPDNewFileVersion($ppppd_id)
    {
        $ppppd = PPPPD::model()->findByPkWithCheck($ppppd_id);
        
        $pdf_html = $this->renderPartial('ppppd_pdf/pdf_html', [
            'ppppd' => $ppppd
        ], true, false);
        
        $temporary_file = new TemporaryFile('', null, 'pdf');

        $temporary_file->createPdfFromHtml(
            [
                'bodyHtml' => $pdf_html
            ],
            [
                'user-style-sheet' => $this->viewPath . '/ppppd_pdf/pdf_css.css'
            ]
        );
        
        $ppppd->file->appendTempFile($temporary_file->getFileName(), $ppppd->DisplayName);

        $this->respondOK([], $ppppd);
    }
}