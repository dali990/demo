
<?php

class IosController extends SIMAController
{
    public function filters()
    {
        return array(
            'ajaxOnly'
            . '-generateIoses'
        ) + parent::filters();
    }
    
    public function actionIndex()
    {
       
    }
    
    public function actionListIoses()
    {
        $uniq = SIMAHtml::uniqid();
        $title = Yii::t('AccountingModule.Account','Ioses');
        $year_id = AccountingYear::getLastConfirmed()->id;
        $years = AccountingYear::model()->findAll(new SIMADbCriteria([
            'Model'=>'AccountingYear',
            'model_filter'=>[
                'display_scopes'=>'byName'
            ]
        ]));
        
        $year = AccountingYear::model()->findByPk($year_id);
        if(!empty($year->ios_date))
        {
            $ios_date = $year->ios_date;
        } else {
            $ios_date = '('.Yii::t('AccountingModule.Account','NotEntered').')';
        }
        $menu_array = [];
        $right_panel_id = $uniq.'_ioses_right_panel';
        $left_panel_id = $uniq.'_ioses_left_panel';
        $id_input_date=$uniq."accounting_ioses_input_date";
        $ios_date_value = $uniq.'_ios_date_value';
        $ios_date_edit_btn = $uniq.'_ios_date_edit_btn';
        foreach ($years as $year)
        {
            $menu_array[] = [
                'display_name' => $year->DisplayName,
                'datas' => ['id' => $year->id]
            ];
        }
        $left_menu = $this->renderPartial('ioses_left_panel', [
            'id'=>$left_panel_id,
            'right_panel_id'=>$right_panel_id,
            'id_input_date' => $id_input_date,
            'year_id'=>$year_id,
            'menu_array'=>$menu_array,
        ], true, false);
                
        $right_panel = $this->renderPartial('ioses_right_panel', [
            'id'=>$right_panel_id,
            'year_id'=>$year_id,
            'ios_date' => date('d.m.Y', strtotime($ios_date)),
            'accounting_year' => $year,
            'id_input_date' => $id_input_date,
            'ios_date_value' => $ios_date_value,
            'ios_date_edit_btn'=>$ios_date_edit_btn
        ], true, false);
        
        $html = Yii::app()->controller->renderContentLayout([
            'name' => $title,
            'title_width'=>'20%',
            'options_width'=>'80%',
            'options'=>[]
            ], 
            [
                [
                    'proportion'=>0.1,
                    'min_width'=>120,                
                    'max_width'=>120,                
                    'html'=>$left_menu
                ],
                [
                    'proportion'=>0.9,
                    'min_width'=>600,
                    'max_width'=>2000,  
                    'html'=>$right_panel
                ]
            ], [
                'id'=>$uniq,
                'class'=>'accounting-ioses'
            ]);
        
        $this->respondOK([
            'html' => $html,
            'title' => $title
        ]);
    }
   
    public function actionListIosCandidateCompanies($year_id)
    {
        $respond = [
            'privilege' => false,
        ];
        if(Yii::app()->user->checkAccess('modelIosConfirm'))
        {
            $respond['privilege']= true;
            $uniq = SIMAHtml::uniqid();
            $id_candidate_companies = $uniq.'_ioses_candidate_companies';
            
            $buyer_code[] = Yii::app()->configManager->get('accounting.codes.partners.customers'); //konto kupaca
            $buyer_code[] = Yii::app()->configManager->get('accounting.codes.partners.customers_ino'); //konto kupaca iz inostranstva
            $buyer_code[] = Yii::app()->configManager->get('accounting.codes.partners.advance_bill_out'); //konto kupaca avansni
            
            $year = AccountingYear::model()->findByPk($year_id); // potrebno da bi izvukli ios_date
            $input_ios_date = date('Y-m-d', strtotime($year->ios_date));
            $choosen_year = $year->year; // godina po kojoj pretrazujemo transakcije za kompaniju, uzima se za selektovanu godinu accounting_years iz levog panela.
            $base_choosen_year = $year;
            
            $companies = Company::model()->byName()->listCompaniesWithtransactionsForIos($base_choosen_year->id, $buyer_code)->findAll();
            $count_companies = count($companies);
            $html = $this->renderPartial('ios_candidate_companies', [
                'companies' => $companies,
                'year' => $choosen_year,
                'id' => $id_candidate_companies,
                'uniq' => $uniq,
            ], true, false);
        
            $respond = array_merge($respond, [
                'title' => Yii::t('AccountingModule.Account', 'IosGenerationCompanies'),
                'html' => $html,
                'id' => $id_candidate_companies,
                'ios_date' => $input_ios_date,
                'proper_year_id' => $base_choosen_year->id,
//                'account_code' => $buyer_code,
                'count_companies' => $count_companies,
            ]);
        }
        $this->respondOK($respond);
    }
    
    public function actionGenerateIoses()
    {
        $data = $this->setEventHeader();
        
        if (!isset($data['year_id'])  || !isset($data['companies']))
        {
            throw new SIMAException('actionGenerateIoses - nisu lepo zadati parametri');
        }
        
        $companies_ids = $data['companies'];
        $year_id = $data['year_id'];
        
        $account_code = []; // samo konto za kupce
        $account_code[] = Yii::app()->configManager->get('accounting.codes.partners.customers'); //konto kupaca
        $account_code[] = Yii::app()->configManager->get('accounting.codes.partners.customers_ino'); //konto kupaca iz inostranstva
        $account_code[] = Yii::app()->configManager->get('accounting.codes.partners.advance_bill_out'); //konto kupaca avansni
        
//        $account_code = [$account_code];       // a f-ji IosAccounts je potrebno proslediti tip niz
        $year = AccountingYear::model()->findByPk($year_id);
        
        $ios_date = SIMAHtml::UserToDbDate($year->ios_date);
        $_total_cnt = count($companies_ids);
        $_finished = 0;
        $max_execution_time = ini_get('max_execution_time');
        foreach($companies_ids as $company_id)
        {
            set_time_limit(5);
            $model_filter = [
                'AND',
                ['model_name' => 'Partner'],
                ['model_id' => $company_id],
                ['year' => ['ids' => $year_id]],
            ];
            if(!empty($account_code))
            {
                $codes = $this->IosAccounts($account_code);
                
                $model_filter = array_merge($model_filter,[$codes]);
            }
            $criteria = new SIMADbCriteria([
                'Model' => 'Account',
                'model_filter' => $model_filter
            ]);
            $accounts = Account::model()->findAll($criteria);
            
            foreach($accounts as $account)
            {   
                if (count($account->account_transactions) > 0)
                {
                    $ios = Ios::model()->findByAttributes([
                        'partner_id' => $company_id,
                        'account_id' => $account->id,
                        'ios_date' => $ios_date,
                    ]);
                    if (is_null($ios))
                    {
                        $ios = new Ios();
                        $ios->partner_id = $company_id;
                        $ios->account_id = $account->id;
                        $ios->ios_date = $ios_date;
                        $ios->ios_year_id = $data['year_id'];
                        if(!$ios->save())
                        {
                            throw new Exception('Nije uspelo čuvanje IOS-a u grupnom generisanju.');
                        }
                    }

                    $ios->regenerateIosItems();
                }
            }
            $_finished++;
            $percent = round(($_finished/$_total_cnt)*100, 0, PHP_ROUND_HALF_DOWN);
            $this->sendUpdateEvent($percent);
        }
        set_time_limit($max_execution_time);
        $this->sendEndEvent();
    }
    
    public function actionGenerateIosForCompanyDialog()
    {
        $respond = [
            'privilege' => false
        ];
        if(Yii::app()->user->checkAccess('modelIosConfirm'))
        {
            $uniq = SIMAHtml::uniqid();
            $id_company_input_date = $uniq.'_ios_dialog';
            $html = $this->renderPartial('generate_ios_dialog', [
                    'id_company_input_date' => $id_company_input_date,
            ], true, false);
            
            $respond = array_merge($respond, [
                'privilege' => true,
                'html' => $html,
                'dialog_id' => $id_company_input_date
            ]);
        }
        $this->respondOK($respond);
    }
    
    public function actionGenerateIosForCompany($company_id, $date)
    {
        if(empty($company_id))
        {
            throw new Exception('Nije pronadjen company_id!');
        } 
        if(!empty($date))
        {
            $codes_conf = Yii::app()->configManager->get('accounting.codes.ios.accounts_for_ios_generation');
            $codes = explode(',', $codes_conf);
            $prepared_codes=[];
            foreach($codes as $code)
            {
                $code = trim($code);
                $prepared_codes = array_merge($prepared_codes, [$code]);
            }
            $year = Date('Y',strtotime($date));
            $date = Date('Y-m-d',strtotime($date));
            $base_year = Year::get($year);
            
                $company = Company::model()->listCompaniesWithtransactionsForIos($base_year->id, $prepared_codes, $company_id)->findByPk($company_id);

                if(!empty($company))
                {
                    $model_filter = [
                        'AND',
                        ['model_name' => 'Partner'],
                        ['model_id' => $company_id],
                        [
                            'year' => [
                                'ids' => $base_year->id
                            ]
                        ],
                    ];
                    if(!empty($prepared_codes))
                    {
                        $codes = $this->IosAccounts($prepared_codes);
                        $model_filter = array_merge($model_filter,[$codes]);
                    }
                    $criteria = new SIMADbCriteria([
                        'Model' => 'Account',
                        'model_filter' => $model_filter
                    ]);
                    $accounts = Account::model()->findAll($criteria);
                    if(!empty($accounts))
                    {
                        foreach($accounts as $account)
                        {
                            $status = true;
                            $message = Yii::t('AccountingModule.Account', 'IosSuccessfullyCreated');
                            $ios = new Ios();
                            $ios->partner_id = $company_id;
                            $ios->ios_date = $date;
                            $ios->account_id = $account->id;
                            if(!$ios->save())
                            {
                                throw new Exception('Nije uspelo čuvanje Izvoda otvorenih stavki!');
                            }
                            $ios->regenerateIosItems();
                        }
                    } else {
                        $message = Yii::t('AccountingModule.Account', 'ConfigParamAccountForTheCompanyNotFound');
                    }
                } else {
                    $status = false;
                    $message = Yii::t('AccountingModule.Account', 'IosGenerationNotPossibleCompanyWithNoTransactions').$year;
                }
        } else {
            $status = false;
            $message = Yii::t('AccountingModule.Account', 'IosGenerationDateNecessary');
        }
        
        $this->respondOK([
            'status' => $status,
            'message' => $message
        ]);
    }
    
    private function IosAccounts($codes)
    {
        if(count($codes)>1)
        {
            $query_part = ['OR'];
            foreach($codes as $code)
            {
                $query_part = array_merge($query_part, [['code'=>"$code*"]]);
            }
        } else {
            foreach($codes as $code)
            {
                $query_part = ['code'=>"$code*"];
            }
        }
        return $query_part;
    }
    
    public function actionGetIosDate($year_id)
    {
        $accounting_year = AccountingYear::model()->findByPk($year_id);
        if(!empty($accounting_year->ios_date))
        {
            $html = date('d.m.Y', strtotime($accounting_year->ios_date));
        } else {
            $html = "(".Yii::t('AccountingModule.Account', 'NotEntered').")";
        }
        $this->respondOK([
            'html' => $html,
        ]);
    }
    
    public function actionRegenerateIos($ios_id)
    {
        // pri regenerisanju IOSa, datum se cita iz accounting year, za slucaj da je promenjen
        if(empty($ios_id))
        {
            throw new Exception('Nije primljen ios_id!');
        }

        $ios = Ios::model()->findByPk($ios_id);
        $ios->regenerateIosItems();
      
        $this->respondOK([]);
    }
    
    public function actionIosTabInCompany($company_id)
    {
        $uniq_id = SIMAHtml::uniqid();
        $title = Yii::t('AccountingModule.Account','Ioses');
        
        $year_id = AccountingYear::getLastConfirmed()->id;
        
        $years = AccountingYear::model()->findAll(new SIMADbCriteria([
            'Model'=>'AccountingYear',
            'model_filter'=>[
                'display_scopes'=>'byName'
            ]
        ]));

        $menu_array = [];
        foreach ($years as $_year)
        {
            $menu_array[] = [
                'title' => $_year->DisplayName,
                'code' => $_year->id
            ];
        }

        $left_menu = $this->renderPartial('iosTabInCompany/left_panel', [
            'uniq_id'=>$uniq_id,
            'year_id'=>$year_id,
            'menu_array'=>$menu_array
        ], true, false);
        
        $register_script = "sima.accountingMisc.iosTabInCompanyOnYearSelectHendler($company_id, '$uniq_id');";
        Yii::app()->clientScript->registerScript("ios_tab_in_company_on_year_select_hendler$uniq_id", $register_script, CClientScript::POS_END); 
        
        $html = Yii::app()->controller->renderContentLayout([
            'name' => $title,
            'title_width'=>'20%',
            'options_width'=>'80%',
            'options'=>[]
        ], [
            [
                'proportion'=>0.1,
                'min_width'=>120,                
                'max_width'=>120,                
                'html'=>$left_menu
            ],
            [
                'proportion'=>0.9,
                'min_width'=>600,
                'max_width'=>2000,  
                'html'=>"<div id='right_panel_wrap_$uniq_id' class='sima-layout-panel'></div>"
            ]
        ], [
            'id'=>$uniq_id,
            'class'=>'sima-partner-ioses'
        ]);
        
        $this->respondOK([
            'title' => $title,
            'html' => $html
        ]);
    }
    
    public function actionRenderIosesForCompanyAndYear($year_id, $company_id, $uniq_id)
    {
        $html = $this->renderPartial('iosTabInCompany/right_panel', [
            'uniq_id'=>$uniq_id,
            'year_id'=>$year_id,
            'company_id' => $company_id
        ], true, false);
        
        $this->respondOK([
            'html' => $html
        ]);
    }
    
}
