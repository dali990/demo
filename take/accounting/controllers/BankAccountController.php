<?php

class BankAccountController extends SIMAController
{
    public function actionSetPartnerMainBankAccount()
    {
        $partner_id = $this->filter_post_input('partner_id');
        $bank_account_id = $this->filter_post_input('bank_account_id');
        
        $partner = Partner::model()->findByPk($partner_id);
        if(!isset($partner))
        {
            throw new Exception("partner is null");
        }
        
        $bankAccount = BankAccount::model()->findByPk($bank_account_id);
        if(!isset($bankAccount))
        {
            throw new Exception("bankaccount is null");
        }
        
        if($bankAccount->partner_id !== $partner->id)
        {
            throw new Exception("bankaccount does not belong to partner");
        }
        
        $partner->main_bank_account_id = $bank_account_id;
        $partner->save();
        
        $this->respondOK([], [SIMAHtml::getTag($partner->getModelType()),SIMAHtml::getTag($bankAccount)]);
    }
}