<?php


class BillOutController extends SIMAController
{
    public function actionIndex()
    {
        $uniq = SIMAHtml::uniqid();
        $left_menu = $this->renderPartial('accounting_left_panel', [
            'uniq'=>$uniq,
            'menu_array' => [
                [
                    'title' => BillOutGUI::modelLabel(true),
                    'code' => 'BillOut'
                ],
                [
                    'title' => AdvanceBillGUI::modelLabel(true),
                    'code' => 'AdvanceBillOut'
                ],
                [
                    'title' => PreBillGUI::modelLabel(true),
                    'code' => 'PreBillOut'
                ],
                [
                    'title' => ReliefBillGUI::modelLabel(true),
                    'code' => 'ReliefBillOut'
                ],
                [
                    'title' => UnReliefBillGUI::modelLabel(true),
                    'code' => 'UnReliefBillOut'
                ],
            ]
        ], true, false);
        
        $right_panel = $this->renderPartial('accounting_right_panel', [
            'uniq'=>$uniq,
        ], true, false);
        
        $settings_subactions = [
                        [
                            'title' => Yii::t('AccountingModule.CostType','IncomeTypes'),
                            'onclick' => [
                                "sima.dialog.openActionInDialog", "accounting/costTypes", ['get_params' => ['cost_or_income' => 'INCOME']]
                            ]
                        ],
                    ];
        if(SIMAMisc::isVueComponentEnabled())
        {
            $settings_button = Yii::app()->controller->widget(SIMAButtonVue::class, [
                'icon' => '_main-settings',
                'iconsize' => 18,
                'subactions_data' => $settings_subactions
            ],true);
        }
        else
        {
            $settings_button = Yii::app()->controller->widget('SIMAButton', [
                'action' => [
                    'icon'=>'sima-icon _main-settings _18',
                    'subactions' => $settings_subactions
                 ]
            ],true);
        }
        
        
        $title = Yii::t('AccountingModule.BillOutModule','Invoicing');
        $html = Yii::app()->controller->renderContentLayout([
            'name' => $title,
            'title_width'=>'20%',
            'options_width'=>'80%',
            'options'=>[
                [
                    'html'=> $settings_button
                ],
            ]
        ], [
            [
                'proportion'=>0.1,
                'min_width'=>180,                
                'max_width'=>180,                
                'html'=>$left_menu
            ],
            [
                'proportion'=>0.9,
                'min_width'=>600,
                'max_width'=>5000,  
                'html'=>$right_panel
            ]
        ], [
            'id'=>$uniq,
            'class'=>'sima-accounting'
        ]);
        
        
        
        $this->respondOK([
            'title' => $title,
            'html' => $html
        ]);
    }
    
}
