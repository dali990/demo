<?php
/**
 * Description of BaseController
 *
 * @author goran-set
 */
class CashDeskController extends SIMAController
{
    
    /**
     * Renderuje stranicu blagajne
     */
    public function actionIndex($id = null)
    {
        if (is_null($id))
        {
            $cash_desk = CashDesk::mainCashDesk();
            $title = 'Blagajna';
        }
        else
        {
            $cash_desk = CashDesk::model()->findByPk($id);
            $title = 'Blagajna - '.$cash_desk->name;
        }
        $uniq_right = SIMAHtml::uniqid();
        
        $html_tab1 = $this->renderPartial('index_tab1', [
            'uniq' => SIMAHtml::uniqid(),
            'uniq_right' => $uniq_right,
            'cash_desk' => $cash_desk,
        ], true, false);
        
        $html_tab2 = $this->renderPartial('index_tab2', [
            'uniq' => $uniq_right,
            'cash_desk' => $cash_desk,
        ], true, false);
        
        $html = $this->widget('SIMATabs', [
            'tabs' => [
                [
                    'title' => Yii::t('AccountingModule.CashDesk','Orders'),
                    'code' => 'orders',
                    'html' => $html_tab1
                ],
                [
                    'title' => Yii::t('AccountingModule.CashDesk','Day reports'),
                    'code' => 'reports',
                    'html' => $html_tab2
                ],
                
            ],
            'default_selected' => 'orders'
        ], true);
        
        $html_fin = $this->processOutput($html);
        
        $this->respondOK(array(
            'html' => $html_fin,
            'title' => $title,
        ));
    }
    
    /**
     * vraca id CashDeskLog-a na osnovu ordera
     * @param int $model_id - model_id CashDeskOrder-a
     * @throws Exception
     */
    //MilosS(05.10.2018): ne koristi se nigde
//    public function actionGetCashDeskLogId($model_id)
//    {
//        if (!isset($model_id))
//            throw new Exception('actionGetCashDeskLogId - nisu lepo zadati parametri');
//        
//        $model = CashDeskOrder::model()->findByPk($model_id);
//
//        $this->respondOK([
//            'value' => ($model!==null)?$model->cash_desk_log_id:null
//        ]);
//    }
}
