<?php


/**
 * Koristi se za prikaz placanja i bankarskih izvoda. Takodje sluzi za rasklnjizavanje izvoda
 * @package SIMA
 * @subpackage Accounting
 */
class BankStatementController extends SIMAController
{
    public function actionCreateBankStatements()
    {
        $data = $this->setEventHeader();
        $upload_session_key = $this->filter_input($data, 'upload_session_key');
        $config_id = $this->filter_input($data, 'config_id');
                
        $config = PaymentConfiguration::model()->findByPk($config_id);
        
        $temp_file_name = null;
        if (strpos($upload_session_key, 'SIMAUpload')!==FALSE)
        {
            $decoded_key = json_decode($upload_session_key);
            $status = $decoded_key->status;
            if($status !== 'Success')
            {
                throw new SIMAWarnException(Yii::t('AccountingModule.BankStatement', 'XmlUploadNotSuccessful'));
            }
            
            $temp_file_name = $decoded_key->temp_file_name;
        }
        else
        {
            $temp_file_name = Yii::app()->sima_session[$upload_session_key]['filename'];
        }
        
        $real_path = TempDir::getFullPath($temp_file_name);

        $valid = $this->validateFileFormat($real_path, $config);
        if($valid !== "success")
        {
            $this->sendEndEvent([
                'message' => "Format fajla nije u redu - ".$valid,
            ]);
        }
//        if($config->type==0) //CSV
//        {
//            $this->createBankStatementsCSV($real_path, $config);
//        }
//        else 
        if($config->type==1) //XML
        {
            $this->createBankStatementsXML($real_path, $config);
        }
        else if($config->type == PaymentConfiguration::$TYPE_RAIFFEISEN
                ||
                $config->type == PaymentConfiguration::$TYPE_ERSTE_WEB
                ||
                $config->type == PaymentConfiguration::$TYPE_HALK_BANK
                ||
                $config->type == PaymentConfiguration::$TYPE_RAIFFEISEN_DEVIZNI
                ||
                $config->type == PaymentConfiguration::$TYPE_SOGE_XML
                ||
                $config->type == PaymentConfiguration::$TYPE_CSV_FILE
                ||
                $config->type == PaymentConfiguration::$TYPE_OTP_DEVIZNI_XML
        )
        {
            $parser = new BankStatementImporter($config, $real_path);
            try
            {
                $parser->run();
            }
            catch(SIMAExceptionInvalidFile $e)
            {
                $this->sendEndEvent([
                    'message' => "Format fajla nije u redu - ".$e->getMessage(),
                ]);
            }
            catch(SIMAExceptionNoBankAccontForSystemCompanyWithNumber $e)
            {
                $this->sendEndEvent([
                    'message' => $e->getMessage()
                ]);
            }
            catch(SIMAExceptionMissingBankAccount $e)
            {
                $missing_account = $e->bank_account_info_data;
                
                $form_name = 'fromFile';
                
                $bank_account_model = BankAccount::model();
                $bank_account_model->setModelAttributes([
                    'number' => $missing_account['account_number'],
                    'bank_id' => !empty($missing_account['bank_id']) ? $missing_account['bank_id'] : null,
                    'partner_id' => !empty($missing_account['partner_id']) ? $missing_account['partner_id'] : null,
                    'currency_id' => !empty($missing_account['currency_id']) ? $missing_account['currency_id'] : null,
                    'start_amount' => 0
                ], $form_name);
                
                $add_partner_btn = '';
                if(isset($missing_account['add_partner_btn_data']))
                {
                    $add_partner_btn_id = 'add_partner_btn_' . SIMAHtml::uniqid();
                    $nbs_pib = $missing_account['add_partner_btn_data']['nbs_pib'];
                    $nbs_mb = $missing_account['add_partner_btn_data']['nbs_mb'];
                    $add_partner_btn = $this->widget('SIMAButton', [
                        'id' => $add_partner_btn_id,
                        'action' => [
                            'title' => Yii::t('AccountingModule.BankStatement', 'AddPartnerFromNBS'),
                            'onclick' => ['sima.accountingMisc.addBankAccountPartnerFromNBS', $add_partner_btn_id, $nbs_pib, $nbs_mb]
                        ],
                    ], true);
                }
                
                $missing_account['add_bank_account_html'] = $this->renderPartial('add_bank_account_html', [
                    'form_id' => $missing_account['form_id'],
                    'form_data' => CJSON::encode([
                        'model_name' => BankAccount::class,
                        'form_name' => $form_name
                    ]),
                    'bank_account_form_html' => ModelController::renderModelForm($bank_account_model, [
                        'form_id' => BankAccount::class . $missing_account['form_id'],
                        'formName' => $form_name
                    ])['html'],
                    'display_message' => $missing_account['display_message'],
                    'add_partner_btn' => $add_partner_btn,
                    'invoice' => $missing_account['invoice'],
                    'account_owner' => $missing_account['account_owner'], 
                    'account_number' => $missing_account['account_number'], 
                    'input_payment_date' => $missing_account['input_payment_date'], 
                    'input_amount' => $missing_account['input_amount'], 
                    'payment_code_display' => $missing_account['payment_code_display'], 
                    'call_for_number' => $missing_account['call_for_number'], 
                    'call_for_number_modul' => $missing_account['call_for_number_modul'], 
                    'call_for_number_partner' => $missing_account['call_for_number_partner'], 
                    'call_for_number_modul_partner' => $missing_account['call_for_number_modul_partner'], 
                ], true, false);
                    
                $this->sendEndEvent($missing_account);
                Yii::app()->end(); /// TODO: izbaciti kada se u sendendevent ubaci yii app end
            }
        }
        else
        {
            throw new SIMAWarnException(Yii::t('AccountingModule.BankStatement', 'ChosenConfigurationTypeProblem'));
        }
        
        $this->sendEndEvent([
            'message' => 'success'
        ]);
    }
    
    private function createBankStatement($bank_account_number, $date, $amount, $number)
    {
        $bank_account_id = SIMASQLFunctions::getBankAccountIdByNumber($bank_account_number);
        if(empty($bank_account_id))
        {
            return null;
        }

        $criteria = new SIMADbCriteria([
            'Model' => 'BankStatement',
            'model_filter' => [
                'account_id' => $bank_account_id,
                'date' => $date,
            ]
        ]);
        $bank_statement = BankStatement::model()->find($criteria);
        
        if($bank_statement === null)
        {            
            $bank_statement = new BankStatement();
            $bank_statement->account_id = $bank_account_id;
            $bank_statement->date = $date;
            $bank_statement->amount = $amount;
            $bank_statement->number = $number;
            $bank_statement->save();
            $bank_statement->refresh();
        }
        
        return $bank_statement;
    }
    
    private function populateBankStatement($account_number, $account_owner, $input_payment_date, $input_amount, $payment_code, 
                                            $call_for_number, $call_for_number_partner, $decimal_separator, 
                                            BankStatement $bank_statement, $encoding, $invoice, $bank_transaction_id, 
                                            $call_for_number_modul, $call_for_number_modul_partner)
    {
        try
        {
            $amount = $this->clearAmount($input_amount, $decimal_separator);
            $payment_date = date('d.m.Y',  strtotime($input_payment_date));

            if($amount === null || empty($amount))
            {
                $amount = 0;
            }

            if ($encoding !== 'UTF-8')
            {
                $call_for_number = iconv($encoding, 'UTF-8', $call_for_number);
                $call_for_number_partner = iconv($encoding, 'UTF-8', $call_for_number_partner);
                $account_owner = iconv($encoding, 'UTF-8//IGNORE', $account_owner); /// ignorise karaktere koji nisu ocekivanog ulaznog encoding-a
            }
            
            $_partner_id = null;
            $recurring_bill = null;
            if (!empty($call_for_number))
            {
                $recurring_bills = RecurringBill::model()->findAllByModelFilter([
                    'call_for_number' => $call_for_number
                ]);
                if (count($recurring_bills)==1)
                {
                    $recurring_bill = $recurring_bills[0];   
                    $_partner_id = $recurring_bill->partner_id;
                }
                elseif (count($recurring_bills)>1)
                {
                    Yii::app()->raiseNote('Za poziv na broj '
                            .$call_for_number
                            .' je nadjeno vise klijenata');
                }
            }

            $pay_code = null;
            $payment_code = trim($payment_code);
            if (!empty($payment_code))
            {
                $pay_code = PaymentCode::model()->findByAttributes(array('code'=>$payment_code));
                if ($pay_code==null)
                {
                    $pay_code = new PaymentCode();
                    $pay_code->code = $payment_code;
                    $pay_code->name = 'generic';
                    $pay_code->payment_type_id = Yii::app()->configManager->get('accounting.default_payment_type');
                    $pay_code->save();
                }
                
            }
            
            if(empty($account_number))
            {
                //NIJE UPISAN BROJ RACUNA, ako ne postoji recuriring, onda banka
                $_account_id = null;
                if (is_null($_partner_id))
                {
                    $_partner_id = $bank_statement->account->bank->company->partner->id;
                }
            }
            else
            {
                //upisan je ziro racun
                $account_id = SIMASQLFunctions::getBankAccountIdByNumber($account_number);

                $bank_account = null;
                if(!empty($account_id))
                {
                    $bank_account = BankAccount::model()->findByPk($account_id);
                }
                
                //ukoliko ne postoji i ne postoji recuring partner, pitamo korisnika ko je
                if(is_null($bank_account))
                {
                    $account_number = (string)$account_number;
                    $payment_code_display = (is_null($pay_code))?$payment_code:$pay_code->DisplayName;
                    $bank_code = substr($account_number,0,3);
                    
                    $missing_account = [
                        'form_id' => SIMAHtml::uniqid(),
                        'account_number'=> $account_number
                    ];
                    
                    $display_message = '';
                    $add_partner_btn = '';
                    if (!is_null($_partner_id))
                    {
                        $missing_account['partner_id'] = $_partner_id;
                        $display_message = Yii::t('AccountingModule.BankStatement', 'PartnerAutomaticallySetedByCallToNumber', [
                            '{company_name}' => $recurring_bill->partner->DisplayName
                        ]);
                    }
                    else
                    {
                        $company_params = $this->getCompanyInformationFromNBSByAccountNumber($account_number);

                        if (!empty($company_params['company_from_system']))
                        {
                            $missing_account['partner_id'] = $company_params['company_from_system']->id;
                            $display_message = Yii::t('AccountingModule.BankStatement', 'PartnerAutomaticallySetedByNBS', [
                                '{company_name}' => $company_params['company_from_system']->DisplayName
                            ]);
                        }
                        else if (!empty($company_params['nbs_pib']) || !empty($company_params['nbs_mb']))
                        {
                            $nbs_pib = !empty($company_params['nbs_pib']) ? $company_params['nbs_pib'] : '';
                            $nbs_mb = !empty($company_params['nbs_mb']) ? $company_params['nbs_mb'] : '';
                            $add_partner_btn_id = 'add_partner_btn_' . SIMAHtml::uniqid();
                            $add_partner_btn = $this->widget(SIMAButtonVue::class, [
                                'id' => $add_partner_btn_id,
                                'title' => Yii::t('AccountingModule.BankStatement', 'AddPartnerFromNBS'),
                                'onclick' => ['sima.accountingMisc.addBankAccountPartnerFromNBS', $add_partner_btn_id, $nbs_pib, $nbs_mb]
                            ], true);
                        }
                    }

                    $bank = Bank::model()->find([
                        'model_filter' => [
                            'bank_account_code' => $bank_code
                        ]
                    ]);
                    if (!empty($bank))
                    {
                        $missing_account['bank_id'] = $bank->id;
                    }
                    
                    $missing_account['currency_id'] = $bank_statement->account->currency_id;
                    
                    $bank_account_model = BankAccount::model();
                    $form_name = 'fromFile';
                    $form_data = CJSON::encode([
                        'model_name' => BankAccount::class,
                        'form_name' => $form_name
                    ]);
                    $bank_account_model->setModelAttributes([
                        'number' => $account_number,
                        'bank_id' => !empty($missing_account['bank_id']) ? $missing_account['bank_id'] : null,
                        'partner_id' => !empty($missing_account['partner_id']) ? $missing_account['partner_id'] : null,
                        'currency_id' => !empty($missing_account['currency_id']) ? $missing_account['currency_id'] : null,
                        'start_amount' => 0
                    ], $form_name);
                    $bank_account_form_html = ModelController::renderModelForm($bank_account_model, [
                        'form_id' => BankAccount::class . $missing_account['form_id'],
                        'formName' => $form_name
                    ])['html'];
                    
                    $missing_account['add_bank_account_html'] = $this->renderPartial('add_bank_account_html', [
                        'form_id' => $missing_account['form_id'],
                        'form_data' => $form_data,
                        'bank_account_form_html' => $bank_account_form_html,
                        'display_message' => $display_message,
                        'add_partner_btn' => $add_partner_btn,
                        'invoice' => $invoice,
                        'account_owner' => $account_owner, 
                        'account_number' => $account_number, 
                        'input_payment_date' => $input_payment_date, 
                        'input_amount' => $input_amount, 
                        'payment_code_display' => $payment_code_display, 
                        'call_for_number' => $call_for_number, 
                        'call_for_number_modul' => $call_for_number_modul, 
                        'call_for_number_partner' => $call_for_number_partner, 
                        'call_for_number_modul_partner' => $call_for_number_modul_partner, 
                    ], true, false);

                    $this->sendEndEvent($missing_account);
                    Yii::app()->end(); /// TODO: izbaciti kada se u sendendevent ubaci yii app end
                }
                 
                $_account_id = $bank_account->id;
                if (is_null($_partner_id))
                {
                    $_partner_id = $bank_account->partner_id;
                }
                elseif ($_partner_id !== $bank_account->partner_id)
                {
//                    Yii::app()->raiseNote('Prepoznat je ponavljajuci racun po pozivu na broj '
//                        .$call_for_number
//                        .' gde je partner '.$recurring_bill->partner->DisplayName
//                        .' a uplaceno je sa ziro racuna partnera '
//                        .$bank_account->partner->DisplayName
//                        );
                }
                
            }

            $new_payment=new Payment();
            $new_payment->payment_date = $payment_date;
            $new_payment->partner_id=$_partner_id;
            $new_payment->account_id=$_account_id;
            $new_payment->bank_statement_id = $bank_statement->id;
            $new_payment->call_for_number=$call_for_number;
            $new_payment->call_for_number_partner=$call_for_number_partner;
            $new_payment->amount=$amount;
            $new_payment->invoice=$invoice;
            $new_payment->transaction_id=$bank_transaction_id;
            $new_payment->call_for_number_modul = $call_for_number_modul;
            $new_payment->call_for_number_modul_partner = $call_for_number_modul_partner;
            if (!is_null($pay_code))
            {
                $new_payment->payment_type_id = $pay_code->payment_type_id;
                $new_payment->payment_code_id = $pay_code->id;
            }
            if (empty($new_payment->payment_type_id))
            {
                $new_payment->payment_type_id = Yii::app()->configManager->get('accounting.default_payment_type_for_empty_payment_code', true);
            }

            $new_payment_cjson = CJSON::encode($new_payment);
            $hash = md5($new_payment_cjson);

            $criteria = new SIMADbCriteria([
                'Model' => 'Payment',
                'model_filter' => [
                    'hash' => $hash,
                ]
            ]);
            
            $payment = Payment::model()->find($criteria);

            $shouldSaveNewPayment = true;
            if($payment !== null)
            {
                $shouldSaveNewPayment = false;
            }
            else if($call_for_number !== null && !empty($call_for_number) && isset($_account_id))
            {
                $criteria2 = new SIMADbCriteria([
                    'Model' => Payment::class,
                    'model_filter' => [
                        'account' => [
                            'ids' => $_account_id
                        ],
                        'call_for_number' => $call_for_number
                    ]
                ]);
                $paymentExistingInDB = Payment::model()->find($criteria2);

                if($paymentExistingInDB !== null && !isset($paymentExistingInDB->bank_statement_id))
                {
                    $shouldSaveNewPayment = false;
                    $paymentExistingInDB->payment_date = $new_payment->payment_date;
                    $paymentExistingInDB->partner_id = $new_payment->partner_id;
                    $paymentExistingInDB->amount = $new_payment->amount;
                    $paymentExistingInDB->invoice = $new_payment->invoice;
                    $paymentExistingInDB->payment_type_id = $new_payment->payment_type_id;
                    $paymentExistingInDB->bank_statement_id = $new_payment->bank_statement_id;
                    $paymentExistingInDB->payment_code_id = $new_payment->payment_code_id;
                    $paymentExistingInDB->call_for_number_partner = $new_payment->call_for_number_partner;
                    $paymentExistingInDB->payment_type_id = $new_payment->payment_type_id;
                    $paymentExistingInDB->payment_code_id = $new_payment->payment_code_id;
                    $paymentExistingInDB->save();
                }
            }

            if($shouldSaveNewPayment == true)
            {       
                $new_payment->hash = $hash;
                $new_payment->save();
            }
        }
        catch(SIMAExceptionBankAccountNumberInvalid $ex)
        {
            Yii::app()->raiseNote(Yii::t(
                    'AccountingModule.BankStatement', 
                    'CouldNotCreatePaymentInvalidPartnerBankAccountNumber',
                    [
                        '{bank_account_number}' => $ex->bank_account_number,
                        '{payment_date}' => $input_payment_date,
                        '{amount}' => $input_amount,
                        '{db_err_message}' => $ex->db_err_message
                    ]
            ));
//                return;
        }
    }
    
    private function getCompanyInformationFromNBSByAccountNumber($account_number)
    {
        $params = [];

        if (Yii::app()->NBSConnection->isConnected() === true)
        {
            $account_number_parsed = str_replace('-', '', $account_number);
            $account_number_parsed = substr_replace($account_number_parsed, '-', 3, 0);
            $account_number_parsed = substr_replace($account_number_parsed, '-', -2, 0);
            
            /**
             * ne treba vrsiti validaciju jer se pre ovog poziva odradila validacija pozivanjem
             * SIMASQLFunctions::getBankAccountIdByNumber($account_number);
             */
            
            $nbs_company_data = Yii::app()->NBSConnection->GetCompanyAccountByBankAccountNumber($account_number_parsed);
            if (!empty($nbs_company_data))
            {
                if (!empty($nbs_company_data[0]['TaxIdentificationNumber']))
                {
                    $params['nbs_pib'] = Company::getFullPIB($nbs_company_data[0]['TaxIdentificationNumber']);
                    $params['company_from_system'] = Company::model()->findByAttributes([
                        'PIB' => $params['nbs_pib']
                    ]);
                }
                else if (!empty($nbs_company_data[0]['NationalIdentificationNumber']))
                {
                    $params['nbs_mb'] = Company::getFullMB($nbs_company_data[0]['NationalIdentificationNumber']);
                    $params['company_from_system'] = Company::model()->findByAttributes([
                        'MB' => $params['nbs_mb']
                    ]);
                }
            }
        }
        
        return $params;
    }
    
//    private function createBankStatementsCSV($real_path, $config)
//    {
//        set_time_limit(5);
//        $number_of_empty_account_numbers = 0;
//        $rowCounter=-1;
//        $number_of_rows = SIMAMisc::numberOfRowsInCSV($real_path);
//        $file = fopen($real_path,"r");
//        while (($csv_row = fgetcsv($file, 0, $config->separator)) !== false)
//        {
//            set_time_limit(1);
//            $rowCounter++;
//            if($rowCounter===0)
//            {
//                continue;
//            }
//            $this->sendUpdateEvent(($rowCounter/$number_of_rows)*100);
//            $bank_account_number = $csv_row[$config->bank_account_id];
//            $csv_date = $csv_row[$config->bank_account_date];
//            $number = $csv_row[$config->bank_statement_number];
//            $date = SIMAHtml::UserToDbDate($csv_date);
//
//            try
//            {
//                $bank_statement = $this->createBankStatement($bank_account_number, $date, 0, $number);
//                if(!empty($bank_statement))
//                {
//                    $account_number = $csv_row[$config->account_number_col];
//                    $account_owner = $csv_row[$config->account_owner_col];
//                    $payment_date = $csv_row[$config->date_col];
//                    $amount = $csv_row[$config->amount_col];
//                    $unreleased = $csv_row[$config->amount2_col];
//                    $payment_code=$csv_row[$config->payment_code_col];
//                    $bank_transaction_id=$csv_row[$config->bank_transaction_id];
//                    $call_for_number_modul = null;
//                    $call_for_number_modul_partner = null;
//                                        
//                    if(empty($account_number))
//                    {
//                        $number_of_empty_account_numbers++;
//                    }
//                    
//                    if(!isset($amount) || $amount=='')
//                    {
//                        $invoice = true;
//                        $amount = isset($unreleased)?$unreleased:0;
//                        $call_for_number=$csv_row[$config->call_for_number_debit_col];
//                        $call_for_number_partner=$csv_row[$config->call_for_number_credit_col];
//                    }
//                    else
//                    {
//                        $invoice = false;
//                        $call_for_number=$csv_row[$config->call_for_number_credit_col];
//                        $call_for_number_partner=$csv_row[$config->call_for_number_debit_col];
//                    }
//
//                    $this->populateBankStatement(
//                            $account_number, 
//                            $account_owner, 
//                            $payment_date,
//                            $amount,
//                            $payment_code,
//                            $call_for_number,
//                            $call_for_number_partner,
//                            $config->decimal_separator,
//                            $bank_statement,
//                            $config->encoding,
//                            $invoice,
//                            $bank_transaction_id,
//                            $call_for_number_modul, 
//                            $call_for_number_modul_partner
//                    );
//                }
//                
//            } 
//            catch(SIMAExceptionBankAccountNumberInvalid $ex)
//            {
//                Yii::app()->raiseNote(Yii::t(
//                    'AccountingModule.BankStatement', 
//                    'CouldNotCreateBankStatementInvalidNumber',
//                    [
//                        '{bank_account_number}' => $ex->bank_account_number,
//                        '{date}' => $date,
//                        '{amount}' => 0,
//                        '{db_err_message}' => $ex->db_err_message
//                    ]
//                ));
//            }
//        }
//        set_time_limit(5);
//        fclose($file);
//        
//        if($number_of_empty_account_numbers > 0)
//        {
//            $this->raiseNote(Yii::t('AccountingModule.BankStatement', 'NumberOfEmptyStatementsThatWereSetBankForPartner', [
//                '{number_of_empty_account_numbers}' => $number_of_empty_account_numbers
//            ]));
//        }
//    }
    
    private function createBankStatementsXML($real_path, $config)
    {
        set_time_limit(5);
        $xml=simplexml_load_file($real_path);
        $xml_children = $xml->children();
        $xml_children_count = count($xml_children);
        $child_counter = 0;
        
        foreach ($xml_children as $child)
        {
            set_time_limit(1);
            $child_counter++;
            $this->sendUpdateEvent(($child_counter/$xml_children_count)*100);
            $bank_account_number = $this->getFromObjectByPath($child, $config->bank_account_id, true);
            $temp_date = $this->getFromObjectByPath($child, $config->bank_account_date, true);
            $date = substr($temp_date, 0, strpos($temp_date, 'T'));
            $amount = $this->getFromObjectByPath($child, $config->bank_account_amount, true);
            $number = $this->getFromObjectByPath($child, $config->bank_statement_number, true);
            
            try
            {
                $bank_statement = $this->createBankStatement($bank_account_number, $date, $amount, $number);

                $this->populateBankStatementXML($bank_statement, $child, $config);
            } 
            catch(SIMAExceptionBankAccountNumberInvalid $ex) 
            {
                Yii::app()->raiseNote(Yii::t(
                    'AccountingModule.BankStatement', 
                    'CouldNotCreateBankStatementInvalidNumber',
                    [
                        '{bank_account_number}' => $ex->bank_account_number,
                        '{date}' => $date,
                        '{amount}' => $amount,
                        '{db_err_message}' => $ex->db_err_message
                    ]
                ));
            }
        }
        set_time_limit(5);
    }
    
    private function populateBankStatementXML($bank_statement, $bank_statement_xml, $config)
    {
        $children = $this->getFromObjectByPath($bank_statement_xml, $config->path);
        
        $number_of_empty_account_numbers = 0;
    
        foreach($children->children() as $child)
        {
            $account_number = $this->getFromObjectByPath($child, $config->account_number_col, true);
            $account_owner = (string)$this->getFromObjectByPath($child, $config->account_owner_col, true);
            $payment_date = $this->getFromObjectByPath($child, $config->date_col, true);
            $amount = (string)$this->getFromObjectByPath($child, $config->amount_col, true);
            $unreleased = $this->getFromObjectByPath($child, $config->amount2_col, true);
            $payment_code=$this->getFromObjectByPath($child, $config->payment_code_col, true);
            $bank_transaction_id=$this->getFromObjectByPath($child, $config->bank_transaction_id, true);
            $call_for_number_modul = null;
            $call_for_number_modul_partner = null;
            
            if(empty($account_number))
            {
                $number_of_empty_account_numbers++;
            }

            if($unreleased=='credit')
            {
                $invoice=true;
                $amount = isset($amount)?$amount:0;
                $call_for_number=$this->getFromObjectByPath($child, $config->call_for_number_debit_col, true);
                $call_for_number_partner=$this->getFromObjectByPath($child, $config->call_for_number_credit_col, true);
            }
            else
            {
                $invoice=false;
                $call_for_number=$this->getFromObjectByPath($child, $config->call_for_number_credit_col, true);
                $call_for_number_partner=$this->getFromObjectByPath($child, $config->call_for_number_debit_col, true);
            }
            
            $this->populateBankStatement(
                    $account_number, 
                    $account_owner, 
                    $payment_date,
                    $amount,
                    $payment_code,
                    $call_for_number,
                    $call_for_number_partner,
                    $config->decimal_separator,
                    $bank_statement,
                    $config->encoding,
                    $invoice,
                    $bank_transaction_id,
                    $call_for_number_modul,
                    $call_for_number_modul_partner
            );
        }
        
        if($number_of_empty_account_numbers > 0)
        {
            $this->raiseNote(Yii::t('AccountingModule.BankStatement', 'NumberOfEmptyStatementsThatWereSetBankForPartner', [
                '{number_of_empty_account_numbers}' => $number_of_empty_account_numbers
            ]));
        }
    }
    
    private function validateFileFormat($real_path, $config)
    {
//        if($config->type === 0) //CSV
//        {
//            $max_col = max(array(
//                $config->bank_account_id, 
//                $config->bank_account_date,
//                $config->bank_account_amount,
//                $config->account_number_col,
//                $config->account_owner_col,
//                $config->date_col,
//                $config->amount_col,
//                $config->amount2_col,
//                $config->payment_code_col,
//                $config->call_for_number_debit_col,
//                $config->call_for_number_credit_col,
//                $config->bank_transaction_id
//            ));
//            
//            $i=0;
//            $file = fopen($real_path,"r");
//            while (($csv_row = fgetcsv($file, 0, $config->separator)) !== false)
//            {
//                if(count($csv_row) <= $max_col)
//                {
//                    return "invalid file - code: 7";
//                }
//                                
//                if($i!=0)
//                {
//                    if($csv_row[$config->bank_account_id] === null
//                        || $csv_row[$config->bank_account_date] === null
//                        || $csv_row[$config->bank_account_amount] === null
//                        || $csv_row[$config->account_number_col] === null
//                        || $csv_row[$config->account_owner_col] === null
//                        || $csv_row[$config->date_col] === null
//                        || $csv_row[$config->amount_col] === null
//                        || $csv_row[$config->amount2_col] === null
//                        || $csv_row[$config->payment_code_col] === null
//                        || $csv_row[$config->call_for_number_debit_col] === null
//                        || $csv_row[$config->call_for_number_credit_col] === null
//                        || $csv_row[$config->bank_transaction_id] === null)
//                    {
//                        return "invalid file - code: 8";
//                    }
//                }
//
//                $i++;
//            }
//
//            fclose($file);
//        }
//        else 
        if($config->type==1) //XML
        {
            libxml_use_internal_errors(true);
            $xml=simplexml_load_file($real_path);
            if($xml === false)
            {
                return Yii::t('AccountingModule.BankStatement', 'XmlLoadFailed');
            }
            
            if($xml->getName() !== "stmtrslist")
            {
                return "invalid file - code: 2";
            }
            
            foreach ($xml->children() as $child)
            {
                if($child->getName() !== "stmtrs")
                {
                    return "invalid file - code: 3";
                }
                
                if($this->getFromObjectByPath($child, $config->bank_account_id) === null
                    || $this->getFromObjectByPath($child, $config->bank_account_date) === null
                    || $this->getFromObjectByPath($child, $config->bank_account_amount) === null)
                {
                    return "invalid file - code: 4";
                }
                
                $sub_children = $this->getFromObjectByPath($child, $config->path);
                if($sub_children === null)
                {
                    return "invalid file - code: 5";
                }
                
                foreach($sub_children->children() as $sub_child)
                {
                    if($this->getFromObjectByPath($sub_child, $config->account_number_col) === null
                        || $this->getFromObjectByPath($sub_child, $config->account_owner_col) === null
                        || $this->getFromObjectByPath($sub_child, $config->date_col) === null
                        || $this->getFromObjectByPath($sub_child, $config->amount_col) === null
                        || $this->getFromObjectByPath($sub_child, $config->amount2_col) === null
                        || $this->getFromObjectByPath($sub_child, $config->payment_code_col) === null
                        || $this->getFromObjectByPath($sub_child, $config->call_for_number_debit_col) === null
                        || $this->getFromObjectByPath($sub_child, $config->call_for_number_credit_col) === null
                        || $this->getFromObjectByPath($sub_child, $config->bank_transaction_id) === null)
                    {
                        return "invalid file - code: 6";
                    }
                }
            }
        }
        else if ($config->type === 2) //ErsteWeb
        {
        }
        else if (
                $config->type === PaymentConfiguration::$TYPE_RAIFFEISEN
                ||
                $config->type === PaymentConfiguration::$TYPE_HALK_BANK
                ||
                $config->type === PaymentConfiguration::$TYPE_RAIFFEISEN_DEVIZNI
                ||
                $config->type === PaymentConfiguration::$TYPE_SOGE_XML
                ||
                $config->type === PaymentConfiguration::$TYPE_CSV_FILE
                ||
                $config->type === PaymentConfiguration::$TYPE_OTP_DEVIZNI_XML
            )
        {
            
        }
        else
        {
            return "unknown conf type";
        }
        
        return "success";
    }
    
    private function getFromObjectByPath($obj, $path, $returnString=false)
    {
        $exp = explode('.', $path);
        $column = $obj;
        foreach($exp as $element)
        {
            if(!isset($column->$element))
            {
                return null;
            }
            $column = $column->$element;
        }
        
        $result = $column;
        
        if($returnString === true)
        {
            $result = $column->__toString();
        }
        
        return $result;
    }
    
    private function clearAmount($amount,$separator)
    {
        $new_amount = '';
        for($i = 0; $i<strlen($amount); $i++)
        {
            if (is_numeric($amount[$i]))
            {
                $new_amount .= $amount[$i];
            }
            if ($separator==$amount[$i])
            {
                $new_amount .= '.';
            }
        }
        
        return $new_amount;
    }
    
    public function actionaddFlyingPaymentsToBankStatement($bank_statement_id)
    {
        
        $payments_jsons = $this->filter_post_input('payments');
        
        $bank_statement = ModelController::GetModel('BankStatement', $bank_statement_id);
        
        foreach ($payments_jsons as $payments_json)
        {
            $payment = Payment::model()->findByPk($payments_json['id']);
            if (is_null($payment) || !empty($payment->bank_statement_id))
            {
                $this->respondFail('zadato placanje nije letece');
            }
            $payment->bank_statement_id = $bank_statement->id;
            $payment->bank_statement = $bank_statement;
            $payment->payment_date = $bank_statement->date;
            $payment->save();
        }
        
        $this->respondOK();
    }
    
}
