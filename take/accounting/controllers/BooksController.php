<?php
/**
 * Description of BooksController
 *
 * @author Milos Sreckovic
 */
class BooksController extends SIMAController
{
    public function filters()
    {
        return array(
            'ajaxOnly-exportAccountToPdf-bookingPrediction-collapseAccountTransactionOrders-getBookDocumentDiffs-ExportAccountOrderToPdf',
        ) + parent::filters();
    }

    public function actionIndex($id, $year_id=null)
    {
        $start_date = 'null';
        $end_date = 'null';
        $_selector = $this->filter_post_input('selector',false);
        if (!is_null($_selector))
        {
            if (isset($_selector['display_scopes']['withDCCurrents']))
            {
                if (isset($_selector['display_scopes']['withDCCurrents'][1]))
                {
                    if (isset($_selector['display_scopes']['withDCCurrents'][1]['start_date']))
                    {
                        $start_date = $_selector['display_scopes']['withDCCurrents'][1]['start_date'];
                    }
                    if (isset($_selector['display_scopes']['withDCCurrents'][1]['end_date']))
                    {
                        $end_date = $_selector['display_scopes']['withDCCurrents'][1]['end_date'];
                    }
                }
            }
            else if (isset($_selector['start_date']) && isset($_selector['end_date']))
            {
                $start_date = $_selector['start_date'];
                $end_date = $_selector['end_date'];
            }
        }
        
        if ($start_date=='null' && $end_date=='null')
        {
            $daterange = '';
        }
        else if ($start_date=='null')
        {
            $daterange = '01.01.1000.<>'.$end_date;
        }
        else if ($end_date=='null')
        {
            $daterange = $start_date.'<>01.01.3000.';
        }
        else
        {
            $daterange = $start_date.'<>'.$end_date;
        }
        
        
        if (is_string($id))
        {            
            $temp_account = Account::model()->findByPk(intval($id));
            if (!empty($year_id))
            {
                $account = Account::model()->findByAttributes([
                    'code'=>$temp_account->code,
                    'year_id'=>$year_id
                ]);
            }
            else
            {
                $account = $temp_account;
            }
        }
        else
        {
            $account = Account::model()->findByPk($id);
        }
        
        if ($account == null)
        {
            $html =  'konto ne postoji';
            $title = $html;
        }
        else
        {
            $title = $account->DisplayName;
            $html = $this->renderPartial('index', array(
                'model' => $account,
                'daterange' => $daterange
            ), true, true);
        }
        
        $this->respondOK([
            'html' => $html,
            'title' => $title,
            'color' => '#FED39E',
        ]);
    }
    
    /**
     * funkcija koja dodaje rasknjizavanje na dokument
     * @param integer $id Document id
     */
    public function actionBookingPrediction()
    {
        $data = $this->setEventHeader();        
        /**
         * also can return WarehouseTransferItems 
         */
        $controller = $this;
        $booking_predicitons = BookingPrediction::Booking($data['id'], $data['style'], function($percent) use ($controller){
            $controller->sendUpdateEvent($percent);
            
        });
        foreach ($booking_predicitons as $prediction)
        {
            $prediction->save();
        }
        
        return $this->sendEndEvent();
    }
    
    /**
     * prikaz svih naloga za knjizenje
     */
    public function actionOrders($year_id=null)
    {
        $params = [
            'year_id' => !empty($year_id)?($year_id):(Year::get()->id),
            'uniq' => SIMAHtml::uniqid()
        ];
        
        $html = $this->renderPartial('orders',$params,true,true);
        
        $this->respondOK(array(
            'html' => $html,
            'title' => 'Nalozi za knjizenje'               
        ));
    }
    
    /**
     * Tab u kojem se prikazuje knjigovodstveni prikaz nekog dokumenta
     * @param type $id
     */
    public function actionDocumentTab($id)
    {
        $file = File::model()->findByPk($id);
        if (is_null($file))
        {
            throw new SIMAWarnException('Zadati model ne postoji ili je obrisan');
        }   

        /**
         * razdvajamo situaciju kada je dokumentu zadat nalog za knjizenje i kada nije zadat nalog za knjizenje
         */
        if (is_null($file->account_document))
        {
            $connected = ConnectAccountOrderDocuments::predictDocumentsAccountOrder($file->id);
            if (!$connected)
            {
                $canBook = $this->canBookDocument($file);
                $canBookMessage = '';
                if (gettype($canBook)!=='boolean')
                {
                    $canBookMessage = $canBook;
                    $canBook = false;
                }

                /**
                 * odredjivanje datuma
                 */
                $year = AccountDocument::predictBookingYearForDocument($file->id);
                
                if ($canBook && isset($file->advance_bill))
                {
                    $html = $this->renderPartial('document_tab_new_advance_bill',array(
                        'uniq' => SIMAHtml::uniqid(),
                        'model' => $file->advance_bill,
                        'payments' => $file->advance_bill->payments
                    ),true,true);
                }
                else
                {
//                    $create_account_order_per_document = Yii::app()->configManager->get('accounting.create_account_order_per_document');
//                    if ($create_account_order_per_document)
//                    {
//                        $onclick = ['sima.accounting.generateAccountDocument',$file->id];
//                    }
//                    else
//                    {
//                        $onclick = ['sima.accounting.addAccountDocumentChoose',$file->id,$year->id];
//                    }
                    $html = $this->renderPartial('document_tab_new',[
                        'file' => $file,
                        'canBook' => $canBook,
                        'canBookMessage' => $canBookMessage
                    ],true,true);
                }
            }
        }
        if (empty($html))
        {
            $file->refresh();
            
            if (isset($file->account_document->canceled_account_order))
            {
                $default_selected = 'canceled_transactions';
            }
            else
            {
                $default_selected = 'transactions'; 
            }
            $html = $this->renderPartial('document_tab',[
                'uniq' => SIMAHtml::uniqid(),
                'model' => $file->account_document,
                'default_selected' => $default_selected
            ],true, true);
        }
        
        $this->respondOK(['html' => $html]);
    }    
    
    private function canBookDocument(File $file)
    {
        /**
         * magacinski dokumenti
         */
        if (isset($file->warehouse_transfer))
        {
            if (
                    $file->warehouse_transfer->isReceiving 
                    || $file->warehouse_transfer->isDispatch
                )
            {
                if (!isset($file->warehouse_transfer->bill))
                {
                    $text = 'Ovaj dokument mora da bude povezan na racun da bi se proknjizio';
                    return $text;
                }
            }

            
            $_wt = $file->warehouse_transfer;
            $_w_from = $_wt->warehouse_from;
            $_w_to = $_wt->warehouse_to;
            
            if ($_w_from->isWarehouseStorage)
            {
                $_previous_wt = $_w_from->previousTransfer($_wt);
                if (
                        !is_null($_previous_wt) 
                        && 
                        (!isset($_previous_wt->file->account_document) || !$_previous_wt->file->account_document->isBooked)
                    )
                {
                    $text = 'Prethodni dokument za magacin '.$_w_from->DisplayHtml.' nije proknjizen - '.$_previous_wt->DisplayHtml;
                    $this->raiseNote($text);
//                    return $text;
                }
            }
            if ($_w_to->isWarehouseStorage)
            {
                $_previous_wt = $_w_to->previousTransfer($_wt);
                if (
                        !is_null($_previous_wt) 
                        && 
                        (!isset($_previous_wt->file->account_document) || !$_previous_wt->file->account_document->isBooked)
                    )
                {
                    $text = 'Prethodni dokument za magacin '.$_w_to->DisplayHtml.' nije proknjizen - '.$_previous_wt->DisplayHtml;
                    $this->raiseNote($text);
//                    return $text;
                }
            }
            
        }
        else if (isset($file->advance_bill))
        {
            if (!$file->advance_bill->connected)
            {
                return 'Avansni racun mora biti povezan sa uplatom da bi bio proknjizen';
            }
        }
        else
        {
//            $this->raiseNote('nije u spisku');
        }
        return true;
    }
    
    public function actionAddAccountDocument($account_order_id, $document_id)
    {
        $_forced_change = boolval($this->filter_get_input('forced_change'));
        
        $_canceled = boolval($this->filter_get_input('canceled',false));
        $account_order = AccountOrder::model()->findByPkWithCheck($account_order_id);
        $file = File::model()->findByPkWithCheck($document_id);
        
        if ($_canceled)
        {
            ConnectAccountOrderDocuments::addCanceledDocumentToOrder($account_order, $file, true, $_forced_change);
        }
        else
        {
            ConnectAccountOrderDocuments::addDocumentToOrder($account_order_id, $document_id, true, $_forced_change);
        }
        
        
        $this->respondOK();
    }
    
    public function actionRemoveAccountDocument($account_document_id)
    {
        $account_document = AccountDocument::model()->findByPkWithCheck($account_document_id);
 
        $_canceled = boolval($this->filter_get_input('canceled',false));
        
        if ($_canceled)
        {
            $account_document->canceled_account_order_id = null;
            $account_document->save();
        }
        else
        {
            $account_document->delete();
        }
        $this->respondOK();
    }
    
    public function actionAddCancelAccountDocument($account_order_id, $document_id)
    {
        $_forced_change = boolval($this->filter_get_input('forced_change'));
        $ao = AccountOrder::model()->findByPkWithCheck($account_order_id);
        $ad = AccountDocument::model()->findByPkWithCheck($document_id);
        if (isset($ad->canceled_account_order) && !$_forced_change)
        {
            throw new SIMAException('fajl je vec dodeljen nalogu za ponistenje knjizenja');
        }
        $note_code = SIMAHtml::uniqid();
        if (isset($ad->canceled_account_order) && $ad->canceled_account_order->booked)
        {
            $ad->canceled_account_order->booked = false;
            $ad->canceled_account_order->save();
            Yii::app()->raiseNote('Nalogu '.$ad->canceled_account_order->DisplayName.' je skinuta potvrda', $note_code);
        }
        if ($ao->booked)
        {
            $ao->booked = false;
            $ao->save();
            Yii::app()->raiseNote('Nalogu '.$ao->DisplayName.' je skinuta potvrda', $note_code);
        }
        $ad->canceled_account_order_id = $ao->id;        
        $ad->save();
        
        $this->respondOK();
    }
    
    public function actionRemoveCancelAccountDocument($document_id)
    {
        $ad = AccountDocument::model()->findByPkWithCheck($document_id);
        if (isset($ad->canceled_account_order) && $ad->canceled_account_order->booked)
        {
            $ad->canceled_account_order->booked = false;
            $ad->canceled_account_order->save();
            Yii::app()->raiseNote('Nalogu '.$ad->canceled_account_order->DisplayName.' je skinuta potvrda');
        }
        $ad->canceled_account_order_id = null;
        $ad->save();
        
        $this->respondOK();
    }
    
    /**
     * NEKAD PREDVIDJALO I PRAVILO NOVI NALOG ZA KNJIZENJE
     * Pravi se AccountDocument za fajl i opciono se dodaje novi AccountOrder za taj fajl
     * @param type $id - id fajla
     * @param type $date
     * @throws Exception
     */
    public function actionGenerateAccountDocument($document_id)
    {
        $document = File::model()->findByPkWithCheck($document_id);
        
        $document->attachBehavior('generateAccountDocument', 'FileGenerateAccountOrderBehavior');
        try
        {
            $document->generateAccountOrder();
        }
        catch (SIMAWarnExceptionNotForAutoAccountOrder $e)
        {
            Yii::app()->raiseNote($e->getMessage());
            $this->respondOK([
                'year_id' => AccountDocument::predictBookingYearForDocument($document->id)->id
            ]);
        }
        
        //eksplicitno pozivamo da se osvezi dokument
        $this->respondOK([],[$document]);
    }
    
    /**
     * Kontni okviri
     */
    public function actionAccountLayouts()
    {
        $html = $this->renderPartial('account_layouts',array(
            'uniq' => SIMAHtml::uniqid()
        ),true,true);
        
        $this->respondOK(array(
            'html' => $html,
            'title' => 'Kontni okviri',
        ));
    }
    
    /**
     * akcija koja sluzi samo za pretragu konta
     * obavezno je poslati year_id u filterima
     * @throws Exception
     */
    public function actionSearchAccounts()
    {
        $for_filter = $this->filter_post_input('filters');
//        if (!isset($for_filter['initial']['year_id']) && !isset($for_filter['initial']['year']['ids']))
//        {
//            throw new Exception('actionSearchAccounts - nije zadat year_id');
//        }
        
        //izvlacimo year_id i text. Polje text brisemo iz niza jer cemo ga obradjivati posebno zbog wildcard-a
        $text = $this->filter_post_input('search_text', false);
        if (empty($text))
        {
            $text = '';
        }
        
        $onlyLeafs = isset($for_filter['initial']['scopes']) && (
            (is_array($for_filter['initial']['scopes']) && in_array('onlyLeafAccounts', $for_filter['initial']['scopes'])) 
            ||
            (is_string($for_filter['initial']['scopes']) && 'onlyLeafAccounts' === $for_filter['initial']['scopes'])
            );
        //pretraga svih konta
        $account = Account::model();        
        $criteriaAccount = new SIMADbCriteria();
        foreach ($for_filter as $_filter_key=>$_filter_value) 
        {
            if (isset($for_filter[$_filter_key]['year_id']))
            {
                $year_id = $for_filter[$_filter_key]['year_id'];
            }
            else if (isset($for_filter[$_filter_key]['year']['ids']))
            {
                $year_id = $for_filter[$_filter_key]['year']['ids'];
            }
            else if (isset($for_filter[$_filter_key]['filter_scopes']['byYear'])) // byYear uvedeno da bi se u zavisnom polju mogla proslediti godina za konta (koji se izlistavaju u searchField-u), prva primena u Ios modelFormi
            {
                $year_id = Year::getByDate($for_filter[$_filter_key]['filter_scopes']['byYear'])->id;
            }
            else if (isset($for_filter[$_filter_key]['scopes']['byYear']))
            {
                $year_id = Year::getByDate($for_filter[$_filter_key]['scopes']['byYear'])->id;
            }
            else if (isset($for_filter[$_filter_key]['display_scopes']['byYear']))
            {
                $year_id = Year::getByDate($for_filter[$_filter_key]['display_scopes']['byYear'])->id;
            }
            
            if (isset($year_id) && !empty($year_id))
            {
                $_filter_value['year'] = [
                    'ids' => $year_id
                ];
            }
            $_temp_criteria = new SIMADbCriteria(array(
                'Model'=>'Account',
                'model_filter'=>$_filter_value
            ));
            $criteriaAccount->mergeWith($_temp_criteria);
        }
        
        if (!empty($text))
        {
            $criteriaAccount->mergeWith(new SIMADbCriteria([
                'Model' => 'Account',
                'model_filter'=>[
                    'text' => $text
                ]
            ]));
        }
        
        if (!isset($year_id))
        {
            throw new Exception('actionSearchAccounts - nije zadat year_id');
        }
        
        $criteriaAccount->limit = $onlyLeafs?20:10;        
        $account_settings = $account->modelSettings('searchField');
        $account_scopes = (isset($account_settings['scopes']))?$account_settings['scopes']:array();
        foreach ($account_scopes as $account_scope)
        {
            $account = $account->$account_scope();
        }
        $accounts = $account->byName()->findAll($criteriaAccount);
        $accounts_codes = [];
        foreach ($accounts as $_account)
        {
            array_push($accounts_codes, $_account->code);
        }
        
        $new_account_layouts = [];
        if (!$onlyLeafs)
        {
            //izvlacimo law_id na osnovu godine
            $year = Year::model()->findByPk($year_id);
            $law_id = $year->param('accounting.account_layout_law_id');

            //pretraga svih okvira
            $account_layout = AccountLayout::model();
            $criteria = new SIMADbCriteria(array(
                'Model'=>'AccountLayout', 
                'model_filter'=>array(
                    'law_id' => $law_id,
                    'text' => $text
                )
            ));
            $criteria->limit = 10;        
            $account_layout_settings = $account_layout->modelSettings('searchField');
            $account_layout_scopes = (isset($account_layout_settings['scopes']))?$account_layout_settings['scopes']:array();
            foreach ($account_layout_scopes as $account_layout_scope)
            {
                $account_layout = $account_layout->$account_layout_scope();
            }
            $account_layouts = $account_layout->byName()->findAll($criteria);

            foreach ($account_layouts as $account_layout)
            {
                if (!in_array($account_layout->code, $accounts_codes))
                {
                    array_push($new_account_layouts, $account_layout);
                }
            }
        }

        $html = $this->renderPartial('searchField', array(
            'accounts' => $accounts,
            'account_layouts' => $new_account_layouts,
            'text' => $text,
            'year_id' => $year_id
        ), true, true);
        
        $this->respondOK(array(
            'result'=>$html
        ));
    }
    
    public function actionAccountRemoveLink($transaction_id)
    {
        $at = AccountTransaction::model()->with('account')->findByPk($transaction_id);
        if (is_null($at) || is_null($at->account))
        {
            throw new Exception('Nije dobro zadat account');
        }
        $at->removeLink();
        $at->save();
        
        $this->respondOK();
    }
    
    public function actionAccountAddLink($transaction_id,$Model,$model_id)
    {
        if (!class_exists($Model))
        {
            $this->respondFail('Model '.$Model.' ne postoji');
        }
        $_m = $Model::model()->findByPk($model_id);
        if (is_null($_m))
        {
            $this->respondFail('Model '.$Model.' sa id '.$model_id.' ne postoji');
        }
        $at = AccountTransaction::model()->with('account')->findByPk($transaction_id);
        if (is_null($at) || is_null($at->account))
        {
            throw new Exception('Nije dobro zadat account');
        }
        $at->account =  $at->account->getLeafAccount($_m);
        $at->account_id =  $at->account->id;
        $at->save();
        
        $this->respondOK();
    }
    
    /**
     * 
     * @param type $old_account_id
     * @param type $account_id
     * @param type $partner_id
     * @throws Exception
     */
    public function actionAccountConvertTo($old_account_id,$account_id,$partner_id)
    {
        //OVO TREBA DORADITI...
        $partner = Partner::model()->findByPk($partner_id);
        $_oa = Account::model()->findByPk($old_account_id);
        $_na = Account::model()->findByPk($account_id);
        
        if (is_null($partner) || is_null($_oa) || is_null($_na))
        {
            throw new Exception('Nije dobro zadat account');
        }
        $_na_child =  $_na->getLeafAccount($partner);
        $transactions = AccountTransaction::model()->findByAttributes([
            'account_id' => $_oa->id
        ]);
        foreach ($transactions as $transaction)
        {
            /**/
            $transaction->account_id = $_na_child->id;
            $transaction->account = $_na_child;
            $transaction->save();
        }
        $at->account_id =  $at->account->id;
        $at->save();
        
        $this->respondOK();
    }
    
    public function actionExportAccountOrderToPdf()
    {
        $data = $this->setEventHeader();
        $account_order_id = $this->filter_input($data, 'account_order_id');
        $account_order = AccountOrder::model()->findByPkWithCheck($account_order_id);  

        //generisanje pdf-a za AccountOrder
        $controller = $this;
        $account_order_report = new AccountOrderReport([
            'account_order' => $account_order,
            'update_func' => 
                function($percent) use ($controller) {
                    $controller->sendUpdateEvent($percent);
                }
        ]);          
        $temp_file = $account_order_report->getPDF();
        $this->sendEndEvent([
            'filename' => $temp_file->getFileName(),
            'downloadname' => Yii::t('AccountingModule.AccountOrder','AccountOrder').'_'.$account_order->DisplayName.'.pdf',
        ]);
    }
    
    public function actionExportAccountToPdf()
    {
        $data = $this->setEventHeader();
        $account_id = $this->filter_input($data, 'account_id');
        $account = Account::model()->findByPkWithCheck($account_id);
        $guitable_filter = $this->filter_input($data, 'guitable_filter', false);
        $controller = $this;
        
        //generisanje pdf-a za karticu konta
        $report_obj = new AccountReport([
            'account' => $account,
            'guitable_filter' => $guitable_filter,
            'update_func' => function($percent) use ($controller) {
                $controller->sendUpdateEvent($percent);
            }
        ]);
        $temp_file = $report_obj->getPDF();
        $this->sendEndEvent([
            'filename' => $temp_file->getFileName(),
            'downloadname' => Yii::t('AccountingModule.Account','AccountCard') . '_' . $account->DisplayName.'.pdf'
        ]);       
    }
    
    public function actionCollapseAccountTransactionOrders()
    {
        $data = $this->setEventHeader();
        if (!isset($_GET['event_id']) || !isset($data['account_document_id']))
        {
            throw new SIMAException('actionCollapseAccountTransactionOrders - nisu lepo zadati parametri');
        }
        $account_document_id = $data['account_document_id'];
        $account_document = ModelController::GetModel('AccountDocument', $account_document_id);
        $account_transactions = AccountTransaction::model()->byOrder()->findAll(new SIMADbCriteria([
            'Model'=>'AccountTransaction',
            'model_filter'=>[
                'account_document'=>[
                    'account_order' => ['ids'=>$account_document->account_order_id]
                ]
            ]
        ]));
        
        $order = 1;
        $account_transactions_cnt = count($account_transactions);
        $max_while_cycle_exec_time_seconds = ini_get('max_execution_time');
        foreach ($account_transactions as $account_transaction)
        {            
            set_time_limit($max_while_cycle_exec_time_seconds);
            $percent = round(($order/$account_transactions_cnt)*100, 0, PHP_ROUND_HALF_DOWN);            
            $this->sendUpdateEvent($percent);
            if (intval($account_transaction->account_order_order) !== $order)
            {
                $account_transaction->account_order_order = $order;
                $account_transaction->save();
            }
            $order++;
        }
        
        $this->sendEndEvent();
    }
    
    public function actionGetAccountDocumentIdFromAccountOrder($account_order_id)
    {
        $account_order = AccountOrder::model()->findByPk($account_order_id);
        $account_document_id = null;
        if ($account_order->account_documents_cnt > 0)
        {
            $account_document_id = $account_order->account_documents[0]->id;
        }
        
        $this->respondOK([
            'account_document_id'=>$account_document_id
        ]);
    }
    
    public function actionGetBookDocumentDiffs()
    {
        set_time_limit(100);
        $data = $this->setEventHeader();
        if (empty($data['account_order_id']))
        {
            throw new SIMAExceptionFunctionWrongParams('actionGetBookDocumentDiffs', 'account_order_id');
        }
        $account_order_id = $data['account_order_id'];
        $account_order = AccountOrder::model()->findByPkWithCheck($account_order_id);
        $account_order->attachBehavior('ReCheck', 'AccountOrderReCheckBehavior');
        $diffs = $account_order->getBookingPredictionDiffs(function($percent){
            $this->sendUpdateEvent($percent);
        });
        
        $html = $this->widget('SIMADiffViewer', [
            'diffs' => $diffs,
            'can_confirm_diffs' => true
        ], true);

        $this->sendEndEvent([
            'html' => $html
        ]);
    }
    
    public function actionConfirmAccountDocumentDiffs($account_order_id)
    {
        set_time_limit(100);
        $account_order = AccountOrder::model()->findByPkWithCheck($account_order_id);
        $account_documents_cache = $this->filter_post_input('account_documents_cache', false);
        
        if (!empty($account_documents_cache))
        {
            $account_order->attachBehavior('ReCheck', 'AccountOrderReCheckBehavior');
            $account_order->setDiffsCache($account_documents_cache);
        }
        
        $this->respondOK();
    }
    
    public function actionAddBillsToAccountOrder($account_order_id)
    {
        $bill_ids = $this->filter_post_input('bill_ids', false);
        
        if (!empty($bill_ids))
        {
            foreach ($bill_ids as $bill_id)
            {
                ConnectAccountOrderDocuments::addDocumentToOrder($account_order_id, $bill_id);
            }
        }
        
        $this->respondOK();
    }
    
    public function actionCheckBillsForAccountOrder()
    {
        $bill_ids = $this->filter_post_input('bill_ids', false);
        
        $bills_year = null;
        
        if (!empty($bill_ids))
        {
            foreach ($bill_ids as $bill_id)
            {
                $bill = Bill::model()->findByPkWithCheck($bill_id);

                if ($bill->file->has_account_document)
                {
                    throw new SIMAWarnException(Yii::t('AccountingModule.Bill', 'BillAlreadyHasAccountOrder', [
                        '{bill_number}' => $bill->bill_number
                    ]));
                }

                $curr_year = date('Y', strtotime($bill->income_date));
                if (!empty($bills_year) && $bills_year !== $curr_year)
                {
                    throw new SIMAWarnException(Yii::t('AccountingModule.Bill', 'AllBillsMustHaveSameYear'));
                }
                if (empty($bills_year))
                {
                    $bills_year = $curr_year;
                }
            }
        }
        
        $bills_year_id = null;
        if (!empty($bills_year))
        {
            $bills_year_id = Year::get($bills_year)->id;
        }
        
        $this->respondOK([
            'bills_year_id' => $bills_year_id
        ]);
    }
    
    public function actionIsAccountOrderConfirmed($account_order_id)
    {
        $_ao = AccountOrder::model()->findByPkWithCheck($account_order_id);
        $this->respondOK([
            'is_confirmed' => $_ao->booked
        ]);
    }
    
    public function actionSetAccountOrderConfirmed($account_order_id, $set_confirmed)
    {
        $_ao = AccountOrder::model()->findByPkWithCheck($account_order_id);
        $_set_confirmed = $this->filter_get_input('set_confirmed');
        $_ao->booked = $_set_confirmed;
        if (!$_ao->validate())
        {
            $this->respondFail(SIMAHtml::modelErrorSummary($_ao));
        }
        $_ao->save();
        $_ao2 = AccountOrder::model()->findByPkWithCheck($account_order_id);
        
        $this->respondOK([
            'is_confirmed' => $_ao2->booked
        ]);
    }
    
    
}
