<?php

/**
 * @package SIMA
 * @subpackage Accounting
 */
class ReportController extends SIMAController
{
    //MilosS(27.11.2018): nisam nasao da se koristi
    //MIlosS(28.11.2918): koristi se za prikaz PDV-a Treba prebaciti u SIMAReport
    //                      nije se koristilo u jednom ajax-u, ali se kasnije koristilo u u nekim akcijama
    public function actionReport_change($month, $year, $name)
    {
        $params = $name::model()->getReportParams($month, $year);
        
        $html = $this->renderPartial('pdf_'.$name, $params, true,true);
        
        $this->respondOK(['html'=>$html]);
    }
    
    private static $foreign_keys_in_report = ['cost_location', 'cost_type', 'partner', 'month', 'year'];
    
    private function checkSumGroupBy()
    {
        $group_by = $this->filter_post_input('group_by');
        $_group_by_string = [];
        if (!is_array($group_by))
        {
            throw new SIMAException('group_by nije niz');
        }
        foreach ($group_by as $_elem)
        {
            if (!is_string($_elem) || !in_array($_elem, ['cost_location', 'cost_type', 'partner', 'month', 'year', 'report_type']))
            {
                throw new SIMAException('ne moze da se grupise po polju: '. SIMAMisc::toTypeAndJsonString($_elem));
            }
            
            if (in_array($_elem, self::$foreign_keys_in_report))
            {
                $_elem .= '_id';
            }
            $_group_by_string[] = $_elem;
        }
        if (count($group_by)<1)
        {
            throw new SIMAException('Mora postojati barem jedan element po kojem se grupise');
        }
        return [
            $group_by,
            implode(', ', $_group_by_string)
        ];
    }
    
    private function checkSumFilters()
    {
        $filters = $this->filter_post_input('model_filter', false);
        if (is_null($filters))
        {
            return [];
        }
        if (!is_array($filters))
        {
            throw new SIMAException('filters nije niz');
        }
        $model_filter = [];
        if (isset($filters['year']['ids']) && is_array($filters['year']['ids']) && count($filters['year']['ids']) === 1)
        {
            $_year_for_months = $filters['year']['ids'][0];
        }
        else
        {
            $_year_for_months = null;
        }
        foreach ($filters as $_filter => $_filter_data)
        {
            if (!is_string($_filter) || !in_array($_filter, ['has_value_on','cost_location', 'cost_type', 'partner', 'month', 'year', 'report_type']))
            {
                throw new SIMAException('filter ne postoji za polje: '. SIMAMisc::toTypeAndJsonString($_filter));
            }
            
            /**
             * TODO(44188)
             * PRIVREMENO dok se ne napravi da za godinu i mesec budu ID = sama godina
             */
            if ($_filter === 'year' && isset($_filter_data['ids']))
            {
                $_new_ids = [];
                foreach ($_filter_data['ids'] as $_year_name)
                {
                    $year = Year::get($_year_name);
                    $_new_ids[] = $year->id;
                }
                $_filter_data['ids'] = $_new_ids;
            }
            if (!is_null($_year_for_months) && $_filter === 'month' && isset($_filter_data['ids']))
            {
                $_new_ids = [];
                foreach ($_filter_data['ids'] as $_month_name)
                {
                    $month = Month::get($_month_name, $_year_for_months);
                    $_new_ids[] = $month->id;
                }
                $_filter_data['ids'] = $_new_ids;
            }
            
            $model_filter[$_filter] = $_filter_data;
            
        }
        return $model_filter;
    }

    private function parseReportType()
    {
        $report_type = $this->filter_post_input('report_type', false);
        if (is_null($report_type))
        {
            $report_type = 'total';
        }
        
        $update_model_tags = [];
        
        switch ($report_type)
        {
            case 'total': $MODEL = Report::model();break;
            case 'total_r': $MODEL = ReportR::model(); break;
//            case 'total': $MODEL = Report::model(); break;
//            case 'total': $MODEL = Report::model(); break;
//            case 'total': $MODEL = Report::model(); break;
//            case 'total': $MODEL = Report::model(); break;
            default:    throw new SIMAException("report_type: $report_type nije implementiran");
               
        }
        return $MODEL;
    }

    public function actionSum()
    {
        list($group_by,$group_by_string) = $this->checkSumGroupBy();
        $model_filter = $this->checkSumFilters();
        $MODEL = $this->parseReportType();
        $update_model_tags = $MODEL->getUpdateModelTags();
        $order = 'sum(income+cost) DESC';
        
        
        
        $reports = $MODEL->findAll([
            'select' => $group_by_string.', '
                . 'sum(income) as income, '
                . 'sum(income_notpayed) as income_notpayed, '
                . 'sum(income_advance) as income_advance, '
                . 'sum(cost) as cost, '
                . 'sum(cost_notpayed) as cost_notpayed, '
                . 'sum(cost_advance) as cost_advance',
            'group' => $group_by_string,
            'model_filter' => $model_filter,
            'order' => $order
        ]);
        
        
        $data = [];
        foreach ($reports as $report)
        {
            $data_prep = [
                'income' => $report->income,
                'income_notpayed' => $report->income_notpayed,
                'income_advance' => $report->income_advance,
                'cost' => $report->cost,
                'cost_notpayed' => $report->cost_notpayed,
                'cost_advance' => $report->cost_advance,
            ];
            foreach ($group_by as $_elem)
            {
                if (in_array($_elem, self::$foreign_keys_in_report))
                {
                    if (!is_null($report->$_elem))//moze da bude null kada nije setovana relacija (npr, nema vrste troska)
                    {
                        $data_prep[$_elem.'_tag'] = SIMAMisc::getVueModelTag($report->$_elem);
                    }
                    $_elem_id = $_elem.'_id';
                    $data_prep[$_elem_id] = $report->$_elem_id;
                }
                else
                {
                    $data_prep[$_elem] = $report->$_elem;
                }
            }
            
            $data[] = $data_prep;
        }
        
        
        $this->respondOK([
            'setter_object' => [
                'DATA' => $data,
                'UPDATE_MODEL_TAGS' => $update_model_tags,
            ]
        ]);
    }
    
    
    private function groupData(array $reports_items)
    {
//        $_model_array = [
//            //SERVICES
//            BillItem::model(),
//            //CASH_DESK
//            CashDeskItem::model(),
//            //WAREHOUSE
//            WarehouseTransferItem::model(),
//            //SALARY
//            AccountTransaction::model(),
//            Paycheck::model()
//            
//        ];
        $grouped_data = [
            'Bills' => [],
//            'CashDeskItem' => [],
//            'WarehouseTransferItem' => [],
//            'AccountTransaction' => [],
//            'Paycheck' => [],
//            'BillItem' => [],
        ];
        foreach ($reports_items as $_item)
        {
            $MODEL_NAME = $_item->model_name;
            $model = $MODEL_NAME::model()->findByPk($_item->model_id);
            if (is_null($model))
            {
                throw SIMAException(' NE POSTOJI MODEL NAME I MODEL_ID');
            }
            
            switch ($MODEL_NAME)
            {
                case 'BillItem':
                    $bill = $model->bill;
                    if (!isset($grouped_data['Bills'][$bill->id]))
                    {
                        $grouped_data['Bills'][$bill->id] = [
                            'bill_tag' => SIMAMisc::getVueModelTag($bill),
                            'included_items' => [],
                            'bill_items_count' => $bill->bill_items_count,
                            
                            'income' => 0,
                            'income_notpayed' => 0,
                            'income_advance' => 0,
                            'cost' => 0,
                            'cost_notpayed' => 0,
                            'cost_advance' => 0,
                        ];
                    }
                    $grouped_data['Bills'][$bill->id]['included_items'][]   = $model->id;
                    $grouped_data['Bills'][$bill->id]['income']          += $_item->income;
                    $grouped_data['Bills'][$bill->id]['income_notpayed'] += $_item->income;
                    $grouped_data['Bills'][$bill->id]['income_advance']  += $_item->income;
                    $grouped_data['Bills'][$bill->id]['cost']            += $_item->cost;
                    $grouped_data['Bills'][$bill->id]['cost_notpayed']   += $_item->cost_notpayed;
                    $grouped_data['Bills'][$bill->id]['cost_advance']    += $_item->cost_advance;

                    break;

                default:
                    break;
            }
            
//            $data_prep = [
//                'income'            => $_item->income,
//                'income_notpayed'   => $_item->income_notpayed,
//                'income_advance'    => $_item->income_advance,
//                'cost'              => $_item->cost,
//                'cost_notpayed'     => $_item->cost_notpayed,
//                'cost_advance'      => $_item->cost_advance,
//                'model_name'        => $_item->model_name,
//                'model_id'          => $_item->model_id,
//            ];
//            foreach ($group_by as $_elem)
//            {
//                if (!is_null($report->$_elem))
//                {
//                    $data_prep[$_elem.'_tag'] = SIMAMisc::getVueModelTag($report->$_elem);
//                }
//                else
//                {
//                    $data_prep[$_elem.'_tag'] = '';
//                }
//            }
            
//            $grouped_data[] = $data_prep;
        }
        
        
        return $grouped_data;
    }
    
    
    public function actionSpecification()
    {
        $model_filter = $this->checkSumFilters();
        $MODEL = $this->parseReportType();
        $update_model_tags = $MODEL->getUpdateModelTags();
        
        $reports_items = $MODEL->findAll([
            'select' => 
                  'income as income, '
                . 'income_notpayed as income_notpayed, '
                . 'income_advance as income_advance, '
                . 'cost as cost, '
                . 'cost_notpayed as cost_notpayed, '
                . 'cost_notpayed as cost_advance, '
                . 'model_name as model_name, '
                . 'model_id as model_id, '
                . 'report_type as report_type',
            'model_filter' => $model_filter,
            'order' => 'report_type, model_name'
        ]);
        
        
        $this->respondOK([
            'setter_object' => [
                'DATA' => $this->groupData($reports_items),
                'UPDATE_MODEL_TAGS' => $update_model_tags,
            ]
        ]);
    }
}