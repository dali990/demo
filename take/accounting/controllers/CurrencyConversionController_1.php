<?php

class CurrencyConversionController extends SIMAController
{
    public function actionForeignConversionInfo($conversionId)
    {
        $conversion = ModelController::GetModel('CurrencyConversion', $conversionId);
        
        $paymentIn = $conversion->payment_in;
        $paymentOut = $conversion->payment_out;
        $inCurrency = $paymentIn->bank_statement->account->currency;
        $outCurrency = $paymentOut->bank_statement->account->currency;
        
        $date = $paymentIn->payment_date;
        
        $criteriaIn = new SIMADbCriteria([
            'Model' => 'MiddleCurrencyRate',
            'model_filter' => [
                'date' => $date,
                'currency' => [
                    'ids' => $inCurrency->id
                ]
            ]
        ]);
        $criteriaOut = new SIMADbCriteria([
            'Model' => 'MiddleCurrencyRate',
            'model_filter' => [
                'date' => $date,
                'currency' => [
                    'ids' => $outCurrency->id
                ]
            ]
        ]);
                
        $middleCurrencyRateIn = MiddleCurrencyRate::model()->find($criteriaIn);
        $middleCurrencyRateOut = MiddleCurrencyRate::model()->find($criteriaOut);
                
        $inInDin = $middleCurrencyRateIn->value;
        $outInDin = $middleCurrencyRateOut->value;
        
        $inNum = $inInDin/$outInDin;
        $outNum = $outInDin/$inInDin;
                
        $html = $this->renderPartial('conversionInfo', [
            'inCurrency' => $inCurrency,
            'outCurrency' => $outCurrency,
            'inNum' => $inNum,
            'outNum' => $outNum,
        ], true, true);
                        
        $this->respondOK([
            'html' => $html
        ]);
    }
}