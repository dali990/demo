<?php
/**
 * Description of BaseController
 *
 * @author goran-set
 */
class CostTypesController extends SIMAController
{
    
    public function filters()
    {
        return array(
            'ajaxOnly'
            . '-getCostTypesForTransferFromPreviousYear'
            . '-getSelectedCostTypesForTransfer'
            . '-addCostTypesToYear'
        ) + parent::filters();
    }
    
    public function actionIndex()
    {
        $cost_or_income = $this->filter_get_input('cost_or_income');
        $is_cost = $cost_or_income === CostType::$TYPE_COST;
        
        
        $title = $is_cost?Yii::t('AccountingModule.CostType','CostTypes'):Yii::t('AccountingModule.CostType','IncomeTypes');
        
        $default_year = AccountingYear::getLastConfirmed();
//        $default_year = Year::get();
//        $default_year = Year::get()->prev();
        $prev_year = $default_year->prev();
        
        $years = AccountingYear::model()->findAll(new SIMADbCriteria([
            'Model'=>'AccountingYear',
            'model_filter'=>[
                'display_scopes'=>'byName'
            ]
        ]));
        $menu_array = [];
        foreach ($years as $year)
        {
            $menu_array[] = [
                'title' => $year->DisplayName,
                'code' => $year->id
            ];
        }                
                
        $html = $this->renderPartial('index', [
            'cost_or_income' => $cost_or_income,
            'menu_array' => $menu_array,
            'default_year' => $default_year,
            'prev_year' => $prev_year,
            'uniq' => SIMAHtml::uniqid(),
        ], true);
        
        
        $this->respondOK([
            'title' => $title,
            'html' => $html
        ]);
    }
    
    
    public function actionIndexRightPanel($year_id,$cost_or_income)
    {

        $is_cost = $cost_or_income === CostType::$TYPE_COST;
        
        
        $title = $is_cost?Yii::t('AccountingModule.CostType','CostTypes'):Yii::t('AccountingModule.CostType','IncomeTypes');
        
        $default_year = Year::model()->findByPkWithCheck($year_id);
        $prev_year = $default_year->prev();
        
        $years = AccountingYear::model()->findAll(new SIMADbCriteria([
            'Model'=>'AccountingYear',
            'model_filter'=>[
                'display_scopes'=>'byName'
            ]
        ]));
        $menu_array = [];
        foreach ($years as $year)
        {
            $menu_array[] = [
                'title' => $year->DisplayName,
                'code' => $year->id
            ];
        }
        
        $transfer_all_button_title = Yii::t('AccountingModule.CostType','TransferAllFromPreviousYear');
        $transfer_selected_button_title = Yii::t('AccountingModule.CostType','TransferAllSelected');
        $left_title = $title.' - '.$default_year->DisplayName;
        $right_title = $title.' '.Yii::t('AccountingModule.CostType','NotDefinedInYear',['{year}' => $default_year->DisplayName]);
        $uniq = SIMAHtml::uniqid();
        $left_table_id = "left_table$uniq";
        $right_table_id = "right_table$uniq";
        //Vrste troska koje nisu definisane u 20__ godini        
        
        $year_confirmed = $default_year->accounting->confirmed;
        if ($year_confirmed)
        {
            $add_button = false;
            $custom_buttons1 = [];
            $custom_buttons2 = [];
        }
        else
        {
            $add_button = [
                'button1' => "sima.model.form('CostType','',{"
                    . "'init_data': {"
                        . "'CostType': {"
                            . "'cost_or_income':'$cost_or_income',"
                            . "'year':{'ids':'$default_year->id'}"
                        . "},"
                    . "},"
                    . "'onSave': function(){"
                        ."\$('#$left_table_id').simaGuiTable('populate');"
                    . "}"
                . "});"
            ];
            $custom_buttons1 = [
                $transfer_all_button_title => [
                    'title'=>$transfer_all_button_title,
                    'func'=>"sima.accounting.transferCostTypesFromPreviousYear('$right_table_id','$left_table_id',$prev_year->id,$default_year->id)",
                ]
            ];
            $custom_buttons2 = [
                $transfer_selected_button_title => [
                    'title' => $transfer_selected_button_title,
                    'func' => "sima.accounting.transferSelectedCostTypesToYear('$right_table_id','$left_table_id',$default_year->id)",
//                    'onclick' => ['sima.test']
                ]
            ];
        }
        
        $html = $this->renderPartial('index_right_panel', [
            'cost_or_income' => $cost_or_income,
            'default_year' => $default_year,
            'prev_year' => $prev_year,
            'left_table_id' => $left_table_id,
            'right_table_id' => $right_table_id,
            'left_title' => $left_title,
            'right_title' => $right_title,
            'add_button' => $add_button,
            'custom_buttons1' => $custom_buttons1,
            'custom_buttons2' => $custom_buttons2,
        ], true);
        
        
        $this->respondOK([
            'html' => $html
        ]);
    }
    
    public function actionGetCostTypesForTransferFromPreviousYear()
    {
        $data = $this->setEventHeader();
        if (!isset($data['prev_year_id']) || !isset($data['curr_year_id']))
        {
            throw new SIMAException('actionGetCostTypesForTransferFromPreviousYear - nisu lepo zadati parametri');
        }
        
        $prev_year_id = $data['prev_year_id'];
        $curr_year_id = $data['curr_year_id'];
        $prev_year = Year::model()->findByPk($prev_year_id);
        $curr_year = Year::model()->findByPk($curr_year_id);
        
        $prev_cost_types_to_accounts = CostTypeToAccount::model()->findAll(new SIMADbCriteria([
            'Model'=>'CostTypeToAccount',
            'model_filter'=>[
                'account'=>[
                    'year'=>[
                        'ids'=>$prev_year_id
                    ]
                ]                
            ]
        ]));
        
        $cost_types = [];
        $prev_cost_types_to_accounts_cnt = count($prev_cost_types_to_accounts);
        $max_while_cycle_exec_time_seconds = ini_get('max_execution_time');
        $i = 1;
        foreach ($prev_cost_types_to_accounts as $prev_cost_type_to_account) 
        {
            set_time_limit($max_while_cycle_exec_time_seconds);
            $percent = round(($i/$prev_cost_types_to_accounts_cnt)*100, 0, PHP_ROUND_HALF_DOWN);
            $this->sendUpdateEvent($percent);
            $curr_cost_type_to_account = CostTypeToAccount::model()->find(new SIMADbCriteria([
                'Model'=>'CostTypeToAccount',
                'model_filter'=>[
                    'cost_type'=>[
                        'ids'=>$prev_cost_type_to_account->cost_type_id
                    ],
                    'account'=>[
//                        'code'=>$prev_cost_type_to_account->account->code,
                        'year'=>[
                            'ids'=>$curr_year_id
                        ]
                    ]
                ]
            ]));            
            if (is_null($curr_cost_type_to_account))
            {
                array_push($cost_types, [
                    'prev_account_code'=>$prev_cost_type_to_account->account->code,
                    'prev_account_display_name'=>$prev_cost_type_to_account->account->DisplayName,
                    'cost_type_id'=>$prev_cost_type_to_account->cost_type_id,
                    'cost_type_display_name'=>$prev_cost_type_to_account->cost_type->DisplayName,
                ]);
            }
            
            $i++;
        }

        if (count($cost_types) === 0)
        {
            throw new SIMAWarnException(Yii::t('AccountingModule.CostType','AllCostTypesTransfered',[
                '{prev_year}' => $prev_year->DisplayName,
                '{curr_year}' => $curr_year->DisplayName,
                '{prev_year}' => $prev_year->DisplayName,
            ]));
//            throw new SIMAWarnException("Sve vrste troškova iz $prev_year->year su prebačene u $curr_year->year godinu.");
        }

        $uniq_id = SIMAHtml::uniqid();
        $html = $this->renderPartial('cost_types_for_transfer',array(
            'uniq_id'=>$uniq_id,
            'cost_types'=>$cost_types,
            'cost_types_for_ignore' => [],
            'curr_year'=>$curr_year
        ),true,false);
        
        $this->sendEndEvent([
            'html'=>$html,
            'uniq_id'=>$uniq_id
        ]);        
    }
    
    
    public function actionGetSelectedCostTypesForTransfer()
    {
        $data = $this->setEventHeader();
        if (!isset($data['cost_types_ids']) || !isset($data['year_id']))
        {
            throw new SIMAException('actionGetCostTypesForTransferFromPreviousYear - nisu lepo zadati parametri');
        }
        
        $curr_year = AccountingYear::model()->findByPkWithCheck($data['year_id']);
        $table_to_id = $data['table_to_id'];
        
        $cost_types = CostType::model()->findAll(new SIMADbCriteria([
            'Model'=>'CostTypeToAccount',
            'model_filter'=>[
                'ids' => $data['cost_types_ids']              
            ]
        ]));
        
        
        $selected_cost_types_cnt = count($cost_types);
        $max_while_cycle_exec_time_seconds = ini_get('max_execution_time');
        $i = 1;
        $cost_types_for_html = [];
        $cost_types_for_ignore = [];
        foreach ($cost_types as $cost_type) 
        {
            set_time_limit($max_while_cycle_exec_time_seconds);
            $percent = round(($i/$selected_cost_types_cnt)*100, 0, PHP_ROUND_HALF_DOWN);
            $this->sendUpdateEvent($percent);
            $curr_cost_type_to_account = CostTypeToAccount::model()->find(new SIMADbCriteria([
                'Model'=>'CostTypeToAccount',
                'model_filter'=>[
                    'cost_type'=>[
                        'ids' => $cost_type->id
                    ],
                    'account'=>[
                        'year'=>[
                            'ids'=>$curr_year->id
                        ]
                    ]
                ]
            ]));            
            if (is_null($curr_cost_type_to_account))
            {
                $_local_year = $curr_year;
                $account_code = null;
                $account_display_name = '';
                while($_local_year!=null && $account_code==null)
                {
                    $curr_cost_type_to_account = CostTypeToAccount::model()->find(new SIMADbCriteria([
                        'Model'=>'CostTypeToAccount',
                        'model_filter'=>[
                            'cost_type'=>[
                                'ids' => $cost_type->id
                            ],
                            'account'=>[
                                'year'=>[
                                    'ids'=>$_local_year->id
                                ]
                            ]
                        ]
                    ]));   
                    if (!is_null($curr_cost_type_to_account))
                    {
                        $_account = $curr_cost_type_to_account->account->getLeafAccount();
                        $account_code = $_account->code;
                        $account_display_name = $_account->DisplayName;
                    }
                    else 
                    {
                        $_local_year = AccountingYear::model()->findByPk($_local_year->base_year->prev()->id);
                    }
                }
                
                if (empty($account_code))
                {
                    $cost_types_for_ignore[] = $cost_type;
                }
                else
                {
                    array_push($cost_types_for_html, [
                        'prev_account_code' => $account_code,
                        'prev_account_display_name' => $account_display_name,
                        'cost_type_id' => $cost_type->id,
                        'cost_type_display_name' => $cost_type->DisplayName,
                    ]);
                }
            }
            else
            {
                //ovo ne bi trebalo da moze da se desi
                error_log(__METHOD__.' ovo ne bi trebalo da moze da se desi');
//                Yii::app()->raiseNote('');
            }
            
            $i++;
        }
        if ($selected_cost_types_cnt === 1 && count($cost_types_for_ignore) === 1)
        {
            $this->raiseAction('sima.model.form', [
                'CostType',
                $cost_types_for_ignore[0]->id,
                [
                    'init_data' =>[
                        'CostType' => [
                            'year' => ['ids' => $curr_year->id]
                        ]
                    ],
                    'onSave' => "function(){\$('#$table_to_id').simaGuiTable('populate');}"
                ]
            ]);
            $this->sendEndEvent([
                'html'=>''
            ]); 
        }
        else
        {
            $uniq_id = SIMAHtml::uniqid();
            $html = $this->renderPartial('cost_types_for_transfer',array(
                'uniq_id' => $uniq_id,
                'cost_types' => $cost_types_for_html,
                'cost_types_for_ignore' => $cost_types_for_ignore,
                'curr_year' => $curr_year
            ),true,false);
            $this->sendEndEvent([
                'html'=>$html,
                'uniq_id'=>$uniq_id
            ]); 
        }
    }
    
    
    public function actionAddCostTypesToYear()
    {
        $data = $this->setEventHeader();
        if (!isset($data['cost_types']) || !isset($data['year_id']))
        {
            throw new SIMAException('actionTransferCostTypesFromPreviousYear - nisu lepo zadati parametri');
        }
        
        $year_id = $data['year_id'];
        $cost_types = $data['cost_types'];
        
        $year = Year::model()->findByPkWithCheck($year_id);
        
        $cost_types_cnt = count($cost_types);
        $max_while_cycle_exec_time_seconds = ini_get('max_execution_time');
        $i = 1;
        foreach ($cost_types as $cost_type) 
        {
            set_time_limit($max_while_cycle_exec_time_seconds);
            $percent = round(($i/$cost_types_cnt)*100, 0, PHP_ROUND_HALF_DOWN);
            $this->sendUpdateEvent($percent);

            $account = Account::get($cost_type['prev_account_code'], $year);
            $cost_type = CostType::model()->findByPkWithCheck($cost_type['cost_type_id']);
            $cost_type_to_curr_account = CostTypeToAccount::model()->findByAttributes([
                'cost_type_id' => $cost_type->id,
                'account_id' => $account->id
            ]);
            if (is_null($cost_type_to_curr_account))
            {
                $cost_type_to_curr_account = new CostTypeToAccount();
                $cost_type_to_curr_account->cost_type_id = $cost_type->id;
                $cost_type_to_curr_account->account_id = $account->id;
                $cost_type_to_curr_account->year_id = $year->id;
                $cost_type_to_curr_account->save();
            }            
            $i++;
        }                                
        
        $this->sendEndEvent();
    }   
    
    
}
