<?php
/**
 * Description of BooksController
 *
 * @author Milos Sreckovic
 */
class BillItemsController extends SIMAController
{
    public function filters()
    {
        return array(
            'ajaxOnly'
        ) + parent::filters();
    }
    
    public function actionIndex($bill_id)
    {
        $bill = Bill::model()->findByPkWithWarnCheck($bill_id);
        //mora da se doda layout okolo jer je moguce da se otvori u dialogu
        //i onda ako se uradi update view-a, ne rasiri se model-view kako treba
        $html = '<div class="sima-layout-panel">'
                .$this->renderModelView($bill,'bill_items')
                .'</div>';
        
        $this->respondOK([
            'title' => 'Stavke u racunu | '.$bill->partner->DisplayName.' | '.$bill->DisplayName,
            'html' => $html
        ]);
    }
    
    public function actionAddToFixedAssets($fixed_asset_type_id, $tax_type)
    {
        $bill_items_ids = isset($_POST['bill_items_ids'])?$_POST['bill_items_ids']:[];
        
        $first_bill_item = BillItem::model()->findByPkWithCheck($bill_items_ids[0]);
        $bill_id = $first_bill_item->bill_id;
        $last_order_of_bill_items_for_fixed_assets = $first_bill_item->bill->getLastOrderNumberOfBillItemsForType(BillItem::$FIXED_ASSETS);
        $params_for_after_work = ['bill_id' => $bill_id];
        
        foreach ($bill_items_ids as $bill_item_id) 
        {
            $bill_item = ModelController::GetModel('BillItem', $bill_item_id);
            if ($bill_item->isWarehouseBillItem)
            {
                $this->respondFail(Yii::t('FixedAssetsModule.FixedAsset','CannotCreateFromWarehouse'));
            }
            $bill_item->addAsFixedAsset($fixed_asset_type_id, $tax_type);
            
            $params_for_after_work['bill_items_ids_with_new_orders'][] = [
                'bill_item_id' => $bill_item_id,
                'new_order' => $last_order_of_bill_items_for_fixed_assets + 1
            ];
            $last_order_of_bill_items_for_fixed_assets++;
            Yii::app()->registerAfterWork('WorkBillItems', $params_for_after_work, false);
        }
        
        $this->respondOK([]);
    }   
    
    public function actionAreItemsLocked($bill_id)
    {
        $bill = Bill::model()->findByPkWithCheck($bill_id);
        $this->respondOK([
            'is_locked' => $bill->lock_amounts
        ]);
    }
    
    public function actionSetItemsLocked($bill_id, $lock_amounts)
    {
        $bill = Bill::model()->findByPkWithCheck($bill_id);
        $_lock_amounts = $this->filter_get_input('lock_amounts');
        $bill->lock_amounts = $_lock_amounts;
        $bill->save();
//        sleep(3);
        //provera
        $bill2 = Bill::model()->findByPkWithCheck($bill_id);
        
        $this->respondOK([
            'is_locked' => $bill2->lock_amounts
        ]);
    }
    
    /**
     * Vue
     * vraca spisak racuna sa spiskom stavki koje pripadaju odredjenom filteru
     * filter je definisan u klasi pod odredjenim filterom
     * zadaje se 
     * $model_name
     * $model_id
     * $filter
     */
    public function actionReportFromBillItems()
    {
        $filter = $this->filter_post_input('filter_type');
        $model_filter = $this->filter_post_input('model_filter');
        
        $UPDATE_MODEL_TAGS = [];

        $bill_items_filter = $this->getFilterForReportPerBillItem($filter, $model_filter, $UPDATE_MODEL_TAGS);

        $this->respondOK([
            'setter_object' => [
                'DATA' => $this->packPerBill($bill_items_filter),
                'UPDATE_MODEL_TAGS' => $UPDATE_MODEL_TAGS
            ]
        ]);
    }
    
    private function packPerBill($bill_items_filter)
    {
        $return_object = [];
        
        $bills = AnyBill::model()->findAll([
            'model_filter' => [
                'bill_items' => $bill_items_filter
            ]
        ]);
        
        foreach ($bills as $bill)
        {
            $found_items = BillItem::model()->findAll([
                'model_filter' => [
                    'AND',
                    $bill_items_filter,
                    ['bill'=>['ids' => $bill->id]]
                ]
            ]);
            $full_amount = count($found_items) == count($bill->bill_items);
            $show_items = [];
            foreach ($found_items as $found_item)
            {
                $show_items[] = $found_item->id;
            }
            
            $return_object[] = [
                'bill_tag' => SIMAMisc::getVueModelTag($bill),
                'full_amount' => $full_amount,
                'show_items' => $show_items
            ];
        }
        
//        $bill_items = BillItem::model()->findAll([
//            'model_filter' => $bill_items_filter
//        ]);
//        foreach ($bill_items as $item)
//        {
//            $return_object[] = SIMAMisc::getVueModelTag($item);
//        }
        return $return_object;
    }
    
    //ovo mora u komponentu ili nesto
    private function getFilterForReportPerBillItem($filter, $model_filter, &$UPDATE_MODEL_TAGS)
    {
        
        $bill_type = (strpos($filter, 'advance') === FALSE)?Bill::$BILL:Bill::$ADVANCE_BILL;
        $scopes = ['notCanceled'];
        if (!in_array($filter, ['income','cost','income_total','cost_total']))
        {
            $scopes[] = 'unreleased';
        }
        $invoice = (strpos($filter, 'cost') === FALSE)?true:false;
        
        
        
        $UPDATE_MODEL_TAGS[] = SIMAHtml::getTag(CostLocation::model());
        if (strpos($filter, 'total') === FALSE)
        {
            $cl_ids = $model_filter['cost_location_id'];
        }
        else
        {
            $_cl = CostLocation::model()->withParentChild()->findByPk($model_filter['cost_location_id']);
            $cl_ids = str_replace(['{','}'], '', $_cl->children_ids);
        }
        
        $partner = [];
        if (isset($model_filter['partner_id']))
        {
            $UPDATE_MODEL_TAGS[] = SIMAHtml::getTag(Partner::model());
            $partner = ['ids' => $model_filter['partner_id']];
        }
        
        if (isset($model_filter['month']))
        {
            $month = Month::get($model_filter['month'], $model_filter['year']);
            if (!is_null($month))
            {
                $scopes['VATInMonth'] = [$model_filter['month'], $model_filter['year']];
            }
//            error_log(SIMAMisc::toTypeAndJsonString($month));;
        }
        
        return [
            'OR',
            [
                'cost_location' => ['ids' => $cl_ids],
                'bill' => [
                    'partner' => $partner,
                    'bill_type' => $bill_type,
                    'invoice' => $invoice,
                    'scopes' => $scopes
                ],
            ],
            [
                'AND',
                [
                    'NOT',
                    ['cost_location' => []]
                ],
                [
                    'bill' => [
                        'partner' => $partner,
                        'bill_type' => $bill_type,
                        'invoice' => $invoice,
                        'local_cost_location' => ['ids' => $cl_ids],
                        'scopes' => $scopes
                    ],
                ]
            ]
        ];
    }
    
    public function actionConvertToWarehouse()
    {
        $bill_item_id = $this->filter_post_input('bill_item_id');
        $bill_item = BillItem::model()->findByPkWithCheck($bill_item_id);
        $bill = $bill_item->bill;
        
        $warehouse_material_id = $this->filter_post_input('warehouse_material_id');
        $warehouse_material = WarehouseMaterial::model()->findByPkWithCheck($warehouse_material_id);
        
        if (!$bill->isBillWT)
        {
            throw new SIMAException('Racun mora biti Racun-prijemnica ili Racun-otpremnica');
        }
        
        $WT_item_check = WarehouseTransferItem::model()->findByAttributes([
            'warehouse_material_id' => $warehouse_material->id,
            'warehouse_transfer_id' => $bill->id
        ]);
        if (!is_null($WT_item_check))
        {
            throw new SIMAWarnException('Ovaj materijal vec postoji kao stavka');
        }
        $WT_item = new WarehouseTransferItem();
        $WT_item->warehouse_material_id = $warehouse_material->id;
        $WT_item->warehouse_transfer_id = $bill->id;
        $WT_item->quantity = $bill_item->quantity;
        $WT_item->save();
        
        $new_bill_item = BillItem::model()->findByAttributes([
            'bill_id' => $bill->id,
            'warehouse_material_id' => $warehouse_material->id
        ]);
        
        $new_bill_item->value = $bill_item->value;
        $new_bill_item->vat = $bill_item->vat;
        $new_bill_item->no_vat_type = $bill_item->no_vat_type;
        /**
         * ovde dodati ako treba jos nesto da se prebaci od informacija
         * naziv ne treba prebacivati jer se automatski dobija od magacina
         */
        
        $bill_item->delete();
        $new_bill_item->save();
        
        
        $this->respondOK();
    }
    
    public function actionUpdateOrderOfBillItem($bill_id, $bill_item_id, $new_order)
    { 
        $params_for_after_work = [
            'bill_id' => $bill_id,
            'bill_items_ids_with_new_orders' => [
                [
                    'bill_item_id' => $bill_item_id,
                    'new_order' => $new_order
                ]
            ]
        ];
        
        Yii::app()->registerAfterWork('WorkBillItems', $params_for_after_work, false);
        
        $this->respondOK([]);
    }
}
