<?php

class IosGUI extends SIMAActiveRecordGUI
{
    public $year = null;

    public function columnLabels()
    {
        return array(
            'account_id' => Yii::t('AccountingModule.Account','Account'),
            'account' => Yii::t('AccountingModule.Account','Account'),
            'ios_date' => Yii::t('AccountingModule.Account','IosDate'),
            'partner_id' => Yii::t('AccountingModule.Account','Company'),
            'partner' => Yii::t('AccountingModule.Account','Company'),
            'partner_agreed_user' => Yii::t('AccountingModule.Account','PartnerAgreedUser'),
            'partner_agreed_user_id' => Yii::t('AccountingModule.Account','PartnerAgreedUser'),
            'partner_agreed' => Yii::t('AccountingModule.Account','PartnerAgreed'),
            'partner_agreed_timestamp' => Yii::t('AccountingModule.Account','PartnerAgreedTimestamp'),
            'partner_agreed_diff' => Yii::t('AccountingModule.Account','PartnerAgreedDiff'),
            'regenerate' => 'Regeneriši',
        ) + parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return 'IOS -Izvod otvorenih stavki';
    }

    public function columnDisplays($column)
    {
        $owner = $this->owner;
        $uniq = SIMAHtml::uniqid();
        
        switch ($column)
        {
//            case 'account_id': return $owner->account->DisplayName; 
            case 'regenerate': {
                 $button_parameters = [
                'id' => 'regenerate_ios_'.$uniq,
                'title' => 'Regeneriši',
                'onclick' => ['sima.accountingMisc.regenerateIos', $owner->id, '', 'regenerate_ios_'.$uniq, $owner->ios_year_id]    
                ];
                if(SIMAMisc::isVueComponentEnabled())
                {
                    $return = Yii::app()->controller->widget(SIMAButtonVue::class, $button_parameters, true);
                }            
                else 
                {
                    $return = Yii::app()->controller->widget('SIMAButton',[
                        'id' => $button_parameters['id'],
                        'action' => $button_parameters
                    ], true);
                }

                return $return;
            }
            default: return parent::columnDisplays($column);
        }
    }
    
//    public function columnDisplays($column)
//    {
//        $owner = $this->owner;
//        switch ($column)
//        {            
//            case 'type':
//                $types = SNAccount::$types;
//                return $types[$this->owner->type];
//            case 'change_password':
//                $return = Yii::app()->controller->widget('SIMAButton',[
//                    'action'=>[
//                        'title'=>'Resetuj',
//                        'tooltip'=>'Resetuj',
//                        'onclick'=>[
//                            'sima.externalMisc.snAccountChangePass', $owner->email, $owner->id
//                        ]
//                    ]
//                ], true);
//                return $return;
//            default: return parent::columnDisplays($column);
//        }
//    }
    
    public function modelForms()
    {
        $owner = $this->owner;
        $disabled = true;

        $ios_year = date("Y");
        if ($owner->isNewRecord)
        {
            $disabled = false;
            $ios_year = date("Y");
            if(!empty($owner->ios_year_id))
            {
                $ios_year = Year::model()->findByPkWithCheck($owner->ios_year_id)->year;
            }
        }
        else
        {
            $ios_year = date("Y", strtotime($owner->ios_date));
        }
        return array(
            'default'=>array(
                'type'=>'generic',
                'columns'=>array(
                    'ios_year_id' => 'hidden',
                    'ios_date'=>['datetimeField', 'disabled'=>$disabled],
                    'partner_id' => [
                        'relation',
                        'relName' => 'partner',
                        'disabled'=>$disabled
                    ],
                    'account_id' => [
                        'relation',
                        'relName' => 'account',
                        'filters'=>[
                            'scopes'=>'onlyLeafAccounts', // iskljucujuci sva konta koja nisu krajnja
                            'single_company_accounts'=>$owner->partner_id
                        ],
                        'dependent_on'=>[
                            'ios_date'=>[
                                'onValue'=>[
                                    'enable',
                                    [
                                        'condition', 
                                        'model_filter' => 'filter_scopes.byYear'
                                    ],
                                ],
                                'onEmpty' => 'disable'
                            ]
                        ],
//                        'filters'=>$account_filter,
                        'disabled'=>$disabled
                    ],
                    'partner_agreed' => 'status',
                    'partner_agreed_diff'=> 'numberField',
                )
            ),
        );
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            case 'single_company':
                return [
                    'columns'=>[
                        'ios_date','account','partner_agreed_diff', 'regenerate'
                    ]
                ];
            default: 
                return [
                    'columns'=>[
                        'partner','ios_date','account','partner_agreed_diff', 'regenerate'
                    ]
                ];
        }
    }
    
    public function afterValidateDependencyError(array $foreign_dependencies)
    {        
        $allowed_tables = [
            IosItem::model()->tableName().':ios_id'
        ];
        
        $parsed_foreign_dependencies = array_diff($foreign_dependencies, $allowed_tables);
        
        parent::afterValidateDependencyError($parsed_foreign_dependencies);
    }
    
    public function generatePdf()
    {
        $owner = $this->owner;
        $ios_report = new IosReport([
            'ios' => $owner
        ]);
        $temp_file = $ios_report->getPDF();
        $owner->file->appendTempFile(
            $temp_file->getFileName(), 
            $owner->DisplayName . '.pdf'
        );   
        return $temp_file;
    }
}