<?php

class AdvanceBillGUI extends BillGUI {

    public function columnLabels() {
        return array(
            'unreleased_amount' => 'Neiskorisceno',
            'percent_of_bill' => 'Procenat'            
        ) + parent::columnLabels();
    }

    public function columnDisplays($column) 
    {
        $owner = $this->owner;
        switch ($column) {

            

            default: return parent::columnDisplays($column);
        }
    }

    public function modelLabel($plural = false)
    {
        return Yii::t('AccountingModule.Bill',$plural?'Advance bills':'Advance bill');
    }

    public function modelForms()
    {
        $owner = $this->owner;
        
        $cost_type_id = [
            'relation', 
            'relName' => 'cost_type', 
            'add_button'=>[
                'action'=>'accounting/costTypes',
                'params' => [
                    'get_params' => [
                        'cost_or_income' => $owner->invoice?CostType::$TYPE_INCOME:CostType::$TYPE_COST
                    ]
                ]
            ],
            'filters' => [
                'cost_or_income' => $owner->invoice?CostType::$TYPE_INCOME:CostType::$TYPE_COST
            ],
            'dependent_on'=>[                        
                'income_date'=>[
                    'onValue' => ['trigger', 'func' => ['sima.accountingMisc.billCostTypeDependentOnBillIncomeDateInForm']],
                    'onEmpty' => ['disable']
                ]                        
            ]
        ];
        if ($owner->cost_type_mixed === true)
        {
            $cost_type_id['disabled'] = 'disabled';
        }
        
        $cost_location_id = ['relation', 'relName' => 'job_order'];
        if ($owner->cost_location_mixed === true)
        {
            $cost_location_id['disabled'] = 'disabled';
        }
        
        $vat_refusal = ['checkBox'];
        if (is_null($owner->vat_refusal))
        {
            $vat_refusal['disabled'] = 'disabled';
        }
        
        $invoicing_basis = 'hidden';
        $invoice_comment = 'hidden';
        if ($owner->invoice === true)
        {
            $invoicing_basis = 'textField';
            $invoice_comment = 'textArea';
        }
        
        $note_of_tax_exemption_id = 'hidden';
        if ($owner->invoice === true)
        {
            $note_of_tax_exemption_id = ['relation', 'relName' => 'note_of_tax_exemption', 'add_button' => true];
        }
        
        return array(
            'default' => array(
                'type' => 'generic',
                'columns' => array(
                    'bill_number' => 'textField',
                    'partner_id' => ['searchField', 'relName' => 'partner', 'add_button' => ['action'=>'simaAddressbook/index']],
                    'partner_bank_account_id'=>['searchField', 'relName' => 'partner_bank_account', 'dependent_on'=>[                        
                        'partner_id'=>[
                            'onValue'=>[
                                'enable',
                                ['condition', 'model_filter' => 'partner.ids']
                            ],
                            'onEmpty'=>'disable'
                        ]                        
                    ]],
                    'income_date'=> array('datetimeField'),
                    'payment_date'=> array('datetimeField'),
                    'cost_type_id' => $cost_type_id,
                    'cost_location_id' => $cost_location_id,
                    'bound_currency_id' => array('searchField','relName'=>'bound_currency'),
                    'lock_amounts' => 'checkBox',
                    'amount' => self::$locked_bill_values_form_type,
                    'base_amount0' => self::$locked_bill_values_form_type,
                    'base_amount10' => self::$locked_bill_values_form_type,
                    'base_amount20' => self::$locked_bill_values_form_type,
                    'vat10' => self::$locked_bill_values_form_type,
                    'vat20' => self::$locked_bill_values_form_type,
                    'responsible_id' => array('dropdown', 'empty'=>'Izaberi'),
                    'vat_refusal' => $vat_refusal,
                    'original'=>array('checkBox'),
                    'call_for_number' => 'textField',
                    'call_for_number_modul' => 'textField',
                    'invoicing_basis' => $invoicing_basis,
                    'note_of_tax_exemption_id' => $note_of_tax_exemption_id,
                    'invoice_comment' => $invoice_comment,
                    'comment'=>'textArea'
                )
            )
        );
    }

//    public function droplist($key, $relName='', $filter=null) {
//        switch ($key) {
//            case 'belongs': return FileTag::model()->getModelDropList();
//            default: return parent::droplist($key, $relName, $filter);
//        }
//    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            case 'inJobFinanse': return [
                'columns' => [
                    'bill_number', 'partner', 'income_date', 'amount', 'amount_in_currency', 
                    'unreleased_amount', 'unreleased_amount_in_currency','vat'
                ],
                'sums' => array('amount', 'amount_in_currency','base_amount',
                        'unreleased_amount','unreleased_amount_in_currency'),
                'sums_function'=>'sumFunc',
            ];
            case 'inBillProposals': return array(
                'columns'=>array( 
                    'bill_number', 'amount', 'amount_in_currency', 'unreleased_amount', 
                    'unreleased_amount_in_currency', 'unreleased_percent_of_amount', 'income_date',
                    'base_amount0', 'base_amount10', 'base_amount20', 'vat10', 'vat20'
                ),
                'sums' => array('amount', 'amount_in_currency','base_amount',
                        'unreleased_amount','unreleased_amount_in_currency'),
                'sums_function'=>'sumFunc',
                'order_by' => array('bill_number','amount','base_amount','unreleased_amount','income_date','payment_date'),
            );
            default:
                return array(
                    'columns'=>array(
                        'bill_number', 'partner', 'income_date', 'payment_date', 'amount', 'amount_in_currency', 
                        'base_amount', 'vat', 'unreleased_amount', 'unreleased_amount_in_currency',
                        'cost_location', 'comment', 'file.filing','belongs',
                        'file.account_document.account_order',
                        'file.account_document.account_order.date'
                    ),
                    'filters'=>array(
                        'year'
                    ),
                    'sums' => array('amount', 'amount_in_currency','base_amount',
                        'unreleased_amount','unreleased_amount_in_currency','vat'),
                    'sums_function'=>'sumFunc',
                    'order_by' => array('amount','base_amount','unreleased_amount','vat', 'income_date', 'payment_date'),
                );
        }
    }
    
    public function vueProp(string $prop, array $scopes = []):array
    {
        $owner = $this->owner;
        switch ($prop)
        {
            case 'remaining_vat_items': 
                $remaining_vat_items = $owner->remainingVatItemsArray();
                return [$remaining_vat_items, SIMAMisc::getModelTags($owner->bill_releases)];
            case 'remaining_vat_items_posible': 
                $bill = Bill::model()->findByPkWithCheck($scopes['bill_id']);
                $remaining_vat_items_posible = ConnectAccountingDocuments::BillToAdvanceBill($bill, $owner, false);
                return [$remaining_vat_items_posible, SIMAMisc::getModelTags($owner->bill_releases)];
            default: return parent::vueProp($prop, $scopes);
        }
    }
}