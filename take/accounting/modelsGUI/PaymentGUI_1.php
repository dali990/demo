<?php

class PaymentGUI extends SIMAActiveRecordGUI {

    public function columnLabels() {
        return array(
            'invoice' => 'Smer',
            'type' => 'Tip',
            'account_id' => 'Partnerov racun',
            'account' => 'Partnerov racun',
            'payment_date' => 'Datum plaćanja',
            'payment_type' => 'Tip plaćanja',
            'payment_type_id' => 'Tip plaćanja',
            'amount' => 'Iznos',
            'cost_location_id' => 'Mesto Troška',
            'cost_location' => 'Mesto Troška',
            'job_order' => 'Mesto Troška',
            'job_id' => 'Posao',
            'unreleased_amount' => 'Nerasknjiženo',
            'link' => '',
            'payment_code' => 'Šifra plaćanja',
            'payment_code_id' => 'Šifra plaćanja',
            'call_for_number' => 'Poziv na broj',
            'call_for_number_modul' => 'Poziv na broj|modul',
            'call_for_number_partner' => 'Partnerov poziv na broj',
            'call_for_number_modul_partner' => 'Partnerov poziv na broj|modul',
            'year' => 'Godina',
            'year_id' => 'Godina',
            'unreleased_percent_of_amount' => 'Procenat',
            'bank_statement_id' => 'Izvod iz banke',
            'bank_statement' => 'Izvod iz banke',
            'bank_statement.account' => Yii::t('AccountingModule.Payment', 'BankStatementAccount'),
            'recurring_bill' => RecurringBillGUI::modelLabel(),
            'recurring_bill_id' => RecurringBillGUI::modelLabel(),
            'bills' => BillGUI::modelLabel(true),
            'saldo_amount' => 'Saldo iznos'
        ) + parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return 'Plaćanje';
    }
    
    public function modelViews() {
        return array(
            'basicInfo' => array('class' => 'basic_info'),
            'title' => ['origin' => 'Payment'],
            'options' => ['origin' => 'Payment']
//            'tabTemplates'=>array('with'=>array('templated_file'),'style'=>'border: 1px solid;')  //.creator
        );
    }

    public function modelForms() 
    {
        return array(
            'default' => array(
                'type' => 'generic',
                'columns' => array(
                    'bank_statement_id' => 'hidden',
                    'invoice'=> 'dropdown',
                    'payment_type_id'=>['relation','relName'=>'payment_type','add_button'=>true],
                    'partner_id'=>array('searchField','relName'=>'partner'),
                    'account_id'=>['searchField', 'relName' => 'account', 'dependent_on'=>[                        
                        'partner_id'=>[
                            'onValue'=>[
                                'enable',
                                ['condition', 'model_filter' => 'partner.ids']
                            ],
                            'onEmpty'=>'disable'
                        ]                        
                    ]],
                    'amount'=>'numberField',
                    'comment'=>'textArea',
                    'cost_location_id'=>['relation','relName'=>'job_order'],
                    'payment_code_id' =>['relation','relName'=>'payment_code','add_button'=>true],
                    'call_for_number'=>'textField',
                    'call_for_number_modul'=>'textField',
                    'call_for_number_partner'=>'textField',
                    'call_for_number_modul_partner'=>'textField',
                    'payment_date' => 'hidden',
                    'recurring_bill_id' => ['relation', 'relName' => 'recurring_bill']
                )
            ),
            'pay' => array(
                'type' => 'generic',
                'columns' => array(
                    'invoice'=> 'hidden',
                    'payment_type_id'=>array('dropdown'),
                    'partner_id'=>array('searchField','relName'=>'partner'),
                    'account_id'=>['searchField', 'relName' => 'account', 'dependent_on'=>[                        
                        'partner_id'=>[
                            'onValue'=>[
                                'enable',
                                ['condition', 'model_filter' => 'partner.ids']
                            ],
                            'onEmpty'=>'disable'
                        ]                        
                    ]],
                    'amount'=>'numberField',
                    'payment_code_id' => array('dropdown'),
                    'call_for_number_partner'=>'textField',
                    'call_for_number_modul_partner'=>'textField',
                    'payment_date' => ['datetimeField', 'jquery_options' => ['minDate' => '+0']]
                )
            )
        );
    }

    public function droplist($key, $relName='', $filter=null) 
    {
        switch ($key) {
//			case 'job_id':				return ActiveJob::model()->getModelDropList();
            case 'payment_type_id': 	return PaymentType::model()->getModelDropList();
            case 'invoice': return array('0' => 'Isplata', '1' => 'Uplata');
            default: return parent::droplist($key, $relName, $filter);
        }
    }

    public function columnDisplays($column) {
        $owner = $this->owner;
        switch ($column) {
            case 'invoice': return ($owner->invoice) ? 'Uplata' : 'Isplata';
            case 'account_id': return isset($owner->bank_statement)?$owner->bank_statement->account->DisplayName:null;
            case 'partner_id': return CHtml::link($owner->partner->DisplayName, array('accounts/partner', 'id' => $owner->partner_id), array('target' => '_blank'));
// 			case 'partner_id': 		return $this->partner->DisplayName;
            case 'link': return CHtml::link('pogledaj', array('accounts/payment', 'id' => $owner->id));
            case 'payment_type': return (($owner->invoice) ? 'uplata-' : 'isplata-') . $owner->payment_type->DisplayName;
            case 'payment_type_id': return (($owner->invoice) ? 'uplata-' : 'isplata-') . $owner->payment_type->DisplayName;
            case 'unreleased_percent_of_amount': return SIMAHtml::money_format($owner->unreleased_percent_of_amount,'%');
            case 'bills': 
                $_bills_list = [];
                foreach ($owner->bills as $_bill)
                {
                    $_bills_list[]= $_bill->DisplayHTML;
                }
                return implode('</br>', $_bills_list);
            case 'addDocument': 
                $result = '';
                
                $action = $owner->addDocumentWidgetAction();
                if(!empty($action))
                {
                    if(SIMAMisc::isVueComponentEnabled())
                    {
                        $action['half'] = true;
                        $result = Yii::app()->controller->widget(SIMAButtonVue::class, $action, true);
                    }
                    else
                    {
                        $result = Yii::app()->controller->widget('SIMAButton', [
                            'class' => '_half',
                            'action' => $action
                        ], true);
                    }
                }
                
                return $result;
            case 'saldo_amount': return SIMAHtml::number_format($owner->amount * ($owner->invoice?1:-1));
            default: return parent::columnDisplays($column);
        }
    }

    public function guiTableSettings($type)
    {
        switch ($type)
        {
            case 'KPR':
                return array(
                    'columns'=>array(
                        'file.account_document.account_order.date', 'bill_number', 'income_date', 'partner', 'pib_jmbg',
                        'KPR_8', 'KPR_9', 'KPR_10', 'KPR_11', 'empty', 'KPR_13', 'KPR_14', 'KPR_15','payment_code'
                    ),
                    'sums' => array('amount','unreleased_amount'),
                    'order_by' => array('amount','unreleased_amount'),
                );
            case 'inBankStatement':
                return array(
                    'columns'=>array(
                        'partner'=>array(
                            'edit'=>'relation'
                        ), 
                        'payment_type_id'=>array('edit'),
                        'amount'=>array('edit'=>'text'), 
                        'unreleased_amount', 
                        'comment'=>array('edit'), 
                        'job_order',
                        'payment_code'=>array('edit'),
                        'call_for_number'=>array('edit'),
                        'call_for_number_partner'=>array('edit')
                    ),
                    'sums' => array('amount','unreleased_amount'),
                    'order_by' => array('unreleased_amount'),
                );
            case 'inPaymentProposals':
                return array(
                    'columns'=>array(
                        'amount', 'unreleased_amount', 'unreleased_percent_of_amount','payment_date'
                    ),
                    'sums' => array('amount','unreleased_amount'),
                    'order_by' => array('amount','unreleased_amount'),
                );
            case 'inPartner': 
                return array(
                    'columns'=>array(
                        'payment_date',
                        'amount' => ['min_width' => '120'], 
                        'saldo_amount' => ['min_width' => '120'], 
                        'invoice',
                        'payment_type' => ['min_width' => '120'],
                        'bills' => ['min_width' => '200'],
                        'recurring_bill' => ['min_width' => '300'],
                        'comment' => ['min_width' => '300'], 
                        'account' => ['min_width' => '300'], 
                        'bank_statement' => ['min_width' => '300'], 
                        
                    ),
                    'sums' => array('amount','saldo_amount'),
                    'sums_function' => 'sumFuncInPartner',
                    'order_by' => array('amount','unreleased_amount'),
                );
            default: 
                return array(
                    'columns'=>array(
                        'partner', 
                        'account', 
                        'payment_date', 'payment_type_id', 'invoice', 'amount', 
                        'unreleased_amount', 'comment', 'job_order',
                        'payment_code','call_for_number','call_for_number_partner',
                        'bank_statement.account', 
                        'recurring_bill'
                    ),
                    'filters'=>array(
                        'year'
                    ),
                    'sums' => array('amount','unreleased_amount'),
                    'order_by' => array('amount','unreleased_amount'),
                );
        }
    }
    
    public function sumFuncInPartner($criteria)
    {
        $_criteria = clone $criteria;
        
        $_criteria->select = 'sum(amount) as amount,'
                . 'sum(case when invoice then amount else -amount end) as saldo_amount';
        $result_payment = Payment::model()->find($_criteria);

        return $result_payment;
    }
    
    public function vuePropValue($prop)
    {
        $owner = $this->owner;
        switch ($prop)
        {
            case 'model_options_data': 
                return array_merge(parent::vuePropValue($prop), [
                    'addDocument' => $owner->addDocumentWidgetAction()
                ]);
            default: return parent::vuePropValue($prop);
        }
    }
}