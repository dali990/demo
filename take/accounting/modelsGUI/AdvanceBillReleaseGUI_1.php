<?php

class AdvanceBillReleaseGUI extends SIMAActiveRecordGUI
{
    public function columnLabels()
    {
        return [
            'advance_bill' => 'Avansni racun',
            'advance_bill_id' => 'Avansni racun',
            'percent_of_bill' => 'Procenat',
            'percent_of_advance_bill' => 'Procenat',
            'bill_id' => 'Račun',
            'bill' => 'Račun',
            'amount' => 'Iznos',
            'base_amount' => 'Osnovica',
            'vat_amount' => 'PDV',
            'vat_rate' => 'Stopa PDV',
            'internal_vat' => 'Interni PDV'
        ];
    }
    
    public function modelLabel($plural = false)
    {
        return 'Rasknjizeno';
    }
    
    public function columnDisplays($column)
    {
        $owner = $this->owner;
        switch ($column)
        {
            case 'percent_of_bill': 
                if (floatval($owner->bill->amount)==0)
                {
                    return '? %';
                }
                else
                {
                    return SIMAHtml::money_format((floatval($owner->amount)*100.0/floatval($owner->bill->amount)),'%');
                }
            case 'percent_of_advance_bill': return SIMAHtml::money_format((floatval($owner->amount)*100.0/floatval($owner->advance_bill->amount)),'%');
            default: return parent::columnDisplays($column);
        }
    }
    
    public function modelForms()
    {
        return array(
            'default' => array(
                'type' => 'generic',
                'columns' => array(
                    'bill_id' => ['relation','relName' => 'bill','disabled'=>'disabled'] ,
                    'advance_bill_id' => ['relation','relName' => 'advance_bill','disabled'=>'disabled'] ,
                    'internal_vat' => 'checkBox',
                    'vat_rate' => 'dropdown',
                    'amount' => 'numberField',
                    'base_amount' => 'numberField',
                    'vat_amount' => 'numberField',
                )
            )
        );
    }
    
    public function droplist($key, $relName='', $filter=null)
    {
        $owner = $this->owner;
        switch ($key)
        {
            case 'vat_rate': return ['0' => '0%', '10' => '10%', '20' => '20%'];
            default: return parent::droplist($key, $relName, $filter);
        }
    }

    public function guiTableSettings($type)
    {
        switch ($type)
        {
            case 'inBill':return [
                'columns' => [
                    'advance_bill','amount','percent_of_bill',
                    'base_amount',
                    'vat_amount',
                    'advance_bill.comment'
                ]
            ];
            case 'inAdvanceBill':return [
                'columns' => [
                    'bill','amount','percent_of_advance_bill',
                    'base_amount',
                    'vat_amount',
                    'advance_bill.comment'
                ]
            ];
            default: return [
                'columns' => [
                    'bill','advance_bill','amount'
                ]
            ];
        }
        
    }
}