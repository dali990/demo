<?php

class BankGuaranteeGUI extends SIMAActiveRecordGUI
{
    
    public function columnLabels()
    {
        return array(
            'request_number' => 'Broj zahteva',
            'bank' => 'Banka',
            'bank_id' => 'Banka',            
            'order_by' => 'Naručioc',
            'order_by_partner' => 'Naručioc',
            'expire_date' => 'Rok važenja',
            'number' => 'Broj garancije',
            'amount' => 'Iznos',
            'type' => 'Tip',
            'description' => 'Napomena'
        )+parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return 'Bankarska garancija';
    }
    
    public function columnDisplays($column)
    {
        $owner = $this->owner;
        switch ($column)
        {
            case 'type' :
                switch ($owner->type)
                {
                    case 'LETTER_OF_INTENT' : return 'Pismo o namerama';
                    case 'BANK_GUARANTEE' : return 'Bankarska garancija';
                    default: return '';
                }
            default: return parent::columnDisplays($column);
        }
    }
    
    public function modelForms()
    {
        return array(
            'default' => array(
                'type' => 'generic',
                'columns' => array(
                    'request_number' => 'textField',
                    'bank_id' => array('relation', 'relName'=>'bank'),
                    'order_by' => array('relation', 'relName'=>'order_by_partner'),
                    'expire_date' => 'datetimeField',
                    'number' => 'textField',
                    'amount' => 'textField',
                    'type'=>array('dropdown', 'empty'=>'Unesite tip'),
                    'description'=>'textArea'
                )
            )
        );
    }

    public function droplist($key, $relName='', $filter=null)
    {
        switch ($key)
        {            
            case 'type': return array('LETTER_OF_INTENT' => 'Pismo o namerama', 'BANK_GUARANTEE' => 'Bankarska garancija');
            default: return parent::droplist($key, $relName, $filter);
        }
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default: return array(
                'columns'=>array(
                    'request_number', 'bank', 'order_by_partner', 'expire_date', 'number', 'amount', 'type', 'description'
                )
            );
        }
    }
    
}
