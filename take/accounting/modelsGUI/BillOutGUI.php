<?php

class BillOutGUI extends BillGUI
{
    public function columnLabels()
    {
        return array(
            'warehouse_dispatches' => 'Otpremnice',
            'recurring_bill_id' => 'Stalna naplata',
            'recurring_bill' => 'Stalna naplata',
            'cost_type' => Yii::t('AccountingModule.CostType','IncomeType'),
            'cost_type_id' => Yii::t('AccountingModule.CostType','IncomeType')
        ) + parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return Yii::t('AccountingModule.Bill', $plural?'BillsOut':'BillOut');
    }
    
    public function modelViews()
    {
        return array_merge([
            'inConcreteOrder',// => array('style' => 'border: 1px solid;'),
            
//            'full_info',// => array('params_function' => 'fullInfoParams'),//ISTI JE KAO ZA SVE
            'bill_items' => ['params_function' => 'billItemsParams', 'with' => ['file']],
//            'inFile' => array('params_function' => 'BillInFileParams'),
//            'warehouseTransfersListForm', //=>array('origin'=>'Bill'),
//            'warehouseTransfersList'//=>array('origin'=>'Bill')
//            'tabTemplates'=>array('with'=>array('templated_file'),'style'=>'border: 1px solid;')  //.creator
        
        ],parent::modelViews());
    }
        
    public function billItemsParams()
    {
        $owner = $this->owner;
//        $warehouse_items = [];
//        foreach ($owner->warehouse_bill_items as $w_bill_item)
//        {
//           
//        }
        return [
            'warehouse_transfers' => $owner->warehouse_transfers,
            'service_bi' => $owner->services_bill_items,
        ];
    }
    
    public function modelForms()
    {
        $owner = $this->owner;
        
        $vat_refusal = ['checkBox'];
        if (is_null($owner->vat_refusal))
        {
            $vat_refusal['disabled'] = 'disabled';
        }
        
        
        $cost_type_id = [
            'relation', 
            'relName' => 'cost_type', 
            'add_button'=>[
                'action'=>'accounting/costTypes',
                'params' => [
                    'get_params' => [
                        'cost_or_income' => CostType::$TYPE_INCOME
                    ]
                ]
            ],
            'filters' => [
                'cost_or_income' => CostType::$TYPE_INCOME
            ],
            'dependent_on'=>[                        
                'income_date'=>[
                    'onValue' => ['trigger', 'func' => ['sima.accountingMisc.billCostTypeDependentOnBillIncomeDateInForm']],
                    'onEmpty' => ['disable']
                ]                        
            ]
        ];
        if ($owner->cost_type_mixed === true)
        {
            $cost_type_id['disabled'] = 'disabled';
        }
        
        $cost_location_id = ['relation', 'relName' => 'job_order'];
        if ($owner->cost_location_mixed === true)
        {
            $cost_location_id['disabled'] = 'disabled';
        }
        
        return array(
            'default' => array(
                'type' => 'generic',
                'columns' => array(
                    'bill_number' => 'textField',  
//                    'belongs'=>'belongs',
                    'partner_id' => ['searchField', 'relName' => 'partner', 'add_button' => ['action'=>'simaAddressbook/index']],
                    'income_date'=> array('datetimeField'),
                    'issue_location_id'=> ['relation', 'relName' => 'issue_location', 'add_button' => true],
                    'cost_type_id' => $cost_type_id,
                    'cost_location_id' => $cost_location_id,
                    'traffic_date'=> array('datetimeField','placeholder' => $this->trafficDatePlaceholder()),
                    'traffic_location_id'=> ['relation', 'relName' => 'traffic_location', 'add_button' => true],
                    'payment_date'=> ['datetimeField', 'placeholder' => Yii::t('AccountingModule.Bill', 'Leave empty if same as income_date')],
                    'bound_currency_id' => array('searchField','relName'=>'bound_currency'),
                    'lock_amounts' => 'checkBox',
                    'amount' => self::$locked_bill_values_form_type,
                    'base_amount0' => self::$locked_bill_values_form_type,
                    'base_amount10' => self::$locked_bill_values_form_type,
                    'base_amount20' => self::$locked_bill_values_form_type,
                    'vat10' => self::$locked_bill_values_form_type,
                    'vat20' => self::$locked_bill_values_form_type,
                    'interest' => self::$locked_bill_values_form_type,
                    'rounding' => self::$locked_bill_values_form_type,
                    'responsible_id' => array('dropdown', 'empty'=>'Izaberi'),
                    'vat_refusal' => $vat_refusal,
                    'vat_in_diferent_month_id' => ['relation', 'relName' => 'vat_in_diferent_month'],
                    'original'=>array('checkBox'),
                    'recurring_bill_id' => ['searchField', 'relName' => 'recurring_bill'],
                    'call_for_number' => 'textField',
//                    'warehouse_dispatches'=>array(
//                        'list',
//                        'add'=>array(
//                            'new',
//                            'search'=>array(
//                                'filter'=>array(
//                                    'bill_id'=>null
//                                )
//                            )
//                        ),
//                        'list_view'=>'warehouseTransfersListForm'   
//                    ),
                    'invoicing_basis' => 'textField',
                    'note_of_tax_exemption_id' => ['relation', 'relName' => 'note_of_tax_exemption', 'add_button' => true],
                    'invoice_comment' => 'textArea',
                    'comment'=>'textArea'
                )
            )
        );
    }
    
    public function guiTableSettings($type)
    {
        $owner = $this->owner;
        switch ($type)
        {
            
            case 'income_in_job': return [
                'columns'=>array(
                    'bill_number', 'file.filing', 'partner', 'traffic_date', 'traffic_location', 'issue_location', 'income_date', 'payment_date', 'amount', 'unreleased_amount',
                    'comment', 'note_of_tax_exemption'
                ),
                'sums' => array('amount','base_amount','unreleased_amount'),
                'order_by' => array('bill_number','amount','base_amount','unreleased_amount'),
            ];
            default: return parent::guiTableSettings($type);
        }
    }
}