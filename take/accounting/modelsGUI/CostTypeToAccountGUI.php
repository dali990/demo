<?php

class CostTypeToAccountGUI extends SIMAActiveRecordGUI
{
    public function columnLabels()
    {
        return array(
            'cost_type' => 'Vrsta troška',
            'cost_type_id' => 'Vrsta troška',
            'account' => 'Konto',
            'account_id' => 'Konto',
            'year_id' => 'Godina',
            'year' => 'Godina',
            'booked_bills' => 'Proknjizeni računi',
            'cost_type_name' => 'Vrsta troska/prihoda',
            'cost_type_book_by_cost_location' => Yii::t('AccountingModule.CostType','book_by_cost_location'),
        ) + parent::columnLabels();
    }

    public function modelLabel($plural = false)
    {
        return 'Vrste troška po kontima';
    }   
    
    public function columnDisplays($column)
    {
        $owner = $this->owner;
        switch ($column)
        {
            case 'booked_bills':
                return 'MNOGOOOO';
            default: return parent::columnDisplays($column);
        }
        
    }
    
    public function modelForms()
    {
        $owner = $this->owner;

        $filter = [];
        $account_add_button_params = [];
        
        if (isset($owner->year_id))
        {           
            $filter = [
                'year' => [
                    'ids' => $owner->year_id
                ],
                'display_scopes' => [
                    'withDCCurrents' => [$owner->year_id, ['hide_zero_accounts' => false]],
                    'byName'
                ]
            ];
            $account_add_button_params = [
                'init_data' => [
                    'Account' => [
                        'year_id' => [
                            'disabled',
                            'init' => $owner->year_id
                        ]
                    ]
                ]
            ];
        }
        
        return array(
            'default' => array(
                'type' => 'generic',
                'columns' => array(
                    'cost_type_name' => 'textField',
                    'cost_type_book_by_cost_location' => 'checkBox',
//                    'cost_type_id' => ['searchField','relName'=>'cost_type', 'disabled' => 'disabled'],
                    'year_id' => ['searchField','relName'=>'year', 'disabled' => 'disabled'],
                    'account_id' => [
                        'searchField', 
                        'relName' => 'account', 
                        'dependent_on' => [
                            'year_id' => [
                                'onValue' => [
                                    'enable',
                                    ['condition', 'model_filter' => 'year.ids']
                                ],
                                'onEmpty' => [
                                    'disable',
                                ]
                            ]
                        ], 
                        'filters' => $filter,
                        'add_button' => [
                            'filter' => $filter,
                            'params' => [
                                'add_button' => $account_add_button_params
                            ]
                        ]
                    ]
                ),
            )
        );
    }

    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default:
                return [
                    'columns' => [
                        'cost_type.name', 
                        'account' => ['min_width' => 300], 
//                        'booked_bills',
                        'cost_type.book_by_cost_location',
                        'cost_type.description', 
                    ],
                    'order_by'=>['cost_type']
                ];
        }
    }
}