<?php

class AccountLayoutGUI extends SIMAActiveRecordGUI
{

    public function columnLabels()
    {
        return array(
            'code' => 'Kod',
            'parent' => 'Nadkonto',            
            'law' => 'Zakon',
            'law_id' => 'Zakon'
        ) + parent::columnLabels();
    }

    public function modelLabel($plural = false)
    {
        return 'Kontni okvir';
    }
    
    public function columnDisplays($column)
    {
        $owner = $this->owner;
        switch ($column)
        {
            case 'parent': return isset($owner->parent) ? $owner->parent->DisplayName : '';
            default: return parent::columnDisplays($column);
        }
    }

    public function modelViews()
    {
        return array(
            'default'//=>array('style'=>'border: 1px solid;'),
//            'tabTemplates'=>array('with'=>array('templated_file'),'style'=>'border: 1px solid;')  //.creator
        );
    }

    public function modelForms()
    {
        $owner = $this->owner;
        return array(
            'default' => array(
                'type' => 'generic',
                'columns' => array(
                    'code' => 'textField',
                    'name' => 'textField',                                       
                    'description' => 'textArea',
                    'law_id' => array('relation', 'relName' => 'law', 'disabled'=>true)
                )
            )
        );
    }

    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default:
                return [
                    'columns' => [ 'code', 'name', 'description']
                ];
        }
    }
}