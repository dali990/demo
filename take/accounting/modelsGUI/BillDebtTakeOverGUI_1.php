<?php

class BillDebtTakeOverGUI extends SIMAActiveRecordGUI
{
    public function columnLabels()
    {
        return [
            'debt_value' => Yii::t('AccountingModule.DebtTakeOver','DebtValue'),
            'take_over_value' => Yii::t('AccountingModule.DebtTakeOver','TakeOverValue'),
        ];
    }
    
    public function modelLabel($plural = false)
    {
        return Yii::t('AccountingModule.DebtTakeOver',$plural?'BillDebtTakeOvers':'BillDebtTakeOver');
    }
    
//    public function columnDisplays($column)
//    {
//        $owner = $this->owner;
//        switch ($column)
//        {
//            default: return parent::columnDisplays($column);
//        }
//    }
    
    public function modelForms()
    {
        return array(
            'default' => array(
                'type' => 'generic',
                'columns' => array(
                    'debt_value' => 'numberField',
                    'take_over_value' => 'numberField',
                    'bill_id' => 'hidden',
                    'debt_take_over_id' => 'hidden'
                )
            )
        );
    }

//    public function guiTableSettings($type)
//    {
//        switch ($type)
//        {
//            case 'inBill':
//            case 'inAdvanceBill':
//                return [
//                    'columns' => [
//                        'payment',
//                        'amount',
//                        'amount_in_currency',
//                        'exchange_rate_difference',
//                        'other_difference',
//                        'percent_of_bill',
//                        'payment.comment'
//                    ]
//                ];
//            case 'inAdvance':return [
//                'columns' => [
//                    'bill','bill.comment'
//                ]
//            ];
//            case 'inPayment':return [
//                'columns' => [
//                    'bill','amount','percent_of_payment','exchange_rate_difference','payment.comment'
//                ]
//            ];
//            default: return [
//                'columns' => [
//                    'bill','payment','amount','exchange_rate_difference'
//                ]
//            ];
//        }
//        
//    }
}