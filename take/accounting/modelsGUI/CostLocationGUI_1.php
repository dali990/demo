<?php

class CostLocationGUI extends SIMAActiveRecordDisplayGUI
{

    public static function generateName()
    {
        return Yii::app()->configManager->get('accounting.cost_location_name_generator');
    }
    
    public function columnLabels()
    {
        return array(
            'prefix' => 'Prefix',
            'work_name' => 'Pun naziv',
            'number' => 'Broj',
            'comment' => 'Komentar',
            'location_id' => 'Lokacija',
            'location' => 'Lokacija',
            'year_id' => 'Godina',
            'year' => 'Godina',
            'for_analytics' => Yii::t('AccountingModule.CostLocation','For analytics'),
            'parent_id' => Yii::t('AccountingModule.CostLocation','ParentID'),
            'virtual_parents' => Yii::t('AccountingModule.CostLocation','VirtualParents'),
        ) + parent::columnLabels();
    }

    public function modelLabel($plural = false)
    {
        return Yii::t('AccountingModule.CostLocation',$plural?'CostLocations':'CostLocation');
    }
    
    public function afterValidateDependencyError(array $foreign_dependencies)
    {
        $allowed_tables = [
            Theme::model()->tableName().':cost_location_id',
            Warehouse::model()->tableName().':cost_location_id',
        ];

        $parsed_foreign_dependencies = array_diff($foreign_dependencies, $allowed_tables);
        
        parent::afterValidateDependencyError($parsed_foreign_dependencies);
    }

    
    public function vueProp(string $prop, array $scopes):array
    {
        $owner = $this->owner;
        switch ($prop)
        {
            case 'virtual_parents':
                $prop_value = [
                    'TYPE' => 'MODEL_ARRAY',
                    'ARRAY' => SIMAMisc::getVueModelTags($owner->virtual_parents(['scopes' => $scopes]))
                ];
                $update_tags = SIMAMisc::getModelTags($owner->virtual_parents_base_relation);// bill_items je base_relation
                return [$prop_value,$update_tags];

            default: return parent::vueProp($prop, $scopes);
        }
        
    }
    
    public function vuePropValue($prop)
    {
        $owner = $this->owner;
        switch ($prop)
        {
            case 'partners':
                $_array = [];
                foreach ($owner->partners as $partner)
                {
                    $_array[] = [
                        'DisplayHTML' => $partner->DisplayHTML,
                        'id' => $partner->id,
                        'income' => $partner->income,
                        'income_notpayed' => $partner->income_notpayed,
                        'income_advance' => $partner->income_advance,
                        'cost' => $partner->cost_services,
                        'cost_notpayed' => $partner->cost_notpayed,
                        'cost_advance' => $partner->cost_advance
                    ];
                }
                return $_array;
            default: return parent::vuePropValue($prop);
        }
    }

    public function modelViews()
    {
        return array_merge([
            'info'=>[
                'params_function'=>'infoViewParams',
                'class' => 'sima-layout-panel'
            ],
            'basic_info',
            'full_info',
        ],parent::modelViews());
    }
    
    public function modelForms()
    {
        $virtual_parents = [
            'list',
            'model'=>'CostLocation',
            'relName' => 'virtual_parents', 
            'multiselect'=>true,
            'filter' => [
                'scopes' => ['onlyVirtual']
            ]
        ];
        
        if (self::generateName())
        {
            $columns = [
                'prefix' => 'textField',
                'work_name' => 'textArea',
//                'parent_id' => ['relation', 'relName' => 'parent'],
                'number' => array('textField', 'placeholder' => 'Upišite 0 za automatsko dodeljivanje'),
                'year_id' => array('searchField', 'relName' => 'year'),
                'location_id' => array('searchField', 'relName' => 'location'),
                'for_analytics' => 'checkBox',
                'virtual_parents' => $virtual_parents,
                'comment' => 'textArea'
            ];
        }
        else
        {
            $columns = [
                'name' => 'textField',
                'work_name' => 'textArea',
//                'parent_id' => ['relation', 'relName' => 'parent'],
//                'number' => array('textField', 'placeholder' => 'Upišite 0 za automatsko dodeljivanje'),
                'year_id' => array('searchField', 'relName' => 'year'),
                'location_id' => array('searchField', 'relName' => 'location'),
                'for_analytics' => 'checkBox',
                'virtual_parents' => $virtual_parents,
                'comment' => 'textArea'
            ];
        }
        return array(
            'default' => array(
                'type' => 'generic',
                'columns' => $columns
            )
        );
    }

    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default: return array(
                'columns' => array(
                    'name', 
                    'work_name', 
                    'year', 
                    'location', 
                    'for_analytics', 
                    'comment',
//                    'display_name_full',
//                    'display_name_path',
                )
            );
        }
    }
    
    public function infoViewParams()
    {
        $owner = $this->owner;
        $warehouse = WarehouseProductionLocation::model()->findByAttributes([
            'cost_location_id' => $owner->id
        ]);
        
        return array(
            'model' => $owner,
            'warehouse' => $warehouse,
        );
    }

}
