<?php

class POPDVItemGUI extends SIMAActiveRecordGUI
{

    public function columnLabels()
    {
        return array(
            'code' => 'Kod/Konto',
//            'parent' => 'Nadkonto',            
//            'debit_start' => 'Duguje(Početno)',
//            'credit_start' => 'Potražuje(Početno)',
//            'debit_current' => 'Duguje',
//            'credit_current' => 'Potražuje',
//            'debit_sum' => 'Duguje(Zbir)',
//            'credit_sum' => 'Potražuje(Zbir)',
//            'year' => 'Godina',
//            'year_id' => 'Godina',            
//            'cost_location' => 'Mesto troška',
//            'name_change' => 'Promena imena'
        ) + parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return 'POPDV stavka';
    }
    
//    public function vueProp(string $prop, array $scopes):array
//    {
//        $owner = $this->owner;
//        switch ($prop)
//        {
//            case 'saldo': return [$owner->saldo, SIMAMisc::getModelTags($owner->account_transactions)];
//            default: return parent::vueProp($prop, $scopes);
//        }
//    }
    
//    public function vuePropValue($prop)
//    {
//        $owner = $this->owner;
//
//        switch ($prop)
//        {
//            case 'model_options_data': 
//                return array_merge(parent::vuePropValue($prop), [
//                    'account_open_with_date' => SIMAHtmlButtons::AccountOpenWithDateWidgetAction($owner)
//                ]);
//                
//            default: return parent::vuePropValue($prop);
//        }
//    }
    
//    public function afterValidateDependencyError(array $foreign_dependencies)
//    {
//        $allowed_tables = [
//            AccountTransaction::model()->tableName().':account_id'
//        ];
//
//        $parsed_foreign_dependencies = array_diff($foreign_dependencies, $allowed_tables);
//        
//        parent::afterValidateDependencyError($parsed_foreign_dependencies);
//    }

//    public function columnDisplays($column)
//    {
//        $owner = $this->owner;
//        switch ($column)
//        {
//            default: return parent::columnDisplays($column);
//        }
//    }

    public function modelViews()
    {
        return [
//            'infoBox'=> ['params_function'=>'infoBoxParams']
        ];
    }
    

    public function modelForms()
    {
        return array(
            'default' => array(
                'type' => 'generic',
                'columns' => array(
                    'account_document_id' => ['relation', 'relName' => 'account_document', 'disabled' => 'disabled'],
                    'param_code' => ['textField', 'disabled' => 'disabled'],                    
                    'param_value' => 'numberField',
                    'note' => 'textArea'
                )
            )
        );
    }
    
    
    
    
    
    
}