<?php

class AdvanceGUI extends PaymentGUI
{

    public function columnLabels()
    {
        return array(
            'advance_bill' => 'Avansni račun',
            'add_advance_bill_to_account_order' => 'Dodaj na nalog'
        ) + parent::columnLabels();
    }
    
    public function modelViews() {
        return array(
            'basicInfo' => array('class' => 'basic_info'),            
//            'tabTemplates'=>array('with'=>array('templated_file'),'style'=>'border: 1px solid;')  //.creator
        ) + parent::modelViews();
    }
    
    public function columnDisplays($column)
    {
        $owner = $this->owner;
        switch ($column)
        {
            case 'add_advance_bill_to_account_order': 
                
                if(is_null($owner->bank_statement) || empty($owner->bank_statement->file->account_document))
                {
                    return 'Ne postoji '.AccountOrder::model()->modelLabel();
                }
                $ao_id = $owner->bank_statement->file->account_document->account_order_id;
                $advance_bill_ids = [];
                foreach ($owner->advance_releases as $rel)
                {
                    $advance_bill_ids[] = $rel->bill_id;
                }
                if (count($advance_bill_ids)!=1)
                {
                    return 'Neodgovarajuc broj avansnih racuna - '.count($advance_bill_ids);
                }
                else
                {
                    $title = 'Dodaj na nalog '.$owner->bank_statement->file->account_document->account_order->DisplayName;
//                    return Yii::app()->controller->widget('SIMAButton', [
//                        'action'=>[ 
//                            'title'=>$title,
//                            'tooltip'=>$title,
//                            'onclick'=>['sima.accounting.addAccountDocument', $advance_bill_ids[0], $ao_id],
//                        ]
//                    ],true);
                    return Yii::app()->controller->widget(SIMAButtonVue::class, [
                        'title'=>$title,
                        'tooltip'=>$title,
                        'onclick'=>['sima.accounting.addAccountDocument', $advance_bill_ids[0], $ao_id],
                    ],true);
                }
                
            default: return parent::columnDisplays($column);
        }
        
    }

    public function guiTableSettings($type)
    {
        $ret = array();
        switch ($type)
        {
            case 'booking_advance_bill':
                return [
                    'columns' => [
                        'payment_date',
                        'amount',
                        'bank_statement',
                        'add_advance_bill_to_account_order'
                    ]
                ];
            case 'inAdvanceProposals':
                return array(
                    'columns'=>array(
                        'amount', 'unreleased_amount', 'unreleased_percent_of_amount','payment_date'
                    ),
                    'sums' => array('amount','unreleased_amount'),
                    'order_by' => array('amount','unreleased_amount'),
                );
            default: $ret = array(
                    'columns'=>  array(
                        'partner',
                        'payment_date',
                        'amount',
                        'unreleased_amount',
                        'advance_bill',
                        'cost_location_id',
                        'comment'
                    ),
                    'filters'=>array(
                        'year'
                    ),
                    'sums' => array('amount','unreleased_amount'),
                    'order_by' => array('amount','unreleased_amount'),
                );
                break;
        }
        
        return $ret;
    }

}
