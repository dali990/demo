<?php

class AccountGUI extends SIMAActiveRecordGUI
{

    public function columnLabels()
    {
        return array(
            'code' => 'Kod/Konto',
            'parent' => 'Nadkonto',            
            'debit_start' => 'Duguje(Početno)',
            'credit_start' => 'Potražuje(Početno)',
            'debit_current' => 'Duguje',
            'credit_current' => 'Potražuje',
            'debit_sum' => 'Duguje(Zbir)',
            'credit_sum' => 'Potražuje(Zbir)',
            'year' => 'Godina',
            'year_id' => 'Godina',            
            'cost_location' => 'Mesto troška',
            'name_change' => 'Promena imena'
        ) + parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return 'Konto';
    }
    
    public function vueProp(string $prop, array $scopes):array
    {
        $owner = $this->owner;
        switch ($prop)
        {
            case 'saldo': return [$owner->saldo, SIMAMisc::getModelTags($owner->account_transactions)];
            default: return parent::vueProp($prop, $scopes);
        }
    }
    
    public function afterValidateDependencyError(array $foreign_dependencies)
    {
        $allowed_tables = [
            AccountTransaction::model()->tableName().':account_id'
        ];

        $parsed_foreign_dependencies = array_diff($foreign_dependencies, $allowed_tables);
        
        parent::afterValidateDependencyError($parsed_foreign_dependencies);
    }

    public function columnDisplays($column)
    {
        $owner = $this->owner;
        switch ($column)
        {
            case 'name_change': 
                $_new_name = $owner->recalcName();
                if (!empty($_new_name) && $owner->name != $_new_name )
                {
                    $result = '<span style="color:red;">PROMENA</span>: '.$owner->name. ' ==> '.$_new_name;
                    
                    if(SIMAMisc::isVueComponentEnabled())
                    {
                        $result .= Yii::app()->controller->widget(SIMAButtonVue::class,[
                            'title' => 'promeni',
                            'onclick'=>['sima.ajax.get', 'accounting/accounting/changeAccountName',['get_params' => ['account_id'=> $owner->id]]],
                        ],true);
                    }
                    else
                    {
                        $result .= Yii::app()->controller->widget('SIMAButton',[
                            'action' => [
                                'title' => 'promeni',
                                'onclick'=>['sima.ajax.get', 'accounting/accounting/changeAccountName',['get_params' => ['account_id'=> $owner->id]]],
                            ]
                        ],true);
                    }
                    
                    return $result;
                }
                else
                {
                    return $owner->name;
                }
            default: return parent::columnDisplays($column);
        }
    }
    
    public function getInfoBoxView()
    {
        return 'infoBox';
    }
    
    public function modelViews()
    {
        return [
            'infoBox'=> ['params_function'=>'infoBoxParams']
        ];
    }
    
    public function infoBoxParams()
    {
        $parents = [];
        $owner = $this->owner;
        $analitic_size = intval(Yii::app()->params['accounting_account_analitic_size']);
        if (strlen($owner->code) === $analitic_size)
        {
            $parents[] = [
                $owner->code,
                $owner->name . (($owner->description != '')?'(' . $owner->description . ')':'')
            ];
        }
        while (isset($owner->parent))
        {
            $owner = $owner->parent;
            if (empty($owner->name))
            {
                $_name = '<span class="sima-font-style-italic">'.$owner->recalcName().'</span>';
            }
            else
            {
                $_name = $owner->name;
            }
            $parents[] = [
                $owner->code,
                $_name . (($owner->description != '')?'(' . $owner->description . ')':'')
            ];
        }
        
        return [
            'parents' => $parents
        ];
    }

    public function modelForms()
    {
        $owner = $this->owner;
        $disabled = true;
        if ($owner->getIsNewRecord())
        {
            $disabled = false;
        }
        return array(
            'default' => array(
                'type' => 'generic',
                'columns' => array(
                    'code' => ['textField', 'disabled'=>$disabled],
                    'name' => 'textField',                    
                    'description' => 'textArea',
                    'year_id' => array('searchField', 'relName' => 'year', 'disabled'=>$disabled)                    
                )
            )
        );
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            case 'byYear': return [
                'columns' => array(
                    'code', 
                    'cost_location',
                    'name',
                    'debit_start',
                    'credit_start',
                    'debit_current',
                    'credit_current',
                    'debit_sum', 
                    'credit_sum',                    
                    'saldo',                    
                    'description',
                    'name_change'
                ),
                'sums' => [
                    'debit_start',
                    'credit_start',
                    'debit_current',
                    'credit_current',
                    'debit_sum', 
                    'credit_sum'
                ],
                'sums_function'=>'sumFunc'
            ];
            default:
                return [
                    'columns' => array(
                        'code', 
                        'name',
                        'debit_start',
                        'credit_start',
                        'debit_current',
                        'credit_current',
                        'debit_sum', 
                        'credit_sum',                        
                        'description'
                    ),
                    'filters'=>array(
                        'year'
                    )
                ];
        }
    }
    
    
    public function sumFunc($criteria, $display_scopes)
    {
        $owner = $this->owner;
        $temp_criteria = new SIMADbCriteria();
        if (isset($display_scopes['withDCCurrents']))
        {
            $_scope = $display_scopes['withDCCurrents'];
            $_scope[1]['is_sum'] = true;
            $display_scopes['withDCCurrents'] = $_scope;
            $temp_criteria->scopes = ['withDCCurrents' => $_scope];            
        }
        else
        {
            return new Account();
        }

        $criteria->mergeWith($temp_criteria);

        $sum_result = $owner->resetScope()->findAll($criteria);
        if (count($sum_result)==0) 
        {
            return new Account();
        }
        return $sum_result[0];
        
    }
    
    public function vuePropValue($prop)
    {
        $owner = $this->owner;

        switch ($prop)
        {
            case 'model_options_data': 
                return array_merge(parent::vuePropValue($prop), [
                    'account_open_with_date' => SIMAHtmlButtons::AccountOpenWithDateWidgetAction($owner)
                ]);
                
            default: return parent::vuePropValue($prop);
        }
    }
}