<?php

class NoteTaxExemptionGUI extends SIMAActiveRecordGUI
{
    public function columnLabels()
    {
        return array(
            'name_of_note' => Yii::t('AccountingModule.NoteTaxExemption', 'NameOfNote'),
            'text_of_note' => Yii::t('AccountingModule.NoteTaxExemption', 'TextOfNote')
        ) + parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return Yii::t('AccountingModule.NoteTaxExemption', $plural ? 'NotesOfTaxExemption' : 'NoteOfTaxExemption');
    }
    
    public function modelForms()
    {
        return [
            'default' => [
                'type' => 'generic',
                'columns' => [
                    'name_of_note' => 'textField',
                    'text_of_note' => 'textArea',
                ]
              ],
        ];
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default: return [
                'columns' => [
                    'name_of_note', 'text_of_note'
                ]
            ];
        }
    }
}