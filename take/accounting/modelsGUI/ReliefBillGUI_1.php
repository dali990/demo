<?php

class ReliefBillGUI extends BillGUI 
{

    public function columnLabels() {
        return array(            
            'percent_of_bill' => 'Procenat'            
        ) + parent::columnLabels();
    }
    
    public function columnDisplays($column) 
    {
        $owner = $this->owner;
        switch ($column) {
            default: return parent::columnDisplays($column);
        }
    }
    
    public function modelLabel($plural = false)
    {
        return Yii::t('AccountingModule.Bill',$plural?'Credit memos':'Credit memo');
    }

    public function modelForms()
    {
        $owner = $this->owner;
        
        $vat_refusal = ['checkBox'];
        if (is_null($owner->vat_refusal))
        {
            $vat_refusal['disabled'] = 'disabled';
        }
        
        
        $cost_type_id = ['relation', 
            'relName' => 'cost_type', 
            'add_button'=>[
                'action'=>'accounting/costTypes',
                'params' => [
                    'get_params' => [
                        'cost_or_income' => $owner->cost_type_direction
                    ]
                ]
            ],
            'dependent_on'=>[                        
                'income_date'=>[
                    'onValue' => ['trigger', 'func' => ['sima.accountingMisc.billCostTypeDependentOnBillIncomeDateInForm']],
                    'onEmpty' => ['disable']
                ]                        
            ]
        ];
        if ($owner->cost_type_mixed === true)
        {
            $cost_type_id['disabled'] = 'disabled';
        }
        
        $cost_location_id = ['relation', 'relName' => 'job_order'];
        if ($owner->cost_location_mixed === true)
        {
            $cost_location_id['disabled'] = 'disabled';
        }
        
        $invoicing_basis = 'hidden';
        $invoice_comment = 'hidden';
        if ($owner->invoice === true)
        {
            $invoicing_basis = 'textField';
            $invoice_comment = 'textArea';
        }
        
        return array(
            'default' => array(
                'type' => 'generic',
                'columns' => array(
                    'bill_number' => 'textField',
                    'partner_id' => ['searchField', 'relName' => 'partner', 'add_button' => ['action'=>'simaAddressbook/index']],
                    'income_date'=> array('datetimeField'),
                    'cost_type_id' => $cost_type_id,
                    'cost_location_id' => $cost_location_id,
                    'payment_date'=> array('datetimeField'),
                    'lock_amounts' => 'checkBox',
                    'amount' => self::$locked_bill_values_form_type,
                    'base_amount0' => self::$locked_bill_values_form_type,
                    'base_amount10' => self::$locked_bill_values_form_type,
                    'base_amount20' => self::$locked_bill_values_form_type,
                    'vat10' => self::$locked_bill_values_form_type,
                    'vat20' => self::$locked_bill_values_form_type,
                    'responsible_id' => array('dropdown', 'empty'=>'Izaberi'),
                    'vat_refusal' => $vat_refusal,
                    'vat_in_diferent_month_id' => ['relation', 'relName' => 'vat_in_diferent_month'],
                    'lock_amounts' => 'checkBox',
                    'invoicing_basis' => $invoicing_basis,
                    'invoice_comment' => $invoice_comment,
                    'comment'=>'textArea'
                )
            )
        );
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            case 'confirm1':
                return array(
                    'columns'=>array(
                        'bill_number', 'partner', 'income_date', 'payment_date', 'responsible_id',
                        'amount', 'base_amount', 'vat', 'unreleased_amount', 'unreleased_amount_in_currency', 'belongs',
                        'cost_location', 'file.filing', 'comment'
                    ),
                    'sums' => array('amount','base_amount','unreleased_amount','vat'),
                    'order_by' => array('amount','base_amount','unreleased_amount','vat'),
                );
            case 'inPartner':
                return array(
                    'columns'=>array(
                        'bill_number', 'income_date', 'payment_date', 'amount', 'amount_in_currency', 
                        'unreleased_amount', 'unreleased_amount_in_currency',
                        'cost_location', 'comment', 'file.filing'
                    ),
                    'sums' => array('amount','base_amount','unreleased_amount','vat'),
                    'order_by' => array('amount','base_amount','unreleased_amount','vat'),
                );
            case 'inJobFinanse': return array(
                    'columns'=>array(
                        'bill_number', 'partner', 
                        'income_date', 'payment_date', 
                        'amount', 'amount_in_currency', 'unreleased_amount', 'unreleased_amount_in_currency', 'vat',
                        'comment', 'belongs'
                    ),
                    'sums' => array('amount','base_amount','unreleased_amount','vat'),
                    'order_by' => array('bill_number','amount','base_amount','unreleased_amount','income_date','payment_date'),
                );
            case 'inBillProposals': return array(
                    'columns'=>array( 
                        'bill_number', 'amount', 'amount_in_currency', 
                        'unreleased_amount', 'unreleased_amount_in_currency', 'unreleased_percent_of_amount', 'income_date',
                        'base_amount0', 'base_amount10', 'base_amount20', 'vat10', 'vat20'
                    ),
                    'sums' => array('amount','unreleased_amount'),
                    'order_by' => array('bill_number','amount','base_amount','unreleased_amount','income_date','payment_date'),
                );
            default:
                return array(
                    'columns'=>array(
                        'bill_number', 'partner', 'income_date', 'payment_date', 
                        'amount', 'amount_in_currency', 
                        'unreleased_amount', 'unreleased_amount_in_currency', 
                        'belongs', 'job_order', 'comment', 'file.filing','interest',
                        'file.account_document.account_order',
                        'file.account_document.account_order.date'
                    ),
                    'filters'=>array(
                        'year'
                    ),
                    'sums' => array('amount','base_amount','unreleased_amount','vat'),
                    'order_by' => array('amount','base_amount','unreleased_amount','vat','income_date','payment_date'),
                );
        }
    }
    
    public function vueProp(string $prop, array $scopes = []):array
    {
        $owner = $this->owner;
        switch ($prop)
        {
            case 'remaining_vat_items': 
                $remaining_vat_items = $owner->remainingVatItemsArray();
                return [$remaining_vat_items, SIMAMisc::getModelTags($owner->bill_releases)];
            case 'remaining_vat_items_posible': 
                $bill = Bill::model()->findByPkWithCheck($scopes['bill_id']);
                $remaining_vat_items_posible = ConnectAccountingDocuments::BillToReliefBill($bill, $owner, false);
                return [$remaining_vat_items_posible, SIMAMisc::getModelTags($owner->bill_releases)];
            default: return parent::vueProp($prop, $scopes);
        }
    }
}