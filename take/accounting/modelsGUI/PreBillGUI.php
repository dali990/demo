<?php

class PreBillGUI extends BillGUI 
{
    public function modelLabel($plural = false)
    {
        return Yii::t('AccountingModule.Bill',$plural?'Proforma bills':'Proforma bill');
    }
    
    public function modelForms()
    {
        $owner = $this->owner;

        $invoicing_basis = 'hidden';
        $invoice_comment = 'hidden';
        if ($owner->invoice === true)
        {
            $invoicing_basis = 'textField';
            $invoice_comment = 'textArea';
        }

        return array(
            'default' => array(
                'type' => 'generic',
                'columns' => array(
                    'bill_number' => 'textField',
                    'cost_location_id' => array('searchField','relName'=>'job_order'),
                    'partner_id' => ['searchField', 'relName' => 'partner', 'add_button' => ['action'=>'simaAddressbook/index']],
                    'partner_bank_account_id' => ['searchField','relName'=>'partner_bank_account', 'dependent_on'=>[                        
                        'partner_id'=>[
                            'onValue'=>[
                                'enable',
                                ['condition', 'model_filter' => 'partner.ids']
                            ],
                            'onEmpty'=>'disable'
                        ]                        
                    ]],
                    'income_date'=> array('datetimeField'),
                    'payment_date'=> array('datetimeField'),
                    'lock_amounts' => 'checkBox',
                    'amount' => self::$locked_bill_values_form_type,
                    'base_amount0' => self::$locked_bill_values_form_type,
                    'base_amount10' => self::$locked_bill_values_form_type,
                    'base_amount20' => self::$locked_bill_values_form_type,
                    'vat10' => self::$locked_bill_values_form_type,
                    'vat20' => self::$locked_bill_values_form_type,
                    'call_for_number' => 'textField',
                    'call_for_number_modul' => 'textField',
                    'invoicing_basis' => $invoicing_basis,
                    'invoice_comment' => $invoice_comment,
                    'comment'=>'textArea'
                )
            )
        );
    }
}
