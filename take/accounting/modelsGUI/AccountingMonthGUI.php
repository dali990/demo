<?php

class AccountingMonthGUI extends SIMAActiveRecordGUI
{

    public function columnLabels()
    {
        return array(
            'year' => Yii::t('AccountingModule.AccountingMonth','Year'),
            'month' => Yii::t('AccountingModule.AccountingMonth','Month'),
            'month_id' => Yii::t('AccountingModule.AccountingMonth','Month'),
            'point_value' => Yii::t('AccountingModule.AccountingMonth','PointValue'),
//            'hours' => Yii::t('AccountingModule.AccountingMonth','Hours'),
            'hot_meal_per_hour' => Yii::t('AccountingModule.AccountingMonth','HotMealPerHour'),
            'regres' => Yii::t('AccountingModule.AccountingMonth','Regres'),
            'exp_perc' => Yii::t('AccountingModule.AccountingMonth','ExpPerc'),
            'pio_empl' => Yii::t('AccountingModule.AccountingMonth','PioEmpl'),
            'zdr_empl' => Yii::t('AccountingModule.AccountingMonth','ZdrEmpl'),
            'nez_empl' => Yii::t('AccountingModule.AccountingMonth','NezEmpl'),
            'tax_empl' => Yii::t('AccountingModule.AccountingMonth','TaxEmpl'),
            'tax_release' => Yii::t('AccountingModule.AccountingMonth','TaxRelease'),
            'pio_comp' => Yii::t('AccountingModule.AccountingMonth','PioComp'),
            'zdr_comp' => Yii::t('AccountingModule.AccountingMonth','ZdrComp'),
            'nez_comp' => Yii::t('AccountingModule.AccountingMonth','NezComp'),
            'pio_int' => Yii::t('AccountingModule.AccountingMonth','PioCnt'),
            'zdr_int' => Yii::t('AccountingModule.AccountingMonth','ZdrCnt'),
            'nez_int' => Yii::t('AccountingModule.AccountingMonth','NezCnt'),
            'tax_int' => Yii::t('AccountingModule.AccountingMonth','TaxCnt'),
            'display_name' => Yii::t('AccountingModule.AccountingMonth','DisplayName'),
            'min_contribution_base' => Yii::t('AccountingModule.AccountingMonth','MinTaxBase'),
        )+parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return Yii::t('AccountingModule.AccountingMonth','AccountingMonth');
    }
    
    public function modelForms()
    {
        $monthDisplay = 'textField';
        $yearDisplay = 'textField';
        if(!$this->owner->getIsNewRecord())
        {
            $monthDisplay = array('textField', 'disabled'=>'disabled');
            $yearDisplay = array('textField', 'disabled'=>'disabled');
        }
        return array(
            'createOldPeriods' => array(
                'type'=>'generic',
                'columns'=>array(
                    'monthFrom' => 'datetimeField',
                    'monthTo' => 'datetimeField',
                    'point_value' => 'textField',
//                    'hours' => 'textField',
                    'hot_meal_per_hour' => 'textField',
                    'regres' => 'textField',
                    'exp_perc' => 'textField',
                    'pio_empl' => 'textField',
                    'zdr_empl' => 'textField',
                    'nez_empl' => 'textField',
                    'tax_empl' => 'textField',
                    'tax_release' => 'textField',
                    'pio_comp' => 'textField',
                    'zdr_comp' => 'textField',
                    'nez_comp' => 'textField',
                    'pio_int' => 'textField',
                    'zdr_int' => 'textField',
                    'nez_int' => 'textField',
                    'tax_int' => 'textField',
                    'min_contribution_base'
                )
            ),
            'default'=>array(
                'type'=>'generic',
                'columns'=>array(
                    'month' => $monthDisplay,
                    'year' => $yearDisplay,
                    'point_value' => array('textField', 'placeholder'=>Yii::t('AccountingModule.AccountingMonth','Leave empty for values from previous month')),
//                    'hours' => array('textField', 'placeholder'=>Yii::t('AccountingModule.AccountingMonth','HoursLeaveEmpty')),
                    'hot_meal_per_hour' => array('textField', 'placeholder'=>Yii::t('AccountingModule.AccountingMonth','Leave empty for values from previous month')),
                    'regres' => array('textField', 'placeholder'=>Yii::t('AccountingModule.AccountingMonth','Leave empty for values from previous month')),
                    'exp_perc' => array('textField', 'placeholder'=>Yii::t('AccountingModule.AccountingMonth','Leave empty for values from previous month')),
                    'pio_empl' => array('textField', 'placeholder'=>Yii::t('AccountingModule.AccountingMonth','Leave empty for values from previous month')),
                    'zdr_empl' => array('textField', 'placeholder'=>Yii::t('AccountingModule.AccountingMonth','Leave empty for values from previous month')),
                    'nez_empl' => array('textField', 'placeholder'=>Yii::t('AccountingModule.AccountingMonth','Leave empty for values from previous month')),
                    'tax_empl' => array('textField', 'placeholder'=>Yii::t('AccountingModule.AccountingMonth','Leave empty for values from previous month')),
                    'tax_release' => array('textField', 'placeholder'=>Yii::t('AccountingModule.AccountingMonth','Leave empty for values from previous month')),
                    'pio_comp' => array('textField', 'placeholder'=>Yii::t('AccountingModule.AccountingMonth','Leave empty for values from previous month')),
                    'zdr_comp' => array('textField', 'placeholder'=>Yii::t('AccountingModule.AccountingMonth','Leave empty for values from previous month')),
                    'nez_comp' => array('textField', 'placeholder'=>Yii::t('AccountingModule.AccountingMonth','Leave empty for values from previous month')),
                    'pio_int' => array('textField', 'placeholder'=>Yii::t('AccountingModule.AccountingMonth','Leave empty for values from previous month')),
                    'zdr_int' => array('textField', 'placeholder'=>Yii::t('AccountingModule.AccountingMonth','Leave empty for values from previous month')),
                    'nez_int' => array('textField', 'placeholder'=>Yii::t('AccountingModule.AccountingMonth','Leave empty for values from previous month')),
                    'tax_int' => array('textField', 'placeholder'=>Yii::t('AccountingModule.AccountingMonth','Leave empty for values from previous month')),
                    'min_contribution_base' => array('textField', 'placeholder'=>Yii::t('AccountingModule.AccountingMonth','Leave empty for values from previous month')),
                )
            )
        );
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default: return array(
                'columns'=>array(
                    'month',
                    'year',
                    'display_name',
                    'point_value',
//                    'hours',
                    'hot_meal_per_hour',
                    'regres',
                    'exp_perc',
                    'pio_empl',
                    'zdr_empl',
                    'nez_empl',
                    'tax_empl',
                    'tax_release',
                    'pio_comp',
                    'zdr_comp',
                    'nez_comp',
                    'min_contribution_base'
                ),
                'order_by' => array('display_name')
            );
        }
    }
}