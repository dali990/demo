<?php

class PPPPDItemGUI extends SIMAActiveRecordGUI
{
    public function columnLabels()
    {
        return [
//            'client_identificator' => Yii::t('AccountingModule.PPPPD', 'ClientIdentificator'),
//            'file' => Yii::t('AccountingModule.PPPPD', 'File'),
//            'create_time' => Yii::t('AccountingModule.PPPPD', 'CreateTime'),
//            'note' => Yii::t('AccountingModule.PPPPD', 'note')
        ] + parent::columnLabels();
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            case 'from_ppppd_full_info':
                return [
                    'columns' => [
                        'indentification_value'
                    ]
                ];
            default:
                return [
                    'columns' => [
                        'svp',
                        'order_number',
                        'indentification_type',
                        'indentification_value',
                        'lastname',
                        'firstname',
                        'municipality_code',
                        'number_of_calendar_days',
                        'effective_hours',
                        'month_hours',
                        'bruto',
                        'tax_base',
                        'tax_empl',
                        'doprinosi_base',
                        'pio',
                        'zdr',
                        'nez',
                        'pio_ben',
                        'mfp1',
                        'mfp2',
                        'mfp3',
                        'mfp4',
                        'mfp5',
                        'mfp6',
                        'mfp7',
                        'mfp8',
                        'mfp9',
                        'mfp10',
                        'mfp11',
                        'mfp12'
                    ]
                ];
        }
    }
}

