<?php
/**
 * Description of BillOfExchangeGUI
 *
 * @author goran-set
 */
class BillOfExchangeGUI extends SIMAActiveRecordGUI
{
    public function modelLabel($plural = false)
    {
        return Yii::t('AccountingModule.BillOfExchange', $plural ? 'BillsOfExchange' : 'BillOfExchange');
    }
    
    public function columnLabels() 
    {
        return array(
            'id'=>'Redni broj',
            'number'=>'Broj',
            'partner_id'=>'Partner',
            'begin_date'=>'Datum izdavanja',
            'end_date'=>'Datum isteka',
            'status'=>'Status',
            'input'=>'Ulazna/Izlazna',
            'description'=>'Napomena',
            'authorization_file_id'=>'Ovlašćenje',
            'authorization_file'=>'Ovlašćenje',
            'belongs' => 'Pripadnost'
        )+parent::columnLabels();
    }
   // Ukoliko je menica vracena, u polju status u postoji link da se napravi nova menica sa istim brojem i statusom "Nova" (sima.misc.neka_funkcija())
    
    public function columnDisplays($column)
    {
        $owner = $this->owner;
        switch ($column)
        {
            case 'input': return ($owner->input)?'Ulazna':'Izlazna';
            case 'status':
                switch ($owner->status)
                {
                    case 0: return "Nova"; 
                    case 1: return "Izdata"; 
                    case 2: 
                        {
                            $criteria=new CDbCriteria;
                            $criteria->condition="number='$owner->number' and status != 2";
                            $equals=  BillOfExchange::model()->findAll($criteria);
                            if(count($equals)>0)
                            {
                                return "Vraćena";
                            }
                            else
                            {
                                return "Vraćena - <span class='link' onclick=sima.misc.addNewBillOfExchange('$owner->number',$(this));>dodaj novu</span>";
                            }   
                        }
                    case 3: return "Aktivirana"; 
                    default : return "Netačan podatak! ".$owner->status;
                }
            case 'authorization_file':
                {
                    if(!empty($owner->authorization_file_id))
                    {
                        $return = '';
                        if (!empty($owner->authorization_file))
                        {
                            $file_name=(isset($owner->authorization_file->filing))?
                                    $owner->authorization_file->filing->filing_number.' od ' . $owner->authorization_file->filing->date 
                                :  Yii::app()->controller->renderModelView($owner->authorization_file, 'filingNumber').' - '.$owner->authorization_file->DisplayName;
                            
                            $return = $file_name.SIMAHtml::modelDialog($owner->authorization_file);
                        }
                        else
                        {
                            $return = 'Nemate pristup fajlu.';
                        }
                       
                       return $return;
                    }
                    else
                    {
                       return "<span class='link' onclick=sima.misc.addAuthorizationFileToBillOfExchange('$owner->id',$(this));>dodaj fajl</span>";
                    }
                }
            case 'partner_id':
                {
                    return isset($owner->partner_id)?$owner->partner->DisplayName.SIMAHtml::modelDialog($owner->partner):'nema';
                }
            default : return parent::columnDisplays($column);
        }
    }
    
    public function modelViews()
    {
        return array(
            'inFile'=>array('class'=>'basic_info')
        );
    }
    
    public function modelForms()
    {
        $number='textField';
        $begin_date='datetimeField';
        $owner = $this->owner;
        $old = $owner->getOld();
        //privremeno dok se ne srede menice
//        if($old['status']==1)
//        {
//            $number='display';
//            $begin_date='display';
//        }
        
        return array(
            'default'=>array(
                'type'=>'generic',
                'columns'=>array(
                    'id'=>'hidden', //mora postojati u svakoj formi
                    'number'=>$number,
                    'input'=>array('dropdown'),
                    'begin_date'=>$begin_date,
                    'authorization_file_id'=>array('searchField','relName'=>'authorization_file'),
                    'end_date'=>'datetimeField',
                    'partner_id'=>array('searchField', 'relName'=>'partner'),
                    'status'=>array('dropdown'),
//                    'belongs'=>'belongs',
                    'description'=>'textArea'
                )
            ));
    } 
    
    public function droplist($key, $relName='', $filter=null) {
        switch ($key) {
            case 'status': return array(0=>'Nova', 1=>'Izdata',2=>'Vraćena', 3=>'Aktivirana');
            case 'input': return array(false=>'Izlazna', true=>'Ulazna');
            default: return parent::droplist($key, $relName, $filter);
        }
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default:
                return [
                    'columns' => ['number','begin_date','authorization_file','end_date','partner','status','belongs','input','description']
                ];
        }
    }
}
