<?php

class CashDeskLogGUI extends SIMAActiveRecordGUI
{

    public function columnLabels()
    {
        return array(
            'date' => 'Datum',
            'in' => 'Ulaz',
            'out' => 'Izlaz',
        ) + parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return 'Dnevnik blagajne';
    }
    
    public function columnDisplays($column)
    {
        $owner = $this->owner;
        switch ($column)
        {
            case 'in':
            {
                //mora rucno jer se ovo radi u parentcolumnDisplays
                return SIMAHtml::number_format($owner->in);
            };
            case 'out':
            {
                //mora rucno jer se ovo radi u parentcolumnDisplays
                return SIMAHtml::number_format($owner->out);
            }
            default: return parent::columnDisplays($column);
        }
    }

    public function modelViews()
    {
        return array(
        );
    }

    public function modelForms()
    {
        return array(
            'default' => array(
                'type' => 'generic',
                'columns' => array(
                    'date' => 'datetimeField'
                )
            )
        );
    }
    
    public function guiTableSettings($type)
    {
        switch ($type) 
        {
            default: return [
                'columns' => [
                    'date', 'in', 'out', 'saldo', 'file.account_document.account_order'
                ]
            ];
        }
    }
    
    public function generatePdf()
    {
        $owner = $this->owner;
        $cash_desk_log_report = new CashDeskLogReport([
            'cash_desk_log' => $owner
        ]);          
        $temp_file = $cash_desk_log_report->getPDF();        
        $owner->file->appendTempFile(
            $temp_file->getFileName(), 
            $owner->DisplayName . '.pdf'
        );
        return $temp_file;
    }
    
}