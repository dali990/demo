<?php

class BillReleaseGUI extends SIMAActiveRecordGUI
{
    public function columnLabels()
    {
        return [
            'payment' => 'Placanje',
            'percent_of_bill' => 'Procenat',
            'percent_of_payment' => 'Procenat',
            'payment_id' => 'Placanje',
            'bill_id' => 'Račun',
            'bill' => 'Račun',
            'amount' => Yii::t('AccountingModule.BillRelease','Released amounts'),
            'amount_in_currency' => Yii::t('AccountingModule.BillRelease','Released amounts in currency'),
            'exchange_rate_difference' => 'Kursna razlika',
            'other_difference' => 'Ostala razlika'
        ];
    }
    
    public function modelLabel($plural = false)
    {
        return 'Rasknjizeno';
    }
    
    public function columnDisplays($column)
    {
        $owner = $this->owner;
        switch ($column)
        {
            case 'amount_in_currency':
                return SIMAHtml::showInCurrency($owner->amount, $owner->bill->bound_currency, Day::get($owner->bill->income_date));
//                return $owner->amount;
                
            case 'percent_of_bill': 
                if (floatval($owner->bill->amount)==0)
                {
                    return '? %';
                }
                else
                {
                    return SIMAHtml::money_format((floatval($owner->amount)*100.0/floatval($owner->bill->amount)),'%');
                }
            case 'percent_of_payment': return SIMAHtml::money_format((floatval($owner->amount)*100.0/floatval($owner->payment->amount)),'%');
            case 'status': 
                $result = $owner->calcStatus();
                $status = $result['status'];
                $status_class = '';
                switch ($status)
                {
                    case BillRelease::$STATUS_PRETIME:
                        $status_text = Yii::t('AccountingModule.BillRelease', 'StatusPRETIME');
                        $status_class = 'sima-acc-br-pretime';
                        break;
                    case BillRelease::$STATUS_ONTIME:
                        $status_text = Yii::t('AccountingModule.BillRelease', 'StatusONTIME');
                        $status_class = 'sima-acc-br-ontime';
                        break;
                    case BillRelease::$STATUS_LATE:
                        $status_text = Yii::t('AccountingModule.BillRelease', 'StatusLATE');
                        $status_class = 'sima-acc-br-late';
                        break;
                    case BillRelease::$STATUS_TODAY:
                    case BillRelease::$STATUS_ONDAY:
                        $status_text = Yii::t('AccountingModule.BillRelease', 'StatusONDAY');
                        $status_class = 'sima-acc-br-ontime';
                        break;
                }
                $days_diff = $result['days_diff'];
                if ($days_diff!=0)
                {
                    $status_text = '('.$days_diff.')'.$status_text;
                }
                return '<span class="'.$status_class.'">'.$status_text.'</span>';
            default: return parent::columnDisplays($column);
        }
    }
    
    public function modelForms()
    {
        return array(
            'default' => array(
                'type' => 'generic',
                'columns' => array(
                    'bill_id' => ['relation','relName' => 'bill','disabled'=>'disabled'] ,
                    'payment_id' => ['relation','relName' => 'payment','disabled'=>'disabled'] ,
                    'amount' => 'numberField',
                    'exchange_rate_difference' => 'numberField',
                    'other_difference' => 'numberField'
                )
            )
        );
    }

    public function guiTableSettings($type)
    {
        switch ($type)
        {
            case 'inBill':
            case 'inAdvanceBill':
                return [
                    'columns' => [
                        'payment',
                        'amount',
                        'amount_in_currency',
                        'exchange_rate_difference',
                        'other_difference',
                        'percent_of_bill',
                        'payment.comment'
                    ]
                ];
            case 'inAdvance':return [
                'columns' => [
                    'bill','bill.comment'
                ]
            ];
            case 'inPayment':return [
                'columns' => [
                    'bill','amount','percent_of_payment','exchange_rate_difference','payment.comment'
                ]
            ];
            default: return [
                'columns' => [
                    'bill','payment','amount','exchange_rate_difference'
                ]
            ];
        }
        
    }
}