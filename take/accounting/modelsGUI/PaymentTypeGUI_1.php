<?php

class PaymentTypeGUI extends SIMAActiveRecordGUI
{

    public function columnLabels()
    {
        return array(
            'name' => 'Naziv',
            'comment' => 'Komentar',
            'parent_id' => 'Nad tip',
            'account_id' => 'Konto',
            'account' => 'Konto',
            'parent' => 'Nadtip',
            'partner_analitic' => 'Analitika po partneru'
        )+parent::columnLabels();
    }

    public function modelLabel($plural = false)
    {
        return Yii::t('AccountingModule.PaymentType','Payment type');;
    }
    
//    public function modelViews()
//    {
//        return array(
//            'paychecks'//=>array('style'=>'border: 1px solid;'),
////            'tabTemplates'=>array('with'=>array('templated_file'),'style'=>'border: 1px solid;')  //.creator
//        );
//    }
    
    public function modelForms()
    {
        return array(
            'default'=>array(
                'type'=>'generic',
                'columns'=>array(
                    'name' => 'textField',
                    'description' => 'textArea',
                    'parent_id' => array('dropdown','empty'=>'Nema'),
                    'account_id' => ['relation', 
                        'relName'=>'account',
                        'filters' => [
                            'year' => ['ids' => Year::get()->id]
                        ]
                    ],
                    'partner_analitic' => 'checkBox'
                )
            )
        );
    }
    
    public function guiTableSettings($type) 
    {
        switch ($type) 
        {
            default:
                return [
                    'columns'=>[
                        'name', 'parent', 'account', 'partner_analitic', 'description'
                    ]
                ];
        }
    }
}