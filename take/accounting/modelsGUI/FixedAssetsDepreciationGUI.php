<?php

class FixedAssetsDepreciationGUI extends SIMAActiveRecordGUI
{

    public function columnLabels()
    {
        return array(
            'year'=>Yii::t('BaseModule.Common','Year'),
            'year_id'=>Yii::t('BaseModule.Common','Year'),
            
//            'account_order_id'=>'Nalog za knjiženje'            
        ) + parent::columnLabels();
    }

    public function modelLabel($plural = false)
    {
        return Yii::t('AccountingModule.FixedAssets','FixedAssetsDepreciation');
    }
    
    public function modelViews()
    {
        return array(
//            'account_order'
        );
    }

    public function modelForms()
    {
        return array(
            'default' => array(
                'type' => 'generic',
                'columns' => array(
                    'year_id' => ['relation','relName' => 'year'],
                    'comment' => 'textArea'
                )
            )
        );
    }
    
    public function guiTableSettings($type)
    {
        return [
            'columns' => [
                'year','comment'
            ]
        ];
    }

}