<?php

class BankGUI extends SIMAActiveRecordGUI
{

    public function columnLabels()
    {
        return array(
            'bank_account_code' => 'Prefix za racune',
            'id' => 'Banka',
            'short_name' => 'Skraceno ime',
            'company' => 'Kompanija',
            'shared_accounts' => Yii::t('AccountingModule.Bank', 'SharedAccounts'),
            'is_in_unified' => Yii::t('AccountingModule.Bank', 'IsInUnified')
        )+parent::columnLabels();
    }
    
    public function columnDisplays($column)
    {
        $owner = $this->owner;
        switch ($column)
        {
            case 'shared_accounts':
                if($owner->shared_accounts === true)
                {
                    return Yii::t('BaseModule.Common', 'Yes');
                }
                else
                {
                    return Yii::t('BaseModule.Common', 'No');
                }
            case 'is_in_unified':
                $result = Yii::t('BaseModule.Common', 'No');
                
                if($owner->IsInUnified())
                {
                    $result = Yii::t('BaseModule.Common', 'Yes');
                }
                
                return $result;
            default: return parent::columnDisplays($column);
        }
    }

//    public function modelViews()
//    {
//        return array(
//            'basicInfoTitle',
//            'basicInfo',
//            'current_materials_status'=>array('params_function'=>'viewParamsCurrentMaterialsStatus')
////            'leaf'=>array(
//////                'style'=>'border: 1px solid;'
////                ),
////            'default_edit'
////            'tabTemplates'=>array('with'=>array('templated_file'),'style'=>'border: 1px solid;')  //.creator
//        );
//    }
    
   
    
    public function modelForms()
    {
        return array(
            'default'=>array(
                'type'=>'generic',
                'columns'=>array(
//                    'code'=>'textField',
                    'id'=>array('searchField','relName'=>'company'),
//                    'partner_id'=>array('searchField','relName'=>'partner'),
                    'bank_account_code'=>'textField',
                    'short_name'=>'textField',
                    'shared_accounts' => 'dropdown'
                )
            )
        );
    }
    
    
    public function guiTableSettings($type) 
    {
        switch ($type) 
        {
            case 'inPeriod':
                return [
                    'columns'=>[
                        'DisplayName', 'is_in_unified'
                    ]
                ];
            default:
                return [
                    'columns'=>[
                        'bank_account_code', 'company.name', 'short_name', 'shared_accounts'
                    ]
                ];
        }
    }
    
    public function droplist($key, $relName='', $filter=null)
    {
        switch ($key)
        {
            case 'shared_accounts':
                return array(
                    true => Yii::t('BaseModule.Common', 'Yes'),
                    false => Yii::t('BaseModule.Common', 'No'),
                );
            default: return parent::droplist($key, $relName, $filter);
        }
    }
}