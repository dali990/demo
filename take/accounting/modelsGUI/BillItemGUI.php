<?php

class BillItemGUI extends SIMAActiveRecordGUI
{
    
    public function columnLabels()
    {
        return array(
            'order' => '#',
            'title' => 'Naziv Usluge',
            'unit.name' => 'Jedinica mere',
            'unit' => 'Jedinica mere',
            'unit_id' => 'Jedinica mere',
            'quantity' => 'Količina',
            'value_per_unit' => 'Jedinična cena',
            'value_total'=>'Ukupno',
            'value' => 'Osnovica',
            'value_vat' => Yii::t('AccountingModule.BillItem','VAT value'),
            'vat' => Yii::t('AccountingModule.BillItem','VAT rate'),
            'discount' => 'Rabat',
            'warehouse_material_id' => 'Magacinski materijal',
            'cost_type' => Yii::t('AccountingModule.CostType','CostIncomeType'),
            'cost_type_id' => Yii::t('AccountingModule.CostType','CostIncomeType'),
            'cost_location' => CostLocationGUI::modelLabel(),
            'cost_location_id' => CostLocationGUI::modelLabel(),
            'vat_refusal' => Yii::t('AccountingModule.Bill','VAT refusal'),
//            'is_internal_vat' => Yii::t('AccountingModule.Bill','VAT calc internally'),
            'internal_vat' => Yii::t('AccountingModule.BillItem','VAT rate internal'),
            'value_internal_vat' => Yii::t('AccountingModule.BillItem','VAT value internal'),
            'is_discount_item' => Yii::t('AccountingModule.BillItem','Discount item'),
            'no_vat_type' => Yii::t('AccountingModule.BillItem','NoVatType'),
            'connected_customs_bill' => Yii::t('AccountingModule.BillItem','ConnectedCustomsBill'),
            'customs_vat_bill' => Yii::t('AccountingModule.BillItem','CustomsBill'),
            'customs_vat_bill_id' => Yii::t('AccountingModule.BillItem','CustomsBill'),
            'customs_vat_bill_item_id' => Yii::t('AccountingModule.BillItem','CustomsBillItem'),
            'popdv_display' => Yii::t('AccountingModule.BillItem','NoVatType'),
        )+parent::columnLabels();
    }
    
    public function modelLabel($plural = false) 
    {
        return Yii::t('AccountingModule.BillItem', 'Bill item');
    }
    
    public function vuePropValue($prop)
    {
        $owner = $this->owner;
        switch ($prop)
        {
            case 'no_vat_type_text': return POPDVBillLists::$names[$owner->no_vat_type];
            case 'DisplayHTML': 
                if ($owner->bill_item_type === BillItem::$WAREHOUSE)
                {
                    return $owner->warehouse_material->DisplayHTML;
                }
                else
                {
                    return parent::vuePropValue($prop);
                }
            case 'add_info': 
                switch ($owner->bill_item_type) 
                {
                    case BillItem::$WAREHOUSE:      
                        if (isset($owner->warehouse_material))
                        {
                            $result = $owner->warehouse_material->DisplayHTML;
                        }
                        else
                        {
                            $result = '';
                        }
                        
                        
                        break;
                    case BillItem::$SERVICES:      
                        $add_info = [];
                        if (in_array($owner->no_vat_type, [POPDVBillLists::$U_2_Ca, POPDVBillLists::$U_2_Cb]))
                        {
                            $add_info[] = 'Carinska veza: '.$owner->getAttributeDisplay('connected_customs_bill');
                        }
                        else
                        {
                            if(isset($owner->cost_location))
                            {
                                $add_info[] = $owner->cost_location->DisplayHTML;
                            }
                            if(isset($owner->cost_type))
                            {
                                $add_info[] = $owner->cost_type->DisplayHTML;
                            }
                        }
                        $result = implode('<span class="alpha">|</span>', $add_info);
                        break;
                    case BillItem::$FIXED_ASSETS:   
                        $_result = [];
                        $_max_display = 10;
                        foreach ($owner->fixed_assets as $fixed_asset)
                        {
                            $_result[] = $fixed_asset->DisplayHTML;
                            $_max_display--;
                            if ($_max_display<0)
                            {
                                break;
                                
                            }
                        }
                        $result = implode('<span class="beta">|</span>', $_result);
                        break;
                    default: $result = '';break;
                }
                return $result;
            case 'value_starred':
                return ($owner->vat_customs !== '0');
            case 'value':
                if ($owner->vat_customs !== '0')
                {
                    return $owner->value_customs;
                }
                else
                {
                    return $owner->value;
                }       
            case 'vat_starred': 
                return $owner->is_internal_vat || ($owner->vat_customs !== '0');
            case 'vat': 
                if ($owner->is_internal_vat)
                {
                    return $owner->internal_vat;
                }
                elseif ($owner->vat_customs !== '0')
                {
                    return $owner->vat_customs;
                }
                else
                {
                    return $owner->vat;
                }
            case 'value_vat':
                if ($owner->is_internal_vat)
                {
                    return $owner->value_internal_vat;
                }
                elseif ($owner->vat_customs !== '0')
                {
                    return $owner->value_vat_customs;
                }
                else
                {
                    return $owner->value_vat;
                }
            default: return parent::vuePropValue($prop);
        }
    }
    
    public function columnDisplays($column)
    {
        $owner = $this->owner;
        switch ($column)
        {
            case 'discount': 
                return SIMAHtml::money_format($owner->$column, '%');
            case 'value_per_unit':
                return SIMAHtml::number_format($owner->$column, 2);
            case 'T':
                switch ($owner->bill_item_type) 
                {
                    case BillItem::$WAREHOUSE:      $result = 'M';break;
                    case BillItem::$SERVICES:       $result = 'U';break;
                    case BillItem::$FIXED_ASSETS:   $result = 'OS';break;
                    default: $result = '';break;
                }
                return $result.(($owner->is_discount_item)?'-':'');
            case 'title':
                if ($owner->isWarehouseBillItem)
                {
                    return $owner->warehouse_material->DisplayHTML;
                }
                else
                {
                    return parent::columnDisplays($column);
                }
            case 'bill_item_type':
                return BillItem::$bill_item_types[$owner->$column];
            case 'no_vat_type': return POPDVBillLists::$names[$owner->$column];
            case 'cost_type': case 'cost_location':
                if ($owner->isWarehouseBillItem || $owner->isFixedAssetsBillItem)
                {
                    return 'N/A';
                }
                else
                {
                    if (is_null($owner->$column))
                    {
                        return $owner->bill->getAttributeDisplay('local_'.$column);
                    }
                    else
                    {
                        return parent::columnDisplays($column);
                    }
                }
              
            case 'value':
                if ($owner->vat_customs !== '0')
                {
                    return '*'.parent::columnDisplays('value_customs');
                }
                else
                {
                    return parent::columnDisplays('value');
                }        
            case 'vat': 
                if ($owner->is_internal_vat)
                {
                    return '*'.SIMAHtml::money_format($owner->internal_vat, '%');
                }
                elseif ($owner->vat_customs !== '0')
                {
                    return '*'.SIMAHtml::money_format($owner->vat_customs, '%');
                }
                else
                {
                    return SIMAHtml::money_format($owner->vat, '%');
                }
            case 'value_vat':
                if ($owner->is_internal_vat)
                {
                    return '*'.parent::columnDisplays('value_internal_vat');
                }
                elseif ($owner->vat_customs !== '0')
                {
                    return parent::columnDisplays('value_vat_customs');
                }
                else
                {
                    return parent::columnDisplays('value_vat');
                }
            case 'popdv_display': return $owner->getAttributeDisplay('no_vat_type');
            case 'connected_customs_bill': 
                if ($owner->no_vat_type == POPDVBillLists::$U_2_Ca && isset($owner->customs_vat_bill_item))
                {
                    return parent::columnDisplays('customs_vat_bill')." (#".$owner->customs_vat_bill_item->DisplayName.")";
                }
                elseif ($owner->no_vat_type == POPDVBillLists::$U_2_Cb && isset($owner->customs_vat_base_bill_item))
                {
                    return $owner->customs_vat_base_bill_item->bill->DisplayHTML." (#".$owner->customs_vat_base_bill_item->DisplayName.")";
                }
                else
                {
                    return '-';
                }
                
            case 'vat_refusal': 
                if (in_array($owner->no_vat_type, POPDVBillLists::$autoVatRefusalTRUEList))
                {
                    if ($owner->vat_refusal===false)
                    {
                        $error_message_display = 'no_vat_type: '.$owner->no_vat_type.' vat_refusal: FALSE ID: '.$owner->id;
                        Yii::app()->errorReport->createAutoClientBafRequest($error_message_display);
                        return Yii::t('BaseModule.Common','No').'('.Yii::t('BaseModule.Common','Yes').')!!!';
                    }
                    else 
                    {
                        return parent::columnDisplays($column);
                    }
                }
                elseif (in_array($owner->no_vat_type, POPDVBillLists::$autoVatRefusalFALSEList))
                {
                    if ($owner->vat_refusal===TRUE)
                    {
                        $error_message_display = 'no_vat_type: '.$owner->no_vat_type.' vat_refusal: TRUE ID: '.$owner->id;
                        Yii::app()->errorReport->createAutoClientBafRequest($error_message_display);
                        return Yii::t('BaseModule.Common','Yes').'('.Yii::t('BaseModule.Common','No').')!!!';
                    }
                    else 
                    {
                        return parent::columnDisplays($column);
                    }
                }
                elseif (in_array($owner->no_vat_type, POPDVBillLists::$chooseVatRefusalList))
                {
                    return parent::columnDisplays($column);
                }
                else
                {
                    return '-';
                }
            default: return parent::columnDisplays($column);
        }
    }
    
    public function modelForms()
    {
        $owner = $this->owner;
        
        $bill = AnyBill::model()->findByPK($owner->bill_id);
        
        if (is_null($bill))
        {   
            return ['default' => [
                'type' => 'generic',
                'columns' => []
            ]];
        }
        
        $_is_invoice = $bill->invoice == true;
        
        $title_tinymce_params = [
            'plugins' => [
                'lists charmap insertdatetime paste'
            ],
            'toolbar' => "undo redo | bold italic underline  | numlist bullist outdent indent | superscript subscript | euro-sign",
            'menubar' => false,
            'statusbar' => false,
        ];
        $title = ['tinymce', 'params' => $title_tinymce_params];
//        $quantity = [
//            'unitMeasureField',
//            'unit'=>array('unit_id','empty'=>'j.m.'),
//            'dependent_on' => [
//                'no_vat_type' => [
//                    'onValue'=>[
//                        ['disable', 'for_values' => POPDVBillLists::$U_2_Cb]
//                    ]
//                ],
//            ]
//        ];
        $vat = ['dropdown'];
        
        
        $no_vat_type = [
            'dropdown',
            'dependent_on' => [
                'vat' => [
                    'onValue'=>[
                        ['trigger', 'func' => ['sima.accounting.billItemFormPOPDVChooser', POPDVBillLists::getFormLists($_is_invoice)]],
                    ],
                ],
            ]
        ];

        if ($_is_invoice)
        {
            $vat_refusal = ['hidden'];
            $customs_vat_bill_id = ['hidden'];
            $customs_vat_bill_item_id = ['hidden'];
        }
        else
        {
            $vat_refusal = [
                'dropdown',
                'dependent_on' => [
                    'no_vat_type' => [
                        'onValue'=>[
                            ['show', 'for_values' => POPDVBillLists::$chooseVatRefusalList, 'set_empty_on_hide' => false],
                        ]
                    ]
                ]
            ];
            $customs_vat_bill_id = [
                'relation',
                'relName' => 'customs_vat_bill',
                'dependent_on' => [
                    'no_vat_type' => [
                        'onValue'=>[
                            ['show', 'for_values' => POPDVBillLists::$U_2_Ca]
                        ]
                    ]
                ]
            ];
            $customs_vat_bill_item_id = [
                'relation',
                'relName' => 'customs_vat_bill_item',
                'dependent_on' => [
                    'customs_vat_bill_id' => [
                        'onValue'=>[
                            ['condition', 'model_filter' => 'bill.ids'],
                            ['enable']
                        ],
                        'onEmpty' => [
                            ['disable']
                        ]
                    ],
                    'no_vat_type' => [
                        'onValue'=>[
                            ['show', 'for_values' => POPDVBillLists::$U_2_Ca]
                        ]
                    ],
                ]
            ];
        }
        
        $value_vat = [
            'numberField',
            'dependent_on' => [
                'no_vat_type' => [
                    'onValue' => [
                        ['hide', 'for_values' => POPDVBillLists::$noVatValueList, 'set_empty_on_hide' => true],//MIlosS(7.8.2018): dodat true zbog izlaznih racuna 
                    ]
                ]
            ]
        ];
        
        $ajaxlong = false;
        
        if ($owner->isWarehouseBillItem)
        {
            $quantity['disabled'] = 'disabled';
            $quantity['unit']['disabled'] = 'disabled';
            $title['disabled'] = 'disabled';
            $owner->title = $owner->warehouse_material->DisplayName;
            $ajaxlong = true;
        }
        
        if ($owner->isFixedAssetsBillItem || $owner->isWarehouseBillItem)
        {
            $cost_type_id = 'hidden';
            $cost_location_id = 'hidden';
        }
        else
        {
            $year = $bill->year;
            $_cost_or_income = $_is_invoice?CostType::$TYPE_INCOME:CostType::$TYPE_COST;
            $cost_type_id = ['relation', 'relName' => 'cost_type', 'filters' => [
                'cost_or_income' => $_cost_or_income,
                'cost_types_to_accounts' => [
                    'account' => [
                        'year' => ['ids' => $year->id]
                    ]
                ]
            ]];
            $cost_location_id = ['relation', 'relName' => 'cost_location'];
        }
        
        $JCI_hide_numeric = [
            'numberField',
            'dependent_on' => [
                'no_vat_type' => [
                    'onValue'=>[
                        ['disable', 'for_values' => POPDVBillLists::$U_2_Cb, 'set_empty_on_disable' => false]
                    ]
                ],
            ]
        ];
        
        $unit = [
            'dropdown',
            'empty' => 'j.m.',
            'dependent_on' => [
                'no_vat_type' => [
                    'onValue'=>[
                        ['disable', 'for_values' => POPDVBillLists::$U_2_Cb]
                    ]
                ],
            ]
        ];
        
        //TODO:ispraviti da se ne koristi vise vat polja nego samo jedno
        //potrebni ozbiljni testovi za ovo
        if (in_array($owner->no_vat_type, POPDVBillLists::$internalVATList))
        {
            $owner->vat = $owner->internal_vat;
            $owner->value_vat = $owner->value_internal_vat;
        }
        
        //MilosS: drugi deo je da se prilikom pucanja provere ne resetuje vrednost
        if ($owner->no_vat_type === POPDVBillLists::$U_2_Cb && $owner->vat == '0')
        {
            $owner->vat = $owner->vat_customs;
            $owner->quantity = 1;
            $owner->value_per_unit = $owner->value_customs;
            $owner->value = $owner->value_customs;
            $owner->value_vat = $owner->value_vat_customs;
        }
        
        //isto u beforeFormOpen
        if ($owner->isNewRecord && !empty($owner->bill_id))
        {
            //da ne bi popunjavao relaciju $owner->bill
            $_bill = Bill::model()->findByPk($owner->bill_id);
            if (count($_bill->cost_locations) > 0)
            {
                $owner->cost_location_id = $_bill->cost_locations[0]->id;
            }
            else
            {
                $owner->cost_location_id = Yii::app()->configManager->get('accounting.default_cost_location_id', false);
            }
            
            if (count($_bill->cost_types) > 0)
            {
                $owner->cost_type_id = $_bill->cost_types[0]->id;
            }
            else
            {
                $owner->cost_type_id = Yii::app()->configManager->get(
                    $owner->bill->invoice?
                        'accounting.default_income_cost_type_id'
                        :
                        'accounting.default_cost_cost_type_id'
                    , false);
            }
         
        }
        
        if ($owner->bill->isBillTypeWithDiscount)
        {
            $_discount = $JCI_hide_numeric;
            $is_discount_item = [
                'checkBox',
                'dependent_on' => [
                    'no_vat_type' => [
                        'onValue'=>[
                            ['disable', 'for_values' => POPDVBillLists::$U_2_Cb]
                        ]
                    ],
                ]
            ];
        }
        else
        {
            $_discount = 'hidden';
            $is_discount_item = 'hidden';
        }
        
        return [
            'default' => [
                'type' => 'generic',
                'ajaxlong' => $ajaxlong,
                'columns' => [
                    'bill_id' => 'hidden',
//                    'confirmed' => 'status',
                    'confirmed' => 'hidden',
                    'title' => $title,
                    'is_discount_item' => $is_discount_item,
//                    'quantity' => $quantity,
                    'quantity' => $JCI_hide_numeric,
                    'unit_id' => $unit,
                    'value_per_unit' => $JCI_hide_numeric,
                    'discount' => $JCI_hide_numeric,
                    'value' => 'numberField',
                    'vat' => $vat,
                    'value_vat' => $value_vat,
                    'no_vat_type' => $no_vat_type,
                    'vat_refusal' => $vat_refusal,
                    'customs_vat_bill_id' => $customs_vat_bill_id,
                    'customs_vat_bill_item_id' => $customs_vat_bill_item_id,
                    'cost_type_id' => $cost_type_id,
                    'cost_location_id' => $cost_location_id,
                ]
            ]
        ];
    }

    public function droplist($key, $relName='', $filter=null)
    {
        $owner = $this->owner;
        switch ($key)
        {
            case 'unit_id': return MeasurementUnit::model()->getModelDropList();
            case 'unit': return MeasurementUnit::model()->getModelDropList();
            case 'vat':         return BillItem::$vat_types;
            case 'internal_vat':return BillItem::$vat_types;
            case 'vat_refusal': return ['0' => 'Ne', '1' => 'Da'];;
            case 'bill_item_type': return BillItem::$bill_item_types;
            case 'no_vat_type': return POPDVBillLists::$names; //postavlja se u zavisnoj formi
            default: return parent::droplist($key, $relName, $filter);
        }
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default: return array(
                'columns'=>array(
                    'order' => ['edit' => 'text','min_width' => '10px','max_width'=>'40px'], 
                    'title' => array(
                        'edit' => 'text',
                    ),
                    'T' => ['min_width' => '20px','max_width'=>'20px'],
                    'unit.name' => ['min_width' => '15px','max_width'=>'40px'],
                    'value_per_unit' => array(
//                        'edit'=>'numberField',
                    ),
                    'discount' => array(
//                        'edit'=>'numberField',
                        'min_width' => '55px',
                        'max_width'=>'55px'
                    ),
                    'quantity' => array(
//                        'edit' => 'numberField',
                    ),
                    'vat' => array(
//                        'edit' => 'dropdown',
                        'min_width' => '62px',
                        'max_width'=>'62px'
                    ),
                    'value' => array(
//                        'edit' => 'numberField',
                    ),
                    'value_vat' => array(
//                        'edit' => 'numberField',
                    ),
                    'vat_refusal',
                    'value_total',
                    'cost_type',// => ['edit'=>'relation'],
                    'cost_location',// => ['edit'=>'relation'],
                    'popdv_display' => [
                        'dropdown',
                        'min_width' => '200px'
                    ],
                    'connected_customs_bill'
                ),
                'sums' => array(
                    'value',
                    'value_vat',
                    'value_total'
                ),
                'sums_function'=>'sumFunc'
            );
//            default: return parent::guiTableSettings($type);
        }
    }
    
    public function sumFunc($criteria)
    {        

        //NOVO  - pretpostavka da nece biti mnogo stavki
        $items = BillItem::model()->findAll($criteria);
        $sum_item = new BillItem();
        foreach ($items as $_item)
        {
            $sum_item->value += $_item->value;
            $sum_item->value_vat += $_item->value_vat + $_item->value_vat_customs;
        }  
        return $sum_item;
    }
    
}
