<?php

class MiddleCurrencyRateGUI extends SIMAActiveRecordGUI
{
    public function columnLabels()
    {
        return array(
            'currency' => Yii::t('AccountingModule.MiddleCurrencyRate', 'Currency'),
            'currency_id' => Yii::t('AccountingModule.MiddleCurrencyRate', 'Currency')
        ) + parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return Yii::t('AccountingModule.MiddleCurrencyRate', $plural?'MiddleCurrencyRates':'MiddleCurrencyRate');
    }
    
    public function modelForms()
    {
        return array(
            'default' => array(
                'type' => 'generic',
                'columns' => array(
                    'currency_id' => array('searchField', 'relName' => 'currency', 'add_button'=>true),
                    'date' => 'datetimeField',
                    'value' => ['textField', 'placeholder'=>Yii::t('AccountingModule.MiddleCurrencyRate','LeaveEmptyValue')]
                )
            )
        );
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default:
                return [
                    'columns' => array(
                        'currency',
                        'date',
                        'value'
                    ),
                    'order_by' => array('date'),
                ];
        }
    }
}

