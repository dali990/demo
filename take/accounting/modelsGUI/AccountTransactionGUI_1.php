<?php

class AccountTransactionGUI extends SIMAActiveRecordGUI
{

    public function columnLabels()
    {
        return array(
            'account_document.account_order'=>'Nalog za knjiženje',
            'account_document_id' => 'Proknjiženi dokument',
            'account_document' => 'Proknjiženi dokument',
            'debit' => 'Duguje',
            'credit' => 'Potražuje',
            'saldo' => 'Saldo',            
            'account' => 'Konto',
            'account_booked' => 'Konto',
            'account_debit' => 'Konto duguje',
            'account_credit' => 'Konto potrazuje',
            'account_debit_display' => 'Konto duguje',
            'account_credit_display' => 'Konto potrazuje',
            'account_id' => 'Konto',
            'account_order_order' => '#',
            'comment' => Yii::t('BaseModule.Common','Description')
        ) + parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return 'Stav naloga za knjiženje';
    }
    
    public function columnDisplays($column)
    {
        $owner = $this->owner;
        switch ($column)
        {
            case 'account':                 return $this->showAccount();
            
            case 'debit':                   return (abs($owner->debit)>0)?parent::columnDisplays($column):'';
            case 'account_debit':           //return (abs($owner->debit)>0)?$this->showAccount():'';
            case 'account_debit_display':   return (abs($owner->debit)>0)?$this->showAccount():'';
            
            case 'credit':                  return (abs($owner->credit)>0)?parent::columnDisplays($column):'';
            case 'account_credit':          //return (abs($owner->credit)>0)?$this->showAccount():'';
            case 'account_credit_display':  return (abs($owner->credit)>0)?$this->showAccount():'';
                
            case 'account_debit_display_just_number':
                if (abs($owner->debit)>0)
                {
                    if ($owner->account->hasMaxCode)
                    {
                        return $owner->account->code;
                    }
                    else
                    {
                        return $owner->account->parent->code;
                    }
                }
                else
                {
                    return '';
                }
            case 'account_credit_display_just_number':
                if (abs($owner->credit)>0)
                {
                    if ($owner->account->hasMaxCode)
                    {
                        return $owner->account->code;
                    }
                    else
                    {
                        return $owner->account->parent->code;
                    }
                }
                else
                {
                    return '';
                }
            case 'account_just_name':
                $model_link = $owner->account->LinkedModel;
                if (is_null($model_link))
                {
                    return $owner->account->name;
                }
                else
                {
                    return $model_link->DisplayName;
                }
            default: return parent::columnDisplays($column);
        }
    }
    
    public function showAccount()
    {
        $owner = $this->owner;
        $_id = $owner->id;
        if ($owner->account_document->isBooked)
        {
            $_options = '';
        }
        else if ($owner->account->hasConnection())
        {
            $_options = ' <span class="sima-icon _delete _16"'
                . 'onclick="sima.accounting.accountRemoveLink('.$_id.')"'
                . '></span>';
        }
        else
        {
            $_options = ' <span class="sima-icon _add _16"'
                . 'onclick="sima.accounting.accountAddLink('.$_id.')"'
                . '></span>';
        }
        $_display_text = $owner->account->recalcName(true);
        return $owner->account->DisplayHtml(['display_text' => $_display_text]).$_options;
    }
    
    public function modelViews()
    {
        return array(
        );
    }

    public function modelForms()
    {
        $owner = $this->owner;
        $account_filter = [];
        if (!isset($owner->account_document_id))
        {
            $account_filter = ['year'=>['ids'=>Year::get()->id]];
        }
        else if (isset($owner->account_document->account_order))
        {
            $account_filter = ['year'=>['ids'=>$owner->account_document->account_order->year_id]];
        }
        
        
        return array(
            'default' => array(
                'type' => 'generic',
                'columns' => array(
                    'account_document_id' => array('searchField','relName'=>'account_document','disabled'=>'disabled'),
                    'account_id' => array('searchField','relName'=>'account','filters'=>$account_filter),
                    'debit' => 'numberField',
                    'credit' => 'numberField',
                    'account_order_order' => 'textField',
                    'comment' => 'textField'
                )
            )
        );
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            case 'account_order_disabled':
            case 'account_order_booked': return array(
                'columns' => array(
                    'account_order_order' => ['min_width' => 20, 'max_width' => 30],
                    'account_debit_display' => ['min_width' => 150, 'default_width' => 250],
                    'account_credit_display' => ['min_width' => 150, 'default_width' => 250],
                    'debit' => ['min_width' => 90, 'max_width' => 115],
                    'credit' => ['min_width' => 90, 'max_width' => 115],
                    'saldo' => ['min_width' => 90, 'max_width' => 115],
                    'comment' => ['default_width' => 300],
                ),
                'sums' => array(
                    'debit', 'credit','saldo'
                ),
                'sums_function'=>'sumFunc'
            );
            case 'canceled_account_order':
            case 'account_order': return array(
                'columns' => array(
                    'account_order_order' => ['edit' => 'text', 'min_width' => 20, 'max_width' => 30],
                    'account_debit_display'=>[
                        'edit' => ['relation', 'relName' => 'account_debit'],
                        'min_width' => 150, 
                        'default_width' => 250
                    ],
                    'account_credit_display'=>[
                        'edit' => ['relation', 'relName' => 'account_credit'],
                        'min_width' => 150, 
                        'default_width' => 250
                    ],
                    'debit' =>  ['edit'=>'numberField','min_width' => 90, 'max_width' => 115],
                    'credit' => ['edit'=>'numberField','min_width' => 90, 'max_width' => 115],
                    'saldo' =>  ['min_width' => 90, 'max_width' => 115],
                    'comment' => ['edit'=>'text','default_width' => 300], 
                ),
                'sums' => array(
                    'debit', 'credit','saldo'
                ),
                'sums_function'=>'sumFunc'
            );
            case 'account_card': return array(
                'columns' => array(
//                    'account',
                    'account_order_order' => ['min_width' => '20px', 'max_width' => '30px'], 
                    'account_document.account_order' => ['max_width' => '120px','min_width' => '120px'], 
                    'account_document.account_order.date' => ['max_width' => '80px','min_width' => '80px'], 
                    
                    'debit'=>array(
                        'edit'=>'text',
                        'min_width' => '90px', 'max_width' => '115px'
                     ), 
                    'credit'=>array(
                        'edit'=>'text',
                        'min_width' => '90px', 'max_width' => '115px'
                     ), 
                    'saldo' => ['min_width' => '90px', 'max_width' => '115px'],
                    'comment'=>array(
                        'edit'=>'text',
                     ), 
                    'account_document', 
                    
                ),
                'filters'=>array(
                    'account_booked'
                ),
                'sums' => array(
                    'debit', 'credit', 'saldo'
                ),   
                'sums_function'=>'sumFunc'
            );
            default:
                return array(
                    'columns' => array(
                        'account_order_order' => ['min_width' => '20px', 'max_width' => '30px'], 
                        'account_document.account_order', 
                        'debit'=>array(
                            'edit'=>true,
                            'type'=>'text',
                            'min_width' => '90px', 'max_width' => '115px'
                         ),
                        'credit'=>array(
                            'edit'=>true,
                            'type'=>'text',
                            'min_width' => '90px', 'max_width' => '115px'
                         ),
                        'account_document', 
                        'comment', 
                    ),
                );
        }
    }
    
    
    public function sumFunc($criteria, $display_scopes)
    {
        $owner = $this->owner;
        $uniq = SIMAHtml::uniqid();
        $temp_criteria = new SIMADbCriteria();
        $temp_criteria->select = "sum(debit) as debit, sum(credit) as credit";

        $criteria->mergeWith($temp_criteria);
        $criteria->order = '';
        $sum_result = $owner->resetScope()->findAll($criteria);
        if (count($sum_result)==0) 
        {
            return new AccountTransaction();
        }
        $sum_result[0]->saldo = $sum_result[0]->debit - $sum_result[0]->credit;
        return $sum_result[0];
        
    }

}