<?php

class BillInGUI extends BillGUI
{
    public function columnLabels()
    {
        return array(
            'warehouse_receives' => 'Prijemnice',
            'cost_type' => Yii::t('AccountingModule.CostType','CostType'),
            'cost_type_id' => Yii::t('AccountingModule.CostType','CostType'),
        ) + parent::columnLabels();
    }

    public function modelLabel($plural = false)
    {
        return 'Ulazni račun';
    }
    
    public function modelForms()
    {
        $owner = $this->owner;
        $cost_type_id = [
            'relation', 
            'relName' => 'cost_type', 
            'add_button'=>[
                'action'=>'accounting/costTypes',
                'params' => [
                    'get_params' => [
                        'cost_or_income' => CostType::$TYPE_COST
                    ]
                ]
            ],
            'filters' => [
                'cost_or_income' => CostType::$TYPE_COST
            ],
            'dependent_on'=>[                        
                'income_date'=>[
                    'onValue' => ['trigger', 'func' => ['sima.accountingMisc.billCostTypeDependentOnBillIncomeDateInForm']],
                    'onEmpty' => ['disable']
                ]                        
            ]
        ];
        if ($owner->cost_type_mixed === true)
        {
            $cost_type_id['disabled'] = 'disabled';
        }
        
        $cost_location_id = ['relation', 'relName' => 'job_order'];
        if ($owner->cost_location_mixed === true)
        {
            $cost_location_id['disabled'] = 'disabled';
        }
        
        $vat_refusal = ['checkBox'];
        if (is_null($owner->vat_refusal))
        {
            $vat_refusal['disabled'] = 'disabled';
        }
        
        return array(
            'default' => array(
                'type' => 'generic',
                'columns' => array(
                    'bill_number' => 'textField',
//                    'belongs'=>'belongs',
                    'partner_id' => ['searchField', 'relName' => 'partner', 'add_button' => ['action'=>'simaAddressbook/index']],
                    'partner_bank_account_id'=>['searchField', 'relName' => 'partner_bank_account', 'dependent_on'=>[                        
                        'partner_id'=>[
                            'onValue'=>[
                                'enable',
                                ['condition', 'model_filter' => 'partner.ids']
                            ],
                            'onEmpty'=>'disable'
                        ]                        
                    ]],
                    'income_date'=> array('datetimeField'),
                    'cost_type_id' => $cost_type_id,
                    'cost_location_id' => $cost_location_id,
                    'traffic_date'=> array('datetimeField','placeholder' => $this->trafficDatePlaceholder()),
                    'traffic_location_id'=> ['relation', 'relName' => 'traffic_location', 'add_button' => true],
                    'payment_date'=> ['datetimeField', 'placeholder' => Yii::t('AccountingModule.Bill', 'Leave empty if same as income_date')],
                    'bound_currency_id' => array('searchField','relName'=>'bound_currency'),
                    'lock_amounts' => 'checkBox',
                    'amount' => self::$locked_bill_values_form_type,
                    'base_amount0' => self::$locked_bill_values_form_type,
                    'base_amount10' => self::$locked_bill_values_form_type,
                    'base_amount20' => self::$locked_bill_values_form_type,
                    'vat10' => self::$locked_bill_values_form_type,
                    'vat20' => self::$locked_bill_values_form_type,
                    'interest' => self::$locked_bill_values_form_type,
                    'rounding' => self::$locked_bill_values_form_type,
                    'value_customs' => self::$locked_bill_values_form_type,
                    'responsible_id' => array('dropdown', 'empty'=>'Izaberi'),
                    'vat_refusal' => $vat_refusal,
                    'vat_in_diferent_month_id' => ['relation', 'relName' => 'vat_in_diferent_month'],
                    'original'=>array('checkBox'),
                    'call_for_number' => 'textField',
                    'call_for_number_modul' => 'textField',
                    
                    'comment'=>'textArea'
                )
            )
        );
    }
    
    public function modelViews()
    {
        return array(
            'full_info',// => array('params_function' => 'fullInfoParams'),
            'payed_with' => ['params_function' => 'payedWithParams', 'with' => ['file']],
            //ostaje samo za tab za stare stavke racuna
            'bill_items' => ['params_function' => 'billItemsParams', 'with' => ['file']],
            //ovo je sad samo ostalo vezano za slovbodne prijemnice
            'bill_items_not_confirmed' => ['params_function' => 'billItemsNotConfirmedParams', 'with' => ['file']],
            'inFile',
            'warehouseTransfersListForm', //=>array('origin'=>'Bill'),
            'warehouseTransfersList'//=>array('origin'=>'Bill')
//            'tabTemplates'=>array('with'=>array('templated_file'),'style'=>'border: 1px solid;')  //.creator
        ) + parent::modelViews();
    }   
    
    public function payedWithParams()
    {
        $owner = $this->owner;
        $base_bill_model = Bill::model()->findByPk($owner->id);
        
        $base_html = Yii::app()->controller->renderModelView($base_bill_model,'payed_with');
        
        $display_pay_button = $owner->unreleased_amount > 0 && ($owner->isBill || $owner->isPreBill);

        $onclick_array = ['sima.accounting.BillPay',
            $owner->id,
            $owner->partner_id,
            $owner->partner_bank_account_id,
            $owner->call_for_number,
            $owner->call_for_number_modul,
            $owner->bill_number,
            $owner->unreleased_amount,
            Yii::app()->configManager->get('accounting.default_payment_type',false),
            Yii::app()->configManager->get('accounting.default_payment_code',false)
        ];

        
        return [
            'base_html' => $base_html,
            'display_pay_button' => $display_pay_button,
            'onclick_array' => $onclick_array
        ];
    }
    
    public function billItemsParams()
    {
        $owner = $this->owner;
//        $warehouse_items = [];
//        foreach ($owner->warehouse_bill_items as $w_bill_item)
//        {
//           
//        }
        return [
            'warehouse_transfers' => $owner->warehouse_transfers,
            'service_bi' => $owner->services_bill_items,
            'fixed_assets_bi' => $owner->fixed_assets_bill_items,
            
        ];
    }
    public function billItemsNotConfirmedParams()
    {
        $owner = $this->owner;
        
        $traffic_date = new SIMADateTime($owner->traffic_date);
        $traffic_year = Year::get($traffic_date->format("Y"));

        $connected_transfers = WarehouseReceiving::model()->count([
            'model_filter' => [
                'bill'=>['ids'=>$owner->id]
            ]
        ]);
        $free_transfers = WarehouseReceiving::model()->count([
            'model_filter' => [
                'warehouse_from' => [
                    'partner'=>[
                        'ids'=>$owner->partner_id,    
                    ]
                ],
                'bill'=>['ids'=>'null'],
                'filter_scopes' => ['inYear' => $traffic_year->id]
            ]
        ]);
        
        return [
            'connected_transfers' => $connected_transfers,
            'free_transfers' => $free_transfers
        ];
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            case 'confirm1':
                return array(
                    'columns'=>array(
                        'bill_number', 'partner', 'traffic_date', 'traffic_location', 'income_date', 'payment_date', 'responsible_id',
                        'amount', 'amount_in_currency', 'base_amount', 'vat', 
                        'unreleased_amount' => ['min_width' => '180px'], 
                        'unreleased_amount_in_currency', 'belongs',
                        'cost_location', 'file.filing', 'comment', 'cost_type'
                    ),
                    'sums' => array('amount','base_amount','unreleased_amount','vat'),
                    'order_by' => array('amount','base_amount','unreleased_amount','vat','income_date','payment_date'),
                );
            case 'inPartner':
                return array(
                    'columns'=>array(
                        'bill_number', 'traffic_date', 'traffic_location', 'income_date', 'payment_date', 'amount', 'amount_in_currency', 
                        'unreleased_amount' => ['min_width' => '180px'], 
                        'unreleased_amount_in_currency',
                        'cost_location', 'comment', 'file.filing', 'cost_type'
                    ),
                    'sums' => array('amount','base_amount','unreleased_amount','vat'),
                    'order_by' => array('amount','base_amount','unreleased_amount','vat'),
                );
            case 'inJobFinanse': return array(
                    'columns'=>array(
                        'bill_number', 'partner', 'traffic_date', 'traffic_location', 
                        'income_date', 'payment_date', 
                        'amount', 'amount_in_currency', 
                        'unreleased_amount' => ['min_width' => '180px'], 
                        'unreleased_amount_in_currency', 'vat',
                        'comment', 'belongs'
                    ),
                    'sums' => array('amount','base_amount','unreleased_amount','vat'),
                    'order_by' => array('bill_number','amount','base_amount','unreleased_amount', 'traffic_date','income_date','payment_date'),
                );
            case 'inBillProposals': return array(
                    'columns'=>array( 
                        'bill_number', 'amount', 'amount_in_currency', 
                        'unreleased_amount' => ['min_width' => '180px'], 
                        'unreleased_amount_in_currency', 'unreleased_percent_of_amount', 'traffic_date', 'traffic_location', 'income_date',
                        'base_amount0', 'base_amount10', 'base_amount20', 'vat10', 'vat20'
                    ),
                    'sums' => array('amount','unreleased_amount'),
                    'order_by' => array('bill_number','amount','base_amount','unreleased_amount', 'traffic_date','income_date','payment_date'),
                );
            default:
                return array(
                    'columns'=>array(
                        'bill_number', 'partner', 'traffic_date', 'traffic_location', 'income_date', 'payment_date', 'amount', 'amount_in_currency', 
                        'unreleased_amount' => ['min_width' => '180px'],
                        'unreleased_amount_in_currency',
                        'belongs', 'cost_location', 'comment', 'file.filing.filing_number','interest', 'cost_type',
                        'file.account_document.account_order',
                        'file.account_document.account_order.date'
                    ),
                    'filters'=>array(
                        'year'
                    ),
                    'sums' => array('amount', 'amount_in_currency','base_amount',
                        'unreleased_amount','unreleased_amount_in_currency','vat'),
                    'sums_function'=>'sumFunc',
                    'order_by' => array('amount','base_amount','unreleased_amount','vat', 'income_date', 'payment_date'),
                );
        }
    }
}