<?php

class CostTypeGUI extends SIMAActiveRecordGUI
{

    public function columnLabels()
    {
        return array(
            'defined_in_years'=>'Definisano u godinama',
            'cost_or_income' => Yii::t('AccountingModule.CostType','cost_or_income'),
            'book_by_cost_location' => Yii::t('AccountingModule.CostType','book_by_cost_location'),
            'account' => Account::model()->modelLabel(),
            'year_id' => Year::model()->modelLabel(),
        )+parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return Yii::t('AccountingModule.CostType',$plural?'CostIncomeTypes':'CostIncomeType');
    }

//    public function modelViews()
//    {
//        return array(
//            'basicInfoTitle',
//            'basicInfo',
//            'current_materials_status'=>array('params_function'=>'viewParamsCurrentMaterialsStatus')
////            'leaf'=>array(
//////                'style'=>'border: 1px solid;'
////                ),
////            'default_edit'
////            'tabTemplates'=>array('with'=>array('templated_file'),'style'=>'border: 1px solid;')  //.creator
//        );
//    }
    
   public function columnDisplays($column) 
    {
        $owner = $this->owner;
        switch ($column)
        {
            case 'cost_or_income':
                return CostType::$cost_or_income[$owner->cost_or_income];
            case 'defined_in_years':                
                $return = '';                
                foreach ($owner->accounts as $account) 
                {
                    $return .= $account->year->year . ' | ';
                }
                return substr($return, 0, -3);
            case 'account':
                if (!empty($owner->account_id))
                {
                    $_conn = CostTypeToAccount::model()->findByModelFilter([
                        'cost_type' => ['ids' => $owner->id],
                        'account' => ['ids' => $owner->account_id]
                    ]);
                    $_conn_str = is_null($_conn)?'':SIMAHtml::modelFormOpen($_conn);
                    return parent::columnDisplays($column).$_conn_str;
                }
                else
                {
                    return parent::columnDisplays($column).' NEMA';
                }
                
            default: return parent::columnDisplays($column);
        }
    }
    
    public function modelForms()
    {
        $owner = $this->owner;
        

        if (!empty($owner->year_id))
        {
        
            $filter = [
                'year' => [
                    'ids' => $owner->year_id
                ],
                'display_scopes' => [
                    'withDCCurrents' => [$owner->year_id, ['hide_zero_accounts' => false]],
                    'byName'
                ]
            ];
            $account_add_button_params = [
                'init_data' => [
                    'Account' => [
                        'year_id' => [
                            'disabled',
                            'init' => $owner->year_id
                        ]
                    ]
                ]
            ];
            
            $account_id = [
                'relation', 
                'relName' => 'account', 
                'dependent_on' =>[
                    'year_id' => [
                        'onValue' => [
                            'enable',
                            ['condition', 'model_filter' => 'year.ids']
                        ],
                        'onEmpty' => [
                            'disable',
                        ]
                    ]
                ],   
                'filters' => $filter,
                'add_button' => [
                    'filter' => $filter,
                    'params' => [
                        'add_button' => $account_add_button_params
                    ]
                ]
            ];
            
            
            
            $columns = [
                'name'=>'textField',
                'cost_or_income' => 'hidden',
                'year_id' => ['relation','relName' => 'year', 'disabled' => 'disabled'],
                'account_id' => $account_id,
                'book_by_cost_location' => 'checkBox',
                'description'=>'textArea',
            ];
        }
        else
        {
            $columns = [
                'name'=>'textField',
                'cost_or_income' => 'hidden',
                'book_by_cost_location' => 'checkBox',
                'description'=>'textArea',
            ];
        }
        
        return array(
            'default'=>array(
                'type'=>'generic',
                'columns' => $columns
            )
        );
    }
    
    public function droplist($key, $relName='', $filter=null)
    {
        $owner = $this->owner;
        switch ($key)
        {
            case 'cost_or_income': return CostType::$cost_or_income;                
            default: return parent::droplist($key, $relName, $filter);
        }
    }
    
    
    public function guiTableSettings($type) 
    {
        switch ($type) 
        {
            case 'in_year': return [
                'columns'=>[
                    'name', 'description', 'account' => ['min_width' => 300], 'book_by_cost_location'
                ]
            ];
            case 'not_in_year': return [
                'columns'=>[
                    'name', 'description', 'defined_in_years', 'book_by_cost_location'
                ]
            ];
            default:
                return [
                    'columns'=>[
                        'name', 'cost_or_income', 'description', 'defined_in_years', 'book_by_cost_location'
                    ]
                ];
        }
    }
    
}