<?php
/**
 * Description of PaymentConfgiruationGUI
 *
 * @author goran-set
 */

class PaymentConfigurationGUI extends SIMAActiveRecordGUI
{
    public function columnLabels()
    {
        return array(
            'name' => Yii::t('AccountingModule.PaymentConfiguration', 'Name'),
            'type' => Yii::t('AccountingModule.PaymentConfiguration', 'Type'),
            'separator'=> Yii::t('AccountingModule.PaymentConfiguration', 'Separator'),
            'account_number_col' => Yii::t('AccountingModule.PaymentConfiguration', 'AccountNumberCol'), 
            'account_owner_col' => Yii::t('AccountingModule.PaymentConfiguration', 'AccountOwnerCol'), 
            'date_col' => Yii::t('AccountingModule.PaymentConfiguration', 'DateCol'),
            'amount_col' => Yii::t('AccountingModule.PaymentConfiguration', 'AmountCol'),
            'amount2_col' => Yii::t('AccountingModule.PaymentConfiguration', 'Amount2Col'),
            'description' => Yii::t('AccountingModule.PaymentConfiguration', 'Description'),
            'payment_code_col' => Yii::t('AccountingModule.PaymentConfiguration', 'PaymentCodeCol'),
            'call_for_number_debit_col' => Yii::t('AccountingModule.PaymentConfiguration', 'CallForNumberDebitCol'),
            'call_for_number_credit_col' => Yii::t('AccountingModule.PaymentConfiguration', 'CallForNumberCreditCol'),
            'path' => Yii::t('AccountingModule.PaymentConfiguration', 'PathXML'),
            'bank_transaction_id' => Yii::t('AccountingModule.PaymentConfiguration', 'BankTransactionId')
        ) + parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return Yii::t('AccountingModule.PaymentConfiguration', 'PaymentConfiguration');
    }

    public function columnDisplays($column)
    {
        $owner = $this->owner;

        switch ($column)
        {
            case 'type':
                $result = "Netačan podatak! ".$owner->type;
                $type_data = $this->typeData();
                if(isset($type_data[$owner->type]))
                {
                    $result = $type_data[$owner->type];
                }
                return $result;
            default: return parent::columnDisplays($column);
        }
    }
    
    public function modelForms()
    {
        return array(
            'default'=>array(
                'type'=>'generic',
                'columns'=>array(
                    'id'=>'hidden',
                    'name'=>'textField',
                    'type'=>'dropdown',
                    'path'=>'textField',
                    'separator' => 'textField',
                    'decimal_separator' => 'textField',
                    'account_number_col'=>'textField', 
                    'account_owner_col'=>'textField', 
                    'date_col'=>'textField',
                    'amount_col'=>'textField',
                    'amount2_col'=>'textField',
                    'payment_code_col' => 'textField',
                    'call_for_number_debit_col' => 'textField',
                    'call_for_number_credit_col' => 'textField',
                    'bank_account_id' => 'textField',
                    'bank_account_date' => 'textField',
                    'bank_account_amount' => 'textField',
                    'bank_statement_number' => 'textField',
                    'bank_transaction_id' => 'textField',
                    'export_format' => 'textField',
                    'description'=>'textArea'
                )
            ),
        );
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default: 
                return [
                    'columns' => [
                        'name','type','separator','decimal_separator','path', 'account_number_col', 
                        'payment_code_col',
                        'call_for_number_debit_col', 'call_for_number_credit_col',
                        'account_owner_col', 'date_col', 'amount_col','amount2_col',
                        'bank_account_id', 'bank_account_date', 'bank_account_amount', 'bank_statement_number', 'bank_transaction_id', 'description'
                    ]
                ];
        }
    }
    
    public function droplist($key, $relName='', $filter=null) {
        switch ($key) {
            case 'type': return $this->typeData();
            default: return parent::droplist($key, $relName, $filter);
        }
    }
    
    private function typeData()
    {
        return [
            0 => 'CSV fajl',
            1 => 'XML fajl',
            2 => 'ErsteWeb',
            PaymentConfiguration::$TYPE_RAIFFEISEN => 'Raiffeisen',
            PaymentConfiguration::$TYPE_HALK_BANK => 'HalkBank',
            PaymentConfiguration::$TYPE_RAIFFEISEN_DEVIZNI => 'Raiffeisen devizni',
            PaymentConfiguration::$TYPE_SOGE_XML => 'SOGE xml',
            PaymentConfiguration::$TYPE_OTP_DEVIZNI_XML => 'OTP devizni',
        ];
    }
}
