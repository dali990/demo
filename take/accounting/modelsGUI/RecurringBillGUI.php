<?php

class RecurringBillGUI extends SIMAActiveRecordDisplayGUI
{
    public function columnLabels()
    {
        return array(
            'name' => Yii::t('AccountingModule.RecurringBill', 'Name'),
            'description' => Yii::t('AccountingModule.RecurringBill', 'Description'),
            'call_for_number' => Yii::t('AccountingModule.RecurringBill', 'CallForNumber'),
            'partner_id' => Yii::t('AccountingModule.RecurringBill', 'Partner'),
            'partner' => Yii::t('AccountingModule.RecurringBill', 'Partner'),
        )+parent::columnLabels();
    }
    
    public function modelLabel($is_plural=false)
    {
        return Yii::t('AccountingModule.RecurringBill', $is_plural ? 'RecurringBills' : 'RecurringBill');
    }
    
    public function modelViews()
    {
        return array(
            'basic_info', 
            'full_info' => ['params_function' => 'params_full_info', 'html_wrapper' => null],
            'full_info_solo' => ['html_wrapper' => null],
            'title_sums'
        ) + parent::modelViews();
    }  
    
    protected function params_full_info()
    {
        $owner = $this->owner;
        $payments = Payment::model()->recently()->findAllByAttributes([
            'recurring_bill_id' => $owner->id
        ]);
        return [
            'model' => $owner,
            'payments' => $payments
        ];
    }
    
    public function columnDisplays($column)
    {
        $owner = $this->owner;
        switch ($column)
        {
            case 'debt': return SIMAHtml::money_format($owner->calcDebt());
            default: return parent::columnDisplays($column);
        }
    }
    
    public function modelForms()
    {
        return array(
            'default' => array(
                'type' => 'generic',
                'columns' => array(
                    'name' => 'textField',
                    'call_for_number' => 'textField',
                    'partner_id' => ['relation', 'relName' => 'partner', 'add_button'=>['action'=>'simaAddressbook/index']],
                    'description'=>'textArea'
                )
            )
        );
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default: return array(
                'columns'=>array(
                    'name', 'call_for_number', 'partner', 'description'
                )
            );
        }
    }
}
