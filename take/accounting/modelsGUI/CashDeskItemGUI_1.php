<?php

class CashDeskItemGUI extends SIMAActiveRecordGUI
{

    public function columnLabels()
    {
        return array(
            'text' => 'Opis',
            'cost_type_id' => 'Vrsta troška',
            'cost_type' => 'Vrsta troška',
            'amount' => 'Iznos',
            'cash_desk_order' => 'Nalog za blagajnu',
            'cash_desk_order_id' => 'Nalog za blagajnu',
            'amount_in' => 'Ulaz',
            'amount_out' => 'Izlaz',
            'cash_desk_order.display_name' => 'Nalog',
            'cost_location_id' => Yii::t('AccountingModule.CashDeskItem', 'CostLocation'),
            'cost_location' => Yii::t('AccountingModule.CashDeskItem', 'CostLocation'),
            'bill_id' => Yii::t('AccountingModule.CashDeskItem', 'Bill'),
            'bill' => Yii::t('AccountingModule.CashDeskItem', 'Bill'),
        ) + parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return 'Stavka blagajne';
    }

    public function columnDisplays($column)
    {
        $owner = $this->owner;
        switch ($column)
        {
            default: return parent::columnDisplays($column);
        }
    }

    public function modelViews()
    {
        return array(
        );
    }

    public function modelForms()
    {
        $owner = $this->owner;
        
        $cost_type_filter = [];
        if (!empty($owner->cash_desk_order_id))
        {
            $cost_type_filter = [
                'cost_or_income' => $owner->isCashDeskOrderPayin() ? CostType::$TYPE_INCOME : CostType::$TYPE_COST
            ];
        }
        
        return array(
            'default' => array(
                'type' => 'generic',
                'columns' => array(
                    'text' => 'textField',
                    'amount' => 'numberField',
                    'cost_type_id' => array('searchField', 'relName' => 'cost_type', 'add_button' => true, 'filters' => $cost_type_filter),
                    'cost_location_id' => ['relation', 'relName' => 'cost_location', 'add_button' => true],
                    'bill_id' => ['relation', 'relName' => 'bill', 'add_button' => true],
                    'cash_desk_order_id' => 'hidden',
                )
            )
        );
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            case 'cash_desk_log': return [
                'columns' => [
                    'cash_desk_order.display_name',
                    'text', 
                    'amount_in', 
                    'amount_out', 
                    'cost_type',
                    'cost_location',
                    'bill'
                ],
            ];
            default: return [
                'columns' => [
                    'text' => ['edit'=>'text'], 
                    'amount' => ['edit'=>'text'], 
                    'cost_type' => ['edit' => true],
                    'cost_location' => ['edit' => true],
                    'bill' => ['edit' => true],
                ],
            ];
        }
        
    }

}