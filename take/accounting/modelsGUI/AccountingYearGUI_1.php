<?php

class AccountingYearGUI extends SIMAActiveRecordGUI
{

    public function columnLabels()
    {
        return array(
            'year' => 'Godina',
            'account_layout_law' => 'Zakon',
            'account_layout_law_id' => 'Zakon',
            'start_booking_order'    => 'Nalog za pocetno stanje',
            'start_booking_order_id' => 'Nalog za pocetno stanje',
            'profit_calc_booking_order'    => 'Nalog za obracun dobiti',
            'profit_calc_booking_order_id' => 'Nalog za obracun dobiti',
            'finish_booking_order'    => 'Nalog za zavrsno knjizenje',
            'finish_booking_order_id' => 'Nalog za zavrsno knjizenje',
            'ios_date' => Yii::t('AccountingModule.Account','IosGenerationDate'),
            'kpr_next_count' => 'KPR - sledeci broj',
            'kir_next_count' => 'KIR - sledeci broj',
            'booking_materials_type' => Yii::t('AccountingModule.AccountingYear','BookingMaterialsType')
        ) + parent::columnLabels();
    }

    public function modelLabel($plural = false)
    {
        return 'Finansijska godina';
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default: return array(
                'columns' => array(
                    'year','account_layout_law', 
                    'start_booking_order',
                    'profit_calc_booking_order',
                    'finish_booking_order',
                    'ios_date',
                )
            );
        }
    }
    
    public function modelForms()
    {
        $owner = $this->owner;
        if (empty($owner->id))
        {
            return [
                'default' => array(
                    'type' => 'generic',
                    'columns' => array(
                        'year' => 'textField',
                        'account_layout_law_id' => array('searchField','relName'=>'account_layout_law'),
                        'ios_date'=> ['datetimeField',
                            'dependent_on' => [
                                'year' => [
                                    'onValue'=>[
                                        ['trigger', 'func' => ['sima.accounting.selectYear']],
                                    ],
                                ],
                            ]
                        ],
                        'kpr_next_count' => 'numberField',
                        'kir_next_count' => 'numberField',
                        'booking_materials_type' => 'dropdown',
                    )
                )
            ];
        }
        else
        {
            $filters = [];
            if (isset($owner->base_year))
            {
                $filters = [
                    'year' => [
                        'ids' => $owner->base_year->id
                    ]
                ];
            }
            return [
                'only_ios_date'=>[
                    'type' => 'generic',
                    'columns' => [
                        'ios_date'=>'datetimeField',
                    ]
                ],
                'default' => array(
                    'type' => 'generic',
                    'columns' => array(
                        'account_layout_law_id' => array('searchField','relName'=>'account_layout_law'),
                        'start_booking_order_id' => array('searchField','relName'=>'start_booking_order','filters' => $filters, 'add_button' => true),
                        'profit_calc_booking_order_id' => array('searchField','relName'=>'profit_calc_booking_order','filters' => $filters, 'add_button' => true),
                        'finish_booking_order_id' => array('searchField','relName'=>'finish_booking_order','filters' => $filters, 'add_button' => true),
                        'confirmed'=>'status',
                        'ios_date'=> 'datetimeField',
                        'kpr_next_count' => 'numberField',
                        'kir_next_count' => 'numberField',
                        'booking_materials_type' => 'dropdown',
                    )
                )
            ];
        }
    }
            
    public function droplist($key, $relName = '', $filter = null)
    {
        switch ($key)
        {
            case 'booking_materials_type': return AccountingYear::$BOOKING_MATERIALS_TYPES;
            default: return parent::droplist($key, $relName, $filter);
        }
        
    }

}