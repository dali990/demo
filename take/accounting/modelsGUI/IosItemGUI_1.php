<?php

class IosItemGUI extends SIMAActiveRecordGUI
{
    public function modelLabel($plural = false)
    {
        return 'IOS stavke';
    }
    
    public function columnLabels()
    {
        return array(
            'ios_id' => Yii::t('AccountingModule.Account','IOS'),
            'ios' => Yii::t('AccountingModule.Account','IOS'),
            'date' => Yii::t('AccountingModule.Account','IosItemDate'),
//            'transaction_id' => Yii::t('AccountingModule.Account','Transaction'),
//            'transaction' => Yii::t('AccountingModule.Account','Transaction'),
            'bill_id' => Yii::t('AccountingModule.Account','Bill'),
            'bill' => Yii::t('AccountingModule.Account','Bill'),
            'credit' => Yii::t('AccountingModule.Account','Credit'),
            'debit' => Yii::t('AccountingModule.Account','Debit'),
            'saldo' => Yii::t('AccountingModule.Account','Saldo'),
            'payment_date' => Yii::t('AccountingModule.Account','PaymentDate'),
        ) + parent::columnLabels();
    }

//    public function columnDisplays($column)
//    {
//        $owner = $this->owner;
//
//        switch ($column)
//        {
//            case 'account_id': 		return $owner->account->DisplayName;     
//            default: return parent::columnDisplays($column);
//        }
//    }
    
//    public function modelForms()
//    {
//        $owner = $this->owner;
//        return array(
//            'default'=>array(
//                'type'=>'generic',
//                'columns'=>array(
//                    'date'=>'datetimeField',
//                    'ios_id' => [
//                        'relation',
//                        'relName' => 'ios',
//                    ],
//                    'transaction_id' => [
//                        'relation',
//                        'relName' => 'transaction',
////                        'filters'=>[
////                            'scopes'=>'onlyLeafAccounts', // iskljucujuci sva konta koja nisu krajnja
////                            'single_company_accounts'=>$owner->partner_id
////                        ],
////                        'dependent_on'=>[
////                            'ios_date'=>[
////                                'onValue'=>[
////                                    'enable',
////                                    [
////                                        'condition', 
////                                        'model_filter' => 'filter_scopes.byYear'
////                                    ],
////                                ],
////                                'onEmpty' => 'disable'
////                            ]
////                        ],
////                        'filters'=>$account_filter,
//                    ],
//                    'partner_agreed' => 'status',
//                    'partner_agreed_diff'=> 'numberField',
//                )
//            ),
//        );
//    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default: 
                return [
                    'columns'=>[
                        'bill' => ['min_width' => 150],
                        'bill.income_date' => ['min_width' => 90,'max_width' => 90],
                        'bill.payment_date' => ['min_width' => 90,'max_width' => 90],
                        'debit' => ['min_width' => 90,'max_width' => 200],
                        'credit' => ['min_width' => 90,'max_width' => 200],
                        'saldo' => ['min_width' => 90,'max_width' => 200],
                    ],
                    'sums' => [
                        'debit', 'credit'//, 'saldo'
                    ]
                ];
        }
    }
}