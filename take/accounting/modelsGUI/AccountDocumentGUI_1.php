<?php

class AccountDocumentGUI extends SIMAActiveRecordGUI
{

    public function columnLabels()
    {
        return array(
            'account_order'=>'Nalog za knjiženje',
            'account_order_id'=>'Nalog za knjiženje'            ,
            'option_buttons' => Yii::t('BaseModule.Common','Options')
        ) + parent::columnLabels();
    }

    public function modelLabel($plural = false)
    {
        return 'Dokument knjizenja';
    }
    
    public function afterValidateDependencyError(array $foreign_dependencies)
    {
        $allowed_tables = [
            AccountTransaction::model()->tableName().':account_document_id'
        ];

        $parsed_foreign_dependencies = array_diff($foreign_dependencies, $allowed_tables);
        
        parent::afterValidateDependencyError($parsed_foreign_dependencies);
    }
    
    public function modelViews()
    {
        return array(
            'inFile',
            'account_order_span'=>[
                'html_wrapper' => 'span', 
                'params_function' => 'paramsOrderSpan', 
                'with' => ['account_order','canceled_account_order']
            ],
        );
    }
    
    public function vueProp(string $prop, array $scopes = []):array
    {
        $owner = $this->owner;
        switch ($prop)
        {
            case 'VatInMonth': case 'CanceledInMonth':
                $models_update = [$owner->account_order];
                if (isset($this->file->any_bill))
                {
                    $models_update[] = $this->file->any_bill;
                }
                return [$owner->vuePropValue($prop), SIMAMisc::getModelTags($models_update)];
            default: return parent::vueProp($prop, $scopes);          
        }

    }
    
    public function paramsOrderSpan()
    {
        $owner = $this->owner;
        
        if ($owner->isBooked)
        {
            $confirmed_msg = Yii::t('BaseModule.Common','Yes');
            $additional_class = '_pressed' ;
        }
        else
        {
            $confirmed_msg = Yii::t('BaseModule.Common','No');
            $additional_class = '' ;
        }
        if (isset($owner->canceled_account_order) && $owner->canceled_account_order->booked)
        {
            $canceled_confirmed_msg = Yii::t('BaseModule.Common','Yes');
            $canceled_additional_class = '_pressed' ;
        }
        else
        {
            $canceled_confirmed_msg = Yii::t('BaseModule.Common','No');
            $canceled_additional_class = '' ;
        }
        
        if(SIMAMisc::isVueComponentEnabled())
        {
            $confirmed_button = Yii::app()->controller->widget(SIMAButtonVue::class, [
                'title' => $confirmed_msg,
                'additional_classes' => [$additional_class],
                'onclick'=>['sima.accounting.confirmAccountOrder',$owner->account_order->id]
            ],true);
            if (isset($owner->canceled_account_order))
            {
                $canceled_button = Yii::app()->controller->widget(SIMAButtonVue::class, [
                    'title' => $canceled_confirmed_msg,
                    'additional_classes' => [$canceled_additional_class],
                    'onclick'=>['sima.accounting.confirmAccountOrder',$owner->canceled_account_order->id]
                ],true);
            }
            else
            {
                $canceled_button = '';
            }
        }
        else
        {
            $confirmed_button = Yii::app()->controller->widget('SIMAButton', [
                'class' => $additional_class,
                'action' => [
                    'title' => $confirmed_msg,
                    'onclick'=>['sima.accounting.confirmAccountOrder',$owner->account_order->id]
                ]
            ], true);
            if (isset($owner->canceled_account_order))
            {
                $canceled_button = Yii::app()->controller->widget('SIMAButton', [
                    'class' => $canceled_additional_class,
                    'action' => [
                        'title' => $canceled_confirmed_msg,
                        'onclick'=>['sima.accounting.confirmAccountOrder',$owner->canceled_account_order->id]
                    ]
                ], true);
            }
            else
            {
                $canceled_button = '';
            }
        }
        
        $confirmed_text = Yii::t('FilesModule.File', 'AccountOrderSpanText',[
            '{account_order}' => $owner->account_order->DisplayHtml,
            '{account_order_date}' => $owner->account_order->date
        ]);
        if (isset($owner->canceled_account_order))
        {
            $canceled_text = Yii::t('FilesModule.File', 'AccountOrderSpanCanceledText',[
                '{account_order}' => $owner->canceled_account_order->DisplayHtml,
                '{account_order_date}' => $owner->canceled_account_order->date
            ]);
        }
        else
        {
            $canceled_text = '';
        }
        
        
        return [
            
            'confirmed_text' => $confirmed_text,
            'confirmed_button' => $confirmed_button,
            
            'canceled_text' => $canceled_text,
            'canceled_button' => $canceled_button,
        ];
    }
    public function columnDisplays($column)
    {
        $owner = $this->owner;
        switch ($column)
        {
            case 'option_buttons':
                if ($owner->isBooked)
                {
                    return '';
                }
                if(SIMAMisc::isVueComponentEnabled())
                {
                    $button_change_account_order = Yii::app()->controller->widget(SIMAButtonVue::class,[
                        'title' => Yii::t('FilesModule.File', 'ChangeAccountOrder'),
                        'onclick' => ['sima.accounting.changeAccountDocumentChoose',$owner->id,AccountDocument::predictBookingYearForDocument($owner->id)->id]
                    ],true, true);
                    $button_remove_from_account = Yii::app()->controller->widget(SIMAButtonVue::class,[
                        'title' => Yii::t('FilesModule.File', 'RemoveThisDocumentFromAccountOrder'),
                        'onclick' => [
                            'sima.ajax.get',
                            'base/model/remove',
                            [
                                'data'=>[
                                    'model' => 'AccountDocument',
                                    'model_id' => $owner->id
                                ]
                            ]
                        ]
                    ],true, true);
                }
                else
                {
                    $button_change_account_order = Yii::app()->controller->widget('SIMAButton',[
                        'action'=>[
                            'title' => Yii::t('FilesModule.File', 'ChangeAccountOrder'),
                            'onclick' => ['sima.accounting.changeAccountDocumentChoose',$owner->id,AccountDocument::predictBookingYearForDocument($owner->id)->id]
                        ]
                    ],true,true);
                    $button_remove_from_account = Yii::app()->controller->widget('SIMAButton',[
                        'action'=>[
                            'title' => Yii::t('FilesModule.File', 'RemoveThisDocumentFromAccountOrder'),
                            'onclick' => [
                                'sima.ajax.get',
                                'base/model/remove',
                                [
                                    'data'=>[
                                        'model' => 'AccountDocument',
                                        'model_id' => $owner->id
                                    ]
                                ]
                            ]
                        ]
                    ],true,true);
                }
                return $button_change_account_order.$button_remove_from_account;
            case 'canceled_option_buttons':
                $canceled_acocunt = $owner->canceled_account_order;
                if ($canceled_acocunt->booked)
                {
                    return '';
                }
                $button_remove_from_account = '';
                if(SIMAMisc::isVueComponentEnabled())
                {
                    $button_change_account_order = Yii::app()->controller->widget(SIMAButtonVue::class,[
                        'title' => Yii::t('FilesModule.File', 'ChangeAccountOrder'),
                        'onclick' => ['sima.accounting.addCancelAccountDocumentChoose',$owner->id,AccountDocument::predictBookingYearForDocument($owner->id)->id]
                    ],true, true);
                    $button_remove_from_account = Yii::app()->controller->widget(SIMAButtonVue::class,[
                        'title' => Yii::t('FilesModule.File', 'RemoveThisDocumentFromAccountOrder'),
                        'onclick' => ['sima.accounting.removeCancelAccountDocument',$owner->id]
                    ],true, true);
                }
                else
                {
                    $button_change_account_order = Yii::app()->controller->widget('SIMAButton',[
                        'action'=>[
                            'title' => Yii::t('FilesModule.File', 'ChangeAccountOrder'),
                            'onclick' => ['sima.accounting.addCancelAccountDocumentChoose',$owner->id,AccountDocument::predictBookingYearForDocument($owner->id)->id]
                        ]
                    ],true,true);
                    $button_remove_from_account = Yii::app()->controller->widget('SIMAButton',[
                        'action'=>[
                            'title' => Yii::t('FilesModule.File', 'RemoveThisDocumentFromAccountOrder'),
                            'onclick' => ['sima.accounting.removeCancelAccountDocument',$owner->id]
                        ]
                    ],true,true);
                }
                return $button_change_account_order.$button_remove_from_account;
            default: return parent::columnDisplays($column);
        }
    }

    public function modelForms()
    {
        return array(
            'default' => array(
                'type' => 'generic',
                'columns' => array(
                    'id'=>'hidden',
                    'account_order_id' => array('searchField','relName'=>'account_order', 'add_button'=>true),                            
                )
            )
        );
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default: return [
                'columns' => ['file', 'option_buttons']
            ];
        }
    }

}