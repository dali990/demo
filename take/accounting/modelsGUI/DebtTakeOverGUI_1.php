<?php

class DebtTakeOverGUI extends SIMAActiveRecordDisplayGUI
{

    public function columnLabels()
    {
        return array(
            'debtor' => Yii::t('AccountingModule.DebtTakeOver','Debtor'),
            'debtor_id' => Yii::t('AccountingModule.DebtTakeOver','Debtor'),
            'creditor' => Yii::t('AccountingModule.DebtTakeOver','Creditor'),
            'creditor_id' => Yii::t('AccountingModule.DebtTakeOver','Creditor'),
            'takes_over' => Yii::t('AccountingModule.DebtTakeOver','TakesOver'),
            'takes_over_id' => Yii::t('AccountingModule.DebtTakeOver','TakesOver'),
            'debt_value' => Yii::t('AccountingModule.DebtTakeOver','DebtValue'),
            'sign_date' => Yii::t('AccountingModule.DebtTakeOver','SignDate'),
        ) + parent::columnLabels();
    }
    
    public function getDisplayModel()
    {
        return $this->owner->file;
    }

    public function modelLabel($plural = false)
    {
        return Yii::t('AccountingModule.DebtTakeOver',$plural?'DebtTakeOvers':'DebtTakeOver');
    }
    
    public function modelViews()
    {
        return [
            'inFile'
        ];
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default: return[
                'columns' => [
                    'file.display_name' => ['default_width' => 500],
                    'creditor' => ['default_width' => 250],
                    'debtor' => ['default_width' => 250],
                    'takes_over' => ['default_width' => 250],
                    'debt_value' => ['default_width' => 150],
                    'sign_date' => ['default_width' => 150],
                ]
            ];
        } 
    }
    
    public function modelForms()
    {
        return array(
            'default' => array(
                'type' => 'generic',
                'columns' => array(
                    'creditor_id' => ['relation', 'relName' => 'creditor'],
                    'debtor_id' => ['relation', 'relName' => 'debtor'],
                    'takes_over_id' => ['relation', 'relName' => 'takes_over'],
                    'debt_value' => 'numberField',
                    'sign_date' => 'datetimeField',
                )
            )
        );
    }
    
    
    
//    public function afterValidateDependencyError(array $foreign_dependencies)
//    {        
//        $allowed_tables = [
//            AccountLayout::model()->tableName().':law_id'
//        ];
//        
//        $parsed_foreign_dependencies = array_diff($foreign_dependencies, $allowed_tables);
//        
//        parent::afterValidateDependencyError($parsed_foreign_dependencies);
//    }

}