<?php

class PaymentCodeGUI extends SIMAActiveRecordGUI
{

    public function columnLabels()
    {
        return array(
            'payment_type' => Yii::t('AccountingModule.PaymentCode', 'PaymentType'),
            'payment_type_id' => Yii::t('AccountingModule.PaymentCode', 'PaymentType'),
            'code' => 'Kod',
        )+parent::columnLabels();
    }
    
    public function modelLabel($plural = false) 
    {
        return Yii::t('AccountingModule.PaymentCode', 'PaymentCode');
    }

//    public function modelViews()
//    {
//        return array(
//            'paychecks'//=>array('style'=>'border: 1px solid;'),
////            'tabTemplates'=>array('with'=>array('templated_file'),'style'=>'border: 1px solid;')  //.creator
//        );
//    }
    
    public function modelForms()
    {
        return array(
            'default'=>array(
                'type'=>'generic',
                'columns'=>array(
                    'code'=>'textField',
                    'name'=>'textField',
                    'description'=>'textArea',
                    'payment_type_id'=>'dropdown'
//                    'bank_id'=>array('searchField','relName'=>'bank'),
//                    'number'=>'textField',
//                    'start_amount'=>'textField',
//                    'comment'=>'textArea'
                )
            )
        );
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default:
                return [
                    'columns' => [ 'code', 'name', 'description','payment_type']
                ];
        }
    }
}