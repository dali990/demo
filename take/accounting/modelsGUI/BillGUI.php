<?php

class BillGUI extends FileGUI
{
    public function getDisplayModel()
    {
        return $this->owner->file;
    }
    
    public function getbill_types()
    {
        return [
            'BILL' => Yii::t('AccountingModule.Bill','Bill'),
            'ADVANCE_BILL' => Yii::t('AccountingModule.Bill','Advance bill'),
            'RELIEF_BILL' => Yii::t('AccountingModule.Bill','Credit memo'),
            'UNRELIEF_BILL' => Yii::t('AccountingModule.Bill','Debit memo'),
            'PRE_BILL' => Yii::t('AccountingModule.Bill','Proforma bill'),
        ];
    }
    
    public function columnLabels()
    {
        return array(
            'bill_number' => 'Broj računa',
            'bill_type' => 'Tip',
            'partner_id' => 'Partner',
            'income_date' => 'Datum računa',
            'traffic_date' => Yii::t('AccountingModule.Bill','TrafficDate'),
            'payment_date' => 'Valuta',
            'amount' => 'Iznos',
            'unreleased_amount' => 'Dug',
            'vat' => 'PDV',
            'vat_refusal' => Yii::t('AccountingModule.Bill','VAT refusal'),
            'base_amount' => 'Osnovica',
            'base_amount0' => 'Osnovica 0%',
            'base_amount10' => 'Osnovica 10%',
            'base_amount20' => 'Osnovica 20%',
            'vat10' => 'PDV 10%',
            'vat20' => 'PDV 20%',
            'value_customs' => 'Carinski PDV',
            'cost_location_id' => 'Mesto Troška',
            'comment' => 'Komentar',
            'file.account_document.account_order.date' => 'Datum knjizenja',
            'filing_id' => 'Zavodni broj',
            'confirm1' => 'Potvrda1',
            'confirm1_no_filter' => 'Potvrda',
            'confirm2' => 'Potvrda2',
            'responsible_id' => 'Kod osobe',
            'by_contract' => 'Po ugovoru',
            'payment_conditions' => 'Uslovi placanja',
            'tax_note' => 'Poreske napomene',
            'warehouse_requisitions' => 'Trebov./Prijem.',
            'call_for_number' => 'Poziv na broj',
            'call_for_number_modul' => 'Poziv na broj|modul',
            'original' => 'Dostavljen original?',
            'year' => 'Godina',
            'year_id' => 'Godina',
            'cost_location_id' => Yii::t('AccountingModule.CostLocation','Cost location'),
            'cost_type_id' => Yii::t('AccountingModule.CostType','Cost type'),
            'cost_type' => Yii::t('AccountingModule.CostType','Cost type'),
            'cost_location' => Yii::t('AccountingModule.CostLocation','Cost location'),
            'job_order' => Yii::t('AccountingModule.CostLocation','Cost location'),
            'unreleased_percent_of_amount' => 'Procenat',
            'interest' => 'Kamata',
            'rounding' => 'Zaokruzivanje',
            'bound_currency_id' => Yii::t('AccountingModule.Bill', 'BoundCurrency'),
            'bound_currency' => Yii::t('AccountingModule.Bill', 'BoundCurrency'),
            'amount_in_currency' => Yii::t('AccountingModule.Bill', 'AmountInCurrency'),
            'unreleased_amount_in_currency' => Yii::t('AccountingModule.Bill', 'UnreleasedAmountInCurrency'),
            'partner_bank_account_id' => 'Žiro račun partnera na koji se plaća',
            'partner_bank_account' => 'Žiro račun partnera na koji se plaća',
            'lock_amounts' => Yii::t('AccountingModule.Bill', 'LockAmounts'),
            'vat_in_diferent_month_id' => 'PDV u drugom mesecu',
            'vat_in_diferent_month' => 'PDV u drugom mesecu',
            'unreleased_amount_full_view' => 'Pregled placanja',
            'invoicing_basis' => Yii::t('AccountingModule.Bill', 'InvoicingBasis'),
            'invoice_comment' => Yii::t('AccountingModule.Bill', 'InvoiceComment'),
            'traffic_location_id' => Yii::t('AccountingModule.Bill', 'TrafficLocation'),
            'traffic_location' => Yii::t('AccountingModule.Bill', 'TrafficLocation'),
            'issue_location_id' => Yii::t('AccountingModule.Bill', 'IssueLocation'),
            'issue_location' => Yii::t('AccountingModule.Bill', 'IssueLocation'),
            'note_of_tax_exemption' => Yii::t('AccountingModule.NoteTaxExemption', 'NoteOfTaxExemption'),
            'note_of_tax_exemption_id' => Yii::t('AccountingModule.NoteTaxExemption', 'NoteOfTaxExemption')
        ) + parent::columnLabels();
    }
    
    public function trafficDatePlaceholder()
    {
        return Yii::t('AccountingModule.Bill', 'Leave empty if same as income_date');
    }

    public function modelLabel($plural = false)
    {
        return Yii::t('AccountingModule.Bill', $plural?'Bills':'Bill');
    }
    
    
    public function vueProp(string $prop, array $scopes = []):array
    {
        $owner = $this->owner;
        switch ($prop)
        {
            case 'warehouse_bill_items':
            case 'fixed_assets_bill_items':
            case 'services_bill_items_unconfirmed':
            case 'services_bill_items':
                $prop_value = [
                    'TYPE' => 'MODEL_ARRAY',
                    'ARRAY' => SIMAMisc::getVueModelTags($owner->$prop(['scopes' => $scopes]))
                ];
                $update_tags = SIMAMisc::getModelTags($owner->bill_items);
                return [$prop_value,$update_tags];
            default: return parent::vueProp($prop, $scopes);
        }
    }
    
    public function vuePropValue($prop)
    {
        $owner = $this->owner;
        switch ($prop)
        {
            case 'has_discount': return (boolean)$owner->has_discount;
            case 'bill_items_max_number': return max([$owner->bill_items_max_value, $owner->bill_items_max_quantity]);
            case 'cost_locations':                
                $cls = [];
        
                /**
                 * moze se desiti da se u toku rada izgubi pravo pristupa fajlu
                 * pa mora sa resetScope
                 */
                $file = File::model()->resetScope()->findByPkWithCheck($owner->id);
                
                foreach ($file->file_belongs as $file_tag)
                {
//                    error_log($file_tag->DisplayName);
                    $tag_model = $file_tag->getTagModel();
                    if (is_null($tag_model))
                    {
                        continue;
                    }
                    if (in_array(get_class($tag_model), ['Theme','FixedAsset']))
                    {
                        if (isset($tag_model->cost_location))
                        {
                            $cl = $tag_model->cost_location;
                            $cl_vue_tag = SIMAMisc::getVueModelTag($cl);
                            $_found = false;
                            foreach ($cls as $_existing_cl)
                            {
                                if ($_existing_cl === $cl_vue_tag)
                                {
                                    $_found = true;
                                }
                            }
//
                            if (!$_found)
                            {
                                $cls[] = $cl_vue_tag; 
////                                        [
////                                    'id' => $cl->id,
////                                    'name' => $cl->DisplayHTML.' - '.$cl->work_name,
////                                    'vue_tag' => 
////                                ];
                            }   
                        }
                    }
                }

                return $cls;
            case 'model_options_data': 
                return array_merge(parent::vuePropValue($prop), [
                    'cancel' => SIMAHtmlButtons::cancelLinkWidgetAction($owner)
                ]);
            case 'diff_between_payment_date_and_today_in_days':
                    $payment_date = new DateTime($owner->payment_date);
                    $today = new DateTime();
                    $days_diff = $payment_date->diff($today)->days;
                return $days_diff;
            case 'currency':
                return $owner->getBillCurrency();
            default: return parent::vuePropValue($prop);
        }
    }

    private function getform_model()
    {
        $owner = $this->owner;
        switch ($owner->bill_type)
        {
            case Bill::$BILL:           $base_name = 'Bill'; break;
            case Bill::$ADVANCE_BILL:   $base_name = 'AdvanceBill'; break;
            case Bill::$PRE_BILL:       $base_name = 'PreBill'; break;
            case Bill::$RELIEF_BILL:    $base_name = 'ReliefBill'; break;
            case Bill::$UNRELIEF_BILL:  $base_name = 'UnReliefBill'; break;
            default: throw new SIMAWarnAutoBafException('getform_model -> ne postoji tip racuna: '.$this->bill_type);
        }
        $model_name = $base_name.($owner->invoice?'Out':'In');
        return $model_name::model()->findByPk($owner->id);
    }
    
    public function columnDisplays($column)
    {
        $owner = $this->owner;
        
        switch ($column)
        {
            case 'form_custom': 
                return SIMAHtmlButtons::modelFormOpenWidgetAction($this->getform_model());
            case 'amount_letters': return SIMAHtml::money2letters($owner->amount);
            case 'bill_type': 
                switch ($owner->bill_type)
                {
                    case Bill::$BILL:           return Yii::t('AccountingModule.Bill','Bill');
                    case Bill::$ADVANCE_BILL:   return Yii::t('AccountingModule.Bill','Advance bill');
                    case Bill::$RELIEF_BILL:    return Yii::t('AccountingModule.Bill','Credit memo');
                    case Bill::$UNRELIEF_BILL:  return Yii::t('AccountingModule.Bill','Debit memo');
                    case Bill::$PRE_BILL:       return Yii::t('AccountingModule.Bill','Proforma bill');
                }
            case 'invoice': return ($owner->invoice) ? 'faktura' : 'racun';
            case 'bill_number': return $owner->bill_number;
            case 'responsible_id': return (isset($owner->responsible)) ? $owner->responsible->DisplayName : '?';
            case 'filing_id': return 'Treba da se koristi filling';
            case 'filing': return 'Treba da se koristi file.filling - prijavite administratoru!!';
            case 'base_amount': return SIMAHtml::money_format($owner->$column);
            case 'unreleased_percent_of_amount': return SIMAHtml::money_format($owner->unreleased_percent_of_amount,'%');
            
            case 'amount_local': return SIMAHtml::money_format($owner->amount,'');
            case 'amount_in_currency':             
                if(isset($owner->bound_currency))
                {
                    try
                    {
                        if(!$owner->isSetMiddleCurrencyRateForBoundCurrency())
                        {
                            $result = SIMAHtml::money_format($owner->amount);

                            $result .= SIMAHtml::modelFormOpen(MiddleCurrencyRate::model(), [
                                'init_data' => [
                                    'MiddleCurrencyRate' => array(
                                        'currency_id' => $owner->bound_currency->id,
                                        'date' => $owner->income_date
                                    )
                                ],
                                'button_title' => Yii::t('AccountingModule.Bill', 'AddMiddleCurrencyRate', [
                                    '{currency}'=>$owner->bound_currency, 
                                    '{date}' => $owner->income_date
                                ]),
                                'updateModelViews'=>SIMAHtml::getTag($owner)
                            ]);
                        }
                        else
                        {
                            $result = SIMAHtml::money_format($owner->BoundCurrencyValue($owner->amount), $owner->bound_currency->DisplayName);
                        }
                    }
                    catch(SIMAWarnNoCurrencyRate $warn)
                    {
                        $result = '????';
                    }
                }
                else
                {
                    $result = SIMAHtml::money_format($owner->amount);
                }
                                
                return $result;
            case 'unreleased_amount':
                $prependix = '';
                if ($owner->unreleased_amount > 0)
                {
                    if (empty($owner->payment_date))
                    {
                        $prependix = '<span class="sima-acc-br-ontime">'.Yii::t('AccountingModule.BillRelease', 'StatusNODUEDATE').'</span> ';
                    }
                    else
                    {
                        $payment_date = new DateTime($owner->payment_date);
                        $today = new DateTime();
                        $days_diff = $payment_date->diff($today)->days; //UVEK DAJE POZITIVNU VREDNOST
                        if ($days_diff > 0) // ovo je prvo provereno da se ne bi ispivala 0
                        {
                            if ($today > $payment_date)
                            {
                                $prependix = '<span class="sima-acc-br-late">'.Yii::t('AccountingModule.BillRelease', 'StatusLATE').'('.$days_diff.')</span> ';
                            }
                            else
                            {
                                $prependix = '<span class="sima-acc-br-pretime">'.Yii::t('AccountingModule.BillRelease', 'StatusPRETIME').'('.$days_diff.')</span> ';
                            }
                        }
                        else
                        {
                            $prependix = '<span class="sima-acc-br-pretime">'.Yii::t('AccountingModule.BillRelease', 'StatusTODAY').'</span> ';
                        }
                    }
                    
                }
                return $prependix.parent::columnDisplays($column);
            case 'unreleased_amount_in_currency':
                if(isset($owner->bound_currency))
                {
                    try
                    {
                        if(!$owner->isSetMiddleCurrencyRateForBoundCurrency())
                        {
                            $result = SIMAHtml::money_format($owner->amount);

                            $result .= SIMAHtml::modelFormOpen(MiddleCurrencyRate::model(), [
                                'init_data' => [
                                    'MiddleCurrencyRate' => array(
                                        'currency_id' => $owner->bound_currency->id,
                                        'date' => $owner->income_date
                                    )
                                ],
                                'button_title' => Yii::t('AccountingModule.Bill', 'AddMiddleCurrencyRate', [
                                    '{currency}'=>$owner->bound_currency, 
                                    '{date}' => $owner->income_date
                                ]),
                                'updateModelViews'=>SIMAHtml::getTag($owner)
                            ]);
                        }
                        else
                        {
                            $result = SIMAHtml::money_format($owner->BoundCurrencyValue($owner->unreleased_amount), $owner->bound_currency->DisplayName);
                        }
                    }
                    catch(SIMAWarnNoCurrencyRate $warn)
                    {
                        $result = '????';
                    }
                }
                else
                {
                    $result = SIMAHtml::money_format($owner->unreleased_amount);
                }
                return $result;         
            case 'unreleased_amount_full_view':
                $result = '';
                foreach ($owner->payments(['scopes' => ['recently']]) as $payment)
                {
                    $bill_release = BillRelease::model()
                            ->findByAttributes([
                                'bill_id' => $owner->id,
                                'payment_id' => $payment->id
                            ]);
                    $value = $bill_release->getAttributeDisplay('amount');
                    $status = $bill_release->getAttributeDisplay('status');
                    $result .= 
                            ' <span>'
                            . $payment->payment_date
                            . '</span>:<span style="display:inline-block; width:100px; text-align:right">'
                            . $value
                            . '</span><span style="display:inline-block; width:120px; text-align:right">'
                            . $status
                            . '</span><br />';
                }
                case 'pdf_suggestion':
                    $btn_params = [
                        'title' => Yii::t('AccountingModule.Bill', 'PdfSuggestion'),
                        'onclick' => ['sima.accountingMisc.billPdfSuggestion', $owner->id]
                    ];
                    if(SIMAMisc::isVueComponentEnabled())
                    {
                        $return = Yii::app()->controller->widget(SIMAButtonVue::class, $btn_params, true);
                    }
                    else
                    {
                        $return = Yii::app()->controller->widget(SIMAButton::class, [
                            'action' => $btn_params
                        ], true);
                    }
                return $return;
            case 'booking_date':
                if (!is_null($owner->booking_date))
                {
                    return $owner->booking_date;
                }
                else
                {
                    return Yii::t('AccountingModule.AccountOrder','NotBooked');
                }
            case 'cost_type':
                $types_names = [];
                foreach ($owner->cost_types as $c_type)
                {
                    $types_names[] = $c_type->DisplayName;
                }
                return implode('<br />', $types_names);
                
            case 'cost_location':
                $types_names = [];
                foreach ($owner->cost_locations as $c_location)
                {
                    $types_names[] = $c_location->DisplayHTML;
                }
                return implode('<br />', $types_names);
                        
            default: return parent::columnDisplays($column);
        }
    }

    public function modelViews()
    {
        return array(
            'full_info' => ['origin' => 'Bill'],
            //ostaje samo za tab za stare stavke racuna
            'bill_items' => ['class' => 'sima-layout-panel','params_function' => 'billItemsParams'],
            'payed_with' => ['params_function' => 'payedWithParams', 'with' => ['file'], 'origin' => 'Bill'],
            'inFile',
            'warehouseTransfersListForm', //=>array('origin'=>'Bill'),
            'warehouseTransfersList',//=>array('origin'=>'Bill')
            'options_in_file' => ['html_wrapper' => null]
//            'tabTemplates'=>array('with'=>array('templated_file'),'style'=>'border: 1px solid;')  //.creator
        );
    }
    
    public function billItemsParams()
    {
        $owner = $this->owner;
        $uniq = SIMAHtml::uniqid();
        $lock_button_id = 'lock_button_id'.$uniq;
        $fa_button_id = 'fa_button_id'.$uniq;
        $gui_table_id = 'gui_table_id'.$uniq;
        
        
        if ($owner->lock_amounts)
        {
            $locked_msg = Yii::t('AccountingModule.Bill','ItemsLocked');
            $additional_class = '_pressed' ;
        }
        else
        {
            $locked_msg = Yii::t('AccountingModule.Bill','ItemsUnLocked');
            $additional_class = '' ;
        }
        $fa_msg = Yii::t('AccountingModule.Bill','AddBillItemsToFixedAssets');
        
        
        if ($owner->invoice)
        {
            $warehouse_transfer_type = WarehouseDispatch::class;
            if ($owner->isBillWT)
            {
                $bill_to_wt_msg = Yii::t('AccountingModule.Bill','ConvertFromBillWTToBillOut');
            }
            else
            {
                $bill_to_wt_msg = Yii::t('AccountingModule.Bill','ConvertFromBillToBillWTOut');
            }
            
        }
        else
        {
            $warehouse_transfer_type = WarehouseReceiving::class;
            if ($owner->isBillWT)
            {
                $bill_to_wt_msg = Yii::t('AccountingModule.Bill','ConvertFromBillWTToBillIn');
            }
            else
            {
                $bill_to_wt_msg = Yii::t('AccountingModule.Bill','ConvertFromBillToBillWTIn');
            }
        }
        
        if(SIMAMisc::isVueComponentEnabled())
        {
            $fa_button = Yii::app()->controller->widget(SIMAButtonVue::class, [
                'id' => $fa_button_id,
                'title' => $fa_msg,
                'onclick'=>['sima.accounting.addBillItemToFixedAssetsOLD',$gui_table_id]
            ],true);
            $lock_button = Yii::app()->controller->widget(SIMAButtonVue::class, [
                'id' => $lock_button_id,
                'title' => $locked_msg,
                'additional_classes' => [$additional_class],
                'onclick'=>['sima.accounting.BillItemsLocked',$owner->id,$lock_button_id]
            ],true);
            if ($owner->isBillWT)
            {
                $wt_button = Yii::app()->controller->widget(SIMAButtonVue::class, [
                    'title' => $bill_to_wt_msg,
                    'onclick'=>['sima.accounting.removeWarehouseTransferFromBIll',$owner->id, $warehouse_transfer_type]
                ],true);
            }
            else
            {
                $wt_button = Yii::app()->controller->widget(SIMAButtonVue::class, [
                    'title' => $bill_to_wt_msg,
                    'onclick'=>['sima.accounting.addWarehouseTransferToBIll',$owner->id, $warehouse_transfer_type, $owner->partner_id]
                ],true);
            }
        }
        else
        {
            $fa_button = Yii::app()->controller->widget('SIMAButton', [
                'id' => $fa_button_id,
                'action' => [
                    'title' => $fa_msg,
                    'onclick'=>['sima.accounting.addBillItemToFixedAssetsOLD',$gui_table_id]
                ]
            ],true);
            $lock_button = Yii::app()->controller->widget('SIMAButton', [
                'id' => $lock_button_id,
                'class' => $additional_class,
                'action' => [
                    'title' => $locked_msg,
                    'onclick'=>['sima.accounting.BillItemsLocked',$owner->id,$lock_button_id]
                ]
            ], true);
            if ($owner->isBillWT)
            {
                $wt_button = Yii::app()->controller->widget('SIMAButton', [
                    'action' => [
                        'title' => $bill_to_wt_msg,
                        'onclick'=>['sima.accounting.removeWarehouseTransferFromBIll',$owner->id, $warehouse_transfer_type]
                    ]
                ], true);
            }
            else
            {
                $wt_button = Yii::app()->controller->widget('SIMAButton', [
                    'action' => [
                        'title' => $bill_to_wt_msg,
                        'onclick'=>['sima.accounting.addWarehouseTransferToBIll',$owner->id, $warehouse_transfer_type, $owner->partner_id]
                    ]
                ], true);
            }
        }
        $lock_button_text = Yii::t('AccountingModule.Bill', 'AmountsAreLocked:');
        
        
        
        $buttons_row = $lock_button_text.$lock_button.' | '.$fa_button.' | '.$wt_button;
        
        return [
            'buttons_row' => $buttons_row,
            'gui_table_id' => $gui_table_id
        ];
        
    }
    
    public function payedWithParams()
    {
        $owner = $this->owner;
        $payments = [];
        $prepared_payments = [];
        foreach ($owner->releases as $release)
        {
            $payment = $release->payment;
            $note = $payment->payment_date.': '.$release->getAttributeDisplay('amount').' - '.$release->getAttributeDisplay('status');
            if (isset($payment->bank_statement))
            {
                $payments[] = $note;
            }
            else
            {
                $prepared_payments[] = $note;
            }
                
            
        }
        $advances = [];
        foreach ($owner->advance_releases as $adv_release)
        {
            $advance_bill = $adv_release->advance_bill;
            $advances[] = $advance_bill->income_date.': '.$adv_release->getAttributeDisplay('amount');
        }
        $reliefs = [];
        foreach ($owner->reliefes as $relief)
        {
            $relief_bill = $relief->relief_bill;
            $reliefs[] = $relief_bill->income_date.': '.$relief->getAttributeDisplay('amount');
        }
        if(empty($owner->amount))
        {
            $unreleased_percent = SIMAHtml::money_format(100, '%');
        }
        else
        {
            $unreleased_percent = SIMAHtml::money_format($owner->unreleased_amount * 100 / $owner->amount, '%');
        }
        return [
            'payments' => $payments,
            'prepared_payments' => $prepared_payments,
            'advances' => $advances,
            'reliefs' => $reliefs,
            'unreleased' => $owner->getAttributeDisplay('unreleased_amount'),
            'unreleased_percent' => $unreleased_percent
        ];
    }
    

    public function modelForms()
    {
        $owner = $this->owner;
        
        $vat_refusal = ['checkBox'];
        if (is_null($owner->vat_refusal))
        {
            $vat_refusal['disabled'] = 'disabled';
        }
        
        return array(
//            'default' => array(
//                'type' => 'generic',
//                'columns' => array(
//                    'bill_number' => 'textField',
//                    'cost_location_id' => array('searchField', 'relName' => 'job_order'),
////                    'cost_location_id' => array('searchField', 'relName' => 'job_order'),
//                    'cost_type_id' => array('searchField', 'relName' => 'cost_type', 'add_button'=>true),
//                    'partner_id' => ['searchField', 'relName' => 'partner', 'add_button' => ['action'=>'simaAddressbook/index']],
//                    'partner_bank_account_id'=>['searchField', 'relName' => 'partner_bank_account', 'dependent_on'=>[                        
//                        'partner_id'=>[
//                            'onValue'=>[
//                                'enable',
//                                ['condition', 'model_filter' => 'partner.ids']
//                            ],
//                            'onEmpty'=>'disable'
//                        ]                        
//                    ]],
//                    'income_date' => array('datetimeField'),
//                    'payment_date' => array('datetimeField'),
//                    'amount' => 'numberField',
//                    'base_amount0' => 'numberField',
//                    'base_amount10' => 'numberField',
//                    'base_amount20' => 'numberField',
//                    'vat10' => 'numberField',
//                    'vat20' => 'numberField',
//                    'interest' => 'numberField',
//                    'rounding' => 'numberField',
//                    'responsible_id' => array('dropdown', 'empty' => 'Izaberi'),
//                    'vat_refusal' => $vat_refusal,
//                    'original' => array('checkBox'),
//                    'call_for_number' => 'textField',
//                    'call_for_number_modul' => 'textField',
//                    'comment' => 'textArea'
//                )
//            )
        );
    }    

    public function droplist($key, $relName='', $filter=null)
    {
        switch ($key)
        {
            case 'belongs': return FileTag::model()->getModelDropList();
            default: return parent::droplist($key, $relName, $filter);
        }
    }

    public function guiTableSettings($type)
    {
        switch ($type)
        {
            case 'confirm1':
                return array(
                    'columns'=>array(
                        'bill_number', 'partner', 'income_date', 'payment_date', 'responsible_id',
                        'amount', 'base_amount', 'vat', 
                        'unreleased_amount' => ['min_width' => '200px'], 
                        'unreleased_amount_in_currency', 'belongs',
                        'cost_location', 'file.filing', 'comment'
                    ),
                    'sums' => array('amount','base_amount','unreleased_amount','vat'),
                    'order_by' => array('amount','base_amount','unreleased_amount','vat'),
                );
            case 'inPartner':
                return array(
                    'columns'=>array(
                        'bill_number', 'income_date', 'payment_date', 'amount', 'amount_in_currency', 
                        'unreleased_amount' => ['min_width' => '200px'], 
                        'unreleased_amount_in_currency',
                        'cost_location', 'comment', 'file.filing'
                    ),
                    'sums' => array('amount','base_amount','unreleased_amount','vat'),
                    'order_by' => array('amount','base_amount','unreleased_amount','vat'),
                );
            case 'inJobFinanse': return array(
                    'columns'=>array(
                        'bill_number', 'partner', 
                        'income_date', 'payment_date', 
                        'amount', 'amount_in_currency', 
                        'unreleased_amount' => ['min_width' => '200px'], 
                        'unreleased_amount_in_currency', 'vat',
                        'comment', 'belongs'
                    ),
                    'sums' => array('amount','base_amount','unreleased_amount','vat'),
                    'order_by' => array('bill_number','amount','base_amount','unreleased_amount','income_date','payment_date'),
                );
            case 'inBillProposals': return array(
                    'columns'=>array( 
                        'bill_number', 'amount', 'amount_in_currency', 
                        'unreleased_amount' => ['min_width' => '200px'], 
                        'unreleased_amount_in_currency', 'unreleased_percent_of_amount', 'income_date',
                        'base_amount0', 'base_amount10', 'base_amount20', 'vat10', 'vat20'
                    ),
                    'sums' => array('amount','unreleased_amount'),
                    'order_by' => array('bill_number','amount','base_amount','unreleased_amount','income_date','payment_date'),
                );
            case 'recurring_overview':
                return array(
                    'columns'=>array(
                        'bill_number', 
                        'partner' => ['min_width' => '200px'], 
                        'amount',
                        'unreleased_amount',
                        'income_date',
                        'payment_date',
                        'unreleased_amount_full_view' => ['min_width' => '320px']
                    ),
                    'sums' => array('amount', 'unreleased_amount'),
//                    'sumsFunc' => 'sumFuncRecurringBIllOverView'
//                    'order_by' => array('amount'),
                );
            case 'no_signatures':
                return array(
                    'columns'=>array(
                        'bill_type',
                        'file.filing.filing_number',
                        'partner' => ['min_width' => '300px'],
                        'bill_number', 
                        'income_date', 
                        'payment_date', 
                        'amount', 
                        'amount_in_currency', 
                        'unreleased_amount' => ['min_width' => '200px'],
                        'belongs' => ['min_width' => '200px'], 
                        'cost_location', 
                        'comment', 
                        'recurring_bill'
                    ),
                    'sums' => array('amount','base_amount','unreleased_amount'),
                    'order_by' => array('amount','base_amount','unreleased_amount'),
                );
            default:
                return array(
                    'columns'=>array(
                        'bill_number', 'partner', 'income_date', 'payment_date', 
                        'amount', 'amount_in_currency',
                        'base_amount', 'vat',
                        'unreleased_amount' => ['min_width' => '200px'], 
                        'unreleased_amount_in_currency', 
                        'belongs', 'cost_location', 'comment', 'file.filing.filing_number','interest',
                        'file.account_document.account_order',
                        'file.account_document.account_order.date','partner_bank_account',
                        'recurring_bill'
                    ),
                    'filters'=>array(
                        'year'
                    ),
                    'sums' => array('amount','base_amount','unreleased_amount','vat'),
                    'order_by' => array('amount','base_amount','unreleased_amount','vat','income_date','payment_date'),
                );
        }
    }

    public function sumFunc($criteria)
    {        
        $owner = $this->owner;
        
        $amount_in_currency_result = [];
        $unreleased_amount_in_currency_result = [];
        
        // dovuci valute koje se pojavljuju
        // po valuti + RSD:
        //  dovuci dane za koje ne postoji srednji kurs na dan i dovuci ih
        //  za dane za koje postoji srednji kurs na dan sumiraj
        
        $currencies_criteria_copy = clone $criteria;
        $currencies_criteria_copy->select = "bound_currency_id";
        $currencies_criteria_copy->group = "bound_currency_id";
        $currencies_criteria_copy->order = "";
        $currencies_criteria_result = $owner->findAll($currencies_criteria_copy);
        foreach($currencies_criteria_result as $ccr)
        {
            $currency = null;
            
            $amount_in_currency_criteria = new SIMADbCriteria();
                
            if(empty($ccr->bound_currency_id)) /// local currency
            {
                $currency = Currency::model()->findByPk(Yii::app()->configManager->get('base.country_currency'));
                if(empty($currency))
                {
//                    throw new Exception(Yii::t('AccountingModule.Bill','ConfigurationCountryCurrencyInvalid'));
                }
                
                $amount_in_currency_criteria->select = "sum(amount) as amount_in_currency, sum(unreleased_amount) as unreleased_amount_in_currency";
                $amount_in_currency_criteria->condition = 'bound_currency_id IS NULL';
            }
            else
            {
                $currency = Currency::model()->findByPk($ccr->bound_currency_id);
                
                $currency_criteria_copy = clone $criteria;
                $currency_criteria_copy->select = "income_date";
                $currency_criteria_copy->condition = 'bound_currency_id='.$currency->id
                        .' AND NOT EXISTS (SELECT 1 FROM '.(MiddleCurrencyRate::model()->tableName()).' mcr '
                            .'WHERE mcr.currency_id=bound_currency_id AND date=income_date LIMIT 1)';
                $currency_criteria_copy->order="";
                $currency_criteria_result = $owner->findAll($currency_criteria_copy);
                
                foreach($currency_criteria_result as $ccr_2)
                {
                    try 
                    {
                        /// dovuci dane
                        MiddleCurrencyRate::get($currency, Day::getByDate($ccr_2->income_date));
                    } 
                    catch (SIMAWarnNoCurrencyRate $warn) 
                    {
                        
                    }
                }
                
                $amount_in_currency_criteria->select = "sum(amount / mcr.value) as amount_in_currency, sum(unreleased_amount / mcr.value) as unreleased_amount_in_currency";
                $amount_in_currency_criteria->join = ', '.(MiddleCurrencyRate::model()->tableName()).' mcr';
                $amount_in_currency_criteria->condition = 'bound_currency_id='.$currency->id
                        .' AND mcr.currency_id=bound_currency_id AND mcr.date=income_date';
            }
            
            if(is_null($currency))
            {
                throw new Exception(Yii::t('AccountingModule.Bill','CurrencyIsNull'));
            }
            
            $currency_criteria_copy_2 = clone $criteria;
            $currency_criteria_copy_2->mergeWith($amount_in_currency_criteria);
            $currency_criteria_copy_2->order = "";
            $currency_criteria_result_2 = $owner->findAll($currency_criteria_copy_2);

            if(count($currency_criteria_result_2) > 0)
            {
                if(!empty($currency_criteria_result_2[0]->amount_in_currency)
                        && $currency_criteria_result_2[0]->amount_in_currency > 0)
                {
                    $amount_in_currency = $currency_criteria_result_2[0]->amount_in_currency;
                    $amount_in_currency_formated = SIMAHtml::money_format($amount_in_currency, $currency->DisplayName);
                    $amount_in_currency_result[] = $amount_in_currency_formated;
                }
                
                if(!empty($currency_criteria_result_2[0]->unreleased_amount_in_currency)
                        && $currency_criteria_result_2[0]->unreleased_amount_in_currency > 0)
                {
                    $unreleased_amount_in_currency = $currency_criteria_result_2[0]->unreleased_amount_in_currency;
                    $unreleased_amount_in_currency_formated = SIMAHtml::money_format($unreleased_amount_in_currency, $currency->DisplayName);
                    $unreleased_amount_in_currency_result[] = $unreleased_amount_in_currency_formated;
                }
            }
        }
                
        $amount_in_currency_result_str = implode(' </br>', $amount_in_currency_result);
        $unreleased_amount_in_currency_result_str = implode(' </br>', $unreleased_amount_in_currency_result);
        
        $temp_criteria = new SIMADbCriteria();
        $temp_criteria->select = "sum(amount) as amount, sum(base_amount) as base_amount, '$amount_in_currency_result_str' as amount_in_currency, sum(unreleased_amount) as unreleased_amount, '$unreleased_amount_in_currency_result_str' as unreleased_amount_in_currency, sum(vat) as vat";
        $criteria->mergeWith($temp_criteria);
        $criteria->order = "";
        $sum_result = $owner->findAll($criteria);
        if (count($sum_result)==0)
        {
            return new BillIn();
        }
                
        return $sum_result[0];
    }
    
    protected static $locked_bill_values_form_type = ['numberField','dependent_on' => [
                        'lock_amounts' => [
                            'onValue' => [
                                'disable', 
                                'for_values' => ['0'], 
                                'set_empty_on_disable' => false
                            ]
                        ]
                    ]];
    
}
