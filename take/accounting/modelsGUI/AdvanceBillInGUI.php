<?php

class AdvanceBillInGUI extends AdvanceBillGUI 
{
    
    public function modelForms()
    {
        $owner = $this->owner;
        
        $cost_type_id = [
            'relation', 
            'relName' => 'cost_type', 
            'add_button'=>[
                'action'=>'accounting/costTypes',
                'params' => [
                    'get_params' => [
                        'cost_or_income' => CostType::$TYPE_COST
                    ]
                ]
            ],
            'filters' => [
                'cost_or_income' => CostType::$TYPE_COST
            ],
            'dependent_on'=>[                        
                'income_date'=>[
                    'onValue' => ['trigger', 'func' => ['sima.accountingMisc.billCostTypeDependentOnBillIncomeDateInForm']],
                    'onEmpty' => ['disable']
                ]                        
            ]
        ];
        if ($owner->cost_type_mixed === true)
        {
            $cost_type_id['disabled'] = 'disabled';
        }
        
        $cost_location_id = ['relation', 'relName' => 'job_order'];
        if ($owner->cost_location_mixed === true)
        {
            $cost_location_id['disabled'] = 'disabled';
        }
        
        $vat_refusal = ['checkBox'];
        if (is_null($owner->vat_refusal))
        {
            $vat_refusal['disabled'] = 'disabled';
        }
        
        return array(
            'default' => array(
                'type' => 'generic',
                'columns' => array(
                    'bill_number' => 'textField',
                    'partner_id' => ['searchField', 'relName' => 'partner', 'add_button' => ['action'=>'simaAddressbook/index']],
                    'partner_bank_account_id'=>['searchField', 'relName' => 'partner_bank_account', 'dependent_on'=>[                        
                        'partner_id'=>[
                            'onValue'=>[
                                'enable',
                                ['condition', 'model_filter' => 'partner.ids']
                            ],
                            'onEmpty'=>'disable'
                        ]                        
                    ]],
                    'income_date'=> array('datetimeField'),
                    'payment_date'=> array('datetimeField'),
                    'cost_type_id' => $cost_type_id,
                    'cost_location_id' => $cost_location_id,
                    'bound_currency_id' => array('searchField','relName'=>'bound_currency'),
                    'lock_amounts' => 'checkBox',
                    'amount' => self::$locked_bill_values_form_type,
                    'base_amount0' => self::$locked_bill_values_form_type,
                    'base_amount10' => self::$locked_bill_values_form_type,
                    'base_amount20' => self::$locked_bill_values_form_type,
                    'vat10' => self::$locked_bill_values_form_type,
                    'vat20' => self::$locked_bill_values_form_type,
                    'responsible_id' => array('dropdown', 'empty'=>'Izaberi'),
                    'vat_refusal' => $vat_refusal,
                    'original'=>array('checkBox'),
                    'call_for_number' => 'textField',
                    'call_for_number_modul' => 'textField',
                    'comment'=>'textArea'
                )
            )
        );
    }

}