<?php

class AccountLayoutLawGUI extends SIMAActiveRecordGUI
{

    public function columnLabels()
    {
        return array(
            'name' => 'Naziv',
            'description' => 'Opis',
            'date' => 'Datum',
            'file_version_id'=>'Dokument',
        ) + parent::columnLabels();
    }

    public function modelLabel($plural = false)
    {
        return 'Zakon kontnog okvira';
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default: return array(
                'columns' => array(
                    'name', 'date', 'description'
                )
            );
        }
    }
    
    public function modelForms()
    {
        return array(
            'default' => array(
                'type' => 'generic',
                'columns' => array(
                    'name' => 'textField',
                    'date' => 'datetimeField',
                    'file_version_id' => 'fileField',
                    'description' => 'textArea',                    
                )
            )
        );
    }
    
    public function afterValidateDependencyError(array $foreign_dependencies)
    {        
        $allowed_tables = [
            AccountLayout::model()->tableName().':law_id'
        ];
        
        $parsed_foreign_dependencies = array_diff($foreign_dependencies, $allowed_tables);
        
        parent::afterValidateDependencyError($parsed_foreign_dependencies);
    }

}