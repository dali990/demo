<?php

class PartnerFinanceGUI extends SIMAActiveRecordGUI
{
    public function columnLabels()
    {
        return array(
            'ft_name' => 'Posao',
            'invoice_amount'=>'Fakturisano',
            'invoice_amount_1'=>'Fakturisano(samo na ovom poslu)',
            'invoice_amount_2'=>'Fakturisano(deli se sa drugim poslovima)',
            'invoice_unreleased'=>'Fakturisano-neplaćeno',
            'invoice_released'=>'Fakturisano-plaćeno',
            'bill_amount'=>'Računi',
            'bill_amount_1'=>'Računi(samo na ovom poslu)',
            'bill_amount_2'=>'Računi(deli se sa drugim poslovima)',
            'bill_unreleased'=>'Računi-neplaćeno',
            'bill_released'=>'Računi-plaćeno',
            'invoice_advance'=>'Primljeni avans',
            'invoice_advance_1'=>'Primljeni avans(samo na ovom poslu)',
            'invoice_advance_2'=>'Primljeni avans(deli se sa drugim poslovima)',
            'bill_advance'=>'Dati avans',
            'bill_advance_1'=>'Dati avans(samo na ovom poslu)',
            'bill_advance_2'=>'Dati avans(deli se sa drugim poslovima)',
        ) + parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return 'Finansije partnera';
    }   
    
    public function columnDisplays($column) 
    {
        $owner = $this->owner;
        switch ($column) 
        {
            case 'ft_name': 
                $_fa_table = FixedAsset::model()->tableName();
                $_bt_table = Theme::model()->tableName();
                switch ($owner->ft_model_table)
                {
                    case $_bt_table: return Theme::model()->findByPk($owner->ft_model_id)->DisplayHtml;
                    case $_fa_table: return FixedAsset::model()->findByPk($owner->ft_model_id)->DisplayHtml;
                    default: return $owner->ft_name;
                }
                
            default: return parent::columnDisplays($column);
        }   
    }
    
    public function guiTableSettings($type)
    {
        return array(
            'columns' => array(
                'ft_name',
                'invoice_amount',
                'invoice_amount_1',
                'invoice_amount_2',
                'invoice_unreleased',
                'invoice_released',
                'bill_amount',
                'bill_amount_1',
                'bill_amount_2',
                'bill_unreleased',
                'bill_released',
                'invoice_advance',
                'invoice_advance_1',
                'invoice_advance_2',
                'bill_advance',
                'bill_advance_1',
                'bill_advance_2',
            ),
            'sums' => array(
                    'invoice_amount', 'invoice_amount_1', 'invoice_amount_2', 'invoice_unreleased','invoice_released',
                    'bill_amount', 'bill_amount_1', 'bill_amount_2', 'bill_unreleased','bill_released',
                    'invoice_advance', 'invoice_advance_1', 'invoice_advance_2',
                    'bill_advance', 'bill_advance_1', 'bill_advance_2'
                ),
            'sums_function'=>'sumFunc'
        );
    }
    
    public function sumFunc($criteria) 
    {
        $owner = $this->owner;
        $bill = Bill::$BILL;
        $adv_bill = Bill::$ADVANCE_BILL;
        $temp_criteria = new SIMADbCriteria();                
        $temp_criteria->select = " sum(CASE WHEN (invoice is true and bill_type='$bill') THEN
                                    case when ft.ccount is not null and ft.ccount !=0 then
                                        round((amount/ft.ccount), 2)
                                    else
                                        round(amount, 2)
                                    end
                            ELSE 0 END) as invoice_amount,
                            sum(CASE WHEN (invoice is true and bill_type='$bill' and (ft.ccount is null or ft.ccount=0 or ft.ccount=1)) THEN
                                        round(amount, 2)
                            ELSE 0 END) as invoice_amount_1,
                            sum(CASE WHEN (invoice is true and bill_type='$bill' and ft.ccount is not null and ft.ccount>1) THEN
                                        round(amount/ft.ccount, 2)
                            ELSE 0 END) as invoice_amount_2,
                            sum(CASE WHEN (invoice is true and bill_type='$bill') THEN 
                                    case when ft.ccount is not null and ft.ccount !=0 then
                                        round(unreleased_amount/ft.ccount, 2)
                                    else
                                        round(unreleased_amount, 2)
                                    end
                            ELSE 0 END) as invoice_unreleased, 
                            sum(CASE WHEN (invoice is true and bill_type='$bill') THEN 
                                    case when ft.ccount is not null and ft.ccount !=0 then
                                        round(((amount-unreleased_amount)/ft.ccount ), 2)
                                    else
                                        round((amount-unreleased_amount), 2)
                                    end
                            ELSE 0 END) as invoice_released, 
                            sum(CASE WHEN (invoice is false and bill_type='$bill') THEN 
                                    case when ft.ccount is not null and ft.ccount !=0 then
                                         round(amount/ft.ccount, 2)
                                    else
                                         round(amount, 2)
                                    end
                            ELSE 0 END) as bill_amount,
                            sum(CASE WHEN (invoice is false and bill_type='$bill' and (ft.ccount is null or ft.ccount=0 or ft.ccount=1)) THEN
                                        round(amount, 2)
                            ELSE 0 END) as bill_amount_1,
                            sum(CASE WHEN (invoice is false and bill_type='$bill' and ft.ccount is not null and ft.ccount>1) THEN
                                        round(amount/ft.ccount, 2)
                            ELSE 0 END) as bill_amount_2,
                            sum(CASE WHEN (invoice is false and bill_type='$bill') THEN 
                                    case when ft.ccount is not null and ft.ccount !=0 then
                                         round(unreleased_amount/ft.ccount , 2)
                                    else
                                         round(unreleased_amount, 2)
                                    end
                            ELSE 0 END) as bill_unreleased,
                            sum(CASE WHEN (invoice is false and bill_type='$bill') THEN 
                                    case when ft.ccount is not null and ft.ccount !=0 then
                                         round((amount-unreleased_amount)/ft.ccount, 2)
                                    else
                                         round((amount-unreleased_amount), 2)
                                    end
                            ELSE 0 END) as bill_released,
                            
                            sum(CASE WHEN (invoice is true and bill_type='$adv_bill') THEN 
                                    case when ft.ccount is not null and ft.ccount !=0 then
                                         round((unreleased_amount)/ft.ccount , 2)
                                    else
                                         round((unreleased_amount) , 2)
                                    end
                            ELSE 0 END) as invoice_advance,
                            sum(CASE WHEN (invoice is true and bill_type='$adv_bill' and (ft.ccount is null or ft.ccount=0 or ft.ccount=1)) THEN
                                        round((unreleased_amount), 2)
                            ELSE 0 END) as invoice_advance_1,
                            sum(CASE WHEN (invoice is true and bill_type='$adv_bill' and ft.ccount is not null and ft.ccount>1) THEN
                                        round((unreleased_amount)/ft.ccount , 2)
                            ELSE 0 END) as invoice_advance_2,
                            sum(CASE WHEN (invoice is false and bill_type='$adv_bill') THEN 
                                    case when ft.ccount is not null and ft.ccount !=0 then
                                         round((unreleased_amount)/ft.ccount , 2)
                                    else
                                         round((unreleased_amount) , 2)
                                    end
                            ELSE 0 END) as bill_advance,
                            sum(CASE WHEN (invoice is false and bill_type='$adv_bill' and (ft.ccount is null or ft.ccount=0 or ft.ccount=1)) THEN
                                        round((unreleased_amount), 2)
                            ELSE 0 END) as bill_advance_1,
                            sum(CASE WHEN (invoice is false and bill_type='$adv_bill' and ft.ccount is not null and ft.ccount>1) THEN
                                        round((unreleased_amount)/ft.ccount , 2)
                            ELSE 0 END) as bill_advance_2";
        $criteria->mergeWith($temp_criteria);
        $sum_result = $owner->findAll($criteria);

        return $sum_result[0];

    }
}