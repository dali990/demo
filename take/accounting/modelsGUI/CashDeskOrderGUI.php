<?php

class CashDeskOrderGUI extends SIMAActiveRecordGUI
{
    public static $types = [
        'PAYOUT' => 'Isplata',
        'PAYIN' => 'Naplata',
        'SUBMIT' => 'Isplata na ŽR',
        'WITHDRAW' => 'Naplata sa ŽR'
    ];
    public function columnLabels()
    {
        return array(
            'number' => 'Broj naloga',
            'type' => 'Tip',
            'cash_desk_log_id' => 'Dnevnik blagajne',
            'cash_desk_log' => 'Dnevnik blagajne',
            'note' => 'Napomena'
        ) + parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return 'Nalog blagajne';
    }
    
    public function columnDisplays($column)
    {
        $owner = $this->owner;
        switch ($column)
        {
            case 'type': return self::$types[$owner->type];
            default: return parent::columnDisplays($column);
        }
    }

//    public function modelViews()
//    {
//        return array(
//        );
//    }

    public function modelForms()
    {
        return array(
            'default' => array(
                'type' => 'generic',
                'columns' => array(
                    'cash_desk_id' => 'hidden',
                    'number' => 'textField',
                    'type' => 'dropdown',
                    'date' => 'datetimeField',
                    'note' => 'textArea'
                )
            )
        );
    }
    
    public function droplist($key, $relName='', $filter=null) 
    {
        switch ($key) 
        {
            case 'type': return self::$types;
            default: return parent::droplist($key, $relName, $filter);
        }
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default: return array(
                'columns' => array(
                    'date','display_name', 'type', 'amount', 'note'
                )
            );
        }
    }
    
    public function generatePdf()
    {
        $owner = $this->owner;
        $cash_desk_order_report = new CashDeskOrderReport([
            'cash_desk_order' => $owner
        ]);          
        $temp_file = $cash_desk_order_report->getPDF();        
        $owner->file->appendTempFile(
            $temp_file->getFileName(), 
            $owner->DisplayName . '.pdf'
        );
        return $temp_file;
    }

}