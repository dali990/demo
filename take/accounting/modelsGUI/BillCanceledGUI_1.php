<?php

class BillCanceledGUI extends BillGUI 
{

    public function afterValidateDependencyError(array $foreign_dependencies)
    {        
        $allowed_tables = [
            BillItem::model()->tableName().':bill_id',
        ];        
        $parsed_foreign_dependencies = array_diff($foreign_dependencies, $allowed_tables);    
        parent::afterValidateDependencyError($parsed_foreign_dependencies);
    }
    

}