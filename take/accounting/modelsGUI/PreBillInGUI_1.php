<?php

class PreBillInGUI extends PreBillGUI 
{
    public function modelLabel($plural = false)
    {
        return 'Ulazni predracun';
    }
    
    public function modelViews()
    {
        return array(
            'full_info',// => array('params_function' => 'fullInfoParams'),
            'payed_with' => ['params_function' => 'payedWithParams', 'with' => ['file']],
//            'bill_items' => ['params_function' => 'billItemsParams', 'with' => ['file']],
//            'bill_items_not_confirmed' => ['params_function' => 'billItemsNotConfirmedParams', 'with' => ['file']],
//            'inFile',
//            'warehouseTransfersListForm', //=>array('origin'=>'Bill'),
//            'warehouseTransfersList'//=>array('origin'=>'Bill')
//            'tabTemplates'=>array('with'=>array('templated_file'),'style'=>'border: 1px solid;')  //.creator
        ) + parent::modelViews();
    }  
    
    public function payedWithParams()
    {
        $owner = $this->owner;
        $base_bill_model = Bill::model()->findByPk($owner->id);
        
        $base_html = Yii::app()->controller->renderModelView($base_bill_model,'payed_with');
        
        $display_pay_button = $owner->unreleased_amount > 0 && ($owner->isBill || $owner->isPreBill);

        $onclick_array = ['sima.accounting.BillPay',
            $owner->id,
            $owner->partner_id,
            $owner->partner_bank_account_id,
            $owner->call_for_number,
            $owner->call_for_number_modul,
            $owner->bill_number,
            $owner->unreleased_amount,
            Yii::app()->configManager->get('accounting.default_payment_type',false),
            Yii::app()->configManager->get('accounting.default_payment_code',false)
        ];

        
        return [
            'base_html' => $base_html,
            'display_pay_button' => $display_pay_button,
            'onclick_array' => $onclick_array
        ];
    }
}
