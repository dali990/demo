<?php

class BillReliefGUI extends SIMAActiveRecordGUI
{
    public function columnLabels()
    {
        return [
            'relief_bill' => 'Knjizno odobrenje',
            'relief_bill_id' => 'Knjizno odobrenje',
            'percent_of_bill' => 'Procenat',
            'percent_of_relief_bill' => 'Procenat',
            'bill_id' => 'Račun',
            'bill' => 'Račun',
            'amount' => 'Iznos',
            'base_amount' => 'Osnovica',
            'vat_amount' => 'PDV',
            'vat_rate' => 'Stopa PDV'
        ];
    }
    
    public function modelLabel($plural = false)
    {
        return 'Rasknjizeno';
    }
    
    public function columnDisplays($column)
    {
        $owner = $this->owner;
        switch ($column)
        {
            case 'relief_bill.bill_number': return parent::columnDisplays('relief_bill');
            case 'bill.bill_number': return parent::columnDisplays('bill');
            case 'percent_of_bill': 
                if (floatval($owner->bill->amount)==0)
                {
                    return '? %';
                }
                else
                {
                    return SIMAHtml::money_format((floatval($owner->amount)*100.0/floatval($owner->bill->amount)),'%');
                }
            case 'percent_of_relief_bill': return SIMAHtml::money_format((floatval($owner->amount)*100.0/floatval($owner->relief_bill->amount)),'%');
            default: return parent::columnDisplays($column);
        }
    }
    
    public function modelForms()
    {
        return array(
            'default' => array(
                'type' => 'generic',
                'columns' => array(
                    'bill_id' => ['relation','relName' => 'bill','disabled'=>'disabled'] ,
                    'relief_bill_id' => ['relation','relName' => 'relief_bill','disabled'=>'disabled'] ,
                    'internal_vat' => 'checkBox',
                    'vat_rate' => 'dropdown',
                    'amount' => 'numberField',
                    'base_amount' => 'numberField',
                    'vat_amount' => 'numberField',
                )
            )
        );
    }
    
    public function droplist($key, $relName='', $filter=null)
    {
        $owner = $this->owner;
        switch ($key)
        {
            case 'vat_rate': return ['0' => '0%', '10' => '10%', '20' => '20%'];
            default: return parent::droplist($key, $relName, $filter);
        }
    }

    public function guiTableSettings($type)
    {
        switch ($type)
        {
            case 'inBill':return [
                'columns' => [
                    'relief_bill.bill_number',
                    'amount',
                    'percent_of_bill',
                    'base_amount',
                    'vat_amount',
                    'relief_bill.comment'
                ]
            ];
            case 'inReliefBill':return [
                'columns' => [
                    'bill.bill_number',
                    'amount',
                    'percent_of_relief_bill',
                    'base_amount',
                    'vat_amount',
                    'relief_bill.comment'
                ]
            ];
            default: return [
                'columns' => [
                    'bill','relief_bill','amount'
                ]
            ];
        }
        
    }
}