<?php

class AccountingFixedAssetGUI extends SIMAActiveRecordDisplayGUI
{

    public function columnLabels()
    {
        return array(
            'order' => 'Fin #',
//            'fixed_asset_type_id' => 'Tip',
//            'fixed_asset_type' => 'Tip',
//            'serial_number' => 'Serijski broj',
//            'manufacturer' => 'Proizvođač',
//            'year_built_id' => 'Godina proizvodnje',
//            'year_built' => 'Godina proizvodnje',
//            'year_built_text' => 'Godina proizvodnje',
//            'model' => 'Model',
//            'capacity' => 'Max. Nosivost(kg)',
            'cost_location_id' => 'Mesto troška - finansije',
            'cost_location' => 'Mesto troška - finansije',
//            'inventory_number' => 'Inventarski broj',
            'purchase_value' => 'Nabavna vrednost',
            'current_purchase_value' => 'Trenutna nabavna vrednost',
            'current_value' => 'Trenutna vrednost',
            'current_value_start' => 'Početna trenutna vr.',
//            'location' => 'Lokacija', 
            'depreciation_rate' => 'Stepen amortizacije %',
            'local_depreciation_rate' => 'Stepen amortizacije %',
            'custom_depreciation_rate' => 'Poseban stepen am.',
//            'acquisition_document_id' => 'Dokument o pribavljanju',
//            'acquisition_document' => 'Dokument o pribavljanju',
            'usage_start_date' => 'Datum pocetka upotrebe',
            'depreciation' => 'Ukupan otpis'
        )+parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return 'Osnovno sredstvo | FIN';
    }
    
    public function columnDisplays($column)
    {
        $owner = $this->owner;
        switch ($column)
        {
            case 'depreciation':
            case 'current_value': return parent::columnDisplays($column).'*';
//            case 'fixed_asset_type': return isset($owner->fixed_asset_type) ? $owner->fixed_asset_type->display_name_full : '?';
//            case 'addFixedAssetType':
//                {
//                    $result = '';
//                    $passenger_type_id = Yii::app()->configManager->get('fixedAssets.vehicles.passenger_type_id', false);
//                    $freight_type_id = Yii::app()->configManager->get('fixedAssets.vehicles.freight_type_id', false);
//                    $work_machines_type_id = Yii::app()->configManager->get('fixedAssets.vehicles.work_machines_type_id', false);
//                    $vehicle_types = [$passenger_type_id, $freight_type_id, $work_machines_type_id];
//                    
//                    if(in_array($owner->fixed_asset_type_id, $vehicle_types))
//                    {
//                        $vehicle_model = isset($owner->vehicle)?$owner->vehicle:Vehicle::model();
//                        $result = SIMAHtml::modelFormOpen($vehicle_model, array(
//                            'init_data'=>array(
//                                'Vehicle'=>array(
//                                    'type'=>[
//                                        'disabled',
//                                        'init'=>$owner->fixed_asset_type->DisplayName
//                                    ]
//                                ),
//                                'FixedAsset'=>[
//                                    'fixed_asset_type_id'=>[
//                                        'disabled',
//                                        'init'=>$owner->fixed_asset_type_id
//                                    ]
//                                ]
//                            ),                
//                            'model_id'=>$owner->id
//                        ));
//                    }
//                }
//                return $result;
//                break;
            default : return parent::columnDisplays($column);
        }
    }
    
    public function modelViews()
    {
        return [
//            'indexTitle',
            'basic_info' => ['params_function' => 'basicInfoParams'],
        ] + parent::modelViews();
    }
    
    public function basicInfoParams()
    {
        $owner = $this->owner;
        if (
                !isset($owner->fixed_asset->acquisition_document->account_document)
                ||
                !$owner->fixed_asset->acquisition_document->account_document->isBooked
                )
        {
            $delete_button = SIMAHtml::modelDelete($owner);
        }
        else
        {
            $delete_button = '';
        }
        return [
            'delete_button' => $delete_button
        ];
    }
    
    public function afterValidateDependencyError(array $foreign_dependencies)
    {        
        $allowed_tables = [
            FixedAssetsDepreciationItem::tableName().':accounting_fixed_asset_id'
        ];
        
        $parsed_foreign_dependencies = array_diff($foreign_dependencies, $allowed_tables);
        
        parent::afterValidateDependencyError($parsed_foreign_dependencies);
    }
    
    public function guiTableSettings($type)
    {
        if (FixedAsset::isCustomInventoryType())
        {
            $inv_number_column = 'fixed_asset.custom_inventory_number';
        }
        else
        {
            $inv_number_column = 'fixed_asset.inventory_number.number';
        }
        switch ($type)
        {
            default: return array(
                'columns' => [
                    'order' => ['min_width' => '40px','max_width'=>'55px'],
                    'fixed_asset.name',
                    'fixed_asset.active',
                    'fixed_asset.quantity',
                    'fixed_asset.cost_location',
                    $inv_number_column => ['min_width' => '40px','max_width'=>'50px'],
                    'current_purchase_value', 
                    'depreciation',
                    'current_value', 
                    'usage_start_date', 
                    'fixed_asset.acquisition_document', 
                    'fixed_asset.location', 
                    'depreciation_rate',
                    'fixed_asset.fixed_asset_type', 
                    'fixed_asset.serial_number', 
                    'fixed_asset.description',
                    
                ],
                'order_by' => [
                    'order',
                    'usage_start_date', 
                    $inv_number_column
                ]
            );
        }
    }
    
    public function modelForms()
    {
        return array(
            'default'=>array(
                'type'=>'generic',
                'columns'=>array(
                    'order' => 'textField',
//                    'cost_location_id' => ['searchField', 'relName'=>'cost_location', 'add_button'=>true],
                    'purchase_value' => 'numberField', 
                    'current_value_start' => 'numberField', 
                    'custom_depreciation_rate' => 'checkBox',
                    'local_depreciation_rate' => ['numberField','dependent_on' => [
                        'custom_depreciation_rate' => [
                            'onValue' => ['show', 'for_values' => true, 'set_empty_on_hide' => false],
                        ]
                    ]],
                    'usage_start_date' => 'datetimeField',
//                    'cost_location_id' => ['searchField', 'relName'=>'cost_location', 'add_button'=>true],
                )
            )
        );
    }
    
    public function getDisplayModel()
    {
        return $this->owner->fixed_asset;
    }
}