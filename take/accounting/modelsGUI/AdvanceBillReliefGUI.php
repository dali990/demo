<?php

class AdvanceBillReliefGUI extends SIMAActiveRecordGUI
{
    public function columnLabels()
    {
        return [
            'advance_bill' => 'Avansni racun',
            'advance_bill_id' => 'Avansni racun',
            'percent_of_bill' => 'Procenat',
            'percent_of_advance_bill' => 'Procenat',
            'relief_bill_id' => 'Knjizno odobrenje',
            'relief_bill' => 'Knjizno odobrenje',
            'amount' => 'Iznos',
            'base_amount' => 'Osnovica',
            'vat_amount' => 'PDV',
            'vat_rate' => 'Stopa PDV'
        ];
    }
    
    public function modelLabel($plural = false)
    {
        return 'Rasknjizeno';
    }
    
    public function columnDisplays($column)
    {
        $owner = $this->owner;
        switch ($column)
        {
            case 'percent_of_relief_bill': return SIMAHtml::money_format((floatval($owner->amount)*100.0/floatval($owner->relief_bill->amount)),'%');
            case 'percent_of_advance_bill': return SIMAHtml::money_format((floatval($owner->amount)*100.0/floatval($owner->advance_bill->amount)),'%');
            default: return parent::columnDisplays($column);
        }
    }
    
    public function modelForms()
    {
        return array(
            'default' => array(
                'type' => 'generic',
                'columns' => array(
                    'relief_bill_id' => ['relation','relName' => 'relief_bill','disabled'=>'disabled'] ,
                    'advance_bill_id' => ['relation','relName' => 'advance_bill','disabled'=>'disabled'] ,
                    'internal_vat' => 'checkBox',
                    'vat_rate' => 'dropdown',
                    'amount' => 'numberField',
                    'base_amount' => 'numberField',
                    'vat_amount' => 'numberField',
                )
            )
        );
    }

    public function guiTableSettings($type)
    {
        switch ($type)
        {
            case 'inAdvanceBill':return [
                'columns' => [
                    'relief_bill','amount',//'percent_of_bill',
                    'base_amount',
                    'vat_amount',
                    'relief_bill.comment'
                ]
            ];
            case 'inReliefBill':return [
                'columns' => [
                    'advance_bill','amount',//'percent_of_advance_bill',
                    'base_amount',
                    'vat_amount',
                    'advance_bill.comment'
                ]
            ];
            default: return [
                'columns' => [
                    'relief_bill','advance_bill','amount'
                ]
            ];
        }
        
    }
}