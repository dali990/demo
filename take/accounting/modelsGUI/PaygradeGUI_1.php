<?php

class PaygradeGUI extends SIMAActiveRecordGUI
{
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default:
                return [
                    'columns' => ['code', 'name', 'points']
                ];
        }
    }
    
}

