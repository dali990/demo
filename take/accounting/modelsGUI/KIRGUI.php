<?php

class KIRGUI extends SIMAActiveRecordGUI
{
    public function columnLabels()
    {
        return [
            'bill' => Yii::t('AccountingModule.Bill','Bill'),
            'bill_id' => Yii::t('AccountingModule.Bill','Bill')
        ] + parent::columnLabels();
    }
    
    public function columnDisplays($column)
    {
        $owner = $this->owner;
        switch ($column)
        {
            case 'KIR_7':
            case 'KIR_8':
            case 'KIR_9':
            case 'KIR_9a':
            case 'KIR_10':
            case 'KIR_11':
            case 'KIR_12':
            case 'KIR_13':
            case 'KIR_14':
            case 'KIR_15':
            case 'KIR_16':
            case 'KIR_17':
            case 'KIR_18':
                if (SIMAMisc::areEqual($owner->$column, 0))
                {
                    return '';
                }
                else
                {
                    return parent::columnDisplays($column);
                }
            default: return parent::columnDisplays($column);
        }   
    }
    
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default:
                return array(
                    'columns' => [
//                        'id',
                        'bill',
                        'kir_7',
                        'kir_8',
                    ]
                );
        }	
    }
    
}