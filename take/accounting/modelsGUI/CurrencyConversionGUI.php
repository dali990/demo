<?php

class CurrencyConversionGUI extends SIMAActiveRecordGUI
{
    public function columnLabels()
    {
        return array(
            'payment_in' => Yii::t('AccountingModule.CurrencyConversion', 'PaymentIn'),
            'payment_in_id' => Yii::t('AccountingModule.CurrencyConversion', 'PaymentIn'),
            'payment_out' => Yii::t('AccountingModule.CurrencyConversion', 'PaymentOut'),
            'payment_out_id' => Yii::t('AccountingModule.CurrencyConversion', 'PaymentOut'),
            'payment_in.bank_statement.account' => Yii::t('AccountingModule.CurrencyConversion', 'PaymentInAccount'),
            'payment_out.bank_statement.account' => Yii::t('AccountingModule.CurrencyConversion', 'PaymentOutAccount'),
            'payment_out.amount' => Yii::t('AccountingModule.CurrencyConversion', 'PaymentOutAmount'),
            'payment_in.amount' => Yii::t('AccountingModule.CurrencyConversion', 'PaymentInAmount'),
            'payment_in.bank_statement.account.currency' => Yii::t('AccountingModule.CurrencyConversion', 'PaymentInCurrency'),
            'payment_out.bank_statement.account.currency' => Yii::t('AccountingModule.CurrencyConversion', 'PaymentOutCurrency'),
            'exchange_rate' => Yii::t('AccountingModule.CurrencyConversion', 'ExchangeRate'),
            'middle_currency_rate' => Yii::t('AccountingModule.CurrencyConversion', 'MiddleCurrencyRate')
        ) + parent::columnLabels();
    }
    
    public function columnDisplays($column)
    {
        $owner = $this->owner;
        switch ($column)
        {
            case 'exchange_rate':
                $result = '';
                
                $larger_payment = $owner->payment_out;
                $lower_payment = $owner->payment_in;
                
                if($larger_payment->amount < $lower_payment->amount)
                {
                    $larger_payment = $owner->payment_in;
                    $lower_payment = $owner->payment_out;
                }
                
                $larger_currency = $larger_payment->bank_statement->account->currency;
                $lower_currency = $lower_payment->bank_statement->account->currency;
                
                $result_num = $larger_payment->amount/$lower_payment->amount;
                
                if($owner->isAnyOfPaymentsFromCountryCurrencyAccount())
                {                    
                    $currency_money_format = $larger_currency->DisplayName;
                    if($lower_currency->isCountryCurrency())
                    {
                        $currency_money_format = $lower_currency->DisplayName;
                    }
                    
                    $result = SIMAHtml::money_format($result_num, $currency_money_format);
                }
                else
                {
                    $result = SIMAHtml::money_format($result_num, $larger_currency->DisplayName)
//                            .' '.Yii::t('AccountingModule.CurrencyConversion', 'For').' '.
//                            .' -/- '.
//                            .' :/: '.
                            .' : '.
                            SIMAHtml::money_format(1, $lower_currency->DisplayName);
                }
                
                return $result;
            case 'middle_currency_rate':
                return $this->middleCurrencyRateDisplay();
            default: return parent::columnDisplays($column);
        }
    }
    
    public function modelForms()
    {
        $currency_conversion_payment_type = Yii::app()->configManager->get('accounting.currency_conversion_payment_type');
        
        $payment_in_filter = [
            'bank_statement' => [
                'account' => [
                    'partner' => [
                        'ids' => Yii::app()->company->id
                    ]
                ]
            ],
            'invoice' => true
        ];
        $payment_out_filter = [
            'bank_statement' => [
                'account' => [
                    'partner' => [
                        'ids' => Yii::app()->company->id
                    ]
                ]
            ],
            'invoice' => false
        ];
        
        if(!empty($currency_conversion_payment_type))
        {
            $payment_in_filter['payment_type_id'] = $currency_conversion_payment_type;
            $payment_out_filter['payment_type_id'] = $currency_conversion_payment_type;
        }
        
        return array(
            'default' => array(
                'type' => 'generic',
                'columns' => array(
                    'payment_out_id' => array('searchField', 'relName' => 'payment_out', 
                        'add_button'=>true,
                        'filters' => $payment_out_filter
                    ),
                    'payment_in_id' => array('searchField', 'relName' => 'payment_in',
                        'add_button' => true,
                        'filters' => $payment_in_filter
                    )
                )
            )
        );
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default:
                return [
                    'columns' => array(
                        'payment_out',
                        'payment_out.bank_statement.account',
                        'payment_out.amount',
                        'payment_out.bank_statement.account.currency',
                        'payment_in', 
                        'payment_in.bank_statement.account',
                        'payment_in.amount',
                        'payment_in.bank_statement.account.currency',
                        'exchange_rate',
                        'middle_currency_rate'
                    )
                ];
        }
    }
    
    private function middleCurrencyRateDisplay()
    {        
        $result = '';
                
        if($this->owner->isAnyOfPaymentsFromCountryCurrencyAccount())
        {
            $result = $this->middleCurrencyRateDisplayWhenOneIsCountryCurrency();
        }
        else
        {
            $result = $this->middleCurrencyRateDisplayWhenNoneIsCountryCurrency();
        }
        
        return $result;
    }
    
    private function middleCurrencyRateDisplayWhenOneIsCountryCurrency()
    {
        $result = '';
        
        $owner = $this->owner;
        
        $out_currency = $owner->payment_out->bank_statement->account->currency;
        $in_currency = $owner->payment_in->bank_statement->account->currency;
        
        $date = null;
        $currency = null;
        $currency_money_format = '';

        if($owner->isOutPaymentFromCountryCurrencyAccount())
        {
            $date = $owner->payment_in->payment_date;
            $currency = $in_currency;
            $currency_money_format = $out_currency->DisplayName;
        }
        else
        {
            $date = $owner->payment_out->payment_date;
            $currency = $out_currency;
            $currency_money_format = $in_currency->DisplayName;
        }

        $model_filter = [
            'date' => $date,
            'currency' => [
                'ids' => $currency->id
            ]
        ];

        $criteria = new SIMADbCriteria([
            'Model' => 'MiddleCurrencyRate',
            'model_filter' => $model_filter
        ], true);

        if(empty($criteria->ids))
        {
            $result = Yii::t('AccountingModule.CurrencyConversion', 'NoMiddleCurrencyRateForDate', 
                                [
                                    '{date}' => $date,
                                    '{currency}' => $currency->DisplayName
                                ]);
        }
        else
        {
            $middleCurrencyRate = MiddleCurrencyRate::model()->findByPk($criteria->ids);
            $result = SIMAHtml::money_format($middleCurrencyRate->value, $currency_money_format);
        }
        
        return $result;
    }
    
    private function middleCurrencyRateDisplayWhenNoneIsCountryCurrency()
    {
        $result = '';
                
        $owner = $this->owner;
        
        $out_currency = $owner->payment_out->bank_statement->account->currency;
        $in_currency = $owner->payment_in->bank_statement->account->currency;
        
        $date = $owner->payment_in->payment_date;
        
        $criteriaIn = new SIMADbCriteria([
            'Model' => 'MiddleCurrencyRate',
            'model_filter' => [
                'date' => $date,
                'currency' => [
                    'ids' => $in_currency->id
                ]
            ]
        ], true);
        $criteriaOut = new SIMADbCriteria([
            'Model' => 'MiddleCurrencyRate',
            'model_filter' => [
                'date' => $date,
                'currency' => [
                    'ids' => $out_currency->id
                ]
            ]
        ], true);

        if(empty($criteriaIn->ids) || empty($criteriaOut->ids))
        {
            if(empty($criteriaIn->ids))
            {
                $result .= '<p>'.Yii::t('AccountingModule.CurrencyConversion', 'NoMiddleCurrencyRateForDate', 
                                [
                                    '{date}' => $owner->payment_in->payment_date,
                                    '{currency}' => $in_currency->DisplayName
                                ]).'</p>';
            }

            if(empty($criteriaOut->ids))
            {
                $result .= '<p>'.Yii::t('AccountingModule.CurrencyConversion', 'NoMiddleCurrencyRateForDate', 
                                [
                                    '{date}' => $owner->payment_out->payment_date,
                                    '{currency}' => $out_currency->DisplayName
                                ]).'</p>';
            }
        }
        else
        {
            $larger_payment = $owner->payment_out;

            if($owner->payment_out->amount < $owner->payment_in->amount)
            {
                $larger_payment = $owner->payment_in;
            }
                        
            $larger_currency = $larger_payment->bank_statement->account->currency;
            
            $middleCurrencyRateIn = MiddleCurrencyRate::model()->find($criteriaIn);
            $middleCurrencyRateOut = MiddleCurrencyRate::model()->find($criteriaOut);
            
            $inInDin = $middleCurrencyRateIn->value;
            $outInDin = $middleCurrencyRateOut->value;
            
            $result_num = 0;
            
            if($inInDin > $outInDin)
            {
               $result_num =  $inInDin/$outInDin;
            }
            else
            {
                $result_num =  $outInDin/$inInDin;
            }

//            $result_num = $larger_payment->amount/$lower_payment->amount;
            
            $result = SIMAHtml::spanWithInfoBox(
//                $out_currency->DisplayHTML.' -> '.$in_currency->DisplayHTML,
                SIMAHtml::money_format($result_num, $larger_currency->DisplayName),
                'accounting/currencyConversion/foreignConversionInfo',
                [
                    'conversionId' => $owner->id
                ]
            );
        }
        
        return $result;
    }
}

