<?php

class ReportGUI extends SIMAActiveRecordGUI
{
    public function columnLabels()
    {
        return [
//            'bill' => Yii::t('AccountingModule.Bill','Bill'),
//            'bill_id' => Yii::t('AccountingModule.Bill','Bill'),
//            'kpr_8' => Yii::t('AccountingModule.KPR','KPR_8'),
//            'KPR_8' => Yii::t('AccountingModule.KPR','KPR_8'),
//            'kpr_9' => Yii::t('AccountingModule.KPR','KPR_9'),
//            'KPR_9' => Yii::t('AccountingModule.KPR','KPR_9'),
//            'kpr_14' => Yii::t('AccountingModule.KPR','KPR_14'),
//            'KPR_14' => Yii::t('AccountingModule.KPR','KPR_14'),
//            'kpr_14b' => Yii::t('AccountingModule.KPR','KPR_14b'),
//            'KPR_14b' => Yii::t('AccountingModule.KPR','KPR_14b'),
        ] + parent::columnLabels();
    }

    public function columnDisplays($column)
    {
        $owner = $this->owner;
        switch ($column)
        {
            case 'model_link':
                if (!empty($owner->model_name))
                {
                    $model_name = $owner->model_name;
                    $model = $model_name::model()->findByPk($owner->model_id);
                    if (!is_null($model))
                    {
                        return $model->DisplayHtml;
                    }
                    else
                    {
                        return 'nema';
                    }
                }
                else
                {
                    return '-'.$owner->model_name.' _ '.$owner->model_id;
                }
//            case 'KPR_9':
//            case 'KPR_9a':
//            case 'KPR_10':
//            case 'KPR_11':
//            case 'KPR_12':
//            case 'KPR_13':
//            case 'KPR_14':
//            case 'KPR_15':
//            case 'KPR_16':
//            case 'KPR_17':
//            case 'KPR_18':
//            case 'KPR_19':
//                if (SIMAMisc::areEqual($owner->$column, 0))
//                {
//                    return '';
//                }
//                else
//                {
//                    return parent::columnDisplays($column);
//                }
            default: return parent::columnDisplays($column);
        }   
    }
    
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default:
                return array(
                    'columns' => [
                        'cost_location_id',
                        'cost_type_id',
                        'partner_id',
                        'month_id',
                        'year_id',
                        'income',
                        'income_notpayed',
                        'income_advance',
                        'cost',
                        'cost_notpayed',
                        'cost_advance',
                        'model_link'
//                        'id',
//                        'bill',
//                        'KPR_8',
//                        'KPR_9',
//                        'KPR_14',
                    ]
                );
        }	
    }
    
}