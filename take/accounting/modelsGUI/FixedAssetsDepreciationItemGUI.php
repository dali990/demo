<?php

/**
 * @package SIMA
 * @subpackage Accounting
 */
class FixedAssetsDepreciationItemGUI extends SIMAActiveRecordGUI
{
    public function columnLabels() {
        return array(
            'order' => Yii::t('BaseModule.Common','Order'),
            'accounting_fixed_asset' => 'Osnovno sredstvo',
            'accounting_fixed_asset_id' => 'Osnovno sredstvo',
            'depreciation' => 'Otpis u godini',
            'depreciation_before' => 'Otpis pre',
            'depreciation_after' => 'Otpis nakon',
            'value_before' => 'Vrednost pre',
            'value_after' => 'Vrednost nakon',
            'note' => Yii::t('BaseModule.Common', 'Note'),
            'quantity_on_end_of_deprication_year' => Yii::t('AccountingModule.FixedAssets','QuantityOnEndOfDepricationYear'),
            'purchase_value_on_end_of_deprication_year' => Yii::t('AccountingModule.FixedAssets','PurchaseValueOnEndOfDepricationYear')
        )+parent::columnLabels();
    }
    
    public function columnDisplays($column) 
    {
        $owner = $this->owner;

        switch ($column) {
            case 'quantity_on_end_of_deprication_year':
                return $owner->accounting_fixed_asset->fixed_asset->quantityOnDate(new SIMADateTime("31.12.{$owner->fixed_assets_depreciation->year->year}."));
            case 'purchase_value_on_end_of_deprication_year':
                return $owner->accounting_fixed_asset->purchaseValueOnDate(new SIMADateTime("31.12.{$owner->fixed_assets_depreciation->year->year}."));
            default: return parent::columnDisplays($column);
        }
    }
    
    public function modelViews()
    {
        return array(
//            'inFile'=>array('class'=>'basic_info')
        );
    }
    
    public function modelForms()
    {
        return array(
            'default'=>array(
                'type'=>'generic',
                'columns'=>array(
                    'order'=>'textField',
                    'accounting_fixed_asset_id'=>array('relation','relName'=>'accounting_fixed_asset'),
                    'depreciation'=>'numberField',
                    'note'=>'textArea'
                )
            ));
    } 
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            case 'inFixedAsset': return [
                'columns' => [
                    'fixed_assets_depreciation.year','depreciation', 'quantity_on_end_of_deprication_year', 'note'
                ]
            ];
            default:
                return array(
//                    'columns' => ['order','accounting_fixed_asset','previous_value', 'new_value', 'depreciation', 'note'],
                    'columns' => [
                        'order',
                        'accounting_fixed_asset.order',
                        'accounting_fixed_asset',
                        'accounting_fixed_asset.depreciation_rate',
                        'purchase_value_on_end_of_deprication_year',
                        'value_before',
                        'value_after', 
                        'depreciation_before',
                        'depreciation', 
                        'depreciation_after',
                        'note'
                    ],
                    'sums' => ['depreciation']
                );
        }
//        parent::guiTableSettings($type);
    }

}
