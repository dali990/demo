<?php

class BankStatementGUI extends SIMAActiveRecordGUI
{

    public function columnLabels()
    {
        return array(
            'account_id'=>'Račun',
            'account'=>'Račun',
            'date'=>'Datum',
            'amount'=>'Stanje na dan',
            'calcAmount'=>'Stanje na dan(sracunato)',
            'prevAmount'=>'Prethodno stanje',
            'unreleased'=>'Nerasknjizeno',
            'file_version_id'=>'Skenirano',
            'year' => 'Godina',
            'year_id' => 'Godina',
            'number' => 'Broj izvoda',
            'file.account_document.account_order.date' => 'Datum knjizenja',
        ) + parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return 'Izvod iz banke';
    }

    public function columnDisplays($column)
    {
        $owner = $this->owner;

        switch ($column)
        {
            case 'account_id': 		return $owner->account->DisplayName;
            case 'date':                return $owner->date;
            case 'calcAmount':		
                    {
                            return money_format('%!i', $owner->calcAmount)
                            .((abs($owner->amount-$owner->calcAmount)>0.001)?'('.money_format('%!i', ($owner->calcAmount - $owner->amount)).')':'')
                            ;
                    }
            case 'prevAmount':	case 'amount':	 case 'calcAmount': case 'unreleased':
                    return money_format('%!i', $owner->$column);      
            default: return parent::columnDisplays($column);
        }
    }

    public function modelViews()
    {
        return array(
            'basicInfo' => array('class' => 'basic_info bank_statement'),
            'inFile'
        );
    }
    
    public function modelForms()
    {
        return array(
//            'multidates'=>array(
//                'type'=>'generic',
//                'columns'=>array(
//                    'date'=>'hidden',
//                    'amount' => 'hidden',
//                    'account_id'=>'dropdown',
//                    'file_version_id' => 'fileField',
//                )
//            ),
            'default'=>array(
                'type'=>'generic',
                'columns'=>array(   
                    'id' => 'hidden',
                    'account_id' => 'dropdown',
                    'date' => 'datetimeField',
                    'amount' => 'numberField',
                    'number' => 'numberField',
                    'file_version_id' => 'fileField',
                )
            ),
        );
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default: 
                return array(
                    'columns'=>array(
                        'account_id','date','prevAmount','amount','calcAmount','unreleased',
                        'number',
                        'file.account_document.account_order',
                        'file.account_document.account_order.date'
                    ),
                    'filters'=>array(
                        'year'
                    ),
                    'order_by' => array(
                        'account_id','date','amount','number'
                    )
                );
        }
    }

}