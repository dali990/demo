<?php

class AdvanceBillOutGUI extends AdvanceBillGUI {

    public function columnLabels() {
        return array(
        ) + parent::columnLabels();
    }

    public function modelViews() {
        return array(
            'inConcreteOrder'=>array('style'=>'border: 1px grey dashed; margin: 5px; padding: 5px;', 'with'=>'file')
        )+parent::modelViews();
    }

}