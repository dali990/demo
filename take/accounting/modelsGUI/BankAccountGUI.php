<?php

class BankAccountGUI extends SIMAActiveRecordGUI
{

    public function columnLabels()
    {
        return array(
            'bank_id' => 'Banka',
            'number' => 'Broj računa',
            'start_amount' => 'Početno stanje',
            'comment' => 'Komentar',
            'first_account_order' => 'Prvi nalog',
            'currency' => Yii::t('AccountingModule.BankAccount', 'Currency'),
            'currency_id' => Yii::t('AccountingModule.BankAccount', 'Currency')
        )+parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return Yii::t('AccountingModule.BankAccount', $plural?'BankAccounts':'BankAccount');
    }
    
    public function columnDisplays($column)
    {
        $owner = $this->owner;
        switch ($column)
        {
            default: return parent::columnDisplays($column);
        }
    }

    public function modelViews()
    {
        return array(
            'default'//=>array('style'=>'border: 1px solid;'),
//            'tabTemplates'=>array('with'=>array('templated_file'),'style'=>'border: 1px solid;')  //.creator
        );
    }
    
    public function modelForms()
    {
        $owner = $this->owner;
        
        $start_amount = 'hidden';
        $first_account_order = 'hidden';
        if (!empty($owner->partner_id) && intval($owner->partner_id) === Yii::app()->company->id)
        {
            $start_amount = 'numberField';
            $first_account_order = 'textField';
        }
        
        return array(
            'default'=>array(
                'type'=>'generic',
                'columns'=>array(
                    'partner_id' => 'hidden',                    
                    'number' => ['bankAccountField'],
                    'bank_id' => ['searchField',
                        'relName'=>'bank', 
                        'add_button' => true, 
                        'disabled' => true,
                        'dependent_on' => [
                            'number'=>[
                                'onFocusOut' => ['trigger', 'func' => ['sima.accountingMisc.bankDependentOnHendler'], 'init_trigger' => false],
                                'onKeyUp' => ['trigger', 'func' => ['sima.accountingMisc.bankDependentHendlerOnKeyUp']]
                            ]
                        ]
                    ],
                    'start_amount' => $start_amount,
                    'first_account_order' => $first_account_order,
                    'comment' => 'textArea',
                    'currency_id' => ['searchField','relName'=>'currency']
                )
            ),
            'fromFile'=>array(
                'type'=>'generic',
                'columns'=>array(
                    'partner_id'=>array('searchField','relName'=>'partner', 'add_button'=>array('action'=>'simaAddressbook/index')),
                    'number'=>'textField',
                    'bank_id'=>array('searchField','relName'=>'bank'),
                    'start_amount'=>$start_amount,
                    'comment'=>'textArea',
                    'currency_id' =>array('searchField','relName'=>'currency')
                )
            )
            
        );
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            case 'warn_belong_to_partner_not_person_nor_company': return [
                'columns' => [
                    'number', 'bank', 'start_amount', 'currency', 'first_account_order', 'comment', 'partner'
                ]
            ];
            default:
                return array(
                    'columns' => array(
                        'number', 'bank', 'start_amount', 'currency', 'first_account_order', 'comment'
                    )
                );
        }
    }
}