<?php

class KIRManualGUI extends SIMAActiveRecordGUI
{

    public function columnLabels()
    {
        return array(
            'order_in_year' => '#',
            'bill_id' => 'Račun',
            'bill' => 'Račun',
            'reason' => 'Razlog',
            'month' => 'Mesec',
            'month_id' => 'Mesec',
            'column_7' => Yii::t('AccountingModule.KIR','KIR_7'),
            'column_8' => Yii::t('AccountingModule.KIR','KIR_8'),
            'column_9' => Yii::t('AccountingModule.KIR','KIR_9'),
            'column_10' => Yii::t('AccountingModule.KIR','KIR_10'),
            'column_11' => Yii::t('AccountingModule.KIR','KIR_11'),
            'column_12' => Yii::t('AccountingModule.KIR','KIR_12'),
            'column_13' => Yii::t('AccountingModule.KIR','KIR_13'),
            'column_14' => Yii::t('AccountingModule.KIR','KIR_14'),
            'column_15' => Yii::t('AccountingModule.KIR','KIR_15'),
            'column_16' => Yii::t('AccountingModule.KIR','KIR_16'),
            'column_17' => Yii::t('AccountingModule.KIR','KIR_17'),
            'column_7_compare' => Yii::t('AccountingModule.KIR','KIR_7'),
            'column_8_compare' => Yii::t('AccountingModule.KIR','KIR_8'),
            'column_9_compare' => Yii::t('AccountingModule.KIR','KIR_9'),
            'column_10_compare' => Yii::t('AccountingModule.KIR','KIR_10'),
            'column_11_compare' => Yii::t('AccountingModule.KIR','KIR_11'),
            'column_12_compare' => Yii::t('AccountingModule.KIR','KIR_12'),
            'column_13_compare' => Yii::t('AccountingModule.KIR','KIR_13'),
            'column_14_compare' => Yii::t('AccountingModule.KIR','KIR_14'),
            'column_15_compare' => Yii::t('AccountingModule.KIR','KIR_15'),
            'column_16_compare' => Yii::t('AccountingModule.KIR','KIR_16'),
            'column_17_compare' => Yii::t('AccountingModule.KIR','KIR_17'),
            'bill.file.account_document.account_order.date' => 'Datum knjizenja'
        )+parent::columnLabels();
    }

    public function columnDisplays($column)
    {
        $owner = $this->owner;
        switch ($column)
        {
            case 'reason': return KIRManual::$reasons[$owner->$column];
            case 'bill.bill_number': return parent::columnDisplays('bill');
            case 'column_7_compare': return $this->compareInHTML('column_7', 'KIR_7');
            case 'column_8_compare': return $this->compareInHTML('column_8', 'KIR_8');
            case 'column_9_compare': return $this->compareInHTML('column_9', 'KIR_9');
            case 'column_10_compare': return $this->compareInHTML('column_10', 'KIR_10');
            case 'column_11_compare': return $this->compareInHTML('column_11', 'KIR_11');
            case 'column_12_compare': return $this->compareInHTML('column_12', 'KIR_12');
            case 'column_13_compare': return $this->compareInHTML_b('column_13', 'KIR_13');
            case 'column_14_compare': return $this->compareInHTML('column_14', 'KIR_14');
            case 'column_15_compare': return $this->compareInHTML_b('column_15', 'KIR_15');
            case 'column_16_compare': return $this->compareInHTML('column_16', 'KIR_16');
            case 'column_17_compare': return $this->compareInHTML('column_17', 'KIR_17');
//            case 'KPR_8':
//            case 'KPR_9':
//            case 'KPR_10':
//            case 'KPR_11':
//            case 'KPR_12':
//            case 'KPR_13':
//            case 'KPR_14':
//            case 'KPR_15':
//                return ($owner->$column==0)?'':  SIMAHtml::number_format($owner->$column);
            default: return parent::columnDisplays($column);
        }
        
    }

    private function compareInHTML($local_column, $KIR_column)
    {
        $owner = $this->owner;
        $local_value = $owner->$local_column;
        if (is_null($owner->kir))
        {
            $KIR_value = 0;
            $text = "pozvan je KIR compareInHTML za ID a nije trebao: ".$owner->id;
            Yii::app()->errorReport->createAutoClientBafRequest($text);
        }
        else
        {
            $KIR_value = $owner->kir->$KIR_column;
        }
        
        if (!SIMAMisc::areEqual($local_value,$KIR_value))
        {
            $diff = SIMAHtml::number_format(floatval($local_value) - floatval($KIR_value),3);
            $local_value = SIMAHtml::number_format($local_value,3);
            $KIR_value = SIMAHtml::number_format($KIR_value,3);
            return '(manual)'.$local_value.' <br />(SIMA)'.$KIR_value.' <br />(diff)'.$diff;
        }
        else
        {
            return '';
        }
    }
    
    private function compareInHTML_b($local_column, $KIR_column)
    {
        $owner = $this->owner;
        $local_value = $owner->$local_column;
        if (is_null($owner->kir))
        {
            $KIR_value = 0;
            $booking_value = 0;
            $text = "pozvan je KIR compareInHTML za ID a nije trebao: ".$owner->id;
            Yii::app()->errorReport->createAutoClientBafRequest($text);
        }
        else
        {
            $KIR_column_b = $KIR_column.'b';
            $KIR_value = $owner->kir->$KIR_column;
            $booking_value = $owner->kir->$KIR_column_b;
        }
        
        if (
                !SIMAMisc::areEqual($local_value,$KIR_value)
                ||
                !SIMAMisc::areEqual($local_value,$booking_value)
                ||
                !SIMAMisc::areEqual($booking_value,$KIR_value)
                
                
                )
        {
            $diff1 = SIMAHtml::number_format(floatval($local_value) - floatval($KIR_value),3);
            $diff2 = SIMAHtml::number_format(floatval($local_value) - floatval($booking_value),3);
            
            $local_value = SIMAHtml::number_format($local_value,3);
            $KIR_value = SIMAHtml::number_format($KIR_value,3);
            $booking_value = SIMAHtml::number_format($booking_value,3);
            return '(manual)'.$local_value.' <br />'
                    . '(SIMA)'.$KIR_value.' <br />'
                    . '(diff1)'.$diff1.' <br />'
                    . '(NZK)'.$booking_value.' <br />'
                    . '(diff2)'.$diff2;
        }
        else
        {
            return '';
        }
    }
    
    public function modelForms() 
    {
        return [
            'default' => array(
                'type' => 'generic',
                'columns' => array(
                    'order_in_year' => 'numberField',
                    'bill_id' => ['searchField','relName'=>'bill',],
                    'reason' => ['dropdown'],
                    'month_id' => ['searchField','relName'=>'month',],
                    'column_7' => 'numberField',                            
                    'column_8' => 'numberField',                            
                    'column_9' => 'numberField',                            
                    'column_10' => 'numberField',                            
                    'column_11' => 'numberField',                            
                    'column_12' => 'numberField',                            
                    'column_13' => 'numberField',                            
                    'column_14' => 'numberField',                            
                    'column_15' => 'numberField',                            
                    'column_16' => 'numberField',                            
                    'column_17' => 'numberField',                         
                )
            )
        ];
    }
            
            
            
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            case 'kir_compare':
                return array(
                    'columns' => [
                        'order_in_year' => ['min_width' => '40px','max_width' => '40px'],
                        'bill.bill_number',
                        'bill.partner',
                        'column_7_compare',
                        'column_8_compare',
                        'column_9_compare',
                        'column_10_compare',
                        'column_11_compare',
                        'column_12_compare',
                        'column_13_compare',
                        'column_14_compare',
                        'column_15_compare',
                        'column_16_compare',
                        'column_17_compare',
//                        'bill.file.account_document.account_order.date', 
//                        'bill_number', 'income_date', 'partner', 'pib_jmbg',
//                        'KPR_8', 'KPR_9', 'KPR_10', 'KPR_11', 'empty', 'KPR_13', 'KPR_14', 'KPR_15',
                    ],
                    'order_by' => 'order_in_year'
                );
            default:
                return array(
                    'columns' => [
                        'order_in_year' => ['min_width' => '40px','max_width' => '40px'],
                        'bill.file.account_document.account_order.date' => [
                            'min_width' => '80px',
                            'max_width' => '80px'
                        ],
                        'bill' => ['edit' => 'relation'],
                        'reason' => ['edit' => 'dropdown'],
                        'bill.income_date' => [
                            'min_width' => '80px',
                            'max_width' => '80px'
                        ],
                        'bill.partner',
                        'bill.partner.pib_jmbg' => [
                            'min_width' => '80px',
                            'max_width' => '110px'
                        ],
//                        'month' => ['edit' => 'relation'],
                        'column_7' => ['edit' => 'numberField','min_width' => '50px'],
                        'column_8' => ['edit' => 'numberField','min_width' => '50px'],
                        'column_9' => ['edit' => 'numberField','min_width' => '50px'],
                        'column_10' => ['edit' => 'numberField','min_width' => '50px'],
                        'column_11' => ['edit' => 'numberField','min_width' => '50px'],
                        'column_12' => ['edit' => 'numberField','min_width' => '50px'],
                        'column_13' => ['edit' => 'numberField','min_width' => '50px'],
                        'column_14' => ['edit' => 'numberField','min_width' => '50px'],
                        'column_15' => ['edit' => 'numberField','min_width' => '50px'],
                        'column_16' => ['edit' => 'numberField','min_width' => '50px'],
                        'column_17' => ['edit' => 'numberField','min_width' => '50px'],
//                        'bill.file.account_document.account_order.date', 
//                        'bill_number', 'income_date', 'partner', 'pib_jmbg',
//                        'KPR_8', 'KPR_9', 'KPR_10', 'KPR_11', 'empty', 'KPR_13', 'KPR_14', 'KPR_15',
                    ],
                    'sums' => [
                        'column_7',
                        'column_8',
                        'column_9',
                        'column_10',
                        'column_11',
                        'column_12',
                        'column_13',
                        'column_14',
                        'column_15',
                        'column_16',
                        'column_17',
                    ],
                    'order_by' => 'order_in_year'
                );
        }	
    }
    
    public function droplist($key, $relName = '', $filter = null)
    {
        switch ($key)
        {
            case 'reason': return KPRManual::$reasons;
            default: return parent::droplist($key, $relName, $filter);
        }
        
    }
    
}