<?php

class AccountOrderGUI extends SIMAActiveRecordGUI
{

    public function columnLabels()
    {
        return array(
            'order' => Yii::t('BaseModule.Common','Order'),
            'id' => Yii::t('BaseModule.Common','Document'),
            'document' => Yii::t('BaseModule.Common','Document'),
            'note' => Yii::t('BaseModule.Common','Note'),
            'date' => Yii::t('BaseModule.Common','Date'),
            'booked' => Yii::t('AccountingModule.AccountOrder','Booked'),
            'year_id' => Yii::t('BaseModule.Common','Year'),
            'year' => Yii::t('BaseModule.Common','Year'),
        ) + parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return Yii::t('AccountingModule.AccountOrder','AccountOrder');
    }
    
    protected function getDisplayHtmlText()
    {
        $owner = $this->owner;
        $displ_name = $owner->DisplayName;
        $booked = SIMAHtml::status($owner,'booked');
        return $booked.$displ_name;
    }
    
    public function columnDisplays($column)
    {
        $owner = $this->owner;
        switch ($column)
        {
            default: return parent::columnDisplays($column);
        }
    }
    
    public function modelViews()
    {
        return [
            'account_documents',
            'options_in_file' => ['html_wrapper' => null]
        ];
    }

    public function modelForms()
    {
        $owner = $this->owner;
        if ($owner->booked)
        {
            $order = ['textField','disabled'=>'disabled'];
            $date = ['datetimeField','disabled'=>'disabled'];
        }
        else
        {
            $order = ['textField'];
            $date = ['datetimeField'];
        }
        
        return array(
            'default' => array(
                'type' => 'generic',
                'columns' => array(
                    'order' => $order,
                    'date' => $date,
                    'note' => 'textArea'
                )
            )
        );
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default: return array(
                'columns' => array(
                    'order' => ['max_width' => '70px','min_width' => '60px'], 
                    'date' => ['max_width' => '80px','min_width' => '80px'], 
                    'saldo' => ['max_width' => '70px','min_width' => '50px'],
                    'note'
                ),
                'filters' => array(
                    'year'
                ),
                'order_by' => ['order','date']
            );
        }
    }
    
    public function droplist($key, $relName='', $filter=null)
    {
        switch ($key)
        {
            case 'saldo':return [
                'diff0' => Yii::t('BaseModule.Common','Different than zero')
            ];
            default: return parent::droplist($key, $relName, $filter);
        }
    }
    
    public function afterValidateDependencyError(array $foreign_dependencies)
    {
        $allowed_tables = [
            AccountDocument::model()->tableName().':account_order_id',
            AccountDocument::model()->tableName().':canceled_account_order_id'
        ];

        $parsed_foreign_dependencies = array_diff($foreign_dependencies, $allowed_tables);
        
        parent::afterValidateDependencyError($parsed_foreign_dependencies);
    }
    
    public function generatePdf($version_name = '')
    {
        $owner = $this->owner;
        $account_order_report = new AccountOrderReport([
            'account_order' => $owner
        ]);          
        $temp_file = $account_order_report->getPDF();
        
        if (empty($version_name))
        {
            $version_name = $owner->DisplayName;
        }
        $owner->file->appendTempFile(
            $temp_file->getFileName(), 
            Yii::t('AccountingModule.AccountOrder','AccountOrder') . '_' . $version_name . '.pdf'
        );
        return $temp_file;
    }
}