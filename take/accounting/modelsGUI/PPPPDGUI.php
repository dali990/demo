<?php

class PPPPDGUI extends SIMAActiveRecordDisplayGUI
{
    public function columnLabels()
    {
        return [
            'client_identificator' => Yii::t('AccountingModule.PPPPD', 'ClientIdentificator'),
            'file' => File::model()->modelLabel(),
            'create_time' => Yii::t('AccountingModule.PPPPD', 'CreateTime'),
            'note' => Yii::t('AccountingModule.PPPPD', 'Note'),
            'creation_type' => Yii::t('AccountingModule.PPPPD', 'CreationType'),
            '_client_uploaded_file' => Yii::t('AccountingModule.PPPPD', 'ClientUploadedFile'),
            'compare' => Yii::t('AccountingModule.PPPPD', 'CompareWithOtherPPPD'),
            'year_id' => AccountingYear::model()->modelLabel(),
            'dirty' => Yii::t('AccountingModule.PPPPD', 'Dirty'),
            '_paycheck_period_id' => PaycheckPeriod::model()->modelLabel()
        ] + parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return Yii::t('AccountingModule.PPPPD', 'PPPPD');
    }
    
    public function modelViews()
    {
        return [
            'basic_info',
            'full_info',
            'dirty'
        ] + parent::modelViews();
    }
    
    public function columnDisplays($column)
    {
        $owner = $this->owner;
        switch ($column)
        {
            case 'creation_type':
                $result = Yii::t('BaseModule.Common', 'Unknown');
                
                $creationTimeDisplayData = PPPPDGUI::getCreationTimeDisplayData();
                if(isset($creationTimeDisplayData[$owner->$column]))
                {
                    $result = $creationTimeDisplayData[$owner->$column];
                }
                
                return $result;
            case 'compare':
                $action = [
                    'title' => Yii::t('AccountingModule.PPPPD', 'ChoosePPPDToCompareWithThis'),
                    'onclick' => [
                        'sima.ppppd.choosePPPPDToCompareWithExisting',
                        $owner->id
                    ]
                ];
                $result = '';
                if(SIMAMisc::isVueComponentEnabled())
                {
                    $result = Yii::app()->controller->widget(SIMAButtonVue::class, $action, true);
                }
                else
                {
                    $result = Yii::app()->controller->widget('SIMAButton', [
                        'action' => $action
                    ], true);
                }
                return $result;
            case 'create_new_file_version':
                $action = [
                    'title' => Yii::t('AccountingModule.PPPPD', 'CreatePPPPDNewFileVersion'),
                    'onclick' => [
                        'sima.ppppd.createPPPPDNewFileVersion',
                        $owner->id
                    ]
                ];
                $return = '';
                if(SIMAMisc::isVueComponentEnabled())
                {
                    $return = Yii::app()->controller->widget(SIMAButtonVue::class, $action, true);
                }
                else
                {
                    $return = Yii::app()->controller->widget('SIMAButton', [
                        'action' => $action
                    ], true);
                }
                return $return;
            default: return parent::columnDisplays($column);
        }
    }
    
    public function modelForms()
    {
        return [
            'default' => [
                'type' => 'generic',
                'columns' => [
                    'note' => 'textArea',
                    'dirty' => 'checkBox'
                ]
            ],
            'user_upload' => [
                'type' => 'generic',
                'columns' => [
                    'creation_type' => 'hidden',
                    'client_identificator' => 'textField',
                    'note' => 'textArea',
                    '_client_uploaded_file' => ['fileField', 'required' => true, 'allowed_extansions' => ['xml'], 'repmanager_upload' => false],
                    'year_id' => ['searchField','relName'=>'year', 'add_button'=>true]
                ]
            ],
            'paycheck_period_ppppd_tab' => [
                'type' => 'generic',
                'columns' => [
                    'creation_type' => 'hidden',
                    'year_id' => 'hidden',
                    '_paycheck_period_id' => 'hidden',
                    'client_identificator' => 'textField',
                    'note' => 'textArea',
                ]
            ]
        ];
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            case 'compare_choose': 
                return [
                    'columns' => [
                        'client_identificator',
                        'file',
                        'create_time',
                        'file.last_version.modification_time',
                        'note',
                        'creation_type',
                        'dirty'
                    ],
                    'order_by' => ['create_time'],
                ];
            default:
                return [
                    'columns' => [
                        'client_identificator',
                        'file',
                        'create_time',
                        'file.last_version.modification_time',
                        'note',
                        'creation_type',
                        'compare',
                        'dirty'
                    ],
                    'order_by' => ['create_time'],
                ];
        }
    }
    
    public function droplist($key, $relName='', $filter=null)
    {
        switch ($key)
        {
            case 'creation_type':
                return PPPPDGUI::getCreationTimeDisplayData();
            default: return parent::droplist($key, $relName, $filter);
        }
    }
    
    public static function getCreationTimeDisplayData()
    {
        return [
            PPPPD::$CREATE_TYPE_SIMA_GENERATE => Yii::t('AccountingModule.PPPPD', PPPPD::$CREATE_TYPE_SIMA_GENERATE),
            PPPPD::$CREATE_TYPE_USER_UPLOAD => Yii::t('AccountingModule.PPPPD', PPPPD::$CREATE_TYPE_USER_UPLOAD),
            PPPPD::$CREATE_TYPE_USER_MANUAL => Yii::t('AccountingModule.PPPPD', PPPPD::$CREATE_TYPE_USER_MANUAL),
        ];
    }
}

