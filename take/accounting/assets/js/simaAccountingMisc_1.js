/* global sima */

function SIMAAccountingMisc()
{
    this.addWarehouseAsCostLocation = function(_this, cost_location_id)
    {
        sima.ajax.get('accounting/warehouse/warehouse/addWarehouseAsCostLocation', {
            get_params:{
                cost_location_id : cost_location_id
            },
            success_function: function(response){
                sima.misc.refreshUiTabForObject(_this,2);
            }
        });
    };
    
    this.calcUnreleasedPercentAmount = function(obj)
    {
        var current_unreleased_percent = obj.find('.current_unreleased_percent');
        current_unreleased_percent.show();
        var table = obj.find('.proposals .sima-guitable');
        var current_unreleased_percent_amount = 0.00;
        table.simaGuiTable('getSelected').each(function(){
            var value = $(this).find('td.unreleased_percent_of_amount').text();            
            value = parseFloat($.trim(value.replace(/%/g, '').replace(/\s/g, '')));            
            current_unreleased_percent_amount += value;
        });
        current_unreleased_percent.find('.amount').text((100-current_unreleased_percent_amount).toFixed(2)+'%');
        if (current_unreleased_percent_amount <= 100)
        {
            current_unreleased_percent.find('.amount').removeClass('overflow');
        }
        else
        {
            current_unreleased_percent.find('.amount').addClass('overflow');
        }
    };
    
    
//    this.showAccountStart = function(table_id, year_id, account_id, _this)
//    {
//        var table = $('#'+table_id);
//        var filter = {};
//        if (table.hasClass('showAccountStart'))
//        {
//            table.removeClass('showAccountStart');
//            filter = {
//                account:{
//                    year:{
//                        ids:year_id
//                    },
//                },
//                display_scopes: [
//                    'withSaldoAndNoStart'
//                ]
//            };
//            _this.text('Uračunaj start');
//        }
//        else
//        {
//            table.addClass('showAccountStart');            
//            filter = {
//                account:{
//                    year:{
//                        ids:year_id
//                    },
//                },
//                display_scopes: {
//                    'withSaldoAndStart':account_id
//                }
//            };
//            _this.text('Umanji start');
//        }
//        table.simaGuiTable('setFixedFilter', filter, true);        
//    };
    
    this.exportAccountLayouts = function(account_layouts_table_id)
    {
        var account_layouts_table = $('#'+account_layouts_table_id);
        var law_id = account_layouts_table.data('law_id');
        
        if (typeof law_id !== 'undefined')
        {
            sima.ajax.get('accounting/accountPlans/exportAccountLayouts', {
                get_params:{
                    law_id : law_id
                },
                success_function: function(response){
                    sima.temp.download(response.fn, response.dn);                
                }
            });
        }
        else
        {
            sima.dialog.openWarn('Niste selektovali zakon kontnog okvira');
        }
    };
    
    this.importAccountLayoutsType = function(account_layouts_table_id)
    {
        var account_layouts_table = $('#'+account_layouts_table_id);
        var law_id = account_layouts_table.data('law_id');
        
        if (typeof law_id !== 'undefined')
        {
            sima.dialog.openYesNo('Da li želite da prepišete postojeći plan?', 
                function(){
                    sima.dialog.close();
                    sima.accountingMisc.importAccountLayouts(law_id, account_layouts_table, true);
                },
                function(){
                    sima.dialog.close();
                    sima.accountingMisc.importAccountLayouts(law_id, account_layouts_table, false);
                }
            );
        }
        else
        {
            sima.dialog.openWarn('Niste selektovali zakon kontnog okvira');
        }
    };
    
    this.importAccountLayouts = function(law_id, account_layouts_table, overwrite_plans)
    {        
        var params = {
            status: 'validate',
            onSave: function(response){
                sima.ajax.get('accounting/accountPlans/importAccountLayouts', {
                    get_params: {
                        law_id: law_id,
                        upload_session_key: response.form_data.File.attributes.file_version_id,
                        overwrite_plans: overwrite_plans
                    },
                    async:true,
                    loadingCircle: account_layouts_table,
                    success_function:function(response)
                    {
                        account_layouts_table.simaGuiTable('populate');
                    }
               });
            },
            formName: 'justupload'
        };
        sima.model.form('File', '', params);
    };
    
    this.exportAccountOrderToPdf = function(account_order_id)
    {
        sima.ajaxLong.start('accounting/books/exportAccountOrderToPdf', {
            showProgressBar: $('body'),
            data:{
                account_order_id: account_order_id
            },
            onEnd: function(response) {                                        
                sima.temp.download(response.filename, response.downloadname);                                        
            }
        });
    };
    
    this.exportAccountToPdf = function(table_id)
    {
        var value = $('#'+table_id).find('.sima-gui-table-filter[column="account_booked"] .sima-ui-sf:first').simaSearchField('getValue');
        if (typeof value.id !== 'undefined')
        {
            var guitable_filter = $('#'+table_id).simaGuiTable('getAllFilters');
            sima.ajaxLong.start('accounting/books/exportAccountToPdf', {
                showProgressBar: $('#'+table_id),
                data:{
                    account_id: value.id,
                    guitable_filter: guitable_filter
                },
                onEnd: function(response) {                                        
                    sima.temp.download(response.filename, response.downloadname);                                        
                }
            });
       }
    };
    
    this.listIosCandidateCompanies = function(right_panel_id, year_id)
    {
        if(year_id !== null)
        {
            sima.ajax.get('accounting/ios/listIosCandidateCompanies', {
                get_params: {
                    year_id: year_id
                },
                async:true,
                loadingCircle:$('body'),
                success_function:function(response)
                {
                    if(response.privilege)
                    {
                        if(response.ios_date !== null)
                        {
                            if(response.count_companies > 1)
                            {
                                var html = response.html;
                                html.title = response.title;
                                var candidate_companies_element = response.id;
//                                    var ios_date = response.ios_date;
//                                    var account_code = response.account_code;
                                var proper_year_id = response.proper_year_id;
                                var yes_function = {};
                                var no_function = {};
                                yes_function.func = function(){
                                    var companies = [];
                                    $("#"+candidate_companies_element).find('.sima-ioses-companies-list').find('.sima-ioses-companies-list-item').each(
                                        function(){
                                            if($(this).find('input').is(":checked"))
                                            {
                                                companies.push($(this).data('company_id'));
                                            }
                                        }
                                    );
                                    if(companies.length === 0)
                                    {
                                        sima.dialog.openWarn(
                                                sima.translate('ChooseAtLeastOnePartner'), 
                                                sima.translate('IosGeneration')
                                                );
                                    } else {
                                        sima.dialog.close();
                                        sima.ajaxLong.start('accounting/ios/generateIoses', {
                                            showProgressBar: $('#'+right_panel_id),
                                            data: {
                                                year_id: proper_year_id,
                                                companies: companies
                                            },
                                            onEnd:function(response)
                                            {
                                                $('#'+right_panel_id).simaGuiTable('populate');
                                            }
                                        });
                                    }
                                };
                                yes_function.title = sima.translate('Generate');
                                no_function.title = sima.translate('Cancel');
                                sima.dialog.openYesNo(response.html, yes_function, no_function);
                            } else {
                                sima.dialog.openWarn(
                                sima.translate('ForChoosenYearThereAreNoCompanies'), 
                                sima.translate('IosCompanies')
                            );
                            }
                        } else {
                            sima.dialog.openWarn(
                                sima.translate('IosGenerationDateNeccessary'), 
                                sima.translate('IosDate')
                            );
                        }
                    } else {
                        sima.dialog.openWarn(
                            sima.translate('IosPrivilegeNeeded'),
                            sima.translate('IOS')
                        );
                    }
                }
           });
        }
    };
    
    this.generateIosForCompany = function(ioses_table_id, company_id)
    {
        sima.ajax.get('accounting/ios/generateIosForCompanyDialog', {
            success_function: function(response) 
            {
                if(response.privilege)
                {
                    var yes_function = {};
                    yes_function.title = sima.translate('Generate');
                    var no_function = {};
                    no_function.title = sima.translate('Cancel');
                    var dialog_id = response.dialog_id;

                    yes_function.func = function(){
                        var date = $('#'+dialog_id).prop('value');

                        sima.ajax.get('accounting/ios/generateIosForCompany', {
                            get_params: {
                                company_id: company_id,
                                date: date
                            },
                            success_function: function(response) {
                                if(response.status)
                                {
                                    sima.dialog.close();
                                    $('#'+ioses_table_id).simaGuiTable('populate');
                                } else {
                                    sima.dialog.openWarn(response.message, sima.translate('IOS'));
                                }
                            }
                        });
                    };

                    sima.dialog.openYesNo(response.html, yes_function, no_function);

                } else {
                    sima.dialog.openWarn(
                        sima.translate('IosPrivilegeNeeded'),
                        sima.translate('IOS')
                    );
                }
            }
        });
    };
    
    this.setIosDate = function(right_panel_header_id){
        
        var year_id = $('#'+right_panel_header_id).data('year_id');
        
        sima.model.form('AccountingYear',year_id,{
            formName:'only_ios_date',
            onSave: function(response){
                date = sima.date.format(response.form_data.AccountingYear.attributes.ios_date);
                $('#'+right_panel_header_id).find('.ios_date_value').html(date);
            }
        });
        
    };
    
    this.regenerateIos = function(ios_id, guitable_id, btn_id)
    {
        var loading_item = null;
        if (btn_id !== '')
        {
            loading_item = $("#" + btn_id).parents('.sima-guitable:first');
        }
        if (guitable_id !== '')
        {
            loading_item = $("#" + guitable_id);
        }
        sima.ajax.get('accounting/ios/regenerateIos', {
            get_params: {
                ios_id: ios_id
            },
            async:true,
            loadingCircle: loading_item,
            success_function: function(response){
                //ovo je trenutno ok, ne ispisuje nista console
                //i onako treba u vue, pa ce onda biti ok
                if (btn_id !== '')
                {
                    $("#" + btn_id).parents('.sima-guitable:first').simaGuiTable('refresh_row', [ios_id]);
                }
                if (guitable_id !== '')
                {
                    $("#" + guitable_id).simaGuiTable('populate');
                }
            }
        });
    };
    
    this.iosTabInCompanyOnYearSelectHendler = function(company_id, uniq_id)
    {
        var years_vmenu = $("#years_vmenu_"+uniq_id);
        var right_panel = $("#right_panel_wrap_"+uniq_id);

        years_vmenu.on('selectItem', function(event, item) {
            var year_id = item.data('code');
            sima.ajax.get('accounting/ios/renderIosesForCompanyAndYear', {
                get_params: {
                    year_id: year_id,
                    company_id: company_id,
                    uniq_id: uniq_id
                },
                success_function: function(response){
                    sima.set_html(right_panel, response.html);
                }
            });
        });
    };
    
    this.addBillsToAccountOrder = function(table_id)
    {
        var table = $('#' + table_id);
        
        var bill_ids = table.simaGuiTable('getSelectedIds');
        
        if (!sima.isEmpty(bill_ids))
        {
            sima.ajax.get('accounting/books/checkBillsForAccountOrder', {
                data: {
                    bill_ids: bill_ids
                },
                success_function: function(response){
                    sima.model.choose('AccountOrder', function(data) {
                        sima.ajax.get('accounting/books/addBillsToAccountOrder', {
                            get_params: {
                                account_order_id: data.id
                            },
                            data: {
                                bill_ids: bill_ids
                            },
                            success_function: function(){
                                
                            }
                        });
                    }, 
                    {
                        select_filter: {
                            year: {
                                ids: response.bills_year_id
                            }
                        }
                    });
                }
            });
        }
        else
        {
            sima.dialog.openWarn(sima.translate('YouHaveNotSelectedAnyBills'));
        }
    };
    
    this.addBankAccountPartnerFromNBS = function(add_partner_btn_id, pib, mb)
    {
        sima.model.form('Company', '', {
            init_data: {
                Company: {
                    PIB: pib,
                    MB: mb
                }
            },
            onLoad: function(form_id) {
                var form = $('#' + form_id);
                if (!sima.isEmpty(pib))
                {
                    form.find('.sima-model-form-row[data-column="PIB"] .fill-from-nbs-in-form').click();
                }
                else if (!sima.isEmpty(mb))
                {
                    form.find('.sima-model-form-row[data-column="MB"] .fill-from-nbs-in-form').click();
                }
            },
            onSave: function(response) {
                var partner_id = response.model_id;
                var partner_display = response.form_data.Company.display_name;
                var add_partner_btn = $('#' + add_partner_btn_id);
                var form_partner_row = add_partner_btn.parents('.add-bank-account-from-bank-statement:first').find('._bank-account-form .sima-model-form-row[data-column="partner_id"]');
                
                add_partner_btn.parents('._add-partner-btn:first').hide();
                form_partner_row.find('.sima-ui-sf:first').simaSearchField('setValue', partner_id, partner_display);
            }
        });
    };

    this.exportVat = function(month_id, export_type, with_questionmark)
    {
        sima.ajaxLong.start(
            'accounting/vat/exportVat',
            {
                showProgressBar: $('body'),
                data: {
                    month_id: month_id,
                    export_type: export_type,
                    with_questionmark: with_questionmark
                },
                onEnd: function(response)
                {
                    sima.temp.download(response.filename, response.downloadname);
                }
            }
        );
    };
    
    this.billCostTypeDependentOnBillIncomeDateInForm = function(form, dependent_on_field, dependent_field, apply_actions_func)
    {
        var income_date = dependent_on_field.val();
        if (!sima.isEmpty(income_date))
        {
            sima.ajax.get('base/base/getYearIdFromDate', {
                get_params: {
                    date: income_date
                },
                success_function: function(response) {
                    var filter = $.extend(true, {}, dependent_field.simaSearchField('getFilter'));
                    var initial_filter = filter.initial;
                    
                    if (sima.isEmpty(initial_filter['cost_types_to_accounts']))
                    {
                        initial_filter['cost_types_to_accounts'] = {
                            account: {
                                year: {
                                    ids: response.year_id
                                }
                            }
                        };
                    }
                    else if (sima.isEmpty(initial_filter['cost_types_to_accounts']['account']))
                    {
                        initial_filter['cost_types_to_accounts']['account'] = {
                            year: {
                                ids: response.year_id
                            }
                        };
                    }
                    else
                    {
                        initial_filter['cost_types_to_accounts']['account']['year'] = {
                            ids: response.year_id
                        };
                    }

                    dependent_field.simaSearchField('setFilter', initial_filter, 'initial');
                    apply_actions_func('enable');
                }
            });
        }
        else
        {
            apply_actions_func('disable');
        }
    };
    
    this.billPdfSuggestion = function(bill_id)
    {
        sima.ajaxLong.start(
            'accounting/bill/billPdfSuggestion',
            {
                showProgressBar: $('body'),
                data: {
                    bill_id: bill_id
                },
                onEnd: function(response)
                {
                    sima.dialog.openYesNo(
                        {
                            title: sima.translate('BillPdfSuggestion'),
                            html: response.html
                        }, 
                        {
                            title: sima.translate('Save'),
                            func: function onConfirm() {
                                sima.dialog.close();
                                sima.ajax.get('accounting/bill/billPdfAddNewVersionFromTempFile', {
                                    get_params:{
                                        bill_id: bill_id,
                                        temp_file_name: response.temp_file_name,
                                        temp_file_downloadname: response.temp_file_downloadname
                                    },
                                    async: true,
                                    loadingCircle: $('#body_sync_ajax_overlay'),
                                    success_function: function(response){

                                    }
                                });
                            }
                        },
                        {
                            title: sima.translate('Cancel')
                        }, 
                        null, null, true
                    );
                }
            }
        );
    };
    
    this.bankDependentOnHendler = function(form, dependent_on_field, dependent_field, apply_actions_func)
    {
        //Prva provera dodata za slucaj kada se otvori forma i omdah zatvori, u tom slucaju dependent_on_field.data je undefined 
        if(!sima.isEmpty(dependent_field.data()) && dependent_on_field.data('old_value') !== dependent_on_field.val())
        {
            if(!dependent_on_field.bankAccountField('isEmpty'))
            {
                var bank_code = dependent_on_field.val().substring(0,3);
                sima.ajax.get('accounting/accounting/getBankModelByBankCode', {
                    get_params:{
                        code: bank_code
                    },
                    success_function: function(response){
                        var bank = response;
                        dependent_field.simaSearchField('setValueWithoutEvents', bank.id, bank.display_name);
                    }
                });
            }
            else
            {
                dependent_field.simaSearchField('setValueWithoutEvents', "", "");
            }
        }      
    };
    
    this.bankDependentHendlerOnKeyUp = function(form, dependent_on_field, dependent_field, apply_actions_func, event)
    {
        if(event.keyCode === 13)
        {
            event.stopPropagation();
            dependent_on_field.trigger('blur');
        }
    };
}