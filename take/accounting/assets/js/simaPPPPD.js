/* global sima */

function SIMAPPPPD()
{
    this.performComparePPPPDs = function(sima_generated_span_id, user_uploaded_span_id)
    {        
        var sima_generated_span = $('#'+sima_generated_span_id);
        var user_uploaded_span = $('#'+user_uploaded_span_id);
        
        var sima_generated_ppppd_id = sima_generated_span.data('ppppd_id');
        var user_uploaded_ppppd_id = user_uploaded_span.data('ppppd_id');
        
        if(sima.isEmpty(sima_generated_ppppd_id))
        {
            sima.dialog.openWarn(sima.translate('ComparePPPPDNotChosenSimaGenerated'));
            return;
        }
        if(sima.isEmpty(user_uploaded_ppppd_id))
        {
            sima.dialog.openWarn(sima.translate('ComparePPPPDNotChosenUserUploaded'));
            return;
        }
        
        compareTwoPPPPDs(sima_generated_ppppd_id, user_uploaded_ppppd_id);
    };
    
    this.choosePPPPDForCompare = function(span_for_data_id, type)
    {
        sima.model.choose('PPPPD',function(data){
            if(!sima.isEmpty(data))
            {                
                var ppppd_id = data[0].id;
                var ppppd_display_name = data[0].display_name;
                
                var span_for_data = $('#'+span_for_data_id);
                span_for_data.data('ppppd_id', ppppd_id);
                span_for_data.html(ppppd_display_name);
            }
        },{
            title: sima.translate('ChoosePPPPDForCompare'),
            view: 'guiTable',
            fixed_filter: {
                creation_type: type
            },
            params: {
                add_button: false
            }
        });
    };
    
    this.choosePPPPDToCompareWithExisting = function(ppppd_id)
    {
        sima.model.choose('PPPPD',function(data){
            if(!sima.isEmpty(data))
            {                
                var chosen_ppppd_id = data[0].id;
                compareTwoPPPPDs(ppppd_id, chosen_ppppd_id);
            }
        },{
            title: sima.translate('ChoosePPPPDForCompare'),
            view: 'guiTable',
            params: {
                columns_type: 'compare_choose'
            }
        });
    };
    
    function compareTwoPPPPDs(ppppd1_id, ppppd2_id)
    {
        sima.ajaxLong.start('accounting/PPPPD/performComparePPPPDs', {
            showProgressBar:$('body'),
            data:{
                ppppd1_id: ppppd1_id,
                ppppd2_id: ppppd2_id
            },
            onEnd: function(response) {
                sima.diffViewer.renderFromHtml(response.html, {
                    save_func_hidden: true
                });
            }
        });
    }
    
    this.createPPPPDNewFileVersion = function(ppppd_id)
    {
        sima.ajax.get('accounting/PPPPD/createPPPPDNewFileVersion', {
            get_params: {                        
                ppppd_id: ppppd_id
            },
            success_function: function() {
                sima.dialog.openInfo(sima.translate('PPPPDFileVersionCreated'));
            }
        });
    };
}


