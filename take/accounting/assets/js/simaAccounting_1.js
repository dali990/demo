/* global sima */

function SIMAAccounting()
{
    this.unreleasing = new SIMAAccountingUnreleasing();
    
    function readPayments(config_id, upload_session_key, refreshTable)
    {
        if(sima.isEmpty(upload_session_key))
        {
            sima.addError('SIMAAccounting::readPayments', 'upload_session_key is empty: '+JSON.stringify(upload_session_key));
            return;
        }
        sima.ajaxLong.start('accounting/bankStatement/createBankStatements',{
            showProgressBar: refreshTable,
            data:{
                config_id: config_id,
                upload_session_key: upload_session_key
            },
            onEnd: function(response){
                if(response.message !== 'success')
                {
                    if(typeof(response.account_number) !== 'undefined')
                    {
                        sima.dialog.openYesNo(
                            {
                                title: sima.translate('AddBankAccount'),
                                html: response.add_bank_account_html
                            }, 
                            {
                                title: sima.translate('Save'),
                                func: function() {
                                    sima.icons.submitForm(response.form_id, {
                                        on_success_func: function(submit_status) {
                                            if (submit_status !== 'validation_fail')
                                            {
                                                readPayments(config_id, upload_session_key, refreshTable);
                                            }
                                        }
                                    });
                                }
                            }, 
                            {
                                hidden: true
                            }, null, null, true
                        );
                    }
                    else
                    {
                        sima.dialog.openWarn("Doslo je do greske:\n"+response.message);
                    }
                }
                else
                {
                    if (typeof refreshTable !== 'undefined')
                    {
                        refreshTable.simaGuiTable('populate');
                    }
                    sima.dialog.openInfo("Zavrseno uctavanje izvoda.");
                }
            }
        });
    }    
    
    this.addAccountsForPartner = function(partner_id)
    {
        sima.ajax.get('accounting/accounts/addAccountsForPartner',{
            get_params:{
                partner_id:partner_id
            },
            success_function:function(response){
                if (response.status==='getCode')
                {
                    sima.dialog.openPrompt('Nije napisan algoritab zaizracunavanje koda, zadajte kod rucno:',function(response){
                        sima.ajax.get('accounting/accounts/addAccountsForPartner',{
                            get_params:{
                                partner_id:partner_id,
                                code:response
                            }
                        });
                    });
                }
            }
        });
    };
    
    this.BookDocument = function(id, pg_id, style)
    {
        if (typeof style === 'undefined')
        {
            style = 'account_document';
        }
        var $pg_table = $(pg_id);
        sima.ajaxLong.start('accounting/books/bookingPrediction',{
            showProgressBar:$pg_table,
            data:{
                id: id,
                style: style
            },
            onEnd: function(){
                $pg_table.simaGuiTable('populate');
            }
        });
    };
    
    this.book_document_diffs = function(account_order_id, table_selector)
    {
        sima.ajaxLong.start('accounting/books/getBookDocumentDiffs',{
            showProgressBar:$(table_selector),
            data:{
                account_order_id: account_order_id
            },
            onEnd: function(response){
                sima.diffViewer.renderFromHtml(response.html, {
                    confirm_diff_func: function(account_documents_cache){
                        sima.ajax.get('accounting/books/confirmAccountDocumentDiffs',{
                            get_params:{
                                account_order_id:account_order_id
                            },
                            data: {
                                account_documents_cache: account_documents_cache
                            },
                            async: true,
                            loadingCircle: $('#body_sync_ajax_overlay'),
                            success_function:function()
                            {
                                $(table_selector).simaGuiTable('populate');
                            }
                        });
                    }
                });
            }
        });
    };
    
    function selectBookkeepingTab(button_id)
    {
        if (typeof button_id !== 'undefined')
        {
            $('#'+button_id)
                    .parents('.sima-page-wrap:first')
                    .find('.sdl-tabs:first')
                    .simaTree2('selectItem', 'bookkeeping');
            
            var vue_tabs = $('#'+button_id).parents('.sima-tabs-vue:first');
            if (!sima.isEmpty(vue_tabs))
            {
                var vue_tabs_vue = sima.vue.getVueComponentFromObj(vue_tabs);
                vue_tabs_vue.reloadTab('bookkeeping');
            }
        }
    };
    
    this.generateAccountDocument = function(document_id, button_id)
    {
        sima.ajax.get('accounting/books/generateAccountDocument',{
            get_params: {
                document_id: document_id
            },
            success_function: function(response){
                if (typeof response.year_id !== 'undefined')
                {
                    sima.accounting.addAccountDocumentChoose(document_id, 1, button_id);
                }
                else
                {
                    selectBookkeepingTab(button_id);
                }
                
            }
        });
    };
    
    this.addAccountDocument = function(document_id,account_order_id, button_id, forced_change)
    {
        if (typeof forced_change === 'undefined')
        {
            forced_change = false;
        }
        sima.ajax.get('accounting/books/addAccountDocument',{
            get_params: {
                account_order_id: account_order_id,
                document_id: document_id,
                forced_change: forced_change
            },
            success_function:function(){
                selectBookkeepingTab(button_id);
            }
        });
    };

    this.addAccountDocumentChoose = function(file_id, year_id, button_id)
    {
        sima.model.choose('AccountOrder',function(obj){sima.accounting.addAccountDocument(file_id, obj[0].id, button_id);},{
            view: 'guiTable',
            select_filter: {
                year: {
                    ids: year_id
                }
            }
        });
    };
    this.changeAccountDocumentChoose = function(file_id, year_id)
    {
        sima.model.choose('AccountOrder',function(obj){sima.accounting.addAccountDocument(file_id, obj[0].id, null, true);},{
            view: 'guiTable',
            select_filter: {
                year: {
                    ids: year_id
                }
            }
        });
    };
    
    this.addCancelAccountDocument = function(document_id,account_order_id, forced_change)
    {
        if (typeof forced_change === 'undefined')
        {
            forced_change = false;
        }
        sima.ajax.get('accounting/books/addCancelAccountDocument',{
            get_params: {
                account_order_id: account_order_id,
                document_id: document_id,
                forced_change: forced_change
            }
        });
    };
    this.removeCancelAccountDocument = function(document_id)
    {
        sima.ajax.get('accounting/books/removeCancelAccountDocument',{
            get_params: {
                document_id: document_id
            }
        });
    };
    
    this.addCancelAccountDocumentChoose = function (file_id, year_id)
    {
        sima.model.choose('AccountOrder',function(obj){
            sima.accounting.addCancelAccountDocument(file_id,obj[0].id, true);
        },{
            view: 'guiTable',
            select_filter: {
                year: {
                    ids: year_id
                }
            }
        });
    };
    
    this.multidateBankStatementForm = function(refreshTable)
    {
        refreshTable = $('#'+refreshTable);
        var params = {
            status: 'validate',
            onSave: function(response){
                const file_version_id_response = response.form_data.File.attributes.file_version_id;
                if(sima.isEmpty(file_version_id_response))
                {
                    sima.addError('SIMAAccounting::multidateBankStatementForm', 'file_version_id_response is empty: '+JSON.stringify(file_version_id_response));
                    return;
                }
                sima.model.choose('PaymentConfiguration', function(data){
                    var payment_conf_id = data.id;
                    readPayments(payment_conf_id, file_version_id_response, refreshTable);
                });
            },
            formName: 'justupload'
        };
        sima.model.form('File', '', params);
    };
    
//    this.KPR_KIRoverview = function(month, year, name, layout)
//    {
//        if (year!=='' && month!=='')
//        {
//            sima.ajax.get('accounting/report/Report_change',{
//                get_params:{
//                    month:month,
//                    year:year,
//                    name:name
//                },
//                success_function:function(response){
//                    layout.html(response.html);
//                }
//            });
//        }
//    };
    
    this.BillPay = function(bill_id, bill_partner_id, partner_bank_account_id, bill_call_for_number, bill_call_for_number_modul, bill_number, unreleased_amount, default_payment_type, 
                            default_payment_code)
    {
        var call_for_number_partner = bill_number;
        if(bill_call_for_number !== null && bill_call_for_number !== "")
        {
            call_for_number_partner = bill_call_for_number;
        }
        
        sima.model.form('Payment', '', {
            formName:'pay',
            scenario: 'billpay',
            init_data:{
                Payment: {
                    partner_id:{
                        disabled:true,
                        init:bill_partner_id
                    },
                    call_for_number_partner: {
                        disabled:true,
                        init:call_for_number_partner
                    },
                    call_for_number_modul_partner: {
                        disabled:true,
                        init:bill_call_for_number_modul
                    },
                    account_id: {
                        init:partner_bank_account_id,
                        model_filter: {
                            partner: {
                                ids: bill_partner_id
                            }
                        }
                    },
                    amount: unreleased_amount,
                    invoice: false,
                    payment_date: sima.misc.today(),
                    payment_type_id: default_payment_type,
                    payment_code_id: default_payment_code
                }
            },
            onSave:function(response)
            {
                if(response.status === "saved")
                {
                    sima.ajax.get('accounting/billReleasing/add',{
                        data:{
                            bill_ids: [bill_id],
                            payment_ids: [response.model_id]
                        },
                        async:true,
                        success_function:function()
                        {
                            sima.dialog.openInfo("Uspesno kreirano placanje.");
                        }
                    });
                }
            }
        });
    };
    
    this.paymentsExportForPay = function(uniq_id, company_id)
    {
        var selected_rows = $('#paymenttable'+uniq_id).find("tbody").children("tr.selected");
        
        if(selected_rows.length === 0)
        {
            sima.dialog.openInfo("Morate izabrati neko placanje za izvoz.");
        }
        else
        {
            var baparams = {
                filter: {
                    partner_id: company_id
                }
            };
            sima.model.choose('BankAccount', function(badata){
                sima.model.choose('PaymentConfiguration', function(pcdata){
                    var payment_ids = [];
                    for(var i=0; i<selected_rows.length; i++)
                    {
                        payment_ids.push($(selected_rows[i]).attr("model_id"));
                    }

                    sima.ajax.get('accounting/payment/export', {
                        data: {
                            payment_ids: payment_ids,
                            conf_id: pcdata.id,
                            account_id: badata.id
                        },
                        async:true,
                        loadingCircle:$('#paymentdiv'+uniq_id),
                        success_function:function(response)
                        {
                            sima.temp.download(response.tn,response.dn);
                        }
                    });
                });
            }, baparams);
        }
    };    
    
    this.setPartnerMainBankAccount = function(partner_id, bank_account_id)
    {
        sima.ajax.get('accounting/bankAccount/setPartnerMainBankAccount', {
            data: {
                partner_id: partner_id,
                bank_account_id: bank_account_id
            },
            success_function:function(response)
            {
            }
        });
    };

    this.accountRemoveLink = function(transaction_id)
    {
        sima.ajax.get('accounting/books/accountRemoveLink',{
            get_params:{
                transaction_id:transaction_id
            }
        });
    };
    
    this.accountAddLink = function(transaction_id)
    {
        sima.dialog.openChooseOption('Izaberi tip veze:',[
            {
                title:'Partner',
                function:function()
                {
                    sima.dialog.close();
                    sima.model.choose('Partner',function(result){
                        sima.ajax.get('accounting/books/accountAddLink',{
                            get_params:{
                                transaction_id:transaction_id,
                                Model: 'Partner',
                                model_id:result.id
                            }
                        });
                    });
                }
            },
            {
                title:'Mesto troska',
                function:function()
                {
                    sima.dialog.close();
                    sima.model.choose('CostLocation',function(result){
                        sima.ajax.get('accounting/books/accountAddLink',{
                            get_params:{
                                transaction_id:transaction_id,
                                Model: 'CostLocation',
                                model_id:result.id
                            }
                        });
                    });
                }
            }
        ]);
        
        
        
    };
    
    this.accountConvertTo = function(old_account_id,year_id)
    {
        sima.model.choose('Account',function(result1){
            sima.model.choose('Company',function(result2){
                sima.ajax.get('accounting/books/AccountConvertTo',{
                    get_params:{
                        old_account_id:old_account_id,
                        account_id:result1.id,
                        partner_id:result2.id
                    }
                });
            });
        },{
            filter:{
                year: {
                    ids: year_id
                }
            }
        });
        
    };
    
    this.addBillItemsAsFA = function(bill_id,uniq)
    {
        sima.ajax.get('accounting/billItems/addBillItemsAsFA', {
            get_params: {
                bill_id:bill_id
            },
            success_function: function() {
                $('#'+uniq).simaGuiTable('populate');
            }
        });
    };
    
    this.calculateFADepreciations = function(depreciation_id, uniq)
    {
        sima.ajaxLong.start('accounting/fixedAssets/calculateFADepreciations', {
            showProgressBar: $('#'+uniq),
            data:{
                depreciation_id: depreciation_id
            },
            onEnd: function(response) {
                $('#'+uniq).simaGuiTable('populate');
            }
        });
    };
    
    function chooseFixedAssetType(tax_type, bill_items_ids, table_id)
    {
        sima.model.choose('FixedAssetType',function(pick){                    
            sima.ajax.get('accounting/billItems/addToFixedAssets', {
                get_params: {                        
                    fixed_asset_type_id: pick.id,
                    tax_type: tax_type
                },
                data:{
                    bill_items_ids:bill_items_ids
                },
                success_function: function(response) {  
                    if (table_id !== '')
                    {
                        $('#'+table_id).simaGuiTable('populate');
                    }
                    sima.dialog.openInfo('Dodato');
                }
            });
        });
    };
    
    this.addBillItemToFixedAssetsOLD = function(table_id)
    {
        var selected_row = $('#'+table_id).simaGuiTable('getSelected');
        var bill_items_ids = [];
        selected_row.each(function(){
            bill_items_ids.push($(this).attr('model_id'));
        });
        sima.accounting.addBillItemsToFixedAssets(bill_items_ids, table_id);
    };
    
    this.addBillItemsToFixedAssets = function(bill_items_ids, table_id)
    {
        if (bill_items_ids.length > 0)
        {
            sima.dialog.openChooseOption('Poreski tip:',[
                {
                    title: 'Grupa osnovnih sredstava',
                    function: function(){
                        sima.dialog.close();
                        chooseFixedAssetType('FA_GROUP', bill_items_ids, table_id);
                    }
                },
                {
                    title: 'Osnovno sredstvo',
                    function: function(){
                        sima.dialog.close();
                        chooseFixedAssetType('FA', bill_items_ids, table_id);
                    }
                },
                {
                    title: 'Alat/Inventar',
                    function: function(){
                        sima.dialog.close();
                        chooseFixedAssetType('TOOL', bill_items_ids, table_id);
                    }
                },
                {
                    title: 'Rezervni deo',
                    function: function(){
                        sima.dialog.close();
                        chooseFixedAssetType('SPARE_PART', bill_items_ids, table_id);
                    }
                }
            ]);
        }
        else
        {
            sima.dialog.openWarn('Niste selektovali ni jednu stavku.');
        }
    };
    
    this.collapseAccountTransactionOrders = function(account_document_id, table_selector)
    {
        sima.ajaxLong.start('accounting/books/collapseAccountTransactionOrders', {
            showProgressBar: $(table_selector),
            data:{
                account_document_id: account_document_id
            },
            onEnd: function(response) {
                $(table_selector).simaGuiTable('populate');
            }
        });
    };
    
    this.prepareExportForApr = function(uniq_id, name)
    {
        var year_id = $('#'+uniq_id+'_right-panel').data('year_id');
        sima.accounting.exportForApr(uniq_id, name, year_id);
    };
    
    this.exportForApr = function(uniq_id, name, year_id)
    {
        sima.ajaxLong.start('accounting/accountPlans/exportForApr', {
            data: {
                name: name,
                year_id: year_id
            },
            showProgressBar: $('#' + uniq_id + '_accounts'),
            onEnd: function(response)
            {
                sima.temp.download(response.fn, response.dn);
            }
        });
    };
    
    this.openBookOrdersInDialog = function(uniq_id)
    {
        sima.dialog.openActionInDialog('accounting/books/orders', {
            get_params: {
                year_id: $('#' + uniq_id + '_right-panel').data('year_id')
            }
        });
    };
    
    this.applyAccountsByFilters = function(uniq_id)
    {        
        var accounts_table = $('#'+uniq_id+'_accounts');
        var date_range = $('#'+uniq_id+'_accounts_by_date').val();
        var year_id = $('#'+uniq_id+'_right-panel').data('year_id');
        var show_all_accounts = $('#'+uniq_id+'_right-panel').data('show_all_accounts');
        var hide_zero_accounts = $('#'+uniq_id+'_right-panel').data('hide_zero_accounts');
        
        var fixed_filter = accounts_table.simaGuiTable('getFixedFilter');
        fixed_filter['year'] = {ids:year_id};
        var filter_scopes = [];
        if (typeof fixed_filter['filter_scopes'] !== 'undefined')
        {
            filter_scopes = fixed_filter['filter_scopes'];
            if (typeof filter_scopes === 'string')
            {
                filter_scopes = [filter_scopes];
            }
        }
        var display_scopes = [];
        if (typeof fixed_filter['display_scopes'] !== 'undefined')
        {
            display_scopes = fixed_filter['display_scopes'];
            if (typeof display_scopes === 'string')
            {
                display_scopes = [display_scopes];
            }
        }
        
        var withDCCurrents = null;
        if (date_range === '')
        {
            withDCCurrents = [year_id,{
                hide_zero_accounts:hide_zero_accounts
            }];
        }
        else
        {
            var dates = [];
            if (date_range.indexOf("<>") > -1)
            {
                dates = date_range.split('<>');
            }
            else
            {
                dates[0] = date_range;
                dates[1] = date_range;
            }
            withDCCurrents = [year_id, {
                start_date: $.trim(dates[0]),
                end_date: $.trim(dates[1]),
                hide_zero_accounts:hide_zero_accounts
            }];
        }
        display_scopes['withDCCurrents'] = withDCCurrents;
        fixed_filter['display_scopes'] = display_scopes;

        if (!show_all_accounts)
        {
            if (jQuery.inArray('showShortAccountsView', filter_scopes) === -1 && typeof filter_scopes['showShortAccountsView'] === 'undefined')
            {
                filter_scopes.push('showShortAccountsView');
            }
        }
        else
        {            
            if (jQuery.inArray('showShortAccountsView', filter_scopes) > -1)
            {                
                filter_scopes.splice(filter_scopes.indexOf('showShortAccountsView'), 1);
            }
            else if (typeof filter_scopes['showShortAccountsView'] !== 'undefined')
            {                
                delete filter_scopes['showShortAccountsView'];
            }
        }
        fixed_filter['filter_scopes'] = filter_scopes;        
        accounts_table.simaGuiTable('setFixedFilter', fixed_filter, true);
    };
    
    this.showAllAccounts = function(uniq_id)
    {
        $this = $('#'+uniq_id+'_saabi');
        if ($('#'+uniq_id+'_right-panel').data('show_all_accounts'))
        {
            sima.vue.objRemoveClass($this, '_pressed');
            $('#'+uniq_id+'_right-panel').data('show_all_accounts',false);
        }
        else
        {
            sima.vue.objAddClass($this, '_pressed');
            $('#'+uniq_id+'_right-panel').data('show_all_accounts',true);
        }
        sima.accounting.applyAccountsByFilters(uniq_id); 
    };
    
    this.BillItemsLocked = function(bill_id,button_id)
    {
        $this = $('#'+button_id);
        sima.ajax.get('accounting/billItems/areItemsLocked',{
            async: true,
            get_params:{
                bill_id : bill_id
            },
            success_function: function(response)
            {
                var lock_amounts = null;
                if (response.is_locked)
                {
                    lock_amounts = false;
                }
                else
                {
                    lock_amounts = true;
                }
                sima.ajax.get('accounting/billItems/setItemsLocked',{
                    async: true, //jer ne znamo da li je potrebno 
                    get_params:{
                        bill_id : bill_id,
                        lock_amounts: lock_amounts
                    }
                });
            }
        });
    };
    
    this.confirmAccountOrder = function(ao_id)
    {
        sima.ajax.get('accounting/books/isAccountOrderConfirmed',{
            async: true,
            get_params:{
                account_order_id : ao_id
            },
            success_function: function(response)
            {
                var _is_confirmed = null;
                if (response.is_confirmed)
                {
                    _is_confirmed = false;
                }
                else
                {
                    _is_confirmed = true;
                }
                sima.ajax.get('accounting/books/setAccountOrderConfirmed',{
                    async: true, //jer ne znamo da li je potrebno 
                    get_params:{
                        account_order_id : ao_id,
                        set_confirmed: _is_confirmed
                    }
                });
            }
        });
    };
    
    this.hideZeroAccounts = function(uniq_id)
    {
        $this = $('#'+uniq_id+'_hzbi');
        if ($('#'+uniq_id+'_right-panel').data('hide_zero_accounts'))
        {
            sima.vue.objRemoveClass($this, '_pressed');
            $('#'+uniq_id+'_right-panel').data('hide_zero_accounts',false);
        }
        else
        {
            sima.vue.objAddClass($this, '_pressed');
            $('#'+uniq_id+'_right-panel').data('hide_zero_accounts',true);
        }
        sima.accounting.applyAccountsByFilters(uniq_id); 
    };
    
    this.chooseADforVAT = function(month_id, year_id)
    {
        sima.model.choose('AccountOrder',function(pick){
            sima.ajax.get('accounting/vat/setVatAccountDocument',{
                get_params: {
                    month_id: month_id,
                    account_document_id: pick[0].id
                }
            });
        },{
            view: 'guiTable',
            fixed_filter: {
                year: {
                    ids: year_id
                }
            }
        });
    };
    
    this.getCostTypesByYear = function(_this, year_id)
    {
        sima.ajax.get('accounting/accounting/getCostTypesByYear',{
            get_params:{
                year_id : year_id
            },
            success_function: function(response){
                sima.set_html(_this,response.html);
            }
        });
    };
    
    this.transferSelectedCostTypesToYear = function(table_from, table_to, year_id)
    {
        var selected_rows = $('#'+table_from).simaGuiTable('getSelected');
        var cost_types_ids = [];
        selected_rows.each(function(){
            cost_types_ids.push($(this).attr('model_id'));
        });
        if (cost_types_ids.length === 0)
        {
            sima.dialog.openWarn(sima.translate('YouHaveNotSelectedAnyCostType'));
        }
        else
        {
            sima.ajaxLong.start('accounting/costTypes/getSelectedCostTypesForTransfer', {
                showProgressBar: $('#'+table_from),
                data:{
                    cost_types_ids : cost_types_ids,
                    year_id : year_id,
                    table_to_id: table_to
                },
                onEnd: function(response) {   
                    if (response.html==='')
                    {
                        return;
                    }
                    var html = {};
                    var yes_func_params = {};
                    var no_func_params = {};
                    html.title = 'Prebacivanje vrste troška iz prethodne godine';
                    html.html = response.html;
                    yes_func_params.func = function onConfirm(obj) {
                        var wrap = $('#'+response.uniq_id);
                        var cost_types = [];
                        wrap.find('.body .row').each(function(){
                            if ($(this).find('.checkbox input').is(':checked'))
                            {                            
                                cost_types.push({
                                    prev_account_code:$(this).find('.account').data('code'),
                                    cost_type_id:$(this).find('.cost_type').data('id')
                                });
                            }
                        });
                        sima.dialog.close();
                        if (cost_types.length > 0)
                        {
                            sima.ajaxLong.start('accounting/costTypes/addCostTypesToYear', {
                                showProgressBar: $('#'+table_to),
                                data:{                            
                                    year_id : year_id,
                                    cost_types : cost_types                       
                                },
                                onEnd: function(response) {
                                    $('#'+table_from).simaGuiTable('populate');
                                    $('#'+table_to).simaGuiTable('populate');
                                }
                            });
                        }
                    };
                    yes_func_params.title = "Sačuvaj";
                    no_func_params.hidden = true;
                    sima.dialog.openYesNo(html, yes_func_params, no_func_params, null, null, true);                
                }
            });
        }
    };
    
    
    this.transferCostTypesFromPreviousYear = function(table_from, table_to, prev_year_id, curr_year_id)
    {
        sima.ajaxLong.start('accounting/costTypes/getCostTypesForTransferFromPreviousYear', {
            showProgressBar: $('#'+table_to),
            data:{
                prev_year_id : prev_year_id,
                curr_year_id : curr_year_id
            },
            onEnd: function(response) {                                        
                var html = {};
                var yes_func_params = {};
                var no_func_params = {};
                html.title = 'Prebacivanje vrste troška iz prethodne godine';
                html.html = response.html;
                yes_func_params.func = function onConfirm(obj) {
                    var wrap = $('#'+response.uniq_id);
                    var cost_types = [];
                    wrap.find('.body .row').each(function(){
                        if ($(this).find('.checkbox input').is(':checked'))
                        {                            
                            cost_types.push({
                                prev_account_code:$(this).find('.account').data('code'),
                                cost_type_id:$(this).find('.cost_type').data('id')
                            });
                        }
                    });
                    sima.dialog.close();
                    if (cost_types.length > 0)
                    {
                        sima.ajaxLong.start('accounting/costTypes/addCostTypesToYear', {
                            showProgressBar: $('#'+table_to),
                            data:{                            
                                year_id : curr_year_id,
                                cost_types : cost_types                       
                            },
                            onEnd: function(response) {
                                $('#'+table_from).simaGuiTable('populate');
                                $('#'+table_to).simaGuiTable('populate');
                            }
                        });
                    }
                };
                yes_func_params.title = "Sačuvaj";
                no_func_params.hidden = true;
                sima.dialog.openYesNo(html, yes_func_params, no_func_params, null, null, true);                
            }
        });
    };
    
    this.grossBalance = function(uniq_id)
    {
        sima.ajax.get('accounting/accounting/getGrossBalanceExportParams',{
            get_params:{
            },
            success_function: function(response) {
                sima.dialog.openYesNo(
                    {
                        html:response.html, title:'Bruto bilans'
                    }, 
                    {
                        title:'Generiši',
                        func:function(event) {
                            var params_chooser_uniq = response.params_chooser_uniq;
                            var date = $('#gross_balance_date_'+params_chooser_uniq).val();
                            var export_type = $('#gross_balance_export_type_'+params_chooser_uniq).val();
                            if (date === '')
                            {
                                sima.dialog.openWarn('Morate izabrati datum!');
                            }
                            else
                            {
                                sima.dialog.close();
                                var year_id = $('#'+uniq_id+'_right-panel').data('year_id');
                                sima.ajaxLong.start('accounting/accounting/generatePdfForGrossBalance', {
                                    showProgressBar: $('body'),
                                    data:{
                                        year_id: year_id,
                                        date: date,
                                        export_type: export_type
                                    },
                                    onEnd: function(response) {                                        
                                        sima.temp.download(response.filename, response.downloadname);
                                    }
                                });
                            }
                        }
                    }, 
                    {
                        hidden:true
                    }
                );
            }
        });
    };
    
    this.accountingPartnersReport = function(uniq_id)
    {
        sima.ajax.get('accounting/accounting/getAccountingPartnersReportExportParams',{
            get_params:{},
            success_function: function(response) {
                sima.dialog.openYesNo(
                    {
                        html: response.html, 
                        title: sima.translate('AccountingPartnersReport')
                    }, 
                    {
                        title: sima.translate('AccountingPartnersReportGenerate'),
                        func:function(event) {
                            var params_chooser_uniq = response.params_chooser_uniq;
                            var date = $('#ios_by_all_clients_date_' + params_chooser_uniq).val();
                            var export_type = $('#ios_by_all_clients_export_type_' + params_chooser_uniq).val();
                            var is_buyers = false;
                            var is_suppliers = false;
                            
                            if ($('#buyers_' + params_chooser_uniq).is(':checked'))
                            {                            
                                is_buyers = true;
                            }
                            if ($('#suppliers_' + params_chooser_uniq).is(':checked'))
                            {                            
                                is_suppliers = true;
                            }
                            if (date === '')
                            {
                                sima.dialog.openWarn(sima.translate('AccountingPartnersReportMustChooseDate'));
                            }
                            else
                            {
                                sima.dialog.close();
                                sima.ajaxLong.start('accounting/accounting/generateAccountingPartnersReport', {
                                    showProgressBar: $('#'+uniq_id),
                                    data:{
                                        date: date,
                                        export_type: export_type,
                                        is_buyers: is_buyers,
                                        is_suppliers: is_suppliers
                                    },
                                    onEnd: function(response) {                                        
                                        sima.temp.download(response.filename, response.downloadname);
                                    }
                                });
                            }
                        }
                    }, 
                    {
                        hidden:true
                    }
                );
            }
        });
    };
    
    this.exportAccountsToPdf = function(table_id)
    {
        var _table =$('#'+table_id);
        var guitable_filter = _table.simaGuiTable('getAllFilters');
        sima.ajaxLong.start('accounting/accounting/exportAccountsToPdf', {
            showProgressBar: _table,
            data:{
                guitable_filter: guitable_filter
            },
            onEnd: function(response) {                                        
                sima.temp.download(response.filename, response.downloadname);                                        
            }
        });
    };
    
    this.addFlyingPaymentsToBankStatement = function(bank_statement_id, guitable_id)
    {
        var params = {};
        params.view = "guiTable";
        params.multiselect = true;
        params.params = {add_button:false};
        params.fixed_filter = {
            filter_scopes: ['for_pay']
        };
        sima.model.choose('Payment', function(data){
            if (data.length > 0)
            {
                sima.ajax.get('accounting/bankStatement/addFlyingPaymentsToBankStatement', {
                    get_params:{
                        bank_statement_id : bank_statement_id
                    },
                    data:{payments:data},
                    async:true,
                    success_function:function(){
                        $('#'+guitable_id).simaGuiTable('populate');
                    }
                });
            }
        }, params);
    };

    
    this.changeTransactionAccount = function(uniq)
    {
        var year_id = $('#'+uniq+'_right-panel').data('year_id');
        sima.ajax.get('accounting/accounting/chooseAccountsForTransactionsChanges',{
            get_params: {
                year_id:year_id,
                uniq:uniq
            },
            success_function: function(response){
                var html = {};
                var yes_func_params = {};
                var no_func_params = {};
                html.title = 'Prekontiranje';
                html.html = response.html;
                yes_func_params.func = function() {
                    var wrap = $('#choose_accounts_for_transactions_changes'+uniq);
                    account1 = wrap.find('.account1 .sima-ui-sf').simaSearchField('getValue');
                    account2 = wrap.find('.account2 .sima-ui-sf').simaSearchField('getValue');
                    if (account1.id === '' || account2.id === '')
                    {
                        sima.dialog.openInfo('Morate popuniti oba konta!');
                    }
                    else
                    {
                        sima.dialog.openYesNo('Da li ste sigurni da želite da izvršite prekontiranje sa konta <b>"'+account1.display_name+'"</b> na konto <b>"'+account2.display_name+'"</b>?',
                            function(){
                                sima.dialog.close();
                                sima.ajax.get('accounting/accounting/changeAccountTransactions',{
                                    get_params: {
                                        account_from_id:account1.id,
                                        account_to_id:account2.id
                                    },
                                    success_function: function(response){
                                        sima.misc.displaySimplePopupBox('Završeno prekontiranje!');
                                    }
                                });
                                sima.dialog.close();
                            }
                        );
                    }
                };
                yes_func_params.title = "Prekontiraj";
                no_func_params.hidden = true;
                sima.dialog.openYesNo(html, yes_func_params, no_func_params);
            }
        });
    };
    
    this.collapseOrderNumbersInKprKirManual = function(model_name, year_id, table_selector)
    {
        sima.dialog.openYesNo(sima.translate('AreYouSureYouWantToCollapseOrderNumbers'),
            function(){
                sima.dialog.close();                          
                sima.ajaxLong.start('accounting/vat/collapseOrderNumbersInKprKirManual', {
                    showProgressBar: $(table_selector),
                    data:{
                        model_name: model_name,
                        year_id: year_id
                    },
                    onEnd: function(response) {
                        sima.dialog.openInfo(response.numbers_by_months_html);
                        if (!sima.isEmpty(table_selector))
                        {
                            $(table_selector).simaGuiTable('populate');
                        }
                    }
                });
            }
        );
    };
    
    this.releaseOrderNumberInKprKirManual = function(model_name, year_id, table_selector)
    {
        var params = {};
        params.type = 'input';
        params.text = sima.translate('EnterOrderNumberToFreeForKprKirManual');
        sima.dialog.openPrompt(sima.translate('EnterOrderNumberToFreeForKprKirManual'),function(value){
            sima.ajaxLong.start('accounting/vat/releaseOrderNumberInKprKirManual', {
                showProgressBar: $(table_selector),
                data:{
                    model_name: model_name,
                    year_id: year_id,
                    order_in_year: value
                },
                onEnd: function(response) {
                    if (!sima.isEmpty(table_selector))
                    {
                        $(table_selector).simaGuiTable('populate');
                    }
                }
            });
        },params);
    };
    
    //custom funkcija za formu BillItemGUI
    this.billItemFormPOPDVChooser = function(popdv_lists, form, dependent_on_field, dependent_field, apply_actions_func, additional_params)
    {
        var _vat = dependent_on_field.val();
        //prilikom inicijalizacije postoji sve u listi. 
        //Posto se sve brise i dodaje iz pocetka, potrebno je sacuvati sta je selektovano
        var _current_val = dependent_field.val();
        dependent_field.html('');
        for (var i in popdv_lists[_vat])
        {
            var _item = popdv_lists[_vat][i];
            dependent_field.append( 
                $("<option>")
                    .val(_item.code)
                    .html(_item.title)
            );
            
        }
        dependent_field.val(_current_val);
        //ovo se desava kada se promeni procenat PDV jer ne postoji sta je bilo u spisku
        if (dependent_field.val() === null) 
        {
            dependent_field.val(popdv_lists[_vat][0].code);
        }
        dependent_field.trigger('change');
    };
    
    this.POPDVBillsDisplay = function(code, month_id)
    {
//        sima.ajax.get('accounting/vat/POPDVBillsDisplay',{
//            get_params: {
//                month_id: month_id,
//                code: code
//            },
//            success_function: function(response){
//                sima.dialog.openInFullScreen(response.html);
//            }
//        });
        var component_name = 'accounting-report_specification';
        var title = sima.translate('AccountingReportSpecification');
        var params = {
            action: 'accounting/vat/POPDVFieldReport',
            action_data: {
                code: code,
                month_id: month_id
            },
            show_values: ['value','value_vat', 'value_internal_vat','advance_vat_releases']
        };
        sima.dialog.openVueComponent(component_name, params, title, {fullscreen:true});

    };
    this.POPDVEditManual = function(item_id, account_document_id, code)
    {
        if (item_id === '')
        {
            sima.model.form('POPDVItem','',{
                init_data: {
                    POPDVItem: {
                        account_document_id: account_document_id,
                        param_code: code
                    }
                    
                }
            });
        }
        else
        {
            sima.model.form('POPDVItem',item_id);
        }
        
    };
    
    this.addWarehouseTransferToBIll = function(bill_id,warehouse_transfer_type,partner_id)
    {
        var init_data = {};
        init_data[warehouse_transfer_type] = {
            partner_id: {
                init: partner_id, 
                disabled: 'disabled'
            },
            bill_id: {
                init: bill_id, 
                disabled: 'disabled'
            }
        };
        sima.model.form(warehouse_transfer_type,bill_id,{
            init_data: init_data
        });
    };
    this.removeWarehouseTransferFromBIll = function(bill_id, warehouse_transfer_type)
    {
        sima.model.remove(warehouse_transfer_type,bill_id);
    };
    
    this.kprKirReport = function(month, year, name, export_type)
    {
        sima.ajaxLong.start('accounting/vat/kprKirReport', {
            showProgressBar: $('body'),
            data:{
                month: month,
                year: year,
                name: name,
                export_type: export_type
            },
            onEnd: function(response) {
                sima.temp.download(response.filename, response.downloadname);
            }
        });
    };
    
    this.multiselectAODownloadAsZip = function(table_id, folder_name)
    {        
        var guitable = $('#'+table_id);
                
        var selected_file_ids = guitable.simaGuiTable('getSelectedIds');

        if(typeof selected_file_ids === "string")
        {
            selected_file_ids = JSON.parse(selected_file_ids);
        }
        
        sima.ajax.get('accounting/accounting/filesDownloadZipPrepare',{
            data:{
                file_ids: selected_file_ids
            },
            success_function:function(response){
                sima.temp.download(response.tn, folder_name);
            },
            failure_function:function(response){
                sima.dialog.openWarn(response.message);
            }
        });
    };
    
    /**
     * 
     * @param {Array} params
     */
    this.openReportSpecification = function(params)
    {
        var component_name = 'accounting-report_specification';
        var title = sima.translate('AccountingReportSpecification');
        sima.dialog.openVueComponent(component_name, params, title, {fullscreen:true});
    };
    
    this.toggleVATReturn = function(month_id)
    {
        sima.dialog.openYesNo("Da li ste sigurni da želite da promenite status za povraćaj PDV?",function(){
            sima.ajax.get('accounting/vat/toggleVATReturn',{
                async: true,
                get_params: {
                    month_id: month_id
                },
                success_function: function(){
                    sima.dialog.close();
                }
            });
        });
    };

    this.selectYear = function(form, dependent_on_field, dependent_field, apply_actions_func, event){
        var year = dependent_on_field.val();
        if(!isNaN(year) && (year > 1900 && year < 2100))
        {
            dependent_field.val('31.12.'+year+'.').trigger('input');
        }
    };
}
