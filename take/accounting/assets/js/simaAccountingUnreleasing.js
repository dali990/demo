/* global sima */

function SIMAAccountingUnreleasing()
{   
    /**
     * Rasknjizavanje racuna placanjima
     * @param {int} bill_id
     */
    this.billPayment = function(bill_id, table_id)
    {
        var table = $('#'+table_id);
        var payments_ids = [];
        table.simaGuiTable('getSelected').each(function(){
            payments_ids.push($(this).attr('model_id'));
        });
        
        if (payments_ids.length > 0)
        {
            sima.dialog.openYesNo('Da li ste sigurni da želite da dodate ova plaćanja?',
                function(){
                    sima.dialog.close();                          
                    sima.ajax.get('accounting/billReleasing/add',{
                        data:{
                            bill_ids: [bill_id],
                            payment_ids: payments_ids
                        },
                        success_function: function() {
                            sima.misc.refreshUiTabForObject(table);
                        }
                    });
                }
            );
        }
    };
    
    /**
     * Rasknjizavanje racuna placanjima
     * @param {int} payment_id
     */
    this.paymentBill = function(payment_id, table_id)
    {
        var table = $('#'+table_id);
        var bill_ids = [];
        table.simaGuiTable('getSelected').each(function(){
            bill_ids.push($(this).attr('model_id'));
        });
        
        if (bill_ids.length > 0)
        {
            sima.dialog.openYesNo('Da li ste sigurni da želite da dodate ove račune?',
                function(){
                    sima.dialog.close();                                    
                    sima.ajax.get('accounting/billReleasing/add',{
                        data:{
                            bill_ids: bill_ids,
                            payment_ids: [payment_id]
                        },
                        success_function: function() {
                            sima.misc.refreshUiTabForObject(table);
                        }
                    });
                }
            );
        }
    };
    
    /**
     * Rasknjizavanje racuna placanjima
     * @param {int} bill_id
     */
    this.billAdvance = function(bill_id, table_id)
    {
        var table = $('#'+table_id);
        var advance_bill_ids = [];
        table.simaGuiTable('getSelected').each(function(){
            advance_bill_ids.push($(this).attr('model_id'));
        });
        
        if (advance_bill_ids.length > 0)
        {
            sima.dialog.openYesNo('Da li ste sigurni da želite da dodate ove avansne račune?',
                function(){
                    sima.dialog.close();                                   
                    sima.ajax.get('accounting/billReleasing/addAdvance',{
                        data:{
                            bill_ids: [bill_id],
                            advance_bill_ids: advance_bill_ids
                        },
                        success_function: function() {
                            sima.misc.refreshUiTabForObject(table);
                        }
                    });
                }
            );
        }
    };
    
    /**
     * Rasknjizavanje racuna placanjima za odredjeni iznos
     * @param {int} bill_id
     */
    this.billAdvanceByAmount = function(bill_id, table_id)
    {
        var table = $('#'+table_id);
        var advance_bill_ids = [];
        table.simaGuiTable('getSelected').each(function(){
            advance_bill_ids.push($(this).attr('model_id'));
        });
        if (advance_bill_ids.length !== 1)
        {
            sima.dialog.openWarn('Morate selektovati samo jedan avans!');
        }
        else
        {
            var advance_bill_id = advance_bill_ids[0];
            var model_id = {
                uniq_check: {
                    bill_id: bill_id,
                    advance_bill_id: advance_bill_id
                }
            };                       
            sima.model.form('AdvanceBillRelease', model_id, {
                init_data: {
                    AdvanceBillRelease: {
                        bill_id:bill_id,
                        advance_bill_id:advance_bill_id
                    }
                },
                onSave:function(){sima.misc.refreshUiTabForObject(table);}
            });
        }
    };
    
    /**
     * Rasknjizavanje avansnog racuna racunom
     * @param {int} advance_bill_id
     * @param {int} table_id
     */
    this.advanceBillBill = function(advance_bill_id, table_id)
    {        
        var table = $('#'+table_id);
        var bill_ids = [];
        table.simaGuiTable('getSelected').each(function(){
            bill_ids.push($(this).attr('model_id'));
        });
        
        if (bill_ids.length > 0)
        {
            sima.dialog.openYesNo('Da li ste sigurni da želite da dodate ove račune?',
                function(){
                    sima.dialog.close();                                           
                    sima.ajax.get('accounting/billReleasing/addAdvance',{
                        data:{
                            bill_ids: bill_ids,
                            advance_bill_ids: [advance_bill_id]
                        },
                        success_function: function() {
                            sima.misc.refreshUiTabForObject(table);
                        }
                    });
                }
            );
        }
    };
    
    /**
     * Povezivanje avansnog racuna Knjiznim odobrenjem
     * @param {int} advance_bill_id
     * @param {int} table_id
     */
    this.advanceBillReliefBill = function(advance_bill_id, table_id)
    {        
        var table = $('#'+table_id);
        var relief_bill_ids = [];
        table.simaGuiTable('getSelected').each(function(){
            relief_bill_ids.push($(this).attr('model_id'));
        });
        
        if (relief_bill_ids.length > 0)
        {
            sima.dialog.openYesNo('Da li ste sigurni da želite da dodate ove račune?',
                function(){
                    sima.dialog.close();                                           
                    sima.ajax.get('accounting/billReleasing/addAdvanceBillRelief',{
                        data:{
                            relief_bill_ids: relief_bill_ids,
                            advance_bill_ids: [advance_bill_id]
                        },
                        success_function: function() {
                            sima.misc.refreshUiTabForObject(table);
                        }
                    });
                }
            );
        }
    };
    
    /**
     * Povezivanje Knjiznog odobrenja Avansnim racunom
     * @param {int} advance_bill_id
     * @param {int} table_id
     */
    this.reliefBillAdvanceBill = function(relief_bill_id, table_id)
    {        
        var table = $('#'+table_id);
        var advance_bill_ids = [];
        table.simaGuiTable('getSelected').each(function(){
            advance_bill_ids.push($(this).attr('model_id'));
        });
        
        if (advance_bill_ids.length > 0)
        {
            sima.dialog.openYesNo('Da li ste sigurni da želite da dodate ove račune?',
                function(){
                    sima.dialog.close();                                           
                    sima.ajax.get('accounting/billReleasing/addAdvanceBillRelief',{
                        data:{
                            relief_bill_ids: [relief_bill_id],
                            advance_bill_ids: advance_bill_ids
                        },
                        success_function: function() {
                            sima.misc.refreshUiTabForObject(table);
                        }
                    });
                }
            );
        }
    };
    
    /**
     * Rasknjizavanje racuna placanjima
     * @param {int} bill_id
     */
    this.billRelief = function(bill_id, table_id)
    {
        var table = $('#'+table_id);
        var relief_bill_ids = [];
        table.simaGuiTable('getSelected').each(function(){
            relief_bill_ids.push($(this).attr('model_id'));
        });
        
        if (relief_bill_ids.length > 0)
        {
            sima.dialog.openYesNo('Da li ste sigurni da želite da dodate ova knjižna odobrenja?',
                function(){
                    sima.dialog.close();                                    
                    sima.ajax.get('accounting/billReleasing/addRelief',{
                        data:{
                            bill_ids: [bill_id],
                            relief_bill_ids: relief_bill_ids
                        },
                        success_function: function() {
                            sima.misc.refreshUiTabForObject(table);
                        }
                    });
                }
            );
        }
    };
    
    /**
     * Rasknjizavanje racuna placanjima
     * @param {int} relief_bill_id
     */
    this.reliefBillBill = function(relief_bill_id, table_id)
    {
        var table = $('#'+table_id);
        var bill_ids = [];
        table.simaGuiTable('getSelected').each(function(){
            bill_ids.push($(this).attr('model_id'));
        });
        
        if (bill_ids.length > 0)
        {
            sima.dialog.openYesNo('Da li ste sigurni da želite da dodate ove račune?',
                function(){
                    sima.dialog.close();                                    
                    sima.ajax.get('accounting/billReleasing/addRelief',{
                        data:{
                            relief_bill_ids: [relief_bill_id],
                            bill_ids: bill_ids
                        },
                        success_function: function() {
                            sima.misc.refreshUiTabForObject(table);
                        }
                    });
                }
            );
        }
    };
    
    /**
     * Rasknjizavanje racuna placanjima
     * @param {int} advance_bill_id
     */
    this.advanceBillAdvance = function(advance_bill_id, table_id)
    {
        var table = $('#'+table_id);
        var advance_ids = [];
        table.simaGuiTable('getSelected').each(function(){
            advance_ids.push($(this).attr('model_id'));
        });
        
        if (advance_ids.length > 0)
        {
            sima.dialog.openYesNo('Da li ste sigurni da želite da dodate ove avanse?',
                function(){
                    sima.dialog.close();                                           
                    sima.ajax.get('accounting/billReleasing/connectAdvance',{
                        data:{
                            advance_bill_ids: [advance_bill_id],
                            advance_ids: advance_ids
                        },
                        success_function: function() {
                            sima.misc.refreshUiTabForObject(table);
                        }
                    });
                }
            );
        }
    };
    
    /**
     * Rasknjizavanje racuna placanjima
     * @param {int} advance_id
     */
    this.advanceAdvanceBill = function(advance_id, table_id)
    {
        var table = $('#'+table_id);
        var advance_bill_ids = [];
        table.simaGuiTable('getSelected').each(function(){
            advance_bill_ids.push($(this).attr('model_id'));
        });
        
        if (advance_bill_ids.length > 0)
        {
            sima.dialog.openYesNo('Da li ste sigurni da želite da dodate ove avanse?',
                function(){
                    sima.dialog.close();                
                    sima.ajax.get('accounting/billReleasing/connectAdvance',{
                        data:{
                            advance_bill_ids: advance_bill_ids,
                            advance_ids: [advance_id]
                        },
                        success_function: function() {
                            sima.misc.refreshUiTabForObject(table);
                        }
                    });
                }
            );
        }
    };
    
    this.chooseFictiveAdvance = function(advance_bill_id)
    {
        sima.ajax.get('accounting/billReleasing/chooseFictiveAdvance',{
            get_params: {
                advance_bill_id:advance_bill_id
            },
            success_function: function(response){
                console.log('uspesno');
            }
        });
    };
    
};