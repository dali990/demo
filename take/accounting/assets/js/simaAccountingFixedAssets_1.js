/* global sima */

function SIMAAccountingFixedAssets()
{
    this.exportInventoryList = function(show_loading_obj_id, on_date)
    {
        if (typeof on_date !== 'undefined' && on_date === true)
        {
            sima.ajax.get('accounting/fixedAssets/getDateFilterForExportInventoryList', {
                success_function: function(response) {
                    sima.dialog.openYesNo({title:response.title, html:response.html}, {title:sima.translate('Export'), func:function(event) {
                        var date = $(event.currentTarget).parents('.ui-dialog:first').find('input[name="date_for_fixed_assets_inventory_list"]').val();
                        if (date === '')
                        {
                            sima.dialog.openWarn(sima.translate('YouMustSelectDate'));
                        }
                        else
                        {
                            sima.dialog.close();
                            exportInventoryList(show_loading_obj_id, date);
                        }
                    }}, {hidden: true});
                }
            });
        }
        else
        {
            exportInventoryList(show_loading_obj_id);
        }
    };
    
    function exportInventoryList(show_loading_obj_id, date)
    {
        if (sima.isEmpty(show_loading_obj_id))
        {
            show_loading_obj = $('body');
        }
        else
        {
            show_loading_obj = $('#' + show_loading_obj_id);
        }

        sima.ajaxLong.start('accounting/fixedAssets/exportInventoryList', {
            showProgressBar: show_loading_obj,
            data:{
                date: date
            },
            onEnd: function(response) {                                        
                sima.temp.download(response.filename, response.downloadname);
            }
        });
    }
}


