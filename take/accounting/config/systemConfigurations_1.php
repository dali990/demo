<?php

return array(
    'display_name' => Yii::t('AccountingModule.Configuration', 'Accounting'),
    include('systemConfigurationsCodes.php'),
    [
        'name' => 'add_bill_in_without_filing_book',
        'display_name' => Yii::t('AccountingModule.Config','AddBillInWithoutFilingNook'),
        'type'=>'dropdown',
        'type_params'=>array(
            array(
                'title'=>'Da',
                'value'=>1
            ),
            array(
                'title'=>'Ne',
                'value'=>0
            )
        ),
        'value_not_null' => true,
        'default'=>0,
        'description' => Yii::t('AccountingModule.Config','AddBillInWithoutFilingNookDesc')
    ],
    [
        'name' => 'book_services_separately',
        'display_name'=>Yii::t('AccountingModule.Config','book_services_separately'),
        'type'=>'dropdown',
        'type_params'=>array(
            array(
                'title'=>'Da',
                'value'=>1
            ),
            array(
                'title'=>'Ne',
                'value'=>0
            )
        ),
        'value_not_null' => true,
        'default'=>1,
        'description'=>Yii::t('AccountingModule.Config','book_services_separately_desc')
    ],
    [
        'name' => 'popdv_show_advances_in_same_month',
        'display_name' => Yii::t('AccountingModule.Config','POPDVShowAdvancesInSameMonth'),
        'type' => 'boolean',
        'value_not_null' => true,
        'default' => false,
        'description' => Yii::t('AccountingModule.Config','POPDVShowAdvancesInSameMonthDesc')
    ],
    [
        'name' => 'bill_footer_text',
        'display_name'=>Yii::t('AccountingModule.Config','BillFooterText'),
        'type'=>'textField',
        'value_not_null' => false,
        'default' => '-',
        'description'=>Yii::t('AccountingModule.Config','BillFooterText')
    ],
    [
        'name' => 'show_income_in_kir',
        'display_name'=>'Prikazi prihod u KIR',
        'type'=>'dropdown',
        'type_params'=>array(
            array(
                'title'=>'Da',
                'value'=>1
            ),
            array(
                'title'=>'Ne',
                'value'=>0
            )
        ),
        'value_not_null' => true,
        'default'=>0,
        'description'=>'U izvestaju KIR u koloni 12 (osnovica 20%) da li da prikaze prihod ili osnovicu'
    ],
    [
        'name' => 'income_rounding_cost_type',
        'display_name' => 'Vrsta troska zaokruzivanja u prihodu',
        'type' => 'searchField',
        'type_params' => array(
            'modelName'=>'CostType',
        ),
        'value_not_null' => false,
        'description' => 'Trosak na koji ce se knjižiti zaokruzivanje u prihodu. Ukoliko se ne izabere, knjižiće se zajedno sa prihodom'
    ],
    array(
        'name' => 'default_payment_type',
        'display_name'=>'Osnovni tip placanja',
        'type'=>'searchField',
        'type_params' => array(
            'modelName'=>'PaymentType',
        ),
        'value_not_null'=>true,
        'description'=>'Tip placanja koji se postavlja kad nije izabran ni jedan drugi '
    ),
    [
        'name' => 'default_payment_type_for_empty_payment_code',
        'display_name'=>Yii::t('AccountingModule.Configuration', 'DefaultPaymentTypeForEmptyPaymentCode'),
        'type'=>'searchField',
        'type_params' => array(
            'modelName'=>'PaymentType',
        ),
        'value_not_null'=>false,
        'description'=>Yii::t('AccountingModule.Configuration', 'DefaultPaymentTypeForEmptyPaymentCodeDescription')
    ],
    array(
        'name' => 'advance_payment_type',
        'display_name'=>'Tip placanja Avanas',
        'type'=>'searchField',
        'type_params' => array(
            'modelName'=>'PaymentType',
        ),
        'value_not_null'=>true,
        'description'=>'Tip placanja koji se postavlja kad je u pitanju avansno placanje '
    ),
    array(
        'name' => 'paycheck_payment_type',
        'display_name'=>'Tip placanja Plata',
        'type'=>'searchField',
        'type_params' => array(
            'modelName'=>'PaymentType',
        ),
        'value_not_null'=>true,
        'description'=>'Tip placanja koji se postavlja kad je u pitanju plata '
    ),
    [
        'name' => 'bank_commission_payment_type',
        'display_name' => 'Tip placanja Provizija banke',
        'type' => 'searchField',
        'value_is_array' => true,
        'type_params' => [
            'modelName' => 'PaymentType',
        ],
        'value_not_null' => true,
        'description' => 'Tip placanja kojim se obelezava provizija banke '
    ],
    [
        'name' => 'cash_desk_payment_type',
        'display_name'=>'Tip placanja - prenos blagajna',
        'type'=>'searchField',
        'type_params' => array(
            'modelName'=>'PaymentType',
        ),
        'value_not_null'=>false,
        'description'=>'Tip placanja koji se postavlja kada se pare prenose za ZR u blagajnu i obrnuto '
    ],
    array(
        'name' => 'default_payment_code',
        'display_name'=>'Osnovni kod placanja',
        'type'=>'searchField',
        'type_params' => array(
            'modelName'=>'PaymentCode',
        ),
        'value_not_null'=>true,
        'description'=>'Kod placanja koji se postavlja kad nije izabran ni jedan drugi (221) '
    ),
    array(
        'name' => 'currency_conversion_payment_type',
        'display_name' => Yii::t('AccountingModule.Configuration', 'CurrencyConversionPaymentType'),
        'type'=>'searchField',
        'type_params' => array(
            'modelName'=>'PaymentType',
        ),
        'description' => Yii::t('AccountingModule.Configuration', 'CurrencyConversionPaymentTypeDescription')
    ),

    
    [
        'name' => 'templates',
        'display_name' => Yii::t('AccountingModule.Configuration', 'Templates'),
        'type' => 'group',
        'type_params' => [
            array(
                'name' => 'cash_desk_log',
                'display_name'=>'Template id za dnevnik blagajne',
                'type'=>'searchField',
                'type_params' => array(
                    'modelName'=>'Template',
                ),
                'value_not_null'=>false,
                'description'=>'ID template-a za dnevnik blagajne'
            ),
            array(
                'name' => 'cash_desk_order_in',
                'display_name'=>'Template id za naplatu naloga blagajne',
                'type'=>'searchField',
                'type_params' => array(
                    'modelName'=>'Template',
                ),
                'value_not_null'=>false,
                'description'=>'ID template-a za naplatu naloga blagajne'
            ),
            array(
                'name' => 'cash_desk_order_out',
                'display_name'=>'Template id za isplatu naloga blagajne',
                'type'=>'searchField',
                'type_params' => array(
                    'modelName'=>'Template',
                ),
                'value_not_null'=>false,
                'description'=>'ID template-a za isplatu naloga blagajne'
            ),
            [
                'name' => 'bill_templates',
                'display_name' => Yii::t('AccountingModule.Configuration', 'BillTemplate'),
                'type' => 'group',
                'type_params' => [
                    [
                        'name' => 'has_bill_responsible_person',
                        'display_name' => Yii::t('AccountingModule.Configuration', 'HasBillResponsiblePerson'),
                        'type' => 'boolean',
                        'description' => Yii::t('AccountingModule.Configuration', 'HasBillResponsiblePersonDesc')
                    ],
                    [
                        'name' => 'bill_responsible_person',
                        'display_name' => Yii::t('AccountingModule.Configuration', 'BillResponsiblePerson'),
                        'type' => 'searchField',
                        'type_params' => [
                            'modelName' => 'Person'
                        ],
                        'description' => Yii::t('AccountingModule.Configuration', 'BillResponsiblePerson')
                    ],
                    [
                        'name' => 'bill_additional_info',
                        'display_name' => Yii::t('AccountingModule.Configuration', 'BillTemplateAdditionalInfo'),
                        'type' => 'textField',
                        'description' => Yii::t('AccountingModule.Configuration', 'BillTemplateAdditionalInfo')
                    ],
                    [
                        'name' => 'bill_bank_accounts',
                        'display_name' => Yii::t('AccountingModule.Configuration', 'BillBankAccounts'),
                        'type' => 'searchField',
                        'type_params' => [
                            'modelName' => 'BankAccount'
                        ],
                        'value_is_array'=>true,
                        'description' => Yii::t('AccountingModule.Configuration', 'BillBankAccounts')
                    ],
                    [
                        'name' => 'bill_show_phone_in_header',
                        'display_name' => Yii::t('AccountingModule.Configuration', 'BillShowPhoneInHeader'),
                        'type' => 'boolean',
                        'value_not_null' => true,
                        'default' => true,
                        'description' => Yii::t('AccountingModule.Configuration', 'BillShowPhoneInHeader')
                    ],
                    [
                        'name' => 'traffic_location_id',
                        'display_name' => Yii::t('AccountingModule.Configuration', 'BillTrafficLocation'),
                        'type' => 'searchField',
                        'type_params' => [
                            'modelName' => City::class
                        ],
                        'description' => Yii::t('AccountingModule.Configuration', 'BillTrafficLocation')
                    ],
                    [
                        'name' => 'show_filing_number',
                        'display_name' => Yii::t('AccountingModule.Configuration', 'ShowFilingNumber'),
                        'type' => 'boolean',
                        'value_not_null' => true,
                        'default' => true,
                        'description' => Yii::t('AccountingModule.Configuration', 'ShowFilingNumber')
                    ],
                    [
                        'name' => 'issue_location_id',
                        'display_name' => Yii::t('AccountingModule.Configuration', 'BillIssueLocation'),
                        'type' => 'searchField',
                        'type_params' => [
                            'modelName' => City::class
                        ],
                        'description' => Yii::t('AccountingModule.Configuration', 'BillIssueLocation')
                    ]
                ],
            ],
        ]
    ],
    array(
        'name' => 'account_order_template',
        'display_name'=>'Template za nalog za knjizenje',
        'type'=>'searchField',
        'type_params' => array(
            'modelName'=>'Template',
        ),
        'description'=>'Template za nalog za knjizenje'
    ),
    [
        'name' => 'name_for_unclassified',
        'display_name'=>'Naziv za nerazvrstani konto',
        'type'=>'textField',
        'value_not_null' => false,
        'default' => '0',
        'description'=>"Ukoliko ostane prazno, bice 'Nerazvrstano' na sistemskom jeziku"
    ],
    [
        'name' => 'book_bill_paymnet_connections_by_date_or_type',
        'display_name'=>'Knjiži vezu plaćanja/avansa i računa po datumu ili tipu plaćanja/avansa',
        'type'=>'dropdown',
        'type_params' => [
            [
                'title' => 'po datumu',
                'value' => 'BY_DATE'
            ],
            [
                'title' => 'po tipu',
                'value' => 'BY_TYPE'
            ]
        ],
        'value_not_null' => true,
        'user_changable' => false,
        'default' => 'BY_DATE',
        'description'=>'Ukoliko je po datumu, gleda se datum placanja i datum povezanog racuna i na osnovu toga se zaključuje da li je avansna uplata ili nije.'
        . ' Ukoliko je po tipu, onda se samo gleda kako je plaćanje obeleženo'
    ],
    [
        'name' => 'paymnet_same_day_as_bill_is_advance',
        'display_name'=>'Ako su račun i uplata isti dan, onda je avansno',
        'type'=>'dropdown',
        'type_params' => [
            [
                'title' => 'Da',
                'value' => 1
            ],
            [
                'title' => 'Ne',
                'value' => 0
            ]
        ],
        'value_not_null' => true,
        'user_changable' => false,
        'default'=>1,
        'description'=>'Ovo se uglavnom dešava kada se uplaćuje po predračunu.'
        . ' Ukoliko je po tipu, onda se samo gleda kako je plaćanje obeleženo'
    ],
    [
        'name' => 'account_order_order_only_numbers',
        'display_name' => Yii::t('AccountingModule.Configuration', 'AccountOrderOrderOnlyNumbers'),
        'description' =>Yii::t('AccountingModule.Configuration', 'AccountOrderOrderOnlyNumbersDesc'),
        'value_not_null' => true,
        'type' => 'boolean',
        'default' => true
    ],
    [
        'name' => 'show_booked_timestamp',
        'display_name' => Yii::t('AccountingModule.Config','ShowBookedTimestamp'),
        'type'=>'dropdown',
        'type_params'=>array(
            array(
                'title'=>'Da',
                'value'=>1
            ),
            array(
                'title'=>'Ne',
                'value'=>0
            )
        ),
        'value_not_null' => true,
        'default'=>1,
        'description' => Yii::t('AccountingModule.Config','ShowBookedTimestampDesc')
    ],
    [
        'name' => 'show_transaction_comments',
        'display_name' => Yii::t('AccountingModule.Config','ShowTransactionComments'),
        'type'=>'dropdown',
        'type_params'=>array(
            array(
                'title'=>'Da',
                'value'=>1
            ),
            array(
                'title'=>'Ne',
                'value'=>0
            )
        ),
        'value_not_null' => true,
        'default'=>1,
        'description' => Yii::t('AccountingModule.Config','ShowTransactionCommentsDesc')
    ],
    [
        'name' => 'create_account_order_per_document',
        'display_name' => Yii::t('AccountingModule.Config','CreateAccountOrderPerDocument'),
        'type'=>'dropdown',
        'type_params'=>array(
            array(
                'title'=>Yii::t('BaseModule.Common','Yes'),
                'value'=>1
            ),
            array(
                'title'=>Yii::t('BaseModule.Common','No'),
                'value'=>0
            )
        ),
        'value_not_null' => true,
        'default'=>0,
        'description'=>Yii::t('AccountingModule.Config','CreateAccountOrderPerDocumentDesc')
    ],
    [
        'name' => 'auto_connect_advance_bill_account_order_to_bank_statement_account_order',
        'display_name' => Yii::t('AccountingModule.Config','ConnectAccountOrderForAdvanceBillAndPayment'),
        'type' => 'boolean',
        'value_not_null' => true,
        'default' => true,
        'description'=>Yii::t('AccountingModule.Config','ConnectAccountOrderForAdvanceBillAndPaymentDesc')
    ],
    [
        'name' => 'cost_location_name_generator',
        'display_name' => Yii::t('AccountingModule.Config','CostLocationNameGenerator'),
        'type' => 'boolean',
        'value_not_null' => true,
        'default' => false,
        'user_changable' => false,
        'description'=>Yii::t('AccountingModule.Config','CostLocationNameGenerator')
    ],
    [
        'name' => 'counters',
        'display_name' => UserConfigCounterGUI::modelLabel(true),
        'type' => 'group',
        'type_params' => [
            [
                'name' => 'bill_out',
                'display_name' => 'Brojač izlaznih faktura',
                'type'=>'searchField',
                'type_params' => array(
                    'modelName'=>'UserConfigCounter',
                ),
                'value_not_null'=>false,
                'description' => 'Brojač izlaznih računa',
            ],
            [
                'name' => 'advance_bill_out',
                'display_name' => 'Brojač izlaznih avansnih faktura',
                'type'=>'searchField',
                'type_params' => array(
                    'modelName'=>'UserConfigCounter',
                ),
                'value_not_null'=>false,
                'description' => 'Brojač izlaznih avansnih faktura',
            ],
            [
                'name' => 'pre_bill_out',
                'display_name' => 'Brojač izlaznih predračuna',
                'type'=>'searchField',
                'type_params' => array(
                    'modelName'=>'UserConfigCounter',
                ),
                'value_not_null'=>false,
                'description' => 'Brojač izlaznih predračuna',
            ],
            [
                'name' => 'relief_bill_out',
                'display_name' => 'Brojač izlaznih knjižnih odobrenja',
                'type'=>'searchField',
                'type_params' => array(
                    'modelName'=>'UserConfigCounter',
                ),
                'value_not_null'=>false,
                'description' => 'Brojač izlaznih knjižnih odobrenja',
            ],
            [
                'name' => 'unrelief_bill_out',
                'display_name' => 'Brojač izlaznih knjižnih zaduženja',
                'type'=>'searchField',
                'type_params' => array(
                    'modelName'=>'UserConfigCounter',
                ),
                'value_not_null'=>false,
                'description' => 'Brojač izlaznih knjižnih zaduženja',
            ],
            [
                'name' => 'account_order_bill_in',
                'display_name' => 'Nalog za knjizenje ulaznih računa',
                'type'=>'searchField',
                'type_params' => array(
                    'modelName'=>'UserConfigCounter',
                ),
                'value_not_null'=>false,
                'description' => 'Nalog za knjizenje ulaznih računa',
            ],
            [
                'name' => 'account_order_bill_out',
                'display_name' => 'Nalog za knjizenje izlaznih faktura',
                'type'=>'searchField',
                'type_params' => array(
                    'modelName'=>'UserConfigCounter',
                ),
                'value_not_null'=>false,
                'description' => 'Nalog za knjizenje izlaznih faktura',
            ],
            [
                'name' => 'account_order_bank_statement',
                'display_name' => 'Nalog za knjizenje izvoda iz banke',
                'type'=>'searchField',
                'type_params' => array(
                    'modelName'=>'UserConfigCounter',
                ),
                'value_not_null'=>false,
                'description' => 'Nalog za knjizenje izvoda iz banke',
            ],
        ]
    ],
    [
        'name' => 'display_denomination_table_in_cash_desk_log_report',
        'display_name' => Yii::t('AccountingModule.Config','DisplayDenominationTableInCashDeskLogReport'),
        'type' => 'dropdown',
        'type_params' => [
            [
                'title' => Yii::t('BaseModule.Common', 'Yes'),
                'value' => true
            ],
            [
                'title' => Yii::t('BaseModule.Common', 'No'),
                'value' => false
            ]
        ],
        'value_not_null' => true,
        'default' => true,
        'description' => Yii::t('AccountingModule.Config','DisplayDenominationTableInCashDeskLogReport')
    ],
    [
        'name' => 'default_cost_location_id',
        'display_name' => Yii::t('AccountingModule.Config', 'DefaultCostLocation'),
        'type' => 'searchField',
        'type_params' => [
            'modelName' => CostLocation::class,
        ],
        'user_changable' => false,
        'description' => Yii::t('AccountingModule.Config', 'DefaultCostLocation')
    ],
    [
        'name' => 'default_cost_cost_type_id',
        'display_name' => Yii::t('AccountingModule.Config', 'DefaultCostCostType'),
        'type' => 'searchField',
        'type_params' => [
            'modelName' => CostType::class,
            'fixed_filter' => [
                'cost_or_income' => CostType::$TYPE_COST
            ]
        ],
        'user_changable' => false,
        'description' => Yii::t('AccountingModule.Config', 'DefaultCostCostType')
    ],
    [
        'name' => 'default_income_cost_type_id',
        'display_name' => Yii::t('AccountingModule.Config', 'DefaultIncomeCostType'),
        'type' => 'searchField',
        'type_params' => [
            'modelName' => CostType::class,
            'fixed_filter' => [
                'cost_or_income' => CostType::$TYPE_INCOME
            ]
        ],
        'user_changable' => false,
        'description' => Yii::t('AccountingModule.Config', 'DefaultIncomeCostType')
    ],
    [
        'name' => 'bill_in_automatic_confirmation1',
        'display_name' => Yii::t('AccountingModule.Config', 'BillInAutomaticConfirmation1'),
        'type' => 'boolean',
        'description' => Yii::t('AccountingModule.Config', 'BillInAutomaticConfirmation1')
    ],
    [
        'name' => 'bill_in_automatic_file_signature_users',
        'display_name' => Yii::t('AccountingModule.Config', 'BillInAutomaticFileSignatureUsers'),
        'type' => 'searchField',
        'type_params' => [
            'modelName' => 'User'
        ],
        'value_is_array' => true,
        'description' => Yii::t('AccountingModule.Config', 'BillInAutomaticFileSignatureUsers')
    ],
    [
        'name' => 'bill_in_file_signature_waiting_for_subordinates',
        'display_name' => Yii::t('AccountingModule.Config', 'BillInFileSignatureWaitingForSubordinates'),
        'type' => 'boolean',
        'user_changable' => true,
        'description' => Yii::t('AccountingModule.Config', 'BillInFileSignatureWaitingForSubordinates')
    ],
    [
        'name' => 'bill_in_automatic_confirmation2',
        'display_name' => Yii::t('AccountingModule.Config', 'BillInAutomaticConfirmation2'),
        'type' => 'boolean',
        'description' => Yii::t('AccountingModule.Config', 'BillInAutomaticConfirmation2')
    ],
    [
        'name' => 'accounting_statistical_report_ownership_designation',
        'display_name' => Yii::t('AccountingModule.Config', 'AccountingStatisticalReportOwnershipDesignation'),
        'type' => 'dropdown',
        'type_params' => [
            [
                'title' => Yii::t('AccountingModule.Config', 'AccountingStatisticalReportOwnershipDesignationForSociety'),
                'value' => '1'
            ],
            [
                'title' => Yii::t('AccountingModule.Config', 'AccountingStatisticalReportOwnershipDesignationForPrivate'),
                'value' => '2'
            ],
            [
                'title' => Yii::t('AccountingModule.Config', 'AccountingStatisticalReportOwnershipDesignationForCooperative'),
                'value' => '3'
            ],
            [
                'title' => Yii::t('AccountingModule.Config', 'AccountingStatisticalReportOwnershipDesignationForMixed'),
                'value' => '4'
            ],
            [
                'title' => Yii::t('AccountingModule.Config', 'AccountingStatisticalReportOwnershipDesignationForState'),
                'value' => '5'
            ]
        ],
        'user_changable' => false,
        'description' => Yii::t('AccountingModule.Config', 'AccountingStatisticalReportOwnershipDesignation')
    ]
);