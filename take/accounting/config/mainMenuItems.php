<?php

return [
    'accounting2' => [
        'order' => 30,
        'visible' => Yii::app()->user->checkAccess('menuShowAccounts'),
        'label' => Yii::t('MainMenu', 'Accounting 2.0'),
        'icon' => 'fas fa-landmark',
        'subitems' => [
            [
                'label' => Yii::t('AccountingModule.OverviewReport', 'OverviewReport'),
                'visible' => Yii::app()->user->checkAccess('menuShowAccounts_OverviewReport'),
                'url' => 'javascript:sima.mainTabs.openNewTab("accounting/overviewReport");',
                'component_name' => 'accounting-overview_report'
            ],
//            [
//                'label' => 'Ulazni računi/troskovi',
//                'visible' => Yii::app()->user->checkAccess('menuShowAccounts_records'),
//                'url' => 'javascript:sima.mainTabs.openNewTab("accounting/billIn");',
//                'action' => 'accounting/billIn'
//            ],
//            [
//                'label' => 'Izlazni racuni/fakturisanje',
//                'visible' => Yii::app()->user->checkAccess('menuShowAccounts_records'),
//                'url' => 'javascript:sima.mainTabs.openNewTab("accounting/billOut");',
//                'action' => 'accounting/billOut'
//            ],
            [
                'label' => 'Finansijsko knjigovodstvo',
//                'visible' => Yii::app()->user->checkAccess('menuShowAccounts_records'),
                'visible' => Yii::app()->user->checkAccess('menuShowAccounts_bookKeeping'),
                'url' => 'javascript:sima.mainTabs.openNewTab("accounting/accounting");',
                'action' => 'accounting/accounting'
            ],
            [
                'label' => 'PDV pregled',
//                'visible' => Yii::app()->user->checkAccess('menuShowAccounts_records'),
                'visible' => Yii::app()->user->checkAccess('menuShowAccounts_vat'),
                'url' => 'javascript:sima.mainTabs.openNewTab("accounting/vat");',
                'action' => 'accounting/vat'
            ],
            [
                'label' => 'Magacin',
                'url' => 'javascript:sima.mainTabs.openNewTab("accounting/warehouse/warehouse/all");',
                'visible' => Yii::app()->user->checkAccess('menuShowAccounts_warehouse'),
                'action' => 'accounting/warehouse/warehouse/all'
            ],
            [
                'label' => 'Fakturisanje betona',
                'visible' => Yii::app()->hasModule('CF') && Yii::app()->user->checkAccess('menuShowAccounts_CF'),
                'url' => 'javascript:sima.mainTabs.openNewTab("CF/finance/index");',
                'action' => 'CF/finance/index'
            ],
            [
                'label' => Yii::t('MainMenu', 'PaycheckCalculation'),
                'url' => 'javascript:sima.mainTabs.openNewTab("accounting/paychecks/paycheckPeriod");',
                'action' => 'accounting/paychecks/paycheckPeriod',
                'visible' => Yii::app()->user->checkAccess('menuShowAccounts_paychecks'),
                'subitems' => [
                    [
                        'label' => Yii::t('MainMenu', 'PaycheckCalculation'),
                        'url' => 'javascript:sima.mainTabs.openNewTab("accounting/paychecks/paycheckPeriod");',
                        'action' => 'accounting/paychecks/paycheckPeriod',
                        'visible' => Yii::app()->configManager->get('base.develop.use_new_layout', false)
                    ],
                    [
                        'label' => Yii::t('MainMenu', 'ComparePaycheck'),
                        'url' => 'javascript:sima.mainTabs.openNewTab("accounting/paychecks/paycheck/comparePaychecksTab");',
                        'action' => 'accounting/paychecks/paycheck/comparePaychecksTab'
                    ],
                    [
                        'label' => 'Finansijski meseci',
                        'url' => 'javascript:sima.mainTabs.openNewTab("guitable", "accounting_27");',
                        'action' => ['guitable', 'accounting_27']
                    ]
                ]
            ],
            [
                'label' => 'Blagajna',
                'visible' => Yii::app()->user->checkAccess('menuShowAccounts_CashDesk'),
                'url' => 'javascript:sima.mainTabs.openNewTab("accounting/cashDesk");',
                'action' => 'accounting/cashDesk'
            ],
            [
                'label' => Yii::t('AccountingModule.PPPPD', 'PPPPDs'),
                'visible' => Yii::app()->user->checkAccess('menuShowAccounts_PPPPD'),
                'url' => 'javascript:sima.mainTabs.openNewTab("accounting/PPPPD/mainMenu");',
                'action' => 'accounting/PPPPD/mainMenu',
                'subitems' => [
                    [
                        'label' => Yii::t('AccountingModule.PPPPD', 'PPPPDs'),
                        'url' => 'javascript:sima.mainTabs.openNewTab("accounting/PPPPD/mainMenu");',
                        'action' => 'accounting/PPPPD/mainMenu',
                        'visible' => Yii::app()->configManager->get('base.develop.use_new_layout', false)
                    ],
                    [
                        'label' => Yii::t('AccountingModule.PPPPD', 'ComparePPPPDs'),
                        'url' => 'javascript:sima.mainTabs.openNewTab("accounting/PPPPD/comparePPPPDs");',
                        'action' => 'accounting/PPPPD/comparePPPPDs'
                    ]
                ]
            ],
            [
                'label' => Yii::t('AccountingModule.Common', 'OtherRecords'),
//                'visible' => Yii::app()->user->checkAccess('menuShowAccounts_paychecks'),
//                'url' => 'javascript:sima.mainTabs.openNewTab("accounting/PPPPD/mainMenu");',
//                'action' => 'accounting/PPPPD/mainMenu',
                'subitems' => [
                    [
                        'label' => 'Menice',
//                        'visible' => Yii::app()->user->checkAccess('menuShowAccounts_BillOfExchange'),
                        'url' => 'javascript:sima.mainTabs.openNewTab("guitable", "accounting_2");',
                        'action' => ['guitable', 'accounting_2']
                    ],
                    [
                        'label' => 'Bankarske garancije',
//                        'visible' => Yii::app()->user->checkAccess('menuShowAccounts_BankGuarantee'),
                        'url' => 'javascript:sima.mainTabs.openNewTab("guitable", "accounting_26");',
                        'action' => ['guitable', 'accounting_26']
                    ],
                ]
            ],
            [
                'label' => Yii::t('AccountingModule.DebtTakeOver', 'DebtTakeOvers'),
                'visible' => Yii::app()->user->checkAccess('menuShowAccounts_records'),
                'url' => 'javascript:sima.mainTabs.openNewTab("guitable", "accounting_33");',
                'action' => ['guitable', 'accounting_33']
            ],
            [
                'label' => Yii::t('AccountingModule.Common', 'Codebooks'),
//                'visible' => Yii::app()->user->checkAccess('menuShowAccounts_conf'),
                'subitems' => [
                    [
                        'label' => 'Žiro računi',
                        'visible' => Yii::app()->user->checkAccess('menuShowAccounts_conf'),
                        'url' => 'javascript:sima.mainTabs.openNewTab("guitable", "accounting_25");',
                        'action' => ['guitable', 'accounting_25']
                    ],
                    [
                        'label' => 'Banke',
                        'visible' => Yii::app()->user->checkAccess('menuShowAccounts_conf'),
                        'url' => 'javascript:sima.mainTabs.openNewTab("guitable", "accounting_24");',
                        'action' => ['guitable', 'accounting_24']
                    ],
                    [
                        'label' => 'Šifre plaćanja',
                        'visible' => Yii::app()->user->checkAccess('menuShowAccounts_conf'),
                        'url' => 'javascript:sima.mainTabs.openNewTab("guitable", "accounting_23");',
                        'action' => ['guitable', 'accounting_23']
                    ],
                    [
                        'label' => 'Vrste placanja',
                        'visible' => Yii::app()->user->checkAccess('menuShowAccounts_conf'),
                        'url' => 'javascript:sima.mainTabs.openNewTab("guitable", "accounting_22");',
                        'action' => ['guitable', 'accounting_22']
                    ],
                    [
                        'label' => 'Mesta troška',
                        'visible' => Yii::app()->user->checkAccess('menuShowAccounts_conf'),
                        'url' => 'javascript:sima.mainTabs.openNewTab("guitable", "accounting_21");',
                        'action' => ['guitable', 'accounting_21']
                    ],
                    [
                        'label' => 'Stornirano',
                        'visible' => Yii::app()->user->checkAccess('menuShowAccounts_conf'),
                        'url' => 'javascript:sima.mainTabs.openNewTab("guitable", "accounting_20");',
                        'action' => ['guitable', 'accounting_20']
                    ],
                    [
                        'label' => Yii::t('BaseModule.GuiTable', 'Currencies'),
                        'visible' => Yii::app()->user->checkAccess('menuShowAccounts_conf'),
                        'url' => 'javascript:sima.mainTabs.openNewTab("guitable", "base_9");',
                        'action' => ['guitable', 'base_9']
                    ],
                    [
                        'label' => Yii::t('BaseModule.GuiTable', 'MiddleCurrencyRates'),
                        'visible' => Yii::app()->user->checkAccess('menuShowAccounts_conf_middleCurrencyRate'),
                        'url' => 'javascript:sima.mainTabs.openNewTab("guitable", "accounting_19");',
                        'action' => ['guitable', 'accounting_19']
                    ],
                    [
                        'label' => Yii::t('BaseModule.MeasurementUnit', 'MeasurementUnits'),
                        'visible' => Yii::app()->user->checkAccess('menuShowAccounts_conf'),
                        'url' => 'javascript:sima.mainTabs.openNewTab("guitable", "base_10");',
                        'action' => ['guitable', 'base_10']
                    ],
                    [
                        'label' => Yii::t('FixedAssetsModule.FixedAssetAmortizationGroup', 'FixedAssetAmortizationGroups'),
                        'visible' => Yii::app()->user->checkAccess('menuShowAccounts_conf'),
                        'url' => 'javascript:sima.mainTabs.openNewTab("guitable", "fixedAssets_18");',
                        'action' => ['guitable', 'fixedAssets_18']
                    ],
                    [
                        'label' => Yii::t('AccountingModule.NoteTaxExemption', 'NotesOfTaxExemption'),
                        'visible' => Yii::app()->user->checkAccess('menuShowAccounts_conf'),
                        'url' => 'javascript:sima.mainTabs.openNewTab("guitable", "accounting_32");',
                        'action' => ['guitable', 'accounting_32']
                    ]
                ]
            ],
//            [
//                'label' => Yii::t('AccountingModule.PPPPD', 'PPPPDs'),
//                'visible' => Yii::app()->user->checkAccess('menuShowAccounts_paychecks'),
//                'url' => 'javascript:sima.mainTabs.openNewTab("accounting/PPPPD/mainMenu");',
//                'action' => 'accounting/PPPPD/mainMenu',
//                'subitems' => [
//                    [
//                        'label' => Yii::t('AccountingModule.PPPPD', 'ComparePPPPDs'),
//                        'url' => 'javascript:sima.mainTabs.openNewTab("accounting/PPPPD/comparePPPPDs");',
//                        'action' => 'accounting/PPPPD/comparePPPPDs'
//                    ]
//                ]
//            ]
        ]
    ],
    'accounting' => [
        'order' => 40,
        'visible' => Yii::app()->user->checkAccess('menuShowAccounts'),
        'label' => Yii::t('MainMenu', 'Accounting'),
        'icon' => 'fas fa-landmark',
        'subitems' => [
            [
                'label' => 'Ulaz',
                'visible' => Yii::app()->user->checkAccess('menuShowAccounts_records'),
                'subitems' => [
                    [
                        'label' => 'Računi',
                        'url' => 'javascript:sima.mainTabs.openNewTab("guitable", "accounting_18");',
                        'action' => ['guitable', 'accounting_18']
                    ],
                    [
                        'label' => 'Avansni računi',
                        'url' => 'javascript:sima.mainTabs.openNewTab("guitable", "accounting_17");',
                        'action' => ['guitable', 'accounting_17']
                    ], 
                    [
                        'label' => 'Predracuni',
                        'url' => 'javascript:sima.mainTabs.openNewTab("guitable", "accounting_16");',
                        'action' => ['guitable', 'accounting_16']
                    ],
                    [
                        'label' => 'Knjižna odobrenja',
                        'url' => 'javascript:sima.mainTabs.openNewTab("guitable", "accounting_15");',
                        'action' => ['guitable', 'accounting_15']
                    ],
                    [
                        'label' => 'Knjižna zaduženja',
                        'url' => 'javascript:sima.mainTabs.openNewTab("guitable", "accounting_14");',
                        'action' => ['guitable', 'accounting_14']
                    ]
                ]
            ],
            [
                'label' => 'Izlaz',
                'visible' => Yii::app()->user->checkAccess('menuShowAccounts_records'),
                'subitems' => [
                    [
                        'label' => 'Fakture',
                        'url' => 'javascript:sima.mainTabs.openNewTab("guitable", "accounting_12");',
                        'action' => ['guitable', 'accounting_12']
                    ],
                    [
                        'label' => 'Avansne fakture',
                        'url' => 'javascript:sima.mainTabs.openNewTab("guitable", "accounting_11");',
                        'action' => ['guitable', 'accounting_11']
                    ],
                    [
                        'label' => 'Predracuni',
                        'url' => 'javascript:sima.mainTabs.openNewTab("guitable", "accounting_10");',
                        'action' => ['guitable', 'accounting_10']
                    ],
                    [
                        'label' => 'Knjižna odobrenja',
                        'url' => 'javascript:sima.mainTabs.openNewTab("guitable", "accounting_9");',
                        'action' => ['guitable', 'accounting_9']
                    ],
                    [
                        'label' => 'Knjižna zaduženja',
                        'url' => 'javascript:sima.mainTabs.openNewTab("guitable", "accounting_8");',
                        'action' => ['guitable', 'accounting_8']
                    ],
                    [
                        'label' => RecurringBillGUI::modelLabel(true),
                        'url' => 'javascript:sima.mainTabs.openNewTab("guitable", "accounting_30");',
                        'action' => ['guitable', 'accounting_30']
                    ]
                ]
            ],
            [
                'label' => 'Izvodi',
//                'visible' => Yii::app()->user->checkAccess('menuShowAccounts_records'),
                'visible' => Yii::app()->user->checkAccess('menuShowAccounts_bankStatements'),
                'url' => 'javascript:sima.mainTabs.openNewTab("guitable", "accounting_7");',
                'action' => ['guitable', 'accounting_7'],
                'subitems' => [
                    [
                        'label' => 'Izvodi',
                        'url' => 'javascript:sima.mainTabs.openNewTab("guitable", "accounting_7");',
                        'action' => ['guitable', 'accounting_7'],
                        'visible' => Yii::app()->configManager->get('base.develop.use_new_layout', false)
                    ],
                    [
                        'label' => 'Placanja',
                        'url' => 'javascript:sima.mainTabs.openNewTab("guitable", "accounting_6");',
                        'action' => ['guitable', 'accounting_6']
                    ],
                    [
                        'label' => 'Za platiti',
                        'url' => 'javascript:sima.mainTabs.openNewTab("accounting/payment/forpay");',
                        'action' => 'accounting/payment/forpay'
                    ],
                    [
                        'label' => 'Primljeni avansi',
                        'url' => 'javascript:sima.mainTabs.openNewTab("guitable", "accounting_5");',
                        'action' => ['guitable', 'accounting_5']
                    ],
                    [
                        'label' => Yii::t('MainMenu', 'GivenAdvances'),
                        'url' => 'javascript:sima.mainTabs.openNewTab("guitable", "accounting_4");',
                        'action' => ['guitable', 'accounting_4']
                    ]
                ]
            ],
            [
                'label' => 'Proizvodnja',
                'visible' => Yii::app()->user->checkAccess('menuShowAccounts_production'),                        
                'subitems' => [
                    [
                        'label' => 'Pogoni',
                        'url' => 'javascript:sima.mainTabs.openNewTab("accounting/warehouse/wProduction/list");',
                        'action' => 'accounting/warehouse/wProduction/list'
                    ],
                    [
                        'label' => 'Recepti',
                        'url' => 'javascript:sima.mainTabs.openNewTab("accounting/warehouse/wProduction/recipes");',
                        'action' => 'accounting/warehouse/wProduction/recipes'
                    ]
                ]
            ],
            [
                'label' => 'Računi',
                'visible' => Yii::app()->user->checkAccess('menuShowAccounts_confirm1'),
                'url' => 'javascript:sima.mainTabs.openNewTab("guitable", "accounting_3");',
                'action' => ['guitable', 'accounting_3']
            ],
            [
                'label' => 'Osnovna sredstva',
                'visible' => Yii::app()->user->checkAccess('menuShowAccounts_FA'),
                'url' => 'javascript:sima.mainTabs.openNewTab("accounting/fixedAssets");',
                'action' => 'accounting/fixedAssets',
                'subitems' => [
                    [
                        'label' => 'Spisak',
                        'url' => 'javascript:sima.mainTabs.openNewTab("accounting/fixedAssets");',
                        'action' => 'accounting/fixedAssets'
                    ],
                    [
                        'label' => 'Amortizacije',
                        'url' => 'javascript:sima.mainTabs.openNewTab("accounting/fixedAssets/depreciations");',
                        'action' => 'accounting/fixedAssets/depreciations'
                    ],
                    [
                        'label' => 'Tipovi osnovnih sredstava',
                        'url' => 'javascript:sima.mainTabs.openNewTab("guitable", "fixedAssets_15");',
                        'action' => ['guitable', 'fixedAssets_15']
                    ],
                    [
                        'label' => 'Odluke o deaktiviranju osnovnih sredstava',
                        'visible' => Yii::app()->user->checkAccess('menuShowFixedAssetsDeactivationDecisions'),
                        'url' => 'javascript:sima.mainTabs.openNewTab("fixedAssets/fixedAsset/fixedAssetsDeactivationDecisions");',
                        'action' => 'fixedAssets/fixedAsset/fixedAssetsDeactivationDecisions'
                    ]
                ]
            ]
        ]
    ]
];
