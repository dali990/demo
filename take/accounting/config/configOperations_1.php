<?php

return array(
    'accountingJobStat',
    'accountingFixedAssetStat',
    'FileCashDeskTab',
    'FileBankStatmentTab',
    'FileBillTab',
    'fileBookkeepingTab' => [
        'description' => 'Privilegija da svaki dokument ima Tab za knjizenje'
    ],
    'accountingPartnerStat' => array(
        'description' => 'Osoba koja moze videti finansijske izvestaje'
    ),
    
    'AdvanceBillsNotConnected' => [
        'description' => 'prikazuje warn sa spiskom svih avansnih racuna koji nisu konektovani'
    ],
    'ResolveDuplicatedBankAccounts' => [
        'description' => Yii::t('AccountingModule.Warns','DuplicatedBankAccounts')
    ],
    'AddBillsToAccountOrder' => [
        'description' => Yii::t('AccountingModule.Bill', 'AddBillsToAccountOrderPrivilegeDesc')
    ],
    'WarnBillDocumentsWithNoSignature' => [
         'description' => Yii::t('AccountingModule.Warns','BillDocumentsWithNoSignature')
    ]
);
