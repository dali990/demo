<?php

return [
    'WARN_ADVANCE_BILLS_NOT_CONNECTED' => [
        'title' => 'Avansni računi koji nisu konektovani',
        'onclick' => ['sima.mainTabs.openNewTab','guitable', 'accounting_28'],
        'countFunc' => function($user) {
            $cnt = 0;
            if(Yii::app()->authManager->checkAccess('AdvanceBillsNotConnected',$user->id)) 
            {
                $criteria = new SIMADbCriteria([
                    'Model' => 'AdvanceBill',
                    'model_filter' => [                        
                        'connected'=>false
                    ],
                ]);
                $cnt = AdvanceBill::model()->count($criteria);
            }
            
            return $cnt;
        }
    ],
    'WARN_DUPLICATED_BANK_ACCOUNTS' => [
        'title' => Yii::t('AccountingModule.Warns','DuplicatedBankAccounts'),
        'onclick' => ['sima.mainTabs.openNewTab','guitable', 'accounting_29'],
        'countFunc' => function($user) {
            $cnt = 0;
            
            if(Yii::app()->authManager->checkAccess('ResolveDuplicatedBankAccounts',$user->id))
            {
                $cnt = BankAccount::model()->countByModelFilter([
                    'display_scopes' => 'onlyDistinctDuplicated'
                ]);
            }
            
            return $cnt;
        }
    ],
    'WARN_BILL_DOCUMENTS_WITH_NO_SIGNATURE' => [
        'title' => Yii::t('AccountingModule.Warns','BillDocumentsWithNoSignature'),
        'onclick' => ['sima.mainTabs.openNewTab','guitable', 'accounting_31'],
        'countFunc' => function($user) {
            $cnt = 0;
            
            if(Yii::app()->authManager->checkAccess('WarnBillDocumentsWithNoSignature',$user->id))
            {
                $cnt = Bill::model()->countByModelFilter([
                    'scopes' => 'withNoSignature'
                ]);
            }
            
            return $cnt;
        }
    ],
];