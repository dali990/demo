<?php

return array(
    array(
        'name' => 'bills',
        'display_name' => 'Računi',
        'type' => 'group',
        'type_params' => array(
            array(
                'name' => 'bill',
                'display_name' => 'Račun',
                'type' => 'searchField',
                'type_params' => array(
                    'modelName' => 'DocumentType',
                ),
                'value_not_null' => false,
                'value_is_array' => true,
                'condition' => array(
                    'bill' => array(
                        'bill_type' => Bill::$BILL
                    )
                ),
                'user_change_forbidden' => true,
                'description' => 'Tip dokumenta za Račun.'
            ),
            array(
                'name' => 'advance_bill',
                'display_name' => 'Avansni račun',
                'type' => 'searchField',
                'type_params' => array(
                    'modelName' => 'DocumentType',
                ),
                'value_not_null' => false,
                'value_is_array' => true,
                'condition' => array(
                    'bill' => array(
                        'bill_type' => Bill::$ADVANCE_BILL
                    )
                ),
                'user_change_forbidden' => true,
                'description' => 'Tip dokumenta za Avansni Račun.'
            ),
            array(
                'name' => 'pre_bill',
                'display_name' => 'Predracun',
                'type' => 'searchField',
                'type_params' => array(
                    'modelName' => 'DocumentType',
                ),
                'value_not_null' => false,
                'value_is_array' => true,
                'condition' => array(
                    'bill' => array(
                        'bill_type' => Bill::$PRE_BILL
                    )
                ),
                'user_change_forbidden' => true,
                'description' => 'Predracun.'
            ),
            array(
                'name' => 'relief_bill',
                'display_name' => 'Knjizno odobrenje',
                'type' => 'searchField',
                'type_params' => array(
                    'modelName' => 'DocumentType',
                ),
                'value_not_null' => false,
                'value_is_array' => true,
                'condition' => array(
                    'bill' => array(
                        'bill_type' => Bill::$RELIEF_BILL
                    )
                ),
                'user_change_forbidden' => true,
                'description' => 'Tip dokumenta za Knjizno odobrenje.'
            ),
            array(
                'name' => 'unrelief_bill',
                'display_name' => 'Knjizno zaduzenje',
                'type' => 'searchField',
                'type_params' => array(
                    'modelName' => 'DocumentType',
                ),
                'value_not_null' => false,
                'value_is_array' => true,
                'condition' => array(
                    'bill' => array(
                        'bill_type' => Bill::$UNRELIEF_BILL
                    )
                ),
                'user_change_forbidden' => true,
                'description' => 'Tip dokumenta za Knjizno odobrenje.'
            )
        )
    ),
    array(
        'name' => 'account_order',
        'display_name' => 'Nalog za knjizenje',
        'type' => 'searchField',
        'type_params' => array(
            'modelName' => 'DocumentType',
        ),
        'value_not_null' => false,
        'condition' => array(
            'account_order' => array()
        ),
        'user_change_forbidden' => true,
        'description' => 'Tip dokumenta za naloge za knjizenje.'
    ),
    array(
        'name' => 'bank_statement',
        'display_name' => 'Izvod iz banke',
        'type' => 'searchField',
        'type_params' => array(
            'modelName' => 'DocumentType',
        ),
        'value_not_null' => false,
        'condition' => array(
        ),
        'user_change_forbidden' => true,
        'description' => 'Tip dokumenta za Izvod iz banke.'
    ),
    array(
        'name' => 'bill_of_exchange',
        'display_name' => 'Menica',
        'type' => 'searchField',
        'type_params' => array(
            'modelName' => 'DocumentType',
        ),
        'value_not_null' => false,
        'condition' => array(
            'bill_of_exchange' => array()
        ),
        'user_change_forbidden' => true,
        'description' => 'Tip dokumenta za Menice u delu Finansije.'
    ),
    array(
        'name' => 'bill_of_exchange_authorization_file',
        'display_name' => 'Menično ovlašćenje',
        'type' => 'searchField',
        'type_params' => array(
        'modelName' => 'DocumentType',
        ),
        'value_not_null' => false,
        'condition' => array(
            'bill_of_exchange_authorization_file' => array()
        ),
        'user_change_forbidden' => true,
        'description' => 'Tip dokumenta za Menično ovlašćenje u delu Finansije.'
    ),
    array(
        'name' => 'debt_take_over_document_type_id',
        'display_name' => 'Ugovor o asignaciji',
        'type' => 'searchField',
        'type_params' => array(
            'modelName' => 'DocumentType',
        ),
        'value_not_null' => false,
        'condition' => array(
            'debt_take_over' => array()
        ),
        'user_change_forbidden' => true,
        'description' => 'Tip dokumenta za Ugovor o preuzimanju duga u delu Finansije.'
    ),
    array(
        'name' => 'cash_desk',
        'display_name' => Yii::t('AccountingModule.Configuration', 'Cash desk'),
        'type' => 'group',
        'type_params' => array(
            array(
                'name' => 'log',
                'display_name' => 'Dnevnik blagajne - tip dokumenta',
                'type' => 'searchField',
                'type_params' => array(
                    'modelName' => 'DocumentType',
                ),
                'value_not_null' => false,
                'condition' => array(
                    'cash_desk_log' => array()
                ),
                'user_change_forbidden' => true,
                'description' => 'Tip dokumenta za dnevnik blagajne.'
            ),
            array(
                'name' => 'order',
                'display_name' => 'Nalog blagajne - tip dokumenta',
                'type' => 'searchField',
                'type_params' => array(
                    'modelName' => 'DocumentType',
                ),
                'value_not_null' => false,
                'condition' => array(
                    'cash_desk_order' => array()
                ),
                'user_change_forbidden' => true,
                'description' => 'Tip dokumenta za nalog blagajne.'
            ),
        )
    )
);
