<?php

return [
    'name' => 'codes',
    'display_name' => Yii::t('AccountingModule.Configuration', 'Codes'),
    'type' => 'group',
    'type_params' => [
        [
            'name' => 'default_costs_account',
            'display_name'=>'Podrazumevani nalog za troskove',
            'type'=>'textField',
            'value_not_null' => true,
            'default'=>'53',
            'description'=>'Podrazumevani nalog za troskove'
        ],
        [
            'name' => 'bill_in_interest',
            'display_name'=>'Kamata ulaznih racuna',
            'type'=>'textField',
            'value_not_null' => true,
            'default'=>'5625',
            'description'=>'Kamata na racune dobavljaca'
        ],
        [
            'name' => 'default_income_account',
            'display_name'=>'Podrazumevani konto za prihod',
            'type'=>'textField',
            'value_not_null' => true,
            'default'=>'614',
            'description'=>'Podrazumevani konto za prihod'
        ],
        [
            'name' => 'fixed_assets',
            'display_name' => 'OS',
            'type' => 'group',
            'type_params' => [
                [
                    'name' => 'depreciation_expenses_account',
                    'display_name' => 'Troskovi amortizacije',
                    'type'=>'textField',
                    'value_not_null' => true,
                    'default'=>'540',
                    'description'=>'Konto na koji se knjize troskovi amortizacije',
                ],
                [
                    'name' => 'deactivation_account',
                    'display_name' => 'Konto za deaktivaciju',
                    'type'=>'textField',
                    'value_not_null' => true,
                    'default'=>'0239',
                    'description'=>'Konto na koji se knjize troskovi amortizacije',
                ],
                [
                    'name' => 'deactivation_cost_account',
                    'display_name' => 'Konto za troskove deaktivacije deaktivaciju',
                    'type'=>'textField',
                    'value_not_null' => true,
                    'default'=>'570',
                    'description'=>'Konto na koji se knjize troskovi amortizacije',
                ],
            ]
        ],
        [
            'name' => 'positive_currency_diff',
            'display_name'=>'Pozitivne kursne razlike',
            'type'=>'textField',
            'value_not_null' => true,
            'default'=>'663',
            'description'=>'Konto na koji se knjize pozitivne kursne razlike'
        ],
        [
            'name' => 'negative_currency_diff',
            'display_name'=>'Negativne kursne razlike',
            'type'=>'textField',
            'value_not_null' => true,
            'default'=>'563',
            'description'=>'Konto na koji se knjize negativne kursne razlike'
        ],
        [
            'name' => 'default_bank_account_account',
            'display_name'=>'Tekuci racuni ',
            'type'=>'textField',
            'value_not_null' => true,
            'default'=>'241',
            'description'=>'Osnovni konto za tekuce racune '
        ],
        [
            'name' => 'cash_desk',
            'display_name'=>'Blagajna',
            'type'=>'textField',
            'value_not_null' => true,
            'default'=>'243',
            'description'=>'Konto za blagajnu'
        ],
        [
            'name' => 'cash_desk_transition',
            'display_name'=>'Blagajna - prelazni konto',
            'type'=>'textField',
            'value_not_null' => true,
            'default'=>'2439',
            'description'=>'Konto za blagajnu - prelazni konto'
        ],
        [
            'name' => 'partners',
            'display_name' => Yii::t('AccountingModule.Configuration', 'Partners'),
            'type' => 'group',
            'type_params' => [
                array(
                    'name' => 'suppliers',
                    'display_name'=>'Dobavljači',
                    'type'=>'textField',
                    'value_not_null' => true,
                    'default'=>'435'
                ),
                array(
                    'name' => 'suppliers_ino',
                    'display_name'=>'Dobavljači u inostranstvu',
                    'type'=>'textField',
                    'value_not_null' => true,
                    'default'=>'436'
                ),
                array(
                    'name' => 'customers',
                    'display_name'=>'Kupci',
                    'type'=>'textField',
                    'value_not_null' => true,
                    'default'=>'204',
                ),
                array(
                    'name' => 'customers_ino',
                    'display_name'=>'Kupci u inostranstvu',
                    'type'=>'textField',
                    'value_not_null' => true,
                    'default'=>'205',
                ),
                array(
                    'name' => 'advance_bill_in_materials',
                    'display_name'=>'Dati avansi',
                    'type'=>'textField',
                    'value_not_null' => true,
                    'default'=>'150',
                ),
                array(
                    'name' => 'advance_bill_in_materials_ino',
                    'display_name'=>'Dati avansi',
                    'type'=>'textField',
                    'value_not_null' => true,
                    'default'=>'151',
                ),
                array(
                    'name' => 'advance_bill_in_goods',
                    'display_name'=>'Dati avansi',
                    'type'=>'textField',
                    'value_not_null' => true,
                    'default'=>'152',
                ),
                array(
                    'name' => 'advance_bill_in_goods_ino',
                    'display_name'=>'Dati avansi',
                    'type'=>'textField',
                    'value_not_null' => true,
                    'default'=>'153',
                ),
                array(
                    'name' => 'advance_bill_in_services',
                    'display_name'=>'Dati avansi',
                    'type'=>'textField',
                    'value_not_null' => true,
                    'default'=>'154',
                ),
                array(
                    'name' => 'advance_bill_in_services_ino',
                    'display_name'=>'Dati avansi',
                    'type'=>'textField',
                    'value_not_null' => true,
                    'default'=>'155',
                ),
                array(
                    'name' => 'advance_bill_out',
                    'display_name'=>'Primljeni avansi',
                    'type'=>'textField',
                    'value_not_null' => true,
                    'default'=>'430'
                ),
            ],
        ],
        [
            'name' => 'vat',
            'display_name' => Yii::t('AccountingModule.Configuration', 'VAT'),
            'type' => 'group',
            'type_params' => [
                [
                    'name' => 'income',
                    'display_name'=>'Ulazni PDV - sve',
                    'type'=>'textField',
                    'value_not_null' => true,
                    'default'=>'27',
                    'description'=>'konto za sav ulazni PDV - koristi se prilikom mesecnog obracuna'
                ],
                [
                    'name' => 'income_month_sum',
                    'display_name'=>'Ulazni PDV - mesecna razlika',
                    'type'=>'textField',
                    'value_not_null' => true,
                    'default'=>'279',
                    'description'=>'Ulazni PDV - koristi se prilikom mesecnog obracuna'
                ],
                [
                    'name' => 'income_small',
                    'display_name'=>'Ulazni PDV 10%',
                    'type'=>'textField',
                    'value_not_null' => true,
                    'default'=>'271',
                    'description'=>'Ulazni PDV 10%'
                ],
                [
                    'name' => 'income_small_internal',
                    'display_name'=>'Ulazni PDV 10% - IO',
                    'type'=>'textField',
                    'value_not_null' => true,
                    'default'=>'271009',
                    'description'=>'Ulazni PDV 10% - interni obracun'
                ],
                array(
                    'name' => 'income_big',
                    'display_name'=>'Ulazni PDV 20%',
                    'type'=>'textField',
                    'value_not_null' => true,
                    'default'=>'270',
                    'description'=>'Ulazni PDV 20%'
                ),
                [
                    'name' => 'income_big_internal',
                    'display_name'=>'Ulazni PDV 20% - IO',
                    'type'=>'textField',
                    'value_not_null' => true,
                    'default'=>'270009',
                    'description'=>'Ulazni PDV 20% - interni obracun'
                ],
                array(
                    'name' => 'advance_income_small',
                    'display_name'=>'Ulazni avansni PDV 10%',
                    'type'=>'textField',
                    'value_not_null' => true,
                    'default'=>'273',
                    'description'=>'Ulazni avansni PDV 10%'
                ),
                [
                    'name' => 'advance_income_small_internal',
                    'display_name'=>'Ulazni avansni PDV 10% - IO',
                    'type'=>'textField',
                    'value_not_null' => true,
                    'default'=>'273009',
                    'description'=>'Ulazni avansni PDV 10% - interni obracun'
                ],
                array(
                    'name' => 'advance_income_big',
                    'display_name'=>'Ulazni avansni PDV 20%',
                    'type'=>'textField',
                    'value_not_null' => true,
                    'default'=>'272',
                    'description'=>'Ulazni avansni PDV 20%'
                ),
                [
                    'name' => 'advance_income_big_internal',
                    'display_name'=>'Ulazni avansni PDV 20% - IO',
                    'type'=>'textField',
                    'value_not_null' => true,
                    'default'=>'272009',
                    'description'=>'Ulazni avansni PDV 20% - interni obracun'
                ],
                [
                    'name' => 'outcome',
                    'display_name'=>'Izlazni PDV - sve',
                    'type'=>'textField',
                    'value_not_null' => true,
                    'default'=>'47',
                    'description'=>'Izlazni PDV - zbirni nadkonto'
                ],
                [
                    'name' => 'outcome_month_sum',
                    'display_name'=>'Izlazni PDV - mesecna razlika',
                    'type'=>'textField',
                    'value_not_null' => true,
                    'default'=>'479',
                    'description'=>'Izlazni PDV - mesecna razlika - koristi se za obracun'
                ],
                [
                    'name' => 'outcome_small',
                    'display_name'=>'Izlazni PDV 10%',
                    'type'=>'textField',
                    'value_not_null' => true,
                    'default'=>'471',
                    'description'=>'Izlazni PDV 10%'
                ],
                [
                    'name' => 'outcome_small_internal',
                    'display_name'=>'Izlazni PDV 10% - IO',
                    'type'=>'textField',
                    'value_not_null' => true,
                    'default'=>'471009',
                    'description'=>'Izlazni PDV 10% - interni obracun'
                ],
                array(
                    'name' => 'outcome_big',
                    'display_name'=>'Izlazni PDV 20%',
                    'type'=>'textField',
                    'value_not_null' => true,
                    'default'=>'470',
                    'description'=>'Izlazni PDV 20%'
                ),
                [
                    'name' => 'outcome_big_internal',
                    'display_name'=>'Izlazni PDV 20% - IO',
                    'type'=>'textField',
                    'value_not_null' => true,
                    'default'=>'470009',
                    'description'=>'Izlazni PDV 20% - interni obracun'
                ],
                array(
                    'name' => 'advance_outcome_small',
                    'display_name'=>'Izlazni avansni PDV 10%',
                    'type'=>'textField',
                    'value_not_null' => true,
                    'default'=>'473',
                    'description'=>'Izlazni avansni PDV 10%'
                ),
                [
                    'name' => 'advance_outcome_small_internal',
                    'display_name'=>'Izlazni avansni PDV 10% - IO',
                    'type'=>'textField',
                    'value_not_null' => true,
                    'default'=>'473009',
                    'description'=>'Izlazni avansni PDV 10% - interni obracun'
                ],
                [
                    'name' => 'advance_outcome_big',
                    'display_name'=>'Izlazni avansni PDV 20%',
                    'type'=>'textField',
                    'value_not_null' => true,
                    'default'=>'472',
                    'description'=>'Izlazni avansni PDV 20%'
                ],
                [
                    'name' => 'advance_outcome_big_internal',
                    'display_name'=>'Izlazni avansni PDV 20% - IO',
                    'type'=>'textField',
                    'value_not_null' => true,
                    'default'=>'472009',
                    'description'=>'Izlazni avansni PDV 20% - interni obracun'
                ],
                [
                    'name' => 'retail_outcome',
                    'display_name'=>'Izlazni PDV za kes',
                    'type'=>'textField',
                    'value_not_null' => true,
                    'default'=>'476',
//                    'description'=>'Izlazni avansni PDV 20%'
                ],
                [
                    'name' => 'customs_costs',
                    'display_name'=>'Carinski racun',
                    'type'=>'textField',
                    'value_not_null' => true,
                    'default'=>'274', //482 je konto za obaveze, nije zamena za ovo neko je umesto 435 za upravu carina, ali je ovako jednostavnije
                    //moralo bi da se prati to placanje da li je vezano za carinski racun
                    'description'=>'Konto za carinski racun'
                ],
            ],
        ],
        [
            'name' => 'warehouse',
            'display_name' => Yii::t('AccountingModule.Configuration', 'Warehouse'),
            'type' => 'group',
            'type_params' => [
                [
                    'name' => 'material_expenses',
                    'display_name'=>'Troskovi materijala na trebovanju',
                    'type'=>'textField',
                    'value_not_null' => true,
                    'default'=>'511',
//                        'description'=>'Konto za obustave od plate'
                ],
                [
                    'name' => 'goods_initial_value',
                    'display_name'=>'Nabavna vrednost prodate robe ',
                    'type'=>'textField',
                    'value_not_null' => true,
                    'default'=>'501',
                ],
                [
                    'name' => 'income_from_goods',
                    'display_name'=>'Prihodi od prodaje robe na domaćem tržištu',
                    'type'=>'textField',
                    'value_not_null' => true,
                    'default'=>'604',
//                        'description'=>'Konto za obustave od plate'
                ],
                [
                    'name' => 'income_from_products_and_services',
                    'display_name'=>'Prihodi od prodaje proizvoda i usluga na domaćem tržištu',
                    'type'=>'textField',
                    'value_not_null' => true,
                    'default'=>'614',
                ],
                [
                    'name' => 'material_storage',
                    'display_name'=>'Magacin za materijal',
                    'type'=>'textField',
                    'value_not_null' => true,
                    'default'=>'101',
                    'description'=>'Magacin za materijal'
                ],
                [
                    'name' => 'material_storage_correction',
                    'display_name'=>'Konto za ispravljanje vrednosti magacina materijala',
                    'type'=>'textField',
                    'value_not_null' => true,
                    'default'=>'1019',
//                        'description'=>'Konto za ispravljanje vrednosti magacina'
                ],
                [
                    'name' => 'products_storage',
                    'display_name'=>'Magacin gotovih proizvoda',
                    'type'=>'textField',
                    'value_not_null' => true,
                    'default'=>'120',
//                        'description'=>'Podrazmevani magacin'
                ],
                [
                    'name' => 'products_storage_correction',
                    'display_name'=>'Konto za ispravljanje magacina gotovih proizvoda',
                    'type'=>'textField',
                    'value_not_null' => true,
                    'default'=>'1209',
//                        'description'=>'Konto za ispravljanje vrednosti magacina'
                ],
                [
                    'name' => 'semi_products_storage',
                    'display_name'=>'Magacin polu proizvoda',
                    'type'=>'textField',
                    'value_not_null' => true,
                    'default'=>'110',
//                        'description'=>'Podrazmevani magacin'
                ],
                [
                    'name' => 'semi_products_storage_correction',
                    'display_name'=>'Konto za ispravljanje vrednosti magacina poluproizvoda',
                    'type'=>'textField',
                    'value_not_null' => true,
                    'default'=>'1109',
//                        'description'=>'Konto za ispravljanje vrednosti magacina'
                ],
                [
                    'name' => 'retail_storage',
                    'display_name'=>'Roba u prodaji na malo',
                    'type'=>'textField',
                    'value_not_null' => true,
                    'default'=>'134',
//                        'description'=>'Podrazmevani magacin'
                ],
                [
                    'name' => 'retail_storage_correction',
                    'display_name'=>'Konto za ispravljanje vrednosti magacina u prodaji na malo',
                    'type'=>'textField',
                    'value_not_null' => true,
                    'default'=>'1349',
//                        'description'=>'Konto za ispravljanje vrednosti magacina'
                ],
                [
                    'name' => 'retail_storage_vat',
                    'display_name'=>'Konto za PDV vrednosti magacina u prodaji na malo',
                    'type'=>'textField',
                    'value_not_null' => true,
                    'default'=>'1344',
//                        'description'=>'Konto za ispravljanje vrednosti magacina'
                ],
                [
                    'name' => 'wholesail_storage',
                    'display_name'=>'Roba u prodaji na veliko',
                    'type'=>'textField',
                    'value_not_null' => true,
                    'default'=>'132',
//                        'description'=>'Podrazmevani magacin'
                ],
                [
                    'name' => 'wholesail_storage_correction',
                    'display_name'=>'Konto za ispravljanje vrednosti magacina u prodaji na veliko',
                    'type'=>'textField',
                    'value_not_null' => true,
                    'default'=>'1329',
//                        'description'=>'Konto za ispravljanje vrednosti magacina'
                ],
            ],
        ],
        [
            'name' => 'nine',
            'display_name' => Yii::t('AccountingModule.Configuration', 'Nine'),
            'type' => 'group',
            'type_params' => [
                [
                    'name' => 'excange_expences',
                    'display_name'=>'Preuzimanje troskova',
                    'type'=>'textField',
                    'value_not_null' => true,
                    'default'=>'902',
//                        'description'=>'Osnovni konto troska za plate'
                ],
                [
                    'name' => 'excange_expences_common_costs',
                    'display_name'=>'Preuzimanje troskova - opsti troskovi',
                    'type'=>'textField',
                    'value_not_null' => true,
                    'default'=>'9026',
//                        'description'=>'Osnovni konto troska za plate'
                ],
                [
                    'name' => 'excange_income',
                    'display_name'=>'Preuzimanje prihoda',
                    'type'=>'textField',
                    'value_not_null' => true,
                    'default'=>'903',
//                        'description'=>'Osnovni konto troska za plate'
                ],
                [
                    'name' => 'cost_cariers',
                    'display_name'=>'Trosak za plate',
                    'type'=>'textField',
                    'value_not_null' => true,
                    'default'=>'95',
//                        'description'=>'Osnovni konto troska za plate'
                ],
                [
                    'name' => 'finished_products',
                    'display_name'=>'Gotovi proizvodi',
                    'type'=>'textField',
                    'value_not_null' => true,
                    'default'=>'960',
//                        'description'=>'Osnovni konto troska za plate'
                ],
                [
                    'name' => 'finished_products_correction',
                    'display_name'=>'Gotovi proizvodi ispravka',
                    'type'=>'textField',
                    'value_not_null' => true,
                    'default'=>'969',
//                        'description'=>'Osnovni konto troska za plate'
                ],
                [
                    'name' => 'finished_product_expensess',
                    'display_name'=>'Troskovi gotovih proizvoda',
                    'type'=>'textField',
                    'value_not_null' => true,
                    'default'=>'980',
//                        'description'=>''
                ],
                [
                    'name' => 'retail_storage',
                    'display_name'=>'Roba u prodaji na malo',
                    'type'=>'textField',
                    'value_not_null' => true,
                    'default'=>'912',
//                        'description'=>'Podrazmevani magacin'
                ],
                [
                    'name' => 'retail_storage_correction',
                    'display_name'=>'Konto za ispravljanje vrednosti magacina u prodaji na malo',
                    'type'=>'textField',
                    'value_not_null' => true,
                    'default'=>'9129',
//                        'description'=>'Konto za ispravljanje vrednosti magacina'
                ],
                [
                    'name' => 'retail_storage_vat',
                    'display_name'=>'Konto za PDV vrednosti magacina u prodaji na malo',
                    'type'=>'textField',
                    'value_not_null' => true,
                    'default'=>'9122',
//                        'description'=>'Konto za ispravljanje vrednosti magacina'
                ],

//                    array(
//                        'name' => 'outcome_nine_percent',
//                        'display_name'=>'Trosak za plate',
//                        'type'=>'textField',
//                        'value_not_null' => true,
//                        'default'=>'450',
//                        'description'=>'Osnovni konto troska za plate'
//                    ),
            ],
        ],
        [
            'name' => 'paychecks',
            'display_name' => Yii::t('PaychecksModule.Configuration', 'Paychecks'),
            'type' => 'group',
            'type_params' => [
                [
                    'name' => 'costs_employee',
                    'display_name'=>'Trosak zaposlenog (mali bruto)',
                    'type'=>'textField',
                    'value_not_null' => true,
                    'default'=>'520',
                    'descripton' => 'Ceo mali bruto'
                ],
                [
                    'name' => 'costs_company',
                    'display_name'=>'Trosak poslodavca',
                    'type'=>'textField',
                    'value_not_null' => true,
                    'default'=>'521',
                    'descripton' => 'Porez i doprinos na teret poslodavca'
                ],
                [
                    'name' => 'obligation_emp_neto',
                    'display_name'=>'Obaveza za zaposlenog - neto',
                    'type'=>'textField',
                    'value_not_null' => true,
                    'default'=>'450'
                ],
                [
                    'name' => 'obligation_emp_neto_stops',
                    'display_name'=>'Trosak za obustavu od plate',
                    'type'=>'textField',
                    'value_not_null' => true,
                    'default'=>'450901',
                    'description'=>'Konto za administrativne zabrane od plate'
                ],
                [
                    'name' => 'obligation_emp_tax',
                    'display_name'=>'Obaveza za zaposlenog - porez',
                    'type'=>'textField',
                    'value_not_null' => true,
                    'default'=>'451'
                ],
                [
                    'name' => 'obligation_emp_comp',
                    'display_name'=>'Obaveza za zaposlenog - nadoknade',
                    'type'=>'textField',
                    'value_not_null' => true,
                    'default'=>'452'
                ],
                [
                    'name' => 'obligation_emp_pio',
                    'display_name'=>'Obaveza za zaposlenog - nadoknada za PIO',
                    'type'=>'textField',
                    'value_not_null' => true,
                    'default'=>'4520'
                ],
                [
                    'name' => 'obligation_emp_zdr',
                    'display_name'=>'Obaveza za zaposlenog - nadoknada za zdravstveno',
                    'type'=>'textField',
                    'value_not_null' => true,
                    'default'=>'4521'
                ],
                [
                    'name' => 'obligation_emp_nez',
                    'display_name'=>'Obaveza za zaposlenog - nadoknada za nezaposlenost',
                    'type'=>'textField',
                    'value_not_null' => true,
                    'default'=>'4522'
                ],
                [
                    'name' => 'obligation_company',
                    'display_name'=>'Obaveza za poslodavca',
                    'type'=>'textField',
                    'value_not_null' => true,
                    'default'=>'453',
                    'description' => 'Obaveza za poslodavca - doprinos'
                ],
                [
                    'name' => 'obligation_company_pio',
                    'display_name'=>'Obaveza za poslodavca - PIO',
                    'type'=>'textField',
                    'value_not_null' => true,
                    'default'=>'4530',
                    'description' => 'Obaveza za poslodavca - doprinos PIO'
                ],
                [
                    'name' => 'obligation_company_zdr',
                    'display_name'=>'Obaveza za poslodavca zdravstveno',
                    'type'=>'textField',
                    'value_not_null' => true,
                    'default'=>'4531',
                    'description' => 'Obaveza za poslodavca - doprinos zdravstveno'
                ],
                [
                    'name' => 'obligation_company_nez',
                    'display_name'=>'Obaveza za poslodavca',
                    'type'=>'textField',
                    'value_not_null' => true,
                    'default'=>'4532',
                    'description' => 'Obaveza za poslodavca - doprinos nezaposleni'
                ],
            ],
        ],
        [
            'name' => 'ios',
            'display_name' => Yii::t('AccountingModule.Configuration', 'IOS'),
            'type' => 'group',
            'type_params' => [
                [
                    'name' => 'accounts_for_ios_generation',
                    'display_name'=>Yii::t('AccountingModule.Configuration', 'AccountsForIOSGeneration'),
                    'type'=>'textField',
                    'value_not_null' => true,
                    'default'=>'435',
                    'description'=>'Konta za generisanje Izvoda otvorenih stavki za kompanije'
                ],
                [
                    'name' => 'ios_template',
                    'display_name'=>Yii::t('AccountingModule.Configuration', 'TemplateForIos'),
                    'type'=>'searchField',
                    'type_params' => [
                        'modelName'=>'Template',
                    ],
                    'value_not_null'=>false,
                    'description'=>'Template za Izvod otvorenih stavki'
                ],
            ],
        ],
        [
            'name' => 'profit_calcs',
            'display_name' => 'Obracun profita',
            'type' => 'group',
            'type_params' => [
                [
                    'name' => 'outcome_sum',
                    'display_name'=>'Konto za sumu troska',
                    'type'=>'textField',
                    'value_not_null' => true,
                    'default'=>'599',
//                    'description'=>'Konta za generisanje Izvoda otvorenih stavki za kompanije'
                ],
                [
                    'name' => 'income_sum',
                    'display_name'=>'Konto za sumu prihoda',
                    'type'=>'textField',
                    'value_not_null' => true,
                    'default'=>'699',
//                    'description'=>'Konta za generisanje Izvoda otvorenih stavki za kompanije'
                ],
                [
                    'name' => 'outcome_selector',
                    'display_name'=>'Selektor za trosak',
                    'type'=>'textField',
                    'value_not_null' => true,
                    'default'=>'5*',
//                    'description'=>'Konta za generisanje Izvoda otvorenih stavki za kompanije'
                ],
                [
                    'name' => 'income_selector',
                    'display_name'=>'Selektor za prihod',
                    'type'=>'textField',
                    'value_not_null' => true,
                    'default'=>'6*',
//                    'description'=>'Konta za generisanje Izvoda otvorenih stavki za kompanije'
                ],
                [
                    'name' => 'income_and_outcome',
                    'display_name' => Yii::t('AccountingModule.Configuration', 'ProfitCalcsIncomeAndOutcome'),
                    'type' => 'textField',
                    'value_not_null' => true,
                    'default' => '710',
                    'description' => Yii::t('AccountingModule.Configuration', 'ProfitCalcsIncomeAndOutcome'),
                ],
                [
                    'name' => 'transfer_total_result',
                    'display_name' => Yii::t('AccountingModule.Configuration', 'ProfitCalcsTransferTotalResult'),
                    'type' => 'textField',
                    'value_not_null' => true,
                    'default' => '712',
                    'description' => Yii::t('AccountingModule.Configuration', 'ProfitCalcsTransferTotalResult'),
                ],
                [
                    'name' => 'profit_or_loss',
                    'display_name' => Yii::t('AccountingModule.Configuration', 'ProfitCalcsProfitOrLoss'),
                    'type' => 'textField',
                    'value_not_null' => true,
                    'default' => '720',
                    'description' => Yii::t('AccountingModule.Configuration', 'ProfitCalcsProfitOrLoss'),
                ],
                [
                    'name' => 'profit',
                    'display_name' => Yii::t('AccountingModule.Configuration', 'ProfitCalcsProfit'),
                    'type' => 'textField',
                    'value_not_null' => true,
                    'default' => '720000',
                    'description' => Yii::t('AccountingModule.Configuration', 'ProfitCalcsProfit'),
                ],
                [
                    'name' => 'loss',
                    'display_name' => Yii::t('AccountingModule.Configuration', 'ProfitCalcsLoss'),
                    'type' => 'textField',
                    'value_not_null' => true,
                    'default' => '720100',
                    'description' => Yii::t('AccountingModule.Configuration', 'ProfitCalcsLoss'),
                ],
                [
                    'name' => 'tax_outcome_of_period',
                    'display_name' => Yii::t('AccountingModule.Configuration', 'ProfitCalcsTaxOutcomeOfPeriod'),
                    'type' => 'textField',
                    'value_not_null' => true,
                    'default' => '721',
                    'description' => Yii::t('AccountingModule.Configuration', 'ProfitCalcsTaxOutcomeOfPeriod'),
                ],
                [
                    'name' => 'delayed_tax_outcome_and_income_of_period',
                    'display_name' => Yii::t('AccountingModule.Configuration', 'ProfitCalcsDelayedTaxOutcomeAndIncomeOfPeriod'),
                    'type' => 'textField',
                    'value_not_null' => true,
                    'default' => '722',
                    'description' => Yii::t('AccountingModule.Configuration', 'ProfitCalcsDelayedTaxOutcomeAndIncomeOfPeriod'),
                ],
                [
                    'name' => 'transfer_profit_or_loss',
                    'display_name' => Yii::t('AccountingModule.Configuration', 'ProfitCalcsTransferProfitOrLoss'),
                    'type' => 'textField',
                    'value_not_null' => true,
                    'default' => '724',
                    'description' => Yii::t('AccountingModule.Configuration', 'ProfitCalcsTransferProfitOrLoss'),
                ],
                [
                    'name' => 'obligation_for_tax_from_result',
                    'display_name' => Yii::t('AccountingModule.Configuration', 'ProfitCalcsObligationForTaxFromResult'),
                    'type' => 'textField',
                    'value_not_null' => true,
                    'default' => '481',
                    'description' => Yii::t('AccountingModule.Configuration', 'ProfitCalcsObligationForTaxFromResult'),
                ],
                [
                    'name' => 'unallocated_profit_for_the_current_year',
                    'display_name' => Yii::t('AccountingModule.Configuration', 'ProfitCalcsUnallocatedProfitForTheCurrentYear'),
                    'type' => 'textField',
                    'value_not_null' => true,
                    'default' => '341',
                    'description' => Yii::t('AccountingModule.Configuration', 'ProfitCalcsUnallocatedProfitForTheCurrentYear'),
                ],
                [
                    'name' => 'loss_of_the_current_year',
                    'display_name' => Yii::t('AccountingModule.Configuration', 'ProfitCalcsLossOfTheCurrentYear'),
                    'type' => 'textField',
                    'value_not_null' => true,
                    'default' => '351',
                    'description' => Yii::t('AccountingModule.Configuration', 'ProfitCalcsLossOfTheCurrentYear'),
                ],
            ],
        ]
    ],
];