<?php

return array(
    'BankStatement'=>array(
        'modelBankStatementAddButton2',
    ),
    'Bill'=>array(
        'modelBillEdit',
        'modelBillDelete',
        'modelBillConfirm1',
        'modelBillConfirm2',
    ),
    'Payment' => array(
        'modelPaymentConfirm1',
        'modelPaymentConfirm2'
    ),
    'BankAccount' => array(
        'modelBankAccountFormOpen',
        'modelBankAccountOpenDialog',
        'modelBankAccountDelete'
    ),
    'Paycheck' => array(
        'modelPaycheckConfirm',
        'modelPaycheckRevert'
    ),
    'PaycheckPeriod' => array(
        'modelPaycheckPeriodConfirm',
        'modelPaycheckPeriodRevert'
    ),
    'UnifiedPaycheck' => array(
        'modelUnifiedPaycheckConfirm',
        'modelUnifiedPaycheckRevert'
    ),
    'Ios' => [
        'modelIosConfirm'
    ]
);