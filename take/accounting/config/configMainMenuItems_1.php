<?php

return array(
    'menuShowAccounts'=>array(
        'menuShowAccounts_OverviewReport',
        'menuShowAccounts_conf',
        'menuShowAccounts_conf_middleCurrencyRate',
        'menuShowAccounts_records',
        'menuShowAccounts_confirm1',
        'menuShowAccounts_CF',
        'menuShowAccounts_CashDesk',
        'menuShowAccounts_BillOfExchange',
        'menuShowAccounts_BankGuarantee',
        'menuShowAccounts_paychecks',
        'menuShowAccounts_FA',
        'menuShowAccounts_OldPaychecks',
        'menuShowAccounts_PPPPD',
        'menuShowAccounts_bookKeeping',
        'menuShowAccounts_vat',
        'menuShowAccounts_bankStatements'
    )
);