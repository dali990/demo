<?php

return array(
    'ForeignCurrencyAccount' => 'Foreign currency account',
    'Currency' => 'Currency',
    'BankPrefixMismatch' => 'Bank prefix mismatch({PREFIX})',
    'BankNumberInvalidFormat' => 'Bank number is in invalid format',
    'BankAccount' => 'Bank account',
    'BankAccounts' => 'Bank accounts'
);
