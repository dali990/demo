<?php

return [
    'Price' => 'Price',
    'price' => 'price',
    'ValuePerUnit' => 'Value per unit',
    'Calculate' => 'Calculate',
    'calculate' => 'calculate',
    'Sum' => 'Sum',
    'sum' => 'sum',
    'OtherRecords' => 'Other records',
];