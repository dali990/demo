<?php

return [
    'KprKirPdfInvalidName' => "Failed to create a pdf because the parameter 'name' is not passed.",
    'ManualPdf' => 'PDF(Manual)',
    'Pdf' => 'PDF',
    'Csv' => 'CSV',
    'UnknownExportType' => 'Unknown export type'
];

