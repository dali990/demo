<?php

return array(
    'BookingMaterialsType' => 'Booking materials storage',
    'IosDateNotSameAsYear' => 'The IOS generation date must be in the year for which the IOS generation is performed',
    'IosDateNotSameKontoYear' => 'The IOS generation date must be in the same year as the account year',
);
