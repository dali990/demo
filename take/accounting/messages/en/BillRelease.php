<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

return array(
    'NotCompatibleInvoice' => 'Bill and payment are not compatible by direction (payin/payout)',
    'NotCompatiblePartner' => 'Bill and payment are not compatible by parnter',
    'NotCompatibleRecurringBill' => 'Bill and payment are not compatible by recurring bill',
    'StatusPRETIME' => 'before time',
    'StatusONTIME' => 'on time',
    'StatusLATE' => 'late',
    'StatusNODUEDATE' => 'no due date',
    'StatusTODAY' => 'today',
    'StatusONDAY' => 'on day',
);