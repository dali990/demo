<?php

return [
    'AddBillAsFA' => 'Add bill as FA',
    'AddBillItemsAsFA' => 'Add bill items as FA',
    'ExportInventoryList' => 'Export inventory list',
    'ExportInventoryListOnDay' => 'Export inventory list on day',
    'ExportInventoryListOnDayPlaceholder' => 'Choose date',
    'ChooseDateForExportInventoryList' => 'Choose date for export inventory list',
    'ExportInventoryListOnDayPdfName' => 'Inventory list on day {date}',
    'InventoryList' => 'Inventory list',
    'QuantityOnEndOfDepricationYear' => 'Quantity for which depreciation was made',
    'PurchaseValueOnEndOfDepricationYear' => 'Current purchase value'
];