<?php

return [
    'EnterOrderNumberToFreeForKprKirManual' => 'Enter the number you want to release',
    'AreYouSureYouWantToCollapseOrderNumbers' => 'Are you sure you want to collapse order numbers?',
    'AddBankAccount' => 'Add a bank account',
    'YouHaveNotSelectedAnyBills' => 'You have not selected any bills',
    'PPPPDFileVersionCreated' => 'Version successfully created.',
    'BillPdfSuggestion' => 'Bill PDF suggestion',
    'YouHaveNotSelectedAnyCostType' => 'You have not selected any cost type.',
    'AccountingPartnersReport' => 'Accounting partners report',
    'AccountingPartnersReportMustChooseDate' => 'Must choose date!',
    'AccountingPartnersReportGenerate' => 'Generate',
    'AreYouSureYouWantToDeleteBillItems' => 'Are you sure you want to delete?',
    'InsertEuroSign' => 'Insert euro sign',
    'BillItemFixedAssets' => 'Fixed assets',
    'BillItemWarehouse' => 'Warehouse',
    'BillItemServices' => 'Services',
    'BillItemLock' => 'Lock',
    'BillItemUnlock' => 'Unlock',
    'TotalValueOfBillIsLocked' => 'The total value of the bill is locked',
    'TotalValueOfBillIsUnlocked' => 'The total value of the bill is unlocked'
];
