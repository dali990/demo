<?php

return [
    'CostLocation' => 'Cost location',
    'Bill' => 'Bill',
    'CostTypeEmpty' => 'You must fill cost type',
    'CostLocationEmpty' => 'You must fill cost location'
];