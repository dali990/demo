<?php

return [
    'ReleaseNumber' => 'Release order number',
    'CollapseNumbers' => 'Collapse order numbers',
    'Month' => 'Month',
    'OrderNumbersThatWasSkipped' => 'Order numbers that was skipped',
    'NoOrderNumbersForCollapse' => 'No order numbers for collapse'
];

