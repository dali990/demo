<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

return [
    'Cost type' => 'Cost type',
    'CostIncomeType' => 'Cost/Income type',
    'CostIncomeTypes' => 'Cost/Income type',
    'CostType' => 'Cost type',
    'CostTypes' => 'Cost types',
    'IncomeType' => 'Income type',
    'IncomeTypes' => 'Income types',
    'cost_or_income' => 'Cost/Income',
    'book_by_cost_location' => 'Book by cost location',
    'NotDefinedInYear' => 'witch are not defined in {year} year',
    'TransferAllFromPreviousYear' => 'Transfer all from previous year',
    'TransferAllSelected' => 'Transfer all selected',
    'AllCostTypesTransfered' => 'All cost/income types transfered from {prev_year} year to {curr_year} year.',
    'IgnoredCostTypes' => 'Automatic transfer is possible just for cost and income types that are defined in previous years, so transfer for these should be done one by one:'
];