<?php

return [
    'book_services_separately' => 'Book services in separate items',
    'book_services_separately_desc' => 'When there is two or more items in bill that should go to samo account, book it separatly',
    'DisplayDenominationTableInCashDeskLogReport' => 'Display the denomination table in the cash register',
    'DefaultCostLocation' => 'Default cost location',
    'DefaultCostCostType' => 'Default cost cost type',
    'DefaultIncomeCostType' => 'Default income cost type',
    'BillInAutomaticConfirmation1' => 'Automatic confirmation 1 for incoming bills',
    'BillInAutomaticFileSignatureUsers' => 'List of users who are automatically added to the signature of all incoming bills',
    'BillInFileSignatureWaitingForSubordinates' => 'For incoming bills, I always wait for the signature of the employees below me in the organizational scheme',
    'BillInAutomaticConfirmation2' => 'Automatic confirmation 2 for incoming bills',
    'AccountingStatisticalReportOwnershipDesignation' => 'Ownership designation for the needs of the accounting statistical report',
    'AccountingStatisticalReportOwnershipDesignationForSociety' => 'For society',
    'AccountingStatisticalReportOwnershipDesignationForPrivate' => 'For private',
    'AccountingStatisticalReportOwnershipDesignationForCooperative' => 'For cooperative',
    'AccountingStatisticalReportOwnershipDesignationForMixed' => 'For mixed',
    'AccountingStatisticalReportOwnershipDesignationForState' => 'For state'
];