<?php

return [
    'UnknownExportType' => 'Unknown export type',
    'AccountingPartnersReport' => 'Accounting partners report',
    'AccountingPartnersReportPerDay' => 'Accounting partners report per day {date}'
];

