<?php

return array(
    'ChosenConfigurationTypeProblem' => 'Problem with type of chosen configuration!',
    'AddFlyingPayments' => 'Add flying payments',
    'PartnerAutomaticallySetedByCallToNumber' => 'Partner <b>"{company_name}"</ b> is automatically set based on the call to number <br />',
    'PartnerAutomaticallySetedByNBS' => 'Partner <b>"{company_name}"</ b> is automatically set based on the data from National Bank of Serbia <br />',
    'AddPartnerFromNBS' => 'Add partner from NBS'
);

