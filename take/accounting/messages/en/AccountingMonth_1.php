<?php

return [
    'NotValidNumericNumber' => 'Invalid number. The number can have a maximum of {precision} digits, and behind the decimal separator maximum {scale}.'
];

