<?php

return [
    'BillIn' => 'Bill in',
    'BillOut' => 'Bill out',
    'AdvanceBill' => 'Advance bill',
    'ReliefBill' => 'Relief bill',
    'UnreliefBill' => 'Unrelief bill',
    'PreBill' => 'Pre bill'
];