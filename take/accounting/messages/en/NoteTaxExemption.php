<?php

return [
    'NoteOfTaxExemption' => 'Note of the tax exemption',
    'NotesOfTaxExemption' => 'Notes of the tax exemption',
    'NameOfNote' => 'Name of the note',
    'TextOfNote' => 'Text of the note'
];