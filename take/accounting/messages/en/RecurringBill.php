<?php

return [
    'RecurringBill' => 'Recurring bill',
    'RecurringBills' => 'Recurring bills',
    'Name' => 'Name',
    'CallForNumber' => 'Call for number',
    'Partner' => 'Partner',
    'Description' => 'Description',
    'UniqPartnerAndCallForNumber' => 'Partner and call for number must be unique'
];