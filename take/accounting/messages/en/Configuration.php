<?php

return [
    'AccountOrderOrderOnlyNumbers' => 'Account order order only numbers',
    'AccountOrderOrderOnlyNumbersDesc' => 'Account order order should be only numbers or not',
    'Templates' => 'Templates',
    'BillTemplate' => 'Bill template',
    'BillTemplateAdditionalInfo' => 'Bill additional text',
    'HasBillResponsiblePerson' => 'Has bill responsible person?',
    'HasBillResponsiblePersonDesc' => 'If set, then the person is taken from the following parameter, or if this parameter is not assigned, then the logged user will be taken',
    'BillResponsiblePerson' => 'Bill responsible person',
    'BillBankAccounts' => 'Bill bank accounts',
    'BillShowPhoneInHeader' => 'Show phone in header',
    'ProfitCalcsIncomeAndOutcome' => 'Income and outcome',
    'ProfitCalcsTransferTotalResult' => 'Transfer of total result',
    'ProfitCalcsProfitOrLoss' => 'Profit or loss',
    'ProfitCalcsProfit' => 'Profit',
    'ProfitCalcsLoss' => 'Loss',
    'ProfitCalcsTaxOutcomeOfPeriod' => 'Tax outcome of period',
    'ProfitCalcsDelayedTaxOutcomeAndIncomeOfPeriod' => 'Delayed tax outcome and income of period',
    'ProfitCalcsTransferProfitOrLoss' => 'Transfer profit or loss',
    'ProfitCalcsObligationForTaxFromResult' => 'Obligation for tax from result',
    'ProfitCalcsUnallocatedProfitForTheCurrentYear' => 'Unallocated profit for the current year',
    'ProfitCalcsLossOfTheCurrentYear' => 'Loss of the current year',
    'BillTrafficLocation' => 'Traffic location',
    'ShowFilingNumber' => 'Show filing number',
    'BillIssueLocation' => 'Bill issue location'
];