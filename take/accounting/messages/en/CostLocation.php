<?php

return [
    'ThemeTabFinance' => 'Finance',
    'ThemeTabFinanceOld' => 'Old overview',
    'ThemeTabFinanceIncome' => 'Income',
    'ThemeTabFinanceCost' => 'Cost',
    'Partners' => 'Partners',
    'Partner' => 'Partner',
    'Amount' => 'Amount',
    'ForPayment' => 'For payment',
    'Advance' => 'Advance',
    'ForPayingWithoutMaterialAndVat' => 'For paying<br />(without material)',
    'DirectCostSpecification' => 'Direct cost specification',
    'Total' => 'Total',
    'Unpaid' => 'Unpaid',
    'RemainingAdvances' => 'Remaining advances',
    'Services' => 'Services',
    'Material' => 'Material',
    'Hours' => 'Hours'
];