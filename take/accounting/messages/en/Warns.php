<?php

return [
    'DuplicatedBankAccounts' => 'Duplicated bank accounts',
    'BillDocumentsWithNoSignature' => 'Bill documents with no signature'
];