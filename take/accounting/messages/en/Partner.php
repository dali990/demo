<?php

return [
    'Accounting' => 'Accounting',
    'UnreleasedPayments' => 'Unreleased payments',
    'AllPayments' => 'All payments',
];