<?php

return [
    'UnconfirmedBookingPrediction' => 'You have not verified differences in the booking predictions',
    'OrderOnlyNumbers' => 'Serial number must be a number',
    'AccountOrder' => 'Account order',
    'UnconfirmedCashDeskLog' => 'You can not book account order because you have an unconfirmed cash desk log',
    'CanNotConfirmAccountOrderBecauseCanNotConfirm2ForBillIn' => 'You can not book the account order because for incoming bill {bill_in} can not made automatic confirmation 2 for next reasons: {validate_error_html}'
];