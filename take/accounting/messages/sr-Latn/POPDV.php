<?php

return [
    'BookedInMonth' => 'Proknjižen u mesecu {month}',
    'NotBooked' => 'Nije proknjižen'
];