<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

return [
    'Balance state' => 'Bilans stanja',
    'Balance success' => 'Bilans uspeha',
    'Statistical report' => 'Statistički izveštaj',
    'Disagreement check' => 'Proveri da li ima neslaganja',
    'Delete empty accounts' => 'Obrisi prazna konta',
    'Translate from previous' => 'Prenesi iz prethodne',
    'Input date range' => 'Unesite opseg datuma',
    'Apply date range' => 'Primeni datum opsega',
    'Show all' => 'Prikaži sve',
    'Hide zeros' => 'Sakrij nule',
    'ChangeTransactionAccount' => 'Prekontiranje'
];