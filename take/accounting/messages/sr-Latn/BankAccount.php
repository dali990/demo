<?php

return array(
    'ForeignCurrencyAccount' => 'Devizni racun',
    'Currency' => 'Valuta',
    'BankPrefixMismatch' => 'Prefiks racuna ne odgovara zadatoj banci({PREFIX})',
    'BankNumberInvalidFormat' => 'Broj racuna je nepravilnog formata',
    'BankAccount' => 'Žiro račun',
    'BankAccounts' => 'Žiro računi',
    'BankNumberAlreadyExists' => 'Broj računa vec postoji'
);
