<?php

return [
    'PPPPDs' => 'PPP-PD',
    'ClientIdentificator' => 'Identifikator',
    'SIMA_GENERATE' => 'Generisan od strane IS-SIMA',
    'USER_UPLOAD' => 'Uvezen od strane korisnika',
    'USER_MANUAL' => 'Kreiran ručno od strane korisnika',
    'CreationTypeInvalidValue' => 'Nevalidan tip kreiranja: "{creation_type}"',
    'ClientUploadedFileEmpty' => 'Morate izabrati XML dokument',
    'CreateTime' => 'Vreme kreiranja',
    'Note' => 'Beleška',
    'CreationType' => 'Nacin kreiranja',
    'ComparePPPPDs' => 'Uporedjivanje PPP-PD-ova',
    'PerformComparePPPPDs' => 'Uporedi izabrane PPP-PD-ove',
    'ChoosePPPDToCompareWithThis' => 'Izaberi',
    'CompareWithOtherPPPD' => 'Uporedite sa drugim PPP-PD',
    'ChoosePPPPDForCompare' => 'Izaberite',
    'ClientUploadedFile' => 'XML dokument',
    'PPPPDItems' => 'Stavke',
    'Dirty' => 'Dirty',
    'CreatePPPPDNewFileVersion' => 'Kreiraj novu verziju',
    'UserUploadedXMLParseProblem' => 'Doslo je do problema u obradi okacenog XML fajla.</br>{exception_message}',
    'InvalidXML' => 'XML nije validnog oblika',
    'PaycheckPeriodIdEmpty' => 'Nije postavljena isplata'
];
