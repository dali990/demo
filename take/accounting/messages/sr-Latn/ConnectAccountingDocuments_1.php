<?php

return [
    'NoConnectionBillToReliefBill' => 'Vrednosti računa i knjižnog odobrenja nemaju iste PDV stope i ne mogu biti likvidirani.',
    'NoConnectionBillToAdvanceBill' => 'Vrednosti računa i avansnog računa nemaju iste PDV stope i ne mogu biti likvidirani.',
    'NoConnectionAdvanceBillToReliefInternal' => 'Vrednosti avansnog računa i knjižnog odobrenja nemaju iste PDV stope i ne mogu biti likvidirani.',
    'CheckBaseAmountAndVatAmount' => 'Proverite izračunate iznose osnovice i PDV'
];