<?php

return [
    'NoteOfTaxExemption' => 'Napomena o poreskom oslobođenju',
    'NotesOfTaxExemption' => 'Napomene o poreskom oslobođenju',
    'NameOfNote' => 'Naziv napomene',
    'TextOfNote' => 'Tekst napomene'
];