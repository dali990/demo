<?php

return array(
    'Currency' => 'Valuta',
    'MiddleCurrencyDateTaken' => 'Postoji srednji kurs za valutu na dan',
    'LeaveEmptyValue' => 'Ostavite prazno kako bi se dovukla vrednost iz NBS',
    'ValueEmptyNBSConnectionNotInstalled' => 'Vrednost ne moze da bude prazna kada NBS veza nije instalirana',
    'EmptyValueFromNBSConnection' => 'Neuspesno dobijanje podataka od NBS',
    'MiddleCurrencyRates' => 'Srednji kursevi',
    'MiddleCurrencyRate' => 'Srednji kurs',
);