<?php

return [
    'ThereWasError' => 'Dogodio se problem zbog kojeg nije moguce generisati fajl za izvoz u E-banking',
    'PartnerNotHaveMainAddress' => 'Partneru "{partner}" nije postavljena glavna adresa'
];
