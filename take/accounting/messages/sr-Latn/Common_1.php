<?php

return [
    'Price' => 'Cena',
    'price' => 'cena',
    'ValuePerUnit' => 'Jedinična cena',
    'Calculate' => 'Izračunaj',
    'calculate' => 'izračunaj',
    'Sum' => 'Ukupno',
    'sum' => 'ukupno',
    'Quantity' => 'Količina',
    'quantity' => 'količina',
    'Bookkeeping' => 'Knjigovodstvo',
    'bookkeeping' => 'knjigovodstvo',
    'OtherRecords' => 'Ostale evidencije',
    'Codebooks' => 'Šifarnici',
];