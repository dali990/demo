<?php

return array(
    'SharedAccounts' => 'Deljeni racuni',
    'IsInUnified' => 'Objedinjeno placanje',
    'NotFoundByCodeException' => 'Nije pronađena banka sa kodom {code}',
    'BankAlreadyExists' => 'Banka "{bank_name}" već postoji!'
);