<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

return array(
    'NotCompatibleInvoice' => 'Račun i plaćanje nisu kompatibilni po smeru (uplata/isplata)',
    'NotCompatiblePartner' => 'Račun i plaćanje nisu kompatibilni po partneru',
    'NotCompatibleRecurringBill' => 'Račun i plaćanje nisu kompatibilni po ponavljajucem računu',
    'Released amounts' => 'Rasknjižena vrednost',
    'Released amounts in currency' => 'Rasknjižena vrednost u valuti',
    'StatusPRETIME' => 'pre vremena',
    'StatusONTIME' => 'na vreme',
    'StatusLATE' => 'kasni',
    'StatusNODUEDATE' => 'nema valute',
    'StatusTODAY' => 'danas',
    'StatusONDAY' => 'na dan',
    'Relief bill and advance bill are not compatible' => 'Knjižno odobrenje i avansni račun nisu kompatibilni'
);