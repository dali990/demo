<?php

return [
    'RecurringBill' => 'Ponavljajući račun',
    'RecurringBills' => 'Ponavljajući računi',
    'Name' => 'Naziv',
    'CallForNumber' => 'Poziv na broj',
    'Partner' => 'Partner',
    'Description' => 'Opis',
    'UniqPartnerAndCallForNumber' => 'Partner i poziv na broj moraju biti jedinstveni'
];