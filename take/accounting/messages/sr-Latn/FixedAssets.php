<?php

return [
    'AddBillAsFA' => 'Dodaj račun kao OS',
    'AddBillItemsAsFA' => 'Dodaj stavke računa kao OS',
    'Depreciation' => 'Amortizacija',
    'FixedAssetsDepreciation' => 'Amortizacija osnovnih sredstava',
    'CantDeleteBookedFA' => 'Ne možete obrisati proknjiženo osnovno sredstvo',
    'AcquisitionDocumentWithBillItemCheck' => 'OS je napravljeno iz dokumenta {AcquisitionDocumentDisplay} i samo taj dokument može biti dokument o pribavljanju',
    'ExportInventoryList' => 'Izvezi popisnu listu',
    'ExportInventoryListOnDay' => 'Izvezi popisnu listu na dan',
    'ExportInventoryListOnDayPlaceholder' => 'Izaberite datum',
    'ChooseDateForExportInventoryList' => 'Izaberite datum za koji izvozite popisnu listu',
    'ExportInventoryListOnDayPdfName' => 'Popisna lista na dan {date}',
    'InventoryList' => 'Popisna lista',
    'QuantityOnEndOfDepricationYear' => 'Količina za koju je rađena amortizacija',
    'PurchaseValueOnEndOfDepricationYear' => 'Trenutna nabavna vrednost'
];