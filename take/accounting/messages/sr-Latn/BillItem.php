<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

return array(
    'Bill item' => 'Stavka računa',
    'VAT value' => 'PDV',
    'VAT rate' => 'PDV %',
    'VAT value internal' => 'PDV interno',
    'VAT rate internal' => 'PDV % interno',
    'Discount item' => 'Stavka je popust',
    'CanNotChangeQuantityWhenFromWarehouse' => 'Količina se menja kroz robno knjig.',
    'CanNotChangeQuantityWhenLessThenZero' => 'Količina ne može biti manja od nule',
    'CanNotDeleteWhenFromWarehouse' => 'Stavka se briše kroz robno knjig.',
    'ValuePerUnitAndValueConflict' => 'Jedinična cena i ukupna cena nisu promenjene uskladjeno: {value}',
    'NoVatType' => 'POPDV',
    'CustomsBillItem' => 'Stavka car. računa',
    'ConnectedCustomsBill' => 'Povezani carinski račun',
    'ChooseBillItem' => 'Izaberite stavku računa',
    'CustomsBillItemTaken' => 'Stavka carinskog računa je već iskorišćena',
    'ChangeUnreleaseAmount' => 'Ovom promenom dug računa {bill_name} ide u minus. Proverite da li su zaključane ukupne vrednosti računa.',
    'BillItemNegative' => 'Ovom promenom jedna od stavki sa računa {bill_name} ide u minus.',
    'With100DiscountValueMustBeZero' => 'Vrednost mora biti 0, zbog popusta 100%.',
    'TransferToWarehouse' => 'Prenesite u magacin',
    'QuantityOverMaxLimit' => 'Maksimalna dozvoljena količina je {max_limit}',
    'FixedAssets' => 'Osnovna sredstva',
    'Warehouse' => 'Magacin',
    'Services' => 'Usluge',
    'ViewAndAddReceiving' => 'Pregled i dodavanje prijemnica',
    'Receiving' => 'Prijemnice',
    'BillItems' => 'Stavke računa',
    'CarryOver' => 'Prenesite',
    'CarryOverInFixedAssets' => 'Prenesite u osnovna sredstva',
    'CarryOverInWarehouse' => 'Prenesite u magacin',
    'AddNewItem' => 'Dodajte novu stavku',
    'AddItem' => 'Dodajte stavku',
    'SerialNumber' => 'Redni broj',
    'AbbreviationSerialNumber' => 'R.Br.',
    'Name' => 'Naziv',
    'Amount' => 'Količina',
    'PricePerUnit' => 'Jedinična cena',
    'AbbreviationPricePerUnit' => 'Jed. cena',
    'Discount' => 'Popust',
    'Value' => 'Vrednost',
    'InTotal' => 'Ukupno',
    'Sum' => 'Suma',
    'DeleteItem' => 'Obrišite stavku',
    'BandTogetherPlaceOfCost' => 'Povežite mesto troška',
    'InternalVAT' => 'Interni PDV',
    'UnprocessedItems' => 'Neobrađene stavke',
    'Convert' => 'Konvertujte',
    'InMaterialTraffic' => 'U promet materijala',
    'InFixedAssets' => 'U osnovna sredstva',
    'BillWarehouseTransferIn' => 'Račun-prijemnica'
);