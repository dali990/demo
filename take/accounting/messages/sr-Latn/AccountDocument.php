<?php

return [
    'RemoveAllItems' => 'Da biste dokument uklonili sa naloga za knjizenje izbrisite njegove knjigovodstvene stavove.',
    'AlreadyOnAccountOrder' => 'Dokument "{document_name}" je već na nalogu za knjiženje.'
];