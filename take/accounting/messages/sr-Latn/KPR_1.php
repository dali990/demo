<?php

return [
    'KPR_1' => '1-Redni broj',
    'KPR_2' => '2-Datum knjiženja',
//    'KPR_3' => 'Redni broj',
    'KPR_4' => '4-Broj računa',
    'KPR_5' => '5-Datum računa',
    'KPR_6' => '6-Partner',
    'KPR_7' => '7-Partner-PIB',
    'KPR_8' => '8-Ukupno sa PDV',
    'KPR_9' => '9-Osnovica za odbitni PDV',
    'KPR_9a' => '9a-Osnovica za PDV koji se ne odbija',
    'KPR_10' => '10-Oslobođeno po tač.18',
    'KPR_11' => '11-Nisu obveznici tač.15',
    'KPR_12' => '12-Uvoz bez PDV tač.22',
    'KPR_13' => '13-Ukupan PDV',
    'KPR_14' => '14-PDV koji se odbija',
    'KPR_14b'=> '14b-PDV koji se odbija - proknjiženo',
    'KPR_15' => '15-PDV koji se ne odbija',
    'KPR_16' => '16-Uvoz osnovica',
    'KPR_17' => '17-Uvoz PDV tač.23',
    'KPR_18' => '18-Poljo osnovica',
    'KPR_19' => '19-5% nakande pod tač.24',
    'ReleaseNumber' => 'Oslobodi broj',
    'CollapseNumbers' => 'Skupi stavove'
];
