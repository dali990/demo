<?php

return [
    'Orders' => 'Nalozi',
    'Day reports' => 'Dnevni izvestaji',
    'ConfirmAllOrders' => 'Molimo vas potvrdite sve naloge blagajni na dan {date}'
];