<?php

return array(
    'ChosenConfigurationTypeProblem' => 'Problem sa tipom izabrane konfiguracije!',
    'XmlLoadFailed' => 'Nije uspelo učitavanje XML fajla',
    'XmlUploadNotSuccessful' => 'Nije uspeo upload XML fajla',
    'CouldNotCreateBankStatementInvalidNumber' => 'Neuspesano kreiran bankarski izvod sa podacima </br>'
            . ' broj računa: {bank_account_number} </br>'
            . ' datum: {date} </br>'
            . ' iznos: {amount} </br>'
            . ' </br> Poruka: </br> {db_err_message}',
    'CouldNotCreatePaymentInvalidPartnerBankAccountNumber' => 'Neuspešno kreirano plaćanje </br>'
            . ' broj računa: {bank_account_number} </br>'
            . ' datum plaćanja: {payment_date} </br>'
            . ' iznos: {amount} </br>'
            . ' </br> Poruka: </br> {db_err_message}',
    'NumberOfEmptyStatementsThatWereSetBankForPartner' => 'Postoje računi koji nisu imali broj računa partnera, '
//        .'najverovatnije predstavljaju proviziju banke '
        .'pa im je za partnera postavljena banka izvoda, '
        .'a takvih je bilo: {number_of_empty_account_numbers}',
    'AddFlyingPayments' => 'Dodaj leteća plaćanja',
    'PartnerAutomaticallySetedByCallToNumber' => 'Partner <b>"{company_name}"</b> je automatski postavljen na osnovu poziva na broj <br />',
    'PartnerAutomaticallySetedByNBS' => 'Partner <b>"{company_name}"</b> je automatski postavljen na osnovu podataka iz Narodne Banke Srbije <br />',
    'AddPartnerFromNBS' => 'Dodaj partnera iz NBS',
    'NoBankAccontForSystemCompanyWithNumber' => 'Sistemska kompanija nema ziro racun sa brojem {bank_account_number}'
);

