<?php

return [
    'KprKirPdfInvalidName' => "Nije uspelo kreiranje pdf-a jer nije prosleđen parametar 'name'.",
    'ManualPdf' => 'PDF(Ručni)',
    'Pdf' => 'PDF',
    'Csv' => 'CSV',
    'UnknownExportType' => 'Nepoznat tip izveštaja'
];

