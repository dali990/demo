<?php

return array(
    'BookingMaterialsType' => 'Knjiženje magacina sirovina',
    'IosDateNotSameAsYear' => 'Datum za generisanje IOS-a mora biti u godini za koju se vrši generisanje IOS-a',
    'IosDateNotSameKontoYear' => 'Datum za generisanje IOS-a ne odgovara godini konta',
);
