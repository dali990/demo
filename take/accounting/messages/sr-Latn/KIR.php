<?php

return [
    'KIR_1' => '1-Redni broj',
    'KIR_2' => '2-Datum knjiženja',
    'KIR_3' => '3-Broj računa',
    'KIR_4' => '4-Datum računa',
    'KIR_5' => '5-Partner',
    'KIR_6' => '6-Partner-PIB',
    'KIR_7' => '7-Ukupno sa PDV',
    'KIR_8' => '8-Oslobodjen sa pravom na odbitak',
    'KIR_9' => '9-Oslobodjen bez prava na odbitak',
//    'KIR_10' => '1-Inostranstvo za koji bi postojalo pravo',
//    'KIR_11' => '1-Redni broj',
    'KIR_12' => '12-Osnovica 20%',
    'KIR_13' => '13-PDV 20%',
    'KIR_14' => '14-Osnovica 10%',
    'KIR_15' => '15-PDV 10%',
    'KIR_16' => '16-Ukupna osnovica',
    'KIR_17' => '17-Ukupna osnovica sa pravom',
    'KIR_18' => '18-Prihod',
    'ReleaseNumber' => 'Oslobodi broj',
    'CollapseNumbers' => 'Skupi stavove',
    'Month' => 'Mesec',
    'OrderNumbersThatWasSkipped' => 'Redni brojevi koji su bili preskočeni',
    'NoOrderNumbersForCollapse' => 'Nema stavova za skupljanje'
];
