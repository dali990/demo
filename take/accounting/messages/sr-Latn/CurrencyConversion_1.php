<?php

return array(
    'PaymentIn' => 'Ulazno placanje',
    'PaymentOut' => 'Izlazno placanje',
    'PaymentInAccount' => 'Ulazni racun',
    'PaymentOutAccount' => 'Izlazni racun',
    'PaymentOutAmount' => 'Izlazna vrednost',
    'PaymentInAmount' => 'Ulazna vrednost',
    'PaymentInCurrency' => 'Ulazna valuta',
    'PaymentOutCurrency' => 'Izlazna valuta',
    'ExchangeRate' => 'Kurs',
    'MiddleCurrencyRate' => 'Srednji kurs',
    'CountryCurrencyConfigNotSet' => 'Nije postavljen kurs drzave u konfiguracijama',
    'NoMiddleCurrencyRateForDate' => 'Nije postavljen srednji kurs na dan {date} za valutu {currency}'
);
