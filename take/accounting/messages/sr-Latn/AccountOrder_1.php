<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

return array(
    'AccountOrder' => 'Nalog za knjiženje',
    'NotAO' => 'Dokument nije nalog za knjiženje',
    'Booked' => 'Proknjiženo',
    'NotBooked' => 'Nije proknjiženo',
    'UnconfirmedBookingPrediction' => 'Niste potvrdili razlike u predloženim knjiženjima.',
    'UnconfirmedNoTransactions' => 'Nalog ne može biti potvrđen jer nema nijedan knjigovodstveni stav.',
    'UnconfirmedSaldoNotZero' => 'Saldo nije 0.',
    'OrderOnlyNumbers' => 'Redni broj mora biti broj',
    'AccountOrdersOfConnectedPayments' => 'Nalozi za knjiženje na povezanim uplatama',
    'UnconfirmedCashDeskLog' => 'Ne možete da proknjižite nalog jer imate nepotvrđen dnevnik blagajne',
    'CanNotConfirmAccountOrderBecauseCanNotConfirm2ForBillIn' => 'Ne možete da proknjižite nalog jer za ulazni račun {bill_in} ne može da se izvrši automatska potvrda 2 iz sledećih razloga: <br /> {validate_error_html}',
    'ProblemInFetchingFileByPathForZip' => 'Ne može biti kreiran zip jer jedan od fajlova nije dostupan:',
    'CannotDeleteConfirmedAccountTransaction' => 'Transakcije u potvrđenom nalogu ne mogu biti obrisane.'
);