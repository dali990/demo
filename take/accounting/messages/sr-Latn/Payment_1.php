<?php

return array(
    'PartnerDoesNotHaveAddress' => 'Partneru {partner} nije postavljena adresa!',
    'AccountDoesNotBelongToPartner' => 'Racun ne pripada partneru',
    'PartnerForeignCurrencyAccount' => 'Partnerov racun devizni',
    'BankStatementAccount' => 'Nas racun',
    'BankStatementAccountForeignCurrency' => 'Nas racun devizni',
    'PaymentDoNotHaveBankAccount' => 'Placanje "{payment}" nema postavljen bankovni racun',
    'ReccuringBillPartnerDontmatch' => 'Ne poklapaju se partner za poziv na broj i u uplati',
    'CantChangeHaveReleases' => 'Ne možete promeniti ukoliko postoji bilo koji vid likvidacije',
    'ReleseDeleted' => 'Skinuta je veza ka računima:    {bills_list}',
);