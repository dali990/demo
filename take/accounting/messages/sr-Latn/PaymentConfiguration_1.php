<?php

return array(
    'Name' => 'Naziv',
    'Type' => 'Tip',
    'AccountNumberCol' => 'Broj racuna(br. kolone)',
    'AccountOwnerCol' => 'Vlasnik racuna(br. kolone)',
    'DateCol' => 'Datum(br. kolone)',
    'AmountCol' => 'Iznos(br. kolone)',
    'Amount2Col' => 'Iznos2/credit(br.kolone)',
    'Description' => 'Napomena',
    'PaymentCodeCol' => 'Sifra placanja(br. kolone)',
    'CallForNumberDebitCol' => 'Poziv na broj odobrenja(br. kolone)',
    'CallForNumberCreditCol' => 'Poziv na broj zaduzenja(br. kolone)',
    'PathXML' => 'Putanja u XML-u',
    'Name' => 'Naziv',
    'BankTransactionId' => 'Transakcioni id',
    'PaymentConfiguration' => 'Konfiguracije bankarskog izvoda'
);