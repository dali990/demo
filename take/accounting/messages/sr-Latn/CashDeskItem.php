<?php

return [
    'CostLocation' => 'Mesto troška',
    'Bill' => 'Račun',
    'CostTypeEmpty' => 'Morate popuniti vrstu troška',
    'CostLocationEmpty' => 'Morate popuniti mesto troška'
];