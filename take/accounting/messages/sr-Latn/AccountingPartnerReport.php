<?php

return [
    'UnknownExportType' => 'Nepoznat tip izveštaja',
    'AccountingPartnersReport' => 'Pregled dugovanja po partnerima',
    'AccountingPartnersReportPerDay' => 'Pregled dugovanja po partnerima na dan {date}'
];

