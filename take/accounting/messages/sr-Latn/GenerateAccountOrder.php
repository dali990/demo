<?php

return [
    'NotForAutoAccountOrder' => 'Dokument "{document}" nije predviđen za automatsku dodelu naloga za knjiženje',
    'NotForAutoAccountOrderConfig' => 'Za tip dokumenta "{document_type}" nije podešen brojač',
];