<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

return [
    'Cost type' => 'Vrsta troška',
    'CostIncomeType' => 'Vrsta troška/prihoda',
    'CostIncomeTypes' => 'Vrste troška/prihoda',
    'CostType' => 'Vrsta troška',
    'CostTypes' => 'Vrste troška',
    'IncomeType' => 'Vrsta prihoda',
    'IncomeTypes' => 'Vrste prihoda',
    'cost_or_income' => 'Trošak/Prihod',
    'book_by_cost_location' => 'Knjiži po mestu troška',
    'NotDefinedInYear' => 'koje nisu definisane u {year} godini',
    'TransferAllFromPreviousYear' => 'Prenesi sve iz prethodne godine',
    'TransferAllSelected' => 'Prenesi sve selektovane',
    'AllCostTypesTransfered' => 'Sve vrste troškova/prihoda iz {prev_year} godine su prebačene u {curr_year} godinu.',
    'IgnoredCostTypes' => 'Automatski prenos je moguć samo za vrste troška i prihoda koje su definisane u prethodnim godinama, pa zato prenos sledećih vrsta morate odraditi jednu po jednu:'
];