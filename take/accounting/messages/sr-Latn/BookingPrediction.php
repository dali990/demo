<?php

return [
    'PreviousYearDoesntExists' => 'Nije unesena prethodna finansijska godina. Unesite je ili popunite nalog ručno',
    'NoFinalBookingForPreviousYear' => 'Niste uradili zavrsno knjizenje za prethodnu godinu' 
    
];

