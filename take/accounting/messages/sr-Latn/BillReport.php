<?php

return [
    'BillIn' => 'Ulazni račun',
    'BillOut' => 'Izlazni račun',
    'AdvanceBill' => 'Avansni račun',
    'ReliefBill' => 'Knjižno odobrenje',
    'UnreliefBill' => 'Knjižno zaduženje',
    'PreBill' => 'Predračun'
];