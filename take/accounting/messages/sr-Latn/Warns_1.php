<?php

return [
    'DuplicatedBankAccounts' => 'Duplirani žiro računi',
    'BillDocumentsWithNoSignature' => 'Računi bez zadatog potpisa'
];