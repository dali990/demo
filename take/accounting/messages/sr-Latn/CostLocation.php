<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

return [
    'Cost location' => 'Mesto troška',
    'CostLocation' => 'Mesto troška',
    'CostLocations' => 'Mesta troška',
    'For analytics' => 'Za analitiku',
    'ParentID' => 'Sastavni deo od',
    'ThemeTabFinance' => 'Finansije',
    'ThemeTabFinanceOld' => 'Stari pregled',
    'ThemeTabFinanceIncome' => 'Prihod',
    'ThemeTabFinanceCost' => 'Trošak',
    'Partners' => 'Podizvođači',
    'Partner' => 'Podizvođač',
    'Amount' => 'Iznos',
    'ForPayment' => 'Za naplatu',
    'Advance' => 'Avans',
    'ForPayingWithoutMaterialAndVat' => 'Za isplatu<br />(bez materijala i PDV)',
    'DirectCostSpecification' => 'Specifikacija direktnog troška',
    'Total' => 'Ukupno',
    'Unpaid' => 'Neplaćeno',
    'RemainingAdvances' => 'Preostao avans',
    'Services' => 'Usluge',
    'Material' => 'Materijal',
    'Hours' => 'Sati',
    'Salary' => 'Plata',
    'CashDesk' => 'Blagajna',
    'VirtualParents' => 'Virtuelni roditelji'
];