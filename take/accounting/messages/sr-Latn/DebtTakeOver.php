<?php

return [
    'DebtTakeOver' => 'Preuzimanje duga',
    'DebtTakeOvers' => 'Preuzimanja dugova',
    'Creditor' => 'Poverilac',
    'Debtor' => 'Dužnik',
    'TakesOver' => 'Preuzima dug',
    'DebtValue' => 'Vrednost duga',
    'TakeOverValue' => 'Vrednost preuzimanja',
    'UniqPartners' => 'Svi partneri moraju biti jedinstveni',
    'OneLocalPartner' => 'Barem jedan partner mora biti "{local_partner_name}"',
    'debtValuesCompare' => 'Ne sme biti veća od vrednosti duga',
    'BillDebtTakeOvers' => 'Rasknjižavanja računa ugovorima',
    'BillDebtTakeOver' => 'Rasknjižavanja računa ugovorom',
    'PaymentDebtTakeOvers' => 'Rasknjižavanja plaćanja ugovorima',
    'PaymentDebtTakeOver' => 'Rasknjižavanje plaćanja ugovorom',
    'SignDate' => 'Datum potpisivanja'
];