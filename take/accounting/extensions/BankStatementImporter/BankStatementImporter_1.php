<?php

class BankStatementImporter
{
    private $_worker = null;
    
    public function __construct(PaymentConfiguration $config, $file_path)
    {
        if($config->type === PaymentConfiguration::$TYPE_CSV_FILE) //CSV
        {
//            $this->createBankStatementsCSV($real_path, $config);
            $this->_worker = new BankStatementImporterHalcomCSV($config, $file_path);
        }
//        else if($config->type==1) //XML
//        {
//            $this->createBankStatementsXML($real_path, $config);
//        }
//        else if($config->type == 2) //ErsteWeb
        else if($config->type == PaymentConfiguration::$TYPE_ERSTE_WEB) //ErsteWeb
        {
            $this->_worker = new BankStatementImporterErsteWeb($config, $file_path);
        }
        else if($config->type == PaymentConfiguration::$TYPE_RAIFFEISEN)
        {
//            $this->createBankStatementsRaiffeisenCSV($real_path, $config);
//            $this->_worker = new BankStatementImporterRaiffeisenCSV($config, $file_path);
            $this->_worker = new BankStatementImporterRaiffeisenXML($config, $file_path);
        }
        else if($config->type == PaymentConfiguration::$TYPE_HALK_BANK)
        {
            $this->_worker = new BankStatementImporterHalkCSV($config, $file_path);
        }
        else if($config->type == PaymentConfiguration::$TYPE_RAIFFEISEN_DEVIZNI)
        {
            $this->_worker = new BankStatementImporterRaiffeisenDevizniXML($config, $file_path);
        }
        else if($config->type == PaymentConfiguration::$TYPE_SOGE_XML)
        {
            $this->_worker = new BankStatementImporterSOGEXML($config, $file_path);
        }
        else if($config->type == PaymentConfiguration::$TYPE_OTP_DEVIZNI_XML)
        {
            $this->_worker = new BankStatementImporterOTPDevizniXML($config, $file_path);
        }
        else
        {
            throw new SIMAWarnException(Yii::t('AccountingModule.BankStatement', 'ChosenConfigurationTypeProblem'));
        }
    }
    
    public function run()
    {
        $this->_worker->validate();
        $this->_worker->run();
    }
}
