<?php

class BankStatementImporterRaiffeisenCSV extends BankStatementImporterParent
{
    private $_encoding_length = 3;
    private $_level_length = 1;
    
    private function parseLevelOne($line)
    {
        $date_parth_length = 8;
        $party_length = 20;
        $bank_statement_number_length = 6;
        $bank_statement_id_length = 20;
        $MB_length = 14;
        $pervious_amount_length = 27;
        $dugovni_promet_length = 24;
        $potrazni_promet_length = 24;
        $new_amount_length = 24;
        $br_nal_upl_length = 6;
        $br_nal_isp_length = 6;
        $datum_formiranja_length = 8;
        $vreme_formiranja_length = 6;
        
        $encoding = substr($line, 0, $this->_encoding_length);
        $line = substr($line, $this->_encoding_length);
        
        $level = substr($line, 0, $this->_level_length);
        $line = substr($line, $this->_level_length);
        
        $date_part = substr($line, 0, $date_parth_length);
        $line = substr($line, $date_parth_length);
        
        $party = trim(substr($line, 0, $party_length));
        $line = substr($line, $party_length);
        
        $bank_statement_number = substr($line, 0, $bank_statement_number_length);
        $line = substr($line, $bank_statement_number_length);
        
        $bank_statement_id = substr($line, 0, $bank_statement_id_length);
        $line = substr($line, $bank_statement_id_length);
        
        $MB = substr($line, 0, $MB_length);
        $line = substr($line, $MB_length);
        
        $pervious_amount = trim(substr($line, 0, $pervious_amount_length));
        $line = substr($line, $pervious_amount_length);
        
        $dugovni_promet = trim(substr($line, 0, $dugovni_promet_length));
        $line = substr($line, $dugovni_promet_length);
        
        $potrazni_promet = trim(substr($line, 0, $potrazni_promet_length));
        $line = substr($line, $potrazni_promet_length);
        
        $new_amount = substr($line, 0, $new_amount_length);
        $line = substr($line, $new_amount_length);
        
        $br_nal_upl = substr($line, 0, $br_nal_upl_length);
        $line = substr($line, $br_nal_upl_length);
        
        $br_nal_isp = substr($line, 0, $br_nal_isp_length);
        $line = substr($line, $br_nal_isp_length);
        
        $datum_formiranja = substr($line, 0, $datum_formiranja_length);
        $line = substr($line, $datum_formiranja_length);
        
        $vreme_formiranja = substr($line, 0, $vreme_formiranja_length);
        $line = substr($line, $vreme_formiranja_length);
        
        $date_part_datetime_obj = DateTime::createFromFormat('d/m/y', $date_part);
        $current_bank_statement_date = SIMAHtml::UserToDbDate($date_part_datetime_obj->format('d.m.Y'));

        return $this->createBankStatement($party, $current_bank_statement_date, 0, $bank_statement_number);

    }
    
    private function parseLevelTwo(BankStatement $bank_statement, $line)
    {
        $id_izvoda_length = 20;
        $broj_izvoda_length = 4;
        $datum_izvoda_length = 8;
        $maticni_broj_length = 13;
        $partija_length = 20;
        $iznos_duguje_length = 24;
        $iznos_potrazuje_length = 24;
        $nalog_korisnik_length = 125;
        $mesto_length = 20;
        $racun_length = 20;
        $sifra_placanja_length = 3;
        $svrha_placanja_length = 105;
        $model_nalogodavca_length = 2;
        $poziv_nalogodavca_length = 20;
        $model_korisnika_length = 2;
        $poziv_korisnika_length = 20;
        $broj_za_reklamaciju_length = 42;
        $broj_naloga_pp_length = 16;
        
        $level = substr($line, 0, $this->_level_length);
        $line = substr($line, $this->_level_length);
        
        $id_izvoda = substr($line, 0, $id_izvoda_length);
        $line = substr($line, $id_izvoda_length);
        
        $broj_izvoda = substr($line, 0, $broj_izvoda_length);
        $line = substr($line, $broj_izvoda_length);
        
        $datum_izvoda = substr($line, 0, $datum_izvoda_length);
        $line = substr($line, $datum_izvoda_length);
        
        $maticni_broj = substr($line, 0, $maticni_broj_length);
        $line = substr($line, $maticni_broj_length);
        
        $partija = substr($line, 0, $partija_length);
        $line = substr($line, $partija_length);
        
        $iznos_duguje = (double)(trim(substr($line, 0, $iznos_duguje_length)));
        $line = substr($line, $iznos_duguje_length);
        
        $iznos_potrazuje = (double)(trim(substr($line, 0, $iznos_potrazuje_length)));
        $line = substr($line, $iznos_potrazuje_length);
        
        $nalog_korisnik = trim(substr($line, 0, $nalog_korisnik_length));
        $line = substr($line, $nalog_korisnik_length);
        
        $mesto = substr($line, 0, $mesto_length);
        $line = substr($line, $mesto_length);
        
        $racun = trim(substr($line, 0, $racun_length));
        $line = substr($line, $racun_length);
        
        $sifra_placanja = substr($line, 0, $sifra_placanja_length);
        $line = substr($line, $sifra_placanja_length);
        
        $svrha_placanja = substr($line, 0, $svrha_placanja_length);
        $line = substr($line, $svrha_placanja_length);
        
        $model_nalogodavca = substr($line, 0, $model_nalogodavca_length);
        $line = substr($line, $model_nalogodavca_length);
        
        $poziv_nalogodavca = substr($line, 0, $poziv_nalogodavca_length);
        $line = substr($line, $poziv_nalogodavca_length);
        
        $model_korisnika = substr($line, 0, $model_korisnika_length);
        $line = substr($line, $model_korisnika_length);
        
        $poziv_korisnika = substr($line, 0, $poziv_korisnika_length);
        $line = substr($line, $poziv_korisnika_length);
        
        $broj_za_reklamaciju = substr($line, 0, $broj_za_reklamaciju_length);
        $line = substr($line, $broj_za_reklamaciju_length);
        
        $broj_naloga_pp = substr($line, 0, $broj_naloga_pp_length);
        $line = substr($line, $broj_naloga_pp_length);
        
        $date_part_datetime_obj = DateTime::createFromFormat('d/m/y', $datum_izvoda);
        $payment_date = SIMAHtml::UserToDbDate($date_part_datetime_obj->format('d.m.Y'));
        
        $invoice = false;
        if($iznos_duguje === 0.0)
        {
            $invoice = true;
        }
        
        $amount = $iznos_duguje;
        if($invoice)
        {
            $amount = $iznos_potrazuje;
        }
          
        $this->populateBankStatement(
                $racun, 
                $nalog_korisnik, 
                $payment_date,
                $amount,
                $sifra_placanja,
                $poziv_nalogodavca,
                $poziv_korisnika,
                '.',
                $bank_statement,
                'UTF-8',
                $invoice,
                $broj_za_reklamaciju,
                $model_nalogodavca,
                $model_korisnika
        );
    }
    
    private function validateEncoding($line)
    {
        $encoding = substr($line, 0, $this->_encoding_length);
        if($encoding !== "\xef\xbb\xbf")
        {
            throw new SIMAExceptionInvalidFile('invalid encoding: '.SIMAMisc::toTypeAndJsonString($encoding));
        }
    }
    
    public function validate()
    {
        $file_content = file_get_contents($this->file_path);
        
        $file_content_utf8 = mb_convert_encoding($file_content , 'UTF-8' , 'UTF-16LE');
        
        $line_count = 0;
        foreach(preg_split("/((\r?\n)|(\r\n?))/", $file_content_utf8) as $line)
        {
            $line_count++;
            if($line_count === 1)
            {
                $this->validateEncoding($line);
                $line = substr($line, $this->_encoding_length);
            }
            
            $level = $line[0];
            if($level != 1 && $level != 2)
            {
                throw new SIMAExceptionInvalidFile('invalid level: '.SIMAMisc::toTypeAndJsonString($level));
            }
        }
    }
    
    public function run()
    {
        
        $file_content = file_get_contents($this->file_path);
        
        $file_content_utf8 = mb_convert_encoding($file_content , 'UTF-8' , 'UTF-16LE');
        
        set_time_limit(5);
        
        $bank_statement = null;
        foreach(preg_split("/((\r?\n)|(\r\n?))/", $file_content_utf8) as $line)
        {
            set_time_limit(1);
            
            $level = (int)($line[0]);
            
            if($level === 2)
            {
                $this->parseLevelTwo($bank_statement, $line);
            }
            else
            {
                $bank_statement = $this->parseLevelOne($line);
            }
        } 
    }
}
