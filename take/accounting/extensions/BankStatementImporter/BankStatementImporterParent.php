<?php

abstract class BankStatementImporterParent
{
    protected $payment_configuration = null;
    protected $file_path = null;

    abstract protected function validate();
    abstract protected function run();
    
    final public function __construct(PaymentConfiguration $config, $file_path)
    {
        $this->payment_configuration = $config;
        $this->file_path = $file_path;
    }
    
    final protected function createBankStatement($bank_account_number, $date, $amount, $number)
    {
        $bank_account = \BankAccount::model()->find([
            'model_filter' => [
                'partner' => [
                    'ids' => Yii::app()->company->id
                ],
                'number_clean' => $bank_account_number
            ]
        ]);
        if(empty($bank_account))
        {
            throw new SIMAExceptionNoBankAccontForSystemCompanyWithNumber($bank_account_number);
        }

        $bank_account_id = $bank_account->id;

        $criteria = new SIMADbCriteria([
            'Model' => 'BankStatement',
            'model_filter' => [
                'account_id' => $bank_account_id,
                'date' => $date,
            ]
        ]);
        $bank_statement = BankStatement::model()->find($criteria);
//        $bank_statement = BankStatement::model()->find([
//            'model_filter' => [
//                'account_id' => $bank_account_id,
//                'date' => $date,
//            ]
//        ]);
        
        if($bank_statement === null)
        {            
            $bank_statement = new BankStatement();
            $bank_statement->account_id = $bank_account_id;
            $bank_statement->date = $date;
            $bank_statement->amount = $amount;
            $bank_statement->number = $number;
            $bank_statement->save();
            $bank_statement->refresh();
        }
        
        return $bank_statement;
    }
    
    final protected function populateBankStatement($account_number, $account_owner, $input_payment_date, $input_amount, $payment_code, 
                                            $call_for_number, $call_for_number_partner, $decimal_separator, 
                                            BankStatement $bank_statement, $encoding, $invoice, $bank_transaction_id, 
                                            $call_for_number_modul, $call_for_number_modul_partner)
    {
        try
        {
            $amount = $this->clearAmount($input_amount, $decimal_separator);
            $payment_date = date('d.m.Y',  strtotime($input_payment_date));

            if($amount === null || empty($amount))
            {
                $amount = 0;
            }

            if ($encoding !== 'UTF-8')
            {
                $call_for_number = iconv($encoding, 'UTF-8', $call_for_number);
                $call_for_number_partner = iconv($encoding, 'UTF-8', $call_for_number_partner);
                $account_owner = iconv($encoding, 'UTF-8//IGNORE', $account_owner); /// ignorise karaktere koji nisu ocekivanog ulaznog encoding-a
            }
            
            $_partner_id = null;
            $recurring_bill = null;
            if (!empty($call_for_number))
            {
                $recurring_bills = RecurringBill::model()->findAllByModelFilter([
                    'call_for_number' => $call_for_number
                ]);
                if (count($recurring_bills)==1)
                {
                    $recurring_bill = $recurring_bills[0];   
                    $_partner_id = $recurring_bill->partner_id;
                }
                elseif (count($recurring_bills)>1)
                {
                    Yii::app()->raiseNote('Za poziv na broj '
                            .$call_for_number
                            .' je nadjeno vise klijenata');
                }
            }

            $pay_code = null;
            $payment_code = trim($payment_code);
            if (!empty($payment_code))
            {
                $pay_code = PaymentCode::model()->findByAttributes(array('code'=>$payment_code));
                if ($pay_code==null)
                {
                    $pay_code = new PaymentCode();
                    $pay_code->code = $payment_code;
                    $pay_code->name = 'generic';
                    $pay_code->payment_type_id = Yii::app()->configManager->get('accounting.default_payment_type');
                    $pay_code->save();
                }
            }
            
            if(empty($account_number))
            {
                //NIJE UPISAN BROJ RACUNA, ako ne postoji recuriring, onda banka
                $_account_id = null;
                if (is_null($_partner_id))
                {
                    $_partner_id = $bank_statement->account->bank->company->partner->id;
                }
            }
            else
            {
                //upisan je ziro racun
                $account_id = SIMASQLFunctions::getBankAccountIdByNumber($account_number);

                $bank_account = null;
                if(!empty($account_id))
                {
                    $bank_account = BankAccount::model()->findByPk($account_id);
                }
                
                //ukoliko ne postoji i ne postoji recuring partner, pitamo korisnika ko je
                if(is_null($bank_account))
                {
//                    $account_number = (string)$account_number;
                    $payment_code_display = (is_null($pay_code))?$payment_code:$pay_code->DisplayName;
                    $bank_code = substr($account_number,0,3);
                    
                    $missing_account = [
                        'form_id' => SIMAHtml::uniqid(),
                        'account_number'=> $account_number,
                        'invoice' => $invoice,
                        'account_owner' => $account_owner,
                        'input_payment_date' => $input_payment_date,
                        'input_amount' => $input_amount,
                        'payment_code_display' => $payment_code_display,
                        'call_for_number' => $call_for_number, 
                        'call_for_number_modul' => $call_for_number_modul, 
                        'call_for_number_partner' => $call_for_number_partner, 
                        'call_for_number_modul_partner' => $call_for_number_modul_partner, 
                    ];
//                    
                    $display_message = '';
//                    $add_partner_btn = '';
                    if (!is_null($_partner_id))
                    {
                        $missing_account['partner_id'] = $_partner_id;
                        $display_message = Yii::t('AccountingModule.BankStatement', 'PartnerAutomaticallySetedByCallToNumber', [
                            '{company_name}' => $recurring_bill->partner->DisplayName
                        ]);
                    }
                    else
                    {
                        $company_params = $this->getCompanyInformationFromNBSByAccountNumber($account_number);

                        if (!empty($company_params['company_from_system']))
                        {
                            $missing_account['partner_id'] = $company_params['company_from_system']->id;
                            $display_message = Yii::t('AccountingModule.BankStatement', 'PartnerAutomaticallySetedByNBS', [
                                '{company_name}' => $company_params['company_from_system']->DisplayName
                            ]);
                        }
                        else if (!empty($company_params['nbs_pib']) || !empty($company_params['nbs_mb']))
                        {
                            $missing_account['$add_partner_btn_data'] = [
                                'nbs_pib' => !empty($company_params['nbs_pib']) ? $company_params['nbs_pib'] : '',
                                'nbs_mb' => !empty($company_params['nbs_mb']) ? $company_params['nbs_mb'] : ''
                            ];
//                            $nbs_pib = !empty($company_params['nbs_pib']) ? $company_params['nbs_pib'] : '';
//                            $nbs_mb = !empty($company_params['nbs_mb']) ? $company_params['nbs_mb'] : '';
//                            $add_partner_btn_id = 'add_partner_btn_' . SIMAHtml::uniqid();
//                            $add_partner_btn = $this->widget('SIMAButton', [
//                                'id' => $add_partner_btn_id,
//                                'action' => [
//                                    'title' => Yii::t('AccountingModule.BankStatement', 'AddPartnerFromNBS'),
//                                    'onclick' => ['sima.accountingMisc.addBankAccountPartnerFromNBS', $add_partner_btn_id, $nbs_pib, $nbs_mb]
//                                ],
//                            ], true);
                        }
                    }
                    $missing_account['display_message'] = $display_message;
//
                    $bank = Bank::model()->find([
                        'model_filter' => [
                            'bank_account_code' => $bank_code
                        ]
                    ]);
                    if (!empty($bank))
                    {
                        $missing_account['bank_id'] = $bank->id;
                    }
                    
                    $missing_account['currency_id'] = $bank_statement->account->currency_id;
                    
//                    $bank_account_model = BankAccount::model();
//                    $form_name = 'fromFile';
//                    $form_data = CJSON::encode([
//                        'model_name' => BankAccount::class,
//                        'form_name' => $form_name
//                    ]);
//                    $bank_account_model->setModelAttributes([
//                        'number' => $account_number,
//                        'bank_id' => !empty($missing_account['bank_id']) ? $missing_account['bank_id'] : null,
//                        'partner_id' => !empty($missing_account['partner_id']) ? $missing_account['partner_id'] : null,
//                        'currency_id' => !empty($missing_account['currency_id']) ? $missing_account['currency_id'] : null,
//                        'start_amount' => 0
//                    ], $form_name);
//                    $bank_account_form_html = ModelController::renderModelForm($bank_account_model, [
//                        'form_id' => BankAccount::class . $missing_account['form_id'],
//                        'formName' => $form_name
//                    ])['html'];
//                    
//                    $missing_account['add_bank_account_html'] = $this->renderPartial('add_bank_account_html', [
//                        'form_id' => $missing_account['form_id'],
//                        'form_data' => $form_data,
//                        'bank_account_form_html' => $bank_account_form_html,
//                        'display_message' => $display_message,
//                        'add_partner_btn' => $add_partner_btn,
//                        'invoice' => $invoice,
//                        'account_owner' => $account_owner, 
//                        'account_number' => $account_number, 
//                        'input_payment_date' => $input_payment_date, 
//                        'input_amount' => $input_amount, 
//                        'payment_code_display' => $payment_code_display, 
//                        'call_for_number' => $call_for_number, 
//                        'call_for_number_modul' => $call_for_number_modul, 
//                        'call_for_number_partner' => $call_for_number_partner, 
//                        'call_for_number_modul_partner' => $call_for_number_modul_partner, 
//                    ], true, false);

//                    $this->sendEndEvent($missing_account);
//                    Yii::app()->end(); /// TODO: izbaciti kada se u sendendevent ubaci yii app end
                    throw new SIMAExceptionMissingBankAccount($missing_account);
                }
                 
                $_account_id = $bank_account->id;
                if (is_null($_partner_id))
                {
                    $_partner_id = $bank_account->partner_id;
                }
                elseif ($_partner_id !== $bank_account->partner_id)
                {
//                    Yii::app()->raiseNote('Prepoznat je ponavljajuci racun po pozivu na broj '
//                        .$call_for_number
//                        .' gde je partner '.$recurring_bill->partner->DisplayName
//                        .' a uplaceno je sa ziro racuna partnera '
//                        .$bank_account->partner->DisplayName
//                        );
                }
                
            }

            $new_payment=new Payment();
            $new_payment->payment_date = $payment_date;
            $new_payment->partner_id=$_partner_id;
            $new_payment->account_id=$_account_id;
            $new_payment->bank_statement_id = $bank_statement->id;
            $new_payment->call_for_number=$call_for_number;
            $new_payment->call_for_number_partner=$call_for_number_partner;
            $new_payment->amount=$amount;
            $new_payment->invoice=$invoice;
            $new_payment->transaction_id=$bank_transaction_id;
            $new_payment->call_for_number_modul = $call_for_number_modul;
            $new_payment->call_for_number_modul_partner = $call_for_number_modul_partner;
            if (!is_null($pay_code))
            {
                $new_payment->payment_type_id = $pay_code->payment_type_id;
                $new_payment->payment_code_id = $pay_code->id;
            }
            if (empty($new_payment->payment_type_id))
            {
                $new_payment->payment_type_id = Yii::app()->configManager->get('accounting.default_payment_type_for_empty_payment_code', true);
            }

            $new_payment_cjson = CJSON::encode($new_payment);
            $hash = md5($new_payment_cjson);

            $criteria = new SIMADbCriteria([
                'Model' => 'Payment',
                'model_filter' => [
                    'hash' => $hash,
                ]
            ]);
            
            $payment = Payment::model()->find($criteria);

            $shouldSaveNewPayment = true;
            if($payment !== null)
            {
                $shouldSaveNewPayment = false;
            }
            else if($call_for_number !== null && !empty($call_for_number) && isset($_account_id))
            {
                $criteria2 = new SIMADbCriteria([
                    'Model' => Payment::class,
                    'model_filter' => [
                        'account' => [
                            'ids' => $_account_id
                        ],
                        'call_for_number' => $call_for_number
                    ]
                ]);
                $paymentExistingInDB = Payment::model()->find($criteria2);

                if($paymentExistingInDB !== null && !isset($paymentExistingInDB->bank_statement_id))
                {
                    $shouldSaveNewPayment = false;
                    $paymentExistingInDB->payment_date = $new_payment->payment_date;
                    $paymentExistingInDB->partner_id = $new_payment->partner_id;
                    $paymentExistingInDB->amount = $new_payment->amount;
                    $paymentExistingInDB->invoice = $new_payment->invoice;
                    $paymentExistingInDB->payment_type_id = $new_payment->payment_type_id;
                    $paymentExistingInDB->bank_statement_id = $new_payment->bank_statement_id;
                    $paymentExistingInDB->payment_code_id = $new_payment->payment_code_id;
                    $paymentExistingInDB->call_for_number_partner = $new_payment->call_for_number_partner;
                    $paymentExistingInDB->save();
                }
            }

            if($shouldSaveNewPayment == true)
            {       
                $new_payment->hash = $hash;
                $new_payment->save();
            }
        }
        catch(SIMAExceptionBankAccountNumberInvalid $ex)
        {
//            error_log(__METHOD__.' - $ex: '.CJSON::encode($ex));
            Yii::app()->raiseNote(Yii::t(
                    'AccountingModule.BankStatement', 
                    'CouldNotCreatePaymentInvalidPartnerBankAccountNumber',
                    [
                        '{bank_account_number}' => $ex->bank_account_number,
                        '{payment_date}' => $input_payment_date,
                        '{amount}' => $input_amount,
                        '{db_err_message}' => $ex->db_err_message
                    ]
            ));
//                return;
        }
    }
    
    /**
     * formatira u oblik koji sistem podrzava
     * @param type $amount
     * @param type $separator
     * @return string
     */
    final protected function clearAmount($amount,$separator)
    {
        $new_amount = '';
        if(gettype($amount) !== 'string')
        {
            $amount = (string)$amount;
        }
        for($i = 0; $i<strlen($amount); $i++)
        {
            if (is_numeric($amount[$i]))
            {
                $new_amount .= $amount[$i];
            }
            if ($separator==$amount[$i])
            {
                $new_amount .= '.';
            }
        }
        return $new_amount;
    }
    
    final protected function getCompanyInformationFromNBSByAccountNumber($account_number)
    {
        $params = [];

        if (Yii::app()->NBSConnection->isConnected() === true)
        {
            $account_number_parsed = str_replace('-', '', $account_number);
            $account_number_parsed = substr_replace($account_number_parsed, '-', 3, 0);
            $account_number_parsed = substr_replace($account_number_parsed, '-', -2, 0);
            
            /**
             * ne treba vrsiti validaciju jer se pre ovog poziva odradila validacija pozivanjem
             * SIMASQLFunctions::getBankAccountIdByNumber($account_number);
             */
            
            $nbs_company_data = Yii::app()->NBSConnection->GetCompanyAccountByBankAccountNumber($account_number_parsed);
            if (!empty($nbs_company_data))
            {
                if (!empty($nbs_company_data[0]['TaxIdentificationNumber']))
                {
                    $params['nbs_pib'] = Company::getFullPIB($nbs_company_data[0]['TaxIdentificationNumber']);
                    $params['company_from_system'] = Company::model()->findByAttributes([
                        'PIB' => $params['nbs_pib']
                    ]);
                }
                else if (!empty($nbs_company_data[0]['NationalIdentificationNumber']))
                {
                    $params['nbs_mb'] = Company::getFullMB($nbs_company_data[0]['NationalIdentificationNumber']);
                    $params['company_from_system'] = Company::model()->findByAttributes([
                        'MB' => $params['nbs_mb']
                    ]);
                }
            }
        }
        
        return $params;
    }
}
