<?php

class SIMAExceptionMissingBankAccount extends SIMAException
{
    public $bank_account_info_data = null;
    
    public function __construct($bank_account_info_data)
    {
        $this->bank_account_info_data = $bank_account_info_data;
        parent::__construct('missing bank account');
    }
}
