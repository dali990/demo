<?php

class BankStatementImporterSOGEXML extends BankStatementImporterParent
{
    private $_loaded_xml = null;
    private $_bank_statement = null;
    
    public function validate()
    {
        libxml_use_internal_errors(true);
        $this->_loaded_xml = simplexml_load_file($this->file_path);
        if($this->_loaded_xml === false)
        {
            throw new SIMAExceptionInvalidFile(Yii::t('AccountingModule.BankStatement', 'XmlLoadFailed'));
        }
        
        $zaglavlje = $this->_loaded_xml->Zaglavlje;
        $zaglavlje_attributes = $zaglavlje->attributes();
        
        $date = $zaglavlje_attributes->DatumIzvoda->__toString();
        
        $date_obj = DateTime::createFromFormat('d.m.Y', $date);
        
        if($date_obj === false)
        {
            throw new SIMAExceptionInvalidFile(Yii::t('AccountingModule.BankStatement', 'SOGEXMLInvalidDate'));
        }
    }
    
    public function run()
    {
        $this->_loaded_xml = simplexml_load_file($this->file_path);
        
        $zaglavlje = $this->_loaded_xml->Zaglavlje;
        $zaglavlje_attributes = $zaglavlje->attributes();
        
        $bank_account_number = $zaglavlje_attributes->Partija->__toString();
        $date = $zaglavlje_attributes->DatumIzvoda->__toString();
        $amount = $zaglavlje_attributes->NovoStanje->__toString();
        $number = $zaglavlje_attributes->IzvodID->__toString();
        
        $date_obj = DateTime::createFromFormat('d.m.Y', $date);
        $date_formated = SIMAHtml::UserToDbDate($date_obj->format('d.m.Y'));
        
        $this->_bank_statement = $this->createBankStatement($bank_account_number, $date_formated, $amount, $number);
        
        foreach ($this->_loaded_xml->Stavke as $child)
        {    
            $this->parseStatement($child, $date_formated);
        }
    }
        
    private function parseStatement(SimpleXMLElement $child, $date)
    {
        $child_attributes = $child->attributes();
        
        $account_number = $child_attributes->Opis5->__toString();
        $account_owner = $child_attributes->Opis3->__toString();
        $input_payment_date = $date;
        $payment_code = substr($child_attributes->Opis6->__toString(), 4);
        $call_for_number = '';
        $call_for_number_partner = '';
        $call_for_number_modul = '';
        $call_for_number_modul_partner = '';
        $bank_transaction_id = crc32(
                $account_number
                .$account_owner
                .$payment_code
                .$child_attributes->Opis2->__toString()
                .$child_attributes->Opis2->__toString()
                .$child_attributes->Opis9->__toString()
            );
        
        $invoice = $this->parseStatement_invoice($child_attributes);
        $amount = (double)($child_attributes->Duguje->__toString());
        if($invoice)
        {
            $invoice = true;
            $amount = (double)($child_attributes->Potrazuje->__toString());
        }
        
        $this->populateBankStatement(
                $account_number, 
                $account_owner, 
                $input_payment_date, 
                $amount,
                $payment_code,
                $call_for_number,
                $call_for_number_partner,
                '.', 
                $this->_bank_statement, 
                'UTF-8', 
                $invoice,
                $bank_transaction_id,
                $call_for_number_modul,
                $call_for_number_modul_partner
        );
    }
    
    private function parseStatement_invoice($child_attributes)
    {
        $invoice = false;
        if($child_attributes->Duguje->__toString() === '0')
        {
            $invoice = true;
        }
        return $invoice;
    }
}

