<?php

class BankStatementImporterErsteWeb extends BankStatementImporterParent
{
    private $_bank_account_number = null;
    private $_begining_bank_statement_number = null;
    private $_current_bank_statement_number = null;
    private $_current_bank_statement_date = null;
    private $_current_bank_statement_year = null;
    private $_bank_statement = null;
    private $_number_of_rows = null;
    private $_number_of_empty_account_numbers = 0;
    
    public function validate()
    {
        $config = $this->payment_configuration;
        $real_path = $this->file_path;
        
        $max_col = max(array(
            $config->bank_account_id, 
            $config->date_col,
            $config->account_number_col,
            $config->account_owner_col,
            $config->amount_col,
            $config->amount2_col,
            $config->payment_code_col,
            $config->call_for_number_credit_col,
            $config->separator,
            $config->decimal_separator,
            $config->encoding
        ));

        $file = fopen($real_path,"r");

        $i = 0;
        while (($csv_row = fgetcsv($file, 0, $config->separator)) !== false)
        {
            if(count($csv_row) <= $max_col)
            {
//                return "invalid file - code: 9";
                throw new SIMAExceptionInvalidFile('code: 9');
            }

            if($i === 0)
            {
                $this->validate_first_row($csv_row);
            }
            else if($i>1)
            {
                $this->validate_rest_rows($csv_row);
            }

            $i++;
        }
    }
    
    private function validate_first_row($csv_row)
    {
        $config = $this->payment_configuration;
        
        if($csv_row[$config->bank_account_id] === null)
        {
            throw new SIMAExceptionInvalidFile('code: 10');
        }

        $bank_account_csv_value = $csv_row[$config->bank_account_id];
        if(preg_match("/Izvod prometa za ra.?un [0-9-]+ za period .+ Broj izvoda [0-9]+/", $bank_account_csv_value) === 0)
        {
            throw new SIMAExceptionInvalidFile('code: 11');
        }
    }
    
    private function validate_rest_rows($csv_row)
    {
        $config = $this->payment_configuration;
        
        if(
            $csv_row[$config->bank_account_id] === null
            || $csv_row[$config->date_col] === null
            || $csv_row[$config->account_number_col] === null
            || $csv_row[$config->account_owner_col] === null
            || $csv_row[$config->amount_col] === null
            || $csv_row[$config->amount2_col] === null
            || $csv_row[$config->payment_code_col] === null
            || $csv_row[$config->call_for_number_credit_col] === null
            || $csv_row[$config->bank_account_amount] === null
            || $csv_row[$config->bank_transaction_id] === null
        )
        {
            throw new SIMAExceptionInvalidFile('code: 12');
        }
    }
    
    public function run()
    {
        $config = $this->payment_configuration;
        $real_path = $this->file_path;
        
        set_time_limit(5);
        $row_counter = 0;
        $this->_number_of_rows = SIMAMisc::numberOfRowsInCSV($real_path);
        $file = fopen($real_path,"r");
        while (($csv_row = fgetcsv($file, 0, $config->separator)) !== false)
        {
            set_time_limit(1);
            $row_counter++;
            
            $this->run_parse_row($row_counter, $csv_row);
        }
        set_time_limit(5);
        fclose($file);
        
        if($this->_number_of_empty_account_numbers > 0)
        {
            Yii::app()->raiseNote(Yii::t('AccountingModule.BankStatement', 'NumberOfEmptyStatementsThatWereSetBankForPartner', [
                '{number_of_empty_account_numbers}' => $this->_number_of_empty_account_numbers
            ]));
        }
    }
    
    private function run_parse_row($row_counter, $csv_row)
    {
        $config = $this->payment_configuration;
        
        if($row_counter === 1)
        {
            $bank_account_csv_value = $csv_row[$config->bank_account_id];

            $preg_match_output = null;
            preg_match("/Izvod prometa za ra.?un [0-9-]+ za period/", $bank_account_csv_value, $preg_match_output);
            preg_match("/[0-9-]+/", $preg_match_output[0], $preg_match_output);

            $this->_bank_account_number = $preg_match_output[0];

            preg_match("/Broj izvoda [0-9]+/", $bank_account_csv_value, $preg_match_output);
            preg_match("/[0-9]+/", $preg_match_output[0], $preg_match_output);

            $this->_begining_bank_statement_number = (int)($preg_match_output[0]);
        }

        if($row_counter <= 2)
        {
            return;
        }

        $update_event_message = ($row_counter/$this->_number_of_rows)*100;
        if(Yii::app()->isWebApplication())
        {
            Yii::app()->controller->sendUpdateEvent($update_event_message);
        }
        else
        {
            error_log(__METHOD__.' - $update_event_message: '.$update_event_message);
        }

        $payment_date = $csv_row[$config->date_col];

        if(is_null($this->_current_bank_statement_date) && is_null($this->_current_bank_statement_number))
        {
            $this->_current_bank_statement_date = $payment_date;
            $this->_current_bank_statement_number = $this->_begining_bank_statement_number;
            $this->_current_bank_statement_year = substr($payment_date, 6);
        }
        else if($this->_current_bank_statement_date !== $payment_date)
        {
            if($this->_current_bank_statement_year !== substr($payment_date, 6))
            {
                $this->_current_bank_statement_year = substr($payment_date, 6);
                $this->_current_bank_statement_number = 0;
            }
            $this->_current_bank_statement_date = $payment_date;
            $this->_current_bank_statement_number++;
        }

        $this->_bank_statement = $this->createBankStatement($this->_bank_account_number, $this->_current_bank_statement_date, 0, $this->_current_bank_statement_number);
        if(!empty($this->_bank_statement))
        {
            $account_number = $csv_row[$config->account_number_col];
            $account_owner = $csv_row[$config->account_owner_col];
            $amount = $csv_row[$config->amount_col];
            $unreleased = $csv_row[$config->amount2_col];
            $payment_code=$csv_row[$config->payment_code_col];
            $bank_transaction_id=$csv_row[$config->bank_transaction_id];
            $call_for_number = null;
            $call_for_number_modul = null;
            $call_for_number_partner = null;
            $call_for_number_modul_partner = null;

            if(empty($account_number))
            {
                $this->_number_of_empty_account_numbers++;
            }
            if (!empty($config->call_for_number_debit_col))
            {
                $call_for_number_csv_content = $csv_row[$config->call_for_number_debit_col];
                if(!empty($call_for_number_csv_content))
                {
                    $call_for_number_csv_content_exploded = explode(' ', $call_for_number_csv_content);
                    $call_for_number_modul = isset($call_for_number_csv_content_exploded[0])?$call_for_number_csv_content_exploded[0]:'';
                    $call_for_number = isset($call_for_number_csv_content_exploded[1])?$call_for_number_csv_content_exploded[1]:'';
                }
            }

            if (!empty($config->call_for_number_credit_col))
            {
                $call_for_number_partner_csv_content = $csv_row[$config->call_for_number_credit_col];
                if(!empty($call_for_number_partner_csv_content))
                {
                    $call_for_number_partner_csv_content_exploded = explode(' ', $call_for_number_partner_csv_content);
                    $call_for_number_modul_partner = isset($call_for_number_partner_csv_content_exploded[0])?$call_for_number_partner_csv_content_exploded[0]:'';
                    $call_for_number_partner = isset($call_for_number_partner_csv_content_exploded[1])?$call_for_number_partner_csv_content_exploded[1]:'';
                }
            }

            if(!isset($amount) || $amount=='')
            {
                $invoice = true;
                $amount = isset($unreleased)?$unreleased:0;
            }
            else
            {
                $invoice = false;
            }

            $this->populateBankStatement(
                    $account_number, 
                    $account_owner, 
                    $payment_date,
                    $amount,
                    $payment_code,
                    $call_for_number,
                    $call_for_number_partner,
                    $config->decimal_separator,
                    $this->_bank_statement,
                    $config->encoding,
                    $invoice,
                    $bank_transaction_id,
                    $call_for_number_modul,
                    $call_for_number_modul_partner
            );

            $bank_account_amount = 0;
            if(!empty($csv_row[$config->bank_account_amount]))
            {
                $bank_account_amount = str_replace(array('.', ','), array('', '.'), $csv_row[$config->bank_account_amount]);
            }
            $this->_bank_statement->amount = $bank_account_amount;
            $this->_bank_statement->save();
        }
        else
        {
            Yii::app()->raiseNote(Yii::t(
                    'AccountingModule.BankStatement', 
                    'CouldNotCreatePaymentInvalidPartnerBankAccountNumber',
                    [
                        '{bank_account_number}' => $this->_bank_account_number,
                        '{payment_date}' => $this->_current_bank_statement_date,
                        '{amount}' => 0,
                        '{db_err_message}' => 'Ne postoji ovaj bankovni racun u sistemu'
                    ]
            ));
        }
    }
}

