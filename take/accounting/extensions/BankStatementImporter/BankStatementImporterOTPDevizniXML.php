<?php

class BankStatementImporterOTPDevizniXML extends BankStatementImporterParent
{
    private $_xml = null;
    private $_zaglavlje_attributes = null;
    private $_bank_account_number = null;
    private $_bank_statement = null;
    
    public function validate()
    {
        libxml_use_internal_errors(true);
        $this->_xml = simplexml_load_file($this->file_path);
        if($this->_xml === false)
        {
            throw new SIMAExceptionInvalidFile(Yii::t('AccountingModule.BankStatement', 'XmlLoadFailed'));
        }
        
        $zaglavlje = $this->_xml->Zaglavlje;
        $this->_zaglavlje_attributes = $zaglavlje->attributes();
        
        $zaglavlje_partija = $this->_zaglavlje_attributes->Partija->__toString();
        if(!strlen($zaglavlje_partija) === 25)
        {
            $this->xmlCompanyBankAccountNumberInvalid($zaglavlje_partija);
        }
        if(!SIMAMisc::stringStartsWith($zaglavlje_partija, 'RS35'))
        {
            $this->xmlCompanyBankAccountNumberInvalid($zaglavlje_partija);
        }
        $this->_bank_account_number = substr($zaglavlje_partija, 4, 18);
    }
    
    public function run()
    {
        $this->_xml = simplexml_load_file($this->file_path);
        
        $date = $this->_zaglavlje_attributes->DatumIzvoda->__toString();
        $amount = $this->_zaglavlje_attributes->NovoStanje->__toString();
        $number = $this->_zaglavlje_attributes->IzvodID->__toString();
                
        $this->_bank_statement = $this->createBankStatement($this->_bank_account_number, $date, $amount, $number);
                
        foreach ($this->_xml->Stavke as $child)
        {    
            $this->parseStatement($child);
        }
    }
    
    private function parseStatement(SimpleXMLElement $child)
    {
        $child_attributes = $child->attributes();
        
        $invoice = $this->parseStatement_invoice($child_attributes);
        $amount = (double)($child_attributes->Duguje->__toString());
        if($invoice)
        {
            $invoice = true;
            $amount = (double)($child_attributes->Potrazuje->__toString());
        }
        
        $account_number = null;
        $account_owner = $child_attributes->Opis4->__toString();
        $input_payment_date = $child_attributes->DatumValute->__toString();
        $payment_code = null;
        $call_for_number = null;
        $call_for_number_partner = null;
        $bank_transaction_id = $child_attributes->Opis2->__toString();
        $call_for_number_modul = null;
        $call_for_number_modul_partner = null;

        $this->populateBankStatement(
                $account_number, 
                $account_owner, 
                $input_payment_date, $amount,
                $payment_code,
                $call_for_number,
                $call_for_number_partner,
                  '.', $this->_bank_statement, 'UTF-8', $invoice,
                $bank_transaction_id,
                $call_for_number_modul,
                $call_for_number_modul_partner
        );
    }
    
    private function parseStatement_invoice($child_attributes)
    {
        $invoice = false;
        if(((double)($child_attributes->Duguje->__toString())) === 0.0)
        {
            $invoice = true;
        }
        return $invoice;
    }
    
    private function xmlCompanyBankAccountNumberInvalid($zaglavlje_partija)
    {
        throw new Exception('broj racuna kompanije "'.$zaglavlje_partija.'" u nepravilnom obliku - ocekivani format: RS35 {broj racuna banke - 18 cifara} {3 cifre devize}');
    }
}
