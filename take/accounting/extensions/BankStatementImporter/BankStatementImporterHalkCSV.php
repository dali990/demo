<?php

class BankStatementImporterHalkCSV extends BankStatementImporterParent
{
    private $_bank_statement = null;
    
    public function validate()
    {
        $file_content = file_get_contents($this->file_path);
        
        set_time_limit(5);
        
        $line_count = 0;
        $lines = preg_split("/((\r?\n)|(\r\n?))/", $file_content);
        $lines_count = count($lines);
        foreach($lines as $line)
        {
            set_time_limit(5);
            
            if(empty($line))
            {
                continue;
            }
            
            $update_event_message = ($line_count/$lines_count)*100;
            if(Yii::app()->isWebApplication())
            {
                Yii::app()->controller->sendUpdateEvent($update_event_message);
            }
            
            $line_count++;
            
            if($line_count === 1)
            {
                $date_part = substr($line, 0, 8);
                $date_part_datetime_obj = DateTime::createFromFormat('dmY', $date_part);
                if($date_part_datetime_obj === false)
                {
                    throw new SIMAExceptionInvalidFile('code: 1</br>proverite da li se fajl i konfiguracija poklapaju');
                }
            }
            else
            {
            }
        }
    }
    
    public function run()
    {
        $file_content = file_get_contents($this->file_path);
                
        set_time_limit(5);
        
        $line_count = 0;
        $lines = preg_split("/((\r?\n)|(\r\n?))/", $file_content);
        $lines_count = count($lines);
        foreach($lines as $line)
        {
            set_time_limit(5);
            
            if(empty($line))
            {
                continue;
            }
            
            $update_event_message = ($line_count/$lines_count)*100;
            if(Yii::app()->isWebApplication())
            {
                Yii::app()->controller->sendUpdateEvent($update_event_message);
            }
            
            $line_count++;
            
            if($line_count === 1)
            {
                $this->parseLeadLine($line);
            }
            else
            {
                $this->parseLine($line);
            }
        }
    }
    
    private function parseLeadLine($line)
    {
        $date_part = substr($line, 0, 8);
        $date_part_datetime_obj = DateTime::createFromFormat('dmY', $date_part);
        $date = SIMAHtml::UserToDbDate($date_part_datetime_obj->format('d.m.Y'));
        
        $bank_account_number = substr($line, 12, 18);
        
        $amount = (float)(substr($line, 87, 15));
        $amount_decimal = (float)('0.'.substr($line, 102, 2));
        $amount += $amount_decimal;
        $amount_sign = substr($line, 104, 1);
        if($amount_sign === '-')
        {
            $amount *= -1;
        }
        
        $number = substr($line, 147, 3);
        
        $this->_bank_statement = $this->createBankStatement($bank_account_number, $date, $amount, $number);
    }
    
    private function parseLine($line)
    {
        $account_number = substr($line, 148, 18);
        $account_owner = substr($line, 18, 35);
        
        $date_part = substr($line, 188, 8);
        $date_part_datetime_obj = DateTime::createFromFormat('dmY', $date_part);
        $input_payment_date = SIMAHtml::UserToDbDate($date_part_datetime_obj->format('d.m.Y'));
        
        $input_amount = (float)(substr($line, 134, 11));
        $amount_decimal = (float)('0.'.substr($line, 145, 2));
        $input_amount += $amount_decimal;
        
        $payment_code = substr($line, 96, 3);
        $call_for_number = substr($line, 76, 20);
        $call_for_number_partner = substr($line, 168, 20);
        $invoice = substr($line, 147, 1) === 'P';
        $broj_za_reklamaciju = substr($line, 200, 14);
        $call_for_number_modul = substr($line, 74, 2);
        $call_for_number_modul_partner = substr($line, 166, 2);
        
        $this->populateBankStatement(
                $account_number, 
                $account_owner, 
                $input_payment_date, 
                $input_amount, 
                $payment_code, 
                $call_for_number, 
                $call_for_number_partner, 
                '.', 
                $this->_bank_statement, 
                'UTF-8', 
                $invoice, 
                $broj_za_reklamaciju, 
                $call_for_number_modul, 
                $call_for_number_modul_partner
            );
    }
}
