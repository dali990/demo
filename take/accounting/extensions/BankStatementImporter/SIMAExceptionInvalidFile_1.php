<?php

class SIMAExceptionInvalidFile extends SIMAException
{    
    public function __construct($message)
    {
        parent::__construct($message);
    }
}
