<?php

class BankStatementImporterRaiffeisenXML extends BankStatementImporterParent
{
    private $_bank_statement = null;
    
    public function validate()
    {
        libxml_use_internal_errors(true);
        $xml = simplexml_load_file($this->file_path);
        if($xml === false)
        {
            throw new SIMAExceptionInvalidFile(Yii::t('AccountingModule.BankStatement', 'XmlLoadFailed'));
        }
    }
    
    public function run()
    {
        $xml = simplexml_load_file($this->file_path);
        
        $zaglavlje = $xml->Zaglavlje;
        $zaglavlje_attributes = $zaglavlje->attributes();
        
        $bank_account_number = $zaglavlje_attributes->Partija->__toString();
        $date = $zaglavlje_attributes->DatumIzvoda->__toString();
        $amount = $zaglavlje_attributes->NovoStanje->__toString();
        $number = $zaglavlje_attributes->BrojIzvoda->__toString();
        
        $this->_bank_statement = $this->createBankStatement($bank_account_number, $date, $amount, $number);
        
        foreach ($xml->Stavke as $child)
        {    
            $this->parseStatement($child);
        }
    }
    
    private function parseStatement(SimpleXMLElement $child)
    {
        $child_attributes = $child->attributes();
        
        $invoice = $this->parseStatement_invoice($child_attributes);
        $amount = (double)($child_attributes->Duguje->__toString());
        if($invoice)
        {
            $invoice = true;
            $amount = (double)($child_attributes->Potrazuje->__toString());
        }

        $this->populateBankStatement(
                $child_attributes->BrojRacunaPrimaocaPosiljaoca->__toString(), 
                $child_attributes->NalogKorisnik->__toString(), 
                $child_attributes->DatumValute->__toString(), $amount,
                $child_attributes->SifraPlacanja->__toString(),
                $child_attributes->PozivNaBrojZaduzenjaOdobrenja->__toString(),
                $child_attributes->PozivNaBrojKorisnika->__toString(),
                  '.', $this->_bank_statement, 'UTF-8', $invoice,
                $child_attributes->Referenca->__toString(),
                $child_attributes->ModelZaduzenjaOdobrenja->__toString(),
                $child_attributes->ModelKorisnika->__toString()
        );
    }
    
    private function parseStatement_invoice($child_attributes)
    {
        $invoice = false;
        if(((double)($child_attributes->Duguje->__toString())) === 0.0)
        {
            $invoice = true;
        }
        return $invoice;
    }
}
