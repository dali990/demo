<?php

class BankStatementImporterHalcomCSV extends BankStatementImporterParent
{
    public function validate()
    {
        $max_col = max(array(
            $this->payment_configuration->bank_account_id, 
            $this->payment_configuration->bank_account_date,
            $this->payment_configuration->bank_account_amount,
            $this->payment_configuration->account_number_col,
            $this->payment_configuration->account_owner_col,
            $this->payment_configuration->date_col,
            $this->payment_configuration->amount_col,
            $this->payment_configuration->amount2_col,
            $this->payment_configuration->payment_code_col,
            $this->payment_configuration->call_for_number_debit_col,
            $this->payment_configuration->call_for_number_credit_col,
            $this->payment_configuration->bank_transaction_id
        ));

        $i=0;
        $file = fopen($this->file_path,"r");
        while (($csv_row = fgetcsv($file, 0, $this->payment_configuration->separator)) !== false)
        {
            if(count($csv_row) <= $max_col)
            {
                return "invalid file - code: 7";
            }

            if($i!=0)
            {
                if($csv_row[$this->payment_configuration->bank_account_id] === null
                    || $csv_row[$this->payment_configuration->bank_account_date] === null
                    || $csv_row[$this->payment_configuration->bank_account_amount] === null
                    || $csv_row[$this->payment_configuration->account_number_col] === null
                    || $csv_row[$this->payment_configuration->account_owner_col] === null
                    || $csv_row[$this->payment_configuration->date_col] === null
                    || $csv_row[$this->payment_configuration->amount_col] === null
                    || $csv_row[$this->payment_configuration->amount2_col] === null
                    || $csv_row[$this->payment_configuration->payment_code_col] === null
                    || $csv_row[$this->payment_configuration->call_for_number_debit_col] === null
                    || $csv_row[$this->payment_configuration->call_for_number_credit_col] === null
                    || $csv_row[$this->payment_configuration->bank_transaction_id] === null)
                {
                    return "invalid file - code: 8";
                }
            }

            $i++;
        }

        fclose($file);
    }
    
    public function run()
    {
        set_time_limit(5);
        $number_of_empty_account_numbers = 0;
        $rowCounter=-1;
        $number_of_rows = SIMAMisc::numberOfRowsInCSV($this->file_path);
        $file = fopen($this->file_path,"r");
        while (($csv_row = fgetcsv($file, 0, $this->payment_configuration->separator)) !== false)
        {
            set_time_limit(1);
            $rowCounter++;
            if($rowCounter===0)
            {
                continue;
            }
            
            if(Yii::app()->isWebApplication())
            {
                Yii::app()->controller->sendUpdateEvent(($rowCounter/$number_of_rows)*100);
            }
            $bank_account_number = $csv_row[$this->payment_configuration->bank_account_id];
            $csv_date = $csv_row[$this->payment_configuration->bank_account_date];
            $number = $csv_row[$this->payment_configuration->bank_statement_number];
            $date = SIMAHtml::UserToDbDate($csv_date);

            try
            {
                $bank_statement = $this->createBankStatement($bank_account_number, $date, 0, $number);
                if(!empty($bank_statement))
                {
                    $account_number = $csv_row[$this->payment_configuration->account_number_col];
                    $account_owner = $csv_row[$this->payment_configuration->account_owner_col];
                    $payment_date = $csv_row[$this->payment_configuration->date_col];
                    $amount = $csv_row[$this->payment_configuration->amount_col];
                    $unreleased = $csv_row[$this->payment_configuration->amount2_col];
                    $payment_code=$csv_row[$this->payment_configuration->payment_code_col];
                    $bank_transaction_id=$csv_row[$this->payment_configuration->bank_transaction_id];
                    $call_for_number_modul = null;
                    $call_for_number_modul_partner = null;
                                        
                    if(empty($account_number))
                    {
                        $number_of_empty_account_numbers++;
                    }
                    
                    if(!isset($amount) || $amount=='')
                    {
                        $invoice = true;
                        $amount = isset($unreleased)?$unreleased:0;
                        $call_for_number=$csv_row[$this->payment_configuration->call_for_number_debit_col];
                        $call_for_number_partner=$csv_row[$this->payment_configuration->call_for_number_credit_col];
                    }
                    else
                    {
                        $invoice = false;
                        $call_for_number=$csv_row[$this->payment_configuration->call_for_number_credit_col];
                        $call_for_number_partner=$csv_row[$this->payment_configuration->call_for_number_debit_col];
                    }

                    $this->populateBankStatement(
                            $account_number, 
                            $account_owner, 
                            $payment_date,
                            $amount,
                            $payment_code,
                            $call_for_number,
                            $call_for_number_partner,
                            $this->payment_configuration->decimal_separator,
                            $bank_statement,
                            $this->payment_configuration->encoding,
                            $invoice,
                            $bank_transaction_id,
                            $call_for_number_modul, 
                            $call_for_number_modul_partner
                    );
                }
                
            } 
            catch(SIMAExceptionBankAccountNumberInvalid $ex)
            {
                Yii::app()->raiseNote(Yii::t(
                    'AccountingModule.BankStatement', 
                    'CouldNotCreateBankStatementInvalidNumber',
                    [
                        '{bank_account_number}' => $ex->bank_account_number,
                        '{date}' => $date,
                        '{amount}' => 0,
                        '{db_err_message}' => $ex->db_err_message
                    ]
                ));
            }
        }
        set_time_limit(5);
        fclose($file);
        
        if($number_of_empty_account_numbers > 0)
        {
            Yii::app()->raiseNote(Yii::t('AccountingModule.BankStatement', 'NumberOfEmptyStatementsThatWereSetBankForPartner', [
                '{number_of_empty_account_numbers}' => $number_of_empty_account_numbers
            ]));
        }
    }
}

