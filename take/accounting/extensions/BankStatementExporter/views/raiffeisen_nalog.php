<?php
$payment_date_datetime_obj = new DateTime($payment->payment_date);
$payment_date = $payment_date_datetime_obj->format('d.m.Y');

$payment_partner_city = '';
if(!is_null($payment->partner->MainAddress) && !is_null($payment->partner->MainAddress->city))
{
    $payment_partner_city = $payment->partner->MainAddress->city->DisplayName;
}
else
{
    $autobaf_message = 'partner nema adresu/grad za placanje: '.SIMAMisc::toTypeAndJsonString($payment);
    error_log(__METHOD__.' - '.$autobaf_message);
    Yii::app()->errorReport->createAutoClientBafRequest($autobaf_message);
}
?>

<Nalog ImeFajla="Nalog_<?=$order_num?>.XML">
    <Data>
        <SifraProizvodaCORE>501</SifraProizvodaCORE>
        <TransakcijaID/>
        <PlacanjeID/>
        <DatumPrijema/>
        <RedniBrojNaloga>1</RedniBrojNaloga>
        <MaticniBrojNalogodavca>41<?=Yii::app()->company->MB;?></MaticniBrojNalogodavca>
        <VasBrojNaloga/>
        <SifraPlacanja><?=$payment->payment_code->code?></SifraPlacanja>
        <SvrhaDoznake><?=$payment->payment_code->name?></SvrhaDoznake>
        <SifraValute><?=$payment->account->currency->code?></SifraValute>
        <Iznos><?=$payment->amount?></Iznos>
        <DatumValute><?=$payment_date?></DatumValute>
        <DatumValuteDo><?=$payment_date?></DatumValuteDo>
        <RacunNalogodavca><?php echo str_replace('-', '', $bank_account->number);?></RacunNalogodavca>
        <RacunKorisnika><?php echo str_replace('-', '', $payment->account->number);?></RacunKorisnika>
        <?php 
        if(empty($payment->call_for_number_modul))
        {
            ?><ModelNalogodavca/>
        <?php
        }
        else
        {
            echo '<ModelNalogodavca>'.$payment->call_for_number_modul.'</ModelNalogodavca>';
        }
        if(empty($payment->call_for_number))
        {
            ?><PozivNaBrojNalogodavca/><?php
        }
        else
        {
            ?><PozivNaBrojNalogodavca><?=$payment->call_for_number?></PozivNaBrojNalogodavca>
        <?php
        }
        if(empty($payment->call_for_number_modul_partner))
        {
            ?><ModelKorisnika/>
        <?php
        }
        else
        {
            ?><ModelKorisnika><?=$payment->call_for_number_modul_partner?></ModelKorisnika>
        <?php
        }
        if(empty($payment->call_for_number_partner))
        {
            ?><PozivNaBrojKorisnika/>
        <?php
        }
        else
        {
            ?><PozivNaBrojKorisnika><?=$payment->call_for_number_partner?></PozivNaBrojKorisnika>
<?php
        }
        ?>
        <NazivNalogodavca><?=Yii::app()->company->DisplayName?></NazivNalogodavca>
        <MestoNalogodavca><?=Yii::app()->company->MainAddress->city->DisplayName?></MestoNalogodavca>
        <NazivKorisnika><?=$payment->partner->DisplayName?></NazivKorisnika>
        <MestoKorisnika><?=$payment_partner_city?></MestoKorisnika>
        <BrojGrupnogNaloga/>
        <BrojZbirnogNaloga/>
        <Status/>
        <Prioritet>100</Prioritet>
    </Data>
</Nalog>