<Client>
    <PaketZaSlanje>
<?php
$counter = 0;
foreach($payments as $payment)
{
    $counter++;
    echo $controller->renderInternal(
        dirname(__FILE__).'/raiffeisen_nalog.php',
        [
            'order_num' => $counter,
            'payment' => $payment,
            'bank_account' => $bank_account
        ],
        true
    );
}
?>
    </PaketZaSlanje>
</Client>