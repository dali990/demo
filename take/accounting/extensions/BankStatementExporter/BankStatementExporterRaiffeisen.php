<?php

class BankStatementExporterRaiffeisen
{
    public $export_content = '';
    
    private $_bank_account = null;
    private $_payments = null;
    
    public function __construct(BankAccount $bankAccount, array $payments)
    {
        $this->_bank_account = $bankAccount;
        $this->_payments = $payments;
    }
    
    public function run()
    {
        try
        {
            $this->performValidations();
        }
        catch(Exception $e)
        {
            throw new SIMAWarnException(Yii::t('AccountingModule.BankStatementExporter', 'ThereWasError').':</br>'.$e->getMessage());
        }
        
        if(isset(Yii::app()->controller))
            $controller = Yii::app()->controller;
        else
            $controller = new CController('SIMAController');
        
        $this->export_content = $controller->renderInternal(
            dirname(__FILE__).'/views/raiffeisen_paket.php',
            [
                'payments' => $this->_payments,
                'bank_account' => $this->_bank_account,
                'controller' => $controller
            ],
            true
        );
    }
    
    private function performValidations()
    {
        foreach($this->_payments as $payment)
        {
            if(is_null($payment->partner->MainAddress))
            {
                throw new Exception(Yii::t('AccountingModule.BankStatementExporter', 'PartnerNotHaveMainAddress', [
                    '{partner}' => $payment->partner->DisplayName
                ]));
            }
            if(is_null($payment->partner->MainAddress->city))
            {
                throw new Exception(Yii::t('AccountingModule.BankStatementExporter', 'PartnersMainAddressNotHaveCity', [
                    '{partner}' => $payment->partner->DisplayName
                ]));
            }
        }
    }
}

