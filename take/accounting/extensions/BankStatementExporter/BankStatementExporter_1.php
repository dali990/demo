<?php

class BankStatementExporter
{
    public $export_content = '';
    
    private $_worker = null;
    
    public function __construct(BankAccount $bankAccount, array $payments)
    {
//        if($config->type==0) //CSV
//        {
//            $this->createBankStatementsCSV($real_path, $config);
//        }
//        else if($config->type==1) //XML
//        {
//            $this->createBankStatementsXML($real_path, $config);
//        }
//        else if($config->type == 2) //ErsteWeb
//        {
//            $this->createBankStatementsErsteWeb($real_path, $config);
//        }
//        else 
//        if($config->type == PaymentConfiguration::$TYPE_RAIFFEISEN_CSV)
//        {
            $this->_worker = new BankStatementExporterRaiffeisen($bankAccount, $payments);
//        }
//        else
//        {
//            throw new SIMAWarnException(Yii::t('AccountingModule.BankStatement', 'ChosenConfigurationTypeProblem'));
//        }
    }
    
    public function run()
    {
//        $this->_worker->validate();
        $this->_worker->run();
        $this->export_content = $this->_worker->export_content;
    }
}
