<?php

class AccountingModule extends SIMAModule
{
    public function init()
    {
        parent::init();
        /**
         * Register Behaviors
         */
        File::registerPermanentBehavior('FileAccountingBehavior');
        Year::registerPermanentBehavior('YearAccountingBehavior');
        Month::registerPermanentBehavior('MonthAccountingBehavior');
        Partner::registerPermanentBehavior('PartnerAccountingBehavior');
        Payment::registerPermanentBehavior('PaymentBehavior');
        FixedAsset::registerPermanentBehavior('FixedAssetAccountingBehavior');
        FixedAsset::registerPermanentBehavior('AutoCostLocationBehavior',['prefix' => 'FA']);
        FixedAssetType::registerPermanentBehavior('AutoCostLocationBehavior',[ 'prefix' => 'FAT', 'has_parent' => true ]);
        Theme::registerPermanentBehavior('ThemeAccountingBehavior');
        Company::registerPermanentBehavior('CompanyAccountingBehavior');
        Ios::registerPermanentBehavior('IosBehavior');
        FileSignature::registerPermanentBehavior('FileSignatureAccountingBehavior');
        KPR::registerPermanentBehavior('KPRBehavior');
        KIR::registerPermanentBehavior('KIRBehavior');

        // this method is called when the module is being created
        // you may place code here to customize the module or the application

        $this->setComponents(array(
//                'errorHandler' => array(
//                        'errorAction' => 'module/default/error'),
//                'defaultController' => 'files',
//                'user' => array(
//                        'class' => 'ModuleWebUser',
//                        'allowAutoLogin'=>true,
//                        'loginUrl' => Yii::app()->createUrl('module/default/login'),
//                )
        ));
        Yii::app()->setParams(include(dirname(__FILE__).'/config/main.php'));
//
//            // import the module-level models and components or any other components..
//            $this->setImport(array(
//                'module.models.*',
//                'module.components.*',
//            ));
        
        POPDVBillLists::init();
    }

    public function getBaseUrl()
    {
        return Yii::app()->assetManager->publish(Yii::getPathOfAlias('accounting.assets'));
    }

    public function registerManual()
    {
        $urlScript = Yii::app()->assetManager->publish(Yii::getPathOfAlias('accounting.assets'));
//        $uniq = SIMAHtml::uniqid();

//        $component_dir = dirname(__FILE__).'/vueGUI/models/File/TabBillItems';
//        Yii::app()->clientScript->registerScript(SIMAHtml::uniqid(),file_get_contents($component_dir.'/TabBillItems.js'),CClientScript::POS_READY);
//        Yii::app()->clientScript->registerScript(SIMAHtml::uniqid(),file_get_contents($component_dir.'/TabBillItemsItem.js'),CClientScript::POS_READY);
//        Yii::app()->clientScript->registerScript(SIMAHtml::uniqid(),include($component_dir.'/TabBillItems.php'),CClientScript::POS_END);
//        Yii::app()->clientScript->registerScript(SIMAHtml::uniqid(),include($component_dir.'/TabBillItemsItem.php'),CClientScript::POS_END);
////        Yii::app()->clientScript->registerCssFile($component_dir.'/TabBillItems.css');
////        Yii::app()->clientScript->registerCssFile($component_dir.'/TabBillItemsItem.css');

        Yii::app()->clientScript->registerCssFile($urlScript . '/css/default.css');
        Yii::app()->clientScript->registerScriptFile($urlScript .'/js/simaAccounting.js');

        Yii::app()->clientScript->registerScriptFile($urlScript .'/js/simaAccountingMisc.js' , CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerScriptFile($urlScript .'/js/simaAccountingUnreleasing.js' , CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerScriptFile($urlScript .'/js/simaPPPPD.js' , CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerScriptFile($urlScript .'/js/simaAccountingFixedAssets.js' , CClientScript::POS_HEAD);

        Yii::app()->clientScript->registerScript(
                SIMAHtml::uniqid(),
                "sima.accountingMisc = new SIMAAccountingMisc();", 
                CClientScript::POS_READY);

        Yii::app()->clientScript->registerScript(
                SIMAHtml::uniqid(),
                "sima.accounting = new SIMAAccounting();", 
                CClientScript::POS_READY);
        
        Yii::app()->clientScript->registerScript(
                SIMAHtml::uniqid(),
                "sima.ppppd = new SIMAPPPPD();", 
                CClientScript::POS_READY);
        Yii::app()->clientScript->registerScript(
                SIMAHtml::uniqid(),
                "sima.accountingFixedAssets = new SIMAAccountingFixedAssets();", 
                CClientScript::POS_READY);
    }
    
    public function getGuiTableDefinition($index)
    {
        switch($index)
        {
            case '0': return [
                'label' => 'Izvodi iz banke',
                'settings' => array(
                    'model' => PaymentConfiguration::model(),
                    'add_button' => true,
                    'custom_buttons' => [
                        Yii::t('BaseModule.Misc','TakeDataFromCodeBook') => [
                            'func' => 'sima.adminMisc.syncPaymentConfigurationsFromCodebook(this)',
                        ],
                    ],
                )
            ];
            case '1': return [
                'label' => 'Admin - Radni nalozi',
                'settings' => array(
                    'model' => CostLocation::model(),
                    'add_button' => true
                )
            ];
            case '2': 
            
                $document_type_bill_of_excange_id = Yii::app()->configManager->get('accounting.bill_of_exchange', false);
                
                $add_button = true;
                if (!empty($document_type_bill_of_excange_id))
                {
                    $add_button = [
                        'init_data' => [
                            'File' => [
                                'document_type' => [
                                    'ids' => $document_type_bill_of_excange_id
                                ],
                            ]
                        ]
                    ];
                }
                
                return [
                    'label' => 'Finansije - Menice',
                    'settings' => array(
                        'model' => BillOfExchange::model(),
                        'add_button' => $add_button
                    )
                ];
            case '3': return [
                'label' => 'Finansije - Racuni',
                'settings' => array(
                    'model' => BillIn::model(),
                    'columns_type' => 'confirm1',
                    'fixed_filter' => array(
                        'confirm1' => 'false',
                    )
                )
            ];
            case '4': return [
                'label' => Yii::t('BaseModule.GuiTable', 'GivenAdvances'),
                'settings' => array(
                    'model' => AdvanceOut::model(),
                )
            ];
            case '5': return [
                'label' => 'Finansije - Primljeni avansi',
                'settings' => array(
                    'model' => AdvanceIn::model(),
                )
            ];
            case '6': return [
                'label' => 'Finansije - Placanja',
                'settings' => [
                    'model' => Payment::model(),
                    'fixed_filter' => [
                        'filter_scopes' => 'payed'
                    ]
                ]
            ];
            case '7':
                $uniq = SIMAHtml::uniqid();
                return [
                'label' => 'Finansije - Izvodi',
                'settings' => array(
                    'id' => $uniq,
                    'model' => BankStatement::model(),
                    'add_button' => array(
                        'button1' => true,
                        'button2' => array(
                            'onhover' => 'Dodaj preko e-banking',
                            'action' => 'sima.accounting.multidateBankStatementForm("' . $uniq . '")'
                        )
                    )
                )
            ];
            case '8': 
                $document_type_unrelief_bill = Yii::app()->configManager->get('accounting.bills.unrelief_bill', false);
                $document_type_unrelief_bill_id = array();
                if(isset($document_type_unrelief_bill))
                {
                    if(is_array($document_type_unrelief_bill))
                    {
                        if( count($document_type_unrelief_bill) > 0)
                        {
                            $document_type_unrelief_bill_id = array(
                                'init' => $document_type_unrelief_bill[0],
                                'disabled'
                            );
                        }
                    }
                    else
                    {
                        $document_type_unrelief_bill_id = array(
                            'init' => $document_type_unrelief_bill,
                            'disabled'
                        );
                    }
                }
                
                return [
                    'label' => 'Izlazna knjižna zaduženja',
                    'settings' => [
                        'model' => UnReliefBillOut::model(),
                        'add_button' => [
                            'init_data' => [
                                'File' => [
                                    'document_type_id' => $document_type_unrelief_bill_id,
                                ]
                            ]
                        ]
                    ]
                ];
            case '9': 
                $document_type_relief_bill = Yii::app()->configManager->get('accounting.bills.relief_bill', false);
                $document_type_relief_bill_id = array();
                if(isset($document_type_relief_bill))
                {
                    if(is_array($document_type_relief_bill))
                    {
                        if( count($document_type_relief_bill) > 0)
                        {
                            $document_type_relief_bill_id = array(
                                'init' => $document_type_relief_bill[0],
                                'disabled'
                            );
                        }
                    }
                    else
                    {
                        $document_type_relief_bill_id = array(
                            'init' => $document_type_relief_bill,
                            'disabled'
                        );
                    }
                }
                
                return [
                    'label' => 'Izlazna knjižna odobrenja',
                    'settings' => array(
                        'model' => ReliefBillOut::model(),
                        'add_button' => array(
                            'init_data' => array(
                                'File' => array(
                                    'document_type_id' => $document_type_relief_bill_id,
                                )
                            )
                        )
                    )
                ];
            case '10': 
                $document_type_pre_bill = Yii::app()->configManager->get('accounting.bills.pre_bill', false);
                $document_type_pre_bill_id = [];
                if(isset($document_type_pre_bill))
                {
                    if(is_array($document_type_pre_bill))
                    {
                        if( count($document_type_pre_bill) > 0)
                        {
                            $document_type_pre_bill_id = array(
                                'init' => $document_type_pre_bill[0],
                                'disabled'
                            );
                        }
                    }
                    else
                    {
                        $document_type_pre_bill_id = array(
                            'init' => $document_type_pre_bill,
                            'disabled'
                        );
                    }
                }

                return [
                    'label' => 'Finansije - Izlazni predracuni',
                    'settings' => array(
                        'model' => PreBillOut::model(),
                        'add_button' => array(
                            'init_data' => array(
                                'File' => array(
                                    'document_type_id' => $document_type_pre_bill_id,
                                    'responsible_id' => Yii::app()->user->id
                                )
                            )
                        )
                    )
                ];
            case '11':
                $document_type_advance_bill = Yii::app()->configManager->get('accounting.bills.advance_bill', false);
                $document_type_advance_id = array();
                if (isset($document_type_advance_bill)) {
                    if (is_array($document_type_advance_bill)) {
                        if (count($document_type_advance_bill) > 0) {
                            $document_type_advance_id = array(
                                'init' => $document_type_advance_bill[0],
                                'disabled'
                            );
                        }
                    } else {
                        $document_type_advance_id = array(
                            'init' => $document_type_advance_bill,
                            'disabled'
                        );
                    }
                }
                
                return [
                    'label' => 'Finansije - Avansne fakture',
                    'settings' => array(
                        'model' => AdvanceBillOut::model(),
                        'add_button' => array(
                            'init_data' => array(
                                'File' => array(
                                    'document_type_id' => $document_type_advance_id,
                                    'responsible_id' => Yii::app()->user->id
                                )
                            )
                        )
                    )
                ];
                
            case '12': 
                $document_type_bill = Yii::app()->configManager->get('accounting.bills.bill', false);
                $document_type_id = array();
                if(isset($document_type_bill))
                {
                    if(is_array($document_type_bill))
                    {
                        if( count($document_type_bill) > 0)
                        {
                            $document_type_id = array(
                                'init' => $document_type_bill[0],
                                'disabled'
                            );
                        }
                    }
                    else
                    {
                        $document_type_id = array(
                            'init' => $document_type_bill,
                            'disabled'
                        );
                    }
                }

                return [
                    'label' => 'Finansije - Fakture',
                    'settings' => array(
                        'model' => BillOut::model(),
                        'add_button' => array(
                            'init_data' => array(
                                'File' => array(
                                    'document_type_id' => $document_type_id,
                                    'responsible_id' => Yii::app()->user->id
                                )
                            )
                        ),
                        'fixed_filter'=>[
                            'filter_scopes' => ['notRecurring']//MilosS(17.10.2017): ovde bi pre trablo da ide oni koji nisu automatski generisani, ali moze i ovako
                        ]
                    )
                ];
            case '14':
            case '15':
            case '16':
            case '17': 
            case '18': 
                $id = null;
                $document_type_bill = null;
                $model = null;
                $label = null;
                $additional_options_html = '';
                if($index === '14')
                {
                    $label = 'Ulazna knjižna zaduženja';
                    $document_type_bill = Yii::app()->configManager->get('accounting.bills.unrelief_bill', false);
                    $model = UnReliefBillIn::class;
                }
                else if($index === '15')
                {
                    $label = 'Ulazna knjižna odobrenja';
                    $document_type_bill = Yii::app()->configManager->get('accounting.bills.relief_bill', false);
                    $model = ReliefBillIn::class;
                }
                else if($index === '16')
                {
                    $label = 'Finansije - Ulazni predracuni';
                    $document_type_bill = Yii::app()->configManager->get('accounting.bills.pre_bill', false);
                    $model = PreBillIn::class;
                }
                else if($index === '17')
                {
                    $label = 'Finansije - Avansni računi';
                    $document_type_bill = Yii::app()->configManager->get('accounting.bills.advance_bill', false);
                    $model = AdvanceBillIn::class;
                }
                else if($index === '18')
                {
                    $id = SIMAHtml::uniqid().'_gta18';
                    $label = 'Finansije - Računi';
                    $document_type_bill = Yii::app()->configManager->get('accounting.bills.bill', false);
                    $model = BillIn::class;
                    $today_day = Day::get();
                    $today_date = $today_day->day_date;
                    $day_before_today_date = $today_day->prev()->day_date;
                    $day_15_days_before_today_date = $today_day->modify('-15 day')->day_date;
                    $min_date = '20.12.2003.';
                    $max_date = '20.12.2033.';
                    $additional_options_html = Yii::app()->controller->widget(SIMAButtonVue::class, [
                        'title' => 'Starosna struktura duga',
                        'subactions_data' => [
                            [
                                'title' => 'svi koji kasne',
                                'onclick'=>['sima.callPluginMethod', '#'.$id, 'simaGuiTable', 'setSelectFilter', [
                                    'unreleased_amount' => '>SEPARATOR0.00',
                                    'payment_date' => $min_date.'<>'.$day_before_today_date
                                ], true]
                            ],
                            [
                                'title' => 'svi koji kasne do 15 dana',
                                'onclick'=>['sima.callPluginMethod', '#'.$id, 'simaGuiTable', 'setSelectFilter', [
                                    'unreleased_amount' => '>SEPARATOR0.00',
                                    'payment_date' => $day_15_days_before_today_date.'<>'.$day_before_today_date
                                ], true]
                            ],
                            [
                                'title' => 'svi koji kasne preko 15 dana',
                                'onclick'=>['sima.callPluginMethod', '#'.$id, 'simaGuiTable', 'setSelectFilter', [
                                    'unreleased_amount' => '>SEPARATOR0.00',
                                    'payment_date' => $min_date.'<>'.$day_15_days_before_today_date
                                ], true]
                            ],
                            [
                                'title' => 'svi u valuti',
                                'onclick'=>['sima.callPluginMethod', '#'.$id, 'simaGuiTable', 'setSelectFilter', [
                                    'unreleased_amount' => '>SEPARATOR0.00',
                                    'payment_date' => $today_date.'<>'.$max_date
                                ], true]
                            ]
                        ]
                    ], true, true);
                }
                $add_bill_in_without_filing_book = Yii::app()->configManager->get('accounting.add_bill_in_without_filing_book', false);
                $document_type_id = array();
                if(isset($document_type_bill))
                {
                    if(is_array($document_type_bill))
                    {
                        if( count($document_type_bill) > 0)
                        {
                            $document_type_id = array(
                                'init' => $document_type_bill[0],
                                'disabled'
                            );
                        }
                    }
                    else
                    {
                        $document_type_id = array(
                            'init' => $document_type_bill,
                            'disabled'
                        );
                    }
                }
                return [
                    'label' => $label,
                    'settings' => array(
                        'id' => $id,
                        'model' => $model,
                        'add_button' => ($add_bill_in_without_filing_book) ? array(
                            'init_data' => array(
                                'File' => array(
                                    'document_type_id' => $document_type_id,
                                    'responsible_id' => Yii::app()->user->id
                                )
                            )
                                ) : false,
                        'additional_options_html' => $additional_options_html
                    )
                ];
            case '19': return [
                'label' => Yii::t('BaseModule.GuiTable', 'MiddleCurrencyRates'),
                'settings' => array(
                    'model' => MiddleCurrencyRate::model(),
                    'add_button' => true,
                    'fixed_filter' => [
                        'order_by' => 'date DESC'
                    ]
                )
            ];
            case '20': return [
                'label' => 'Finansije - Stornirano',
                'settings' => array(
                    'model' => BillCanceled::model()
                )
            ];
            case '21': return [
                'label' => 'Mesta troška',
                'settings' => array(
                    'add_button' => true,
                    'model' => CostLocation::model(),
                    'fixed_filter' => [
                        'scopes' => ['withAllNames']
                //                    'expiration_date_of_insurance'=>['<',SIMAHtml::UserToDbDate(date('Y-m-d', strtotime("+1 week")))]
                    ]
                ),
            ];
            case '22': return [
                'label' => Yii::t('BaseModule.GuiTable', 'PaymentTypes'),
                'settings' => array(
                    'model' => PaymentType::model(),
                    'add_button' => true
                )
            ];
            case '23': return [
                'label' => 'Šifre plaćanja',
                'settings' => array(
                    'model' => PaymentCode::model(),
                    'add_button' => true,
                    'custom_buttons' => [
                        Yii::t('BaseModule.Misc','TakeDataFromCodeBook') => [
                            'func' => 'sima.adminMisc.syncPaymentCodesFromCodebook(this)',
                        ],
                    ],
                )
            ];
            case '24': return [
                'label' => 'Banke',
                'settings' => array(
                    'model' => Bank::model(),
                    'add_button' => true,
                    'custom_buttons' => [
                        Yii::t('BaseModule.Misc','TakeDataFromCodeBook') => [
                            'func' => 'sima.adminMisc.syncBanksFromCodebook(this)',
                        ],
                    ],
                )
            ];
            case '25':
                $cid = isset(Yii::app()->company) ? Yii::app()->company->id : -1;
                return [
                    'label' => 'Finansije - Ziro racuni',
                    'settings' => array(
                        'model' => BankAccount::model(),
                        'columns_type' => 'confirm1',
                        'add_button' => array(
                            'init_data' => array(
                                'BankAccount' => array('partner_id' => $cid)
                            )
                        ),
                        'fixed_filter' => array(
                            'partner_id' => $cid
                        )
                    )
                ];
            case '26': return [
                'label' => 'Bankarske garancije',
                'settings' => array(
                    'model' => BankGuarantee::model(),
                    'add_button' => [
                        'init_data' => [
                            'File' => array(
                                'responsible_id' => Yii::app()->user->id
                            )
                        ]
                    ]
                )
            ];
            case '27': return [
                'label' => 'Finansijski meseci',
                'settings' => array(
                    'model' => AccountingMonth::model(),
                    'add_button' => true,
                    'fixed_filter' => [
                        'order_by' => 'display_name DESC'
                    ]
                ),
            ];
            case '28': return [
                'label' => Yii::t('BaseModule.GuiTable', 'NonConnectedAdvanceBills'),
                'settings' => array(
                    'model' => AdvanceBill::model(),
                    'fixed_filter' => [
                        'connected' => false
                    ]
                )
            ];
            case '29':
                return [
                    'label' => Yii::t('AccountingModule.Warns','DuplicatedBankAccounts'),
                    'settings' => [
                        'model' => BankAccount::model(),
                        'add_button' => false,
                        'fixed_filter' => [
                            'display_scopes' => 'byNumberAsc',
                            'filter_scopes' => 'onlyDuplicated'
                        ]
                    ]
                ];
            case '30':
                return [
                    'label' => RecurringBillGUI::modelLabel(true),
                    'settings' => [
                        'model' => RecurringBill::model(),
                        'add_button' => true
                    ]
                ];
            case '31':
                return [
                    'label' => Yii::t('AccountingModule.Warns','BillDocumentsWithNoSignature'),
                    'settings' => [
                        'model' => Bill::model(),
                        'columns_type' => 'no_signatures',
                        'add_button' => false,
                        'fixed_filter' => [
                            'scopes' => ['withNoSignature', 'recently'],
                        ]
                    ]
                ];
            case '32':
                return [
                    'label' => Yii::t('AccountingModule.NoteTaxExemption', 'NotesOfTaxExemption'),
                    'settings' => [
                        'model' => NoteTaxExemption::model(),
                        'add_button' => true
                    ]
                ];
            case '33': 
//                $document_type_unrelief_bill = Yii::app()->configManager->get('accounting.bills.unrelief_bill', false);
//                $document_type_unrelief_bill_id = [];
//                if(isset($document_type_unrelief_bill))
//                {
//                    if(is_array($document_type_unrelief_bill))
//                    {
//                        if( count($document_type_unrelief_bill) > 0)
//                        {
//                            $document_type_unrelief_bill_id = array(
//                                'init' => $document_type_unrelief_bill[0],
//                                'disabled'
//                            );
//                        }
//                    }
//                    else
//                    {
//                        $document_type_unrelief_bill_id = array(
//                            'init' => $document_type_unrelief_bill,
//                            'disabled'
//                        );
//                    }
//                }
                
                return [
                    'label' => Yii::t('AccountingModule.DebtTakeOver','DebtTakeOvers'),
                    'settings' => [
                        'model' => DebtTakeOver::model(),
                        'add_button' => [
//                            'init_data' => [
//                                'File' => [
//                                    'document_type_id' => $document_type_unrelief_bill_id,
//                                ]
//                            ]
                        ]
                    ]
                ];
            default:
                throw new SIMAGuiTableIndexNotFound('accounting', $index);
        }
    }
} 