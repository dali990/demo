<?php

class IncomeCostRefreshCommand extends SIMACommand
{
    public function performWork($args)
    {
        $error_messages = [];
        
        Yii::app()->db->createCommand(
            'REFRESH MATERIALIZED VIEW accounting.cost_location_parent_child;' 
        )->execute();
        
        return $error_messages;
    }
}
