<?php

class ValidatePaycheckXMLAndPPPPDXMLCommand extends SIMACommand
{
    public function performWork($args)
    {
        $error_messages = [];
        
        PaycheckPeriod::model()->findAllByParts(function($paycheck_periods){
            foreach($paycheck_periods as $paycheck_period)
            {
                $xml_file = $paycheck_period->xml_file;
                $sima_generated_ppppd_file = null;

                $ppppds = $paycheck_period->ppppds;
                foreach($ppppds as $ppppd)
                {
                    if($ppppd->creation_type === PPPPD::$CREATE_TYPE_SIMA_GENERATE)
                    {
                        $sima_generated_ppppd_file = $ppppd->file;
                        break;
                    }

                }

                if(empty($xml_file))
                {
                    error_log(__METHOD__.' - isplata: '.$paycheck_period->DisplayName.' - nema xml fajl');
                    continue;
                }
                if(empty($sima_generated_ppppd_file))
                {
                    error_log(__METHOD__.' - isplata: '.$paycheck_period->DisplayName.' - nema generisani ppppd fajl');
                    continue;
                }

                $xml_file_temp = TemporaryFile::CreateFromFile($xml_file);
                $sima_generated_ppppd_file_temp = TemporaryFile::CreateFromFile($sima_generated_ppppd_file);

                if(
                        $xml_file_temp->getFileSize() !== $sima_generated_ppppd_file_temp->getFileSize()
                        || $xml_file_temp->getMD5() !== $sima_generated_ppppd_file_temp->getMD5()
                )
                {
                    error_log(__METHOD__.' - razlikuju se za isplatu: '.$paycheck_period->DisplayName);
                }
            }
        });
        
        return $error_messages;
    }
}
