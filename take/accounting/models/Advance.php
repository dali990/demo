<?php

class Advance extends Payment {

    public $vat;

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function beforeSave() 
    {
        $this->payment_type_id = Yii::app()->configManager->get('accounting.advance_payment_type');

        return parent::beforeSave();
    }

    public function defaultScope() 
    {
        $alias = $this->getTableAlias(FALSE, FALSE);
        $pt_id = Yii::app()->configManager->get('accounting.advance_payment_type');
        return array(
            'condition' => "$alias.payment_type_id = $pt_id",
        );
    }

    public function relations($child_relations = [])
    {
        $bills_table = Bill::model()->tableName();
        $adv_bill = Bill::$ADVANCE_BILL;
        return parent::relations(array_merge([
            'advance_releases' => array(self::HAS_MANY, 'BillRelease', 'payment_id',
                    'condition' => "bill_id in (select id from $bills_table where bill_type = '$adv_bill')"
            ),
        ],$child_relations));
    }

    public function __get($column) {
        switch ($column) {
            case 'advance_bill': return (count($this->advance_releases) == 0) ? 'nema' :
                        CHtml::link($this->advance_releases[0]->bill->getAttributeDisplay('bill_number'));
            default: return parent::__get($column);
        }
    }

}