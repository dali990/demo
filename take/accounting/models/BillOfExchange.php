<?php
/**
 * Description of BillOfExchange
 *
 * @author goran-set
 */
class BillOfExchange extends SIMAActiveRecord
{   
    public $file_version_id;
    public $file_id;
    
    public $belongs;
    public $belongs_recursive;
    
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'accounting.bills_of_exchange';
    }

    public function moduleName()
    {
        return 'accounting';
    }
    
    public function __get($column) {
        switch ($column)
        {
           case 'path2'    :   return 'files/file';
           case 'DisplayName': return ($this->file->has_filing) ? $this->file->filing->filing_number . ' od ' . $this->file->filing->date : $this->file->DisplayName;
           case 'SearchName':return $this->DisplayName;
           default: return parent::__get($column);
        }
    }
    
    public function rules()
    {
        return array(
            array('number', 'required'),
            array('begin_date,end_date, number, input,authorization_file_id,partner_id, status,description', 'safe'),
//            array('status','checkStatus'), //privremeno dok se ne srede menice
            array('status','checkBeginDate'),
            array('authorization_file_id, partner_id,begin_date, end_date','default',
                'value'=>null,
                'setOnEmpty'=>true,'on'=>array('insert','update'))
        );
    }
    
    public function relations($child_relations = [])
    {
        return array(
            'file' => array(self::BELONGS_TO, 'File', 'id'),
            'authorization_file' => array(self::BELONGS_TO, 'File', 'authorization_file_id'),
            'partner' => array(self::BELONGS_TO, 'Partner', 'partner_id'),
        );
    }

    public function checkStatus($attribute, $params)
    {        
        if(isset($this->status) && $this->status!=null &&  $this->__old!=null && $this->__old['status']!=$this->status)
        {
           
            if($this->__old['status']==0 && $this->status!=1)
            {
                $this->addError('status', 'Status "Nova" moguće je promeniti isključivo u "Izdata"!');
            }
            
            if($this->__old['status']==1 && $this->status!=2 && $this->status!=3)
            {
                $this->addError('status', 'Status "Izdata" moguće je promeniti u "Vraćena" ili "Aktivirana"!');
            }
        }
    }
    
    public function checkBeginDate($attribute, $params)
    {
        if($this->status!=0 && (!isset($this->begin_date) || $this->begin_date==null))
        {
            $this->addError('begin_date', 'Morate uneti datum izdavanja!');
        }
    }
    
    public function scopes() {
        return array(
            'byName' => array(
                'order' => 'begin_date desc',
            ),
        );
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'textSearch': return array(
                'number' => 'text'
            );
            case 'options':  return array('form', 'open');
            
            case 'filters': return array(
                'status'=>'dropdown',
                'number' => 'text',
                'begin_date' => 'date_range',
                'end_date' => 'date_range',
                'partner' => 'relation',
                'description'=>'text',
                'input'=>'boolean',
                'belongs'=>array('belongs','func'=>'filter_belongs'),
            );
            case 'mutual_forms' : return [
                'base_relation' => 'file'
            ];
            default: return parent::modelSettings($column);
        }
    }
    
    public function filter_belongs($condition)
    {
        if ($this->belongs != null) 
        {
            $query = $this->belongs;
            if ($this->belongs_recursive === 'true')
                $query.='$R';
            $condition->mergeWith(SIMAFileStorage::applyQuery($query));
        }
    }
    
    public function beforeMutualSave()
    {
        $this->file->name = $this->number;
        
        return parent::beforeMutualSave();
    }
    
    public function afterSave()
    {
        parent::afterSave();
        
        if (isset($this->authorization_file))
        {
            $connection = ConnectedFile::model()->findByAttributes([
                'connected_file_id' => $this->id,
                'connected_to_file_id' => $this->authorization_file_id
            ]);
            if (is_null($connection))
            {
                $connection = new ConnectedFile();
                $connection->connected_file_id = $this->id;
                $connection->connected_to_file_id = $this->authorization_file_id;
                $connection->save();
            }
        }
        
        
        if ($this->__old['authorization_file_id']!=$this->authorization_file_id 
                && isset($this->authorization_file) 
                && isset($this->authorization_file->filing) 
                )
        {
            $link = new FilingContrib();
            $link->note_id = $this->authorization_file->id;
            $link->contrib_id = $this->id;
            $link->save();
            
            if (isset($this->__old['authorization_file_id']) 
                    && $this->__old['authorization_file_id']!='' 
                            && $this->__old['authorization_file_id']!=NULL)
                            
            
            FilingContrib::model()->deleteByPk(array(
                'note_id'=>($this->__old['authorization_file_id']),
                'contrib_id' => $this->id
            ));
        }
    }
}
