<?php

class BillOut extends Bill
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    
    public function init()
    {
        parent::init();
        $this->bill_type = self::$BILL;
        $this->invoice = true;
        $this->lock_amounts = false;
    }

    public function defaultScope()
    {
        $alias = $this->getTableAlias(FALSE, FALSE);
        $bill_type = Bill::$BILL;
        return array(
            'condition' => "$alias.invoice=true and $alias.bill_type = '$bill_type' and $alias.canceled=false",
        );
    }
    
    public function relations($child_relations = [])
    {
        return parent::relations(array_merge([
            'sales_order' => [self::HAS_ONE,'SalesOrder','bill_out_id'],
            'cost_type' => array(self::BELONGS_TO, 'CostType', 'cost_type_id', 'condition' => "cost_or_income = 'INCOME'")
        ],$child_relations));
    }
    
    public function beforeSave()
    {
        if (empty($this->payment_date))
        {
            $this->payment_date = $this->income_date;
        }
        
        return parent::beforeSave();
    }
}