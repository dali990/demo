<?php

class Account extends SIMAActiveRecord
{
    public $debit_start;
    public $credit_start;
    public $debit_current;
    public $credit_current;
    public $debit_sum;
    public $credit_sum;
    public $start_date = 'null';
    public $end_date = 'null';
    public $single_company_accounts = null;
    public $only_buyers = null;
    public $only_suppliers = null;
    
    private $_model = -1;
    
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'accounting.accounts';
    }

    public function moduleName()
    {
        return 'accounting';
    }

    public function __get($column)
    {
        switch ($column)
        {
            case 'path2': return 'accounting/books';
            case 'DisplayName': 
                $analitic_size = Yii::app()->params['accounting_account_analitic_size'];
                $_code =  (!empty($this->parent) && strlen($this->code) > $analitic_size)  ?  $this->parent->code  :  $this->code;

                $_name = (strpos($this->name, $_code.' - ') !== false)  ? substr($this->name, strlen($_code.' - ')) : $this->name;
//                $_name = $this->name;
                return $_code.' - '.$_name;
            case 'SearchName': return $this->DisplayName . (($this->description != '')?'(' . $this->description . ')':'');
//            case 'saldo':
//                return $this->debit_sum - $this->credit_sum;
            case 'hasMaxCode':
                $analitic_size = intval(Yii::app()->params['accounting_account_analitic_size']);
                return strlen($this->code)=== $analitic_size; 
            case 'isUnclassifiedLeaf':
                $analitic_size = intval(Yii::app()->params['accounting_account_analitic_size']);
                return (strlen($this->code)>$analitic_size && empty($this->model_id));
                    
            default: return parent::__get($column);
        }
    }
    
    public function afterFind()
    {
        parent::afterFind();
        $this->debit_sum = $this->debit_start + $this->debit_current;
        $this->credit_sum = $this->credit_start + $this->credit_current;
    }
    
    //TODO:DC
    public function getsaldo($time = null)
    {
        return $this->debit_sum - $this->credit_sum;
    }
    
    //TODO:DC
    public function getSaldoWithoutChildren()
    {
        return ($this->debit_start + $this->debit_current_without_children) - ($this->credit_start + $this->credit_current_without_children);
    }
    
    public function getStartAccountTransaction()
    {
        $_at = null;
        if (isset( $this->year->accounting->start_booking_order))
        {
            $_at = AccountTransaction::model()->findByModelFilter([
                'account' => ['ids' => $this->id],
                'account_document' => [
                    'account_order' => ['ids' => $this->year->accounting->start_booking_order_id]
                ]
            ]);
        }
        return $_at;
    }
    
    /**
     * Za taj konto i sva pod konta
     * @return type
     */
//    public function getSaldo()
//    {
//        $saldo = $this->debit_start - $this->credit_start;
//        if (count($this->children) === 0)
//        {
//            foreach ($this->account_transactions as $transaction)
//            {
//                $saldo += $transaction->debit - $transaction->credit;
//            }
//        }
//        else
//        {
//            foreach ($this->children as $child)
//            {
//                $saldo += $child->getSaldo();
//            }
//        }
//        
//        return $saldo;
//    }

    public function relations($child_relations = [])
    {
//        $alias = $this->getTableAlias();
        return array(
            'parent' => array(self::BELONGS_TO, 'Account', 'parent_id'),
            'children'=>array(self::HAS_MANY, 'Account', 'parent_id'),            
            'children_count'=>array(self::STAT, 'Account', 'parent_id', 
                'select' => 'count(*)'
            ),            
            'debit_current_without_children'=>array(self::STAT, 'AccountTransaction', 'account_id', 'select'=>'sum(debit)'),
            'credit_current_without_children'=>array(self::STAT, 'AccountTransaction', 'account_id', 'select'=>'sum(credit)'),
            'account_transactions'=>array(self::HAS_MANY, 'AccountTransaction', 'account_id'),
            'account_transactions_count'=>array(self::STAT, 'AccountTransaction', 'account_id', 
                'select' => 'count(*)'
            ),
            'account_transactions_with_saldo'=>array(self::HAS_MANY, 'AccountTransaction', 'account_id', 'scopes'=>'withSaldoAndNoStart'),
            'year' => array(self::BELONGS_TO, 'Year', 'year_id'),
            'cost_location' => array(self::BELONGS_TO, 'CostLocation', 'model_id',
//                'condition' => "cost_location.model_name = 'CostLocation'"
            ),
//            'bank_statements' => array(self::HAS_MANY, 'BankStatement', 'account_id'),     
            'accounting_year' => [self::BELONGS_TO, 'AccountingYear', 'year_id'],
        );
    }
    
    public function rules()
    {
        return array(
            array('code, year_id', 'required', 'message' => 'Polje "{attribute}" mora biti uneto'),
            array('debit_start, credit_start, code', 'numerical', 'message' => 'Polje "{attribute}" mora biti broj!'),
            array('parent_id, description, debit_sum, credit_sum, name', 'safe'),
            array('code', 'checkCode', 'on' => array('insert', 'update')),
            array('code', 'checkCodeDelete', 'on' => array('delete')),
            array('credit_start, debit_start', 'default',
                'value' => 0,
                'setOnEmpty' => true, 'on' => array('insert', 'update')),
            array('parent_id, year_id', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => array('insert', 'update')),            
        );
    }    
    
    
    
//    public function checkCode($attribute, $params) 
    public function checkCode() 
    {        
        if ($this->__old['code'] != $this->code || $this->__old['year_id'] != $this->year_id)
        {
            $account = Account::model()->findByAttributes(array('code'=>$this->code, 'year_id'=>$this->year_id));
            if  ($account !== null && $account->id != $this->id) 
            {
                $this->addError('code', 'Već postoji ovaj konto za ovu godinu!');
            }
        }
        $max_code_length = Yii::app()->params['accounting_account_analitic_size'];        
        if (empty($this->model_id))
        {
            if  (!(//negacija za sve -> znaci ako nije zadovoljen sledeci uslov
                    (strlen($this->code) <= $max_code_length) //kod je manji ili jednak od maksimalne duzine
                    || //ili
                    //kod je jednak tacno maksimalnoj duzini + 1 ukoliko mu je poslednji $max + 1 jednak 0
                    (strlen($this->code) === ($max_code_length+1) && ($this->code[$max_code_length]==="0") )

                ))
            {            
                $this->addError('code', "Maksimalna duzina koda je $max_code_length! - ($this->code)");
            }
        }
    }
    
    public function hasConnection()
    {
        return class_exists($this->model_name);
    }
    
//    public function checkCodeDelete($attribute, $params)
    public function checkCodeDelete()
    {        
        $can_delete = true;
        foreach ($this->children as $child)
        {
            $child->setScenario('delete');
            if (!$child->validate())
            {
                $can_delete = false;
            }
        }
        if(
                !($this->isUnclassifiedLeaf && $this->parent->children_count === 1) //ako je ostao samo unclasified, moze da se obrise
                &&
                ($this->account_transactions_count !== 0 || intval($this->debit_start) !== 0 || intval($this->credit_start) !== 0 || !$can_delete)
            )      
        {            
            throw new SIMAWarnException('Ne može da se obriše konto jer ima knjiženja!');
        }
            
    }
    
    public function scopes()
    {
        $alias = $this->getTableAlias();
        $max_code_length = Yii::app()->params['accounting_account_analitic_size'];
        $account_table = Account::model()->tableName();
        $account_transactions_table = AccountTransaction::model()->tableName();
        
        return array(
            'byName' => array(
                'order' => $alias.'.code'
            ),
            'showShortAccountsView' => [
                'condition' => "char_length($alias.code)=3 or char_length($alias.code)=:max_code_length",
                'params' => [':max_code_length'=>$max_code_length]
            ],
            'withoutParent'=>[
                'condition'=>'parent_id is null'
            ],
            'withoutModel'=>[
                'condition'=>'model_id is null'
            ],
            'onlyLeafAccounts' => [
                //MilosS: ne oslanja se na parent, nego za izgled koda, sto bi trebalo da je tacnije
//                'condition' => "not exists (select 1 from $account_table where parent_id = $alias.id )",
                'condition' => "char_length($alias.code)>$max_code_length"
                                . " OR "
                                . "("
                                    . "char_length($alias.code)=$max_code_length"
                                    . " AND "
                                    . "not exists (select 1 from accounting.accounts where "
                                                            . "code ~ ('^' || ('^' || $alias.code::TEXT || '.$') )"
                                                            . " AND "
                                                            . "year_id = $alias.year_id"
                                                . ")"
                                . ")",
            ],
            'withTransactions' => [
                'condition' => "exists (select 1 from $account_transactions_table where account_id = $alias.id )",
            ],
            'onlyOneAndThreeDigits' => [
                'condition' => "char_length($alias.code)=1 or char_length($alias.code)=3"                
            ],
        ); 
    }
    
    /**
     * scope koji izracunava DC (Debit/Credit) trenutni
     * @param integer $year_id
     * @param type $start_date
     * @param type $end_date
     * @param boolean $onlyWithTransactions - samo transakcije koje u periodu imaju neke transakcije
     * @return \Account
     */
//    public function withDCCurrents($year_id, $start_date = 'null', $end_date = 'null', $onlyWithTransactions = false, $is_sum = false)
    
    /**
     * 
     * @param integer $year_id
     * @param type $params
     *      start_date - prvi dan koji se gleda
     *      end_date - poslednji dan koji se gleda
     *      onlyWithTransactions(true) - ispisuje samo konta koja imaju transakcije
     *      hide_zero_accounts(true) - ispisuje samo konta koja imaju transakcije
     *      include_orders - spisak naloga koji se gledaju
     *      exclude_orders - spisak naloga koji se ne gledaju
     *      is_sum(false) - interno - menja select prilikom sume
     *      
     * @return \Account
     */
    public function withDCCurrents($year_id, $params = [])
    {
        $start_date = 'null';
        if (isset($params['start_date']) && $params['start_date'] != 'null')
        {
            $start_date = SIMAHtml::UserToDbDate($params['start_date']);
            if (is_null($start_date))
            {
                $start_date = 'null';
            }
        }
        $end_date = 'null';
        if (isset($params['end_date']) && $params['end_date'] != 'null')
        {
            $end_date = SIMAHtml::UserToDbDate($params['end_date']);
            if (is_null($end_date))
            {
                $end_date = 'null';
            }
        }
        
        $alias = $this->getTableAlias();
        $uniqid = SIMAHtml::uniqid();
        $max_code_length = Yii::app()->params['accounting_account_analitic_size'];
        
        $condition = '';
        $onlyWithTransactions = true;
        if (isset($params['onlyWithTransactions']) && $params['onlyWithTransactions'] === 'false')
        {
            $onlyWithTransactions = false;
        }

        if (isset($params['hide_zero_accounts']) && $params['hide_zero_accounts'] === 'false')
        {
            $onlyWithTransactions = false;
        }
        
        if (($onlyWithTransactions))
        {
            $condition .= "(at$uniqid.debit_start != 0  OR ";
            $condition .= "at$uniqid.credit_start != 0  OR ";
            $condition .= "at$uniqid.debit_current != 0  OR ";
            $condition .= "at$uniqid.credit_current != 0 )";
        }
        
        $is_sum = false;
        if (isset($params['is_sum']) && $params['is_sum'] === true)
        {
            $is_sum = true;
        }
        if ($is_sum)
        {
            $_select = "sum(at$uniqid.debit_start) as debit_start, "
                . "sum(at$uniqid.credit_start) as credit_start, "
                . "sum(at$uniqid.debit_current) as debit_current, "
                . "sum(at$uniqid.credit_current) as credit_current ";
        }
        else
        {
            $_select = $alias.'.*, '
                    . "at$uniqid.debit_start as debit_start, "
                    . "at$uniqid.credit_start as credit_start, "
                    . "at$uniqid.debit_current as debit_current, "
                    . "at$uniqid.credit_current as credit_current, "
                    . "'$start_date' as start_date, "
                    . "'$end_date' as end_date ";
        }
        $include_orders = [];
        if (isset($params['include_orders']))
        {
            $include_orders = $params['include_orders'];
        }
        $_include_orders = implode(',', $include_orders);
        
        $exclude_orders = [];
        if (isset($params['exclude_orders']))
        {
            $exclude_orders = $params['exclude_orders'];
        }
        $_exclude_orders = implode(',', $exclude_orders);
        
        $this->getDbCriteria()->mergeWith(array(
            'select' => $_select,
                    
            'join' => " left join accounting.accounts_debits_credits($year_id, $max_code_length, :start_date$uniqid, :end_date$uniqid,"
                . "array[$_include_orders]::integer[],"
                . "array[$_exclude_orders]::integer[]"
                . ") at$uniqid "
                . " on $alias.id = at$uniqid.id",
            'condition' => $condition,
            'params' => [
                ":start_date$uniqid" => $start_date,
                ":end_date$uniqid" => $end_date
            ]
        ));
        
        return $this;
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'options': return [
                'delete', 
                'form', 
                'account_open_with_date'
            ];
            case 'filters': return [
                'id' => 'dropdown',                             
                'code' => ['text','options'=>['wildcard']],
                'description' => 'text',
                'name' => 'text',
                'year' => 'relation',
                'account_transactions' => 'relation',
                'parent' => 'relation',
                'model_name' => 'text',
                'model_id' => 'dropdown',
                'cost_location' => ['relation','func' => 'cost_location_filter'],
                'single_company_accounts' => ['integer', 'func'=>'SingleCompanyAccounts'],
                'only_buyers' => ['func' => 'onlyBuyersFilter'],
                'only_suppliers' => ['func' => 'onlySuppliersFilter']
            ];
            case 'textSearch': return [
                'name' => 'text',
                'code' => ['text', 'options' => ['wildcard']]
            ];
            case 'number_fields' : return [
                'debit_start', 
                'credit_start', 
                'debit_current', 
                'credit_current', 
                'debit_sum', 
                'credit_sum',
                'saldo',
            ];
            case 'searchField' : return [
                'action'=>'accounting/books/searchAccounts'                
            ];
            case 'GuiTable': return array(
                'scopes' => array('byName')
            );
            default: return parent::modelSettings($column);
        }
    }
    
    public function onlyBuyersFilter(SIMADbCriteria $criteria, $alias)
    {
        if($this->only_buyers === true)
        {
            $buyers_codes = $this->getAllBuyersConfigCodes();
            
            if (count($buyers_codes) > 0)
            {
                $temp_criteria = new SIMADbCriteria([
                    'Model' => Account::class,
                    'model_filter' => $this->getBuyerSupplierModelFilter($buyers_codes),
                    'alias' => $alias
                ]);

                $criteria->mergeWith($temp_criteria);
            }
        }
    }

    public function onlySuppliersFilter(SIMADbCriteria $criteria, $alias)
    {
        if($this->only_suppliers === true)
        {
            $suppliers_codes = $this->getAllSuppliersConfigCodes();
            
            if (count($suppliers_codes) > 0)
            {
                $temp_criteria = new SIMADbCriteria([
                    'Model' => Account::class,
                    'model_filter' => $this->getBuyerSupplierModelFilter($suppliers_codes),
                    'alias' => $alias
                ]);
                
                $criteria->mergeWith($temp_criteria);
            }
        }
    }

    public function cost_location_filter(SIMADbCriteria $condition, $alias)
    {
        if(!empty($this->cost_location->ids))
        {
            $_local_cond = new SIMADbCriteria([
                'Model' => 'Account',
                'model_filter' => [
                    'model_name' => 'CostLocation',
                    'model_id' => $this->cost_location->ids
                ],
                'alias' => $alias
            ]);

            $condition->mergeWith($_local_cond);
        }
    }
    
    public function afterSave()
    {        
        //kreira decu do duzine koja je zadata u konfiguracionom parametru
        if ($this->__old['code'] != $this->code)
        {
            $this->getLeafAccount();            
        }        
        //kreira nadkonta ako ne postoje
        if (empty($this->parent_id))
        {
            $code = substr($this->code, 0, -1);
            if ($code !== '')
            {
                $parent_account = Account::get($code, $this->year);
                $curr_account = Account::model()->findByPk($this->id);
                $curr_account->parent_id = $parent_account->id;                
                $curr_account->update();
            }
        }

        if ($this->isNewRecord || $this->columnChanged('model_name') || $this->columnChanged('model_id'))
        {
            if ($this->__old['model_name'] === Partner::class && intval($this->__old['model_id']) !== intval($this->model_id))
            {
                $this->removeBuyerOrSupplierFromPartnerList($this->__old['model_id']);
            }

            if ($this->model_name === Partner::class)
            {
                $this->addBuyerOrSupplierToPartnerList($this->model_id);
            }
        }

        return parent::afterSave();
    }
    
    public function afterDelete() 
    {
        parent::afterDelete();
        
        if ($this->model_name === Partner::class)
        {
            $this->removeBuyerOrSupplierFromPartnerList($this->model_id);
        }
    }
    
    public function addBuyerOrSupplierToPartnerList($partner_id)
    {
        $partner = Partner::model()->findByPk($partner_id);
        if (!empty($partner))
        {
            $code = substr($this->code, 0, 3);
            $buyers_codes = $this->getAllBuyersConfigCodes();
            $suppliers_codes = $this->getAllSuppliersConfigCodes();

            if (in_array($code, $buyers_codes))
            {
                PartnerToPartnerList::add(PartnerList::getSystemList('buyers'), $partner);
            }
            else if (in_array($code, $suppliers_codes))
            {
                PartnerToPartnerList::add(PartnerList::getSystemList('suppliers'), $partner);
            }
        }
    }
    
    public function removeBuyerOrSupplierFromPartnerList($partner_id)
    {
        $partner = Partner::model()->findByPk($partner_id);
        if (!empty($partner))
        {
            $code = substr($this->code, 0, 3);
            $buyers_codes = $this->getAllBuyersConfigCodes();
            $suppliers_codes = $this->getAllSuppliersConfigCodes();

            if (in_array($code, $buyers_codes))
            {
                $partner_accounts_cnt = Account::model()->countByModelFilter($this->getBuyerSupplierModelFilter($buyers_codes, $partner_id));
                if ($partner_accounts_cnt === 0)
                {
                    PartnerToPartnerList::remove(PartnerList::getSystemList('buyers'), $partner);
                }
            }
            else if (in_array($code, $suppliers_codes))
            {
                $partner_accounts_cnt = Account::model()->countByModelFilter($this->getBuyerSupplierModelFilter($suppliers_codes, $partner_id));
                if ($partner_accounts_cnt === 0)
                {
                    PartnerToPartnerList::remove(PartnerList::getSystemList('suppliers'), $partner);
                }
            }
        }
    }
    
    public function isLeafAccount()
    {
        return $this->children_count == 0;
    }
    
    /**
     * Vraca konto koji moze da se knjizi. To znaci da ima fiksan broj analitickih cifara, 
     * osim ukoliko je vezan za model(mesto troska, partner...) onda ima taj broj + ID tog modela
     * @param SIMAActiveRecord $model koji god da je bio model setovan, postavlja se na ono sto je zadato ovde (ako je null, a postoji model onda se vraca isti)
     * @param boolean $set_null ako je true i $model je null, onda se postavlja na null, odnosno dovlaci se Unlisted
     * @return \Account
     */
    public function getLeafAccount($model = null, $set_null = false)
    {
        $analitic_size = Yii::app()->params['accounting_account_analitic_size'];
        $year_id = $this->year_id;
        //ako je veci od predvidjenog, znaici da je vec Leaf, samo provera modela
        if (strlen($this->code) > $analitic_size )
        {
            if (SIMAMisc::modelMatch($model, $this->LinkedModel)) //ako se model poklapa, vrati (ukljucuje dva null)
            {
                return $this;
            }
            else if (!$set_null && is_null($model)) //ako je prosledjen null, ali ne treba da se zada null, isto vrati - svejedno da li je null ili model
            {
                return $this;
            }
            //u ostalim slucajevima postavi roditelja
            $parent_account = $this->parent;
        }
        else
        {
            //get full_length parent
            $parent_account = $this;
            $prefix = $parent_account->code;
            for ($index = strlen($this->code)+1; $index <= $analitic_size; $index++)
            {
                $prefix = str_pad($this->code , $index , "0");
                $parent_account = Account::get($prefix, $this->year);
            }
        }
            
        if ($parent_account->isLeafAccount()) //ako je trenutni parent list
        {
            if (is_null($model))//i ako se trazi null, samo vrati
            {
                return $parent_account;
            }
            else//a trazi se model, onda ide online konvertovanje
            {
                $parent_account->getUnclassifiedLeaf();
            }
        }
        
        //ovde sigurno ima decu
        $prefix = $parent_account->code;
        if (is_null($model))
        {
            
            return $parent_account->getUnclassifiedLeaf();
        }
        else
        {
            $model_name = get_class($model);            
            $_code = $prefix . $model->id;
            $_acc = Account::model()->findByAttributes(array(
                'parent_id' => $parent_account->id,
                'code' => $_code,
                //MIlosS: u sustini moze da se desi da ne nadje konto i da pokusa da napravi isti
                //to se desi ako recimo hoces da imas vezu u kontu ka partneru i ka mestu troska
//                'model_name' => $model_name, //ovo ne bi trebalo da pravi problem posto je samo na odredjene kodove moguce staviti samo odredjene modele
                'model_id' => $model->id,
                'year_id' => $year_id
            ));

            if (!isset($_acc))
            {
                $_acc = new Account();
                $_acc->code         = $_code;
                $_acc->parent_id    = $parent_account->id;
                $_acc->name         = '';
                $_acc->model_name   = $model_name;
                $_acc->model_id     = $model->id;
                $_acc->year_id      = $year_id;
                $_acc->save(); 
                //MilosS(2.2.2017.): bolje da generisanje imena bude na jednom mestu, makar dva puta cuvao
                $_acc->name         = $_acc->recalcName();
                $_acc->update(['name']);
            }

            return $_acc;
        }   
    }
    
    //ukoliko unclassifiedLeaf postoji, samo ga vrati, ukoliko ne, doda ga i prebaci sva knjizenja na njega
    private function getUnclassifiedLeaf()
    {
        $year_id = $this->year_id;
        $parent_id = $this->id;
        $_code = $this->code . "0";
        $_np = Account::model()->findByAttributes(array(
            'parent_id' => $parent_id,
            'code' => $_code,
            'year_id' => $year_id
        ));
        if (!isset($_np))
        {
            $_np = new Account();
            $_np->code          = $_code;
            $_np->name          = '';
            $_np->year_id       = $year_id;
            $_np->parent_id     = $parent_id;
            $_np->save();  
            //MilosS(2.2.2017.): bolje da generisanje imena bude na jednom mestu, makar dva puta cuvao
            $_np->name          = $_np->recalcName();
            $_np->update(['name']);
            
            $old_id = $this->id;
            $new_id = $_np->id;
            
            //prebacuje sve naloge na novi konto
            Yii::app()->db
                    ->createCommand("UPDATE accounting.account_transactions set account_id = $new_id where account_id = $old_id")
                    ->execute();
        }

        return $_np;
    }
    
    /**
     * Vraca konto na osnovu kontnog okvira. Ako ne postoji konto onda se kreira i kreiraju se sva nadkonta
     * @param int $account_layout_id
     * @param int $year_id
     * @return model - konto
     */
    public static function getAccountByLayout($account_layout_id, $year_id)
    {
        $account_layout = AccountLayout::model()->findByPkWithCheck($account_layout_id);
        $year = Year::model()->findByPkWithCheck($year_id);
        
        return Account::get($account_layout->code, $year);
    }
    
    /**
     * Funkcija za dati kod i godinu vraca Account. Ukoliko ne postoji u bazi, kreira ga i vraca ga
     * @param type $code 
     * @param Year $year
     * @return \Account
     */
    public static function get($code, Year $year)
    {
        $analitic_size = intval(Yii::app()->params['accounting_account_analitic_size']);
        $get_leaf = false;
        
        if (
                strlen($code) === ($analitic_size + 1)
                    &&
                $code[$analitic_size] === '0'
            )
        {
            $code = substr($code, 0, $analitic_size);
            $get_leaf = true;
        }
        
        
        if(strlen($code)>$analitic_size)
        {
            throw new Exception("Account::get($code,$year) - premasena maksimalna velicina koda, odnosno, kod je vezan za model");
        }
        elseif(strlen($code) === 0)
        {
            return null;
        }
        
        $res = Account::model()->findByAttributes([
            'code' => $code,
            'year_id' => $year->id
        ]);
        
        if (is_null($res))
        {
            $res = new Account();
            $res->code = $code;
            $res->name = '';
            $res->year_id = $year->id;
            $res->save();
            //MilosS(2.2.2017.): bolje da generisanje imena bude na jednom mestu, makar dva puta cuvao
            $res->name         = $res->recalcName();
            $res->update(['name']);
            //MilosS(2.2.2017.): zasto ovaj refresh? mozda zbog relacija ako se pravi parent?
            $res->refresh();
        }
        
        if ($get_leaf)
        {
            return $res->getLeafAccount(NULL, true);
        }
        else
        {
            return $res;
        }
    }
    
    /**
     * 
     * @param account_code $param_code
     * @param Year $year
     * @return type
     */
    public static function getFromParam($param_code, $year)
    {
        $_code = Yii::app()->configManager->get($param_code,false);
        return self::get($_code, $year)->getLeafAccount();
    }
    
    public function getLinkedModel()
    {
        if ($this->_model === -1)
        {
            $this->_model = null;
            
            if (class_exists($this->model_name))
            {
                $model_name = $this->model_name;
                $this->_model = $model_name::model()->findByPk($this->model_id);
            }

        }
        return $this->_model;
    }
    

    
    public function beforeDelete()
    {        
        $can_delete = true;
        foreach ($this->children as $child)
        {
            if (!$child->delete())
            {
                $can_delete = false;
            }
        }
        if (!$can_delete)
        {
            return false;
        }
        
        if($this->isUnclassifiedLeaf && $this->parent->children_count === 1) 
        {
            $old_account_id = $this->id;
            $new_account_id = $this->parent_id;
            Yii::app()->db
                ->createCommand("UPDATE accounting.account_transactions set account_id = $new_account_id where account_id = $old_account_id")
                ->execute();
        }
        //prebacuje sve naloge na novi konto
        
        return parent::beforeDelete();
    }
    
    /**
     * get same Account for different year
     * @param Year $year
     */
    public function forYear(Year $year)
    {
        $analitic_size = intval(Yii::app()->params['accounting_account_analitic_size']);
        
        if (!empty($this->model_id))
        {
            return self::get($this->parent->code, $year)->getLeafAccount($this->LinkedModel);
        }
        elseif (strlen($this->code)==($analitic_size+1) && $this->code[$analitic_size]==='0')
        {
            return self::get($this->parent->code, $year)->getLeafAccount();
        }
        else
        {
            return self::get($this->code, $year);
        }
    }
    
    public function SingleCompanyAccounts(SIMADbCriteria $condition)
    {
        if(!empty($this->single_company_accounts))
        {
            $single_company_id = $this->single_company_accounts;
            $criteria = new SIMADbCriteria([
                'Model' => 'Account',
                'model_filter' => [
                    'model_name' => 'Partner',
                    'model_id' => $single_company_id
                ]
            ]);
            $condition->mergeWith($criteria);
        }
    }
  
    public function byYear($date)
    {
        $year = Year::getByDate($date);
        
        $criteria = new SIMADbCriteria([
            'Model' => 'Account',
            'model_filter' => [
                'year' => [
                    'ids'=>$year->id
                ]
            ]
        ]);
        $this->getDbCriteria()->mergeWith($criteria);
    }
    
    private static function getLayoutNameCodePair($code, Year $year)
    {
        $_name = '';
        $_layout_code = '';
        $_account_layout = null;
        $_code = $code;
        
        while (is_null($_account_layout) && $_code !== '')
        {
            $_account_layout = AccountLayout::model()->findByAttributes(array(
                'code' => $_code,
                'law_id' => $year->param('accounting.account_layout_law_id')
            ));
            $_code = substr($_code, 0, -1);
        }
        if ($_account_layout !== null)
        {
            $_name = $_account_layout->name;
            $_layout_code = $_account_layout->code;
        }
        
        return [
            'name' => $_name,
            'code' => $_layout_code
        ];
    }
    
    public function recalcName($just_code = false)
    {
        $analitic_size = intval(Yii::app()->params['accounting_account_analitic_size']);
        $year = $this->year;
        $_new_name = '';
        $code = $this->code;
        if(strlen($code)>$analitic_size)
        {
            $_parent_part = $just_code?$this->parent->code:$this->parent->name;
            if (empty($this->model_id))
            {
                $name_for_unclassified = Yii::app()->configManager->get('accounting.name_for_unclassified',false);
                if (!is_string($name_for_unclassified))
                {
                    $name_for_unclassified = Yii::t('AccountingModule.Account', 'Unclassified');
                }
                $_new_name = $_parent_part.' | '.$name_for_unclassified;
            }
            else
            {
                $_linked_model = $this->LinkedModel;
                if (is_null($_linked_model) || !is_object($_linked_model))
                {
                    $error_text = 'IZBRISAN MODEL: '.$this->model_name.' ID: '.$this->model_id;
                    $_new_name = $_parent_part.' | '.$error_text;
                    Yii::app()->errorReport->createAutoClientBafRequest('u AccountDocument LinkedModel -> '.$error_text);
//                    throw new SIMAException('Konto ima model_id, a nema LinkedModel - ID: '.$this->id
//                            .' $_linked_model: '.SIMAMisc::toTypeAndJsonString($_linked_model));
                }
                else
                {
                    $_new_name = $_parent_part.' | '.$this->LinkedModel->DisplayName;
                }
            }
        }
        else
        {
            $_layout_name_code = Account::getLayoutNameCodePair($code, $year);
            $_layout_name = $_layout_name_code['name'];
            $_layout_code = $_layout_name_code['code'];
            
            $_parent_code = substr($code, 0, -1);
            $_parent_layout_name_code = Account::getLayoutNameCodePair($_parent_code,$year);
            $_parent_layout_name = $_parent_layout_name_code['name'];
//            $_parent_layout_code = $_parent_layout_name_code['code'];

            $_is_direct_child = strlen(trim(substr($code, strlen($_layout_code)),'0')) == 0;
            
            if ($just_code)
            {
                $_new_name = $code;
            }
            elseif ($_layout_name !== $_parent_layout_name || $_is_direct_child)
            {
                $_new_name = $_layout_name;
            }
            else
            {
                $_new_name = '';
            }
        }
        return $_new_name;
    }
    
    private function getBuyerSupplierModelFilter($codes_array, $partner_id = null)
    {
        $model_filter = ['OR'];
        foreach ($codes_array as $code)
        {
            array_push($model_filter, ['code' => "$code*"]);
        }
        
        if (!empty($partner_id))
        {
            $model_filter = [
                'AND',
                (count($model_filter) > 1) ? $model_filter : [],
                [
                    'model_name' => Partner::class,
                    'model_id' => $partner_id
                ]
            ];
        }
        
        return count($model_filter) > 1 ? $model_filter : [];
    }
    
    private function getAllBuyersConfigCodes()
    {
        $codes = [];
        
        $customers_code = Yii::app()->configManager->get('accounting.codes.partners.customers', false); //konto kupaca
        if (!empty($customers_code))
        {
            array_push($codes, $customers_code);
        }
        
        $customers_ino_code = Yii::app()->configManager->get('accounting.codes.partners.customers_ino', false); //konto kupaca iz inostranstva
        if (!empty($customers_ino_code))
        {
            array_push($codes, $customers_ino_code);
        }
        
        $advance_bill_out_code = Yii::app()->configManager->get('accounting.codes.partners.advance_bill_out', false); //konto kupaca avansni
        if (!empty($advance_bill_out_code))
        {
            array_push($codes, $advance_bill_out_code);
        }
        
        return $codes;
    }
    
    private function getAllSuppliersConfigCodes()
    {
        $codes = [];

        $suppliers_code = Yii::app()->configManager->get('accounting.codes.partners.suppliers', false);
        if (!empty($suppliers_code))
        {
            array_push($codes, $suppliers_code);
        }

        $suppliers_ino_code = Yii::app()->configManager->get('accounting.codes.partners.suppliers_ino', false);
        if (!empty($suppliers_ino_code))
        {
            array_push($codes, $suppliers_ino_code);
        }

        $advance_bill_in_materials_code = Yii::app()->configManager->get('accounting.codes.partners.advance_bill_in_materials', false);
        if (!empty($advance_bill_in_materials_code))
        {
            array_push($codes, $advance_bill_in_materials_code);
        }

        $advance_bill_in_materials_ino_code = Yii::app()->configManager->get('accounting.codes.partners.advance_bill_in_materials_ino', false);
        if (!empty($advance_bill_in_materials_ino_code))
        {
            array_push($codes, $advance_bill_in_materials_ino_code);
        }

        $advance_bill_in_goods_code = Yii::app()->configManager->get('accounting.codes.partners.advance_bill_in_goods', false);
        if (!empty($advance_bill_in_goods_code))
        {
            array_push($codes, $advance_bill_in_goods_code);
        }

        $advance_bill_in_goods_ino_code = Yii::app()->configManager->get('accounting.codes.partners.advance_bill_in_goods_ino', false);
        if (!empty($advance_bill_in_goods_ino_code))
        {
            array_push($codes, $advance_bill_in_goods_ino_code);
        }

        $advance_bill_in_services_code = Yii::app()->configManager->get('accounting.codes.partners.advance_bill_in_services', false);
        if (!empty($advance_bill_in_services_code))
        {
            array_push($codes, $advance_bill_in_services_code);
        }

        $advance_bill_in_services_ino_code = Yii::app()->configManager->get('accounting.codes.partners.advance_bill_in_services_ino', false);
        if (!empty($advance_bill_in_services_ino_code))
        {
            array_push($codes, $advance_bill_in_services_ino_code);
        }
        
        return $codes;
    }
    
    public function isAssets()
    {
        $code = (string)$this->code;
        $code_first_char = $code[0];
        if ($code_first_char === '0' || $code_first_char === '1' || $code_first_char === '2')
        {
            return true;
        }
        
        return false;
    }
}