<?php

class AdvanceBill extends Bill
{

    public $bill_id;
    public $like_bill_id;
    public $advance_id;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    
    public function init()
    {
        parent::init();
        $this->bill_type = self::$ADVANCE_BILL;
    }

    public function defaultScope()
    {
        $alias = $this->getTableAlias(FALSE, FALSE);
        $adv_bill = Bill::$ADVANCE_BILL;
        return array(
            'condition' => "$alias.bill_type='$adv_bill' and $alias.canceled=false",
        );
    }
    
    public function scopes()
    {
        $alias = $this->getTableAlias();
        return array_merge(parent::scopes(),[
            'connected' => [
                'condition' => $alias.'.connected = TRUE'
            ]
        ]);
    }
    
    public function rules()
    {
        return array_merge(parent::rules(), [
            ['bill_id, like_bill_id, advance_id', 'safe'],
            ['amount', 'checkAmountWithConnectedValues'],
        ]);
    }
    
    /**
     * MilosS(12.2.2019): bitna je provera iz Bill da vrednosti ne mogu da se menjaju kada su zakljucane, 
     * jer se podrazumeva da je $this->lock_amounts === true
     */
    public function checkAmountWithConnectedValues()
    {
        if (!$this->hasErrors() && $this->columnChanged('amount'))
        {
            $old_amount = floatval($this->__old['amount']);
            $amount_down_for = $old_amount - $this->amount;
            
            if (SIMAMisc::lessThen($old_amount - $this->released_sum, $amount_down_for))
            {
                $this->addError('amount',Yii::t('AccountingModule.AdvanceBill','AmountUnderReleased'));
            }
        }
       
    }

    public function attach_advance($attribute, $params)
    {
        if (!$this->hasErrors() && !$this->isNewRecord)
        {
            $advance = Advance::model()->with('releases')->findByPk($this->advance_id);
            if ($advance == null)
                $this->addError('advance_id', 'ne postoji avans!');
// 			if (!$advance->invoice) $this->addError('advance_id','nije isti invoice!');
            else
            {
                if ($advance->amount != $this->amount)
                    $this->addError('advance_id', 'sume avansa i avansnog racuna nisu iste!');
                if (count($advance->releases) != 0)
                    $this->addError('advance_id', 'avans je vec rasknjizavan!');
            }
        }
    }

    

    public function getClasses()
    {
        return parent::getClasses() .
                ((!$this->connected) ? ' not_attached ' : '');
    }

    public function relations($child_relations = [])
    {
        return parent::relations(array_merge([
            'partner' => array(self::BELONGS_TO, 'Partner', 'partner_id'),
            'job_order' => array(self::BELONGS_TO, 'CostLocation', 'cost_location_id'),
            'bill_releases' => array(self::HAS_MANY, 'AdvanceBillRelease', 'advance_bill_id'),
            'bill_releases_count' => array(self::STAT, 'AdvanceBillRelease', 'advance_bill_id', 'select' => 'count(*)'),
            'relief_bill_reliefs' => array(self::HAS_MANY, 'AdvanceBillRelief', 'advance_bill_id'),
            'relief_bill_reliefs_count' => array(self::STAT, 'AdvanceBillRelief', 'advance_bill_id', 'select' => 'count(*)'),
            'fictive_account_link' => array(self::HAS_MANY, 'AdvanceBillFictiveAccountLink', 'advance_bill_id'),
        ],$child_relations));
    }

    public function __get($column)
    {
        switch ($column)
        {
            case 'isReleased':		return (abs($this->unreleased_amount)<0.001);
            case 'hasReliefes':         return false;
            default: return parent::__get($column);
        }
    }

    public function getBillReleasedAmount($bill_id, $column = 'amount')
    {
        if (!isset($this->id))
            return 0;
        $advanceBillReleases = AdvanceBillRelease::model()->findAllByAttributes(array('bill_id' => $bill_id, 'advance_bill_id' => $this->id));
        $sum = 0;
        foreach ($advanceBillReleases as $advanceBillRelease)
        {
            $sum+=$advanceBillRelease->$column;
        }
        return $sum;
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters': return array_merge(parent::modelSettings($column),[
                'bill_id' => array('func' => 'filter_bill_id')
            ]);
            default: return parent::modelSettings($column);
        }
    }

    public function filter_bill_id($condition)
    {
        if ($this->bill_id != null)
        {
            $bill = Bill::model()->findByPk($this->bill_id);
            if ($bill != null)
            {
                $_advance_bill_release_table = AdvanceBillRelease::model()->tableName();
                $alias = $this->getTableAlias();
                $condition->select .= ", '" . $this->bill_id . "' as like_bill_id";
                $param_id = SIMAHtml::uniqid();
                $temp_cond = new SIMADbCriteria();
                $temp_cond->condition = "$alias.id in (select advance_bill_id from $_advance_bill_release_table where bill_id=:bill_id$param_id)";
                $temp_cond->params = array(":bill_id${param_id}" => $this->bill_id);
                $condition->mergeWith($temp_cond);
            }
        }
    }
    
    public function remainingVatItemsArray()
    {
        $_vats = $this->vatItemsArray(true);
        foreach ($this->bill_releases as $_bill_release)
        {
            $I_R_code = $_bill_release->internalVatCode();
            $_vats[$I_R_code][$_bill_release->vat_rate]['base'] -= $_bill_release->base_amount;
            $_vats[$I_R_code][$_bill_release->vat_rate]['vat']  -= $_bill_release->vat_amount;
        }
        return $_vats;
    }
    
    public function remainingReliefsVatItemsArray()
    {
        $_vats = $this->vatItemsArray(true);
        foreach ($this->reliefes as $_bill_relief)
        {
            $I_R_code = $_bill_relief->internalVatCode();
            $_vats[$I_R_code][$_bill_relief->vat_rate]['base'] -= $_bill_relief->base_amount;
            $_vats[$I_R_code][$_bill_relief->vat_rate]['vat']  -= $_bill_relief->vat_amount;
        }
        if ($this->releases_count > 0)
        {
            Yii::app()->errorReport->createAutoClientBafRequest('Avansni racun '.$this->id.' pokusano rasknjizavanje po knjiznom odobrenju, a imaju avanse uplate');
            throw new SIMAWarnException('postoje avansne uplate, prvo skinite te uplate');
        }
        
        return $_vats;
    }

}