<?php

class FixedAssetsDepreciationItem extends SIMAActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'accounting.fixed_assets_depreciation_items';
    }

    public function moduleName()
    {
        return 'accounting';
    }

    public function __isset($column)
    {
        switch ($column)
        {
            case 'value_before': 
            case 'value_after':
            case 'depreciation_before': 
            case 'depreciation_after': return true;
            default: return parent::__isset($column);
        }
    }
//    public function __get($column)
//    {
//        switch ($column)
//        {
//            case 'DisplayName': 
//            case 'SearchName': return $this->code . ' - ' . $this->name;
//            default: return parent::__get($column);
//        }
//    }
    
    private $_local_depreciation_before = null;
    
    private function local_depreciation_before()
    {
        if (is_null($this->_local_depreciation_before))
        {
            $_sum_item = FixedAssetsDepreciationItem::model()->find([
                'model_filter' => [
                    'accounting_fixed_asset' => ['ids' => $this->accounting_fixed_asset_id],
                    'fixed_assets_depreciation' => [
                        'year' => [
                            'scopes' => ['beforeYear' => $this->fixed_assets_depreciation->year->year]
                        ]
                    ]
                ],
                'select' => 'sum(depreciation) as depreciation',

            ]);
            $this->_local_depreciation_before = (empty($_sum_item->depreciation)?0:$_sum_item->depreciation);
        }
        return $this->_local_depreciation_before;
    }
    
    public function getvalue_before()
    {
        return $this->accounting_fixed_asset->purchaseValueOnDate(new SIMADateTime("31.12.{$this->fixed_assets_depreciation->year->year}.")) - $this->depreciation_before;
    }
    
    public function getvalue_after()
    {
        return $this->value_before - $this->depreciation;
    }
    
    public function getdepreciation_before()
    {
        return $this->local_depreciation_before() + $this->accounting_fixed_asset->depreciation_start;
    }
    
    public function getdepreciation_after()
    {
        return $this->depreciation_before + $this->depreciation;
    }

    public function calcLocalValues()
    {
        parent::calcLocalValues();
        $this->_local_depreciation_before = null;
    }
    
    public function rules()
    {
        return array(
            array('accounting_fixed_asset_id, fixed_assets_depreciation_id', 'required'),
            array('note', 'safe'),
            array('depreciation', 'numerical',),
            array('accounting_fixed_asset_id, fixed_assets_depreciation_id', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => array('insert', 'update'))
        );
    }
    
    public function relations($child_relations = [])
    {
        return array(
            'fixed_assets_depreciation' => array(self::BELONGS_TO, 'FixedAssetsDepreciation', 'fixed_assets_depreciation_id'),
            'accounting_fixed_asset' => array(self::BELONGS_TO, 'AccountingFixedAsset', 'accounting_fixed_asset_id'),
//            'paychecks' => array(self::HAS_MANY, 'Paychecks', 'paycheck_period_id'),
        );
    }

    
    
    public function scopes()
    {
        $alias = $this->getTableAlias();
        $dep_table = FixedAssetsDepreciation::model()->tableName();
        $dep_table_alias = SIMAHtml::uniqid();
        return array(
            'byName'=>array(
                'order' => $alias.'.order'
            ),
            'byYear' => [
                'order' => "(select year from base.years byear where byear.id = "
                . "(select year_id from $dep_table $dep_table_alias where $dep_table_alias.id = $alias.fixed_assets_depreciation_id))"
            ]
        );
    }

    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters' : return [
                'fixed_assets_depreciation' => 'relation',
                'accounting_fixed_asset' => 'relation',
                'note' => 'text',
            ];
            case 'textSearch' : return [
                'fixed_assets_depreciation' => 'relation',
                'accounting_fixed_asset' => 'relation',
                'note' => 'text',
            ];
            case 'multiselect_options': return ['delete'];
            case 'number_fields': return [
                'depreciation',
                'depreciation_before',
                'depreciation_after',
                'value_before',
                'value_after',
            ];
            default: return parent::modelSettings($column);
        }
    }

}