<?php

class CashDeskOrder extends SIMAActiveRecord
{
    public $cash_desk_log_id_is_null;
    public $amount;
    public $amount_letters;
    private $_date = null;
    
    public static $PAYOUT = 'PAYOUT'; //isplatiti iz blagajne
    public static $PAYIN = 'PAYIN'; //uplatiti u blagajnu
    public static $SUBMIT = 'SUBMIT'; //uplatiti na ziro racun iz blagajne
    public static $WITHDRAW = 'WITHDRAW'; //podici za ziro racuna u blagajnu
    
    //jednom kad se postavi, nema menjanja
    public $cash_desk_id = null;
    
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'accounting.cash_desk_orders';
    }

    public function moduleName()
    {
        return 'accounting';
    }

    public function __get($column)
    {
        switch ($column)
        {
            case 'path2': return 'files/file';
            case 'DisplayName': return $this->display_name;
            case 'SearchName': return $this->display_name;
            case 'amount': return $this->calcAmount();
//            case 'amount_letters': $this->amount_letters;
            case 'date': return (!isset($this->cash_desk_log) || ($this->_date!=''))?$this->_date:$this->cash_desk_log->date;
            case 'isIN': return ($this->type == self::$PAYIN || $this->type == self::$WITHDRAW);
            default: return parent::__get($column);
        }
    }

    public function __set($name, $value)
    {           
        switch ($name)
        {
            case 'date': $this->_date = $value; break;
            default: parent::__set($name, $value); break;
        }
    }
    
    public function __isset($column)
    {
        switch ($column)
        {
            case 'date': return $this->_date!==null;
            default: return parent::__isset($column);
        }
    }

    public function afterFind()
    {
        parent::afterFind();
        $this->amount = $this->calcAmount();
        $this->amount_letters = SIMAHtml::money2letters($this->amount, 'latin');
        if (isset($this->cash_desk_log))
        {
            $this->cash_desk_id = $this->cash_desk_log->cash_desk_id;
        }
    }
    
    public function scopes()
    {
        $alias = $this->getTableAlias();
        $cash_desk_log_table = CashDeskLog::model()->tableName();
        return [
            'byOfficialDESC' => [
                'select' => "$alias.*, (select date from $cash_desk_log_table where id = $alias.cash_desk_log_id)",
                'order' => "(select date from $cash_desk_log_table where id = $alias.cash_desk_log_id) DESC, $alias.id DESC"
            ],
            'byName' => array('order' => "$alias.number"),
        ];
    }
    
    public function relations($child_relations = [])
    {
        return array(
            'file' => array(self::BELONGS_TO, 'File', 'id'),
            'cash_desk_log' => array(self::BELONGS_TO, 'CashDeskLog', 'cash_desk_log_id'),
            'cash_desk_items'=>array(self::HAS_MANY, 'CashDeskItem', 'cash_desk_order_id'),
            'confirmed_user' => array(self::BELONGS_TO, 'User', 'confirmed_user_id'),
        );
    }

    public function rules()
    {
        return array(
            array('date, cash_desk_id, type', 'required'),
            array('id, note, cash_desk_log_id, confirmed, confirmed_user_id, confirmed_timestamp, cash_desk_log_id_is_null, amount', 'safe'),
            array('number, amount', 'numerical'),
            array('cash_desk_log_id','default',
                'value'=>null,
                'setOnEmpty'=>true,'on'=>array('insert','update')),
            array('number', 'checkNumber')
        );
    }    
    
//    public function checkNumber($attribute, $params)
    public function checkNumber()
    {
        if (isset($this->date) && $this->date !== null && $this->date !== '')
        {
            $cash_desk_log = $this->getCashDeskLog($this->date);
            //ako je doslo do bilo kakve promene
            if (
                    ($this->__old['number'] !== intval($this->number))
                    || ($this->__old['cash_desk_log_id'] !== $cash_desk_log->id)
                    || ($this->__old['type'] !== $this->type)
               )
            {            
                $number = CashDeskOrder::model()->findByAttributes([
                    'cash_desk_log_id' => $cash_desk_log->id,
                    'type' => $this->type,
                    'number' => intval($this->number)
                ]);
                if ($number !== null)
                {
                    $this->addError('number', 'Već postoji nalog sa ovim brojem!');
                }
            }
        }
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'textSearch': return array(
                'number' => 'text',
            );
            case 'filters': return array(
                'id' => 'dropdown',
                'number' => 'text',
                'type' => 'dropdown',
                'cash_desk_log' => 'relation',
                'cash_desk_log_id_is_null'=>array('func'=>'filter_cash_desk_log_id_is_null'),
                'display_name' => 'text'
            );
            case 'options': return array(
                'open',
                'form',
                'delete',
                'download'
            );
            case 'update_relations': return array(
                  'cash_desk_log'
            );
            case 'statuses': return array(
                'confirmed' => array(
                    'title' => 'Potvrdjeno',
                    'timestamp' => 'confirmed_timestamp',
                    'user' => array('confirmed_user_id','relName'=>'confirmed_user'),
                    'checkAccessConfirm' => 'canConfirmCashDeskOrder',
                    'onConfirm'=>'onConfirm',
                    'onRevert'=>'onRevert',
                )
            );
            case 'number_fields': return array(
                'amount'
            );
            default: return parent::modelSettings($column);
        }
    }
    
    public function canConfirmCashDeskOrder()
    {
        return true;
    }
    
    public function onConfirm()
    {
        if (isset($this->file))
        {
            $this->generatePDF();
        }
    }
    
    public function onRevert()
    {
        $this->changeConfirmForLog($this->cash_desk_log_id);
    }
    
    function filter_cash_desk_log_id_is_null($condition)
    {
        $temp_condition = new CDbCriteria();
        $temp_condition->condition = "cash_desk_log_id is null";
        $condition->mergeWith($temp_condition);
    }
    
    public function beforeSave()
    {
        if (!isset($this->file) && ($this->id == null || $this->id == ''))
        {
            $cash_desk_order_document_type_id = Yii::app()->configManager->get('accounting.cash_desk.order',false);

            $file = new File();
            $file->document_type_id = $cash_desk_order_document_type_id;
            $file->responsible_id = Yii::app()->user->id;
            $file->save();
            $file->refresh();
            $this->id = $file->id;
            $this->file = $file;
        }
    
        if ($this->date !== null && $this->date !== '')
        {            
            $cash_desk_log = $this->getCashDeskLog($this->date);
            $this->cash_desk_log_id = $cash_desk_log->id;
        }
        if  (
                intval($this->__old['number']) !== intval($this->number) 
                || 
                $this->__old['type'] !== $this->type
                || 
                $this->__old['cash_desk_log_id'] !== $this->cash_desk_log_id
            )
        {
            $this->confirmed = false;
        }
        return parent::beforeSave();
    }
    
    public function afterSave() 
    {
        parent::afterSave();
        
//        $this->refresh();
        if ($this->__old['number'] !== $this->number || $this->__old['type'] !== $this->type)
        {
            $this->changeConfirmForLog($this->cash_desk_log_id);
        }
        if ($this->__old['cash_desk_log_id'] !== $this->cash_desk_log_id)
        {
            $this->changeConfirmForLog($this->__old['cash_desk_log_id']);
            $this->changeConfirmForLog($this->cash_desk_log_id);
            
            if (!empty($this->__old['cash_desk_log_id']))
            {
                $this->deleteCashDeskLogIfEmpty($this->__old['cash_desk_log_id']);
            }
        }
        
        $this->file->refresh();
        $this->file->beforeChangeLock();
        $this->file->name = 'Nalog za blagajnu br. ' . $this->DisplayName;
        $this->file->save();
        $this->file->afterChangeUnLock();
    }
    
    public function changeConfirmForLog($cash_desk_log_id)
    {
        $cash_desk_log = CashDeskLog::model()->findByPk($cash_desk_log_id);
        if ($cash_desk_log !== null)
        {
            $param_id = SIMAHtml::uniqid();
            $cash_desk_log->confirmed = false;
            $cash_desk_log->save();
            $criteria = new SIMADbCriteria();
            $criteria->condition = "date > :param1$param_id";
            $criteria->params = array(":param1$param_id" => SIMAHtml::UserToDbDate($cash_desk_log->date));
            $cash_desk_newer_logs = CashDeskLog::model()->findAll($criteria);
            foreach ($cash_desk_newer_logs as $cash_desk_newer_log) 
            {
                if ($cash_desk_newer_log->confirmed)
                {
                    $cash_desk_newer_log->confirmed = false;
                    $cash_desk_newer_log->save();
                }
            }
        }
    }
    
    public function calcAmount()
    {
        $amount = 0;
        
        if (isset($this->cash_desk_items))
        {
            foreach ($this->cash_desk_items as $item)
            {
                $amount += $item->amount;
            }
        }  
        return $amount;
    }
    
    /**
     * @return model CashDeskLog
     */
    private function getCashDeskLog($date)
    {
        $cash_desk_log = CashDeskLog::model()->findByAttributes([
            'date'=>SIMAHtml::UserToDbDate($date),
            'cash_desk_id' => $this->cash_desk_id
        ]);
        if ($cash_desk_log === null)
        {
            $cash_desk_log = new CashDeskLog();
            $cash_desk_log->date = SIMAHtml::UserToDbDate($date);
            $cash_desk_log->cash_desk_id = $this->cash_desk_id;
            $cash_desk_log->save();
            $cash_desk_log->refresh();
        }
        
        return $cash_desk_log;
    }
    
    public function afterDelete() 
    {
        parent::afterDelete();

        $this->deleteCashDeskLogIfEmpty($this->cash_desk_log_id);
    }
    
    private function deleteCashDeskLogIfEmpty($cash_desk_log_id)
    {
        $cash_desk_log = CashDeskLog::model()->findByPkWithCheck($cash_desk_log_id);
        if ($cash_desk_log->cash_desk_orders_cnt === 0)
        {
            $cash_desk_log->delete();
        }
    }
}