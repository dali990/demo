<?php

class AnyBill extends Bill
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    
    
    public function defaultScope()
    {
        parent::defaultScope();
        return [];
    }
}