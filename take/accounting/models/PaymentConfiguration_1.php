<?php

/**
 * Description of PaymentConfiguration
 *
 * @author goran-set
 */
class PaymentConfiguration extends SIMAActiveRecord
{
    public static $TYPE_CSV_FILE = 0;
    public static $TYPE_XML_FILE = 1;
    public static $TYPE_ERSTE_WEB = 2;
    public static $TYPE_RAIFFEISEN = 3;
    public static $TYPE_HALK_BANK = 4;
    public static $TYPE_RAIFFEISEN_DEVIZNI = 5;
    public static $TYPE_SOGE_XML = 6;
    public static $TYPE_OTP_DEVIZNI_XML = 7;
    
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'accounting.payment_configurations';
    }

    public function moduleName()
    {
        return 'accounting';
    }
    
    public function __get($column)
    {
        switch ($column)
        {
            case 'DisplayName':		return $this->name;
            case 'SearchName':		return $this->DisplayName;
            default: return parent::__get($column);
        }
    }
    
    public function rules()
    {
        return array(
            array('name, type, account_owner_col, account_number_col, date_col, amount_col,amount2_col, export_format, bank_transaction_id', 'required'),
            array('name, type, separator, decimal_separator, date_col,path, description, payment_code_col, bank_account_id, bank_account_date, bank_account_amount, bank_account_representation, bank_statement_number', 'safe'),
            array('call_for_number_debit_col, call_for_number_credit_col', 'safe'),
            array('separator','checkSeparator'),
            array('path','checkPath'),
            array('name, type,separator, account_owner_col, account_number_col, date_col, amount_col, amount2_col, path', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => array('insert', 'update')),
        );
    }
    
//    public function checkSeparator($attribute, $params)
    public function checkSeparator()
    {
        if ($this->separator=='' && $this->type==0)
        {
            $this->addError('separator', "Ukoliko je izabran tip fajla iz kog se ucitavaju izvodi i on je CSV, mora biti unet i separator u fajlu!");
        }
    }
    
//    public function checkPath($attribute, $params)
    public function checkPath()
    {
        if ($this->path=='' && $this->type==1)
        {
            $this->addError('path', "Ukoliko je izabran tip fajla iz kog se ucitavaju izvodi i on je XML, mora biti uneta i putanja do racuna u fajlu!");
        }
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters' : return array_merge(parent::modelSettings($column),array(
                //'account_id' => 'dropdown',
                'name' => 'text',
                'type' => 'dropdown'
            ));
            case 'textSearch' : return array(
                'name' => 'text',
            );
            case 'options' : return array('delete', 'form');
            default: return parent::modelSettings($column);
        }
    }
    
//    public function columnOrder()
//    {
//        return array('name','type','separator','decimal_separator','path', 'account_number_col', 
//            'payment_code_col',
//            'call_for_number_debit_col', 'call_for_number_credit_col',
//            'account_owner_col', 'date_col', 'amount_col','amount2_col',
//            'bank_account_id', 'bank_account_date', 'bank_account_amount', 'bank_statement_number', 'bank_transaction_id', 'description');
//    }
    
    public function scopes()
    {
        return array(
            'recently' => array(
                'order' => 'name',
            ),
        );
    }
    
    public function beforeSave()
    {
        return parent::beforeSave();
    }

    public function afterSave()
    {
        return parent::afterSave();
    }
    
}
