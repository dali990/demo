<?php

//namespace Accounting;

class AccountingFixedAsset extends SIMAActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'accounting.fixed_assets';
    }

    public function moduleName()
    {
        return 'accounting';
    }

    public function __get($column)
    {
        switch ($column)
        {
            case 'DisplayName': return $this->fixed_asset->DisplayName;
            case 'SearchName': return $this->fixed_asset->SearchName;
            case 'current_value': return $this->current_purchase_value - $this->depreciation;
            case 'current_purchase_value':
                if (!empty($this->fixed_asset))
                {
                    return $this->purchase_value - $this->fixed_asset->deactivation_value_sum;
                }
                
                return 0.00;
            case 'depreciation_start': return $this->purchase_value - $this->current_value_start;
            case 'depreciation': 
                //pocetna amortizacija + celo godisnja amortizacija + amortizacija otpisane kolicine u godini otpisa
                return $this->depreciation_start + $this->depreciation_sum + $this->fixed_asset->deactivation_depreciation_value_curr_year_sum;
            case 'depreciation_rate': 
                if ($this->custom_depreciation_rate)
                {
                    return $this->local_depreciation_rate;
                }
                else
                {
                    if (isset($this->fixed_asset->fixed_asset_type->fixed_asset_amortization_group))
                    {
                        return $this->fixed_asset->fixed_asset_type->fixed_asset_amortization_group->depreciation_rate;
                    }
                    else
                    {
                        return 0;
                    }
                }
            default: return parent::__get($column);
        }
    }
    
    public function __isset($column)
    {
        switch ($column)
        {
            case 'depreciation_rate': return true;
            default: return parent::__isset($column);
        }
        
    }

    public function rules()
    {
        return array(
            ['order', 'numerical', 'integerOnly'=>true],
            ['order', 'unique'],
            ['purchase_value, current_value_start', 'required'],
            ['local_depreciation_rate, custom_depreciation_rate', 'safe'],
            array('purchase_value, current_value_start, local_depreciation_rate', 'numerical'),
            array('usage_start_date', 'safe'),
            ['id','checkDelete','on' => ['delete']],
            array('cost_location_id', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => array('insert', 'update')),
        );
    }
    
    public function checkDelete()
    {
        if (
                isset($this->fixed_asset->acquisition_document->account_document)
                &&
                $this->fixed_asset->acquisition_document->account_document->isBooked
                )
        {
            $this->addError('id', 'Nalog za knjizenje dokumenta o pribavljanju je potvrdjen');
        }
        if ($this->depreciations_count > 0)
        {
            $this->addError('id', 'Osnovno sredstvo amortizovano u nekim godinama');
        }
    }
    
    public function relations($child_relations = [])
    {
        return array(
            'fixed_asset' => [self::BELONGS_TO, 'FixedAsset', 'id'],
            'cost_location' => [self::BELONGS_TO, 'CostLocation', 'cost_location_id'],
            'depreciations' => [self::HAS_MANY, 'FixedAssetsDepreciationItem', 'accounting_fixed_asset_id'],
            'depreciations_count' => [self::STAT, 'FixedAssetsDepreciationItem', 'accounting_fixed_asset_id', 'select' => 'count(*)'],
            'depreciation_sum' => [self::STAT, 'FixedAssetsDepreciationItem', 'accounting_fixed_asset_id','select' => 'sum(depreciation)'],
            'last_depreciations' => [self::HAS_ONE, 'FixedAssetsDepreciationItem', 'accounting_fixed_asset_id',
                'scopes' => ['byYear']
            ],
        );
    }

    public function scopes()
    {
        $alias = $this->getTableAlias();
        return array(
            'byOrder' => array(
                'order' => "$alias.order ASC",
            ),
            'byOrderDESC' => [
                'order' => "$alias.order DESC",
            ]
        );
    }

    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'options': return ['form','open'];
            case 'filters' : return array_merge(parent::modelSettings('filters'), array(
                    'order' => 'integer',
                    'fixed_asset' => 'relation',
                    'usage_start_date' => 'date_range'
//                    'location' => 'relation',
//                    'comment' => 'text',
//                    'number' => 'integer'
                ));
            case 'textSearch' : return array(
                    'order' => 'integer',
                    'fixed_asset' => 'relation',
                );
            case 'number_fields': return ['purchase_value', 'current_purchase_value', 'current_value', 'depreciation_rate', 'local_depreciation_rate', 'depreciation'];
            case 'mutual_forms': return ['base_relation' => 'fixed_asset'];
            case 'multiselect_options': return ['delete'];
            default: return parent::modelSettings($column);
        }
    }
    
    public function afterSave() 
    {
        parent::afterSave();
        
        if ($this->isNewRecord)
        {
            Yii::app()->warnManager->sendWarns('WarnFixedAssetsWithoutAccountingOrGroup', 'WARN_FIXED_ASSETS_WITHOUT_ACCOUNTING_OR_GROUP');
        }
    }
    
    public function getInitialAccountTransaction()
    {
        if (isset($this->fixed_asset->bill_item->bill->file->account_document))
        {
            $ad = $this->fixed_asset->bill_item->bill->file->account_document;
        }  
        else
        {
            error_log('nema naloga za knjizenje');
            return null;
        }

        $account_id = $this->fixed_asset->fixed_asset_type->account->forYear(
                $this->fixed_asset->bill_item->bill->file->account_document->account_order->year
        )->id;
        
        $at = AccountTransaction::model()->find([
            'model_filter' => [
                'account_document' => ['ids' => $ad->id],
                'account_booked' => ['ids' => $account_id]
            ]
        ]);
        
        return $at;
    }
    
    public function purchaseValueOnDate(SIMADateTime $date)
    {
        $fixed_asset_deactivation_sum = FixedAssetToDeactivationDecision::model()->find([
            'model_filter' => [
                'fixed_asset' => [
                    'ids' => $this->id
                ],
                'deactivation_decision' => [
                    'confirmed' => true,
                    'deactivation_date' => ['<=', $date->format('Y-m-d H:i')]
                ]
            ],
            'select' => 'sum(deactivation_value) as deactivation_value',
        ]);
        
        return $this->purchase_value - (empty($fixed_asset_deactivation_sum) ? 0 : $fixed_asset_deactivation_sum->deactivation_value);
    }
    
    public function depreciationOnDate(SIMADateTime $date)
    {
        $fixed_asset_depreciation_sum = FixedAssetsDepreciationItem::model()->find([
            'model_filter' => [
                'accounting_fixed_asset' => [
                    'ids' => $this->id
                ],
                'fixed_assets_depreciation' => [
                    'year' => [
                        'scopes' => ['beforeYear' => $date->format("Y")]
                    ]
                ]
            ],
            'select' => 'sum(depreciation) as depreciation',
        ]);
        
        $fixed_asset_deactivation_sum = FixedAssetToDeactivationDecision::model()->find([
            'model_filter' => [
                'fixed_asset' => [
                    'ids' => $this->id
                ],
                'deactivation_decision' => [
                    'confirmed' => true,
                    'deactivation_date' => ['<=', $date->format('Y-m-d H:i')]
                ]
            ],
            'select' => 'sum(depreciation_value_curr_year) as depreciation_value_curr_year',
        ]);
        
        return $this->depreciation_start + 
               (empty($fixed_asset_depreciation_sum) ? 0 : $fixed_asset_depreciation_sum->depreciation) + 
               (empty($fixed_asset_deactivation_sum) ? 0 : $fixed_asset_deactivation_sum->depreciation_value_curr_year);
    }
}
