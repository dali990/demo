<?php 



class ReliefBill extends Bill
{  
    public $bill_id;
    public $like_bill_id;

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
    
    public function init()
    {
        parent::init();
        $this->bill_type = self::$RELIEF_BILL;
    }

    public function defaultScope()
    {
        $alias = $this->getTableAlias(FALSE, FALSE);
        $bill_type = Bill::$RELIEF_BILL;
        return array(
            'condition' => "$alias.bill_type = '$bill_type' and $alias.canceled=false",
        );
    }
	
    public function rules()
    {
        return array_merge(array(
            array('bill_id, like_bill_id','safe')
        ),parent::rules());
    }
	
	
    public function getBillReleasedAmount($bill,$column='amount')
    {
        if (!isset($this->id)) return 0;
        $billReliefs = BillRelief::model()->findAllByAttributes(array('bill_id'=>$bill,'relief_bill_id'=>$this->id));
        $sum = 0;
        foreach ($billReliefs as $billRelief)
        {
            $sum+=$billRelief->$column;
        }
        return $sum;
    }
	
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters': return array_merge(parent::modelSettings($column),[
                'bill_id' => array('func' => 'filter_bill_id')
            ]);
            default: return parent::modelSettings($column);
        }
        return $result;
    }
	
    public function relations($child_relations = [])
    {
        return parent::relations(array_merge([
            'file'=>array(self::BELONGS_TO, 'File', 'id'),
            'partner'=>array(self::BELONGS_TO, 'Partner', 'partner_id'),
            'bill_releases' => array(self::HAS_MANY, 'BillRelief', 'relief_bill_id'),
            'bill_releases_count' => array(self::STAT, 'BillRelief', 'relief_bill_id', 'select' => 'count(*)'),
            'advance_bill_reliefs' => array(self::HAS_MANY, 'AdvanceBillRelief', 'relief_bill_id'),
            'advance_bill_reliefs_count' => array(self::STAT, 'AdvanceBillRelief', 'relief_bill_id', 'select' => 'count(*)'),
            'job_order'=>array(self::BELONGS_TO, 'CostLocation', 'cost_location_id'),
            'cost_type'=>array(self::BELONGS_TO, 'CostType', 'cost_type_id'),
            
            'warehouse_requisitions'=>array(self::HAS_MANY, 'WarehouseRequisition', 'bill_id'),
            'year' => array(self::BELONGS_TO, 'Year', 'year_id'),
        ],$child_relations));
    }
	
    public function __get($column)
    {
        // 		if (sub_str($column)=='sum_') {$column = 'sum_'.$column; return $this->$column;}
        if (substr($column,0)=='sum_') {
                return $this->$column;
        }
        switch ($column)
        {
            case 'pib_jmbg': 					return $this->partner->pib_jmbg;
            case 'hasReliefes':		return false;
            default: 			return parent::__get($column);
        }
    }
	
    public function filter_bill_id($condition)
    {
        if ($this->bill_id!=null)
        {
            $bill = Bill::model()->findByPk($this->bill_id);
            if ($bill!=null)
            {
                $param_id = SIMAHtml::uniqid();
                $bill_reliefs_table = BillRelief::model()->tableName();
                $alias = $this->getTableAlias();
                $temp_cond = new SIMADbCriteria();
                $temp_cond->condition = "$alias.id in (select relief_bill_id from $bill_reliefs_table where bill_id=:bill_id${param_id})";
                $temp_cond->params = array(":bill_id${param_id}"=>$this->bill_id);
                $condition->mergeWith($temp_cond);
            }
        }
    }
    
    public function filter_like_advance_bill_id($condition)
    {
        if (isset($this->like_advance_bill_id) && $this->like_advance_bill_id != null)
        {
            $bill = Bill::model()->findByPk($this->like_advance_bill_id);
            if ($bill != null)
            {
                $temp_cond = new SIMADbCriteria([
                    'Model' => 'ReliefBill',
                    'model_filter' => [
                        'partner' => ['ids' => $bill->partner_id],
                        'invoice' => $bill->invoice,
                        'bill_type' => 'RELIEF_BILL'
                    ]
                ]);
                $condition->mergeWith($temp_cond);
            }
        }
    }
    	
    public function remainingVatItemsArray()
    {
        $_vats = $this->vatItemsArray(true);
        foreach ($this->bill_releases as $_bill_release)
        {
            $I_R_code = $_bill_release->internalVatCode();
            $_vats[$I_R_code][$_bill_release->vat_rate]['base'] -= $_bill_release->base_amount;
            $_vats[$I_R_code][$_bill_release->vat_rate]['vat']  -= $_bill_release->vat_amount;
        }
        return $_vats;
    }
	
}