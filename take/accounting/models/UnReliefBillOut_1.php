<?php

class UnReliefBillOut extends UnReliefBill
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function init()
    {
        parent::init();
        $this->invoice = true;
        $this->lock_amounts = false;
    }

    public function defaultScope()
    {
        $alias = $this->getTableAlias(FALSE, FALSE);
        $bill_type = Bill::$UNRELIEF_BILL;
        return array(
            'condition' => "$alias.invoice=true and $alias.bill_type = '$bill_type' and $alias.canceled=false",
        );
    }

}