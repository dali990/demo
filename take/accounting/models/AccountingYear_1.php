<?php
/**
 * @author sasa
 */
class AccountingYear extends SIMAActiveRecord
{
    public static $BOOKING_MATERIALS_TYPE_AVARAGE_PRICE = 'AVERAGE_PRICE';
    public static $BOOKING_MATERIALS_TYPE_PLANNED_PRICE = 'PLANNED_PRICE';
    public static $BOOKING_MATERIALS_TYPES = [
        'AVERAGE_PRICE' => 'Po prosecnoj ceni',
        'PLANNED_PRICE' => 'Po planskoj ceni',
    ];


    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
 
    public function tableName()
    {
       return 'accounting.years';
    }
    
    public function moduleName() 
    {
        return 'accounting';
    }
    
    public function rules()
    {
        return array(
            array('year, account_layout_law_id, booking_materials_type, ios_date', 'required'),
            array('year', 'unsafe', 'on'=>['update']),
            array('confirmed, confirmed_timestamp, confirmed_user_id', 'safe'),
            array('start_booking_order_id, finish_booking_order_id, profit_calc_booking_order_id', 'safe'),
            array('kpr_next_count, kir_next_count', 'safe'),
            array('confirmed', 'checkConfirmed'),
            array('year', 'numerical', 'integerOnly' => TRUE,'max' => 2100,'min' => 1900),
            array('year', 'unique'),
            ['ios_date', 'checkIosDateHasSameYear']
        );
    }
    
    public function checkConfirmed()
    {
        if (!empty($this->id) && boolval($this->__old['confirmed']) !== boolval($this->confirmed) && boolval($this->confirmed) === true)
        {
            //moraju da budu svi nalozi potvrdjeni
            $unconfirmed_account_orders = AccountOrder::model()->countByAttributes([
                'year_id'=>$this->id,
                'booked'=>false
            ]);
            if ($unconfirmed_account_orders > 0)
            {
                $this->addError('confirmed', 'Da bi se potvrdila godina moraju svi nalozi biti potvrdjeni!');
            }
        }
    }
    
    public function __get($column)
    {
        switch ($column)
        {    
            case 'DisplayName': return $this->year;
            case 'SearchName': return $this->year;
            default: return parent::__get($column);
        }
    }
    
    public function relations($child_relations = [])
    {
        return array(            
            'account_layout_law' => array(self::BELONGS_TO, 'AccountLayoutLaw', 'account_layout_law_id'),
            'base_year' => array(self::BELONGS_TO, 'Year', 'id'),
            'confirmed_user' => array(self::BELONGS_TO, 'User', 'confirmed_user_id'),
            'start_booking_order' => array(self::BELONGS_TO, 'AccountOrder', 'start_booking_order_id'),
            'finish_booking_order' => array(self::BELONGS_TO, 'AccountOrder', 'finish_booking_order_id'),
            'profit_calc_booking_order' => array(self::BELONGS_TO, 'AccountOrder', 'profit_calc_booking_order_id'),
            'months' => [self::HAS_MANY,'Month','year_id']
        );
    }
    
    public function scopes()
    {
        $alias = $this->getTableAlias();
        return array(
            'byName' => ['order' => $alias.'.year DESC'],
            'byYear' => ['order' => $alias.'.year DESC'],
            'byYearASC' => ['order' => $alias.'.year ASC'],
        );
    }
    
    public function modelSettings($column)
    {        
        switch ($column)
        {
            case 'options': return ['form'];
            case 'filters': return [
                'year' => 'integer',
                'account_layout_law' => 'relation'
            ];
            case 'textSearch' : return [
                'year' => 'integer',
            ];
            case 'statuses' : return array(
                'confirmed' => array(
                    'title' => 'Potvrdjeno',
                    'timestamp' => 'confirmed_timestamp',
                    'user' => array('confirmed_user_id', 'relName' => 'confirmed_user'),
//                    'checkAccessConfirm' => 'checkConfirmAccess'
                ),
            );
            case 'number_fields': return [
                'kpr_next_count', 'kir_next_count'
            ];
            default: return parent::modelSettings($column);
        }
    }
    
//    public function checkConfirmAccess()
//    {
//        //moraju da budu svi nalozi potvrdjeni
//        $unconfirmed_account_orders = AccountOrder::model()->countByAttributes([
//            'year_id'=>$this->id,
//            'booked'=>false
//        ]);
//
//        return ($unconfirmed_account_orders > 0) ? ['Da bi se potvrdila godina moraju svi nalozi biti potvrdjeni!'] : true;
//    }
    
    public function beforeSave() 
    {
        if ($this->getIsNewRecord())
        {
            $this->id = Year::get($this->year)->id;
        }
        return parent::beforeSave();
    }
    
    /**
     * 
     * @return Year
     */
    public function getLastConfirmed()
    {        
        $last_confirmed_year = AccountingYear::model()->byName()->find(new SIMADbCriteria([
            'Model'=>'AccountingYear',
            'model_filter'=>[
                'confirmed'=>true
            ]
        ]));
        if (!is_null($last_confirmed_year))
        {
            return $last_confirmed_year->base_year->next();
        }
        else
        {
            $last_year = AccountingYear::model()->byYearASC()->find();
            if (!is_null($last_year))
            {
                return $last_year->base_year;
            }
            else
            {
                return Year::get();
            }
        }
    }
    
    public function getNextKPRCount()
    {
        $curent = $this->kpr_next_count;
        $this->kpr_next_count += 1;
        $this->save();
        return !empty($curent) ? $curent : 1;
    }
    
    public function getNextKIRCount()
    {
        $curent = $this->kir_next_count;
        $this->kir_next_count += 1;
        $this->save();
        return !empty($curent) ? $curent : 1;
    }
    
    public static function extractAccYearIdFromDate($year)
    {
        $date = new DateTime($year);
        $year = $date->format('Y');
        $accounting_year = AccountingYear::model()->findByAttributes(['year'=>$year]);
        return $accounting_year->id;
    }
    
    /**
     * 
     * @return array
     */
    public function getAccountingMonths() : array
    {
        return AccountingMonth::model()->findAll([
            'model_filter' => [
                'year' => $this->year
            ]
        ]);
    }
    
    /**
     * 
     * @return int
     */
    public function getAccountingMonthsCount() : int
    {
        return AccountingMonth::model()->count([
            'model_filter' => [
                'year' => $this->year
            ]
        ]);
    }
    
    public function checkIosDateHasSameYear() 
    {
        $year = date("Y", strtotime($this->ios_date));
        
        if ($year !== strval($this->year))
        {
            $this->addError('ios_date', Yii::t('AccountingModule.AccountingYear', 'IosDateNotSameAsYear'));
        }
    }
}
    