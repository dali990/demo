<?php

class BillCanceled extends Bill
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

// 	public function beforeSave() {
// 		$this->invoice = false;
// 		return parent::beforeSave();
// 	}

    public function defaultScope()
    {
        $alias = $this->getTableAlias(FALSE, FALSE);
        return array(
            'condition' => $alias . '.canceled=true',
        );
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'options':
                $options = parent::modelSettings($column);
                unset($options[array_search('cancel', $options)]);
                
                //TODO(MIloS:30.10.2018): brisanje racuna povlaci jos brdo stvari -> sta sa fajlom, sta ako je zaveden...
//                if (!isset($this->file->account_document))
//                {
//                    $options[] = 'delete';
//                }
                
                return $options;

            default: return parent::modelSettings($column);
        }
        
    }

}