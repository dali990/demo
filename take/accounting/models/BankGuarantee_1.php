<?php

class BankGuarantee extends SIMAActiveRecord 
{

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'accounting.bank_guarantee';
    }
    
    public function moduleName()
    {
        return 'accounting';
    }

    public function __get($column) {
        switch ($column) {
            default: return parent::__get($column);
        }
    }

    public function relations($child_relations = []) {
        return array(
            'file' => array(self::BELONGS_TO, 'File', 'id'),
            'bank' => array(self::BELONGS_TO, 'Bank', 'bank_id'),
            'order_by_partner' => array(self::BELONGS_TO, 'Partner', 'order_by'),
        );
    }

    public function rules() {
        return array(
            array('request_number, bank_id, order_by, expire_date, number, amount, type', 'required'),
            array('description', 'safe'),
        );
    }

    public function modelSettings($column) 
    {
        switch ($column)
        {
            case 'filters' : return array_merge(parent::modelSettings('filters'),array(
                'request_number' => 'text',
                'bank' => 'relation',
                'file' => 'relation',
                'order_by_partner' => 'relation',
                'expire_date' => 'date_range',
                'number' => 'text',
                'type' => 'dropdown'
            ));
//            case 'mutual_forms' : return array(
//                'base_relation'=>'file'
//            );
            default: return parent::modelSettings($column);
        }
    }
    
}