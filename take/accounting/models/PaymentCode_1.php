<?php

class PaymentCode extends SIMAActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'accounting.payment_codes';
    }

    public function moduleName()
    {
        return 'accounting';
    }

    public function __get($column)
    {
        switch ($column)
        {
            case 'DisplayName': 
            case 'SearchName': return $this->code . ' - ' . $this->name;
            default: return parent::__get($column);
        }
    }

    public function relations($child_relations = [])
    {
        return array(
            'payment_type' => array(self::BELONGS_TO, 'PaymentType', 'payment_type_id'),
//            'paychecks' => array(self::BELONGS_TO, 'PaycheckPeriod', 'paycheck_period_id'),
//            'paychecks' => array(self::HAS_MANY, 'Paychecks', 'paycheck_period_id'),
        );
    }

    public function rules()
    {
        return array(
            array('code, name', 'required'),
//            array('points', 'numerical', 'message' => 'Polje "{attribute}" mora biti broj!'),
            array('description, payment_type_id', 'safe'),
            array('code', 'numerical', 'integerOnly' => TRUE),
            array('code', 'length', 'min' => 3, 'max' => 3),
//            array('paycheck_period_id', 'default',
//                'value' => null,
//                'setOnEmpty' => true, 'on' => array('insert', 'update'))
        );
    }
    
    public function scopes()
    {
        $alias = $this->getTableAlias();
        return array(
            'byName'=>array(
                'order' => $alias.'.code'
            )
        );
    }

    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters' : return array_merge(parent::modelSettings($column),array(
                'name' => 'text',
                'description' => 'text',
                'code' => 'text',
            ));
            case 'textSearch' : return array(
                'name' => 'text',
                'code' => 'text',
            );
            default: return parent::modelSettings($column);
        }
    }

}