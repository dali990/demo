<?php

//Tabela koja spaja racune i olaksice
class BillRelief extends SIMAActiveRecord
{

//    public $amount_sum;
//    public $vat_sum;
// 	public $old_amount = 0;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'accounting.bill_reliefs';
    }

    public function moduleName()
    {
        return 'accounting';
    }

    public function relations($child_relations = [])
    {
        return array(
            'bill' => array(self::BELONGS_TO, 'Bill', 'bill_id'),
            'relief_bill' => array(self::BELONGS_TO, 'Bill', 'relief_bill_id')
        );
    }
    
    public function internalVatCode()
    {
        return Bill::internalVatCodeConvertor($this->internal_vat);
    }

    public function rules()
    {
        return array(
            array('bill_id, relief_bill_id', 'required'),
            array('amount, base_amount, vat_amount, vat_rate', 'required'),
            array('internal_vat', 'safe'),
            array('amount, base_amount, vat_amount', 'numerical'),
            array('amount', 'amountCheck', 'on' => array('insert', 'update')),
            array('bill_id, relief_bill_id', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => array('insert', 'update'))
        );
    }

    public function scopes()
    {
        $alias = $this->getTableAlias(false, false);
        return [
            'orderByReliefBill' => [
                'order' => "$alias.relief_bill_id"
            ],
        ];
    }
    
    public function amountCheck($attribute, $params)
    {
        if (!$this->hasErrors())
        {
            $bill = Bill::model()->findByPk($this->bill_id);
            $relief_bill = ReliefBill::model()->findByPk($this->relief_bill_id);
            $I_R_code = $this->internalVatCode();

            if ($bill->partner_id != $relief_bill->partner_id || $bill->invoice != $relief_bill->invoice)
            {
                $this->addError('bill_id', Yii::t('AccountingModule.BillRelease','Bill and Relief bill are not compatible'));
                $this->addError('relief_bill_id', Yii::t('AccountingModule.BillRelease','Bill and Relief bill are not compatible'));
            }
            
//            $amount_diff = floatval($this->amount)-floatval($this->__old['amount']);
//            $base_amount_diff = floatval($this->base_amount)-floatval($this->__old['base_amount']);
//            $vat_amount_diff = floatval($this->vat_amount)-floatval($this->__old['vat_amount']);
            
            $old_amount = 0;
            $old_base_amount = 0;
            $old_vat_amount = 0;
            if(!$this->isNewRecord)
            {
                $old_amount = $this->__old['amount'];
                $old_base_amount = $this->__old['base_amount'];
                $old_vat_amount = $this->__old['vat_amount'];
            }
            $amount_diff = floatval($this->amount)-floatval($old_amount);
            $base_amount_diff = floatval($this->base_amount)-floatval($old_base_amount);
            $vat_amount_diff = floatval($this->vat_amount)-floatval($old_vat_amount);
            
            $_sums_bill = $bill->remainingVatItemsArray();
            $_sums_rel =  $relief_bill->remainingVatItemsArray();
            
            $_bill_base = $_sums_bill[$I_R_code][$this->vat_rate]['base'];
            $_bill_vat = $_sums_bill[$I_R_code][$this->vat_rate]['vat'];
            if ($I_R_code === 'I')
            {
                $_bill_amount = $_bill_base;
            }
            else
            {
                $_bill_amount = $_bill_base + $_bill_vat;
            }
            
            if (SIMAMisc::lessThen($_bill_amount,$amount_diff))
            {
                $this->addError('amount', "nerasknjizeni deo racuna nije dovoljno velik");
            }
            if (SIMAMisc::lessThen($_bill_base,$base_amount_diff))
            {
                $this->addError('base_amount', "nerasknjizeni deo racuna nije dovoljno velik");
            }
            if (SIMAMisc::lessThen($_bill_vat,$vat_amount_diff))
            {
                $this->addError('vat_amount', "nerasknjizeni deo racuna nije dovoljno velik");
            }
            
            $_rel_bill_base = $_sums_rel[$I_R_code][$this->vat_rate]['base'];
            $_rel_bill_vat = $_sums_rel[$I_R_code][$this->vat_rate]['vat'];
            if ($I_R_code === 'I')
            {
                $_rel_bill_amount = $_rel_bill_base;
            }
            else
            {
                $_rel_bill_amount = $_rel_bill_base + $_rel_bill_vat;
            }
            
            if (SIMAMisc::lessThen(-$_rel_bill_amount, -$amount_diff))
            {
                $this->addError('amount', "nerasknjizeni deo knjiznog odobrenja nije dovoljno velik");
            }
            if (SIMAMisc::lessThen(-$_rel_bill_base, -$base_amount_diff))
            {
                $this->addError('base_amount', "nerasknjizeni deo knjiznog odobrenja nije dovoljno velik");
            }
            if (SIMAMisc::lessThen(-$_rel_bill_vat, -$vat_amount_diff))
            {
                $this->addError('vat_amount', "nerasknjizeni deo knjiznog odobrenja nije dovoljno velik");
            }
        }
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters':return [
                'bill' => 'relation',
                'relief_bill' => 'relation'
            ];
            case 'number_fields': return [
                'amount','base_amount','vat_amount'
            ];
            case 'update_relations' : return ['bill','relief_bill'];
            case 'multiselect_options': return ['delete'];
            default: return parent::modelSettings($column);
        }
        
    }

}
