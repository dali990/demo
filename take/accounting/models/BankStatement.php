<?php
/**
 * Description of BankStatement
 *
 * @author goran-set
 */
class BankStatement extends SIMAActiveRecord
{
    public $file_version_id;
    
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'accounting.bank_statements';
    }

    public function moduleName()
    {
        return 'accounting';
    }
    
    public function __get($column)
    {
        switch ($column)
        {
            case 'path2': return 'files/file';
            case 'DisplayName':		return $this->account->DisplayName.' - '.$this->number;
            case 'SearchName':		return $this->DisplayName;
            default: return parent::__get($column);
        }
    }
    
    public function relations($child_relations = [])
    {
        $default_payment_type = Yii::app()->configManager->get('accounting.default_payment_type');
        return array(
            'file' => array(self::BELONGS_TO, 'File', 'id'),
            'account' => array(self::BELONGS_TO, 'BankAccount', 'account_id'),
            'payments' => array(self::HAS_MANY, 'Payment', 'bank_statement_id',
                'scopes' => ['inBankStatement']),
            'unreleased' => array(self::STAT, 'Payment', 'bank_statement_id',
                'select' => 'sum(unreleased_amount)',
                'condition' => "payment_type_id = $default_payment_type and recurring_bill_id IS NULL",
            ),
            'year' => array(self::BELONGS_TO, 'Year', 'year_id'),
        );
    }
    
    public function rules()
    {
        return array(
            array('account_id, date, amount, number', 'required'),
            array('confirm1, confirm2,id,file_version_id, year_id', 'safe'),
            array('amount', 'numerical'),
            ['number', 'checkNumberUniq'],
            array('account_id', 'checkUniqCustom'),
            array('account_id, id, year_id', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => array('insert', 'update')),
        );
    }
    
    public function checkNumberUniq()
    {
        if (!$this->hasErrors())
        {
            //proveravamo koji je datum i na osnovu toga upisujemo godinu u kolonu year_id
            if ($this->year_id === null)
            {
                $date = new DateTime($this->date);
                $this->year_id = Year::get($date->format("Y"))->id;
            }
            $this->unique_with('number', ['with' => ['year_id', 'account_id']]);
        }
    }
    
//    public function checkUniqCustom($attribute)
    public function checkUniqCustom()
    {
        $bs = BankStatement::model()->findByAttributes(array(
            'account_id' => $this->account_id,
            'date' => $this->date
        ));
        if ($bs != null && ($bs->id!=$this->id))
        {
            $this->addError ('account_id', 'Zadata banka vec ima unet izvod za zadati datum');
        }
    }
    
    public function getClasses()
    {
        return parent::getClasses() .
                ((abs($this->amount - $this->calcAmount) > 0.001) ? ' not_const' : '') .
                ((abs($this->unreleased) > 0.001) ? ' have_unreleased' : '');
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters' : return array_merge(parent::modelSettings($column),array(
                'account_id' => 'dropdown',
                'date' => 'date_range',
                'year' => 'relation',
                'file' => 'relation',
                'account' => 'relation',
                'payments' => 'relation',
            ));
            case 'textSearch' : return array(
                'account' => 'relation',
            );
            case 'options' : return array('form', 'open', 'download');
            case 'mutual_forms': return ['weak_base_relation' => 'file'];
            default: return parent::modelSettings($column);
        }
    }
    
    public function droplist($key)
    {
        switch ($key)
        {
            case 'account_id': return BankAccount::model()->forPartner(Yii::app()->Company->id)->getModelDropList();
            default: return parent::droplist($key);
        }
    }
    
    public function scopes()
    {
        return array(
            'recently' => array(
                'order' => 'date DESC, id',
            ),
        );
    }
    
    public function beforeSave()
    {
        if (!isset($this->file))
        {
            $file_name = $this->account->bank->short_name;
            $file_name .= '_'.$this->year->year;
            $file_name .= '_'.$this->number;
            $file_name .= '_'.$this->date;

            
            $document_type_id=Yii::app()->configManager->get('accounting.bank_statement',false);
            $file = new File();
            $file->name = $file_name;
            $file->document_type_id=$document_type_id;
            $file->save();
            $file->refresh();
            $this->id = $file->id;
            $this->file = $file;
        }
        
        /** ubacivanje upload-a fajla */
        $this->file->appendUpload($this->file_version_id);
        
        return parent::beforeSave();
    }

    public function afterSave()
    {
        /** kacenje FileTag-a filing_book */
//        if (isset($this->file))
//        {
//            $this->file->addTag('bank_statement');
//        }
        return parent::afterSave();
    }

    public function getprevAmount()
    {
        $criteria = new CDbCriteria();
        $criteria->condition = "date < :this_date and account_id=:account_id";
        $criteria->params = array(':this_date' => SIMAHtml::UserToDbDate($this->date), ':account_id' => $this->account_id);
        $criteria->order = 'date DESC';

        $prev_statement = BankStatement::model()->find($criteria);
        if ($prev_statement == null)
        {
            $new_amount = $this->account->start_amount;
        }
        else
        {
            $new_amount = $prev_statement->amount;
        }

        return $new_amount;
    }

    public function getcalcAmount()
    {
        $new_amount = $this->prevAmount;

        foreach ($this->payments as $payment)
        {
            if ($payment->invoice)
            {
                $new_amount+=$payment->amount;
            }
            else
            {
                $new_amount-=$payment->amount;
            }
        }

        return $new_amount;
    }
    
    public function getconfirm1()
    {
        $res = true;
        $default_payment_type = Yii::app()->configManager->get('accounting.default_payment_type');
        foreach ($this->payments as $payment)
        {
            $res = 
                    $res 
                    && 
                    (
                        ($payment->payment_type_id != $default_payment_type && $payment->confirm1) 
                        || 
                        ($payment->payment_type_id == $default_payment_type && $payment->connected)
                    );
        }

        return $res;
    }

    public function getconfirm2()
    {
        $res = true;
        $default_payment_type = Yii::app()->configManager->get('accounting.default_payment_type');
        foreach ($this->payments as $payment)
        {
            $res = $res 
                    && 
                    (
                        ($payment->payment_type_id != $default_payment_type && $payment->confirm2) 
                        || 
                        ($payment->payment_type_id == $default_payment_type && $payment->connected)
                    );
        }

        return $res;
    }
}
