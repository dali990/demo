<?php

//Tabela koja spaja racune i olaksice
class AdvanceBillFictiveAccountLink extends SIMAActiveRecord
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'accounting.advance_bill_fictive_account_link';
    }
    
    public function moduleName()
    {
        return 'accounting';
    }

    public function relations($child_relations = [])
    {
        return [
            'advance_bill' => array(self::BELONGS_TO, 'Bill', 'advance_bill_id'),
            'account_transaction' => array(self::BELONGS_TO, 'AccountTransaction', 'account_transaction_id'),
        ];
    }

    public function rules()
    {
        return array(
            array('account_transaction_id, advance_bill_id', 'required'),
            array('value', 'required'),
            array('value', 'numerical'),
//            array('value', 'valueCheck', 'on' => array('insert', 'update')),
            array('account_transaction_id, advance_bill_id', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => array('insert', 'update'))
        );
    }

//    public function valueCheck()
//    {
//        if (!$this->hasErrors())
//        {
//            
//        }
//    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters':return [
                'account_transaction' => 'relation',
                'advance_bill' => 'relation'
            ];
            case 'number_fields': return [
                'value'
            ];
            case 'update_relations' : return ['account_transaction','advance_bill'];
            default: return parent::modelSettings($column);
        }
        
    }
    
}
