<?php

class Ios extends SIMAActiveRecord
{
    public $ios_year_id = "";//sluzi za prosledjivanje izabrane godine
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'accounting.ioses';
    }

    public function moduleName()
    {
        return 'accounting';
    }

    public function __get($column)
    {
        switch ($column)
        {
            case 'DisplayName': 
            case 'SearchName': 
            {
                $return = 'IOS - '.$this->partner->DisplayName.' '.$this->ios_date;
                return $return;                
            }
            case 'path2': return 'files/file';
            default: return parent::__get($column);
        }
    }

    public function relations($child_relations = [])
    {
        return array(
            'file' => array(self::BELONGS_TO, 'File', 'id'),
            'account' => array(self::BELONGS_TO, 'Account', 'account_id'),
            'partner_agreed_user' => array(self::BELONGS_TO, 'User', 'partner_agreed_user_id'),
            'partner' => array(self::BELONGS_TO, Partner::class, 'partner_id'),
            'ios_items' => [self::HAS_MANY, 'IosItem', 'ios_id'],
            'ios_items_opened' => [self::HAS_MANY, 'IosItem', 'ios_id', 'scopes' => ['opened', 'byDateRelation']],
            'credit' => [self::STAT, 'IosItem', 'ios_id', 'select' => 'sum(credit)', 'scopes' => ['opened']],
            'debit' => [self::STAT, 'IosItem', 'ios_id', 'select' => 'sum(debit)', 'scopes' => ['opened']],
            'saldo' => [self::STAT, 'IosItem', 'ios_id', 'select' => 'sum(debit-credit)', 'scopes' => ['opened']],
            'credit_total' => [self::STAT, 'IosItem', 'ios_id', 'select' => 'sum(credit)'],
            'debit_total' => [self::STAT, 'IosItem', 'ios_id', 'select' => 'sum(debit)'],
        );
    }
    
    public function rules()
    {
        return array(
            array('partner_id, ios_date, account_id', 'required'),
            array('partner_id, ios_date, account_id', 'check_unique'),
            array('id, partner_agreed, partner_agreed_user_id, partner_agreed_timestamp, partner_agreed_diff, ios_year_id', 'safe'),
            ['ios_date', 'checkIosDateHasSameAccountingYear', 'on'=>['insert', 'update']],
            ['partner_id, ios_date, account_id', 'noChange', 'on' => ['update']]
        );
    }
    
    public function checkIosDateHasSameAccountingYear() 
    {
        if($this->isNewRecord)
        {
            $year = date("Y", strtotime($this->ios_date));
            $selected_ios_year = Year::model()->findByPkWithCheck($this->ios_year_id)->year;
            $account_year = $this->account->year->year;
            if ($year !== strval($selected_ios_year))
            {
                $this->addError('ios_date', Yii::t('AccountingModule.AccountingYear', 'IosDateNotSameAsYear'));
            }
            else if(($year !== strval($account_year)))
            {
                $this->addError('ios_date', Yii::t('AccountingModule.AccountingYear', 'IosDateNotSameKontoYear'));
            }
        }
    }
    
    public function getAccount_with_dc_currents()
    {
        $account = Account::model()->find(new SIMADbCriteria([
            'Model'=>'Account',
            'model_filter'=>[
                'ids'=>$this->account_id,
                'display_scopes'=>[
                    'withDCCurrents'=>[$this->account->year_id]
                ]
            ]
        ]));

        return $account;
    }
    
    public function check_unique()
    {
        if(($this->isNewRecord) || ($this->__old['partner_id']!==$this->partner_id) || ($this->__old['account_id']!==$this->account_id) || ($this->__old['ios_date']!==$this->ios_date))
        {
            $account_exists = Ios::model()->findByAttributes([
                'partner_id' => $this->partner_id,
                'account_id' => $this->account_id,
                'ios_date' => $this->ios_date,
            ]);
            if(!empty($account_exists) && 
                    ((!empty($this->id) && $account_exists->id !== $this->id)
                        ||
                    empty($this->id)))
            {
                throw new SIMAWarnException("Za kompaniju $this->partner, sa kontom $this->account, za datum $this->ios_date je vec generisan IOS!");
            }
        }
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters': return array(
                'ios_date' => 'date_range',
                'account' => 'relation',
                'account_id' => 'dropdown',
                'partner' => 'relation',
                'partner_id' => 'dropdown',
                'partner_agreed_diff' => 'numeric',
                'partner_agreed' => 'boolean',
                'partner_agreed_timestamp' => 'date_range',
                'partner_agreed_user' => 'relation',
            );
            case 'textSearch': return array(
                'ios_date' => 'date_range',
                'account' => 'relation',
                'partner' => 'relation',
                'partner_agreed' => 'boolean',
            );
            case 'statuses':  return array(
                'partner_agreed' => array(
                    'title' => 'Potvrđena saglasnost',
                    'timestamp' => 'partner_agreed_timestamp',
                    'user' => array('partner_agreed_user_id','relName'=>'partner_agreed_user'),
                    'checkAccessConfirm' => 'completedConfirm',
                    'checkAccessRevert' => 'completedRevert'
                )
            );
            case 'number_fields': return [
                'debit', 'credit', 'saldo'
            ];
            case 'multiselect_options': return ['delete'];
            case 'options': return ['delete','form','open','download'];
            default : return parent::modelSettings($column);
        }
    }
    
    public function completedConfirm()
    {
        $msgs = [];
        if ( !Yii::app()->user->checkAccess('modelIosConfirm') )
        {
            $msgs[] = 'Nemate privilegiju da potvrdite Izvod otvorenih stavki';
        }
        
        return (count($msgs)>0)?$msgs:true;
    }
    
    public function completedRevert()
    {
        $msgs = [];
        if ( !Yii::app()->user->checkAccess('modelIosConfirm') )
        {
            $msgs[] = 'Nemate privilegiju da uklonite potvrdu za Izvod otvorenih stavki';
        }
        
        return (count($msgs)>0)?$msgs:true;
    }
    
    public function beforeSave() 
    {
        if($this->isNewRecord && (!isset($this->file)))
        {
            $file = new File();
            $file->name = $this->DisplayName;
            
            if(!$file->save())
            {
                throw new Exception('Cuvanje fajla u Ios modelu nije uspelo.');
            }
            $this->id = $file->id;
            $this->file = $file;
        }
        
        return parent::beforeSave();
    }
    
    public function beforeDelete()
    {
        $ios_items = IosItem::model()->findAllByAttributes(['ios_id'=>$this->id]);
        foreach($ios_items as $ios_item)
        {
            $ios_item->delete();
        }
        
        return parent::beforeDelete();
    }
}
