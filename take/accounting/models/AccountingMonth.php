<?php

class AccountingMonth extends SIMAActiveRecord
{
    private $nullableParameters = array(
        'point_value', 'hot_meal_per_hour', 'regres', 'exp_perc', 'pio_empl', 'zdr_empl', 'tax_empl', 
        'tax_release', 'pio_comp', 'zdr_comp', 'nez_comp', 'nez_empl',
        'tax_int', 'pio_int', 'zdr_int', 'nez_int', 'min_contribution_base'
    );
    public $_monthFrom = null;
    public $_monthTo = null;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'accounting.months';
    }

    public function moduleName()
    {
        return 'accounting';
    }
    
    public function __isset($column)
    {
        switch ($column)
        {
            case 'KPRs': case 'KIRs': return true;
            default: return parent::__isset($column);
        }
        
    }
    
    public function __get($column)
    {
        switch ($column)
        {
            case 'DisplayName': return $this->base_month->DisplayName;
            case 'SearchName': return $this->DisplayName;
            case 'monthFrom': return $this->_monthFrom;
            case 'monthTo': return $this->_monthTo;
            case 'KPR_base_20': 
            case 'KPR_vat_20': 
            case 'KPR_base_10': 
            case 'KPR_vat_10': 
            case 'KPR_vat_total': 
            case 'KPR_income': 
                return $this->KPRsums[$column];
            case 'KIR_base_20': 
            case 'KIR_vat_20': 
            case 'KIR_base_10': 
            case 'KIR_vat_10': 
            case 'KIR_vat_total': 
            case 'KIR_income': 
                return $this->KIRsums[$column];
            default: return parent::__get($column);
        }
    }
    
    public function __set($name, $value)
    {           
        switch ($name)
        {
            case 'monthFrom': $this->_monthFrom = $value; break;
            case 'monthTo': $this->_monthTo = $value; break;
            default: parent::__set($name, $value); break;
        }
    }
    
    public function relations($child_relations = [])
    {
        return array(
            'vat_account_document' => array(self::BELONGS_TO, 'AccountOrder', 'vat_account_document_id'),
            'paycheck_periods' => array(self::HAS_MANY, 'PaycheckPeriod', 'month_id'),
            'base_month' => array(self::BELONGS_TO, 'Month', 'id'),
        );
    }

    public function rules()
    {
        return array(
            array('month, year', 'required', 'on'=>array('insert')),
            array('point_value, hot_meal_per_hour, regres, exp_perc, pio_empl, zdr_empl, '
                    . 'nez_empl, tax_empl, tax_release, pio_comp, zdr_comp, nez_comp, tax_int, '
                    . 'pio_int, zdr_int, nez_int, min_contribution_base', 'numerical'),
            array('month', 'numerical', 'integerOnly' => TRUE,'max' => 12,'min' => 1),
            array('year', 'numerical', 'integerOnly' => TRUE,'max' => 2100,'min' => 1900),
            ['month', 'checkNumericNumber', 'precision' => 2, 'scale' => 0],
            ['year', 'checkNumericNumber', 'precision' => 4, 'scale' => 0],
            ['point_value', 'checkNumericNumber', 'precision' => 6, 'scale' => 2],
//            ['hours', 'checkNumericNumber', 'precision' => 4, 'scale' => 0],
            ['hot_meal_per_hour', 'checkNumericNumber', 'precision' => 3, 'scale' => 0],
            ['regres', 'checkNumericNumber', 'precision' => 6, 'scale' => 0],
            ['exp_perc', 'checkNumericNumber', 'precision' => 5, 'scale' => 2],
            ['pio_empl', 'checkNumericNumber', 'precision' => 5, 'scale' => 2],
            ['zdr_empl', 'checkNumericNumber', 'precision' => 5, 'scale' => 2],
            ['tax_empl', 'checkNumericNumber', 'precision' => 5, 'scale' => 2],
            ['tax_release', 'checkNumericNumber', 'precision' => 8, 'scale' => 0],
            ['pio_comp', 'checkNumericNumber', 'precision' => 5, 'scale' => 2],
            ['zdr_comp', 'checkNumericNumber', 'precision' => 5, 'scale' => 2],
            ['nez_comp', 'checkNumericNumber', 'precision' => 5, 'scale' => 2],
            ['tax_int', 'checkNumericNumber', 'precision' => 5, 'scale' => 2],
            ['pio_int', 'checkNumericNumber', 'precision' => 5, 'scale' => 2],
            ['zdr_int', 'checkNumericNumber', 'precision' => 5, 'scale' => 2],
            ['nez_int', 'checkNumericNumber', 'precision' => 5, 'scale' => 2],
            ['min_contribution_base', 'checkNumericNumber', 'precision' => 10, 'scale' => 2],
            array('point_value', 'checkNullableParameters'),
            array('month', 'checkMonthYearUnique'),
            array('vat_account_document_id', 'unique'),
            array('monthFrom, monthTo', 'safe')
        );
    }
    
    public function checkNumericNumber($attribute, $params)
    {
        if (!is_null($this->$attribute) && SIMAMisc::isValidNumericNumber($this->$attribute, $params['precision'], $params['scale']) === false)
        {
            $this->addError($attribute, Yii::t('AccountingModule.AccountingMonth', 'NotValidNumericNumber', [
                '{precision}' => $params['precision'],
                '{scale}' => $params['scale']
            ]));
        }
    }
    
    public function scopes()
    {
        $alias = $this->getTableAlias();
        return array(
            'byOfficial' => array('order' => "$alias.year, $alias.month"),
            'byOfficialDESC' => array('order' => "$alias.year DESC, $alias.month DESC"),
        );
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters': return array(
                'year' => 'dropdown',
                'month' => 'dropdown'
            );
            case 'textSearch' : return [
                'year' => 'integer',
                'month' => 'integer'
            ];
            case 'options' : return array('form');
            case 'default_order_scope': return 'byOfficialDESC';
            default: return parent::modelSettings($column);
        }
    }
    
    public function beforeSave() 
    {
        $this->setThisMonthIdIfNewRecord();
        $this->populateNullableParametersFromPreviousMonth();

        return parent::beforeSave();
    }
    
    public function AfterSave()
    {  
        if ($this->getIsNewRecord())
        {
            Yii::app()->warnManager->sendWarns('HRInsufficientDataForEmployees', 'WARN_INSUFFICIENT_DATA_EMPOLOYEES');
        }
        
        parent::AfterSave();
        
        $this->afterSave_paycheckPeriodModification();
    }
    
    /// svi isplatam u ovom mesecu da se obelezi vreme modifikacije
    private function afterSave_paycheckPeriodModification()
    {
        $paycheck_periods_in_this_month = PaycheckPeriod::model()->findAll([
            'model_filter' => [
                'month' => [
                    'ids' => $this->id
                ]
            ]
        ]);
        if(empty($paycheck_periods_in_this_month))
        {
            return;
        }
        
        $columns_whose_changes_to_ignore = [
            'vat_account_document_id'
        ];
        $confirmed_period_changes = [];
        $columns = $this->getMetaData()->tableSchema->columns;
        foreach($columns as $column)
        {
            $column_name = $column->name;
            
            if(in_array($column_name, $columns_whose_changes_to_ignore))
            {
                continue;
            }
            
            if($this->columnChanged($column_name))
            {
                /**
                 * TODO
                 * radi se formatiranje jer ako se vrsi promena vrednosti sa npr 10 na 12
                 * old ima vrednost 10.00
                 * new ima vrednost 12
                 * ovako oba imaju isto formatiranje
                 * mora ovako, jer ne moze da se stavi numberfield jer
                 *  polje mora da ima placeholder
                 *  numberfield nema podrsku za placeholder
                 */
                $temp_old_am = new AccountingMonth();
                $temp_old_am->$column_name = $this->__old[$column_name];
                $old_display_val = $temp_old_am->getAttributeDisplay($column_name);
                $new_display_val = $this->getAttributeDisplay($column_name);
                $confirmed_period_changes[] = 'Promenjena vrednost za '.$this->getAttributeLabel($column_name).' - stara: '.$old_display_val.' - nova: '.$new_display_val;
            }
        }
        foreach($paycheck_periods_in_this_month as $paycheck_period_in_this_month)
        {
            $paycheck_period_in_this_month->updateLastModificationTime();
            $paycheck_period_in_this_month->confirmed_period_changes = $confirmed_period_changes;
            $paycheck_period_in_this_month->update(['last_modification_time']);
        }
    }
    
    private function setThisMonthIdIfNewRecord()
    {
        if ($this->getIsNewRecord())
        {
            $this->id = Month::get($this->month, $this->year)->id;
        }
    }
    
    public function checkNullableParameters()
    {
        if (!$this->hasErrors())
        {
            if(isset($this->month) && $this->year)
            {
                $previousAccountingMonth = $this->getFirstPreviousAccountingMonth();

                if(!isset($previousAccountingMonth))
                {
                    foreach($this->nullableParameters as $nullableParameter)
                    {
                        if(!isset($this->$nullableParameter))
                        {
                            $this->addError($nullableParameter, Yii::t('AccountingModule.AccountingMonth','You need to enter at least one accounting month before this date'));
                        }
                    }
                }
            }
        }
    }
    
    public function checkMonthYearUnique()
    {
        if($this->getIsNewRecord() && isset($this->month) && isset($this->year))
        {
            $criteria = new SIMADbCriteria([
                'Model' => 'AccountingMonth',
                'model_filter' => [
                    'month' => $this->month,
                    'year' => $this->year,
                ]
            ]);
            $count = AccountingMonth::model()->count($criteria);
            
            if($count > 0)
            {
                $this->addError('month', Yii::t('AccountingModule.AccountingMonth','MonthPeriodExists'));
                $this->addError('year', Yii::t('AccountingModule.AccountingMonth','MonthPeriodExists'));
            }
        }
    }
    
    private function populateNullableParametersFromPreviousMonth()
    {
        $previousAccountingMonth = $this->getFirstPreviousAccountingMonth();
        
        foreach($this->nullableParameters as $nullableParameter)
        {
            if(!isset($this->$nullableParameter))
            {
                $this->$nullableParameter = $previousAccountingMonth->$nullableParameter;
            }
        }
    }
    
    private function getFirstPreviousAccountingMonth()
    {
        $criteria = new SIMADbCriteria();
        $criteria->condition = "(year||'-'||month||'-'||'1')::date<'$this->year-$this->month-1'::date";
        $criteria->order = 'year desc, month desc';
        $criteria->limit = '1';
        $previousAccountingMonth = AccountingMonth::model()->find($criteria);
        
        return $previousAccountingMonth;
    }
    
    public static function previousNmonths($limitN)
    {
        $months = array();
        
        $month = Month::current();
        for($i=0; $i<$limitN; $i++)
        {
            $month = $month->prev();
            $months[] = $month;
        }
        
        return $months;
    }
    
    public static function getByID($id)
    {
        $a_month = AccountingMonth::model()->findByPk($id);
        if (!isset($a_month))
        {
            $month = Month::model()->findByPk($id);
            $a_month = new AccountingMonth();
//            $a_month->id = $id;
            $a_month->month = $month->month;
            $a_month->year = $month->year->year;
            $a_month->save();
            $a_month->refresh();
        }
        return $a_month;
    }
}