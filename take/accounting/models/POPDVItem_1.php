<?php

class POPDVItem extends SIMAActiveRecord
{
    
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'accounting.popdv_items';
    }

    public function moduleName()
    {
        return 'accounting';
    }

    public function __get($column)
    {
        switch ($column)
        {
            case 'DisplayName': return $this->param_code.' '.$this->param_value;
            case 'SearchName': return $this->param_code.' '.$this->param_value.' - '.$this->note;
                
            default: return parent::__get($column);
        }
    }


    public function relations($child_relations = [])
    {
        return array(
            'account_document' => array(self::BELONGS_TO, 'AccountDocument', 'account_document_id')
        );
    }
    
    public function rules()
    {
        return array(
            array('account_document_id, param_code, param_value', 'required'),
            array('param_code', 'unique_with', 'with' => 'account_document_id'),
            array('note, account_id', 'safe'),
        );
    }
    
    public function scopes()
    {
        $alias = $this->getTableAlias();
        return array(
            'byCode' => array(
                'order' => $alias.'.param_code'
            )
        );
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters': return [
                'account_document' => 'relation',
                'param_code' => 'dropdown',
                'note' => 'test',
            ];
            case 'textSearch': return array(
                'account_document_id' => 'relation',
                'note' => 'test',
            );  
            default : return parent::modelSettings($column);
        }
    }
    
}