<?php

class DebtTakeOver extends SIMAActiveRecord
{    
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'accounting.debt_take_overs';
    }

    public function moduleName()
    {
        return 'accounting';
    }

    public function __get($column)
    {
        switch ($column)
        {
            case 'DisplayName': return $this->file->DisplayName;
            case 'isSystemCompanyCreditor':
                return Yii::app()->company->id === $this->creditor_id;
            case 'isSystemCompanyDebtor':
                return Yii::app()->company->id === $this->debtor_id;
            case 'isSystemCompanyTakesOver':
                return Yii::app()->company->id === $this->takes_over_id;
            default: return parent::__get($column);
        }
    }

    public function relations($child_relations = [])
    {
        return [
            'file' => [self::BELONGS_TO, 'File', 'id'],
            'debtor' => [self::BELONGS_TO, 'Partner', 'debtor_id'],
            'creditor' => [self::BELONGS_TO, 'Partner', 'creditor_id'],
            'takes_over' => [self::BELONGS_TO, 'Partner', 'takes_over_id'],
            'year' => [self::BELONGS_TO, 'Year', 'year_id'],
        ];
    }
    
    public function scopes()
    {
        $alias = $this->getTableAlias();
        $system_company_id = Yii::app()->company->id;
        return [
            'unreleased' => [
                
            ],
            'systemCompanyCreditor' => [
                'condition' => $alias.'.creditor_id = :creditor_id',
                'params' => [':creditor_id' => $system_company_id]
            ],
            'systemCompanyDebtor' => [
                'condition' => $alias.'.debtor_id = :debtor_id',
                'params' => [':debtor_id' => $system_company_id]
            ],
            'systemCompanyTakesOver' => [
                'condition' => $alias.'.takes_over_id = :takes_over_id',
                'params' => [':takes_over_id' => $system_company_id]
            ]
        ];
    }

    public function rules()
    {
        return [
            ['debtor_id, creditor_id, takes_over_id, sign_date', 'required'],
            ['id','uniqPartners'],
            ['id','oneLocalPartner'],
            ['debt_value','numerical','min'=>0.01],
            ['sign_date', 'date', 'format' => Yii::app()->params['date_format']],
            ['debtor_id, creditor_id, takes_over_id', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => ['insert', 'update']],
        ];
    }
    
    public function uniqPartners()
    {
        if (!$this->hasErrors())
        {
            $not_uniq = [];
            if ($this->debtor_id == $this->creditor_id)
            {
                $not_uniq = ['debtor_id','creditor_id'];
            }
            if ($this->debtor_id == $this->takes_over_id)
            {
                $not_uniq[] = 'debtor_id';
                $not_uniq[] = 'takes_over_id';
            }
            if ($this->creditor_id == $this->takes_over_id)
            {
                $not_uniq[] = 'creditor_id';
                $not_uniq[] = 'takes_over_id';
            }
            $_msg = Yii::t('AccountingModule.DebtTakeOver','UniqPartners');
            foreach (array_unique($not_uniq) as $_item)
            {
                $this->addError($_item, $_msg);
            }
        }
    }
    public function oneLocalPartner()
    {
        if (!$this->hasErrors())
        {
            $local_company_id = Yii::app()->company->id;
            if ($this->debtor_id != $local_company_id
                &&
                $this->creditor_id != $local_company_id
                &&
                $this->takes_over_id != $local_company_id)
            {
                $company_name = Yii::app()->company->DisplayName;
                $_msg = Yii::t('AccountingModule.DebtTakeOver','OneLocalPartner',['{local_partner_name}' => $company_name]);
                $this->addError('debtor_id', $_msg);
                $this->addError('takes_over_id', $_msg);
                $this->addError('creditor_id', $_msg);
            }
        }
    }
    
    protected function modelOptions(\User $user = null):array
    {
//        if (isset($this->file))
//        {
//            return [];//$this->file->getModelOptions($user);
//        }
//        else 
        {
            return ['open','form'];
        }
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters': return [
                'file' => 'relation',
                'debtor' => 'relation',
                'creditor' => 'relation',
                'takes_over' => 'relation',
                'debt_value' => 'numeric',
                'sign_date' => 'date_range',
            ];
            case 'textSearch': return [
                'file' => 'relation',
                'debtor' => 'relation',
                'creditor' => 'relation',
                'takes_over' => 'relation',
            ];
            case 'mutual_forms': return [
                'base_relation' => 'file',
            ];
            case 'number_fields': return ['debt_value'];
                    
            default : return parent::modelSettings($column);
        }
    }    
    
    
}