<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class AccountDocument extends SIMAActiveRecord
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
 
    public function tableName()
    {
        return 'accounting.account_documents';
    }
    
    public function moduleName()
    {
        return 'accounting';
    }
    
    public function __get($column)
    {
        switch ($column)
        {
            case 'path2': return 'files/file';
            case 'DisplayName': return isset($this->file)?$this->file->DisplayName:'';
            case 'SearchName': return isset($this->file)?$this->file->SearchName:'';
            case 'isBooked': return isset($this->account_order) && $this->account_order->booked;
            default: return parent::__get($column);
        }
    }
    
    public function rules()
    {
        return array(
            array('id, account_order_id', 'required'),
            ['account_order_id','checkAccountOrderWarehouse', 'on' => ['insert','update']],
            ['account_order_id','relationNotConfirmed', 'on' => ['insert','update']],
            ['canceled_account_order_id','relationNotConfirmed', 'on' => ['insert','update']],
            ['id', 'unique', 
                'message' => Yii::t('AccountingModule.AccountDocument','AlreadyOnAccountOrder',[
                    '{document_name}' => $this->DisplayName
                ]), 
                'on' => ['insert','update']],
            array('account_order_id','checkSameYear'),
            ['id','noItemsCheck','on'=>['delete']]
        );
    }
    
    public function relationNotConfirmed($attribute)
    {
        if (!$this->hasErrors() && $this->columnChanged($attribute))
        {
            $order_cnt = AccountOrder::model()->countByAttributes([
                'id' => $this->$attribute,
                'booked' => true
            ]);
            if ($order_cnt>0)
            {
                $this->addError($attribute, "Dokument ne moze da se doda na potvrdjen nalog");
            }
        }
    }
    
    public function checkAccountOrderWarehouse($attribute)
    {
        if (!$this->hasErrors($attribute))
        {
            try
            {
                $_ao = $this->account_order;
                if (isset($_ao))
                {
                    $_ao->attachBehavior('DocumentCombination', 'AccountOrderDocumentCombinationBehavior');
                    $_ao->hasRegularAccountDocumentsCombination([$this]);
                }
            }
            catch (SIMAWarnException $e)
            {
                $this->addError($attribute, $e->getMessage());
            }
        }
    }
    
    public function noItemsCheck()
    {
        if ($this->account_transactions_count > 0)
        {
            $this->addError('id',Yii::t('AccountingModule.AccountDocument','RemoveAllItems'));
        }
    }
    
    public function checkSameYear()
    {
        $old_AO = null;
        if(!$this->isNewRecord)
        {
            $old_AO = AccountOrder::model()->findByPk($this->__old['account_order_id']);
        }
        $new_AO = AccountOrder::model()->findByPk($this->account_order_id);
        if (!is_null($old_AO))
        {
            if (is_null($new_AO))
            {
                $this->addError('account_order_id',"Nije dobro zadat Nalog za knjizenje");
            }
            else
            {
                if ($this->account_transactions_count>0)
                {
                    $date1 = new DateTime($new_AO->date);
                    $date2 = new DateTime($old_AO->date);
                    $date1_str = $date1->format("Y");
                    $date2_str = $date2->format("Y");
                    if ($date1_str !== $date2_str)
                    {
                        $this->addError('date',"Datum mora da bude u istoj godini ($date2_str)");
                    }
                }
            }
        }
    }
    
    public function getVatInMonth()
    {
        $bill = AnyBill::model()->withVatInMonth()->findByPk($this->id);
        if (!is_null($bill))
        {
            $month = $bill->vat_in_month;
        }
        else
        {
            $month = $this->account_order->month;
        }
        
        return $month;
    }
    public function getCanceledInMonth()
    {
        $bill = AnyBill::model()->withVatInMonth()->findByPk($this->id);
        if (!is_null($bill))
        {
            $month = $bill->canceled_in_month;
        }
        else
        {
            $month = null;
        }
        
        return $month;
    }
    
    public function relations($child_relations = []) 
    {
        return array(
            'account_order' => array(self::BELONGS_TO, 'AccountOrder', 'account_order_id'),
            'canceled_account_order' => array(self::BELONGS_TO, 'AccountOrder', 'canceled_account_order_id'),
            'file' => array(self::BELONGS_TO, 'File', 'id'),
            'account_transactions' => [self::HAS_MANY, 'AccountTransaction','account_document_id'],
            'account_transactions_count' => [self::STAT, 'AccountTransaction','account_document_id', 'select'=>'count(*)'],
            'vat_month' => [self::HAS_ONE, 'AccountingMonth', 'vat_account_document_id'],
            'start_order_for_year' => [self::HAS_ONE, 'AccountingYear', 'start_booking_order_id'],
            'profit_calc_order_for_year' => [self::HAS_ONE, 'AccountingYear', 'profit_calc_booking_order_id'],
            'finish_order_for_year' => [self::HAS_ONE, 'AccountingYear', 'finish_booking_order_id'],
            
            'popdv_items' => [self::HAS_MANY, 'POPDVItem','account_document_id'],
        );
    }
    
    public function modelSettings($column) 
    {
        switch ($column)
        {
            //MIlosS: ovaj model ne moze da ima klasicne dugmice jer je vezivni model
            case 'options': return [];
            case 'filters' : return array(
                'id' => 'dropdown',
                'account_order' => 'relation',
                'canceled_account_order' => 'relation',
                'file' => 'relation',
                'popdv_manual' => 'boolean'
            );
            case 'textSearch' : return array(
                'file'=>'relation',
            );
            case 'update_relations' : return array(
                'file','account_order','file.warehouse_transfer'
//                ,'account_transactions'
            );
            case 'multiselect_options' : return ['delete'];
            default: return parent::modelSettings($column);
        }
    }
    
    public function afterSave()
    {
        //ako je izvod upisujemo nalog za knjizenje kao poslednji za taj izvod
        if (isset($this->file->bank_statement))
        {
            $this->file->bank_statement->last_account_order_id = $this->account_order_id;
            $this->file->bank_statement->save();
        }
        
        return parent::afterSave();
    }
    
    /**
     * Predvidja u kojoj godini treba proknjiziti dokument
     * @param integer $document_id
     * @return Year - predlozena godina
     */
    public static function predictBookingYearForDocument($document_id)
    {
        $file = File::model()->findByPkWithCheck($document_id);
        if (isset($file->bill))
        {
            $date = $file->bill->income_date;
        }
        else if (isset($file->bank_statement))
        {
            $date = $file->bank_statement->date;
        }
        else if (isset($file->regular_paycheck_period))
        {
            $date = $file->regular_paycheck_period->payment_date;
        }
        else if (isset($file->warehouse_transfer))
        {
            $date = $file->warehouse_transfer->transfer_time;
        }
        else
        {
            $date = date("Y-m-d");
        }
        $date_obj = new DateTime($date);
        $year = $date_obj->format("Y");
        
        return Year::get($year);
    }
}
