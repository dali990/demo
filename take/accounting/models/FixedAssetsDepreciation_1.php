<?php

class FixedAssetsDepreciation extends SIMAActiveRecord 
{

    public static function model($className = __CLASS__) 
    {
        return parent::model($className);
    }

    public function tableName() 
    {
        return 'accounting.fixed_assets_depreciations';
    }
    
    public function moduleName()
    {
        return 'accounting';
    }

    public function __get($column) 
    {
        switch ($column) 
        {
            case 'DisplayName': return $this->file->name;
            default: return parent::__get($column);
        }
    }
    
    public function scopes()
    {
        $alias = $this->getTableAlias();
        $years_table = Year::tableName();
        return [
            'byYearDesc' => [
                'order' => "(select year from $years_table where id = $alias.year_id) DESC"
            ]
        ];
    }

    public function relations($child_relations = []) 
    {
        return array(
            'fixed_assets_depreciation_items' => array(self::HAS_MANY, 'FixedAssetsDepreciationItem', 'fixed_assets_depreciation_id'),
            'file' => array(self::BELONGS_TO, 'File', 'id'),
            'year' => array(self::BELONGS_TO, 'Year', 'year_id')
        );
    }

    public function rules() 
    {
        return array(
            array('year_id', 'required'),
            array('year_id', 'unique', 'message' => Yii::t('BaseModule.Year', 'YearAlreadyUsed')),
            array('id, comment', 'safe'),
        );
    }

    public function modelSettings($column) 
    {
        switch ($column)
        {
            case 'default_order_scope': return 'byYearDesc';
            case 'filters' : return array_merge(parent::modelSettings('filters'),array(
                'year' => 'relation',
                'file' => 'relation',
                'comment' => 'text'
            ));
            default: return parent::modelSettings($column);
        }
    }
    
    public function beforeSave()
    {
        if (!isset($this->file) && empty($this->id))
        {
            $file = new File();
            $file->name = Yii::t('AccountingModule.FixedAssets','Depreciation').' - '.$this->year;
            $file->save();
            $file->refresh();
            $this->id = $file->id;
            $this->file = $file;
        }
        
        return parent::beforeSave();
    }
    
}