<?php

class PartnerFinance extends SIMAActiveRecord
{
    public $ft_name;
    public $ft_model_table;
    public $ft_model_id;
    public $invoice_amount;
    public $invoice_amount_1;
    public $invoice_amount_2;
    public $invoice_unreleased;
    public $invoice_released;
    public $bill_amount;
    public $bill_amount_1;
    public $bill_amount_2;
    public $bill_unreleased;
    public $bill_released;
    public $invoice_advance;
    public $invoice_advance_1;
    public $invoice_advance_2;
    public $bill_advance;
    public $bill_advance_1;
    public $bill_advance_2;

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
 
    public function tableName()
    {
        return 'private.bills';
    }
    
    public function moduleName()
    {
        return 'accounting';
    }
    
    public function defaultScope()
    {
        $alias = $this->getTableAlias(false, false);
        $_fa_table = FixedAsset::model()->tableName();
        $_bt_table = Theme::model()->tableName();
        return array(
            'condition' => $alias.'.canceled = false',
            'join'=>"left join 
                    (
                        select ftft.file_id as file_id, fff.model_table as model_table, fff.model_id as model_id, fff.name as name, count(*) over (PARTITION BY ftft.file_id) as ccount
                        from files.files_to_file_tags ftft left join files.file_tags fff on fff.id=ftft.file_tag_id
                        where model_table IN ('$_bt_table', '$_fa_table') ) ft on ft.file_id=t.id",
        );
    }
    
    public function scopes() 
    {
        $alias = $this->getTableAlias();
        $bill = Bill::$BILL;
        $adv_bill = Bill::$ADVANCE_BILL;
        return array(
            'byName' => array(
                'select' => "ft.name as ft_name, ft.model_id as ft_model_id, ft.model_table as ft_model_table, array_agg($alias.id) as id,
                            sum(CASE WHEN (invoice is true and bill_type='$bill') THEN
                                    case when ft.ccount is not null and ft.ccount !=0 then
                                        amount/ft.ccount 
                                    else
                                        amount
                                    end
                            ELSE 0 END) as invoice_amount,
                            sum(CASE WHEN (invoice is true and bill_type='$bill' and (ft.ccount is null or ft.ccount=0 or ft.ccount=1)) THEN
                                        amount
                            ELSE 0 END) as invoice_amount_1,
                            sum(CASE WHEN (invoice is true and bill_type='$bill' and ft.ccount is not null and ft.ccount>1) THEN
                                        amount/ft.ccount 
                            ELSE 0 END) as invoice_amount_2,
                            sum(CASE WHEN (invoice is true and bill_type='$bill') THEN 
                                    case when ft.ccount is not null and ft.ccount !=0 then
                                        unreleased_amount/ft.ccount 
                                    else
                                        unreleased_amount
                                    end
                            ELSE 0 END) as invoice_unreleased, 
                            sum(CASE WHEN (invoice is true and bill_type='$bill') THEN 
                                    case when ft.ccount is not null and ft.ccount !=0 then
                                        (amount-unreleased_amount)/ft.ccount 
                                    else
                                        (amount-unreleased_amount)
                                    end
                            ELSE 0 END) as invoice_released, 
                            sum(CASE WHEN (invoice is false and bill_type='$bill') THEN 
                                    case when ft.ccount is not null and ft.ccount !=0 then
                                         round(amount/ft.ccount, 2)  
                                    else
                                         round(amount, 2)  
                                    end
                            ELSE 0 END) as bill_amount,
                            sum(CASE WHEN (invoice is false and bill_type='$bill' and (ft.ccount is null or ft.ccount=0 or ft.ccount=1)) THEN
                                        round(amount, 2)  
                            ELSE 0 END) as bill_amount_1,
                            sum(CASE WHEN (invoice is false and bill_type='$bill' and ft.ccount is not null and ft.ccount>1) THEN
                                        round(amount/ft.ccount , 2)
                            ELSE 0 END) as bill_amount_2,
                            sum(CASE WHEN (invoice is false and bill_type='$bill') THEN 
                                    case when ft.ccount is not null and ft.ccount !=0 then
                                         round(unreleased_amount/ft.ccount, 2)
                                    else
                                         round(unreleased_amount, 2)
                                    end
                            ELSE 0 END) as bill_unreleased,
                            sum(CASE WHEN (invoice is false and bill_type='$bill') THEN 
                                    case when ft.ccount is not null and ft.ccount !=0 then
                                         round(((amount-unreleased_amount)/ft.ccount), 2)
                                    else
                                         round((amount-unreleased_amount), 2)
                                    end
                            ELSE 0 END) as bill_released,

                            sum(CASE WHEN (invoice is true and bill_type='$adv_bill') THEN 
                                    case when ft.ccount is not null and ft.ccount !=0 then
                                         round(((unreleased_amount)/ft.ccount), 2)
                                    else
                                         round((unreleased_amount), 2)
                                    end
                            ELSE 0 END) as invoice_advance,
                            sum(CASE WHEN (invoice is true and bill_type='$adv_bill' and (ft.ccount is null or ft.ccount=0 or ft.ccount=1)) THEN
                                        round((unreleased_amount), 2)
                            ELSE 0 END) as invoice_advance_1,
                            sum(CASE WHEN (invoice is true and bill_type='$adv_bill' and ft.ccount is not null and ft.ccount>1) THEN
                                        round(((unreleased_amount)/ft.ccount), 2)
                            ELSE 0 END) as invoice_advance_2,
                            sum(CASE WHEN (invoice is false and bill_type='$adv_bill') THEN 
                                    case when ft.ccount is not null and ft.ccount !=0 then
                                         round(((unreleased_amount)/ft.ccount), 2)
                                    else
                                         round((unreleased_amount), 2)
                                    end
                            ELSE 0 END) as bill_advance,
                            sum(CASE WHEN (invoice is false and bill_type='$adv_bill' and (ft.ccount is null or ft.ccount=0 or ft.ccount=1)) THEN
                                        round((unreleased_amount), 2)
                            ELSE 0 END) as bill_advance_1,
                            sum(CASE WHEN (invoice is false and bill_type='$adv_bill' and ft.ccount is not null and ft.ccount>1) THEN
                                        round(((unreleased_amount)/ft.ccount), 2)
                            ELSE 0 END) as bill_advance_2",
                'group'=>"ft.model_table, ft.model_id, ft_name",
                'order' => 'ft_name'
            ),
        );
    }
    
    public function partner($partner_id)
    {
        $alias = $this->getTableAlias();
        $this->getDbCriteria()->mergeWith(array(
            'condition'=>"$alias.partner_id=:partner_id",
            'params'=> array(':partner_id' => $partner_id),
        ));
        return $this;
    }
    
    public function relations($child_relations = [])
    {
    	return array(
            'file'=>array(self::BELONGS_TO, 'File', 'id'),
            'filing'=>array(self::BELONGS_TO, 'Filing', 'id'),
    	);
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'options' : return array();
            case 'number_fields' : return array(
                'invoice_amount',
                'invoice_amount_1',
                'invoice_amount_2',
                'invoice_unreleased',
                'invoice_released',
                'bill_amount',
                'bill_amount_1',
                'bill_amount_2',
                'bill_unreleased',
                'bill_released',
                'invoice_advance',
                'invoice_advance_1',
                'invoice_advance_2',
                'bill_advance',
                'bill_advance_1',
                'bill_advance_2',
                );
            default : return parent::modelSettings($column);
        }
    }    
}