<?php

class KPRManual extends SIMAActiveRecord
{
    public $column_8_compare;
    public $column_9_compare;
    public $column_9a_compare;
    public $column_10_compare;
    public $column_11_compare;
    public $column_12_compare;
    public $column_13_compare;
    public $column_14_compare;
    public $column_15_compare;
    public $column_16_compare;
    public $column_17_compare;
    public $column_18_compare;
    public $column_19_compare;
    
    public static $REGULAR = 'REGULAR';
    public static $CANCELED = 'CANCELED';
    
    public static $reasons = [
        'REGULAR' => 'Regularan',
        'CANCELED' => 'Storniran',
    ];
    
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'accounting.kpr_manual';
    }

    public function moduleName()
    {
        return 'accounting';
    }

    public function __get($column)
    {
        switch ($column)
        {
            case 'DisplayName': return $this->bill->DisplayName;
            default: return parent::__get($column);
        }
    }
    
    public function rules() 
    {
        return [
            ['bill_id, reason', 'required'],
            ['month_id', 'required'],
            ['column_8, column_9, column_9a, column_10, column_11, column_12, column_13', 'required'],
            ['column_14, column_15, column_16, column_17, column_18, column_19', 'required'],
            ['bill_id','unique_with','with' => ['reason'], 'message' => Yii::t('AccountingModule.KPRManual','UniqWithReason')],
            ['order_in_year', 'checkOrder', 'on'=>['insert','update']],
        ];
    }
    
    public function beforeValidate()
    {
        if (empty($this->order_in_year) && !empty($this->month))
        {
            $this->order_in_year = $this->month->year->accounting->getNextKPRCount();
        }

        return parent::beforeValidate();
    }
    
    public function checkOrder()
    {
        if (!$this->hasErrors())
        {
            $_duplicate = KPRManual::model()->findByAttributes(['order_in_year' => $this->order_in_year]);
            if (isset($_duplicate) && $_duplicate->id != $this->id)
            {
                if ($this->month->year_id == $_duplicate->month->year_id)
                {
                    $this->addError('order_in_year', 'Redni broj vec postoji');
                }
            }
        }
    }
    
    public function relations($child_relations = [])
    {
        return [
            'bill' => [self::BELONGS_TO,'AnyBill','bill_id'],
            'month' => [self::BELONGS_TO,'Month','month_id'],
        ];
    }
    
    private $_kpr = -1;
    public function getkpr()
    {
        if ($this->_kpr === -1)
        {
            $this->_kpr = KPR::model()->findByAttributes([
                'bill_id' => $this->bill_id,
                'month_id' => $this->month_id,
                'reason' => $this->reason
            ]);
        }
        return $this->_kpr;
    }
    
    public function refresh()
    {
        $this->_kpr = -1;
        return parent::refresh();
    }
    
    public function getClasses()
    {
        return parent::getClasses().$this->bill->getVATClasses();
    }

    public function scopes()
    {
        $alias = $this->getTableAlias();
        return [
            'diffKPRAndInKPRMonth' => [//MILOSS:modrazumeva da mora biti u istom
                'condition' => 
                    "exists "
                    . "("
                        . "select 1 from accounting.kpr acckpr where bill_id = $alias.bill_id and reason = $alias.reason and month_id = $alias.month_id"
                        . " AND ("
                                ."acckpr.kpr_8 != $alias.column_8 OR  "
                                ."acckpr.kpr_9 != $alias.column_9 OR  "
                                ."acckpr.kpr_9a != $alias.column_9a OR  "
                                ."acckpr.kpr_10 != $alias.column_10 OR  "
                                ."acckpr.kpr_11 != $alias.column_11 OR  "
                                ."acckpr.kpr_12 != $alias.column_12 OR  "
                                ."acckpr.kpr_13 != $alias.column_13 OR  "
                                ."acckpr.kpr_14 != $alias.column_14 OR  "
                                ."acckpr.kpr_14 != acckpr.kpr_14b OR  "
                                ."acckpr.kpr_14b != $alias.column_14 OR  "
            //                    ."acckpr.kpr_15 != $alias.column_15 OR  "
                                ."acckpr.kpr_16 != $alias.column_16 OR  "
                                ."acckpr.kpr_17 != $alias.column_17 OR  "
                                ."acckpr.kpr_18 != $alias.column_18 OR  "
                                ."acckpr.kpr_19 != $alias.column_19 "
                        . ")"
                    . ")"
            ],
            'byOrder' => [
                'order' => $alias.'.order_in_year DESC'
            ],
            'byOrderInYearAsc' => [
                'order' => $alias.'.order_in_year ASC'
            ]
        ];
    }
    
    public function inKPRInMonth($month_id)
    {
        $month = Month::model()->findByPk($month_id);
        $month_name = $month->month;
        $year_name = $month->year->year;
        $alias = $this->getTableAlias();
        
        $criteria = new SIMADbCriteria();
        $criteria->condition = "exists "
                . "("
                . "select 1 from accounting.kpr where bill_id = $alias.bill_id and reason = $alias.reason and month_id = $alias.month_id"
                . ")";
        
        $this->getDbCriteria()->mergeWith($criteria);
        return $this;
    }
    
    public function notInKPRInMonth($month_id)
    {
        $month = Month::model()->findByPk($month_id);
        $month_name = $month->month;
        $year_name = $month->year->year;
        $alias = $this->getTableAlias();
        
        $criteria = new SIMADbCriteria();
        $criteria->condition = "not exists "
                . "("
                . "select 1 from accounting.kpr where bill_id = $alias.bill_id and reason = $alias.reason and month_id = $alias.month_id"
                . ")";
        
        $this->getDbCriteria()->mergeWith($criteria);
        return $this;
    }
    
    public function inYear($year_id)
    {
        $uniq = SIMAHtml::uniqid();
        $alias = $this->getTableAlias();
        $month_table = AccountingMonth::model()->tableName();
        $year = AccountingYear::model()->findByPk($year_id);
        
        $criteria = new SIMADbCriteria();
        $criteria->condition = "m$uniq.year = :param1$uniq";
        $criteria->join = "left join $month_table m$uniq on m$uniq.id = $alias.month_id";
        $criteria->params = [
            ":param1$uniq" => $year->year
        ];
        
        $this->getDbCriteria()->mergeWith($criteria);
        
        return $this;
    }
    
    public function orderInYearEqualGreaterThen($value)
    {
        $uniq = SIMAHtml::uniqid();
        $alias = $this->getTableAlias();
        
        $criteria = new SIMADbCriteria();
        $criteria->condition = "$alias.order_in_year >= :param1$uniq";
        $criteria->params = [
            ":param1$uniq" => $value
        ];
        
        $this->getDbCriteria()->mergeWith($criteria);
        
        return $this;
    }

    public function modelSettings($column) 
    {
        switch ($column)
        {
            case 'number_fields': return [
                'column_8',
                'column_9',
                'column_9a',
                'column_10',
                'column_11',
                'column_12',
                'column_13',
                'column_14',
                'column_15',
                'column_16',
                'column_17',
                'column_18',
                'column_19',
                'order_in_year',
                'column_8_compare',
                'column_9_compare',
                'column_9a_compare',
                'column_10_compare',
                'column_11_compare',
                'column_12_compare',
                'column_13_compare',
                'column_14_compare',
                'column_15_compare',
                'column_16_compare',
                'column_17_compare',
                'column_18_compare',
                'column_19_compare',
            ];
            case 'filters': return [
                'order_in_year' => 'integer',
                'month' => 'relation',
                'bill' => 'relation',
                'reason' => 'dropdown',
//                'kpr' => 'relation',
            ];
            default: return parent::modelSettings($column);
        }
        
    }

}
