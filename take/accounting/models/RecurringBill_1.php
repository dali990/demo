<?php

class RecurringBill extends SIMAActiveRecord
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'accounting.recurring_bills';
    }

    public function moduleName()
    {
        return 'accounting';
    }

    public function __get($column) 
    {
        switch ($column)
        {
           case 'DisplayName': return $this->name;
           case 'SearchName':return $this->DisplayName;
           default: return parent::__get($column);
        }
    }

    public function relations($child_relations = [])
    {
        return parent::relations(array_merge([
            'bills' => array(self::HAS_MANY, 'Bill', 'recurring_bill_id'),
            'partner' => array(self::BELONGS_TO, 'Partner', 'partner_id'),
            'comment_thread'=>array(self::BELONGS_TO,'CommentThread', 'comment_thread_id'),
        ],$child_relations));
    }

    public function rules()
    {
        return array(
            array('name, call_for_number, partner_id', 'required'),
            array('description, comment_thread_id', 'safe'),
            ['call_for_number', 'checkUniq']
        );
    }
    
    public function checkUniq()
    {
        if (
                !$this->hasErrors() && 
                (
                    intval($this->__old['partner_id']) !== intval($this->partner_id) || 
                    $this->__old['call_for_number'] !== $this->call_for_number
                )
           )
        {
            $recurring_bill = RecurringBill::model()->findByAttributes([
                'partner_id' => $this->partner_id,
                'call_for_number' => $this->call_for_number
            ]);
            if (!is_null($recurring_bill))
            {
                $this->addError('call_for_number', Yii::t('AccountingModule.RecurringBill', 'UniqPartnerAndCallForNumber'));
            }
        }
    }
    
    /**
     * 
     * @param Day $day na dan - default trenutno
     */
    public function calcDebt(Day $day = null, $dont_include_ids = [-1])
    {
        if (is_null($day))
        {
            $day = Day::get();
        }
        $condition_bill = new SIMADbCriteria([
            'Model' => Bill::model(),
            'model_filter' => [
                'AND',
                [
                    'NOT',
                    ['ids'=>$dont_include_ids]
                ],
                [
                    'income_date' => ['<=',$day->day_date],
                    'recurring_bill' => ['ids' => $this->id]
                ]
                
            ],
            'select' => 'sum(amount) as amount'
        ]);
        $bills_sum = Bill::model()->find($condition_bill);
        
        $condition_payment = new SIMADbCriteria([
            'Model' => Payment::model(),
            'model_filter' => [
                'bank_statement' => [
                    'date' => ['<=',$day->day_date]
                ],
                'recurring_bill' => ['ids' => $this->id]
            ],
            'select' => 'sum(amount) as amount'
        ]);
        $payments_sum = Payment::model()->find($condition_payment);
        return $bills_sum->amount - $payments_sum->amount;
    }
    
    /**
     * Funkcija koja vraca poslednju uplatu
     * @param Day $day - do kog dana, podrazumeva se danas
     * @return Payment uplata koja je bila poslednja za ovaj RecurringBill
     */
    public function getLastPayment(Day $day = null)
    {
        if (is_null($day))
        {
            $day = Day::get();
        }
        return Payment::model()->findByModelFilter([
            'payment_date' => ['<=',$day->day_date],
            'recurring_bill' => ['ids' => $this->id],
            'display_scopes' => ['byOfficialDESC']
        ]);
    }

    public function modelSettings($column)
    {
        switch ($column) {
            case 'filters' : return [
                    'name' => 'text',
                    'description' => 'text',
                    'call_for_number' => ['func'=>'filterCallForNumber'],
                    'partner' => 'relation',
                    'abclient' => 'relation' //Sasa A. - bice ignorisano ako nije ukljucen modul autoBilling. Mora ovako jer behavior ne podrzava funkciju modelSettings
                ];
            case 'textSearch' : return [
                    'name' => 'text',
                    'description' => 'text',
                    'call_for_number' => 'text',
                ];
            case 'mutual_forms': return [
                'relations'=>[
                    'abclient'
                ]
            ];
            default: return parent::modelSettings($column);
        }
    }
    
    public function filterCallForNumber($condition, $alias, $column)
    {
        if (!empty($this->call_for_number)) 
        {
            $param_id = SIMAHtml::uniqid();
            $temp_cond = new SIMADbCriteria();
            $temp_cond->condition = "replace($alias.$column,'-','') = replace(:call_for_number$param_id,'-','')";
            $temp_cond->params = [":call_for_number$param_id" => $this->$column];
            $condition->mergeWith($temp_cond);
        }
    }
    
    public function afterValidateDependencyError(array $foreign_dependencies)
    {
        $allowed_tables = [];
        
        if (Yii::app()->hasModule('autoBilling'))
        {
            array_push($allowed_tables, ABClient::model()->tableName().':id');
        }
        
        $parsed_foreign_dependencies = array_diff($foreign_dependencies, $allowed_tables);
        
        parent::afterValidateDependencyError($parsed_foreign_dependencies);
    }
    
    /**
     * 
     * @param Bill $bill
     * @return Bill
     */
    public function previousBill(Bill $bill)
    {
        return Bill::model()->findByModelFilter([
            'recurring_bill' => ['ids' => $this->id],
            'display_scopes' => ['recently'],
            'income_date' => ['<',$bill->income_date],
        ]); 
    }
    /**
     * 
     * @param Bill $bill
     * @return Bill
     */
    public function nextBill(Bill $bill)
    {
        return Bill::model()->findByModelFilter([
            'recurring_bill' => ['ids' => $this->id],
            'display_scopes' => ['byOfficial'],
            'income_date' => ['>',$bill->income_date],
        ]); 
    }
}
