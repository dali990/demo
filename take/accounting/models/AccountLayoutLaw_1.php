<?php

class AccountLayoutLaw extends SIMAActiveRecord
{
    public $file_version_id;
    
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'accounting.account_layouts_laws';
    }

    public function moduleName()
    {
        return 'accounting';
    }

    public function __get($column)
    {
        switch ($column)
        {
            case 'DisplayName': return $this->name . '(' . $this->date . ')';
            case 'SearchName': return $this->name . '(' . $this->date . ')';
            default: return parent::__get($column);
        }
    }

    public function relations($child_relations = [])
    {
        return array(
            'file' => array(self::BELONGS_TO, 'File', 'id'),
            'account_layouts' => array(self::HAS_MANY, 'AccountLayout', 'law_id'),
            'account_layouts_without_parent'=>array(self::HAS_MANY, 'AccountLayout', 'law_id', 'condition'=>'parent_id is null'),
            'accounting_years' => [self::HAS_MANY, 'AccountingYear', 'account_layout_law_id'],
            'accounting_years_count' => [self::STAT, 'AccountingYear', 'account_layout_law_id', 'select' => 'count(*)'],
        );
    }

    public function rules()
    {
        return array(
            array('name, date', 'required'),
            array('description, file_version_id', 'safe'),
        );
    }
    
    public function scopes()
    {
        return array(
            'byName' => array(
                'order' => 'date desc'
            )
        );
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters': return array(
                'name' => 'text',
                'date' => 'date_range',
                'description' => 'text',
            );
            case 'textSearch': return array(
                'name' => 'text',
            );
            case 'options': 
                $options = ['form', 'download'];
                if($this->accounting_years_count == 0)
                {
                    $options[] = 'delete';
                }
                return $options;
                
            default : return parent::modelSettings($column);
        }
    }    
    
    public function beforeSave()
    {           
        if (!isset($this->file))
        {            
            $file = new File();
            $file->name = 'Zakonski okvir - '.$this->name;            
            $file->save();
            $file->refresh();
            $this->id = $file->id;
            $this->file = $file;
        }
        
        /** ubacivanje upload-a fajla */
        $this->file->appendUpload($this->file_version_id);
        
        return parent::beforeSave();
    }
    
    public function beforeDelete()
    {           
        $deleted = AccountLayout::model()->deleteAllByAttributes([
            'law_id' => $this->id
        ]);
        return $deleted && parent::beforeSave() ;
    }
    
}