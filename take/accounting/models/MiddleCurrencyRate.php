<?php

class MiddleCurrencyRate extends SIMAActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'accounting.middle_currency_rates';
    }

    public function moduleName()
    {
        return 'accounting';
    }
    
    public function relations($child_relations = [])
    {
        return array(
            'currency' => array(self::BELONGS_TO, 'Currency', 'currency_id')
        );
    }
    
    public function rules()
    {
        return array(
            array('currency_id, date', 'required'),
            array('currency_id', 'currencyDateCheck'),
//            array('value', 'valueCheck'),
            array('value', 'numerical','min' => 0),
        );
    }
    
    public function currencyDateCheck()
    {
        $middleCurrencyRateCriteria = new SIMADbCriteria([
            'Model' => 'MiddleCurrencyRate',
            'model_filter' => [
                'currency' => [
                    'ids' => $this->currency_id
                ],
                'date' => $this->date
            ]
        ]);
        
        $middleCurrencyRates = MiddleCurrencyRate::model()->findAll($middleCurrencyRateCriteria);
        
        $haveTakenError = false;
        
        if ($this->getIsNewRecord())
        {
            if(count($middleCurrencyRates) > 0)
            {
                $haveTakenError = true;
            }
        }
        else
        {
            foreach($middleCurrencyRates as $mcr)
            {
                if($mcr->id !== $this->id)
                {
                    $haveTakenError = true;
                    break;
                }
            }
        }
        
        if($haveTakenError === true)
        {
            $this->addError('currency_id', Yii::t('AccountingModule.MiddleCurrencyRate','MiddleCurrencyDateTaken'));
            $this->addError('date', Yii::t('AccountingModule.MiddleCurrencyRate','MiddleCurrencyDateTaken'));
        }
    }
    
//    public function valueCheck()
//    {
////        if(empty($this->value) && Yii::app()->params['nbs_connection_installed'] === false)
//        if(empty($this->value))
//        {
//            $this->addError('value', Yii::t('AccountingModule.MiddleCurrencyRate','ValueEmptyNBSConnectionNotInstalled'));
//        }
//    }
    
    public function beforeSave()
    {
//        if(empty($this->value) && Yii::app()->params['nbs_connection_installed'] === true)
        if(empty($this->value))
        {
            $nbs_value = Yii::app()->NBSConnection->GetCurrentCurrencyMiddleExchangeRateOnDate($this->currency->code, $this->date);
                        
            if(empty($nbs_value))
            {
                throw new SIMAWarnException(Yii::t('AccountingModule.MiddleCurrencyRate', 'EmptyValueFromNBSConnection'));
            }
            
            $this->value = $nbs_value;
        }
        
        return parent::beforeSave();
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters': return [
                'currency' => 'relation',
                'currency_id' => 'dropdown',
                'date' => 'date_range',
                'value' => 'numeric'
            ];
            default: return parent::modelSettings($column);
        }
    }
    
    public static function get(Currency $cur, Day $day = null)
    {
        if (is_null($day))
        {
            $day = Day::get();
        }
        
        $middleCurrencyRateCriteria = new SIMADbCriteria([
            'Model' => 'MiddleCurrencyRate',
            'model_filter' => [
                'currency' => [
                    'ids' => $cur->id
                ],
                'date' => $day->day_date
            ]
        ]);
        
        $middleCurrencyRate = MiddleCurrencyRate::model()->find($middleCurrencyRateCriteria);
        
        if(empty($middleCurrencyRate) && !$cur->isCountryCurrency())
        {
            if (Yii::app()->NBSConnection->isConnected())
            {
                $nbs_value = Yii::app()->NBSConnection->GetCurrentCurrencyMiddleExchangeRateOnDate($cur->code, $day->day_date);
                $middleCurrencyRate = new MiddleCurrencyRate();
                $middleCurrencyRate->currency_id = $cur->id;
                $middleCurrencyRate->date = $day->day_date;
                $middleCurrencyRate->value = $nbs_value;
                $middleCurrencyRate->save();
            }
            else
            {
                throw new SIMAWarnNoCurrencyRate($cur,$day);
            }
        }
        
        return $middleCurrencyRate;
    }
}

