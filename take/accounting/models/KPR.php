<?php

class KPR extends SIMAActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'accounting.kpr';
    }

    public function moduleName()
    {
        return 'accounting';
    }
    
    public function getTableSchema()
    {
        $schema = new CDbTableSchema();
        $schema->primaryKey = 'id';
        $schema->name = 'accounting.kpr';
        $schema->rawName = 'accounting.kpr';
        $schema->sequenceName = NULL;
        
        $bill_id = new CDbColumnSchema();
        $bill_id->name = 'bill_id';
        $bill_id->rawName = 'bill_id';
        $bill_id->allowNull = false;
        $bill_id->dbType = 'integer';
        $bill_id->type = 'integer';
        
        $month_id = new CDbColumnSchema();
        $month_id->name = 'month_id';
        $month_id->rawName = 'month_id';
        $month_id->allowNull = false;
        $month_id->dbType = 'integer';
        $month_id->type = 'integer';
        
        $reason = new CDbColumnSchema();
        $reason->name = 'reason';
        $reason->rawName = 'reason';
        $reason->allowNull = false;
        $reason->dbType = 'TEXT';
        $reason->type = 'string';
        
        $schema->columns = [
            'id',
            'bill_id' => $bill_id,
            'month_id' => $month_id,
            'reason' => $reason,
            'order_in_year',
            'kpr_8',
            'kpr_9',
            'kpr_9a',
            'kpr_10',
            'kpr_11',
            'kpr_12',
            'kpr_13',
            'kpr_14',
            'kpr_14b',
            'kpr_15',
            'kpr_16',
            'kpr_17',
            'kpr_18',
            'kpr_19'
        ];
        
        return $schema;
    }

    public function __get($column)
    {
        switch ($column)
        {
            case 'KPR_8': return $this->kpr_8;
            case 'KPR_9': return $this->kpr_9;
            case 'KPR_9a': return $this->kpr_9a;
            case 'KPR_10': return $this->kpr_10;
            case 'KPR_11': return $this->kpr_11;
            case 'KPR_12': return $this->kpr_12;
            case 'KPR_13': return $this->kpr_13;
            case 'KPR_14': return $this->kpr_14;
            case 'KPR_14b': return $this->kpr_14b;
            case 'KPR_15': return $this->kpr_13 - $this->kpr_14;
            case 'KPR_16': return $this->kpr_16;
            case 'KPR_17': return $this->kpr_17;
            case 'KPR_18': return $this->kpr_18;
            case 'KPR_19': return $this->kpr_19;
            default: return parent::__get($column);
        }
    }

    public function inMonth($month,$year)
    {
        $alias = $this->getTableAlias();
        $uniq = SIMAHtml::uniqid();
        $criteria = new SIMADbCriteria();
        $criteria->condition = "$alias.year = :year$uniq and $alias.month = :month$uniq";
        $criteria->params = [
            ":year$uniq" => $year,
            ":month$uniq" => $month
        ];
        $this->getDbCriteria()->mergeWith($criteria);
        return $this;
    }
    
    public function scopes()
    {
        $alias = $this->getTableAlias();
        return array(
            'byOrderInYear' => array(
                'order' => "$alias.order_in_year",
            ),
            'BadVATBooked' => [
                'condition' =>  "$alias.kpr_14 != $alias.kpr_14b"
            ]
        );
    }
    
    public function notInKPRManualInMonth($month_id)
    {
        $alias = $this->getTableAlias();
        $kpr_manual_table = KPRManual::model()->tableName();
        
        $criteria = new SIMADbCriteria();
        $criteria->condition = 
                "not exists (select 1 from $kpr_manual_table where bill_id = $alias.bill_id "
                . "and reason = $alias.reason "
                . "and month_id = $month_id)";

        $this->getDbCriteria()->mergeWith($criteria);
        return $this;
    }
    
    public function relations($child_relations = [])
    {
        return [
            'bill' => [self::BELONGS_TO,'AnyBill','bill_id']
        ];
    }

    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'options' : return [];
            case 'number_fields': return [
                'KPR_8',
                'KPR_9',
                'KPR_9a',
                'KPR_10',
                'KPR_11',
                'KPR_12',
                'KPR_13',
                'KPR_14',
                'KPR_14b',
                'KPR_15',
                'KPR_17',
                'KPR_16',
                'KPR_17',
                'KPR_18',
                'KPR_19',
            ];
            default: return parent::modelSettings($column);
        }
    }
    
    public function getClasses()
    {
        return parent::getClasses().$this->bill->getVATClasses();
    }    
}
