<?php

class CostType extends SIMAActiveRecord
{
    public static $TYPE_COST = 'COST';
    public static $TYPE_INCOME = 'INCOME';
    
    public static $cost_or_income = [
        'COST' => 'Trošak',
        'INCOME' => 'Prihod'
    ];
    
    public $account_id = null;//popunjava se u scope-u inYear
    public $year_id = null;
    
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'accounting.cost_types';
    }

    public function moduleName()
    {
        return 'accounting';
    }

    public function __get($column)
    {
        switch ($column)
        {
            case 'DisplayName': 
            case 'SearchName': 
                return $this->name;
            default: return parent::__get($column);
        }
    }


    public function relations($child_relations = [])
    {
        $cost_types_to_accounts_table = CostTypeToAccount::model()->tableName();
        return array(
            'account' => array(self::BELONGS_TO, 'Account', 'account_id'),
            'cost_types_to_accounts' => array(self::HAS_MANY, 'CostTypeToAccount', 'cost_type_id'),
            'accounts' => array(self::MANY_MANY, 'Account', $cost_types_to_accounts_table . '(cost_type_id, account_id)'),    
            'year' => [self::BELONGS_TO, 'Year', 'year_id'],
        );
    }

    public function accountForYear(Year $year)
    {
        $cost_type_to_account = CostTypeToAccount::model()->find(new SIMADbCriteria([
            'Model'=>'CostTypeToAccount',
            'model_filter'=>[
                'cost_type'=>[
                    'ids'=>$this->id
                ],
                'account'=>[
                    'year'=>[
                        'ids'=>$year->id
                    ]
                ]
            ]
        ]));
        
        return !is_null($cost_type_to_account) ? $cost_type_to_account->account : null;
    }
    
    public function rules()
    {
        return array(
            array('name', 'required'),
            array('name', 'unique_with', 'with' => 'cost_or_income'),
            array('description, account_id, cost_or_income, book_by_cost_location, year_id, account_id', 'safe'),
        );
    }
    
    public function scopes()
    {
        $alias = $this->getTableAlias();
        return array(
            'byName' => array(
                'order' => $alias.'.name'
            )
        );
    }
    
    public function inYear($year_id)
    {
        $uniq = SIMAHtml::uniqid();
        $alias = $this->getTableAlias();
        $cost_types_to_accounts = CostTypeToAccount::model()->tableName();
        $accounts_table = Account::model()->tableName();
        
        $this->getDbCriteria()->mergeWith(array(
            'select' => "$alias.*, ctta$uniq.account_id as account_id",
            'condition'=> "ac$uniq.year_id = :param1$uniq",
            'join' => "left join $cost_types_to_accounts ctta$uniq on ctta$uniq.cost_type_id = $alias.id"
                . " left join $accounts_table ac$uniq on ac$uniq.id=ctta$uniq.account_id",
            'params' => [
                ":param1$uniq" => $year_id,
            ]
        ));
        
        return $this;
    }
    
    public function notInYear($year_id)
    {
        $uniq = SIMAHtml::uniqid();
        $alias = $this->getTableAlias();
        $cost_types_to_accounts = CostTypeToAccount::model()->tableName();
        $accounts_table = Account::model()->tableName();
        
        $this->getDbCriteria()->mergeWith(array(
            'condition'=> "$alias.id not in "
                . "(select ctta$uniq.cost_type_id from $cost_types_to_accounts ctta$uniq "
                . "join $accounts_table ac$uniq on ac$uniq.id=ctta$uniq.account_id "
                . "where ac$uniq.year_id=:param1$uniq)",
            'params' => [
                ":param1$uniq" => $year_id,
            ]
        ));
        
        
        return $this;
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters': return array(
                'name' => 'text',
                'description' => 'text',
                'cost_or_income' => 'dropdown',
                'cost_types_to_accounts' => 'relation'
            );
            case 'textSearch': return array(
                'name' => 'text',
                'description' => 'text',
            );  
            case 'default_order_scope': return 'byName';
//            case 'options': return [
//                'form', 'download'
//            ];
            default : return parent::modelSettings($column);
        }
    }
    
    public function afterSave()
    {
        parent::afterSave();
        
        if (!empty($this->year_id) && !(empty($this->account_id)))
        {
            $_conn = CostTypeToAccount::model()->findByModelFilter([
                'cost_type' => ['ids' => $this->id],
                'account' => [
                    'year' => ['ids' => $this->year_id]
                ]
            ]);
            
            if (is_null($_conn))
            {
                $_conn = new CostTypeToAccount();
                $_conn->cost_type_id = $this->id;
                $_conn->year_id = $this->year_id;
            }
            $_conn->account_id = $this->account_id;
            $_conn->save();
        }
        
    }
}