<?php

class BillIn extends Bill
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    
    public function init()
    {
        parent::init();
        $this->bill_type = self::$BILL;
        $this->invoice = false;
    }

    public function defaultScope()
    {
        $alias = $this->getTableAlias(FALSE, FALSE);
        $bill_type = Bill::$BILL;
        return array(
            'condition' => "$alias.invoice=false and $alias.bill_type = '$bill_type' and $alias.canceled=false",
        );
    }
    
    public function relations($child_relations = [])
    {
        return parent::relations(array_merge([
            'cost_type' => array(self::BELONGS_TO, 'CostType', 'cost_type_id', 'condition' => "cost_or_income = 'COST'"),
        ],$child_relations));
    }
    
    public function beforeSave()
    {
        if (empty($this->payment_date))
        {
            $this->payment_date = $this->income_date;
        }
        
        return parent::beforeSave();
    }
}