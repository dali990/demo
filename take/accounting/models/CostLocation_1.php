<?php

class CostLocation extends SIMAActiveRecord
{
    public $display_name;
    public $display_name_path;
    public $display_name_full;
    public $parent_ids;
    public $children_ids;
    
    public $income;
    public $income_notpayed;
    public $income_advance;
    public $cost;
    public $cost_notpayed;
    public $cost_advance;
    
    public $income_services;
    public $income_notpayed_services;
    public $income_advance_services;
    public $cost_services;
    public $cost_notpayed_services;
    public $cost_advance_services;
    
    public $income_warehouse;
    public $income_notpayed_warehouse;
    public $income_advance_warehouse;
    public $cost_warehouse;
    public $cost_notpayed_warehouse;
    public $cost_advance_warehouse;
    
    public $income_salary;
    public $income_notpayed_salary;
    public $income_advance_salary;
    public $cost_salary;
    public $cost_notpayed_salary;
    public $cost_advance_salary;
    
    public $income_cash_desk;
    public $income_notpayed_cash_desk;
    public $income_advance_cash_desk;
    public $cost_cash_desk;
    public $cost_notpayed_cash_desk;
    public $cost_advance_cash_desk;
    
    public $income_r;
    public $income_notpayed_r;
    public $income_advance_r;
    public $cost_r;
    public $cost_notpayed_r;
    public $cost_advance_r;
    
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'accounting.cost_locations';
    }

    public function moduleName()
    {
        return 'accounting';
    }

    public function __get($column)
    {
        switch ($column)
        {
            //voditi racuna da se ne prikazuje u nalozima za knjizenje
            case 'DisplayName': 
                $_anl = $this->findForAnalytics(false);
                if (is_null($_anl) || $_anl->id === $this->id)
                {
                    return $this->name;
                }
                else
                {
                    return $_anl->name.' - '.$this->name;
                }
                
                return $this->name;
//            case 'DisplayName': return $this->name . ' - '.$this->work_name;
            case 'SearchName': return $this->name . ' (' . $this->work_name . ')';
            case 'DisplayNameFull': 
                $_this_cost_location = CostLocation::model()->withAllNames()->findByPk($this->id);
                return $this->name.' - '.$_this_cost_location->display_name_full;
            default: return parent::__get($column);
        }
    }
    
    public function __isset($column)
    {
        switch ($column)
        {
            case 'model': return is_null($this->model);
            default: return parent::__isset($column);
        }
    }
    
    public function getmodel()
    {
        $model_name = $this->model_name;
        if (empty($model_name))
        {
            return null;
        }
        return $model_name::model()->findByPk($this->model_id);
    }

    public function getpartners()
    {
        return Partner::model()->withIncomeCostForCostLocation($this->id)->byName()->findAll();
    }
    
    public function rules()
    {
        return array(
            array((CostLocationGUI::generateName()?'prefix':'name'), 'required'),
            array('prefix, name', 'safe'),
            array('work_name', 'required'),
            array('year_id, number, location_id, comment, name, for_analytics', 'safe'),
            ['number', 'checkNumber', 'on' => ['insert', 'update']],
            array('location_id, year_id', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => array('insert', 'update')),
            ['id', 'canDelete', 'on' => ['delete']]
        );
    }
    
    public function checkNumber($attribute, $params)
    {
        //ako je doslo do promene
        if (!empty($this->number) && $this->__old['number'] !== intval($this->number))
        {            
            if (!empty($this->year_id))
            {
                $number = CostLocation::model()->findByAttributes([
                    'year_id'=>$this->year_id,
                    'prefix'=>$this->prefix,
                    'number'=>$this->number
                ]);
                if ($number !== null)
                {
                    $this->addError('number', "Već postoji broj $this->number za godinu ".$this->year->year);
                }
            }
            else
            {
                $criteria = new SIMADbCriteria([
                    'Model'=>'CostLocation',
                    'model_filter'=>[
                        'year'=>[
                            'ids'=>'null'
                        ],
                        'prefix'=>$this->prefix,
                        'number'=>$this->number
                    ]
                ]);                
                $number = CostLocation::model()->find($criteria);
                if ($number !== null)
                {
                    $this->addError('number', "Već postoji broj $this->number");
                }
            }            
        }
    }
    
    public function canDelete()
    {
        $_warehouse = $this->owner->warehouse_production_location;
        if (!is_null($_warehouse))
        {
            $_warehouse->setScenario('delete');
            try
            {
                if ($_warehouse->validate() !== true)
                {
                    $this->addErrors($_warehouse->getErrors());
                }
            }
            catch (SIMAWarnException $e)
            {
                $this->addError('id', $e->getMessage());
            }
        }
    }
    
    public function relations($child_relations = [])
    {
        $_connection_table = CostLocationVirtualParent::model()->tableName();
        return parent::relations(array_merge([
            'location' => array(self::BELONGS_TO, 'City', 'location_id'),
            'year' => array(self::BELONGS_TO, 'Year', 'year_id'),
            'parent' => array(self::BELONGS_TO, 'CostLocation', 'parent_id'),
            'children' => array(self::HAS_MANY, 'CostLocation', 'parent_id'),
            'virtual_parents_base_relation' => [self::MANY_MANY, 'CostLocation', $_connection_table.'(virtual_child_id,virtual_parent_id)'],
            'virtual_parents' => [self::MANY_MANY, 'CostLocation', $_connection_table.'(virtual_child_id,virtual_parent_id)',
                'scopes' => ['onlyVirtual']
            ],
            'virtual_children' => [self::MANY_MANY, 'CostLocation', $_connection_table.'(virtual_parent_id,virtual_child_id)'],
            'bills' => array(self::HAS_MANY, 'Bill', 'cost_location_id'),
            'theme' => [self::HAS_ONE, 'Theme', 'cost_location_id']
        ], $child_relations));
    }

    public function scopes()
    {
        $alias = $this->getTableAlias();
        $uniq = SIMAHtml::uniqid();
        return array(
            'byName' => array(
                'order' => "$alias.display_name ASC",
            ),
            'topParents' => [
                'condition' => $alias.'.parent_id IS NULL'
            ],
            'withAllNames' => array(
                'select' => "  rfat$uniq.display_name_full as display_name_full,"
                            . "rfat$uniq.display_name_path as display_name_path, "
                            . "rfat$uniq.parent_ids as parent_ids, "
                            . "rfat$uniq.children_ids as children_ids, "
                            . "$alias.*",
                'join' => "left join accounting.recursive_cost_locations rfat$uniq on $alias.id=rfat$uniq.id",
                'order' => "rfat$uniq.display_name_full",
            ),
            'onlyVirtual' =>[
                'condition' => "$alias.model_name IS NULL OR $alias.model_name=''::TEXT"
            ],
            'withFinance' => [
                'condition' => "exists(select 1 from accounting.report_total_r where cost_location_id = $alias.id)"
            ]
        );
    }
    
    public function withIncomeCostServices()
    {
        $alias = $this->getTableAlias();
        $uniq = SIMAHtml::uniqid();
        $criteria = new SIMADbCriteria([
            'select' => "$alias.*, "
                . "rtcl$uniq.income            as income_services, "
                . "rtcl$uniq.income_notpayed   as income_notpayed_services, "
                . "rtcl$uniq.income_advance    as income_advance_services, "
                . "rtcl$uniq.cost              as cost_services, "
                . "rtcl$uniq.cost_notpayed     as cost_notpayed_services, "
                . "rtcl$uniq.cost_advance      as cost_advance_services ",
            'join' => "left join accounting.report_services rtcl$uniq on $alias.id = rtcl$uniq.cost_location_id"
        ]);
        
        $this->getDbCriteria()->mergeWith($criteria);
        return $this;
    }
    
    public function withIncomeCostWarehouse()
    {
        $alias = $this->getTableAlias();
        $uniq = SIMAHtml::uniqid();
        $criteria = new SIMADbCriteria([
            'select' => "$alias.*, "
                . "aclic$uniq.income            as income_warehouse, "
                . "aclic$uniq.income_notpayed   as income_notpayed_warehouse, "
                . "aclic$uniq.income_advance    as income_advance_warehouse, "
                . "aclic$uniq.cost              as cost_warehouse, "
                . "aclic$uniq.cost_notpayed     as cost_notpayed_warehouse, "
                . "aclic$uniq.cost_advance      as cost_advance_warehouse ",
            'join' => "left join accounting.report_warehouse_cost_location aclic$uniq on $alias.id = aclic$uniq.cost_location_id"
        ]);
        
        $this->getDbCriteria()->mergeWith($criteria);
        return $this;
    }
    
    public function withIncomeCostSalary()
    {
        $alias = $this->getTableAlias();
        $uniq = SIMAHtml::uniqid();
        $criteria = new SIMADbCriteria([
            'select' => "$alias.*, "
                . "aclic$uniq.income            as income_salary, "
                . "aclic$uniq.income_notpayed   as income_notpayed_salary, "
                . "aclic$uniq.income_advance    as income_advance_salary, "
                . "aclic$uniq.cost              as cost_salary, "
                . "aclic$uniq.cost_notpayed     as cost_notpayed_salary, "
                . "aclic$uniq.cost_advance      as cost_advance_salary ",
            'join' => "left join accounting.report_salary_cost_location aclic$uniq on $alias.id = aclic$uniq.cost_location_id"
        ]);
        
        $this->getDbCriteria()->mergeWith($criteria);
        return $this;
    }
    
    public function withIncomeCostCashDesk()
    {
        $alias = $this->getTableAlias();
        $uniq = SIMAHtml::uniqid();
        $criteria = new SIMADbCriteria([
            'select' => "$alias.*, "
                . "aclic$uniq.income            as income_cash_desk, "
                . "aclic$uniq.income_notpayed   as income_notpayed_cash_desk, "
                . "aclic$uniq.income_advance    as income_advance_cash_desk, "
                . "aclic$uniq.cost              as cost_cash_desk, "
                . "aclic$uniq.cost_notpayed     as cost_notpayed_cash_desk, "
                . "aclic$uniq.cost_advance      as cost_advance_cash_desk ",
            'join' => "left join accounting.report_cash_desks_cost_location aclic$uniq on $alias.id = aclic$uniq.cost_location_id"
        ]);
        
        $this->getDbCriteria()->mergeWith($criteria);
        return $this;
    }
    
    
    public function withIncomeCost()
    {
        $alias = $this->getTableAlias();
        $uniq = SIMAHtml::uniqid();
        $criteria = new SIMADbCriteria([
            'select' => "$alias.*, "
            . "aclic$uniq.income            as income, "
            . "aclic$uniq.income_notpayed   as income_notpayed, "
            . "aclic$uniq.income_advance    as income_advance, "
            . "aclic$uniq.cost              as cost, "
            . "aclic$uniq.cost_notpayed     as cost_notpayed, "
            . "aclic$uniq.cost_advance      as cost_advance ",
            'join' => "left join accounting.report_total_cost_location aclic$uniq on $alias.id = aclic$uniq.cost_location_id"
        ]);
        
        $this->getDbCriteria()->mergeWith($criteria);
        return $this;
    }    
    
    public function withIncomeCostR()
    {
        $alias = $this->getTableAlias();
        $uniq = SIMAHtml::uniqid();
        $criteria = new SIMADbCriteria([
            'select' => "$alias.*, "
            . "rtcl$uniq.income            as income_r, "
            . "rtcl$uniq.income_notpayed   as income_notpayed_r, "
            . "rtcl$uniq.income_advance    as income_advance_r, "
            . "rtcl$uniq.cost              as cost_r, "
            . "rtcl$uniq.cost_notpayed     as cost_notpayed_r, "
            . "rtcl$uniq.cost_advance      as cost_advance_r ",
            'join' => "left join accounting.report_total_r_cost_location rtcl$uniq on $alias.id = rtcl$uniq.cost_location_id"
        ]);
        
        $this->getDbCriteria()->mergeWith($criteria);
        return $this;
    }
    
    /**
     * OVO JE PRIVREMENA FUNKCIJA DOK SE NE RESI PROBLEM DA MOGU DA SE JOINUJU scopovi odozgo
     * mada je mozda i OK da ovo ide u VueSHOT da bi bio samo jedan model u SHOT-u
     * @return $this
     */
    public function withIncomeCostALL()
    {
        $alias = $this->getTableAlias();
        $uniq1 = SIMAHtml::uniqid();
        $uniq2 = SIMAHtml::uniqid();
        $uniq0 = SIMAHtml::uniqid();
        $uniq3 = SIMAHtml::uniqid();
        $uniq4 = SIMAHtml::uniqid();
        $uniq5 = SIMAHtml::uniqid();
        $criteria = new SIMADbCriteria([
            'select' => "$alias.*, "
                . "aclic$uniq1.income            as income, "
                . "aclic$uniq1.income_notpayed   as income_notpayed, "
                . "aclic$uniq1.income_advance    as income_advance, "
                . "aclic$uniq1.cost              as cost, "
                . "aclic$uniq1.cost_notpayed     as cost_notpayed, "
                . "aclic$uniq1.cost_advance      as cost_advance, "
                . "rtcl$uniq2.income             as income_r, "
                . "rtcl$uniq2.income_notpayed    as income_notpayed_r, "
                . "rtcl$uniq2.income_advance     as income_advance_r, "
                . "rtcl$uniq2.cost               as cost_r, "
                . "rtcl$uniq2.cost_notpayed      as cost_notpayed_r, "
                . "rtcl$uniq2.cost_advance       as cost_advance_r, "
                . "aclic$uniq0.income            as income_services, "
                . "aclic$uniq0.income_notpayed   as income_notpayed_services, "
                . "aclic$uniq0.income_advance    as income_advance_services, "
                . "aclic$uniq0.cost              as cost_services, "
                . "aclic$uniq0.cost_notpayed     as cost_notpayed_services, "
                . "aclic$uniq0.cost_advance      as cost_advance_services, "
                . "aclic$uniq3.income            as income_warehouse, "
                . "aclic$uniq3.income_notpayed   as income_notpayed_warehouse, "
                . "aclic$uniq3.income_advance    as income_advance_warehouse, "
                . "aclic$uniq3.cost              as cost_warehouse, "
                . "aclic$uniq3.cost_notpayed     as cost_notpayed_warehouse, "
                . "aclic$uniq3.cost_advance      as cost_advance_warehouse, "
                . "aclic$uniq4.income            as income_salary, "
                . "aclic$uniq4.income_notpayed   as income_notpayed_salary, "
                . "aclic$uniq4.income_advance    as income_advance_salary, "
                . "aclic$uniq4.cost              as cost_salary, "
                . "aclic$uniq4.cost_notpayed     as cost_notpayed_salary, "
                . "aclic$uniq4.cost_advance      as cost_advance_salary, "
                . "aclic$uniq5.income            as income_cash_desk, "
                . "aclic$uniq5.income_notpayed   as income_notpayed_cash_desk, "
                . "aclic$uniq5.income_advance    as income_advance_cash_desk, "
                . "aclic$uniq5.cost              as cost_cash_desk, "
                . "aclic$uniq5.cost_notpayed     as cost_notpayed_cash_desk, "
                . "aclic$uniq5.cost_advance      as cost_advance_cash_desk ",
            'join' => "left join accounting.report_total_cost_location aclic$uniq1 on $alias.id = aclic$uniq1.cost_location_id "
                . "left join accounting.report_total_r_cost_location rtcl$uniq2 on $alias.id = rtcl$uniq2.cost_location_id "
                . "left join accounting.report_services_cost_location aclic$uniq0 on $alias.id = aclic$uniq0.cost_location_id "
                . "left join accounting.report_warehouse_cost_location aclic$uniq3 on $alias.id = aclic$uniq3.cost_location_id "
                . "left join accounting.report_salary_cost_location aclic$uniq4 on $alias.id = aclic$uniq4.cost_location_id "
                . "left join accounting.report_cash_desks_cost_location aclic$uniq5 on $alias.id = aclic$uniq5.cost_location_id " 
        ]);
        
        $this->getDbCriteria()->mergeWith($criteria);
        return $this;
    }   
    
    /**
     * scope koji popunjava children_ids
     * @return $this
     */
    public function withParentChild()
    {
        $alias = $this->getTableAlias();
        $uniq = SIMAHtml::uniqid();
        $criteria = new SIMADbCriteria([
            'select' => "$alias.*, aclpc$uniq.children_ids            as children_ids ",
            'join' => "left join "
            . " (   SELECT parent_id, array_agg(child_id) as children_ids
                    FROM accounting.cost_location_parent_child
                    group by parent_id) aclpc$uniq "
                . " on $alias.id = aclpc$uniq.parent_id"
        ]);
        
        $this->getDbCriteria()->mergeWith($criteria);
        return $this;
    }

    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters' : return array_merge(parent::modelSettings('filters'), array(
                    'work_name' => 'text',
                    'year' => 'relation',
                    'location' => 'relation',
                    'comment' => 'text',
                    'number' => 'integer',
                    'for_analytics' => 'boolean',
                    'prefix' => 'text'
                ));
            case 'textSearch' : return array(
                    'name' => 'text',
                    'work_name' => 'text',
                );
            case 'options': return ['open','form','delete'];
            case 'update_relations': return ['parent'];
            case 'form_list_relations' : return ['virtual_parents'];
            default: return parent::modelSettings($column);
        }
    }
    
    public function beforeDelete()
    {
        $_warehouse = $this->owner->warehouse_production_location;
        if (!is_null($_warehouse))
        {
            $_warehouse->delete();
        }
        return parent::beforeDelete();
    }
    
    /**
     * Trazenje Mesta troska koji je roditelj trenutnom mestu troska, a za koji se radi analiticko knjizenje
     * trazi si i pravi i virtuelni roditelj. S obzirom da je moguce naleteti na dve, koristi se prvi
     * @param bool $raise_notice - da li da se digne Notice ukoliko postoji vise od jednog roditelja
     * @return ?CostLocation - null je ako ne postoji roditelj na koji treba da se knjizi
     */
    public function findForAnalytics(bool $raise_notice = true):?CostLocation
    {
        if ($this->for_analytics)
        {
            return $this;
        }
        
        $result_parents = [];
        
        //TODO: MIlosS: OVO NE MOZE DA OSTANE. MORA DA SE PREPAKUJE DA SE LEPO IZRACUNA
        if (is_array($this->virtual_parents))
        {
            foreach($this->virtual_parents as $virtual_parent)
            {
                $parent_for_analytic = $virtual_parent->findForAnalytics($raise_notice);
                if (!is_null($parent_for_analytic))
                {
                    $result_parents[] = $parent_for_analytic;
                }
            }
        }
        
        if (!is_null($this->parent))
        {
            $parent_for_analytic = $this->parent->findForAnalytics($raise_notice);
            if (!is_null($parent_for_analytic))
            {
                $result_parents[] = $parent_for_analytic;
            }
        }
        
        if (count($result_parents) === 0)
        {
            return null;
        }
        else
        {
            if (count($result_parents) > 1)
            {
                $result_parents_names = [];
                foreach ($result_parents as $_parent)
                {
                    $result_parents_names[] = $_parent->DisplayName;
                }
                if ($raise_notice)
                {
                    Yii::app()->raiseNote($this->DisplayName. ' može da se knjiži na nalozima: <br />'. implode('<br />', $result_parents_names)
                        .'<br /><br />Izabran je nalog: '.$result_parents[0]->DisplayName
                        );
                }
            }
            
            return $result_parents[0];
        }
        
        
    }
    
    public function getBillItemsFilter($filter)
    {
        switch ($filter)
        {
            case 'income':  case 'cost':
            case 'income_advance':  case 'cost_advance':
            case 'income_notpayed':  case 'cost_notpayed':
            case 'income_total': case 'cost_total':
            case 'income_advance_total':  case 'cost_advance_total':
            case 'income_notpayed_total':  case 'cost_notpayed_total':
                $bill_type = (strpos($filter, 'advance') === FALSE)?Bill::$BILL:Bill::$ADVANCE_BILL;
                $scopes = [];
                if (!in_array($filter, ['income','cost','income_total','cost_total']))
                {
                    $scopes = ['unreleased'];
                }
                $invoice = (strpos($filter, 'cost') === FALSE)?true:false;
                if (strpos($filter, 'total') === FALSE)
                {
                    $cl_ids = $this->id;
                }
                else
                {
                    $_cl = CostLocation::model()->withParentChild()->findByPk($this->id);
                    $cl_ids = str_replace(['{','}'], '', $_cl->children_ids);
                }
                return [
                    'model_filter' => [
                        'OR',
                        [
                            'cost_location' => ['ids' => $cl_ids],
                            'bill' => [
                                'bill_type' => $bill_type,
                                'invoice' => $invoice,
                                'scopes' => $scopes
                            ],
                        ],
                        [
                            'AND',
                            [
                                'NOT',
                                ['cost_location' => []]
                            ],
                            [
                                'bill' => [
                                    'bill_type' => $bill_type,
                                    'invoice' => $invoice,
                                    'local_cost_location' => ['ids' => $cl_ids],
                                    'scopes' => $scopes
                                ],
                            ]
                        ]
                    ]
                ];
            default: 
//                error_log(__METHOD__.': '.$filter);
                return ['model_filter' => ['ids' => -1]];
        }
    }
}
