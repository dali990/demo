<?php

class PreBillOut extends PreBill
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function init()
    {
        parent::init();
        $this->invoice = true;
        $this->lock_amounts = false;
    }

    public function defaultScope()
    {
        $alias = $this->getTableAlias(FALSE, FALSE);
        $bill_type = Bill::$PRE_BILL;
        return array(
            'condition' => "$alias.invoice=true and $alias.bill_type = '$bill_type' and $alias.canceled=false",
        );
    }
    
    public function relations($child_relations = [])
    {
        return parent::relations(array_merge([
            'sales_order' => [self::HAS_ONE,'SalesOrder','pre_bill_out_id']
        ],$child_relations));
    }

}