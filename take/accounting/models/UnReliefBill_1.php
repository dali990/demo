<?php

class UnReliefBill extends Bill
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    
    public function init()
    {
        parent::init();
        $this->bill_type = self::$UNRELIEF_BILL;
    }

    public function defaultScope()
    {
        $alias = $this->getTableAlias(FALSE, FALSE);
        $bill_type = Bill::$UNRELIEF_BILL;
        return array(
            'condition' => "$alias.bill_type = '$bill_type' and $alias.canceled=false",
        );
    }

}