<?php

class CashDesk extends SIMAActiveRecord
{
    protected static $_main = null;
    
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'accounting.cash_desks';
    }

    public function moduleName()
    {
        return 'accounting';
    }

    public function __get($column)
    {
        switch ($column)
        {
            case 'DisplayName': 
            case 'SearchName': return $this->name;
            default: return parent::__get($column);
        }
    }


    public function relations($child_relations = [])
    {
        return array(
            'cash_desk_logs' => array(self::BELONGS_TO, 'CashDeskLog', 'cash_desk_id'),
        );
    }

    public function rules()
    {
        return array(
            array('name', 'required'),
            array('description', 'safe'),
        );
    }
    
    public function scopes()
    {
        $alias = $this->getTableAlias();
        return array(
            'byName' =>[
                'order' => $alias.'name'
            ],
            'mainCashDesk' => [
                'condition' => $alias.'.is_main = true'
            ]
        );
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters': return array(
                'name' => 'text',
                'description' => 'text',
            );
            case 'textSearch': return array(
                'name' => 'text',
                'description' => 'text',
            );
            default : return parent::modelSettings($column);
        }
    }  
    
    public static function mainCashDesk()
    {
        if (is_null(self::$_main))
        {
            self::$_main = CashDesk::model()->find('is_main = true');
        }
        if (is_null(self::$_main))
        {
            self::$_main = new CashDesk();
            self::$_main->name = 'PRIMAR';
            self::$_main->is_main = true;
            self::$_main->save();
        }
        return self::$_main;
    }

}