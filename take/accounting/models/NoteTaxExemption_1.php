<?php

class NoteTaxExemption extends SIMAActiveRecord
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'accounting.notes_of_tax_exemption';
    }

    public function moduleName()
    {
        return 'accounting';
    }

    public function __get($column)
    {
        switch ($column)
        {
           case 'DisplayName': return $this->name_of_note;
           case 'SearchName':return $this->DisplayName;
           default: return parent::__get($column);
        }
    }
    
    public function rules()
    {
        return [
            ['name_of_note', 'required'],
            ['text_of_note', 'safe'],
            ['name_of_note', 'unique'],
        ];
    }
    
    public function relations($child_relations = [])
    {
        return parent::relations(array_merge([
            'bills' => [self::HAS_MANY, 'BillOut', 'note_of_tax_exemption_id']
        ],$child_relations));
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters': return [
                'name_of_note' => 'text',
                'text_of_note' => 'text'
            ];
            case 'textSearch': return [
                'name_of_note' => 'text'
            ];
            case 'options': return ['form', 'delete'];
            default: return parent::modelSettings($column);
        }
    }
}