<?php

class ReliefBillIn extends ReliefBill
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function init()
    {
        parent::init();
        $this->invoice = false;
    }

    public function defaultScope()
    {
        $alias = $this->getTableAlias(FALSE, FALSE);
        $bill_type = Bill::$RELIEF_BILL;
        return array(
            'condition' => "$alias.invoice=false and $alias.bill_type = '$bill_type' and $alias.canceled=false",
        );
    }

}