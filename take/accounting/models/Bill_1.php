<?php

class Bill extends SIMAActiveRecord
{

    public static $BILL = 'BILL';
    public static $ADVANCE_BILL = 'ADVANCE_BILL';
    public static $RELIEF_BILL = 'RELIEF_BILL';
    public static $UNRELIEF_BILL = 'UNRELIEF_BILL';
    public static $PRE_BILL = 'PRE_BILL';
    

    /**
     * prilikom filtriranja, ovde se smestaju tagovi
     */
    public $belongs;
    public $belongs_recursive;
    public $unreleased_percent_of_amount;
    public $amount_local = 'o';
    public $amount_in_currency;
    public $unreleased_amount_in_currency;

    public $vat_refusal = null;
    public $_cost_type_id = null;
    public $cost_type_mixed = false; //inicijalno je false za nove modele
    public $cost_types = []; //izracunato iz stavki
    
    public $_cost_location_id = null;
    public $cost_location_mixed = false; //inicijalno je false za nove modele
    public $cost_locations = []; //izracunato iz stavki
    
    //popunjava se scope-om - withVatInMonth
    public $vat_in_month_id = null;
    public $canceled_in_month_id = null;
    
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'private.bills';
    }

    public function moduleName()
    {
        return 'accounting';
    }

    public function __get($column)
    {
        switch ($column)
        {
            case 'DisplayName': return $this->bill_number;
            case 'SearchName': return $this->bill_number;
            case 'path2': return 'files/file';

            case 'pib_jmbg': return $this->partner->pib_jmbg;
            case 'mb_jmbg': return $this->partner->mb_jmbg;
            case 'work_code': return $this->partner->work_code;
            case 'isReleased': return (abs($this->unreleased_amount) < 0.001);
            case 'isBill': return $this->bill_type == self::$BILL;
            case 'isAdvanceBill': return $this->bill_type == self::$ADVANCE_BILL;
            case 'isReliefBill': return $this->bill_type == self::$RELIEF_BILL;
            case 'isUnReliefBill': return $this->bill_type == self::$UNRELIEF_BILL;
            case 'isPreBill': return $this->bill_type == self::$PRE_BILL;
            case 'isBillWT': return !is_null(WarehouseTransfer::model()->findByPk($this->id));
            
            //tipovi racuna koji mogu da imaju popust
            case 'isBillTypeWithDiscount': return $this->isBill || $this->isPreBill;
                
            case 'hasWarehouseItems': return $this->warehouse_bill_items_count>0;
            case 'hasServicesItems': return $this->services_bill_items_count>0;
            case 'hasUnconfirmedItems': return $this->services_bill_items_unconfirmed_count>0;
            case 'hasFixedAssetsItems': return $this->fixed_assets_bill_items_count>0;
                
            case 'hasReliefes': return (isset($this->reliefes) && count($this->reliefes) > 0);
                
            case 'amount_letters': return SIMAHtml::money2letters($this->amount);
            case 'amount_letters_lat': return SIMAHtml::money2letters($this->amount, 'lat');
                
            case 'total_amount10': return $this->base_amount10 + $this->vat10;
            case 'total_amount20': return $this->base_amount20 + $this->vat20;
              
            case 'cost_type_direction': 
                if ($this->invoice == true)
                {
                    return CostType::$TYPE_INCOME;
                }
                elseif ($this->invoice == false)
                {
                    return CostType::$TYPE_COST;
                }
                else
                {
                    throw new SIMAWarnException('pozvan cost_type_direction bez potrebe');
                }
                
            case 'booking_date':
                if (isset($this->file->account_document->account_order))
                    return $this->file->account_document->account_order->date;
                else
                    return null;
                
            case 'days_for_payout': 
                
                if (empty($this->payment_date))
                {
                    return 0;
                }

                return (new SIMADateTime($this->payment_date))->diffDays(new DateTime($this->income_date));
            case 'cost_type_id':
                if ($this->cost_type_mixed === false)
                {
                    return $this->_cost_type_id;
                }
                else
                {
                    return null;
                }
            case 'cost_location_id':
                if ($this->cost_location_mixed === false)
                {
                    return $this->_cost_location_id;
                }
                else
                {
                    return null;
                }
            default: return parent::__get($column);
        }
    }
    
    public function __set($column, $value)
    {
        switch ($column)
        {
            case 'cost_type_id': 
                if ($this->cost_type_mixed === false)
                {
                    $this->_cost_type_id = $value;
                    $this->local_cost_type_id = $value;
                }
                else
                {
                    //kulira se. Ovo uglavnom ide prazno iz forme kada i onako ne treba setovati
//                    throw new SIMAException('cost_type_mixed miksovan bill '.SIMAMisc::toTypeAndJsonString($this->cost_type_mixed));
                }
                break;
            case 'cost_location_id': 
                if ($this->cost_location_mixed === false)
                {
                    $this->_cost_location_id = $value;
                    $this->local_cost_location_id = $value;
                }
                else
                {
                    //kulira se. Ovo uglavnom ide prazno iz forme kada i onako ne treba setovati
//                    throw new SIMAException('cost_location miksovan bill '.SIMAMisc::toTypeAndJsonString($this->cost_location_mixed));
                }
                break;
            default: parent::__set($column, $value); break;
        }   
    }
    
    public function __isset($column)
    {
        switch ($column)
        {
            //u sustini su oba uvek setovana, odnosno postoje
            case 'cost_type_id': 
            case 'cost_location_id': 
                return true;
            default: return parent::__isset($column);
        }   
    }
    
    protected function calcLocalValues()
    {
        parent::calcLocalValues();
        $this->vat_refusal = $this->calcVatRefusal();
        $this->calcCostTypeLocationID('type','CostType');
        $this->calcCostTypeLocationID('location','CostLocation');
    }
    
    protected function populateOld()
    {
        parent::populateOld();
        $this->__old['vat_refusal'] = $this->vat_refusal;

        $this->__old['cost_type_id'] = $this->cost_type_id;
        $this->__old['cost_type_mixed'] = $this->cost_type_mixed;

        $this->__old['cost_location_id'] = $this->cost_location_id;
        $this->__old['cost_location_mixed'] = $this->cost_location_mixed;
    }
    
    private function calcVatRefusal()
    {
        $vat_refusal_allowed_list = ['OR'];
        foreach (POPDVBillLists::$chooseVatRefusalList as $item)
        {
            $vat_refusal_allowed_list[] = ['no_vat_type' => $item];
        }
        $cnt_dont_use_vat_refusal = BillItem::model()->countByModelFilter([
            'AND',
            ['bill' => ['ids' => $this->id]],
            ['NOT',$vat_refusal_allowed_list]
        ]);
        
        $ret = $this->vat_refusal_calc;
        if ($cnt_dont_use_vat_refusal > 0 || $ret === 0)
        {
            $ret = null;
        }
        return $ret;
    }
    
    private function calcCostTypeLocationID($column_part,$model)
    {
        $column = "cost_$column_part"."_id";
        $local_column = 'local_'.$column;
        $_column = '_'.$column;
        $column_mixed = "cost_$column_part"."_mixed";
        $model_param = "cost_$column_part"."s";
        
        $bill_items = BillItem::model()->findAll([
            'model_filter' => [
                'bill' => ['ids'=>$this->id],
                'confirmed' => true
            ],
            'group' => $column,
            'select' => $column,
            'order' => 'sum(value) DESC' //da prvo izadje sa najvecom vrednoscu
        ]);
        
        $_cl_ids = [];
        $_cl_top = null;
        foreach ($bill_items as $one_item)
        {
            if (empty($one_item->$column))
            {
                $one_item->$column = $this->$local_column;
            }
            
            if (!empty($one_item->$column))
            {
                $_cl_ids[] = $one_item->$column;
                if (is_null($_cl_top))
                {
                    $_cl_top = $one_item->$column;
                }
            }
        }
        $_used_ides = array_unique($_cl_ids);
        
        if (count($_used_ides)>1)
        {
            $this->$_column = null;
            $this->$column_mixed = true;
        }
        elseif (count($_used_ides)===0)
        {
            $this->$_column = $this->$local_column;
            $this->$column_mixed = false;
        }
        else
        {
            $this->$_column = $_used_ides[0];
            $this->$column_mixed = false;
        }

        if (empty($_used_ides))
        {
            $this->$model_param = [];
        }
        else
        {
            $this->$model_param = $model::model()->findAll([
                'model_filter' => [
                    'ids' => $_used_ides
                ],
                //fora da ovaj  
               'order' => !empty($_cl_top) ? "array_position(ARRAY[$_cl_top], id) ASC NULLS LAST" : ''
            ]);
        }
    }
    
    
    
    public function updateLocalCostTypeLocation()
    {
        $this->refresh();
        $changed = false;
        if (count($this->cost_locations) > 0)
        {
            //uzima se prvi jer je sortirano po iznosima stavki, pa se uzima najveci
            if ($this->local_cost_location_id != $this->cost_locations[0]->id)
            {
                $this->local_cost_location_id = $this->cost_locations[0]->id;
                $changed = true;
            }
            
        }
        elseif(!empty($this->local_cost_location_id))
        {
            $this->local_cost_location_id = null;
            $changed = true;
        }
        if (count($this->cost_types) > 0)
        {
            //uzima se prvi jer je sortirano po iznosima stavki, pa se uzima najveci
            if ($this->local_cost_type_id != $this->cost_types[0]->id)
            {
                $this->local_cost_type_id = $this->cost_types[0]->id;
                $changed = true;
            }
        }
        elseif(!empty($this->local_cost_type_id))
        {
            $this->local_cost_type_id = null;
            $changed = true;
        }
        if ($changed)
        {
            $this->save();
        }
    }
    
    public function defaultScope()
    {
        $alias = $this->getTableAlias(FALSE, FALSE);
        return array(
            'condition' => $alias . '.canceled=false',
        );
    }

    public function setscan_order($bool)
    {
        if (isset($this->file))
        {
            $this->file->scan_order = $bool;
            $this->file->save();
        }
    }

    public function getscan_order()
    {
        if (isset($this->file))
            return $this->file->scan_order;
        else
            return false;
    }

    public function rules()
    {
        return array(
            array('partner_id, income_date', 'required'),
            ['partner_id','NoPartnerChangeWhenReleased'],
            array('traffic_date, invoicing_basis, invoice_comment, traffic_location_id, issue_location_id', 'safe'),
            array('amount,  base_amount0, base_amount10, base_amount20, vat10, vat20, rounding, recurring_bill_id, value_customs', 'safe'),
//     		array('invoice', 'required'), TODO(srecko):srediti da je obavezno?
            array('invoice, payment_date, comment, vat_refusal,responsible_id, confirm1_user_id, confirm2_user_id, confirm1_timestamp, confirm2_timestamp', 'safe'),
            array('payment_id, advance_bill_id, relief_bill_id, year_id, interest, partner_bank_account_id', 'safe'),
            array('confirm1, confirm2, call_for_number_modul', 'safe'),
            array('like_payment_id, like_advance_bill_id, like_relief_bill_id', 'safe'),
            array('cost_location_id, id, call_for_number, original', 'safe'),
            ['vat_in_diferent_month_id, note_of_tax_exemption_id', 'safe'],
            ['vat10', 'checkVat','percent'=>10,'base_column'=>'base_amount10'],
            ['vat20', 'checkVat','percent'=>20,'base_column'=>'base_amount20'],
            ['amount', 'checkAmountSumWithVAT'], //zbir osnovica i PDV
            ['vat10, vat20, base_amount0, base_amount10, base_amount20, value_customs', 'noChangeWhenConfirmed', 
                'confirmed_column' => 'lock_amounts', 'negative' => true, 
                'message' => Yii::t('AccountingModule.Bill','noChangeWhenUnLocked')
            ],
            ['amount, vat10, vat20, base_amount0, base_amount10, base_amount20, value_customs','onlyPositiveAmounts'],
            ['amount', 'checkAmountWithUnreleased'],
            ['income_date, payment_date', 'date', 'format' => Yii::app()->params['date_format']],
            array('belongs, belongs_recursive', 'safe'),
            array('ids, group_by, order_by, by_contract, payment_conditions, tax_note', 'safe'),
            array('amount, base_amount0, base_amount10, base_amount20, vat10, vat20', 'numerical', 'message' => 'Polje "{attribute}" mora biti broj!'),
            array('bill_number', 'checkBillNumber'),
            ['lock_amounts','safe'],
            ['id', 'unique'],
            array('payment_date, id, year_id', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => array('insert', 'update')),
            array('cost_location_id, partner_id', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => array('insert', 'update')),
            ['confirm2', 'checkConfirm2', 'on' => ['insert', 'update']]
        );
    }

    public function checkConfirm2()
    {
        if (!$this->hasErrors())
        {
            if ($this->columnChanged('confirm2') && $this->confirm2 && !$this->confirm1)
            {
                $this->addError('confirm2', Yii::t('AccountingModule.Bill', 'BillDoesNotHaveConfirmation1'));
            }
        }
    }
    
    public function NoPartnerChangeWhenReleased()
    {
        if ($this->hasErrors() || !$this->columnChanged('partner_id'))
        {
            return;
        }
        if  (
                $this->releases_count > 0
                ||
                $this->advance_releases_count > 0
                ||
                $this->reliefes_count > 0
                ||
                //avansni racun i knjizno odobrenje ka obicnom racunu
                (isset($this->bill_releases_count) && $this->bill_releases_count > 0)
                ||
                //avansni racun ka knjiznom odobrenju
                (isset($this->advance_bill_reliefs_count) && $this->advance_bill_reliefs_count > 0)
                ||
                //knjizno odobrenje ka avansnom racunu
                (isset($this->relief_bill_reliefs_count) && $this->relief_bill_reliefs_count > 0)
            )
        {
            $this->addError('partner_id', Yii::t('AccountingModule.Bill','CantChangePartnerHaveReleases'));
        }
        if  (
                $this->warehouse_transfers_count > 0
                ||
                $this->warehouse_bill_items_count > 0
            )
        {
            $this->addError('partner_id', Yii::t('AccountingModule.Bill','CantChangePartnerHaveWarehouseTransfers'));
        }
    }
    
    public function checkVat($attribute, $params)
    {
        if (!$this->hasErrors())
        {
            $base_column = $params['base_column'];
            $percent = $params['percent']/100.0;
            if (
                    $this->columnChangedOrNew($attribute)
                    || $this->columnChangedOrNew($base_column)
//                    !SIMAMisc::areEqual($this->$attribute,  $this->__old[$attribute]) 
//                    || !SIMAMisc::areEqual($this->$base_column,$this->__old[$base_column])
                )
            {
                if (!SIMAMisc::areEqual($this->$attribute, $this->$base_column * $percent,1)) //ostavljeno je malo vise prostora
                {
                    $this->addError($attribute, "Nije dobar iznos PDV-a za procenat ".$params['percent']."%");
                }
            }
        }
    }

    public function checkAmountSumWithVAT()
    {
        if (!$this->hasErrors() && $this->lock_amounts)
        {
            $_vat_sum = $this->vat10 + $this->vat20;
            $_base_amount = $this->base_amount0 + $this->base_amount10 + $this->base_amount20;
            $sum = $_base_amount + $_vat_sum + doubleval($this->rounding) + $this->interest + $this->value_customs;
            if (!SIMAMisc::areEqual($this->amount, $sum))
            {
                $this->addError('amount', 'Zbir osnovica i svih stopa PDV nije jednak ukupnom iznosu(' . SIMAHtml::number_format($sum) . ')');
            }
        }
    }
    
    public function onlyPositiveAmounts($column)
    {
        if ($this->isReliefBill)
        {
            if (SIMAMisc::lessThen(0, $this->$column))
            {
                $this->addError($column, Yii::t('AccountingModule.Bill','CouldNotBePositive'));
            }
        }
        else
        {
            if (SIMAMisc::lessThen($this->$column, 0))
            {
                $this->addError($column, Yii::t('AccountingModule.Bill','CouldNotBeNegative'));
            }
        }
        
    }
    
    
    /**
     * MilosS(20.6.2017): bitna je provera iz Bill da vrednosti ne mogu da se menjaju kada su zakljucane, 
     * jer se podrazumeva da je $this->lock_amounts === true
     */
    public function checkAmountWithUnreleased()
    {
        if (!$this->hasErrors() && $this->columnChanged('amount'))
        {
            if ($this->bill_type === Bill::$RELIEF_BILL)
            {
                $sign = -1;
            }
            else
            {
                $sign = 1;
            }
            $amount_down_for = floatval($this->__old['amount']) - $this->amount;
            if (SIMAMisc::lessThen($sign*$this->unreleased_amount, $sign*$amount_down_for))
            {
                $this->addError('amount',Yii::t('AccountingModule.Bill','AmountUnderReleased'));
            }
        }
    }

    private function counterConfigCode()
    {
        switch ($this->bill_type)
        {
            case self::$BILL:           return 'accounting.counters.bill_out';
            case self::$ADVANCE_BILL:   return 'accounting.counters.advance_bill_out';
            case self::$PRE_BILL:       return 'accounting.counters.pre_bill_out';
            case self::$RELIEF_BILL:    return 'accounting.counters.relief_bill_out';
            case self::$UNRELIEF_BILL:  return 'accounting.counters.unrelief_bill_out';
            default: throw new SIMAWarnAutoBafException('counterConfigCode -> ne postoji tip racuna: '.$this->bill_type);
        }
    }
    
    /**
     * Proverava da li u sistemu postoji racun koji je od istog partnera i ima isti naziv
     * @param type $attribute
     * @param type $params
     */
    public function checkBillNumber()
    {
        if (!$this->hasErrors() && !$this->canceled) //mora zbog income_date
        {
            /**
             * ukoliko je prazan, proverava se da li je sledeci broj zauzet
             */
            $day = Day::getByDate($this->income_date);
            $local_bill_number = $this->bill_number;
            
            $user_config_counter_id = Yii::app()->configManager->get($this->counterConfigCode(), false);
            if (empty($local_bill_number) && !is_null($user_config_counter_id))
            {
                $counter = UserConfigCounter::model()->findByPk($user_config_counter_id);
                $local_bill_number = $counter->getNextText($day, true);
            }   
            if (empty($local_bill_number))
            {
                $this->addError('bill_number', Yii::t('AccountingModule.Bill','BillNumberEmpty'));
            }
            
            $_attributes = [
                'bill_number' => $local_bill_number,
                'year_id' => $day->year->id,
                'bill_type' => $this->bill_type,
                'canceled' => false,
                'invoice' => $this->invoice
            ];
            if (!$this->invoice)
            {
                $_attributes['partner_id'] = $this->partner_id;
            }
            $bill = Bill::model()->findByAttributes($_attributes);
            
            if ($bill != null && $bill->id != $this->id)
            {
                //bill number se postavlja jer se zna da nece ici dalje jer ima greska
                $this->bill_number = $local_bill_number;
                $this->addError('bill_number', 'Ovakav racun vec postoji u sistemu - ' . ((isset($bill->file->filing) ? $bill->file->filing->filing_number : $bill->bill_number)));
                if (!$this->invoice)
                {
                    $this->addError('partner_id', 'Ovakav racun vec postoji u sistemu - ' . ((isset($bill->file->filing) ? $bill->file->filing->filing_number : $bill->bill_number)));
                }
                
            }
        }
    }
    
    public function getsum_per_vat_bill_items()
    {
        $sum_array = [];
        
        $alias = 't';
//        $alias = SIMAHtml::uniqid();
        $sum_items = BillItem::model()->findAll([
            'alias' => $alias,
            'model_filter' => [
                'bill' => ['ids'=>$this->id]
            ],
            'group' => "$alias.vat",
            'select' => "$alias.vat as vat"
                . ", sum($alias.value) as value "
                . ", sum($alias.value_vat) as value_vat"
        ]);
        
        foreach ($sum_items as $sum_item)
        {
            $sum_array[] = [
                'vat' => $sum_item->vat.'%',
                'value' => $sum_item->value,
                'value_vat' => $sum_item->value_vat,
                'value_total' => $sum_item->value_total,
            ];
        }
        
        return $sum_array;
    }
    
    public function relations($child_relations = [])
    {
        $bill_releases_table = BillRelease::model()->tableName();
        return parent::relations(array_merge([
            'file' => array(self::BELONGS_TO, 'File', 'id'),
            'filing' => array(self::BELONGS_TO, 'Filing', 'id'),
            'responsible' => array(self::BELONGS_TO, 'User', 'responsible_id'),
            'partner' => array(self::BELONGS_TO, 'Partner', 'partner_id'),
            'job_order' => array(self::BELONGS_TO, 'CostLocation', 'cost_location_id'),
            'cost_location' => array(self::BELONGS_TO, 'CostLocation', 'cost_location_id'),
            'local_cost_location' => array(self::BELONGS_TO, 'CostLocation', 'local_cost_location_id'),
            'cost_type' => array(self::BELONGS_TO, 'CostType', 'cost_type_id'),
            'local_cost_type' => array(self::BELONGS_TO, 'CostType', 'local_cost_type_id'),
            'job' => array(self::BELONGS_TO, 'Job', 'job_id'),
            
            'recurring_bill' => [self::BELONGS_TO, 'RecurringBill', 'recurring_bill_id'],
            
            'vat_refusal_calc' => array(self::STAT, 'BillItem', 'bill_id',
                'select' => 'case 
                                    when bool_and(vat_refusal) then true
                                    when not bool_or(vat_refusal) then false
                                    else null
                            end'
            ),
            
            'bill_items' => array(self::HAS_MANY, 'BillItem', 'bill_id'),
            'bill_items_value' => array(self::STAT, 'BillItem', 'bill_id', 'select' => 'sum(value)'),
            'bill_items_value_vat' => array(self::STAT, 'BillItem', 'bill_id', 'select' => 'sum(value_vat)'),
            'bill_items_value_with_vat' => array(self::STAT, 'BillItem', 'bill_id', 'select' => 'sum(value+value_vat)'),
            'bill_items_internal_vat_count' => array(self::STAT, 'BillItem', 'bill_id', 
                'select' => 'count(*)', 
                'condition' =>"is_internal_vat=true and internal_vat!='0'"
            ),
            'bill_items_non_internal_vat_count' => array(self::STAT, 'BillItem', 'bill_id', 
                'select' => 'count(*)',
                'condition' =>"is_internal_vat=false and vat!='0'"
            ),
            'warehouse_bill_items' => array(self::HAS_MANY, 'BillItem', 'bill_id', 
                'condition' => "bill_item_type = '".BillItem::$WAREHOUSE."'",
                'scopes' => ['byOrder']
            ),
            'services_bill_items' => array(self::HAS_MANY, 'BillItem', 'bill_id', 
                'condition' => "bill_item_type = '".BillItem::$SERVICES."' AND services_bill_items.confirmed = true",
                'scopes' => ['byOrder']
            ),
            'services_bill_items_unconfirmed' => array(self::HAS_MANY, 'BillItem', 'bill_id', 
                'condition' => "bill_item_type = '".BillItem::$SERVICES."' and confirmed=false",
                'scopes' => ['byVat']
            ),
            'fixed_assets_bill_items' => array(self::HAS_MANY, 'BillItem', 'bill_id', 
                'condition' => "bill_item_type = '".BillItem::$FIXED_ASSETS."'",
                'scopes' => ['byOrder']
            ),
            
            'bill_items_count' => array(self::STAT, 'BillItem', 'bill_id', 'select' => 'count(*)'),
            'has_discount' => [self::STAT, 'BillItem', 'bill_id', 'select' => 'count(*)>0',
                'condition' => "discount>0"
            ],
            //case 'value_total' : return $this->value + $this->value_vat + $this->value_vat_customs;
            'bill_items_max_value' => [self::STAT, 'BillItem', 'bill_id', 'select' => 'max(value+value_vat+value_vat_customs)' ],
            'bill_items_max_quantity' => [self::STAT, 'BillItem', 'bill_id', 'select' => 'max(quantity)' ],
            'warehouse_bill_items_count' => array(self::STAT, 'BillItem', 'bill_id', 
                'select' => 'count(*)', 
                'condition' => "bill_item_type = '".BillItem::$WAREHOUSE."'"
            ),
            'services_bill_items_count' => array(self::STAT, 'BillItem', 'bill_id', 
                'select' => 'count(*)', 
                'condition' => "bill_item_type = '".BillItem::$SERVICES."' AND confirmed = true"
            ),
            'services_bill_items_unconfirmed_count' => array(self::STAT, 'BillItem', 'bill_id', 
                'select' => 'count(*)', 
                'condition' => "bill_item_type = '".BillItem::$SERVICES."' and confirmed=false",
            ),
            'fixed_assets_bill_items_count' => array(self::STAT, 'BillItem', 'bill_id', 
                'select' => 'count(*)', 
                'condition' => "bill_item_type = '".BillItem::$FIXED_ASSETS."'"
            ),
            'bill_items_quantity' => array(self::STAT, 'BillItem', 'bill_id', 'select' => 'sum(quantity)'),
            'warehouse_bill_items_quantity' => array(self::STAT, 'BillItem', 'bill_id', 
                'select' => 'sum(quantity)', 
                'condition' => "bill_item_type = '".BillItem::$WAREHOUSE."'"
            ),
            'services_bill_items_quantity' => array(self::STAT, 'BillItem', 'bill_id', 
                'select' => 'sum(quantity)', 
                'condition' => "bill_item_type = '".BillItem::$SERVICES."'  AND confirmed = true"
            ),
            'fixed_assets_bill_items_quantity' => array(self::STAT, 'BillItem', 'bill_id', 
                'select' => 'sum(quantity)', 
                'condition' => "bill_item_type = '".BillItem::$FIXED_ASSETS."'"
            ),
            'releases' => array(self::HAS_MANY, 'BillRelease', 'bill_id'),
            'releases_count' => array(self::STAT, 'BillRelease', 'bill_id', 'select' => 'count(*)'),
            'released_sum'   => array(self::STAT, 'BillRelease', 'bill_id', 'select' => 'sum(amount)'),
            'confirm1_user' => array(self::BELONGS_TO, 'User', 'confirm1_user_id'),
            'confirm2_user' => array(self::BELONGS_TO, 'User', 'confirm2_user_id'),
            'warehouse_requisitions' => array(self::HAS_MANY, 'WarehouseRequisition', 'bill_id'),
            'warehouse_receives' => array(self::HAS_MANY, 'WarehouseReceiving', 'bill_id'),
            'warehouse_receives_count' => array(self::STAT, 'WarehouseReceiving', 'bill_id','select'=>'count(*)'),
            'warehouse_dispatches' => array(self::HAS_MANY, 'WarehouseDispatch', 'bill_id'),   
            'warehouse_transfers' => array(self::HAS_MANY, 'WarehouseTransfer', 'bill_id'),
            'warehouse_transfers_count' => array(self::STAT, 'WarehouseTransfer', 'bill_id', 'select'=>'count(*)'),
            'warehouse_transmittings' => array(self::HAS_MANY, 'WarehouseTransmitting', 'bill_id'),
            'payments' => array(self::MANY_MANY, 'Payment', $bill_releases_table . '(bill_id, payment_id)'),
            'payments_released_amount_sum' => array(self::STAT, 'AdvanceBillRelease', 'advance_bill_id','select'=>'sum(amount)'),
            'reliefes' => array(self::HAS_MANY, 'BillRelief', 'bill_id'),
            'reliefes_count' => array(self::STAT, 'BillRelief', 'bill_id', 'select' => 'count(*)'),
            'advance_releases' => array(self::HAS_MANY, 'AdvanceBillRelease', 'bill_id'),
            'advance_releases_count' => array(self::STAT, 'AdvanceBillRelease', 'bill_id', 'select' => 'count(*)'),
            'advance_released_amount_sum' => array(self::STAT, 'AdvanceBillRelease', 'bill_id', 'select' => 'sum(amount)'),
            'advance_released_base_amount_sum' => array(self::STAT, 'AdvanceBillRelease', 'bill_id', 'select' => 'sum(base_amount)'),
            'advance_released_base_amount10_sum' => array(self::STAT, 'AdvanceBillRelease', 'bill_id', 
                'select' => 'sum(base_amount)',
                'condition' => "vat_rate = '10'"
            ),
            'advance_released_base_amount20_sum' => array(self::STAT, 'AdvanceBillRelease', 'bill_id', 
                'select' => 'sum(base_amount)',
                'condition' => "vat_rate = '20'"
            ),
            'advance_released_vat_sum' => array(self::STAT, 'AdvanceBillRelease', 'bill_id', 'select' => 'sum(vat_amount)'),
            'advance_released_vat10_sum' => array(self::STAT, 'AdvanceBillRelease', 'bill_id', 
                'select' => 'sum(vat_amount)',
                'condition' => "vat_rate = '10'"
            ),
            'advance_released_vat20_sum' => array(self::STAT, 'AdvanceBillRelease', 'bill_id', 
                'select' => 'sum(vat_amount)',
                'condition' => "vat_rate = '20'"
            ),
            'reliefs' => [self::HAS_MANY, 'BillRelief', 'bill_id'],
            'relief_base_amount_sum' => array(self::STAT, 'BillRelief', 'bill_id', 'select' => 'sum(base_amount)'),
            'relief_base_amount10_sum' => array(self::STAT, 'BillRelief', 'bill_id', 
                'select' => 'sum(base_amount)',
                'condition' => "vat_rate = '10'"
            ),
            'relief_base_amount20_sum' => array(self::STAT, 'BillRelief', 'bill_id', 
                'select' => 'sum(base_amount)',
                'condition' => "vat_rate = '20'"
            ),
            'relief_vat_sum' => array(self::STAT, 'BillRelief', 'bill_id', 'select' => 'sum(vat_amount)'),
            'relief_vat10_sum' => array(self::STAT, 'BillRelief', 'bill_id', 
                'select' => 'sum(vat_amount)',
                'condition' => "vat_rate = '10'"
            ),
            'relief_vat20_sum' => array(self::STAT, 'BillRelief', 'bill_id', 
                'select' => 'sum(vat_amount)',
                'condition' => "vat_rate = '20'"
            ),
            'year' => array(self::BELONGS_TO, 'Year', 'year_id'),
            
            //prava promenljiva gde se setuje da se PDV racuna prikazuje u drugom mesecu u odnosu na knjizenje
            'vat_in_diferent_month' => array(self::BELONGS_TO, 'Month', 'vat_in_diferent_month_id'),
            //2 relacije  koje su na osnovu virtuelnih promenljivih gde se dobja mesec u kojem se PDV prikazuje
            'vat_in_month' => array(self::BELONGS_TO, 'Month', 'vat_in_month_id'),
            'canceled_in_month' => array(self::BELONGS_TO, 'Month', 'canceled_in_month_id'),
            
            'bound_currency' => array(self::BELONGS_TO, 'Currency', 'bound_currency_id'),
            'partner_bank_account'=>array(self::BELONGS_TO, 'BankAccount', 'partner_bank_account_id'),
            'bill_out' => [self::HAS_ONE, 'BillOut', 'id'],
            
            'bill_debt_take_overs' => [self::HAS_MANY, 'BillDebtTakeOver', 'bill_id'],
            'traffic_location' => [self::BELONGS_TO, City::class, 'traffic_location_id'],
            'issue_location' => [self::BELONGS_TO, City::class, 'issue_location_id'],
            'note_of_tax_exemption' => [self::BELONGS_TO, 'NoteTaxExemption', 'note_of_tax_exemption_id']
        ],$child_relations));
    }
    
    //MilosS(7.7.2017): nisam nasao gde se koristi, a i ako se koristi, ne treba
    //MIlosS(7.7.2017): milsim da se koristi u template->u treba napraviti opciju da template moze da dovuce sistemsku kompaniju
    public function getcompany_info()
    {
        return Yii::app()->company;
    }
    
    //MilosS(7.7.2017): nisam nasao gde se koristi, a i ako se koristi, ne treba
    //MIlosS(7.7.2017): milsim da se koristi u template->u treba napraviti opciju da template moze da dovuce sistemsku kompaniju
    public function getcompany_vat_info_for_template()
    {
        if (Yii::app()->company->inVat)
            $vatInfo = 'je';
        else
            $vatInfo = 'nije';
        
        return $vatInfo;
    }
    
    public function inMonth($month,$year)
    {
        $next_m = ($month + 1);
        $next_y = $year;
        if (($month) / 12 == 1)
        {
            $next_m = 1;
            $next_y = $year + 1;
        }
        
        $start = "$year-$month-01";
        $end = "$next_y-$next_m-01";
        
        $criteria = new SIMADbCriteria([
            'Model' => 'Bill',
            'model_filter'=>[
                'AND',
                ['file'=>[
                    'account_document'=>[
                        'account_order' => [
                            'date' => ['<',$end]
                        ]
                    ]
                ]],
                ['file'=>[
                    'account_document'=>[
                        'account_order' => [
                            'date' => ['>=',$start],
                        ]
                    ]
                ]],
            ]
        ]);
        $this->getDbCriteria()->mergeWith($criteria);
        return $this;
    }
    
    public function VATInMonth($month,$year)
    {
        $next_m = ($month + 1);
        $next_y = $year;
        if (($month) / 12 == 1)
        {
            $next_m = 1;
            $next_y = $year + 1;
        }
        
        $start = "$year-$month-01";
        $end = "$next_y-$next_m-01";
        $_month = Month::get($month, $year);
        
        $alias = $this->getTableAlias();
        $criteria = new SIMADbCriteria([
            'alias' => $alias,
            'Model' => 'Bill',
            'model_filter'=>[
                'OR',
                [//eksplicitno stavljeno
                    'vat_in_diferent_month' => ['ids' => $_month->id]
                ],
                [//nije stavljeno, ali proknjizeno u tom mesecu
                    'AND',
                    ['NOT',['vat_in_diferent_month' => []]],
                    ['file'=>[
                        'account_document'=>[
                            'account_order' => [
                                'date' => ['<',$end]
                            ]
                        ]
                    ]],
                    ['file'=>[
                        'account_document'=>[
                            'account_order' => [
                                'date' => ['>=',$start],
                            ]
                        ]
                    ]]
                ],
                [//nije stavljeno, ali nije ni proknjizen, pa se gleda income
                    'AND',
                    ['NOT',['vat_in_diferent_month' => []]],
                    ['NOT',['file'=>['account_document'=>[]]]],
                    [
                        'AND',
                        ['income_date' => ['<',$end]],
                        ['income_date' => ['>=',$start]],
                    ]
                ]
            ]
        ]);
        $this->getDbCriteria()->mergeWith($criteria);
        return $this;
    }
    
    /**
     * naredne 3 funkcije s
     * @param unknown_type $payment
     */
    public function getPaymentReleasedAmount($payment)
    {
        if (!isset($this->id))
            return 0;
        $billReleases = BillRelease::model()->findAllByAttributes(array('bill_id' => $this->id, 'payment_id' => $payment));
        $sum = 0;
        foreach ($billReleases as $billRelease)
        {
            $sum+=$billRelease->amount;
        }
        return $sum;
    }

    public function getAdvanceBillReleasedAmount($advance_bill, $column = 'amount')
    {
        if (!isset($this->id))
            return 0;
        $billReleases = AdvanceBillRelease::model()->findAllByAttributes(array('bill_id' => $this->id, 'advance_bill_id' => $advance_bill));
        $sum = 0;
        foreach ($billReleases as $billRelease)
        {
            $sum+=$billRelease->$column;
        }
        return $sum;
    }

    public function getReliefBillReleasedAmount($relief_bill, $column = 'amount')
    {
        if (!isset($this->id))
            return 0;
        $billReliefs = BillRelief::model()->findAllByAttributes(array('bill_id' => $this->id, 'relief_bill_id' => $relief_bill));
        $sum = 0;
        foreach ($billReliefs as $billRelief)
        {
            $sum+=$billRelief->$column;
        }
        return $sum;
    }

    /**
     * sluze kao prijemne promenljive za filtere
     */
    public $payment_id;   //svi racuni koji su povezani za zadato placanje
    public $like_payment_id;  //svi racuni koji mogu biti primenjeni za zadato placanje
    public $advance_bill_id;  //svi racuni koji su povezani sa zadatim avansnim racunom
    public $like_advance_bill_id;
    public $relief_bill_id;  //svi racuni koji su povezani sa zadatim knjiznim odobrenjem
    public $like_relief_bill_id;
    public $bill_id;   //svi avansni racuni ili knjizna odobrenja koja su povezana za zadatim racunom (koristi se iz ta dva, ne odavde)
    public $like_bill_id;
    public $selected = false; //da li je trenutni model selektovan.

    public function filter_like_payment_id($condition)
    {
        if (isset($this->like_payment_id) && $this->like_payment_id != null)
        {
            $payment = Payment::model()->findByPk($this->like_payment_id);
            if ($payment != null)
            {
                $advance_payment_type = Yii::app()->configManager->get('accounting.advance_payment_type');
                if ($advance_payment_type == $payment->payment_type_id)
                {
                    $temp_cond = new SIMADbCriteria([
                        'Model' => 'Bill',
                        'model_filter' => [
                            'OR',
                            [
                                'partner' => ['ids' => $payment->partner_id],
                                'invoice' => $payment->invoice,
                                'bill_type' => self::$ADVANCE_BILL
                            ],
                            [
                                'partner' => ['ids' => $payment->partner_id],
                                'invoice' => $payment->invoice,
                                'bill_type' => self::$BILL
                            ]
                            
                        ]
                    ]);
                }
                else
                {
                    $temp_cond = new SIMADbCriteria([
                        'Model' => 'Bill',
                        'model_filter' => [
                            'partner' => ['ids' => $payment->partner_id],
                            'invoice' => $payment->invoice,
                            'bill_type' => self::$BILL
                        ]
                    ]);
                }
                
                $condition->mergeWith($temp_cond);
            }
        }
    }

    public function filter_payment_id($condition)
    {
        if ($this->payment_id != null)
        {
            $temp_cond = new CDbCriteria();
            $bill_releases_table = BillRelease::model()->tableName();
            $alias = $this->getTableAlias();
            $param_id = SIMAHtml::uniqid();
            $temp_cond->condition = "$alias.id in (select bill_id from $bill_releases_table where payment_id=:payment_id${param_id})";
            $temp_cond->params = array(":payment_id${param_id}" => $this->payment_id);
            $condition->mergeWith($temp_cond);
        }
    }

    public function filter_advance_bill_id($condition)
    {
        if ($this->advance_bill_id != null)
        {
            $advance_bill_releases_table = AdvanceBillRelease::model()->tableName();
            $alias = $this->getTableAlias();
            $temp_cond = new CDbCriteria();
            $param_id = SIMAHtml::uniqid();
            $temp_cond->condition = "$alias.id in (select bill_id from $advance_bill_releases_table where advance_bill_id=:advance_bill_id${param_id})";
            $temp_cond->params = array(":advance_bill_id${param_id}" => $this->advance_bill_id);
            $condition->mergeWith($temp_cond);
        }
    }

    public function filter_relief_bill_id($condition)
    {

        if ($this->relief_bill_id != null)
        {
            $bill_relief_table = BillRelief::model()->tableName();
            $alias = $this->getTableAlias();
            $temp_cond = new CDbCriteria();
            $param_id = SIMAHtml::uniqid();
            $temp_cond->condition = "$alias.id in (select bill_id from $bill_relief_table where relief_bill_id=:relief_bill_id${param_id})";
            $temp_cond->params = array(":relief_bill_id${param_id}" => $this->relief_bill_id);
            $condition->mergeWith($temp_cond);
        }
    }

    public function filter_like_advance_bill_id($condition)
    {
        if (isset($this->like_advance_bill_id) && $this->like_advance_bill_id != null)
        {
            $bill = Bill::model()->findByPk($this->like_advance_bill_id);
            if ($bill != null)
            {
                $temp_cond = new SIMADbCriteria([
                    'Model' => 'Bill',
                    'model_filter' => [
                        'partner' => ['ids' => $bill->partner_id],
                        'invoice' => $bill->invoice,
                        'bill_type' => 'BILL'
                    ]
                ]);
                $condition->mergeWith($temp_cond);
            }
        }
    }

    public function filter_like_relief_bill_id($condition)
    {
        if (isset($this->like_relief_bill_id) && $this->like_relief_bill_id != null)
        {
            $bill = Bill::model()->findByPk($this->like_relief_bill_id);
            if ($bill != null)
            {
                $temp_cond = new SIMADbCriteria([
                    'Model' => 'Bill',
                    'model_filter' => [
                        'partner' => ['ids' => $bill->partner_id],
                        'invoice' => $bill->invoice,
                        'bill_type' => 'BILL'
                    ]
                ]);
                $condition->mergeWith($temp_cond);
            }
        }
    }

    public function filter_like_bill_id($condition)
    {
        if ($this->like_bill_id != null)
        {
            $bill = Bill::model()->findByPk($this->like_bill_id);
            if ($bill != null)
            {
                $temp_cond = new SIMADbCriteria([
                    'Model' => 'Bill',
                    'model_filter' => [
                        'partner' => ['ids' => $bill->partner_id],
                        'invoice' => $bill->invoice
                    ]
                ]);
                $condition->mergeWith($temp_cond);
            }
        }
    }

    public function filter_belongs($condition)
    {
        if ($this->belongs != null)
        {
            $query = $this->belongs;
            if ($this->belongs_recursive === 'true')
                $query.='$R';
            $condition->mergeWith(SIMAFileStorage::applyQuery($query));
        }
    }

    /**
     * TODO:
     * @see SIMAActiveRecord::droplist()
     */
    public function droplist($key)
    {
        switch ($key)
        {
            case 'bill_type': return array(
                    'BILL' => 'Račun',
                    'ADVANCE_BILL' => 'Avansni račun',
                    'RELIFE_BILL' => 'Knjižno odobrenje',
                    'UNRELIFE_BILL' => 'Knjižno zaduzenje',
                    'PRE_BILL' => 'Predracun'
                );
            case 'belongs': return FileTag::model()->getModelDropList();
            default: return parent::droplist($key);
        }
    }

    public function scopes()
    {
        $alias = $this->getTableAlias(false, false);
        $uniq = SIMAHtml::uniqid();
        $_pre_bill = Bill::$PRE_BILL;
        return array(
            'recently' => array(
                'order' => "${alias}.income_date DESC, ${alias}.id DESC",
            ),
            'byOfficial' => array(
                'order' => "${alias}.income_date, ${alias}.id",
            ),
            'unreleased' => [
                'condition' => "$alias.unreleased_amount != 0"
            ],
            'notRecurring' => [
                'condition' => "$alias.recurring_bill_id is null"
            ],
            'withNoSignature' => [
                'join' => "inner join accounting.years ay$uniq on $alias.year_id = ay$uniq.id",
                'condition' => "ay$uniq.confirmed = false "
                        . "and not exists (select 1 from files.file_signatures where file_id = $alias.id)"
            ],
            'notBooked' => [
                'condition' => 
                        "       (not exists (select 1 from accounting.account_documents where id = $alias.id) "
                        . "         OR "
                        . "      not exists (select 1 from accounting.account_transactions where account_document_id = $alias.id))"
                        . " AND "
                        . " $alias.bill_type != '$_pre_bill'"
            ],
            'notCanceled' => [
                'condition' => "$alias.canceled = false"
            ],
            'withVatInMonth' => [
                'select' => "(select month_id          from accounting.bill_vat_month_id where id = $alias.id) as vat_in_month_id, "
                           ."(select canceled_month_id from accounting.bill_vat_month_id where id = $alias.id) as canceled_in_month_id",
            ]
        );
    }
    
    /**
     * select year_id, EXTRACT(YEAR FROM income_date), * 
from files.files ff 
	inner join private.bills pb on ff.id = pb.id
	inner join accounting.years ay on pb.year_id = ay.id
where 	ay.confirmed = false
	and not exists (select 1 from files.file_signatures where file_id = ff.id)
	and invoice = false
order by create_time desc
     * @param type $column
     * @return type
     */

    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters': return array_merge(parent::modelSettings($column), [
                'file' => 'relation',
                'bill_number' => 'text',
                'partner' => 'relation',
                'income_date' => 'date_range',
                'payment_date' => 'date_range',
                'responsible_id' => 'dropdown',
                'bill_type' => 'dropdown',
//                'belongs'=>array('file_tag','func'=>'filter_belongs'),
                'belongs' => array('belongs', 'func' => 'filter_belongs'),
                'payment_id' => array('func' => 'filter_payment_id'),
                'like_payment_id' => array('func' => 'filter_like_payment_id'),
                'advance_bill_id' => array('func' => 'filter_advance_bill_id'),
                'like_advance_bill_id' => array('func' => 'filter_like_advance_bill_id'),
                'relief_bill_id' => array('func' => 'filter_relief_bill_id'),
                'like_relief_bill_id' => array('func' => 'filter_like_relief_bill_id'),
                //      'bill_id'=>array('func'=>'filter_bill_id'),
                'like_bill_id' => array('func' => 'filter_like_bill_id'),
                'invoice' => 'boolean',
                'year' => 'relation',
                'connected' => 'boolean',
                'cost_type' => ['relation','func' => 'filterCostLocationTypeWithItems'],
                'local_cost_type' => ['relation'],
                'cost_location' => ['relation','func' => 'filterCostLocationTypeWithItems'],
                'local_cost_location' => ['relation'],
                'job_order' => 'relation',
                'amount' => 'numeric',
                'base_amount' => 'numeric',
                'vat' => 'numeric',
                'unreleased_amount' => 'numeric',
                'partner_bank_account'=>'relation',
                'comment' => 'text',
                'recurring_bill' => 'relation',
                'abbill' => 'relation',
                'call_for_number' => ['func'=>'filterCallForNumber'],
                'bill_items' => 'relation',
                'vat_in_diferent_month' => 'relation',
                'invoicing_basis' => 'text',
                'invoice_comment' => 'text',
                'traffic_location' => 'relation',
                'issue_location' => 'relation',
                'note_of_tax_exemption' => 'relation'
            ]);
            case 'textSearch': return [
                'file' => 'relation',
                'bill_number' => 'text',
                'partner' => 'relation',
            ];
            case 'order_by' : return explode(',', 'amount,bill_number,bill_date,payment_date,income_date,confirm1,confirm2,bill_number');
            case 'options': 
                if (get_class($this) === 'Bill')
                {
                    return ['open','form_custom', 'download', 'orderScan','cancel'];
                }
                else
                {
                    return ['open','form',        'download', 'orderScan','cancel'];
                }
            case 'multiselect_options' : return array(
                    'orderScan', 'add_bills_to_account_order'
                );
            case 'statuses' : return array(
                    'confirm1' => array(
                        'title' => 'Potvrda 1',
                        'timestamp' => 'confirm1_timestamp',
                        'user' => array('confirm1_user_id', 'relName' => 'confirm1_user'),
                        'checkAccessConfirm' => 'checkConfirm1Access',
                        'checkAccessRevert' => 'checkConfirm1RevertAccess',
                        'onConfirm' => 'onConfirm1Function',
                        'onRevert' => 'onRevert1Function'
                    ),
                    'confirm2' => array(
                        'title' => 'Potvrda 2',
                        'timestamp' => 'confirm2_timestamp',
                        'user' => array('confirm2_user_id', 'relName' => 'confirm2_user'),
                        'checkAccessConfirm' => 'checkConfirm2Access',
                        'checkAccessRevert' => 'checkConfirm2RevertAccess',
                        'onConfirm' => 'onConfirm2Function',
                        'onRevert' => 'onRevert2Function'
                    ),
                );
            case 'GuiTable' : return array(
                    'scopes' => array('recently')
                );
            case 'mutual_forms' : return array(
                    'base_relation' => 'file',
                    'relations'=>['energy_bill', 'abbill']
                );
            case 'number_fields': return [
                    'amount', 'base_amount', 'base_amount0', 'base_amount10', 'base_amount20', 'vat10', 'vat20', 'vat',
                    'unreleased_amount',
                    'rounding', 'amount_in_currency', 'unreleased_amount_in_currency',
                    'rounding', 'interest',
                    'value_customs'
                ];
            case 'update_relations': return [
                'bill_items'
            ];
            default: return parent::modelSettings($column);
        }
    }
    
    public function filterCallForNumber($condition, $alias, $column)
    {
        if (!empty($this->call_for_number)) 
        {
            $param_id = SIMAHtml::uniqid();
            $temp_cond = new SIMADbCriteria();
            $temp_cond->condition = "replace($alias.$column,'-','') = replace(:call_for_number$param_id,'-','')";
            $temp_cond->params = [":call_for_number$param_id" => $this->$column];
            $condition->mergeWith($temp_cond);
        }
    }
    
    /**
     * Filter koji hvata sve racune koji imasju odredjenu vrstu/mesto troska
     * @param SIMADbCriteria $condition
     * @param string $alias
     * @param string $column
     */
    public function filterCostLocationTypeWithItems(SIMADbCriteria $condition, string $alias, string $column)
    {
        $ids = $this->$column['ids'];
        if (!empty($ids)) 
        {
            $temp_cond = new SIMADbCriteria([
                'alias' => $alias,
                'Model' => 'Bill',
                'model_filter' => [
                    'OR',
                    //stavka ima tu vrstu/mesto troska
                    ['bill_items' => [
                            $column => ['ids' => $ids]
                    ]],
                    [//stavka nema vrstu/mesto troska, ali je zato setovano u local polju racuna
                        'AND',
                        [
                            "local_$column" => ['ids' => $ids],
                        ],
                        [
                            'NOT',
                            [
                                'bill_items' => [
                                    $column => []
                                ]
                            ]
                        ]
                    ]
                ]
            ]);
            $condition->mergeWith($temp_cond);
        }
    }

    public function checkConfirm1Access()
    {
        $errors = [];

        if (
                !$this->invoice && 
                Yii::app()->configManager->get('accounting.bill_in_automatic_confirmation1', false)
            )
        {
            $errors[] = Yii::t('AccountingModule.Bill', 'ForBillInConfirmation1IsAutomatic');
        }
        else
        {
            if(empty($this->file))
            {
                $errors[] = 'Nemate privilegije da pristupate ovom dokumentu';
            }
            else if (!Yii::app()->user->checkAccess('modelBillConfirm1'))
            {
                $errors[] = 'Nemate privilegiju';
            }
        }
        
        return (count($errors) > 0) ? $errors : true;
    }
    
    public function checkConfirm1RevertAccess()
    {
        $errors = [];

        if (
                !$this->invoice && 
                Yii::app()->configManager->get('accounting.bill_in_automatic_confirmation1', false)
            )
        {
            $errors[] = Yii::t('AccountingModule.Bill', 'ForBillInConfirmation1IsAutomatic');
        }
        
        return (count($errors) > 0) ? $errors : true;
    }

    public function onConfirm1Function()
    {
//        if (isset($this->file->filing) 
//                && isset($this->file->filing->registry)
//                && $this->file->filing->registry->isFinance())
//        {
//            $this->file->filing->original = true;
//            $this->file->filing->save();
//        }
    }
    
    public function onRevert1Function()
    {
        if ($this->confirm2)
        {
            $this->confirm2 = false;
            $this->confirm2_user_id = $this->confirm1_user_id;
            $this->save();
        }
    }

    public function checkConfirm2Access()
    {
        $msgs = [];
        
        if (
                !$this->invoice && 
                Yii::app()->configManager->get('accounting.bill_in_automatic_confirmation2', false)
            )
        {
            $msgs[] = Yii::t('AccountingModule.Bill', 'ForBillInConfirmation2IsAutomatic');
        }
        else
        {
            if (!Yii::app()->user->checkAccess('modelBillConfirm2'))
            {
                $msgs[] = 'Nemate privilegiju za potvrdjivanje';
            }
        }
        
        return (count($msgs)>0)?$msgs:true;
    }
    
    public function checkConfirm2RevertAccess()
    {
        $errors = [];

        if (
                !$this->invoice && 
                Yii::app()->configManager->get('accounting.bill_in_automatic_confirmation2', false)
            )
        {
            $errors[] = Yii::t('AccountingModule.Bill', 'ForBillInConfirmation2IsAutomatic');
        }
        
        return (count($errors) > 0) ? $errors : true;
    }

    public function onConfirm2Function()
    {
        if ($this->file->responsible_id == Yii::app()->user->id)
        {
            $this->file->confirmed = true;
            $this->file->save(false, null, false);
        }
    }
    
    public function onRevert2Function()
    {
        if (isset($this->file->account_document->account_order) && $this->file->account_document->account_order->booked === true)
        {
            if  (
                    !$this->invoice &&
                    Yii::app()->configManager->get('accounting.bill_in_automatic_confirmation2', false)
                )
            {
                $this->file->account_document->account_order->booked = false;
                $this->file->account_document->account_order->booker_id = $this->confirm2_user_id;
                $this->file->account_document->account_order->save();
            }
            else
            {
                throw new SIMAWarnException(Yii::t('AccountingModule.Bill', 'YouUnconfirmed2BillButAccountOrderIsStillConfirmed'));
            }
        }
    }

    public function getClasses()
    {
        return parent::getClasses() .
                (($this->selected) ? ' selected ' : '') .
                ((!$this->isReleased) ? ' not_released ' : '');
    }
    
    public function getVATClasses()
    {
        $classes = ' ';
        switch ($this->bill_type)
        {
            case Bill::$ADVANCE_BILL: $classes = ' VAT_advance_bill '; break;
            case Bill::$RELIEF_BILL: $classes = ' VAT_relief_bill '; break;
            case Bill::$UNRELIEF_BILL: $classes = ' VAT_relief_bill '; break;
        }
        if ($this->bill_items_internal_vat_count > 0)
        {
            $classes .= ' VAT_internal ';
        }
        if ($this->bill_items_non_internal_vat_count > 0)
        {
            $classes .= ' VAT_non_internal ';
        }
        return parent::getClasses().$classes;
    }

    public function getAdditionalInfo()
    {
        return " bill_id='$this->id' partner_id='$this->partner_id' " .
                (($this->like_payment_id != null) ? 'data="' . get_class($this) . '[like_payment_id]=' . $this->like_payment_id . '"' : '') .
                (($this->like_advance_bill_id != null) ? 'data="' . get_class($this) . '[like_advance_bill_id]=' . $this->like_advance_bill_id . '"' : '') .
                (($this->like_relief_bill_id != null) ? 'data="' . get_class($this) . '[like_relief_bill_id]=' . $this->like_relief_bill_id . '"' : '') .
                parent::getAdditionalInfo();
    }

    public function beforeSave()
    {
        /**
         * dodavanje broja racuna
         */
        if (empty($this->bill_number))
        {
            $user_config_counter_id = Yii::app()->configManager->get($this->counterConfigCode(), false);
            if (!is_null($user_config_counter_id))
            {
                $day = Day::getByDate($this->income_date);
                $counter = UserConfigCounter::model()->findByPk($user_config_counter_id);
                $this->bill_number = $counter->getNextText($day);
            }
        }
        
        //ako je traffic date prazan, popunjava se sa income_date
        if (empty($this->traffic_date))
        {
            $this->traffic_date = $this->income_date;
        }
        
        //ako je traffic location prazan, popunjava se iz konfiguracije
        if (empty($this->traffic_location_id))
        {
            $this->traffic_location_id = Yii::app()->configManager->get('accounting.templates.bill_templates.traffic_location_id', false);
        }
        
        //ako je issue location prazan, popunjava se iz konfiguracije
        if (empty($this->issue_location_id))
        {
            $this->issue_location_id = Yii::app()->configManager->get('accounting.templates.bill_templates.issue_location_id', false);
        }
        
        if (empty($this->id)) //kada se iz koda dodaje racun
        {
            $document_type_id = '';
            $document_type_bill = Yii::app()->configManager->get('accounting.bills.bill', false);
            if (count($document_type_bill)>0)
            {
                $document_type_id = $document_type_bill[0];
            }
            $this->file = new File();
            $this->file->name = $this->bill_number;
            $this->file->document_type_id = $document_type_id;
            if (Yii::app()->isWebApplication())
            {
                $this->file->responsible_id = Yii::app()->user->id;
            }
            $this->file->save();
            
            $this->id = $this->file->id;
        }
        
        if($this->bound_currency_id === Yii::app()->configManager->get('base.country_currency'))
        {
            $this->bound_currency_id = null;
        }
        
        //ako je promenjen recurring ili vrednost, a reccuring nije empty
        //podrazumeva se da se recurring_bill_id nije menjan, jer da jeste, true ce biti prvi uslov
        if (
                $this->columnChanged('recurring_bill_id') 
                || 
                ($this->columnChanged('amount') && !empty($this->recurring_bill_id))
            )
        {
            $this->dettachAllReleases(); 
        }
        
        //popunjavanje mesta troska na osnovu pripadnosti
        if (empty($this->cost_location_id) && !$this->cost_location_mixed)
        {
            $cost_locations = $this->getCostLocationsFromFileTags();
            if (count($cost_locations)===1)
            {
                $this->cost_location_id = $cost_locations[0]->id;
            }
            elseif(count($cost_locations)>1)
            {
                Yii::app()->raiseNote('Morate u stavkama racuna samostalno popuniti mesta troska, nema automatska podrska');
            }
            else
            {
                $this->cost_location_id = Yii::app()->configManager->get('accounting.default_cost_location_id', false);
            }
        }
        
        
        if (empty($this->cost_type_id))
        {
            $this->cost_type_id = Yii::app()->configManager->get(
                    $this->invoice?
                        'accounting.default_income_cost_type_id'
                        :
                        'accounting.default_cost_cost_type_id'
                    , false);
        }
        
        

        return parent::beforeSave();
    }
    
    public function save($runValidation = true, $attributes = null, $throwExceptionOnFail = true)
    {
        //MilosS(12.11.2018): ovo bi trebalo ubaciti u validaciju i onda ovde da ne bude warn nego exception, ili nista posto exception i onako ide
            //ali je validaciju zezanje napraviti jer moze da se promeni i jedinicna cena i ukupna vrednost
        try
        {
            return parent::save($runValidation, $attributes, $throwExceptionOnFail);
        }
//        catch (SIMADbExceptionS0007UnreleasedAmountUnderZero $e)
//        {
//            throw new SIMAWarnUnreleasedAmountUnderZero($this->bill);
//        }
        catch (CdbException $e)
        {
            if (substr_count($e->getMessage(), 'SQLSTATE[S0009]') > 0)
            {
                throw new SIMAWarnBillItemNegative($this, $e->getMessage(), $this);
            }
            else
            {
                throw $e;
            }
        }
    }
    
    private function setChangedValues()
    {
        $bill_items = BillItem::model()->findAll([
            'model_filter' => [
                'bill' => ['ids' => $this->id]
            ]
        ]);
        
        $items_changed = false;
        if (is_bool($this->calcVatRefusal()) && $this->columnChanged('vat_refusal'))
        {
            $items_changed = true;
            foreach ($bill_items as $item)
            {
                $item->vat_refusal = $this->vat_refusal;
            }
        }
        
        if ($this->columnChanged('cost_location_id'))
        {
            $items_changed = true;
            foreach ($bill_items as $item)
            {
                $item->cost_location_id = $this->cost_location_id;
            }
        }
        
        if ($this->columnChanged('cost_type_id'))
        {
            $items_changed = true;
            foreach ($bill_items as $item)
            {
                $item->cost_type_id = $this->cost_type_id;
            }
        }
        if ($items_changed)
        {
            foreach ($bill_items as $item)
            {
                $item->save();
            }
        }
        
    }
    
    protected function getCostLocationsFromFileTags()
    {
        $ftfts = FiletoFileTag::model()->findAll([
            'model_filter' => [
                'OR',
                [
                    'file' => ['ids' => $this->id],
                    'file_tag' => [
                        'model_table' => Theme::tableName()
                    ]
                ],
                [
                    'file' => ['ids' => $this->id],
                    'file_tag' => [
                        'model_table' => FixedAsset::tableName()
                    ]
                ]    
            ]
        ]);
        $cost_locations = [];
        foreach ($ftfts as $ftft)
        {
            $cost_locations[] = $ftft->file_tag->TagModel->cost_location;
        }
        return $cost_locations;
    }
    
    private function distrubuteCostLocationsToItems()
    {
        //TODO: za sada nije implementirano
//        $cost_locations = $this->getCostLocationsFromFileTags();
//        
//        $no_for = [];
//        foreach ($cost_locations as $cost_location)
//        {
//            $items_cnt = BillItem::model()->count([
//                'model_filter' => [
//                    'cost_location' => ['ids' => $cost_location->id],
//                    'bill' => ['ids' => $this->id]
//                ]
//            ]);
//            
//            if ($items_cnt === 0)
//            {
//                $no_for[] = $cost_location->DisplayNameFull;
//            }
//        }
//        if (!empty($no_for))
//        {
//            Yii::app()->raiseNote('Nema stavke u racunu za mesto troska:<br /><br />'
//                    .implode('<br />', $no_for));
//        }
    }
    private function checkCostLocationsToItems()
    {
        $cost_locations = $this->getCostLocationsFromFileTags();
        
        $no_for = [];
        foreach ($cost_locations as $cost_location)
        {
            $items_cnt = BillItem::model()->count([
                'model_filter' => [
                    'cost_location' => ['ids' => $cost_location->id],
                    'bill' => ['ids' => $this->id]
                ]
            ]);
            
            if ($items_cnt === 0)
            {
                $no_for[] = $cost_location->DisplayNameFull;
            }
        }
        if (!empty($no_for))
        {
            Yii::app()->raiseNote('Nema stavke u racunu za mesto troska:<br /><br />'
                    .implode('<br />', $no_for));
        }
    }
    
   
    public function afterSave()
    {
        parent::afterSave();
        
        if (
                ($this->columnChanged('recurring_bill_id') || $this->columnChanged('amount') || $this->isNewRecord)
                && !empty($this->recurring_bill_id)
            )
        {
            $this->attachRecurringReleases();
        }
        
        $this->distrubuteCostLocationsToItems();
        $this->setChangedValues();
        $this->checkCostLocationsToItems();
        
        if (!$this->isNewRecord && $this->columnChanged(['amount','base_amount0','base_amount10','base_amount20','vat10','vat20']))
        {
            Yii::app()->raiseNote(Yii::t('AccountingModule.Bill','BillValuesChangedNote'));
        }
        
        unset($this->bill_items);
        
        SystemEvent::add(
            'Bill', 
            'addCostControllersToFileSignatures', 
            date("d.m.Y. H:i:s"),
            [
                'file_id' => $this->id,
                'curr_user_id' => Yii::app()->user->id
            ],
            Yii::t('BaseModule.PersonToTheme', 'CostController'),
            false
        );
        
        if ($this->isNewRecord)
        {
            $this->automaticallyAddUsersToBillInSignature();
            
            if (isset($this->file->bill_in) && !isset($this->file->filing) && !Yii::app()->configManager->get('accounting.add_bill_in_without_filing_book', false))
            {
                Yii::app()->errorReport->createAutoClientBafRequest("Dodat je ulazni racun sa ID:{$this->id} koji nije zaveden, a zabranjeno je dodavanje ulaznih racuna van zavodne knjige(postoji konfiguracija).");
            }
        }
        else if($this->canceled && $this->columnChanged('canceled'))
        {
            foreach ($this->file->signatures as $signature)
            {
                $signature->sendNotificationWhenCanceledBill();
            }
        }
    }
    
    /**
     * Automatski dodaje korisnike koji su postavljeni u konfiguraciji na potpis ulaznog racuna
     */
    private function automaticallyAddUsersToBillInSignature()
    {
        if (!$this->invoice)
        {
            $bill_in_automatic_file_signature_users = Yii::app()->configManager->get('accounting.bill_in_automatic_file_signature_users', false);
            if (!empty($bill_in_automatic_file_signature_users))
            {
                foreach ($bill_in_automatic_file_signature_users as $user_id) 
                {
                    $file_signature = FileSignature::model()->findByAttributes([
                        'file_id' => $this->id,
                        'user_id' => $user_id
                    ]);
                    if (empty($file_signature))
                    {
                        $file_signature = new FileSignature();
                        $file_signature->file_id = $this->id;
                        $file_signature->user_id = $user_id;
                        $file_signature->save();
                    }
                }
            }
        }
    }
    
    public static function addCostControllersToFileSignatures($input_params)
    {
        $file_id = $input_params['file_id'];
        $curr_user_id = $input_params['curr_user_id'];
        
        $file_tags = FileTag::model()->findAll([
            'model_filter' => [
                'file_tag_to_files' => [
                    'file' => [
                        'ids' => $file_id
                    ]
                ],
                'model_table' => Theme::model()->tableName()
            ]
        ]);

        foreach ($file_tags as $file_tag)
        {
            $cost_controllers_for_theme = PersonToTheme::model()->findAll([
                'model_filter' => [
                    'theme' => [
                        'ids' => $file_tag->model_id,
                    ],
                    'is_cost_controller' => true,
                    'person' => [
                        'user' => []
                    ]
                ]
            ]);
            
            foreach ($cost_controllers_for_theme as $cost_controller_for_theme)
            {
                $file_signature = FileSignature::model()->findByAttributes(['file_id' => $file_id, 'user_id' => $cost_controller_for_theme->person_id]);
                if (empty($file_signature))
                {
                    $file_signature = new FileSignature();
                    $file_signature->file_id = $file_id;
                    $file_signature->user_id = $cost_controller_for_theme->person_id;
                    $file_signature->requested_by_id = $curr_user_id;
                    $file_signature->save();
                }
            }
        }
    }
    
    //prisilno rekonektovanje placanja za recurring racune
    public function reconnectRecurringBill()
    {
        if (empty($this->recurring_bill_id))
        {
            throw new SIMAExceptionBillNotRecurring($this);
        }
        $this->dettachAllReleases();
        $this->attachRecurringReleases();
    }
    
    /**
     * Funkcija koja skida veze ka svim Payments
     */
    private function dettachAllReleases()
    {
        foreach ($this->releases as $_release)
        {
            $_release->delete();
        }
        unset($this->releases);
        $_old_recurring_bill = RecurringBill::model()->findByPk($this->__old['recurring_bill_id']);
        if (!is_null($_old_recurring_bill))
        {
            $_nb = $_old_recurring_bill->nextBill($this);
            if (!is_null($_nb))
            {
                $_nb->dettachAllReleases();
            }
        }
    }
    
    /**
     * Funkcija koja povezuje sve Payments koji imaju isti recurring_bill_id
     */
    private function attachRecurringReleases()
    {
        if (empty($this->recurring_bill_id))
        {
            throw new SIMAExceptionBillNotRecurring($this);
        }
        $reccuring_payments = Payment::model()->findAll(new SIMADbCriteria([
            'Model' => Payment::model(),
            'model_filter' => [
                'recurring_bill' => ['ids' => $this->recurring_bill_id],
                'filter_scopes' => ['unreleased'],
                'display_scopes' => ['byOfficial'],
            ]
        ]));

        foreach ($reccuring_payments as $reccuring_payment)
        {
            ConnectAccountingDocuments::BillToPayment($this, $reccuring_payment);
            $check_value = Bill::model()->findByPk($this->id)->unreleased_amount;
            if ($check_value <= 0 )
            {
                break;
            }
        }
        unset($this->releases);
        $_nb = $this->recurring_bill->nextBill($this);
        if (!is_null($_nb))
        {
            $_nb->attachRecurringReleases();
        }
    }
    
    public function unreleased_percent_of_amount($amount)
    {        
        $alias = $this->getTableAlias();
        $this->getDbCriteria()->mergeWith(array(
            'select'=>"$alias.*,(CASE WHEN :amount < 0.001 THEN :amount ELSE ($alias.unreleased_amount * 100/:amount) END) as unreleased_percent_of_amount",
            'params'=> array(':amount' => $amount),
        ));
        return $this;
    }

    public function applyValueFromItems()
    {        
        $this->base_amount0 = 0.00;
        $this->base_amount10 = 0.00;
        $this->base_amount20 = 0.00;
        $this->vat10 = 0.00;
        $this->vat20 = 0.00;
                
        foreach ($this->bill_items as $bill_item)
        {
            if ($bill_item->vat === '0')
            {
                $this->base_amount0 += $bill_item->value;
            } 
            else if ($bill_item->vat === '10')
            {
                $this->base_amount10 += $bill_item->value;
                $this->vat10 += $bill_item->value_vat;
            }
            else if ($bill_item->vat === '20')
            {
                $this->base_amount20 += $bill_item->value;
                $this->vat20 += $bill_item->value_vat;
            }            
        }
        $this->amount = 
                $this->base_amount0 + 
                $this->base_amount10 + 
                $this->base_amount20 + 
                $this->vat10 + 
                $this->vat20;
        $this->save();
    }
    
    public function setItemValuesFromWarehouse()
    {
        foreach ($this->bill_items as $key => $item)
        {
            $_value_per_unit = NULL;
            if ($item->isWarehouseBillItem)
            {
                foreach ($this->warehouse_transfers as $transfer)
                {
                    foreach ($transfer->warehouse_transfer_items as $wt_item)
                    {
                        if ($wt_item->warehouse_material_id == $item->warehouse_material_id)
                        {
                            if (is_null($_value_per_unit))
                            {
                                $_value_per_unit = $wt_item->value_per_unit;
                            }
                            else if($_value_per_unit != $wt_item->value_per_unit)
                            {
                                Yii::app()->raiseNote('Stavka '.$item->warehouse_material->DisplayName.' ima razlicite cene');
                            }
                        }
                    }
                }
                $item->value_per_unit = $_value_per_unit;
                $item->save();
            }
        }
    }
    
    public function BoundCurrencyValue($value)
    {
        $result = null;
        
        $bound_currency = $this->bound_currency;
        $country_currency = Yii::app()->configManager->get('base.country_currency');
        
        if(empty($bound_currency) || empty($country_currency))
        {
            $result = '-';
        }
        else
        {
            if(!$this->isSetMiddleCurrencyRateForBoundCurrency())
            {
                $result = '-';
            }
            else
            {
                $date = $this->income_date;
                $middleCurrencyRateCriteria = new SIMADbCriteria([
                    'Model' => 'MiddleCurrencyRate',
                    'model_filter' => [
                        'currency_id' => $bound_currency->id,
                        'date' => $date
                    ]
                ]);
                $middleCurrencyRate = MiddleCurrencyRate::model()->find($middleCurrencyRateCriteria);
                
                $result = $value/$middleCurrencyRate->value;
            }
        }
        
        return $result;
    }
    
    public function isSetMiddleCurrencyRateForBoundCurrency()
    {        
        $bound_currency = $this->bound_currency;
                
        if(empty($bound_currency))
        {
            return false;
        }
                
        $date = $this->income_date;
        
        return !empty(MiddleCurrencyRate::get($bound_currency, Day::getByDate($date)));
    }
    
    public function unlockAmount($save = true)
    {
        $this->lock_amounts = false;
        if ($save)
        {
            $this->save();
        }
    }
    
    public function clearItems()
    {
        $this->interest = 0.00;
        $this->rounding = 0.00;
        $this->save();
        
        foreach ($this->bill_items as $bill_item)
        {
            $bill_item->delete();
        }
        $this->refresh();
    }
    
    public function unlockAmountAndClearItems()
    {
        $this->unlockAmount(false);
        $this->clearItems();
    }
    
    /**
     * izracunava zateznu kamatu na dan po "ZAKON O ZATEZNOJ KAMATI" ("Sl. glasnik RS", br. 119/2012)
     * @param Day $day
     * @return real
     */
    public function calcInterest(Day $day = null)
    {
        if (is_null($day))
        {
            $day = Day::get();
        }
        
//        $_rest_for_calculation = $this->amount;
        $_interest = 0.00;
        $_deadline_day = $this->payment_date;
//        error_log('-----------------------------------------------------------');
//        error_log('bill: '.$this->DisplayName);
//        error_log('$_deadline_day: '.$_deadline_day);
        //TODO: obracunati kamatu
        $releases = $this->releases([
//            'scopes' => 
        ]);
        
        foreach ($releases as $bill_release) 
        {
//            
            $_payed_on_date = $bill_release->payment->bank_statement->date;
//            
//            error_log('$_payed_on_date: '.$_payed_on_date);
//            
            $days_count = count(Day::getBetweenDates($_deadline_day, $_payed_on_date));
            
//            error_log('$days_count: '.  SIMAMisc::toTypeAndJsonString($days_count));
//            
//            
//            $_interest += $Cdays_count * 100;
//            $_rest_for_calculation -= $bill_release->amount;
        }
//        if ($this->unreleased_amount != $_rest_for_calculation)
//        {
//            throw new SIMAException('ne poklapa se ostatak i ne rasknjizeno');
//        }
//        else
//        {
//            $days_count = count(Day::getBetweenDates($_deadline_day, $day->day_date));
//            $_interest += $days_count * 100;
//        }
        
        
        return $_interest;
    }
    
    public static function internalVatCodeConvertor($is_internal_vat)
    {
        return $is_internal_vat?'I':'R';
    }
    
    private $_sums_vat_bill = null;
    public function vatItemsArray($force_recalc = false)
    {
        if (is_null($this->_sums_vat_bill) || $force_recalc)
        {
            $this->_sums_vat_bill = [
                'R' => [
                    '0'=>['base' => 0,'vat' => 0,],
                    '10'=>['base' => 0,'vat' => 0,],
                    '20'=>['base' => 0,'vat' => 0,],
                ],
                'I' => [
                    '0'=>['base' => 0,'vat' => 0,],
                    '10'=>['base' => 0,'vat' => 0,],
                    '20'=>['base' => 0,'vat' => 0,],
                ]

            ];
            foreach ($this->bill_items as $bill_item)
            {
                if ($bill_item->is_internal_vat)
                {
                    $this->_sums_vat_bill['I'][$bill_item->internal_vat]['base'] += $bill_item->value;
                    $this->_sums_vat_bill['I'][$bill_item->internal_vat]['vat']  += $bill_item->value_internal_vat;
                }
                else
                {                    
                    $this->_sums_vat_bill['R'][$bill_item->vat]['base'] += $bill_item->value;
                    $this->_sums_vat_bill['R'][$bill_item->vat]['vat']  += $bill_item->value_vat;
                }
            }
        }
        
        return $this->_sums_vat_bill;
    }
    
    public function remainingVatItemsArray()
    {
        $_vats = $this->vatItemsArray(true);
        foreach ($this->advance_releases as $_adv_release)
        {
            $I_R_code = $_adv_release->internalVatCode();
            $_vats[$I_R_code][$_adv_release->vat_rate]['base'] -= $_adv_release->base_amount;
            $_vats[$I_R_code][$_adv_release->vat_rate]['vat']  -= $_adv_release->vat_amount;
        }
        
        foreach ($this->reliefes as $_relief)
        {
            $I_R_code = $_relief->internalVatCode();
            $_vats[$I_R_code][$_relief->vat_rate]['base'] -= $_relief->base_amount;
            $_vats[$I_R_code][$_relief->vat_rate]['vat']  -= $_relief->vat_amount;
        }
        return $_vats;
    }
    
    public function isDomesticCurrency()
    {
        return empty($this->bound_currency_id) || ($this->bound_currency_id === Yii::app()->configManager->get('base.country_currency'));
    }
    
    public function getBillCurrency()
    {
        $currency = null;
        if (empty($this->bound_currency_id))
        {
            $currency_id = Yii::app()->configManager->get('base.country_currency');
            if (!empty($currency_id))
            {
                $currency_model = Currency::model()->findByPkWithCheck($currency_id);
                $currency = $currency_model->DisplayName;
            }
        }
        else
        {
            $currency = $this->bound_currency->DisplayName;
        }
        
        return $currency;
    }
    
    /**
     * Poslednja uplata vezana za ovaj racun
     * @return Payment 
     */
    public function getLastPayment()
    {
        return Payment::model()->findByModelFilter([
            'bill_id' => $this->id,
            'display_scopes' => ['byOfficialDESC']
        ]);
    }
    
    public function getLastOrderNumberOfBillItemsForType($type = null)
    {
        $model_filter = [
            'scopes' => ['byOrderDesc'],
            'bill' => [
                'ids' => $this->id
            ]
        ];

        if(!is_null($type))
        {
            $model_filter['bill_item_type'] = "$type";
        }
        $last_bill_item = BillItem::model()->find([
            'model_filter' => $model_filter
        ]);

        if(!empty($last_bill_item))
        {
            $last_order_number = $last_bill_item->order;
        }
        else if($type === BillItem::$WAREHOUSE)
        {
            $last_order_number = $this->getLastOrderNumberOfBillItemsForType(BillItem::$FIXED_ASSETS);
        }
        else if($type === BillItem::$SERVICES)
        {
            $last_order_number = $this->getLastOrderNumberOfBillItemsForType(BillItem::$WAREHOUSE);
        }
        else 
        {
            $last_order_number = 0;
        }
        
        return $last_order_number;
    }
    
}
