<?php

class BillItem extends SIMAActiveRecord 
{  
    
    public static $WAREHOUSE = 'WAREHOUSE';    
    public static $SERVICES = 'SERVICES';
    public static $FIXED_ASSETS = 'FIXED_ASSETS';
    
    public $order_for_new_item = null; //order koji treba da ima nova stavka
    
    public $customs_vat_bill_id;
    
    public static $bill_item_types = [
        'WAREHOUSE' => 'Magacin',
        'SERVICES' => 'Usluga',
        'FIXED_ASSETS' => 'Osnovno sredstvo',
    ];
    
    public static $VAT_0 = '0';
    public static $VAT_10 = '10';
    public static $VAT_20 = '20';
    
    public static $vat_types = [
        '0' => '0%',
        '10' => '10%',
        '20' => '20%',
    ];
            
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'accounting.bill_items';
    }
    
    public function moduleName()
    {
        return 'accounting';
    }
    
    public function __get($column) 
    {
        switch ($column) 
        {
            case 'value_per_unit_with_rabat': return $this->value/$this->quantity;
            case 'value_total' : return $this->value + $this->value_vat + $this->value_vat_customs;
            case 'SearchName':
            case 'DisplayName': return ($this->isWarehouseBillItem)?$this->warehouse_material->DisplayName:$this->title;
            case 'isWarehouseBillItem': return $this->bill_item_type == self::$WAREHOUSE;
            case 'isServicesBillItem': return $this->bill_item_type == self::$SERVICES;
            case 'isFixedAssetsBillItem': return $this->bill_item_type == self::$FIXED_ASSETS;
            case 'value_per_unit_with_vat': return SIMAHtml::number_format($this->value_total / $this->quantity);
            default: return parent::__get($column);
        }
    }
    
    public function afterFind()
    {
        parent::afterFind();
        if (!empty($this->customs_vat_bill_item_id))
        {
            $this->customs_vat_bill_id = $this->customs_vat_bill_item->bill_id;
        }
    }
    
    public function relations($child_relations = [])
    {
        $bill_items_table = BillItem::tableName();
        $alias1 = SIMAHtml::uniqid();
        $alias2 = SIMAHtml::uniqid();
        return parent::relations(array_merge([
            'bill' => array(self::BELONGS_TO, 'AnyBill', 'bill_id'),
            'customs_vat_bill' => array(self::BELONGS_TO, 'BillIn', 'customs_vat_bill_id',
                'alias' => $alias1,
                'condition' => "exists (select 1 from $bill_items_table where bill_id = $alias1.id and no_vat_type = '".POPDVBillLists::$U_2_Cb."')"
            ),
            'customs_vat_bill_item' => array(self::BELONGS_TO, 'BillItem', 'customs_vat_bill_item_id',
                'condition' => "no_vat_type = '".POPDVBillLists::$U_2_Cb."'"
            ),

            'customs_vat_base_bill_item' => array(self::HAS_ONE, 'BillItem', 'customs_vat_bill_item_id',
//                'condition' => "no_vat_type = '".POPDVBillLists::$U_2_Cb."'"
            ),
            'confirmed_user' => array(self::BELONGS_TO, 'User', 'confirmed_user_id'),
            'unit' => array(self::BELONGS_TO, 'MeasurementUnit', 'unit_id'),
            'warehouse_material' => array(self::BELONGS_TO, 'WarehouseMaterial', 'warehouse_material_id'),
            'cost_type' => array(self::BELONGS_TO, 'CostType', 'cost_type_id'),
            'cost_location' => array(self::BELONGS_TO, 'CostLocation', 'cost_location_id'),
            'fixed_assets' => array(self::HAS_MANY, 'FixedAsset', 'bill_item_id')
        ],$child_relations));
    }
    
    
    public function beforeValidate()
    {
        //TODO:ispraviti da se ne koristi vise vat polja nego samo jedno
        //potrebni ozbiljni testovi za ovo
        if (in_array($this->no_vat_type, POPDVBillLists::$internalVATList))
        {
            if ($this->vat !=  '0') //ovo znaci da je bio u formi
            {
                $this->internal_vat = $this->vat;
                if (!$this->bill->invoice)//u formi ce biti postavljeno na 0, jer se za izlazne sakriva, a ne treba da se menja u 0
                {
                    $this->value_internal_vat = $this->value_vat;
                }
                $this->is_internal_vat = true;
                $this->vat = 0;
                $this->value_vat = 0;
            }
        }
        else
        {
            $this->internal_vat = 0;
            $this->value_internal_vat = 0;
            $this->is_internal_vat = false;
        }
        if ( $this->no_vat_type === POPDVBillLists::$U_2_Cb)
        {
            if ($this->vat !=  '0') //ovo znaci da je bio u formi
            {
                $this->value_customs = $this->value;
                $this->vat_customs = $this->vat;
                $this->value_vat_customs = $this->value_vat;
                $this->vat = 0;
                $this->value = 0;
                $this->value_per_unit = 0;
                $this->value_vat = 0;
            }
        }
        else
        {
            $this->value_customs = 0;
            $this->vat_customs = 0;
            $this->value_vat_customs = 0;
        }
        
        return parent::beforeValidate();
    }

    public function rules() {
        return array(
            ['bill_id','noChange', 'on' => ['update']],
            array('value_per_unit', 'default',
                'value' => 0,
                'setOnEmpty' => true, 'on' => array('insert', 'update')),
            ['quantity, value_per_unit, bill_id', 'required', 'on' => array('insert','update')],
            ['title', 'checkTitle'],
            ['cost_location_id, cost_type_id', 'safe'],
//            ['cost_location_id','required'],
            ['value', 'checkBillUnreleasedAmount'],
            ['id', 'noDiscountOnNoBill'],
            ['value, value_per_unit', 'lessThenZeroIfNotDiscount'],
            
            ['unit_id, is_discount_item, confirmed', 'safe'],
            ['vat, internal_vat, internal_vat_value, is_internal_vat, vat_refusal, no_vat_type, customs_vat_bill_id', 'safe'],
            ['value_total, value_vat, value', 'safe'],
            ['discount','numerical','max' => 100, 'min' => 0],
            ['discount','checkDiscountAndValue'],
            ['customs_vat_bill_item_id','checkCustomsVatBillItem'],
            ['customs_vat_bill_item_id','unique', 'message' => Yii::t('AccountingModule.BillItem', 'CustomsBillItemTaken')],
            ['order', 'numerical', 'min' => 1, 'max' => '1000000'],
            ['order', 'checkOrder'],
            ['quantity', 'checkQuantity'],
            ['warehouse_material_id', 'checkWarehouseMaterialId', 'on' => array('delete')],
            ['vat_refusal','default',
                'value' => 'true',
                'setOnEmpty' => true, 'on' => array('insert', 'update')
            ],
            ['vat_refusal','checkVatRefusal'],
            ['vat','default',
                'value' => BillItem::$VAT_20,
                'setOnEmpty' => true, 'on' => array('insert', 'update')
            ],
        );
    }  
    
    public function checkCustomsVatBillItem()
    {
        if (empty($this->customs_vat_bill_item_id) && !empty($this->customs_vat_bill_id))
        {
            $this->addError(customs_vat_bill_item_id, Yii::t('AccountingModule.BillItem', 'ChooseBillItem'));
        }
    }
    
    public function checkDiscountAndValue()
    {
        if (!$this->hasErrors())
        {
            if ($this->discount == 100 && $this->new_value != 0)
            {
                $error_txt = Yii::t('AccountingModule.BillItem', 'With100DiscountValueMustBeZero');
                $this->addError('discount', $error_txt);
                $this->addError('value', $error_txt);
            }
        }
    }    
    
    //maksimalne vrednosti koje stavka moze da ima
    //u sustini, ogranicenje postoji samo ako su zakljucane vrednosti racuna
    private $max_value = 0;
    private $max_value_vat = 0;
    private function calcMaxValues()
    {
        //uzima se vrednost pre cuvanja
        if (!$this->columnChanged(['vat']))
        {
            $this->max_value = floatval($this->__old['value']);
            $this->max_value_vat = floatval($this->__old['value_vat']);
        }
        else
        {//za svaki slucaj da se resetuje
            $this->max_value = 0;
            $this->max_value_vat = 0;
        }
        //i dodaju se sve stavke koje nisu potvrdjene
        $unreleased_amount_in_vat_sum = BillItem::model()->find([
            'model_filter' => [
                'bill' => ['ids' => $this->bill_id],
                'confirmed' => false,
                'vat' => $this->vat
            ],
            'select' => 'sum(value) as value, sum(value_vat) as value_vat',
        ]);
        $this->max_value += $unreleased_amount_in_vat_sum->value;
        $this->max_value_vat += $unreleased_amount_in_vat_sum->value_vat;
    }
    
    private $new_value = 0;
    private $new_value_vat = 0;
    private function calcNewValues()
    {
//        $_old_value = $this->__old['value'] + $this->__old['value_vat'];
        $this->new_value = $this->__old['value'];
        $this->new_value_vat = $this->__old['value_vat'];
        $changed_base_value = false;
        if ($this->columnChanged(['value']))
        {
            $this->new_value = $this->value;
            $changed_base_value = true;
        }
        elseif($this->columnChanged('value_per_unit'))
        {
            $this->new_value = round($this->value_per_unit * $this->quantity * (100.00 - $this->discount)/100, 2);
            $changed_base_value = true;
        }
        elseif($this->columnChanged(['quantity','discount']) && !$this->bill->lock_amounts)
        {
            $this->new_value = round($this->value_per_unit * $this->quantity * (100.00 - $this->discount)/100, 2);
            $changed_base_value = true;
        }
        
        if ($this->columnChanged('value_vat'))
        {
            $this->new_value_vat = $this->value_vat;
        }
        elseif($changed_base_value)
        {
//            error_log("$this->new_value*$this->vat/100");
//            error_log($this->new_value*$this->vat);
            $_new_value_vat = round($this->new_value*$this->vat/100, 2);
            if (!SIMAMisc::areEqual($this->value_vat, $_new_value_vat, 0.03)) //2 pare razlike je dozvoljeno
            {
                $this->new_value_vat = $_new_value_vat;
            }
        }
    }
    
    public function checkBillUnreleasedAmount()
    {
        $this->calcMaxValues();
        $this->calcNewValues();
        
        /**
         * ako su vrednosti otkljucane, moze maksimalno da se spusti za vrednost koja nije rasklnjizena
         * absolutne vrednosti idu na ukupne vrednosti zbog knjiznog odobrenja
         * bila je greska i stavljena je apsolutna vrednost na promenu(svn28046), 
         * ali mora na vrednosti da bi promena mogla da bude i pozitivna i negativna
         */
        if (!$this->bill->lock_amounts &&
                SIMAMisc::lessThen(abs($this->bill->unreleased_amount), 
                        //promena vrednosti
                            abs($this->__old['value'] + $this->__old['value_vat'])//stara vrednost
                                -
                            abs($this->new_value + $this->new_value_vat) //nova vrednost
                        ))
        {
            foreach (['value','value_vat','value_per_unit','quantity'] as $error_column)
            {
                if ($this->columnChanged($error_column))
                {
                    $this->addError($error_column, Yii::t('AccountingModule.Bill', 'UnreleasedAmountUnderZero'));
                }
            }
        }
        
        /**
         * Ukoliko su vrednosti zakljucane, vrednost moze da se poveca maksimalno za nerasporedjeni deo 
         */
        if ($this->bill->lock_amounts &&
                SIMAMisc::lessThen(abs($this->max_value + $this->max_value_vat), abs($this->new_value + $this->new_value_vat)))
        {
//            error_log("SIMAMisc::lessThen(abs($this->max_value + $this->max_value_vat), abs($this->new_value + $this->new_value_vat))");
            $this->addError('value',Yii::t('AccountingModule.Bill', 'ValueNoMoreThen', ['{value}' =>  SIMAHtml::number_format($this->max_value)]));
            $this->addError('value_vat',Yii::t('AccountingModule.Bill', 'ValueNoMoreThen', ['{value}' =>  SIMAHtml::number_format($this->max_value_vat)]));
        }
    }
    
    public function noDiscountOnNoBill()
    {
        if (!$this->hasErrors())
        {
            if (!$this->bill->isBillTypeWithDiscount && $this->is_discount_item)
            {
                $this->addError($column, 'Popust ne postoji za ovaj tip dokumenta');
            }
        }
    }
    
    public function lessThenZeroIfNotDiscount($column)
    {
        if (!$this->hasErrors() && ($this->columnChanged($column) || $this->isNewRecord))
        {
            if ($this->bill->isReliefBill)
            {
                if (SIMAMisc::lessThen(0.0, $this->$column))
                {
                    $this->addError($column, 'Stavke knjiznog odobrenja moraju biti negativne');
                }
            }
            else
            {
                if (!$this->is_discount_item && SIMAMisc::lessThen($this->$column, 0.0))
                {
                    $this->addError($column, 'Stavka koja nije popust ne sme biti manja od nule');
                }
            }
            //podrazumeva se da je racun zbog prethodnog rule-a
            if ($this->is_discount_item && SIMAMisc::lessThen(0.0, $sign*$this->$column))
            {
                $this->addError($column, 'Popust mora biti manji od nule');
            }
        }
    }
  
    public function checkTitle()
    {
        if (!$this->isWarehouseBillItem)
        {
            if (empty($this->title))
            {
                $this->addError('title', 'Morate popuniti naziv');
            }
        }
    }
    
    public function checkOrder($attribute, $params)
    {
        if ($this->scenario !== 'update_order_from_after_work' && !empty($this->order) && $this->__old['order'] !== intval($this->order))
        {
            $bill_item = BillItem::model()->findByAttributes([
                'bill_id'=>$this->bill_id,
                'order'=>$this->order
            ]);
            if ($bill_item !== null)
            {
                $this->addError('order', 'Već postoji stavka sa ovim rednim brojem!');
            }
        }
    }
    
    public function checkQuantity($attribute, $params)
    {
        if ($this->isWarehouseBillItem && floatval($this->__old['quantity']) !== $this->quantity)
        {
            $this->addError('quantity', Yii::t('AccountingModule.BillItem', 'CanNotChangeQuantityWhenFromWarehouse'));
        }
        elseif($this->quantity <= 0)
        {
            $this->addError('quantity', Yii::t('AccountingModule.BillItem', 'CanNotChangeQuantityWhenLessThenZero'));
        }
        elseif($this->quantity > 100000000000)
        {
            $this->addError('quantity', Yii::t('AccountingModule.BillItem', 'QuantityOverMaxLimit', [
                '{max_limit}' => SIMAHtml::number_format(100000000000, 3)
            ]));
        }
    }
    
    /**
     * Funkcija koja vraca true ukoliko moz da se obrise stavka. Ako je nema, isto vraca true
     * @param boolean $and_delete - da li da usput i obrise stavku, ako postoji
     * @return boolean vraca true ako moze da se brise. Ako je prosledjen parametar $and_delete, onda vraca true samo ako je stvarno obrisao
     */
    private function checkCanDeleteWarehouseitem(bool $and_delete = false)
    {
        if ($this->isWarehouseBillItem)
        {
            if ($this->bill->isBillWT)
            {
                $WT_item = WarehouseTransferItem::model()->findByAttributes([
                    'warehouse_material_id' => $this->warehouse_material_id,
                    'warehouse_transfer_id' => $this->bill->id
                ]);
                if (is_null($WT_item))
                {
                    if ($and_delete)
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                if (SIMAMisc::areEqual($WT_item->quantity, $this->quantity))
                {
                    if ($and_delete)
                    {
                        $WT_item->delete();
                    }
                    return true;
                }
            }
            
            return false;
        }
        
        if ($and_delete)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    
    public function checkWarehouseMaterialId($attribute, $params)
    {
        if (!$this->checkCanDeleteWarehouseitem())
        {
            $this->addError('warehouse_material_id', Yii::t('AccountingModule.BillItem', 'CanNotDeleteWhenFromWarehouse'));
        }
    }
    
    public function checkVatRefusal()
    {
        if (!$this->hasErrors())
        {
            if (is_null($this->vat_refusal) && in_array($this->vat,['10','20']))
            {
                $this->addError('vat_refusal', 'Obavezno polje');//jer nema smisla da se ne odbije? mozda?
            }
        }
    }
    
    public function afterValidate() 
    {
        if (!$this->hasErrors() && !$this->valueChangedCorrectly())
        {
            $_value = round($this->value_per_unit * $this->quantity,2);
            $_value_per_unit = round($this->value / $this->quantity,10);
            $this->addError('value_per_unit', Yii::t('AccountingModule.BillItem', 'ValuePerUnitAndValueConflict',['{value}'=>$_value_per_unit]));
            $this->addError('value', Yii::t('AccountingModule.BillItem', 'ValuePerUnitAndValueConflict',['{value}'=>$_value]));
        }
        
        parent::afterValidate();
    }
    
    private function valueChangedCorrectly()
    {
        return
            ($this->no_vat_type === POPDVBillLists::$U_2_Cb)
            ||
            SIMAMisc::areEqual($this->value,$this->value_per_unit * $this->quantity, 0.005)
            ||
            (
                $this->isNewRecord
                &&
                (empty($this->value_per_unit) || empty($this->value) )
            )
            ||
            (
                !$this->isNewRecord
                &&
                !$this->columnChanged(['value_per_unit', 'value'], 'AND') 
            )
        ;
    }
    
    public function modelSettings($column) 
    {
        switch ($column)
        {
            case 'filters' : return array_merge(parent::modelSettings('filters'),array(
//                'bill_id' => 'dropdown',
                'bill' => 'relation',
                'title' => 'text',
                'warehouse_material' => 'relation',
                'cost_type' => 'relation',
                'cost_location' => 'relation',
                'bill_item_type' => 'text',
                'confirmed' => 'boolean',
                'no_vat_type' => 'dropdown',
                'internal_vat' => 'dropdown',
                'vat_refusal' => 'boolean',
                'vat' => 'dropdown',
                'vat_customs' => 'dropdown',
                'abbill_item' => 'relation' //Sasa A. - bice ignorisano ako nije ukljucen modul autoBilling. Mora ovako jer behavior ne podrzava funkciju modelSettings
            ));
            case 'update_relations': return ['bill','cost_location','cost_type'];
            case 'number_fields': return [
                'value_per_unit', 
                'vat',
                'quantity', 
                'value',
                'value_vat', 
                'value_internal_vat', 
                'value_total', 
                'value_customs',
                'value_vat_customs',
                'discount'
            ];
            case 'multiselect_options':  return ['delete', 'statuses' => ['confirmed']];
            case 'statuses' : return array(
                    'confirmed' => array(
                        'title' => 'Potvrdjen',
                        'timestamp' => 'confirmed_timestamp',
                        'user' => array('confirmed_user_id', 'relName' => 'confirmed_user'),
//                        'checkAccessConfirm' => 'checkConfirm1Access',
//                        'onConfirm' => 'onConfirm1Function'
                    ),
                );
            case 'mutual_forms': return [
                'relations'=>[
                    'abbill_item'
                ]
            ];
            default: return parent::modelSettings($column);
        }
    }

    protected function modelOptions(User $user = null):array
    {
        $model_options = ['form'];
        if ($this->isServicesBillItem)
        {
            array_push($model_options, 'delete');
        }
        return $model_options;
    }
    
    public function scopes() 
    {
        $alias = $this->getTableAlias();
        return array(
            'byName' => array(
                'order' => "$alias.\"order\", $alias.id",
            ),
            'byOrder' => array(
                'order' => "$alias.\"order\", $alias.id",
            ),
            'byOrderDesc' => array(
                'order' => "$alias.\"order\" DESC, $alias.id",
            ),
            'byVat' => array(
                'order' => "$alias.\"vat\", $alias.id",
            ),
            'byValueDesc' => array(
                'order' => "$alias.\"value\" DESC, $alias.id",
            ),
        );
    }
    
    public function beforeSave()
    {
        if ($this->isWarehouseBillItem)
        {
            $this->title = $this->warehouse_material->DisplayName;
        }
        if (in_array($this->no_vat_type, POPDVBillLists::$autoVatRefusalTRUEList))
        {
            $this->vat_refusal = true;
        }
        elseif (in_array($this->no_vat_type, POPDVBillLists::$autoVatRefusalFALSEList))
        {
            $this->vat_refusal = false;
        }
        
        if (
            !($this->no_vat_type === POPDVBillLists::$U_2_Cb)
            &&
            (
                (
                    $this->isNewRecord
                    &&
                    (!empty($this->value_per_unit) && !empty($this->value) )
                )
                ||
                (
                    !$this->isNewRecord
                    &&
                    $this->columnChanged(['value_per_unit', 'value'], 'AND') 
                )
            )
            && 
            $this->valueChangedCorrectly()
        )
        {
            $this->value = $this->__old['value'];
        }
        
        if (empty($this->cost_location_id))
        {
            $this->cost_location_id = Yii::app()->configManager->get('accounting.default_cost_location_id', false);
        }
        if (empty($this->cost_type_id))
        {
            $this->cost_type_id = Yii::app()->configManager->get(
                    $this->bill->invoice?
                        'accounting.default_income_cost_type_id'
                        :
                        'accounting.default_cost_cost_type_id'
                    , false);
        }
        
        if ($this->isNewRecord)
        {
            $this->order_for_new_item = $this->bill->getLastOrderNumberOfBillItemsForType($this->bill_item_type) + 1;

            //privremen order jer je polje obavezno, pa ne bi dozvolilo cuvanje (prvo se vrsi cuvanje pa prepakivanje)
            $this->order = $this->bill->getLastOrderNumberOfBillItemsForType() + 1;
        }
        
        return parent::beforeSave();
    }
    
    public function registerAfterWork()
    {
        $params_for_after_work = [
            'bill_id' => $this->bill_id,
            'bill_items_ids_with_new_orders' => [
                [
                    'bill_item_id' => $this->id,
                    'new_order' => $this->order_for_new_item
                ]
            ]
        ];
        Yii::app()->registerAfterWork('WorkBillItems', $params_for_after_work, false);
    }
    
    public function save($runValidation = true, $attributes = null, $throwExceptionOnFail = true)
    {
        //MilosS(22.11.2017): ovo bi trebalo ubaciti u validaciju i onda ovde da ne bude warn nego exception, ili nista posto exception i onako ide
            //ali je validaciju zezanje napraviti jer moze da se promeni i jedinicna cena i ukupna vrednost
        try
        {
            return parent::save($runValidation, $attributes, $throwExceptionOnFail);
        }
//        catch (SIMADbExceptionS0007UnreleasedAmountUnderZero $e)
//        {
//            throw new SIMAWarnUnreleasedAmountUnderZero($this->bill);
//        }
        catch (CdbException $e)
        {
            if (substr_count($e->getMessage(), 'SQLSTATE[S0007]') > 0)
            {
                throw new SIMAWarnUnreleasedAmountUnderZero($this->bill);
            }
            if (substr_count($e->getMessage(), 'SQLSTATE[S0009]') > 0)
            {
                throw new SIMAWarnBillItemNegative($this->bill, $e->getMessage(), $this);
            }
            else
            {
                throw $e;
            }
        }
    }
    
//    public function save($runValidation = true, $attributes = null, $throwExceptionOnFail = true)
//    {
//        try
//        {
//            parent::save($runValidation, $attributes, $throwExceptionOnFail);
//        } 
//        catch (CDbException $DBExxception)
//        {
//            //S0007
//            error_log(__METHOD__.$DBExxception->getCode());
////            error_log($DBExxception->getMessage());
////            error_log($DBExxception->);
////            error_log(CJSON::encode($DBExxception));
//            throw $DBExxception;
//        }
//    }
    
    public function afterSave()
    {
        parent::afterSave();
        
        //MIlosS: ovo mora zbog vrednosti koje se postavljaju u bazi
        //bilo bi jos oblje da parent::afterSave radi refresh()
        $this->refresh();
        
        $this->bill->updateLocalCostTypeLocation();

        //setovanje prijemnica i otpremnica
        if (isset($this->warehouse_material))
        {
            foreach($this->bill->warehouse_receives as $recieve)
            {
                foreach($recieve->warehouse_transfer_items as $item)
                {
                    if ($item->warehouse_material_id == $this->warehouse_material_id)
                    {
                        $item->value_per_unit = $this->value_per_unit;
                        $item->save();
                    }
                }
            }
        }
        
        if (
                !$this->bill->lock_amounts 
                &&
                !empty($this->bill->recurring_bill_id)
                && 
                $this->columnChanged([
                    'value_per_unit', 
                    'vat',
                    'quantity', 
                    'value',
                    'value_vat', 
                    'value_internal_vat',
                    'discount'
                ])
            )//za svaki slucaj, bilo sta
        {
            $this->bill->reconnectRecurringBill();
        }
        if ($this->isNewRecord)
        {
            $this->registerAfterWork();
        }
    }
    
    public function delete($check_result=true, $throwExceptionOnFail = true)
    {
        if ($this->checkCanDeleteWarehouseitem(true))
        {
            return true;
        }
        else
        {
            return parent::delete($check_result, $throwExceptionOnFail);
        }
    }
    
    
    
    public function afterDelete()
    {
        parent::afterDelete();
        $this->bill->updateLocalCostTypeLocation();
        
        $params_for_after_work = [
            'bill_id' => $this->bill_id,
            'bill_items_ids_with_new_orders' => []
        ];
        Yii::app()->registerAfterWork('WorkBillItems', $params_for_after_work, false);
    }
    
    public function addAsFixedAsset($fixed_asset_type_id, $tax_type)
    {
        if (count($this->fixed_assets) === 0)
        {
            $fixed_asset_type = ModelController::GetModel('FixedAssetType', $fixed_asset_type_id);
            $_qnt = ($tax_type==FixedAsset::$FA_GROUP)?1:$this->quantity;
            for($i=$_qnt;$i>0;$i--)
            {      
                
                $fixed_asset = new FixedAsset();
                $title = empty($this->title)?'genericko osnovno sredstvo':$this->title;
                $fixed_asset->name = $title;
                $fixed_asset->fixed_asset_type_id = $fixed_asset_type_id;
                $fixed_asset->tax_type = $tax_type;
                $fixed_asset->acquisition_document_id = $this->bill->file->id;
                $fixed_asset->bill_item_id = $this->id;
                if (in_array($tax_type,[FixedAsset::$FA_GROUP]))
                {
                    $fixed_asset->quantity = $this->quantity;
                }
                $fixed_asset->save(false);
                if (in_array($tax_type,[FixedAsset::$FA, FixedAsset::$FA_GROUP]))
                {
                    $purchase_value = $this->value/$_qnt;
                    $fin_fixed_asset = new AccountingFixedAsset();
                    $fin_fixed_asset->id = $fixed_asset->id;
                    $fin_fixed_asset->purchase_value = $purchase_value;
                    $fin_fixed_asset->current_value_start = $purchase_value;
                    $fin_fixed_asset->save();
                }
            }
        }
        else
        {
            throw new SIMAWarnException('Već je dodata ova stavka u osnovana sredstva');
        }
        
        return $fixed_asset;
    }
    
    /**
     * 
     * @param Year $_year
     */
    public function getBookingAccount(Year $_year)
    {
        
        $raise_note = '';
        $cost_type = $this->cost_type;
        if (is_null($cost_type))
        {
            $cost_type = $this->bill->local_cost_type;
        }
        if (isset($cost_type))
        {
            $job_order_account_base = $cost_type->accountForYear($_year);                
            if (is_null($job_order_account_base))
            {
                $raise_note = "Ne postoji vrsta troška '{$cost_type->DisplayName}' za godinu {$_year->year}.";
            }
        }
        else
        {
            $job_order_account_base = ($this->bill->invoice) ? 
                    Account::getFromParam('accounting.codes.default_income_account',$_year)
                    :
                    Account::getFromParam('accounting.codes.default_costs_account',$_year);
            if (is_null($job_order_account_base))
            {
                $raise_note = 'Nisu postavljeni osnovni konti za radne naloge';
            }
        }
        
        if (is_null($job_order_account_base))
        {
            Yii::app()->raiseNote($raise_note);
            return null;
        }
        //ukoliko nema mesta troska ili ako ima, onda ako je otkaceno da knjizi po lokaciji
        elseif (is_null($cost_type) || $cost_type->book_by_cost_location)
        {
            $cost_location = $this->cost_location;
            if (is_null($cost_location))
            {
                $cost_location = $this->bill->job_order;
            }
            
            $account_for_analytic = null;
            if (!is_null($cost_location))
            {
                $account_for_analytic = $cost_location->findForAnalytics();
            }
            
            return $job_order_account_base->getLeafAccount($account_for_analytic);
        }
        else
        {
            return $job_order_account_base;
        }
    }
}
