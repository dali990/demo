<?php

//Tabela koja spaja racune i olaksice
class AdvanceBillRelease extends SIMAActiveRecord
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'accounting.advance_bill_releases';
    }
    
    public function moduleName()
    {
        return 'accounting';
    }

    public function relations($child_relations = [])
    {
        return array(
            'bill' => array(self::BELONGS_TO, 'Bill', 'bill_id'),
            'advance_bill' => array(self::BELONGS_TO, 'Bill', 'advance_bill_id')
        );
    }
    
    public function scopes()
    {
        $alias = $this->getTableAlias(false, false);
        return [
            'orderByAdvanceBill' => [
                'order' => "$alias.advance_bill_id"
            ],
        ];
    }
    
    public function internalVatCode()
    {
        return Bill::internalVatCodeConvertor($this->internal_vat);
    }

    public function rules()
    {
        return array(
            array('bill_id, advance_bill_id', 'required'),
            array('amount, base_amount, vat_amount, vat_rate', 'required'),
            array('internal_vat', 'safe'),
            array('amount, base_amount, vat_amount', 'numerical'),
            array('amount', 'amountCheck', 'on' => array('insert', 'update')),
            array('bill_id, advance_bill_id', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => array('insert', 'update'))
        );
    }

    public function amountCheck($attribute, $params)
    {
        if (!$this->hasErrors())
        {
            $bill = Bill::model()->findByPk($this->bill_id);
            $advance_bill = AdvanceBill::model()->findByPk($this->advance_bill_id);
            $I_R_code = $this->internalVatCode();

            if ($bill->partner_id != $advance_bill->partner_id || $bill->invoice != $advance_bill->invoice)
            {
                $this->addError('bill_id', Yii::t('AccountingModule.BillRelease','Bill and Advance bill are not compatible'));
                $this->addError('advance_bill_id', Yii::t('AccountingModule.BillRelease','Bill and Advance bill are not compatible'));
            }
            
            $amount_diff = floatval($this->amount)-floatval($this->__old['amount']);
            $base_amount_diff = floatval($this->base_amount)-floatval($this->__old['base_amount']);
            $vat_amount_diff = floatval($this->vat_amount)-floatval($this->__old['vat_amount']);
            if ($I_R_code === 'I')
            {
                if (!SIMAMisc::areEqual(floatval($this->amount), floatval($this->base_amount)))
                {
                    $this->addError('amount', "iznos i osnovica moraju biti jednaki");
                    $this->addError('base_amount', "iznos i osnovica moraju biti jednaki");
                }
            }
            else
            {
                if (!SIMAMisc::areEqual(floatval($this->amount), floatval($this->base_amount) +  floatval($this->vat_amount)))
                {
                    $this->addError('amount', "iznos mora biti jednak zbiru osnovice i PDV");
                    $this->addError('base_amount', "iznos mora biti jednak zbiru osnovice i PDV");
                }
            }
            
            
            
            $_sums_bill = $bill->remainingVatItemsArray();
            $_sums_adv =  $advance_bill->remainingVatItemsArray();
            
            $_bill_base = $_sums_bill[$I_R_code][$this->vat_rate]['base'];
            $_bill_vat = $_sums_bill[$I_R_code][$this->vat_rate]['vat'];
            if ($I_R_code === 'I')
            {
                $_bill_amount = $_bill_base;
            }
            else
            {
                $_bill_amount = $_bill_base + $_bill_vat;
            }
            
            if (SIMAMisc::lessThen(($_bill_amount),$amount_diff))
            {
                $this->addError('amount', "nerasknjizeni deo racuna nije dovoljno velik");
            }
            if (SIMAMisc::lessThen($_bill_base,$base_amount_diff))
            {
                $this->addError('base_amount', "nerasknjizeni deo racuna nije dovoljno velik");
            }
            if (SIMAMisc::lessThen($_bill_vat,$vat_amount_diff))
            {
                $this->addError('vat_amount', "nerasknjizeni deo racuna nije dovoljno velik");
            }
            
            $_adv_bill_base = $_sums_adv[$I_R_code][$this->vat_rate]['base'];
            $_adv_bill_vat = $_sums_adv[$I_R_code][$this->vat_rate]['vat'];
            if ($I_R_code === 'I')
            {
                $_adv_bill_amount = $_bill_base;
            }
            else
            {
                $_adv_bill_amount = $_bill_base + $_bill_vat;
            }
            
            if (SIMAMisc::lessThen(($_adv_bill_amount), $amount_diff))
            {
                $this->addError('amount', "nerasknjizeni deo avansnog racuna nije dovoljno velik");
            }
            if (SIMAMisc::lessThen($_adv_bill_base, $base_amount_diff))
            {
                $this->addError('base_amount', "nerasknjizeni deo avansnog racuna nije dovoljno velik");
            }
            if (SIMAMisc::lessThen($_adv_bill_vat, $vat_amount_diff))
            {
                $this->addError('vat_amount', "nerasknjizeni deo avansnog racuna nije dovoljno velik");
            }
        }
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters':return [
                'bill' => 'relation',
                'advance_bill' => 'relation'
            ];
            case 'number_fields': return [
                'amount','base_amount','vat_amount'
            ];
            case 'update_relations' : return ['bill','advance_bill'];
            case 'multiselect_options': return ['delete'];
            default: return parent::modelSettings($column);
        }
        
    }
    
}
