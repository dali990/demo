<?php

class CashDeskItem extends SIMAActiveRecord
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'accounting.cash_desk_items';
    }

    public function moduleName()
    {
        return 'accounting';
    }

    public function __get($column)
    {
        switch ($column)
        {
            case 'DisplayName': return $this->text;
            case 'SearchName': return $this->text;
            case 'amount_in': return ($this->isIN)?$this->amount:NULL;
            case 'amount_out': return ($this->isIN)?NULL:$this->amount;
            case 'isIN': return (isset($this->cash_desk_order))?$this->cash_desk_order->isIN:NULL;
            default: return parent::__get($column);
        }
    }

    public function relations($child_relations = [])
    {
        return array(
            'cost_type' => array(self::BELONGS_TO, 'CostType', 'cost_type_id'),
            'cash_desk_order' => array(self::BELONGS_TO, 'CashDeskOrder', 'cash_desk_order_id'),
            'cost_location' => array(self::BELONGS_TO, 'CostLocation', 'cost_location_id'),
            'bill' => array(self::BELONGS_TO, 'Bill', 'bill_id'),
        );
    }

    public function rules()
    {
        return array(
            array('text, amount, cash_desk_order_id', 'required', 'message' => 'Polje "{attribute}" mora biti uneto'),
            array('cost_type_id, cost_location_id, bill_id', 'safe'),
            array('amount', 'numerical'),
            ['amount', 'checkNumericNumber', 'precision' => 20, 'scale' => 2],
            ['cost_type_id', 'checkCostTypeAndCostLocation', 'on' => ['insert','update']],
            array('cash_desk_order_id, cost_type_id','default',
                'value'=>null,
                'setOnEmpty'=>true,'on'=>array('insert','update')),
        );
    }
    
    public function checkCostTypeAndCostLocation()
    {
        if (empty($this->cost_type_id))
        {
            if ($this->isCashDeskOrderPayin())
            {
                $default_income_cost_type_id = Yii::app()->configManager->get('accounting.default_income_cost_type_id', false);
                if (empty($default_income_cost_type_id))
                {
                    $this->addError('cost_type_id', Yii::t('AccountingModule.CashDeskItem', 'CostTypeEmpty'));
                }
            }
            else
            {
                $default_cost_cost_type_id = Yii::app()->configManager->get('accounting.default_cost_cost_type_id', false);
                if (empty($default_cost_cost_type_id))
                {
                    $this->addError('cost_type_id', Yii::t('AccountingModule.CashDeskItem', 'CostTypeEmpty'));
                }
            }
        }
        
        /**
         * MilosS(16.7.2019): nema bas smisla da mesto troska bude uvek obavezno,
         * to ima smisla samo kada su u pitanju naplata i isplata, ali prenos sa i na ZR, nema smisla
         */
//        if (empty($this->cost_location_id))
//        {
//            $default_cost_location_id = Yii::app()->configManager->get('accounting.default_cost_location_id', false);
//            if (empty($default_cost_location_id))
//            {
//                $this->addError('cost_location_id', Yii::t('AccountingModule.CashDeskItem', 'CostLocationEmpty'));
//            }
//        }
    }

    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'options': return ['delete'];
            case 'filters': return array(
                'text' => 'text',
                'cost_type' => 'relation',
                'cash_desk_order' => 'relation',
                'amount' => 'numeric',
                'cost_location' => 'relation',
                'bill' => 'relation'
            );
            case 'textSearch' : return array(
                'text' => 'text'
            );
            case 'number_fields': return [
                'amount','amount_in','amount_out'
            ];
            case 'update_relations': return array(
                'cash_desk_order'
            );
            case 'multiselect_options': return ['delete'];
            default: return parent::modelSettings($column);
        }
    }
    
    public function scopes()
    {
        $alias = $this->getTableAlias();
        return array(
            'byName' => array('order' => "$alias.text"),
        );
    }
    
    public function beforeSave() 
    {
        if (empty($this->cost_type_id))
        {
            if ($this->isCashDeskOrderPayin())
            {
                $this->cost_type_id = Yii::app()->configManager->get('accounting.default_income_cost_type_id', false);
            }
            else
            {
                $this->cost_type_id = Yii::app()->configManager->get('accounting.default_cost_cost_type_id', false);
            }
        }
        
        if (empty($this->cost_location_id))
        {
            $this->cost_location_id = Yii::app()->configManager->get('accounting.default_cost_location_id', false);
        }
        
        return parent::beforeSave();
    }
    
    public function afterSave()
    {
//        $this->refresh();
        if (
                $this->__old['text'] !== $this->text || 
                $this->__old['amount'] !== $this->amount || 
                $this->__old['cash_desk_order_id'] !== $this->cash_desk_order_id || 
                $this->__old['cost_type_id'] !== $this->cost_type_id ||
                $this->__old['cost_location_id'] !== $this->cost_location_id ||
                $this->__old['bill_id'] !== $this->bill_id
            )
        {
            $this->removeOrderLogConfirmed();
        }
        
        return parent::afterSave();
    }
    
    public function afterDelete() 
    {    
        $this->removeOrderLogConfirmed();
        
        return parent::afterDelete();
    }
    
    /**
     * pomocna funkcija koja skida potvrdjenost CashDeskOrder-u i CashDeskLog-u
     */
    private function removeOrderLogConfirmed()
    {
        $this->cash_desk_order->confirmed = false;
        $this->cash_desk_order->save();
        $cash_desk_log = CashDeskLog::model()->findByPk($this->cash_desk_order->cash_desk_log_id);
        if ($cash_desk_log !== null)
        {
            $cash_desk_log->confirmed = false;                
            $cash_desk_log->save();                
            $param_id = SIMAHtml::uniqid();
            $criteria = new SIMADbCriteria();
            $criteria->condition = "date > :param1$param_id";
            $criteria->params = array(
                ":param1$param_id" => SIMAHtml::UserToDbDate($cash_desk_log->date)
            );
            $cash_desk_newer_logs = CashDeskLog::model()->findAll($criteria);
            foreach ($cash_desk_newer_logs as $cash_desk_newer_log) 
            {
                $cash_desk_newer_log->confirmed = false;
                $cash_desk_newer_log->save();
            }
        }
    }
    
    public function isCashDeskOrderPayin()
    {
        return $this->cash_desk_order->type === CashDeskOrder::$PAYIN || $this->cash_desk_order->type === CashDeskOrder::$WITHDRAW;
    }
}