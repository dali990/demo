<?php

class PaymentType extends SIMAActiveRecord
{

    public $pathname;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'accounting.payment_types';
    }
    
    public function moduleName()
    {
        return 'accounting';
    }

    public function __get($column)
    {
        switch ($column)
        {
            case 'hasChildren': return ($this->count_children > 0);
            case 'DisplayName': return $this->name;
//     		case 'droplist': 
//     			{
//     				$list = $this->byName()->findAll();
//     				return CHtml::listData($list,'id','name');
//     			}
            default: return parent::__get($column);
        }
    }

    /**
     * ne moze deca jer su u privatnim shemama
     */
    public function relations($child_relations = [])
    {
    	return array(
            'parent'=>array(self::BELONGS_TO, 'PaymentType', 'parent_id'),
            'children'=>array(self::HAS_MANY, 'PaymentType', 'parent_id'),
            'account' => array(self::BELONGS_TO, 'Account', 'account_id'),
    	);
    }
    
    public function accountForYear(Year $year)
    {
        return $this->account->forYear($year);
    }

    public function getModelDropList()
    {
        $paymet_table = PaymentType::model()->tableName();
        $sql = "WITH RECURSIVE reccur(id,name,parent_id,path,depth) AS (
					select id,name,parent_id,'--'||name,''
					from $paymet_table a where parent_id is null
					UNION ALL
					SELECT pt.id, pt.name, pt.parent_id,reccur.path||'0'||pt.name,reccur.depth||'--' FROM $paymet_table pt, reccur  WHERE reccur.id=pt.parent_id
				)
				select r.id,r.depth||'--'||r.name as pathname , pt.name, pt.description 
				from reccur r left join $paymet_table pt on r.id=pt.id
				order by r.path;";
        return CHtml::listData($this->findAllBySql($sql), 'id', 'pathname');
    }

    public function rules()
    {
        return array(
            array('name', 'required', 'message' => 'Polje "{attribute}" mora biti uneto'),
            array('description, parent_id, partner_analitic', 'safe'),
            array('name', 'unique'),
        );
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'textSearch': return array(
                'name'=>'text',
                'description'=>'text',
                'account' => 'relation',
                'partner_analitic' => 'boolean'
            );
            default : return parent::modelSettings($column);
        }
        
    }

    public function beforeSave()
    {
        if (strpos($this->account_id,'l_') !== false) 
        {
            $arr = explode("l_", $this->account_id);
            $account = Account::getAccountByLayout($arr[0], $arr[1]);
            $this->account = $account;
            $this->account_id = $account->id;
        }
        return parent::beforeSave();
    }
    
}
