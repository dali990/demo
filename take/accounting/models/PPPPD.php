<?php

class PPPPD extends SIMAActiveRecord
{
    public static $CREATE_TYPE_SIMA_GENERATE = 'SIMA_GENERATE';
    public static $CREATE_TYPE_USER_UPLOAD = 'USER_UPLOAD';
    public static $CREATE_TYPE_USER_MANUAL = 'USER_MANUAL';
    
    public $_client_uploaded_file = null;
    public $_paycheck_period_id = null;
    
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'accounting.ppppds';
    }

    public function moduleName()
    {
        return 'accounting';
    }
    
    public function __get($column)
    {
        switch ($column)
        {
            case 'DisplayName':
            case 'SearchName': return $this->client_identificator;
            default: return parent::__get($column);
        }
    }
    
    public function rules()
    {
        return [
            ['creation_type, year_id', 'required'],
            ['client_identificator, note, _client_uploaded_file, dirty, _paycheck_period_id', 'safe'],
            ['client_identificator', 'clientIdentificatorCheck'],
            ['creation_type', 'creationTypeCheck'],
            ['creation_type', 'creationTypeCheckUserUpload'],
            ['creation_type', 'creationTypeCheckUserManual'],
        ];
    }
    
    public function clientIdentificatorCheck($attribute, $params)
    {
        if (!empty($this->client_identificator))
        {
            if($this->isNewRecord)
            {
                if(PPPPD::existsClientIndentificator($this->client_identificator))
                {
                    $this->addError($attribute, Yii::t('AccountingModule.PPPPD', 'ClientIdentificatorAlreadyExists'));
                }
            }
        }
    }
    
    public function creationTypeCheck($attribute, $params)
    {
        if (empty($this->creation_type))
        {
            $this->addError('creation_type', Yii::t('AccountingModule.PPPPD', 'CreationTypeEmpty'));
        }
        else if(
                $this->creation_type !== PPPPD::$CREATE_TYPE_SIMA_GENERATE 
                && $this->creation_type !== PPPPD::$CREATE_TYPE_USER_UPLOAD
                && $this->creation_type !== PPPPD::$CREATE_TYPE_USER_MANUAL
            )
        {
            $this->addError('creation_type', Yii::t('AccountingModule.PPPPD', 'CreationTypeInvalidValue', [
                '{creation_type}' => $this->creation_type
            ]));
        }
    }
    
    public function creationTypeCheckUserUpload($attribute, $params)
    {
        if($this->creation_type === PPPPD::$CREATE_TYPE_USER_UPLOAD && empty($this->_client_uploaded_file))
        {
            $this->addError('_client_uploaded_file', Yii::t('AccountingModule.PPPPD', 'ClientUploadedFileEmpty'));
        }
    }
    
    public function creationTypeCheckUserManual($attribute, $params)
    {
        if ($this->creation_type === PPPPD::$CREATE_TYPE_USER_MANUAL && $this->getScenario() === 'paycheck_period_ppppd_tab' && empty($this->_paycheck_period_id))
        {
            $this->addError('_paycheck_period_id', Yii::t('AccountingModule.PPPPD', 'PaycheckPeriodIdEmpty'));
        }
    }
    
    public function relations($child_relations = [])
    {
        return parent::relations(array_merge([
            'file' => [self::BELONGS_TO, 'File', 'id'],
            'items' => [self::HAS_MANY, 'PPPPDItem', 'ppppd_id'],
            'year' => [self::BELONGS_TO, 'AccountingYear', 'year_id']
        ], $child_relations));
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters' : return array_merge(parent::modelSettings('filters'),[
                'paycheck_periods_to_ppppds' => 'relation',
                'client_identificator' => 'text',
                'creation_type' => 'dropdown',
                'year' => 'relation',
                'year_id' => 'dropdown',
                'dirty' => 'boolean'
            ]);
            case 'options': 
                $result = ['form'];
                
                if($this->creation_type === PPPPD::$CREATE_TYPE_SIMA_GENERATE)
                {
                    $result[] = 'download';
                }
                
                return $result;
            default: return parent::modelSettings($column);
        }
    }
    
    public function beforeSave()
    {
        if(empty($this->client_identificator))
        {
            $this->client_identificator = PPPPD::generateUniqueClientIdentificator();
        }
        
        if($this->isNewRecord)
        {
            $file = new File();
            $file->save();

            $this->id = $file->id;
            $this->file = $file;
        }
        
        /// ako je korisnicki okacen dokument
        if(
                $this->creation_type === PPPPD::$CREATE_TYPE_USER_UPLOAD 
                && !empty($this->_client_uploaded_file))
        {
            $client_uploaded_file = $this->_client_uploaded_file;            
            try
            {
                $parsed_key = SIMAUpload::ParseUploadKey($client_uploaded_file, [
                    SIMAUpload::$STATUS_SUCCESS, SIMAUpload::$STATUS_STOPPED
                ]);
                if($parsed_key !== false)
                {
                    $this->populateFromSelfXML($parsed_key['temp_file_name']);
                }
            }
            catch(Exception $e)
            {
                throw new SIMAWarnException(Yii::t('AccountingModule.PPPPD', 'UserUploadedXMLParseProblem', [
                    '{exception_message}' => $e->getMessage()
                ]));
            }
            
            $this->file->appendUpload($client_uploaded_file);
        }
        
        return parent::beforeSave();
    }
    
    public function afterSave()
    {
        parent::afterSave();
        
        if ($this->creation_type === PPPPD::$CREATE_TYPE_USER_MANUAL && $this->getScenario() === 'paycheck_period_ppppd_tab')
        {
            $paycheck_periods_to_ppppds = new PaycheckPeriodToPPPPD();
            $paycheck_periods_to_ppppds->paycheck_period_id = $this->_paycheck_period_id;
            $paycheck_periods_to_ppppds->ppppd_id = $this->id;
            $paycheck_periods_to_ppppds->save();
        }
    }
    
    private function populateFromSelfXML($temp_filename)
    {        
        foreach($this->items as $item)
        {
            $item->delete();
        }
                
        $tempXML = TemporaryFile::GetByName($temp_filename);
                
        $tempPPPDItems = PPPPDComponent::generateTempPPPDItemsFromTempXML($tempXML);
        
        foreach($tempPPPDItems as $tempPPPDItem)
        {
            $tempPPPDItem->ppppd_id = $this->id;
            $tempPPPDItem->save();
        }
    }
    
    public static function generateUniqueClientIdentificator()
    {
        $result = null;
        
        while(true)
        {
            $result = SIMAHtml::uniqid();
            
            if(!PPPPD::existsClientIndentificator($result))
            {
                break;
            }
            
            usleep(100000); //// 100 ms
        }
        
        return $result;
    }
    
    public static function existsClientIndentificator($client_identificator)
    {
        $criteria = new SIMADbCriteria([
            'Model' => PPPPD::model(),
            'model_filter' => [
                'client_identificator' => $client_identificator
            ]
        ], true);
        
        return !empty($criteria->ids);
    }
}
