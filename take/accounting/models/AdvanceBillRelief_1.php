<?php

//Tabela koja spaja racune i olaksice
class AdvanceBillRelief extends SIMAActiveRecord
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'accounting.advance_bill_reliefs';
    }
    
    public function moduleName()
    {
        return 'accounting';
    }
    
    public function internalVatCode()
    {
        return Bill::internalVatCodeConvertor($this->internal_vat);
    }

    public function relations($child_relations = [])
    {
        return array(
            'relief_bill' => array(self::BELONGS_TO, 'ReliefBill', 'relief_bill_id'),
            'advance_bill' => array(self::BELONGS_TO, 'AdvanceBill', 'advance_bill_id')
        );
    }

    public function rules()
    {
        return array(
            array('relief_bill_id, advance_bill_id', 'required'),
            array('amount, base_amount, vat_amount, vat_rate', 'required'),
            array('internal_vat', 'safe'),
            array('amount, base_amount, vat_amount', 'numerical'),
            array('amount', 'amountCheck', 'on' => array('insert', 'update')),
            array('relief_bill_id, advance_bill_id', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => array('insert', 'update'))
        );
    }

    public function amountCheck($attribute, $params)
    {
        if (!$this->hasErrors())
        {
            $relief_bill = ReliefBill::model()->findByPk($this->relief_bill_id);
            $advance_bill = AdvanceBill::model()->findByPk($this->advance_bill_id);
            $I_R_code = $this->internalVatCode();

            if ($relief_bill->partner_id != $advance_bill->partner_id || $relief_bill->invoice != $advance_bill->invoice)
            {
                $this->addError('relief_bill_id', Yii::t('AccountingModule.BillRelease','Relief bill and advance bill are not compatible'));
                $this->addError('advance_bill_id', Yii::t('AccountingModule.BillRelease','Relief bill and advance bill are not compatible'));
            }
            
//            $amount_diff = floatval($this->amount)-floatval($this->__old['amount']);
//            $base_amount_diff = floatval($this->base_amount)-floatval($this->__old['base_amount']);
//            $vat_amount_diff = floatval($this->vat_amount)-floatval($this->__old['vat_amount']);
            
            $old_amount = 0;
            $old_base_amount = 0;
            $old_vat_amount = 0;
            if(!$this->isNewRecord)
            {
                $old_amount = $this->__old['amount'];
                $old_base_amount = $this->__old['base_amount'];
                $old_vat_amount = $this->__old['vat_amount'];
            }
            $amount_diff = floatval($this->amount)-floatval($old_amount);
            $base_amount_diff = floatval($this->base_amount)-floatval($old_base_amount);
            $vat_amount_diff = floatval($this->vat_amount)-floatval($old_vat_amount);
            
            $_sums_adv = $advance_bill->remainingVatItemsArray();
            $_sums_rel =  $relief_bill->remainingVatItemsArray();
            
            $_adv_base = $_sums_adv[$I_R_code][$this->vat_rate]['base'];
            $_adv_vat = $_sums_adv[$I_R_code][$this->vat_rate]['vat'];
            if ($I_R_code === 'I')
            {
                $_adv_bill_amount = $_adv_base;
            }
            else
            {
                $_adv_bill_amount = $_adv_base + $_adv_vat;
            }
            
            
            if (SIMAMisc::lessThen($_adv_bill_amount, $amount_diff))
            {
                $this->addError('amount', "nerasknjizeni deo racuna nije dovoljno velik");
            }
            if (SIMAMisc::lessThen($_adv_base, $base_amount_diff))
            {
                $this->addError('base_amount', "nerasknjizeni deo racuna nije dovoljno velik");
            }
            if (SIMAMisc::lessThen($_adv_vat, $vat_amount_diff))
            {
                $this->addError('vat_amount', "nerasknjizeni deo racuna nije dovoljno velik");
            }
            
            $_rel_bill_base = $_sums_rel[$I_R_code][$this->vat_rate]['base'];
            $_rel_bill_vat = $_sums_rel[$I_R_code][$this->vat_rate]['vat'];
            if ($I_R_code === 'I')
            {
                $_rel_bill_amount = $_rel_bill_base;
            }
            else
            {
                $_rel_bill_amount = $_rel_bill_base + $_rel_bill_vat;
            }
            
            if (SIMAMisc::lessThen($_rel_bill_amount, $amount_diff))
            {
                $this->addError('amount', "nerasknjizeni deo avansnog racuna nije dovoljno velik");
            }
            if (SIMAMisc::lessThen($_rel_bill_base, $base_amount_diff))
            {
                $this->addError('base_amount', "nerasknjizeni deo avansnog racuna nije dovoljno velik");
            }
            if (SIMAMisc::lessThen($_rel_bill_vat, $vat_amount_diff))
            {
                $this->addError('vat_amount', "nerasknjizeni deo avansnog racuna nije dovoljno velik");
            }
        }
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters':return [
                'relief_bill' => 'relation',
                'advance_bill' => 'relation'
            ];
            case 'number_fields': return [
                'amount','base_amount','vat_amount'
            ];
            case 'update_relations' : return ['relief_bill','advance_bill'];
            default: return parent::modelSettings($column);
        }
        
    }
    
}
