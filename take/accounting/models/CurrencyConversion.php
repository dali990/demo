<?php

class CurrencyConversion extends SIMAActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'accounting.currency_conversions';
    }

    public function moduleName()
    {
        return 'accounting';
    }
    
    public function relations($child_relations = [])
    {
        return array(
            'payment_in' => array(self::BELONGS_TO, 'Payment', 'payment_in_id'),
            'payment_out' => array(self::BELONGS_TO, 'Payment', 'payment_out_id')
        );
    }
    
    public function rules()
    {
        return array(
            array('payment_in_id, payment_out_id', 'required'),
            array('payment_in_id', 'checkPaymentInOut'),
        );
    }
    
    public function checkPaymentInOut() 
    {
        if(empty($this->payment_in->bank_statement->account->currency))
        {
            $this->addError('payment_in_id', Yii::t('AccountingModule.CurrencyConversion', 'PaymentInAccountNotSetCurrency'));
        }
        else if(empty($this->payment_out->bank_statement->account->currency))
        {
            $this->addError('payment_out_id', Yii::t('AccountingModule.CurrencyConversion', 'PaymentOutAccountNotSetCurrency'));
        }
        else if($this->payment_in->bank_statement->account->currency->id === $this->payment_out->bank_statement->account->currency->id)
        {
            $this->addError('payment_in_id', Yii::t('AccountingModule.CurrencyConversion', 'PaymentInAndOutAccountsSameCurrency'));
            $this->addError('payment_out_id', Yii::t('AccountingModule.CurrencyConversion', 'PaymentInAndOutAccountsSameCurrency'));
        }
        else if($this->payment_in->payment_date !== $this->payment_out->payment_date)
        {
            $this->addError('payment_in_id', Yii::t('AccountingModule.CurrencyConversion', 'PaymentInAndOutNotOnSameDate'));
            $this->addError('payment_out_id', Yii::t('AccountingModule.CurrencyConversion', 'PaymentInAndOutNotOnSameDate'));
        }
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters': return [
                'payment_in' => 'relation',
                'payment_out' => 'relation'
            ];
            case 'number_fields':
                return [
                    'middle_currency_rate',
                    'exchange_rate'
                ];
            default: return parent::modelSettings($column);
        }
    }
    
    public function isAnyOfPaymentsFromCountryCurrencyAccount()
    {
        if($this->isInPaymentFromCountryCurrencyAccount() 
            || $this->isOutPaymentFromCountryCurrencyAccount())
        {
            return true;
        }
        
        return false;
    }
    
    public function isInPaymentFromCountryCurrencyAccount()
    {
        return $this->payment_in->bank_statement->account->currency->isCountryCurrency();
    }
    
    public function isOutPaymentFromCountryCurrencyAccount()
    {
        return $this->payment_out->bank_statement->account->currency->isCountryCurrency();
    }
}