<?php

/**
 * Uplate. Ubacuju se i one koje nisu rasknjizene/povezane sa racunima
 */
class PaymentIn extends Payment
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function beforeSave()
    {
        $this->invoice = true;
        return parent::beforeSave();
    }

    //Ukljucuju se i avansne uplate koje nisu povezane sa avansima
    public function defaultScope()
    {
        $payments_table = Payment::model()->tableName();
        $bill_table = Bill::model()->tableName();
        $bill_release_table = BillRelease::model()->tableName();
        $default_payment_type = Yii::app()->configManager->get('accounting.default_payment_type');
        $advance_payment_type = Yii::app()->configManager->get('accounting.advance_payment_type');
        $alias = $this->getTableAlias(FALSE, FALSE);
        $adv_bill = Bill::$ADVANCE_BILL;
        return array(
            'condition' => "$alias.invoice=true and ($alias.payment_type_id=$default_payment_type or
                    ($alias.payment_type_id=$advance_payment_type and not exists 
                        (select br.id 
                            from $bill_release_table br 
                                left join $bill_table bls on br.bill_id = bls.id 
                                left join $payments_table pay on br.bill_id = pay.id 
                            
                            where
                                br.payment_id = $alias.id
                                and bls.bill_type = '$adv_bill'
                                and br.amount = bls.amount
                                and br.amount = pay.amount
                        ) 
                    )
                )",
        );
    }

}
