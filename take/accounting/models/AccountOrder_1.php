<?php

/**
 * @package SIMA
 * @subpackage Accounting
 */
class AccountOrder extends SIMAActiveRecord
{
    
    private $_saldo = null;
    
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'accounting.account_orders';
    }

    public function moduleName()
    {
        return 'accounting';
    }

    public function __get($column)
    {
        switch ($column)
        {
            case 'path2': return 'files/file';
            case 'DisplayName':
            case 'SearchName': 
                $result = (string)$this->order.'/';
                $year_num = '????';
                if(!empty($this->year))
                {
                    $year_num = $this->year->year;
                }
                $result .= $year_num;
                return $result;
            case 'isBooked': return $this->booked;
            case 'isBooked_status': { 
                if($this->booked)
                {
                    $isBooked_status = 'Potvrđeno';
                } else {
                    $isBooked_status = 'Nepotvrđeno';
                }
                return $isBooked_status;
            }
            case 'bookerName': {
                $bookerName = '-';
                if($this->booked && !empty($this->booker))
                {
                    $bookerName = $this->booker->display_name;
                }
                return $bookerName;
            }
            case 'bookedTimestamp': {
                $bookedTimestamp = '-';
                if($this->booked)
                {
                    /**
                     * MIlosJ: kada se tek proknjizi nalog, u tom trenutku, u aftersave-u, preko onconfirm f-ja
                     * se zove generisanje pdf-a, a tada ovaj atribut ima vrednost 'now()'
                     * i ovde se generise datum '01.01.1970'
                     * pa treba posebno obraditi now()
                     * MilosS: Nabudzeno - pravo resenje je opisano u zahtevu "posle ->save() da ide ->refresh()"
                     */
                    $booked_timestamp = $this->booked_timestamp;
                    if($booked_timestamp === 'now()')
                    {
                        $ao = AccountOrder::model()->findByPkWithCheck($this->id);
                        if(!empty($ao))
                        {
                            $booked_timestamp = $ao->booked_timestamp;
                            $bookedTimestamp = date('d.m.Y', strtotime($booked_timestamp));
                        }
                    }
                    else
                    {
                        $bookedTimestamp = date('d.m.Y', strtotime($booked_timestamp));
                    }
                }
                return $bookedTimestamp;
            }
            case 'pdf_generation_time': return date('d.m.Y');
            default: return parent::__get($column);
        }
    }
    
    public function __isset($column)
    {
        switch ($column)
        {
            case 'saldo': !empty($this->_saldo);
            default: return parent::__isset($column);
        }
    }
    
    public function __set($name, $value)
    {           
        switch ($name)
        {
            case 'saldo': $this->_saldo = $value; break;            
            default: parent::__set($name, $value); break;
        }
    }
    
//    public function setsaldo($value)
//    {
//        $this->_saldo = $value;
//    }
    
    public function getsaldo()
    {
        /**
         * 
         */
        if (empty($this->_saldo) && !empty($this->id))
        {
            $criteria = new SIMADbCriteria([
                'Model' => 'AccountTransaction',
                'model_filter' => array(
                    'account_document'=>array(
                        'account_order' => array(
                            'ids' => $this->id
                        )
                    )
                ),
                'select'=>'sum(credit) as credit, sum(debit) as debit'
            ]);
            $sum = AccountTransaction::model()->find($criteria);            
            $this->_saldo = number_format($sum->credit - $sum->debit, 2);
        }
        return $this->_saldo;
    }
    
    public function getdebit()
    {        
        if (!empty($this->id))
        {
            $criteria = new SIMADbCriteria([
                'Model' => 'AccountTransaction',
                'model_filter' => array(
                    'account_document'=>array(
                        'account_order' => array(
                            'ids' => $this->id
                        )
                    )
                ),
                'select'=>'sum(debit) as debit'
            ]);
            $sum = AccountTransaction::model()->find($criteria);
            return $sum->debit;
        }
        else
        {
            return null;
        }        
    }
    
    public function getcredit()
    {        
        if (!empty($this->id))
        {
            $criteria = new SIMADbCriteria([
                'Model' => 'AccountTransaction',
                'model_filter' => array(
                    'account_document'=>array(
                        'account_order' => array(
                            'ids' => $this->id
                        )
                    )
                ),
                'select'=>'sum(credit) as credit'
            ]);
            $sum = AccountTransaction::model()->find($criteria);
            return $sum->credit;
        }
        else
        {
            return null;
        }        
    }
    
    public function getaccount_transactions()
    {
        $criteria = new SIMADbCriteria([
            'Model' => 'AccountTransaction',
            'model_filter' => array(
                'account_document'=>array(
                    'account_order' => array(
                        'ids' => $this->id
                    )
                ),
                'display_scopes'=>['byOrder','withSaldoAndNoStart']
            )
        ]);
        return AccountTransaction::model()->findAll($criteria);
    }
    
    public function getaccount_transactions_count()
    {
        $criteria = new SIMADbCriteria([
            'Model' => 'AccountTransaction',
            'model_filter' => array(
                'account_document'=>array(
                    'account_order' => array(
                        'ids' => $this->id
                    )
                )
            ),
            'select'=>'count(*) as saldo'
        ]);
        $ret = AccountTransaction::model()->find($criteria);
        return $ret->saldo;
    }
    
    public function getis_account_document()
    {
        return AccountDocument::model()->countByAttributes(['id'=>$this->id])>0;
    }

    public function relations($child_relations = [])
    {
        return array(
            'file' => array(self::BELONGS_TO, 'File', 'id'),
            'account_documents' => array(self::HAS_MANY, 'AccountDocument', 'account_order_id'),
            'account_documents_cnt' => array(self::STAT, 'AccountDocument', 'account_order_id', 'select'=>'count(*)'),
            'canceled_account_documents' => array(self::HAS_MANY, 'AccountDocument', 'canceled_account_order_id'),
            'year' => array(self::BELONGS_TO, 'Year', 'year_id'),
            'month' => array(self::BELONGS_TO, 'Month', 'month_id'),
            'booker' => [self::BELONGS_TO, 'User', 'booker_id']
        );
    }

    public function rules()
    {
        return array(
            array('order, date', 'required'),
            array('id, note, order, year_id', 'safe'),
            ['order', 'length', 'max' => 30],
            array('order', 'uniqueSameYear'),
            array('date','checkSameYear'),
            array('booked', 'checkBooked'),
            array('id, date', 'default',
                 'value' => null,
                 'setOnEmpty' => true, 'on' => array('insert', 'update')),
            array('id','checkDelete', 'on' => ['delete']),
            ['order', 'checkOrder']
        );
    }
    
    public function checkDelete()
    {
        $cnt1 = $this->account_transactions_count;
        if ($cnt1>0)
        {
            $this->addError('id',"Nalog ima $cnt1 transakcija, ne moze biti obrisan!");
        }
        $cnt2 = $this->account_documents_cnt;
        $cnt2 -= ($this->is_account_document)?1:0;
        if ($cnt2>0)
        {
            $this->addError('id',"Nalog ima $cnt2 dokumenata za knjizenje, ne moze biti obrisan!");
        }
        
        $fileErrors = $this->file->canRecycle();
        if (is_array($fileErrors))
        {
            foreach ($fileErrors as $f_error)
            {
                $this->addError('id',$f_error);
            }
        }
    }
    
    public function uniqueSameYear()
    {
        $date = new DateTime($this->date);
        $year = Year::get($date->format("Y"));
        $ao = AccountOrder::model()->findByAttributes([
            'order' => $this->order,
            'year_id' => $year->id
        ]);
        if (isset($ao) && $ao->id!=$this->id)
        {
            $this->addError('order',"Broj '$this->order' već postoji u godini $year->year");
        }
    }
    
    public function checkSameYear()
    {
        if (!$this->isNewRecord)
        {
            $date1 = new DateTime($this->date);
            $date2 = new DateTime($this->__old['date']);
            $date1_str = $date1->format("Y");
            $date2_str = $date2->format("Y");
            if ($date1_str !== $date2_str)
            {
                $this->addError('date',"Datum mora da bude u istoj godini ($date2_str)");
            }
        }
    }
    
    public function checkBooked()
    {
        if (!$this->hasErrors())
        {
            if (!empty($this->id) && boolval($this->__old['booked']) !== boolval($this->booked))
            {
                if (boolval($this->booked) === true)
                {
                    if (!SIMAMisc::areEqual($this->saldo, 0))
                    {
                        $this->addError('booked',Yii::t('AccountingModule.AccountOrder','UnconfirmedSaldoNotZero'));
                    }
                    elseif ($this->account_transactions_count === 0)
                    {
                        $this->addError('booked',Yii::t('AccountingModule.AccountOrder','UnconfirmedNoTransactions'));
                    }
                    else//nema potrebe da proveravamo ako vec ima neka greska
                    {
                        //proveravamo da li su zapamcena odstupanja
                        $this->attachBehavior('ReCheck', 'AccountOrderReCheckBehavior');
                        try
                        {
                            if ($this->hasUnconfirmedDiffs())
                            {
                                $this->addError('booked', Yii::t('AccountingModule.AccountOrder','UnconfirmedBookingPrediction'));
                            }
                        }
                        catch (SIMAWarnException $e)
                        {
                            $this->addError('booked', $e->getMessage());
                        }
                    }
                    
                    //proveravamo da li su potvrdjeni svi dnevnici blagajne
                    foreach ($this->account_documents as $account_document)
                    {
                        if(isset($account_document->file->cash_desk_log) && $account_document->file->cash_desk_log->confirmed === false)
                        {
                            $this->addError('booked', Yii::t('AccountingModule.AccountOrder', 'UnconfirmedCashDeskLog'));
                            break;
                        }
                    }
                }
                elseif (boolval($this->booked) === false)
                {
                    if ($this->year->accounting->confirmed !== false) //razlicito je zbog tri jednakosti, da ne prodje nesto slucajno
                    {
                        $this->addError('booked',"Godina je potvrdjena, ne mozete skidati potvrdu naloga");
                    }
                }
            }
        }
    }
    
    public function checkOrder()
    {
        if (!$this->hasErrors())
        {
            if ($this->isOrderOnlyNumbers() && !ctype_digit($this->order))
            {
                $this->addError('order', Yii::t('AccountingModule.AccountOrder', 'OrderOnlyNumbers'));
            }
        }
    }
    
    public function scopes()
    {
        $alias = $this->getTableAlias();
        
        $order_by = "$alias.order ASC";
        if ($this->isOrderOnlyNumbers())
        {
            $order_by = "$alias.order::bigint ASC";
        }
        
        return array(
            'byName' => array(
                'order' => $order_by
            )
        );
    }
    
    //MIlosS(01.10.2018)ovo je samo za zajednicke forme
    //ali su one sad ugasene... neka za sad nek stoji kod, ali cemo videti jos sta cemo sa ovim
//    public function beforeMutualSave()
//    {
//          //MIloS(01.10.2018): ovaj if ima smisla kada nije zajednicka forma... ali onda se ovo unutra nikad nece otvoriti
////        if (isset($this->file))
////        {
//            $account_order_document_type_id = Yii::app()->configManager->get('accounting.account_order', false);
//            $this->file->document_type_id = $account_order_document_type_id;
//
//            $this->file->name = $this->calcFileName();
////        }
//        return parent::beforeMutualSave();
//    }
//    
    public function beforeSave()
    {
        $date = new DateTime($this->date);
        $_year = Year::get($date->format("Y"));
        $this->year_id = $_year->id;
        $this->year_display = $_year->year;
        
        $_month = Month::get($date->format("m"), $date->format("Y"));
        $this->month_id = $_month->id;
        $this->month_display = $_month->month;
        
        if ($this->isNewRecord)
        {
            $account_order_document_type_id = Yii::app()->configManager->get('accounting.account_order', false);
            $this->file = new File();
            $this->file->name = $this->calcFileName();
            $this->file->document_type_id = $account_order_document_type_id;
            $this->file->save();
            $this->id = $this->file->id;
        }
        
        return parent::beforeSave();
    }
    
    private function calcFileName()
    {
        $date = new DateTime($this->date);
        return $this->order.'/'.$date->format("Y");
    }
    
    public function afterSave()
    {
        parent::afterSave();
        //refreshj
        $account_order_document_type_id = Yii::app()->configManager->get('accounting.account_order', false);
        if (isset($this->file) && 
                (
                    $this->file->name !== $this->calcFileName()
                    ||
                    $this->file->document_type_id !== $account_order_document_type_id
                ) 
            )
        {
            $this->file->name = $this->calcFileName();
            $this->file->document_type_id = $account_order_document_type_id;
            $this->file->save(false);
        }
        
        if (!isset($this->file->account_document))
        {
            $this->file->account_document = new AccountDocument();
            $this->file->account_document->id = $this->id;
            $this->file->account_document->account_order_id = $this->id;
            $this->file->account_document->save(false);
        }
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters': return array(
                'id' => 'dropdown',
                'account_documents' => 'relation',
                'canceled_account_documents' => 'relation',
                'order' => ['text', 'filter_function' => 'orderFilterFunction'],
                'date' => 'date_range',
                'note' => 'text',
                'saldo' => ['dropdown','func'=>'filterSaldo'],
                'year' => 'relation',
                'month' => 'relation'
            );
            case 'textSearch': return array(
                'order' => 'integer',
                'note' => 'text'
            );
            case 'options': 
                if ($this->booked)
                {
                    return ['form','open'];
                }
                else
                {
                    return ['delete','form','open'];
                }
                
            case 'searchField' : return [
                'scopes' => ['byName']
            ];
            case 'multiselect_options':  return ['delete', 'download_ao_zip', 'statuses'=>['booked']];
            case 'default_order': return 'order';
            case 'update_relations': return ['account_documents'];
            case 'statuses': return [
                'booked' => [
                    'title' => Yii::t('AccountingModule.AccountOrder','Booked'),
                    'timestamp' => 'booked_timestamp',
                    'user' => array('booker_id', 'relName' => 'booker'),
                    'checkAccessRevert' => true,
                    'checkAccessConfirm' => 'checkConfirm',
                    'onConfirm' => 'onConfirmFunc',
                    'onRevert' => 'onRevertFunc'
                ],
            ];
            case 'number_fields': return ['debit', 'credit', 'saldo'];
            case 'mutual_forms': return ['weak_base_relation' => 'file'];
            default : return parent::modelSettings($column);
        }
    }
    
    public function orderFilterFunction(&$model_filter_value, $alias, $model_filter_key, $filter_type)
    {
        $criteria = null;
        if ($this->isOrderOnlyNumbers())
        {
            $uniq = SIMAHtml::uniqid();
            $criteria = new CDbCriteria();
            $criteria->condition = "$alias.order LIKE :param1$uniq || '%'";
            $criteria->params = [
                ":param1$uniq" => $model_filter_value
            ];
        }
        
        return $criteria;
    }
    
    public function onConfirmFunc()
    {
        $this->generatePdf($this->DisplayName.'_on_confirm');
        
        $this->confirmAllBillIn();
    }
    
    public function onRevertFunc()
    {
        if ($this->year->accounting->confirmed)
        {
            $this->year->accounting->confirmed = false;
            $this->year->accounting->save();
            Yii::app()->raiseNote('Godini '.$this->year->year.' je skinuta potvrda');
        }
        
        $this->unConfirmAllBillIn();
    }
    
    public function checkConfirm()
    {
        $errs = [];
        
        if (!SIMAMisc::areEqual($this->saldo, 0))
        {
            $errs[] = 'Saldo nije 0';
        }
        
        if (Yii::app()->configManager->get('accounting.bill_in_automatic_confirmation2', false))
        {
            //za svaki ulazni racun proknjizen na ovom nalogu proveravamo da li mozemo da izvrsimo potvrdu 2 
            //i ako ne mozemo bacamo gresku da ne moze i nalog da se potvrdi
            foreach ($this->account_documents as $account_document)
            {
                if  (
                        isset($account_document->file->bill_in) && 
                        !$account_document->file->bill->confirm2
                    )
                {
                    $account_document->file->bill_in->confirm2 = true;
                    if (!$account_document->file->bill_in->validate())
                    {
                        $errs[] = Yii::t('AccountingModule.AccountOrder', 'CanNotConfirmAccountOrderBecauseCanNotConfirm2ForBillIn', [
                            '{bill_in}' => $account_document->file->DisplayName,
                            '{validate_error_html}' => SIMAHtml::showErrorsInHTML($account_document->file->bill_in)
                        ]);
                        break;
                    }
                }
            }
        }
        
        if (count($errs)>0)
        {
            return $errs;
        }
        else
        {
            return true;
        }
    }
    
    public function filterSaldo($condition)
    {
        if (isset($this->saldo))
        {
            $alias = $this->getTableAlias();
            $uniq = SIMAHtml::uniqid();
            $criteria = null;
            switch ($this->saldo)
            {
                
                case 'diff0':
                    $criteria = new CDbCriteria();
                    $criteria->condition = "exists 
                        (
                            select 1 from accounting.account_transactions at
                                left join accounting.account_documents ad on at.account_document_id = ad.id

                                group by ad.account_order_id
                                having (sum(credit) - sum(debit))!=0 and ad.account_order_id = $alias.id
                        )";
//                    $criteria->params = array(
//                        ":order_id$uniq" => $this->id
//                    );
                    break;
            }
            if ($criteria!=null)
            {
                $condition->mergeWith($criteria);
            }
        }
    }
    
    public function beforeDelete()
    {
        //pravimo PDF da bi ostalo zapamceno kako je izbrisano
        $this->generatePdf($this->DisplayName.'_deleted_as');
        
        if (isset($this->file->account_document))
        {
            $this->file->account_document->delete();   
        }
        foreach ($this->account_documents as $doc)
        {
            $doc->delete();
        }
        return parent::beforeDelete();
    }
    
    public function afterDelete()
    {
        //u before delete se pravi PDF da bi se videlo kako je izbrisano
        parent::afterDelete();
        $this->file->recycle();
    }

    public function isOrderOnlyNumbers()
    {
        return SIMAMisc::filter_bool_var(Yii::app()->configManager->get('accounting.account_order_order_only_numbers', false));
    }
    
    public function confirmAllBillIn()
    {
        if (Yii::app()->configManager->get('accounting.bill_in_automatic_confirmation2', false))
        {
            foreach ($this->account_documents as $account_document)
            {
                if (
                        isset($account_document->file->bill_in) && 
                        $account_document->file->bill->confirm2 === false
                    )
                {
                    $account_document->file->bill_in->confirm2 = true;
                    $account_document->file->bill_in->confirm2_user_id = $this->booker_id;
                    $account_document->file->bill_in->save();
                }
            }
        }
    }

    public function unConfirmAllBillIn()
    {
        if (Yii::app()->configManager->get('accounting.bill_in_automatic_confirmation2', false))
        {
            foreach ($this->account_documents as $account_document)
            {
                if (
                        isset($account_document->file->bill_in) &&
                        $account_document->file->bill->confirm2 === true
                    )
                {
                    $account_document->file->bill_in->confirm2 = false;
                    $account_document->file->bill_in->confirm2_user_id = $this->booker_id;
                    $account_document->file->bill_in->save();
                }
            }
        }
    }
}