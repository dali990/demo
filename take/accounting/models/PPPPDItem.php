<?php

class PPPPDItem extends SIMAActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'accounting.ppppd_items';
    }

    public function moduleName()
    {
        return 'accounting';
    }
    
    public function __get($column)
    {
        switch ($column)
        {
            case 'DisplayName':
            case 'SearchName': return $this->indentification_value;
            default: return parent::__get($column);
        }
    }
    
    public function relations($child_relations = [])
    {
        return [
            'ppppd' => [self::BELONGS_TO, 'PPPPD', 'ppppd_id'],
        ];
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters' : return array_merge(parent::modelSettings('filters'),[
                'ppppd' => 'relation',
                'indentification_value' => 'text'
            ]);
            case 'options': return [];
            default: return parent::modelSettings($column);
        }
    }
    
    public function afterSave() 
    {
        parent::afterSave();
        
        $this->ppppd->dirty = true;
        $this->ppppd->save();
    }
    
    public function afterDelete()
    {
        parent::afterDelete();
        
        $this->ppppd->dirty = true;
        $this->ppppd->save();
    }
}
