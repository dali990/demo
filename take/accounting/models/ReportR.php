<?php

class ReportR extends Report
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    
    public function tableName()
    {
        return 'accounting.report_total_r';
    }
  
}
