<?php

class Report extends SIMAActiveRecord
{
    /**
     * sluzi za filter. pakuje se niz u kojim poljima mora da bude vrednosti da bi niz prosao
     * @var array 
     */
    public $has_value_on = [];
    
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'accounting.report_total';
    }

    public function moduleName()
    {
        return 'accounting';
    }
    
    public function getTableSchema()
    {
        $schema = new CDbTableSchema();
        $schema->primaryKey = 'id';
        $schema->name = $this->tableName();
        $schema->rawName = $this->tableName();
        $schema->sequenceName = NULL;
        
       
        
        $columns = [];
        foreach ([
            'cost_location_id',
            'cost_type_id',
            'partner_id',
            'month_id',
            'year_id'
            ] as $column_id)
        {
            $column_def = new CDbColumnSchema();
            $column_def->name = $column_id;
            $column_def->rawName = $column_id;
            $column_def->allowNull = false;
            $column_def->dbType = 'integer';
            $column_def->type = 'integer';
            
            $columns[$column_id] = $column_def;
        }
        
        foreach ([
            'income',
            'income_notpayed',
            'income_advance',
            'cost',
            'cost_notpayed',
            'cost_advance'
            ] as $column_id)
        {
            $columns[] = $column_id;
        }

        $model_name_def = new CDbColumnSchema();
        $model_name_def->name = 'model_name';
        $model_name_def->rawName = 'model_name';
        $model_name_def->allowNull = false;
        $model_name_def->dbType = 'TEXT';
        $model_name_def->type = 'string';
        $columns['model_name'] = $model_name_def;
        
        
        $model_id_def = new CDbColumnSchema();
        $model_id_def->name = 'model_id';
        $model_id_def->rawName = 'model_id';
        $model_id_def->allowNull = false;
        $model_id_def->dbType = 'TEXT';
        $model_id_def->type = 'string';
        $columns['model_id'] = $model_id_def;
        
        
        $report_type_def = new CDbColumnSchema();
        $report_type_def->name = 'report_type';
        $report_type_def->rawName = 'report_type';
        $report_type_def->allowNull = false;
        $report_type_def->dbType = 'TEXT';
        $report_type_def->type = 'string';
        $columns['report_type'] = $report_type_def;
        
        $schema->columns = $columns;
        
        return $schema;
    }

    public function __get($column)
    {
        switch ($column)
        {
            case 'id': return 1;
            default: return parent::__get($column);
        }
    }

//    public function inMonth($month,$year)
//    {
//        $alias = $this->getTableAlias();
//        $uniq = SIMAHtml::uniqid();
//        $criteria = new SIMADbCriteria();
//        $criteria->condition = "$alias.year = :year$uniq and $alias.month = :month$uniq";
//        $criteria->params = [
//            ":year$uniq" => $year,
//            ":month$uniq" => $month
//        ];
//        $this->getDbCriteria()->mergeWith($criteria);
//        return $this;
//    }
    
//    public function scopes()
//    {
//        $alias = $this->getTableAlias();
//        return array(
//            'byOrderInYear' => array(
//                'order' => "$alias.order_in_year",
//            ),
//            'BadVATBooked' => [
//                'condition' =>  "$alias.kpr_14 != $alias.kpr_14b"
//            ]
//        );
//    }
    
//    public function notInKPRManualInMonth($month_id)
//    {
//        $alias = $this->getTableAlias();
//        $kpr_manual_table = KPRManual::model()->tableName();
//        
//        $criteria = new SIMADbCriteria();
//        $criteria->condition = 
//                "not exists (select 1 from $kpr_manual_table where bill_id = $alias.bill_id "
//                . "and reason = $alias.reason "
//                . "and month_id = $month_id)";
//
//        $this->getDbCriteria()->mergeWith($criteria);
//        return $this;
//    }

    public function relations($child_relations = [])
    {
        return [
            'cost_location' => [self::BELONGS_TO,'CostLocation','cost_location_id'],
            'cost_type' => [self::BELONGS_TO,'CostType','cost_type_id'],
            'partner' => [self::BELONGS_TO,'Partner','partner_id'],
            'month' => [self::BELONGS_TO,'Month','month_id'],
            'year' => [self::BELONGS_TO,'Year','year_id'],
        ];
    }

    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'options' : return [];
            case 'filters': return [
                'cost_location' => 'relation',
                'cost_type' => 'relation',
                'partner' => 'relation',
                'month' => 'relation',
                'year' => 'relation',
                'report_type' => 'dropdown',
                'has_value_on' => ['func' => 'hasValueOnFilter']
            ];
            case 'number_fields': return [
                'income',
                'income_notpayed',
                'income_advance',
                'cost',
                'cost_notpayed',
                'cost_advance'
            ];
            default: return parent::modelSettings($column);
        }
    }

    public function hasValueOnFilter(SIMADbCriteria $condition, string $alias, string $column)
    {
        if (is_array($this->$column) && !empty($this->$column)) 
        {
            $_conditions = [];
            foreach ($this->$column as $_has_value_on)
            {
                if (in_array($_has_value_on, ['income','income_notpayed','income_advance','cost','cost_notpayed','cost_advance']))
                {
                    $_conditions[] = "$alias.$_has_value_on != 0";
                }
            }
            
            $temp_cond = new SIMADbCriteria();
            $temp_cond->condition = implode(' AND ', $_conditions);
            $condition->mergeWith($temp_cond);
        }
    }
    
    public function getUpdateModelTags()
    {
        $_model_array = [
            //SERVICES
            BillItem::model(),
            //CASH_DESK
            CashDeskItem::model(),
            //WAREHOUSE
            WarehouseTransferItem::model(),
            //SALARY
            AccountTransaction::model(),
            Paycheck::model()
            
        ];
        return SIMAMisc::getModelTags($_model_array);
    }
  
}
