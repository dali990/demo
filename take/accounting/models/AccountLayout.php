<?php

class AccountLayout extends SIMAActiveRecord
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'accounting.account_layouts';
    }

    public function moduleName()
    {
        return 'accounting';
    }

    public function __get($column)
    {
        switch ($column)
        {
            case 'DisplayName': return $this->code . '-' . $this->name; // . '(' . $this->number . (($this->comment != '') ? ' | ' : '') . $this->comment . ')';
            case 'SearchName': return $this->code . '-' . $this->name . (($this->description != '') ? '(' . $this->description . ')' : '');
            default: return parent::__get($column);
        }
    }

    public function relations($child_relations = [])
    {
        return array(
            'parent' => array(self::BELONGS_TO, 'AccountLayout', 'parent_id'),
            'law' => array(self::BELONGS_TO, 'AccountLayoutLaw', 'law_id'),
            'children'=>array(self::HAS_MANY, 'AccountLayout', 'parent_id'),            
//            'partner' => array(self::BELONGS_TO, 'Partner', 'partner_id'),
//            'bank_statements' => array(self::HAS_MANY, 'BankStatement', 'account_id'),
        );
    }

    public function rules()
    {
        return array(
            array('code, law_id', 'required'),
//            array('start_amount', 'numerical'),
            array('name, parent_id, description', 'safe'),
            array('code', 'checkCode', 'on' => array('insert', 'update')),
//            array('code', 'length', 'max'=>3, 'on'=>['insert','update']),
            array('parent_id, law_id', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => array('insert', 'update'))
        );
    }
    
    public function checkCode($attribute, $params) 
    {
        if ($this->__old['code'] != $this->code || $this->__old['law_id'] != $this->law_id)
        {
            $account_layout = AccountLayout::model()->findByAttributes(array('code'=>$this->code, 'law_id'=>$this->law_id));
            if  ($account_layout !== null) 
            {
                $this->addError('code', 'Već postoji konto '. $this->code .' za zakon '.$this->law.' !');
            }
        }
    }
    
    public function scopes()
    {
        return array(
            'byName' => array(
                'order' => 'code'
            )            
        );
    }

    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'textSearch': return [
                'name' => 'text',
                'code' => ['text','options'=>['wildcard']],
                'description' => 'text',
            ];
            case 'filters': return [
                'code' => ['text','options'=>['wildcard']],
                'law'=> 'relation',
                'law_id'=>'dropdown',
                'name' => 'text',
                'description' => 'text',
            ];
            case 'default_order' : return 'name';
            case 'searchField' : return [
                'scopes' => ['byName']
            ];
            default: return parent::modelSettings($column);
        }
        
    }
    
    public function afterSave()
    {        
        //kreira nadkonta ako ne postoje
        if ($this->__old['code'] != $this->code)
        {
            $code = substr($this->code, 0, -1);            
            if ($code !== '')
            {
                $account_layout = AccountLayout::model()->findByAttributes(array(
                    'code' => $code,
                    'law_id' => $this->law_id
                ));
                if ($account_layout === null)
                {                    
                    $account_layout = new AccountLayout();
                    $account_layout->code          = $code;
                    $account_layout->name          = $this->name;
                    $account_layout->law_id       = $this->law_id;
                    $account_layout->save();
                }
                $curr_account_layout = AccountLayout::model()->findByPk($this->id);
                $curr_account_layout->parent_id = $account_layout->id;
                $curr_account_layout->update();
            }
        }
        
        return parent::afterSave();
    }

//	public function getAttributeDisplay($column)
//	{
//		switch ($column)
//		{
//			case 'partner_id': 		return CHtml::link($this->partner->DisplayName,array('accounts/partner','id'=>$this->partner_id),array('target'=>'_blank'));
//			case 'bank_id':			return (isset($this->bank))?$this->bank->name:'?';
//			case 'start_amount':	return money_format('%!i', $this->$column);
//
//			default: return parent::getAttributeDisplay($column);
//		}
//	}
}