<?php

/**
 * @package SIMA
 * @subpackage Accounting
 */
class AccountTransaction extends SIMAActiveRecord
{
    public $saldo;    
    public $account_saldo;
    
    public $account_debit_display;
    public $account_credit_display;
    
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'accounting.account_transactions';
    }

    public function moduleName()
    {
        return 'accounting';
    }

    public function __get($column)
    {
        switch ($column)
        {
            case 'DisplayName': return $this->account_order_order;
            case 'SearchName': return $this->account_order_order;
            case 'isDebit': return abs($this->debit) > 0;
            case 'isCredit': return abs($this->credit) > 0;
            default: return parent::__get($column);
        }
    }

    public function relations($child_relations = [])
    {
        return array(
            'account' => array(self::BELONGS_TO, 'Account', 'account_id'),
            //sluzi samo za specijalan filter
            'account_booked' => array(self::BELONGS_TO, 'Account', 'account_id'),
            'account_debit' => array(self::BELONGS_TO, 'Account', 'account_id'),
            'account_credit' => array(self::BELONGS_TO, 'Account', 'account_id'),
            'account_document' => array(self::BELONGS_TO, 'AccountDocument', 'account_document_id'),
//            'payment' => [self::HAS_ONE, 'Payment', 'transaction_id'],
        );
    }

    public function rules()
    {
        return array(
            array('account_document_id, account_id', 'required'),
            array('debit, credit', 'safe'),
            array('debit, credit', 'numerical'),
            array('account_id, account_order_order, saldo, comment', 'safe'),
            array('debit', 'checkDebitCredit'),
            array('account_id', 'checkAccount'),
            ['id','checkDelete', 'on'=>['delete']],
//            array('account_order_order', 'checkAccountOrder', 'on' => array('insert', 'update')),
            ['account_order_order', 'numerical', 'integerOnly'=>true, 'min' => 1],
            array('account_id, account_document_id', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => array('insert', 'update')),
            array('debit, credit', 'default',
                'value' => '0',
                'setOnEmpty' => true, 'on' => array('insert', 'update'))
        );
    }

    public function checkDebitCredit($attribute, $params)
    {
        if (intval($this->debit) > 0 && intval($this->credit) > 0)
        {
            $this->addError('debit','Ne mogu i duguje i potražuje da budu veći od nule!');
        }
    }
    
//    public function checkAccountOrder($attribute, $params)
//    {
//        if (isset($this->account_order_order) && $this->account_order_order !== null && $this->account_order_order !== '')
//        {
//            $account_transaction = AccountTransaction::model()->findByAttributes([
//                'account_document_id'=>$this->account_document_id,
//                'account_order_order'=>$this->account_order_order
//            ]);
//            if ($account_transaction !== null && $this->id!=$account_transaction->id)
//            {
//                $this->addError('account_order_order','Već postoji transakcija sa ovim stavom i proknjiženim dokumentom!');
//            }
//        }
//    }
    
    public function beforeValidate()
    {
        if (!empty($this->account_id))
        {
            $this->parseAccount();
            $this->account = $this->account->getLeafAccount();
        }
        return parent::beforeValidate();
    }
    
    public function checkAccount()
    {
        if ( !$this->hasErrors() && !$this->account->isLeafAccount())
        {
            $code = $this->account->code;
            $this->addError('account_id',"Konto sa kodom $code nije predvidjen za knjizenje");
        }
    }
    
    public function checkDelete()
    {
        if (!isset($this->account_document) || $this->account_document->isBooked)
        {
            $this->addError('id', Yii::t('AccountingModule.AccountOrder','CannotDeleteConfirmedAccountTransaction'));
        }
    }
    
    public function scopes()
    {
//        $uniq = SIMAHtml::uniqid();
        $alias = $this->getTableAlias();     
//        $officialOrderJoin = "left join accounting.account_documents ad$uniq on $alias.account_document_id=ad$uniq.id "
//                . "left join accounting.account_orders ao$uniq on ad$uniq.account_order_id=ao$uniq.id ";
//        $officialOrder = "ao$uniq.datee, ao$uniq.order, $alias.account_order_order";
//        $officialSelect = "ao$uniq.date as fff, ao$uniq.order as ggg, $alias.account_order_order as ccc";
        
        
        return array(
            'byOrder' => array(
                'order' => $alias.'.account_order_order'
            ),
            //nema potrebe za ovim, to se radi u withSaldo
            'byOfficial' => [ 
//                'select' => $officialSelect,
//                'order' => $officialOrder,
//                'join' => $officialOrderJoin
            ],
            
        );
    }
    
    private function notVATAccountDocumentsBooking(Month $m)
    {
        $_acc_year = $m->year->accounting;
        $_acc_month = $m->accounting;
        $result = [];
        
        if (!empty($_acc_year->start_booking_order_id))
        {
            $result[] = $_acc_year->start_booking_order_id;
        }
        if (!empty($_acc_year->finish_booking_order_id))
        {
            $result[] = $_acc_year->finish_booking_order_id;
        }
        if (!empty($_acc_year->profit_calc_booking_order_id))
        {
            $result[] = $_acc_year->profit_calc_booking_order_id;
        }
        if (!empty($_acc_month->vat_account_document_id))
        {
            $result[] = $_acc_month->vat_account_document_id;
        }
        return !empty($result) ? $result : -1;
    }
    
    /**
     * 
     * @param type $account_id -- mora da se prosledi account_id jer je scope
     * @return \AccountTransaction
     */
    public function VATNotInKPR($month,$year)
    {
        $alias = $this->getTableAlias();
        $uniq = SIMAHtml::uniqid();
        $_m = Month::get($month, $year);
        
        $db_crit1 = new SIMADbCriteria([
            'Model' => AccountTransaction::model(),
            'model_filter' => [
                'AND',
                [
                    'account_document' => [
                        'account_order' => [
                            'month' => ['ids' => $_m->id]
                        ]
                    ],
                    'account_debit_display' => '27*',
                ],
                [
                    'NOT',
                    [
                        'account_document' => [
                            'ids' => $this->notVATAccountDocumentsBooking($_m)
                        ] 
                    ]
                ]
            ]
        ]);
        $db_crit2 = new SIMADbCriteria([
            'condition' => 
                //stavka nije u kpr
                "not exists (select 1 from accounting.kpr ak$uniq where ak$uniq.month_id = :month_id$uniq "
                . "AND ("
                    . " (ak$uniq.reason = 'REGULAR'::text  AND $alias.account_document_id = ak$uniq.bill_id) "
                    . " OR "
                    . " (ak$uniq.reason = 'CANCELED'::text AND $alias.account_document_id = (select canceled_account_order_id from accounting.account_documents where id = ak$uniq.bill_id) ) "
                . " )) ",
            'params' => [':month_id'.$uniq => $_m->id]
        ]);

        $this->getDbCriteria()->mergeWith($db_crit1);
        $this->getDbCriteria()->mergeWith($db_crit2);
        return $this;
    }
    /**
     * 
     * @param type $account_id -- mora da se prosledi account_id jer je scope
     * @return \AccountTransaction
     */
    public function VATNotInKIR($month,$year)
    {
        $alias = $this->getTableAlias();
        $uniq = SIMAHtml::uniqid();
        $_m = Month::get($month, $year);
        
        $db_crit1 = new SIMADbCriteria([
            'Model' => AccountTransaction::model(),
            'model_filter' => [
                'AND',
                [
                    'account_document' => [
                        'account_order' => [
                            'month' => ['ids' => $_m->id]
                        ]
                    ],
                    'account_credit_display' => '47*',
                ],
                [
                    'NOT',
                    [
                        'account_document' => [
                            'ids' => $this->notVATAccountDocumentsBooking($_m)
                        ] 
                    ]
                ]
            ]
        ]);
        $db_crit2 = new SIMADbCriteria([
            'condition' => 
                //stavka nije u kpr
                "not exists (select 1 from accounting.kir ak$uniq where ak$uniq.month_id = :month_id$uniq "
                . "AND ("
                    . " (ak$uniq.reason = 'REGULAR'::text  AND $alias.account_document_id = ak$uniq.bill_id) "
                    . " OR "
                    . " (ak$uniq.reason = 'CANCELED'::text AND $alias.account_document_id = (select canceled_account_order_id from accounting.account_documents where id = ak$uniq.bill_id) ) "
                . " )) ",
            'params' => [':month_id'.$uniq => $_m->id]
        ]);

        $this->getDbCriteria()->mergeWith($db_crit1);
        $this->getDbCriteria()->mergeWith($db_crit2);
        return $this;
    }
    
    /**
     * 
     * @param type $account_id -- mora da se prosledi account_id jer je scope
     * @return \AccountTransaction
     */
    public function withSaldoForAccount($account_id)
    {
        $alias = $this->getTableAlias();
        $uniq_alias = SIMAHtml::uniqid().'_alias';
        $this->getDbCriteria()->mergeWith(array(
            'select' => array(
                "$alias.*",
                "$uniq_alias.balance as account_saldo",
            ),
            'join' => "left join accounting.account_balance_listing($account_id) $uniq_alias "
                . "on $alias.id = $uniq_alias.transaction_id"
        ));
        return $this;
    }
    
    public function withSaldoAndNoStart()
    {
        $alias = $this->getTableAlias();
        $this->getDbCriteria()->mergeWith(array(
            'select' => array(
                        "$alias.*",
                        "(sum(debit) OVER (ORDER BY account_order.date, account_order.order, $alias.account_order_order))"
                        . " - "
                        . "(sum(credit) OVER (ORDER BY account_order.date, account_order.order, $alias.account_order_order))"
                        . " as saldo",
                ),
                'with'=>array('account_document.account_order'),
                'order' => 'account_order."date", account_order."order", '.$alias.'.account_order_order'
        ));
        return $this;
    }
    
    public function modelOptions(\User $user = null): array 
    {
        if (isset($this->account_document) && !$this->account_document->isBooked)
        {
            return ['form', 'delete'];
        }
        else
        {
            return [];
        }
    }
  
    public function modelSettings($column)
    {
        switch ($column)
        {   
            case 'filters' : return array(
                'account_document' => 'relation',
//                'account_document_id' => 'dropdown',
                'account' => array('relation'),         
                //sluzi samo za specijalan filter
                'account_booked' => array('relation','func'=>'filterAccount'),               
                //naredna dva moraju da budu tu zbog pretrage u inline
                'account_debit' => array('relation','func'=>'filterAccount'),
                'account_credit' => array('relation','func'=>'filterAccount'),
                'account_debit_display' => ['text','func'=>'textFilterAccount'],
                'account_credit_display' => ['text','func'=>'textFilterAccount'],
                'comment' => 'text',
                'account_order_order' => ['integer','func' => 'filterFullMatch']
            );
            case 'textSearch' : return array(
                'account' => 'relation',
            );
            case 'number_fields' : return array(
                'debit', 'credit', 'saldo'
            );
//            case 'update_relations': return ['account_document'];
            case 'multiselect_options':  
                if (isset($this->account_document) && !$this->account_document->isBooked)
                {
                    return ['delete'];
                }
                else
                {
                    return [];
                }
            default : return parent::modelSettings($column);
        }
    }
    
    
    /**
     * Specijalan filter koji izlistava sve transakcije proknjizene na kontu, 
     * ali tako da se podrazumevaju i sva pod konta. Poenta je da se ovo koristi u kartici konta
     * OVDE NE RADI REKURZIVNO POZIVANJE
     * MILoS: pokusao sam da napravim, ali je dosta kompolikovano
     *        za to treba da se uradi da se od svih nadjenih konta poziva code ~ '^$account_code'
     *        sto bi bio haos
     * @param type $criteria
     * @param type $alias
     * @param type $column
     */
    public function filterAccount($criteria, $alias, $column)
    {   
        $_ids = null;
        if (isset($this->$column))
        {
            $_ids = $this->$column->ids;
        }
        if (isset($_ids))
        {            
            $account = Account::model()->findByPk($_ids);
            if ($account !== null)
            {
                $local_crit = new SIMADbCriteria();
                $analitic_size = Yii::app()->params['accounting_account_analitic_size'];
                $uniqid = SIMAHtml::uniqid();
                
                if (strlen($account->code)>$analitic_size) //konta sa vezama - analitika
                {
                    $local_crit->condition = 
                        $alias.".account_id = :account_id$uniqid";
                    $local_crit->params = [
                        "account_id$uniqid" => $account->id
                    ];
                }
                else
                {
                    $account_code = $account->code;
                    $local_crit->condition = 
                        $alias.".account_id in (select id from accounting.accounts where code ~ '^$account_code' and year_id=$account->year_id)";
                }
                $criteria->mergeWith($local_crit);
            }
        }
        
    }
    
    public function textFilterAccount(SIMADbCriteria $criteria, $alias, $column)
    {
        if (!empty($this->$column))
        {
            $addition_cond = '';
            if (strpos($column, 'debit') !== FALSE)
            {
                $addition_cond = "$alias.debit != 0";
            }
            elseif (strpos($column, 'credit') !== FALSE)
            {
                $addition_cond = "$alias.credit != 0";
            }
            
            $temp_crit = new SIMADbCriteria([
                'Model' => AccountTransaction::class,
                'alias' => $alias,
                'model_filter' => [
                    'account' => [
                        'code' => $this->$column
                    ]
                ],
                'condition' => $addition_cond
            ]);
            $criteria->mergeWith($temp_crit);
        }
    }            

    public function beforeSave()
    {
        if ($this->columnChangedOrNew('account_id'))
        {    
            $old_account = null;
            if(!$this->isNewRecord)
            {
                $old_account = Account::model()->findByPk($this->__old['account_id']); 
            }        
            if ($old_account!=null 
                    && $old_account->model_name!='' && $old_account->model_name!=null 
                    && ($this->account->model_name=='' || $this->account->model_name==null) )
            {   //kada novi account nema model, a stari ima
                $new_account = $this->account->getLeafAccount($old_account->LinkedModel);
                $this->account_id = $new_account->id;
            }
            else
            {
                $new_account = $this->account->getLeafAccount($this->account->LinkedModel);
                $this->account_id = $new_account->id;
            }
        }
        
        if (!empty($this->account_order_order) && $this->columnChangedOrNew('account_order_order'))
        {
            $criteria = new SIMADbCriteria([
                'Model' => 'AccountTransaction',
                'model_filter' => [
                    'account_document' => [
                        'account_order' => ['ids' => $this->account_document->account_order_id]
                    ],
                    'account_order_order'=>$this->account_order_order
                ]
            ]);
            $account_transactions = AccountTransaction::model()->findAll($criteria);
            $diff = 1;
            foreach ($account_transactions as $account_transaction)
            {
                $account_transaction->account_order_order += $diff;
                $account_transaction->save();
                $diff++;
            }
        }
        
        return parent::beforeSave();
    }
    
    public function removeLink()
    {
        $_acc = $this->account->getLeafAccount(null,true);
        $this->account_id = $_acc->id;
        $this->account = $_acc;
        $this->__old['account_id'] = null;
    }
    public function forceAccount(Account $forced_account)
    {
        $this->account_id = $forced_account->id;
        $this->account = $forced_account;
        $this->__old['account_id'] = null;
    }
    
    private function parseAccount()
    {
        if (strpos($this->account_id,'l_') !== false) 
        {
            $arr = explode("l_", $this->account_id);
            $account = Account::getAccountByLayout($arr[0], $arr[1]);
            $this->account = $account;
            $this->account_id = $account->id;
        }
    }
    
    public function addTransaction(AccountTransaction $_at)
    {
        if (
            $this->account_id === $_at->account_id
            &&
            $this->comment === $_at->comment
            &&
            ( $this->debit>0 && $_at->debit>0 ) || ( $this->credit>0 && $_at->credit>0 )

            )
        {
            $this->debit += $_at->debit;
            $this->credit += $_at->credit;
            return true;
        }
        else
        {
            return false;
        }
    }
    
    public function upToDateFilter($date) //, $partner_id, $year_id
    {
        $date_db = SIMAHtml::UserToDbDate($date);
        $year = Date('Y',strtotime($date));
        $begin_date = $year.'-01-01';
        
        $alias = $this->getTableAlias();
        $criteria = new SIMADbCriteria();
        $criteria->condition = "$alias.account_document_id in (
	select id from accounting.account_documents where account_order_id in (
		select id from accounting.account_orders where date BETWEEN '$begin_date' AND '$date_db'))";
        
        $this->getDbCriteria()->mergeWith($criteria);
        return $this;
    }
}
