<?php 

class KIR extends SIMAActiveRecord
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
 
    public function tableName()
    {
        return 'accounting.kir';
    }

    public function moduleName()
    {
        return 'accounting';
    }
    
    public function getTableSchema()
    {
        $schema = new CDbTableSchema();
        $schema->primaryKey = 'id';
        $schema->name = 'accounting.kir';
        $schema->rawName = 'accounting.kir';
        $schema->sequenceName = NULL;
        
        $bill_id = new CDbColumnSchema();
        $bill_id->name = 'bill_id';
        $bill_id->rawName = 'bill_id';
        $bill_id->allowNull = false;
        $bill_id->dbType = 'integer';
        $bill_id->type = 'integer';
        
        $month_id = new CDbColumnSchema();
        $month_id->name = 'month_id';
        $month_id->rawName = 'month_id';
        $month_id->allowNull = false;
        $month_id->dbType = 'integer';
        $month_id->type = 'integer';
        
        $reason = new CDbColumnSchema();
        $reason->name = 'reason';
        $reason->rawName = 'reason';
        $reason->allowNull = false;
        $reason->dbType = 'TEXT';
        $reason->type = 'string';
        
        $schema->columns = [
            'id',
            'bill_id' => $bill_id,
            'month_id' => $month_id,
            'reason' => $reason,
            'order_in_year',
            'kir_7',
            'kir_8',
            'kir_9',
            'kir_10',
            'kir_11',
            'kir_12',
            'kir_13',
            'kir_14',
            'kir_15',
            'kir_16',
            'kir_17',
            'kir_18',
        ];
        
        return $schema;
    }
    
    private static $_show_income = null;
    public static function showIncome()
    {
        if (is_null(self::$_show_income))
        {
            self::$_show_income = Yii::app()->configManager->get('accounting.show_income_in_kir');
        }
        return self::$_show_income;
    }
    
    public function __get($column)
    {
        switch ($column)
        {	
            case 'KIR_7': return $this->kir_7;
            case 'KIR_8': return $this->kir_8;
            case 'KIR_9': return $this->kir_9;
            case 'KIR_10': return $this->kir_10;
            case 'KIR_11': return $this->kir_11;
            case 'KIR_12': return KIR::showIncome()?$this->kir_18:$this->kir_12;
            case 'KIR_13': return $this->kir_13;
            case 'KIR_13b': return $this->kir_13b;
            case 'KIR_14': return $this->kir_14;
            case 'KIR_15': return $this->kir_15;
            case 'KIR_15b': return $this->kir_15b;
            case 'KIR_16': return $this->kir_8 + $this->kir_9 + $this->kir_10 + $this->kir_11 + $this->kir_12 + $this->kir_14;
            case 'KIR_17': return $this->kir_8 + $this->kir_10 + $this->kir_12 + $this->kir_14;
            case 'KIR_18': return KIR::showIncome()?$this->kir_12:$this->kir_18;
            default: 			return parent::__get($column);
        }
    
    }
    
    public function inMonth($month,$year)
    {
        $alias = $this->getTableAlias();
        $uniq = SIMAHtml::uniqid();
        $criteria = new SIMADbCriteria();
        $criteria->condition = "$alias.year = :year$uniq and $alias.month = :month$uniq";
        $criteria->params = [
            ":year$uniq" => $year,
            ":month$uniq" => $month
        ];
        $this->getDbCriteria()->mergeWith($criteria);
        return $this;
    }
    
    public function relations($child_relations = [])
    {
        return [
            'bill' => [self::BELONGS_TO,'AnyBill','bill_id']
        ];
    }

    public function notInKIRManualInMonth($month_id)
    {
        $alias = $this->getTableAlias();
        $kir_manual_table = KIRManual::model()->tableName();
        
        $criteria = new SIMADbCriteria();
        $criteria->condition = 
                "not exists (select 1 from $kir_manual_table where bill_id = $alias.bill_id "
                . "and reason = $alias.reason "
                . "and month_id = $month_id)";

        $this->getDbCriteria()->mergeWith($criteria);
        return $this;
    }

    public function scopes()
    {
        $alias = $this->getTableAlias();
        return array(
            'byOrderInYear' => array(
                'order' => "$alias.order_in_year",
            )
        );
    }

    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'options' : return [];
                case 'number_fields': return [
                'KIR_7',
                'KIR_8',
                'KIR_9',
                'KIR_10',
                'KIR_11',
                'KIR_12',
                'KIR_13',
                'KIR_14',
                'KIR_15',
                'KIR_16',
                'KIR_17',
                'KIR_18'
            ];
            default: return parent::modelSettings($column);
        }
    }
    
    public function getClasses()
    {
        return parent::getClasses().$this->bill->getVATClasses();
    }
	
}