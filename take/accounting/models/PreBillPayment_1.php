<?php


class PreBillPayment extends SIMAActiveRecord
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'accounting.pre_bill_payments';
    }
    
    public function moduleName()
    {
        return 'accounting';
    }

    public function relations($child_relations = [])
    {
        return array(
            'pre_bill' => array(self::BELONGS_TO, 'PreBill', 'pre_bill_id'),
            'payment' => array(self::BELONGS_TO, 'Payment', 'payment_id')
        );
    }

    public function rules()
    {
        return array(
            array('pre_bill_id, payment_id', 'required'),
            array('pre_bill_id, payment_id', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => array('insert', 'update'))
        );
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters':return [
                'pre_bill' => 'relation',
                'payment' => 'relation'
            ];
            case 'update_relations' : return ['pre_bill','payment'];
            default: return parent::modelSettings($column);
        }
        
    }
   
}
