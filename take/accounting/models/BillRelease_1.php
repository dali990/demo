<?php

class BillRelease extends SIMAActiveRecord
{

//    public $amount_sum;
//    public $vat_sum;
    public $old_amount = 0;
    
    public static $STATUS_PRETIME = 'PRETIME';
    public static $STATUS_ONTIME  = 'ONTIME';
    public static $STATUS_LATE    = 'LATE';
    public static $STATUS_TODAY   = 'TODAY';
    public static $STATUS_ONDAY   = 'ONDAY';

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'accounting.bill_releases';
    }

    public function moduleName()
    {
        return 'accounting';
    }
    
    public function __get($column)
    {
        switch ($column)
        {
//            case 'DisplayName': return $this->amount.'('.$this->payment->payment_date.')';
            case 'DisplayName': return $this->payment->payment_date.' - '.$this->amount;
            default: return parent::__get($column);
        }
        
    }
    
    public function relations($child_relations = [])
    {
        return array(
            'bill' => array(self::BELONGS_TO, 'Bill', 'bill_id'),
            'payment' => array(self::BELONGS_TO, 'Payment', 'payment_id')
        );
    }

    public function rules()
    {
        return array(
            array('bill_id, payment_id, amount', 'required', 'message' => 'Polje "{attribute}" mora biti uneto'),
            array('amount', 'numerical', 'message' => 'Polje "{attribute}" mora biti broj!'),
            ['id','checkBillPaymentCompatibility'],
            array('exchange_rate_difference, other_difference', 'safe'),
            array('amount', 'amountCheck','on'=>['insert','update']),
            array('bill_id, payment_id', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => array('insert', 'update'))
        );
    }
    
    public function checkBillPaymentCompatibility()
    {
        if ($this->hasErrors())
        {
            return;
        }
        $bill_is_recurring = !empty($this->bill->recurring_bill_id);
        $payment_is_recurring = !empty($this->payment->recurring_bill_id);
        if (!$bill_is_recurring && !$payment_is_recurring)
        {
            if ($this->bill->partner_id != $this->payment->partner_id)
            {
                $this->addError('bill_id', Yii::t('AccountingModule.BillRelease','NotCompatiblePartner'));
                $this->addError('payment_id', Yii::t('AccountingModule.BillRelease','NotCompatiblePartner'));
            }
            if ($this->bill->invoice != $this->payment->invoice)
            {
                $this->addError('bill_id', Yii::t('AccountingModule.BillRelease','NotCompatibleInvoice'));
                $this->addError('payment_id', Yii::t('AccountingModule.BillRelease','NotCompatibleInvoice'));
            }
        }
        elseif(
            ! ($bill_is_recurring && $payment_is_recurring && $this->bill->recurring_bill_id == $this->payment->recurring_bill_id)
            )
        {
            $this->addError('bill_id', Yii::t('AccountingModule.BillRelease','NotCompatibleRecurringBill'));
            $this->addError('payment_id', Yii::t('AccountingModule.BillRelease','NotCompatibleRecurringBill'));
        }
        
    }

    public function amountCheck($attribute, $params)
    {
        if (!$this->hasErrors())
        {
            $bill = Bill::model()->findByPk($this->bill_id);
            $payment = Payment::model()->findByPk($this->payment_id);
            
            $advance_payment_type = Yii::app()->configManager->get('accounting.advance_payment_type');
//            $amount_diff = floatval($this->amount)-floatval($this->__old['amount']);
            
            $old_amount = 0;
            $old_exchange_rate_difference = 0;
            $old_other_difference = 0;
            if(!$this->isNewRecord)
            {
                $old_amount = $this->__old['amount'];
                $old_exchange_rate_difference = $this->__old['exchange_rate_difference'];
                $old_other_difference = $this->__old['other_difference'];
            }
            $amount_diff = floatval($this->amount)-$old_amount;

            $amount_diff_payment = 
                    floatval($this->amount) + floatval($this->exchange_rate_difference) + floatval($this->other_difference)
                    -
//                    (floatval($this->__old['amount']) + floatval($this->__old['exchange_rate_difference']) + floatval($this->__old['other_difference']));
                    (floatval($old_amount) + floatval($old_exchange_rate_difference) + floatval($old_other_difference));
            if ($bill->isAdvanceBill && $payment->payment_type_id == $advance_payment_type)
            {    
                $unreleased_bill_amount = $this->bill->amount - $this->bill->released_sum;
            }
            else
            {
                $unreleased_bill_amount = $bill->unreleased_amount;
            }
            if (($unreleased_bill_amount + 0.001) < $amount_diff)
            {
                $this->addError('amount', "nerasknjizeni deo racuna nije dovoljno velik");
            }
            if (($payment->unreleased_amount  + 0.001) < $amount_diff_payment)
            {
                $this->addError('amount', 'nerasknjizeni deo placanja nije dovoljno velik');
            }
        }
    }
    
    public function modelSettings($column)
    {
        $options = ['delete','form'];
        
        switch ($column)
        {
            case 'filters':return [
                'bill' => 'relation',
                'payment' => 'relation'
            ];
            case 'number_fields': return [
                'amount', 'exchange_rate_difference','other_difference', 'amount_in_currency'
            ];
            case 'options': return $options;
            case 'update_relations' : return ['bill','payment'];
            case 'multiselect_options': return ['delete'];
            default: return parent::modelSettings($column);
        }
        
    }
    
    public function beforeSave()
    {
        //automatsko maksimalno rasknjizavanje
        if (empty($this->amount))
        {
            $currency_corrector = 1;
            if ($this->bill->isSetMiddleCurrencyRateForBoundCurrency())
            {
                $bill_day = Day::getByDate($this->bill->income_date);
                $bill_currency = MiddleCurrencyRate::get($this->bill->bound_currency, $bill_day);
                $payment_day = Day::getByDate($this->payment->payment_date);
                $payment_currency = MiddleCurrencyRate::get($this->bill->bound_currency, $payment_day);
                $currency_corrector = $bill_currency->value / $payment_currency->value;
            }
            //koliko je poguce rasknjiziti = koliko je preostalo(UNreleased) + koliko je bilo
            $_can_release = round($this->payment->unreleased_amount * $currency_corrector, 2);
//            $_new_payment_un = $_can_release + $this->__old['amount'];
            
            $old_amount = 0;
            if(!$this->isNewRecord)
            {
                $old_amount = $this->__old['amount'];
            }
            $_new_payment_un = $_can_release + $old_amount;
            
            $currency_corr = $this->payment->unreleased_amount - $_can_release;

            if ($this->bill->isAdvanceBill)
            {   
//                $_new_bill_un = ($this->bill->amount - $this->bill->released_sum) + $this->__old['amount'];
                $_new_bill_un = ($this->bill->amount - $this->bill->released_sum) + $old_amount;
            }
            else
            {
                //koliko NIJE rasknjizeno + koliko je do sada rasknjizeno
//                $_new_bill_un = $this->bill->unreleased_amount + $this->__old['amount'];
                $_new_bill_un = $this->bill->unreleased_amount + $old_amount;
            }

            if ($_new_payment_un < $_new_bill_un)
            {
                $this->amount = $_new_payment_un;
                $this->exchange_rate_difference = $currency_corr;
            }
            else
            {
                $this->amount = $_new_bill_un;
                $this->exchange_rate_difference = $currency_corr;
            }
        }

        return parent::beforeSave();
    }
    
    
    public function calcStatus()
    {
        $status='NOT_CALCED';
//        $days_diff = 0;
        $income_date = new DateTime($this->bill->income_date);
        
        $payment_date = new DateTime($this->payment->payment_date);
        if (empty($this->bill->payment_date))
        {
            $due_date = new DateTime();
        }
        else
        {
            $due_date = new DateTime($this->bill->payment_date);
        }
        
//        if (isset($this->bill->abbill))
//        {
//            //OVO NE RADI ZA RACUNE KOJI NISU GENERISANI U AB modulu, sto je prvih nekoliko u SET
//            $status = 'ABBILL';
//        }
//        else
//        {
            $days_diff = $payment_date->diff($due_date)->days;
            if ($days_diff > 0)
            {
                if ($payment_date < $income_date)
                {
                    $status = self::$STATUS_PRETIME;
                }
                elseif ($payment_date < $due_date)
                {
                    $status = self::$STATUS_ONTIME;
                }
                else
                {
                    $status = self::$STATUS_LATE;
                }
            }
            else
            {
                $status = self::$STATUS_ONDAY;
            }
//        }
        return [
            'status' => $status,
            'days_diff' => $days_diff,
        ];
    }
}
