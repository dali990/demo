<?php

class BankAccount extends SIMAActiveRecord
{

    public $amount_sum;
    public $number_clean;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'accounting.bank_accounts';
    }

    public function moduleName()
    {
        return 'accounting';
    }

    public function __get($column)
    {
        switch ($column)
        {
            case 'DisplayName': 
                $bank = '';
                $number = '';
                $comment = '';
                if(isset($this->bank))
                {
                    $bank = $this->bank->DisplayName;
                }
                if(isset($this->number))
                {
                    $number = $this->number;
                }
                if(isset($this->comment) && !empty($this->comment))
                {
                    $comment = ' | '.$this->comment;
                }
                return $bank.'('.$number.$comment.')';
            case 'SearchName': return $this->DisplayName;
            default: return parent::__get($column);
        }
    }

    public function relations($child_relations = [])
    {
        return array(
            'bank' => array(self::BELONGS_TO, 'Bank', 'bank_id'),
            'currency' => array(self::BELONGS_TO, 'Currency', 'currency_id'),
            'partner' => array(self::BELONGS_TO, 'Partner', 'partner_id'),
            'bank_statements' => array(self::HAS_MANY, 'BankStatement', 'account_id'),
            'last_bank_statement' => array(self::HAS_ONE, 'BankStatement', 'account_id',
                'alias' => 'lastbankstatementinbankaccount',
                'order' => 'lastbankstatementinbankaccount."date" ASC'
            ),
        );
    }

    public function rules()
    {
        return array(
            array('bank_id, start_amount, number, partner_id, currency_id', 'required'),
            array('start_amount', 'numerical', 'message' => 'Polje "{attribute}" mora biti broj!'),
            array('comment, first_account_order', 'safe'),
            array('first_account_order','numerical', 'max' => 999999999),
            array('bank_id, partner_id', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => array('insert', 'update')),
            array('number', 'numberCheckPrefix'),
            array('number', 'numberCheckFormat'),
            ['number', 'uniqueCheck', 'on' => ['insert', 'update']]
        );
    }
    
    public function numberCheckPrefix() 
    {
        if(isset($this->bank_id) && isset($this->number))
        {
            $bank_prefix = str_replace('*', '', $this->bank->bank_account_code);
            $number_prefix = substr( $this->number, 0, strlen($bank_prefix) );
            
            if($number_prefix !== $bank_prefix)
            {
                $this->addError('number', Yii::t('AccountingModule.BankAccount', 'BankPrefixMismatch',['{PREFIX}' => $bank_prefix]));
            }
        }
    }
    
    public function numberCheckFormat() 
    {
        if(isset($this->number))
        {
            if (preg_match("/^\d{3}\-?\d{1,13}\-?\d{2}$/", $this->number) === 0)
            {
                $this->addError('number', Yii::t('AccountingModule.BankAccount', 'BankNumberInvalidFormat'));
            }
        }
    }
    
    public function uniqueCheck()
    {
        if(empty($this->number))
        {
            return;
        }
        
        /**
         * mora da se koristi SIMASQLFunctions::getBankAccountIdByNumber
         * jer se moze proslediti broj 15500000000000000
         * koji se u bazi cuva kao 155-000000000000-00
         * i jednostavnom pretragom se ne bi skontala slicnost
         */
        
        $bank_account_id = null;
        try
        {
            $bank_account_id = SIMASQLFunctions::getBankAccountIdByNumber($this->number);
        }
        catch (SIMAExceptionBankAccountNumberInvalid $ex)
        {
        }
        
        $unique_passes = true;
        if($this->isNewRecord)
        {
            if(!empty($bank_account_id))
            {
                $unique_passes = false;
            }
        }
        else
        {
            $old_bank_account_id = SIMASQLFunctions::getBankAccountIdByNumber($this->__old['number']);
            if($old_bank_account_id !== $bank_account_id && !empty($bank_account_id))
            {
                $unique_passes = false;
            }
        }
        
        if($unique_passes !== true)
        {
            $this->addError('number', Yii::t('AccountingModule.BankAccount', 'BankNumberAlreadyExists'));
        }
    }
    
    public function scopes()
    {
        $alias = $this->getTableAlias();
        $bank_accounts_table = BankAccount::model()->tableName();
        $uniq = SIMAHtml::uniqid();
        
        return [
            'byNumberAsc' => [
                'order' => "$alias.number ASC"
            ],
            'onlyDuplicated' => [
                'condition' => "$alias.number in (
                    select ba$uniq.number
                    from $bank_accounts_table ba$uniq
                    group by ba$uniq.number
                    having count(*) > 1
                )"
            ],
            'onlyDistinctDuplicated' => [
                'select' => "$alias.number",
                'group' => "$alias.number",
                'having' => "count(*) > 1"
            ]
        ];
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters' : return array_merge(parent::modelSettings('filters'),array(
                'partner_id' => 'dropdown',
                'partner' => 'relation',
                'bank' => 'relation',
                'currency' => 'relation',
                'number' => 'text',
                'number_clean' => ['func' => 'filter_number_clean'],
                'start_amount' => 'numeric',
                'comment' => 'text',
                'first_account_order' => 'integer'
            ));
            case 'textSearch' : return array(
                'number' => 'text',
                'bank' => 'relation'
            );
            case 'options': return array('delete', 'form');
            case 'multiselect_options':  return ['delete'];
            default: return parent::modelSettings($column);
        }
    }
    
    public function filter_number_clean($condition)
    {
        if(empty($this->number_clean))
        {
            return;
        }
        
        $temp_cond = new CDbCriteria();
        $alias = $this->getTableAlias();
        $uniq = SIMAHtml::uniqid();
        $temp_cond->condition = "$alias.number=clean_account(:number$uniq)";
        $temp_cond->params = [
            ":number$uniq" => $this->number_clean
        ];
        $condition->mergeWith($temp_cond);
    }
    
    public function forPartner($partner)
    {
        $this->getDbCriteria()->mergeWith(array(
            'condition' => 'partner_id=' . $partner,
        ));
        return $this;
    }
    
//	public function attributeLabels()
//	{
//		return array(
//			'bank_id'=>'Banka',
//			'number'=>'Broj računa',
//			'start_amount'=>'Početno stanje',
//			'comment'=>'Komentar',
//		);
//	}

//    public function getIdByNumber($number)
//    {
//        if ($number=='')
//        {
//            return null;
//        }
////        $sql = "select * from web_sima.get_account_id_by_number('$number');"; 
////        $data = Yii::app()->db->createCommand($sql)->queryAll();
////        $account_id=$data[0]['get_account_id_by_number'];
//        $account_id = SIMASQLFunctions::getBankAccountIdByNumber($number);
//        return $account_id;
//    }
    
    public function beforeSave()
    {
        $this->number = trim($this->number);
        
        return parent::beforeSave();
    }
    
    public function afterSave()
    {
        parent::afterSave();
        
        if ($this->isNewRecord)
        {
            Yii::app()->warnManager->sendWarns('ResolveDuplicatedBankAccounts', 'WARN_DUPLICATED_BANK_ACCOUNTS');
        }
    }
    
    public function afterDelete()
    {
        parent::afterDelete();
        
        Yii::app()->warnManager->sendWarns('ResolveDuplicatedBankAccounts', 'WARN_DUPLICATED_BANK_ACCOUNTS');
    }
}
