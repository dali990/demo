<?php

class CostTypeToAccount extends SIMAActiveRecord
{
    public $_year_id = null;
    public $_cost_type_name = null;
    public $_cost_type_book_by_cost_location = null;
    
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
 
    public function tableName()
    {
        return 'accounting.cost_types_to_accounts';
    }
    
    public function moduleName()
    {
        return 'accounting';
    }
    
    public function __get($column)
    {
        switch ($column)
        {
            case 'DisplayName': return $this->cost_type->DisplayName.'('.$this->account->DisplayName.')';
            case 'SearchName': return $this->DisplayName;
            case 'year_id': return $this->_year_id;
            case 'cost_type_name': return $this->_cost_type_name;
            case 'cost_type_book_by_cost_location': return $this->_cost_type_book_by_cost_location;
            default: return parent::__get($column);
        }
    }
    
    public function __set($name, $value)
    {           
        switch ($name)
        {
            case 'year_id': $this->_year_id = $value; break;            
            case 'cost_type_name': $this->_cost_type_name = $value; break;            
            case 'cost_type_book_by_cost_location': $this->_cost_type_book_by_cost_location = $value; break;            
            default: parent::__set($name, $value); break;
        }
    }
    
    public function __isset($column)
    {
        switch ($column)
        {
            case 'year_id': return $this->_year_id!==null;
            case 'cost_type_name': return $this->_cost_type_name!==null;
            case 'cost_type_book_by_cost_location': return $this->_cost_type_book_by_cost_location!==null;
            default: return parent::__isset($column);
        }
    }
    
    public function afterFind()
    {
        parent::afterFind();
        if (isset($this->account))
        {
            $this->year_id = $this->account->year_id;
            $this->__old['year_id'] = $this->year_id;
        }
        if (isset($this->cost_type))
        {
            $this->cost_type_name = $this->cost_type->name;
            $this->__old['cost_type_name'] = $this->cost_type_name;
            $this->cost_type_book_by_cost_location = $this->cost_type->book_by_cost_location;
            $this->__old['cost_type_book_by_cost_location'] = $this->cost_type_name;
        }
    }
    
    public function rules()
    {
        return array(
            array('cost_type_id, account_id, year_id', 'required'),   
            ['cost_type_name, cost_type_book_by_cost_location','safe'],
            array('cost_type_id, account_id','default',
                'value'=>null,
                'setOnEmpty'=>true,'on'=>array('insert','update')
            ),
            ['cost_type_id', 'checkCostType', 'on' => ['insert', 'update']]
        );
    }
    
    public function relations($child_relations = []) 
    {
        return array(
            'cost_type' => array(self::BELONGS_TO, 'CostType', 'cost_type_id'),
            'account' => array(self::BELONGS_TO, 'Account', 'account_id'),
            'year' => array(self::BELONGS_TO, 'Year', 'year_id'),
        );
    }
    
    public function modelSettings($column)
    { 
        switch ($column)
        {
            case 'filters' : return array_merge(parent::modelSettings($column),array(
                'cost_type' => 'relation',
                'account' => 'relation'                
            ));            
            default: return parent::modelSettings($column);
        }
    }
    
    public function checkCostType()
    {
        if (!$this->hasErrors())
        {
            $this->setAccountIfIsLayout();
            $leaf_account = $this->account->getLeafAccount();        
//            if (intval($this->account_id) !== $leaf_account->id)
//            {
//                $this->account = $leaf_account;
//                $this->account_id = $leaf_account->id;            
//            }
            if (
                    isset($this->account) && isset($this->cost_type) && 
                    (
                        intval($this->__old['cost_type_id']) !== intval($this->cost_type_id) || 
                        intval($this->__old['account_id']) !== intval($this->account_id)
                    ) && 
                    intval($this->account_id) !== intval($leaf_account->id)
               )
            {
                $year = $this->account->year;
                $cost_type_to_account = CostTypeToAccount::model()->find(new SIMADbCriteria([
                    'Model'=>'CostTypeToAccount',
                    'model_filter'=>[
                        'cost_type'=>[
                            'ids'=>$this->cost_type_id
                        ],
                        'account'=>[
                            'year'=>[
                                'ids'=>$year->id
                            ]
                        ]
                    ]
                ]));
                if (!is_null($cost_type_to_account))
                {
                    $this->addError('cost_type_id', "Već postoji vrsta troška {$this->cost_type->DisplayName} za godinu {$year->DisplayName}.");
                }
            }
        }
    }
    
    public function afterSave()
    {
        parent::afterSave();
        $_cost_type_changed = false;
        if ($this->columnChanged('cost_type_name'))
        {
            $this->cost_type->name = $this->cost_type_name;
            $_cost_type_changed = true;
        }
        if ($this->columnChanged('cost_type_book_by_cost_location'))
        {
            $this->cost_type->book_by_cost_location = $this->cost_type_book_by_cost_location;
            $_cost_type_changed = true;
        }
        if ($_cost_type_changed)
        {
            $this->cost_type->save();
        }
    }
    
    /**
     * Privatna funkcija koja proverava da li je zadat kontni okvir, i ako jeste onda iz njega kreira konto i setuje account_id na kreirani konto 
     */
    private function setAccountIfIsLayout()
    {        
        if (strpos($this->account_id,'l_') !== false) 
        {
            $arr = explode("l_", $this->account_id);
            $account = Account::getAccountByLayout($arr[0], $arr[1]);
            $this->account = $account;
            $this->account_id = $account->id;
        }
    }
}
