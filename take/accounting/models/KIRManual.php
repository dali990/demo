<?php

class KIRManual extends SIMAActiveRecord
{
    public $column_7_compare;
    public $column_8_compare;
    public $column_9_compare;
    public $column_10_compare;
    public $column_11_compare;
    public $column_12_compare;
    public $column_13_compare;
    public $column_14_compare;
    public $column_15_compare;
    public $column_16_compare;
    public $column_17_compare;
    
    public static $REGULAR = 'REGULAR';
    public static $CANCELED = 'CANCELED';
    
    public static $reasons = [
        'REGULAR' => 'Regularan',
        'CANCELED' => 'Storniran',
    ];
    
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'accounting.kir_manual';
    }

    public function moduleName()
    {
        return 'accounting';
    }

    public function __get($column)
    {
        switch ($column)
        {
            default: return parent::__get($column);
        }
    }
    
    public function rules() 
    {
        return [
            ['bill_id, reason', 'required'],
            ['month_id', 'required'],
            ['column_7, column_8, column_9, column_10, ', 'required'],
            ['column_11, column_12, column_13', 'required'],
            ['column_14, column_15, column_16, column_17', 'required'],
            ['bill_id','unique_with','with' => ['reason']],
            ['order_in_year', 'checkOrder', 'on'=>['insert','update']],
        ];
    }
    
    public function beforeValidate()
    {
        if (empty($this->order_in_year) && !empty($this->month))
        {
            $this->order_in_year = $this->month->year->accounting->getNextKIRCount();
        }

        return parent::beforeValidate();
    }
    
    public function checkOrder()
    {
        if (!$this->hasErrors())
        {
            $_duplicate = KIRManual::model()->findByAttributes(['order_in_year' => $this->order_in_year]);
            if (isset($_duplicate) && $_duplicate->id != $this->id)
            {
                if ($this->month->year_id == $_duplicate->month->year_id)
                {
                    $this->addError('order_in_year', 'Redni broj vec postoji');
                }
            }
        }
    }

    public function relations($child_relations = [])
    {
        return [
            'bill' => [self::BELONGS_TO,'AnyBill','bill_id'],
            'month' => [self::BELONGS_TO,'Month','month_id'],
        ];
    }
    
    private $_kir = -1;
    public function getkir()
    {
        if ($this->_kir === -1)
        {
            $this->_kir = KIR::model()->findByAttributes([
                'bill_id' => $this->bill_id,
                'month_id' => $this->month_id,
                'reason' => $this->reason
            ]);
        }
        return $this->_kir;
    }
    
    public function refresh()
    {
        $this->_kir = -1;
        return parent::refresh();
    }
    
    public function getClasses()
    {
        return parent::getClasses().$this->bill->getVATClasses();
    }

    public function scopes()
    {
        $alias = $this->getTableAlias();
        $kir_12_diff = KIR::showIncome()?"acckir.kir_18 != $alias.column_12 OR  ":"acckir.kir_12 != $alias.column_12 OR  ";
//        $ad_table = AccountDocument::model()->tableName();
        return [
            'diffKIRAndInKIRMonth' => [
                'condition' => "exists "
                    . "("
                    . "select 1 from accounting.kir acckir where acckir.bill_id = $alias.bill_id and acckir.reason = $alias.reason and acckir.month_id = $alias.month_id "
                    . " AND ("
                            ."acckir.kir_7 != $alias.column_7 OR  "
                            ."acckir.kir_8 != $alias.column_8 OR  "
                            ."acckir.kir_9 != $alias.column_9 OR  "
                            ."acckir.kir_10 != $alias.column_10 OR  "
                            ."acckir.kir_11 != $alias.column_11 OR  "
                            .$kir_12_diff
                            ."acckir.kir_13 != $alias.column_13 OR  "
                            ."acckir.kir_13 != acckir.kir_13b OR  "
                            ."acckir.kir_13b != $alias.column_13 OR  "
                            ."acckir.kir_14 != $alias.column_14 OR "
                            ."acckir.kir_15b != $alias.column_15 OR "
                            ."acckir.kir_15 != acckir.kir_15b OR "
                            ."acckir.kir_15 != $alias.column_15 "
                            //."acckir.kir_16 != $alias.column_16 OR  "
                            //."acckir.kir_17 != $alias.column_17 "
                    . ")"
                . ")"
            ],
            'byOrder' => [
                'order' => $alias.'.order_in_year DESC'
            ],
            'byOrderInYearAsc' => [
                'order' => $alias.'.order_in_year ASC'
            ]
        ];
    }
    
    
    public function inKIRInMonth($month_id)
    {
        $month = Month::model()->findByPk($month_id);
        $month_name = $month->month;
        $year_name = $month->year->year;
        $alias = $this->getTableAlias();
        
        $criteria = new SIMADbCriteria();
        $criteria->condition = "exists "
                . "("
                . "select 1 from accounting.kir where bill_id = $alias.bill_id and reason = $alias.reason and month_id = $alias.month_id"
                . ")";
        
        $this->getDbCriteria()->mergeWith($criteria);
        return $this;
    }
    
    public function notInKIRInMonth($month_id)
    {
        $month = Month::model()->findByPk($month_id);
        $month_name = $month->month;
        $year_name = $month->year->year;
        $alias = $this->getTableAlias();
        
        $criteria = new SIMADbCriteria();
        $criteria->condition = "not exists "
                . "("
                . "select 1 from accounting.kir where bill_id = $alias.bill_id and reason = $alias.reason and month_id = $alias.month_id"
                . ")";
        
        $this->getDbCriteria()->mergeWith($criteria);
        return $this;
    }
    
    public function inYear($year_id)
    {
        $uniq = SIMAHtml::uniqid();
        $alias = $this->getTableAlias();
        $month_table = AccountingMonth::model()->tableName();
        $year = AccountingYear::model()->findByPk($year_id);
        
        $criteria = new SIMADbCriteria();
        $criteria->condition = "m$uniq.year = :param1$uniq";
        $criteria->join = "left join $month_table m$uniq on m$uniq.id = $alias.month_id";
        $criteria->params = [
            ":param1$uniq" => $year->year
        ];
        
        $this->getDbCriteria()->mergeWith($criteria);
        
        return $this;
    }
    
    public function orderInYearEqualGreaterThen($value)
    {
        $uniq = SIMAHtml::uniqid();
        $alias = $this->getTableAlias();
        
        $criteria = new SIMADbCriteria();
        $criteria->condition = "$alias.order_in_year >= :param1$uniq";
        $criteria->params = [
            ":param1$uniq" => $value
        ];
        
        $this->getDbCriteria()->mergeWith($criteria);
        
        return $this;
    }

    public function modelSettings($column) 
    {
        switch ($column)
        {
            case 'number_fields': return [
                'column_7',
                'column_8',
                'column_9',
                'column_10',
                'column_11',
                'column_12',
                'column_13',
                'column_14',
                'column_15',
                'column_16',
                'column_17',
                'order_in_year',
                'column_7_compare',
                'column_8_compare',
                'column_9_compare',
                'column_10_compare',
                'column_11_compare',
                'column_12_compare',
                'column_13_compare',
                'column_14_compare',
                'column_15_compare',
                'column_16_compare',
                'column_17_compare',
            ];
            case 'filters': return [
                'order_in_year' => 'integer',
                'month' => 'relation',
                'bill' => 'relation',
                'reason' => 'dropdown',
//                'kpr' => 'relation',
            ];
            default: return parent::modelSettings($column);
        }
        
    }

}
