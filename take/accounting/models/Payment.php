<?php

class Payment extends SIMAActiveRecord 
{
    
//     protected $type;
    public $advance;
    public $bill_id;
    public $like_bill_id;
    public $selected = false;
    private $_confirm1 = null;
    private $_confirm2 = null;
    public $unreleased_percent_of_amount;
    public $saldo_amount;
    
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'accounting.payments';
    }

    public function moduleName()
    {
        return 'accounting';
    }
    
    public function relations($child_relations = [])
    {
        $bill_release_table = BillRelease::model()->tableName();
        return parent::relations(array_merge([
            'partner' => array(self::BELONGS_TO, 'Partner', 'partner_id'),
            'account' => array(self::BELONGS_TO, 'BankAccount', 'account_id'),
            'payment_type' => array(self::BELONGS_TO, 'PaymentType', 'payment_type_id'),
            'payment_code' => array(self::BELONGS_TO, 'PaymentCode', 'payment_code_id'),
            'bank_statement' => array(self::BELONGS_TO, 'BankStatement', 'bank_statement_id'),
            'job_order' => array(self::BELONGS_TO, 'CostLocation', 'cost_location_id'),
            'bills' => array(self::MANY_MANY, 'Bill', $bill_release_table.'(payment_id,bill_id)'),
            'releases' => array(self::HAS_MANY, 'BillRelease', 'payment_id'),
            'releases_count' => array(self::STAT, 'BillRelease', 'payment_id', 'select' => 'count(*)'),
            'released_sum' => array(self::STAT, 'BillRelease', 'payment_id', 'select' => 'sum(amount)'),
            'exchange_rate_difference_sum' => array(self::STAT, 'BillRelease', 'payment_id', 'select' => 'sum(exchange_rate_difference)'),
            'other_difference_sum' => array(self::STAT, 'BillRelease', 'payment_id', 'select' => 'sum(other_difference)'),
            'confirm1_user' => array(self::BELONGS_TO, 'User', 'confirm1_user_id'),
            'confirm2_user' => array(self::BELONGS_TO, 'User', 'confirm2_user_id'),
            'year' => array(self::BELONGS_TO, 'Year', 'year_id'),
            'recurring_bill' => [self::BELONGS_TO, 'RecurringBill', 'recurring_bill_id'],
            'releases' => array(self::HAS_MANY, 'BillRelease', 'payment_id'),
            'payment_debt_take_overs' => [self::HAS_MANY, 'PaymentDebtTakeOver', 'payment_id']
            
//            'bills'=> [self::MANY_MANY, 'BIll', $bill_releases_table .'(payment_id, bill_id)'],
        ],$child_relations));
    }

    public function canAddDocument() 
    {   
        if ($this->id == null 
                || $this->payment_type_id != Yii::app()->configManager->get('accounting.advance_payment_type') 
                || !$this->invoice
                || count($this->releases) != 0)
        {
            return false;
        }
        return true;
    }

    public function __get($column) 
    {
        $default_payment_type = Yii::app()->configManager->get('accounting.default_payment_type');
        $advance_payment_type = Yii::app()->configManager->get('accounting.advance_payment_type');
        switch ($column) 
        {
            case 'DisplayName': return $this->payment_date.'|'.$this->partner->DisplayName.'|'.$this->getAttributeDisplay('amount');
            case 'SearchName': return $this->DisplayName;
            case 'path2': return 'accounting/payment';
            case 'isReleased':
                //ovo u sustini sluzi samo za avans, posto placanje ne moze da visi
                if (/**$this->payment_type_id == $default_payment_type || */$this->payment_type_id == $advance_payment_type)
                {
                    return $this->connected;
                }
                else
                {
                    return true;
                }
            //da li je povezan sa placanjima
            case 'connected': 
                if ($this->payment_type_id == $default_payment_type || $this->payment_type_id == $advance_payment_type)
                {
                    return abs($this->unreleased_amount) < 0.001;
                }
                else
                {
                    return true;
                }
            case 'confirm1': return ($this->payment_type_id != $default_payment_type && $this->_confirm1) || ($this->payment_type_id == $default_payment_type && $this->connected);
            case 'confirm2': return ($this->payment_type_id != $default_payment_type && $this->_confirm2) || ($this->payment_type_id == $default_payment_type && $this->connected);
            default: return parent::__get($column);
        }
    }
    
    public function __isset($column)
    {
        switch ($column)
        {
            case 'confirm1': return $this->_confirm1!==null;
            case 'confirm2': return $this->_confirm2!==null;
            default: return parent::__isset($column);
        }
    }
    
    public function __unset($column)
    {
        switch ($column)
        {
            case 'confirm1': $this->_confirm1=null;
            case 'confirm2': $this->_confirm2=null;
            default: return parent::__unset($column);
        }
    }
    
    public function afterFind()
    {
        parent::afterFind();
        $this->confirm1 = isset($this->confirm1_timestamp);
        $this->confirm2 = isset($this->confirm2_timestamp);
    }
    
    public function __set($column,$value)
    {
        switch ($column)
        {
            case 'confirm1':
                if(
                    (gettype($value)==='boolean' && $value) 
                    || 
                    (gettype($value)==='string' && $value=='true')
                    || 
                    (gettype($value)==='string' && $value=='1')
                  )
                { 
                    $this->_confirm1 = true; 
                }
                else
                {
                    $this->_confirm1 = false; 
                }
                break;
            case 'confirm2': 
                if(
                    (gettype($value)==='boolean' && $value) 
                    || 
                    (gettype($value)==='string' && $value=='true')
                    || 
                    (gettype($value)==='string' && $value=='1')
                  ) 
                {
                    $this->_confirm2 = true; 
                }
                else
                {
                    $this->_confirm2 = false; 
                }
                break;
            default: parent::__set($column,$value); break;
        }
    }
    
    public function getBillReleasedAmount($payment) {
        if (!isset($this->id))
        {
            return 0;
        }
        $billReleases = BillRelease::model()->findAllByAttributes(array('bill_id' => $payment, 'payment_id' => $this->id));
        $sum = 0;
        foreach ($billReleases as $billRelease) {
            $sum+=$billRelease->amount;
        }
        return $sum;
    }

    public function rules() 
    {
        return array(
            array('partner_id, amount, payment_type_id', 'required'),
            array('amount', 'numerical'),
            array('advance, bank_statement_id, comment, cost_location_id, account_id', 'safe'),
            array('confirm1_user_id, confirm2_user_id, confirm1_timestamp, confirm2_timestamp', 'safe'),
            array('payment_code_id, call_for_number, call_for_number_partner, year_id', 'safe'),
            array('confirm1, confirm2, call_for_number_modul_partner, payment_date, recurring_bill_id', 'safe'),
            ['invoice, payment_type_id, partner_id','NoChangeWhenReleased'],
//            ['recurring_bill_id','checkSamePartner'],
            array('account_id, partner_id, cost_location_id, year_id, recurring_bill_id', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => array('insert', 'update')),
            array('account_id', 'required', 'on' => array('billpay'))
        );
    }
    
    public function NoChangeWhenReleased($column)
    {
        if  ($this->releases_count > 0 && $this->columnChanged($column))
        {
            $this->addError($column, Yii::t('AccountingModule.Payment','CantChangeHaveReleases'));
        }
    }
    
//    public function checkSamePartner()
//    {
//        if (!$this->hasErrors() && !empty($this->recurring_bill))
//        {
//            if ($this->recurring_bill->partner_id != $this->partner_id)
//            {
//                $error_text = Yii::t('AccountingModule.Payment','ReccuringBillPartnerDontmatch');
//                $this->addError('recurring_bill_id', $error_text);
//                $this->addError('partner_id', $error_text);
//            }
//        }
//    }
    
    
    public function beforeSave() 
    {        
        if ($this->year_id === null)
        {
            //proveravamo koji je datum placanja i na osnovu toga upisujemo godinu u kolonu year_id
            $payment_date = new DateTime($this->payment_date);
            $this->year_id = Year::get($payment_date->format("Y"))->id;
        }
        if ($this->isNewRecord)
        {
            if (!empty($this->call_for_number))
            {
                $recurring_bill = RecurringBill::model()->findByModelFilter([
                    'call_for_number' => $this->call_for_number
                ]);
                if (!is_null($recurring_bill))
                {
                    $this->recurring_bill_id = $recurring_bill->id;
                }
            }
        }
        else
        {
            if ($this->columnChanged('recurring_bill_id') || $this->columnChanged('amount'))
            {
                $bills = [];
                foreach ($this->releases as $_release)
                {
                    $_release->delete();
                    $bills[] = $_release->bill->DisplayName;
                }
                if (!empty($bills))
                {
                    Yii::app()->raiseNote(Yii::t('AccountingModule.Payment','ReleseDeleted',[
                        '{bills_list}'=>implode(' | ', $bills)
                    ]));
                }
            }
        }
        
        return parent::beforeSave();
    }
    
    public function afterSave()
    {
        parent::afterSave();
        if (
                ($this->columnChanged('recurring_bill_id') || $this->columnChanged('amount') || $this->isNewRecord)
                && !empty($this->recurring_bill_id))
        {
            $condition = new SIMADbCriteria([
                'Model' => Bill::model(),
                'model_filter' => [
                    'recurring_bill' => ['ids' => $this->recurring_bill_id],
                    'filter_scopes' => ['unreleased'],
                    'display_scopes' => ['byOfficial'],
                ]
            ]);
            $reccuring_bills = Bill::model()->findAll($condition);
            
            foreach ($reccuring_bills as $reccuring_bill)
            {
                ConnectAccountingDocuments::BillToPayment($reccuring_bill, $this);
                $check_value = Payment::model()->findByPk($this->id)->unreleased_amount;
                if ($check_value <= 0 )
                {
                    break;
                }
            }
        }
    }

    public function scopes() 
    {
        $alias = $this->getTableAlias(FALSE, FALSE);
        $paycheck_payment_type_id = Yii::app()->configManager->get('accounting.paycheck_payment_type');
        return array(
            'recently' => array(
                'order' => "$alias.payment_date DESC, $alias.id DESC",
            ),
            'byOfficial' => [
                'order' => "$alias.payment_date, $alias.id",
            ],
            'byOfficialDESC' => [
                'order' => "$alias.payment_date DESC, $alias.id",
            ],
            'inBankStatement' => array(
                'order' => "$alias.invoice, $alias.amount DESC",
            ),
            'for_pay' => array(
                'condition'=>"$alias.bank_statement_id IS NULL"
            ),
            'payed' => array(
                'condition'=>"$alias.bank_statement_id IS NOT NULL"
            ),
            'unreleased' => [
                'condition'=>"$alias.unreleased_amount != 0"
            ],
            'noPaychecks' => [
                'condition'=>"$alias.payment_type_id != ".$paycheck_payment_type_id
            ]
        );
    }

    public function getClasses() {
        return parent::getClasses() .
                ((!$this->isReleased) ? ' not_released ' : '') .
//                ((!$this->connected) ? ' not_attached ' : '') .
                ' payment_type_id_' . $this->payment_type_id;
    }

    public function getAdditionalInfo() {
        return " payment_id='$this->id' partner_id='$this->partner_id'" .
                (($this->like_bill_id != null) ? 'data="' . get_class($this) . '[like_bill_id]=' . $this->like_bill_id . '"' : '') .
                parent::getAdditionalInfo();
    }

    public function modelSettings($column) 
    {
        switch ($column)
        {
            case 'textSearch': return [
                'partner'=>'relation',
                'payment_code' => 'relation',
                'payment_date' => 'date_range',
                'comment' => 'text'
            ];
            case 'filters': return array(
                'payment_date' => 'date_range',
                'link' => 'hidden',
//                'bank_statement_id' => 'dropdown',
                'payment_type_id' => 'dropdown',
                'invoice' => 'boolean',
                'bill_id'=>array('func'=>'filter_bill_id'),
                'like_bill_id'=>array('func'=>'filter_like_bill_id'),
                'partner'=>'relation',
                'payment_code' => 'relation',
                'year' => 'relation',
                'transaction_id' => 'integer',
                'call_for_number' => ['func'=>'filterCallForNumber'],
                'call_for_number_partner' => ['func'=>'filterCallForNumber'],
                'call_for_number_modul' => 'text',
                'call_for_number_modul_partner' => 'text',
                'account' => 'relation',
                'hash' => 'text',
                'bank_statement' => 'relation',
                'amount' => 'numeric',
                'comment' => 'text',
                'job_order' => 'relation',
                'recurring_bill' => 'relation',
                'payment_type' => 'relation'
            );            
            case 'options' : return array('delete', 'form', 'open', 'addDocument');
            case 'multiselect_options' : return array('delete');
            case 'statuses' : return array(
                'confirm1' => array(
                    'title' => 'Potvrdjeno1',
                    'timestamp' => 'confirm1_timestamp',
                    'user' => array('confirm1_user_id','relName'=>'confirm1_user'),
                    'checkAccessConfirm' => 'checkConfirm1Access',
                    'filter_func' => 'filter_confirm1',
                ),
                'confirm2' => array(
                    'title' => 'Potvrdjeno2',
                    'timestamp' => 'confirm2_timestamp',
                    'user' => array('confirm2_user_id','relName'=>'confirm2_user'),
                    'checkAccessConfirm' => 'checkConfirm2Access',
                    'filter_func' => 'filter_confirm2',
                ),
            );
            case 'update_relations': return array(
                'bank_statement'
            );
            case 'number_fields': return [
                'amount', 
                'unreleased_amount',
                'saldo_amount'
            ];
            default: return parent::modelSettings($column);
        }
        
    }
    
    public function filterCallForNumber($condition, $alias, $column)
    {
        if (!empty($this->call_for_number)) 
        {
            $param_id = SIMAHtml::uniqid();
            $temp_cond = new SIMADbCriteria();
            $temp_cond->condition = "replace($alias.$column,'-','') = replace(:call_for_number$param_id,'-','')";
            $temp_cond->params = [":call_for_number$param_id" => $this->$column];
            $condition->mergeWith($temp_cond);
        }
    }
    
    public function filter_bill_id($condition)
    {
        if ($this->bill_id != null) 
        {
            $param_id = SIMAHtml::uniqid();
            $bill_releases_table = BillRelease::model()->tableName();
            $alias = $this->getTableAlias();
            $temp_cond = new SIMADbCriteria();
            $temp_cond->condition = "$alias.id in (select payment_id from $bill_releases_table where bill_id=:bill_id$param_id)";
            $temp_cond->params = array(":bill_id$param_id" => $this->bill_id);
            $condition->mergeWith($temp_cond);
        }
    }
    
    public function filter_like_bill_id($condition)
    {
        if (isset($this->like_bill_id) && $this->like_bill_id != null) 
        {
            $bill = Bill::model()->findByPk($this->like_bill_id);
            if ($bill != null) 
            {
                $temp_cond = new SIMADbCriteria([
                    'Model' => 'Advance',
                    'model_filter' => [
                        'partner' => ['ids'=>$bill->partner_id],
                        'invoice' => $bill->invoice
                    ]
                ]);        
                $condition->mergeWith($temp_cond);
            }
        }
    }
    
    public function filter_confirm1($condition) 
    {
        if (isset($this->confirm1))
        {
            $alias = $this->getTableAlias();
            $bill_release_table = BillRelease::model()->tableName();
            $temp_condition = new SIMADbCriteria();
            $default_payment_type = Yii::app()->configManager->get('accounting.default_payment_type');
            
            if ($this->confirm1)
            {                
                    $temp_condition->condition = "($alias.confirm1_timestamp is not null and $alias.payment_type_id!=$default_payment_type) "
                            . "or ($alias.payment_type_id=$default_payment_type and "
                            . "($alias.amount = (select sum(amount) from $bill_release_table where payment_id = $alias.id))"
                            . ")";
            }
            else if (!$this->confirm1)
            {                   
                    $temp_condition->condition = "($alias.confirm1_timestamp is null or $alias.payment_type_id=$default_payment_type) "
                            . "and ($alias.payment_type_id!=$default_payment_type or "
                            . "($alias.amount != (select sum(amount) from $bill_release_table where payment_id = $alias.id))"
                            . ")";
            }
            $condition->mergeWith($temp_condition);
        }
    }
    
    public function filter_confirm2($condition) 
    {
        if (isset($this->confirm2))
        {
            $alias = $this->getTableAlias();
            $bill_release_table = BillRelease::model()->tableName();
            $default_payment_type = Yii::app()->configManager->get('accounting.default_payment_type');
            $temp_condition = new SIMADbCriteria();
            if ($this->confirm2)
            {                
                    $temp_condition->condition = "($alias.confirm2_timestamp is not null and $alias.payment_type_id!=$default_payment_type) "
                            . "or ($alias.payment_type_id=$default_payment_type and "
                            . "($alias.amount = (select sum(amount) from $bill_release_table where payment_id = $alias.id))"
                            . ")";
            }
            else if (!$this->confirm2)
            {                   
                    $temp_condition->condition = "($alias.confirm2_timestamp is null or $alias.payment_type_id=$default_payment_type) "
                            . "and ($alias.payment_type_id!=$default_payment_type or "
                            . "($alias.amount != (select sum(amount) from $bill_release_table where payment_id = $alias.id))"
                            . ")";
            }            
            $condition->mergeWith($temp_condition);
        }
    }
    
    public function checkConfirm1Access()
    {
        $errors = [];
        
        $default_payment_type = Yii::app()->configManager->get('accounting.default_payment_type');
        
        if (!Yii::app()->user->checkAccess('modelPaymentConfirm1'))
        {
            $errors[] = 'Nije vam dodeljena privilegija za ovu potvrdu.';
        }
        
        if ((int)$this->payment_type_id === (int)$default_payment_type)
        {
            $errors[] = 'Plaćanja po računu se potvrđuju likvidacijom.';
        }
        
        return (count($errors) > 0) ? $errors : true;
    }
    
    public function checkConfirm2Access()
    {
        $errors = [];
        
        $default_payment_type = Yii::app()->configManager->get('accounting.default_payment_type');
        
        if (!Yii::app()->user->checkAccess('modelPaymentConfirm1'))
        {
            $errors[] = 'Nije vam dodeljena privilegija za ovu potvrdu';
        }
        
        if (!$this->confirm1)
        {
            $errors[] = 'Prvo mora biti izvršena prva potvrda';
        }
        
        if ((int)$this->payment_type_id === (int)$default_payment_type)
        {
            $errors[] = 'Plaćanja po računu se potvrđuju likvidacijom';
        }
        
        return (count($errors) > 0) ? $errors : true;
    }

    public function checkConfirm() {
        if ($this->cost_location_id == null)
        {
            throw new Exception('nije postavljeno mesto troska');
        }
        return true;
    }
    
    public function unreleased_percent_of_amount($amount)
    {        
        $alias = $this->getTableAlias();
        $this->getDbCriteria()->mergeWith(array(
            'select'=>"$alias.*,(CASE WHEN :amount < 0.001 THEN :amount ELSE ($alias.unreleased_amount * 100/:amount) END) as unreleased_percent_of_amount",
            'params'=> array(':amount' => $amount),
        ));
        return $this;
    }
}