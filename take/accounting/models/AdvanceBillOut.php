<?php

class AdvanceBillOut extends AdvanceBill
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function init()
    {
        parent::init();
        $this->invoice = true;
        $this->lock_amounts = false;
    }

    public function defaultScope()
    {
        $alias = $this->getTableAlias(FALSE, FALSE);
        $adv_bill = Bill::$ADVANCE_BILL;
        return array(
            'condition' => "$alias.invoice=true and $alias.bill_type = '$adv_bill' and $alias.canceled=false",
        );
    }

}