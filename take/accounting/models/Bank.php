<?php

class Bank extends SIMAActiveRecord {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'accounting.banks';
    }

    public function moduleName() {
        return 'accounting';
    }

    public function __get($column) {
        switch ($column) {
            case 'DisplayName': return $this->company->DisplayName;
            case 'SearchName': return $this->company->SearchName;
            case 'path2': return Company::model()->path2;
            default: return parent::__get($column);
        }
    }

    public function rules() {
        return array(
            array('id, bank_account_code, short_name', 'required'),
            ['bank_account_code', 'unique'],
            ['id', 'checkBankUniq'],
            array('shared_accounts', 'safe'),
        );
    }
    
    public function checkBankUniq()
    {
        if (!$this->hasErrors() && ($this->isNewRecord || $this->columnChanged('id')))
        {
            $bank = Bank::model()->findByPk($this->id);
            if (!empty($bank))
            {
                $this->addError('id', Yii::t('AccountingModule.Bank', 'BankAlreadyExists', [
                    '{bank_name}' => $this->company->DisplayName
                ]));
            }
        }
    }

    public function scopes() {
        return array(
            'byName' => array(
                'order' => 'bank_account_code',
            ),
        );
    }

    public function relations($child_relations = []) {
        return array(
            'company' => array(self::BELONGS_TO, 'Company', 'id')
        );
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'textSearch': case 'filters': return [
                'bank_account_code' => ['integer', 'func' => 'filter_bank_account_code'],
                'short_name' => 'text',
                'company' => 'relation',
                'shared_accounts' => 'boolean'
            ];
            default: return parent::modelSettings($column);
        }
        
    }
    
    public function filter_bank_account_code($condition, $alias)
    {
        if (!empty($this->bank_account_code))
        {
            $uniq = SIMAHtml::uniqid();
            $temp_condition = new CDbCriteria();
            $temp_condition->condition = ":param1$uniq ~* ('^' || replace($alias.bank_account_code, '*', '.*') || '$')";
            $temp_condition->params = [
                ":param1$uniq" => $this->bank_account_code
            ];
            $condition->mergeWith($temp_condition, true);
        }
    }

    public function IsInUnified()
    {
        $result = SIMADbCriteria::DoesHaveAnyModelByFilter(UnifiedPaycheck::model(), [
            'bank' => [
                'ids' => $this->id
            ],
            'active' => true
        ]);
        
        return $result;
    }
    
    public static function GetByBankCode($bank_code)
    {
        $bank_criteria = new SIMADbCriteria([
            'Model' => Bank::model(),
            'model_filter' => [
                'bank_account_code' => $bank_code
            ]
        ], true);
        
        $bank = Bank::model()->find($bank_criteria);
        
        if(empty($bank))
        {
            throw new SIMAExceptionBankNotFoundByCode($bank_code);
        }
        
        return $bank;
    }
}