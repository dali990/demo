<?php

class CostLocationVirtualParent extends SIMAActiveRecord
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
 
    public function tableName()
    {
        return 'accounting.cost_location_virtual_parent';
    }
    
    public function moduleName()
    {
        return 'accounting';
    }
    
    public function __get($column)
    {
        switch ($column)
        {
//            case 'DisplayName': return $this->cost_type->DisplayName.'('.$this->account->DisplayName.')';
//            case 'SearchName': return $this->DisplayName;
            default: return parent::__get($column);
        }
    }
    
    public function rules()
    {
        return array(
            array('virtual_parent_id, virtual_child_id', 'required'),            
            array('virtual_parent_id, virtual_child_id','default',
                'value'=>null,
                'setOnEmpty'=>true,'on'=>array('insert','update')
            )
        );
    }
    
    public function relations($child_relations = []) 
    {
        return array(
            'virtual_parent' => array(self::BELONGS_TO, 'CostLocation', 'virtual_parent_id'),
            'virtual_child' => array(self::BELONGS_TO, 'CostLocation', 'virtual_child_id'),
        );
    }
    
    public function modelSettings($column)
    { 
        switch ($column)
        {
            case 'filters' : return array_merge(parent::modelSettings($column),array(
                'virtual_parent' => 'relation',
                'virtual_child' => 'relation'                
            ));            
            default: return parent::modelSettings($column);
        }
    }
    
}
