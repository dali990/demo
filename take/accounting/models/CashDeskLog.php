<?php

class CashDeskLog extends SIMAActiveRecord
{
    private $_in = null;
    private $_out = null;
    public $date_yesterday;
    public $saldo_letters;
    
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'accounting.cash_desk_logs';
    }

    public function moduleName()
    {
        return 'accounting';
    }

    public function __get($column)
    {
        switch ($column)
        {
            case 'path2': return 'files/file';
            case 'DisplayName': return $this->date;
            case 'SearchName': return $this->date;
            case 'date_yesterday': return $this->date_yesterday;
            case 'saldo_letters': return $this->saldo_letters;
            case 'in': return (is_null($this->_in)) ? $this->_in = $this->calcIn() : $this->_in;
            case 'out': return (is_null($this->_out)) ? $this->_out = $this->calcOut() : $this->_out;
            default: return parent::__get($column);
        }
    }
    
    public function __isset($column)
    {
        switch ($column)
        {
            case 'in': return true;
            case 'out': return true;
            default: return parent::__isset($column);;
        }
    }
    
    public function afterFind()
    {
        parent::afterFind();
        $this->date_yesterday = date('d.m.Y.',(strtotime( '-1 day' , strtotime( $this->date ) ) ));
        $this->saldo_letters = SIMAHtml::money2letters($this->saldo, 'latin');
    }
    
    public function relations($child_relations = [])
    {
        $_payout = CashDeskOrder::$PAYOUT;
        $_payin = CashDeskOrder::$PAYIN;
        $_submit = CashDeskOrder::$SUBMIT;
        $_withdraw = CashDeskOrder::$WITHDRAW;
        
        return array(
            'file' => array(self::BELONGS_TO, 'File', 'id'),
            'year' => array(self::BELONGS_TO, 'Year', 'year_id'),
            
            'cash_desk_orders'=>array(self::HAS_MANY, 'CashDeskOrder', 'cash_desk_log_id'),
            'cash_desk_orders_cnt'=>array(self::STAT, 'CashDeskOrder', 'cash_desk_log_id', 'select'=>'count(*)'),
            'cash_desk_orders_in'=>array(self::HAS_MANY, 'CashDeskOrder', 'cash_desk_log_id', 
                'condition'=>"cash_desk_orders_in.type in ('$_payin', '$_withdraw')"
            ),
            'cash_desk_orders_out'=>array(self::HAS_MANY, 'CashDeskOrder', 'cash_desk_log_id', 
                'condition'=>"cash_desk_orders_out.type in ('$_payout', '$_submit')"
            ),
            'confirmed_user' => array(self::BELONGS_TO, 'User', 'confirmed_user_id'),
        );
    }

    public function rules()
    {
        return array(
//            array('date', 'required', 'message' => 'Polje "{attribute}" mora biti uneto'),
            ['confirmed', 'checkIfAllOrdersAreConfirmed'],
            array('date', 'unique'),
            ['saldo', 'checkNumericNumber', 'precision' => 20, 'scale' => 2],
            ['previous_saldo', 'checkNumericNumber', 'precision' => 20, 'scale' => 2]
        );
    }    
    
    public function checkIfAllOrdersAreConfirmed()
    {
        if ($this->columnChanged('confirmed') && $this->confirmed === true)
        {
            $all_confirmed = true;
            foreach ($this->cash_desk_orders as $cash_desk_order)
            {
                if ($cash_desk_order->confirmed === false)
                {
                    $all_confirmed = false;
                }
            }

            if (!$all_confirmed)
            {
                $this->addError('confirmed',Yii::t('AccountingModule.CashDesk','ConfirmAllOrders',['{date}' => $this->date]));
            }
        }
    }
    
    public function scopes()
    {
        $alias = $this->getTableAlias();
        return [
            'byName' => array('order' => "$alias.date desc"),
            'byDate' => array('order' => "$alias.date"),
            'byDateDESC' => array('order' => "$alias.date desc"),
        ];
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'options': return array(
                'open',
                'download'
            );
            case 'number_fields': return array(
                'in','out','saldo'
            );
            case 'filters': return array(
                'date' => 'date_range',
            );
            case 'statuses': return array(
                'confirmed' => array(
                    'title' => 'Potvrdjeno',
                    'timestamp' => 'confirmed_timestamp',
                    'user' => array('confirmed_user_id','relName'=>'confirmed_user'),
//                    'checkAccessConfirm' => 'canConfirmCashDeskLog',
                    'onConfirm'=>'onConfirm',
                    'onRevert'=>'onRevert',
                )
            );
            case 'multiselect_options': return [
                'statuses' => ['confirmed']
            ];
            default:return parent::modelSettings($column);
        }
        
    }
    
//    public function canConfirmCashDeskLog()
//    {
//        return true;
//    }
    
    public function onConfirm()
    {       
        if (isset($this->file))
        {
            $this->generatePDF();
        }
    }
    
    public function onRevert()
    {
        //ako je skinuta potvrda sa dnevnog izvestaja blagajne onda skidamo potvrdu i sa naloga za knjizenje(ako je postavljen)
        if (
                isset($this->file->account_document->account_order) && 
                $this->file->account_document->account_order->booked === true
            )
        {
            $this->file->account_document->account_order->booked = false;
            $this->file->account_document->account_order->save();
        }
    }
    
    public function beforeSave()
    {
        if (!isset($this->file) && ($this->id == null || $this->id == ''))
        {
            $cash_desk_log_document_type_id = Yii::app()->configManager->get('accounting.cash_desk.log', false);
            $file = new File();
            $file->name = 'Dnevnik blagajne za '.$this->date;
            $file->document_type_id = $cash_desk_log_document_type_id;
            $file->responsible_id = Yii::app()->user->id;
            $file->save();
            $file->refresh();
            $this->id = $file->id;
            $this->file = $file;
        }
        
        if ($this->__old['date'] !== $this->date)
        {
            $this->confirmed = false;
        }
        $date = new DateTime($this->date);
        $this->year_id = Year::get($date->format("Y"))->id;
        
        return parent::beforeSave();
    }
    
    public function afterSave() 
    {
//        $this->refresh();
        if ($this->__old['date'] !== $this->date && $this->__old['date'] !== null)
        {
            $date = $this->__old['date'];
            if (strtotime($this->date) < strtotime($this->__old['date']))
            {
                $date = $this->date;
            }
            $this->changeConfirmForNewerLogs($date);
        }
        parent::afterSave();
    }
    
    public function afterDelete() 
    {
        parent::afterDelete();
        
        $this->changeConfirmForNewerLogs($this->date);
    }
    
    private function changeConfirmForNewerLogs($date)
    {
        $param_id = SIMAHtml::uniqid();
        $criteria = new SIMADbCriteria();
        $criteria->condition = "date > :param1$param_id";
        $criteria->params = array(":param1$param_id" => SIMAHtml::UserToDbDate($date));
        $cash_desk_newer_logs = CashDeskLog::model()->findAll($criteria);
        foreach ($cash_desk_newer_logs as $cash_desk_newer_log) 
        {
            if ($cash_desk_newer_log->confirmed)
            {
                $cash_desk_newer_log->confirmed = false;
                $cash_desk_newer_log->save();
            }
        }
    }
    
    public function getaccount()
    {
        
        return Account::getFromParam('accounting.codes.cash_desk', $this->year);
    }
    
    public function calcIn()
    {
        $amount = 0.00;
        if (isset($this->cash_desk_orders_in))
        {
            foreach ($this->cash_desk_orders_in as $order)
            {
                if (isset($order->cash_desk_items))
                {
                    foreach ($order->cash_desk_items as $item)
                    {
                        $amount += $item->amount;
                    }
                }
            }
        }

//        $amount = number_format ($amount, 2, "." , "");
        
        return $amount;
    }
    
    public function calcOut()
    {
        $amount = 0.00;
        if (isset($this->cash_desk_orders_out))
        {
            foreach ($this->cash_desk_orders_out as $order)
            {
                if (isset($order->cash_desk_items))
                {
                    foreach ($order->cash_desk_items as $item)
                    {
                        $amount += $item->amount;
                    }
                }
            }
        }

//        $amount = number_format ($amount, 2, "." , "");
        
        return $amount;
    }
    
}