<?php

class AdvanceIn extends Advance
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function defaultScope()
    {
        $alias = $this->getTableAlias(FALSE, FALSE);
        $pt_id = Yii::app()->configManager->get('accounting.advance_payment_type');
        return array(
            'condition' => "$alias.invoice=true and $alias.payment_type_id = $pt_id",
        );
    }

}
