<?php

class IosItem extends SIMAActiveRecord
{
    public $income_date;
    
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'accounting.ios_items';
    }

    public function moduleName()
    {
        return 'accounting';
    }

    public function relations($child_relations = [])
    {
        return array(
            'ios' => [self::BELONGS_TO, 'Ios', 'ios_id'],
            'bill' => [self::BELONGS_TO, 'Bill', 'bill_id']
        );
    }
    
    public function rules()
    {
        return [
            ['ios_id', 'required', 'on'=>['insert', 'update']], // bill_id nije required jer moze imati vrednost null
//            ['debit, credit, saldo', 'safe'], //MilosS: safe ide samo kada se popunjava iz forme
        ];
    }
    
    public function scopes()
    {
        $alias = $this->getTableAlias();
        $bill_table = Bill::model()->tableName();
        $uniq = SIMAHtml::uniqid();
        return [
            'opened' => [
                'condition' => "$alias.debit != $alias.credit"
            ],
            'byDateRelation' => [
                'join' => "left join $bill_table bt$uniq on $alias.bill_id = bt$uniq.id",
                'order' => "bt$uniq.income_date NULLS FIRST" //NULLS FIRST -> da bi pocetno stanje bilo na pocetku
            ],
            'byDate' => [
                'select' => "$alias.*, bt$uniq.income_date",
                'join' => "left join $bill_table bt$uniq on $alias.bill_id = bt$uniq.id",
                'order' => "bt$uniq.income_date NULLS FIRST" //NULLS FIRST -> da bi pocetno stanje bilo na pocetku
            ]
        ];
    }
    
    public function __get($column)
    {
        switch ($column)
        {
            case 'saldo': return $this->debit - $this->credit;
            case 'bill_number_display': 
                if (isset($this->bill))
                {
                    return $this->bill->bill_number;
                }
                else
                {
                    return 'Početno stanje';
                }
            case 'income_date_display': 
                if (isset($this->bill))
                {
                    return $this->bill->income_date;
                }
                else
                { 
                    $accunt_order_start = $this->ios->account->accounting_year->start_booking_order;
                    if(!empty($accunt_order_start))
                    {
                        return $accunt_order_start->date;
                    } else {
                        return '';
                    }
                        
                }
            case 'payment_date_display': 
                if (isset($this->bill))
                {
                    return $this->bill->payment_date;
                }
                else
                {
                    return '';//$this->ios->year->firstDay();
                }
//            case 'DisplayName': return $this->bill->bill_number;
//            case 'SearchName': return $this->bill->bill_number;
            default: return parent::__get($column);
        }
    }
    
    public function __isset($column)
    {
        switch ($column)
        {
            case 'saldo': return true;
            default: return parent::__isset($column);
        }
    }

    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters': return array(
                'ios'=>'relation',
                'ios_id'=>'dropdown',
                'payment_date'=>'date_range',
                'debit' => 'numeric',
                'credit' => 'numeric',
                'bill' => 'relation'
            );
            case 'textSearch':  return array(
                'name'=>'text',
                'filing'=>'relation',
                'document_type'=>'relation',
                'display_name' => 'text'
            );
            case 'default_order_scope': return 'byDate';
            case 'number_fields': return [
                'debit', 'credit', 'saldo'
            ];
            case 'options': return [];
            
            default: return parent::modelSettings($column);
        }
    }
}

