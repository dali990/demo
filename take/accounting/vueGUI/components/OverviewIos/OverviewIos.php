<script type="text/x-template" id="accounting-overview_ios"> 
    
    
    <div class="accounting-overview_ios">

        <div class="years">
            <ul>
                <li v-for="year_model in years" 
                    @click="selected_year_id = year_model.id" 
                    v-bind:class="{selected: (selected_year_id === year_model.id)}">
                    {{year_model.year}}
                </li>
            </ul>
        </div>
        <div class="customers" v-if="customers.length > 0">
            <h3>Kupci</h3>
            <div v-for="(partner, key) in customers" class="partner-ioses">
                {{++key}} - {{partner.DisplayName}}
                <hr>
                <accounting-ios
                    v-bind:model_filter="iosFilterBuyers(partner)">
                </accounting-ios>
            </div>
        </div>
        <div class="suppliers" v-if="suppliers.length > 0">
            <h3>Dobavljači</h3>
            <div v-for="(partner, key) in suppliers" class="partner-ioses">
                {{++key}} - {{partner.DisplayName}}
                <hr>
                <accounting-ios
                    v-bind:model_filter="iosFilterSuppliers(partner)">
                </accounting-ios>
            </div>
        </div>
      
    </div>    
    
</script>

