<script type="text/x-template" id="accounting-ios">
    
    
    <div class="accounting-ios">
        <div v-bind:class="{'slideup': selected_iosses.length > 1, 'slidedown': selected_iosses.length <= 1}">
            <sima-button-group>
                <div class="sima-model-option sima-btn" title="Obrišite" @click="multiselectDelete">
                    <span class="sima-icon sil-trash-alt"></span>
                </div>
                <div class="sima-model-option sima-btn" title="Potvrdite" @click="multiselectConfirm(true)">
                    <span class="sima-icon check sil-check"></span>
                </div>
                <div class="sima-model-option sima-btn" title="Odbijte" @click="multiselectConfirm(false)">
                    <span class="sima-icon times sil-times"></span>
                </div>
                <div class="sima-model-option sima-btn" title="Poništite potvrde" @click="multiselectConfirm(null)">
                    <span class="_not-ok _intermediate sima-icon _16"></span>
                </div>
            </sima-button-group>
        </div>
        <table class="ioses-table" style="width: 100%" ref="iosesTable" v-click-outside="clickOutside">
            <thead>
                <tr>
                    <td style="width: 35px">       
                    </td>
                    <td style="width: 70px">Datum</td>
                    <td>Konto</td>
                    <td class="text-center">Osporeni iznos</td>
                </tr>
            </thead>
            <tbody>
                <tr v-for="(ios, index) in iosses" @dblclick="openIosModel(ios.id)" @click="selectIosModel(ios.id, index, $event)">
                    <td class="status">
                        <sima-status-span v-bind:status="ios.partner_agreed"></sima-status-span>
                        <span class="sima-icon sil-trash-alt"
                            @click="deleteIos(ios.id)"
                            v-show="selected_iosses.includes(ios.id)"
                        ></span>
                    </td>
                    <td>
                        {{ios.ios_date}}
                    </td>
                    <td>
                        {{ios.account.DisplayName}}
                    </td>
                    <td class="text-center">
                        {{ios.partner_agreed_diff}}
                    </td>
                </tr>
            </tbody>
        </table>      
    </div>    
    
</script>