/* global sima, Vue */

Vue.component('accounting-ios', {
    template: '#accounting-ios',
    props:  {
//        id: {type: Number, defaul: ""},
//        model_id: {type: Number, defaul: ""},
//        model_name: {type:String, default: "Partner"},
        model_filter: {type:Object, default: null},
    },
    computed: {
        iosses(){
            var _this = this;
            return sima.vue.shot.getters.model_list(this, 'Ios', {
                model_filter: _this.model_filter
            });
        }
    },
    data: function () {
        return {
            selected_iosses: [],
            selected_rows: [],
            ios_table: null,
            opened_dialog: false
        };
    },
    mounted: function(){
        this.ios_table = this.$refs.iosesTable;
    },
    methods: {
        clickOutside(e){
            if(!e.target.closest('.sima-btn-group') && !this.opened_dialog)
            {
                for (var i = this.ios_table.rows.length - 1; i >= 0; i--) {
                    this.ios_table.rows[i].classList.remove('selected-row');
                }
                console.log("BRISI");
                this.selected_iosses = [];
                this.selected_rows = [];
            }
        },
        openIosModel(id){
            sima.icons.openModel('files/file',id);
        },
        selectIosModel(id, iosses_index, e){
            iosses_index++;
            var ios_table = this.$refs.iosesTable;
            var row = ios_table.rows[iosses_index];
            if(row.classList.contains("selected-row"))
            {
                row.classList.remove("selected-row");
                this.selected_rows = this.selected_rows.filter(_index => _index !== iosses_index);
                this.selected_iosses = this.selected_iosses.filter(_id => _id !== id);
            }
            else
            {
                if(e.ctrlKey === true)
                {
                    this.selected_iosses.push(id);
                    this.selected_rows.push(iosses_index);
                    row.classList.add('selected-row');
                }
                else
                {
                    this.selected_iosses = [id];
                    this.selected_rows = [iosses_index];

                    for (var i = ios_table.rows.length - 1; i >= 0; i--) {
                        ios_table.rows[i].classList.remove('selected-row');
                    }

                    row.classList.add('selected-row');
                }
            }
            console.log(this.selected_iosses);
        },
        multiselectConfirm(status=true){
            console.log('confirm');
            console.log(this.selected_iosses);
            var msg = sima.translate('ModelStatusConfirmMessage');
            if(!status)
            {
                msg = sima.translate('ModelStatusRevertMessage');
            }
                    
            var _this = this;
            _this.opened_dialog = true;
            sima.dialog.openYesNo(msg, function() {
                console.log(_this.selected_iosses);
                sima.ajax.get('accounting/ios/confirmIoses', {
                    async: true,
                    get_params: {
                        status: status
                    },
                    data: {
                        ioses_ids: _this.selected_iosses
                    },
                    success_function: function (response) {
                        for (var i = _this.ios_table.rows.length - 1; i >= 0; i--) {
                            _this.ios_table.rows[i].classList.remove('selected-row');
                        }
                        _this.opened_dialog = false;
                        _this.selected_iosses = [];
                        _this.selected_rows = [];
                        sima.dialog.close();
                    }
                });
            });
        },
        deleteIos(id){
            sima.icons.remove(null,'Ios',id,'');
        },
        multiselectDelete: function(){
            var _this = this;
            _this.opened_dialog = true;
            sima.dialog.openYesNo(sima.translate('IosAreYouSureYouWantToDeleteSelectedIoses'), function() {
                console.log(_this.selected_iosses);
                sima.ajaxLong.start('base/model/multiselectDelete', {
                    data: {
                        ids: _this.selected_iosses,
                        model: 'Ios'
                    },
                    showProgressBar: $(_this.$el),
                    onEnd:function(response){
                        for (var i = _this.ios_table.rows.length - 1; i >= 0; i--) {
                            _this.ios_table.rows[i].classList.remove('selected-row');
                        }
                        _this.opened_dialog = false;
                        _this.selected_iosses = [];
                        _this.selected_rows = [];
                    }
                });
                sima.dialog.close();
            });
        }
    }
});