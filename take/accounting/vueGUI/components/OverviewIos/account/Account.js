/* global sima, Vue */

Vue.component('accounting-account', {
    template: '#accounting-account',
    props:  {
//        id: {type: Number, defaul: ""},
//        model_id: {type: Number, defaul: ""},
//        model_name: {type:String, default: "Partner"},
        model_filter: {type:Object, default: null},
    },
    computed: {
        accounts(){
            var _this = this;
            return sima.vue.shot.getters.model_list(this, 'Account', {
                model_filter: _this.model_filter
            });
        }
    },
    data: function () {
        return {
            
        };
    },
    mounted: function(){

    },
    methods: {
        
    }
});