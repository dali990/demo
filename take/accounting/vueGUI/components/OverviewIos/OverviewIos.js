/* global sima, Vue */
/**
 * Srediti filtere, trenutno vraca konta i za dobavljace i za kupce
 */
Vue.component('accounting-overview_ios', {
    template: '#accounting-overview_ios',
    props:  {
        year_id: Number
    },
    computed: {
        years(){
            var criteria = {
                model_filter: {
                    filter_scopes: ['byYear']
                }
            };
            return sima.vue.shot.getters.model_list(null, 'AccountingYear', criteria);
        },
        partners(){
            var _this = this;
            return sima.vue.shot.getters.model_list(this, 'Partner', {
                model_filter: {
                    scopes: {
                        //accountYear: _this.id || _this.year_id,
                        listPartnersWithIos: _this.selected_year_id
                    },
                    //account: {
                        //year: {
                            //ids: _this.id || _this.year_id
                        //}
                    //}
                }
            });
        },
        //Potrebno je filtrirati konta, trenutno vraca konta i za dobavljace i za kupce
        suppliers(){
            var _this = this;
            return sima.vue.shot.getters.model_list(this, 'Partner', {
                model_filter: {
                    scopes: {
                        listSuppliersWithIos: _this.selected_year_id
                    }
                },
                order: 'display_name'
            });
        },
        customers(){
            var _this = this;
            return sima.vue.shot.getters.model_list(this, 'Partner', {
                model_filter: {
                    scopes: {
                        listCustomersWithIos: _this.selected_year_id
                    }
                },
                order: 'display_name'
            });
        }
    },
    watch: {
        selected_year_id(new_val, old_val){
            
        }
    },
    data: function () {
        return {
            selected_year_id: this.year_id,
            //partners: this.getPartners()
        };
    },
    mounted: function(){

    },
    methods: {
        accountFilter(partner){
            var filter = {
                model_id: partner.id,
                model_name: "Partner",
                year: {ids: this.selected_year_id}
            };
            return filter;
        },
        //dodati i filter za konta, trenutno vraca i dobavljace i kupce
        iosFilterBuyers(partner){
            var filter = {
                partner_id: partner.id,
                account: { 
                    year: {ids: this.selected_year_id},
                    scopes: ['onlyBuyers']
                }
            };
            return filter;
        },
        iosFilterSuppliers(partner){
            var filter = {
                partner_id: partner.id,
                account: { 
                    year: {ids: this.selected_year_id},
                    scopes: ['onlySuppliers']
                }
            };
            return filter;
        },
        getPartners(){
            var _this = this;
            this.partners = sima.vue.shot.getters.model_list(this, 'Partner', {
                model_filter: {
                    scopes: {
                        //accountYear: _this.id || _this.year_id,
                        listPartnersWithIos: _this.selected_year_id || _this.year_id
                    },
//                    account: {
//                        year: {
//                            ids: _this.id || _this.year_id
//                        }
//                    }
                }
            });
        }
    }
});