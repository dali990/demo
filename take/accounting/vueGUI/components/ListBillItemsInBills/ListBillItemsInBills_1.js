/* global sima, Vue */

Vue.component('accounting-list_bill_items_in_bills', {
    template: '#accounting-list_bill_items_in_bills',
    props:  {
//        model: {
//            validator: sima.vue.ProxyValidator
//        },
//        filter: String,
        action: String,
        action_data: Object
    },
    computed: {
        items_per_bills_list: function() {

            if (typeof this.action !== 'undefined')
            {
                var _list = sima.vue.shot.getters.custom_data(this, this.list_id, {
                    action: this.action,
                    action_data: this.action_data
                });
                $.each(_list, function(i, item){
                    _list[i].bill = sima.vue.shot.getters.model(item.bill_tag);
                });
                return _list;
            }
            
            return [];
        }
    },
    data: function () {
        return {list_id: sima.uniqid()};
    },
    mounted: function() {
        var _this = this;
        $(this.$el).on('destroyed', function(){
            _this.$destroy();
        });
    },
    updated: function () {
        this.$nextTick(function () {
            sima.layout.allignObject($(this.$el), 'TRIGGER_accounting-cost_location_tab-overview_updated');
        });
    },
    destroyed: function(){
        sima.vue.shot.commit('remove_custom_data',this.list_id);
    },
    methods: {
        
    }
});