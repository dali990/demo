<script type="text/x-template" id="accounting-list_bill_items_in_bills">
    
    <div class="sima-layout-panel">
        <table>
            <tr>
                <td style="width: 200px; text-align: center;">Racun</td>
                <td style="width: 200px; text-align: center;">stavke</td>
                <td style="width: 200px; text-align: center;">iznos</td>
            </tr>
            <tr class="accounting-bill_items_in_bills-bills_list" v-for="_item in this.items_per_bills_list">
                <td v-html="_item.bill.DisplayHTML"></td>
                <td>
                    <ul class="accounting-bill_items_in_bills-items_list" v-if="!_item.full_amount">
                        <li v-for="_bill_item in _item.bill.bill_items" v-bind:class="{_covered:_item.show_items.indexOf(_bill_item.id) === -1}">
                            {{_bill_item.DisplayName}}
                        </li>
                    </ul>
                </td>
                <td style="text-align: right; min-width: 200px" >
                    <table class="accounting-bill_items_in_bills-items_list" v-if="!_item.full_amount" style="width: 100%">
                        <tr v-for="_bill_item in _item.bill.bill_items">
                            <td 
                                style="text-align: right;" 
                                v-bind:class="{_covered:_item.show_items.indexOf(_bill_item.id)===-1}"
                            >
                                {{_bill_item.value | formatNumber}}
                            </td>
                        </tr>
                    </table>
                    <template v-if="_item.full_amount">
                            {{_item.bill.base_amount | formatNumber}}
                    </template>
                </td>
            </tr>
        </table>
    </div>    
    
</script>