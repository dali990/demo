<script type="text/x-template" id="accounting-overview_report">
    
    <div class="sima-layout-panel accounting-overview_report">
        <table class="time_selector_table">
            <tr class="years">
                <td>Godina: </td>
                <td>
                    <span 
                        v-for="year in this.years" 
                        @click="select_year($event, year)"
                        v-bind:class="{ _selected: (selected_years.indexOf(year) !== -1) }"
                        >
                        {{year}}
                    </span>
                </td>
            </tr>
            <tr class="months" v-bind:class="{_disabled:!can_select_months}">
                <td>Mesec: </td>
                <td>
                    <span 
                        v-for="month in this.months" 
                        @click="select_month($event, month)"
                        v-bind:class="{ _selected: (selected_months.indexOf(month) !== -1) }"
                        >
                        {{month}}
                    </span>
                </td>
            </tr>
        </table>
    
    
    
        <table style="width: 100%;">
            <tr >
                <td rowspan="2" style="width: 30px">#</td>
                <td rowspan="2">naziv</td>
                
                <th colspan="3">Prihod</th>
                
                
                <th colspan="3">Trosak</th>


            </tr>
            <tr>
                
                
                <td>ukupno</td>
                <td>neplaceno</td>
                <td>avans</td>
                <td>ukupno</td>
                <td>neplaceno</td>
                <td>avans</td>
            </tr>
            
            <tr class="_sum_row">
                <td></td>
                <td>SUMA</td>
                <td class="_number">{{data_sums.income_sum | formatNumber}}</td>
                <td class="_number">{{data_sums.income_notpayed_sum | formatNumber}}</td>
                <td class="_number">{{data_sums.income_advance_sum | formatNumber}}</td>
                <td class="_number">{{data_sums.cost_sum | formatNumber}}</td>
                <td class="_number">{{data_sums.cost_notpayed_sum | formatNumber}}</td>
                <td class="_number">{{data_sums.cost_advance_sum | formatNumber}}</td>
            </tr>
            
            <tr v-for="(_tcl, index)  in this.top_cost_locations" class="_data_row">
                <td>{{index+1}}</td>
                <td><span v-html="_tcl.cost_location.DisplayHTML" ></span> - {{_tcl.cost_location.work_name}}</td>
                    
                <td v-for="column in ['income','income_notpayed','income_advance','cost','cost_notpayed','cost_advance']" class="_number">

                    <span   class="sima-display-html sima-model-link" 
                            @click="preview_specification(column,_tcl.cost_location.id)">
                            {{_tcl[column]  | formatNumber}}
                     </span>
                </td>
            </tr>
            
            
        </table>
        
    </div>    
    
</script>