/* global sima, Vue */

Vue.component('accounting-overview_report', {
    template: '#accounting-overview_report',
    props:  {
//        file_tag: String
    },
    computed: {
        can_select_months: function(){
            return this.selected_years.length === 1;
        },
        top_cost_locations: function() {

            var _top_cost = sima.vue.shot.getters.custom_data(this, this._report_list_uniq_key, {
                action: 'accounting/report/sum',
                action_data: {
                    group_by: ['cost_location'],
                    model_filter: {
                        cost_location: {
                            scopes: ['topParents']
                        },
                        
                        month: {ids: this.can_select_months ? this.selected_months : [] },
                        year: {ids: this.selected_years},
                    },
                    report_type: 'total_r'
//                        group_by: []
                }
            });
                
            for (var item_i in _top_cost)
            {
                var element = _top_cost[item_i];
                element.DisplayName = '';
                if (element.cost_location_tag !== '')
                {
                    element.cost_location = sima.vue.shot.getters.model(element.cost_location_tag);
                    element.DisplayName += element.cost_location.DisplayName;
                }
            }

            return _top_cost;
        },
        data_sums: function(){
            var _local_sum = {
                income_sum: 0,
                income_notpayed_sum: 0,
                income_advance_sum: 0,
                cost_sum: 0,
                cost_notpayed_sum: 0,
                cost_advance_sum: 0
            };
            for(var i in this.top_cost_locations)
            {
                _local_sum.income_sum           += Number(this.top_cost_locations[i].income);
                _local_sum.income_notpayed_sum  += Number(this.top_cost_locations[i].income_notpayed);
                _local_sum.income_advance_sum   += Number(this.top_cost_locations[i].income_advance);
                _local_sum.cost_sum             += Number(this.top_cost_locations[i].cost);
                _local_sum.cost_notpayed_sum    += Number(this.top_cost_locations[i].cost_notpayed);
                _local_sum.cost_advance_sum     += Number(this.top_cost_locations[i].cost_advance);
            }
            return _local_sum;
        },
        _report_list_uniq_key: function(){
            return JSON.stringify(this.selected_years) + JSON.stringify(this.selected_months);
        }
    },
    data: function () {
        return {
//            _report_list_uniq_key: sima.uniqid(),
            years: [2014, 2015, 2016, 2017, 2018, 2019],
            months: [1,2,3,4,5,6,7,8,9,10,11,12],
            selected_years: [new Date().getFullYear()],
            selected_months: []
        };
    },
    mounted: function(){

    },
    methods: {
        preview_specification: function(filter_type, cost_location_id){
            sima.accounting.openReportSpecification({
                action: 'accounting/report/specification',
                action_data: { //filter
                    model_filter: {
                        has_value_on: [filter_type],
                        cost_location: {
                            ids: [cost_location_id]
                        },
                        month: {ids: this.can_select_months ? this.selected_months : [] },
                        year: {ids: this.selected_years}
                    },
                    report_type: 'total_r'
                },
                show_values: [filter_type]
            });
        },
        select_year: function(event, year){

            if (event.ctrlKey)
            {
                var index = this.selected_years.indexOf(year);
                if (index === -1) 
                {
                    this.selected_years.push(year);
                    this.selected_years.sort();//da ne bi dovlacio [2016,2017] i [2017,2016] kao razlicito
                } 
                else 
                {
                    this.selected_years.splice(index, 1);
                }
            }
            else
            {
                this.selected_years = [year];
                this.selected_months = [];
            }
            
            
            if (!this.can_select_months)
            {
                this.selected_months = [];
            }
        },
        select_month: function(event, month){
            if (!this.can_select_months)
            {
                return;
            }
            
            if (event.ctrlKey)
            {
                var index = this.selected_months.indexOf(month);

                if (index === -1) 
                {
                    this.selected_months.push(month);
                    this.selected_months.sort();//da ne bi dovlacio [1,2] i [2,1] kao razlicito
                } 
                else 
                {
                    this.selected_months.splice(index, 1);
                }
            }
            else
            {
                this.selected_months = [month];
            }
            
            
           
        },
    }
});