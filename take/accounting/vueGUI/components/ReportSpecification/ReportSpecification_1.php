<script type="text/x-template" id="accounting-report_specification">
    
    <div class="sima-layout-panel accounting-report_specification">
        <div class="sima-layout-fixed-panel">
            <h2>{{this.component_title}}</h2>
        </div>
        <div class="sima-layout_panel _horizontal _splitter">
        
        
                <div class="sima-layout_panel" >
                    <h3>Racuni</h3>
                    <table>
                        <tr>
                            <th rowspan="2" style="width: 20px; text-align: left;">#</th>
                            <th rowspan="2" style="width: 200px; text-align: left;">Racun</th>
                            <th colspan="4" style="width: 820px; text-align: left;">Stavke</th>
                            <th v-for="value_to_show in show_values" rowspan="2" style="width: 150px; text-align: right;">
                                {{value_to_show}}
                            </th>
                        </tr>
                        <tr>
                            <th style="width: 20px; text-align: left; font-size: 0.8em">#</th>
                            <th style="width: 500px; text-align: left; font-size: 0.8em">Naziv</th>
                            <th style="width: 150px; text-align: right; font-size: 0.8em">Iznos</th>
                            <th style="width: 150px; text-align: right; font-size: 0.8em">PDV</th>
                        </tr>
                        <tr v-for="(bill, bill_id, index)  in this.bills" class="bill_row" >
                            <td>{{index}}</td>
                            <td><sima-model-display-html :model='bill.model'></sima-model-display-html></td>
                            <td>
                                <ul class="bill_item_list" v-if="!bill.full_amount">
                                    <li v-for="_bill_item in bill.model.scoped('bill_items',['byOrder'])" v-bind:class="{_covered:bill.included_items.indexOf(_bill_item.id) === -1}">
                                        {{_bill_item.order}}
                                    </li>
                                </ul>
                            </td>
                            <td>
                                <ul class="bill_item_list" v-if="!bill.full_amount">
                                    <li v-for="_bill_item in bill.model.scoped('bill_items',['byOrder'])" v-bind:class="{_covered:bill.included_items.indexOf(_bill_item.id) === -1}">
                                        {{_bill_item.DisplayName}}
                                    </li>
                                </ul>
                            </td>
                            <td style="text-align: right; min-width: 120px" >
                                <table class="bill_item_list" v-if="!bill.full_amount" style="width: 100%">
                                    <tr v-for="_bill_item in bill.model.scoped('bill_items',['byOrder'])">
                                        <td 
                                            style="text-align: right;" 
                                            v-bind:class="{_covered:bill.included_items.indexOf(_bill_item.id)===-1}"
                                        >
                                            {{_bill_item['value'] | formatNumber}}
                                        </td>
                                    </tr>
                                </table>

                            </td>
                            <td style="text-align: right; min-width: 120px" >
                                <table class="bill_item_list" v-if="!bill.full_amount" style="width: 100%">
                                    <tr v-for="_bill_item in bill.model.scoped('bill_items',['byOrder'])">
                                        <td 
                                            style="text-align: right;" 
                                            v-bind:class="{_covered:bill.included_items.indexOf(_bill_item.id)===-1}"
                                        >
                                            {{_bill_item['value_vat'] | formatNumber}}
                                        </td>
                                    </tr>
                                </table>

                            </td>
                            <td v-for="value_to_show in show_values" style="text-align: right; min-width: 200px" >
                                {{bill[value_to_show] | formatNumber}}
                            </td>
                        </tr>
                        
                        <tr class="bill_sum_row">
                            <th colspan="6" style="text-align: centar;">SUMA</th>
                            <th v-for="value_to_show in show_values" style="font-size: 1.1em; text-align: right;">
                                {{bills_sum[value_to_show] | formatNumber}}
                            </th>
                        </tr>
                        
                    </table>
                    
                    <template v-if="this.has_payments">
                        <h3>Uplate</h3>
                        <table>

                            <tr>
                                <th colspan='2'>#</th>
                                <!--<th></th>-->
                                <th>Datum</th>
                                <th>Iznos</th>
                                <th>Komentar</th>
                                <th>Izvod iz banke</th>
                            </tr>

                            <tr v-for="(payment, payment_id, index)  in this.payments" class="bill_row" >
                                <td>{{index}}</td>
                                <td><sima-model-open class="icon-only" :model='payment.model'></sima-model-open></td>
                                <td>{{payment.model.payment_date}}</td>
                                <td style="text-align: right; min-width: 100px" >{{payment.model.amount | formatNumber}}</td>
                                <td style="min-width: 100px" >{{payment.model.comment}}</td>
                                <td><sima-model-display-html :model='payment.model.bank_statement'></sima-model-display-html></td>
                            </tr>

                            <tr class="bill_sum_row">
                                <th colspan='3'>SUMA</th>
                                <th style="text-align: right;">{{this.payments_sum | formatNumber}}</th>
                                <th colspan='2'></th>
                            </tr>

                        </table>
                    </template>
                </div>
                
        </div>
    </div>    
    
</script>