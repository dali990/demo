/* global sima, Vue */

Vue.component('accounting-report_specification', {
    template: '#accounting-report_specification',
    props:  {
        action: String, //akcija gde se izracunava niz
        action_data: Object, //parametri akcije
        show_values: Array //niz "kolona" koje treba da se prikazu -> npr: 'cost','cost_notpayed','cost_advance'
    },
    computed: {
        
        specification_items: function() {
            return sima.vue.shot.getters.custom_data(this, JSON.stringify(this.report_specification_uniq_key), {
                action: this.action,
                action_data: this.action_data
            });
        },
        bills: function(){
            if (typeof this.specification_items.Bills === 'undefined')
            {
                return [];
            }
            
            for(var bill_id in this.specification_items.Bills)
            {
                var _bill = Object.assign({}, this.specification_items.Bills[bill_id]);
                //vracaju se isti podaci i moguce je da se podaci setuju u VueShot
                if (typeof _bill.model === 'undefined')
                {
                    this.specification_items.Bills[bill_id].model = sima.vue.shot.getters.model(_bill.bill_tag);
                    this.specification_items.Bills[bill_id].full_amount = 
                            this.specification_items.Bills[bill_id].included_items.length
                            ===
                            this.specification_items.Bills[bill_id].bill_items_count;
                }
                
            }
            return this.specification_items.Bills;
        },
        payments: function(){
            if (typeof this.specification_items.Payments === 'undefined')
            {
                return [];
            }
            
            for(var payment_id in this.specification_items.Payments)
            {
                var _payment = Object.assign({}, this.specification_items.Payments[payment_id]);
                //vracaju se isti podaci i moguce je da se podaci setuju u VueShot
                if (typeof _payment.model === 'undefined')
                {
                    this.specification_items.Payments[payment_id].model = sima.vue.shot.getters.model(_payment.payment_tag);
                }
                
            }
            return this.specification_items.Payments;
        },
        bills_sum: function(){
            var sums = {};
            for(var _iii in this.show_values)
            {
                sums[this.show_values[_iii]] = 0;
            }
            for(var bill_id in this.specification_items.Bills)
            {
                for(var _iii in this.show_values)
                {
                    sums[this.show_values[_iii]] += this.specification_items.Bills[bill_id][this.show_values[_iii]];
                }
            }
            return sums;
        },
        payments_sum: function(){
            var _sum = 0;
            for(var payment_id in this.specification_items.Payments)
            {
                _sum += this.specification_items.Payments[payment_id].model.amount;
            }
            return _sum;
        },
        has_bills: function(){
            return Object.keys(this.bills).length > 0;
        },
        has_payments: function(){
            return Object.keys(this.payments).length > 0;
        }
    },
    data: function () {
        return {
            component_title: sima.translate('AccountingReportSpecification'),
            report_specification_uniq_key: JSON.stringify({a:this.action,ad:this.action_data})//kesiranje
        };
    },
    mounted: function() {
        var _this = this;
        $(this.$el).on('destroyed', function(){
            _this.$destroy();
        });
    },
    updated: function () {
        this.$nextTick(function () {
            sima.layout.allignObject($(this.$el), 'TRIGGER_accounting-report_specification');
        });
    },
    destroyed: function(){
//        sima.vue.shot.commit('remove_custom_data',this.list_id);
    },
    methods: {
        
    }
});