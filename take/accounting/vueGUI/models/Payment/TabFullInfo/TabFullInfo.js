/* global Vue, sima */

Vue.component('accounting-payment_tab-full_info',{
    props:  {
        model: {validator: sima.vue.ProxyValidator}
    },
    computed: {
        released_percent: function() {
            return 100 - 100 * this.model.unreleased_amount / this.model.amount ;
        },
        posible_bills: function() {
            if (this.model.partner === '')
            {
                return [];
            }
            return this.model.partner.scoped(
                    this.model.invoice?'bills_out':'bills_in',
                    ['unreleased']
            );
        },
        //ovo su debitor i creditor
        //za ovo ide pun iznos duga iz ugovora
        posible_debt_take_overs: function() {
            if (this.model.partner === '')
            {
                return [];
            }
            var contracts = [];
            if (this.model.invoice)//UPLATA
            {
                //placanje izlazne fakture
                //A - conn2
                var local_contracts1 = this.model.partner.scoped('debt_take_over_as_takes_over',['unreleased', 'systemCompanyCreditor']);
                for(var i in local_contracts1)
                {
                    contracts.push({
                        contract: local_contracts1[i],
                        unreleased_value: local_contracts1[i].unreleased_take_over_value
                    });
                }
                //C - conn3
                var local_contracts2 = this.model.partner.scoped('debt_take_over_as_debtor',['unreleased', 'systemCompanyTakesOver']);
                for(var i in local_contracts2)
                {
                    contracts.push({
                        contract: local_contracts2[i],
                        unreleased_value: local_contracts2[i].unreleased_debt_value
                    });
                }

            }
            else//ISPLATA
            {
                //B - conn3
                var local_contracts1 = this.model.partner.scoped('debt_take_over_as_takes_over',['unreleased', 'systemCompanyDebtor']);
                for(var i in local_contracts1)
                {
                    contracts.push({
                        contract: local_contracts1[i],
                        unreleased_value: local_contracts1[i].unreleased_take_over_value
                    });
                }
                //C -conn2
                var local_contracts2 = this.model.partner.scoped('debt_take_over_as_creditor',['unreleased', 'systemCompanyTakesOver']);
                for(var i in local_contracts2)
                {
                    contracts.push({
                        contract: local_contracts2[i],
                        unreleased_value: local_contracts2[i].unreleased_take_over_value
                    });
                }

            }
            return contracts;
            
        }
    },
    watch: {
//        cost_locations: function(oldVal, newVal){
//            this.selected_cost_location_id = null;
//            this.selected_bill_items = [];
//        }
    },
    data: function () {
        return {
//            selected_cost_location_id: null,
////            selected_cost_locations: [],
//            selected_bill_items: []
        };
    },
    mounted: function() {
        var _this = this;
        $(this.$el).on('destroyed', function(){
            _this.$destroy();
        });
    },
    updated: function () {
        this.$nextTick(function () {
            sima.layout.allignObject($(this.$el), 'TRIGGER_tab_bill_releasing_updated');
        });
    },
    
    methods: {
        liq_by_bill:function(bill_id)
        {
            sima.ajax.get('accounting/billReleasing/add',{
                data:{
                    bill_ids: [bill_id],
                    payment_ids: [this.model.id]
                }
            });
        },
        liq_by_debt:function(debt_take_over_id)
        {                      
            sima.ajax.get('accounting/billReleasing/addDebtTakeOverToPayment',{
                data:{
                    payment_ids: [this.model.id],
                    debt_take_over_ids: [debt_take_over_id]
                }
            });
        }
    },
    template: '#accounting-payment_tab-full_info'
});
