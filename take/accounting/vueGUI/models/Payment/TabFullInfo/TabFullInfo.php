<?php $uniq = SIMAHtml::uniqid()?>

<script type="text/x-template" id="accounting-payment_tab-full_info">

    <div class='sima-layout-panel _splitter _vertical'>
    
        <div class='sima-layout-panel' data-sima-layout-init='{"proportion":0.3}'>
            
            <h2>Placanje likvidirano: {{this.released_percent | formatNumber}} %</h2>
                
            <h3>Racuni: </h3>
            <table>
                <tr v-for='release in this.model.releases' :key='release.id'>
                    <td v-html="release.bill.DisplayHTML"></td>
                    <td><sima-model-options v-bind:model="release"></sima-model-options></td>
                </tr>
            </table>
         
            <h3>Ugovori o preuzimanju duga: </h3>
            <table>
                <tr v-for='payment_debt_take_over in this.model.payment_debt_take_overs' :key='payment_debt_take_over.id'>
                    <td v-html="payment_debt_take_over.debt_take_over.DisplayHTML"></td>
                    <td><sima-model-options v-bind:model="payment_debt_take_over"></sima-model-options></td>
                </tr>
            </table>

                

        </div>
        
        <div class='sima-layout-panel'>
        
            <h2>Mogućnosti likvidacije: </h2>
                
            <h3>Ne placeni racuni: </h3>
            <table>
                <tr v-for='bill in this.posible_bills' :key='bill.id'>
                    <td v-html="bill.DisplayHTML" ></td>
                    <td></td>
                    <td><sima-button 
                        @click='liq_by_bill(bill.id)'
                        security_question="Da li ste sigurni da želite da dodate ova plaćanja?"
                        >povezi</sima-button></td>
                    <td></td>
                </tr>
            </table>
            
            <h3>Ugovor o preuzimanju duga: </h3>
            <table>
                <tr>
                    <td>Ugovor</td>
                    <td><?=Yii::t('AccountingModule.DebtTakeOver','TakeOverValue')?></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr v-for='debt_take_over in this.posible_debt_take_overs' :key='debt_take_over.id'>
                    <td v-html="debt_take_over.contract.DisplayHTML" ></td>
                    <td>{{debt_take_over.unreleased_value}}</td>
                    <td><sima-button 
                        @click='liq_by_debt(debt_take_over.contract.id)'
                        security_question="Da li ste sigurni da želite da dodate ovaj ugovor?"
                        >povezi</sima-button></td>
                    <td></td>
                </tr>
            </table>
            
          
        </div>
    
    </div>

</script>
