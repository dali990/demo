/* global Vue, sima */

Vue.component('accounting-file_tab-popdv',{
    props:  {
        model: {validator: sima.vue.ProxyValidator},
        booking_type: String
    },
    computed: {
        dataPresent: function(){
            this.model.account_document;
            if ( this.model.shotStatus('account_document') !== 'INIT' )
            {
                this.model.account_document.popdv_manual;
                var param = 'VatInMonth';
                if (this.booking_type === 'REGULAR')
                {
                    this.model.account_document.VatInMonth;
                    
                }
                else if (this.booking_type === 'CANCELED')
                {
                    this.model.account_document.CanceledInMonth;
                    param  = 'CanceledInMonth';
                }
                
                
                if (this.model.account_document.shotStatus(param) !== 'INIT')
                {
                    var month = null
                    if (this.booking_type === 'REGULAR')
                    {
                        month = this.model.account_document.VatInMonth;

                    }
                    else if (this.booking_type === 'CANCELED')
                    {
                        month = this.model.account_document.CanceledInMonth;
                    }
                    month.DisplayName;
                    month.month;
                    month.year;
                    if (month.shotStatus('year') !== 'INIT')
                    {
                        month.year.year;
                        return this.model.account_document.shotStatus('popdv_manual') !== 'INIT'
                            && month.year.shotStatus('year') !== 'INIT'
                            && month.shotStatus('DisplayName') !== 'INIT'
                            && month.shotStatus('month') !== 'INIT';
                    }
                }
            }
            
            return false;
        },
        dataTrue: function(){
            this.model.account_document;
            if ( this.model.shotStatus('account_document') === 'OK' )
            {
                this.model.account_document.popdv_manual;
                var param = 'VatInMonth';
                if (this.booking_type === 'REGULAR')
                {
                    this.model.account_document.VatInMonth;
                    
                }
                else if (this.booking_type === 'CANCELED')
                {
                    this.model.account_document.CanceledInMonth;
                    param  = 'CanceledInMonth';
                }
                
                
                if (this.model.account_document.shotStatus(param) === 'OK')
                {
                    var month = null
                    if (this.booking_type === 'REGULAR')
                    {
                        month = this.model.account_document.VatInMonth;

                    }
                    else if (this.booking_type === 'CANCELED')
                    {
                        month = this.model.account_document.CanceledInMonth;
                    }
                    month.DisplayName;
                    month.month;
                    month.year;
                    if (month.shotStatus('year') === 'OK')
                    {
                        return this.model.account_document.shotStatus('popdv_manual') === 'OK'
                            && month.year.shotStatus('year') === 'OK'
                            && month.shotStatus('DisplayName') === 'OK'
                            && month.shotStatus('month') === 'OK';
                    }
                }
            }
            return false;
        },
        
        display_month: function() {
//            if (this.model.shotDataPresent(['account_document.VatInMonth']))
            if (this.model.shotStatus('account_document') !== 'INIT')
            {
                if (this.booking_type === 'REGULAR')
                {
                    if (this.model.account_document.shotStatus('VatInMonth') !== 'INIT')
                    {
                        return this.model.account_document.VatInMonth;
                    }
                }
                else if (this.booking_type === 'CANCELED')
                {
                    if (this.model.account_document.shotStatus('CanceledInMonth') !== 'INIT')
                    {
                        return this.model.account_document.CanceledInMonth;
                    }
                }
            }
            return null;
        },
        isBooked: function() {
            return this.dataPresent && 
                    (typeof this.model.account_document === 'object') ;
        },
        POPDV_report_button_ready: function(){
            return this.dataTrue && !this.POPDV_report_loading_in_progress;
        },
        popdv_manual: function(){
            return this.dataTrue && this.model.account_document.popdv_manual;
        },
        manual_icon_class: function(){
            return this.popdv_manual?'fa-toggle-on':'fa-toggle-off';
        },
        security_question: function(){
            return this.popdv_manual?"Ovom akcijom brišete ručno izmenje stavke. Nastavljate?":false;
        },
        used_params: function(){
            var result = [];
            for(var i in this.filled_params)
            {
                var _code = this.filled_params[i];
                if (this.manual_edited_params.indexOf(_code)===-1)
                {
                    result.push(_code);
                }
                else
                {
                    result.push(_code+"*");
                }
            }
            
            for(var i in this.manual_edited_params)
            {
                var _code = this.manual_edited_params[i];
                if (this.filled_params.indexOf(_code)===-1)
                {
                    result.push("-"+ _code +"-");
                }
            }
            return result;
        }
    },
    watch: {
        dataTrue: function(){
            this.private_autoReloadReport();
        },
        POPDV_report_loaded: function(){
            this.private_autoReloadReport();
        },
        display_month: function(new_value, old_value){
            if (new_value === null)
            {
                this.POPDV_report_loaded = false;
            }
            else if (old_value !== null && new_value !== null)
            {   
                if (old_value.id !== new_value.id)
                {
                    this.POPDV_report_loaded = false;
                }
            }
        },
        popdv_manual: function(new_value, old_value){
            if (new_value === '')
            {
                this.POPDV_report_loaded = false;
            }
            else if (old_value !== '' && new_value !== '')
            {   
                if (old_value !== new_value)
                {
                    this.POPDV_report_loaded = false;
                }
            }
        },
    },
    data: function () {
        return {
            t:{
                Accounting_BookedInMonth: sima.translate('Accounting_BookedInMonth'),
                Accounting_NotBooked: sima.translate('Accounting_NotBooked'),
                Accounting_AutoBooking: sima.translate('Accounting_AutoBooking'),
                Accounting_ManualBooking: sima.translate('Accounting_ManualBooking'),
                Refresh: sima.translate('Refresh')
            },
            POPDV_report_loaded: false,//da li je inicijalno loadovan
            POPDV_report_loading_in_progress: false,
            POPDV_report_content: "",
            filled_params: [],
            manual_edited_params: [],
            sums_params: []
//            selected_cost_location_id: null,
////            selected_cost_locations: [],
//            selected_bill_items: []
        };
    },
    mounted: function() {
        var _this = this;
        $(this.$el).on('destroyed', function(){
            _this.$destroy();
        });
        this.private_autoReloadReport();
    },
    updated: function () {
        this.$nextTick(function () {
            sima.layout.allignObject($(this.$el), 'TRIGGER_accounting-file_tab-popdv');
        });
    },
    
    methods: {
        private_autoReloadReport: function(){
            if (this.dataTrue && !this.POPDV_report_loaded)
            {
                this.POPDV_report_loaded = true;
                this.loadPOPDVReport();
            }
        },
        loadPOPDVReport:function()
        {    
            this.POPDV_report_loading_in_progress = true;
            if (!this.dataTrue)
            {
                sima.dialog.openWarn('Podaci nisu ispravno ucitani');
                return;
            }
            var _component = this;
            var _loadingCircle = null;
            if (typeof this.$refs.POPDV_report_content !== 'undefined')
            {
                _loadingCircle = $(this.$refs.POPDV_report_content);
            }
            else
            {
                _loadingCircle = $(this.$el);
            }
            
            
            sima.ajax.get('accounting/vat/getPOPDVDisplayHTML',{
                async:true,
                loadingCircle: _loadingCircle,
                get_params:{
                    month: _component.display_month.month,
                    year: _component.display_month.year.year,
                    account_document_id: _component.model.account_document.id,
                    booking_type: _component.booking_type
                },
                success_function: function(response){
                    _component.POPDV_report_content = response.html;
                    _component.POPDV_report_loading_in_progress = false;
                    _component.filled_params = response.filled_params;
                    _component.manual_edited_params = response.manual_edited_params;
                    _component.sums_params = response.sums_params;
                }
            });
        },
        toggle_manual: function(){
            var _new_value = !this.popdv_manual;
            var _component = this;
            this.model.account_document.popdv_manual = _new_value;
            sima.ajax.get('accounting/vat/switchPOPDVToManual',{
                async: true,
                get_params: {
                    account_document_id: _component.model.account_document.id,
                    manual: _new_value
                },
                failure_function: function(response){
                    sima.vue.shot.commit('flush_wait_for_dirty');
                    sima.dialog.openWarn(response.message);
                    sima.model.updateViews(response.updateModelViews,{async:'true'});
                }
            });
        }        
    },
    template: '#accounting-file_tab-popdv'
});
