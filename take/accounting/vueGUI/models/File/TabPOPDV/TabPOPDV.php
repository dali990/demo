<?php $uniq = SIMAHtml::uniqid()?>

<script type="text/x-template" id="accounting-file_tab-popdv">

    <div class='accounting-file_tab-popdv sima-layout-panel _horizontal'>
        <template v-if="!dataPresent">
            ucitava se
        </template>
        
        <template v-else-if="isBooked">
            
            <div class="sima-layout-fixed-panel" style="height: 110px; border-bottom: 1px solid lightgray;" data-sima-layout-init='{"height_cut":1}'>
                <div style="float:left;" >
                    <p>
                        <span style="font-weight: bold; font-size: 1.5em; margin: 5px;">
                            {{t.Accounting_BookedInMonth}} {{this.display_month.DisplayName}}
                        </span>
                        <sima-button v-bind:class="{_disabled:!POPDV_report_button_ready}" @click="loadPOPDVReport">{{t.Refresh}}</sima-button>
                    </p>
                        
                    <div style="float: left" v-bind:class="{_disabled:!POPDV_report_button_ready, _manual_off: !popdv_manual}" >
                        Ručno izmenjen POPDV: 
                            <sima-button class="_no_css" 
                                            v-bind:class="{_disabled:!POPDV_report_button_ready}" 
                                            @click="toggle_manual"
                                            v-bind:security_question="security_question"
                                            >
                                <i class="fas" v-bind:class="manual_icon_class" 
                                    v-bind:style="{color: popdv_manual?'green':'black'}"
                                    style="font-size:24px;"
                                    ></i>
                            </sima-button>
                        
                    </div>
                    <div v-if="popdv_manual" style="font-size:0.8em; float:right">
                        <span style="display:inline-block; width:30px;">*</span> - modifikovana polja<br />
                        <span style="display:inline-block; width:30px;">-XX-</span> - obrisana polja
                    </div>  

                </div>

                <div style="float: left; margin-left: 20px;">
                    <p>Podaci iskazani u poljima:</p>
                    <p>{{used_params.join(', ')}}</p>
                    <p>{{sums_params.join(', ')}}</p>
                </div>
             </div>

             <div class="sima-layout-panel">
                 <div 
                         ref="POPDV_report_content" 
                         class="sima-layout-panel" 
                         style="padding: 20px;" 
                         data-sima-layout-init='{"width_cut":40, "height_cut":40}'
                     >
                     <sima-iframe  class="sima-layout-panel">{{POPDV_report_content}}</sima-iframe>
                 </div>
             </div>
                
        </template>
        
        <template v-else>
           {{t.Accounting_NotBooked}}
        </template>
        
    
    </div>

</script>
