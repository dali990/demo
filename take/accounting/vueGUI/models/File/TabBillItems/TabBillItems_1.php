
<script type="text/x-template" id="accounting-file_tab-bill_items">


<!-- file_tab_bill_items -->
<div class='file_tab_bill_items sima-layout-panel _splitter _vertical'> 

    <!-- tab_bill_head_body_footer -->
    <div class='sima-layout-panel _horizontal'>
        <!-- tab_bill_head -->
        <div class='sima-layout-fixed-panel tab_bill_head'>                    
            <div class="flex" style="justify-content: space-between;">
                <div 
                    v-bind:title="padlock_icon_title"
                    class="locked-value">                        
                    <h2><?=Yii::t('AccountingModule.BillItem', 'BillItems')?></h2>
                    <div><i class="sima-icon" v-bind:class="this.padlock_icon"></i></div>
                </div>

                <div v-if="!this.read_only" class="buttons-group">

                    <template  v-if="selected_bill_items.length > 0">
                        <!-- Obrišite (global)-->
                        <sima-button 
                            v-on:click="multiselectDelete"
                            title="<?=Yii::t('AccountingModule.Bill', 'Delete')?>"
                            >
                            <span class="sima-icon sil-trash-alt"></span><hr>
                            <span class="sima-btn-title"><?=Yii::t('AccountingModule.Bill', 'Delete')?></span>
                        </sima-button><!-- END Obrišite (global)-->
                        
                        <sima-button-group>
                            <span slot="main_icon" class="sima-btn-title"><?=Yii::t('AccountingModule.BillItem', 'Convert')?></span>
                            <sima-button 
                                v-on:click="convertToWarehouse"
                                title="<?=Yii::t('AccountingModule.BillItem', 'CarryOverInWarehouse')?>"
                                v-if="this.model.isBillWT"
                            >
                                <span class="sima-icon sil-boxes"></span><hr>
                                <span class="sima-btn-title"><?=Yii::t('AccountingModule.BillItem', 'InMaterialTraffic')?></span>
                                <!-- <span class="sima-btn-title"><?=Yii::t('AccountingModule.BillItem', 'TransferToWarehouse')?></span> -->
                            </sima-button>
                            <!-- Dodaj osnovna sredstva -->
                            <sima-button 
                                v-on:click="convertToFixedAssets"
                                title="<?=Yii::t('AccountingModule.BillItem', 'CarryOverInFixedAssets')?>"
                            >
                                <span class="sima-icon sil-car-building-alt"></span><hr>
                                <span class="sima-btn-title"><?=Yii::t('AccountingModule.BillItem', 'InFixedAssets')?></span>
                                <!-- <span class="sima-btn-title"><?=Yii::t('AccountingModule.Bill', 'AddBillItemsToFixedAssets')?></span> -->
                            </sima-button><!-- END Dodaj osnovna sredstva -->
                        </sima-button-group>
                    </template>
                    <template  v-if="!model.lock_amounts">
                        <!-- Dodaj stavku -->
                        <sima-button
                            class="add_button button button1 access"
                            v-bind:class="add_item_btn_ready"
                            title="<?=Yii::t('AccountingModule.BillItem', 'AddNewItem')?>"
                            v-on:click="addItem('')"
                        >
                            <i class="sima-icon sil-layer-plus"></i><hr>
                            <span class="sima-btn-title"><?=Yii::t('AccountingModule.BillItem', 'AddItem')?></span>
                        </sima-button><!-- END Dodaj stavku -->
                    </template>

                    <!-- Otkljucaj / Zakljucaj -->
                    <sima-button 
                        v-bind:id="component_uniq_id + '_yes'"
                        v-bind:class="{'_pressed':this.model.lock_amounts}"
                        v-bind:title="padlock_btn_title"
                        v-on:click="lockUnlock"
                    >
                        <i class="sima-icon" v-bind:class="this.padlock_icon_inverse"></i><hr>
                        <span class="sima-btn-title">{{padlock_btn_title}}</span>
                    </sima-button><!-- END Otkljucaj / Zakljucaj -->
                </div><!-- end buttons-group -->
            </div>

            <div class="table-header">             
                <span class="id-col" title="<?=Yii::t('AccountingModule.BillItem', 'SerialNumber')?>" style="text-align: center;"><?=Yii::t('AccountingModule.BillItem', 'AbbreviationSerialNumber')?></span>
                <span class="name-col" title="<?=Yii::t('AccountingModule.BillItem', 'Name')?>"><?=Yii::t('AccountingModule.BillItem', 'Name')?></span>
                <span class="quantity-col" title="<?=Yii::t('AccountingModule.BillItem', 'Amount')?>"><?=Yii::t('AccountingModule.BillItem', 'Amount')?></span>
                <!-- <span class="unit-col"></span> -->
                <span class="measure-col" title="<?=Yii::t('AccountingModule.BillItem', 'PricePerUnit')?>"><?=Yii::t('AccountingModule.BillItem', 'AbbreviationPricePerUnit')?></span>
                <span class="discount-col" title="<?=Yii::t('AccountingModule.BillItem', 'Discount')?>" v-if="this.show_discount"><?=Yii::t('AccountingModule.BillItem', 'Discount')?></span>
                <span class="value-col" title="<?=Yii::t('AccountingModule.BillItem', 'Value')?>"><?=Yii::t('AccountingModule.BillItem', 'Value')?></span>
                <span class="pdv-value-col" title="<?=Yii::t('AccountingModule.BillItem', 'InTotal')?>"><?=Yii::t('AccountingModule.BillItem', 'InTotal')?></span>
            </div>
        </div><!-- end tab_bill_head -->





        <!-- tab_bill_body -->  
        <div class='sima-layout-panel tab_bill_body'>
            <div class="bill_item_wrap">
                <template v-if='this.has_fa_items'>
                    <div class='bill_item_subtitle'>
                        <?=Yii::t('AccountingModule.BillItem', 'FixedAssets')?>
                    </div>
                    <draggable 
                        data-type="FIXED_ASSETS"
                        class="sima-layout-bill-items-sortable-list"
                        v-bind:sort="true"
                        v-on:end="onEnd"
                        v-bind:group="{ name: 'fixed_assets_bill_items', put: draggablePutCheck}" 
                        animation="500" ghost-class="draggable-ghost-class" filter="._not-draggable" dragClass="draggable-drag-class"
                    >
                        <bills_tab-one_bill_item 
                            v-bind:show_discount='show_discount'
                            v-bind:number_width='number_width'
                            v-for='fa_item in this.model.fixed_assets_bill_items' 
                            v-bind:bill_item='fa_item' 
                            v-bind:key='fa_item.id'
                            v-bind:read_only='read_only'
                            v-bind:class="{
                                '_not-draggable': !is_fixed_assets_bill_items_ok
                            }"
                        >
                        </bills_tab-one_bill_item>
                    </draggable>
                </template>

                <template v-if='this.has_w_items'>
                    <div class='bill_item_subtitle'>
                        <?=Yii::t('AccountingModule.BillItem', 'Warehouse')?>
                    </div>
                    <draggable 
                        data-type="WAREHOUSE"
                        class="sima-layout-bill-items-sortable-list"
                        v-bind:sort="true"
                        v-on:end="onEnd"
                        animation="500" ghost-class="draggable-ghost-class" filter="._not-draggable" dragClass="draggable-drag-class"
                        v-bind:group="{ name: 'warehouse_bill_items', put: false}" 
                    >
                        <bills_tab-one_bill_item 
                            v-bind:show_discount='show_discount'
                            v-bind:number_width='number_width'
                            v-for='w_item in this.model.warehouse_bill_items' 
                            v-bind:bill_item='w_item' 
                            v-bind:key='w_item.id' 
                            v-bind:read_only='read_only'
                            v-bind:class="{
                                '_not-draggable': !is_warehouse_bill_items_ok
                            }"
                        >
                        </bills_tab-one_bill_item>
                    </draggable>
                </template>

                <template v-if='this.has_s_items'>
                    <div class='bill_item_subtitle'>
                        <?=Yii::t('AccountingModule.BillItem', 'Services')?>
                    </div>
                    <draggable 
                        data-type="SERVICES"
                        class="sima-layout-bill-items-sortable-list"
                        v-bind:sort="true"
                        v-on:end="onEnd"
                        animation="500" ghost-class="draggable-ghost-class" filter="._not-draggable" dragClass="draggable-drag-class"
                        v-bind:group="{ name: 'services_bill_items', put: false}" 
                    >
                        <bills_tab-one_bill_item 
                            v-bind:show_discount='show_discount'
                            v-bind:number_width='number_width'
                            v-for='s_item in this.model.services_bill_items' 
                            v-bind:bill_item='s_item' 
                            v-bind:key='s_item.id' 
                            v-bind:class="{
                                '_selected': selected_bill_items.indexOf(s_item.id) > -1,
                                '_not-draggable': !is_services_bill_items_ok
                            }"
                            v-on:click.native.ctrl="toggleSelectedBillItems(s_item.id)"
                            v-bind:read_only='read_only'
                            v-on:toggleSelectedBillItem="toggleSelectedBillItems"
                            >
                        </bills_tab-one_bill_item>  
                    </draggable>
                </template>
            </div>
        </div>    
        <!-- tab_bill_footer -->
        <div 
            class="sima-layout-fixed-panel tab_bill_footer"
        >            
            <div class="tables sum">
                <h4 v-if="this.model.services_bill_items_unconfirmed.length > 0">Suma</h4>
                <div class="table-wrap">
                    <table class="cf_one_pixel_border">
                        <tr>
                            <td><?=BillItem::model()->getAttributelabel('vat')?></td>
                            <td><?=BillItem::model()->getAttributeLabel('value')?></td>
                            <td><?=BillItem::model()->getAttributeLabel('value_vat')?></td>
                            <td><?=BillItem::model()->getAttributeLabel('value_total')?></td>
                        </tr>
                        <tr v-for='sum_item in this.model.sum_per_vat_bill_items' >
                            <td style="text-align: right;">{{sum_item.vat}}</td>
                            <td style="text-align: right;">{{sum_item.value | formatNumber}}</td>
                            <td style="text-align: right;">{{sum_item.value_vat | formatNumber}}</td>
                            <td style="text-align: right;">{{sum_item.value_total | formatNumber}}</td>
                        </tr>
                        <tr style='font-weight: 600;'>
                            <td style="text-align: right;"><span class="sima-icon sis-sigma"></span></td>
                            <td style="text-align: right;">{{sum_item_total.value | formatNumber}}</td>
                            <td style="text-align: right;">{{sum_item_total.value_vat | formatNumber}}</td>
                            <td style="text-align: right;">{{sum_item_total.value_total | formatNumber}}</td>
                        </tr>
                    </table>
                </div>
            </div><!-- end table one -->

            <div class="tables unprocessed" v-if="this.model.services_bill_items_unconfirmed.length > 0">
                <h4><?=Yii::t('AccountingModule.BillItem', 'UnprocessedItems')?></h4>
                <div class="table-wrap">
                    <table class="cf_one_pixel_border">
                        <tr>
                            <td><?=BillItem::model()->getAttributelabel('vat')?></td>
                            <td><?=BillItem::model()->getAttributeLabel('value')?></td>
                            <td><?=BillItem::model()->getAttributeLabel('value_vat')?></td>
                            <td><?=BillItem::model()->getAttributeLabel('value_total')?></td>
                            <td></td>
                        </tr>
                        <tr v-for='un_item in this.model.services_bill_items_unconfirmed'>
                            <td style="text-align: right;">{{un_item.vat}}%</td>
                            <td style="text-align: right;">{{un_item.value | formatNumber}}</td>
                            <td style="text-align: right;">{{un_item.value_vat | formatNumber}}</td>
                            <td style="text-align: right;">{{un_item.value_total | formatNumber}}</td>
                            <td>
                                <!-- Dodaj stavku -->
                                <sima-button
                                    class="add_button button button1 access"
                                    v-bind:class="add_item_btn_ready"
                                    title="<?=Yii::t('AccountingModule.BillItem', 'AddNewItem')?>"
                                    v-on:click="addItem(un_item.id)"
                                >
                                    <i class="sima-icon sil-layer-plus"></i>
                                </sima-button><!-- END Dodaj stavku -->

                                <!-- Obrisite stavku -->
                                <sima-button
                                    class="add_button button button1 access"
                                    title="<?=Yii::t('AccountingModule.BillItem', 'DeleteItem')?>"
                                    v-on:click="deleteItem(un_item.id)"
                                >
                                    <i class="sima-icon sil-trash-alt"></i>
                                </sima-button><!-- END Obrisite stavku -->
                            </td>
                        </tr>
                    </table>
                </div>
            </div><!-- end table two -->

            <div class="sum-logo" v-if="this.model.services_bill_items_unconfirmed.length <= 0">
                <!-- <img src="https://i.ibb.co/B2tvMmc/sil-sigma.png"/> -->
            </div><!-- end sum-logo -->
        </div><!-- end tab_bill_footer -->
    </div><!-- end tab_bill_head_body_footer -->




    <!-- tab_bill_asside -->
    <div v-if="!this.read_only" class='tab_bill_asside sima-layout-panel _splitter _horizontal' data-sima-layout-init='{"proportion":0.3, "max_width":400}'>
        <div class='sima-layout-panel'>
            <div class='sima-layout-fixed-panel'>     
                <sima-button 
                    title="<?=Yii::t('AccountingModule.BillItem', 'BandTogetherPlaceOfCost')?>"
                    v-on:click="setCostLocation" 
                >
                    <span class="sima-icon sil-connect"></span><hr>
                    <span class="sima-btn-title"><?=Yii::t('AccountingModule.BillItem', 'BandTogetherPlaceOfCost')?></span>
                </sima-button>      
            </div>
            <ul>
                <li v-for="_cl in this.cost_locations" 
                    v-bind:key="_cl.id" 
                    v-bind:class="{'_selected': selected_cost_location_id === _cl.id}"
                >
                    <span v-html='_cl.DisplayHTML'></span> 
                    - 
                    <span style="cursor: pointer;" v-on:click="toggleSelectedCostLocation(_cl.id)">{{_cl.work_name}}</span>
                </li>
            </ul>
        </div>
        <div class='sima-layout-panel'>
            <p>
                <span class="_warehause_transfer"><?=Yii::t('AccountingModule.BillItem', 'BillWarehouseTransferIn')?></span>: &nbsp;
                    <label 
                        class="sima-switch" 
                        v-bind:class="switch_classes" 
                        v-on:click="ConvertFromBillToBillWTIn"
                    >
                        <span class="sima-slider round"></span>
                    </label>
            </p>
            <!-- Prijemnice -->
            <sima-button 
                title="<?=Yii::t('AccountingModule.BillItem', 'ViewAndAddReceiving')?>"
                v-on:click="billWarehouseTransfersConnection"
                style="margin: 20px;"
            >
                <i class="sima-icon sil-file-plus"></i><hr>
                <span class="sima-btn-title"><?=Yii::t('AccountingModule.BillItem', 'Receiving')?></span>
            </sima-button><!-- END Prijemnice -->
            <ul>
                <li v-for="warehouse_receives_with_is_bill in this.warehouse_receives_with_is_bill" 
                    v-bind:key="warehouse_receives_with_is_bill.warehouse_receive.id"
                >
                    <span v-html='warehouse_receives_with_is_bill.warehouse_receive.DisplayHTML'></span>
                    <span v-if="warehouse_receives_with_is_bill.is_bill">*</span>    
                </li>
            </ul>
        </div>
    </div><!-- end tab_bill_asside --> 
</div><!-- end file_tab_bill_items -->


</script>
