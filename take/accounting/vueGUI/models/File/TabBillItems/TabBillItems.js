/* global Vue, sima */

Vue.component('accounting-file_tab-bill_items',{
    props:  {
        model: {validator: sima.vue.ProxyValidator},
	read_only: Boolean
    },
    computed: {
        component_uniq_id: function() {
            return 'bill_items' + sima.uniqid();
        },
        padlock_btn_title: function() {
            return this.model.lock_amounts ? sima.translate('BillItemUnlock') : sima.translate('BillItemLock');
        },
        padlock_icon: function() {
            var class_name = '';

            if (this.model.lock_amounts) {
                class_name = 'sil-lock-alt';
            } else {
                class_name = 'sil-unlock-alt';
            }

            return class_name;
        },
        padlock_icon_inverse: function() {
            var class_name = '';

            if (!this.model.lock_amounts) {
                class_name = 'sil-lock-alt';
            } else {
                class_name = 'sil-unlock-alt';
            }

            return class_name;
        },
        padlock_icon_title: function() {
            return this.model.lock_amounts ? sima.translate('TotalValueOfBillIsLocked') : sima.translate('TotalValueOfBillIsUnlocked');
        },
        WT_button_ready: function(){
            
            this.model.isBillWT;
            this.model.invoice;
            this.model.partner_id;
            
            return     (this.model.shotStatus('isBillWT')   === 'OK') 
                    && (this.model.shotStatus('invoice')    === 'OK') 
                    && (this.model.shotStatus('partner_id') === 'OK');
        },
        WT_button_title: function(){
            if (this.model.invoice)
            {
                if (this.model.isBillWT)
                {
                    return sima.translate('ConvertFromBillWTToBillOut');
                }
                else
                {
                    return sima.translate('ConvertFromBillToBillWTOut');
                }
            }
            else
            {
                if (this.model.isBillWT)
                {
                    return sima.translate('ConvertFromBillWTToBillIn');
                }
                else
                {
                    return sima.translate('ConvertFromBillToBillWTIn');
                }
            }
        },
        WT_button_transfer_type: function(){
            if (this.model.invoice)
            {
                return 'WarehouseDispatch';
            }
            else
            {
                return 'WarehouseReceiving';
            }
        },
        
//            has_w_items: () => {return Object.keys(this.bill.warehouse_bill_items).length > 0},
        has_w_items: function(){
            return Object.keys(this.model.warehouse_bill_items).length > 0;
        },
        has_fa_items: function(){
            return Object.keys(this.model.fixed_assets_bill_items).length > 0;
        },
        has_s_items: function(){
            return Object.keys(this.model.services_bill_items).length > 0;
        },
        cost_locations: function(){
            var _res = [];
            for(var i in this.model.cost_locations)
            {            
                _res.push(   sima.vue.shot.getters.model(this.model.cost_locations[i])   );
            }
            return _res;
        },
        show_discount: function(){

            var _local_has = this.model.has_discount;

//                console.log(_local_has);
//                console.log(typeof _local_has);

//                if (typeof _local_has === 'boolean')
            {
                return _local_has;
            }
//                else
//                {
//                    return false;
//                }

        },
        numeric_style: function(){
            return {
                width: this.number_width+'px',
                'text-align': 'right'
            };
        },
        number_width: function(){

            var curr_min = 100;//default minimum

            //posmatra se samo value_total jer bi sustinski on trebao da bude najveci, 
            //pa se prema tome povecavaju i ostali brojevi
            var _calc = Math.floor(Math.log10(this.model.bill_items_max_number) * 15);
            if (_calc > curr_min)
            {
                curr_min = _calc;
            }
            return curr_min;
        },
        sum_item_total: function(){
            var _value = 0;
            var _value_vat = 0;
            var _value_total = 0;
            
            $.each(this.model.sum_per_vat_bill_items, function(index,item){
                _value += item.value;
                _value_vat += item.value_vat;
                _value_total += item.value_total;
            });
            
            return {
                value: _value,
                value_vat: _value_vat,
                value_total: _value_total
            };
        },
        add_item_btn_ready: function(){
            this.model.local_cost_location_id;
            this.model.local_cost_type_id;
            return {
                _disabled: 
                    this.model.shotStatus('local_cost_location_id') !== 'OK' ||
                    this.model.shotStatus('local_cost_type_id') !== 'OK'
            };
        },
//        selected_bill_items_ids: function(){
//            var _ret_array = [];
//            $.each(this.selected_bill_items,function(index, element){
//                _ret_array.push(sima.vue.shot.getters.model(element).id);
//            });
//            return _ret_array;
//        },        
        is_services_bill_items_ok: function() {
            return this.model.shotStatus('services_bill_items') === 'OK';
        },
        is_fixed_assets_bill_items_ok: function() {
            return this.model.shotStatus('fixed_assets_bill_items') === 'OK';
        },
        is_warehouse_bill_items_ok: function() {
            return this.model.shotStatus('warehouse_bill_items') === 'OK';
        },
        dataTrue: function(){
            this.model.invoice;
            this.model.partner_id;
            return this.model.shotStatus('invoice') === 'OK' && this.model.shotStatus('partner_id') === 'OK';
        },
        switch_classes: function(){
            return {
                checked: this.model.isBillWT,
                _disabled: !this.dataTrue
            };
        },
        warehouse_receives_with_is_bill: function(){
            var warehouse_receives_with_is_bill = [];
            var _this = this;
            $.each(this.model.warehouse_receives, function(index, warehouse_receive){
                if(_this.is_bill_warehouse_receive(warehouse_receive))
                {
                    warehouse_receives_with_is_bill.unshift({
                        warehouse_receive: warehouse_receive,
                        is_bill: true
                    });
                }
                else
                {
                    warehouse_receives_with_is_bill.push({
                        warehouse_receive: warehouse_receive,
                        is_bill: false
                    });
                }
            });
            return warehouse_receives_with_is_bill;
        }
    },
    watch: {
        cost_locations: function(oldVal, newVal){
            this.selected_cost_location_id = null;
            this.selected_bill_items = [];
        }
    },
    data: function () {
        return {
            selected_cost_location_id: null,
//            selected_cost_locations: [],
            selected_bill_items: []
        };
    },
    mounted: function() {
        var _this = this;
        $(this.$el).on('destroyed', function(){
            _this.$destroy();
        });
    },
//    mounted: function(){
//        console.log('TRIGGER_tab_bill_items_mounted');
//        this.$nextTick(function () {
//            sima.layout.allignObject($(this.$el), 'TRIGGER_tab_bill_items_mounted');
//        });
//    },
    updated: function () {
//        console.log('TRIGGER_tab_bill_items_updated');
        this.$nextTick(function () {
            sima.layout.allignObject($(this.$el), 'TRIGGER_tab_bill_items_updated');
        });
//        this.selected_cost_location_id = null;
//        this.selected_bill_items = [];
        
    },
    methods: {
        toggleSelectedCostLocation: function(cost_location_id){
            if (this.selected_cost_location_id !== cost_location_id)
            {
                this.selected_cost_location_id = cost_location_id;
            }
            else
            {
                this.selected_cost_location_id = null;
            }
//            var index = this.selected_cost_locations.indexOf(cost_location_id);
//            if (index > -1)
//            {
//                this.selected_cost_locations.splice(index, 1);
//            }
//            else
//            {
//                this.selected_cost_locations.push(cost_location_id);
//            }
//                console.log(this.selected_cost_locations);
        },
        toggleSelectedBillItems: function(bill_item_id){
            var index = this.selected_bill_items.indexOf(bill_item_id);
            if (index > -1)
            {
                this.selected_bill_items.splice(index, 1);
            }
            else
            {
                this.selected_bill_items.push(bill_item_id);
            }
//                console.log(this.selected_bill_items);
        },
        setCostLocation: function(){
            var cost_location_ids = [];
            if (this.selected_cost_location_id !== null)
            {
                cost_location_ids.push(this.selected_cost_location_id);
            }
            sima.ajax.get('accounting/bill/connectBillItemsAndCostLocations',{
                data: {
                    bill_item_ids: this.selected_bill_items,
//                    cost_location_ids: this.selected_cost_locations,
                    cost_location_ids: cost_location_ids
                }
            });
        },
        add_button_params: function(bill_item_id){
            return {
                0:'sima.model.form',
                1:'BillItem', 
                2:bill_item_id, 
                3:{
                    init_data: {
                        BillItem: {
                            confirmed: true,
                            bill_id: this.model.id,
                            cost_location_id: this.model.local_cost_location_id,
                            cost_type_id: this.model.local_cost_type_id
                        }
                    }
                }
            };
        },
        multiselectDelete: function(){
            var _this = this;
            sima.dialog.openYesNo(sima.translate('AreYouSureYouWantToDeleteBillItems'),
                function() {
                    sima.ajaxLong.start('base/model/multiselectDelete', {
                        data: {
                            ids: _this.selected_bill_items, 
                            model: 'BillItem'
                        },
                        showProgressBar: $(_this.$el),
                        onEnd:function(response){
                        }
                    });
                    sima.dialog.close();
                }
            );
        },
        WT_button_click: function(){
            if (this.model.isBillWT)
            {
                sima.accounting.removeWarehouseTransferFromBIll(this.model.id, this.WT_button_transfer_type);
            }
            else
            {
                sima.accounting.addWarehouseTransferToBIll(this.model.id, this.WT_button_transfer_type, this.model.partner_id);
            }
        },
        convertToWarehouse: function(){
            if (this.selected_bill_items.length === 1)
            {
                var _selected_item_id = this.selected_bill_items[0];
                sima.model.choose('WarehouseMaterial',function(picked){
                    sima.ajax.get('accounting/billItems/convertToWarehouse',{
                        async:true,
                        data: {
                            bill_item_id: _selected_item_id,
                            warehouse_material_id: picked.id
                        }
                    });
                });
                
            }
            else
            {
                console.warn('nije selektovana tacno jedna stavka nego '+this.selected_bill_items.length);
            }
        },
        onEnd: function(event) {
            if (
                    sima.isEmpty(event.item.__vue__) ||
                    event.to === event.from && event.oldIndex === event.newIndex
                )
            {
                return false;
            }
            
            var new_bill_item_divs = $(this.$el).find('.file_tab_bill_items_one_item');
            var bill_item_id = event.item.__vue__.bill_item.id;
            var new_order = null;
            $.each(new_bill_item_divs, function(index, new_bill_item_div){
                if(new_bill_item_div.__vue__.bill_item.id === bill_item_id)
                {
                    new_order = index + 1;
                }
            });

            sima.ajax.get('accounting/billItems/updateOrderOfBillItem',{
                async: true,
                get_params:{
                    bill_id: this.model.id,
                    bill_item_id: bill_item_id,
                    new_order: new_order
                }
            });
        },
        draggablePutCheck: function(to, from, dragEl, evt) {
            //privremena zabrana prebacivanja stavki iz jedne grupe u drugu
            return false;
        },
        lockUnlock: function() {
            sima.accounting.BillItemsLocked(this.model.id, this.component_uniq_id + '_yes');
        },
        convertToFixedAssets: function() {
            sima.accounting.addBillItemsToFixedAssets(this.selected_bill_items, '');
        },
        addItem: function(bill_item_id){
            sima.model.form('BillItem', bill_item_id, {
                init_data: {
                    BillItem: {
                        confirmed: true,
                        bill_id: this.model.id,
                        cost_location_id: this.model.local_cost_location_id,
                        cost_type_id: this.model.local_cost_type_id
                    }
                }
            });
        },
        deleteItem: function(bill_item_id){
            sima.model.remove('BillItem', bill_item_id);
        },
        billWarehouseTransfersConnection: function(){
            sima.accounting.warehouse.BillWarehouseTransfersConnection(this.model.id);
        },
        ConvertFromBillToBillWTIn: function(){
            var warehouse_transfer_type = this.model.invoice ? 'WarehouseDispatch' : 'WarehouseReceiving';
            if(this.model.isBillWT)
            {
                sima.accounting.removeWarehouseTransferFromBIll(this.model.id, warehouse_transfer_type);
            }
            else
            {
                sima.accounting.addWarehouseTransferToBIll(this.model.id, warehouse_transfer_type, this.model.partner_id);
            }
        },
        is_bill_warehouse_receive: function(warehouse_receive){
            var is_bill_warehouse_receive = false;
            if(typeof warehouse_receive.file !== 'undefined')
            {
                if(typeof warehouse_receive.file.bill !== 'undefined' && warehouse_receive.file.bill !== '')
                {
                    is_bill_warehouse_receive = true;
                }
            }
            return is_bill_warehouse_receive;
        }
    },
    template: '#accounting-file_tab-bill_items'
});
