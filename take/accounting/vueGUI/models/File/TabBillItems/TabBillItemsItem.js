/* global Vue, sima */

Vue.component('bills_tab-one_bill_item',{
    props:  {
        bill_item: {
            validator: sima.vue.ProxyValidator
        },
        t: Object,
        number_width: Number,
        show_discount: Boolean,
        read_only: Boolean
    },
    computed: {
        vat_refusal_class: function() {
            return this.bill_item.vat_refusal?'sir-check':'sis-times';
        },vat_refusal_color: function() {
            return this.bill_item.vat_refusal?'green':'red';
        },
        numeric_style: function(){
            return {
                flex: '0 0 ' + this.number_width + 'px',
                'text-align': 'right'
            };
        },
        popdv_field_style: function() {
            return {
                flex:  '0 0 ' + (this.number_width + 150)+'px'
            };
        },
        is_warehouse: function() {
            return this.bill_item.bill_item_type === 'WAREHOUSE';
        },
        is_services: function() {
            return this.bill_item.bill_item_type === 'SERVICES';
        },
        is_fixed_assets: function() {
            return this.bill_item.bill_item_type === 'FIXED_ASSETS';
        },
        icon_class: function() {
            var class_name = '';

            if (this.is_fixed_assets)
            {                
                class_name = 'sima-icon sil-car-building-alt';
            }
            else if (this.is_warehouse)
            {
                class_name = 'sima-icon sil-boxes';
            }
            else if (this.is_services)
            {
                class_name = '';
            }

            return class_name;
        },
        rbr_class: function() {
            var class_name = '';

            if (!this.is_services)
            {
                class_name = 'icon-notify';
            }

            return class_name;
        },
        row_title: function() {
            var title = '';

            if (this.is_fixed_assets)
            {
                title = sima.translate('BillItemFixedAssets');
            }
            else if (this.is_warehouse)
            {
                title = sima.translate('BillItemWarehouse');
            }
            else if (this.is_services)
            {
                title = sima.translate('BillItemServices');
            }

            return title;
        }
    },
    data: function () {
        return {
            mouse_is_over: false,
            is_active: true
        };
    },
    destroyed: function() {
        $(this.$el).remove();
    },
    methods: {
        testAkcija: function(){
            console.log(this.bill_item_tag);
        },
        toggleClass: function(event){
            if(this.is_active){
                this.is_active = false;
            }
            else
            {
                this.is_active = true;
            }     
         },
        toggleSelectedBillItem: function(){
            if(this.is_services)
            {
                this.$emit('toggleSelectedBillItem', this.bill_item.id);
            }
        }
    },
    template: '#bills_tab-one_bill_item'
});
