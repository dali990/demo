
<script type="text/x-template" id="bills_tab-one_bill_item">
    <div class='file_tab_bill_items_one_item' @mouseover="mouse_is_over = true" @mouseleave="mouse_is_over = false">
        <!-- row  -->
        <div 
            class="one_item_wrap"
            v-bind:class="[is_active ? '' : 'shown']"
        >   
            <span class="id-col" v-bind:class="this.rbr_class">
                <sima-button @click="is_active = !is_active">
                    <span class="sima-icon sil-long-arrow-down"></span>
                </sima-button>  
                <i v-bind:title="row_title" v-bind:class="this.icon_class"></i>
                <span 
                    v-bind:title="row_title"
                    class="numb"
                    v-on:click="toggleSelectedBillItem"
                >
                    {{bill_item.order}}
                    <span 
                        class="sima-icon check sir-check"
                        v-bind:title="bill_item.order"
                    >
                    </span>
                </span>
            </span>
            <span class="name-col">
                <div class="first-child" v-html="bill_item.add_info"></div>
                <div 
                    class="last-child"
                    v-bind:title="bill_item.DisplayName"
                    v-html="bill_item.DisplayName"
                    >
                </div>
            </span>
            <span class="quantity-col">
                 <!-- edit and delete buttons -->     
                 <template v-if="!this.read_only">
                    <span
                        class="more-options"
                        style="display: flex; left: 0;" 
                        v-show="mouse_is_over"
                    >
                        <sima-model-options v-bind:model="bill_item"></sima-model-options>
                    </span>                
                </template><!-- end edit and delete buttons -->   
                <span class="monospace">
                    {{bill_item.quantity | formatNumber}} <i>{{bill_item.unit.DisplayName}}</i>
                </span>
            </span>
            <span class="measure-col">
                <div class="monospace">{{bill_item.value_per_unit | formatNumber}}</div> 
                <div v-bind:class="this.vat_refusal_color">
                    <span class="monospace"><template v-if="bill_item.vat_starred"><span title="<?=Yii::t('AccountingModule.BillItem', 'InternalVAT')?>" class="intern">*</span></template>{{bill_item.vat}}</span>
                    <span>%</span>
                    <span><i class="sima-icon" v-bind:class="this.vat_refusal_class"></i></span>
                </div>           
            </span>
            <span class="discount-col">
                <div>{{(bill_item.discount>0)?bill_item.discount+'%':''}}</div>
                <div v-if="this.show_discount"></div>
            </span>
            <span class="value-col">
                <div class="monospace"><template v-if="bill_item.value_starred"><span title="<?=Yii::t('AccountingModule.BillItem', 'InternalVAT')?>" class="intern">*</span></template>{{bill_item.value | formatNumber}}</div>
                <div><span class="monospace"><template v-if="bill_item.vat_starred"><span title="<?=Yii::t('AccountingModule.BillItem', 'InternalVAT')?>" class="intern">*</span></template>{{bill_item.value_vat | formatNumber}}</span></div>
            </span>
            <span class="pdv-value-col">
                <div class="monospace">{{bill_item.value_total | formatNumber}}</div>
                <div 
                    v-bind:title="bill_item.no_vat_type_text"
                >
                    {{bill_item.no_vat_type_text}}
                </div>
            </span>
        </div><!-- end row  -->
    </div><!-- end file_tab_bill_items_one_item -->
</script>