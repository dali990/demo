<script type="text/x-template" id="accounting-file_tab-bill_releasing_posible_debt_take_over">
    <div class='accounting_file_tab_bill_releasing_posible_debt_take_over' 
        @mouseover="mouse_is_over = true" 
        @mouseleave="mouse_is_over = false"
        v-on:click="onClickRow"
        v-bind:class="{'_selected': is_checked}"
    >    
        <div class="check-col">
            <input 
                type="checkbox"
                v-show="mouse_is_over || this.is_checked"
                v-on:click="onClickCheckBox"
                v-bind:checked="this.is_checked"
            >
        </div>
        <div class="amount-col">
            <span class="value monospace">{{this.posible_debt_take_over.debt_value | formatNumber}}</span></br>
            <span class="monospace percent">{{percent_of_bill | formatNumber}}</span>
        </div>
        <div class="date-col">
            <span class="monospace percent">{{this.posible_debt_take_over.sign_date}}</span>
        </div>
        <div class="name-col">
            <sima-model-display-html v-bind:model="posible_debt_take_over"></sima-model-display-html>
        </div>
        <div  class="available-amount-col">
            <span class="value monospace">{{this.posible_debt_take_over.unreleased_debt_value | formatNumber}}</span> 
        </div>
        <div class="div_options">
            <sima-button  
                class="icon-only"
                title="<?=Yii::t('AccountingModule.Bill','Connect')?>" 
                v-on:click="connectDebtTakeOver"
                v-show="mouse_is_over"
                security_question="<?=Yii::t('AccountingModule.Bill','AreYouSureYouWantToConnect')?>"
            >
                <span class="sima-icon sir-connect"></span>
            </sima-button>
            <sima-button  
                class="icon-only"
                title="<?=Yii::t('AccountingModule.Bill','ConnectPart')?>" 
                v-on:click="connectPartDebtTakeOver"
                v-show="mouse_is_over"
            >
                <span class="sima-icon sil-connect-alt"></span>
            </sima-button>
        </div>
    </div>
</script>
