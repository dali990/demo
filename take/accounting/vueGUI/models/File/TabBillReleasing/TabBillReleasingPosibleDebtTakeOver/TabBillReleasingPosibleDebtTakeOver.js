/* global Vue, sima */

Vue.component('accounting-file_tab-bill_releasing_posible_debt_take_over',{
    props:  {
        posible_debt_take_over: {validator: sima.vue.ProxyValidator},
        bill: {validator: sima.vue.ProxyValidator}
    },
    computed: {
        percent_of_bill: function () { 
            var percent_of_bill = this.posible_debt_take_over.unreleased_debt_value * 100.0 / this.bill.amount;
            return percent_of_bill > 100 ? 100 : percent_of_bill;
        },
        is_checked: function(){
            if (typeof this.$parent.posible_releases[this._uid] === 'undefined')
            {
                return false;
            }
            return this.$parent.posible_releases[this._uid].is_checked;
        }
    },
    data: function () {
        return {
            mouse_is_over: false
        };
    },
    mounted: function() {
        var _this = this;
        $(this.$el).on('destroyed', function(){
            _this.$destroy();
        });
        this.$parent.addToPosibleReleases(this);
    },
    destroyed: function() {
        this.$parent.removeToPosibleReleases(this);
    },
    methods: {
        connectDebtTakeOver: function() {
            sima.ajax.get('accounting/billReleasing/addDebtTakeOver',{
                data:{
                    bill_ids: [this.bill.id],
                    debt_take_over_ids: [this.posible_debt_take_over.id]
                },
                async: true
            });
        },
        connectPartDebtTakeOver: function() {
            var _this = this;                     
            var model_id = {
                uniq_check: {
                    bill_id: _this.bill.id,
                    debt_take_over_id: _this.posible_debt_take_over.id
                }
            };
            sima.model.form('BillDebtTakeOver', model_id, {
                init_data: {
                    BillDebtTakeOver: {
                        bill_id: _this.bill.id,
                        debt_take_over_id: _this.posible_debt_take_over.id
                    }
                }
            });
        },
        onClickCheckBox: function(event) {
           event.stopPropagation(); 
           this.$parent.posible_releases[this._uid].is_checked = !this.$parent.posible_releases[this._uid].is_checked;
        },
        onClickRow: function(event) {
            if (event.ctrlKey)
            {
                this.$parent.posible_releases[this._uid].is_checked = !this.$parent.posible_releases[this._uid].is_checked;
            }
        },
        setChecked: function(checked) {
            this.is_checked = checked;
        },
        getModelName: function() {
            return 'DebtTakeOver';
        },
        getModelId: function() {
            return this.posible_debt_take_over.id;
        }
    },
    template: '#accounting-file_tab-bill_releasing_posible_debt_take_over'
});
