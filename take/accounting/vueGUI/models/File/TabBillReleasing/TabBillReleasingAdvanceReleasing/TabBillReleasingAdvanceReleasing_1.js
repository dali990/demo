/* global Vue, sima */

Vue.component('accounting-file_tab-bill_releasing_advance_releasing',{
    props:  {
        advance_releases: Array,
        bill: {validator: sima.vue.ProxyValidator}
    },
    computed: {
        advance_bill: function () { 
            return this.advance_releases.length > 0 ? sima.vue.shot.getters.model(sima.vue.getVueModelTag('AdvanceBill', this.advance_releases[0].advance_bill_id)) : '';
        },
        percent_of_bill: function () { 
            return this.advance_releases_amount * 100.0 / this.bill.amount;
        },
        advance_bill_release_ids : function () {
            var advance_bill_release_ids = [];
            $.each(this.advance_releases, function(index, advance_release)
            {
                advance_bill_release_ids.push(advance_release.id);
            });
            return advance_bill_release_ids;
        },
        haveAllData: function () {
            var result = true;
            $.each(this.advance_releases, function(index, advance_release){
                advance_release.internal_vat;
                advance_release.vat_rate;
                advance_release.base_amount;
                advance_release.vat_amount;
                advance_release.amount;
                if
                (
                    advance_release.shotStatus('internal_vat') !== 'OK' ||
                    advance_release.shotStatus('vat_rate') !== 'OK' ||
                    advance_release.shotStatus('base_amount') !== 'OK' ||
                    advance_release.shotStatus('vat_amount') !== 'OK' ||
                    advance_release.shotStatus('amount') !== 'OK'
                )
                {
                    result = false;
                    return false; /// each break
                }
            });
            return result;
        },
        is_checked: function(){
            if (typeof this.$parent.releases[this._uid] === 'undefined')
            {
                return false;
            }
            return this.$parent.releases[this._uid].is_checked;
        },
        count_advance_bill_amounts_differnet_from_zero: function(){
            var count = 0;
            if (this.advance_bill_amount0r !== 0)
            {
                count++;
            }
            if (this.advance_bill_amount10r !== 0)
            {
                count++;
            }
            if (this.advance_bill_amount20r !== 0)
            {
                count++;
            }
            if (this.advance_bill_amount10i !== 0)
            {
                count++;
            }
            if (this.advance_bill_amount20i !== 0)
            {
                count++;
            }
            return count;
        },
        class_expand_this: function(){
            return {
                'expand_this': this.count_advance_bill_amounts_differnet_from_zero > 3
            };
        }
    },
    data: function () {
        return {
            mouse_is_over: false,
            advance_releases_amount: 0,
            advance_bill_amount0r: 0,
            advance_bill_amount10r: 0,
            advance_bill_amount20r: 0,
            advance_bill_amount10i: 0,
            advance_bill_amount20i: 0
        };
    },
    watch: {
        haveAllData: function(new_val, old_val){
            this.calculatingDataParams();
        }
    },
    mounted: function() {
        var _this = this;
        this.calculatingDataParams();
        $(this.$el).on('destroyed', function(){
            _this.$destroy();
        });
        this.$parent.addToReleases(this);
    },
    destroyed: function() {
        this.$parent.removeToReleases(this);
    },
    methods: {
        cancelAdvanceBillRelease: function() {
            var _this = this;
            sima.ajax.get('accounting/billReleasing/cancelRelease',{
                get_params: {
                    model_name: 'AdvanceBillRelease'
                },
                data: {
                    model_ids: _this.advance_bill_release_ids,
                },
                async: true,
                success_function: function(response) {
                }
            });
        },
        editAdvanceBillRelease: function() {
            sima.dialog.openVueComponent(
                "accounting-file_tab-bill_releasing_base_amount_and_vat", 
                {
                    releases: this.advance_releases,
                    bill_id: this.bill.id,
                    model_id: this.advance_bill.id,
                    model_name: 'AdvanceBill'
                },
            );
        },
        onClickCheckBox: function(event) {
           event.stopPropagation(); 
           this.$parent.releases[this._uid].is_checked = !this.$parent.releases[this._uid].is_checked;
        },
        onClickRow: function(event) {
            if (event.ctrlKey)
            {
                this.$parent.releases[this._uid].is_checked = !this.$parent.releases[this._uid].is_checked;
            }
        },
        calculatingDataParams: function(){
            var _this = this;
            if(this.haveAllData === false)
            {
                return;
            }
            _this.advance_releases_amount = 0;
            $.each(_this.advance_releases, function(index, advance_release){
                _this.advance_releases_amount += Number(advance_release.amount);
                if(!advance_release.internal_vat)
                {
                    if(advance_release.vat_rate === '0')
                    {
                        _this.advance_bill_amount0r = advance_release.base_amount;
                    }
                    else if(advance_release.vat_rate === '10')
                    {
                        _this.advance_bill_amount10r = advance_release.base_amount + advance_release.vat_amount;
                    }
                    else
                    {
                        _this.advance_bill_amount20r = advance_release.base_amount + advance_release.vat_amount;
                    }
                }
                else
                {
                    if(advance_release.vat_rate === '10')
                    {
                        _this.advance_bill_amount10i = advance_release.base_amount + advance_release.vat_amount;
                    }
                    else
                    {
                        _this.advance_bill_amount20i = advance_release.base_amount + advance_release.vat_amount;
                    }
                }
            });
        },
        getModelName: function() {
            return 'AdvanceBill';
        },
        getModelId: function() {
            return this.advance_bill.id;
        }
    },
    template: '#accounting-file_tab-bill_releasing_advance_releasing'
});
