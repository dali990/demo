<?php $uniq = SIMAHtml::uniqid()?>

<script type="text/x-template" id="accounting-file_tab-bill_releasing">
    <div class='sima-layout-panel accounting_file_tab_bill_releasing'>     

        <div class='sima-layout-fixed-panel'>
            <div class="flex">
                <h2><?=Yii::t('AccountingModule.Bill','Payed')?>: <strong>{{this.released_amount | formatNumber}} {{this.model.currency}}</strong></h2>
                <h2><?=Yii::t('AccountingModule.Bill','Rest')?>: <strong>{{this.unreleased_amount | formatNumber}} {{this.model.currency}}</strong></h2>
            </div>

            <div class="progress-bar">
                <span v-bind:style="this.advance_released_percent_style"><i class="green">{{this.advance_released_percent | formatNumber}}% <?=Yii::t('AccountingModule.Bill','Advance')?></i></span> 
                <span v-bind:style="this.direct_released_percent_style"><i class="blue">{{this.direct_released_percent | formatNumber}}% <?=Yii::t('AccountingModule.Bill','Excerpt')?></i></span> 
                <span v-bind:style="this.reliefs_percent_style"><i class="red">{{this.reliefs_percent | formatNumber}}% <?=Yii::t('AccountingModule.Bill','Bookish')?></i></span> 
                <span v-bind:style="this.debt_take_overs_percent_style"><i class="yellow">{{this.debt_take_overs_percent | formatNumber}}% <?=Yii::t('AccountingModule.Bill','Contract')?></i></span> 
                <span v-bind:style="this.unreleased_percent_style"><i class="grey">{{this.unreleased_percent | formatNumber}}% <?=Yii::t('AccountingModule.Bill','Rest')?></i></span> 
            </div>

            <div class="flex">
                <h2><?=Yii::t('AccountingModule.Bill','TotalPercent')?>: <strong>{{this.released_percent | formatNumber}}% </strong></h2>
                <h2><?=Yii::t('AccountingModule.Bill','Late')?>: <strong>{{this.model.diff_between_payment_date_and_today_in_days}} <?=Yii::t('AccountingModule.Bill','Days')?></strong></h2>
            </div>
        </div>


        


        <div v-bind:class="splitter_style">

            <div class='sima-layout-panel left-tab' data-sima-layout-init='{"proportion":0.5, "max_width":800}' style="max-width: 800px">
                <div class="title-option">
                    <h2><?=Yii::t('AccountingModule.Bill','Released')?></h2>
                    <div class="btn-options">
                        <template  v-if="is_least_one_release_checked">
                            <sima-button 
                                v-on:click="multiselectCancelReleases"
                                security_question="<?=Yii::t('AccountingModule.Bill','AreYouSureYouWantToCancel')?>"
                            >
                                <span class="sima-icon sir-ban"></span><hr>
                                <span class="sima-btn-title"><?=Yii::t('AccountingModule.Bill','Cancel')?></span>
                            </sima-button>
                        </template> 
                    </div><!-- end btn-options -->
                </div>

                <div class="table liquidate">
                    <div id="liquidate">
                        <span class="check-col">
                            <input 
                                type="checkbox"
                                v-on:click="clickCheckedAllReleases"
                                v-bind:checked="this.is_checked_all_releases"
                            ><?=Yii::t('AccountingModule.Bill','CheckAll')?>
                        </span>
                        <span class="amount-col"><?=Yii::t('AccountingModule.Bill','Value')?></span>
                        <span class="date-col"><?=Yii::t('AccountingModule.Bill','Date')?></span>
                        <span class="name-col"><?=Yii::t('AccountingModule.Bill','Document')?></span>
                        <span class="available-amount-col"><?=Yii::t('AccountingModule.Bill','AvailableAmount')?></span>
                    </div>
                    <accounting-file_tab-bill_releasing_advance_releasing
                        v-for="advance_releases in this.releases_group_by_advance_bill"
                        v-bind:advance_releases='advance_releases'
                        v-bind:bill='model'
                    >
                    </accounting-file_tab-bill_releasing_advance_releasing>     
                    <accounting-file_tab-bill_releasing_direct_releasing 
                        v-for='release in this.model.releases'
                        v-bind:release='release'
                    >
                    </accounting-file_tab-bill_releasing_direct_releasing>
                    <accounting-file_tab-bill_releasing_bill_relief 
                        v-for='bill_reliefs in this.bill_reliefs_group_by_relief_bill'
                        v-bind:bill_reliefs='bill_reliefs'
                        v-bind:bill='model'
                    >
                    </accounting-file_tab-bill_releasing_bill_relief>
                    <accounting-file_tab-bill_releasing_debt_take_over
                        v-for='bill_debt_take_over in this.model.bill_debt_take_overs'
                        v-bind:bill_debt_take_over='bill_debt_take_over'
                    >
                    </accounting-file_tab-bill_releasing_debt_take_over>
                </div><!-- end table -->
            </div><!-- end left -->



            <div class='sima-layout-panel right-tab' v-show="this.released_percent < 100" data-sima-layout-init='{"proportion":0.5, "max_width":800}'>
                <div class="title-option">      
                    <h2><?=Yii::t('AccountingModule.Bill','PossibleWaysRelease')?></h2>
                    <div class="btn-options">
                        <template  v-if="is_least_one_posible_release_checked">
                            <sima-button 
                                title="<?=Yii::t('AccountingModule.Bill','Connect')?>"
                                v-on:click="multiselectConnectPosibleReleases"
                                security_question="<?=Yii::t('AccountingModule.Bill','AreYouSureYouWantToConnect')?>"
                            >
                                <span class="sima-icon sir-connect"></span><hr>
                                <span class="sima-btn-title"><?=Yii::t('AccountingModule.Bill','Connect')?></span>
                            </sima-button>
                        </template>
                    </div>
                </div><!-- end title-option -->
                <div class="table to-liquidate">
                    <div id="to-liquidate">
                        <span class="check-col">
                            <input 
                                type="checkbox"
                                v-on:click="clickCheckedAllPosibleReleases"
                                v-bind:checked="this.is_checked_all_posible_releases"
                            ><?=Yii::t('AccountingModule.Bill','CheckAll')?>
                        </span>
                        <span class="amount-col"><?=Yii::t('AccountingModule.Bill','Value')?></span>
                        <span class="date-col"><?=Yii::t('AccountingModule.Bill','Date')?></span>
                        <span class="name-col"><?=Yii::t('AccountingModule.Bill','Document')?></span>
                        <span class="available-amount-col"><?=Yii::t('AccountingModule.Bill','AvailableAmount')?></span>
                    </div>
                    <accounting-file_tab-bill_releasing_posible_advance
                        v-for='advance_bill in this.posible_advance_bills'
                        v-bind:advance_bill='advance_bill'
                        v-bind:bill='model'
                    >
                    </accounting-file_tab-bill_releasing_posible_advance>
                    <accounting-file_tab-bill_releasing_posible_payment
                        v-for='posible_payment in this.posible_payments'
                        v-bind:posible_payment='posible_payment'
                        v-bind:bill='model'
                    >
                    </accounting-file_tab-bill_releasing_posible_payment>
                    <accounting-file_tab-bill_releasing_posible_relief_bill
                        v-for='posible_relief_bill in this.posible_relief_bills'
                        v-bind:posible_relief_bill='posible_relief_bill'
                        v-bind:bill='model'
                    >
                    </accounting-file_tab-bill_releasing_posible_relief_bill>
                    <accounting-file_tab-bill_releasing_posible_debt_take_over
                        v-for='posible_debt_take_over in this.posible_debt_take_overs'
                        v-bind:posible_debt_take_over='posible_debt_take_over'
                        v-if='posible_debt_take_over.unreleased_debt_value !== 0'
                        v-bind:bill='model'
                    >
                    </accounting-file_tab-bill_releasing_posible_debt_take_over>
                </div><!-- end table -->
            </div><!-- end right -->


        </div>
    </div>
</script>