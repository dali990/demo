<script type="text/x-template" id="accounting-file_tab-bill_releasing_base_amount_and_vat">
    <div class='accounting_file_tab_bill_releasing_base_amount_and_vat'>
        <form>
            <table>
                <tr>
                    <td></td>
                    <td><?=Yii::t('AccountingModule.Bill','BaseAmount')?></td>
                    <td><?=Yii::t('AccountingModule.Bill','VatAmount')?></td>
                </tr>
                <tr class="shown" v-if="base_amount0r !== 0 || releases.length === 0">
                    <td class="monospace percent">0</td>
                    <td>
                        <input type="text" v-model="base_amount0r">
                    </td>
                    <td></td>
                </tr>
                <tr class="shown" v-if="base_amount10r !== 0 || releases.length === 0">
                    <td class="monospace">10</td>
                    <td>
                        <input type="text" v-model="base_amount10r">
                    </td>
                    <td>
                        <input type="text" v-model="vat10r">
                    </td>
                </tr>
                <tr class="shown" v-if="base_amount20r !== 0 || releases.length === 0">
                    <td class="monospace">20</td>
                    <td>
                        <input type="text" v-model="base_amount20r">
                    </td>
                    <td>
                        <input type="text" v-model="vat20r">
                    </td>
                </tr>
                <tr class="hidden" v-if="base_amount0i !== 0 || releases.length === 0">
                    <td class="monospace">*0</td>
                    <td>
                        <input type="text" v-model="base_amount0i">
                    </td>
                    <td></td>
                </tr>
                <tr class="hidden" v-if="base_amount10i !== 0 || releases.length === 0">
                    <td class="monospace">*10</td>
                    <td>
                        <input type="text" v-model="base_amount10i">
                    </td>
                    <td>
                        <input type="text" v-model="vat10i"> 
                    </td>
                </tr>
                <tr class="hidden" v-if="base_amount20i !== 0 || releases.length === 0">
                    <td class="monospace">*20</td>
                    <td>
                        <input type="text" v-model="base_amount20i">
                    </td>
                    <td>
                        <input type="text" v-model="vat20i">
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="text-align: right;">
                        <sima-button  
                            title="<?=Yii::t('AccountingModule.Bill','Save')?>"
                            v-on:click="addOrEditReleases"
                        >
                            <span class="sima-btn-title"><?=Yii::t('AccountingModule.Bill','Save')?></span>
                        </sima-button>
                    </td>
                </tr>
            </table>
        </form>
    </div>
</script>
