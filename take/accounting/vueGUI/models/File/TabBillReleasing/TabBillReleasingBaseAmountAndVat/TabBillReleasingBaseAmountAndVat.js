/* global Vue, sima */

Vue.component('accounting-file_tab-bill_releasing_base_amount_and_vat',{
    props:  {
        releases: Array,
        bill_id: Number,
        model_id: Number,
        model_name: String
    },
    computed: {
        model: function () { 
            return sima.vue.shot.getters.model(sima.vue.getVueModelTag(this.model_name, this.model_id));
        },
        remaining_vat_items_posible: function () { 
            return this.model.scoped('remaining_vat_items_posible', {bill_id: this.bill_id});
        },
        using_releases: function () {
            return this.releases.length > 0;
        },
        haveAllData: function () {
            var result = false;
    
            if(this.using_releases)
            {
                result = true;
                $.each(this.releases, function(index, release){
                    release.internal_vat;
                    release.vat_rate;
                    release.base_amount;
                    release.vat_amount;
                    if
                    (
                        release.shotStatus('internal_vat') !== 'OK' ||
                        release.shotStatus('vat_rate') !== 'OK' ||
                        release.shotStatus('base_amount') !== 'OK' ||
                        release.shotStatus('vat_amount') !== 'OK'
                    )
                    {
                        result = false;
                        return false; /// each break
                    }
                });
            }
            else
            {
                this.remaining_vat_items_posible
                if(
                        this.remaining_vat_items_posible !== ''
                        && typeof this.remaining_vat_items_posible["R"] !== 'undefined'
                        && typeof this.remaining_vat_items_posible["I"] !== 'undefined'
                )
                {
                    result = true;
                }
            }
    
            return result;
        },
    },
    data: function () {
        return {
            initially_calculated: false,
            base_amount0r: 0,
            base_amount10r: 0,
            base_amount20r: 0,
            vat10r: 0,
            vat20r: 0,
            base_amount0i: 0,
            base_amount10i: 0,
            base_amount20i: 0,
            vat10i: 0,
            vat20i: 0
        };
    },
    watch: {
        haveAllData: function(new_val, old_val){
            this.calculatingDataParams();
        }
    },
    mounted: function() {
        var _this = this;
        this.calculatingDataParams();
        $(this.$el).on('destroyed', function(){
            _this.$destroy();
        });
    },
    methods: {
        calculatingDataParams: function(event) {
            var _this = this;
            if(this.initially_calculated === true)
            {
                this.warn_about_modif = true;
                return;
            }
            
            if(this.haveAllData === false)
            {
                return;
            }
            
            if(this.using_releases)
            {
                $.each(_this.releases, function(index, release){
                    if(!release.internal_vat)
                    {
                        if(release.vat_rate === '0')
                        {
                            _this.base_amount0r = release.base_amount;
                        }
                        else if(release.vat_rate === '10')
                        {
                            _this.base_amount10r = release.base_amount;
                            _this.vat10r = release.vat_amount;
                        }
                        else
                        {
                            _this.base_amount20r = release.base_amount;
                            _this.vat20r = release.vat_amount;
                        }
                    }
                    else
                    {
                        if(release.vat_rate === '0')
                        {
                            _this.base_amount0i = release.base_amount;
                        }
                        else if(release.vat_rate === '10')
                        {
                            _this.base_amount10i = release.base_amount;
                            _this.vat10i = release.vat_amount;
                        }
                        else
                        {
                            _this.base_amount20i = release.base_amount;
                            _this.vat20i = release.vat_amount;
                        }
                    }
                });
            }
            else
            {
                _this.base_amount0r = _this.remaining_vat_items_posible["R"]["0"]["base"];
                _this.base_amount10r = _this.remaining_vat_items_posible["R"]["10"]["base"];
                _this.base_amount20r = _this.remaining_vat_items_posible["R"]["20"]["base"];
                _this.vat10r = _this.remaining_vat_items_posible["R"]["10"]["vat"];
                _this.vat20r = _this.remaining_vat_items_posible["R"]["20"]["vat"];
                _this.base_amount0i = _this.remaining_vat_items_posible["I"]["0"]["base"];
                _this.base_amount10i = _this.remaining_vat_items_posible["I"]["10"]["base"];
                _this.base_amount20i = _this.remaining_vat_items_posible["I"]["20"]["base"];
                _this.vat10i = _this.remaining_vat_items_posible["I"]["10"]["vat"];
                _this.vat20i = _this.remaining_vat_items_posible["I"]["20"]["vat"];
            }
            this.initially_calculated = true;
        },
        addOrEditReleases: function() {
            var _this = this;
            if(_this.model_name === 'AdvanceBill')
            {
                sima.ajaxLong.start('accounting/billReleasing/addOrEditAdvanceBillReleases', {
                    data:{
                        advance_bill_id: _this.model_id,
                        bill_id: _this.bill_id,
                        base_amount0r: Number(_this.base_amount0r),
                        base_amount10r: Number(_this.base_amount10r),
                        base_amount20r: Number(_this.base_amount20r),
                        vat10r: Number(_this.vat10r),
                        vat20r: Number(_this.vat20r),
                        base_amount0i: Number(_this.base_amount0i),
                        base_amount10i: Number(_this.base_amount10i),
                        base_amount20i: Number(_this.base_amount20i),
                        vat10i: Number(_this.vat10i),
                        vat20i: Number(_this.vat20i)
                    },
                    onEnd: function(response) {
                        sima.dialog.close();
                    }
                });
            }
            else if(_this.model_name === 'ReliefBill')
            {
                sima.ajaxLong.start('accounting/billReleasing/addOrEditBillReliefs', {
                    data:{
                        relief_bill_id: _this.model_id,
                        bill_id: _this.bill_id,
                        base_amount0r: Number(_this.base_amount0r),
                        base_amount10r: Number(_this.base_amount10r),
                        base_amount20r: Number(_this.base_amount20r),
                        vat10r: Number(_this.vat10r),
                        vat20r: Number(_this.vat20r),
                        base_amount0i: Number(_this.base_amount0i),
                        base_amount10i: Number(_this.base_amount10i),
                        base_amount20i: Number(_this.base_amount20i),
                        vat10i: Number(_this.vat10i),
                        vat20i: Number(_this.vat20i)
                    },
                    onEnd: function(response) {
                        sima.dialog.close();
                    }
                });
            }
        }
    },
    template: '#accounting-file_tab-bill_releasing_base_amount_and_vat'
});
