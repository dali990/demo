<script type="text/x-template" id="accounting-file_tab-bill_releasing_posible_relief_bill">
    <div class='accounting_file_tab_bill_releasing_posible_relief_bill' 
        @mouseover="mouse_is_over = true" 
        @mouseleave="mouse_is_over = false"
        v-on:click="onClickRow"
        v-bind:class="{'_selected': is_checked}"
    >
        <div class="check-col">
            <input 
                type="checkbox"
                v-show="mouse_is_over || this.is_checked"
                v-on:click="onClickCheckBox"
                v-bind:checked="this.is_checked"
            >
        </div>
        <div class="amount-col">
            <span class="value monospace">{{posible_relief_bill.amount | formatNumber}}</span><br>
            <span class="monospace percent">{{percent_of_bill | formatNumber}}</span>
        </div>
        <div class="date-col">
            <span>{{posible_relief_bill.income_date}}</span>
        </div>
        <div class="name-col">
            <sima-model-display-html v-bind:model="posible_relief_bill"></sima-model-display-html>
        </div>
        <div  class="available-amount-col" v-bind:class="class_expand_this">
            <table class="amount_vat">
                <tr class="shown" v-if="relief_bill_amount0r !== 0">
                    <td class="monospace percent">0</td>
                    <td class="monospace">{{relief_bill_amount0r | formatNumber}}</td>
                </tr>
                <tr class="shown" v-if="relief_bill_amount10r !== 0">
                    <td class="monospace percent">10</td>
                    <td class="monospace">{{relief_bill_amount10r | formatNumber}}</td>
                </tr>
                <tr class="shown" v-if="relief_bill_amount20r !== 0">
                    <td class="monospace percent">20</td>
                    <td class="monospace">{{relief_bill_amount20r | formatNumber}}</td>
                </tr>
                <tr class="hidden" v-if="relief_bill_amount10i !== 0">
                    <td class="monospace percent">*10</td>
                    <td class="monospace">{{relief_bill_amount10i | formatNumber}}</td>
                </tr>
                <tr class="hidden" v-if="relief_bill_amount20i !== 0">
                    <td class="monospace percent">*20</td>
                    <td class="monospace">{{relief_bill_amount20i | formatNumber}}</td>
                </tr>
                <span class="sima-icon sir-expand-alt"></span>
            </table>
        </div>
        <div class="div_options">
            <sima-button
                class="icon-only"
                title="<?=Yii::t('AccountingModule.Bill','Connect')?>" 
                v-on:click="connectReliefBill"
                v-show="mouse_is_over"
                security_question="<?=Yii::t('AccountingModule.Bill','AreYouSureYouWantToConnect')?>"
            >
                <span class="sima-icon sir-connect"></span>
            </sima-button>
            <sima-button
                class="icon-only"
                title="<?=Yii::t('AccountingModule.Bill','ConnectPart')?>" 
                v-on:click="connectPartReliefBill"
                v-show="mouse_is_over"
            >
                <span class="sima-icon sil-connect-alt"></span>
            </sima-button>
        </div>
    </div>
</script>
