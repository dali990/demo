/* global Vue, sima */

Vue.component('accounting-file_tab-bill_releasing_posible_relief_bill',{
    props:  {
        posible_relief_bill: {validator: sima.vue.ProxyValidator},
        bill: {validator: sima.vue.ProxyValidator}
    },
    computed: {
        percent_of_bill: function () { 
            var percent_of_bill = Math.abs(this.posible_relief_bill.unreleased_amount * 100.0 / this.bill.amount);
            return percent_of_bill > 100 ? 100 : percent_of_bill;
        },
        relief_bill_amount0r: function(){
            return !sima.isEmpty(this.posible_relief_bill.remaining_vat_items) ? 
                this.posible_relief_bill.remaining_vat_items["R"]["0"]["base"] : 0;
        },
        relief_bill_amount10r: function(){
            return !sima.isEmpty(this.posible_relief_bill.remaining_vat_items) ? 
                this.posible_relief_bill.remaining_vat_items["R"]["10"]["base"] + this.posible_relief_bill.remaining_vat_items["R"]["10"]["vat"] : 0;
        },
        relief_bill_amount20r: function(){
            return !sima.isEmpty(this.posible_relief_bill.remaining_vat_items) ? 
                this.posible_relief_bill.remaining_vat_items["R"]["20"]["base"] + this.posible_relief_bill.remaining_vat_items["R"]["20"]["vat"] : 0;
        },
        relief_bill_amount10i: function(){
            return !sima.isEmpty(this.posible_relief_bill.remaining_vat_items) ? 
                this.posible_relief_bill.remaining_vat_items["I"]["10"]["base"] + this.posible_relief_bill.remaining_vat_items["I"]["10"]["vat"]: 0;
        },
        relief_bill_amount20i: function(){
            return !sima.isEmpty(this.posible_relief_bill.remaining_vat_items) ? 
                this.posible_relief_bill.remaining_vat_items["I"]["20"]["base"] + this.posible_relief_bill.remaining_vat_items["I"]["20"]["vat"]: 0;
        },
        is_checked: function(){
            if (typeof this.$parent.posible_releases[this._uid] === 'undefined')
            {
                return false;
            }
            return this.$parent.posible_releases[this._uid].is_checked;
        },
        count_relief_bill_amounts_differnet_from_zero: function(){
            var count = 0;
            if (this.relief_bill_amount0r !== 0)
            {
                count++;
            }
            if (this.relief_bill_amount10r !== 0)
            {
                count++;
            }
            if (this.relief_bill_amount20r !== 0)
            {
                count++;
            }
            if (this.relief_bill_amount10i !== 0)
            {
                count++;
            }
            if (this.relief_bill_amount20i !== 0)
            {
                count++;
            }
            return count;
        },
        class_expand_this: function(){
            return {
                'expand_this': this.count_relief_bill_amounts_differnet_from_zero > 3
            };
        }
    },
    data: function () {
        return {
            mouse_is_over: false
        };
    },
    mounted: function() {
        var _this = this;
        $(this.$el).on('destroyed', function(){
            _this.$destroy();
        });
        this.$parent.addToPosibleReleases(this);
    },
    destroyed: function() {
        this.$parent.removeToPosibleReleases(this);
    },
    methods: {
        connectReliefBill: function() {
            sima.ajax.get('accounting/billReleasing/addRelief',{
                data:{
                    bill_ids: [this.bill.id],
                    relief_bill_ids: [this.posible_relief_bill.id]
                },
                async: true
            });
        },
        connectPartReliefBill: function() {
            sima.dialog.openVueComponent(
                "accounting-file_tab-bill_releasing_base_amount_and_vat", 
                {
                    releases: [],
                    bill_id: this.bill.id,
                    model_id: this.posible_relief_bill.id,
                    model_name: 'ReliefBill',
                },
            );
        },
        onClickCheckBox: function(event) {
           event.stopPropagation(); 
           this.$parent.posible_releases[this._uid].is_checked = !this.$parent.posible_releases[this._uid].is_checked;
        },
        onClickRow: function(event) {
            if (event.ctrlKey)
            {
                this.$parent.posible_releases[this._uid].is_checked = !this.$parent.posible_releases[this._uid].is_checked;
            }
        },
        setChecked: function(checked) {
            this.is_checked = checked;
        },
        getModelName: function() {
            return 'ReliefBill';
        },
        getModelId: function() {
            return this.posible_relief_bill.id;
        }
    },
    template: '#accounting-file_tab-bill_releasing_posible_relief_bill'
});
