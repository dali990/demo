/* global Vue, sima */

Vue.component('accounting-file_tab-bill_releasing_bill_relief',{
    props:  {
        bill_reliefs: [String, Array],
        bill: {validator: sima.vue.ProxyValidator}
    },
    computed: {
        relief_bill: function () { 
            return (this.bill_reliefs.length > 0) ? sima.vue.shot.getters.model(sima.vue.getVueModelTag('ReliefBill', this.bill_reliefs[0].relief_bill_id)) : '';
        },
        percent_of_bill: function () { 
            return Math.abs(this.bill_relief_amount * 100.0 / this.bill.amount);
        },
        bill_relief_ids: function () { 
            var bill_relief_ids = [];
            $.each(this.bill_reliefs, function(index, bill_relief)
            {
                bill_relief_ids.push(bill_relief.id);
            });
            return bill_relief_ids;
        },
        haveAllData: function () {
            var result = true;
            $.each(this.bill_reliefs, function(index, bill_relief){
                bill_relief.internal_vat;
                bill_relief.vat_rate;
                bill_relief.base_amount;
                bill_relief.vat_amount;
                bill_relief.amount;
                if
                (
                    bill_relief.shotStatus('internal_vat') !== 'OK' ||
                    bill_relief.shotStatus('vat_rate') !== 'OK' ||
                    bill_relief.shotStatus('base_amount') !== 'OK' ||
                    bill_relief.shotStatus('vat_amount') !== 'OK' ||
                    bill_relief.shotStatus('amount') !== 'OK'
                )
                {
                    result = false;
                    return false; /// each break
                }
            });
            return result;
        },
        is_checked: function(){
            if (typeof this.$parent.releases[this._uid] === 'undefined')
            {
                return false;
            }
            return this.$parent.releases[this._uid].is_checked;
        },
        count_relief_bill_amounts_differnet_from_zero: function(){
            var count = 0;
            if (this.relief_bill_amount0r !== 0)
            {
                count++;
            }
            if (this.relief_bill_amount10r !== 0)
            {
                count++;
            }
            if (this.relief_bill_amount20r !== 0)
            {
                count++;
            }
            if (this.relief_bill_amount10i !== 0)
            {
                count++;
            }
            if (this.relief_bill_amount20i !== 0)
            {
                count++;
            }
            return count;
        },
        class_expand_this: function(){
            return {
                'expand_this': this.count_relief_bill_amounts_differnet_from_zero > 3
            };
        }
    },
    data: function () {
        return {
            mouse_is_over: false,
            bill_relief_amount: 0,
            relief_bill_amount0r: 0,
            relief_bill_amount10r: 0,
            relief_bill_amount20r: 0,
            relief_bill_amount10i: 0,
            relief_bill_amount20i: 0
        };
    },
    watch: {
        haveAllData: function(new_val, old_val){
            this.calculatingDataParams();
        }
    },
    mounted: function() {
        var _this = this;
        this.calculatingDataParams();
        $(this.$el).on('destroyed', function(){
            _this.$destroy();
        });
        this.$parent.addToReleases(this);
    },
    destroyed: function() {
        this.$parent.removeToReleases(this);
    },
    methods: {
        cancelBillRelief: function() {
            var _this = this;
            sima.ajax.get('accounting/billReleasing/cancelRelease',{
                get_params: {
                    model_name: 'BillRelief'
                },
                data: {
                    model_ids: _this.bill_relief_ids,
                },
                async: true,
                success_function: function(response) {
                }
            });
        },
        editBillRelief: function() {
            sima.dialog.openVueComponent(
                "accounting-file_tab-bill_releasing_base_amount_and_vat", 
                {
                    releases: this.bill_reliefs,
                    bill_id: this.bill.id,
                    model_id: this.relief_bill.id,
                    model_name: 'ReliefBill'
                }
            );
        },
        onClickCheckBox: function(event) {
           event.stopPropagation(); 
           this.$parent.releases[this._uid].is_checked = !this.$parent.releases[this._uid].is_checked;
        },
        onClickRow: function(event) {
            if (event.ctrlKey)
            {
                this.$parent.releases[this._uid].is_checked = !this.$parent.releases[this._uid].is_checked;
            }
        },
        calculatingDataParams: function(){
            var _this = this;
            if(this.haveAllData === false)
            {
                return;
            }
            _this.bill_relief_amount = 0;
            $.each(_this.bill_reliefs, function(index, bill_relief){
                _this.bill_relief_amount += Number(bill_relief.amount);
                if(!bill_relief.internal_vat)
                {
                    if(bill_relief.vat_rate === '0')
                    {
                        _this.relief_bill_amount0r = bill_relief.base_amount;
                    }
                    else if(bill_relief.vat_rate === '10')
                    {
                        _this.relief_bill_amount10r = bill_relief.base_amount + bill_relief.vat_amount;
                    }
                    else
                    {
                        _this.relief_bill_amount20r = bill_relief.base_amount + bill_relief.vat_amount;
                    }
                }
                else
                {
                    if(bill_relief.vat_rate === '10')
                    {
                        _this.relief_bill_amount10i = bill_relief.base_amount + bill_relief.vat_amount;
                    }
                    else
                    {
                        _this.relief_bill_amount20i = bill_relief.base_amount +  bill_relief.vat_amount;
                    }
                }
            });
        },
        getModelName: function() {
            return 'ReliefBill';
        },
        getModelId: function() {
            return this.relief_bill.id;
        }
    },
    template: '#accounting-file_tab-bill_releasing_bill_relief'
});
