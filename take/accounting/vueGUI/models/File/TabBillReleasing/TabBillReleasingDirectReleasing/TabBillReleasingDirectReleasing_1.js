/* global Vue, sima */

Vue.component('accounting-file_tab-bill_releasing_direct_releasing',{
    props:  {
        release: {validator: sima.vue.ProxyValidator}
    },
    computed: {
        payment_amount: function () {
            return this.release.payment !== 'undefined' ? this.release.payment.amount : 0;
        },
        percent_of_bill: function () { 
            return this.release.amount * 100.0 / this.release.bill.amount;
        },
        bank_statement: function () {
            var bank_statement = '';
            if(typeof this.release.payment !== 'undefined' && this.release.payment !== '')
            {
                bank_statement = this.release.payment.bank_statement;
            }
            return bank_statement;
        },
        bank_name: function () {
            var bank_name = '';
            if(typeof this.release.payment !== 'undefined' && this.release.payment !== '')
            {
                if(typeof this.release.payment.bank_statement.account !== 'undefined' && this.release.payment.bank_statement.account !== '')
                {
                    if(typeof this.release.payment.bank_statement.account.bank.company !== 'undefined' && this.release.payment.bank_statement.account.bank.company !== '')
                    {
                        bank_name = this.release.payment.bank_statement.account.bank.company.DisplayName;
                    }
                }
            }
            return bank_name;
        },
        bank_account_number: function () {
            var bank_account_number = '';
            if(typeof this.release.payment !== 'undefined' && this.release.payment !== '')
            {
                if(typeof this.release.payment.bank_statement.account !== 'undefined' && this.release.payment.bank_statement.account !== '')
                {
                    bank_account_number = this.release.payment.bank_statement.account.number;
                }
            }
            return bank_account_number;
        },
        payment_date: function () {
            return (typeof this.release.payment !== 'undefined' && this.release.payment !== '') ?
                this.release.payment.payment_date :
                '';
        },
        payment_type: function () {
            return (typeof this.release.payment !== 'undefined' && this.release.payment !== '') ?
                this.release.payment.payment_type.DisplayName :
                '';
        },
        class_bank_name: function () {
            var class_name = "";
            if(typeof this.release.payment.bank_statement !== 'undefined' && sima.isEmpty(this.release.payment.bank_statement.id))
            {
                class_name = "_disabled";
            }
            return class_name;
        },
        is_checked: function(){
            if (typeof this.$parent.releases[this._uid] === 'undefined')
            {
                return false;
            }
            return this.$parent.releases[this._uid].is_checked;
        }
    },
    data: function () {
        return {
            mouse_is_over: false
        };
    },
    mounted: function() {
        var _this = this;
        $(this.$el).on('destroyed', function(){
            _this.$destroy();
        });
        this.$parent.addToReleases(this);
    },
    destroyed: function() {
        this.$parent.removeToReleases(this);
    },
    methods: {
        cancelBillRelease: function() {
            var _this = this;
            sima.ajax.get('accounting/billReleasing/cancelRelease',{
                get_params: {
                    model_name: 'BillRelease'
                },
                data: {
                    model_ids: [_this.release.id],
                },
                async: true,
                success_function: function(response) {
                }
            });
        },
        onClickCheckBox: function(event) {
           event.stopPropagation(); 
           this.$parent.releases[this._uid].is_checked = !this.$parent.releases[this._uid].is_checked;
        },
        onClickRow: function(event) {
            if (event.ctrlKey)
            {
                this.$parent.releases[this._uid].is_checked = !this.$parent.releases[this._uid].is_checked;
            }
        },
        getModelName: function() {
            return 'BillRelease';
        },
        getModelId: function() {
            return this.release.id;
        }
    },
    template: '#accounting-file_tab-bill_releasing_direct_releasing'
});
