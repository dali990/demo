/* global Vue, sima */

Vue.component('accounting-file_tab-bill_releasing',{
    props:  {
        model: {validator: sima.vue.ProxyValidator}
    },
    computed: {
        released_amount: function() {
            return this.advance_released_sum + this.direct_released_sum + this.reliefs_sum + this.debt_take_overs_sum;
        },
        released_percent: function() {
            return 100 * this.released_amount / this.model.amount;
        },
        releases_group_by_advance_bill: function() {
            var releases_order_by_advance_bill = this.model.scoped('advance_releases', ['orderByAdvanceBill']);
            var releases = [];
            var releases_for_advance_bill = [];
            if(releases_order_by_advance_bill.length > 0)
            {
                var advance_bill_id = releases_order_by_advance_bill[0].advance_bill_id;
                $.each(releases_order_by_advance_bill, function(index, advance_release){
                    if(advance_bill_id !== advance_release.advance_bill_id)
                    {
                        releases.push(releases_for_advance_bill);
                        releases_for_advance_bill = [];
                    }
                    releases_for_advance_bill.push(advance_release);
                    advance_bill_id = advance_release.advance_bill_id;
                });
                releases.push(releases_for_advance_bill);
            }
            return releases;
        },
        bill_reliefs_group_by_relief_bill: function() {
            var reliefs_order_by_relief_bill = this.model.scoped('reliefs', ['orderByReliefBill']);
            var reliefs = [];
            var reliefs_for_relief_bill = [];
            if(reliefs_order_by_relief_bill.length > 0)
            {
                var relief_bill_id = reliefs_order_by_relief_bill[0].relief_bill_id;
                $.each(reliefs_order_by_relief_bill, function(index, relief){
                    if(relief_bill_id !== relief.relief_bill_id)
                    {
                        reliefs.push(reliefs_for_relief_bill);
                        reliefs_for_relief_bill = [];
                    }
                    reliefs_for_relief_bill.push(relief);
                    relief_bill_id = relief.relief_bill_id;
                });
                reliefs.push(reliefs_for_relief_bill);
            }
            return reliefs;
        },
        advance_released_sum: function() {
            var advance_released_sum = 0;
            $.each(this.model.advance_releases, function(index, advance_release){
               advance_released_sum += advance_release.amount;
            });
            return advance_released_sum;
        },
        advance_released_percent: function() {
            return 100 * this.advance_released_sum / this.model.amount;
        },
        advance_released_percent_style: function(){
            return {
                width: this.advance_released_percent + '%',
                'background-color': '#34A853'
            };
        },
        direct_released_sum: function() {
            var direct_released_sum = 0;
            $.each(this.model.releases, function(index, release){
               direct_released_sum += release.amount;
            });
            return direct_released_sum;
        },
        direct_released_percent: function() {
            return 100 * this.direct_released_sum / this.model.amount;
        },
        direct_released_percent_style: function(){
            return {
                width: this.direct_released_percent + '%',
                'background-color': '#4285F4'
            };
        },
        reliefs_sum: function() {
            var reliefs_sum = 0;
            $.each(this.model.reliefs, function(index, relief){
               reliefs_sum += relief.amount;
            });
            return Math.abs(reliefs_sum);
        },
        reliefs_percent: function() {
            return 100 * this.reliefs_sum / this.model.amount;
        },
        reliefs_percent_style: function(){
            return {
                width: this.reliefs_percent + '%',
                'background-color': '#EA4335'
            };
        },
        debt_take_overs_sum: function() {
            var debt_take_overs_sum = 0;
            $.each(this.model.bill_debt_take_overs, function(index, bill_debt_take_over){
               debt_take_overs_sum += bill_debt_take_over.debt_value;
            });
            return debt_take_overs_sum;
        },
        debt_take_overs_percent: function() {
            return 100 * this.debt_take_overs_sum / this.model.amount;
        },
        debt_take_overs_percent_style: function(){
            return {
                width: this.debt_take_overs_percent + '%',
                'background-color': '#FBBC05'
            };
        },
        unreleased_amount: function() {
            return this.model.amount - this.released_amount;
        },
        unreleased_percent: function() {
            return 100 - this.released_percent;
        },
        unreleased_percent_style: function(){
            return {
                width: this.unreleased_percent + '%',
                'box-shadow': '#ccc 0px 0px 2px inset',
                'background': '#f6f6f6'
            };
        },
        posible_payments: function() {
            var posible_payments = (this.model.partner === '') ?
                [] :
                this.model.partner.scoped(
                        this.model.invoice ? 'payments_in' : 'payments_out',
                        ['unreleased']
                    );
            return !sima.isEmpty(posible_payments) ? posible_payments : [];
        },
        posible_advance_bills: function() {
            var posible_advance_bills = (this.model.partner === '') ?
                [] :
                this.model.partner.scoped(
                    this.model.invoice ? 'advance_bills_out' : 'advance_bills_in',
                    ['unreleased', 'connected']
                );        
            return !sima.isEmpty(posible_advance_bills) ? posible_advance_bills : [];
        },
        posible_relief_bills: function() {
            var posible_relief_bills = (this.model.partner === '') ?
                [] :
                this.model.partner.scoped(
                    this.model.invoice ? 'relief_bills_out' : 'relief_bills_in',
                    ['unreleased']
                );
            return !sima.isEmpty(posible_relief_bills) ? posible_relief_bills : [];
        },
        //ovo su debitor i creditor
        //za ovo ide pun iznos duga iz ugovora
        posible_debt_take_overs: function() {
            if (this.model.partner === '')
            {
                return [];
            }
            var contracts = [];
            if (this.model.invoice)
            {
                //izlazna faktura
                //A - conn1
                var local_contracts = this.model.partner.scoped('debt_take_over_as_debtor',['unreleased', 'systemCompanyCreditor']);
                contracts = local_contracts;
//                //sve gde je duznik i partner sistemska firma
//                for(var i in debtor_contracts)
//                {
//                    contracts.push({
//                        contract: debtor_contracts[i].DisplayHTML,
//                        value: debtor_contracts[i].debt_value
//                    });
//                }
//                var takes_over = this.model.partner.scoped('debt_take_over_as_takes_over',['unreleased']);
                
                
//                return  debtors.concat(takes_over);
            }
            else
            {
                //creditor je partner a duznik je sistemska kompanija
                var local_contracts = this.model.partner.scoped('debt_take_over_as_creditor',['unreleased', 'systemCompanyDebtor']);
                contracts = local_contracts;
//                for(var i in local_contracts)
//                {
//                    contracts.push({
//                        contract: local_contracts[i].DisplayHTML,
//                        value: local_contracts[i].debt_value
//                    });
//                }
//                return [];
//                var creditors = this.model.partner.scoped('debt_take_over_as_creditor',['unreleased']);
//                for(var i in creditors)
//                {
//                    contracts.push({
//                        contract: creditors.DisplayHTML,
//                        value: creditors.debt_value
//                    });
//                }
//                var takes_over = this.model.partner.scoped('debt_take_over_as_takes_over',['unreleased']);
//                return creditors.concat(takes_over);
            }
            return contracts;
        },
        splitter_style: function() {
            return (this.released_percent < 100) ? 'sima-layout-panel _splitter _vertical' : '';
        },
        is_checked_all_releases: function(){
            var is_checked_all_releases = false;
            for (var key in this.releases)
            {
                if(this.releases[key].is_checked)
                {
                    is_checked_all_releases = true;
                }
                else
                {
                    is_checked_all_releases = false;
                    break;
                }
            }
            return is_checked_all_releases;
        },
        is_checked_all_posible_releases: function(){
            var is_checked_all_posible_releases = false;
            for (var key in this.posible_releases)
            {
                if(this.posible_releases[key].is_checked)
                {
                    is_checked_all_posible_releases = true;
                }
                else
                {
                    is_checked_all_posible_releases = false;
                    break;
                }
            }
            return is_checked_all_posible_releases;
        },
        is_least_one_release_checked: function() {
            var is_least_one_release_checked = false;
            for (var key in this.releases)
            {
                if(this.releases[key].is_checked)
                {
                    is_least_one_release_checked = true;
                    break;
                }
            }
            return is_least_one_release_checked;
        },
        is_least_one_posible_release_checked: function() {
            var is_least_one_posible_release_checked = false;
            for (var key in this.posible_releases)
            {
                if(this.posible_releases[key].is_checked)
                {
                    is_least_one_posible_release_checked = true;
                    break;
                }
            }
            return is_least_one_posible_release_checked;
        }
    },
    data: function () {
        return {
            releases: {},
            posible_releases: {}
        };
    },
    mounted: function() {
        var _this = this;
        $(this.$el).on('destroyed', function(){
            _this.$destroy();
        });
    },
    updated: function () {
        this.$nextTick(function () {
            sima.layout.allignObject($(this.$el), 'TRIGGER_tab_bill_releasing_updated');
        });
    },
    
    methods: {
        addToReleases: function(release)
        {
            var element_value = {
                is_checked: false,
                release: release
            };
            Vue.set(this.releases, release._uid, element_value);
        },
        removeToReleases: function(release)
        {
            Vue.delete(this.releases, release._uid);
        },
        addToPosibleReleases: function(posible_release)
        {
            var element_value = {
                is_checked: false,
                posible_release: posible_release
            };
            Vue.set(this.posible_releases, posible_release._uid, element_value);
        },
        removeToPosibleReleases: function(posible_release)
        {
            Vue.delete(this.posible_releases, posible_release._uid);
        },
        multiselectCancelReleases: function()
        {
            var _this = this;
            var selected_release_names_and_ids = [];
            for (var key in this.releases)
            {
                if(this.releases[key].is_checked)
                {
                    selected_release_names_and_ids.push({
                        model_name: this.releases[key].release.getModelName(),
                        model_id: this.releases[key].release.getModelId()
                    });
                }
            }
            sima.ajaxLong.start('accounting/billReleasing/cancelReleases',{
                data:{
                    selected_releases: selected_release_names_and_ids,
                    bill_id: _this.model.id
                },
                onEnd: function(response) {
                }
            });
        },
        multiselectConnectPosibleReleases: function()
        {
            for (var key in this.posible_releases)
            {
                if(this.posible_releases[key].is_checked)
                {
                    var posible_release = this.posible_releases[key].posible_release;
                    if(posible_release.getModelName() === 'AdvanceBill')
                    {
                        this.liq_by_advance(posible_release.getModelId());
                    }
                    else if(posible_release.getModelName() === 'Payment')
                    {
                        this.liq_by_payment(posible_release.getModelId());
                    }
                    else if(posible_release.getModelName() === 'ReliefBill')
                    {
                        this.liq_by_relief(posible_release.getModelId());
                    }
                    else if(posible_release.getModelName() === 'DebtTakeOver')
                    {
                        this.liq_by_debt(posible_release.getModelId());
                    }
                    this.posible_releases[key].is_checked = false;
                }
            }
        },
        liq_by_advance: function(advance_bill_id)
        {
            sima.ajax.get('accounting/billReleasing/addAdvance',{
                data:{
                    bill_ids: [this.model.id],
                    advance_bill_ids: [advance_bill_id]
                },
                async: true
            });
        },
        liq_by_payment: function(payment_id)
        {
            sima.ajax.get('accounting/billReleasing/add',{
                data:{
                    bill_ids: [this.model.id],
                    payment_ids: [payment_id]
                },
                async: true
            });
        },
        liq_by_relief:function(relief_bill_id)
        {              
            sima.ajax.get('accounting/billReleasing/addRelief',{
                data:{
                    bill_ids: [this.model.id],
                    relief_bill_ids: [relief_bill_id]
                },
                async: true
            });
        },
        liq_by_debt:function(debt_take_over_id)
        {                      
            sima.ajax.get('accounting/billReleasing/addDebtTakeOver',{
                data:{
                    bill_ids: [this.model.id],
                    debt_take_over_ids: [debt_take_over_id]
                },
                async: true
            });
        },
        clickCheckedAllReleases: function(event) {
            var new_value = !this.is_checked_all_releases;
            for (var key in this.releases)
            {
                this.releases[key].is_checked = new_value;
            }
        },
        clickCheckedAllPosibleReleases: function(event) {
            var new_value = !this.is_checked_all_posible_releases;
            for (var key in this.posible_releases)
            {
                this.posible_releases[key].is_checked = new_value;
            }
        }
    },
    template: '#accounting-file_tab-bill_releasing'
});
