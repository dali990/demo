<script type="text/x-template" id="accounting-file_tab-bill_releasing_debt_take_over">
    <div class='accounting_file_tab_bill_releasing_debt_take_over' 
        @mouseover="mouse_is_over = true" 
        @mouseleave="mouse_is_over = false"
        v-on:click="onClickRow"
        v-bind:class="{'_selected': is_checked}"
    >       
        <div class="check-col">
            <input 
                type="checkbox"
                v-show="mouse_is_over || this.is_checked"
                v-on:click="onClickCheckBox"
                v-bind:checked="this.is_checked"
            >
        </div>
        <div class="amount-col">
            <span class="value monospace">{{this.bill_debt_take_over.debt_value | formatNumber}}</span></br>
            <span class="monospace percent">{{percent_of_bill | formatNumber}}</span>
        </div>
        <div class="date-col">
            <span>{{this.bill_debt_take_over.debt_take_over.sign_date}}</span>
        </div>
        <div class="name-col">
            <sima-model-display-html v-bind:model="bill_debt_take_over.debt_take_over"></sima-model-display-html>
        </div>
        <div  class="available-amount-col">
        </div>
        <div class="div_options">
            <sima-button  
                class="icon-only"
                title="<?=Yii::t('AccountingModule.Bill','Cancel')?>" 
                v-on:click="cancelDebtTakeOver"
                v-show="mouse_is_over"
                security_question="<?=Yii::t('AccountingModule.Bill','AreYouSureYouWantToCancel')?>"
            >
            <span class="sima-icon sir-ban"></span>
            </sima-button>
            <!-- <sima-model-options               
                v-bind:model="bill_debt_take_over" 
                v-show="mouse_is_over"
                v-bind:exclude_options="['delete']"
            ></sima-model-options> -->
            <sima-button  
                class="icon-only"
                title="Izmenite" 
                v-show="mouse_is_over"
            >
                <span class="sima-icon sil-edit"></span>
            </sima-button>
        </div>
    </div>
</script>
