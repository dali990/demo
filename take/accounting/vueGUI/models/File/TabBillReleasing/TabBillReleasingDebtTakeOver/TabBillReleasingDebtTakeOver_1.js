/* global Vue, sima */

Vue.component('accounting-file_tab-bill_releasing_debt_take_over',{
    props:  {
        bill_debt_take_over: {validator: sima.vue.ProxyValidator}
    },
    computed: {
        percent_of_bill: function () { 
            return this.bill_debt_take_over.debt_value * 100.0 / this.bill_debt_take_over.bill.amount;
        },
        is_checked: function(){
            if (typeof this.$parent.releases[this._uid] === 'undefined')
            {
                return false;
            }
            return this.$parent.releases[this._uid].is_checked;
        }
    },
    data: function () {
        return {
            mouse_is_over: false
        };
    },
    mounted: function() {
        var _this = this;
        $(this.$el).on('destroyed', function(){
            _this.$destroy();
        });
        this.$parent.addToReleases(this);
    },
    destroyed: function() {
        this.$parent.removeToReleases(this);
    },
    methods: {
        cancelDebtTakeOver: function() {
            var _this = this;
            sima.ajax.get('accounting/billReleasing/cancelRelease',{
                get_params: {
                    model_name: 'BillDebtTakeOver'
                },
                data: {
                    model_ids: [_this.bill_debt_take_over.id],
                },
                async: true,
                success_function: function(response) {
                }
            });
        },
        onClickCheckBox: function(event) {
           event.stopPropagation(); 
           this.$parent.releases[this._uid].is_checked = !this.$parent.releases[this._uid].is_checked;
        },
        onClickRow: function(event) {
            if (event.ctrlKey)
            {
                this.$parent.releases[this._uid].is_checked = !this.$parent.releases[this._uid].is_checked;
            }
        },
        getModelName: function() {
            return 'BillDebtTakeOver';
        },
        getModelId: function() {
            return this.bill_debt_take_over.id;
        }
    },
    template: '#accounting-file_tab-bill_releasing_debt_take_over'
});
