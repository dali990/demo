/* global Vue, sima */

Vue.component('accounting-file_tab-bill_releasing_posible_payment',{
    props:  {
        posible_payment: {validator: sima.vue.ProxyValidator},
        bill: {validator: sima.vue.ProxyValidator}
    },
    computed: {
        bank_name: function () {
            var bank_name = '';
            if (typeof this.posible_payment.bank_statement.account !== 'undefined' && this.posible_payment.bank_statement.account !== '')
            {
                if(typeof this.posible_payment.bank_statement.account.bank.company !== 'undefined' && this.posible_payment.bank_statement.account.bank.company !== '')
                {
                    bank_name =  this.posible_payment.bank_statement.account.bank.company.DisplayName;
                }
            }
            return bank_name;
        },
        bank_statement: function () {
            var bank_statement = '';
            if(typeof this.posible_payment.bank_statement !== 'undefined' && this.posible_payment.bank_statement !== '')
            {
                bank_statement = this.posible_payment.bank_statement;
            }
            return bank_statement;
        },
        bank_account_number: function () {
            return (typeof this.posible_payment.bank_statement.account !== 'undefined' && this.posible_payment.bank_statement.account !== '') ?
                this.posible_payment.bank_statement.account.number : '';
        },
        percent_of_bill: function () { 
            var percent_of_bill = this.posible_payment.unreleased_amount * 100.0 / this.bill.amount;
            return percent_of_bill > 100 ? 100 : percent_of_bill;
        },
        class_bank_name: function () {
            var class_name = "";
            if(typeof this.posible_payment.bank_statement !== 'undefined' && sima.isEmpty(this.posible_payment.bank_statement.id))
            {
                class_name = "_disabled";
            }
            return class_name;
        },
        is_checked: function(){
            if (typeof this.$parent.posible_releases[this._uid] === 'undefined')
            {
                return false;
            }
            return this.$parent.posible_releases[this._uid].is_checked;
        }
    },
    data: function () {
        return {
            mouse_is_over: false
        };
    },
    mounted: function() {
        var _this = this;
        $(this.$el).on('destroyed', function(){
            _this.$destroy();
        });
        this.$parent.addToPosibleReleases(this);
    },
    destroyed: function() {
        this.$parent.removeToPosibleReleases(this);
    },
    methods: {
        connectBillRelease: function() {
            sima.ajax.get('accounting/billReleasing/add',{
                data:{
                    bill_ids: [this.bill.id],
                    payment_ids: [this.posible_payment.id]
                },
                async: true
            });
        },
        connectPartBillRelease: function() {
            var _this = this;
            var model_id = {
                uniq_check: {
                    bill_id: _this.bill.id,
                    payment_id: _this.posible_payment.id
                }
            };
            sima.model.form('BillRelease', model_id, {
                init_data: {
                    BillRelease: {
                        bill_id: _this.bill.id,
                        payment_id: _this.posible_payment.id
                    }
                },
            });
        },
        onClickCheckBox: function(event) {
           event.stopPropagation(); 
           this.$parent.posible_releases[this._uid].is_checked = !this.$parent.posible_releases[this._uid].is_checked;
        },
        onClickRow: function(event) {
            if (event.ctrlKey)
            {
                this.$parent.posible_releases[this._uid].is_checked = !this.$parent.posible_releases[this._uid].is_checked;
            }
        },
        setChecked: function(checked) {
            this.is_checked = checked;
        },
        getModelName: function() {
            return 'Payment';
        },
        getModelId: function() {
            return this.posible_payment.id;
        }
    },
    template: '#accounting-file_tab-bill_releasing_posible_payment'
});
