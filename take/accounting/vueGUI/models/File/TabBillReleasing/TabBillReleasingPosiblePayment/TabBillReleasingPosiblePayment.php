<script type="text/x-template" id="accounting-file_tab-bill_releasing_posible_payment">
    <div class='accounting_file_tab_bill_releasing_posible_payment' 
        @mouseover="mouse_is_over = true" 
        @mouseleave="mouse_is_over = false"
        v-on:click="onClickRow"
        v-bind:class="{'_selected': is_checked}"
    >
        <div class="check-col">
            <input 
                type="checkbox"
                v-show="mouse_is_over || this.is_checked"
                v-on:click="onClickCheckBox"
                v-bind:checked="this.is_checked"
            >
        </div>
        <div class="amount-col">
            <span class="value monospace">{{this.posible_payment.amount | formatNumber}}</span><br>
            <span class="monospace percent">{{percent_of_bill | formatNumber}}</span>
        </div>
        <div class="date-col">
            <span>{{this.posible_payment.payment_date}}</span>
        </div>
        <div class="name-col">
            <sima-model-display-html 
                v-bind:model="this.bank_statement"
                v-bind:display_name="this.bank_name"
                v-bind:title="bank_account_number"
            >
            </sima-model-display-html>
        </div>
        <div  class="available-amount-col">
            <span class="value monospace">{{this.posible_payment.unreleased_amount | formatNumber}}</span><br>
            <span class="monospace">{{this.posible_payment.payment_type.DisplayName}}</span>
        </div>
        <div class="div_options">
            <sima-button  
                class="icon-only"
                title="<?=Yii::t('AccountingModule.Bill','Connect')?>" 
                v-on:click="connectBillRelease"
                v-show="mouse_is_over"
                security_question="<?=Yii::t('AccountingModule.Bill','AreYouSureYouWantToConnect')?>"
            >
                <span class="sima-icon sir-connect"></span>
            </sima-button>
            <sima-button  
                class="icon-only"
                title="<?=Yii::t('AccountingModule.Bill','ConnectPart')?>" 
                v-on:click="connectPartBillRelease"
                v-show="mouse_is_over"
            >
                <span class="sima-icon sil-connect-alt"></span>
            </sima-button>
        </div>
    </div>
</script>
