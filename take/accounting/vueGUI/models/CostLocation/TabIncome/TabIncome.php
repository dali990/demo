<script type="text/x-template" id="accounting-cost_location_tab-income">
    
<div class="sima-layout-panel accounting-cost_location_tab-income">
    <table>
        <tr>
            <th colspan='4'><?=Yii::t('AccountingModule.CostLocation', 'Partners')?></th>
        </tr>
        <tr>
            <th><?=Yii::t('AccountingModule.CostLocation', 'Partner')?></th>
            <th><?=Yii::t('AccountingModule.CostLocation', 'Amount')?></th>
            <th><?=Yii::t('AccountingModule.CostLocation', 'ForPayment')?></th>
            <th><?=Yii::t('AccountingModule.CostLocation', 'Advance')?></th>
        </tr>
        <tr v-for="partner in cost_location.partners" v-bind:key="partner.id" v-if="partner.income > 0 || partner.income_advance > 0">
            <td v-html="partner.DisplayHTML"></td>
            <td><span  class="_number sima-display-html sima-model-link" 
                        @click="preview_bill_item_list('income',{cost_location_id: cost_location.id, partner_id:partner.id})">
                    {{partner.income | formatNumber}}
            </span></td> 
            <td><span  class="_number sima-display-html sima-model-link" 
                        @click="preview_bill_item_list('income_notpayed',{cost_location_id: cost_location.id, partner_id:partner.id})">
                    {{partner.income_notpayed | formatNumber}}
            </span></td> 
            <td><span  class="_number sima-display-html sima-model-link" 
                        @click="preview_bill_item_list('income_advance',{cost_location_id: cost_location.id, partner_id:partner.id})">
                    {{partner.income_advance | formatNumber}}
            </span></td>
        </tr>
    </table>
</div>
    
</script>