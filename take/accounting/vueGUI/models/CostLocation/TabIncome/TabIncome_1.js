/* global Vue, sima */

Vue.component('accounting-cost_location_tab-income',{
    template: '#accounting-cost_location_tab-income',
    props:  {
        cost_location: {
            validator: sima.vue.ProxyValidator
        }
    },
    computed: {
        
    },
    data: function () {
        return {
            
        };
    },
    mounted: function() {
        var _this = this;
        $(this.$el).on('destroyed', function(){
            _this.$destroy();
        });
    },
    methods: {
        preview_bill_item_list: function(filter_type, model_filter){
            var component_name = 'accounting-list_bill_items_in_bills';
            var title = 'Pregled racuna';
            var params = {
                action: 'accounting/billItems/ReportFromBillItems',
                action_data: {
                    filter_type: filter_type,
                    model_filter: model_filter
                }
            };
            sima.dialog.openVueComponent(component_name, params, title, {fullscreen:true});
        }
    }
});