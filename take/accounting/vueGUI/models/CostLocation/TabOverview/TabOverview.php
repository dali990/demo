<script type="text/x-template" id="accounting-cost_location_tab-overview">
    

<div class="sima-layout-panel _horizontal accounting-cost_location-overview" >

    <div  class='sima-layout-fixed-panel'>
        <h3>
            Mesto troškа: {{cost_location.DisplayNameFull}} | <span v-html="cost_location.model.DisplayHTML"></span>
        </h3>
    </div>
    <div  class='sima-layout-fixed-panel' v-if="has_virtual_parents">
        Pripada zbirnim mestima troska:
        <ul>
            <li v-for="v_parent in cost_location.virtual_parents" v-bind:key="v_parent.id">
                <span v-html="v_parent.DisplayHTML"></span> - {{v_parent.work_name}}
            </li>
        </ul>
    </div>

    <div class="sima-layout-panel">
        <table>
            <tr>
                <th><sima-yii-button
                    title="Prikazi detalje" 
                    v-bind:class="{'_pressed':this.show_sub_costlocations_detail}"
                    @click.native="toggle_sub_costlocations_detail"
                ></sima-yii-button></th>
                <th colspan="3">prihod</th>
                <th colspan="3">trosak</th>
            </tr>
            <tr>
                <th></th>
                <th>iznos</th>
                <th>za naplatu</th>
                <th>avans</th>
                <th>iznos</th>
                <th><?=Yii::t('AccountingModule.CostLocation', 'ForPayingWithoutMaterialAndVat')?></th>
                <th>avans</th>
            </tr>
            <tr>
                <td>ukupno</td>
                <td class="_number"><span  class="sima-display-html sima-model-link" 
                        @click="preview_bill_item_list('income_r',{cost_location_id: cost_location.id})">
                        {{cost_location.income_r | formatNumber}}
                </span></td> 
                <td class="_number"><span  class="sima-display-html sima-model-link" 
                        @click="preview_bill_item_list('income_notpayed_r',{cost_location_id: cost_location.id})">
                        {{cost_location.income_notpayed_r | formatNumber}}
                </span></td> 
                <td class="_number"><span  class="sima-display-html sima-model-link" 
                        @click="preview_bill_item_list('income_advance_r',{cost_location_id: cost_location.id})">
                        {{cost_location.income_advance_r | formatNumber}}
                </span></td> 
                <td class="_number"><span  class="sima-display-html sima-model-link" 
                        @click="preview_bill_item_list('cost_r',{cost_location_id: cost_location.id})">
                        {{cost_location.cost_r | formatNumber}}
                </span></td> 
                <td class="_number"><span  class="sima-display-html sima-model-link" 
                        @click="preview_bill_item_list('cost_notpayed_r',{cost_location_id: cost_location.id})">
                        {{cost_location.cost_notpayed_r | formatNumber}}
                </span></td> 
                <td class="_number"><span  class="sima-display-html sima-model-link" 
                        @click="preview_bill_item_list('cost_advance_r',{cost_location_id: cost_location.id})">
                        {{cost_location.cost_advance_r | formatNumber}}
                </span></td>
            </tr>
            <template v-if="show_sub_costlocations_detail">
                <tr>
                    <td>direktno</td>
                    <td class="_number"><span  class="sima-display-html sima-model-link" 
                            @click="preview_bill_item_list('income',{cost_location_id: cost_location.id})">
                            {{cost_location.income | formatNumber}}
                    </span></td> 
                    <td class="_number"><span  class="sima-display-html sima-model-link" 
                            @click="preview_bill_item_list('income_notpayed',{cost_location_id: cost_location.id})">
                            {{cost_location.income_notpayed | formatNumber}}
                    </span></td>
                    <td class="_number"><span  class="sima-display-html sima-model-link" 
                            @click="preview_bill_item_list('income_advance',{cost_location_id: cost_location.id})">
                            {{cost_location.income_advance | formatNumber}}
                    </span></td>
                    <td class="_number"><span  class="sima-display-html sima-model-link" 
                            @click="preview_bill_item_list('cost',{cost_location_id: cost_location.id})">
                            {{cost_location.cost | formatNumber}}
                    </span></td> 
                    <td class="_number"><span  class="sima-display-html sima-model-link" 
                            @click="preview_bill_item_list('cost_notpayed',{cost_location_id: cost_location.id})">
                            {{cost_location.cost_notpayed | formatNumber}}
                    </span></td>
                    <td class="_number"><span  class="sima-display-html sima-model-link" 
                            @click="preview_bill_item_list('cost_advance',{cost_location_id: cost_location.id})">
                            {{cost_location.cost_advance | formatNumber}}
                    </span></td>
                </tr>

                <tr v-for="child in scoped_children" v-bind:key="child.id" >
                    <td><span v-html="child.DisplayHTML"></span> - {{child.work_name}}</td>
                    <td class="_number"><span  class="sima-display-html sima-model-link" 
                            @click="preview_bill_item_list('income_r',{cost_location_id: child.id})">
                        {{child.income_r | formatNumber}}
                    </span></td> 
                    <td class="_number"><span  class="sima-display-html sima-model-link" 
                            @click="preview_bill_item_list('income_notpayed_r',{cost_location_id: child.id})">
                        {{child.income_notpayed_r | formatNumber}}
                    </span></td> 
                    <td class="_number"><span  class="sima-display-html sima-model-link" 
                            @click="preview_bill_item_list('income_advance_r',{cost_location_id: child.id})">
                        {{child.income_advance_r | formatNumber}}
                    </span></td> 
                    <td class="_number"><span  class="sima-display-html sima-model-link" 
                            @click="preview_bill_item_list('cost_r',{cost_location_id: child.id})">
                        {{child.cost_r | formatNumber}}
                    </span></td> 
                    <td class="_number"><span  class="sima-display-html sima-model-link" 
                            @click="preview_bill_item_list('cost_notpayed_r',{cost_location_id: child.id})">
                        {{child.cost_notpayed_r | formatNumber}}
                    </span></td> 
                    <td class="_number"><span  class="sima-display-html sima-model-link" 
                            @click="preview_bill_item_list('cost_advance_r',{cost_location_id: child.id})">
                        {{child.cost_advance_r | formatNumber}}
                    </span></td>
                </tr>
                <tr v-for="child in cost_location.virtual_children" v-bind:key="child.id" >
                    <td>*<span v-html="child.DisplayHTML"></span> - {{child.work_name}}</td>
                    <td class="_number"><span  class="sima-display-html sima-model-link" 
                            @click="preview_bill_item_list('income_r',{cost_location_id: child.id})">
                        {{child.income_r | formatNumber}}
                    </span></td> 
                    <td class="_number"><span  class="sima-display-html sima-model-link" 
                            @click="preview_bill_item_list('income_notpayed_r',{cost_location_id: child.id})">
                        {{child.income_notpayed_r | formatNumber}}
                    </span></td> 
                    <td class="_number"><span  class="sima-display-html sima-model-link" 
                            @click="preview_bill_item_list('income_advance_r',{cost_location_id: child.id})">
                        {{child.income_advance_r | formatNumber}}
                    </span></td> 
                    <td class="_number"><span  class="sima-display-html sima-model-link" 
                            @click="preview_bill_item_list('cost_r',{cost_location_id: child.id})">
                        {{child.cost_r | formatNumber}}
                    </span></td> 
                    <td class="_number"><span  class="sima-display-html sima-model-link" 
                            @click="preview_bill_item_list('cost_notpayed_r',{cost_location_id: child.id})">
                        {{child.cost_notpayed_r | formatNumber}}
                    </span></td> 
                    <td class="_number"><span  class="sima-display-html sima-model-link" 
                            @click="preview_bill_item_list('cost_advance_r',{cost_location_id: child.id})">
                        {{child.cost_advance_r | formatNumber}}
                    </span></td>
                </tr>

            </template>
        </table>
    </div>

    <div v-if='false' class='sima-layout-fixed-panel'>
        <table>
            <tr>
                <td>godina</td>
                <td  v-for="year in this.years" colspan="12" style="text-align: center;">{{year}}</td>
            </tr>
            <tr>
                <td>mesec</td>
                <template   v-for="year in this.years">
                    <td v-for="month in months"  style="text-align: center;">{{month}}</td>
                </template>
            </tr>
            <tr>
                <td>prihod</td>
                <template v-for="year in this.years">
                    <td v-for="month in months"><span  class="sima-display-html sima-model-link"   style="text-align: right; min-width: 100px"
                            @click="preview_bill_item_list('income',{cost_location_id: cost_location.id, month: month, year:year})">
                        {{cost_location.income | formatNumber}}
                    </span></td>
                </template>
            </tr>
            <tr>
                <td>rashod</td>
                <template   v-for="year in this.years">
                    <td v-for="month in months"><span  class="sima-display-html sima-model-link"  style="text-align: right; min-width: 100px"
                            @click="preview_bill_item_list('cost',{cost_location_id: cost_location.id, month: month, year:year})">
                        {{cost_location.cost | formatNumber}}
                    </span></td>
                </template>
            </tr>
        </table>
    </div>

</div>
    
</script>