/* global sima, Vue */

Vue.component('accounting-cost_location_tab-overview',{
    props:  {
        cost_location_tag: String
    },
    computed: {
        cost_location: function() {
//            return sima.vue.shot.getters.model(this.cost_location_tag,['withIncomeCost','withIncomeCostR']);
            return sima.vue.shot.getters.model(this.cost_location_tag,['withIncomeCostALL']);
        },
        scoped_children: function(){
//            return this.cost_location.scoped('children',[],['withIncomeCost','withIncomeCostR']);
            return this.cost_location.scoped('children',[],['withIncomeCostALL']);
        },
        has_virtual_parents: function (){
            return this.cost_location.virtual_parents.length > 0;
        }
    },
    data: function () {
        return {
            months: [1,2,3,4,5,6,7,8,9,10,11,12],
            years: [2017, 2018, 2019],
            show_sub_costlocations_detail: false,
//            selected_cost_locations: [],
//            selected_bill_items: []
        };
    },
    mounted: function() {
        var _this = this;
        $(this.$el).on('destroyed', function(){
            _this.$destroy();
        });
    },
    updated: function () {
        this.$nextTick(function () {
            sima.layout.allignObject($(this.$el), 'TRIGGER_accounting-cost_location_tab-overview_updated');
        });
    },
    methods: {
        toggle_sub_costlocations_detail: function(){
            this.show_sub_costlocations_detail = !this.show_sub_costlocations_detail;
        },
        preview_bill_item_list: function(filter_type, model_filter){
            var component_name = 'accounting-list_bill_items_in_bills';
            var title = 'Pregled racuna';
            var params = {
                action: 'accounting/billItems/ReportFromBillItems',
                action_data: {
                    filter_type: filter_type,
                    model_filter: model_filter
                }
            };
            sima.dialog.openVueComponent(component_name, params, title, {fullscreen:true});
        }
    },
    template: '#accounting-cost_location_tab-overview'
});