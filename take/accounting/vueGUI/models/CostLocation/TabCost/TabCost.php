<script type="text/x-template" id="accounting-cost_location_tab-cost">
    
<div class="sima-layout-panel accounting-cost_location_tab-cost">
    <table>
        <tr>
            <th colspan='4'><?=Yii::t('AccountingModule.CostLocation', 'DirectCostSpecification')?></th>
        </tr>
        <tr>
            <th></th>
            <th><?=Yii::t('AccountingModule.CostLocation', 'Total')?></th>
            <th><?=Yii::t('AccountingModule.CostLocation', 'Unpaid')?></th>
            <th><?=Yii::t('AccountingModule.CostLocation', 'RemainingAdvances')?></th>
        </tr>
        <tr v-for="(_item, report_type) in this.cost_location_report">
            <td>{{_item.title}}</td>
            <td v-for="column in ['cost','cost_notpayed','cost_advance']" class="_number">
                <span   class="sima-display-html sima-model-link" 
                        @click="preview_specification(column,{report_type:report_type})">
                        {{_item[column]  | formatNumber}}
                 </span>
            </td>
        </tr>
        <tr>
            <th>{{this.cost_location_report_sum.title}}</th>
            <th class="_number">{{this.cost_location_report_sum.cost | formatNumber}}</th> 
            <th class="_number">{{this.cost_location_report_sum.cost_notpayed | formatNumber}}</th>  
            <th class="_number">{{this.cost_location_report_sum.cost_advance | formatNumber}}</th>  
        </tr>

    </table>
    
    <br />
    
    <table>
        <tr>
            <th colspan='4'><?=Yii::t('AccountingModule.CostLocation', 'Partners')?></th>
        </tr>
        <tr>
            <th><?=Yii::t('AccountingModule.CostLocation', 'Partner')?></th>
            <th><?=Yii::t('AccountingModule.CostLocation', 'Amount')?></th>
            <th><?=Yii::t('AccountingModule.CostLocation', 'ForPayingWithoutMaterialAndVat')?></th>
            <th><?=Yii::t('AccountingModule.CostLocation', 'Advance')?></th>
        </tr>
        <tr v-for="(_item, partner_id) in this.cost_location_partner_report">
            <td v-if="_item.partner === 'NULL'">
                <span style="color: red;">NEODREDJEN</span>
            </td>
            <td v-else><sima-model-display-html :model="_item.partner"></sima-model-display-html></td>
            
            <td v-for="column in ['cost','cost_notpayed','cost_advance']" class="_number">
                <span   class="sima-display-html sima-model-link" 
                        @click="preview_specification(column,{partner_id:partner_id})">
                        {{_item[column]  | formatNumber}}
                 </span>
            </td>
        </tr>
    </table>
</div>
    
</script>