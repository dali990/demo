/* global Vue, sima */

Vue.component('accounting-cost_location_tab-cost',{
    template: '#accounting-cost_location_tab-cost',
    props:  {
        model: {
            validator: sima.vue.ProxyValidator
        }
    },
    computed: {
        cost_location_report: function() {
            var result_costs = {
                SERVICES: {title:'Usluge',   cost:0,cost_notpayed:0,cost_advance:0},
                WAREHOUSE:{title:'Materijal',cost:0,cost_notpayed:0,cost_advance:0},
                SALARY:   {title:'Plate',    cost:0,cost_notpayed:0,cost_advance:0},
                CASH_DESK:{title:'Blagajna', cost:0,cost_notpayed:0,cost_advance:0}
            };
                        
            var _top_cost = sima.vue.shot.getters.custom_data(this, this.report_list_uniq_key, {
                action: 'accounting/report/sum',
                action_data: {
                    group_by: ['report_type'],
                    model_filter: {
                        cost_location: {
                            ids: [this.model.id]
                        },
                        
//                        month: {ids: this.can_select_months ? this.selected_months : [] },
//                        year: {ids: this.selected_years},
                    },
                    report_type: 'total_r'
                }
            });
                
            for (var item_i in _top_cost)
            {
                
                var element = _top_cost[item_i];
                if (typeof result_costs[element.report_type] === 'undefined')
                {
                    result_costs[element.report_type] = {title:element.report_type,   cost:0,cost_notpayed:0,cost_advance:0};
                }
                result_costs[element.report_type].cost          = element.cost;
                result_costs[element.report_type].cost_notpayed = element.cost_notpayed;
                result_costs[element.report_type].cost_advance  = element.cost_advance;
            }
            
            return result_costs;
        },
        cost_location_report_sum: function()
        {
            var _sum = {title:'UKUPNO',   cost:0,cost_notpayed:0,cost_advance:0};
            for (var item_i in this.cost_location_report)
            {
                var element = this.cost_location_report[item_i];
                _sum.cost += element.cost;
                _sum.cost_notpayed += element.cost_notpayed;
                _sum.cost_advance += element.cost_advance;
            }
            return _sum;
        },
        cost_location_partner_report: function() {
            var result_costs = {};
            
            var _top_cost = sima.vue.shot.getters.custom_data(this, this.report_partner_list_uniq_key, {
                action: 'accounting/report/sum',
                action_data: {
                    group_by: ['partner'],
                    model_filter: {
                        cost_location: {
                            ids: [this.model.id]
                        },
                        
//                        month: {ids: this.can_select_months ? this.selected_months : [] },
//                        year: {ids: this.selected_years},
                    },
                    report_type: 'total_r'
                }
            });
                
            for (var item_i in _top_cost)
            {
                var element = _top_cost[item_i];
                var partner = (element.partner_id === null)?
                        'NULL'
                        :
                        sima.vue.shot.getters.model(element.partner_tag);

                result_costs[element.partner_id] = {
                    partner:       partner,
                    cost:          element.cost,
                    cost_notpayed: element.cost_notpayed,
                    cost_advance:  element.cost_advance
                };
            }
            
            return result_costs;
        },
        cost_location_partner_report_sum: function()
        {
            var _sum = {title:'UKUPNO',   cost:0,cost_notpayed:0,cost_advance:0};
            for (var item_i in this.cost_location_report)
            {
                var element = this.cost_location_report[item_i];
                _sum.cost += element.cost;
                _sum.cost_notpayed += element.cost_notpayed;
                _sum.cost_advance += element.cost_advance;
            }
            return _sum;
        }
    },
    data: function () {
        return {
            report_list_uniq_key: sima.uniqid()+'accounting-cost_location_tab-cost',
            report_partner_list_uniq_key: sima.uniqid()+'accounting-cost_location_tab-cost'
        };
    },
    mounted: function() {
        var _this = this;
        $(this.$el).on('destroyed', function(){
            _this.$destroy();
        });
    },
    methods: {
        preview_specification: function(filter_type, filters){

            if (typeof filters.report_type === 'undefined')
            {
                filters.report_type = null;
            }
            var partner_filter = (typeof filters.partner_id === 'undefined')?
                {}:
                {
                    ids: [filters.partner_id]
                };
            
            
            sima.accounting.openReportSpecification({
                action: 'accounting/report/specification',
                action_data: {
                    model_filter: {
                        has_value_on: [filter_type],
                        cost_location: {
                            ids: [this.model.id]
                        },
                        partner: partner_filter,
                        report_type: filters.report_type
//                        month: {ids: this.can_select_months ? this.selected_months : [] },
//                        year: {ids: this.selected_years}
                    },
                    report_type: 'total_r'
                },
                show_values: [filter_type]
            });
        }
    }
});