<script type="text/x-template" id="accounting-account_document_view-account_order_span">
    
<div class="accounting-account_document_view-account_order_span">
    
    <div v-bind:class="regular_account_order_classes">
        Nalog za knjiženje 
            <sima-model-display-html :model="this.model.account_order"></sima-model-display-html>
            ( {{this.model.account_order.date}} )
            {{this.isBooked?'je potvrđen':'nije potvrđen'}} :
        <sima-button v-bind:class="button_class" @click="confirmAccountOrder(false)">{{confirmOrderButtonTitle}}</sima-button>
        <sima-button v-bind:class="button_class" v-if="!isBooked && !isAccountOrder" @click="changeAccountOrder(false)">Prebaci na drugi nalog</sima-button>
        <sima-button v-bind:class="button_class" v-if="!isBooked && !isAccountOrder" @click="removeFromAccountOrder(false)">Skini sa naloga</sima-button>
    </div>
    

    <template v-if="isCanceled">
        <br />
        <div v-bind:class="canceled_account_order_classes">
            Nalog za storniranje knjiženja 
                <sima-model-display-html :model="this.model.canceled_account_order"></sima-model-display-html>
                ( {{this.model.canceled_account_order.date}} )
                {{this.isCancelBooked?'je potvrđen':'nije potvrđen'}} :
            <sima-button v-bind:class="button_class" @click="confirmAccountOrder(true)">{{confirmCancelButtonTitle}}</sima-button>
            <sima-button v-bind:class="button_class" v-if="!isCancelBooked && !isAccountOrder" @click="changeAccountOrder(true)">Prebaci na drugi nalog</sima-button>
            <sima-button v-bind:class="button_class" v-if="!isCancelBooked && !isAccountOrder" @click="removeFromAccountOrder(true)">Skini sa naloga</sima-button>
        </div>
    </template>
    <template v-else>
            <sima-button v-bind:class="button_class" @click="changeAccountOrder(true)">Storniraj</sima-button>
    </template>
        

</div>
    
</script>