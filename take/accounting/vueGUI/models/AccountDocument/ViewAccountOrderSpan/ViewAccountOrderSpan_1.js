/* global Vue, sima */

Vue.component('accounting-account_document_view-account_order_span',{
    template: '#accounting-account_document_view-account_order_span',
    props:  {
        model: {
            validator: sima.vue.ProxyValidator
        }
    },
    computed: {
        dataPresent: function(){
            this.model.account_order;
            this.model.canceled_account_order;
            if (       this.model.shotStatus(         'account_order') !== 'INIT' 
                    && this.model.shotStatus('canceled_account_order') !== 'INIT' )
            {
                this.model.account_order.booked;
                this.model.account_order.year;
                var _canceled_data_ready = true;
                if (this.model.canceled_account_order !== '')
                {
                    this.model.canceled_account_order.booked;
                    this.model.canceled_account_order.year;
                    _canceled_data_ready = this.model.canceled_account_order.shotStatus('year') !== 'INIT'
                                        && this.model.canceled_account_order.shotStatus('booked') !== 'INIT';
                }
                return  this.model.account_order.shotStatus('year')   !== 'INIT'
                     && this.model.account_order.shotStatus('booked') !== 'INIT'
                     && _canceled_data_ready;
            }
            
            return false;
        },
        dataTrue: function(){
           
            this.model.account_order;
            this.model.canceled_account_order;
            if (       this.model.shotStatus(         'account_order') === 'OK' 
                    && this.model.shotStatus('canceled_account_order') === 'OK' )
            {
                this.model.account_order.booked;
                this.model.account_order.year;
                var _canceled_data_ready = true;
                if (this.model.canceled_account_order !== '')
                {
                    this.model.canceled_account_order.booked;
                    this.model.canceled_account_order.year;
                    _canceled_data_ready = this.model.canceled_account_order.shotStatus('year') === 'OK'
                                        && this.model.canceled_account_order.shotStatus('booked') === 'OK';
                }
                return  this.model.account_order.shotStatus('year')   === 'OK'
                     && this.model.account_order.shotStatus('booked') === 'OK'
                     && _canceled_data_ready;
            }
            
            return false;
        },
        button_class: function(){
            return {_disabled:!this.dataTrue};
        },
        isBooked: function(){
            return this.dataPresent && this.model.account_order.booked.value;
        },
        isCancelBooked: function(){
            return this.dataPresent && this.model.canceled_account_order.booked.value;
        },
        isAccountOrder: function(){
            return this.dataPresent && 
                    (
                        this.model.account_order.id === this.model.id
                        ||
                        this.model.canceled_account_order.id === this.model.id
                    );
        },
        isCanceled: function(){
            return this.dataPresent && this.model.canceled_account_order !== '';
        },
        confirmOrderButtonTitle: function() {
            return this.isBooked?'Poništi potvrdu':'Potvrdi';
        },
        confirmCancelButtonTitle: function() {
            return this.model.canceled_account_order.booked.value?'Poništi potvrdu':'Potvrdi';
        },
        regular_account_order_classes: function(){
            return {_confirmed: this.dataPresent && this.model.account_order.booked.value};
        },
        canceled_account_order_classes: function(){
            return {_confirmed: this.dataPresent && this.model.canceled_account_order.booked.value};
        }
        
    },
    data: function () {
        return {
//            report_list_uniq_key: sima.uniqid()+'accounting-cost_location_tab-cost',
//            report_partner_list_uniq_key: sima.uniqid()+'accounting-cost_location_tab-cost'
        };
    },
    mounted: function() {
        var _this = this;
        $(this.$el).on('destroyed', function(){
            _this.$destroy();
        });
    },
    methods: {
        confirmAccountOrder: function(canceled){
            var _account_order = canceled?
                this.model.canceled_account_order:
                this.model.account_order;
            var _new_value = !_account_order.booked.value;

            //WAITING_FOR_DIRTY nije realan ovde jer je potvrda prespora i onda se dugo ceka DIRTY
            //samim ti je i failure functionzakomentarisan jer nije potreban flush
//            _account_order.booked.value = _new_value;
//            _account_order.booked = _account_order.booked; //potreban red da bi se pokrenuo WAITING_FOR_DIRTY jer je booked zapravo status
            
            sima.ajax.get('accounting/books/setAccountOrderConfirmed',{
                async: true, 
                get_params:{
                    account_order_id : _account_order.id,
                    set_confirmed: _new_value
                }//,
//                failure_function: function(response){
//                    sima.model.updateViews(response.updateModelViews,{async:'true'});
//                    sima.vue.shot.commit('flush_wait_for_dirty');
//                    sima.dialog.openWarn(response.message);
//                }
            });
            
        },
        changeAccountOrder: function(canceled){
            var _account_document_id = this.model.id;
            sima.model.choose('AccountOrder',function(obj){
                //ova promenljiva nije potrebna zbog fukcionisanja koda, ali jeste zbog razumevanja koda
                //nadam se da ce js optimizator ovo da izbaci
                var _choosed_account_order_id = obj[0].id;
                
                sima.ajax.get('accounting/books/addAccountDocument',{
                    async: true,
                    get_params: {
                        account_order_id: _choosed_account_order_id,
                        document_id: _account_document_id,
                        canceled: canceled,
                        forced_change: true
                    },
                });
            },{
                view: 'guiTable',
                select_filter: {
                    year: {
                        ids: this.model.account_order.year.id
                    }
                }
            });
        },
        removeFromAccountOrder: function(canceled){
            var _account_document_id = this.model.id;
            sima.ajax.get('accounting/books/removeAccountDocument',{
                async: true,
                get_params: {
                    account_document_id: _account_document_id,
                    canceled: canceled
                }
            });
        }
    }
});