/* global Vue, sima */

Vue.component('accounting-partner_tab-finance',{
    props:  {
        model: {
            validator: sima.vue.ProxyValidator
        }
    },
    computed: {
        accounts_in_selected_year: function() {
            return sima.vue.shot.getters.model_list(this, 'Account',{
                model_filter: {
                    scopes: {
                        withDCCurrents: [this.selected_year]
                    },
                    model_name: 'Partner',
                    model_id: this.model.id
                }
            }, {
                withDCCurrents: [this.selected_year]
            });
        },
        years: function(){
            var years = sima.vue.shot.getters.model_list(this, 'AccountingYear',{
                model_filter: {
                    scopes: ['byYear']
                }
            });
            if (this.selected_year === -1 && years.length > 0)
            {
                this.selected_year = years[0].id;
            }
            
            return years;
        }
    },
    data: function () {
        return {
            selected_year: -1,
            accounts_in_years_local: {}
        };
    },
    mounted: function() {
        var _this = this;
        $(this.$el).on('destroyed', function(){
            _this.$destroy();
        });
    },
////    mounted: function(){
////        console.log('TRIGGER_tab_bill_items_mounted');
////        this.$nextTick(function () {
////            sima.layout.allignObject($(this.$el), 'TRIGGER_tab_bill_items_mounted');
////        });
////    },
    updated: function () {
//        console.log('TRIGGER_tab_bill_items_updated');
        this.$nextTick(function () {
            sima.layout.allignObject($(this.$el).parent(), 'TRIGGER_tab_finance_updated');
        });
//        this.selected_cost_location_id = null;
//        this.selected_bill_items = [];
        
    },
    methods: {
        select_year: function(year)
        {
            this.selected_year = year;
        }
    },
    template: '#accounting-partner_tab-finance'
});
