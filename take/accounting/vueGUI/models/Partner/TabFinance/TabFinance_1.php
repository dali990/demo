<?php $uniq = SIMAHtml::uniqid()?>


<script type="text/x-template" id="accounting-partner_tab-finance">

    <div class="sima-layout-fixed-panel accounting-partner_tab-finance">
        <h3>
            Kartice u godini: 
            <span v-for="year in this.years" 
                class="accounting-partner_tab-finance-year" v-bind:class="{'_pressed':selected_year == year.id}"
                @click="select_year(year.id)"
                >
                {{year.year}}
            </span>
        
        </h3>
        <table>
            <tr>
                <th><?=Account::model()->getAttributelabel('year')?></th>
                <th><?=Account::model()->getAttributelabel('saldo')?></th>
                <th><?=AccountGUI::modelLabel()?></th>
            </tr>
            

            <tr v-for="account in this.accounts_in_selected_year">
                <td style="text-align: right;padding-right: 20px;">
                    {{account.year.year}}
                </td>
                <td style="text-align: right;padding-right: 20px;">
                    {{account.saldo | formatNumber}}
                </td>
                <td>
                    <span v-html="account.DisplayHTML"></span>
                </td>
            </tr>
            
            
        </table>
    </div>
</script>
