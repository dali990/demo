<?php 

class MainTabsSetting extends SIMAActiveRecord
{
    public $apply_sync_with_new_main_menu_favorite_items = true;
	
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
 
    public function tableName()
    {
        return 'public.main_tabs_settings';
    }
    
    
    public function relations($child_relations = [])
    {
    	return array(
    		'user'=>array(self::BELONGS_TO, 'User', 'user_id'),
    	);
    }
    
    public function rules()
    {
        return array(
            array('user_id, main_tabs', 'required'),
            array('user_id','default',
                            'value'=>null,
                            'setOnEmpty'=>true,'on'=>array('insert','update'))
// 			array('partner_id, street, number, city_id', 'safe'),
        );
    }

// 	public function attributeLabels()
// 	{
// 		return array(
// 			'street'=>'Ulica',
// 			'number'=>'Broj',
// 			'city_id'=>'Grad',
// 		);
// 	}
	
// 	public function getCondition($condition,$alias,$param)
// 	{
// 		$condition->condition = $alias.'."street" ~* '.$param.' or '.$alias.'."number" ~* '.$param;
		
// 	}

    public function afterSave() 
    {
        parent::afterSave();
        
        if ($this->apply_sync_with_new_main_menu_favorite_items)
        {
            $this->syncWithNewMainMenuFavoriteItems();
        }
    }
    
    public function afterDelete() 
    {
        parent::afterDelete();
        
        if ($this->apply_sync_with_new_main_menu_favorite_items)
        {
            $this->syncWithNewMainMenuFavoriteItems(true);
        }
    }
    
    public function syncWithNewMainMenuFavoriteItems($is_delete = false)
    {
        $transaction = Yii::app()->db->beginTransaction();
        
        try
        {
            $main_tabs = CJSON::decode($this->main_tabs);
            
            $default_group = MainMenuFavoriteGroup::getDefaultGroupForUser($this->user);

            $applied_actions = [];
            $group_to_item_ids = [];
            $prev_item_id = null;
            foreach ($main_tabs as $main_tab)
            {
                $action = [];
                if (!empty($main_tab['action']))
                {
                    $action[0] = $main_tab['action'];
                }
                if (!empty($main_tab['id']))
                {
                    $action[1] = $main_tab['id'];
                }
                
                if (empty($action))
                {
                    continue;
                }

                $action_string = MainMenuFavoriteItem::arrayToJsonString($action);
                $item_to_group = MainMenuFavoriteItemToGroup::model()->find([
                    'model_filter' => [
                        'group' => [
                            'ids' => $default_group->id
                        ],
                        'item' => [
                            'user' => [
                                'ids' => $this->user_id
                            ],
                            'action' => $action_string
                        ]
                    ]
                ]);
                
                if (!empty($item_to_group) && in_array($action_string, $applied_actions))
                {
                    continue;
                }
                
                if (!empty($item_to_group))
                {
                    if (intval($item_to_group->prev_id) !== intval($prev_item_id))
                    {
                        $item_to_group->prev_id = $prev_item_id;
                        $item_to_group->apply_sync_with_old_main_menu_favorite_items = false;
                        $item_to_group->save();
                    }
                    else if ($is_delete)
                    {
                        $item_to_group->apply_sync_with_old_main_menu_favorite_items = false;
                        $item_to_group->delete();
                    }
                }
                else if (empty($item_to_group))
                {
                    $item = MainMenuFavoriteItem::model()->findByAttributes([
                        'user_id' => $this->user_id,
                        'action' => $action_string
                    ]);
                    if (empty($item))
                    {
                        $item = new MainMenuFavoriteItem();
                        $item->user_id = $this->user_id;
                        $item->action = $action_string;
                        $item->label = $main_tab['title'];
                        $item->save();
                    }
                    $item_to_group = new MainMenuFavoriteItemToGroup();
                    $item_to_group->item_id = $item->id;
                    $item_to_group->group_id = $default_group->id;
                    $item_to_group->prev_id = $prev_item_id;
                    $item_to_group->apply_sync_with_old_main_menu_favorite_items = true;
                    $item_to_group->save();
                }
                $prev_item_id = $item_to_group->id;
                array_push($group_to_item_ids, $item_to_group->id);
                array_push($applied_actions, $action_string);
            }

            if (!$is_delete && !empty($group_to_item_ids))
            {
                $group_to_items = MainMenuFavoriteItemToGroup::model()->findAll([
                    'model_filter' => [
                        'group' => [
                            'ids' => $default_group->id
                        ],
                        'filter_scopes' => [
                            'notWithIds' => [$group_to_item_ids]
                        ]
                    ]
                ]);
                foreach ($group_to_items as $_group_to_item)
                {
                    $_group_to_item->apply_sync_with_old_main_menu_favorite_items = false;
                    $_group_to_item->delete();
                }
            }
            
            Yii::app()->mainMenu->checkItemsIntegrityForUser($this->user);
            
            $transaction->commit();
        }
        catch(Exception $e)
        {
            $current_transaction = Yii::app()->db->getCurrentTransaction();
            if(!is_null($current_transaction))
            {
                $current_transaction->rollback();
            }
            throw $e;
        }
    }
}