<?php

class Contact extends SIMAActiveRecord
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'public.contacts';
    }

    public function moduleName()
    {
        return 'application';
    }

    public function __get($column)
    {
        switch ($column)
        {
            case 'DisplayName': {
                $display = $this->type->name.': '.$this->value;
                if(!empty($this->comment))
                {
                    $display .= ' - '.$this->comment;
                } 
                return $display;
            }
            case 'SearchName':      return $this->value;
            default: return parent::__get($column);
        }
    }
    
    public function relations($child_relations = [])
    {
        return array(
            'partner' => array(self::BELONGS_TO, 'Partner', 'partner_id'),
            'type' => array(self::BELONGS_TO, 'ContactType', 'type_id')
        );
    }

    public function rules()
    {
        return array(
            array('type_id, partner_id, value', 'required', 'on' => ['insert', 'update']),
            array('comment', 'safe'),
            array('value', 'valueCheck', 'on' => ['insert', 'update']),
            array('partner_id, type_id', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => array('insert', 'update'))
        );
    }
    
    public function valueCheck()
    {
        if(isset($this->value))
        {
            $value_len = strlen($this->value);
            if($value_len > 100)
            {
                $this->addError('value', Yii::t('Contact','ValueTooLong', array('{value_len}' => $value_len)));
            }
        }
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters': return [
                'type' => 'relation',
                'partner' => 'relation',
                'value' => 'text'               
            ];
            case 'textSearch' : return array(
                'value' => 'phone_number'
            );
            default: return parent::modelSettings($column);
        }
    }
    
    public function beforeDelete() 
    {
        if($this->id === $this->partner->main_contact_id)
        {
            $this->partner->main_contact_id = NULL;
            $this->partner->save();
        }
        return parent::beforeDelete();
    }
    
    public function afterValidateDependencyError(array $foreign_dependencies)
    {
        $allowed_tables = [
            Partner::model()->tableName().':main_contact_id'
        ];

        $parsed_foreign_dependencies = array_diff($foreign_dependencies, $allowed_tables);
        
        parent::afterValidateDependencyError($parsed_foreign_dependencies);
    }
}
