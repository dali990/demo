<?php

class Company extends Partner
{
    public $file_version_id;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'public.companies';
    }

    public function moduleName()
    {
        return 'application';
    }

    public function __get($column)
    {
        switch ($column)
        {
            case 'path2' : return 'addressbook/partner';
            case 'DisplayName': 
                return $this->name;
            case 'SearchName': return $this->name . (($this->comment != '') ? ' % ' . $this->comment : '');
            case 'base_company_phone_contact': return $this->bc->partner->MainOrAnyPhoneContact;
            case 'base_company_phone_contact_display': return $this->bc->partner->MainOrAnyPhoneContactDisplay;
            case 'base_company_city': {
                $city = $this->bc->partner->main_or_any_address->city;
                return $city;
            }
            default: return parent::__get($column);
        }
    }

    public function relations($child_relations = [])
    {
        $company_to_theme_table_name = CompanyToTheme::model()->tableName();
        return parent::relations(array_merge([
            'employees' => array(self::HAS_MANY, 'Person', 'company_id'),
            'image' => array(self::BELONGS_TO, 'File', 'image_id'),
            'partner' => array(self::BELONGS_TO, 'Partner', 'id'),
            'head_sector' => array(self::BELONGS_TO, 'CompanySector', 'head_sector_id'),
            'work_code' => array(self::BELONGS_TO, 'CompanyWorkCode', 'work_code_id'),
            'themes' => [self::MANY_MANY, 'Theme', "$company_to_theme_table_name(company_id, theme_id)"],
            'company_to_themes' => [self::HAS_MANY, 'CompanyToTheme', 'company_id'],
        ],$child_relations));
    }
    
    /*
     * return basic company object
     */
    public function getbc()
    {
        return Yii::app()->company;
    }
    
    public function getHeadSector()
    {
        if (!isset($this->head_sector))
        {
            $_hs = new CompanySector(); 
            $_hs->name = $this->DisplayName;
            $_hs->type = "Head";
            $_hs->code = "1.0";
            $_hs->setScenario('getHeadSector');
            $_hs->save();
            $_hs->refresh();

            $this->head_sector_id = $_hs->id;
            $this->head_sector = $_hs;
            $this->save();
        }
        return $this->head_sector;
    }
    
    /**
     * stvarni direktori, ne sefovi
     * directori su model Person
     */
    public function getDirectors()
    {
        $directors = [];
        
        $headSector = $this->getHeadSector();
        $director_work_positions = $headSector->director_work_positions;
        foreach($director_work_positions as $director_work_position)
        {
            $persons = $director_work_position->persons;
            foreach($persons as $person)
            {
                $directors[] = $person;
            }
        }
        
        return $directors;
    }
    
    public function getDirectorIds()
    {
        $director_ids = [];
        
        $directors = $this->getDirectors();
        foreach($directors as $director)
        {
            $director_ids[] = $director->id;
        }
        
        return $director_ids;
    }

    public function rules()
    {
        return array(
            array('name', 'required'),
            array('name, nbs_confirmed, full_name', 'safe'),
            array('inVat, comment, text, file_version_id', 'safe'),
            ['MB', 'safe'],
            array('PIB', 'numerical', 'integerOnly'=>true),
            array('PIB', 'length', 'min'=>9, 'max'=>9),
            ['PIB', 'unique'],
            [
                'PIB', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => ['insert', 'update']
            ],
            ['MB', 'unique'],
            ['MB', 'length', 'min'=>8, 'max'=>8],
            [
                'MB', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => ['insert', 'update']
            ],
            array('MB', 'numerical', 'integerOnly'=>true),
            array('image_id', 'safe'),
            array('image_id', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => array('insert', 'update')),
            ['nbs_confirmed', 'checkNBSConfirmed', 'on' => ['insert', 'update']]
        );
    }
    
    public function checkNBSConfirmed()
    {
        if (
                !$this->hasErrors() && 
                (
                    $this->columnChanged('PIB') || $this->columnChanged('MB') || !$this->nbs_confirmed
                )
            )
        {
            $this->nbs_confirmed = false;
            if (Yii::app()->NBSConnection->isConnected() === true)
            {
                $nbs_company_data = null;
                if(!empty($this->PIB))
                {
                    try
                    {
                        $nbs_company_data = Yii::app()->NBSConnection->GetCompanyInformationByPIB($this->PIB);
                    }
                    catch (Exception $e)
                    {
                        //Sasa A. - posto postoje neke firme koje imaju PIB ali se ne nalazi u spisku NBS-a, ne baca se greska ako ne postoji
//                        $this->addError('PIB', $e->getMessage());
                    }
                }
                else if(!empty($this->MB))
                {
                    try
                    {
                        $nbs_company_data = Yii::app()->NBSConnection->GetCompanyInformationByMB($this->MB);
                    }
                    catch (Exception $e)
                    {
//                        $this->addError('MB', $e->getMessage());
                    }
                }

                if (!empty($nbs_company_data))
                {
                    $nbs_pib = Company::getFullPIB($nbs_company_data['TaxIdentificationNumber']);
                    $nbs_mb = Company::getFullMB($nbs_company_data['NationalIdentificationNumber']);

                    if($this->PIB !== $nbs_pib)
                    {
                        $this->addError('PIB', Yii::t('Company', 'PIBDoesNotMatchNBS'));
                    }
                    else if ($this->MB !== $nbs_mb)
                    {
                        $this->addError('MB', Yii::t('Company', 'MBDoesNotMatchNBS'));
                    }
                    else
                    {
                        $this->nbs_confirmed = true;
                    }
                }
            }
        }
    }
    
    public static function getFullPIB($pib)
    {
        return str_pad($pib, 9, '0', STR_PAD_LEFT);
    }
    
    public static function getFullMB($mb)
    {
        return str_pad($mb, 8, '0', STR_PAD_LEFT);
    }

    public function __toString()
    {
        return $this->name;
    }

    public function BeforeSave()
    {
        if ($this->getIsNewRecord())
        {
            $partner = new Partner();
            $partner->company = true;
            $partner->save();
            $this->id = $partner->id;
        }
        
        if (!empty($this->file_version_id))
        {
            if (empty($this->image_id))
            {
                $file = new File();
                $file->responsible_id = Yii::app()->user->id;
                $file->name = 'Avatar '.$this->DisplayName;
                $file->save();
                $file->beforeChangeLock();
    //            $file->addTag('address_book'); //obavezan red zbog prava pristupa!!! (ne moze fajl da se uploaduje ako nema prava pristupa)
                EmployeeTQLLimit::addFileToPublicTag('address_book', $file->id);
                $file->responsible_id = null;
                $file->save();
                $file->afterChangeUnLock();

                $this->image_id = $file->id;
                $this->image = $file;
            }

            /** ubacivanje upload-a fajla */
            $this->image->appendUpload($this->file_version_id, true);
            
            $this->image->refresh();
            
            if($this->image->last_version->type !== FileVersion::$TYPE_SHELL)
            {
                $temp_image = TemporaryFile::CreateFromFile($this->image);
                $width = Yii::app()->params['avatar_width'];
                $resized_temp_file = $temp_image->resizeImage($width);
                
                $this->image->appendTempFile($resized_temp_file->filename, $this->image->name);
            }
        }

        return parent::BeforeSave();
    }

    public function afterSave()
    {
        parent::afterSave();
        
        if(!$this->isNewRecord && $this->name !== $this->__old['name'])
        {
            $head_sector = CompanySector::model()->findByPk($this->head_sector_id);
            if(!is_null($head_sector) && $head_sector->name !== $this->DisplayName)
            {
                $head_sector->name = $this->DisplayName;
//                $head_sector->save(false);
                $head_sector->update(['name']);
            }
        }
    }
    
    //zabranjeno je brisanje jer je moguce da prodje brisanje kompanije, a kasnije pukne brisanje partnera
    public function delete($check_result=true, $throwExceptionOnFail = true)
    {
        Yii::app()->errorReport->createAutoClientBafRequest("Pokušaj brisanja kompanije. Stack trace: ".SIMAMisc::stackTrace());

        throw new SIMAWarnException(Yii::t('Company', 'CompanyDeleteForbiden'));
    }

    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters' : return array_merge(parent::modelSettings('filters'), array(
                    'PIB' => 'integer',
                    'name' => 'text',
                    'full_name' => 'text',
                    'comment' => 'text',
                    'PIB' => 'integer',
                    'MB' => 'integer',
                    'contacts' => 'relation',
                    'partner_to_address' => 'relation',
                    'bank' => 'relation',
                    'head_sector' => 'relation',
                    'company_to_themes' => 'relation'
                ));
            case 'textSearch' : return array(
                    'name' => 'text',
                    'full_name' => 'text',
                    'comment' => 'text',
                    'PIB' => 'integer',
                    'MB' => 'integer',
                    'contacts' => 'relation',
                    'partner_to_address' => 'relation',
                    'phone_numbers' => 'relation',
                    'email_addresses' => 'relation'
                    //MilosS(18.20.2017): Kada u imenik kucam "komenrcijalna", izadju mi sve firme koje imaju ziro racun u komercijalnoj banci
                    //dok se ne sredi bodovanje prilikom pretrage, ovo ne moze da se pusti
//                    'bank_accounts' => 'relation'
                );
            case 'form_list_relations' : return array('bank_accounts', 'addresses', 'contacts', 'partner_lists', 'phone_numbers', 'email_addresses');
            case 'update_relations': return [
                'partner'
            ];
            case 'options': return array('open','form');
            default: return parent::modelSettings($column);
        }
    }

    public function scopes()
    {
        $alias = $this->getTableAlias();
        return array(
            'byName' => array('order' => "$alias.name, $alias.id"),
        );
    }
    
    public function withCompanyToTheme($theme_id)
    {
        $alias = $this->getTableAlias();
        $uniq = SIMAHtml::uniqid();
        $company_to_theme_table_name = CompanyToTheme::model()->tableName();
        
        $this->getDbCriteria()->mergeWith([
            'condition'=>"EXISTS (SELECT 1 FROM $company_to_theme_table_name as ctt$uniq WHERE ctt$uniq.company_id = $alias.id and ctt$uniq.theme_id = :theme_id$uniq)",
            'params' => [
                ":theme_id$uniq" => $theme_id,
            ]
        ]);

        return $this;
    }
    
    public function getCompanySectors()
    {
        $result = CompanySector::model()->withHeadSector()->findAll('recursive_company_sectors.head_sector_id=' . $this->head_sector_id);
        return $result;
    }

    public function getPersonsWithoutWorkPosition()
    {
        $work_position_table_name = WorkPosition::model()->tableName();
        $person_to_work_position_table_name = PersonToWorkPosition::model()->tableName();
        return Person::model()->findAllBySql(
            "select * from persons where company_id=:company_id and id not in 
                (select person_id from $person_to_work_position_table_name where work_position_id in 	
                        (select wp.id from $work_position_table_name wp left join hr.recursive_company_sectors rcs on wp.company_sector_id=rcs.id where head_sector_id=:head_sector_id))",
            array(
                ':company_id' => $this->id,
                ':head_sector_id' => $this->head_sector_id
            )
        );

    }
    
//        public function mainAddress()
//        {
//            $partner=Partner::model()->findByPk($this->id);
//            if(isset($partner->main_address))
//            {
//             return $partner->main_address->DisplayName.', '.$partner->main_address->city->postal_code.' '.$partner->main_address->city;
//            }
//        }

    public function getDirectorsIds($only_main_director=false)
    {
        $directors_ids = [];
        $head_sector = $this->getHeadSector();
        foreach ($head_sector->director_work_positions as $work_position)
        {
            foreach ($work_position->persons_positions as $person_to_work_position) 
            {
                if (!$only_main_director || !$person_to_work_position->fictive)
                {
                    array_push($directors_ids, $person_to_work_position->person_id);
                }
            }
        }
        
        return $directors_ids;
    }
}