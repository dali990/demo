<?php

class Partner extends SIMAActiveRecord 
{
    public $main_bank_account_in_bank=null;
    public $depend_data = null;
    
    public $income;
    public $income_notpayed;
    public $income_advance;
    public $cost;
    public $cost_notpayed;
    public $cost_advance;
    
    
    public $cost_services;
    public $cost_material;
    
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'public.partners';
    }
    
    public function moduleName()
    {
        return 'application';
    }

    public function __get($column) {
        switch ($column) {
            case 'path2' : return 'addressbook/partner';
            case 'DisplayName': return $this->getDisplayName();
            case 'SearchName': return $this->getSearchName();
            case 'MainAddress': return $this->getmain_address();
            case 'MainPhoneNumber': return $this->getmain_phone_number(); //vraca samo main telefon iz nove tabele PhoneNumber
            case 'MainPhoneNumberDisplay': 
                $main_phone_number = $this->getmain_phone_number();
                return !is_null($main_phone_number) ? $main_phone_number : '';
            case 'MainOrAnyPhoneContact': return $this->getmain_or_any_phone_contact(); //vraca main ili bilo koji telefon iz nove tabele PhoneNumber ili iz stare Contact
            case 'MainOrAnyPhoneContactDisplay': return $this->getmain_or_any_phone_contact_display();
            case 'MainEmailAddress': return $this->getmain_email_address();
            case 'MainEmailAddressDisplay': 
                $main_email_address = $this->getmain_email_address();
                return !is_null($main_email_address) ? $main_email_address : '';
            case 'MainBankAccount' : return $this->getmain_bank_account();
            case 'MainContact' : return $this->getmain_contact();
            case 'DisplayMainAddress':
                $result = ' - ';
                
                $mainAddress = $this->MainAddress;
                if(!empty($mainAddress))
                {
                    $result = $mainAddress->DisplayName;
                }
                
                return $result;
            case 'mb_jmbg_template':
                return $this->getmb_jmbg_template();
            case 'cost': return $this->cost_material + $this->cost_services;
            default: return parent::__get($column);
        }
    }
    
    public function relations($child_relations = []) 
    {
        $partners_to_partner_lists_table_name = PartnerToPartnerList::model()->tableName();

        $_result = parent::relations(array_merge(array(
            'bank_accounts' => array(self::HAS_MANY, 'BankAccount', 'partner_id'),
            //'addresses' => array(self::HAS_MANY, 'Address', 'partner_id'),
            'addresses' => array(self::MANY_MANY, 'Address', 'public.partners_to_addresses(partner_id, address_id)'),            
//            'main_address'=>array(self::BELONGS_TO, 'Address','main_address_id'),
            'partner_to_address' => array(self::HAS_MANY, 'PartnerToAddress', 'partner_id'),
            'contacts' => array(self::HAS_MANY, 'Contact', 'partner_id'),
//            'emails' => array(self::HAS_MANY, 'Contact', 'partner_id', 'condition' => 'type_id=3'),
            //MilosS: naredne dve relacije se koriste u AddressBook kontroleru za roundcube - actionListEmails
            'accounts' => array(self::HAS_MANY, 'BankAccount', 'partner_id'),
            'payments' => array(self::HAS_MANY, 'Payment', 'partner_id'),
            'bills' => array(self::HAS_MANY, 'Bill', 'partner_id'),
            'bids' => array(self::HAS_MANY, 'Bid', 'partner_id'),
            'filing_book' => array(self::HAS_MANY, 'Filing', 'partner_id'),
            'contracts' => array(self::HAS_MANY, 'Contract', 'partner_id'),
            'bill_of_exchange' => array(self::HAS_MANY, 'BillOfExchange', 'partner_id'),
            'company_search' => array(self::HAS_ONE, 'Company', 'id'),
            'person_search' => array(self::HAS_ONE, 'Person', 'id'),
            'person' => array(self::HAS_ONE, 'Person', 'id'),
            'company_model' => array(self::HAS_ONE, 'Company', 'id'),
            'company' => array(self::HAS_ONE, 'Company', 'id'), /** @todo relacija company ima isto ime kao polje u bazi i ne vidi se* */
            'company_model' => array(self::HAS_ONE, 'Company', 'id'),
//            'comment_thread'=>array(self::BELONGS_TO, 'CommentThread', 'comment_thread_id'),
            
//            'bills_count' => array(self::STAT, 'Bill', 'partner_id','select'=>'count(*)'),
//            'filing_book_count' => array(self::STAT, 'Filing', 'partner_id','select'=>'count(*)'),
//            'contracts_count' => array(self::STAT, 'Contract', 'partner_id','select'=>'count(*)'),
            'client_baf_requests'=>array(self::HAS_MANY, 'ClientBafRequest', 'requested_by_id'),
            'client_baf_requests_count'=>array(self::STAT, 'ClientBafRequest', 'requested_by_id', 'select'=>'count(*)'),
            'has_unsend_client_baf_requests' => array(self::STAT, 'ClientBafRequest', 'requested_by_id', 'condition'=>'server_baf_request_id is null', 'select' => 'count(*) > 0'),
            'partner_lists' => [self::MANY_MANY, 'PartnerList', "$partners_to_partner_lists_table_name(partner_id, partner_list_id)"],
            'partner_to_partner_lists' => [self::HAS_MANY, 'PartnerToPartnerList', 'partner_id'],
            'phone_numbers' => array(self::HAS_MANY, 'PhoneNumber', 'partner_id'),
            'phone_numbers_cnt' => [self::STAT, 'PhoneNumber', 'partner_id', 'select' => 'count(*)']
        ), $child_relations));
        
        return $_result;
    }

    public function modelSettings($column) 
    {
        switch ($column)
        {
            case 'filters' : return array_merge(parent::modelSettings($column),array(
                'main_bank_account_in_bank' => array('dropdown', 'func' => 'filter_main_bank_account_in_bank'),
                'main_bank_account_id' => ['dropdown'],
                'person' => 'relation',
                'bills_in' => 'relation',
                'bills_out' => 'relation',
                'display_name' => 'text',
                'partner_accounts' => 'relation',
                'partner_to_partner_lists' => 'relation',
                'phone_numbers' => 'relation',
            ));
            case 'textSearch' : return array(
                'company_search' => 'relation',
                'person_search' => 'relation',
            );
            case 'update_relations': return [
                'person', 'company'
            ];
            case 'search_model' : return [
                'action' => 'simaAddressbook'
            ];
            case 'options': return [
                'delete'
            ];
            case 'multiselect_options':return [
                'delete'
            ];
            default: return parent::modelSettings($column);
        }
    }
    
    public function rules()
    {
        return [
            ['main_email_address_id, main_phone_number_id', 'safe']
        ];
    }

    public function scopes()
    {
        $alias = $this->getTableAlias();
        $person_table_name = Person::model()->tableName();
        $company_table_name = Company::model()->tableName();
        $users_table = User::model()->tableName();
        return array(
            'byName' => array('order' => "$alias.display_name ASC"),
            'not_person' => [
                'condition' => "NOT EXISTS (SELECT 1 FROM $person_table_name p WHERE p.id=$alias.id LIMIT 1)"
            ],
            'not_company' => [
                'condition' => "NOT EXISTS (SELECT 1 FROM $company_table_name c WHERE c.id=$alias.id LIMIT 1)"
            ],
            'not_user' => [
                'condition' => "NOT EXISTS (SELECT 1 FROM $users_table u where u.id=$alias.id LIMIT 1)"
            ],
            'not_active_user' => [
                'condition' => "NOT EXISTS (SELECT 1 FROM $users_table u where u.id=$alias.id AND u.active=TRUE LIMIT 1)"
            ]
        );
    }
    
    public function filter_main_bank_account_in_bank($condition, $alias)
    {
        if(isset($this->main_bank_account_in_bank))
        {
            $uniq = SIMAHtml::uniqid();
            $bank_account_table = BankAccount::model()->tableName();
            $bankId = $this->main_bank_account_in_bank;
            
            $temp_condition = new SIMADbCriteria();
            $temp_condition->condition = "exists (SELECT 1 FROM $bank_account_table ba$uniq "
                                                . "WHERE ba$uniq.id=(select main_bank_account_id from partners p$uniq where p$uniq.id=$alias.id) "
                                                . "AND ba$uniq.bank_id=:bankId)";
            $temp_condition->params = array(':bankId' => $bankId);

            $condition->mergeWith($temp_condition, true);
        }
    }

    public function getDisplayName() {
        if ($this->company)
        {
            $partner = Company::model()->findByPk($this->id);
        }
        else
        {
            $partner = Person::model()->findByPk($this->id);
        }
        
        if (isset($partner))
        {
            return $partner->DisplayName;
        }
        elseif (!empty($this->id))
        {
            Yii::app()->errorReport->createAutoClientBafRequest("Partner sa ID: {$this->id} nije ni osoba ni kompanija.");
            return '!!!'.$this->id.'!!!';
        }
        else{
            return '';
        }
    }

    public function getSearchName() {
        if ($this->company)
        {
            $partner = Company::model()->findByPk($this->id);
        }
        else
        {
            $partner = Person::model()->findByPk($this->id);
        }
        return ($partner == null) ? "partnera nema ni u osobama ni u firmama, obavestiti milosa (id={$this->id})" : $partner->SearchName;
    }

    public function getComment() {
        if ($this->company)
        {
            $partner = Company::model()->findByPk($this->id);
        }
        else
        {
            $partner = Person::model()->findByPk($this->id);
        }

        return ($partner !== null) ? $partner->comment : 'partneer nije ni osoba ni kompanija!';
    }

    public function getpib_jmbg() {
        if ($this->company) {
            $partner = Company::model()->findByPk($this->id);
            return $partner->PIB;
        } else {
            $partner = Person::model()->findByPk($this->id);
            return $partner->JMBG;
        }
        return 'nema partnera ili fizocko lice!';
    }

    public function getmb_jmbg() 
    {
        if ($this->company) {
            $partner = Company::model()->findByPk($this->id);
            return $partner->MB;
        } else {
            $partner = Person::model()->findByPk($this->id);
            return $partner->JMBG;
        }
        return 'nema partnera ili fizocko lice!';
    }
    
    public function getmb_jmbg_template() 
    {
        if ($this->company) {
            $partner = Company::model()->findByPk($this->id);
            return 'MB: '.$partner->MB;
        } else {
            $partner = Person::model()->findByPk($this->id);
            return 'JMBG: '.$partner->JMBG;
        }
        return 'nema partnera ili fizocko lice!';
    }

    public function getwork_code() 
    {
        if ($this->company) {
            $partner = Company::model()->findByPk($this->id);
            return $partner->work_code;
        } else {
            return null;
        }
        return 'nema partnera ili fizocko lice!';
    }

    public function getinVat() {
        if ($this->company) {
            $partner = Company::model()->findByPk($this->id);
            return $partner->inVat;
        }
        else
        {
            return false;
        }
    }

    public function getModelType() {
        if ($this->company)
        {
            return Company::model()->findByPk($this->id);
        }
        else 
        {
            return Person::model()->findByPk($this->id);
        }
    }

    public function getModelTypeName() {
        if ($this->company)
        {
            return 'Company';
        }
        else
        {
            return 'Person';
        }
    }
    
    public function getmain_address() 
    {
        $partner = Partner::model()->findByPk($this->id);
        return Address::model()->findByPk($partner->main_address_id); 
    }
    
    public function getmain_or_any_address() {         
         $address = null;
         
         if(isset($this->main_address_id))
         {
             $address = Address::model()->findByPk($this->main_address_id);
         }
         else if(count($this->addresses) > 0)
         {
             $address = $this->addresses[0];
         }
         
         return $address;
    }
    
    public function getmain_bank_account() {
         $partner = Partner::model()->findByPk($this->id);
         return BankAccount::model()->findByPk($partner->main_bank_account_id); 
    }
    
    public function getmain_or_any_bank_account() {
        $partner = Partner::model()->findByPk($this->id);

        $bank_account = null;
        
        if(isset($partner->main_bank_account_id))
        {
            $bank_account = BankAccount::model()->findByPk($partner->main_bank_account_id);
        }
        
        if(!isset($bank_account) && count($partner->bank_accounts) > 0)
        {
            $bank_account = $this->bank_accounts[0];
        }

        return $bank_account; 
    }
    
    public function getmain_contact() {
        $exception_for_stacktrace = new Exception();
        $stack_trace = $exception_for_stacktrace->getTraceAsString();
        Yii::app()->errorReport->createAutoClientBafRequest('treba izbaciti upotrebu - stacktrace: '.$stack_trace);
        $partner = Partner::model()->findByPk($this->id);
        return Contact::model()->findByPk($partner->main_contact_id); 
    }
    
    public function getmain_or_any_email() 
    {
        $partner = Partner::model()->findByPkWithCheck($this->id);
        
        $email = null;
        
        if (isset($partner->main_email_address_id))
        {
            $email = EmailAddress::model()->findByPkWithCheck($partner->main_email_address_id);
        }
        
        if(empty($email) && $partner->email_addresses_cnt > 0)
        {
            $email = $partner->email_addresses[0];
        }
      
        return $email;
    }
    
    public function getmain_or_any_email_display()
    {
        $partner = Partner::model()->findByPkWithCheck($this->id);
        
        $main_or_any_email = $partner->getmain_or_any_email();
        $main_or_any_email_display = '';
        if (!empty($main_or_any_email))
        {
            $main_or_any_email_display = $main_or_any_email->DisplayName;
        }
        
        return $main_or_any_email_display;
    }
    
    public function getmain_or_any_phone_contact()
    {
        $partner = Partner::model()->findByPkWithCheck($this->id);     
        
        $phone_contact = null;
        
        if (isset($partner->main_phone_number_id))
        {
            $phone_contact = PhoneNumber::model()->findByPkWithCheck($partner->main_phone_number_id);
        }
       
        if(empty($phone_contact) && $partner->phone_numbers_cnt > 0)
        {
            $phone_contact = $partner->phone_numbers[0];
        }
        
        return $phone_contact;
    }
    
    public function getmain_or_any_phone_contact_display()
    {
        $partner = Partner::model()->findByPkWithCheck($this->id);
        
        $phone_contact = $partner->getmain_or_any_phone_contact();
        $phone_contact_display = '';
        if (!empty($phone_contact))
        {
            $phone_contact_display = $phone_contact->DisplayName;
        }
        
        return $phone_contact_display;
    }
    
    public function getmain_address_display() {
        $address = $this->getmain_address();
         if ($address !== null)
         {
            return $address->DisplayName;
         }
         else
         {
            return '';
         }
    }
    
    public function getcomment_thread() {
         $partner = Partner::model()->findByPk($this->id);
         return CommentThread::model()->findByPk($partner->comment_thread_id); 
    }
    
    public function getDisplayMainAddressLong()
    {
        if (isset($this->main_address))
        {
            return $this->main_address->DisplayNameLong;
        }
        else if (count($this->addresses)>0)
        {
            return $this->addresses[0]->DisplayNameLong;
        }
        else
        {
            return '';
        }
    }
    
    public function getmain_phone_number() 
    {
         $partner = Partner::model()->findByPkWithCheck($this->id);
         return PhoneNumber::model()->findByPk($partner->main_phone_number_id); 
    }
    
    public function getmain_email_address() 
    {
         $partner = Partner::model()->findByPkWithCheck($this->id);
         return EmailAddress::model()->findByPk($partner->main_email_address_id); 
    }
    
    /**
     * Da li je partner registrovan u srbiji. Prepoznaje na osnovu konfiguracionog parametra koja je domaca drzava i main adrese
     */
    public function isDomestic()
    {
        $main_address = $this->main_or_any_address;
        $_country_id = Yii::app()->configManager->get('base.system_country_id');
        if (!is_null($main_address))
        {
//            if ($main_address->city->country_id!=$_country_id)
//            {
//                $address_country_id = $main_address->city->country_id;
//            }
            return ($main_address->city->country_id==$_country_id); 
        }
        else
        {
            return true;
        }
        return true;
    }
    
    public function getall_apartments()
    {
        return array_merge($this->sn_apartments,$this->sn_garages,$this->sn_offices);
    }
    
    public function getPartnersListsAsString()
    {
        $partner_lists_names = [];
        
        foreach ($this->partner_lists as $partner_list)
        {
            array_push($partner_lists_names, strtolower($partner_list->DisplayName));
        }
        
        return count($partner_lists_names) > 0 ? '(' . implode(', ', $partner_lists_names) . ')' : '';
    }
    
    public function beforeDelete()
    {
        /**
         * u trigger-u u bazi ce se obrisati:
         * * kontakti
         * * veze ka adresama
         * * commentthread-ovi koji nemaju pravih komentara - nije implementirano
         */
        
        //prvo pokusamo da obrisemo sva konta vezana za partnera. Ako postoji bar jedno koje ne moze da se obrise, onda ne brisemo nijedno
        $this->deleteAllAccounts();

        //brisemo sve liste za partnera
        PartnerToPartnerList::model()->deleteAllByAttributes([
            'partner_id' => $this->id
        ]);
        
        return parent::beforeDelete();
    }
    
    private function deleteAllAccounts()
    {
        $can_not_be_deleted_accounts = [];
        foreach ($this->partner_accounts as $partner_account)
        {
            try 
            {
                $partner_account->setScenario('delete');
                if (!$partner_account->validate())
                {
                    array_push($can_not_be_deleted_accounts, $partner_account->DisplayName);
                }
            } 
            catch (Exception $ex) 
            {
                array_push($can_not_be_deleted_accounts, $partner_account->DisplayName);
            }
        }
        if (count($can_not_be_deleted_accounts) > 0)
        {
            throw new SIMAWarnException(
                Yii::t('Partner', 'PartnerCanNotBeDeletedBecauseIsUsedInNextAccounts') . '<br />' . implode('<br />', $can_not_be_deleted_accounts)
            );
        }
        else
        {
            foreach ($this->partner_accounts as $partner_account)
            {
                $partner_account->delete();
            }
        }
    }
    
    public function afterDelete()
    {
        parent::afterDelete();
        
        Yii::app()->warnManager->sendWarns('masterAdmin', 'PARTNERS_NOT_PERSON_NOR_COMPANY');   
    }
    
    public function getImageDownloadUrl()
    {
        $download_url = '';

        if(isset($this->image) && !$this->image->isDeleted() && isset($this->image->last_version_id) && $this->image->last_version->canDownload())
        {
            try
            {
                $download_url = $this->image->last_version->getDownloadLink();
            }
            catch(Exception $ex)
            {
                Yii::app()->errorReport->createAutoClientBafRequest($ex->getMessage());
            }
        }
        
        return $download_url;
    }
}