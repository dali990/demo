<?php

class BuildingStructureGUI extends SIMAActiveRecordGUI
{

    public function columnLabels()
    {
        return array(
            'name'=>'Naziv',
            'description' => 'Opis',
            'building_structure_type_id' => 'Tip objekta',
            'job_id' => 'Posao',
            'work_name' => 'Zadatak',
            'size'=>'Veličina objekta',
            'size_unit_id'=>'Jedinica mere',
            'address_id' => 'Adresa'
        )+parent::columnLabels();
    }
    
//    public function columnDisplays($column)
//    {
//        $owner = $this->owner;
//        switch ($column)
//        {
//            case 'address_id': return Yii::app()->controller->renderModelView($owner,'building_address_load');
//            default: return parent::columnDisplays($column);
//        }
//    }

    public function modelViews()
    {
        return array(
            'default',//=>array('style'=>'border: 1px solid;'),
//            'tabTemplates'=>array('with'=>array('templated_file'),'style'=>'border: 1px solid;')  //.creator
            'building_address'=>array('html_wrapper'=>'span', 'with'=>'address'),
            'building_address_load',
        );
    }
    
    public function modelForms()
    {
        return array(
            'default'=>array(
//                'type'=>'file',
//                'fileName'=>'default',
                'type'=>'generic',
                'columns'=>array(
                    'name' => 'textField',
                    'building_structure_type_id'=>array('dropdown','relName'=>'building_structure_type','empty'=>'Izaberi'),
                    'size'=>array('unitMeasureField','unit'=>array('size_unit_id','empty'=>'j.m.')),
                    'address_id'=>array('display','modelView'=>'building_address_load')
                )
            )
        );
    }
}