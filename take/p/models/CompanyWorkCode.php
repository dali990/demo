<?php

class CompanyWorkCode extends SIMAActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'public.company_work_codes';
    }

    public function moduleName()
    {
        return 'application';
    }
    
    public function __get($column)
    {
        switch ($column)
        {
            case 'DisplayName': return "{$this->name}({$this->code})";
            case 'SearchName': return $this->DisplayName;
            default: return parent::__get($column);
        }
    }
    
    public function rules()
    {
        return array(
            ['name, code', 'required', 'on'=>'insert, update'],
            ['parent_id', 'safe'],
            ['code', 'unique']
        );
    }
    
    public function relations($child_relations = [])
    {
        return [
            'parent' => [self::BELONGS_TO, 'CompanyWorkCode', 'parent_id']
        ];
    }
    
    public function scopes()
    {
        $alias = $this->getTableAlias();
        $uniq = SIMAHtml::uniqid();

        return [
            'byCode' => [
                'select'=>"rcwc$uniq.display_name_full as display_name_full,
                           rcwc$uniq.display_code_full as display_code_full,
                           rcwc$uniq.display_name_path as display_name_path,
                           rcwc$uniq.parent_ids as parent_ids,
                           rcwc$uniq.children_ids as children_ids,
                           $alias.*",
                'join'=>"left join public.recursive_company_work_codes rcwc$uniq on $alias.id=rcwc$uniq.id",
                'order'=>"rcwc$uniq.display_code_full"
            ]
        ];
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters': return [
                'name' => 'text',
                'code' => 'text',
                'parent' => 'relation'
            ];
            case 'textSearch': return [
                'name' => 'text',
                'code' => 'text',
                'parent' => 'relation'
            ];
            case 'default_order_scope': return 'byCode';
            default: return parent::modelSettings($column);
        }
    }
}

