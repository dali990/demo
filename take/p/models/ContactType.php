<?php 



class ContactType extends SIMAActiveRecord
{

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
 
    public function tableName()
    {
        return 'public.contact_types';
    }
    
    public function moduleName()
    {
        return 'application';
    }
    
    public function rules()
    {
        return array(
            array('name', 'required', 'message'=>'Polje "{attribute}" mora biti uneto'),
            array('description, icon_class', 'safe'),
        );
    }
    
    public function scopes()
    {
        return array(
            'byName'=>array(
                'order'=>'name, id',
            ),
        );
    }
    
    
// 	public function relations($child_relations = [])
//     {
//     	return array(
//     		'contacts'=>array(self::HAS_MANY, 'Contact', 'type_id')
//     	);
//     }
    //zbog kompatibilnosti
//     public function gettitle()
//     {
//     	return $this->name;
//     }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters': return [
                'name' => 'text'
            ];
            default: return parent::modelSettings($column);
        }
    }
}