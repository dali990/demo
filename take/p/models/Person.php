<?php

class Person extends Partner
{
    public $under_contract=null;
    public $file_version_id;
    public $work_license_id;
    
    /**
     * sluzi da obelezi da se osoba menja iz ugovora o radu
     * @var boolean
     */
    public $from_work_contract = false;
    
    /**
     * Ima objekat starog modela sa starim podacima, radi uporedjivanja sa novim u afterSave()
     * @var Person 
     */
    private $old_model = null;
    
    public $not_collect_min_iks_points = null;
    
    public $not_in_recursive_company_sector_id = null;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'public.persons';
    }

    public function moduleName()
    {
        return 'application';
    }
    
    public function __get($column)
    {
        switch ($column)
        {
            case 'path2' : return 'addressbook/partner';
            case 'DisplayName': return $this->display_name;
//            case 'SearchName':
//            {
//                return $this->firstname . ' ' . $this->lastname;// .
//////                        '(firma:' . ((isset($this->company) ? $this->company->DisplayName : '/')) . (($this->comment != '') ? ' % ' . $this->comment : '') . ')';
//            }
            
            case 'SearchName':
            {
                $addition = '';
                if (!empty($this->nickname))
                {
                    $addition .= ' - '.$this->nickname;
                }
                if (isset($this->company))
                {
                    $addition .= ' @'.$this->company->DisplayName;
                }
                if (!empty($this->comment))
                {
                    $addition .= ' % '.$this->comment;
                }
                return $this->DisplayName . $addition;
            }
            case 'national_regulations_and_standards': return $this->getIKSPoints('national_regulations_and_standards',true);
            case 'new_materials_technologies_in_nacional_ind': return $this->getIKSPoints('new_materials_technologies_in_nacional_ind',true);   
            case 'international_regulations_and_standards': return $this->getIKSPoints('international_regulations_and_standards',true);
            case 'new_materials_technologies_in_internacional_ind': return $this->getIKSPoints('new_materials_technologies_in_internacional_ind',true);
            case 'activePeriod': return $this->getAllIKSPoints(true);
            case 'lastYear': return $this->getAllIKSPoints(false);
            default: return parent::__get($column);
        }
    }

    public function relations($child_relations = [])
    {
        $user_id = -1;
        if(Yii::app()->isWebApplication())
        {
            $user_id = Yii::app()->user->id;
        }
        $person_to_work_position = PersonToWorkPosition::model()->tableName();
        $persons_to_working_licences_table_name = PersonToWorkLicense::model()->tableName();

        return parent::relations(array_merge(array( 
            'company' => array(self::BELONGS_TO, 'Company', 'company_id'),
            'professional_degree' => array(self::BELONGS_TO, 'ProfessionalDegree', 'professional_degree_id'),
            'employee' => array(self::HAS_ONE, 'Employee', 'id'),
            'isEmployee' => array(self::STAT, 'Employee', 'id', 'select' => 'count(*) > 0'),
            'user' => array(self::HAS_ONE, 'User', 'id'),
            'isUser' => array(self::STAT, 'User', 'id', 'select' => 'count(*) > 0'),
            'image' => array(self::BELONGS_TO, 'File', 'image_id'),
            'work_contracts'=> array(self::HAS_MANY, 'WorkContract', 'employee_id'),
            'work_license'=> array(self::HAS_MANY, 'PersonToWorkLicense', 'person_id'),
            'work_licenses'=>array(self::MANY_MANY, 'WorkLicense', "$persons_to_working_licences_table_name(person_id,working_license_id)"),
            'person_companies'=>array(self::MANY_MANY, 'Company','persons_to_companies(person_id,company_id)'),
            'active_work_contract' => array(self::HAS_ONE, 'WorkContract', 'employee_id', 'scopes'=>['activeFromTo'=>[null,null]]),
            'partner'=>array(self::BELONGS_TO,'Partner','id'),
            'safe_evd_4'=>array(self::HAS_MANY, 'SafeEvd4', 'person_id'),
            'safe_evd_5'=>array(self::HAS_MANY, 'SafeEvd5', 'person_id'),
            'trainings'=>array(self::HAS_MANY, 'PersonToTraining', 'person_id'),
            'employee_candidate'=>array(self::HAS_ONE, 'EmployeeCandidate', 'id'),
            'unconfirmed_trainings'=>array(self::STAT, 'Training','responsible_id',
                'condition'=>"confirmed is null"
            ),
            'meetings'=>array(self::HAS_MANY, 'PersonToMeeting', 'person_id'),
            'present_meetings'=>array(self::HAS_MANY, 'PersonToMeeting', 'person_id',
                'condition'=>'person_present=true'),
            'iks_points'=>array(self::MANY_MANY, 'IKSPoint', 'hr.persons_to_iks_points(person_id,iks_point_id)'),
            'work_positions'=>array(self::MANY_MANY, 'WorkPosition', "$person_to_work_position(person_id, work_position_id)"),
            'directory_work_positions'=>array(self::MANY_MANY, 'WorkPosition', "$person_to_work_position(person_id, work_position_id)",
                'condition'=>"is_director=true"),
            'theme' => array(self::MANY_MANY, 'Theme', 'base.persons_to_themes(person_id,theme_id)'),
            'person_to_themes'=>array(self::HAS_MANY, 'PersonToTheme', 'person_id')
        ), $child_relations));
    }

    public function rules()
    {
        return array(
            array('lastname, firstname', 'required'),
            array('company_id, JMBG, comment, id, text,with, degree, middlename, nickname, '
                . 'professional_degree_id, education_title_id, birthdate', 'safe'),
            array('image_id, under_contract, scopes, iks_license_activated_date, not_collect_min_iks_points, qualification_level_id', 'safe'),
            array('file_version_id', 'safe'),
            ['firstname, lastname', 'length', 'max' => 30],
            array('birthdate', 'ruleDateCheck'),
            array('JMBG', 'numerical'),
            array('JMBG', 'checkJMBG'),
            array('company_id', 'checkCompany'),
            array('company_id, image_id, JMBG, professional_degree_id, education_title_id', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => array('insert', 'update')),
            ['firstname, lastname', 'ruleWithoutNumbers'],
            ['iks_license_activated_date', 'ruleDateCheck'],
            ['JMBG, birthdate', 'checkJMBGBirthdateSync'],
            ['birthdate', 'checkBirthdateLessThanOrEqualToCurrentDate'],
        );
    }
    
    public function checkBirthdateLessThanOrEqualToCurrentDate($attribute, $params)
    {
        if(
                !$this->hasErrors('birthdate') && !empty($this->birthdate)
                && strtotime($this->birthdate) > strtotime('today')
        )
        {
            $this->addError('birthdate', Yii::t('Person', 'BirthdateLessThanOrEqualToCurrentDateError'));
        }
    }
    
    public function checkJMBGBirthdateSync($attribute, $params)
    {
        if(!$this->hasErrors() && !empty($this->JMBG))
        {
            if(!empty($this->birthdate))
            {
                $jmbg_day = intval(substr($this->JMBG, 0, 2));
                $jmbg_month = intval(substr($this->JMBG, 2, 2));
                $jmbg_year = intval(substr($this->JMBG, 4, 3));

                if($jmbg_year < 100)
                {
                    $jmbg_year += 1000;
                }
                $jmbg_year += 1000;

                $birthdate_day = intval(substr($this->birthdate, 0, 2));
                $birthdate_month = intval(substr($this->birthdate, 3, 2));
                $birthdate_year = intval(substr($this->birthdate, 6, 4));

                if(
                    $jmbg_day !== $birthdate_day
                    || $jmbg_month !== $birthdate_month
                    || $jmbg_year !== $birthdate_year
                )
                {
                    $this->addError('JMBG', Yii::t('Person', 'JMBGBirthdateSyncError'));
                    $this->addError('birthdate', Yii::t('Person', 'JMBGBirthdateSyncError'));
                }
            }
            else
            {
                $year = intval(substr($this->JMBG, 4, 3));
                if($year < 100)
                {
                    $year += 1000;
                }
                $year += 1000;
                $_date = 
                        substr($this->JMBG, 0, 2)
                        .'.'.
                        substr($this->JMBG, 2, 2)
                        .'.'.
                        $year
                        .'.';

                $old_birthdate = $this->birthdate;
                $this->birthdate = $_date;
                if ($this->birthdate !== $_date || !$this->validate(['birthdate']))
                {
                    $this->addError('JMBG', Yii::t('Person','WrongJMBGFormat',[
                        '{date}' => $_date
                    ]));
                    $this->addError('birthdate', Yii::t('Person','WrongJMBGFormat',[
                        '{date}' => $_date
                    ]));
                }
                $this->birthdate = $old_birthdate;
            }
        }
    }
    
    public function checkJMBG($attribute, $params)
    {
        if (!empty($this->JMBG))
        {
            $error_messages = [];
            
            if (!ctype_digit($this->JMBG))
            {
                $error_messages[] = Yii::t('Person','JMBGMustContainOnlyDigits');
            }
            
            if (strlen($this->JMBG) !== 13)
            {
                $error_messages[] = Yii::t('Person','JMBG must have 13 digits.');
            }
            
            $day = intval(substr($this->JMBG, 0, 2));
            $month = intval(substr($this->JMBG, 2, 2));
            $year = intval(substr($this->JMBG, 4, 3));
            
            if($day < 1 || $day > 31)
            {
                $error_messages[] = Yii::t('Person', 'JMBGDayInvalid', [
                    '{day}' => $day
                ]);
            }
            if($month < 1 || $month > 12)
            {
                $error_messages[] = Yii::t('Person', 'JMBGMonthInvalid', [
                    '{month}' => $month
                ]);
            }
            
            if(!empty($error_messages))
            {
                $error_message = implode('</br>', $error_messages);
                $this->addError('JMBG', $error_message);
            }
        }
    }
    
    public function checkCompany()
    {
        if (!$this->hasErrors())
        {
            if (
                    ($this->isNewRecord || $this->columnChanged('company_id')) && 
                    !empty($this->company_id) && 
                    intval($this->company_id) === Yii::app()->company->id && 
                    empty($this->active_work_contract)
               )
            {
                   $this->addError('company_id', Yii::t('Person', 'PersonDontHaveActiveWorkContractInCompany', [
                        '{company}' => $this->company->DisplayName
                   ]));
            }
        }
    }
    
    public function __toString()
    {
        return $this->DisplayName;
    }

    public function beforeSave()
    {
        $this->old_model = Person::model()->findByPk($this->id);

        if ($this->getIsNewRecord())
        {
            $partner = new Partner();
            $partner->company = false;
            $partner->save();
            $this->id = $partner->id;
        }

        if (!empty($this->file_version_id))
        {
            $file = null;
            
            if (empty($this->image_id))
            {
                $file = new File();
                $file->responsible_id = Yii::app()->user->id;
                $file->name = 'Avatar '.$this->DisplayName;
                $file->save();
                $file->beforeChangeLock();
                if (isset($this->user))
                {
                    EmployeeTQLLimit::addFileToPublicTag('address_book_light', $file->id);
                }
                else
                {
                    EmployeeTQLLimit::addFileToPublicTag('address_book', $file->id);
                }
                $file->responsible_id = null;
                $file->save();
                $file->afterChangeUnLock();

                $this->image_id = $file->id;
                $this->image = $file;
            }
            else
            {
                $file = File::model()->resetScope()->findByPk($this->image_id);
            }
            
            /** ubacivanje upload-a fajla */
            $file->appendUpload($this->file_version_id, true);
            
            $file->refresh();
            
            if($this->image->last_version->type !== FileVersion::$TYPE_SHELL)
            {
                $temp_image = TemporaryFile::CreateFromFile($file);
                $width = Yii::app()->params['avatar_width'];
                $resized_temp_file = $temp_image->resizeImage($width);

                $file->appendTempFile($resized_temp_file->filename, $file->name);
            }
        }
        
        if (empty($this->birthdate) && !empty($this->JMBG))
        {
            $year = intval(substr($this->JMBG, 4, 3));
            if($year < 100)
            {
                $year += 1000;
            }
            $year += 1000;
            $_date = 
                    substr($this->JMBG, 0, 2)
                    .'.'.
                    substr($this->JMBG, 2, 2)
                    .'.'.
                    $year
                    .'.';
            
            $this->birthdate = $_date;
        }
        
        return parent::BeforeSave();
    }
    
    //zabranjeno je brisanje jer je moguce da prodje brisanje osobe, a kasnije pukne brisanje partnera
    public function delete($check_result=true, $throwExceptionOnFail = true)
    {
        Yii::app()->errorReport->createAutoClientBafRequest("Pokušaj brisanja osobe. Stack trace: ".SIMAMisc::stackTrace());
                
        throw new SIMAWarnException(Yii::t('Person','PersonDeleteForbiden'));
    }
    
    public function afterSave()
    {
        //delete all connections if company is changed - PersonToCompanies & PersonToWorkPosition
        if (!empty($this->__old['company_id']) && $this->columnChanged('company_id'))
        {
            PersonToCompanies::model()->deleteAllByAttributes([
                'person_id' => $this->id,
                'company_id' => $this->__old['company_id']
            ]);
            PersonToWorkPosition::model()->deleteAllByAttributes([
                'person_id' => $this->id
            ]);
        }
        
        //set new connection or if old doesnt exist
        if (
                !empty($this->company_id) && 
                (
                    $this->isNewRecord || 
                    $this->columnChanged('company_id'))
                )
        {
            $conn = PersonToCompanies::model()->findByAttributes([
                'person_id' => $this->id,
                'company_id' => $this->company_id
            ]);
            if (empty($conn))
            {
                $conn = new PersonToCompanies();
                $conn->person_id = $this->id;
                $conn->company_id = $this->company_id;
                $conn->save();
            }
        }
        
        return parent::AfterSave();
    }

    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters' : return array_merge(parent::modelSettings('filters'),array(
                'firstname' => 'text',
                'lastname' => 'text',
                'middlename' => 'text',
                'nickname' => 'text',
                'JMBG'=>'text',
                'comment'=>'text',
                'company' => 'relation',
                'company_id' => 'dropdown',
                'work_license_id'=>array('func'=>'filter_work_license_id'),
                'work_license'=>'relation',
                'under_contract'=>array('dropdown','func'=>'filter_under_contract'),
                'partner' => 'relation',
                'not_collect_min_iks_points' => ['func'=>'filter_not_collect_min_iks_points'],
                'employee'=>'relation',
                'birthdate' => 'date_range',
                'person_to_work_positions' => 'relation',
                'user' => 'relation',
                'employee_candidate' => 'relation',
                'person_to_themes' => 'relation',
                'person_to_worked_hours_evidences' => 'relation',
                'worked_hours_reports' => 'relation',
                'meetings' => 'relation',
                'not_in_recursive_company_sector_id' => ['func' => 'filter_not_in_recursive_company_sector_id'],
                'safe_evd_2_records' => 'relation',
                'safe_evd_13_records' => 'relation',
                'qualification_level' => 'relation'              
            ));
            case 'textSearch' : return array(
                'firstname' => 'text',
                'lastname' => 'text',
                'middlename' => 'text',
                'nickname' => 'text',
 		'JMBG'=>'text',
                'comment' => 'text',
                'contacts' => 'relation',
                'partner_to_address'=>'relation',
                'bank_accounts' => 'relation',
                'phone_numbers' => 'relation',
                'email_addresses' => 'relation'
//                'company' => 'relation' /// namerno iskljuceno jer bi predugo trajalo; ceka prioritetnu pretragu ili pretragu po konkretnim atributima;
            );
            case 'options': return array('open','form');
            case 'form_list_relations' : return array('addresses', 'contacts', 'bank_accounts', 'partner_lists', 'phone_numbers', 'email_addresses');
            case 'update_relations': return [
                'employee', 'user', 'partner', 'person_to_themes', 'person_to_worked_hours_evidences'
            ];
            case 'mutual_forms': 
                $result = [
                    'relations' => []
                ];
                
                if( Yii::app()->user->checkAccess('menuShowLegalWorkPositionCompetitionCandidates'))
                {
                    $result['relations'][] = 'employee_candidate';
                }
                
                return $result;
            default: return parent::modelSettings($column);
        }
    }
    
    public function filter_not_collect_min_iks_points($condition, $alias)
    {        
        if (!is_null($this->not_collect_min_iks_points))
        {
            $uniq = SIMAHtml::uniqid();
            $iks_min_points_year_quota = IKSPoint::$MIN_POINTS_YEAR_QUOTA;
            $persons_table = Person::model()->tableName();
            $persons_to_iks_points_table = PersonToIKSPoint::model()->tableName();
            $iks_points_table = IKSPoint::model()->tableName();
            $temp_condition = new CDbCriteria();
            $temp_condition->condition = "$alias.iks_license_activated_date is not null and $iks_min_points_year_quota < ("
                    . "select sum(iks$uniq.national_regulations_and_standards+iks$uniq.new_materials_technologies_in_nacional_ind+"
                    . "iks$uniq.international_regulations_and_standards+iks$uniq.new_materials_technologies_in_internacional_ind) as iks_points_sum$uniq"
                    . " from $persons_table p$uniq"
                    . " join $persons_to_iks_points_table piks$uniq on p$uniq.id=piks$uniq.person_id"
                    . " join $iks_points_table iks$uniq on piks$uniq.iks_point_id=iks$uniq.id"
                    . " where p$uniq.id=$alias.id and iks$uniq.date > "
                                         . "(select case when $alias.iks_license_activated_date > CURRENT_DATE - INTERVAL '1 years' then $alias.iks_license_activated_date"
                                         . " else CURRENT_DATE - INTERVAL '1 years' end))";
            $condition->mergeWith($temp_condition,true);            
        }
    }
    
    public function filter_company_id($condition)
    {
        if(isset($this->company_id) && $this->company_id!=null)
        {
            $param_id = SIMAHtml::uniqid();
            $alias = $this->getTableAlias();
            $temp_condition = new CDbCriteria();
            $temp_condition->condition="${alias}.company_id=:param${param_id} and ${alias}.id in (select id from employees)";
            $temp_condition->params = array(':param'.$param_id=>$this->company_id);
            $condition->mergeWith($temp_condition,true);
        }
    }
    
    public function filter_work_license_id($condition)
    {
        if(isset($this->work_license_id) && $this->work_license_id!=null)
        {
            $persons_to_working_licences_table_name = PersonToWorkLicense::model()->tableName();
            $param_id=  SIMAHtml::uniqid();
            $temp_condition=new CDbCriteria();
            $temp_condition->condition="t.id in (select person_id from $persons_to_working_licences_table_name where working_license_id=:param$param_id)";
            $temp_condition->params=array(':param'.$param_id=>$this->work_license_id);
            $condition->mergeWith($temp_condition, true);
        }
    }
    
    public function filter_under_contract($condition)
    {
        if (!is_null($this->under_contract))
        {
            $temp_condition = null;
                                    
            if ($this->under_contract == 'true')
            {                
                $temp_condition = new SIMADbCriteria([
                    'Model' => 'Employee',
                    'model_filter' => [
                        'active_work_contract' => [
                            'activeFromTo' => [null, null]
                        ]
                    ]
                ], true);
            }
            else
            {
                $active_condition = new SIMADbCriteria([
                    'Model' => 'Employee',
                    'model_filter' => [
                        'active_work_contract' => [
                            'activeFromTo' => [null, null]
                        ]
                    ]
                ], true);
                
                $all_condition = new SIMADbCriteria([
                    'Model' => 'Employee',
                    'model_filter' => []
                ], true);
                
                $active_employees_ids = explode(',', $active_condition->ids);
                $all_employees_ids = explode(',', $all_condition->ids);
                
                
                $inactive_ids = [];
                foreach($all_employees_ids as $alle)
                {
                    $found = false;
                    foreach($active_employees_ids as $activee)
                    {
                        if($alle === $activee)
                        {
                            $found = true;
                            unset($activee);
                            break;
                        }
                    }
                    
                    if($found === false)
                    {
                        $inactive_ids[] = $alle;
                    }
                    
                    unset($alle);
                }
                                
                $temp_condition = new SIMADbCriteria([
                    'Model' => 'Employee',
                    'model_filter' => [
                        'ids' => empty($inactive_ids) ? -1 : $inactive_ids
                    ]
                ]);
            }
            
            $condition->mergeWith($temp_condition, true);
        }
        if (isset($this->under_contract)) $this->under_contract = null;
    }
    
    public function filter_not_in_recursive_company_sector_id($criteria, $alias)
    {
        if (!empty($this->not_in_recursive_company_sector_id))
        {
            $uniq = SIMAHtml::uniqid();
            $persons_to_work_positions_tbl = PersonToWorkPosition::model()->tableName();
            $work_positions_tbl = WorkPosition::model()->tableName();
            
            $temp_criteria = new SIMADbCriteria();
            $temp_criteria->condition = "
                not exists(select 1 from $persons_to_work_positions_tbl ptwp$uniq where ptwp$uniq.person_id = $alias.id) or
                exists(
                    select * 
                    from $persons_to_work_positions_tbl ptwp$uniq
                    left join $work_positions_tbl wp$uniq on wp$uniq.id = ptwp$uniq.work_position_id
                    where (
                        ptwp$uniq.person_id = $alias.id and 
                        (
                            wp$uniq.company_sector_id != :param1$uniq and 
                            wp$uniq.company_sector_id not in (select unnest(rcs$uniq.children_ids) from hr.recursive_company_sectors rcs$uniq where rcs$uniq.id=:param1$uniq)
                        )
                    )
                )
            ";
            $temp_criteria->params = [
                ":param1$uniq" => $this->not_in_recursive_company_sector_id
            ];
            $criteria->mergeWith($temp_criteria, true);
        }
    }
    
    public function droplist($key) {
        switch ($key) {
            case 'under_contract': return array('false' => Yii::t('Person','UnderContractNotWork'), 'true' => Yii::t('Person','UnderContractWork'));
            case 'qualification_level': 
                $model_filter = [
                    'model_filter' => [
                        'order_by' => 'code ASC'
                    ]
                ];
                $qualifications = QualificationLevel::model()->findAll($model_filter);
                return CHtml::listData($qualifications, 'id', 'DisplayName');
            default: return parent::droplist($key);
        }
    }
    
    public function getQualificationLevelOptionsTitleDescription() 
    {
        $options = [];
        $qls = QualificationLevel::model()->findAll();
        foreach ($qls as $ql) 
        {
            $options[$ql->id] = ['title' => $ql->description];
        }
        return $options;
    }

    public function scopes()
    {
        $alias = $this->getTableAlias();
        $user_table = User::model()->tableName();
        return array(
            'byName' => array('order' => "$alias.lastname, $alias.firstname, $alias.id"),
            'byCompanyOrder' => array(
                'order'=>"$alias.order_in_company"
            ),
            'iks_points_review' => [
                'condition' => 'iks_license_activated_date is not null'
            ],
            'not_user' => [
                'condition' => "NOT EXISTS (SELECT 1 FROM $user_table u where u.id=$alias.id LIMIT 1)"
            ],
            'not_active_user' => [
                'condition' => "NOT EXISTS (SELECT 1 FROM $user_table u where u.id=$alias.id AND u.active=TRUE LIMIT 1)"
            ]
        );
    }
    
    public function has_licenses($ids)
    {       
//        $uniq = SIMAHtml::uniqid();
        $alias = $this->getTableAlias();
        $persons_to_working_licences_table_name = PersonToWorkLicense::model()->tableName();

        $this->getDbCriteria()->mergeWith(array(
            'join'=>"left join $persons_to_working_licences_table_name ptwl on $alias.id=ptwl.person_id",
            'condition'=>"ptwl.working_license_id in ($ids)",
        ));
        return $this;
    }
    
    /**
     * 
     * @param boolean $wiki If true initial are Capitol (MilosSreckovic) else not (milossreckovic)
     * @return string
     */
    public function generateUsername($wiki = false)
    {
        $firstname = SIMAHtml::removeSerbianLettersAndLower($this->firstname);
        $lastname = SIMAHtml::removeSerbianLettersAndLower($this->lastname);
        
        if ($wiki)
        {
            $firstname[0] = strtoupper($firstname[0]);
            $lastname[0] = strtoupper($lastname[0]);
        }
        
        return $firstname.$lastname;
    }
    
    public function getAllIKSPoints($active_period=true)
    {
        $iks_points = 0;
        $iks_points += $this->getIKSPoints('national_regulations_and_standards', $active_period);
        $iks_points += $this->getIKSPoints('new_materials_technologies_in_nacional_ind', $active_period);
        $iks_points += $this->getIKSPoints('international_regulations_and_standards', $active_period);
        $iks_points += $this->getIKSPoints('new_materials_technologies_in_internacional_ind', $active_period);
        
        return $iks_points;
    }
    
    /**
     * vraca broj iks bodova iz oblasti koja je zadata
     * @param boolean $iks_section - oblast za koju se racunaju iks bodovi
     * @param boolean $active_period - ako je true onda je period racunanja poslednji aktivni period, inace je period racunanja poslednja godina
     */
    public function getIKSPoints($iks_section, $active_period=true)
    {
        $iks_points = 0;
        if ($this->iks_license_activated_date !== null)
        {
            foreach ($this->iks_points as $iks_point)
            {
                if ($active_period) //poslednji aktivni period
                {
                    $active_date = strtotime($this->getActiveStartDate());
                    //ako je datum aktivacije noviji od perioda racunanja onda racunamo od datuma aktivacije
                    if (strtotime($this->iks_license_activated_date) > $active_date)
                    {
                        if (strtotime($iks_point->date) > strtotime($this->iks_license_activated_date))
                        {
                            $iks_points += $iks_point->$iks_section;
                        }
                    }
                    //inace racunamo za ceo period
                    else
                    {
                        if (strtotime($iks_point->date) > $active_date)
                        {
                            $iks_points += $iks_point->$iks_section;
                        }
                    }
                }
                else //poslednja godina
                {
                    $year_ago_date = strtotime("-1 year", time());
                    //ako je datum aktivacije noviji od poslednje godine onda racunamo od datuma aktivacije
                    if (strtotime($this->iks_license_activated_date) > $year_ago_date)
                    {
                        if (strtotime($iks_point->date) > strtotime($this->iks_license_activated_date))
                        {
                            $iks_points += $iks_point->$iks_section;
                        }
                    }
                    //inace racunamo za poslednju godinu
                    else
                    {
                        if (strtotime($iks_point->date) > $year_ago_date)
                        {
                            $iks_points += $iks_point->$iks_section;
                        }
                    }
                }
            }
        }
        
        return $iks_points;
    }
    
    public function getActiveStartDate()
    {
        if ($this->iks_license_activated_date !== null)
        {
            $curr_date = new DateTime();
            $iks_license_activated_date = new DateTime($this->iks_license_activated_date);
            $interval = $curr_date->diff($iks_license_activated_date);
            $months = $interval->m;
            $days = $interval->d;
            $years_diff = intval($interval->format('%y'));
            $mod = $years_diff % 5;

            $year_period = strtotime("-$mod year -$months month -$days day", time());
            $date = date("Y-m-d", $year_period);
            
            return $date;
        }
        else
            return null;
    }
    
    public function getActivePassedInterval()
    {
        if ($this->iks_license_activated_date !== null)
        {
            $curr_date = new DateTime();  
            $active_date = new DateTime($this->getActiveStartDate());
            $interval = $curr_date->diff($active_date);
            
            return $interval;
        }
        else
            return null;
    }
    
    public function getActiveRemainingInterval()
    {
        if ($this->iks_license_activated_date !== null)
        {
            $active_start_date = $this->getActiveStartDate();
            $five_year_active_time = strtotime("+5 year", strtotime($active_start_date));
            $five_year_active_date = new DateTime(date("Y-m-d", $five_year_active_time));
            $curr_date = new DateTime();
            $interval = $five_year_active_date->diff($curr_date);
            
            return $interval;
        }
        else
            return null;
    }
    
    /**
     * pomocna funkcija koja proverva da li neka licenca pripada iks_points-u
     * @param Model $iks_point
     * @param int $license_id
     * @return boolean
     */
    private function IKSPointHasLicense($iks_point, $license_id)
    {
        $founded = false;
        foreach ($iks_point->work_licenses as $license)
        {
            if ($license->id === $license_id)
                $founded = true;
        }
        
        return $founded;
    }
    
    /**
     * vraca id-jeve referenca koje zadovoljavaju uslov za velike licence(da ih poseduje osoba i da je neka velika licenca vec imala tu referencu)
     * @return Reference
     */
    public function getReferencesIds()
    {
        $references_ids = [];
        foreach ($this->work_license as $license)
        {
            foreach ($license->person_license_to_reference as $person_license_to_reference)
            {
                if (
                        isset($person_license_to_reference->reference->reference_to_company_license) 
                        && count($person_license_to_reference->reference->reference_to_company_license) > 0
                        && !in_array($person_license_to_reference->reference_id, $references_ids)
                   )
                {
                    array_push($references_ids, $person_license_to_reference->reference_id);
                }
            }
        }
        return $references_ids;
    }
    
    /**
     * dohvati ID od sef-a
     * @param boolean $all_bosses ukoliko je true, dohvata sve ID-eve
     * @return array niz ID-eva od svih sefova
     */
    public function getSectorDirectors($all_bosses=false)
    {
        return SIMASQLFunctions::getPersonSectorDirectorsIds($this, $all_bosses);
    }
    
    public function isDirector()
    {
        return count($this->person->directory_work_positions) > 0;
    }
    
    /**
     * Da li je prva osoba sef drugoj osobi. 
     * Ukoliko je osoba na sefovskom mestu, sef mu je direktor nad sektora
     * Generalni direktor je sam sebi sef i to direktan (jer nema ko drugi da bude)
     * 
     * @param Person $boss osoba za koju se pita da li je sef zaposlenom
     * @param Person $worker osoba za koju se pita da li je zaposleni sefu
     * @param boolean $direct_boss da li je osoba direktan sef. Osoba je direktan sef ako ima direktorsku poziciju
     *                      u sektoru u kojem se worker nalazi, ili u sledecem nad sektoru, 
     *                      ukoliko taj sektor nema nikog na direktorskoj poziciji
     * @return boolean
     */
    public static function isBossOf(Person $boss, Person $worker, $direct_boss = false)
    {
        $boss_ids = $worker->getSectorDirectors(!$direct_boss);
        $is_boss = !empty($boss_ids) && in_array($boss->id, array_map('intval', $boss_ids));
        
        $is_director = in_array($boss->id, Yii::app()->company->getDirectorIds())
                        && $worker->id === $boss->id;
        
        return $is_boss || $is_director;
    }
    
    public function getAllSectorsWhereIamDirector()
    {
        $sectors = [];        
        foreach ($this->person->directory_work_positions as $directory_work_position)
        {
            array_push($sectors, $directory_work_position->company_sector);
        }
        
        return $sectors;
    }
    
    public function needToWriteReports()
    {
        return !empty($this->user) && !in_array($this->id, Yii::app()->configManager->get('admin.no_day_reports_persons', false));
    }
}
