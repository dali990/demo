<?php

/**
 * @package SIMA
 * @subpackage Base
 */
class MedicalReports extends SIMAActiveRecord
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'public.medical_reports';
    }

    public function __get($column)
    {
        switch ($column)
        {
            case 'path2': return 'files/file';
            case 'DisplayName': return $this->name;
            case 'SearchName': return $this->name;
            default: return parent::__get($column);
        }
    }

    public function rules()
    {
        return array(
            array('id, name, opening_date, review_date, c6_date, description, employee_id', 'safe'),
        );
    }

    public function relations($child_relations = [])
    {
        return array(
            'file' => array(self::BELONGS_TO, 'File', 'id'),
            'employee' => array(self::BELONGS_TO, 'Employee', 'employee_id')
        );
    }

    public function scopes()
    {
        return array(
            'byName' => array('order' => 'name, id'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'name' => 'Naziv',
            'opening_date' => 'Datum otvaranja',
            'review_date' => 'Datum pregleda',
            'c6_date' => 'C6',
            'description' => 'Opis',
            'employee_id'=>'Zaposleni'
        ) + parent::attributeLabels();
    }
    
    protected function modelOptions(User $user = null):array
    {
        return ['open','form','delete'];
    }

    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters' : return array_merge(parent::modelSettings('filters'),array(
                'name' => 'text',
                'employee_id'=>'dropdown'
            ));
            default: return parent::modelSettings($column);
        }
    }

    public function BeforeSave()
    {

        /** svaki zavodni broj mora da ima fajl - ovo se uglavnom desava kad je novi broj */
        if (!isset($this->file) && ($this->id == null || $this->id == ''))
        {
            $file = new File();
            $file->name = $this->name;
            $file->save();
            $this->id = $file->id;
            $this->file = $file;
        }
        return parent::BeforeSave();
    }

}