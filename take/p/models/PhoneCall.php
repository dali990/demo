<?php

class PhoneCall extends SIMAActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'public.phone_calls';
    }
    
    public function moduleName()
    {
        return 'application';
    }
}