<?php

class ChangePasswordForm extends CFormModel
{
    public $username;
    public $old_password;
    public $new_password;
    public $repeat_new_password;
    
    public $user_id = null;
    
    private $_identity;
    
    public function rules()
    {
        return array(
            array('username, old_password, new_password, repeat_new_password, user_id', 'required'),
            array('new_password', 'checkPassword'),
            array('repeat_new_password', 'compare', 'compareAttribute' => 'new_password', 'message' => Yii::t('ChangePasswordForm', 'RepeatedPasswordDoesNotMatchPassword'))
        );
    }

    /**
     * Declares attribute labels.
     */
    public function attributeLabels()
    {
        return array(
            'username'=>Yii::t('ChangePasswordForm', 'Username'),
            'old_password'=>Yii::t('ChangePasswordForm', 'OldPassword'),
            'new_password'=>Yii::t('ChangePasswordForm', 'NewPassword'),
            'repeat_new_password'=>Yii::t('ChangePasswordForm', 'RepeatNewPassword'),
        );
    }

    public function checkPassword($attribute, $params)
    {
        if(!$this->hasErrors())
        {
            $this->_identity = new SIMAUserIdentity($this->username, $this->old_password);
                
            if(!$this->_identity->authenticate())
            {
                $this->addError('old_password', Yii::t('ChangePasswordForm', 'IncorrectUsernameOrPassword'));
                $this->addError('username', Yii::t('ChangePasswordForm', 'IncorrectUsernameOrPassword'));
            }
            
            if(Yii::app()->passwordValidator->isUserHasAlreadyUsedPassword($this->user_id, $this->new_password) === true)
            {
                $this->addError('new_password', Yii::t('ChangePasswordForm', 'PasswordAlreadyUsed'));
            }
            
            $check_password_strength_msg = Yii::app()->passwordValidator->checkStrength($this->new_password);
            if(!empty($check_password_strength_msg))
            {
                $this->addError('new_password', implode('<br />', $check_password_strength_msg));
            }
        }
    }
}
