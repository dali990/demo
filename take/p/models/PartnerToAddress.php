<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class PartnerToAddress extends SIMAActiveRecord
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
 
    public function tableName()
    {
        return 'public.partners_to_addresses';
    }
    
    public function moduleName()
    {
        return 'application';
    }
    
    public function primaryKey() {
        return array('partner_id', 'address_id');
    }
    
    public function relations($child_relations = [])
    {
        return array(
            'partner' => array(self::BELONGS_TO, 'Partner', 'partner_id'),
            'address' => array(self::BELONGS_TO, 'Address', 'address_id'),
        );
    }
    
    public function modelSettings($column) 
    {
        switch ($column)
        {
            case 'filters':
                return [
                    'partner' => 'relation',
                    'address' => 'relation'
                ];
            case 'textSearch' : return array('address'=>'relation',);
            case 'update_relations' : return array(
                'partner',
                'address'
            );
            default: return parent::modelSettings($column);
        }
    }
}
