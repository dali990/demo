<?php

class PartnerGUI extends SIMAActiveRecordDisplayGUI
{
    public function columnLabels() 
    {
        return [
            'partner_lists' => Yii::t('Partner', 'PartnerLists'),
            'main_phone_number' => Yii::t('Partner', 'MainPhoneNumber'),
            'main_phone_number_id' => Yii::t('Partner', 'MainPhoneNumber'),
            'main_email_address' => Yii::t('Partner', 'MainEmailAddress'),
            'main_email_address_id' => Yii::t('Partner', 'MainEmailAddress'),
            'main_address_display' => Yii::t('Partner', 'MainAddress'),
        ] + parent::columnLabels();
    }
    
    public function modelViews()
    {
        return array_merge(parent::modelViews(), [
            'options' => ['origin' => 'Partner'],
            'basic_info',
            'full_info' => ['origin' => 'Partner'],
            'partner_lists'
        ]);
    }
    
    public function modelLabel($plural = false)
    {
        return Yii::t('BaseModule.Common', $plural?'Partners':'Partner');
    }
    
    public function columnDisplays($column) 
    {
        $owner = $this->owner;

        switch ($column) 
        {
            case 'depend_data': 
                $result = Yii::t('BaseModule.Common', 'DoNotHave');
                
                $model = clone $owner;
                $model->setScenario('delete');
                try
                {
                    $model->afterValidate();
                }
                catch(SIMAWarnException $e)
                {
                    $result = $e->getMessage();
                }
                
                return $result;
            case 'display_name':
                $result = parent::columnDisplays($column);
                if(empty($owner->person) && empty($owner->company_model))
                {
                    $result = $owner->display_name;
                }
                return $result;
            default: return parent::columnDisplays($column);
        }
    }
    
    public function afterValidateDependencyError(array $foreign_dependencies)
    {        
        $allowed_tables = [
            Company::model()->tableName().':id',
            Person::model()->tableName().':id',
            Contact::model()->tableName().':partner_id',
            PartnerToAddress::model()->tableName().':partner_id',
            PartnerToPartnerList::model()->tableName().':partner_id',
        ];
        
        $parsed_foreign_dependencies = array_diff($foreign_dependencies, $allowed_tables);
        
        parent::afterValidateDependencyError($parsed_foreign_dependencies);
    }
    
    public function renderOptions()
    {
        $owner = $this->owner;
        
        if (!empty($owner->person))
        {
            $model = $owner->person;
        }
        else if (!empty($owner->company_model))
        {
            $model = $owner->company_model;
        }
        else
        {
            $model = $owner;
        }
        
        return SIMAHtml::modelFormOpen($model).SIMAHtml::modelDelete($owner);
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            case 'warn_not_person_nor_company': return [
                'columns' => [
                    'display_name', 'company', 'main_address', 'comment_thread', 'account_code', 'main_bank_account', 'main_contact', 'depend_data'
                ]
            ];
            default: return [
                'columns' => ['display_name']
            ];
        }
    }
    
    public function vueProp(string $prop, array $scopes = []):array
    {
        $owner = $this->owner;

        switch ($prop)
        {
            case 'is_person':
                return [!empty($owner->person), SIMAMisc::getModelTags($owner)];
            case 'is_company':
                return [!empty($owner->company_model), SIMAMisc::getModelTags($owner)];
            case 'display_icon': 
                if (isset($owner->person))
                {
                    return $owner->person->vueProp('display_icon', $scopes);
                }
                else if (isset($owner->company_model))
                {
                    return $owner->company_model->vueProp('display_icon', $scopes);
                }

                return parent::vueProp($prop, $scopes);
                
            default: return parent::vueProp($prop, $scopes);
        }
    }
}