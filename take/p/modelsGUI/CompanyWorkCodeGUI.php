<?php

class CompanyWorkCodeGUI extends SIMAActiveRecordGUI 
{
    public function columnLabels() 
    {
        return array(
            'name' => Yii::t('CompanyWorkCode', 'Name'),
            'display_name' => Yii::t('CompanyWorkCode','Name'),
            'code' => Yii::t('CompanyWorkCode', 'Code'),
            'parent' => Yii::t('CompanyWorkCode', 'Parent'),
            'parent_id' => Yii::t('CompanyWorkCode', 'Parent')
        ) + parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return Yii::t('CompanyWorkCode', $plural ? 'CompanyWorkCodes' : 'CompanyWorkCode');
    }

    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default:
                return array(
                    'columns' => array('name', 'code', 'parent')
                );
        }
    }
    
    public function modelForms() 
    {
        return array(
            'default' => array(
                'type'=>'generic',
                'columns'=>array(
                    'name' => 'textField',
                    'code' => 'textField',
                    'parent_id' => ['relation', 'relName' => 'parent', 'add_button' => true]
                )
            )
        );
    }
}

