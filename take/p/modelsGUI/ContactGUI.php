<?php

class ContactGUI extends SIMAActiveRecordGUI
{

    public function columnLabels()
    {
        return array(
            'type_id'=>Yii::t('Contact','ContactType'),
            'partner_id'=>Yii::t('Contact','ContactOwner'),
            'value'=>Yii::t('Contact','Contact'),
            'phone_number'=>Yii::t('BaseModule.PhoneNumber','PhoneNumber'),
            'email' => Yii::t('Contact','Email')
        )+parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return Yii::t('Contact', $plural?'Contacts':'Contact');
    }
    
    public function modelViews()
    {
        return array(
            'default'=>array(
//                'style'=>'border: 1px solid;'
                ),
            'default_edit'
//            'tabTemplates'=>array('with'=>array('templated_file'),'style'=>'border: 1px solid;')  //.creator
        );
    }
    
    public function modelForms()
    {
        return array(
            'default'=>array(
                'type'=>'generic',
                'columns'=>array(
                    'type_id'=>'dropdown',
                    'value'=>'textField',
                    'comment'=>'textArea',
                    'partner_id'=>'hidden',
                )
            )
        );
    }
    
    public function guiTableSettings($type)
    {
        switch($type)
        {
            case 'contact_convertor': return [
                'columns'=>[
                    'value' => ['min_width' => 200], 
                    'partner' => ['min_width' => 200], 
                    'phone_number' => ['min_width' => 500], 
                    'email' => ['min_width' => 200]
                ]
            ];
            default: return array(
                'columns'=>['value', 'partner']
            );
        }
    }
    
    public function columnDisplays($column) 
    {
        $owner = $this->owner;

        switch ($column) 
        {
            case 'phone_number':
                $phone_types = CHtml::listData(PhoneNumberType::model()->findAll(), 'id', 'name');
                $phone_parsed_values = $this->parsePhoneNumber($owner->value);
                
                return 
                    SIMAHtml::dropDownList("PhoneNumberType[type_id]", null, $phone_types, [
                        'empty' => Yii::t('BaseModule.PhoneNumber', 'PhoneNumberType'), 
                        'class' => ' phone-number-type'
                    ]) .
                    Yii::app()->controller->widget('SIMAPhoneField', [
                        'provider_number' => $phone_parsed_values['provider_number'],
                        'phone_number'    => $phone_parsed_values['phone_number'],
                        'country'         => $phone_parsed_values['country']
                    ], true);
            case 'email':
                return CHtml::activeTextField(EmailAddress::model(), 'email', ['class' => 'converted-email', 'value' => trim($owner->value)]);
            default: return parent::columnDisplays($column);
        }
    }
    
    private function parsePhoneNumber($value) 
    {
        $new_value = preg_replace("/[^+0-9]/", '', $value);

        $ret = [
            'country' => Country::model()->findByAttributes([
                'area_code' => '+381' //zakucana je Srbija jer su 99% kontakti koje konvertujemo srpski brojevi pa da olaksa posao
            ])
        ];

        if (strlen($new_value) >= 1 && $new_value[0] === "+")
        {
            $ret['provider_number'] = substr($new_value, 4, 2);
            $ret['phone_number'] = substr($new_value, 6);
        } 
        else if (strlen($new_value) > 1 && $new_value[0] === '0' && $new_value[1] === '0') 
        {
            $ret['provider_number'] = substr($new_value, 5, 2);
            $ret['phone_number'] = substr($new_value, 7);
        }
        else
        {
            $ret['provider_number'] = substr($new_value, 0, 3);
            $ret['phone_number'] = substr($new_value, 3);
        }

        return $ret;
    }
}