<?php

class PersonGUI extends PartnerGUI {

    public function columnLabels() {
        return array(
            'firstname' => Yii::t('Person','FirstName'),
            'lastname' => Yii::t('Person','LastName'),
            'JMBG' => 'JMBG',
            'birthdate' => Yii::t('Person', 'Birthdate'),
            'addresses' => Yii::t('Person', 'Addresses'),
            'professional_degree_id' => Yii::t('Person', 'ProfessionalQualifications'),
            'image_id' => Yii::t('Person', 'Picture'),
            'company_id' => Yii::t('Person', 'EmployedAtCompany'),
            'company' => Yii::t('Person', 'EmployedAtCompany'),
            'iks_license_activated_date' => Yii::t('Person', 'DateOfObtainingIKSLicense'),
            'activePeriod'=>'Zbir IKS poena u aktivnom periodu',
            'lastYear'=>'Zbir IKS poena u poslednjoj godini',
            'national_regulations_and_standards'=>'Oblast nacionalnih propisa i standarda',
            'new_materials_technologies_in_nacional_ind'=>'Oblast novih materijala i tehnologija u nacionalnoj privredi i indrustriji',
            'international_regulations_and_standards'=>'Oblast internacionalnih propisa i standarda',
            'new_materials_technologies_in_internacional_ind'=>'Oblast novih materijala i tehnologija u internacionalnoj privredi i indrustriji',
            'education_title_id' => Yii::t('Person', 'EducationTitle'),
            'education_title' => Yii::t('Person', 'EducationTitle'),
            'middlename' => Yii::t('Person', 'MiddleName'),
            'nickname' => Yii::t('Person', 'Nickname'),
            'password' => Yii::t('Person', 'Password'),
            'comment' => Yii::t('Person', 'Comment'),
            'bank_accounts' => Yii::t('Person', 'BankAccounts'),
            'addresses' => Yii::t('Person', 'Addresses'),
            'contacts' => Yii::t('Person', 'Contacts'),
            'work_positions' => Yii::t('Person', 'WorkPosition'),
            'other_contacts' => Yii::t('Partner', 'OtherContacts'),
            'qualification_level' => Yii::t('HRModule.QualificationLevel', 'QualificationLevel'),
            'qualification_level_id' => Yii::t('HRModule.QualificationLevel', 'QualificationLevel')
        ) + parent::columnLabels();
    }
    
    public function modelLabel($plural = false)
    {
        return Yii::t('Person', $plural?'Persons':'Person');
    }
    
    public function columnDisplays($column) 
    {
        $owner = $this->owner;
        switch ($column) {
            case 'working_until':   return (isset($owner->employee))?$owner->employee->getAttributeDisplay($column):Yii::t('Person', 'NotInTheSystem');
            case 'company_id':      return (isset($owner->company)) ? $owner->company->DisplayName : '';
            case 'username':        return (isset($owner->user)) ? $owner->user->columnDisplays('username') : '?';
            case 'password':      return (isset($owner->user)) ? $owner->user->columnDisplays('password') : '';
            case 'professional_degree_id': return(isset($owner->professional_degree)) ? $owner->professional_degree->name : '';
            case 'image_id':
                $result = SIMAMisc::getPersonDefaultImgSrc();
                if(isset($owner->image) && !$owner->image->isDeleted() && isset($owner->image->last_version_id) && $owner->image->last_version->canDownload())
                {
                    try
                    {
                        $result = $owner->image->last_version->getDownloadLink();
                    }
                    catch(Exception $ex)
                    {
                        Yii::app()->errorReport->createAutoClientBafRequest($ex->getMessage());
                    }
                }
                return $result;
            case 'addresses': $ret = [];
                foreach ($owner->addresses as $address)
                {
                    $ret[] = $address->DisplayHTML;
                }
                return implode(' | ', $ret);
            case 'company': 
                if (empty($owner->company_id))
                {
                    return Yii::t('Person','NotInTheSystem');
                }
                else
                {
                    return parent::columnDisplays($column);
                }
            case 'work_positions': 
                if (count($owner->work_positions)>0)
                {
                    $_positions = [];
                    $_used_positions = [];
                    if (isset($owner->employee) 
                            && isset($owner->employee->active_work_contract)
                            )
                    {
                        $_positions[] = $owner->employee->active_work_contract->work_position->DisplayName;
                        $_used_positions[] = $owner->employee->active_work_contract->work_position_id;
                    }
                    foreach ($owner->work_positions as $_position) 
                    {
                        if (!in_array($_position->id, $_used_positions))
                        {
                            $_positions[] = $_position->DisplayName;
                            $_used_positions[] = $_position->id;
                        }
                    }
                    return implode('<br />', $_positions);
                }
                else
                {
                    return Yii::t('Person','DoNotHaveWorkingPosition');
                }
            default: return parent::columnDisplays($column);
        }
    }

    public function modelViews() {
        $owner = $this->owner;
        return array_merge(parent::modelViews(), array(
//            'default' => array(
////                'style'=>'border: 1px solid;'
//            ),
            'addEmployeeLink'=>array('html_wrapper'=>'span', 'class'=>'data'),
            'addUserLink'=>array('html_wrapper'=>'span', 'class'=>'data'),
            'basic_info' => array('class'=>'basic_info','with'=>array('employee')),
            'basicInfoOrganizationScheme' => array('class'=>'basic_info','with'=>array('employee')),
            'basicInfoTitle' => array('class'=>'top_info'),
            'withOutPosition'=>array('class'=>'person'),
            'personWorkLicenseInfo'=>array('with'=>'work_license'),
            'addresses',
            'contacts',
            'personSafeEvd'=>array('class'=>'sima-layout-panel'),
//            'tabTemplates'=>array('with'=>array('templated_file'),'style'=>'border: 1px solid;')  //.creator
            'addressbook_basic_info',
            'vertical_basic_info_person',
            'expanded_basic_info_contact',
            'expanded_basic_info',
            'inPartner',
            'full_info' => ['html_wrapper' => null],
            'full_info_solo' => ['html_wrapper' => null],
            'left_top_info',
            'left_bottom_cooperation',
            'left_top_info_emplo',
             
            
        ));
    }

    public function modelForms() {
        return array(
            'default' => array(
                'type' => 'file',
                'fileName'=>'default'
//                'columns' => array(
//                    'street' => 'textField',
//                    'number' => 'textField',
//                    'city_id' => array('relation', 'relName' => 'city'),
//                    'partner_id' => 'hidden',
//                )
            ),
            'forWorkLicense' => array(
                'type'=>'generic',
                'columns'=>array(
                    'iks_license_activated_date'=>'datetimeField',
                )
            )
        );
    }
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            case 'legal_employees': return array(
                'columns'=>array('employee_order','firstname','lastname','working_until', 'under_contract'),
            );
            case 'admin_employees': return array(
                'columns'=>array('firstname','lastname'),
            );
            case 'legal': return array(
                'columns'=>array('firstname', 'lastname','comment'),
            );
            case 'iks_points_review': return array(
                'columns'=>array('firstname','lastname','national_regulations_and_standards',
                    'new_materials_technologies_in_nacional_ind','international_regulations_and_standards','new_materials_technologies_in_internacional_ind',
                    'activePeriod','lastYear'
                ),
            );
            default: return array(
                'columns'=>array('firstname', 'lastname', 'company', 'comment'),
            );
        }
    }
    
    public function getDisplayModel()
    {
        return $this->owner->partner;
    }
    
     public function vueProp(string $prop, array $scopes = []):array
    {
        $owner = $this->owner;
        switch ($prop)
        {
            case 'work_position_names':
                $work_position_display_names = [];
                if (!empty($owner->work_positions)) 
                {
                    foreach ($owner->work_positions as $work_position) 
                    {
                        $work_position_display_names[] = $work_position->DisplayName;
                    }
                } 
                else 
                {
                    $work_position_display_names[] = Yii::t('Person', 'DoNotHaveWorkingPosition');
                }
                $return_value = implode(', ', $work_position_display_names);
                return [$return_value, SIMAMisc::getModelTags($owner->work_positions)];
            case 'display_icon': 
                $icon_params = [
                    'type' => SIMAActiveRecordGUI::$MODEL_DISPLAY_ICON_TYPE_ICON,
                    'code' => Person::class
                ];
                
                $img_src = $owner->getImageDownloadUrl();
                if (!empty($img_src))
                {
                    $icon_params = [
                        'type' => SIMAActiveRecordGUI::$MODEL_DISPLAY_ICON_TYPE_IMG,
                        'src' => $img_src
                    ];
                }
                
                //mora da se posalje update tag persona, jer je moguce da neki drugi modeli za vue prop display_icon vrse redirekciju na ovaj model
                return [$icon_params, SIMAMisc::getModelTags($owner)];
            default: return parent::vueProp($prop, $scopes);          
        }

    }
    
    public function vuePropValue($prop)
    {
        $owner = $this->owner;

        switch ($prop)
        {
            case 'image_url': return $owner->columnDisplays('image_id');
            case 'is_director': return $owner->isDirector();
                default: return parent::vuePropValue($prop);
        }
    }
}
