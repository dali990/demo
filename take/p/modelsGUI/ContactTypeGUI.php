<?php

class ContactTypeGUI extends SIMAActiveRecordGUI
{
    public function modelLabel($plural = false)
    {
        return Yii::t('Contact', $plural?'ContactTypes':'ContactType');
    }
}