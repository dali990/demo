<?php

class PartnerToAddressGUI extends SIMAActiveRecordGUI {

    public function modelLabel($plural = false)
    {
        return Yii::t('PartnerToAddress', $plural?'PartnerToAddresses':'PartnerToAddress');
    }
   
    public function modelViews() {
        return array(
            'partners_addresses',
        );
    }


}
