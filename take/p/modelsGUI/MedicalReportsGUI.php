<?php


class MedicalReportsGUI extends SIMAActiveRecordGUI
{
    
    public function guiTableSettings($type)
    {
        switch ($type)
        {
            default:
                return [
                    'columns' => ['name', 'opening_date', 'review_date', 'c6_date', 'description']
                ];
        }
    }
 
}