/* global Vue, sima */

Vue.component('sima-popup', {
    template: '#sima_popup_template',
    props: {
        position: {
            type: String,
            default: 'bottom' //top, right, bottom, left
        },
        show: {
            type: Boolean,
            default: false
        }
    },
    data: function () {
        return {
            element_width: 0,
            element_height: 0,
            popup_style_left: 0,
            popup_style_top: 0,
            popup_style_width: 'auto',
            popup_style_height: 'auto',
            parent_bounding_rect: null
        };
    },
    computed: {
        popup_classes: function(){
            return {
                'sima-popup-vue': true
            };
        },
        popup_display_style: function(){
            var result = 'none';
            if(this.show === true)
            {
                result = 'block';
            }
            return result;
        },
        popup_style: function(){
            return {
                display: this.popup_display_style,
                left: this.popup_style_left + 'px',
                top: this.popup_style_top + 'px',
                width: this.popup_style_width,
                height: this.popup_style_height
            };
        },
        relative_to_el_left: function(){
            return this.parent_bounding_rect.left;
        },
        relative_to_el_top: function(){
            return this.parent_bounding_rect.top;
        },
        relative_to_el_width: function(){
            return this.parent_bounding_rect.width;
        },
        relative_to_el_height: function(){
            return this.parent_bounding_rect.height;
        },
        popup_width: function(){
            return this.$el.offsetWidth;
        },
        popup_height: function(){
            return this.$el.offsetHeight;
        },
        padding: function(){
            return this.element_width;
        }
    },
    mounted: function() {
        var _this = this;
        
        $(this.$el).on('destroyed', function(){
            _this.$destroy();
        });
    },
    methods: {
        setLeftRightAlign: function(){
            var distance_from_left = this.relative_to_el_left + this.relative_to_el_width;
            var distance_from_right = $(window).width() - this.relative_to_el_left;
            
            //ako ima mesta popup ide desno
            if (distance_from_right >= this.popup_width)
            {
                this.popup_style_left = this.relative_to_el_left;
            }
            //ako ima mesta popup ide levo
            else if (distance_from_left >= this.popup_width)
            {
                this.popup_style_left = this.relative_to_el_left - this.popup_width + this.relative_to_el_width;
            }
            //ako nema mesta ni desno ni levo ide gde ima vise mesta
            else if (distance_from_right >= distance_from_left)
            {
                this.popup_style_width = (distance_from_right - this.padding) +'px';
                this.popup_style_left = this.relative_to_el_left;
            }        
            else
            {
                this.popup_style_width = (distance_from_left - this.padding) +'px';    
                
                var new_left = this.relative_to_el_left - this.popup_width + this.relative_to_el_width;
                
                if(new_left < 0)
                {
                    new_left = 0;
                }
                
                this.popup_style_left = new_left;       
            }
        },
        setTopBottomAlign: function(){
            var distance_from_top = this.relative_to_el_top;
            var distance_from_bottom = $(window).height() - this.relative_to_el_top - this.relative_to_el_height;

            //ako ima mesta popup ide dole
            if (distance_from_bottom >= this.popup_height)
            {
                this.popup_style_top = this.relative_to_el_top;
            } 
            //ako ima mesta popup ide gore
            else if (distance_from_top >= this.popup_height)
            {
                this.popup_style_top = this.relative_to_el_top + this.relative_to_el_height - this.popup_height;
            }               
            //ako nema mesta ni dole ni gore ide gde ima vise mesta
            else if (distance_from_bottom >= distance_from_top)
            {
                this.popup_style_top = this.relative_to_el_top;
            }        
            else
            {
                this.popup_style_top = this.relative_to_el_top + this.relative_to_el_height - this.popup_height;
            }
        },
        setBottomPosition: function(){
            var distance_from_top = this.relative_to_el_top;
            var distance_from_bottom = $(window).height() - this.relative_to_el_top - this.relative_to_el_height;

            //ako ima mesta popup ide dole
            if (distance_from_bottom >= this.popup_height)
            {
                this.popup_style_top = this.relative_to_el_top + this.relative_to_el_height;
            }
            //ako ima mesta popup ide gore
            else if (distance_from_top >= this.popup_height)
            {   
                this.popup_style_top = this.relative_to_el_top - this.popup_height;
            }        
            //ako nema mesta ni dole ni gore ide gde ima vise mesta
            else if (distance_from_bottom >= distance_from_top)
            {
                this.popup_style_height = distance_from_bottom - this.padding;
                this.popup_style_top = this.relative_to_el_top + this.relative_to_el_height;
            }        
            else
            {
                this.popup_style_height = distance_from_top - this.padding;
                this.popup_style_top = this.relative_to_el_top - this.popup_height;
            }
        },
        setTopPosition: function(){
            var distance_from_top = this.relative_to_el_top;
            var distance_from_bottom = $(window).height() - this.relative_to_el_top - this.relative_to_el_height;

            //ako ima mesta popup ide gore
            if (distance_from_top >= this.popup_height)
            {
                this.popup_style_top = this.relative_to_el_top - this.popup_height;
            }
            //ako ima mesta popup ide dole
            else if (distance_from_bottom >= this.popup_height)
            {
                this.popup_style_top = this.relative_to_el_top + this.relative_to_el_height;
            }                
            //ako nema mesta ni dole ni gore ide gde ima vise mesta
            else if (distance_from_bottom >= distance_from_top)
            {
                this.popup_style_height = distance_from_bottom - this.padding;
                this.popup_style_top = this.relative_to_el_top + this.relative_to_el_height;
            }        
            else
            {
                this.popup_style_height = distance_from_top - this.padding;
                this.popup_style_top = this.relative_to_el_top - this.popup_height;
            }
        },
        setRightPosition: function(){
            var distance_from_left = this.relative_to_el_left;
            var distance_from_right = $(window).width() - this.relative_to_el_left - this.relative_to_el_width;

            //ako ima mesta popup ide desno
            if (distance_from_right >= this.popup_width)
            {
                this.popup_style_left = this.relative_to_el_left + this.relative_to_el_width;
            }
            //ako ima mesta popup ide levo
            else if (distance_from_left >= this.popup_width)
            {
                this.popup_style_left = this.relative_to_el_left - this.popup_width;
            }
            //ako nema mesta ni desno ni levo ide gde ima vise mesta
            else if (distance_from_right >= distance_from_left)
            {
                this.popup_style_width = (distance_from_right - this.padding) +'px';
                this.popup_style_left = this.relative_to_el_left + this.relative_to_el_width;           
            }        
            else
            {
                this.popup_style_width = (distance_from_left - this.padding) +'px';
                this.popup_style_left = this.relative_to_el_left + this.popup_width;            
            }
        },
        setLeftPosition: function(){
            var distance_from_left = this.relative_to_el_left;
            var distance_from_right = $(window).width() - this.relative_to_el_left - this.relative_to_el_width;

            //ako ima mesta popup ide levo
            if (distance_from_left >= this.popup_width)
            {            
                this.popup_style_left = this.relative_to_el_left - this.popup_width;
            }
            //ako ima mesta popup ide desno
            else if (distance_from_right >= this.popup_width)
            {
                this.popup_style_left = this.relative_to_el_left + this.relative_to_el_width;
            }        
            //ako nema mesta ni desno ni levo ide gde ima vise mesta
            else if (distance_from_right >= distance_from_left)
            {                
                var _padding = this.padding;
                this.popup_style_width = distance_from_right - _padding;
                this.popup_style_left = this.relative_to_el_left + this.relative_to_el_width;
            }        
            else
            {                
                var _padding = this.padding;
                this.popup_style_width = distance_from_left - _padding;
                this.popup_style_left = this.relative_to_el_left + this.popup_width;
            }
        },
        recalculateSubactionsStyleLeftTop: function(){
            if(this.$parent.mounted === false)
            {
                return;
            }

            if (this.position === 'bottom')
            {
                this.setLeftRightAlign();
                this.setBottomPosition();
            }
            else if (this.position === 'top')
            {
                this.setLeftRightAlign();
                this.setTopPosition();
            }
            else if (this.position === 'right')
            {
                this.popup_style_top = this.relative_to_el_top;
                this.setRightPosition();
            }
            else if (this.position === 'left')
            {
                this.setTopBottomAlign();
                this.setLeftPosition();
            }
        }
    },
    watch: {
        show: function(new_show, old_show) {
            if(new_show === old_show)
            {
                return;
            }
            if(new_show === true)
            {
                this.$nextTick(function () {
                    // Code that will run only after the
                    // entire view has been re-rendered
                    if(this.element_width !== this.$el.clientWidth)
                    {
                        this.element_width = this.$el.clientWidth;
                    }
                    if(this.element_height !== this.$el.clientHeight)
                    {
                        this.element_height = this.$el.clientHeight;
                    }

                    var new_parent_bounding_rect = $(this.$el).parent()[0].getBoundingClientRect();
                    if(this.parent_bounding_rect === null || (
                            this.parent_bounding_rect.x !== new_parent_bounding_rect.x
                            || this.parent_bounding_rect.y !== new_parent_bounding_rect.y
                            || this.parent_bounding_rect.width !== new_parent_bounding_rect.width
                            || this.parent_bounding_rect.height !== new_parent_bounding_rect.height
                            || this.parent_bounding_rect.top !== new_parent_bounding_rect.top
                            || this.parent_bounding_rect.right !== new_parent_bounding_rect.right
                            || this.parent_bounding_rect.bottom !== new_parent_bounding_rect.bottom
                            || this.parent_bounding_rect.left !== new_parent_bounding_rect.left
                        ))
                    {
                        this.parent_bounding_rect = new_parent_bounding_rect;
                        this.recalculateSubactionsStyleLeftTop();
                    }
                });
            }
        }
    }
});

