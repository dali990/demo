<script type="text/x-template" id="sima_popup_template">
    <div v-bind:class="popup_classes" v-bind:style="popup_style">
        <slot></slot>
    </div>
</script>