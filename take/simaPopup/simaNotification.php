<script type="text/x-template" id="sima-notification-template">
    <li v-bind:class="notification_classes" id="sima-notification"
        v-on:mouseenter="popup_additional_info = true" 
        v-on:mouseleave.stop.prevent="popup_additional_info = false"
    >
        <p class="_title" v-on:click="onClick">{{model.title}}</p>
        <p class="_info">
            <span class="_icons">
                <span v-for="params_icon in params.icons" v-bind:class="getIconClass(params_icon)"></span>
            </span>
            <span class="_time">{{model.create_time}}</span>
            <sima-popup
                v-if="has_params_info"               
                v-bind:show="popup_additional_info"
                v-bind:position="'right'"
                class="popup-additional-info"
            >
                <div v-html="params.info"></div>
            </sima-popup>
            <input
                class="_check_box"
                v-bind:class="{_disabled: !is_notif_seen_prop_status_ok}"
                title="<?=Yii::t('BaseModule.Notification', 'MarkAsRead')?>" type="checkbox" v-bind:checked="model.seen" @click="seenToggle"
            />
        </p>
    </li>
</script>