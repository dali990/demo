    (function($) {
    var methods = {

        init: function(obj) 
        {     
            var _this = $(this);
            //treba da primam niz parametara
            var schema=obj.date_format;
            var show_seconds=obj.show_seconds;
            _this.data('show_seconds',show_seconds);
            var DELIMITER=[];
            var delimiteri=[];
            var shcema_length=schema.length;
            _this.attr('maxLength',schema.length+1);
            //var out_delimiter;
            _this.data('schema',schema);
            //takeing position of delimiters and their own values , puting in the array
            for(var i=0;i<shcema_length;i++){
                var code=schema[i].charCodeAt(0);
                if( (code<=48 || (code>=57 && code<=65 ) || code>=122)){                          
                    DELIMITER.push({del:schema[i],pos:i});
                    delimiteri.push(schema[i]);
                    }
              }
            _this.data('delimiters',delimiteri);
            _this.data('isCtrl', false);
            _this.on('keydown', keydownEvent);
            _this.on('keyup', keyupEvent);
            _this.bind('input',{_this: _this }, liveTypeing);
            _this.on("click", {_this: _this }, function() {

            });
            // to focuse when you click 1st time on input field
            _this.on('blur', focuseOutValidation);
        }
    };

    function focuseOutValidation()
    {
        var _this=$(this);
        if((_this.val().length === _this.data('schema').length))
        {
            if ( is_valid_date(_this.val(),_this)) 
            {
                _this.css('background', '#dfd');
            } else {
                _this.css('background', '#fdd');
            }
        }
        else 
        {
            _this.css('background', '#fdd');
        }
       
        if(_this.data('show_seconds')===false){
            var index=_this.data('schema').indexOf('s');
            if(index !== -1)
            {
                _this.val(replace2At(index,'00',_this.val()));
            }
        }
    }
    function replace2At(index, replacement,str) {
        return str.substr(0, index) + replacement+ str.substr(index + replacement.length);
    }
    function keydownEvent(event) 
    {

        var _this = $(this);
        var kursor_position = _this.prop("selectionStart");
        var event_code = event.which;
        var schema=_this.data('schema');
        var input=_this.val();

        if(kursor_position+1<=input.length )
        {
            if(event_code === 190 || event_code === 191 || event_code === 110)
                return false;
        }              
        if(event_code === 190 || event_code === 191 || event_code === 110)
        {
            if( input.slice(-1) === '.' || input.slice(-1) === '/' || input.slice(-1) === ' ' ){
                return false;
            }
            else if(
                schema[kursor_position] === '.' ||
                schema[kursor_position] === ' '
            ){}
            else if(schema[kursor_position] === 'y')
                return false;
            else if(input.length>=10)
                return false;
        }

        if (event.which === 17) //ctrl
        {
            _this.data('isCtrl', true);
        }

        if(is_valid_charachter( event_code) ||
        event_code === 8 || ( event_code === 37  ||  event_code === 39) ||
        event_code === 190 ||  event_code === 110 || event_code === 191 ||
        (_this.data('isCtrl') === true &&
        (event_code === 65 || event_code === 67 || event_code === 86 || event_code === 88)) )
        {
            if(input.length === 0 && (event_code === 190 || event_code === 110 || event_code === 191))
            {
                return false;
            }
        }
        else
        {
            return false;
        }
        if(event_code === 37  ||  event_code === 39)
        {
            return true;
        }

        if(is_true(input[kursor_position]) && kursor_position<input.length && event_code !== 8)
        {
            _this.get(0).setSelectionRange(kursor_position+1, kursor_position+1);
            kursor_position++;
            if(input.length>11 && kursor_position === 11)
            {
                    _this.val(input.slice(0,kursor_position+1)+input.slice(kursor_position+2,input.length));
                    _this.get(0).setSelectionRange(kursor_position, kursor_position);
            }
        }

        // skiping white space
        if(kursor_position === 11 && event_code !== 37)
        {
            _this.get(0).setSelectionRange(kursor_position+1, kursor_position+1);
        }
        // skiping the delimiter
        if(is_true(input[kursor_position-1]) && kursor_position !== input.length && event_code === 8)
        {
            _this.get(0).setSelectionRange(kursor_position-1, kursor_position-1);
            return false;
        }//slice 2 last digits
        else if( is_true(input[kursor_position-1]) && kursor_position === input.length  && event_code === 8)
        {
            _this.val(input.slice(0,input.length-1));
        }
        // backspace puting 0
        else if( !is_true(input[kursor_position-1])
        && kursor_position<input.length && event_code === 8 && kursor_position !== 0 )
        {
            var char=_this.data('schema')[kursor_position-1];
            _this.val(input.slice(0,kursor_position-1)+char+input.slice(kursor_position,input.length));
            _this.get(0).setSelectionRange(kursor_position-1, kursor_position-1);
            return false;
        }
        else if(!is_true(input[kursor_position]) && kursor_position<input.length && is_valid_charachter( event_code) )
        {
            _this.val(input.slice(0,kursor_position)+input.slice(kursor_position+1,input.length));
            _this.get(0).setSelectionRange(kursor_position, kursor_position);
        }
    }

    function keyupEvent(event) {
        var _this=$(this);
        if (event.which === 17)
        {
            _this.data('isCtrl', false);
        }
    }

    function liveTypeing() 
    {
        var _this = $(this);
        var value=_this.val();
        var value_length=value.length;
        var schema=_this.data('schema');
        var kursor_position = _this.prop("selectionStart");
        var resenje=_this.val();

        //last digit
        if (value.length > schema.length) 
        {
            resenje = value.slice(0, value_length - 1);
        }

        if(kursor_position === value.length)
        {
            if(is_true(schema[kursor_position-1]) &&  is_true(schema[kursor_position]) && !is_true(value[kursor_position-1]))
            {
                resenje=repair(value,schema[kursor_position-1]);
                kursor_position+=2;
            }
            else if(is_true(schema[kursor_position-1]) && !is_true(value[kursor_position-1])   )
            {
                // 123 -> 12.3
                resenje=dot_repair(value,schema[kursor_position-1]);
                kursor_position++;
            }
            else if(is_true(schema[kursor_position]) && is_true(resenje[kursor_position-1]) && value.length<=10)
            {
                //  1. -> 01.
                resenje=short_dot(value);
                kursor_position++;
            } 
            else if (10 === value.length && kursor_position === 10) 
            {
                resenje += schema[10];
                kursor_position++;
            }
        }
        _this.val(resenje);
        _this.get(0).setSelectionRange(kursor_position, kursor_position);
    }

    function dot_repair(input,sep)
    {
        return input.slice(0,input.length-1)+sep+input.slice(input.length-1);
    }
    function short_dot(input)
    {
        return input.slice(0,input.length-2)+"0"+input.slice(input.length-2);
    }
    function repair(input,sep)
    {
        var slovo=input.slice(input.length-1);
        return input.slice(0,input.length-1)+sep+" "+slovo;
    }
    function is_true(value)
    {
        if(value === '.' || value === ':' || value === ' ' || value === '/' || value === '.')
            return true;
        else false;
    }

    function is_valid_charachter(charCode) 
    {
        if((charCode>=48 && charCode<=57) || (charCode>=96 && charCode<=105) )
        {
            return true;
        }
        return false;
    }

    function is_valid_date(value,_this) 
    {
        var schema=_this.data('schema');
        if(schema.length<12)
            var matches = value.match(/^(\d{2})\.(\d{2})\.(\d{4}).$/);
        if(schema.length>13)
            var matches = value.match(/^(\d{2})\.(\d{2})\.(\d{4}). (\d{2}):(\d{2}):(\d{2})$/);// (\d{2}):(\d{2}):(\d{2})
        if (matches === null) 
        {
            return false;
        } 
        else
        {
            if(value.length>13)
            {
                var year = parseInt(matches[3], 10);
                var month = parseInt(matches[2], 10) - 1; // months are 0-11
                var day = parseInt(matches[1], 10);
                var hour = parseInt(matches[4], 10);
                var minute = parseInt(matches[5], 10);
                var second = parseInt(matches[6], 10);
                var date = new Date(year, month, day, hour, minute, second);
                if (date.getFullYear() !== year
                || date.getMonth() !== month
                || date.getDate() !== day
                || date.getHours() !== hour
                || date.getMinutes() !== minute
                || date.getSeconds() !== second
                ) 
                {
                    return false;
                } 
                else 
                {
                    return true;
                }
            }
            else
            {
                var year = parseInt(matches[3], 10);
                var month = parseInt(matches[2], 10) - 1; // months are 0-11
                var day = parseInt(matches[1], 10);
                var date = new Date(year, month, day);
                if (date.getFullYear() !== year
                    || date.getMonth() !== month
                    || date.getDate() !== day)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }
    }

    $.fn.simaDateInputField = function(methodOrOptions) 
    {
        var ar = arguments;
        var i = 1;
        var ret = this;
        this.each(function() 
        {
            i++;

            if (methods[methodOrOptions]) 
            {
                var povr = methods[methodOrOptions].apply($(this), Array.prototype.slice.call(ar, 1));
                if (typeof povr !== 'undefined') {
                    ret = povr;
                }
                return;
            } 
            else if (typeof methodOrOptions === 'object' || !methodOrOptions) 
            {
                // Default to "init"
                return methods.init.apply($(this), ar);
            } 
            else 
            {
                $.error('Method ' + methodOrOptions + ' does not exist on jQuery.simaDateInputField');
            }
        });
        return ret;
    };
})(jQuery);