
/* global sima */

function SIMAReport()
{
    this.downloadPDF = function(report_class, report_params)
    {
        downloadReport('base/SIMAReport/downloadPDF', report_class, report_params);
    };
    
    this.downloadXML = function(report_class, report_params)
    {
        downloadReport('base/SIMAReport/downloadXML', report_class, report_params);
    };
    
    function downloadReport(action, report_class, report_params)
    {
        if (sima.isEmpty(report_params))
        {
            report_params = {};
        }
        
        var ajax_long_params = {
            data: {
                report_class: report_class,
                report_params: report_params
            },
            onEnd: function(response) {                                        
                sima.temp.download(response.filename, response.downloadname);
            }
        };
        
        if (!sima.isEmpty(report_params.loading_object_selector))
        {
            ajax_long_params.showProgressBar = $(report_params.loading_object_selector);
        }

        sima.ajaxLong.start(action, ajax_long_params);
    }
}
