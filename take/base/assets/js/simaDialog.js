
/* global sima */

/**
 * open(html,title,params) - params {close_func}
 * openYesNo(question, yes_function, no_function, data);
 * openAJAX(codename, data) - POST data
 * html(html) - update html in top opend Dialog
 * @returns 
 */
function SIMADialog() 
{
    var dialog_full_screen_margins = 40;//ovo su dve margine
    
    this.AutoHeight = function()
    {
        $('.sima-dialog-position-full-screen').each(function() {
            $(this).dialog("option", "width", sima.getScreenWidth() - dialog_full_screen_margins);
            $(this).dialog("option", "height", sima.getScreenHeight() - dialog_full_screen_margins);
            $(this).dialog('option', 'position', [dialog_full_screen_margins / 2, dialog_full_screen_margins / 2]);
        });

        $('.sima-dialog-position-center').each(function() {
            $(this).dialog('option', 'position', "left top");
        });
    };
    
    this.html = function(html)
    {
        if (!dialog.hasClass('sima-dialog-position-full-screen'))
        {
            dialog.dialog("option", "height", 'auto');
            dialog.dialog("option", "width", 'auto');
        }
        dialog.html(html);
        var _height = dialog.parent().height();//uzima se parent, posto je to ono sto je vidljivo i to je ono sto se setuje
        var _width = dialog.parent().width();
        if (_height>(sima.getScreenHeight() - dialog_full_screen_margins))
        {
            _height = sima.getScreenHeight() - dialog_full_screen_margins;
            dialog.dialog("option", "height", _height);
        }
        if (_width>(sima.getScreenWidth() - dialog_full_screen_margins))
        {
            _width = sima.getScreenWidth() - dialog_full_screen_margins;
            dialog.dialog("option", "width", _width);
        }
        //sad se mere velicine koje se salju
        _height = dialog.height();
        _width = dialog.width();
        
        sima.layout.setLayoutSize(dialog.children('.sima-layout-panel'),_height,_width,'TRIGGER_dialog_html');
    };
    
    
    var dialog = null;
    var dialogs_stack = new Array();
    
    /**
     * 
     * @param {type} html
     * @param {function} yes_function
     * @param {function} no_function
     * @param {object} data
     * @param {function} close_function
     * @param {boolean} fullscreen
     */
    this.openYesNo = function(html, yes_function, no_function, data, close_function, fullscreen)
    {
        var yes_func = false;
        var yes_hidden = false;
        var no_func = false;
        var no_hidden = false;
        var yes_title = 'Da';
        var no_title = 'Ne';
        var title = '';
        var content = '';
        var params = {};
        
        if (typeof(html) === 'object')
        {
            if (typeof html.title !== 'undefined')
            {
                title = html.title;
            }
            if (typeof html.html !== 'undefined')
            {
                content = html.html;
            } 
            if (typeof html.params !== 'undefined')
            {
                params = html.params;
            } 
        }
        else
        {
            content = html;
        }
        
        if (!sima.isEmpty(close_function) && typeof close_function === 'function')
        {
            params.close_func = close_function;
        }
        
        if (yes_function !== null && yes_function !== '' && typeof yes_function !== 'undefined')
        {
            //prvo proveravamo da li su poslati parametri za yes funkciju i ako jesu obradjujemo ih
            if (typeof(yes_function) === 'object')
            {
                if (typeof yes_function.func !== 'undefined')
                {
                    yes_func = yes_function.func;
                }
                if (typeof yes_function.hidden !== 'undefined')
                {
                    yes_hidden = yes_function.hidden;
                }
                if (typeof yes_function.title !== 'undefined')
                {
                    yes_title = yes_function.title;
                }
            }
            else if (typeof(yes_function) === 'function')
            {
                yes_func = yes_function;
            }
        }
        
        if (no_function !== null && no_function !== '' && typeof no_function !== 'undefined')
        {
            //zatim proveravamo da li su poslati parametri za no funkciju i ako jesu obradjujemo ih
            if (typeof(no_function) === 'object' && no_function !== null)
            {
                if (typeof no_function.func !== 'undefined')
                {
                    no_func = no_function.func;
                }
                if (typeof no_function.hidden !== 'undefined')
                {
                    no_hidden = no_function.hidden;
                }
                if (typeof no_function.title !== 'undefined')
                {
                    no_title = no_function.title;
                }
            }
            else if (typeof(no_function) === 'function')
            {
                no_func = no_function;
            }
        }
        
        if (yes_func != false)
        {
            var uniqid = 'yes_no_id' + sima.uniqid();
            var yes_span = '';
            var no_span = '';
            if (yes_hidden == false)
            {
                yes_span = "<span class='sima-dialog-button _yes-left'>"+yes_title+"</span>";
            }
            if (no_hidden == false)
            {
                no_span = "<span class='sima-dialog-button _no-right'>"+no_title+"</span>";
            }
            
            var html = "<div id='"+uniqid+"' class='sima-layout-panel _horizontal'>"
                        +"<div class='sima-layout-panel sima-ui-dialog-part'>"+content+"</div>"
                        +"<div class='sima-layout-fixed-panel'>"+yes_span+no_span+"</div></div>";
            
            if(fullscreen === true)
            {
                sima.dialog.openInFullScreen(html, title, params);
            }
            else
            {
                open(html, title, params);
            }
            
            var data_temp = (!sima.isEmpty(data) && typeof(data) === 'object') ? data : {};
            $('div#' + uniqid).on('click', 'span.sima-dialog-button._yes-left', data, yes_func);
            

            if (no_func != false)
            {
                $('div#' + uniqid).on('click', 'span.sima-dialog-button._no-right', data_temp, no_func);
            }
            else
            {
                $('div#' + uniqid).on('click', 'span.sima-dialog-button._no-right', sima.dialog.close);
            }
            
            dialog.addClass('sima-ui-yes_no-dialog');
            
            return;
        } else sima.dialog.openCustom('sima.dialog.openYesNo - Niste zadali yes_function kao funkciju','error');
    };
    
    /**
     * 
     * @param {type} title
     * @param {type} ok_function
     * @param {type} params
     * @returns {undefined}
     */    
    this.openPrompt = function(title,ok_function,params)
    {
        
        if (typeof(ok_function) !== 'undefined')
        {
            if (typeof(title)!=='string') { title=''; }
            
            var uniq = sima.uniqid();
            var html='';
            var text='';
            var type='input';
            if(typeof(params)==='undefined') { params={};}
            
            if(typeof(params.type)!=='undefined')
            {
                type=params.type;
            }
            
            if (typeof(params.html)==='undefined')
            {
                if(type==='input')
                { 
                    var def_value= (typeof(params.default_option)!== 'undefined') ? params.default_option:'';
                    html="<input id='"+uniq+"'  type='text' value='"+def_value+"'/>";
                }
                else if(type==='dropdown')
                {
                    if (typeof(params.options)!== 'object')
                    {
                        sima.dialog.openCustom('sima.dialog.openPrompt - Niste zadali dobre parametre, tip je '+(typeof(params.options)),'error');
                    }
                    else
                    {
                        var options=params.options, opt_cnt = options.length, default_option='';
                        if (typeof(params.default_option)!== 'undefined') { default_option=params.default_option;}
                        html='<select id="'+uniq+'">';
                        for(var i = 0; i< opt_cnt; i++)
                        {
                            var selected='';
                            if(options[i].title.trim()===default_option.trim()) {selected="selected='selected'";}
                            html+='<option class="sima-dialog-prompt-option" value="'+options[i].value+'"'+selected+' >'+options[i].title+'</li>';
                        }
                        
                        html+='</select>';
                    }
                                   
                }
            }
            else
            {
                html=params.html;
                if (typeof(params.uniq)!=='undefined') { uniq=params.uniq; }
            }
            
            if (typeof(params.text)!=='undefined') { text=params.text; }

            var uniqid = 'prompt_id' + sima.uniqid();
            open("<div id='"+uniqid+"' class='open'><p class='sima-ui-dialog-part'>"+text+"</p><p class='sima-ui-dialog-part'>"+html+"</p>\n\
                    <span class='sima-dialog-button _ok-left'>U redu</span><span class='sima-dialog-button _no-right'>Otkaži</span></div>", title);
            
            $('div#' + uniqid).on('click', 'span.sima-dialog-button._ok-left', function(){
                     var input_value='';
                     if(type==='input')
                     {
                        input_value= $("input#"+uniq).val();
                     }
                     else if(type==='dropdown')
                     {
                         var value=$("select#"+uniq).find(":selected").val();
                         var display=$("select#"+uniq).find(":selected").text();
                         input_value={value:value, display:display};
                     }
                     else if(type==='searchField')
                     {
                         var value=$("#"+uniq).find('input.sima-ui-sf-input').val();
                         var display=$("#"+uniq).find('input.sima-ui-sf-display').val();
                         input_value={value:value, display:display};
                     }
                     sima.dialog.close();
                     
                     (typeof(params.init_data)!=='undefined')?
                         ok_function(input_value, params.init_data):
                         ok_function(input_value);

            });
            $('div#' + uniqid).on('click', 'span.sima-dialog-button._no-right', sima.dialog.close);
            
            dialog.addClass('sima-ui-prompt-dialog');

            
        } 
        else
        {
            sima.dialog.openCustom('sima.dialog.openPrompt - Niste zadali ok_function funkciju','error');
        }
    };
    
    this.openModelForm = function(html, fullscreen, params)
    {
        if (typeof fullscreen !== 'undefined' && fullscreen)
        {
            sima.dialog.openInFullScreen(html, null, params);
        }
        else
        {
            open(html, null, params);
        }
    };
    
    /**
     * 
     * @param {type} html
     * @param {type} title
     * @param {type} params
     */
    this.openCustom = function(html,title, params)
    {
        if (typeof(title)!=='string') { title=''; }
        if (typeof(params)!=='object') params = {};
        if(html===undefined) {  html=''; }
        
        var dialog = open(html, title, params);
        dialog.addClass('sima-ui-custom-dialog');
        
        return dialog;
    };
    
    var openAlert = function(html, title, params)
    {
        if (typeof(title)==='string') title='SIMA - '+title; else title = 'SIMA';
        var ok_function = function(){};
        var close_function = function(){};
        var modal = false;
        if (typeof(params)!=='object') params = {};
        if (typeof(params.function)==='function') ok_function = params.function;
        if (typeof(params.close_function)==='function') 
        {
            close_function = params.close_function;
        }
        if (typeof(params.modal)!=='undefined') modal = params.modal;
        var button_title = (typeof(params.title)==='string') ? params.title:'OK';
        var additional_classes = (typeof(params.additional_classes)==='string') ? params.additional_classes:'';
        
        var uniqid = 'alert_dialog_' + sima.uniqid();
        
    	var _dialog = $("<div id='"+uniqid+"'  class='sima-ui-alert-dialog "+additional_classes+"'></div>");
        var _display = $("<div class='scroll-container'></div>");
        var _button = $("<span class='sima-dialog-button _ok-left'>"+button_title+"</span>");
        _button.on('click',function(){
            $("#"+uniqid).dialog("close");
            ok_function();
        });

    	_dialog.dialog({ 
            title: title, 
            width : 'auto', 
            height : 'auto', 
            modal : modal ,
            resizable: false
            
        });
        _display.html(html);
        _display.append(_button);
        _dialog.append(_display);
    	
        if (_dialog.height()>(sima.getScreenHeight() - dialog_full_screen_margins))
            _dialog.dialog("option", "height", sima.getScreenHeight() - dialog_full_screen_margins);
        if (_dialog.width()>(sima.getScreenWidth() - dialog_full_screen_margins))
            _dialog.dialog("option", "width", sima.getScreenWidth() - dialog_full_screen_margins);
        setDialogPosition(_dialog, 'center');        
        
//        sima.refreshChildrenForce(_dialog);
        
        _dialog.unbind( "dialogclose" );
    	_dialog.bind( "dialogclose", function(event, ui) {
            $(this).remove();
            close_function();
        });
        return _dialog;
    };
    
    this.openInfo = function(message, title, params)
    {
        var info_title='';
        if(typeof(title)!=='undefined') info_title=title;
        var icon="<span class='sima-ui-dialog-info-icon'>&nbsp;</span>";
        var title=sima.translate('DialogTitleNotice')+" - "+info_title;
        var html = "<p class='sima-ui-dialog-part'>"+icon+"  "+message+"</p>";
        return openAlert(html, title, params);
    };
    
    this.openWarn = function(message, title, params)
    {
        if (typeof title === 'undefined')
        {
            title = '';
        }
        var icon="<span class='sima-ui-dialog-warning-icon'>&nbsp;</span>";
        var title=sima.translate('Warning')+"! - "+title;
        var html = "<p class='sima-ui-dialog-part'>"+icon+"  "+message+"</p>";
        return openAlert(html, title, params);
    };
    
    this.openError = function(error_id, message)
    {
        sima.errorReport.addErrorDesc(error_id, message);
    };
   
    this.openAlert = function(html, title, params)
    {
        openAlert(html, title, params);
    };
    
    var open=function(html, title, params)
    {
        if (typeof(title)==='string') title='SIMA - '+title; else title = 'SIMA';
        if (typeof(params)!=='object') params = {};
        if (typeof(params.fullscreen)!=='boolean') params.fullscreen = false;
        
        var close_func = function(){};
        const params_close_func_type = typeof(params.close_func);
        if(!sima.isEmpty(params.close_func))
        {
            if (params_close_func_type==='function')
            {
                close_func = params.close_func;
            }
            else if (params_close_func_type==='string')
            {
                close_func = function(func_params){
                    sima.callFunction(params.close_func);
                };
            }
            else if (params_close_func_type==='array' || params_close_func_type==='object')
            {
                close_func = params.close_func;
            }
            else
            {
                close_func = function(func_params){
                    sima.callFunction(params.close_func, func_params);
                };
            }
        }
       
        var uniq = sima.uniqid();
        dialogs_stack.push(dialog);
    	$('#dialogs_stack').append("<div></div>");
    	dialog = $('#dialogs_stack div:last');
    	dialog.attr('id', uniq+'dialogs_stack-'+dialogs_stack.length);
    	
        var _height = 'auto';
        var _width = 'auto';
        var _position = 'center';
        var _max_height = sima.getScreenHeight() - dialog_full_screen_margins;
        var _max_width = sima.getScreenWidth() - dialog_full_screen_margins;
        if (params.fullscreen)
        {
            _height = _max_height;
            _width = _max_width;
            _position = [dialog_full_screen_margins / 2, dialog_full_screen_margins / 2];
        }
        
    	dialog.dialog({ 
            title: title, 
            height : _height,
            width : _width,
            modal : true,
            position: _position,
            resizable: false,
            //SASA A. Open sam morao da dodam jer ako je dialog modal, onda kad dodam simaPopup body-iju, ne moze da se fokusira nijedan input unutar simaPopup-a.
            //Zakomentarisani kod ispod sam nasao na netu, ali da ne bi selektovao input, jer moze da ne radi i za nesta drugo, stavio sam samo return true i to radi.
            open: function () {
//                jQuery(this).closest( ".ui-dialog" ).find(":button").blur(); /// predlog resenja da nema fokusa na X na otvorenom dialogu
                $.ui.dialog.prototype._allowInteraction = function (e) {
                    return true;                    
//                    if ($(e.target).closest('input').length)
//                    {
//                        return true;
//                    }
//                    return ui_dialog_interaction.apply(this, arguments);
                };
                $.ui.dialog.prototype._allowInteractionRemapped = true;
            }
        });
        dialog.css('max-height',_max_height);
        dialog.css('max-width',_max_width);
        if (params.fullscreen)
        {
            dialog.addClass('sima-dialog-position-full-screen');
        }

    	sima.dialog.html(html);
        setDialogPosition(dialog, 'center');
        dialog.data('close_func_params',{});
        dialog.unbind( "dialogclose" );
    	dialog.bind( "dialogclose", function(event, ui) {
            var close_data = dialog.data('close_func_params');
            var old_dialog = dialog;
            dialog = dialogs_stack.pop();
            old_dialog.remove();
            const close_func_type = typeof close_func;
            if(close_func_type === 'function')
            {
                close_func(close_data);
            }
            else if(close_func_type === 'string')
            {
                sima.callFunction(close_func);
            }
            else if(close_func_type === 'array' || close_func_type === 'object')
            {
                close_func.forEach(function(cf) {
                    const cf_type = typeof cf;
                    if(cf_type === 'function' || cf_type === 'object')
                    {
                        sima.callFunction(cf, close_data);
                    }
                    else if(cf_type === 'string')
                    {
                        sima.callFunction(cf);
                    }
                    else
                    {
                        const error_message = 'nepoznat tip: '+cf_type+' za close_func: '+JSON.stringify(cf);
                        console.log('simaDialog - open: '+error_message);
        //                sima.addError('simaDialog - open', error_message);
                    }
                });
            }
            else
            {
                const error_message = 'nepoznat tip: '+close_func_type+' za close_func: '+JSON.stringify(close_func);
                console.log('simaDialog - open: '+error_message);
//                sima.addError('simaDialog - open', error_message);
            }
            sima.removeActiveFunctionalElement(old_dialog);
        });
        sima.pushActiveFunctionalElement(dialog);
        return dialog;
    };
   
    var close = function(params) {
        if (typeof(params)==='undefined') params={};
        dialog.data('close_func_params',params);
        dialog.dialog("close");
    };
    this.close = close;
    this.closeYesNo = close;

    
    
    /**
     * 
     * @param {html} question - pitanje
     * @param {array} options - niz objekata koje u sebi imaju title i funkction
     * @returns {undefined}
     */
    this.openChooseOption = function(question, options)
    {
        if (typeof options !== 'object')
        {
            sima.dialog.openCustom('sima.dialog.openChooseOption - Niste zadali dobre parametre, tip je '+(typeof options),'error');
        }
        else
        {
            var uniqid = 'choose_option_id' + sima.uniqid();
            var html = $('<div id="' + uniqid + '"></div>');
            html.append('<p class="sima-ui-dialog-part sima-dialog-choice-question">'+question+'</p><ul style="display:inline-block;list-style-type: none;"></ul>');
            var opt_cnt = options.length;
            for(var i = 0; i< opt_cnt; i++)
            {
                if (typeof options[i].data === 'undefined')
                {
                    options[i].data = null;
                }
                var choise = $('<li class="sima-dialog-choice-option link">'+options[i].title+'</li>')
                        .on('click',options[i].data,options[i].function);
                html.find('ul').append(choise);
            }
            open(html);
        }
    };
    
    this.openModel = function(action, model_id, selector, params)
    {
        if (typeof params === 'undefined')
        {
            params = {};
        }
        
        if(typeof params.displayhtml !== 'undefined' && params.displayhtml === true)
        {
            /// da li otvarati u novom tabu
            if(
                    (sima.getModelDisplayhtmlClickOpenConf() === 'ctrl+klik' && sima.keysPressed.ctrl === true && sima.keysPressed.shift === true)
                    ||
                    (sima.getModelDisplayhtmlClickOpenConf() === 'klik' && sima.keysPressed.ctrl === true)
                )
            {
                if (typeof params.event !== 'undefined')
                {
                    params.event.stopPropagation(); 
                }
                sima.mainTabs.openNewTab(action, model_id);
                return;
            }
            else if(sima.getModelDisplayhtmlClickOpenConf() === 'ctrl+klik' && sima.keysPressed.ctrl !== true)
            {
                return;
            }
        }
        
        var data = {};
        //kod koji pokriva otvaranje starih poruka koje su se nalazile u profilu. Sad su poruke izbacene u novom tabu.
        //Treba da se izbrise vremenom
        if (action === 'addressbook/partner')
        {
            if (typeof selector !== 'undefined' && !sima.isEmpty(selector))
            {
                $.each(selector, function(index, element) {
                    if (element.simaTabs === 'messages')
                    {
                        action = 'comments';
                        model_id = undefined;
                    }
                });
            }
        }

        var get_params = {id:model_id};
        if (typeof model_id === 'object')
        {
            get_params = model_id;
        }
            
        if (typeof selector !== 'undefined')
        {
            data.selector = selector;
        }
        
        var ajax_params = {
            get_params:get_params,
            data: data,
            async: true,
            success_function:function(response){
                sima.dialog.openInFullScreen(response.html);
                var pin = $('<a class="ui-dialog-titlebar-maximize ui-corner-all" href="#" role="button">\n\
                                                                    <span class="ui-icon ui-icon-maximize">restore</span>\n\
                                                                    </a>');
                dialog.siblings('.ui-dialog-titlebar').append(pin);
                pin.on('click','.ui-icon-maximize', function(events, ui){
                       sima.mainTabs.openNewTab(action, model_id, selector);
                       sima.dialog.close();
                });
                
                if ((typeof response.selected === 'undefined') || (!response.selected))
                    setTimeout(function() {
                        sima.selectSubObj(dialog, selector);
                    }, 200);
            }
        };
        
        if (typeof params.failure_function !== 'undefined')
        {
            ajax_params.failure_function = params.failure_function;
        }

        if (typeof params.event !== 'undefined')
        {
            params.event.stopPropagation(); 
        }

        sima.ajax.get(action, ajax_params);
    };
    
    this.openAJAX = function(codename, data)
    {
        sima.ajax.get('site/customDialog',{
            get_params:{codename:codename},
            data:data,
            success_function:function(response){
                open(response.html);
            }
        });
    };
    
    this.openInFullScreen = function(html,title,params)
    {
        if (typeof(params)!=='object') params = {};
        params.fullscreen = true;
        var dialog = open(html,title,params);
        return dialog;
    };
    
    
    this.openAction = function(action, model_id)
    {
        sima.ajax.get(action,{
            get_params:{id:model_id},
            success_function:function(response){
                sima.dialog.openInFullScreen(response.html);
                var pin = $('<a class="ui-dialog-titlebar-maximize ui-corner-all" href="#" role="button">\n\
                                                                    <span class="ui-icon ui-icon-maximize">restore</span>\n\
                                                                    </a>');
                dialog.siblings('.ui-dialog-titlebar').append(pin);
                pin.on('click','.ui-icon-maximize', function(events, ui){
                       sima.mainTabs.openNewTab(action, model_id);
                       sima.dialog.close();
                });
            }
        });
    };
    
    /**
     * ista kao this.openAction samo poziva ajax async
     * @param {type} action
     * @param {type} model_id
     * @returns {undefined}
     */
    this.openActionAsync = function(action, model_id)
    {
        sima.ajax.get(action,{
            get_params:{id:model_id},
            async: true,
            loadingCircle: $('#body_sync_ajax_overlay'),
            success_function:function(response){
                sima.dialog.openInFullScreen(response.html);
                var pin = $('<a class="ui-dialog-titlebar-maximize ui-corner-all" href="#" role="button">\n\
                                                                    <span class="ui-icon ui-icon-maximize">restore</span>\n\
                                                                    </a>');
                dialog.siblings('.ui-dialog-titlebar').append(pin);
                pin.on('click','.ui-icon-maximize', function(events, ui){
                       sima.mainTabs.openNewTab(action, model_id);
                       sima.dialog.close();
                });
            }
        });
    };
    
    this.openActionInDialog=function(action, params, selector)
    {
        var get_params=(typeof(params)!=='undefined' && typeof(params.get_params)!=='undefined')?params.get_params:{};
        var post_params=(typeof(params)!=='undefined' && typeof(params.post_params)!=='undefined')?params.post_params:{};
        sima.ajax.get(action,{
            get_params:get_params,
            data: post_params,
            success_function:function(response){
                sima.dialog.openInFullScreen(response.html, response.title, params);
                var pin = $('<a class="ui-dialog-titlebar-maximize ui-corner-all" href="#" role="button">\n\
                                                                    <span class="ui-icon ui-icon-maximize">restore</span>\n\
                                                                    </a>');
                dialog.siblings('.ui-dialog-titlebar').append(pin);
                pin.on('click','.ui-icon-maximize', function(events, ui){
                    sima.mainTabs.openNewTab(action, get_params, selector);
                    sima.dialog.close();
                });
            }
        });
    };
    
    /**
     * ista kao this.openActionInDialog samo poziva ajax async
     * @param {type} action
     * @param {type} params
     * @param {type} selector
     * @returns {undefined}
     */
    this.openActionInDialogAsync=function(action, params, selector)
    {
        var get_params=(typeof(params)!=='undefined' && typeof(params.get_params)!=='undefined')?params.get_params:{};
        var post_params=(typeof(params)!=='undefined' && typeof(params.post_params)!=='undefined')?params.post_params:{};
        sima.ajax.get(action,{
            get_params:get_params,
            data: post_params,
            async: true,
            loadingCircle: $('#body_sync_ajax_overlay'),
            success_function:function(response){
                sima.dialog.openInFullScreen(response.html, response.title, params);
                var pin = $('<a class="ui-dialog-titlebar-maximize ui-corner-all" href="#" role="button">\n\
                                                                    <span class="ui-icon ui-icon-maximize">restore</span>\n\
                                                                    </a>');
                dialog.siblings('.ui-dialog-titlebar').append(pin);
                pin.on('click','.ui-icon-maximize', function(events, ui){
                    sima.mainTabs.openNewTab(action, get_params, selector);
                    sima.dialog.close();
                });
            }
        });
    };
    
    /**
     * 
     * @param {string} title - Tekst koji ce se prikazati u zaglavlju dialoga
     * @param {function} ok_function - funkcija koja ce se pozvati kada se klikne na sacuvaj
     * @param {string} init_html - inicijalni tekst koji ce se prikazati prilikom otvaranja tinymce-a
     * @param {array} options - dodatni parametri     
     */
    this.openTinyMce = function(title, ok_function, init_html, options)
    {
        var _init_html = (typeof init_html !== 'undefined')?init_html:'';
        sima.ajax.get("base/base/renderTinyMce", {
            data: {
                init_html:_init_html,
                options: options
            },
            success_function:function(response){
                var uniq = sima.uniqid();
                var wrap = $("<div id='tiny_mce_wrap"+uniq+"' class='sima-layout-panel'></div>");
                var ok_button = $("<span class='tiny_mce_ok sima-dialog-button _yes-left'>Sačuvaj</span>");
                wrap.append(ok_button);
                wrap.on('click', '.tiny_mce_ok', function(){
                    var textarea = wrap.find('textarea[name="tiny_mce_name"]');                    
                    ok_function(textarea.val());
                    sima.dialog.close();
                });
                sima.dialog.openInFullScreen(wrap);
                wrap.append(response.html);
                var dialog = $(document.activeElement);
                dialog.find('.ui-dialog-title').html(dialog.find('.ui-dialog-title').html()+title);
//                sima.refreshChildrenForce(wrap.parent());
                setTinyMceHeight(wrap);
            }
        });
    };    
    
    function setTinyMceHeight(wrap)
    {
        if (wrap.find('.mceIframeContainer').length > 0)
        {            
            wrap.find('iframe').height(wrap.find('.mceIframeContainer').outerHeight());
        }
        else
        {
            setTimeout(function(){
                setTinyMceHeight(wrap);
            }, 100);
        }
    }
    
    /**
     * Ova funkcija proverava da li je zadati objekat u dialogu koji je na vrhu.
     * @param obj moze biti JQuery objekat ili selektor. Ukoliko je selektor i ima vise objekata, posmatra se prvi
     */
    this.inTopDialog = function(obj)
    {
        var o = null;
        var result = false;
        if (typeof(obj) === 'string')
        {
            o = $(obj);
            if (obj.size() === 0)
                return false;
        }
        else
            o = obj;
        o.parents('div.ui-dialog-content').each(function() {
            if (dialog.attr('id') === $(this).attr('id'))
            {
                result = true;
            }
        });
        return result;
    };

    /**
     * ne znam gde se pojavljuje ova funkcija
     * @param tag
     */
    this.Hide = function(tag) {
        dialog.find(tag).hide();
    };

    /**
     * naredne dve funkcije moraju da se pozovu prilikom AutoHeight
     * panel ne moze istovremeno da bude i center i fullscreen!!!!
     */
    this.Center = function()
    {
        if (dialog.hasClass('sima-dialog-position-full-screen'))
            return;
        dialog.addClass('sima-dialog-position-center');        
        setDialogPosition(dialog, 'center');
    };
    this.TopDialogFullScreen = function()
    {
//		if (dialog.hasClass('sima-dialog-position-center')) return;
        var _dialog_height = sima.getScreenHeight() - dialog_full_screen_margins;
        var _dialog_width = sima.getScreenWidth() - dialog_full_screen_margins;
        dialog.addClass('sima-dialog-position-full-screen');
        dialog.dialog("option", "height", _dialog_height);
        dialog.dialog("option", "width", _dialog_width);
        dialog.dialog('option', 'position', [dialog_full_screen_margins / 2, dialog_full_screen_margins / 2]);
        dialog.dialog({resizable: false});

        var _usable_height = _dialog_height - 25;
        var _usable_width = _dialog_width - 25;
        var source = 'TRIGGER_dialog_topDialogFullScreen:'+_usable_height+':'+_usable_width;
        sima.layout.setLayoutSize(dialog.children('.sima-layout-panel'),_usable_height,_usable_width,source);
//        sima.refreshChildrenForce(dialog);
//        sima.adjustHeight();
    };

    this.Width = function(new_width)
    {
        setDialogPosition(dialog, 'left top');
        dialog.dialog("option", "width", new_width);
    };
    
    function setDialogPosition(dialog, position)
    {
        dialog.dialog('option', 'position', {my: position, at: position, of: window});
    }
    
    /**
     * 
     * @param {string} component_name
     * @param {Object} component_params
     * @param {string} title
     * @param {Object} dialog_params
     * @returns {SIMADialog.dialog}
     */
    this.openVueComponent = function(component_name, component_params, title, dialog_params)
    {
        var id = 'sima_vue_dialog_id_'+sima.uniqid();
        
        var params_str = "";
        $.each(component_params,function(key,element){
            params_str = params_str +' v-bind:'+key+'="'+key+'"';
        });
        var html = '<'+component_name
                +' id="'+id+'"'
                +' class="sima-layout-panel"'
                + params_str
                +'></'+component_name+'>';

        var dialog = open(html,title,dialog_params);

        new Vue({
            el: '#'+id,
            data: component_params
        });
        return dialog;
    };
    
}
