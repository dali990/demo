
/* global sima */

function SIMAIcons()
{
    this.form = function(_this, model, model_id, data)
    {   
//        if (!_this.hasClass('_disabled'))
//        {
            sima.model.form(model, model_id, data);
//        }
    };
    
    this.orderScan = function(_this, message, id, value)
    {   
//        if (!_this.hasClass('_disabled'))
//        {
            sima.dialog.openYesNo(message,
                function() {
                    sima.orderScanModelTag(id, value); 
                    sima.dialog.close();
            });
//        }
    };
    
    this.remove = function(_this, model, pk, yes_function)
    {
//        if (!_this.hasClass('_disabled'))
//        {
            sima.model.remove(model, pk, yes_function);
//        }
    };
    
    this.recycle = function(_this, model, pk)
    {
//        if (!_this.hasClass('_disabled'))
//        {
            sima.model.recycle(model, pk);
//        }
    };
    
    this.deleteforever = function(_this, model, pk)
    {
//        if (!_this.hasClass('_disabled'))
//        {
            sima.model.deleteforever(model, pk);
//        }
    };
    
    this.restore_from_trash = function(_this, model, pk)
    {
//        if (!_this.hasClass('_disabled'))
//        {
            sima.model.restore_from_trash(model, pk);
//        }
    };
    
    this.cancelLink = function(_this, message, model, model_id)
    {      
//        if (!_this.hasClass('_disabled'))
//        {
            sima.dialog.openYesNo(message,
                function() {
                    sima.model.cancelTag(model, model_id);
            });
//        }
    };
    
    this.openModel = function(action, model_id)
    {   
//        if (!_this.hasClass('_disabled'))
//        {
            sima.dialog.openModel(action, model_id);
//        }
    };
    
    this.openInClient = function(_this, model_id, model)
    {   
        if (!_this.hasClass('_not_connected') && !_this.hasClass('_disabled'))
        {
            if(model_id === null)
            {
                sima.addError('sima.icons - openInClient', 'model_id is null');
                return; /// TODO: MilosJ - morao bi return, ne sme se nastaviti ako je doslo do greske
            }
            if(model === null)
            {
                sima.addError('sima.icons - openInClient', 'model is null');
                return; /// TODO: MilosJ - morao bi return, ne sme se nastaviti ako je doslo do greske
            }
            
            sima.file.openInClient(model_id, model);
        }
    };
    
    this.fileDownload = function(fileid, fileversionid)
    {
        sima.ajax.get('files/transfer/fileDownloadPrepare',{
            data:{
                file_id:fileid,
                file_version_id:fileversionid
            },
            async: true,
            loadingCircle: $('#body_sync_ajax_overlay'),
            success_function:function(response){
                if(sima.getUseNewRepmanager() === true)
                {
                    sima.openLink(response.dl, {target:"_blank"});
                }
                else
                {
                    sima.openLink(response.dl, {download:response.dn});
                }                  
            },
            failure_function:function(response){
                sima.dialog.openWarn(response.message);
            }
        });
    };
    
    this.status = function(_this, params)
    {
        var model = params.model;
        var model_id = params.model_id;
        var column = params.column;
        var confirm = params.confirm;
        var revert = params.revert;
        var message = params.message;
        var ok = params.ok;
        var not_ok = params.not_ok;     
        var is_multiselect = (typeof _this.data('is_multiselect') !== 'undefined' && _this.data('is_multiselect') === true) ? true : false;
        
        var model_ids = model_id;
        if (is_multiselect && typeof _this.data('table_id') !== 'undefined')
        {
            model_ids = $('#'+_this.data('table_id')).simaGuiTable('getSelectedIds');
        }
        
//        if (!_this.hasClass('_disabled'))
//        {
            var allow_null = _this.data('allow_null');
            //ako ima dva stanja
//            if (typeof allow_null !== 'undefined' && allow_null == false)
            if (typeof allow_null !== 'undefined' && allow_null === false)
            {
                sima.ajax.get('base/model/modelConfirmCheckAccess', {
                    sync: true,
                    get_params: {
                        model: model,
                        column: column,
                        is_revert: revert
                    },
                    data: {
                        model_ids:model_ids
                    },
                    async: true,
                    loadingCircle: $('#body_sync_ajax_overlay'),
                    success_function: function(){
                        sima.dialog.openYesNo(message,function() {
                            confirm_status(model, model_ids, column, confirm);
                        });
                    },
                    failure_function: function(repsponse){
                        sima.dialog.openWarn(repsponse.message);
                    }
                });
            }//ako ima tri stanja
            else
            {
                var _status = 'null';
                if (confirm)
                {
                    _status = true;
                }
                sima.ajax.get('base/model/modelConfirmCheckAccess', {
                    sync: true,
                    get_params: {
                        model: model,
                        column: column,
                        is_revert: revert
                    },
                    data: {
                        model_ids:model_ids
                    },
                    async: true,
                    loadingCircle: $('#body_sync_ajax_overlay'),
                    success_function: function(){
                        if (confirm)
                        {
                            //treba da se potvrdi u da ili ne
                            var params = [];
                            var par1 = {};
                            par1.title = 'Da';
                            par1.function = function() {
                                    confirm_status(model, model_ids, column, true);
                            };
                            params.push(par1);
                            var par2 = {};
                            par2.title = 'Ne';
                            par2.function = function() {
                                    confirm_status(model, model_ids, column, false);
                            };
                            params.push(par2);
                            var par3 = {};
                            par3.title = 'Odustani';
                            par3.function = function() {
                                    sima.dialog.close();
                            };
                            params.push(par3);
                            sima.dialog.openChooseOption(message,params);
                        }
                        else if (revert)
                        {
                            //treba da se vrati na stanje null
                            sima.dialog.openYesNo(message,function() { 
                                confirm_status(model, model_ids, column, 'null');
                            });
                        }
                    },
                    failure_function: function(repsponse){
                        sima.dialog.openWarn(repsponse.message);
                    }
                });
            }
//        }
    };
    
    function confirm_status(model, model_ids, column, status)
    {
        sima.ajax.get('base/model/ModelConfirm', {
            get_params: {
                model: model,
                column: column,
                new_status: status
            },
            data: {
                model_ids:model_ids
            },
            async: true,
            loadingCircle: $('#body_sync_ajax_overlay'),
            success_function: function(){
                sima.dialog.close();
            }
        });
    };
    
//    this.ConfirmCheck = function(model, model_id, column, status)
//    {
//        var response = false;
//        sima.ajax.get('base/model/ModelConfirm', {
//            sync: true,
//            get_params: {
//                model: model,
//                model_id: model_id,
//                column: column,
//                status: status,
//                check: true
//            },            
//            success_function: function(r){
//                response = r;
//            }
//        });
//        
//        return response;
//    };
    
    this.formStatus = function(_this, params)
    {
        var model = params.model;
        var column = params.column;
        var confirm = params.confirm;
        var revert = params.revert;
        var message = params.message;
        var ok = params.ok;
        var not_ok = params.not_ok;
        var intermediate = params.intermediate;
        
        if (typeof _this.data('revert') !== 'undefined')
            revert = _this.data('revert');
        if (typeof _this.data('confirm') !== 'undefined')
            confirm = _this.data('confirm');
        if (!_this.hasClass('_disabled'))
        {
            var allow_null = _this.data('allow_null');
            //ako ima dva stanja
//            if (typeof allow_null !== 'undefined' && allow_null == false)
            if (typeof allow_null !== 'undefined' && allow_null === false)
            {
                if (_this.hasClass(not_ok))
                {
                    _this.removeClass(not_ok).addClass(ok).data('value', true);
                    createInput(_this, model, column, 1);
                }
                else if (_this.hasClass(ok))
                {
                    _this.removeClass(ok).addClass(not_ok).data('value', false);
                    createInput(_this, model, column, 0);
                }
            }//ako ima tri stanja
            else
            {
                if (confirm)
                {
                    //treba da se potvrdi u da ili ne
                    var params = [];
                    var par1 = {};
                    par1.title = 'Da';
                    par1.function = function() {
                        _this.removeClass(intermediate).addClass(ok).data('value', true);
                        createInput(_this, model, column, 1);
                        _this.data('revert', true);
                        _this.data('confirm', false);
                        sima.dialog.close();
                    };
                    params.push(par1);
                    var par2 = {};
                    par2.title = 'Ne';
                    par2.function = function() {
                        _this.removeClass(intermediate).addClass(not_ok).data('value', false);
                        createInput(_this, model, column, 0);
                        _this.data('revert', true);
                        _this.data('confirm', false);
                        sima.dialog.close();
                    };
                    params.push(par2);
                    var par3 = {};
                    par3.title = 'Odustani';
                    par3.function = function() {
                            sima.dialog.close();
                    };
                    params.push(par3);
                    sima.dialog.openChooseOption(message,params);
                }
                else if (revert)
                {
                    //treba da se vrati na stanje null
                    _this.removeClass(not_ok).removeClass(ok).addClass(intermediate).data('value', 'null');
                    createInput(_this, model, column, 'null');
                    _this.data('revert', false);
                    _this.data('confirm', true);
                }
            }
        }
    };
    
    /**
     * pomocna funkcija za kreiranje inputa kod statusa. Koristi se u funkciji status u simaIcons-u.
     * @param {type} _this
     * @param {type} model
     * @param {type} column
     * @param {type} value
     */
    function createInput(_this, model, column, value)
    {
        var row = _this.parents('.sima-model-form-row:first');
        var input_name = model+'['+column+']';
        var input = $("<input class='status_input' type='hidden' name='"+input_name+"' value='"+value+"'>");
        var old_input = _this.parents('.sima-model-form-row:first').find('.status_input');
        input.data(old_input.data());
        old_input.remove();
        row.append(input);
        _this.trigger('formStatusChanged');
    }
    
    this.setStatusIcon = function(statusObj, value)
    {
        if (sima.helpers.isTrue(value))
        {
            statusObj.removeClass('_intermediate').removeClass('_not-ok').addClass('_ok');
            statusObj.data('value', true);
            statusObj.data('revert', true);
            statusObj.data('confirm', false);
        }
        else if (sima.helpers.isFalse(value))
        {
            statusObj.removeClass('_intermediate').removeClass('_ok').addClass('_not-ok');
            statusObj.data('value', false);
            statusObj.data('revert', true);
            statusObj.data('confirm', false);
        }
        else if (value === null || value === 'null')
        {
            statusObj.removeClass('_ok').removeClass('_not-ok').addClass('_intermediate');
            statusObj.data('value', 'null');
            statusObj.data('revert', false);
            statusObj.data('confirm', true);
        }
    };
    
    this.docWiki = function(_this, page)
    {
        if (!_this.hasClass('_disabled'))
        {
            window.open(sima.getBaseUrl()+'/index.php?r=site/docWikiCheck&redirect_link='+page, '_blank');
        }
    };
    
    this.personalWiki = function(_this, page)
    {
        if (!_this.hasClass('_disabled'))
        {
            window.open(sima.getBaseUrl()+'/index.php?r=site/personalWikiCheck&redirect_link='+page, '_blank');
        }
    };
    
    this.refresh = function(_this, model_tag)
    {
//        if (!_this.hasClass('_disabled'))
//        {
            sima.model.updateViews(model_tag,{async:'true'});
//        }
    };
    
    this.lockFile = function(_this, locked, file_id)
    {
//        if (!_this.hasClass('_disabled'))
//        {
//            var file_id = _this.data('file_id');
            sima.ajax.get('files/file/lockFile',{
                get_params:{
                    file_id:file_id,
                    locked:locked
                },
                async: true,
                loadingCircle: $('#body_sync_ajax_overlay'),
                success_function:function(response){
                    
                }
            });
//        }
    };
    
    this.submitForm = function(form_id, params)
    {
        var parsed_params = typeof params !== 'undefined' ? params : {};
        var forms_container = $('#' + form_id);
        var form_data = forms_container.data('form_data');

        if(!sima.isEmpty(form_data.before_submit_question))
        {
            sima.dialog.openYesNo(form_data.before_submit_question, function(){
                sima.dialog.close();
                submitForm(form_data, forms_container, parsed_params, form_id);
            });
        }
        else
        {
            submitForm(form_data, forms_container, parsed_params, form_id);
        }
    };
    
    function submitForm(form_data, forms_container, params, form_id)
    {
        var model_name = form_data.model_name;
        var model_id = typeof form_data.model_id !== 'undefined' ? form_data.model_id : null;
        var form_name = typeof form_data.form_name !== 'undefined' ? form_data.form_name : 'default';
        var form_status = typeof form_data.form_status !== 'undefined' ? form_data.form_status : '';
        var form_scenario = typeof form_data.form_scenario !== 'undefined' ? form_data.form_scenario : '';
        var form_params = typeof form_data.params !== 'undefined' ? form_data.params : {};
        var ajaxlong = typeof form_data.ajaxlong !== 'undefined' ? form_data.ajaxlong : false;
        
        //mogu da se serijalizuju samo polja koja nisu disable. Ukoliko nesto nije ok, polje ce ponovo biti disable posle ponovnog rendera
        forms_container.find('form').each(function(){                
              $(this).find(':disabled').removeAttr('disabled');
        });
        
        var data = forms_container.find('form').serialize();
        
        var success_func = function(response){
            sima.model.formResponse(response, forms_container);
            if (response.status === 'saved' && form_status === 'save_and_continue')
            {
                sima.model.form(model_name, '', {
                    formName: form_name,
                    scenario: form_scenario,
                    params: form_params
                });
            }

            if (typeof params.on_success_func !== 'undefined')
            {
                sima.executeFunction(params.on_success_func, response.status);
            }
        };
        
        if(ajaxlong === true)
        {
            var ajaxloading_div = $('<div style="z-index:1000000; width:100%; height:100%; position:absolute;"></div>');
            $('body').prepend(ajaxloading_div);
            sima.ajaxLong.start('base/model/formLong', {
                showProgressBar: ajaxloading_div,
                data:{
                    get: {
                        model: model_name,
                        model_id: model_id,
                        form_id: form_id,
                        formName: form_name,
                        status: form_status,
                        scenario: form_scenario,
                        is_submit: true
                    },
                    post: data
                },
                onEnd: function(response){
                    ajaxloading_div.remove();
                    success_func(response);
                },
                onCancel: function(){
                    onAjaxLongFormError(ajaxloading_div);
                },
                failure_function: function(response){
                    onAjaxLongFormError(ajaxloading_div);
                    sima.dialog.openWarn(typeof response === 'object' ? JSON.stringify(response) : response);
                },
                fatal_failure_function: function(){
                    onAjaxLongFormError(ajaxloading_div);
                },
                error_function: function(){
                    onAjaxLongFormError(ajaxloading_div);
                }
            });
        }
        else
        {
            sima.ajax.get('base/model/form',{
                get_params: {
                    model: model_name,
                    model_id: model_id,
                    form_id: form_id,
                    formName: form_name,
                    status: form_status,
                    scenario: form_scenario,
                    is_submit: true
                },
                data: data,
                async: true,
                loadingCircle: $('#body_sync_ajax_overlay'),
                success_function: success_func
            });
        }
    }
    
    function onAjaxLongFormError(ajaxloading_div)
    {
        ajaxloading_div.remove();
    }
    
    this.multiselectLock = function(_this, message, table_id)
    {
//        if (!_this.hasClass('_disabled'))
//        {
            var locks = [];
            $('#'+table_id).data('_body').find('tr.selected').each(function(){
                var id = $(this).attr('model_id');
                locks.push(id);
                $(this).removeClass('selected');
            });

            sima.ajax.get('files/file/multiselectLock', {
                data: {locks:locks},
                success_function:function(response){
                    $('#'+table_id).simaGuiTable('disableMultiSelect');
                }
            });
//        }
    };
    
    this.multiselectDelete = function (_this, message, table_id)
    {
//        if (!_this.hasClass('_disabled'))
//        {
            sima.dialog.openYesNo(message,
                function() {
                    var ids = [];
                    var model = $('#'+table_id).attr('model');
                    $('#'+table_id).data('_body').find('tr.selected').each(function(){
                        var id = $(this).attr('model_id');
                        ids.push(id);
                        $(this).removeClass('selected');
                    });
                    if (ids.length > 0)
                    {
                        sima.ajaxLong.start('base/model/multiselectDelete', {
                            data: {ids:ids, model:model},
                            showProgressBar:$('#'+table_id),
                            onEnd:function(response){
                                $('#'+table_id).simaGuiTable('disableMultiSelect');
                            }
                        });
                    }
                    sima.dialog.close();
                }
            );
//        }
    };
    
    this.multiselectRecycle = function(_this, message, table_id)
    {
//        if (!_this.hasClass('_disabled'))
//        {
            sima.dialog.openYesNo(message,
                function() {
                    var ids = [];
                    var model = $('#'+table_id).attr('model');
                    $('#'+table_id).data('_body').find('tr.selected').each(function(){
                        var id = $(this).attr('model_id');
                        ids.push(id);
                        $(this).removeClass('selected');
                    });

                    sima.ajax.get('base/model/multiselectRecycle', {
                        data: {ids:ids, model:model},
                        success_function:function(response){
                            $('#'+table_id).simaGuiTable('disableMultiSelect');
                        }
                    });
                    sima.dialog.close();
                }
            );
//        }
    };
    
//    this.multiselectReview = function (_this, message, table_id)
//    {
////        if (!_this.hasClass('_disabled'))
////        {
//            sima.dialog.openYesNo(message,
//                function() {
//                    var ids = [];
//                    var model = $('#'+table_id).attr('model');
//                    $('#'+table_id).data('_body').find('tr.selected').each(function(){
//                        var id = $(this).attr('model_id');
//                        ids.push(id);
//                        $(this).removeClass('selected');
//                    });
//
//                    sima.ajax.get('base/model/multiselectReview', {
//                        data: {ids:ids, model:model},
//                        success_function:function(response){
//                            $('#'+table_id).simaGuiTable('disableMultiSelect');
//                        }
//                    });
//                    sima.dialog.close();
//                }
//            );
////        }
//    };
    
    this.multiselectQRCode = function (_this, message, table_id)
    {
//        if (!_this.hasClass('_disabled'))
//        {
            sima.dialog.openYesNo(message,
                function() {
                    var ids = [];
                    var model = $('#'+table_id).attr('model');
                    $('#'+table_id).data('_body').find('tr.selected').each(function(){
                        var id = $(this).attr('model_id');
                        ids.push(id);
                        $(this).removeClass('selected');
                    });

                    sima.ajax.get('base/QRCode/multiselectQRCode', {
                        data: {
                            ids:ids, 
                            model:model
                        },
                        success_function:function(response){
                            $('#'+table_id).simaGuiTable('disableMultiSelect');
                            sima.temp.download(response.tn, 'QRCodes.pdf');
                        }
                    });
                    sima.dialog.close();
                }
            );
//        }
    };
    
    this.multiselectStatus = function (_this, message, table_id)
    {
//        if (!_this.hasClass('_disabled'))
//        {
            sima.dialog.openYesNo(message,
                function() {
                    var ids = [];
                    var model = $('#'+table_id).attr('model');
                    $('#'+table_id).data('_body').find('tr.selected').each(function(){
                        var id = $(this).attr('model_id');
                        ids.push(id);
                        $(this).removeClass('selected');
                    });

                    sima.ajax.get('base/model/multiselectStatus', {
                        data: {
                            ids:ids, 
                            model:model
                        },
                        success_function:function(response){
                            $('#'+table_id).simaGuiTable('disableMultiSelect');
                            sima.temp.download(response.tn, 'QRCodes.pdf');
                        }
                    });
                    sima.dialog.close();
                }
            );
//        }
    };
    
    this.downloadQRCode = function(model_name, model_id, size)
    {
        sima.ajax.get('base/QRCode/downloadQRCode',{
            get_params:{
                model_name: model_name,
                model_id: model_id,
                size: size
            },
            success_function:function(response){
                sima.temp.download(response.tn, 'QRCode.pdf');
            }
        });
    };
    
    this.copyModel = function (Model, model_id)
    {
//        if (!_this.hasClass('_disabled'))
//        {
//            var _this = this;
            sima.dialog.openYesNo('Da li ste sigurni da želite da iskopirate ovaj red?',
                function() {
                    sima.dialog.close();                    
                    sima.ajax.get('base/model/copyModel', {
                        get_params:{
                            Model:Model,
                            model_id:model_id
                        },
                        success_function:function(response){  
                            var guitable = sima.getLastActiveFunctionalElement();
                            if(sima.isEmpty(guitable))
                            {
                                return;
                            }
                            if(!sima.elementHasPlugin(guitable, 'simaGuiTable'))
                            {
                                guitable = guitable.find('.sima-guitable:first');
                            }
                            guitable.simaGuiTable('append_row', response.model_id);
                        }
                    });                    
            });
//        }
    };
    
    this.quickOpenFile  = function(params)
    {
        var getSelectedDesktopAppEventRelayId = sima.desktopapp.getSelectedDesktopAppEventRelayId();

        if(sima.isEmpty(getSelectedDesktopAppEventRelayId))
        {
            sima.callFunction(params.download_action);
        }
        else
        {
            sima.callFunction(params.readonly_action);
        }
    };
}
