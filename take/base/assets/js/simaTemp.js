
/* global sima */

function SIMATemp()
{
    this.download = function(tempName, downloadName)
    {
        if(downloadName === undefined || downloadName === null)
        {
            downloadName = '';
        }

        var url = sima.getBaseUrl() + '?r=files/transfer/downloadTemp&fn='+tempName+'&dn='+escape(downloadName);
        sima.openLink(url, {download:downloadName});
    };
}
