
/* global sima */

/**
 * open(html,title,params) - params {close_func}
 * openYesNo(question, yes_function, no_function, data);
 * openAJAX(codename, data) - POST data
 * html(html) - update html in top opend Dialog
 * @returns 
 */

function SIMAModel() 
{
    this.prepareModelViewsTagsForUpdate = function(tags)
    {
        if (typeof(tags)==='undefined' || tags==='') return;
        if (typeof(tags)==='string') tags=[tags];
        
        var oldModels = {};
        for (var i in tags)
        {
            var tag_i = tags[i].replace(/"/g, ""); 
            $('.sima-model-view:not(.sima-full-info).'+tag_i+', '+'.sima-guitable-row.'+tag_i+', '+'li.sima-ui-tree-leaf.'+
                    tag_i+', '+'.sima-ui-tabs.'+tag_i+', '+'.sdl-rp-fi.'+tag_i).each(function(i,value)
            {
                var obj = $(this);     
                if (obj.hasClass('sima-model-view-update-requested'))
                {                    
                    return true;
                }
                
                /**
                 * MilosS(10.7.2019.):
                 * ukoliko samom tabu nije setovana opcija reload_on_update, ne treba da se ubacuje u spisak.
                 * Ako se ovo ne uradi, onda se ne updateuju stvari koje su unutar njega, a to se videlo kada je unutar taba gui table.
                 * U nekom trenutku se ovo pokvarilo, ali ne znam kad 
                 * Mislim da ne treba ovde previse obracati paznju jer ovi tabovi treba da nestanu
                 * ovo je bilo i u zahtevu : kasni refresh kod magacina, prilikom brisanja stavki dokumenta prometa
                 */
                if (obj.hasClass('sima-ui-tabs'))
                {
                    var tabs = obj.data('tabs');                        
                    var active_tab = obj.simaTabs('getActiveTab');
                    var active_tab_index = active_tab.index();
                    var active_tab_params = tabs[active_tab_index];
                    if (!sima.isEmpty(active_tab_params) && typeof active_tab_params.reload_on_update !== 'undefined' && active_tab_params.reload_on_update === true)
                    {
                        
                    }
                    else
                    {
                        return true;
                    }
                }
                
                
                for (var ii in oldModels)
                {
                    //slucaj ako je sima tab i ako nije setovano reload_on_update onda treba da se updejtuju view-ovi unutar taba
                    var reload_on_update = false;                    
                    if ($(oldModels[ii]).hasClass('sima-ui-tabs'))
                    {                        
                        var tabs = $(oldModels[ii]).data('tabs');                        
                        var active_tab = $(oldModels[ii]).simaTabs('getActiveTab');
                        var active_tab_index = active_tab.index();
                        var active_tab_params = tabs[active_tab_index];
                        if (!sima.isEmpty(active_tab_params) && typeof active_tab_params.reload_on_update !== 'undefined' && active_tab_params.reload_on_update === true)
                        {
                            reload_on_update = true;
                        }
                    }      
                    //ne ubacujemo ukoliko je ovaj view nadjen u vec postojecem, 
                    //izuzeci (znaci ubacujemo u tim slucajevima)
                    // treba da se update-uje full_info
                    // treba da se update-uje tab -> zato sto se njegov update poziva samo zbog spiska tabova
                    if (
                            (
                                !$(oldModels[ii]).hasClass('sima-ui-tabs') || 
                                ($(oldModels[ii]).hasClass('sima-ui-tabs') && reload_on_update)
                            ) 
                            &&
                            //ne proveravamo da li se objekat za updejt nalazi unutar full infa za SIMADefaultLayout da bi se pozvao klasican 
                            //model view updejt za sve objekte koji se nalaze u njemu
                            !oldModels[ii].hasClass('sdl-rp-fi') 
                            && 
                            oldModels[ii].find(this).size()>0 
                       )
                    {                        
                        return true;
                    }
                }

                //ukoliko se modeli nalaze u guitabeli
                if (obj.parents('.sima-guitable-row.'+tag_i).size()>0)
                {
                    return true;
                }
                //proverava da li se selektovani objekti nalaze u novom
                //MilosS(2.9.2013): OVO NE BI TREBALO DA SE DESAVA JER SE PRVO NADJE OTAC PA ONDA DECA
                //MilosS(28.6.2017): OVO SE DESAVA -> jer je moguce da razliciti modeli imaju svoje view-ove, pa ne znamo ko je prvi stigao
                for (var ii in oldModels)
                {
                    if (!$(this).hasClass('sdl-rp-fi') && $(this).find(oldModels[ii].get()).size()>0) 
                    {
                        oldModels[ii].removeClass('sima-model-view-update-requested');
                        delete oldModels[ii];
                    }
                }
                var request_id = sima.uniqid();
                obj.addClass('sima-model-view-update-requested');
                oldModels['sima-model-view-update-request-'+request_id] = obj;    
            });
        }
    };
    
    this.updateViews = function(tag,parpar,excludePrepare)
    {     
        if (
                typeof(tag)==='undefined' 
                || 
                tag===''
                ||
                (typeof(tag)==='array' && tag.length === 0)
            ) 
        {
            return;
        }
        if (typeof(tag)==='string') tag=[tag];
        
        if (typeof(parpar)!=='object')                 parpar={};
        if (typeof(parpar.async)!=='string')           parpar.async='true';
        
        if (typeof excludePrepare === 'undefined' || (typeof excludePrepare !== 'undefined' && excludePrepare === false))
        {
            sima.model.prepareModelViewsTagsForUpdate(tag);
        }
        
        var requests = {};
        var oldModels = {};        
        var is_requested = false;
        for (var i in tag)
        {
            var tag_i = tag[i].replace(/"/g, "");
            
//            console.log('trazim .sima-model-view.'+tag[i]);
            
            $('.sima-model-view.sima-model-view-update-requested.'+tag_i).each(function(i,value)
            {
                var obj = $(this);                
                obj.removeClass('sima-model-view-update-requested');
                var model = obj.attr('model');
                var model_id = obj.attr('model_id');
                var model_view = obj.attr('model_view');
                var params = jQuery.parseJSON(obj.attr('params'));
                var additional_classes = obj.attr('additional_classes');
                if (typeof parpar.params !== 'undefined')
                {
                    $.extend(true, params, parpar.params);
                }
                
                if (typeof(model)==='undefined' || typeof(model_id)==='undefined' || typeof(model_view)==='undefined')
                {
                    sima.dialog.openCustom('updateModelViews - ERROR 1');
                    return;
                }
                
                var request_id = sima.uniqid();                
                requests['sima-model-view-update-request-'+request_id]={
                    model:model,
                    model_id:model_id,
                    model_view:model_view,
                    params:params,
                    additional_classes:additional_classes
                };                
                is_requested = true;
                oldModels['sima-model-view-update-request-'+request_id] = $(this);
            });
        }
//        console.log('parpar.async -> '+parpar.async);
        if (is_requested)
        {
            sima.ajax.get('base/model/viewUpdate',{
                data:{requests:requests},
                async:(parpar.async==='true'),                
                success_function:function(response)
                {
                    for (var key in response.views)
                    {
                        oldModels[key].hide();
                        oldModels[key].after(response.views[key]);
                        var new_model = oldModels[key].next();
//                        var trigger_on = new_model.parent();
                        oldModels[key].remove();
                        var _fixed_parent = new_model.parent('.sima-layout-fixed-panel');
                        if (_fixed_parent.length === 1)
                        {
                            if (new_model.siblings().length === 0)
                            {
                                if (_fixed_parent.parent().hasClass('_vertical'))
                                {
                                    _fixed_parent.width(new_model.outerWidth());
                                }
                                else
                                {
                                    _fixed_parent.height(new_model.outerHeight());
                                }
                            }
                            else
                            {
                                console.error('POSTOJI VISE OD 1 model-view-a u fiksnom panelu: ');
                                console.error(_fixed_parent);
                            }
                        }
                        sima.layout.allignFirstLayoutParent(new_model,'TRIGGER_sima_update_model_view');
                    }
                    $('body').append(response.processOutput);
                }
            });
        }
        
        /** update iz GuiTable-a. Ako ima vise redova za update, onda se update vrsi odjednom za sve redove u sklopu jedne tabele. Znaci za svaku tabelu se pravi novi poziv. 
         * Treba izmeniti da se sve vrsi u jednom pozivu*/
        var table_ids = []; //niz u kome se pamte razliciti id-evi tabela
        for (i in tag)
        {
            $('.sima-guitable-row.sima-model-view-update-requested.'+tag[i]).each(function(){
                var this_col = $(this);
                this_col.removeClass('sima-model-view-update-requested');
                var table_id = this_col.parents('.sima-guitable:first').attr('id');
                //vrsi se provera za guitable. Upisuje se id tabele u niz ako nije vec upisan i kao data(koje je niz modela) toj tabeli se dodaje model_id.
                if (typeof table_id !== 'undefined')
                {
                    if (table_ids.indexOf(table_id) === -1)
                    {
                        table_ids.push(table_id);
                        this_col.parents('.sima-guitable:first').data('models_requested',[this_col.attr('model_id')]);
                    }
                    else
                    {
                        var models_requested = this_col.parents('.sima-guitable:first').data('models_requested');
                        models_requested.push(this_col.attr('model_id'));
                        this_col.parents('.sima-guitable:first').data('models_requested', models_requested);
                    }
                }
                //vrsi se provera za simpletable
                else
                {
                    var table_id = this_col.parents('.sima-simpletable:first').attr('id');
                    if (table_ids.indexOf(table_id) === -1)
                    {
                        table_ids.push(table_id);
                        this_col.parents('.sima-simpletable:first').data('models_requested',[this_col.attr('model_id')]);
                    }
                    else
                    {
                        var models_requested = this_col.parents('.sima-simpletable:first').data('models_requested');
                        models_requested.push(this_col.attr('model_id'));
                        this_col.parents('.sima-simpletable:first').data('models_requested', models_requested);
                    }
                }
            });
        }
        //za svaku tabelu se poziva refresh_row kome se prosledjuju id-evi modela koji treba da se update-uju.
        for (var table_id in table_ids)
        {
            var table = $('#'+table_ids[table_id]);
            if (table.hasClass('sima-simpletable'))
            {
                table.simaSimpleTable('refresh_row', table.data('models_requested'), {async:(parpar.async==='true')});
            }
            else
            {
                table.simaGuiTable('refresh_row', table.data('models_requested'), {async:(parpar.async==='true')});
            }
        }
        /** 
         * @todo da se zadaje koji tabovi ne treba da se refreshuju 
         * 
         * */
        for (var i in tag)
        {
            $('li.sima-ui-task-element.sima-model-view-update-requested.'+tag[i]).each(function(){
                var this_task = $(this);
                this_task.removeClass('sima-model-view-update-requested');
                this_task.parents('.sima-ui-task-tree').simaTree('update_leaf', this_task);
            });
            $('li.sima-ui-tree-leaf.sima-model-view-update-requested.'+tag[i]).each(function(){
                var this_leaf = $(this);
                this_leaf.removeClass('sima-model-view-update-requested');
                this_leaf.parents('.sima-ui-tree').simaTree('update_leaf', this_leaf);
            });
            var tab_i = $('.sima-ui-tabs.sima-model-view-update-requested.'+tag[i]);
            tab_i.removeClass('sima-model-view-update-requested');         
            tab_i.simaTabs('list_tabs');
            
            var sdl_fi = $('.sdl-rp-fi.sima-model-view-update-requested.'+tag[i]);
            if (!sima.isEmpty(sdl_fi))
            {
                sdl_fi.removeClass('sima-model-view-update-requested');         
                sdl_fi.parents('.sdl:first').simaDefaultLayout('update');
            }
        }
    };
    
    /**
     * funkcija kojom se samo brisu pogledi
     * @param {type} tag
     * @returns {undefined}
     */
    this.removeViews = function(tag){
        if (typeof(tag)=== 'string') tag=[tag];
        for (var i in tag)
        {
            var for_update = [];
            $('.sima-model-view.'+tag[i]).each(function(){
                var local_this = $(this);
                if (local_this.attr('tag')===tag[i])
                    local_this.remove();
                else
                    for_update.push(tag[i]);
            });
            $('.sima-guitable-row.'+tag[i]).each(function(){
                var local_this = $(this);
                if (local_this.attr('tag')===tag[i])
                    local_this.remove();
                else
                    for_update.push(tag[i]);
            });
            sima.model.updateViews(for_update);
        }
    };
    
    /**
     * Funkcija kojom se Brise model iz baze. nakon toga se poziva removeViews da bi se obrisali i pogledi
     * @param {type} model
     * @param {type} model_id
     * @param {type} func
     * @returns {undefined}
     */
    this.remove = function(model, model_id, func, params)
    {
//        if (typeof(model)!=='string' || typeof(model_id)!=='string')            return;
        sima.dialog.openYesNo('Da li zaista zelite da obrisete?',
            function(){
                if (typeof params !== 'undefined' && typeof params.beforeYesFunc !== 'undefined')
                {
                    params.beforeYesFunc();
                }
                sima.ajax.get('/base/model/remove',{
                    data: {
                        model:model,
                        model_id:model_id
                    },
                    success_function: function(){
                        sima.dialog.close();
                        if(!sima.isEmpty(func))
                        {
                            if (typeof func === 'function')
                            {
                                func();
                            }
                            else if (typeof(func)==='string')
                            {
                                var response = {
                                    model_id:model_id
                                };
                                eval('(' + func +')('+JSON.stringify(response)+');');
                            }
                        }
                    }
                });
            }
        );      
    };
    
    this.cancelTag = function (model, model_id)
    {   
        sima.ajax.get('base/model/cancel',{
            get_params:{
                model:model,
                id:model_id
            },
            success_function: function(){
                sima.dialog.closeYesNo();
            }
        });
    };
    
    /**
     * Funkcija kojom se Ubacuje u kantu
     * @param {string} model
     * @param {integer} model_id
     * @param {function} func
     * @returns {undefined}
     */
    this.recycle = function(model, model_id, func)
    {
        sima.dialog.openYesNo(sima.translate('RecycleAreYouSure'),
            function(){
                sima.ajax.get('base/model/recycle',{
                    get_params: {
                        model:model,
                        model_id:model_id
                    },
                    success_function: function(){
                        sima.dialog.close();
                        if (typeof func === 'function')
                            func();
                    },
                    failure_function: function(response){
                        sima.dialog.close();
                        sima.dialog.openWarn(response.message);
                    }
                });
            }
        );      
    };
    
    /**
     * Funkcija kojom se zauvek brise
     * @param {string} model
     * @param {integer} model_id
     * @returns {undefined}
     */
    this.deleteforever = function(model, model_id)
    {
        sima.dialog.openYesNo('Da li zaista zelite da trajno obrisete?',
            function(){
                sima.ajax.get('base/model/deleteForever',{
                    get_params: {
                        model:model,
                        model_id:model_id
                    },
                    success_function: function(){
                        sima.dialog.close();
                    },
                    failure_function: function(responce){
                        sima.dialog.close();
                        sima.dialog.openWarn(responce.message);
                    }
                });
            }
        );      
    };
    
    /**
     * Funkcija koja vraca iz kante
     * @param {string} model
     * @param {integer} model_id
     * @returns {undefined}
     */
    this.restore_from_trash = function(model, model_id)
    {
        sima.dialog.openYesNo(sima.translate('RecycleRevertAreYouSure'),
            function(){
                sima.ajax.get('base/model/restoreFromTrash',{
                    get_params: {
                        model:model,
                        model_id:model_id
                    },
                    success_function: function(){
                        sima.dialog.close();
                    },
                    failure_function: function(responce){
                        sima.dialog.close();
                        sima.dialog.openWarn(responce.message);
                    }
                });
            }
        );      
    };
    
    this.removeForeignKey = function(model, model_id, fk_model, fk_model_id, column)
    {
//        if (typeof(model)!=='string' || typeof(model_id)!=='string')            return;
        sima.dialog.openYesNo('Da li zaista zelite da obrisete?',
            function(){
                sima.ajax.get('/base/model/removeForeignKey',{
                    data: {
                        model:model,
                        model_id:model_id,
                        fk_model:fk_model,
                        fk_model_id:fk_model_id,
                        column:column
                    },
                    success_function: function(){
                        sima.dialog.close();
                    }
                });
            }
        );      
    };
    
    /**
     * ovde se nalaze funkcije koje se pokrecu prilikom zatvaranja forme.
     * Posto je moguce da ima ugnjezdenih formi (zavodna knjiga moze da doda fajl)
     * potrebno je svaku funkciju jedinstveno obeleziti
     * Milos S
     */
//    var localOnSave=function(response) {};
    var localOnSave = [];
    
    this.form = function (model,model_id,params)
    {
        /** 
         * promenljiva koja oznacava konkretnu form
         * preko nje se trazi localOnSave
         * 
         * @type @exp;sima@call;uniqid
         */
        var form_id = 'simamodelform'+sima.uniqid();
        var append_obj = {};
        if (typeof(model)!=='string')            return;
        if (typeof(model_id)==='undefined')      model_id='';
        if (typeof(model_id)==='object')      
        {
            if (typeof(model_id.append_object)==='string')
            {
                model_id.obj = $(model_id.append_object);
                
                if (typeof(model_id.model_view)!=='string') model_id.model_view = 'default';
                append_obj = model_id;
                model_id='';
            }
            else if (typeof model_id.uniq_check !== 'undefined')
            {
                model_id=model_id.uniq_check;                
            }
        }
        if (typeof(params)==='undefined')         params={};
        if (typeof(params.title)==='undefined')   params.title='Forma';
        if (typeof(params.formName)!=='string')   params.formName = 'default';
        if (typeof(params.scenario)!=='string')   params.scenario = null;
        
        var localUpdateModelViews = '';
        if (typeof(params.updateModelViews)==='undefined')  
        {
           /// console.warn('nije izdefinisano');
//            localUpdateModelViews='neki tag';
        }
        else
            localUpdateModelViews = params.updateModelViews;
        
        localOnSave['index'+form_id]=function(response){
            //proverava se da li je potrebno dodavanje objekta
            if (typeof(params.onSave)==='function')
            {
//                console.warn('jeste funkcija i poziva se');                
                params.onSave(response);
            }
            else if (typeof(params.onSave)==='string')
            {
                eval('(' + params.onSave +')('+JSON.stringify(response)+');');
            }
            else
            {
//                console.warn('nije funkcija!!!!!!!!');
            }
            if (typeof(response.model_id)!=='undefined' && typeof(append_obj.obj)!=='undefined' && append_obj.obj.size()>0)
            {
//                if (typeof(append_obj.type)!=='undefined' && append_obj.type==='GuiTable')
//                {
//                     $(append_obj.append_object).simaGuiTable('append_row', response.model_id);
//                }
//                else 
                if (typeof(append_obj.type)!=='undefined' && append_obj.type==='SimpleTable')
                {
                     $(append_obj.append_object).simaSimpleTable('append_row', response.model_id);
                }
                else
                    sima.ajax.get('base/model/view',{
                        get_params:{
                           model:model,
                           model_id:response.model_id,
                           model_view:append_obj.model_view
                        },
                        success_function:function(response){
                            append_obj.obj.prepend(response.html);
                           // console.log('klase su '+append_obj.obj.attr('class'));
                        }
                    });
            }
//            else console.log('manje je od nula'); //nije potrebno dodavanje objekta
//            console.log('updateuje se '+localUpdateModelViews+'dsdsds');
            sima.model.updateViews(localUpdateModelViews);
            
        };
//        if (typeof(params.data)==='undefined')   
//        params.data = {};
//        if (typeof(params.filters)!=='object')   params.filters = {aa:'aa'};
//        params.data.filters = params.filters;

        var init_data = (typeof(params.init_data)!=='undefined')?params.init_data:{};        
        var status = (typeof params.status !== 'undefined')?params.status:'initial';
        init_data.is_submit = true;
        if (typeof params.params !== 'undefined')
        {
            init_data.params = params.params;
        }        
        sima.ajax.get('base/model/form',{
            get_params:{
                model:model,
                model_id:model_id,
                formName:params.formName,
                scenario:params.scenario,
                form_id:form_id,
                status:status
            },
            data: init_data,
//            async: typeof params.async !== 'undefined' ? params.async : false,
            async: true,
            loadingCircle: $('#body_sync_ajax_overlay'),
            success_function:function(response){
                var close_func = (typeof(params.onClose)!=='undefined')?params.onClose:null;
                if(!sima.isEmpty(response.onClose))
                {
                    var response_onClose = response.onClose;
                    if(!sima.isEmpty(close_func))
                    {
                        if(typeof close_func !== 'array')
                        {
                            close_func = [close_func];
                        }
                        
                        /// ono sto iz php-a dodje kao niz ovde ume da bude i objekat
                        var response_onClose_typeof = typeof response_onClose;
                        if(response_onClose_typeof !== 'array' && response_onClose_typeof !== 'object')
                        {
                            response_onClose = [response_onClose];
                        }
                        close_func = close_func.concat(response_onClose);
                    }
                    else
                    {
                        close_func = response_onClose;
                    }
                }
                sima.dialog.openModelForm(response.form_html, response.fullscreen, {
                    close_func: close_func
                });
                if (!sima.isEmpty(params.onLoad))
                {
                    sima.executeFunction(params.onLoad, form_id);
                }
                setOnScroll();
                setTimeout(function(){
                    setFocusOnFirstEnabledField();
                }, 0); //Sasa A. - mora sa timeoutom, jer inace se ne pozove focus()
                setFormSaveOnEnter();
            }
        });
        
		
    };
        
    this.enableFormInput = function(obj)
    {
        obj.parent().find('input').removeAttr('disabled');
        obj.parent().find('select').removeAttr('disabled');
        obj.parent().find('.sima-ui-sf-remove').removeClass('_disabled');
    };
    
    function setFormSaveOnEnter()
    {
        var last_active_functional_element = sima.getLastActiveFunctionalElement();        
        last_active_functional_element.on('keyup',function(e) {
            if (e.which === 13 && !$(e.target).is('textarea'))
            {                                         
                var submit_button = last_active_functional_element.find('.sima-model-form-row-button .sima-model-form-button.submit_form');                
                if (typeof submit_button[0] !== 'undefined')
                {                    
                    submit_button[0].click();
                }                        
            }
        });
    }
    
    function setFocusOnFirstEnabledField()
    {        
        var dialog = sima.getLastActiveFunctionalElement();        
        dialog.find('.sima-model-form-row:not(._disabled) input:visible').on('focus', function (e) {
            $(this)
                .one('mouseup', function () {
                    $(this).select();
                    return false;
                })
                .select();
        });

        var form_rows = dialog.find('.sima-model-form-row');
        var form_rows_length = form_rows.length;
        for (var i = 0; i < form_rows_length; i++)
        {
            var visible_input = $(form_rows[i]).find('input:visible:first');
            var visible_select = $(form_rows[i]).find('select:visible:first');
            var visible_textarea = $(form_rows[i]).find('textarea:visible:first');

            var is_visible_input = (visible_input.length > 0 && visible_input.is(':enabled'));
            var is_visible_select = (visible_select.length > 0 && visible_select.is(':enabled'));
            var is_visible_textarea = (visible_textarea.length > 0 && visible_textarea.is(':enabled'));

            if(is_visible_input === true || is_visible_select === true || is_visible_textarea === true)
            {
                if(is_visible_input === true)
                {                    
                    visible_input.focus();
                }
                else if(is_visible_select === true)
                {
                    visible_select.find('select').focus();
                }
                else if(is_visible_textarea === true)
                {                    
                    visible_textarea.focus();
                }
                
                break;
            }
        }
    }
    
    //poziva se kod scrolla u formi da bi se sakrio rezultat search field-a
    function setOnScroll()
    {
        var dialog = $(document.activeElement);
        dialog.find('.sima-model-forms').on('scroll',{},function(){
            $(this).find('.sima-ui-sf').each(function(){
                    $(this).simaSearchField('hideResult');
            });
        });
    }
    
    this.formResponse = function(response, forms_container)
    {
        switch (response.status)
        {
            case "validated":
            case "saved": 
                {
                    var _index = 'index'+response.form_id;
                    sima.dialog.close(response); 
                    if (typeof localOnSave[_index] === 'function')
                    {
                        localOnSave[_index](response);
                        localOnSave[_index] = undefined;
                    }
                    else
                        console.log('nije funkcija nego '+(typeof localOnSave[response.form_id]));                    
                }
              //  eval(response.onSave);
                break;
            case "validation_fail":
                forms_container.html(response.form_html);
                sima.layout.allignObject(forms_container, 'TRIGGER_model_form_validation_fail');
                break;
            case "refresh": 
                forms_container.html(response.form_html);
                sima.layout.allignObject(forms_container, 'TRIGGER_model_form_refresh');
                break;
        }

    };
    
    this.choose = function(model, ok_function, params)
    {
        params = params || {};
        var uniq_id = sima.uniqid();
        var search_model_params = {
            uniq_id: uniq_id
        };
        if (typeof params.view !== 'undefined')
        {
            search_model_params.view = params.view;
        }
        if (typeof params.multiselect !== 'undefined' && params.multiselect===true)
        {
            search_model_params.multiselect = params.multiselect;
        }
        if (typeof params.filter !== 'undefined')
        {
            search_model_params.filter = params.filter;
        }
        if (typeof params.fixed_filter !== 'undefined')
        {
            search_model_params.fixed_filter = params.fixed_filter;
        }
        if (typeof params.select_filter !== 'undefined')
        {
            search_model_params.select_filter = params.select_filter;
        }
        if (typeof params.title !== 'undefined')
        {
            search_model_params.title = params.title;
        }
        if (typeof params.params !== 'undefined')
        {
            search_model_params.params = params.params;
        }
        if (typeof params.model_id !== 'undefined' && params.model_id !== null)
        {
            search_model_params.model_id = params.model_id;
        }
        
        search_model_ids = [];
        if (typeof params.ids !== 'undefined')
        {
            $.each(params.ids, function(index, value) {
                search_model_ids.push(value.id);
            });
            search_model_params.search_model_ids = search_model_ids;
        }

        sima.ajax.get('base/model/searchModel',{
            get_params:{
                model:model
            },
            data: search_model_params,
            async: true,
            loadingCircle: $('#body_sync_ajax_overlay'),
            success_function:function(response)
            {
                var choose_uniq = uniq_id+"sima_search_model_choose";
                var choose_wrap = $("<div id='"+choose_uniq+"' class='sima-layout-panel sima-search-model-choose' data-search_model_ids='"+search_model_ids.join()+"'></div>");
                if (response.fullscreen)
                {
                    //ako je multiselect
                    if (typeof params.multiselect !== 'undefined' && params.multiselect === true)
                    {
                        choose_wrap.addClass('sima-layout-panel _splitter _vertical');
                        var left_side  = $("<div class='sima-layout-panel left_side'></div>");
                        var right_side = $("<div class='sima-layout-panel right_side'></div>");
                        choose_wrap.append(left_side);
                        
                        //is_guitable_view znaci da je desno guitable a ne lista izabranih stavki. To moze biti samo u slucaju da je levi view koji se renderuje guitable
                        if (!response.is_guitable_view)
                        {
                            var ids_list = $("<div class='sima-search-model-choose-list'></div>");
                            //ako imamo zadate id-eve kreiramo html za njih
                            if (typeof params.ids !== 'undefined')
                            {
                                $.each(params.ids, function(index, value) {
                                    var _class = (typeof value.class !== 'undefined')?value.class:'';
                                    var span = $('<span class="item '+_class+'" data-id="'+value.id+'"></span>');
                                    var span_delete = $('<span class="sima-icon _delete _16" title="Obriši" onclick="$(this).parent().remove();"></span>');
                                    var span_name = $('<span class="name" title="'+$.trim(value.display_name)+'">'+$.trim(value.display_name)+'</span>');
                                    span.append(span_delete).append(span_name);
                                    ids_list.append(span);
                                });
                            }
                            right_side.append(ids_list);
                        }
                        choose_wrap.append(right_side);

                        //event koji dodaje id u listu
                        choose_wrap.on('add', function(event, data, which_guitable_dbl_clicked){
                            //ako je is_guitable_view onda je dupli klik u guitable-u i data je row nad kojim je izvrsen dupli klik
                            if (response.is_guitable_view)
                            {
                                var clicked_row = data;
                                var clicked_model_id = clicked_row.attr('model_id');
                                var curr_ids = choose_wrap.data('search_model_ids').toString();
                                curr_ids_array = [];
                                if (curr_ids.indexOf(',') >= 0)
                                {
                                    curr_ids_array = curr_ids.split(",");
                                }
                                else if (curr_ids.length > 0)
                                {
                                    curr_ids_array = [curr_ids];
                                }
                                var left_guitable = choose_wrap.find('.sima-model-choose-left-guitable:first');//mora ovako jer nema fiksan id, moze da bude prosledjen
                                var right_guitable = $('#right_tbl_'+uniq_id);
                                
                                if (which_guitable_dbl_clicked === 'LEFT')
                                {
                                    curr_ids_array.push(clicked_model_id);
                                }
                                else if (which_guitable_dbl_clicked === 'RIGHT')
                                {
                                    curr_ids_array.splice($.inArray(clicked_model_id, curr_ids_array), 1);
                                }
                                choose_wrap.data('search_model_ids', curr_ids_array.join());
                                
                                var curr_ids = (curr_ids_array.length > 0) ? curr_ids_array : -1;
                                var left_guitable_curr_fixed_filter = left_guitable.simaGuiTable('getFixedFilter');
                                if (!sima.isEmpty(left_guitable_curr_fixed_filter[1]))
                                {
                                    left_guitable_curr_fixed_filter[1] = {
                                        0: 'NOT', 
                                        1: {
                                            ids: curr_ids
                                        }
                                    };
                                }
                                left_guitable.simaGuiTable('setFixedFilter', left_guitable_curr_fixed_filter);
                                right_guitable.simaGuiTable('setFixedFilter', {ids: curr_ids});
                                
                                sima.model.updateViews(clicked_row.attr('tag'));
                                
                                if (which_guitable_dbl_clicked === 'LEFT')
                                {
                                    right_guitable.simaGuiTable('append_row', clicked_model_id);
                                }
                                else if (which_guitable_dbl_clicked === 'RIGHT')
                                {
                                    left_guitable.simaGuiTable('append_row', clicked_model_id);
                                }
                            }
                            else
                            {
                                $.each(data, function(index, value) {
                                    if (ids_list.find(".item[data-id='" + value.id + "']").length === 0)
                                    {
                                        var span = $('<span class="item" data-id="'+value.id+'"></span>');
                                        var span_delete = $('<span class="sima-icon _delete _16" title="Obriši" onclick="$(this).parent().remove();"></span>');
                                        var span_name = $('<span class="name" title="'+$.trim(value.display_name)+'">'+$.trim(value.display_name)+'</span>');
                                        span.append(span_delete).append(span_name);
                                        ids_list.append(span);
                                    }
                                });
                            }
                        });

                        var ok_button = $("<span class='sima-search-model-choose-ok sima-dialog-button _yes-left'>"+sima.translate('Save')+"</span>");
                        choose_wrap.append(ok_button);
                        //na ok se zatvara dialog i poziva se ok_funkcija kojoj se prosledjuju id-evi
                        choose_wrap.on('click', '.sima-search-model-choose-ok', function(){
                            var ids_objs = [];
                            if (response.is_guitable_view)
                            {
                                var right_guitable = $('#right_tbl_'+uniq_id);
                                var models_ids = [];
                                right_guitable.find('.sima-guitable-row').each(function(){
                                    if (!sima.isEmpty($(this).attr('model_id')))
                                    {
                                        models_ids.push($(this).attr('model_id'));
                                    }
                                });
                                if (!sima.isEmpty(models_ids))
                                {
                                    sima.ajax.get('base/model/getModelsDisplayName',{
                                        get_params:{
                                            model: model
                                        },
                                        data: {
                                            models_ids: models_ids
                                        },
                                        success_function:function(response) {
                                            sima.dialog.close();
                                            ok_function(response.models_display_name);
                                        }
                                    });
                                }
                            }
                            else
                            {
                                ids_list.find('.item').each(function(){
                                    var _class = ($(this).hasClass('_hidden'))?'_hidden':($(this).hasClass('_non_editable'))?'_non_editable':'';
                                    ids_objs.push({'id':$(this).data('id'), 'display_name':$(this).find('.name').text(), class:_class});
                                });
                                sima.dialog.close();
                                ok_function(ids_objs);
                            }
                        });
                        sima.dialog.openInFullScreen(choose_wrap);
                        left_side.append(response.left_side_html);
                        if (response.is_guitable_view)
                        {
                            right_side.append(response.right_side_html);
                        }
                    }
                    //ako je singleselect
                    else
                    {
                        //u ovom slucaju se zatvara dialog i poziva se ok funkcija
                        choose_wrap.on('add', function(event,data){
                            sima.dialog.close();
                            ok_function(data);
                        });
                        sima.dialog.openInFullScreen(choose_wrap);
                        choose_wrap.append(response.left_side_html);
                    }
                    //MilosS(6.3.2018.): mora da ide u setTimeout kako bi se radilo nakon prikaza dijaloga. Mislim da to ne moze da se sredi zbog jQuery
                    setTimeout(function(){ 
                        sima.layout.allignObject(choose_wrap,'TRIGGER_sima_model_choose');
                    }, 0);
                    
                }
                else
                {
                    sima.dialog.openYesNo(choose_wrap[0]['outerHTML'],
                        function(){ //yes_function
                            var id = $("#"+choose_uniq).find(".sima-ui-sf-input").val();
                            var display_name = $("#"+choose_uniq).find(".sima-ui-sf-display").val();
                            var return_value = {'id':id, 'display_name':display_name};
                            
                            var can_be_null = false;
                            if (typeof params.can_be_null !== 'undefined')
                            {
                                can_be_null = params.can_be_null;
                            }
                            
                            if ((id !== '' && display_name !== '') || can_be_null === true)
                            {
                                sima.dialog.close();
                                ok_function(return_value);
                            }
                            else
                            {
                                sima.dialog.openWarn('Niste odabrali vrednost');
                            }
                        }
                    );
                    $("#"+choose_uniq).append(response.left_side_html);
                }
            }
        });
    };
    
    this.onChooseModelDblClick = function(which_guitable_dbl_clicked, multiselect, row)
    {
        var sima_search_model_choose = row.parents('.sima-search-model-choose:first');
        var model = row.attr('model');
        var model_id = row.attr('model_id');
        if (multiselect === true)
        {
            sima_search_model_choose.trigger('add', [row, which_guitable_dbl_clicked]);
        }
        else
        {
            sima_search_model_choose.trigger('add', [[{
                id: model_id,
                display_name: sima.model.getDisplayName(model, model_id)
            }]]);
        }
    };
    
    /*
     * searchForm lista
     * @param obj _this
     * @param obj params
     */
    this.searchFormList = function(_this, params)
    {
        if (!_this.parent().hasClass('_disabled'))
        {
            params = params || {};
            var settings = {};
            var ids = [];
            $('#'+params.uniq).find('.sima-model-contribs .item').each(function(){
                var _class = ($(this).hasClass('_hidden'))?'_hidden':($(this).hasClass('_non_editable'))?'_non_editable':'';
                ids.push({'id':$(this).data('id'), 'display_name':$(this).find('.name').text(), class:_class});
            });
            settings.ids = ids;
            if (typeof params.view !== 'undefined')
                settings.view = params.view;
            if (typeof params.multiselect !== 'undefined' && params.multiselect === true)
                settings.multiselect = true;
            if (typeof params.filter !== 'undefined')
                settings.filter = params.filter;
            if (typeof params.params !== 'undefined')
                settings.params = params.params;
            
            sima.model.choose(params.search_model, function(data){
                var form_list_wrapper = $('#'+params.uniq);
                var is_multiselect = typeof settings.multiselect !== 'undefined' && settings.multiselect === true;
                sima.model.searchFormListSetItems(form_list_wrapper, data, null, is_multiselect);
            }, settings);
        }
    };
    
    this.searchFormListSetItems = function(form_list_wrapper, items, default_item, is_multiselect)
    {
        var _is_multiselect = (typeof is_multiselect !== 'undefined') ? is_multiselect : true;
        var contribs_list = form_list_wrapper.find('.sima-model-contribs');
        var selected_id = contribs_list.find('.item .name._selected').parent('.item:first').data('id');

        if (_is_multiselect === true)
        {
            contribs_list.empty();
        }
        if (!$.isArray(items))
        {
            items = jQuery.makeArray(items);
        }
        $.each(items, function(index, value) {
            if (contribs_list.find(".item[data-id='" + value.id + "']").length === 0)
            {
                var _class = (typeof value.class !== 'undefined')?value.class:'';
                createSpan(form_list_wrapper, value.id, value.display_name, null, _class);
            }
        });
        contribs_list.find(".item[data-id='"+selected_id+"']").find('.name').addClass('_selected');
        createInput(form_list_wrapper);

        triggerFormListEvents(form_list_wrapper);
    };
    
    /**
     * SearchFormList - Dodaje stavku u listu
     * @param {string} search_list_uniq
     * @param {obj} response     
     */
    this.searchFormListAddItem = function(search_list_uniq, response)
    {
        var search_list_wrapper = $('#'+search_list_uniq);
        var search_model = search_list_wrapper.attr('search_model');
        var form_data = response.form_data;
        var model_data = form_data[search_model];
        var contribs_list = search_list_wrapper.find('.sima-model-contribs');
        if (contribs_list.hasClass('single_list'))
        {
            contribs_list.empty();
            search_list_wrapper.children('.add-item').addClass('_hidden');
        }
        createSpan(search_list_wrapper, model_data['attributes'], model_data['display_name']);     
        createInput(search_list_wrapper);
        
        triggerFormListEvents(search_list_wrapper);
    };
    
    /**
     * SearchFormList - Edituje stavku u listi
     * @param {string} search_list_uniq
     * @param {obj} response
     * @param {obj} span     
     */
    this.searchFormListEditItem = function(search_list_uniq, response, span)
    {
        var search_list_wrapper = $('#'+search_list_uniq);
        var search_model = search_list_wrapper.attr('search_model');
        var form_data = response.form_data;
        var model_data = form_data[search_model];        
        
        if (typeof response.model_id !== 'undefined' && response.model_id !== null)
        {
            var id_data = model_data['attributes'];
            id_data.id = response.model_id;
            span.data('id', id_data);
        }
        else
        {
            span.data('id', model_data['attributes']);
        }
        createSpan(search_list_wrapper, model_data['attributes'], model_data['display_name'], span);     
        createInput(search_list_wrapper);
        
        triggerFormListEvents(search_list_wrapper);
    };    
    
    /**
     * SearchFormList - setuje default stavku liste
     * @param {obj} _this     
     */
    this.setFormListDefaultItem = function(_this)
    {       
        if (!_this.hasClass('_selected') && _this.hasClass('_clickable') && !_this.hasClass('_disabled'))
        {
            var search_list_wrapper = _this.parents('.sima-model-form-search-list:first');
            var contribs_list = search_list_wrapper.find('.sima-model-contribs');
            var default_item_params = contribs_list.data('default_item_params');
            default_item_params['value'] = _this.parent().data('id');
            contribs_list.data('default_item_params', default_item_params);
            contribs_list.find('.item ._selected').removeClass('_selected');
            _this.addClass('_selected');            
            createInput(search_list_wrapper);
        }
    }; 
    
    /**
     * SearchFormList - pomocna funkcija koja brise stavku iz liste
     * @param {obj} _this     
     */
    this.removeContrib = function(_this) {
        var search_list_wrapper = _this.parents('.sima-model-form-search-list:first');
        if (!search_list_wrapper.hasClass('_disabled'))
        {
            var contribs_list = _this.parents('.sima-model-contribs:first');            
            if (contribs_list.hasClass('single_list'))
            {
                search_list_wrapper.children('.add-item').removeClass('_hidden');
            }
            _this.parent().remove();        
            createInput(search_list_wrapper);
            
            triggerFormListEvents(search_list_wrapper);
        }
    };
    
    /**
     * SearchFormList - pomocna funckcija koja kreira stavku u listi
     * @param {obj} search_list_wrapper
     * @param {string/obj} id
     * @param {string} display_name
     * @param {obj} span
     * @param {string} _class     
     */
    function createSpan(search_list_wrapper, id, display_name, span, _class)
    {
        var contribs_list = search_list_wrapper.find('.sima-model-contribs');
        display_name = sima.htmlEntities(display_name);
        var search_model = contribs_list.parents('.sima-model-form-search-list:first').attr('search_model');
        if (typeof _class === 'undefined')
            _class = '';
        
        if (typeof span === 'undefined' || span === null)
        {
            var _id = (typeof id === 'object')?JSON.stringify(id):id;
            span = $("<span class='item dots "+_class+"' data-id='"+_id+"'></span>");
        }
        var span_edit = '';
        if (search_list_wrapper.attr('relType')!=='CManyManyRelation')
        {
            if (typeof id === 'object')
            {
                span_edit = $('<div id="'+sima.uniqid()+'" class="sima-old-btn _half"><span class="sima-old-btn-icon sima-icon _edit _16" title="Izmeni"></span></div>');
                span_edit.on('click', function() {
                    var init_data = {};
                    init_data[search_model] = id;                
                    sima.model.form(search_model, '', {
                        init_data:init_data,
                        status:'validate',
                        onSave:function(response) {                        
                            sima.model.searchFormListEditItem(search_list_wrapper.attr('id'), response, span);
                        }
                    });
                });            
            }
            else
            {
                span_edit = $('<div id="'+sima.uniqid()+'" class="sima-old-btn _half"><span class="sima-old-btn-icon sima-icon _edit _16" title="Izmeni"></span></div>');
                span_edit.on('click', function() {
                    sima.model.form(search_model, id,{
                        status:'validate',
                        onSave:function(response) {                        
                            sima.model.searchFormListEditItem(search_list_wrapper.attr('id'), response, span);
                        }
                    });
                });
            }
        }
        
        var span_delete = $('<span class="sima-icon _delete _16" title="Obriši" onclick="sima.model.removeContrib($(this));"></span>');
        var span_name = $('<span class="name" title="'+$.trim(display_name)+'">'+$.trim(display_name)+'</span>');
        span.empty().append(span_edit).append(span_delete).append(span_name);
        contribs_list.append(span);
    }
    
    /**
     * SearchFormList - pomocna funkcija koja kreira skriveni input koji se koristi prilikom pamcenja forme
     * @param {obj} search_list_wrapper     
     */
    function createInput(search_list_wrapper)
    {
        var base_model = search_list_wrapper.attr('base_model');
        var relName = search_list_wrapper.attr('relName');
        var contribs_list = search_list_wrapper.find('.sima-model-contribs');
        var input_value_obj = search_list_wrapper.find('.mfslinput');
        var ids = [];
        contribs_list.find('.item').each(function(){
            var curr_id = {};
            curr_id.id=$(this).data('id');
            curr_id.display_name=$(this).find('.name').text().replace(/'/g, '').replace(/"/g, '');            
            curr_id.class='_visible';
            ids.push(curr_id);
        });
        //kreira se input
        var input_value = {};
        input_value['ids'] = ids;
        input_value['default_item'] = contribs_list.data('default_item_params');
        var input_value_json = JSON.stringify(input_value);
        var input = $("<input class='mfslinput' type='hidden' name='"+base_model+"["+relName+"][ids]' value='"+input_value_json+"'>");
        input.data(input_value_obj.data());
        input_value_obj.remove();
        search_list_wrapper.append(input);
    }
    
    function triggerFormListEvents(search_list_wrapper)
    {
        var items_ids = [];
        search_list_wrapper.find('.item').each(function(){
            items_ids.push($(this).data('id'));
        });

        search_list_wrapper.trigger("itemsChanged", [{items_ids:items_ids}]);

        if(sima.isEmpty(items_ids))
        {
            search_list_wrapper.trigger("onEmpty");
        }
    }
    
    this.addEmployee = function(model_id){
        sima.dialog.openYesNo('Da li ste sigurni da želite da dodate osobu kao zaposlenog?', function(){
            sima.dialog.close();
            sima.ajax.get('admin/admin/addEmployee',{
                get_params:{
                    model_id:model_id
                }
            });
        });
    };
    
    this.addUser = function(id, init_username)
    {
        sima.model.form('User', id, {
            formName: 'admin',
            status: 'validate',
            init_data: {
                User: {
                    username: init_username
                }
            },
            onSave: function(response) {
                var username = response.form_data.User.attributes.username;
                var active = response.form_data.User.attributes.active;

                sima.ajax.get('admin/admin/addUser',{
                    get_params:{
                        id: id
                    },
                    data: {
                        attributes: {
                            username: username,
                            active: active
                        }
                    }
                });
            }
        });
    };

    this.formCustomSearch = function(params)
    {
        var model = params.model;
        var model_id = params.model_id;
        var relName = params.relName;
        var action = params.action;
        sima.ajax.get(action,{
            get_params:{
                model:model,
                model_id:model_id,
                relName:relName
            },
            success_function:function(response){
                sima.dialog.openInFullScreen(response.html);
            }
        });
    };
    
    this.addToRelation = function(model, model_id, relName, rel_ids)
    {
        sima.ajax.get('base/model/addToRelation',{
            get_params:{
                model:model,
                model_id:model_id,
                relName:relName
            },
            data:{
                rel_ids:rel_ids
            },
            success_function:function(){
                
            }
        });
    };
    
    this.getDisplayName = function(model, model_id)
    {
        var display_name = '';
        sima.ajax.get('base/model/getModelDisplayName', {
            get_params: {
                model: model,
                model_id: model_id
            },
            success_function: function(response) {
                display_name = response.display_name;
            }
        });
        
        return display_name;
    };
    
    /**
     * 
     * @param {string} model
     * @param {string/int} model_id
     * @param {string} column
     * @param {string} title - Tekst koji ce se prikazati u zaglavlju dialoga
     * @param {array} options - dodatni parametri     
     */
    this.openTinyMce = function(model, model_id, column, title, options)
    {        
        var _title = (typeof title !== 'undefined')?title:'';
        sima.ajax.get("base/model/getColumnValue",{            
            get_params: {
                model:model,
                model_id:model_id,
                column:column
            },
            success_function:function(response){
                sima.dialog.openTinyMce(_title, function(value){
                    sima.ajax.get("base/model/setColumnValue",{            
                        get_params: {
                            model:model,
                            model_id:model_id,
                            column:column
                        },
                        data:{value:value}
                    });
                }, response.value, options);
            }
        });
    };
    
    this.openGuiTable = function(guitable_params, dialog_params)
    {
        sima.ajax.get('base/model/guiTable',{
            data:{
                params:guitable_params
            },
            async: true,
            loadingCircle: $('#body_sync_ajax_overlay'),
            success_function:function(response){
                sima.dialog.openInFullScreen(response.html, dialog_params.title, dialog_params);
            }
        });
    };
    
    this.checkBoxListClick = function(_this)
    {
        var checkBoxList = _this.parents('.sima-form-check-box-list:first');
        var is_checked = _this.is(':checked');
        var value = _this.val();
        var hidden_input = _this.parent().parent().find('.mfslinput');
        var hidden_input_value = jQuery.parseJSON(hidden_input.val());
        
        for (var i in hidden_input_value) 
        {
            if(hidden_input_value[i].value === value)
            {
                hidden_input_value[i].isChecked = is_checked;
                break;
            }
        }
        
        hidden_input.val(JSON.stringify(hidden_input_value));
        
        checkBoxList.trigger("checkedItemsChanged");
    };
    
//    this.onUploadInitializing = function(input_field, event_data)
//    {
//        var params = {
//            key: "SIMAUpload",
//            status: "Initializing"
//        };
//        var params_str = JSON.stringify(params);
//        input_field.attr("value", params_str);
//    };
    
//    this.onUploadStarted = function(input_field, event_data)
//    {
//        var params = {
//            key: "SIMAUpload",
//            status: "Started",
//            transfer_id: event_data.transfer_id
//        };
//        var params_str = JSON.stringify(params);
//        
//        input_field.attr("value", params_str);
//    };
    
//    this.onUploadSuccess = function(input_field, event_data)
//    {
//        var params = {
//            key: "SIMAUpload",
//            status: "Success",
//            temp_file_name: event_data.temp_file_name,
//            file_display_name: event_data.file_display_name,
//            transfer_id: event_data.transfer_id
//        };
//        
//        if(sima.getUseNewRepmanager())
//        {
//            params.rep_manager_id = event_data.rep_manager_id;
//        }
//        
//        var params_str = JSON.stringify(params);
//        
//        input_field.attr("value", params_str);
//    };
    
//    this.onUploadError = function(input_field, event_data)
//    {
//        var params = {
//            key: "SIMAUpload",
//            status: "Error",
//            transfer_id: event_data.transfer_id
//        };
//        var params_str = JSON.stringify(params);
//        
//        input_field.attr("value", params_str);
//    };
    
//    this.onUploadStopped = function(input_field, event_data)
//    {
//        var params = {
//            key: "SIMAUpload",
//            status: "Stopped",
//            transfer_id: event_data.transfer_id
//        };
//        var params_str = JSON.stringify(params);
//        
//        input_field.attr("value", params_str);
//    };
    
//    this.onUploadProgress = function(input_field, event_data)
//    {
//        var params = {
//            key: "SIMAUpload",
//            status: "Progress",
//            transfer_id: event_data.transfer_id
//        };
//        var params_str = JSON.stringify(params);
//        
//        input_field.attr("value", params_str);
//    };
    
    this.renderFullInfo = function(model_name, model_id)
    {
        return sima.model.renderModelView(model_name, model_id, 'full_info_solo');
    };
    
    this.renderModelView = function(model_name, model_id, view_name)
    {
        var html = '';
        sima.ajax.get('base/model/renderModelView',{
            get_params: {
                model_name:model_name,
                model_id:model_id,
                view_name:view_name
            },
            success_function: function(response){
                html = response.html;
            }
        });
        
        return html;
    };
}

