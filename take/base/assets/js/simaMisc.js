/* global sima */

function SIMAMisc()
{
    
    //registracija eventa za span with info
    $('body').on('mouseenter', '.sima-span-with-info', function(){
        var _this = $(this);
        var info_box = $("#info_box"+_this.attr('id'));
        _this.data('hovered', true);
        if(!sima.isEmpty(info_box) && !info_box.is(":visible"))
        {
            if(!sima.isEmpty(_this.data('global_timeout')))
            {
                clearTimeout(_this.data('global_timeout'));
            }
            _this.data('global_timeout', setTimeout(function(){
                //kada se pojavi loading, posmatra se kao da mis vise nije on hover pa pocne ponovno pokretanje funkcije
                //tako da mora da bude zakomentarisan loading
                sima.ajax.get(_this.data('action'), {
                    get_params: _this.data('get_params'),
                    data: _this.data('post_params'),
                    async:true,
                    success_function: function(response) {
                        if (_this.data('hovered') === true || info_box.data('hovered') === true)
                        {
                            info_box.html(response.html);
                            info_box.show().simaPopup('refresh');
                        }
                    }
                });
            },300));
        }
    });
    $('body').on('mouseleave','.sima-span-with-info', function() {
        var _this = $(this);
        _this.data('hovered', false);
        setTimeout(function(){
            var info_box = $("#info_box"+_this.attr('id'));
            if(!sima.isEmpty(info_box) && info_box.data('hovered') !== true)
            {
                if(!sima.isEmpty(_this.data('global_timeout')))
                {
                    clearTimeout(_this.data('global_timeout'));
                }
                info_box.hide('fast');
            }
        }, 1);
    });
    $('body').on('mouseenter','.sima-span-info-box', function() {
        $(this).data('hovered', true);
    });
    $('body').on('mouseleave','.sima-span-info-box', function() {
        var _this = $(this);
        _this.data('hovered', false);
        setTimeout(function(){
            var span = $("#"+$.trim(_this.attr('id').replace('info_box','')));
            if(span.data('hovered') !== true)
            {
                _this.hide('fast');
            }
        }, 1);
    });
    
    this.logOutSession = function(_this) 
    {
        var model_id = _this.data('model_id');
        sima.ajax.get('base/model/setEndTime',{
            get_params:{
                id:model_id               
            }
        });
    };
    
    this.calcDeadlineDialog = function(input_field_selector,comment_selector)
    {
        sima.dialog.openAJAX('calc_deadline',{input_field_selector:input_field_selector,comment_selector:comment_selector});
    };
    
    this.today = function()
    {
        var date = new Date();
        return date.getDate() + '.' + (date.getMonth()+1) + '.' + date.getFullYear() + '.';
    };
    
    this.formatSize = function(bytes)
    {
        var i = 0;
        var extracted_result = 0;
        if(bytes !== 0)
        {
            do {
                bytes = bytes / 1024;
                i++;
            } while (bytes > 99);
            
            extracted_result = Math.max(bytes, 0.1);
        }
        
        var final_result = extracted_result.toFixed(1) + ['B', 'kB', 'MB', 'GB', 'TB', 'PB', 'EB'][i];
        
        return final_result;
    };
    
    this.formatTime = function(seconds)
    {
        seconds = parseInt(seconds, 10);
        
        var final_result_number = seconds;
        var final_result_marker = "s";

        if(seconds > 3600)
        {
            final_result_number = Math.floor(seconds / 3600);
            final_result_marker = "h";
        }
        else if(seconds > 60)
        {
            final_result_number = Math.floor(seconds / 60);
            final_result_marker = "m";
        }
        
        return final_result_number + final_result_marker;
    };
    
    this.calcDeadline = function(dialog_id){
        var date = $('#start_date'+dialog_id).datepicker('getDate');
        var diff = parseInt($('#days'+dialog_id).val());
        var work_days = $('#' + dialog_id + ' input[value="working"]').attr('checked')==='checked';
        if (diff===NaN) diff=0;
        for (var i=0;i<diff;i++)
        {
            if (work_days)
                if (date.getDay()===0 || date.getDay()===6)
                    i--;
            date.setDate(date.getDate() + 1);
        }   
        var result = date.getDate() + '.' + (date.getMonth()+1) + '.' + date.getFullYear() + '.';
        $('#end_date'+dialog_id).val(result);
        return result;
    };
    
    this.addFileFromFileVersion_YesFunc = function(event)
    {
        var params = event.data;
        sima.dialog.close();
        sima.ajax.get('files/file/addFileFromVersion', {
             get_params:{
                 version_id:params.version_id
             },
             async: true,
             loadingCircle: $('#body_sync_ajax_overlay'),
             success_function:function(response){
                 sima.dialog.openAction('files/file',response.model_id);
             }
          });
    };
    
    this.addFileFromFileVersion=function(version_id)
    {
        sima.dialog.openYesNo("Da li zaista želite da napravite novi fajl od ove verzije?", sima.misc.addFileFromFileVersion_YesFunc, null, {
            version_id:version_id
        });
    };
    
    //funkcija koja se poziva onSave za editovanje access-a u privilegijama
    this.funcEditAccess = function(response)
    {
        $('.auth_designer_items_'+response.model_id).parents('.content:first').simaAccessTree('refresh');
        $('.auth_designer_items_'+response.model_id).parents('.content:first').simaAccessTree('deactiveVersion');
    };
    //funkcija koja se poziva onSave za editovanje modula u privilegijama
    this.funcEditModul = function(response)
    {
        $('.designer_moduls_'+response.model_id).parents('.content:first').simaAccessTree('refresh');
        $('.designer_moduls_'+response.model_id).parents('.content:first').simaAccessTree('deactiveVersion');
    };
    
    this.addNewFileFromMenu = function(responsible_id)
    {
        sima.model.form('File', '', {
           init_data: {
               File: {
                    responsible_id:responsible_id
               }
           },
           onSave:function(response){
               if (typeof response.model_id !== 'undefined')
               {
                   sima.dialog.openModel('files/file',response.model_id);
               }
           }
            
        });
    };
    
    /// MilosJ: ne koristi se
//    this.regenerateWikiPass = function(user_id)
//    {
//        sima.ajax.get('admin/admin/regenerateWikiPass',{
//            get_params:{
//                user_id:user_id
//            }
//        });
//    };
    
    this.addEmailAccountToUser=function(account_id, user_id)
    {
        sima.ajax.get('messages/emailAccount/addEmailAccountToUser', {
             get_params:{
                 account_id:account_id,
                 user_id:user_id
             },
             success_function:function(response){
             }
        });
    };
    
    this.setConfigParamToDefault_YesFunc=function(event)
    {
        sima.dialog.close();
        sima.ajax.get('admin/configParam/removeConfigParam', {
             get_params:{
                 config_path:event.data.params.path,
                 config_type:event.data.params.type_config
             },
             success_function:function(response)
             {
                 sima.dialog.openInfo('Da bi se primenile promene, \n\
                                       potrebno je da osvežite stranicu, pritiskom na F5.','Pritisnite F5');
             }
        });
        
        event.data.value_field.find('.value_conf_param').html(event.data.params.default_display);
    };
    
    this.setUserEmailToReadNotif=function(user_to_account_id, add)
    {
        if(typeof(add)==='undefined'){ add=true; }
            sima.ajax.get('messages/emailAccount/setUserEmailToReadNotif', {
                 get_params:{
                     user_to_account_id:user_to_account_id,
                     add:add
                 },
                 success_function:function(response)
                 {
                      sima.model.updateViews(response.updateViews);
                 }
            });
    };
    
    this.addNewBillOfExchange=function(number, link)
    {
         sima.model.form('BillOfExchange', '',{
             init_data:{
                 BillOfExchange: {
                    number:number
                }
             },
             onSave:function(response)
             {   
                 var _table=link.parents('.sima-guitable');
                 _table.simaGuiTable('inst_populate');
             }
         });
    };
    this.addAuthorizationFileToBillOfExchange_YesFunc=function(event)
    {
        var params = event.data;
        sima.dialog.close();
        sima.ajax.get('accounting/accounting/addAuthorizationFileToBillOfExchange', {
                get_params:{
                    bill_of_exchange_id:params.id
                },
                success_function:function(response)
                {
                     sima.model.updateViews(response.updateViews);
                }
        });
    };
    
    this.addAuthorizationFileToBillOfExchange=function(id, link)
    {
        sima.dialog.openYesNo("Da li zaista želite da dodate novo ovlašćenje za menicu?", sima.misc.addAuthorizationFileToBillOfExchange_YesFunc, null,{
                id:id,
                link:link
        });
    };
    
    this.reCheckContribTagsInFiling = function (changes)
    {
        var uniq_id = sima.uniqid();
        sima.ajax.get('files/file/reCheckContribTagsInFiling',{
            get_params:{
                uniq_id : uniq_id
            },
            data:{changes:changes},
            async:true,
            success_function: function(response){
                var html = {};
                var yes_func_params = {};
                var no_func_params = {};
                html.title = 'Provera tagova';
                html.html = response.html;
                yes_func_params.func = function onConfirm(obj) {
                    var wrap = $('#'+uniq_id);
                    var contribs = [];
                    wrap.find('.contrib').each(function(){
                        var _contrib = {};
                        _contrib.id = $(this).data('id');
                        _contrib.tags = [];
                        $(this).find('.tag').each(function(){
                            var _tag = {};
                            _tag.id = $(this).data('id');
                            if ($(this).find('.add').length > 0)
                            {
                                if ($(this).find("input[type='checkbox']").is(':checked'))
                                    _tag.add = true;
                                else
                                    _tag.add = false;
                            }
                            else if ($(this).find('.remove').length > 0)
                            {
                                if ($(this).find("input[type='checkbox']").is(':checked'))
                                    _tag.add = false;
                                else
                                    _tag.add = true;
                            }
                            _contrib.tags.push(_tag);
                        });
                        contribs.push(_contrib);
                    });
                    
                    sima.ajax.get('files/file/saveContribTagsInFiling',{
                        get_params:{
                        },
                        data:{contribs:contribs},
                        success_function: function(response){
                            sima.dialog.openInfo('Sačuvano');
                        }
                    });
                    sima.dialog.close();
                };
                yes_func_params.title = "Sačuvaj";
                no_func_params.hidden = true;
                sima.dialog.openYesNo(html, yes_func_params, no_func_params);
            }
        });
    };
    
    this.searchFieldAddButton = function(id, params)
    {       
        if (!$(this).hasClass('_disabled'))
        {
            if(!sima.isEmpty(params.search_action))
            {
                sima.dialog.openActionInDialog(params.search_action, params);
            }
            else
            {
                sima.model.choose(params.model, function(data){
                    var value = data[0].id;
                    var display = data[0].display_name;
                    $('#'+id).simaSearchField('setValue', value, display);
                }, params);
            }
        }
    };
    
    this.showIksTable = function(_this, person_id)
    {
        if (!_this.hasClass('_disabled'))
        {
            sima.ajax.get('user/showIksTable',{
                get_params:{
                    person_id:person_id
                },
                success_function:function(response){
                    sima.dialog.openInFullScreen(response.html);
                }
            });
        }
    };
    
    /**
     * 
     * @param {type} _this
     * @param {type} table_id
     * @returns {undefined}
     */
    this.searchModelText = function(_this, table_id)
    {
        var text = _this.val();
        var params = {};
        params.text = text;
        $('#'+table_id).simaGuiTable('populate', params);
    };
    
    this.refreshUiTabForObject = function(obj, parent_order)
    {
        var tabs = obj.parents('.sima-ui-tabs:first');
        if (typeof parent_order === 'undefined')
        {
            parent_order = 1;
        }
        for(var _i = 1; _i < parent_order; _i++)
        {
            var new_tabs = tabs.parents('.sima-ui-tabs:first');
            if (typeof new_tabs !== 'undefined')
            {
                tabs = new_tabs;
            }
        }
        var activeTabIndex = tabs.find('.sima-ui-tabs-nav:first .sima-ui-active').index();

        tabs.simaTabs('select_tab', activeTabIndex+1, true);
    };
    
    this.refreshMainTabForObject = function(obj)
    {
        if (sima.useNewLayout())
        {
            var display_layout_panel = obj.parents('.sima-layout-display-panel:first');
            if (!sima.isEmpty(display_layout_panel) && typeof display_layout_panel[0].__vue__ !== 'undefined')
            {
                display_layout_panel[0].__vue__.loadActionHtml();
            }
        }
        else
        {
            var activeTabIndex = obj.parents('#sima-main-tabs').find('.sima-main-tabs-nav .sima-ui-active:first').index();

            if(!sima.isEmpty(activeTabIndex) && activeTabIndex !== -1)
            {
                sima.mainTabs.select_tab(activeTabIndex, true);
            }
        }
    };
    
    this.getMainTabContentForObject = function(obj)
    {
        var panel;

        if (sima.useNewLayout())
        {
            panel = obj.parents('.sima-layout-display-panel:first');
        }
        else
        {
            var main_tab = obj.parents('#sima-main-tabs').find('.sima-main-tabs-nav .sima-ui-active:first');
            var main_tab_index = main_tab.index();
            var content_index = sima.mainTabs.getTabPanelIndexByTabIndex(main_tab_index);
            panel =  $("#sima-main-tabs").find('div.sima-main-tabs-panel:eq(' + content_index + ')');
        }
        
        return panel;
    };
    
    this.isInDialog = function(obj)
    {
        return obj.parents('.ui-dialog').length > 0;
    };
    this.getDialogParent = function(obj)
    {
        return obj.parents('.ui-dialog');
    };
    
    //uporedjuje dva datetime i vraca true ako je prvi veci, inace false
    this.compareDateTime = function(datetime1, datetime2)
    {        
        if (typeof datetime1 !== 'undefined' && typeof datetime2 !== 'undefined')
        {
            var new_datetime1 = $.trim(datetime1.replace(/\.|:|\s+/g, '/').replace("//", '/'));
            var new_datetime2 = $.trim(datetime2.replace(/\.|:|\s+/g, '/').replace("//", '/'));
            var arrdt1 = new_datetime1.split("/");
            var arrdt2 = new_datetime2.split("/");            
            var datedt1 = new Date(arrdt1[2], arrdt1[1] - 1, arrdt1[0], arrdt1[3], arrdt1[4], arrdt1[5]);
            var datedt2 = new Date(arrdt2[2], arrdt2[1] - 1, arrdt2[0], arrdt2[3], arrdt2[4], arrdt2[5]);
            if (datedt1 > datedt2)
            {                
                return true;
            }
            else
            {                
                return false;
            }
        }
        return true;
    };
    
    this.initStaticInfoBox = function(id, info_box_id)
    {
        //Renderovanje notifikacija u modelu se vrsi preko vue prop-a (preko commit f-je), tako da se prvo izvrsi js, pa se tek onda html ubaci u dom.
        //U vue prop-u ne bi smelo da koristimo html, tako da treba prebaciti renderovanje notifikacija u modelu u vue komponentu. Do tada je resenje setTimeout.
        setTimeout(function() {
            var model_link_wrap = $('#'+id);
            var popup_obj = $('#'+info_box_id);
            popup_obj.simaPopup('init',$('body'),model_link_wrap);
            popup_obj.hide();
            model_link_wrap.on('mouseenter', function(){
                if(!popup_obj.is(":visible"))
                {
                    popup_obj.show();
                    popup_obj.simaPopup('refresh');
                }
            });
            model_link_wrap.on('mouseleave', function(){
              setTimeout(function() {
                    if(popup_obj.data('hovered') !== true)
                    {
                        popup_obj.hide('fast');
                    }
                }, 1);
            });
            popup_obj.on('mouseenter', function(){
                $(this).data('hovered', true);
            });
            popup_obj.on('mouseleave', function(){
                $(this).data('hovered', false);
                popup_obj.hide('fast');
            });
        }, 0);
    };
    
    this.copyToClipboard = function(text)
    {
        var $temp = $("<input>");
        $("body").append($temp);
        $temp.val(text).select();
        document.execCommand("copy");
        $temp.remove();
    };
    
    this.displaySimplePopupBox = function(message)
    {
        var uniq_id = 'popup_'+sima.uniqid();
        var popup = $('<div id='+uniq_id+'></div>');
        popup.css('border', '1px solid black');
        popup.css('border-radius', '25px');
        popup.css('background-color', 'yellow');
        popup.css('position', 'absolute');
        popup.css('bottom', 0);
        popup.css('right', 0);
        popup.css('width', '200px');
        popup.css('height', '50px');
        popup.css('text-align', 'center');
        popup.css('z-index', 1005);
        popup.html(message);
        var close_button = $('<span><b>X</b></span>');
        close_button.css('position', 'absolute');
        close_button.css('top', 0);
        close_button.css('right', 10);
        close_button.css('cursor', 'pointer');
        popup.append(close_button);
        
        close_button.on('click', function(){
            $('#'+uniq_id).remove();
        });
        
        $('body').append(popup);
        
        setTimeout(function(){
            $('#'+uniq_id).remove();
        }, 1000);
    };
    
    this.themeList = function(code,div_selector)
    {
        if (!isNaN(parseInt(code)))
        {
            sima.ajax.get('base/theme/fullInfo',{
                get_params: {
                    theme_id: code
                },
                success_function: function(response){
                    sima.set_html($(div_selector),response.html);
                }
            });
        }
    };
    
    this.fillFromNbsInForm = function(button_obj_id, company_id)
    {
        var button_obj = $('#'+button_obj_id);
        var form = button_obj.parents('.sima-model-form:first');
        var pib_field = form.find('input[name="Company[PIB]"]');
        var mb_field = form.find('input[name="Company[MB]"]');
        var name_field = form.find('input[name="Company[name]"]');
        var full_name_field = form.find('textarea[name="Company[full_name]"]');
        var accounts_field = form.find('.sima-model-form-row[data-column="bank_accounts"] .sima-model-form-search-list:first');
                
        var fill_by_pib = true;
        if (button_obj.parents('.sima-model-form-row[data-column="MB"]').length > 0)
        {
            fill_by_pib = false;
        }
        
        sima.ajax.get('base/base/fillFromNbsInForm',{
            get_params: {
                fill_by_pib: fill_by_pib,
                company_id: company_id
            },
            data: {
                pib: pib_field.val(),
                mb: mb_field.val()
            },
            success_function: function(response) {
                if (!sima.isEmpty(response.name))
                {
                    name_field.val(response.name).change();
                }
                full_name_field.val(response.full_name).change();
                pib_field.val(response.pib).change();
                mb_field.val(response.mb).change();
                sima.model.searchFormListSetItems(accounts_field, response.accounts);
            }
        });
    };
    
    this.calcSystemPartnerList = function(system_partner_list_code)
    {
        var progress_bar_obj = sima.getLastActiveFunctionalElement();
        if (sima.isEmpty(progress_bar_obj))
        {
            progress_bar_obj = $('body');
        }
        
        sima.ajaxLong.start('base/base/calcSystemPartnerList', {
            showProgressBar: progress_bar_obj,
            data: {
                system_partner_list_code: system_partner_list_code
            },
            onEnd: function(response) {
            }
        });
    };
    
    this.addUsersToPartnerList = function(partner_list_id)
    {
        sima.model.choose(
            'User', 
            function(data) {
                if (data.length > 0)
                {
                    sima.ajax.get('base/base/addUsersToPartnerList', {
                        get_params: {
                            partner_list_id: partner_list_id
                        },
                        data: {
                            users: data
                        },
                        async:true,
                        success_function:function(response) {
                        }
                    });
                }
            },
            {
                view: 'guiTable',
                multiselect: true,
                params: {
                    add_button: false
                },
                filter: {
                    filter_scopes: 'onlyActive'
                }
            }
        );
    };
    
    /**
     * Proverava da li je bar neki deo teksta selektovan u inputu
     * @param {Javascript input element} input
     * @returns {Boolean}
     */
    this.isInputTextSelected = function(input)
    {
        var startPos = input.selectionStart;
        var endPos = input.selectionEnd;
        var doc = document.selection;

        if(doc && doc.createRange().text.length !== 0)
        {
           return true;
        }
        else if (!doc && input.value.substring(startPos,endPos).length !== 0)
        {
           return true;
        }
        
        return false;
     };
}

function SIMAScrollManager()
{
    var scroll_objects = [];
    var scroll_increment = 50; //korak za koji se uvecava/smanjuje skrol
    var scroll_value = null;
    var animation_time = 500;
    
    this.addObject = function(object)
    {
        scroll_objects.push(object);
    };
    
    this.setScrollTop = function(new_scroll_value)
    {
        scroll_value = new_scroll_value;
    };
    
    this.removeObject = function(scroll_obj)
    {
        $.each(scroll_objects, function(index, obj) {
            if (obj.is(scroll_obj) || !jQuery.contains(document, obj[0]))
            {
                scroll_objects.splice(index, 1);
                return false;
            }
        });
    };
    
    this.syncScroll = function(scroll_obj, delta, animation)
    {
        max_scroll = 0;
        scroll_obj.children().each(function(){
            max_scroll += $(this).outerHeight(true);
        });
        var max_scroll = max_scroll - scroll_obj.height() + 15;
        if (scroll_value === null)
            scroll_value = scroll_obj.scrollTop();
        if (typeof delta !== 'undefined')
        {
            scroll_value = delta>0 ? scroll_value-=scroll_increment : scroll_value+=scroll_increment;
        }
        if (scroll_value < 0 || max_scroll < 0)
            scroll_value=0;
        if (max_scroll > 0 && scroll_value > max_scroll )
            scroll_value=max_scroll;

        if (typeof animation !== 'undefined')
            animation_time = animation;
        $.each(scroll_objects, function(index, obj) {
            obj.stop().animate({scrollTop:scroll_value}, animation_time, 'swing');
        });
    };
}
