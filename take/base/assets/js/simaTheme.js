
/* global sima */

function SIMATheme()
{
    this.convertThemeTo = function(theme_id, model_name)
    {
        sima.ajax.get('base/theme/convertTo',{
            get_params: {
                theme_id: theme_id,
                model_name: model_name
            }
        });
    };
    
    this.renderThemeSettingsView = function(theme_id)
    {
        sima.ajax.get('base/theme/renderThemeSettingsView',{
            get_params: {
                theme_id: theme_id
            },
            success_function: function(response) {
                
                sima.dialog.openYesNo(
                {
                    title: sima.translate('ThemeSettings'),
                    html: response.html
                }, 
                {
                    title: sima.translate('Save'),
                    func: function() {
                        var wrap = $('#'+response.uniq_id);
                        var settings = {};
                        wrap.find('.theme-settings-item').each(function() {
                            if (!$(this).hasClass('_disabled'))
                            {
                                if ($(this).data('type') === 'boolean')
                                {
                                    settings[$(this).data('code')] = $(this).find('.theme-settings-item-value > input').prop('checked');
                                }
                            }
                        });

                        sima.dialog.close();

                        sima.ajax.get('base/theme/saveThemeSettings',{
                            get_params: {
                                theme_id: theme_id
                            },
                            data: {
                                settings: settings
                            },
                            success_function: function(response) {

                            }
                        });
                    }
                }, 
                {
                    hidden: true
                }, null, null, false);
            }
        });
    };
    
    this.themeJobTypeAndCostLocationDependencyInForm = function(form, dependent_on_field, dependent_field, apply_actions_func)
    {
        var parent_theme_value = dependent_on_field.simaSearchField('getValue');
        if (!sima.isEmpty(parent_theme_value.id))
        {
            sima.ajax.get('base/theme/getParentThemeJobTypeAndCostLocation', {
                get_params: {
                    parent_theme_id: parent_theme_value.id
                },
                success_function: function(response) {
                    var dependent_field_row = dependent_field.parents('.sima-model-form-row:first');
                    
                    apply_actions_func('disable');
                    if (dependent_field_row.data('column') === 'job_type_id')
                    {
                        dependent_field.val(response.job_type_id).change();
                    }
                    else if (dependent_field_row.data('column') === 'cost_location_id')
                    {
                        dependent_field.simaSearchField('setValue', response.cost_location_id, response.cost_location_name);
                    }
                }
            });
        }
        else
        {
            apply_actions_func('enable');
        }
    };
    
    this.addCostControllerToFileSignatures = function(files_count, user_name, theme_name, user_id, theme_id)
    {
        sima.dialog.openYesNo(
            sima.translate('DoYouWantAddUserToSignaturesOfAllBillsOfTheme', {
                '{files_count}': files_count,
                '{user_name}': user_name,
                '{theme_name}': theme_name
            }),
            function(){
                sima.dialog.close();
                sima.ajax.get('base/theme/addCostControllerToFileSignatures',{
                    get_params: {
                        user_id: user_id,
                        theme_id: theme_id
                    },
                    success_function: function(response){}
                });
            }
        );
    };
    
    this.personOnCostControllerHendler = function(form, dependent_on_field, dependent_field, apply_actions_func)
    {
        var person_id = dependent_on_field.simaSearchField('getValue').id;
        
        sima.ajax.get('base/theme/personOnCostControllerHendler',{
            get_params: {
                person_id: person_id
            },
            success_function: function(response){
                if(response.is_user === false)
                {
                    apply_actions_func('hide');
                    dependent_field.prop('checked', false);
                }
                else
                {
                    apply_actions_func('show');
                }
            }
        });
    };
    
}
