/* global sima, Vue, Vuex */

function SIMAVue()
{
    this.init = function()
    {
        this.shot = SIMAVueShotConstructor(Vue, Vuex.Store, sima.ajax.get, sima.errorReport);
        $(sima.eventrelay).on('onopen', onEventRelayConnectonOpen);
    };
    
    this.isVueSIMAButton = function(obj)
    {
        var result = false;
        if(obj.hasClass('_vue'))
        {
            result = true;
        }
        return result;
    };
    
    this.getVueComponentFromObj = function(obj)
    {
        var result = null;
        
        if(typeof obj[0].__vue__.$refs !== 'undefined' && Object.keys(obj[0].__vue__.$refs).length > 0)
        {
            result = obj[0].__vue__;
        }
        else if(typeof obj[0].__vue__.$children[0].$refs !== 'undefined' && Object.keys(obj[0].__vue__.$children[0].$refs).length > 0)
        {
            result = obj[0].__vue__.$children[0];
        }
        
        return result;
    };
    
    this.objHasClass = function(obj, class_name) {
        if (this.isVueSIMAButton(obj))
        {
            return this.getVueComponentFromObj(obj).hasClass(class_name);
        }
        else
        {
            return obj.hasClass(class_name);
        }
    };
    
    this.objAddClass = function(obj, class_name) {
        if (this.isVueSIMAButton(obj))
        {
            this.getVueComponentFromObj(obj).addClass(class_name);
        }
        else
        {
            obj.addClass(class_name);
        }
    };
    
    this.objRemoveClass = function(obj, class_name) {
        if (this.isVueSIMAButton(obj))
        {
            this.getVueComponentFromObj(obj).removeClass(class_name);
        }
        else
        {
            obj.removeClass(class_name);
        }
    };
    
    this.ProxyValidator = function(){
        return 'success';
    };
    
    this.isVueModelArraysAreSameById = function(array1, array2) {
        var array1_ids = [];
        for(var i = 0; i < array1.length; i++)
        {
            array1_ids.push(array1[i].id);
        }

        var are_same = true;
        for(var j = 0; j < array2.length; j++)
        {
            if ($.inArray(array2[j].id, array1_ids) === -1)
            {
                are_same = false;
                break;
            }
        }

        return are_same;
    };
    
    this.getVueModelTag = function(model_name, model_id) {
        var model_ids = '';
        if ($.isArray(model_id))
        {
            $.each(model_id, function(index, value) {
                model_ids += ('_' + value);
            });
        }
        else
        {
            model_ids = model_id;
        }

        return model_name + '_' + model_ids;
    };
    
    function onEventRelayConnectonOpen(){
        sima.vue.shot.commit("clear_models");
    }
    
    this.getBaseComponent = function() {
        return {
            data: function () {
                return {
                    is_component_instance_visible: false
                };
            },
            computed: {
                component_instance_visibility: function() {
                    var return_object = {};
                    return_object[this._uid] = this.is_component_instance_visible;

                    return return_object;
                }
            },
            mounted: function() {
                var _this = this;
                
                $(this.$el).on('destroyed', function(){
                    _this.$destroy();
                });
            },
            destroyed: function() {
                sima.vue.shot.commit('remove_component_instance_from_visibility', this._uid);
            },
            methods: {
                componentInstanceVisibilityChanged: function(is_visible, entry) {
                    this.is_component_instance_visible = is_visible;
                }
            }
        };
    };
    
    this.getStatusComponent = function() {
        return {
            computed: {
                _status: function() {
                    if (typeof this.status === 'string') //dok se ne dovuce sa servera
                    {
                        return {};
                    }
                    else
                    {
                        return this.status;
                    }
                },
                is_revert: function() {
                    if (this._status.allow_null === false)
                    {
                        return this._status.value === true;
                    }
                    else
                    {
                        return this._status.value === true || this._status.value === false;
                    }
                },
                getOnClickMessage: function() {
                    if (this.is_revert)
                    {
                        message = sima.translate('ModelStatusRevertMessage');
                    }
                    else if (this._status.allow_null === false)
                    {
                        message = sima.translate('ModelStatusConfirmMessage');
                    }
                    else
                    {
                        message = sima.translate('ModelStatusConfirm2Message');
                    }

                    return message;
                },
                status_model: function() {
                    if (sima.isEmpty(this.status.model) || sima.isEmpty(this.status.model_id))
                    {
                        return null;
                    }
                    return sima.vue.shot.getters.model(this.status.model + '_' + this.status.model_id);
                },
                status_access: function() {
                    if (this.status_model === null)
                    {
                       return null;
                    }
                    return this.status_model.scoped('status_access', {status_column: this.status.column, is_revert: this.is_revert});
                },
                has_access: function() {
                    return (this.status_access === null || this.status_access.length !== 0) ? false : true;
                },
                is_has_access_ready: function() {
                    if (this.status_model === null)
                    {
                        return false;
                    }
                    return this.status_model.shotStatus('status_access', {status_column: this.status.column, is_revert: this.is_revert}) === 'OK';
                }
            },
            mounted: function() {
                var _this = this;
                $(this.$el).on('destroyed', function(){
                    _this.$destroy();
                });
                
                setTimeout(function(){
                    _this.status_access;
                }, 500);
            },
            methods: {
                confirmStatus: function(new_status) {
                    if (this.has_access === false && !this.is_has_access_ready)
                    {
                        return;
                    }
                    sima.ajax.get('base/model/modelConfirm', {
                        get_params: {
                            model: this._status.model,
                            column: this._status.column,
                            new_status: new_status
                        },
                        data: {
                            model_ids: this._status.model_id
                        },
                        async: true,
                        loadingCircle: $('#body_sync_ajax_overlay'),
                        success_function: function() {
                            sima.dialog.close();
                        }
                    });
                }
            }
        }
    };
}
