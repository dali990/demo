/* global sima */

function SIMAErrorReport()
{
    this.init = function()
    {
        
    };

    this.addError = function(params, add_desc)
    {
        if (sima.isF5Pressed === true)
        {
            return;
        }
        var _add_desc = (typeof add_desc === 'undefined') ? true : add_desc;
        sima.ajax.get('base/errorReports/addError',{
            data:params,
            async: true,
            success_function:function(response){
                if (_add_desc === true && !sima.isEmpty(response.error_id))
                {
                    sima.errorReport.addErrorDesc(response.error_id, response.client_message);
                }
            }
        });
    };
    
    this.editError = function(error_id, attributes, add_desc)
    {
        var _add_desc = (typeof add_desc === 'undefined') ? true : add_desc;
        sima.ajax.get('base/errorReports/editError',{
            get_params: {
                error_id: error_id
            },
            data: {
                attributes: attributes
            },
            success_function:function(response){
                if (_add_desc === true)
                {
                    sima.errorReport.addErrorDesc(error_id, response.client_message);
                }
            }
        });
    };

    this.addErrorDesc = function(error_id, message)
    {
        if (!sima.isEmpty(error_id))
        {
            var ta = $("<textarea class='sima-dialog-error-textbox'></textarea>");
            var html = $("<div></div>");
            html.append("<p class='sima-ui-dialog-part'>GREŠKA!!!!</p>"
                    +"<p class='sima-ui-dialog-part'>Unesite opis situacije u kojoj je greška nastala:</p>");

            html.append(ta);

            html.append("<p class='sima-ui-dialog-part'>Tekst greške:</p>"
                    +"<p class='sima-ui-dialog-part'>"+message+"</p>");
            sima.dialog.openAlert(html,'Greška',{
                title: 'Prijavi',
                function:function(){
                    sima.ajax.get("base/errorReports/addErrorDesc",{
                        get_params:{error_id:error_id},
                        data: {
                            description : ta.val()
                        },
                        async: true,
                        success_function:function(){
                            sima.dialog.openInfo("Hvala na prijavljenoj grešci!");
                        }
                    });

                }
            });
        }
    };
    
    this.sendError = function(error_id)
    {
        sima.ajax.get("base/errorReports/sendError",{
            get_params:{error_id:error_id},
            success_function:function(){
                sima.dialog.openInfo("Greška je poslata.");
            }
        });
    };
    
    this.sendAllUnsentErrors = function(_this)
    {
        var _guitable = _this.parents('.sima-guitable:first');
        sima.ajaxLong.start("base/errorReports/sendAllUnsentErrors", {
            data: {},
            showProgressBar: _guitable,
            onEnd: function(){
                sima.dialog.openInfo("Poslate su sve greške.");
                _guitable.simaGuiTable('populate');
            }
        });
    };
    
    this.addClientBafRequest = function(user_id, foreign_user_id)
    {
        if (!sima.isEmpty(foreign_user_id))
        {
            sima.model.form('BafRequest','',{
                'formName': 'typeNew',
                'init_data': {
                    'BafRequest': {
                        'requested_by_id': foreign_user_id,
                        'coordinator_id': user_id,
                        theme_id: {
                            model_filter: {
                                scopes: 'onlyImportantThemes'
                            }
                        }
                    }
                },
                'onSave': function() {
                    sima.dialog.openInfo(sima.translate('ResponseForSubmitedRequsest'));
                }
            });
        }
        else
        {
            sima.model.form('ClientBafRequest','',{
                'init_data':{
                    'ClientBafRequest':{
                        'requested_by_id':{
                            'disabled':'disabled',
                            'init':user_id
                        }
                    }
                },
                'onSave': function() {
                    sima.dialog.openInfo(sima.translate('ResponseForSubmitedRequsest'));
                }
            });
        }
    };
    
    this.sendClientBafRequest = function(client_baf_request_id)
    {
        sima.ajax.get("base/errorReports/sendClientBafRequest",{
            get_params:{client_baf_request_id:client_baf_request_id},
            async: true,
            loadingCircle: $('body'),
            success_function:function(){
                sima.dialog.openInfo("Zahtev je poslat.");
            }
        });
    };
    
    this.sendAllUnsentClientBafRequests = function(_this, model_filter)
    {
        var _guitable = _this.parents('.sima-guitable:first');
        sima.ajaxLong.start("base/errorReports/sendAllUnsentClientBafRequests", {
            data: {
                model_filter:model_filter
            },
            showProgressBar: _guitable,
            onEnd: function(){
                sima.dialog.openInfo("Poslati su svi zahtevi.");
                _guitable.simaGuiTable('populate');
            }
        });
    };
    
    this.createAutoClientBafRequest = function(message, theme_id, theme_name)
    {
        sima.ajax.get("base/errorReports/CreateAutoClientBafRequest", {
            data: {
                message: message,
                theme_id: theme_id,
                theme_name: theme_name
            },
            async: true,
            success_function:function() {
                
            }
        });
    };
}