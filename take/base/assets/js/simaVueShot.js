
/* global sima */

function SIMAVueShotConstructor(Vue, VuexComponent, simaComunicationAction, simaErrorReport = null)
{
    const SIMAVueShot = new VuexComponent({
        state: {
            models: {},//modeli iz kojih se citaju podaci. Ukoliko je objekat i ima polje model_tag, onda gleda na drugi model (relacija)
            model_lists: {},
            model_lists_count: {},
            custom_data: {},
            deleted_models: [],
            model_hash_for_update_new: [], //svi tagovi koji treba da se  updateuju, ali se ceka sledeci tick
            update_timeout: 50, //vreme izmedju tickova
            update_timeout_handler: null,
            waiting_for_dirty_auto_bafrs: [],
            waiting_for_dirty_timeout_handler: null
        },
        getters: {
            //PUBLIC
            //dohvatanje modela za koriscenje u sistemu
            model: (state) => (model_tag, scopes = []) => {
                
                var model_tag_obj = {model_tag: model_tag, scopes: scopes};
                var model_tag_hash = JSON.stringify(model_tag_obj);
//                if ( state.deleted_models.indexOf(model_tag_hash) > -1)
//                {
//                    //ovo u sustini ne bi trebalo da se desi, jer ne bi trebalo da se zove prikaz modela koji je obrisan
//                    return new Proxy({},{
//                        get: function (obj, prop) {
//                            console.warn('trazen je property '+prop+' za model '+model_tag+' koji je OBRISAN!!');
//                            return '!!DELETED!!';
//                        }
//                    });
//                }
//                else
//                {
                    return new Proxy({},{
                        get: function (target, property) {
                            
                            if (typeof state.models[model_tag_hash] === 'undefined')
                            {
                                SIMAVueShot.commit('private_set_element_value', {
                                    element_object: state.models,
                                    element_key: model_tag_hash,
                                    element_value: {
                                        model_class:   model_tag.split('_')[0],
                                        model_id: parseInt(model_tag.split('_')[1]),
                                        model_scopes: scopes,
                                        model_tag: model_tag,
                                        old_model_tag: null,
                                        model_tag_hash: model_tag_hash,
                                        status: 'OK',
                                        status_time: Date.now(),
                                        props: {
                                            //ovaj property se odmah zna
                                            id: {
                                                property: 'id',
                                                hashed_key: 'id',
                                                data: parseInt(model_tag.split('_')[1]),
                                                status: 'OK',
                                                status_time: Date.now(),
                                                dependent_on_model_tags: [],
                                                dependent_on_general_model_tags: []
                                            }
                                        }
                                    }
                                });
                            }

                            var internal_model_props = state.models[model_tag_hash]['props'];

                            //ovaj property je uvek funkicija i vraca relaciju
                            //TODO: ovo moze da bude i neka funkcija koja se poziva uvek
                            if (property==='scoped')
                            {
                                return function(local_property,relation_scopes, model_scopes){
                                    var hashed_key = local_property + '_' + JSON.stringify(relation_scopes) + '_' + JSON.stringify(model_scopes);
                                    if (typeof internal_model_props[hashed_key] === 'undefined')
                                    {
                                        SIMAVueShot.commit('private_set_element_value', {
                                            element_object: internal_model_props,
                                            element_key: hashed_key,
                                            element_value: {
                                                property: local_property,
                                                relation_scopes: relation_scopes,
                                                model_scopes: model_scopes,
                                                hashed_key: hashed_key,
                                                data:[],
                                                status: 'INIT',
                                                status_time: Date.now(),
                                                dependent_on_model_tags: [],
                                                dependent_on_general_model_tags: []
                                            }
                                        });
                                        SIMAVueShot.dispatch('private_update_model', model_tag_hash);
                                    }
                                    else if (internal_model_props[hashed_key]['status'] === 'DIRTY')
                                    {
                                        SIMAVueShot.commit('private_set_element_status',{
                                            element_object: internal_model_props[hashed_key], 
                                            status_value: 'RETRIEVING'
                                        });
                                        SIMAVueShot.dispatch('private_update_model', model_tag_hash);
                                    }
                                    return internal_model_props[hashed_key]['data'];
                                };
                            }
                            else if (property === 'setScoped')
                            {
                                return function(local_property, value, relation_scopes, model_scopes){
                                    var hashed_key = local_property + '_' + JSON.stringify(relation_scopes) + '_' + JSON.stringify(model_scopes);

                                    if (typeof internal_model_props[hashed_key] !== 'undefined')
                                    {
                                        SIMAVueShot.commit('private_change_model_prop_value',{
                                            property_object: internal_model_props[hashed_key], 
                                            new_value: value
                                        });
                                        
                                        var status_params = {
                                            element_object: internal_model_props[hashed_key], 
                                            status_value: 'WAITING_FOR_DIRTY'
                                        };
                                        
                                        if (simaErrorReport !== null)
                                        {
                                            status_params.waiting_for_dirty_status_timeout = SIMAVueShot.getWaitingForDirtyStatusTimeout(state, internal_model_props[hashed_key], model_tag_hash);
                                        }

                                        SIMAVueShot.commit('private_set_element_status', status_params);
                                    }
                                };
                            }
                            else if (property === 'manualUpdate')
                            {
                                return function(property) {
                                    if (typeof internal_model_props[property] === 'undefined')
                                    {
                                        SIMAVueShot.commit('private_set_element_value', {
                                            element_object: internal_model_props,
                                            element_key: property,
                                            element_value: {
                                                property: property,
                                                hashed_key: property,
                                                data:'',
                                                status: 'INIT',
                                                status_time: Date.now(),
                                                dependent_on_model_tags: [],
                                                dependent_on_general_model_tags: [],
                                                manual_update: true
                                            }
                                        });
                                        SIMAVueShot.dispatch('private_update_model', model_tag_hash);
                                    }
                                    else if (internal_model_props[property]['status'] === 'DIRTY')
                                    {
                                        SIMAVueShot.commit('private_set_element_status',{
                                            element_object: internal_model_props[property], 
                                            status_value: 'RETRIEVING'
                                        });
                                        SIMAVueShot.dispatch('private_update_model', model_tag_hash);
                                    }

                                    return internal_model_props[property]['data'];
                                };
                            }
                            //else if(property === 'toJSON')
                            //{
                            //    return (state.models[model_tag_hash]).toJSON();
                            //}
                            //else if(property === 'inspect')
                            //{
                            //    return (state.models[model_tag_hash]).inspect();
                            //}
                            else if(property === 'wrapped')
                            {
                                /// SMA po default-u zove odnekud wrapped
                                /// https://docs.nativescript.org/api-reference/classes/_data_observable_.wrappedvalue
                                /// Helper class that is used to fire property change even when real object is the same.
                                /// https://docs.nativescript.org/api-reference/classes/_data_observable_.wrappedvalue#wrapped
                                /// Property which holds the real value.
                                return null;
                            }
                            else if(property === 'shotStatus')
                            {
                                return function(property_name, relation_scopes, model_scopes) {
                                    if (typeof relation_scopes !== 'undefined' || typeof model_scopes !== 'undefined')
                                    {
                                        var hashed_key = property_name + '_' + JSON.stringify(relation_scopes) + '_' + JSON.stringify(model_scopes);
                                        if (typeof internal_model_props[hashed_key] !== 'undefined')
                                        {
                                            return internal_model_props[hashed_key]['status'];
                                        }
                                    }
                                    else if (typeof internal_model_props[property_name] !== 'undefined')
                                    {
                                        return internal_model_props[property_name]['status'];
                                    }
                                };
                            }
                            else if(property === 'setDirty')
                            {
                                return function(property_name, relation_scopes, model_scopes) {
                                    if (typeof relation_scopes !== 'undefined' || typeof model_scopes !== 'undefined')
                                    {
                                        var hashed_key = property_name + '_' + JSON.stringify(relation_scopes) + '_' + JSON.stringify(model_scopes);
                                        if (typeof internal_model_props[hashed_key] !== 'undefined')
                                        {
                                            SIMAVueShot.commit('private_set_element_status',{
                                                element_object: internal_model_props[hashed_key], 
                                                status_value: 'DIRTY'
                                            });

                                            return true;
                                        }
                                    }
                                    else if (typeof internal_model_props[property_name] !== 'undefined')
                                    {
                                        SIMAVueShot.commit('private_set_element_status',{
                                            element_object: internal_model_props[property_name], 
                                            status_value: 'DIRTY'
                                        });
                                        
                                        return true;
                                    }
                                    
                                    return false;
                                };
                            }
                            else if(property === 'get_class')
                            {
                                return state.models[model_tag_hash].model_class;
                            }

                            if (typeof internal_model_props[property] === 'undefined')
                            {
                                //ako propery na postoji, zadaje se data i odmah se trazi update
                                SIMAVueShot.commit('private_set_element_value', {
                                    element_object: internal_model_props,
                                    element_key: property,
                                    element_value: {
                                        property: property,
                                        hashed_key: property,
                                        data:'',
                                        status: 'INIT',
                                        status_time: Date.now(),
                                        dependent_on_model_tags: [],
                                        dependent_on_general_model_tags: []
                                    }
                                });
                                SIMAVueShot.dispatch('private_update_model', model_tag_hash);
                            }
                            //ako property postoji i ako je njegov status DIRTY onda se trazi update
                            else if (internal_model_props[property]['status'] === 'DIRTY')
                            {
                                SIMAVueShot.commit('private_set_element_status',{
                                    element_object: internal_model_props[property], 
                                    status_value: 'RETRIEVING'
                                });
                                SIMAVueShot.dispatch('private_update_model', model_tag_hash);
                            }

                            return internal_model_props[property]['data'];
                        },
                        set: function (target, property, value) {
                            if (typeof state.models[model_tag_hash]['props'][property] !== 'undefined')
                            {
                                SIMAVueShot.commit('private_change_model_prop_value',{
                                    property_object: state.models[model_tag_hash]['props'][property], 
                                    new_value: value
                                });
                                if (state.models[model_tag_hash]['props'][property].manual_update !== true)
                                {
                                    var status_params = {
                                        element_object: state.models[model_tag_hash]['props'][property], 
                                        status_value: 'WAITING_FOR_DIRTY'
                                    };

                                    if (simaErrorReport !== null)
                                    {
                                        status_params.waiting_for_dirty_status_timeout = SIMAVueShot.getWaitingForDirtyStatusTimeout(state, state.models[model_tag_hash]['props'][property], model_tag_hash);
                                    }

                                    SIMAVueShot.commit('private_set_element_status', status_params);
                                }
                            }
                            /**
                             * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Proxy/handler/set
                             * "The set method should return a boolean value. Return true to indicate that assignment succeeded. 
                             *   If the set method returns false, and the assignment happened in strict-mode code, 
                             *   a TypeError will be thrown."
                             */
                            return true;
                        }
                    });
//                }
            },
            //PUBLIC
            model_list: (state) => (component_instance, model_name, criteria, model_scopes) => {
                var list_key = model_name + JSON.stringify(criteria) + '_' + JSON.stringify(model_scopes);
                if (typeof state.model_lists[list_key] === 'undefined')
                {
                    //objekat koji ce biti u spisku
                    var list_object = {
                        model_name: model_name,
                        criteria: criteria,
                        scopes: model_scopes,
                        update_model_tag: [],
                        list: [], //lista koja se vraca klijentu
                        status: 'INIT',
                        status_time: Date.now(),
                        component_instances_visibility: {}
                    };
                    SIMAVueShot.commit('private_set_element_value', {
                        element_object: state.model_lists,
                        element_key: list_key,
                        element_value: list_object
                    });
                    SIMAVueShot.dispatch('update_list',list_object);
                }
                
                SIMAVueShot.checkComponentInstanceVisibilityForElement(state.model_lists[list_key], component_instance);
                
                if (
                        state.model_lists[list_key]['status'] === 'DIRTY' && 
                        (
                            Object.keys(state.model_lists[list_key]['component_instances_visibility']).length === 0  || 
                            Object.values(state.model_lists[list_key]['component_instances_visibility']).indexOf(true) !== -1
                        )
                    )
                {
                    SIMAVueShot.commit('private_set_element_status',{
                        element_object: state.model_lists[list_key], 
                        status_value: 'RETRIEVING'
                    });
                    SIMAVueShot.dispatch('update_list', state.model_lists[list_key]);
                }

                return new Proxy(state.model_lists[list_key].list, {
                    get: function (target, property) {
                        if(property === 'shotStatus')
                        {
                            if (typeof state.model_lists[list_key] !== 'undefined')
                            {
                                return state.model_lists[list_key]['status'];
                            }

                            return null;
                        }
                        else if(property === 'setDirty')
                        {
                            return function(list_key) {
                                if (typeof state.model_lists[list_key] !== 'undefined')
                                {
                                    SIMAVueShot.commit('private_set_element_status',{
                                        element_object: state.model_lists[list_key], 
                                        status_value: 'DIRTY'
                                    });

                                    return true;
                                }

                                return false;
                            };
                        }
                        else if (property in target) 
                        {
                            return target[property];
                        }
                        else if (property in Array.prototype) 
                        {
                            return Array.prototype[property];
                        }
                    }
                });
            },
            //PUBLIC
            model_list_count: (state) => (component_instance, model_name, criteria) => {
                var list_key = model_name + JSON.stringify(criteria);
                if (typeof state.model_lists_count[list_key] === 'undefined')
                {
                    //objekat koji ce biti u spisku
                    var list_object = {
                        model_name: model_name,
                        criteria: criteria,
                        update_model_tag: [],
                        list_count: 0,
                        status: 'INIT',
                        status_time: Date.now(),
                        component_instances_visibility: {}
                    };
                    SIMAVueShot.commit('private_set_element_value', {
                        element_object: state.model_lists_count,
                        element_key: list_key,
                        element_value: list_object
                    });
                    SIMAVueShot.dispatch('update_list_count',list_object);
                }
                
                SIMAVueShot.checkComponentInstanceVisibilityForElement(state.model_lists_count[list_key], component_instance);
                
                if (
                    state.model_lists_count[list_key]['status'] === 'DIRTY' &&
                    (
                        Object.keys(state.model_lists_count[list_key]['component_instances_visibility']).length === 0  || 
                        Object.values(state.model_lists_count[list_key]['component_instances_visibility']).indexOf(true) !== -1
                    )
                )
                {
                    SIMAVueShot.commit('private_set_element_status',{
                        element_object: state.model_lists_count[list_key], 
                        status_value: 'RETRIEVING'
                    });
                    SIMAVueShot.dispatch('update_list_count', state.model_lists_count[list_key]);
                }
                
                return state.model_lists_count[list_key].list_count;
            },
            /**
             * PUBLIC
             * @param {string} uniq_key
             * @param {object} params --mora biti staticki
             * @returns {unresolved}
             */
            custom_data: (state) => (component_instance, uniq_key, params) => {
                
                if (typeof state.custom_data[uniq_key] === 'undefined')
                {
                    //objekat koji ce biti u spisku
                    var list_object = {
                        params: params,
                        update_model_tag: [],
                        data: [], //podaci koji se vracaju klijentu
                        status: 'INIT',
                        status_time: Date.now(),
                        component_instances_visibility: {}
                    };
                    SIMAVueShot.commit('private_set_element_value', {
                        element_object: state.custom_data,
                        element_key: uniq_key,
                        element_value: list_object
                    });
                    SIMAVueShot.dispatch('update_custom_data',list_object);
                }
                
                SIMAVueShot.checkComponentInstanceVisibilityForElement(state.custom_data[uniq_key], component_instance);
                
                if (
                    state.custom_data[uniq_key]['status'] === 'DIRTY' && 
                    (
                        Object.keys(state.custom_data[uniq_key]['component_instances_visibility']).length === 0  || 
                        Object.values(state.custom_data[uniq_key]['component_instances_visibility']).indexOf(true) !== -1
                    )
                )
                {
                    SIMAVueShot.commit('private_set_element_status',{
                        element_object: state.custom_data[uniq_key], 
                        status_value: 'RETRIEVING'
                    });
                    SIMAVueShot.dispatch('update_custom_data', state.custom_data[uniq_key]);
                }
                
                return state.custom_data[uniq_key].data;
            }
        },
        mutations: {
            clear_models (state)
            {
                state.models = {};
                state.model_lists = {};
                state.model_lists_count = {};
                state.custom_data = {};
            },
            set_all_dirty(state)
            {
                for (var vue_model_tag_hash in state.models)
                {
                    var element = state.models[vue_model_tag_hash];
                    for (var property_key in element.props)
                    {
                        SIMAVueShot.commit('private_set_element_status',{
                            element_object: element.props[property_key], 
                            status_value: 'DIRTY'
                        });
                    }
                };
                
                for (var list_key in state.model_lists){
                    var element = state.model_lists[list_key];
                    SIMAVueShot.commit('private_set_element_status',{
                        element_object: element, 
                        status_value: 'DIRTY'
                    });
                };
                
                for (var list_key in state.model_lists_count){
                    var element = state.model_lists_count[list_key];
                    SIMAVueShot.commit('private_set_element_status',{
                        element_object: element, 
                        status_value: 'DIRTY'
                    });
                };
                
                for (var list_key in state.custom_data){
                    var element = state.custom_data[list_key];
                    SIMAVueShot.commit('private_set_element_status',{
                        element_object: element, 
                        status_value: 'DIRTY'
                    });
                };
            },
            flush_wait_for_dirty(state)
            {
                for (var vue_model_tag_hash in state.models)
                {
                    var element = state.models[vue_model_tag_hash];
                    for (var property_key in element.props)
                    {
                        if (element.props[property_key]['status'] === 'WAITING_FOR_DIRTY')
                        {
                            SIMAVueShot.commit('private_set_element_status',{
                                element_object: element.props[property_key], 
                                status_value: 'DIRTY'
                            });
                        }
                    }
                };
                
//                for (var list_key in state.model_lists){
//                    var element = state.model_lists[list_key];
//                    SIMAVueShot.commit('private_set_element_status',{
//                        element_object: element, 
//                        status_value: 'DIRTY'
//                    });
//                };
//                
//                for (var list_key in state.model_lists_count){
//                    var element = state.model_lists_count[list_key];
//                    SIMAVueShot.commit('private_set_element_status',{
//                        element_object: element, 
//                        status_value: 'DIRTY'
//                    });
//                };
//                
//                for (var list_key in state.custom_data){
//                    var element = state.custom_data[list_key];
//                    SIMAVueShot.commit('private_set_element_status',{
//                        element_object: element, 
//                        status_value: 'DIRTY'
//                    });
//                };
            },
            private_set_element_value (state, {element_object, element_key, element_value})
            {
                Vue.set(element_object, element_key, element_value);
            },
            //koristi se za postavljanje statusa bilo kog objekta(property-i modela, liste, custom data...)
            private_set_element_status (state, {element_object, status_value, waiting_for_dirty_status_timeout = null})
            {
                element_object.status = status_value;
                element_object.status_time = Date.now();
                if (waiting_for_dirty_status_timeout !== null)
                {
                    element_object.waiting_for_dirty_status_timeout = waiting_for_dirty_status_timeout;
                }

                if (
                        typeof element_object.waiting_for_dirty_status_timeout !== 'undefined' && 
                        element_object.waiting_for_dirty_status_timeout !== null && 
                        status_value !== 'WAITING_FOR_DIRTY'
                    )
                {
                    clearTimeout(element_object.waiting_for_dirty_status_timeout);
                    delete element_object.waiting_for_dirty_status_timeout;
                }
            },
            //razlikuje se od komita private_set_model_prop_value po tome sto samo treba da postavi vrednost bez menjanja statusa
            //koristi se za postavljanje vrednosti poperty-ija modela
            private_change_model_prop_value (state, {property_object, new_value})
            {
                property_object['data'] = new_value;
            },
            //ovo je samo za interno koriscenje prilikom setovanja novog podatka
            //PRIVATE
            private_set_model_prop_value (state, {model_tag_hash, model_prop_hash, prop_value, prop_dependent_on_model_tags}) 
            {
                var value_to_set = '';
                
                if (prop_value !== null && typeof prop_value === 'object' && typeof prop_value.TYPE === 'string')
                {
                    if (prop_value.TYPE === 'MODEL')
                    {
                        var model_scopes = state.models[model_tag_hash]['props'][model_prop_hash]['model_scopes'];
                        if (typeof model_scopes === 'undefined')
                        {
                            model_scopes = [];
                        }
                        value_to_set = SIMAVueShot.getters.model(prop_value.TAG, model_scopes);
                    }
                    else if (prop_value.TYPE === 'MODEL_ARRAY')
                    {
                        var model_scopes = state.models[model_tag_hash]['props'][model_prop_hash]['model_scopes'];
                        if (typeof model_scopes === 'undefined')
                        {
                            model_scopes = [];
                        }
                        var _value_to_set = [];
                        for (var key in prop_value.ARRAY) {
                            //TODO: da podrzava scopeove
                            var elem = prop_value.ARRAY[key];
                            _value_to_set.push(SIMAVueShot.getters.model(elem, model_scopes));
                        };
                        value_to_set = _value_to_set;
                    }
                    else
                    {
                        console.log('SHOT - ne zna se tip '+(prop_value.TYPE));
                    }
                }
                else
                {
                    value_to_set = prop_value;
                }
                state.models[model_tag_hash]['props'][model_prop_hash]['data'] = value_to_set;
                SIMAVueShot.commit('private_set_element_status',{
                    element_object: state.models[model_tag_hash]['props'][model_prop_hash], 
                    status_value: value_to_set === 'ND!!!' ? 'FAILURE' : 'OK'
                });
                state.models[model_tag_hash]['props'][model_prop_hash]['dependent_on_model_tags'] = prop_dependent_on_model_tags;
                //spisak opstih model tagova(ne onih koji su vezani za konkretan model). Npr public.notifications
                var prop_dependent_on_general_model_tags = [];
                for(var i in prop_dependent_on_model_tags)
                {
                    var _model_tag = prop_dependent_on_model_tags[i];
                    var model_tag_split = (_model_tag !== null && typeof _model_tag !== 'undefined') ? _model_tag.split('_') : [];
                    var is_last_part_of_model_tag_number = /^\d+$/.test(model_tag_split[model_tag_split.length-1]);
                    
                    //ako poslednji deo model taga nema broj, znaci da je opsti tag i on treba da se proverava posebno sa includes funkcijom
                    if (!is_last_part_of_model_tag_number)
                    {
                        prop_dependent_on_general_model_tags.push(_model_tag);
                    }
                }
                state.models[model_tag_hash]['props'][model_prop_hash]['dependent_on_general_model_tags'] = prop_dependent_on_general_model_tags;
            },
            private_set_model_old_tag (state,{model_tag_hash, old_model_tag})
            {
//                state.models[model_tag_hash]['old_model_tag'] = old_model_tag;
                SIMAVueShot.commit('private_set_element_value', {
                    element_object: state.models[model_tag_hash],
                    element_key: 'old_model_tag',
                    element_value: old_model_tag
                });
                if (state.models[model_tag_hash].status !== 'OK')
                {
                    SIMAVueShot.commit('private_set_element_status',{
                        element_object: state.models[model_tag_hash], 
                        status_value: 'OK'
                    });
                }
            },
            //private
            private_set_model_deleted (state, model_tag_hash)
            {
                SIMAVueShot.commit('private_set_element_status',{
                    element_object: state.models[model_tag_hash], 
                    status_value: 'DELETED'
                });
//                delete state.models[model_tag_hash];
//                state.deleted_models.push(model_tag_hash);
            },
            /**
             * funkcija koja izdvaja model hash-eve za update i pokrece update
             * @param {type} state
             * @returns {undefined}
             */
            private_dispatch_update (state) 
            {
                var tags_for_update = state.model_hash_for_update_new;
                state.model_hash_for_update_new = [];
                SIMAVueShot.dispatch('private_internal_update_models',tags_for_update);
                state.update_timeout_handler = null;
            },
            /**
             * PRIVATE!!!
             * registrovanje konkretnog modela za hash i pokrece update
             * @param {type} state
             * @param {type} model_hash
             * @returns {undefined}
             */
            private_update_model (state,model_hash) 
            {

                var found = state.model_hash_for_update_new.find(function(item) {
                    return item === model_hash;
                });
                if (!found)
                {
                    state.model_hash_for_update_new.push(model_hash);
                }
                
                if (state.update_timeout_handler === null)
                {
                    state.update_timeout_handler = setTimeout(function() {
                        SIMAVueShot.commit('private_dispatch_update');
                    }, state.update_timeout);
                }
            },
            //ovo potencijalno moze da bude sporo
            /**
             * Funkcija koja trazi sva stanja koja zavise od starih model_Tag-ova i pokrece update-ove
             * @param {type} state
             * @param {type} old_model_tag_to_update
             * @returns {undefined}
             */
            update_model_old_tag(state, old_model_tag_to_update)
            {
                var t0 = performance.now();
                console.log('UPDATE VUE MODEL: '+ (new Date()).toLocaleTimeString()+ ' | '+old_model_tag_to_update);

                for (var vue_model_tag_hash in state.models)
                {
                    var element = state.models[vue_model_tag_hash];

                    for (var property_key in element.props)
                    {
                        var property_value = element.props[property_key];
                        
                        //proveravamo u spisak opstih model tagova da li se nalazi old_model_tag_to_update
                        var is_general_model_tag = false;
                        for(var i in property_value.dependent_on_general_model_tags)
                        {
                            var general_model_tag = property_value.dependent_on_general_model_tags[i];
                            if(typeof old_model_tag_to_update === 'undefined') /// MilosJ: desavalo mi se na SMA kada posaljem poruku
                            {
                                console.log('typeof old_model_tag_to_update === undefined 1');
                            }
                            else
                            {
                                if (old_model_tag_to_update.includes(general_model_tag))
                                {
                                    is_general_model_tag = true;
                                    break;
                                }
                            }
                        }
                        
                        if(typeof property_value.dependent_on_model_tags === 'undefined') /// MilosJ: desavalo mi se na SMA kada posaljem poruku
                        {
                            console.log('typeof property_value.dependent_on_model_tags === undefined');
                        }
                        else
                        {
                            if (
                                    (
                                        typeof property_value.manual_update === 'undefined' || !property_value.manual_update
                                    ) && 
                                    (
                                        is_general_model_tag || 
                                        element.old_model_tag === old_model_tag_to_update || 
                                        property_value.dependent_on_model_tags.includes(old_model_tag_to_update)
                                    )
                               )
                            {
                                SIMAVueShot.commit('private_set_element_status',{
                                    element_object: element.props[property_key], 
                                    status_value: 'DIRTY'
                                });
                            }
                        }
                    }
                };
                
                for (var list_key in state.model_lists){
                    var element = state.model_lists[list_key];
                    for (var i in element.update_model_tag){
                        var old_tag = element.update_model_tag[i];
                        if(typeof old_model_tag_to_update === 'undefined') /// MilosJ: desavalo mi se na SMA kada posaljem poruku
                        {
                            console.log('typeof old_model_tag_to_update === undefined 2');
                        }
                        else
                        {
                            if (old_model_tag_to_update.includes(old_tag))
                            {
                                SIMAVueShot.commit('private_set_element_status',{
                                    element_object: element, 
                                    status_value: 'DIRTY'
                                });
                            }
                        }
                    };
                };
                
                for (var list_key in state.model_lists_count){
                    var element = state.model_lists_count[list_key];
                    for (var i in element.update_model_tag){
                        var old_tag = element.update_model_tag[i];
                        if(typeof old_model_tag_to_update === 'undefined') /// MilosJ: desavalo mi se na SMA kada posaljem poruku
                        {
                            console.log('typeof old_model_tag_to_update === undefined 3');
                        }
                        else
                        {
                            if (old_model_tag_to_update.includes(old_tag))
                            {
                                SIMAVueShot.commit('private_set_element_status',{
                                    element_object: element, 
                                    status_value: 'DIRTY'
                                });
                            }
                        }
                    };
                };
                
                for (var list_key in state.custom_data){
                    var element = state.custom_data[list_key];
                    for (var i in element.update_model_tag){
                        var old_tag = element.update_model_tag[i];
                        if(typeof old_model_tag_to_update === 'undefined') /// MilosJ: desavalo mi se na SMA kada posaljem poruku
                        {
                            console.log('typeof old_model_tag_to_update === undefined 4: ' + (typeof old_model_tag_to_update === 'undefined'));
                        }
                        else
                        {
                            if (old_model_tag_to_update.includes(old_tag))
                            {
                                SIMAVueShot.commit('private_set_element_status',{
                                    element_object: element, 
                                    status_value: 'DIRTY'
                                });
                            }
                        }
                    };
                };
                
                var t1 = performance.now();
                //MilosS(9.9.2018): cisto sam ostavio da vidim da li treba optimizovati funkciju
                if ((t1 - t0) > 10)
                {
                    console.warn("SIMAVueShot |  update_model_old_tag - potrebno vise od 10ms da se izvrsi: " 
                            + (t1 - t0) + "ms. Modela ima: "+ Object.keys(state.models).length);
                }
            },
            
            update_list_internal(state,{list_object, setter_object}){
                var result_list = [];
                for (var key in setter_object.ARRAY) {
                    var elem = setter_object.ARRAY[key];
                    result_list.push(SIMAVueShot.getters.model(elem,list_object.scopes));
                };
                list_object.list = result_list;
                list_object.update_model_tag = [setter_object.UPDATE_MODEL_LIST_TAG];
                SIMAVueShot.commit('private_set_element_status',{
                    element_object: list_object, 
                    status_value: 'OK'
                });
            },
            update_list_count_internal(state,{list_object, setter_object}){
                list_object.list_count = setter_object.list_count;
                list_object.update_model_tag = [setter_object.UPDATE_MODEL_LIST_TAG];
                SIMAVueShot.commit('private_set_element_status',{
                    element_object: list_object, 
                    status_value: 'OK'
                });
            },
            update_custom_data_internal(state,{list_object, setter_object}){

//                var result_list = [];
//                $.each(setter_object.DATA, function(key,elem){
//                    result_list.push(SIMAVueShot.getters.model(elem));
//                });
//                list_object.data = result_list;
                list_object.data = setter_object.DATA;
                list_object.update_model_tag = setter_object.UPDATE_MODEL_TAGS;
                SIMAVueShot.commit('private_set_element_status',{
                    element_object: list_object, 
                    status_value: 'OK'
                });
            },
            remove_custom_data(state, custom_data_index)
            {
                Vue.delete(state.custom_data,custom_data_index);
            },
            remove_component_instance_from_visibility (state, component_instance_id)
            {
                var state_objects = [state.model_lists, state.model_lists_count, state.custom_data];

                for (var object_key in state_objects)
                {
                    var state_object = state_objects[object_key];
                    for (var key in state_object)
                    {
                        var state_object_item = state_object[key];
                        if (typeof state_object_item.component_instances_visibility[component_instance_id] !== 'undefined')
                        {
                            delete state_object_item.component_instances_visibility[component_instance_id];
                        }
                    }
                };
            }
        },
        actions: {
            /**
             * funkcija koja pakuje modele koji treba da budu update-ovani i kada stigne odgovor, vrsi update
             * @param {type} model_hashes_for_update
             * @param {type} state
             * @param {type} commit
             * @param {type} getters
             * @returns {undefined}
             */
            private_internal_update_models ({state,commit},model_hashes_for_update) {

                var _requests = [];

                for(var i in model_hashes_for_update)
                {
                    var _model = state.models[ model_hashes_for_update[i] ];

                    /// moze se desiti da modela nema
                    /// to je situacija kada se samostalno iz koda pozove update_model_old_tag
                    /// trenutno potreban za SMA i azuriranje listi diskusija
                    if(typeof _model === 'undefined')
                    {
                        continue;
                    }
                    /// desava se u sma (retko)
                    if(_model.model_class === '')
                    {
                        console.log('************************************************************************************');
                        console.log('************************************************************************************');
                        console.log('_model.model_class empty');
                        console.log('************************************************************************************');
                        console.log('************************************************************************************');
                        continue;
                    }
                    var _props = [];

                    for(var key in _model.props)
                    {
                        var _property = _model.props[key];
                        if (_property.status === 'INIT' || _property.status === 'RETRIEVING')
                        {
                            _props.push({
                                property: _property.property,
                                scopes: _property.relation_scopes,
                                hashed_key: _property.hashed_key
                            });
                        }
                    }
                    
                    _requests.push({
                        model_class:    _model.model_class,
                        model_id:       _model.model_id,
                        model_scopes:   _model.model_scopes,
                        model_tag_hash: _model.model_tag_hash,
                        props: _props
                    });
                }
                
                simaComunicationAction('base/model/vueShotProps',{
                    async: true,
                    data: {
                        requests: JSON.stringify(_requests)
                    },
                    success_function: function(response){
                        for(var ii in response.tags_props_values)
                        {
                            var _elem = response.tags_props_values[ii];
                            if (_elem.status === 'DELETED')
                            {
                                commit('private_set_model_deleted',_elem.model_tag_hash);
                            }
                            else
                            {
                                for(var prop in _elem.props)
                                {
                                    commit('private_set_model_prop_value',{
                                        model_tag_hash: _elem.model_tag_hash, 
                                        model_prop_hash: _elem.props[prop].model_prop_hash, 
                                        prop_value: _elem.props[prop].value,
                                        prop_dependent_on_model_tags: _elem.props[prop].dependent_on_model_tags
                                    });
                                }
                                commit('private_set_model_old_tag',{
                                    model_tag_hash: _elem.model_tag_hash,
                                    old_model_tag: _elem.old_model_tag
                                });
                            }
                        }
                    }
                });
            },
            private_update_model ({}, model_tag_hash) 
            {
                //mora preko timeouta kako bi poziv bio async jer se ova akcija zove iz getters-a a oni se gledaju kao computed data u vue
                //i onda pretstavlja problem sto se u commitu private_update_model menja state i to zabelezi getters i na svaku promenu state se opet zove, 
                //a kad je async onda computed ne gleda dalje
                setTimeout(function() {
                    SIMAVueShot.commit('private_update_model', model_tag_hash);
                }, 0);
            },
            update_list ({commit},list_object) 
            {
                simaComunicationAction('base/vue/list',{
                    async: true,
                    data:{
                        model_name: list_object.model_name,
                        criteria: list_object.criteria
                    },
                    success_function: function(response){
                        commit('update_list_internal',{
                            list_object:list_object,
                            setter_object:response.list
                        });
                    }
                });
            },
            update_list_count ({commit},list_object) 
            {
                simaComunicationAction('base/vue/listCount',{
                    async: true,
                    data:{
                        model_name: list_object.model_name,
                        criteria: list_object.criteria
                    },
                    success_function: function(response){
                        commit('update_list_count_internal',{
                            list_object: list_object,
                            setter_object: response.setter_object
                        });
                    }
                });
            },
            update_custom_data ({commit},list_object) 
            {
                //ajax poziv je menjao sam liz koji se prosledjuje, odnosno dodavao mu je
                //"YIISIMASESSION":"RequireLogin_sima787601114","is_idle_since":"true","network_outage":"false"
                var _data = JSON.parse(JSON.stringify(list_object.params.action_data));
                simaComunicationAction(list_object.params.action,{
                    async: true,
                    data: _data,
                    success_function: function(response){
                        commit('update_custom_data_internal',{
                            list_object:list_object,
                            setter_object:response.setter_object
                        });
                    }
                });
            }
        }
    });
    
    /**
     * Update-uje spisak vidljivosti komponenata(component_instances_visibility) koje koriste prosledjeni element_object sa vidljivoscu prosledjene komponente component_instance
     * @param {Object} element_object
     * @param {Object} component_instance - opcioni parametar, jer se vidljivost rucno zadaje komponenti pa je u startu nemaju sve komponente
     */
    SIMAVueShot.checkComponentInstanceVisibilityForElement = function(element_object, component_instance = null) {
        if (component_instance !== null && typeof component_instance.component_instance_visibility !== 'undefined')
        {
            var component_instances_visibility = element_object['component_instances_visibility'];
            if (component_instance.is_component_instance_visible !== component_instances_visibility[component_instance._uid])
            {
                var component_instance_visibility = component_instance.component_instance_visibility;
                component_instances_visibility = Object.assign(component_instances_visibility, component_instance_visibility);

                SIMAVueShot.commit('private_set_element_value', {
                    element_object: element_object,
                    element_key: 'component_instances_visibility',
                    element_value: component_instances_visibility
                });
            }
        }
    };
    
    SIMAVueShot.getWaitingForDirtyStatusTimeout = function(state, property_object, model_tag_hash) {
        return setTimeout(function() {
            if (
                    property_object['status'] === 'WAITING_FOR_DIRTY' && 
                    Date.now() - property_object['status_time'] > 10000
                )
            {
                state.waiting_for_dirty_auto_bafrs.push(
                    'SIMAVueShot - Predugo trajanje statusa WAITING_FOR_DIRTY za property ' + property_object.property + ' i za model hash ' + model_tag_hash
                );
                if (state.waiting_for_dirty_timeout_handler === null)
                {
                    state.waiting_for_dirty_timeout_handler = setTimeout(function() {
                        simaErrorReport.createAutoClientBafRequest(
                            state.waiting_for_dirty_auto_bafrs.join('<br/>'),
                            3131,
                            'AutoBafReq - SIMAVueShot - predugo trajanje statusa WAITING_FOR_DIRTY'
                        );
                        state.waiting_for_dirty_auto_bafrs = [];
                        state.waiting_for_dirty_timeout_handler = null;
                    }, 4000);
                }
            }
        }, 10000);
    };
    
    return SIMAVueShot;
};

