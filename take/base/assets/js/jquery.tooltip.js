
(function($) {
    var methods = {
        init: function(params)
        {
            var _this = this;            
            
            if (typeof params.code === 'undefined')
            {
                sima.dialog.openWarn('Nepravilno je zadat tooltip. Morate zadati code tooltipa.');
            }
            
            var code = params.code;
            var topOffset = (typeof params.topOffset !== 'undefined') ? params.topOffset : 0;
            var leftOffset = (typeof params.leftOffset !== 'undefined') ? params.leftOffset : 0;
            var text = (typeof params.text !== 'undefined') ? params.text : '';
            var wiki_page = (typeof params.wiki_page !== 'undefined') ? params.wiki_page : '';
            
            var old_z_index = _this.css('z-index');            
            _this.css({'z-index':115});

            var tooltip_overlay = $("#tooltip_overlay");            
            _this.data('tooltip_overlay', tooltip_overlay);
            var tooltip = create(text, wiki_page);
            tooltip.offset({top: topOffset, left: leftOffset});

            tooltip_overlay.show().append(tooltip);
            
            tooltip_overlay.on('click', '.tooltip-wrapper .tooltip-ok', function(){
                tooltip_overlay.empty().hide();
                _this.css({'z-index': old_z_index});
                if ($(this).parents('.tooltip-wrapper:first').find('.show-again input').is(':checked'))
                {
                    sima.ajax.get('base/base/saveTooltipForUser', {
                        get_params: {
                            code: code
                        },
                        async:true,
                        success_function: function(response) {             
                        }
                    });
                }
            });
            
            _this.data('tooltip_overlay', tooltip_overlay);
        },        
    };
    
    function create(text, wiki_page)
    {
        var url_div = '';
        if (typeof wiki_page !== 'undefined' && wiki_page !== '')
        {
            var url = sima.getBaseUrl()+'/index.php?r=site/docWikiCheck&redirect_link='+wiki_page;
            url_div = "<div class='tooltip-url'>Za više informacija kliknite  <a href='"+url+"' target='_blank'>ovde.</a> </div>";
        }        
        var show_again = "<div class='show-again'><input id='show-again-tooltip' type='checkbox' value='false' name='show-again-tooltip'>\n\
                          <label for='show-again-tooltip'><span>Ne prikazuj ponovo.</span></label></div>";
        var button = "<button class='tooltip-ok'>OK</button>";
        var content = "<div class='tooltip-content'><div class='text'>"+text+"</div>"+url_div+show_again+button+"</div>";
        var tooltip = $("<div class='tooltip-wrapper'><div class='tooltip-hand'></div>"+content+"</div>");
        
        return tooltip;
    }
    
    $.fn.tooltip = function(methodOrOptions) {
        var ar = arguments;
        var i=1;
        var ret = this;
        this.each(function() {
            i++;
            if (methods[methodOrOptions]) {
                var povr = methods[ methodOrOptions ].apply($(this), Array.prototype.slice.call(ar, 1));
                if (typeof povr !== 'undefined')
                {
                    ret = povr;
                }
                return;
            } else if (typeof methodOrOptions === 'object' || !methodOrOptions) {
                // Default to "init"
                return methods.init.apply($(this), ar);
            } else {
                $.error('Method ' + methodOrOptions + ' does not exist on jQuery.tooltip');
            }
        });
        return ret;
    };
    
})(jQuery);