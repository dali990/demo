
/* global sima */

function SIMAConfig()
{
    this.changeConfigParamDialog = function(path, is_user_personal, type)
    {
        if(type === 'searchField')
        {
            sima.ajax.get("base/configuration/getModelChooseParams",{
                data: {
                    path: path,
                    is_user_personal: is_user_personal
                },
                success_function:function(response){
                    sima.model.choose(response.model, function(data){
                        var selected_value = null;
                        
                        if(response.params.multiselect === true)
                        {
                            selected_value = [];
                            
                            $.each(data, function(index, value){
                                selected_value.push(value.id);
                            });
                        }
                        else
                        {
                            selected_value = data.id;
                        }
                        
                        setConfigParam(path, selected_value, is_user_personal);
                    }, response.params);
                }
            });
        }
        else
        {
            var uniq_id = sima.uniqid()+'_config_param';
            sima.ajax.get("base/configuration/changeConfigParamDialog",{
                get_params:{
                },
                data: {
                    uniq_id: uniq_id,
                    path: path,
                    is_user_personal: is_user_personal
                },
                success_function:function(response){
                    sima.dialog.openYesNo(response.html, function(){
                        var continue_to_ajax = true;
                        var selected_value = null;

                        if(response.type === 'dropdown' || response.type === 'boolean')
                        {
                            selected_value = $('#'+uniq_id+' option:selected').val();
                        }
                        else if(response.type === 'textField')
                        {
                            selected_value = $('#'+uniq_id).val();
                        }
                        else if(response.type === 'numeric')
                        {
                            selected_value = $('#'+uniq_id).numberField('getRealValue');
                        }
                        else
                        {
                            continue_to_ajax = false;
                            sima.dialog.openWarn('Doslo je do greske:</br>Nije podrzan tip.');
                        }

                        sima.dialog.close();

                        if(typeof selected_value === 'undefined')
                        {
                            continue_to_ajax = false;
                            sima.dialog.openWarn('Doslo je do greske');
                        }

                        if(continue_to_ajax === true)
                        {
                            setConfigParam(path, selected_value, is_user_personal);
                        }
                    }, null, null, null, response.fullscreen);
                }
            });
        }
    };
    
    this.setConfigParamToDefault = function(path, is_user_personal, thisObj)
    {
        sima.dialog.openYesNo("Da li zaista želite da vratite na podrazumevano?", function(){
            sima.dialog.close();
            sima.ajax.get("base/configuration/setConfigParamToDefault",{
                get_params:{
                },
                data: {
                    path: path,
                    is_user_personal: is_user_personal
                },
                success_function:function(){
                    sima.dialog.openInfo('Uspesno sacuvan parametar');
//                    sima.misc.refreshUiTabForObject(thisObj);
                }
            });
        });
    };
    
    this.paramUserChangableChanged = function(path, thisObj)
    {
        var checked = thisObj.prop("checked");
        
        sima.dialog.openYesNo("Da li zaista želite promenite vrednost?", 
            function(){
                sima.dialog.close();
                thisObj.prop("checked", checked);
                sima.ajax.get("base/configuration/paramUserChangableChanged",{
                    get_params:{
                    },
                    data: {
                        path: path
                    },
                    success_function:function(){
                        sima.dialog.openInfo('Uspesno sacuvan parametar');
//                        sima.misc.refreshUiTabForObject(thisObj);
                    }
                });
            },
            function(){
                sima.dialog.close();
                thisObj.prop("checked", !checked);
            },
            null,
            function(){
                thisObj.prop("checked", !checked);
            }
        );
    };
    
    function setConfigParam(path, new_value, is_user_personal)
    {
        sima.ajax.get("base/configuration/setConfigParam",{
            get_params:{
            },
            data: {
                path: path,
                new_value: new_value,
                is_user_personal: is_user_personal
            },
            async:true,
            success_function:function() {
                sima.dialog.openInfo('Uspesno sacuvan parametar');
            }
        });
    }
    
    var configurations_mapper = [];
    this.get = function(path, user_id, callback_function)
    {
        var configurations_mapper_key = path + user_id;
        
        if (typeof configurations_mapper[configurations_mapper_key] !== 'undefined')
        {
            callback_function(configurations_mapper[configurations_mapper_key].value, configurations_mapper[configurations_mapper_key].vueshot_model);
        }
        else
        {
            sima.ajax.get('base/configuration/getConfig', {
                get_params: {
                    path: path,
                    user_id: user_id
                },
                async:true,
                success_function: function(response) {
                    var vueshot_model = sima.vue.shot.getters.model(response.model_tag);

                    configurations_mapper[configurations_mapper_key] = {
                        vueshot_model: vueshot_model,
                        value: response.value
                    };

                    callback_function(response.value, vueshot_model);
                }
            });
        }
    };
    
    this.set = function(path, user_id, new_value)
    {
        sima.ajax.get('base/configuration/setConfig', {
            get_params: {
                path: path,
                user_id: user_id
            },
            data: {
                new_value: new_value
            },
            async:true
        });
    };
}
