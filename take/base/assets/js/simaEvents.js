/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* global sima */

//function SIMAEvents(params)
function SIMAEvents()
{    
    $(sima.eventrelay).on('CLIENT_AUTHENTICATION_RESPONSE', onClientAuthenticationResponse);
    $(sima.eventrelay).on('JS_INITIAL_MESSAGES', onJsInitialMessages);
    $(sima.eventrelay).on('JS_SESSION_LOGOUT', onSessionLogout);
    $(sima.eventrelay).on('UPDATE_MODELS', onUpdateModels);
    $(sima.eventrelay).on('UPDATE_VUE_MODELS', onUpdateVueModels);
    $(sima.eventrelay).on('WEB_CLIENT_ACTIVITY_GLOBAL_MESSAGE', onWebClientActivityGlobalMessage);
    $(sima.eventrelay).on('JS_CLIENT_DISCONNECTED', onClientDisconnected);
    
    function onClientAuthenticationResponse()
    {
        onConnectonOpen();
    }
    
    function onConnectonOpen()
    {
        sima.ajax.get('base/events/getInitialEventsData',{
            is_idle_since: false,
            async: true
        });
    };
    
    function checkIfAlreadyConnected()
    {
        if(sima.isEmpty(sima.eventrelay))
        {
            return;
        }
        var isEventRelayClientConnected = sima.eventrelay.isConnected();
        var eventRelayId = sima.eventrelay.getEventRelayId();
        if(isEventRelayClientConnected === true && !sima.isEmpty(eventRelayId))
        {
            onConnectonOpen();
        }
    }
    
    function onJsInitialMessages(event, event_message)
    {        
        var data = event_message.message.data;
        if (!sima.isEmpty(sima.messages))
        {
            sima.messages.update(data.messages, data.unread_messages, data.marked_messages, data.unread_messages_count, data.marked_messages_count);
        }
    }
//    function onJsInitialNotifs(event, event_message)
//    {        
////        var data = event_message.message.data;
////        if (!sima.isEmpty(sima.messages))
////        {
////            sima.messages.update(data.messages, data.unread_messages, data.marked_messages, data.unread_messages_count);
////        }
//
//        var data = event_message.message.data;
////        var currentdate = new Date();
////        var datetime = "Last Sync: " + currentdate.getDay() + "/"+currentdate.getMonth() 
////        + "/" + currentdate.getFullYear() + " @ " 
////        + currentdate.getHours() + ":" 
////        + currentdate.getMinutes() + ":" + currentdate.getSeconds();
////        console.log(datetime+'  -> onNotifs');
// 
//        if ( sima.notifications!==null && typeof data.notifs !== 'undefined')
//        {
//            sima.notifications.update(data.notifs);
//        }
//        if ( sima.warns!==null && typeof data.warns !== 'undefined')
//        {
//            sima.warns.update(data.warns);
//        }
//    }
//    function onJsInitialChatUsers(event, event_message)
//    {
//        var data = event_message.message.data;
//        if (!sima.isEmpty(sima.messages))
//        {
//            if (typeof data.chat_users !== 'undefined')
//            {
//                sima.messages.updateUsers(data.chat_users);
//            }
//        }
//    }
//    function onJsInitialChatUsersStatus(event, event_message)
//    {
//        var data = event_message.message.data;
//        if (!sima.isEmpty(sima.messages))
//        {
//            if (typeof data.chat_users_status !== 'undefined')
//            {
//                sima.messages.updateUsersStatus(data.chat_users_status);
//            }
//        }
//    }
//    function onNewNotif(event, event_message)
//    {
//        var data = event_message.message.data;
//        var notif_array = [];
//        notif_array.push(data);
//        if (!sima.isEmpty(sima.notifications))
//            sima.notifications.update(notif_array);
//    }
//    function onNewWarn(event, event_message)
//    {
//        var data = event_message.message.data;
//        if (!sima.isEmpty(sima.warns))
//        {
//            sima.warns.update(data);
//        }
//    }
//    function onNotifRead(event, event_message)
//    {
//        var data = event_message.message.data;
//        var notif_obj = $('.notification_table .notifs li[model_id="' + data.model_id + '"]');
//        
//        if (data.class === 'unseen_temp')
//        {
//            sima.notifications.setSeen(notif_obj, true);
//        }
//        else if (data.class === 'seen')
//        {
//            sima.notifications.setSeen(notif_obj, false);
//        }
//    }
    
    function onSessionLogout()
    {
        sima.logout();
    }
    
    function onUpdateModels(event, event_message)
    {
        var model_views = event_message.message.data;
        
        if (!$.isArray(model_views))
        {
            model_views = [model_views];
        }

        sima.model.updateViews(model_views);
        $.each(model_views, function(index, model_tag) {
            sima.vue.shot.commit('update_model_old_tag', model_tag);
        });
    }
    
    function onUpdateVueModels(event, event_message)
    {
        var model_views = event_message.message.data;
        if (!$.isArray(model_views))
        {
            model_views = [model_views];
        }
        $.each(model_views, function(index, model_tag) {
            sima.vue.shot.commit('update_model_old_tag', model_tag);
        });
    }
    
    function onWebClientActivityGlobalMessage(event, event_message)
    {
        var data = event_message.message.data;
        var user_id = parseInt(data.user_id);
        var new_status = data.status;
        
        onUpdateUserOnlineStatus(new_status, user_id);
    }
    
    function onClientDisconnected(event, event_message)
    {
        var data = event_message.message.data;
        var user_id = parseInt(data.user_id);
        var new_status = 'inactive';
        
        onUpdateUserOnlineStatus(new_status, user_id);
    }
    
    function onUpdateUserOnlineStatus(new_status, user_id) {
        var user = sima.vue.shot.getters.model('User_' + user_id);
        user.online_status = new_status;
    }
    
    checkIfAlreadyConnected();
    
//    var _last_time = params.time;
    
//    var eventSource = new EventSource("index.php?r=base/events/index");
    
    //EVENTS ZA NOTIFIKACIJE
//    function onNotifs(e)
//    {
//        var data = JSON.parse(e.data);
////        var currentdate = new Date();
////        var datetime = "Last Sync: " + currentdate.getDay() + "/"+currentdate.getMonth() 
////        + "/" + currentdate.getFullYear() + " @ " 
////        + currentdate.getHours() + ":" 
////        + currentdate.getMinutes() + ":" + currentdate.getSeconds();
////        console.log(datetime+'  -> onNotifs');
// 
//        if ( sima.notifications!==null && typeof data.notifs !== 'undefined')
//            sima.notifications.update(data.notifs);
//        if ( sima.warns!==null && typeof data.warns !== 'undefined')
//            sima.warns.update(data.warns);
//    }
    
//    function onNewNotif(e)
//    {
//        var data = JSON.parse(e.data);
//        var notif_array = [];
//        notif_array.push(data);
//        if (sima.notifications!==null)
//            sima.notifications.update(notif_array);
//    }
    
//    function onNotifRead(e)
//    {
//        var data = JSON.parse(e.data);
//        var notif_obj = $('.notification_table .notifs li[model_id="' + data.model_id + '"]');
//        
//        if (data.class == 'unseen_temp')
//        {
//            sima.notifications.setSeen(notif_obj, true);
//        }
//        else if (data.class == 'seen')
//        {
//            sima.notifications.setSeen(notif_obj, false);
//        }
//    }
    
    //EVENT ZA UPOZORENJA
//    function onNewWarn(e)
//    {
//        var data = JSON.parse(e.data);
//        if (sima.warns!==null)
//            sima.warns.update(data);
//    }
    
    //EVENTS ZA PORUKE
//    function onMessages(e)
//    {
//        var data = JSON.parse(e.data);
//        if (sima.messages!==null)
//        {
//            sima.messages.update(data.messages, data.unread_messages, data.marked_messages, data.unread_messages_count, data.marked_messages_count);
//        }
//    }
    
    //EVENT ZA CHAT
//    function onChatUsers(e)
//    {
//        var data = JSON.parse(e.data);
//        if (sima.messages!==null)
//        {
//            if (typeof data.chat_users !== 'undefined')
//                sima.messages.updateUsers(data.chat_users);
//        }
//    }
    
//    function onUpdateChatUsersStatus(e)
//    {
//        var data = JSON.parse(e.data);
//        if (sima.messages!==null)
//        {
//            if (typeof data.chat_users_status !== 'undefined')
//                sima.messages.updateUsersStatus(data.chat_users_status);
//        }
//    }
    
//    function onSessionLogout(e)
//    {
//        sima.logout();
//    }
    
//    function onOpenModelInDialog(e)
//    {
//        var data = JSON.parse(e.data);
//        sima.dialog.openModel(data.action, data.action_id);
//    }
    
//    function onPing(e)
//    {
//        var data = JSON.parse(e.data);
////        console.log('PING - provera je trajala '+data.time_diff);
//    }
    
//    function onNewTabSession(e)
//    {
//        var data = JSON.parse(e.data);        
//        sima.ajax.get('base/events/newTabSession',{
//            get_params:{
//                new_session_id: data.session_id,
//                time: _last_time
//            }
//        });
//    }
    
//    function onError(e)
//    {
//        console.log("EventSource failed.");
////        initiateSource();//nema potrebe jer pokusava ponovo po automatizmu
//    }
    
//    function initiateSource()
//    {
//        eventSource.addEventListener('notifs', onNotifs, false); //update zadnjih notifikacija
//        eventSource.addEventListener('notif_read', onNotifRead, false); //notifikacija procitana u drugom browseru/tabu
//        eventSource.addEventListener('new_notif', onNewNotif, false); //nova notifikacija
//        eventSource.addEventListener('new_warn', onNewWarn, false); //novo upozorenje
//        eventSource.addEventListener('sessionLogout', onSessionLogout, false); //potrebno je izlogovati korisnika
//        eventSource.addEventListener('ping', onPing, false); //PING
//        eventSource.addEventListener('newTabSession', onNewTabSession, false); //pokrenuta je nova SSE sesija
//        eventSource.addEventListener('openModelInDialog', onOpenModelInDialog, false); //otvara model u dialogu
//        eventSource.addEventListener('messages', onMessages, false); //update zadnjih notifikacija        
//        eventSource.addEventListener('chat_users', onChatUsers, false); //update korisnika za chat
//        eventSource.addEventListener('chat_users_status', onUpdateChatUsersStatus, false); //update statusa korisnika za chat
//        
//        eventSource.onerror = onError;
//    }
    
//    this.sendToEventRelay = function(id)
//    {
//        sima.ajax.get('base/events/sendToEventRelay', {});
//    };
    
//    initiateSource();
    
}