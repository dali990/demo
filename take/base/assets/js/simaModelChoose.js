/* global sima */

function SIMAModelChoose()
{
    this.create = function({
        model: model,
        ok_function: ok_function,
        view: view,
        multiselect: multiselect,
        filter: filter,
        fixed_filter: fixed_filter,
        select_filter: select_filter,
        title: title,
        params: params,
        model_id: model_id,
        ids: ids,
        can_be_null: can_be_null
    })
    {
        var uniq_id = sima.uniqid();
        var search_model_params = {
            uniq_id: uniq_id
        };
        if (typeof view !== 'undefined')
        {
            search_model_params.view = view;
        }
        if (typeof multiselect !== 'undefined' && multiselect===true)
        {
            search_model_params.multiselect = multiselect;
        }
        if (typeof filter !== 'undefined')
        {
            search_model_params.filter = filter;
        }
        if (typeof fixed_filter !== 'undefined')
        {
            search_model_params.fixed_filter = fixed_filter;
        }
        if (typeof select_filter !== 'undefined')
        {
            search_model_params.select_filter = select_filter;
        }
        if (typeof title !== 'undefined')
        {
            search_model_params.title = title;
        }
        if (typeof params !== 'undefined')
        {
            search_model_params.params = params;
        }
        if (typeof model_id !== 'undefined' && model_id !== null)
        {
            search_model_params.model_id = model_id;
        }

        search_model_ids = [];
        if (typeof ids !== 'undefined')
        {
            $.each(ids, function(index, value) {
                search_model_ids.push(value.id);
            });
            search_model_params.search_model_ids = search_model_ids;
        }

        sima.ajax.get('base/modelChoose/index',{
            get_params:{
                model_name: model
            },
            data: search_model_params,
            success_function:function(response)
            {
                var choose_uniq = uniq_id+"sima_search_model_choose";
                var choose_wrap = $("<div id='"+choose_uniq+"' class='sima-layout-panel sima-search-model-choose' data-search_model_ids='"+search_model_ids.join()+"'></div>");
                choose_wrap.data('search_model_ids', search_model_ids);
                if (response.fullscreen)
                {
                    fullScreen(choose_wrap, response, multiselect, uniq_id, model, ok_function);
                }
                else
                {
                    notFullScreen(choose_uniq, choose_wrap, response.left_side_html);
                }
            }
        });
    };
    
    this.user = function({
        ok_function: ok_function,
        add_to_model_id: add_to_model_id, //model nad kojim se biraju korisnici, kako bi mogle da se procene i ponude teme vezane za taj model
        add_to_model_name: add_to_model_name
    }){
        sima.modelChoose.create({
            model: 'User',
            ok_function: ok_function,
            view: 'tabs',
            multiselect: true,
            params: {
                add_button:false,
                right_side_view: 'guitable_right_side',
                right_side_view_params: {
                    title: sima.translate('ChosenUsers'),
                    columns_type: 'model_choose_chosen'
                },
                add_to_model_id: add_to_model_id,
                add_to_model_name: add_to_model_name
            }
        });
    };
    
    this.person = function({
        ok_function: ok_function,
        add_to_model_id: add_to_model_id,
        add_to_model_name: add_to_model_name
    }){
        sima.modelChoose.create({
            model: 'Person',
            ok_function: ok_function,
            view: 'tabs',
            multiselect: true,
            params: {
                add_button:false,
                right_side_view: 'guitable_right_side',
                right_side_view_params: {
                    title: sima.translate('ChosenUsers'),
                    columns_type: 'model_choose_chosen'
                },
                add_to_model_id: add_to_model_id,
                add_to_model_name: add_to_model_name
            }
        });
    };
    
    this.onTableChooseModel = function(guitable_id, clicked_row)
    {
////        var sima_search_model_choose = clicked_row.parents('.sima-search-model-choose:first');
////        sima_search_model_choose.trigger('add', [clicked_row, guitable_id]);
////        if(which_table === 'LEFT')
////        {
//            onChooseLeftTableModelDblClick(guitable_id, clicked_row);
////        }
////        else
////        {
////            onChooseRightTableModelDblClick(guitable_id, clicked_row);
////        }

        var left_guitable = $('#'+guitable_id);
        var search_model_choose = left_guitable.parents('.sima-search-model-choose:first');
//        var right_guitable = search_model_choose.find('.sima-layout-panel.right_side').find('.sima-guitable');
////        console.log(right_guitable);
////        console.log(guitable_id);
////        console.log(left_guitable);
////        console.log(left_guitable.data('search_model_ids'));
//        
//        var search_model_ids = search_model_choose.data('search_model_ids');
//        if(typeof search_model_ids === 'undefined')
//        {
//            search_model_ids = '';
//        }
//        
        var clicked_model_id = clicked_row.attr('model_id');
//        var curr_ids = search_model_ids.toString();
//        curr_ids_array = [];
//        if (curr_ids.indexOf(',') >= 0)
//        {
//            curr_ids_array = curr_ids.split(",");
//        }
//        else if (curr_ids.length > 0)
//        {
//            curr_ids_array = [curr_ids];
//        }
//        curr_ids_array.push(clicked_model_id);
//        search_model_choose.data('search_model_ids', curr_ids_array.join());
//        
//        
//        var curr_ids = (curr_ids_array.length > 0) ? curr_ids_array : -1;
//        
////        console.log(curr_ids);
//        
//        var left_guitable_curr_fixed_filter = left_guitable.simaGuiTable('getFixedFilter');
//        if (!sima.isEmpty(left_guitable_curr_fixed_filter[1]))
//        {
//            left_guitable_curr_fixed_filter[1] = {
//                0: 'NOT', 
//                1: {
//                    ids: curr_ids
//                }
//            };
//        }
//        
//        left_guitable.simaGuiTable('setFixedFilter', left_guitable_curr_fixed_filter);
//        right_guitable.simaGuiTable('setFixedFilter', {ids: curr_ids});
//        
//        sima.model.updateViews(clicked_row.attr('tag'));
//        
//        right_guitable.simaGuiTable('append_row', clicked_model_id);
        left_guitable.simaGuiTable('remove_row', clicked_model_id);
        search_model_choose.trigger('model_chosen', [clicked_model_id]);

    };
    
    this.onTableChoosePartnerList = function(guitable_id, clicked_row)
    {
        var guitable = $('#'+guitable_id);
        var search_model_choose = guitable.parents('.sima-search-model-choose:first');
        var clicked_model_id = clicked_row.attr('model_id');
        
        sima.ajax.get('base/modelChoose/onTableChoosePartnerList',{
            get_params:{
                model_id: clicked_model_id
            },
            success_function:function(response)
            {
                sima.dialog.openYesNo(response.html, function(){
                    search_model_choose.trigger('model_chosen', [response.user_ids_to_add]);
                    sima.dialog.close();
                });
            }
        });
    };
    
    this.onTableChooseTheme = function(guitable_id, clicked_row)
    {
        var guitable = $('#'+guitable_id);
        var search_model_choose = guitable.parents('.sima-search-model-choose:first');
        var clicked_model_id = clicked_row.attr('model_id');
        
        sima.ajax.get('base/modelChoose/onTableChooseTheme',{
            get_params:{
                model_id: clicked_model_id
            },
            success_function:function(response)
            {
                sima.dialog.openYesNo(response.html, function(){
                    search_model_choose.trigger('model_chosen', [response.user_ids_to_add]);
                    sima.dialog.close();
                });
            }
        });
    }
    
    this.onTableUnchooseModel = function(guitable_id, clicked_row)
    {
        var right_guitable = $('#'+guitable_id);
        var search_model_choose = right_guitable.parents('.sima-search-model-choose:first');
        
        var clicked_model_id = clicked_row.attr('model_id');
        
        right_guitable.simaGuiTable('remove_row', clicked_model_id);
        search_model_choose.trigger('model_unchosen', [clicked_model_id]);
    };
    
    this.bindTableToRemoveTrigger = function(guitable_id, choose_uniq_id)
    {
        var search_model_choose = $('#'+choose_uniq_id+'sima_search_model_choose');
        var guitable = $('#'+guitable_id);
        search_model_choose.bind('model_unchosen', function(event, data){
            guitable.simaGuiTable('append_row', data);
        });
    };
    
    this.partnerListDialogDetail = function(partner_list_id)
    {
        sima.dialog.openActionInDialog('guitable', {
            get_params: {
                label: sima.translate('PartnersThatAreNotActiveUsers'),
                settings: {
                    model: 'Partner',
                    fixed_filter: {
                        partner_to_partner_lists: {
                            partner_list: {
                                ids: partner_list_id
                            }
                        },
                        scopes: [
                            'not_active_user'
                        ]
                    }
                }
            }
        });
    };
    
    this.themeDialogDetail = function(theme_id)
    {
        sima.dialog.openActionInDialog('guitable', {
            get_params: {
                label: sima.translate('PersonsThatAreNotActiveUsers'),
                settings: {
                    model: 'Person',
                    fixed_filter: {
                        person_to_themes: {
                            theme: {
                                ids: theme_id
                            }
                        },
                        scopes: [
                            'not_active_user'
                        ]
                    }
                }
            }
        });
    };
    
//    function onChooseRightTableModelDblClick()
//    {
//        
//    }
    
//    function onChooseLeftTableModelDblClick(guitable_id, clicked_row)
//    {
//        var left_guitable = $('#'+guitable_id);
//        var search_model_choose = left_guitable.parents('.sima-search-model-choose:first');
//        var right_guitable = search_model_choose.find('.sima-layout-panel.right_side').find('.sima-guitable');
////        console.log(right_guitable);
////        console.log(guitable_id);
////        console.log(left_guitable);
////        console.log(left_guitable.data('search_model_ids'));
//        
//        var search_model_ids = search_model_choose.data('search_model_ids');
//        if(typeof search_model_ids === 'undefined')
//        {
//            search_model_ids = '';
//        }
//        
//        var clicked_model_id = clicked_row.attr('model_id');
//        var curr_ids = search_model_ids.toString();
//        curr_ids_array = [];
//        if (curr_ids.indexOf(',') >= 0)
//        {
//            curr_ids_array = curr_ids.split(",");
//        }
//        else if (curr_ids.length > 0)
//        {
//            curr_ids_array = [curr_ids];
//        }
//        curr_ids_array.push(clicked_model_id);
//        search_model_choose.data('search_model_ids', curr_ids_array.join());
//        
//        
//        var curr_ids = (curr_ids_array.length > 0) ? curr_ids_array : -1;
//        
////        console.log(curr_ids);
//        
//        var left_guitable_curr_fixed_filter = left_guitable.simaGuiTable('getFixedFilter');
//        if (!sima.isEmpty(left_guitable_curr_fixed_filter[1]))
//        {
//            left_guitable_curr_fixed_filter[1] = {
//                0: 'NOT', 
//                1: {
//                    ids: curr_ids
//                }
//            };
//        }
//        
//        left_guitable.simaGuiTable('setFixedFilter', left_guitable_curr_fixed_filter);
//        right_guitable.simaGuiTable('setFixedFilter', {ids: curr_ids});
//        
//        sima.model.updateViews(clicked_row.attr('tag'));
//        
//        right_guitable.simaGuiTable('append_row', clicked_model_id);
//        left_guitable.simaGuiTable('remove_row', clicked_model_id);
//    }
    
    function fullScreen(choose_wrap, response, multiselect, uniq_id, model_name, ok_function)
    {
        //ako je multiselect
        if (typeof multiselect !== 'undefined' && multiselect === true)
        {
            fullScreenMultiselect(choose_wrap, response, uniq_id, model_name, ok_function);
        }
        //ako je singleselect
        else
        {
            fullScreenSingleselect(choose_wrap, response, ok_function);
        }
        var _parent = choose_wrap.parent();
        sima.layout.setLayoutSize(choose_wrap,_parent.height(),_parent.width(),'TRIGGER_modelChoose_fullScreen');
    }
    
    function fullScreenMultiselect(choose_wrap, response, uniq_id, model_name, ok_function)
    {
        choose_wrap.addClass('sima-layout-panel _splitter _vertical');
        var left_side  = $("<div class='sima-layout-panel left_side'></div>");
        var right_side = $("<div class='sima-layout-panel right_side'></div>");
        choose_wrap.append(left_side);
        choose_wrap.append(right_side);

//        //is_guitable_view znaci da je desno guitable a ne lista izabranih stavki. To moze biti samo u slucaju da je levi view koji se renderuje guitable
//        if (!response.is_guitable_view)
//        {
//            var ids_list = $("<div class='sima-search-model-choose-list'></div>");
//            //ako imamo zadate id-eve kreiramo html za njih
//            if (typeof ids !== 'undefined')
//            {
//                $.each(ids, function(index, value) {
//                    var _class = (typeof value.class !== 'undefined')?value.class:'';
//                    var span = $('<span class="item '+_class+'" data-id="'+value.id+'"></span>');
//                    var span_delete = $('<span class="sima-icon _delete _16" title="Obriši" onclick="$(this).parent().remove();"></span>');
//                    var span_name = $('<span class="name" title="'+$.trim(value.display_name)+'">'+$.trim(value.display_name)+'</span>');
//                    span.append(span_delete).append(span_name);
//                    ids_list.append(span);
//                });
//            }
//            right_side.append(ids_list);
//        }
//
//        //event koji dodaje id u listu
//        choose_wrap.on('add', function(event, data, which_guitable_dbl_clicked){
//            //ako je is_guitable_view onda je dupli klik u guitable-u i data je row nad kojim je izvrsen dupli klik
//            if (response.is_guitable_view)
//            {
//                var clicked_row = data;
//                var clicked_model_id = clicked_row.attr('model_id');
//                var curr_ids = choose_wrap.data('search_model_ids').toString();
//                curr_ids_array = [];
//                if (curr_ids.indexOf(',') >= 0)
//                {
//                    curr_ids_array = curr_ids.split(",");
//                }
//                else if (curr_ids.length > 0)
//                {
//                    curr_ids_array = [curr_ids];
//                }
//                var left_guitable = choose_wrap.find('.sima-model-choose-left-guitable:first');//mora ovako jer nema fiksan id, moze da bude prosledjen
//                var right_guitable = $('#right_tbl_'+uniq_id);
//
//                if (which_guitable_dbl_clicked === 'LEFT')
//                {
//                    curr_ids_array.push(clicked_model_id);
//                }
//                else if (which_guitable_dbl_clicked === 'RIGHT')
//                {
//                    curr_ids_array.splice($.inArray(clicked_model_id, curr_ids_array), 1);
//                }
//                choose_wrap.data('search_model_ids', curr_ids_array.join());
//
//                var curr_ids = (curr_ids_array.length > 0) ? curr_ids_array : -1;
//                var left_guitable_curr_fixed_filter = left_guitable.simaGuiTable('getFixedFilter');
//                if (!sima.isEmpty(left_guitable_curr_fixed_filter[1]))
//                {
//                    left_guitable_curr_fixed_filter[1] = {
//                        0: 'NOT', 
//                        1: {
//                            ids: curr_ids
//                        }
//                    };
//                }
//                left_guitable.simaGuiTable('setFixedFilter', left_guitable_curr_fixed_filter);
//                right_guitable.simaGuiTable('setFixedFilter', {ids: curr_ids});
//
//                sima.model.updateViews(clicked_row.attr('tag'));
//
//                if (which_guitable_dbl_clicked === 'LEFT')
//                {
//                    right_guitable.simaGuiTable('append_row', clicked_model_id);
//                }
//                else if (which_guitable_dbl_clicked === 'RIGHT')
//                {
//                    left_guitable.simaGuiTable('append_row', clicked_model_id);
//                }
//            }
//            else
//            {
//                $.each(data, function(index, value) {
//                    if (ids_list.find(".item[data-id='" + value.id + "']").length === 0)
//                    {
//                        var span = $('<span class="item" data-id="'+value.id+'"></span>');
//                        var span_delete = $('<span class="sima-icon _delete _16" title="Obriši" onclick="$(this).parent().remove();"></span>');
//                        var span_name = $('<span class="name" title="'+$.trim(value.display_name)+'">'+$.trim(value.display_name)+'</span>');
//                        span.append(span_delete).append(span_name);
//                        ids_list.append(span);
//                    }
//                });
//            }
//        });
//
//        var ok_button = $("<span class='sima-search-model-choose-ok sima-dialog-button _yes-left'>"+sima.translate('Save')+"</span>");
//        choose_wrap.append(ok_button);
//        //na ok se zatvara dialog i poziva se ok_funkcija kojoj se prosledjuju id-evi
//        choose_wrap.on('click', '.sima-search-model-choose-ok', function(){
//            var ids_objs = [];
//            if (response.is_guitable_view)
//            {
//                var right_guitable = $('#right_tbl_'+uniq_id);
//                var models_ids = [];
//                right_guitable.find('.sima-guitable-row').each(function(){
//                    if (!sima.isEmpty($(this).attr('model_id')))
//                    {
//                        models_ids.push($(this).attr('model_id'));
//                    }
//                });
//                if (!sima.isEmpty(models_ids))
//                {
//                    sima.ajax.get('base/model/getModelsDisplayName',{
//                        get_params:{
//                            model: model
//                        },
//                        data: {
//                            models_ids: models_ids
//                        },
//                        success_function:function(response) {
//                            sima.dialog.close();
//                            ok_function(response.models_display_name);
//                        }
//                    });
//                }
//            }
//            else
//            {
//                ids_list.find('.item').each(function(){
//                    var _class = ($(this).hasClass('_hidden'))?'_hidden':($(this).hasClass('_non_editable'))?'_non_editable':'';
//                    ids_objs.push({'id':$(this).data('id'), 'display_name':$(this).find('.name').text(), class:_class});
//                });
//                sima.dialog.close();
//                ok_function(ids_objs);
//            }
//        });

        var ok_button = $("<span class='sima-search-model-choose-ok sima-dialog-button _yes-left'>"+sima.translate('Save')+"</span>");
        choose_wrap.append(ok_button);
        sima.dialog.openInFullScreen(choose_wrap);

        left_side.append(response.left_side_html);
        if (!sima.isEmpty(response.right_side_html))
        {
            right_side.append(response.right_side_html);
        }
        
        
        choose_wrap.on('click', '.sima-search-model-choose-ok', function(){
            if(response.right_side_is_guitable_view === true)
            {
                var right_guitable = $('#right_tbl_'+uniq_id);
                var models_ids = [];
                right_guitable.find('.sima-guitable-row').each(function(){
                    if (!sima.isEmpty($(this).attr('model_id')))
                    {
                        models_ids.push($(this).attr('model_id'));
                    }
                });
                if (!sima.isEmpty(models_ids))
                {
                    sima.ajax.get('base/model/getModelsDisplayName',{
                        get_params:{
                            model: model_name
                        },
                        data: {
                            models_ids: models_ids
                        },
                        success_function:function(response) {
                            sima.dialog.close();
                            ok_function(response.models_display_name);
                        }
                    });
                }
            }
            else
            {
                sima.addError('fullScreenMultiselect 1', 'not implemented');
//                var ids_objs = [];
//                ids_list.find('.item').each(function(){
//                    var _class = ($(this).hasClass('_hidden'))?'_hidden':($(this).hasClass('_non_editable'))?'_non_editable':'';
//                    ids_objs.push({'id':$(this).data('id'), 'display_name':$(this).find('.name').text(), class:_class});
//                });
//                sima.dialog.close();
//                ok_function(ids_objs);
            }
        });
//        
        choose_wrap.on('model_chosen', function(event, data){

            var clicked_model_ids = data;
            if (!$.isArray(clicked_model_ids))
            {
                clicked_model_ids = [clicked_model_ids];
            }

            var search_model_ids = choose_wrap.data('search_model_ids');
            $.each(clicked_model_ids, function(index, value) {
                if ($.inArray(value, search_model_ids) === -1)
                {
                    search_model_ids.push(value);
                }
            });
            choose_wrap.data('search_model_ids', search_model_ids);

            if(response.right_side_is_guitable_view === true)
            {
                
//                var curr_ids = choose_wrap.data('search_model_ids').toString();
//                curr_ids_array = [];
//                if (curr_ids.indexOf(',') >= 0)
//                {
//                    curr_ids_array = curr_ids.split(",");
//                }
//                else if (curr_ids.length > 0)
//                {
//                    curr_ids_array = [curr_ids];
//                }
//                curr_ids_array.push(clicked_model_id);
////                var curr_ids = (curr_ids_array.length > 0) ? curr_ids_array : -1;

                
                var right_guitable = $('#right_tbl_'+uniq_id);
                right_guitable.simaGuiTable('setFixedFilter', {ids: (search_model_ids.length > 0) ? search_model_ids : -1});
                right_guitable.simaGuiTable('append_row', clicked_model_ids);
            }
            else
            {
                sima.addError('fullScreenMultiselect 2', 'not implemented');
            }
        });
        choose_wrap.on('model_unchosen', function(event, data){
////            console.log(event);
////            console.log(data);
////            console.log(response);
//
            var clicked_model_id = data;
            var search_model_ids = choose_wrap.data('search_model_ids');
            search_model_ids.push(clicked_model_id);
            choose_wrap.data('search_model_ids', search_model_ids);
//
//            if(response.right_side_is_guitable_view === true)
//            {
//                var clicked_model_id = data;
//                
////                var curr_ids = choose_wrap.data('search_model_ids').toString();
////                curr_ids_array = [];
////                if (curr_ids.indexOf(',') >= 0)
////                {
////                    curr_ids_array = curr_ids.split(",");
////                }
////                else if (curr_ids.length > 0)
////                {
////                    curr_ids_array = [curr_ids];
////                }
////                curr_ids_array.push(clicked_model_id);
//////                var curr_ids = (curr_ids_array.length > 0) ? curr_ids_array : -1;
//
//                var search_model_ids = choose_wrap.data('search_model_ids');
//                search_model_ids.push(clicked_model_id);
//                choose_wrap.data('search_model_ids', search_model_ids);
//                
//                var right_guitable = $('#right_tbl_'+uniq_id);
//                right_guitable.simaGuiTable('setFixedFilter', {ids: search_model_ids});
//                right_guitable.simaGuiTable('append_row', clicked_model_id);
//            }
//            else
//            {
//                sima.addError('fullScreenMultiselect 2', 'not implemented');
//            }
        });
    }
    
    function fullScreenSingleselect(choose_wrap, response, ok_function)
    {
        //u ovom slucaju se zatvara dialog i poziva se ok funkcija
        choose_wrap.on('add', function(event,data){
            sima.dialog.close();
            ok_function(data);
        });
        sima.dialog.openInFullScreen(choose_wrap);
        choose_wrap.append(response.left_side_html);
    }
    
    function notFullScreen(choose_uniq, choose_wrap, left_side_html)
    {
        sima.addError('SIMAModelChoose - notFullScreen', 'not implemented');
//        sima.dialog.openYesNo(choose_wrap[0]['outerHTML'],
//            function(){ //yes_function
//                var id = $("#"+choose_uniq).find(".sima-ui-sf-input").val();
//                var display_name = $("#"+choose_uniq).find(".sima-ui-sf-display").val();
//                var return_value = {'id':id, 'display_name':display_name};
//
//                if (typeof can_be_null === 'undefined')
//                {
//                    can_be_null = false;
//                }
//
//                if ((id !== '' && display_name !== '') || can_be_null === true)
//                {
//                    sima.dialog.close();
//                    ok_function(return_value);
//                }
//                else
//                {
//                    sima.dialog.openWarn('Niste odabrali vrednost');
//                }
//            }
//        );
//        $("#"+choose_uniq).append(left_side_html);
    }
}
