
/* global sima */
$('body').on('sima-layout-allign', '.sima-page-wrap', function(event, height, width, source) {
    if (event.target === this)
    {
        var _resizable_layout = $(event.target);
        if (sima.layout.log())
        {
            console.log(source);
            console.log('HANDLER_base_layout za: ');
            console.log(_resizable_layout);
            console.log('height: ' + height + ' width: ' + width);
        }

        _resizable_layout.height(height);
        _resizable_layout.width(width);

        var _child = _resizable_layout.children('.sima-layout-panel');
        if (_child.length > 1)
        {
            console.error('POSTOJI VISE OD 1 DINAMICKOG PANELA U PANELU KOJI NIJE SPLITER-u: ');
            console.error(_resizable_layout);
        }
        if ( _child.length > 0)
        {
            _resizable_layout.addClass('_has_layout_children');
        }

        //racunanje fiksnih panela sima-layout-fixed-panel
        var _fix_panel_height = 0;
        var _fix_panel_width = 0;
        var _fix_panels = _child.siblings('.sima-layout-fixed-panel');
        if (_fix_panels.length > 0)
        {
            if (_resizable_layout.hasClass('_vertical'))
            {
                _fix_panels.each(function(){
                    _fix_panel_width += $(this).outerWidth(true);
                });
            }
            if (_resizable_layout.hasClass('_horizontal'))
            {
                _fix_panels.each(function(){
                    _fix_panel_height += $(this).outerHeight(true);
                });
            }
        }

        var _init_data = _child.data('simaLayoutInit') || {};

        if ( typeof _init_data['height_cut'] === 'undefined' ) _init_data['height_cut'] = 0;
        if ( typeof _init_data['width_cut'] === 'undefined' ) _init_data['width_cut'] = 0;

        //NOVA VISINA treba da se umanje za cut visine i fix panele
        var _child_height = height - _init_data['height_cut'] - _fix_panel_height;

        //NOVA SIRINA
        var _child_width = width - _init_data['width_cut'] - _fix_panel_width;
        
        var page_wrap_title = _resizable_layout.find('.sima-page-title:first');
        var page_wrap_title_name = page_wrap_title.find('.sima-page-title-name:first');
        var page_wrap_title_options = page_wrap_title.find('.sima-page-title-nav:first');
        //-1 zbog toga sto chrome radi sa decimalnim velicinama, pa se sirina ne uklapa
        page_wrap_title_name.width(page_wrap_title.outerWidth(true) - page_wrap_title_options.outerWidth(true) - 1, true);

        //REKURZIVNO POZIVANJE DECE
        sima.layout.setLayoutSize(_resizable_layout.children('.sima-layout-panel'),_child_height,_child_width,'TRIGGER_base_layout_handler');

        event.stopPropagation();
    }
});