
/* global sima */

(function($) {
    var methods = {
        init: function(params)
        {
            var _table = this;
            
            _table.data('row_select_func',[]);
            _table.data('row_unselect_func',[]);
            _table.data('row_dblclick_func',[]);
            _table.data('query_data',params.data);
         
            _table.data('_header', _table.find('thead'));
            
//            var alreadyclicked = false, clickDelay = 180; //milliseconds
//            this.delegate('.row', 'click', function() {
//                var row = $(this);
//                if (alreadyclicked)
//                {
//                    alreadyclicked = false; // reset
//                    clearTimeout(alreadyclickedTimeout); // prevent this from happening
//                    dblclick_row(row);
//                }
//                else
//                {
//                    alreadyclicked = true;
//                    alreadyclickedTimeout = setTimeout(function() {
//                        alreadyclicked = false; // reset when it happens
//                        click_row(row);
//                    }, clickDelay); // <-- dblclick tolerance here
//                }
//            });
        },
                
        append_row: function(model_id) {
            var _table = this;
            var data = _table.data('query_data');
            data.columns = get_columns(_table);
            data[_table.data('model')+"[ids]"] = model_id;
            data.without_order_number = true;
            console.log('poziva dodavanje reda');
            
            prependFakeRow(_table);
            
            sima.ajax.get('guitable/getRow', {
                get_params: {
                    model: _table.data('model')
                },
                async: true,
                data: data,
                success_function: function(response) {
                    removeFakeRow(_table);
                    
                    var rows = response.rows;
                    if (typeof rows[0] !== 'undefined')
                    {
                        _table.find('tbody').append(rows[0].row);
                    }
                }
            });
        },
        refresh_row: function(model_ids) {
            var _table = this;
            
            var data = _table.data('query_data');
            if (typeof _table.data('_default_filter') !== 'undefined')
            {
                var filter = _table.data('_default_filter');
                data[_table.data('model')] = filter;
            }
            
            data.columns = get_columns(_table);
            data[_table.data('model')+"[ids]"] = model_ids;
            data['model_ids'] = model_ids;
            data.without_order_number = true;
           
           //disable-ujemo sve redove za koje se radi refresh
            $.each(model_ids, function(key, model_id) {
                var table_row = _table.find('.sima-guitable-row[model_id=' + model_id + ']');
                if (!sima.isEmpty(table_row))
                {
                    table_row.addClass('_disabled');
                }
            });
           
            sima.ajax.get('guitable/getRow', {
                get_params: {
                    model: _table.data('model')
                },
                async: true,
                data: data,
                success_function: function(response) {
                    var rows = response.rows;
                    $.each(rows, function(key, value) {
                        var old = _table.find('tbody .sima-guitable-row[model_id=' + value.model_id + ']');
                        old.after(value.row);
                        old.remove();
                    });
                    
                    //enable-ujemo sve redove za koje se radi refresh
                    $.each(model_ids, function(key, model_id) {
                        var table_row = _table.find('.sima-guitable-row[model_id=' + model_id + ']');
                        if (!sima.isEmpty(table_row))
                        {
                            table_row.removeClass('_disabled');
                        }
                    });
                }
            });
        },
        setDefaultFilter : function(filter)
        {
            var _table = this;
            
            function createObj(resto, params, value)
            {
                if ((typeof params === 'array' || typeof params === 'object') && params.length > 1)
                {
                    if (typeof resto[params[0]] === 'undefined')
                    {
                        resto[params[0]] = {};
                    }
                    var new_arr = [];
                    for (var i=1; i<params.length; i++)
                    {
                        new_arr.push(params[i]);
                    }
                    createObj(resto[params[0]], new_arr, value);
                }
                else if (params.length == 1)
                {
                    resto[params[0]] = value;
                }
                
                return resto;
            }
            
            function resolve_obj(prefix,obj)
            {
                var resto = {};
                for (var i in obj)
                {
                    if (typeof obj[i] !=='object')
                    {
                        //ako je relacija
                        if(i.indexOf('.') !== -1)
                        {
                            var params = i.split('.');
                            resto = createObj(resto, params, obj[i]); //kreira objekat od niza polja
                        }
                        else
                        {
                            resto[i] = obj[i];
                        } 
                    }
                    else
                    {
                        var new_prefix = (prefix==='')?i:(prefix+'.'+i);
                        resto[i] = resolve_obj(new_prefix,obj[i]);
                    }
                }
                return resto;
            };
            
            var main_resto = resolve_obj('',filter);
            this.data('_default_filter', main_resto);
        }
//
//        setRowSelect: function(func) {
//            this.data('row_select_func').push(func);
//        },
//                
//        setRowUnSelect: function(func) {
//            this.data('row_unselect_func').push(func);
//        },
//        
//        setRowDblClick:  function(func) {
//            this.data('row_dblclick_func').push(func);
//        },
//
//        
//        
//        

    };
    
    function get_columns(_table)
    {
        var _columns = [];
        _table.data('_header').find('th:not(._hidden)').each(function() {
            var column = $(this).attr('column');
            if (typeof (column) === 'string' && column !== '')
                _columns.push(column);
        });
        return _columns;
    }
    
    function prependFakeRow(_table)
    {
        var columns_html = '';

        _table.find('thead th').each(function() {
            columns_html += '<td class="' + $(this).attr('class') + '"><div></div></td>';
        });
        
        _table.prepend($("<tr class='sima-guitable-fake-row'>'" + columns_html + "'</tr>"));
    }
    
    function removeFakeRow(_table)
    {
        _table.find('tbody .sima-guitable-fake-row').remove();
    }
    
//    function click_row(row)
//    {
//        if (selected != null)
//        {
//            for (var i = 0; i < row_unselect_func.length; i++)
//                row_unselect_func[i](selected);
//            if (row.hasClass('selected'))
//            {
//                selected.removeClass('selected');
//                return;
//            }
//            selected.removeClass('selected');
//        }
//
//        selected = row;
//        selected.addClass('selected');
//
//        if (row.hasClass('selected'))
//            for (var i = 0; i < row_select_func.length; i++)
//                row_select_func[i](selected);
//    }
//
//    function dblclick_row(row)
//    {
//        var i = 0;
//        for (i = 0; i < row_dblclick_func.length; i++)
//            row_dblclick_func[i](row);
//        if (i == 0)
//            alert('ne postoje omogucene opcije');
//
//    }
//    
//    
//
//    this.hideOptions = function()
//    {
//        _table.find('.options').hide();
//    };
//
//    if (sima.dialog.inTopDialog(_table))
//        sima.dialog.TopDialogFullScreen();

    $.fn.simaSimpleTable = function(methodOrOptions) {
        var ar = arguments;
        this.each(function() {
            if (methods[methodOrOptions]) {
                return methods[ methodOrOptions ].apply($(this), Array.prototype.slice.call(ar, 1));
            } else if (typeof methodOrOptions === 'object' || !methodOrOptions) {
                // Default to "init"
                return methods.init.apply($(this), ar);
            } else {
                $.error('Method ' + method + ' does not exist on jQuery.simaGuiTable');
            }
        });
    };

})(jQuery);

//function SIMASimpleTableDisplay(t)
//{
//    var _table = t;
////	var url = u;
//
////	var search_text = null;
////	var filter = null;
//
////	var selected = null;
////	this.getSelected = function(){return selected;};
//
////	var multiselect = false;
//
////	var js_data_save = '';
//
////	var curr_page = 1;
////	var max_page = 1;
////	function setMaxPage(new_max_page)
////	{
////		max_page = new_max_page;
////		_table.find('span.curr_page').html(curr_page+'/'+max_page);
////	}
////	setMaxPage(max_page);
//
////	var _header = _table.find('div.pt_head');
////	var _body = _table.find('div.pt_body');
////	var _footer = _table.find('div.pt_foot');
//
//    var row_select_func = new Array();
//    var row_unselect_func = new Array();
//    var row_dblclick_func = new Array();
//
//    this.setRowDblClick = function(func) {
//        row_dblclick_func.push(func);
//    };
//    this.setRowSelect = function(func) {
//        row_select_func.push(func);
//    };
//    this.setRowUnSelect = function(func) {
//        row_unselect_func.push(func);
//    };
////	this.setMultiSelect 	= function(){multiselect=true;};
//
//
////	var selected_sum_status = false;
////	this.selected_sum = function() 
////	{
////		if (this.checked)
////		{
////			selected_sum_status = true;
////			multiselect = true;
////			if (selected!=null) selected.removeClass('selected');
////			selected = null;
////			
////			populate_selected_sum();
////		}
////		else
////		{
////			selected_sum_status = false;
////			multiselect = false;
////			_table.find('.row.selected').removeClass('selected');
////			
////			populate_sum('');
////		}
////	};
//
//
//
//
//    
//
//    
//
//}