/* global sima */

/**
 *  Document   : jquery.numberField
 Author     : sasa
 Description:
 
 */

(function($) {
    var methods = {
        init: function(params)
        {
            var _this = this;
            params = params || {};
            
            _this.data('INTEGER_MIN', -2147483648);
            _this.data('INTEGER_MAX', 2147483647);
            _this.data('key_down_code', null);
            
            _this.data('number_validation', (typeof params.number_validation !=='undefined') ? params.number_validation : true);
            _this.data('max_value', (typeof params.max_value !=='undefined') ? params.max_value : null);
            
            //separator za hiljade
            _this.data('thousands', (typeof params.thousands !=='undefined') ? params.thousands : ' ');
            
            //separator za decimale
            _this.data('decimal', (typeof params.decimal !=='undefined') ? params.decimal : '.'); //default je 0.00
            
            //ukupan broj cifara
            _this.data('precision', (typeof params.precision !=='undefined') ? params.precision : null);
            
            //broj decimala
            _this.data('scale', (typeof params.scale !=='undefined') ? params.scale : 2); //default je 0.00
            
            _this.data('old_value', null);
            
            //promenljiva u kojoj se pamti broj pojavljivanja separatora za hiljade            
            _this.data('thousands_splitter_occurence', 0);
            //promenljiva u kojoj se pamti broj nula koji je obrisan sa pocetka
            _this.data('left_zero_removed_number', 0);
            
            _this.data('isCtrl', false);
            
            _this.data('minus', (typeof params.minus !=='undefined' && params.minus) ? '-' : '');

            if (_this.data('thousands') === _this.data('decimal'))
            {
                sima.dialog.openWarn('Separator za hiljade i decimale ne moze biti isti!');
            }
            else
            {
                setField(_this);
            }
            
            _this.data('onchange',null);
            if (typeof params.onchange !=='undefined' && params.onchange !== '')
            {
                _this.data('onchange', params.onchange);
            }
            
            _this.data('globalTimeout', null);
            
            //events            
            _this.bind("keydown", {_this:_this}, keydownEvent);
            _this.bind("keypress", {_this:_this}, keypressEvent);
            _this.bind("keyup", {_this:_this}, keyupEvent);
            _this.bind("focus", {_this:_this}, function() {
                var caret_position = _this.val().indexOf(_this.data('decimal'));
                _this.caret(caret_position);
            });
            _this.bind("mouseup", {_this:_this}, function(event) {
                if (sima.misc.isInputTextSelected(_this[0]) && getRealValue(_this) == 0) //mora == jer getRealValue vraca broj u obliku stringa zbog velikih brojeva
                {
                    _this.caret(_this.val().indexOf(_this.data('decimal')));
                    event.preventDefault();
                }
            });
            
            //setujemo defaultValue jer jquery setuje tu vrednost prilikom prvog dodeljivanja vrednosti inputu, a nama treba vrednost koja je prosla parsiranje
            _this.prop('defaultValue', _this.val());
        },
        resetValue: function()
        {
            var _this = this;
            setField(_this);
        },
        getRealValue: function()
        {
            var _this = this;
            return getRealValue(_this);
        },
        setValue: function(value)
        {
            var _this = this;
            
            _this.val(value);
            setField(_this);
        }
    };
    
    /**
     * Funkcija koja setuje pravilan zapis hiljadarki i decimala
     * @param {obj} _this
     */
    function setField(_this)
    {
        var decimal = _this.data('decimal');
        var input_value = _this.val();
        var decimal_occurence = input_value.split(decimal).length-1;
        var thousands = '';
        var decimals = '';        
        if (parseInt(_this.data('scale')) > 0)
        {
            //ako input sadrzi decimalni separator
            if(input_value.indexOf(decimal) !== -1)
            {                
                thousands = setFieldThousands(_this, input_value.substr(0, input_value.indexOf(decimal)));
                decimals = setFieldDecimals(_this, input_value.substr(input_value.indexOf(decimal) + 1));                
                _this.val(thousands+decimal+decimals);
            }
            else
            {                
                //ako input sadrzi druge karaktere osim brojeva obavestavamo korisnika da se ne slaze broj sa zadatim separatorom za decimale
                thousands = setFieldThousands(_this, input_value);
                decimals = setFieldDecimals(_this);
                _this.val(thousands+decimal+decimals);
            }
        }
        else
        {
            thousands = setFieldThousands(_this, input_value);
            _this.val(thousands);
        }        
        
        //dodajemo minus na pocetku ako postoji
        if (_this.data('minus') !== '')
        {
            _this.val(_this.data('minus')+_this.val());
        }
        
        return {
            decimals:decimals,
            thousands:thousands
        };
    }
    
    function setFieldThousands(_this, thousands)
    {       
        //prvo brisemo separator za hiljade
        thousands = thousands.split(_this.data('thousands')).join('');
        var result = '';        
        thousands = thousands.replace(/\D/g,'');        
        if (thousands !== '')
        {
            var reverse_result = '';
            var i = thousands.length;
            var j = 1;

            for (i=thousands.length-1; i>=0; i--)
            {
                reverse_result += thousands[i];
                if ((j%3) === 0 && i !== 0)
                {
                    reverse_result += _this.data('thousands');
                    _this.data('thousands_splitter_occurence',_this.data('thousands_splitter_occurence')+1);
                }
                j++;
            }

            for (var i=reverse_result.length-1; i>=0; i--)
            {
                result += reverse_result[i];
            }
        }
        else if (thousands === '')
        {
            result = '0';
        }        
        //proveravamo da li hiljade pocinju sa nulom. Ako pocinju onda brisemo sve nule od pocetka. Ako su svi brojevi nule onda ostavljamo samo
        //jednu nula
        var left_zero_removed_number = 0;
        for (var i = 0; i < result.length - 1; i++)
        {
            if  (
                    result.length > 1 && 
                    (
                        result[i] === '0' || result[i] === _this.data('thousands')
                    )
                )
            {
                left_zero_removed_number++;
            }
            else
            {
                break;
            }
        }
        if (left_zero_removed_number > 0)
        {
            result = result.substring(left_zero_removed_number, result.length);
            //kursor pomeramo za jedno mesto samo ako je obrisana jedna nula, a ako su vise onda ga ne pomeramo(Sasa A. - nisam siguran zasto je tako ali radi)
            if (left_zero_removed_number === 1)
            {
                _this.data('left_zero_removed_number', 1);
            }
        }

        result = $.trim(result);
        
        return result;
    }
    
    function setFieldDecimals(_this, decimals)
    {
        var result = '';
        var scale = parseInt(_this.data('scale'));
        //ako su decimale definisane onda ispisemo onoliko decimala koliko je zadato kao parametar
        if (typeof decimals !== 'undefined')
        {
            decimals = decimals.replace(/\D/g,'');
            for (var i=0; i<scale; i++)
            {
                if (decimals.charAt(i) !== '')
                {
                    result += decimals.charAt(i);
                }
                else
                {
                    result += '0';
                }
            }
        }
        //ako nisu definisane onda dodamo nule u zavisnosti od broja definisanog za broj decimala
        else
        {
            for (var i=0; i<scale; i++)
            {
                result += '0';
            }
        }           
        
        return result;
    }  
    
    /**
     * Mora da postoji jer jedino u keydown i keyup imamo tacan kod koji je kliknut. Ako hendlujemo u keyup-a tamo je vec upisan karakter,
     * tako da mora da se hendluje u keydown funkciji
     * @param {type} event
     * @returns {Boolean}
     */
    function keydownEvent(event)
    {        
//        console.log('keydown');
        var _this = $(this);
        //Zabranjuje se unos vise brojeva na keydown, pamcenjem poslednjeg key_down_code omogucuje se izvrsavanje kombinacija (ctrl+v)
        if( _this.data('key_down_code') !== null && _this.data('key_down_code') === event.which)
        {
            return false;
        }
        var input_value = _this.val();        
        var decimal = _this.data('decimal');
        var thousands = _this.data('thousands');
                 
        if (event.which === 8) //backspace
        {
            var char_before_caret_position = input_value.charAt(_this.caret() - 1);
            if (char_before_caret_position === decimal || char_before_caret_position === thousands)
            {                
                _this.caret(_this.caret()-1);
            }
            if (char_before_caret_position === '-')
            {
                _this.data('minus','');
            }
        }
        else if (event.which === 46) //del
        {
            var char_at_caret_position = input_value.charAt(_this.caret());
            if (char_at_caret_position === decimal || char_at_caret_position === thousands)
            {
                _this.caret(_this.caret()+1);
            }
            if (char_at_caret_position === '-')
            {
                _this.data('minus','');
            }
        }
        else if (event.which === 109 || event.which === 173) //minus(crtica)
        {
            if (_this.data('minus') !== '')
            {
                return false;
            }
            _this.data('minus','-');
        }
        else if (event.which === 17) //ctrl
        {
            _this.data('isCtrl', true);
        }
        //ako nije validan karakter
        else if (!is_valid_charachter(_this, event.which))
        {
            return false;
        }
        
        _this.data('old_value', _this.val());
        _this.data('key_down_code', event.which);
    }
    
    /**
     * Funkcija keypress mora da postoji da bi se prepoznao koji je karakter kliknut. U keyup i keydown(ne prepoznaje tacku i zarez) nemamo tu informaciju.
     * @param {type} event
     * @returns {Boolean}
     */
    function keypressEvent(event)
    {
//        console.log('keypress');
        var _this = $(this);
        var input_value = _this.val();        
        var decimal = _this.data('decimal');
        
        //ako postoji vec decimalni separator onda ubacujemo novi a postojeci brisemo
        if (String.fromCharCode(event.which) === decimal)
        {
            var decimal_occurence = input_value.split(decimal).length-1;
            if (decimal_occurence > 0)
            {
                var new_value = input_value.replace(decimal, '');
                if (_this.caret() > input_value.indexOf(decimal))
                {
                    new_value = new_value.substr(0, _this.caret()-1) + decimal + new_value.substr(_this.caret()-1);
                }
                else
                {
                    new_value = new_value.substr(0, _this.caret()) + decimal + new_value.substr(_this.caret());
                }
                //bugfix za Chrome - mora da se prvo sacuva trenutna pozicija caret-a pa nakon upisa vrednosti u input da se opet postavi
                var temp_caret_pos = _this.caret();
                _this.val(new_value);
                _this.caret(temp_caret_pos);
            }
        }
    }
    
    /**
     * Moramo imati ovu funkciju jer u njoj se setuje separator za hiljade. U keypress-u ne moze biti ovo, jer tek u keyup-u imamo poslednju vrednost inputa
     * @param {type} event
     * @returns {undefined}
     */
    function keyupEvent(event)
    {        
//        console.log('keyup');

        var _this = $(this);
        _this.data('key_down_code', null);
        //provera za tab mora biti tu jer ako je stavimo u keydown, bezobzira na return true, keyup event se izvrsava, sto ne bi trebalo.
        if (event.which === 9) //tab
        {
            return true;
        }

        //posto u keyupEvent imamo celu vrednost inputa i ako se u njoj ne sadrzi minus, onda ga brisemo i iz lokalne promenljive(na osnovu koje se kasnije vrsi ispis minusa)
        if (_this.val().indexOf('-') === -1)
        {
            _this.data('minus', '');
        }
        
        if (_this.data('number_validation') === true)
        {
            //proveravamo da li broj validan. Ako nije onda ga vracamo na prethodnu vrednost
            var message = checkIsValidNumber(_this);
            if (message !== '')
            {
                _this.val(_this.data('old_value'));
                setTimeout(function(){_this.blur();},0);
                sima.dialog.openWarn(message, '', {modal:true});
            }
        }

        //ako ctrl nije ukljucen ili ako je ctrl+x, onda mora da pozovemo da se preracuna vrednost inputa jer je promenjena
        if (_this.data('isCtrl') === false || event.which === 88 || event.which === 86)
        {
            var caret_position = _this.caret();

            //broj pojavljivanja separatora za hiljade do pozicije kareta
            var thousands_splitter_number = _this.val().substring(0,caret_position).split(_this.data('thousands')).length-1;

            var value_parts = setField(_this);
            //ako je hiljaditi deo nula onda karet mora biti uvek iza nule
            if (value_parts.thousands === '0' && caret_position === 0)
            {
                caret_position += 1;
            }
            
            //ako je pozicija kareta na pocetku, a imamo minus, onda poziciju pomerimo posle minusa
            if (caret_position === 0 && _this.data('minus') !== '')
            {
                caret_position += 1;
            }
            
            //setujemo poziciju kareta
            if (thousands_splitter_number !== _this.data('thousands_splitter_occurence'))
            {
                var new_thousands_splitter_number = _this.val().substring(0,caret_position).split(_this.data('thousands')).length-1;
                _this.caret(caret_position+new_thousands_splitter_number-thousands_splitter_number-_this.data('left_zero_removed_number'));
            }
            else
            {
                _this.caret(caret_position-_this.data('left_zero_removed_number'));
            }
        }
        
        _this.data('old_value', null);
        _this.data('thousands_splitter_occurence', 0);
        _this.data('left_zero_removed_number', 0);
        if (event.which === 17) //ctrl
        {
            _this.data('isCtrl', false);
        }
        if(_this.data('globalTimeout') !== null) 
        {
            clearTimeout(_this.data('globalTimeout'));            
        } 
        _this.data('globalTimeout', setTimeout(function(){            
            sima.callFunction(_this.data('onchange'));
        },500));
    }
    
    function is_valid_charachter(_this, code)
    {
        //ako je broj, backspace, tab, delete, zarez, tacka, levo, desno, minus(crtica)...
        if (
                (code >= 48 && code <= 57) || (code >= 96 && code <= 105) || code === 8 || code === 9 || code === 0 || code === 46 || code === 188 
                || code === 190 || code === 110 || code === 37 || code === 39 || code === 109 || code === 173 || 
                (_this.data('isCtrl') === true && (code === 65 || code === 67 || code === 86 || code === 88)) || //select all,copy,paste,cut
                code === 35 || code === 36 || code === 13 || code === 27 //end, home, enter, esc
            )
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    function checkIsValidNumber(_this)
    {
        var value = getRealValue(_this);
        var precision = _this.data('precision');
        var scale = _this.data('scale');
        var max_value = _this.data('max_value');

        var message = '';

        if (value !== '')
        {
            if (max_value !== null)
            {
                if(value > max_value)
                {
                    message = sima.translate('NumberFieldMaxAllowedValue', {
                        '{max_value}': max_value.toFixed(scale).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1 ').replace(".", _this.data('decimal'))
                    });
                    return message;
                }
            }
            if (scale === 0) //proveravamo da li je ispravan integer
            {
                var parse_int = parseInt(value);

                if (isNaN(parse_int) || parse_int < _this.data('INTEGER_MIN') || parse_int > _this.data('INTEGER_MAX'))
                {
                    message = sima.translate('NotValidIntegerNumber', {
                        '{int_min}': _this.data('INTEGER_MIN'),
                        '{int_max}': _this.data('INTEGER_MAX')
                    });
                }
            }
            else if (scale > 0 && typeof precision !== 'undefined' && precision !== null) //ako je numericki broj proveravamo precision i scale
            {
                var num = Math.abs(value);
                var max = '9'.repeat(precision - scale) + _this.data('decimal') + '9'.repeat(scale);

                if (num > max)
                {
                    message = sima.translate('NotValidNumericNumber', {
                        '{precision}': precision,
                        '{scale}': scale
                    });
                }
            }
        }
        
        return message;
    }
    
    function getRealValue(_this)
    {
        var value = _this.val();

        return value.replace(new RegExp(_this.data('thousands'), 'g'), '').replace(new RegExp(',', 'g'), '.');
    }
    
    $.fn.numberField = function(methodOrOptions) {
        var ar = arguments;
        var i=1;
        var ret = this;
        this.each(function() {
            i++;
            if (methods[methodOrOptions]) {
                var povr = methods[ methodOrOptions ].apply($(this), Array.prototype.slice.call(ar, 1));
                if (typeof povr !== 'undefined')
                {
                    ret = povr;
                }
                return;
            } else if (typeof methodOrOptions === 'object' || !methodOrOptions) {
                // Default to "init"
                return methods.init.apply($(this), ar);
            } else {
                $.error('Method ' + methodOrOptions + ' does not exist on jQuery.numberField');
            }
        });
        return ret;
    };
    
})(jQuery);