
(function($) {
  $.event.special.destroyed = {
    remove: function(o) {
      if (o.handler) {
        o.handler();
      }
    }
  };
  $(window).resize(function() {
    if(this.resizeTO) clearTimeout(this.resizeTO);
    this.resizeTO = setTimeout(function() {
        $(this).trigger('resizeEnd');
    }, 500);
  });
  
  jQuery.fn.onPositionChanged = function (trigger, millis) {
        if (millis === null) millis = 100;
        var o = $(this[0]); // our jquery object
        if (o.length < 1) return -1;

        var lastPos = null;
        var lastOff = null;
        return setInterval(function () {
            if (o === null || o.length < 1) return o; // abort if element is non existend eny more
            if (lastPos === null) lastPos = o.position();
            if (lastOff === null) lastOff = o.offset();
            var newPos = o.position();
            var newOff = o.offset();
            if (lastPos.top !== newPos.top || lastPos.left !== newPos.left) {
                $(this).trigger('onPositionChanged', { lastPos: lastPos, newPos: newPos });
                if (typeof (trigger) === "function") trigger(lastPos, newPos);
                lastPos = o.position();
            }
            if (lastOff.top !== newOff.top || lastOff.left !== newOff.left) {
                $(this).trigger('onOffsetChanged', { lastOff: lastOff, newOff: newOff});
                if (typeof (trigger) === "function") trigger(lastOff, newOff);
                lastOff= o.offset();
            }
        }, millis);

//        return o;
    };
    
    $.fn.visible = function(partial, container)
    {
        container = container || $('body');
        
        var contHeight = container.height();

        var elemTop = this.offset().top - container.offset().top;
        var elemBottom = elemTop + this.height();

        var isTotal = (elemTop >= 0 && elemBottom <=contHeight);
        var isPart = ((elemTop < 0 && elemBottom > 0 ) || (elemTop > 0 && elemTop <= container.height())) && partial;

        return isTotal || isPart;
    };
    
    $.fn.getScrollParent = function(includeHidden) {
        element = this[0];
        var style = getComputedStyle(element);
        var excludeStaticParent = style.position === "absolute";
        var overflowRegex = includeHidden ? /(auto|scroll|hidden)/ : /(auto|scroll)/;

        if (style.position === "fixed") return $(document.body);
        for (var parent = element; (parent = parent.parentElement);) {
            style = getComputedStyle(parent);
            if (excludeStaticParent && style.position === "static") {
                continue;
            }
            if (overflowRegex.test(style.overflow + style.overflowY + style.overflowX)) return $(parent);
        }

        return $(document.body);
    };

})(jQuery);

