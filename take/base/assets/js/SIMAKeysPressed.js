/* global sima */

function SIMAKeysPressed()
{
    this.ctrl = false;
    this.shift = false;
}

$(window).keydown(function(event){
    const old_ctrl = sima.keysPressed.ctrl;
    const old_shift = sima.keysPressed.shift;
    
    if(event.which === 17) /// ctrl
    {
        sima.keysPressed.ctrl = true;
    }
    else if(event.which === 16) /// shift
    {
        sima.keysPressed.shift = true;
    }
    
    if(
        (
            sima.getModelDisplayhtmlClickOpenConf() === 'klik' 
            && old_ctrl === false && sima.keysPressed.ctrl === true
        )
        ||
        (
            sima.getModelDisplayhtmlClickOpenConf() === 'ctrl+klik' 
            && sima.keysPressed.ctrl === true
            && sima.keysPressed.shift === true
            && 
            (old_ctrl === false || old_shift === false)
        )
    )
    {
        $('.sima-model-link:visible').addClass('_open_new_tab');
    }
});

$(window).keyup(function(event){
    onKeyUp(event.keyCode);
});

$(window).blur(function() {
    onKeyUp('SIMA-ONBLUR');
});

function onKeyUp(keyCode)
{
    const old_ctrl = sima.keysPressed.ctrl;
    const old_shift = sima.keysPressed.shift;
    
    if(keyCode==='SIMA-ONBLUR')
    {
        sima.keysPressed.ctrl = false;
        sima.keysPressed.shift = false;
    }
    else
    {
        if(keyCode === 17) /// ctrl
        {
            sima.keysPressed.ctrl = false;
        }
        else if(keyCode === 16) /// shift
        {
            sima.keysPressed.shift = false;
        }
    }
    
    if(
        (
            sima.getModelDisplayhtmlClickOpenConf() === 'klik' 
            && old_ctrl === true && sima.keysPressed.ctrl === false
        )
        ||
        (
            sima.getModelDisplayhtmlClickOpenConf() === 'ctrl+klik' 
            && sima.keysPressed.ctrl === false
            && sima.keysPressed.shift === false
            && 
            (old_ctrl === true || old_shift === true)
        )
    )
    {
        $('.sima-model-link._open_new_tab').removeClass('_open_new_tab');
    }
}