/* global sima */

/**
 *  Document   : jquery.simaPopup
 Author     : sasa
 Description:
 params: {
 
    on_position_changed_container: jQuery objekat u odnosu na koji se gleda da li je uradjen scroll
    on_position_changed_reaction: default je 'HIDE', gde se sakrije na skrol, a moze da se stavi 'MOVE' i onda se pomera. 
            Ovo radi samo ako postoji container
 
 }
 */

(function ($) {
    var methods = {
        init: function (appendToEl, relativeToEl, position, params)
        {
            var popup = this;          
            popup.data('relativeToEl', relativeToEl);
            var _position = (!sima.isEmpty(position))?position:'bottom';
            popup.data('position', _position);
            popup.data('appendToEl', appendToEl);
            if (!sima.isEmpty(appendToEl))
            {
                appendToEl.append(popup);
            }
            popup.addClass('sima-popup');
            refresh(popup);
            relativeToEl.bind('destroyed', function() {
                if (!sima.isEmpty(popup))
                {
                    popup.remove();
                }
            });
            
            if(!sima.isEmpty(params) && typeof params.on_position_changed_container !== 'undefined')
            {
                popup.data('on_position_changed_container',params.on_position_changed_container);
                popup.data('on_position_changed_move',
                            typeof params.on_position_changed_reaction !== 'undefined' 
                            && 
                            params.on_position_changed_reaction === 'MOVE'
                );
            }
            else
            {
                popup.data('on_position_changed_container',null);
            }
        },
        refresh: function()
        {
            refresh($(this));
        },
        toggle: function()
        {
            toggle($(this));
        }
    };
    
    function toggle(popup)
    {
        popup.toggleClass('_sima-popup-shown');
        popup.toggle();
        var _on_position_changed_container = popup.data('on_position_changed_container');
        if(popup.hasClass('_sima-popup-shown') && _on_position_changed_container !== null)
        {
            var relativeToEl = popup.data('relativeToEl');
            var intervalInteger =  relativeToEl.onPositionChanged(function(){
                if(relativeToEl.visible(false, _on_position_changed_container) && popup.data('on_position_changed_move'))
                {
                    if(popup.hasClass('_sima-popup-shown'))
                    {
                        refresh(popup);
                    }
                }
                else
                {
                    if(popup.hasClass('_sima-popup-shown'))
                    {
                        toggle(popup);
                    }
                }
            }, 10);
            popup.data('intervalInteger',intervalInteger);
            popup.trigger('sima-popup-shown');
        }
        else
        {
            clearInterval(popup.data('intervalInteger'));
            popup.trigger('sima-popup-hidden');
        }
    }

    function refresh(popup)
    {
        var relativeToEl = popup.data('relativeToEl');
        if (popup.is(':visible'))
        {            
            //ako je element za koji je vezan popup(relativeToEl) unutar dialoga povecavamo z-index od popup-a
            if (relativeToEl.parents('.ui-dialog').length > 0)
            {                
                popup.css({'z-index':parseInt(relativeToEl.parents('.ui-dialog').css('z-index'))+100});
            }
            
            //ako je element za koji je vezan popup(relativeToEl) unutar popupa povecavamo z-index od popup-a
            if (relativeToEl.parents('.sima-popup').length > 0)
            {                
                popup.css({'z-index':parseInt(relativeToEl.parents('.sima-popup').css('z-index'))+10});
            }
                        
            if (popup.data('position') === 'bottom')
            {
                setLeftRightAlign(popup);
                setBottomPosition(popup);
            }
            else if (popup.data('position') === 'top')
            {
                setLeftRightAlign(popup);
                setTopPosition(popup);
            }
            else if (popup.data('position') === 'right')
            {
                popup.offset({top: relativeToEl.offset().top});
                setRightPosition(popup);
            }
            else if (popup.data('position') === 'left')
            {
                setTopBottomAlign(popup);
                setLeftPosition(popup);
            }
        }
    }   
    
    function setBottomPosition(popup)
    {        
        var relativeToEl = popup.data('relativeToEl');
        var relativeToElTop = relativeToEl.offset().top;
        var relativeToElHeight = relativeToEl.outerHeight(true);
        var popupHeight = popup.outerHeight(true);
        var distance_from_top = relativeToElTop;
        var distance_from_bottom = $(window).height() - relativeToElTop - relativeToElHeight;

        //ako ima mesta popup ide dole
        if (distance_from_bottom >= popupHeight)
        {            
            popup.offset({top: relativeToElTop + relativeToElHeight});
        }
        //ako ima mesta popup ide gore
        else if (distance_from_top >= popupHeight)
        {            
            popup.offset({top: relativeToElTop - popupHeight});            
        }        
        //ako nema mesta ni dole ni gore ide gde ima vise mesta
        else if (distance_from_bottom >= distance_from_top)
        {
            var _padding = popup.innerHeight() - popup.height();
            popup.css({height:distance_from_bottom - _padding});
            popup.offset({top:relativeToElTop + relativeToElHeight});
            refreshSimaPopupScrollContainer(popup);
        }        
        else
        {            
            var _padding = popup.innerHeight() - popup.height();
            popup.css({height:distance_from_top - _padding});
            popup.offset({top:relativeToElTop - popup.outerHeight(true)});
            refreshSimaPopupScrollContainer(popup);
        }                
    }
    
    function setTopPosition(popup)
    {
        var relativeToEl = popup.data('relativeToEl');
        var relativeToElTop = relativeToEl.offset().top;
        var relativeToElHeight = relativeToEl.outerHeight(true);
        var popupHeight = popup.outerHeight(true);
        var distance_from_top = relativeToElTop;
        var distance_from_bottom = $(window).height() - relativeToElTop - relativeToElHeight;

        //ako ima mesta popup ide gore
        if (distance_from_top >= popupHeight)
        {
            popup.offset({top: relativeToElTop - popupHeight});            
        }
        //ako ima mesta popup ide dole
        else if (distance_from_bottom >= popupHeight)
        {
            popup.offset({top: relativeToElTop + relativeToElHeight});
        }                
        //ako nema mesta ni dole ni gore ide gde ima vise mesta
        else if (distance_from_bottom >= distance_from_top)
        {                
            var _padding = popup.innerHeight() - popup.height();
            popup.css({height:distance_from_bottom - _padding});
            popup.offset({top:relativeToElTop + relativeToElHeight});
            refreshSimaPopupScrollContainer(popup);
        }        
        else
        {                
            var _padding = popup.innerHeight() - popup.height();
            popup.css({height:distance_from_top - _padding});
            popup.offset({top:relativeToElTop - popup.outerHeight(true)});
            refreshSimaPopupScrollContainer(popup);
        }
    }
    
    function setRightPosition(popup)
    {        
        var relativeToEl = popup.data('relativeToEl');
        var relativeToElLeft = relativeToEl.offset().left;
        var relativeToElWidth = relativeToEl.outerWidth(true);
        var popupWidth = popup.outerWidth(true);
        var distance_from_left = relativeToElLeft;
        var distance_from_right = $(window).width() - relativeToElLeft - relativeToElWidth;
        
        //ako ima mesta popup ide desno
        if (distance_from_right >= popupWidth)
        {
            popup.offset({left:relativeToElLeft + relativeToElWidth});
        }
        //ako ima mesta popup ide levo
        else if (distance_from_left >= popupWidth)
        {
            popup.offset({left:relativeToElLeft - popupWidth});
        }
        //ako nema mesta ni desno ni levo ide gde ima vise mesta
        else if (distance_from_right >= distance_from_left)
        {                
            var _padding = popup.innerWidth() - popup.width();
            popup.css({width:distance_from_right - _padding});
            popup.offset({left:relativeToElLeft + relativeToElWidth});            
        }        
        else
        {                
            var _padding = popup.innerWidth() - popup.width();
            popup.css({width:distance_from_left - _padding});
            popup.offset({left:relativeToElLeft - popup.outerWidth(true)});            
        }
    }
    
    function setLeftPosition(popup)
    {
        var relativeToEl = popup.data('relativeToEl');
        var relativeToElLeft = relativeToEl.offset().left;
        var relativeToElWidth = relativeToEl.outerWidth(true);
        var popupWidth = popup.outerWidth(true);
        var distance_from_left = relativeToElLeft;
        var distance_from_right = $(window).width() - relativeToElLeft - relativeToElWidth;
        
        //ako ima mesta popup ide levo
        if (distance_from_left >= popupWidth)
        {            
            popup.offset({left:relativeToElLeft - popupWidth});
        }
        //ako ima mesta popup ide desno
        else if (distance_from_right >= popupWidth)
        {
            popup.offset({left:relativeToElLeft + relativeToElWidth});
        }        
        //ako nema mesta ni desno ni levo ide gde ima vise mesta
        else if (distance_from_right >= distance_from_left)
        {                
            var _padding = popup.innerWidth() - popup.width();
            popup.css({width:distance_from_right - _padding});
            popup.offset({left:relativeToElLeft + relativeToElWidth});            
        }        
        else
        {                
            var _padding = popup.innerWidth() - popup.width();
            popup.css({width:distance_from_left - _padding});
            popup.offset({left:relativeToElLeft - popup.outerWidth(true)});            
        }
    }    
    
    function setLeftRightAlign(popup)
    {        
        var relativeToEl = popup.data('relativeToEl');
        var relativeToElLeft = relativeToEl.offset().left;
        var relativeToElWidth = relativeToEl.outerWidth(true);
        var popupWidth = popup.outerWidth(true);
        var distance_from_left = relativeToElLeft + relativeToElWidth;
        var distance_from_right = $(window).width() - relativeToElLeft;
        
        //ako ima mesta popup ide desno
        if (distance_from_right >= popupWidth)
        {
            popup.offset({left:relativeToElLeft});
        }
        //ako ima mesta popup ide levo
        else if (distance_from_left >= popupWidth)
        {
            popup.offset({left:relativeToElLeft - popupWidth + relativeToElWidth});
        }
        //ako nema mesta ni desno ni levo ide gde ima vise mesta
        else if (distance_from_right >= distance_from_left)
        {                
            var _padding = popup.innerWidth() - popup.width();
            popup.css({width:distance_from_right - _padding});
            popup.offset({left:relativeToElLeft});
        }        
        else
        {                
            var _padding = popup.innerWidth() - popup.width();
            popup.css({width:distance_from_left - _padding});
            popup.offset({left:relativeToElLeft - popup.outerWidth(true) + relativeToElWidth});            
        }
    }
    
    function setTopBottomAlign(popup)
    {
        var relativeToEl = popup.data('relativeToEl');
//        var appendToEl = popup.data('appendToEl');
        var relativeToElTop = relativeToEl.offset().top;
        var relativeToElHeight = relativeToEl.outerHeight(true);
        var popupHeight = popup.outerHeight(true);
        var distance_from_top = relativeToElTop;
        var distance_from_bottom = $(window).height() - relativeToElTop - relativeToElHeight;
//        var distance_from_bottom = appendToEl.height() - relativeToElTop - relativeToElHeight;

        //ako ima mesta popup ide dole
        if (distance_from_bottom >= popupHeight)
        {
            popup.offset({top: relativeToElTop});
        } 
        //ako ima mesta popup ide gore
        else if (distance_from_top >= popupHeight)
        {
            popup.offset({top: relativeToElTop + relativeToElHeight - popupHeight});
//            popup.offset({top: relativeToElTop + popupHeight});  
        }               
        //ako nema mesta ni dole ni gore ide gde ima vise mesta
        else if (distance_from_bottom >= distance_from_top)
        {
            popup.offset({top: relativeToElTop});
        }        
        else
        {
            popup.offset({top: relativeToElTop + relativeToElHeight - popupHeight}); 
        }
    }
    
    function refreshSimaPopupScrollContainer(popup)
    {
        if (popup.is(':visible'))
        {            
            var popup_scroll_container = popup.children('.sima-popup-container-for-scroll');
            var new_height = popup.outerHeight(true) - popup_scroll_container.position().top - 10;        
            popup_scroll_container.css({height:new_height});
        }
    }

    $.fn.simaPopup = function (methodOrOptions) {
        var ar = arguments;
        var i = 1;
        var ret = this;
        this.each(function () {
            i++;
            if (methods[methodOrOptions]) {
                var povr = methods[ methodOrOptions ].apply($(this), Array.prototype.slice.call(ar, 1));
                if (typeof povr !== 'undefined')
                {
                    ret = povr;
                }
                return;
            } else if (typeof methodOrOptions === 'object' || !methodOrOptions) {
                // Default to "init"
                return methods.init.apply($(this), ar);
            } else {
                $.error('Method ' + methodOrOptions + ' does not exist on jQuery.simaPopup');
            }
        });
        return ret;
    };

})(jQuery);