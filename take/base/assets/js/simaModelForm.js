
/* global sima */

function SIMAModelForm()
{
    this.init = function()
    {
        this.setFormElementsRevertEvents();
    };
    
    this.setFormElementsRevertEvents = function()
    {
        $('body').on('click', '.sima-model-form-row .revert-value', function(){
            $(this).parents('.sima-model-form-row:first').trigger('elementValueReverted');
        });

        setTextFieldRevertEvents();
        setTextAreaRevertEvents();
        setDropdownRevertEvents();
        setSearchFieldRevertEvents();
        setCheckBoxRevertEvents();
        setDatetimeFieldRevertEvents();
        setUnitMeasureFieldRevertEvents();
        setNumberFieldRevertEvents();
        setStatusRevertEvents();
        setListRevertEvents();
        setFileFieldRevertEvents();
        setCheckBoxListRevertEvents();
        setPhoneFieldRevertEvents();
        setSvpFieldRevertEvents();
        setBankAccountFieldRevertEvents();
    };
    
    function setTextFieldRevertEvents()
    {
        $('body').on('input change click','.sima-model-form-row[data-type="textField"]', function() {
            var textFieldObj = $(this).find('input:visible:first');
            var initialValue = textFieldObj.prop('defaultValue');
            var currValue = textFieldObj.val();
            
            checkIsValueChanged(initialValue, currValue, $(this).find('.revert-value'), initialValue);
        });
        $('body').on('elementValueReverted','.sima-model-form-row[data-type="textField"]', function() {
            var textFieldObj = $(this).find('input:visible:first');
            var initialValue = textFieldObj.prop('defaultValue');
            
            textFieldObj.val(initialValue).trigger('input');
        });
    }
    
    function setTextAreaRevertEvents()
    {
        $('body').on('input change click','.sima-model-form-row[data-type="textArea"]', function() {
            var textAreaObj = $(this).find('textarea:visible:first');
            var initialValue = textAreaObj.prop('defaultValue');
            var currValue = textAreaObj.val();
            
            checkIsValueChanged(initialValue, currValue, $(this).find('.revert-value'), initialValue);
        });
        $('body').on('elementValueReverted','.sima-model-form-row[data-type="textArea"]', function() {
            var textAreaObj = $(this).find('textarea:visible:first');
            var initialValue = textAreaObj.prop('defaultValue');
            
            textAreaObj.val(initialValue).trigger('input');
        });
    }

    function setDropdownRevertEvents()
    {
        $('body').on('change click','.sima-model-form-row[data-type="dropdown"]', function() {
            var dropdownObj = $(this).find('select:visible:first');
            var initialOption = getDropdownInitialOption(dropdownObj);
            var currValue = dropdownObj.val();
            
            var initialOptionValue = '';
            var initialOptionDisplayValue = '';
            if (initialOption !== null)
            {
                initialOptionValue = initialOption.val();
                initialOptionDisplayValue = initialOption.text();
            }
            
            checkIsValueChanged(initialOptionValue, currValue, $(this).find('.revert-value'), initialOptionDisplayValue);
        });
        $('body').on('elementValueReverted','.sima-model-form-row[data-type="dropdown"]', function() {
            var dropdownObj = $(this).find('select:visible:first');
            var initialOption = getDropdownInitialOption(dropdownObj);
            
            var initialOptionValue = (initialOption !== null) ? initialOption.val() : '';
            
            dropdownObj.val(initialOptionValue).trigger('change');
        });
    }
    
    function setSearchFieldRevertEvents()
    {
        $('body').on('onValue onEmpty','.sima-model-form-row[data-type="relation"]', function() {
            searchFieldRelationRevert($(this));
        });
        $('body').on('onValue onEmpty','.sima-model-form-row[data-type="searchField"]', function() {
            searchFieldRelationRevert($(this));
        });
        $('body').on('elementValueReverted','.sima-model-form-row[data-type="relation"]', function() {
            searchFieldRelationSetInitialValue($(this));
        });
        $('body').on('elementValueReverted','.sima-model-form-row[data-type="searchField"]', function() {
            searchFieldRelationSetInitialValue($(this));
        });
    }
    
    function searchFieldRelationRevert(obj){
            var searchFieldObj = obj.find('.sima-ui-sf:first');
            var initialValue = searchFieldObj.simaSearchField('getInitialValue');
            var currValue = searchFieldObj.simaSearchField('getValue');

            var initialValueId = (initialValue.id !== '') ? parseInt(initialValue.id) : -1;
            var currValueId = (currValue.id !== '') ? parseInt(currValue.id) : -1;

            checkIsValueChanged(initialValueId, currValueId, obj.find('.revert-value'), initialValue.display_name);  
    }
        
    function searchFieldRelationSetInitialValue(obj){
            var searchFieldObj = obj.find('.sima-ui-sf:first');
            var initialValue = searchFieldObj.simaSearchField('getInitialValue');

            searchFieldObj.simaSearchField('setValue', initialValue.id, initialValue.display_name);
    }

    function setCheckBoxRevertEvents()
    {
        $('body').on('input change click','.sima-model-form-row[data-type="checkBox"]', function() {
            var checkBoxObj = $(this).find('input:visible:first');
            if (!sima.isEmpty(checkBoxObj))
            {
                var initialValue = checkBoxObj.prop('defaultChecked');
                var currValue = checkBoxObj[0].checked;

                var initialValueDisplay = (initialValue === true) ? sima.translate('Checked') : sima.translate('NotChecked');

                checkIsValueChanged(initialValue, currValue, $(this).find('.revert-value'), initialValueDisplay);
            }
        });
        $('body').on('elementValueReverted','.sima-model-form-row[data-type="checkBox"]', function() {
            var checkBoxObj = $(this).find('input:visible:first');
            var initialValue = checkBoxObj.prop('defaultChecked');
            
            checkBoxObj.prop('checked', initialValue).trigger('input');
        });
    }
    
    function setDatetimeFieldRevertEvents()
    {
        $('body').on('input change click','.sima-model-form-row[data-type="datetimeField"]', function() {
            var datetimeFieldObj = $(this).find('input:visible:first');
            var initialValue = datetimeFieldObj.prop('defaultValue');
            var currValue = datetimeFieldObj.val();
            
            checkIsValueChanged(initialValue, currValue, $(this).find('.revert-value'), initialValue);
        });
        $('body').on('elementValueReverted','.sima-model-form-row[data-type="datetimeField"]', function() {
            var datetimeFieldObj = $(this).find('input:visible:first');
            var initialValue = datetimeFieldObj.prop('defaultValue');
            
            datetimeFieldObj.val(initialValue).trigger('input');
        });
    }
    
    function setUnitMeasureFieldRevertEvents()
    {
        $('body').on('input change click','.sima-model-form-row[data-type="unitMeasureField"]', function() {
            var revertObj = $(this).find('.revert-value');
            var textFieldObj = $(this).find('input:visible:first');
            var currencyFieldObj = $(this).find('.currency select:visible:first');
            var initialValueText = textFieldObj.prop('defaultValue');
            var initialCurrencyOption = getDropdownInitialOption(currencyFieldObj);
            var currValueText = textFieldObj.val();
            var currValueCurrency = currencyFieldObj.val();

            var initialCurrencyOptionValue = '';
            var initialCurrencyOptionDisplayValue = '';
            if (initialCurrencyOption !== null)
            {
                initialCurrencyOptionValue = initialCurrencyOption.val();
                initialCurrencyOptionDisplayValue = initialCurrencyOption.text();
            }
            
            if (initialValueText !== currValueText || initialCurrencyOptionValue !== currValueCurrency)
            {
                showRevertObj(revertObj, initialValueText+' '+initialCurrencyOptionDisplayValue);
            }
            else
            {
                hideRevertObj(revertObj);
            }
        });
        $('body').on('elementValueReverted','.sima-model-form-row[data-type="unitMeasureField"]', function() {
            var textFieldObj = $(this).find('input:visible:first');
            var currencyFieldObj = $(this).find('.currency select:visible:first');
            var initialValueText = textFieldObj.prop('defaultValue');
            var initialCurrencyOption = getDropdownInitialOption(currencyFieldObj);
            var initialCurrencyOptionValue = (initialCurrencyOption !== null) ? initialCurrencyOption.val() : '';
            
            currencyFieldObj.val(initialCurrencyOptionValue);
            textFieldObj.val(initialValueText).trigger('input');
        });
    }
    
    function setNumberFieldRevertEvents()
    {
        $('body').on('input change click','.sima-model-form-row[data-type="numberField"]', function() {
            var numberFieldObj = $(this).find('input:visible:first');
            var initialValue = numberFieldObj.prop('defaultValue');
            var currValue = numberFieldObj.val();

            checkIsValueChanged(initialValue, currValue, $(this).find('.revert-value'), initialValue);
        });
        $('body').on('elementValueReverted','.sima-model-form-row[data-type="numberField"]', function() {
            var numberFieldObj = $(this).find('input:visible:first');
            var initialValue = numberFieldObj.prop('defaultValue');
            
            numberFieldObj.val(initialValue).trigger('input');
        });
    }

    function setStatusRevertEvents()
    {
        $('body').on('formStatusChanged','.sima-model-form-row[data-type="status"]', function() {
            var statusInputObj = $(this).find('.status_input:first');
            var initialValue = sima.helpers.filterBoolean(statusInputObj.data('initial_value'));
            var currValue = sima.helpers.filterBoolean(statusInputObj.val());

            var initialValueDisplay = sima.translate('NotKnown');
            if (initialValue === true)
            {
                initialValueDisplay = sima.translate('Confirmed');
            }
            if (initialValue === false)
            {
                initialValueDisplay = sima.translate('NotConfirmed');
            }

            checkIsValueChanged(initialValue, currValue, $(this).find('.revert-value'), initialValueDisplay);
        });
        $('body').on('elementValueReverted','.sima-model-form-row[data-type="status"]', function() {
            var statusObj = $(this).find('.sima-model-form-status-field:first');
            var statusInputObj = $(this).find('.status_input:first');
            var initialValue = sima.helpers.filterBoolean(statusInputObj.data('initial_value'));
            
            sima.icons.setStatusIcon(statusObj, initialValue);
            statusInputObj.val(initialValue);

            statusObj.trigger('formStatusChanged');
        });
    }
    
    function setListRevertEvents()
    {
        $('body').on('itemsChanged','.sima-model-form-row[data-type="list"]', function() {
            var form_list_obj = $(this).find('.sima-model-form-search-list:first');
            var form_list_input_obj = form_list_obj.find('.mfslinput:first');
            
            var initial_form_list_value = form_list_input_obj.data('initial_value');
            if (typeof initial_form_list_value === 'string')
            {
                initial_form_list_value = JSON.parse(initial_form_list_value);
            }
            var initial_form_list_ids = initial_form_list_value.ids;
            if (sima.isEmpty(initial_form_list_ids))
            {
                initial_form_list_ids = [];
            }
            
            var curr_form_list_value = form_list_input_obj.val();
            if (typeof curr_form_list_value === 'string')
            {
                curr_form_list_value = JSON.parse(curr_form_list_value);
            }
            var curr_form_list_ids = curr_form_list_value.ids;
            if (sima.isEmpty(curr_form_list_ids))
            {
                curr_form_list_ids = [];
            }
            
            var are_equal = true;
            var initial_form_list_display = '';

            if (curr_form_list_ids.length !== initial_form_list_ids.length)
            {
                are_equal = false;
            }
            else
            {
                $.each(initial_form_list_ids, function(index, value) {

                    initial_form_list_display += value.display_name + ', ';
                    
                    var founded = false;
                    $.each(curr_form_list_ids, function(index1, value1) {
                        if (value.id === value1.id)
                        {
                            founded = true;
                        }
                    });
                    if (founded === false)
                    {
                        are_equal = false;
                    }
                });
                initial_form_list_display = initial_form_list_display.slice(0,-2);
            }
            
            var revert_obj = $(this).find('.revert-value');
            if (are_equal === true)
            {
                hideRevertObj(revert_obj);
            }
            else
            {
                showRevertObj(revert_obj, initial_form_list_display);
            }
        });
        $('body').on('elementValueReverted','.sima-model-form-row[data-type="list"]', function() {
            var formListObj = $(this).find('.sima-model-form-search-list:first');
            var formListInputObj = formListObj.find('.mfslinput:first');
            
            var initialFormListValue = formListInputObj.data('initial_value');
            if (typeof initialFormListValue === 'string')
            {
                initialFormListValue = jQuery.parseJSON(initialFormListValue);
            }
            var formListItems = {};
            if (typeof initialFormListValue.ids !== 'undefined')
            {
                formListItems = initialFormListValue.ids;
            }
            
            sima.model.searchFormListSetItems(formListObj, formListItems);
            
            formListObj.trigger('itemsChanged');
        });
    }
    
    function setFileFieldRevertEvents()
    {
//        localRow.find('.revert-value').on('click', function() {
////                        var initial_value = localRow.data('initial_file_data');
////                        console.log(typeof(initial_value));
////                        console.log(initial_value);
////                        if (initial_value) {
////                            console.log('not empty');
////                            localRow.find('input:text').val(initial_value);
////                        } else {
////                            console.log('empty');
////                            console.log(localRow.find('input:text'));
////                            localRow.find('input:text').val("");
////                            console.log(localRow.find('input:text'));
////                        }
////                        
////                        localRow.find('.revert-value:first').removeClass('_changed');
////                        localRow.removeClass('_data-changed');
////                    });
//                    
//                    var val = localRow.find('input:text').val();
//                    
//                    localRow.on('UPLOAD_SUCCESS', function(event, data) {
//                        
//                        var new_val = localRow.find('input:text').val();
//                        console.log(new_val);
//                        if (val !== new_val) {
//                            localRow.data('initial_file_data', val);
//                            var title = setChangesTitle(val);
//                            localRow.find('.revert-value:first').attr('title', title);
//                            localRow.find('.revert-value:first').addClass('_changed');
//                            localRow.addClass('_data-changed');
//                        } else {
//                            localRow.find('.revert-value:first').attr('title', '');
//                            localRow.find('.revert-value:first').removeClass('_changed');
//                            localRow.removeClass('_data-changed');
//                        }
//                    });
    }
    
    function setCheckBoxListRevertEvents()
    {
        $('body').on('checkedItemsChanged','.sima-model-form-row[data-type="checkBoxList"]', function() {
            var checkBoxListObj = $(this).find('.sima-form-check-box-list:first');
            var checkBoxListInputObj = checkBoxListObj.find('.mfslinput:first');
            
            var initialCheckBoxListValue = checkBoxListInputObj.data('initial_value');
            var initialCheckBoxListValueString = (typeof initialCheckBoxListValue !== 'string') ? JSON.stringify(initialCheckBoxListValue) : initialCheckBoxListValue;
            var currCheckBoxListValue = checkBoxListInputObj.val();
            
            var initialCheckBoxListDisplay = '';
            $.each(initialCheckBoxListValue, function(index, value) {
                if (sima.helpers.filterBoolean(value.isChecked) && !sima.isEmpty(value.aditional_params))
                {
                    var additionalParams = value.aditional_params;
                    if (!sima.isEmpty(additionalParams.display_name))
                    {
                        initialCheckBoxListDisplay += additionalParams.display_name + ', ';
                    }
                }
            });
            initialCheckBoxListDisplay = initialCheckBoxListDisplay.slice(0,-2);
            
            checkIsValueChanged(initialCheckBoxListValueString, currCheckBoxListValue, $(this).find('.revert-value'), initialCheckBoxListDisplay);
        });
        $('body').on('elementValueReverted','.sima-model-form-row[data-type="checkBoxList"]', function() {
            var checkBoxListObj = $(this).find('.sima-form-check-box-list:first');
            var checkBoxListInputObj = checkBoxListObj.find('.mfslinput:first');
            
            var initialCheckBoxListValue = checkBoxListInputObj.data('initial_value');
            var initialCheckBoxListValueString = initialCheckBoxListValue;
            if (typeof initialCheckBoxListValue !== 'string')
            {
                initialCheckBoxListValueString = JSON.stringify(initialCheckBoxListValue);
            }
            else
            {
                initialCheckBoxListValue = jQuery.parseJSON(initialCheckBoxListValue);
            }
            
            $.each(initialCheckBoxListValue, function(index, value) {
                var item = checkBoxListObj.find('.sima-form-check-box-list-item input[value="'+value.value+'"]');
                item.prop('checked', sima.helpers.filterBoolean(value.isChecked));
            });
            
            checkBoxListInputObj.val(initialCheckBoxListValueString);
            checkBoxListObj.trigger('checkedItemsChanged');
        });
    }
    
    function setSvpFieldRevertEvents()
    {
        $('body').on('input change click','.sima-model-form-row[data-type="svpField"]', function() {
            var type_of_payer = $(this).find('.type-of-payer:first');
            var employment_code = $(this).find('.employment-code:first');
            var basic_income_type = $(this).find('.basic-income-type:first');
            var benefit = $(this).find('.benefit:first');
            var benefited_work_experience = $(this).find('.benefited-work-experience:first');
            
            var initial_value_type_of_payer = type_of_payer.prop('defaultValue');
            var initial_employment_code_option = getDropdownInitialOption(employment_code);
            var initial_value_employment_code = (initial_employment_code_option !== null) ? initial_employment_code_option.val() : '';
            var initial_value_basic_income_type = basic_income_type.prop('defaultValue');
            var initial_benefit_option = getDropdownInitialOption(benefit);
            var initial_value_benefit = (initial_benefit_option !== null) ? initial_benefit_option.val() : '';
            var initial_benefited_work_experience_option = getDropdownInitialOption(benefited_work_experience);
            var initial_value_benefited_work_experience = (initial_benefited_work_experience_option !== null) ? initial_benefited_work_experience_option.val() : '';
            
            var curr_value_type_of_payer = type_of_payer.val();
            var curr_value_employment_code = employment_code.val();
            var curr_value_basic_income_type = basic_income_type.val();
            var curr_value_benefit = benefit.val();
            var curr_value_benefited_work_experience = benefited_work_experience.val();

             if (
                    initial_value_type_of_payer !== curr_value_type_of_payer ||
                    initial_value_employment_code !== curr_value_employment_code ||
                    initial_value_basic_income_type !== curr_value_basic_income_type ||
                    initial_value_benefit !== curr_value_benefit ||
                    initial_value_benefited_work_experience !== curr_value_benefited_work_experience
                )
            {
                showRevertObj($(this).find('.revert-value'), '');
            }
            else
            {
                hideRevertObj($(this).find('.revert-value'));
            }
        });
        $('body').on('elementValueReverted','.sima-model-form-row[data-type="svpField"]', function() {
            var type_of_payer = $(this).find('.type-of-payer:first');
            var employment_code = $(this).find('.employment-code:first');
            var basic_income_type = $(this).find('.basic-income-type:first');
            var benefit = $(this).find('.benefit:first');
            var benefited_work_experience = $(this).find('.benefited-work-experience:first');
            
            var initial_value_type_of_payer = type_of_payer.prop('defaultValue');
            var initial_employment_code_option = getDropdownInitialOption(employment_code);
            var initial_value_employment_code = (initial_employment_code_option !== null) ? initial_employment_code_option.val() : '';
            var initial_value_basic_income_type = basic_income_type.prop('defaultValue');
            var initial_benefit_option = getDropdownInitialOption(benefit);
            var initial_value_benefit = (initial_benefit_option !== null) ? initial_benefit_option.val() : '';
            var initial_benefited_work_experience_option = getDropdownInitialOption(benefited_work_experience);
            var initial_value_benefited_work_experience = (initial_benefited_work_experience_option !== null) ? initial_benefited_work_experience_option.val() : '';
            
            type_of_payer.val(initial_value_type_of_payer).trigger('input');
            employment_code.val(initial_value_employment_code).trigger('change');
            basic_income_type.val(initial_value_basic_income_type).trigger('input');
            benefit.val(initial_value_benefit).trigger('change');
            benefited_work_experience.val(initial_value_benefited_work_experience).trigger('change');      
        });
    }
    
    function setPhoneFieldRevertEvents()
    {
        $('body').on('onValue onEmpty input change click','.sima-model-form-row[data-type="phoneField"]', function() {
            var phone_field     = $(this).find('.sima-phone-field');
            var country_number  = $(this).find('.sima-ui-sf:first');
            var provider_number = $(this).find('.provider-number:first');
            var phone_number    = $(this).find('.phone-number:first');

            var initial_country_number  = country_number.simaSearchField('getInitialValue');
            var initial_provider_number = provider_number.prop('defaultValue');
            var initial_phone_number    = phone_number.prop('defaultValue');

            var curr_value_country_number  = country_number.simaSearchField('getValue');
            var curr_value_provider_number = provider_number.val();
            var curr_value_phone_number    = phone_field.simaPhoneField('getPhoneNumberOnly');
            
             if (
                    initial_country_number.id  !== curr_value_country_number.id ||
                    initial_provider_number !== curr_value_provider_number ||
                    initial_phone_number    !== curr_value_phone_number 
                )
            {
                showRevertObj($(this).find('.revert-value'));
            }
            else
            {
                hideRevertObj($(this).find('.revert-value'));
            }
        });
        $('body').on('elementValueReverted','.sima-model-form-row[data-type="phoneField"]', function() {
            var country_number  = $(this).find('.sima-ui-sf:first');
            var provider_number = $(this).find('.provider-number:first');
            var phone_number    = $(this).find('.phone-number:first');
           
            var initial_country_number  = country_number.simaSearchField('getInitialValue');
            var initial_provider_number = provider_number.prop('defaultValue');
            var initial_phone_number    = phone_number.prop('defaultValue');
            
            country_number.simaSearchField('setValue', initial_country_number.id, initial_country_number.display_name);
            provider_number.val(initial_provider_number).trigger('input');
            phone_number.val(initial_phone_number).trigger('input');           
        });
    }
    
    function setBankAccountFieldRevertEvents()
    {
        $('body').on('input change','.sima-model-form-row[data-type="bankAccountField"]', function() {
            var bank_account_field = $(this).find('.bank-account-number-field:first');
            var initial_bank_account_field = bank_account_field.prop('defaultValue');
            var curr_bank_account_field = bank_account_field.val();           
            if (initial_bank_account_field !== curr_bank_account_field)
            {
                showRevertObj($(this).find('.revert-value'));
            }
            else
            {
                hideRevertObj($(this).find('.revert-value'));
            }
        });
        $('body').on('elementValueReverted','.sima-model-form-row[data-type="bankAccountField"]', function() {
            var bank_account_field = $(this).find('.bank-account-number-field:first');
            var initial_bank_account_field = bank_account_field.prop('defaultValue');
            bank_account_field.val(initial_bank_account_field).trigger('input').trigger('focusout');
        });
    }
    
    function checkIsValueChanged(initialValue, currValue, revertObj, revertObjTitle)
    {
        if (initialValue !== currValue)
        {
            showRevertObj(revertObj, revertObjTitle);
        }
        else
        {
            hideRevertObj(revertObj);
        }
    }
    
    function showRevertObj(revertObj, revertObjTitle)
    {
        revertObj.attr('title', revertObjTitle).css('display', 'inline-block').show();
    }
    
    function hideRevertObj(revertObj)
    {
        revertObj.attr('title', '').hide();
    }
    
    function getDropdownInitialOption(dropdownObj)
    {
        var initialOption = null;
        
        dropdownObj.find('option').each(function () {
            if (this.defaultSelected) 
            {
                initialOption = $(this);
                return false;
            }
        });
        
        if (initialOption === null && !sima.isEmpty(dropdownObj.find('option:first-child')))
        {
            return dropdownObj.find('option:first-child');
        }
        
        return initialOption;
    }
}
