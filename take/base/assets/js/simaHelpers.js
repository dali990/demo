/* global sima */

function SIMAHelpers()
{
    this.init = function()
    {
        
    };
    
    this.isTrue = function(value)
    {
        if (value === 1 || value === '1' || value === true || value === 'true')
        {
            return true;
        }
        
        return false;
    };
    
    this.isFalse = function(value)
    {
        if (value === 0 || value === '0' || value === false || value === 'false')
        {
            return true;
        }
        
        return false;
    };
    
    this.filterBoolean = function(value)
    {
        if (sima.helpers.isTrue(value))
        {
            return true;
        }
        else if (sima.helpers.isFalse(value))
        {
            return false;
        }
        
        return null;
    };
}
