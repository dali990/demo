
function SIMADate(date_format, datetime_format)
{
    this.getByParts = function(dayNumber,monthNumber,year)
    {
        return dayNumber+'.'+monthNumber+'.'+year+'.';
    };
    
    this.format = function(date, is_time = false)
    {
        if (!(date instanceof Date))
        {
            date = new Date(date);
        }

        var dd = date.getDate();
        var mm = date.getMonth() + 1; //January is 0!
        var yyyy = date.getFullYear();

        if (dd < 10) 
        {
            dd = '0' + dd;
        } 
        if (mm < 10) 
        {
            mm = '0' + mm;
        }
        
        var date_string = dd + '.' + mm + '.' + yyyy + '.';
        
        if (is_time)
        {
            var hours = date.getHours();
            if (hours < 10)
            {
                hours = '0' + hours;
            }
            var minutes = date.getMinutes();
            if (minutes < 10)
            {
                minutes = '0' + minutes;
            }
            var seconds = date.getSeconds();
            if (seconds < 10)
            {
                seconds = '0' + seconds;
            }
            date_string += (' ' + hours + ':' + minutes + ':' + seconds);
        }

        return date_string;
    };
}

