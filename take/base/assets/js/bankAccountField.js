  (function($) {
          var methods = {

              init: function() {
                  var _this = $(this);
                  var schema="000-0-00";

                     if(_this.val()==='0' || _this.val()===''){
                          _this.val(schema);
                          _this.prop('defaultValue', schema);
                      }
                  _this.attr("pattern", "[0-9]{3}-[0-9]{1,13}-[0-9]{2}");
                  _this.data('schema',schema);
                  _this.data('back_space',false);
                  _this.data('isCtrl', false);
                  //validate charapters  by event handlesrs && makeing 0 while you delete a control digits
                  _this.on('keydown', keydownEvent);
                  _this.on('keyup', keyupEvent);
                  //this is for converting input( Contains a main logic)
                  _this.bind('input', liveTypeing);
                  //set cursor position
                  _this.on("click", {_this: _this }, function() {
                      // work just 1st time when you click on input field
                      if (_this.val() === schema)
                        _this.get(0).setSelectionRange(0, 0);
                  });
                 //this blur function check  is input valid by regex and format it
                  _this.on('blur', fromNumber_to_Bank_Acc);
              },
              reset: function() {
                  var _this = $(this);
                  _this.val(_this.data('schema'));
                  _this.trigger("input");
              },
              isEmpty: function() {
                  var _this = $(this);
                  return _this.val() === _this.data('schema');
              }
          };
        //this part of code clean invalid input and works only  on blur event
          function fromNumber_to_Bank_Acc() {
              var _this = $(this);
              var schema=_this.data('schema');
              var value = _this.val();
              value = value.replace(/[\D]/g, "");
              var len=value.length;
         
              var end_input;

              if (len>=6  && len<=18)//&& value!="000-0-00"
              {
                  value = value.replace(/[-]/g, "");
                  //length of value
                  var n = value.length;
                  var firstPart = value.slice(0, 3);
                  var thirdPart = value.slice(-2);
                  var secondPart = value.slice(3, n - 2);
                  var str=firstPart +"-"+ secondPart  +"-"+ thirdPart;
                  if(str==='--')
                    end_input=schema;
                  else
                    end_input=str;
              }
              else
              {
                end_input=schema;
              }
              _this.val(end_input);
              _this.get(0).setSelectionRange(0,0);

          }

          function keydownEvent(event) {
              
              var _this = $(this);
              var mask = _this.val();
              //length of value
              var n = mask.length;
              var kursor_position = _this.prop("selectionStart");
              var event_code = event.which;

              if(event_code===8){
                _this.data('back_space',true);
              }else{
                _this.data('back_space',false);
              }
              //this part of code make magic when you press backspace to skip - charapter
              // FIRST 3 DIGITS
              var it_happened=first_3_digits(mask,kursor_position,event_code,_this);
              if(it_happened===false)
                return false;
              ///// last 2 digits with last 2 digits
              it_happened=last_2_digits(mask,kursor_position,event_code,_this,n);
              if(it_happened===false)
                return false;
              // fixing midle digit
              it_happened=mid_fix_digits(mask,kursor_position,event_code,_this);
              if(it_happened===false)
                return false;

              // when you press dash or minus , this park of code will automatic move you to last 2 controll numbers
              if (event_code === 109 || event_code === 173)
              {
                  if(kursor_position<4){
                      _this.get(0).setSelectionRange(4,4);
                  }else{
                      _this.get(0).setSelectionRange(n - 2, n - 2);
                  }

                  return false;
              }
              //109 173 dash & minus
              if (kursor_position === n - 2 && event_code === 8)
              {
                  _this.get(0).setSelectionRange(kursor_position - 1, kursor_position - 1);
                  return true;
              }
              if (event.which === 17) //ctrl
              {
                  _this.data('isCtrl', true);
              }
              //this make to skip last dash when i try to backspace
              if (kursor_position === 17 && mask[17] === '-' && event_code !== 8 && event_code !== 37 && event.event_code !== 39)
              {
                  _this.get(0).setSelectionRange(kursor_position + 1, kursor_position + 1);
                  return true;
              }
              //checkis charapter valid
              else if (is_valid_charachter(_this, event_code))
              {
                  return false;
              }
              var mask = mask.split("-");
              
            if(mask.length!=1)
            { 
                 drugi = mask[1];
                if (drugi.length === 14)
                {
                    return false;
                }
            }
          }
          function keyupEvent(event) {
              var _this = $(this);
              var mask = _this.val();
              var n = mask.length;
              var schema=_this.data().schema;

               if(n<schema.length){
                  _this.val(schema);
                  _this.get(0).setSelectionRange(0, 0);
              }
              if (event.which === 9) //tab
              {
                  return true;
              }
              // cursor position
              var kursor_position = _this.prop("selectionStart");
              //consoole.log(kursor_position);

              // This aloowed you to skip last dash
              if (kursor_position === 17 && mask[n - 2] === '-')
              {
                  _this.get(0).setSelectionRange(kursor_position + 1, kursor_position + 1);
                  return false;
              }
              //enable ctrl
              if (event.which === 17) //ctrl
              {
                  _this.data('isCtrl', false);
              }
          }
          // for my this is logic
          function is_valid_charachter(_this, charCode) {
              if (
                  (charCode !== 173 && charCode !== 37 && charCode !== 39 && charCode !== 109 && charCode > 31 &&
                      (charCode < 96 || charCode > 105) && (charCode < 48 || charCode > 57) &&
                      !(_this.data('isCtrl') === true &&
                          (charCode === 65 || charCode === 67 || charCode === 86 || charCode === 88)))
              ) {
                  return true;
              } else {
                  return false;
              }
          }
          //ovde pisem funkcije
          function liveTypeing()
          {
              var _this = $(this);
              var schema=_this.data('schema');

              var kursor_position = _this.prop("selectionStart");
              var maska = _this.val();


              if(_this.data('back_space')===false)
              {
                // boundary conditions
                if (maska === "")
                {
                    _this.val(schema);
                    _this.get(0).setSelectionRange(0, 0);
                    return false;
                } else if (maska.length === 1)
                {
                    var take = $(this).val();
                    _this.val(take + "00-0-00");
                    _this.get(0).setSelectionRange(1, 1);
                    return false;
                }
                //split current string into 3 arrays
                var niz = maska.split("-");
                // boundary conditions
                if(niz.length<3){
                  return false;
                }
                var prvi = niz[0].split("");
                var drugi = niz[1].split("");
                var treci = niz[2].split("");

                //logic main
                if (drugi.length === 14)
                {
                    drugi.splice(kursor_position - 4, 1);
                }
                else if(drugi.length === 0)
                {
                  drugi=["0"];
                }
                else {
                    if (kursor_position <= prvi.length)
                    {
                        if (prvi.length === 4)
                            prvi.splice(kursor_position, 1);
                        if (kursor_position === 3)
                            kursor_position = 4;
                    } else if (kursor_position >= 4 && drugi.length !== 13)
                    {
                        if (!(kursor_position - 4 === drugi.length))
                            drugi.splice(kursor_position - 4, 1);
                    } else if (kursor_position <= maska.length && kursor_position >= maska.length - 2)
                    {
                        if (treci.length <= 3)
                            treci.splice(kursor_position - 18, 1);
                    }
                    //  eleminate excess elements
                    if (prvi.length === 4)
                    {
                        prvi.pop();
                    }
                    if (treci.length === 3 && drugi.length<=14)
                    {
                        treci.splice(kursor_position - prvi.length + drugi.length + 2, 1);
                        treci.pop();
                    }
                }
                //from 3 arrays we again make input and convert it to string
                maska = prvi.concat(["-"], drugi);
                maska = maska.concat(["-"], treci);
                maska = maska.join("");
                //printing on input field
                _this.val(maska);
                _this.get(0).setSelectionRange(kursor_position, kursor_position);                  
              }

          }
          // functions form keydown
          function first_3_digits(mask,kursor_position,event_code,_this)
          {
                            if (mask[3] === '-' && (kursor_position === 4) && event_code === 8)
                            {
                                if (kursor_position === 4 && event_code === 8) {
                                    mask = replaceAt(mask,2, "0");
                                    _this.val(mask);
                                    _this.get(0).setSelectionRange(kursor_position - 2, kursor_position - 2);
                                    return false;
                                }
                            } else if (mask[1] !== undefined && kursor_position === 2 && event_code === 8)
                            {
                                mask = replaceAt(mask,1, "0");
                                _this.val(mask);
                                _this.get(0).setSelectionRange(kursor_position - 1, kursor_position - 1);
                                return false;
                            } else if (mask[0] !== undefined && kursor_position === 1 && event_code === 8)
                            {
                                mask = replaceAt(mask,0, "0");
                                _this.val(mask);
                                _this.get(0).setSelectionRange(kursor_position - 1, kursor_position - 1);
                                return false;
                            } else if (mask[0] !== undefined && kursor_position === 3 && event_code === 8)
                            {
                                mask = replaceAt(mask,2, "0");
                                _this.val(mask);
                                _this.get(0).setSelectionRange(kursor_position - 1, kursor_position - 1);
                                return false;
                            }
                            return true;
          }
          function last_2_digits(mask,kursor_position,event_code,_this,n)
          {
                             if (mask[n - 2] !== undefined && kursor_position === n && event_code === 8)
                            {
                                mask = replaceAt(mask,n - 1, "0");
                                _this.val(mask);
                                _this.get(0).setSelectionRange(kursor_position - 1, kursor_position - 1);
                                return false;
                            } else if (mask[n - 3] !== undefined && kursor_position === n - 1 && event_code === 8)
                            {
                                mask = replaceAt(mask,n - 2, "0");
                                _this.val(mask);
                                _this.get(0).setSelectionRange(kursor_position - 1, kursor_position - 1);
                                return false;
                            }

                            return true;
          }
          function mid_fix_digits(mask,kursor_position,event_code,_this)
          {
                             if (mask[4] !== undefined && (kursor_position === 5) && event_code === 8)
                            {
                                mask = replaceAt(mask,4, "0");
                                _this.val(mask);
                                _this.get(0).setSelectionRange(kursor_position - 1, kursor_position - 1);
                                return false;
                              }
                              //else if(mask[4]!== undefined && (kursor_position>=6) && )
                              return true;
          }
          // function for replaceing charapter in string
          // String.prototype.replaceAt = function(index, character) {
          //     return this.substr(0, index) + character + this.substr(index + character.length);
          // }
          function replaceAt(mask, index, character) {
              return mask.substr(0, index) + character + mask.substr(index + character.length);
          }
          //Jquey Schema
          $.fn.bankAccountField = function(methodOrOptions) {
              var ar = arguments;
              var i = 1;
              var ret = this;
              this.each(function() {
                  i++;
                  if (methods[methodOrOptions]) {
                      var povr = methods[methodOrOptions].apply($(this), Array.prototype.slice.call(ar, 1));
                      if (typeof povr !== 'undefined') {
                          ret = povr;
                      }
                      return;
                  } else if (typeof methodOrOptions === 'object' || !methodOrOptions) {
                      // Default to "init"
                      return methods.init.apply($(this), ar);
                  } else {
                      $.error('Method ' + methodOrOptions + ' does not exist on jQuery.bankAccountField');
                  }
              });
              return ret;
          };

      })(jQuery);
