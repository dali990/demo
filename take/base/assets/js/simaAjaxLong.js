/* sima */

/* global sima */

/**
 * Function for using ajax
 * @returns 
 */
function SIMAAjaxLong()
{
    var request = [];
    var request_params = [];
    this.start = function(action, params)
    {                
        if (sima.isEmpty(params.loadingData))
        {
            params.loadingData = {};
        }
        
        if (typeof params.data === 'undefined')
        {
            params.data = {};
        }
        if (typeof params.failure_function === 'undefined')
        {
            params.failure_function = function(response){
                sima.dialog.openWarn(typeof response === 'object' ? JSON.stringify(response) : response);
            };
        }
        var group = (typeof params.group !== 'undefined') ? params.group : null;
        if (group !== null && typeof request[group] !== 'undefined')
        {
            request[group].dispatchEvent(new CustomEvent('cancel'));
            endEventSource(request[group], request_params[group]);            
        }
                
        if (sima.isEmpty(params.loadingCircleDelay) || typeof(params.loadingCircleDelay) !== 'number')
        {
            params.loadingCircleDelay = 500;
        }
        saveEventData(action, params, group);       
    };
    
    this.endEventSource = function(eventSource)
    {       
        endEventSource(eventSource);
    };
    
    function saveEventData(action, params, group)
    {
        var last_package = true;
        var data_to_send = sima.clone(params.data);
        if(typeof params.split_data !== 'undefined')
        {
            var client_data = params.data.client_data;            
            if (client_data.length <= params.split_data)
            {
                last_package = true;
            }
            else
            {
                last_package = false;                
            }
            data_to_send.client_data = client_data.splice(0, params.split_data);
            params.data.client_data = client_data;
        }
        
        if (!sima.isEmpty(params.showProgressBar) && sima.isEmpty(params.progressBarData))
        {
            params.progressBarData = startProgressBar(params);          
        }
        
        sima.ajax.get('base/events/saveEventData', { 
            data: data_to_send,
            showLoading: false,        
            async: true,         
            loadingCircleDelay: params.loadingCircleDelay,
            group: group,
            success_function: function(response) {
                if(last_package === false)
                {
                    params.data.event_id = response.event_id;
                    saveEventData(action, params, group);
                }
                else
                {
                    var eventSource = new EventSource("index.php?r="+action+"&event_id="+response.event_id+"&YIISIMASESSION="+sima.getTabSessionID()+"&is_ajax_long=TRUE");                
                    request[group] = eventSource;
                    request_params[group] = params;
                    initiateSource(eventSource, params);
                    if (typeof params.afterStart !== 'undefined')
                    {
                        var afterStartFunc = params.afterStart;
                        afterStartFunc({eventSource: eventSource});
                    }
                }
            }
        });
    }
    
    function setProgresBarValueByData(progressBarData, percent)
    {
        $.each(progressBarData.progressBarObjs, function(index, progressBar){
            if (
                typeof percent !== 'undefined' && progressBar !== null && sima.isElementInDom(progressBar)
                //&& 
                //progressBar.progressbar("value") < percent
                // MilosJ: za sada zakomentarisano jer postoje situacije kada se procenti restartuju
            )
            {
                progressBar.progressbar("value", percent);
            }
        });
    }
    
    function initiateSource(eventSource, params)
    {
        var progressBarData = null;
        if (!sima.isEmpty(params.showProgressBar))
        {
            progressBarData = params.progressBarData;
        }
        
        eventSource.addEventListener('update', function(e){
            try
            {
                var data = JSON.parse(e.data);
                if(!sima.isEmpty(progressBarData))
                {
                    // mozda nije jos inicijalizovan progressbar obj pa probati za 1s
                    if(progressBarData.progressBarObjs !== null && $.isArray(progressBarData.progressBarObjs))
                    {
                        setProgresBarValueByData(progressBarData, (data.percent | 0));
                    }
                    else
                    {
                        setTimeout(function(){ 
                            if(progressBarData.progressBarObjs !== null && $.isArray(progressBarData.progressBarObjs))
                            {
                                setProgresBarValueByData(progressBarData, (data.percent | 0));
                            }
                        }, 1000); // 1s
                    }
                }
                if (typeof params.onUpdate !== 'undefined')
                {
                    var onUpdateFunc = params.onUpdate;
                    onUpdateFunc(data);
                }
            }
            catch(error)
            {
                var message = 'Greska:</br>'+error.message+'</br>'+JSON.stringify(error);
                
                /*
                 * MilosJ:
                 * zakomentarisano prijavljivanje greske jer je moguce da bude i u redovnom radu
                 * otvorim model u dialogu, dok se on ucitava spustim u tab
                 * ovde se "abort"-uju sva dosadasnja izvrsavanja i moguce da ajax pukne
                 */
                console.log('simaAjaxLong update - '+message);
//                sima.addError('simaAjaxLong update', message);
            }
        }, false);
        eventSource.addEventListener('end', function(e){
            var data = JSON.parse(e.data);
            endEventSource(eventSource, params);
            if (typeof params.onEnd !== 'undefined')
            {
                var onEndFunc = params.onEnd;
                onEndFunc(data);
            }
            if (typeof(data.actions)!=='undefined' && !jQuery.isEmptyObject(data.actions))
            {
                for(var action in data.actions)
                {
                    var _action = data.actions[action];
                    var _func = _action[0];
                    var _params = _action[1];
                    var methods = _func.split(".");
                    var result = window;
                    for(var i in methods) {
                        result = result[methods[i]];
                    }
                    result.apply(this,_params);
                }
            }
            if (typeof(data.notes)!=='undefined' && !jQuery.isEmptyObject(data.notes))
            {
                for(var n in data.notes)
                {
                    sima.dialog.openWarn(data.notes[n].text);
                }
            }

            if (typeof(data.update_models) !== 'undefined')
            {
                sima.model.updateViews(data.update_models);
            }
            
            if (typeof(data.iframes)!=='undefined')
            {
                for(var n in data.iframes)
                {
                    var iframe = document.getElementById(data.iframes[n].frame_id);
                    var doc = iframe.contentWindow.document;
                    doc.open();
                    doc.write(data.iframes[n].content);
                    doc.close();
                    //na chromu load event ne radi
                    if (sima.isChromeBrowser())
                    {
                        setTimeout(function() {
                            iframe.height = doc.body.scrollHeight + 'px';
                        }, 50);
                    }
                    else
                    {
                        $(iframe).load(function() {
                            iframe.height = doc.body.scrollHeight + 'px';
                        });
                    }
                }
            }

            $('body').append(data.processOutput);
        }, false);
        eventSource.addEventListener('cancel', function(e){
            if (typeof params.onCancel !== 'undefined')
            {
                var onCancelFunc = params.onCancel;                
                onCancelFunc();
            }
        }, false);
        eventSource.addEventListener('failure', function(e){
            var data = null;
            try{
                data = JSON.parse(e.data);
            }catch(ex){
                data = 'sima.ajaxLong - failure</br>'+ex;
            }
            
            endEventSource(eventSource, params);
            
            var message = data;
            if(typeof data.message !== 'undefined')
            {
                message = data.message;
            }
            
            params.failure_function(message);
        }, false);
        eventSource.addEventListener('fatal_failure', function(e){
            if (sima.isF5Pressed === false)
            {
                endEventSource(eventSource, params);

                var parsed_data = JSON.parse(e.data); 

                sima.errorReport.addErrorDesc(parsed_data.error_id, parsed_data.message);
                
                if (typeof params.fatal_failure_function !== 'undefined')
                {
                    var fatal_failure_func = params.fatal_failure_function;                
                    fatal_failure_func();
                }
            }
        }, false);
        eventSource.addEventListener('error', function(e){
            if (sima.isF5Pressed === false)
            {
                endEventSource(eventSource, params);
                
                sima.errorReport.addError({
                    action: e.target.url,
                    message: 'Undefined event source error',
                    post_params: params.data
                });
                
                if (typeof params.error_function !== 'undefined')
                {
                    var error_func = params.error_function;                
                    error_func();
                }
            }
        }, false);
    }
    
    function startProgressBar(params)
    {
        var progressBarWrapper = $("<div class='sima-ajaxLong-progressBar-wrapper'></div>");
        var progressBar = sima.startProgressBar(progressBarWrapper, params.loadingCircleDelay);
        params.loadingData = sima.showLoading(params.showProgressBar, params.loadingCircleDelay);
        params.showProgressBar.after(progressBarWrapper);
        
        var new_z = 'auto';
        if(params.showProgressBar.zIndex() !== 'auto')
        {
            new_z = params.showProgressBar.zIndex();
            new_z += 10;
        }
        progressBarWrapper.zIndex(new_z);
        
        return progressBar;
    }
    
    function endProgressBar(progressBarData, params)
    {
        if (!sima.isEmpty(progressBarData) && typeof params !== 'undefined' && !sima.isEmpty(params.showProgressBar))
        {
            var obj = params.showProgressBar;
            sima.endProgressBar(params.showProgressBar, progressBarData);
            sima.hideLoading(obj, params.loadingData);
            obj.siblings('.sima-ajaxLong-progressBar-wrapper:first').remove();
        }
    }
    
    function endEventSource(eventSource, params)
    {
        var group = ((typeof params !== 'undefined' && typeof params.group !== 'undefined')) ? params.group : null;
        delete request[group];
        eventSource.close();        
        endProgressBar(params.progressBarData, params);
    }
}

