
/* global sima */

function SIMAModelMerge() 
{
    this.mergeWith = function(model_name, model_displayname, model_id)
    {
        var select_filter = null;
        var last_active_functional_element = sima.getLastActiveFunctionalElement();
        if(sima.elementHasPlugin(last_active_functional_element, 'simaGuiTable'))
        {
            select_filter = last_active_functional_element.simaGuiTable('getAllFilters')[model_name];
        }
        
        sima.model.choose(model_name, function(chosen_models){
            const chosen_model = chosen_models[0];
            const chosen_model_id = chosen_model.id;
            const chosen_model_display_name = chosen_model.display_name;
            if(chosen_model_id == model_id)
            {
                sima.dialog.openWarn('ne mozete izabrati isti objekat');
                return;
            }
            
            sima.dialog.openYesNo('Da li ste sigurni da zelite da spojite "'+model_displayname+'" sa "'+chosen_model_display_name+'"', function(){
                sima.dialog.close();
                sima.ajaxLong.start('base/modelsMerge/perform', {
                    showProgressBar: $('body'),
                    data:{
                        model_name: model_name,
                        model_id1: chosen_model_id,
                        model_id2: model_id
                    },
                    onEnd: function(response) {
                        sima.dialog.openInfo(response.message);
                    }
                });
            });
        }, {
            view: 'guiTable',
            select_filter: select_filter,
            fixed_filter: {
                0: 'NOT', 
                1: {
                    ids: model_id
                }
            }
        });
    };
}


