function SIMAGuiTable(t,u,model) 
{
	var _table = t;
	var url = u;
	
	var search_text = null;
//	var filter = null;
	
	var selected = null;
	this.getSelected = function(){return selected;};
	
	var multiselect = false;
	var add_button = {};
	var selectRow=true; //selektovanje redova je dozvoljeno, tj. ukljuceno
	
        var _default_filter = {};
        this.setDefaultFilter = function(obj){_default_filter = obj;};
	
	var curr_page = 1;
	var max_page = 1;
	function setMaxPage(new_max_page)
	{
		max_page = new_max_page;
		_table.find('span.curr_page').html(curr_page+'/'+max_page);
	}
	setMaxPage(max_page);
	
	var _header = _table.find('div.sima-guitable-head');
	var _body = _table.find('div.sima-guitable-body');
	
	var row_select_func = new Array();
	var row_unselect_func = new Array();
	var row_dblclick_func = new Array();
	
	this.setRowDblClick 	= function (func){row_dblclick_func.push(func);};
	this.setRowSelect   	= function (func){row_select_func.push(func);};
	this.setRowUnSelect 	= function (func){row_unselect_func.push(func);};
	this.setTextSearch  	= function (ts){search_text=ts;};
	this.setFilter  		= function (ts){filter=ts;};
	this.setMultiSelect 	= function(){multiselect=true;};
	this.unsetMultiSelect	= function() {multiselect=false;};

        this.setAddButton = function(add_button)
        {
            this.add_button = add_button;
        };
	
	this.addButton = function()
        {
            sima.model.form(model, {}, {
                init_data: this.add_button
            });
        };
	
	/**
	 * funkcija koja iskljucuje mogucnost selektovanja redova u tabeli
	 */
	this.disable_row_click = function()
	{
		selectRow=false;
	};
	
        /**
	 * funkcija koja selektuje red u tabeli koji ima klasu koja je ulazni parametar funkcije , tag
         * @param {type} tag
         * @returns {undefined}
         */
	this.select_row = function(tag)
	{
		click_row(_table.find('.'+tag));
	};
	
	/**
	 * funkcija deselktuje sve redove u tabeli koji su selektovani, tj. koji imaju klasu 'selected'
	 */
	this.unselect_rows = function()
	{
		_table.find('.row').each(function(){
			var temp=$(this);
			temp.removeClass('selected');
			for (var i = 0; i < row_unselect_func.length; i++) row_unselect_func[i](temp);
			
		});		
	};
	
	/**
	 * funkcija koja niz funkcija za dupli klik postavlja da bude prazan
	 */
	this.clean_dblclick_func = function()
	{
		row_dblclick_func= new Array();
	};
	
	
	
	
	function click_row(row)
	{
		if (multiselect) 
		{
			row.toggleClass('selected');
			//ako je upravo ugasen select onda pokreni unselect
			if (!row.hasClass('selected')) for (var i = 0; i < row_unselect_func.length; i++) row_unselect_func[i](selected);
			
		}
		else
		{
			if (selected!==null) 
			{
				for (var i = 0; i < row_unselect_func.length; i++) row_unselect_func[i](selected);
				if (row.hasClass('selected'))
				{
					selected.removeClass('selected');
					return;
				}
				selected.removeClass('selected');
			}
			
			selected=row;
			selected.addClass('selected');
		}
		
		if (row.hasClass('selected')) for (var i = 0; i < row_select_func.length; i++) row_select_func[i](selected);
	}
	
	function dblclick_row(row)
	{
		var i=0;
		for (i = 0; i < row_dblclick_func.length; i++) row_dblclick_func[i](row);
                if (i===0) 
                {
                    sima.dialog.openInfo('ne postoje omogucene opcije');
                }
        
	}

    var alreadyclicked=false, clickDelay = 300; //milliseconds //TODO: moze da se postavi kao podesavanje
    _table.delegate('.row','click', function(){
        var row=$(this);
        if (alreadyclicked)
        {
            alreadyclicked=false; // reset
            clearTimeout(alreadyclickedTimeout); // prevent this from happening
            dblclick_row(row);
        }
        else
        {
            alreadyclicked=true;
            alreadyclickedTimeout=setTimeout(function(){
                alreadyclicked=false; // reset when it happens
               if(selectRow){ click_row(row); }
            },clickDelay); // <-- dblclick tolerance here
        }
    });
	
	this.putInFilter = function(column,data)
	{
		_header.find('.'+column+' input').val(data);   
		
	};
        
    var get_columns = function()
    {
        var _columns = []; 
        _header.find('th').each(function(){
            var column = $(this).attr('column');
            if (typeof(column)==='string' && column!=='')
                _columns.push(column);
        });
        return _columns;
    };
        
    var collect_data = function()
    {
        var filter = _default_filter; //objekat koji predstavlja filter
        
        if (search_text !== null)
            filter.text = search_text.val();
        if (_header.find('input.text').size() > 0)
               filter.text_header.find('input.text').val();

        _header.find('input').each(function() {
            if ($(this).attr('name')===(_table.attr('model')+'['+$(this).parent().attr('column')+']'))
                filter[$(this).parent().attr('column')] = $(this).val();
            else if ($(this).attr('name')===(_table.attr('model')+'['+$(this).parent().attr('relName')+']'+'['+$(this).parent().attr('fk_column')+']'))
            {
                var temp_array = {};
                temp_array[$(this).parent().attr('fk_column')] = $(this).val();
                filter[$(this).parent().attr('relName')] = temp_array;
            }
        });
        _header.find('select').each(function() {
            filter[$(this).parent().attr('column')] = $(this).val();
        });
   

        var data = new Object(); // podaci koji se salju
        data.page = curr_page;
        data[_table.attr('model')] = filter; 
        data.columns = get_columns();
        
        return data;
    };
        
	/**
	 * ova promenljiva predstavlja sam AJAX zahtev. ukoliko se pojavi novi zahtev, treba prethodni pokinuti
	 */
    var populate_xhr = null;
    var populate = function(params)
    {
        if (populate_xhr !== null)
            populate_xhr.abort();
        populate_xhr = null;
        
        params = typeof params !== 'undefined' ? params : {};

        selected = null;
        
        for (var i = 0; i < row_unselect_func.length; i++)
            row_unselect_func[i](selected);

        _table.find('.tbody').css({opacity: 0.5});
        _table.find('.loading_circle').css("z-index", "100");

        populate_xhr = $.ajax(
                {
                    'url': url,
                    'type': 'POST',
                    'cache': false,
//                    'data': page_data,
                    'data': collect_data(),
                    'success': function(html) {
//                        _table.find('.tbody').html(html);
                        _table.find('.tbody').css({opacity: 1});
//                        _table.find('.loading_circle').css("z-index", "-100");
                        sima.set_html(_table.find('.tbody'),html);
                        setMaxPage(_table.find('.max_page').html());
                        
//                        sima.refreshChildrenForce(_table.find('.tbody'));
                    }
                });
    };
    
    this.append_row = function(model_id)
    {
        sima.ajax.get('guitable/getRow',{
            get_params:{
                model:_table.attr('model'),
                model_id:model_id
            },
            data:collect_data(),
            success_function:function(response){
                _body.find('.width_row').after(response.rows);
            }
        });
    };
    this.refresh_row = function(model_id,params)
    {
        if (typeof(params)!=='object')                      params={};
        if (typeof(params.async)!=='boolean')                params.async=false;
        sima.ajax.get('guitable/getRow',{
            get_params:{
                model:_table.attr('model'),
                model_id:model_id
            },
            async:params.async,
            data:collect_data(),
            success_function:function(response){
                var old = _body.find('.sima-guitable-row[model_id='+model_id+']');
                old.after(response.rows);
                old.remove();
            }
        });
    };
	
	

	var last_id=0;
	this.populate = function (params)
	{
		var local_id = ++last_id;
		setTimeout(function()
				{
					if (local_id===last_id)
					{
						curr_page=1;
                                                populate(params);

					}
				},500);
		return false;
	};
	
	
	this.inst_populate = function() 	{ 								populate(); };
	this.first = function()	{ curr_page=1;							populate(); };
	this.next = function()		{ if (curr_page<max_page) curr_page++;	populate(); };
	this.prev = function()		{ if (curr_page>1) curr_page--;			populate(); };
	this.last = function()		{ curr_page=max_page;					populate(); };
	
	this.reset = function() 	{ 
		_header.find('input').each(function(){
			$(this).val('');
		});
		_header.find('select').each(function(){
			$(this).val('');
		});		
	
	
			populate(); 
	};

//	_header.css('overflow-x','hidden');
//	_header.css('overflow-y','scroll');
//	
//	_body.css('overflow-y','scroll');
//
//        _body.css('overflow-x','scroll');

}