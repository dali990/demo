/* global sima, JSGantt */

function SIMAGantt()
{
    this.init = function()
    {

    };

    this.draw = function(elem_id, gantt_data, params)
    {
        var g = this.initGant(elem_id, gantt_data, params);
        var parents = this.getAllParents(gantt_data);
        for (var key in gantt_data)
        {
            var value = gantt_data[key];
            //ukoliko se ne uradi provera izbacuje dete od roditelja i stavlja negde druge pri ispisu
            if (this.isValidStartEndDate(value.start, value.end))
            {
                if (parents.indexOf(value.id) > -1)
                {
                    value.end = '';
                }
                else
                {
                    value.color = 'gtaskred';
                    value.note = sima.translate('GanttDateError');
                    value.end  = new Date(this.timeToInteger(value.start) + (1000 * 60 * 60 * 23)) ; //var date = new Date(value.end);
                }
                sima.errorReport.createAutoClientBafRequest(
                    'simaGantt - dateError' +
                    '<br>Nepravilno zadati podaci:' + 
                    '<br><b>naziv:</b> ' + value.name + 
                    '<br><b>startDate:</b>' + value.start + 
                    '<br><b>endDate:</b>' + value.end
                );
            }
            var color = this.parseValue(value.color, 'gtaskblue');
            var group = 0;
            if (parents.indexOf(value.id) > -1)
            {
                group = 1;
                color = 'ggroupblack';
            }

            g.AddTaskItem(
                new JSGantt.TaskItem(
                    value.id, //Activity ID
                    value.name, //Activity name
                    value.start, //Activity start date
                    value.end, //Activity end date
                    color, //css class name for stripe
                    '', // link to show in popup
                    0, //mile
                    this.parseValue(value.coordinator, ''), //pResorces
                    this.parseValue(value.complete, 0), //complete - completion percent
                    group, //group 0 = normal; 1 = for parents(when have child); 2 - all child in one row(parent have no sripe)
                    this.parseValue(value.parent, 0), // Activity parent_id
                    1, // pOpen: 0 = closed; 1 = open
                    '', //dependecies parent id or ids plus FS/SS/FF example1: '123FS' example2: '233,76' 
                    '', //(optional) detailed task info added if  setCaptionType('Caption')
                    this.parseValue(value.note), //detailed description in popup 
                    g
                )
            );
        }
        g.Draw();
    };

    this.configureLanguage = function()
    {
        return {
            'format': sima.translate('GanttFormat'),
            'january': sima.translate('GanttJanuar'), 'february': sima.translate('GanttFebruary'), 'march': sima.translate('GanttMarch'), 'april': sima.translate('GanttApril'), 'maylong': sima.translate('GanttMayLong'), 'june': sima.translate('GanttJun'), 'july': sima.translate('GanttJuly'), 'august': sima.translate('GanttAugust'), 'september': sima.translate('GanttSeptember'), 'october': sima.translate('GanttOctober'), 'november': sima.translate('GanttNovember'), 'december': sima.translate('GanttDecember'),
            'day': sima.translate('GanttDay'),
            'Day': sima.translate('GanttDay'),
            'week': sima.translate('GanttWeek'),
            'months': sima.translate('GanttMonths'),
            'duration': sima.translate('GanttDuration'),
            'comp': sima.translate('GanttComplete'),
            'resource': sima.translate('GanttResource'),
            'startdate': sima.translate('GanttStartDate'),
            'moreinfo': sima.translate('GanttMoreInfo'),
            'enddate': sima.translate('GanttStopDate'),
            'qtrs': sima.translate('GanttQuarters'),
            'quarter': sima.translate('GanttQuarter'),
            'month': sima.translate('GanttMonth'),
            'dys': sima.translate('GanttDays'),
            'completion': sima.translate('GanttCompletion'),
            'notes': sima.translate('GanttNotes'),
            'jan': sima.translate('GanttJan'), 'feb': sima.translate('GanttFeb'), 'mar': sima.translate('GanttMar'), 'apr': sima.translate('GanttApr'), 'may': sima.translate('GanttMay'), 'jun': sima.translate('GanttJun'), 'jul': sima.translate('GanttJul'), 'aug': sima.translate('GanttAug'), 'sep': sima.translate('GanttSep'), 'oct': sima.translate('GanttOct'), 'nov': sima.translate('GanttNov'), 'dec': sima.translate('GanttDec'),
            'monday': sima.translate('GanttMonday'), 'tuesday': sima.translate('GanttTuesday'), 'wednesday': sima.translate('GanttWednesday'), 'thursday': sima.translate('GanttThursday'), 'friday': sima.translate('GanttFriday'), 'saturday': sima.translate('GanttSaturday'), 'sunday': sima.translate('GanttSunday'),
            'mon': sima.translate('GanttMon'), 'tue': sima.translate('GanttTue'), 'wed': sima.translate('GanttWed'), 'thu': sima.translate('GanttThu'), 'fri': sima.translate('GanttFri'), 'sat': sima.translate('GanttSat'), 'sun': sima.translate('GanttSun'),
            'hour': sima.translate('GanttHour'),
            'hours': sima.translate('GanttHours'),
            'days': sima.translate('GanttDays'),
            'weeks': sima.translate('GanttWeeks'),
            'hr': sima.translate('GanttHr'),
            'dy': sima.translate('GanttDay'),
            'wk': sima.translate('GanttWk'),
            'mth': sima.translate('GanttMth'),
            'qtr': sima.translate('GanttQtr'),
            'hrs': sima.translate('GanttHrs'),
            'wks': sima.translate('GanttWks'),
            'mths': sima.translate('GanttMths')
        };
    };

    this.initGant = function(elem_id, gantt_data, params)
    {
        var elem = document.getElementById(elem_id);
        var default_view;
        if (typeof params.default_period_preview !== 'undefined' && this.validateDefaultViewOption(params.default_period_preview))
        {
            default_view = params.default_period_preview;
        } 
        else
        {
            default_view = this.getDefaultViewDuration(gantt_data);
        }
        var g = new JSGantt.GanttChart(elem, default_view);
        g.setUseSingleCell(200000);
        g.addLang('NU', this.configureLanguage());
        g.setQuarterColWidth(75);
        g.setScrollTo("today");
        g.setCaptionType('None'); // (default)"None", "Caption", "Resource", "Duration", "Complete"
        g.setUseToolTip(1);
        g.setUseSort(1);
        g.setShowRes(1);
        g.setShowComp(1);
        g.setShowDur(1);
        g.setShowStartDate(1);
        g.setShowEndDate(1);
        g.setShowEndWeekDate(0);
//        g.setDateInputFormat('');
        g.setQuarterMinorDateDisplayFormat('q');
//        g.setShowTaskInfoLink(1);
        g.setLang('NU');
        g.setFormatArr('Day', 'Week', 'Month', 'Quarter');

        return g;
    };

    this.getDefaultViewDuration = function(gantt_data)
    {
        var value;
        var arrayForMin = [], arrayForMax = [];
        for (var key in gantt_data)
        {
            value = gantt_data[key];
            arrayForMin.push(this.timeToInteger(value.start) / 1000 / 60 / 60 / 24);
            arrayForMax.push(this.timeToInteger(value.end) / 1000 / 60 / 60 / 24);
        }
        var full_duration = Math.max(...arrayForMax) - Math.min(...arrayForMin);
        var def_view = 'quarter';
        if (full_duration < 365 && full_duration > 180)
        {
            def_view = 'month';
        } 
        else if (full_duration < 180 && full_duration > 100)
        {
            def_view = 'week';
        } 
        else if (full_duration < 100)
        {
            def_view = 'day';
        }
        return def_view;
    };

    this.isValidStartEndDate = function(start, end)
    {
        if (this.timeToInteger(start) > this.timeToInteger(end))
        {
            return 1;
        }
        return 0;
    };

    this.validateDefaultViewOption = function(value)
    {
        return ['quarter', 'day', 'month', 'week'].includes(value);
    };
    
    this.timeToInteger = function(value)
    {
        return new Date(value).getTime();
    };

    this.getAllParents = function(gantt_data)
    {
        var parents = [];
        for (var key in gantt_data)
        {
            var value = gantt_data[key];
            if (value.parent > 0 && !parents.includes(value.parent))
            {
                parents.push(value.parent);
            }
        }
        return parents;
    };

    this.parseValue = function(value, default_value)
    {
        return typeof value !== 'undefined' && value !== null ? value : default_value;
    };

}
