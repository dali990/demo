/* global sima */

(function($){
     var methods = {
         init:function(params)
         {
            var wrap=this;
            var calendar_panel=wrap.find('div.calendar_panel');
            var calendar_container=$("<div class='sima-ui-event-calendar'></div>");
            var active_tasks_panel=wrap.find('div.active_tasks');
            
            var legend=[
                {
                    type:'daily_work_hour',
                    text:'Broj u gornjem levom uglu dana - Odradjeno sati po danu'
                },
                {
                    type:'irregular_days_coloring',
                    text:'Obojeni dani: neregularni'
                },
                {
                    type:'meeting',
                    text:'Sastanak - Početak sastanka'
                },
                {
                    type:'task',
                    text:'Zadatak - Isticanje roka'
                },
                {
                    type:'training',
                    text:'Obuka - Početak obuke'
                },
                {
                    type:'cube_break',
                    text:'Lomljenje kocke - Betonska baza'
                },
                {
                    type:'work_contract',
                    text:'Radni ugovor - Isticanje radnog ugovora'
                },
                {
                    type:'licence_certificate',
                    text:'Potvrda za licencu zaposlenih - Isticanje potvrde'
                },
                {
                    type:'more_event',
                    text:'Postoji više od 4 dogadjaja'
                },
                {
                    type:'medical_examinations',
                    text:'Lekarski pregledi koji ističu'
                },
                {
                    type:'safe_evd6_theoretical_trainings',
                    text:'Evidencije o zaposlenima osposobljenim za teoretski rad'
                },
                {
                    type:'safe_evd6_practical_trainings',
                    text:'Evidencije o zaposlenima osposobljenim za praktični rad'
                },
                {
                    type:'contracts',
                    text:sima.translate('ContractExpiration')
                },
                {
                    type: 'vehicle_registrations_expire',
                    text: sima.translate('VehicleRegistrationsExpire')
                },
                {
                    type: 'vehicle_tehnical_controls_expire',
                    text: sima.translate('VehicleTehnicalControlsExpire')
                },
                {
                    type: 'vehicle_tachographs_expire',
                    text: sima.translate('VehicleTachographsExpire')
                },
                {
                    type: 'insurance_policies_expire',
                    text: sima.translate('InsurancePoliciesExpire')
                },
                {
                    type: 'activity_start',
                    text: sima.translate('ActivityBeginDate')
                },
                {
                    type: 'activity_end',
                    text: sima.translate('ActivityDeadlineDate')
                },
                {
                    type: 'fixed_assets_leasing_end',
                    text: sima.translate('FixedAssetsLeasingDeadline')
                }
            ];
            
            var daily_activities_panel=wrap.find('.center_panel');
                      
            var calendar_params = {
                user_id: params.user_id,
                legend:legend,
                get_events:'user/getCalendarEvents',
                get_day_events:'user/getDayActivities',
                daily_activities_panel: daily_activities_panel,
                icons_legend: params.icons_legend,
                monthNames: params.monthNames,
                dayNamesShort: params.dayNamesShort
            };
            
            calendar_container.simaCalendar('init', calendar_params);
          
            calendar_panel.append(calendar_container);
            
            listActiveTasks();
            
            function listActiveTasks()
            {
                sima.ajax.get('jobs/task/tabActiveTasks', {
                   async:true,
                   loadingCircle:active_tasks_panel,
                   get_params:{
                       user_id:params.user_id
                   },
                   success_function:function(response){
                       sima.set_html(active_tasks_panel,response.html,'TRIGGER_start_page_listActiveTasks');
                   },
                   is_idle_since: false
                });
            }
         }
     };
$.fn.simaStartPage = function(methodOrOptions) {
        var ar = arguments;
        this.each(function(){
            if ( methods[methodOrOptions] ) {
                return methods[ methodOrOptions ].apply( $(this), Array.prototype.slice.call( ar, 1 ));
            } else if ( typeof methodOrOptions === 'object' || ! methodOrOptions ) {
                // Default to "init"
                return methods.init.apply( $(this), ar );
            } else {
                $.error( 'Method ' +  methodOrOptions + ' does not exist on jQuery.simaCalendar');
            }    
        });
    };  
})( jQuery );
