/* global sima */

function SIMAWarns()
{
//    var notification_table = $('.notification_table');
//    var _notif_table_warn = $('.notification_table.warn');
//    var _notif_bar = $('#top_bar div.notifs');    
//    var _warn_wrapper = _notif_bar.find('.notifs_info_wrapper');
//    var _warn_cloud = _notif_bar.find('.warn_cloud');
//    var _warn_icon = _notif_bar.find('.warn');
    
//    var irregularDaysWarnDialogToStop = false;

    this.init = function()
    {
        
    };

    /**
     * klik na warn znak (setovano u mainmenu.php)
     */
//    this.warn_click = function()
//    {
//        notification_table.not('.warn').hide();
//        _notif_table_warn.slideToggle('slow');
//    };

//    _notif_table_warn.on('click', '.sima-warn-item', function(event) {
//        sima.notifications.hideOverlayAndTables();
//    });

    /*
     * ako se klikne van upozorenja onda se ona zatvaraju
     */
//    _notif_table_warn.bind("clickoutside", function(event) {
//        if (!_warn_icon.is(event.target) && !_warn_cloud.is(event.target) && !_warn_wrapper.is(event.target))
//        {            
//            _notif_table_warn.slideUp('fast');
//        }
//    });
    
//    this.update = function(warns)
//    {
//        if (!sima.isEmpty(warns))
//        {
//            $.each(warns, function(index, value) {
//                var warn_amount_el = _notif_table_warn.find('.sima-warns .' + index + ' .sima-warn-amount');
//                if (!sima.isEmpty(warn_amount_el))
//                {
//                    warn_amount_el.html(parseInt(value));
//                }
//            });
//            
//            var count = 0;
//            _notif_table_warn.find('.sima-warns .sima-warn-item').each(function(){                
//                if (parseInt($(this).find('.sima-warn-amount').text()) === 0)
//                {
//                    $(this).hide();
//                }
//                else
//                {
//                    $(this).show();
//                    count++;
//                }
//            });
//
//            warn_set_count(count);
//
//            /**
//             * dialog za nepotvrdjene dane
//             */
//            if (typeof warns.WARN_UNCONFIRMED_DAYS !== 'undefined' && sima.getShowUnconfirmedDaysWarn() === true)
//            {
//                var unconfirmed_days = parseInt(warns.WARN_UNCONFIRMED_DAYS);                
//                if (unconfirmed_days > 0)
//                {
//                    openIrregularDaysWarnDialog("Imate " + unconfirmed_days + " dana u kojima niste potvrdili izveštaje. Molimo Vas da to učinite u najskorije vreme!", null);
//                }
//                else if (irregularDaysWarnDialogToStop === false)
//                {
//                    irregularDaysWarnDialogToStop = true;
//                }
//            }
//        }
//    };
    
    this.renderWarnsForModel = function(model_name, model_id, warn_container_selector)
    {
        var warn_container = $(warn_container_selector);
        sima.ajax.get('base/warn/renderWarnsForModel', {
            get_params: {
                model_name: model_name,
                model_id: model_id
            },
            async: true,
            success_function: function(response) {
                warn_container.html(response.html).show();
                sima.layout.allignFirstLayoutParent(warn_container,'TRIGGER_renderWarnsForModel');
            }
        });
    };
    
    this.resolveWarnForModel = function(model_name, model_id, warn_code)
    {
        sima.ajax.get('base/warn/checkResolveWarnForModel', {
            get_params: {
                model_name: model_name,
                model_id: model_id,
                warn_code: warn_code
            },
            async: true,
            success_function: function(response) {
                if (response.status === 'OK')
                {
                    if (!sima.isEmpty(response.message))
                    {
                        sima.dialog.openYesNo(response.message, function(){
                            sima.dialog.close();
                            resolveWarnForModelInternal(model_name, model_id, warn_code);
                        });
                    }
                    else
                    {
                        resolveWarnForModelInternal(model_name, model_id, warn_code);
                    }
                }
                else if (response.status === 'NOT_OK')
                {
                    sima.dialog.openWarn(response.message);
                }
                else if (response.status === 'JS_FUNC')
                {
                    sima.executeFunction(response.js_func);
                }
            }
        });
    };
    
    function resolveWarnForModelInternal(model_name, model_id, warn_code)
    {
        sima.ajax.get('base/warn/resolveWarnForModel', {
            get_params: {
                model_name: model_name,
                model_id: model_id,
                warn_code: warn_code
            },
            async: true,
            loadingCircle: $(document.activeElement),
            success_function: function(response) {

            }
        });
    }
    
//    function openIrregularDaysWarnDialog(message, isIrregularDaysWarnDialogOpened)
//    {
//        if (irregularDaysWarnDialogToStop === false)
//        {
//            if (isIrregularDaysWarnDialogOpened === null)
//            {
//                var params = {};
//                params.function = function()
//                {
//                    isIrregularDaysWarnDialogOpened = null;
//                };
//                params.close_function = function()
//                {
//                    isIrregularDaysWarnDialogOpened = null;
//                };
//                isIrregularDaysWarnDialogOpened = sima.dialog.openWarn(message, '', params);
//            }
//            setTimeout(function()
//            {
//                openIrregularDaysWarnDialog(message, isIrregularDaysWarnDialogOpened);
//            }, 3600000);
//        }
//    }
    
//    function warn_set_count(inc)
//    {
//        _warn_cloud.html(inc);
//        if (_warn_cloud.html() === '0')
//        {
//            _warn_cloud.hide();
//            _warn_icon.removeClass('new');
//        }
//        else
//        {
//            _warn_cloud.show();
//            _warn_icon.addClass('new');
//        }
//    }
    
    this.renderWarnsOnUserSelect = function(content_uniq_id, row)
    {
        var user_id = row.attr('model_id');
        var content = $('#'+content_uniq_id);
        var content_right_panel = content.find('.warns_by_users_right_panel');
        
        sima.ajax.get('base/warn/renderWarnsForUser', {
            get_params: {
                user_id: user_id
            },
            async: true,
            loadingCircle: content_right_panel,
            success_function: function(response) {
                content_right_panel.html(response.html);
            }
        });
    };
    
    this.renderWarnsOnUserUnSelect = function(content_uniq_id)
    {
        var content = $('#'+content_uniq_id);
        content.find('.warns_by_users_right_panel').empty();
    };
}

