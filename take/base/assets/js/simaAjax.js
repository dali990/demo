
/* global sima, encodeURIComponent */

/**
 * Function for using ajax
 * @returns 
 */
function SIMAAjax()
{
    this.was_network_outage = false;
    this.currently_network_outage = false;
    this.network_outage_start_datetime = null;
    this.network_outage_end_datetime = null;
    this.sync_actions_whitelist = [
        'admin/admin/addEmployee',
        'admin/admin/devalidateCacheById',
        'admin/admin/syncMeasurementUnitsFromCodebook',
        'admin/admin/syncPaymentCodesFromCodebook',
        'admin/admin/syncPaymentConfigurationsFromCodebook',
        'admin/authManager/addDeleteAllConnections',
        'admin/authManager/checkOperationsDifference',
        'admin/authManager/connectAccesses',
        'admin/authManager/mainMenuOperationDifference',
        'admin/authManager/modelOperationDifference',
        'admin/authManager/hasConnections',
        'admin/authManager/operationDifference',
        'admin/authManager/positionModul',
        'admin/authManager/removeAccessAndConnections',
        'admin/authManager/sizeModul',
        'admin/authManager/toggleAccess',
        'admin/authManager/addOperation',
        'HR/WPCompetitions/expiredWorkPositionCompetitionsWithoutCompetitionGradeForCurrUser',
        'HR/WPCompetitions/index',
        'HR/absence/addSickLeaveDocument',
        'HR/absence/allAbsences',
        'HR/absence/annualLeaveDecisionsByYear',
        'HR/absence/collectiveAnnualLeaves',
        'HR/absence/employeeSlave',
        'HR/absence/generateAnnualLeaveDecisionDocument',
        'HR/absence/getAbsenceTabsForEmployee',
        'HR/absence/getAnnualLeaveDecisionsForYear',
        'HR/absence/getAnnualLeavesForEmployee',
        'HR/absence/getCollectiveAnnualLeaveFullInfo/',
        'HR/absence/getFreeDaysForEmployee',
        'HR/absence/addSickLeaveDocument',
        'HR/absence/getSickLeavesForEmployee',
        'HR/employeeOfficeEntryLog/getNonExistingEntryDoorLogCards',
        'HR/headOf/directorTabs',
        'HR/nationalEvents/autoFillForCurrentYear',
        'HR/nationalEvents/getUnusedAndWithoutNullDatesNationalEvents',
        'HR/safeRecord/getPersonWorkPositionInSystemCompany',
        'HR/safeRecord/safeRecordTabs',
        'HR/workContract/getBenefitedWorkExperience',
        'HR/workContract/generateWorkContractTemplates',
        'HR/workLicense/licenseTabs',
        'accounting/accounting/accountingYears',
        'accounting/accounting/addAuthorizationFileToBillOfExchange',
        'accounting/accounting/changeAccountName',
        'accounting/accounting/changeAccountTransactions',
        'accounting/accounting/chooseAccountsForTransactionsChanges',
        'accounting/accounting/getAccountingPartnersReportExportParams',
        'accounting/accounting/getBankModelByBankCode',
        'accounting/accounting/getGrossBalanceExportParams',
//        'accounting/bill/billPdfAddNewVersionFromTempFile',
        'accounting/bill/connectBillItemsAndCostLocations',
        'accounting/billReleasing',
        'accounting/billReleasing/add',
        'accounting/billReleasing/addAdvance',
        'accounting/billReleasing/addAdvanceBillRelief',
        'accounting/billItems/addToFixedAssets',
        'accounting/billReleasing/addRelief',
        'accounting/billReleasing/connectAdvance',
        'accounting/bill/connectBillItemsAndCostLocations',
        'accounting/books',
        'accounting/books/accountAddLink',
        'accounting/books/accountLayouts',
        'accounting/books/accountRemoveLink',
        'accounting/books/addAccountDocument',
        'accounting/books/addBillsToAccountOrder',
        'accounting/books/checkBillsForAccountOrder',
//        'accounting/books/confirmAccountDocumentDiffs',
        'accounting/books/getAccountDocumentIdFromAccountOrder',
        'accounting/books/orders',
//        'accounting/books/setAccountOrderConfirmed',
        'accounting/costTypes',
        'accounting/costTypes/indexRightPanel',
//        'accounting/fixedAssets/calculateFADepreciations',
        'accounting/ios/generateIosForCompany',
        'accounting/ios/generateIosForCompanyDialog',
        'accounting/ios/getIosDate',
        'accounting/ios/listIoses',
        'accounting/ios/renderIosesForCompanyAndYear',
        'accounting/paychecks/paycheck',
        'accounting/paychecks/paycheck/generateTaxesPaidConfirmationDocument',
        'accounting/paychecks/paycheck/paygrades',
        'accounting/paychecks/paycheck/sendOZFileForPaycheck',
        'accounting/paychecks/paycheck/unifiedPaycheck',
        'accounting/paychecks/paycheckPeriod/exportPaycheckForBank',
        'accounting/paychecks/paycheckPeriod/yearStatistic',
//        'accounting/paychecks/paycheckPeriod/preRecalcPeriodValidation',
        'accounting/paychecks/suspension/groupIndex',
        'accounting/payment',
        'accounting/vat/POPDVBillsDisplay',
        'accounting/vat/rightPanel',
        'accounting/vat/setVatAccountDocument',
        'accounting/warehouse/warehouse',
        'accounting/warehouse/warehouse/addWarehouseAsCostLocation',
        'accounting/warehouse/warehouse/AddWarehouseTransferToBill',
        'accounting/warehouse/warehouse/connectBillToTransfer',
        'accounting/warehouse/warehouse/warehouseInYear',
        'accounting/warehouse/wMaterial',
        'accounting/warehouse/wMaterial/all',
        'accounting/warehouse/wTransfer/addToBill',
        'accounting/warehouse/wProduction', //ovo ide iz defaulLayout-a
        'activity/confirmDate',
        'activity/getLastConfirmedDayMonth',
        'activity/onConfirmDate',
        'activity/onRevertConfirmDate',
        'activity/revertConfirmDate',
        'admin/admin/addUser',
        'admin/admin/resetPass',
        'admin/authManager/mainmenuModelOperationDetails',
        'admin/authManager/selectUser',
        'admin/authManager/drawTree',
        'admin/authManager/designerDifference',
        'admin/interProcessLocks/removeLock',
//        'addressbook/partner', /// MilosJ: sva mesta poziva su dinamicka i trebalo bi da su ona sredjena
        'autoBilling/aBInvoicing/getClientDependencyEventForBillingItem',
        'autoBilling/aBInvoicing/insertBillingItemsMonthValues',
        'base/base/fillFromNbsInForm',
        'base/base/getYearIdFromDate',
//        'base/errorReports/CreateAutoClientBafRequest',
//        'base/errorReports/sendClientBafRequest',
        'base/configuration/getModelChooseParams',
        'base/configuration/setConfigParam',
        'base/configuration/setConfigParamToDefault',
        'base/configuration/changeConfigParamDialog',
        'base/configuration/parameters',
        'base/configuration/paramUserChangableChanged',
//        'base/errorReports/addError',
//        'base/errorReports/addErrorDesc',
        'base/errorReports/editError',
        'base/model/addToRelation',
        'base/model/cancel',
//        'base/model/form',
        'base/model/GetModelAttributeValue',
        'base/model/getModelAttributeValue',
        'base/model/getModelDisplayName',
//        'base/model/guiTable',
//        'base/model/modelConfirm',
//        'base/model/ModelConfirm',
//        'base/model/modelConfirmCheckAccess',
        'base/model/multiselectRecycle',
        'base/model/view',
        'base/model/getValueFromRelation',
        'base/model/getModelsDisplayName',
        'base/model/recycle',
        'base/model/renderModelView',
        'base/model/restoreFromTrash',
//        'base/model/searchModel',
        'base/model/setEndTime',
        'base/model/tabs',
        'base/model/remove',
        '/base/model/remove',
        'base/modelChoose/index',
        'base/modelChoose/onTableChoosePartnerList',
        'base/modelChoose/onTableChooseTheme',
        'base/QRCode/downloadQRCode',
        'base/theme/convertTo',
        'base/theme/fullInfo',
        'base/theme/personOnCostControllerHendler',
        'base/theme/renderThemeSettingsView',
        'base/theme/saveThemeSettings',
        'CF/CF/orderTabs',
        'CF/CFAutoDocuments/makeProduction',
        'CF/CFWarehouse/WarehouseInput',
        'CF/CFWarehouse/WarehouseOverview',
        'comments',
        'ddmanager/addDirectoryDefinition',
        'ddmanager/changeSelectedDirectoryDefinition',
        'ddmanager/createDirectoryDefinition',
        'ddmanager/fileManager',
        'ddmanager/loadDDTree',
//        'defaultLayout',
        'desktopapp/openOptions',
        'external/energyReadings/countEnergySpentInPeriod',
        'external/energyReadings/getEnergyReadingInfo',
        'external/energyReadings/populateAutoBilling',
        'external/nekretnine/apartment',
        'external/nekretnine/apartmentFilesTab',
        'external/nekretnine/categories',
        'external/nekretnine/deletePicture',
        'external/nekretnine/listSnAccounts',
        'external/nekretnine/saveApartmentMapPoint',
        'external/nekretnine/saveApartmentMapPoints',
        'external/nekretnine/saveFloorMapPoints',
        'external/nekretnine/saveGarageMapPoints',
        'external/nekretnine/saveRoomMapPoints',
        'external/nekretnine/showGallery',
        'external/snNews/gallery',
//        'filebrowser2/getPrepareDataForAddFile',
//        'filebrowser2/initDDDropdownList',
//        'filePreview/downloadFileFromCompressedFileVersion',
        'files/file', /// zove se iz SIMADialog.openAction
//        'files/file/addFileFromVersion',
        'files/file/addUsersToContribSignatureViews',
        'files/file/changeSignWait',
//        'files/file/convertTo',
        'files/file/filesWaitingSignatureTabs',
        'files/file/fileVersion',
        'files/file/isfilelocked',
        'files/file/lockFile', /// mora posebno pristupiti za simaDesktopApp.js:lockFile
        'files/file/lockFileForFileVersion',
        'files/file/multiselectLock',
        'files/file/requestSignatureViewForFilingContribs',
        'files/file/saveContribTagsInFiling',
//        'files/fileVersion/addFileVersionAsLastForFile',
        'files/tags/addTagToFileByModel',
        'files/tags/getTag',
        'files/tags/removeTagFromFile',
//        'files/template/addVersion',
        'files/template/editor',
        'files/template/headerEditor',
        'files/template/saveTemplate',
        'files/template/setTemplate',
        'files/template/viewTemplate',
//        'files/transfer/fileDownloadPrepare',
//        'files/transfer/prepareConverted',
        'filePreview/previewDialogContent', /// zahteva reimplementaciju sima.dialog.openActionInDialog
        'fixedAssets/fixedAsset/exportFixedAsset',
        'fixedAssets/fixedAsset/generateFixedAssetDeactivationDecisionPdf',
        'guitable', /// pozvalo se iz SIMADialog.openActionInDialog
//        'guitable/getColumns',
//        'guitable/getRow',
//        'guitable/exportOptions',
//        'guitable/renderFilterForColumn',
//        'guitable/saveColumnValue',
        'jobs/dayReport',
        'jobs/dayReport/confirmDayReport',
        'jobs/dayReport/getNewReports',
        'jobs/meeting/addThemesToMeeting',
        'jobs/meeting/addThemeToMeeting',
        'jobs/workedHoursEvidence/getMonthColumnForHide',
        'jobs/workedHoursEvidence/getNextMonth',
        'jobs/workedHoursEvidence/getPrevMonth',
        'jobs/workedHoursEvidence/listWorkedHoursEvidencesForTheme',
        'jobs/workedHoursEvidence/notActiveWorkedHoursEvidencesForUser',
        'jobs/workedHoursEvidence/openWorkedHoursEvidence',
        'jobs/job/buildingStructure',
        'jobs/job/getModelForTaskReportAttributess',
        'jobs/job/getTrainingTabs',
        'jobs/job/meeting',
        'jobs/job/tabArchiveBoxes',
        'jobs/job/tabArchiveShelfs',
        'jobs/job/training',
        'jobs/jobFinanse/details',
//        'jobs/personActivityOverview/getPrevMonth',
//        'jobs/personActivityOverview/renderPersonActivityOverviewForMonth',
        'jobs/personActivityOverview/renderPersonActivityOverviewForYear',
        'jobs/task',
//        'jobs/task/listTree',
        'jobs/task/updateTask',
        'jobs/taskReport/newTaskReportPrepare',
        'jobs/themeTasks/devalidateThemeCachedStatistics',
        'jobs/themeTasks/tabThemeTasks',
        'legal/filingBook/addFileFilingNumberLockRequest',
        'legal/filingBook/addFileFilingNumberUnLockRequest',
        'legal/filingBook/reserve',
        'legal/filingTransfer/cancelTransfer',
        'legal/filingTransfer/CancelTransfer',
        'legal/filingTransfer/confirmTransfer',
        'legal/filingTransfer/ConfirmTransfer',
        'legal/filingTransfer/fileTransfers',
        'legal/legal/addBidDocument',
        'legal/legal/reference',
//        'messages/email/downloadAttachmentFromImap',
//        'messages/email/downloadMessagePdf',
//        'messages/email/downloadMessagePdfFromSystem',
//        'messages/email/index',
//        'messages/email/getAttachmentsList',
//        'messages/email/getFileAttributes',
//        'messages/email/getMessageBodyHtml',
//        'messages/email/getMessageContent',
//        'messages/email/getMessageContentFromEml',
//        'messages/email/getMessageContentFromSystem',
//        'messages/email/getMessageFromSystemBodyHtml',
//        'messages/email/prepareDownloadEml',
//        'messages/email/prepareDownloadEmlFromSystem',
//        'messages/email/renderAttachmentsIfMessageInSystem',
//        'messages/email/renderEmailViewOptionsIfMessageInSystem',
//        'messages/email/renderMessageFilingCustomDialog',
//        'messages/email/renderMessageFromSystemFilingCustomDialog',
//        'messages/email/updateMessageRowFromImap',
//        'messages/email/addEmailFromImapToSystem',
        'messages/email/createPdfForEmailFile',
        'messages/emailAccount/accountSettings',
        'organizationScheme/sectorActiveTasksTabs',
//        'pdfManipulation/createDownloadablePDF',
//        'pdfManipulation/saveAsNewVersionForFile',
//        'pdfManipulation/saveNewFile',
        'repManager/distributeFVToRepmanager',
        'repManager/downloadFVFromRM',
        'repManager/getFVIntegrityDataFromRM',
        'repManager/removeFromSimaAndRepmanager',
        'repManager/resetToDistributeFVOnRepmanager',
        'repManager/secsamfrasar',
        'simaAddressbook/index',
        'simaAddressbook/partnersToPartnerLists',
        'simaAddressbook/phoneNumberTypes',
        'site/customDialog',
        'site/mailTab', //MIlosS: ovo je mozda i namerno ostavljeno zbog onog problema za iskakanje u novi tab
//        'site/orderScan',
//        'site/restoreMainTabs',
//        'site/saveMainTabs',
//        'setpp/bafs/baf/addBafsToBafRequest',
        'setpp/bafs/baf/addBafRequestsToBaf',
        'setpp/bafs/baf/addServerErrorReportsToBaf',
//        'setpp/bafs/baf/bafRequestsOverview',
        'setpp/bafs/baf/confirmBafForCurrUser',
        'setpp/bafs/baf/confirmBafRequestAndAddCurrUserAsCoordinator',
        'setpp/codebooks/codebooks/convertToCyrilic',
        'setpp/testing/testing/showModelAttributeInDialog',
        'setpp/testing/testing/svnTags',
//        'setpp/testing/testResult/codeTagTestingSegmentsStatuses', /// zahteva izmenu u sima.dialog.openActionInDialog
        'setpp/testing/testResult/getAcceptanceFailedTest',
        'setpp/testing/testResult/getUnitFailedTest',
        'setpp/testing/testResult/getTestRunResult',
        'setpp/testResult/getAcceptanceFailedTest',
        'setpp/testResult/getUnitFailedTest',
        'setpp/testResult/getTestRunResult',
        'taskFilterTree/listTree',
        'user/logout',
        'user/showIksTable'
    ];
    
    this.getWasNetworkOutage = function()
    {
        return this.was_network_outage;
    };
    this.getCurrentlyNetworkOutage = function()
    {
        return this.currently_network_outage;
    };
    this.getNetworkOutageStartDateTime = function()
    {
        return this.network_outage_start_datetime;
    };
    this.getNetworkOutageEndDateTime = function()
    {
        return this.network_outage_end_datetime;
    };
    this.setNetworkOutage = function(new_network_outage)
    {
        this.currently_network_outage = new_network_outage;
        if(new_network_outage === true)
        {
            this.was_network_outage = true;
            this.network_outage_start_datetime = new Date();
        }
        else if(new_network_outage === false)
        {
            this.network_outage_end_datetime = new Date();
        }
    };
    
    var request = [];
    var keep_alive_timeout = null;
    var isDialogOpened = null; //promenljiva u kojoj je pamti warn dialog koji je otvoren, sluzi kod osvezavanja sime
    var keepAlive_failCount = 0;
    function keepAlive() {
        sima.ajax.get('site/keepAlive', {
            async:true,
            is_idle_since: false,
            report_error: false,
            success_function:function(response){
                keepAlive_failCount = 0;
                
                if(!sima.isEmpty(sima.messages))
                {
                    sima.messages.setClientStatus('idle');
                }
                
                if(sima.ajax.getWasNetworkOutage() === true)
                {
                    if(sima.ajax.getCurrentlyNetworkOutage() === true)
                    {
                        sima.ajax.setNetworkOutage(false);
                    }
                    sima.dialog.close();
                }
                else
                {
                    resetKeepAlive();
                }
            },
            error_function: function() {
                keepAlive_failCount++;
                
                if(keepAlive_failCount === 3)
                {
                    clearTimeout(keep_alive_timeout);
                    sima.ajax.setNetworkOutage(true);
                    showNetworkOutageDialog();
                }
                
                resetKeepAlive(5000);
            }
        });
    };
    
    function showNetworkOutageDialog()
    {
        var html = 'Nestanak mreze' 
                + '</br>Od: ' + sima.ajax.getNetworkOutageStartDateTime().toLocaleString();
        
        if(sima.ajax.getWasNetworkOutage() === true && sima.ajax.getCurrentlyNetworkOutage() === false)
        {
            html += '</br>Do: ' + sima.ajax.getNetworkOutageEndDateTime().toLocaleString();
        }
        
        html += '</br>Moracete osveziti pretrazivac (f5) i potencijalno ulogovati ponovo';
        
        sima.dialog.openCustom(html, 'Nestanak mreze', {
            'close_func': function(){
                showNetworkOutageDialog();
            }
        });
    }
    
    //funkcija koja resetuje keep_alive. Poziva se pri svakom ajax.get.
    function resetKeepAlive(timeout)
    {
        if(sima.isEmpty(timeout) && timeout !== 0)
        {
            timeout = 60000;
        }
        
        clearTimeout(keep_alive_timeout);
        keep_alive_timeout =  setTimeout(function() {
            keepAlive();
        }, timeout);
    };
    
    /// MilosJ 20171207 - zakomentarisano jer se ne koristi
//    function stopKeepAlive()
//    {
//        clearTimeout(keep_alive_timeout);
//    };
    
    //funkcija koja periodicno otvara upozorenje da sima treba da se osvezi
    function openWarnDialog()
    {
        if (isDialogOpened === null)
        {
            var params = {};
            params.function = function()
            {
                isDialogOpened = null;
            };
            params.close_function = function()
            {
                isDialogOpened = null;
            };
            isDialogOpened = sima.dialog.openWarn('Morate osvežiti stranicu pritiskom na dugme f5, jer su napravljene izmene!', '', params);
        }
        setTimeout(function()
        {
            if (!sima.isReloaded)
            {
                openWarnDialog();
            }
        }, 10000);
    }
    
    /**
     * 
     * @param {string} action
     * @param {Object} params
     * @returns {Boolean}
     */
    this.get = function(action,params)
    {
        if (typeof(action)!=='string')
        {
            sima.dialog.openCustom('sima.ajax.get - Niste zadali akciju kao string','error');
            return false;
        }
        if (typeof(params)!=='object')                      params={};
        if (typeof(params.get_params)!=='object')           params.get_params={};
        
        if (typeof(params.data)!=='object' && typeof(params.data)!=='string')                  params.data={}; //data moze da bude i object i string
        if (typeof(params.data)==='object' && typeof params.data.length !=='undefined')//niz
        {
            if (params.data.length === 0)
                params.data = {};
            else
            {
                sima.dialog.openWarn('sima.ajax.get -> prosledjen je niz kao parametar','sima.ajax.get');
                return;
            }
        }
        
        if (typeof(params.data)==='object')
            params.data['YIISIMASESSION'] = sima.getTabSessionID();
        else //typeof(params.data)==='string'
            params.data = 'YIISIMASESSION='+(sima.getTabSessionID())+((params.data.length>0)?('&'+params.data):'');

        if (typeof(params.success_function)!=='function')   params.success_function = function(){};
        if (typeof(params.failure_function)!=='function') 
        {
            params.failure_function = function(ret)
            {
                sima.dialog.openWarn(ret.message);
                sima.model.updateViews(ret.updateModelViews,{async:'true'});
            };
        }
        if (typeof(params.error_function)!=='function')   params.error_function = null;
        if (typeof(params.async)!=='boolean')               params.async = false;//ako sve bude na true dolazi do totalne DESINHRONIZACIJE
        var url_data = sima.ajax.http_build_query(params.get_params);
        if (url_data!=='') url_data='&'+url_data;
        
        var showLoading = (typeof params.showLoading !== 'undefined') ? params.showLoading : true; 
        
        if ((sima.isEmpty(params.loadingCircleDelay) && params.loadingCircleDelay !== 0) || typeof(params.loadingCircleDelay) !== 'number')
        {
            params.loadingCircleDelay = 500;
        }
        
        var group = null;
        if (typeof params.group !== 'undefined')
        {
            group = params.group;
//            console.log('grupa je '+group);
        }
        
        if (group !== null && typeof request[group] !== 'undefined')
        {
//            console.log('abortujemo grupu '+ group);
            request[group].abort();
            delete request[group];
        }
        
        if (typeof params.report_error === 'undefined') params.report_error = true;
        
        if (sima.isEmpty(params.is_idle_since) && params.is_idle_since !== false) params.is_idle_since = true;
        var is_idle_since_str = 'true';
        if(params.is_idle_since === false)
        {
            is_idle_since_str = 'false';
        }
        
        var network_outage = 'false';
        if(sima.ajax.getWasNetworkOutage() === true)
        {
            if(action !== 'site/keepAlive')
            {
                return;
            }
            
            network_outage = 'true';
        }
        
        if(sima.isEmpty(params.data))
        {
            params.data = 'is_idle_since='+is_idle_since_str+'&network_outage='+network_outage;
        }
        else
        {
            if (typeof(params.data)==='object')
            {
                params.data['is_idle_since'] = is_idle_since_str;
                params.data['network_outage'] = network_outage;
            }
            else //typeof(params.data)==='string'
            {
                params.data += '&is_idle_since='+is_idle_since_str+'&network_outage='+network_outage;
            }
        }
        
        var loadingData = null;
        
        request[group] = jQuery.ajax({
            url: sima.getBaseUrl() + '/index.php?r='+action+url_data,
            type: 'POST',
            async: params.async,
            cache: false,
            data: params.data,
            dataType: 'JSON',
            beforeSend: function() {
                if (showLoading === true)
                {
                    if (params.async === true)
                    {
                        if (typeof(params.loadingCircle) !== 'undefined')
                        {
                            loadingData = sima.showLoading(params.loadingCircle, 0);
                        }
                    }
                    else
                    {
                        //Sasa A. - posto postoji problem sa setTimeout i sihronim pozivima(jer se izvrsavaju u istoj niti) za sada je iskljucen delay dok se ne nadje neko resenje
//                        loadingData = sima.showLoading(null, params.loadingCircleDelay);
                        loadingData = sima.showLoading(null, 0);
                    }
                }
                /// u php-u se na pocetku akcije upisuje last_seen, pa treba na pocetku i resetovati keepalive
                
                if(sima.ajax.getWasNetworkOutage() !== true)
                {
                    resetKeepAlive();
                }
            },
            success: function(ret, textStatus, jqXHR) {
//                resetKeepAlive();
//                console.log('usao u funkciju');
                if (ret===null) 
                {
                    if (params.error_function === null)
                    {
//                        sima.dialog.open('povratna vrednost nije AJAX u funkciji #'+action+'#',{type:'warning'});
                        sima.dialog.openError(-1,'povratna vrednost nije AJAX u funkciji #'+action+'#');
                    }
                    else
                    {
                        params.error_function(jqXHR, textStatus);
                    }
                }
                if (typeof(ret.status)==='undefined' || typeof(ret.message)==='undefined')
                {
                    if (params.error_function === null)
                    {
                        sima.dialog.openError(-1,'povratna vrednost za funkciju  #'+action+'# nema status ili message u svom JSON-u');
                    }
                    else
                    {
                        params.error_function(jqXHR, textStatus, null, ret);
                    }
                }    
                if (ret.status==='success')
                {
                    var response = ret.response;
                    var processOutputHead = ret.processOutputHead;
                    if (!sima.isEmpty(ret.processOutputBegin) && (sima.isEmpty(response) || sima.isEmpty(response.html)))
                    {
                        processOutputHead += ret.processOutputBegin;
                    }
                    $('head').append(processOutputHead);
                    
                    if (typeof(ret.updateModelViews)!=='undefined')
                    {                        
                        sima.model.prepareModelViewsTagsForUpdate(ret.updateModelViews);
                    }

                    var to_perform_success_function = true;
                    if(!sima.isEmpty(params.relative_elements))
                    {
                        $.each(params.relative_elements, function( index, relative_element ) {
                            if(!sima.isElementInDom(relative_element))
                            {
                                to_perform_success_function = false;
                                if(!sima.isEmpty(params.relative_elements_error_function))
                                {
                                    params.relative_elements_error_function();
                                }
                            }
                        });
                    }
                    if(to_perform_success_function === true)
                    {
                        if (typeof(response)!=='undefined')  // pa pozovi success funkciju
                        {
                            if (typeof response.html !== 'undefined')
                            {
                                response.html = ret.processOutputBegin + response.html + ret.processOutputEnd;
                            }

                            params.success_function(response);
                        }
                        else
                        {
                            params.success_function();
                        }
                    }
                    
                    //ako je setovan processOutput za begin ili end, a nije setovan html onda bacamo gresku
//                    if (
//                            (
//                                (typeof ret.processOutputBegin !== 'undefined' && ret.processOutputBegin !== '') || 
//                                (typeof ret.processOutputEnd !== 'undefined' && ret.processOutputEnd !== '')
//                            ) && 
//                            (typeof response === 'undefined' || typeof response.html === 'undefined' || response.html === '')
//                       )
//                    {
//                        //greska
//                        sima.errorReport.addError({
//                            action:action,
//                            message:'SIMAAjax - Nije prosledjen html, a setovan je processOutputBegin ili processOutputEnd.',
//                            get_params:params.get_params,
//                            post_params:params.data
//                        }, false);
//                    }

                    /** 
                     * prvo radimo success_function pa onda update view-ova
                     * u suprotom ce se bespotrebno updateovati view-ovi u formamam
                     *   */
                    if (typeof(ret.removeModelViews)!=='undefined') //prvo obrisi nepotrebne poglede
                        sima.model.removeViews(ret.removeModelViews); 
                    
                    if (typeof(ret.updateModelViews)!=='undefined') //pa update-uj poglede
                    {
                        sima.model.updateViews(ret.updateModelViews,{},true);
                    }

                    if (typeof(ret.iframes)!=='undefined') //pa update-uj poglede
                    {
                        for(var n in ret.iframes)
                        {
                            var iframe = document.getElementById(ret.iframes[n].frame_id);
                            if (!sima.isEmpty(iframe))
                            {
                                var doc = iframe.contentWindow.document;
                                doc.open();
                                doc.write(ret.iframes[n].content);
                                doc.close();
                                //na chromu load event ne radi
                                if (sima.isChromeBrowser())
                                {
                                    setTimeout(function() {
                                        iframe.height = doc.body.scrollHeight + 'px';
                                    }, 50);
                                }
                                else
                                {
                                    $(iframe).load(function() {
                                        iframe.height = doc.body.scrollHeight + 'px';
                                    });
                                }
                            }
                        }
                    }
                    
                    if (typeof(ret.actions)!=='undefined' && !jQuery.isEmptyObject(ret.actions))
                    {
                        for(var action in ret.actions)
                        {
                            var _action = ret.actions[action];
                            var _func = _action[0];
                            var _params = _action[1];
                            var methods = _func.split(".");
                            var result = window;
                            for(var i in methods) {
                                result = result[methods[i]];
                            }
                            result.apply(this,_params);
                        }
                    }
                    
                    var processOutputLoadAndReady = ret.processOutputLoadAndReady;
                    if (!sima.isEmpty(ret.processOutputEnd) && (sima.isEmpty(response) || sima.isEmpty(response.html)))
                    {
                        processOutputLoadAndReady = ret.processOutputEnd + ret.processOutputLoadAndReady;
                    }
                    $('body').append(processOutputLoadAndReady);
                }
                else if (ret.status==='failure')
                {
                    console.log('AJAX failure'+ret.message); //ovo je stavljeno jer negde otvori prozor koji se posle zatvori, ali ne znam gde (Mile Pajic i Jovan prijavili - BB)
                    params.failure_function(ret);
                    
                }
                else if (ret.status==='fatal_failure')
                {
                    console.log('fatal_failure: '+ret.message); //ovo je stavljeno jer negde otvori prozor koji se posle zatvori, ali ne znam gde (Mile Pajic i Jovan prijavili - BB)
                    if (typeof params.fatal_failure_function === 'function')
                    {
                        params.fatal_failure_function(jqXHR, textStatus, ret);
                    }
                    else
                    {
                        sima.dialog.openError(ret.error_id, ret.message);
                    }
                }
                else if (ret.status==='fatal_db_failure')
                {
                    console.log('fatal_db_failure: '+ret.message); //ovo je stavljeno jer negde otvori prozor koji se posle zatvori, ali ne znam gde (Mile Pajic i Jovan prijavili - BB)
                    sima.dialog.openError(ret.error_id,ret.message);
                }
                else if (ret.status==='requestLogin')
                {
//                    ???TREBA PROVERITI LICENCE I OSTALO>>>
//                    stopKeepAlive();
////                    window.location=sima.getBaseUrl();
//                    sima.dialog.openYesNo('Morate se ponovo ulogovati.<br />'
//                            +'Da li zelite da se ulogujete kao postojeci ili kao drugi korisnik?',{
//                        title: 'Postojeci',
//                        func: function(){
//                            ???TREBA PROVERITI LICENCE I OSTALO>>>
//                            sima.dialog.openPromt
//                            sima.ajax.get('user/login',{
//                                get_params:{}
//                            });
//                        }
//                    },{
//                        title: 'Novi',
//                        func: function(){
                            window.location=sima.getBaseUrl();
//                        }
//                    });
                    
                }
                else if (typeof ret.status === 'string')
                {
                    if (params.error_function === null)
                    {
                        var message = ret.message;
                        sima.errorReport.addError({
                            action:action,
                            message:'1-'+message.replace(/'/g, '').replace(/"/g, ''),
                            get_params:params.get_params,
                            post_params:params.data
                        });
                    }
                    else
                        params.error_function(jqXHR, textStatus, null, ret);
                }
                else 
                {
                    if (params.error_function === null)
                    {
                        var message = ret.message;
                        sima.errorReport.addError({
                            action:action,
                            message:'2-'+message.replace(/'/g, '').replace(/"/g, ''),
                            get_params:params.get_params,
                            post_params:params.data
                        });
                    }
                    else
                        params.error_function(jqXHR, textStatus, null, ret);
                }
                
                if (showLoading === true)
                {
                    if (params.async === true)
                    {
                        if (typeof(params.loadingCircle) !== 'undefined')
                        {
                            sima.hideLoading(params.loadingCircle, loadingData);
                        }
                    }
                    else
                    {
                        sima.hideLoading(null, loadingData);
                    }
                }
                
                if(params.is_idle_since === true && !sima.isEmpty(sima.messages))
                {
                    sima.messages.setClientStatus('active');
                }

                if (typeof(ret.notes) !== 'undefined')
                {
                    for(var n in ret.notes)
                    {
                        sima.dialog.openWarn(ret.notes[n].text);
                    }
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                if (showLoading === true)
                {
                    if (params.async === true)
                    {
                        if (typeof(params.loadingCircle) !== 'undefined')
                        {
                            sima.hideLoading(params.loadingCircle, loadingData);
                        }
                    }
                    else
                    {
                        sima.hideLoading(null, loadingData);
                    }
                }
                if (textStatus !== 'abort') //abort je pozvan iz javascript-a
                {
                    if(params.report_error === true && jqXHR.status !== 0)
                    {
                        sima.errorReport.addError({
                            action:action,
                            message:'3-'+(jqXHR.responseText).replace(/'/g, '').replace(/"/g, ''),
                            get_params:params.get_params,
                            post_params:params.data
                        });
                    }
                    
                    if (params.error_function !== null)
                    {
                        params.error_function(jqXHR, textStatus, errorThrown);
                    }
                }
            }
        });
        
        if(params.async === false && this.sync_actions_whitelist.indexOf(action) === -1)
        {
            var err = new Error();
            const stack_trace = err.stack;
            if(sima.getAjaxSyncActionConsoleLog() === true)
            {
                console.log('============== SYNC AJAX ==============');
                console.log('action:');
                console.log(action);
                console.log('params:');
                console.log(params);
                console.log('stack_trace:');
                console.log(stack_trace);
            }
            if(sima.getAjaxSyncActionAutoBaf() === true)
            {
                sima.errorReport.createAutoClientBafRequest(
                    '============== SYNC AJAX ==============' + 
                        '<br><b>action:</b> ' + JSON.stringify(action) + 
                        '<br><b>params:</b>' + JSON.stringify(params) +
                        '<br><b>stack_trace:</b>' + stack_trace,
                    2960,
                    'AutoBafReq async ajax'
                );
            }
        }
    };


    this.http_build_query = function(formdata, numeric_prefix, arg_separator) {
        // http://kevin.vanzonneveld.net
        // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
        // +   improved by: Legaev Andrey
        // +   improved by: Michael White (http://getsprink.com)
        // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
        // +   improved by: Brett Zamir (http://brett-zamir.me)
        // +    revised by: stag019
        // +   input by: Dreamer
        // +   bugfixed by: Brett Zamir (http://brett-zamir.me)
        // +   bugfixed by: MIO_KODUKI (http://mio-koduki.blogspot.com/)
        // %        note 1: If the value is null, key and value is skipped in http_build_query of PHP. But, phpjs is not.
        // -    depends on: urlencode
        // *     example 1: http_build_query({foo: 'bar', php: 'hypertext processor', baz: 'boom', cow: 'milk'}, '', '&amp;');
        // *     returns 1: 'foo=bar&amp;php=hypertext+processor&amp;baz=boom&amp;cow=milk'
        // *     example 2: http_build_query({'php': 'hypertext processor', 0: 'foo', 1: 'bar', 2: 'baz', 3: 'boom', 'cow': 'milk'}, 'myvar_');
        // *     returns 2: 'php=hypertext+processor&myvar_0=foo&myvar_1=bar&myvar_2=baz&myvar_3=boom&cow=milk'
        var value, key, tmp = [],
                that = sima.ajax;

        var _http_build_query_helper = function(key, val, arg_separator) {
            var k, tmp = [];
            if (val === true) {
                val = "1";
            } else if (val === false) {
                val = "0";
            }
            if (val != null) {
                if (typeof val === "object") {
                    for (k in val) {
                        if (val[k] != null) {
                            tmp.push(_http_build_query_helper(key + "[" + k + "]", val[k], arg_separator));
                        }
                    }
                    return tmp.join(arg_separator);
                } else if (typeof val !== "function") {
                    return that.urlencode(key) + "=" + that.urlencode(val);
                } else {
                    throw new Error('There was an error processing for http_build_query().');
                }
            } else {
                return '';
            }
        };

        if (!arg_separator) {
            arg_separator = "&";
        }
        for (key in formdata) {
            value = formdata[key];
            if (numeric_prefix && !isNaN(key)) {
                key = String(numeric_prefix) + key;
            }
            var query = _http_build_query_helper(key, value, arg_separator);
            if (query !== '') {
                tmp.push(query);
            }
        }

        return tmp.join(arg_separator);
    };


    this.urlencode = function(str) {
        // http://kevin.vanzonneveld.net
        // +   original by: Philip Peterson
        // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
        // +      input by: AJ
        // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
        // +   improved by: Brett Zamir (http://brett-zamir.me)
        // +   bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
        // +      input by: travc
        // +      input by: Brett Zamir (http://brett-zamir.me)
        // +   bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
        // +   improved by: Lars Fischer
        // +      input by: Ratheous
        // +      reimplemented by: Brett Zamir (http://brett-zamir.me)
        // +   bugfixed by: Joris
        // +      reimplemented by: Brett Zamir (http://brett-zamir.me)
        // %          note 1: This reflects PHP 5.3/6.0+ behavior
        // %        note 2: Please be aware that this function expects to encode into UTF-8 encoded strings, as found on
        // %        note 2: pages served as UTF-8
        // *     example 1: urlencode('Kevin van Zonneveld!');
        // *     returns 1: 'Kevin+van+Zonneveld%21'
        // *     example 2: urlencode('http://kevin.vanzonneveld.net/');
        // *     returns 2: 'http%3A%2F%2Fkevin.vanzonneveld.net%2F'
        // *     example 3: urlencode('http://www.google.nl/search?q=php.js&ie=utf-8&oe=utf-8&aq=t&rls=com.ubuntu:en-US:unofficial&client=firefox-a');
        // *     returns 3: 'http%3A%2F%2Fwww.google.nl%2Fsearch%3Fq%3Dphp.js%26ie%3Dutf-8%26oe%3Dutf-8%26aq%3Dt%26rls%3Dcom.ubuntu%3Aen-US%3Aunofficial%26client%3Dfirefox-a'
        str = (str + '').toString();

        // Tilde should be allowed unescaped in future versions of PHP (as reflected below), but if you want to reflect current
        // PHP behavior, you would need to add ".replace(/~/g, '%7E');" to the following.
        return encodeURIComponent(str).replace(/!/g, '%21').replace(/'/g, '%27').replace(/\(/g, '%28').
                replace(/\)/g, '%29').replace(/\*/g, '%2A').replace(/%20/g, '+');
    };

    
}

