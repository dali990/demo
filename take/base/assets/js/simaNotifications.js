/* global sima */

function SIMANotifications()
{
    //koristi se u guitable-u pregleda svih notifikacija kada se klikne na dugme da se procitaju sve neprocitane notifikacije za trenutnog korisnika
    this.markAllAsRead = function() {
        sima.ajax.get('base/notification/markAllAsReadForCurrUser', {async: true});
    };
    
    //koristi se u multiselect opcijama guitable-a pregleda svih notifikacija kada se odabere check ili uncheck
    this.markCheckedNotificationsAsReadOrUnread = function(table_id, is_seen) {
        var notification_ids = $('#' + table_id).simaGuiTable('getSelectedIds');
        sima.ajax.get('base/notification/markNotificationsAsReadOrUnread', {
            data: {
                notification_ids: notification_ids
            },
            get_params: {
                is_seen: is_seen
            },
            async: true
        });
    };
    
    //koristi se u guitable-u pregleda svih notifikacija kada se klikne pojedinacno na notifikaciju da se procita
    this.onGUITableCheckBoxToggle = function(notif_id, is_seen) {
        var action = is_seen ? 'base/notification/notifUnRead' : 'base/notification/notifRead';
        sima.ajax.get(action, {
            get_params: {
                id: notif_id
            },
            async: true
        });
    };
}

