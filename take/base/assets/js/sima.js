/* global moment */

function SIMA(params)
{
    var base_url = params.base_url;
    var _session_id = params.session_id;
    var date_format = params.date_format;
    var datetime_format = params.datetime_format;
    var app_name = params.app_name;
    var use_new_repmanager = params.use_new_repmanager;
    var use_vue_components = params.use_vue_components;
    var session_key = params.session_key;
    var show_unconfirmed_days_warn = params.show_unconfirmed_days_warn;
    var show_unconfirmed_days_warn_interval = params.show_unconfirmed_days_warn_interval;
    var tolerable_unconfirmed_days = params.tolerable_unconfirmed_days;
    var model_displayhtml_click_open_conf = params.model_displayhtml_click_open_conf;
    var curr_user_id = params.curr_user_id;
    var current_language = params.current_language;
    var ajax_sync_action_consolelog = params.ajax_sync_action_consolelog;
    var ajax_sync_action_autobaf = params.ajax_sync_action_autobaf;
    var _init_functions = [];
    var _scrollbar_width = null;
    
    this.getBaseUrl = function() 
    {
        return base_url;
    };

    this.getAppName = function() 
    {
        return app_name;
    };
    
    this.getPageTitle = function() 
    {
        return params.page_title;
    };
    
    this.getUseNewRepmanager = function()
    {
        return use_new_repmanager;
    };
    
    this.getUseVueComponents = function()
    {
        return use_vue_components === true;
    };
    
    this.getSessionKey = function()
    {
        return session_key;
    };
    
    /// treba samo za ne-vue warn-ove
    this.getShowUnconfirmedDaysWarn = function()
    {
        return show_unconfirmed_days_warn;
    };
    
    this.getShowUnconfirmedDaysWarnInterval = function()
    {
        return show_unconfirmed_days_warn_interval;
    };
    
    this.getTolerableUnconfirmedDays = function()
    {
        return tolerable_unconfirmed_days;
    };
    
    this.getModelDisplayhtmlClickOpenConf = function()
    {
        return model_displayhtml_click_open_conf;
    };
    
    this.getCurrentUserId = function()
    {
        return curr_user_id;
    };
    
    this.getCurrentLanguage = function()
    {
        return current_language;
    };
    
    this.getAjaxSyncActionConsoleLog = function()
    {
        return ajax_sync_action_consolelog;
    };
    
    this.getAjaxSyncActionAutoBaf = function()
    {
        return ajax_sync_action_autobaf;
    };

    this.getConfigNewCommentUnread = function()
    {
        return params.config_new_comment_unread;
    };
    
    this.getNumericSeparators = function()
    {
        return params.numeric_separators;
    };
    
    this.getTinymceDefaultParams = function()
    {
        return params.tinymce_default_params;
    };
    
    this.useNewLayout = function()
    {
        return params.use_new_layout;
    };
    
    this.getCurrentCompanyId = function()
    {
        return params.current_company_id;
    };
    
    this.getAllWarnings = function()
    {
        return params.all_warnings;
    };
    
    this.getDatetimeFormat = function()
    {
        return params.datetime_format;
    };
    
    this.useCommentsVue = function()
    {
        return params.use_comments_vue;
    };
    
    this.haveDocWiki = function()
    {
        return params.have_doc_wiki;
    };

    this.translations = [];
    this.addTranslations = function(translations)
    {
        sima.translations = $.extend({}, sima.translations, translations);
    };
    
    this.translate = function(code, params)
    {
        var translations = sima.translations;
        
        var result = code;
        
        if(!sima.isEmpty(translations[code]))
        {
            result = translations[code];
            
            if(!sima.isEmpty(params))
            {
                $.each(params, function(key, value){
                    result = result.split(key).join(value);
                });
            }
        }
        
        return result;
    };

    var id_counter = 1;
    this.uniqid = function() {
        return id_counter++;
    };
    
    this.today = function()
    {
        var date = new Date();
        return date.toLocaleDateString('sr-SR').replace(/\s/g, '');
    };
    
    this.getTabSessionID = function()
    {
        return _session_id;
    };
    
    this.inDOM = function(jquery_object)
    {
        return document.contains(jquery_object[0]);
    };
    

    this.functional_elements = ['sima-guitable','sima-ui-tabs-panel', 'ui-dialog', 'sima-main-tabs-panel', 'sima-layout-display-panel'];
    this.active_functional_elements = [];    
    
    this.getLastActiveFunctionalElement = function()
    {
        sima.checkActiveFunctionalElements();
        var _active_functional_elements = sima.active_functional_elements;
        var reverse = _active_functional_elements.slice().reverse();
        var founded = null;
        $.each(reverse, function(index, value) {
            if (founded === null && value.is(':visible'))
            {                
                founded = value;
            }
        });
        
        return founded;
    };
    
    this.pushActiveFunctionalElement = function(element)
    {        
        sima.removeActiveFunctionalElement(element);                       
        var _active_functional_elements = sima.active_functional_elements;
        _active_functional_elements.push(element);        
    };
    
    this.removeActiveFunctionalElement = function(element)
    {        
        var old_active_functional_elements = sima.active_functional_elements;
        var new_active_functional_elements = [];
        $.each(old_active_functional_elements, function(index, value) {
            if (typeof value !== 'undefined')
            {
                if (value.attr('id') !== element.attr('id'))
                {
                    new_active_functional_elements.push(value);
                }
            }
        });
        sima.active_functional_elements = new_active_functional_elements;
    };
    
    this.checkActiveFunctionalElements = function()
    {        
        var active_functional_elements = sima.active_functional_elements;        
        $.each(active_functional_elements, function(index, value) {            
            if (typeof value !== 'undefined' && value.length === 0 && !sima.isElementInDom(value))
            {
                sima.removeActiveFunctionalElement(value);
            }
        });
    };
    
    this.getDate = function(date, is_datetime)
    {
        if (typeof is_datetime !== 'undefined')
            return moment(date, datetime_format, 'sr')._d;
        else
            return moment(date, date_format, 'sr')._d;
    };
    
    var sima = this;
    this.ajax = null; //placeholder for SIMAAjax
    this.dialog = null; //placeholder for SIMADialog
    this.model = null; //placeholder for SIMAModel
    this.mainTabs = null; //placeholder for SIMAModel
    this.notifications = null; //placeholder for SIMANotifications
    this.file = null; //placeholder for SIMAFile
    this.events = null; //placeholder for SIMAEvents
    this.icons = null; //placeholder for SIMAIcons
    this.temp = null; //placeholder for SIMATemp
    this.ajaxLong = null; //placeholder for SIMAAjax
    this.warns = null; //placeholder for SIMAWarns
    this.keysPressed = null;
    this.warnsVue = null;
    this.notificationsVue = null;
    
    this.isF5Pressed = false; //slucaj ako je f5 pritisnut
    
    /**
     * Initialize function
     */
    this.init = function() 
    {
        $(document.body).on("keydown", this, function (event) {
            if (event.keyCode === 116)
            {
                sima.isF5Pressed = true;
            }
        });
        
        window.addEventListener("dragover",function(e){
            e = e || event;
            e.preventDefault();
        },false);
        window.addEventListener("drop",function(e){
            e = e || event;
            e.preventDefault();
        },false);
        window.addEventListener('error', function(error) {
            var real_error = error.error;
            var message = 'error message: '+real_error.message 
                                    + '</br>filename: '+real_error.fileName
                                    + '</br>line number: '+real_error.lineNumber
                                    + '</br>stack trace: '+error.error.stack;
            console.log(message);
//            sima.addError('Error', message);
            return false;
        });
        
        sima.misc = new SIMAMisc();
        sima.ajax = new SIMAAjax();
        sima.dialog = new SIMADialog();
        sima.model = new SIMAModel();
        sima.mainTabs = new SIMAMainTabs();
//        if(this.getUseVueComponents() === false)
//        {
            /// ne moze da se izbaci zbog public f-ja koje koriste drugi delovi sistema, 
            /// a nemaju striktno veze sa ovim objektima
            ///     sima.warns.renderWarnsForModel
            ///     ...
            sima.notifications = new SIMANotifications();
            sima.warns = new SIMAWarns();
//        }
        sima.events = new SIMAEvents();
        sima.icons = new SIMAIcons();
        sima.temp = new SIMATemp();
        sima.date = new SIMADate(date_format, datetime_format);
        sima.config = new SIMAConfig();
        sima.ajaxLong = new SIMAAjaxLong();
        sima.layout = new SIMALayout();
        sima.dependentFields = new SIMADependentFields();
        sima.dependentFields.init();
        sima.errorReport = new SIMAErrorReport();
        sima.theme = new SIMATheme();
        sima.modelForm = new SIMAModelForm();
        sima.modelForm.init();
        sima.helpers = new SIMAHelpers();
        sima.helpers.init();
        this.keysPressed = new SIMAKeysPressed();
        sima.vue = new SIMAVue();
        sima.vue.init();
        sima.gantt = new SIMAGantt();
        sima.gantt.init();

        //pamtimo funkcionalni element koji je poslednji kliknut
        $(document).click(function(event) {
            var functional_elements = sima.functional_elements;
            var founded = false;
            $.each(functional_elements, function(index, value) {
                var search_element = $(event.target).parents('.'+value);
                if (search_element.length > 0)
                {
                    if (!founded)
                    {
                        founded = true;
                        window.last_functional_element_clicked = search_element;
                    }
                }
            });
            sima.checkActiveFunctionalElements();
        });
        
        $(document).click(function(event) {
            window.last_element_clicked = $(event.target);
        });
        
        $('body').on('click', '.sima-image-file-preview', function(e){
            sima.dialog.openActionInDialog('filePreview/previewDialogContent', $(this).data('filePreviewParams'));
        });
        
        //kod koji sluzi da se u mainmenu-iju pozove klik na a i kada se klikne na li(ne mora da se klikne tacno na naziv, moze i na ceo li)
        $('#top_bar .mm').on('click', 'ul > li', function(event) {           
            var li = $(this);
            var a = li.children('a');            
            
            if (typeof a.attr('href') !== 'undefined' && !a.is(event.target) && li.is(event.target))
            {                
                a[0].click();
            }
            event.stopPropagation();
        });
        
        for(var i in _init_functions)
        {
            _init_functions[i]();
        }
        
        sima.errorReport.init();
        if (!sima.useNewLayout())
        {
            sima.mainTabs.init();
        }
        sima.layout.init(); //mora na kraju da bi layout bio kako treba (poziva se trigger)
        
//        postavljamo jezik za js moment biblioteku kako bi svuda u kodu mogli da koristimo bez ponovnog postavljanja jezika
        moment.locale(sima.getCurrentLanguage());
        setTimeout(function(){
            //cuvanje rezolucije i zoom-a u sesiji
            sima.ajax.get('base/base/updateSessionParams', {
                get_params: {
                    width_resolution: $(window).width(),
                    height_resolution: $(window).height(),
                    zoom: Math.round(window.devicePixelRatio * 100)
                },
                async: true
            });
        }, 3000);
    };
    
    this.addInitFunction = function(_func){
        _init_functions.push(_func);
    };
    
    this.set_html = function(obj,html,source)
    {
        source = source || 'TRIGGER_sima_set_html';
        obj.html(html);
        var trigger_on = obj.children('.sima-layout-panel');
        if (trigger_on.length > 1)
        {
            console.warn('VISE OD 1 panel za pozivanje trigger-a');
            console.warn(source);
            console.log(trigger_on);
        }
        else if (trigger_on.length === 0)
        {
            console.warn('NEMA nad cime da se zove trigger u set_html nad objektom:');
            console.warn(obj);
        }
        else
        {
            trigger_on.trigger('sima-layout-allign', [obj.height(),obj.width(), source]);
        }
    };
    
    
    
    this.isReloaded = true; //promenljiva u kojoj se pamti da li je sima refresovan
    
    //funkcija koja hvata refresh sime
    $('body').bind('beforeunload',function(){
        sima.isReloaded = true;
    });
    
    this.getScreenWidth = function() {
        return window.innerWidth;
    };
    this.getScreenHeight = function()
    {
        return window.innerHeight;
    };
    
    this.selectSubObj = function(object, select)
    {        
        if (typeof select === 'undefined' || sima.isEmpty(select))
            return;
        
        if (typeof select === 'string')
        {
            select = JSON.parse(select);
        }
        
        if (typeof(select.length) === 'undefined')
            return;
        
        var new_select = new Array();
        
        for(var i=0; i<select.length; i++)
        {
            if (i > 0)
                new_select[i-1] = select[i];
        }

        if (typeof select[0] !== 'undefined')
        {
            var select_obj;
            var select_obj_index;
            $.each(select[0], function(i, item) {
                select_obj_index = i;
                select_obj = item;
                return false;
            });
            
            var obj;
            if (select_obj_index === 'simaTabs')
            {
                obj = object.find('.sima-ui-tabs-panel:first').parent();
                obj1 = object.find('.sdl-tabs:first'); //podrska za novi simaDefaultLayout
                obj2 = object.find('.sima-tabs-vue:first'); //podrska za vue simaDefaultLayout
                
                if (!sima.isEmpty(obj2))
                {
                    var obj2_vue = sima.vue.getVueComponentFromObj(obj2);
                    if (obj2_vue !== null)
                    {
                        obj2_vue.selectTabByCode(select_obj);
                        setTimeout(function() {
                            sima.selectSubObj(object.find('.sima-tab-vue-content:visible:first'), new_select);
                        }, 200);
                    }
                }
                else if (!sima.isEmpty(obj1))
                {
                    obj1.simaTree2('selectItem', select_obj);
                    setTimeout(function() {
                        sima.selectSubObj(object.find('.sdl-rp-tab-content:visible:first'), new_select);
                    }, 200);
                }
                else if (!sima.isEmpty(obj))
                {
                    obj.simaTabs('select_tab', select_obj);
                    setTimeout(function() {
                        sima.selectSubObj(object.find('.sima-ui-tabs-panel.sima-ui-active:first'), new_select);
                    }, 200);
                }
            }
            else if (select_obj_index === 'commentThread')
            {                
                obj = object.find('.sima-comments:first');
                obj_vue = object.find('.sima-comments-vue:first');
                if (obj.length > 0)
                {
                    obj.simaComments('jumpToTheme', select_obj);
                }
                else if (obj_vue.length > 0)
                {
                    var obj_vue_obj = sima.vue.getVueComponentFromObj(obj_vue);
                    obj_vue_obj.selectCommentThread(select_obj);
                }
            }
            else if (select_obj_index === 'guiTable')
            {
                obj = object.find('.sima-guitable:first');
                if (typeof obj !== 'undefined')
                {
                    obj.simaGuiTable('setFixedFilter', select_obj, true); //true je da se odradi repopulate - potrebno kada je tab vec otvoren
                }
            }
            else if (select_obj_index === 'guiTableRepopulate')
            {
                obj = object.find('.sima-guitable:first');
                if (typeof obj !== 'undefined')
                {
                    obj.simaGuiTable('populate');
                }
            }
        }
    };
    
    
    
    /**
     * nove funkcije, ali nerasporedjene
     */

     /**
      * 
      * @param {type} param
      * @returns {undefined}
      */
    function resetPasswordYesFunction(param)
    {
        sima.ajax.get('admin/admin/resetPass', {
            get_params: {user_id: param.data.user_id},
            success_function: function() {
                sima.dialog.close();
            }
        });
    }
    ;

    this.resetPassword = function(user_id)
    {
        sima.dialog.openYesNo('Da li zaista zelite da resetujete sifru?', resetPasswordYesFunction, null, {user_id: user_id});
    };

//    function changePasswordYesFunction(param)
//    {
//        sima.ajax.get('admin/admin/resetPass',{
//            get_params:{user_id:param.data.emp_id},
//            success_function:function(){sima.dialog.close();}
//        });
//    };
//    
//    this.changePassword = function(emp_id)
//    {
//        sima.model.openForm()
//    };



    /**
     * PREGLEDATI!!!!!
     */

    /**
     * 
     * @param {type} str
     * @returns {Boolean}
     */
    var IsJsonString = function(str) {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    };
    this.IsJsonString = IsJsonString;

    var deleteTag = function(tag)
    {
        var target = $("#" + tag);
        target.hide('400', function() {
            target.remove();
        });
        var target = $("." + tag);
        target.hide('400', function() {
            target.remove();
        });
    };
    this.deleteTag = deleteTag;

    this.deleteID = function(data)
    {
        if (data.status === 'success')
        {
            deleteTag(data.deleteDivID);
        }
        else
        {
            sima.dialog.openInfo(data.message);
        }
    };


    /**
     * funkcija vraca random niz tokena, koristi se pri postavljanju naziva slika referenci
     * @param {type} size
     * @returns {String}
     */
    this.createRandomToken = function(size) {
        var chars = "abcdefghijkmnopqrstuvwxyz0123456789";
        var i = 0;
        var token = '';
        while (i < size) {
            var num = Math.floor(Math.random() * 35);
            var tmp = chars.charAt(num);
            token = token + tmp;
            i++;
        }
        return token;
    };


    /**
     * funkcija koja rezervise novi zavodni broj u zavodnoj knjizi - poziva se iz zavodne knjige
     * @param {type} id
     */
    this.filing_reserve = function(id)
    {
        sima.ajax.get('legal/filingBook/reserve', {
            success_function: function(response) {
                $(id).simaGuiTable('putInFilter', 'filing_number', response.filing_number);
                $(id).simaGuiTable('populate');
                sima.dialog.closeYesNo();
            }
        });

    };

    /**
     * funkcija koja rezervise fajl/fajlove za skeniranje
     * @param {type} id
     * @param {type} value
     */
    this.orderScanModelTag = function(id, value)
    {
        var orders = [];
        if (typeof value !== 'undefined')
        {
            var order = {};
            order.id = id;
            order.value = value;
            orders.push(order);
        }
        else
        {
            $('#'+id).data('_body').find('tr.selected').each(function(){
                var order = {};
                order.id = $(this).attr('model_id');
                if ($(this).attr('order_scan') === 'true')
                {
                    order.value = false;
                }
                else
                {
                    order.value = true;
                }
                orders.push(order);
                $(this).removeClass('selected');
            }); 
        }

        sima.ajax.get('site/orderScan', {
            data: {orders:orders},
            async: true,
            loadingCircle: $('#body_sync_ajax_overlay'),
            success_function:function(response){
                $('#'+id).simaGuiTable('disableMultiSelect');
            }
        });
    };

    this.logout = function()
    {
        var logout = function(){window.location.href = sima.getBaseUrl() + '?session_key='+sima.getSessionKey();};
        sima.ajax.get('user/logout',{
            success_function:logout,
            failure_function:logout,
            error_function:logout,
            report_error: false,
            is_idle_since: false
        });
    };
    
    var mail_windows = [];
    var current_account = null; //-1 je ulogovani korisnik
    
    this.mailTab = function(account_id)
    {
        if (current_account!==account_id)
        {
            for (var i in mail_windows)
            {
                mail_windows[i].close();
            }
            mail_windows = [];
            current_account = account_id;
        }
        sima.ajax.get('site/mailTab',{
            get_params:{
                action: 'login',
                account_id: account_id
            },
            async: false,
            loadingCircle: $('body'),
            success_function: function(response){
                var win=window.open(response.path, '_blank');
                if (win === null)
                {
                    sima.dialog.openWarn(sima.translate('TurnOffPopupBlocker'));
                }
                else
                {
                    mail_windows.push(win);
                    win.focus();
                }
            }
        });
        
    };

    this.showLoading = function(obj, loadingCircleDelay)
    {
        if(typeof loadingCircleDelay === 'undefined')
        {
            loadingCircleDelay = 500;
        }
        
        var ret = {
            settimeout_id: null,
            loadingObjs: null
        };
         
        if (loadingCircleDelay !== 0)
        {
            ret.settimeout_id = setTimeout(function() {
                ret.loadingObjs = _showLoading(obj);
            }, loadingCircleDelay);
        }
        else
        {
            ret.loadingObjs = _showLoading(obj);
        }
        
        return ret;
    };
    
    function _showLoading(obj)
    {
        var loadingObjects = [];
        
        if (typeof obj === 'undefined' || obj === null || obj.length <= 0)
        {
            obj = $('#body_sync_ajax_overlay');
            obj.show();
        }
        
        for (var i=0; i<obj.length; i++)
        {
            var el = $(obj[i]);
            if (el.length > 0)
            {
                el.each(function(){
                    var _this = $(this);
                    
                    var loading_circle_id = 'loading_circle_'+sima.uniqid();
                    
                    var newobj = $("<div id='"+loading_circle_id+"' class='sima-loading-circle'></div>");
                    newobj.css({opacity: 0});
                    
                    var new_z = 'auto';
                    if(_this.zIndex() !== 'auto')
                    {
                        new_z = el.zIndex();
                        new_z += 10;
                    }
                    newobj.zIndex(new_z);
                    
                    var new_left = _this.position().left;
                    var new_top = _this.position().top;
                    var new_width = _this.outerWidth(true);
                    var new_height = _this.outerHeight(true);
                    
                    newobj.css({left:new_left, top:new_top, width:new_width, height:new_height});
                    _this.after(newobj);                    
                    newobj.css({opacity: 1});
                    newobj.animate({opacity: 1},200);
                    
                    var loading_circle_ids = _this.data('loading_circle_ids');
                    if(sima.isEmpty(loading_circle_ids))
                    {
                        loading_circle_ids = [];
                    }
                    loading_circle_ids.push(loading_circle_id);
                    _this.data('loading_circle_ids', loading_circle_ids);
                    
                    loadingObjects.push(newobj);
                });
            }
        }
        
        return loadingObjects;
    }
    
    this.hideLoading = function(obj, loadingData)
    {
        if (typeof obj === 'undefined' || obj === null || obj.length <= 0)
        {
            obj = $('#body_sync_ajax_overlay');
            obj.hide();
        }
        
        if(!sima.isEmpty(loadingData))
        {
            if(!sima.isEmpty(loadingData.settimeout_id))
            {
                clearTimeout(loadingData.settimeout_id);
            }

            if(!sima.isEmpty(loadingData.loadingObjs))
            {
                for (var i=0; i<loadingData.loadingObjs.length; i++)
                {
                    var loadingObj = loadingData.loadingObjs[i];
                    var loading_circle_id = loadingObj.attr('id');
                    
                    for (var j=0; j<obj.length; j++)
                    {
                        var el = $(obj[j]);
                        var loading_circle_ids = el.data('loading_circle_ids');
                        if (typeof loading_circle_ids !== 'undefined')
                        {
                            var index = loading_circle_ids.indexOf(loading_circle_id);
                            if(index !== -1)
                            {
                                delete loading_circle_ids[index];
                                el.data('loading_circle_ids', loading_circle_ids);
                            }
                        }
                    }
                    loadingObj.remove();
                }
            }
        }
    };
    
    this.htmlEntities = function(str) 
    {
        return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;').replace(/'/g, '&apos;');
    };
    
    this.startProgressBar = function(obj, progressBarDelay)
    {
        if(typeof progressBarDelay === 'undefined')
        {
            progressBarDelay = 1;
        }
        
        var ret = {
            settimeout_id: null,
            progressBarObjs: null
        };
         
        if (progressBarDelay !== 0)
        {
            ret.settimeout_id = setTimeout(function() {
                ret.progressBarObjs = _startProgressBar(obj);
            }, progressBarDelay);
        }
        else
        {
            ret.progressBarObjs = _startProgressBar(obj);
        }
        
        return ret;
    };
    
    function _startProgressBar (obj)
    {        

        var progressObjects = [];
        
        for (var i=0; i<obj.length; i++)
        {
            var el = $(obj[i]);
            if (el.length > 0)
            {
                el.each(function(){
                    var _this = $(this);
                    
                    var progress_bar_id = 'progressBar'+sima.uniqid();

                    var progressBar = $("<div id='"+progress_bar_id+"' class='sima-progressBar'></div>");
                    var progressBarLabel = $("<div class='progress-label'></div>");
                    progressBar.progressbar({
                        value: 0,
                        change: function() {
                            progressBarLabel.text(progressBar.progressbar("value") + "%");
                        }
                    });
                    progressBar.height(15);
                    progressBar.append(progressBarLabel);
                    obj.show().append(progressBar);
                    
                    var progress_bar_ids = _this.data('progress_bar_id');
                    if(sima.isEmpty(progress_bar_ids))
                    {
                        progress_bar_ids = [];
                    }
                    progress_bar_ids.push(progress_bar_ids);
                    _this.data('progress_bar_ids', progress_bar_ids);
                    
                    progressObjects.push(progressBar);
                });
            }
        }
        
        return progressObjects;
    };
    
    this.endProgressBar = function(obj, progressData)
    {
        if (typeof obj === 'undefined' || obj === null || obj.length <= 0)
        {
            obj = $('#body_sync_ajax_overlay');
            obj.hide();
        }
        
        if(!sima.isEmpty(progressData))
        {
            if(!sima.isEmpty(progressData.settimeout_id))
            {
                clearTimeout(progressData.settimeout_id);
            }

            if(!sima.isEmpty(progressData.progressBarObjs))
            {
                for (var i=0; i<progressData.progressBarObjs.length; i++)
                {
                    var progressBarObj = progressData.progressBarObjs[i];
                    var progress_bar_id = progressBarObj.attr('id');
                    
                    for (var j=0; j<obj.length; j++)
                    {
                        var el = $(obj[j]);
                        var progress_bar_ids = el.data('progress_bar_ids');
                        if (typeof progress_bar_ids !== 'undefined')
                        {
                            var index = progress_bar_ids.indexOf(progress_bar_id);
                            if(index !== -1)
                            {
                                delete progress_bar_ids[index];
                                el.data('progress_bar_ids', progress_bar_ids);
                            }
                        }
                    }
                    progressBarObj.remove();
                }
            }
        }
    };
    
    this.showNewMessagesCountInTitle = function(messages_cnt = null, notification_cnt = null)
    {
        if (typeof $('title').data('messages_cnt') === 'undefined')
        {
            $('title').data('messages_cnt', 0);
        }
        if (typeof $('title').data('notification_cnt') === 'undefined')
        {
            $('title').data('notification_cnt', 0);
        }
        
        if (messages_cnt !== null)
        {
            $('title').data('messages_cnt', messages_cnt);
        }
        
        if (notification_cnt !== null)
        {
            $('title').data('notification_cnt', notification_cnt);
        }

        var number = parseInt($('title').data('messages_cnt')) + parseInt($('title').data('notification_cnt'));
        if (number > 0)
        {
            $('title').text('(' + number + ') '+sima.getPageTitle());
        }
        else
        {
            $('title').text(sima.getPageTitle());
        }
    };
    
    this.refreshActiveTab  = function(obj)
    {
        var obj = $(obj); //za svaki slucaj, jer mozda je obj prosledjen kao selektor string(npr ako dolazi iz php-a)
        if (!sima.isEmpty(obj.parents(".sima-ui-tabs:first")))
        {
            obj.parents(".sima-ui-tabs:first").simaTabs("select_tab", obj.parents(".sima-ui-tabs:first").find(".sima-ui-tabs-nav .sima-ui-active").index()+1, true);
        }
        else if (!sima.isEmpty(obj.parents('.sdl:first')))
        {
            obj.parents('.sdl:first').simaDefaultLayout('refreshSelectedTabContent');
        }
        else
        {
            sima.misc.refreshMainTabForObject(obj);
        }
    };
    
    this.openInNewBrowser = function(link)
    {
        window.open(link,'_blank');
    };
    
    this.getTag = function(model_name_tag, model_id)
    {
        return model_name_tag+'_'+model_id;
    };
    
    this.addError = function(action, message)
    {
        if(sima.isEmpty(action))
        {
            action = 'sima.addError';
            message = 'action empty\n';
        }
        if(sima.isEmpty(message))
        {
            action = 'sima.addError';
            message = 'message empty\n';
        }
        var err = new Error();
        message += '</br>'+err.stack;
        
        sima.errorReport.addError({
            action: action,
            message: message.replace(/'/g, '').replace(/"/g, '')
        });
    };
    
    this.notImplemented = function(message)
    {
        var final_message = sima.translate('Not implemented');
        
        if(typeof message !== 'undefined' && message !== null && message.length>0)
        {
            final_message += ':</br>' + message;
        }
        
        sima.dialog.openWarn(final_message);
    };
    
    /**
     * Zove funkciju sa prosledjenim parametrima. Parametri(params) se primaju kao niz, a funkciji(func) se prosleduju kao argumenti.
     * Znaci ako je niz parametara [param1, param2] onda ce se funkcija pozvati na sledeci nacin: func(param1, param2)
     * @param {string/array/func} func
     * @param {array} params
     */
    this.executeFunction = function(func, params)
    {
        if (typeof params === 'undefined')
        {
            params = [];
        }
        else if (!$.isArray(params))
        {
            params = [params];
        }
        
        if(typeof func === 'string')
        {
            func = [func];
        }
        
        if($.isArray(func) || $.isPlainObject(func))
        {
            var extended_func = $.extend(true, [], func);
            $.each(params, function(index, value) {
                extended_func.push(value);
            });
            sima.callFunctionFromArray(extended_func);
        }
        else if (typeof func === 'function')
        {
            func.apply(this, params);
        }
    };
    
    this.callFunction = function(func, params)
    {
        if(typeof func === 'string')
        {
            if (typeof params !== 'undefined')
            {
                eval(func+'('+params+')');
            }
            else
            {
                eval(func);
            }
        }
        else if($.isArray(func))
        {
            /**
             * func - kada je func array, trebao bi imati samo jednostavne tipove (string, int)
             * jer ce kloniranje da poremeti ako su u pitanju jquery/dom objekti
             */
            var parsed_func = sima.clone(func);
            parsed_func.push(params);
            sima.callFunctionFromArray(parsed_func);
        }
        else if (typeof func === 'function')
        {       
            func(params);
        }
    };
    
    this.callFunctionFromArray = function(func_as_array)
    {
        if (func_as_array.length < 1)
        {
            return;
        }

        var _func = func_as_array[0];
        const typeof_func = typeof _func;
        if(typeof_func === 'object' || typeof_func === 'array')
        {
            var err = new Error();
            const error_message = 'funkcija nije string vec je '+typeof_func
                                    +' </br>- _func: '+JSON.stringify(_func)
                                    +' </br>- func_as_array: '+JSON.stringify(func_as_array)
                                    +' </br>- stack: '+JSON.stringify(err.stack);
            const autobafreq_theme_id = 3181;
            const autobafreq_theme_name = 'AutoBafReq sima.callFunctionFromArray funkcija nije string';
            sima.errorReport.createAutoClientBafRequest(error_message, autobafreq_theme_id, autobafreq_theme_name);
            return;
        }
        var methods = _func.split(".");
        var result = window;
        for(var i in methods) {
            result = result[methods[i]];
        }
        func_as_array.shift();
        if (!sima.isEmpty(result))
        {
            result.apply(this,func_as_array);
        }
    };
    
    this.callPluginMethod = function()
    {
        var ar = arguments;
        var obj_selector = ar[0];
        var plugin_name = ar[1];
        var plugin_method = ar[2];

        var func_string = '';        
        if (ar.length > 3)
        {           
            var params_as_string = '';
            $.each(ar, function(index, value) {
                if (index > 2)
                {                    
                    if (typeof value === 'number' || typeof value === 'boolean')
                    {
                        params_as_string += value;
                    }
                    else if (typeof value === 'object')
                    {
                        params_as_string += JSON.stringify(value);
                    }
                    else
                    {
                        params_as_string += '"'+value+'"';
                    }
                    
                    if (index < ar.length-1)
                    {
                        params_as_string += ',';
                    }
                }
            });            
            func_string = '$("'+obj_selector+'").'+plugin_name+'("'+plugin_method+'",'+params_as_string+')';
        }
        else
        {
            func_string = '$("'+obj_selector+'").'+plugin_name+'("'+plugin_method+'")';
        }

        eval(func_string);        
    };
    
    //  discuss at: http://phpjs.org/functions/empty/
    // original by: Philippe Baumann
    //    input by: Onno Marsman
    //    input by: LH
    //    input by: Stoyan Kyosev (http://www.svest.org/)
    // bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // improved by: Onno Marsman
    // improved by: Francesco
    // improved by: Marc Jansen
    // improved by: Rafal Kukawski
    //   example 1: empty(null);
    //   returns 1: true
    //   example 2: empty(undefined);
    //   returns 2: true
    //   example 3: empty([]);
    //   returns 3: true
    //   example 4: empty({});
    //   returns 4: true
    //   example 5: empty({'aFunc' : function () { alert('humpty'); } });
    //   returns 5: false
    this.isEmpty = function(mixed_var) 
    {       
        var undef, key, i, len;
        var emptyValues = [undef, null, false, 0, '', '0'];
        
        if (typeof mixed_var === 'string')
        {
            mixed_var = mixed_var.trim();
        }
        
        for (i = 0, len = emptyValues.length; i < len; i++) 
        {
            if (mixed_var === emptyValues[i]) 
            {
                return true;
            }
        }

        if (typeof mixed_var === 'object') 
        {
            if (mixed_var.length === 0)
            {
                return true;
            }
            for (key in mixed_var) 
            {                
                // TODO: should we check for own properties only?
                //if (mixed_var.hasOwnProperty(key)) {
                return false;
                //}
            }
            return true;
        }

        return false;
    };
    
    this.consoleLogStackTrace = function()
    {
        var err = new Error();
        console.log(err.stack);
    };
    
    this.getStackTrace = function()
    {
        var err = new Error();
        
        return err.stack;
    };
    
    this.openLink = function(url, attributes)
    {        
        var append_to_el = document.body;
        var last_active_functional_element = sima.getLastActiveFunctionalElement();
        if (!sima.isEmpty(last_active_functional_element))
        {
            append_to_el = last_active_functional_element[0];
        }

        var element = document.createElement('a');
        element.setAttribute('href', url);
        if (typeof attributes !== 'undefined')
        {
            $.each(attributes, function(key, value) {
                element.setAttribute(key, value);
            });
        }

        element.style.display = 'none';
        append_to_el.appendChild(element);        
        element.click();
        append_to_el.removeChild(element);
    };
    
    this.isElementInDom = function(el)
    {
        return jQuery.contains(document, el[0]);
    };

    this.isHTML = function(str) 
    {
        var a = document.createElement('div');
        a.innerHTML = str;
        for (var c = a.childNodes, i = c.length; i--; ) 
        {
            if (c[i].nodeType == 1) return true; 
        }
        
        return false;
    };
    
    /**
     * Easiest way to deep clone Array or Object
     * @param {type} elem
     * @returns {undefined}
     */
    this.clone = function(elem)
    {
        return JSON.parse(JSON.stringify(elem));
    };
    
    this.isChromeBrowser = function()
    {
        return /chrom(e|ium)/.test(navigator.userAgent.toLowerCase());
    };
    
    /**
     * za prosledjeni element proverava da li ima definisan plugin
     * @param {object} element
     * @param {string} plugin
     * @returns {Boolean}
     */
    this.elementHasPlugin = function(element, plugin)
    {
        var result = false;
        if(typeof element === 'object' && typeof element[plugin] === 'function')
        {
            result = true;
        }
        return result;
    };
    
    /**
     * Ekvivalent php funkciji str_split()
     * @param {string} string
     * @param {integer} length
     * @returns {Array} 
     */
    this.strSplit = function(string, length)
    {
        if (string === null || length < 1) 
        {
            return false;
        }

        var chunks = [];
        var pos = 0;
        var len = string.length;

        while (pos < len) 
        {
            chunks.push(string.slice(pos, pos += length));
        }

        return chunks;
    };
    
    /**
     * funkcija koja prvo proverava da li je neka notifikacija otvorena u nekom tabu, ako jeste onda je otvara u tabu, a ako nije onda u dialogu
     * @param {string} action - akcija
     * @param {int} action_id
     * @param {obj} selector
     * @param {obj} params
     * @returns {undefined}
     */
    this.openAction = function(action, action_id, selector, params)
    {
        var isOpened = true;
        if (!sima.useNewLayout())
        {
            isOpened = sima.mainTabs.isTabOpened(action, action_id);
        }

        if (isOpened === true)
        {            
            sima.mainTabs.openNewTab(action, action_id, selector, undefined, undefined, undefined, params);
        }
        else
        {            
            sima.dialog.openModel(action, action_id, selector, params);
        }
    };
    
    this.getScrollbarWidth = function() {
        if (_scrollbar_width === null)
        {
            // Creating invisible container
            const outer = document.createElement('div');
            outer.style.visibility = 'hidden';
            outer.style.overflow = 'scroll'; // forcing scrollbar to appear
            outer.style.msOverflowStyle = 'scrollbar'; // needed for WinJS apps
            document.body.appendChild(outer);

            // Creating inner element and placing it in the container
            const inner = document.createElement('div');
            outer.appendChild(inner);

            // Calculating difference between container's full width and the child width
            const scrollbarWidth = (outer.offsetWidth - inner.offsetWidth);

            // Removing temporary elements from the DOM
            outer.parentNode.removeChild(outer);

            _scrollbar_width = scrollbarWidth;
        }

        return _scrollbar_width;
    };
}
