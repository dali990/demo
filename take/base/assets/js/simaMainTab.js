/* global tinyMCE, sima */

function SIMAMainTabs()
{
    var _main_tabs = $("#sima-main-tabs");

    this.getOpenedPanel = function() 
    {
        return _main_tabs.find('.sima-main-tabs-panel:visible');
    };

    var tabs = Array();
    var selected_tab = {};
    var navigation_bar = _main_tabs.find('ul.sima-main-tabs-nav');

    _main_tabs.find('.sima-main-tabs-arrow-down').on('click', function() {
        _main_tabs.find('.sima-main-tabs-list-tabs').fadeToggle("fast", "linear");
    });

    /**
     * kada se klikne van padajuceg menija tabova on se zatvara
     */
    _main_tabs.find('.sima-main-tabs-list-tabs').bind("clickoutside", function(event){
        if (!_main_tabs.find('.sima-main-tabs-arrow-down').is(event.target))
            $(this).hide();
    });
    
    _main_tabs.find('.sima-main-tabs-list-tabs ul').on('click', 'li a', function() {
        _main_tabs.find('.sima-main-tabs-list-tabs').toggle();
        select_tab($(this).parent().index());
    });

    _main_tabs.find('ul.sima-main-tabs-nav').on('click', 'li a', function() {
        select_tab($(this).parent().index());
    });

    var scrolled = false,
    scrollTimeout;
    _main_tabs.find('.sima-main-tabs-nav-wrapper').mousewheel(function(event, delta) {
        event.preventDefault();

        if (!scrolled)
        {
            scrolled = true;
            if (delta > 0)
            {
                _main_tabs.find('.sima-main-tabs-right-scroll').trigger("click");
            }
            else
            {
                _main_tabs.find('.sima-main-tabs-left-scroll').trigger("click");
            }
        }

        clearTimeout(scrollTimeout);

        scrollTimeout = setTimeout(function() {
            scrolled = false;
        }, 200);
    });

    _main_tabs.find('ul.sima-main-tabs-nav').on('click', 'li span.sima-icon-close', function() {
        if ($(this).parent('li').hasClass('starred'))
        {
            $(this).parent('li').removeClass('starred');
            saveStarred();
        }
        remove_tab($(this).parent().index());
        _main_tabs.trigger('closeTab');
    });
    
    _main_tabs.find('ul.sima-main-tabs-nav').on('click', 'li span.sima-icon-refresh', function() {
        select_tab(navigation_bar.find('.sima-ui-active').index(), true);
    });
    
    _main_tabs.find('ul.sima-main-tabs-nav').on('mousemove', 'li', function(event) {
        var Li = $(this);
        var posX = Li.offset().left;
        var posY = Li.offset().top;
        var x = event.pageX - posX;
        var y = event.pageY - posY;
        if (isPointInsideTriangle(0, 0, 18, 0, 0, 18, Math.round(x), Math.round(y)))
        {
            Li.addClass('overStarred');
        }
        else
        {
            Li.removeClass('overStarred');
        }
    });
    
    _main_tabs.find('ul.sima-main-tabs-nav').on('click', 'li', function(event) {
        var Li = $(this);
        var posX = Li.offset().left;
        var posY = Li.offset().top;
        var x = event.pageX - posX;
        var y = event.pageY - posY;
        if (isPointInsideTriangle(0, 0, 18, 0, 0, 18, Math.round(x), Math.round(y)))
        {
            if (!sima.isEmpty(Li.data('code')))
            {
                if (Li.hasClass('starred'))
                {
                    Li.removeClass('starred');
                    saveStarred();
                }
                else
                {
                    Li.addClass('starred');
                    saveStarred();
                }
            }
        }
    });
    
    function isPointInsideTriangle(x1, y1, x2, y2, x3, y3, x, y)
    {
        var A = area(x1, y1, x2, y2, x3, y3);
        var A1 = area(x, y, x2, y2, x3, y3);
        var A2 = area(x1, y1, x, y, x3, y3);
        var A3 = area(x1, y1, x2, y2, x, y);

        if (A == (A1 + A2 + A3))
            return true;
        else
            return false;
    }
    
    function area(x1, y1, x2, y2, x3, y3)
    {
       return Math.abs((x1*(y2-y3) + x2*(y3-y1)+ x3*(y1-y2))/2.0);
    }

    selected_tab = {
        index: -1,
        code: '',
        nav: null,
        panel: null
    };

    tabs.push({
        action: '',
        get_params: '',
        code: '',
        loaded: true
    });

    var start_index;
    _main_tabs.find("ul.sima-main-tabs-nav").sortable({
        axis: "x",
        forcePlaceholderSize: true,
        placeholder: "sima-main-tabs-placeholder",
        cursor: 'move',
        start: function(event, ui) {
            start_index = $(ui.item[0]).index();
        },
        stop: function(event, ui) {
            var end_index = $(ui.item[0]).index();
            if (start_index === 0 || end_index === 0)
            {
                $(this).sortable('cancel');
            }
            else
            {
                arraymove(tabs, start_index, end_index);
                saveStarred();
                reOrderMainTabsList(start_index, end_index);
            }
        }
    }).disableSelection();

    $("#sima-main-tabs").scrolltab({
        ul: 'sima-main-tabs-nav',
        left: 'sima-main-tabs-left-scroll',
        right: 'sima-main-tabs-right-scroll'
    });
    
    var openNewTab = function(action, id, select, color, title, icon, params)
    {
        if (sima.useNewLayout())
        {
            sima.mainLayout.$children[0].openItem({
                label: title,
                action: [action, id],
                selector: select,
                additional_params: params
            });
            return;
        }
        
        var code = action;
        var get_params = {};
        if (typeof id === 'number' || typeof id === 'string')
        {
            code += id;
            get_params = {
                id: id
            };
        }
        else if (typeof id === 'object')
        {
            code += JSON.stringify(id);
            get_params = id;
        }

        if (typeof select === 'undefined')
            select = true;
        else if (typeof select !== 'undefined' && typeof select !== 'boolean')
        {
            var select_params = select;
            select = true;
        }
        if (typeof title === 'undefined')
            title = 'new tab2';
        if (typeof color === 'undefined')
            color = '#FED39E';
        if (typeof icon === 'undefined')
            icon = 'default';
        if (typeof action === 'undefined')
        {
            select_tab(0);
            return;
        }
        var founded = false;
        for (var i=0; i< tabs.length; i++)
        {
            if (findTabByCode(tabs[i].code, action, id) === true)
            {
                founded = true;                
                if (tabs[i].loaded)
                {
                    var panel_index = getTabPanelIndexByTabIndex(i);
                    sima.selectSubObj(_main_tabs.find('div.sima-main-tabs-panel:eq(' + panel_index + ')'), select_params);
                }
                if (typeof select_params !== 'undefined')
                    tabs[i].select = select_params;                    
                select_tab(i);
            }
        }
        
        if (!founded)
        {
            tabs.push({
                action: action,
                get_params: get_params,
                code: code,
                loaded: false,
                select: select_params,
                params: params
            });
            $("#sima-main-tabs ul.sima-main-tabs-nav").append("<li title='"+title+"' data-icon='" + icon +"' data-code='"+code+"'><a>" + title + "</a><span class='sima-icon-refresh'></span><span class='sima-icon-close'></span></li>");
            $("#sima-main-tabs .sima-main-tabs-list-tabs ul").append("<li title='"+title+"'><span class='iconName "+icon+"'></span><a>"+title+"</a></li>");
                
            //TODO(sima-resize): ukinuti medju promenljive
            var _new_panel = $("<div class='sima-main-tabs-panel sima-page' data-code='"+code+"'></div>");
            //Iskoriscava visine otvorenog taba za novi tab
            _new_panel.height(sima.mainTabs.getOpenedPanel().height());
            _new_panel.width(sima.mainTabs.getOpenedPanel().width());
            
            $("#sima-main-tabs").append(_new_panel);
            $("#sima-main-tabs ul.sima-main-tabs-nav li:last").css('background', color);

            if (select)
            {
                select_tab($("#sima-main-tabs ul.sima-main-tabs-nav li:last").index(), false, false);
                $("#sima-main-tabs").trigger('addTab');
            }
            else
            {
                $("#sima-main-tabs").trigger('addTab', true);//true sluzi da se ne prebacuje scroll u tabovima
            }
        }          
    };
    
    this.openNewTab = openNewTab;
    this.select_tab = select_tab;
    this.getTabPanelIndexByTabIndex = getTabPanelIndexByTabIndex;

    function select_tab(index, refresh, triggerSelectTab)
    {
        /** podrska za selektovanje preko code */
        if (typeof index === 'string')
        {
            var new_index = -1;
            for (var i in tabs)
            {
                if (tabs[i].code == index)
                    new_index = i;
            }
            if (new_index == -1)
                return;
            else
                index = parseInt(new_index, 10) + 1;
        }

        var panel_index = getTabPanelIndexByTabIndex(index);

        unselect_tab();
        
        var refreshChildrenForce = false;
        /* loaduje tab ako nije loadovan */
        if (!tabs[index].loaded || refresh)
        {
            var panel = _main_tabs.find('div.sima-main-tabs-panel:eq(' + panel_index + ')');
            var get_params = tabs[index].get_params;
            var post_params = {selector: tabs[index].select};
            var ajax_params = {
                get_params: get_params,
                data: post_params,
                async:true,
                report_error: false,
                loadingCircle: panel.parent(),
                loadingCircleDelay: 0,
                success_function: function(response) {
                    var nav = _main_tabs.find('ul.sima-main-tabs-nav li:eq(' + index + ')');
                    var listTabs = _main_tabs.find('div.sima-main-tabs-list-tabs ul li:eq(' + index + ')');
                    var changed = false;
                    nav.attr('title', response.title);
                    var title = nav.children('a');
                    var listTabsTitle = listTabs.children('a');
                    if (title.html() != response.title)
                    {
                        if (title.html() != 'new tab2')
                        {
                            changed = true;
                        }
                        title.html(response.title).attr('title',response.title);
                        listTabs.children('a').html(response.title).attr('title',response.title);
                        if (typeof response.icon != 'undefined')
                            listTabs.children('span').attr('class', 'iconName '+response.icon);
                        listTabsTitle.html(response.title);
                    }
                    var old_color = nav.css('background-color');  
                    nav.css('background', response.color);

                    if (nav.css('background-color') != old_color)
                    {
                        nav.css('background-color', response.color);
                    }
                    if (changed)
                        saveStarred();
                    
                    sima.set_html(panel, response.html);
                    
                    tabs[index].loaded = true;                    
                    if ((typeof response.selected === 'undefined') || (!response.selected))
                    {                        
                        setTimeout(function() {                            
                            if (typeof tabs[index].select !== 'undefined' && refresh !== true)
                            {                                
                                sima.selectSubObj(panel, tabs[index].select);
                            }
                        }, 200);
                    }
                },
                fatal_failure_function: function(jqXHR, textStatus, response) {
                    onSelectTabError(tabs[index].code, jqXHR, response, {
                        action:tabs[index].action,
                        get_params:get_params,
                        post_params:post_params
                    });
                },
                error_function: function(jqXHR, textStatus, errorThrown, response) {
                    onSelectTabError(tabs[index].code, jqXHR, response, {
                        action:tabs[index].action,
                        get_params:get_params,
                        post_params:post_params
                    });
                }
            };

            if (typeof tabs[index].params !== 'undefined' && typeof tabs[index].params.failure_function !== 'undefined')
            {
                ajax_params.failure_function = tabs[index].params.failure_function;
            }
            
            sima.ajax.get(tabs[index].action, ajax_params);
        }
        else
        {
            //ovo nema smisla, sto bi radio rifresh, ako je ucitan?
//            refreshChildrenForce = true;
        }
        
       
        
        selected_tab.index = index;
        selected_tab.code = tabs[index].code;

        selected_tab.nav = navigation_bar.find('li:eq(' + index + ')').addClass('sima-ui-active');
        _main_tabs.find('ul.sima-main-tabs-nav li').css('border-bottom-color','black');       
        selected_tab.nav.css('border-bottom','1px solid '+selected_tab.nav.css('background-color'));
        $('.sima-main-tabs-gradient').css('background','linear-gradient('+ selected_tab.nav.css('background-color')+' , #FFFFFF)');
        
        selected_tab.panel = _main_tabs.find('div.sima-main-tabs-panel:eq(' + panel_index + ')').addClass('sima-ui-active');
       // selected_tab.panel.html(tabs[index].html);
        
        //MilosS(12.4.2017): Mora svaki put jer ne znamo kad je poslednji put pozvan allign i da li je bio dok je tab bio skriven
        //primer je kada se ucitava tab sa guitable i allign se poziva posle sume... (ulazni racuni recimo ili zavodna kniga)
        sima.layout.setLayoutSize(
            selected_tab.panel.children('.sima-layout-panel'),
            selected_tab.panel.height(),
            selected_tab.panel.width(),
            'TRIGGER_main_tab_select_tab'
        );
        
        if (typeof triggerSelectTab === 'undefined' || triggerSelectTab === true)
        {
            _main_tabs.trigger('selectTab', index);
        }
    }
    
    function onSelectTabError(tab_code, jqXHR, response, error_params)
    {
        if (typeof response === 'undefined')
        {
            response = {};
        }
        
        if (
                isTabSaved(tab_code) && 
                (
                    (!sima.isEmpty(response.status_code) && response.status_code === 404) || jqXHR.status === 404
                )
            )
        {
            error_params.message = sima.translate('MainTabActionNotFoundError');
            if (sima.isEmpty(response.error_id))
            {
                sima.errorReport.addError(error_params, false);
            }
            else
            {
                sima.errorReport.editError(response.error_id, error_params, false);
            }
            sima.dialog.openWarn(sima.translate('MainTabActionNotFound'));
        }
        else
        {
            if (sima.isEmpty(response.error_id))
            {
                error_params.message = !sima.isEmpty(response.message) ? response.message : jqXHR.responseText;
                sima.errorReport.addError(error_params);
            }
            else
            {
                sima.errorReport.editError(response.error_id);
            }
        }
    }

    function unselect_tab()
    {
        if (selected_tab.index != -1)
        {
            selected_tab.nav.removeClass('sima-ui-active');
            selected_tab.panel.removeClass('sima-ui-active');

            selected_tab.index = -1;
            selected_tab.code = '';
            selected_tab.nav = null;
            selected_tab.panel = null;
        }
    }

    function remove_tab(index)
    {
        tabs.splice(index, 1);
        var last_index = navigation_bar.find('li:last').index();
        var panel_index = getTabPanelIndexByTabIndex(index);
        
        _main_tabs.find('.sima-main-tabs-list-tabs ul li:eq(' + index + ')').remove();
        navigation_bar.find('li:eq(' + index + ')').remove();
        _main_tabs.find('div.sima-main-tabs-panel:eq(' + panel_index + ')').remove();

        if ((selected_tab).index == index)
        {
            unselect_tab();
            if (index == last_index)
                select_tab(index - 1);
            else
                select_tab(index);
        }
        else if ((selected_tab).index > index)
        {
            select_tab(selected_tab.index - 1);
        }
    }

    /**
     * pali sve tabove koji imaju decu sa klasom zadatom preko modelTag-a
     * @param modelTag
     */
    this.activateByTag = function(modelTag)
    {
//        console.log(modelTag);
        /**
         * trazimo sve tabove koji su parents od tag-a koji se updatuje
         * malo promenjeno
         */
        var to_activate_tabs = $('.' + modelTag).parents('div#sima-main-tabs');
//        console.log(to_activate_tabs[0]);

        to_activate_tabs.each(function(index, to_activate_tabs)
        {
//            console.log($(to_activate_tabs).find('.ui-tabs-panel').size());

            $(to_activate_tabs).find('.sima-main-tabs-panel').each(function(index2, tab)
            {
                if ($(tab).find('.' + modelTag).size() > 0)
                {
                    //console.log('index = '+$(tab_group).find('.ui-tabs-panel').index(tab));    
                    var ind = $(to_activate_tabs).find('.sima-main-tabs-panel').index(tab);
//                    console.log(ind);
                    select_tab(ind);
                }
            });
        });
    };

    function saveStarred()
    {
        var starred_tabsJSON = new Array();
        var i = 0;
        $('#sima-main-tabs ul.sima-main-tabs-nav li').each(function() {
            if ($(this).hasClass('starred'))
            {
                var tab_sett = new Object;
                var index = $(this).index();
                tab_sett.id = tabs[index].get_params;
                tab_sett.action = tabs[index].action;
                tab_sett.title = $(this).children('a').html();
                tab_sett.color = $(this).css('background-color');
                tab_sett.icon = $(this).data('icon');

                if (!sima.isEmpty(tabs[index].action))
                {
                    starred_tabsJSON[i] = tab_sett;
                    i++;
                }
            }
        });
        var starred_tabs = JSON.stringify(starred_tabsJSON);

        sima.ajax.get('site/saveMainTabs', {
            async: true,
            data: {main_tabs: starred_tabs}
        });
    }

    //old restoreStarred()
    this.init = function()
    {
        sima.mainTabs.select_tab(0);
        sima.ajax.get('site/restoreMainTabs', {
            async: true,
            loadingCircle: $("body"),
            success_function: function(response) {
                var local_tabs = jQuery.parseJSON(response.main_tabs);
                for (var temp_tab in local_tabs)
                {
                    if (typeof(local_tabs[temp_tab].action) !== 'undefined')
                    {
//                                    console.log('restorujem -> action: ' + local_tabs[temp_tab].action + ' i id:' + local_tabs[temp_tab].id + '');
                        openNewTab(local_tabs[temp_tab].action, local_tabs[temp_tab].id, false, local_tabs[temp_tab].color, local_tabs[temp_tab].title, local_tabs[temp_tab].icon);
                    }
                    if (typeof(local_tabs[temp_tab].href) !== 'undefined')
                    {
                        var href = local_tabs[temp_tab].href;
                        var index = href.indexOf("files/file&id=");
                        if (index !== -1)
                        {
                            var id = href.substring(index+14);
                            openNewTab('files/file', id, false,'#00FF00',local_tabs[temp_tab].label);
                        }
                    }
                }
                $("#sima-main-tabs ul.sima-main-tabs-nav li:not(:first-child)").addClass('starred');
            },
            is_idle_since: false
        });
    };

    /**
     * Funkcija koja ponovno ucitava velicine
     * @returns {undefined}
     */
    this.trigger_resize = function()
    {
        _main_tabs.trigger('sima-layout-allign',[-1, -1, 'TRIGGER_main_tabs_outside']);   
    };
    
    _main_tabs.on('sima-layout-allign',function (event, height, width, source){
        if (event.target === this)
        {
            if (sima.layout.log())
            {
                console.log(source);
                console.log('HANDLER_main_tabs');
            }
            var window_height = $(window).outerHeight(true);
            var panels_height = window_height - _main_tabs.find('.sima-main-tabs-nav').outerHeight(true) - $('#top_bar').outerHeight(true);
            var panels_width = $(window).outerWidth(true);
            $('div#page').height(window_height);
            
            _main_tabs.find('.sima-main-tabs-panel').height(panels_height);
            _main_tabs.find('.sima-main-tabs-panel').width(panels_width);
            
            if (selected_tab.panel !== null)
            {
                sima.layout.setLayoutSize(
                    selected_tab.panel.children('.sima-layout-panel'),
                    selected_tab.panel.height(),
                    selected_tab.panel.width(),
                    'TRIGGER_main_tabs_layout_allign'
                );
            }
            event.stopPropagation();
        }
    });

    function arraymove(arr, fromIndex, toIndex)
    {
        var element = arr[fromIndex]
        arr.splice(fromIndex, 1);
        arr.splice(toIndex, 0, element);
    }
    
    this.isTabOpened = function(action, action_id)
    {
        var founded = false;
        for (var i=0; i< tabs.length; i++)
        {
            founded = findTabByCode(tabs[i].code, action, action_id);
            if (founded === true)
            {
                break;
            }
        }
        
        return founded;
    };
    
    function findTabByCode(code, action, action_id)
    {
        var founded = false;
        if (code === action)
        {
            founded = true;
        }
        else if(typeof action_id === 'number' || typeof action_id === 'string')
        {
            if (code === (action+action_id))
            {
                founded = true;
            }
            else if (code === (action+JSON.stringify({id:action_id})))
            {
                founded = true;
            }
        }
        else if (typeof action_id === 'object')
        {
            if (code === (action+JSON.stringify(action_id)))
            {
                founded = true;
            }
            //mozda su neke vrednosti objekta(action_id) integer-i ali su zapisani kao string(u code se nalaze kao string)
            //action_id ne bi trebalo da sadrzi integere kao string, pa zato prolazimo kroz njega i sve integere zapisujemo kao string pa proveravamo 
            //da li se slaze sa kodom
            else
            {
                var extended_action_id = $.extend(true, {}, action_id);
                for (var property in extended_action_id) 
                {
                    if (extended_action_id.hasOwnProperty(property) && $.isNumeric(extended_action_id[property]))
                    {
                        extended_action_id[property] = String(extended_action_id[property]);
                    }
                }
                if (code === (action+JSON.stringify(extended_action_id)))
                {
                    founded = true;
                }
            }
        }
        else if ((action_id === null || typeof action_id === 'undefined') && code === action+'{}')
        {
            founded = true;
        }
        
        return founded;
    }

    function getTabPanelIndexByTabIndex(tab_index)
    {
        var tab = $(navigation_bar.find('li').get(tab_index));
        if (!sima.isEmpty(tab))
        {
            var tab_code = tab.data('code');
            if (!sima.isEmpty(tab_code))
            {
                var tab_content = _main_tabs.find(".sima-main-tabs-panel[data-code='"+tab_code+"']");
                if (!sima.isEmpty(tab_content))
                {
                    return tab_content.index('.sima-main-tabs-panel');
                }
            }
        }
        
        return 0;
    }
    
    function reOrderMainTabsList(start_index, end_index)
    {
        var main_tabs_list = _main_tabs.find('.sima-main-tabs-list-tabs');
        var start_element = main_tabs_list.find('ul li').eq(start_index);
        var end_element = main_tabs_list.find('ul li').eq(end_index);
        
        if (start_index > end_index)
        {
            start_element.insertBefore(end_element);
        }
        else
        {
            start_element.insertAfter(end_element);
        }
    }
    
    function isTabSaved(tab_code)
    {
        var is_saved = false;
        $('#sima-main-tabs ul.sima-main-tabs-nav li.starred').each(function() {
            if (is_saved === false && $(this).data('code') === tab_code)
            {
                is_saved = true;
            }
        });
        
        return is_saved;
    }
}

