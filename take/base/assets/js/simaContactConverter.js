/* global sima */

function SIMAContactConverter()
{   
    this.saveContacts = function(uniq)
    {      
        var localThis    = $('#contact-converter-'+uniq);
        var contact_type = localThis.find('#contact-converter-type'+uniq).val();
        var convert_type = localThis.find('#contact-converter-typeto'+uniq).val();
        
        if (sima.isEmpty(convert_type))
        {
            sima.dialog.openWarn(sima.translate('ContactConverterNoSelectedConvertTypeto'));
            return;
        }
        else if(sima.isEmpty(contact_type))
        {
            sima.dialog.openWarn(sima.translate('ContactConverterNoSelectedConvertType'));
            return;
        }
        
        var contact_table = localThis.find('#contact-gui-table'+uniq);       
        var ids_rows      = contact_table.simaGuiTable('getSelected');
        var contacts      = [];
        
        ids_rows.each(function()
        {
            var row = $(this);
           
            //Fiksni i mobilni
            if (convert_type === 'phone_number') 
            {
                contacts.push({
                    contact_id: row.attr('model_id'),
                    phone_params: row.find('.sima-phone-field').simaPhoneField('getValue'),
                    phone_type: row.find(".phone-number-type").val()
                });
            }
            //Email
            else if (convert_type === 'email') 
            {
                contacts.push({
                    contact_id: row.attr('model_id'),
                    email: row.find('.converted-email').val()
                });
            }       
        });

        if (sima.isEmpty(contacts))
        {
            sima.dialog.openWarn(sima.translate('ContactConverterNoSelectedContacts'));
            return;
        }
        
        sima.ajax.get('base/contactConverter/saveContacts',
        {
            data: {
                contacts: contacts
            },
            success_function: function() {
                contact_table.simaGuiTable('populate');
            }
        });       
        
    };
    
    this.contactTypeChange = function (uniq) 
    {
        var contact_type = $('#contact-converter-type' + uniq).val();
        var contacts_table = $('#contact-gui-table' + uniq);
        
        if(!sima.isEmpty(contact_type)) 
        {
            contacts_table.simaGuiTable('setFixedFilter', {
                type: {
                    ids: contact_type
                }
            }).simaGuiTable('populate');
        }        
    };
    
    this.convertTypeChange = function (uniq)
    {
        var phone_number_types = $('#phone_number_types_' + uniq);
        var convert_type = $('#contact-converter-typeto' + uniq).val();
        var contacts_table = $('#contact-gui-table' + uniq);

        contacts_table.simaGuiTable('hideColumns', ['phone_number', 'email']);
        
        if (convert_type === 'phone_number')
        {
            phone_number_types.show();
        }
        else
        {
            phone_number_types.hide();
        }
        
        if(!sima.isEmpty(convert_type)) 
        {
            contacts_table.simaGuiTable('showColumns', [convert_type]);
        }
        
        contacts_table.simaGuiTable('populate');
    };

    this.phoneNumberTypeChange = function(uniq)
    {
        var phone_number_type_val = $('#phone_number_types_' + uniq).val();
        var contacts_table = $('#contact-gui-table' + uniq);
        
        contacts_table.find('.sima-guitable-row .phone-number-type').each(function() {
            $(this).val(phone_number_type_val);
        });
    };
}

