
/* global sima */

function SIMALayout()
{
    //MilosS(23.04.2016.): privremena funkcija da li treba da se loguje, treba je izbrisati i sve sto ona obuhvata
    //sima.layout.log()
    this.log = function()
    {
        return false;
    };
    
    var default_min_width = 100; //px
    var default_max_width = 2000; //px

    var splitter_width = 1; //px
    var handle_width = 9; //px
    var colapsed_panel_width = 20; //px
    
    this.init = function()
    {
        $(window).on('resize', windowResizeHandler);
        $('html').on('sima-layout-allign','.sima-layout-panel', simaResizeHandler);
        $('body').on('sima-layout-allign','.sima-layout-panel._splitter', simaResizeSplitterHandler);
        $('body').on('sima-layout-allign','.sima-layout-panel._floater', simaResizeFloaterHandler);
        $('body').on('sima-layout-allign','.scroll-container', simaScrollContainerResizeHandler);
        if (!sima.useNewLayout())
        {
            sima.mainTabs.trigger_resize();
        }
    };
    
    this.setLayoutSize = function(obj,height,width,source)
    {
        if (typeof source === 'undefined') source = 'TRIGGER_setLayoutSize';
        if ( obj.children('.sima-layout-panel').length > 0)
        {
            obj.addClass('_has_layout_children');
        }
        obj.trigger('sima-layout-allign', [height,width, source]);
    };
    
    this.allignObject = function(obj,source)
    {
        if (typeof source === 'undefined') source = 'TRIGGER_allignObject';
        sima.layout.setLayoutSize( obj, obj.height(), obj.width(), source);
    };
    
    this.allignFirstLayoutParent = function(obj,source)
    {
        if (typeof source === 'undefined') source = 'TRIGGER_allignFirstLayoutParent';
        sima.layout.allignObject(obj.parents('.sima-layout-panel:first'), source);
    };
    
    function resizeSplitterInit(ResizePanel)
    {
        //postavljanje visine na fiksno
        var orig_height = Math.floor(ResizePanel.parent().height());
        ResizePanel.height(orig_height); 

        var orig_width = Math.floor(ResizePanel.parent().width());
        ResizePanel.width(orig_width); //postavljanje sirine na fiksno


        /*prolaz kroz sve panel u okviru widget-a*/
        ResizePanel.data('panels', ResizePanel.children('.sima-layout-panel'));
        if (ResizePanel.children('.sima-layout-fixed-panel').length)
        {
            console.error('POSTOJE FIKSNI PANELI U SPLITER-u: ');
            console.error(ResizePanel);
        }


        var panels = ResizePanel.data('panels');
        var panels_count = panels.size();
        var panel_iter = 0;
        var cumulative_panel_width = (panels_count - 1)*splitter_width;
        if (ResizePanel.hasClass('_vertical'))
        {
            var _orientation_size = 'width';
            var _orientation_position = 'left';
            var _orientation_axis = 'x';
            var _org_size = orig_width;
        }
        else //if (ResizePanel.hasClass('_horizontal')) MilosS(19.5.2016.): ne znam sta bi stavio u else
        {
            var _orientation_size = 'height';
            var _orientation_position = 'top';
            var _orientation_axis = 'y';
            var _org_size = orig_height;
        }
        //zadaju se sirine panela prema zadatim proporcijama i visine preba roditelju, ili prema title
        panels.each(function() {
            var current_panel = $(this);
            current_panel.addClass('sima-layout-panel');
            // mora ".prevAll(".sima-layout-panel").first();" jer moze da se desi scroll loading
            var prev_panel = current_panel.prevAll(".sima-layout-panel").first(); 
            //ubacuje drag handle
            if (prev_panel.size()!==0)
            {
                var separator = $('<div class = "sima-ui-resizable-separator" style="'+_orientation_size+': '+splitter_width+'px;"></div>')
                        .insertBefore(current_panel);
                var handle_half = (handle_width-1)/2;
                var draghandle = $('<div class="sima-ui-resizable-draghandle" style="'+_orientation_size+': '+handle_width+'px;'+_orientation_position+': -'+handle_half+'px;"></div>')
                        .draggable({
                            axis: _orientation_axis,
                            start: sima.layout.onDraghandleStart,
                            stop: sima.layout.onDraghandleStop
                        });
                separator.html(draghandle);
                current_panel.css('margin-'+_orientation_position,(splitter_width+'px'));
            }                

            /*setovanje velicina panela na njihovu stvarnu sirinu ali u pikselima*/
            //samo se postavljaju min i max, ali se refresh poziva odmah nakon init, zato je i dozvoljeno da odnos bude netacan
            current_panel.data('minimized', false);
            
            var _init_data = current_panel.data('simaLayoutInit') || [];
            
            var min_width  = parseInt(_init_data['min_width']) || default_min_width;
            var max_width  = parseInt(_init_data['max_width']) || default_max_width;
            var proportion  = parseFloat(_init_data['proportion']) || 1.0;
            
            var can_minimize  = _init_data['can_minimize']===false?false:true;
            var minimized  = can_minimize && (_init_data['minimized'] || false);

            current_panel.data('min_width',min_width);
            current_panel.data('max_width',max_width);
            current_panel.data('can_minimize',can_minimize);
            current_panel.data('minimized',minimized);
            current_panel.data('minimized_on_purpose',minimized);//ako je minimizovana prilikom init, onda je namerno
            current_panel.data('proportion',proportion);
            
            var _curr_panel_width = 0;
            if (minimized)
            {
                _curr_panel_width = colapsed_panel_width;
                hide_panel(current_panel, true);
            }
            else
            {
                _curr_panel_width = Math.floor((_org_size - panels_count*splitter_width) * proportion);
            }
            
            if (max_width < _curr_panel_width)
            {
                _curr_panel_width = max_width;
            }
            else if (min_width > _curr_panel_width)
            {
                _curr_panel_width = min_width;
            }
                
            if (ResizePanel.hasClass('_vertical'))
            {
                current_panel.width(_curr_panel_width);
            }
            else
            {
                current_panel.height(_curr_panel_width);
            }

            cumulative_panel_width += _curr_panel_width;
            panel_iter++;
        });

        //ako odnosi nisu u zbiru zadati 100% - OK je posto se poziva refresh koji to pegla
        ResizePanel.attr('original_panel_size', cumulative_panel_width);
        //TODO(sima-resize)


        //ovo sluzi da u google chrome nema fantomskih scrollova
        setTimeout(function(){
            ResizePanel.parent().css('overflow','visible');
        },1);

        sima.pushActiveFunctionalElement(ResizePanel);
    };
    
    function hide_panel(panel, on_purpose)
    {
        if (panel.parent().hasClass('_vertical'))
        {
            var _orientation_size = 'width';
        }
        else //if (ResizePanel.hasClass('_horizontal')) MilosS(19.5.2016.): ne znam sta bi stavio u else
        {
            var _orientation_size = 'height';
        }

        if (typeof(panel.data('front_panel')) === 'undefined')
        {
            var style_size = _orientation_size+': '+colapsed_panel_width+'px';
            var _color = 'rgb('+(Math.random()*255|0)+','+(Math.random()*255|0)+','+(Math.random()*255|0)+')';
            var style_color = 'background-color: '+_color;
            
            var _front_panel = $('<div class="sima-ui-resizable-minimized-front" style="'+style_size+'; '+style_color+'"></div>');
            panel.prepend(_front_panel);
            panel.data('front_panel', _front_panel);
            
        }
        else
        {
            panel.data('front_panel').show();
        }
        panel.data('minimized',true);
        panel.data('minimized_on_purpose',on_purpose);
        panel.addClass('_enforce_no_scroll');
        panel.children(':not(.sima-ui-resizable-minimized-front)').hide();
    };
    
    function show_panel(panel)
    {
        panel.data('front_panel').hide();
        panel.data('minimized',false);
        panel.removeClass('_enforce_no_scroll');
        panel.children(':not(.sima-ui-resizable-minimized-front)').show();
    };
    
    function showAlert(some_element)
    {
        var separators = some_element.parents('.sima-layout-panel._splitter').first().find('div.sima-ui-resizable-draghandle');
        separators.addClass('_warning');
        setTimeout(function(){
            separators.removeClass('_warning');
        },100);
    }
    
    this.onDraghandleStart = function(event, ui)
    {
        $(this).toggleClass('dragged');
    };
    
    this.onDraghandleStop = function(event, ui)
    {
        var drag_element = $(this);
        var separator = drag_element.parent();
        var divL = separator.prev();
        var divR = separator.next();
        drag_element.toggleClass('dragged');
        
        var ResizePanel = separator.parent();
        
        if (ResizePanel.hasClass('_vertical'))
        {
            var oldPos = (drag_element.data("ui-draggable").originalPosition.left);
            var newPos = ui.position.left;
            var change = newPos - oldPos;
            
            var old_widthL = divL.width();
            var old_widthR = divR.width();
            
            var _orientation_size = 'width';
            var _orientation_position = 'left';
            var _orientation_axis = 'x';
        }
        else
        {
            var oldPos = (drag_element.data("ui-draggable").originalPosition.top);
            var newPos = ui.position.top;
            var change = newPos - oldPos;
            
            var old_widthL = divL.height();
            var old_widthR = divR.height();
            
            var _orientation_size = 'height';
            var _orientation_position = 'top';
            var _orientation_axis = 'y';
        }
        var min_widthL = divL.data('min_width');
        var min_widthR = divR.data('min_width');
        var max_widthL = divL.data('max_width');
        var max_widthR = divR.data('max_width');
        
        
        var can_giveL = old_widthL - min_widthL;
        var can_giveR = old_widthR - min_widthR;
        var can_give_colapseL = old_widthL - colapsed_panel_width;
        var can_give_colapseR = old_widthR - colapsed_panel_width;
        var colapse_treshholdL = Math.floor(old_widthL / 2);
        var colapse_treshholdR = Math.floor(old_widthR / 2);
        var can_getL  = max_widthL - old_widthL;
        var can_getR  = max_widthR - old_widthR;
           
        //TODO: implementirati davanje prostora drugima
        var other_panels_can_get = 0;
        
        if (change > 0) //pomeraj udesno
        {
            if (change > colapse_treshholdR) //treba da se skupi desni
            {
                if (can_getL < can_give_colapseR)
                {
                    change = 0;
                }
                else
                {
//                    change = can_give_colapseR;
                }
            }
            else
            {
                if (can_getL < change)
                {
                    change = can_getL;
                }
//                else
//                {
//                    change = change;
//                }
            }

        }
        else //pomeraj ulevo
        {
            var local_change = -change;
            if (local_change > colapse_treshholdL) //treba da se skupi levi
            {
                if (can_getR < can_give_colapseL)
                {
                    local_change = 0;
                }
                else
                {
//                    local_change = can_give_colapseL;
                }
            }
            else
            {
                if (can_getR < local_change)
                {
                    local_change = can_getR;
                }
//                else
//                {
//                    local_change = local_change;
//                }
            }
            change = -local_change;
        }
        
        if (change === 0)
            showAlert(drag_element);
        
        if (ResizePanel.hasClass('_vertical'))
        {
            var newRwidth = divR.width() - change;
            var newLwidth = divL.width() + change;
            var _separator_old_position = separator.position().left;
        }
        else
        {
            var newRwidth = divR.height() - change;
            var newLwidth = divL.height() + change;
            var _separator_old_position = separator.position().top;
        }
        
        separator.css(_orientation_position, (_separator_old_position + change) + 'px');
        var handle_half = (handle_width-1)/2;
        drag_element.css(_orientation_position, '-'+handle_half+'px');
/**
 * TODO: treba implementirati i max_width i obraditi sve slucajeve
 */
        //SKRIVANJE i OGRANICAVANJE
        
        if (ResizePanel.hasClass('_vertical'))
        {
            _separator_old_position = separator.position().left;
        }
        else
        {
            _separator_old_position = separator.position().top;
        }
        
        if ((newRwidth < (min_widthR / 2)) && divR.data('can_minimize'))
        {
            //hide R panel
            var _vector = newRwidth - colapsed_panel_width;
            newLwidth += _vector;
            separator.css(_orientation_position, (_separator_old_position + _vector) + 'px');
            newRwidth = colapsed_panel_width;
            hide_panel(divR, true);
        }
        else 
        {
            if (divR.data('minimized') === true)
            {
                show_panel(divR);
            }
            if (newRwidth < min_widthR)
            {
                //restore previous width for R panel
                newLwidth -= min_widthR - newRwidth;
                separator.css(_orientation_position, (_separator_old_position - min_widthR + newRwidth) + 'px');
                newRwidth = min_widthR;
            }
        }
        
//        else if (newRwidth > max_widthR)
//        {
//            //restore previous width for R panel
//            newLwidth += max_widthR - newRwidth;
//            separator.css('left', (separator.position().left + max_widthR - newRwidth) + 'px');
//            newRwidth = max_widthR;
//        }

        if (ResizePanel.hasClass('_vertical'))
        {
            _separator_old_position = separator.position().left;
        }
        else
        {
            _separator_old_position = separator.position().top;
        }
        
        if ((newLwidth < (min_widthL / 2)) && divL.data('can_minimize'))
        {
            //hide L panel
            var _vector = newLwidth - colapsed_panel_width;
            newRwidth += _vector;
            separator.css(_orientation_position, (_separator_old_position - _vector) + 'px');
            newLwidth = colapsed_panel_width;
            hide_panel(divL, true);
        }
        else 
        {
            if (divL.data('minimized') === true)
            {
                show_panel(divL);
            }

            if (newLwidth < min_widthL)
            {
                //restore previous width for L panel
                newRwidth -= min_widthL - newLwidth;
                separator.css(_orientation_position, (_separator_old_position + min_widthL - newLwidth) + 'px');
                newLwidth = min_widthL;
            }
        }
//        else if (newLwidth > max_widthL)
//        {
//            //restore previous width for L panel
//            newRwidth += max_widthL - newLwidth;
//            separator.css('left', (separator.position().left - max_widthL + newLwidth) + 'px');
//            newLwidth = max_widthL;
//        }

//        separator.css('left', (separator.position().left + change) + 'px');
        if (ResizePanel.hasClass('_vertical'))
        {
            divR.width(newRwidth);
            divL.width(newLwidth);
            
            var height = separator.parent().height();
            divR.height(height);
            divR.prev('.sima-ui-resizable-separator').height(height);
        }
        else
        {
            divR.height(newRwidth);
            divL.height(newLwidth);
            
            var height = separator.parent().width();
            divR.width(height);
            divR.prev('.sima-ui-resizable-separator').width(height);
        }

        divL.trigger('sima-layout-allign',[divL.height(),divL.width(),'TRIGGER_resizable_panel_moveL']);
        divR.trigger('sima-layout-allign',[divR.height(),divR.width(),'TRIGGER_resizable_panel_moveR']);
    };
    
    function simaResizeSplitterHandler(event, height, width, source)
    {
        if (event.target === this)
        {
            /*lokalna promenljiva u kojoj se cuva sam selektor i promenljiva u kojoj se cuva sirina glavnog panela nakon promene sirine prozora*/
            var ResizePanel = $(this);
            if (sima.layout.log())
            {
                console.log(source);
                console.log('HANDLER_resizable_panel');
                console.log(ResizePanel);
            }
            if (!ResizePanel.data('init_done'))
            {
                resizeSplitterInit(ResizePanel);
                ResizePanel.data('init_done',true);
            }
            
            
            if (ResizePanel.hasClass('_vertical'))
            {
                var new_widget_width = Math.floor(width);
                ResizePanel.width(new_widget_width);
            }
            else //if (ResizePanel.hasClass('_horizontal')) MilosS(19.5.2016.): ne znam sta bi stavio u else
            {
                var new_widget_width = Math.floor(height);
                ResizePanel.height(new_widget_width);
            }
            ResizePanel.width(Math.floor(width));
            ResizePanel.height(Math.floor(height));
            
//            var title = ResizePanel.children('div.sima-ui-resizable-title');
            var panels = ResizePanel.children('.sima-layout-panel');
            var panel_count = panels.size();
            var panel_iter = 0;
            
            

            new_widget_width -= (panel_count-1)*splitter_width;
            var orig_width = ResizePanel.attr('original_panel_size');
            
            /*
             * prolazi se kroz sve panele u okviru glavnog panela i siri ih i skuplja
             * i postavlja koliko mogu da daju i uzmu
             * i za koliko ne moze da se uklopi zbog min i max
             * */
            var cumulative_panel_width = 0.0;
            var can_give_cumulative_maximize = 0;//koliko svi RASIRENI paneli mogu da daju (i toliko se smanje)
            var can_get_cumulative_maximize  = 0;//koliko svi RASIRENI paneli mogu da uzmu (i toliko se povecaju)
            var can_give_cumulative = 0;//koliko svi paneli mogu da daju (i toliko se smanje)
            var can_get_cumulative  = 0;//koliko svi paneli mogu da uzmu (i toliko se povecaju)
            var needed_for_maximize = [];//koliko svi paneli mogu da uzmu (i toliko se povecaju)
            panels.each(function() {
                var current_panel = $(this);

                var min_width = current_panel.data('min_width');
                var max_width = current_panel.data('max_width');
                var minimized = current_panel.data('minimized');
                
                if (minimized)
                {
                    new_width = colapsed_panel_width;
                    if (!current_panel.data('minimized_on_purpose'))
                    {
                        needed_for_maximize.push({
                            panel: current_panel,
                            needed: (min_width - colapsed_panel_width)
                        });
                    }
                }
                else
                {
                    var curr_changeable_size = 0;
                    if (ResizePanel.hasClass('_vertical'))
                    {
                        curr_changeable_size = Math.floor(current_panel.width());
                    }
                    else
                    {
                        curr_changeable_size = Math.floor(current_panel.height());
                    }
                    /*odnos sirine panela prema originalnoj velicini panela*/
                    var panel_ratio = (curr_changeable_size / orig_width);
                    var new_width = Math.round(panel_ratio * new_widget_width);
                    if (new_width < min_width) //manji od minimalne
                        new_width = min_width;

                    if (new_width > max_width) //veci od maksimalne
                        new_width = max_width;
                    can_give_cumulative_maximize += new_width - min_width;
                    can_get_cumulative_maximize  += max_width - new_width;
                }
                

                cumulative_panel_width += new_width;
                var _can_give = Math.max(new_width - min_width,0);
                var _can_get = max_width - new_width;
                can_give_cumulative += _can_give;
                can_get_cumulative += _can_get;
                current_panel.data('can_give', _can_give);//da ne ide u minus
                current_panel.data('can_get',  _can_get);

                current_panel.data('new_width',new_width);
                panel_iter++;
            });

            var widget_needs = cumulative_panel_width - new_widget_width;
             
             
            //Provera da li nesto mozemo da rasirimo na ustrb potreba widget-a
            if (can_give_cumulative_maximize > widget_needs)
            {
                $.each(needed_for_maximize,function(){
                    var _needed = this.needed;
                    if (can_give_cumulative_maximize > widget_needs && can_give_cumulative_maximize >= _needed)
                    {
                        var _panel = this.panel;

                        var min_width = _panel.data('min_width');
                        var max_width = _panel.data('max_width');
                        _panel.data('minimized', false);

                        var can_give = 0;
                        var can_get = max_width - min_width;
                        _panel.data('can_give', can_give);
                        _panel.data('can_get',  can_get);
                        _panel.data('new_width', min_width);

                        show_panel(_panel);

                        widget_needs += min_width - colapsed_panel_width;
                        can_give_cumulative_maximize += can_give; 
                        can_get_cumulative_maximize  += can_get;
                        can_give_cumulative = 0;//samo se povecao ali i dalje ne moze nista da da
                        can_get_cumulative  -= min_width - colapsed_panel_width; //za toliko se povecao
                    }
                });
            }
            
            
            //potrebno je vise nego sto mogu dati otvoreni - neki mora da se sklopi
            while (widget_needs > can_give_cumulative_maximize)
            {
                var _collapsed = false;
                panels.each(function() {
                    var current_panel = $(this);


                    var old_can_give = current_panel.data('can_give');
                    var old_can_get = current_panel.data('can_get');
                    var min_width = current_panel.data('min_width');
                    var max_width = current_panel.data('max_width');
                    var minimized = current_panel.data('minimized');
                    var can_minimize = current_panel.data('can_minimize');
                    var old_width = current_panel.data('new_width');


                    if (!minimized && can_minimize) //SKLAPANJE JEDNOG
                    {


                        var will_give = old_width - colapsed_panel_width;
                        current_panel.data('can_give', colapsed_panel_width - min_width);
                        current_panel.data('can_get',  max_width - colapsed_panel_width);
                        current_panel.data('new_width',colapsed_panel_width);

                        hide_panel(current_panel, false);

                        widget_needs -= will_give;
                        can_give_cumulative_maximize -= old_can_give;
                        can_get_cumulative_maximize  -= old_can_get;


                        _collapsed = true;
                        return false; //samo se jedan minimizuje, pa se proverava
                    }
                });
                if (!_collapsed)
                {
                    console.warn('NEMA NI JEDAN DA SE SKUPI!!!');
                    break;
                }
            }
            
            //DAJ WIDGETU KOLIKO MU TREBA OD RASIRENIH
            while (widget_needs > 0)
            {
                var _given = false;
                panels.each(function() {
                    var current_panel = $(this);

                    var can_give = current_panel.data('can_give');
                    var min_width = current_panel.data('min_width');
                    var max_width = current_panel.data('max_width');
                    var minimized = current_panel.data('minimized');
                    var old_width = current_panel.data('new_width');
    //
                    if (!minimized)
                    {
                        var will_give = Math.min(can_give,Math.abs(widget_needs));
                        var new_width = old_width - will_give;
                        can_give = can_give - will_give;

                        current_panel.data('new_width',new_width);
                        current_panel.data('can_give',can_give);
                        current_panel.data('can_get', max_width - new_width);

                        widget_needs -= will_give;
                        can_give_cumulative_maximize -= will_give;
                        can_get_cumulative_maximize  += will_give;

                        if (will_give > 0)
                        {
                            _given = true;
                        }
                    }
                });
                if (!_given)
                {
                    console.warn('NEMA NI JEDAN DA DA WIDGETU!!!');
                    break;
                }
            }
            
            //UKOLIKO NE MOZES DA POPUNIS WIDGET RASIRENIM, POKUSAJ DA RASIRIS SKLOPLJENE
            while ((- widget_needs) > can_get_cumulative_maximize)
            {
                var _expanded = false;
                panels.each(function() {
                    var current_panel = $(this);
    //
    //
                    var can_give = current_panel.data('can_give'); //u minusu ako je minimiziran
                    var min_width = current_panel.data('min_width');
                    var max_width = current_panel.data('max_width');
                    var minimized = current_panel.data('minimized');
    //
                    if (minimized)
                    {
                        if (can_give > 0)
                            console.error('NEMOGUCE DA JE MINIMIZOVAN I DA MOZE DA DA');

                        if ((can_give_cumulative_maximize - widget_needs) > (-can_give))
                        {
                            var will_get = -can_give; //uzima koliko mu treba za minimum
                            var new_width = min_width;
                            var can_get = max_width - min_width;
                            current_panel.data('can_give', 0);
                            current_panel.data('can_get',  can_get);
                            current_panel.data('new_width',new_width);

                            show_panel(current_panel);

                            widget_needs += will_get;
                            can_give_cumulative_maximize += 0; //0 jer sirimo na minimum
                            can_get_cumulative_maximize  += can_get; 

                            return false; //sirimo jedan po jedan, ne oba
                        }
                    }
                });
                if (!_expanded)
                {
                    console.warn('NEMA NI JEDAN DA SE RASIRI!!!');
                    break;
                }
            }

            //OSTATAK PODELI PANELIMA KOJI MOGU DA UZMU
            while (widget_needs < 0)
            {
                var _given = false;
                panels.each(function() {
                    var current_panel = $(this);

                    var can_get = current_panel.data('can_get');
                    var min_width = current_panel.data('min_width');
                    var max_width = current_panel.data('max_width');
                    var minimized = current_panel.data('minimized');
                    var old_width = current_panel.data('new_width');
    //
                    if (!minimized)
                    {
                        var will_get = Math.min(can_get,Math.abs(widget_needs));
                        var new_width = old_width + will_get;
                        can_get = can_get - will_get;

                        current_panel.data('new_width',new_width);
                        current_panel.data('can_get',can_get);
                        current_panel.data('can_give',max_width - new_width);

                        widget_needs += will_get;
                        can_give_cumulative_maximize += will_get; 
                        can_get_cumulative_maximize  -= will_get; 

                        if (will_get > 0)
                        {
                            _given = true;
                        }
                    }
                });
                if (!_given)
                {
                    console.warn('NEMA NI JEDAN DA PREUZME!!!');
                    break;
                }
            }


            //MilosS(29.9.2018): prilikom zoomin i zoomout, jQuery ne daje pravu sirinu DIV-a, 
            //nego je zaokruzi i onda ako je div recimo 1199.88px, on da 1200px. 
            //previse se cesto desava da na nekim rezolucijama cak mozda i bez zoom-a to ne radi kako treba
            //pa je ovo ubaceno kao zakrpa
            //inace, ovo bi trebalo da bude reseno ubacivanjem novog jQuery-a
            //
            //ovde u sustini samo postavimo panel na manje nego sto smo rekli da cemo ga postaviti...
            
            var _curr_panel_cnt = 0;
            var _is_chrome = sima.isChromeBrowser();
            //setuj velicine i preracunaj visinu
            panels.each(function() {
                var current_panel = $(this);
                
                _curr_panel_cnt++;
                var _minus = (_is_chrome && (_curr_panel_cnt === 1))?1:0;
                
                if (ResizePanel.hasClass('_vertical'))
                {
                    current_panel.width(current_panel.data('new_width') - _minus);
                    //izracunavanje visine
                    current_panel.height(height - _minus);
                    current_panel.prev('.sima-ui-resizable-separator').height(height);
                }
                else
                {
                    current_panel.height(current_panel.data('new_width') - _minus);
                    //izracunavanje sirine
                    current_panel.width(width - _minus);
                    current_panel.prev('.sima-ui-resizable-separator').width(width);
                }
                current_panel.trigger('sima-layout-allign',[current_panel.height(),current_panel.width(),'TRIGGER_resizable_panel_global']);

                panel_iter++;
            });
            /*ponovno postaljvanje pozicije div za resajzovanje*/
            var cumulativ_draghole_position = 0;
            if (ResizePanel.hasClass('_vertical'))
            {
                ResizePanel.children('div.sima-ui-resizable-separator').each(function() {            
                    var left_panel = $(this).prevAll(".sima-layout-panel").first();
                    cumulativ_draghole_position +=
                            left_panel.width() +
                            parseInt(left_panel.css('margin-left'));

                    $(this).css({'left': cumulativ_draghole_position + 'px'});

                });
            }
            else
            {
                ResizePanel.children('div.sima-ui-resizable-separator').each(function() {            
                    var left_panel = $(this).prevAll(".sima-layout-panel").first();
                    cumulativ_draghole_position +=
                            left_panel.height() +
                            parseInt(left_panel.css('margin-top'));

                    $(this).css({'top': cumulativ_draghole_position + 'px'});

                });
            }
            /*sada je nova velicina originalna*/
            ResizePanel.attr('original_panel_size', new_widget_width);
            ResizePanel.removeClass('sima-layout-broken');
            event.stopPropagation();
        }
    };
    
    function simaResizeFloaterHandler(event, height, width, source)
    {
        if (event.target !== this)
        {
            return;
        }
        
        event.stopPropagation();
        
        var floater_panel = $(this);
        if (sima.layout.log())
        {
            console.log(source);
            console.log('HANDLER_floater_panel');
            console.log(floater_panel);
            console.log(width);
        }
        
        floater_panel.width(Math.floor(width));
        floater_panel.height(Math.floor(height));

        var panels = floater_panel.children('.sima-layout-panel');
        var panel_count = panels.size();
        
        /**
         * validiranje podataka
         */
        if(simaResizeFloaterHandler_validateData(floater_panel) !== true)
        {
            return;
        }
        
        var data_boxes_min_width = 0;
        
        results_panel_width = width;
        
        panels.each(function(){
            var panel = $(this);
            var panel_data = panel.data('sima-layout-floater');
            
            //izracunavanje minimalne sirine skupa data box-eva u HORIZONTALNOM polozaju
            data_boxes_min_width += parseInt(panel_data.min_width);
        });
        
        if (width < data_boxes_min_width) // prepakivanje prikaza u VERIKALNI prikaz
        {
            panels.each(function()
            {
                var panel = $(this);
//                var panel_data = panel.data('sima-layout-floater');
//                var max_width = panel_data.max_width;
                
//                if(max_width < width)
//                {
                    panel.css('width', '100%');
//                    panel.css('max-width', max_width+'px');
//                }
//                else
//                {
//                    panel.css('width', width+'px');
//                }
            });
        }
        else // prepakivanje prikaza u HORIZONTALNI prikaz
        {
            /// TODO MilosJ 20170915 - treba srediti kada su razlicitih sirina, trenutno se smatra da svi imaju isti max width
            
            var element_width = results_panel_width/panel_count;
            
//            var width_left_to_give = results_panel_width;
//            var panels_parsed = 0;

            var extra_width = 0;
            var shortage_width = 0;
            
            var panels_taken_width = 0;
            var panels_with_percentage_width_count = 0;
            panels.each(function()
            {
//                var element_width = width_left_to_give/(panel_count-panels_parsed);
//                
//                
                var panel = $(this);
                var panel_data = panel.data('sima-layout-floater');
                var panel_width = panel_data.width;
                var min_width = panel_data.min_width;
                var max_width = panel_data.max_width;
                
                if(typeof panel_width === 'string' && panel_width[panel_width.length -1] === '%')
                {
//                    panel_width_num = panel_width.substring(0, panel_width.length-1);
//                    
//                    panel.css('width', panel_width);
                    panels_with_percentage_width_count++;
                }
                else
                {
                    if(element_width > max_width)
                    {
                        extra_width = element_width-max_width;

                        element_width = max_width;
                    }
    //                
                    if(element_width < min_width)
                    {
                        shortage_width = min_width-element_width;

                        element_width = min_width;
                    }

    //                if(element_width > width_left_to_give)
    //                {
    //                    var width_to_take_from_others = 
    //                    var panel_counter = 0;
    //                    panels.each(function(){
    //                        if(panel_counter >= panels_parsed)
    //                        {
    //                            return false;
    //                        }
    //                        
    //                        
    //                        
    //                        panel_counter++;
    //                    });
    //                }

                    panel.css('width', element_width+'px');
                    
                    panels_taken_width += element_width;
                }
                
//                width_left_to_give -= element_width;
//                panels_parsed++;
            });
            
//            console.log('panels_with_percentage_width_count: '+panels_with_percentage_width_count);
//            console.log('panels_taken_width: '+panels_taken_width);
            if(panels_with_percentage_width_count > 0)
            {
                var width_left_to_give_per_element = (width - panels_taken_width)/panels_with_percentage_width_count;
                
                panels.each(function(){
                    var panel = $(this);
                    var panel_data = panel.data('sima-layout-floater');
                    var panel_data_width = panel_data.width;
                    
                    if(typeof panel_data_width !== 'string' || panel_data_width[panel_data_width.length -1] !== '%')
                    {
                        return true; /// continue
                    }
                    
//                    console.log(panel);
//                    console.log(panel);
                    
                    var panel_data_width_percent = panel_data_width.substring(0, panel_data_width.length-1);
                    
//                    console.log('panel_data_width_percent: '+panel_data_width_percent);
                    
                    panel_data_width_to_set = (panel_data_width_percent/100) * width;
                    
//                    console.log('panel_data_width_to_set: '+panel_data_width_to_set);
                    
                    if(panel_data_width_to_set > width_left_to_give_per_element)
                    {
                        panel_data_width_to_set = width_left_to_give_per_element;
                    }
                    
//                    console.log('panel_data_width_to_set: '+panel_data_width_to_set);
                    
                    panel.css('width', panel_data_width_to_set+'px');
                });
            }
            
            extra_width = Math.floor(extra_width);
            shortage_width = Math.ceil(shortage_width);
            panels.each(function(){
                if(extra_width <= 0 && shortage_width <= 0)
                {
                    return false;
                }

                var panel = $(this);
                var panel_data = panel.data('sima-layout-floater');
                var panel_data_width = panel_data.width;
                
                if(typeof panel_data_width === 'string' && panel_data_width[panel_data_width.length -1] === '%')
                {
                    return true; /// continue
                }
                
                var min_width = panel_data.min_width;
                var max_width = panel_data.max_width;
                var panel_width = panel.width();

                if(extra_width !== 0)
                {
                    if(panel_width < max_width)
                    {
                        var width_to_add = max_width-panel_width;
                        if(width_to_add > extra_width)
                        {
                            width_to_add = extra_width;
                        }
//                        width_to_add = Math.floor(width_to_add);
                        
                        panel.css('width', (panel_width+width_to_add)+'px');
                        extra_width -= width_to_add;
                    }
                }
                
                if(shortage_width !== 0)
                {
                    if(panel_width > min_width)
                    {
                        var width_to_reduce = panel_width-min_width;
                        if(width_to_reduce > shortage_width)
                        {
                            width_to_reduce = shortage_width;
                        }
//                        width_to_reduce = Math.floor(width_to_reduce);
                        
                        panel.css('width', (panel_width-width_to_reduce)+'px');
                        shortage_width -= width_to_reduce;
                    }
                }
            });
        }
    }
    
    /**
     * 
     * @param {type} floater_panel
     * @returns {Boolean}
     */
    function simaResizeFloaterHandler_validateData(floater_panel)
    {
        var result = true;
        
        var panels = floater_panel.children('.sima-layout-panel');
        var panel_count = panels.size();
        
        if(panel_count < 2)
        {
            console.warn('simaResizeFloaterHandler - need at least 2 elements');
            console.warn(floater_panel);
            result = false;
            return result;
        }
        panels.each(function(){
            var panel = $(this);
            
            var panel_data = panel.data('sima-layout-floater');
            
            if(sima.isEmpty(panel_data))
            {
                console.warn('simaResizeFloaterHandler - no panel data');
                result = false;
                return false;
            }
            if(sima.isEmpty(panel_data.min_width))
            {
                if(sima.isEmpty(panel_data.width))
                {
                    console.warn('simaResizeFloaterHandler - no panel data min width (nor width)');
                    result = false;
                    return false;
                }
                else
                {
                    panel_data.min_width = panel_data.width;
                    panel.data('sima-layout-floater', panel_data);
                }
            }
            if(sima.isEmpty(panel_data.max_width))
            {
                if(sima.isEmpty(panel_data.width))
                {
                    console.warn('simaResizeFloaterHandler - no panel data max width (nor width)');
                    result = false;
                    return false;
                }
                else
                {
                    panel_data.max_width = panel_data.width;
                    panel.data('sima-layout-floater', panel_data);
                }
            }
        });
        
        return result;
    }
    
    /**
     * funkcija koja se poziva za sima-layout-panel koji nije splitter
     * @param {type} event
     * @param {type} height - visina na koju se div postavlja
     * @param {type} width - sirina na koju se div postavlja
     * @param {type} source
     * @returns {undefined}
     */
    function simaResizeHandler(event, height, width, source)
    {
        if (event.target === this)
        {
            var _resizable_layout = $(event.target);
            if (sima.layout.log())
            {
                console.log(source);
                console.log('HANDLER_default za: ');
                console.log(_resizable_layout);
                console.log('height: ' + height + ' width: ' + width);
            }
            
            _resizable_layout.height(height);
            _resizable_layout.width(width);
            
            var _child = _resizable_layout.children('.sima-layout-panel');
            if (_child.length > 1)
            {
                console.error('POSTOJI VISE OD 1 DINAMICKOG PANELA U PANELU KOJI NIJE SPLITER-u: ');
                console.error(_resizable_layout);
            }
            else if ( _child.length > 0)
            {
                _resizable_layout.addClass('_has_layout_children');
            }
            else
            {
                event.stopPropagation();
                return;
            }

            //racunanje fiksnih panela sima-layout-fixed-panel
            var _fix_panel_height = 0;
            var _fix_panel_width = 0;
            var _fix_panels = _child.siblings('.sima-layout-fixed-panel');
            if (_fix_panels.length > 0)
            {
                if (_resizable_layout.hasClass('_vertical'))
                {
                    _fix_panels.each(function(){
                        _fix_panel_width += $(this).outerWidth(true);
                    });
                }
                if (_resizable_layout.hasClass('_horizontal'))
                {
                    _fix_panels.each(function(){
                        _fix_panel_height += $(this).outerHeight(true);
                    });
                }
            }
            
            //dohvatanje init podatka - data-sima-layout-init
            //MilosS(22.2.2018) width_cut i height_cut se prebacuje da div ne smanjuje sam sebi sirinu,
            //nego div iznad smanjuje sirinu. Poenta je da div, ako se nad njim pozove trigger, 
            //poslata sirina i visina je njegova smanjena, pa on jos dodatno smanji
            //Tako da ubuduce vazi pravilo - Poslata sirina se postavlja div-u i to je to
            //i onda kad se poziva triger nad divom poziva se njegova sirina
            //to je potrebno kada se samo on resizeuje
            
            var _init_data = _child.data('simaLayoutInit') || {};
            
            if ( typeof _init_data['height_cut'] === 'undefined' ) _init_data['height_cut'] = 0;
            if ( typeof _init_data['width_cut'] === 'undefined' ) _init_data['width_cut'] = 0;

            //NOVA VISINA treba da se umanje za cut visine i fix panele
            var _child_height = height - _init_data['height_cut'] - _fix_panel_height;

            //NOVA SIRINA
            var _child_width = width - _init_data['width_cut'] - _fix_panel_width;

            //REKURZIVNO POZIVANJE DECE
            
            sima.layout.setLayoutSize(_child,_child_height,_child_width,'TRIGGER_default_handler');
            
            event.stopPropagation();
        }
    };
    
    //funkcija koja je sustinski resize handler, ali po principu scroll-container koji treba da se izbaci, ali ga ima previse
    function simaScrollContainerResizeHandler(event, height, width, source)
    {
        if (event.target === this)
        {
            var _resizable_layout = $(event.target);
            if (sima.layout.log())
            {
                console.log(source);
                console.log('HANDLER_scroll_container za: ');
                console.log(_resizable_layout);
                console.log('height: ' + height + ' width: ' + width);
            }

            var height_cut = _resizable_layout.attr('sima-layout-panel-height-cut');
                height_cut = (typeof height_cut !== 'undefined' && height_cut !== false) ? height_cut : 0;
            height -= height_cut + $(this).position().top;//kompatibilnost sa scroll-container
            _resizable_layout.height(height);

            width = _resizable_layout.width();

            sima.layout.setLayoutSize(_resizable_layout.children('.scroll-container'),height,width,'TRIGGER_scroll_container_handler');
            event.stopPropagation();
        }
    };
    
    /** funkcija zaduzena za resize prozora
     * * pokrece se nakon 200ms nakon zadnjeg resize-a
     * 
     * 1. osvezava maintab
     * 
     * */
    var windowResizeTimeout = 200; //ms
    var windowResizeTimeoutFunction = null;
    function windowResizeHandler() {
        if (windowResizeTimeoutFunction !== null)
            clearTimeout(windowResizeTimeoutFunction);
        
        windowResizeTimeoutFunction = setTimeout(function() {
            if (!sima.useNewLayout())
            {
                sima.mainTabs.trigger_resize();
            }
            $('.sima-popup').each(function() {
                $(this).simaPopup('refresh');
            });

        }, windowResizeTimeout);
    };

}

