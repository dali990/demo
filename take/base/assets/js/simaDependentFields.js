
/* global sima */
/*
 * Zavisna polja u formama
 * Spisak akcija: condition, enable, disable, show, hide, trigger
 * Spisak eventa: onValue, onEmpty, onHide, onShow, onFocusOut, onKeyUp
 * Spisak podrzanih polja za gore navedene evente(polja koja trigeruju akciju): searchField, dropdown, textField, checkBox, numberField, passwordField, list, datetimeField, bankAccountField
 * Spisak podrzanih zavisnih polja(polja nad kojim se primenjuje akcija): searchField, dropdown, textField, checkBox, numberField, passwordField, list, datetimeField, bankAccountField
 * Primer zadavanja:
 * 'account_id' => ['searchField','relName'=>'account', 'dependent_on'=>[
        'year_id'=>[
            'onValue'=>[
                'enable',
                ['condition', 'model_filter' => 'year.ids', 'for_values'=>[1,2,3,4]]
            ],
            'onEmpty'=>[
                ['disable'],
            ],
            'onShow' => ['trigger', 'events' => ['onValue'], 'func' => ['neka js funkcija(zadaje se kao niz. Prima 4 parametra: form, dependent_on_field, dependent_field, apply_actions_func(prosledjuje se akcija ili niz akcija koje se primenjuju nad dependent_field))'],
        ],
    ], 'filters'=>$filter]
 * 
 * @returns {SIMADependentFields}
 */
function SIMADependentFields()
{
    this.init = function()
    {
        
    };
    
    this.applyDependentOnFields = function(dependent_field_selector, dependent_on_fields)
    {
        var dependent_field = $(dependent_field_selector);
        var form_obj = dependent_field.parents('.mutual_form_wrapper._model-group:first');

        $.each(dependent_on_fields, function(index, value) {
            var field_row = form_obj.find('.sima-model-form-row[data-column="'+index+'"]');
            setEventsForDependentOnField(dependent_field, field_row, value);
        });
    };
    
    function setEventsForDependentOnField(dependent_field, dependent_on_field_row, value_params)
    {
        var dependent_on_field_row_type = dependent_on_field_row.data('type');
        
        if (dependent_on_field_row_type === 'searchField' || dependent_on_field_row_type === 'relation')
        {
            dependent_on_field = dependent_on_field_row.find('.sima-ui-sf:first');
            dependent_on_field_val = dependent_on_field.simaSearchField('getValue');
            if (typeof value_params['onValue'] !== 'undefined')
            {        
                dependent_on_field.on('onValue', function(event, data){
                applyActionsToDependentField(
                    $(this), 
                    dependent_field, 
                    value_params['onValue'], 
                    data.id, 
                    {
                        is_init: (typeof data.is_init !== 'undefined') ? data.is_init : false
                    }
                    );
                });
                //inicijalno se trigeruje onValue ako je zadata vrednost
                if (value_params.onValue.init_trigger !== false && !sima.isEmpty(dependent_on_field_val.id))
                {
                    dependent_on_field.trigger('onValue', [{id:dependent_on_field_val.id, is_init: true}]);
                }
            }
            if (typeof value_params['onEmpty'] !== 'undefined')
            {
                dependent_on_field.on('onEmpty', function(event, data){                    
                    applyActionsToDependentField($(this), dependent_field, value_params['onEmpty']);
                });
                //inicijalno se trigeruje onEmpty ako je prazna vrednost
                if (sima.isEmpty(dependent_on_field_val.id))
                {
                    dependent_on_field.trigger('onEmpty');
                }
            }
            if (typeof value_params['onHide'] !== 'undefined')
            {
                dependent_on_field.on('fieldRowHideAndEmpty', function(){
                    applyActionsToDependentField($(this), dependent_field, value_params['onHide']);
                });
                if (!dependent_on_field.is(':visible'))
                {
                    dependent_on_field.trigger('fieldRowHideAndEmpty');
                }
            }
            if (typeof value_params['onShow'] !== 'undefined')
            {
                dependent_on_field.on('fieldRowShow', function(){
                    applyActionsToDependentField($(this), dependent_field, value_params['onShow']);
                });
                if (dependent_on_field.is(':visible'))
                {
                    dependent_on_field.trigger('fieldRowShow');
                }
            }
        }
        else if (dependent_on_field_row_type === 'dropdown')
        {
            dependent_on_field = dependent_on_field_row.find('select:first');
            if (typeof value_params['onValue'] !== 'undefined')
            {
                dependent_on_field.on('change', function(event){                    
                    var dropdown_value = $(this).val();                    
                    if (dropdown_value !== '')
                    {                        
                        applyActionsToDependentField($(this), dependent_field, value_params['onValue'], dropdown_value);
                    }
                });
                //inicijalno se trigeruje onValue
                dependent_on_field.trigger('change');
            }
            if (typeof value_params['onEmpty'] !== 'undefined')
            {                
                dependent_on_field.on('change', function(event){
                    var dropdown_value = $(this).val();
                    if (dropdown_value === '')
                    {
                        applyActionsToDependentField($(this), dependent_field, value_params['onEmpty']);
                    }
                });
                //inicijalno se trigeruje onEmpty
                dependent_on_field.trigger('change');
            }
            if (typeof value_params['onHide'] !== 'undefined')
            {
                dependent_on_field.on('fieldRowHideAndEmpty', function(){
                    applyActionsToDependentField($(this), dependent_field, value_params['onHide']);
                });
                if (!dependent_on_field.is(':visible'))
                {
                    dependent_on_field.trigger('fieldRowHideAndEmpty');
                }
            }
            if (typeof value_params['onShow'] !== 'undefined')
            {
                dependent_on_field.on('fieldRowShow', function(){
                    applyActionsToDependentField($(this), dependent_field, value_params['onShow']);
                });
                if (dependent_on_field.is(':visible'))
                {
                    dependent_on_field.trigger('fieldRowShow');
                }
            }
        }
        else if (dependent_on_field_row_type === 'checkBox')
        {
            dependent_on_field = dependent_on_field_row.find('input[type="checkbox"]');
            if (typeof value_params['onValue'] !== 'undefined')
            {
                dependent_on_field.on('input change', function(event){
                    var new_value = $(this).is(':checked');
                    applyActionsToDependentField($(this), dependent_field, value_params['onValue'], new_value);
                });
                //inicijalno se trigeruje onEmpty
                dependent_on_field.trigger('input');
            }
            if (typeof value_params['onHide'] !== 'undefined')
            {
                dependent_on_field.on('fieldRowHideAndEmpty', function(){
                    applyActionsToDependentField($(this), dependent_field, value_params['onHide']);
                });
                if (!dependent_on_field.is(':visible'))
                {
                    dependent_on_field.trigger('fieldRowHideAndEmpty');
                }
            }
            if (typeof value_params['onShow'] !== 'undefined')
            {
                dependent_on_field.on('fieldRowShow', function(){
                    applyActionsToDependentField($(this), dependent_field, value_params['onShow']);
                });
                if (dependent_on_field.is(':visible'))
                {
                    dependent_on_field.trigger('fieldRowShow');
                }
            }
        }
        else if (
            dependent_on_field_row_type === 'textField' || 
            dependent_on_field_row_type === 'numberField' || 
            dependent_on_field_row_type === 'passwordField' || 
            dependent_on_field_row_type === 'hidden'  
        )
        {
            dependent_on_field = dependent_on_field_row.find('input');
            if (typeof value_params['onValue'] !== 'undefined')
            {
                dependent_on_field.on('input change', function(event){
                    var new_value = $(this).val();
                    applyActionsToDependentField($(this), dependent_field, value_params['onValue'], new_value);
                });

                dependent_on_field.trigger('input');
            }
            if (typeof value_params['onHide'] !== 'undefined')
            {
                dependent_on_field.on('fieldRowHideAndEmpty', function(){
                    applyActionsToDependentField($(this), dependent_field, value_params['onHide']);
                });
                if (!dependent_on_field.is(':visible'))
                {
                    dependent_on_field.trigger('fieldRowHideAndEmpty');
                }
            }
            if (typeof value_params['onShow'] !== 'undefined')
            {
                dependent_on_field.on('fieldRowShow', function(){
                    applyActionsToDependentField($(this), dependent_field, value_params['onShow']);
                });
                if (dependent_on_field.is(':visible'))
                {
                    dependent_on_field.trigger('fieldRowShow');
                }
            }
        }
        else if (dependent_on_field_row_type === 'bankAccountField')
        {        
            dependent_on_field = dependent_on_field_row.find('input');     
            if (typeof value_params['onValue'] !== 'undefined')
            {
                dependent_on_field.on('input change', function(event){
                    var new_value = $(this).val();
                    applyActionsToDependentField($(this), dependent_field, value_params['onValue'], new_value);      
                    if(dependent_on_field.data('old_value') !== new_value)
                    {
                        dependent_on_field.data('old_value', new_value);
                    }
                });
                dependent_on_field.trigger('input');
            }
            if (typeof value_params['onFocusOut'] !== 'undefined')
            {
                dependent_on_field.data('old_value', dependent_on_field.val());                              
                dependent_on_field.on('focusout', function(event){
                    //mora timeout jer postoji problem kada se u formi klikne na revert ikonicu prvo se izvrsi fokus out pa klik,
                    //a recimo za polje bankAccountField ako je upisan nepostojeci kod za banku u trenutku fokusout-a bacice se warn korisniku,
                    //iako je on nebitan jer ce se stanje ispraviti nakon revert-a
                    setTimeout(function(){
                        var new_value = dependent_on_field.val();
                        applyActionsToDependentField(dependent_on_field, dependent_field, value_params['onFocusOut'], new_value);
                        
                        if(dependent_on_field.data('old_value') !== new_value)
                        {
                            dependent_on_field.data('old_value', new_value);
                        }
                    },200);                   
                });
                if (value_params.onFocusOut.init_trigger !== false)
                {
                    dependent_on_field.trigger('focusout');
                }
            }
            if (typeof value_params['onHide'] !== 'undefined')
            {
                dependent_on_field.on('fieldRowHideAndEmpty', function(){
                    applyActionsToDependentField($(this), dependent_field, value_params['onHide']);
                });
                if (!dependent_on_field.is(':visible'))
                {
                    dependent_on_field.trigger('fieldRowHideAndEmpty');
                }
            }
            if (typeof value_params['onShow'] !== 'undefined')
            {
                dependent_on_field.on('fieldRowShow', function(){
                    applyActionsToDependentField($(this), dependent_field, value_params['onShow']);
                });
                if (dependent_on_field.is(':visible'))
                {
                    dependent_on_field.trigger('fieldRowShow');
                }
            }
            if (typeof value_params['onKeyUp'] !== 'undefined')
            {
                dependent_on_field.on('keyup', function(event){
                    var new_value = $(this).val();
                    applyActionsToDependentField($(this), dependent_field, value_params['onKeyUp'], new_value, event);
                });
            }
        }
        else if (dependent_on_field_row_type === 'list')
        {
            dependent_on_field = dependent_on_field_row.find('.sima-model-form-search-list:first');
            if (typeof value_params['onValue'] !== 'undefined')
            {            
                dependent_on_field.on('itemsChanged', function(event, data){ 
                    applyActionsToDependentField($(this), dependent_field, value_params['onValue'], data.items_ids);
                });
                var items_ids = [];
                dependent_on_field.find('.item').each(function(){
                   items_ids.push($(this).data('id')); 
                });
                //inicijalno se trigeruje itemsChanged ako je zadata vrednost
                if (!sima.isEmpty(items_ids))
                {
                    dependent_on_field.trigger('itemsChanged', [{items_ids:items_ids}]);
                }
            }
            if (typeof value_params['onEmpty'] !== 'undefined')
            {
                dependent_on_field.on('onEmpty', function(event, data){                    
                    applyActionsToDependentField($(this), dependent_field, value_params['onEmpty']);
                });
//                inicijalno se trigeruje onEmpty ako je prazna vrednost
                if (dependent_on_field.find('.item').length === 0)
                {
                    dependent_on_field.trigger('onEmpty');
                }
            }
            if (typeof value_params['onHide'] !== 'undefined')
            {
                dependent_on_field.on('fieldRowHideAndEmpty', function(){
                    applyActionsToDependentField($(this), dependent_field, value_params['onHide']);
                });
                if (!dependent_on_field.is(':visible'))
                {
                    dependent_on_field.trigger('fieldRowHideAndEmpty');
                }
            }
            if (typeof value_params['onShow'] !== 'undefined')
            {
                dependent_on_field.on('fieldRowShow', function(){
                    applyActionsToDependentField($(this), dependent_field, value_params['onShow']);
                });
                if (dependent_on_field.is(':visible'))
                {
                    dependent_on_field.trigger('fieldRowShow');
                }
            }
            
//            dependent_on_field.on('onRemoveItem', function(event, data){               
//                applyActionsToDependentField($(this), dependent_field, value_params['onValue'], data.id);
//            });
        }
        else if (dependent_on_field_row_type === 'datetimeField')
        {
            dependent_on_field = dependent_on_field_row.find('input:first');
            if (typeof value_params['onValue'] !== 'undefined')
            {            
                dependent_on_field.on('onValue', function(event, data){
                    applyActionsToDependentField($(this), dependent_field, value_params['onValue'], data.val);
                });
                var val = dependent_on_field.val();
                //inicijalno se trigeruje onValue ako je zadata vrednost
                if(!sima.isEmpty(val))
                {
                    dependent_on_field.trigger('onValue', [{val:val}]);
                }
            }
            if (typeof value_params['onEmpty'] !== 'undefined')
            {
                
                dependent_on_field.on('onEmpty', function(event, data){
                    applyActionsToDependentField($(this), dependent_field, value_params['onEmpty']);
                });
//                inicijalno se trigeruje onEmpty ako je prazna vrednost
                if(sima.isEmpty(dependent_on_field.val()))
                {
                    dependent_on_field.trigger('onEmpty');
                }
            }
            if (typeof value_params['onHide'] !== 'undefined')
            {
                dependent_on_field.on('fieldRowHideAndEmpty', function(){
                    applyActionsToDependentField($(this), dependent_field, value_params['onHide']);
                });
                if (!dependent_on_field.is(':visible'))
                {
                    dependent_on_field.trigger('fieldRowHideAndEmpty');
                }
            }
            if (typeof value_params['onShow'] !== 'undefined')
            {
                dependent_on_field.on('fieldRowShow', function(){
                    applyActionsToDependentField($(this), dependent_field, value_params['onShow']);
                });
                if (dependent_on_field.is(':visible'))
                {
                    dependent_on_field.trigger('fieldRowShow');
                }
            }
            
//            var input_field = $('#$uniq').children('input');
//            $('#$uniq').on('click','.sima-icon._remove',function(){ 
//                input_field.val(''); 
//                input_field.trigger('onEmpty'); 
//            });
//            input_field.on('change', function(){
//                console.log('test');
//                val = input_field.attr('value');
//                input_field.trigger('onValue', [{val: val}] ); 
//            });
            
//            dependent_on_field.on('onRemoveItem', function(event, data){               
//                applyActionsToDependentField($(this), dependent_field, value_params['onValue'], data.id);
//            });
        }
    }    

    function applyActionsToDependentField(dependent_on_field, dependent_field, value_params, value, additional_params)
    {
        if ($.isArray(value_params))
        {
            $.each(value_params, function(index, _value) {
                applyActionToDependentField(dependent_on_field, dependent_field, _value, value, additional_params);
            });
        }
        else
        {
            applyActionToDependentField(dependent_on_field, dependent_field, value_params, value, additional_params);
        }       
    }
    
    function applyActionToDependentField(dependent_on_field, dependent_field, value_params, value, additional_params)
    {
        var action_type = getActionTypeFromValue(value_params);
        applyActionToDependentFieldInternal(action_type, dependent_on_field, dependent_field, value_params, value, additional_params);
    }
    
    function applyActionToDependentFieldInternal(action_type, dependent_on_field, dependent_field, value_params, value, additional_params)
    {
        if (action_type === 'condition')
        {            
            applyConditionToDependentField(dependent_field, value_params, value);
        }
        else if (action_type === 'disable')
        {
            disableDependentField(dependent_field, value_params, value);
        }
        else if (action_type === 'enable')
        {           
            enableDependentField(dependent_field, value_params, value);
        }
        else if (action_type === 'hide')
        {
            hideDependentField(dependent_field, value_params, value);
        }
        else if (action_type === 'show')
        {
            showDependentField(dependent_field, value_params, value);
        }
        else if (action_type === 'trigger')
        {
            triggerDependentOnFieldEvents(dependent_on_field, dependent_field, value_params, value, additional_params);
        }
    }
    
    function applyConditionToDependentField(dependent_field, value_params, value)
    {
        var dependent_field_type = dependent_field.parents('.sima-model-form-row:first').data('type');
        if (typeof value_params['model_filter'] === 'undefined')
        {
            return;
        }
        if (dependent_field_type === 'searchField' || dependent_field_type === 'relation')
        {
            if (sima.isEmpty(value))
            {
                value = -1;
            }
            var model_filter = {};
            if (typeof value_params['for_values'] !== 'undefined' && typeof value !== 'undefined')
            {
                if (isValueInArray(value_params['for_values'], value))
                {
                    model_filter = convertModelFilterStringToObjectAndSelectValue(value_params['model_filter'], value);
                }
            }
            else
            {
                model_filter = convertModelFilterStringToObjectAndSelectValue(value_params['model_filter'], value);
            }
            
            dependent_field.simaSearchField('setFilter', model_filter, dependent_field.attr('id'));
        }
    }
    
    function disableDependentField(dependent_field, value_params, value)
    {
        var set_empty = typeof value_params['set_empty_on_disable'] === 'undefined' || value_params['set_empty_on_disable'] === true;
        if (typeof value_params['for_values'] !== 'undefined' && typeof value !== 'undefined')
        {
            if (isValueInArray(value_params['for_values'], value))
            {
                dependentFieldRowDisableAndEmpty(dependent_field.parents('.sima-model-form-row:first'), set_empty);
            }
            else
            {
                dependent_field.parents('.sima-model-form-row:first').removeClass('_disabled');
            }
        }
        else
        {
            dependentFieldRowDisableAndEmpty(dependent_field.parents('.sima-model-form-row:first'), set_empty);
        }
    }
    
    function dependentFieldRowDisableAndEmpty(dependent_field_row, set_empty)
    {
        dependent_field_row.addClass('_disabled');
        if (set_empty)
        {
            emptyDependedField(dependent_field_row);
        }
    }
    
    function enableDependentField(dependent_field, value_params, value)
    {
        var set_empty = typeof value_params['set_empty_on_disable'] === 'undefined' || value_params['set_empty_on_disable'] === true;
        if (typeof value_params['for_values'] !== 'undefined' && typeof value !== 'undefined')
        {
            if (isValueInArray(value_params['for_values'], value))
            {
                dependent_field.parents('.sima-model-form-row:first').removeClass('_disabled');
            }
            else
            {
                dependentFieldRowDisableAndEmpty(dependent_field.parents('.sima-model-form-row:first'), set_empty);
            }
        }
        else
        {
            dependent_field.parents('.sima-model-form-row:first').removeClass('_disabled');
        }
    }
    
    function hideDependentField(dependent_field, value_params, value)
    {
        var to_hide = true;
        if (typeof value_params['for_values'] !== 'undefined' && typeof value !== 'undefined')
        {
            if (!isValueInArray(value_params['for_values'], value))
            {
                to_hide = false;
            }
        }
        
        if(to_hide === true)
        {
            var set_empty = typeof value_params['set_empty_on_hide'] === 'undefined' || value_params['set_empty_on_hide'] === true;
            dependentFieldRowHideAndEmpty(dependent_field, set_empty);
        }
        else
        {
            dependentFieldRowShow(dependent_field);
        }
    }
    
    function dependentFieldRowHideAndEmpty(dependent_field, set_empty)
    {
        var dependent_field_row = dependent_field.parents('.sima-model-form-row:first');
        if (set_empty)
        {
            emptyDependedField(dependent_field_row);
        }
        dependent_field_row.hide();
        dependent_field.trigger('fieldRowHideAndEmpty');
    }
    
    function showDependentField(dependent_field, value_params, value)
    {
        if (typeof value_params['for_values'] !== 'undefined' && typeof value !== 'undefined')
        {
            if (isValueInArray(value_params['for_values'], value))
            {
                dependentFieldRowShow(dependent_field);
            }
            else
            {
                var set_empty = typeof value_params['set_empty_on_hide'] === 'undefined' || value_params['set_empty_on_hide'] === true;
                dependentFieldRowHideAndEmpty(dependent_field, set_empty);
            }
        }
        else
        {
            dependentFieldRowShow(dependent_field);
        }
    }
    
    function dependentFieldRowShow(dependent_field)
    {
        var dependent_field_row = dependent_field.parents('.sima-model-form-row:first');
        dependent_field_row.show();
        dependent_field.trigger('fieldRowShow');
    }
    
    function emptyDependedField(dependent_field_row)
    {
        var dependent_field_type = (!sima.isEmpty(dependent_field_row.data('type')))?dependent_field_row.data('type'):null;
        if (dependent_field_type === 'searchField' || dependent_field_type === 'relation')
        {
            var dependent_field = dependent_field_row.find('.sima-ui-sf:first');  
            dependent_field.simaSearchField('empty');
        }
        else if(dependent_field_type === 'dropdown')
        {
            dependent_field_row.find('select').val('');
        }
        else if(dependent_field_type === 'textField' || dependent_field_type === 'passwordField')
        {
            dependent_field_row.find('input').val('');
        }
        else if(dependent_field_type === 'numberField')
        {
            dependent_field_row.find('input').val('').numberField('resetValue');
        }
        else if(dependent_field_type === 'list')
        {
            sima.model.searchFormListSetItems(dependent_field_row.find('.sima-model-form-search-list:first'), []);
        }
        else if (dependent_field_type === 'datetimeField')
        {            
            dependent_field_row.find('input').val('').trigger('change');
        }
        else if(dependent_field_type === 'checkBox')
        {
            var dependent_field_row_input = dependent_field_row.find('input');
            dependent_field_row_input.prop('checked', false);
            dependent_field_row_input.trigger('change');
        }
        else if (dependent_field_type !== 'checkBox')
        {            
            var message = 'emptyDependedField - not implemented for type: '+dependent_field_type;            
            sima.addError('emptyDependedField', message);
        }
    }
    
    function triggerDependentOnFieldEvents(dependent_on_field, dependent_field, value_params, value, additional_params)
    { 
        if (!sima.isEmpty(value_params['events']))
        { 
            if ($.isArray(value_params['events']))
            {
                $.each(value_params['events'], function(index, _value) {
                    dependent_on_field.trigger(_value);
                });
            }
            else
            {
                dependent_on_field.trigger(value_params['events']);
            }
        }
        else if (!sima.isEmpty(value_params['func']))
        {
            var form = dependent_on_field.parents('form:first');
            var extended_func = $.extend(true, [], value_params['func']);
            extended_func.push(form);
            extended_func.push(dependent_on_field);
            extended_func.push(dependent_field);
            extended_func.push(function(actions){
                applyActionsToDependentField(dependent_on_field, dependent_field, actions);
            });
            if (!sima.isEmpty(additional_params)){
                extended_func.push(additional_params);
            }
            else
            {
                extended_func.push({});
            }
            sima.callFunctionFromArray(extended_func);
        }
    }
    
    function isValueInArray(values, value)
    {
        if (!$.isArray(values))
        {
            values = [values];
        }

        var founded = false;
        $.each(values, function(ind, val) {
            if (val == value) //bitno je da bude == umesto === jer value moze biti i string
            {
                founded = true;
            }
        });

        return founded;
    }
    
    /**
     * Konvertuje model_filter koji je zapisan kao string(npr. country.ids) 
     * u model_filter kao objekat i postavlja mu prosledjenu vrednost(npr. {country:{ids:selected_value}})     
     * @param {string} model_filter_as_string - model_filter koji je zapisan kao string(npr. country.ids)
     * @param {string/int} selected_value - vrednost koja se postavlja na kraju
     * @returns {object}
     */
    function convertModelFilterStringToObjectAndSelectValue(model_filter_as_string, selected_value)
    {
        var model_filter_as_array = model_filter_as_string.split('.');
        var key = model_filter_as_array.shift();   
        if (model_filter_as_array.length > 0)
        {
            model_filter_as_string = model_filter_as_array.join('.');
            var val = convertModelFilterStringToObjectAndSelectValue(model_filter_as_string, selected_value);
            var curr_obj = {};
            curr_obj[key] = val;
            return curr_obj;
        }
        else
        {            
            var curr_obj = {};
            curr_obj[key] = selected_value;
            return curr_obj;
        }
    };    
    
    function getActionTypeFromValue(value)
    {
        var action = value;        
        if (typeof value === 'object' || $.isArray(value))
        {
            action = value[0];            
        }
        
        return action;
    }
}
