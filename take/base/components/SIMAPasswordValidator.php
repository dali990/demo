<?php

class SIMAPasswordValidator extends SIMAComponent
{
    public $strength = [];
    public $change_request_type = null; //ON_LOGIN, DURING_THE_WORK
    public $change_deadline = null; //days number
    //ako hocemo da iskljucimo safe zonu onda mora u components.localhost.config da setujemo na null. Jer ako se obrise, povuci ce iz
    //components.config
    public $safe_zone_ip_range = null;
    
    //Tipovi zahteva za promenu sifre nakon isteka:
    //ON_LOGIN - Korisnik ne moze da se uloguje dok ne promeni sifru
    //DURING_THE_WORK - Stalno iskace obavestenje korisniku da treba da promeni sifru
    const ON_LOGIN = 'ON_LOGIN';
    const DURING_THE_WORK = 'DURING_THE_WORK';
    
    public function init(){}
    
    /**
     * Proverava da li je sifra istekla
     * @param string $password_last_change - datum kada je poslednji put promenjena sifra
     * @return boolean
     */
    public function isExpired($password_last_change=null)
    {
        $check_expired = $this->checkExpired($password_last_change);

        return is_null($check_expired) || $check_expired < 0;
    }
    
    /**
     * Proverava da li je sifra istekla
     * @param string $password_last_change - datum kada je poslednji put promenjena sifra
     * @return 0 - nije istekla, null - istekla ali se ne zna broj isteklih dana, -broj - broj isteklih dana, +broj - broj dana do isteka
     */
    public function checkExpired($password_last_change=null)
    {
        //ako nije zadat datum poslednje promene, znaci da je istekla ali se ne zna broj isteklih dana pa se vraca null
        if (empty($password_last_change))
        {
            return null;
        }
        //ako nije zadat deadline onda sifra nije istekla
        else if (empty($this->change_deadline))
        {
            return 0;
        }
        //inace se preracunava broj isteklih dana ili jos koliko treba do isteka
        else
        {
            $curr_datetime = new SIMADateTime();
            $last_change_datetime = new SIMADateTime($password_last_change);
            $expired_datetime = $curr_datetime->sub(new DateInterval("P{$this->change_deadline}D"));
            
            return $last_change_datetime->diffDays($expired_datetime);
        }
    }
    
    /**
     * Proverava jacinu sifre na osnovu zadatih parametara, i vraca niz gresaka za koje nije ispunjena jacina sifre
     * @param string $password
     * @return array
     */
    public function checkStrength($password)
    {
        $messages = [];
        
        if (!empty($this->strength))
        {
            $strength_params = $this->strength;
            
            if (isset($strength_params['min_length']) && strlen($password) < $strength_params['min_length'])
            {
                array_push($messages, Yii::t('BaseModule.SIMAPasswordValidator', 'WrongPasswordLength', ['{min_length}' => $strength_params['min_length']]));
            }

            if (
                    isset($strength_params['must_have_lowercase_letters']) && $strength_params['must_have_lowercase_letters'] === true && 
                    strlen(preg_replace('/[^a-z]+/', '', $password)) === 0
                )
            {
                array_push($messages, Yii::t('BaseModule.SIMAPasswordValidator', 'PasswordMustHaveLowercaseLetters'));
            }
            
            if (
                    isset($strength_params['must_have_uppercase_letters']) && $strength_params['must_have_uppercase_letters'] === true && 
                    strlen(preg_replace('/[^A-Z]+/', '', $password)) === 0
                )
            {
                array_push($messages, Yii::t('BaseModule.SIMAPasswordValidator', 'PasswordMustHaveUppercaseLetters'));
            }
          
            if (
                    isset($strength_params['must_have_numbers']) && $strength_params['must_have_numbers'] === true && 
                    strlen(preg_replace('/[^0-9]+/', '', $password)) === 0
                )
            {
                array_push($messages, Yii::t('BaseModule.SIMAPasswordValidator', 'PasswordMustHaveNumbers'));
            }

            if (
                    isset($strength_params['must_have_special_characters']) && 
                    $strength_params['must_have_special_characters'] === true && 
                    !empty($strength_params['allowed_special_characters']))
            {
                if (!preg_match('/['.preg_quote($strength_params['allowed_special_characters'], '/').']/', $password))
                {
                    array_push($messages, Yii::t('BaseModule.SIMAPasswordValidator', 'PasswordMustHaveSpecialCharacters', [
                        '{allowed_special_characters}' => $strength_params['allowed_special_characters']
                    ]));
                }
            }
        }
        
        return $messages;
    }
    
    public function isUserHasAlreadyUsedPassword($user_id, $password)
    {
        $user_to_password = UserToPassword::model()->findByAttributes([
            'user_id' => $user_id,
            'password' => $this->getPasswordHash($password)
        ]);
        
        return !is_null($user_to_password);
    }
    
    public function getPasswordHash($password)
    {
        return md5($password);
    }
}

