<?php

class QRCodeComponent
{
    static public function generateQRCode($model)
    {
        if(isset($model->QRCode))
        {
            throw new SIMAWarnException(Yii::t('BaseModule.QRCode', 'ModelAlreadyHaveQRCode'));
        }
        
        $qrCodeModel = new QR();
        $qrCodeModel->model = get_class($model);
        $qrCodeModel->model_id = $model->id;
        $qrCodeModel->save();
        
        return $qrCodeModel;
    }
    
    static public function generateQRPngTemp(QR $qr, $size=3)
    {
        $temp_file_repository_qrcache = Yii::app()->params['temp_file_repository_qrcache'];
        if(!file_exists($temp_file_repository_qrcache))
        {
            throw new Exception('temp_file_repository_qrcache not exists');
        }
        if(!is_dir($temp_file_repository_qrcache))
        {
            throw new Exception('temp_file_repository_qrcache not dir');
        }
        
        $temp_file = new TemporaryFile('', 'qrimage', 'png');
        QRCodeGenerator::png($qr->qr_data, $temp_file->getFullPath(), QR_ECLEVEL_L, $size);
        return $temp_file;
    }
    
    static public function GenerateHTMLForModelQRCode(SIMAActiveRecord $model, $size=3)
    {
        if(get_class($model) === QR::class && isset($model->base_model))
        {
            $model = $model->base_model;
        }
                
        if(is_null($model->QRCode))
        {
            QRCodeComponent::generateQRCode($model);
        }
        
        $temp_file = QRCodeComponent::generateQRPngTemp($model->QRCode, $size);
        $pngTempFilePath = $temp_file->getFullPath();
        
        $html = Yii::app()->controller->renderPartial('base.views.qRCode.qrcode_'.$size, array(
            'model' => $model,
            'pngTempFilePath' => $pngTempFilePath
        ), true, false);
                
        return $html;
    }
    
//    static public function GenerateTempPdfForModelsQRCodes(array $models, $size=3)
    static public function GenerateTempPdfForModelsQRCodes($update_func=null)
    {
//        $criteria = new SIMADbCriteria([
//            'Model' => 'InventoryNumber',
//            'model_filter' => [
//                'filter_scopes' => [
//                    'available'
//                ],
//                'display_scopes' => [
////                    'orderIdASC'
//                    'byNumber'
//                ],
//                'limit' => '100',
//                'offset' => '900'
//            ]
//        ]);
//        $inventoryNumbers = InventoryNumber::model()->findAll($criteria);
//        
//        $qrCodes = [];
//        
//        foreach($inventoryNumbers as $in)
//        {
//            $qrCodes[] = $in->QRCode;
//        }
//        
//        $htmlCodes = [];
//        
//        foreach($qrCodes as $model)
//        {
//            $htmlCode = QRCodeComponent::GenerateHTMLForModelQRCode($model, $size);
//            
//            $htmlCodes[] = $htmlCode;
//        }
//        
        $tempFileName = TempDir::createNewTempFile('', 'SIMATablePDF', 'pdf');
        $tempFileFullPath = TempDir::getFullPath($tempFileName);
        
        $pdf = new SIMATCPdf('L', 'mm', array( 1000,   500), true, 'UTF-8', false);
//        
//        if ($size == 1)
//        {
//            $pdf->ColoredTable(25, $htmlCodes, 38, 13, 1);//QR1
//        }
//        elseif ($size == 2)
//        {
//            $pdf->ColoredTable(10, $htmlCodes, 95, 13, 1);//QR2
//        }
//        else //size 3
//        {
//            $pdf->ColoredTable(25, $htmlCodes, 38, 26, 1); //QR3
//        }
////        $pdf = new SIMATCPdf('P', 'mm', 'A4', true, 'UTF-8', false);
////        $pdf = new SIMATCPdf('P', 'mm', array( 850,   5000), true, 'UTF-8', false); //QR3
////        $pdf = new SIMATCPdf('L', 'mm', array( 1000,   500), true, 'UTF-8', false); //QR2
//         //QR1
//        
//        // print colored table
//        
////        
////        
//        
//        $criteria = new SIMADbCriteria([
//            'Model' => 'InventoryNumber',
//            'model_filter' => [
//                'filter_scopes' => [
//                    'available'
//                ],
//                'display_scopes' => [
////                    'orderIdASC'
//                    'byNumber'
//                ],
//                'limit' => '100',
//                'offset' => '1000'
//            ]
//        ]);
//        $inventoryNumbers = InventoryNumber::model()->findAll($criteria);
//        
//        $qrCodes = [];
//        
//        foreach($inventoryNumbers as $in)
//        {
//            $qrCodes[] = $in->QRCode;
//        }
//        
//        $htmlCodes = [];
//        
//        foreach($qrCodes as $model)
//        {
//            $htmlCode = QRCodeComponent::GenerateHTMLForModelQRCode($model, 2);
//            
//            $htmlCodes[] = $htmlCode;
//        }
//        
////        $tempFileName = TempDir::createNewTempFile('', 'SIMATablePDF', 'pdf');
////        $tempFileFullPath = TempDir::getFullPath($tempFileName);
//        
////        $pdf = new SIMATCPdf('L', 'mm', array( 1000,   90), true, 'UTF-8', false);
//        
////        if ($size == 1)
////        {
////            $pdf->ColoredTable(25, $htmlCodes, 38, 13, 1);//QR1
////        }
////        elseif ($size == 2)
////        {
//            $pdf->ColoredTable(10, $htmlCodes, 95, 13, 1, false);//QR2
////        }
////        else //size 3
////        {
////            $pdf->ColoredTable(25, $htmlCodes, 38, 26, 1); //QR3
////        }
        
        $criteria = new SIMADbCriteria([
            'Model' => InventoryNumber::class,
            'model_filter' => [
                'filter_scopes' => [
                    'available'
                ],
                'display_scopes' => [
//                    'orderIdASC'
                    'byNumber'
                ],
//                'limit' => '450',
//                'offset' => '2250'
            ]
        ]);
        $inventoryNumbers = InventoryNumber::model()->findAll($criteria);
        
        $htmlCodes = [];
        $qrCodes_count = count($inventoryNumbers);
        $qrCodes_counter = 1;
        foreach($inventoryNumbers as $model)
        {
            if (!empty($update_func))
            {
                $update_func($qrCodes_counter++/$qrCodes_count);
            }
            
            $htmlCode = QRCodeComponent::GenerateHTMLForModelQRCode($model, 3);
            
            $htmlCodes[] = $htmlCode;
        }
        
//        $tempFileName = TempDir::createNewTempFile('', 'SIMATablePDF', 'pdf');
//        $tempFileFullPath = TempDir::getFullPath($tempFileName);
        
//        $pdf = new SIMATCPdf('L', 'mm', array( 1000,   90), true, 'UTF-8', false);
        
//        if ($size == 1)
//        {
//            $pdf->ColoredTable(25, $htmlCodes, 38, 13, 1);//QR1
//        }
//        elseif ($size == 2)
//        {
//            $pdf->ColoredTable(10, $htmlCodes, 95, 13, 1, false);//QR2
//        }
//        else //size 3
//        {
//            $pdf->ColoredTable(25, $htmlCodes, 38, 26, 1, true); //QR3
            $pdf->ColoredTable(8, $htmlCodes, 120, 80, 1, true); //QR3
//        }
        
            
        $pdf->Output($tempFileFullPath, 'F');
        
        return $tempFileName;
    }
}

