<?php

class SIMAModelMerger
{
    
    /**
     * stvari koje modeli mogu opciono definisati u modelsettings pod "merge" kljucem da bi se olaksao merge:
     * 
     * columns_that_may_differ - array of strings
     *  kolone koje se mogu razlikovati izmedju modela
     *  ako je u jednom vrednost prazna, uzimaju se iz onog koji ima informacije
     *  ako oba imaju informacije, a razlikuju se, ostaju od originalnog - model1
     * 
     * columns_that_are_equal_trimmed - array of strings
     *  podskup od $columns_that_may_differ - inace ce biti ignorisano
     *  ako se vrednosti razlikuju, a mogu biti jednake nakon sto se uradi trim nad oba
     * 
     * columns_to_empty - array of string
     *  kolone koje treba isprazniti - staviti na null - da bi mogao model2 da se obrise i/ili model1 sacuva
     * 
     * hasmany_relationmodels_to_merge - array
     *  niz oblika $key=>$value gde je
     *      $key naziv relacije
     *      a $value je niz atributa po kojima se trazi jednakost za merge
     * 
     * relations_to_skip - array
     *  relacije koje ne treba spajati
     *  primer:
     *      u adresama postoje relacije partner (koja se odredjuje na osnovu partners_to_addresses) i partner_to_address
     *      nakon sto se prvo obradi relacija partner, partner_to_address nema rezultata
     *      i obrnuto
     */
    private $_columns_that_may_differ = [];
    private $_columns_that_are_equal_trimmed = [];
    private $_columns_to_empty = [];
    private $_hasmany_relationmodels_to_merge = [];
    private $_relations_to_skip = [];
    
    /**
     *  modeli za brisanje jer model1 vec ima vezu ka istoj relaciji
     * @var array of models
     */
    private $many_many_relation_models_to_delete = [];
    
    private $_run_in_transaction = true;
    
    private $_save_merge_errors = false;
    private $_merge_errors = [];
    
    private $_internal_update_counter = 0;
    private $_send_update_percentage_events = false;
    private $_update_percentage_max = 100;
            
    private $model1 = null;
    private $model2 = null;
    private $columns = null;
    
    /**
     * 
     * @param SIMAActiveRecord $model1 - osnovni, koji ostaje nakon merge-a
     * @param SIMAActiveRecord $model2 - sa kog se prebacuju informacije i koji se brise
     * @param bool $send_update_percentage_events
     */
    public function __construct(SIMAActiveRecord $model1, SIMAActiveRecord $model2, $send_update_percentage_events=false)
    {
        $this->model1 = $model1;
        $this->model2 = $model2;
        $this->_send_update_percentage_events = $send_update_percentage_events;
        
        $db_columns = $this->model1->getMetaData()->tableSchema->columns;
        $this->columns = array_keys($db_columns);
        
        $model_merge_options = $this->model1->modelSettings('merge_options');
        if(isset($model_merge_options['columns_that_may_differ']))
        {
            $this->_columns_that_may_differ = $model_merge_options['columns_that_may_differ'];
        }
        if(isset($model_merge_options['columns_that_are_equal_trimmed']))
        {
            $this->_columns_that_are_equal_trimmed = $model_merge_options['columns_that_are_equal_trimmed'];
        }
        if(isset($model_merge_options['columns_to_empty']))
        {
            $this->_columns_to_empty = $model_merge_options['columns_to_empty'];
        }
        if(isset($model_merge_options['hasmany_relationmodels_to_merge']))
        {
            $this->_hasmany_relationmodels_to_merge = $model_merge_options['hasmany_relationmodels_to_merge'];
        }
        if(isset($model_merge_options['relations_to_skip']))
        {
            $this->_relations_to_skip = $model_merge_options['relations_to_skip'];
        }
    }
    
    public function run()
    {
        $started_transaction = false;
        if($this->_run_in_transaction === true)
        {
            $transaction = null;
            $current_transaction = Yii::app()->db->getCurrentTransaction();
            if(is_null($current_transaction))
            {
                $transaction = Yii::app()->db->beginTransaction();
                $started_transaction = true;
            }
        }
        
        try
        {
            $this->validation();

            $this->merge();

            $this->deleteAfterMerge();
            
            if($started_transaction === true)
            {
                $transaction->commit();
            }
        }
        catch(Exception $e)
        {
            if($started_transaction === true)
            {
                $current_transaction = Yii::app()->db->getCurrentTransaction();
                if(!is_null($current_transaction))
                {
                    $current_transaction->rollback();
                }
            }
            
            if($this->_save_merge_errors === true)
            {
                $this->_merge_errors[] = $e->getMessage();
            }
            else
            {
                throw $e;
            }
        }
    }
    
    public function setInternalUpdateCounter($new_val)
    {
        $this->_internal_update_counter = $new_val;
    }
    
    public function setUpdatePercentageMax($new_val)
    {
        $this->_update_percentage_max = $new_val;
    }
    
    public function setRunInTransaction($new_val)
    {
        $this->_run_in_transaction = $new_val;
    }
    
    /**
     * 
     */
    public function saveMergeErrors()
    {
        $this->_save_merge_errors = true;
    }
    
    /**
     * vraca niz poruka o problemima prilikom merge-a
     * pakuje se samo 
     * @return array of strings
     */
    public function getMergeErrors()
    {
        return $this->_merge_errors;
    }
    
    private function deleteAfterMerge()
    {
        foreach($this->many_many_relation_models_to_delete as $model_to_delete)
        {
            $model_to_delete->delete();
        }
        $this->model2->delete();
    }
    
    private function merge()
    {
        $this->sendUpdatePercentageEvent($this->parseUpdatePercent(1));
        
        $model1_had_change = false;
        $model2_had_change = false;
        
        foreach($this->columns as $column)
        {
            $model1_col_val = $this->model1->$column;
            $model2_col_val = $this->model2->$column;
            
            if(in_array($column, $this->_columns_that_may_differ))
            {
                if(
                    in_array($column, $this->_columns_that_are_equal_trimmed)
                    && !empty($model1_col_val) && !empty($model2_col_val) && trim($model1_col_val)===trim($model2_col_val)
                )
                {
                    /// moze se samo dodati jer se druga vrednosti ionako ignorise
                    $model2_col_val = trim($model2_col_val).'temp_val';
                    $this->model2->$column = $model2_col_val;
                    $model2_had_change = true;
                }
                else
                {
                    continue;
                }
            }
            
            if(empty($model2_col_val))
            {
                continue;
            }
            
            if(empty($model1_col_val))
            {
                $this->model1->$column = $model2_col_val;
                $model1_had_change = true;
            }
        }
        
        $this->sendUpdatePercentageEvent($this->parseUpdatePercent(9));
            
        $this->merge_relations();
        
        $this->sendUpdatePercentageEvent($this->parseUpdatePercent(80));
        
        foreach($this->_columns_to_empty as $column)
        {
            $this->model2->$column = null;
            $model2_had_change = true;
        }
        
        if($model2_had_change === true)
        {
            $this->model2->save();
        }
        if($model1_had_change === true)
        {
            $this->model1->save();
        }
        
        $this->sendUpdatePercentageEvent($this->parseUpdatePercent(15));
    }
    
    /**
     * ne sme da se redefinise skroz
     * tj mora bar da se pozove ovaj roditeljski
     */
    private function validation()
    {
        $model1_class = get_class($this->model1);
        $model2_class = get_class($this->model2);
        if($model1_class !== $model2_class)
        {
            throw new Exception('not same class');
        }
        
        foreach($this->columns as $column)
        {
            $model1_col_val = $this->model1->$column;
            $model2_col_val = $this->model2->$column;
                
            if(in_array($column, $this->_columns_that_are_equal_trimmed))
            {
                $model1_col_val = trim($model1_col_val);
                $model2_col_val = trim($model2_col_val);
            }
            
            if(!empty($model1_col_val) && !empty($model2_col_val) && $model1_col_val!==$model2_col_val && !in_array($column, $this->_columns_that_may_differ))
            {
                $column_label = $this->model1->getAttributeLabel($column);
                $column1_display_value = $this->model1->getAttributeDisplay($column);
                $column2_display_value = $this->model2->getAttributeDisplay($column);
                throw new Exception('razlikuje se kolona "'.$column_label.'" - $model1: '.$column1_display_value.' - $model2: '.$column2_display_value);
            }
        }
    }
    
    private function merge_relations()
    {
        $relations = $this->model2->relations();
        $relations_count = count($relations);
        $percentage_per_relation = $this->parseUpdatePercent(70/$relations_count);
        foreach($relations as $relation_name => $relation_data)
        {
            if(in_array($relation_name, $this->_relations_to_skip))
            {
                $this->sendUpdatePercentageEvent($percentage_per_relation);
                continue;
            }
            
            $model2_relation_value = $this->model2->$relation_name;
            if(empty($model2_relation_value))
            {
                $this->sendUpdatePercentageEvent($percentage_per_relation);
                continue;
            }

            $relation_type = $relation_data[0];
            if($relation_type === SIMAActiveRecord::MANY_MANY)
            {
                $this->merge_relations_manymany($relation_data, $model2_relation_value);
            }
            else if($relation_type === SIMAActiveRecord::HAS_MANY)
            {
                $percentage_per_relation_element = $percentage_per_relation/count($model2_relation_value);
                $this->merge_relations_hasmany($relation_name, $relation_data, $model2_relation_value, $percentage_per_relation_element);
            }
            else if($relation_type === SIMAActiveRecord::HAS_ONE)
            {
                $this->merge_relations_hasone($relation_data, $model2_relation_value);
            }
            
            $this->sendUpdatePercentageEvent($percentage_per_relation);
            
            unset($this->model1->$relation_name);
            unset($this->model2->$relation_name);
            gc_collect_cycles();
        }
    }
    
    private function merge_relations_manymany($relation_data, $model2_relation_value)
    {
        $table_string = $relation_data[2];
        $parsed_table_string = $this->manymanyrelations_parsetablestring($table_string);
        $table_name = $parsed_table_string['table_name'];
        $base_column = $parsed_table_string['base_column'];
        $rel_column = $parsed_table_string['rel_column'];
        
        $configTablesToModelsMapper = SIMAModule::getTablesToModelsMapped();
        $table_name_model = $configTablesToModelsMapper[$table_name];


        foreach($model2_relation_value as $relation_element)
        {
            $model2_existing_relation = $table_name_model::model()->findByAttributes([
                $base_column => $this->model2->id,
                $rel_column => $relation_element->id
            ]);
            if(empty($model2_existing_relation))
            {
                $new_exception = new Exception('$model2_existing_relation ne postoji'
                                                .' - $table_name_model: '.$table_name_model
                                                .' - $base_column: '.$base_column
                                                .' - $this->model2->id: '.$this->model2->id
                                                .' - $rel_column: '.$rel_column
                                                .' - $relation_element->id: '.$relation_element->id
                );
                throw $new_exception;
            }
            
            /**
             * ako vec postoji model1 u vezi po relaciji
             * ne dodaje se vec se ova vezana za model2 uklanja
             */
            $model1_existing_relation_count = $table_name_model::model()->countByAttributes([
                $base_column => $this->model1->id,
                $rel_column => $relation_element->id
            ]);
            if($model1_existing_relation_count > 0)
            {
                $this->many_many_relation_models_to_delete[] = $model2_existing_relation;
                continue;
            }

            $model2_existing_relation->$base_column = $this->model1->id;
            $model2_existing_relation->save();
        }
    }
    
    /**
     * 
     * @param string $relation_name
     * @param array $relation_data
     * @param array $model2_relation_value
     */
    private function merge_relations_hasmany($relation_name, $relation_data, $model2_relation_value, $percentage_per_relation_element)
    {
        $model_name = $relation_data[1];
        $relation_element_column = $relation_data[2];
        
        foreach($model2_relation_value as $relation_element)
        {
            $performed_merge = $this->merge_relations_hasmany_submerge($relation_name, $relation_element_column, $relation_element, $model_name, $percentage_per_relation_element);
            if($performed_merge === false)
            {
                $this->merge_relations_hasmany_modifyRelationalElement($relation_element, $relation_element_column);
            }
        }
    }
    
    private function merge_relations_hasmany_modifyRelationalElement($relation_element, $relation_element_column)
    {
        $relation_element->$relation_element_column = $this->model1->id;

        try
        {
            $relation_element->save();
        }
        catch(Exception $e)
        {
            if($this->_save_merge_errors === true)
            {
                $this->_merge_errors[] = $e->getMessage();
            }
            else
            {
                throw $e;
            }
        }
    }
    
    private function merge_relations_hasmany_submerge($relation_name, $relation_element_column, $relation_element, $model_name, $percentage_per_relation_element)
    {
        $performed_merge = false;
        if(isset($this->_hasmany_relationmodels_to_merge[$relation_name]))
        {
            $hasmany_relationmodels_to_merge_columns = $this->_hasmany_relationmodels_to_merge[$relation_name];
            $criteria = new SIMADbCriteria();
            $relation_element_columnparam_key = ':relation_element_column'.SIMAHtml::uniqid();
            $criteria->addCondition("$relation_element_column=$relation_element_columnparam_key", 'AND');
            $criteria_params = [
                $relation_element_columnparam_key => $this->model1->id
            ];
            foreach($hasmany_relationmodels_to_merge_columns as $hasmany_relationmodels_to_merge_column)
            {
                $relation_element_column_value = $relation_element->$hasmany_relationmodels_to_merge_column;
//                error_log(__METHOD__.' - $hasmany_relationmodels_to_merge_column: '.$hasmany_relationmodels_to_merge_column);
//                error_log(__METHOD__.' - $relation_element_column_value: '.SIMAMisc::toTypeAndJsonString($relation_element_column_value));
//                error_log(__METHOD__.' - $relation_element: '.SIMAMisc::toTypeAndJsonString($relation_element));

                if(is_null($relation_element_column_value) || $relation_element_column_value === '')
                {
                    $criteria->addCondition("($hasmany_relationmodels_to_merge_column IS NULL OR $hasmany_relationmodels_to_merge_column='')", 'AND');
                    continue;
                }

                $param_key = ':hasmany_relationmodels_to_merge_column'.SIMAHtml::uniqid();
                $param_key_condition_value = $param_key;

                $relation_element_merge_options = $relation_element->modelSettings('merge_options');
                if(
                    isset($relation_element_merge_options['columns_that_are_equal_trimmed'])
                    && in_array($hasmany_relationmodels_to_merge_column, $relation_element_merge_options['columns_that_are_equal_trimmed'])
                ) 
                {
                    $hasmany_relationmodels_to_merge_column = 'trim('.$hasmany_relationmodels_to_merge_column.')';
                    $param_key_condition_value = "trim($param_key)";
                }

                $criteria->addCondition("$hasmany_relationmodels_to_merge_column=$param_key_condition_value", 'AND');
                $criteria_params[$param_key] = $relation_element_column_value;
            }

            $criteria->params = $criteria_params;
            $model1_same_element_to_merge = $model_name::model()->find($criteria);

            if(!empty($model1_same_element_to_merge))
            {
                $merge_with_max = $this->_internal_update_counter + $percentage_per_relation_element;
                $submerge_errors = $model1_same_element_to_merge->modelMergeWith(
                        $relation_element, 
                        $this->_run_in_transaction, 
                        $this->_send_update_percentage_events, 
                        $this->_internal_update_counter, 
                        $merge_with_max,
                        $this->_save_merge_errors
                );
                if(!empty($submerge_errors))
                {
                    $this->_merge_errors[] = $this->parseSubmergeErrors($model1_same_element_to_merge, $relation_element, $submerge_errors);
                }
                $performed_merge = true;
            }
        }
        
        return $performed_merge;
    }
    
    /**
     * 
     * @param type $relation_data
     * @param type $model2_relation_value
     */
    private function merge_relations_hasone($relation_data, $model2_relation_value)
    {
        $relation_element_column = $relation_data[2];        
        $model2_relation_value->$relation_element_column = $this->model1->id;
        $model2_relation_value->save();
    }
    
    /**
     * 
     * @param string $table_string
     * @return array
     */
    private function manymanyrelations_parsetablestring(string $table_string):array
    {
        $table_name = trim(strstr($table_string, '(', true));
        $attributes = trim(str_replace(array('(', ')'), '', strstr($table_string, '(')));
        $attributes_exploded = explode(',', $attributes);
        $base_column = trim($attributes_exploded[0]);
        $rel_column = trim($attributes_exploded[1]);
        
        return [
            'table_name' => $table_name,
            'base_column' => $base_column,
            'rel_column' => $rel_column
        ];
    }
    
    private function sendUpdatePercentageEvent($segment_part)
    {
        if($this->_send_update_percentage_events !== true)
        {
            return;
        }
        $this->_internal_update_counter += $segment_part;
        Yii::app()->sendUpdateEvent($this->_internal_update_counter);
    }
    
    private function parseUpdatePercent($update_percent)
    {
        $update_percent_parsed = (($this->_update_percentage_max-$this->_internal_update_counter)/100)*$update_percent;
        return $update_percent_parsed;
    }
    
    private function parseSubmergeErrors($model1, $model2, $errors)
    {
        $error_message = 'Spajanje '.$model1->modelLabel()
                    .' '.$model1->DisplayName
                    .' sa '.$model2->DisplayName
                    .' nije uspelo zbog: ';
        foreach($errors as $submerge_error)
        {
            $error_message .= '</br>'.$submerge_error;
        }
        return $error_message;
    }
}

