<?php

class SIMANotifManager
{
    public function init()
    {}
    
    
    /**
     * 
     * @param integer $user_id - ID korisnika koji prima notifikaciju
     * @param text $title - naziv notifikacije
     * @param array $params
     * @param text $code
     * @throws Exception
     */
//    public function sendNotif($user_id, $title, $params = array(), $code = null, $delay='-1 sec')
    public function sendNotif($user_id, $title, $params = array(), $code = null, $delay='', $is_attach_info = false)
    {
        $delay_mail_notification = Yii::app()->configManager->get('base.detach_proces_for_every_notification', true);
        if ($delay_mail_notification && empty($delay))
        {
            $delay = '-1 sec';
        }
        $func_params = [
            'user_id' => $user_id,
            'title' => $title,
            'params' => $params,
            'code' => $code,
            'is_attach_info' => $is_attach_info
        ];
        if (empty($delay))
        {
            self::sendNotifInternal($func_params);
        }
        else
        {
            SystemEvent::add(
                    'SIMANotifManager', 
                    'sendNotifInternal', 
                    date("d.m.Y. H:i:s", strtotime($delay)), 
                    $func_params, 
                    "odlozeno slanje notifikacija"
            );
        }
    }
            
            
    public static function sendNotifInternal($input_params)
    {
        $user_id = $input_params['user_id'];
        $title = $input_params['title'];
        $params = $input_params['params'];
        $code = $input_params['code'];
        $is_attach_info = $input_params['is_attach_info'];
        
//        $params['info'] = false;
        $user_ids = [];
        if (gettype($user_id) === 'string' && !is_numeric($user_id))
        {   
            $condition = new SIMADbCriteria([
                'Model' => User::class,
                'model_filter' => [
                    'access' => $user_id
                ]
            ], true);
            if (!empty($condition->ids))
            {
                $user_ids = explode(',', $condition->ids);
            }
        }
        elseif(gettype($user_id) === 'array')
        {
            $user_ids = array_unique($user_id);
        }
        else
        {
            $user_ids[] = $user_id;
        }
        
        if (gettype($params)!='array')           throw new Exception('parametri prilikom slanja notifikacije nije array nego '.gettype($params));
            
        foreach ($user_ids as $_user_id)
        {
            $user = User::model()->findByPk($_user_id);
            if (!empty($user))
            {
                $notif = null;
                if ($code != null && $code != '')
                {
                    $notif = Notification::model()->findByAttributes(array(
                        'code' => $code,
                        'user_id' => $_user_id,
                    ));
                }
                else
                {
                    $code = SIMAHtml::uniqid ();
                }
                if ($notif == null)
                {
                    $notif = new Notification();
                    $notif->user_id = $_user_id;
                    $notif->params = $params;
                    $notif->code = $code;            
                }
                else
                {
                    if($is_attach_info)
                    {
                        $params['info'] = $notif->params['info'] . '<br>' . $params['info'];
                    }
                    $notif->params = $params;
                    $notif->seen = false;
                    $notif->create_time = 'now()';
                }
                $notif->title = $title;
                $notif->save();
                $notif->refresh();

    //            $number_of_unread_notifs = 1;

    //            if(Yii::app()->isWebApplication())
    //            {
                $userModel = User::model()->findByPkWithCheck($_user_id);
                $number_of_unread_notifs = $userModel->unseen_notifications_count;
    //            }

                if(Yii::app()->eventRelay->isInstalled())
                {
    //                WebSocketClient::send(
    //                    $_user_id, 'USER', 
    //                    'NEW_UNREAD_NOTIF', 
    //                    [
    //                        'number_of_unread_notifs' => $number_of_unread_notifs
    //                    ]
    //                );
                    WebSocketClient::sendBulk([
                        [
                            'receiver_id' => $_user_id,
                            'receiver_type' => 'USER',
                            'message_type' => 'NEW_UNREAD_NOTIF',
                            'data' => [
                                'number_of_unread_notifs' => $number_of_unread_notifs
                            ]
                        ]
                    ]);
                }

                $send_mail_for_new_notifications = Yii::app()->configManager->get('messages.send_mail_for_new_notifications', true, $_user_id);
                if($send_mail_for_new_notifications === 'everynotification')
                {
                    $userModel->sendEmailUnreadNotifications();
                }
            }
            else
            {
                Yii::app()->errorReport->createAutoClientBafRequest("Za korisnika sa ID: $_user_id, koji ne postoji u sistemu, je pozvano slanje obaveštenja: $title");
            }
        }
        //Dodaje se i u SSE
//        $params['info'] = isset($params['info']);
//        $session_event = new SseEvents();
//        $session_event->session_id = Yii::app()->session['sima_session'];
//        $session_event->user_id = $user_id;
//        $session_event->event_code = 'new_notif';
//        $session_event->params = array(
//            'id' => $notif->id,
//            'seen' => false,
//            'seen_time' => null,
//            'create_time' => date("d.m.Y. H:i", strtotime($notif->create_time)),
//            'title' => $title,
//            'params' => $params
//        );
//        $session_event->save(); 
    }
}