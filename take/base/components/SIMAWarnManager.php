<?php

/*
 * Primer zadavanja warna
 * 'WARN_FILE_UNCONFIRMED' => [
        'title' => Yii::t('MainMenu', 'UnconfirmedFiles'),
        'onclick' => ['sima.mainTabs.openNewTab','guitable','files_15',[
                [
                    'guiTable'=>[
                        'confirmed'=>0,
                        'responsible_id'=>Yii::app()->user->id
                    ]
                ]
            ]
        ],
        'countFunc' => function(User $user) {
            return $user->unconfirmed_files_count;
        },
        'forModel' => [
            [
                'model' => 'File',
                'title' => 'sdfds',
                'countFunc' => function (User $user, File $model){
                    return $cnt;
                },
                'checkResolveFunc' => function (User $user, File $model){
                    return [ //status i message moraju biti definisani
                        'status' => 'OK', //NOT_OK
                        'message' => 'sdfgdgd'
                    ];
                },
                'resolveFunc' => function (User $user, File $model){
                    //neka logika
                }
            ],
        ]
    ],
 * 
 */

class SIMAWarnManager
{
    public $warns = [];
    public $warns_by_models = [];

    public function init(){}     
    
    /**
     * Dodaje prosledjena upozorenja u lokalni niz
     * @param array $warns
     */
    public function addWarns($warns=[])
    {
        if (!is_array($warns))
        {
            $warns = [$warns];
        }

        $this->warns = array_merge($this->warns, $warns);
    }
    
    public function getWarnsList()
    {
        return array_keys($this->warns);
    }
    
    public function reMapWarnsByModelDisplayAction()
    {
        //rasporedjujemo warnove po display akciji modela
        foreach ($this->warns as $warn_key => $warn_value)
        {
            if (isset($warn_value['forModel']))
            {
                if (isset($warn_value['forModel']['model']))
                {
                    $this->addWarnToModel($warn_key, $warn_value['forModel']);
                }
                else
                {
                    foreach ($warn_value['forModel'] as $warn_model) 
                    {
                        $this->addWarnToModel($warn_key, $warn_model);
                    }
                }
            }
        }
    }
    
    /**
     * Dodaje warn code u niz warnova za model
     * @param string $warn_code
     * @param array $warn_model
     */
    private function addWarnToModel($warn_code, $warn_model)
    {
        if (empty($warn_model['model']) || empty($warn_model['countFunc']))
        {
            throw new SIMAException(Yii::t('BaseModule.Warns', 'warnForModelWrongFormat', [
                '{warn_code}' => $warn_code,
            ]));
        }
        $model_path = $this->getModelDisplayPath($warn_model['model']);
        if (empty($model_path))
        {
            throw new SIMAException("Nije zadata default akcija za pregled modela {$warn_model['model']}.");
        }
        if (empty($this->warns_by_models[$model_path]))
        {
            $this->warns_by_models[$model_path] = [];
        }
        array_push($this->warns_by_models[$model_path], ['warn_code'=>$warn_code, 'model_name'=>$warn_model['model']]);
    }
    
    /**
     * Renderuje html za sva upozorenja
     * @return string
     * @throws SIMAException
     */
    public function renderAllWarns()
    {
        return $this->renderWarns($this->warns);
    }
    
    /**
     * Renderuje html za prosledjene warninge
     * @param array $warns - moze biti $warn_key=>$warn_value, a mogu i samo kodovi warninga
     * @return string
     * @throws SIMAException
     */
    public function renderWarns($warns=[])
    {
        $warns_html = "<ul class='sima-warns'>";
        foreach ($warns as $warn_key => $warn_value)
        {
            if (is_string($warn_value))
            {
                if (!isset($this->warns[$warn_value]))
                {
                    throw new SIMAException("ne Postoji definicija za upozorenje $warn_value");
                }
                $warn_value = $this->warns[$warn_value];
            }
            if (empty($warn_value['title']) || empty($warn_value['onclick']) || empty($warn_value['countFunc']))
            {
                throw new SIMAException("Nepravilno zadato upozorenje '$warn_key'. Polja title, onclick, countFunc su obavezna prilikom zadavanja.");
            }
            
            $warn_onclick = $warn_value['onclick'];
            if(gettype($warn_onclick) === 'object')
            {
                $warn_onclick = $warn_onclick(Yii::app()->user->model);
            }
            
            $func_string = SIMAMisc::generateFunctionStringFromArray($warn_onclick);
            $htmlOptions = [];
            if (!empty($warn_value['htmlOptions']))
            {
                $htmlOptions = $warn_value['htmlOptions'];
            }
            $htmlOptions['class'] = (isset($htmlOptions['class'])?$htmlOptions['class']:'')." sima-warn-item $warn_key";
            $html_options_as_string = SIMAMisc::renderHtmlOptionsFromArray($htmlOptions);
            $warns_html .= "
                        <li $html_options_as_string onclick=\"$func_string\">
                            <span class=\"sima-warn-title\">{$warn_value['title']}</span>:
                            <span class=\"sima-warn-amount\">0</span>
                        </li>";
        }
        $warns_html .= "</ul>";
        
        return $warns_html;
    }
    
    /**
     * Dohvata count za prosledjena upozorenja
     * @param User $user
     * @param array $warn_codes
     * @return array[warn_code]=warn_count;
     */
    public function getWarns(User $user=null, $warn_codes = [])
    {
        if (empty($user))
        {
            $user = Yii::app()->user->model;
        }
        $warn_codes = (is_string($warn_codes))?[$warn_codes]:$warn_codes;
        if (empty($warn_codes))
        {
            $warn_codes = array_keys($this->warns);
        }
        $warns = [];
        foreach ($warn_codes as $warn_code) 
        {
            if (
                    (
                        !isset($this->warns[$warn_code]['visible']) || $this->warns[$warn_code]['visible']
                    ) && 
                    isset($this->warns[$warn_code]['countFunc'])
               )
            {
                $warns[$warn_code] = call_user_func($this->warns[$warn_code]['countFunc'], $user);
            }
        }

        return $warns;
    }
    
    public function getWarnsVue(User $user=null, $warn_codes = [])
    {
        if (empty($user))
        {
            $user = Yii::app()->user->model;
        }
        $warn_codes = (is_string($warn_codes))?[$warn_codes]:$warn_codes;
        if (empty($warn_codes))
        {
            $warn_codes = array_keys($this->warns);
        }
        $warns = [];
        foreach ($warn_codes as $warn_code) 
        {
            if (
                    (
                        !isset($this->warns[$warn_code]['visible']) || $this->warns[$warn_code]['visible']
                    ) && 
                    isset($this->warns[$warn_code]['countFunc'])
               )
            {
                $warn_onclick = $this->warns[$warn_code]['onclick'];
                if(gettype($warn_onclick) === 'object')
                {
                    $warn_onclick = $warn_onclick(Yii::app()->user->model);
                }
                
                $warns[] = [
                    'code' => $warn_code,
                    'title' => $this->warns[$warn_code]['title'],
                    'amount' => call_user_func($this->warns[$warn_code]['countFunc'], $user),
                    'onclick' => $warn_onclick
                ];
            }
        }

        return $warns;
    }
    
    /**
     * 
     * @param type $model
     * @param User $user
     * @return array  - oblika: ['WARN_CODE_1'=>['display_name'=>'dsfsd','has_resolve_func'=>true,...], 'WARN_CODE_2'=>['display_name'=>'gfddf','has_resolve_func'=>false,...],...]
     */
    public function getWarnsForModel($model, User $user=null)
    {
        if (empty($user))
        {
            $user = Yii::app()->user->model;
        }
        $base_model_name = get_class($model);
        $model_path = $this->getModelDisplayPath($base_model_name);
        $warns = [];
        if (!empty($this->warns_by_models[$model_path]))
        {
            foreach ($this->warns_by_models[$model_path] as $warn_by_model)
            {
                $warn_code = $warn_by_model['warn_code'];
                $model_name = $warn_by_model['model_name'];
                $_model = $model;
                if (($model_name !== $base_model_name))
                {
                    $_model = $model_name::model()->findByPk($model->id);
                }
                
                if (!empty($_model))
                {
                    $warn_for_model_definition = $this->getWarnForModelDefinition($warn_code, $model_name);
                    if (!empty($warn_for_model_definition) && key_exists('countFunc', $warn_for_model_definition))
                    {
                        $count_num = call_user_func($warn_for_model_definition['countFunc'], $user, $_model);
                        if ($count_num > 0)
                        {
                            if (isset($warn_for_model_definition['title']))
                            {
                                $warn_display_name = $warn_for_model_definition['title'];
                            }
                            else
                            {
                                $warn_display_name = $this->getWarnDisplayName($warn_code);
                            }
                            $warns[$warn_code] = [
                                'display_name' => $warn_display_name,
                                'has_resolve_func' => (key_exists('checkResolveFunc', $warn_for_model_definition) && key_exists('resolveFunc', $warn_for_model_definition)) ? true : false,
                                'description' => isset($warn_for_model_definition['description']) ? $warn_for_model_definition['description'] : '',
                                'count_num' => $count_num,
                                'show_count_num' => isset($warn_for_model_definition['show_count_num']) ? $warn_for_model_definition['show_count_num'] : false
                            ];
                        }
                    }
                }
            }
        }

        return $warns;
    }
    
    public function getWarnForModelDefinition($warn_code, $model_name)
    {
        if (!empty($this->warns[$warn_code]['forModel']))
        {
            $for_model = $this->warns[$warn_code]['forModel'];
            if (!empty($for_model['model']) && $for_model['model'] === $model_name)
            {
                return $for_model;
            }
            else
            {
                foreach ($for_model as $value) 
                {
                    if ($value['model'] === $model_name)
                    {
                        return $value;
                    }
                }
            }
        }
        
        return null;
    }
    
    public function renderWarnsForModel($model)
    {
        $html = '';

        $warns = Yii::app()->warnManager->getWarnsForModel($model);
        
        $model_name = get_class($model);
        
        foreach ($warns as $warn_code => $warn_for_model_params)
        {
            $not_clickable = '';
            $onclick = "onclick=\"sima.warns.resolveWarnForModel('$model_name', {$model->id}, '$warn_code');\"";
            if (!$warn_for_model_params['has_resolve_func'])
            {
                $not_clickable = '_not-clickable';
                $onclick = '';
            }
            $html .= "
                <div class='sima-model-warn $not_clickable' $onclick 
                    title='{$warn_for_model_params['description']}'
                >
                      <div class='sima-model-warn-title sima-display-inline'>
                            {$warn_for_model_params['display_name']}
                      </div>
            ";
            if (($warn_for_model_params['show_count_num'] === true))
            {
                $html .= "
                    <div class='sima-model-warn-count sima-display-inline'>
                        ({$warn_for_model_params['count_num']})
                    </div>
                ";
            }
            $html .= '</div>';
        }
        
        return $html;
    }
    
    /**
     * Salje upozorenje
     * @param mixed $user_id stavlja se user_id ili naziv privilegije koje zaposleni treba da zadovoljavaju
     * @param array $warn_codes
     */
    public function sendWarns($user_id, $warn_codes=[])
    {
        $user_ids = [];
        $warn_codes = (is_string($warn_codes))?[$warn_codes]:$warn_codes;
        if (gettype($user_id) === 'string' && !is_numeric($user_id))
        {   
            $condition = new SIMADbCriteria([
                'Model' => 'Employee',
                'model_filter' => [
                    'access' => $user_id
                ]
            ], true);
            if (!empty($condition->ids))
            {
                $user_ids = explode(',', $condition->ids);
            }
        }
        elseif(gettype($user_id) === 'array')
        {
            $user_ids = array_unique($user_id);
        }
        else
        {
            $user_ids[] = $user_id;
        }
        
        if(Yii::app()->eventRelay->isInstalled())
        {
            foreach ($user_ids as $idd)
            {
                if (empty($idd))
                {
                    throw new SIMAException('Warn-u poslat prazan $idd');
                }
                $user = User::model()->findByPk($idd);
                if (!empty($user))
                {
                    WebSocketClient::send(
                        $idd, 'USER', 
                        'JS_NEW_WARN', 
                        Yii::app()->warnManager->getWarns($user, $warn_codes)
                    );
                }
                else
                {
                    Yii::app()->errorReport->createAutoClientBafRequest(
                        "Za korisnika sa ID: $idd, koji ne postoji u sistemu, je pozvano slanje sledećih upozorenja: ".CJSON::encode($warn_codes),
                        3065,
                        'SIMA_razvoj_AutoBafRequest - upozorenja koja se salju za korisnike koji ne postoje'
                    );
                }
            }
        }
    }
    
    /**
     * Vraca display name za warning
     * @param string $warn_code
     * @return string
     */
    public function getWarnDisplayName($warn_code)
    {
        $display_name = '';
        if (!empty($this->warns[$warn_code]))
        {
            $display_name = $this->warns[$warn_code]['title'];
        }
        
        return $display_name;
    }
    
    private function getModelDisplayPath($model_name)
    {
        $model = $model_name::model();
        $warn_default_model = $model->modelSettings('warn_default_model');

        return (!empty($warn_default_model) ? $warn_default_model : $model_name) . '_' . $model->getDisplayAction();
    }
    
    public function renderWarnsByUsers($users_fixed_filter = [])
    {
        $uniq = SIMAHtml::uniqid();
        $title = Yii::t('BaseModule.Warns', 'WarnsByUsersOverview');

        return Yii::app()->controller->renderContentLayout([
            'name'=>$title
        ], [
            [
                'html' => Yii::app()->controller->widget('SIMAGuiTable', [
                    'id' => "users_$uniq",
                    'model' => User::model(),
                    'add_button' => false,
                    'columns_type' => 'from_warns',
                    'fixed_filter' => $users_fixed_filter,
                    'setRowSelect' => ['sima.warns.renderWarnsOnUserSelect', "$uniq"],
                    'setRowUnSelect' => ['sima.warns.renderWarnsOnUserUnSelect', "$uniq"],
                    'setMultiSelect' => ['sima.warns.renderWarnsOnUserUnSelect', "$uniq"]
                ], true),
                'class' => 'warns_by_users_left_panel'
            ],
            [
                'html' => '',
                'class' => 'warns_by_users_right_panel'
            ]
        ], [
            'id'=>$uniq,
            'class'=>'warns_by_users'
        ]);
    }
}