<?php

class ExceptionThrower
{
    static $IGNORE_DEPRECATED = true;    
    
    /**
     * Start redirecting PHP errors
     * @param int $level PHP Error level to catch (Default = E_ALL & ~E_DEPRECATED)
     * @param string $exception_class Exception class name. (e.g Exception, SIMAException...)
     */
    static function Start($level = null, $exception_class='Exception')
    {
        if ($level == null)
        {
            if (defined("E_DEPRECATED"))
            {
                $level = E_ALL & ~E_DEPRECATED ;
            }
            else
            {
                // php 5.2 and earlier don't support E_DEPRECATED
                $level = E_ALL;
                self::$IGNORE_DEPRECATED = true;
            }
        }
        set_error_handler(function($code, $string, $file, $line, $context) use ($exception_class) {
            // ignore supressed errors
            if (error_reporting() == 0) return;
            if (self::$IGNORE_DEPRECATED && strpos($string,"deprecated") === true) return true;

            throw new $exception_class($string, $code);
        }, $level);
    }

    /**
     * Stop redirecting PHP errors
     */
    static function Stop()
    {
        restore_error_handler();
    }
}