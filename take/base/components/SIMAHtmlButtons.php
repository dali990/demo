<?php

class SIMAHtmlButtons
{
    public static function fileOpen($model)
    {
        $return = '';
        $button_action = SIMAHtmlButtons::fileVersionOpenInDesktopAppWidgetAction($model);
        if (!empty($button_action))
        {
            $class = '';
            if(!empty($button_action['parent_class']))
            {
                $class = $button_action['parent_class'];
            }
            if(SIMAMisc::isVueComponentEnabled())
            {
                if(!empty($class))
                {
                    $button_action['additional_classes'] = $class;
                }
                $return = Yii::app()->controller->widget(SIMAButtonVue::class, $button_action, true);
            }
            else
            {
                $return = Yii::app()->controller->widget('SIMAButton', [
                    'class' => $class,
                    'action' => $button_action
                ], true);
            }
        }
        
        return $return;
    }
    
    public static function ConfirmDesktopAppFileModifications($event_relay_id, $original_id, $html_options=[])
    {
        $result = '';
        $action = SIMAHtmlButtons::ConfirmDesktopAppFileModificationsWidgetAction($event_relay_id, $original_id);
        if(SIMAMisc::isVueComponentEnabled())
        {
            $action['half'] = true;
            $result = Yii::app()->controller->widget(SIMAButtonVue::class, $action, true);
        }
        else
        {
            $result = Yii::app()->controller->widget('SIMAButton', [
                'class' => '_half',
                'action' => $action
            ], true);
        }
        
        return $result;
    }
    
    public static function RevertDesktopAppFileModifications($event_relay_id, $original_id, $html_options=[])
    {
        $result = '';
        $action = SIMAHtmlButtons::RevertDesktopAppFileModificationsWidgetAction($event_relay_id, $original_id);
        if(SIMAMisc::isVueComponentEnabled())
        {
            $action['half'] = true;
            $result = Yii::app()->controller->widget(SIMAButtonVue::class, $action, true);
        }
        else
        {
            $result = Yii::app()->controller->widget('SIMAButton', [
                'class' => '_half',
                'action' => SIMAHtmlButtons::RevertDesktopAppFileModificationsWidgetAction($event_relay_id, $original_id)
            ], true);
        }
        
        return $result;
    }
    
    public static function OpenModifiedFileInDesktopApp($model, $event_relay_id, $html_options=[])
    {
        $result = '';
        
        $was_some_error = false;
        if (get_class($model)==='FileVersion' && $model->canDownload())
        {
            $MODEL_NAME = 'FileVersion';
            $MODEL_ID = $model->id;
        }
        elseif (get_class($model)==='File' && $model->last_version->canDownload())
        {
            $MODEL_NAME = 'File';
            $MODEL_ID = $model->id;
        }
        elseif (isset($model->file) && get_class($model->file)==='File' && $model->file->last_version->canDownload())
        {
            $MODEL_NAME = 'File';
            $MODEL_ID = $model->file->id;
        }
        else
        {
            $was_some_error = true;
        }
        
        if($was_some_error == false)
        {
            if(SIMAMisc::isVueComponentEnabled())
            {
                $result = Yii::app()->controller->widget(SIMAButtonVue::class, [
                    'half' => true,
                    'icon' => '_open-modified-file-in-da',
                    'onclick' => [
                        'sima.desktopapp.openModifiedFileInDesktopApp', 
                        $event_relay_id, $MODEL_NAME, $MODEL_ID
                    ],
                    'tooltip' => Yii::t('BaseModule.SIMAHtmlButtons', 'OpenInClient')
                ], true);
            }
            else
            {
                $result = Yii::app()->controller->widget('SIMAButton', [
                    'class' => '_half',
                    'action' => [
                        'icon' => '_open-modified-file-in-da',
                        'onclick' => [
                            'sima.desktopapp.openModifiedFileInDesktopApp', 
                            $event_relay_id, $MODEL_NAME, $MODEL_ID
                        ],
                        'tooltip' => Yii::t('BaseModule.SIMAHtmlButtons', 'OpenInClient')
                    ]
                ], true);
            }
        }
        
        return $result;
    }
    
    /**
     * modelIconsContracted actions
     */
    
    public static function modelFormOpenWidgetAction($model,$params=array(),$px_size=16)
    {        
        if (!SIMAHtml::checkAccess($model, 'FormOpen'))
        {
            return [];
        }
        
        $Model = get_class($model);
        if (isset($model->id))
        {
            $model_id = $model->id;
            $button_img = 'edit';
        }
        else
        {
            $add_obj = [];
            if (isset($params['append_object'])) 
            {
                //u ovom slucaju dodajemo u odredjeni jQuery objekat i to odredjeni pogled
                if (gettype($params['append_object'])==='array' && count($params['append_object'])>0)      
                {    
                    $add_obj['append_object'] = array_shift($params['append_object']);
                    $add_obj += $params['append_object'];
                }
                //u ovom slucaju samo dodajemo u odredjeni jQuery objekat
                else if (gettype($params['append_object'])==='string') 
                {
                    $add_obj['append_object'] = $params['append_object'];
                }
            }
//
//            $model_id= CJSON::encode($add_obj);
            $model_id= $add_obj;
            $button_img = 'add';
        }
        if (isset($params['model_id']))
        {
            $model_id = $params['model_id'];
        }
        if (isset($params['button_img']))
        {
            $button_img = $params['button_img'];
        }

        $buttonTitle = (isset($params['button_title']))?$params['button_title']:Yii::t('BaseModule.SIMAHtmlButtons', 'OpenForm');
        $formName = (isset($params['formName']))?$params['formName']:'default';
        $scenario = (isset($params['scenario']))?$params['scenario']:'';

        $init_data = isset($params['init_data'])?$params['init_data']: [];
        $status = isset($params['status'])?$params['status']:'initial';

        $updateModelViews = '';
        if (isset($params['updateModelViews']) && gettype($params['updateModelViews'])=='string')
        {
            $updateModelViews = $params['updateModelViews'];
        }

        $onSave = 'function(){}';
        if (isset($params['onSave']) && gettype($params['onSave'])=='string')
        {
            $onSave = $params['onSave'];
        }
        
        $title = '';
        if(isset($params['title']))
        {
            $title = $params['title'];
        }

        $result = [
            'onclick' => [
                'sima.icons.form', '$(this)', $Model, $model_id, [
                    'init_data' => $init_data,
                    'updateModelViews' => $updateModelViews,
                    'formName' => $formName,
                    'scenario' => $scenario,
                    'onSave' => $onSave,
                    'status' => $status
                ]
            ],
            'title' => $title,
            'icon' => 'sima-icon _'.$button_img.' _'.$px_size,
            'tooltip' => $buttonTitle
        ];
        
        if(SIMAMisc::isVueComponentEnabled())
        {
            $result['icon'] = $button_img;
            $result['iconsize'] = $px_size;
        }
        
        return $result;
    }
    
    //Sasa A. - $is_called_from_js sluzi samo da bi drugacije napisao prop za vue posto se nazivi razlikuju za zadavanje dugmeta iz php i iz js-a
    public static function fileVersionOpenInDesktopAppWidgetAction($model, $is_called_from_js = false)
    {
        if(empty($model) || !is_object($model))
        {
            return [];
        }
        
        $browser_download_file_id = 'NULL';
        $browser_download_file_version_id = 'NULL';
        $sda_open_fileversion_id = 'NULL';
        $file_version_model = null;
        if (get_class($model)==='FileVersion' && $model->canDownload())
        {
            $MODEL_NAME = 'FileVersion';
            $MODEL_ID = $model->id;
            $browser_download_file_version_id = $model->id;
            $sda_open_fileversion_id = $model->id;
            $file_version_model = $model;
        }
        elseif (get_class($model)==='File' && $model->last_version->canDownload())
        {
            $MODEL_NAME = 'File';
            $MODEL_ID = $model->id;
            $browser_download_file_id = $model->id;
            $sda_open_fileversion_id = $model->last_version->id;
            $file_version_model = $model->last_version;
        }
        elseif (isset($model->file) && get_class($model->file)==='File' && $model->file->last_version->canDownload())
        {
            $MODEL_NAME = 'File';
            $MODEL_ID = $model->file->id;
            $browser_download_file_id = $model->file->id;
            $sda_open_fileversion_id = $model->file->last_version->id;
            $file_version_model = $model->file->last_version;
        }
        else
        {
            return [];
        }

        $title = Yii::t('BaseModule.SIMAHtmlButtons', 'OpenInDesktopApp');

        $downloadAsSubactions = SIMAHtmlButtons::GetFilePossibleConversionsWidgetActions($file_version_model, 'sima.files.downloadAsConverted');
        $fileExtension = $file_version_model->file_extension;
        if((empty($fileExtension)) || (!empty($fileExtension) && !$fileExtension->isCompressed()))
        {
            if(empty($browser_download_file_id))
            {
                $autobafreq_theme_id = 3439;
                $autobafreq_theme_name = 'AutoBafReq sima.file.downloadAsZip file_ids empty';
                $autobafreq_message = '$browser_download_file_id is empty'
                                    .'</br>- $browser_download_file_id: '.SIMAMisc::toTypeAndJsonString($browser_download_file_id)
                                    .'</br>- $model: '.SIMAMisc::toTypeAndJsonString($model)
                                    .'</br>- $is_called_from_js: '.SIMAMisc::toTypeAndJsonString($is_called_from_js);
                Yii::app()->errorReport->createAutoClientBafRequest($autobafreq_message, $autobafreq_theme_id, $autobafreq_theme_name);
            }
            
            $download_as_zip = [
                'onclick' => [
                    "sima.files.downloadAsZip", 
                    [$browser_download_file_id], $model->DisplayName.'.zip'
                ],
                'title' => Yii::t('BaseModule.SIMAHtmlButtons', 'DownloadZip'),
            ];
            if(SIMAMisc::isVueComponentEnabled())
            {
                $download_as_zip['additional_classes'] = ['sima-da-open-zip-option'];
            }
            else
            {
                $download_as_zip['class'] = 'sima-da-open-zip-option';
            }

            $downloadAsSubactions[] = $download_as_zip;
        }

        $previewAction = ['sima.dialog.openActionInDialog', 'filePreview/previewDialogContent', [
            'get_params' => [
                'model_name' => $MODEL_NAME,
                'model_id' => $MODEL_ID
            ]
        ]];

        $uniq = SIMAHtml::uniqid();

        $subactions = [];
        $subactions[] = [
            'onclick' => $previewAction,
            'title' => Yii::t('BaseModule.SIMAHtmlButtons', 'PreviewFile'),
        ];

        $download_open = [
            'onclick' => ['sima.icons.fileDownload', $browser_download_file_id, $browser_download_file_version_id],
            'title' => Yii::t('BaseModule.SIMAHtmlButtons', 'Download'),
        ];

        $subactions[] = $download_open;

        if(!empty($downloadAsSubactions))
        {
            $download_as = [
                'title' => Yii::t('BaseModule.SIMAHtmlButtons', 'DownloadAs'),
                'subactions_position'=>'left',
            ];
            if(SIMAMisc::isVueComponentEnabled())
            {
                $subactions_data = $is_called_from_js ? 'subactiondata' : 'subactions_data';
                $download_as['additional_classes'] = ['sima-da-download-as-option'];
                $download_as[$subactions_data] = $downloadAsSubactions;
            }
            else
            {
                $download_as['class'] = 'sima-da-download-as-option';
                $download_as['subactions'] = $downloadAsSubactions;
            }

            $subactions[] = $download_as;
        }

        $open_in_sda_readonly = [
            'onclick' => [
                'sima.desktopapp.openInDesktopAppRequest',
                $MODEL_NAME, $MODEL_ID, $sda_open_fileversion_id, true, true
            ],
            'title' => Yii::t('BaseModule.SIMAHtmlButtons', 'OpenReadOnly'),
        ];
        $subactions[] = $open_in_sda_readonly;

        $open_in_sda_edit = [
            'onclick' => [
                'sima.desktopapp.openInDesktopAppRequest',
                $MODEL_NAME, $MODEL_ID, $sda_open_fileversion_id, true, false
            ],
            'title' => Yii::t('BaseModule.SIMAHtmlButtons', 'OpenReadWrite'),
        ];
        $subactions[] = $open_in_sda_edit;

        $subactions[] = [
            'title' =>  Yii::t('FilesModule.File', 'SaveAsNew'),
            'onclick' => ['sima.misc.addFileFromFileVersion', $sda_open_fileversion_id]
        ];

        $parent_class = '_sda-fvid-'.$sda_open_fileversion_id;
        if($MODEL_NAME === 'File')
        {
            $parent_class .= ' _sda-fid-'.$MODEL_ID;
        }

        $result = [
            'id'=>$uniq,
            'tooltip' => $title,
            'onclick' => $previewAction
        ];
        if(SIMAMisc::isVueComponentEnabled())
        {
            $result['icon'] = '_open-in-desktop-app';
            $result['half'] = true;
            $result['iconsize'] = 16;
            $result['additional_classes'] = ['sima-da-open', $parent_class];
            $subactions_data = $is_called_from_js ? 'subactiondata' : 'subactions_data';
            $result[$subactions_data] = $subactions;
        }
        else
        {
            $result['icon'] = '_open-in-desktop-app _half _16';
            $result['parent_class'] = $parent_class;
            $result['class'] = 'sima-da-open '.$parent_class;
            $result['subactions'] = $subactions;
        }
        
        return $result;
    }
    
    public static function fileLockWidgetAction($model)
    {
//        $cache_key = __METHOD__.'_'.$model->tableName().'_'.$model->id.'_'.Yii::app()->user->id;
//        $cache_result = Yii::app()->simacache->get($cache_key);
//        
//        if($cache_result === false)
//        {
//            $keywords = array(
//                array($model, 'locked')
//            );
            
            $buttonTitle = '';
            $icon_classes = '';
            $locked = null;
            $vue_disabled = false;
            if($model->isLocked)
            {
                $locked = true;
                $buttonTitle = Yii::t('BaseModule.SIMAHtmlButtons', 'FileLocked', [
                    '{user}' => $model->locked->user->DisplayName
                ]);
                $icon_classes = 'sima-icon _file-locked _16';

                $disabled = ' _disabled';
                $vue_disabled = true;
                if ($model->locked->user->id == Yii::app()->user->id 
                            || $model->responsible_id == Yii::app()->user->id 
                            || Yii::app()->user->checkAccess('unlockFileMasterAdmin'))
                {
                    $disabled = '';
                    $vue_disabled = false;
                }
                $icon_classes .= $disabled;
            }
            else
            {
                $locked = false;
                $buttonTitle = Yii::t('BaseModule.SIMAHtmlButtons', 'LockFile');
                $icon_classes = 'sima-icon _file-unlocked _16';
            }
        
            $cache_result = [
                'onclick' => [
                    'sima.icons.lockFile', '$(this)', $locked, $model->id
                ],
                'icon' => $icon_classes,
                'htmlOptions' => [
                    'data-file_id' => $model->id
                ],
                'tooltip' => $buttonTitle
            ];
            
            if(SIMAMisc::isVueComponentEnabled())
            {
                $cache_result['icon'] = $locked ? '_file-locked' : '_file-unlocked';
                $cache_result['disabled'] = $vue_disabled;
                $cache_result['iconsize'] = 16;
                $cache_result['id'] = SIMAHtml::uniqid();
                $cache_result['html_options'] = $cache_result['htmlOptions'];
                unset($cache_result['htmlOptions']);
            }
            
//            Yii::app()->simacache->set($cache_key, $cache_result, $keywords, true);
//            
//        }
        
        return $cache_result;
    }
    
    public static function modelDialogWidgetAction($model, $pix_size=16)
    {
        if(empty($model) || !is_object($model))
        {
            throw new Exception(Yii::t('BaseModule.SIMAHtmlButtons', 'InvalidModelPassed', [
                '{model}' => CJSON::encode($model)
            ]));
        }
        
        if(empty($pix_size))
        {
            $pix_size = 16;
        }
        
        $subaction = [];
        if ($model->hasDisplay())
        {
            $onclick = ['sima.dialog.openActionInDialogAsync', 'defaultLayout', ['get_params'=>['model_name'=>get_class($model), 'model_id'=>$model->id]]];
            $subaction = [
                'onclick' => [
                    'sima.mainTabs.openNewTab',
                    'defaultLayout', 
                    [
                        'model_name' => get_class($model), 'model_id'=>$model->id
                    ]

                ],
                'title' => Yii::t('BaseModule.SIMAHtmlButtons', 'OpenInMainTab')
            ];
        }
        else
        {
            $onclick = ['sima.icons.openModel', $model->path2, $model->id];
            $subaction = [
                'onclick' => [
                    'sima.mainTabs.openNewTab',
                    $model->path2, 
                    $model->id

                ],
                'title' => Yii::t('BaseModule.SIMAHtmlButtons', 'OpenInMainTab')
            ];
        }
        
        $result = [
            'onclick' => $onclick,
            'icon' => 'sima-icon _dialog _'.$pix_size,
            'tooltip' => Yii::t('BaseModule.SIMAHtmlButtons', 'OpenInDialog'),
        ];
        if(SIMAMisc::isVueComponentEnabled())
        {
            $result['icon'] = '_dialog';
            $result['iconsize'] = $pix_size;
            $result['subactions_data'] = [$subaction];
        }
        else
        {
            $result['subactions'] = [$subaction];
        }
        
        return $result;
    }
    
    public static function modelRecycleWidgetAction($model, $pix_size=16)
    {
        $result = [
            'onclick' => [
                'sima.icons.recycle', '$(this)', get_class($model), $model->id, 
            ],
            'icon' => "sima-icon _recycle _$pix_size",
            'tooltip' => Yii::t('BaseModule.SIMAHtmlButtons', 'Recycle')
        ];
        if(SIMAMisc::isVueComponentEnabled())
        {
            $result['icon'] = '_recycle';
            $result['iconsize'] = $pix_size;
        }
        
        return $result;
    }
    
    public static function orderScanWidgetAction($model, $pix_size=16)
    {
        $onclick = [
            'sima.icons.orderScan',
            '$(this)',
            'Da li zaista zelite da narucite/povucete selektovana skeniranja?',
            $model
        ];
        
        $icon = '_order-scan-add';
        
        $tooltip = Yii::t('BaseModule.SIMAHtmlButtons', 'OrderScan');
                
        if (is_object($model))
        {
            if ($model->scan_order)
            {
                $onclick = [
                    'sima.icons.orderScan',
                    '$(this)',
                    'Da li zaista zelite da povucete naruceno skeniranje?',
                    $model->id,
                    false
                ];
                
                $icon = '_order-scan-accepted';
                
                $tooltip = Yii::t('BaseModule.Common', 'RevokeScan');
            }
            else
            {
                $onclick = [
                    'sima.icons.orderScan',
                    '$(this)',
                    'Da li zaista zelite da narucite skeniranje?',
                    $model->id,
                    true
                ];
            }
        }
        
        
        $result = [
            'onclick' => $onclick,
            'icon' => "sima-icon $icon _$pix_size",
            'tooltip' => $tooltip
        ];
        if(SIMAMisc::isVueComponentEnabled())
        {
            $result['icon'] = $icon;
            $result['iconsize'] = $pix_size;
        }
        
        return $result;
    }
    
    public static function multiselectAddFileTagsWidgetAction($guitable_id, $pix_size=16)
    {
        $icon = '_add-tags';

        $result = [
            'onclick' => [
                'sima.files.addTagsToFiles',
                $guitable_id
            ],
            'icon' => "sima-icon $icon _$pix_size",
            'tooltip' => Yii::t('FilesModule.FileTag', 'AddTags')
        ];
        if(SIMAMisc::isVueComponentEnabled())
        {
            $result['icon'] = $icon;
            $result['iconsize'] = $pix_size;
        }
        
        return $result;
    }
    
    public static function modelRefreshWidgetAction($model, $pix_size=16)
    {
        $result = [
            'onclick' => [
                'sima.icons.refresh', 
                '$(this)', 
                SIMAHtml::getTag($model)
            ],
            'icon' => "sima-icon _refresh _$pix_size",
            'tooltip' => Yii::t('BaseModule.SIMAHtmlButtons', 'Refresh')
        ];
        if(SIMAMisc::isVueComponentEnabled())
        {
            $result['icon'] = '_refresh';
            $result['iconsize'] = $pix_size;
        }
        
        return $result;
    }
    
    public static function modelDeleteWidgetAction($model,$yes_function='',$pix_size=16)
    {        
        if((isset($model->confirmed) && $model->confirmed))
        {
            return [];
        }
        
        $pk = $model->getPrimaryKey();
//        if (gettype($pk)==='array')
//        {
//            $new_pk = '{';
//            foreach ($pk as $key => $value) {
//               $new_pk .= "$key:$value,";
//            }
//            $new_pk[strlen($new_pk)-1]='}';
//            $pk = $new_pk;
//        }
        
        $result = [
            'onclick' => [
                'sima.icons.remove', 
                '$(this)', 
                get_class($model),
                $pk,
                $yes_function
            ],
            'icon' => "sima-icon _delete _$pix_size",
            'tooltip' => Yii::t('BaseModule.SIMAHtmlButtons', 'Delete')
        ];
        if(SIMAMisc::isVueComponentEnabled())
        {
            $result['icon'] = '_delete';
            $result['iconsize'] = $pix_size;
        }
        
        return $result;
    }
    
    public static function modelDeleteForeverWidgetAction($model,$yes_function='',$pix_size=16)
    {
        $pk = $model->getPrimaryKey();
        if (gettype($pk)==='array')
        {
            $new_pk = '{';
            foreach ($pk as $key => $value) {
               $new_pk .= "$key:$value,";
            }
            $new_pk[strlen($new_pk)-1]='}';
            $pk = $new_pk;
        }
        
        $result = [
            'onclick' => [
                'sima.icons.deleteforever', 
                '$(this)', 
                get_class($model),
                $pk,
                $yes_function
            ],
            'icon' => "sima-icon _delete-forever _$pix_size",
            'tooltip' => Yii::t('BaseModule.SIMAHtmlButtons', 'DeleteForever')
        ];
        if(SIMAMisc::isVueComponentEnabled())
        {
            $result['icon'] = '_delete-forever';
            $result['iconsize'] = $pix_size;
        }
        
        return $result;
    }
    
    public static function cancelLinkWidgetAction($model, $pix_size=16)
    {
        if(empty($pix_size))
        {
            $pix_size = 16;
        }
        
        $result = [
            'onclick' => [
                'sima.icons.cancelLink', 
                '$(this)', 
                'Da li zaista zelite da stornirate racun?',
                get_class($model),
                $model->id
            ],
            'icon' => "sima-icon _cancel _$pix_size",
            'tooltip' => Yii::t('BaseModule.SIMAHtmlButtons', 'Cancel')
        ];
        if(SIMAMisc::isVueComponentEnabled())
        {
            $result['icon'] = '_cancel';
            $result['iconsize'] = $pix_size;
        }
        
        return $result;
    }
    
    public static function modelRestoreFromTrashWidgetAction($model,$yes_function='',$pix_size=16)
    {
        $pk = $model->getPrimaryKey();
        if (gettype($pk)==='array')
        {
            $new_pk = '{';
            foreach ($pk as $key => $value) {
               $new_pk .= "$key:$value,";
            }
            $new_pk[strlen($new_pk)-1]='}';
            $pk = $new_pk;
        }
        
        $result = [
            'onclick' => [
                'sima.icons.restore_from_trash', 
                '$(this)', 
                get_class($model),
                $pk,
                $yes_function
            ],
            'icon' => "sima-icon _restore-from-trash _$pix_size",
            'tooltip' => Yii::t('BaseModule.SIMAHtmlButtons', 'RestoreFromTrash')
        ];
        if(SIMAMisc::isVueComponentEnabled())
        {
            $result['icon'] = '_restore-from-trash';
            $result['iconsize'] = $pix_size;
        }
        
        return $result;
    }
    
    public static function copyModelWidgetAction($model, $px_size=16)
    {
        $result = [
            'onclick' => [
                'sima.icons.copyModel', 
                get_class($model),
                $model->id
            ],
            'icon' => "sima-icon _copy _$px_size",
            'tooltip' => Yii::t('BaseModule.SIMAHtmlButtons', 'CopyModel')
        ];
        if(SIMAMisc::isVueComponentEnabled())
        {
            $result['icon'] = '_copy';
            $result['iconsize'] = $px_size;
        }
        
        return $result;
    }
    
    public static function DownloadQRCodeWidgetAction($model, $is_called_from_js = false)
    {
        $subactions = [
            [
                'onclick' => [
                    'sima.icons.downloadQRCode', 
                    get_class($model),
                    $model->id,
                    1
                ],
                'title' => Yii::t('BaseModule.QRCode', 'Size1')
            ],
            [
                'onclick' => [
                    'sima.icons.downloadQRCode', 
                    get_class($model),
                    $model->id,
                    2
                ],
                'title' => Yii::t('BaseModule.QRCode', 'Size2'),
            ]
        ];
        
        $result = [
            'onclick' => [
                'sima.icons.downloadQRCode', 
                get_class($model),
                $model->id,
                3
            ],
            'icon' => "_qrcode",
            'tooltip' => Yii::t('BaseModule.QRCode', 'QRCode')
        ];
        if(SIMAMisc::isVueComponentEnabled())
        {
            $subactions_data = $is_called_from_js ? 'subactiondata' : 'subactions_data';
            $result[$subactions_data] = $subactions;
        }
        else
        {
            $result['subactions'] = $subactions;
        }
        
        return $result;
    }
    
    public static function ConfigurationParameterEditWidgetAction($model, $is_user_personal=true)
    {
        $icon_class = '';
        if($model->value_is_array === true)
        {
            $icon_class = '_add';
        }
        else
        {
            $icon_class = '_edit';
        }
        
        $model_data = '';
        if($model->type === 'searchField')
        {
            $model_data = $model->type_params['modelName'];
        }
        
        $result = [
            'onclick' => [
                'sima.config.changeConfigParamDialog', 
                $model->path,
                $is_user_personal,
                $model->type
            ],
            'icon' => 'sima-icon _16 '.$icon_class,
            'htmlOptions' => [
                'data-type' => $model->type,
                'data-model' => $model_data
            ]
        ];
        
        if(SIMAMisc::isVueComponentEnabled())
        {
            $result['icon'] = $icon_class;
            $result['iconsize'] = 16;
            $result['html_options'] = $result['htmlOptions'];
            unset($result['htmlOptions']);
        }
        
        return $result;
    }
    
    public static function ConfigurationParameterRevertToDefaultWidgetAction($model, $is_user_personal)
    {
        $result = [];
        
        $default = $model->default;
        $value = $model->value;
        
        if($default != $value
                        && (($model->value_not_null === false) || ($model->value_not_null === true && !empty($default))))
        {
            $result = [
                'onclick' => [
                    'sima.config.setConfigParamToDefault', 
                    $model->path,
                    $is_user_personal,
                    '$(this)'
                ],
                'icon' => 'sima-icon _reset_default _16',
                'tooltip' => Yii::t('BaseModule.Configuration', 'ReturnToDefault')
            ];
            if(SIMAMisc::isVueComponentEnabled())
            {
                $result['icon'] = '_reset_default';
                $result['iconsize'] = 16;
            }
        }
        
        return $result;
    }
    
    public static function ConfigurationParameterUserChangableWidgetAction($model)
    {
        $checked = '';
        if($model->user_changable === true)
        {
            $checked = ' checked ';
        }
        
        $result = [
            'html' => "<input type='checkbox' title='Da li je korisnicki' $checked onclick='"
                . "sima.config.paramUserChangableChanged("
                    . "\"".$model->path."\", "
                    . "$(this)"
                . ");'/>"
        ];
        
        return $result;
    }
    
    public static function OpenNewTabNotificationWidgetAction($model)
    {
        $result = [];
        
        $params = $model->params;
        
        if(!empty($params['action']))
        {
            $action = $params['action']['action'];
            $action_id = isset($params['action']['action_id'])?$params['action']['action_id']:'null';
            $selector = isset($params['action']['selector'])?CJSON::encode($params['action']['selector']):'{}';
            
            $result = [
                'onclick' => [
                    'sima.dialog.openModel',
                    $action, 
                    $action_id,
                    $selector
                ],
                'icon' => 'sima-icon _dialog _16',
                'tooltip' => Yii::t('BaseModule.SIMAHtmlButtons', 'OpenInDialog')
            ];
            if(SIMAMisc::isVueComponentEnabled())
            {
                $result['icon'] = '_dialog';
                $result['iconsize'] = 16;
            }
        }
        
        return $result;
    }
    
    public static function AccountOpenWithDateWidgetAction($model)
    {
        $result = [
            'onclick' => [
                'sima.dialog.openModel',
                'accounting/books', 
                $model->id,
                [
                    'start_date' => SIMAHtml::DbToUserDate($model->start_date),
                    'end_date' => SIMAHtml::DbToUserDate($model->end_date)
                ]
            ],
            'icon' => 'sima-icon _dialog _16',
            'tooltip' => Yii::t('BaseModule.SIMAHtmlButtons', 'OpenInDialog')
        ];
        if(SIMAMisc::isVueComponentEnabled())
        {
            $result['icon'] = '_dialog';
            $result['iconsize'] = 16;
        }
        
        return $result;
    }
    
    public static function CacheResetWidgetAction($model)
    {
        $result = [
            'onclick' => [
                'sima.adminMisc.resetCachebyId',
                [
                    'id' => $model->id
                ]
            ],
            'icon' => 'sima-icon _refresh _16',
            'tooltip' => Yii::t('BaseModule.SIMAHtmlButtons', 'Reset')
        ];
        if(SIMAMisc::isVueComponentEnabled())
        {
            $result['icon'] = '_refresh';
            $result['iconsize'] = 16;
        }
        
        return $result;
    }
    
    public static function CacheDevalidateWidgetAction($model)
    {
        $result = [
            'onclick' => [
                'sima.adminMisc.devalidateCacheById',
                [
                    'id' => $model->id
                ]
            ],
            'icon' => 'sima-icon _not_ok _16',
            'tooltip' => Yii::t('BaseModule.SIMAHtmlButtons', 'Devalidate')
        ];
        if(SIMAMisc::isVueComponentEnabled())
        {
            $result['icon'] = '_not_ok';
            $result['iconsize'] = 16;
        }
        
        return $result;
    }
    
    public static function multiselectLockWidgetAction($table_id, $px_size)
    {
        $result = [
            'onclick' => [
                'sima.icons.multiselectLock', 
                '$(this)',
                Yii::t('BaseModule.SIMAHtmlButtons', 'MultiselectLockQuestion'),
                $table_id
            ],
            'icon' => "sima-icon _file-locked _$px_size",
            'tooltip' => Yii::t('BaseModule.SIMAHtmlButtons', 'MultiselectLockTooltip')
        ];
        if(SIMAMisc::isVueComponentEnabled())
        {
            $result['icon'] = '_file-locked';
            $result['iconsize'] = 16;
        }
        
        return $result;
    }
    
    public static function multiselectRecycleWidgetAction($table_id, $px_size)
    {
        $result = [
            'onclick' => [
                'sima.icons.multiselectRecycle', 
                '$(this)',
                Yii::t('BaseModule.SIMAHtmlButtons', 'MultiselectRecycleQuestion'),
                $table_id
            ],
            'icon' => "sima-icon _recycle _$px_size",
            'tooltip' => Yii::t('BaseModule.SIMAHtmlButtons', 'MultiselectRecycleTooltip')
        ];
        if(SIMAMisc::isVueComponentEnabled())
        {
            $result['icon'] = '_recycle';
            $result['iconsize'] = $px_size;
        }
        
        return $result;
    }
    
//    public static function multiselectReviewWidgetAction($table_id, $px_size)
//    {
//        $result = [
//            'onclick' => [
//                'sima.icons.multiselectReview', 
//                '$(this)',
//                Yii::t('BaseModule.SIMAHtmlButtons', 'MultiselectReviewQuestion'),
//                $table_id
//            ],
//            'icon' => "sima-icon _review _$px_size",
//            'tooltip' => Yii::t('BaseModule.SIMAHtmlButtons', 'MultiselectReviewTooltip')
//        ];
//        if(SIMAMisc::isVueComponentEnabled())
//        {
//            $result['icon'] = '_review';
//            $result['iconsize'] = $px_size;
//        }
//        
//        return $result;
//    }
    
    public static function filesDownloadZipWidgetAction($table_id, $px_size, $model, $params=[])
    {
        $zip_folder_name = date('Y_m_d_H_i_s').'.zip';
                
        $onclick = [
            'sima.files.multiselectDownloadAsZip', 
            $table_id,
            $zip_folder_name
        ];
        
        if(isset($params['relation']) && is_string($params['relation']))
        {
            $onclick[] = [
                'model' => get_class($model),
                'relation' => $params['relation']
            ];
        }
        
        $result = [
            'onclick' => $onclick,
            'icon' => "_zip",
            'tooltip' => Yii::t('BaseModule.SIMAHtmlButtons', 'MultiselectFilesDownloadZipTooltip')
        ];
        
        return $result;
    }
    
    /**
     * Posebna funkcija za download AccuntOrder
     * @param type $table_id
     * @param type $px_size
     * @param type $model
     * @param type $params
     * @return type
     */
    public static function AccountOrderDownloadZipWidgetAction($table_id, $px_size, $model, $params=[])
    {
        $zip_folder_name = date('Y_m_d_H_i_s').'.zip';
                
        $onclick = [
            'sima.accounting.multiselectAODownloadAsZip', 
            $table_id,
            $zip_folder_name
        ];
        
        $result = [
            'onclick' => $onclick,
            'icon' => "_zip",
            'tooltip' => Yii::t('BaseModule.SIMAHtmlButtons', 'MultiselectFilesDownloadZipTooltip')
        ];
        
        return $result;
    }
    
    public static function multiselectFileignatureViewWidgetAction($model, $table_id)
    {        
        if(get_class($model) !== File::class && !is_subclass_of($model, File::class))
        {
            throw new Exception('file_signature_view not used on file: '.get_class($model));
        }
        
        $result = [
            'onclick' => [
                'sima.files.multiselectSignatureView', 
                $table_id
            ],
            'icon' => "_review",
            'iconsize' => 16,
            'tooltip' => Yii::t('BaseModule.SIMAHtmlButtons', 'MultiselectFilesSignatureViewTooltip')
        ];
        
        return $result;
    }
    
    public static function multiselectFileignatureWidgetAction($model, $table_id)
    {        
        if(get_class($model) !== File::class && !is_subclass_of($model, File::class))
        {
            throw new Exception('file_signature not used on file: '.get_class($model));
        }
        
        $result = [
            'onclick' => [
                'sima.files.multiselectSignature', 
                $table_id
            ],
            'icon' => "_file_signature",
            'iconsize' => 16,
            'tooltip' => Yii::t('BaseModule.SIMAHtmlButtons', 'MultiselectFilesSignatureTooltip')
        ];
        
        return $result;
    }
    
    public static function multiselectQRCodeWidgetAction($table_id)
    {
        $result = [
            'onclick' => [
                'sima.icons.multiselectQRCode', 
                '$(this)',
                Yii::t('BaseModule.SIMAHtmlButtons', 'MultiselectQRCodeQuestion'),
                $table_id,
            ],
            'icon' => "_qrcode"
        ];
        
        return $result;
    }

    public static function multiselectAddBillsToAccountOrderWidgetAction($table_id)
    {
        $result = [
            'onclick' => [
                'sima.accountingMisc.addBillsToAccountOrder',
                $table_id
            ],
            'title' => Yii::t('AccountingModule.Bill', 'AddBillsToAccountOrder'),
            'tooltip' => Yii::t('AccountingModule.Bill', 'AddBillsToAccountOrderDesc')
        ];
        
        return $result;
    }
    
    public static function multiselectEmployeeTravelOrderPayoutWidgetAction($table_id)
    {
        $result = [
            'onclick' => [
                'sima.fixed_assets.employeeTravelOrderPayout',
                $table_id
            ],
            'title' => Yii::t('FixedAssetsModule.EmployeeTravelOrder', 'EmployeeTravelOrderPayout'),
            'tooltip' => Yii::t('FixedAssetsModule.EmployeeTravelOrder', 'EmployeeTravelOrderPayoutDesc')
        ];
        
        return $result;
    }
    
    public static function multiselectDeleteWidgetAction($table_id, $px_size)
    {
        $result = [
            'onclick' => [
                'sima.icons.multiselectDelete', 
                '$(this)',
                Yii::t('BaseModule.SIMAHtmlButtons', 'MultiselectDeleteQuestion'),
                $table_id,
            ],
            'icon' => "sima-icon _recycle _$px_size",
            'tooltip' => Yii::t('BaseModule.SIMAHtmlButtons', 'MultiselectDeleteTooltip')
        ];
        if(SIMAMisc::isVueComponentEnabled())
        {
            $result['icon'] = '_recycle';
            $result['iconsize'] = $px_size;
        }
        
        return $result;
    }
    
    public static function multiselectStatusWidgetAction($model, $status_column, $table_id, $px_size)
    {
        $html_options = [
            'data-table_id' => $table_id, 
            'data-is_multiselect' => 'true', 
            'class' => 'sima-old-btn-icon'
        ];

        if (is_array($status_column))
        {
            $model->{$status_column[0]} = $status_column['status_state'];
            $html_options['icon_class'] = $status_column['status_icon_class'];
            $status_column = $status_column[0];
        }
        else
        {
            $model->$status_column = false;
            if ($model->isColumnAllowNull($status_column) === true)
            {
                $model->$status_column = 'null';
            }
        }

        return [
            'html' => SIMAHtml::status(
                $model, 
                $status_column, 
                $px_size, 
                true, 
                '', 
                $html_options
            )
        ];
    }
    
    public static function ConfirmDesktopAppFileModificationsWidgetAction($event_relay_id, $original_id)
    {
        $result = [
            'icon' => '_confirm-da-file-modifications',
            'onclick' => [
                'sima.desktopapp.confirmDesktopAppFileModifications', $event_relay_id, $original_id
            ],
            'tooltip' => Yii::t('BaseModule.SIMAHtmlButtons', 'ConfirmChanges'),
            'htmlOptions' => [
                'class' => 'sda-btn',
                'data-original_id' => $original_id
            ]
        ];
        if(SIMAMisc::isVueComponentEnabled())
        {
            $result['html_options'] = [
                'data-code' => 'sda_original_id_'.$original_id
            ];
            unset($result['htmlOptions']);
        }
        
        return $result;
    }
    public static function RevertDesktopAppFileModificationsWidgetAction($event_relay_id, $original_id)
    {
        $result = [
            'icon' => '_revert-da-file-modifications',
            'onclick' => [
                'sima.desktopapp.revertDesktopAppFileModifications', $event_relay_id, $original_id
            ],
            'tooltip' => Yii::t('BaseModule.SIMAHtmlButtons', 'RevertChanges'),
            'htmlOptions' => [
                'class' => 'sda-btn',
                'data-original_id' => $original_id
            ]
        ];
        if(SIMAMisc::isVueComponentEnabled())
        {
            $result['html_options'] = [
                'data-code' => 'sda_original_id_'.$original_id
            ];
            unset($result['htmlOptions']);
        }
        
        return $result;
    }
    
    public static function GetFilePossibleConversionsWidgetActions(FileVersion $fileVersion, $action)
    {
        $subactions = [];
        foreach($fileVersion->GetPossibleConversions() as $extension)
        {
            $subactions[] = [
                'title' => $extension,
                'onclick' => [
                    $action, $fileVersion->id, $extension
                ]
            ];
        }
        
        return $subactions;
    }
}

