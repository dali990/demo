<?php

use Litipk\BigNumbers\Decimal;

class SIMABigNumber extends Decimal
{
    public static $separators = [];
    
    /**
     * Sledece funkcije su predefinisane sa novim nazivima, pa ako se pokusa koriscenje neke funkcije sa starim nazivom baca se greska
     */
    public function add(Decimal $b, int $scale = null): Decimal
    {
        $this->showForbiddenFunctionUseException('add', 'plus');
    }
    public function sub(Decimal $b, int $scale = null): Decimal
    {
        $this->showForbiddenFunctionUseException('sub', 'minus');
    }
    public function mul(Decimal $b, int $scale = null): Decimal
    {
        $this->showForbiddenFunctionUseException('mul', 'multiple');
    }
    public function div(Decimal $b, int $scale = null): Decimal
    {
        $this->showForbiddenFunctionUseException('div', 'divide');
    }
    
    /**
     * Wrapper oko Decimal add funkcije, samo je drugaciji naziv
     * @param SIMABigNumber $b
     * @param int $scale
     * @return SIMABigNumber
     */
    public function plus(SIMABigNumber $b, int $scale = null): SIMABigNumber
    {
        return parent::add($b, $scale);
    }

    /**
     * Wrapper oko Decimal sub funkcije, samo je drugaciji naziv
     * @param SIMABigNumber $b
     * @param int $scale
     * @return SIMABigNumber
     */
    public function minus(SIMABigNumber $b, int $scale = null): SIMABigNumber
    {
        return parent::sub($b, $scale);
    }
    
    /**
     * Wrapper oko Decimal mul funkcije. Mora da se vrsi provera da li prosledjeni broj 0,
     * jer inace mul funkcija vrsi tu proveru i vraca objekat Decimal, a nama treba da se vrati objekat SIMABigNumber
     * @param SIMABigNumber $b
     * @param int $scale
     * @return SIMABigNumber
     */
    public function multiple(SIMABigNumber $b, int $scale = null): SIMABigNumber
    {
        if ($b->isZero()) 
        {
            return SIMABigNumber::fromInteger(0);
        }
        
        return parent::mul($b, $scale);
    }

    /**
     * Wrapper oko Decimal div funkcije. Mora da se vrsi provera da li prosledjeni broj 0,
     * jer inace div funkcija vrsi tu proveru i vraca objekat Decimal, a nama treba da se vrati objekat SIMABigNumber
     * @param SIMABigNumber $b
     * @param int $scale
     * @return SIMABigNumber
     */
    public function divide(SIMABigNumber $b, int $scale = null): SIMABigNumber
    {
        if ($this->isZero())
        {
            return SIMABigNumber::fromInteger(0);
        }
        
        return parent::div($b, $scale);
    }
    
    /**
     * Kreira instancu SIMABigNumber na osnovu zadatog broja kao string, ali pre toga skine iz tog stringa znakove za separaciju decimala i hiljada
     * koji su zadati u sima konfiguraciji
     * @param string $str_value
     * @param int $scale
     * @return SIMABigNumber
     */
    public static function fromString(string $str_value, int $scale = null): Decimal
    {
        $separators = SIMABigNumber::getSeparators();
        
        $new_str_value = str_replace($separators, ['', '.'], $str_value);
        
        return parent::fromString($new_str_value, $scale);
    }
    
    public function toString()
    {
        return $this->__toString();
    }
    
    /**
     * Vraca formatiran prikaz broja na osnovu konfiguracije u simi za decimalni znak i znaka za razmak izmedju hiljada
     * @return string
     */
    public function __toString() : string
    {
        $separators = SIMABigNumber::getSeparators();
        
        $left_side = $this->value;
        $right_side = null;
        if (strpos($this->value, '.') !== false)
        {
            $value_parts = explode('.', $this->value);
            $left_side = $value_parts[0];
            $right_side = $value_parts[1];
        }

        $formated_value = strrev(implode($separators['thousands'], str_split(strrev($left_side), 3)));

        if (!is_null($right_side))
        {
            $formated_value .= $separators['decimals'] . $right_side;
        }
        
        return $formated_value;
    }
    
    /**
     * Vraca pravu, neformatiranu vrednost
     * @return string
     */
    public function getValue() : string
    {
        return $this->value;
    }
    
    /**
     * Vraca decimalni znak i znak za razmak izmedju hiljada na osnovu sima konfiguracije
     * @return array - oblika ['thousands' => 'znak za hiljade', 'decimals' => 'znak za decimale']
     */
    public static function getSeparators() : array
    {
        if (!empty(self::$separators))
        {
            return self::$separators;
        }
        
        $thousands_separator = null;
        $decimals_separator = null;

        if(Yii::app()->isWebApplication())
        {
            $thousands_separator = Yii::app()->user->model->confParamValue('base.amount_field_thousands');
            $decimals_separator = Yii::app()->user->model->confParamValue('base.amount_field_decimals');
        }
        else
        {
            $thousands_separator = Yii::app()->configManager->get('base.amount_field_thousands');
            $decimals_separator = Yii::app()->configManager->get('base.amount_field_decimals');
        }
        
        $new_thousands_separator = $thousands_separator === 'space' ? ' ' : ($decimals_separator === '.' ? ',' : '.');
        
        self::$separators = [
            'thousands' => $new_thousands_separator,
            'decimals' => $decimals_separator
        ];
        
        return self::$separators;
    }
    
    private function showForbiddenFunctionUseException($old_func_name, $new_func_name)
    {
        throw new SIMAException("SIMABigNumber - Funkcija '$old_func_name' je zamenjena funkcijom '$new_func_name'.");
    }
}

