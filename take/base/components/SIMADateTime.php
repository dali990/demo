<?php

class SIMADateTime extends DateTime
{
    /**
     * Vraca broj dana za koliko se razlikuju dva datuma
     * @param DateTime $datetime
     * @return -broj - ako je $datetime veci od datuma nad kojim se pozvala funkcija, +broj - inace
     */
    public function diffDays(DateTime $datetime)
    {
        $diff_days = $this->diff($datetime)->format("%a");
        if ($datetime > $this)
        {
            return -1 * intval($diff_days);
        }
        else
        {
            return intval($diff_days);
        }
    }
}

