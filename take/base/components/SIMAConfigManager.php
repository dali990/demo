<?php

class SIMAConfigManager
{
    public static $TYPE_SEARCH_FIELD = 'searchField';
    public static $TYPE_BOOLEAN = 'boolean';
    public static $TYPE_NUMERIC = 'numeric';
    
    private $_system_config_definitions = array();
    private $_document_types_config_definitions = array();
    private $_config_params = array();
    private $_path_display_names = array();
    private $_configurationsLoaded = false;
    
    public function init()
    {
    }

    public function get($path, $warn=true, $user_id=null)
    {
        $this->loadConfigurationDefinitions();
        
        $param = $this->getConfigParam($path, $warn, true, $user_id);
        return $param['value'];
    }
    
    public function set($path, $value, $is_user_personal=true, $user_id=null)
    {
        $this->loadConfigurationDefinitions();
        
        $value_for_db = null;
        
        $confParamDefinition = $this->getConfigParamDefinition($path);
            
        if(is_array($value) && (!isset($confParamDefinition['value_is_array']) 
                                || $confParamDefinition['value_is_array'] === false))
        {
            $value = $value[0];
        }
        
        if($confParamDefinition['type'] === 'searchField' && empty($value))
        {

        }
        else
        {
            $value_for_db = CJSON::encode($value);
        }
                
        if($is_user_personal === true || $is_user_personal === 'true') // ako korisnik postavlja sebi vrednost
        {            
            if(is_null($user_id))
            {
                $user_id = Yii::app()->user->id;
            }
            $dbConfig = $this->getConfigParamDbData($path, $user_id);
            
            // ako je vec dobijen taj red (vec postoji), samo update-ovati
            if($dbConfig['user_changable'] && $dbConfig['user_id'] === intval($user_id))
            {
                $criteriaConfigurationParameter = new SIMADbCriteria([
                    'Model' => 'ConfigurationParameter',
                    'model_filter' => array(
                        'ids'=>$dbConfig['id']
                    )
                ]);
                
                $configParam = ConfigurationParameter::model()->find($criteriaConfigurationParameter);
                $configParam->value = $value_for_db;
                $configParam->save();
            }
            else // inace insert
            {                
                $confParam = new ConfigurationParameter();
                $confParam->mapping_id = $dbConfig['mapping_id'];
                $confParam->user_changable = $dbConfig['user_changable'];
                $confParam->user_id = $user_id;
                $confParam->value = $value_for_db;
                $confParam->save();
            }
        }
        else // ako administrator postavlja globalnu vrednost
        {    
            $dbConfig = $this->getConfigParamDbData($path, null);
                        
            $configParam = ConfigurationParameter::model()->findByPk($dbConfig['id']);
            $configParam->value = $value_for_db;
            $configParam->save();
        }
        
        return true;
    }
    
    public function flush()
    {
        $this->_config_params = [];
    }
    
    public function setDefault($path, $is_user_personal=true, $user_id=null)
    {
        $this->loadConfigurationDefinitions();
        
        $value_for_db = null;
        $confParamWithDefinition = $this->getConfigParam($path, false, $is_user_personal);
        
        if($is_user_personal === true) // ako korisnik postavlja sebi vrednost
        {
            if(is_null($user_id))
            {
                $user_id = Yii::app()->user->id;
            }
            $dbConfig = $this->getConfigParamDbData($path, $user_id);
        }
        else
        {
            $dbConfig = $this->getConfigParamDbData($path);
        }
        
        $default_value = $this->parseDefaultValueToSet($confParamWithDefinition);
        
        if(isset($default_value))
        {
            $value_for_db = CJSON::encode($default_value);
        }
                
        $configParam = ConfigurationParameter::model()->findByPk($dbConfig['id']);
        $configParam->value = $value_for_db;
        $configParam->save();
    }
    
    public function userChangableChanged($path)
    {
        $this->loadConfigurationDefinitions();
        
        $dbConfig = $this->getConfigParamDbData($path, null);
        
        $configParam = ConfigurationParameter::model()->findByPk($dbConfig['id']);
        $configParam->user_changable = !$configParam->user_changable;
        $configParam->save();
    }
    
//    public function getDocTypeGroupValues($path, $warn=true)
    public function getDocTypeGroupValues()
    {
        $this->loadConfigurationDefinitions();
        
        // treba umesto starog groupParamValues
        throw new Exception('not implemented');
    }
    
    public function getAllGroups($path, $type, $is_user_personal=false)
    {        
        $this->loadConfigurationDefinitions();
                
        $allGroupsArray = array();
        
        $is_user_personal_text = 'false';
        if($is_user_personal)
        {
            $is_user_personal_text = 'true';
        }
        
        if($is_user_personal===false && (!isset($path) || !isset($type)))
        {
            $docTypesDefinitions_keys = array_keys($this->_document_types_config_definitions);
            foreach($docTypesDefinitions_keys as $key)
            {
                $paramDefinition = $this->getConfigParam($key, false);
                
                if($is_user_personal 
                        && (!isset($paramDefinition['user_changable']) || $paramDefinition['user_changable'] !== true))
                {
                    continue;
                }
                $allGroupsArray[] = array(
                    'title' => Yii::t('BaseModule.Configuration', 'DocumentTypes'),
                    'data' => array(
                        'path' => '',
                        'type' => 'doctypes',
                    ),
                    'subtree' => $this->getAllGroups('', 'doctypes', $is_user_personal)                   
                );
                break;
            }

            $systemDefinitions_keys = array_keys($this->_system_config_definitions);
            foreach($systemDefinitions_keys as $key)
            {
                $paramDefinition = $this->getConfigParam($key, false);
                
                if($is_user_personal && $paramDefinition['user_changable'])
                {
                    continue;
                }
                
                $allGroupsArray[] = array(
                    'title' => Yii::t('BaseModule.Configuration', 'Configurations'),
                    'data' => array(
                        'path' => '',
                        'type' => 'sys',
                    ),
                    'subtree' => $this->getAllGroups('', 'sys', $is_user_personal)
                );
                break;
            }
        }
        else
        {
            if(empty($path))
            {
                $path = '';
            }

            if($type === 'doctypes')
            {
                $confDefinitions = $this->_document_types_config_definitions;
            }
            else
            {
                $confDefinitions = $this->_system_config_definitions;
            }
            
            $added_subgroups = array();
            $confDefinitions_keys = array_keys($confDefinitions);
            foreach($confDefinitions_keys as $key)
            {
                $pos = strrpos($key, $path, -strlen($key));
                
                if(!empty($path) && $pos === FALSE)
                {
                    continue;
                }
                
                $paramDefinition = $this->getConfigParam($key, false, false);
                
                if(strrpos(substr($key, strlen($path)+1), '.') === FALSE
                        && $paramDefinition['type'] !== 'group')
                {
                    continue;
                }
                
                if($is_user_personal)
                {
                    $paramDefinition = $this->getConfigParam($key, false);
                    if(!$paramDefinition['user_changable'])
                    {
                        continue;
                    }
                }
                
                $exp_key = explode('.', $key);

                if($pos !== false)
                {
                    $subkey = substr($key, strlen($path)+1);
                    $exp_subkey = explode('.', $subkey);
                    $tabName = $exp_subkey[0];
                    $tabPath = $path.'.'.$exp_subkey[0];
                }
                else
                {
                    $tabName = $exp_key[0];
                    $tabPath = $exp_key[0];
                }
                
                if(isset($added_subgroups[$tabPath]))
                {
                    if(isset($added_subgroups[$tabPath]['custom-action']))
                    {
                        unset($added_subgroups[$tabPath]['custom-action']);
                        
                        $allGroupsArray_keys = array_keys($allGroupsArray);
                        foreach($allGroupsArray_keys as $allGroupsArray_key)
                        {
                            unset($allGroupsArray[$allGroupsArray_key]['data']['custom-action']);
                        }
                    }
                    else
                    {
                        continue;
                    }
                }
                
                if(isset($this->_path_display_names[$tabPath]))
                {
                    $tabName = $this->_path_display_names[$tabPath];
                }

                $datas = array(
                    'path' => $tabPath,
                    'type' => $type,
                );

                $submenu = array();
                $superscript = '';
                if($paramDefinition['type'] === 'group' && isset($paramDefinition['type_params']['customAction']))
                {
                    $datas['custom-action'] = $paramDefinition['type_params']['customAction'];
                }
                else
                {
                    $submenu = $this->getAllGroups($tabPath, $type, $is_user_personal);

                    $config_parameters = Yii::app()->configManager->getConfTabParameters($tabPath, $type, $is_user_personal);
                    $config_parameters_count = count($config_parameters);
                    if($config_parameters_count > 0)
                    {
                        $superscript = $config_parameters_count;
                    }
                }

                $added_subgroups[$tabPath] = $datas;

                $element = array(
                    'data' => $datas,
                    'title' => $tabName
                );

                if(!empty($submenu))
                {
                    $element['subtree'] = $submenu;

                    if(!empty($superscript))
                    {
                        $element['title'] = [
                            'text'=>$tabName,
                            'parts'=>[
                                [
                                    'text'=>$superscript,
                                    'position'=>'after'
                                ]
                            ]
                        ];
                    }
                }
                
                $allGroupsArray[$type.'_'.$tabPath] = $element;
            }
        }
        
        usort($allGroupsArray, function($a, $b)
        {
            $title_a = $a['title'];
            $title_b = $b['title'];
            if (is_array($a['title']))
            {
                $title_a = $a['title']['text'];
            }
            if (is_array($b['title']))
            {
                $title_b = $b['title']['text'];
            }
            return strcasecmp($title_a, $title_b);
        });
        
        return $allGroupsArray;
    }
    
    public function getConfTabParameters($path, $type, $is_user_personal=false)
    {
        $this->loadConfigurationDefinitions();
        
        $parameters = array();
        
        if(!empty($path))
        {
            if($type === 'doctypes')
            {
                $confDefinitions = $this->_document_types_config_definitions;
            }
            else
            {
                $confDefinitions = $this->_system_config_definitions;
            }

            $confDefinitions_keys = array_keys($confDefinitions);
            foreach($confDefinitions_keys as $key)
            {
                $pos = strrpos($key, $path, -strlen($key));

                if($pos !== FALSE && strrpos(substr($key, strlen($path)+1), '.')===FALSE)
                {
                    $configParam = null;
                    if($is_user_personal)
                    {
                        $configParam = $this->getConfigParam($key, false);
                        if(!$configParam['user_changable'])
                        {
                            continue;
                        }
                    }
                    else
                    {
                        $configParam = $this->getConfigParam($key, false, false);
                    }
                    
                    if(isset($configParam['type_params']) && isset($configParam['type_params']['customAction']))
                    {
                        continue;
                    }

                    $parameters[] = $configParam;
                }
            }
        }
                
        return $parameters;
    }
    
    public function getModelChooseParams($path, $is_user_personal)
    {
        $configParam = $this->getConfigParam($path, false, $is_user_personal);
        
        $model = $configParam['type_params']['modelName'];
        $params = [];
        
        $search_model_settings = $model::model()->modelSettings('search_model');
        if(isset($search_model_settings['action']))
        {
            $params['params']['search_action'] = $search_model_settings['action'];
        }
        
        if(isset($configParam['value_is_array']) && $configParam['value_is_array'] === true)
        {
            $params['multiselect'] = true;
            $params['ids'] = [];
                        
            foreach($configParam['value'] as $model_id)
            {
                $initial_model = $model::model()->findByPk($model_id);
                
                $params['ids'][] = [
                    'id' => $initial_model->id,
                    'display_name' => $initial_model->DisplayName
                ];
            }
        }
        else
        {
            $params['multiselect'] = false;
            $params['model_id'] = null;
            if (!empty($model) && !empty($configParam['value']))
            {
                $_model = $model::model()->findByPk($configParam['value']);
                if (!empty($_model))
                {
                    $params['model_id'] = $configParam['value'];
                }
            }
        }
        
        if(isset($configParam['type_params']['fixed_filter']))
        {
            $params['fixed_filter'] = $configParam['type_params']['fixed_filter'];
        }
        
        if($configParam['type'] === 'searchField')
        {
            $can_be_null = true;
            if(isset($configParam['value_not_null']) && $configParam['value_not_null'] === true)
            {
                $can_be_null = false;
            }
            $params['can_be_null'] = $can_be_null;
        }
                
        return [
            'model' => $model,
            'params' => $params
        ];
    }
    
    public function renderChangeParamDialog($uniq_id, $path, $is_user_personal)
    {
        $this->loadConfigurationDefinitions();
        
        $dialogHtml = '';
        $fullscreen = false;
        
        $configParam = $this->getConfigParam($path, false, $is_user_personal);
        
        switch ($configParam['type'])
        {
            case 'dropdown': 
                $dialogHtml = Yii::app()->controller->renderPartial('base.views.configuration.dropdown_dialog', array(
                    'uniq_id' => $uniq_id,
                    'configParam' => $configParam
                ), true, true);
                break;
            case 'textField':
                $dialogHtml = Yii::app()->controller->renderPartial('base.views.configuration.textField_dialog', array(
                    'uniq_id' => $uniq_id,
                    'current_value' => $configParam['value']
                ), true, true);
                break;
            case 'boolean':
                $configParam['type_params'] = [
                    [
                        'title' => Yii::t('BaseModule.Common', 'Yes'),
                        'value' => true
                    ],
                    [
                        'title' => Yii::t('BaseModule.Common', 'No'),
                        'value' => false
                    ]
                ];
                $dialogHtml = Yii::app()->controller->renderPartial('base.views.configuration.dropdown_dialog', array(
                    'uniq_id' => $uniq_id,
                    'configParam' => $configParam
                ), true, true);
                break;
            case SIMAConfigManager::$TYPE_NUMERIC:
                $dialogHtml = Yii::app()->controller->renderPartial('base.views.configuration.numeric_dialog', array(
                    'uniq_id' => $uniq_id,
                    'display_name' => $configParam['display_name'],
                    'current_value' => $configParam['value'],
                    'type_params' => $configParam['type_params']
                ), true, true);
                break;
            default: 
                throw new Exception('unsupported conf type: '.CJSON::encode($configParam['type']));
        }
        
        return array(
            'html' => $dialogHtml,
            'type' => $configParam['type'],
            'fullscreen' => $fullscreen
        );
    }
    
    public function validate($throwException=true)
    {
        $this->loadConfigurationDefinitions();
        
        $error_messages = array();
        
        $path_display_names_count = count($this->_path_display_names);
        $system_config_definitions_count = count($this->_system_config_definitions);
        $document_types_config_definitions_count = count($this->_document_types_config_definitions);
        
        if($path_display_names_count !== ($system_config_definitions_count + $document_types_config_definitions_count))
        {
            $error_messages[] = Yii::t('BaseModule.Configuration', 'InvalidCounts', array(
                '{path_display_names_count}' => $path_display_names_count,
                '{system_config_definitions_count}' => $system_config_definitions_count,
                '{document_types_config_definitions_count}' => $document_types_config_definitions_count
            ));
        }
        
        $passed_values = array();
        foreach(Yii::app()->params["configMapper"] as $key => $value)
        {
            if(in_array($value, $passed_values))
            {
                $error_messages[] = Yii::t('BaseModule.Configuration', 'ConfValidationMappingIdDoubled', array(
                    '{param}' => $key,
                    '{mappingid}' => $value
                ));
            }
            else
            {
                $passed_values[] = $value;
            }
        }
        
        if($throwException === true && count($error_messages) > 0)
        {
            $imploded = implode('</br>', $error_messages);
            
            throw new SIMAWarnException($imploded);
        }
        
        return $error_messages;
    }
    
    public function getByConfigurationParameter(ConfigurationParameter $configurationParamter)
    {        
        $result = null;
        
        $this->loadConfigurationDefinitions();
                
        $confPath = null;
        
        foreach(Yii::app()->params["configMapper"] as $key => $value)
        {
            if($value === $configurationParamter->mapping_id)
            {
                $confPath = $key;
                break;
            }
        }
        
        if(!isset($confPath))
        {
            throw new Exception(Yii::t('BaseModule.Configuration', 'NoMappingForId', array(
                '{mappingid}' => $configurationParamter->mapping_id
            )));
        }
        
        $userConfigParam = $this->getConfigParam($confPath, false, true);                 
        $systemConfigParam = $this->getConfigParam($confPath, false, false);
        
        if($systemConfigParam['dbId'] === $configurationParamter->id)
        {
            $result = $systemConfigParam;
        }
        else if($userConfigParam['dbId'] === $configurationParamter->id)
        {
            $result = $userConfigParam;
        }
        else
        {
            throw new Exception(Yii::t('BaseModule.Configuration', 'ConfNotFoundForDbId', array(
                '{dbId}' => $configurationParamter->id
            )));
        }
        
        return $result;
    }
    
    public function getDefinitionByConfigurationParameter(ConfigurationParameter $configurationParamter)
    {        
//        $result = null;
        
        $this->loadConfigurationDefinitions();
                
        $confPath = null;
        
        foreach(Yii::app()->params["configMapper"] as $key => $value)
        {
            if($value === $configurationParamter->mapping_id)
            {
                $confPath = $key;
                break;
            }
        }
        
        if(!isset($confPath))
        {
            throw new Exception(Yii::t('BaseModule.Configuration', 'NoMappingForId', array(
                '{mappingid}' => $configurationParamter->mapping_id
            )));
        }
        
        $result = $this->getConfigParamDefinition($confPath);
        
        return $result;
    }
    
    public function mustSetNulledParams()
    {
        $result = array();
        
        $this->loadConfigurationDefinitions();
        
        $mapping_keys = array_keys(Yii::app()->params["configMapper"]);
        foreach($mapping_keys as $value)
        {
            $systemConfParam = $this->getConfigParam($value, false, false);
            
            if(isset($systemConfParam['value_not_null']) && $systemConfParam['value_not_null'] === true
                    && (!isset($systemConfParam['value']) 
                            || $systemConfParam['value'] === '' 
                            || empty($systemConfParam['value']))
                    && is_null($systemConfParam['default']))
            {                
                $result[] = $systemConfParam;
            }
        }
        
        return $result;
    }
    
    public function documentTypeGroupParamValues($path, $warn=true)
    {
        $values=array();
        
        $this->loadConfigurationDefinitions();
        
        $params = $this->documentTypeGroupParams($path, $warn);
        
        foreach ($params as $param)
        {
            if(isset($param['value']))
            {
                if($param['value_is_array'] === true)
                {
                    $values=array_merge($values, $param['value']);
                }
                else
                {
                    $values[] = $param['value'];
                }
            }
        }
        
        return $values;
    }
    
    public function documentTypeParamByValue($value)
    {
        $result = null;
        
        $this->loadConfigurationDefinitions();
                
        $params = $this->documentTypeGroupParams('', false);
                
        foreach($params as $param)
        {
            if($param['value'] == $value
                || (is_array($param['value']) && in_array($value, $param['value'])))
            {
                $result = $param;
                break;
            }
        }
                
        return $result;
    }
    
    public function getConfigParameterMappingId($path)
    {
        $this->loadConfigurationDefinitions();
              
        $dbId = $this->getConfigParameterMappingIdFromDb($path);
                
        return $dbId;
    }
    
    public function getPathDisplayName($path)
    {
        $this->loadConfigurationDefinitions();
        
        if(!isset($this->_path_display_names[$path]))
        {
            throw new Exception(Yii::t('BaseModule.Configuration', 'PathDisplayNameNotSet', array('{path}' => $path)));
        }
        
        $pathName = $this->_path_display_names[$path];
        
        return $pathName;
    }
    
    private function getConfigParameterMappingIdFromDb($path)
    {        
        if(!isset(Yii::app()->params["configMapper"][$path]))
        {
            throw new Exception('error in requesting conf with path: '.$path);
        }
                        
        $dbId = Yii::app()->params["configMapper"][$path];
                
        return $dbId;
    }
    
    private function documentTypeGroupParams($path, $warn=true)
    {
        $documentTypeGroupParams = array();
                
        $confDefinitions_keys = array_keys($this->_document_types_config_definitions);
                
        foreach($confDefinitions_keys as $key)
        {
            $pos = strrpos($key, $path, -strlen($key));

            if($path === '' || ($pos !== FALSE && strrpos(substr($key, strlen($path)+1), '.')===FALSE))
            {
                $configParam = $this->getConfigParam($key, $warn);
                
                $documentTypeGroupParams[] = $configParam;
            }
        }
        
        return $documentTypeGroupParams;
    }
    
    private function getConfigParamDefinition($path)
    {
        $paramDefinition = null;
        
        if(isset($this->_system_config_definitions[$path]))
        {
            $paramDefinition = $this->_system_config_definitions[$path];
        }
        else if(isset($this->_document_types_config_definitions[$path]))
        {
            $paramDefinition = $this->_document_types_config_definitions[$path];
        }
        else
        {
            throw new SIMAWarnException('config path not defined: '.$path);
        }
        
        return $paramDefinition;
    }
    
    private function getConfigParamDbData($path, $for_user_id=null)
    {
        $dbMappingId = $this->getConfigParameterMappingIdFromDb($path);
        
        $dbConfig = SIMASQLFunctions::getConfigParamDbData($dbMappingId, $for_user_id);
        $paramDefinition = $this->getConfigParamDefinition($path);
        
        if(!isset($dbConfig))
        {
            $default_value = $this->parseDefaultValueToSet($paramDefinition);
            
            $value_for_db = null;
            if(isset($default_value))
            {
                $value_for_db = CJSON::encode($default_value);
            }
            
            $user_changable = false;
            if(isset($paramDefinition['user_changable']))
            {
                $user_changable = $paramDefinition['user_changable'];
            }
                    
            $confParam = new ConfigurationParameter();
            $confParam->mapping_id = $dbMappingId;
            $confParam->value = $value_for_db;
            $confParam->user_changable = $user_changable;
            $confParam->save();
            
            $dbConfig = SIMASQLFunctions::getConfigParamDbData($dbMappingId, $for_user_id);
        }
        
        if(isset($paramDefinition['value_not_null']) && $paramDefinition['value_not_null']===true && empty($dbConfig['value']) && isset($paramDefinition['default']))
        {
            $default_value = $this->parseDefaultValueToSet($paramDefinition);
            
            $value_for_db = null;
            if(isset($default_value))
            {
                $value_for_db = CJSON::encode($default_value);
            }
            
            if(!empty($value_for_db))
            {
                $configParam = ConfigurationParameter::model()->findByPkWithCheck($dbConfig['id']);
                $configParam->value = $value_for_db;
                $configParam->save();
            }
        }
        
        if(
                !is_null($for_user_id) 
                && (
                        !isset($paramDefinition['user_changable'])
                        || (isset($paramDefinition['user_changable']) && $paramDefinition['user_changable'] !== true)
                )
                && $dbConfig['user_changable'] === true 
                && $dbConfig['user_id'] !== (int)$for_user_id
        )
        {
            $confParam = new ConfigurationParameter();
            $confParam->mapping_id = $dbConfig['mapping_id'];
            $confParam->value = $dbConfig['value'];
            $confParam->user_changable = $dbConfig['user_changable'];
            $confParam->user_id = $for_user_id;
            $confParam->save();
            
            $dbConfig = SIMASQLFunctions::getConfigParamDbData($dbMappingId, $for_user_id);
        }
        
        return $dbConfig;
    }
    
    public function getConfigParam($path, $warn=true, $is_user_personal=true, $user_id=null)
    {
        $result = null;
        
        $for_user_id = null;
        if($is_user_personal)
        {
            $for_user_id = $user_id;
            if(!isset($for_user_id) && Yii::app()->isWebApplication())
            {
                $for_user_id = Yii::app()->user->id;
            }
        }
        
        $real_path = $path;
        
        if($is_user_personal === true)
        {
            $real_path .= '_is_user_personal_true_'.$for_user_id;
        }
        else
        {
            $real_path .= '_is_user_personal_false';
        }
                        
        if (!isset($this->_config_params[$real_path]))
        {
            $paramDefinition = $this->getConfigParamDefinition($path);
            
            if($paramDefinition['type'] === 'group')
            {
                $result = $paramDefinition;
            }
            else
            {
                $dbConfig = $this->getConfigParamDbData($path, $for_user_id);
                
//                if($is_user_personal)
//                {
//                    $dbConfigSystem = $this->getConfigParamDbData($path, null);
//                    $paramDefinition['default'] = CJSON::decode($dbConfigSystem['value']);
//                }

                $dbId = $dbConfig['id'];
                $mappingId = $dbConfig['mapping_id'];
                $value = CJSON::decode($dbConfig['value']);
                $user_changable = $dbConfig['user_changable'];
                if($is_user_personal)
                {
                    $dbConfigSystem = $this->getConfigParamDbData($path, null);
                    $paramDefinition['default'] = CJSON::decode($dbConfigSystem['value']);
                    
                    if(isset($paramDefinition['user_changable']) && $paramDefinition['user_changable'] === false)
                    {
                        $value = CJSON::decode($dbConfigSystem['value']);
                        $user_changable = false;
                    }
                }

                $value_not_null = false;
                if(isset($paramDefinition['value_not_null']) && $paramDefinition['value_not_null'] === true)
                {                
                    $value_not_null = true;
                }

                if($warn === true && (!isset($value) || $value === ''))
                {
                    if(isset($paramDefinition['default']) && $paramDefinition['type'] === SIMAConfigManager::$TYPE_BOOLEAN)
                    {
                        $value = false;
                    }
                    else
                    {
                        throw new SIMAWarnException(Yii::t('BaseModule.Configuration', 'ParameterMustBeSet', array(
                            '{path}' => $path,
                            '{display_name}' => $paramDefinition['display_name']
                        )));
                    }
                }
                
                if($paramDefinition['type'] === SIMAConfigManager::$TYPE_BOOLEAN)
                {
                    $value = filter_var($value, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
                }

                $value_is_array = false;
                if(isset($paramDefinition['value_is_array']) && $paramDefinition['value_is_array'] === true)
                {                
                    $value_is_array = true;

                    if(empty($value))
                    {
                        $value = array();
                    }
                }
                $user_change_forbidden = false;
                if(isset($paramDefinition['user_change_forbidden']) && $paramDefinition['user_change_forbidden'] === true)
                {
                    $user_change_forbidden = true;
                }

                $type_params = array();
                if(isset($paramDefinition['type_params']))
                {
                    $type_params = $paramDefinition['type_params'];
                }
                $default = null;
                if(isset($paramDefinition['default']))
                {
                    $default = $paramDefinition['default'];
                }
                $description = '';
                if(isset($paramDefinition['description']))
                {
                    $description = $paramDefinition['description'];
                }
                $display_to_users = true;
                if(isset($paramDefinition['display_to_users']))
                {
                    $display_to_users = $paramDefinition['display_to_users'];
                }
                
                $configParam = $paramDefinition;
                $configParam['user_change_forbidden'] = $user_change_forbidden;
                $configParam['dbId'] = $dbId;
                $configParam['value'] = $value;
                $configParam['mapping_id'] = $mappingId;
                $configParam['path'] = $path;
                $configParam['type_params'] = $type_params;
                $configParam['value_is_array'] = $value_is_array;
                $configParam['value_not_null'] = $value_not_null;
                $configParam['default'] = $default;
                $configParam['user_changable'] = $user_changable;
                $configParam['description'] = $description;
                $configParam['display_to_users'] = $display_to_users;

                $result = $configParam;
            }
            
            $this->_config_params[$real_path] = $result;
        }
        else
        {
            $result = $this->_config_params[$real_path];
        }
                        
        return $result;
    }
    
    public function loadConfigurationDefinitions()
    {
        if($this->_configurationsLoaded === false)
        {            
            unset($this->_system_config_definitions);
            unset($this->_document_types_config_definitions);
            unset($this->_config_params);
            unset($this->_path_display_names);
            
            $this->validateMapper();
            
            $languageToUse = null;
            $user_id = null;
            
            if (Yii::app()->isWebApplication())
            {
                $dbMappingId = $this->getConfigParameterMappingIdFromDb('base.language');                
                $userDbConfig = SIMASQLFunctions::getConfigParamDbData($dbMappingId, Yii::app()->user->id);
                if(isset($userDbConfig))
                {
                    $user_language = CJSON::decode($userDbConfig['value']);
                    if(isset($user_language) && $user_language !== '')
                    {
                        $languageToUse = $user_language;
                        Yii::app()->language = $user_language;
                    }
                }
                
                $user_id = Yii::app()->user->id;
            }
            
            $sizeToUse = 1;
            if(!(Yii::app()->simaDemo->IsDemoISSIMA() && Yii::app()->isConsoleApplication()))
            {
                $simaSizeDbMappingId = $this->getConfigParameterMappingIdFromDb('base.sima_size');                
                $simaSizeUserDbConfig = SIMASQLFunctions::getConfigParamDbData($simaSizeDbMappingId, $user_id);
                if(isset($simaSizeUserDbConfig))
                {
                    $user_sima_size = CJSON::decode($simaSizeUserDbConfig['value']);
                    if(isset($user_sima_size) && $user_sima_size !== '')
                    {
                        $sizeToUse = $user_sima_size;
                    }
                }
            }
            Yii::app()->sima_size = $sizeToUse;
            
            if(!isset($languageToUse))
            {
                $baseModuleConfPath = Yii::getPathOfAlias('base.config');
                $base_definitions = include($baseModuleConfPath.'/systemConfigurations.php');
                
                foreach ($base_definitions as $key => $definition) 
                {
                    if (is_array($definition) && $definition['name'] === 'language') 
                    {
                        $languageDefinition = $base_definitions[$key];
                        break;
                    }
                }
                
                if(!isset($languageDefinition))
                {
                    throw new Exception(Yii::t('BaseModule.Configuration', 'NoLanguageDefinition'));
                }
                                
                $languageToUse = $base_definitions[$key]['default'];
            }
            Yii::app()->language = $languageToUse;
            
            if (Yii::app()->isWebApplication())
            {
                if(!isset($sizeToUse))
                {
                    $baseModuleConfPath = Yii::getPathOfAlias('base.config');
                    $base_definitions = include($baseModuleConfPath.'/systemConfigurations.php');

                    foreach ($base_definitions as $key => $definition) 
                    {
                        if (is_array($definition) && $definition['name'] === 'sima_size') 
                        {
                            $simaSizeDefinition = $base_definitions[$key];
                            break;
                        }
                    }

                    if(!isset($simaSizeDefinition))
                    {
                        throw new Exception(Yii::t('BaseModule.Configuration', 'NoSIMASizeDefinition'));
                    }

                    $sizeToUse = $base_definitions[$key]['default'];
                }
                Yii::app()->sima_size = $sizeToUse;
            }
        
            $modules = Yii::app()->getModules();
            $modules_keys = array_keys($modules);
            foreach($modules_keys as $key)
            {
                $module = Yii::app()->getModule($key);
                $this->loadConfigurationDefinitionsFromModule($module, '');
            }
            
            $this->_configurationsLoaded = true;
        }
    }
    
    private function loadConfigurationDefinitionsFromModule(CWebModule $module, $pathPrefix)
    {
        $modulePrefix = $module->getName();
        if(!empty($pathPrefix))
        {
            $modulePrefix = $pathPrefix.'.'.$module->getName();
        }
        $predefinedFiles = ['systemConfigurations', 'documentTypeConfigurations'];
        
        foreach($predefinedFiles as $predefinedFile)
        {
            $isLoadingDocTypes = false;
            if($predefinedFile === 'documentTypeConfigurations')
            {
                $isLoadingDocTypes = true;
            }
            
            if (file_exists($module->basePath.'/config/'.$predefinedFile.'.php'))
            {
                $predefinedFileConfigurations = include($module->basePath.'/config/'.$predefinedFile.'.php');
                foreach($predefinedFileConfigurations as $key => $conf)
                {     
                    if(is_array($conf))
                    {
                        $this->validateConf($conf);
                        
                        $newPrefix = $modulePrefix.'.'.$conf['name'];
                        
                        if(isset($this->_path_display_names[$newPrefix])
                            || isset($this->_document_types_config_definitions[$newPrefix])
                            || isset($this->_system_config_definitions[$newPrefix]))
                        {
                            throw new SIMAWarnException(Yii::t('BaseModule.Configuration', 'PathInUse', array('{path}' => $newPrefix)));
                        }
                        
                        $this->addToPathDisplayNames($newPrefix, $conf['name']);
                        if(isset($conf['display_name']))
                        {
                            $this->addToPathDisplayNames($newPrefix, $conf['display_name']);
                        }

                        if($conf['type'] === 'group' && !isset($conf['type_params']['customAction']))
                        {
                            $this->loadConfigurationDefinitionsFromGroup($conf['type_params'], $newPrefix, $isLoadingDocTypes);
                        }
                        else
                        {
                            if($isLoadingDocTypes)
                            {
                                $this->_document_types_config_definitions[$newPrefix] = $conf;
                            }
                            else
                            {
                                $this->_system_config_definitions[$newPrefix] = $conf;
                            }
                        }
                    }
                    else
                    {
                        if($key === 'display_name')
                        {
                            $this->addToPathDisplayNames($modulePrefix, $conf);
                        }
                    }
                    
                    if(!isset($this->_path_display_names[$modulePrefix]))
                    {
                        $this->addToPathDisplayNames($modulePrefix, $module->getName());
                    }
                }
            }
        }
        
        $submodules = $module->getModules();
        $submodules_keys = array_keys($submodules);
        foreach($submodules_keys as $key)
        {
            $submodule = $module->getModule($key);
            $this->loadConfigurationDefinitionsFromModule($submodule, $modulePrefix);
        }
    }
    
    private function loadConfigurationDefinitionsFromGroup(array $groupDatas, $pathPrefix, $isLoadingDocTypes)
    {
        foreach($groupDatas as $datas)
        {
            $this->validateConf($datas);

            $newPrefix = $pathPrefix.'.'.$datas['name'];

            if(isset($this->_path_display_names[$newPrefix])
                || isset($this->_document_types_config_definitions[$newPrefix])
                || isset($this->_system_config_definitions[$newPrefix]))
            {
                throw new SIMAWarnException(Yii::t('BaseModule.Configuration', 'PathInUse', array('{path}' => $newPrefix)));
            }

            $this->addToPathDisplayNames($newPrefix, $datas['name']);
            if(isset($datas['display_name']))
            {
                $this->addToPathDisplayNames($newPrefix, $datas['display_name']);
            }

            if($datas['type'] === 'group')
            {
                $this->loadConfigurationDefinitionsFromGroup($datas['type_params'], $newPrefix, $isLoadingDocTypes);
            }
            else
            {
                if($isLoadingDocTypes)
                {
                    $this->_document_types_config_definitions[$newPrefix] = $datas;
                }
                else
                {
                    $this->_system_config_definitions[$newPrefix] = $datas;
                }
            }
        }
    }
    
    private function validateConf(array $conf, $throwException=true)
    {
        $error_messages = array();
        
        if(!isset($conf['type']))
        {
            $error_messages[] = Yii::t('BaseModule.Configuration', 'ConfValidationFailNoType', array(
                '{conf}' => CJSON::encode($conf)
            ));
        }
        else if($conf['type'] !== 'group' 
                    && $conf['type'] !== 'dropdown'
                    && $conf['type'] !== SIMAConfigManager::$TYPE_SEARCH_FIELD
                    && $conf['type'] !== 'textField'
                    && $conf['type'] !== 'customAction'
                    && $conf['type'] !== SIMAConfigManager::$TYPE_BOOLEAN
                    && $conf['type'] !== SIMAConfigManager::$TYPE_NUMERIC
                )
        {
            $error_messages[] = Yii::t('BaseModule.Configuration', 'ConfValidationInvalidType', array(
                '{conf}' => CJSON::encode($conf)
            ));
        }
        
        if($conf['type'] === SIMAConfigManager::$TYPE_SEARCH_FIELD
                && isset($conf['default'])
                && (
                    empty($conf['default'])
                    || !is_array($conf['default'])
                )
        )
        {
            $error_messages[] = Yii::t('BaseModule.Configuration', 'ConfValidationInvalidDefault', array(
                '{conf}' => CJSON::encode($conf)
            ));
        }
        
        if($throwException === true && count($error_messages) > 0)
        {
            $imploded = implode('</br>', $error_messages);
            
            throw new Exception($imploded);
        }
        
        return $error_messages;
    }
    
    private function addToPathDisplayNames($path, $displayName)
    {
        $this->_path_display_names[$path] = $displayName;
    }
    
    private function validateMapper($throwException=true)
    {
        $error_messages = array();
        
        $mapperValues = Yii::app()->params["configMapper"];
        
        $passed_mapping_ids = array();
        
        foreach($mapperValues as $mapperValue)
        {
            if(in_array($mapperValue, $passed_mapping_ids))
            {
                $error_messages[] = Yii::t('BaseModule.Configuration', 'MappingIdDuplicated', ['{mapping_id}' => $mapperValue]);
            }
            else
            {
                $passed_mapping_ids[] = $mapperValue;
            }
        }
        
        if($throwException === true && count($error_messages) > 0)
        {
            $imploded = implode('</br>', $error_messages);
            
            throw new Exception($imploded);
        }
        
        return $error_messages;
    }
    
    /**
     * na osnovu definicije generise se vrednost za postavljanje
     */
    private function parseDefaultValueToSet(array $paramDefinition)
    {
        $default_value = null;
        if(isset($paramDefinition['default']))
        {
            if($paramDefinition['type'] === SIMAConfigManager::$TYPE_SEARCH_FIELD)
            {
                $modelName = $paramDefinition['type_params']['modelName'];
                $model = null;
                if(isset($paramDefinition['default'][0])) /// ovo verovatno znaci da je niz id-eva
                {
                    $model = $modelName::model()->find([
                        'model_filter' => [
                            'ids' => $paramDefinition['default']
                        ]
                    ]);
                }
                else /// verovatno znaci da je niz key=>value parova gde je key naziv atributa, a value vrednost
                {
                    $model = $modelName::model()->findByAttributes($paramDefinition['default']);
                    if(empty($model))
                    {
                        $model = new $modelName;
                        foreach($paramDefinition['default'] as $key => $value)
                        {
                            $model->$key = $value;
                        }
                        $model->save();
                    }
                }
                if(empty($model))
                {
                    throw new Exception('default model empty');
                }
                
                $default_value = $model->id;
            }
            else
            {
                $default_value = $paramDefinition['default'];
            }
        }
        if(isset($paramDefinition['value_is_array']) && $paramDefinition['value_is_array'] === true && isset($default_value))
        {
            $default_value = array($default_value);
        }
        
        return $default_value;
    }
}
