<?php

/**
 * klasa se izvrsava samo ako je postavljena konfiguracija
 * sto bi trebalo da je samo u testovima
 */
class SIMACronTester extends SIMACron
{
    public function run()
    {
        if(Yii::app()->configManager->get('base.develop.perform_work_for_cron_job_tester') !== true)
        {
            return;
        }
        
        $sql = "INSERT INTO admin.eventrelay_server_logs (log_data, log_type) values ('SIMACronForTestToSuccessExtend', 'INFO');";
        Yii::app()->db->createCommand($sql)->execute();
    }
}

