<?php

class SIMAMainMenu
{
    private $main_menu_items = [];
    
    public function init()
    {
    }
    
    public function add($main_menu_items)
    {
        $this->main_menu_items = array_merge_recursive($this->main_menu_items, $main_menu_items);
    }
    
    public function get($is_reverse = false)
    {
        SIMAModule::registerModules_MainMenuItems(Yii::app());
        
        if ($is_reverse)
        {
            return array_reverse(SIMAArrayHelper::multisortByKey($this->main_menu_items, 'order'));
        }
        
        return SIMAArrayHelper::multisortByKey($this->main_menu_items, 'order');
    }
    
    public function moveItem($item_model_name, $item_model_id, $prev_item_id = null, $next_item_id = null)
    {
        if (empty($prev_item_id) && empty($next_item_id))
        {
            return;
        }
        
        $item = $item_model_name::model()->findByPkWithCheck($item_model_id);

        $item_next = $item->next;
        if (!empty($item_next))
        {
            $item_next->prev_id = !empty($item->prev) ? $item->prev->id: null;
            $item_next->save();
        }
        $item->refresh();

        if (!empty($next_item_id))
        {
            $next_item = $item_model_name::model()->findByPkWithCheck($next_item_id);
            if (intval($next_item->prev_id) !== intval($item->id))
            {
                $item->prev_id = $next_item->prev_id;
                $item->save();

                $next_item->prev_id = $item->id;
                $next_item->save();
            }
        }
        //$prev_item_id se salje samo ako je poslednji i nema next, inace se uvek salje next
        else if (!empty($prev_item_id))
        {
            $item->prev_id = $prev_item_id;
            $item->save();
        }
    }
    
    public function checkItemsIntegrityForUser(User $user)
    {
        if (!$this->checkGroupsIntegrityForUser($user))
        {
            throw new SIMAWarnException(Yii::t('BaseModule.MainMenuFavoriteItem', 'IntegrityError'));
        }
    }
    
    public function checkGroupsIntegrityForUser(User $user)
    {
        $main_menu_groups_criteria = new SIMADbCriteria([
            'Model' => MainMenuFavoriteGroup::class,
            'model_filter' => [
                'user' => [
                    'ids' => $user->id
                ]
            ]
        ], true);
        
        $main_menu_favorite_group_ids = !empty($main_menu_groups_criteria->ids) ? array_map('intval', explode(',', $main_menu_groups_criteria->ids)) : [];
        $sorted_main_menu_favorite_group_ids = MainMenuFavoriteGroup::getSortedMainMenuFavoriteGroups($user, true);
        
        sort($main_menu_favorite_group_ids);
        sort($sorted_main_menu_favorite_group_ids);
        
        if ($main_menu_favorite_group_ids !== $sorted_main_menu_favorite_group_ids)
        {
            return false;
        }
        
        $main_menu_groups = MainMenuFavoriteGroup::model()->findAll($main_menu_groups_criteria);
        
        foreach ($main_menu_groups as $main_menu_group)
        {
            $main_menu_favorite_group_to_item_ids = $main_menu_group->mainmenuFavoriteGroupToItemsIds();
            $sorted_main_menu_favorite_group_to_item_ids = $main_menu_group->getSortedMainMenuFavoriteGroupToItems(true);

            sort($main_menu_favorite_group_to_item_ids);
            sort($sorted_main_menu_favorite_group_to_item_ids);
            
            if ($main_menu_favorite_group_to_item_ids !== $sorted_main_menu_favorite_group_to_item_ids)
            {
                return false;
            }
        }

        return true;
    }
    
    public function sendUpdateEventForUser(User $user, $message_type = 'MAIN_MENU_ITEMS_CHANGED')
    {
        WebSocketClient::sendBulk([
            [
                'receiver_id' => $user->id,
                'receiver_type' => 'USER',
                'message_type' => $message_type,
                'data' => []
            ]
        ]);
    }
}