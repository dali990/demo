<?php

class SIMAFormParams
{
    static public $STATUS_INITIAL = 'initial';
    
    static public $BEFORE_TYPE_RENDER = 'BEFORE_TYPE_RENDER';
    static public $BEFORE_TYPE_SAVE = 'BEFORE_TYPE_SAVE';
    
    static public $AFTER_TYPE_FORM_CLOSE = 'AFTER_TYPE_FORM_CLOSE';
    
    static public $RECORD_TYPE_ON_NEW = 'RECORD_TYPE_ON_NEW';
    static public $RECORD_TYPE_ON_EDIT = 'RECORD_TYPE_ON_EDIT';
    
    static public $ACTION_TYPE_STATIC = 'ACTION_TYPE_STATIC';
    static public $ACTION_TYPE_MODEL = 'ACTION_TYPE_MODEL';
}

