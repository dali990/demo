<?php

class BaseMaintain
{
    static public function pushOldModelUpdateEvents()
    {
        $error_messages = [];
        
        $events = ModelUpdateEvent2::model()->oldExpired()->unsent()->findAll();
                
        $events_count = count($events);
        if($events_count === 0)
        {
            return $error_messages;
        }
        
        $error_messages[] = 'broj neposlatih model update-va: '.$events_count;
        
        $original_phpsessionid = Yii::app()->getModelUpdatePhpSessionId();
        $unsent_php_session_ids = [];
        foreach($events as $event)
        {
            $event_php_session_id = $event->php_session_id;
            
            if(empty($event_php_session_id))
            {
                $error_messages[] = 'event_php_session_id empty: '.SIMAMisc::toTypeAndJsonString($event);
                
//                $event_php_session_id = Yii::app()->getModelUpdatePhpSessionId().'_oldexpired_unsent';
//                $event->php_session_id = $event_php_session_id;
//                $event->save();
                continue;
            }
            
            if(in_array($event_php_session_id, $unsent_php_session_ids))
            {
                continue;
            }
            
            $unsent_php_session_ids[] = $event_php_session_id;
            
            MarkModelUpdateEventsWaitingCommand::work($event_php_session_id);
        }
        $error_messages[] = 'id-evi neposlatih model update-va: '.json_encode($unsent_php_session_ids);
        
        /// restore phpsessionid
        Yii::app()->db->createCommand("SET sima.models_update_event_php_session_id='$original_phpsessionid';")->execute();
        
        return $error_messages;
    }
    
    /**
     * maintain f-ja
     * svim modelupdatevent-ovima koji imaju php_session_id=null postavlja php_session_id na vrednost iz ove komande
     * time ce zavrsetkom komande svi njeni (ovi) modelupdateevent-ovi biti poslati
     * 
     * @return array
     */
    static public function pushModelUpdateEventsWithPhpSessionIdNull()
    {
        $modelupdateevents_phpsessionid_null = ModelUpdateEvent2::model()->php_session_id_empty()->findAll([
            'limit' => 100
        ]);
        
        $modelupdateevents_phpsessionid_null_count = count($modelupdateevents_phpsessionid_null);
        
        if($modelupdateevents_phpsessionid_null_count === 0)
        {
            return [];
        }
        
        $error_messages = [
            'broj model update-va sa php_session_id null: '.$modelupdateevents_phpsessionid_null_count
        ];
        
        foreach($modelupdateevents_phpsessionid_null as $modelupdateevent_phpsessionid_null)
        {            
            $error_messages[] = '$modelupdateevent: '.SIMAMisc::toTypeAndJsonString($modelupdateevent_phpsessionid_null);
            $modelupdateevent_phpsessionid_null->php_session_id = Yii::app()->getModelUpdatePhpSessionId();
            $modelupdateevent_phpsessionid_null->save();
        }
//        ModelUpdateEvent2::model()->php_session_id_empty()->findAllByParts(function($modelupdateevents_phpsessionid_null)  use (&$messages){
//            
//        });
        
        return $error_messages;
    }
    
    public static function checkExpiredPersonalLicencedSessions()
    {
        $personal_licenced_usernames = Yii::app()->licenceManager->personal_licences;
        foreach($personal_licenced_usernames as $username)
        {
            $user = User::model()->findByAttributes([
                'username' => $username
            ]);
            if(empty($user))
            {
                error_log(__METHOD__.' - empty $user: '.SIMAMisc::toTypeAndJsonString($user).' - $username: '.$username);
                continue;
            }
            
            $shared_session = Session::model()
                ->online()
                ->sharedLicence()
                ->orderByStartTimeASC()
                ->find([
                    'model_filter' => [
                        'user' => [
                            'ids' => $user->id
                        ]
                    ]
                ]);
            if(empty($shared_session))
            {
                continue;
            }
            
            Yii::app()->licenceManager->checkLicencePersonal($shared_session, true);
        }
    }
    
    /**
     * sve sto je starije od 7 dana ili vise od 10k
     * brise models_update_events2_completed_phpsessionids i base.models_update_events2
     */
    public static function modelUpdateEventsCleanup()
    {
        $error_messages = [];
        
        try
        {
            $old_limit_interval = '7d';
            Yii::app()->db->createCommand()->delete('base.models_update_events2', "create_time<(now()-INTERVAL '".$old_limit_interval."')");
            Yii::app()->db->createCommand()->delete('base.models_update_events2_completed_phpsessionids', "create_time<(now()-INTERVAL '".$old_limit_interval."')");

            $size_limit = 10000;
            $mue2_count = ModelUpdateEvent2::model()->count();
            if($mue2_count > $size_limit)
            {
                $count_diff = $mue2_count-$size_limit;
                Yii::app()->db->createCommand()->delete(
                    'base.models_update_events2', 
                    "id in (SELECT id FROM base.models_update_events2 ORDER BY create_time LIMIT $count_diff)"
                );
            }
            $mue2cpsi_count = ModelUpdateEvent2CompletedPhpsessionid::model()->count();
            if($mue2cpsi_count > $size_limit)
            {
                $count_diff = $mue2cpsi_count-$size_limit;
                Yii::app()->db->createCommand()->delete(
                    'base.models_update_events2_completed_phpsessionids', 
                    "php_session_id in (SELECT php_session_id FROM base.models_update_events2_completed_phpsessionids ORDER BY create_time LIMIT $count_diff)"
                );
            }
        }
        catch(Exception $e)
        {
            $error_messages[] = $e->getMessage();
        }
        
        return $error_messages;
    }
}

