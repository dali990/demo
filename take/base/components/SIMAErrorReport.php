<?php

class SIMAErrorReport extends CComponent
{
    public $baseURL = null;
    
    public function init(){}
    
    public function addError($params=[])
    {
        $error_report = null;

        try
        {
            $error_report = new ErrorReport();
            $error_report->attributes = $params;
            $error_report->save();
            $error_report->refresh();
            
            $this->sendError($error_report);
        } 
        catch (Exception $ex) 
        {
            error_log(__METHOD__. " - nije uspelo dodavanje greske: ".CJSON::encode($params).'. Poruka: '.$ex->getMessage());
        }        

        return $error_report;
    }
    
    public function sendError(ErrorReport $error_report)
    {
        $return = false;
        try
        {
            $error_params = $error_report->getAttributes();
            $this->addAdditionalDataForSend($error_params, $error_report->submitter);

            if (!empty($this->baseURL))
            {
                $response_string = Yii::app()->curl->post($this->baseURL.'?r=setpp/bafs/baf/addError', ['error_params'=>CJSON::encode($error_params)],[],true);
                $response = (!empty($response_string)) ? CJSON::decode($response_string) : [];
                if (!empty($response['server_error_report_id']))
                {
                    $error_report->server_error_report_id = $response['server_error_report_id'];
                    $error_report->save();
                    $return = $response;
                }
            }
        } 
        catch (Exception $ex) 
        {
            error_log(__METHOD__. " - nije uspelo slanje greske sa ID: ".$error_report->id.'. Poruka: '.$ex->getMessage());
        }
        
        return $return;
    }
    
    public function sendClientBafRequest(ClientBafRequest $client_baf_request, $additional_params = [])
    {
        $return = false;
        try
        {
            $client_baf_request_params = array_merge($client_baf_request->getAttributes(), $additional_params);
            $this->addAdditionalDataForSend($client_baf_request_params, $client_baf_request->requested_by);
            if (!empty($this->baseURL))
            {
                $response_string = '';
                if(Yii::app()->params['is_production'])
                {
                    $response_string = Yii::app()->curl->post($this->baseURL.'?r=setpp/bafs/baf/addBafRequest', [
                        'baf_request_params'=>CJSON::encode($client_baf_request_params)
                    ],[],true);
                }
                $response = (!empty($response_string)) ? CJSON::decode($response_string) : [];
                if (!empty($response['server_baf_request_id']))
                {
                    //SASA A. - Morao sam opet da dovucem model, jer npr ako se prosledi $this iz modela ClientBafRequest u afterSave(sto i jeste slucaj) onda ne mogu 
                    //da ga updejtujem jer kaze da postoji model sa ovim ID. Vrv $this ima u sebi negde nesta sto mu smeta i zbog cega ne moze da se reSave.
                    $_client_baf_request = ClientBafRequest::model()->findByPk($client_baf_request->id);
                    $_client_baf_request->server_baf_request_id = $response['server_baf_request_id'];
                    $_client_baf_request->send_time = "now()";
                    $_client_baf_request->save();
                    $return = $response;
                }
            }
        } 
        catch (Exception $ex) 
        {
            error_log(__METHOD__. " - nije uspelo slanje zahteva sa ID: ".$client_baf_request->id.'. Poruka: '.$ex->getMessage());
        }
        
        return $return;
    }
    
    /**
     * Salje automatski zahtev za unapredjenje sa zadatim tekstom
     * @param type $text
     */
    public function createAutoClientBafRequest($text, $theme_id=null, $theme_name = '')
    {
        $requested_by_id = null;
        if(Yii::app()->isWebApplication() && !is_null(Yii::app()->controller) && !Yii::app()->controller->isApiAction())
        {
            $requested_by_id = Yii::app()->user->id;
        }
        else
        {
            $requested_by_id = Yii::app()->company->id;
        }
        
        if (is_null($theme_id))
        {
            $req_name = 'Automatski zahtev za dodavanjem greske';
        }
        else
        {
            $req_name = "Automatski zahtev za dodavanjem greske TEMA: '$theme_name' ($theme_id)";
        }
        
        $client_baf_request = new ClientBafRequest();
        $client_baf_request->name = $req_name;
//        $client_baf_request->requested_by_id = Yii::app()->user->id;
        $client_baf_request->requested_by_id = $requested_by_id;
        $client_baf_request->priority = ClientBafRequest::$HIGH;
        $client_baf_request->description = $text;
        $client_baf_request->send = false; //u save-u se ne poziva sendClientBafRequest, vec mi to eksplicitno radimo nakon save-a da bi smo prosledili status
        $client_baf_request->save();
        Yii::app()->errorReport->sendClientBafRequest($client_baf_request, ['status' => 'AUTO_REQUEST']);
    }
    
    private function addAdditionalDataForSend(&$params, $submitter)
    {
        //mora se poslati $params['submitter_domain'] jer je kod BAFr-a ta kolona izbrisana, ali je ostala u greskama
        $params['submitter_domain'] = Yii::app()->params['server_FQDN'];
        $params['submitter_additional_info'] = [
            'SUBMITTER_NAME'=>!empty($submitter)?$submitter->DisplayName:'',
            'SUBMITTER_ID'=>!empty($submitter)?$submitter->id:'',
            'SUBMITTER_IP'=>SIMAMisc::getClientIpAddress(),
            'SUBMITTER_DOMAIN' => Yii::app()->params['server_FQDN'],
            'IP'=>filter_input(INPUT_SERVER, 'SERVER_ADDR')
        ];
        
        if(Yii::app()->simaDemo->IsDemoISSIMA())
        {
            $session_key = 'NO SESSION KEY!!!';
            if(isset($_SESSION["db_session_key"]))
            {
                $session_key = $_SESSION["db_session_key"];
            }
            $params['submitter_additional_info']['sessionkey'] = $session_key;
        }
    }
    
    public function getErrorMessageDisplay($error_message)
    {
        try 
        {
            $decoded_message = $error_message;
            if (is_string($error_message))
            {
                $decoded_message = CJSON::decode($error_message);
            }
        } 
        catch (Exception $ex) 
        {
            $decoded_message = null;
        }
        
        $display_message = $error_message;
        if (is_array($decoded_message))
        {
            $display_message = '';
            if (!empty($decoded_message['exception_class']))
            {
                $display_message .= '<h1>'.$decoded_message['exception_class']."</h1>\n";
            }
            
            $display_message .= '<p>';
            if (!empty($decoded_message['exception_message']))
            {
                $display_message .= $decoded_message['exception_message'];
            }
            if (!empty($decoded_message['exception_file']) && !empty($decoded_message['exception_line']))
            {
                $display_message .= ' ('.$decoded_message['exception_file'].':'.$decoded_message['exception_line'].')';
            }
            $display_message .= '</p>';

            if (!empty($decoded_message['exception_trace']))
            {
                $display_message .= '<pre>'.$decoded_message['exception_trace'].'</pre>';
            }
        }
        
        return $display_message;
    }

    public function getClientErrorMessageDisplay($error_message)
    {
        try 
        {
            $decoded_message = CJSON::decode($error_message);
        } 
        catch (Exception $ex) 
        {
            $decoded_message = null;
        }
        
        $client_display_message = $error_message;
        if (is_array($decoded_message) && !empty($decoded_message['exception_message']))
        {
            $client_display_message = $decoded_message['exception_message'];
        }
        
        return $client_display_message;
    }
}

