<?php

/**
 * http://www.yiiframework.com/doc/api/1.1/CDbCommand
 */
class SIMASQLFunctions
{
    public static $PG_ERROR_CODE_INVALID_BANK_ACCOUNT_NUMBER = 'S0000';
    public static $PG_ERROR_CODE_FOREIGN_KEY_INVALID_TYPE_VALUE = 'S0001'; /// kada vrednost stranog kljuca ne zadovoljava uslov tipa
    public static $PG_ERROR_CODE_COMPLEX_QUERY = 'S0002'; /// kada je tql query predugacak/prekompleksan da bi se obradjivao
    public static $PG_ERROR_CODE_DD_CHILD_DOES_NOT_EXISTS = 'S0003';
    public static $PG_ERROR_CODE_WORK_DAY_TYPE_CONF_NOT_SET = 'S0004'; /// nije postavljen konfiguracioni parametar za tip dana
    public static $PG_ERROR_CODE_WEEKEND_DAY_TYPE_CONF_NOT_SET = 'S0005'; /// nije postavljen konfiguracioni parametar za tip dana
    public static $PG_ERROR_CODE_STACK_DEPTH_LIMIT_EXCEEDED = '54001'; /// previse rekurzivnih poziva, stack preopterecen
    
    /**
     * 
     * @param integer $file_id fajl kojem treba da se doda shell verzija
     * @return integer Vraca ID napravljene shell verzije
     */
    public static function CreateShellFileVersion($file_id=null, $prev_fv_id=null)
    {
        $result = SIMASQLFunctions::CreateNewFileVersion('generic_name', 0, md5(''), 'shell', $file_id, $prev_fv_id);
        return $result['version_id'];
    }

    /**
     * 
     * @param type $file_id - ID of file for which is new version is created
     * @param type $file_version_name - name od new file version
     * @param type $file_size - size od new file version
     * @param type $md5_sum - md5 sum od new file version
     * @param type $type - type of new version: 'regular', 'shell', 'deleted', 'version_deleted', 
     *                                          'permanently_deleted', 'approved_permanently_deleted'
     * @return boolean if function succeded
     */
    public static function CreateNewFileVersion($file_version_name, $file_size, $md5_sum, $type, $file_id=null, $prev_fv_id=null, $session_id=null)
    {        
        $sessionid = self::GetSessionId($session_id);

        $sql = "select * from web_sima.create_new_file_version(:file_id, :file_name, :file_size , :md5_sum, :type, :session_id);";
        if(!is_null($prev_fv_id))
        {
            $sql = "select * from web_sima.create_new_file_version(:file_id, :file_name, :file_size , :md5_sum, :type, :session_id, :previous_version_id);";
        }
        
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(':file_id', $file_id);
        $command->bindParam(':file_name', $file_version_name);
        $command->bindParam(':file_size', $file_size);
        $command->bindParam(':md5_sum', $md5_sum);
        $command->bindParam(':type', $type);
        $command->bindParam(':session_id', $sessionid);
        if(!is_null($prev_fv_id))
        {
            $command->bindParam(':previous_version_id', $prev_fv_id);
        }
        
        $data = $command->queryAll();
        if ($data[0]['create_new_file_version'] != 'OK')
        {
            throw new Exception('Greska u kreiranju nove verzije fajla');
        }
        else
        {
            $params = array(
                'filename'=>$data[1]['create_new_file_version'],
                'version_id'=>$data[2]['create_new_file_version']
            );
        }
        
        if (!is_null($file_id))
        {
            $fileModel = File::model()->resetScope()->findByPk($file_id);
            $fileModel->notifyAboutChangedVersion();
        }
                
        return $params;
    }

    public static function DeleteFile($file_id, $file_version_id)
    {
        $session_id = Yii::app()->user->db_session->id;
        
        $sql = "select * from web_sima.delete_file(:file_id, :file_version_id, :sessionid);";
        
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(':file_id', $file_id);
        $command->bindParam(':file_version_id', $file_version_id);
        $command->bindParam(':sessionid', $session_id);
        
        $data = $command->queryAll();

        $result = $data[0]['delete_file'];
        $tables = explode(',', $result);
        foreach ($tables as $table)
        {
            switch ($table)
            {
                case 'OK': return;
                case 'DEPEND': 
                    $errors[] = 'Fajl ima dodatne podatke (zavodni broj/racun/ugovor...) ';
                    break;
                case 'private.filing_book': 
                    $errors[] = 'Fajl ne moze biti obrisan jer je zaveden ';
                    break;
                case 'private.bills': 
                    $errors[] = 'Fajl ne moze biti obrisan jer se nalazi u tabeli racuna ';
                    break;
                case 'files.locked_files': 
                    $errors[] = 'Fajl ne moze biti obrisan jer je zakljucan ';
                    break;
                default:
                    $errors[] = "Fajl ima dodatne podatke (zavodni broj/racun/ugovor...)($table) ";
                    break;
            }
        }
        
        
        $error_msg = 'Fajl ne moze biti prebacen u kantu iz sledecih razloga<ul><li>'
                . implode('</li><li>', $errors)
                . '</li></ul>';

        throw new SIMAWarnException($error_msg);
    }
    
    public static function deleteFileForever($file_id)
    {
        $session_id = Yii::app()->user->db_session->id;
        
        $sql = "select * from web_sima.delete_file_permanent(:file_id, :session_id);";
        
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(':file_id', $file_id);
        $command->bindParam(':session_id', $session_id);
        
        $data = $command->queryAll();
                
        $result = $data[0]['delete_file_permanent'];
                
        if($result !== 'OK')
        {
            if($result == 'DEPEND')
            {
                $errors[] = 'Fajl ima dodatne podatke (zavodni broj/racun/ugovor...) ';
            }
            else
            {
                $errors[] = 'Doslo je do greske';
            }

            $error_msg = 'Fajl ne moze biti trajno obrisan iz sledecih razloga<ul><li>'
                . implode('</li><li>', $errors)
                . '</li></ul>';

            throw new SIMAWarnException($error_msg);
        }
    }
    
//    public static function restoreFileFromTrash($file_id)
//    {
//        $session_id = Yii::app()->user->db_session->id;
//        
//        $sql = "select * from web_sima.restore_file_from_trash(:file_id, :session_id);";
//        
//        $command = Yii::app()->db->createCommand($sql);
//        $command->bindParam(':file_id', $file_id);
//        $command->bindParam(':session_id', $session_id);
//        
//        $data = $command->queryAll();
//                        
//        if($data[0]['restore_file_from_trash'] !== 'OK')
//        {
//            throw new SIMAWarnException('Doslo je do greske pri vracanju fajla iz kante!');
//        }
//    }
    
    public static function checkFilesInTrash()
    {
        $sql = 'SELECT * FROM web_sima.check_files_in_trash()';
        $data = Yii::app()->db->createCommand($sql)->queryAll();
        
        return $data[0]['check_files_in_trash'];
    }
    
    public static function getFileDependTables($fileid)
    {
        $sql = "select * from web_sima.get_depend_tables_for_fileid(:file_id);";
        
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(':file_id', $fileid);
        
        $data = $command->queryAll();
                
        return $data[0]['get_depend_tables_for_fileid'];
    }
    
    public static function getDirContent($query, $show_files_from_parents, $short_path = 'false', $exclude_empty = 'false', $getfilesfolder_flag = 'false')
    {
//        $sessionid = Yii::app()->user->db_session->id;
        $sessionid = self::GetSessionId();
        
//        $sql = "select * from web_sima.get_dir_content(:query, :sessionid, :show_files_from_parents, :short_path, :exclude_empty, :getfilesfolder_flag);";
        $sql = "select * from web_sima.get_dir_content2(:query, :sessionid, :show_files_from_parents, :short_path, :exclude_empty, :getfilesfolder_flag);";
        
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(':query', $query);
        $command->bindParam(':sessionid', $sessionid);
        $command->bindParam(':show_files_from_parents', $show_files_from_parents);
        $command->bindParam(':short_path', $short_path);
        $command->bindParam(':exclude_empty', $exclude_empty);
        $command->bindParam(':getfilesfolder_flag', $getfilesfolder_flag);
                
        try
        {
            $data = $command->queryAll();
        }
        catch (Exception $exc) 
        {
            if(isset($exc->errorInfo) 
                && gettype($exc->errorInfo) === 'array' 
                && count($exc->errorInfo) === 3)
            {
                if($exc->errorInfo[0] === SIMASQLFunctions::$PG_ERROR_CODE_STACK_DEPTH_LIMIT_EXCEEDED
                    ||
                    $exc->errorInfo[0] === SIMASQLFunctions::$PG_ERROR_CODE_COMPLEX_QUERY)
                {
                    throw new SIMAWarnExceptionQueryComplexForGetDirContent();
                }
                else if($exc->errorInfo[0] === SIMASQLFunctions::$PG_ERROR_CODE_DD_CHILD_DOES_NOT_EXISTS)
                {
                    throw new SIMAWarnInvalidDirectoryDefinition();
                }
            }
            
            throw $exc;
        }
                
        return $data;
    }
    
    public static function GetFilesQueryForDirQuery($dir_query)
    {
//        $sessionid = Yii::app()->user->db_session->id;
        $sessionid = self::GetSessionId();
        
        $function_to_use = 'get_files_query_for_dir_query';
        if(Yii::app()->configManager->get('base.develop.use_get_files_query_for_dir_query2') === true)
        {
            $function_to_use = 'get_files_query_for_dir_query2';
        }
        $sql = "select * from web_sima.$function_to_use(:dir_query, :sessionid);";
        
        $command = Yii::app()->db->createCommand($sql);
        
        $command->bindParam(':dir_query', $dir_query);
        $command->bindParam(':sessionid', $sessionid);
        
        try
        {
            $data = $command->queryAll();
        }
        catch (Exception $exc) 
        {
            if(isset($exc->errorInfo) 
                && gettype($exc->errorInfo) === 'array' 
                && count($exc->errorInfo) === 3
            )
            {
                if($exc->errorInfo[0] === SIMASQLFunctions::$PG_ERROR_CODE_STACK_DEPTH_LIMIT_EXCEEDED
                        ||
                        $exc->errorInfo[0] === SIMASQLFunctions::$PG_ERROR_CODE_COMPLEX_QUERY)
                {
                    throw new SIMAWarnExceptionQueryComplexForGetFiles();
                }
                else if($exc->errorInfo[0] === SIMASQLFunctions::$PG_ERROR_CODE_DD_CHILD_DOES_NOT_EXISTS)
                {
                    throw new SIMAWarnInvalidDirectoryDefinition();
                }
            }
            
            throw $exc;
        }
        
        return $data[0][$function_to_use];
    }
    
    public static function canAddFixDirForQuery($query)
    {
        $sql = "select * from web_sima.canaddfixdir_forquery(:query);";
        
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(':query', $query);
        
        $data = $command->queryAll();
                
        return $data[0]['canaddfixdir_forquery'];
    }
    
    public static function canAddFileForQuery($query)
    {
        $sql = "select * from web_sima.canaddfile_forquery(:query);";
        
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(':query', $query);
        
        $data = $command->queryAll();
                
        return $data[0]['canaddfile_forquery'];
    }
    
    public static function createDir($parent_query, $new_name, $dir_type)
    {
        $session_id = Yii::app()->user->db_session->id;
        
        $sql = "select * from web_sima.create_dir(:parent_query, :new_name, :session_id, :dir_type);";
        
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(':parent_query', $parent_query);
        $command->bindParam(':new_name', $new_name);
        $command->bindParam(':session_id', $session_id);
        $command->bindParam(':dir_type', $dir_type);
        
        $data = $command->queryAll();
                
        return $data[0]['create_dir'];
    }
    
    public static function renameDir($parent_query, $dir_query, $new_name)
    {
        $session_id = Yii::app()->user->db_session->id;
        
        $sql = "select * from web_sima.rename_dir(:parent_query, :dir_query, :new_name, :session_id);";
        
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(':parent_query', $parent_query);
        $command->bindParam(':dir_query', $dir_query);
        $command->bindParam(':new_name', $new_name);
        $command->bindParam(':session_id', $session_id);
        
        $data = $command->queryAll();
                
        return $data;
    }
    
    public static function removeDir($dir_query)
    {
        $session_id = Yii::app()->user->db_session->id;
        
        $sql = "select * from web_sima.remove_dir(:dir_query, :session_id);";
        
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(':dir_query', $dir_query);
        $command->bindParam(':session_id', $session_id);
        
        $data = $command->queryAll();
                
        return $data;
    }
    
    public static function moveFile($current_query_id, $target_query_id, $new_files, $move_type)
    {
        $session_id = Yii::app()->user->db_session->id;
        
        $sql = "select * from web_sima.move_file(:current_query_id, :target_query_id, :new_files, :move_type, :session_id);";
        
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(':current_query_id', $current_query_id);
        $command->bindParam(':target_query_id', $target_query_id);
        $command->bindParam(':new_files', $new_files);
        $command->bindParam(':move_type', $move_type);
        $command->bindParam(':session_id', $session_id);
        
        $data = $command->queryAll();
                
        return $data;
    }
    
    /**
     * 
     * @param type $source_dir_query
     * @param type $destination_dir_query
     * @param type $dir_query
     * @return type
     */
    public static function moveDir($source_dir_query, $destination_dir_query, $dir_query)
    {
        $session_id = Yii::app()->user->db_session->id;
        
        $sql = "select * from web_sima.move_dir(:source_dir_query, :destination_dir_query, :dir_query, :session_id);";
        
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(':source_dir_query', $source_dir_query);
        $command->bindParam(':destination_dir_query', $destination_dir_query);
        $command->bindParam(':dir_query', $dir_query);
        $command->bindParam(':session_id', $session_id);
        
        $data = $command->queryAll();
                
        return $data;
    }

    public static function tasksReportsView($task_ids, $start_date, $end_date, $user_id)
    {
        $sql = "select * from web_sima.tasks_reports_view(:task_ids, :start_date, :end_date, :user_id);";
        
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(':task_ids', $task_ids);
        $command->bindParam(':start_date', $start_date);
        $command->bindParam(':end_date', $end_date);
        $command->bindParam(':user_id', $user_id);
        
        $data = $command->queryAll();
                        
        return $data;
    }
    
    public static function employeeWorkHoursLeft($emp_id, $firstreportdate, $lastreportdate)
    {
        $first_report_date = SIMAHtml::UserToDbDate($firstreportdate);
        $last_report_date = SIMAHtml::UserToDbDate($lastreportdate);
        
        $sql = "select (SELECT (("
                    . "select count(*) "
                    . "from days "
                    . "where day_type_id=2 and day_date>=:first_report_date and day_date<now()::date"
                . ") * 8)) as required_work_hours";
        
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(':first_report_date', $first_report_date);
        
        $data = $command->queryAll();
        
        $needed_work_hours = $data[0]['required_work_hours'];
        $done_work_hours = SIMASQLFunctions::employeeWorkHours($emp_id, $first_report_date, $last_report_date);
        
        $needed_work_hours -= $done_work_hours;
                        
        return $needed_work_hours;
    }
    
    public static function employeeWorkHours($emp_id, $begindate, $enddate)
    {
        $begin_date = SIMAHtml::UserToDbDate($begindate, true);
        $end_date = SIMAHtml::UserToDbDate($enddate, true);
        $absences_table_name = Absence::model()->tableName();
        $sql = "select ("
                        ." (SELECT coalesce(sum(hours-reduction_hours), 0)::numeric(20,1) as work_hours" 
                            ." FROM task_reports t " 
                            ." WHERE t.day_id in ( select id from days "
                                                    . "where day_date between :begin_date "
                                                        . "and :end_date ) AND (t.user_id=:emp_id))"
                        ." + " 
                        ."(SELECT coalesce(("
                            ." select sum(t.hours)" 
                                ." from trainings t, persons_to_trainings ptt" 
                                ." where ptt.training_id=t.id and ptt.person_id=:emp_id "
                                    . "and t.begin>=:begin_date and t.end<=:end_date"
                            ." ), 0))"
                        ." + "
                        ."(with abs as"
                                ." ("
                                ." select case"
                                        ." when a.begin<:begin_date then :begin_date"
                                        ." ELSE a.begin"
                                    ." END as begin,"
                                    ." case"
                                        ." when a.end>:end_date then :end_date"
                                        ." ELSE a.end"
                                    ." END as end"
                                ." from $absences_table_name a" 
                                ." where employee_id=:emp_id and confirmed=true"
                                    ." and ( (a.begin between :begin_date and :end_date) "
                                            . "or (a.end between :begin_date and :end_date) "
                                            . "or (a.begin<:begin_date and a.end>:end_date))"
                                .")"
                                ." select coalesce(count(*) * 8, 0)"
                                ." from days, abs"
                                ." where day_date between abs.begin and abs.end and day_type_id=2)"
                        ." + " 
                        ."(SELECT coalesce(EXTRACT(hour FROM ("
                                . " select sum((m.end-m.begin)::interval)"
                                . " from private.persons_to_meetings ptm, private.meetings m"
                                . " where ptm.person_id=:emp_id and person_present=true and ptm.meeting_id=m.id"
                                    . " and ( (m.begin between :begin_date and :end_date) "
                                                . "or (m.end between :begin_date and :end_date) "
                                                . "or (m.begin<:begin_date and m.end>:end_date))"
                        . ")), 0))"
                . " ) as work_hours";
        
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(':emp_id', $emp_id);
        $command->bindParam(':begin_date', $begin_date);
        $command->bindParam(':end_date', $end_date);
        
        $data = $command->queryAll();
        
        return $data[0]['work_hours'];
    }
    
    public static function accesslimits($alias, $user_id, $level)
    {
        $sql = "select * from web_sima.accesslimits(:alias, :user_id, :level);";

        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(':alias', $alias);
        $command->bindParam(':user_id', $user_id);
        $command->bindParam(':level', $level);

        $data = $command->queryAll();

        return $data[0]['accesslimits'];
    }
    
    /**
     * f-ja za svaki prijavljen neregularan dan proverava da li je stvarno neregularan
     * 
     * @return niz id-eva
     */
    public static function getWronglyIrregularDayIds()
    {
        $absences_table_name = Absence::model()->tableName();
        $sql = "select array_agg(trid.id) as wrongly_irregylar_day_ids
                from task_reports_irregular_days trid, admin.users u, days d
                where trid.user_id=u.id and trid.day_id=d.id and
                (
                select (
                        (SELECT coalesce(sum(hours), 0) as work_hours
                            FROM task_reports t 
                            WHERE 
                                t.day_id=trid.day_id
                                AND (t.user_id=trid.user_id))
                         +  
                        (SELECT coalesce(EXTRACT(hour FROM (
                             select sum((t.end-t.begin)::interval)
                                 from trainings t, persons_to_trainings ptt 
                                 where ptt.training_id=t.id and ptt.person_id=trid.user_id and t.begin>=d.day_date and t.end<=d.day_date
                             )), 0))
                         + 
                        (with abs as
                                 (
                                 select case
                                         when a.begin<d.day_date then d.day_date
                                         ELSE a.begin
                                     END as begin,
                                     case
                                         when a.end>d.day_date then d.day_date
                                         ELSE a.end
                                     END as end
                                 from $absences_table_name a 
                                 where employee_id=trid.user_id and confirmed=true
                                     and ( (a.begin = d.day_date) 
                                                or (a.end=d.day_date) 
                                                or (a.begin<d.day_date and a.end>d.day_date))
                                )
                                 select coalesce(count(*) * 8, 0)
                                 from days, abs
                                 where day_date between abs.begin and abs.end and day_type_id=2)
                         +  
                        (SELECT coalesce(EXTRACT(hour FROM (
                                 select sum((m.end-m.begin)::interval)
                                 from private.persons_to_meetings ptm, private.meetings m
                                 where ptm.person_id=trid.user_id and person_present=true and ptm.meeting_id=m.id
                                     and ( (m.begin between d.day_date and d.day_date) 
                                                or (m.end between d.day_date and d.day_date) 
                                                or (m.begin<d.day_date and m.end>d.day_date))
                        )), 0))
                 ) as work_hours
                ) = 8";
        
        $data = Yii::app()->db->createCommand($sql)->queryAll();
                
        return $data[0]['wrongly_irregylar_day_ids'];
    }
    
    public static function addFileVersionToRepository($fileversionid, $repositoryname, $server, $status, $statusinfo)
    {
        $sql = "select * from web_sima.add_fileversion_to_repository(:fileversionid, :repositoryname, :server, :status, :statusinfo);";
        
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(':fileversionid', $fileversionid);
        $command->bindParam(':repositoryname', $repositoryname);
        $command->bindParam(':server', $server);
        $command->bindParam(':status', $status);
        $command->bindParam(':statusinfo', $statusinfo);
        
        $data = $command->queryAll();
        
        return $data;
    }
    
    public static function addFileVersionToLocalRepository($fileversionid, $statusinfo, $for_simaserver = false)
    {
        $repositoryname = 'local';
        $server = Yii::app()->params['server_FQDN'];
        if(isset($for_simaserver) && $for_simaserver === true)
        {
            $server = Yii::app()->params['sima_server_FQDN'];
        }
        $status = 'STAT_READY';
        
        $result = SIMASQLFunctions::addFileVersionToRepository($fileversionid, $repositoryname, $server, $status, $statusinfo);
        
        return $result;
    }
    
    /**
     * vraca sql upite koji predstavljaju promene potrebne za regularno stanje neregularnih dana
     * koji se i izvrsavaju ako je $with_fix=true 
     * u opsegu [$start_date, $end_date] zajedno sa njima
     * ukoliko se ne navede $start_date, pocinje se od minimalne vrednosti kolone admin.users.first_report_day_id
     * ukoliko se ne navede $end_date, kao kraj ce se koristiti danasnji datum
     * ukoliko se ne navede $employee_id, proveravace se za sve zaposlene
     * 
     * @param bool $with_fix
     * @param string $start_date pocetni datum od kog poceti proveru
     * @param string $end_date krajnji datum do kog vrsiti proveru
     * @param int $employee_id id zaposlenog samo za kog vrsiti proveru
     */
    public static function recheckIrregularDays($with_fix = false, $start_date = null, $end_date = null, $employee_id=null)
    {
        $sql = "select * from web_sima.recheck_irregular_days(:with_fix, :start_date, :end_date, :employee_id);";
        
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(':with_fix', $with_fix);
        $command->bindParam(':start_date', $start_date);
        $command->bindParam(':end_date', $end_date);
        $command->bindParam(':employee_id', $employee_id);
        
        $data = $command->queryAll();
        
        return $data;
    }
    
    public static function getTagsToBeAddedToFileFromQuery($query)
    {
        $sql = "select * from web_sima.get_tags_to_be_added_to_file_from_query(:query);";
        
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(':query', $query);
        
        $data = $command->queryAll();
        
        return $data[0]['get_tags_to_be_added_to_file_from_query'];
    }
    
    public static function getStaticFixDirIdFromQuery($query)
    {                
        $sql = "select * from web_sima.get_static_fix_dir_id_from_query(:query);";
        
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(':query', $query);
        
        $data = $command->queryAll();
        
        return $data[0]['get_static_fix_dir_id_from_query'];
    }
    
    public static function getBankAccountIdByNumber($number)
    {        
        try 
        {
            $sql = "select * from web_sima.get_account_id_by_number(:number);";

            $command = Yii::app()->db->createCommand($sql);
            $command->bindParam(':number', $number);

            $data = $command->queryAll();
            
            $get_account_id_by_number = $data[0]['get_account_id_by_number'];
            
            return $get_account_id_by_number;
        }
        catch (Exception $exc) 
        {
            if(isset($exc->errorInfo) 
                && gettype($exc->errorInfo) === 'array' 
                && count($exc->errorInfo) === 3
                && $exc->errorInfo[0] === SIMASQLFunctions::$PG_ERROR_CODE_INVALID_BANK_ACCOUNT_NUMBER)
            {
                throw new SIMAExceptionBankAccountNumberInvalid($number, $exc->errorInfo[2]);
            }
            
            throw $exc;
        }
    }
    
    public static function getPersonSectorDirectorsIds($person, $all_bosses)
    {        
        $result = array();
        $person_id = $person->id;
        
        $sql = 'select * from web_sima.get_person_boss_ids(:person_id, :return_all);';
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(':person_id', $person_id);
        $command->bindParam(':return_all', $all_bosses);
        
        $data = $command->queryAll();
        
        if($data[0]['get_person_boss_ids'] !== '')
        {
            $result = explode(',', $data[0]['get_person_boss_ids']);
        }
        
        return $result;
    }
    
    public static function getEmployeeSubordinateIds(Employee $employee, $all_employees = true)
    {
        $empoyee_id = $employee->id;
        $result = array();
        
        $sql = 'select * from web_sima.get_employee_subordinate_ids(:employee_id, :return_all);';
        
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(':employee_id', $empoyee_id);
        $command->bindParam(':return_all', $all_employees);
        
        $data = $command->queryAll();
        
        if(!empty($data[0]['get_employee_subordinate_ids']) && $data[0]['get_employee_subordinate_ids'] !== '')
        {
            $result = explode(',', $data[0]['get_employee_subordinate_ids']);
        }
        
        return $result;
    }
    
    public static function getFileversionAvailableLocationForHostname(FileVersion $file_version, $hotstname)
    {
        $file_version_id = $file_version->id;
        
        $sql = 'select * from web_sima.get_fileversion_available_location_for_hostname(:file_version_id, :server_hostname)';
                
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(':file_version_id', $file_version_id);
        $command->bindParam(':server_hostname', $hotstname);
        
        $data = $command->queryAll();
        
        return $data[0]['get_fileversion_available_location_for_hostname'];
    }
    
    public static function getDayId($day_date)
    {
        if(empty($day_date))
        {
            throw new SIMAException('day_date is empty');
        }
        
        $sql = 'select * from web_sima.get_day_id(:day_date)';
                
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(':day_date', $day_date);
        
        try
        {
            $data = $command->queryAll();
        }
        catch (Exception $exc) 
        {
            if(isset($exc->errorInfo) 
                && gettype($exc->errorInfo) === 'array' 
                && count($exc->errorInfo) === 3)
            {
                if($exc->errorInfo[0] === SIMASQLFunctions::$PG_ERROR_CODE_WORK_DAY_TYPE_CONF_NOT_SET)
                {
                    throw new SIMAWarnExceptionWorkDayTypeConfNotSet();
                }
                else if($exc->errorInfo[0] === SIMASQLFunctions::$PG_ERROR_CODE_WEEKEND_DAY_TYPE_CONF_NOT_SET)
                {
                    throw new SIMAWarnExceptionWekendDayTypeConfNotSet();
                }
            }
            
            throw $exc;
        }
        
        return $data[0]['get_day_id'];
    }
    
    public static function connect($username, $hostname, $clientipaddress, $sessionType)
    {
        $sql = 'select * from web_sima.connect(:username, :hostname, :clientipaddress, :sessionType)';
                
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(':username', $username);
        $command->bindParam(':hostname', $hostname);
        $command->bindParam(':clientipaddress', $clientipaddress);
        $command->bindParam(':sessionType', $sessionType);
        
        $data = $command->queryAll();
        
        if($data[0]['connect'] !== 'OK')
        {
            throw new Exception($data[1]['connect']);
        }
        
        return $data[1]['connect'];
    }
    
    public static function getCurrentDatabaseName()
    {
        $sql = 'select * from current_database()';
                
        $command = Yii::app()->db->createCommand($sql);
        
        $data = $command->queryAll();
        
        return $data[0]['current_database'];
    }
    
    public static function getConfigParamDbData($dbMappingId, $userId)
    {
        $sql = 'select *
                from base.configuration_parameters
                where mapping_id=:dbMappingId
                order by (case
                                when user_changable=false then 1
                                when user_changable=true and user_id=:userId then 2
                                when user_changable=true and user_id is null then 3
                        end) asc
                limit 1';
                
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(':dbMappingId', $dbMappingId);
        $command->bindParam(':userId', $userId);
        
        $data = $command->queryAll();
                
        $dbData = null;
        if(isset($data[0]))
        {
            $dbData = $data[0];
        }
        
        return $dbData;
    }
    
//    public static function getWorkContractEndDate(WorkContract $wc, Day $end_date_limit=null)
//    {
//        $wc_id = $wc->id;
//        
//        if(isset($end_date_limit))
//        {
//            $end_day_date = SIMAHtml::UserToDbDate($end_date_limit->day_date);
//        }
//        else
//        {
//            $curdate = (new DateTime())->format('d.m.Y');
//            $end_day_date = SIMAHtml::UserToDbDate($curdate);
//        }
//        
//        $sql = 'select base.min_dates(base.min_dates(base.min_dates(wc.end_date, (select end_date 
//                                                            from hr.close_work_contracts cwc 
//                                                            where cwc.work_contract_id=wc.id 
//                                                            limit 1)),
//                                            (select (start_date - INTERVAL \'1 day\')::date from hr.work_contracts wc_next
//                                            where wc_next.employee_id=wc.employee_id 
//                                            and ((wc_next.start_date=wc.start_date AND wc_next.id>wc.id)
//                                                    OR (wc_next.start_date>wc.start_date AND wc_next.id!=wc.id))
//                                            order by wc_next.start_date ASC
//                                            limit 1)), :end_date_limit) as end_date
//                from hr.work_contracts wc
//                where id=:wc_id';
//
//        $command = Yii::app()->db->createCommand($sql);
//        $command->bindParam(':wc_id', $wc_id);
//        $command->bindParam(':end_date_limit', $end_day_date);
//        
//        $data = $command->queryAll();
//                
//        $dbData = null;
//        if(isset($data[0]))
//        {
//            $dbData = $data[0]['end_date'];
//        }
//        
//        return $dbData;
//    }
    
    public static function getWorkContractWorkedDays(WorkContract $wc, $end_date_limit=null)
    {
        $start_date = SIMAHtml::UserToDbDate($wc->start_date);
        $end_date = $wc->getEndDate($end_date_limit);
                        
        if(isset($end_date))
        {
            $sql = 'SELECT count(*) AS count_days_no_weekend
                    FROM generate_series(:start_date::date, :end_date::date, \'1 day\') d(the_day)';
        
            $command = Yii::app()->db->createCommand($sql);
            $command->bindParam(':start_date', $start_date);
            $command->bindParam(':end_date', $end_date);
        }
        else
        {
            $sql = 'SELECT count(*) AS count_days_no_weekend
                    FROM generate_series(:start_date::date, now(), \'1 day\') d(the_day)';
        
            $command = Yii::app()->db->createCommand($sql);
            $command->bindParam(':start_date', $start_date);
        }
        
        $data = $command->queryAll();
        
        $dbData = null;
        if(isset($data[0]))
        {
            $dbData = $data[0]['count_days_no_weekend'];
        }
        
        return $dbData;
    }
    
    public static function addFileToOpen(FileVersion $fileVersion, $session_id)
    {
        $fileversionid = $fileVersion->id;
        
        $sql = 'select * from web_sima.add_file_to_open(:fileversionid, :sessionid)';
                
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(':fileversionid', $fileversionid);
        $command->bindParam(':sessionid', $session_id);
        
        $data = $command->queryAll();
        
        if($data[0]['add_file_to_open'] !== 'OK')
        {
            throw new Exception($data[1]['connect']);
        }
        
        return true;
    }
    
    public static function getCompanySectorChildrenIds(CompanySector $companySector)
    {
        $sql = 'WITH RECURSIVE reccur(id, parent_id) AS (
                    SELECT cs.id,
                       cs.parent_id
                      FROM '.CompanySector::model()->tableName().' cs
                     WHERE cs.parent_id='.$companySector->id.'
                   UNION ALL
                    SELECT cs.id,
                       cs.parent_id
                      FROM '.CompanySector::model()->tableName().' cs,
                       reccur
                     WHERE reccur.id = cs.parent_id
                   )
            SELECT rr.id
              FROM reccur rr
             ORDER BY rr.id;';
                
        $command = Yii::app()->db->createCommand($sql);
        
        $data = $command->queryAll();
        
        $result = [];
        foreach($data as $row)
        {
            $result[] = $row['id'];
        }
        
        return $result;
    }
    
    public static function FileBrowserNameFormat($file_id, $dir_query)
    {
        $sql = 'select * from web_sima.file_browser_name_format(:file_id, :dir_query)';
                
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(':file_id', $file_id);
        $command->bindParam(':dir_query', $dir_query);
        
        $data = $command->queryAll();
        
        return $data[0]['file_browser_name_format'];
    }
    
    /**
     * 
     * @param type $tablename
     * @param type $idvalue
     * @return array - niz ciji su elementi oblika {tabela}:{kolona tabele}
     */
    public static function GetForeignDependencies($tablename, $idvalue)
    {
        $sql = 'select * from web_sima.get_foreign_dependencies(:tablename, :idvalue)';
        
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(':tablename', $tablename);
        $command->bindParam(':idvalue', $idvalue);
        
        $data = $command->queryAll();
        
        $result = [];
        foreach($data as $row)
        {
            $result[] = $row['get_foreign_dependencies'];
        }
        
        return $result;
    }
    
    public static function SetFileversionStatusOnRepository(FileVersion $fileVersion, FileRepository $fileRepository, $status, $statusinfo)
    {
        $fileversion_id = $fileVersion->id;
        $repository_id = $fileRepository->id;
        
        $sql = 'select * from web_sima.set_fileversion_status_on_repository(:fileversion_id, :repository_id, :newstatus, :newstatusinfo)';
        
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(':fileversion_id', $fileversion_id);
        $command->bindParam(':repository_id', $repository_id);
        $command->bindParam(':newstatus', $status);
        $command->bindParam(':newstatusinfo', $statusinfo);
        
        $data = $command->queryAll();
        
        if($data[0]['set_fileversion_status_on_repository'] !== 'OK')
        {
            $message = 'SetFileversionStatusOnRepository error';
            if(isset($data[1]) && isset($data[1]['message']))
            {
                $message = $data[1]['message'];
            }
            throw new Exception($message);
        }
        
        return true;
    }
    
    public static function applyQuery($query, $alias='t')
    {
        $sessionid = self::GetSessionId();
        
        $sql = "select * from web_sima.applyquery('$alias',:query,:sessionid);";
        
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(':query', $query);
        $command->bindParam(':sessionid', $sessionid);
                
        try
        {
            $data = $command->queryAll();
        }
        catch (Exception $exc) 
        {
            if(isset($exc->errorInfo) 
                && gettype($exc->errorInfo) === 'array' 
                && count($exc->errorInfo) === 3
                && (
                        $exc->errorInfo[0] === SIMASQLFunctions::$PG_ERROR_CODE_STACK_DEPTH_LIMIT_EXCEEDED
                        ||
                        $exc->errorInfo[0] === SIMASQLFunctions::$PG_ERROR_CODE_COMPLEX_QUERY
                    )
            )
            {
                throw new SIMAWarnExceptionQueryComplexForGetFiles();
            }
            
            throw $exc;
        }
                
        return $data;
    }
    
    /**
     * 
     * @param Task $task
     * @return array - niz id-eva zadataka
     */
    public static function getTaskParents(Task $task)
    {
        $task_id = $task->id;
        
        $sql = "select * from web_sima.get_task_parents(:task_id);";
        
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(':task_id', $task_id);
        
        $data = $command->queryAll();
        
        $result = [];
        
        if(!empty($data[0]['get_task_parents']))
        {            
            $get_task_parents = str_replace('}', '', str_replace('{', '', $data[0]['get_task_parents']));
            
            $result = explode(',', $get_task_parents);
        }
        
        return $result;
    }
    
    public static function GetTaskSubcoordinatorIds(Task $task)
    {
        $task_id = $task->id;
        
        $sql = "select * from web_sima.task_subcoordinators(:task_id)";
        
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(':task_id', $task_id);
        
        $data = $command->queryAll();
        
        $task_subcoordinators = $data[0]['task_subcoordinators'];
        
        $task_subcoordinator_ids = explode(',',$task_subcoordinators);
        
        return $task_subcoordinator_ids;
    }
    
    /**
     * 
     * @param RepManager $repmanager
     * @param type $offset
     * @param type $limit
     * @return string
     */
    public static function GetFilesRepmanagerHash(RepManager $repmanager, $offset=null, $limit=null)
    {
        $repmanager_id = $repmanager->id;
        
        $sql = "select md5(CAST((array_agg(f.* order by file_version_id))AS text))
                from (
                select file_version_id, file_size, md5sum
                from files.file_versions_to_repmanagers fvtr, files.file_versions fv
                where fvtr.file_version_id=fv.id and repmanager_id=:repmanager_id and fvtr.status='STAT_READY'
                ORDER BY file_version_id ASC";
        
        if(!is_null($offset))
        {
            $sql .= ' OFFSET '.$offset;
        }
        if(!is_null($limit))
        {
            $sql .= ' LIMIT '.$limit;
        }
        
        $sql .= ') f';
        
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(':repmanager_id', $repmanager_id);
        
        $data = $command->queryAll();
        
        $md5_result = $data[0]['md5'];
        
        if(is_null($md5_result))
        {
            $md5_result = '';
        }
        
        return $md5_result;
    }
    
    public static function LockFile(File $file)
    {
        $file_id = $file->id;
        $session_id = Yii::app()->user->db_session->id;
        
        $sql = "select * from web_sima.lock_file('$file_id','$session_id','zakljucano iz web-a');";
        $sql_result = Yii::app()->db->createCommand($sql)->queryAll();
        
        $result = $sql_result[0]['lock_file'];
        
        return $result;
    }
    
    public static function GetQueryFromOptimizatorHash($query, $parse_dd_id=true)
    {
        $dd_id_part = '';
        if($parse_dd_id === true)
        {
            $matches = null;
            if(preg_match('/^\$\$\d+\$\$/', $query, $matches))
            {
                $dd_id_part = $matches[0];
                $rest_of_query = substr($query, strlen($dd_id_part));
                if(!empty($rest_of_query))
                {
                    $query = $rest_of_query;
                }
            }
        }
        
        $sql = "select * from web_sima.get_query_from_optimizator_hash(:query);";
        
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(':query', $query);
        
        $data = $command->queryAll();
        
        return $dd_id_part.$data[0]['get_query_from_optimizator_hash'];
    }
    
    public static function OptimizeQueryAndGetHash($query, $parse_dd_id=true)
    {
        $dd_id_part = '';
        if($parse_dd_id === true)
        {
            $matches = null;
            if(preg_match('/^\$\$\d+\$\$/', $query, $matches))
            {
                $dd_id_part = $matches[0];
                $rest_of_query = substr($query, strlen($dd_id_part));
                if(!empty($rest_of_query))
                {
                    $query = $rest_of_query;
                }
            }
        }
        
        $optimize_query_and_get_hash = '';
        if(!empty($query))
        {
            $sql = "select * from web_sima.optimize_query_and_get_hash(:query);";

            $command = Yii::app()->db->createCommand($sql);
            $command->bindParam(':query', $query);

            $data = $command->queryAll();
            $optimize_query_and_get_hash = $data[0]['optimize_query_and_get_hash'];
        }
        
        return $dd_id_part.$optimize_query_and_get_hash;
    }
    
    private static function GetSessionId($session_id = null)
    {
        $sessionid = -1;
        if(!is_null($session_id))
        {
            $sessionid = $session_id;
        }
        else
        {
            if(Yii::app()->isWebApplication() && !Yii::app()->controller->isApiAction())
            {
                $sessionid = Yii::app()->user->db_session->id;
            }
            else
            {
                if(isset($_SESSION['sima_session']))
                {
                    $sessionid = $_SESSION['sima_session'];
                }
                else if(isset($_SESSION['db_session_id']))
                {
                    $sessionid = $_SESSION['db_session_id'];
                }
                else
                {
                    $sessionid = 2;//$_SESSION['sima_session'];
                }
            }
        }
        
        return $sessionid;
    }
}
