<?php

class SessionEndAfterWork extends SIMAWork
{
    public function run(array $args=[])
    {
        foreach($args as $args_part)
        {
            $session_id = $args_part['session_id'];
            $session = Session::model()->findByPkWithCheck($session_id);

            if($session->licence_type === Session::$LICENCE_TYPE_PERSONAL)
            {
                $this->transformOnlineSharedToPersonal($session);
            }
        }
    }
    
    private function transformOnlineSharedToPersonal(Session $session)
    {
        $shared_session = Session::model()
                ->online()
                ->sharedLicence()
                ->orderByStartTimeASC()
                ->find([
                    'model_filter' => [
                        'user' => [
                            'ids' => $session->user->id
                        ]
                    ]
                ]);
        if(!empty($shared_session))
        {
            Yii::app()->licenceManager->checkLicencePersonal($shared_session, true);
        }
    }
}

