<?php

class SIMAComponent extends CComponent
{
    public function __construct()
    {        
        $this->init();
        $this->internalAttachBehavior();
    }
    
    public function init()
    {
        
    }
    
    public function behaviors()
    {        
        return [];
    }
    
    private static $_added_behaviors = array();

    public static function registerPermanentBehavior($behavior)
    {
        $model_name = get_called_class();

        if (!isset(self::$_added_behaviors[$model_name]))
        {
            self::$_added_behaviors[$model_name] = [];
        }

        self::$_added_behaviors[$model_name][] = $behavior;
    }
    
    private function internalAttachBehavior()
    {
        if (method_exists($this, 'behaviors'))
        {
            $this->attachBehaviors($this->behaviors());
        }
        $model_name = get_called_class();
        if (isset(self::$_added_behaviors[$model_name]))
        {
            $this->attachBehaviors(self::$_added_behaviors[$model_name]);
        }
    }
}

