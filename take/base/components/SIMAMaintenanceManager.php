<?php

class SIMAMaintenanceManager
{
    private $_conf_dir_file_path = null;
    private $_lock_file_path = null;
    private $_updating_start_timestamp = null;
    private $_is_updating = false;
    
    public function init()
    {
        $this->reload();
    }
    
    public function reload()
    {
        $this->_is_updating = Yii::app()->params['isUpdating'];
        $this->_updating_start_timestamp = Yii::app()->params['updatingStartTimestamp'];
        $this->_conf_dir_file_path = Yii::app()->basePath.'/config/';
        $this->_lock_file_path = Yii::app()->basePath.'/../install/update.lock';
        if(file_exists($this->_lock_file_path))
        {
            $this->_is_updating = true;
            $this->_updating_start_timestamp = file_get_contents($this->_lock_file_path);
        }
    }
    
    /**
     * 
     * @param string $enviroment
     * @param bool $is_updating
     * @param bool $lockfilecheck_throw_exception
     *  privremeni parametar zbog prvog azuriranja pod novim sistemom
     *  kada svuda prodje prvo azuriranje ovaj parametar ukloniti
     *  mada je ovaj parametar potreban i za azuriranje demo trunk/test jer se prvo postavi updating, pa tek onda pokrene install run
     *  TODO: ukloniti mozda
     * @throws Exception
     */
    public function setUpdating($enviroment, $is_updating, $lockfilecheck_throw_exception=true)
    {
        $this->_is_updating = $is_updating;
        $this->_updating_start_timestamp = time();
        
        if($is_updating === true)
        {
            if(file_exists($this->_lock_file_path))
            {
                if($lockfilecheck_throw_exception === true)
                {
                    throw new Exception('update lock exists');
                }
            }
            else
            {
                $this->createLockFile();
            }
        }
        else
        {
            if(!file_exists($this->_lock_file_path))
            {
                if($lockfilecheck_throw_exception === true)
                {
                    throw new Exception('update lock not exists');
                }
            }
            else /// else ukloniti kada se ukloni $lockfilecheck_throw_exception
            {
                $this->removeLockFile();
            }
        }
        
        $this->setInConf('main', $enviroment);
        $this->setInConf('console', $enviroment);
        $this->setInConf('params', $enviroment);
    }
    
    public function isUpdating()
    {
        if(file_exists($this->_lock_file_path))
        {
            return true;
        }
        
        return Yii::app()->params['isUpdating'] === true;
    }
    
    public function getUpdatingStartTimestamp()
    {
        return $this->_updating_start_timestamp;
    }
    
    public function getLockFilePath()
    {
        return $this->_lock_file_path;
    }
    
    public function setUpdatingStartTimestamp($new_val)
    {
        $this->_updating_start_timestamp = $new_val;
    }
    
    private function setInConf($conf_name, $enviroment)
    {
        $mainFilePath = $this->_conf_dir_file_path.'/'.$conf_name.'.'.$enviroment.'.php';
        
        $mainContentToMarkUpdating = file_get_contents($mainFilePath);

        if(strpos($mainContentToMarkUpdating, 'isUpdating') !== false)
        {
            if($this->_is_updating === true)
            {
                $mainContentToMarkUpdating = preg_replace([
                    "/'isUpdating' => false/"
                ], [
                    "'isUpdating' => true"
                ], $mainContentToMarkUpdating);
            }
            else
            {
                $mainContentToMarkUpdating = preg_replace([
                    "/'isUpdating' => true/",
                ], [
                    "'isUpdating' => false",
                ], $mainContentToMarkUpdating);
            }
        }
        if(strpos($mainContentToMarkUpdating, 'updatingStartTimestamp') !== false)
        {
            if($this->_is_updating === true)
            {
                $mainContentToMarkUpdating = preg_replace([
                    "/'updatingStartTimestamp' => .+,/"
                ], [
                    "'updatingStartTimestamp' => ".$this->_updating_start_timestamp.","
                ], $mainContentToMarkUpdating);
            }
        }

        file_put_contents($mainFilePath, $mainContentToMarkUpdating);
    }
    
    private function createLockFile()
    {
        file_put_contents($this->_lock_file_path, $this->_updating_start_timestamp);
    }
    
    private function removeLockFile()
    {
        unlink($this->_lock_file_path);
    }
}

