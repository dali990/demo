<?php

class WebTabSession extends SIMAActiveRecord implements ArrayAccess
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'web_sima.web_tab_sessions';
    }
    
    public function moduleName()
    {
        return 'base';
    }

//     public function __get($column)
//     {
//     	switch ($column)
//     	{
//     		case 'hasChildren': return ($this->count_childrens>0);
// //     		case 'path': 		return 'site/todo';
//     		default: 			return parent::__get($column);
//     	}
//     }

    public function relations($child_relations = [])
    {
        return array(
            'session' => array(self::BELONGS_TO, 'Session', 'session_id'),
        );
    }

    public function rules()
    {
        return array(
            array('id', 'required'),
            array('data', 'safe'),
            ['id', 'unique']
//     			array('parent_id, description, model, model_id, file_attributes_table', 'safe'),
//     			array('comment_thread_id, assign_to_id, creator_id, todo_status_id, todo_priority_id','default',
//     					'value'=>null,
//     					'setOnEmpty'=>true,'on'=>array('insert','update')),
        );
    }
    
    protected static function getJSONColumns()
    {
        return array('data');
    }

    
    /**
     * pravi novu tab sesiju
     * @return \WebTabSession
     */
    public static function createNew($location)
    {
        $new_session = new WebTabSession();
        $new_session->session_id = Yii::app()->user->db_session->id;
        $new_session->data = [
            'time' => time(),
            'sima_apps_session' => -1,
            'sima_apps_sessions' => []
        ];
        
        $attempt_counter = 0;
        $allowed_attempts = 3;
        while(true)
        {
            try
            {
                $new_session->id = $location.'_'.SIMAHtml::uniqid();
                $new_session->save();
                break;
            }
            catch(SIMAWarnModelNotValid $e)
            {
                $erros = $new_session->getErrors('id');
                if(
                        !empty($erros)
                        && $attempt_counter < $allowed_attempts
                )
                {
                    /// u pitanju je uniq violation koji ce sledecim prolazom while-a da se verovatno resi
                    $attempt_counter++;
                }
                else
                {
                    throw $e;
                }
            }
        }
        
        return $new_session;
    }

   
    public function checkPHPSESSION($phpSessionId)
    {
        $webTabSessionModel = WebTabSession::model()->findByPk($this->id);
        
        if(empty($webTabSessionModel))
        {
            Yii::app()->user->loginRequired();
        }
        else if(!isset($webTabSessionModel->session['php_session_id']))
        {
            $webTabSessionModel->session['php_session_id'] = $phpSessionId;
        }
        else if($webTabSessionModel->session['php_session_id'] !== $phpSessionId)
        {
            Yii::app()->user->loginRequired();
        }
        else
        {
            /// all ok
        }
    }

    /**
     * No need for transaction
     * This method is required by the interface ArrayAccess.
     * @param mixed $offset the offset to check on
     * @return boolean
     */
    public function offsetExists($offset)
    {
        $model = WebTabSession::model()->findByPk($this->id);
        if (!$model) return false;
        $arr = $model->data;
        return isset($arr[$offset]);
    }

    /**
     * This method is required by the interface ArrayAccess.
     * @param integer $offset the offset to retrieve element.
     * @return mixed the element at the offset, null if no element is found at the offset
     */
    public function offsetGet($offset)
    {
        $model = WebTabSession::model()->findByPk($this->id);
        if(empty($model))
        {
            throw new Exception(Yii::t('BaseModule.WebTabSession', 'WebTabSessionNotfoundById', ['{id}'=>$this->id]));
        }
        $arr = $model->data;
        return isset($arr[$offset]) ? $arr[$offset] : null;
    }

    /**
     * This method is required by the interface ArrayAccess.
     * @param integer $offset the offset to set element
     * @param mixed $item the element value
     */
    public function offsetSet($offset,$item)
    {
        if (isset(Yii::app()->session['allowed_pages']) && Yii::app()->session['allowed_pages']==='allowed_pages')
        {
            return;
        }
        $success = false;
        while (!$success)
        {
            $model = WebTabSession::model()->findByPk($this->id);
            if(empty($model))
            {
                throw new Exception(Yii::t('BaseModule.WebTabSession', 'WebTabSessionNotfoundById', ['{id}'=>$this->id]));
            }

            $arr = $model->data;
            $arr[$offset] = $item;

            $table = WebTabSession::model()->tableName();
            
            $attr_data = $model->getAttribute('data');
            $attr_data = str_replace('\'', '\'\'', $attr_data);

            $q = Yii::app()->db->createCommand()
                ->update($table,[
                    'data' => CJSON::encode($arr)
                ],[
                    'AND',
                    'id = :id',
                    "data = '".$attr_data."'"
                ],[
                    ':id' => $this->id
                ]);

            if ($q>1)
            {
                throw new Exception ('WebTabSession sa isitim ID!!');
            }
            elseif($q == 1)
            {
                $success = true;
            }
        }
    }

    /**
     * This method is required by the interface ArrayAccess.
     * @param mixed $offset the offset to unset element
     */
    public function offsetUnset($offset)
    {
        if (isset(Yii::app()->session['allowed_pages']) && Yii::app()->session['allowed_pages']==='allowed_pages')
        {
            return;
        }
        $success = false;
        while (!$success)
        {
            $model = WebTabSession::model()->findByPk($this->id);

            $arr = $model->data;
            unset($arr[$offset]);

            $table = WebTabSession::model()->tableName();

            $q = Yii::app()->db->createCommand()
                ->update($table,[
                    'data' => CJSON::encode($arr)
                ],[
                    'AND',
                    'id = :id',
                    "data = '".$model->getAttribute('data')."'"
                ],[
                    ':id' => $this->id
                ]);

            if ($q>1)
            {
                throw new Exception ('WebTabSession sa isitim ID!!');
            }
            elseif($q == 1)
            {
                $success = true;
            }
        }
    }

    
}