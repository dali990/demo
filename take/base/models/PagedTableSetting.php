<?php

class PagedTableSetting extends SIMAActiveRecord
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'web_sima.paged_table_settings';
    }

    public function relations($child_relations = [])
    {
        return array(
            'user' => array(self::BELONGS_TO, 'User', 'user_id'),
        );
    }

    public function rules()
    {
        return array(
            array('user_id, table_string', 'required'),
            array('user_id', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => array('insert', 'update'))
// 			array('partner_id, street, number, city_id', 'safe'),
        );
    }

    public static function getJSONColumns()
    {
        return array('settings','columns');
    }
    
}
