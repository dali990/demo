<?php

class ModelUpdateEvent2 extends SIMAActiveRecord
{
//    public static $STATUS_INITIAL = 'STAT_INITIAL';
//    public static $STATUS_WAITING = 'STAT_WAITING';
//    public static $STATUS_PROGRESS = 'STAT_PROGRESS';
//    public static $STATUS_DONE = 'STAT_DONE';
//    
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function moduleName()
    {
        return 'base';
    }
    
    public function tableName()
    {
        return 'base.models_update_events2';
    }
//    
//    public function rules()
//    {
//    	return [
//            ['model_tag', 'required'],
//            ['event_time, update_status', 'safe']
//        ];
//    }
//    
    public function scopes()
    {
        $alias = $this->getTableAlias();
        $completed_phpsessionids_table_alias = ModelUpdateEvent2CompletedPhpsessionid::model()->tableName();
        return [
            'oldExpired' => [
                'condition' => "$alias.create_time<now() AND $alias.create_time<(now()-INTERVAL '1h')"
            ],
            'unsent' => [
                'condition' => "NOT EXISTS (SELECT 1 FROM $completed_phpsessionids_table_alias cpt WHERE cpt.php_session_id=$alias.php_session_id LIMIT 1)"
            ],
            'php_session_id_empty' => [
                'condition' => "$alias.php_session_id = '' OR $alias.php_session_id IS NULL"
            ]
        ];
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters': return [
                'php_session_id' => 'dropdown'
            ];
            default: return parent::modelSettings($column);
        }
    }
}

