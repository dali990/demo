<?php

class MainMenuFavoriteItemToGroup extends SIMAActiveRecord
{
    public $delete_item_if_is_last_item_to_group = true;
    public $apply_sync_with_old_main_menu_favorite_items = true;
    
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'base.mainmenu_favorite_items_to_groups';
    }

    public function __get($column)
    {
        switch ($column)
        {
            case 'DisplayName': return "{$this->item->DisplayName}({$this->group->DisplayName})";
            case 'SearchName': return $this->DisplayName;
            default: return parent::__get($column);
        }
    }
    
    public function rules()
    {
        return [
            ['item_id, group_id', 'required'],
            ['prev_id', 'safe'],
            ['item_id, group_id', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => ['insert', 'update']],
            ['item_id', 'unique_with', 'with' => ['group_id']],
            ['item_id', 'checkUser', 'on' => ['insert', 'update']]
        ];
    }
    
    public function checkUser()
    {
        if (
                !$this->hasErrors() &&
                (
                    $this->isNewRecord || 
                    $this->columnChanged('group_id') || 
                    $this->columnChanged('item_id')
                ) &&
                intval($this->group->user_id) !== intval($this->item->user_id)
            )
        {
            $this->addError('item_id', Yii::t('BaseModule.MainMenuFavoriteItemToGroup', 'GroupAndItemUserMustBeSame'));
        }
    }
    
    public function scopes()
    {
        return [
            'byGroupName' => [
                'order' => "group.group_name ASC",
                'with' => 'group'
            ],
            'byItemName' => [
                'order' => "item.label ASC",
                'with' => 'item'
            ],
        ];
    }
    
    public function notWithIds($ids)
    {
        $alias = $this->getTableAlias();

        $condition = '';
        if (!empty($ids))
        {
            $ids_string = implode(',', $ids);
            $condition = "$alias.id not in ($ids_string)";
        }
        $this->getDbCriteria()->mergeWith([
            'condition'=> $condition
        ]);
    }
    
    public function relations($child_relations = [])
    {
        return [
            'item' => [self::BELONGS_TO, MainMenuFavoriteItem::class, 'item_id'],
            'group' => [self::BELONGS_TO, MainMenuFavoriteGroup::class, 'group_id'],
            'prev' => [self::BELONGS_TO, MainMenuFavoriteItemToGroup::class, 'prev_id'],
            'next' => [self::HAS_ONE, MainMenuFavoriteItemToGroup::class, 'prev_id']
        ];
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters' : return [
                'item' => 'relation',
                'group' => 'relation',
                'prev' => 'relation',
                'next' => 'relation'
            ];
            case 'textSearch': return array(
                'item' => 'relation',
                'group' => 'relation'
            );
            default: return parent::modelSettings($column);
        }
    }
    
    public function afterSave()
    {
        parent::afterSave();

        if ($this->isNewRecord && empty($this->prev_id))
        {
            $first_item_to_group = MainMenuFavoriteItemToGroup::model()->find([
                'model_filter' => [
                    'AND',
                    [
                        'group' => [
                            'ids' => $this->group_id
                        ],
                        'prev' => [
                            'ids' => 'null'
                        ],
                    ],
                    [
                        'NOT',
                        [
                            'ids' => $this->id
                        ]
                    ]
                ]
            ]);
            if (!empty($first_item_to_group))
            {
                $first_item_to_group->prev_id = $this->id;
                $first_item_to_group->save();
            }
        }
        
        if ($this->apply_sync_with_old_main_menu_favorite_items === true && $this->group->is_default === true)
        {
            self::syncWithOldMainMenuFavoriteItems($this->group->user);
        }
    }
    
    public function beforeDelete() 
    {
        if (!empty($this->next))
        {
            $this->next->prev_id = !empty($this->prev_id) ? $this->prev_id : null;
            $this->next->save();
        }

        return parent::beforeDelete();
    }
    
    public function afterDelete()
    {
        parent::afterDelete();
        
        if ($this->delete_item_if_is_last_item_to_group === true)
        {
            $main_menu_favorite_item_to_groups_count = MainMenuFavoriteItemToGroup::model()->count([
                'model_filter' => [
                    'item' => [
                        'ids' => $this->item_id
                    ]
                ]
            ]);

            if ($main_menu_favorite_item_to_groups_count === 0)
            {
                $this->item->delete();
            }
        }
        
        if ($this->apply_sync_with_old_main_menu_favorite_items === true && $this->group->is_default === true)
        {
            self::syncWithOldMainMenuFavoriteItems($this->group->user);
        }
    }
    
    public function afterValidateDependencyError(array $foreign_dependencies)
    {        
        $allowed_tables = [
            MainMenuFavoriteItemToGroup::tableName().':prev_id'
        ];
        
        $parsed_foreign_dependencies = array_diff($foreign_dependencies, $allowed_tables);
        
        parent::afterValidateDependencyError($parsed_foreign_dependencies);
    }
    
    public static function add(MainMenuFavoriteItem $item, MainMenuFavoriteGroup $group)
    {
        $item_to_group = MainMenuFavoriteItemToGroup::model()->findByAttributes([
            'item_id' => $item->id,
            'group_id' => $group->id
        ]);
        
        if (empty($item_to_group))
        {
            $item_to_group = new MainMenuFavoriteItemToGroup();
            $item_to_group->item_id = $item->id;
            $item_to_group->group_id = $group->id;
            $item_to_group->save();
        }

        return $item_to_group;
    }
    
    public static function syncWithOldMainMenuFavoriteItems(User $user)
    {
        $main_tab_setting = MainTabsSetting::model()->findByAttributes(['user_id' => $user->id]);
        
        if (!empty($main_tab_setting))
        {
            $first_item_to_group = MainMenuFavoriteItemToGroup::model()->find([
                'model_filter' => [
                    'group' => [
                        'user' => [
                            'ids' => $user->id
                        ],
                        'is_default' => true
                    ],
                    'prev' => [
                        'ids' => 'null'
                    ]
                ]
            ]);

            $sorted_items = [];
            if (!empty($first_item_to_group))
            {
                array_push($sorted_items, self::parseItemForOldMainMenuFavoriteItems($first_item_to_group));
                $curr_item_to_group = $first_item_to_group;
                while (!empty($curr_item_to_group->next))
                {
                    array_push($sorted_items, self::parseItemForOldMainMenuFavoriteItems($curr_item_to_group->next));
                    $curr_item_to_group = $curr_item_to_group->next;
                }
            }

            $main_tab_setting->main_tabs = MainMenuFavoriteItem::arrayToJsonString($sorted_items);
            $main_tab_setting->apply_sync_with_new_main_menu_favorite_items = false;
            $main_tab_setting->save();
        }
    }
    
    private static function parseItemForOldMainMenuFavoriteItems($item_to_group)
    {
        $action_array = CJSON::decode($item_to_group->item->action);
        $new_tab = [
            'title' => $item_to_group->item->label,
            'action' => $action_array[0]
        ];
        if (!empty($action_array[1]))
        {
            $new_tab['id'] = $action_array[1];
        }

        return $new_tab;
    }
}
