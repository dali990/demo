<?php

class PhoneNumberType extends SIMAActiveRecord
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'base.phone_number_types';
    }
    
    public function moduleName()
    {
        return 'base';
    }

    public function __get($column)
    {
        switch ($column)
        {
            case 'DisplayName': return $this->name;
            case 'SearchName': return $this->name;
            default: return parent::__get($column);
        }
    }

    public function rules()
    {
        return [
            ['name', 'required'],
            ['name', 'unique'],
        ];
    }

    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters' : return [
                'name' => 'text'
            ];
            case 'textSearch' : return [
                'name' => 'text'
            ];
            default: return parent::modelSettings($column);
        }
    }

}