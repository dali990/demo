<?php

class MainMenuFavoriteItem extends SIMAActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'base.mainmenu_favorite_items';
    }
    
    public function __get($column)
    {
        switch ($column)
        {
            case 'DisplayName': return $this->label;
            case 'SearchName': return $this->DisplayName;
            default: return parent::__get($column);
        }
    }

    public function rules()
    {
        return [
            ['label, user_id', 'required'],
            ['action, component_name', 'safe'],
            ['user_id', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => ['insert', 'update']],
            ['user_id', 'unique_with', 'with' => ['action']],
            ['user_id', 'unique_with', 'with' => ['component_name']],
            ['action', 'checkActionAndComponentName']
        ];
    }
    
    public function checkActionAndComponentName()
    {
        if (!$this->hasErrors())
        {
            if (empty($this->action) && empty($this->component_name))
            {
                $this->addError('action', Yii::t('BaseModule.MainMenuFavoriteItem', 'ActionOrComponentNameMustBeSet'));
            }
        }
    }
    
    public function scopes()
    {
        $alias = $this->getTableAlias();
        $items_to_groups_table_name = MainMenuFavoriteItemToGroup::model()->tableName();
        $uniq = SIMAHtml::uniqid();
        
        return [
            'byName' => [
                'order' => "$alias.label ASC"
            ],
            'without_groups' => [
                'condition' => "not exists (select 1 from $items_to_groups_table_name itg$uniq where itg$uniq.item_id = $alias.id)"
            ]
        ];
    }
    
    public function relations($child_relations = [])
    {
        return [
            'user' => [self::BELONGS_TO, User::class, 'user_id'],
            'mainmenu_favorite_item_to_groups' => [self::HAS_MANY, MainMenuFavoriteItemToGroup::class, 'item_id'],
            'mainmenu_favorite_item_to_groups_cnt' => [self::STAT, MainMenuFavoriteItemToGroup::class, 'item_id', 'select' => 'count(*)']
        ];
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters' : return [
                'label' => 'text',
                'action' => 'text',
                'component_name' => 'text',
                'user' => 'relation',
                'mainmenu_favorite_item_to_groups' => 'relation'
            ];
            case 'textSearch': return array(
                'label' => 'text'
            );
            default: return parent::modelSettings($column);
        }
    }
    
    protected function modelOptions(\User $user = null):array
    {
        $options = [];

        if (!empty($user) && intval($user->id) === intval($this->user_id))
        {
            $options = ['form', 'delete'];
        }

        return $options;
    }
    
    public function beforeDelete() 
    {
        foreach ($this->mainmenu_favorite_item_to_groups as $mainmenu_favorite_item_to_group)
        {
            $mainmenu_favorite_item_to_group->delete_item_if_is_last_item_to_group = false;
            $mainmenu_favorite_item_to_group->delete();
        }

        return parent::beforeDelete();
    }
    
    public function afterValidateDependencyError(array $foreign_dependencies)
    {        
        $allowed_tables = [
            MainMenuFavoriteItemToGroup::tableName().':item_id'
        ];
        
        $parsed_foreign_dependencies = array_diff($foreign_dependencies, $allowed_tables);
        
        parent::afterValidateDependencyError($parsed_foreign_dependencies);
    }
    
    public static function arrayToJsonString($array)
    {
        return json_encode($array, JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
    }
}
