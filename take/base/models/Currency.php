<?php 



class Currency extends SIMAActiveRecord
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
 
    public function tableName()
    {
        return 'base.currencies';
    }
    
    public function moduleName()
    {
        return 'base';
    }
    
    public function __get($column)
    {
    	switch ($column)
    	{
    		case 'DisplayName': 	return $this->short_name;
    		case 'SearchName':      return $this->short_name;
    		default: 		return parent::__get($column);
    	}
    }
    
    public function scopes()
    {
        $alias = $this->getTableAlias();
        return [
            'byName' => [
                'order' => "$alias.name"
            ]
        ];
    }
    
    public function rules()
    {
    	return array(
            array('name, short_name, code', 'required'),
            array('description', 'safe'),
            array('code', 'length', 'min'=>2, 'max'=>3)
    	);
    }
    
    public function isCountryCurrency()
    {
        $country_currency_id = Yii::app()->configManager->get('base.country_currency');
        if(empty($country_currency_id))
        {
            throw new SIMAWarnException(Yii::t('AccountingModule.CurrencyConversion', 'CountryCurrencyConfigNotSet'));
        }
        
        if($this->id == $country_currency_id)
        {
            return true;
        }
        
        return false;
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters' : return [
                'name' => 'text',
                'code' => 'text',
                'short_name' => 'text',
                'description' => 'text',
            ];
            case 'textSearch': return array(
                'short_name' => 'text'
            ); 
            case 'default_order_scope': return 'byName';
            default: return parent::modelSettings($column);
        }
    }
}