<?php

class ConfigurationParameter extends SIMAActiveRecord
{
    private $_confParam = null;
    private $_confParamDefinition = null;
    
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    
    public function moduleName()
    {
        return 'base';
    }
    
    public function tableName()
    {
        return 'base.configuration_parameters';
    }
    
    public function __get($column) 
    {
        switch ($column)
        {
            case 'display_name':
            case 'DisplayName':
                $confParam = $this->getConfParam();
                return $confParam['display_name'];
            case 'description': 
                $confParam = $this->getConfParam();
                return $confParam['description'];
            case 'value_is_array': 
                $confParam = $this->getConfParam();
                return $confParam['value_is_array'];
            case 'value_not_null':
                $confParam = $this->getConfParam();
                return $confParam['value_not_null'];
            case 'path': 
                $confParam = $this->getConfParam();
                return $confParam['path'];
            case 'user_changable': 
                $confParam = $this->getConfParam();
                return $confParam['user_changable'];
            case 'user_changable_by_definition': 
                $result = true;
                $confParam = $this->getConfigParamDefinition();
                if(isset($confParam['user_changable']) && $confParam['user_changable'] === false)
                {
                    $result = false;
                }
                return $result;
            case 'type': 
                $confParam = $this->getConfParam();
                return $confParam['type'];
            case 'type_params': 
                $confParam = $this->getConfParam();
                return $confParam['type_params'];
            case 'value': 
                $confParam = $this->getConfParam();
                return $confParam['value'];
            case 'default': 
                $confParam = $this->getConfParam();
                return $confParam['default'];
            case 'user_change_forbidden': 
                $confParam = $this->getConfParam();
                return $confParam['user_change_forbidden'];
            case 'display_to_users': 
                $confParam = $this->getConfParam();
                return $confParam['display_to_users'];
            default: return parent::__get($column);
        }
    }
    
    public function relations($child_relations = [])
    {
        return array(
            'user' => array(self::BELONGS_TO, 'User', 'user_id'),
        );
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters': return array(
                'user'=>'relation',
                'user_id' => 'dropdown',
                'mapping_id' => 'dropdown'
            );
            case 'options': 
                $result = [
                    'configuration_parameter_edit'
                ];
                
                if($this->user_changable_by_definition === true)
                {
                    $result[] = 'configuration_parameter_user_changable';
                }
                
                $result[] = 'configuration_parameter_revert_to_default';
                return $result;
            default: return parent::modelSettings($column);
        }
    }
    
    public function getClasses()
    {
        return parent::getClasses() . ($this->display_to_users ? '' : ' _hidden-important');
    }
    
    private function getConfParam()
    {        
        if(!isset($this->_confParam))
        {
            $this->_confParam = Yii::app()->configManager->getByConfigurationParameter($this);
        }
        
        return $this->_confParam;
    }
    
    private function getConfigParamDefinition()
    {        
        if(!isset($this->_confParamDefinition))
        {
            $this->_confParamDefinition = Yii::app()->configManager->getDefinitionByConfigurationParameter($this);
        }
        
        return $this->_confParamDefinition;
    }
}
