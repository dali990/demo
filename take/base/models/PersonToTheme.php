<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class PersonToTheme extends SIMAActiveRecord
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
 
    public function tableName()
    {
        return 'base.persons_to_themes';
    }
    
    public function moduleName()
    {
        return 'base';
    }
  
    public function rules()
    {
    	return [
            ['person_id, theme_id', 'required'],
            ['role_in_team, to_notif, is_cost_controller', 'safe'],
            ['person_id', 'checkUniq'],
            ['person_id', 'checkCanDelete', 'on' => 'delete']
    	];
    }
    
    public function relations($child_relations = [])
    {
        return array(
            'person' => array(self::BELONGS_TO, 'Person', 'person_id'),
            'theme' => array(self::BELONGS_TO, 'Theme', 'theme_id'),
        );
    }
    
    public function checkUniq()
    {
        if (
                $this->columnChangedOrNew('person_id')
                ||
                $this->columnChangedOrNew('theme_id')
//                intval($this->__old['person_id']) !== intval($this->person_id) ||
//                intval($this->__old['theme_id']) !== intval($this->theme_id)
            )
        {
            $person_to_theme = PersonToTheme::model()->findByAttributes([
                'person_id' => $this->person_id,
                'theme_id' => $this->theme_id
            ]);
            if (!is_null($person_to_theme))
            {
                $this->addError('person_id', Yii::t('BaseModule.Theme','PersonToThemeUniq',[
                    '{person_name}'=>$this->person->DisplayName,
                    '{theme_name}'=>$this->theme->DisplayName
                ]));
            }
        }
    }
    
    public function checkCanDelete()
    {
        //ako postoji building modul
        if (Yii::app()->hasModule('buildings'))
        {
            //ako je ovaj clan tima teme ujedno clan nekog predmera, onda se zabranjuje njegovo brisanje
            if (!$this->hasErrors() && $this->scenario === 'delete')
            {
                $bill_of_quantity = BillOfQuantity::model()->find([
                    'model_filter' => [
                        'AND',
                        [
                            'OR',
                            [
                                'owner' => [
                                    'ids' => $this->person_id
                                ]
                            ],
                            [
                                'bill_of_quantity_to_users' => [
                                    'user' => [
                                        'ids' => $this->person_id
                                    ]
                                ]
                            ]
                        ],
                        [
                            'theme' => [
                                'ids' => $this->theme_id
                            ]
                        ]
                    ]
                ]);
                if (!empty($bill_of_quantity))
                {
                    $this->addError('person_id', Yii::t('BaseModule.PersonToTheme', 'CanNotDeleteBecauseThisMemberIsAlreadyMemberOfBillOfQuantity', [
                        '{bill_of_quantity_name}' => $bill_of_quantity->DisplayName
                    ]));
                }
            }
            
            //ako je ovaj clan tima teme ujedno clan nekog izovdjenja(sef ili pomocnik sefa), onda se zabranjuje njegovo brisanje
            if (!$this->hasErrors() && $this->scenario === 'delete')
            {
                $building_construction = BuildingConstruction::model()->find([
                    'model_filter' => [
                        'AND',
                        [
                            'OR',
                            [
                                'construction_boss' => [
                                    'ids' => $this->person_id
                                ]
                            ],
                            [
                                'construction_to_boss_assistants' => [
                                    'boss_assistant' => [
                                        'ids' => $this->person_id
                                    ]
                                ]
                            ]
                        ],
                        [
                            'theme' => [
                                'ids' => $this->theme_id
                            ]
                        ]
                    ]
                ]);
                if (!empty($building_construction))
                {
                    $this->addError('person_id', Yii::t('BaseModule.PersonToTheme', 'CanNotDeleteBecauseThisMemberIsAlreadyMemberOfBuildingConstruction', [
                        '{building_construction_name}' => $building_construction->DisplayName
                    ]));
                }
            }
        }
    }
    
    public function isCoordinator()
    {
        return $this->person_id === $this->theme->coordinator_id;
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'options': return ['form', 'delete'];
            case 'filters': return array_merge(parent::modelSettings($column),array(
                'person' => 'relation',
                'theme' => 'relation',
                'role_in_team' => 'text',
                'is_cost_controller' => 'boolean'
            ));
            case 'update_relations': return [
                'person'
            ];
            default: return parent::modelSettings($column);
        }
    }
    
    public function getConnectionTag()
    {
        //TODO: prebaciti da sve funkcionise sa R, ali proveriti kako radi
        if (
                $this->theme->id === 540 
                && 
                (
                    isset(Yii::app()->params['server_FQDN'])
                    &&
                    Yii::app()->params['server_FQDN'] === 'sima.set.rs'
                )
        )//Centar 5 na sima.set.rs
        {
            return $this->theme->tag->queryR;
        }
        else
        {
            return $this->theme->tag->query;
        }
    }
    
    public function afterSave() 
    {
        parent::afterSave();
        if ($this->isNewRecord && isset($this->person->user))
        {
            EmployeeTQLLimit::add($this->person_id, EmployeeTQLLimit::LimitTagLevels('InTeamOnTheme'), $this->getConnectionTag());
            
            //ako osoba nije koordinator teme, onda se obavestava da je postala deo tima
            if (
                    intval($this->person_id) !== intval($this->theme->coordinator_id) && 
                    intval($this->person_id) !== Yii::app()->user->id
                )
            {
                $text = Yii::t('BaseModule.PersonToTheme', 'NotifPersonInTeam', [
                    '{theme_name}' => $this->theme->DisplayName
                ]);
                Yii::app()->notifManager->sendNotif(
                    $this->person_id, 
                    $text, 
                    [
                        'info' => $text,
                        'action' => [
                            'action' => 'defaultLayout',
                            'action_id' => [
                                'model_name' => Theme::class,
                                'model_id' => $this->theme->id
                            ]
                        ]
                    ]
                );
            }
        }
        
        if (Yii::app()->isWebApplication() && ($this->isNewRecord || $this->columnChanged('is_cost_controller')) && $this->is_cost_controller === true)
        {
            $user = User::model()->findByPk($this->person_id);
            if (!empty($user))
            {
                $files_count = File::model()->count([
                    'model_filter' => [
                        'bill' => [],
                        'file_to_file_tags' => [
                            'file_tag' => [
                                'model_table' => Theme::model()->tableName(),
                                'model_id' => $this->theme->id,
                            ]
                        ],
                        'scopes' => [
                            'withoutSignatureForUser' => $user->id
                        ]
                    ]
                ]);
                
                if($files_count > 0)
                {
                    Yii::app()->controller->raiseAction('sima.theme.addCostControllerToFileSignatures', [$files_count, $this->person->DisplayName, $this->theme->DisplayName, $user->id, $this->theme_id], false);
                }
            }
        }

        /// MilosJ: testirano da ne treba, kada se budu ucitali komentari, tada ce dodati
//        $base_ct = CommentThread::getCommonForModel($this->theme);
//        try
//        {
//            $base_ct->addListener($this->person_id);
//        }
//        catch (SIMAExceptionModelNotFound $e)
//        {
//            //ako nije user, ne mora ni da ga dodaje
//        }

    }
    
    public function afterDelete() 
    {
        parent::afterDelete();
        if (isset($this->person->user))
        {
            EmployeeTQLLimit::remove($this->person_id, EmployeeTQLLimit::LimitTagLevels('InTeamOnTheme'), $this->getConnectionTag());

            //ako osoba nije koordinator teme, onda se obavestava da vise nije deo tima
            if (
                    intval($this->person_id) !== intval($this->theme->coordinator_id) && 
                    intval($this->person_id) !== Yii::app()->user->id
                )
            {
                $text = Yii::t('BaseModule.PersonToTheme', 'NotifPersonOutOfTeam', [
                    '{theme_name}' => $this->theme->DisplayName
                ]);
                Yii::app()->notifManager->sendNotif(
                    $this->person_id, 
                    $text, 
                    [
                        'info' => $text,
                        'action' => [
                            'action' => 'defaultLayout',
                            'action_id' => [
                                'model_name' => Theme::class,
                                'model_id' => $this->theme->id
                            ]
                        ]
                    ]
                );
            }
        }
    }
    
    public static function add($theme, $person)
    {
        $person_to_theme = PersonToTheme::model()->findByAttributes([
            'person_id' => $person->id,
            'theme_id' => $theme->id
        ]);

        if (empty($person_to_theme))
        {
            $person_to_theme = new PersonToTheme();
            $person_to_theme->person_id = $person->id;
            $person_to_theme->theme_id = $theme->id;
            $person_to_theme->save();
        }
        
        return $person_to_theme;
    }
}
