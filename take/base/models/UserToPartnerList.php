<?php

class UserToPartnerList extends SIMAActiveRecord
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
 
    public function tableName()
    {
        return 'base.users_to_partner_lists';
    }
    
    public function moduleName()
    {
        return 'base';
    }
    
    public function rules()
    {
        return [
            ['user_id, partner_list_id', 'required'],
            ['user_id','unique_with','with' => ['partner_list_id']]
        ];
    }
    
    public function relations($child_relations = [])
    {
        return [
            'user' => [self::BELONGS_TO, 'User', 'user_id'],
            'partner_list' => [self::BELONGS_TO, 'PartnerList', 'partner_list_id']
        ];
    }
    
    public function modelSettings($column)
    {
        $options = [];
        
        if (Yii::app()->isWebApplication() && !empty($this->partner_list_id))
        {
            $partner_list = $this->partner_list; //Sasa A. - mora ovako u promenljivu, jer ne mozemo da pitamo empty($this->partner_list) jer se resetuje model,
            //i ponisti se init data ako je zadat preko forme
            if (!empty($partner_list) && $partner_list->hasUserAccessToList())
            {
                $options[] = 'form';
                $options[] = 'delete';
            }
        }

        switch ($column)
        {
            case 'filters' : return array_merge(parent::modelSettings('filters'), [
                'user'=>'relation',
                'partner_list'=>'relation'
            ]);
            case 'options' : return $options;
            default: return parent::modelSettings($column);
        }
    }
    
    public static function add(PartnerList $partner_list, User $user)
    {
        return UserToPartnerList::addByIds($partner_list->id, $user->id);
    }
    
    public static function addByIds($partner_list_id, $user_id)
    {
        $user_to_partner_list = UserToPartnerList::model()->findByAttributes([
            'partner_list_id' => $partner_list_id,
            'user_id' => $user_id
        ]);

        if (empty($user_to_partner_list))
        {
            $user_to_partner_list = new UserToPartnerList();
            $user_to_partner_list->partner_list_id = $partner_list_id;
            $user_to_partner_list->user_id = $user_id;
            $user_to_partner_list->save();
        }
        
        return $user_to_partner_list;
    }
}
