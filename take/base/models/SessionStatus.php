<?php

class SessionStatus extends SIMAActiveRecord
{    
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    
    public function primaryKey()
    {
        return 'id';
    }
 
    public function tableName()
    {
        return 'public.sessions_status';
    }
    
    public function relations($child_relations = [])
    {
        return array(
            'session' => array(self::BELONGS_TO, 'Session', 'id'),            
        );
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters' : return array_merge(parent::modelSettings('filters'), [
                'session' => 'relation'
            ]);
            case 'options' : return [];
            default: return parent::modelSettings($column);
        }
    }
}