<?php

class Year extends SIMAActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'base.years';
    }
    
    public function moduleName()
    {
        return 'base';
    }
    
    public function __get($column)
    {
        switch ($column)
        {
          
            case 'DisplayName': return $this->year;
            case 'SearchName': return $this->year;
            default: return parent::__get($column);
        }
    }
    
    public function scopes()
    {
        $alias = $this->getTableAlias();
        return array(
            'byName' => array('order' => $alias.'.year DESC'),
            'byYear' => array('order' => $alias.'.year DESC'),
        );
    }
    
    public function beforeYear($year)
    {
        $alias = $this->getTableAlias();
        $uniq = SIMAHtml::uniqid();
        $this->getDbCriteria()->mergeWith([
            'condition' => $alias.'.year < :year'.$uniq,
            'params' => [':year'.$uniq => $year]
        ]);
        return $this;
    }
    
    public function afterYear($year)
    {
        $alias = $this->getTableAlias();
        $uniq = SIMAHtml::uniqid();
        $this->getDbCriteria()->mergeWith([
            'condition' => $alias.'.year > :year'.$uniq,
            'params' => [':year'.$uniq => $year]
        ]);
        return $this;
    }
    
    public function rules()
    {
        return array(
            array('year', 'numerical', 'integerOnly' => TRUE,'max' => 2100,'min' => 1900),
            array('year', 'unique')
        );
    }
    
    public function relations($child_relations = [])
    {
        return parent::relations(array_merge([
            'months' => [self::HAS_MANY,'Month','year_id']            
        ], $child_relations));
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'options': return [];
            case 'filters': return [
                'year' => 'integer',
                'hr' => 'relation',
                'accounting' => 'relation',
                'months' => 'relation'
            ];
            case 'textSearch' : return [
                'year' => 'integer',
            ];
            case 'default_order': return 'year';
            case 'default_order_scope': return 'byName';
            default: return parent::modelSettings($column);
        }
    }
    
    /**
     * 
     * @param integer $year
     * @return \Year
     * @throws Exception
     */
    public static function get($year = null)
    {
        if ($year === null)
        {
            $year = date('Y');
        }
        if (!is_numeric($year))
        {
            throw new Exception("Year::get($year) -> godina nije broj");
        }
        $res = Year::model()->findByAttributes([
            'year' => $year
        ]);
        if (is_null($res))
        {
            $res = new Year();
            $res->year = $year;
            $res->save();            
        }
        
        return $res;
    }
    
    public function getByDate($date)
    {
        $year = date('Y',strtotime($date));
        $res = Year::model()->findByAttributes([
            'year' => $year
        ]);
        if (is_null($res))
        {
            $res = new Year();
            $res->year = $year;
            $res->save();            
        }
        return $res;
    }
    
    public static function current()
    {
        $curr_year = date('Y');
        return Year::get($curr_year);
    }
    
    public function prev()
    {
        return $this->get(intval($this->year)-1);
    }
    
    public function next()
    {
        return $this->get(intval($this->year)+1);
    }
    
    public function param($name)
    {
        $_ar = explode('.', $name);
        if (count($_ar)<2)
        {
            throw new Exception("Year::param($name) -> parametar nije dobar");
        }
        $relation = $_ar[0];
        $param = $_ar[1];
        
        $relations = $this->relations();
        if (!isset($relations[$relation]))
        {
            throw new Exception("Year::param($name) -> relacija $relation ne postoji");
        }
        if (!isset($this->$relation))
        {
            $Model = $relations[$relation][1];
            $name = $Model::model()->modelLabel();
            throw new SIMAWarnException("Year::param($name) -> parametar ne moze biti dovucen jer nije dodata $name - ".$this->year);
        }

        return $this->$relation->$param;
    }
    
    public function __toString()
    {
        return (string)$this->year;
    }
    
    public function daysCount()
    {
        $begin = new DateTime( '01.01.'.$this->year.'.' );
        $end = new DateTime( '31.12.'.$this->year.'.' );
        
        $interval = new DateInterval('P1D');
        $daterange = new DatePeriod($begin, $interval ,$end);

        $_cnt = 0;
        foreach($daterange as $date){
            $_cnt ++;
        }
        return $_cnt;
    }

//    public function rules()
//    {
//        return array(
//            array('timestamp, name', 'required'),
////     			array('parent_id, description, model, model_id, file_attributes_table', 'safe'),
////     			array('comment_thread_id, assign_to_id, creator_id, todo_status_id, todo_priority_id','default',
////     					'value'=>null,
////     					'setOnEmpty'=>true,'on'=>array('insert','update')),
//        );
//    }

  
    
}