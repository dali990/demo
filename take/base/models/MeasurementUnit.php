<?php

class MeasurementUnit extends SIMAActiveRecord
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    
    public function moduleName()
    {
        return 'base';
    }

    public function tableName()
    {
        return 'base.measurement_units';
    }

    public function __get($column)
    {
        switch ($column)
        {
            case 'DisplayName':
            case 'SearchName': return $this->name;
            default: return parent::__get($column);
        }
    }

    public function rules()
    {
        return array(
            array('name', 'required', 'message' => 'Polje "{attribute}" mora biti uneto'),
            ['name', 'length', 'max' => 10],
//            array('description', 'safe'),
        );
    }

    public function scopes()
    {
        $alias = $this->getTableAlias();
        return array(
            'byName' => array(
                'order' => "$alias.name, $alias.id",
            ),
        );
    }

//    public function attributeLabels()
//    {
//        return array(
// 			'name'=>'Ime',
//        ) + parent::attributeLabels();
//    }

    public function relations($child_relations = [])
    {
        return array(
//            'building_structures' => array(self::HAS_MANY, 'BuildingStructure', 'building_structure_type_id'),
        );
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters': return [
                'name' => 'text',
            ];
            case 'textSearch' : return [
                'name' => 'text'
            ];
            case 'default_order_scope': return 'byName';
            default: return parent::modelSettings($column);
        }
    }
}