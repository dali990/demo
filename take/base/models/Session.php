<?php

class Session extends SIMAActiveRecord implements ArrayAccess
{   
    public static $SESSION_EXPIRE = 100; //sekunde koliko traje sesija
    public static $STATUS_ACTIVE = 'active';
    public static $STATUS_IDLE = 'idle';
    public static $STATUS_INACTIVE = 'inactive';
    
    public static $TYPE_SIMAapp = 'TYPE_SIMAapp';
    public static $TYPE_SIMAweb = 'TYPE_SIMAweb';
    public static $TYPE_SMB = 'TYPE_SMB';
    public static $TYPE_SIMADesktopApp = 'TYPE_SIMADesktopApp';        
    public static $TYPE_SIMAMobileApp = 'TYPE_SIMAMobileApp';
    
    public static $LICENCE_TYPE_MASTER = 'master';
    public static $LICENCE_TYPE_PERSONAL = 'personal';
    public static $LICENCE_TYPE_SHARED = 'shared';
    public static $LICENCE_TYPE_UNLICENCED = 'unlicenced';
 
    public $active;
    public $log_out_session;
    
    
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'public.sessions';
    }
    
    public function moduleName()
    {
        return 'base';
    }
    
    public function __get($column)
    {
        switch ($column)
        {
            case 'is_active': 
                if(!isset($this->active_session_status))
                {
                    return false;
                }
                return $this->active_session_status->status === Session::$STATUS_ACTIVE;
            case 'is_idle':
                if(!isset($this->active_session_status))
                {
                    return false;
                }
                return $this->active_session_status->status === Session::$STATUS_IDLE;
            case 'is_inactive':
                return !isset($this->active_session_status);
            case 'is_online':
                return isset($this->active_session_status);
            case 'status':
                if(!isset($this->active_session_status))
                {
                    return Session::$STATUS_INACTIVE;
                }
                return $this->active_session_status->status;
            default: return  parent::__get($column);
        }
    }
    
    public function relations($child_relations = [])
    {
        return array(
            'user' => array(self::BELONGS_TO, 'User', 'user_id'),
            'active_session' => array(self::HAS_ONE, 'ActiveSession', 'id'),
            'active_session_status' => array(self::HAS_ONE, 'SessionStatus', 'id'),
        );
    }
    
    /**
     * No need for transaction
     * This method is required by the interface ArrayAccess.
     * @param mixed $offset the offset to check on
     * @return boolean
     */
    public function offsetExists($offset)
    {
        $model = Session::model()->findByPk($this->id);
        if (!$model) return false;
        $arr = $model->data;
        return isset($arr[$offset]);
    }

    /**
     * This method is required by the interface ArrayAccess.
     * @param integer $offset the offset to retrieve element.
     * @return mixed the element at the offset, null if no element is found at the offset
     */
    public function offsetGet($offset)
    {
        $model = Session::model()->findByPk($this->id);
        $arr = $model->data;
        return isset($arr[$offset]) ? $arr[$offset] : null;
    }

    /**
     * This method is required by the interface ArrayAccess.
     * @param integer $offset the offset to set element
     * @param mixed $item the element value
     */
    public function offsetSet($offset,$item)
    {
//        if (isset(Yii::app()->session['allowed_pages']) && Yii::app()->session['allowed_pages']==='allowed_pages')
//        {
//            return;
//        }
        $success = false;
        while (!$success)
        {
            $model = Session::model()->findByPk($this->id);

            $arr = $model->data;
            $arr[$offset] = $item;

            $table = Session::model()->tableName();

            $q = Yii::app()->db->createCommand()
                ->update($table,[
                    'data' => CJSON::encode($arr)
                ],[
                    'AND',
                    'id = :id',
                    "data = '".$model->getAttribute('data')."'"
                ],[
                    ':id' => $this->id
                ]);

            if ($q>1)
            {
                throw new Exception ('Session sa isitim ID!!');
            }
            else if($q == 1)
            {
                $success = true;
            }
        }
    }

    /**
     * This method is required by the interface ArrayAccess.
     * @param mixed $offset the offset to unset element
     */
    public function offsetUnset($offset)
    {
//        if (isset(Yii::app()->session['allowed_pages']) && Yii::app()->session['allowed_pages']==='allowed_pages')
//        {
//            return;
//        }
        $success = false;
        while (!$success)
        {
            $model = Session::model()->findByPk($this->id);

            $arr = $model->data;
            unset($arr[$offset]);

            $table = Session::model()->tableName();

            $q = Yii::app()->db->createCommand()
                ->update($table,[
                    'data' => CJSON::encode($arr)
                ],[
                    'AND',
                    'id = :id',
                    "data = '".$model->getAttribute('data')."'"
                ],[
                    ':id' => $this->id
                ]);

            if ($q>1)
            {
                throw new Exception ('Session sa isitim ID!!');
            }
            elseif($q == 1)
            {
                $success = true;
            }
        }
    }        
    
    protected static function getJSONColumns()
    {
        return array('data');
    }
    
    public function scopes()
    {
        $alias = $this->getTableAlias(FALSE,FALSE);
        $session_status_view_name = SessionStatus::model()->tableName();
        $status_active = Session::$STATUS_ACTIVE;
        $status_idle = Session::$STATUS_IDLE;
        $uniq = SIMAHtml::uniqid();
        
        $sima_session = -1;
        if(Yii::app()->isWebApplication())
        {
            $sima_session = Yii::app()->session['sima_session'];
        }
        
        return array(
            'byName'=>array('order'=>'last_seen'),
            'orderByStartTimeASC' => ['order'=>'start_time ASC'],
            'orderByStartTimeDESC'=>array('order'=>'start_time DESC'),
            'orderByLastSeenDESC'=>array('order'=>'last_seen DESC'),
            'active'=>[
                'join' => "join $session_status_view_name ssa$uniq on $alias.id=ssa$uniq.id",
                'condition' => "ssa$uniq.status='$status_active'"
            ],
            'idle'=>[
                'join' => "join $session_status_view_name ssi$uniq on $alias.id=ssi$uniq.id",
                'condition' => "ssi$uniq.status='$status_idle'"
            ],
            'inactive'=>[                
                'condition' => "$alias.id not in (select ssin$uniq.id from $session_status_view_name ssin$uniq)"
            ],
            'online'=>[
                'join' => "join $session_status_view_name sso$uniq on $alias.id=sso$uniq.id"                
            ],
            'personalLicence' => [
                'condition' => "$alias.licence_type = 'personal'"
            ],
            'sharedLicence' => [
                'condition' => "$alias.licence_type = 'shared'"
            ],
            'notThisSession'=>[
                'condition' => "$alias.id != :curr_sima_session$uniq",
                'params' => array(
                    ':curr_sima_session'.$uniq => $sima_session
                )
            ]
        );
    }
    
    public function exceptUser($user_id)
    {
        $alias = $this->getTableAlias();
        $uniq = SIMAHtml::uniqid();
        $this->getDbCriteria()->mergeWith([
            'condition' => "$alias.user_id != :curr_sima_user_id$uniq",
            'params' => array(
                ':curr_sima_user_id'.$uniq => $user_id
            )
        ]);
        return $this;
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters' : return array_merge(parent::modelSettings('filters'),array(
                'user' => 'relation',
                'start_time' => 'date_time_range',
                'end_time' => 'date_time_range',
                'last_seen' => 'date_time_range',
                'session_type' =>'dropdown',
                'active' => array('dropdown','func'=>'filter_is_active'),
                'user_address' => 'text',
                'licence_type' => 'dropdown',
                'operating_system' => 'text',
                'browser' => 'text',
                'browser_string' => 'text',
                'browser_version' => 'text',
                'width_sima_resolution' => 'text',
                'height_sima_resolution' => 'text',
                'zoom' => 'text'
            ));
            case 'options' : return array();
            default: return parent::modelSettings($column);
        }
    }
    
    public function filter_is_active($condition) 
    {        
        if($this->active !== '') 
        {
            $alias = $this->getTableAlias(FALSE,FALSE);
            $session_status_view_name = SessionStatus::model()->tableName();
            $status_active = Session::$STATUS_ACTIVE;
            $status_idle = Session::$STATUS_IDLE;
            $uniq = SIMAHtml::uniqid();
            
            switch($this->active) 
            {    
                case '0':       
                    $cond = "ss$uniq.status='$status_active'"; break;
                case '1': 
                    $cond = "ss$uniq.status='$status_idle'"; break;
                case '2': 
                    $cond = "ss$uniq.status is null"; break;
                default: 
                    $cond = ""; break;
            }
            
            $temp_cond = new CDbCriteria();
            $temp_cond->join = "left join $session_status_view_name ss$uniq on $alias.id=ss$uniq.id";
            $temp_cond->condition=$cond;
            $condition->mergeWith($temp_cond);
        }        
    }
    
    public function getTypesArray()
    {
        $type_displays = [
            Session::$TYPE_SIMADesktopApp => Yii::t('BaseModule.Session', 'TYPE_SIMADesktopApp'),
            Session::$TYPE_SIMAapp => Yii::t('BaseModule.Session', 'TYPE_SIMAapp'),
            Session::$TYPE_SIMAweb => Yii::t('BaseModule.Session', 'TYPE_SIMAweb'),
            Session::$TYPE_SMB => Yii::t('BaseModule.Session', 'TYPE_SMB'),
            Session::$TYPE_SIMAMobileApp => Yii::t('BaseModule.Session', 'TYPE_SIMAMobileApp'),
        ];
        return $type_displays;
    }
}