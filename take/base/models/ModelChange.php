<?php

class ModelChange extends SIMAActiveRecord
{
    public static $CHANGE_TYPE_INSERT = 'INSERT';
    public static $CHANGE_TYPE_UPDATE = 'UPDATE';
    public static $CHANGE_TYPE_DELETE = 'DELETE';
    
    private $_actual_model = null;
    
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function moduleName()
    {
        return 'base';
    }
    
    public function tableName()
    {
        return 'base.model_changes';
    }
    
    public function __get($column)
    {
    	switch ($column)
    	{
            case 'DisplayName':
            case 'SearchName':
                $result = 'Unknown';
                if(isset($this->model_name) && isset($this->model_id))
                {
                    if($this->change_type === ModelChanges::$CHANGE_TYPE_DELETE)
                    {
                        $result = 'Deleted';
                    }
                    else
                    {
                        $model = $this->actual_model;

                        if(empty($model))
                        {
                            $result = 'NotFound';
                        }
                        else
                        {
                            $result = $model->DisplayName;
                        }
                    }
                }
                return $result;
            default: return parent::__get($column);
    	}
    }
    
    public function getactual_model()
    {
        if(empty($this->_actual_model) && isset($this->model_name) && isset($this->model_id))
        {
            $model_name = $this->model_name;
            $this->_actual_model = $model_name::model()->findByPk($this->model_id);
        }
        
        return $this->_actual_model;
    }
    
    public function rules()
    {
    	return [
            ['model_name, model_id, change_type, change_columns', 'required'],
            ['user_id', 'safe']
        ];
    }
    
    public function relations($child_relations = [])
    {
        return [
            'user' => [self::BELONGS_TO, 'User', 'user_id']
        ];
    }
    
    public function modelSettings($column) 
    {
        switch ($column)
        {
            case 'filters' : return [
                'model_name' => 'text',
                'model_id' => 'integer',
                'change_type' => 'dropdown',
                'change_time' => 'date_time_range',
                'user_id' => 'integer',
                'user' => 'relation'
            ];
            case 'options': return [];
            default: return parent::modelSettings($column);
        }
    }
}
