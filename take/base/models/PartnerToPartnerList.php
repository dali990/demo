<?php

class PartnerToPartnerList extends SIMAActiveRecord
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
 
    public function tableName()
    {
        return 'base.partners_to_partner_lists';
    }
    
    public function moduleName()
    {
        return 'base';
    }
    
    public function rules()
    {
        return [
            ['partner_id, partner_list_id', 'required'],
            ['partner_id','unique_with','with' => ['partner_list_id']]
        ];
    }
    
    public function relations($child_relations = [])
    {
        return [
            'partner' => [self::BELONGS_TO, 'Partner', 'partner_id'],
            'partner_list' => [self::BELONGS_TO, 'PartnerList', 'partner_list_id']
        ];
    }
    
    public function modelSettings($column)
    {
        $options = [];
        
        if (Yii::app()->isWebApplication() && !empty($this->partner_list_id))
        {
            $partner_list = $this->partner_list; //Sasa A. - mora ovako u promenljivu, jer ne mozemo da pitamo empty($this->partner_list) jer se resetuje model,
            //i ponisti se init data ako je zadat preko forme
            if (!empty($partner_list) && !$partner_list->is_system && $partner_list->hasUserAccessToList())
            {
                $options[] = 'form';
                $options[] = 'delete';
            }
        }
        
        switch ($column)
        {
            case 'filters' : return array_merge(parent::modelSettings('filters'), [
                'partner'=>'relation',
                'partner_list'=>'relation'
            ]);
            case 'options' : return $options;
            case 'update_relations': return [
                'partner'
            ];
            default: return parent::modelSettings($column);
        }
    }
    
    public static function add(PartnerList $partner_list, Partner $partner)
    {
        return PartnerToPartnerList::addByIds($partner_list->id, $partner->id);
    }
    
    public static function addByIds($partner_list_id, $partner_id)
    {
        $partner_to_partner_list = PartnerToPartnerList::model()->findByAttributes([
            'partner_list_id' => $partner_list_id,
            'partner_id' => $partner_id
        ]);

        if (empty($partner_to_partner_list))
        {
            $partner_to_partner_list = new PartnerToPartnerList();
            $partner_to_partner_list->partner_list_id = $partner_list_id;
            $partner_to_partner_list->partner_id = $partner_id;
            $partner_to_partner_list->save();
        }
        
        return $partner_to_partner_list;
    }
    
    public function remove(PartnerList $partner_list, Partner $partner)
    {
        $partner_to_partner_list = PartnerToPartnerList::model()->findByAttributes([
            'partner_list_id' => $partner_list->id,
            'partner_id' => $partner->id
        ]);

        if (!empty($partner_to_partner_list))
        {
            $partner_to_partner_list->delete();
        }
    }
}
