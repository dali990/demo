<?php

class ActiveSession extends Session
{
    
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function defaultScope()
    {
        $alias = $this->getTableAlias(FALSE,FALSE);
        $session_status_view_name = SessionStatus::model()->tableName();
        $uniq = SIMAHtml::uniqid();
        
        $condition = "$alias.id in (select ss$uniq.id from $session_status_view_name ss$uniq)";
        
//        if(Yii::app()->hasModule('mobile'))
//        {
            /**
             * stavljen je OR TYPE_SIMAMobileApp jer se sesije koriste u pozivu user->isGuest
             * pa da SMA sesije uvek budu aktivne
             * jer se za svaku SMA kreira po jedna trajna sesija
             */
            $TYPE_SIMAMobileApp = Session::$TYPE_SIMAMobileApp;
            $authenticated_device_table = AuthenticatedDevice::model()->tableName();
            $condition .= " OR ($alias.session_type='$TYPE_SIMAMobileApp'"
                                . " AND EXISTS (SELECT 1 "
                                                . "FROM $authenticated_device_table ad$uniq "
                                                . "WHERE ad$uniq.session_id=$alias.id))";
//        }
        
        return [
            'condition' => $condition
        ];
    }

    public function scopes()
    {
        $alias = $this->getTableAlias(FALSE,FALSE);
        $session_status_view_name = SessionStatus::model()->tableName();        
        $uniq = SIMAHtml::uniqid();
        
        return array(            
            'withStatus'=>[
                'join' => "join $session_status_view_name ss$uniq on $alias.id=ss$uniq.id"                
            ],
        );
    }
}