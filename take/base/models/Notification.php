<?php

class Notification extends SIMAActiveRecord
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'public.notifications';
    }
    
    public function moduleName()
    {
        return 'base';
    }
    
    public function __get($column)
    {
        switch ($column)
        {
            case 'DisplayName': return $this->title;
            case 'SearchName': return $this->DisplayName;
//     		case 'path': 		return 'site/todo';
            default: return parent::__get($column);
        }
    }

    public function relations($child_relations = [])
    {
        return [
            'user' => [self::BELONGS_TO, User::class, 'user_id']
        ];
    }

    public function rules()
    {
        return array(
//     			array('assign_to_id, name', 'required'),
            array('user_id, seen, scopes', 'safe'),
//     			array('comment_thread_id, assign_to_id, creator_id, todo_status_id, todo_priority_id','default',
//     					'value'=>null,
//     					'setOnEmpty'=>true,'on'=>array('insert','update')),
        );
    }

    public function getClasses()
    {
        return parent::getClasses() .
                (($this->seen) ? ' notification_seen ' : ' notification_unseen');
    }

    public function attributeLabels()
    {
        return array(
            'options' => 'Opcije',
//    			'text'=>'Tekst',
            'create_time' => 'Vreme',
            'title' => 'Tip notifikacija',
            'display_model_name' => 'Ime modela notifikacije',
            'seen' => 'Procitano',
//     			'todo_status_id'=>'Status',
//     			'todo_priority_id'=>'Prioritet'
            'additional_info' => Yii::t('BaseModule.Notification', 'AdditionalInformation')
        );
    }

    protected static function getJSONColumns()
    {
        return array('params');
    }

        public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters' : return array(
                    'title' => 'text',
//                    'display_model_name' => 'text',
                    'create_time' => 'date_time_range',
                    'seen' => ['dropdown','func'=>'filter_seen'],  
                    'user_id' => 'dropdown',
                    'user' => 'relation'
                );
            case 'textSearch' : return array(
                    'title' => 'text',
//                    'display_model_name' => 'text',
                );
            case 'options' : return array(
                'open_new_tab_notification',
                'form'
            );
            case 'multiselect_options':
                return ['mark_notifications_as_read', 'mark_notifications_as_unread'];
            default: return parent::modelSettings($column);
        }
    }
    
    public function filter_seen($condition) 
    {        
        if($this->seen !== '') 
        {
            $alias = $this->getTableAlias(FALSE,FALSE);            
            
            switch($this->seen) 
            {    
                case '0':       
                    $cond = "$alias.seen=false"; break;
                case '1': 
                    $cond = "$alias.seen=true"; break;                
                default: 
                    $cond = ""; break;
            }
            
            $temp_cond = new CDbCriteria();            
            $temp_cond->condition=$cond;
            $condition->mergeWith($temp_cond);
        }        
    }

    public function scopes()
    {
        $alias = $this->getTableAlias();

        return [
            'byTime' => ['order' => 'create_time DESC'],
            'byName' => ['order' => 'create_time DESC'],
            'onlyUnseen' => [
                'condition' => "$alias.seen = false"
            ],
            'onlyRealUnseen' => [
                'condition' => "$alias.seen_time is null"
            ]
        ];
    }

    public function droplist($key)
    {
        switch ($key)
        {
            case 'user_id': return Employee::model()->getModelDropList();
            case 'title': return array(
//                'Postali ste odgovorni za fajl' => 'Postali ste odgovorni za fajl', 
//                'Dodati su komentari kao opcija' => 'Dodati su komentari kao opcija', 
//                'Pristigla nova poruka!' => 'Pristigla nova poruka!', 
                'Dodati ste da osluškujete komentare' => 'Dodati ste da osluškujete komentare', 
                'Nov/i komentar/i' => 'Nov/i komentar/i', 
                'Postali ste odgovorni' => 'Postali ste odgovorni'
            );
            case 'seen': return array('0' => 'Nije', '1' => 'Jeste');
            default: return parent::droplist($key);
        }
    }

    public function countUnseen()
    {
        return Notification::model()->countByAttributes(array('user_id' => Yii::app()->user->id, 'seen' => false));
    }

    public function getUnseen()
    {
        return Notification::model()->findAllByAttributes(array('user_id' => Yii::app()->user->id, 'seen' => false));
    }

//    public function getAttributeDisplay($column)
//    {
//        switch ($column)
//        {
//            
//            default: return parent::getAttributeDisplay($column);
//        }
//    }
    
    public function getInfoText()
    {
        $text = '';
        if (!empty($this->info))
        {
            $text = $this->info;
        }
        else
        {
            $params = $this->params;
            $text = isset($params['info']) ? $params['info'] : '';
        }
        
        return $text;
    }
    
    public function forUser($user_id)
    {
        $uniq = SIMAHtml::uniqid();
        $alias = $this->getTableAlias();
        
        $this->getDbCriteria()->mergeWith([
            'condition'=> "$alias.user_id = :user_id$uniq",
            'params' => [
                ":user_id$uniq" => $user_id,
            ]
        ]);

        return $this;
    }

    public function forActions(array $actions)
    {
        $alias = $this->getTableAlias();
        $condition = "";
        foreach ($actions as $key => $action)
        {
            if ($key !== 0)
            {
                $condition .= ' or ';
            }
            
            $condition .= "(
                (($alias.params::json)->>'action')::json->>'action' = '{$action['action']}' and 
            ";
            if (is_array($action['action_id']))
            {
                $condition .= "
                    ((($alias.params::json)->>'action')::json->>'action_id')::json->>'model_name' = '{$action['action_id']['model_name']}' and
                    ((($alias.params::json)->>'action')::json->>'action_id')::json->>'model_id' = '{$action['action_id']['model_id']}'
                )";
            }
            else
            {
                $condition .= "
                    (($alias.params::json)->>'action')::json->>'action_id' = '{$action['action_id']}'
                )";
            }
        }

        $this->getDbCriteria()->mergeWith([
            'condition'=> $condition
        ]);

        return $this;
    }
}
