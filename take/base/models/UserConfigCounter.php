<?php 

class UserConfigCounter extends SIMAActiveRecord
{
    public static $INTERVAL_YEAR = 'INTERVAL_YEAR';
    public static $INTERVAL_MONTH = 'INTERVAL_MONTH';
    
    public static $interval_types = [
        'INTERVAL_YEAR' => 'Godina',
        'INTERVAL_MONTH' => 'Mesec',
    ];
    
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
 
    public function tableName()
    {
        return 'base.user_config_counter';
    }
    
    public function moduleName()
    {
        return 'base';
    }
    
    public function __get($column)
    {
    	switch ($column)
    	{
    		case 'DisplayName': 	
    		case 'SearchName':      return $this->name.' '.$this->template;
    		default: 		return parent::__get($column);
    	}
    }
    
    public function scopes()
    {
        $alias = $this->getTableAlias();
        return [
            'byName' => [
                'order' => "$alias.name"
            ]
        ];
    }
    
    public function rules()
    {
    	return array(
            array('name, time_interval', 'required'),
            array('template, note', 'safe')
    	);
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'options': return ['open','form','delete'];
            case 'filters' : return [
                'name' => 'text',
                'time_interval' => 'dropdown',
                'note' => 'text',
            ];
            case 'textSearch': return array(
                'name' => 'text',
                'note' => 'text',
            ); 
            case 'default_order_scope': return 'byName';
            default: return parent::modelSettings($column);
        }
    }
    
    
    /**
     * Vraca string sa sledecim brojem provucenim kroz template
     * @param Day $day
     */
    public function getNextText(Day $day, $just_calc = false)
    {
        $_temp_text = '';
        if ($this->time_interval === self::$INTERVAL_MONTH)
        {
            $counter_item = UserConfigCounterItem::model()->findByAttributes([
                'user_config_counter_id' => $this->id,
                'month_id' => $day->month->id
            ]);
            if (is_null($counter_item))
            {
                $counter_item = new UserConfigCounterItem();
                $counter_item->user_config_counter_id = $this->id;
                $counter_item->month_id = $day->month->id;
                $counter_item->last_taken_value = 0;
            }
            $_new_value = $counter_item->last_taken_value + 1;
            if (!$just_calc)
            {
                $counter_item->last_taken_value = $_new_value;
            }
            $counter_item->save();
            
            $_temp_text = trim(str_replace([
                '{number}',
                '{month}',
                '{month_full}',
                '{year}',
                '{year_full}'
            ], [
                $_new_value,
                $day->month->month,
                str_pad($day->month->month, 2, '0', STR_PAD_LEFT),
                substr($day->month->year->year, 2, 2),
                $day->month->year->year
            ], $this->template));
        }
        else
        {
            $counter_item = UserConfigCounterItem::model()->findByAttributes([
                'user_config_counter_id' => $this->id,
                'year_id' => $day->month->year_id
            ]);
            if (is_null($counter_item))
            {
                $counter_item = new UserConfigCounterItem();
                $counter_item->user_config_counter_id = $this->id;
                $counter_item->year_id = $day->month->year_id;
                $counter_item->last_taken_value = 0;
            }
            $_new_value = $counter_item->last_taken_value + 1;
            if (!$just_calc)
            {
                $counter_item->last_taken_value = $_new_value;
            }
            $counter_item->save();
            $_temp_text = trim(str_replace([
                '{number}',
                '{year}',
                '{year_full}'
            ], [
                $_new_value,
                substr($day->month->year->year, 2, 2),
                $day->month->year->year
            ], $this->template));
        }
        return $_temp_text;
    }
    
    
}