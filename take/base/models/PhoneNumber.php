<?php

class PhoneNumber extends SIMAActiveRecord 
{    
    const PROVIDER_DELIMITER   = " / ";
    const PHONE_DELIMITER      = "-";
    
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'base.phone_numbers';
    }
    
    public function moduleName()
    {
        return 'base';
    }
    
    public function __get($column)
    {
        switch ($column)
        {
            case 'DisplayName': 
                $display_name = $this->display_name;
                if (!empty($this->type))
                {
                    $display_name = "{$this->type->DisplayName}: $display_name";
                }

                return $display_name;
            case 'SearchName': return $this->display_name;
            default: return parent::__get($column);
        }
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters' : return [
                'country' => 'relation',
                'provider_number' => 'integer',
                'phone_number'=>'text',
                'comment' => 'text'
            ];
            case 'textSearch': return [
                'display_name_only_numbers' => 'text',
                'display_name' => 'text'
            ];
            default: return parent::modelSettings($column);
        }
    }
    
    public function relations($child_relations = [])
    {
        return [
            'partner' => [self::BELONGS_TO, 'Partner', 'partner_id'],
            'country' => [self::BELONGS_TO, 'Country', 'country_id'],
            'type'    => [self::BELONGS_TO, 'PhoneNumberType', 'type_id'],
        ];
    }

    public function rules()
    {
        return [
            ['country_id, provider_number, phone_number, type_id, partner_id', 'required'],
            ['comment, display_name_only_numbers', 'safe'],
            ['country_id, provider_number, phone_number, type_id', 'numerical', 'integerOnly'=>true],
            ['country_id', 'checkCountryProviderPhoneUnique']
        ];
    }
        
    public function afterValidate() 
    {      
        parent::afterValidate();
        $this->display_name = $this->getFullDisplayName();        
    }
    
    public function checkCountryProviderPhoneUnique()
    {
        if (
                $this->columnChangedOrNew('country_id')
                ||
                $this->columnChangedOrNew('provider_number')
                ||
                $this->columnChangedOrNew('phone_number')
                ||
                $this->columnChangedOrNew('partner_id')
//                intval($this->__old['country_id']) !== intval($this->country_id) ||
//                intval($this->__old['provider_number']) !== intval($this->provider_number) ||
//                intval($this->__old['phone_number']) !== intval($this->phone_number) || 
//                intval($this->__old['partner_id']) !== intval($this->partner_id)
           )
        {
            $phone_number = PhoneNumber::model()->findByAttributes([
                'country_id'     => $this->country_id,
                'provider_number'=> $this->provider_number,
                'phone_number'   => $this->phone_number
            ]);
            if (!is_null($phone_number))
            {
                $this->addError('phone_number', Yii::t('BaseModule.PhoneNumber', 'PhoneNumberAlreadyExist'));
            }
        }
    } 
       
    public function getFullDisplayName() 
    {
        if(empty($this->country) || !isset($this->provider_number) || !isset($this->phone_number))
        {
            return '';
        }
        
        $chunks     = str_split($this->phone_number, 3);
        $chunks_cnt = count($chunks);
        
        if ($chunks_cnt > 1 && strlen($chunks[$chunks_cnt-1]) === 1) 
        {
            $last_num = $chunks[$chunks_cnt-2][2];
            $chunks[$chunks_cnt-2] = substr($chunks[$chunks_cnt-2], 0, -1);
            $chunks[$chunks_cnt-1] = $last_num . $chunks[$chunks_cnt-1];
        }
        $phone_number = implode(self::PHONE_DELIMITER, $chunks);
        
        return '('.$this->country->area_code.') '.$this->provider_number.self::PROVIDER_DELIMITER.$phone_number;
    }
    
    public function beforeSave() 
    {
        if (
                $this->columnChangedOrNew('country_id')
                ||
                $this->columnChangedOrNew('provider_number')
                ||
                $this->columnChangedOrNew('phone_number')
//                intval($this->__old['country_id']) !== intval($this->country_id) ||
//                intval($this->__old['provider_number']) !== intval($this->provider_number) ||
//                intval($this->__old['phone_number']) !== intval($this->phone_number)
           )
        {
            //privremena kolona koja sluzi za text pretragu
            $this->display_name_only_numbers = "{$this->country->area_code}{$this->provider_number}{$this->phone_number} 0{$this->provider_number}{$this->phone_number}";
        }
        
        return parent::beforeSave();
    }
    
    public function beforeDelete() 
    {
        if($this->partner->main_phone_number_id === $this->id)
        {
            $this->partner->main_phone_number_id = null;
            $this->partner->save();
        }

        return parent::beforeDelete();
    }
    
    public function afterValidateDependencyError(array $foreign_dependencies)
    {
        $allowed_tables = [
            Partner::model()->tableName().':main_phone_number_id'
        ];

        $parsed_foreign_dependencies = array_diff($foreign_dependencies, $allowed_tables);
        
        parent::afterValidateDependencyError($parsed_foreign_dependencies);
    }
}