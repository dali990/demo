<?php

class Month extends SIMAActiveRecord
{
    public $period = null;
    public $month_range = null;
    
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'base.months';
    }
    
    public function moduleName()
    {
        return 'base';
    }
    
    public function __get($column)
    {
        switch ($column)
        {
            case 'SearchName':
            case 'DisplayName': return $this->display_name;
            case 'filter_period_string':
                $fist_month_day = $this->firstDay()->day_date;
                $last_month_day = $this->lastDay()->day_date;
                $period = $fist_month_day.'<>'.$last_month_day;
                return $period;
            default: return parent::__get($column);
        }
    }
    
    public function relations($child_relations = [])
    {
        return parent::relations(array_merge([
            'year' => array(self::BELONGS_TO, 'Year', 'year_id'),
        ],$child_relations));
    }
    
    public function scopes()
    {
        $alias = $this->getTableAlias();
        
        $scopes = [
            'byName' => ['order' => $alias.'.display_name'],
            'byNameDESC' => ['order' => $alias.'.display_name DESC']
        ];
        
        if (Yii::app()->hasModule('accounting'))
        {
            $uniq = SIMAHtml::uniqid();
            $accounting_month_tbl = AccountingMonth::model()->tableName();
            
            $scopes['onlyWithAccountingMonth'] = [
                'condition' => "exists (select 1 from $accounting_month_tbl am$uniq where am$uniq.id=$alias.id)"
            ];
        }
        
        return $scopes;
    }
    
    public function rules()
    {
        return array(
            array('month', 'numerical', 'integerOnly' => TRUE,'max' => 12,'min' => 1)
        );
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'options': return [];
            case 'filters': return [
                'month' => 'dropdown',
                'year' => 'relation',
                'display_name' => 'text',
                'period' => ['func' => 'filter_period'],
                'month_range' => ['date_time_range', 'func' => 'filter_month_range'],
                'abmonth' => 'relation'
            ];
            case 'textSearch' : return [
                'display_name' => 'text',
            ];
            case 'default_order_scope': return 'byNameDESC';
            case 'default_order': return 'display_name';
            case 'searchField': return array(
                'scopes' => array('byName')
            );
            default: return parent::modelSettings($column);
        }
    }
    
    public function filter_month_range($condition, $alias)
    {
        if (isset($this->month_range))
        {
            $formated_range = SIMAHtml::FormatDateRangeParam($this->month_range);
            
            $startDate = $formated_range['startDate'];
            $endDate = $formated_range['endDate'];
            
            $startDay = Day::getByDate(SIMAHtml::UserToDbDate($startDate));
                        
            $endDay = Day::getByDate(SIMAHtml::UserToDbDate($endDate));
            
            $first_month = $startDay->getYearNumber().'-'.str_pad($startDay->getMonthNumber(), 2, '0', STR_PAD_LEFT);
            $second_month = $endDay->getYearNumber().'-'.str_pad($endDay->getMonthNumber(), 2, '0', STR_PAD_LEFT);
                        
            $temp_condition = new CDbCriteria();
            $temp_condition->condition = "$alias.display_name >= :fist_month AND $alias.display_name <= :second_month";
            $temp_condition->params = [
                ":fist_month" => $first_month, 
                ":second_month" => $second_month
            ];

            $condition->mergeWith($temp_condition);
        }
    }
    
    public function filter_period($condition, $alias)
    {
        if (isset($this->period))
        {
            if(gettype($this->period)==='array' && count($this->period)===2
                    && is_object($this->period[0]) && get_class($this->period[0])==='Month' 
                    && is_object($this->period[1]) && get_class($this->period[1])==='Month')
            {
                $first_month = $this->period[0]->display_name;
                $second_month = $this->period[1]->display_name;
                
                $temp_condition = new CDbCriteria();
                $temp_condition->condition = "$alias.display_name >= :fist_month AND $alias.display_name <= :second_month";
                $temp_condition->params = [
                    ":fist_month" => $first_month, 
                    ":second_month" => $second_month
                ];
                
                $condition->mergeWith($temp_condition);
            }
            else if(gettype($this->period)==='array' && count($this->period)===2
                    && is_string($this->period[0]) && in_array($this->period[0], [
                        '>', '>=', '=', '<', '<='
                    ])
                    && is_object($this->period[1]) && get_class($this->period[1])==='Month')
            {
                $operator = $this->period[0];
                $month = $this->period[1]->display_name;
                
                $temp_condition = new CDbCriteria();
                $temp_condition->condition = "$alias.display_name $operator :month";
                $temp_condition->params = [
                    ":month" => $month
                ];
                
                $condition->mergeWith($temp_condition);
            }
            else
            {
                throw new Exception('invalid param type: '.CJSON::encode($this->period));
            }
        }
    }
    
    /**
     * 
     * @param string/int $month
     * @param string/int/Year $year
     * @return \Month
     * @throws Exception
     */
    public static function get($month = null, $year = null)
    {
        if ($month === null)
        {
            $month = date('m');
        }
        if (!is_null($year) && gettype($year)==='object' && get_class($year)==='Year')
        {
            $yearModel = $year;
            $year = $year->year;
        }
        else
        {
            $yearModel = Year::get($year);
        }
        
        if (!is_numeric($month))
        {
            throw new Exception("Month::get($month, $year) -> mesec nije broj");
        }
        
        if (!isset($yearModel))
        {
            throw new Exception("Month::get($month, $year) -> godina ne postoji");
        }
        $criteria = new SIMADbCriteria([
            'Model' => 'Month',
            'model_filter' => [
                'month' => $month,
                'year'=>array(
                    'ids' => $yearModel->id
                )
            ]
        ]);
        $res = Month::model()->find($criteria);
        if (is_null($res))
        {
            $res = new Month();
            $res->month = $month;
            $res->year_id = $yearModel->id;
            $res->save();            
            $res->refresh();
        }
        
        return $res;
    }
    
    public static function current()
    {
        return Month::get();
    }
    
    public function prev()
    {
        $prev = intval($this->month)-1;
        $year = $this->year->year;
        if ($prev===0)
        {
            $prev = 12;
            $year--;
        }
        return $this->get($prev,$year);
    }
    
    public function next()
    {
        $next = intval($this->month)+1;
        $year = $this->year->year;
        if ($next===13)
        {
            $next = 1;
            $year++;
        }
        return $this->get($next,$year);
    }
    
    public function firstDay()
    {
        return Day::get(1,$this,$this->year);
    }
    
    public function lastDay()
    {
        return Day::get(cal_days_in_month(CAL_GREGORIAN,$this->month,$this->year->year),$this,$this->year);
    }
    
    public function param($name)
    {
        $_ar = explode('.', $name);
        if (count($_ar)<2)
        {
            throw new Exception("Month::param($name) -> parametar nije dobar");
        }
        $relation = $_ar[0];
        $param = $_ar[1];
        
        $relations = $this->relations();
        if (!isset($relations[$relation]))
        {
            throw new Exception("Month::param($name) -> relacija $relation ne postoji");
        }
        if (!isset($this->$relation))
        {
            $Model = $relations[$relation][1];
            $name = $Model::model()->modelLabel();
            throw new SIMAWarnException("Month::param($name) -> parametar ne moze biti dovucen jer nije dodata $name - ".$this->month);
        }

        return $this->$relation->$param;
    }
    
    public function __toString()
    {
        return (string)($this->DisplayName);
    }
    
    public static function previousNmonths($limitN, Month $month=null)
    {
        $months = array();
        
        if(!isset($month))
        {
            $month = Month::current();
        }
        for($i=0; $i<$limitN; $i++)
        {
            $month = $month->prev();
            $months[] = $month;
        }
        
        return $months;
    }
    
    public static function getFromToMonths(Month $fromMonth, Month $toMonth)
    {
        if($fromMonth->display_name > $toMonth->display_name)
        {
            throw new Exception('frommonth '.$fromMonth->display_name.' is after tomonth '.$toMonth->display_name);
        }
        
        $months = array($fromMonth);
        
        $currentMonth = $fromMonth;
        while($currentMonth->id !== $toMonth->id)
        {
            $currentMonth = $currentMonth->next();
            $months[] = $currentMonth;
        }
        
        return $months;
    }
    
    public function countWorkDays()
    {        
        $this->assureExistanceOfDays();
                
        $criteria = new CDbCriteria();
        $criteria->select = 'COUNT(*) as cnt';
        $criteria->join = 'left join '.DayType::model()->tableName().' dt on t.day_type_id=dt.id';
        $criteria->condition = "date_part('month', t.day_date)=".$this->month
                . " and date_part('year', t.day_date)=".$this->year->year
                . " and dt.id=".Yii::app()->configManager->get('base.work_day_type_id');
                
        $result = Day::model()->count($criteria);
                
        return $result;
    }
    
    public function countDays()
    {
        $this->assureExistanceOfDays();
            
        $criteria = new CDbCriteria();
        $criteria->select = 'COUNT(*) as cnt';
        $criteria->condition = "date_part('month', t.day_date)=".$this->month
                . " and date_part('year', t.day_date)=".$this->year->year;
                
        $result = Day::model()->count($criteria);
                
        return $result;
    }
    
//    public function countHolidays()
//    {
//        $this->assureExistanceOfDays();
//                
//        $criteria = new CDbCriteria();
////        $criteria->select = 'sum(dt.work_hours) as sum';
//        $criteria->select = 'COUNT(*) as cnt';
//        $criteria->join = 'left join '.DayType::model()->tableName().' dt on t.day_type_id=dt.id';
//        $criteria->condition = "date_part('month', t.day_date)=".$this->month
//                . " and date_part('year', t.day_date)=".$this->year->year
//                . " and dt.id=".Yii::app()->configManager->get('base.holiday_day_type_id');
//                
//        $result = Day::model()->count($criteria);
////        $result = $dbResult->cnt;
//                
//        return $result;
//    }
    
    /**
     * vraca koliko u mesecu ima dana (subota, nedelja) vikenda
     * @param type $exclude_holidays - ukoliko se prosledi true ne broji dane koji su i praznik
     * @throws Exception
     */
    public function countWeekendDays($exclude_holidays=false)
    {
        $this->assureExistanceOfDays();
        
        $result = null;
        if($exclude_holidays === true)
        {
            $criteria = new CDbCriteria();
            $criteria->select = 'COUNT(*) as cnt';
            $criteria->join = 'left join '.DayType::model()->tableName().' dt on t.day_type_id=dt.id';
            $criteria->condition = "date_part('month', t.day_date)=".$this->month
                    . " and date_part('year', t.day_date)=".$this->year->year
                    . " and dt.id=".Yii::app()->configManager->get('base.weekend_day_type_id');

            $result = Day::model()->count($criteria);
        }
        else
        {
            $startDate = new DateTime($this->firstDay()->day_date);
            $endDate = new DateTime($this->lastDay()->next()->day_date);
            
            $weekend_days = 0;

            while($startDate->diff($endDate)->days > 0) 
            {
                $dayOfWeek = $startDate->format('N');
                if($dayOfWeek === '6' || $dayOfWeek === '7')
                {
                    $weekend_days++;
                }
                
                $startDate = $startDate->add(new \DateInterval("P1D"));
            }
            
            $result = $weekend_days;
        }
                
        return $result;
    }
    
    public function countWorkHours()
    {        
        $this->assureExistanceOfDays();
        
        $criteria = new CDbCriteria();
        $criteria->select = 'sum(dt.work_hours) as sum';
        $criteria->join = 'left join '.DayType::model()->tableName().' dt on t.day_type_id=dt.id';
        $criteria->condition = "date_part('month', t.day_date)=".$this->month
                . " and date_part('year', t.day_date)=".$this->year->year;
//                . " and dt.id=".Yii::app()->configManager->get('base.holiday_day_type_id');
                
        $dbResult = Day::model()->find($criteria);
        $result = $dbResult->sum;
                
        return $result;
    }
    
    /**
     * obezbedjivanje da su uneti svi meseci u trenutnom mesecu
     * tako sto se dohvata prvi dan sledeceg meseca 
     * sto unosi sve prethodne nepostojece
     */
    private function assureExistanceOfDays()
    {
        $nextMonth = $this->next();
        $nextMonth->firstDay();
        
        $first_day = $this->firstDay();
        $last_day = $this->lastDay();
        $days = Day::getBetweenDates($first_day, $last_day, true);
        $days_count = count($days);
        $start_day_date = new DateTime($first_day);
        $daysInMonth = $start_day_date->format('t');
        if(intval($daysInMonth) !== intval($days_count))
        {
            for($i=1; $i <= $daysInMonth; $i++)
            {
                Day::get($i, $this->month, $this->year->year);
            }
        }
    }
    
    public function getDays()
    {
        $this->assureExistanceOfDays();
        
        $first_day = $this->firstDay();
        $last_day = $this->lastDay();
        
        $days = Day::getBetweenDates($first_day, $last_day);
        
        return $days;
    }
    
    public function getDaysIds()
    {
        $result = [];
        
        $days = $this->getDays();
        
        foreach($days as $d)
        {
            $result[] = $d->id;
        }
        
        return $result;
    }
    
    /**
     * 
     * @param Month $input_month
     * @return int
     *  $this < $month = -1
     *  $this == $month = 0
     *  $this > $month = 1
     */
    public function compare(Month $input_month)
    {
        $this_first_day = $this->firstDay();
        $input_month_first_day = $input_month->firstDay();
        
        return $this_first_day->compare($input_month_first_day);
    }
}