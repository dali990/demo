<?php

class CompanyToTheme extends SIMAActiveRecord
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
 
    public function tableName()
    {
        return 'base.companies_to_themes';
    }
    
    public function moduleName()
    {
        return 'base';
    }
    
    public function __get($column) 
    {
        switch ($column) {
            case 'DisplayName': return (!empty($this->company) && !empty($this->theme)) ? ("{$this->company->DisplayName}({$this->theme->DisplayName})") : '';
            case 'SearchName': return $this->DisplayName;

            default: return parent::__get($column);
        }
    }
  
    public function rules()
    {
    	return [
            ['company_id, theme_id', 'required'],
            ['contract_id', 'safe'],
            ['company_id', 'unique_with', 'with' => ['theme_id']],
    	];
    }
    
    public function relations($child_relations = [])
    {
        return array(
            'company' => [self::BELONGS_TO, 'Company', 'company_id'],
            'theme' => [self::BELONGS_TO, 'Theme', 'theme_id'],
            'contract' => [self::BELONGS_TO, 'Contract', 'contract_id'],
        );
    }

    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'options': return ['form', 'delete'];
            case 'filters': return array_merge(parent::modelSettings($column),[
                'company' => 'relation',
                'theme' => 'relation',
                'contract' => 'relation'
            ]);
            case 'textSearch': return [
                'company' => 'relation',
                'theme' => 'relation'
            ];
            case 'update_relations': return ['company', 'theme'];
            default: return parent::modelSettings($column);
        }
    }
}
