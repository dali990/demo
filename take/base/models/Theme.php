<?php

class Theme extends SIMAActiveRecord
{
    public static $PLAIN = 'PLAIN';
    public static $JOB = 'JOB';
    public static $BID = 'BID';
    public static $BAF = 'BAF';
    
    public static $ACTIVE = 'ACTIVE';
    public static $NOT_ACTIVE = 'NOT_ACTIVE';
    
    public $display_name_path;
    public $display_name_full;
    public $parent_ids;
    public $children_ids;
    
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'base.themes';
    }
    
    public function moduleName()
    {
        return 'base';
    }
    
    public function __get($column)
    {
        switch ($column)
        {
            case 'DisplayName': return $this->display_name;
            case 'SearchName': return $this->display_name_with_code;
            case 'isActive': return $this->status === Theme::$ACTIVE;
            default: return parent::__get($column);
        }
    }
    
    public function rules()
    {
        return array(
            array('name', 'required'),
            ['name', 'length', 'max' => 100],
//            ['name', 'checkNameLength'],
            array('parent_id, coordinator_id, job_type_id, status, settings, comment', 'safe'),
            ['geo_objects_list','safe'],
            ['parent_id', 'checkTypeRules', 'on' => ['insert', 'update']],
            ['parent_id', 'checkParentCycle', 'on' => ['insert', 'update']],
            ['job_type_id', 'check_job_type_id_parent', 'on' => ['insert', 'update']],
            ['status', 'checkStatus', 'on' => ['insert', 'update']],
            array('coordinator_id, parent_id, cost_location_id, parent_id', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => array('insert', 'update')),
            ['id','canDelete','on'=>['delete']]
        );
    }
    
    public function canDelete()
    {
        if ($this->tasks_count > 0)
        {
            $this->addError('id', 'ne mozete obrisati temu sa zadatkom');
        }
        if ($this->children_count > 0)
        {
            $this->addError('id', 'ne mozete obrisati temu koja ima podteme');
        }
        $crit = new SIMADbCriteria([
            'Model' => 'File',
            'model_filter' => [
                'belongs' => $this->tag->queryR
            ]
        ]);
        $taks_cnt = File::model()->count($crit);
        if ($taks_cnt > 0)
        {
            $this->addError('id', 'ne mozete obrisati temu koja ima fajl');
        }
        $cl = CostLocation::model()->findByPk($this->owner->cost_location_id);
        $cl->setScenario('delete');
        try
        {
            if ($cl->validate() !== true)
            {
//                $this->addError('id', 'ne mozete obrisati temu kojoj mesto troska ima teret');
                $this->addErrors($cl->getErrors());
            }
        }
        catch (SIMAWarnException $e)
        {
//            $this->addError('id', 'ne mozete obrisati temu kojoj mesto troska ima teret');
            $this->addError('id', $e->getMessage());
        }
        $job = Job::model()->findByPk($this->id);
        if (!is_null($job))
        {
            $job->setScenario('delete');
            try
            {
                if ($job->validate() !== true)
                {
                    $this->addErrors($job->getErrors());
                }
            }
            catch (SIMAWarnException $e)
            {
                $this->addError('id', 'ne mozete obrisati temu jer posao ne moze da se obrise');
            }
        }
        $bid = Bid::model()->findByPk($this->id);
        if (!is_null($bid))
        {
            $bid->setScenario('delete');
            try
            {
                if ($bid->validate() !== true)
                {
                    $this->addErrors($bid->getErrors());
                }
            }
            catch (SIMAWarnException $e)
            {
                $this->addError('id', 'ne mozete obrisati temu jer posao ne moze da se obrise');
            }
        }
    }
    
//    public function checkNameLength()
//    {        
//        if(!$this->hasErrors() && !empty($this->name))
//        {   
//            if(strlen($this->name))
//            {
//               $this->addError('name', "name len");
//            }
//        }
//    }
    
    public function checkStatus()
    {
        if ($this->columnChanged('status'))
        {
            if ($this->status == Theme::$ACTIVE) //ozivljavanje teme
            {
                if (isset($this->parent) && $this->parent->status == Theme::$NOT_ACTIVE)
                {
                    $this->addError('status', 'Ne mozete da aktivirate Temu ako roditelj nije aktivan');
                }
            }
            else //zaustavljanje teme
            {
                foreach ($this->children as $subtheme) 
                {
                    if ($subtheme->status == Theme::$ACTIVE)
                    {
                        $this->addError('status', 'Ne mozete da deaktivirate Temu ako ima aktivnu decu');
                        break;
                    }
                }
            }
        }
    }
    
    public function checkTypeRules()
    {
        if (!empty($this->parent_id) && $this->type == self::$JOB && $this->parent->type == self::$BID)
        {
            $this->addError('parent_id', Yii::t('BaseModule.Theme','Job cannot be part of Bid'));
        }
    }
    
    
    /**
     * funkcija koja ne dozvoljava da se unese tip posla koji nije usaglasen sa roditeljskim poslom
     */
    public function check_job_type_id_parent() 
    {
        if (!$this->hasErrors()) 
        {
            if ($this->parent != null && $this->parent->job_type_id != null && $this->job_type_id != $this->parent->job_type_id)
                $this->addError('job_type_id', 'Tip posla mora da bude "'. $this->parent->getAttributeDisplay('job_type')
                        .'" zbog nad teme!');
        }
    }
    
    public function scopes()
    {
        $alias = $this->getTableAlias();
        $jobs_table = Job::model()->tableName();
        $job_active_statuses = ActiveJob::getActiveStatusesAsString();
        $uniq = SIMAHtml::uniqid();
        return [
            'withFullPaths' => array(
                'select' => "rt.display_name_path as display_name_path, rt.display_name_full as display_name_full, rt.parent_ids as parent_ids, rt.children_ids as children_ids, $alias.*",
                'join' => "left join base.recursive_themes rt on $alias.id=rt.id"
            ),
            'byDisplayName' => [
                'order' => "$alias.display_name"
            ],
            'onlyActiveJobs' => [
                'join' => "left join $jobs_table jt$uniq on jt$uniq.id=$alias.id",
                'condition' => "jt{$uniq}.status in ($job_active_statuses)"
            ],
            'active' => [
                'condition' => "$alias.status = '".Theme::$ACTIVE."'"
            ],
            'onlyImportantThemes' => [
                'condition' => "$alias.type in ('" . implode("','", array_keys(ThemeGUI::$types)) . "')"
            ]
        ];
    }
    
    /**
     * Korisnik ima pristup temi ako ima privilegiju modelThemeSeeAll ili
     * je clan tima te teme ili clan tima neke njene roditeljske teme. Koordinator je automatski clan tima teme
     * @param integer $user_id
     * @return $this
     */
    public function hasAccess($user_id)
    {
        if (!Yii::app()->user->checkAccess('modelThemeSeeAll'))
        {
            $alias = $this->getTableAlias();
            $_person_to_themes_table = PersonToTheme::model()->tableName();
            $uniq = SIMAHtml::uniqid();

            $this->getDbCriteria()->mergeWith([
                'condition' => "
                    exists (
                        select 1 
                        from base.recursive_themes rt$uniq 
                        where                             
                            $alias.id=rt$uniq.id and
                            exists(
				select 1 from $_person_to_themes_table pt$uniq 
                                where 
                                    (
                                        pt$uniq.theme_id=rt$uniq.id or pt$uniq.theme_id = ANY(rt$uniq.parent_ids)
                                    ) and 
                                    pt$uniq.person_id=:person_id$uniq
                            )
                    )
                ",
                'params' => [":person_id$uniq" => $user_id]
            ]);
        }

        return $this;
    }
    
    /**
     * Korisnik ima super pristup temi ako ima privilegiju modelThemeSeeAll ili
     * je koordinator te teme ili koordinator neke njene roditeljske teme
     * @param integer $user_id
     * @return $this
     */
    public function hasSuperAccess($user_id)
    {
        if (!Yii::app()->user->checkAccess('modelThemeSeeAll'))
        {
            $alias = $this->getTableAlias();
            $themes_table = Theme::model()->tableName();
            $uniq = SIMAHtml::uniqid();

            $this->getDbCriteria()->mergeWith([
                'condition' => "
                    exists (
                        select 1 
                        from base.recursive_themes rt$uniq 
                        where                             
                            $alias.id=rt$uniq.id and
                            exists(
				select 1 from $themes_table t$uniq 
                                where 
                                    (
                                        t$uniq.id=rt$uniq.id or t$uniq.id = ANY(rt$uniq.parent_ids)
                                    ) and 
                                    t$uniq.coordinator_id=:coordinator_id$uniq
                            )
                    )
                ",
                'params' => [":coordinator_id$uniq" => $user_id]
            ]);
        }

        return $this;
    }
    
    /**
     * Korisnik ima pravo da menja temu ako ima privilegiju modelThemeAdmin ili
     * je koordinator te teme ili koordinator neke njene roditeljske teme
     * @param integer $user_id
     * @return $this
     */
    public function hasChangeAccess($user_id)
    {
        if (!Yii::app()->user->checkAccess('modelThemeAdmin'))
        {
            $alias = $this->getTableAlias();
            $themes_table = Theme::model()->tableName();
            $uniq = SIMAHtml::uniqid();

            $this->getDbCriteria()->mergeWith([
                'condition' => "
                    exists (
                        select 1 
                        from base.recursive_themes rt$uniq 
                        where                             
                            $alias.id=rt$uniq.id and
                            exists(
				select 1 from $themes_table t$uniq 
                                where 
                                    (
                                        t$uniq.id=rt$uniq.id or t$uniq.id = ANY(rt$uniq.parent_ids)
                                    ) and 
                                    t$uniq.coordinator_id=:coordinator_id$uniq
                            )
                    )
                ",
                'params' => [":coordinator_id$uniq" => $user_id]
            ]);
        }

        return $this;
    }
    
    public function onlyCurrThemeAndSubThemes($theme_id)
    {
        $alias = $this->getTableAlias();
        $theme = Theme::model()->withFullPaths()->findByPk($theme_id);

        $condition = "$alias.id=$theme_id";
        if (!empty($theme->children_ids))
        {
            $children_ids_string = trim(str_replace(['{','}'], '', $theme->children_ids));
            if (!empty($children_ids_string))
            {
                $condition .= " or $alias.id in ($children_ids_string)";
            }
        }

        $this->getDbCriteria()->mergeWith([
            'condition' => $condition
        ]);

        return $this;
    }
    
    public function onlyCurrThemeAndParentThemes($theme_id)
    {
        $alias = $this->getTableAlias();
        $theme = Theme::model()->withFullPaths()->findByPk($theme_id);

        $condition = "$alias.id=$theme_id";
        if (!empty($theme->parent_ids))
        {
            $parent_ids_string = trim(str_replace(['{','}'], '', $theme->parent_ids));
            if (!empty($parent_ids_string))
            {
                $condition .= " or $alias.id in ($parent_ids_string)";
            }
        }

        $this->getDbCriteria()->mergeWith([
            'condition' => $condition
        ]);

        return $this;
    }
    
    public function relations($child_relations = [])
    {
        $company_to_theme_table_name = CompanyToTheme::model()->tableName();
        return parent::relations(array_merge([
            'parent' => array(self::BELONGS_TO, 'Theme', 'parent_id', 'scopes' => 'withoutTypeBaf'),
            'children' => [self::HAS_MANY, 'Theme', 'parent_id'],
            'children_count' => [self::STAT, 'Theme', 'parent_id', 'select' => 'count(*)'],
            'coordinator' => array(self::BELONGS_TO, 'User', 'coordinator_id'),
            'meetings' => array(self::MANY_MANY, 'Meeting', 'private.meetings_to_themes(meeting_id,theme_id)'),
            'persons_to_teams' => array(self::HAS_MANY, 'PersonToTheme', 'theme_id'),
            'team_count' => array(self::STAT, 'PersonToTheme', 'theme_id','select' => 'count(*)'),
            'team' => array(self::MANY_MANY, 'Person', 'base.persons_to_themes(person_id,theme_id)'),
            'job_type' => array(self::BELONGS_TO, 'JobType', 'job_type_id'),
            'cost_location' => array(self::BELONGS_TO, 'CostLocation', 'cost_location_id'),
            'comment_thread'=>array(self::BELONGS_TO, 'CommentThread', 'comment_thread_id'),
            'comment_threads'=>array(self::HAS_MANY, 'CommentThread', 'model_id', 'condition'=>"model='Theme'"),
            'comment_threads_count'=>array(self::STAT, 'CommentThread', 'model_id', 'condition'=>"model='Theme'", 'select'=>'count(*)'),
            'comment_threads_with_not_system_comments'=>array(self::HAS_MANY, 'CommentThread', 'model_id',
                'condition'=>"model='Theme'",
                'scopes'=>'onlyWithUserComments'
            ),
            'comment_threads_with_not_system_comments_count'=>array(self::STAT, 'CommentThread', 'model_id', 
                'select'=>'count(*)',
                'condition'=>"model='Theme'",
                'scopes'=>'onlyWithUserComments'
            ),
            'companies' => [self::MANY_MANY, 'Company', "$company_to_theme_table_name(theme_id, company_id)"],
            'theme_to_companies' => [self::HAS_MANY, 'CompanyToTheme', 'theme_id'],
        ], $child_relations));
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters': return array(
                'display_name'=>'text',
                'display_name_with_code'=>'text',
                'parent'=>'relation',
                'type'=>'dropdown',
                'status' => 'dropdown',
                'coordinator' => 'relation',
                'job_type' => 'relation',
                'cost_location' => 'relation',
                'job' => 'relation',
                'active_job' => 'relation',
                'geo_objects' => ['relation', 'func' => 'filterThemesForGeoObject'],
                'settings' => 'json',
                'comment' => 'text',
                'name' => 'text',
                'tasks' => 'relation',
                'children' => 'relation',
                'persons_to_teams' => 'relation',
                'theme_to_companies' => 'relation'
            );
            case 'textSearch': return [
                'display_name'=>'text'
            ];
            case 'options': 
                $options = ['open'];
                if (
                        Yii::app()->user->checkAccess('Form', [], $this)
                        ||
                        (isset($this) && $this->coordinator_id == Yii::app()->user->id)
                        )
                {
                    $options[] = 'form';
                }
                if (Yii::app()->user->checkAccess('Delete', [], $this))
                {
                    $options[] = 'delete';
                }
                return $options;
            case 'mutual_forms': return [
                'relations' => ['job','job_bid', 'qualification_bid', 'baf', 'building_construction']
            ];
            case 'default_order_scope': return 'byDisplayName';
            case 'form_list_relations' : return [
                'geo_objects'
            ];
            case 'relationsCommentThreads': return [
                'baf'
            ];
            case 'update_relations' : return [
                'baf'
            ];
            default: return parent::modelSettings($column);
        }
    }
    
    public function afterSave()
    {
        parent::afterSave();
        $current_user_id = Yii::app()->isWebApplication() ? Yii::app()->user->id : null;
        $this->addPerson($this->coordinator_id);
        if ($this->isNewRecord || $this->columnChanged('coordinator_id'))
        {
            $user_displayname_for_notif = 'system';
            if(Yii::app()->isWebApplication())
            {
                $user_displayname_for_notif = Yii::app()->user->model->DisplayName;
            }
                
            $for_display_name = Theme::model()->findByPk($this->id);
            if (!empty($this->coordinator_id) && $current_user_id != $this->coordinator_id)
            {
                $text = Yii::t('BaseModule.Theme', 'NotifUserAddedYouAsCoordinator', [
                    '{added_by_user}' => $user_displayname_for_notif,
                    '{theme_name}' => $for_display_name->DisplayName
                ]);
                Yii::app()->notifManager->sendNotif(
                    $this->coordinator_id, 
                    $text, 
                    array(
                        'info' => $text,
                        'action' => [
                            'action' => 'defaultLayout',
                            'action_id' => [
                                'model_name' => Theme::class,
                                'model_id' => $this->id
                            ]
                        ]
                    )
                );
            }
            if (!empty($this->__old['coordinator_id']) && $current_user_id != $this->__old['coordinator_id'])
            {
                $text = Yii::t('BaseModule.Theme', 'NotifUserRemovedYouAsCoordinator', [
                    '{removed_by_user}' => $user_displayname_for_notif,
                    '{theme_name}' => $for_display_name->DisplayName
                ]);
                Yii::app()->notifManager->sendNotif(
                    $this->__old['coordinator_id'], 
                    $text, 
                    array(
                        'info' => $text,
                        'action' => [
                            'action' => 'defaultLayout',
                            'action_id' => [
                                'model_name' => Theme::class,
                                'model_id' => $this->id
                            ]
                        ]
                    )
                );
            }
        }
        
        if ($this->columnChanged('job_type_id'))
        {
            foreach ($this->children as $_sub_theme)
            {
                $_sub_theme->job_type_id = $this->job_type_id;
                $_sub_theme->save();
            }
        }
        
        //notifikacija clanovima tima da je neko promenio status teme
        if($this->columnChanged('status') && !empty($this->persons_to_teams))
        {
            $text = Yii::t('BaseModule.Theme', 'UserChangedStatusForTheme', ['{username}' => Yii::app()->user->model->DisplayName, '{theme}' => $this->DisplayName]);
            foreach ($this->persons_to_teams as $persons_to_team)
            {
                if (
                        !empty($persons_to_team->person->user) && 
                        $persons_to_team->to_notif === true && 
                        $persons_to_team->person_id !== $current_user_id
                    )
                {
                    Yii::app()->notifManager->sendNotif($persons_to_team->person_id, $text, [
                        'info' => $text,
                        'action' => [
                            'action' => 'defaultLayout',
                            'action_id' => [
                                'model_name' => Theme::class,
                                'model_id' => $this->id
                            ]
                        ]
                    ]);                       
                }
            }
        }
    }
    
//    public function setCoordinator($person_id)
//    {
//        $this
//    }
    public function addPerson($person_id)
    {
        if (!empty($person_id))
        {
            $add_coordinator = PersonToTheme::model()->findByAttributes([
                'theme_id' => $this->id,
                'person_id' => $person_id,
            ]);
            if (empty($add_coordinator))
            {
                $add_coordinator = new PersonToTheme();
                $add_coordinator->theme_id = $this->id;
                $add_coordinator->person_id = $person_id;
                $add_coordinator->save();
            }
        }
    }
    
    public function beforeDelete()
    {
        PersonToTheme::model()->deleteAllByAttributes(['theme_id'=>$this->id]);
        Job::model()->deleteByPk($this->id);
        Bid::model()->deleteByPk($this->id);
        return parent::beforeDelete();
    }
    
    
    /**
     * SIMAWarnException
     * SIMAExceptionModelNotDeleted
     */
    public function delete($check_result=true, $throwExceptionOnFail = true)
    {
        $transaction = Yii::app()->db->beginTransaction();
        try
        {
            parent::delete($check_result, true);
            $transaction->commit();
        }
        catch (SIMAExceptionModelNotDeleted $e)
        {
//            $transaction->rollback();
            /**
             * mora ponovno dohvatanje jer postoje dobre sanse da je neko pre nas uradio rollback
             * simaactiverecord::delete
             */
            $current_transaction = Yii::app()->db->getCurrentTransaction();
            if(!is_null($current_transaction))
            {
                $current_transaction->rollback();
            }
            throw $e;
        }
        catch (SIMAWarnException $e)
        {
//            $transaction->rollback();
            /**
             * mora ponovno dohvatanje jer postoje dobre sanse da je neko pre nas uradio rollback
             * simaactiverecord::delete
             */
            $current_transaction = Yii::app()->db->getCurrentTransaction();
            if(!is_null($current_transaction))
            {
                $current_transaction->rollback();
            }
            throw $e;
        }
        catch (Exception $e)
        {
//            $transaction->rollback();
            /**
             * mora ponovno dohvatanje jer postoje dobre sanse da je neko pre nas uradio rollback
             * simaactiverecord::delete
             */
            $current_transaction = Yii::app()->db->getCurrentTransaction();
            if(!is_null($current_transaction))
            {
                $current_transaction->rollback();
            }
            throw $e;
        }
    }
    
    public function filterThemesForGeoObject(SIMADbCriteria $condition)
    {
        if(!empty($this->geo_objects) && is_array($this->geo_objects))
        {
            $geo_objects_ids = [];
            foreach ($this->geo_objects as $geo_object)
            {
                array_push($geo_objects_ids, $geo_object->id);
            }
            $geo_objects_ids_string = implode(',', $geo_objects_ids);
            
            if (!empty($geo_objects_ids_string))
            {
                $temp_cond = new CDbCriteria();
                $theme_to_geo_objects_table = ThemeToGeoObject::model()->tableName();
                $alias = $this->getTableAlias();
                $param_id = SIMAHtml::uniqid();
                
                $temp_cond->condition = "$alias.id in (select theme_id from $theme_to_geo_objects_table where geo_object_id in (:param1{$param_id}))";
                $temp_cond->params = array(":param1{$param_id}" => $geo_objects_ids_string);
                $condition->mergeWith($temp_cond);
            }
        }
    }
    
    public function withoutTypeBaf()
    {    
        $alias = $this->getTableAlias();
        $this->getDbCriteria()->mergeWith([
            'condition' => "$alias.type <> '".Theme::$BAF."'"
        ]);
        
        return $this;
    }
    
    public function forStatuses($statuses)
    {
        $uniq = SIMAHtml::uniqid();
        $alias = $this->getTableAlias();
        
        $this->getDbCriteria()->mergeWith([
            'condition' => "$alias.status in (:param1$uniq)",
            'params' => [
                "param1$uniq" => implode(',', $statuses)
            ]
        ]);
        
        return $this;
    }
    
    public function forJobStatuses($statuses)
    {
        $uniq = SIMAHtml::uniqid();
        $alias = $this->getTableAlias();
        $jobs_table = Job::model()->tableName();
        
        $this->getDbCriteria()->mergeWith([
            'join' => "left join $jobs_table as jt$uniq on jt$uniq.id=$alias.id",
            'condition' => "jt$uniq.status in (:param1$uniq)",
            'params' => [
                "param1$uniq" => implode(',', $statuses)
            ]
        ]);
        
        return $this;
    }
    
    public function forBidStatuses($statuses)
    {
        $uniq = SIMAHtml::uniqid();
        $alias = $this->getTableAlias();
        $bids_table = Bid::model()->tableName();
        
        $this->getDbCriteria()->mergeWith([
            'join' => "left join $bids_table as bt$uniq on bt$uniq.id=$alias.id",
            'condition' => "bt$uniq.status in (:param1$uniq)",
            'params' => [
                "param1$uniq" => implode(',', $statuses)
            ]
        ]);
        
        return $this;
    }
    
    public static function getJSONColumns()
    {
        return ['settings'];
    }
    
    public function isThemeSettingSupported($setting_code)
    {
        $is_supported = true;
        
        $setting = $this->getSettings($setting_code);
        $saved_settings = $this->settings;
        
        if (!empty($setting))
        {
            switch ($setting['type']) 
            {
                case 'boolean':
                    $setting_value = $setting['default_value'];
                    if (isset($saved_settings[$setting_code]))
                    {
                        $setting_value = $saved_settings[$setting_code];
                    }
                    $is_supported = SIMAMisc::filter_bool_var($setting_value);
                    break;

                default:
                    break;
            }
        }
        
        return $is_supported;
    }
    
    public function getOldSettingsValue($setting_code)
    {
        $is_supported = true;
        
        $setting = $this->getSettings($setting_code);
        $saved_settings = CJSON::decode($this->__old['settings']);
        
        if (!empty($setting))
        {
            switch ($setting['type']) 
            {
                case 'boolean':
                    $setting_value = $setting['default_value'];
                    if (isset($saved_settings[$setting_code]))
                    {
                        $setting_value = $saved_settings[$setting_code];
                    }
                    $is_supported = SIMAMisc::filter_bool_var($setting_value);
                    break;

                default:
                    break;
            }
        }
        
        return $is_supported;
    }
    
    public function getSettingsValue($setting_code)
    {
        $is_supported = true;
        
        $setting = $this->getSettings($setting_code);
        $saved_settings = $this->settings;
        
        if (!empty($setting))
        {
            switch ($setting['type']) 
            {
                case 'boolean':
                    $setting_value = $setting['default_value'];
                    if (isset($saved_settings[$setting_code]))
                    {
                        $setting_value = $saved_settings[$setting_code];
                    }
                    $is_supported = SIMAMisc::filter_bool_var($setting_value);
                    break;

                default:
                    break;
            }
        }
        
        return $is_supported;
    }
    
    public function settingsChanged($setting_code)
    {
        $_old = $this->getOldSettingsValue($setting_code);
        $_new = $this->getSettingsValue($setting_code);
        return ($_old !==$_new );
    }
    
    public function withSettings($settings)
    {
        $settings_model_filter = ['AND'];

        foreach ($settings as $setting_key => $setting_value)
        {
            $setting = $this->getSettings($setting_key);
            $setting_model_filter = [
                $setting_key => $setting_value
            ];
            
            if ($setting['type'] === 'boolean')
            {
                $setting_value = SIMAMisc::filter_bool_var($setting_value);
            }
            
            if ($setting['default_value'] === $setting_value)
            {
                $setting_model_filter = [
                    'OR',
                    [$setting_key => null],
                    $setting_model_filter
                ];
            }
            array_push($settings_model_filter, $setting_model_filter);
        }

        if (count($settings_model_filter) > 1)
        {
            $this->getDbCriteria()->mergeWith(new SIMADbCriteria([
                'Model' => Theme::class,
                'model_filter' => [
                    'settings' => $settings_model_filter
                ]
            ]));
        }
        
        return $this;
    }
    
    public static function addCostControllerToFileSignatures($input_params)
    {
        $user_id = $input_params['user_id'];
        $theme_id = $input_params['theme_id'];
        $curr_user_id = $input_params['curr_user_id'];

        $files_to_file_tags = FiletoFileTag::model()->findAll([
            'model_filter' => [
                'file' => [
                    'bill' => []
                ],
                'file_tag' => [
                    'model_table' => Theme::model()->tableName(),
                    'model_id' => $theme_id,
                ]
            ]
        ]);
        
        foreach ($files_to_file_tags as $file_to_file_tag)
        {
            $file_signature = FileSignature::model()->findByAttributes(['file_id' => $file_to_file_tag->file_id, 'user_id' => $user_id]);
            if (empty($file_signature))
            {
                $file_signature = new FileSignature();
                $file_signature->file_id = $file_to_file_tag->file_id;
                $file_signature->user_id = $user_id;
                $file_signature->requested_by_id = $curr_user_id;
                $file_signature->save();
            }
        }
    }
    
    public function isPersonInThemeTeam(Person $person)
    {
        return PersonToTheme::model()->count([
            'model_filter' => [
                'theme' => [
                    'ids' => $this->id
                ],
                'person' => [
                    'ids' => $person->id
                ],
            ]
        ]) > 0;
    }
}