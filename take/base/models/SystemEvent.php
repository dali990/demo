<?php

class SystemEvent extends SIMAActiveRecord
{
    public static $STATUS_WAITING = 'STATUS_WAITING';
    public static $STATUS_PROGRESS = 'STATUS_PROGRESS';
    public static $STATUS_DONE = 'STATUS_DONE';
    
    public $class_exact = null;
    public $function_exact = null;
    public $params_exact = null;
    
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'web_sima.system_events';
    }
    
    public function moduleName()
    {
        return 'base';
    }

    public function relations($child_relations = [])
    {
        return array(
        );
    }

    public function rules()
    {
        return [
            ['timestamp, name', 'required'],
            ['class, function, status, params, static_call', 'safe']
        ];
    }
    
    public function scopes()
    {
        $alias = $this->getTableAlias();
        return [
            'byTimestampDesc' => [
                'order' => $alias.'.timestamp DESC'
            ],
            'notStarted' => [
                'condition' => $alias.'.status=\''.SystemEvent::$STATUS_WAITING.'\''
            ],
            'done' => [
                'condition' => $alias.'.status=\''.SystemEvent::$STATUS_DONE.'\''
            ],
            'timeExpired' => [
                'condition' => $alias.'."timestamp"<now()'
            ]
        ];
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters': return array(
                'name' => 'text',
                'timestamp'=>'date_range',
                'status' => 'dropdown',
                'is_long' => 'boolean',
                'class' => 'text',
                'function' => 'text',
                'class_exact' => ['func' => 'filter_class_exact'],
                'function_exact' => ['func' => 'filter_function_exact'],
                'params' => 'text',
                'params_exact' => ['func' => 'filter_params_exact'],
                'static_call' => 'boolean'
            );
            default: return parent::modelSettings($column);
        }
    }
    
    public function filter_class_exact($condition, $alias)
    {
        if(isset($this->class_exact))
        {
            $condition->mergeWith([
                'condition' => "$alias.class = '$this->class_exact'"
            ]);
        }
    }
    
    public function filter_function_exact($condition, $alias)
    {
        if(isset($this->function_exact))
        {
            $condition->mergeWith([
                'condition' => "$alias.function = '$this->function_exact'"
            ]);
        }
    }
    
    public function filter_params_exact($condition, $alias)
    {
        if(isset($this->params_exact))
        {
            $condition->mergeWith([
                'condition' => "$alias.params = '$this->params_exact'"
            ]);
        }
    }
    
    public function beforeSave() 
    {
        if (empty($this->status))
        {
            $this->status = SystemEvent::$STATUS_WAITING;
        }
        
        return parent::beforeSave();
    }

    /**
     * 
     * @param string $class - clasa gde se nalazi staticka funkcija
     * @param string $function - staticka funkcija koja se izvrsava
     * @param string $timestamp - vreme kada treba da se izvrsi -> ne sme da zavisi od trenutnog vremena
     * @param array $params - parametri
     * @param string $name - opisni naziv sta se izvrsava
     * @param boolean $update - ukoliko postoji ista funkcija, a da nije izvrsena, samo da se promeni vreme. Proverava se po klasi, imenu funkcije i parametrima
     */
    public static function add($class,$function,$timestamp,$params,$name='',$update = false)
    {
//        if(Yii::app()->params['systemevent_execute_immediately'] === true)
//        {
//            $class::$function($params);
//            return;
//        }
        
        $event = null;
        if ($update)
        {
            $eventCriteria = new SIMADbCriteria([
                'Model' => SystemEvent::model(),
                'model_filter' => [
                    'class_exact' => $class,
                    'function_exact' => $function,
                    'params_exact' => CJSON::encode($params),
                    'scopes' => [
                        'notStarted',
                    ]
                ]
            ]);
            $event = SystemEvent::model()->find($eventCriteria);
        }
        if ($event == null)
            $event = new SystemEvent();
        
        $event->timestamp = $timestamp;
        $event->class = $class;
        $event->function = $function;
        $event->name = $name;
        $event->params = CJSON::encode($params);
        $event->save();
    }
    
}