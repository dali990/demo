<?php

PartnerList::$types = [
    'PUBLIC' => Yii::t('BaseModule.PartnerList', 'Public'),
    'PRIVATE' => Yii::t('BaseModule.PartnerList', 'Private')
];

class PartnerList extends SIMAActiveRecord
{
    public static $PUBLIC = 'PUBLIC';
    public static $PRIVATE = 'PRIVATE';
    public static $types = [];
    
    public $is_system_filter = null;
    
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
 
    public function tableName()
    {
        return 'base.partner_lists';
    }
    
    public function moduleName()
    {
        return 'base';
    }
    
    public function __get($column)
    {
        switch ($column)
        {
            case 'DisplayName': return $this->name;
            case 'SearchName': return $this->DisplayName;
            case 'is_system': return empty($this->owner_id);
            default: return parent::__get($column);
        }
    }
    
    public function rules()
    {
        return [
            ['name', 'required'],
            ['owner_id, type, created_time, code, description', 'safe'],
            ['code', 'checkCodeUniq']
        ];
    }
    
    public function checkCodeUniq()
    {
        if (
                !$this->hasErrors() && 
                !empty($this->code) && 
                (
                    $this->isNewRecord || $this->columnChanged('code')
                )
            )
        {
            $partner_list = PartnerList::model()->findByAttributes([
                'code' => $this->code
            ]);
            if (!empty($partner_list))
            {
                $this->addError('code', Yii::t('BaseModule.PartnerList', 'CodeUniq'));
            }
        }
    }
    
    public function relations($child_relations = [])
    {
        $partners_to_partner_lists_table_name = PartnerToPartnerList::model()->tableName();
        $users_to_partner_lists_table_name = UserToPartnerList::model()->tableName();

        return [
            'owner' => [self::BELONGS_TO, 'User', 'owner_id'],
            'partners' => [self::MANY_MANY, 'Partner', "$partners_to_partner_lists_table_name(partner_list_id, partner_id)"],
            'partner_list_to_partners' => [self::HAS_MANY, 'PartnerToPartnerList', 'partner_list_id'],
            'users' => [self::MANY_MANY, 'User', "$users_to_partner_lists_table_name(partner_list_id, user_id)"],
            'partner_list_to_users' => [self::HAS_MANY, 'UserToPartnerList', 'partner_list_id'],
        ];
    }
    
    public function scopes()
    {
        $alias = $this->getTableAlias();
        
        return [
            'byName' => [
                'order' => "$alias.name ASC"
            ],
            'withoutSystems' => [
                'condition' => "$alias.owner_id is not null"
            ]
        ];
    }
    
    public function withUserAccessToList($user_id = null)
    {
        if (empty($user_id) && Yii::app()->isWebApplication())
        {
            $user_id = Yii::app()->user->id;
        }
        
        if (!empty($user_id))
        {
            $alias = $this->getTableAlias();
            $uniq = SIMAHtml::uniqid();
            $users_to_partner_lists_tbl_name = UserToPartnerList::model()->tableName();

            $this->getDbCriteria()->mergeWith([
                'condition' => "
                    $alias.owner_id = :param1$uniq or 
                    $alias.type = :param2$uniq or 
                    (
                        $alias.type = :param3$uniq and 
                        exists(select 1 from $users_to_partner_lists_tbl_name utpl$uniq where utpl$uniq.partner_list_id = $alias.id and utpl$uniq.user_id = :param1$uniq)
                    )
                ",
                'params' => [
                    ":param1$uniq" => $user_id,
                    ":param2$uniq" => PartnerList::$PUBLIC,
                    ":param3$uniq" => PartnerList::$PRIVATE
                ]
            ]);
        }

        return $this;
    }
    
    public function hasUserAccessToList(User $user = null)
    {
        if (empty($user) && Yii::app()->isWebApplication())
        {
            $user = Yii::app()->user->model;
        }
        
        $has_access = false;

        if (
                !empty($user) && 
                (
                    $this->owner_id === $user->id ||
                    $this->type === PartnerList::$PUBLIC || 
                    ($this->type === PartnerList::$PRIVATE && $this->isUserInGroup($user))
                )
           ) 
        {
            $has_access = true;
        }
        
        return $has_access;
    }
    
    public function isUserInGroup(User $user)
    {
        $user_to_partner_list_cnt = UserToPartnerList::model()->countByAttributes([
            'partner_list_id' => $this->id,
            'user_id' => $user->id
        ]);
        
        return $user_to_partner_list_cnt > 0 ? true : false;
    }
    
    public function modelSettings($column)
    {
        $options = [];

        if (Yii::app()->isWebApplication() && $this->hasUserAccessToList())
        {
            $options[] = 'form';
            if (!$this->is_system)
            {
                $options[] = 'delete';
            }
        }

        switch ($column)
        {
            case 'filters' : return array_merge(parent::modelSettings('filters'), [
                'code' => 'text',
                'name' => 'text',
                'owner'=>'relation',
                'type' => 'dropdown',
                'description' => 'text',
                'is_system_filter' => ['boolean', 'func' => 'filterIsSystem'],
                'partner_list_to_partners' => 'relation',
                'partner_list_to_users' => 'relation',
                'created_time' => 'date_time_range'
            ]);
            case 'options' : return $options;
            case 'textSearch' : return [
                'name' => 'text'
            ];
            case 'form_list_relations' : return ['users'];
            default: return parent::modelSettings($column);
        }
    }
    
    public function filterIsSystem($criteria, $alias)
    {
        if (!is_null($this->is_system_filter))
        {
            if (SIMAMisc::filter_bool_var($this->is_system_filter))
            {
                $condition = "$alias.owner_id is null";
            }
            else
            {
                $condition = "$alias.owner_id is not null";
            }

            $temp_criteria = new CDbCriteria();
            $temp_criteria->condition = $condition;
            $criteria->mergeWith($temp_criteria);
        }
    }
    
    public function beforeSave()
    {
        if ($this->isNewRecord && $this->getScenario() !== 'system')
        {
            $this->owner_id = Yii::app()->user->id;
        }
        
        return parent::beforeSave();
    }
    
    public function beforeDelete() 
    {
        foreach ($this->partner_list_to_partners as $partner_list_to_partner)
        {
            $partner_list_to_partner->delete();
        }
        
        return parent::beforeDelete();
    }
    
    public function getClasses()
    {
        return parent::getClasses() . ($this->is_system ? '_system-partner-list' : '');
    }

    public function syncPartnersInListByModelFilter(array $model_filter)
    {
        $criteria = new SIMADbCriteria([
            'Model' => Partner::class,
            'model_filter' => $model_filter
        ]);
        
        gc_enable();
        
        $partners_ids = [];
        Partner::model()->findAllByParts(function($partners) use (&$partners_ids) {
            foreach ($partners as $partner)
            {
                array_push($partners_ids, $partner->id);
                $partner_to_partner_list = PartnerToPartnerList::addByIds($this->id, $partner->id);
                unset($partner_to_partner_list);
                gc_collect_cycles();
            }
            
            unset($partners);
            gc_collect_cycles();
        }, $criteria);

        //Sasa A. - zauzima duplo manje memorije na ovaj nacin, nego kada se pozove relacija $this->partner_list_to_partners
        $partner_list_to_partners = PartnerToPartnerList::model()->findAllByAttributes([
            'partner_list_id' => $this->id
        ]);
        foreach ($partner_list_to_partners as $partner_list_to_partner)
        {
            if (!in_array($partner_list_to_partner->partner_id, $partners_ids))
            {
                $partner_list_to_partner->delete();
            }
        }
        
        unset($partner_list_to_partners);
        unset($partners_ids);
        gc_collect_cycles();
    }
    
    public function emptyList()
    {
        PartnerToPartnerList::model()->deleteAllByAttributes([
            'partner_list_id' => $this->id
        ]);
        
        return $this;
    }
    
    public static function getSystemLists($update_function = null)
    {
        $system_partner_lists_array = PartnerList::getSystemListsArray();
        
        $system_partner_lists_models = [];

        $i = 1;
        $system_partner_lists_array_cnt = count($system_partner_lists_array);
        foreach ($system_partner_lists_array as $list_code => $list_params)
        {
            if (!empty($update_function))
            {
                $percent = round(($i/$system_partner_lists_array_cnt)*100, 0, PHP_ROUND_HALF_DOWN);
                call_user_func($update_function, $percent);
            }
            
            array_push($system_partner_lists_models, PartnerList::getSystemListAsModel($list_code, $list_params));
            
            $i++;
        }
        
        return $system_partner_lists_models;
    }
    
    public static function getSystemList($list_code)
    {
        $system_partner_lists_array = PartnerList::getSystemListsArray();

        return PartnerList::getSystemListAsModel($list_code, $system_partner_lists_array[$list_code]);
    }
    
    public static function calcSystemLists($update_function = null)
    {
        $system_partner_lists_array = PartnerList::getSystemListsArray();

        $i = 1;
        $system_partner_lists_array_cnt = count($system_partner_lists_array);
        foreach ($system_partner_lists_array as $list_code => $list_params)
        {
            if (!empty($update_function))
            {
                $percent = round(($i/$system_partner_lists_array_cnt)*100, 0, PHP_ROUND_HALF_DOWN);
                call_user_func($update_function, $percent);
            }

            $list_model = PartnerList::getSystemListAsModel($list_code, $list_params);

            call_user_func($list_params['calc_list_func'], $list_model);

            $i++;
        }
    }
    
    public static function calcSystemList($list_code)
    {
        $system_partner_lists_array = PartnerList::getSystemListsArray();
        $list_params = $system_partner_lists_array[$list_code];
        
        $list_model = PartnerList::getSystemListAsModel($list_code, $list_params);
        
        call_user_func($list_params['calc_list_func'], $list_model);
    }
    
    public static function getSystemListAsModel($list_code, $list_params)
    {
        $partner_list = PartnerList::model()->findByAttributes([
            'code' => $list_code
        ]);
        
        if (empty($partner_list))
        {
            $partner_list = new PartnerList();
            $partner_list->scenario = 'system';
            $partner_list->code = $list_code;
            $partner_list->name = $list_params['name'];
            $partner_list->type = PartnerList::$PUBLIC;
            $partner_list->description = $list_params['description'];
            $partner_list->save();
        }
        
        return $partner_list;
    }
    
    public static function getSystemListsArray()
    {
        return Yii::app()->params['system_partner_lists'];
    }
    
    public function afterValidateDependencyError(array $foreign_dependencies)
    {        
        $allowed_tables = [
            PartnerToPartnerList::tableName().':partner_list_id'
        ];
        
        $parsed_foreign_dependencies = array_diff($foreign_dependencies, $allowed_tables);
        
        parent::afterValidateDependencyError($parsed_foreign_dependencies);
    }
}
