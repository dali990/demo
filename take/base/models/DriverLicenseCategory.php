<?php

class DriverLicenseCategory extends SIMAActiveRecord {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'base.driver_license_categories';
    }

    public function moduleName()
    {
        return 'base';
    }
    
    public function __get($column)
    {
        switch ($column)
        {
            case 'DisplayName': return $this->category;
            default: return parent::__get($column);
        }
    }
    
    public function rules() {
        return array(
            array('category', 'required'),
            array('description, age_range', 'safe')
        );
    }

    public function modelSettings($column) 
    {
        switch ($column)
        {
            case 'filters' : return array(
                'category' => 'text',
                'age_range' => 'text',
                'description' => 'text',
            );
            case 'textSearch' : return array(
                'category'=>'text'
            );
            default: return parent::modelSettings($column);
        }
    }

}