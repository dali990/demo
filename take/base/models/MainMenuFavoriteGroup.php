<?php

class MainMenuFavoriteGroup extends SIMAActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'base.mainmenu_favorite_groups';
    }

    public function __get($column)
    {
        switch ($column)
        {
            case 'DisplayName': return $this->group_name;
            case 'SearchName': return $this->DisplayName;
            default: return parent::__get($column);
        }
    }
    
    public function rules()
    {
        return [
            ['group_name, user_id', 'required'],
            ['prev_id, is_default', 'safe'],
            ['is_default', 'checkDefaultGroupDelete', 'on' => 'delete'],
            ['user_id', 'default',
                'value' => null,
                'setOnEmpty' => true, 'on' => ['insert', 'update']],
            ['group_name', 'unique_with', 'with' => ['user_id']]
        ];
    }
    
    public function checkDefaultGroupDelete()
    {
        if (!$this->hasErrors() && $this->is_default === true)
        {
            $this->addError('is_default', Yii::t('BaseModule.MainMenuFavoriteGroup', 'CanNotDeleteDefaultGroup'));
        }
    }
    
    public function scopes()
    {
        $alias = $this->getTableAlias();
        return [
            'byName' => [
                'order' => "$alias.group_name ASC"
            ]
        ];
    }
    
    public function relations($child_relations = [])
    {
        return [
            'user' => [self::BELONGS_TO, User::class, 'user_id'],
            'mainmenu_favorite_group_to_items' => [self::HAS_MANY, MainMenuFavoriteItemToGroup::class, 'group_id'],
            'prev' => [self::BELONGS_TO, MainMenuFavoriteGroup::class, 'prev_id'],
            'next' => [self::HAS_ONE, MainMenuFavoriteGroup::class, 'prev_id']
        ];
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters' : return [
                'group_name' => 'text',
                'user' => 'relation',
                'mainmenu_favorite_group_to_items' => 'relation',
                'prev' => 'relation',
                'next' => 'relation',
                'is_default' => 'boolean'
            ];
            case 'textSearch': return array(
                'group_name' => 'text'
            );
            case 'default_order_scope': return 'byName';
            default: return parent::modelSettings($column);
        }
    }
    
    protected function modelOptions(\User $user = null):array
    {
        $options = [];

        if (!empty($user) && intval($user->id) === intval($this->user_id))
        {
            $options[] = 'form';
            if ($this->is_default === false)
            {
                $options[] = 'delete';
            }
        }

        return $options;
    }
    
    public function afterSave()
    {
        parent::afterSave();
        
        if ($this->isNewRecord && empty($this->prev_id))
        {
            $first_group = MainMenuFavoriteGroup::model()->find([
                'model_filter' => [
                    'AND',
                    [
                        'user' => [
                            'ids' => $this->user_id
                        ],
                        'prev' => [
                            'ids' => 'null'
                        ],
                    ],
                    [
                        'NOT',
                        [
                            'ids' => $this->id
                        ]
                    ]
                ]
            ]);
            if (!empty($first_group))
            {
                $first_group->prev_id = $this->id;
                $first_group->save();
            }
        }
        
        if ($this->columnChanged('is_default') && $this->is_default === true)
        {
            MainMenuFavoriteItemToGroup::syncWithOldMainMenuFavoriteItems($this->user);
        }
        
        Yii::app()->mainMenu->sendUpdateEventForUser($this->user);
    }
    
    public function beforeDelete() 
    {
        foreach ($this->mainmenu_favorite_group_to_items as $mainmenu_favorite_group_to_item)
        {
            $mainmenu_favorite_group_to_item->delete();
        }
        
        if (!empty($this->next))
        {
            $this->next->prev_id = !empty($this->prev_id) ? $this->prev_id : null;
            $this->next->save();
        }

        return parent::beforeDelete();
    }
    
    public function afterDelete() 
    {
        parent::afterDelete();
        
        Yii::app()->mainMenu->sendUpdateEventForUser($this->user);
    }
    
    public function afterValidateDependencyError(array $foreign_dependencies)
    {        
        $allowed_tables = [
            MainMenuFavoriteItemToGroup::tableName().':group_id',
            MainMenuFavoriteGroup::tableName().':prev_id'
        ];
        
        $parsed_foreign_dependencies = array_diff($foreign_dependencies, $allowed_tables);
        
        parent::afterValidateDependencyError($parsed_foreign_dependencies);
    }
    
    public static function getSortedMainMenuFavoriteGroups(User $user, $only_ids = false)
    {
        $first_main_menu_favorite_group = MainMenuFavoriteGroup::model()->findByAttributes([
            'user_id' => $user->id,
            'prev_id' => null
        ]);

        $sorted_main_menu_favorite_groups = [];
        if (!empty($first_main_menu_favorite_group))
        {
            if ($only_ids)
            {
                array_push($sorted_main_menu_favorite_groups, $first_main_menu_favorite_group->id);
            }
            else
            {
                array_push($sorted_main_menu_favorite_groups, $first_main_menu_favorite_group);
            }
            $curr_main_menu_favorite_group = $first_main_menu_favorite_group;
            while (!empty($curr_main_menu_favorite_group->next))
            {
                if ($only_ids)
                {
                    array_push($sorted_main_menu_favorite_groups, $curr_main_menu_favorite_group->next->id);
                }
                else
                {
                    array_push($sorted_main_menu_favorite_groups, $curr_main_menu_favorite_group->next);
                }
                $curr_main_menu_favorite_group = $curr_main_menu_favorite_group->next;
            }
        }
        
        return $sorted_main_menu_favorite_groups;
    }
    
    public function getSortedMainMenuFavoriteGroupToItems($only_ids = false)
    {
        $first_main_menu_favorite_group_to_item = MainMenuFavoriteItemToGroup::model()->findByAttributes([
            'group_id' => $this->id,
            'prev_id' => null
        ]);

        $sorted_main_menu_favorite_grupe_to_items = [];
        if (!empty($first_main_menu_favorite_group_to_item))
        {
            if ($only_ids)
            {
                array_push($sorted_main_menu_favorite_grupe_to_items, $first_main_menu_favorite_group_to_item->id);
            }
            else
            {
                array_push($sorted_main_menu_favorite_grupe_to_items, $first_main_menu_favorite_group_to_item);
            }
            $curr_main_menu_favorite_group_to_item = $first_main_menu_favorite_group_to_item;
            while (!empty($curr_main_menu_favorite_group_to_item->next))
            {
                if ($only_ids)
                {
                    array_push($sorted_main_menu_favorite_grupe_to_items, $curr_main_menu_favorite_group_to_item->next->id);
                }
                else
                {
                    array_push($sorted_main_menu_favorite_grupe_to_items, $curr_main_menu_favorite_group_to_item->next);
                }
                $curr_main_menu_favorite_group_to_item = $curr_main_menu_favorite_group_to_item->next;
            }
        }
        
        return $sorted_main_menu_favorite_grupe_to_items;
    }
    
    public function mainmenuFavoriteGroupToItemsIds()
    {
        $ids = [];
        
        foreach ($this->mainmenu_favorite_group_to_items as $mainmenu_favorite_group_to_item)
        {
            array_push($ids, $mainmenu_favorite_group_to_item->id);
        }
        
        return $ids;
    }
    
    public static function getDefaultGroupForUser(User $user)
    {
        $default_group = MainMenuFavoriteGroup::model()->findByAttributes([
            'user_id' => $user->id,
            'is_default' => true
        ]);
        if(empty($default_group))
        {
            $default_group = new MainMenuFavoriteGroup();
            $default_group->user_id = $user->id;
            $default_group->group_name = Yii::t('BaseModule.MainMenuFavoriteGroup', 'Default');
            $default_group->is_default = true;
            $default_group->save();
        }
        
        return $default_group;
    }
}
