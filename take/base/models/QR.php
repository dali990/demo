<?php

class QR extends SIMAActiveRecord
{
    private $__base_model = null;
    
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'base.qrs';
    }
    
    public function moduleName()
    {
        return 'base';
    }
    
    public function __get($column)
    {
        switch ($column)
        {
//            case 'path2': return 'fixedAssets/fixedAsset';
            case 'DisplayName':
                $result = 'QR Code';
                
                if(!empty($this->base_model))
                {
                    $result .= ' '.$this->base_model->DisplayName;
                }
                
                return $result;
            default: return parent::__get($column);
        }
    }
    
    public function getbase_model()
    {
        if(empty($this->__base_model))
        {
            if(!empty($this->model) && !empty($this->model_id))
            {
                $this->__base_model = ModelController::GetModel($this->model, $this->model_id);
            }
        }
        
        return $this->__base_model;
    }
    
    public function modelSettings($column) 
    {
        switch ($column)
        {
            case 'filters' : return array(
                'model' => 'text',
                'model_id' => 'integer',
                'qr_data' => 'dropdown'
            );
            case 'options': return [
//                'delete',
                'qrcode'
            ];
            case 'multiselect_options':  return ['qrcode'];
            default: return parent::modelSettings($column);
        }
    }
}

