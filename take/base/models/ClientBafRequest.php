<?php

ClientBafRequest::$priorities = [
    'LOW' => Yii::t('BaseModule.ClientBafRequest', 'PriorityLow'),
    'MIDDLE' => Yii::t('BaseModule.ClientBafRequest', 'PriorityMiddle'),
    'HIGH' => Yii::t('BaseModule.ClientBafRequest', 'PriorityHigh'),
    'ERROR' => Yii::t('BaseModule.ClientBafRequest', 'PriorityError'),
];

class ClientBafRequest extends SIMAActiveRecord
{
    public $client_baf_request_sent = null;
    public $send = true;
    
    public static $LOW = 'LOW';
    public static $MIDDLE = 'MIDDLE';
    public static $HIGH = 'HIGH';
    public static $ERROR = 'ERROR';
    public static $priorities = [];
    
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function moduleName()
    {
        return 'base';
    }
    
    public function tableName()
    {
        return 'base.client_baf_requests';
    }
    
    public function __get($column) {
        switch ($column)
        {
           case 'path2'    :   return 'base/errorReports/openClientBafRequest/';
           case 'DisplayName': return $this->name;
           case 'SearchName':return $this->DisplayName;
           default: return parent::__get($column);
        }
    }
      
    public function rules()
    {
        return array(
            ['name, requested_by_id, priority', 'required'],
            ['comment_thread_id, create_time, send_time, description, server_baf_request_id, send', 'safe'],
            ['comment_thread_id, requested_by_id','default',
                'value'=>null,
                'setOnEmpty'=>true,'on'=>['insert','update']
            ]
        );
    }
    
    public function relations($child_relations = [])
    {
        return array(
            'requested_by' => array(self::BELONGS_TO, 'Partner', 'requested_by_id'),
            'comment_thread'=>array(self::BELONGS_TO, 'CommentThread', 'comment_thread_id')
        );
    }
    
    public function scopes()
    {
        $alias = $this->getTableAlias();
        return array(
            'byTime' => array(
                'order' => $alias.'."create_time" DESC'
            ),
            'onlyUnsent' => [
                'condition' => "$alias.server_baf_request_id is null"
            ]
        );
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters': return array(
                'name'=>'text',
                'requested_by'=>'relation',
                'create_time' => 'date_time_range',
                'send_time' => 'date_time_range',
                'description'=>'text',
                'client_baf_request_sent' => ['dropdown','func'=>'filter_client_baf_request_sent'],
                'priority'=>'dropdown',
                'server_baf_request_id'=>'dropdown',
                'id' => 'integer'
            );
            case 'options':  
                $options = ['delete','open'];
                if(!isset($this->server_baf_request_id))
                {
                    $options[] = 'form';
                }
                return $options;
            case 'multiselect_options':  return ['delete'];
            case 'GuiTable': return array(
                'scopes' => array('byTime')
            );
            case 'textSearch': return array(
                'name'=>'text',
                'requested_by'=>'relation',
                'description'=>'text'
            );
            default: return parent::modelSettings($column);
        }
    }
    
    public function beforeSave() 
    {
//        if (empty($this->requested_by_id))
//        {
//            $this->requested_by_id = Yii::app()->company->id;
//        }
        
        return parent::beforeSave();
    }
    
    public function afterSave() 
    {
        parent::afterSave();
        $_send = filter_var($this->send, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
        if (empty($this->server_baf_request_id) && $_send === true)
        {
            Yii::app()->errorReport->sendClientBafRequest($this);
        }
        
        if(isset($this->requested_by->person->user))
        {
            Yii::app()->warnManager->sendWarns($this->requested_by_id, 'WARN_NOT_SEND_CLIENT_BAF_REQUESTS');
        }
    }
    
    public function filter_client_baf_request_sent($condition) 
    {        
        if(!empty($this->client_baf_request_sent))
        {
            $alias = $this->getTableAlias(FALSE,FALSE);
            switch($this->client_baf_request_sent)
            {
                case 'NO':
                    $cond = "$alias.server_baf_request_id is null"; break;
                case 'YES':
                    $cond = "$alias.server_baf_request_id is not null"; break;
                default:
                    $cond = ""; break;
            }

            $temp_cond = new CDbCriteria();
            $temp_cond->condition=$cond;
            $condition->mergeWith($temp_cond);
        }        
    }
    
    public function afterDelete() 
    {
        parent::afterDelete();
        if(isset($this->requested_by->person->user))
        {
            Yii::app()->warnManager->sendWarns($this->requested_by_id, 'WARN_NOT_SEND_CLIENT_BAF_REQUESTS');
        }
    }
 }
