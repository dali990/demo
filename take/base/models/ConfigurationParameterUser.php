<?php

class ConfigurationParameterUser extends ConfigurationParameter
{
    private $_confParam = null;
    
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'options': return [
                'configuration_parameter_edit_user',
                'configuration_parameter_revert_to_default_user'
            ];
            default: return parent::modelSettings($column);
        }
    }
}
