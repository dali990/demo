<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class SseEvents extends SIMAActiveRecord
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
 
    public function tableName()
    {
        return 'web_sima.sse_events';
    }
    
    public function moduleName()
    {
        return 'base';
    }
    
    protected static function getJSONColumns()
    {
        return array('params');
    }
    
    protected static function getMicrosecondColumns()
    {
        return array('timestamp');
    }
    
    public function relations($child_relations = [])
    {
        return array(            
            'user' => array(self::BELONGS_TO, 'User', 'user_id'),
        );
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters': return array(
                'user_id'=>'dropdown',
                'timestamp'=>'date_time_range'
            );
            default: return parent::modelSettings($column);
        }
    }
}
