<?php

class ModelUpdateEvent2CompletedPhpsessionid extends SIMAActiveRecord
{
//    public static $STATUS_INITIAL = 'STAT_INITIAL';
//    public static $STATUS_WAITING = 'STAT_WAITING';
//    public static $STATUS_PROGRESS = 'STAT_PROGRESS';
//    public static $STATUS_DONE = 'STAT_DONE';
//    
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function moduleName()
    {
        return 'base';
    }
    
    public function tableName()
    {
        return 'base.models_update_events2_completed_phpsessionids';
    }
//    
//    public function rules()
//    {
//    	return [
//            ['model_tag', 'required'],
//            ['event_time, update_status', 'safe']
//        ];
//    }
//    
//    public function scopes()
//    {
//        $alias = $this->getTableAlias();
//        return [
//            'oldExpired' => [
//                'condition' => "$alias.event_time<now() AND $alias.event_time<(now()-INTERVAL '1h')"
//            ]
//        ];
//    }
//    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters': return [
                'php_session_id' => 'dropdown'
            ];
            default: return parent::modelSettings($column);
        }
    }
}

