<?php 

class UserConfigCounterItem extends SIMAActiveRecord
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
 
    public function tableName()
    {
        return 'base.user_config_counter_item';
    }
    
    public function moduleName()
    {
        return 'base';
    }
    
    public function __get($column)
    {
    	switch ($column)
    	{
    		case 'DisplayName': 	return $this->last_taken_value;
    		case 'SearchName':      return $this->last_taken_value;
    		default: 		return parent::__get($column);
    	}
    }
    
//    public function scopes()
//    {
//        $alias = $this->getTableAlias();
//        return [
//            'byName' => [
//                'order' => "$alias.name"
//            ]
//        ];
//    }
    
    public function relations($child_relations = [])
    {
        return parent::relations(array_merge([
            'user_config_counter' => [self::BELONGS_TO, 'UserConfigCounter', 'user_config_counter_id'],
            'month' => [self::BELONGS_TO, 'Month', 'month_id'],
            'year' => [self::BELONGS_TO, 'Year', 'year_id'],
        ],$child_relations));
    }
    
    public function rules()
    {
    	return array(
            array('user_config_counter_id', 'required'),
            array('month_id, year_id, last_taken_value', 'safe')
    	);
    }
    
    public function modelSettings($column)
    {
        $owner = $this->owner;
        switch ($column)
        {
            case 'options': 
                $options = [];
                if (
                        (
                            $owner->user_config_counter->time_interval === UserConfigCounter::$INTERVAL_YEAR
                            &&
                            !empty($owner->year_id)
                        )
                        ||
                        (
                            $owner->user_config_counter->time_interval === UserConfigCounter::$INTERVAL_MONTH
                            &&
                            !empty($owner->month_id)
                        )
                    )
                {
                    $options[] = 'form';
                }
                $options[] = 'delete';
                return $options;
            case 'filters' : return [
                'user_config_counter' => 'relation',
                'month' => 'relation',
                'year' => 'relation',
            ];
            case 'number_fields': return ['last_taken_value'];
//            case 'textSearch': return array(
//                'name' => 'text',
//                'note' => 'text',
//            ); 
//            case 'default_order_scope': return 'byName';
            default: return parent::modelSettings($column);
        }
    }
    
    
}