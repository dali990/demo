<?php

class RequestLog extends SIMAActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'web_sima.request_logs';
    }
    
    public function moduleName()
    {
        return 'base';
    }

//     public function __get($column)
//     {
//     	switch ($column)
//     	{
//     		case 'hasChildren': return ($this->count_childrens>0);
// //     		case 'path': 		return 'site/todo';
//     		default: 			return parent::__get($column);
//     	}
//     }

    public function relations($child_relations = [])
    {
        return array(
        );
    }

    public function rules()
    {
        return array(
//            array('timestamp, name', 'required'),
//     			array('parent_id, description, model, model_id, file_attributes_table', 'safe'),
//     			array('comment_thread_id, assign_to_id, creator_id, todo_status_id, todo_priority_id','default',
//     					'value'=>null,
//     					'setOnEmpty'=>true,'on'=>array('insert','update')),
        );
    }

    protected static function getJSONColumns()
    {
        return array('params');
    }
  
    
}