<?php

class ErrorReport extends SIMAActiveRecord
{
    public $error_sent = null;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'base.error_reports';
    }
    
    public function __get($column) {
        switch ($column)
        {
           case 'DisplayName': return Yii::t('BaseModule.ErrorReport', 'ErrorReport').": {$this->time}";
           case 'SearchName':return $this->DisplayName;
           case 'path2'    :   return 'base/errorReports/';
           default: return parent::__get($column);
        }
    }
    
    public function moduleName()
    {
        return 'base';
    }
      
    public function rules()
    {
        return array(
            array('time, action, message, get_params, post_params, submitter_id, fixed, description, server_error_report_id, comment_thread_id', 'safe'),
            array('submitter_id, server_error_report_id, comment_thread_id','default',
                'value'=>null,
                'setOnEmpty'=>true,'on'=>array('insert','update'))
        );
    }
    
    public function relations($child_relations = [])
    {
        return array(
            'submitter' => array(self::BELONGS_TO, 'User', 'submitter_id'),
            'comment_thread'=>array(self::BELONGS_TO, 'CommentThread', 'comment_thread_id')
        );
    }
    
    public function scopes()
    {
        $alias = $this->getTableAlias();
        return array(
            'byTime' => array(
                'order' => $alias.'."time" desc'
            ),
            'onlyUnsent' => [
                'condition' => "$alias.server_error_report_id is null"
            ]
        );
    }

    public static function getJSONColumns()
    {
        return array('get_params', 'post_params');
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {
            case 'filters': return array(
                'fixed' => 'dropdown',
                'submitter' => 'relation',
                'description' => 'text',
                'time'=>'date_time_range',
                'action' => 'text',
                'error_sent' => ['dropdown','func'=>'filter_error_sent'],
                'id' => 'integer'
            );
            case 'options': return ['delete', 'open'];
            case 'multiselect_options':  return ['delete'];
            case 'textSearch': return array(
                'message' => 'text',
                'time'=>'date_time_range'
            ); 
            case 'GuiTable': return array(
                'scopes' => array('byTime')
            );

            default: return parent::modelSettings($column);
        }
    }
    
    public function filter_error_sent($condition) 
    {        
        if(!empty($this->error_sent))
        {
            $alias = $this->getTableAlias(FALSE,FALSE);
            switch($this->error_sent)
            {
                case 'NO':
                    $cond = "$alias.server_error_report_id is null"; break;
                case 'YES':
                    $cond = "$alias.server_error_report_id is not null"; break;
                default:
                    $cond = ""; break;
            }

            $temp_cond = new CDbCriteria();
            $temp_cond->condition=$cond;
            $condition->mergeWith($temp_cond);
        }        
    }
    
    public function beforeSave() 
    {
        if (empty($this->get_params))
        {
            unset($this->get_params);
        }
        
        if (empty($this->post_params))
        {
            unset($this->post_params);
        }
        
        return parent::beforeSave();
    }
    
    public function AfterSave()
    {
        parent::AfterSave();

        if ($this->isNewRecord)
        {
            $receiver_id = intval(Yii::app()->configManager->get('base.errors_receiver_id', false));
            $server_receivers_ids = [];
            if (Yii::app()->hasModule('setpp'))
            {
                $server_receivers_ids = Yii::app()->configManager->get('setpp.bafs.server_errors_receivers_ids', false);
            }
            if(!empty($receiver_id) && Yii::app()->user->id !== $receiver_id && !in_array($receiver_id, $server_receivers_ids))
            {
                $text = Yii::t('BaseModule.ErrorReport', 'ErrorReportedInSystem');
                Yii::app()->notifManager->sendNotif($receiver_id, $text, 
                    array(
                        'info' => $text,
                        'action' => array(
                           'action' => 'guitable',
                           'action_id' => 'base_5'
                        )
                    ),
                    'error_report' //code
                );
            }
        }
    }
 }
