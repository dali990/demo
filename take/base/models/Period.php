<?php

class Period extends SIMAActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'base.periods';
    }
    
    public function moduleName()
    {
        return 'base';
    }
    
    public function __get($column)
    {
        switch ($column)
        {
            case 'DisplayName': return $this->period_name;
            case 'SearchName': return $this->period_name;
            default: return parent::__get($column);
        }
    }    
    
    public function rules()
    {
        return array(
            array('period_name', 'required'),
            array('years, months, days', 'safe'),
            array('days', 'checkPeriod')
        );
    }
    
    public function checkPeriod()
    {
        if (empty($this->years) && empty($this->months) && empty($this->days))
        {
            $this->addError('days', 'Morate uneti bar jedan od godinu, mesec i dan!');
        }
    }
    
    public function modelSettings($column)
    {
        switch ($column)
        {            
            case 'filters': return [
                'period_name' => 'text',
                'years' => 'integer',
                'months' => 'integer',
                'days' => 'integer'
            ];
            case 'textSearch': return [
                'period_name' => 'text'
            ];
            default: return parent::modelSettings($column);
        }
    }
    
}