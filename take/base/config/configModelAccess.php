<?php

return [
    'Theme' => [
        'modelThemeConvert',
        'modelThemeSeeAll',
        'modelThemeDelete',
        'modelThemeForm',
        'modelThemeSettings',
        'modelThemeAdmin',
    ],
    'PersonToTheme' => [
        'modelPersonToThemeCostController',
    ]
];