<?php

return [
    'buyers' => [
        'name' => Yii::t('BaseModule.PartnerList', 'BuyersListName'),
        'calc_list_func' => function(PartnerList $partner_list) {
            $partner_list->syncPartnersInListByModelFilter([
                'partner_accounts' => [
                    'only_buyers' => true
                ]
            ]);
        },
        'description' => Yii::t('BaseModule.PartnerList', 'BuyersListDesc')
    ],
    'suppliers' => [
        'name' => Yii::t('BaseModule.PartnerList', 'SuppliersListName'),
        'calc_list_func' => function(PartnerList $partner_list) {
            $partner_list->syncPartnersInListByModelFilter([
                'partner_accounts' => [
                    'only_suppliers' => true
                ]
            ]);
        },
        'description' => Yii::t('BaseModule.PartnerList', 'SuppliersListDesc')
    ],
    'hr_candidates' => [
        'name' => Yii::t('BaseModule.PartnerList', 'HRCandidatesListName'),
        'calc_list_func' => function(PartnerList $partner_list) {
            $partner_list->syncPartnersInListByModelFilter([
                'person' => [
                    'employee_candidate' => []
                ]
            ]);
        },
        'description' => Yii::t('BaseModule.PartnerList', 'HRCandidatesListDesc')
    ],
    'employees' => [
        'name' => Yii::t('BaseModule.PartnerList', 'EmployeesListName'),
        'calc_list_func' => function(PartnerList $partner_list) {
            $partner_list->syncPartnersInListByModelFilter([
                'person' => [
                    'employee' => [
                        'work_contracts' => [
                            'currently_active' => true
                        ]
                    ]
                ]
            ]);
        },
        'description' => Yii::t('BaseModule.PartnerList', 'EmployeesListDesc')
    ],
    'former_employees' => [
        'name' => Yii::t('BaseModule.PartnerList', 'FormerEmployeesListName'),
        'calc_list_func' => function(PartnerList $partner_list) {
            $partner_list->syncPartnersInListByModelFilter([
                'person' => [
                    'under_contract' => false,
                    'employee' => [
                        'work_contracts' => []
                    ]
                ]
            ]);
        },
        'description' => Yii::t('BaseModule.PartnerList', 'FormerEmployeesListDesc')
    ],
    'users' => [
        'name' => Yii::t('BaseModule.PartnerList', 'UsersListName'),
        'calc_list_func' => function(PartnerList $partner_list) {
            $partner_list->syncPartnersInListByModelFilter([
                'person' => [
                    'user' => []
                ]
            ]);
        },
        'description' => Yii::t('BaseModule.PartnerList', 'UsersListDesc')
    ],
];