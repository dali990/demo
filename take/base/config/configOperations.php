<?php

return array(
    'address_book_read' => array(
        'bizrule' => 'true',
        'tql_limits' => array(
            array(
                'level' => 40,
                'type' => 'LimitTAG',
                'name' => 'address_book'
            ),
            array(
                'level' => 20,
                'type' => 'LimitTAG',
                'name' => 'address_book_light'
            )
        ),
        'description' => 'Pristup fajlovima iz imenika. Ukoliko osoba nije u sistemu, moze da se menja slika'
    ),
    'comments_add_all_listeners' => array(
        'description' => 'Osoba moze da doda sve zaposlene da osluskuju temu u komentarima'
    ),
    'AddPhoneNumberTypes' => [
        'description' => Yii::t('BaseModule.PhoneNumber', 'AddPhoneNumberTypesOperation')
    ],
    'AddressbookMerge' => [
        'description' => 'Osoba moze da radi merge drzava, gradova, ...'
    ]
);
