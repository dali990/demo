<?php

$profile = [];
$addressbook = [];
if (!Yii::app()->configManager->get('base.develop.use_new_layout', false))
{
    $profile = [
        'order' => 10,
        'label' => Yii::app()->user->model->confParamValue('base.profile_alias', false),
        'img' => Yii::app()->getAssetManager()->getPublishedUrl(Yii::getPathOfAlias('base.assets')) . '/images/top_bar/mainmenu/profile.png',
        'url' => 'javascript:sima.mainTabs.openNewTab("addressbook/partner", '.Yii::app()->user->id.')',
        'subitems' => [
            [
                'label' => Yii::t('BaseModule.Theme', 'Themes'),
                'url' => 'javascript:sima.mainTabs.openNewTab("base/theme/forUser",'.Yii::app()->user->id.');',
                'order' => 15
            ],
            [
                'label' => Yii::t('MainMenu', 'UserSettings'),
                'url' => 'javascript:sima.mainTabs.openNewTab("addressbook/partner",'.Yii::app()->user->id.',[{simaTabs:"configurations"}]);',
                'order' => 20
            ]
        ]
    ];
    $addressbook = [
        'order' => 120,
        'label' => Yii::t('MainMenu', 'AddressBook'),
        'img' => Yii::app()->getAssetManager()->getPublishedUrl(Yii::getPathOfAlias('base.assets')) . '/images/top_bar/mainmenu/address.png',
        'url' => 'javascript:sima.mainTabs.openNewTab("simaAddressbook/index");',
        'visible' => Yii::app()->user->checkAccess('menuShowAddressBook')
    ];
}

return [
    'base' => [],
    'profile' => $profile,
    'addressbook' => $addressbook
];