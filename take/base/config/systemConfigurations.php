<?php

return array(
    'display_name' => Yii::t('BaseModule.Configuration', 'Base'),
    [
        'name' => 'measurement_unit',
        'display_name' => Yii::t('BaseModule.Configuration', 'Measurement units'),
        'type' => 'group',
        'type_params' => [
            [
                'name' => 'kg',
                'display_name' => Yii::t('BaseModule.Configuration', 'Kilogram'),
                'type'=>'searchField',
                'type_params' => array(
                    'modelName'=>'MeasurementUnit',
                ),
                'value_not_null'=>true,
                'default' => [
                    'name' => 'kg'
                ],
                'description'=>'Jedinica mere koja predstavlja kilogram'
            ],
            [
                'name' => 'm',
                'display_name' => Yii::t('BaseModule.Configuration', 'Meter'),
                'type'=>'searchField',
                'type_params' => array(
                    'modelName'=>'MeasurementUnit',
                ),
                'value_not_null'=>true,
                'default'=>[
                    'name' => 'm'
                ],
                'description'=>'Jedinica mere koja predstavlja kilogram'
            ],
            [
                'name' => 'm2',
                'display_name' => Yii::t('BaseModule.Configuration', 'Square meter'),
                'type'=>'searchField',
                'type_params' => array(
                    'modelName'=>'MeasurementUnit',
                ),
                'value_not_null'=>true,
                'default'=>[
                    'name' => 'm2'
                ],
                'description'=>'Jedinica mere koja predstavlja kvadratni metar'
            ],
            [
                'name' => 'm3',
                'display_name' => Yii::t('BaseModule.Configuration', 'Cubic meter'),
                'type'=>'searchField',
                'type_params' => array(
                    'modelName'=>'MeasurementUnit',
                ),
                'value_not_null'=>true,
                'default'=>[
                    'name' => 'm3'
                ],
                'description'=>'Jedinica mere koja predstavlja kubni metar'
            ],
            [
                'name' => 'pcs',
                'display_name' => Yii::t('BaseModule.Configuration', 'Piece'),
                'type'=>'searchField',
                'type_params' => array(
                    'modelName'=>'MeasurementUnit',
                ),
                'value_not_null'=>true,
                'default'=>[
                    'name' => Yii::t('BaseModule.Configuration', 'pcs')
                ],
                'description'=>'Jedinica mere koja predstavlja komad'
            ],
            [
                'name' => 'kwh',
                'display_name' => Yii::t('BaseModule.Configuration', 'kWh'),
                'type'=>'searchField',
                'type_params' => array(
                    'modelName'=>'MeasurementUnit',
                ),
                'value_not_null'=>true,
                'default'=>[
                    'name' => Yii::t('BaseModule.Configuration', 'kWh')
                ],
                'description'=>'Jedinica mere koja predstavlja kilovat cas'
            ],
            [
                'name' => 'minute',
                'display_name' => Yii::t('BaseModule.Configuration', 'Minute'),
                'type'=>'searchField',
                'type_params' => array(
                    'modelName'=>'MeasurementUnit',
                ),
                'value_not_null'=>true,
                'default'=>[
                    'name' => Yii::t('BaseModule.Configuration', 'min')
                ],
                'description'=>'Jedinica mere koja predstavlja kilovat cas'
            ],
            [
                'name' => 'tour',
                'display_name' => Yii::t('BaseModule.Configuration', 'Tour'),
                'type'=>'searchField',
                'type_params' => array(
                    'modelName'=>'MeasurementUnit',
                ),
                'value_not_null'=>true,
                'default'=>[
                    'name' => Yii::t('BaseModule.Configuration', 'tour')
                ],
                'description'=>'Jedinica mere koja predstavlja kilovat cas'
            ],
        ]
    ],
    array(
        'name' => 'errors_receiver_id',
        'display_name' => Yii::t('BaseModule.Configuration', 'ErrorsReceiver'),
        'type'=>'searchField',
        'type_params' => array(
            'modelName'=>'User',
        ),
        'value_not_null'=>false,
        'description'=>'Primalac obavestenja o prijavama novih greski u sistemu'
    ),
    array(
        'name' => 'system_company_id',
        'display_name' => Yii::t('BaseModule.Configuration', 'SystemCompany'),
        'type'=>'searchField',
        'type_params' => array(
            'modelName'=>'Company',
        ),
        'value_not_null'=>true,
        'description'=>'Sistemska kompanija'
    ),
    array(
        'name' => 'system_country_id',
        'display_name' => Yii::t('BaseModule.Configuration', 'LocalCountry'),
        'type'=>'searchField',
        'type_params' => array(
            'modelName'=>'Country',
        ),
        'value_not_null'=>true,
//        'default' => [
//            'name' => 'Srbija',
//            'numeric_code' => '688',
//            'area_code' => '+381',
//            'domain_name' => 'rs'
//        ],
        'description'=>'Lokalna drzava'
    ),
    array(
        'name' => 'language',
        'user_changable' => true,
        'display_name'=> Yii::t('BaseModule.Configuration', 'Language'),
        'type'=>'dropdown',
        'type_params'=>array(
            array(
                'title'=>'English',
                'value'=>'en'
            ),
            array(
                'title'=>'Srpski',
                'value'=>'sr-Latn'
            ),
            array(
                'title'=>'Српски',
                'value'=>'sr-Cyrl'
            ),
        ),
        'default' => 'sr-Latn',
        'description'=>'Biranje jezika na kome će se prikazivati informacioni sistem SIMA'
    ),
    array(
        'name' => 'profile_alias',
        'user_changable' => true,
        'display_name'=>'Labela Profil',
        'type'=>'textField',
        'default'=>'Profil',
        'description'=>'Parametar koji oznacava sta ce da pise u meni labeli za profil'
    ),
    array(
        'name' => 'amount_field_thousands',
        'user_changable' => true,
        'display_name'=>'Separator za hiljadarke u polju za iznos',
        'type'=>'dropdown',
        'type_params' => array(
            array(
                'title'=>'Znak',
                'value'=>'sign'
            ),
            array(
                'title'=>'Praznina',
                'value'=>'space'
            ),
        ),
        'default' => 'space',
        'description'=>'Parametar koji oznacava sta ce biti separator za hiljadarke u polju za iznos'
    ),
    array(
        'name' => 'amount_field_decimals',
        'user_changable' => true,
        'display_name'=>'Separator za decimale u polju za iznos',
        'type'=>'dropdown',
        'type_params'=>array(
          array(
              'title'=>'.',
              'value'=>'.'
          ),
          array(
              'title'=>',',
              'value'=>','
          ),
        ),
        'default'=>'.',
        'description'=>'Parametar koji oznacava sta ce biti separator za decimale u polju za iznos'
    ),
    array(
        'name' => 'forbiden_document_types',
        'display_name' => 'Zabranjeni tipovi dokumenata',
        'type' => 'searchField',
        'type_params' => array(
            'modelName'=>'DocumentType',
        ),
        'value_is_array'=>true,
        'description'=>'Tipovi dokumenata koji se vise ne koriste, ali ukoliko neki dokument vec ima taj tip, nece smetati'
    ),
    array(
        'name' => 'types_for_any_filing_direction',
        'display_name' => 'Dokumenti za bilo koji smer',
        'type' => 'searchField',
        'type_params' => array(
            'modelName'=>'DocumentType',
        ),
        'value_is_array'=>true,
        'description'=>'Tipovi dokumenata koji mogu da se koriste za bilo koji smer'
    ),
    array(
        'name' => 'weekend_day_type_id',
        'display_name'=>'Tip dana koji predstavlja vikend',
        'type'=>'searchField',
        'type_params' => array(
            'modelName'=>'DayType',
        ),
        'value_not_null'=>true,
        'default' => [
            'name' => 'weekend',
            'work_hours' => 0
        ],
        'description'=>'Tip dana koji predstavlja vikend'
    ),
    array(
        'name' => 'work_day_type_id',
        'display_name'=>'Tip dana koji predstavlja radni dan',
        'type'=>'searchField',
        'type_params' => array(
            'modelName'=>'DayType',
        ),
        'value_not_null'=>true,
        'default' => [
            'name' => 'work_day',
            'work_hours' => 8
        ],
        'description'=>'Tip dana koji predstavlja radni dan'
    ),
    array(
        'name' => 'style_menu_items',
        'user_changable' => true,
        'display_name'=>'Stil prikaza stavki u glavnom meniju',
        'type'=>'dropdown',
        'type_params'=>array(
          array(
              'title'=>'Ikonice i nazivi',
              'value'=>'icon_and_title'
          ),
          array(
              'title'=>'Ikonice',
              'value'=>'icon'
          )  
        ),
        'default'=>'icon_and_title',
        'description'=>'Moze se odabrati nacin prikaza stavki u glavnom meniju, ikonice i nazivi ili samo ikonice.'
    ),
    array(
        'name' => 'country_currency',
        'display_name' => Yii::t('BaseModule.Configuration', 'CountryCurrency'),
        'type'=>'searchField',
        'type_params' => array(
            'modelName'=>'Currency',
        ),
        'description' =>Yii::t('BaseModule.Configuration', 'CountryCurrencyDescription'),
        'value_not_null' => true
    ),
    array(
        'name' => 'sima_size',
        'user_changable' => true,
        'display_name' => Yii::t('BaseModule.Configuration', 'SimaSize'),
        'type'=>'dropdown',
        'type_params'=>array(
          array(
              'title'=>Yii::t('BaseModule.Configuration', 'SimaSize1'),
              'value'=>'1'
          ),
          array(
              'title'=>Yii::t('BaseModule.Configuration', 'SimaSize2'),
              'value'=>'2'
          ),
          array(
              'title'=>Yii::t('BaseModule.Configuration', 'SimaSize3'),
              'value'=>'3'
          )
        ),
        'default'=>'1',
        'description' => Yii::t('BaseModule.Configuration', 'SimaSizeDescription')
    ),
    [
        'name' => 'use_new_basicinfo',
        'display_name' => Yii::t('BaseModule.Configuration', 'UseNewBasicinfo'),
        'description' =>Yii::t('BaseModule.Configuration', 'UseNewBasicinfoDescription'),
        'value_not_null' => true,
        'type' => 'boolean',
        'default' => false
    ],
    [
        'name' => 'js_log_sda_session_chose',
        'display_name' => Yii::t('BaseModule.Configuration', 'JsLogSdaSessionChose'),
        'description' =>Yii::t('BaseModule.Configuration', 'JsLogSdaSessionChoseDescription'),
        'value_not_null' => true,
        'type' => 'boolean',
        'default' => false
    ],
    [
        'name' => 'use_vue_components',
        'display_name' => Yii::t('BaseModule.Configuration', 'UseVueComponents'),
        'description' =>Yii::t('BaseModule.Configuration', 'UseVueComponentsDescription'),
        'value_not_null' => true,
        'type' => 'boolean',
        'default' => false,
        'user_changable' => true
    ],
    [
        'name' => 'model_displayhtml_click_open',
        'user_changable' => true,
        'display_name' => Yii::t('BaseModule.Configuration', 'ModelDisplayhtmlClickOpen'),
        'type' => 'dropdown',
        'type_params' => [
            [
                'title' => 'ctrl+klik',
                'value' => 'ctrl+klik'
            ],
            [
                'title' => 'klik',
                'value' => 'klik'
            ]
        ],
        'default' => 'ctrl+klik',
        'description' => Yii::t('BaseModule.Configuration', 'ModelDisplayhtmlClickOpenDesc'),
    ],
    [
        'name' => 'detach_proces_for_every_notification',
        'user_changable' => false,
        'display_name' => 'Salji notifikacije preko SystemEvent',
        'type' => 'boolean',
        'default' => false,
        'description' => 'Salji notifikacije preko SystemEvent'
    ],
    [
        'name' => 'use_new_tinymce',
        'display_name' => Yii::t('BaseModule.Configuration', 'UseNewTinyMce'),
        'description' =>Yii::t('BaseModule.Configuration', 'UseNewTinyMceDescription'),
        'value_not_null' => true,
        'type' => 'boolean',
        'default' => true,
        'user_changable' => false
    ],
    [
        'name' => 'tinymce_max_file_size_upload',
        'display_name' => Yii::t('BaseModule.Configuration', 'TinymceMaxFileSizeUpload'),
        'description' =>Yii::t('BaseModule.Configuration', 'LimitTinymceFileSizeUploadInMB'),
        'type' => 'numeric',
        'type_params' => [
            'scale' => 0
        ],
        'default' => 5
    ],
//    [
//        'name' => 'update_models_via_db',
//        'display_name' => Yii::t('BaseModule.Configuration', 'UpdateModelsViaDB'),
//        'description' => Yii::t('BaseModule.Configuration', 'UpdateModelsViaDBDesc'),
//        'value_not_null' => true,
//        'user_changable' => false,
//        'type' => 'boolean',
//        'default' => false
//    ],
    [
        'name' => 'sql_log_function_disable',
        'display_name' => Yii::t('BaseModule.Configuration', 'SQLLogFunctionDisable'),
        'description' => Yii::t('BaseModule.Configuration', 'SQLLogFunctionDisableDesc'),
        'value_not_null' => true,
        'user_changable' => false,
        'type' => 'boolean',
        'default' => false
    ],
    [
        'name' => 'eventrelay',
        'display_name' => Yii::t('BaseModule.Configuration', 'Eventrelay'),
        'type' => 'group',
        'type_params' => [
            [
                'name' => 'js_log_every_received_message',
                'display_name' => Yii::t('BaseModule.Configuration', 'EventrelayJsLogEveryReceivedMessage'),
                'description' =>Yii::t('BaseModule.Configuration', 'EventrelayJsLogEveryReceivedMessageDescription'),
                'value_not_null' => true,
                'type' => 'boolean',
                'default' => false
            ],
            [
                'name' => 'log_to_db',
                'display_name' => Yii::t('BaseModule.Configuration', 'EventRelayLogToDB'),
                'description' => Yii::t('BaseModule.Configuration', 'EventRelayLogToDBDesc'),
                'value_not_null' => true,
                'user_changable' => false,
                'type' => 'boolean',
                'default' => true
            ],
            [
                'name' => 'log_messages',
                'display_name' => Yii::t('BaseModule.Configuration', 'EventRelayLogMessages'),
                'description' => Yii::t('BaseModule.Configuration', 'EventRelayLogMessagesDesc'),
                'value_not_null' => true,
                'user_changable' => false,
                'type' => 'boolean',
                'default' => false
            ],
            [
                'name' => 'js_send_autobaf_on_disconnect_reconnecting',
                'display_name' => Yii::t('BaseModule.Configuration', 'JsSendAutobafOnDisconnectReconnecting'),
                'description' => Yii::t('BaseModule.Configuration', 'JsSendAutobafOnDisconnectReconnectingDesc'),
                'value_not_null' => true,
                'user_changable' => true,
                'type' => 'boolean',
                'default' => false
            ],
            [
                'name' => 'js_consolelog_keepalive_send',
                'display_name' => Yii::t('BaseModule.Configuration', 'JsConsolelogKeepaliveSend'),
                'description' => Yii::t('BaseModule.Configuration', 'JsConsolelogKeepaliveSendDesc'),
                'value_not_null' => true,
                'user_changable' => true,
                'type' => 'boolean',
                'default' => false
            ],
            [
                'name' => 'messages_to_send_number',
                'display_name' => Yii::t('BaseModule.Configuration', 'EventRelayMessagesToSendNumber'),
                'description' => Yii::t('BaseModule.Configuration', 'EventRelayMessagesToSendNumberDesc'),
                'value_not_null' => true,
                'user_changable' => false,
                'type' => 'numeric',
                'type_params' => [
                    'scale' => 0
                ],
                'default' => 1
            ],
            [
                'name' => 'messagequeue_exact_count_to_log',
                'display_name' => Yii::t('BaseModule.Configuration', 'EventRelayMessageQueueExactCountToLog'),
                'description' => Yii::t('BaseModule.Configuration', 'EventRelayMessageQueueExactCountToLogDesc'),
                'value_not_null' => true,
                'user_changable' => false,
                'type' => 'numeric',
                'type_params' => [
                    'scale' => 0
                ],
                'default' => 1000
            ],
            [
                'name' => 'max_messagesending_exec_time_s',
                'display_name' => Yii::t('BaseModule.Configuration', 'EventRelayMaxMessageSendingExecTimeS'),
                'description' => Yii::t('BaseModule.Configuration', 'EventRelayMaxMessageSendingExecTimeSDesc'),
                'value_not_null' => true,
                'user_changable' => false,
                'type' => 'numeric',
                'type_params' => [
                    'scale' => 10
                ],
                'default' => 1
            ],
            [
                'name' => 'sendmessage_max_prev_start_time_diff_s',
                'display_name' => Yii::t('BaseModule.Configuration', 'EventRelaySendMessageMaxPrevStartTimeDiffS'),
                'description' => Yii::t('BaseModule.Configuration', 'EventRelaySendMessageMaxPrevStartTimeDiffSDesc'),
                'value_not_null' => true,
                'user_changable' => false,
                'type' => 'numeric',
                'type_params' => [
                    'scale' => 10
                ],
                'default' => 1
            ],
            [
                'name' => 'push_logs_to_db_s',
                'display_name' => Yii::t('BaseModule.Configuration', 'PushLogsToDbS'),
                'description' => Yii::t('BaseModule.Configuration', 'PushLogsToDbSDesc'),
                'value_not_null' => true,
                'user_changable' => false,
                'type' => 'numeric',
                'type_params' => [
                    'scale' => 2
                ],
                'default' => 60
            ],
        ]
    ], /// eventrelay
    [
        'name' => 'develop',
        'display_name' => Yii::t('BaseModule.Configuration', 'Develop'),
        'type' => 'group',
        'type_params' => [
            [
                'name' => 'new_comment_unread2',
                'display_name' => 'Korisnik nije procitao komentar koji je on poslao',
                'value_not_null' => true,
                'type' => 'boolean',
                'default' => false,
                'user_changable' => true,
                'description' => 'Konfiguracija za slucaj kada neki korisnik posalje komentar njemu se prikaze da nije procitao taj komentar, iako ga je on poslao. '
                    . 'Ukljucivanjem ove konfiguracije nece se vrsiti autosave draft poruke.'
            ],
            [
                'name' => 'new_tab_unreleased',
                'display_name' => 'Novi tab likvidacije',
                'value_not_null' => true,
                'type' => 'boolean',
                'default' => false,
                'description' => 'Da se vidi novi tab likvidacije'
            ],
            [
                'name' => 'hide_old_tab_bill_items',
                'display_name' => 'Sakrij stari tab stavke racuna',
                'value_not_null' => true,
                'type' => 'boolean',
                'default' => true,
                'description' => 'Da se NE vidi STARI tab stavke racuna'
            ],
            [
                'name' => 'use_tinymce_vue',
                'display_name' => Yii::t('BaseModule.Configuration', 'UseTinyMceVue'),
                'description' =>Yii::t('BaseModule.Configuration', 'UseTinyMceVueDescription'),
                'value_not_null' => true,
                'type' => 'boolean',
                'default' => false,
                'user_changable' => true
            ],
            [
                'name' => 'use_jquery_3_3_1',
                'display_name' => 'Koristiti jquery 3.3.1',
                'value_not_null' => true,
                'user_changable' => true,
                'type' => 'boolean',
                'default' => false
            ],
            [
                'name' => 'use_comments_vue',
                'display_name' => 'Koristiti komentare u vue',
                'value_not_null' => true,
                'type' => 'boolean',
                'default' => false,
                'user_changable' => true
            ],
            [
                'name' => 'tql_complex_query_return_plain',
                'display_name' => 'TQL queryoptimizator umesto complexquery da vrati original plain query',
                'value_not_null' => true,
                'type' => 'boolean',
                'default' => false,
                'user_changable' => false
            ],
            [
                'name' => 'ajax_sync_action_consolelog',
                'display_name' => 'ajax_sync_action_consolelog',
                'value_not_null' => true,
                'type' => 'boolean',
                'default' => true,
                'user_changable' => false
            ],
            [
                'name' => 'ajax_sync_action_autobaf',
                'display_name' => 'ajax_sync_action_autobaf',
                'value_not_null' => true,
                'type' => 'boolean',
                'default' => false,
                'user_changable' => false
            ],
//            [
//                'name' => 'sma_events_over_eventrelay',
//                'display_name' => 'sma_events_over_eventrelay',
//                'value_not_null' => true,
//                'type' => 'boolean',
//                'default' => false,
//                'user_changable' => false
//            ],
            [
                'name' => 'use_get_files_query_for_dir_query2',
                'display_name' => 'use_get_files_query_for_dir_query2',
                'value_not_null' => true,
                'type' => 'boolean',
                'default' => false,
                'user_changable' => false
            ],
            [
                'name' => 'tql_queryoptimizatorcache_use_new_last_access',
                'display_name' => Yii::t('FilesModule.Configuration', 'TqlQueryoptimizatorcacheUseNewLastAccess'),
                'value_not_null'=>true,
                'type'=>'boolean',
                'default' => false,
                'description' => Yii::t('FilesModule.Configuration', 'TqlQueryoptimizatorcacheUseNewLastAccessDESC'),
                'user_changable' => false,
            ],
            [
                'name' => 'use_new_layout',
                'display_name' => 'Novi layout',
                'value_not_null' => true,
                'type' => 'boolean',
                'default' => false,
                'user_changable' => true
            ],
            [
                'name' => 'perform_work_for_cron_job_tester',
                'display_name' => Yii::t('FilesModule.Configuration', 'PerformWorkForCronJobTester'),
                'value_not_null'=>true,
                'type'=>'boolean',
                'default' => false,
                'description' => Yii::t('FilesModule.Configuration', 'PerformWorkForCronJobTesterDESC'),
                'user_changable' => false,
            ],
//            [
//                'name' => 'use_after_work',
//                'display_name' => Yii::t('BaseModule.Configuration', 'UseAfterWork'),
//                'value_not_null' => true,
//                'type' => 'boolean',
//                'default' => false,
//                'description' => Yii::t('BaseModule.Configuration', 'UseAfterWorkDesc'),
//                'user_changable' => false
//            ],
            [
                'name' => 'use_test_classes_in_eventrelay',
                'display_name' => Yii::t('BaseModule.Configuration', 'UseTestClassesInEventRelay'),
                'value_not_null' => false,
                'type' => 'boolean',
                'default' => false,
                'description' => Yii::t('BaseModule.Configuration', 'UseTestClassesInEventRelayDesc'),
                'user_changable' => false
            ],
//            [
//                'name' => 'markmodelupdateeventswaiting_same_process',
//                'display_name' => Yii::t('BaseModule.Configuration', 'MarkmodelupdateeventswaitingSameProcess'),
//                'value_not_null' => false,
//                'type' => 'boolean',
//                'default' => false,
//                'description' => Yii::t('BaseModule.Configuration', 'MarkmodelupdateeventswaitingSameProcessDesc'),
//                'user_changable' => false
//            ]
        ]
    ], /// develop
    [
        'name' => 'mainmenu_last_opened_module_tab',
        'display_name' => Yii::t('BaseModule.Configuration', 'MainmenuLastOpenedModuleTab'),
        'description' => Yii::t('BaseModule.Configuration', 'MainmenuLastOpenedModuleTabDesc'),
        'type' => 'textField',
        'default' => 'opened_items',
        'user_changable' => true,
        'display_to_users' => false
    ],
    [
        'name' => 'favorite_currencies',
        'display_name' => Yii::t('BaseModule.Configuration', 'FavoriteCurrencies'),
        'description' => Yii::t('BaseModule.Configuration', 'FavoriteCurrencies'),
        'type' => 'searchField',
        'type_params' => [
            'modelName' => Currency::class
        ],
        'user_changable' => false,
        'value_not_null' => false,
        'value_is_array' => true
    ]
);
