<?php

return [
    'WARN_NO_EMPTY_CONFIG_PARAM' => [
        'title' => 'Broj konfiguracionih parametara koji moraju biti postavljeni',
        'onclick' => ['sima.mainTabs.openNewTab','base/configuration/mustSetNulledParams'],
        'countFunc' => function($user) {
            $cnt = 0;
            if (Yii::app()->authManager->checkAccess('ConfigParamsWarning',$user->id))
            {
                $params = Yii::app()->configManager->mustSetNulledParams();
                $cnt = count($params);
            }
            
            return $cnt;
        }
    ],
    'WARN_NOT_SEND_CLIENT_BAF_REQUESTS' => [
        'title' => Yii::t('BaseModule.ClientBafRequest','UnsendClientBafRequestsCount'),
        'onclick' => ['sima.mainTabs.openNewTab','base/errorReports/unsendCurrentPartnerClientBafRequests/'],
        'countFunc' => function($user) {
            $cnt = ClientBafRequest::model()->count(new SIMADbCriteria([
                'Model'=>'ClientBafRequest',
                'model_filter'=>[
                    'requested_by'=>['ids'=>$user->id],
                    'server_baf_request_id'=>'null'
                ]
            ]));
            
            return $cnt;
        }
    ],
];

