<div id='panel-<?=$uniq?>' class="sima-layout-panel sima-contact-converter">
        <?php
        $additional_options_html = '';
        if(SIMAMisc::isVueComponentEnabled())
        {
            $additional_options_html = Yii::app()->controller->widget(SIMAButtonVue::class, [
                'title' => Yii::t('SIMAHtml', 'Save'),
                'onclick' => ['sima.contactConverter.saveContacts', $uniq]
            ], true);
        }
        else
        {
            $additional_options_html = Yii::app()->controller->widget('SIMAButton', [
                'action' => [ 
                    'title' => Yii::t('SIMAHtml', 'Save'),
                    'onclick' => ['sima.contactConverter.saveContacts', $uniq]
                ]                    
            ], true);
        }
        
        
            $this->widget(SIMAGuiTable::class, [
                'id' => 'contact-gui-table'.$uniq,   
                'model' => Contact::class,     
                'columns_type' => 'contact_convertor',
                'table_settings' => false,
                'show_add_button' => false,
                'additional_options_html' => $additional_options_html
            ]);

        ?>
</div>
