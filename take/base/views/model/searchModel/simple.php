
<?php 
    if (Yii::app()->request->isAjaxRequest) Yii::app()->clientScript->clearScriptFiles();
?>
    <?php     
        $uniq = SIMAHtml::uniqid();
        $local_params = [];
        $local_params['model'] = get_class($model);
        $local_params['params'] = $params;
        $local_params['filter'] = $simple_filter;
        if(isset($params['search_action']))
        {
            $local_params['search_action'] = $params['search_action'];
        }
        else
        {
            $local_params['multiselect'] = false;
            $local_params['view'] = 'guiTable';
        }
    ?>
<p style='max-width: 455px; margin: 10px 30px 20px 30px;'><?php echo $title?></p>
    <div>
        <span  style='display: inline-block; width: 20px;'></span>
        <span style="display: inline-block;">
            <?php
                Yii::app()->controller->widget('ext.SIMASearchField.SIMASearchField', [
                    'id'=>$uniq,
                    'model'=>$model, 
                    'filters'=>$simple_filter
                ]);
            ?>
        </span>
        <?php if (!isset($params['add_button']) || $params['add_button'] === true) { ?>
            <span style='display: inline-block;' class='sima-icon _search_form _16' onclick='sima.misc.searchFieldAddButton("<?php echo $uniq; ?>",<?php echo CJSON::encode($local_params); ?>);'></span>
        <?php } ?>
            
    </div>    
<?php 
if (Yii::app()->request->isAjaxRequest) Yii::app()->clientScript->clearScriptFiles();
?>