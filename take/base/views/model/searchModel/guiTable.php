
<?php

    $guitable_params = [
        'id'=>isset($params['guitable_id']) ? $params['guitable_id'] : "left_tbl_$uniq_id",
        'class' => 'sima-model-choose-left-guitable',
        'model'=>$model,
        'title' => Yii::t('BaseModule.Misc', 'ModelChooseGuitableChooseItems'),
        'add_button'=>isset($params['add_button'])?$params['add_button']:true,
        'setRowDblClick' => ['sima.model.onChooseModelDblClick', 'LEFT', $multiselect]
    ];

    $guitable_params['fixed_filter'] = [];
    if (isset($fixed_filter))
    {
        $guitable_params['fixed_filter'] = $fixed_filter;
    }
    else if (isset($filter))
    {
        $guitable_params['fixed_filter'] = $filter;
    }
    
    if (isset($select_filter))
    {
        $guitable_params['select_filter'] = $select_filter;
    }
    if(isset($params['columns_type']))
    {
        $guitable_params['columns_type'] = $params['columns_type'];
    }
    if(isset($params['custom_buttons']))
    {
        $guitable_params['custom_buttons'] = $params['custom_buttons'];
    }
    
    if ($multiselect === true)
    {
        $search_model_ids = !empty($search_model_ids) ? $search_model_ids : -1;
        //Sasa A. - indekse sam stavio jer mi trebaju za javascript. PHP automatski dodeljuje ove indekse, ali sam ih stavio za svaki slucaj ako se promeni redosled
        $guitable_params['fixed_filter'] = [
            0 => 'AND',
            1 => [
                0 => 'NOT', 
                1 => [
                    'ids' => $search_model_ids
                ]
            ],
            2 => $guitable_params['fixed_filter']
        ];
    }

    $this->widget('SIMAGuiTable', $guitable_params);