<?php

$search_model_ids = !empty($search_model_ids) ? $search_model_ids : -1;
$this->widget('SIMAGuiTable', [
    'id' => "right_tbl_$uniq_id",
    'class' => 'sima-model-choose-right-guitable',
    'model' => $model,
    'title' => Yii::t('BaseModule.Misc', 'ModelChooseGuitableItemsThatYouChoose'),
    'add_button' => false,
    'fixed_filter' => [
        'ids' => $search_model_ids
    ],
    'setRowDblClick' => ['sima.model.onChooseModelDblClick', 'RIGHT', true]
]);

