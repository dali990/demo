<?php
echo $this->renderContentLayout([
        'name'=> $this->renderModelView($model, 'title'),                
        'options' => [
            [
                'html' => $this->renderModelView($model, 'options')
            ]
        ]
    ],
    [
        [
            'proportion' => 0.2,
            'html' => $this->renderModelView($model, 'basicInfo')
        ],
        [
            'proportion' => 0.8,
            'html'=> $this->widget('SIMATabs', $tabs_params, true)
        ]
    ],
    [
//        'title_name_info_box' => $this->renderModelView($model, 'title')
    ]
);
