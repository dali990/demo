<?php

foreach ($model->getAttributes() as $attribute_name=>$attribute_value)
{
    ?>
        <div class='row <?php echo $attribute_name; ?>'>
            <div class='title'><?php echo $model->getAttributeLabel($attribute_name) ?>:</div>
            <div class='data'><?php echo $model->getAttributeDisplay($attribute_name); ?></div>
        </div>
    <?php
}

