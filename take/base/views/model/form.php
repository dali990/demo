<?php
    if (Yii::app()->request->isAjaxRequest)
    {
        Yii::app()->clientScript->clearScriptFiles();
    }
?>

<div class="sima-model-form" data-model_name="<?= get_class($model)?>" data-model_id="<?=$model->id?>">

    <?php
        $form = $this->beginWidget('CActiveForm', array(
        'id' => $form_id,
        'enableClientValidation' => true,
        'htmlOptions' => [
            //Sasa A. - ispravka za slucaj kada u formi imamo samo jedno vidljivo polje i kada je fokus na njemu i kada se pritisne enter onda se 
            //javlja greska "your request is invalid". Ne znam zasto se to desava ali sam resio tako sto sam dodao onsubmit false
            'onsubmit'=>"return false;"
        ]
    ));
    ?>
    <?php 
        $forceRefresh = false;
        $fullScreen = false;
        $calcDate = false;
        
        foreach ($columns as $column => $col_conf) 
        {
            $htmlAfter = '';
            $wiki = '';
            $personal_wiki = '';
            $placeHolder = $model->getAttributeLabel($column);
            $htmlOptions = array();
            
            if (isset($filters[$column]) && is_array($filters[$column]))
            {
                if (in_array('disabled', $filters[$column]) || (isset($filters[$column]['disabled']) && $filters[$column]['disabled'] !== 'false'))
                {
                    $htmlOptions['disabled'] = 'disabled';
                }
                if (in_array('focus', $filters[$column]) || isset($filters[$column]['focus']))
                {
                    $htmlOptions['autofocus'] = 'autofocus';
                }
                if (in_array('hidden', $filters[$column]) || isset($filters[$column]['hidden']))
                {
                    $col_conf = 'hidden';
                }
                if (isset($filters[$column]['placeholder']))
                {
                    $htmlOptions['placeholder'] = $filters[$column]['placeholder'];
                }
            }
            
            if (gettype($col_conf)==='string') 
            {
                $col_type = $col_conf;
            }
            else if (gettype($col_conf)==='array')
            {
                if (count($col_conf)<1) 
                {
                    throw new Exception('forme nisu lepo definisane za model '.get_class($model).' - malo argumenata u nizu');
                }
                $col_type = $col_conf[0];
                if (isset($col_conf['htmlAfter'])) 
                {
                    $htmlAfter = $col_conf['htmlAfter'];
                }
                if (isset($col_conf['placeholder']) && !isset($htmlOptions['placeholder']))
                {
                    $htmlOptions['placeholder'] = $col_conf['placeholder'];
                }
                if (isset($col_conf['disabled']) && $col_conf['disabled'] !== false) 
                {
                    $htmlOptions['disabled'] = 'disabled';
                }
                if(isset($col_conf['value']) && $col_conf['value']=='checked')
                {
                     $htmlOptions['checked']=$col_conf['value'];
                }
                if (isset($col_conf['empty'])) 
                {
                    $htmlOptions['empty'] = $col_conf['empty'];
                }
                if (isset($col_conf['required'])) 
                {
                    $htmlOptions['required'] = $col_conf['required'];
                }
                if (isset($col_conf['wiki']))
                {
                    $wiki = $col_conf['wiki'];
                }
                if (isset($col_conf['personal_wiki']))
                {
                    $personal_wiki = $col_conf['personal_wiki'];
                }
            }
            else    
            {
                throw new Exception('forme nisu lepo definisane za model '.get_class($model));
            }
            
            if (!in_array($col_type, array(
                'textField',
                'textArea',
                'hidden',
                'relation',
                'searchField',
                'dropdown',
                'checkBox',
                'fileField',        
                'passwordField',
                'datetimeField',
                'unitMeasureField',
                'display',
                'form',
                'list',
                'tinymce',
                'numberField',
                'customSearch', //ne koristi se
                'status',
                'checkBoxList',
                'bankAccountField',
                'svpField',
                'phoneField',
                'addressField',
                'partnerField'
            ))) 
            {
                throw new Exception(' tip polja '.$col_type.' nije podrzan u formi za model '.get_class($model));
            }
            
            
            /** pravljenje polja **/
            if ($col_type=='hidden')
            {
                /** pravljenje SKRIVENOG polja **/
                echo '<div class="sima-model-form-row" data-column="'.$column.'" data-type="'.$col_type.'" style="display:none;">';
                echo $form->hiddenField($model,$column);
                echo '</div>';
            }
            else
            {
                if (!empty($wiki) || !empty($personal_wiki))
                {
                    
                    $wiki_action = 'personalWiki';
                    $wiki_page = $personal_wiki;
                    if(!empty($wiki))
                    {
                        $wiki_action = 'docWiki';
                        $wiki_page = $wiki;
                    }
                    echo '<div class="sima-model-form-row has_wiki" data-column="'.$column.'">';
                    echo "<span style='position:relative; z-index:5; margin-right:5px;vertical-align:top;' class='sima-icon _wiki _16' onclick='sima.icons.$wiki_action($(this),\"$wiki_page\");'></span>";
                }
                else
                {
                    echo '<div class="sima-model-form-row" data-column="'.$column.'" data-type="'.$col_type.'">';
                }
                echo $form->labelEx($model, $column, $htmlOptions);
                
                $dependent_element_type = '';
                $dependent_element_selector = '';
                switch($col_type)
                {
                    case 'textField': 
                        if (isset($col_conf['dependent_on']))
                        {
                            if (isset($htmlOptions['id']))
                            {
                                $id = $htmlOptions['id'];
                            }
                            else
                            {
                                $id = SIMAHtml::uniqid();
                                $htmlOptions['id'] = $id;
                            }
                            $dependent_element_type = 'textField';
                            $dependent_element_selector = "#$id";
                        } 
                        echo $form->textField($model, $column,$htmlOptions); break;
                    case 'textArea':  echo $form->textArea($model, $column,$htmlOptions); break;
                    case 'checkBox':  
                        if (isset($col_conf['dependent_on']))
                        {
                            $dependent_element_type = 'checkBox';
                            if (isset($htmlOptions['id']))
                            {
                                $id = $htmlOptions['id'];
                            }
                            else
                            {
                                $id = SIMAHtml::uniqid();
                                $htmlOptions['id'] = $id;
                            }
                            $dependent_element_selector = "#$id";
                        }
                        echo $form->checkBox($model, $column, $htmlOptions); 
                        break;
                    case 'passwordField':  
                        if (isset($col_conf['dependent_on']))
                        {
                            if (isset($htmlOptions['id']))
                            {
                                $id = $htmlOptions['id'];
                            }
                            else
                            {
                                $id = SIMAHtml::uniqid();
                                $htmlOptions['id'] = $id;
                            }
                            $dependent_element_type = 'passwordField';
                            $dependent_element_selector = "#$id";
                        }

                        echo $form->passwordField($model, $column, $htmlOptions); break;
                    case 'fileField': 
                        $relation='file';
                        $repmanager_upload = true;
                        $allowed_extensions=[];
                        $additional_options=[];
                        if (gettype($col_conf)==='array')
                        {
                            if (isset($col_conf['relName'])) 
                            {
                                $relation = $col_conf['relName'];
                            }
                            if (isset($col_conf['repmanager_upload'])) 
                            {
                                $repmanager_upload = $col_conf['repmanager_upload'];
                            }
                            if( isset($col_conf['allowed_extansions']))
                            {                                
                                 $allowed_extensions=$col_conf['allowed_extansions'];
                            }
                            if( isset($col_conf['additional_options']))
                            {                                
                                 $additional_options=$col_conf['additional_options'];
                            }
                        }
                        echo SIMAHtml::fileField($form_id, $form, $model, $column, $relation, $repmanager_upload, $allowed_extensions, $additional_options); 
     
                        break;
                    case 'display': 
                        if ((gettype($col_conf)==='array') && isset($col_conf['modelView'])) 
                        {
                            if ($model_id==='')
                            {
                                echo 'prvo sacuvaj, pa upisi';
                                $forceRefresh = true;
                            }
                            else
                            {
                                echo Yii::app()->controller->renderModelView($model,$col_conf['modelView']);
                            }
                        }
                        else
                        {
                            echo $model->getAttributeDisplay($column); 
                            if (isset($model->$column))
                            {
                                echo $form->hiddenField($model,$column);
                            }
                        }
                        
                        break;
                    case 'unitMeasureField':
                        if (!(gettype($col_conf)==='array') && !isset($col_conf['unit'])) 
                        {
                            throw new Exception('Nije definisana koja je jedinica u pitanju za polje '.$column);
                        }
                        $relName='';
                        $htmlOptions2 = array();
                        if (gettype($col_conf['unit'])==='array')
                        {
                            if (count($col_conf['unit'])<1) 
                            {
                                throw new Exception('forme nisu lepo definisane za model '.get_class($model).' - malo argumenata u nizu');
                            }
                            $unit_type = $col_conf['unit'][0];
                            if (isset($col_conf['unit']['disabled'])) 
                            {
                                $htmlOptions2['disabled'] = $col_conf['unit']['disabled'];
                            }
                            if (isset($col_conf['unit']['empty'])) 
                            {
                                $htmlOptions2['empty'] = $col_conf['unit']['empty'];
                            }
                            if (isset($col_conf['unit']['relName'])) 
                            {
                                $relName = $col_conf['unit']['relName'];
                            }
                        }
                        else if (gettype($col_conf['unit'])==='string')
                        {
                            $unit_type = $col_conf['unit'];
                        }
                        else
                        {
                            throw new Exception('nije lepo definisana jedinica mere za '.$column);
                        }
                        echo "<span class='short'>".$form->textField($model,$column,$htmlOptions)."</span>";
                        echo "<span class='currency'>".$form->dropDownList($model,$unit_type,$model->droplist($unit_type,$relName), $htmlOptions2)."</span>";
                        break;
                    case 'datetimeField': 
                        $conf = [
                            'mode' => 'date',
                            'htmlOptions' => $htmlOptions,
                            'readonly' => true
                        ];
                        $calc_date = false;
                        if (gettype($col_conf)==='array')
                        {
                            if (isset($col_conf['mode'])) 
                            {
                                $conf['mode'] = $col_conf['mode'];
                            }
                            if (in_array('calcDate',$col_conf)) 
                            {
                                $calcDate = $calc_date = true;
                            }
                        }
                        
                        if (isset($col_conf['dependent_on']))
                        {
                            if (isset($htmlOptions['id']))
                            {
                                $id = $htmlOptions['id'];
                            }
                            else
                            {
                                $id = SIMAHtml::uniqid();
                                $conf['htmlOptions']['id'] = $id;
                            }
                            $dependent_element_type = 'datetimeField';
                            $dependent_element_selector = "#$id";
                        }
                        
                        $jquery_options = [];
                        if (isset($col_conf['jquery_options'])) 
                        {
                            $jquery_options = $col_conf['jquery_options'];
                        }

                        echo SIMAHtml::datetimeField($model, $column, $conf, $jquery_options); 
                        if ($calc_date)
                        {
                            ?><span class='sima-model-form-calc' style='font-size:10px;' onclick='deadline_function<?php echo $form_id?>("<?php echo $column?>");'>izracunaj</span><?php }
                        break;
                    case 'relation':  
                    case 'searchField':
                        
                        if (!(gettype($col_conf)==='array') && !isset($col_conf['relName'])) 
                        {
                            throw new Exception('Nije definisana koja relacija je u pitanju za polje '.$column);
                        }
                        $_filters = array();
                        
                        if (isset($col_conf['filters']))
                        {
                            $_filters = $col_conf['filters'];    
                        }
                        
                        if (isset($filters[$column]['model_filter']))
                        {
                            $_filters = $filters[$column]['model_filter'];
                        }
                        
                        $id = SIMAHtml::uniqid();
                        $this->widget('ext.SIMASearchField.SIMASearchField', array(
                            'id'=>$id,
                            'model'=>$model,
                            'relation'=>$col_conf['relName'],
                            'filters' => $_filters,
                            'htmlOptions' => $htmlOptions
                        ));
                        
                        if (isset($col_conf['dependent_on']))
                        {
                            $dependent_element_type = 'searchField';
                            $dependent_element_selector = "#$id";
                        }
                        if (!empty($col_conf['add_button']))
                        {
                            $button_params=$col_conf['add_button'];
                            if(is_array($button_params) && isset($button_params['action']))
                            {
                                $action_params=(isset($button_params['params']))?$button_params['params']:array();
                                echo SIMAHtml::openActionInDialog($button_params['action'],$action_params);
                            }
                            else
                            {
                                $params = [
                                    'multiselect' => false//it's in form
                                ];
                                if (is_array($button_params) && isset($button_params['view']))
                                {
                                    $params['view'] = $button_params['view'];
                                }
                                else
                                {
                                    $params['view'] = 'guiTable';
                                }
                                if (is_array($button_params) && isset($button_params['model']))
                                {
                                    $params['model'] = $button_params['model'];
                                }
                                else
                                {
                                    $relations = $model->relations();
                                    $params['model'] = $relations[$col_conf['relName']][1]; 
                                }
                                
                                if(isset($button_params['params']))
                                {
                                    $params['params'] = $button_params['params'];
                                }
                                
                                $params['filter'] = isset($button_params['filter'])? $button_params['filter'] : $_filters;
                                
                                if (isset($htmlOptions['disabled']) && $htmlOptions['disabled']==='disabled')
                                {
                                    echo "<span class='sima-icon _search_form _16 _disabled'></span>";
                                }
                                else
                                {
                                    echo "<span class='sima-icon _search_form _16' onclick='sima.misc.searchFieldAddButton(\"".$id."\",".CJSON::encode($params).");'></span>";
                                }
                                
                            }
                        }
                        break;
                    case 'dropdown':  
                        $relName='';
                        $default=null;
                        $model_filter = null;
                        if (gettype($col_conf)==='array')
                        {
                            if (isset($col_conf['empty'])) 
                            {
                                $htmlOptions['empty'] = $col_conf['empty'];
                            }
                            if (isset($col_conf['relName'])) 
                            {
                                $relName = $col_conf['relName'];
                            }
                            if (isset($col_conf['default'])) 
                            {
                                $default = $col_conf['default'];
                            }
                            if (isset($col_conf['model_filter']))
                            {                                
                                $model_filter = $col_conf['model_filter'];
                            }
                        }
                        
                        if (is_null($model->$column) && !is_null($default))
                        {
                            $model->$column=$default;
                        }
                        $columns = $model->getMetaData()->tableSchema->columns;
                        $default_selected = $model->$column;
                        if (isset($columns[$column]) && $columns[$column]->dbType == 'boolean')
                        {
                            if (gettype($model->$column) === 'NULL') 
                            {
                                 $default_selected = 'null';
                            }
                            else 
                            {
                                $default_selected = ($model->$column)?'1':'0'; 
                            }                            
                        }
                        if (is_object($default_selected))
                        {
                            throw new SIMAWarnAutoBafException('Greska - kolona '.$column.' zadata relacija umesto stranog kljuca');
                        }
                        $htmlOptions['options'] = array(
                            $default_selected=>array(
                                'selected'=>true
                            )
                        );
                        
                        if (isset($col_conf['dependent_on']))
                        {
                            if (isset($htmlOptions['id']))
                            {
                                $id = $htmlOptions['id'];
                            }
                            else
                            {
                                $id = SIMAHtml::uniqid();
                                $htmlOptions['id'] = $id;
                            }
                            $dependent_element_type = 'dropdown';
                            $dependent_element_selector = "#$id";
                        }
                        
                        echo $form->dropDownList($model,$column,$model->droplist($column,$relName,$model_filter), $htmlOptions);  
                        break;
                    case 'form':
                        echo SIMAHtml::modelFormOpen($model);
                        break;
                    case 'list':
                        if (!isset($col_conf['model']) || !isset($col_conf['relName']))
                        {
                            throw new Exception('modelSearchList - nisu lepo zadati parametri.');
                        }
                        $params = $col_conf;
                        $params['base_model'] = $model;
                        $params['search_model'] = $col_conf['model'];
                        
                        if (isset($col_conf['dependent_on']))
                        {
                            $id = SIMAHtml::uniqid();
                            $params['uniq'] = $id;
                            $dependent_element_type = 'list';
                            $dependent_element_selector = "#$id";
                        }

                        echo SIMAHtml::searchFormList($params);
                        break;
                    case 'tinymce':
                        $uniq = SIMAHtml::uniqid();
                        $fullScreen = true;
                        
                        $read_only = false;
                        $ui = [];
                        $tinymce_params = [];
                        if (isset($col_conf['read_only']) && $col_conf['read_only'] === true)
                        {
                            $read_only = true;
                            $ui = [
                                'menubar' => false,
                                'toolbar' => false,
                                'statusbar' => false
                            ];                            
                        }
                        $image = "image ";
                        if (isset($col_conf['image_upload']) && $col_conf['image_upload'] === false)
                        {
                            $image = "";
                        }
                        if (isset($col_conf['params'])) 
                        {
                            $tinymce_params = $col_conf['params'];
                        }
                        
                        $tinymce_default_params = [
                            'name' => $uniq,
                            'attribute' => $column,
                            'readOnly' => $read_only,
                            'height' => '400px',
                            'width' => '85%',
                            'htmlOptions' => ['rows' => 6, 'cols' => 50, 'class' => 'tinymce'],
                            'useSwitch' => false
                                                      
                        ];
                        if (Yii::app()->configManager->get('base.develop.use_tinymce_vue') === false) 
                        {   
                            $tinymce_default_params['model'] = $model; 
                            if (Yii::app()->configManager->get('base.use_new_tinymce') === true) 
                            {
                                $tinymce_default_params['plugins'] = [
                                    "modelchooser advlist lists table code $image link autolink media textcolor 
                                    charmap hr pagebreak anchor toc insertdatetime codesample paste nonbreaking"
                                ];
                                $tinymce_default_params['options'] = array_merge([
                                    'toolbar' => "
                                        undo redo | bold italic underline strikethrough forecolor backcolor | 
                                        link unlink | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | 
                                        removeformat | $image modelchooser |
                                    ",
                                    'branding' => false,
                                    'images_upload_url' => ETinyMce::$images_upload_url,
                                    'extended_valid_elements' => 'img[src|class|style|height|width|onclick],span[onclick|class|style]',
                                    'nonbreaking_force_tab' => true,
                                    'image_max_width' => 300,
                                    'image_max_height' => 200,
                                    'image_dimensions' => false,
                                    'image_description' => false
                                ], $ui);
                                if(isset($tinymce_params['toolbar']))
                                {
                                    $tinymce_default_params['options']['toolbar'] = $tinymce_params['toolbar'];
                                    unset($tinymce_params['toolbar']);
                                }
                                if(isset($tinymce_params['menubar']))
                                {
                                    $tinymce_default_params['options']['menubar'] = $tinymce_params['menubar'];
                                    unset($tinymce_params['menubar']);
                                }
                                if(isset($tinymce_params['statusbar']))
                                {
                                    $tinymce_default_params['options']['statusbar'] = $tinymce_params['statusbar'];
                                    unset($tinymce_params['statusbar']);
                                }
                            }
                            else 
                            {                                 
                                $tinymce_default_params['editorTemplate'] = 'full';
                            }
                            $this->widget('application.extensions.tinymce.ETinyMce', array_merge($tinymce_default_params, $tinymce_params));
                        }
                        else
                        {
                            $options = [
                                'toolbar' => "
                                    undo redo | bold italic underline strikethrough forecolor backcolor | 
                                    link unlink | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | 
                                    removeformat | $image modelchooser |
                                ",
                                'plugins' => "modelchooser advlist lists table code $image link autolink media fullscreen 
                                    textcolor charmap hr pagebreak anchor toc insertdatetime codesample paste nonbreaking"
                            ];
                            $tinymce_default_params = array_merge($tinymce_default_params, $options); 
                            $tinymce_default_params = array_merge($tinymce_default_params, $tinymce_params); 
                            
                            $text = addcslashes($model->{$column}, "\r\n.\'.\"");
                            
                            $textarea_name = CHtml::resolveName($model,$column);
                            $tinymce_default_param_json = CJSON::encode($tinymce_default_params);
                            
                            echo <<<EOP
                                <tinymce
                                    name={$textarea_name}
                                    id={$uniq}
                                    v-model='text'
                                    v-bind:tinymce_params='params' 
                                >
                                </tinymce>
                                <script>
                                    new Vue({
                                        el: '#{$uniq}',
                                        data: {
                                            text: '{$text}',
                                            params: {$tinymce_default_param_json}
                                        }
                                    });
                                </script> 
EOP;
                        }
                                               
                        break;
                        
                    case 'partnerField':
                        $uniq = SIMAHtml::uniqid();
                        $model_name = Partner::class;
                        $name = CHtml::resolveName($model,$column);
                        $text = "";
                        $model_id = "";
                        
                        if(!empty($model->{$column}))
                        {
                            $model_id = $model->{$column};
                            $partner = Partner::model()->findByPkWithCheck($model_id);
                            $text = $partner->DisplayName;
                        }
                        echo <<<EOP
                                <sima-partner-field 
                                    name={$name}
                                    id="sima-partner-field-{$uniq}"
                                    placeholder=""
                                    v-model="spf"
                                    v-bind:model_name="'{$model_name}'"
                                >
                                </sima-partner-field>
                                <script>
                                    new Vue({
                                        el: '#sima-partner-field-{$uniq}',
                                        data: {
                                            spf: {
                                                label: '{$text}',
                                                id: '{$model_id}',
                                            }
                                        }
                                    });
                                </script> 
EOP;
                        break;
                        
                    case 'addressField':
                        $uniq = SIMAHtml::uniqid();
                        $name = CHtml::resolveName($model,$column);
                        $text = "";
                        $model_id = "";

                        if(!empty($model->{$column}))
                        {
                            $model_id = $model->{$column};
                            $address = Address::model()->findByPkWithCheck($model_id);
                            $text = $address->DisplayName;
                        }
                        echo <<<EOP
                                <sima-address-field 
                                    name={$name}
                                    id="sima_address_field_{$uniq}"
                                    v-model="saf"
                                >
                                </sima-address-field>
                                <script>
                                    new Vue({
                                        el: '#sima_address_field_{$uniq}',
                                        data: {
                                            saf: {
                                                label: '{$text}',
                                                id: '{$model_id}',
                                            }
                                        }
                                    });
                                </script> 
EOP;
                        if (isset($col_conf['dependent_on']))
                        {
                            $dependent_element_type = 'addressField';
                            $dependent_element_selector = "#sima_address_field_$uniq";
                        }
                        break;
                        
                    case 'numberField':
                        $number_field_params = [];
                        if(is_array($col_conf))
                        {
                            $number_field_params = $col_conf;
                        }
                        if (isset($col_conf['dependent_on']))
                        {
                            if (isset($htmlOptions['id']))
                            {
                                $id = $htmlOptions['id'];
                            }
                            else
                            {
                                $id = SIMAHtml::uniqid();
                                $htmlOptions['id'] = $id;
                            }

                            $dependent_element_type = 'numberField';
                            $dependent_element_selector = "#$id";
                        }
                        
                        if(!isset($col_conf['number_validation']))
                        {
                            $number_field_params['number_validation'] = false;
                        }
                        
                        if (gettype($col_conf)==='array' && isset($col_conf['unit']))
                        {
                            $htmlOptions2 = [];
                            $relName = null;
                            if (gettype($col_conf['unit'])==='array')
                            {
                                if (count($col_conf['unit'])<1) 
                                {
                                    throw new Exception('forme nisu lepo definisane za model '.get_class($model).' - malo argumenata u nizu');
                                }
                                $unit_column = $col_conf['unit'][0];
                                if (isset($col_conf['unit']['disabled'])) 
                                {
                                    $htmlOptions2['disabled'] = $col_conf['unit']['disabled'];
                                }
                                if (isset($col_conf['unit']['empty'])) 
                                {
                                    $htmlOptions2['empty'] = $col_conf['unit']['empty'];
                                }
                                if (isset($col_conf['unit']['relName'])) 
                                {
                                    $relName = $col_conf['unit']['relName'];
                                }
                            }
                            else
                            {
                                $unit_column = $col_conf['unit'];
                            }
                            
                            if (isset($col_conf['dependent_on']))
                            {
                                if (isset($htmlOptions['id']))
                                {
                                    $id = $htmlOptions['id'];
                                }
                                else
                                {
                                    $id = SIMAHtml::uniqid();
                                    $htmlOptions['id'] = $id;
                                }
                                $dependent_element_type = 'numberField';
                                $dependent_element_selector = "#$id";
                            }

                            echo "<span class='short'>".SIMAHtml::numberField($model, $column, $htmlOptions, $number_field_params)."</span>";
                            echo "<span class='currency'>".$form->dropDownList($model,$unit_column,$model->droplist($unit_column,$relName), $htmlOptions2)."</span>";
                        }
                        else
                        {
                            echo SIMAHtml::numberField($model, $column, $htmlOptions, $number_field_params);
                        }
                        break;
                    case 'status':
                        if (!isset($htmlOptions['class']))
                        {
                            $htmlOptions['class'] = '';
                        }
                        $htmlOptions['class'] .= ' sima-model-form-status-field';
                        echo SIMAHtml::status($model, $column, 16, false, $status, $htmlOptions);
                        break;
                    case 'checkBoxList':
                        if (!isset($col_conf['elements_array']))
                        {
                            throw new Exception(Yii::t('BaseModule.Form', 'CheckBoxLustElementsArrayNotSet'));
                        }
                        
                        $params = [];
                        $params['elements_array'] = $col_conf['elements_array'];
                        
                        echo SIMAHtml::checkBoxList($model, $column, $params);
                        break;
                    case 'bankAccountField':                           
                        if (isset($col_conf['dependent_on']))
                        {
                            if (isset($htmlOptions['id']))
                            {
                                $id = $htmlOptions['id'];
                            }
                            else
                            {
                                $id = SIMAHtml::uniqid();
                                $htmlOptions['id'] = $id;
                            }
                            $dependent_element_type = 'bankAccountField';
                            $dependent_element_selector = "#$id";
                        }
                        echo SIMAHtml::bankAccauntNumberField($model, $column, $htmlOptions);
                        break;                   
                    case 'svpField':
                        $id = SIMAHtml::uniqid();                       
                        if (isset($col_conf['dependent_on']))
                        {
                            if (isset($htmlOptions['id']))
                            {
                                $id = $htmlOptions['id'];
                            }
                            else
                            {
                                $htmlOptions['id'] = $id;
                            }
                            $dependent_element_type = 'svpField';
                            $dependent_element_selector = "#$id";
                        }
                        $this->widget(SIMASVPField::class, [
                            'id' => $id,
                            'model' => $model,
                            'html_options' => $htmlOptions
                        ]);
                        break;
                    case 'phoneField': 
                        $this->widget('SIMAPhoneField', [
                            'model' => $model
                        ]);
                        break;
//                    default:
//                        throw new Exception(Yii::t('BaseModule.Form', 'Invalid'));
                }
                if (!empty($dependent_element_type) && !empty($dependent_element_selector))
                {
                    $dependent_on_json = CJSON::encode($col_conf['dependent_on']);
                    $register_script = "sima.dependentFields.applyDependentOnFields('$dependent_element_selector', $dependent_on_json);";
                    Yii::app()->clientScript->registerScript("script_registration$id", $register_script, CClientScript::POS_LOAD);
                }
                echo $htmlAfter;
                echo '<div class="revert-value"></div>';
                echo $form->error($model, $column);
                echo '<div class="clearfix"></div>';
                echo '</div>';
            }
            
        }
    ?>
    <?php $this->endWidget('CActiveForm'); ?>

</div>


<?php
 	if (Yii::app()->request->isAjaxRequest)
        {
            Yii::app()->clientScript->clearScriptFiles();
        }
?>
<?php if ($fullScreen) {?>
    <script type="text/javascript">
        $(function() {
            sima.dialog.TopDialogFullScreen();
        });
    </script>
<?php }?>
<?php if (isset($calcDate)) {?>
    <script type="text/javascript">
        function deadline_function<?php echo $form_id?>(column){
            sima.misc.calcDeadlineDialog('div#<?php echo $form_id?> div.row input[name="<?php echo get_class($model)?>[' + column +']"]');
        };
    </script>
<?php }