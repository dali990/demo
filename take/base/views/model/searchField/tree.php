<?php

    $ids = [];

    foreach ($rows as $row) 
    {
        //prvo ispisujemo sve parente do pocetka
        if (!empty($row->parent_ids))
        {
            $parents = $row->parent_ids;
            if (is_string($parents))
            {
                $parents = SIMAMisc::pgArrayToPhpArray($parents);
            }

            $parent_ids = array_diff($parents, $ids); //svi roditelji koji se nisu do sad ispisivali(ne nalaze se u ids nizu)
            if (!empty($parent_ids))
            {
                $parent_models = $modelName::model()->findAll([
                    'scopes' => $scopes,
                    'model_filter' => [
                        'ids' => $parent_ids
                    ]
                ]);
                foreach ($parent_models as $parent_model)
                {
                    ?>
                        <p class="sima-ui-sf-choice" data-value="<?=$parent_model->id?>" data-display="<?=$parent_model->DisplayName?>">
                            <?=$parent_model->display_name_path?>
                        </p>
                    <?php
                    array_push($ids, $parent_model->id);
                }
            }
        }
        
        //ispisujemo trazenu stavku
        if (!in_array($row->id, $ids))
        {
            ?>
                <p class="sima-ui-sf-choice" data-value="<?=$row->id?>" data-display="<?=$row->DisplayName?>">
                    <?=str_replace($text, "<span style='font-weight:bold;'>" . $text . "</span>", $row->display_name_path)?>
                </p>
            <?php
            array_push($ids, $row->id);
        }

        //zatim ispisujemo prvu decu
        if (!empty($row->children_ids))
        {
            $childrens = $row->children_ids;
            if (is_string($childrens))
            {
                $childrens = SIMAMisc::pgArrayToPhpArray($childrens);
            }

            $children_ids = array_diff($childrens, $ids); //sva deca koja se nisu do sad ispisivala(ne nalaze se u ids nizu)
            if (!empty($children_ids))
            {
                $children_models = $modelName::model()->findAll([
                    'scopes' => $scopes,
                    'model_filter' => [
                        'ids' => $children_ids,
                        //za sada se prikazuju svi potomci
//                        'parent' => [
//                            'ids' => $row->id //gledamo samo prvu decu
//                        ]
                    ]
                ]);
                foreach ($children_models as $children_model)
                {
                    ?>
                        <p class="sima-ui-sf-choice" data-value="<?=$children_model->id?>" data-display="<?=$children_model->DisplayName?>">
                            <?=$children_model->display_name_path?>
                        </p>
                    <?php
                }
            }
            
            //oznacavamo svu decu da su ispisana
            $ids = array_merge($ids, $childrens);
        }
    }