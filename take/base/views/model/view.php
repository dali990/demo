<?php
/**
 * koristi se prilikom renderovanja ModelView-ova
 * NE POSTVALJA SE KAO layout, nego mu se prosledjuju 
 * $content 
 * $modelModifiers
 */

?>
<<?php echo $wrapper?>
    class="<?php echo $class?>" 
    style='<?php echo $style?>'
    model='<?php echo $model?>'
    model_id='<?php echo $model_id?>'
    model_view='<?php echo $model_view?>'
    params='<?php echo CJSON::encode($params); ?>'
    tag="<?php echo $tag?>"
    additional_classes='<?php echo $additional_classes; ?>'
>
   <?php echo $content?> 
</<?php echo $wrapper?>>