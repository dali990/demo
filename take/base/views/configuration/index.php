<?php 
$uniq = SIMAHtml::uniqid(); 
$vertical_menu_id = 'vertical_menu_'.$uniq;
$params_wrapper_id = 'params_wrapper_'.$uniq;
?>

<div class="sima-layout-panel _splitter _vertical">
    <div class="sima-layout-panel"
         data-sima-layout-init='<?=CJSON::encode([
                'min_width' => 300,
                'max_width' => 300,
            ])?>'>
        <?php  
        $this->widget('SIMATree2', [
            'id' => $vertical_menu_id,
            'class'=>'sima-vm',
            'list_tree_data'=>$menu_array
        ]);
        ?>
    </div>
    <div class="sima-layout-panel">
        <div id="<?php echo $params_wrapper_id ?>" class="sima-ui-vertical-menu sima-layout-panel">
        </div>
    </div>
</div>

<script type='text/javascript'>
    $('#<?php echo $vertical_menu_id; ?>').on('selectItem', function(event, item){
        
        var ajaxAction = 'base/configuration/parameters';

        var customAction = item.data('custom-action');
        if(typeof customAction !== 'undefined')
        {
            ajaxAction = customAction;
        }
        var path = item.data('path');
        var type = item.data('type');
        var is_user_personal = <?php echo $is_user_personal?'true':'false'; ?>;
        
        sima.ajax.get(ajaxAction, {
            get_params: {
                path: path,
                type: type,
                is_user_personal: is_user_personal
            },
            success_function: function(response) {
                sima.set_html($("#<?php echo $params_wrapper_id?>"), response.html);
            }
        });
    });
    $('#<?php echo $vertical_menu_id; ?>').on('unselectItem', function(event, item){
        sima.set_html($("#<?php echo $params_wrapper_id?>"), '');
    });
</script>