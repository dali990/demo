<p><?=$configParam['display_name']?></p>
<select id="<?php echo $uniq_id; ?>">
<?php
    foreach($configParam['type_params'] as $param)
    {
        $selected = '';
        if((string)($param['value']) === (string)($configParam['value']))
        {
            $selected = 'selected="selected"';
        }
    ?>
        <option value="<?php echo $param['value']; ?>" <?php echo $selected; ?>><?php echo $param['title']; ?></option>
    <?php
    }
?>
</select>