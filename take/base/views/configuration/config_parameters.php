<?php

$uniq = SIMAHtml::uniqid();

if(count($config_parameters) > 0)
{
    $conf_ids = array();
    foreach($config_parameters as $conf)
    {
        if($conf['type'] !== 'group')
        {
            $conf_ids[] = $conf['dbId'];
        }
    }

    $model_class = ConfigurationParameter::class;
    if($is_user_personal === true)
    {
        $model_class = ConfigurationParameterUser::class;
    }
    
    if(!isset($title))
    {
        $title = '';
    }
    
    $this->widget('SIMAGuiTable', [
        'id'=>'configurationparameters'.$uniq,
        'model'=> $model_class,
        'fixed_filter'=>array(
            'ids' => $conf_ids,
            'order_by' => 'mapping_id ASC'
        ),
        'headerOptionRows' => [
            ['table_settings', 'title']
        ],
        'title' => $title
    ]);
}