<?php
    $has_tasks = Yii::app()->hasModule('jobs');
?>
<div class="sima-layout-panel _splitter _vertical">
    <div class="sima-layout-panel <?= $has_tasks ? '_splitter _horizontal' : '' ?>">
        <div class="sima-layout-panel _horizontal" data-sima-layout-init='{"proportion":"0.7"}'>
        <?php
            $guitable_id = SIMAHtml::uniqid();
            $this->widget('SIMAGuiTable',[
                'id' => $guitable_id,
                'model' => PersonToTheme::model(),
                'add_button' => [
                    'button1' => [
                        'init_data' => [
                            'PersonToTheme' => [
                                'theme' => ['ids' => $model->id]
                            ]
                        ]
                    ],
                    'button2' => "sima.modelChoose.person({
                            ok_function: function(data){
                                if (data.length > 0)
                                {
                                    sima.ajax.get('base/theme/addPersonToTheme', {
                                        get_params:{
                                            theme_id : {$model->id}
                                        },
                                        data:{
                                            persons: data
                                        },
                                        async:true,
                                        success_function:function(response){
                                            $('#{$guitable_id}').simaGuiTable('populate');
                                        }
                                    });
                                }
                            },
                            add_to_model_id: {$model->id},
                            add_to_model_name: 'Theme'
                        });",
                ],
                'title' => Yii::t('BaseModule.Theme', 'Team members'),
                'fixed_filter' => [
                    'theme' => ['ids' => $model->id]
                ]
            ]);
        ?>
        </div>
        <?php if ($has_tasks === true) { ?>
        <div class="sima-layout-panel _horizontal" data-sima-layout-init='{"proportion":"0.3"}'>
            <?php
                $this->widget('SIMAGuiTable',[
                    'model' => User::model(),
                    'add_button' => false,
                    'title' => Yii::t('BaseModule.Theme', 'UsersHasTasksAndAreNotInTheTeam'),
                    'fixed_filter' => [
                        'tasks' => [
                            'theme' => [
                                'ids' => $model->id
                            ]
                        ],
                        'filter_scopes' => [
                            'notInTheThemeTeam' => $model->id
                        ]
                    ]
                ]);
            ?>
        </div>
    <?php } ?>
    </div>
    <div class="sima-layout-panel">
        <?php
            $this->widget('SIMAGuiTable',[
                'model'=> CompanyToTheme::model(),
                'columns_type' => 'from_theme',
                'fixed_filter' => [
                    'theme' => [
                        'ids' => $model->id
                    ]
                ],
                'add_button' => [
                    'init_data' => [
                        'CompanyToTheme' => [
                            'theme_id' => $model->id,
                        ],
                    ]
                ],
                'title' => Yii::t('BaseModule.CompanyToTheme', 'CompaniesToTheme'),
            ]);
        ?>
    </div>
</div>