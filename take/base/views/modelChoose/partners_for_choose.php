<?php
$id = SIMAHtml::uniqid();
$this->widget(SIMAGuiTable::class, [
    'id' => $id,
    'class' => 'sima-model-choose-left-guitable',
    'model' => Person::class,
    'setRowDblClick' => ['sima.modelChoose.onTableChooseModel', $id],
    'columns_type' => 'model_choose'
]);

Yii::app()->clientScript->registerScript(SIMAHtml::uniqid(), "sima.modelChoose.bindTableToRemoveTrigger('$id', '$uniq_id')", CClientScript::POS_READY);
