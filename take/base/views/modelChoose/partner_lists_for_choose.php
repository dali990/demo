<?php
$id = SIMAHtml::uniqid();
$this->widget(SIMAGuiTable::class, [
    'id' => $id,
    'class' => 'sima-model-choose-left-guitable',
    'model' => PartnerList::class,
    'setRowDblClick' => ['sima.modelChoose.onTableChoosePartnerList', $id],
    'columns_type' => 'model_choose',
    'fixed_filter' => [
        'scopes' => [
            'withUserAccessToList' => Yii::app()->user->id
        ]
    ]
]);
