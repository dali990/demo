<?php
echo Yii::t('BaseModule.PartnerList', 'UsersToAdd').':';
foreach($users_to_add_data as $users_to_add_data)
{
    $id = $users_to_add_data['id'];
    $display_html = $users_to_add_data['display_html'];
    echo "<p data-model='$id'>$display_html</p>";
}
if($persons_not_added_count > 0)
{
    echo "</br><p>".Yii::t('BaseModule.PartnerList', 'PersonsNotAddedNotUsersCount').": $persons_not_added_count</p>";
    if(SIMAMisc::isVueComponentEnabled())
    {
        echo $this->widget(SIMAButtonVue::class, [
            'title' => Yii::t('BaseModule.Common', 'SeeMoreDetail'),
            'onclick' => ['sima.modelChoose.themeDialogDetail', $theme->id],
        ], true);
    }
    else
    {
        echo $this->widget(SIMAButton::class, [
            'action'=>[
                'title' => Yii::t('BaseModule.Common', 'SeeMoreDetail'),
                'onclick' => ['sima.modelChoose.themeDialogDetail', $theme->id],
            ]
        ], true);
    }
}
echo '</br></br>';