<?php
$id = SIMAHtml::uniqid();
$this->widget(SIMAGuiTable::class, [
    'id' => $id,
    'class' => 'sima-model-choose-left-guitable',
    'model' => User::class,
    'setRowDblClick' => ['sima.modelChoose.onTableChooseModel', $id],
    'columns_type' => 'model_choose',
    'fixed_filter' => [
        'active' => true
    ]
]);

Yii::app()->clientScript->registerScript(SIMAHtml::uniqid(), "sima.modelChoose.bindTableToRemoveTrigger('$id', '$uniq_id')", CClientScript::POS_READY);
