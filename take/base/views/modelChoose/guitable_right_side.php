<?php

$search_model_ids = !empty($search_model_ids) ? $search_model_ids : -1;
$title = !empty($title)?$title:Yii::t('BaseModule.Misc', 'ModelChooseGuitableItemsThatYouChoose');
$columns_type = !empty($columns_type)?$columns_type:null;

$table_id = "right_tbl_$uniq_id";
$this->widget('SIMAGuiTable', [
    'id' => $table_id,
    'class' => 'sima-model-choose-right-guitable',
    'model' => $model,
    'title' => $title,
    'add_button' => false,
    'fixed_filter' => [
        'ids' => $search_model_ids
    ],
    'setRowDblClick' => ['sima.modelChoose.onTableUnchooseModel', $table_id],
    'columns_type' => $columns_type
]);

