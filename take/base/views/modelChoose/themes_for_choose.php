<?php
$id = SIMAHtml::uniqid();
$this->widget(SIMAGuiTable::class, [
    'id' => $id,
    'class' => 'sima-model-choose-left-guitable',
    'model' => Theme::class,
    'setRowDblClick' => ['sima.modelChoose.onTableChooseTheme', $id],
    'columns_type' => 'model_choose',
    'fixed_filter' => [
        'ids' => $theme_ids
    ]
]);
