<?php
echo Yii::t('BaseModule.PartnerList', 'UsersToAdd').':';
foreach($users_to_add_data as $users_to_add_data)
{
    $id = $users_to_add_data['id'];
    $display_html = $users_to_add_data['display_html'];
    echo "<p data-model='$id'>$display_html</p>";
}
if($partners_not_added_count > 0)
{
    echo "</br><p>".Yii::t('BaseModule.PartnerList', 'PartnersNotAddedNotActiveUsersCount').": $partners_not_added_count</p>";
    if(SIMAMisc::isVueComponentEnabled())
    {
        echo $this->widget(SIMAButtonVue::class, [
            'title' => Yii::t('BaseModule.Common', 'SeeMoreDetail'),
            'onclick' => ['sima.modelChoose.partnerListDialogDetail', $partner_list->id],
        ], true);
    }
    else
    {
        echo $this->widget(SIMAButton::class, [
            'action'=>[
                'title' => Yii::t('BaseModule.Common', 'SeeMoreDetail'),
                'onclick' => ['sima.modelChoose.partnerListDialogDetail', $partner_list->id],
            ]
        ], true);
    }
}
echo '</br></br></br>';