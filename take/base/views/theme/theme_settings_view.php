
<div id="<?=$uniq_id?>" class="theme-settings">
    <?php
        foreach ($code_settings as $code_setting_key => $code_setting_value)
        {
            $check_access = !isset($code_setting_value['check_access_func']) || call_user_func($code_setting_value['check_access_func'], $theme);
            $item_disabled = $check_access ? '' : '_disabled';
            ?>
            <div class="theme-settings-item sima-layout-fixed-panel <?=$item_disabled?>" 
                 data-code="<?=$code_setting_key?>" 
                 data-type="<?=$code_setting_value['type']?>"
            >
                <div class="theme-settings-item-title">
                    <?=$code_setting_value['title']?>
                </div>
                <div class="theme-settings-item-value">
                    <?php 
                        if ($code_setting_value['type'] === 'boolean') 
                        {
                            $item_value = $code_setting_value['default_value'];
                            if (isset($saved_settings[$code_setting_key]))
                            {
                                $item_value = $saved_settings[$code_setting_key];
                            }

                            $checked = SIMAMisc::filter_bool_var($item_value) ? 'checked': '';
                            ?>
                                <input type="checkbox" <?=$checked?>>
                            <?php 
                        } 
                    ?>
                </div>
            </div>
            <?php
        }
    ?>
</div>

