<div class="">
    <?php echo $this->renderModelView($model, 'indexTitle'); ?>
</div>

<div class="scroll-container">
    <?php
    $id_tab = SIMAHtml::uniqid();
    $this->widget('SIMATabs', [
        'id' => $id_tab,
        'list_tabs_model' => $model
    ]);
    ?>
</div>