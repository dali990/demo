
<style>
.sima-qr-cell-table
{
/*    width: 38mm;
    height: 26mm;*/
    width: 120mm;
    height: 80mm;
    margin: 0mm;
    padding: 0mm;
}
    
.sima-qr-cell-qrimg
{
/*    height: 64px;
    width: 64px;*/
    height: 170px;
    width: 170px;
}

.sima-qr-cell-logo-td
{
    
    
}

.sima-qr-cell-logo-img
{
/*    height: 34px;
    width: 40px;*/
    margin-top: 50px;
    padding-top: 50px;
    height: 100px;
    width: 120px;
}

.sima-qr-cell-logo-span
{
    vertical-align: bottom; 
    /*padding: 0 5px;*/ 
    /*font-size: 2.2mm;*/
    font-size: 8mm;
}
</style>

<table cellpadding="0" cellspacing="0" class="sima-qr-cell-table">
    <tr>
        <td>
            <img class="sima-qr-cell-qrimg" src="<?php echo $pngTempFilePath; ?>" alt="qr">
        </td>
        <td rowspan="2" class="sima-qr-cell-logo-td" style="padding: 10mm;">
            <img class="sima-qr-cell-logo-img" src="<?php echo 'images/logo_fallback.png'; ?>" alt="logo">

            <span class="sima-qr-cell-logo-span"> PIB<?=Yii::app()->company->pib_jmbg?></span>
        </td>
    </tr>
</table>
<table cellpadding="0" cellspacing="0" class="sima-qr-cell-table">
    <tr>
        <td width="30mm" style="vertical-align: top; text-align: right; font-size: 8mm; ">
            IB:
        </td>
        <td width="30mm" style="vertical-align: top; text-align: right;font-size: 8mm; ">
            <?php echo $model->DisplayName; ?>
        </td>
    </tr>
</table>
<!--<table style="width: 100mm; height: 10mm">
    <tr>
        <td>
            <img class="sima-qr-cell-qrimg" src="<?php // echo $pngTempFilePath; ?>" alt="qr">
            </br>
            <span class="sima-qr-cell-logo-span">PIB1234567890</span>
            </br>
            <span style="font-size: 12px; font-weight: bold;"><?php // echo $model->DisplayName; ?></span>
        </td>
    </tr>
</table>-->

<!--<div width="20mm">
    <img class="sima-qr-cell-qrimg" src="<?php // echo $pngTempFilePath; ?>" alt="qr">
    </br>
    <span class="sima-qr-cell-logo-span">PIB1234567890</span>
    </br>
    <span style="font-size: 12px; font-weight: bold;"><?php // echo $model->DisplayName; ?></span>
</div>-->