<style>
.sima-qr-main-table
{
    height: 748px;
    width: 400px;
    max-width: 400px;
    min-width: 400px;
    vertical-align: top;
    font-family: arial, san-serif;
}

.sima-qr-main-table-tr
{
    vertical-align: top;
}

.sima-qr-main-table td, .sima-qr-main-table td *
{
    vertical-align: top;
    font-family: arial, san-serif;
}

.sima-qr-cell-table
{
    /*width: 100%;*/
    width: 33.33%;
    height: 85px;
    border-spacing: 0;
    vertical-align: top;
    border: 1px solid #aaa;
}

.sima-qr-cell-qrimg
{
    height: 64px;
    width: 64px;
}

.sima-qr-cell-logo-td
{
    text-align: center; 
    vertical-align: top; 
    padding: 10px 0 0 0;
}

.sima-qr-cell-logo-img
{
    height: 34px;
    width: 44px;
}

.sima-qr-cell-logo-span
{
    vertical-align: bottom; 
    padding: 0 5px; 
    font-size: 9px;
}

.sima-qr-main-table-td
{
    vertical-align: top;
    width: 33.33%;
}
</style>

<?php
    $cells_per_page = 24;
    $cells_per_column = 8;

    //$htmlCodesCount = count($htmlCodes);
//    $i=0;
    $cell_counter = 0;
    $page_counter = 0;
    //for(; $i < $htmlCodesCount; $i++)
    foreach($htmlCodes as $hc)
    {
        if($cell_counter === 0 && $page_counter == 0)
        {
            error_log(__METHOD__.' - 1 - $cell_counter: '.$cell_counter);
            error_log(__METHOD__.' - 1 - $page_counter: '.$page_counter);
            echo '<table cellpadding="0" cellspacing="0" bgcolor="#fff" class="sima-qr-main-table">
                ';
            echo '<tr class="sima-qr-main-table-tr">
                ';
        }
        
//        if($i === 0 || $i%27 === 0)
//        {
//            error_log(__METHOD__.' - 1 - $i: '.$i);
//            echo '<table cellpadding="0" cellspacing="0" bgcolor="#fff" class="sima-qr-main-table">
//                ';
//            echo '<tr class="sima-qr-main-table-tr">
//                ';
//        }
        
        if($cell_counter === 0)
        {
            echo '<td class="sima-qr-main-table-td">
                ';
        }

//        if($i === 0 || $i%8 === 0)
//        {
//            echo '<td class="sima-qr-main-table-td">
//                ';
//        }

        echo $hc;
        
        $cell_counter++;
        $page_counter++;

//        if($i !== 0 && $i%8 === 0)
        if($cell_counter === $cells_per_column)
        {
            echo '</td>
                ';
            $cell_counter = 0;
        }
        
        if($page_counter === $cells_per_page)
        {
//            error_log(__METHOD__.' - 2 - $i: '.$i);
            error_log(__METHOD__.' - 2 - $cell_counter: '.$cell_counter);
            error_log(__METHOD__.' - 2 - $page_counter: '.$page_counter);
            echo '</tr>
                ';
            echo '</table>
                ';
            
            $page_counter = 0;
        }

//        if($i !== 0 && $i%26 === 0)
//        {
//            error_log(__METHOD__.' - 2 - $i: '.$i);
//            echo '</tr>
//                ';
//            echo '</table>
//                ';
//        }
    }

//    if($i === 0 || $i%8 !== 0)
//    {
//        echo '</td>
//            ';
//    }
    
    if($cell_counter !== 0)
    {
        echo '</td>
            ';
    }
    
    if($page_counter !== 0)
    {
//        error_log(__METHOD__.' - 3 - $i: '.$i);
        error_log(__METHOD__.' - 3 - $cell_counter: '.$cell_counter);
        error_log(__METHOD__.' - 3 - $page_counter: '.$page_counter);
        echo '</tr>
            ';
        echo '</table>
            ';
    }

//    if($i === 0 || $i%26 !== 0)
//    {
//        error_log(__METHOD__.' - 3 - $i: '.$i);
//        echo '</tr>
//            ';
//        echo '</table>
//            ';
//    }

?>
