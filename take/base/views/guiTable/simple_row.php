
<tr class='simple-guitable-row <?php echo $row->getClasses()?> 
		<?php if (isset($expand_child)) echo $expand_child;?>'
                model="<?php echo get_class($row)?>"
                model_id="<?php echo $row->id?>"
	<?php echo $row->getAdditionalInfo()?>>
	<td class='options'>
            <?php 
                if (!isset($filter) || $filter->group_by=='')
                {
                    echo $row->getAttributeDisplay('statuses');
//                    echo SIMAHtml::modelIcons($row,16);
                    echo SIMAHtml::modelIconsContracted($row,16);
                }
            ?>
	</td>
        <?php foreach ($columns as $column) {?>
		<td class='<?php echo $column;?>'>
                    <?php
			 echo $row->getAttributeDisplay($column);?>
		</td>
	<?php }?>
</tr>