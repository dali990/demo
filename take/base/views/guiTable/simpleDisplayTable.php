
<table id="<?php echo $id?>" data-model="<?php echo $ModelName?>" class="sima-simpletable">
    <thead>
        <tr>
            <th class='options'></th>
            <?php foreach ($columns as $column){ ?>
                <th class='<?php echo $column ?>' column='<?php echo $column ?>'><?php echo $Model->getAttributeLabel($column) ?></th>
            <?php } ?>
        </tr>
    </thead>

    <tbody>
        <?php
        foreach ($rows as $row)
        {
            $this->renderPartial('base.extensions.SIMAGuiTable.views.guiTable.row', array('row' => $row, 'columns' => $columns, 'without_order_number' => true));
        }
        ?>
    </tbody>

    <tfoot>
    </tfoot>
</table>

<script type="text/javascript">
    $('#<?php echo $id?>').simaSimpleTable('init',{
        data:{
            columns:<?php echo CJSON::encode($columns)?>,
            modifiers:<?php echo CJSON::encode($modifiers)?>
        }
    });
    <?php
        if (isset($filter_params))
        {
            $json_html = '{}';
            if (gettype($filter_params) == 'array')
            {
                $json_html = CJSON::encode($filter_params);
            }
            if ($json_html!='{}')
            {
                echo "$('#$id').simaSimpleTable('setDefaultFilter',$json_html);";
            }
        }
    ?>
</script>





