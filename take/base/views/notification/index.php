
<?php $uniq = SIMAHtml::uniqid()?>

<?php   
    $this->widget('base.extensions.SIMAGuiTable.SIMAGuiTable', [
        'id'=>'todos'.$uniq,
        'model'=>Notification::model(),
        'fixed_filter'=>array(
//            'scopes'=>'byTime',
            'user_id'=>Yii::app()->user->id
        )
    ]);
?>
	
<script>
        $('#todos<?php echo $uniq?>').on('hover', 'tr.sima-guitable-row', function(){
            if ($(this).hasClass('notification_unseen'))
                $(this).attr('title', 'Nije procitano');
            else if ($(this).hasClass('notification_seen'))
                $(this).attr('title', 'Procitano');
        });
        
        $('#todos<?php echo $uniq?>').on('hover', 'tr.sima-guitable-row td.options img.button:not(.edit)', function(){
                $(this).attr('title', 'Izmeni procitano/neprocitano');
        });
        
        $('#todos<?php echo $uniq?>').on('click', 'tr.sima-guitable-row td.seen input', function(){
                var parentTr = $(this).parent().parent();
                var action = '';
                if (parentTr.hasClass('notification_seen'))
                {
                    action = 'base/notification/notifUnRead';
                    parentTr.removeClass('notification_seen');
                    parentTr.addClass('notification_unseen');
                }
                else if (parentTr.hasClass(('notification_unseen')))
                {
                    action = 'base/notification/notifRead';
                    parentTr.removeClass('notification_unseen');
                    parentTr.addClass('notification_seen');
                }
                var id = parentTr.attr('model_id');
                sima.ajax.get(action,{
                      get_params: {
                            id: id
                      }
                });
        });
</script>



<script type='text/javascript'>
    function selectSameModelRows(jquery_row)
    {
        var parent = jquery_row.parent();
        var model = jquery_row.find('div.notif_title').attr('model');
        var model_id = jquery_row.find('div.notif_title').attr('model_id');

        var sameNotifs = parent.find ('div[model='+model+'][model_id='+model_id+']');

        sameNotifs.each(function(){
                $(this).parent().parent().addClass('selected');
        //	alert($(this).html());
        });
    //	parent.find('.notification_1417').addClass('selected');
    }

    function unselectSameModelRows(jquery_row)
    {
        var parent = jquery_row.parent();
        var model = jquery_row.find('div.notif_title').attr('model');
        var model_id = jquery_row.find('div.notif_title').attr('model_id');

        var sameNotifs = parent.find ('div[model='+model+'][model_id='+model_id+']');

        sameNotifs.each(function(){
                $(this).parent().parent().removeClass('selected');
        //	alert($(this).html());
        });
    //	parent.find('.notification_1417').addClass('selected');
    }
	
</script>