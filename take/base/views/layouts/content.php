<div class="sima-layout-panel">
    <div id="<?php echo $id; ?>" class="sima-layout-panel _horizontal <?php echo $class; ?>" data-sima-layout-init='{"width_cut":20}'>
        <div class="sima-page-title sima-layout-fixed-panel <?php echo $title_class; ?>">
            <div class="sima-page-title-name sima-text-dots" <?php echo !empty($title_width)?"style='width:{$title_width}'":''; ?>>
                <?php 
                if(!empty($title_name_info_box))
                {
                    echo SIMAHtml::spanWithStaticInfoBox($title_name, $title_name_info_box);

                }
                else
                {
                    echo $title_name;
                } 
                ?>
            </div>
            <div class="sima-page-title-nav sima-text-dots <?php echo $title_options_position_type.$title_options_class; ?>"
                 <?php echo !empty($title_options_width)?"style='width:{$title_options_width}'":''; ?>>
                <ul>
                    <?php foreach ($title_options as $title_option) { ?>
                        <li class="sima-page-title-nav-opt <?php echo isset($title_option['class'])?$title_option['class']:''; ?>">
                            <?php
                                echo $title_option['html'];
                            ?>
                        </li>
                    <?php } ?>
                </ul>            
            </div>
            <div class="clearfix"></div>
        </div>
        <div class='sima-layout-panel sima-page-content' style='overflow: visible !important'>
            <div class='sima-layout-panel ' data-sima-layout-init='{"height_cut":10}'>
                <?php
                    if ($use_resizable_panel) {?>
                        <div class="sima-layout-panel _vertical _splitter">
                    <?php }
                    foreach ($panels as $panel) {
                        ?>
                        <div class='sima-layout-panel <?php echo isset($panel['class']) ? $panel['class'] : ''; ?>'
                            data-sima-layout-init='<?=CJSON::encode([
                                'min_width' => $panel['min_width'],
                                'max_width' => $panel['max_width'],
                                'proportion' => $panel['proportion'],
                            ])?>'
                            >
                            <?php echo $panel['html']; ?>
                        </div>
                        <?php
                    }
                    if ($use_resizable_panel) 
                    {
                    ?>
                        </div>
                    <?php  
                    }
                ?>
            </div>
        </div>
    </div>
</div>