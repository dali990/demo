<?php
$uniq = SIMAHtml::uniqid();
$model_filter_json = htmlspecialchars(CJSON::encode($model_filter));
$this->widget('base.extensions.SIMAGuiTable.SIMAGuiTable', [
    'id'=>'client_baf_requests'.$uniq,
    'model'=> ClientBafRequest::model(),
    'fixed_filter'=>$model_filter,
    'columns_type'=>!empty($params['columns_type'])?$params['columns_type']:'default',
    'custom_buttons' => [
        'Pošalji sve neposlate' => [
            'func' => "sima.errorReport.sendAllUnsentClientBafRequests($(this), {$model_filter_json});"
        ],
    ]
]);

