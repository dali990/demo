<?php
 	if (Yii::app()->request->isAjaxRequest) Yii::app()->clientScript->clearScriptFiles();
?>


<?php 
    $this->beginWidget('ext.SIMAResizablePanel.SIMAResizablePanel',
        array(
            'id'=>'panel_'.$uniq,
            'type'=>'vertical',
            'class'=> 'sima-ui-tabs',
            'proportions'=>array(0.3,0.7),
            'min_widths' => array(400,400)
//            'max_widths' => array(1400,1250,350)
            ));
?>
    <div class='sima-layout-panel'>
        <?php
            $this->widget('base.extensions.SIMAGuiTable.SIMAGuiTable', [
                'id' => 'sites_'.$uniq,
                'model' =>  BuildingSite::model(),
    //                            'columns_type'=>'admin_employees',                
                'add_button'=>true,                
//                'default_filter'=>array(
//                    'order_by'=>'time DESC',
//                )
                'setRowSelect' => "on_select$uniq"                
            ]);
        ?>
    </div>
    <div class='employees additional_info sima-layout-panel'>
        <?php
            $this->widget('base.extensions.SIMAGuiTable.SIMAGuiTable', [
                'id' => 'structures_'.$uniq,
                'model' => BuildingStructure::model(),
    //                            'columns_type'=>'admin_employees',
                'add_button'=>true,                
//                'default_filter'=>array(
//                    'order_by'=>'time DESC',
//                )               
            ]);
        ?>
    </div>
    
<?php $this->endWidget();?>


<script type="text/javascript">
    function on_select<?php echo $uniq; ?>(obj)
    {
        $('#structures_<?php echo $uniq?>').simaGuiTable('setFixedFilter',{
            building_site_id: obj.attr('model_id')
        }).simaGuiTable('populate');
    }
</script>

<?php
 	if (Yii::app()->request->isAjaxRequest) Yii::app()->clientScript->clearScriptFiles();
?>