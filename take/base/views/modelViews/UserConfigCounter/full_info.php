<?php 

$this->widget('SIMAGuiTable',[
    'model' => UserConfigCounterItem::model(),
    'add_button' => [
        'init_data' => [
            'UserConfigCounterItem' => [
                'user_config_counter' => ['ids' => $model->id]
            ]
        ]
    ],
    'fixed_filter' => [
        'user_config_counter' => ['ids' => $model->id]
    ]
]);

?>