<div class="sima-layout-panel">
    
    <div class='basic_info'>
        <p class='row'>
                <span class='title'><?php echo $model->getAttributeLabel('cost_location')?>:</span> 
                <span class='data'><?php echo $model->getAttributeDisplay('cost_location');?></span>
        </p>
    </div> 
    <?php if (isset($model->job)){?>
        <h4>Podaci iz posla</h4>
        <div class='basic_info'>
            <p class='row value'>
                    <span class='title'><?php echo $model->getAttributeLabel('job.value')?>:</span> 
                    <span class='data'><?php echo $model->getAttributeDisplay('job.value');?></span>
            </p>
            <p class='row advance'>
                    <span class='title'><?php echo $model->getAttributeLabel('job.advance')?>:</span> 
                    <span class='data'><?php echo $model->getAttributeDisplay('job.advance');?></span>
            </p>
        </div> 
        <?php if ($model->job->BaseDocumentType === Job::$BASE_CONTRACT){?>
        <h4>Podaci iz ugovora</h4>
        <div class='basic_info'>
            <p class='row value'>
                    <span class='title'><?php echo $model->job->getAttributeLabel('base_document')?>:</span> 
                    <span class='data'><?php echo $model->job->getAttributeDisplay('base_document');?></span>
            </p>
            <p class='row'>
                    <span class='title'><?php echo $model->job->getAttributeLabel('base_document.contract.value')?>:</span> 
                    <span class='data'><?php echo $model->job->getAttributeDisplay('base_document.contract.value');?></span>
            </p>
            <p class='row'>
                    <span class='title'><?php echo $model->job->getAttributeLabel('base_document.contract.advance')?>:</span> 
                    <span class='data'><?php echo $model->job->getAttributeDisplay('base_document.contract.advance');?></span>
            </p>
        </div> 
        <?php }?>
    <?php } ?>
    <?php if (!empty($job_finance)) { ?>
        <h4>Racuni</h4>
        <div class='basic_info'>
            <h5>Ulaz</h5>
            <p class='row'>
                    <span class='title'><?php echo $job_finance->getAttributeLabel('invoice_advance')?>:</span> 
                    <span class='data'><?php echo $job_finance->getAttributeDisplay('invoice_advance');?></span>
            </p>
            <p class='row'>
                    <span class='title'><?php echo $job_finance->getAttributeLabel('invoice_amount')?>:</span> 
                    <span class='data'><?php echo $job_finance->getAttributeDisplay('invoice_amount');?></span>
            </p>
            <p class='row'>
                    <span class='title'><?php echo $job_finance->getAttributeLabel('invoice_unreleased')?>:</span> 
                    <span class='data'><?php echo $job_finance->getAttributeDisplay('invoice_unreleased');?></span>
            </p>
            <h5>Izlaz</h5>
            <p class='row'>
                    <span class='title'><?php echo $job_finance->getAttributeLabel('bill_advance')?>:</span> 
                    <span class='data'><?php echo $job_finance->getAttributeDisplay('bill_advance');?></span>
            </p>
            <p class='row'>
                    <span class='title'><?php echo $job_finance->getAttributeLabel('bill_amount')?>:</span> 
                    <span class='data'><?php echo $job_finance->getAttributeDisplay('bill_amount');?></span>
            </p>
            <p class='row'>
                    <span class='title'><?php echo $job_finance->getAttributeLabel('bill_unreleased')?>:</span> 
                    <span class='data'><?php echo $job_finance->getAttributeDisplay('bill_unreleased');?></span>
            </p>
            <h5>Dobit</h5>
            <p class='row'>
                    <span class='title'><?php echo $job_finance->getAttributeLabel('profit')?>:</span> 
                    <span class='data'><?php echo $job_finance->getAttributeDisplay('profit');?></span>
            </p>
            <?php if (isset($model->job) && $model->job->BaseDocumentType === Job::$BASE_CONTRACT){?>
                <p class='row value'>
                        <span class='title'>Ugovoreno minus trošak:</span> 
                        <span class='data'><?=(SIMAHtml::number_format($model->job->base_document->contract->value - $job_finance->bill_amount))?></span>
                </p>
            <?php }?>
            
        </div>
    <?php } ?>

    
</div>