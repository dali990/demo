
<div class='basic_info'>
    <p class='row name'>
            <span class='title'><?php echo $model->getAttributeLabel('name')?>:</span> 
            <span class='data'><?php echo $model->getAttributeDisplay('name');?></span>
    </p>
    <p class="row coordinator">
        <span class="title"><?php echo $model->getAttributeLabel('coordinator')?>:</span> 
        <span class='data'><?php echo $model->getAttributeDisplay('coordinator');?> </span>
    </p>
    <?php
        $theme_basic_info_by_type = '';
        if (isset($model->bid))
        {
            $theme_basic_info_by_type .= $this->renderModelView($model->bid, 'inTheme');
        }
        else if (isset($model->job))
        {
            $theme_basic_info_by_type .= $this->renderModelView($model->job, 'inTheme');
        }
        
        echo $theme_basic_info_by_type;
    ?>
</div>