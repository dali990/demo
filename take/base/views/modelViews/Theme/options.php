<?php

if($model->type === Theme::$BAF)
{
    echo Yii::app()->controller->renderModelView($model->baf, 'options_in_theme');
}

echo SIMAHtml::modelIcons($model, 16, ['open']);

if(!empty($subactions))
{
    if(SIMAMisc::isVueComponentEnabled())
    {
        $this->widget(SIMAButtonVue::class, [
            'half' => true,
            'title' => Yii::t('BaseModule.Common', 'ConvertTo'),
            'subactions_data' => $subactions
        ]);
    }
    else
    {
        $this->widget('SIMAButton', [
            'class' => '_half',
            'action' => [
                'title' => Yii::t('BaseModule.Common', 'ConvertTo'),
                'subactions' => $subactions
            ]
        ]);
    }
}

if (Yii::app()->user->checkAccess('modelThemeSettings') || Yii::app()->user->id === intval($model->coordinator_id))
{
//    if(SIMAMisc::isVueComponentEnabled())
//    {
        $this->widget(SIMAButtonVue::class, [
            'half' => true,
            'icon' => '_main-settings',
            'iconsize' => 18,
            'tooltip' => Yii::t('BaseModule.Common', 'Settings'),
            'subactions_data' => [
                [
                    'title' => Yii::t('BaseModule.Theme', 'ThemeSettings'),
                    'onclick' => ['sima.theme.renderThemeSettingsView', $model->id]
                ],
                [
                    'title' => Yii::t('JobsModule.WorkedHoursEvidence', 'WorkedHoursEvidences'),
                    'onclick' => ['sima.dialog.openActionInDialogAsync', 'jobs/workedHoursEvidence/listWorkedHoursEvidencesForTheme', [
                        'get_params' => [
                            'theme_id' => $model->id
                        ]
                    ]]
                ]
            ]
        ]);
//    }
//    else
//    {
//        $this->widget('SIMAButton', [
//            'class' => '_half',
//            'action' => [
//                'icon' => 'sima-icon _main-settings _18',
//                'tooltip' => Yii::t('BaseModule.Common', 'Settings'),
//                'subactions' => [
//                    [
//                        'title' => Yii::t('BaseModule.Theme', 'ThemeSettings'),
//                        'onclick' => ['sima.theme.renderThemeSettingsView', $model->id]
//                    ],
//                    [
//                        'title' => Yii::t('JobsModule.WorkedHoursEvidence', 'WorkedHoursEvidences'),
//                        'onclick' => ['sima.dialog.openActionInDialogAsync', 'jobs/workedHoursEvidence/listWorkedHoursEvidencesForTheme', [
//                            'get_params' => [
//                                'theme_id' => $model->id
//                            ]
//                        ]]
//                    ]
//                ]
//            ]
//        ]);
//    }
}