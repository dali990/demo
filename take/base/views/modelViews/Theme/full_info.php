
<div style="float: left">
    <h4>U timu</h4>
    <ul>
        <?php foreach ($model->persons_to_teams as $team_member) {?>
        <li><?=$team_member->DisplayHTML.(!empty($team_member->role_in_team) ? " ({$team_member->role_in_team})" : '');?></li>
        <?php }?>
    </ul>
</div>

<div style="float: left; margin-left: 30px;">
    <h4><?=Yii::t('BaseModule.CompanyToTheme', 'CompaniesToTheme')?></h4>
    <ul>
        <?php foreach ($model->companies as $company) {?>
        <li><?=$company->DisplayHTML;?></li>
        <?php }?>
    </ul>
</div>

<div style="clear: left">
    <h4>Pod teme:</h4>
    <ul>
        <?php foreach ($model->children(['scopes'=>['byDisplayName','onlyImportantThemes']]) as $_sub_theme) {?>
        <li><?=$_sub_theme->DisplayHTML?></li>
        <?php }?>
    </ul>    
</div>

<h4>Osnovne informacije</h4>
-----------------------------------
<?=$this->renderModelView($model,'basic_info');?>
-----------------------------------
<div>
    <h4>Statistika:</h4>
    <ul>
        <li>Broj fajlova na temi: <?=$file_cnt?></li>
        <li>Broj fajlova na temi i podtemama: <?=$file_cnt_r?></li>
        <!--<li>Broj zadataka na temi: <?=$task_cnt?></li>-->
        <!--<li>Broj zadataka na temi i podtemama: <?=$task_cnt_r?></li>-->
        <!--<li>Broj podtema: <?=$subtheme_cnt?></li>-->
    </ul>
</div>

<p class='row add_cost_location'>
    <span class='title'><?php echo $model->getAttributeLabel('add_cost_location')?>:</span> 
    <span class='data'><?php echo $model->getAttributeDisplay('add_cost_location');?></span>
</p>