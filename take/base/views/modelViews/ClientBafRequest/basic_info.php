<p class="row name">
    <span class="title"><?php echo $model->getAttributeLabel('name')?>:</span> 
    <span class='data'><?php echo $model->getAttributeDisplay('name');?> </span>
</p>

<p class="row create_time">
    <span class="title"><?php echo $model->getAttributeLabel('create_time')?>:</span> 
    <span class='data'><?php echo $model->getAttributeDisplay('create_time');?> </span>
</p>

<p class="row send_time">
    <span class="title"><?php echo $model->getAttributeLabel('send_time')?>:</span> 
    <span class='data'><?php echo $model->getAttributeDisplay('send_time');?> </span>
</p>

<p class="row requested_by">
    <span class="title"><?php echo $model->getAttributeLabel('requested_by')?>:</span> 
    <span class='data'><?php echo $model->getAttributeDisplay('requested_by');?> </span>
</p>

<div class="row description">
    <span class="title"><?php echo $model->getAttributeLabel('description')?>:</span> 
    <div class='data'><?php echo $model->getAttributeDisplay('description');?> </div>
</div>

