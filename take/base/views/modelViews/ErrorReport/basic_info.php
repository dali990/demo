
<p class="row time">
    <span class="title"><?php echo $model->getAttributeLabel('time')?>:</span> 
    <span class='data'><?php echo $model->getAttributeDisplay('time');?> </span>
</p>
    
<p class="row submitter_id">
    <span class="title"><?php echo $model->getAttributeLabel('submitter_id')?>:</span>
    <span class='data'><?php echo isset($model->submitter) ? $model->submitter->DisplayHtml : '';?></span>
</p>

<p class="row action">
    <span class="title"><?php echo $model->getAttributeLabel('action')?>:</span>
    <span class='data'><?php echo $model->getAttributeDisplay('action');?> </span>
</p>

<p class="row get_params">
    <span class="title"><?php echo $model->getAttributeLabel('get_params')?>:</span>
    <span class='data'><?php echo $model->getAttributeDisplay('get_params');?> </span>
</p>

<p class="row post_params">
    <span class="title"><?php echo $model->getAttributeLabel('post_params')?>:</span>
    <span class='data'><?php echo $model->getAttributeDisplay('post_params');?> </span>
</p>

<p class="row message_iframe">
    <span class="title" style="float: left; display: block;"><?php echo $model->getAttributeLabel('message')?>:</span>
    <span class='data' style="width: 100%;"><?php echo $model->getAttributeDisplay('message');?> </span>
</p>

<p class="row message_strip_tags">
    <span class="title"><?php echo $model->getAttributeLabel('message_strip_tags')?>:</span>
    <span class='data'><?php echo $model->getAttributeDisplay('message_strip_tags');?> </span>
</p>

<p class="row description">
    <span class="title"><?php echo $model->getAttributeLabel('description')?>:</span>
    <span class='data'><?php echo $model->getAttributeDisplay('description');?> </span>
</p>

