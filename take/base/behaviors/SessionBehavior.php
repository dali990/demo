<?php

class SessionBehavior extends SIMAActiveRecordBehavior
{
    public function endSession()
    {
        $owner = $this->owner;
        
        $owner->end_time = 'now()';
        $owner->save();
        
        WebSocketClient::send($owner->id, 'SESSION', 'JS_SESSION_LOGOUT', []);
        
        $session_afterwork_params = [
            'session_id' => $owner->id
        ];
//        if(Yii::app()->configManager->get('base.develop.use_after_work', false))
//        {
            Yii::app()->registerAfterWork('SessionEndAfterWork', $session_afterwork_params);
//        }
//        else
//        {
//            Yii::app()->registerSystemEvent('SessionEndAfterWork', [$session_afterwork_params]);
//        }
    }
}
