<?php

class ErrorReportTabs extends SIMAActiveRecordTabs
{
    protected function tabsDefinition($code = null)
    {
        $owner = $this->owner;
        $err_tabs = array(
            array(
                'title' => Yii::t('MessagesModule.Common','Comments'),
                'code'=>'comments',
                'params_function' => 'tabComments',
                'selector_used' => true
            )
        );
        
        return $err_tabs;
    }
    
    protected function tabInfo()
    {
        $owner=  $this->owner;
        return array(
          'model'=>$owner
                
        );
    }
    
    protected function tabComments()
    {
        $owner = $this->owner;
        
        $comment_thread = CommentThread::getCommonForModel($owner);
        if(isset($owner->submitter_id))
        {
            $comment_thread->addListener($owner->submitter_id);
        }

        return array(
            'model' => $owner,
            'comment_thread'=>$comment_thread
        );
    }    
}
