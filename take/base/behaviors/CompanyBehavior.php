<?php

/**
 * @package SIMA
 */
class CompanyBehavior extends SIMAActiveRecordBehavior
{
    public function basicInfo($event)
    {
        $event->addResult($this->basicInfoParams());
    }
    
    public function basicInfoParams()
    {
        $owner = $this->owner;
        return [
            [
                'type' => SIMABasicInfo::$TYPE_HTML,
                'html' => "<img title='{$owner->DisplayName}' src='{$owner->getAttributeDisplay('image_id')}' />"
            ]
        ];
    }
    
    public function getLogoImagePath($use_default_if_not_available=true)
    {
        $company_logo_path = null;
        
        if($use_default_if_not_available === true)
        {
            $root_dir = SIMAMisc::GetProjectRootDir();
            $company_logo_path = $root_dir.DIRECTORY_SEPARATOR.'images/logo_not_available_fallback.png';
        }
        
        try
        {
            if(isset(Yii::app()->company->image) && Yii::app()->company->image->canDownload())
            {
                $company_logo_temp_file = TemporaryFile::CreateFromFile(Yii::app()->company->image, false);
                $company_logo_path = $company_logo_temp_file->getFullPath();
            }
        }
        catch(Exception $e)
        {
            /// prijavljivanje greske nama
            Yii::app()->errorReport->addError([
                'submitter_id'=>Yii::app()->user->id,
                'action'=>Yii::app()->controller->getRoute(),
                'message'=>CJSON::encode(Yii::app()->controller->getExceptionParamsAsArray($e)),
                'get_params'=>filter_input_array(INPUT_GET),
                'post_params'=>filter_input_array(INPUT_POST)
            ]);
        }
        
        return $company_logo_path;
    }
}
