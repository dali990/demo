<?php

class UserBaseBehavior extends SIMAActiveRecordBehavior 
{
    public function relations($event)
    {
        $event->addResult([
            'main_menu_favorite_items' => [SIMAActiveRecord::HAS_MANY, MainMenuFavoriteItem::class, 'user_id'],
            'main_menu_favorite_groups' => [SIMAActiveRecord::HAS_MANY, MainMenuFavoriteGroup::class, 'user_id'],
            'main_menu_favorite_groups_count' => [SIMAActiveRecord::STAT, MainMenuFavoriteGroup::class, 'user_id', 'select' => 'count(*)']
        ]);
    }
}