<?php

/**
 * @package SIMA
 */
class PersonBehavior extends SIMAActiveRecordBehavior
{
    public function basicInfo($event)
    {
        $event->addResult($this->basicInfoParams());
    }
    
    public function basicInfoParams()
    {
        $owner = $this->owner;
        
        $controller = Yii::app()->controller;
        
        $result = [
            [
                'type' => SIMABasicInfo::$TYPE_HTML,
                'html' => "<img title='{$owner->DisplayName}' src='{$owner->getAttributeDisplay('image_id')}' />"
            ]
        ];
        
        $result[] = [
            'type' => SIMABasicInfo::$TYPE_LABEL_VALUE,
            'label' => Yii::t('Person', 'System'),
            'value' => $controller->renderModelView($owner,'addEmployeeLink')
                        .$controller->renderModelView($owner,'addUserLink')
        ];
        
        if ($owner->id == Yii::app()->user->id)
        {
            $result[] = [
                'type' => SIMABasicInfo::$TYPE_ATTRIBUTE,
                'attribute' => 'person.password'
            ];
        }
        
        $business_widget_params = SIMAAddressbookComponent::getAddressbookHtmlPerson_businessWidgetParams($owner);
        $result[] = [
            'type' => SIMABasicInfo::$TYPE_HTML,
            'html' => $controller->widget(SIMAExpandableInformations::class, [
                'basic_informations' => $business_widget_params['business_basic_informations'],
                'expanded_informations' => $business_widget_params['business_expanded_informations'],
                'expanded_informations_absolute' => false
            ], true)
        ];
        
        $contact_widget_params = SIMAAddressbookComponent::getAddressbookHtmlPartner_contactWidgetParams($owner->partner);
        $result[] = [
            'type' => SIMABasicInfo::$TYPE_HTML,
            'html' => $controller->widget(SIMAExpandableInformations::class, [
                'basic_informations' => $contact_widget_params['contact_basic_informations'],
                'expanded_informations' => $contact_widget_params['contact_expanded_informations'],
                'expanded_informations_absolute' => false
            ], true)
        ];
        
        
        $basic_informations = [];
        $expanded_informations = [];
        $to_use_expanded_informations = false;
        
        if (Yii::app()->user->checkAccess('HRWorkContractRead'))
        {
            if ($owner->employee)
            {
                $basic_informations[] = [
                    'type' => SIMABasicInfo::$TYPE_ATTRIBUTE,
                    'attribute' => 'person.professional_degree_id'
                ];
                $expanded_informations[] = [
                    'type' => SIMABasicInfo::$TYPE_ATTRIBUTE,
                    'attribute' => 'person.professional_degree_id'
                ];
                $basic_informations[] = [
                    'type' => SIMABasicInfo::$TYPE_ATTRIBUTE,
                    'attribute' => 'person.education_title_id'
                ];
                $expanded_informations[] = [
                    'type' => SIMABasicInfo::$TYPE_ATTRIBUTE,
                    'attribute' => 'person.education_title_id'
                ];
            }
            
            $basic_informations[] = [
                'type' => SIMABasicInfo::$TYPE_ATTRIBUTE,
                'attribute' => 'person.JMBG'
            ];
            $expanded_informations[] = [
                'type' => SIMABasicInfo::$TYPE_ATTRIBUTE,
                'attribute' => 'person.JMBG'
            ];
        }
        
        if(!empty($owner->comment))
        {
            $expanded_informations[] = [
                'type' => SIMABasicInfo::$TYPE_ATTRIBUTE,
                'attribute' => 'person.comment'
            ];
            $to_use_expanded_informations = true;
        }
        
        if ($owner->employee && $owner->employee->haveRelation('slava'))
        {
            $slava_part = [
                'type' => SIMABasicInfo::$TYPE_ATTRIBUTE,
                'attribute' => 'person.employee.slava',
            ];
            
            if ($owner->employee->canLoggedInUserEditSlavaForThisEmp())
            {
                $slava_part['attribute_post_html'] = SIMAHtml::modelFormOpen($owner->employee, ['formName' => 'only_slava']);
            }
            
            $expanded_informations[] = $slava_part;
            $to_use_expanded_informations = true;
        }
        
        if(!empty($basic_informations))
        {
            if($to_use_expanded_informations === false)
            {
                $expanded_informations = [];
            }
            
            $result[] = [
                'type' => SIMABasicInfo::$TYPE_EXPANDABLE_INFORMATIONS_FOR_MODEL,
                'model' => $owner,
                'basic_informations' => $basic_informations,
                'expanded_informations' => $expanded_informations
            ];
        }
        
        return $result;
    }
}
