<?php

class ThemeTabs extends SIMAActiveRecordTabs
{
    protected function tabsDefinition($code = null)
    {
        $owner = $this->owner;
        $_team_tab_order = ($owner->team_count > 1)?200:50;
        
        $tabs = [
            [
                'title' => Yii::t('BaseModule.Theme','Team members'),
                'code' => 'team',
                'order' => $_team_tab_order
            ],
            [
                'title' => Yii::t('BaseModule.Misc','Others'),
                'code' => 'others',
                'origin' => 'Theme',
                'selectable' => false,
                'order' => 1000
            ],
        ];
        
        return $tabs;
    }
}