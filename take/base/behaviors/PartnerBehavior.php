<?php

/**
 * @package SIMA
 */
class PartnerBehavior extends SIMAActiveRecordBehavior {

    public function fullInfo($event) {
        $partner = $this->owner;
        if (isset($partner->person)) {
            $event->addResult([
                [
                    'model' => $partner->person,
                    'priority' => 10
                ]
            ]);

        } else if (isset($partner->company)) {
            $event->addResult([
                [
                    'model' => $partner->company_model,
                    'priority' => 10
                ]
            ]);
        }
    }

    public function basicInfo($event)
    {
        $owner = $this->owner;
        $model_to_use = $owner;
        if (isset($owner->person)) 
        {
            $model_to_use = $owner->person;
        }
        else if (isset($owner->company_model))
        {
            $model_to_use = $owner->company_model;
        }
        $params = $model_to_use->basicInfoParams();
        if(!empty($params))
        {
            $event->addResult($params);
        }
    }
    
    public function getModelChooseTabParams($params)
    {
        $tabs = [
            [
                'title' => Yii::t('BaseModule.PartnerList', 'Partners'),
                'code' => 'partners',
                'action' => 'base/modelChoose/partnersForChoose'
            ],
            [
                'title' => Yii::t('BaseModule.PartnerList', 'Users'),
                'code' => 'users',
                'action' => 'base/modelChoose/usersForChoose'
            ],
            [
                'title' => Yii::t('BaseModule.PartnerList', 'PartnerLists'),
                'code' => 'partner_list',
                'action' => 'base/modelChoose/partnerListsForChoose'
            ]
        ];
        return [
            'tabs' => $tabs,
            'default_selected' => 'partners'
        ];
    }
}
