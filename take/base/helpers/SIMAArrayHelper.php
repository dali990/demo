<?php

class SIMAArrayHelper 
{
    /**
     * Rekurzivno sortira niz na osnovu prosledjenih key-eva. Sortira po prvom key-u iz niza koji postoji. Umesto niza key-eva moze da se prosledi i jedan key
     * @param array $array
     * @param string/array $by_keys - niz key-eva, ili jedan key
     * @return array
     */
    public static function multisortByKey(array $array, $by_keys = [])
    {
        if (is_string($by_keys))
        {
            $by_keys = [$by_keys];
        }

        uksort($array, function($a, $b) use ($array, $by_keys) {
            foreach ($by_keys as $by_key)
            {
                if (isset($array[$a][$by_key]) && isset($array[$b][$by_key]))
                {
                    return $array[$a][$by_key] > $array[$b][$by_key];
                }
                else if (isset($array[$a][$by_key]))
                {
                    return -1;
                }
                else if (isset($array[$b][$by_key]))
                {
                    return 1;
                }
            }
            
            //default redosled je inicijalni redosled zadat u nizu
            return array_search($a, array_keys($array)) > array_search($b, array_keys($array));
        });

        foreach ($array as $key => $value) 
        {
            if (is_array($value))
            {
                $array[$key] = self::multisortByKey($value, $by_keys);
            }
        }
        
        return $array;
    }
    
    /**
     * Rekurzivno menja naziv key-a u nizu
     * @param array $array
     * @param string $old_key_name
     * @param string $new_key_name
     * @return array
     */
    public function changeKeyName(array $array, string $old_key_name, string $new_key_name)
    {
        foreach ($array as $key => $value) 
        {
            if ($key === $old_key_name)
            {
                $array[$new_key_name] = $value;
                unset($array[$key]);
                $key = $new_key_name;
            }
            
            if (is_array($value))
            {
                $array[$key] = self::changeKeyName($value, $old_key_name, $new_key_name);
            }
        }
        
        return $array;
    }
}
