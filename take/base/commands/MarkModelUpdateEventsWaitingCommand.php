<?php

/**
 * poziva se iz ModelUpdateEventsTraits::modelUpdateEventsMarkWaiting i ne proizvodi nikakve logove
 */
class MarkModelUpdateEventsWaitingCommand extends SIMACommand
{
    public $command_update_models_via_db = false;
    
    /**
     * 
     * @param array $args
     *  0 => $model_update_php_session_id - required
     *  1 => additional logging informations - optional
     * @throws Exception
     */
    public function performWork($args)
    {
        if(count($args) < 1)
        {
            throw new Exception('need model update session id - $args: '.SIMAMisc::toTypeAndJsonString($args));
        }
        
        $model_update_php_session_id = $args[0];
        
        if(empty($model_update_php_session_id))
        {
            throw new Exception('model_update_php_session_id empty: '.SIMAMisc::toTypeAndJsonString($model_update_php_session_id));
        }
        
        $additional_logging_informations = '';
        if(count($args) === 2)
        {
            $additional_logging_informations = $args[1];
        }
        
        MarkModelUpdateEventsWaitingCommand::work($model_update_php_session_id, $additional_logging_informations);
    }
    
    /**
     * 
     * @param string $model_update_php_session_id
     * @param string $additional_logging_informations
     * @throws Exception
     */
    public static function work($model_update_php_session_id, $additional_logging_informations='')
    {
        try
        {
            if(empty($model_update_php_session_id))
            {
                throw new Exception('model_update_php_session_id empty: '.SIMAMisc::toTypeAndJsonString($model_update_php_session_id));
            }
        
            /// ako nema modelupdateevent-ova, nema potrebe ista raditi
            if(ModelUpdateEvent2::model()->count([
                'model_filter' => [
                    'php_session_id' => $model_update_php_session_id
                ]
            ]) === 0)
            {
                return;
            }

            $sql = "SET sima.models_update_event_php_session_id='$model_update_php_session_id';"
                    . "SELECT base.model_update_events2_mark_waiting();";
            $command = Yii::app()->db->createCommand($sql);
        
            $command->execute();
        }
        catch(Exception $e)
        {
//            error_log(__METHOD__.' - get_class($e): '.get_class($e));
//            error_log(__METHOD__.' - $e->getMessage(): '.$e->getMessage());
            $autobafreq_message = 'message: '.$e->getMessage()
                                    .' <br> - stack trace: '.$e->getTraceAsString()
                                    .' <br> - exception class: '.get_class($e)
                                    .' <br> - $model_update_php_session_id: '.$model_update_php_session_id
                                    .' <br> - $additional_logging_informations: '.$additional_logging_informations;
            $autobafreq_id = 3335;
            $autobafreq_name = 'AutoBafReq MarkModelUpdateEventsWaitingCommand work Exception';
            
            if($e->errorInfo[0] === '23505') /// unique_violation
            {
                $ModelUpdateEvent2CompletedPhpsessionid = ModelUpdateEvent2CompletedPhpsessionid::model()->find([
                    'model_filter' => [
                        'php_session_id' => $model_update_php_session_id
                    ]
                ]);
                $autobafreq_message .= ' <br> - $ModelUpdateEvent2CompletedPhpsessionid: '.SIMAMisc::toTypeAndJsonString($ModelUpdateEvent2CompletedPhpsessionid);
                $ModelUpdateEvent2 = ModelUpdateEvent2::model()->findAll([
                    'model_filter' => [
                        'php_session_id' => $model_update_php_session_id
                    ]
                ]);
                $autobafreq_message .= ' <br> - $ModelUpdateEvent2: '.SIMAMisc::toTypeAndJsonString($ModelUpdateEvent2);
                
                $autobafreq_id = 3353;
                $autobafreq_name = 'AutoBafReq MarkModelUpdateEventsWaitingCommand work php_session_id unique_violation Exception';
            }
            
            Yii::app()->errorReport->createAutoClientBafRequest($autobafreq_message, $autobafreq_id, $autobafreq_name);
            throw $e;
        }
    }
}
