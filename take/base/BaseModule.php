<?php

/**
 * @package SIMA
 * @subpackage Base
 * 
 */
class BaseModule extends SIMAModule
{
    public function init()
    {
        parent::init();
        
        $s = DIRECTORY_SEPARATOR;

        // this method is called when the module is being created
        // you may place code here to customize the module or the application

        $this->setComponents(array(
            'defaultController' => 'ddmanager',
            'ddmanager' => array(
                'class' => 'SIMADDManager',
            ),
            'simaComponent' => array(
                'class' => 'application.modules.base.components.SIMAComponent',
            )
        ));
        Yii::app()->setComponents(array(
            'errorReport' => [
                'class' => 'application.modules.base.components.SIMAErrorReport',
            ]
        ));

        SIMADesktopApp::init();
        
        //ucitavamo big-numbers eksteniziju
        require_once $this->basePath.$s.'extensions'.$s.'BigNumbers'.$s.'vendor/autoload.php';
        require_once $this->basePath.$s.'extensions'.$s.'UAParser'.$s.'vendor/autoload.php';
    }

    public function permanentBehaviors()
    {
        return [
            'Company' => 'CompanyBehavior',
            'Session' => 'SessionBehavior',
            'Partner' => 'PartnerBehavior',
            'Person' => 'PersonBehavior',
            'Theme' => 'ThemeBehavior',
            'User' => 'UserBaseBehavior'
        ];
    }
    
    public function maintainOneMinuteCommands()
    {
        return [
            'BaseMaintain::pushModelUpdateEventsWithPhpSessionIdNull',
            'BaseMaintain::checkExpiredPersonalLicencedSessions',
        ];
    }
    
    public function maintainFiveMinuteCommands()
    {
        return [
            'BaseMaintain::pushOldModelUpdateEvents'
        ];
    }
    
    public function getBaseUrl()
    {
        return Yii::app()->assetManager->publish(Yii::getPathOfAlias('base.assets'));
    }

    public function registerManual()
    {
        $uniq = SIMAHtml::uniqid();

        $urlScript = Yii::app()->assetManager->publish(Yii::getPathOfAlias('base.assets'));
        
        Yii::app()->clientScript->registerScriptFile($urlScript . '/js/vue.js', CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerScriptFile($urlScript . '/js/vuex.js', CClientScript::POS_HEAD);
        
        Yii::app()->clientScript->registerScriptFile($urlScript .'/js/sima.js' , CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerScriptFile($urlScript .'/js/lightbox.min.js' , CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerScriptFile($urlScript .'/js/simaDialog.js' , CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerScriptFile($urlScript .'/js/simaAjax.js' , CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerScriptFile($urlScript .'/js/simaModel.js' , CClientScript::POS_HEAD); 
        Yii::app()->clientScript->registerScriptFile($urlScript .'/js/simaModelChoose.js' , CClientScript::POS_HEAD); 
        Yii::app()->clientScript->registerScriptFile($urlScript .'/js/simaModelMerge.js' , CClientScript::POS_HEAD); 
        Yii::app()->clientScript->registerScriptFile($urlScript .'/js/simaMainTab.js' , CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerScriptFile($urlScript .'/js/simaMisc.js' , CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerScriptFile($urlScript .'/js/simaGuiTable.js' , CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerScriptFile($urlScript .'/js/simaSimpleTableDisplay.js' , CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerScriptFile($urlScript .'/js/simaNotifications.js' , CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerScriptFile($urlScript .'/js/simaWarns.js' , CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerScriptFile($urlScript .'/js/simaEvents.js' , CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerScriptFile($urlScript .'/js/simaIcons.js' , CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerScriptFile($urlScript .'/js/simaTemp.js' , CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerScriptFile($urlScript .'/js/simaTheme.js' , CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerScriptFile($urlScript .'/js/jquery.numberField.js' , CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerScriptFile($urlScript .'/js/simaDate.js' , CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerScriptFile($urlScript .'/js/simaConfig.js' , CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerScriptFile($urlScript .'/js/simaAjaxLong.js' , CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerScriptFile($urlScript .'/js/simaLayout.js' , CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerScriptFile($urlScript .'/js/jquery.tooltip.js' , CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerScriptFile($urlScript .'/js/jqueryExtendedFunctions.js' , CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerScriptFile($urlScript .'/js/jquery.simaStartPage.js' , CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerScriptFile($urlScript .'/js/jquery.simaPopup.js' , CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerScriptFile($urlScript .'/js/simaDependentFields.js' , CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerScriptFile($urlScript .'/js/simaErrorReport.js' , CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerScriptFile($urlScript .'/js/simaModelForm.js' , CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerScriptFile($urlScript .'/js/simaHelpers.js' , CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerScriptFile($urlScript .'/js/eventSourceEdge.js' , CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerScriptFile($urlScript .'/js/bankAccountField.js' , CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerScriptFile($urlScript .'/js/simaLayoutHandler.js' , CClientScript::POS_END);
        Yii::app()->clientScript->registerScriptFile($urlScript .'/js/simaContactConverter.js' , CClientScript::POS_END);
        Yii::app()->clientScript->registerScriptFile($urlScript .'/js/SIMAKeysPressed.js' , CClientScript::POS_END);
        Yii::app()->clientScript->registerScriptFile($urlScript .'/js/simaVue.js' , CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerScriptFile($urlScript .'/js/simaVueShot.js' , CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerScriptFile($urlScript .'/js/simaGantt.js' , CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerScriptFile($urlScript .'/js/nanobar.js' , CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerScriptFile($urlScript .'/js/jsgantt.js' , CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerScriptFile($urlScript .'/js/momentLocal.js' , CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerScriptFile($urlScript .'/js/perfect-scrollbar.min.js' , CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerScriptFile($urlScript .'/js/sortable.min.js' , CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerScriptFile($urlScript .'/js/vuedraggable.min.js' , CClientScript::POS_HEAD);
        //Sasa A. - zakomentarisano jer treba da se pregleda lepo i ispravi sta ne valja
//        Yii::app()->clientScript->registerScriptFile($urlScript . '/js/simaDateInputField.js', CClientScript::POS_HEAD);

        Yii::app()->clientScript->registerScriptFile($urlScript .'/js/poper.min.js' , CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerScriptFile($urlScript .'/js/tooltip.min.js' , CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerScriptFile($urlScript .'/js/v-calendar.min.js' , CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerScriptFile($urlScript .'/js/v-observe-visibility.min.js' , CClientScript::POS_HEAD);

        Yii::app()->clientScript->registerCssFile($urlScript . '/css/notifications.css');
        Yii::app()->clientScript->registerCssFile($urlScript . '/css/lightbox.css');
        Yii::app()->clientScript->registerCssFile($urlScript . '/css/maintabs.css');
        Yii::app()->clientScript->registerCssFile($urlScript . '/css/buttons.css');
        Yii::app()->clientScript->registerCssFile($urlScript . '/css/simple-table.css');
        Yii::app()->clientScript->registerCssFile($urlScript . '/css/icons.css');
        Yii::app()->clientScript->registerCssFile($urlScript . '/css/default.css');
        Yii::app()->clientScript->registerCssFile($urlScript . '/css/configuration.css');
        Yii::app()->clientScript->registerCssFile($urlScript . '/css/tooltip.css');
        Yii::app()->clientScript->registerCssFile($urlScript . '/css/simaForm.css');
        Yii::app()->clientScript->registerCssFile($urlScript . '/css/layouts.css');
        Yii::app()->clientScript->registerCssFile($urlScript . '/css/vertical-menu.css');
        Yii::app()->clientScript->registerCssFile($urlScript . '/css/theme_list.css');
        Yii::app()->clientScript->registerCssFile($urlScript . '/css/sima-iframe.css');
        Yii::app()->clientScript->registerCssFile($urlScript . '/css/basic_info.css');
        Yii::app()->clientScript->registerCssFile($urlScript . '/css/simaDialog.css');
        Yii::app()->clientScript->registerCssFile($urlScript . '/css/fontawesome.min.css');
        Yii::app()->clientScript->registerCssFile($urlScript . '/css/jsgantt.css');
        Yii::app()->clientScript->registerCssFile($urlScript . '/css/simaGantt.css');
        Yii::app()->clientScript->registerCssFile($urlScript . '/css/simaCalendar.css');
        Yii::app()->clientScript->registerCssFile($urlScript . '/css/mainmenu.css');
        Yii::app()->clientScript->registerCssFile($urlScript . '/css/perfect-scrollbar.css');
        
        Yii::app()->sass->register(Yii::getPathOfAlias('base.assets') .'/css/base.scss', '', 'base.assets', 'css');
       
        //angular
        Yii::app()->clientScript->registerScriptFile($urlScript .'/js/angular/angular.js' , CClientScript::POS_HEAD);
        //hamster
        Yii::app()->clientScript->registerScriptFile($urlScript .'/js/hamsterjs/hamster.js' , CClientScript::POS_HEAD);
        //mousewheel
        Yii::app()->clientScript->registerScriptFile($urlScript .'/js/angular-mousewheel/mousewheel.js' , CClientScript::POS_HEAD);
        //panzoom
        Yii::app()->clientScript->registerScriptFile($urlScript .'/js/angular-pan-zoom/panzoom.js' , CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerCssFile($urlScript . '/js/angular-pan-zoom/panzoomwidget.css');        
        
        if(Yii::app()->eventRelay->isInstalled())
        {
            Yii::app()->eventRelay->registerManual();
        }
        
        if(Yii::app()->isWebApplication())
        {
            $sima_tab_session_id = Yii::app()->sima_session->id;
            $tinymce_css_url = Yii::app()->assetManager->publish(dirname(__FILE__).'/vueGUI/components/tinymce/css');
            Yii::app()->clientScript->registerCssFile($tinymce_css_url . '/tinymce.css');
            $content_css_url = Yii::app()->getAssetManager()->getPublishedUrl(Yii::getPathOfAlias('base.vueGUI.components.tinymce.css')).'/tinymce_content.css';
            $max_file_size_upload = Yii::app()->configManager->get('base.tinymce_max_file_size_upload', false);
            
            $params = [
                'base_url'=>Yii::app()->request->baseUrl,
                'session_id'=>$sima_tab_session_id,
                'date_format'=>'DD.MM.YYYY.',
                'datetime_format'=>'DD.MM.YYYY. hh:mm:ss',
                'app_name' => Yii::app()->name,
                'page_title' => Yii::app()->name.' - '.Yii::app()->company->DisplayName,
                'session_key' => Yii::app()->getSessionKey(),
                'show_unconfirmed_days_warn' => Yii::app()->configManager->get('jobs.show_unconfirmed_days_warn'),
                'show_unconfirmed_days_warn_interval' => Yii::app()->configManager->get('jobs.show_unconfirmed_days_warn_interval'),
                'tolerable_unconfirmed_days' => Yii::app()->configManager->get('jobs.tolerable_unconfirmed_days'),
                'model_displayhtml_click_open_conf' => Yii::app()->configManager->get('base.model_displayhtml_click_open'),
                'curr_user_id' => Yii::app()->user->id,
                'current_language' => Yii::app()->language,
                'ajax_sync_action_consolelog' => Yii::app()->configManager->get('base.develop.ajax_sync_action_consolelog'),
                'ajax_sync_action_autobaf' => Yii::app()->configManager->get('base.develop.ajax_sync_action_autobaf'),
                'config_new_comment_unread' => Yii::app()->configManager->get('base.develop.new_comment_unread2'),
                'numeric_separators' => SIMABigNumber::getSeparators(),
                'tinymce_default_params' => [
                    'language' => Yii::app()->language,
                    'content_css_url' => $content_css_url,
                    'max_file_size_upload_mb' => $max_file_size_upload,
                    'max_file_size_upload_bytes' => SIMAMisc::convertMemoryUnit($max_file_size_upload, "MB", "B"),
                    'image_extensions' => implode(',', SIMAFilePreview::$image_extensions),
                    'send_message_using_enter' => Yii::app()->configManager->get('messages.tinymce_send_message_using_enter'),
                ],
                'use_new_layout' => Yii::app()->configManager->get('base.develop.use_new_layout', false),
                'current_company_id' => Yii::app()->company->id,
                'all_warnings' => Yii::app()->warnManager->getWarnsList(),
                'use_comments_vue' => Yii::app()->configManager->get('base.develop.use_comments_vue', false),
                'have_doc_wiki' => Yii::app()->docWiki->isInstalled()
            ];
            
//            if(Yii::app()->useNewRepmanager())
            if(Yii::app()->repManager->use_new_repmanager === true)
            {
                $params['use_new_repmanager'] = true;
            }
            
            if(SIMAMisc::isVueComponentEnabled())
            {
                $params['use_vue_components'] = true;
            }

            $json_params = CJSON::encode($params);

            $sima_init_script = "var sima = new SIMA($json_params);";
            Yii::app()->clientScript->registerScript('sima-create',$sima_init_script, CClientScript::POS_HEAD);        

            $sima_init_script1 = "sima.init();";
            Yii::app()->clientScript->registerScript('sima-init',$sima_init_script1, CClientScript::POS_END);
            
            Yii::app()->clientScript->registerScript(SIMAHtml::uniqid(), "sima.modelChoose = new SIMAModelChoose();", CClientScript::POS_READY);
            Yii::app()->clientScript->registerScript(SIMAHtml::uniqid(), "sima.modelMerge = new SIMAModelMerge();", CClientScript::POS_READY);

            Yii::app()->clientScript->registerScriptFile($urlScript . '/js/simaReport.js', CClientScript::POS_HEAD);
            Yii::app()->clientScript->registerScript("sima_report_register_$uniq", "sima.report = new SIMAReport();", CClientScript::POS_END);
            Yii::app()->clientScript->registerScript("sima_converter_$uniq", "sima.contactConverter = new SIMAContactConverter();", CClientScript::POS_READY);   
        }
        
//        //mora ovde zbog redosleda ucitavanja
//        $filters_dir = dirname(__FILE__).'/vueGUI/filters';
//        Yii::app()->clientScript->registerScript(SIMAHtml::uniqid(),file_get_contents($filters_dir.'/formatNumber.js'),CClientScript::POS_READY);
//        Yii::app()->clientScript->registerScript(SIMAHtml::uniqid(),file_get_contents($filters_dir.'/round.js'),CClientScript::POS_READY);
//        Yii::app()->clientScript->registerScript(SIMAHtml::uniqid(),file_get_contents($filters_dir.'/formatDate.js'),CClientScript::POS_READY);
        
        //vue direktive
//        $vue_directives_dir = dirname(__FILE__).'/vueGUI/directives';
//        Yii::app()->clientScript->registerScript("onlyNumber_directive_js_$uniq",file_get_contents($vue_directives_dir.'/onlyNumber.js'),CClientScript::POS_READY);
//        Yii::app()->clientScript->registerScript("tinymceViewerDirective_js_$uniq", file_get_contents($vue_directives_dir.'/tinymceViewerDirective.js'), CClientScript::POS_READY);
//        Yii::app()->clientScript->registerScript("clickOutside_directive_js_$uniq",file_get_contents($vue_directives_dir.'/clickOutside.js'),CClientScript::POS_READY);

//        $component_dir = dirname(__FILE__).'/vueGUI/components/simaStatusSpan';
//        Yii::app()->clientScript->registerScript(SIMAHtml::uniqid(),file_get_contents($component_dir.'/simaStatusSpan.js'),CClientScript::POS_READY);
//        Yii::app()->clientScript->registerScript(SIMAHtml::uniqid(),include($component_dir.'/simaStatusSpan.php'),CClientScript::POS_END);
////        Yii::app()->clientScript->registerCssFile($component_dir.'/simaStatusSpan.css');

//        $default_layout_component_dir = dirname(__FILE__).'/vueGUI/components/simaModelDefaultLayout';
//        Yii::app()->clientScript->registerScript("simaModelDefaultLayout_js_$uniq", file_get_contents($default_layout_component_dir.'/simaModelDefaultLayout.js'), CClientScript::POS_READY);
//        Yii::app()->clientScript->registerScript("simaModelDefaultLayout_php_$uniq", include($default_layout_component_dir.'/simaModelDefaultLayout.php'), CClientScript::POS_END);
//        Yii::app()->clientScript->registerCss("simaModelDefaultLayout_css_$uniq", file_get_contents($default_layout_component_dir.'/simaModelDefaultLayout.css'));

//        $model_options_dir = dirname(__FILE__).'/vueGUI/components/simaModelOptions';
//        Yii::app()->clientScript->registerScript("simaModelOptions_js_$uniq", file_get_contents($model_options_dir.'/simaModelOptions.js'), CClientScript::POS_READY);
//        Yii::app()->clientScript->registerScript("simaModelOptions_php_$uniq", include($model_options_dir.'/simaModelOptions.php'), CClientScript::POS_END);
//        Yii::app()->clientScript->registerCss("simaModelOptions_css_$uniq", file_get_contents($model_options_dir.'/simaModelOptions.css'));

//        $model_edit_dir = dirname(__FILE__).'/vueGUI/components/simaModelEdit';
//        Yii::app()->clientScript->registerScript("simaModelEdit_js_$uniq", file_get_contents($model_edit_dir.'/simaModelEdit.js'), CClientScript::POS_READY);
//        Yii::app()->clientScript->registerScript("simaModelEdit_php_$uniq", include($model_edit_dir.'/simaModelEdit.php'), CClientScript::POS_END);
//        Yii::app()->clientScript->registerCss("simaModelEdit_css_$uniq", file_get_contents($model_edit_dir.'/simaModelEdit.css'));
        
//        $model_delete_dir = dirname(__FILE__).'/vueGUI/components/simaModelDelete';
//        Yii::app()->clientScript->registerScript("simaModelDelete_js_$uniq", file_get_contents($model_delete_dir.'/simaModelDelete.js'), CClientScript::POS_READY);
//        Yii::app()->clientScript->registerScript("simaModelDelete_php_$uniq", include($model_delete_dir.'/simaModelDelete.php'), CClientScript::POS_END);
//        Yii::app()->clientScript->registerCss("simaModelDelete_css_$uniq", file_get_contents($model_delete_dir.'/simaModelDelete.css'));
        
//        $model_fake_basic_info_dir = dirname(__FILE__).'/vueGUI/components/simaModelFakeBasicInfo';
//        Yii::app()->clientScript->registerScript("simaModelFakeBasicInfo_js_$uniq", file_get_contents($model_fake_basic_info_dir.'/simaModelFakeBasicInfo.js'), CClientScript::POS_READY);
//        Yii::app()->clientScript->registerScript("simaModelFakeBasicInfo_php_$uniq", include($model_fake_basic_info_dir.'/simaModelFakeBasicInfo.php'), CClientScript::POS_END);
//        Yii::app()->clientScript->registerCss("simaModelFakeBasicInfo_css_$uniq", file_get_contents($model_fake_basic_info_dir.'/simaModelFakeBasicInfo.css'));
        
//        $sima_button_dir = dirname(__FILE__).'/vueGUI/components/simaButton';
//        Yii::app()->clientScript->registerScript("simaButton_js_$uniq", file_get_contents($sima_button_dir.'/simaButton.js'), CClientScript::POS_READY);
//        Yii::app()->clientScript->registerScript("simaButton_php_$uniq", include($sima_button_dir.'/simaButton.php'), CClientScript::POS_END);
//        Yii::app()->clientScript->registerCss("simaButton_css_$uniq", file_get_contents($sima_button_dir.'/simaButton.css'));
        
//        $sima_popup_dir = dirname(__FILE__).'/vueGUI/components/simaPopup';
//        Yii::app()->clientScript->registerScript("simaPopup_js_$uniq", file_get_contents($sima_popup_dir.'/simaPopup.js'), CClientScript::POS_READY);
//        Yii::app()->clientScript->registerScript("simaPopup_php_$uniq", include($sima_popup_dir.'/simaPopup.php'), CClientScript::POS_END);
//        Yii::app()->clientScript->registerCss("simaPopup_css_$uniq", file_get_contents($sima_popup_dir.'/simaPopup.css'));
        
//        $sima_pagination_dir = dirname(__FILE__).'/vueGUI/components/simaPagination';
//        Yii::app()->clientScript->registerScript("simaPagination_js_$uniq", file_get_contents($sima_pagination_dir.'/simaPagination.js'), CClientScript::POS_READY);
//        Yii::app()->clientScript->registerScript("simaPagination_php_$uniq", include($sima_pagination_dir.'/simaPagination.php'), CClientScript::POS_END);
//        Yii::app()->clientScript->registerCss("simaPagination_css_$uniq", file_get_contents($sima_pagination_dir.'/simaPagination.css'));
        
//        $tinymce_viewer_dir = dirname(__FILE__).'/vueGUI/components/tinymceViewer';
//        Yii::app()->clientScript->registerScript("simaTinymceView_js_$uniq", file_get_contents($tinymce_viewer_dir.'/tinymceViewer.js'), CClientScript::POS_READY);
//        Yii::app()->clientScript->registerScript("prism_js_$uniq", file_get_contents($tinymce_viewer_dir.'/prism.js'), CClientScript::POS_READY);
//        Yii::app()->clientScript->registerScript("simaTinymceViewphp_$uniq", include($tinymce_viewer_dir.'/tinymceViewer.php'), CClientScript::POS_END);
//        Yii::app()->clientScript->registerCss("simaTinymceViewcss_$uniq", file_get_contents($tinymce_viewer_dir.'/tinymceViewer.css'));
        
//        $tinymce_dir = dirname(__FILE__).'/vueGUI/components/tinymce';
//        Yii::app()->clientScript->registerScript("simaTinymce_js_$uniq", file_get_contents($tinymce_dir.'/tinymce.js'), CClientScript::POS_READY);
//        Yii::app()->clientScript->registerScript("simaTinymce_php_$uniq", include($tinymce_dir.'/tinymce.php'), CClientScript::POS_END);
//        $tinymce_css_url = Yii::app()->assetManager->publish($tinymce_dir.'/css');
//        Yii::app()->clientScript->registerCssFile($tinymce_css_url . '/tinymce.css');
        
//        $iframe_dir = dirname(__FILE__).'/vueGUI/components/simaIFrame';
//        Yii::app()->clientScript->registerScript("simaIFrame_js_$uniq", file_get_contents($iframe_dir.'/simaIFrame.js'), CClientScript::POS_READY);
//        Yii::app()->clientScript->registerScript("simaIFrame_php_$uniq", include($iframe_dir.'/simaIFrame.php'), CClientScript::POS_END);
//        Yii::app()->clientScript->registerCss("simaIFrame_css_$uniq", file_get_contents($iframe_dir.'/simaIFrame.css'));
        
//        $model_display_icon_dir = dirname(__FILE__).'/vueGUI/components/simaModelDisplayIcon';
//        Yii::app()->clientScript->registerScript("simaModelDisplayIcon_js_$uniq", file_get_contents($model_display_icon_dir.'/simaModelDisplayIcon.js'), CClientScript::POS_READY);
//        Yii::app()->clientScript->registerScript("simaModelDisplayIcon_php_$uniq", include($model_display_icon_dir.'/simaModelDisplayIcon.php'), CClientScript::POS_END);
//        Yii::app()->clientScript->registerCss("simaModelDisplayIcon_css_$uniq", file_get_contents($model_display_icon_dir.'/simaModelDisplayIcon.css'));

//        $model_display_html_dir = dirname(__FILE__).'/vueGUI/components/simaModelDisplayHtml';
//        Yii::app()->clientScript->registerScript("simaModelDisplayHtml_js_$uniq", file_get_contents($model_display_html_dir.'/simaModelDisplayHtml.js'), CClientScript::POS_READY);
//        Yii::app()->clientScript->registerScript("simaModelDisplayHtml_php_$uniq", include($model_display_html_dir.'/simaModelDisplayHtml.php'), CClientScript::POS_END);
//        Yii::app()->clientScript->registerCss("simaModelDisplayHtml_css_$uniq", file_get_contents($model_display_html_dir.'/simaModelDisplayHtml.css'));
        
//        $vue_tabs_dir = dirname(__FILE__).'/vueGUI/components/vueTabs';
//        Yii::app()->clientScript->registerScript("vueTabs_js_$uniq", file_get_contents($vue_tabs_dir.'/vueTabs.js'), CClientScript::POS_READY);
//        Yii::app()->clientScript->registerCss("vueTabs_css_$uniq", file_get_contents($vue_tabs_dir.'/vueTabs.css'));
        
//        $sima_notifications = dirname(__FILE__).'/vueGUI/components/simaNotifications';
//        Yii::app()->clientScript->registerScript("simaNotifications_js_$uniq", file_get_contents($sima_notifications.'/simaNotifications.js'), CClientScript::POS_READY);
//        Yii::app()->clientScript->registerScript("simaNotifications_php_$uniq", include($sima_notifications.'/simaNotifications.php'), CClientScript::POS_END);
//        Yii::app()->clientScript->registerCss("simaNotifications_css_$uniq", file_get_contents($sima_notifications.'/simaNotifications.css'));
//        Yii::app()->clientScript->registerScript("simaNotification_js_$uniq", file_get_contents($sima_notifications.'/simaNotification/simaNotification.js'), CClientScript::POS_READY);
//        Yii::app()->clientScript->registerScript("simaNotification_php_$uniq", include($sima_notifications.'/simaNotification/simaNotification.php'), CClientScript::POS_END);
//        Yii::app()->clientScript->registerCss("simaNotification_css_$uniq", file_get_contents($sima_notifications.'/simaNotification/simaNotification.css'));
        
//        $sima_action_notifications = dirname(__FILE__).'/vueGUI/components/simaActionNotifications';
//        Yii::app()->clientScript->registerScript("simaActionNotifications_js_$uniq", file_get_contents($sima_action_notifications.'/simaActionNotifications.js'), CClientScript::POS_READY);
//        Yii::app()->clientScript->registerScript("simaActionNotifications_php_$uniq", include($sima_action_notifications.'/simaActionNotifications.php'), CClientScript::POS_END);
//        Yii::app()->clientScript->registerCss("simaActionNotifications_css_$uniq", file_get_contents($sima_action_notifications.'/simaActionNotifications.css'));
        
        SIMADesktopApp::registerManual();
        SIMATabs2::registerManual();
        SIMAActivities::registerManual();
        SIMAModelChoose::registerManual();
        SIMASpanWithInfoBox::registerManual();
        SIMAPartialDisplay::registerManual();
        SIMAPDFManipulation::registerManual();
        SIMAExpandableInformations::registerManual();
        SIMAPhoneField::registerManual();
        SIMAButton::registerManual();
            
        SIMATreeVue::registerManual();
        SIMATabsVue::registerManual();
            
        SIMAButtonVue::registerManual();
        SIMAIcons::registerManual();
    }

    public function getGuiTableDefinition($index)
    {
        switch($index)
        {
            case '0': 
                $uniq = SIMAHtml::uniqid();
                $button_parameters = [
                    'title' => Yii::t('GeoModule.Country', 'SyncCountriesFromCodebook'),
                    'onclick' => ["sima.adminMisc.syncCountriesFromCodebook","countries_$uniq"]
                ];
                if(SIMAMisc::isVueComponentEnabled())
                {
                    $additional_options_html = Yii::app()->controller->widget(SIMAButtonVue::class, $button_parameters, true);
                }
                else
                {
                    $additional_options_html = Yii::app()->controller->widget('SIMAButton',[
                        'action' => $button_parameters
                    ], true);
                }
                return [
                   'label' => Yii::t('BaseModule.GuiTable', 'Countries'),
                   'settings' => array(
                       'id' => "countries_$uniq",
                       'model' => Country::model(),
                       'add_button' => true,
                       'additional_options_html' => $additional_options_html
                   )
               ];
            case '1': 
                $uniq = SIMAHtml::uniqid();
                $button_parameters = [
                    'title' => Yii::t('GeoModule.City', 'SyncCitiesFromCodebook'),
                    'onclick' => ["sima.adminMisc.syncCitiesFromCodebook","cities_$uniq"]
                ];
                if(SIMAMisc::isVueComponentEnabled())
                {
                    $additional_options_html = Yii::app()->controller->widget(SIMAButtonVue::class, $button_parameters, true);
                }
                else
                {
                    $additional_options_html = Yii::app()->controller->widget('SIMAButton',[
                        'action' => $button_parameters
                    ], true);
                }
                return [
                    'label' => Yii::t('BaseModule.GuiTable', 'Cities'),
                    'settings' => array(
                        'id' => "cities_$uniq",
                        'model' => City::model(),
                        'add_button' => true,
                        'additional_options_html' => $additional_options_html,
                    )
                ];
            case '2': return [
                 'label' => 'Periodi',
                 'settings' => array(
                     'model' => Period::model(),
                     'add_button' => true
                 )
             ];
            case '3': return [
                'label' => 'Vozačke kategorije',
                'settings' => array(
                    'model' => DriverLicenseCategory::model(),
                    'add_button' => true,
                    'custom_buttons' => [
                        Yii::t('BaseModule.Misc','TakeDataFromCodeBook') => [
                            'func' => 'sima.adminMisc.syncDriverLicenseCategoriesFromCodebook(this)',
                        ],
                    ],
                ),
            ];
            case '4': return [
                'label' => Yii::t('BaseModule.GuiTable', 'ContactTypes'),
                'settings' => array(
                    'model' => ContactType::model(),
                    'add_button' => true
                )
            ];
            case '5': return [
                'label' => Yii::t('MainMenu', 'ClientErrorReports'),
                'settings' => array(
                    'model' => ErrorReport::model(),
                    'add_button' => false,
                    'custom_buttons' => [
                        'Pošalji sve neposlate' => [
                            'func' => "sima.errorReport.sendAllUnsentErrors($(this));"
                        ],
                    ]
                )
            ];
            case '6': return [
                'label' => 'Sesije',
                'settings' => array(
                    'model' => Session::model(),
                    'select_filter' => [
                        'display_scopes' => [
                            'orderByLastSeenDESC'
                        ]
                    ]
                ),
            ];
            case '7': return [
                'label' => Yii::t('BaseModule.Common', 'Themes'),
                'settings' => array(
                    'model' => Theme::model(),
                    'fixed_filter' => [
                        'filter_scopes' => [
                            'hasAccess' => Yii::app()->user->id,
                            'onlyImportantThemes'
                        ]
//                            'status' => Theme::$ACTIVE
//                            'type' => 'PLAIN'
                    ],
                    'add_button' => Yii::app()->user->checkAccess('modelThemeSeeAll')
                )
            ];
            case '8': return [
                'label' => Yii::t('BaseModule.GuiTable', 'QRCodes'),
                'settings' => array(
                    'model' => QR::model()
                )
            ];
            case '9': return [
                'label' => Yii::t('BaseModule.GuiTable', 'Currencies'),
                'settings' => array(
                    'model' => Currency::model(),
                    'add_button' => true,
                    'custom_buttons' => [
                        Yii::t('BaseModule.Misc','TakeDataFromCodeBook') => [
                            'func' => 'sima.adminMisc.syncCurrenciesFromCodebook(this)',
                        ],
                    ],
                )
            ];
            case '10': return [
                    'label' => Yii::t('BaseModule.MeasurementUnit','MeasurementUnits'),
                    'settings' => array(
                        'model' => MeasurementUnit::model(),
                        'add_button' => true,
                        'custom_buttons' => [
                            Yii::t('BaseModule.Misc','TakeDataFromCodeBook') => [
                                'func' => 'sima.adminMisc.syncMeasurementUnitsFromCodebook(this)',
                            ],
                        ],
                    )
                ];
            case '11':
                $uniq = SIMAHtml::uniqid();
                return [
                    'label' => Yii::t('CompanyWorkCode', 'CompanyWorkCodes'),
                    'settings' => array(
                        'id' => "company_work_codes_$uniq",
                        'model' => CompanyWorkCode::model(),
                        'add_button' => true,
                        'custom_buttons' => [
                            Yii::t('AdminModule.Codebook', 'SyncCompanyWorkCodesFromCodebook') => [
                                'func' => "sima.adminMisc.syncWorkCodesFromCodebook('company_work_codes_$uniq')",
                            ],
                        ],
                    ),
                ];
            case '12':
                return [
                    'label' => Yii::t('BaseModule.Notification', 'Notfications'),
                    'settings' => [
                        'model' => Notification::class,
                        'add_button' => false,
                        'fixed_filter' => [
                            'user_id'=>Yii::app()->user->id
                        ],
                        'custom_buttons' => [
                            Yii::t('BaseModule.Notification','MarkAllAsRead') => [
                                'func' => 'sima.notifications.markAllAsRead()',
                            ],
                        ],
                    ]
                ];
            case '13':
                $uniq = SIMAHtml::uniqid();
                $button_parameters = [
                    'title' => Yii::t('GeoModule.PostalCode', 'SyncPostalCodesFromCodebook'),
                    'onclick' => ["sima.adminMisc.syncPostalCodesFromCodebook","postal_codes_$uniq"]
                ];
                if(SIMAMisc::isVueComponentEnabled())
                {
                    $additional_options_html = Yii::app()->controller->widget(SIMAButtonVue::class, $button_parameters, true);
                }
                else
                {
                    $additional_options_html = Yii::app()->controller->widget('SIMAButton',[
                        'action' => $button_parameters
                    ], true);
                }
                return [
                    'label' => Yii::t('GeoModule.PostalCode', 'PostalCodes'),
                    'settings' => [
                        'id' => "postal_codes_$uniq",
                        'model' => PostalCode::class,
                        'add_button' => true,
                        'additional_options_html' => $additional_options_html,
                    ]
                ];
            case '14': return [
                    'label' => Yii::t('GeoModule.Street', 'Streets'),
                    'settings' => [
                        'model' => Street::class,
                        'add_button' => true,
                    ]
                ];
            case '15': return [
                    'label' => Yii::t('GeoModule.Address', 'Addresses'),
                    'settings' => [
                        'model' => Address::class,
                        'add_button' => true,
                    ]
                ];
            case '16': return [
                'label' => UserConfigCounterGUI::modelLabel(true),
                'settings' => [
                    'model' => UserConfigCounter::model(),
                    'add_button' => true,
                ]
            ];
            case '17': return [
                'label' => SystemEventGUI::modelLabel(true),
                'settings' => [
                    'model' => SystemEvent::class
                ]
            ];
            default:
                throw new SIMAGuiTableIndexNotFound('base', $index);
        }
    }
} 
