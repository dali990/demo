/* global Vue */
Vue.directive('sima-tooltip', {
    bind: function (el, binding, vnode) {
        el._tooltip = null;

        var title = typeof binding.value === 'string' ? binding.value : '';
        //Default settings
        var tooltip_params = {
            popperOptions: {
                onCreate: data => {
                    // data is an object containing all the informations computed
                    // by Popper.js and used to style the popper and its arrow
                    // The complete description is available in Popper.js documentation
                    if (tooltip_params.closeOnClickInside) 
                    {
                        el._tooltip.popperInstance.popper.onclick = function (e) {
                            el._tooltip.toggle();
                        };
                    }
                    if(tooltip_params.closeIfElementNotVisible)
                    {
                        let callback = (entries, observer) => { 
                            entries.forEach(entry => {
                                if(!entry.isIntersecting)
                                {
                                    el._tooltip.hide();
                                }
                            });
                        };
                        let observer = new IntersectionObserver(callback);
                        observer.observe(el);
                    }
                },
                onUpdate: data => {
                    // same as `onCreate` but called on subsequent updates
                },
                modifiers: {}
            },
            title: title,
            html: true,
            placement: 'top',
            classes: [],
            template: '<div class="sima-tooltip" role="tooltip"><div class="sima-tooltip-arrow"></div><div class="sima-tooltip-inner"></div></div>',
            arrowSelector: '.sima-tooltip-arrow',
            innerSelector: '.sima-tooltip-inner',
            closeOnClickOutside: true,
            closeOnClickInside: false, //Dodato - onCreate - trigeruje toggle tooltipa na klik
            closeIfElementNotVisible: false, //Dodato - onCreate - trigeruje hide tooltipa kada 'el' nije vidiljiv
            titleFromNextSibling: false, //Upisuje kontent od sledeceg elementa za prikaz u tooltip
        };
        //User settings
        if (typeof binding.value === 'object') {
            Object.assign(tooltip_params, binding.value);
            if (binding.value.hasOwnProperty('classes')) {
                tooltip_params.template = '<div class="sima-tooltip ' + binding.value.classes.join(" ") + '" role="tooltip"><div class="sima-tooltip-arrow"></div><div class="sima-tooltip-inner"></div></div>';
            }
            if (binding.value.hasOwnProperty('boundariesElement')) {
                tooltip_params.popperOptions.modifiers.preventOverflow = {
                    boundariesElement: binding.value.boundariesElement
                };
            }
            if (binding.value.hasOwnProperty('closeOnClickInside')) {
                tooltip_params.closeOnClickInside = binding.value.closeOnClickInside;
            }
            if (binding.value.hasOwnProperty('titleFromNextSibling')) {
                tooltip_params.titleFromNextSibling = binding.value.titleFromNextSibling;
            }
        }
        if(tooltip_params.titleFromNextSibling)
        {
            if(el.nextElementSibling)
            {
                tooltip_params.title = el.nextElementSibling.innerHTML;
            }
        }
        //Create tooltip
        if (!el._tooltip) {
            el._tooltip = new Tooltip(el, tooltip_params);
        }
    },
    inserted: function (el, binding) {

    },
    update: function (el, binding) {
        var title = typeof binding.value === 'string' ? binding.value : '';
        if (typeof binding.value === 'object') {
            if (binding.value.hasOwnProperty('title')) {
                title = binding.value.title;
            }
            if(binding.value.titleFromNextSibling)
            {
                window.pop = el;
                console.log(el.nextElementSibling);
                if(el.nextElementSibling)
                {
                    title = el.nextElementSibling.innerHTML;
                }
            }
        }
        el._tooltip.updateTitleContent(title);
    },
    unbind(el) {
        el._tooltip.dispose();
        delete el._tooltip;
    }
});



