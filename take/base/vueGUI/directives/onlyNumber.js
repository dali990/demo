
/* global Vue */

Vue.directive('onlyNumber', {
    inserted(el, binding) {
        el.oninput = (event) => {
            var regex = /[^0-9\-]+/g;
            
            if (binding.arg === 'positive')
            {
                regex = /[^0-9]+/g;
            }

            var curr_value = (event.target.value).trim();
            var new_value = curr_value.replace(regex, '');
            var parsed_new_value = parseInt(new_value); //parseInt radimo za slucaj da je korisnik - stavio negde u sredini broja
            
            el.value = isNaN(parsed_new_value) ? '' : parsed_new_value;
        };
    }
});