/* global Prism, Vue */

Vue.directive('copy-to-clipboard', {
    bind: function (el, binding) {
        el.addEventListener('click', function(event) {
            el.classList.add("clicked");
            setTimeout(function() {
                el.classList.remove('clicked')
            }, 500);
            
            var success = true;
            var range = document.createRange();
            var selection;

            // Create a temporary element off screen.
            var tmp_elem = $('<div>');
            tmp_elem.css({
                position: "absolute",
                left: "-1000px",
                top: "-1000px",
            });
            
            //Add the input value to the temp element.
            tmp_elem.text(el.innerText);
            $("body").append(tmp_elem);

            // Select temp element.
            range.selectNodeContents(tmp_elem.get(0));
            selection = window.getSelection();
            selection.removeAllRanges();
            selection.addRange(range);
            // Lets copy.
            try { 
              success = document.execCommand ("copy", false, null);
            }
            catch (e) {
                sima.dialog.openWarn(sima.translate('Text not copied!'));
            }
            if(success) 
            {
                tmp_elem.remove();
            }
        });
    }
});


