/* global Prism, Vue, sima */

Vue.directive('tinymce-viewer', {
    bind: function (el, binding) {
        el.classList.add('tinymce-viewer');
        this.tinymceViewer = function(el, binding) {           
            if(binding.oldValue !== binding.value)
            {
                if (typeof el.__vue__ !== 'undefined' && el.__vue__.$options.name === 'sima-iframe')
                {
                    var temp_div = $('<div></div>');
                    $("body").append(temp_div);
                    temp_div[0].innerHTML = binding.value;
                    if(typeof Prism !== 'undefined')
                    {
                        Prism.highlightAllUnder(temp_div[0]);
                    }
                    tinymceViewerSetDisplayHtmlCtrlClass(temp_div);
                    
                    //Sasa A. - ovaj css je iz fajla prismTinymceViewer.css, samo nisam znao kako da ga ucitam ovde pa sam ga duplirao
                    var iframe_content = `
                        <style>
                            .tinymce-viewer code[class*="language-"],
                            .tinymce-viewer pre[class*="language-"] {
                                    color: black;
                                    background: none;
                                    text-shadow: 0 1px white;
                                    font-family: Consolas, Monaco, 'Andale Mono', 'Ubuntu Mono', monospace;
                                    text-align: left;
                                    white-space: pre;
                                    word-spacing: normal;
                                    word-break: normal;
                                    word-wrap: normal;
                                    line-height: 1.5;

                                    -moz-tab-size: 4;
                                    -o-tab-size: 4;
                                    tab-size: 4;

                                    -webkit-hyphens: none;
                                    -moz-hyphens: none;
                                    -ms-hyphens: none;
                                    hyphens: none;
                            }

                            .tinymce-viewer pre[class*="language-"]::-moz-selection, pre[class*="language-"] ::-moz-selection,
                            .tinymce-viewer code[class*="language-"]::-moz-selection, code[class*="language-"] ::-moz-selection {
                                    text-shadow: none;
                                    background: #b3d4fc;
                            }

                            .tinymce-viewer pre[class*="language-"]::selection, pre[class*="language-"] ::selection,
                            .tinymce-viewer code[class*="language-"]::selection, code[class*="language-"] ::selection {
                                    text-shadow: none;
                                    background: #b3d4fc;
                            }

                            @media print {
                                    .tinymce-viewer code[class*="language-"],
                                    .tinymce-viewer pre[class*="language-"] {
                                            text-shadow: none;
                                    }
                            }

                            /* Code blocks */
                            .tinymce-viewer pre[class*="language-"] {
                                    padding: 1em;
                                    margin: .5em 0;
                                    overflow: auto;
                            }

                            .tinymce-viewer :not(pre) > code[class*="language-"],
                            .tinymce-viewer pre[class*="language-"] {
                                    background: #f5f2f0;
                            }

                            /* Inline code */
                            .tinymce-viewer :not(pre) > code[class*="language-"] {
                                    padding: .1em;
                                    border-radius: .3em;
                                    white-space: normal;
                            }

                            .tinymce-viewer .token.comment,
                            .tinymce-viewer .token.prolog,
                            .tinymce-viewer .token.doctype,
                            .tinymce-viewer .token.cdata {
                                    color: slategray;
                            }

                            .tinymce-viewer .token.punctuation {
                                    color: #999;
                            }

                            .namespace {
                                    opacity: .7;
                            }

                            .tinymce-viewer .token.property,
                            .tinymce-viewer .token.tag,
                            .tinymce-viewer .token.boolean,
                            .tinymce-viewer .token.number,
                            .tinymce-viewer .token.constant,
                            .tinymce-viewer .token.symbol,
                            .tinymce-viewer .token.deleted {
                                    color: #905;
                            }

                            .tinymce-viewer .token.selector,
                            .tinymce-viewer .token.attr-name,
                            .tinymce-viewer .token.string,
                            .tinymce-viewer .token.char,
                            .tinymce-viewer .token.builtin,
                            .tinymce-viewer .token.inserted {
                                    color: #690;
                            }

                            .tinymce-viewer .token.operator,
                            .tinymce-viewer .token.entity,
                            .tinymce-viewer .token.url,
                            .tinymce-viewer .language-css .token.string,
                            .tinymce-viewer .style .token.string {
                                    color: #9a6e3a;
                                    background: hsla(0, 0%, 100%, .5);
                            }

                            .tinymce-viewer .token.atrule,
                            .tinymce-viewer .token.attr-value,
                            .tinymce-viewer .token.keyword {
                                    color: #07a;
                            }

                            .tinymce-viewer .token.function,
                            .tinymce-viewer .token.class-name {
                                    color: #DD4A68;
                            }

                            .tinymce-viewer .token.regex,
                            .tinymce-viewer .token.important,
                            .tinymce-viewer .token.variable {
                                    color: #e90;
                            }

                            .tinymce-viewer .token.important,
                            .tinymce-viewer .token.bold {
                                    font-weight: bold;
                            }
                            .tinymce-viewer .token.italic {
                                    font-style: italic;
                            }

                            .tinymce-viewer .token.entity {
                                    cursor: help;
                            }

                            .tinymce-viewer div.code-toolbar {
                                    position: relative;
                            }

                            .tinymce-viewer div.code-toolbar > .toolbar {
                                    position: absolute;
                                    top: .3em;
                                    right: .2em;
                                    transition: opacity 0.3s ease-in-out;
                                    opacity: 0;
                            }

                            .tinymce-viewer div.code-toolbar:hover > .toolbar {
                                    opacity: 1;
                            }

                            .tinymce-viewer div.code-toolbar > .toolbar .toolbar-item {
                                    display: inline-block;
                            }

                            .tinymce-viewer div.code-toolbar > .toolbar a {
                                    cursor: pointer;
                            }

                            .tinymce-viewer div.code-toolbar > .toolbar button {
                                    background: none;
                                    border: 0;
                                    color: inherit;
                                    font: inherit;
                                    line-height: normal;
                                    overflow: visible;
                                    padding: 0;
                                    -webkit-user-select: none; /* for button */
                                    -moz-user-select: none;
                                    -ms-user-select: none;
                            }

                            .tinymce-viewer div.code-toolbar > .toolbar a,
                            .tinymce-viewer div.code-toolbar > .toolbar button,
                            .tinymce-viewer div.code-toolbar > .toolbar span {
                                    color: #bbb;
                                    font-size: .8em;
                                    padding: 0 .5em;
                                    background: #f5f2f0;
                                    background: rgba(224, 224, 224, 0.2);
                                    box-shadow: 0 2px 0 0 rgba(0,0,0,0.2);
                                    border-radius: .5em;
                            }

                            .tinymce-viewer div.code-toolbar > .toolbar a:hover,
                            .tinymce-viewer div.code-toolbar > .toolbar a:focus,
                            .tinymce-viewer div.code-toolbar > .toolbar button:hover,
                            .tinymce-viewer div.code-toolbar > .toolbar button:focus,
                            .tinymce-viewer div.code-toolbar > .toolbar span:hover,
                            .tinymce-viewer div.code-toolbar > .toolbar span:focus {
                                    color: inherit;
                                    text-decoration: none;
                            }

                            .tinymce-viewer p a {
                                text-decoration: underline;
                                cursor: pointer;
                            }
                            .tinymce-viewer p a:link {
                                color: #0000FF;
                            }
                            .tinymce-viewer p a:visited{
                                color: #800080;
                            }
                            .tinymce-viewer p a:active{
                                color: #FF0000;
                            }
                            .tinymce-viewer img {
                                max-width: 100%;
                            }
                        </style>
                    ` + temp_div[0].innerHTML;
                    temp_div.remove();
                    el.__vue__.setContent(iframe_content, 'tinymce-viewer');
                }
                else
                {
                    el.innerHTML = binding.value;
                    if(typeof Prism !== 'undefined')
                    {
                        Prism.highlightAllUnder(el);
                    }
                    tinymceViewerSetDisplayHtmlCtrlClass($(el));
                }
            }
        };
    },
    inserted: function (el, binding) {
        this.tinymceViewer(el, binding);
    },
    update: function (el, binding) {
        //mora i u update da se postavi klasa tinymce-viewer jer ako element nad kojim je pozvana ova direktiva ima bindovanu klasu onda ona pregazi ovu
        el.classList.add('tinymce-viewer');
        this.tinymceViewer(el, binding);
    },
    unbind(el) {
        setTimeout(function() {
            $(el).remove();
        }, 0);
    }
});

function tinymceViewerSetDisplayHtmlCtrlClass(obj) {
    if (sima.getModelDisplayhtmlClickOpenConf() === 'ctrl+klik')
    {
        obj.find('.sima-display-html').each(function() {
            if (!$(this).hasClass('_ctrl'))
            {
                $(this).addClass('_ctrl');
            }
        });
    }
    else
    {
        obj.find('.sima-display-html').each(function() {
            if ($(this).hasClass('_ctrl'))
            {
                $(this).removeClass('_ctrl');
            }
        });
    }
}