/* global Vue, sima */

Vue.component('sima-button', {
    template: '#sima_button_template',
    mixins: [sima.vue.getBaseComponent()],
    props: {
        disabled: {
            type: Boolean,
            default: false
        },
        security_question: [Boolean, String],
        disable_fast_click: { // true, false ili vreme u milisekundama
            type: [Boolean, Number],
            default: true
        }
    },
    data: function () {
        return {
            local_timed_disabled: false,
        };
    },
    computed: {
        button_classes: function() {
            return {
                'sima-btn': true,
                _disabled: this.disabled || this.local_timed_disabled
            };
        },
        security_question_text: function() {
            return this.security_question === true ? sima.translate('SIMAButtonSecurityQuestion') : this.security_question;
        },
        have_click: function() {
            return typeof this.$listeners.click !== 'undefined';
        }
    },
    mounted: function() {
    },
    methods: {
        onClick: function(event) {
            var _this = this;

            event.stopImmediatePropagation();

            if(this.disable_fast_click === true || typeof this.disable_fast_click === 'number')
            {
                this.local_timed_disabled = true;
                setTimeout(
                    function() {
                        _this.local_timed_disabled = false;
                    }, 
                    (typeof _this.disable_fast_click === 'number') ? _this.disable_fast_click : 1000
                );
            }

            //ako je security_question zadat i ako je razlicit od false
            if (
                    typeof this.$options.propsData.security_question !== 'undefined' &&
                    this.security_question !== false
                )
            {
                sima.dialog.openYesNo(this.security_question_text + '?', 
                    function() {
                        sima.dialog.close();
                        _this.executeClick(event);
                    },
                    function() {
                        sima.dialog.close();
                    }
                );
            }
            else
            {
                this.executeClick(event);
            }
        },
        executeClick: function(event) {
            if (this.have_click)
            {
                this.$emit('click', event);
            }
        }
    }
});

