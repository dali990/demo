<script type="text/x-template" id="sima_button_template">
    <div 
        v-bind:class="this.button_classes" 
        v-on:click="this.onClick"
    >
        <slot></slot>
    </div>
</script>