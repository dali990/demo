<script type="text/x-template" id="sima-model-open">
    <sima-button-group>
        <sima-button 
            v-on:click="openInDialog"
            v-bind:class="{_disabled: !dataTrue}"
            title="<?=Yii::t('BaseModule.Common', 'OpenInDialog')?>"
            slot="main_button"
        >
            <i class="sima-icon sil-external-link-alt"></i>
        </sima-button>
        <sima-button 
            v-on:click="openInMainTab"
            v-bind:class="{_disabled: !dataTrue}"
            title="<?=Yii::t('BaseModule.Common', 'OpenInMainTab')?>"
        >
            <span class="sima-btn-title"><?=Yii::t('BaseModule.Common', 'OpenInMainTab')?></span>
        </sima-button>
    </sima-button-group>
</script>