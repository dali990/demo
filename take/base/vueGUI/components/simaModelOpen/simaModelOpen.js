
/* global Vue, sima */

Vue.component('sima-model-open', {
    template: '#sima-model-open',
    mixins: [sima.vue.getBaseComponent()],
    props: {
        model: {
            validator: sima.vue.ProxyValidator
        }
    },
    data: function () {
        return {
            
        };
    },
    computed: {
        dataTrue: function(){
            this.model.path2;
            this.model.has_display;
            
            return this.model.shotStatus('path2') === 'OK' &&
                this.model.shotStatus('has_display') === 'OK';
        }
    },
    mounted: function() {
    },
    methods: {
        openInDialog: function(event) {
            event.stopPropagation();
            if(this.model.has_display)
            {
                sima.dialog.openActionInDialogAsync('defaultLayout', {
                    get_params: {
                        model_name: this.model.get_class, 
                        model_id: this.model.id
                    }
                });
            }
            else
            {
                sima.icons.openModel(this.model.path2, this.model.id);
            }
        },
        openInMainTab: function(event) {
            event.stopPropagation();
            if(this.model.has_display)
            {
                sima.mainTabs.openNewTab( 'defaultLayout', {
                    model_name: this.model.get_class, 
                    model_id: this.model.id
                });
            }
            else
            {
                sima.mainTabs.openNewTab(this.model.path2, this.model.id);
            }
        }
    }
});