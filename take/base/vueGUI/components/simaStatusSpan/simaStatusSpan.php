<script type="text/x-template" id="sima-status-span">
    <span
        v-bind:class="classes"
        v-bind:title='title || _status.title'
        v-on:click='this.clickOnStatus'
    >
        <slot></slot>
    </span>
</script>