/* global Vue, sima */

Vue.component('sima-status-span', {
    template: '#sima-status-span',
    mixins: [sima.vue.getStatusComponent()],
    props:  {
        status: [String, Object],
        use_sima_icon: {
            type: Boolean,
            default: true
        },
        title: {type: String, default: null },
        options: {type: Array, default: function(){ return ['yes', 'no', 'cancel']; } }
    }, 
    computed: {
        classes: function() {
            return {
                '_not-ok': !this._status.value,
                '_ok': this._status.value,
                '_intermediate': this._status.value === null,
                'sima-icon _16': this.use_sima_icon
            };
        },
//        flip_classes: function() {
//            return {
//                '_not-ok': !this._status.value,
//                '_ok': this._status.value,
//                '_intermediate': this._status.value === null,
//                'sima-icon _16': this.use_sima_icon
//            };
//        },
    },
    mounted: function(){
        var _this = this;
        $(this.$el).on('destroyed', function(){
            _this.$destroy();
        });
    },
    methods: {
        clickOnStatus: function(event) {
            var _this = this;
            
            event.stopPropagation();

            sima.ajax.get('base/model/modelConfirmCheckAccess', {
                get_params: {
                    model: this._status.model,
                    column: this._status.column,
                    is_revert: this.is_revert
                },
                data: {
                    model_ids: this._status.model_id
                },
                async: true,
                loadingCircle: $('#body_sync_ajax_overlay'),
                success_function: function() {
                    //ako su dva stanja
                    if (_this._status.allow_null === false)
                    {
                        sima.dialog.openYesNo(_this.getOnClickMessage, function() {
                            _this.confirmStatus(!_this._status.value);
                        });
                    }
                    //ako su tri stanja
                    else
                    {
                        if (_this.is_revert)
                        {
                            sima.dialog.openYesNo(_this.getOnClickMessage, function() {
                                _this.confirmStatus('null');
                            });
                        }
                        else
                        {
                            var _options = [];
                            for (var i in _this.options) 
                            {
                                if(_this.options[i] === 'yes')
                                {
                                    _options.push({
                                        title: sima.translate('ModelStatusYes'),
                                        function: function() {
                                            _this.confirmStatus(true);
                                        }
                                    });
                                }
                                else if(_this.options[i] === 'no')
                                {
                                    _options.push({
                                        title: sima.translate('ModelStatusNo'),
                                        function: function() {
                                            _this.confirmStatus(false);
                                        }
                                    });
                                }
                                else if(_this.options[i] === 'cancel')
                                {
                                    _options.push({
                                        title: sima.translate('ModelStatusCancel'),
                                        function: function() {
                                            sima.dialog.close();
                                        }
                                    });
                                }
                            }
                            sima.dialog.openChooseOption(_this.getOnClickMessage, _options);
                        }
                    }
                }
            });
        }
    }
});