
/* global Vue, sima */

Vue.component('sima-action-notifications', {
    template: '#sima-action-notifications-template',
    mixins: [sima.vue.getBaseComponent()],
    props: {
        actions: {
            type: Array,
            default: function() {
                return [];
            }
        }
    },
    data: function () {
        return {
            
        };
    },
    watch: {
        
    },
    computed: {
        curr_user_action_notifications: function() {
            return sima.vue.shot.getters.model_list(this, 'Notification',{
                model_filter: {
                    scopes: {
                        forUser: sima.getCurrentUserId(),
                        byTime: true,
                        onlyUnseen: true,
                        forActions: [this.actions]
                    }
                }
            });
        },
        curr_user_action_notifications_status: function() {
            return this.curr_user_action_notifications.shotStatus;
        }
    },
    updated: function() {
        var _this = this;

        this.$nextTick(function() {
            sima.layout.allignObject($(_this.$el).parents('.sima-layout-panel:first'));
        });
    },
    methods: {
        onNotificationClose: function(notification_id) {
            sima.ajax.get('base/notification/notifRead', {
                get_params: {
                    id: notification_id
                },
                async: true
            });
        }
    }
});