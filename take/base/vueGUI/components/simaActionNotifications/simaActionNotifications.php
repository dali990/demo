<script type="text/x-template" id="sima-action-notifications-template">
    <div 
        class="sima-action-notifications"
        v-observe-visibility="componentInstanceVisibilityChanged"
    >
        <div
            v-for="curr_user_action_notification in curr_user_action_notifications"
            class="notification"
        >
            <span 
                class="title"
                v-sima-tooltip="{
                    title: curr_user_action_notification.info_text,
                    placement: 'auto-start',
                    classes: ['sima-action-notification-tooltip']
                }"
            >
                {{ curr_user_action_notification.DisplayName }}
            </span>
            <i class="close fas fa-times" 
               v-on:click="onNotificationClose(curr_user_action_notification.id)"
            >
            </i>
        </div>
    </div>
</script>