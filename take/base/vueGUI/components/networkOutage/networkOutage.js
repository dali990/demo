/* global Vue, sima */

Vue.component('network-outage', {
    template: '#network_outage_template',
    mixins: [sima.vue.getBaseComponent()],
    props: {
    },
    data: function () {
        return {
            is_currently_network_outage: false,
            was_network_outage: false,
            network_outage_start_datetime: null,
            network_outage_end_datetime: null,
            registered_components: []
        };
    },
    watch: {
        is_currently_network_outage: function(new_val, old_val){
            if(new_val === old_val)
            {
                return;
            }
            if(new_val === true)
            {
                this.was_network_outage = true;
                this.network_outage_start_datetime = new Date();
                
                $(sima.vue.shot).trigger('network_outage_2');
                $(sima.eventrelay).trigger('network_outage_2');
                $(sima.ajax).trigger('network_outage_2');
                
                this.showNetworkOutageDialog();

            }
            else if(new_val === false)
            {
                this.network_outage_end_datetime = new Date();
            }
        }
    },
    computed: {
    },
    mounted: function() {
//        console.log('networkOutage - mounted');
        $(sima.vue.shot).on('network_outage_1', this.onNetworkOutage);
        $(sima.eventrelay).on('network_outage_1', this.onNetworkOutage);
        $(sima.ajax).on('network_outage_1', this.onNetworkOutage);
    },
    methods: {
        onNetworkOutage: function(){
            if(this.is_currently_network_outage === false)
            {
//                console.log('networkOutage - onNetworkOutage');
                this.is_currently_network_outage = true;
            }
        },
        getWasNetworkOutage: function(){
            return this.was_network_outage;
        },
        getIsCurrentlyNetworkOutage(){
            return this.is_currently_network_outage;
        },
        showNetworkOutageDialog(){
            var html = 'Nestanak mreze' 
                    + '</br>Od: ' + this.network_outage_start_datetime.toLocaleString();

            if(this.was_network_outage === true && this.is_currently_network_outage === false)
            {
                html += '</br>Do: ' + this.network_outage_end_datetime.toLocaleString();
            }

            html += '</br>Moracete osveziti pretrazivac (f5) i potencijalno ulogovati ponovo';

            var _this = this;
            sima.dialog.openCustom(html, 'Nestanak mreze', {
                'close_func': function(){
                    _this.showNetworkOutageDialog();
                }
            });
        }
    }
});
