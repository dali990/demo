/*!
 * vue-nav-tabs v0.5.6
 * (c) 2017-present cristij <joracristi@gmail.com>
 * Released under the MIT License.
 */
/* global sima */

(function (global, factory) {
	typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports) :
	typeof define === 'function' && define.amd ? define(['exports'], factory) :
	(factory((global.vueTabs = {})));
}(this, (function (exports) { 'use strict';

var VueTabs = {
    name: 'vue-tabs',
    props: {
        activeTabColor: String,
        activeTextColor: String,
        disabledColor: String,
        disabledTextColor: String,
        /**
         * Tab title position: center | bottom | top
         */
        textPosition: {
            type: String,
            default: 'center'
        },
        /**
         * Tab type: tabs | pills
         */
        type: {
            type: String,
            default: 'tabs'
        },
        direction: {
            type: String,
            default: 'horizontal'
        },
        /**
         * Centers the tabs and makes the container div full width
         */
        centered: Boolean,
        value: [String, Number, Object],
        use_sima_default_layout: {
            type: Boolean,
            default: false
        }
    },
    data: function data() {
        return {
            activeTabIndex: 0,
            tabs: []
        };
    },

    computed: {
        isTabShape: function isTabShape() {
            return this.type === 'tabs';
        },
        isStacked: function isStacked() {
            return this.direction === 'vertical';
        },
        classList: function classList() {
            var navType = this.isTabShape ? 'nav-tabs' : 'nav-pills';
            var centerClass = this.centered ? 'nav-justified' : '';
            var isStacked = this.isStacked ? 'nav-stacked' : '';
            return 'nav ' + navType + ' ' + centerClass + ' ' + isStacked;
        },
        stackedClass: function stackedClass() {
            return this.isStacked ? 'stacked' : '';
        },
        activeTabStyle: function activeTabStyle() {
            return {
                backgroundColor: this.activeTabColor,
                color: this.activeTextColor
            };
        }
    },
    methods: {
        navigateToTab: function navigateToTab(index, route) {
            this.changeTab(this.activeTabIndex, index, route);
        },
        activateTab: function activateTab(index) {
            this.activeTabIndex = index;
            var tab = this.tabs[index];
            tab.active = true;
            this.$emit('input', tab.title);
        },
        changeTab: function changeTab(oldIndex, newIndex, route) {
            var oldTab = this.tabs[oldIndex];
            var newTab = this.tabs[newIndex];
            if (newTab.disabled) return;
            this.activeTabIndex = newIndex;
            oldTab.active = false;
            newTab.active = true;
            this.$emit('input', this.tabs[newIndex].title);
            this.$emit('tab-change', newIndex, newTab, oldTab);
            this.tryChangeRoute(route);
            
            if (this.use_sima_default_layout)
            {
                var _this = this;
                setTimeout(function() {
                    sima.layout.allignObject($(_this.$el));
                }, 0);
            }
        },
        tryChangeRoute: function tryChangeRoute(route) {
            if (this.$router && route) {
                this.$router.push(route);
            }
        },
        addTab: function addTab(item) {
            var index = this.$slots.default.indexOf(item.$vnode);
            var old_uid = typeof this.tabs[this.activeTabIndex] !== 'undefined' ? this.tabs[this.activeTabIndex]._uid : null;
            this.tabs.splice(index, 0, item);
            if(item.order !== null)
            {
                this.sortTabs(old_uid);
            }
        },
        removeTab: function removeTab(item) {
            var tabs = this.tabs;
            var index = tabs.indexOf(item);
            var old_uid = typeof this.tabs[this.activeTabIndex] !== 'undefined' ? this.tabs[this.activeTabIndex]._uid : null;
            if (index > -1) {
                tabs.splice(index, 1);
            }
            
            if(item.order !== null)
            {
                this.sortTabs(old_uid);
            }
        },
        sortTabs: function(old_uid) {
            var new_tabs = this.tabs;
            for (var i = 0; i < new_tabs.length - 1; i++)
            {
                if (new_tabs[i].order > new_tabs[i + 1].order)  
                { 
                    var temp = new_tabs[i]; 
                    new_tabs[i] = new_tabs[i + 1]; 
                    new_tabs[i + 1] = temp; 
                    
                    i = -1; 
                } 
            }

            var new_index = null;
            for(var i=0; i<new_tabs.length; i++)
            {
                if (new_tabs[i]._uid === old_uid)
                {
                    new_index = i;
                    break;
                }
            }
            if (new_index === null)
            {
                if (this.activeTabIndex === 0)
                {
                    new_index = 0;
                }
                else
                {
                    new_index = this.activeTabIndex - 1;
                }
            }
            this.activeTabIndex = new_index;
            this.tabs = new_tabs;
        },
        getTabs: function getTabs() {
            if (this.$slots.default) {
                return this.$slots.default.filter(function (comp) {
                    return comp.componentOptions;
                });
            }
            return [];
        },
        findTabAndActivate: function findTabAndActivate(tabNameOrIndex) {
            var indexToActivate = this.tabs.findIndex(function (tab, index) {
                return tab.title === tabNameOrIndex || index === tabNameOrIndex;
            });
            if (indexToActivate !== -1) {
                this.changeTab(this.activeTabIndex, indexToActivate);
            } else {
                this.changeTab(this.activeTabIndex, 0);
            }
        },
        renderTabTitle: function renderTabTitle(index) {
            var position = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'top';
            var h = this.$createElement;

            if (this.tabs.length === 0) return;
            var tab = this.tabs[index];
            var active = tab.active,
                title = tab.title;
            var counter = "";
            if(tab.counter !== null)
            {
                counter = h('span', { 'class': 'partner-counter' },["("+tab.counter+")"]);
            }

            var titleStyles = { color: this.activeTabColor };
            if (position === 'center') titleStyles.color = this.activeTextColor;
            var simpleTitle = h(
                'span',
                { 'class': 'title title_' + position, style: active ? titleStyles : {} },
                [position === 'center' && this.renderIcon(index, title), title, counter],
            );

            if (tab.$slots.title) return tab.$slots.title;
            return simpleTitle;
        },
        renderIcon: function renderIcon(index, title) {
            var h = this.$createElement;

            if (this.tabs.length === 0) return;
            var tab = this.tabs[index];
            var icon = tab.icon;
            var icon_children = sima.isEmpty(title) ? [] : ['\xA0'];

            var simpleIcon = h(
                'i',
                { 'class': icon },
                icon_children
            );
            if (!tab.$slots.title && icon) return simpleIcon;
        },
        tabStyles: function tabStyles(tab) {
            if (tab.disabled) {
                return {
                    backgroundColor: this.disabledColor,
                    color: this.disabledTextColor
                };
            }
            return {};
        },
        renderTabs: function renderTabs() {
            var _this = this;

            var h = this.$createElement;

            return this.tabs.map(function (tab, index) {
                if (!tab) return;
                var route = tab.route,
                    id = tab.id,
                    title = tab.title,
                    icon = tab.icon,
                    tabId = tab.tabId;

                var active = _this.activeTabIndex === index;
                return h(
                    'li',
                    {
                        attrs: { name: 'tab',
                            id: 't-' + tabId,
                            'aria-selected': active,
                            'aria-controls': 'p-' + tabId,
                            role: 'tab' },
                        on: {
                            'click': function click() {
                                return !tab.disabled && _this.navigateToTab(index, route);
                            }
                        },

                        'class': ['tab', { active: active }, { disabled: tab.disabled }],
                        key: index },
                    [_this.textPosition === 'top' && _this.renderTabTitle(index, _this.textPosition), h(
                        'a',
                        {
                            attrs: { href: '#',

                                role: 'tab' },
                            on: {
                                'click': function click(e) {
                                    e.preventDefault();
                                    return false;
                                }
                            },

                            style: active ? _this.activeTabStyle : _this.tabStyles(tab),
                            'class': [{ 'active_tab': active }, 'tabs__link'] },
                        [_this.textPosition !== 'center' && !tab.$slots.title && _this.renderIcon(index, title), _this.textPosition === 'center' && _this.renderTabTitle(index, _this.textPosition)]
                    ), _this.textPosition === 'bottom' && _this.renderTabTitle(index, _this.textPosition)]
                );
            });
        }
    },
    render: function render() {
        var h = arguments[0];

        var tabList = this.renderTabs();
        return h(
            'div',
            { 'class': ['vue-tabs', this.use_sima_default_layout ? ('sima-layout-panel _' + this.direction) : '' , this.stackedClass] },
            [h(
                'div',
                { 'class': [{ 'nav-tabs-navigation': !this.isStacked, 'sima-layout-fixed-panel': this.use_sima_default_layout }, { 'left-vertical-tabs': this.isStacked }] },
                [h(
                    'div',
                    { 'class': ['nav-tabs-wrapper', this.stackedClass] },
                    [h(
                        'ul',
                        { 'class': this.classList, attrs: { role: 'tablist' }
                        },
                        [tabList]
                    )]
                )]
            ), h(
                'div',
                { 'class': ['tab-content', { 'right-text-tabs': this.isStacked, 'sima-layout-panel': this.use_sima_default_layout }] },
                [this.$slots.default]
            )]
        );
    },

    watch: {
        tabs: function tabs(newList) {
            if (newList.length > 0 && !this.value) {
                if (newList.length <= this.activeTabIndex) {
                    this.activateTab(this.activeTabIndex - 1);
                } else {
                    this.activateTab(this.activeTabIndex);
                }
            }
            if (newList.length > 0 && this.value) {
                this.findTabAndActivate(this.value);
            }
        },
        value: function value(newVal) {
            this.findTabAndActivate(newVal);
        }
    }
};

var VTab = {
    name: 'v-tab',
    props: {
        title: {
            type: String,
            default: ''
        },
        counter: {
            type: [Number, String],
            default: null
        },
        icon: {
            type: String,
            default: ''
        },
        /***
         * Function to execute before tab switch. Return value must be boolean
         * If the return result is false, tab switch is restricted
         */
        beforeChange: {
            type: Function
        },
        id: String,
        route: {
            type: [String, Object]
        },
        disabled: Boolean,
        transitionName: String,
        transitionMode: String,
        selectOnMount: {
            type: String,
            default: 'false'
        },
        //Order je ceo pozitivan broj pocevsi od 0, jer su to indeksi elemenata niza
        order: {
            type: Number,
            default: null
        },
        tabName: {
            type: String,
            default: ''
        }
    },
    computed: {
        isValidParent: function isValidParent() {
            return this.$parent.$options.name === 'vue-tabs';
        },
        hash: function hash() {
            return '#' + this.id;
        },
        tabId: function tabId() {
            return this.id ? this.id : this.title;
        }
    },
    data: function data() {
        return {
            active: false,
            validationError: null
        };
    },
    mounted: function mounted() {
        this.$parent.addTab(this);
        if(this.selectOnMount === "true")
        {
            this.$parent.findTabAndActivate(this.title);
        }
        else if (this.order !== null && this.order === this.$parent.activeTabIndex)
        {
            this.$parent.findTabAndActivate(this.order);
        }
        else if (this.$parent.$slots.default.indexOf(this.$vnode) === this.$parent.activeTabIndex)
        {
            this.$parent.findTabAndActivate(this.$parent.activeTabIndex);
        }
    },
    destroyed: function destroyed() {
        if (this.$el && this.$el.parentNode) {
            this.$el.parentNode.removeChild(this.$el);
        }
        this.$parent.removeTab(this);
    },
    render: function render() {
        var h = arguments[0];

        return h(
            'section',
            { 'class': ['tab-container', {'sima-layout-panel': this.$parent.use_sima_default_layout}],
                attrs: { id: 'p-' + this.tabId,
                    'aria-labelledby': 't-' + this.tabId,
                    role: 'tabpanel' },
                directives: [{
                    name: 'show',
                    value: this.active
                }]
            },
            [this.$slots.default]
        );
    }
};

var VueTabsPlugin = {
  install: function install(Vue) {
    Vue.component('vue-tabs', VueTabs);
    Vue.component('v-tab', VTab);
  }
};
// Automatic installation if Vue has been added to the global scope.
if (typeof window !== 'undefined' && window.Vue) {
  window.Vue.use(VueTabsPlugin);
  window.VueTabs = VueTabsPlugin;
}

exports['default'] = VueTabsPlugin;
exports.VueTabs = VueTabs;
exports.VTab = VTab;

Object.defineProperty(exports, '__esModule', { value: true });

})));
