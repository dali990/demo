
/* global sima */

$('body').on('sima-layout-allign', '.vue-tabs .tab-content.sima-layout-panel', function(event, height, width, source){

    if (event.target === this)
    {
        if (sima.layout.log())
        {
            console.log(source);
            console.log('HANDLER_vue_tabs_simaresize');
        }

        var _tabs_content = $(this);

        _tabs_content.height(height);
        _tabs_content.width(width);
        
        var fixed_height = 0;
        _tabs_content.children('.sima-layout-fixed-panel').each(function() {
            fixed_height += $(this).outerHeight(true);
        });
        
        var _panels_height = height - fixed_height;

        _tabs_content.children('.sima-layout-panel').each(function() {
            $(this).height(_panels_height);
            $(this).width(width);
        });

        var selected_tab_content = _tabs_content.children('.tab-container:visible');
        sima.layout.setLayoutSize(
                selected_tab_content,
                _panels_height,
                width,
                'TRIGGER_vue_tabs_simaresize'
        );

        event.stopPropagation();
    }
});