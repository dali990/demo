
/* global Vue, sima */

Vue.component('sima-addressbook', {
    template: '#sima-addressbook',
    mixins: [sima.vue.getBaseComponent()],
    props: {
        disabled: {type: Boolean, default: false}
    },
    computed: {
        events(){
            return {
                'keydown': this.onKeyDown,
                'keyup': this.onKeyUp,
                'blur': this.onBlur,
                'focus': this.onFocus
            }
        },
        company_scope(){
            
        },
        person_scope(){
            
        },
        companies(){
            return sima.vue.shot.getters.model_list(this, 'Company', this.criteriaWithScope(this.company_scopes, 'Company'));
        },
        companies_all_cnt(){
            return sima.vue.shot.getters.model_list_count(this, 'Company', this.countCriteriaWithScope([], 'Company'));
        },
        companies_suppliers_cnt(){
            return sima.vue.shot.getters.model_list_count(this, 'Company', this.countCriteriaWithScope('listSuppliers', 'Company'));
        },
        companies_customers_cnt(){
            return sima.vue.shot.getters.model_list_count(this, 'Company', this.countCriteriaWithScope('listCustomers', 'Company'));
        },
        persons(){
            return sima.vue.shot.getters.model_list(this, 'Person', this.criteriaWithScope(this.person_scopes, 'Person'));
        },
        persons_all_cnt(){
//            console.log(this.countCriteriaWithScope([], 'Person'));
            var c = sima.vue.shot.getters.model_list_count(this, 'Person', this.countCriteriaWithScope([], 'Person'));
//            console.log(c);
            return c;
        },
        persons_suppliers_cnt(){
            return sima.vue.shot.getters.model_list_count(this, 'Person', this.countCriteriaWithScope('listSuppliers', 'Person'));
        },
        persons_customers_cnt(){
            return sima.vue.shot.getters.model_list_count(this, 'Person', this.countCriteriaWithScope('listCustomers', 'Person'));
        },
        isEmptySearch(){
            return (
                sima.isEmpty(this.model_filter_person) && 
                sima.isEmpty(this.model_filter_company) && 
                this.partner_search_text === ""
            );
        },
        isOpenSearch(){
            return this.show_search_partner;
        }
    },
    watch: {
        partner_search_text(new_val){
            var _this = this;
            this.debounce(function () {
                _this.partner_search_debounced_text = new_val;
                //_this.event_bus.$emit("event_bus_poper_update");
            }, 500);  
        },
//        company_scopes:{
//            deep: true,
//            handler(new_val){
//                console.log(new_val);
//            }
//        }
    },
    data: function () {
        return {
            event_bus: new Vue(),
            partner_search_text: "",
            partner_search_debounced_text: "",
            offset: 0,
            limit: 50,
            company_scopes:{},
            person_scopes:{},
            show_search_partner: false,
            search_partner_html: null,
            search_partner_popper: null,
            model_filter_person: {},
            model_filter_company: {},
        };
    },
    mounted: function() {
        var _this = this;
        this.event_bus.$on('event_bus_close_partner_search', function(){
            _this.show_search_partner = false;
        });
        this.makePopper();
    },
    destroyed() {
        if(this.search_partner_html)
        {
            this.search_partner_html.remove();
        }
        if (this.search_partner_popper) {
            this.search_partner_popper.destroy();
            this.search_partner_popper = null;
        }
    },
    methods: {
        clearSearch(){
            this.partner_search_text = "";
            this.model_filter_person = {};
            this.model_filter_company = {};
            Vue.set(this, 'model_filter_person', {});
            this.event_bus.$emit('event_bus_clear_search');
        },
        toggleSearch(){
            var _this = this;
            if(_this.isOpenSearch)
            {
                _this.show_search_partner = false;
                _this.closeSearch();
            }
            else
            {
                _this.show_search_partner = true;
                _this.openSearch();
            }
        },
        openSearch(){
            this.show_search_partner = true;
            Vue.set(this, 'show_search_partner', true);
        },
        closeSearch(){
            this.show_search_partner = false;
        },
        addCompany(){
            sima.model.form("Company");
        },
        addPerson(){
            sima.model.form("Person");
        },
        editCompany(id){
            sima.model.form("Company", id);
        },
        editPerson(id){
            sima.model.form("Person", id);
        },
        companyResetScopes(){
            this.company_scopes = {};
            this.limit = 50;
        },
        personResetScopes(){
            this.person_scopes = {};
            this.limit = 50;
        },
        applyScopeForCompany(scope_name, param=null){
            if(scope_name !== '')
            {
                Vue.set(this.company_scopes, scope_name, true);
            }
        },
        applyScopeForPerson(scope_name, param=null){
            if(scope_name !== '')
            {
                Vue.set(this.person_scopes, scope_name, true);
            }
        },
        onKeyUp(e){
        },
        onKeyDown(e){
        },
        onFocus(e){
            this.show_search_partner = true;
        },
        onBlur(e){
            
        },
        //event komponente sima-partner-search
        partnerSearch(model_filter){
            var _this = this;
//            console.log('partnerSearch');
//            console.log(model_filter);
            //this.debounce(function () {
                Vue.set(this, 'model_filter_person', model_filter.Person);
                Vue.set(this, 'model_filter_company', model_filter.Company);
            //}, 500);  
        },
        //Inicijalizacija popera, pozicioniranje komponente sima-partner-search
        makePopper(){
            this.show_search_partner = true;
            this.search_partner_html = document.body.appendChild(this.$refs.simaPartnerSearch.$el);
//            this.search_partner_html = this.$refs.simaPartnerSearch.$el;
            var parent = this.$el.parentElement;
            var z_index = $(parent).zIndex();
            this.search_partner_html.style.zIndex = ++z_index;
            
            this.$nextTick(function() {
                this.search_partner_popper = new Popper(this.$refs.searchInptuField, this.search_partner_html, {
                    placement: 'bottom-start',
                    removeOnDestroy: true
                });
                this.show_search_partner = false;
            });
        },
        //Primenjujemo scope nad modelom company, tabName - naziv scope-a
        companyTabChange(index, selected_tab, old_tab){
            this.companyResetScopes();
            if(selected_tab.tabName !== '')
            {
                this.applyScopeForCompany(selected_tab.tabName);
            }
        },
        personTabChange(index, selected_tab, old_tab){
            this.personResetScopes();
            if(selected_tab.tabName !== '')
            {
                this.applyScopeForPerson(selected_tab.tabName);
            }
        },
        //Opcije (tockic)
        openAddresses(){
            sima.dialog.openActionInDialogAsync('guitable', {
                get_params: {
                    label: "ADRESE",
                    settings: {
                        model: 'Address',
                        add_button: true
                    }
                }
            });
        },
        openStreets(){
            sima.dialog.openActionInDialogAsync('guitable', {
                get_params: {
                    label: "ULICE",
                    settings: {
                        model: 'Street',
                        add_button: true
                    }
                }
            });
        },
        openPostalCodes(){
            sima.dialog.openActionInDialogAsync('guitable', {
                get_params: {
                    label: "POSTANKI BROJEVI",
                    settings: {
                        model: 'PostalCode',
                        add_button: true
                    }
                }
            });
        },
        criteriaWithScope(scope, model_name){
            var criteria = {
                order: 'display_name',
                model_filter: {
                    limit: this.limit,
                    offset: this.offset,
                    scopes: scope,
                    text: this.partner_search_text
                }
            };
            if(model_name === 'Company')
            {
                Object.assign(criteria.model_filter, this.model_filter_company);
            }
            else if(model_name === 'Person')
            {
                Object.assign(criteria.model_filter, this.model_filter_person);
            }
            return criteria;
        },
        countCriteriaWithScope(scope, model_name){
            var criteria = {
                order: 'display_name',
                model_filter: {
                    scopes: scope,
                    text: this.partner_search_text
                }
            };
            if(model_name === 'Company')
            {
                Object.assign(criteria.model_filter, this.model_filter_company);
            }
            else if(model_name === 'Person')
            {
                Object.assign(criteria.model_filter, this.model_filter_person);
            }
            return criteria;
        }
    }
});
