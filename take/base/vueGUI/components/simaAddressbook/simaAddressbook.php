<script type="text/x-template" id="sima-addressbook">
    <div class="sima-addressbook sima-layout-panel" v-observe-visibility="componentInstanceVisibilityChanged">
        <div class="addressbook-header sima-layout-fixed-panel">
            <h2><?=Yii::t('SIMAAddressbook.SIMAAddressbook', 'Addressbook')?></h2>
            <div class="search-field">
                <span class="sima-icon input-icon sil-search"></span>
                <input class="addressbook-search-input"
                    v-on="events"
                    v-bind:placeholder="$attrs.placeholder"
                    v-model="partner_search_text"
                    ref="searchInptuField"
                    v-bind:class="{'_disabled': disabled}"
                    placeholder="<?=Yii::t('SIMAAddressbook.SIMAAddressbook', 'PlaceholderSearchAddressbook')?>"
                />
                <span v-show="!isEmptySearch" @click="clearSearch" class="clear-search input-icon sima-icon sil-times"></span>
                <span @click.stop="toggleSearch" class="input-icon sima-icon sil-check" :class="{'open-search': isOpenSearch}" style="right: 0"></span>
                <sima-partner-search 
                    ref="simaPartnerSearch"
                    v-show="show_search_partner"
                    v-bind:event_bus="event_bus"
                    @search="partnerSearch">
                </sima-partner-search>
            </div>
            <sima-button v-on:click="addCompany">
                <i class="sima-icon sil-building"></i><hr>
                <span class="sima-btn-title"><?=Yii::t('SIMAAddressbook.SIMAAddressbook', 'AddCompany')?></span>
            </sima-button>
            <sima-button v-on:click="addPerson">
                <i class="sima-icon sil-user-plus"></i><hr>
                <span class="sima-btn-title"><?=Yii::t('SIMAAddressbook.SIMAAddressbook', 'AddPerson')?></span>
            </sima-button>
            <sima-button-group class="main-icon-only" ref="simaButtonGroup">
                <span slot="main_icon" class="sima-btn-icon sima-icon sil-cog"></span>
                <sima-button v-on:click="openAddresses">
                    <?=Yii::t('GeoModule.Address', 'Addresses')?>
                </sima-button>
                <sima-button v-on:click="openStreets">
                    <?=Yii::t('GeoModule.PostalCode', 'Poštanski brojevi')?>
                </sima-button>
                <sima-button v-on:click="openPostalCodes">
                    <?=Yii::t('GeoModule.Street', 'Streets')?>
                </sima-button>
            </sima-button-group>
        </div><!-- end addressbook-header -->

        <div class="addressbook-body sima-layout-panel _splitter _vertical">
            <vue-tabs class="addressbook-companies sima-layout-panel" data-sima-layout-init='{"proportion":0.5}' @tab-change="companyTabChange">
                <span class="sima-vue-tabs-left-scroll btn-vue-tabs sima-icon sis-chevron-left"></span>
                <span class="sima-vue-tabs-right-scroll btn-vue-tabs sima-icon sis-chevron-right"></span>
                <!-- Sve firme -->
                <v-tab title="<?=Yii::t('SIMAAddressbook.SIMAAddressbook', 'AllCompanies')?>" 
                    selectOnMount="true"
                    :counter="companies_all_cnt">
                    <vue-perfect-scrollbar class="perfect-scrollbar" v-bind:settings="{ minScrollbarLength: 30 }">
                        <sima-partner-list v-bind:model_list="companies" v-bind:type="'company'"></sima-partner-list>
                    </vue-perfect-scrollbar>
                </v-tab>
                <!-- Oznacene firme -->
                <v-tab title="<?=Yii::t('SIMAAddressbook.SIMAAddressbook', 'MarkedCompanies')?>" 
                    :counter="companies_all_cnt">
                    <vue-perfect-scrollbar class="perfect-scrollbar" v-bind:settings="{ minScrollbarLength: 30 }">
                        <sima-partner-list v-bind:model_list="companies" v-bind:type="'company'"></sima-partner-list>
                    </vue-perfect-scrollbar>
                </v-tab>
                <!-- Dobavljaci -->
                <v-tab title="<?=Yii::t('BaseModule.PartnerList', 'SuppliersListName')?>" 
                v-bind:tabName="'listSuppliers'"
                :counter="companies_suppliers_cnt">
                    <vue-perfect-scrollbar class="perfect-scrollbar" v-bind:settings="{ minScrollbarLength: 30 }">
                        <sima-partner-list v-bind:model_list="companies" v-bind:type="'company'"></sima-partner-list>
                    </vue-perfect-scrollbar>
                </v-tab>
                <!-- Kupci -->
                <v-tab title="<?=Yii::t('BaseModule.PartnerList', 'BuyersListName')?>" 
                    v-bind:tabName="'listCustomers'"
                    :counter="companies_customers_cnt">
                    <vue-perfect-scrollbar class="perfect-scrollbar" v-bind:settings="{ minScrollbarLength: 30 }">
                        <sima-partner-list v-bind:model_list="companies" v-bind:type="'company'"></sima-partner-list>
                    </vue-perfect-scrollbar>
                </v-tab>
            </vue-tabs><!-- end addressbook-companies -->

            <vue-tabs class="addressbook-persons sima-layout-panel" data-sima-layout-init='{"proportion":0.5}' @tab-change="personTabChange">
                <span class="sima-vue-tabs-left-scroll btn-vue-tabs sima-icon sis-chevron-left"></span>
                <span class="sima-vue-tabs-right-scroll btn-vue-tabs sima-icon sis-chevron-right"></span>
                <!-- Sve osobe -->
                <v-tab title="<?=Yii::t('SIMAAddressbook.SIMAAddressbook', 'AllPersons')?>" :counter="persons_all_cnt">
                   <vue-perfect-scrollbar class="perfect-scrollbar" v-bind:settings="{ minScrollbarLength: 30 }">
                        <sima-partner-list v-bind:model_list="persons" v-bind:type="'person'"></sima-partner-list>
                    </vue-perfect-scrollbar>
                </v-tab>
                <!-- Oznacene osobe -->
                <v-tab title="<?=Yii::t('SIMAAddressbook.SIMAAddressbook', 'MarkedPersons')?>" :counter="persons_all_cnt">
                    <vue-perfect-scrollbar class="perfect-scrollbar" v-bind:settings="{ minScrollbarLength: 30 }">
                        <sima-partner-list v-bind:model_list="persons" v-bind:type="'person'"></sima-partner-list>
                    </vue-perfect-scrollbar>
                </v-tab>
                <!-- Dobavljaci -->
                <v-tab title="<?=Yii::t('BaseModule.PartnerList', 'SuppliersListName')?>" 
                    v-bind:tabName="'listSuppliers'" 
                    :counter="persons_suppliers_cnt">
                    <vue-perfect-scrollbar class="perfect-scrollbar" v-bind:settings="{ minScrollbarLength: 30 }">
                        <sima-partner-list v-bind:model_list="persons" v-bind:type="'person'"></sima-partner-list>
                    </vue-perfect-scrollbar>
                </v-tab>
                <!-- Kupci -->
                <v-tab title="<?=Yii::t('BaseModule.PartnerList', 'BuyersListName')?>" 
                    v-bind:tabName="'listCustomers'" 
                    :counter="persons_customers_cnt">
                    <vue-perfect-scrollbar class="perfect-scrollbar" v-bind:settings="{ minScrollbarLength: 30 }">
                        <sima-partner-list v-bind:model_list="persons" v-bind:type="'person'"></sima-partner-list>
                    </vue-perfect-scrollbar>
                </v-tab>
                <!-- HR kandidati -->
                <v-tab title="<?=Yii::t('BaseModule.PartnerList', 'HRCandidatesListName')?>"></v-tab>
                <!-- Zaposleni -->
                <v-tab title="<?=Yii::t('BaseModule.PartnerList', 'EmployeesListName')?>"></v-tab>
                <!-- Bivsi zaposleni -->
                <v-tab title="<?=Yii::t('BaseModule.PartnerList', 'FormerEmployeesListName')?>"></v-tab>
                <!-- Korisnici -->
                <v-tab title="<?=Yii::t('BaseModule.PartnerList', 'Users')?>"></v-tab>
            </vue-tabs><!-- end addressbook-companies -->
        </div><!-- end addressbook-body --> 
    </div><!-- end sima-addressbook --> 
</script>

