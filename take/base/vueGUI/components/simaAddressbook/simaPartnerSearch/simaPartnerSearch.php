<script type="text/x-template" id="sima-partner-search">
    <div class="sima-partner-search" v-click-outside="onClickOutside">
        <div class="company-search">
            <div>
                Naziv
                <input 
                    type="text"
                    v-model="model_filter.Company.name"
                    @input="searchPartner"
                    Placeholder="<?=Yii::t('SIMAAddressbook.SIMAAddressbook', 'PlaceholderSearchByName')?>"
                />
            </div>
            <div>
                PIB
                <input
                    type="text"
                    v-model="model_filter.Company.PIB"
                    @input="searchPartner"
                    Placeholder="<?=Yii::t('SIMAAddressbook.SIMAAddressbook', 'PlaceholderSearchByPIB')?>"
                />
            </div>
            <div>
                MB
                <input 
                    type="text"
                    v-model="model_filter.Company.MB"
                    @input="searchPartner"
                    Placeholder="<?=Yii::t('SIMAAddressbook.SIMAAddressbook', 'PlaceholderSearchByMB')?>"
                />
            </div>
            <div>
                Komentar
                <input 
                    type="text"
                    v-model="model_filter.Company.comment"
                    @input="searchPartner"
                    Placeholder="<?=Yii::t('SIMAAddressbook.SIMAAddressbook', 'PlaceholderSearchByComment')?>"
                />
            </div>
            <div>
                Šifra delatnosti
                <input 
                    type="text"
                    v-model="model_filter.Company.work_code.code"
                    @input="searchPartner"
                    Placeholder="<?=Yii::t('SIMAAddressbook.SIMAAddressbook', 'PlaceholderSearchByActivityCode')?>"
                />
            </div> 
        </div>
        <div class="person-search">
            <div>
                Ime
                <input 
                    type="text"
                    v-model="model_filter.Person.firstname"
                    @input="searchPartner"
                    Placeholder="<?=Yii::t('SIMAAddressbook.SIMAAddressbook', 'PlaceholderSearchByFirstname')?>"
                />
            </div>
            <div>
                Prezime 
                <input 
                    type="text"
                    v-model="model_filter.Person.lastname"
                    @input="searchPartner"
                    Placeholder="<?=Yii::t('SIMAAddressbook.SIMAAddressbook', 'PlaceholderSearchByLastname')?>"
                />
            </div>
            <div>
                Adresa
                <input type="text"
                    v-model="model_filter.Person.partner_to_address.address.display_name"
                    @input="searchPartner"
                    Placeholder="<?=Yii::t('SIMAAddressbook.SIMAAddressbook', 'PlaceholderSearchByAddress')?>"
                />
            </div>
        </div>
        <div>
            <span @click="closeSearch" class="clear-search input-icon sima-icon sil-times"></span>
        </div>
    </div>
</script>

