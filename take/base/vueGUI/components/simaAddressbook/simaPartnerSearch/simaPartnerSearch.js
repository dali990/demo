
/* global Vue, sima */

Vue.component('sima-partner-search', {
    template: '#sima-partner-search',
    mixins: [sima.vue.getBaseComponent()],
    props: {
        disabled: {type: Boolean, default: false},
        event_bus: {type: Object},
    },
    computed: {
        
    },
    watch: {
        'model_filter.Person.address'(new_val){
            this.model_filter.Company.addresses.display_name = new_val;
        }
    },
    data: function () {
        return {
            search_timeout: null,
            model_filter: {
                Company: {
                    name: "",
                    PIB: "",
                    MB: "",
                    comment: "",
                    work_code: {
                        code: ""
                    },
                    partner_to_address: {
                        address: {
                            display_name: ""
                        }
                    },
                },
                Person: {
//                    firstname: "",
//                    lastname: "",
                    partner_to_address: {
                        address: {
                            display_name: ""
                        }
                    },
                }
            },
            model_filter_init: null //za reset model_filter
        };
    },
    mounted: function() {
        var _this = this;
        this.model_filter_init = this.model_filter;
        this.event_bus.$on('event_bus_clear_search', function(){
            _this.clearSearch();
        });
    },
    destroyed() {
        
    },
    methods: {
        onClickOutside(e){
            if(!e.target.closest('.addressbook-search-input'))
            {
                this.closeSearch();
            }
        },
        searchPartner(){
            this.model_filter.Company.partner_to_address.address.display_name = this.model_filter.Person.partner_to_address.address.display_name;
            var _this = this;
            var globalTimeout = this.search_timeout;
            if (globalTimeout !== null) {
                clearTimeout(globalTimeout);
                this.search_timeout = globalTimeout;
            }
            this.search_timeout = setTimeout(function () {
                console.log('search');
                console.log(_this.model_filter);
                _this.$emit('search', _this.model_filter);
            }, 500);
//            this.debounce(function () {
//                _this.$emit('search', _this.model_filter);
//            }, 500);  
        },
        clearSearch(){
            this.model_filter.Company = {
                name: "",
                PIB: "",
                MB: "",
                comment: "",
                work_code: {
                    code: ""
                },
                partner_to_address: {
                    address: {
                        display_name: ""
                    }
                },
            };
            this.model_filter.Person = {
                firstname: "",
                lastname: "",
                partner_to_address: {
                    address: {
                        display_name: ""
                    }
                },
            };
//            this.model_filter = this.model_filter_init;
        },
        closeSearch(){
            this.event_bus.$emit('event_bus_close_partner_search');
        }
    }
});