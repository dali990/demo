
/* global Vue, sima */

Vue.component('sima-partner-list', {
    template: '#sima-partner-list',
    mixins: [sima.vue.getBaseComponent()],
    props: {
        model_list: {type: Array},
        type: {type: String, default: ""},
        event_bus: {type: Object}
    },
    computed: {
//        models(){
//            var scopes = 'Person';
//            if(this.type == 'company') model_name = 'Company';
//            var criteria = {
//                order: 'display_name',
//                model_filter: {
//                    limit: this.limit,
//                    offset: this.offset,
//                    scopes: this.person_scopes,
//                    text: this.partner_search_debounced_text,
//                    ...this.model_filter_person
//                }
//            };
//            var model_name = 'Person';
//            if(this.type == 'company') model_name = 'Company';
//            return sima.vue.shot.getters.model_list(this, model_name, criteria);
//        },
    },
    watch: {
        
    },
    data: function () {
        return {
            offset: 0,
            limit: 50,
            company_scopes:{},
            person_scopes:{},
            model_filter_person: {},
            model_filter_company: {},
        };
    },
    mounted: function() {       
        var phone_icon = this.$el.querySelector(".all-info.sir-phone");
        var phone_popper = this.$el.querySelector(".all-info.phone-popper");
//        this.$nextTick(function() {
//           var instance = new Tooltip(phone_icon,{
//                title: trigger.getAttribute('data-tooltip'),
//                trigger: "hover",
//            });
//        });
    },
    destroyed() {
        
    },
    methods: {
        getInitials(model){
            if(model)
            {
                if(this.type === 'company' && model.name)
                {
                    var initials = model.name.trim().split(" ");
                    var first = initials[0][0];
                    var second = "";
                    if(initials[0][1])
                    {
                        second = initials[0][1];
                    }
                    else if(initials[1][0])
                    {
                        second = initials[1][0];
                    }
                    return first+second.toUpperCase();
                }
                else if(this.type === 'person' && model.firstname)
                {
                    return (model.firstname.trim()[0]+model.lastname.trim()[0]).toString().toUpperCase();
                }
            }
            return "";
        },
        partnerTitle(model){
            if(this.type === 'company')
            {
                return model.name;
            }
            else if(this.type === 'person')
            {
                return model.firstname+" "+model.lastname;
            }
        },
        showPopup(event){
//            console.log(event);
//            event.target.nextElementSibling.style.display = "block";
//            this.search_partner_popper = new Popper(event.target, event.target.nextElementSibling, {
//                placement: 'bottom',
//                removeOnDestroy: true
//            });
                
//            var instance = new Tooltip(event.target,{
//                html: true,
//                title: event.target.nextElementSibling.innerHTML,
//                trigger: "hover",
//                class: 'sima-partner-list-tooltip',
//                placement: 'bottom'
//            });
        },
        hidePopup(event){
//            console.log(event);
//            window.ev = event.target;
//            if(!event.target.closest(".partner-list-popper"))
//            {
//                event.target.nextElementSibling.style.display = "none";
//            }
        },
        toolTipConent(el){
//            console.log(el);
            if(el)
            return el.innerHTML;
        }
    }
});