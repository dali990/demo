<script type="text/x-template" id="sima-partner-list">
    <div class="sima-partner-list" v-observe-visibility="componentInstanceVisibilityChanged">
        <vue-perfect-scrollbar class="perfect-scrollbar" v-bind:settings="{ minScrollbarLength: 30 }">
            <div v-for="model in model_list" class="partner-card">
                <div class="partner-initials">{{getInitials(model)}}</div>
                <div class="partner-maincontent">
                    <div class="partner-topbar" >
                        <div class="name-and-list" >
                           {{model.DisplayName}}
                            <span>Dobavljaci, Kupci</span>
                        </div><!-- end name-and-list -->
                        <div class="all-options">
<!--                            <sima-model-options v-bind:model="model"></sima-model-options>-->
                            <i class="sima-icon sis-ellipsis-h"></i>
                        </div><!-- end all-options -->
                    </div><!-- end partner-topbars -->
                    <div class="partner-bottombar">
                        <div class="main-info" v-copy-to-clipboard v-if="model.main_or_any_phone">
                            <span class="sima-icon sir-phone"></span>
                            <i>{{model.main_or_any_phone}}</i>
                        </div>
                        <div class="main-info" v-copy-to-clipboard v-else-if="model.main_or_any_email">
                            <span class="sima-icon sir-envelope"></span>
                            <i>{{model.main_or_any_email}}</i>
                        </div>
                        <div class="main-info" v-copy-to-clipboard v-else-if="model.main_or_any_address">
                            <span class="sima-icon sir-map-marker-alt"></span>
                            <i>{{model.main_or_any_address}}</i>
                        </div>
                        <div class="main-info" v-copy-to-clipboard v-else-if="model.main_or_any_bank_account">
                            <span class="sima-icon sir-globe-americas"></span>
                            <i>{{model.main_or_any_bank_account}}</i>
                        </div>
                        <div class="main-info" v-copy-to-clipboard v-else-if="model.comment">
                            <span class="sima-icon sir-quote-left"></span>
                            <i>{{model.comment}}</i>
                        </div>
                        <div class="main-info" v-else>
                            <span class="sima-icon sir-info-circle"></span>
                            <i>Nema informacija</i>
                        </div><!-- end main-info -->
                        <div class="all-info">
                            <span class="sima-icon sir-phone phone-popper" v-bind:class="{'no-info':!model.main_or_any_phone}" @mouseover="showPopup($event)" @mouseout="hidePopup($event)"
                                v-sima-tooltip="{
                                    title: toolTipConent($el),
                                    placement: 'right',
                                    classes: ['sima-notification-tooltip'],
                                    boundariesElement: 'viewport',
                                    closeIfElementNotVisible: true,
                                    titleFromNextSibling: true
                                }"
                                ></span>
                            <div class="partner-list-popper" style="display:none">
                                <div class="partner-list-tooltip-content">
                                    <p>Telefon</p>
                                </div>
<!--                                <div x-arrow></div>-->
                                <div class="pl-popper-arrow" x-arrow data-popper-arrow></div>
                            </div>
                            <span class="sima-icon sir-envelope" v-bind:class="{'no-info':!model.main_or_any_email}"></span>
                            <span class="sima-icon sir-map-marker-alt" v-bind:class="{'no-info':!model.main_or_any_address}"></span>
                            <span class="sima-icon sir-globe-americas" v-bind:class="{'no-info':!model.main_or_any_bank_account}"></span>
                            <span class="sima-icon sir-quote-left" v-bind:class="{'no-info':!model.comment}"></span>
                            <span class="sima-icon sir-star"></span>
                        </div><!-- end all-info -->
                    </div><!-- end partner-bottombar -->
                </div> 
            </div>
        </vue-perfect-scrollbar>
    </div>
</script>

