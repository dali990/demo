/* global Vue, sima */

Vue.component('sima-button-group', {
    template: '#sima_button_group_template',
    mixins: [sima.vue.getBaseComponent()],
    props: {
        dropdown_position: {
            type: String,
            default: 'bottom' //top, right, bottom, left
        },
        hover_display_dropdown: {
            type: Boolean,
            default: false
        }
    },
    data: function () {
        return {
            show_dropdown: false
        };
    },
    computed: {
        dropdown_horizontal: function(){
            var result = null;
            if(this.dropdown_position === 'up' || this.dropdown_position === 'bottom')
            {
                result = false;
            }
            else if(this.dropdown_position === 'left' || this.dropdown_position === 'right')
            {
                result = true;
            }
            else
            {
                var message = sima.translate('DropdownPositionMustBeUpBottomLeftOrRight');
                console.error(message);
            }
            return result;
        },
        dropdown_classes: function() {
            return {
                'btn-dropdown-box': true,
                _horizontal: this.dropdown_horizontal,
                _vertical: !this.dropdown_horizontal
            };
        },
        sort_position_class: function() {
            return {
                'icon-btn sima-icon sil-chevron-down': this.dropdown_position === 'bottom',
                'icon-btn sima-icon sil-chevron-up': this.dropdown_position === 'up',
                'icon-btn sima-icon sil-chevron-left icon-left': this.dropdown_position === 'left',
                'icon-btn sima-icon sil-chevron-right': this.dropdown_position === 'right'
            };
        },
        exist_main_button: function() {
            return (typeof this.$slots.main_button !== 'undefined');
        },
        exist_main_icon: function() {
            return (typeof this.$slots.main_icon !== 'undefined');
        }
    },
    mounted: function() {
    },
    methods: {
        toggleDropdownDisplay: function(forcing_display){
            if(forcing_display === true || (typeof forcing_display === 'undefined' && !this.show_dropdown))
            {
                this.show_dropdown = true;
                
                this.bindOnScroll();
            }
            else
            {
                this.show_dropdown = false;
                
                this.unbindOnScroll();
            }
        },
        bindOnScroll: function()
        {
            var local_this = this;
            var this_el = $(this.$el);
            var on_scroll_interval =  this_el.onPositionChanged(function(){
                if(local_this.on_scroll_action === 'HIDE')
                {
                    local_this.toggleDropdownDisplay(false);
                }
                else /// TO IMPLEMENT: MOVE
                {
                    
                }
            }, 10);
            local_this.on_scroll_interval = on_scroll_interval;
        },
        unbindOnScroll: function(){
            clearInterval(this.on_scroll_interval);
        },
        onCloseDropdown: function(){
            this.toggleDropdownDisplay(false);
        },
        onMouseEnter: function(){
            if(this.hover_display_dropdown === true)
            {
                this.toggleDropdownDisplay(true);
            }
        },
        onMouseLeave: function(){
            if(this.hover_display_dropdown === true)
            {
                this.toggleDropdownDisplay(false);
            }
        },
        onClickOutside: function(){
            this.toggleDropdownDisplay(false);
        }
    }
});

