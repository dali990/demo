<script type="text/x-template" id="sima_button_group_template">
    <div 
        class="sima-btn-group"
        v-on:mouseenter="onMouseEnter" 
        v-on:mouseleave.stop.prevent="onMouseLeave"
        v-click-outside="onClickOutside"
    >
        <template v-if="exist_main_button || exist_main_icon">
            <div v-if="exist_main_button">
                <span v-if="dropdown_position === 'left'" v-bind:class="sort_position_class" v-on:click.stop="toggleDropdownDisplay()"></span>
                <slot name="main_button"></slot>
                <span v-if="dropdown_position !== 'left'" v-bind:class="sort_position_class" v-on:click.stop="toggleDropdownDisplay()"></span>
            </div>
            <sima-button 
                v-if="exist_main_icon" 
                v-on:click="toggleDropdownDisplay()"
                v-bind:disable_fast_click="false"
            >
                <slot name="main_icon"></slot>
            </sima-button>
            <sima-popup
                v-bind:class="this.dropdown_classes"
                v-bind:show="show_dropdown"
                v-bind:position="dropdown_position"
                @close-subactions="onCloseDropdown"
            >
                <slot></slot>
            </sima-popup>
        </template>
        <template v-else>
            <slot></slot>
        </template>
    </div>
</script>