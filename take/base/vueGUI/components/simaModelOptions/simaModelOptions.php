<script type="text/x-template" id="sima-model-options">
    <sima-button-group class="sima-model-options">
        <component 
            v-for="(option, index) in this.options"
            v-bind:key="index"
            class="sima-model-option"
            v-bind:is="isOptionComponent(option) ? option : 'sima-yii-button'"
            v-bind="isOptionComponent(option) ? option_as_component_params : option"
        ></component>
    </sima-button-group>
</script>
