
/* global Vue, sima */

Vue.component('sima-model-options', {
    template: '#sima-model-options',
    mixins: [sima.vue.getBaseComponent()],
    props: {
        model: {
            validator: sima.vue.ProxyValidator
        },
        option_icon_size: {
            type: Number,
            default: 16
        },
        exclude_options: {
            type: Array,
            default: function() {
                return [];
            }
        }
    },
    data: function () {
        return {
            
        };
    },
    watch: {
        
    },
    computed: {
        model_options: function() {
            return this.model.model_options;
        },
        options: function() {
            return this.getOptions();
        },
        option_as_component_params: function() {
            return {
                model: this.model,
                icon_size: this.option_icon_size
            };
        }
    },
    mounted: function() {
    },
    methods: {
        isOptionComponent: function(option) {
            return typeof option === 'string';
        },
        getOptions: function() {
            var options = [];
            
            var model_options = this.model_options;
            for (var i = 0; i < model_options.length; i++) 
            {
                var model_option = model_options[i];
                
                if ($.inArray(model_option, this.exclude_options) !== -1)
                {
                    continue;
                }

                switch(model_option) 
                {
                    case 'form': 
                        options.push('sima-model-edit');
                        break;
                    case 'delete':
                        options.push('sima-model-delete');
                        break;
                    case 'recycle':
                        options.push('sima-model-recycle');
                        break;
                    case 'new_tab':
                    case 'new_tab2':
                    case 'open':
                        options.push('sima-model-open');
                        break;
                    case 'refresh':
                        options.push('sima-model-refresh');
                        break;
                    case 'copy':
                        options.push('sima-model-copy');
                        break;
                    default: 
                        //ovo treba vremenom da nestane. Inace mora da se nalazi ovde zbog redosleda opcija
                        $.each(this.model.model_options_data, function(option_key, option_value) {
                            if (model_option === option_key && !sima.isEmpty(option_value))
                            {
                                options.push(option_value);
                            }
                        });
                        break;
                }
            }

            return options;
        }
    }
});