
/* global Vue, sima */

Vue.component('sima-user-display-online-status', {
    template: '#sima_user_display_online_status_template',
    props: {
        user: {
            validator: sima.vue.ProxyValidator
        }
    },
    computed: {
        online_status: function(){
            return this.user.manualUpdate('online_status');
        },
        online_status_class: function(){
            return {
                _active: this.online_status === 'active',
                _idle: this.online_status === 'idle',
                _inactive: this.online_status === 'inactive'
            };
        }
    },
    mounted: function() {
        var _this = this;
        $(this.$el).on('destroyed', function(){
            _this.$destroy();
        });
    },
    methods: {
    }
});