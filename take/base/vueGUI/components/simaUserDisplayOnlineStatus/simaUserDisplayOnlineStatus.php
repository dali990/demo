<script type="text/x-template" id="sima_user_display_online_status_template">
    <div 
        class="sima-user-display-online-status"
    >
        <span class="sima-icon sis-circle user_status" v-bind:class="online_status_class"></span>
    </div>
</script>