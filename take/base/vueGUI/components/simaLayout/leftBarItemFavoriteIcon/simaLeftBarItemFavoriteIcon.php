<script type="text/x-template" id="sima_layout_left_bar_item_favorite_icon_template">
    <div 
        class="sima-layout-left-bar-item-favorite-icon"
        v-bind:class="{_disabled: disabled}"
        v-if="item.is_selectable" 
        v-show="(is_favorite && !item.is_inside_favorites) || item.mouse_is_over_current_item"
        v-on:mouseover="mouseOver" v-on:mouseleave="mouseLeave"
        title=""
    >
        <i
            v-bind:class="{'fa-star item-favorite': true, 'fas': is_favorite, 'far': !is_favorite}" 
            v-on:click="toggleItemFromFavorites"
            title="<?=Yii::t('BaseModule.MainMenu', 'ToggleItemFromFavorites')?>"
        ></i>
        <div 
            class="item-favorite-popup"
            v-if="item.path !== 'opened_items' && is_favorite && mouse_is_over_favorite_icon" 
            v-on:click="$event.stopPropagation()"
            v-bind:style="{top: favorite_popover_position_top + 'px', 'max-height': favorite_popover_max_height + 'px'}" 
        >
            <sima-layout-left-bar-item-favorite-icon-group
                v-for="(group, index) in item.main_bus.sorted_main_menu_favorite_groups" 
                v-bind:key="index"
                v-bind:item="item"
                v-bind:group="group"
            ></sima-layout-left-bar-item-favorite-icon-group>
        </div>
    </div>
</script>