
/* global Vue, sima */

Vue.component('sima-layout-left-bar-item-favorite-icon', {
    template: '#sima_layout_left_bar_item_favorite_icon_template',
    mixins: [sima.vue.getBaseComponent()],
    props: {
        item: Object
    },
    computed: {
        disabled: function() {
            return this.local_disabled || !this.item.main_bus.favorite_items_ready;
        },
        is_favorite: function() {
            var is_favorite = false;

            if (this.item.path === 'opened_items')
            {
                is_favorite = this.item.favorite_group_id === this.item.main_bus.default_main_menu_favorite_group.id;
            }
            else
            {
                is_favorite = this.item.is_favorite;
            }
            
            if (this.local_disabled)
            {
                return !is_favorite;
            }
            
            return is_favorite;
        },
        favorite_group_id: function() {
            var favorite_group_id = this.item.main_bus.default_main_menu_favorite_group.id;
            if (this.item.is_inside_favorites)
            {
                favorite_group_id = this.item.favorite_group_id;
            }
            
            return favorite_group_id;
        }
    },
    data: function () {
        return {
            mouse_is_over_favorite_icon: false,
            favorite_popover_position_top: 0,
            favorite_popover_max_height: 0,
            mouse_over_leave_favorite_icon_timeout: null,
            local_disabled: false
        };
    },
    watch: {
        
    },
    mounted: function() {
        
    },
    updated: function() {
        
    },
    methods: {
        mouseOver: function() {
            var _this = this;

            if (this.mouse_over_leave_favorite_icon_timeout !== null)
            {
                clearTimeout(this.mouse_over_leave_favorite_icon_timeout);
            }
            
            this.mouse_over_leave_favorite_icon_timeout = setTimeout(function() {
                _this.mouse_is_over_favorite_icon = true;

                _this.favorite_popover_position_top = $(_this.$el).offset().top + $(_this.$el).height();
                _this.favorite_popover_max_height = $(window).height() - _this.favorite_popover_position_top - 20; //20px padding
            }, 100);
        },
        mouseLeave: function() {
            var _this = this;

            if (this.mouse_over_leave_favorite_icon_timeout !== null)
            {
                clearTimeout(this.mouse_over_leave_favorite_icon_timeout);
            }
            
            this.mouse_over_leave_favorite_icon_timeout = setTimeout(function() {
                _this.mouse_is_over_favorite_icon = false;
            }, 100);
        },
        toggleItemFromFavorites: function(event) {
            event.stopPropagation();
            var _this = this;
            
            var get_params = {
                favorite_group_id: this.favorite_group_id,
                label: this.item.label,
                component_name: this.item.component_name,
                action: JSON.stringify(this.item.action)
            };
            
            if (this.item.path === 'opened_items')
            {
                var prev_favorite_item = $(this.item.$el).prevAll('._favorite:first');
                if (!sima.isEmpty(prev_favorite_item))
                {
                    prev_favorite_item_vue = prev_favorite_item[0].__vue__;
                    if (!sima.isEmpty(prev_favorite_item_vue))
                    {
                        get_params.prev_item_to_group_id = prev_favorite_item_vue.favorite_item_to_group_id;
                    }
                }
                var next_favorite_item = $(this.item.$el).nextAll('._favorite:first');
                if (!sima.isEmpty(next_favorite_item))
                {
                    next_favorite_item_vue = next_favorite_item[0].__vue__;
                    if (!sima.isEmpty(next_favorite_item_vue))
                    {
                        get_params.next_item_to_group_id = next_favorite_item_vue.favorite_item_to_group_id;
                    }
                }
            }

            this.local_disabled = true;
            sima.ajax.get('base/mainMenu/toggleItemFromFavoriteGroup', {
                get_params: get_params,
                async: true,
                failure_function: function(response){
                    _this.local_disabled = false;
                    sima.dialog.openWarn(response.message);
                },
                success_function: function(response){
                    _this.local_disabled = false;
                }
            });
        }
    }
});