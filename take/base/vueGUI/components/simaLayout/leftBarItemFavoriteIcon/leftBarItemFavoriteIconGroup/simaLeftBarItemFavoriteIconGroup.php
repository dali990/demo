<script type="text/x-template" id="sima_layout_left_bar_item_favorite_icon_group_template">
    <div
        class="sima-layout-left-bar-item-favorite-icon-group sima-text-dots"
        v-bind:class="{_selected: is_selected, _disabled: disabled}"
        v-on:click="toggleItemFromFavoriteGroup"
        v-bind:title="group.group_name"
    >
        {{ group.group_name }}
    </div>
</script>