
/* global Vue, sima */

Vue.component('sima-layout-left-bar-item-favorite-icon-group', {
    template: '#sima_layout_left_bar_item_favorite_icon_group_template',
    mixins: [sima.vue.getBaseComponent()],
    props: {
        item: Object,
        group: {
            validator: sima.vue.ProxyValidator
        }
    },
    computed: {
        is_selected: function() {
            return this.group.scoped('has_curr_user_item', this.item.database_uniq) === true;
        },
        disabled: function() {
            return this.group.shotStatus('has_curr_user_item', this.item.database_uniq) !== 'OK';
        }
    },
    data: function () {
        return {
            
        };
    },
    watch: {
        
    },
    mounted: function() {
        
    },
    updated: function() {
        
    },
    methods: {
        toggleItemFromFavoriteGroup: function(event) {
            var _this = this;

            event.stopPropagation();
            
            var get_params = {
                favorite_group_id: this.group.id,
                label: this.item.label,
                component_name: this.item.component_name,
                action: JSON.stringify(this.item.action)
            };
            
            this.group.setScoped('has_curr_user_item', !this.is_selected, this.item.database_uniq);
            sima.ajax.get('base/mainMenu/toggleItemFromFavoriteGroup', {
                get_params: get_params,
                async: true,
                failure_function: function(response) {
                    _this.group.setDirty('has_curr_user_item', _this.item.database_uniq);
                    sima.dialog.openWarn(response.message);
                }
            });
        }
    }
});