<script type="text/x-template" id="sima_layout_display_panel_template">
    <div class="sima-layout-display-panel sima-layout-panel">
        <component 
            v-if="component_name !== null"
            v-bind:is="component_name"
            class="sima-layout-panel"
        >
        </component>
    </div>
</script>