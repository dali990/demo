
/* global Vue, sima */

Vue.component('sima-layout-display-panel', {
    template: '#sima_layout_display_panel_template',
    mixins: [sima.vue.getBaseComponent()],
    props: {
        main_bus: Object,
        item: Object
    },
    computed: {
        component_name: function() {
            return !sima.isEmpty(this.item.component_name) ? this.item.component_name : null;
        },
        action: function() {
            return !sima.isEmpty(this.item.action) ? this.item.action : null;
        },
        code: function() {
            return this.item.code;
        }
    },
    data: function () {
        return {
            
        };
    },
    watch: {
        
    },
    mounted: function() {
        var _this = this;
        
        //iako se nakon dovlacenje content-a radi align, treba nam pre dovlacenje da bi imali dimenzije kontejnera gde se ubacuje kontent zbog sima ajax loadingCircle-a
        sima.layout.allignObject($(_this.$el).parent());
        
        if (this.action !== null)
        {
            this.loadActionHtml();
        }
        
        this.main_bus.$on('applySelectorForCode', function(code, selector) {
            if (_this.code === code)
            {
                sima.selectSubObj($(_this.$el), selector);
            }
        });

        this.main_bus.$on('reloadItemAction', function(code) {
            if (_this.code === code)
            {
                _this.loadActionHtml();
            }
        });
    },
    updated: function() {
        
    },
    methods: {
        loadActionHtml: function() {
            var _this = this;

            var action = this.action;
            var get_params = {};
            var post_params = {};
            if (typeof action === 'string')
            {
                action = [action];
            }
            if (typeof action[1] !== 'undefined')
            {
                get_params = action[1];
            }
            if (typeof action[2] !== 'undefined')
            {
                post_params = action[2];
            }
            
            if (!sima.isEmpty(_this.item.getSelector()))
            {
                post_params.selector = _this.item.getSelector();
            }
            
            var ajax_params = {
                get_params: $.extend(true, {}, get_params),
                data: $.extend(true, {}, post_params),
                async: true,
                loadingCircle: $(_this.$el),
                success_function: function(response) {
                    if (!sima.isEmpty(response.title))
                    {
                        Vue.set(_this.main_bus.action_labels, _this.code, response.title);
                        _this.main_bus.$emit('setLabelForCode', _this.code, response.title);
                    }
                    
                    sima.set_html($(_this.$el), response.html);

                    if (
                            !sima.isEmpty(_this.item.getSelector()) && 
                            (
                                typeof response.selected === 'undefined' || !response.selected
                            )
                        )
                    {
                        _this.$nextTick(function() {
                            sima.selectSubObj($(_this.$el), _this.item.getSelector());
                        });
                    }
                },
                fatal_failure_function: function(jqXHR, textStatus, response) {
                    _this.onErrorRemoveOpenedItem();
                    _this.onLoadActionError(_this.code, jqXHR, response, {
                        action: _this.action,
                        get_params: get_params,
                        post_params: post_params
                    });
                },
                error_function: function(jqXHR, textStatus, errorThrown, response) {
                    _this.onErrorRemoveOpenedItem();
                    _this.onLoadActionError(_this.code, jqXHR, response, {
                        action: _this.action,
                        get_params: get_params,
                        post_params: post_params
                    });
                }
            };

            if (typeof this.item.additional_params.failure_function === 'function')
            {
                ajax_params.failure_function = function() {
                    _this.onErrorRemoveOpenedItem();
                    _this.$nextTick(function() {
                        _this.item.additional_params.failure_function();
                    });
                };
            }

            sima.ajax.get(action[0], ajax_params);
        },
        onLoadActionError: function(item_code, jqXHR, response, error_params)
        {
            if (typeof response === 'undefined')
            {
                response = {};
            }

            if (
                    this.isFavoriteItem(item_code) && 
                    (
                        (!sima.isEmpty(response.status_code) && response.status_code === 404) || jqXHR.status === 404
                    )
                )
            {
                error_params.message = sima.translate('MainTabActionNotFoundError');
                if (sima.isEmpty(response.error_id))
                {
                    sima.errorReport.addError(error_params, false);
                }
                else
                {
                    sima.errorReport.editError(response.error_id, error_params, false);
                }
                sima.dialog.openWarn(sima.translate('MainTabActionNotFound'));
            }
            else
            {
                if (sima.isEmpty(response.error_id))
                {
                    error_params.message = !sima.isEmpty(response.message) ? response.message : jqXHR.responseText;
                    sima.errorReport.addError(error_params);
                }
                else
                {
                    sima.errorReport.editError(response.error_id);
                }
            }
        },
        isFavoriteItem: function(item_code) {
            return this.main_bus.isItemFavorite(item_code);
        },
        //ako se desi greska prilikom otvaranja taba i on se otvorio u otvorenim tabovima, onda ga uklanjamo iz otvorene tabove da ne smeta bezveze jer nije funkcionalan
        onErrorRemoveOpenedItem: function() {
            if (this.item.path === 'opened_items')
            {
                this.main_bus.$emit('removeOpenedItem', this.item);
            }
        }
    }
});