<script type="text/x-template" id="sima_layout_template">
    <div id="sima-layout">
        <sima-layout-top-bar
            v-bind:main_bus="main_bus"
        ></sima-layout-top-bar>
        <div id="sima-layout-content">
            <sima-layout-left-bar
                ref="left_bar"
                v-bind:main_bus="main_bus"
                v-on:collapsed="leftBarCollapsed"
            ></sima-layout-left-bar>
            <div class="sima-layout-display-panels" ref="display_panels">
                <sima-layout-display-panel
                    v-for="(loaded_item, code) in main_bus.loaded_items" 
                    v-bind:key="code" 
                    v-show="code === main_bus.selected_item.code"
                    v-bind:main_bus="main_bus"
                    v-bind:item="loaded_item"
                ></sima-layout-display-panel>
            </div>
        </div>
        <div id="sima-popup-overlay" style="display: none;"></div>
    </div>
</script>