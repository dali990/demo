
/* global Vue, sima */

Vue.component('sima-layout-top-bar', {
    template: '#sima_layout_top_bar_template',
    mixins: [sima.vue.getBaseComponent()],
    props: {
        main_bus: Object
    },
    computed: {
        curr_user: function() {
            return sima.vue.shot.getters.model('User_' + sima.getCurrentUserId());
        },
        curr_company: function() {
            return sima.vue.shot.getters.model('Company_' + sima.getCurrentCompanyId());
        },
        data_present: function(){
            this.curr_user.foreign_user_id;

            if (this.curr_user.shotStatus('foreign_user_id') !== 'INIT')
            {
                return true;
            }
            
            return false;
        },
        is_direct_profile_click: function() {
            return this.profile_selector === [];
        }
    },
    data: function () {
        return {
            mouse_is_over_profile: false,
            profile_selector: []
        };
    },
    watch: {
        
    },
    mounted: function() {
        var _this = this;
        
        this.main_bus.$on('selectHomePage', function() {
            _this.main_bus.selectItem(_this.$refs.home_item);
        });
        
        this.main_bus.selectItem(this.$refs.home_item);
        
        this.loadSIMADesktopAppWidget();
    },
    updated: function() {
        
    },
    methods: {
        loadSIMADesktopAppWidget: function() {
            var _this = this;

            sima.ajax.get('base/mainMenu/getSIMADesktopAppWidget', {
                async: true,
                success_function: function(response) {
                    if (typeof _this.$refs.sima_desktop_app !== 'undefined')
                    {
                        $(_this.$refs.sima_desktop_app).html(response.sda_html);
                    }
                }
            });
        },
        openProfile: function(event) {
            this.profile_selector = [];
        },
        openProfileUserConfig: function(event) {
            this.profile_selector = [{"simaTabs": 'configurations'}];
            //automatski ce se propagirati klik parent itema(u ovom slucaju to je profil), tako ne moramo to rucno raditi
        },
        openProfileTasks: function(event) {
            this.profile_selector = [{"simaTabs": 'tasks'}];
        },
        openProfileActivities: function(event) {
            this.profile_selector = [{"simaTabs": 'activities'}];
        },
        addClientBafRequest: function() {
            sima.errorReport.addClientBafRequest(this.curr_user.id, this.curr_user.foreign_user_id);
        },
        openUserMessages: function(event) {
            event.stopPropagation();
            sima.messages.messages_click();
        }
    }
});