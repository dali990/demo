<script type="text/x-template" id="sima_layout_top_bar_item_template">
    <div
        class="sima-layout-top-bar-item"
        v-bind:class="{'_selected' : selected}" 
        v-on:click="click"
    >
        <slot></slot>
    </div>
</script>