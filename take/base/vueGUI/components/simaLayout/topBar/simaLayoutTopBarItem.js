
/* global Vue, sima */

Vue.component('sima-layout-top-bar-item', {
    template: '#sima_layout_top_bar_item_template',
    mixins: [sima.vue.getBaseComponent()],
    props: {
        main_bus: Object,
        action: [String, Array, Object],
        component_name: String,
        external_selector: [Array, Object],
        additional_params: {
            type: Object,
            default: function() {
                return {};
            }
        },
        //treba nam jer u profilu imamo podopcije, a one nisu zasebne item komponente pa da znamo kad smo kliknuli direktno na profil
        //ako se klikne direktno na profil(ili bilo koji drugi topbar item) onda ne treba da se izvrsava selector
        is_direct_item_click: {
            type: Boolean,
            default: true
        }
    },
    data: function () {
        return {
            selected: false,
            local_selector: this.external_selector
        };
    },
    computed: {
        code: function() {
            return this.main_bus.getItemCode(this);
        }
    },
    watch: {
        external_selector: function(new_val) {
            this.local_selector = new_val;
        }
    },
    mounted: function() {
        var _this = this;

        this.main_bus.addToTopItemsList(this.code);
        
        this.main_bus.$on('selectTopBarItem', function(code, selector) {
            if (_this.code === code)
            {
                _this.local_selector = selector;
                _this.select();
            }
        });
    },
    updated: function() {
        
    },
    methods: {
        click: function(event) {
            this.select(event, this.is_direct_item_click ? true : false);
        },
        select: function(event = null, is_click_on_item = false) {
            var _this = this;
            
            if (!this.selected)
            {
                //mora nextTick jer treba da se registruje menjanje external_selector
                this.$nextTick(function() {
                    _this.main_bus.selectItem(_this);
                });
            }
            
            this.$nextTick(function() {
                if (is_click_on_item === false && !sima.isEmpty(_this.getSelector()))
                {
                    _this.main_bus.$emit('applySelectorForCode', _this.code, _this.getSelector());
                }
            });
        },
        unselect: function() {
            if (this.selected)
            {
                this.selected = false;
            }
        },
        //SasaA. mora kao metoda da bi bio univerzalan poziv i sa stavkom iz levog menija posto u stavci levog menija selector ne moze da bude computed(objasnjeno je tamo)
        getSelector: function() {
            return this.local_selector;
        }
    }
});