<?php 
    $use_vue_popup_messages = Yii::app()->configManager->get('base.develop.use_comments_vue');
?>
<script type="text/x-template" id="sima_layout_top_bar_template">
    <div id="sima-layout-top-bar">
        <div class="first-col _not-hoverable">
            <i class="sima-icon sis-sima-logo"></i>
            <span class="horizontal-separator"></span>
        </div><!-- end first-col -->

        <span class="vertical-separator"></span>

        <div class="second-col _not-hoverable">
            <div class="left-side">
                <span class="top-bar-company-name">{{ curr_company.DisplayName }}</span>
                <span class="vertical-separator"></span>
                <sima-layout-top-bar-item
                    ref="home_item"
                    class="icon-wrap sima-icon sil-home-alt"
                    v-bind:main_bus="main_bus"
                    action="site/homePage"
                ></sima-layout-top-bar-item>
                <?php if (!$use_vue_popup_messages) { ?>
                    <div class="top-bar-messages" v-on:click="openUserMessages">
                        <div class="notifs_messages_wrapper icon-wrap">
                            <i class="messages sima-icon sil-comments-alt"></i>
                            <span class="messages_cloud">0</span>
                        </div>
                    </div>
                <?php } else { ?>
                    <sima-popup-messages class="icon-wrap"></sima-popup-messages>
                <?php } ?>
                <sima-notifications class="icon-wrap"></sima-notifications>

                <sima-warnings class="icon-wrap"></sima-warnings>
                    
                <sima-layout-top-bar-item
                    class="icon-wrap sima-icon sil-envelope"
                    v-bind:main_bus="main_bus"
                    action="messages/email"
                    title="<?=Yii::t('BaseModule.MainMenu', 'Email')?>"
                >
                </sima-layout-top-bar-item>
                    
                <?php if (Yii::app()->eventRelay->without_sda === false) { ?>
                    <div class="top-bar-sda icon-wrap " ref="sima_desktop_app"></div>
                <?php } ?>
                <?php if (Yii::app()->user->checkAccess('menuShowAddressBook')) { ?>
                    <sima-layout-top-bar-item
                        class="icon-wrap sima-icon sil-address-book"
                        v-bind:main_bus="main_bus"
                        action="simaAddressbook/index"
                        title="<?=Yii::t('BaseModule.MainMenu', 'AddressBook')?>"
                    >
                    </sima-layout-top-bar-item>
                <?php } ?>
                <sima-layout-top-bar-item
                    v-if="curr_user.person.is_director"
                    class="icon-wrap sima-icon sil-users"
                    v-bind:main_bus="main_bus"
                    action="HR/headOf"
                    title="<?=Yii::t('BaseModule.MainMenu', 'MyTeam')?>"
                >
                </sima-layout-top-bar-item>
            </div><!-- end left-side -->

            <div class="right-side">
                <i 
                    class="icon-wrap sima-icon sil-bug"
                    v-bind:class="{_disabled: !data_present}"
                    v-on:click='addClientBafRequest'
                    title="<?=Yii::t('BaseModule.MainMenu', 'AddNewClientBafRequest');?>"
                ></i>  
                <?php if(Yii::app()->docWiki->isInstalled()) { ?>
                    <i 
                        class="icon-wrap sima-icon sil-book"
                        title="<?=Yii::t('BaseModule.MainMenu', 'SIMADocumentation')?>"
                        onclick='sima.icons.docWiki($(this), "<?php Yii::app()->params['doc_wiki_main_page'] ?>" );'
                    ></i>
                <?php } ?>
                <?php if(Yii::app()->personalWiki->isInstalled()) { ?>
                    <i 
                        class="icon-wrap sima-icon sib-wikipedia-w"
                        onclick='sima.icons.personalWiki($(this), "<?php echo Yii::app()->personalWiki->main_page ?>");'
                        title="<?=Yii::t('BaseModule.MainMenu', 'PersonalDocumentation')?>"
                    ></i>
                <?php } ?>

                <span class="vertical-separator"></span>

                <sima-layout-top-bar-item
                    class="top-bar-profile"
                    v-bind:main_bus="main_bus"
                    v-bind:action="['addressbook/partner', {id: curr_user.id}]"
                    v-bind:external_selector="profile_selector"
                    v-bind:is_direct_item_click="is_direct_profile_click"
                    v-on:mousedown.native="openProfile"
                    ref="profile"
                    v-on:mouseover.native="mouse_is_over_profile = true" v-on:mouseleave.native="mouse_is_over_profile = false"
                >
                    <sima-model-display-icon 
                        class="top-bar-profile-icon" 
                        v-bind:model="curr_user"
                        v-bind:allow_click_on_icon="false"
                    ></sima-model-display-icon>
                    <span 
                        class="top-bar-profile-name"
                    >
                        {{ curr_user.DisplayName }}
                    </span>
                    <div class="top-bar-profile-subitems" v-show="mouse_is_over_profile">
                        <div class="top-bar-profile-subitem" v-on:click="openProfileUserConfig">
                            <?=Yii::t('BaseModule.MainMenu', 'ProfileUserConfiguration')?>
                        </div>
                        <div class="top-bar-profile-subitem" v-on:click="openProfileTasks">
                            <?=Yii::t('BaseModule.MainMenu', 'ProfileTasks')?>
                        </div>
                        <div class="top-bar-profile-subitem" v-on:click="openProfileActivities">
                            <?=Yii::t('BaseModule.MainMenu', 'ProfileActivities')?>
                        </div>
                    </div>
                </sima-layout-top-bar-item>

                <span class="vertical-separator"></span>
                
                <i 
                    class="icon-wrap sima-icon sil-sign-out-alt"
                    onclick="sima.logout();"
                    title="<?=Yii::t('BaseModule.MainMenu', 'Logout')?>"
                ></i>
            </div><!-- end right-side -->
        </div><!-- end second-col -->
    </div><!-- end sima-layout-top-bar -->
</script>