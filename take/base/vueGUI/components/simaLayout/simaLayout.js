
/* global Vue, sima */

Vue.component('sima-layout', {
    template: '#sima_layout_template',
    mixins: [sima.vue.getBaseComponent()],
    props: {},
    computed: {},
    data: function () {
        return {
            main_bus: new Vue({
                data: {
                    loaded_items: {},
                    selected_item: null,
                    dragged_item: null,
                    action_labels: {}, //posto se kod akcija osim html-a vraca i title(labela stavke), moze da se desi da vec ucitana akcija ponovo otvori negde u meniju(posto moze jedna stavka na vise mesta u meniju da se pojavi)
                                      //i da nema labelu definisanu, pa da se ne bi opet pozivala akcija samo za labelu, ovako se labela za tu akciju sacuva prilikom prvog dovlacenja html-a
                                      //i sve ostale stavke u meniju za tu akciju koriste tu labelu
                    //sledeca promenljiva sluzi za reset nizova stavki i grupa ako dodje do greske. Ne moze preko vueshot dirty jer se samo promeni redosled
                    //elemenata u nizu ali su oni i dalje isti pa se ne izvrsi update. Ovako ako se resetuju nizovi izvrsi se update pa se odmah vrate na prethodno stanje
                    local_sorted_main_menu_favorite_groups: null,
                    all_favorite_item_codes: [], //niz kodova koji su omiljeni. Treba nam da znamo da li je stavka koja je van omiljenog segmenta omiljena
                    top_items_list: []
                },
                computed: {
                    curr_user: function() {
                        return sima.vue.shot.getters.model('User_' + sima.getCurrentUserId());
                    },
                    sorted_main_menu_favorite_groups: function() {
                        if (this.local_sorted_main_menu_favorite_groups !== null)
                        {
                            return this.local_sorted_main_menu_favorite_groups;
                        }
                        return this.curr_user.manualUpdate('sorted_main_menu_favorite_groups');
                    },
                    default_main_menu_favorite_group: function() {
                        return this.curr_user.manualUpdate('default_main_menu_favorite_group');
                    },
                    favorite_items_ready: function() {
                        return this.curr_user.shotStatus('sorted_main_menu_favorite_groups') === 'OK' && 
                               this.curr_user.shotStatus('default_main_menu_favorite_group') === 'OK';
                    },
                    opened_items: function() {
                        if (typeof this.default_main_menu_favorite_group.id !== 'undefined')
                        {
                            var default_group = this.favorite_items['group_' + this.default_main_menu_favorite_group.id];
                            if (!sima.isEmpty(default_group.subitems))
                            {
                                return default_group.subitems;
                            }
                        }

                        return {};
                    },
                    favorite_items: function() {
                        var _this = this;
                        var favorite_groups = {};
                        this.all_favorite_item_codes = [];

                        var saved_favorite_groups = this.sorted_main_menu_favorite_groups;
                        if (!sima.isEmpty(saved_favorite_groups))
                        {
                            $.each(saved_favorite_groups, function(index, saved_favorite_group) {
                                var favorite_group = {
                                    label: saved_favorite_group.group_name,
                                    favorite_group_id: saved_favorite_group.id
                                };

                                var favorite_group_subitems = {};
                                var saved_favorite_group_to_items = saved_favorite_group.sorted_main_menu_favorite_group_to_items;
                                if (!sima.isEmpty(saved_favorite_group_to_items))
                                {
                                    $.each(saved_favorite_group_to_items, function(index, saved_favorite_group_to_item) {
                                        var favorite_subitem = {
                                            label: saved_favorite_group_to_item.item.label,
                                            favorite_item_id: saved_favorite_group_to_item.item.id,
                                            favorite_group_id: saved_favorite_group.id,
                                            favorite_item_to_group_id: saved_favorite_group_to_item.id
                                        };
                                        if(!sima.isEmpty(saved_favorite_group_to_item.item.component_name))
                                        {
                                            favorite_subitem.component_name = saved_favorite_group_to_item.item.component_name;
                                        }
                                        else if (!sima.isEmpty(saved_favorite_group_to_item.item.action))
                                        {
                                            favorite_subitem.action = JSON.parse(saved_favorite_group_to_item.item.action);
                                        }
                                        var favorite_subitem_code = _this.getItemCode(favorite_subitem);
                                        if (!sima.isEmpty(favorite_subitem_code))
                                        {
                                            favorite_group_subitems[favorite_subitem_code] = favorite_subitem;
                                            if ($.inArray(favorite_subitem_code, _this.all_favorite_item_codes) === -1)
                                            {
                                                _this.all_favorite_item_codes.push(favorite_subitem_code);
                                            }
                                        }
                                    });
                                }

                                if (!sima.isEmpty(favorite_group_subitems))
                                {
                                    favorite_group.subitems = favorite_group_subitems;
                                }

                                favorite_groups['group_' + saved_favorite_group.id] = favorite_group;
                            });
                        }

                        return favorite_groups;
                    }
                },
                methods: {
                    updateFavoriteItems: function() {
                        this.curr_user.setDirty('sorted_main_menu_favorite_groups');
                        this.curr_user.setDirty('default_main_menu_favorite_group');
                    },
                    loadItem: function(item) {
                        var code = this.getItemCode(item);

                        if (code !== '' && !this.isItemLoaded(code))
                        {
                            Vue.set(this.loaded_items, code, item);
                        }
                    },
                    removeLoadedItem: function(item) {
                        var code = this.getItemCode(item);

                        if (code !== '' && this.isItemLoaded(code))
                        {
                            Vue.delete(this.loaded_items, code, item);
                        }
                    },
                    isItemLoaded: function(item_code) {
                        return typeof this.loaded_items[item_code] !== 'undefined';
                    },
                    //item moze biti i vue objekat i js objekat
                    getItemCode: function(item) {
                        var code = '';
                        if (!sima.isEmpty(item.component_name))
                        {
                            code = this.getComponentCode(item.component_name);
                        }
                        else if (!sima.isEmpty(item.action))
                        {
                            code = this.getActionCode(item.action);
                        }
                        
                        return code;
                    },
                    getComponentCode: function(component_name) {
                        var component_code = component_name;
                        
                        return component_code;
                    },
                    getActionCode: function(action) {
                        //ako je default layout action
                        if (
                                $.isArray(action) && action[0] === 'defaultLayout' && typeof action[1] !== 'undefined' &&
                                typeof action[1].model_name !== 'undefined' && typeof action[1].model_id !== 'undefined'
                           )
                        {
                            return action[1].model_name + action[1].model_id;
                        }
                        
                        return JSON.stringify(this.parseAction(action));
                    },
                    //obradjuje akciju tako da bude univerzalni zapis te akcije, posto moze doci u razlicitim zapisima
                    parseAction: function(action) {
                        if (typeof action === 'object')
                        {
                            var parsed_action = $.extend(true, [], action);
                            if (typeof parsed_action[1] !== 'undefined' && parsed_action[1] === null)
                            {
                                //SasaA - delete ne radi lepo jer i dalje ostanu 2 elementa u nizu a drugi je undefined(prazan). Posle nekog vremena se to isprazni,
                                //ali ne odmah
//                                delete parsed_action[1];
                                return [parsed_action[0]];
                            }
                            //action[1] su get_params koji se salje ajax-u i moze biti string/broj pa u tom slucaju se postavlja taj string za id u get-u
                            else if (typeof parsed_action[1] === 'string' || typeof parsed_action[1] === 'number')
                            {
                                parsed_action[1] = {id: parsed_action[1]};
                            }

                            return parsed_action;
                        }
                        else if (typeof action === 'string')
                        {
                            return [action];
                        }
                        
                        return action;
                    },
                    selectItem: function(item, event = null) {
                        if (this.selected_item !== null)
                        {
                            this.selected_item.unselect();
                        }
                        this.selected_item = item;
                        
                        item.selected = true;
                        item.$emit('itemSelected');
                        
                        this.loadItem(item);
                    },
                    isItemFavorite: function(item_code) {
                        return $.inArray(item_code, this.all_favorite_item_codes) !== -1;
                    },
                    updateFavoriteItemsOnError: function() {
                        var _this = this;

                        this.local_sorted_main_menu_favorite_groups = [];
                        this.$nextTick(function() {
                            _this.local_sorted_main_menu_favorite_groups = null;
                        });
                    },
                    addToTopItemsList: function(item_code) {
                        this.top_items_list.push(item_code);
                    }
                }
            }),
            layout_allign_timeout: null,
            left_bar_width: 40
        };
    },
    watch: {
        
    },
    mounted: function() {
        var _this = this;

        this.addLayoutAllignEventToDisplayPanels();
        
        this.allignLayout();
        
        $(sima.eventrelay).on('MAIN_MENU_ITEMS_CHANGED', function(event, event_message){
            _this.main_bus.updateFavoriteItems();
        });
        
        $(sima.eventrelay).on('MAIN_MENU_ITEMS_CHANGED_ERROR', function(event, event_message){
            _this.main_bus.updateFavoriteItemsOnError();
        });
        
        $(window).on('resize', function() {
            _this.allignLayout();
        });
    },
    updated: function() {
        this.allignLayout();
    },
    methods: {
        //poziva se spolja i sluzi da otvori tab u meniju. Prima podatke za tab kao objekat item_params
        openItem: function(item_params, select_item = true) {
            var _this = this;

            var new_item_params = $.extend(true, {}, item_params); //posto se openItem zove spolja, pa da se item_params ne promeni jer se salje po referenci
            
            var item_code = this.main_bus.getItemCode(new_item_params);
            if ($.inArray(item_code, this.main_bus.top_items_list) !== -1)
            {
                this.main_bus.$emit('selectTopBarItem', item_code, new_item_params.selector);
            }
            else
            {
                this.$refs.left_bar.addItemToOpened(new_item_params);
                if (select_item)
                {
                    this.$nextTick(function() {
                        _this.main_bus.$emit('selectItemByPath', 'opened_items.' + item_code);
                    });
                }
            }
        },
        leftBarCollapsed: function(collapsed) {
            this.left_bar_width = collapsed ? 40 : 250;
            this.allignLayout();
        },
        addLayoutAllignEventToDisplayPanels: function() {
            var _this = this;

            $(_this.$el).on('sima-layout-allign', '.sima-layout-display-panels', function (event, height, width, source) {
                if (event.target === _this.$refs.display_panels)
                {
                    if (sima.layout.log())
                    {
                        console.log(source);
                        console.log('HANDLER_sima_layout_display_panels_resize');
                    }

                    var display_panels = $(this);
                    var display_panels_height = window.innerHeight - 40 - 10; //40px top bar, 10px display-panels padding
                    var display_panels_width = window.innerWidth - _this.left_bar_width - 10; //10px display-panels padding

                    var selected_tab_content = display_panels.children('.sima-layout-display-panel:visible');
                    sima.layout.setLayoutSize(
                            selected_tab_content,
                            display_panels_height,
                            display_panels_width,
                            'TRIGGER_sima_layout_display_panels_resize'
                    );

                    event.stopPropagation();
                }
            });
        },
        allignLayout: function() {
            var _this = this;
            
            if (_this.layout_allign_timeout !== null)
            {
                clearTimeout(_this.layout_allign_timeout);
            }
            
            _this.layout_allign_timeout = setTimeout(function() {
                sima.layout.allignObject($(_this.$refs.display_panels));
            }, 100);
        }
    }
});