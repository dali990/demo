<script type="text/x-template" id="sima_layout_left_bar_template">
    <div 
        id="sima-layout-left-bar"
    >
        <vue-perfect-scrollbar 
            class="perfect-scrollbar menu-bar"
            v-bind:settings="{
                minScrollbarLength: 30
            }"
        >
            <div
                v-for="(item_params, item_index) in main_menu_items" 
                v-bind:key="item_index"
                v-if="!isEmptyItem(item_params) && (typeof item_params.visible === 'undefined' || item_params.visible)"
                class="main-bar-item"
                v-bind:class="{
                    _selected: item_index === selected_item_index, 
                    _active: item_index === active_item_index && item_index !== selected_item_index && item_index !== hovered_item_index
                }"
                v-bind:title="item_params.label"
                v-on:click="onItemClick($event, item_index)"
                v-on:mouseover="onItemOver($event, item_index)" 
                v-on:mouseleave="onItemLeave($event, item_index)" 
            >
                <i 
                    v-if="typeof item_params.icon !== 'undefined'"
                    class="main-bar-item-icon"
                    v-bind:class="item_params.icon"
                ></i>
                <span v-else>
                    {{ labelItemFirstLetter(item_params.label) }}
                </span>
            </div>
        </vue-perfect-scrollbar>
        <transition-group 
            name="fade"
            tag="div"
            class="menu-bar-content" 
            v-bind:class="{_absolute: hovered_item_index !== null && selected_item_index === null}"
            v-show="selected_item_index !== null || hovered_item_index !== null"
        >
            <div
                v-for="(item_params, item_index) in main_menu_items"
                v-bind:key="item_index"
                v-show="hovered_item_index === item_index || (selected_item_index === item_index && hovered_item_index === null)"
                class="menu-bar-content-tab"
                v-on:mouseover="onItemOver($event, item_index)" 
                v-on:mouseleave="onItemLeave($event, item_index)" 
            >
                <div class="menu-bar-content-head sima-text-dots" v-bind:title="item_params.label">
                    {{ item_params.label }}
                    <i 
                        v-if="item_index === 'favorite_items'"
                        class="fas fa-plus add-favorite-group" 
                        v-on:click="addFavoriteItemGroup"
                        title="<?=Yii::t('BaseModule.MainMenu', 'AddFavoriteItemGroup')?>"
                    ></i>
                </div>
                <vue-perfect-scrollbar 
                    class="perfect-scrollbar menu-bar-content-body"
                    v-bind:settings="{
                        minScrollbarLength: 30
                    }"
                >
                    <sima-layout-left-bar-subitems
                        v-bind:subitems="main_menu_items[item_index].subitems"
                        v-bind:main_bus="main_bus"
                        v-bind:level="1"
                        v-bind:path="item_index"
                        v-on:itemSelected="subItemSelected($event, item_index)" 
                        v-on:itemUnselected="subItemUnselected($event, item_index)"
                    >
                    </sima-layout-left-bar-subitems>
                </vue-perfect-scrollbar>
            </div>
        </transition-group>
    </div>
</script>