
/* global Vue, sima */

Vue.component('sima-layout-left-bar', {
    template: '#sima_layout_left_bar_template',
    mixins: [sima.vue.getBaseComponent()],
    props: {
        main_bus: Object
    },
    computed: {
        curr_user: function() {
            return sima.vue.shot.getters.model('User_' + sima.getCurrentUserId());
        },
        opened_items_group_name: function() {
            return this.main_bus.default_main_menu_favorite_group.group_name;
        },
        opened_items: function() {
            return this.main_bus.opened_items;
        },
        favorite_items: function() {
            return this.main_bus.favorite_items;
        }
    },
    data: function () {
        return {
            main_menu_items: {
                opened_items: {
                    label: sima.translate('MainLayoutOpenedItems'),
                    icon: 'sima-icon sil-window',
                    subitems: {}
                },
                favorite_items: {
                    label: sima.translate('MainLayoutFavoriteItems'),
                    icon: 'sima-icon sil-window-restore',
                    subitems: {}
                }
            },
            selected_item_index: null,
            hovered_item_index: null,
            active_item_index: null,
            item_over_leave_timeout: null,
            save_opened_item_timeout: null,
            is_loaded_mainmenu_last_opened_module_tab: false,
            loaded_main_menu_items: null
        };
    },
    watch: {
        opened_items_group_name: function(new_val, old_val) {
            if (new_val !== old_val)
            {
                this.main_menu_items.opened_items.label = new_val;
            }
        },
        opened_items: function(new_val, old_val) {
            var _this = this;

            if (JSON.stringify(new_val) !== JSON.stringify(old_val))
            {
                var old_subitems = this.main_menu_items.opened_items.subitems;
                //niz u koji se pamte stavke koje nisu sacuvane(nalaze se u starom nizu, a nema ih u novom). One se u nizu vezuju za prvu sacuvanu stavku koja je ispred njih
                //znaci niz je oblika key: value, gde je key code prve sacuvane stavke koja je ispred nesacuvanih, a value je niz neprekidno nesacuvanih nakon sacuvane
                var unsaved_items = {};
                //poslednja sacuvana stavka. Za nju se vezuju neprekidno nesacuvane stavke koje idu odmah posle nje. Ako je -1, onda nesacuvane stavke se postavljaju na pocetak
                var last_saved_item = -1;
                $.each(old_subitems, function(index, value) {
                    if (typeof new_val[index] === 'undefined')
                    {
                        if (typeof unsaved_items[last_saved_item] === 'undefined')
                        {
                            unsaved_items[last_saved_item] = [index];
                        }
                        else
                        {
                            unsaved_items[last_saved_item].push(index);
                        }
                        //posto se stavka vise ne nalazi u novom nizu, ali se i dalje nalazi u starom, onda je ponistavamo iz omiljenih
                        Vue.set(_this.main_menu_items.opened_items.subitems[index], 'favorite_item_id', null);
                        Vue.set(_this.main_menu_items.opened_items.subitems[index], 'favorite_group_id', null);
                        Vue.set(_this.main_menu_items.opened_items.subitems[index], 'favorite_item_to_group_id', null);
                    }
                    else
                    {
                        last_saved_item = index;
                    }
                });

                var new_subitems = {};
                if (typeof unsaved_items[-1] !== 'undefined')
                {
                    $.each(unsaved_items[-1], function(index, value) {
                        new_subitems[value] = old_subitems[value];
                    });
                }

                $.each(new_val, function(index, value) {
                    new_subitems[index] = value;
                    if (typeof unsaved_items[index] !== 'undefined')
                    {
                        $.each(unsaved_items[index], function(index1, value1) {
                            new_subitems[value1] = old_subitems[value1];
                        });
                    }
                });

                this.main_menu_items.opened_items.subitems = new_subitems;
            }
        },
        favorite_items: function(new_val, old_val) {
            if (JSON.stringify(new_val) !== JSON.stringify(old_val))
            {
                this.main_menu_items.favorite_items.subitems = new_val;
            }
        },
        selected_item_index: function(new_val, old_val) {
            if (new_val !== old_val)
            {
                this.$emit('collapsed', new_val !== null ? false : true);
            }
        }
    },
    mounted: function() {
        var _this = this;

        this.main_bus.$on('addItemToOpened', function(item_params) {
            _this.addItemToOpened(item_params);
        });
        
        this.main_bus.$on('removeOpenedItem', function(item) {
            _this.removeOpenedItem(item);
        });
        
        this.main_bus.$on('emptyOpenedItems', function() {
            _this.emptyOpenedItems();
        });
        
        this.main_bus.$on('setOpenedItems', function(opened_items) {
            _this.main_menu_items.opened_items.subitems = opened_items;
        });
        
        this.loadMainMenuItems();
        
        sima.config.get('base.mainmenu_last_opened_module_tab', sima.getCurrentUserId(), function(value, config) {
            var new_value = value.replace(/\"/g, "");
            if (!sima.isEmpty(new_value))
            {
                _this.selected_item_index = new_value;
            }
            _this.is_loaded_mainmenu_last_opened_module_tab = true;
            //moram da vrsim proveru da li su ucitani tabovi sa servera i ako jesu onda da ih postavim. To je zbog toga sto postoji problem da se oba ajaxa
            //izvrse priblizno u isto vreme, dok vue radi update for petlje ucitanih tabova, iskulira selected_item_index koji je u medjuvremenu stigao, jer se on
            //koristi unutar for petlje u template-u. Ovim smo se osigurali da su se obe(savucani tab i svi tabovi) stvari ucitale, pa tek onda postavljamo dovucene tabove
            if(_this.loaded_main_menu_items !== null)
            {
                _this.main_menu_items = $.extend(true, {}, _this.main_menu_items, _this.loaded_main_menu_items);
                _this.loaded_main_menu_items = null;
            }
        });
    },
    updated: function() {
        
    },
    methods: {
        loadMainMenuItems: function() {
            var _this = this;

            sima.ajax.get('base/mainMenu/getItems', {
                async: true,
                success_function: function(response) {
                    if (_this.is_loaded_mainmenu_last_opened_module_tab === true)
                    {
                        _this.main_menu_items = $.extend(true, {}, _this.main_menu_items, response.items);
                    }
                    else
                    {
                        _this.loaded_main_menu_items = response.items;
                    }
                }
            });
        },
        addItemToOpened: function(item_params) {
            var item_code = this.main_bus.getItemCode(item_params);
            if (typeof this.main_menu_items.opened_items.subitems[item_code] === 'undefined')
            {
                var opened_subitems = this.main_menu_items.opened_items.subitems;
                var new_opened_subitems = $.extend(true, {[item_code]: item_params}, opened_subitems);
                Vue.set(this.main_menu_items.opened_items, 'subitems', new_opened_subitems);
            }
            else
            {
                var new_item_params = $.extend(true, this.main_menu_items.opened_items.subitems[item_code], item_params);
                Vue.set(this.main_menu_items.opened_items.subitems, item_code, new_item_params);
            }
        },
        removeOpenedItem: function(item) {
            var item_code = item.code;
            if (typeof this.main_menu_items.opened_items.subitems[item_code] !== 'undefined')
            {
                Vue.delete(this.main_menu_items.opened_items.subitems, item_code);
                if(this.main_bus.selected_item === null || this.main_bus.selected_item.code !== item_code)
                {
                    this.main_bus.removeLoadedItem(item);
                }
            }
        },
        emptyOpenedItems: function() {
            Vue.set(this.main_menu_items.opened_items, 'subitems', {});
        },
        onItemClick: function(event, item_index) {
            if (this.selected_item_index === item_index)
            {
                this.selected_item_index = null;
            }
            else
            {
                this.selected_item_index = item_index;
            }
            
            this.saveOpenedItem(this.selected_item_index);
        },
        onItemOver: function(event, item_index) {
            var _this = this;

            if (this.item_over_leave_timeout !== null)
            {
                clearTimeout(this.item_over_leave_timeout);
            }
            
            this.item_over_leave_timeout = setTimeout(function() {
                _this.hovered_item_index = item_index;
            }, 100);
        },
        onItemLeave: function(event, item_index) {
            var _this = this;

            if (this.item_over_leave_timeout !== null)
            {
                clearTimeout(this.item_over_leave_timeout);
            }
            
            this.item_over_leave_timeout = setTimeout(function() {
                _this.hovered_item_index = null;
            }, 100);
        },
        labelItemFirstLetter: function(item_label) {
            if (sima.isEmpty(item_label))
            {
                return '';
            }
            
            return item_label.charAt(0).toUpperCase();
        },
        isEmptyItem: function(item_params) {
            return sima.isEmpty(item_params);
        },
        subItemSelected: function(event, item_index) {
            this.active_item_index = item_index;
        },
        subItemUnselected: function(event, item_index) {
            this.active_item_index = null;
        },
        addFavoriteItemGroup: function(event) {
            event.stopPropagation();

            sima.model.form('MainMenuFavoriteGroup', '', {
                init_data: {
                    MainMenuFavoriteGroup: {
                        user_id: {
                            disabled: true,
                            init: this.curr_user.id
                        }
                    }
                }
            });
        },
        saveOpenedItem: function(opened_item) {
            if (this.save_opened_item_timeout !== null)
            {
                clearTimeout(this.save_opened_item_timeout);
            }
            
            this.save_opened_item_timeout = setTimeout(function() {
                sima.config.set('base.mainmenu_last_opened_module_tab', sima.getCurrentUserId(), opened_item);
            }, 1000);
        }
    }
});