<script type="text/x-template" id="sima_layout_left_bar_subitems_template">
    <ul 
        class="sima-layout-left-bar-subitems"
        v-bind:class="subitems_class"
    >
        <draggable 
            class="sima-layout-left-bar-sortable-list"
            v-bind:sort="is_favorite_list_items || path === 'opened_items'"
            v-bind:group="{ name: 'favorite_items', put: draggablePutItemCheck, pull: draggablePullCheck }" 
            animation="50" ghost-class="draggable-ghost-class" filter="._not-draggable" dragClass="draggable-drag-class" forceFallback="true"
            v-on:start="onStart"
            v-on:end="onEnd"
            v-on:clone="onClone"
            delay="200"
        >
            <sima-layout-left-bar-item
                v-if="typeof item_params.favorite_group_id === 'undefined' || typeof item_params.favorite_item_id !== 'undefined'"
                v-for="(item_params, index) in subitems" 
                v-bind:key="index" 
                v-bind:item_params="item_params"
                v-bind:main_bus="main_bus"
                v-bind:level="level"
                v-bind:path="path"
                v-bind:subitems_index="index"
                v-on:itemSelected="itemSelected"
                v-on:itemUnselected="itemUnselected"
            >
            </sima-layout-left-bar-item>
        </draggable>
        <draggable 
            v-if="is_favorite_list_groups"
            class="sima-layout-left-bar-sortable-list"
            v-bind:sort="is_favorite_list_groups"
            v-bind:group="{ name: 'favorite_items', put: draggablePutGroupCheck, pull: draggablePullCheck }" 
            animation="50" ghost-class="draggable-ghost-class" filter="._not-draggable" dragClass="draggable-drag-class" forceFallback="true"
            v-on:start="onStart"
            v-on:end="onEnd"
            delay="200"
        >
            <sima-layout-left-bar-item
                v-if="typeof item_params.favorite_group_id !== 'undefined' && typeof item_params.favorite_item_id === 'undefined'"
                v-for="(item_params, index) in subitems" 
                v-bind:key="index" 
                v-bind:item_params="item_params"
                v-bind:main_bus="main_bus"
                v-bind:level="level"
                v-bind:path="path"
                v-bind:subitems_index="index"
                v-on:itemSelected="itemSelected"
                v-on:itemUnselected="itemUnselected"
            >
            </sima-layout-left-bar-item>
        </draggable>
    </ul>
</script>