
/* global Vue, sima */

Vue.component('sima-layout-left-bar-item', {
    template: '#sima_layout_left_bar_item_template',
    mixins: [sima.vue.getBaseComponent()],
    props: {
        item_params: [Array, Object],
        main_bus: Object,
        level: Number,
        path: [String, Number],
        subitems_index: [String, Number]
    },
    data: function () {
        return {
            expanded: false,
            selected: false,
            active: false, //item je aktivan ako je selektovan neki item unutar njega
            mouse_is_over_current_item: false //samo ako je mis iznad trenutne stavke(li elementa), ne i njegovog podmenija(ul lista)
        };
    },
    computed: {
        curr_user: function() {
            return sima.vue.shot.getters.model('User_' + sima.getCurrentUserId());
        },
        item_options: function(){
            var result = {};
            if(!sima.isEmpty(this.item_params.itemOptions))
            {
                result = this.item_params.itemOptions;
            }
            return result;
        },
        label: function() {
            return !sima.isEmpty(this.item_params.label) ? this.item_params.label : sima.translate('MainLayoutNewTab');
        },
        action: function() {
            if (sima.isEmpty(this.item_params.action))
            {
                return null;
            }

            return this.main_bus.parseAction(this.item_params.action);
        },
        component_name: function() {
            return !sima.isEmpty(this.item_params.component_name) ? this.item_params.component_name : null;
        },
        additional_params: function() {
            return !sima.isEmpty(this.item_params.additional_params) ? this.item_params.additional_params : {};
        },
        is_selectable: function() {
            return this.component_name !== null || this.action !== null;
        },
        code: function() {
            return this.main_bus.getItemCode(this);
        },
        calculated_path: function() {
            var code = this.code;
            if (sima.isEmpty(code))
            {
                code = this.subitems_index;
            }
            return !sima.isEmpty(this.path) ? (this.path + '.' + code) : code;
        },
        database_uniq: function() {
            var obj = {};
            if (this.component_name !== null)
            {
                obj.component_name = this.component_name;
            }
            else if (this.action !== null)
            {
                obj.action = JSON.stringify(this.action);
            }
            
            return obj;
        },
        has_subitems: function() {
            var has_subitems = false;
            
            if (!sima.isEmpty(this.item_params.subitems))
            {
                var subitems = this.item_params.subitems;
                $.each(subitems, function(index, subitem) {
                    if (!sima.isEmpty(subitem) && (typeof subitem.visible === 'undefined' || subitem.visible))
                    {
                        has_subitems = true;
                        return;
                    }
                });
            }
            
            return has_subitems;
        },
        show_subitems: function() {
            if (this.is_dragging_item)
            {
                return false;
            }
            
            return this.level === 1 && !this.is_inside_favorites ? true : this.expanded;
        },
        is_favorite: function() {
            if (this.favorite_item_id !== null)
            {
                return true;
            }

            return this.main_bus.isItemFavorite(this.code);
        },
        favorite_group: function() {
            if (!sima.isEmpty(this.item_params.favorite_group_id))
            {
                return sima.vue.shot.getters.model('MainMenuFavoriteGroup_' + this.item_params.favorite_group_id);
            }
            
            return null;
        },
        favorite_group_id: function() {
            return this.favorite_group !== null ? this.favorite_group.id : null;
        },
        favorite_item: function() {
            if (!sima.isEmpty(this.item_params.favorite_item_id))
            {
                return sima.vue.shot.getters.model('MainMenuFavoriteItem_' + this.item_params.favorite_item_id);
            }
            
            return null;
        },
        favorite_item_id: function() {
            return this.favorite_item !== null ? this.favorite_item.id : null;
        },
        favorite_item_to_group: function() {
            if (!sima.isEmpty(this.item_params.favorite_item_to_group_id))
            {
                return sima.vue.shot.getters.model('MainMenuFavoriteItemToGroup_' + this.item_params.favorite_item_to_group_id);
            }
            
            return null;
        },
        favorite_item_to_group_id: function() {
            return this.favorite_item_to_group !== null ? this.favorite_item_to_group.id : null;
        },
        is_favorite_group: function () {
            return !sima.isEmpty(this.favorite_group_id) && sima.isEmpty(this.favorite_item_id);
        },
        is_empty_item: function() {
            return sima.isEmpty(this.item_params);
        },
        is_dragging_item: function() {
            return this.main_bus.dragged_item !== null && this.main_bus.dragged_item._uid === this._uid;
        },
        is_item_loaded: function() {
            return this.main_bus.isItemLoaded(this.code);
        },
        is_inside_favorites: function() {
            return !sima.isEmpty(this.path) && this.path.indexOf("favorite_items") === 0;
        }
    },
    watch: {
        
    },
    mounted: function() {
        var _this = this;

        this.main_bus.$on('selectItemByPath', function(path) {
            if (_this.calculated_path === path)
            {
                _this.select();
            }
        });
        
        this.main_bus.$on('setLabelForCode', function(code, new_label) {
            if (_this.code === code && sima.isEmpty(_this.item_params.label))
            {
                Vue.set(_this.item_params, 'label', new_label);
            }
        });
    },
    updated: function() {
        
    },
    beforeDestroy: function() {
        var _this = this;

        this.$nextTick(function() {
            if (_this.selected)
            {
                _this.main_bus.$emit('selectHomePage');
            }
        });
    },
    destroyed: function() {
        //Sasa A. - iz nekog razloga kada se prevuce poslednja(ako ima vise onda je ok) stavka iz grupe van te grupe onda ona ostane duplirana. 
        //Ovo je resenje koje radi, ali nisam siguran zasto je neophodan ovaj kod
        $(this.$el).remove();
    },
    methods: {
        click: function(event) {
            event.stopPropagation();
            
            this.toggle(event);
            this.select(event, true);
        },
        toggle: function(event) {
            event.stopPropagation();

            if (this.expanded)
            {
                this.collapse(event);
            }
            else
            {
                this.expand(event);
            }
        },
        expand: function(event = null) {
            if (!this.expanded)
            {
                this.expanded = true;
            }
        },
        collapse: function(event = null) {
            if (this.expanded)
            {
                this.expanded = false;
            }
        },
        select: function(event = null, is_click_on_item = false) {
            var _this = this;

            if (this.is_selectable)
            {
                if (!this.selected)
                {
                    this.$nextTick(function() {
                        _this.main_bus.selectItem(_this, event);
                    });
                }

                this.$nextTick(function() {
                    //ako nije direktan klik na item onda se primenjuje selektor.
                    if (is_click_on_item === false && !sima.isEmpty(_this.getSelector()))
                    {
                        _this.main_bus.$emit('applySelectorForCode', _this.code, _this.getSelector());
                    }
                    if (sima.isEmpty(_this.item_params.label) && !sima.isEmpty(_this.main_bus.action_labels[_this.code]))
                    {
                        Vue.set(_this.item_params, 'label', _this.main_bus.action_labels[_this.code]);
                    }
                });
            }
            else if (!sima.isEmpty(this.item_params.js_function))
            {
                eval(this.item_params.js_function);
            }
        },
        unselect: function() {
            if (this.selected)
            {
                this.selected = false;
                this.$emit('itemUnselected');
            }
            this.active = false;
        },
        subItemSelected: function() {
            this.active = true;
            if (!this.expanded && this.calculated_path !== 'opened_items')
            {
                this.expanded = true;
            }
            this.$emit('itemSelected');
        },
        subItemUnselected: function() {
            this.active = false;
            this.$emit('itemUnselected');
        },
        removeOpenedItem: function(event) {
            event.stopPropagation();
            
            this.main_bus.$emit('removeOpenedItem', this);
        },
        reloadItemAction: function(event) {
            event.stopPropagation();
            
            this.main_bus.$emit('reloadItemAction', this.code);
        },
        //sledece 3 funkcije ne rade jer sam stavio forceFallback u dragable komponenti da bi radio skrol prilikom prevlacenja. Zbog toga ne radi default html5 prevlacenje
        //trenutno je bitnije skrolovanje prilikom prevlacenja jer spustanje stavke u grupu korisnik moze da odradi na drugi nacin. Kada se resi ovaj problem radice i spustenje
        //stavke direktno na grupu
        dragOverFavoriteGroup: function(event) {
            if (sima.isEmpty(this.main_bus.dragged_item))
            {
                return;
            }
            
            if (
                    !this.main_bus.dragged_item.is_favorite_group &&
                    (this.is_favorite_group || this.calculated_path === 'favorite_items')
                )
            {
                $(this.$el).addClass('draggable-over');
            }
        },
        dragLeaveFavoriteGroup: function(event) {
            if (sima.isEmpty(this.main_bus.dragged_item))
            {
                return;
            }
            
            if (
                    !this.main_bus.dragged_item.is_favorite_group &&
                    (this.is_favorite_group || this.calculated_path === 'favorite_items')
                )
            {
                $(this.$el).removeClass('draggable-over');
            }
        },
        dropOnFavoriteGroup: function(event) {
            var _this = this;
            
            var dragged_item = this.main_bus.dragged_item;
            
            if (sima.isEmpty(dragged_item))
            {
                return;
            }

            if (
                    !dragged_item.is_favorite_group &&
                    (this.is_favorite_group || this.calculated_path === 'favorite_items')
                )
            {
                this.main_bus.dragged_item = null;

                $(this.$el).removeClass('draggable-over');
                
                var favorite_item_id = dragged_item.favorite_item_id;
                if (sima.isEmpty(favorite_item_id))
                {
                    favorite_item_id = $.extend(true, {label: dragged_item.label}, dragged_item.database_uniq);
                }
                
                sima.ajax.get('base/mainMenu/moveFavoriteItemToGroup', {
                    get_params: {
                        favorite_item_id: favorite_item_id,
                        favorite_group_id: this.favorite_group_id,
                        old_favorite_item_to_group_id: dragged_item.favorite_item_to_group_id
                    },
                    async: true,
                    failure_function: function(response) {
                        sima.dialog.openWarn(response.message);
                    }
                });
            }
        },
        setFavoriteGroupToOpened: function(event) {
            event.stopPropagation();
            
            this.main_bus.$emit('emptyOpenedItems');
            
            sima.ajax.get('base/mainMenu/setFavoriteGroupToOpened', {
                get_params: {
                    favorite_group_id: this.favorite_group_id
                },
                async: true
            });
        }, 
//        SasaA. - ne moze selector kao computed jer nije obavezan i moguce da ga u startu nema pa vue ne stavi da je item_params.selector reaktivan
//        i kada se doda selector to se ne propagira reaktivno u computed. Umesto toga mora u kodu da se koristi item_params.selector, odnosno ovde je napravljena metoda
//        koja ce se samo pozivati u drugim metodama. To je tako napravljena zbog univerzalnosti koda jer imamo selektor i za stavku top menija pa da poziv bude isti.
        getSelector: function() {
            return this.item_params.selector;
        }
    }
});