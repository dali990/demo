<script type="text/x-template" id="sima_layout_left_bar_item_template">
    <li 
        class="sima-layout-left-bar-item"
        v-bind:class="{
            '_not-draggable': (!main_bus.favorite_items_ready || !(is_inside_favorites || path === 'opened_items')),
            '_favorite': favorite_item_id !== null || favorite_group_id !== null
        }"
        v-if="!is_empty_item && (typeof item_params.visible === 'undefined' || item_params.visible)"
        v-bind="item_options"
    >
        <div 
            v-if="is_inside_favorites || level !== 1 || !has_subitems"
            class="item-wrap" 
            v-bind:class="{
                '_selected' : selected, 
                '_active' : active && !expanded
            }"
            v-on:click="click"
            v-on:mouseover="mouse_is_over_current_item = true" 
            v-on:mouseleave="mouse_is_over_current_item = false"
            v-on:dragover="dragOverFavoriteGroup"
            v-on:dragleave="dragLeaveFavoriteGroup"
            v-on:drop="dropOnFavoriteGroup"
        >
            <span class="item-name sima-text-dots" v-bind:title="label">{{ label }}</span>
            <i class="sima-icon sis-check-circle" v-if="is_favorite_group && favorite_group.is_default === true && mouse_is_over_current_item === false"></i>
            <i 
                v-else-if="is_favorite_group && favorite_group.is_default === false"
                v-show="mouse_is_over_current_item && !is_dragging_item"
                class="sima-icon sis-thumbtack"
                v-on:click="setFavoriteGroupToOpened"
                title="<?=Yii::t('BaseModule.MainMenu', 'SetFavoriteGroupToOpened')?>"
            ></i>
            <sima-model-options
                class="favorite-group-options"
                v-if="is_favorite_group" 
                v-show="mouse_is_over_current_item && !is_dragging_item" 
                v-bind:model="favorite_group"
            ></sima-model-options>
            <i 
                v-if="action !== null && is_item_loaded"
                v-show="mouse_is_over_current_item"
                class="fas fa-redo-alt" 
                v-on:click="reloadItemAction"
                title="<?=Yii::t('BaseModule.MainMenu', 'ReloadItemAction')?>"
            ></i>
            <i 
                v-if="mouse_is_over_current_item && path === 'opened_items' && this.favorite_group_id !== main_bus.default_main_menu_favorite_group.id"
                class="fas fa-times" 
                v-on:click="removeOpenedItem"
                title="<?=Yii::t('BaseModule.MainMenu', 'RemoveOpenedItem')?>"
            ></i>
            <sima-layout-left-bar-item-favorite-icon v-bind:item="this"></sima-layout-left-bar-item-favorite-icon>
            <i 
                v-if="has_subitems" 
                v-on:click="toggle"
                class="expand-icon fas fa-angle-right" 
                v-bind:class="{_expanded: expanded}"
            ></i>
        </div>
        <div 
            v-else
            class="item-wrap-group" 
        >
            <span class="item-name sima-text-dots" v-bind:title="label">{{ label }}</span>
        </div>
        <transition name="fade">
            <sima-layout-left-bar-subitems
                v-if="has_subitems"
                v-show="show_subitems"
                v-bind:class="{_expanded: expanded}"
                v-bind:subitems="item_params.subitems"
                v-bind:main_bus="main_bus"
                v-bind:level="level + 1"
                v-bind:path="calculated_path"
                v-on:itemSelected="subItemSelected" 
                v-on:itemUnselected="subItemUnselected" 
            >
            </sima-layout-left-bar-subitems>
        </transition>
    </li>
</script>