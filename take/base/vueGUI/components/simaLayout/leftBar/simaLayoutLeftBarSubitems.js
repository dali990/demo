
/* global Vue, sima */

Vue.component('sima-layout-left-bar-subitems', {
    template: '#sima_layout_left_bar_subitems_template',
    mixins: [sima.vue.getBaseComponent()],
    props: {
        subitems: [Array, Object],
        main_bus: Object,
        level: Number,
        path: [String, Number]
    },
    computed: {
        curr_user: function() {
            return sima.vue.shot.getters.model('User_' + sima.getCurrentUserId());
        },
        level_class: function() {
            return '_level' + this.level;
        },
        subitems_class: function() {
            var subitems_class = {};

            subitems_class[this.level_class] = true;
            subitems_class['_dragging-active-inside'] = this.main_bus.dragged_item !== null;
            
            return subitems_class;
        },
        is_favorite_list_groups: function() {
            return !sima.isEmpty(this.path) && this.path === 'favorite_items';
        },
        is_favorite_list_items: function() {
            return !sima.isEmpty(this.path) && this.path.indexOf("favorite_items.") === 0;
        }
    },
    data: function () {
        return {
            
        };
    },
    watch: {
        
    },
    mounted: function() {
        
    },
    updated: function() {
        
    },
    methods: {
        itemSelected: function() {
            this.$emit('itemSelected');
        },
        itemUnselected: function() {
            this.$emit('itemUnselected');
        },
        onClone: function(event) {
            
        },
        onStart: function(event) {
            this.main_bus.dragged_item = event.item.__vue__;
        },
        onEnd: function(event) {
            var main_bus_dragged_item = this.main_bus.dragged_item;
            
            this.main_bus.dragged_item = null;

            if (
                    main_bus_dragged_item === null ||
                    sima.isEmpty(event.item.__vue__) ||
                    event.to === event.from && event.oldIndex === event.newIndex
                )
            {
                return false;
            }

            var dragged_item_vue = event.item.__vue__;

            var dragged_model_name = null;
            var dragged_model_id = null;
            if (dragged_item_vue.is_favorite_group) //ako se prevlaci grupa
            {
                dragged_model_name = 'MainMenuFavoriteGroup';
                dragged_model_id = dragged_item_vue.favorite_group_id;
            }
            else if (!sima.isEmpty(dragged_item_vue.favorite_item_to_group_id)) //ako je u pitanju stavka unutar grupe
            {
                dragged_model_name = 'MainMenuFavoriteItemToGroup';
                dragged_model_id = dragged_item_vue.favorite_item_to_group_id;
            }

            var target_prev_model_id = null;
            var target_next_model_id = null;
            
            var sortable_list = $(event.to);
            
            var target_item = sortable_list.children('.sima-layout-left-bar-item').eq(event.newIndex);
            var target_next_item = target_item.nextAll('._favorite:first');
            //ako postoji sledeca stavka, znaci nije stavka prebacena na poslednjem mestu
            if (!sima.isEmpty(target_next_item) && !sima.isEmpty(target_next_item[0].__vue__))
            {
                var target_next_item_vue = target_next_item[0].__vue__;
                if (dragged_model_name === 'MainMenuFavoriteGroup')
                {
                    target_next_model_id = target_next_item_vue.favorite_group_id;
                }
                else if (dragged_model_name === 'MainMenuFavoriteItemToGroup')
                {
                    target_next_model_id = target_next_item_vue.favorite_item_to_group_id;
                }
            }
            else
            {
                var target_prev_item = target_item.prevAll('._favorite:first');
                if (!sima.isEmpty(target_prev_item) && !sima.isEmpty(target_prev_item[0].__vue__))
                {
                    var target_prev_item_vue = target_prev_item[0].__vue__;
                    if (dragged_model_name === 'MainMenuFavoriteGroup')
                    {
                        target_prev_model_id = target_prev_item_vue.favorite_group_id;
                    }
                    else if (dragged_model_name === 'MainMenuFavoriteItemToGroup')
                    {
                        target_prev_model_id = target_prev_item_vue.favorite_item_to_group_id;
                    }
                }
            }

            //ako su otvorene stavke postavljamo novi redosled tabova nakon prevlacenja. To radimo samo za otovorene stavke jer tu imamo i stvari koje nisu sacuvani tabovi
            //pa moramo prepraviti tacan redosled i u vue nizu po uzoru na gui. To nam je bitno da bi kasnije kada vueshot posalje update mogli lepo da sihronizujemo nove stvari koje su dosle
            //za omiljene stavke koje nisu u otvorenim, nije bitan redosled jer ce se on ispraviti nakon sto vueshot posalje update
            if (dragged_item_vue.path === 'opened_items')
            {
                var new_opened_items = {};
                $.each(sortable_list.children(), function(index, value) {
                    if (!sima.isEmpty(value.__vue__))
                    {
                        var li_vue = value.__vue__;
                        new_opened_items[li_vue.code] = li_vue.item_params;
                    }
                });
                this.main_bus.$emit('setOpenedItems', new_opened_items);
            }

            if (dragged_model_name !== null && (target_prev_model_id !== null || target_next_model_id !== null))
            {
                sima.ajax.get('base/mainMenu/saveFavoriteItemNewOrder', {
                    get_params: {
                        dragged_model_name: dragged_model_name,
                        dragged_model_id: dragged_model_id,
                        target_prev_model_id: target_prev_model_id,
                        target_next_model_id: target_next_model_id
                    },
                    async: true,
                    failure_function: function(response) {
                        sima.dialog.openWarn(response.message);
                    }
                });
            }
        },
        draggablePullCheck: function(to, from, dragEl, evt) {
            if (this.is_favorite_list_groups || this.is_favorite_list_items || this.path === 'opened_items')
            {
                return true;
            }
            
            return false;
        },
        draggablePutGroupCheck: function(to, from, dragEl, evt) {
            return this.is_favorite_list_groups && dragEl.__vue__.is_favorite_group;
        },
        draggablePutItemCheck: function(to, from, dragEl, evt) {
            return (this.is_favorite_list_items || this.path === 'opened_items') && !dragEl.__vue__.is_favorite_group;
        }
    }
});