/* global Vue, sima, tinymce, this */

Vue.component('sima-iframe', {
    template: '#sima_iframe',
    props: {},
    computed: {},
    data: function () {
        return {
            iframe_content: typeof this.$slots.default !== 'undefined' ? this.$slots.default[0].text : '',
            body_class: ''
        };
    },
    watch: {
        iframe_content: function(new_val, old_val){
            if(new_val !== old_val)
            {
                this.renderIframe();
            }
        }
    },
    mounted: function() {
        var _this = this;     
        $(this.$el).on('destroyed', function(){
            _this.$destroy();
        });
        
        var tab_layout = this.$el.closest(".sima-layout-panel");
        if(tab_layout)
        {
            $(tab_layout).on('sima-layout-allign', function(e){
                if(_this.$el.contentWindow)
                {             
                    var ifr_body = _this.$el.contentWindow.document.body;
                    var ifr_heigth = 0;
                    var total_nodes = ifr_body.children.length;
                    for (var i = total_nodes-1; i >= 0; i--) {
                        var node = ifr_body.children[i];
                        ifr_heigth += $(node).outerHeight(true);
                    }
                    ifr_heigth = ifr_heigth === 0 ? ifr_body.scrollHeight : ifr_heigth;
                    _this.$el.style.height = ifr_heigth +'px';
                }
            });
        }
        
        this.renderIframe();
    },
    updated(){
        this.iframe_content = typeof this.$slots.default !== 'undefined' ? this.$slots.default[0].text : '';
    },
    methods: {
        renderIframe: function(){
            var doc = this.$el.contentWindow.document;
            doc.open();
            doc.write(`
                <style>
                    html, body {
                        width: 100%;
                        height: 100%;
                        margin: 0;
                        padding: 0;
                    }

                    body {
                        color: #222222;
                        font: normal 10pt Arial,Helvetica,sans-serif;
                        word-break: break-word;
                        display: initial;
                    }

                    .sima-image-file-preview {
                        cursor: pointer;
                    }
            
                    .sima-display-html .sima-model-link {
                        z-index: 0;
                        cursor: default;
                        padding: 0px 2px;
                        overflow: hidden;
                        color: black;
                        border-radius: 2px;
                        position: relative;
                        display: inline-block;
                        vertical-align: bottom;
                        transition: color 0.25s ease-out;
                    }

                    .sima-display-html._open_new_tab .sima-model-link {
                        cursor: se-resize !important;
                    }

                    .sima-display-html .sima-model-link._disabled {
                        opacity: 1 !important;
                    }

                    .sima-display-html .sima-model-link {
                        cursor: pointer;
                    }

                    .sima-display-html .sima-model-link:active {
                        opacity: 0.5;
                    }

                    .sima-display-html .sima-model-link:before {
                        content: "";
                        position: absolute;
                        z-index: -1;
                        top: 0;
                        left: 0;
                        width: 100%;
                        height: 100%;
                        background-color: #606060;
                        transition: transform 0.25s ease-out;
                        transform: translateY(calc(100% - 1px));
                    }

                    /* display html */
                    .sima-display-html .sima-model-link:hover {
                        color: white;
                        transition-delay: 0.25s;
                    }

                    .sima-display-html .sima-model-link:hover:before {
                        transform: translateY(0);
                        background-color: #2e3d4c;
                        transition: transform 0.25s ease-out;
                        transition-delay: 0.25s;
                    }

                    /* _job */
                    .sima-display-html._job .sima-model-link, .sima-theme-display-colored-span._job {
                        color: blue;
                    }

                    /* _bid */
                    .sima-display-html._bid .sima-model-link, .sima-theme-display-colored-span._bid {
                        color: green;
                    }

                    /* _job _bid hover */
                    .sima-display-html._job .sima-model-link:hover,
                    .sima-display-html._bid .sima-model-link:hover {
                        color: white;
                    }

                    /* _ctrl */
                    .sima-display-html._ctrl .sima-model-link {
                        cursor: default;
                    }

                    .sima-display-html._ctrl .sima-model-link:active {
                        opacity: 1;
                    }

                    .sima-display-html._ctrl .sima-model-link:hover {
                        color: black;
                    }

                    .sima-display-html._ctrl .sima-model-link:hover:before {
                        transform: translateY(100%);
                    }

                    /* _ctrl_ctrl_pressed */
                    .sima-display-html._ctrl_pressed .sima-model-link {
                        color: white !important;
                        cursor: pointer;
                        transition-delay: 0.25s;
                    }
                    .sima-display-html._ctrl_pressed .sima-model-link:before,
                    .sima-display-html._ctrl_pressed .sima-model-link:hover:before {
                        transform: translateY(0);
                        background-color: #7a848e;
                        transition: transform 0.25s ease-out;
                        transition-delay: 0.25s;
                    }
                    .sima-display-html._ctrl_pressed .sima-model-link:hover:before {
                        background-color: #2e3d4c;
                    }
                    .sima-display-html._ctrl_pressed .sima-model-link:active {
                        opacity: 0.5;
                    }
                    /* end display html */

                    /* display html u novom basicinfu */
                     .dark-background .sima-display-html .sima-model-link {
                             color: #ccc;
                             cursor: auto;
                    }
                     .dark-background .sima-display-html .sima-model-link._disabled {
                             cursor: auto;
                    }

                    /* sima-display-html */
                     .dark-background .sima-display-html .sima-model-link {
                             cursor: pointer;
                             color: #ccc;
                    }
                     .dark-background .sima-display-html .sima-model-link:before {
                             background-color: #ccc;
                    }
                     .dark-background .sima-display-html .sima-model-link:hover {
                             color: #2e3d4c;
                    }
                    .dark-background .sima-display-html .sima-model-link:hover:before {
                        background-color: white;
                        transition-delay: 0.25s;
                    }

                    /* sima-display-html _ctrl */
                    .dark-background .sima-display-html._ctrl .sima-model-link {
                             cursor: default;
                    }
                     .dark-background .sima-display-html._ctrl .sima-model-link:active {
                             opacity: 1;
                    }
                     .dark-background .sima-display-html._ctrl .sima-model-link:hover {
                        color: #ccc;
                    }
                    .dark-background .sima-display-html._ctrl .sima-model-link:hover:before {
                        background-color: #ccc;
                        transform: translateY(100%);
                    }

                    /* sima-display-html _ctrl_pressed */
                    .dark-background .sima-display-html._ctrl_pressed .sima-model-link {
                         cursor: pointer;
                         color: #2e3d4c !important;
                    }
                    .dark-background .sima-display-html._ctrl_pressed .sima-model-link:hover {
                        color: #2e3d4c !important;
                    }
                    .dark-background .sima-display-html._ctrl_pressed .sima-model-link:before {
                             transform: translateY(0);
                             background-color: #ccc;
                    }
                    .dark-background .sima-display-html._ctrl_pressed .sima-model-link:hover:before {
                             transform: translateY(0);
                             background-color: white;
                    }
                    /* end display html in dark-background */
                </style>
                <script>
                    var sima = window.parent.sima;
                    var jQuery = window.parent.jQuery;
                    var $ = jQuery;

                    document.addEventListener('click', function(e) {
                        if(e.target && e.target.classList.contains('sima-image-file-preview'))
                        {
                            window.parent.sima.dialog.openActionInDialogAsync('filePreview/previewDialogContent', JSON.parse(e.target.dataset.filePreviewParams));
                        }
                    });
                    
                    window.onkeydown = function(event){
                        window.parent.dispatchEvent(new KeyboardEvent('keydown', event));
                    };

                    window.onkeyup = function(event){
                        window.parent.dispatchEvent(new KeyboardEvent('keyup', event));
                    };
                <\/script>
                ` + this.iframe_content
            );
            doc.close();
            if (!sima.isEmpty(this.body_class))
            {
                doc.body.classList.add(this.body_class);
            }
            this.$el.style.height = this.$el.contentWindow.document.body.scrollHeight + 'px';
        },
        setContent: function(content, body_class = '') {
            this.iframe_content = content;
            this.body_class = body_class;
        }
    }
});