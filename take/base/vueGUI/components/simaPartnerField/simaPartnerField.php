<script type="text/x-template" id="sima-partner-field">
    <div class="sima-partner-field" >
        <input
            class="spf-search-field"
            v-bind:placeholder="$attrs.placeholder"
            v-model="search"
            v-on="scope.events"
            ref="searchInptuField"
            v-bind:maxlength="256"
        />
        <span class="sima-icon _search_form _16 partner-dialog-icon" 
            v-if="settings.add_button"
            @click="openDialog">
        </span>
        
        <input type="hidden" v-bind:name="$attrs.name" v-bind:value="input_value" class="sima-partner-field-input-value"/>
        
        <sima-partner-field-result-list ref="safResultListComponent"             
            v-show="open_result_list"
            v-bind:results="results"
            v-bind:selected_item="selected_item"
            v-bind:search="search"
            v-bind:show_add_new_item="show_add_new_item"
            v-bind:saf_bus="saf_bus"
            v-bind:total="total"
            v-bind:company_display_label="company_display"
            v-bind:person_display_label="person_display"
            @savedPartnerForm="savedPartnerForm"
            @selectedCompanyItem="selectedCompanyItem"
            @selectedPersonItem="selectedPersonItem">
        </sima-partner-field-result-list>           
        
    </div>
</script>