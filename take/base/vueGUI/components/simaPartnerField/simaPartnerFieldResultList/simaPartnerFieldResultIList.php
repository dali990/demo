<script type="text/x-template" id="sima-partner-field-result-list">
    <div class="sima-partner-field-result-list">
        <div class="column-section" v-bind:class="{'slideout': !show_company_result_list, 'slidein': show_company_result_list}">
            <div ref="companyTitle"> <?=Yii::t('BaseModule.PartnerField', 'Companies')?> </div>
            <div v-show="show_add_new_item && display_search.company !== '' " 
                class="spf-add-new-item" 
                v-on:click="openPartnerForm('Company')"
                ref="add_new_item_company">      
                {{display_search.company}}<i class="fa fa-plus" style="float:right"></i>
            </div>
            <div v-bind:class="{'slideup': !show_company_form, 'slidedown': show_company_form}" style="width: 100%" >
                <div class="spf-form">
                    <table>
                        <tr>
                            <td style="text-align: right;" v-bind:class="{'has-error':Company.errors.name}">
                                <?=Yii::t('Company','Name')?> <span class="required">*</span>
                            </td>
                            <td>
                                <input class="text-field" v-model="Company.name" ref="companyName"/>
                                <div v-if="Company.errors.name" class="error-message" v-html="Company.errors.name[0]"></div>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right;" v-bind:class="{'has-error':Company.errors.PIB}">
                                <?=Yii::t('Company','PIB')?>
                            </td>
                            <td>
                                <input class="text-field" v-model="Company.PIB" v-bind:maxlength="field_settings.company_pib_length"/>
                                <div v-if="Company.errors.PIB" class="error-message" v-html="Company.errors.PIB[0]"></div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align: right;">
                            <sima-button
                                style="float:right"
                                v-on:click="saveNewCompany"
                                v-bind:class="{_disabled: !can_save_company}"
                                title="<?=Yii::t('BaseModule.PartnerField', 'Add')?>"
                            >
                                <span class="sima-btn-title"><?=Yii::t('BaseModule.PartnerField', 'Add')?></span>
                            </sima-button>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="result-list-wrapper" v-bind:class="{'slideup': show_company_form || show_person_form, 'slidedown': !show_company_form && !show_person_form}">
                <ul class="result-list" ref="results_companies"               
                    v-show="results.companies.length > 0 && !show_company_form" >
                    <li v-bind:class="'result-item model_'+result.id+' item_index_'+(i+1)" v-for="(result, i) in results.companies"
                        v-on:click="selectCompanyItem(i, result.id)">
                        <div style="display:inline" v-html="hits(result[company_display_label], i)"></div>
                    </li>
                </ul>
                <hr v-if="companyTotalResultsDisplay !=''">
                <div class="total-results" v-html="companyTotalResultsDisplay"></div>
            </div>
        </div>
        
        <div class="column-section" v-bind:class="{'slideout': !show_person_result_list, 'slidein': show_person_result_list}">
            <div ref="personTitle"> <?=Yii::t('BaseModule.PartnerField', 'Persons')?> </div>
            <div v-show="show_add_new_item && display_search.person !== '' " 
                class="spf-add-new-item" 
                v-on:click="openPartnerForm('Person')"
                ref="add_new_item_person">
                {{display_search.person}}<i class="fa fa-plus" style="float:right"></i>
            </div>
            <div v-bind:class="{'slideup': !show_person_form, 'slidedown': show_person_form}" style="width: 100%" >
                <div class="spf-form " >
                    <table>
                        <tr>
                            <td style="text-align: right;" v-bind:class="{'has-error':Person.errors.firstname}">
                                <?=Yii::t('Person','FirstName')?> <span class="required">*</span>
                            </td>
                            <td>
                                <input class="text-field" v-model="Person.firstname" ref="personFirstName"/>
                                <div v-if="Person.errors.firstname" class="error-message" v-html="Person.errors.firstname[0]"></div>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right;" v-bind:class="{'has-error':Person.errors.lastname}">
                                <?=Yii::t('Person','LastName')?> <span class="required">*</span>
                            </td>
                            <td>
                                <input class="text-field" v-model="Person.lastname"/>
                                <div v-if="Person.errors.lastname" class="error-message" v-html="Person.errors.lastname[0]"></div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align: right;">
                            <sima-button
                                style="float:right"
                                v-on:click="saveNewPerson"
                                v-bind:class="{_disabled: !can_save_person}"
                                title="<?=Yii::t('BaseModule.PartnerField', 'Add')?>"
                            >
                                <span class="sima-btn-title"><?=Yii::t('BaseModule.PartnerField', 'Add')?></span>
                            </sima-button>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="result-list-wrapper" v-bind:class="{'slideup': show_person_form, 'slidedown': !show_person_form}">
                <ul class="result-list"                    
                    v-show="results.persons.length > 0 && !show_company_form" 
                    ref="results_persons">
                    <li v-bind:class="'result-item model_'+result.id+' item_index_'+(i+1)" v-for="(result, i) in results.persons"
                        v-on:click="selectPersonItem(i, result.id)">
                        <div style="display:inline" v-html="hits(result[person_display_label], i)"></div>
                    </li>
                </ul>
                <hr v-if="personTotalResultsDisplay!=''">
                <div class="total-results" v-if="!show_company_form" v-html="personTotalResultsDisplay"></div>
            </div>
        </div>
    </div>
</script>