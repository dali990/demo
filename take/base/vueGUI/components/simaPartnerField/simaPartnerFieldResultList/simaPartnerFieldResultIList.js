
/* global Vue, sima */
Vue.component('sima-partner-field-result-list', {
    template: '#sima-partner-field-result-list',
    props: {
        results: {
            type: Object,
            default() { return {companies:[], persons:[]}; }
        },
        total: {type: Object},
        search: { type: String, default: '' },
        selected_item: { type: Object, default: null }, 
        selected_model_name: { type: String, default: '' },
        show_add_new_item: { type: Boolean, default: true },//Prikaz elementa za otvaranje forme
        saf_bus: {type: Object},
        person_display_label: { type: String, default: 'display_name' },
        company_display_label: { type: String, default: 'display_name' }
    },
    data: function () {
        return {
            show_company_form: false,
            show_person_form: false,
            show_company_result_list: true,
            show_person_result_list: true,
            search_params: {},
            selected_model: "",
            selected_id: "",
            Company: {
                name: "",
                PIB: "",
                errors: {}
            },
            Person: {
                firstname: "",
                lastname: "",
                errors: {}
            },
            field_settings: {
                company_mb_length: 20,
                company_pib_length: 9,
                person_jmbg_length: 13
            },
            display_search: {
                company: "",
                person: ""
            },
            search_field_focus: false, //cuva padatak da li je focus nad poljem za pretrage
            navigation_key_position: -1, // navigacija za selektovanje stavke u listi rezultata. -1 - deselect
            navigation_selected_list: null,
            navigation_selected_title: null,
            navigation_selected_results: 'companies',
            navigation_selected_add_new_item: null
        };
    },
    computed: {
        can_save_person(){
            if(this.Person.firstname !== "" && this.Person.lastname !== "")
            {
                return true;
            }
            return false;
        },
        can_save_company(){
            if(this.Company.name)
            {
                if(this.Company.PIB !== "" && (isNaN(this.Company.PIB) || this.Company.PIB.length !== 9))
                {
                    return false;
                }
                return true;
            }
            return false;
        },
        companyTotalResultsDisplay(){
            var string = "";
            if(this.total.companies > 50)
            {
                string = sima.translate('SearchFieldTotalResultsPreciseSearchCriteria', {
                            '{total}': this.total.companies
                        });
            }
            else if(this.total.companies === 0)
            {
                string = sima.translate('SearchFieldNoResults');
            }
            return string;
        },
        personTotalResultsDisplay(){
            var string = "";
            if(this.total.persons > 50)
            {
                string = sima.translate('SearchFieldTotalResultsPreciseSearchCriteria', {
                            '{total}': this.total.persons
                        });
            }
            else if(this.total.persons === 0)
            {
                string = sima.translate('SearchFieldNoResults');
            }
            return string;
        }
    },
    watch: {
        search(new_val, old_val) {
            if(new_val !== old_val)
            {
                this.show_company_form = false;
                this.show_person_form = false;
                this.show_company_result_list = true;
                this.show_person_result_list = true;
                this.selectResultList('company');
            }
            this.display_search = this.generateAddNewItemString();
        },
        selected_item(new_val, old_val) {
            if(this.selected_item)
            {
                this.navigation_key_position = -1; //deselect
            }
        }
    },
    created() {
        window.addEventListener('keyup', this.onKeyUp);
        window.addEventListener('keydown', this.onKeyDown);       
    },
    destroyed() {
        window.removeEventListener('keyup', this.onKeyUp);
        window.removeEventListener('keydown', this.onKeyDown);
    },
    mounted: function () {
        var _this = this;

        $(this.$el).on('destroyed', function () {
            _this.$destroy();
        });
        
        this.init();
    },
    methods: {
        hits(text, i) {
            return text.replace(this.search, "<b>" + this.search + "</b>");
        },
        selectPartnerItem(i){
            if(this.navigation_selected_results === 'companies')
            {
                this.selectCompanyItem(i);
            }
            else
            {
                this.selectPersonItem(i);
            }
        },
        selectCompanyItem(i, id) {
            if(!id && this.results.companies.length > 0)
            {
                id = this.results.companies[i].id;
            }
            this.selected_model = 'Company';
            this.show_person_form = false;
            this.show_company_form = false;
            this.selectResultList('company');
            this.$emit('selectedCompanyItem', i, id);
        },
        selectPersonItem(i, id) {
            if(!id && this.results.persons.length > 0)
            {
                id = this.results.persons[i].id;
            }
            this.selected_model = 'Person';
            this.show_company_form = false;
            this.show_person_form = false;
            this.selectResultList('persons');
            this.$emit('selectedPersonItem', i, id);
        },
        init(){
            this.navigation_selected_list = this.$refs.results_companies;
            this.navigation_selected_title = this.$refs.companyTitle;
            this.navigation_selected_add_new_item = this.$refs.add_new_item_company;
            this.saf_bus.$on('bus_event_enter', (data) => {
                if(this.navigation_key_position > 0)
                {
                    this.selectItem(this.navigation_key_position-1);
                }
                else if(this.navigation_key_position === 0)
                {
                    this.navigation_key_position = -1;
                    this.navigation_selected_add_new_item.classList.remove('active-item');
                    this.openPartnerForm();
                }
            });
            this.saf_bus.$on('bus_event_is_focus', () => {
                this.search_field_focus = true;
            });
            this.saf_bus.$on('bus_event_is_blur', () => {
                this.search_field_focus = false;
            });
        },
        onKeyDown(e){
            //Izvrsavanje eventova kada je otvorena lista rezultata ili forma
            if(this.$el.style.display !== 'none' && !this.show_company_form && !this.show_person_form)
            {
                var partner = this.navigation_selected_results;
                //Izvrsavanje eventova samo za listu rezultata
                if(e.keyCode === 38)//up
                {
                    var add_new_item = this.navigation_selected_add_new_item;
                    var ul_list = this.navigation_selected_list;
                    if(this.navigation_key_position >= 1)
                    {
                        for (let item of ul_list.children) {
                            item.classList.remove('active-item');
                        }
                        this.navigation_key_position--;
                        if(this.navigation_key_position > 0)
                        {
                            var list_item = ul_list.children[this.navigation_key_position-1];
                            list_item.classList.add('active-item');
                            this.scrollIntoView(list_item, false);
                        }
                        else if(this.navigation_key_position === 0)
                        {
                            add_new_item.classList.add('active-item');
                        }
                    }
                    else if(this.navigation_key_position === 0)
                    {
                        this.navigation_key_position--;
                        add_new_item.classList.remove('active-item');
                        this.saf_bus.$emit('bus_event_focus_field', e.target);
                    }
                    e.stopPropagation();
                    e.preventDefault();
                }
                else if(e.keyCode === 36)//home
                {
                    var add_new_item = this.navigation_selected_add_new_item;
                    var ul_list = this.navigation_selected_list;
                    
                    add_new_item.classList.remove('active-item');
                    for (let item of ul_list.children) {
                        item.classList.remove('active-item');
                    }
                    this.navigation_selected_list.children[0].classList.add('active-item');
                    this.scrollIntoView(this.navigation_selected_title);
                    this.navigation_key_position = 1;
                }
                else if(e.keyCode === 35)//end
                {
                    var add_new_item = this.navigation_selected_add_new_item;
                    var ul_list = this.navigation_selected_list;
                    
                    add_new_item.classList.remove('active-item');
                    for (let item of ul_list.children) {
                        item.classList.remove('active-item');
                    }
                    var last_index = this.results[this.navigation_selected_results].length-1;
                    this.navigation_selected_list.children[last_index].classList.add('active-item');
                    this.scrollIntoView(this.navigation_selected_list.children[last_index]);
                    this.navigation_key_position = last_index;
                }
                else if(e.keyCode === 37 && !this.search_field_focus)//left
                {
                    var add_new_item = this.navigation_selected_add_new_item;
                    var ul_list = this.navigation_selected_list;

                    add_new_item.classList.remove('active-item');
                    for (let item of ul_list.children) {
                        item.classList.remove('active-item');
                    }
                    this.toggleSelectedResultList();
                    var selected_el = this.navigation_selected_list.children[this.navigation_key_position-1];
                    if(this.navigation_key_position === 0)
                    {
                        selected_el = this.navigation_selected_add_new_item;
                    }
                    else if(this.navigation_key_position === -1)
                    {
                        this.navigation_key_position = 0;
                        selected_el = this.navigation_selected_add_new_item;
                    }
                    else if(this.navigation_key_position > this.results[this.navigation_selected_results].length)
                    {
                        this.navigation_key_position = this.results[this.navigation_selected_results].length;
                        selected_el = this.navigation_selected_list.children[this.navigation_key_position-1];
                    }
                    selected_el.classList.add('active-item');
                    this.scrollIntoView(selected_el);
                }
                else if(e.keyCode === 39 && !this.search_field_focus)//right
                {
                    var add_new_item = this.navigation_selected_add_new_item;
                    var ul_list = this.navigation_selected_list;

                    add_new_item.classList.remove('active-item');
                    for (let item of ul_list.children) {
                        item.classList.remove('active-item');
                    }
                    this.toggleSelectedResultList();
                    var selected_el = this.navigation_selected_list.children[this.navigation_key_position-1];
                    if(this.navigation_key_position === 0)
                    {
                        selected_el = this.navigation_selected_add_new_item;
                    }
                    else if(this.navigation_key_position === -1)
                    {
                        this.navigation_key_position = 0;
                        selected_el = this.navigation_selected_add_new_item;
                    }
                    else if(this.navigation_key_position > this.results[this.navigation_selected_results].length)
                    {
                        this.navigation_key_position = this.results[this.navigation_selected_results].length;
                        selected_el = this.navigation_selected_list.children[this.navigation_key_position-1];
                    }
                    selected_el.classList.add('active-item');
                    this.scrollIntoView(selected_el);
                }
                else if(e.keyCode === 40)//down
                {
                    var add_new_item = this.navigation_selected_add_new_item;
                    var ul_list = this.navigation_selected_list;

                    if(this.navigation_key_position === this.results[this.navigation_selected_results].length)
                    {
                        add_new_item.classList.remove('active-item');
                        for (let item of ul_list.children) {
                            item.classList.remove('active-item');
                        }
                        if(!this.isScrolledIntoView(this.navigation_selected_title))
                        {
                            this.navigation_selected_title.scrollIntoView(true);
                        }
                        this.navigation_key_position = 0;
                        this.navigation_selected_add_new_item.classList.add('active-item');
                    }
                    else if(this.navigation_key_position === -1)
                    {
                        this.navigation_key_position++;
                        add_new_item.classList.add('active-item');
                        this.saf_bus.$emit('bus_event_blur_field', e.target);
                    }
                    else if(this.navigation_key_position < this.results[this.navigation_selected_results].length)
                    {
                        add_new_item.classList.remove('active-item');
                        for (let item of ul_list.children) {
                            item.classList.remove('active-item');
                        }
                        this.navigation_key_position++;
                        var list_item = ul_list.children[this.navigation_key_position-1];
                        list_item.classList.add('active-item');
                        if(!this.isScrolledIntoView(list_item))
                        {
                            list_item.scrollIntoView(true);
                        }
                    }

                    e.stopPropagation();
                    e.preventDefault();
                }
            }
        },
        onKeyUp(e){
            //Izvrsavanje eventova kada je otvorena lista rezultata ili forma
            if(this.$el.style.display !== 'none')
            {
                if(e.keyCode === 27)//esc
                {
                    if(this.show_company_form || this.show_person_form)
                    {
                        this.show_company_form = false;
                        this.show_person_form = false;
                        this.show_person_result_list = true;
                        this.show_company_result_list = true;
                    }
                    else
                    {
                        if(e.target.tagName === "BODY")//ako je ESC nad poljem onda zatvaramo listu rezultata
                        {
                            this.saf_bus.$emit('bus_event_close_result_list');
                        }
                        else
                        {
                            this.saf_bus.$emit('bus_event_blur_field', e.target);
                        }
                    }
                }
                else if(e.keyCode === 13)//enter
                {
                    if(this.navigation_key_position > 0)
                    {
                        this.navigation_selected_list.children[this.navigation_key_position-1].classList.remove('active-item');
                        if(this.navigation_selected_results === 'companies')
                        {
                            this.selectCompanyItem(this.navigation_key_position-1);
                        }
                        else
                        {
                            this.selectPersonItem(this.navigation_key_position-1);
                        }
                        this.navigation_key_position = -1;
                    }
                    else if(this.navigation_key_position === 0)
                    {
                        this.openPartnerForm();
                        this.navigation_selected_add_new_item.classList.remove('active-item');
                        this.navigation_key_position = -1;
                        if(this.navigation_selected_results === 'companies')
                        {
                            this.show_person_result_list = false;
                        }
                        else
                        {
                            this.show_company_result_list = false;
                        }
                    }
                }
                e.stopPropagation();
                e.preventDefault();
            }
        },
        toggleSelectedResultList(){
            if(this.navigation_selected_results === 'companies')
            {
                this.selectResultList('person', this.navigation_key_position);  
            }
            else
            {
                this.selectResultList('company', this.navigation_key_position);
            }
        },
        selectResultList(list='company', position=-1){
            if(list === 'company')
            {
                this.navigation_key_position = position;
                this.navigation_selected_results = 'companies',
                this.navigation_selected_list = this.$refs.results_companies;
                this.navigation_selected_title = this.$refs.companyTitle;
                this.navigation_selected_add_new_item = this.$refs.add_new_item_company;
            }
            else
            {
                this.navigation_key_position = position;
                this.navigation_selected_results = 'persons',
                this.navigation_selected_list = this.$refs.results_persons;
                this.navigation_selected_title = this.$refs.personTitle;
                this.navigation_selected_add_new_item = this.$refs.add_new_item_person;
            }
        },
        openCompanyForm() {
            this.selected_model = 'Company';
            this.focusCompanyNameField();
            this.show_person_form = false;
            this.show_company_form = true;
            this.show_person_result_list = false;
        },
        openPersonForm() {
            this.selected_model = 'Person';
            this.focusPersonFirstNameField();
            this.show_company_form = false;
            this.show_person_form = true;
            this.show_company_result_list = false;
        },
        openPartnerForm(model="") {
            var _this = this;
            this.resetParams();

            this.fillSearchParams();
            this.Company.name = this.search_params.company_name;
            this.Company.PIB = this.search_params.company_PIB;
            this.Person.firstname = this.search_params.person_first_name;
            this.Person.lastname = this.search_params.person_last_name;

            if(model === 'Company')
            {
                _this.openCompanyForm();
            }
            else if(model === 'Person')
            {
                _this.openPersonForm();
            }
            else
            {
                if(_this.navigation_selected_results === 'companies')
                {
                    _this.openCompanyForm();
                }
                else if(_this.navigation_selected_results === 'persons')
                {
                    _this.openPersonForm();
                }
            }
            _this.$emit('openedPertnerForm');
        },
        focusPersonFirstNameField(){
            var _this = this;
            setTimeout(function(){
                if(_this.$refs.personFirstName)
                {
                    _this.$refs.personFirstName.focus();
                }
            },500);
        },
        focusCompanyNameField(){
            var _this = this;
            setTimeout(function(){
                if(_this.$refs.companyName)
                {
                    _this.$refs.companyName.focus();
                }
            },500);
        },
        saveNewCompany(){
            var _this = this;
            var company = {};
            
            company.name = this.Company.name;
            company.PIB = this.Company.PIB;
            
            sima.ajax.get('base/model/simaPartnerFieldSaveNewPartner', {
                data: {
                    company_data: company
                },
                async: true,
                success_function: function (response) {
                    if(response.errors)
                    {
                        _this.Company.errors = response.errors;
                    }
                    else if(response.result)
                    {
                        _this.show_company_form = false;
                        _this.$emit('savedPartnerForm', response.result);
                    }
                }
            });
        },
        saveNewPerson(){
            var _this = this;
            var person = {};
            
            person.firstname = this.Person.firstname;
            person.lastname = this.Person.lastname;
            
            sima.ajax.get('base/model/simaPartnerFieldSaveNewPartner', {
                data: {
                    person_data: person
                },
                async: true,
                success_function: function (response) {
                    if(response.errors)
                    {
                        _this.Person.errors = response.errors;
                    }
                    else if(response.result)
                    {
                        _this.show_person_form = false;
                        _this.$emit('savedPartnerForm', response.result);
                    }
                }
            });
        },
        resetParams(){
            this.search_params = {};
            this.description = "";
            
            this.Company.name = "";
            this.Company.PIB = "";
            this.Company.errors = {};
            
            this.Person.firstname = "";
            this.Person.lastname = "";
            this.Person.errors = {};
        },
        onClickOutside: function (e) {
            var _this = this;
            if (!_this.$el.contains(e.target) && _this.$el !== e.target) {
                _this.$el.remove();
            }
        },
        extractName(string){
            var result = {
                company_name: string,
                company_PIB: "",
                person_first_name: string,
                person_last_name: "",
                person_JMBG: ""
            };
            var n = string.split(" ");
            if(n.length > 1)
            {
                var company_n = sima.clone(n);
                var person_n = sima.clone(n);
                var PIB = company_n[company_n.length - 1];
//                var JMBG = person_n[person_n.length - 1];
                if(!isNaN(PIB) && PIB.length === 9)
                {
                    company_n.pop();//izbacujemo poslednji element
                    result.company_name = company_n.join(" ");
                    result.company_PIB = PIB;
                }
//                if(!isNaN(JMBG) && JMBG.length === 13)
//                {
//                    person_n.pop();
//                    result.person_JMBG = JMBG;
//                }
                result.person_first_name = person_n[0];
                person_n.shift();
                result.person_last_name = person_n.join(" ");
            }
            else
            {
                if(!isNaN(string))
                {
                    //company PIB
                    if(string.length === 9)
                    {
                        result.company_name = "";
                        result.company_PIB = string;
                    }
                    //person JMBG
//                    if(string.length === 13)
//                    {
//                        result.person_first_name = "";
//                        result.person_JMBG = string;
//                    }
                }
            }
            return result;
        },
        generateAddNewItemString(){           
            this.fillSearchParams();
            
            var display_search = {
                company: "",
                person: ""
            };
            
            if(this.search !== "")
            {          
//                var company_display_string = "Dodaj firmu ("+this.search_params.company_name+ ")";
//                var PIB = "";
//                if(this.search_params.company_PIB !=="")
//                {
//                    company_display_string += ", PIB("+this.search_params.company_PIB+")";
//                }
//                display_search.company = company_display_string;
                display_search.company = sima.translate('SimaPartnerFieldAddCompany', {
                    '{name}': this.search_params.company_name,
                    '{pib}': this.search_params.company_PIB
                });
                
//                var person_display_string = "Dodaj osobu (ime: "+this.search_params.person_first_name+"{last_name})";
//                var last_name = "";
//                if(this.search_params.person_last_name !== "")
//                {
//                    last_name = ", prezime: "+this.search_params.person_last_name;
//                }
//                person_display_string = person_display_string.replace('{last_name}', last_name);
//                display_search.person = person_display_string;
                
                display_search.person = sima.translate('SimaPartnerFieldAddPerson', {
                    '{first_name}': this.search_params.person_first_name,
                    '{last_name}': this.search_params.person_last_name
                });
            }
            
            return display_search;
            
        },
        fillSearchParams(){
            if(this.search.trim() !== "")
            {
                var search_params_array = this.search.split(",").map(item => item.trim());
                var extracted_name = this.extractName(search_params_array[0]);
                
                this.search_params.company_name = extracted_name.company_name;
                this.search_params.company_PIB = extracted_name.company_PIB;
                this.search_params.person_first_name = extracted_name.person_first_name;
                this.search_params.person_last_name = extracted_name.person_last_name;
//                this.search_params.person_JMBG = extracted_name.person_JMBG;
            }
            else
            {
                this.search_params = {};
            }
        },
        isScrolledIntoView(el) { //Proverava da li je element vidljiv
            var rect = el.getBoundingClientRect();
            return (
                    (rect.top >= 0) && (rect.bottom <= this.$el.getBoundingClientRect().bottom) && 
                    (rect.bottom >= 0) && (rect.top >= this.$el.getBoundingClientRect().top)
            );
        },
        scrollIntoView(el, top=true){ //false=bottom
            if(!this.isScrolledIntoView(el))
            {
                el.scrollIntoView(top);
            }
        }
    }
});