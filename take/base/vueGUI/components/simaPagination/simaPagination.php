<script type="text/x-template" id="sima_pagination_template">
    <div 
        class="sima-pagination"
    >
        <sima-button
            class="_half"
            v-bind:class="this.local_curr_page <= 1 ? '_disabled' : ''"
            title="<?=Yii::t('BaseModule.SIMAPagination', 'FirstPage')?>"
            v-on:click="firstPageClick()"
        >
            <span class="sima-old-btn-icon sima-icon _half _18 _start-arrow"></span>
        </sima-button>
        <sima-button
            class="_half"
            v-bind:class="this.local_curr_page <= 1 ? '_disabled' : ''"
            title="<?=Yii::t('BaseModule.SIMAPagination', 'PreviousPage')?>"
            v-on:click="previousPageClick()"
        >
            <span class="sima-old-btn-icon sima-icon _half _18 _left-arrow"></span>
        </sima-button>
        <div class="sima-pagination-page-num-wrap">
            <input class="sima-pagination-curr-page-num" 
                title="<?php echo Yii::t('BaseModule.SIMAPagination', 'CurrentPage'); ?>" 
                v-bind:value="this.local_curr_page" 
                @keyup.enter="onCurrPageEnter"
                v-onlyNumber:positive
            />
            <span class="sima-pagination-page-num-separator">/</span>
            <input class="sima-pagination-total-page-num" 
                title="<?php echo Yii::t('BaseModule.SIMAPagination', 'TotalNumberOfPages'); ?>" 
                v-bind:value="this.last_page" readonly 
            />
        </div>
        <sima-button
            class="_half"
            v-bind:class="this.local_curr_page >= this.last_page ? '_disabled' : ''"
            title="<?=Yii::t('BaseModule.SIMAPagination', 'NextPage')?>"
            v-on:click="nextPageClick()"
        >
            <span class="sima-old-btn-icon sima-icon _half _18 _right-arrow"></span>
        </sima-button>
        <sima-button
            class="_half"
            v-bind:class="this.local_curr_page >= this.last_page ? '_disabled' : ''"
            title="<?=Yii::t('BaseModule.SIMAPagination', 'LastPage')?>"
            v-on:click="lastPageClick()"
        >
            <span class="sima-old-btn-icon sima-icon _half _18 _end-arrow"></span>
        </sima-button>
    </div>
</script>