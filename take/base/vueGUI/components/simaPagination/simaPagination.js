/* global Vue, sima */

Vue.component('sima-pagination', {
    template: '#sima_pagination_template',
    props: {
        total: {
            type: Number,
            default: 0
        },
        page_size: {
            type: Number,
            default: 50
        },
        curr_page: {
            type: Number,
            default: 1
        }
    },
    data: function () {
        return {
            local_curr_page: this.curr_page
        };
    },
    computed: {
        offset: function() {
            return (this.local_curr_page - 1) * this.page_size;
        },
        last_page: function() {
            return Math.ceil(this.total / this.page_size);
        }
    },
    watch: {
        curr_page: function() {
            this.local_curr_page = this.curr_page;
        }
    },
    mounted: function() {
        var _this = this;
        
        $(this.$el).on('destroyed', function(){
            _this.$destroy();
        });
    },
    methods: {
        firstPageClick: function() {
            this.local_curr_page = 1;
            this.$emit('firstPage', this.offset, this.page_size);
        },
        previousPageClick: function() {
            if (this.local_curr_page > 1)
            {
                this.local_curr_page--;
            }
            this.$emit('prevPage', this.offset, this.page_size);
        },
        nextPageClick: function() {
            if (this.local_curr_page < this.last_page)
            {
                this.local_curr_page++;
            }
            this.$emit('nextPage', this.offset, this.page_size);
        },
        lastPageClick: function() {
            this.local_curr_page = this.last_page;
            this.$emit('lastPage', this.offset, this.page_size);
        },
        onCurrPageEnter: function(event) {
            var curr_value = parseInt((event.target.value).trim());
            if (sima.isEmpty(curr_value))
            {
                sima.dialog.openWarn(sima.translate('SIMAPaginationCurrentPageCanNotBeEmpty'));
            }
            else if (curr_value > this.last_page)
            {
                sima.dialog.openWarn(sima.translate('SIMAPaginationCurrentPageCanNotBeLargerThanTheTotalNumberOfPages'));
            }
            else if (curr_value !== this.local_curr_page)
            {
                this.local_curr_page = curr_value;
                this.$emit('currPageEnter', this.offset, this.page_size);
            }
        },
        setOffset: function(offset) {
            this.local_curr_page = offset/this.page_size + 1;
        }
    }
});

