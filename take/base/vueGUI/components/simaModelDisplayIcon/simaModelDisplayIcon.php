<script type="text/x-template" id="sima-model-display-icon">
    <div 
        class="sima-model-display-icon"
        v-bind:class="{'_clickable': allow_click_on_icon}"
        v-bind:title="model.DisplayName"
        v-on:click="onIconClick"
    >
        <span 
            v-if="is_icon_type_icon" 
            class="sima-model-display-icon-stack fa-stack"
        >
            <i class="sima-model-display-icon-circle fa fa-stack-2x"></i>
            <i 
                class="sima-model-display-icon-icon fa-stack-1x"
                v-bind:class="get_model_icon"
            >
            </i>
        </span>
        <img 
            v-if="is_icon_type_img" 
            class="sima-model-display-icon-img" 
            v-bind:src="model.display_icon.src">
        </img>
    </div>
</script>