
/* global Vue, sima */

Vue.component('sima-model-display-icon', {
    template: '#sima-model-display-icon',
    mixins: [sima.vue.getBaseComponent()],
    props: {
        model: {
            validator: sima.vue.ProxyValidator
        },
        allow_click_on_icon: {
            type: Boolean,
            default: true
        },
        model_icon: {
            type: String,
            default: ''
        },
        icon_prefix: {
            type: String,
            default: 'sis'
        }
    },
    data: function () {
        return {
            
        };
    },
    computed: {
        is_icon_type_img: function() {
            return typeof this.model.display_icon !== 'undefined' ? this.model.display_icon.type === 'IMG' : false;
        },
        is_icon_type_icon: function() {
            return typeof this.model.display_icon !== 'undefined' ? this.model.display_icon.type === 'ICON' : false;
        },
        get_model_icon: function() {
            if (!sima.isEmpty(this.model_icon))
            {
                return this.model_icon;
            }

            var model_icon = '';
            
            if (
                    typeof this.model.display_icon !== 'undefined' &&
                    this.model.display_icon.type === 'ICON'
                )
            {
                var icon_code = this.model.display_icon.code;

                switch(icon_code)
                {
                    case 'Person':
                      model_icon = 'user';
                      break;
                    case 'Company':
                      model_icon = 'building';
                      break;
                    case 'CommentThread':
                      model_icon = 'users';
                      break;
                    case 'File':
                      model_icon = 'file-alt';
                      break;
                    case 'Task':
                      model_icon = 'tasks';
                      break;
                    case 'BafRequest':
                      model_icon = 'bug';
                      break;
                    case 'ServerErrorReport':
                      model_icon = 'exclamation-triangle';
                      break;
                    case 'Baf':
                      model_icon = 'clipboard-list';
                      break;
                  default:
                      model_icon = 'database';
                };
            }

            return 'sima-icon ' + this.icon_prefix + '-' + model_icon;
        },
        model_name: function() {
            return this.model.get_class;
        },
        has_model_display: function() {
            return this.model.has_display;
        },
        model_path2: function() {
            return this.model.path2;
        }
    },
    watch: {
        model_name: () => {},
        has_model_display: () => {},
        model_path2: () => {}
    },
    mounted: function() {
    },
    methods: {
        onIconClick: function(event) {
            if (this.allow_click_on_icon)
            {
                var action = '';
                var action_id = '';

                if (this.has_model_display)
                {
                    action = 'defaultLayout';
                    action_id = {
                        model_name: this.model_name,
                        model_id: this.model.id
                    };
                }
                else if (!sima.isEmpty(this.model_path2))
                {
                    action = this.model_path2;
                    action_id = this.model.id;
                }

                if (action !== '' && action_id !== '')
                {
                    sima.dialog.openModel(action, action_id, null, {displayhtml: true, event: event});
                }
            }
        }
    }
});