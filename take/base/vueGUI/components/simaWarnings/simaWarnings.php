<script type="text/x-template" id="warnings_template">
    <div id="sima-popup-warnings" class="navbar-popup" v-click-outside="hidePopup"> 
        <i v-bind:class="warning_icon_classes" 
            v-on:click="list_show = !list_show"
            v-bind:data-count="warnings_amount_cnt"
        ></i>
        <div v-show="list_show" class="_tabs_wrapper" v-bind:class="preanimated_class">
            <div class="_arrow"></div>
            <div class="_header">
                <span class="_title"><?=Yii::t('BaseModule.Warns', 'Warnings')?></span>
            </div>
            <vue-tabs class="_body warnings-body">
                <v-tab icon="sima-icon sis-exclamation-circle" title="<?=Yii::t('BaseModule.Warns', 'AllWarnings')?>">
                    <vue-perfect-scrollbar class="perfect-scrollbar" v-bind:settings="{ minScrollbarLength: 30 }" ref="perfect_scrollbar">
                        <ul>
                            <sima-warning 
                                v-for="warning in all_warnings"
                                v-bind:key="warning"
                                v-bind:code="warning" 
                                v-on:amountChanged="warningAmountCnt"
                                v-on:updated="onWarningUpdated"
                                v-on:open_warning="hidePopup"
                            >
                            </sima-warning>
                        </ul>
                    </vue-perfect-scrollbar>
                </v-tab>
            </vue-tabs>
        </div>
    </div>
</script>

