<script type="text/x-template" id="warning_element_template">
    <li class="_item" v-if="warning_amount > 0" v-on:click="clicked" >
        <span class="_title">{{warning_title}}:</span>
        <span class="_amount">{{warning_amount}}</span>
    </li>
</script>