/* global Vue, sima */

Vue.component('sima-warning', {
    props: ['code'],
    data: function(){
        return {
            
        };
    },
    computed: {
        curr_user: function(){ 
            return sima.vue.shot.getters.model('User_' + sima.getCurrentUserId());
        },
        warning_element: function() {
            var warn_code = 'warn__'+ this.code;
            return this.curr_user.manualUpdate(warn_code);
        },
        warning_title: function(){
            return !sima.isEmpty(this.warning_element) ? this.warning_element.title : '';
        },
        warning_amount: function(){
            return !sima.isEmpty(this.warning_element) ? this.warning_element.amount : 0;
        },
        warning_onclick_data: function(){
            return !sima.isEmpty(this.warning_element) ? this.warning_element.onclick : null;
        }
    },
    watch:{
        warning_amount: function(new_val, old_val){ 
            if (new_val !== old_val)
            {
                this.$emit('amountChanged');
            }
        }
    },
    methods: {
        clicked: function(){
            sima.callFunction(this.warning_onclick_data);
            
            this.$emit('open_warning');
        }
    },
    mounted: function(){
        
    },
    updated: function(){
        if(this.warning_amount > 0 && this.code === 'WARN_UNCONFIRMED_DAYS')
        {
            this.$emit('updated');
        }
    },
    template: '#warning_element_template'
});