/* global Vue, sima */

    Vue.component('sima-warnings', {
        template: '#warnings_template',
        data: function(){
            return {
                unconfirmed_days_warn_dialog_initialy_shown: false,
                unconfirmed_days_warn_dialog_shown: {
                    type: Boolean,
                    default: false
                },
                unconfirmed_days_warn_dialog_interval: null,
                list_show: false,
                show_unconfirmed_days_warn: sima.getShowUnconfirmedDaysWarn(),
                show_unconfirmed_days_warn_interval: sima.getShowUnconfirmedDaysWarnInterval(),
                tolerable_unconfirmed_days: sima.getTolerableUnconfirmedDays(),
                warnings_amount_cnt: 0
            };
        },
        watch: {
            list_show: function(new_val, old_val) {
                if (new_val !== old_val)
                {
                    $('#sima-popup-overlay').toggle();
                }
            }
        },
        mounted: function () {
            var _this = this;
            // da se ne bi aktivirala periodicna provera
            if(this.show_unconfirmed_days_warn === true)
            {
                this.showUnconfirmedDaysWarnDialogPeriodicly();
            }
            $(sima.eventrelay).on('JS_NEW_WARN', function(event, event_message){
                $.each( event_message.message.data, function( key, value ) {
                    _this.curr_user.setDirty('warn__' + key);                        
                });
            });
        },
        beforeDestroy: function(){
            clearInterval(this.unconfirmed_days_warn_dialog_interval);
        },
        computed: {
            unconfirmed_days_amount: function(){
                var unconfirmed_days_warns = this.all_warnings.filter(function(warning){
                    return warning === 'WARN_UNCONFIRMED_DAYS';
                });
                var unconfirmed_days_warn_amount = 0;
                if(unconfirmed_days_warns.length === 1)
                {
                    var unconfirmed_days_warn = unconfirmed_days_warns[0];
                    unconfirmed_days_warn_amount = parseInt(unconfirmed_days_warn.amount);
                }
                return unconfirmed_days_warn_amount;
            },
            unconfirmed_days_warn_dialog_to_stop: function(){
                return this.unconfirmed_days_amount === 0;
            },
            warning_icon_classes: function(){
                return {
                    '_icon sima-icon sil-exclamation-circle': true,
                    'sis-exclamation-circle has-badge' : this.warnings_amount_cnt > 0
                };
            },
            preanimated_class: function(){
                return {
                    '_preanimated': !this.list_show,
                    '_animated': this.list_show
                };
            },
            show_unconfirmed_days_warn_interval_num: function(){
                return this.show_unconfirmed_days_warn_interval.substring(0, this.show_unconfirmed_days_warn_interval.length-1);
            },
            show_unconfirmed_days_warn_interval_char: function(){
                return this.show_unconfirmed_days_warn_interval.substring(this.show_unconfirmed_days_warn_interval.length-1, this.show_unconfirmed_days_warn_interval.length);
            },
            tolerable_unconfirmed_days_length: function(){
                return this.tolerable_unconfirmed_days.length;
            },
            tolerable_unconfirmed_days_num: function(){
                return this.tolerable_unconfirmed_days.substring(0, this.tolerable_unconfirmed_days_length-1);
            },
            tolerable_unconfirmed_days_char: function(){
                return this.tolerable_unconfirmed_days.substring(this.tolerable_unconfirmed_days_length-1, this.tolerable_unconfirmed_days_length);
            },
            show_unconfirmed_days_warn_interval_to_ms: function(){
                var time_diff = 60 * 60; /// 1h default
                if(this.show_unconfirmed_days_warn_interval_char === 'd')
                {
                    time_diff = this.show_unconfirmed_days_warn_interval_num * 60 * 60 * 24;
                }
                else if(this.show_unconfirmed_days_warn_interval_char === 'h')
                {
                    time_diff = this.show_unconfirmed_days_warn_interval_num * 60 * 60;
                }
                else if(this.show_unconfirmed_days_warn_interval_char === 'm')
                {
                    time_diff = this.show_unconfirmed_days_warn_interval_num * 60;
                }
                else if(this.show_unconfirmed_days_warn_interval_char === 's')
                {
                    time_diff = this.show_unconfirmed_days_warn_interval_num;
                }
                return time_diff * 1000; /// to ms
            },
            curr_user: function(){ 
                return sima.vue.shot.getters.model('User_' + sima.getCurrentUserId());
            },
            all_warnings: function() {
                return sima.getAllWarnings();
            }
        },
        methods: {
            hidePopup: function(){
                if(this.list_show === true)
                {
                    this.list_show = false;
                }
            },
            warningAmountCnt: function(){
                var cnt = 0;
                if (typeof this.$refs['perfect_scrollbar'] !== 'undefined')
                {
                    for(var i=0; i<this.$refs['perfect_scrollbar'].$children.length; i++)
                    {
                        if(this.$refs['perfect_scrollbar'].$children[i].warning_amount > 0)
                        {
                            cnt++;
                        }
                    }
                }            
                this.warnings_amount_cnt = cnt;
            },
            onWarningUpdated: function() {
                this.showUnconfirmedDaysWarnDialogInitially();
            },
            tolerableIntervalExceeded: function(){
                var result = false;
                
                if(this.tolerable_unconfirmed_days_char === 'd')
                {
                    if(this.unconfirmed_days_amount >= this.tolerable_unconfirmed_days_num)
                    {
                        result = true;
                    }
                }
                else if(this.tolerable_unconfirmed_days_char === 'w')
                {
                    if(this.unconfirmed_days_amount >= this.tolerable_unconfirmed_days_num*7)
                    {
                        result = true;
                    }
                }
                else if(this.tolerable_unconfirmed_days_char === 'm')
                {
                    if(this.unconfirmed_days_amount >= this.tolerable_unconfirmed_days_num*30)
                    {
                        result = true;
                    }
                }

                return result;
            },
            toShowWarnDialog: function(){
                var result = true;
                if(
                    this.unconfirmed_days_warn_dialog_to_stop === true
                    || this.unconfirmed_days_warn_dialog_shown === true
                    || this.tolerableIntervalExceeded() === false
                )
                {
                    result = false;
                }
                return result;
            },
            showUnconfirmedDaysWarnDialogInitially: function(){
                if(this.unconfirmed_days_warn_dialog_initialy_shown === false)
                {
                    this.showUnconfirmedDaysWarnDialog();
                }
            },
            showUnconfirmedDaysWarnDialogPeriodicly: function(){
                this.showUnconfirmedDaysWarnDialog();
                this.unconfirmed_days_warn_dialog_interval = setInterval(function () {
                    this.showUnconfirmedDaysWarnDialog();
                }.bind(this), this.show_unconfirmed_days_warn_interval_to_ms);
            },
            showUnconfirmedDaysWarnDialog: function(){
                var _this = this;
                if(this.toShowWarnDialog() !== true)
                {
                    return;
                }
                
                this.unconfirmed_days_warn_dialog_shown = true;
                if(this.unconfirmed_days_warn_dialog_initialy_shown === false)
                {
                    this.unconfirmed_days_warn_dialog_initialy_shown = true;
                }
                                
                sima.dialog.openWarn(sima.translate('YouHaveUnconfirmedActivities', {
                    '{unconfirmed_days_amount}': this.unconfirmed_days_amount
                }), sima.translate('UnconfirmedActivities'), {
                    function: function(){
                        _this.unconfirmed_days_warn_dialog_shown = false;
                    },
                    close_function: function(){
                        _this.unconfirmed_days_warn_dialog_shown = false;
                    }
                });
            }
        }
    });
