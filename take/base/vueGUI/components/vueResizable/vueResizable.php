<script type="text/x-template" id="vue-resizable-template">
    <div class="resizable-component" :style="style">
        <slot></slot>
        <template v-for="el in active">
            <div :class="'resizable-'+el" :key="el"></div>
        </template>
    </div>
</script>
