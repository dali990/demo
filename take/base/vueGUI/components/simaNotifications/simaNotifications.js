/* global Vue, sima */

Vue.component('sima-notifications', {
    template: '#sima-notifications-template',
    mixins: [sima.vue.getBaseComponent()],
    props: {
        
    },
    data: function () {
        return {
            show_popup: false,
            all_limit: 10,
            unseen_limit: 10,
            notification_number_per_page: 10,
            local_all_notifications: [],
            local_unseen_notifications: []
        };
    },
    watch: {
        real_unseen_notifications_count: function(new_val, old_val){
            sima.showNewMessagesCountInTitle(null, new_val);
        },
        //mora da se navede u watch-u iako se nalazi u template-u, jer vue iz nekog razloga ne trigeruje update za ovu promenljivu
        unseen_notifications_count: () => {},
        all_notifications: function(new_val, old_val) {
            if (this.all_notifications_status === 'OK')
            {
                this.local_all_notifications = new_val;
            }
        },
        unseen_notifications: function(new_val, old_val) {
            if (this.unseen_notifications_status === 'OK')
            {
                this.local_unseen_notifications = new_val;
            }
        },
        show_popup: function(new_val, old_val) {
            if (new_val !== old_val)
            {
                $('#sima-popup-overlay').toggle();
            }
        }
    },
    computed: {
        show_loading: function() {
            return this.all_notifications_status !== 'OK' || this.unseen_notifications_status !== 'OK';
        },
        all_notifications: function() {
            return sima.vue.shot.getters.model_list(this, 'Notification',{
                model_filter: {
                    limit: this.all_limit,
                    scopes: {
                        forUser: sima.getCurrentUserId(),
                        byTime: true
                    }
                }
            });
        },
        all_notifications_status: function() {
            return this.all_notifications.shotStatus;
        },
        unseen_notifications: function() {
            return sima.vue.shot.getters.model_list(this, 'Notification',{
                model_filter: {
                    limit: this.unseen_limit,
                    scopes: {
                        forUser: sima.getCurrentUserId(),
                        byTime: true,
                        onlyUnseen: true
                    }
                }
            });
        },
        unseen_notifications_status: function() {
            return this.unseen_notifications.shotStatus;
        },
        all_notifications_count: function() {
            return sima.vue.shot.getters.model_list_count(this, 'Notification',{
                model_filter: {
                    scopes: {
                        forUser: sima.getCurrentUserId()
                    }
                }
            });
        },
        unseen_notifications_count: function() {
            return sima.vue.shot.getters.model_list_count(this, 'Notification',{
                model_filter: {
                    scopes: {
                        forUser: sima.getCurrentUserId(),
                        onlyUnseen: true
                    }
                }
            });
        },
        real_unseen_notifications_count: function() {
            return sima.vue.shot.getters.model_list_count(null, 'Notification',{
                model_filter: {
                    scopes: {
                        forUser: sima.getCurrentUserId(),
                        onlyRealUnseen: true
                    }
                }
            });
        },
        show_load_more_all_notifications: function() {
            return this.all_limit <= this.all_notifications_count;
        },
        show_load_more_unseen_notifications: function() {
            return this.unseen_limit <= this.unseen_notifications_count;
        },
        notifications_icon_classes: function(){
            return {
                '_icon sima-icon sil-bell': true,
                'has-badge sis-bell': this.real_unseen_notifications_count > 0
            };
        },
        preanimated_class: function(){
            return {
                '_preanimated': !this.show_popup,
                '_animated': this.show_popup
            };
        }
    },
    mounted: function() {
        
    },
    methods: {
        hidePopup: function() {
            if(this.show_popup === true)
            {
                this.show_popup = false;
            }
        },
        loadMoreAllNotifications: function(){
            this.all_limit += this.notification_number_per_page;
        },
        loadMoreUnseenNotifications: function(){
            this.unseen_limit += this.notification_number_per_page;
        },
        seeAllNotifications: function() {
            sima.mainTabs.openNewTab('guitable', 'base_12');
            this.hidePopup();
        }
    }
});