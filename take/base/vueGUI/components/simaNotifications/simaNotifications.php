<script type="text/x-template" id="sima-notifications-template">
    <div id="sima-popup-notifications" class="navbar-popup" v-click-outside="hidePopup">
        <i v-bind:class="notifications_icon_classes" 
            v-on:click="show_popup = !show_popup"
            v-bind:data-count="real_unseen_notifications_count"
        ></i>
        <div v-show="show_popup" class="_tabs_wrapper" v-bind:class="preanimated_class">
            <!--Sasa A. - mora odvojeno preanimated_class i v-observe-visibility jer su u konfliktu oko display-a-->
            <div v-show="show_popup" v-observe-visibility="componentInstanceVisibilityChanged">
                <div class="_arrow"></div>
                <div class="_header">
                    <span class="_title"><?=Yii::t('BaseModule.Notification', 'Notfications')?></span>
                    <span class="_see_all" v-on:click="seeAllNotifications"><?=Yii::t('BaseModule.Notification', 'SeeAll')?></span>
                </div>
                <vue-tabs class="_body notifications-body">
                    <div class="sima-loading-circle" v-show="show_loading"></div>
                    <v-tab icon="sima-icon sis-bells" title="<?=Yii::t('BaseModule.Notification', 'AllNotfications')?>">
                        <vue-perfect-scrollbar class="perfect-scrollbar" v-bind:settings="{ minScrollbarLength: 30 }">
                            <ul>
                                <sima-notification 
                                    v-for="notification in local_all_notifications" 
                                    v-bind:key="notification.id" 
                                    v-bind:model="notification"
                                    v-on:open_notification="hidePopup"
                                >
                                </sima-notification>
                            </ul>
                        </vue-perfect-scrollbar>
                        <div class="_load_more" v-if="show_load_more_all_notifications">
                            <i 
                                class="fas fa-ellipsis-h" 
                                title="<?=Yii::t('BaseModule.Notification', 'LoadMore')?>" 
                                @click="loadMoreAllNotifications" 
                            ></i>
                        </div>
                    </v-tab>
                    <v-tab icon="sima-icon sis-bell-slash" title="<?=Yii::t('BaseModule.Notification', 'UnreadNotfications')?>" v-if="this.unseen_notifications_count > 0" selectOnMount="true">
                        <vue-perfect-scrollbar class="perfect-scrollbar" v-bind:settings="{ minScrollbarLength: 30 }">
                            <ul>
                                <sima-notification 
                                    v-for="notification in local_unseen_notifications" 
                                    v-bind:key="notification.id" 
                                    v-bind:model="notification"
                                    v-on:open_notification="hidePopup"
                                >
                                </sima-notification>
                            </ul>
                        </vue-perfect-scrollbar>
                        <div class="_load_more" v-if="show_load_more_unseen_notifications">
                            <i 
                                class="fas fa-ellipsis-h" 
                                title="<?=Yii::t('BaseModule.Notification', 'LoadMore')?>" 
                                @click="loadMoreUnseenNotifications" 
                            ></i>
                        </div>
                    </v-tab>
                </vue-tabs>
            </div>
        </div>
    </div>
</script>