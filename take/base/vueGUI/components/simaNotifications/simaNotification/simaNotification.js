
/* global Vue, sima */

Vue.component('sima-notification', {
    template: '#sima-notification-template',
    props: {
        model: {
            validator: sima.vue.ProxyValidator
        }
    },
    data: function () {
        return {
            
        };
    },
    computed: {
        seen: function() {
            return this.model.seen;
        },
        real_seen: function() {
            return !sima.isEmpty(this.model.seen_time);
        },
        params: function() {
            return !sima.isEmpty(this.model.params) ? this.model.params : {};
        },
        has_params_info: function() {
            return !sima.isEmpty(this.notification_info_text);
        },
        notification_action: function() {
            return typeof this.params.action !== 'undefined' ? this.params.action: {};
        },
        notification_action_action: function() {
            return this.notification_action.action;
        },
        notification_action_params: function() {
            return this.notification_action.action_id;
        },
        notification_action_selector: function() {
            return this.notification_action.selector;
        },
        notification_classes: function(){
            return {
                _item: true,
                _seen: this.real_seen,
                _unseen: !this.real_seen
            };
        },
        is_notif_seen_prop_status_ok: function() {
            return this.model.shotStatus('seen') === 'OK';
        },
        notification_info_text: function() {
            var text = '';
            if (this.model.info)
            {
                text = this.model.info;
            }
            else
            {
                var params = this.model.params;
                text = typeof params.info !== 'undefined' ? params.info : '';
            }

            return text;
        }
    },
    watch: {
        seen: () => {},
        real_seen: () => {},
        notification_action_action: () => {},
        notification_action_params: () => {},
        notification_action_selector: () => {}
    },
    mounted: function() {
        var _this = this;
        $(this.$el).on('destroyed', function(){
            _this.$destroy();
        });
    },
    methods: {
        onClick: function() {
            if (!this.real_seen)
            {
                this.setNotificationSeen(true);
            }
            this.openNotification();
            
            this.$emit('open_notification');
        },
        seenToggle: function() {
            this.setNotificationSeen(!this.seen);
        },
        setNotificationSeen: function(seen) {
            var _this = this;
            
            var action = 'base/notification/NotifUnRead';
            if (seen)
            {
                action = 'base/notification/NotifRead';
            }
            this.model.seen = seen;
            this.model.seen_time = sima.date.format(Date.now(), true);
            sima.ajax.get(action, {
                get_params: {
                    id: this.model.id
                },
                async: true
            });
        },
        openNotification: function() {
            if(!sima.isEmpty(this.notification_action_action))
            {
                sima.openAction(this.notification_action_action, this.notification_action_params, this.notification_action_selector);
            }
        },
        getIconClass: function(icon_name) {
            var icon_class = '';
            
            if (icon_name === 'file')
            {
                icon_class = 'fas fa-file';
            }
            
            return icon_class;
        }
    }
});