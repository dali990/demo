<script type="text/x-template" id="sima-notification-template">
    <li v-bind:class="notification_classes" class="sima-notification"
        v-sima-tooltip="{
            title: notification_info_text,
            placement: 'right',
            classes: ['sima-notification-tooltip'],
            boundariesElement: 'viewport',
            closeIfElementNotVisible: true
        }"
    >
        <p class="_title" v-on:click="onClick">{{model.title}}</p>
        <p class="_info">
            <span>
                <span class="_icons"><span v-for="params_icon in params.icons" v-bind:class="getIconClass(params_icon)"></span></span>
                <span class="_time">{{model.create_time}}</span>
            </span>
            <input
                class="_check_box"
                v-bind:class="{_disabled: !is_notif_seen_prop_status_ok}"
                title="<?=Yii::t('BaseModule.Notification', 'MarkAsRead')?>" type="checkbox" v-bind:checked="model.seen" @click="seenToggle"
            />
        </p>
    </li>
</script>