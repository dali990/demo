
/* global Vue, sima */

Vue.component('sima-model-refresh', {
    template: '#sima-model-refresh',
    mixins: [sima.vue.getBaseComponent()],
    props: {
        model: {
            validator: sima.vue.ProxyValidator
        }
    },
    data: function () {
        return {
            
        };
    },
    computed: {
        dataTrue: function(){
            this.model.update_model_tag;
            return this.model.shotStatus('update_model_tag') === 'OK';
        }
    },
    mounted: function() {
    },
    methods: {
        onClick: function(event) {
            event.stopPropagation();
            sima.model.updateViews(this.model.update_model_tag);
        }
    }
});