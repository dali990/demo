<script type="text/x-template" id="sima-model-refresh">
    <sima-button 
        v-on:click="onClick"
        v-bind:class="{_disabled: !dataTrue}"
        title="<?= Yii::t('BaseModule.Common', 'Refresh')?>"
    >
        <span class="fas fa-sync-alt"></span>
    </sima-button>
</script>