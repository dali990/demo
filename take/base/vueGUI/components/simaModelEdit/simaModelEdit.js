
/* global Vue, sima */

Vue.component('sima-model-edit', {
    template: '#sima-model-edit',
    mixins: [sima.vue.getBaseComponent()],
    props: {
        model: {
            validator: sima.vue.ProxyValidator
        },
        params: {
            type: Object,
            function() {
                return {};
            }
        }
    },
    data: function () {
        return {
            
        };
    },
    computed: {
    },
    mounted: function() {
    },
    methods: {
        onClick: function(event) {
            event.stopPropagation();
            sima.model.form(this.model.get_class, this.model.id, this.params);
        }
    }
});