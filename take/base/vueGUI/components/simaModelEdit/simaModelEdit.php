<script type="text/x-template" id="sima-model-edit">
    <sima-button 
        v-on:click="onClick"
        title="<?= Yii::t('BaseModule.Common', 'Edit')?>"
    >
        <span class="sima-icon sil-edit"></span>
    </sima-button>
</script>