<script type="text/x-template" id="vue_perfect_scrollbar_template">
    <section class="ps-container" v-on:mouseover="update" v-on="$listeners">
        <slot></slot>
    </section>
</script>