
/* global Vue, sima */

Vue.component('vue-perfect-scrollbar', {
    template: '#vue_perfect_scrollbar_template',
    props: {
        settings: {
            type: Object,
            default: function() {
                return {};
            }
        }
    },
    computed: {},
    data: function () {
        return {
            ps: null,
            update_timeout: null
        };
    },
    watch: {
        $route() {
            this.update();
        },
        settings: function() {
            if (this.ps)
            {
                this.ps.settings = $.extend(true, this.ps.settings, this.settings);
            }
        }
    },
    mounted: function() {
        // for support ssr
        if (!this.$isServer) 
        {
            this.__init();
        }
    },
    updated: function() {
        this.$nextTick(this.update);
    },
    activated: function() {
        this.__init();
    },
    deactivated: function() {
        this.__uninit();
    },
    beforeDestroy: function() {
        this.__uninit();
    },
    methods: {
        update: function() {
            var _this = this;

            if (this.update_timeout !== null)
            {
                clearTimeout(this.update_timeout);
            }
            
            this.update_timeout = setTimeout(function() {
                if (_this.ps)
                {
                    _this.ps.update();
                }
            }, 300);
        },
        __init: function() {
            if (!this.ps)
            {
                this.ps = new PerfectScrollbar(this.$el, this.settings);
            }
        },
        __uninit: function() {
            if (this.ps)
            {
                this.ps.destroy();
                this.ps = null;
            }
        }
    }
});