/* global Vue, sima, mixin_search_simaSearchField */
Vue.component('sima-search-field', {
    template: '#sima-search-field',
    name: 'sima-search-field',
    inheritAttrs: false,
    props: {
        value: {default: () => ({ label: '', id: '' }) },
        model_name: { type: String, default: '' },
        model_filter: { type: Object, default() { return {}; } },
        search_min_char: { type: Number, default: 0 },
        settings: {default() { return {}; }},
        add_button: {default() { return true; }}, 
        add_new: {default: false},
        label: {type: String, default: 'DisplayName'},
    },
    data: function () {
        return {
            search: "",
            input_value: '', // Internal value managed by saf if no `value` prop is passed
            ssf_result_list: null, //sima_address_field_result_list dom object
            open_result_list: false,
            results: [],
            total: 0,
            selected_item: null,
            result_list_html: null,
            initial_text_search_value: null,
            initial_id_value: null,
            text_search_prev_value: null,
            id_prev_value: "",
            can_show_list: true, //dodatna provera prikaza rezultata zbog asinhrone pretrage (kada prodje fokus na drugo polje, on ipak prikaze listu zbog asinhronog poziva)
            ssf_bus: new Vue(),
            stop_async_render: false, //za stopiranje renderovanja result liste kada se okine blur nad poljem a ajax poziv se zavrsi kasnije
            select_cnt: 0,
            local_model_name: this.model_name,
            local_add_button: this.add_button
        };
    },
    computed: {
        scope() {
            return {
                events: {
                    'keydown': this.onKeyDown,
                    'keyup': this.onKeyUp,
                    'blur': this.onBlur,
                    'focus': this.onFocus
                }
            };
        },
        update_label(){
            if(this.add_new === "RETURN")
            {
                 if(
                        this.value !== null &&
                        (!this.value.hasOwnProperty('label') || this.value.label === "") && 
                        typeof this.value.id !== 'undefined' && this.value.id !== ""
                    )
                {
                    return true;
                }
            }
            else
            {
                if(this.value)
                {
                    return true;
                }
            }
            return false;
        },
        vue_model(){
            if(this.update_label)
            {
                var id = this.value;
                if(this.add_new === "RETURN")
                {
                    id = this.value.id;
                }
                return sima.vue.shot.getters.model(this.local_model_name+'_'+id);
            }
            return null;
        },
        display_label(){
            if(this.vue_model)
            {
                return this.vue_model[this.label];
            }
            return this.search;
        }
    },
    watch: {
        add_button(new_val, old_val){
            this.local_add_button = new_val;
        },
        display_label(new_val, old_val){
            this.updateLabel();
        },
        model_name: function(new_val, old_val) {
            if (new_val !== old_val)
            {
                this.local_model_name = new_val;
            }
        },
        value: function(new_val, old_val) {
            //parent je kasnije postavio value prop
            if(this.selected_item === null && this.initial_id_value === null)
            {
                if(new_val && new_val.id)
                {
                    this.selected_item = {id: new_val.id, label: new_val.label};
                }
                this.setInitialValue();
                this.updateLabel();
            }
            //update od parenta
            else
            {
                //ovo znaci da parent salje validan value prop za update (ima id i label)
                if(this.add_new === "RETURN")
                {
                    //za slucaj ako je poslat samo id (number)
                    if(!isNaN(new_val))
                    {
                        this.$emit('input', {id: new_val, label: ""});
                    }
                    else
                    {
                        this.selected_item = new_val;
                        this.updateField(this.selected_item);
                        this.$emit('input', this.selected_item);
                    }
                }                
            }
        },
    },
    created() {
        window.addEventListener('mousedown', this.onMouseDown);
    },
    mounted: function () {
        var _this = this;
        this.result_list_html = this.$refs.ssfResultListComponent.$el;
        var _result_list_html = this.result_list_html;
        
        this.ssf_result_list = document.body.appendChild(this.result_list_html);
        
        var _el = $(this.$el);
        _el.trigger('input');
        _el.on('destroyed', function () {
            _this.$destroy();
            _result_list_html.remove();
        });
        this.init();
    },
    destroyed() {
        if(this.ssf_result_list)
        {
            this.ssf_result_list.remove();
        }
        window.removeEventListener('mousedown', this.onMouseDown);
    },
    methods: {
        init(){
            this.setInitialValue();
            this.updateLabel();
                     
            this.ssf_bus.$on('bus_event_close_result_list', (data) => {
                if(this.ssf_result_list)
                {
                    this.open_result_list = false;
                }
            });
            this.ssf_bus.$on('bus_event_add_new', (data) => {
                this.$emit('input', {id:"", label: this.search});
                this.open_result_list = false;
            });
            var parent = this.$el.parentElement;
            var z_index = $(parent).zIndex();
            this.result_list_html.style.zIndex = ++z_index;
        },
        updateLabel(){
            if(this.vue_model && !sima.isEmpty(this.value))
            {
                var label = this.vue_model[this.label];
                this.selected_item = {label: label, id: this.vue_model.id};
                this.updateField(this.selected_item);
                if(this.selected_item)
                {
                    this.selected_item.label = label;
                    if(this.initial_text_search_value === null && this.selected_item.label)
                    {
                        this.initial_text_search_value = this.selected_item.label;
                    }
                }                
                this.emitUpdate();                
            }
        },
        setInitialValue(){
            if(this.value)
            {
                if (this.add_new === 'RETURN')
                {
                    if(!isNaN(this.value))
                    {
                        this.initial_id_value = this.value;
                        this.input_value = this.value;
                        this.selected_item = {id: this.value, label: ""};
                        this.emitUpdate();
                    }
                    else
                    {
                        if(this.value.hasOwnProperty('id') && this.value.hasOwnProperty('label'))
                        {
                            //ako ne stigne do ovde to znaci da parent jos nije postavio value prop
                            this.updateField(this.value);  
                            this.initial_text_search_value = this.value.label;
                            this.initial_id_value = this.value.id;//inicijalno postavljanje id bilo kog modela
                            this.selected_item = this.value;
                        }
                        else
                        {
                            this.selected_item = null;
                            this.initial_id_value = null;
                        }
                    }
                }
                else
                {
                    this.initial_id_value = this.value;
                    this.input_value = this.value;
                    this.selected_item = {id: this.value, label: null};
                }
            }
            else(!this.value)
            {
                this.initial_text_search_value = "";
            }
        },
        searchText() {
            var _this = this;
            this.results = [];
            
            var globalTimeout = this.search_timeout;
            if (globalTimeout !== null) {
                clearTimeout(globalTimeout);
                this.search_timeout = globalTimeout;
            }
            return new Promise((resolve, reject) => {
                _this.search_timeout = setTimeout(function () {
                    if (_this.search.length >= _this.search_min_char) 
                    {
                        var _model_filter = Object.assign(_this.model_filter, {text: _this.search.replace(/\,/g, '')});
                        sima.ajax.get('base/model/simaSearchModel', {
                            get_params: {
                                model_name: _this.local_model_name,
                            },
                            data: {
                                model_filter: _model_filter
                            },
                            async: true,
                            success_function: function (response) {
                                _this.results = response.result;
                                _this.total = response.total;
                                resolve(response);
                            },
                            error_function: function(response){
                                reject(response);
                            }
                        });
                    }
                    _this.renderResults();
                }, 500);
            });
        },
        renderResults() {
            if(!this.stop_async_render && this.isVisible())
            {
                var address_field_position = this.$el.getBoundingClientRect();
                this.ssf_result_list.style.top = address_field_position.top + 18 + "px";
                this.ssf_result_list.style.left = address_field_position.left + "px";        
                this.open_result_list = true;
            }
        },
        onKeyDown(e) {
            this.text_search_prev_value = e.target.value;
        },
        onKeyUp(e) {
            var _this = this;
            
            if(e.keyCode === 27)//esc
            {
                if(this.ssf_result_list && this.open_result_list)
                {
                    this.open_result_list = false;
                    e.stopPropagation();
                    e.preventDefault();
                }
                e.target.blur();
            }
            else if(e.keyCode === 9)//tab
            {
                
            }
            else if(e.keyCode === 13)//enter
            {
                if(this.ssf_result_list && this.open_result_list)
                {
                    e.stopPropagation();
                    e.preventDefault();
                }
            }
            else if(this.validKeyCodes(e.keyCode, e.ctrlKey, e.shiftKey))
            {
                if(this.search === "")
                {
                    this.emptyField();
                }
                else
                {
                    this.searchText().then(function(){
                        if(_this.add_new === "RETURN")
                        {
                            if(_this.selected_item && _this.selected_item.label === _this.search)
                            {
                                _this.$emit("input", _this.selected_item);
                            }
                            else
                            {
                                _this.$emit("input", {id: "", label: _this.search});
                            }
                        }
                    });
                }
                this.$emit('onKeyUp', e.target.value);
                this.$emit('update', {id: "", label: e.target.value});
            }
        },
        onMouseDown(e){
            if(!e.target.classList.contains("ssf-search-field"))
            {
                this.onClickOutside(e);
            }
        },
        onBlur(e) {
            if(e.relatedTarget)
            {
                this.open_result_list = false;
                this.stop_async_render = true; 
            }
        },
        onFocus(e) {
            this.stop_async_render = false;
            this.open_result_list = true;
            this.searchText();
        },
        onInput(e) {
            //this.$emit('input', {id: "", label: ""});
        },
        onClickOutside: function (e) {
            if(this.ssf_result_list)
            {
                if (
                        !e.target.closest(".sima-search-field") && !e.target.closest(".sima-search-field-result-list") && 
                        !this.isMe(e.target)
                    ) 
                {
                    this.open_result_list = false;
                    this.$emit('clickOutside', e);
                    if(this.search !== "" && this.add_new !== "RETURN")
                    {
                        if(this.selected_item && this.search !== this.selected_item.label)
                        {
                            this.undoField();
                        }
                        else if(!this.selected_item && this.input_value === "")
                        {
                            this.undoField();
                        }
                    }
                }
            }
        },
        selectedItem(i) {
            this.selected_item = {
                id: this.results[i].id,
                label: this.results[i].display_name
            };
            
            this.updateField(this.selected_item);
            this.emitUpdate();
            this.$emit('selected', this.selected_item);
            this.open_result_list = false;
        },
        openDialog(){
            var _this = this;
            var params = {};
            if(this.local_add_button)
            {
                if(this.local_add_button.action)
                {
                    this.$emit('openForm', {model_name: this.local_model_name});
                    var action = this.local_add_button.action;
                    params = this.local_add_button.params || {};
                    sima.dialog.openActionInDialogAsync(action, params);
                }
                else
                {
                    if(typeof this.local_add_button === 'object')
                    {
                        params = this.local_add_button;
                    }
                    this.$emit('openForm', {model_name: this.local_model_name});
                    params.view = "guiTable";
                    params.multiselect = false;
                    params.title = this.local_add_button.dialog_title || "";
                    params.fixed_filter = this.local_add_button.fixed_filter || {};
                    params.dialog_params = {
                        close_func: function(e){
                            _this.$emit('closedForm', {model_name: _this.local_model_name});
                        }
                    };

                    sima.model.choose(this.local_model_name, function(data){
                        _this.selected_item = {
                            id: data[0].id,
                            label: _this.htmlDecode(data[0].display_name)
                        };
                        _this.updateField(_this.selected_item);
                        _this.emitUpdate();
                        _this.$emit('selected', _this.selected_item);
                        _this.selected_item = {id: data[0].id, label: ""};
                        _this.$emit('savedForm', {model_name: _this.local_model_name, data: data});
                    }, params);
                }
            }          
        },
        openForm(){
            var _this = this;
            this.$emit('openForm', {model_name: this.local_model_name});
            var form_params = {
                onSave: function(response){
                    var id = response.model_id;
                    var attribute_name = _this.label;
                    var label = response.form_data[_this.local_model_name].attributes[attribute_name];
                    _this.selected_item = {id: id, label: label};
                    _this.updateField(label, id);
                    _this.emitUpdate();
                    _this.$emit('savedForm', {label: label, id: id, model_name: _this.local_model_name, data: response.form_data});
                },
                onClose: function(e){
                    _this.$emit('closedForm', {model_name: _this.local_model_name});
                }
            };
            if(typeof this.add_button === 'object')
            {
                if(this.add_button.params)
                {
                    Object.assign(form_params, this.add_button.params);
                }
            }
            sima.model.form(this.local_model_name, '', form_params);
        },
        emitUpdate(){
            if(this.add_new !== "RETURN" && this.selected_item)
            {
                this.$emit('input', this.selected_item.id);
            }
            else
            {
                this.$emit('input', this.selected_item);
            }
        },
        updateField(label, id) {
            if(typeof label === "object")
            {
                this.updateFieldFromVModel(label);
            }
            else
            {
                this.search = label;
                this.input_value = id;
            }
        },
        updateFieldFromVModel(v_model){
            var label = "";
            var id = v_model.id;
            if(v_model.hasOwnProperty('label'))
            {
                label = v_model.label;
            }
            this.search = label;
            this.input_value = id;
        },
        hasSearchTextChanged(){
            return this.text_search_prev_value !== this.search || false;
        },
        isMe(el){
            var _el = null;
            if(el.tagName === "INPUT")
            {
                _el = el.parentElement.querySelector('input[type="hidden"]');
            }
            return (_el && _el.name && (_el.name === this.$attrs.name)) || false;
        },
        validKeyCodes(keyCode, ctrl, shift){
            if(
                (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (ctrl === false && (keyCode >= 65 && keyCode <= 90)) || 
                shift || 
                keyCode === 46 || keyCode === 8 ||
                (ctrl === true && (keyCode === 86 || keyCode === 88 || keyCode === 90)) //paste,cut,undo
            )
            {
                return  true;
            }
            return false;
        },
        isVisible(){
            if(this.$refs.searchInptuField)
            {
                return !!( 
                    this.$refs.searchInptuField.offsetWidth || 
                    this.$refs.searchInptuField.offsetHeight || 
                    this.$refs.searchInptuField.getClientRects().length ||
                    this.$refs.searchInptuField.offsetParent
                );
            }
            return false;
        },
        getId(){
            if(this.value)
            {
                if(this.add_new === 'RETURN' && this.value.hasOwnProperty('id'))
                {
                    return this.value.id;
                }
                else
                {
                    return this.value;
                }
            }
            return null;
        },
        undoField(){
            //Kada se obrise ceo inptu
            if(this.search === "")
            {
                this.input_value = "";
                this.selected_item = null;
            }
            //Vraca na prethodno dodatog partnera
            else if(this.selected_item !== null && this.selected_item.id)
            {
                this.search = this.selected_item.label;
                this.input_value = this.selected_item.id;
            }
            //Vraca na inicijalno postavljene podatke
            else
            {
                if(this.initial_text_search_value !== this.search)
                {
                    this.search = this.initial_text_search_value || "";
                    this.input_value = this.initial_id_value;
                    if(this.add_new === "RETURN")
                    {
                        this.selected_item = {id:"", label:""};
                    }
                    else
                    {
                        this.selected_item = null;
                    }
                }
            }
        },
        revertField(){
            this.search = this.initial_text_search_value;
            this.input_value = this.initial_id_value;
            this.selected_item = {id:this.input_value, label:this.initial_text_search_value};
            this.emitUpdate();
            this.$emit('selected', this.selected_item);            
        },
        htmlDecode(input) {
            var doc = new DOMParser().parseFromString(input, "text/html");
            return doc.documentElement.textContent;
        },
        setModelName: function(new_model_name) {
            this.local_model_name = new_model_name;
        },
        setAddButton: function(new_add_button){
            this.local_add_button = new_add_button;
        },
        emptyField() {
            this.search = "";
            this.input_value = "";
            this.open_result_list = false;
            this.selected_item = null;
            if(this.add_new === "RETURN")
            {
                this.$emit('input', {id:"",label:""});
                this.$emit('selected', {id:"",label:""});
            }
            else
            {
                this.$emit('input', null);
                this.$emit('selected', this.selected_item);
            }
        }
    }
});