<script type="text/x-template" id="sima-search-field">
    <div class="sima-search-field" v-bind:class="{'_disabled': settings.disabled}" v-click-outside="onClickOutside">
        <input
            class="ssf-search-field"
            v-bind:placeholder="$attrs.placeholder"
            v-model="search"
            v-on="scope.events"
            ref="searchInptuField"
            v-bind:disabled="settings.disabled"
        />
        
        <sima-button v-if="add_new == true" v-on:click="openForm" class="ssf-form-btn">
            <span class="sima-icon sil-plus"></span>
        </sima-button>
        <span class="sima-icon _search_form _16" 
            v-if="local_add_button"
            @click="openDialog">
        </span>
        
        <input type="hidden" v-bind:name="$attrs.name" v-bind:value="input_value" ref="inputValue" class="sima-search-field-input-value"/>
        
        <sima-search-field-result-list ref="ssfResultListComponent"             
            v-show="open_result_list"
            v-bind:results="results"
            v-bind:search="search"
            v-bind:ssf_bus="ssf_bus"
            v-bind:model_name="model_name"
            v-bind:total="total"
            v-bind:add_new="add_new"
            @selectedItem="selectedItem">
        </sima-search-field-result-list>
        
    </div>
</script>