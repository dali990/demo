/* global Vue, sima, tinymce, this */

Vue.component('tinymce', {
    template: '#tinymce_template',
    props:  {
        value: {
            type: String,
            default: ''
        },
        read_only: {
            type: Boolean,
            default: false
        },
        tinymce_params: {
            type: Object,
            default: function(){return {}; }
        } 
    },
    computed: {},
    data: function(){
        return {
            editor: null,
            params: {
                branding: false,
                read_only: false,
                nonbreaking_force_tab: true,
                relative_urls: false,
                remove_script_host: false,
                fullscreen_new_window: true,
                media_use_script: true,
                default_link_target: '_blank',
                paste_as_text: true,
                image_dimensions: false,
                images_upload_url: 'index.php?r=files/transfer/uploadTinymceImage',
                extended_valid_elements: 'img[src|class|style|height|width|onclick],span[onclick|class|style]',
                plugins: 'paste',
                image_source_disabled: true,
                image_max_width: 200,
                image_max_height: 100,               
                target_list: false, //prikaz opcija za otvaranje linka
                anchor_top: true,
                anchor_bottom: true,
                elementpath: true,
                statusbar: true,
                image_description: false,
                setup_handler: null
            },
            initial_value: null //desava se ponekad da se tinymce editor jos uvek nije inicijalizovao a pozove se watch value, odnosno dodje do izmene vrednosti.
                                //U ovu promenljivu se pamti vrednost u takvom slucaju i prilikom inicijalizacije editora postavlja se ta vrednost
        };
    },
    destroyed: function() {
        if (this.editor !== null)
        {
            this.editor.remove();
        }
    },
    mounted: function(){
        var _this = this;
        $(this.$el).on('destroyed', function(){
            _this.$destroy();
        });
        
        _this.params.target = _this.$el;
        Object.assign(_this.params, sima.getTinymceDefaultParams());
        Object.assign(_this.params, _this.tinymce_params);//Object.assign(a,b) b overrides a
        _this.params.setup = function (editor) {
            _this.addButtonEuroSign(editor);
            if(typeof _this.params.setup_handler === 'function')
            {
                _this.params.setup_handler(editor);
            }
            else if (typeof _this.params.setup_handler === 'string')
            {
                var setup_handler_func = eval(_this.params.setup_handler);
                setup_handler_func(editor);
            }
        };
        _this.params.paste_preprocess = function(plugin, args) {
            var regex_image_extensions = sima.getTinymceDefaultParams().image_extensions.replace(/,/g, '|');
            var regex_rule = new RegExp("(http(s?):|www)([\/|.|\\w|\\s|-])*\\.(?:"+regex_image_extensions+")");
            if(regex_rule.test(args.content))
            {
                var link = '<a href="'+args.content+'" target="_blank" rel="noopener" data-mce-selected="inline-boundary">'+args.content+'</a>';
                args.content = link;                    
            }
        };
        if(_this.read_only)
        {
            _this.params.menubar = false;
            _this.params.toolbar = false;
            _this.params.statusbar = false;
        }
        _this.params.init_instance_callback = function(editor){
            _this.editor = editor;

            _this.tinymceSetHandlers(editor);
            
            if (_this.initial_value !== null)
            {
                editor.setContent(_this.initial_value);
                _this.initial_value = null;
            }
            
            _this.focusTinymce();
            _this.setTinymceCursorOnEnd();

            _this.$emit('afterInitTinymce');
            sima.layout.allignObject($(_this.$el).parents('.sima-layout-panel:first').parent());
        };
        $(document).ready(function(){
            tinymce.init(_this.params);
        });
        
    },
    watch: {
        value: function(new_val, old_val){
            if (new_val !== old_val)
            {
                if (this.editor === null)
                {
                    this.initial_value = new_val;
                }
                else if(new_val !== this.$el.value)
                {
                    this.editor.setContent(new_val);
                    //Vraca kursor na kraju. Posto setContent postavlja kursor na pocetak
                    this.setTinymceCursorOnEnd();
                }
            }
        }
    },
    methods: {
        tinymceSetHandlers: function(editor){
            var _this = this;
            
            editor.dom.loadCSS(_this.params.content_css_url);
            
            editor.on('focus', function(e) {
                _this.$emit('focus');
            });
            
            editor.on('blur', function(e) {
                _this.$emit('blur');
            });
            
            var on_change_timeout = null;
            editor.on('keyup undo redo change paste', function () {
                var editor_this = this;
                if (on_change_timeout !== null)
                {
                    clearTimeout(on_change_timeout);
                }
                on_change_timeout = setTimeout(function() {
                    if(_this.$el.value !== editor.getContent())
                    {
                        editor_this.save();
                        _this.$emit('input', editor.getContent());
                    }
                }, 500);
            });
            
            editor.on('SetContent', function (e) {
                editor.fire('change');
            });
    
            editor.settings.images_upload_handler = function(blobInfo, success, failure) {          
                var file = blobInfo.blob();
                var extensions = _this.params.image_extensions.split(",");
                if (file.type !== "" && file.type.indexOf("/") !== -1 && extensions.indexOf(file.type.split("/")[1]) !== -1)
                {
                    var formData = new FormData();
                    formData.append('file', blobInfo.blob(), blobInfo.filename());           
                    formData.append("upload_file", true);

                    if(blobInfo.blob().size > _this.params.max_file_size_upload_bytes)
                    {
                        var size_mb = _this.params.max_file_size_upload_mb;
                        failure(sima.translate('FileSizeOver')+size_mb+"MB");
                    }
                    else
                    {
                        //sima.ajax ne prolazi zato sto je formData fajl, u $.ajax postavlja se processData i contentType na false kako bi prosao formData 
                        $.ajax({
                            url : _this.params.images_upload_url,
                            type : 'POST',
                            data : formData,
                            processData: false,  // tell jQuery not to process the data
                            contentType: false,  // tell jQuery not to set contentType
                            dataType: 'JSON',
                            success : function(response) {
                                if(response.response.location)
                                {
                                    success(response.response.location);
                                }
                                else
                                {
                                    failure(response.response.error);
                                }
                            }
                        });
                    }
                }
                else
                {
                    failure(sima.translate('NotSupportedImageExtension'));
                }           
            };

            editor.on('nodeChange', function(e){
                var node_name = e.element.nodeName.toLowerCase();
                if (e && node_name === 'img') 
                {
                    _this.setImageFileParams(editor, e);
                    _this.setImageRatio(editor, e);
                }
                else if (e && node_name === 'a') 
                {
                    _this.setLinkTarget(editor, e);
                }
            });

            var parent_layout_div = $(this.$el).parents('.sima-layout-panel:first');
            if (!sima.isEmpty(parent_layout_div))
            {
                parent_layout_div.on('sima-layout-allign', function() {
                    editor.fire('ResizeWindow');
                });
            }
        },
        addButtonEuroSign: function(editor){
            editor.addButton('euro-sign', {
                text: '',
                icon: 'eur-icon',
                classes: ' bill-item-eur-icon',
                tooltip: sima.translate('InsertEuroSign'),
                onclick: function () {
                  editor.insertContent('&nbsp;&euro;&nbsp;');
                }
            });
        },
        setTinymceCursorOnEnd: function() {
            if (this.editor !== null)
            {
                this.editor.selection.select(this.editor.getBody(), true);
                this.editor.selection.collapse(false);
            }
        },
        focusTinymce: function() {
            if (this.editor !== null && this.editor.initialized)
            {
                tinymce.execCommand('mceFocus', false, this.$el);
            }
        },
        getContent: function() {
            if (this.editor !== null)
            {
                return this.editor.getContent();
            }
            
            return '';
        },
        insertContent: function(content) {
            if (this.editor !== null && this.editor.initialized)
            {
                tinymce.execCommand('mceInsertContent', false, content + "&nbsp;");
            }
        },
        setImageFileParams(editor, e){
            if(e.element.hasAttribute('data-file-preview-params') === false)
            {
                var url = new URL(e.element.src);
                var file_version_id = url.searchParams.get("file_version_id");
                var params = { 
                    get_params: {
                        model_name: "FileVersion",
                        model_id: file_version_id
                    }
                };
                editor.dom.setAttribs(e.element, {
                    'data-file-preview-params': JSON.stringify(params),
                    'class': 'sima-image-file-preview'
                });
            }
        },
        setImageRatio: function(editor, e){
            var _this = this;
            var style = {};
            if(e.element.width === 0)
            {
                setTimeout(function(){
                    _this.setImageRatio(editor, e);
                },100);
            }
            else
            {   
                var max_width = editor.settings.image_max_width;
                var max_height = editor.settings.image_max_height;
                var img_width_factor = e.element.width / max_width;
                var img_height_factor = e.element.height / max_height;
                if(e.element.hasAttribute('style') === false)
                {
                    if(img_width_factor > 1 && img_width_factor > img_height_factor)
                    {
                        style = {'width': max_width+'px', 'height': 'auto'};
                    } 
                    else if (img_height_factor > 1 && img_height_factor >= img_width_factor)
                    {
                        style = {'height': max_height+'px', 'width': 'auto'};
                    }

                    editor.dom.setAttribs(e.element, {
                        'style': style
                    });
                    editor.fire('change');
                }
            }
        },
        setLinkTarget: function(editor, e){
            if(e.element.hasAttribute('target') === false)
            {
                editor.dom.setAttribs(e.element, {                   
                    'target': '_blank'
                });
            }
        }
    }
});