
/* global Vue, sima */

Vue.component('sima-model-recycle', {
    template: '#sima-model-recycle',
    mixins: [sima.vue.getBaseComponent()],
    props: {
        model: {
            validator: sima.vue.ProxyValidator
        }
    },
    data: function () {
        return {
            
        };
    },
    computed: {
    },
    mounted: function() {
    },
    methods: {
        onClick: function(event) {
            event.stopPropagation();
            sima.model.recycle(this.model.get_class, this.model.id);
        }
    }
});