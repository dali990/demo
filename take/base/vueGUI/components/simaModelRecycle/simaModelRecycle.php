<script type="text/x-template" id="sima-model-recycle">
    <sima-button 
        v-on:click="onClick"
        title="<?= Yii::t('BaseModule.Common', 'Recycle')?>"
    >
        <span class="sima-icon sil-recycle"></span>
    </sima-button>
</script>