
/* global Vue, sima */

Vue.component('sima-model-display-html', {
    template: '#sima-model-display-html',
    mixins: [sima.vue.getBaseComponent()],
    props: {
        model: {
            validator: sima.vue.ProxyValidator
        },
        display_name: String,
        title: String
    },
    data: function () {
        return {
            
        };
    },
    computed: {
        name_classes: function() {
            return {
                _clickable: this.has_model_display || !sima.isEmpty(this.model_path2),
                _ctrl: sima.getModelDisplayhtmlClickOpenConf() === 'ctrl+klik'
            };
        },
        model_name: function() {
            return this.model.get_class;
        },
        has_model_display: function() {
            return this.model.has_display;
        },
        model_path2: function() {
            return this.model.path2;
        },
        calc_display_name: function() {
            return !sima.isEmpty(this.display_name) ? this.display_name : this.model.DisplayName;
        },
        calc_title: function() {
            return !sima.isEmpty(this.title) ? this.title : this.model.DisplayName;
        }
    },
    watch: {
        model_name: () => {},
        has_model_display: () => {},
        model_path2: () => {}
    },
    mounted: function() {
    },
    methods: {
        onNameClick: function(event) {
            
            var action = '';
            var action_id = '';

            if (this.has_model_display)
            {
                action = 'defaultLayout';
                action_id = {
                    model_name: this.model_name,
                    model_id: this.model.id
                };
            }
            else if (!sima.isEmpty(this.model_path2))
            {
                action = this.model_path2;
                action_id = this.model.id;
            }
            
            if (action !== '' && action_id !== '')
            {
                sima.dialog.openModel(action, action_id, null, {displayhtml: true, event: event});
            }
        }
    }
});