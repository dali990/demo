<script type="text/x-template" id="sima-model-display-html">
    <div 
        class="sima-model-display-html"
        v-bind:class="name_classes"
        v-bind:title="calc_title"
        v-on:click="onNameClick"
    >
        {{ calc_display_name }}
    </div>
</script>