<script type="text/x-template" id="sima-model-default-layout">
    <div
        class="sima-model-default-layout sima-layout-panel _horizontal"
    >
        <sima-action-notifications
            class="sima-model-default-layout-notifs sima-layout-fixed-panel"
            v-bind:actions="notification_actions"
        ></sima-action-notifications>
        <div class="sima-model-default-layout-warns sima-layout-fixed-panel" v-html="this.model.default_layout_model_warns">
            
        </div>
        <component 
            v-bind:is="this.basic_info_component_name"
            v-bind:model="this.model"
            class="sima-model-default-layout-basic-info sima-layout-fixed-panel"
        >
        </component>
        <sima-tabs
            ref="tabs"
            class="sima-model-default-layout-tabs"
            v-bind:tabs="this.default_layout_model_tabs"
            v-bind:default_selected="this.local_default_selected_tab"
        >
                    
        </sima-tabs>
    </div>
</script>