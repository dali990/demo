
/* global Vue, sima */

Vue.component('sima-model-default-layout', {
    template: '#sima-model-default-layout',
    mixins: [sima.vue.getBaseComponent()],
    props: {
        model: {
            validator: sima.vue.ProxyValidator
        },
        //nije implementirano
        ignore_tabs: {
            type: Array,
            default: function() {
                return [];
            }
        },
        default_selected_tab: {
            type: [String, Object, Array],
            default: function() {
                return 'default_layout_full_info';
            }
        }
    },
    data: function () {
        return {
            local_default_selected_tab: this.default_selected_tab
        };
    },
    watch: {
        model_name: () => {},
        display_action: () => {},
        path2: () => {}
    },
    computed: {
        default_layout_model_tabs: function() {
            var _default_layout_model_tabs = this.model.default_layout_model_tabs;

            if (!sima.isEmpty(_default_layout_model_tabs))
            {
                return _default_layout_model_tabs;
            }
            else
            {
                return [];
            }
        },
        module_name: function() {
            return this.model.module_name;
        },
        model_name: function() {
            return this.model.get_class;
        },
        display_action: function() {
            return this.model.display_action;
        },
        path2: function() {
            return this.model.path2;
        },
        basic_info_component_name: function() {
            var component_name = 'sima-model-fake-basic_info';

            if (!sima.isEmpty(this.module_name) && !sima.isEmpty(this.model_name))
            {
                component_name = this.module_name + '-' + (this.model_name).toLowerCase() + '-basic_info';
            }

            return component_name;
        },
        notification_actions: function() {
            return [
                {
                    action: this.display_action,
                    action_id: {
                        model_name: this.model_name,
                        model_id: this.model.id
                    }
                },
                {
                    action: this.path2,
                    action_id: this.model.id
                }
            ];
        }
    },
    created: function() {
        if (sima.isEmpty(this.local_default_selected_tab))
        {
            this.local_default_selected_tab = 'default_layout_full_info';
        }
    },
    updated: function() {
        var _this = this;

        this.$nextTick(function() {
            sima.layout.allignObject($(_this.$el));
        });
    },
    mounted: function() {
        var _this = this;

        this.$refs.tabs.$on('afterTreeItemSelectionChanged', function(tree_item, selection_type) {
            if (selection_type === 'UNSELECTED')
            {
                _this.$refs.tabs.selectTabByCode('default_layout_full_info');
            }
        });

        this.$nextTick(function() {
            sima.layout.allignObject($(_this.$el));
        });
    },
    methods: {
        
    }
});