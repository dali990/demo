
/* global Vue, sima */

Vue.component('sima-model-fake-basic_info', {
    template: '#sima-model-fake-basic_info',
    mixins: [sima.vue.getBaseComponent()],
    props: {
        model: {
            validator: sima.vue.ProxyValidator
        }
    },
    data: function () {
        return {};
    },
    computed: {
        
    },
    mounted: function(){
    },
    methods: {
        
    }
});