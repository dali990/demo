<script type="text/x-template" id="sima-model-delete">
    <sima-button
        v-on:click="onClick"
        title="<?= Yii::t('BaseModule.Common', 'Delete')?>"
    >
        <span class="sima-icon sil-trash-alt"></span>
    </sima-button>
</script>