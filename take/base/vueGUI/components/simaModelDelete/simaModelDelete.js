
/* global Vue, sima */

Vue.component('sima-model-delete', {
    template: '#sima-model-delete',
    mixins: [sima.vue.getBaseComponent()],
    props: {
        model: {
            validator: sima.vue.ProxyValidator
        }
    },
    data: function () {
        return {
            
        };
    },
    computed: {
        model_name: function() {
            return this.model.get_class;
        }
    },
    watch: {
        model_name: () => {}
    },
    mounted: function() {
    },
    methods: {
        onClick: function(event) {
            event.stopPropagation();
            sima.model.remove(this.model_name, this.model.id);
        }
    }
});