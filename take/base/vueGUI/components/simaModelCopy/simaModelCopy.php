<script type="text/x-template" id="sima-model-copy">
    <sima-button 
        v-on:click="onClick"
        title="<?= Yii::t('BaseModule.Common', 'Copy')?>"
    >
        <span class="sima-icon sil-copy"></span>
    </sima-button>
</script>