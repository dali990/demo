
/* global Vue, sima */

Vue.component('sima-model-copy', {
    template: '#sima-model-copy',
    mixins: [sima.vue.getBaseComponent()],
    props: {
        model: {
            validator: sima.vue.ProxyValidator
        }
    },
    data: function () {
        return {
            
        };
    },
    computed: {
    },
    watch: {
    },
    mounted: function() {
    },
    methods: {
        onClick: function(event) {
            event.stopPropagation();
            sima.icons.copyModel(this.model.get_class, this.model.id);
        }
    }
});