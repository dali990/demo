<script type="text/x-template" id="sima-addressbook">
    <div class="sima-addressbook sima-layout-panel" v-observe-visibility="componentInstanceVisibilityChanged">
        <div class="sima-addressbook-overlay" ref="overlay" v-show="show_overlay"></div>
        <div class="addressbook-header sima-layout-fixed-panel">
            <div class="addressbook-header-wrap">
                <h2 class="sima-page-title"><?=Yii::t('BaseModule.SIMAAddressbook', 'Addressbook')?></h2>
                <div class="search-field">
                    <span class="sima-icon input-icon sil-search" v-bind:style="{'z-index':z_index_after_overlay+1}"></span>
                    <input class="addressbook-search-input"
                        v-bind:placeholder="$attrs.placeholder"
                        v-model="partner_search_text"
                        spellcheck="false"
                        ref="searchInptuField"
                        v-bind:class="{'_disabled': disabled, 'has-text': !isEmptySearch, 'open-search':isOpenSearchFielter}"
                        placeholder="<?=Yii::t('BaseModule.SIMAAddressbook', 'PlaceholderSearchAddressbook')?>"
                    />
                    <span v-show="!isEmptySearch" @click="clearSearch" class="clear-search input-icon sima-icon sil-times" v-bind:style="{'z-index':z_index_after_overlay}"></span>
                    <span @click.stop="toggleSearch" class="input-icon sima-icon sil-sliders-h" :class="{'open-search': isOpenSearchFielter}" style="right: 0" v-bind:style="{'z-index':z_index_after_overlay}"></span>
                </div>
                <sima-button v-on:click="openCompanyForm">
                    <i class="sima-icon sil-building-plus"></i><hr>
                    <span class="sima-btn-title"><?=Yii::t('BaseModule.SIMAAddressbook', 'AddCompany')?></span>
                </sima-button>
                <sima-button v-on:click="openPersonForm">
                    <i class="sima-icon sil-user-plus"></i><hr>
                    <span class="sima-btn-title"><?=Yii::t('BaseModule.SIMAAddressbook', 'AddPerson')?></span>
                </sima-button>
                <sima-button-group class="main-icon-only" ref="simaButtonGroup">
                    <span slot="main_icon" class="sima-btn-icon sima-icon sil-cog"></span>
                    <sima-button v-on:click="openAddresses">
                        <?=Yii::t('GeoModule.Address', 'Addresses')?>
                    </sima-button>
                    <sima-button v-on:click="openPostalCodes">
                        <?=Yii::t('GeoModule.Street', 'Streets')?>
                    </sima-button>
                    <sima-button v-on:click="openStreets">
                        <?=Yii::t('GeoModule.PostalCode', 'PostalCodes')?>
                    </sima-button>
                    <sima-button v-on:click="openCities">
                        <?=Yii::t('GeoModule.City', 'Cities')?>
                    </sima-button>
                    <sima-button v-on:click="openMunicipalities">
                        <?=Yii::t('GeoModule.City', 'Municipalities')?>
                    </sima-button>
                    <sima-button v-on:click="openCountries">
                        <?=Yii::t('GeoModule.Country', 'Countries')?>
                    </sima-button>
                    <sima-button v-on:click="openWorkCodes">
                        <?=Yii::t('CodebooksModule.Codebooks', 'WorkCodes')?>
                    </sima-button>
                    <sima-button v-on:click="openPartnerLists">
                        <?=Yii::t('BaseModule.PartnerList', 'PartnerLists')?>
                    </sima-button>
                    <sima-button v-on:click="openContactTypes">
                        <?=Yii::t('Contact', 'ContactTypes')?>
                    </sima-button>
                </sima-button-group>
            </div><!-- end addressbook-header-wrap -->
            <sima-partner-search-filter 
                ref="simaPartnerSearch"
                :class="{'show-search-filter':show_search_filter}"
                v-bind:event_bus="event_bus"
                @search="partnerSearchFilter">
            </sima-partner-search-filter>
        </div><!-- end addressbook-header -->

        <div class="addressbook-body sima-layout-panel _splitter _vertical">
            <div class="sima-layout-panel" data-sima-layout-init='{"proportion":0.5}'>
                <vue-tabs class="addressbook-companies sima-layout-panel"  scroll
                    @tab-change="companyTabChange"
                    ref="companiesTabs">
                    <!-- Sve firme -->
                    <v-tab title="<?=Yii::t('BaseModule.SIMAAddressbook', 'AllCompanies')?>" 
                        v-bind:tabName="'companies'"
                        selectOnMount="true"
                        :counter="companies_all_cnt">
                        <vue-perfect-scrollbar class="perfect-scrollbar"                      
                            v-bind:settings="{ minScrollbarLength: 30, suppressScrollX: true }">
                            <sima-partner-list v-if="new_added_companies.length"
                                v-bind:model_list.sync="new_added_companies" 
                                v-bind:type="'company'">
                            </sima-partner-list>
                            <sima-partner-list 
                                v-bind:model_list="companies"
                                v-bind:type="'company'"
                                v-bind:list_count="companies_all_cnt"
                                @removedListItem="removeListItem('company', $event)"
                                @load_more="loadMoreCompanies('companies')">
                            </sima-partner-list>
                        </vue-perfect-scrollbar>
                    </v-tab>
                    <!-- Sve dostupne liste za prikaz korisniku -->
                    <v-tab v-for="partner_list in company_partner_lists" :key="partner_list.id"
                        :title="partner_list.name" 
                        v-bind:tabName="partner_list.code ? partner_list.code : partner_list.id"
                        :counter="comapnies_counter[partner_list.id]">
                        <vue-perfect-scrollbar class="perfect-scrollbar" 
                            v-bind:settings="{ minScrollbarLength: 30 }">
                            <sima-partner-list 
                                v-bind:list_count="comapnies_counter[partner_list.id]"
                                v-bind:limit="limit"
                                v-bind:model_list="companies" 
                                v-bind:type="'company'"
                                @load_more="loadMoreCompanies('companies')">
                            </sima-partner-list>
                        </vue-perfect-scrollbar>
                    </v-tab>
                </vue-tabs><!-- end addressbook-companies -->
            </div>
            <div class="sima-layout-panel" data-sima-layout-init='{"proportion":0.5}'>
                <vue-tabs class="addressbook-persons sima-layout-panel"  scroll
                    @tab-change="personTabChange"
                    ref="personsTabs">
                    <!-- Sve osobe -->
                    <v-tab title="<?=Yii::t('BaseModule.SIMAAddressbook', 'AllPersons')?>" 
                        :counter="persons_all_cnt"
                        v-bind:tabName="'persons'">
                       <vue-perfect-scrollbar class="perfect-scrollbar" 
                            v-bind:settings="{ minScrollbarLength: 30, suppressScrollX: true }">
                            <sima-partner-list v-if="new_added_persons.length"
                                v-bind:model_list.sync="new_added_persons" 
                                v-bind:type="'person'">
                            </sima-partner-list>
                            <sima-partner-list 
                                v-bind:list_count="persons_all_cnt"
                                v-bind:model_list="persons" 
                                v-bind:type="'person'"
                                @removedListItem="removeListItem('person', $event)"
                                @load_more="loadMorePersons('persons')">
                            </sima-partner-list>
                        </vue-perfect-scrollbar>
                    </v-tab>
                    <!-- Sve dostupne liste za prikaz korisniku -->
                    <v-tab v-for="partner_list in person_partner_lists" :key="partner_list.id"
                        :title="partner_list.name" 
                        v-bind:tabName="partner_list.code ? partner_list.code : partner_list.id"
                        :counter="persons_counter[partner_list.id]">
                        <vue-perfect-scrollbar class="perfect-scrollbar" 
                            v-bind:settings="{ minScrollbarLength: 30 }">
                            <sima-partner-list 
                                v-bind:list_count="persons_counter[partner_list.id]"
                                v-bind:limit="limit"
                                v-bind:model_list="persons" 
                                v-bind:type="'person'"
                                @load_more="loadMorePersons('users')">
                            </sima-partner-list>
                        </vue-perfect-scrollbar>
                    </v-tab>
                </vue-tabs><!-- end addressbook-companies -->
            </div>
        </div><!-- end addressbook-body --> 
    </div><!-- end sima-addressbook --> 
</script>

