<script type="text/x-template" id="sima-company-employee-list">
    <div class="sima-company-employee-list" v-observe-visibility="componentInstanceVisibilityChanged">
        <div v-if="model_list.length == 0"><?=Yii::t('SIMAAddressbook.SIMAAddressbook', 'NoPersonFound')?></div>
        <div v-else>
            <sima-partner-list :model_list="model_list" :type="'person'"></sima-partner-list>
            <sima-button class="cta" v-show="'model_list_cnt > limit'" :class="{'_disabled':model_list_cnt <= limit}" 
                @click="loadMore">
                <i class="sima-icon sil-long-arrow-down" ref="btn_icon"></i><hr>
                <span class="sima-btn-title"><?=Yii::t('SIMAAddressbook.SIMAAddressbook', 'LoadMore')?></span>
            </sima-button>
        </div>
    </div>
</script>

