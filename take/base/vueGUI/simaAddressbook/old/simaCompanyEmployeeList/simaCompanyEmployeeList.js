
/* global Vue, sima */

Vue.component('sima-company-employee-list', {
    template: '#sima-company-employee-list',
    mixins: [sima.vue.getBaseComponent()],
    props: {
        list_count: {type: Number, default: 0},
        company: {
            validator: sima.vue.ProxyValidator
        }
    },
    computed: {
        model_list_cnt(){
            return sima.vue.shot.getters.model_list_count(this, 'Person', {
                order: 'display_name',
                model_filter: {
                    limit: this.limit,
                    company: {ids: this.company.id}
                }
            });
        },
        model_list(){
            return sima.vue.shot.getters.model_list(this, 'Person', {
                order: 'display_name',
                model_filter: {
                    limit: this.limit,
                    company: {ids: this.company.id}
                }
            });
        }
    },
    watch: {
    },
    data: function () {
        return {
            limit: 30
        };
    },
    mounted: function() {       
        
    },
    methods: {
        loadMore(e){
            var arrow_down = this.$refs.btn_icon;
            if(arrow_down)
            {
                arrow_down.classList.remove('sil-long-arrow-down');
                arrow_down.classList.add('sil-spinner');
                arrow_down.classList.add('fa-spin');
            }
            this.limit += 10;
        }
    }
});