<script type="text/x-template" id="sima-partner-search-filter">
    <div class="sima-partner-search-filter">
        <div class="company-search">
            <div>
                <span><?=Yii::t('SIMAAddressbook.SIMAAddressbook', 'CompanyName')?></span>
                <input 
                    type="text"
                    spellcheck="false"
                    v-model="Company.name"
                    @input="searchPartner"
                    Placeholder="<?=Yii::t('SIMAAddressbook.SIMAAddressbook', 'PlaceholderSearchAddressbook')?>"
                />
                <span v-show="Company.name != ''" @click="Company.name = '';searchPartner() " class="sima-icon sil-times"></span>
            </div>
            <div>
                <span><?=Yii::t('SIMAAddressbook.SIMAAddressbook', 'PIB')?></span>
                <input
                    type="text"
                    spellcheck="false"
                    v-model="Company.PIB"
                    @input="searchPartner"
                    Placeholder="<?=Yii::t('SIMAAddressbook.SIMAAddressbook', 'PlaceholderSearchAddressbook')?>"
                />
                <span v-show="Company.PIB != ''" @click="Company.PIB = '';searchPartner() " class="sima-icon sil-times"></span>
            </div>
            <div>
                <span><?=Yii::t('SIMAAddressbook.SIMAAddressbook', 'MB')?></span>
                <input 
                    type="text"
                    spellcheck="false"
                    v-model="Company.MB"
                    @input="searchPartner"
                    Placeholder="<?=Yii::t('SIMAAddressbook.SIMAAddressbook', 'PlaceholderSearchAddressbook')?>"
                />
                <span v-show="Company.MB != ''" @click="Company.MB = '';searchPartner() " class="sima-icon sil-times"></span>
            </div>
            <div>
                <span><?=Yii::t('SIMAAddressbook.SIMAAddressbook', 'ActivityCode')?></span>
                <input 
                    type="text"
                    spellcheck="false"
                    v-model="Company.work_code.code"
                    @input="searchPartner"
                    Placeholder="<?=Yii::t('SIMAAddressbook.SIMAAddressbook', 'PlaceholderSearchAddressbook')?>"
                />
                <span v-show="Company.work_code.code != ''" @click="Company.work_code.code = '';searchPartner() " class="sima-icon sil-times"></span>
            </div> 
        </div>
        <div class="person-search">
            <div>
                <span><?=Yii::t('SIMAAddressbook.SIMAAddressbook', 'FirstName')?></span>
                <input 
                    type="text"
                    spellcheck="false"
                    v-model="Person.firstname"
                    @input="searchPartner"
                    Placeholder="<?=Yii::t('SIMAAddressbook.SIMAAddressbook', 'PlaceholderSearchAddressbook')?>"
                />
                <span v-show="Person.firstname != ''" @click="Person.firstname = '';searchPartner() " class="sima-icon sil-times"></span>
            </div>
            <div>
                <span><?=Yii::t('SIMAAddressbook.SIMAAddressbook', 'LastName')?></span>
                <input 
                    type="text"
                    spellcheck="false"
                    v-model="Person.lastname"
                    @input="searchPartner"
                    Placeholder="<?=Yii::t('SIMAAddressbook.SIMAAddressbook', 'PlaceholderSearchAddressbook')?>"
                />
                <span v-show="Person.lastname != ''" @click="Person.lastname = '';searchPartner() " class="sima-icon sil-times"></span>
            </div>
            <div>
                <span><?=Yii::t('SIMAAddressbook.SIMAAddressbook', 'Address')?></span>
                <input
                    type="text"
                    spellcheck="false"
                    v-model="Person.partner_to_address.address.display_name"
                    @input="searchPartner"
                    Placeholder="<?=Yii::t('SIMAAddressbook.SIMAAddressbook', 'PlaceholderSearchAddressbook')?>"
                />
                <span 
                    v-show="Person.partner_to_address.address.display_name != ''"
                    @click="Person.partner_to_address.address.display_name = '';searchPartner() "
                    class="sima-icon sil-times">
                </span>
            </div>
            <div>
                <span><?=Yii::t('SIMAAddressbook.SIMAAddressbook', 'Comment')?></span>
                <input 
                    type="text"
                    spellcheck="false"
                    v-model="Person.comment"
                    @input="searchPartner"
                    Placeholder="<?=Yii::t('SIMAAddressbook.SIMAAddressbook', 'PlaceholderSearchAddressbook')?>"
                />
                <span v-show="Person.comment != ''" @click="Person.comment = '';searchPartner() " class="sima-icon sil-times"></span>
            </div>
        </div>
        <span @click="closeSearch" class="input-icon sima-icon sil-times"></span>
    </div>
</script>

