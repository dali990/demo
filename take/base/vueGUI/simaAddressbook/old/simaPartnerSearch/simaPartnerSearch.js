
/* global Vue, sima */

Vue.component('sima-partner-search-filter', {
    template: '#sima-partner-search-filter',
    mixins: [sima.vue.getBaseComponent()],
    props: {
        disabled: {type: Boolean, default: false},
        event_bus: {type: Object},
    },
    computed: {
        isEmptyCompanyFilter(){
            if(
                this.Company.name === "" && this.Company.PIB === "" && 
                this.Company.MB === "" && this.Company.comment === "" && 
                this.Company.work_code.code === "" && this.Company.partner_to_address.address.display_name === "" &&
                this.Company.comment === "" &&
                this.Company.employees.firstname === "" 
            )
            {
                return true;
            }
            return false;
        },
        isEmptyPersonFilter(){
            if(
                this.Person.firstname === "" && this.Person.lastname === "" && 
                this.Person.partner_to_address.address.display_name === "" &&
                this.Person.comment === "" 
            )
            {
                return true;
            }
            return false;
        },
        events(){
            return {
                'keydown': this.onKeyDown,
                'keyup': this.onKeyUp,
                'blur': this.onBlur,
                'focus': this.onFocus
            };
        },
        filter(){
            var filter = {
                Company: {},
                Person: {}
            };
            if(this.Company.name)
                filter.Company.name = this.Company.name;
            if(this.Company.PIB)
                filter.Company.PIB = this.Company.PIB;
            if(this.Company.MB)
                filter.Company.MB = this.Company.MB;
            if(this.Company.work_code.code)
                filter.Company.work_code = {code: this.Company.work_code.code};
            if(this.Person.comment)
                filter.Company.comment = this.Person.comment;
            if(this.Person.partner_to_address.address.display_name)
                filter.Company.partner_to_address = {address: {display_name: this.Person.partner_to_address.address.display_name} };
            if(this.Person.firstname)
                filter.Company.employees = {firstname: this.Person.firstname};
            if(this.Person.lastname)
                filter.Company.employees = {lastname: this.Person.lastname};
            
            if(this.Person.firstname)
                filter.Person.firstname = this.Person.firstname;
            if(this.Person.lastname)
                filter.Person.lastname = this.Person.lastname;
            if(this.Person.comment)
                filter.Person.comment = this.Person.comment;
            if(this.Person.partner_to_address.address.display_name)
                filter.Person.partner_to_address = {address: {display_name: this.Person.partner_to_address.address.display_name} };
            if(this.Company.name)
                filter.Person.company = {name: this.Company.name};
            if(this.Company.PIB)
                filter.Person.company = {PIB: this.Company.PIB};
            if(this.Company.MB)
                filter.Person.company = {MB: this.Company.MB};
            if(this.Company.work_code.code)
                filter.Person.company = {work_code: {code: this.Company.work_code.code} };
            
            return filter;
        }
    },
    watch: {
//        'Person.address'(new_val){
//            this.Company.addresses.display_name = new_val;
//        },
//        'Person.firstname'(new_val){
//            this.Company.employees.firstname = new_val;
//        }
    },
    data: function () {
        return {
            search_timeout: null,
            Company: {
                name: "",
                PIB: "",
                MB: "",
                work_code: {
                    code: ""
                },
                comment: "",
                partner_to_address: {
                    address: {
                        display_name: ""
                    }
                }
            },
            Person: {
                firstname: "",
                lastname: "",
                comment: "",
                partner_to_address: {
                    address: {
                        display_name: ""
                    }
                }
            },
            model_filter_init: null //za reset model_filter
        };
    },
    mounted: function() {
        var _this = this;
        this.model_filter_init = this.model_filter;
        this.event_bus.$on('event_bus_clear_search', function(){
            _this.clearSearch();
        });
        var z_index = $(this.$parent.$el).zIndex() + 102;
        this.$el.style.zIndex = z_index;
    },
    destroyed() {
        
    },
    methods: {
        onClickOutside(e){
            if(!e.target.closest('.addressbook-search-input'))
            {
                this.closeSearch();
            }
        },
        searchPartner(){
//            this.Company.partner_to_address.address.display_name = this.Person.partner_to_address.address.display_name;
//            this.Company.comment = this.Person.comment;
            var _this = this;
            var globalTimeout = this.search_timeout;
            if (globalTimeout !== null) {
                clearTimeout(globalTimeout);
                this.search_timeout = globalTimeout;
            }
            this.search_timeout = setTimeout(function () {
//                var filter = sima.clone(_this.model_filter);
//                if(_this.isEmptyCompanyFilter)
//                {
//                    filter.Company = {};
//                }
//                if(_this.isEmptyPersonFilter)
//                {
//                    filter.Person = {};
//                }
                _this.$emit('search', _this.filter);
            }, 500); 
        },
        clearSearch(){
            this.Company = {
                name: "",
                PIB: "",
                MB: "",
                work_code: {
                    code: ""
                },
                comment: "",
                partner_to_address: {
                    address: {
                        display_name: ""
                    }
                }
            };
            this.Person = {
                firstname: "",
                lastname: "",
                comment: "",
                partner_to_address: {
                    address: {
                        display_name: ""
                    }
                }
            };
        },
        closeSearch(){
            this.clearSearch();
            this.searchPartner();
            this.event_bus.$emit('event_bus_close_partner_search');
        }
    }
});