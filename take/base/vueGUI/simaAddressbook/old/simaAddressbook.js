
/* global Vue, sima */

Vue.component('sima-addressbook', {
    template: '#sima-addressbook',
    mixins: [sima.vue.getBaseComponent()],
    props: {
        disabled: {type: Boolean, default: false}
    },
    created(){
        
    },
    computed: {
        events(){
            return {
                'keydown': this.onKeyDown,
                'keyup': this.onKeyUp,
                'blur': this.onBlur,
                'focus': this.onFocus
            };
        },
        company_scope(){
            
        },
        person_scope(){
            
        },
        companies(){
            return sima.vue.shot.getters.model_list(this, 'Company', this.getCriteria('Company', this.company_scopes));
        },
        companies_all_cnt(){
            return sima.vue.shot.getters.model_list_count(this, 'Company', this.countCriteria('Company'));
        },
        companies_suppliers_cnt(){
            return sima.vue.shot.getters.model_list_count(this, 'Company', this.countCriteria('Company', {partnersInList: 'suppliers'}));
        },
        companies_customers_cnt(){
            return sima.vue.shot.getters.model_list_count(this, 'Company', this.countCriteria('Company', {partnersInList: 'buyers'}));
        },
        
        persons(){
            return sima.vue.shot.getters.model_list(this, 'Person', this.getCriteria('Person', this.person_scopes));
        },
        persons_all_cnt(){
            var c = sima.vue.shot.getters.model_list_count(this, 'Person', this.countCriteria('Person'));
            return c;
        },
        persons_counter(){
            var counter = {};
            for(var i in this.person_partner_lists)
            {
                var partner_list = this.person_partner_lists[i];
                counter[partner_list.id] = 0;
                
                if(partner_list.code)
                {
                    var list_cnt = sima.vue.shot.getters.model_list_count(this, 'Person', this.countCriteria('Person', {partnersInList: partner_list.code}));
                    counter[partner_list.id] = list_cnt;
                }
                else 
                {
                    var list_cnt = sima.vue.shot.getters.model_list_count(this, 'Person', this.countCriteria('Person', {partnersWithListId: partner_list.id}));
                    counter[partner_list.id] = list_cnt;
                }
            }
            return counter;
        },
        comapnies_counter(){
            var counter = {};
            for(var i in this.company_partner_lists)
            {
                var partner_list = this.company_partner_lists[i];
                counter[partner_list.id] = 0;
                
                if(partner_list.code)
                {
                    var list_cnt = sima.vue.shot.getters.model_list_count(this, 'Company', this.countCriteria('Company', {partnersInList: partner_list.code}));
                    counter[partner_list.id] = list_cnt;
                }
                else 
                {
                    var list_cnt = sima.vue.shot.getters.model_list_count(this, 'Company', this.countCriteria('Company', {partnersWithListId: partner_list.id}));
                    counter[partner_list.id] = list_cnt;
                }
            }
            return counter;
        },
        persons_suppliers_cnt(){
            return sima.vue.shot.getters.model_list_count(this, 'Person', this.countCriteria('Person', {partnersInList: 'suppliers'}));
        },
        persons_customers_cnt(){
            return sima.vue.shot.getters.model_list_count(this, 'Person', this.countCriteria('Person', {partnersInList: 'buyers'}));
        },
        persons_hr_candidates_cnt(){
            return sima.vue.shot.getters.model_list_count(this, 'Person', this.countCriteria('Person', {partnersInList: 'hr_candidates'}));
        },
        persons_employees_cnt(){
            return sima.vue.shot.getters.model_list_count(this, 'Person', this.countCriteria('Person', {partnersInList: 'employees'}));
        },
        persons_former_employees_cnt(){
            return sima.vue.shot.getters.model_list_count(this, 'Person', this.countCriteria('Person', {partnersInList: 'former_employees'}));
        },
        persons_user_cnt(){
            return sima.vue.shot.getters.model_list_count(this, 'Person', this.countCriteria('Person', {partnersInList: 'users'}));
        },
        isEmptySearch(){
            return (
                this.partner_search_text === ""
            );
        },
        isOpenSearchFielter(){
            return this.show_search_filter;
        },
        popperOptions(){
            var _this = this;
            return {
                placement: 'bottom-start',
                strategy: 'fixed',
                onFirstUpdate(e){
                    _this.$refs.simaPartnerSearch.$el.style.width = _this.$refs.searchInptuField.clientWidth+"px";
                },
                onUpdate(e){
                    setTimeout(function(){
                        _this.$refs.simaPartnerSearch.$el.style.width = _this.$refs.searchInptuField.clientWidth+"px";
                    },500);
                },
                modifiers: [
                    {
                        name: 'updatePartnerSearchWidth',
                        enabled: true,
                        phase: 'afterWrite',
                        fn({ state }) {
                            setTimeout(function(){
                                _this.$refs.simaPartnerSearch.$el.style.width = _this.$refs.searchInptuField.clientWidth+"px";
                            },500);
                        }
                    }
                ]
            };
        },
        person_partner_lists(){          
            return sima.vue.shot.getters.model_list(this, 'PartnerList', {
                order: 'name',
                model_filter: {
                    scopes: {
                        forUser: sima.getCurrentUserId()
                    }
                }
            });
        },
        company_partner_lists(){          
            return sima.vue.shot.getters.model_list(this, 'PartnerList', {
                order: 'name',
                model_filter: {
                    scopes: {
                        forUser: sima.getCurrentUserId()
                    }
                }
            });
        }
    },
    watch: {
        partner_search_text(new_val){
            var _this = this;
            this.debounce(function () {
                _this.new_added_companies = [];
                _this.new_added_persons = [];
                _this.partner_search_debounced_text = new_val;
            }, 500);
        }
    },
    data: function () {
        return {
            event_bus: new Vue(),
            partner_search_text: "",
            partner_search_debounced_text: "",
            offset: 0,
            company_scopes:{},
            person_scopes:{},
            show_search_filter: false,
            search_partner_html: null,
            search_partner_popper: null,
            model_filter_person: {},
            model_filter_company: {},
            show_overlay: false,
            z_index_after_overlay: 100,
            company_selected_tab: 'companies',
            person_selected_tab: 'persons',
            new_added_companies: [],
            new_added_persons: [],
            limit: 5,
            tab_limit: {
                company: {
                    companies: 5,
//                    marked: 5,
                    suppliers: 5,
                    buyers: 5
                },
                person: {
                    persons: 5,
//                    marked: 5,
                    suppliers: 5,
                    markbuyersed: 5,
                    hr_candidates: 5,
                    employees: 5,
                    former_employees: 5,
                    users: 5,
                }
            },
            search_timeout: null
        };
    },
    mounted: function() {
        var _this = this;
        window.ad = _this;
        this.event_bus.$on('event_bus_close_partner_search', function(){
            _this.closeSearch();
        });
        var z_index = $(this.$el.parentElement).zIndex();
        this.$el.style.zIndex = (z_index+1);
        this.$refs.overlay.style.zIndex = (z_index+2);
        this.z_index_after_overlay = z_index+2;
        this.$refs.searchInptuField.style.zIndex = this.z_index_after_overlay;
        //this.makePopper();
        
    },
    destroyed() {
        if(this.search_partner_html)
        {
            this.search_partner_html.remove();
        }
        if (this.search_partner_popper) {
            this.search_partner_popper.destroy();
            this.search_partner_popper = null;
        }
    },
    methods: {
        clearSearch(){
            this.partner_search_text = "";
            this.model_filter_person = {};
            this.model_filter_company = {};
            this.new_added_companies = [];
            this.new_added_persons = [];
            Vue.set(this, 'model_filter_person', {});
            this.event_bus.$emit('event_bus_clear_search');
        },
        toggleSearch(){
            var _this = this;
            //Toggle otvaranja filtera za pretragu trenutno iskljucen dok se ne doda nova funkcionalnost, 
            //prikaz onoga sto je kucano u filteru dok je zatvoren
//            if(_this.isOpenSearchFielter)
//            {
//                _this.closeSearch();
//            }
//            else
//            {
//                _this.openSearch();
//            }
            //Privremeno
            if(!_this.isOpenSearchFielter)
            {
                _this.openSearch();
            }
        },
        openSearch(){
            this.show_search_filter = true;
            this.show_overlay = true;
            Vue.set(this, 'show_search_filter', true);
        },
        closeSearch(){
            this.show_search_filter = false;
            this.show_overlay = false;
        },
        editCompany(id){
            sima.model.form("Company", id);
        },
        editPerson(id){
            sima.model.form("Person", id);
        },
        companyResetModelList(){
            this.company_scopes = {};
            this.limit = 30;
        },
        personResetModelList(){
            this.person_scopes = {};
            this.limit = 30;
        },
        onKeyUp(e){
        },
        onKeyDown(e){
        },
        onFocus(e){
            this.openSearch();
        },
        onBlur(e){
            
        },
        //event komponente sima-partner-search
        partnerSearchFilter(model_filter){
            var _this = this;
            var globalTimeout = this.search_timeout;
            if (globalTimeout !== null) {
                clearTimeout(globalTimeout);
                this.search_timeout = globalTimeout;
            }
            
            this.search_timeout = setTimeout(function () {
                Vue.set(_this, 'model_filter_person', model_filter.Person);
                Vue.set(_this, 'model_filter_company', model_filter.Company);
            }, 400);
        },
        //Inicijalizacija popera, pozicioniranje komponente sima-partner-search
        makePopper(){
            var _this = this;
            this.show_search_filter = true;
            this.$nextTick(function() {
                this.search_partner_popper = Popper.createPopper(this.$refs.searchInptuField, this.$refs.simaPartnerSearch.$el, {
                    placement: 'bottom-start',
                    strategy: 'relative',
                    onFirstUpdate(e){
                        _this.$refs.simaPartnerSearch.$el.style.width = _this.$refs.searchInptuField.clientWidth+"px";
                    },
                    onUpdate(e){
                        setTimeout(function(){
                            _this.$refs.simaPartnerSearch.$el.style.width = _this.$refs.searchInptuField.clientWidth+"px";
                        },500);
                    },
                    modifiers: [
                        {
                            name: 'updatePartnerSearchWidth',
                            enabled: true,
                            phase: 'afterWrite',
                            fn({ state }) {
                                setTimeout(function(){
                                    _this.$refs.simaPartnerSearch.$el.style.width = _this.$refs.searchInptuField.clientWidth+"px";
                                },500);
                            }
                        }
                    ]
                });
                this.show_search_filter = false;
            });
        },
        //Primenjujemo scope nad modelom company, tabName - naziv scope-a
        companyTabChange(index, selected_tab, old_tab){
            this.companyResetModelList();
            //Genericke liste, tabName je list_code
            if(selected_tab.tabName !== '' && isNaN(selected_tab.tabName) && selected_tab.tabName !== 'companies')
            {
                Vue.set(this.company_scopes, 'partnersInList', selected_tab.tabName);
            }
            //Korisnicke liste, tabName je ID
            else if(!isNaN(selected_tab.tabName))
            {
                Vue.set(this.person_scopes, 'partnersWithListId', selected_tab.tabName);
            }
            this.company_selected_tab = selected_tab.tabName;
        },
        personTabChange(index, selected_tab, old_tab){
            this.personResetModelList();
            if(selected_tab.tabName !== '' && isNaN(selected_tab.tabName) && selected_tab.tabName !== 'persons')
            {
                Vue.set(this.person_scopes, 'partnersInList', selected_tab.tabName);
            }
            else if(!isNaN(selected_tab.tabName))
            {
                Vue.set(this.person_scopes, 'partnersWithListId', selected_tab.tabName);
            }
            this.person_selected_tab = selected_tab.tabName;
        },
        //Opcije (tockic)
        openAddresses(){
            sima.dialog.openActionInDialogAsync('guitable', {
                get_params: {
                    label: sima.translate('GeoAddresses'),
                    settings: {
                        model: 'Address',
                        add_button: true
                    }
                }
            });
        },
        openStreets(){
            sima.dialog.openActionInDialogAsync('guitable', {
                get_params: {
                    label: sima.translate('GeoStreets'),
                    settings: {
                        model: 'Street',
                        add_button: true
                    }
                }
            });
        },
        openPostalCodes(){
            sima.dialog.openActionInDialogAsync('guitable', {
                get_params: {
                    label: sima.translate('GeoPostalCodes'),
                    settings: {
                        model: 'PostalCode',
                        add_button: true
                    }
                }
            });
        },
        openCities(){
            sima.dialog.openActionInDialogAsync('guitable', {
                get_params: {
                    label: sima.translate('GeoCities'),
                    settings: {
                        model: 'City',
                        add_button: true
                    }
                }
            });
        },
        openMunicipalities(){
            sima.dialog.openActionInDialogAsync('guitable', {
                get_params: {
                    label: sima.translate('GeoMunicipalities'),
                    settings: {
                        model: 'Municipality',
                        add_button: true
                    }
                }
            });
        },
        openCountries(){
            sima.dialog.openActionInDialogAsync('guitable', {
                get_params: {
                    label: sima.translate('GeoCountries'),
                    settings: {
                        model: 'Country',
                        add_button: true
                    }
                }
            });
        },
        openWorkCodes(){
            sima.dialog.openActionInDialogAsync('guitable', {
                get_params: {
                    label: sima.translate('WorkCodes'),
                    settings: {
                        model: 'CompanyWorkCode',
                        add_button: true
                    }
                }
            });
        },
        openPartnerLists(){
            sima.dialog.openActionInDialogAsync('simaAddressbook/partnersToPartnerLists');
        },
        openContactTypes(){
            sima.dialog.openActionInDialogAsync('guitable', {
                get_params: {
                    label: sima.translate('ContactTypes'),
                    settings: {
                        model: 'ContactType',
                        add_button: true
                    }
                }
            });
        },
        getCriteria(model_name, scope={}, model_filter=null){
            var criteria = {
                order: 'display_name',
                model_filter: {
                    limit: this.limit,
                    offset: this.offset,
                    scopes: scope,
                    text: this.partner_search_debounced_text
                }
            };
            if(model_name === 'Company')
            {
                criteria.model_filter.limit = this.tab_limit.company[this.company_selected_tab];
                Object.assign(criteria.model_filter, this.model_filter_company);
            }
            else if(model_name === 'Person')
            {
                criteria.model_filter.limit = this.tab_limit.person[this.person_selected_tab];
                Object.assign(criteria.model_filter, this.model_filter_person);
            }
            if(model_filter)
            {
                Object.assign(criteria.model_filter, model_filter);
            }
            
            return criteria;
        },
        countCriteria(model_name, scope={}, model_filter=null){
            var criteria = {
                order: 'display_name',
                model_filter: {
                    scopes: scope,
                    text: this.partner_search_debounced_text
                }
            };
            if(model_name === 'Company')
            {
                Object.assign(criteria.model_filter, this.model_filter_company);
            }
            else if(model_name === 'Person')
            {
                Object.assign(criteria.model_filter, this.model_filter_person);
            }
            if(model_filter)
            {
                Object.assign(criteria.model_filter, model_filter);
            }
            return criteria;
        },
        loadMoreCompanies(list_name){
            this.tab_limit.company[list_name] += this.limit;
        },
        loadMorePersons(list_name){
            this.tab_limit.person[list_name] += this.limit;
        },
        openCompanyForm(){
            var _this = this;
            var form_params = {
                onSave: function(response){
                    var id = response.model_id;
                    var model = sima.vue.shot.getters.model('Company_' + id);
                    _this.new_added_companies.push(model);
                },
                onClose: function(e){
                    
                }
            };
            sima.model.form('Company', '', form_params);
        },
        openPersonForm(){
            var _this = this;
            var form_params = {
                onSave: function(response){
                    var id = response.model_id;
                    var model = sima.vue.shot.getters.model('Person_' + id);
                    _this.new_added_persons.push(model);
                },
                onClose: function(e){
                    
                }
            };
            sima.model.form('Person', '', form_params);
        },
        //Kada se obrise partner iz model list, brisemo istog partnera iz liste novododatih 
        removeListItem(list='person', model_id){
            if(list === 'company')
            {
                this.new_added_companies = this.new_added_companies.filter(model => {
                    return model.id !== model_id;
                });
            }
            else
            {
                this.new_added_persons = this.new_added_persons.filter(model => {
                    return model.id !== model_id;
                });
                
            }
        }
    }
});
