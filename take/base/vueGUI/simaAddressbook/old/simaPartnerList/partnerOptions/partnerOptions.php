<script type="text/x-template" id="partner-options">
    <div class="partner-options" v-observe-visibility="componentInstanceVisibilityChanged" @mouseleave="show_content=false">
        <div class="partner-options-activator" @mouseover="show_content = true">
            <i class="sima-icon sis-ellipsis-h"></i>
        </div>
        <div class="content-model-options" v-if="show_content">
            <slot></slot>
        </div>
    </div>
</script>

