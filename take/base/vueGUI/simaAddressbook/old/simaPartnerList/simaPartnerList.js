
/* global Vue, sima */

Vue.component('sima-partner-list', {
    template: '#sima-partner-list',
    mixins: [sima.vue.getBaseComponent()],
    props: {
        list_count: {type: Number, default: 0},
        model_list: {type: Array},
        type: {type: String, default: ""}
    },
    computed: {
        dataReady(){
            if(this.model_list.length > 0)
            {
                for(var i in this.model_list)
                {
                    var model = this.model_list[i];
                    if(
                            model.shotStatus('DisplayName') !== 'OK' ||
                            model.shotStatus('partner_to_partner_lists') !== 'OK' ||
                            model.shotStatus('main_or_any_phone') !== 'OK' ||
                            model.shotStatus('main_or_any_email') !== 'OK' ||
                            model.shotStatus('main_or_any_address') !== 'OK' ||
                            model.shotStatus('first_other_contact') !== 'OK' ||
                            model.shotStatus('comment') !== 'OK'
                    )
                    {
                        return false;
                    }
                }
                return true;
            }
            else if(this.model_list.shotStatus === 'OK')
            {
                return true;
            }
            return false;
        }
    },
    watch: {
        dataReady(new_val, old_val){
           if(new_val){
               sima.hideLoading(null, this.loading_obj);
           }
        }
    },
    data: function () {
        return {
            offset: 0,
            company_scopes:{},
            person_scopes:{},
            model_filter_person: {},
            model_filter_company: {},
            options_popper: null,
            update_tooltip: null,
            z_index: 1,
            render_if_visible_key: 0,
            loading_obj: null,
            limit: 30
        };
    },
    mounted: function() {       
        this.z_index = $(this.$el.parentElement).zIndex();
        this.z_index++;
        this.$el.style.zIndex = this.z_index;
        
        if(!this.dataReady)
        {
            this.loading_obj = sima.showLoading($(this.$parent.$el));
        }
    },
    destroyed() {
        if (this.options_popper) {
            this.options_popper.destroy();
            this.options_popper = null;
        }
    },
    methods: {
        getInitials(model){
            if(model)
            {
                if(this.type === 'company' && model.name)
                {
                    var initials = model.name.trim().split(" ");
                    var first = initials[0][0];
                    var second = "";
                    if(initials[0][1])
                    {
                        second = initials[0][1];
                    }
                    else if(initials[1][0])
                    {
                        second = initials[1][0];
                    }
                    return first+second.toUpperCase();
                }
                else if(this.type === 'person' && model.firstname && model.lastname)
                {
                    return (model.firstname.trim()[0]+model.lastname.trim()[0]).toString().toUpperCase();
                }
            }
            return "";
        },
        partnerTitle(model){
            if(this.type === 'company')
            {
                return model.name;
            }
            else if(this.type === 'person')
            {
                return model.firstname+" "+model.lastname;
            }
        },
        loadMore(e){
            var btn = e.target.closest('.cta');
            var arrow_down = btn.querySelector('.sil-long-arrow-down');
            if(arrow_down)
            {
                arrow_down.classList.remove('sil-long-arrow-down');
                arrow_down.classList.add('sil-spinner');
                arrow_down.classList.add('fa-spin');
            }
            this.$emit('load_more');
        },
        //TODO: za brisanje
        optionsClickOutside(e){
            if(!e.target.closest(".partner-options") && !e.target.closest(".all-options"))
            {
                var partner_options = this.$el.querySelectorAll(".partner-options");
                partner_options.forEach(function(el){
                    el.style.display =  "none";
                });
            }
        },
        //TODO: za brisanje
        expandPartner(e){
            var partner_maincontent = e.target.closest(".partner-maincontent");
            if(partner_maincontent)
            {
                var partner_bottombar_expand = partner_maincontent.querySelector(".partner-bottombar-expand");
                if(partner_bottombar_expand.style.display === "inline-block")
                {
                    this.closePartnerBottombarExpand(e);
                }
                else
                {
                    var partner_bottombar = partner_maincontent.querySelector(".partner-bottombar");
                    partner_bottombar.style.display = "none";
                    partner_bottombar_expand.style.display = "inline-block";
                }
            }
        },
        //TODO: za brisanje
        closePartnerBottombarExpand(e){
            if(!e.target.closest(".partner-bottombar-expand"))
            {
                var partner_bottombar = this.$el.querySelectorAll(".partner-bottombar");
                var partner_bottombars_expand = this.$el.querySelectorAll(".partner-bottombar-expand");
                partner_bottombar.forEach(function(el){
                    el.style.display =  "flex";
                });
                partner_bottombars_expand.forEach(function(el){
                    el.style.display =  "none";
                });
            }
        },
        openEmployeeListInDialog: function(model, event) {
            event.stopPropagation();
            sima.dialog.openVueComponent('sima-company-employee-list', {
                company: model
            }, sima.translate('SIMAAddressBookCompanyEmployeeListInCompany')+model.DisplayName, {fullscreen: true});
        },
        //Kada se izbrise partnera iz default liste radimo update u listi novododatih ili brnuto
        removeListItem(model_id){
            var new_model_list = this.model_list.filter(model => {
                return model.id !== model_id;
            });
            this.$emit('update:model_list', new_model_list);//kada se izbrise partner iz liste novododatih
            this.$emit('removedListItem', model_id);
        }
    }
});