<script type="text/x-template" id="sima-partner-list">
    <div class="sima-partner-list" v-observe-visibility="componentInstanceVisibilityChanged">
        <vue-perfect-scrollbar class="perfect-scrollbar" v-bind:settings="{ minScrollbarLength: 30, suppressScrollX: true }">
            <div v-for="model in model_list" class="partner-card">
                <div class="partner-initials">
                    <sima-model-display-icon 
                        v-bind:model="model"
                    >
                    </sima-model-display-icon>
                    <div class="partner-bottombar-expand">
                        <!-- expand PIB -->
                        <div v-if="type === 'company'" class="type-company" >
                            <i class="sima-icon sis-pib company-icon"></i>
                            <span v-if="model.PIB" class="copy-to-clipboard" v-copy-to-clipboard>{{model.PIB}}</span>
                            <span v-else class="no-info-comment"><?=Yii::t('JSTranslations', 'NoInformation')?></span>
                        </div>
                        <!-- expand MB -->
                        <div v-if="type === 'company'" class="type-company">
                            <i class="sima-icon sis-mb company-icon" ></i>
                            <span v-if="model.MB" class="copy-to-clipboard" v-copy-to-clipboard>{{model.MB}}</span>
                            <span v-else class="no-info-comment"><?=Yii::t('JSTranslations', 'NoInformation')?></span>
                        </div>
                        <!-- expand PDV -->
                        <div v-if="type === 'company'" class="type-company">
                            <i class="sima-icon sis-pdv company-icon" ></i>
                            <span v-if="model.inVat" class="copy-to-clipboard" v-copy-to-clipboard><?=Yii::t('Company', 'YesVATsystem')?></span>
                            <span v-else class="copy-to-clipboard" v-copy-to-clipboard><?=Yii::t('Company', 'NoVATsystem')?></span>
                        </div>
                        <!-- expand SD -->
                        <div v-if="type === 'company'" class="type-company">
                            <i class="sima-icon sis-sd company-icon" ></i>
                            <span v-if="model.work_code.DisplayName" class="copy-to-clipboard" v-copy-to-clipboard>{{model.work_code.DisplayName}}</span>
                            <span v-else class="no-info-comment"><?=Yii::t('JSTranslations', 'NoInformation')?></span>
                        </div>
                        <!-- expand PHONE -->
                        <template v-if="model.phone_numbers.length > 0">
                            <div v-for="phone_number in model.phone_numbers">
                                <i class="sima-icon sir-phone"></i>
                                <div class="with-comment">
                                    <span class="copy-to-clipboard" v-copy-to-clipboard>{{phone_number.DisplayName}}</span>
                                    <span v-if="phone_number.comment" class="highlight-comment">{{phone_number.comment}}</span>
                                </div>
                            </div>
                        </template>
                        <div v-else>
                            <i class="sima-icon sir-phone"></i>
                            <span class="no-info-comment"><?=Yii::t('JSTranslations', 'NoInformation')?></span>
                        </div>
                        <!-- expand EMAIL -->
                        <template v-if="model.email_addresses.length > 0">
                            <div v-for="email_address in model.email_addresses">
                                <i class="sima-icon sir-envelope"></i>
                                <div class="with-comment">
                                    <span class="copy-to-clipboard" v-copy-to-clipboard>{{email_address.DisplayName}}</span>
                                    <span class="highlight-comment" v-if="email_address.comment">{{email_address.comment}}</span>
                                </div>
                            </div>
                        </template>
                        <div v-else>
                            <i class="sima-icon sir-envelope"></i>
                            <span class="no-info-comment"><?=Yii::t('JSTranslations', 'NoInformation')?></span>
                        </div>
                        <!-- expand ADDRESS -->
                        <template v-if="model.main_or_any_address">
                            <div  v-for="address in model.addresses">
                                <i class="sima-icon sir-map-marker-alt"></i>
                                <span class="copy-to-clipboard" v-copy-to-clipboard>{{address.DisplayName}}</span>
                            </div>
                        </template>
                        <div v-else>
                            <i class="sima-icon sir-map-marker-alt"></i>
                            <span class="no-info-comment"><?=Yii::t('JSTranslations', 'NoInformation')?></span>
                        </div>
                        <!-- expand OTHER CONTACTS -->
                        <template v-if="model.first_other_contact">
                            <div  v-for="address in model.addresses">
                                <i class="sima-icon sir-globe-americas"></i>
                                <span class="copy-to-clipboard" v-copy-to-clipboard>{{model.first_other_contact}}</span>
                            </div>
                        </template>
                        <div v-else>
                            <i class="sima-icon sir-globe-americas"></i>
                            <span class="no-info-comment"><?=Yii::t('JSTranslations', 'NoInformation')?></span>
                        </div>
                        <!-- expand BANK ACCOUNT -->
                        <template v-if="model.bank_accounts.length">
                            <div v-for="bank_account in model.bank_accounts">
                                <i class="sima-icon sir-euro-sign"></i>
                                <div class="with-comment">
                                    <div class="copy-to-clipboard" v-copy-to-clipboard>{{bank_account.number}}</div>
                                    <div class="copy-to-clipboard" v-copy-to-clipboard>{{bank_account.bank.DisplayName}}</div>
                                    <div class="highlight-comment">{{bank_account.comment}}</div>
                                </div>
                            </div>
                        </template>
                        <div v-else>
                            <i class="sima-icon sir-euro-sign"></i>
                            <span class="no-info-comment"><?=Yii::t('JSTranslations', 'NoInformation')?></span>
                        </div>   
                        <!-- expand COMMENT -->
                        <div v-if="model.comment !== ''">
                            <i class="sima-icon sir-quote-left"></i>
                            <span class="copy-to-clipboard" v-copy-to-clipboard>{{model.comment}}</span>
                        </div>
                        <div v-else>
                            <i class="sima-icon sir-quote-left"></i>
                            <span class="no-info-comment"><?=Yii::t('JSTranslations', 'NoInformation')?></span>
                        </div>                        
                    </div><!-- end partner-bottombar-bottombar -->
                </div><!-- PARTNER BOTTOMBAR EXPAND -->
                <div class="partner-maincontent">
                    <div class="partner-topbar" >
                        <div class="name-and-list" >
                            <span class="name dots" v-bind:title="model.DisplayName" v-html="model.DisplayHTML"></span>
                            <span class="lists dots">
                                <span class="list" v-for="ptp_list in model.partner_to_partner_lists">
                                    <i class="sima-icon sis-circle"></i>
                                    <span v-bind:title="ptp_list.partner_list.name">{{ptp_list.partner_list.name}}</span>
                                </span>
                            </span>
                        </div><!-- end name-and-list -->
                        <div class="partner-options-wrap">
                            <sima-button v-if="type === 'company'" class="btn-transparent-full" @click="openEmployeeListInDialog(model, $event)">
                                <i class="sima-icon sil-users"></i>
                            </sima-button> 
<!--                            <sima-button class="btn-transparent-full" @click="expandPartner" title="<?=Yii::t('SIMAAddressbook.SIMAAddressbook', 'ExtendedView')?>">
                                <i class="sima-icon sir-angle-down"></i>
                            </sima-button> -->
                            <partner-options class="sima-btn btn-transparent-full"> 
                                <sima-model-options v-bind:model="model" ></sima-model-options>
                                <sima-model-delete v-if="model.partner.can_delete" v-bind:model="model.partner" @afterDelete="removeListItem(model.id)" style="margin: 4px 0 0 2px;"></sima-model-delete>
                            </partner-options> 
                        </div><!-- end name-and-list-wrap -->           
                    </div><!-- end partner-topbars -->
                    <div class="partner-bottombar" style="display: flex;">
                        <!-- MAIN INFO -->
                        <div class="main-info" >
                            <!-- main-info COMPANY -->
                            <div v-if="type === 'person' && model.company" v-bind:title="model.company.DisplayName" class="main-contact dots" >
                                <i class="sima-icon sil-building"></i>
                                <span class="dots copy-to-clipboard" v-copy-to-clipboard>{{model.company.DisplayName}}</span>
                            </div>
                            <!-- main-info PHONE -->
                            <div v-else-if="model.main_or_any_phone" v-bind:title="model.main_or_any_phone" class="main-contact dots" >
                                <i class="sima-icon sir-phone"></i>
                                <span class="dots copy-to-clipboard" v-copy-to-clipboard>{{model.main_or_any_phone}}</span>
                            </div>
                            <!-- main-info EMAIL -->
                            <div v-else-if="model.main_or_any_email" v-bind:title="model.main_or_any_email" class="main-contact dots" >
                                <i class="sima-icon sir-envelope"></i>
                                <span class="dots copy-to-clipboard" v-copy-to-clipboard>{{model.main_or_any_email}}</span>
                            </div>
                            <!-- main-info EMAIL -->
                            <div v-else-if="model.main_or_any_address" v-bind:title="model.main_or_any_address" class="main-contact dots" >
                                <i class="sima-icon sir-map-marker-alt"></i>
                                <span class="dots copy-to-clipboard" v-copy-to-clipboard>{{model.main_or_any_address}}</span>
                            </div>
                            <!-- main-info OTHER CONTACT -->
                            <div v-else-if="model.first_other_contact" v-bind:title="model.first_other_contact" class="main-contact dots" >
                                <i class="sima-icon sir-globe-americas"></i>
                                <span class="dots copy-to-clipboard" v-copy-to-clipboard>{{model.first_other_contact}}</span>
                            </div>
                            <!-- main-info OTHER CONTACT -->
                            <div v-else-if="model.first_other_contact" v-bind:title="model.first_other_contact" class="main-contact dots" >
                                <i class="sima-icon sir-globe-americas"></i>
                                <span class="dots copy-to-clipboard" v-copy-to-clipboard>{{model.first_other_contact}}</span>
                            </div>
                            <!-- main-info PIB - company -->
                            <div v-else-if="type === 'company' && model.PIB" v-bind:title="model.PIB" class="main-contact dots" >
                                <i class="sima-icon sir-pib"></i>
                                <span class="dots copy-to-clipboard" v-copy-to-clipboard>{{model.PIB}}</span>
                            </div>
                            <!-- main-info MB - company -->
                            <div v-else-if="type === 'company' && model.MB" v-bind:title="model.MB" class="main-contact dots" >
                                <i class="sima-icon sir-mb"></i>
                                <span class="dots copy-to-clipboard" v-copy-to-clipboard>{{model.MB}}</span>
                            </div>
                            <!-- main-info SD - company -->
                            <div v-else-if="type === 'company' && model.work_code.DisplayName" v-bind:title="model.work_code.DisplayName" class="main-contact dots" >
                                <i class="sima-icon sir-sd"></i>
                                <span class="dots copy-to-clipboard" v-copy-to-clipboard>{{model.work_code.DisplayName}}</span>
                            </div>
                            <!-- main-info PDV Da - company -->
                            <div v-else-if="type === 'company' && model.inVat" title="<?=Yii::t('Company', 'YesVATsystem')?>" class="main-contact dots" >
                                <i class="sima-icon sir-pdv"></i>
                                <span class="dots copy-to-clipboard" v-copy-to-clipboard><?=Yii::t('Company', 'YesVATsystem')?></span>
                            </div>
                            <!-- main-info COMMENT -->
                            <div v-if="model.comment"
                                 v-bind:title="model.comment"
                                 class="main-info-comment dots"
                            >
                                <i class="sima-icon sis-circle"></i>
                                <span class="main-info-comment">"{{model.comment}}"</span>
                            </div>
                        </div><!-- end main info -->

                        <!-- ALL INFO -->
                        <div class="all-info" v-if="false">
                            <!-- PIB sima-popup -->
                            <sima-popup v-if="type === 'company'" :show_arrow="false" v-bind:delay_on_mouse_over="100" >
                                <span slot="reference" class="sima-icon sir-pib company-icon" v-bind:class="{'no-info':!model.PIB}"></span>
                                <div class="partner-list-popper" v-if="model.PIB">
                                    <div class="first-child-tooltip-content">
                                        <div class="tooltip-header">
                                            <i class="sima-icon sir-pib company-icon"></i>
                                            <span><?=Yii::t('Company', 'PIBfull')?></span>
                                        </div><!-- end tooltip-header -->
                                        <div class="tooltip-body" style="margin-left: 30px;">
                                            <span class="copy-to-clipboard" v-copy-to-clipboard>{{model.PIB}}</span>
                                        </div><!-- end tooltip-body -->
                                    </div>
                                </div>
                            </sima-popup>
                            <!-- MB sima-popup -->
                            <sima-popup v-if="type === 'company'" :show_arrow="false" v-bind:delay_on_mouse_over="100">
                                <span slot="reference" class="sima-icon sir-mb company-icon" v-bind:class="{'no-info':!model.MB}"></span>
                                <div class="partner-list-popper" v-if="model.MB">
                                    <div class="first-child-tooltip-content">
                                        <div class="tooltip-header">
                                            <i class="sima-icon sir-mb company-icon" ></i>
                                            <span><?=Yii::t('Company', 'MB')?></span>
                                        </div>
                                        <div class="tooltip-body" style="margin-left: 30px;">
                                            <span class="copy-to-clipboard" v-copy-to-clipboard>{{model.MB}}</span>
                                        </div><!-- end tooltip-body -->
                                    </div>
                                </div>
                            </sima-popup>
                            <!-- PDV sistem sima-popup -->
                            <sima-popup v-if="type === 'company'" :show_arrow="false" v-bind:delay_on_mouse_over="100">
                                <span slot="reference" class="sima-icon sir-pdv company-icon" v-bind:class="{'no-info':!model.inVat}"></span>
                                <div class="partner-list-popper" v-if="model.inVat">
                                    <div class="first-child-tooltip-content">
                                        <div class="tooltip-header">
                                            <i class="sima-icon sir-pdv company-icon" ></i>
                                            <span><?=Yii::t('Company', 'InVATsystem')?></span>
                                        </div><!-- end tooltip-header -->
                                        <div class="tooltip-body" style="margin-left: 35px;" v-copy-to-clipboard>
                                            <span v-if="model.inVat" class="copy-to-clipboard" v-copy-to-clipboard><?=Yii::t('Company', 'YesVATsystem')?></span>
                                            <span v-else class="copy-to-clipboard" v-copy-to-clipboard><?=Yii::t('Company', 'NoVATsystem')?></span>
                                        </div>
                                    </div>
                                </div>
                            </sima-popup>
                            <!-- Sifra delatnosti sima-popup -->
                            <sima-popup v-if="type === 'company'" :show_arrow="false" v-bind:delay_on_mouse_over="100">
                                <span slot="reference" class="sima-icon sir-sd company-icon" v-bind:class="{'no-info':!model.work_code}"></span>
                                <div class="partner-list-popper" v-if="model.work_code.DisplayName">
                                    <div class="first-child-tooltip-content">
                                        <div class="tooltip-header">
                                            <i class="sima-icon sir-sd company-icon" ></i>
                                            <span><?=Yii::t('Company', 'ActivityCode')?></span>
                                        </div><!-- end tooltip-header -->
                                        <div class="tooltip-body" style="margin-left: 30px;">
                                            <span class="copy-to-clipboard" v-copy-to-clipboard>{{model.DisplayName}}</span>
                                        </div><!-- end tooltip-body -->
                                    </div>
                                </div>
                            </sima-popup>                            
                            <!-- Telefon adrese sima-popup -->
                            <sima-popup :show_arrow="false" v-bind:delay_on_mouse_over="100">
                                <span slot="reference" class="sima-icon sir-phone" v-bind:class="{'no-info':!model.phone_numbers.length}"></span>
                                <div class="partner-list-popper" v-if="model.phone_numbers.length">
                                    <div class="first-child-tooltip-content">
                                        <div class="tooltip-header">
                                            <i class="sima-icon sir-phone" ></i>
                                            <span><?=Yii::t('Partner', 'Phones')?> ({{model.phone_numbers.length}})</span>
                                        </div><!-- end tooltip-header -->
                                        <div class="tooltip-body">
                                            <div v-for="phone_number in model.phone_numbers">
                                                <div class="copy-to-clipboard" v-copy-to-clipboard>{{phone_number.DisplayName}}</div>
                                                <div class="highlight-comment">{{phone_number.comment}}</div>
                                            </div>
                                        </div><!-- end tooltip-body -->
                                    </div>
                                </div>
                            </sima-popup>
                            <!-- Email adrese sima-popup -->
                            <sima-popup :show_arrow="false" v-bind:delay_on_mouse_over="100">
                                <span slot="reference" class="sima-icon sir-envelope" v-bind:class="{'no-info':!model.main_or_any_email}"></span>
                                <div class="partner-list-popper" v-if="model.email_addresses.length">
                                    <div class="first-child-tooltip-content">
                                        <div class="tooltip-header">
                                            <i class="sima-icon sir-envelope"></i>
                                            <span><?=Yii::t('Partner', 'EmailAddresses')?> ({{model.email_addresses.length}})</span>
                                        </div><!-- end tooltip-header -->
                                        <div class="tooltip-body">
                                            <div v-for="email_address in model.email_addresses">
                                                <div class="copy-to-clipboard" v-copy-to-clipboard>{{email_address.DisplayName}}</div>
                                                <div class="highlight-comment">{{email_address.comment}}</div>
                                            </div>
                                        </div><!-- end tooltip-body -->
                                    </div>
                                </div>
                            </sima-popup>
                            <!-- Adrese sima-popup -->
                            <sima-popup :show_arrow="false" v-bind:delay_on_mouse_over="100">
                                <span slot="reference" class="sima-icon sir-map-marker-alt" v-bind:class="{'no-info':!model.main_or_any_address}"></span>
                                <div class="partner-list-popper" v-if="model.addresses.length">
                                    <div class="first-child-tooltip-content">
                                        <div class="tooltip-header">
                                            <i class="sima-icon sir-map-marker-alt"></i>
                                            <span><?=Yii::t('Company', 'Addresses')?> ({{model.addresses.length}})</span>
                                        </div><!-- end tooltip-header -->
                                        <div class="tooltip-body" style="margin-left: 20px;">
                                            <div v-for="address in model.addresses" class="copy-to-clipboard" v-copy-to-clipboard>{{address.DisplayName}}</div>
                                        </div><!-- end tooltip-body -->
                                    </div>
                                </div>
                            </sima-popup>
                            <!-- Ostale adrese sima-popup -->
                            <sima-popup :show_arrow="false" v-bind:delay_on_mouse_over="100">
                                <span slot="reference" class="sima-icon sir-globe-americas" v-bind:class="{'no-info':!model.contacts.length}"></span>
                                <div class="partner-list-popper" v-if="model.contacts.length">
                                    <div class="first-child-tooltip-content">
                                        <div class="tooltip-header">
                                            <i class="sima-icon sir-globe-americas"></i>
                                            <span><?=Yii::t('Partner', 'OtherContacts')?> ({{model.contacts.length}})</span>
                                        </div><!-- end tooltip-header -->
                                        <div class="tooltip-body">
                                            <div v-for="contact in model.contacts" class="copy-to-clipboard" v-copy-to-clipboard>{{contact.DisplayName}}</div>
                                        </div><!-- end tooltip-body -->
                                    </div>
                                </div>
                            </sima-popup>
                            <!-- Bankovni racuni sima-popup -->
                            <sima-popup :show_arrow="false" v-bind:delay_on_mouse_over="100">
                                <span slot="reference" class="sima-icon sir-euro-sign" v-bind:class="{'no-info':!model.bank_accounts.length}"></span>
                                <div class="partner-list-popper" v-if="model.bank_accounts.length">
                                    <div class="first-child-tooltip-content">
                                        <div class="tooltip-header">
                                            <i class="sima-icon sir-euro-sign"></i>
                                            <span><?=Yii::t('Company', 'BankAccounts')?> ({{model.bank_accounts.length}})</span>
                                        </div><!-- end tooltip-header -->
                                        <div class="tooltip-body">
                                            <div v-for="bank_account in model.bank_accounts">
                                                <div class="copy-to-clipboard" v-copy-to-clipboard>{{bank_account.number}}</div>
                                                <div class="copy-to-clipboard" v-copy-to-clipboard>{{bank_account.bank.DisplayName}}</div>
                                                <div class="highlight-comment">{{bank_account.comment}}</div>
                                            </div>
                                        </div><!-- end tooltip-body -->
                                    </div>
                                </div>
                            </sima-popup>
                            <!-- Komentar sima-popup -->
                            <sima-popup :show_arrow="false" v-bind:delay_on_mouse_over="100">
                                <span slot="reference" class="sima-icon sir-quote-left" v-bind:class="{'no-info':!model.comment}"></span>
                                <div class="partner-list-popper" v-if="model.comment">
                                    <div class="first-child-tooltip-content">
                                        <div class="tooltip-header">
                                            <i class="sima-icon sir-quote-left" ></i>
                                            <span><?=Yii::t('Person', 'Comment')?></span>
                                        </div><!-- end tooltip-header -->
                                        <div class="tooltip-body">
                                            <div v-if="model.comment !== ''" class="copy-to-clipboard" v-copy-to-clipboard>{{model.comment}}</div>
                                        </div><!-- end tooltip-body -->
                                    </div>
                                </div>
                            </sima-popup>
                        </div><!-- end all-info -->                        
                    </div><!-- end partner-bottombar -->

                    
                </div> 
            </div>
            <div class="load-more" v-if="model_list.length && list_count > limit">
                <div>
                    <span><?=Yii::t('SIMAAddressbook.SIMAAddressbook', 'Loaded')?></span>
                    <span>{{model_list.length}}</span>
                    <span><?=Yii::t('SIMAAddressbook.SIMAAddressbook', 'From')?></span>
                    <span>{{list_count}}</span>
                    <span><?=Yii::t('SIMAAddressbook.SIMAAddressbook', 'Contacts')?></span>
                </div>
                <sima-button class="cta" :class="{'_disabled':model_list.length == list_count}" 
                    @click="loadMore">
                    <i class="sima-icon sil-long-arrow-down"></i><hr>
                    <span class="sima-btn-title"><?=Yii::t('SIMAAddressbook.SIMAAddressbook', 'LoadMore')?></span>
                </sima-button>
            </div><!-- end load-more -->
        </vue-perfect-scrollbar>
    </div>
</script>

