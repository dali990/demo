/* global Vue, sima */

Vue.filter("formatNumber", function (value) {
    if (typeof value === 'number')
    {
        value = (value/1).toFixed(2);
    }
    
    var numeric_separators = sima.getNumericSeparators();
    
    var left_side = value;
    var right_side = null;
    if (value.indexOf('.') > -1)
    {
        var value_parts = value.split('.');
        var left_side = value_parts[0];
        var right_side = value_parts[1];
    }
    
    var formated_value = left_side.replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1' + numeric_separators['thousands']);
    
    if (right_side !== null)
    {
        formated_value += numeric_separators['decimals'] + right_side;
    }
    
    return formated_value;
});