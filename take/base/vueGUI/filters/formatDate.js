Vue.filter("formatDate", function (value, format = '', is_date_time = false, is_miliseonds = false) {
    if (format === '')
    {
        format = 'DD.MM.YYYY.';
        if (is_date_time)
        {
            format += ' HH:mm:ss';
            if(is_miliseonds)
            {
                format += ' .u';
            }
        }
    }
    if (value !== '')
    {
        return moment(value, format).locale(sima.getCurrentLanguage()).fromNow();
    }
    else
    {
        return '';
    }
});