/* global Vue */

Vue.filter("round", function (value, decimal_places) {
    var _disp = Math.pow(10,decimal_places);
    return Math.round(value * _disp) / _disp;
});