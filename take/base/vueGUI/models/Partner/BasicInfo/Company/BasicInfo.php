<?php $uniq = SIMAHtml::uniqid()?>

<script type="text/x-template" id="application-company-basic_info">
    <div class="basic-info application-company-basic_info">

        <div class="binfo-left">
            <div class='binfo-avatar' v-bind::title="this.model.DisplayName">
                <sima-model-display-icon v-bind:model='model' icon_prefix="sil"></sima-model-display-icon>
            </div>
        </div><!-- end binfo-left -->

        <div class="binfo-right">
            <div class="binfo-header">
                <div class="binfo-title-wrap">
                    <h2 class="binfo-title master" v-bind::title="this.model.DisplayName">
                        {{ this.model.DisplayName }}
                    </h2>                    
                    <ul class="binfo-title slave">
                        <li v-for="partner_to_partner_list in partner_to_partner_lists">
                            <i class="sima-icon sis-circle"></i>
                            {{partner_to_partner_list.partner_list.DisplayName}}
                        </li>
                    </ul>
                </div> 
                <sima-model-options 
                    class="binfo-options"
                    v-bind:model="this.model"
                    v-bind:exclude_options="['open']"
                ></sima-model-options>
            </div><!-- end binfo-header -->
            <vue-perfect-scrollbar 
                class="binfo-body perfect-scrollbar sima-layout-panel"
                v-bind:settings="{ minScrollbarLength: 30, suppressScrollY: true }"
            >
                <div class="binfo-item pib-item flex-column">
                    <div>
                        <div 
                            class="item-icon sima-icon sir-pib"
                            title="<?=Yii::t('Company', 'PIBfull')?>"
                        ></div>
                        <span 
                            v-html="model.PIB"
                            v-if="model.PIB !== ''"
                            class="item-value copy-element"
                            title="<?=Yii::t('Contact', 'CopyToClipboard')?>"
                            v-copy-to-clipboard
                        ></span>   
                        <div
                            v-else
                            class="item-value _disable"
                        ><?=Yii::t('JSTranslations', 'NoInformation')?></div> 
                    </div>
                    <div>
                        <div 
                            class="item-icon sima-icon sir-mb"
                            title="<?=Yii::t('Company', 'MB')?>"
                        ></div>
                        <div 
                            v-html="model.MB"
                            v-if="model.MB !== ''"
                            class="item-value copy-element"
                            title="<?=Yii::t('Contact', 'CopyToClipboard')?>"
                            v-copy-to-clipboard
                        ></div>
                        <div 
                            v-else
                            class="item-value _disable"
                        ><?=Yii::t('JSTranslations', 'NoInformation')?></div> 
                    </div>
                </div><!-- end pib-item -->

                <div class="binfo-item phone-item suppress-scroll-y mincontent">
                    <div 
                        class="item-icon sima-icon sil-phone"
                        title="<?=Yii::t('Partner', 'Phones')?>"
                    ></div>
                    <vue-perfect-scrollbar
                        v-if="model.partner.main_or_any_phone !== ''"
                        class="item-value perfect-scrollbar sima-layout-panel"
                        v-bind:settings="{ minScrollbarLength: 30, suppressScrollX: true }"
                    >
                        <ul title="<?=Yii::t('Partner', 'Phone')?>">
                            <li v-for="phone_number in phone_numbers">
                                <span 
                                    class="value copy-element" 
                                    v-html="phone_number.DisplayName"
                                    title="<?=Yii::t('Contact', 'CopyToClipboard')?>"
                                    v-copy-to-clipboard
                                ></span>
                                <span
                                    class="comment copy-element"
                                    v-copy-to-clipboard
                                    :title="phone_number.comment"
                                    v-if="phone_number.comment !== ''"
                                >{{phone_number.comment}}</span>
                            </li>
                        </ul>
                    </vue-perfect-scrollbar>
                    <div v-else class="item-value _disable"><?=Yii::t('JSTranslations', 'NoInformation')?></div> 
                </div><!-- end phone-item -->

                <div class="binfo-item envelope-item suppress-scroll-y mincontent">
                    <div class="item-icon sima-icon sil-envelope" title="<?=Yii::t('Partner', 'EmailAddresses')?>"></div>
                    <vue-perfect-scrollbar
                        v-if="model.partner.main_or_any_email !== ''"
                        class="item-value perfect-scrollbar sima-layout-panel"
                        v-bind:settings="{ minScrollbarLength: 30, suppressScrollX: true }"
                    >
                        <ul>
                            <li v-for="email_address in email_addresses">
                                <span class="value copy-element"
                                    v-html="email_address.DisplayName"
                                    title="<?=Yii::t('Contact', 'CopyToClipboard')?>"
                                    v-copy-to-clipboard
                                ></span>
                                <span 
                                    class="comment copy-element"
                                    v-copy-to-clipboard
                                    :title="email_address.comment"
                                    v-if="email_address.comment !== ''"
                                >{{email_address.comment}}</span>
                            </li>
                        </ul>
                    </vue-perfect-scrollbar>
                    <div v-else class="item-value _disable"><?=Yii::t('JSTranslations', 'NoInformation')?></div> 
                </div><!-- end envelope-item -->

                <div class="binfo-item map-marker-item suppress-scroll-y">
                    <div class="item-icon sima-icon sil-map-marker-alt" title="<?=Yii::t('Company', 'Addresses')?>"></div>
                    <vue-perfect-scrollbar
                        v-if="addresses.length > 0"
                        title="<?=Yii::t('Contact', 'CopyToClipboard')?>"
                        class="item-value perfect-scrollbar sima-layout-panel"
                        v-bind:settings="{ minScrollbarLength: 30, suppressScrollX: true }"
                    >
                        <ul>
                            <li v-for="address in addresses">
                                <span 
                                    class="value copy-element"
                                    v-copy-to-clipboard
                                    title="<?=Yii::t('Contact', 'CopyToClipboard')?>"
                                >{{address.DisplayName}}</span>
                            </li>
                        </ul>
                    </vue-perfect-scrollbar>
                    <div v-else class="item-value _disable"><?=Yii::t('JSTranslations', 'NoInformation')?></div>
                </div><!-- end map-marker-item -->

                <div class="binfo-item bank-acc-item suppress-scroll-y">
                    <div class="item-icon sima-icon sil-money-check-alt" title="<?=Yii::t('Company', 'BankAccounts')?>"></div>
                    <vue-perfect-scrollbar
                        v-if="bank_accounts.length > 0"
                        class="item-value perfect-scrollbar sima-layout-panel"
                        v-bind:settings="{ minScrollbarLength: 30, suppressScrollX: true }"
                    >
                        <ul>
                            <li v-for="bank_account in bank_accounts">
                                <span 
                                    class="value copy-element"
                                    title="<?=Yii::t('Contact', 'CopyToClipboard')?>"
                                    v-copy-to-clipboard
                                >{{bank_account.number}}</span>
                                <span
                                    class="comment copy-element"
                                    v-copy-to-clipboard
                                    v-bind:title="bank_account.bank.DisplayName"
                                >{{bank_account.bank.DisplayName}}</span>
                            </li>
                        </ul>
                    </vue-perfect-scrollbar>
                    <div v-else class="item-value _disable"><?=Yii::t('JSTranslations', 'NoInformation')?></div> 
                </div><!-- end bank-acc-item -->

                <div class="binfo-item globe-item suppress-scroll-y">
                    <div class="item-icon sima-icon sil-globe" title="<?=Yii::t('Partner', 'OtherContacts')?>"></div>
                    <vue-perfect-scrollbar
                        v-if="contacts.length > 0"
                        class="item-value perfect-scrollbar sima-layout-panel"
                        v-bind:settings="{ minScrollbarLength: 30, suppressScrollX: true }"
                    >
                        <ul>
                            <li v-for="contact in contacts">
                                <span 
                                    class="value"
                                    title="<?=Yii::t('Contact', 'CopyToClipboard')?>"
                                >
                                    <span style="float: left;">{{contact.type.name}}: &nbsp;</span> 
                                    <span class="copy-element" style="float: left;" v-copy-to-clipboard>{{contact.value}}</span>
                                </span><br>
                                <span
                                    class="comment copy-element"
                                    v-copy-to-clipboard
                                    v-bind:title="contact.comment"
                                    v-if="contact.comment !== ''"
                                >{{contact.comment}}</span>
                            </li>
                        </ul>
                    </vue-perfect-scrollbar>
                    <div v-else class="item-value _disable"><?=Yii::t('JSTranslations', 'NoInformation')?></div> 
                </div><!-- end globe-item -->

                <div class="binfo-item quote-item suppress-scroll-y"> 
                    <div class="item-icon sima-icon sil-quote-left" title="<?=Yii::t('Person', 'Comment')?>"></div>
                    <vue-perfect-scrollbar 
                        v-if="model.comment !== ''"
                        class="item-value perfect-scrollbar sima-layout-panel"
                        v-bind:settings="{ minScrollbarLength: 30, suppressScrollX: true }"
                    >
                        <span 
                            class="copy-element"
                            v-copy-to-clipboard
                            v-html="model.comment" 
                            title="<?=Yii::t('Contact', 'CopyToClipboard')?>"
                        ></span>
                    </vue-perfect-scrollbar>
                    <div v-else class="item-value _disable"><?=Yii::t('JSTranslations', 'NoInformation')?></div> 
                </div><!-- end quote-item -->
            </vue-perfect-scrollbar><!-- end binfo-body -->
        </div><!-- end binfo-right -->
                
    </div><!-- end company-basic-info -->
</script>