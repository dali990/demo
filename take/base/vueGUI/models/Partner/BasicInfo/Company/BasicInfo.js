/* global Vue, sima */

Vue.component('application-company-basic_info', {
    template: '#application-company-basic_info',
    props:  {
        model: {
            validator: sima.vue.ProxyValidator
        }
    },
    computed: {
        phone_numbers: function() {
            return (typeof this.model.partner.phone_numbers !== 'undefined' && this.model.partner.phone_numbers !== '') ?
                this.model.partner.phone_numbers :
                [];
        },
        email_addresses: function() {
            return (typeof this.model.partner.email_addresses !== 'undefined' && this.model.partner.email_addresses !== '') ?
                this.model.partner.email_addresses :
                [];
        },
        bank_accounts: function() {
            return (typeof this.model.partner.bank_accounts !== 'undefined' && this.model.partner.bank_accounts !== '') ?
                this.model.partner.bank_accounts :
                [];
        },
        contacts: function() {
            return (typeof this.model.partner.contacts !== 'undefined' && this.model.partner.contacts !== '') ?
                this.model.partner.contacts :
                [];
        },
        addresses: function() {
            return (typeof this.model.partner.addresses !== 'undefined' && this.model.partner.addresses !== '') ?
                this.model.partner.addresses :
                [];
        },
        partner_to_partner_lists: function() {
            return (typeof this.model.partner.partner_to_partner_lists !== 'undefined' && this.model.partner.partner_to_partner_lists !== '') ?
                this.model.partner.partner_to_partner_lists :
                [];
        },
        isCommentEmpty: function() {
            return sima.isEmpty(this.model.comment) ? true : false;
        }
    },
    data: function () {
        return {
        };
    },
    mounted: function() {
        var _this = this;
        $(this.$el).on('destroyed', function(){
            _this.$destroy();
        });
    }
});
