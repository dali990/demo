<?php $uniq = SIMAHtml::uniqid()?>

<script type="text/x-template" id="application-person-basic_info">
    <div class="basic-info application-person-basic_info dark-background">
        <div class="binfo-left">
            <div class="binfo-avatar">
                <sima-model-display-icon 
                    v-bind:model="model"
                    icon_prefix="sil"
                ></sima-model-display-icon>
            </div>
            <div class='binfo-user-status'>
                <div v-if="model.user" class="user-status-wrap">
                    <i class="sima-icon sil-user"></i>
                    <span><?=Yii::t('Person', 'UserInSystem')?></span>
                </div>
                <div v-else-if="check_access_add_user" class="user-status-wrap">
                    <i class="sima-icon sil-user"></i>
                    <span class="status-not"><?=Yii::t('Person', 'NotUserInSystem')?></span>
                    <sima-button class="sima-btn icon-only btn-transparent btn-half" v-bind:class="{_disabled:!dataTrue}">
                        <span class="sima-icon sil-plus" v-on:click='addUser'></span>
                    </sima-button>
                </div>
                <div v-else class="user-status-wrap _disable">
                    <i class="sima-icon sil-user"></i>
                    <span><?=Yii::t('Person', 'NotUserInSystem')?></span>
                </div>
                <div v-if="model.employee" class="user-status-wrap">
                    <i class="sima-icon sil-briefcase"></i>
                    <span><?=Yii::t('Person', 'EmployeeInSystem')?></span>
                </div>
                <div v-else-if="check_access_add_employee" class="user-status-wrap">
                    <i class="sima-icon sil-briefcase"></i>
                    <span class="status-not"><?=Yii::t('Person', 'NotEmployeeInSystem')?></span>
                    <sima-button class="sima-btn icon-only btn-transparent btn-half">
                        <span class="sima-icon sil-plus" v-on:click='addEmployee'></span>
                    </sima-button>
                </div>
                <div v-else class="user-status-wrap _disable">
                    <i class="sima-icon sil-briefcase"></i>
                    <span><?=Yii::t('Person', 'NotEmployeeInSystem')?></span>
                </div>
            </div>
        </div>
        <div class="binfo-right">
            <div class="binfo-header">
                <div class="binfo-title-wrap">
                    <h2 class="binfo-title master" v-bind:title="this.model.DisplayName" >
                        {{ this.model.DisplayName }}
                    </h2>
                    <ul class="binfo-title slave">
                        <li v-for="partner_to_partner_list in partner_to_partner_lists">
                            <i class="sima-icon sis-circle"></i>
                            {{partner_to_partner_list.partner_list.DisplayName}}
                        </li>
                    </ul>
                </div>
                <sima-model-options
                    class="binfo-options"
                    v-bind:model="this.model"
                    v-bind:exclude_options="['open']"
                ></sima-model-options> 
            </div>
            <vue-perfect-scrollbar 
                class="binfo-body perfect-scrollbar sima-layout-panel"
                v-bind:settings="{ minScrollbarLength: 30, suppressScrollY: true }"
            >
                <div class="binfo-item building-item">
                    <div 
                        class="item-icon sima-icon sil-building"
                        title="<?=Yii::t('Company', 'Company')?>"
                    ></div>
                    <vue-perfect-scrollbar
                        class="item-value perfect-scrollbar sima-layout-panel"
                        v-bind:settings="{ minScrollbarLength: 30, suppressScrollX: true }"
                    >
                        <span 
                            v-html="company_name" 
                            v-bind:title="model.company.DisplayName"
                            v-copy-to-clipboard
                        ></span><br>
                        <span 
                            class="position copy-element"
                            v-if="work_positions.length > 0" 
                            v-for="work_position in work_positions"
                            title="<?=Yii::t('Contact', 'CopyToClipboard')?>"
                            v-copy-to-clipboard
                        >{{work_position.value}}<br></span>
                        <span v-else><?=Yii::t('Person','DoNotHaveWorkingPosition')?></span>
                    </vue-perfect-scrollbar>
                </div><!-- end building-item -->

                <div class="binfo-item phone-item suppress-scroll-y mincontent">
                    <div class="item-icon sima-icon sil-phone" title="<?=Yii::t('Contact', 'CopyToClipboard')?>"></div>
                    <vue-perfect-scrollbar
                        v-if="model.partner.main_or_any_phone !== ''"
                        class="item-value perfect-scrollbar sima-layout-panel"
                        v-bind:settings="{ minScrollbarLength: 30, suppressScrollX: true }"
                    >
                        <ul>
                            <li v-for="phone_number in phone_numbers">
                                <span 
                                    class="value copy-element" 
                                    title="<?=Yii::t('Contact', 'CopyToClipboard')?>"
                                    v-copy-to-clipboard
                                >{{phone_number.DisplayName}}</span>
                                <span 
                                    class="comment copy-element"
                                    v-copy-to-clipboard
                                    v-bind:title="phone_number.comment"
                                    v-if="phone_number.comment !== ''"
                                >{{phone_number.comment}}</span>
                            </li>
                        </ul>
                    </vue-perfect-scrollbar>
                    <div v-else class="item-value _disable"><?=Yii::t('JSTranslations', 'NoInformation')?></div> 
                </div><!-- end phone-item -->

                <div class="binfo-item envelope-item suppress-scroll-y mincontent">
                    <div class="item-icon sima-icon sil-envelope" title="<?=Yii::t('Partner', 'EmailAddresses')?>"></div>
                    <vue-perfect-scrollbar
                        v-if="model.partner.main_or_any_email !== ''"
                        class="item-value perfect-scrollbar sima-layout-panel"
                        v-bind:settings="{ minScrollbarLength: 30, suppressScrollX: true }"
                    >
                        <ul>
                            <li v-for="email_address in email_addresses">
                                <span 
                                    class="value copy-element"
                                    title="<?=Yii::t('Contact', 'CopyToClipboard')?>"
                                    v-copy-to-clipboard
                                >{{email_address.DisplayName}}</span>
                                <span 
                                    class="comment copy-element"
                                    v-copy-to-clipboard
                                    v-bind:title="email_address.comment"
                                    v-if="email_address.comment !== ''"
                                >{{email_address.comment}}</span>
                            </li>
                        </ul>
                    </vue-perfect-scrollbar>
                    <div v-else class="item-value _disable"><?=Yii::t('JSTranslations', 'NoInformation')?></div> 
                </div><!-- end envelope-item -->

                <div class="binfo-item map-marker-item suppress-scroll-y">
                    <div class="item-icon sima-icon sil-map-marker-alt" title="<?=Yii::t('Company', 'Addresses')?>"></div>
                    <vue-perfect-scrollbar
                        v-if="addresses.length > 0"
                        title="<?=Yii::t('Contact', 'CopyToClipboard')?>"
                        class="item-value perfect-scrollbar sima-layout-panel"
                        v-bind:settings="{ minScrollbarLength: 30, suppressScrollX: true }"
                    >
                        <ul>
                            <li v-for="address in addresses">
                                <span 
                                    class="value copy-element"
                                    v-copy-to-clipboard
                                    title="<?=Yii::t('Contact', 'CopyToClipboard')?>"
                                >{{address.DisplayName}}</span>
                            </li>
                        </ul>
                    </vue-perfect-scrollbar>
                    <div v-else class="item-value _disable"><?=Yii::t('JSTranslations', 'NoInformation')?></div> 
                </div><!-- end map-marker-item -->

                <div class="binfo-item globe-item suppress-scroll-y">
                    <div class="item-icon sima-icon sil-globe" title="<?=Yii::t('Partner', 'OtherContacts')?>"></div>
                    <vue-perfect-scrollbar
                        v-if="contacts.length > 0"
                        class="item-value perfect-scrollbar sima-layout-panel"
                        v-bind:settings="{ minScrollbarLength: 30, suppressScrollX: true }"
                    >
                        <ul>
                            <li v-for="contact in contacts">
                                <span 
                                    class="value"
                                    title="<?=Yii::t('Contact', 'CopyToClipboard')?>"
                                >
                                    <span style="float: left;">{{contact.type.name}}: &nbsp;</span> 
                                    <span class="copy-element" style="float: left;" v-copy-to-clipboard>{{contact.value}}</span>
                                </span><br>
                                <span
                                    class="comment copy-element"
                                    v-copy-to-clipboard
                                    title="<?=Yii::t('Contact', 'CopyToClipboard')?>"
                                    v-if="contact.comment !== ''"
                                >{{contact.comment}}</span>
                            </li>
                        </ul>
                    </vue-perfect-scrollbar>
                    <div v-else class="item-value _disable"><?=Yii::t('JSTranslations', 'NoInformation')?></div> 
                </div><!-- end globe-item -->

                <div class="binfo-item quote-item suppress-scroll-y">
                    <div class="item-icon sima-icon sil-quote-left" title="<?=Yii::t('Person', 'Comment')?>"></div>
                    <vue-perfect-scrollbar 
                        v-if="model.comment !== ''"
                        class="item-value perfect-scrollbar sima-layout-panel"
                        v-bind:settings="{ minScrollbarLength: 30, suppressScrollX: true }"
                    >
                        <span 
                            class="copy-element"
                            v-html="model.comment" 
                            title="<?=Yii::t('Contact', 'CopyToClipboard')?>"
                            v-copy-to-clipboard
                        ></span>
                    </vue-perfect-scrollbar>
                    <div v-else class="item-value _disable"><?=Yii::t('JSTranslations', 'NoInformation')?></div> 
                </div><!-- end map-marker-item -->
                
            </vue-perfect-scrollbar>
        </div>
    </div>
</script>