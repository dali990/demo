/* global Vue, sima */

Vue.component('application-person-basic_info', {
    template: '#application-person-basic_info',
    props:  {
        model: {
            validator: sima.vue.ProxyValidator
        }
    },
    computed: {
        curr_user_id: function(){
            return sima.getCurrentUserId();
        },
        isCommentEmpty: function() {
            return sima.isEmpty(this.model.comment) ? true : false;
        },
        haveSlava: function() {
            var have_slava = false;
            if (typeof this.model.employee !== 'undefined' && this.model.employee !== '')
            {
                if(typeof this.model.employee.slava !== 'undefined' && this.model.employee.slava !== '')
                {
                    have_slava = true;
                }
            }
            return have_slava;
        },
        can_edit_slava: function() {
            var can_edit_slava = false;
            if (typeof this.model.employee !== 'undefined' && this.model.employee !== '')
            {
                can_edit_slava = this.model.employee.can_logged_in_user_edit_slava_for_this_emp;
            }
            return can_edit_slava;
        },
        phone_numbers: function() {
            return (typeof this.model.partner.phone_numbers !== 'undefined' && this.model.partner.phone_numbers !== '') ?
                this.model.partner.phone_numbers :
                [];
        },
        email_addresses: function() {
            return (typeof this.model.partner.email_addresses !== 'undefined' && this.model.partner.email_addresses !== '') ?
                this.model.partner.email_addresses :
                [];
        },
        addresses: function() {
            return (typeof this.model.partner.addresses !== 'undefined' && this.model.partner.addresses !== '') ?
                this.model.partner.addresses :
                [];
        },
        contacts: function() {
            return (typeof this.model.partner.contacts !== 'undefined' && this.model.partner.contacts !== '') ?
                this.model.partner.contacts :
                [];
        },
        partner_to_partner_lists: function() {
            return (typeof this.model.partner.partner_to_partner_lists !== 'undefined' && this.model.partner.partner_to_partner_lists !== '') ?
                this.model.partner.partner_to_partner_lists :
                [];
        },
        occupation_name: function() {
            var occupation_name = '';
            if (typeof this.model.active_work_contract.occupation !== 'undefined' && this.model.active_work_contract.occupation !== '')
            {
                occupation_name = this.model.active_work_contract.occupation.name;
            }
            return occupation_name;
        },
        qualification_level: function() {
            var qualification_level = '';
            if (typeof this.model.qualification_level !== 'undefined' && this.model.qualification_level !== '')
            {
                qualification_level = this.model.qualification_level.DisplayName;
            }
            return qualification_level;
        },
        company_name: function() {
            return (typeof this.model.company !== 'undefined' && this.model.company !== '') ? 
                this.model.company.DisplayHTML : 
                sima.translate('PersonUnemployed');
        },
        work_positions: function() {
            var positions = [];
            var used_positions = [];
            if (typeof this.model.employee !== 'undefined' && this.model.employee !== '')
            {
                if (typeof this.model.employee.active_work_contract !== 'undefined' && this.model.employee.active_work_contract !== '')
                {
                    positions.push({ 
                        'value' :this.model.employee.active_work_contract.work_position.DisplayName,
                        'id': this.model.employee.active_work_contract.work_position.id
                    });
                    used_positions.push(this.model.employee.active_work_contract.work_position.id);
                }
            }
            $.each(this.model.work_positions, function(index, value)
            {
                if (used_positions.indexOf(value.id) === -1)
                {
                    positions.push({
                        'value': value.DisplayName,
                        'id': value.id
                    });
                    used_positions.push(value.id);
                }
            });
            return positions;
        },
        check_access_hr_work_contract_read: function() {
            return sima.params.getParam('check_access_hr_work_contract_read');
        },
        check_access_hr_occupation: function() {
            return sima.params.getParam('check_access_hr_occupation');
        },
        check_access_add_user: function() {
            return sima.params.getParam('check_access_add_user');
        },
        check_access_add_employee: function() {
            return sima.params.getParam('check_access_add_employee');
        },
        check_access_change_all_passwords: function() {
            return sima.params.getParam('check_access_change_all_passwords');
        },
        check_access_reset_user_password: function() {
            return sima.params.getParam('check_access_reset_user_password');
        },
        dataTrue: function(){
            this.model.generate_username;
            return this.model.shotStatus('generate_username') === 'OK';
        }
    },
    data: function () {
        return {
        };
    },
    mounted: function() {
        var _this = this;
        $(this.$el).on('destroyed', function(){
            _this.$destroy();
        });
    },
    methods: {
        addEmployee: function() {
            sima.model.addEmployee(this.model.id);
        },
        addUser: function() {
            sima.model.addUser(this.model.id, this.model.generate_username);
        },
        changePassword: function() {
            sima.model.form("User", this.model.id,{formName: "chPass"});
        },
        resetPassword: function() {
            sima.resetPassword(this.model.id);
        }
    }
});