<script type="text/x-template" id="application-partner-basic_info">
    <div class="application-partner-basic-info">
        <application-person-basic_info
            v-if="model.person"
            v-bind:model="model.person"
        >
        </application-person-basic_info>
        <application-company-basic_info
            v-else-if="model.company_model"
            v-bind:model="model.company_model"
        >
        </application-company-basic_info>
    </div>
</script>