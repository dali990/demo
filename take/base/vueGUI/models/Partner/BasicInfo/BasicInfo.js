/* global Vue, sima */

Vue.component('application-partner-basic_info', {
    template: '#application-partner-basic_info',
    props:  {
        model: {
            validator: sima.vue.ProxyValidator
        }
    },
    computed: {

    },
    data: function () {
        return {
        };
    },
    mounted: function() {
        var _this = this;
        $(this.$el).on('destroyed', function(){
            _this.$destroy();
        });
    },
    methods: {
    }
});
