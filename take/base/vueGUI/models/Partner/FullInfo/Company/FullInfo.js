/* global Vue, sima */

Vue.component('application-company-full_info', {
    template: '#application-company-full_info',
    props: {
        model: {
            validator: sima.vue.ProxyValidator
        }
    },
    computed: {
        dataTrue: function(){
            this.model.partner;
            this.model.company_to_themes;
            return this.model.shotStatus('partner') === 'OK' &&
                this.model.shotStatus('company_to_themes') === 'OK';
        },
        phone_numbers: function() {
            return (typeof this.model.partner.phone_numbers !== 'undefined' && this.model.partner.phone_numbers !== '') ?
                this.model.partner.phone_numbers :
                [];
        },
        email_addresses: function() {
            return (typeof this.model.partner.email_addresses !== 'undefined' && this.model.partner.email_addresses !== '') ?
                this.model.partner.email_addresses :
                [];
        },
        contacts: function() {
            return (typeof this.model.partner.contacts !== 'undefined' && this.model.partner.contacts !== '') ?
                this.model.partner.contacts :
                [];
        },
        partner_to_partner_lists: function() {
            return (typeof this.model.partner.partner_to_partner_lists !== 'undefined' && this.model.partner.partner_to_partner_lists !== '') ?
                this.model.partner.partner_to_partner_lists :
                [];
        },
        check_access_model_agricultural_holding_see: function() {
            return sima.params.getParam('check_access_model_agricultural_holding_see');
        },
        check_access_model_agricultural_holding_form_open: function() {
            return sima.params.getParam('check_access_model_agricultural_holding_form_open');
        },
        agricultural_holding_number: function() {
            return (typeof this.model.partner.agricultural_holding !== 'undefined' && this.model.partner.agricultural_holding !== '') ? 
                this.model.partner.agricultural_holding.number : 
                '';
        }
    },
    data: function () {
        return {};
    },
    mounted: function() {
        var _this = this;
        $(this.$el).on('destroyed', function(){
            _this.$destroy();
        });
    },
    methods: {
        addPartnerList: function() {
            sima.model.form("PartnerToPartnerList", [],
                {
                    init_data: {
                        'PartnerToPartnerList': {
                            'partner_id': {
                                'init': this.model.id,
                                'disabled': true
                            },
                            'partner_list_id': {
                                'model_filter': {
                                    'filter_scopes': ['withUserAccessToList', 'withoutSystems']
                                }
                            }
                        }
                    }
                }
            );
        },
        addCompanyToTheme: function() {
            sima.model.form("CompanyToTheme", [],
                {
                    init_data: {
                        'CompanyToTheme': {
                            'company_id': {
                                'init': this.model.id,
                                'disabled': true
                            }
                        }
                    }
                }
            );
        },
        addPerson: function() {
            var init_data = sima.getCurrentCompanyId() !== this.model.id ?
                {
                    'Person': {
                        'company_id': {
                            'init': this.model.id,
                            'disabled': true
                        }
                    }
                } :
                {};
            sima.model.form("Person", [],
                {
                    init_data: init_data
                }
            );
        },
        addAgriculturalHolding: function() {
            sima.model.form("AgriculturalHolding", [],
                {
                    init_data: {
                        'AgriculturalHolding': {
                            'id': this.model.id
                        }
                    }
                }
            );
        }
    }
});
