<script type="text/x-template" id="application-company-full_info">
    <div class=" application-company-full-info sima-layout-panel _splitter _vertical">
        <div class="sima-layout-panel">
            <div class="sima-layout-panel _splitter _horizontal" data-sima-layout-init='<?= CJSON::encode(['min_width' => 600, 'max_width' => 1000,]) ?>' >
                <div class="sima-layout-panel" data-sima-layout-init='<?= CJSON::encode(["proportion" => 0.7]) ?>'>
                    <div class="body">
                        <h3>Statistika zaposljenja:</h3>                        
                        <div class="item">
                            <div class='title'>
                                <?=Yii::t('Company', 'NumberOfEmployeesInAdressbook')?>: <span v-html="model.employees.length"></span>
                            </div>
                        </div>
                        <div class="item">
                            <div class="title">
                                <b><?=Yii::t('Partner', 'PartnerLists')?>:</b> 
                                <sima-button
                                    v-on:click="addPartnerList" 
                                    v-bind:class="{_disabled:!dataTrue}"
                                >
                                    <span class="fas fa-plus"></span>
                                </sima-button>
                            </div>
                            <div class="desc">
                                <ul>
                                    <li v-for="partner_to_partner_list in partner_to_partner_lists">
                                        {{partner_to_partner_list.partner_list.DisplayName}} 
                                        <sima-model-delete
                                            v-if="!partner_to_partner_list.partner_list.is_system && partner_to_partner_list.partner_list.has_user_access_to_list"
                                            v-bind:model="partner_to_partner_list"
                                            v-bind:class="{_disabled:!dataTrue}"
                                        >
                                        </sima-model-delete>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="item">
                            <div class="title">
                                <b><?=Yii::t('BaseModule.Theme','Themes')?>:</b> 
                                <sima-button
                                    v-on:click="addCompanyToTheme" 
                                    v-bind:class="{_disabled:!dataTrue}"
                                >
                                    <span class="fas fa-plus"></span>
                                </sima-button>
                            </div>
                            <div class="desc">
                                <ul v-if="model.company_to_themes.length > 0">
                                    <li v-for="company_to_theme in model.company_to_themes">
                                        <span v-html="company_to_theme.theme.DisplayHTML"></span>
                                        <sima-model-delete
                                            v-bind:model="company_to_theme"
                                            v-bind:class="{_disabled:!dataTrue}"
                                        >
                                        </sima-model-delete>
                                    </li>
                                </ul>
                                <ul v-else>
                                    <li><?=Yii::t('Company', 'DoesNotHaveTheme')?></li>
                                </ul>
                            </div>
                        </div>
                        <div class="item" v-if="check_access_model_agricultural_holding_see">
                            <div class="title">
                                <b><?=Yii::t('LegalModule.AgriculturalHolding','AgriculturalHoldingOwner')?>:</b> 
                            </div>
                            <div class="desc">
                                <span v-if="agricultural_holding_number !== ''"><?=Yii::t('BaseModule.Common', 'Yes')?></span>
                                <span v-else>
                                    <?=Yii::t('BaseModule.Common', 'No')?> 
                                    <sima-button
                                        v-on:click="addAgriculturalHolding"
                                        v-if="check_access_model_agricultural_holding_form_open"
                                    >
                                        <span class="fas fa-plus"></span>
                                    </sima-button>
                                </span>
                            </div>
                        </div>
                        <div class="item" v-if="agricultural_holding_number !== ''">
                            <div class="title">
                                <b><?=Yii::t('LegalModule.AgriculturalHolding','AgriculturalHoldingNumber')?>:</b> 
                            </div>
                            <div class="desc">
                                {{agricultural_holding_number}}
                            </div>
                        </div>
                    </div>
                </div>  
                
            </div>
        </div>  

        <div class="sima-layout-panel"data-sima-layout-init='<?= CJSON::encode(["proportion" => 0.3, 'min_width' => 250]) ?>'>
            <h3 style="padding: 0 10px;">
                <sima-button
                    v-on:click="addPerson" 
                    title="<?=Yii::t('Person', 'AddPerson')?>"
                    v-bind:class="{_disabled:!dataTrue}"
                >
                    <span class="fas fa-plus"></span>
                </sima-button>
                <?=Yii::t('Company','ListOfEmployees')?>:
            </h3>
            <div class="sima-el" v-for="employee in model.employees">
                <div class="img-holder">
                    <sima-model-display-icon v-bind:model='employee'></sima-model-display-icon>
                    <span class="status"></span>
                </div>
                <p class="name" v-html="employee.displayHTML"></p>
            </div>
            <p v-if="model.employees.length === 0"><?=Yii::t('Company','ThereAreNoEmployees')?></p>
        </div>
    </div>
</script>