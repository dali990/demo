Vue.component('application-person-full_info', {
    template: '#application-person-full_info',
    mixins: [sima.vue.getBaseComponent()],
    props: {
        model: {
            validator: sima.vue.ProxyValidator
        }
    },
    computed: {
        dataTrue: function(){
            this.model.partner;
            this.model.user;
            return this.model.shotStatus('partner') === 'OK' && 
                this.model.shotStatus('user') === 'OK';
        },
        curr_user_id: function(){
            return sima.getCurrentUserId();
        },
        company_name: function() {
            return (typeof this.model.company !== 'undefined' && this.model.company !== '') ? 
                this.model.company.DisplayHTML : 
                sima.translate('PersonNotInTheSystem');
        },
        work_positions: function() {
            var positions = [];
            var used_positions = [];
            if (typeof this.model.employee !== 'undefined' && this.model.employee !== '')
            {
                if (typeof this.model.employee.active_work_contract !== 'undefined' && this.model.employee.active_work_contract !== '')
                {
                    positions.push(this.model.employee.active_work_contract.work_position.DisplayName);
                    used_positions.push(this.model.employee.active_work_contract.work_position.id);
                }
            }
            $.each(this.model.work_positions, function(index, value)
            {
                if (used_positions.indexOf(value.id) === -1)
                {
                    positions.push(value.DisplayName);
                    used_positions.push(value.id);
                }
            });
            return positions;
        },
        phone_numbers: function() {
            return (typeof this.model.partner.phone_numbers !== 'undefined' && this.model.partner.phone_numbers !== '') ?
                this.model.partner.phone_numbers :
                [];
        },
        email_addresses: function() {
            return (typeof this.model.partner.email_addresses !== 'undefined' && this.model.partner.email_addresses !== '') ?
                this.model.partner.email_addresses :
                [];
        },
        contacts: function() {
            return (typeof this.model.partner.contacts !== 'undefined' && this.model.partner.contacts !== '') ?
                this.model.partner.contacts :
                [];
        },
        qualification_level: function() {
            var qualification_level = '';
            if (typeof this.model.qualification_level !== 'undefined' && this.model.qualification_level !== '')
            {
                qualification_level = this.model.qualification_level.DisplayName;
            }
            return qualification_level;
        },
        occupation_name: function() {
            var occupation_name = '';
            if (typeof this.model.active_work_contract.occupation !== 'undefined' && this.model.active_work_contract.occupation !== '')
            {
                occupation_name = this.model.active_work_contract.occupation.name;
            }
            return occupation_name;
        },
        haveSlava: function() {
            var have_slava = false;
            if (typeof this.model.employee !== 'undefined' && this.model.employee !== '')
            {
                if(typeof this.model.employee.slava !== 'undefined' && this.model.employee.slava !== '')
                {
                    have_slava = true;
                }
            }
            return have_slava;
        },
        check_access_hr_occupation: function() {
            return sima.params.getParam('check_access_hr_occupation');
        },
        active_annual_leave_decisions: function() {
            return sima.vue.shot.getters.model_list(this, 'AnnualLeaveDecision',{
                model_filter: {
                    'employee': {
                        'ids': this.model.employee.id
                    },
                    'year': {
                        'hr': {
                            'must_be_used_until': ['>=', sima.date.format(Date.now(), true)]
                        }
                    },
                    'order_by': 'year.hr.must_be_used_until ASC'
                }
            });
        },
        absances: function() {
            return sima.vue.shot.getters.model_list(this, 'Absence',{
                model_filter: {
                    'order_by': 'begin DESC',
                    'limit': 5,
                    'employee': {
                        'ids': this.model.employee.id
                    }
                }
            });
        },
        themes: function() {
            return sima.vue.shot.getters.model_list(this, 'Theme',{
                'model_filter': {
                    'persons_to_teams': {
                        'person': {
                            'ids': this.model.id
                        }
                    }
                },
                'scopes': [
                    'withoutTypeBaf',
                    'byDisplayName'
                ]
            });
        },
        partner_to_partner_lists: function() {
            return (typeof this.model.partner.partner_to_partner_lists !== 'undefined' && this.model.partner.partner_to_partner_lists !== '') ?
                this.model.partner.partner_to_partner_lists :
                [];
        },
        email_account: function() {
            return (typeof this.model.user.email_account !== 'undefined' && this.model.user.email_account !== '') ?
                this.model.user.email_account.DisplayName :
                '-';
        },
        check_access_hr_work_contract_read: function() {
            return sima.params.getParam('check_access_hr_work_contract_read');
        },
        check_access_change_all_passwords: function() {
            return sima.params.getParam('check_access_change_all_passwords');
        },
        check_access_reset_user_password: function() {
            return sima.params.getParam('check_access_reset_user_password');
        },
        check_access_person_admin_tab: function() {
            return sima.params.getParam('check_access_person_admin_tab');
        },
        check_access_log_in_as_user: function() {
            return sima.params.getParam('check_access_log_in_as_user');
        },
        personal_wiki_is_installed: function() {
            return sima.params.getParam('personal_wiki_is_installed');
        },
        personal_wiki_is_administrator: function() {
            return sima.params.getParam('personal_wiki_is_administrator');
        },
        doc_wiki_is_installed: function() {
            return sima.params.getParam('doc_wiki_is_installed');
        },
        doc_wiki_is_administrator: function() {
            return sima.params.getParam('doc_wiki_is_administrator');
        },
        spliter: function() {
            return {
                '_splitter _vertical': this.model.isUser === true && this.check_access_person_admin_tab
            };
        },
        check_access_model_agricultural_holding_see: function() {
            return sima.params.getParam('check_access_model_agricultural_holding_see');
        },
        check_access_model_agricultural_holding_form_open: function() {
            return sima.params.getParam('check_access_model_agricultural_holding_form_open');
        },
        agricultural_holding_number: function() {
            return (typeof this.model.partner.agricultural_holding !== 'undefined' && this.model.partner.agricultural_holding !== '') ? 
                this.model.partner.agricultural_holding.number : 
                '';
        },
        bank_accounts: function() {
            return (typeof this.model.partner.bank_accounts !== 'undefined' && this.model.partner.bank_accounts !== '') ?
                this.model.partner.bank_accounts :
                [];
        },
        is_admin: function() {
            return this.model.isUser === true && this.check_access_person_admin_tab;
        }
    },
    data: function () {
        return {};
    },
    mounted: function() {
        var _this = this;
        $(this.$el).on('destroyed', function(){
            _this.$destroy();
        });
    },
    methods: {
        addPartnerList: function() {
            sima.model.form("PartnerToPartnerList", [],
                {
                    init_data: {
                        'PartnerToPartnerList': {
                            'partner_id': {
                                'init': this.model.id,
                                'disabled': true
                            },
                            'partner_list_id': {
                                'model_filter': {
                                    'filter_scopes': ['withUserAccessToList', 'withoutSystems']
                                }
                            }
                        }
                    }
                }
            );
        },
        changePassword: function() {
            sima.model.form("User", this.model.id,{formName: "chPass"});
        },
        resetPassword: function() {
            sima.resetPassword(this.model.id);
        },
        logInAsUser: function() {
            sima.ajax.get('user/login', {
                get_params: {
                    as_user: this.model.user.id
                },
                async: true
            });
        },
        regeneratePersonalWikiPass: function() {
            sima.ajax.get('admin/admin/regeneratePersonalWikiPass', {
                get_params: {
                    user_id: this.model.user.id
                },
                async: true
            });
        },
        regenerateDocWikiPass: function() {
            sima.ajax.get('admin/admin/regenerateDocWikiPass', {
                get_params: {
                    user_id: this.model.user.id
                },
                async: true
            });
        },
        addUserToEmailAccount: function() {
            sima.model.form("UserToEmailAccount", [],
                {
                    init_data: {
                        'UserToEmailAccount': {
                            'user_id': {
                                'init': this.model.user.id,
                                'disabled': true
                            }
                        }
                    }
                }
            );
        },
        setUserEmailToReadNotif: function(user_to_email_account_id, is_read) {
            sima.misc.setUserEmailToReadNotif(user_to_email_account_id, is_read);
        },
        addAgriculturalHolding: function() {
            sima.model.form("AgriculturalHolding", [],
                {
                    init_data: {
                        'AgriculturalHolding': {
                            'id': this.model.id
                        }
                    }
                }
            );
        }
    }
});