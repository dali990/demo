<script type="text/x-template" id="application-person-full_info">
    <div class="application-person-full-info sima-layout-panel" v-bind:class="{'_splitter _vertical': is_admin}" v-observe-visibility="componentInstanceVisibilityChanged">
        <div class="sima-layout-panel" data-sima-layout-init='<?= CJSON::encode(["proportion" => 0.7]) ?>'>
            <div class="sima-layout-panel _splitter _horizontal" data-sima-layout-init='<?= CJSON::encode(['min_width' => 600, 'max_width' => 1000,]) ?>' >
                <div class="sima-layout-panel" data-sima-layout-init='<?= CJSON::encode(["proportion" => 0.6]) ?>'>
                    <div class="body">
                        <!-- statistika zaposljenja -->
                        <template v-if="check_access_hr_work_contract_read">
                            <template v-if="model.isEmployee">
                                <h3><?=Yii::t('Person','StatisticsOfEmployment')?>:</h3>
                                <div class="item">
                                    <div class="title"><b><?=Yii::t('Person','ActiveAnnualLeaveDecision')?>:</b></div>
                                    <div class="desc">
                                        <span v-for="active_annual_leave_decision in active_annual_leave_decisions">
                                            <template v-if="active_annual_leave_decision.remaining_days > 0"> 
                                                <?=Yii::t('LegalModule.Legal', 'ByDecision')?>
                                                <strong>{{active_annual_leave_decision.DisplayName}}</strong>
                                                <?=Yii::t('LegalModule.Legal', 'AnnualLeavesDaysLeft')?> {{active_annual_leave_decision.remaining_days}} </br>
                                            </template>
                                        </span>
                                        <template v-if="active_annual_leave_decisions.length === 0"><?=Yii::t('Person','NoStatisticsOnEmployment')?></template>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="title"><b><?=Yii::t('Person','LastAbsence')?>:</b></div>
                                    <div class="desc">
                                        <ul v-if="absances.length > 0">
                                            <li v-for="absance in absances">
                                                <?=Yii::t('BaseModule.Common','from')?> {{absance.begin}} 
                                                <?=Yii::t('BaseModule.Common','to')?> {{absance.end}}
                                            </li>
                                        </ul>
                                        <ul v-else>
                                            <li><?=Yii::t('Person','DoNotHaveAbsence')?></li>
                                        </ul>
                                    </div>
                                </div>
                            </template>
                        </template><!-- end statistika zaposljenja -->

                        <!-- ostale informacije -->
                        <div class="other-info">
                            <h3><?=Yii::t('Person','OtherInfo')?>:</h3>
                            <!-- zanimanje -->
                            <div class="item" v-if="check_access_hr_occupation && occupation_name !== ''">
                                <div class="title">
                                    <b><?=Yii::t('HRModule.Occupation','Occupation')?>:</b>
                                </div>
                                <div class="item-value">
                                    {{occupation_name}}
                                </div>
                            </div><!-- end zanimanje -->
                            <!-- nivo kvalifikacije -->
                            <div class="item" v-if="qualification_level !== ''">
                                <div class="title">
                                    <b><?=Yii::t('HRModule.QualificationLevel', 'QualificationLevel')?>:</b>
                                </div>
                                <div class="desc">
                                    {{qualification_level}}
                                </div>
                            </div><!-- end nivo kvalifikacije -->
                            <!-- strucna sprema | titula obrazovanja | jmbg -->
                            <template v-if="check_access_hr_work_contract_read">
                                <template v-if="model.employee">
                                    <div class="item" v-if="model.professional_degree_id !== null">
                                        <div class="title">
                                            <b><?=Yii::t('Person', 'ProfessionalQualifications')?>:</b>
                                        </div>
                                        <div class="desc">
                                            <sima-model-display-html v-bind:model="model.professional_degree"></sima-model-display-html>
                                        </div>
                                    </div>
                                    <div class="item" v-if="model.education_title_id !== null">
                                        <div class="title">
                                            <b><?=Yii::t('Person', 'EducationTitle')?>:</b>
                                        </div>
                                        <div class="desc">
                                            <sima-model-display-html v-bind:model="model.education_title"></sima-model-display-html>
                                        </div>
                                    </div>
                                </template>
                                <div class="item" v-if="model.JMBG !== ''">
                                    <div class="title">
                                        <b><?=Yii::t('Person','JMBG')?>:</b>
                                    </div>
                                    <div class="desc" v-html="model.JMBG"></div>
                                </div>
                            </template><!-- end strucna sprema | titula obrazovanja | jmbg -->
                            <!-- slava -->
                            <div class="item" v-if="haveSlava">
                                <div class="title">
                                    <b><?=Yii::t('Person', 'Slava')?>:</b>
                                </div>
                                <div class="desc">
                                    <sima-model-display-html v-bind:model="this.model.employee.slava"></sima-model-display-html>
                                    <sima-model-edit
                                        v-bind:model="model.employee"
                                        v-bind:class="{_disabled:!dataTrue}"
                                        v-bind:params="{'formName': 'only_slava'}"
                                    ></sima-model-edit>
                                </div>
                            </div><!-- slava slava -->
                            <!-- racuni -->
                            <div class="item" v-if="bank_accounts.length > 0">
                                <div class="title">
                                    <b><?=Yii::t('Person', 'BankAccounts')?>:</b>
                                </div>
                                <div class="desc">
                                    <ul>
                                        <li v-for="bank_account in bank_accounts" v-html="bank_account.DisplayHTML"></li>
                                    </ul>
                                </div>
                            </div><!-- end racuni -->
                            <!-- liste -->
                            <div class="item">
                                <div class="title">
                                    <b><?=Yii::t('Partner', 'PartnerLists')?>:</b> 
                                    <sima-button
                                        v-on:click="addPartnerList"
                                        v-bind:class="{_disabled:!dataTrue}"
                                    >
                                        <span class="fas fa-plus"></span>
                                    </sima-button>
                                </div>
                                <div class="desc">
                                    <ul>
                                        <li v-for="partner_to_partner_list in partner_to_partner_lists">
                                            {{partner_to_partner_list.partner_list.DisplayName}} 
                                            <sima-model-delete
                                                v-if="!partner_to_partner_list.partner_list.is_system && partner_to_partner_list.partner_list.has_user_access_to_list"
                                                v-bind:model="partner_to_partner_list"
                                                v-bind:class="{_disabled:!dataTrue}"
                                            >
                                            </sima-model-delete>
                                        </li>
                                    </ul>
                                </div>
                            </div><!-- end liste -->
                            <!-- teme -->
                            <div class="item">
                                <div class="title">
                                    <b><?=Yii::t('Person','InTeamOnThemes')?>:</b> 
                                </div>
                                <div class="desc">
                                    <ul>
                                        <li v-for="theme in themes" v-html="theme.DisplayHTML"></li>
                                    </ul>
                                </div>
                            </div><!-- end teme -->
                            <!-- poljoprivreno gazdinstvo -->
                            <div class="item" v-if="check_access_model_agricultural_holding_see">
                                <div class="title">
                                    <b><?=Yii::t('LegalModule.AgriculturalHolding','AgriculturalHoldingOwner')?>:</b> 
                                </div>
                                <div class="desc">
                                    <span v-if="agricultural_holding_number !== ''"><?=Yii::t('BaseModule.Common', 'Yes')?></span>
                                    <span v-else>
                                        <?=Yii::t('BaseModule.Common', 'No')?> 
                                        <sima-button
                                            v-on:click="addAgriculturalHolding"
                                            v-if="check_access_model_agricultural_holding_form_open"
                                        >
                                            <span class="fas fa-plus"></span>
                                        </sima-button>
                                    </span>
                                </div>
                            </div><!-- end poljoprivredno gazdinstvo -->
                            <!-- broj poljoprivrednog gazdinstva -->
                            <div class="item" v-if="agricultural_holding_number !== ''">
                                <div class="title">
                                    <b><?=Yii::t('LegalModule.AgriculturalHolding','AgriculturalHoldingNumber')?>:</b> 
                                </div>
                                <div class="desc">
                                    {{agricultural_holding_number}}
                                </div>
                            </div><!-- end broj poljoprivrednog gazdinstva -->
                        </div><!-- end ostale informacije -->
                    </div><!-- end body -->
                </div>
            </div>
        </div>
        <div 
            v-bind:class="{'sima-layout-panel': is_admin}"
            v-bind:style="is_admin ? '' : 'display:none;'"
            data-sima-layout-init='<?= CJSON::encode(["proportion" => 0.3, 'min_width' => 250]) ?>'
        >
            <template v-if="is_admin">
                <div class="right_panel">
                    <h3><?=Yii::t('Person','AdminsPanel')?>:</h3>
                    <h3 style='width: auto;'>
                        {{model.DisplayName}}
                        <sima-model-edit
                            v-bind:model="model.user"
                            v-bind:class="{_disabled:!dataTrue}"
                            v-bind:params="{'formName': 'admin'}"
                        >
                        </sima-model-edit>
                        <sima-model-delete
                            v-bind:model="model.user"
                            v-bind:class="{_disabled:!dataTrue}"
                        >
                        </sima-model-delete>
                    </h3>
                    <div class="item">
                        <div class="title">
                            <b><?=Yii::t('AdminModule.User', 'Username')?>:</b> 
                        </div>
                        <div class="desc">
                            {{model.user.username}}
                        </div>
                    </div>
                    <div class="item" v-if="model.user.id === curr_user_id || check_access_reset_user_password" >
                        <div class="title">
                            <b><?=Yii::t('AdminModule.User', 'Password')?>:</b> 
                        </div>
                        <div class="desc">
                            <span 
                                class='link' 
                                v-if="model.user.id === curr_user_id || check_access_change_all_passwords" 
                                v-on:click='changePassword'
                            >
                                <?=Yii::t('AdminModule.User', 'ChangePassword')?>
                            </span>
                            <template v-if="check_access_reset_user_password">
                                <template v-if="model.is_pass_eq_md5_username">
                                    <?=Yii::t('AdminModule.User', 'PasswordNotChanged')?>
                                </template>
                                <template v-else>
                                    | <span class='link' v-on:click='resetPassword'><?=Yii::t('Person', 'ResetPassword')?></span>
                                </template>
                            </template>
                        </div>
                    </div>
                    <div class="item">
                        <div class="title">
                            <b><?=Yii::t('AdminModule.User', 'Activity')?>:</b> 
                        </div>
                        <div class="desc">
                            <span v-if="model.user.active"><?=Yii::t('AdminModule.User', 'Active')?></span>
                            <span v-else><?=Yii::t('AdminModule.User', 'NotActive')?></span>
                        </div>
                    </div>
                    <div class="item" v-if="check_access_log_in_as_user">
                        <div class="title">
                            <span class='link' v-bind:class="{_disabled:!dataTrue}" v-on:click='logInAsUser'><?=Yii::t('AdminModule.User', 'LogInAsUser')?></span>
                        </div>
                    </div>
                    <div class="item">
                        <div class="title">
                        </div>
                        <div class="desc">
                            <sima-button
                                v-if="personal_wiki_is_installed === true && personal_wiki_is_administrator === true"
                                v-bind:class="{_disabled:!dataTrue}"
                                v-on:click="regeneratePersonalWikiPass"
                            >
                                <span v-if="model.personal_wiki_is_wiki_individual_user"><?=Yii::t('AdminModule.User', 'RegeneratePersonalWikiPass')?></span>
                                <span v-else><?=Yii::t('AdminModule.User', 'AddPersonalWikiPass')?></span>
                            </sima-button>
                            <sima-button
                                v-if="doc_wiki_is_installed === true && doc_wiki_is_administrator === true"
                                v-bind:class="{_disabled:!dataTrue}"
                                v-on:click="regenerateDocWikiPass"
                            >
                                <span v-if="model.doc_wiki_is_wiki_individual_user"><?=Yii::t('AdminModule.User', 'RegenerateDocWikiPass')?></span>
                                <span v-else><?=Yii::t('AdminModule.User', 'AddDocWikiPass')?></span>
                            </sima-button>
                        </div>
                    </div>
                    <div class="item">
                        <div class="title">
                            <b><?=Yii::t('AdminModule.User', 'FirstReportDay')?>:</b> 
                        </div>
                        <div class="desc">
                            <span>{{model.user.first_report_day.DisplayName}}</span>
                            <sima-model-edit
                                v-bind:model="model.user"
                                v-bind:class="{_disabled:!dataTrue}"
                                v-bind:params="{'formName': 'firstDayReport'}"
                            >
                            </sima-model-edit>
                        </div>
                    </div>
                    <div class="item">
                        <div class="title">
                            <b><?=Yii::t('AdminModule.User', 'LastReportDay')?>:</b> 
                        </div>
                        <div class="desc">
                            <span>{{model.user.last_report_day.DisplayName}}</span>
                            <sima-model-edit
                                v-bind:model="model.user"
                                v-bind:class="{_disabled:!dataTrue}"
                                v-bind:params="{'formName': 'lastDayReport'}"
                            >
                            </sima-model-edit>
                        </div>
                    </div>
                    <div class="item">
                        <div class="title">
                            <b><?=Yii::t('AdminModule.User', 'LastBossConfirmDay')?>:</b> 
                        </div>
                        <div class="desc">
                            <span>{{model.user.last_boss_confirm_day.DisplayName}}</span>
                            <sima-model-edit
                                v-bind:model="model.user"
                                v-bind:class="{_disabled:!dataTrue}"
                                v-bind:params="{'formName': 'lastBossConfirmDay'}"
                            >
                            </sima-model-edit>
                        </div>
                    </div>
                    <h3><?=Yii::t('MessagesModule.Email','EmailAccounts')?>:</h3>
                    <div class="item">
                        <div class="title">
                            <b><?=Yii::t('MessagesModule.Email','DefaultEmailAccount')?>:</b> 
                        </div>
                        <div class="desc">
                            <span>{{email_account}}</span>
                        </div>
                    </div>
                    <h4><?=Yii::t('MessagesModule.Email','EmailAccountsThatICanView')?>:</h4>
                    <sima-button
                        v-on:click="addUserToEmailAccount"
                        v-bind:class="{_disabled:!dataTrue}"
                        v-if="model.user.id === curr_user_id"
                    >
                        <span class="fas fa-plus"></span>
                    </sima-button>
                    <table v-if="model.user.user_to_email_accounts.length > 0">
                        <tr>
                            <td></td>
                            <td><?=Yii::t('MessagesModule.Email', 'EmailAccount')?></td>
                            <td><?=Yii::t('MessagesModule.Email', 'Owner')?></td>
                            <td><?=Yii::t('MessagesModule.Email', 'GetNotification')?></td>
                        </tr>
                        <tr v-for="user_to_email_account in model.user.user_to_email_accounts">
                            <td>
                                <sima-model-options
                                    v-bind:model="user_to_email_account"
                                >
                                </sima-model-options>
                            </td>
                            <td>{{user_to_email_account.email_account.DisplayName}}</td>
                            <td><template v-if="typeof user_to_email_account.email_account.owner !== 'undefined'">{{user_to_email_account.email_account.owner.DisplayName}}</template></td>
                            <td>
                                <template v-if="user_to_email_account.read_notif">
                                    <?=Yii::t('BaseModule.Common', 'Yes')?>
                                    <span 
                                        v-if="user_to_email_account.user_id === curr_user_id"
                                        class="link" 
                                        v-on:click="setUserEmailToReadNotif(user_to_email_account.id, false)"
                                    >
                                        - <?=Yii::t('MessagesModule.Email', 'UserToEmailAccountRemove')?>
                                    </span>
                                </template>
                                <template v-else>
                                    <?=Yii::t('BaseModule.Common', 'No')?>
                                    <span 
                                        v-if="user_to_email_account.user_id === curr_user_id"
                                        class="link" 
                                        v-on:click="setUserEmailToReadNotif(user_to_email_account.id, true)"
                                    >
                                        - <?=Yii::t('MessagesModule.Email', 'UserToEmailAccountAdd')?>
                                    </span>
                                </template>
                            </td>
                        </tr>
                    </table>
                </div>
            </template>
        </div>
    </div>
</script>