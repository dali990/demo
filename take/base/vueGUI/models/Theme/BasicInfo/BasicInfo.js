/* global Vue, sima */

Vue.component('base-theme-basic_info', {
    template: '#base_theme_basic_info',
    props: {
        model: {
            validator: sima.vue.ProxyValidator
        }
    },
    computed: {
    },
    data: function () {
        return {
        };
    },
    mounted: function() {
        var _this = this;
        $(this.$el).on('destroyed', function(){
            _this.$destroy();
        });
    }
});
