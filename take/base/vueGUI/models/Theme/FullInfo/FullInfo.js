/* global Vue, sima */

Vue.component('base-theme-full_info', {
    template: '#base_theme_full_info',
    props: {
        model: {
            validator: sima.vue.ProxyValidator
        }
    },
    computed: {
        dataTrue: function(){
            var is_data_true = false;

            this.model.job_type;
            if(
                this.model.shotStatus('job_type') === 'OK'
            )
            {
                if(!sima.isEmpty(this.model.job_type))
                {
                    this.model.job_type.code;
                    is_data_true = this.model.job_type.shotStatus('code') === 'OK';
                }
                else
                {
                    is_data_true = true;
                }
            }
            return is_data_true;
        },
        only_important_children: function(){
            return this.model.scoped('children', ['byDisplayName','onlyImportantThemes']);
        },
        check_access_model_cost_location_add: function() {
            return sima.params.getParam('check_access_model_cost_location_add');
        }
    },
    data: function () {
        return {};
    },
    mounted: function() {
        var _this = this;
        $(this.$el).on('destroyed', function(){
            _this.$destroy();
        });
    },
    methods: {
        addSubTheme: function() {
            sima.model.form("Theme", [],
                {
                    init_data: {
                        'Theme': {
                            'parent_id': {
                                'init': this.model.id,
                                'disabled': true
                            }
                        }
                    }
                }
            );
        },
        addCostLocation: function() {
            var prefix = this.model.job_type !== '' ? this.model.job_type.code : 'SE';
            var year_id = sima.params.getParam('curr_year_id');
            sima.jobs.addCostLocation(this.model.id, prefix, year_id);
        }
    }
});
