<script type="text/x-template" id="base_theme_full_info">
    <div class="base-theme-full-info">
        <div style="float: left">
            <h4><?=Yii::t('BaseModule.Theme', 'InTeam')?></h4>
            <ul>
                <li v-for="team_member in model.persons_to_teams">
                    <sima-model-display-html 
                        v-bind:model="team_member.person" 
                        v-bind:display_name="team_member.DisplayName"
                    ></sima-model-display-html>
                    <span v-if="team_member.role_in_team">{{team_member.role_in_team}}</span>
                </li>
            </ul>
        </div>
        <div style="float: left; margin-left: 30px;">
            <h4><?=Yii::t('BaseModule.CompanyToTheme', 'CompaniesToTheme')?></h4>
            <ul>
                <li v-for="company in model.companies">
                    <sima-model-display-html v-bind:model="company"></sima-model-display-html>
                </li>
            </ul>
        </div>
        <div style="clear: left">
            <h4>
                <sima-button
                    v-on:click="addSubTheme" 
                    v-if="model.has_change_access"
                >
                    <span class="fas fa-plus"></span>
                </sima-button>
                <?=Yii::t('BaseModule.Theme', 'SubThemes')?>:
            </h4>
            <ul>
                <li v-for="child in only_important_children">
                    <sima-model-edit v-if="child.has_change_access" v-bind:model="child"></sima-model-edit>
                    <sima-model-delete v-if="child.has_change_access" v-bind:model="child"></sima-model-delete>
                    <sima-model-display-html v-bind:model="child"></sima-model-display-html>
                </li>
            </ul>    
        </div>
        <div>
            <h4><?=Yii::t('BaseModule.Theme', 'Statistics')?>:</h4>
            <ul>
                <li><?=Yii::t('BaseModule.Theme', 'NumberOfFilesOnTheme')?>: {{model.file_cnt}}</li>
                <li><?=Yii::t('BaseModule.Theme', 'NumberOfFilesOnThemeAndSubthemes')?>: {{model.file_cnt_r}}</li>
            </ul>
        </div>
        <div>
            <h4><?=Yii::t('BaseModule.Theme', 'CostLocation')?>:</h4>
            <template v-if="model.cost_location !== ''">
                <sima-model-edit v-if="check_access_model_cost_location_add" v-bind:model="model.cost_location"></sima-model-edit>
                <sima-model-display-html v-bind:model="model.cost_location"></sima-model-display-html>
            </template>
            <sima-button
                v-on:click="addCostLocation" 
                v-bind:class="{_disabled: !dataTrue}"
                v-else-if="check_access_model_cost_location_add"
            >
                <span class="fas fa-plus"></span>
            </sima-button>
        </div>
    </div>
</script>