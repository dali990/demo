<?php

class SIMAReportController extends SIMAController
{
    public function actionDownloadPDF()
    {
        $data = $this->setEventHeader();
        $report_class = $this->filter_input($data, 'report_class');
        $report_params = $this->filter_input($data, 'report_params', false);
        
        $report_obj = new $report_class($report_params);
        $temporary_file = $report_obj->getPDF();
        
        $this->sendEndEvent([
            'filename' => $temporary_file->getFileName(),
            'downloadname' => $report_obj->getDownloadName()
        ]);
    }
    
    public function actionDownloadXML()
    {
        $data = $this->setEventHeader();
        $report_class = $this->filter_input($data, 'report_class');
        $report_params = $this->filter_input($data, 'report_params', false);
        
        $report_obj = new $report_class($report_params);
        $temporary_file = $report_obj->getXML();
        
        $this->sendEndEvent([
            'filename' => $temporary_file->getFileName(),
            'downloadname' => $report_obj->getDownloadName()
        ]);
    }
}
