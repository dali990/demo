<?php

class ModelDefaultLayoutController extends SIMAController 
{
    public function actionGetFullInfoHtml($model_name, $model_id)
    {
        $model = $model_name::model()->findByPk($model_id);
        
        $full_info_html = '';
        if (!is_null($model))
        {
            $full_info_html = $this->renderFullInfo($model);
        }
        
        $this->respondOK([
            'html' => $full_info_html
        ]);
    }
    
    private function renderFullInfo(SIMAActiveRecord $model)
    {
        $max_priority_fullinfo_data = $this->getMaxPriorityFullInfoData($model);

        return $this->getFullInfo($model, $max_priority_fullinfo_data);
    }
    
    private function getMaxPriorityFullInfoData(SIMAActiveRecord $model)
    {
        $event = new SIMAEvent();
        $model->onGetFullInfo($event);

        $max_priority = 0;
        $max_priority_fullinfo_data = [
            'type' => 'model',
            'value' => $model
        ];

        foreach ($event->getResults() as $result)
        {
            foreach ($result as $value)
            {
                if(!is_array($value))
                {
                    throw new SIMAException('must be array');
                }

                if(!isset($value['priority']))
                {
                    throw new SIMAException('priority not set');
                }

                if(!isset($value['model']) && !isset($value['view']))
                {
                    throw new SIMAException('model or view must be set');
                }

                $curr_priority = intval($value['priority']);
                if ($curr_priority === $max_priority)
                {
                    throw new SIMAException('Na dva mesta ste zadali full info sa istim prioritetom.');
                }
                if ($curr_priority > $max_priority)
                {
                    $max_priority = $curr_priority;

                    if(isset($value['model']))
                    {
                        $max_priority_fullinfo_data = [
                            'type' => 'model',
                            'value' => $value['model']
                        ];
                    }
                    else if(isset($value['view']))
                    {
                        $max_priority_fullinfo_data = [
                            'type' => 'view',
                            'value' => $value['view']
                        ];
                    }
                    else
                    {
                        throw new SIMAException('should not be here');
                    }
                }
            }
        }

        return $max_priority_fullinfo_data;
    }
    
    private function getFullInfo(SIMAActiveRecord $model, array $max_priority_fullinfo_data)
    {
        if($max_priority_fullinfo_data['type'] === 'model')
        {
            $uniq = SIMAHtml::uniqid(); 
            $curr_model = $max_priority_fullinfo_data['value'];
            $model_tag = SIMAMisc::getVueModelTag($curr_model);
            $model_path = strtolower($curr_model->moduleName()."-".get_class($curr_model));
            return '
                <'.$model_path.'-full_info
                    id = '.$uniq.'
                    v-bind:model="model"
                >
                </'.$model_path.'-full_info>
                <script>
                    new Vue({
                        el: "#'.$uniq.'",
                        data: {
                            model: sima.vue.shot.getters.model("'.$model_tag.'"),
                        }
                    });
                </script>
            ';
        }
        else if($max_priority_fullinfo_data['type'] === 'view')
        {
            return Yii::app()->controller->renderPartial($max_priority_fullinfo_data['value'], [
                'class' => 'sima-full-info sima-layout-panel',
                'model' => $model
            ], true, false);
        }
        else
        {
            throw new SIMAException('wrong full info type');
        }
    }
}
