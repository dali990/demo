<?php

class BaseController extends SIMAController 
{
    public function actionRenderTinyMce()
    {        
        $init_html = isset($_POST['init_html'])?$_POST['init_html']:'';
        $init_options = isset($_POST['options'])? CJSON::decode($_POST['options'], true):[];
        
        $options = [
            'name' => "tiny_mce_name",
            'useSwitch' => false,
            'value' => $init_html           
        ];

        if(Yii::app()->configManager->get('base.use_new_tinymce') === true)
        {
            $options['width'] = 'auto';
            $options['plugins'] = [
                'pagebreak','table','code','codesample','save','insertdatetime','preview','media','searchreplace','print','contextmenu','paste','directionality',
                'fullscreen','noneditable','visualchars','nonbreaking','template', 'image'
            ];
            $options['options'] = [
                'branding' => false
            ];
        }
        else
        {
            $options['plugins'] = [
                'safari','pagebreak','style','layer','table','save','advhr','advimage','advlink','emotions','spellchecker',
                'inlinepopups','insertdatetime','preview','media','searchreplace','print','contextmenu','paste','directionality',
                'fullscreen','noneditable','visualchars','nonbreaking','xhtmlxtras','template'
            ];
            $options['options'] = [
                'theme' => 'advanced',
                'theme_advanced_toolbar_location' => 'top',
                'theme_advanced_toolbar_align' => 'left',
                'theme_advanced_statusbar_location' => 'none',
                'theme_advanced_path_location' => 'none',
                'theme_advanced_resize_horizontal' => true,
                'theme_advanced_resizing' => true,
                'theme_advanced_buttons1' => "save,print,|,cut,copy,paste,|,search,replace,|,undo,redo,|,help",
                'theme_advanced_buttons2' => "styleselect,formatselect,fontselect,fontsizeselect,|,forecolor,backcolor,|,bold,italic,underline,strikethrough",
                'theme_advanced_buttons3' => "justifyleft,justifycenter,justifyright,justifyfull,|,bullist,numlist,|,hr,advhr,pagebreak,|,charmap,emotions,media,image,|,link,unlink",
                'theme_advanced_buttons4' => "tablecontrols"
            ];
        }

        $html = $this->renderPartial('render_tiny_mce', [            
            'options' => array_merge($options, $init_options)
        ], true, false);        
        
        $this->respondOK([
            'html'=>$html
        ]);
    }
    
    public function actionSaveTooltipForUser($code)
    {
        if (!isset($code))
        {
            throw new Exception('actionSaveTooltipForUser - nije zadat code.');        }
        
        $tooltip = Tooltip::getTooltip($code);
        if ($tooltip !== null)
        {
            $tooltip->removeTooltipForUser(Yii::app()->user->id);
        }
        else
        {
            throw new Exception("Ne postoji tooltip za code $code.");
        }
        
        $this->respondOK();
    }
    
    public function actionFillFromNbsInForm($fill_by_pib, $company_id = null)
    {
        if(Yii::app()->NBSConnection->isConnected() !== true)
        {
            throw new SIMAWarnException(Yii::t('Company', 'NBSConnectionIsNotSet'));
        }
        
        $pib = $this->filter_post_input('pib', false);
        $mb = $this->filter_post_input('mb', false);

        $nbs_company_data = null;
        if($fill_by_pib)
        {
            if (empty($pib))
            {
                throw new SIMAWarnException(Yii::t('Company', 'YouDidNotFillPIB'));
            }
            
            if (!is_numeric($pib) || strlen($pib) !== 9)
            {
                throw new SIMAWarnException(Yii::t('Company', 'PIBNotValid'));
            }
            
            $nbs_company_data = Yii::app()->NBSConnection->GetCompanyInformationByPIB($pib);
        }
        else
        {
            if (empty($mb))
            {
                throw new SIMAWarnException(Yii::t('Company', 'YouDidNotFillMB'));
            }

            if (!is_numeric($mb) || strlen($mb) !== 8)
            {
                throw new SIMAWarnException(Yii::t('Company', 'MBNotValid'));
            }
            
            $nbs_company_data = Yii::app()->NBSConnection->GetCompanyInformationByMB($mb);
        }
        
        if(empty($nbs_company_data))
        {
            throw new SIMAWarnException(Yii::t('Company', 'DidNotGetAnyDataForFillFromNBS'));
        }

        $nbs_pib = Company::getFullPIB($nbs_company_data['TaxIdentificationNumber']);
        $nbs_mb = Company::getFullMB($nbs_company_data['NationalIdentificationNumber']);
        
        if($fill_by_pib)
        {
            if(!empty($mb) && $mb !== $nbs_mb)
            {
                $this->raiseNote(Yii::t('Company', 'MBHasChangedToMatchNBS'));
            }
        }
        else
        {
            if(!empty($pib) && $pib !== $nbs_pib)
            {
                $this->raiseNote(Yii::t('Company', 'PIBHasChangedToMatchNBS'));
            }
        }

        $this->respondOK([
            'pib' => $nbs_pib,
            'mb' => $nbs_mb,
            'name' => $nbs_company_data['ShortName'],
            'full_name' => $nbs_company_data['Name'],
            'accounts' => $this->getCompanyNBSAccountsAsFormList($fill_by_pib, $pib, $mb, $company_id)
        ]);
    }
    
    private function getCompanyNBSAccountsAsFormList($fill_by_pib, $pib, $mb, $company_id = null)
    {
        if(Yii::app()->NBSConnection->isConnected() !== true)
        {
            throw new SIMAWarnException(Yii::t('Company', 'NBSConnectionIsNotSet'));
        }
        
        $nbs_accounts_data = null;
        if($fill_by_pib)
        {
            $nbs_accounts_data = Yii::app()->NBSConnection->GetCompanyAccountByPIB($pib);
        }
        else
        {
            $nbs_accounts_data = Yii::app()->NBSConnection->GetCompanyAccountByMB($mb);
        }
        
        $company = null;
        if (!empty($company_id))
        {
            $company = Company::model()->findByPk($company_id);
        }

        $accounts = [];
        if (!empty($nbs_accounts_data))
        {
            $currency_id = Yii::app()->configManager->get('base.country_currency');

            foreach($nbs_accounts_data as $nbs_account_data)
            {
                $number = $nbs_account_data['BankCode']
                        .'-'.str_pad($nbs_account_data['AccountNumber'], 13, '0', STR_PAD_LEFT)
                        .'-'.str_pad($nbs_account_data['ControlNumber'], 2, '0', STR_PAD_LEFT);

                try
                {
                    $bank_account = BankAccount::model()->findByAttributes([
                        'number' => $number
                    ]);

                    if(empty($bank_account))
                    {
                        $bank = Bank::GetByBankCode($nbs_account_data['BankCode']);

                        $bank_account = new BankAccount();
                        $bank_account->bank_id = $bank->id;
                        $bank_account->number = $number;
                        $bank_account->partner_id = $company_id;
                        $bank_account->currency_id = $currency_id;

                        array_push($accounts, [
                            'id' => [
                                'bank_id' => $bank->id,
                                'number' => $number,
                                'partner_id' => $company_id,
                                'currency_id' => $currency_id
                            ],
                            'display_name' => $bank_account->DisplayName,
                            'class' => '_visible'
                        ]);
                    }
                    else
                    {
                        array_push($accounts, [
                            'id' => $bank_account->id,
                            'display_name' => $bank_account->DisplayName,
                            'class' => '_visible'
                        ]);
                    }
                }
                catch(SIMAExceptionBankNotFoundByCode $e)
                {
                    Yii::app()->raiseNote(Yii::t(
                            'Company', 
                            !empty($company_id) ? 'CouldNotAddCompanyAccountBankNotFoundByCode' : 'CouldNotAddAccountBankNotFoundByCode',
                            [
                                '{company_name}' => !empty($company) ? $company->DisplayName : null,
                                '{number}' => $number,
                                '{reason}' => $e->getMessage()
                            ]
                    ));
                }
            }
        }
        
        return $accounts;
    }
    
    public function actionCalcSystemPartnerList()
    {
        Yii::app()->setUpdateModelViews(false);
        
        $data = $this->setEventHeader();
        
        $system_partner_list_code = $this->filter_input($data, 'system_partner_list_code');
        
        $this->sendUpdateEvent(40);
        
        PartnerList::calcSystemList($system_partner_list_code);
        
        $this->sendUpdateEvent(100);
        
        $this->sendEndEvent();
    }
    
    public function actionGetYearIdFromDate($date)
    {
        $date_model = new DateTime($date);
        $year = $date_model->format("Y");
        $year_model = Year::get($year);
        
        $this->respondOK([
            'year_id' => $year_model->id
        ]);
    }
    
    public function actionAddUsersToPartnerList($partner_list_id)
    {
        $partner_list = PartnerList::model()->findByPkWithCheck($partner_list_id);
        $users = $this->filter_post_input('users', false);
        
        if (!empty($users))
        {
            foreach ($users as $user)
            {
                $user_to_partner_list = UserToPartnerList::model()->findByAttributes([
                    'partner_list_id' => $partner_list_id,
                    'user_id' => $user['id']
                ]);
                
                if (empty($user_to_partner_list))
                {
                    $user_to_partner_list = new UserToPartnerList();
                    $user_to_partner_list->partner_list_id = $partner_list_id;
                    $user_to_partner_list->user_id = $user['id'];
                    $user_to_partner_list->save();
                }
            }
        }
        
        $this->respondOK([], $partner_list);
    }
    
    public function actionGenerateDateTimeField()
    {
        $mode = $this->filter_post_input('mode');
        $this->respondOK([
            'title' => Yii::t('BaseModule.Common', 'DateTimeChoose'),
            'html' => SIMAHtml::datetimeFieldByName('datetime_field', '', [
                'mode' => $mode,
                'readonly' => true,
                'htmlOptions' => [
                    'placeholder' => Yii::t('BaseModule.Common', 'DateTimeChoose')
                ]
            ])
        ]);
    }
    
    public function actionUpdateSessionParams($width_resolution, $height_resolution, $zoom)
    {
        $session = Yii::app()->user->getdb_session();
        $session->width_sima_resolution = $width_resolution;
        $session->height_sima_resolution = $height_resolution;
        $session->zoom = $zoom;
        $session->save();
        
        $this->respondOK([]);
    }
}
