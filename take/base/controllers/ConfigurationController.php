<?php

class ConfigurationController extends SIMAController
{
    public function actionIndex($path=null, $type=null, $is_user_personal=null)
    {
        if($is_user_personal === false 
                || $is_user_personal === 'false'
                || $is_user_personal === 0
                || $is_user_personal === '0')
        {
            $is_user_personal = false;
        }
        else
        {
            $is_user_personal = true;
        }
                
        $menuArray = Yii::app()->configManager->getAllGroups($path, $type, $is_user_personal);
        
        $html = $this->renderPartial('index', array(
            'menu_array' => $menuArray,
            'is_user_personal' => $is_user_personal
        ), true, true);
        
        $this->respondOK(array(
            'html'=>$html, 
            'title' => Yii::t('BaseModule.Configuration', 'Configurations')
        ));
    }
    
    public function actionParameters($path, $type=null, $is_user_personal=null)
    {
        $html = '';
                
        if(!empty($path))
        {
            if($is_user_personal === false 
                    || $is_user_personal === 'false'
                    || $is_user_personal === 0
                    || $is_user_personal === '0')
            {
                $is_user_personal = false;
            }
            else
            {
                $is_user_personal = true;
            }
            
            $title = Yii::app()->configManager->getPathDisplayName($path);
            $config_parameters = Yii::app()->configManager->getConfTabParameters($path, $type, $is_user_personal);

            $html = $this->renderPartial('config_parameters', array(
                'config_parameters'=>$config_parameters,
                'is_user_personal' => $is_user_personal,
                'title' => $title
            ), true, true);
        }
        
        $this->respondOK(array('html'=>$html));
    }
    
    public function actionGetModelChooseParams()
    {
        $path = $this->filter_post_input('path');
        $is_user_personal = $this->filter_post_input('is_user_personal');
        
        $result = Yii::app()->configManager->getModelChooseParams($path, $is_user_personal);
        
        $this->respondOK($result);
    }
    
    public function actionChangeConfigParamDialog()
    {
        $uniq_id = $this->filter_post_input('uniq_id');
        $path = $this->filter_post_input('path');
        $is_user_personal = $this->filter_post_input('is_user_personal');
        
        if($is_user_personal === false 
                || $is_user_personal === 'false'
                || $is_user_personal === 0
                || $is_user_personal === '0')
        {
            $is_user_personal = false;
        }
        else
        {
            $is_user_personal = true;
        }
        
        $result_array = Yii::app()->configManager->renderChangeParamDialog($uniq_id, $path, $is_user_personal);
        
        $this->respondOK($result_array);
    }
    
    public function actionSetConfigParam()
    {
        $path = $this->filter_post_input('path');
        $new_value = $this->filter_post_input('new_value', false);
        $is_user_personal = $this->filter_post_input('is_user_personal');
        
        if($is_user_personal === false 
                || $is_user_personal === 'false'
                || $is_user_personal === 0
                || $is_user_personal === '0')
        {
            $is_user_personal = false;
        }
        else
        {
            $is_user_personal = true;
        }
                
        Yii::app()->configManager->set($path, $new_value, $is_user_personal);
        
        Yii::app()->warnManager->sendWarns('ConfigParamsWarning', 'WARN_NO_EMPTY_CONFIG_PARAM');
        
        $this->respondOK();
    }
    
    public function actionSetConfigParamToDefault()
    {
        $path = $this->filter_post_input('path');
        $is_user_personal = $this->filter_post_input('is_user_personal');
        
        if($is_user_personal === false 
                || $is_user_personal === 'false'
                || $is_user_personal === 0
                || $is_user_personal === '0')
        {
            $is_user_personal = false;
        }
        else
        {
            $is_user_personal = true;
        }
                
        Yii::app()->configManager->setDefault($path, $is_user_personal);
        
        Yii::app()->warnManager->sendWarns('ConfigParamsWarning', 'WARN_NO_EMPTY_CONFIG_PARAM');
        
        $this->respondOK();
    }
    
    public function actionParamUserChangableChanged()
    {
        $path = $this->filter_post_input('path');
                
        Yii::app()->configManager->userChangableChanged($path);
        
        $this->respondOK();
    }
    
    public function actionValidate()
    {
        $error_messages = Yii::app()->configManager->validate();
        
        $message = Yii::t('BaseModule.Configuration', 'SuccessValidation');
        
        if(count($error_messages) > 0)
        {
            $message = implode('</br>', $error_messages);
        }
        
        $this->respondOK(array(
            'message'=>$message,
        ));
    }
    
    public function actionMustSetNulledParams()
    {
        $config_parameters = Yii::app()->configManager->mustSetNulledParams();
                
        $html = $this->renderPartial('config_parameters', array(
            'config_parameters'=>$config_parameters,
            'is_user_personal' => false
        ), true, true);
        
        $this->respondOK([
            'html' => $html,
            'title' => Yii::t('BaseModule.Configuration', 'MustSetNulledParams'),
        ]);
    }
    
    public function actionGetConfig($path, $user_id = null)
    {
        $config_params = Yii::app()->configManager->getConfigParam($path, false, !empty($user_id), $user_id);

        $this->respondOK([
            'model_tag' => "ConfigurationParameter_{$config_params['dbId']}",
            'value' => $config_params['value']
        ]);
    }
    
    public function actionSetConfig($path, $user_id = null)
    {
        $new_value = $this->filter_post_input('new_value', false);

        Yii::app()->configManager->set($path, $new_value, !empty($user_id), $user_id);
        
        $this->respondOK();
    }
}
