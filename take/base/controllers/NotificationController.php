<?php

class NotificationController extends SIMAController 
{
    public function actionIndex()
    {        
        $html = $this->renderPartial('index', [], true, false);

        $this->respondOk([
            'html' => $html,
            'title' => Yii::t('BaseModule.Notification', 'Notfications')
        ]);
    }

    /**
     *  Koristi se za postavljanje kliknutih notifikacija na unseen. Ukoliko se odnose na komentare, onda se prebrojava koliko je notifikacija
     *  vezano za isti comment thread. Broj notifikacija koje su vezane za isti comment thread se koristi da se smanji broj
     *  neprocitanih notifkacija (vezanih za comment thread).
     */
    public function actionNotifRead($id)
    {
        $notif = Notification::model()->findByPkWithCheck($id);
        $notif->seen = true;
        $notif->save();
        
        //event se salje zbog SDA
        if(Yii::app()->eventRelay->isInstalled())
        {
            WebSocketClient::sendBulk([
                [
                    'receiver_id' => Yii::app()->user->id,
                    'receiver_type' => 'USER',
                    'message_type' => 'NOTIF_READ',
                    'data' => [
                        'number_of_unread_notifs' => Yii::app()->user->model->unseen_notifications_count
                    ]
                ]
            ]);
        }

        $this->respondOK();
    }
    
    public function actionNotifUnRead($id)
    {
        $notif = Notification::model()->findByPkWithCheck($id);
        $notif->seen = false;
        $notif->save();

        //event se salje zbog SDA
        if(Yii::app()->eventRelay->isInstalled())
        {
            WebSocketClient::sendBulk([
                [
                    'receiver_id' => Yii::app()->user->id,
                    'receiver_type' => 'USER',
                    'message_type' => 'NOTIF_READ',
                    'data' => [
                        'number_of_unread_notifs' => Yii::app()->user->model->unseen_notifications_count
                    ]
                ]
            ]);
        }

        $this->respondOk();
    }
    
    public function actionMarkAllAsReadForCurrUser()
    {
        $curr_user_unseen_notifs = Notification::model()->findAll([
            'model_filter' => [
                'user' => [
                    'ids' => Yii::app()->user->id
                ],
                'filter_scopes' => 'onlyUnseen'
            ]
        ]);
        foreach ($curr_user_unseen_notifs as $curr_user_unseen_notif)
        {
            $curr_user_unseen_notif->seen = true;
            $curr_user_unseen_notif->save();
        }
        
        $this->respondOK();
    }
    
    public function actionMarkNotificationsAsReadOrUnread($is_seen)
    {
        $notification_ids = $this->filter_post_input('notification_ids');
        foreach ($notification_ids as $notification_id)
        {
            $notification = Notification::model()->findByPkWithCheck($notification_id);
            $notification->seen = $is_seen;
            $notification->save();
        }
   
        $this->respondOK();
    }
}
