<?php

class ThemeController extends SIMAController 
{
    //Sasa A. - ne koristi se vise. Jedino koriscenje je ostalo u starim notifikacijama, tako da je vremenom treba obrisati
    public function actionIndex($id)
    {
        try
        {
            $model = Theme::model()->hasAccess(Yii::app()->user->id)->findByPkWithCheck($id);
        }
        catch (SIMAExceptionModelNotFound $e)
        {
            $this->respondFail('Nemate pravo pristupa');
        }
        
        $title = $model->DisplayName;
        
        $selector = $this->filter_post_input('selector', false);
        if(!isset($selector))
        {
            $selector = [];
        }

        if (gettype($selector)==='string')
        {
           $selector = CJSON::decode($selector,true);
        }
        
        $default_selected_tab = [];
        $default_selected = array_shift($selector);
        if (!empty($default_selected) && isset($default_selected['simaTabs']))
        {
            $default_selected_tab = [$default_selected['simaTabs'], 'selector'=>$selector];
        }

        $html = $this->widget('SIMADefaultLayout', [
            'model'=>$model,
            'default_selected_tab'=>$default_selected_tab
        ], true);
        
        $this->respondOK([
            'html' => $html,
            'title' => $title
        ]);
    }
    
    public function actionForUser($id)
    {
        $title = Yii::t('BaseModule.Theme', 'ThemesOverview');
        $uniq = SIMAHtml::uniqid();
        $tree_id = 'tree_'.$uniq;
        $right_panel_id = 'right_panel_'.$uniq;
        
        $left_menu = $this->widget('SIMATree', [
            'id' => $tree_id,
            'class' => 'sima-theme-list',
            'list_on_init' => true,
            'build_in_js' => true,
//            'params' => $tree_sub_params,
            'list_tree_action'=>'base/theme/list',
//            'list_tree_params' => [
//                'files_from_parents' => false,
//                'short_path' => false,
//                'exclude_empty' => false,
//                'used_filters' => []
//            ],
            'add' => false,
            'edit' => false
        ],true);
        
        $right_panel = "<div id='$right_panel_id' class='sima-layout-panel'></div>";
       
        $html = Yii::app()->controller->renderContentLayout([
            'name' => $title,            
//            'options_class'=>'sima-email-client-options',
//            'options'=>$this->getEmailClientOptions($uniq, Yii::app()->params['sima_mailbox_msg_number'], $user_email_accounts)
        ], [
            [
                'proportion'=>0.3,
                'min_width'=>100,                
                'max_width'=>400,                
                'html'=>$left_menu,
//                'class'=>'sima-email-client-mailboxes'
            ],
            [
                'proportion'=>0.7,
                'min_width'=>600,
                'max_width'=>2000,
                'html'=>$right_panel,
//                'class'=>''
            ]
        ], [
            'id'=>$uniq,
            'processOutput' => false
//            'class'=>'sima-email-client'
        ]);
        
        $sima_init_script = "$('#$tree_id').on('selectLeaf',function(event,data){sima.misc.themeList(data.id,'#$right_panel_id');})";
        Yii::app()->clientScript->registerScript('theme-list',$sima_init_script, CClientScript::POS_READY);
        
        $this->respondOK([
            'title' => $title,
            'html' => $html
        ]);
    }
    
    public function actionList($parent_id='root', $params = [])
    {
        $can_see_all_themes = Yii::app()->user->checkAccess('modelThemeSeeAll');
        $response = [];
        if ($parent_id === 'root')
        {
            if ($can_see_all_themes)
            {
                $response[] = [
                    'id' => 'all_active',
                    'title' => 'Sve aktivne',
                    'class' => '_list'
                ];
                $response[] = [
                    'id' => 'all_inactive',
                    'title' => 'Sve neaktivne',
                    'class' => '_list'
                ];
            }
            
            $response[] = [
                'id' => 'user_coordinator_active',
                'title' => 'Koordinator aktivne',
                    'class' => '_list'
            ];
            $response[] = [
                'id' => 'user_coordinator_inactive',
                'title' => 'Koordinator neaktivne',
                    'class' => '_list'
            ];
            $response[] = [
                'id' => 'user_active',
                'title' => 'Moje aktivne',
                    'class' => '_list'
            ];
            $response[] = [
                'id' => 'user_inactive',
                'title' => 'Moje neaktivne',
                    'class' => '_list'
            ];

        }
        elseif (is_numeric($parent_id))
        {
            $themes = Theme::model()->findAll([
                'condition' => "parent_id=$parent_id",
                'order' => 'name'
            ]);
            foreach ($themes as $theme)
            {
                $response[] = [
                    'id' => $theme->id,
                    'title' => $theme->name,
                    'class' => $theme->getHTMLClass()
                ];
            }
        }
        else
        {
            $alias = 't';
            $themes = [];
            $_active = 'ACTIVE';
            $user_id = Yii::app()->user->id;
            $uniq = SIMAHtml::uniqid();
            switch ($parent_id) {
                case 'all_active':
                    $themes = Theme::model()->findAll([
                        'condition' => "parent_id is null and status in ('$_active')",
                        'order' => 'name'
                    ]);
                    break;
                case 'all_inactive':
                    $themes = Theme::model()->findAll([
                        'condition' => "parent_id is null and status not in ('$_active')",
                        'order' => 'name'
                    ]);
                    break;
                case 'user_coordinator_active':
                    $themes = Theme::model()->findAll([
                        'condition' => "parent_id is null and status in ('$_active') "
                        . "and exists (select 1 from base.persons_to_themes where coordinator_id=:user$uniq and theme_id=$alias.id)",
                        'params' => [":user$uniq" => $user_id],
                        'order' => 'name',
                        'alias' => $alias
                    ]);
                    break;
                case 'user_coordinator_inactive':
                    $themes = Theme::model()->findAll([
                        'condition' => "parent_id is null and status not in ('$_active') "
                        . "and exists (select 1 from base.persons_to_themes where coordinator_id=:user$uniq and theme_id=$alias.id)",
                        'params' => [":user$uniq" => $user_id],
                        'order' => 'name',
                        'alias' => $alias
                    ]);
                    break;
                case 'user_active':
                    $alias = 't';
                    $themes = Theme::model()->findAll([
                        'condition' => "parent_id is null and status in ('$_active') "
                        . "and exists (select 1 from base.persons_to_themes where person_id=:user$uniq and theme_id=$alias.id)",
                        'params' => [":user$uniq" => $user_id],
                        'order' => 'name',
                        'alias' => $alias
                    ]);
                    break;
                case 'user_inactive':
                    $alias = 't';
                    $themes = Theme::model()->findAll([
                        'condition' => "parent_id is null and status not in ('$_active') "
                        . "and exists (select 1 from base.persons_to_themes where person_id=:user$uniq and theme_id=$alias.id)",
                        'params' => [":user$uniq" => $user_id],
                        'order' => 'name',
                        'alias' => $alias
                    ]);
                    break;
                default: $this->respondFail('code '.$parent_id.' not implemented');
                    
            }
            foreach ($themes as $theme)
            {
                $response[] = [
                    'id' => $theme->id,
                    'title' => $theme->name,
                    'class' => $theme->getHTMLClass()
                ];
            }
        }
        $this->respondOK([
            'subtree' => $response,
            'json' => []
        ]);
    }
    
    public function actionFullInfo($theme_id)
    {
        $theme = ModelController::GetModel('Theme', $theme_id);
        $html = $this->renderModelView($theme, 'full_info_solo');
        $this->respondOK([
            'html'=> $html
        ]);
    }
    
    public function actionConvertTo($theme_id, $model_name)
    {
        $theme = ModelController::GetModel('Theme', $theme_id);
        if (!$theme->validate())
        {
            $this->respondFail(SIMAHtml::showErrors($theme));
        }
        if (!Yii::app()->user->checkAccess('Convert',[],$theme))
        {
            $this->respondFail('Nemate pravo da konvertujete Teme');
        }
        $transaction=Yii::app()->db->beginTransaction();
        try
        {
            if ($model_name == 'Theme')
            {
                switch ($theme->type) 
                {
                    case Theme::$BID:
                        $this->respondFail('Nije implementirano pretvaranje Ponude u obicnu temu');
//                        $bid = $theme->bid;
//                        $bid->setScenario('delete');
//                        $bid->theme->type = Theme::$BID;
//                        if ($bid->validate())
//                        {
//                            $bid->theme->save();
//                            $bid->delete();
//                        }
//                        else
//                        {
//                            throw new SIMAWarnException(SIMAHtml::showErrors($job));
//                        }
                        break;
                    case Theme::$JOB:
                        $job = $theme->job;
                        $job->setScenario('delete');
                        $job->theme->type = Theme::$PLAIN;
                        if ($job->validate())
                        {
                            $job->theme->save();
                            $job->delete();
                        }
                        else
                        {
                            throw new SIMAWarnException(SIMAHtml::showErrors($job));
                        }
                        break;
                    case Theme::$BAF:
                        $baf = $theme->baf;
                        $requests_to_set_theme = [];
                        if (!empty($baf->error_reports)) $this->respondFail ('Izbacite sve greske sa BAF-a');
                        if (!empty($baf->baf_to_baf_requests)) 
                        {
                            foreach ($baf->baf_to_baf_requests as $conn) 
                            {
                                $requests_to_set_theme[] = $conn->baf_request;
                                $conn->delete();
                            }
                        }
                        if (!empty($baf->baf_to_users))
                        {
                            foreach ($baf->baf_to_users as $conn) 
                            {
                                $conn_in_team = PersonToTheme::model()->findByAttributes([
                                    'person_id' => $conn->user_id,
                                    'theme_id' => $theme->id,
                                ]);
                                if (is_null($conn_in_team))
                                {
                                    $conn_in_team = new PersonToTheme();
                                    $conn_in_team->person_id = $conn->user_id;
                                    $conn_in_team->theme_id = $theme->id;
                                    $conn_in_team->save();
                                }
                                $conn->delete();
                            }
                        }

                        $baf->scenario = 'delete';
                        $baf->theme->type = Theme::$PLAIN;
                        if ($baf->validate())
                        {
                            $baf->theme->save();
                            $baf->delete();
                            foreach ($requests_to_set_theme as $request) 
                            {
                                $request->theme_id = $theme->id;
                                $request->save();
                            }
                        }
                        else
                        {
                            throw new SIMAWarnException(SIMAHtml::showErrors($baf));
                        }
                        break;
                    default: $this->respondFail('Nije implementiran '.$theme->type.' -> defult');
                }
            }
            elseif ($model_name == 'Job' && $theme->type == Theme::$PLAIN)
            {
                $_new_job = new Job();
                $_new_job->id = $theme->id;
                $_new_job->theme = $theme;
                $_new_job->status = ($theme->isActive)?Job::$WORKING:Job::$FOR_ARCHIVE;
                $_new_job->financial_status = 'NOT_INVOICED';

                $theme->type = Theme::$JOB;
                $theme->job = $_new_job;

                if ($_new_job->validate() && $theme->validate())
                {
                    $_new_job->save();
                    $theme->save();
                }
                else //exception
                {
                    $errors = '';
                    if ($_new_job->hasErrors())
                    {
                        $errors .= SIMAHtml::showErrors($baf);
                    }
                    if ($theme->hasErrors())
                    {
                        $errors .= SIMAHtml::showErrors($theme);
                    }
                    throw new SIMAWarnException($errors);
                }
            }
            else//exception
            {
                $this->respondFail('Nije implementirano');
            }
            $transaction->commit();
        }
        catch(Exception $e)
        {
            $transaction->rollback();
            throw $e;
        }
        $this->respondOK([], [$theme]);
    }
    
    public function actionRenderThemeSettingsView($theme_id)
    {
        $theme = Theme::model()->findByPkWithCheck($theme_id);
        
        $code_settings = $theme->getSettings();
        $saved_settings = $theme->settings;
        
        $uniq_id = SIMAHtml::uniqid();

        $html = $this->renderPartial('theme_settings_view', [
            'uniq_id' => $uniq_id,
            'theme' => $theme,
            'code_settings' => $code_settings,
            'saved_settings' => $saved_settings
        ], true, false);
        
        $this->respondOK([
            'html' => $html,
            'uniq_id' => $uniq_id
        ]);
    }
    
    public function actionSaveThemeSettings($theme_id)
    {
        $theme = Theme::model()->findByPkWithCheck($theme_id);

        $settings = $this->filter_post_input('settings', false);

        if (!empty($settings))
        {
            $theme->settings = $settings;
            $theme->save();
        }
        
        $this->respondOK();
    }
    
    public function actionGetParentThemeJobTypeAndCostLocation($parent_theme_id)
    {
        $parent_theme = Theme::model()->findByPk($parent_theme_id);
        
        $params = [
            'job_type_id' => null,
            'cost_location_id' => null,
            'cost_location_name' => ''
        ];
        
        if (!empty($parent_theme))
        {
            $params['job_type_id'] = $parent_theme->job_type_id;
            $params['cost_location_id'] = $parent_theme->cost_location_id;
            if (!empty($parent_theme->cost_location_id))
            {
                $params['cost_location_name'] = $parent_theme->cost_location->DisplayName;
            }
        }
        
        $this->respondOK($params);
    }
    
    public function actionAddPersonToTheme($theme_id)
    {
        $theme = Theme::model()->findByPkWithCheck($theme_id);
        $persons = $this->filter_post_input('persons');
        
        foreach ($persons as $person)
        {
            $person_id = $person['id'];
            
            if(PersonToTheme::model()->count([
                'model_filter' => [
                    'person' => [
                        'ids' => $person_id
                    ],
                    'theme' => [
                        'ids' => $theme->id
                    ]
                ]
            ]) === 0)
            {
                $new_person_to_theme = new PersonToTheme;
                $new_person_to_theme->person_id = $person_id;
                $new_person_to_theme->theme_id = $theme->id;
                $new_person_to_theme->save();
            }
        }
        
        $this->respondOK();
    }
    
    public function actionAddCostControllerToFileSignatures($user_id, $theme_id)
    {
        SystemEvent::add(
            'Theme', 
            'addCostControllerToFileSignatures', 
            date("d.m.Y. H:i:s"),
            [
                'user_id' => $user_id,
                'theme_id' => $theme_id,
                'curr_user_id' => Yii::app()->user->id
            ],
            Yii::t('BaseModule.PersonToTheme', 'CostController'),
            false
        );
        
        $this->respondOK();
    }
    
    public function actionPersonOnCostControllerHendler($person_id)
    {
        $is_user = (User::model()->count([
            'model_filter' => [
                'ids' => $person_id
            ]
        ]) === 0) ? false : true;
        
        $this->respondOK([
            'is_user' => $is_user
        ]);
    }
}