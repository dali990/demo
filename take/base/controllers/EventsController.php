<?php

class EventsController extends SIMAController
{

//    public function filters()
//    {
//        return array(
//            'ajaxOnly'
//        ) + parent::filters();
//    }

//    private $tab_session = null;
    
    public function actionGetInitialEventsData()
    {
        $this->sendMessages();
        $this->sendChatUsers();
        $this->sendUpdateVersionEvents();
        
        $this->respondOK();
    }
    
    /**
     * 
     * @param type $new_session_id
     */
    public function actionNewTabSession($new_session_id)
    {
        $tab_session = WebTabSession::model()->findByPk($new_session_id);
        $tab_session['new_session_id'] = Yii::app()->sima_session->id;
        $this->respondOK();
    }
    
    public function actionSetSession($sima_apps_session)
    {
        Yii::app()->sima_session['sima_apps_session'] = $sima_apps_session;   
        $this->respondOK();
    }
    
    /**
     * trenutna funkcija za slanje poruka i poziva se kao inicijalna
     */
    private function sendMessages()
    {
        WebSocketClient::send(
            Yii::app()->user->id, 'USER', 
            'JS_INITIAL_MESSAGES', 
            [
                'messages' => Yii::app()->messageManager->getMessages(['limit'=>Yii::app()->messageManager->getMaxMessagesNum()]),
                'unread_messages' => Yii::app()->messageManager->getUnreadMessages(['limit'=>Yii::app()->messageManager->getMaxMessagesNum()]),
                'marked_messages' => Yii::app()->messageManager->getMarkedMessages(['limit'=>Yii::app()->messageManager->getMaxMessagesNum()]),
                'unread_messages_count' => Yii::app()->messageManager->getUnreadMessagesCount(),
                'marked_messages_count' => Yii::app()->messageManager->getMarkedMessagesCount()
            ]
        );
        return true;
    }
    
    private function sendChatUsers()
    {
        WebSocketClient::send(
            Yii::app()->user->id, 'USER', 
            'JS_INITIAL_CHATUSERS', 
            [  
                'chat_users' => Yii::app()->messageManager->getChatUsers()
            ]
        );
        return true;
    }
    
    private function sendUpdateVersionEvents()
    {
//        $updateScheduleSystemEventCriteria = new SIMADbCriteria([
//            'Model' => UpdateScheduleSystemEvent::model(),
//            'model_filter' => [
//                'scopes' => [
//                    'notStarted',
//                    'toNotify'
//                ]
//            ]
//        ], true);
//        $updateScheduleSystemEvent = UpdateScheduleSystemEvent::model()->find($updateScheduleSystemEventCriteria);
        $updateScheduleSystemEvent = UpdateScheduleSystemEvent::model()->find([
            'scopes' => [
                'notStarted',
                'toNotify'
            ]
        ]);
        
        if(!empty($updateScheduleSystemEvent))
        {
            $message = $updateScheduleSystemEvent->getFormatedNotificationMessage();
            WebSocketClient::send(Yii::app()->user->id, 'USER', 'SHOW_UPDATE_NOTICE', [
                'message' => $message,
            ]);
        }
    }
    
    public function actionSaveEventData()
    {   
        $event_data = filter_input_array(INPUT_POST);
        if(!empty($event_data['event_id']))
        {
            $event_id = $event_data['event_id'];
        }
        else
        {
            $event_id = SIMAHtml::uniqid();
        }
        
        /// TODO: iz nekog razloga ovo ostaje nakon nekog api poziva
        if (isset(Yii::app()->session['allowed_pages']) && Yii::app()->session['allowed_pages']==='allowed_pages')
        {
            unset(Yii::app()->session['allowed_pages']);
        }
        $user_data = Yii::app()->sima_session->offsetGet($event_id);
        if (!empty($user_data['client_data']) && !empty($event_data['client_data']))
        {
            $event_data['client_data'] = array_merge($event_data['client_data'], $user_data['client_data']);
            Yii::app()->sima_session->offsetSet($event_id, $event_data);
        }
        else
        {
            Yii::app()->sima_session->offsetSet($event_id, $event_data);
        }

        $this->respondOK([
            'event_id' => $event_id
        ]);
    }
}
