<?php

class ModelChooseController extends SIMAController
{
    public function actionIndex($model_name)
    {
        $model = $model_name::model();
        $module_name = $model->moduleName();
        $uniq_id = $this->filter_post_input('uniq_id', false);

        if (empty($uniq_id))
        {
            $uniq_id = SIMAHtml::uniqid();
        }
        $params = $this->filter_post_input('params', false);
        $search_model_ids = $this->filter_post_input('search_model_ids', false);

        if(empty($params))
        {
            $params = [];
        }
        $multiselect = filter_var($this->filter_post_input('multiselect', false), FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
        if(is_null($multiselect))
        {
            $multiselect = false;
        }
//        if (isset($_POST['view']))
//        {
//            $view = $_POST['view'];
//        }
        $view = $this->filter_post_input('view', false);
        $title = $this->filter_post_input('title', false);
        if(is_null($title))
        {
            $title = 'Izaberite '.$model->modelLabel();
        }
        
        $modelSearchViews = $model->modelSearchViews();
        $fullscreen = true;

        if (isset($view))
        {
            if ($view === 'guiTable')
            {
                $render_view = 'guiTable';
            }
            else if ($view === 'simple')
            {
                $render_view = 'simple';
                $fullscreen = false;
            }
            else if($view === 'tabs')
            {
                $render_view = 'tabs';
                
                if(!isset($params['tab_params']))
                {
                    $tab_params = $model->getModelChooseTabParams($params);
                    $params['tab_params'] = $tab_params;
                }
                
                $tabs_data = $params['tab_params']['tabs'];
                foreach($tabs_data as $key => $value)
                {
                    if(isset($value['get_params']))
                    {
                        $value['get_params'] = [];
                    }
                    
                    $value['get_params']['uniq_id'] = $uniq_id;
                    
                    if(isset($params['add_to_model_id']))
                    {
                        $value['get_params']['add_to_model_id'] = $params['add_to_model_id'];
                    }
                    if(isset($params['add_to_model_name']))
                    {
                        $value['get_params']['add_to_model_name'] = $params['add_to_model_name'];
                    }
                                        
                    $params['tab_params']['tabs'][$key] = $value;
                }
            }
            else if (in_array($view, $modelSearchViews))
                $render_view = $module_name.'.views.modelSearch.'.  get_class($model).'.'. $view;
            else
                throw new Exception('actionSearchModel - nije zadat search view <b>'.$view.'</b> za model <b>'.  get_class($model).'</b>.');
        }
        else
        {
            if (in_array('default', $modelSearchViews))
            {
                $render_view = $module_name.'.views.modelSearch.'.  get_class($model).'.default';
            }
            else if ($multiselect === true)
            {
                $render_view = 'guiTable';
            }
            else
            {
                $render_view = 'simple';
                $fullscreen = false;
            }
        }
        
        $model_id = $this->filter_post_input('model_id', false);
        if($render_view === 'simple' && !empty($model_id))
        {
            $model = $model->findByPk($model_id);
        }
        
        $view_params = [
            'uniq_id'=>$uniq_id,
            'model'=>$model,
            'search_model_ids'=>$search_model_ids,
            'params'=>$params,
            'title' => $title,
            'multiselect' => $multiselect,
            'simple_filter' => []
        ];
        $filter = $this->filter_post_input('filter', false);
        if (!empty($filter))
        {
            $view_params['filter'] = $filter;
            $view_params['simple_filter'] = $filter;
        }
        $fixed_filter = $this->filter_post_input('fixed_filter', false);
        if (!empty($fixed_filter))
        {
            $view_params['fixed_filter'] = $fixed_filter;
            $view_params['simple_filter'] = $fixed_filter;
        }
        $select_filter = $this->filter_post_input('select_filter', false);
        if (!empty($select_filter))
        {
            $view_params['select_filter'] = $select_filter;
            $view_params['simple_filter'] = $select_filter;
        }
        $left_side_html = $this->renderPartial($render_view, $view_params, true, false);
        
        $respond_params = [
            'left_side_html'=>$left_side_html,
            'fullscreen'=>$fullscreen,
            'is_guitable_view'=>false,
            'right_side_is_guitable_view' => false
        ];
        
        if ($multiselect === true && ($render_view === 'guiTable' || $render_view === 'tabs'))
        {
            if($render_view === 'guiTable')
            {
                $respond_params['is_guitable_view'] = true;
                $respond_params['right_side_html'] = $this->renderPartial('guitable_right_side', $view_params, true, false);
            }
            else if($render_view === 'tabs')
            {
//                $respond_params['is_guitable_view'] = true;
                $respond_params['right_side_is_guitable_view'] = true;
                $right_side_view = $this->filter_input($params, 'right_side_view');
                $right_side_view_params = $this->filter_input($params, 'right_side_view_params');
                $view_params = array_merge($view_params, $right_side_view_params);
                $respond_params['right_side_html'] = $this->renderPartial($right_side_view, $view_params, true, false);
            }
        }
        
        $this->respondOK($respond_params);
    }
    
    public function actionUsersForChoose($uniq_id)
    {
        $this->respondOK([
            'html' => $this->renderPartial('users_for_choose', [
                'uniq_id' => $uniq_id
            ], true)
        ]);
    }
    
    public function actionPartnerListsForChoose($uniq_id)
    {
        $this->respondOK([
            'html' => $this->renderPartial('partner_lists_for_choose', [
                'uniq_id' => $uniq_id
            ], true)
        ]);
    }
    
    public function actionOnTableChoosePartnerList($model_id)
    {
        $partner_list = PartnerList::model()->findByPkWithCheck($model_id);
        
        $users_to_add_data = [];
        $partners_not_added_count = 0;
        
        foreach($partner_list->partners as $partner)
        {
            $user = User::model()->findByPk($partner->id);
            if(is_null($user) || $user->active === false)
            {
                $partners_not_added_count++;
            }
            else
            {
                $users_to_add_data[] = [
                    'id' => $partner->id,
                    'display_html' => $partner->DisplayHTML
                ];
            }
        }
        
        $html = $this->renderPartial('partner_list_choose_dialog', [
            'partners_not_added_count' => $partners_not_added_count,
            'users_to_add_data' => $users_to_add_data,
            'partner_list' => $partner_list
        ], true);
        
        $this->respondOK([
            'html' => $html,
            'user_ids_to_add' => array_column($users_to_add_data, 'id')
        ]);
    }
    
    public function actionThemesForChoose($uniq_id, $add_to_model_id, $add_to_model_name)
    {
        $themes = null;
        if(!empty($add_to_model_id) && !empty($add_to_model_name))
        {
            if($add_to_model_name === CommentThread::class)
            {
                $add_to_model = $add_to_model_name::model()->findByPkWithCheck($add_to_model_id);
                $real_model_id = $add_to_model->model_id;
                $real_model_name = $add_to_model->model;
                $real_model = $real_model_name::model()->findByPkWithCheck($real_model_id);
                
                $themes = $real_model->getThemesModelBelongsTo();
            }
            else if($add_to_model_name === File::class)
            {
                $add_to_model = $add_to_model_name::model()->findByPkWithCheck($add_to_model_id);
                $themes = $add_to_model->getThemesModelBelongsTo();
            }
            else
            {
                throw new Exception('should not have come to this');
            }
        }
        else
        {
            throw new Exception('should not have come to this');
        }
        
        $theme_ids = [-1];
        if(!empty($themes))
        {
            $theme_ids = [];
            foreach($themes as $theme)
            {
                $theme_ids[] = $theme->id;
            }
        }
        
        $this->respondOK([
            'html' => $this->renderPartial('themes_for_choose', [
                'uniq_id' => $uniq_id,
                'theme_ids' => $theme_ids
            ], true)
        ]);
    }
    
    public function actionOnTableChooseTheme($model_id)
    {
        $theme = Theme::model()->resetScope()->findByPkWithCheck($model_id);
        
        $users_to_add_data = [];
        $persons_not_added_count = 0;
        
        foreach($theme->team as $person)
        {
            $user = User::model()->findByPk($person->id);
            if(is_null($user) || $user->active === false)
            {
                $persons_not_added_count++;
            }
            else
            {
                $users_to_add_data[] = [
                    'id' => $person->id,
                    'display_html' => $person->DisplayHTML
                ];
            }
        }
        
        $html = $this->renderPartial('theme_choose_dialog', [
            'persons_not_added_count' => $persons_not_added_count,
            'users_to_add_data' => $users_to_add_data,
            'theme' => $theme
        ], true);
        
        $this->respondOK([
            'html' => $html,
            'user_ids_to_add' => array_column($users_to_add_data, 'id')
        ]);
    }
    
    public function actionPartnersForChoose($uniq_id)
    {
        $this->respondOK([
            'html' => $this->renderPartial('partners_for_choose', [
                'uniq_id' => $uniq_id
            ], true)
        ]);
    }
}