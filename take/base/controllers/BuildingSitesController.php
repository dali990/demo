<?php

class BuildingSitesController extends SIMAController
{

    public function filters()
    {
        return array(
            'ajaxOnly'
        ) + parent::filters();
    }

    public function actionIndex()
    {
        $html = $this->renderPartial('index', array(
            'uniq' => SIMAHtml::uniqid()
        ), true, true);
        
        $this->respondOK(array(
            'html' => $html,
            'title' => 'Gradilista'
        ));
    }
    
    
}
