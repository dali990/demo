<?php

class ModelsMergeController extends SIMAController 
{
    public function actionPerform()
    {
        $data = $this->setEventHeader();
        $model_name = $data['model_name'];
        $model_id1 = $data['model_id1'];
        $model_id2 = $data['model_id2'];
        
        $model1 = $model_name::model()->findByPkWithCheck($model_id1);
        $model2 = $model_name::model()->findByPkWithCheck($model_id2);
        
        try
        {
            $merge_errors = $model1->modelMergeWith($model2, false, true, 0, 100, true);
            if(!empty($merge_errors))
            {
                throw new Exception(json_encode($merge_errors));
            }
        }
        catch(Exception $e)
        {
            $display_message = '';
            $error_message = $e->getMessage();
            $error_message_decoded = json_decode($error_message);
            foreach($error_message_decoded as $error_message_part)
            {
                $display_message .= $error_message_part.'</br>';
            }
            
            $autobafreq_theme_id = 3395;
            $autobafreq_theme_name = 'AutoBafReq Models Merge Fail';
            $autobafreq_message = 'Nije uspelo spajanje modela:'
                                .'</br> - error message: '.$e->getMessage()
                                .'</br> - $model_name: '.SIMAMisc::toTypeAndJsonString($model_name)
                                .'</br> - $model_id1: '.SIMAMisc::toTypeAndJsonString($model_id1)
                                .'</br> - $model_id2: '.SIMAMisc::toTypeAndJsonString($model_id2)
                                .'</br> - $model1: '.SIMAMisc::toTypeAndJsonString($model1)
                                .'</br> - $model2: '.SIMAMisc::toTypeAndJsonString($model2);
            error_log(__METHOD__.' - $autobafreq_message: '.$autobafreq_message);
            Yii::app()->errorReport->createAutoClientBafRequest($autobafreq_message, $autobafreq_theme_id, $autobafreq_theme_name);
                    
            $this->sendEndEvent([
                'message' => $display_message
            ]);
        }
        
        $this->sendEndEvent([
            'message' => 'uspesno'
        ]);
    }
}

