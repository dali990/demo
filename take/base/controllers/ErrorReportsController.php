<?php

class ErrorReportsController extends SIMAController 
{
    public function actionIndex($id)
    {
        $model = ErrorReport::model()->findByPk($id);
        if (is_null($model))
        {
            throw new SIMAWarnExceptionModelNotFound('ErrorReport', $id);
        }
        $selector = $this->filter_post_input('selector',false);
        if (is_null($selector))
        {
            $selector = array();
        }
            
        if (gettype($selector)==='string')
        {
           $selector = CJSON::decode($selector,true) ;
        }

        $default_selected = array_shift($selector);
        if ($default_selected === null || !isset($default_selected['simaTabs']))
        {
            $default_selected = 'info';
        }
        else
        {
            $default_selected = $default_selected['simaTabs'];
        }
        
        $title = $model->DisplayName;
        
        $panel_html = $this->widget('SIMATabs', [
            'list_tabs_model' =>$model,
            'default_selected'=>$default_selected
        ],true);
        $html = $this->renderContentLayout([
            'name' => $title,
            'options' => []
        ],[['html'=>$panel_html]]);
        
        $this->respondOK(array(
            'html' => $html,
            'title' => $title,
        ));
    }
    
    public function actionAddError()
    {
        $submitter_id = Yii::app()->user->id;
        $action = $this->filter_post_input('action',false);
        $message = $this->filter_post_input('message',false);
        $get_params = $this->filter_post_input('get_params',false);
        $post_params = $this->filter_post_input('post_params',false);
        $description = $this->filter_post_input('description',false);
        
        $error_report = Yii::app()->errorReport->addError([
            'submitter_id'=>$submitter_id,
            'action'=>$action,
            'message'=>$message,
            'get_params'=>$get_params,
            'post_params'=>$post_params,
            'description'=>$description
        ]);

        $this->respondOK([
            'error_id' => (!empty($error_report)) ? $error_report->id : null,
            'client_message' => (!empty($error_report)) ? Yii::app()->errorReport->getClientErrorMessageDisplay($error_report->message) : null
        ]);
    }
    
    public function actionEditError($error_id)
    {
        $error_report = ErrorReport::model()->findByPk($error_id);
        if (is_null($error_report))
        {
            throw new SIMAWarnExceptionModelNotFound('ErrorReport', $error_id);
        }
        
        $attributes = $this->filter_post_input('attributes', false);
        if (empty($attributes))
        {
            $attributes = [];
        }
        
        $error_report->attributes = $attributes;
        $error_report->save();
        
        Yii::app()->errorReport->sendError($error_report);

        $this->respondOK([
            'client_message' => Yii::app()->errorReport->getClientErrorMessageDisplay($error_report->message)
        ]);
    }
    
    public function actionAddErrorDesc($error_id)
    {
        $error_report = ErrorReport::model()->findByPkWithCheck($error_id);
        $description = $this->filter_post_input('description',false);
        $error_report->description = $description;
        $error_report->save();
        
        Yii::app()->errorReport->sendError($error_report);
        
        $this->respondOK([]);
    }
    
    public function actionSendError($error_id)
    {
        $error_report = ErrorReport::model()->findByPkWithCheck($error_id);
        $response = Yii::app()->errorReport->sendError($error_report);
        
        if ($response === false)
        {
            throw new SIMAWarnException('Nije uspelo slanje greške sa ID: '.$error_id);
        }
        
        $this->respondOK([]);
    }
    
    public function actionSendAllUnsentErrors()
    {
        $data = $this->setEventHeader();
        if (!isset($_GET['event_id']))
        {
            throw new SIMAException('actionSendAllUnsentErrors - nisu lepo zadati parametri');
        }
        
        $criteria = new SIMADbCriteria([
            'Model'=>'ErrorReport',
            'model_filter'=>[
                'filter_scopes'=>'onlyUnsent'
            ]
        ]);
        $unsent_errors_cnt = ErrorReport::model()->count($criteria);
        
        $i = 1;
        $unsent_errors_ids = [];
        ErrorReport::model()->findAllByParts(function($unsent_errors) use ($unsent_errors_cnt, $unsent_errors_ids, &$i) {
            foreach ($unsent_errors as $unsent_error)
            {
                $percent = round(($i/$unsent_errors_cnt)*100, 0, PHP_ROUND_HALF_DOWN);
                $this->sendUpdateEvent($percent);
                $response = Yii::app()->errorReport->sendError($unsent_error);
                if ($response === false)
                {
                    array_push($unsent_errors_ids, $unsent_error->id);
                }
                $i++;
            }
        }, $criteria);
        
        if (!empty($unsent_errors_ids))
        {
            throw new SIMAWarnException("Nisu poslate sledeće greške sa ID: ".implode(',',$unsent_errors_ids));
        }
        
        $this->sendEndEvent([]);
    }
    
    public function actionOpenClientBafRequest($id)
    {
        $model = ClientBafRequest::model()->findByPkWithCheck($id);
        
        $selector = $this->filter_post_input('selector',false);
        if (is_null($selector))
        {
            $selector = array();
        }
            
        if (gettype($selector)==='string')
        {
           $selector = CJSON::decode($selector,true) ;
        }

        $default_selected = array_shift($selector);
        if ($default_selected === null || !isset($default_selected['simaTabs']))
        {
            $default_selected = 'info';
        }
        else
        {
            $default_selected = $default_selected['simaTabs'];
        }
        
        $title = 'Novi zahtev: '.$model->DisplayName;
        
        $panel_html = $this->widget('SIMATabs', [
            'list_tabs_model' =>$model,
            'default_selected'=>$default_selected
        ],true);
        $html = $this->renderContentLayout([
            'name' => $title,
            'options' => []
        ],[['html'=>$panel_html]]);
        
        $this->respondOK(array(
            'html' => $html,
            'title' => $title,
        ));
    }
    
    public function actionClientBafRequests()
    {
        $html = $this->renderClientBafRequests();

        $this->respondOK([
            'html' => $html,
            'title' => Yii::t('MainMenu', 'ClientBafRequests')
        ]);
    }
    
    public function actionUnsendCurrentPartnerClientBafRequests()
    {
        $html = $this->renderClientBafRequests([
            'requested_by'=>['ids'=>Yii::app()->user->id],
            'server_baf_request_id'=>'null'
        ],[
            'columns_type'=>'fromRequestedBy'
        ]);
        
        $this->respondOK([
            'title' => Yii::t('BaseModule.ClientBafRequest','UnsendClientBafRequests'),
            'html'=>$html
        ]);
    }
    
    public function actionUnsendPartnerClientBafRequests($partner_id)
    {
        $html = $this->renderClientBafRequests([
            'requested_by'=>['ids'=>$partner_id],
            'server_baf_request_id'=>'null'
        ], [
            'columns_type'=>'fromRequestedBy'
        ]);
        
        $this->respondOK([
            'title' => Yii::t('BaseModule.ClientBafRequest','UnsendClientBafRequests'),
            'html'=>$html
        ]);
    }
    
    private function renderClientBafRequests($model_filter=[], $params=[])
    {
        return $this->renderPartial('client_baf_requests', [
            'model_filter'=>$model_filter,
            'params'=>$params
        ], true, false);
    }
    
    public function actionSendClientBafRequest($client_baf_request_id)
    {
        $client_baf_request = ClientBafRequest::model()->findByPkWithCheck($client_baf_request_id);
        $response = Yii::app()->errorReport->sendClientBafRequest($client_baf_request);
        
        if ($response === false)
        {
            throw new SIMAWarnException('Nije uspelo slanje zahteva sa ID: '.$client_baf_request_id);
        }
        
        $this->respondOK([]);
    }
    
    public function actionSendAllUnsentClientBafRequests()
    {
        $data = $this->setEventHeader();
        if (!isset($_GET['event_id']))
        {
            throw new SIMAException('actionSendAllUnsentClientBafRequests - nisu lepo zadati parametri');
        }
        
        $model_filter = isset($data['model_filter']) ? $data['model_filter'] : [];
        
        $criteria = new SIMADbCriteria([
            'Model'=>'ClientBafRequest',
            'model_filter'=>array_merge($model_filter, ['filter_scopes'=>'onlyUnsent'])
        ]);
        $unsent_requests_cnt = ClientBafRequest::model()->count($criteria);
        
        $i = 1;
        $unsent_requests_ids = [];
        ClientBafRequest::model()->findAllByParts(function($unsent_requests) use ($unsent_requests_cnt, $unsent_requests_ids, &$i) {
            foreach ($unsent_requests as $unsent_request)
            {
                $percent = round(($i/$unsent_requests_cnt)*100, 0, PHP_ROUND_HALF_DOWN);
                $this->sendUpdateEvent($percent);
                $response = Yii::app()->errorReport->sendClientBafRequest($unsent_request);
                if ($response === false)
                {
                    array_push($unsent_requests_ids, $unsent_request->id);
                }
                $i++;
            }
        }, $criteria);
        
        if (!empty($unsent_requests_ids))
        {
            throw new SIMAWarnException("Nisu poslate sledeći zahtevi sa ID: ".implode(',',$unsent_requests_ids));
        }
        
        $this->sendEndEvent([]);
    }
    
    public function actionCreateAutoClientBafRequest()
    {
        $message = $this->filter_post_input('message', false);
        $theme_id = $this->filter_post_input('theme_id', false);
        $theme_name = $this->filter_post_input('theme_name', false);

        Yii::app()->errorReport->createAutoClientBafRequest($message, $theme_id, $theme_name);
        
        $this->respondOK();
    }
}
