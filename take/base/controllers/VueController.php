<?php

class VueController extends SIMAController 
{
    public function actionList()
    {
        $model_name = $this->filter_post_input('model_name');
        $criteria = $this->filter_post_input('criteria');
        
        if ($this->isMobileAppRequest() && !is_null($criteria) && is_string($criteria))
        {
            $criteria = CJSON::decode($criteria);
        }
        
        $_model_array = $model_name::model()->findAll($criteria);

        $_array = [];
        foreach ($_model_array as $one_value)
        {
            $_array[] = SIMAMisc::getVueModelTag($one_value);
        }
        
        $_tag = SIMAHtml::getTag(($model_name::model()));
        
        $prop_value = [
            'TYPE' => 'MODEL_ARRAY',
            'ARRAY' => $_array,
            'UPDATE_MODEL_LIST_TAG' => $_tag
        ];
        
        $this->respondOK([
            'list' => $prop_value
        ]);
        
    }
    
    public function actionListCount()
    {
        $model_name = $this->filter_post_input('model_name');
        $criteria = $this->filter_post_input('criteria');
        
        /// MOBILE APP PARSER
        if ($this->isMobileAppRequest() && !is_null($criteria) && is_string($criteria))
        {
            $criteria = CJSON::decode($criteria);
        }
        
        $list_count = $model_name::model()->count($criteria);

        $prop_value = [
            'list_count' => $list_count,
            'UPDATE_MODEL_LIST_TAG' => SIMAHtml::getTag(($model_name::model()))
        ];
        
        $this->respondOK([
            'setter_object' => $prop_value
        ]);
        
    }
    
    private function isMobileAppRequest()
    {
        $is_mobile_app_request = false;
        
        $headers = apache_request_headers();
        if(
                isset($headers['SIMA-Connection-Type']) 
                && $headers['SIMA-Connection-Type'] === 'SIMAMobileApp'
        )
        {
            $is_mobile_app_request = true;
        }
        
        return $is_mobile_app_request;
    }
}