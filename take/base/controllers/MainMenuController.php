<?php

class MainMenuController extends SIMAController 
{
    public function actionGetItems()
    {
        $this->respondOK([
            'items' => Yii::app()->mainMenu->get(true)
        ]);
    }
    
    public function actionGetSIMADesktopAppWidget()
    {
        $sda_html = $this->widget(SIMADesktopApp::class, [], true);
        
        $this->respondOK([
            'sda_html' => $sda_html
        ]);
    }
    
    public function actionToggleItemFromFavoriteGroup($favorite_group_id, $label, $component_name = null, $action = null, $prev_item_to_group_id = null, $next_item_to_group_id = null)
    {
        $transaction = Yii::app()->db->beginTransaction();
        
        try
        {
            $favorite_group = MainMenuFavoriteGroup::model()->findByPkWithCheck($favorite_group_id);
            
            $attributes = [
                'user_id' => Yii::app()->user->id
            ];

            if (!empty($component_name))
            {
                $attributes['component_name'] = $component_name;
            }
            else if (!empty($action))
            {
                $attributes['action'] = $action;
            }

            $favorite_item = MainMenuFavoriteItem::model()->findByAttributes($attributes);
            //ako stavka ne postoji, dodaje se stavka i dodaje se grupi
            if (empty($favorite_item))
            {
                $favorite_item = new MainMenuFavoriteItem();
                $favorite_item->user_id = Yii::app()->user->id;
                $favorite_item->label = $label;
                $favorite_item->action = $action;
                $favorite_item->component_name = $component_name;
                $favorite_item->save();
                
                $item_to_group = MainMenuFavoriteItemToGroup::add($favorite_item, $favorite_group);
                
                if (!empty($prev_item_to_group_id) || !empty($next_item_to_group_id))
                {
                    Yii::app()->mainMenu->moveItem(MainMenuFavoriteItemToGroup::class, $item_to_group->id, $prev_item_to_group_id, $next_item_to_group_id);
                }
            }
            //inace se stavka toggle-uje iz grupe
            else
            {
                $item_to_group = MainMenuFavoriteItemToGroup::model()->findByAttributes([
                    'group_id' => $favorite_group->id,
                    'item_id' => $favorite_item->id
                ]);

                if (empty($item_to_group))
                {
                    $item_to_group = new MainMenuFavoriteItemToGroup();
                    $item_to_group->group_id = $favorite_group->id;
                    $item_to_group->item_id = $favorite_item->id;
                    $item_to_group->save();
                }
                else
                {
                    $item_to_group->delete();
                }
            }
            
            Yii::app()->mainMenu->checkItemsIntegrityForUser(Yii::app()->user->model);
            
            $transaction->commit();
        }
        catch(Exception $e)
        {
            $this->rollbackCurrentTransaction();
            Yii::app()->mainMenu->sendUpdateEventForUser(Yii::app()->user->model, 'MAIN_MENU_ITEMS_CHANGED_ERROR');
            throw $e;
        }
        
        Yii::app()->mainMenu->sendUpdateEventForUser(Yii::app()->user->model);

        $this->respondOK();
    }
    
    public function actionMoveFavoriteItemToGroup($favorite_item_id, $favorite_group_id, $old_favorite_item_to_group_id)
    {
        $transaction = Yii::app()->db->beginTransaction();
        
        try
        {
            $favorite_item = MainMenuFavoriteItem::model()->findByPkWithCheck($favorite_item_id);
            $favorite_group = MainMenuFavoriteGroup::model()->findByPkWithCheck($favorite_group_id);
            
            MainMenuFavoriteItemToGroup::add($favorite_item, $favorite_group);
            
            //brisemo stavku iz grupe iz koje se prevlaci
            $old_item_to_group = MainMenuFavoriteItemToGroup::model()->findByPk($old_favorite_item_to_group_id);
            if (!empty($old_item_to_group))
            {
                $old_item_to_group->delete();
            }

            Yii::app()->mainMenu->checkItemsIntegrityForUser(Yii::app()->user->model);
            
            $transaction->commit();
        }
        catch(Exception $e)
        {
            $this->rollbackCurrentTransaction();
            Yii::app()->mainMenu->sendUpdateEventForUser(Yii::app()->user->model, 'MAIN_MENU_ITEMS_CHANGED_ERROR');
            throw $e;
        }
        
        Yii::app()->mainMenu->sendUpdateEventForUser(Yii::app()->user->model);
        
        $this->respondOK();
    }
    
    public function actionSaveFavoriteItemNewOrder($dragged_model_name, $dragged_model_id, $target_prev_model_id = null, $target_next_model_id = null)
    {
        $transaction = Yii::app()->db->beginTransaction();
        
        try
        {
            //$dragged_item je model koji se prevlaci(MainMenuFavoriteGroup, MainMenuFavoriteItemToGroup)
            $dragged_item = $dragged_model_name::model()->findByPkWithCheck($dragged_model_id);

            //$target_sibling_item je stavka pre/posle koje se ubacuje stavka koja se prevlaci. Sluzi nam da znamo koji nam je target model bez obzira da li on ide pre ili posle stavke koja se prevlaci
            //moze biti MainMenuFavoriteGroup ili MainMenuFavoriteItemToGroup
            $target_sibling_item = $dragged_model_name::model()->findByPkWithCheck(!empty($target_prev_model_id) ? $target_prev_model_id : $target_next_model_id);

            //ako prevlacimo stavku iz grupe u drugu grupu
            if (
                    $dragged_model_name === MainMenuFavoriteItemToGroup::class && 
                    $dragged_item->group_id !== $target_sibling_item->group_id
                )
            {
                $dragged_item_item = $dragged_item->item;
                $dragged_item->delete_item_if_is_last_item_to_group = false;
                $dragged_item->delete();
                $new_dragged_item = MainMenuFavoriteItemToGroup::add($dragged_item_item, $target_sibling_item->group);
                $dragged_model_id = $new_dragged_item->id;
            }

            Yii::app()->mainMenu->moveItem($dragged_model_name, $dragged_model_id, $target_prev_model_id, $target_next_model_id);
            
            Yii::app()->mainMenu->checkItemsIntegrityForUser(Yii::app()->user->model);
            
            $transaction->commit();
        }
        catch(Exception $e)
        {
            $this->rollbackCurrentTransaction();
            Yii::app()->mainMenu->sendUpdateEventForUser(Yii::app()->user->model, 'MAIN_MENU_ITEMS_CHANGED_ERROR');
            throw $e;
        }
        
        Yii::app()->mainMenu->sendUpdateEventForUser(Yii::app()->user->model);
        
        $this->respondOK();
    }
    
    private function rollbackCurrentTransaction()
    {
        $current_transaction = Yii::app()->db->getCurrentTransaction();
        if(!is_null($current_transaction))
        {
            $current_transaction->rollback();
        }
    }
    
    public function actionSetFavoriteGroupToOpened($favorite_group_id)
    {
        //trebalo bi da je jedna grupa otvorena, ali za svaki slucaj ako se slucajno poremetilo stanje, ovim se ispravi
        $opened_favorite_groups = MainMenuFavoriteGroup::model()->findAllByAttributes([
            'user_id' => Yii::app()->user->id,
            'is_default' => true
        ]);
        foreach ($opened_favorite_groups as $opened_favorite_group)
        {
            $opened_favorite_group->is_default = false;
            $opened_favorite_group->save();
        }
        
        $favorite_group = MainMenuFavoriteGroup::model()->findByPkWithCheck($favorite_group_id);
        $favorite_group->is_default = true;
        $favorite_group->save();
        
        $this->respondOK();
    }
}
