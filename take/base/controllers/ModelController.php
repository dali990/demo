<?php

class ModelController extends SIMAController
{
    const viewsPath = 'base.views.model';
    
    public function filters()
    {
        return array(
            'ajaxOnly - tabAjaxLong-multiselectDelete-formLong'
        ) + parent::filters();
    }

    /**
     * funkcija koja vraca update-a pogleda
     * @throws Exception
     */
    public function actionViewUpdate()
    {
        if (!isset($_POST['requests']))
            throw new Exception('Nisu zadati zahtevi prema kojima se updatetuje - $_POST[requests]');
        $responses = array();
        foreach ($_POST['requests'] as $key => $params)
        {
            if (!isset($params['model']) || !isset($params['model_id']) || !isset($params['model_view']))
                throw new Exception('Zahtevi nisu pravilno zadatai - fali jedan od parametara model, model_id, model_view');

            $Model = $params['model'];
            $model = new $Model;
            $views = $model->modelViews();
            $model_view_params = isset($params['params'])?$params['params']:[];
            $additional_classes = isset($params['additional_classes'])?$params['additional_classes']:'';
            if (!isset($views[$params['model_view']]) && !in_array($params['model_view'], $views))
                throw new Exception('Pogled ' . ($params['model_view']) . ' za model ' . $Model . ' ne postoji');

            if (isset($params['model_view']['with']) && isset($views[$params['model_view']['with']]))
            {
                $withs = $views[$params['model_view']] ['with'];
                if (get_type($views[$params['model_view']] ['with']) === 'string')
                    $withs = array($views[$params['model_view']]['with']);
                foreach ($withs as $value)
                {
                    $model = $model->with($value);
                }
            }

            if (!is_numeric($params['model_id']))
            {
                $attributes = CJSON::decode($params['model_id'], true);
                $model = $model->findByAttributes($attributes);
            }
            else
            {
               $model = $model->findByPk($params['model_id']); 
            }            

            if ($model === null)
            {
//                throw new Exception('Model ' . $Model . ' sa PK=' . ($params['model_id']) . ' ne postoji');
                $responses[$key] = '';
            }
            else
            {
                //$response[$key] = $this->renderModelView($model, $params['model_view'], true);
                $responses[$key] = $this->renderModelView($model, $params['model_view'],['params'=>$model_view_params, 'class'=>$additional_classes]);
            }
//            if ($cached)
        }
        
        $processOutput = $this->processOutput('');
        
        $this->respondOK([
            'views' => $responses,
            'processOutput' => $processOutput
        ]);
    }

    public function actionView($model, $model_view, $model_id)
    {
        $model = $model::model()->findByPk($model_id);
        if ($model === null)
            throw new Exception('zadati model ne postoji');

        $response = $this->renderModelView($model, $model_view, [
            'processOutput'=>true
        ]);
        $this->respondOK(['html' => $response]);
    }
    
    public function actionFormLong()
    {
        $data = $this->setEventHeader(); 
        
        $get_array = $this->filter_input($data, 'get');
        $post_array = [];
        parse_str($this->filter_input($data, 'post'), $post_array);
        
        $parsed_form = $this->parseFormAction($get_array, $post_array);
        
        $parsed_status = $parsed_form['status'];
        $form_html = $parsed_form['form_html'];
//        $form_html = $this->processOutput($parsed_form['form_html']);
        $parsed_model_id = $parsed_form['model_id'];
        $parsed_onSave = $parsed_form['onSave'];
        $parsed_form_id = $parsed_form['form_id'];
        $form_data = $parsed_form['form_data'];
        $fullscreen = $parsed_form['fullscreen'];
        $updateModels = $parsed_form['updateModels'];
        
        $this->sendEndEvent([
            'status' => $parsed_status,
            'form_html' => $form_html,
            'model_id' => $parsed_model_id,
            'onSave' => $parsed_onSave,
            'form_id' => $parsed_form_id,
            'form_data' => $form_data,
            'fullscreen' => $fullscreen
        ], $updateModels);
    }

    /**
     * TODO - iskoristiti parseFormAction
     * 
     * @param type $model
     */
    public function actionForm($model)
    {
        // TODO milosj - za mobileapp
        //if(empty($_POST) && !empty(file_get_contents("php://input")))
        //{
        //    $_POST = json_decode(file_get_contents("php://input"), true);
        //}
        
        $form_html = '';
        $updateModels = array();
        $fullscreen = false;
        $model = new $model();
        $model_id = isset($_GET['model_id'])?$_GET['model_id']:null;
        $model->onSave = (isset($_POST['onSave'])) ? $_POST['onSave'] : '';
        $formName = (isset($_GET['formName']) && $_GET['formName'] !== '') ? $_GET['formName'] : 'default';
        $scenario = (isset($_GET['scenario']) && $_GET['scenario'] !== '') ? $_GET['scenario'] : null;
        $form_id = ((isset($_GET['form_id'])) ? $_GET['form_id'] : SIMAHtml::uniqid());
        $relations = $model->relations();        
        $fallbackFormName = null;
        $on_close = null;

        $form_data = array(); //u slucaju da se forma ne cuva nego se podaci iz nje prosledjuju dalje, onda se oni pamte u form_data

        if (is_array($model_id))
        {
            $tempmodel = $model->findByAttributes($model_id);
            $model_id = '';
        }
        else
        {
            $tempmodel = $model->findByPk($model_id);
            $model_id = empty($model_id)?'':intval($model_id);
        }
        
        if ($tempmodel != null)
        {
            $model = $tempmodel;
        }
        else
        {
            $model->id = $model_id;
        }

        //dovlacimo sve zajednicke modele
        //Sasa A. - getRelatedModels mora da bude pozvan pre funkcije modelForms() jer je moguce da se u funkciji modelForms postavljaju init podaci iz
        //nekog povezanog modela
        $models = $this->getRelatedModels($model, $model_id, $formName);

        //Sasa A. - u funkciji modelForms je moguce da se postavljaju init podaci za $model, pa zato mora da bude pre setModelAttributes
        $model_forms = $model->modelForms();
        if (isset($model_forms[$formName]['fallback']))
        {
            $fallbackFormName = $model_forms[$formName]['fallback'];
        }
        
        $ajaxlong = false;
        if(isset($model_forms[$formName]['ajaxlong']))
        {
            $ajaxlong = $model_forms[$formName]['ajaxlong'];
        }
        $before_submit_question = null;
        if(isset($model_forms[$formName]['before_submit_question']))
        {
            $before_submit_question = $model_forms[$formName]['before_submit_question'];
        }

        //postavljamo post vrednosti za sve modele
        foreach ($models as $key=>$_model)
        {
            if(isset($scenario) && $scenario !== '')
            {
                $_model->setScenario($scenario);
            }

            if (isset($_POST[get_class($_model)]))
            {
                //error_log(__METHOD__.' - $_POST[get_class($_model)]: '.SIMAMisc::toTypeAndJsonString($_POST[get_class($_model)]));
                $models[$key]->setModelAttributes($_POST[get_class($_model)], $formName);
                //error_log(__METHOD__.' - $models[$key]: '.SIMAMisc::toTypeAndJsonString($models[$key]));
            }
        }

        $status = $_GET['status'];
        $is_submit = isset($_GET['is_submit'])?true:false;
        if (isset($_POST[get_class($model)]))
        {
            if ($status !== SIMAFormParams::$STATUS_INITIAL && $is_submit) /** Nije inicijalno otvaranje forme * */
            {
                /**
                 * TODO
                 * OVO TREBA IZBACITI - PRIVREMENA ZAKRPA
                 * ceo justupload treba izbaciti iz modela fajla
                 */
                $this->validateJustUploadStatus($model, $formName);
                
                //prvo se vrsi validacija
                foreach ($models as $key=>$_model)
                {
                    if (!$models[$key]->validate())
                    {
                        $status = 'validation_fail';
                        break;//provera samo dok ne dodje do prve lose validacije
                    }
                }
                //ako je prosla validacija
                if ($status !== 'validation_fail')
                {
                    //ako je u pitanju cuvanje forme
                    if ($status !== 'validate')
                    {
                        foreach ($models as $key => $_model)
                        {
                            $_model->beforeMutualSave($_model); 
                        }

                        if ($status !== 'validation_fail')
                        {
                            $id = '';
                            foreach ($models as $key => $_model)
                            {
//                                error_log(__METHOD__.' - $key: '.SIMAMisc::toTypeAndJsonString($key));
                                //error_log(__METHOD__.' - $_model: '.SIMAMisc::toTypeAndJsonString($_model));
                                if (!isset($_model->id) && $id !== '')
                                {
                                    $_model->id = $id;
                                }
                                if (!$_model->isNewRecord)
                                {
                                    array_push($updateModels, $_model);
                                }
                                $id = $this->saveModel($_model);
                            }
                            $status = 'saved';
                        }
                        
                        foreach ($models as $key => $_model)
                        {
                            $_model->afterMutualSave($_model); 
                        }
                    }
                    else
                    {
                        $status = 'validated';
                    }
                    foreach ($models as $key => $_model)
                    {
                        $_model_class_name = get_class($_model);
                        
                        /**
                         * ne mogu samo atributi modela
                         * mora merge sa prosledjenim iz post-a zbog virtuelnih atributa
                         * setModelAttributes ignorise virtuelna polja
                         * atributi modela imaju prioritet, pa su drugi navedeni, da gaze
                         */
                        $post_model_attributes = $this->filter_post_input(get_class($_model) , false);
                        if(empty($post_model_attributes))
                        {
                            $post_model_attributes = [];
                        }
                        $model_attributes = array_merge($post_model_attributes, $_model->attributes);
                        
                        $form_data[$_model_class_name]['attributes'] = $model_attributes;
                        $form_data[$_model_class_name]['display_name'] = $_model->DisplayName;
                    }
                }
            }
        }
        //ako forma nije sacuvana ili validirana onda je renderujemo
        if ($status !== 'saved' && $status !== 'validated')
        {
            //prolazimo kroz sve modele i njihove relacije koje su manymany i ako nadjemo neku da je setovana i da joj je vrednost string
            //onda pretvaramo taj string nazad na niz(to je slucaj kod liste u formama jer se tu setuje vrednost relacije na string id-eva)
            foreach ($models as $key=>$_model)
            {
                $form_list_relations = $_model->modelSettings('form_list_relations');
                $relations = $_model->relations();
                foreach ($relations as $rel_key => $rel_value)
                {
                    if (in_array($rel_key, $form_list_relations))
                    {
                        if (
                                isset($_model->$rel_key) && gettype($_model->$rel_key)==='string'
                                && ($rel_value[0]==='CManyManyRelation' || $rel_value[0]==='CHasManyRelation' || 
                                ($rel_value[0]==='CBelongsToRelation' && $_model->$rel_key !== ''))
                           )
                        {
                            $_model->$rel_key = CJSON::decode($_model->$rel_key);
                        }
                    }
                }
            }
            //renderujemo osnovnu formu(ona koja je otvorena)
            if ($status !== 'validation_fail')
            {
                $form_data = CJSON::encode([
                    'model_name' => get_class($model),
                    'model_id' => $model->id,
                    'form_name' => $formName,
                    'form_status' => $_GET['status']!==SIMAFormParams::$STATUS_INITIAL?$_GET['status']:'',
                    'form_scenario' => $model->getScenario(),
                    'ajaxlong' => $ajaxlong,
                    'before_submit_question' => $before_submit_question
                ]);
                $form_html = "<div id='$form_id' data-form_data='$form_data' class='sima-model-forms sima-layout-panel'>";
            }
            $temp_form_html = '';
            $models_errors = '';
            $response_htmls = '';
            //renderujemo forme za sve zajednicke modele
            $clicked_form_first = $model->modelSettings('clicked_form_first');
            foreach ($models as $_model)
            {
                $models_errors .= SIMAHtml::modelErrorSummary($_model);

                $_class1 = get_class($_model);
                $_class2 = get_class($model);
                $_filter = isset($_POST[$_class1])?$_POST[$_class1]:array();
                $_params = isset($_POST['params'])?$_POST['params']:[];
                $response = ModelController::renderModelForm($_model, [
                    'form_id'=>get_class($_model).$form_id,
                    'formName'=>$formName,                    
                    'filters'=>$_filter,
                    'status'=>$status,
                    'params'=>$_params,
                    'fallbackFormName'=>$fallbackFormName,
                    'title'=>$_model->modelLabel()
                ]);


                //renderujemo sve forme osim osnovne koju smo vec renderovali
                if ($clicked_form_first && !SIMAMisc::classMatch($_class1, $_class2))
                {
                    $temp_form_html .= $response['html'];
                    if ($response['fullscreen'])
                    {
                        $fullscreen = $response['fullscreen'];
                    }
                }
                else
                {
                    $response_htmls .= $response['html'];
                    if ($response['fullscreen'])
                    {
                        $fullscreen = $response['fullscreen'];
                    }
                }
                
                $after_form_close_actions = ModelController::getAfterFormCloseActions($_model, $formName);
                if(!empty($after_form_close_actions))
                {
                    if(is_null($on_close))
                    {
                        $on_close = $after_form_close_actions;
                    }
                    else
                    {
                        $on_close = array_merge($on_close, $after_form_close_actions);
                    }
                }
            }
            $form_html .= "<div class='sima-model-form-errors'>$models_errors</div>";
            $form_html .= $response_htmls;
            $form_html .= $temp_form_html;
            $form_html .= "<div class='clearfix'></div>";

            if ($status !== 'validation_fail')
            {
                $form_html .= '</div>';
                $form_html .= "<div class='sima-model-form-row-button'>".SIMAHtml::modelFormSubmitButton($form_id)."</div>";
            }
            $form_html = $this->processOutput($form_html);
        }

        /** vracanje rezultata */
        $this->respondOK(array(
            'status' => $status,
            'form_html' => $form_html,
            'model_id' => $model->id,
            'onSave' => $model->onSave,
            'form_id' => $form_id,
            'form_data' => $form_data,
            'fullscreen' => $fullscreen,
            'onClose' => $on_close
        ),$updateModels);
    }
    
    /**
     * TODO - koristiti i u actionForm
     * 
     * @param array $get_array
     * @param array $post_array
     * @return type
     */
    private function parseFormAction(array $get_array, array $post_array)
    {
        $form_html = '';
        $updateModels = array();
        $fullscreen = false;
        $model = $get_array['model'];
        $model = new $model();
        $model_id = isset($get_array['model_id'])?$get_array['model_id']:null;
        $model->onSave = (isset($post_array['onSave'])) ? $post_array['onSave'] : '';
        $formName = (isset($get_array['formName']) && $get_array['formName'] !== '') ? $get_array['formName'] : 'default';
        $scenario = (isset($get_array['scenario']) && $get_array['scenario'] !== '') ? $get_array['scenario'] : null;
        $form_id = ((isset($get_array['form_id'])) ? $get_array['form_id'] : SIMAHtml::uniqid());
        $relations = $model->relations();        
        $fallbackFormName = null;

        $form_data = array(); //u slucaju da se forma ne cuva nego se podaci iz nje prosledjuju dalje, onda se oni pamte u form_data

        if (is_array($model_id))
        {
            $tempmodel = $model->findByAttributes($model_id);
            $model_id = '';
        }
        else
        {
            $tempmodel = $model->findByPk($model_id);
            $model_id = empty($model_id)?'':intval($model_id);
        }

        if ($tempmodel != null)
        {
            $model = $tempmodel;
        }

        //dovlacimo sve zajednicke modele
        //Sasa A. - getRelatedModels mora da bude pozvan pre funkcije modelForms() jer je moguce da se u funkciji modelForms postavljaju init podaci iz
        //nekog povezanog modela
        $models = $this->getRelatedModels($model, $model_id, $formName);

        //Sasa A. - u funkciji modelForms je moguce da se postavljaju init podaci za $model, pa zato mora da bude pre setModelAttributes
        $model_forms = $model->modelForms();
        if (isset($model_forms[$formName]['fallback']))
        {
            $fallbackFormName = $model_forms[$formName]['fallback'];
        }
        
        $ajaxlong = false;
        if(isset($model_forms[$formName]['ajaxlong']))
        {
            $ajaxlong = $model_forms[$formName]['ajaxlong'];
        }
        $before_submit_question = null;
        if(isset($model_forms[$formName]['before_submit_question']))
        {
            $before_submit_question = $model_forms[$formName]['before_submit_question'];
        }

        //postavljamo post vrednosti za sve modele
        foreach ($models as $key=>$_model)
        {
            if(isset($scenario) && $scenario !== '')
            {
                $_model->setScenario($scenario);
            }

            if (isset($post_array[get_class($_model)]))
            {
                $models[$key]->setModelAttributes($post_array[get_class($_model)], $formName);
            }
        }

        $status = $get_array['status'];
        $is_submit = isset($get_array['is_submit'])?true:false;
        if (isset($post_array[get_class($model)]))
        {
            if ($status !== SIMAFormParams::$STATUS_INITIAL && $is_submit) /** Nije inicijalno otvaranje forme * */
            {
                /**
                 * TODO
                 * OVO TREBA IZBACITI - PRIVREMENA ZAKRPA
                 * ceo justupload treba izbaciti iz modela fajla
                 */
                $this->validateJustUploadStatus($model, $formName);
                
                //prvo se vrsi validacija
                foreach ($models as $key=>$_model)
                {
                    if (!$models[$key]->validate())
                    {
                        $status = 'validation_fail';
                        break;//provera samo dok ne dodje do prve lose validacije
                    }
                }
                //ako je prosla validacija
                if ($status !== 'validation_fail')
                {
                    //ako je u pitanju cuvanje forme
                    if ($status !== 'validate')
                    {
                        foreach ($models as $key => $_model)
                        {
                            $_model->beforeMutualSave($_model); 
                        }

                        if ($status !== 'validation_fail')
                        {
                            $id = '';
                            foreach ($models as $key => $_model)
                            {
                                if (!isset($_model->id) && $id !== '')
                                {
                                    $_model->id = $id;
                                }
                                if (!$_model->isNewRecord)
                                {
                                    array_push($updateModels, $_model);
                                }
                                $id = $this->saveModel($_model);
                            }
                            $status = 'saved';
                        }
                        
                        foreach ($models as $key => $_model)
                        {
                            $_model->afterMutualSave($_model); 
                        }
                    }
                    else
                    {
                        $status = 'validated';
                    }
                    foreach ($models as $key => $_model)
                    {
                        $_model_class_name = get_class($_model);
                        
                        /**
                         * ne mogu samo atributi modela
                         * mora merge sa prosledjenim iz post-a zbog virtuelnih atributa
                         * setModelAttributes ignorise virtuelna polja
                         * atributi modela imaju prioritet, pa su drugi navedeni, da gaze
                         */
                        $post_model_attributes = $this->filter_post_input(get_class($_model) , false);
                        if(empty($post_model_attributes))
                        {
                            $post_model_attributes = [];
                        }
                        $model_attributes = array_merge($post_model_attributes, $_model->attributes);
                        
                        $form_data[$_model_class_name]['attributes'] = $model_attributes;
                        $form_data[$_model_class_name]['display_name'] = $_model->DisplayName;
                    }
                }
            }
        }
        //ako forma nije sacuvana ili validirana onda je renderujemo
        if ($status !== 'saved' && $status !== 'validated')
        {
            //prolazimo kroz sve modele i njihove relacije koje su manymany i ako nadjemo neku da je setovana i da joj je vrednost string
            //onda pretvaramo taj string nazad na niz(to je slucaj kod liste u formama jer se tu setuje vrednost relacije na string id-eva)
            foreach ($models as $key=>$_model)
            {
                $form_list_relations = $_model->modelSettings('form_list_relations');
                $relations = $_model->relations();
                foreach ($relations as $rel_key => $rel_value)
                {
                    if (in_array($rel_key, $form_list_relations))
                    {
                        if (
                                isset($_model->$rel_key) && gettype($_model->$rel_key)==='string'
                                && ($rel_value[0]==='CManyManyRelation' || $rel_value[0]==='CHasManyRelation' || 
                                ($rel_value[0]==='CBelongsToRelation' && $_model->$rel_key !== ''))
                           )
                        {
                            $_model->$rel_key = CJSON::decode($_model->$rel_key);
                        }
                    }
                }
            }
            //renderujemo osnovnu formu(ona koja je otvorena)
            if ($status !== 'validation_fail')
            {
                $form_data = CJSON::encode([
                    'model_name' => get_class($model),
                    'model_id' => $model->id,
                    'form_name' => $formName,
                    'form_status' => $get_array['status']!==SIMAFormParams::$STATUS_INITIAL?$get_array['status']:'',
                    'form_scenario' => $model->getScenario(),
                    'ajaxlong' => $ajaxlong,
                    'before_submit_question' => $before_submit_question
                ]);
                $form_html = "<div id='$form_id' data-form_data='$form_data' class='sima-model-forms sima-layout-panel'>";
            }
            $temp_form_html = '';
            $models_errors = '';
            $response_htmls = '';
            //renderujemo forme za sve zajednicke modele
            $clicked_form_first = $model->modelSettings('clicked_form_first');
            foreach ($models as $_model)
            {
                $models_errors .= SIMAHtml::modelErrorSummary($_model);

                $_class1 = get_class($_model);
                $_class2 = get_class($model);
                $_filter = isset($post_array[$_class1])?$post_array[$_class1]:array();
                $_params = isset($post_array['params'])?$post_array['params']:[];
                $response = ModelController::renderModelForm($_model, [
                    'form_id'=>get_class($_model).$form_id,
                    'formName'=>$formName,                    
                    'filters'=>$_filter,
                    'status'=>$status,
                    'params'=>$_params,
                    'fallbackFormName'=>$fallbackFormName,
                    'title'=>$_model->modelLabel()
                ]);


                //renderujemo sve forme osim osnovne koju smo vec renderovali
                if ($clicked_form_first && !SIMAMisc::classMatch($_class1, $_class2))
                {
                    $temp_form_html .= $response['html'];
                    if ($response['fullscreen'])
                    {
                        $fullscreen = $response['fullscreen'];
                    }
                }
                else
                {
                    $response_htmls .= $response['html'];
                    if ($response['fullscreen'])
                    {
                        $fullscreen = $response['fullscreen'];
                    }
                }
            }
            $form_html .= "<div class='sima-model-form-errors'>$models_errors</div>";
            $form_html .= $response_htmls;
            $form_html .= $temp_form_html;
            $form_html .= "<div class='clearfix'></div>";

            if ($status !== 'validation_fail')
            {
                $form_html .= '</div>';
                $form_html .= "<div class='sima-model-form-row-button'>".SIMAHtml::modelFormSubmitButton($form_id)."</div>";
            }
//            $form_html = $this->processOutput($form_html); /// TODO - zasto ovo, videti sa sasom
        }
        
        return [
            'status' => $status,
            'form_html' => $form_html,
            'model_id' => $model->id,
            'onSave' => $model->onSave,
            'form_id' => $form_id,
            'form_data' => $form_data,
            'fullscreen' => $fullscreen,
            'updateModels' => $updateModels
        ];
    }
    
    private function validateJustUploadStatus($model, $formName)
    {
        if(gettype($model) !== 'object')
        {
            return;
        }
        if(get_class($model) !== File::class)
        {
            return;
        }
        if($formName !== 'justupload')
        {
            return;
        }
        if(empty($model->file_version_id))
        {
            throw new SIMAWarnException(Yii::t('FilesModule.File', 'JustUploadNotSuccess'));
        }
        try
        {
            SIMAUpload::ParseUploadKey($model->file_version_id, [
                SIMAUpload::$STATUS_SUCCESS
            ]);
        }
        catch (SIMAExceptionSIMAUploadNotRequestedStatus $ex)
        {
            throw new SIMAWarnException(Yii::t('FilesModule.File', 'JustUploadNotSuccess'));
        }
    }
    
    private function getRelatedModels($model, $model_id, $formName)
    {
        $_childrens = $this->getChildrenModels($model, $model_id, $formName);
        return $this->getBaseModels($model, $model_id, $formName, $_childrens);
    }
    
    /**
     * 
     * @param obj $model - pocetni model, tj model za koji se otvara forma
     * @param int $model_id - id tog modela ili id nekog osnovnog modela ako je setovan
     * @param string $formName - ime forme koja se renderuje
     * @param array $done_children - niz gde je prvi element dete a ostalo njegova obradjena deca
     * @return array $models - niz u koji se pamte modeli za koje treba da se renderuju forme
     */
    private function getChildrenModels($model, $model_id, $formName, $done_children = [])
    {
        $models_done = [$model];
        $_model = $model;//array_shift($models_to_do);
        $mutual_forms = $_model->modelSettings('mutual_forms');
        $relations = isset($mutual_forms['relations']) && is_array($mutual_forms['relations'])?$mutual_forms['relations']:[];
        $real_relations = $_model->relations();
        
        if (count($done_children)>0)
        {
            $_md = $done_children[0];
            $_mdc = get_class($_md);
        }
        
        foreach ($relations as $_rel)
        {
            $_founded = false;
            
            if (isset($_md) 
                    && isset($real_relations[$_rel]) 
                    && $this->classMatch($real_relations[$_rel][1],$_mdc)
                )
            {
                $rel_class = $real_relations[$_rel][1];
                $rel_forms = $rel_class::model()->modelForms();
                if (isset($rel_forms[$formName]))
                {
                    $_founded = true;
                    $_model->$_rel = $_md;
                    $models_done = array_merge($models_done,$done_children);
                }
            }
            else if (isset($_model->$_rel))
            {
                $_child_model = $_model->$_rel;
                //povezivanje base_relation
                $child_mutual_forms = $_child_model->modelSettings('mutual_forms');
                if(isset($child_mutual_forms['base_relation']))
                {
                    $_base_relation =  $child_mutual_forms['base_relation']; 
                    $_child_model->$_base_relation = $_model;
                }
                else if (isset($child_mutual_forms['weak_base_relation']))
                {
                    $_base_relation =  $child_mutual_forms['weak_base_relation']; 
                    $_child_model->$_base_relation = $_model;
                }
                else
                {
                    throw new Exception(Yii::t('BaseModule.Common', 'BaseRelationNotSetForMutualForm'));
                }

                $_new_models = $this->getChildrenModels($_child_model, $model_id, $formName);
                $models_done = array_merge($models_done,$_new_models);
            }
        }
        return $models_done;
    }
    
    /**
     * 
     * @param type $model
     * @param int $model_id
     * @param string $formName - ime forme koja se renderuje
     * @param type $done_children
     * @return type
     */
    private function getBaseModels($model, $model_id, $formName, $done_children = [])
    {
        $mutual_forms = $model->modelSettings('mutual_forms');
        $base_relation = isset($mutual_forms['base_relation'])?$mutual_forms['base_relation']:null;
        $_model = $model;
        while(!empty($base_relation))
        {
            $real_relations = $_model->relations();
            $_base_class = $real_relations[$base_relation][1];
            
            $base_forms = $_base_class::model()->modelForms();
            if (!isset($base_forms[$formName]))
            {
                break;
            }
            
            $_base = $_base_class::model()->findByPk($model_id);
            if (!empty($model_id) && empty($_base))
            {
                throw new SIMAWarnException(Yii::t('BaseModule.Common', 'DoNotHaveAccessToInformations'));
            }
            else if ($_base === null)
            {
                $_base = new $_base_class();
            }
            else
            {
                $_model->id = $model_id;
            }
            $_model->$base_relation = $_base;

            $done_children = $this->getChildrenModels($_base, $model_id, $formName, $done_children);
            
            $mutual_forms = $_base->modelSettings('mutual_forms');
            $base_relation = isset($mutual_forms['base_relation'])?$mutual_forms['base_relation']:null;
            $_model = $_base;
        }
        
        return $done_children;
    }
    
    /**
     * 
     * @param array $models_done
     * @param array $models_to_do
     * @param array $_model
     * @return boolean - da li je izvrseno dodavanje
     */
//    private function addModelIfNotDone(&$models_done,&$models_to_do, $_model, $_base_model = null)
//    {
//        $_rel_m_class = get_class($_model);
//
//        $_founded = false;
//        foreach ($models_done as $value)
//        {
//            $_class = get_class($value);
//            if ($this->classMatch($_class,$_rel_m_class))
//            {
//                $_founded = true;
//            }
//        }
//        if(!$_founded)
//        {
//            $models_to_do[] = $_model;
//            if (!is_null($_base_model))
//            {
//                $base_relation =  $_model->modelSettings('mutual_forms')['base_relation']; 
//                $_model->$base_relation = $_base_model;
//            }
//        }
//        return !$_founded;
//    }
    
    /**
     * 
     * @param string $class1
     * @param string $class2
     * @return boolean
     */
    private function classMatch($class1, $class2)
    {
        return $class1 === $class2 || is_subclass_of($class1, $class2) || is_subclass_of($class2, $class1);
    }
    
    /**
     * 
     * @param obj $model
     * @return id - vraca id modela koji je sacuvan
     */
    private function saveModel($model)
    {
        if (!($model->save(false))) //validacija je vec izvrsena
        {
            error_log(__METHOD__.' - '."*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*");
            error_log(__METHOD__.' - '.SIMAHtml::showErrors($model));
            error_log(__METHOD__.' - '."*********************************************************");
//                        throw new Exception("Model je validiran ali nije sacuvan!!!!");
        }
//        $model->refresh(); //ne treba raditi refresh jer Yii sam popunjava PK prilikom cuvanja modela
        return $model->id;
    }
    
    /**
     * 
     * @param Model $model
     * @param array $settings
     * @return array
     * @throws Exception
     */
    public static function renderModelForm($model, $settings)
    {
        $form_id = $settings['form_id'];
        $formName = isset($settings['formName']) ? $settings['formName'] : 'default';
        $filters = isset($settings['filters']) ? $settings['filters'] : [];
        $status = isset($settings['status']) ? $settings['status'] : '';
        $params = isset($settings['params']) ? $settings['params'] : [];
        $fallbackFormName = isset($settings['fallbackFormName']) ? $settings['fallbackFormName'] : null;
        $form_title = isset($settings['title']) ? $settings['title'] : '';

        $model_id = isset($model->id)?$model->id:'';
        $formConf = ModelController::getModelFormToUse($model, $formName);
        
        if (!isset($formConf['type']))
        {
            throw new Exception('nije definisan tip forme za formu sa nazivom "' . $formName . '" za model ' . get_class($model));
        }
        
        if($status === SIMAFormParams::$STATUS_INITIAL)
        {
            ModelController::renderModelForm_beforeActions($model, $formConf);
        }
        
        switch ($formConf['type'])
        {
            case 'generic':                
                if (!isset($formConf['columns']))
                {
                    throw new Exception('nisu definisane kolone za formu sa nazivom "' . $formName . '" za model ' . get_class($model));
                }
                $inner_html = Yii::app()->controller->renderPartial(ModelController::viewsPath . '.form', array(
                    'model' => $model,
                    'form_id' => $form_id,
                    'columns' => $formConf['columns'],
                    'model_id' => $model_id,
                    'formName' => $formName, //ako se forma ne zada, ne pojavljuje se submit dugme
                    'filters' => $filters,
                    'status'=>$status,
                    'params' => $params
                ), true, false);
                break;
            case 'file':                
                if (!isset($formConf['fileName']))
                {
                    throw new Exception('nije definisano ime fajla za formu za model ' . get_class($model));
                }
                $inner_html = Yii::app()->controller->renderPartial(
                        $model->moduleName() . '.views.modelForms.' . get_class($model) . '.' . $formConf['fileName'], 
                        array(
                            'model' => $model,
                            'model_id' => $model_id,
                            'form_id' => $form_id,
                            'formName' => $formName,
                            'filters' => $filters,
                            'status'=>$status,
                            'params' => $params
                        ), 
                        true, //return/echo
                        false //proccessOutput
                );
                break;
            case 'composite':                
                if (!isset($formConf['forms']))
                {
                    throw new Exception('Nisu definisane forme za model ' . get_class($model));
                }
                $inner_html = '';
                foreach ($formConf['forms'] as $_form_key=>$_form_value)
                {
                    $_form_name = $_form_value;
                    $_form_title = '';
                    $_form_visible = true;
                    if (is_array($_form_value))
                    {
                        $_form_name = $_form_key;
                        if (isset($_form_value['title']))
                        {
                            $_form_title = $_form_value['title'];
                        }
                        if (isset($_form_value['visible']))
                        {
                            $_form_visible = $_form_value['visible'];
                        }
                    }
                    if ($_form_visible)
                    {
                        $render_model_form_params = ModelController::renderModelForm($model, [
                            'form_id'=>$_form_key.$form_id,
                            'formName'=>$_form_name,                        
                            'filters'=>$filters,
                            'status'=>$status,
                            'params'=>$params,
                            'fallbackFormName'=>$fallbackFormName,
                            'title'=>$_form_title,
                            'is_recursive_call' => true
                        ]);
                        $inner_html .= $render_model_form_params['html'];
                    }
                }
                break;
            case 'procedure': $inner_html = '';break;//CEMU OVO SLUZI???
            default: $inner_html = '';
                break;
        }
        
        if (isset($formConf['showFormWrapper']) && $formConf['showFormWrapper'] === false)
        {
            $form_html = $inner_html;
        }
        else
        {
            $mutual_form_wrapper_class = !isset($settings['is_recursive_call']) ? '_model-group' : '';
            if (isset($formConf['withoutBorder']) && $formConf['withoutBorder'] === true)
            {
                $mutual_form_wrapper_class .= ' _without-border';
            }
            $form_html = "<div class='mutual_form_wrapper $mutual_form_wrapper_class'>".(!empty($form_title)?"<div class='mutual_form_splitter'>$form_title</div>":'').$inner_html.'</div>';
        }
        
        return [
            'html' => $form_html,
            'fullscreen' => (isset($formConf['fullscreen']) && $formConf['fullscreen'])
        ];
    }
    
    private static function getModelFormToUse(SIMAActiveRecord $model, $formName)
    {
        $model_class = get_class($model);
        $filters = isset($_POST[$model_class])?$_POST[$model_class]:[];
        $formConfs = $model->modelForms(['filters'=>$filters]);
        if (!isset($formConfs[$formName]))
        {
            if (!is_null($fallbackFormName))
            {
                if (isset($formConfs[$fallbackFormName]))
                {
                    $formConf = $formConfs[$fallbackFormName];
                }
                else
                {
                    throw new Exception('nije definisana fallback forma sa nazivom "' . $fallbackFormName  . ' za model '. get_class($model));                
                }
            }
            else
            {
                throw new Exception('nije definisana forma sa nazivom "' . $formName.' za model '.  get_class($model));                
            }
        }
        else
        {
            $formConf = $formConfs[$formName];
        }
        return $formConf;
    }
    
    private static function renderModelForm_beforeActions(SIMAActiveRecord $model, $formConf)
    {
        if(!isset($formConf['before_actions']) || empty($formConf['before_actions']))
        {
            return;
        }
        if(!is_array($formConf['before_actions']))
        {
            throw new SIMAException('form before_actions not array');
        }
        
        foreach($formConf['before_actions'] as $before_action)
        {
            if(!is_array($before_action))
            {
                throw new SIMAException('before_action not array');
            }
            if(!isset($before_action['before_type']))
            {
                throw new SIMAException('before_action before_type not set');
            }
            
            if($before_action['before_type'] !== SIMAFormParams::$BEFORE_TYPE_RENDER)
            {
                continue;
            }
            
            if(!isset($before_action['record_type']))
            {
                throw new SIMAException('before_action record_type not set');
            }
            if(!isset($before_action['action_type']))
            {
                throw new SIMAException('before_action action_type not set');
            }
            if(!isset($before_action['action']))
            {
                throw new SIMAException('before_action action not set');
            }
            
            if(
                $before_action['record_type'] === SIMAFormParams::$RECORD_TYPE_ON_EDIT
                && $before_action['action_type'] === SIMAFormParams::$ACTION_TYPE_MODEL
            )
            {
                $action = $before_action['action'];
                if(!is_array($action))
                {
                    $action = [$action];
                }
                $action_name = array_shift($action);
                $action_params = $action;
                call_user_func_array([$model, $action_name], $action_params);

            }
        }
    }
    
    private static function getAfterFormCloseActions(SIMAActiveRecord $model, $formName)
    {
        $after_form_close_actions = [];
        
        $formConf = ModelController::getModelFormToUse($model, $formName);
        if(!isset($formConf['after_actions']) || empty($formConf['after_actions']))
        {
            return;
        }
        if(!is_array($formConf['after_actions']))
        {
            throw new SIMAException('form after_actions not array');
        }
        foreach($formConf['after_actions'] as $after_action)
        {
            if(!is_array($after_action))
            {
                throw new SIMAException('after_action not array');
            }
            if(!isset($after_action['after_type']))
            {
                throw new SIMAException('after_action after_type not set');
            }
            
            if($after_action['after_type'] !== SIMAFormParams::$AFTER_TYPE_FORM_CLOSE)
            {
                continue;
            }
            
            if(!isset($after_action['action']))
            {
                throw new SIMAException('after_action action not set');
            }
            
            $after_form_close_actions[] = $after_action['action'];
        }
        
        return $after_form_close_actions;
    }
    
    public function actionRemove()
    {
        $modelClass = $this->filter_post_input('model');
        $model_id = $this->filter_post_input('model_id');
        $model = $modelClass::model()->findByPkWithWarnCheck($model_id);
        
        $model->setScenario('delete');
        if (!$model->validate())
        {
            $respond = '<p>'.(Yii::t('BaseModule.Model','CannotDelete')).':</p><ul>';
            
            foreach ($model->getErrors() as $col => $error) 
            {
                foreach ($error as $sub_error)
                {
                    $respond .= '<li><strong>';
                    $respond .= ($col === 'id')?'':$model->getAttributeLabel($col).' - ';
                    $respond .= $sub_error.'</strong></li>';
                }
                
            }
            
            $respond .= '</ul>'; 
            throw new SIMAWarnException($respond);
        }
        else
        {
            $model->delete();
        }
                
        $this->respondOK(array(),$model);
    }
    
    /**
     * 
     * @param string $model
     * @param string $model_id
     * @throws Exception
     */
    public function actionRecycle($model,$model_id)
    {

        $m = $model::model()->findByPk($model_id);
        if ($m == null)
            throw new Exception("ne postoji model '$model' sa zadatim ID '$model_id'");

        $m->recycle();
        $this->respondOK(array(),$m);
    }
    
    /**
     * 
     * @param string $model
     * @param string $model_id
     * @throws Exception
     */
    public function actionDeleteForever($model,$model_id)
    {

        $m = $model::model()->findByPk($model_id);
        if ($m == null)
            throw new Exception("ne postoji model '$model' sa zadatim ID '$model_id'");

        $m->deleteForever();
        $this->respondOK(array(),$m);
    }
    
    /**
     * 
     * @param string $model
     * @param string $model_id
     * @throws Exception
     */
    public function actionRestoreFromTrash($model,$model_id)
    {        
        $m = $model::model()->findByPk($model_id);
        if ($m == null)
            throw new Exception("ne postoji model '$model' sa zadatim ID '$model_id'");
        
        $m->restoreFromTrash();
        $this->respondOK([],[$m]);
    }

    public function actionRemoveForeignKey()
    {
        if (!isset($_POST['model']) || !isset($_POST['model_id']) || !isset($_POST['fk_model']) || !isset($_POST['fk_model_id']) || !isset($_POST['column']))
            throw new Exception('nisu lepo zadati parametri za brisanje');


        $model = $_POST['model']::model()->findByPk($_POST['model_id']);
        $fk_model = $_POST['fk_model']::model()->findByPk($_POST['fk_model_id']);
        $column = $_POST['column'];

        if ($model == null || $fk_model == null)
            throw new Exception('ne postoji model sa zadatim ID');

        $_POST['fk_model']::model()->updateByPk($_POST['fk_model_id'], array("$column" => ""));

        $this->respondOK(array(),array($fk_model,$model));
    }

    public function actionSearchModel($model)
    {
        $model = $model::model();
        $module_name = $model->moduleName();
        $uniq_id = $this->filter_post_input('uniq_id', false);

        if (empty($uniq_id))
        {
            $uniq_id = SIMAHtml::uniqid();
        }
        $params = $this->filter_post_input('params', false);
        $search_model_ids = $this->filter_post_input('search_model_ids', false);

        if(empty($params))
        {
            $params = [];
        }
        $multiselect = false;
        if (isset($_POST['multiselect']))
        {
            $multiselect = filter_var($_POST['multiselect'], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
        }
        if (isset($_POST['view']))
        {
            $view = $_POST['view'];
        }
        if (isset($_POST['title']))
        {
            $title = $_POST['title'];
        }
        else
        {
            $title = 'Izaberite '.$model->modelLabel();
        }
        
        $modelSearchViews = $model->modelSearchViews();
        $fullscreen = true;

        if (isset($view))
        {
            if ($view === 'guiTable')
            {
                $render_view = 'searchModel/guiTable';
            }
            else if ($view === 'simple')
            {
                $render_view = 'searchModel/simple';
                $fullscreen = false;
            }
            else if (in_array($view, $modelSearchViews))
                $render_view = $module_name.'.views.modelSearch.'.  get_class($model).'.'. $view;
            else
                throw new Exception('actionSearchModel - nije zadat search view <b>'.$view.'</b> za model <b>'.  get_class($model).'</b>.');
        }
        else
        {
            if (in_array('default', $modelSearchViews))
            {
                $render_view = $module_name.'.views.modelSearch.'.  get_class($model).'.default';
            }
            else if ($multiselect === true)
            {
                $render_view = 'searchModel/guiTable';
            }
            else
            {
                $render_view = 'searchModel/simple';
                $fullscreen = false;
            }
        }
        
        if($render_view === 'searchModel/simple' && isset($_POST['model_id']))
        {
            $model = $model->findByPk($_POST['model_id']);
        }
        
        $view_params = [
            'uniq_id'=>$uniq_id,
            'model'=>$model,
            'search_model_ids'=>$search_model_ids,
            'params'=>$params,
            'title' => $title,
            'multiselect' => $multiselect,
            'simple_filter' => []
        ];
        if (isset($_POST['filter']))
        {
            $view_params['filter'] = $_POST['filter'];
            $view_params['simple_filter'] = $_POST['filter'];
        }
        if (isset($_POST['fixed_filter']))
        {
            $view_params['fixed_filter'] = $_POST['fixed_filter'];
            $view_params['simple_filter'] = $_POST['fixed_filter'];
        }
        if (isset($_POST['select_filter']))
        {
            $view_params['select_filter'] = $_POST['select_filter'];
            $view_params['simple_filter'] = $_POST['select_filter'];
        }
        $left_side_html = $this->renderPartial($render_view, $view_params, true, false);
        
        $respond_params = [
            'left_side_html'=>$left_side_html,
            'fullscreen'=>$fullscreen,
            'is_guitable_view'=>false
        ];
        
        if ($multiselect === true && $render_view === 'searchModel/guiTable')
        {
            $respond_params['is_guitable_view'] = true;
            $respond_params['right_side_html'] = $this->renderPartial('searchModel/guitable_right_side', $view_params, true, false);
        }
        
        $this->respondOK($respond_params);
    }

    /**
     * funkcija koja na $model i $model_id dodaje objekat u relaciji relName sa $id
     * relacija mora biti MANY_MANY
     * @param type $model -> model na koji 
     * @param type $model_id
     * @param type $relName
     * @param type $rel_id
     */
    public function actionAddToRelation($model, $model_id, $relName)
    {
        if (!isset($model) || !isset($model_id) || !isset($relName))
            throw new Exception("Model controller - actionAddToRelation - nisu lepo zadati parametri!");
        if (!isset($_POST['rel_ids']))
            throw new Exception("Model controller - actionAddToRelation - nisu zadati id-evi relacije!");
        $rel_ids = $_POST['rel_ids'];
        if (!is_array($rel_ids))
            $rel_ids = array($rel_ids);
        
        $model = $model::model()->findByPk($model_id);
        $model_relations = $model->relations();
        foreach ($model_relations as $key => $value)
        {
            if ($value[0] === "CManyManyRelation" && $key === $relName)
            {
                $table_string = $value[2];
                $table_name = trim(strstr($table_string, '(', true));
                $attributes = trim(str_replace(array('(',')'),'',strstr($table_string, '(')));
                $attributes = explode(',', $attributes);
                $base_column = trim($attributes[0]);
                $rel_column = trim($attributes[1]);

                foreach ($rel_ids as $id)
                {
                    $sql = "select * from $table_name where $base_column=$model_id and $rel_column=$id";
                    $contrib = Yii::app()->db->createCommand($sql)->queryAll();
                    if (count($contrib) === 0)
                    {
                        $command = Yii::app()->db->createCommand();
                        $command->insert($table_name, array($base_column=>$model_id, $rel_column=>$id));
                    }
                }
            }
        }
        
        $this->respondOK(array(),array($model));
    }
    
    /**
     * funkcija koja dovlaci vrednost kolone u modelu
     * @param model $model -> model na koji 
     * @param int $model_id
     * @param string $column
     */
    public function actionGetColumnValue($model, $model_id, $column)
    {
        $model = $model::model()->findByPk($model_id);
        if ($model === null)
            throw new Exception('ne postoji model!!');

        $this->respondOK([
            'value' => $model->$column
        ]);
    }
    
    /**
     * funkcija koja zadaje odredjenoj koloni vrednost
     * relacija mora biti MANY_MANY
     * @param type $model -> model na koji 
     * @param type $model_id
     * @param type $relName
     * @param type $rel_id
     */
    public function actionSetColumnValue($model, $model_id, $column)
    {
        $model = $model::model()->findByPk($model_id);
        if ($model === null)
            throw new Exception('ne postoji model!!');
        $value = isset($_POST['value'])?$_POST['value']:'';
        //moraju biti updetovani sve relevantne relacije
        $updateModelViews = array(SIMAHtml::getTag($model));
        $relations = $model->relations();
        foreach ($relations as $rel_name => $rel_def)
            if ($rel_def[2] == $column && isset($model->$rel_name))
                $updateModelViews[] = SIMAHtml::getTag($model->$rel_name);

        $new_values = array($column => $value);
        $model->attributes = $new_values;
        $model->save();

        $this->respondOK([], $updateModelViews);
    }
    
    public function actionSearchField($Model, $relation=null)
    {        
        $html = '';
        
        $text = $this->filter_post_input('search_text', false);
        if (empty($text))
        {
            $text = '';
        }
        
        //specijalni slucaj za partnera
        if (isset($relation) && $relation == 'partner')
        {
            $criteria = new SIMADbCriteria();
            $for_filter = (isset($_POST['filters']) && is_array($_POST['filters'])) ? $_POST['filters'] : [];
            foreach ($for_filter as $_filter_key=>$_filter_value) 
            {
                $_temp_criteria = new SIMADbCriteria(array(
                    'Model'=>'Company',
                    'model_filter'=>$_filter_value
                ));
                $criteria->mergeWith($_temp_criteria);
            }
            
            if (!empty($text))
            {
                $criteria->mergeWith(new SIMADbCriteria([
                    'Model' => 'Company',
                    'model_filter'=>[
                        'text' => $text
                    ]
                ]));
            }
            
            $criteria->limit = 10;
            $companyModel = Company::model();
            $company_settings = $companyModel->modelSettings('searchField');
            $company_type = $company_settings['type'];
            if (gettype($company_type) === 'boolean' && $company_type === true)
            {
                $company_type = 'default';
            }
            $company_scopes = (isset($company_settings['scopes']))?$company_settings['scopes']:array();
            foreach ($company_scopes as $company_scope)
                $companyModel = $companyModel->$company_scope();
            $companies = $companyModel->findAll($criteria);
            
            $criteria = new SIMADbCriteria();
            foreach ($for_filter as $_filter_key=>$_filter_value) 
            {                
                $_temp_criteria = new SIMADbCriteria(array(
                    'Model'=>'Person',
                    'model_filter'=>$_filter_value
                ));
                $criteria->mergeWith($_temp_criteria);
            }
            
            if (!empty($text))
            {
                $criteria->mergeWith(new SIMADbCriteria([
                    'Model' => 'Person',
                    'model_filter'=>[
                        'text' => $text
                    ]
                ]));
            }
            
            $criteria->limit = 10;
            $personModel = Person::model();
            $person_settings = $personModel->modelSettings('searchField');
            $person_type = $person_settings['type'];
            if (gettype($person_type) === 'boolean' && $person_type === true)
            {
                $person_type = 'default';
            }
            $person_scopes = (isset($person_settings['scopes']))?$person_settings['scopes']:array();
            foreach ($person_scopes as $person_scope)
                $personModel = $personModel->$person_scope();
            $persons = $personModel->findAll($criteria);

            $html = $this->renderPartial('searchField/' . $company_type, array(
                'rows' => $companies,
                'text' => $text
                    ), true, false);
            $html = $html . $this->renderPartial('searchField/' . $person_type, array(
                        'rows' => $persons,
                        'text' => $text
                            ), true, false);
        }
        else
        {
            $model = $Model::model();
            if (isset($relation))
            {
                $rel_arr = explode('.', $relation);
                foreach ($rel_arr as $relation)
                {
                    $curr_relations = $model->relations();
                    $_relation = $curr_relations[$relation];
                    if (isset($model->$relation) && is_object($model->$relation))
                        $model = $model->$relation;
                    else
                        $model = $curr_relations[$relation][1]::model();
                }
            }
            
            if (isset($_relation['alias']))
                $alias = $_relation['alias'];
            else
                $alias = 't';
            $Model = get_class($model);
            
            $criteria = new SIMADbCriteria();
            
            $for_filter = (isset($_POST['filters']) && is_array($_POST['filters'])) ? $_POST['filters'] : [];
            foreach ($for_filter as $_filter_key=>$_filter_value) 
            {               
                $_temp_criteria = new SIMADbCriteria(array(
                    'Model'=>$Model,
                    'alias'=>$alias,
                    'model_filter'=>$_filter_value
                ));
                $criteria->mergeWith($_temp_criteria);
            }
            
            if (!empty($text))
            {
                $criteria->mergeWith(new SIMADbCriteria([
                    'Model' => $Model,
                    'alias'=>$alias,
                    'model_filter'=>[
                        'text' => $text
                    ]
                ]));
            }

            $temp_criteria = new SIMADbCriteria();
            //promenljiva u kojoj je relacija kao niz iz koje izvlacimo dodatne ulove za cdb criteriu ako su postavljeni
            if (isset($_relation))
            {
                foreach ($_relation as $key=>$value)
                {
                    if (trim($key) == "select")
                    {
                        $temp_criteria->select = $value;
                    }
                    else if (trim($key) == "alias")
                    {
                        $temp_criteria->alias = $value;
                    }
                    else if (trim($key) == "join")
                    {
                        $temp_criteria->join = $value;
                    }
                    else if (trim($key) == "condition")
                    {
                        $temp_criteria->condition = $value;
                    }
                    else if (trim($key) == "group")
                    {
                        $temp_criteria->group = $value;
                    }
                    else if (trim($key) === 'model_filter')
                    {
                        $_criteria = new SIMADbCriteria(array(
                            'Model'=>$_relation[1],
                            'alias'=>$alias,
                            'model_filter'=>$value
                        ));
                        $temp_criteria->mergeWith($_criteria);
                    }
                }
            }
            $temp_criteria->limit = 50;
            $criteria->mergeWith($temp_criteria);
            $model = $Model::model();
            
            $settings = $model->modelSettings('searchField');
            if (gettype($settings)!=='array')
                throw new Exception('podesavanje searchField mora da bude niz!!');
                
            if (isset($settings['type']))
            {
                $type = $settings['type'];
                if (gettype($type) === 'boolean' && $type === true)
                {
                    $type = 'default';
                }
            }
            else
            {
                $type = 'default';
            }
            
            $scopes = (isset($settings['scopes']))?$settings['scopes']:array();
            foreach ($scopes as $scope)
                $model = $model->$scope();
            
            $order_scope = $model->modelSettings('default_order_scope');
            if (!empty($order_scope))
            {
                $model = $model->$order_scope();
            }
            $model->setTableAlias($alias);
            $rows = $model->findAll($criteria);

            $html = $this->renderPartial('searchField/' . $type, array(
                'rows' => $rows,
                'modelName' => $Model,
                'text' => $text,
                'scopes' => $scopes
            ), true, false);
        }
        
        $this->respondOK(array(
            'result'=>$html
        ));
    }
    
    /**
     * Akcija koja pretrazuje model na osnovu model filtera i vraca rezultat u obliku [id, display_name, $return_additional_columns]
     * @param string $model_name
     * @param array $return_additional_columns
     * @param integer $limit
     * @param string $order
     */
    public function actionSimaSearchModel($model_name, $limit = 50, $order = 'id')
    {
        $model_filter = $this->filter_post_input('model_filter', false) ?? [];
        $additional_columns = $this->filter_post_input('additional_columns', false) ?? [];
        
        $results = [];
        
        $cnt = $model_name::model()->count(['model_filter' => $model_filter]);
        $models = $model_name::model()->findAll([
            'model_filter' => $model_filter,
            'limit' => $limit,
            'order' => $order
        ]);
        
        foreach ($models as $key => $_model) 
        {           
            $result = [
                'id' => $_model->id,
                'display_name' => $_model->DisplayName
            ];
            foreach ($additional_columns as $additional_column)
            {
                $result[$additional_column] = $_model->$additional_column;
            }
            
            $results[] = $result;
        }
        
        $this->respondOK([
            'result' => $results,
            'total' => $cnt
        ]);
    }
    
    public function actionSimaPartnerFieldSaveNewPartner() 
    {
        $company_data = $this->filter_post_input('company_data', false);
        $person_data = $this->filter_post_input('person_data', false);
        $result = [];
        
        if($company_data)
        {
            $company = new Company();
            $company->name = $company_data['name'];
            $company->save();
            $result = [
                'id' => $company->id,
                'DisplayName' => $company->DisplayName
            ];
            
        }

        else if($person_data)
        {
            $person = new Person();
            $person->firstname = $person_data['firstname'];
            $person->lastname = $person_data['lastname'];
            $person->save();
            $result = [
                'id' => $person->id,
                'DisplayName' => $person->DisplayName
            ];
        }
        $this->respondOK([
            'result'=>$result
        ]);
    }

    /**
     * Funkcija koja vraca pretragu za belongs
     * @param type $model
     * @param type $params
     */
    public function actionBelongs($model, $params)
    {
        $attributes = explode(';', $params);
        $for_filter = (isset($_POST['filters']) && is_array($_POST['filters'])) ? $_POST['filters']: [];
        $searched_text = isset($for_filter['text']) ? $for_filter['text'] : '';
        
        $tags = [];

        $themes_model_filter = [
            'OR'
        ];
        
        $plain_themes_model_filter = [];
        if (in_array('plain_themes', $attributes))
        {
            $plain_themes_model_filter['type'] = Theme::$PLAIN;
            $for_themes_statuses = [];
            $themes_statuses = ThemeGUI::$statuses;
            foreach ($themes_statuses as $theme_status_key => $theme_status_value)
            {
                if(in_array("plain_themes_$theme_status_key", $attributes))
                {
                    array_push($for_themes_statuses, $theme_status_key);
                }
            }
            
            if (!empty($for_themes_statuses))
            {
                $plain_themes_model_filter['filter_scopes'] = [
                    'forStatuses' => [$for_themes_statuses]
                ];
            }
            
            array_push($themes_model_filter, $plain_themes_model_filter);
        }
        
        $jobs_model_filter = [];
        if (in_array('jobs', $attributes))
        {
            $jobs_model_filter['type'] = Theme::$JOB;
            $for_jobs_statuses = [];
            $jobs_statuses = Job::$statuses;
            foreach ($jobs_statuses as $job_status_key => $job_status_value)
            {
                if(in_array("jobs_$job_status_key", $attributes))
                {
                    array_push($for_jobs_statuses, $job_status_key);
                }
            }

            if (!empty($for_jobs_statuses))
            {
                $jobs_model_filter['filter_scopes'] = [
                    'forJobStatuses' => [$for_jobs_statuses]
                ];
            }
            
            array_push($themes_model_filter, $jobs_model_filter);
        }
        
        $bids_model_filter = [];
        if (in_array('bids', $attributes))
        {
            $bids_model_filter['type'] = Theme::$BID;
            $for_bids_statuses = [];
            $bids_statuses = Bid::$statuses;
            foreach ($bids_statuses as $bid_status_key => $bid_status_value)
            {
                if(in_array("bids_$bid_status_key", $attributes))
                {
                    array_push($for_bids_statuses, $bid_status_key);
                }
            }
            
            if (!empty($for_bids_statuses))
            {
                $bids_model_filter['filter_scopes'] = [
                    'forBidStatuses' => [$for_bids_statuses]
                ];
            }
            
            array_push($themes_model_filter, $bids_model_filter);
        }
        
        if (count($themes_model_filter) > 1)
        {
            $themes_model_filter = [
                'AND',
                [
                    'limit' => '15',
                    'display_scopes' => 'withFullPaths'
                ],
                $themes_model_filter
            ];
            if (!empty($for_filter))
            {
                array_push($themes_model_filter, $for_filter);
            }

            $models = Theme::model()->findAllByModelFilter($themes_model_filter);
            
            $tags_parents = [];
            $ids = [];
            foreach ($models as $model)
            {
                if (!empty($model->parent_ids) && strlen($model->parent_ids) > 2)
                {
                    $parent_ids = trim(str_replace(array('{', '}'), '', $model->parent_ids));
                    $parents = explode(',', $parent_ids);

                    foreach ($parents as $parent)
                    {
                        if (!in_array($parent, $ids))
                        {
                            $parent_row = Theme::model()->findByPk($parent);                                
                            if (!is_null($parent_row))
                            {
                                if (intval($parent_row->id) === intval($parents[0]))
                                {
                                    $tags[$parent_row->tag->query] = [$this->getThemeDisplayNameForBelongs($parent_row, $searched_text), $parent_row->DisplayName];
                                }
                                else
                                {
                                    $display_model_span = $this->getThemeDisplayNameForBelongs($parent_row, $searched_text);
                                    $tags[$parent_row->tag->query] = ["<span style='padding-left:4em'></span>$display_model_span", $parent_row->DisplayName];
                                }
                                array_push($ids, $parent_row->id);
                            }
                        }
                    }
                }
                if (!empty($model->parent_ids) && strlen($model->parent_ids) <= 2 && !in_array($model->id, $ids))
                {
                    $tags_parents[$model->tag->query] = [$this->getThemeDisplayNameForBelongs($model, $searched_text), $model->DisplayName];
                }
                else
                {
                    $display_model_span = $this->getThemeDisplayNameForBelongs($model, $searched_text);
                    $tags[$model->tag->query] = ["<span style='padding-left:4em'></span>$display_model_span", $model->DisplayName];
                }
            }
            $tags = array_merge($tags, $tags_parents);
        }

        if (in_array('fixed_assets', $attributes))
        {
            $fixed_assets_model_filter = $for_filter;
            $fixed_assets_model_filter['limit'] = 8;
            $models = FixedAsset::model()->byName()->findAllByModelFilter($fixed_assets_model_filter);
            foreach ($models as $model)
            {
                $tags[$model->tag->query] = array("<span style='color: red;'>osn.sred.-></span>".SIMAHtml::boldSearchedText($model->DisplayName, $searched_text), $model->DisplayName);
            }
        }

        if (in_array('employees', $attributes))
        {
            $employees_model_filter = $for_filter;
            $employees_model_filter['limit'] = 8;
            $models = Employee::model()->byName()->findAllByModelFilter($employees_model_filter);
            foreach ($models as $model)
            {
                $tags[$model->tag->query] = array("<span style='color: red;'>zaposleni-></span>".SIMAHtml::boldSearchedText($model->DisplayName, $searched_text), $model->DisplayName);
            }
        }

        if (in_array('personal', $attributes))
        {
            $personal_model_filter = $for_filter;
            $personal_model_filter['limit'] = 5;
            if (!isset($personal_model_filter['scopes']))
            {
                $personal_model_filter['scopes'] = [];
            }
            if (!is_array($personal_model_filter['scopes']))
            {
                $personal_model_filter['scopes'] = [$personal_model_filter['scopes']];
            }

            array_push($personal_model_filter['scopes'], 'user_personal_tags');

            $models = TQLPersonalTag::model()->byName()->findAllByModelFilter($personal_model_filter);
            foreach ($models as $model)
            {
                $tags[$model->tag->query] = array("<span style='color: red;'>lična-></span>".SIMAHtml::boldSearchedText($model->DisplayName, $searched_text), $model->DisplayName);
            }
        }

        if (in_array('security', $attributes))
        {   
            $security_model_filter = $for_filter;
            $security_model_filter['limit'] = 5;
            if (!isset($security_model_filter['scopes']))
            {
                $security_model_filter['scopes'] = [];
            }
            if (!is_array($security_model_filter['scopes']))
            {
                $security_model_filter['scopes'] = [$security_model_filter['scopes']];
            }
            
            array_push($security_model_filter['scopes'], 'user_security_tags');

            $models = TQLSecurityTag::model()->byName()->findAllByModelFilter($security_model_filter);
            foreach ($models as $model)
            {
                $tags[$model->tag->query] = array("<span style='color: red;'>bezbednosna-></span>".SIMAHtml::boldSearchedText($model->DisplayName, $searched_text), $model->DisplayName);
            }
        }

        $html = $this->renderPartial('belongs/tree', [
            'rows' => $tags
        ], true, false);

        $this->respondOK([
            'result' => $html
        ]);
    }
    
    private function getThemeDisplayNameForBelongs($theme, $searched_text)
    {
        $class = $theme->getHTMLClass();
        $theme_name = SIMAHtml::boldSearchedText($theme->name, $searched_text);

        return "<span class='sima-theme-display-colored-span $class'>$theme_name</span>";
    }
    
    /**
     * MilosS(12.12.2018): ova funkcija je vezana koliko znam samo za racune... Potrebno je izbaciti odavde i prebaciti u Accouting Modul
     * poslat je zahtev
     */
    public function actionCancel($model, $id)
    {

        $m = $model::model()->findByPk($id);
        if ($m == null)
            throw new Exception('ne postoji model sa zadatim ID');


        if (isset($m->file) && isset($m->file->bill))
        {
            $Model = 'Bill';
            if ($m->isBill)
            {
                $Model = 'Bill';
            }
            elseif ($m->isAdvanceBill)
            {
                $Model = 'AdvanceBill';
            }
            elseif ($m->isReliefBill)
            {
                $Model = 'ReliefBill';
            }
            elseif ($m->isUnReliefBill)
            {
                $Model = 'UnReliefBill';
            }
            $m = $Model::model()->findByPk($id);


            if ($m->unreleased_amount != $m->amount)
            {
                throw new SIMAWarnBillCancelHaveReleases($m);
            }
            if ($m->bill_type == Bill::$ADVANCE_BILL && $m->connected)
            {
                throw new SIMAWarnBillCancelAdvanceConneced($m);
            }
        }

        $m->canceled = true;
        $m->save();

        $this->respondOK([],$m);
    }

    public function actionTabs($model, $id)
    {
        $model = $model::model()->findByPk($id);
        $tabs = '';
        $params = (isset($_POST['params'])?$_POST['params']:[]);
        if ($model !== null)
        {
            $ModelTabs = $model->TabsBehavior;
            $model->attachBehavior('Tabs',new $ModelTabs());
            $tabs = $model->listTabs($params);
        }
        
        $this->respondOK(array(
            'tabs' => $tabs
        ));
        
    }
    
    /**
     * 
     * @param type $model
     * @param type $id
     * @param type $code
     * @throws SIMAWarnExceptionModelNotFound
     * @throws SIMAWarnExceptionModelTabNotFound
     */
    public function actionTab($model, $id, $code)
    {
        $modelObj = $model::model()->findByPk($id);
        if (empty($modelObj))
        {
            throw new SIMAWarnExceptionModelNotFound($model, $id);
        }
        
        $selector = $this->filter_post_input('selector', false);
        
        $params = [
            'selector' => !empty($selector) ? $selector : []
        ];
        
        $params['params'] = isset($_POST['params'])?$_POST['params']:[];
        if (isset($_GET['module_origin']))
        {
            $params['module_origin'] = $_GET['module_origin'];
        }
        
        $local_params = $this->filter_get_input('local_params',false);
        if (is_null($local_params))
        {
            $local_params = [];
        }
        $params['local_params'] = $local_params;
        
        $ModelTabs = $modelObj->TabsBehavior;
        $modelObj->attachBehavior('Tabs',new $ModelTabs());
        
        $this->respondOK(array(
            'html' => $this->renderModelTab($modelObj,$code,$params,false)
        ));
        
    }
    
    public function actionTabAjaxLong()
    {        
        $data = $this->setEventHeader();
        
        $get_params = $data['get_params'];
        
        $model = $get_params['model'];
        $id = $get_params['id'];
        $code = $get_params['code'];
        
        $modelObj = $model::model()->findByPk($id);
        if ($modelObj == null)
        {
            throw new Exception("ne postoji model $model sa ID-em $id");
        }
        
        $params = array(
            'selector' => array()
        );
        
        $params['params'] = isset($data['params'])?$data['params']:[];
        if (isset($get_params['module_origin']))
        {
            $params['module_origin'] = $get_params['module_origin'];
        }
        
        $ModelTabs = $modelObj->TabsBehavior;
        $modelObj->attachBehavior('Tabs',new $ModelTabs());
        
        $this->sendEndEvent([
            'html' => $this->renderModelTab($modelObj,$code,$params,false)
        ]); 
    }
    
    /**
     * Proverava privilegiju za potvrdjivanje statusa modela
     * @param string $model
     * @param string $column
     * @param boolean $is_revert
     */
    public function actionModelConfirmCheckAccess($model, $column, $is_revert)
    {
        $model_ids = $this->filter_post_input('model_ids', false);
        if (!empty($model_ids))
        {
            if (!is_array($model_ids))
            {
                $model_ids = [$model_ids];
            }

            $errors = [];
            foreach ($model_ids as $model_id)
            {
                $model = $model::model()->findByPkWithWarnAutoBafCheck($model_id);
                $errors[$model->id] = [];
                $errors[$model->id]['model_display_name'] = $model->DisplayName;
                $errors[$model->id]['error_messages'] = $model->checkModelStatusAccessConfirmOrRevert($column, $is_revert);
            }
            
            SIMAHtml::modelsStatusShowErrors($model_ids, $errors);
        }
        
        $this->respondOK();
    }
    
    /**
     * Postavlja status modela(statusi za model zadati u modelSettingsu) u odgovarajuci status
     * @param string $model
     * @param string $column
     * @param mixed(true, false, null) $new_status
     */
    public function actionModelConfirm($model, $column, $new_status)
    {
        $model_ids = $this->filter_post_input('model_ids', false);
        if (!empty($model_ids))
        {
            if (!is_array($model_ids))
            {
                $model_ids = [$model_ids];
            }

            $errors = [];
            foreach ($model_ids as $model_id)
            {
                $model = $model::model()->findByPkWithWarnAutoBafCheck($model_id);
                $errors[$model_id] = [];
                $errors[$model_id]['model_display_name'] = $model->DisplayName;
                $errors[$model_id]['error_messages'] = $model->checkModelStatusAccessConfirmOrRevert($column, $model->isModelNewStatusRevert($column, $new_status));
                
                if (empty($errors[$model_id]['error_messages']))
                {
                    try
                    {
                        $model->beforeChangeLock(true);
                        $model->$column = $new_status;
                        if (!empty($model->modelSettings('statuses')[$column]['user'][0]))
                        {
                            $column_confirm_user_id = $model->modelSettings('statuses')[$column]['user'][0];
                            $model->$column_confirm_user_id = Yii::app()->user->id;
                        }
                        if (!$model->validate())
                        {
                            $errors[$model_id]['error_messages'][] = SIMAHtml::showErrorsInHTML($model);
                        }
                        else
                        {
                            $model->save(false);
                        }
                        $model->afterChangeUnlock();
                    }
                    catch(SIMAWarnException $e)
                    {
                        $errors[$model_id]['error_messages'][] = $e->getMessage();
                    }
                }
            }

            SIMAHtml::modelsStatusShowErrors($model_ids, $errors);
        }
        
        $this->respondOK();
    }
    
    public function actionGetValueFromRelation($model, $relation, $id)
    {
        if (!isset($model) || !isset($relation))
        {
            throw new Exception('Nije zadata model ili relacija!');
        }

        $relations = $model::model()->relations();
        $_relation = $relations[$relation];
        $value = $_relation[1]::model()->findByPk($id);
        $this->respondOK(array(
            'value' => !empty($value)?$value->DisplayName:''
        ));
    }
    
    public function actionMultiselectDelete()
    {
        $data = $this->setEventHeader(); 
        $Model = $this->filter_input($data, 'model');
        $ids = $this->filter_input($data, 'ids');
        $crit = new SIMADbCriteria([
            'Model' => $Model,
            'model_filter' => ['ids' => $ids]
        ]);
        $models = $Model::model()->findAll($crit);
        
        $total_cnt = count($models);
        $curr_cnt = 0;
        foreach ($models as $model)
        {
            set_time_limit(1);
            $model->setScenario('delete');
            if ($model->validate())
            {
                if (!$model->delete(true))
                {
                    $this->raiseNote('Nije uspelo brisanje za '.$model->DisplayName);
                }
            }
            else
            {
                $this->raiseNote('Nije dozvoljeno brisanje '.$model->DisplayName.SIMAHtml::showErrorsInHTML($model));
            }
            $this->sendUpdateEvent(100.0 * ++$curr_cnt / $total_cnt);
//            array_push($updateModelViews, SIMAHtml::getTag($model));
        }
        
        $this->sendEndEvent();
    }
    
    public function actionMultiselectRecycle()
    {
        $Model = $this->filter_post_input('model');
        $ids = $this->filter_post_input('ids');
        $updateModels = [];        
        
        $criteria = new SIMADbCriteria([
            'Model'=>$Model,
            'model_filter'=>[
                'ids'=>$ids
            ]
        ]);        
        $models = $Model::model()->findAll($criteria);
        
        foreach ($models as $model)
        {            
            $model->recycle();
            array_push($updateModels, $model);
        }
        
        $this->respondOK([],$updateModels);
    }
    
//    public function actionMultiselectReview()
//    {
//        $Model = $this->filter_post_input('model');
//        $ids = $this->filter_post_input('ids');
//        $updateModelViews = array();
//        $criteria = new SIMADbCriteria([
//            'Model'=>$Model,
//            'model_filter'=>[
//                'ids'=>$ids
//            ]
//        ]);        
//        $models = $Model::model()->findAll($criteria);
//        
//        foreach ($models as $model)
//        {
//            $signature_view = FileSignatureView::model()->findByAttributes(array('file_id'=>$model->id, 'user_id'=>Yii::app()->user->id));
//            if ($signature_view !== null)
//            {
//                if ($signature_view->signed)
//                {
//                    Yii::app()->raiseNote('Fajl '.$model->DisplayName.' je već pregledan.');
//                }
//                else
//                {
//                    $signature_view->signed = true;
//                    $signature_view->signed_user_id = Yii::app()->user->id;
//                    $signature_view->signed_timestamp = 'now()';
//                    $signature_view->save();
//                    array_push($updateModelViews, SIMAHtml::getTag($model));
//                }
//            }
//        }
//        
//        $this->respondOK([],$updateModels);
//    }
    
    public function actionSetEndTime($id) 
    {
        $session = Session::model()->findByPk($id);
        
        if($session !== null) {
            $session->endSession();
        }
        
        $this->respondOK();
    }
    
    public function actionGetModelDisplayName($model, $model_id)
    {
        $modelObj = $model::model()->findByPkWithCheck($model_id);
        
        $this->respondOK(array(
            'display_name' => htmlentities($modelObj->DisplayName,ENT_QUOTES)
        ));
    }
    
    public function actionGetModelsDisplayName($model)
    {
        $models_ids = $this->filter_post_input('models_ids');
        $models_display_name = [];
        
        if (is_array($models_ids))
        {
            foreach ($models_ids as $model_id)
            {
                $model_obj = $model::model()->findByPkWithCheck($model_id);
                array_push($models_display_name, [
                    'id' => $model_id,
                    'display_name' => $model_obj->DisplayName
                ]);
            }
        }
        
        $this->respondOK([
            'models_display_name' => $models_display_name
        ]);
    }
    
    public function actionGetModelAttributeValue($model, $model_id, $attribute)
    {
        $modelObj = $model::model()->findByPkWithCheck($model_id);
        
        $result = $modelObj->$attribute;
        
        $this->respondOK($result);
    }
    
    public function actionRenderInfoBox($model, $model_id, $view)
    {
        if (!isset($model) || !isset($model_id) || !isset($view))
            throw new Exception('actionRenderInfoBox - nisu lepo zadati parametri');
        
        $model = $model::model()->findByPk($model_id);
        $html = $this->renderModelView($model, $view);
        
        $this->respondOK(array(
            'html'=>$html
        ));
    }
    
    public static function GetModel($modelName, $modelId)
    {
        return $modelName::model()->findByPkWithCheck($modelId);
    }
    
    public function actionCopyModel($Model, $model_id)
    {        
        if (!isset($Model) || !isset($model_id))
            throw new Exception('copyModel - nisu lepo zadati parametri');
        
        $model = $Model::model()->findByPk($model_id);
        if ($model === null)
        {
            throw new Exception("Ne postoji model $Model sa id-em $model_id");
        }
        
        $new_model = $model->copy();
        
        $this->respondOK([
            'model_id' => $new_model->id
        ]);
    }
    
    public function actionGuiTable()
    {
        $params = $this->filter_post_input('params');
        $params['model'] = $params['model']::model();
        
        $html = $this->renderPartial('guiTable', array('params' => $params), true, true);
        
        $this->respondOK(array('html' => $html));
    }
    
    public function actionRenderModelView($model_name, $model_id, $view_name)
    {
        $model = $model_name::model()->findByPk($model_id);
        $html = '';
        if (!is_null($model))
        {
            $html = Yii::app()->controller->renderModelView($model, $view_name);
        }
        
        $this->respondOK([
            'html'=>$html
        ]);
    }
    
    public function actionVueShotProps()
    {
        $requests_string = $this->filter_post_input('requests', false);
        $requests = CJSON::decode($requests_string);

        /// MOBILE APP PARSER
        $headers = apache_request_headers();
        if(
                isset($headers['SIMA-Connection-Type']) 
                && $headers['SIMA-Connection-Type'] === 'SIMAMobileApp' 
                && !is_null($requests) 
                && is_string($requests)
        )
        {
            $requests = CJSON::decode($requests);
        }
        
        if (is_null($requests))
        {
            $this->respondOK([
                'tags_props' => [],
            ]);
        }
        
        $_tags_props_values = [];
        
        foreach ($requests as $request)
        {
            
            $model_name =   $request['model_class'];
            $model_id =     $request['model_id'];
            $model_scopes = isset($request['model_scopes'])?$request['model_scopes']:[];
            
            $model = $model_name::model()->find([
                'model_filter' => [
                    'ids' => $model_id,
                    'scopes' => $model_scopes
                ]
            ]);
            
            $props = $request['props'];
            $_prop_values = [];
            $status = 'NOT_DELETED';
            
            if (is_null($model))
            {
                $status = 'DELETED';
            }
            else
            {
                foreach ($props as $key => $prop)
                {
                    try
                    {
                        $_prop_name = $prop['property'];
                        $_scopes = [];
                        if (isset($prop['scopes']))
                        {
                            $_scopes = $prop['scopes'];
                        }
                        list( $value, $updates) = $model->vueProp($_prop_name, $_scopes);
                        $_prop_values[$prop['hashed_key']] = [
                            'model_prop_hash' => $prop['hashed_key'],
                            'value' => $value,
                            'dependent_on_model_tags' => $updates
                        ];
                    }
                    catch (Exception $e)
                    {
                        if(Yii::app()->params['is_production'] === true)
                        {
                            Yii::app()->errorReport->createAutoClientBafRequest("
                                Za model_name: $model_name i ID: $model_id nije definisan vue-prop: ".SIMAMisc::toTypeAndJsonString($prop)." <br />
                                {$e->getMessage()}
                            ");
                        }
                        else
                        {
                            error_log(__METHOD__." - Za model_name: $model_name i ID: $model_id nije definisan vue-prop: ".SIMAMisc::toTypeAndJsonString($prop));
                            error_log(__METHOD__.' - '.$e->getMessage());
                        }
                        $_prop_values[$prop['hashed_key']] = [
                            'model_prop_hash' => $prop['hashed_key'],
                            'value' => 'ND!!!'
                        ];
                    }
                }
            }
            $_tags_props_values[] = [
                'model_tag_hash' => $request['model_tag_hash'],
                'old_model_tag' => SIMAHtml::getTag($model),
                'status' => $status,
                'props' => $_prop_values
            ];
        }

        $this->respondOK([
            'tags_props_values' => $_tags_props_values,
        ]);
        
    }
}