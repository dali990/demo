<?php

//include('/var/www/html/sima/protected/extensions/phpqrcode/phpqrcode.php');

class QRCodeController extends SIMAController 
{
    public function actionDownloadQRCode($model_name, $model_id, $size)
    {  
        $model = $model_name::model()->findByPkWithCheck($model_id);
        
        $htmlCode = QRCodeComponent::GenerateHTMLForModelQRCode($model, $size);
                 
        $tempPDFFile = new TemporaryFile(null, null, 'pdf');
        $tempPDFFile->createPdfFromHtml([
            'bodyHtml' => $htmlCode
        ], [
            'zoom' => 1.3
        ]);
        $tempFileName = $tempPDFFile->getFileName();
        
        $this->respondOK(['tn' => $tempFileName]);
    }
    
    public function actionMultiselectQRCode()
    {
//        $ids = $this->filter_post_input('ids');
//        $model = $this->filter_post_input('model');
        
//        $criteria = new SIMADbCriteria([
//            'Model'=>$model,
//            'model_filter'=>[
//                'ids'=>$ids
//            ]
//        ]);        
//        $models = $model::model()->byNumber()->findAll($criteria);
        
//        $tempFileName = QRCodeComponent::GenerateTempPdfForModelsQRCodes($models);
        $tempFileName = QRCodeComponent::GenerateTempPdfForModelsQRCodes();
        
        $this->respondOK(['tn' => $tempFileName]);
    }
}
